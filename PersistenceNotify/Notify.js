﻿ItHelpCenter.TNotifyList = function (inObject)
{
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();

    this.Modal.Body.This.innerHTML = "";
    this.Object = this.Modal.Body.This;
    _this.Modal._inObjectHTMLBack = inObject;

    _this.Mythis = "TNotifyList";
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1", "Notifications");

    this.NotifyRows = new Array();
    this.ViewDeleted = false;
    this.ColDetalle = null;

    this.InitializeComponent();

    _this.ModalResult = Source.Page.Properties.TModalResult.Create;

    _this.CreateNotifyList();
}.Implements(new Source.Page.Properties.PageModal());


ItHelpCenter.TNotifyList.prototype.chkViewDeleted_onClick = function (sender, EventArgs)
{
    var _this = sender;
    _this.ViewDeleted = chkViewDeleted.Checked;
}


ItHelpCenter.TNotifyList.prototype.BtnRefresh_OnClick = function (sender, EventArgs)
{
    var _this = sender;
    _this.ColDetalle.innerHTML = "";
    _this.CreateNotifyList();
}


ItHelpCenter.TNotifyList.prototype.CreateNotifyList = function ()
{
    var _this = this.TParent();
    direc = SysCfg.App.Properties.xRaiz + 'PersistenceNotify/wcfChildViewNotifyList.svc/GetNotifyList';
    $.ajax({
        type: "POST", url: direc,
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response)
        {
            for (var i = 0; i < response.length; i++)
            {
                var notify = new TNotify(response[i], _this);
                _this.NotifyRows.push(notify)
                notify.CreateLayout(_this.ColDetalle, i);
                if (!_this.ViewDeleted) //Si no quiero ver eliminados
                {
                    if (notify.Notify.ISSENDCONSOLE) {
                        notify.VclDropDownPanel.Visible = false;
                    }
                }
            }
        },
        error: function (response) {
            SysCfg.Log.Methods.WriteLog("Notify.js TNotifyList.prototype.CreateNotifyList 'PersistenceNotify/wcfChildViewNotifyList.svc/GetNotifyList" + "Error no llamo al servicio GetNotifyList");
        }
    });
}

/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////

var TNotify = function (inNotify, inNotifyList) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();

    this.Notify = inNotify;
    this.NotifyList = inNotifyList;
    this.VclDropDownPanel;
    this.btnDelete = null;
}

TNotify.prototype.CreateLayout = function (inObject, inIdx) {
    var _this = this.TParent();

    this.VclDropDownPanel = new TVclDropDownPanel(inObject, "");
    this.VclDropDownPanel.BoderTopColor = "#F39C12";
    this.VclDropDownPanel.BoderAllColor = "gray";
    this.VclDropDownPanel.BackgroundColor = "white";

    this.VclDropDownPanel.MarginBottom = "10px";
    if (!Source.Menu.IsMobil)
    {
        this.VclDropDownPanel.MarginLeft = "5px";
        this.VclDropDownPanel.MarginRight = "5px";
    }

    this.VclDropDownPanel._functionOpen = function ()
    {
        _this.VclDropDownPanel.CleanBody();
        var sp_Detalle = new TVclStackPanel(inObject/*null*/, "detalle" + inIdx, 1, [[12], [12], [12], [12], [12]]);
        sp_Detalle.Row.Padding = "0px";

        var SDTYPEUSER = UsrCfg.SD.Properties.TSDTypeUser.GetEnum(_this.Notify.IDSDTYPEUSER)

        if (SDTYPEUSER == UsrCfg.SD.Properties.TSDTypeUser._User)
        {
            var SMConsoleUserSwitch = new ItHelpCenter.SD.CaseUser.Atention.TSMConsoleUserSwitch(
                sp_Detalle.Column[0].This,
                function (_this)
                {
                    $(sp_Detalle.Column[0].This.childNodes).addClass("col-lg-12");

                },
                _this.Notify.IDSDCASE
            );
        }
        else
        {

            var IDSDCASE = _this.Notify.IDSDCASE;
            var IDSDCASEMT = _this.Notify.IDSDCASEMT;
            var IDSDTYPEUSER = _this.Notify.IDSDTYPEUSER;
            var SMConsoleManagerSwitch = new ItHelpCenter.SD.CaseManager.Atention.TSMConsoleManagerSwitch(null, //MODO Forza caso 3 en owner             
             sp_Detalle.Column[0].This,
            function (outthis)
            {
                $(sp_Detalle.Column[0].This.childNodes).addClass("col-lg-12");
            },
            IDSDCASE, IDSDCASEMT, IDSDTYPEUSER  //MODO Forza caso 3 en owner   
            );

        }

        _this.VclDropDownPanel.AddToBody(sp_Detalle.Column[0].This/*sp_Detalle.Row.This*/);

    }
    ////////////////////////////
    ////////////////////////////

    var sp_Cabecera = new TVclStackPanel(null, "cabecera" + inIdx, 4, [[1, 7, 3, 1], [1, 7, 3, 1], [1, 7, 3, 1], [1, 8, 2, 1], [1, 8, 2, 1]]);
    sp_Cabecera.Row.MarginTop = "3px";
    sp_Cabecera.Row.MarginLeft = "0px";

    Uimg(sp_Cabecera.Column[0].This, "idimage1" + inIdx, "image/24/case.png", "");

    var lblTitulo = new TVcllabel(sp_Cabecera.Column[1].This, "", TlabelType.H0);
    lblTitulo.Text = "[" + this.Notify.TYPEUSERNAME + "] " + this.Notify.CHANGE_DESCRIPTION
    lblTitulo.VCLType = TVCLType.BS;

    var lblFecha = new TVcllabel(sp_Cabecera.Column[2].This, "", TlabelType.H0);
    lblFecha.Text = formatJSONDate(this.Notify.EVENTDATE);
    lblFecha.VCLType = TVCLType.BS;

    if (!this.Notify.ISSENDCONSOLE)
    {
        this.btnDelete = new TVclImagen(sp_Cabecera.Column[3].This, "");
        var dirimg = "image/24/Trash-can.png";
        this.btnDelete.Src = dirimg;
        this.btnDelete.Title = "Delete notification";
        this.btnDelete.Cursor = "pointer";
        this.btnDelete.onClick = function ()
        {
            var param = {
                'IDSDNOTIFY': _this.Notify.IDSDNOTIFY
            }
            param = JSON.stringify(param);
            direc = SysCfg.App.Properties.xRaiz + 'PersistenceNotify/wcfChildViewNotifyList.svc/DeleteNotify';
            $.ajax({
                type: "POST", url: direc,
                data: param,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
            });
            _this.VclDropDownPanel.This[0].This.parentNode.removeChild(_this.VclDropDownPanel.This[0].This);
        }
    }

    this.VclDropDownPanel.AddToHeader(sp_Cabecera.Row.This);

    ////////////////////////////////
    ////////////////////////////////

}