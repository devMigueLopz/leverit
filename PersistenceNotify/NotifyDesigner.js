﻿ItHelpCenter.TNotifyList.prototype.InitializeComponent = function ()
{
    var _this = this.TParent();

    var sp_Principal = new TVclStackPanel(_this.Object, "2", 1, [[12], [12], [12], [12], [12]]);

    ////////////////////////////////////

    var sp_Titulo = new TVclStackPanel(sp_Principal.Column[0].This, "zonaTitulo", 3, [[6, 3, 2], [6, 3, 2], [6, 3, 2], [6, 4, 2], [6, 4, 2]]);

    var lblTitulo = new TVcllabel(sp_Titulo.Column[0].This, "", TlabelType.H0);
    lblTitulo.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1");
    lblTitulo.VCLType = TVCLType.BS;
    lblTitulo.This.style.fontSize = "18px";

    if (!UsrCfg.Version.IsProduction) {
        var chkViewDeleted = new TVclInputcheckbox(sp_Titulo.Column[1].This, "notify_chkViewDeleted");
        chkViewDeleted.Checked = _this.ViewDeleted;
        chkViewDeleted.VCLType = TVCLType.BS;
        chkViewDeleted.onClick = function myfunction() {
            _this.chkViewDeleted_onClick(_this, chkViewDeleted)
        }
    }

    var btnRefresh = new TVclButton(sp_Titulo.Column[2].This, "btnRefresh");
    btnRefresh.Cursor = "pointer";
    btnRefresh.Src = "image/24/Refresh.png";
    btnRefresh.VCLType = TVCLType.BS;
    btnRefresh.This.style.MaxWidth = "10%";
    btnRefresh.This.classList.add("btn-xs");
    btnRefresh.onClick = function myfunction()
    {
        _this.BtnRefresh_OnClick(_this, btnRefresh);
    }

    ////////////////////////////////////

    var sp_Detalle = new TVclStackPanel(sp_Principal.Column[0].This, "zonaDetalle", 1, [[12], [12], [12], [12], [12]]);
    _this.ColDetalle = sp_Detalle.Column[0].This;

    sp_Detalle.Row.MarginTop = "5px";

    ////////////////////////////////////
}

