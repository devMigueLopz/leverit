﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="prueba.aspx.cs" Inherits="ITHelpCenter.prueba" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head id="Head1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="Componet/BootStrap_Source/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="Componet/BootStrap_Source/bootstrap-table/src/bootstrap-table.css" rel="stylesheet">
    <link href="Scripts/jquery-ui.min.css" rel="stylesheet">
    <link href="Scripts/jquery-ui-timepicker-addon.css" rel="stylesheet">

    <script src="Scripts/jquery2-1-4min.js"></script>
    <script src="Scripts/jquery-ui.min.js"></script>
    <script src="Scripts/jquery-ui-timepicker-addon.js"></script>
    <script src="Scripts/jquery.mousewheel.js"></script>
    <script src="Componet/BootStrap_Source/bootstrap/js/bootstrap.min.js"></script>
    <script src="Componet/BootStrap_Source/bootstrap-table/src/bootstrap-table.js"></script>
</head>
<%--<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>--%>
<body>
    <div class="row" id="StackPanel__0" style="margin: 15px 20px 10px 10px; border: 1px solid black; border-image: none; background-color: rgb(221, 230, 189);" data-type="Row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="StackPanel__0_0" data-type="Col">
            <div class="row" id="StackPanel_h1_0" data-type="Row">
                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 pull-right" id="StackPanel_h1_0_0" style="min-height: 42px;" data-type="Col">
                    <div class="row" id="StackPanel_h1_1_0" data-type="Row">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 text-right" id="StackPanel_h1_1_0_0" data-type="Col">
                            <img title="Refresh" style="margin-top: 8px; cursor: pointer;" alt="" src="image/32/Rules.png">
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 text-right" id="StackPanel_h1_1_0_1" data-type="Col">
                            <img title="Close" style="margin-top: 8px; cursor: pointer;" alt="" src="image/32/Go-back.png">
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-xl-2" id="StackPanel_h1_0_1" style="min-height: 42px;" data-type="Col">
                    <div class="row" id="StackPanel_h1_2_0" data-type="Row">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6" id="StackPanel_h1_2_0_0" style="min-height: 42px; background-color: rgb(128, 128, 128);" data-type="Col">
                            <img title="" style="margin-top: 8px;" alt="" src="image/24/CaseGreen.png">
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6" id="StackPanel_h1_2_0_1" style="background-color: rgb(128, 128, 128);" data-type="Col">
                            <h4 style="color: white; font-size: 20px; font-weight: 700;">0000</h4>
                        </div>
                    </div>
                </div>
                <div class=" col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8" id="StackPanel_h1_0_5" style="min-height: 42px;" data-type="Col">
                    <h4 style="color: rgb(0, 148, 255); font-size: 20px; font-weight: 700;">Titulo del caso</h4>
                </div>
            </div>
            <div class="row" id="StackPanel_h1_1" data-type="Row">
                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 pull-left" id="StackPanel_h1_0_2" style="min-height: 42px;" data-type="Col">
                    <div class="row" data-type="Row">
                        <div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="Div6" data-type="Col">
                            <img style="margin-top: 6px;" alt="" src="http:/image/24/siren_on.png"><img style="margin-top: 6px;" alt="" src="http:/image/24/siren_on.png"><img style="margin-top: 6px;" alt="" src="http:/image/24/siren_off.png">
                        </div>
                    </div>
                    <div class="row" data-type="Row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="Div7" data-type="Col">
                            <h4 class="text-right" style="font-size: 15px; font-weight: 700;">Fecha del Reporte:13/04/2018 </h4>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 pull-right" id="StackPanel_h1_0_4" style="min-height: 42px;" data-type="Col">
                    <div class="row" data-type="Row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="Div5" data-type="Col">
                            <h4 class="text-right" style="font-size: 15px; font-weight: 700;">Actual Status: </h4>
                        </div>
                    </div>
                    <div class="row" data-type="Row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="Div8" data-type="Col">
                            <h4 class="text-center" style="font-size: 15px; font-weight: 700;">User caso: </h4>
                        </div>
                    </div>

                </div>
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8" id="StackPanel_h1_0_6" style="min-height: 42px;" data-type="Col">
                    <div class="row" id="StackPanel_0_0" data-type="Row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="StackPanel_0_0_0" data-type="Col">
                            <h4 style="font-size: 18px; font-weight: normal; margin-top: 3px; margin-bottom: 3px;">Detalle: El firewal of Servicio d\Fallo</h4>
                        </div>
                    </div>
                    <div class="row" id="StackPanel_1_0" data-type="Row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="StackPanel_1_0_0" data-type="Col">
                            <h4 style="font-size: 15px; font-weight: normal; margin-top: 3px; margin-bottom: 3px;">Fecha del Reporte:13/04/2018</h4>
                        </div>
                    </div>
                    <div class="row" id="Div1" data-type="Row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="Div2" data-type="Col">
                            <h4 style="font-size: 15px; font-weight: normal; margin-top: 3px; margin-bottom: 3px;">Solución alternativa no encontrada:Owner Step Summary of</h4>
                        </div>
                    </div>
                    <div class="row" id="Div3" data-type="Row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="Div4" data-type="Col">
                            <h4 style="font-size: 15px; font-weight: normal; margin-top: 3px; margin-bottom: 3px;">Adjunto_Incident Recommendation forServicio de Seguridad\Firewall\Desconocido Detail:Fallo</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" id="StackPanel_h1_2" data-type="Row">
                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 pull-left" id="StackPanel_h1_0_8" style="min-height: 42px;" data-type="Col">
                    <h4 class="text-center" style="font-size: 15px; font-weight: 700;">Timer: </h4>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 pull-right" id="StackPanel_h1_0_9" style="min-height: 42px;" data-type="Col">
                    <h4 class="text-center" style="font-size: 15px; font-weight: 700;">Atention: </h4>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8" id="StackPanel_h1_0_10" style="min-height: 42px;" data-type="Col">
                    <h4 class="text-left" style="font-size: 15px; font-weight: normal;">Adjunto_Incident Recommendation forServicio de Seguridad\Firewall\Desconocido Detail:Fallo: </h4>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
