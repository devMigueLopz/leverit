﻿ItHelpCenter.Demo.TDemoDevices = function (inObject, incallback) {

    this.TParent = function () {
        return this;
    }.bind(this);

    var _this = this.TParent();
    this.ObjectHtml = inObject;
    this.Callback = incallback;    
    this.Mythis = "TDemoDevices";

    ///Recuperamos el path del css segun Devie establecido
    var PathCSS = ItHelpCenter.ScriptManager.GetStyle("Demo/DemoDevices/DemoDevices.css");
    //Creamos un Id para el Css
    var IDparthCss = "DemoDevicesCSS";
    //Cargamos el css pasando el Path que recibimos y el Id creado
   
    ItHelpCenter.ScriptManager.AddStyleFile(PathCSS, IDparthCss, function () {
        //Continuamos con las funciones 
        _this.Load();
    }, function (e) {
    });   
}

ItHelpCenter.Demo.TDemoDevices.prototype.Load = function () {
    var _this = this.TParent();
    var ObjHtml = _this.ObjectHtml;
    ObjHtml.innerHTML = "";

    //ESTRUCTURA 
    var Container = new TVclStackPanel(ObjHtml, "ContainerDemo", 1, [[12], [12], [12], [12], [12]]);
    var ContainerCont = Container.Column[0].This;
    $(ContainerCont).addClass(_this.Mythis + "_Container");

    //div1  IMG
    var div1 = new TVclStackPanel(ContainerCont, "CDdiv1", 1, [[12], [12], [12], [12], [12]]);
    var div1Cont = div1.Column[0].This;
    $(div1Cont).addClass(_this.Mythis + "_div");

    //div2 LABEL
    var div2 = new TVclStackPanel(ContainerCont, "CDdiv2", 2, [[12], [12], [12], [12], [12]]);
    var div2Cont = div2.Column[0].This;
    $(div2Cont).addClass(_this.Mythis + "_div");
    $(div2Cont).addClass(_this.Mythis + "_divC");

    //div3 textboxt
    var div3 = new TVclStackPanel(ContainerCont, "CDdiv3", 1, [[12], [12], [12], [12], [12]]);
    var div3Cont = div3.Column[0].This;
    $(div3Cont).addClass(_this.Mythis + "_div");
    $(div3Cont).addClass(_this.Mythis + "_divC");

    //div3 Boton
    var div4 = new TVclStackPanel(ContainerCont, "CDdiv4", 1, [[12], [12], [12], [12], [12]]);
    var div4Cont = div4.Column[0].This;
    $(div4Cont).addClass(_this.Mythis + "_div");

    //AGREGAR ELEMENTOS
    var Img = new TVclImagen(div1Cont, "CDimg");
    Img.Src = "image/High/IT-Help-Center-A.png";
    $(Img.This).addClass(_this.Mythis + "_Img");

    var Lab = new Vcllabel(div2Cont, "Label", TVCLType.BS, TlabelType.H0, "Imformación para demo");
    $(Lab).addClass(_this.Mythis + "_Lab");
   
    Text = new TVclTextBox(div3Cont, "Text");
    $(Text.This).addClass(_this.Mythis + "_Text");

    var btnAccept = new TVclButton(div4Cont, "Btn");
    btnAccept.Cursor = "pointer";
    btnAccept.Text ="Click here";
    btnAccept.VCLType = TVCLType.BS;
    btnAccept.onClick = function myfunction() {
        alert("click");
    }
    $(btnAccept.This).addClass(_this.Mythis + "_btnAccept");
}
