﻿ItHelpCenter.Demo.TDemoDropDownPanel = function (inObjectHtml, _this) {
    //************* INICIO TAB DROP DOWN PANEL ***********************************///
    var ConDropDownPanel = new TVclStackPanel(inObjectHtml, "1", 1, [[12], [12], [12], [12], [12]]);
    ConDropDownPanel.Row.This.style.marginTop = "20px";

    var imgImage2 = new TVclImagen(ConDropDownPanel.Column[0].This, "");
    imgImage2.Src = "../../../image/24/add.png";
    imgImage2.Title = "esta es una imagen de prueba";

    var lblImagen2 = new TVcllabel(ConDropDownPanel.Column[0].This, "", TlabelType.H0);
    lblImagen2.Text = "Add";
    lblImagen2.VCLType = TVCLType.BS;

    var Memo2 = new TVclMemo(ConDropDownPanel.Column[0].This, "");
    Memo2.Text = "Write a text";    // PROPIEDAD PARA ASIGNAR EL TEXTO
    Memo2.VCLType = TVCLType.BS;    // PROPIEDAD PARA DARLE EL ESTILO DE LA LIBRERIA UTILIZADA (BOOTSTRAP, ETC) 
    Memo2.Rows = 5;                 // PROPIEDAD PARA DARLE EL ALTO AL CONTROL 
    Memo2.Cols = 50;                // PROPIEDAD PARA DARLE EL ANCHO AL CONTROL

    var VclDropDownPanel = new TVclDropDownPanel(ConDropDownPanel.Column[0].This, "");//CREA EL DROP DOWN PANEL
    VclDropDownPanel.BoderTopColor = "green"; //ASIGNA O EXTRAE EL COLOR DEL BORDE SUPERIOR 
    VclDropDownPanel.BoderAllColor = "gray"; //ASIGNA O EXTRAE EL COLOR DE TODO EL BODER 
    VclDropDownPanel.BackgroundColor = "white"; //ASIGNA O EXTRAE EL COLOR DE FONDO DEL DROPDOWNPANEL 
    VclDropDownPanel.MarginBottom = "20px"; //ASIGNA O EXTRAE EL MARGEN INFERIOR DEL DROPDOWNPANEL
    VclDropDownPanel.MarginTop = "20px"; //ASIGNA O EXTRAE EL MARGEN SUPERIOR DEL DROPDOWNPANEL
    VclDropDownPanel.MarginLeft = "20px"; //ASIGNA O EXTRAE EL MARGEN IZQUIERDO DEL DROPDOWNPANEL
    VclDropDownPanel.MarginRight = "20px"; //ASIGNA O EXTRAE EL MARGEN DERECHO DEL DROPDOWNPANEL

    VclDropDownPanel.AddToHeader(imgImage2.This); //ASIGNA EL CONTENIDO HTML AL HEADER DEL DROPDOWNPANEL 
    VclDropDownPanel.AddToHeader(lblImagen2.This);//ASIGNA EL CONTENIDO HTML AL HEADER DEL DROPDOWNPANEL 
    VclDropDownPanel.AddToBody(Memo2.This); //ASIGNA EL CONTENIDO HTML AL BODY DEL DROPDOWNPANEL 
    //************* FIN TAB DROP DOWN PANEL ***********************************///
}