ItHelpCenter.Demo.TDemoScheduler = function (inObjectHtml, id, callback) {
    this.TParent = function () {
        return this;
    }.bind(this);
    this.ObjectHtml = inObjectHtml;
    this.Id = id;
    this.Scheduler = callback;
    this.She = new Object();
    this.Mythis = "TDemoScheduler";
    UsrCfg.Traslate.GetLangText(this.Mythis, "SHOWGROUPDATA", "Show Group Data");
    UsrCfg.Traslate.GetLangText(this.Mythis, "SECTIONID", "Section ID");
    UsrCfg.Traslate.GetLangText(this.Mythis, "SECTIONNAME", "Section Name");
    UsrCfg.Traslate.GetLangText(this.Mythis, "TEXTSECTIONNAME", "Text in section name");
    UsrCfg.Traslate.GetLangText(this.Mythis, "SECTIONDESCRIPTION", "Section Description");
    UsrCfg.Traslate.GetLangText(this.Mythis, "TEXTSECTIONDESCRIPTION", "Text in section description");
    UsrCfg.Traslate.GetLangText(this.Mythis, "SECTIONHTML", "Section HTML");
    UsrCfg.Traslate.GetLangText(this.Mythis, "TEXTSECTIONHTML", "Text in section HTML");
    UsrCfg.Traslate.GetLangText(this.Mythis, "SECTIONTAG", "Section Tag");
    UsrCfg.Traslate.GetLangText(this.Mythis, "TEXTSECTIONTAG", "Text in section tag");
    UsrCfg.Traslate.GetLangText(this.Mythis, "SECTIONEVENTID", "Section Event Id");
    UsrCfg.Traslate.GetLangText(this.Mythis, "EVENTTITLE", "Event Title");
    UsrCfg.Traslate.GetLangText(this.Mythis, "TEXTEVENTTITLE", "Text in event title");
    UsrCfg.Traslate.GetLangText(this.Mythis, "EVENTDESCRIPTION", "Event Description");
    UsrCfg.Traslate.GetLangText(this.Mythis, "TEXTEVENTDESCRIPTION", "Text in event description");
    UsrCfg.Traslate.GetLangText(this.Mythis, "EVENTCOLORTEXT", "Event Color Text");
    UsrCfg.Traslate.GetLangText(this.Mythis, "EVENTBACKGROUND", "Event Background");
    UsrCfg.Traslate.GetLangText(this.Mythis, "EVENTTIMESTART", "Event Time Start");
    UsrCfg.Traslate.GetLangText(this.Mythis, "EVENTTIMEEND", "Event Time End");
    UsrCfg.Traslate.GetLangText(this.Mythis, "EVENTTAG", "Event Tag");
    UsrCfg.Traslate.GetLangText(this.Mythis, "TEXTEVENTTAG", "Text in event tag");
    UsrCfg.Traslate.GetLangText(this.Mythis, "BOTTOMSSECTION", "Bottoms Section");
    UsrCfg.Traslate.GetLangText(this.Mythis, "BOTTOMSEVENT", "Bottoms Event");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ADD", "Add");
    UsrCfg.Traslate.GetLangText(this.Mythis, "GET", "Get");
    UsrCfg.Traslate.GetLangText(this.Mythis, "UPDATE", "Update");
    UsrCfg.Traslate.GetLangText(this.Mythis, "DELETE", "Delete");
    UsrCfg.Traslate.GetLangText(this.Mythis, "LIST", "List");
    UsrCfg.Traslate.GetLangText(this.Mythis, "MOVE", "Move");
    UsrCfg.Traslate.GetLangText(this.Mythis, "BLOCKDAY", "Block Day");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ELEMENTADDEDCORRECTLY", "Element Added Correctly");
    UsrCfg.Traslate.GetLangText(this.Mythis, "EDITELEMENTCORRECTLY", "Edited Element Correctly");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ELIMINATEDELEMENTCORRECTLY", "Eliminated Element Correctly");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ELEMENTCHANGEDCORRECTLY", "Element Changed Correctly");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ENTERACORRECTID", "Enter a Correct ID");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ELEMENTNOTADDED", "Element not Added");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ELEMENTNOTFOUND", "Element not Found");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ELEMENTNOTEDITED", "Element not edited");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ELEMENTNOTELIMINATED", "Element Not Eliminated");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ELEMENTMOVEDCORRECTLY", "Element Moved Correctly");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ELEMENTNOTMOVED", "Element not moved");
    UsrCfg.Traslate.GetLangText(this.Mythis, "EVENT", "Event");
    this.InitializeDesigner();
    this.InitializeComponent();
}
ItHelpCenter.Demo.TDemoScheduler.prototype.InitializeDesigner = function () {
    var _this = this.TParent();
    try {
        var boxbtnGroupData = new TVclStackPanel(_this.ObjectHtml, "boxbtnGroupData", 1, [[12], [12], [12], [12], [12]]);
        $(boxbtnGroupData.Column[0].This)[0].className += " text-center";
        boxbtnGroupData.Column[0].This.style.marginBottom = "5px";
        var btnGroupData = new TVclInputbutton(boxbtnGroupData.Column[0].This, "btnGroupData");
        btnGroupData.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "SHOWGROUPDATA");
        btnGroupData.VCLType = TVCLType.BS;
        _this.boxInit = new TVclStackPanel(_this.ObjectHtml, "boxInit", 1, [[12], [12], [12], [12], [12]]);
        btnGroupData.onClick = function myfunction() {
            $(_this.boxInit.Column[0].This).toggle("slow");
        }
        $(_this.boxInit.Column[0].This).toggle("slow");
        //************ INICIO DE SECTION ID ***********************************************
        var ConTextControl0 = new TVclStackPanel(_this.boxInit.Column[0].This, "boxId", 2, [[12, 12], [12, 12], [2, 10], [2, 10], [2, 10]]);
        ConTextControl0.Row.This.style.marginTop = "20px";

        var lblLabel0 = new TVcllabel(ConTextControl0.Column[0].This, "", TlabelType.H0);
        lblLabel0.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "SECTIONID");
        lblLabel0.VCLType = TVCLType.BS;

        _this.TxtId = new TVclTextBox(ConTextControl0.Column[1].This, "txtId");
        _this.TxtId.Text = "1"
        _this.TxtId.This.type = "number"
        _this.TxtId.This.setAttribute("min", "1");
        _this.TxtId.VCLType = TVCLType.BS;
        //************ FIN DE SECTION ID ***********************************************
        //************ INICIO DE SECTION NAME ***********************************************
        var ConTextControlSectionName = new TVclStackPanel(_this.boxInit.Column[0].This, "boxSectionName", 2, [[12, 12], [12, 12], [2, 10], [2, 10], [2, 10]]);
        ConTextControlSectionName.Row.This.style.marginTop = "20px";

        var lblLabelSectionName = new TVcllabel(ConTextControlSectionName.Column[0].This, "", TlabelType.H0);
        lblLabelSectionName.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "SECTIONNAME");
        lblLabelSectionName.VCLType = TVCLType.BS;

        _this.TxtSectionName = new TVclTextBox(ConTextControlSectionName.Column[1].This, "txtSectionName");
        _this.TxtSectionName.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TEXTSECTIONNAME");
        _this.TxtSectionName.VCLType = TVCLType.BS;
        //************ FIN DE SECTION NAME ***********************************************
        //************ INICIO DE SECTION DESCRIPTION ***********************************************
        var ConTextControlSectionDescription = new TVclStackPanel(_this.boxInit.Column[0].This, "boxSectionDescription", 2, [[12, 12], [12, 12], [2, 10], [2, 10], [2, 10]]);
        ConTextControlSectionDescription.Row.This.style.marginTop = "20px";

        var lblLabelSectionDescription = new TVcllabel(ConTextControlSectionDescription.Column[0].This, "", TlabelType.H0);
        lblLabelSectionDescription.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "SECTIONDESCRIPTION");
        lblLabelSectionDescription.VCLType = TVCLType.BS;

        _this.TxtSectionDescription = new TVclTextBox(ConTextControlSectionDescription.Column[1].This, "txtSectionDescription");
        _this.TxtSectionDescription.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TEXTSECTIONDESCRIPTION");
        _this.TxtSectionDescription.VCLType = TVCLType.BS;
        //************ FIN DE SECTION DESCRIPTION ***********************************************
        //************ INICIO DE SECTION HTNL ***********************************************
        var ConTextControlSectionHtml = new TVclStackPanel(_this.boxInit.Column[0].This, "boxSectionHtml", 2, [[12, 12], [12, 12], [2, 10], [2, 10], [2, 10]]);
        ConTextControlSectionHtml.Row.This.style.marginTop = "20px";

        var lblLabelSectionHtml = new TVcllabel(ConTextControlSectionHtml.Column[0].This, "", TlabelType.H0);
        lblLabelSectionHtml.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "SECTIONHTML");
        lblLabelSectionHtml.VCLType = TVCLType.BS;

        _this.TxtSectionHtml = new TVclTextBox(ConTextControlSectionHtml.Column[1].This, "txtSectionHtml");
        _this.TxtSectionHtml.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TEXTSECTIONHTML");
        _this.TxtSectionHtml.VCLType = TVCLType.BS;
        //************ FIN DE SECTION HTML ***********************************************
        //************ INICIO DE SECTION TAG ***********************************************
        var ConTextControlSectionTag = new TVclStackPanel(_this.boxInit.Column[0].This, "boxSectionTag", 2, [[12, 12], [12, 12], [2, 10], [2, 10], [2, 10]]);
        ConTextControlSectionTag.Row.This.style.marginTop = "20px";

        var lblLabelSectionTag = new TVcllabel(ConTextControlSectionTag.Column[0].This, "", TlabelType.H0);
        lblLabelSectionTag.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "SECTIONTAG");
        lblLabelSectionTag.VCLType = TVCLType.BS;

        _this.TxtSectionTag = new TVclTextBox(ConTextControlSectionTag.Column[1].This, "txtSectionTag");
        _this.TxtSectionTag.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TEXTSECTIONTAG");
        _this.TxtSectionTag.VCLType = TVCLType.BS;
        //************ FIN DE SECTION TAG ***********************************************
        //************ INICIO DE EVENT ID ***********************************************
        var ConTextControlEventId = new TVclStackPanel(_this.boxInit.Column[0].This, "boxEventId", 2, [[12, 12], [12, 12], [2, 10], [2, 10], [2, 10]]);
        ConTextControlEventId.Row.This.style.marginTop = "20px";

        var lblLabelEventId = new TVcllabel(ConTextControlEventId.Column[0].This, "", TlabelType.H0);
        lblLabelEventId.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "SECTIONEVENTID");
        lblLabelEventId.VCLType = TVCLType.BS;

        _this.TxtEventId = new TVclTextBox(ConTextControlEventId.Column[1].This, "txtEventId");
        _this.TxtEventId.Text = "1"
        _this.TxtEventId.This.type = "number"
        _this.TxtEventId.This.setAttribute("min", "1");
        _this.TxtEventId.VCLType = TVCLType.BS;
        //************ FIN DE EVENT ID ***********************************************
        //************ INICIO EVENT TITLE ***********************************************
        var ConTextControlEventTitle = new TVclStackPanel(_this.boxInit.Column[0].This, "boxEventTitle", 2, [[12, 12], [12, 12], [2, 10], [2, 10], [2, 10]]);
        ConTextControlEventTitle.Row.This.style.marginTop = "20px";

        var lblLabelEventTitle = new TVcllabel(ConTextControlEventTitle.Column[0].This, "", TlabelType.H0);
        lblLabelEventTitle.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "EVENTTITLE");
        lblLabelEventTitle.VCLType = TVCLType.BS;

        _this.TxtEventTitle = new TVclTextBox(ConTextControlEventTitle.Column[1].This, "txtEventTitle");
        _this.TxtEventTitle.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TEXTEVENTTITLE");
        _this.TxtEventTitle.VCLType = TVCLType.BS;
        //************ FIN DE EVENT TITLE ***********************************************
        //************ INICIO DE EVENT DESCRIPTION ***********************************************
        var ConTextControlEventDescription = new TVclStackPanel(_this.boxInit.Column[0].This, "boxEventDescription", 2, [[12, 12], [12, 12], [2, 10], [2, 10], [2, 10]]);
        ConTextControlEventDescription.Row.This.style.marginTop = "20px";

        var lblLabelEventDescription = new TVcllabel(ConTextControlEventDescription.Column[0].This, "", TlabelType.H0);
        lblLabelEventDescription.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "EVENTDESCRIPTION");
        lblLabelEventDescription.VCLType = TVCLType.BS;

        _this.TxtEventDescription = new TVclTextBox(ConTextControlEventDescription.Column[1].This, "txtEventDescription");
        _this.TxtEventDescription.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TEXTEVENTDESCRIPTION");
        _this.TxtEventDescription.VCLType = TVCLType.BS;
        //************ FIN DE EVENT DESCRIPTION ***********************************************
        //************ INICIO DE EVENT COLOR TEXT ***********************************************
        var ConTextControlEventColorText = new TVclStackPanel(_this.boxInit.Column[0].This, "boxEventColorText", 2, [[12, 12], [12, 12], [2, 10], [2, 10], [2, 10]]);
        ConTextControlEventColorText.Row.This.style.marginTop = "20px";

        var lblLabelEventColorText = new TVcllabel(ConTextControlEventColorText.Column[0].This, "", TlabelType.H0);
        lblLabelEventColorText.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "EVENTCOLORTEXT");
        lblLabelEventColorText.VCLType = TVCLType.BS;

        _this.objEventColorText = new Componet.TPalleteColor(ConTextControlEventColorText.Column[1].This, "colorEventColorText");
        _this.objEventColorText.CreatePallete();
        //************ FIN DE EVENT COLOR TEXT ***********************************************
        //************ INICIO DE EVENT BACKGROUND ***********************************************
        var ConTextControlEventBackground = new TVclStackPanel(_this.boxInit.Column[0].This, "boxEventBackground", 2, [[12, 12], [12, 12], [2, 10], [2, 10], [2, 10]]);
        ConTextControlEventBackground.Row.This.style.marginTop = "20px";

        var lblLabelEventBackground = new TVcllabel(ConTextControlEventBackground.Column[0].This, "", TlabelType.H0);
        lblLabelEventBackground.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "EVENTBACKGROUND");
        lblLabelEventBackground.VCLType = TVCLType.BS;

        _this.objEventBackground = new Componet.TPalleteColor(ConTextControlEventBackground.Column[1].This, "colorEventBackground");
        _this.objEventBackground.CreatePallete();
        //************ FIN DE EVENT BACKGROUND ***********************************************
        //************ INICIO DE EVENT TIME START ***********************************************
        var ConTextControlEventTimeStart = new TVclStackPanel(_this.boxInit.Column[0].This, "boxEventTimeStart", 2, [[12, 12], [12, 12], [2, 10], [2, 10], [2, 10]]);
        ConTextControlEventTimeStart.Row.This.style.marginTop = "20px";

        var lblLabelEventTimeStart = new TVcllabel(ConTextControlEventTimeStart.Column[0].This, "", TlabelType.H0);
        lblLabelEventTimeStart.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "EVENTTIMESTART");
        lblLabelEventTimeStart.VCLType = TVCLType.BS;

        _this.TxtEventTimeStart = new TVclTextBox(ConTextControlEventTimeStart.Column[1].This, "txtEventTimeStart");
        _this.TxtEventTimeStart.Text = "2018/07/01 05:00:00";
        _this.TxtEventTimeStart.VCLType = TVCLType.BS;
        //************ FIN DE EVENT TIME START ***********************************************
        //************ INICIO DE EVENT TIME END ***********************************************
        var ConTextControlEventTimeEnd = new TVclStackPanel(_this.boxInit.Column[0].This, "boxEventTimeEnd", 2, [[12, 12], [12, 12], [2, 10], [2, 10], [2, 10]]);
        ConTextControlEventTimeEnd.Row.This.style.marginTop = "20px";

        var lblLabelEventTimeEnd = new TVcllabel(ConTextControlEventTimeEnd.Column[0].This, "", TlabelType.H0);
        lblLabelEventTimeEnd.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "EVENTTIMEEND");
        lblLabelEventTimeEnd.VCLType = TVCLType.BS;

        _this.TxtEventTimeEnd = new TVclTextBox(ConTextControlEventTimeEnd.Column[1].This, "txtEventTimeEnd");
        _this.TxtEventTimeEnd.Text = "2018/07/01 08:00:00";
        _this.TxtEventTimeEnd.VCLType = TVCLType.BS;
        //************ FIN DE EVENT TIME END ***********************************************
        //************ INICIO DE EVENT TAG ***********************************************
        var ConTextControlEventTag = new TVclStackPanel(_this.boxInit.Column[0].This, "boxEventTag", 2, [[12, 12], [12, 12], [2, 10], [2, 10], [2, 10]]);
        ConTextControlEventTag.Row.This.style.marginTop = "20px";

        var lblLabelEventTag = new TVcllabel(ConTextControlEventTag.Column[0].This, "", TlabelType.H0);
        lblLabelEventTag.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "EVENTTAG");
        lblLabelEventTag.VCLType = TVCLType.BS;

        _this.TxtEventTag = new TVclTextBox(ConTextControlEventTag.Column[1].This, "txtEventTag");
        _this.TxtEventTag.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TEXTEVENTTAG");
        _this.TxtEventTag.VCLType = TVCLType.BS;
        //************ FIN DE EVENT TAG ***********************************************
        //************ INICIO BOTTOMS SECTION ***********************************************
        var ConTextControlBottomsSection = new TVclStackPanel(_this.boxInit.Column[0].This, "BottomsSection", 2, [[12, 12], [12, 12], [2, 10], [2, 10], [2, 10]]);
        ConTextControlBottomsSection.Row.This.style.marginTop = "20px";

        var lblBottomsSection = new TVcllabel(ConTextControlBottomsSection.Column[0].This, "", TlabelType.H0);
        lblBottomsSection.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "BOTTOMSSECTION");
        lblBottomsSection.VCLType = TVCLType.BS;

        var buttonAddSection = new TVclInputbutton(ConTextControlBottomsSection.Column[1].This, "buttonAddSection");
        buttonAddSection.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ADD");
        buttonAddSection.VCLType = TVCLType.BS;
        buttonAddSection.onClick = function () {
            try {
                _this.AddSection(_this, buttonAddSection);
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("DemoScheduler.js ItHelpCenter.Demo.TDemoScheduler.prototype.InitializeDesigner buttonAddSection.onClick", e);
            }
        }
        buttonAddSection.This.style.marginTop = "5px";
        var buttonGetSection = new TVclInputbutton(ConTextControlBottomsSection.Column[1].This, "buttonGetSection");
        buttonGetSection.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "GET");
        buttonGetSection.VCLType = TVCLType.BS;
        buttonGetSection.onClick = function () {
            try {
                _this.GetSection(_this, buttonGetSection);
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("DemoScheduler.js ItHelpCenter.Demo.TDemoScheduler.prototype.InitializeDesigner buttonGetSection.onClick", e);
            }
        }
        buttonGetSection.This.style.marginLeft = "5px";
        buttonGetSection.This.style.marginTop = "5px";
        var buttonUpdateSection = new TVclInputbutton(ConTextControlBottomsSection.Column[1].This, "buttonUpdateSection");
        buttonUpdateSection.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "UPDATE");
        buttonUpdateSection.VCLType = TVCLType.BS;
        buttonUpdateSection.onClick = function () {
            try {
                _this.UpdateSection(_this, buttonUpdateSection);
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("DemoScheduler.js ItHelpCenter.Demo.TDemoScheduler.prototype.InitializeDesigner buttonUpdateSection.onClick", e);
            }
        }
        buttonUpdateSection.This.style.marginLeft = "5px";
        buttonUpdateSection.This.style.marginTop = "5px";
        var buttonDeleteSection = new TVclInputbutton(ConTextControlBottomsSection.Column[1].This, "buttonDeleteSection");
        buttonDeleteSection.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "DELETE");
        buttonDeleteSection.VCLType = TVCLType.BS;
        buttonDeleteSection.onClick = function () {
            try {
                _this.DeleteSection(_this, buttonDeleteSection);
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("DemoScheduler.js ItHelpCenter.Demo.TDemoScheduler.prototype.InitializeDesigner buttonDeleteSection.onClick", e);
            }
        }
        buttonDeleteSection.This.style.marginLeft = "5px";
        buttonDeleteSection.This.style.marginTop = "5px";
        var buttonListSection = new TVclInputbutton(ConTextControlBottomsSection.Column[1].This, "buttonListSection");
        buttonListSection.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "LIST");
        buttonListSection.VCLType = TVCLType.BS;
        buttonListSection.onClick = function () {
            try {
                _this.ListSection(_this, buttonListSection);
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("DemoScheduler.js ItHelpCenter.Demo.TDemoScheduler.prototype.InitializeDesigner buttonListSection.onClick", e);
            }
        }
        buttonListSection.This.style.marginLeft = "5px";
        buttonListSection.This.style.marginTop = "5px";
        //************ FIN DE BOTTOMS SECTION  ***********************************************
        //************ INICIO BOTTOMS EVENT ***********************************************
        var ConTextControlBottomsEvent = new TVclStackPanel(_this.boxInit.Column[0].This, "BottomsEvent", 2, [[12, 12], [12, 12], [2, 10], [2, 10], [2, 10]]);
        ConTextControlBottomsEvent.Row.This.style.marginTop = "20px";
        ConTextControlBottomsEvent.Row.This.style.marginBottom = "20px";

        var lblBottomsEvent = new TVcllabel(ConTextControlBottomsEvent.Column[0].This, "", TlabelType.H0);
        lblBottomsEvent.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "BOTTOMSEVENT");
        lblBottomsEvent.VCLType = TVCLType.BS;

        var buttonAddEvent = new TVclInputbutton(ConTextControlBottomsEvent.Column[1].This, "buttonAddEvent");
        buttonAddEvent.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ADD");
        buttonAddEvent.VCLType = TVCLType.BS;
        buttonAddEvent.onClick = function () {
            try {
                _this.AddEvent(_this, buttonAddEvent);
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("DemoScheduler.js ItHelpCenter.Demo.TDemoScheduler.prototype.InitializeDesigner buttonAddEvent.onClick", e);
            }
        }
        buttonAddEvent.This.style.marginTop = "5px";
        var buttonGetEvent = new TVclInputbutton(ConTextControlBottomsEvent.Column[1].This, "buttonGetEvent");
        buttonGetEvent.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "GET");
        buttonGetEvent.VCLType = TVCLType.BS;
        buttonGetEvent.onClick = function () {
            try {
                _this.GetEvent(_this, buttonGetEvent);
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("DemoScheduler.js ItHelpCenter.Demo.TDemoScheduler.prototype.InitializeDesigner buttonGetEvent.onClick", e);
            }
        }
        buttonGetEvent.This.style.marginLeft = "5px";
        buttonGetEvent.This.style.marginTop = "5px";
        var buttonUpdateEvent = new TVclInputbutton(ConTextControlBottomsEvent.Column[1].This, "buttonUpdateEvent");
        buttonUpdateEvent.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "UPDATE");
        buttonUpdateEvent.VCLType = TVCLType.BS;
        buttonUpdateEvent.onClick = function () {
            try {
                _this.UpdateEvent(_this, buttonUpdateEvent);
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("DemoScheduler.js ItHelpCenter.Demo.TDemoScheduler.prototype.InitializeDesigner buttonUpdateEvent.onClick", e);
            }
        }
        buttonUpdateEvent.This.style.marginLeft = "5px";
        buttonUpdateEvent.This.style.marginTop = "5px";
        var buttonDeleteEvent = new TVclInputbutton(ConTextControlBottomsEvent.Column[1].This, "buttonDeleteEvent");
        buttonDeleteEvent.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "DELETE");
        buttonDeleteEvent.VCLType = TVCLType.BS;
        buttonDeleteEvent.onClick = function () {
            try {
                _this.DeleteEvent(_this, buttonDeleteEvent);
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("DemoScheduler.js ItHelpCenter.Demo.TDemoScheduler.prototype.InitializeDesigner buttonDeleteEvent.onClick", e);
            }
        }
        buttonDeleteEvent.This.style.marginLeft = "5px";
        buttonDeleteEvent.This.style.marginTop = "5px";
        var buttonListEvent = new TVclInputbutton(ConTextControlBottomsEvent.Column[1].This, "buttonListEvent");
        buttonListEvent.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "LIST");
        buttonListEvent.VCLType = TVCLType.BS;
        buttonListEvent.onClick = function () {
            try {
                _this.ListEvent(_this, buttonListEvent);
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("DemoScheduler.js ItHelpCenter.Demo.TDemoScheduler.prototype.InitializeDesigner buttonListEvent.onClick", e);
            }
        }
        buttonListEvent.This.style.marginLeft = "5px";
        buttonListEvent.This.style.marginTop = "5px";
        var buttonMoveEvent = new TVclInputbutton(ConTextControlBottomsEvent.Column[1].This, "buttonMoveEvent");
        buttonMoveEvent.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "MOVE");
        buttonMoveEvent.VCLType = TVCLType.BS;
        buttonMoveEvent.onClick = function () {
            try {
                _this.MoveEvent(_this, buttonMoveEvent);
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("DemoScheduler.js ItHelpCenter.Demo.TDemoScheduler.prototype.InitializeDesigner buttonMoveEvent.onClick", e);
            }
        }
        buttonMoveEvent.This.style.marginLeft = "5px";
        buttonMoveEvent.This.style.marginTop = "5px";
        var buttonBlockDayEvent = new TVclInputbutton(ConTextControlBottomsEvent.Column[1].This, "buttonBlockDayEvent");
        buttonBlockDayEvent.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "BLOCKDAY");
        buttonBlockDayEvent.VCLType = TVCLType.BS;
        buttonBlockDayEvent.onClick = function () {
            try {
                _this.BlockDayEvent(_this, buttonBlockDayEvent);
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("DemoScheduler.js ItHelpCenter.Demo.TDemoScheduler.prototype.InitializeDesigner buttonBlockDayEvent.onClick", e);
            }
        }
        buttonBlockDayEvent.This.style.marginLeft = "5px";
        buttonBlockDayEvent.This.style.marginTop = "5px";
        //************ FIN DE BOTTOMS EVENT ***********************************************
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoScheduler.js ItHelpCenter.Demo.TDemoScheduler.prototype.InitializeDesigner", e);
    }
}
ItHelpCenter.Demo.TDemoScheduler.prototype.InitializeComponent = function () {
    var _this = this.TParent();
    try {
        //FIX
        var fix = false;
        if ($("#Cont2_idmain_0").hasClass("burger_visible") && Source.Menu.IsMobil) {
            $("#Cont2_idmain_0").removeClass('burger_visible');
            fix = true;
        }
        //FIX
        var scheduler1 = new Componet.Scheduler.TScheduler(_this.ObjectHtml, "Scheduler1", null);
        scheduler1.Config.FormatDate = "%Y/%m/%d %H:%i";
        scheduler1.Config.UpdateRender = true;
        scheduler1.CreateUnitsSectionsDefault();
        scheduler1.MarkDateYearInit = "2018/01/01 00:00:00";
        scheduler1.MarkDateYearFinish = "2018/07/25 00:00:00";
        _this.Scheduler = scheduler1;
        scheduler1.BtnSaveClick = function (objInit, NewSectionOption, e) {
            //object actual para insertar Elemento Section
            //object principal del plugin objInit
            //Btn que genera la acci�n objInit
            NewSectionOption.Id = Math.floor((Math.random() * 100) + 1);
            NewSectionOption.Name = _this.Scheduler.TextSave.Text;
            NewSectionOption.Description = "Description " + NewSectionOption.Id;
            NewSectionOption.Tag = "Tag " + NewSectionOption.Id;
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELEMENTADDEDCORRECTLY"));
            return true;
        }
        scheduler1.BtnEditClick = function (objInit, Section, e) {
            //object actual a editar Section
            //object principal del plugin objInit
            //Btn que genera la acci�n objInit
            Section.Id = Section.Id;
            Section.Name = _this.Scheduler.TextSave.Text;
            Section.Description = "Description " + Section.Id;
            Section.Tag = "Tag " + Section.Id;
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "EDITELEMENTCORRECTLY"));
            return true;
        }
        scheduler1.BtnDeleteClick = function (objInit, Section, e) {
            //object actual a eliminar Section
            //object principal del plugin objInit
            //Btn que genera la acci�n objInit
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELIMINATEDELEMENTCORRECTLY"));
            return true;
        }
        scheduler1.Events.OnEventChanged = function (_this, oldEvent, event) { // evento que se genera cuando se cambia un evento en el editor
            //elemento antes de que se actualize oldEvent
            //object principal del plugin _this
            //Btn que genera la acci�n event
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "EDITELEMENTCORRECTLY"));
            return true;
        }
        scheduler1.Events.OnEventAdd = function (ObjectInit, eventCreate) {
            //elemento que se creo cuando se agrego el evento en el editor eventCreate
            //object principal del plugin ObjectInit
            eventCreate.Id = Math.floor((Math.random() * 100) + 1);
            eventCreate.IdSectionOption = eventCreate.IdSectionOption;
            eventCreate.Title = "Event " + eventCreate.Id;
            eventCreate.Description = eventCreate.Description;
            eventCreate.ColorText = "white";
            eventCreate.Background = "orange";
            eventCreate.TimeStart = eventCreate.TimeStart;
            eventCreate.TimeEnd = eventCreate.TimeEnd;
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELEMENTADDEDCORRECTLY"));
            return true;
        }
        scheduler1.Events.OnEventDelete = function (ObjectInit, eventDelete) {
            //object principal del plugin ObjectInit
            //object actual a eliminar eventDelete
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELIMINATEDELEMENTCORRECTLY"));
            return true;
        }
        //FIX
        if (fix) {
            $("#Cont2_idmain_0").addClass('burger_visible');
        }
        //FIX

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoScheduler.js ItHelpCenter.Demo.TDemoScheduler.prototype.InitializeComponent", e);
    }
}
ItHelpCenter.Demo.TDemoScheduler.prototype.AddSection = function () {
    var _this = this.TParent();
    try {
        if (!isNaN(_this.TxtId.Text)) {
            var listObject = new Array();
            var SectionOptions = new _this.Scheduler.SectionOptions();
            SectionOptions.Id = parseInt(_this.TxtId.Text);
            SectionOptions.Name = _this.TxtSectionName.Text;
            SectionOptions.Description = _this.TxtSectionDescription.Text;
            SectionOptions.Html = _this.TxtSectionHtml.Text;
            SectionOptions.Tag = _this.TxtSectionTag.Text;
            if (!isNaN(_this.TxtEventId.Text)) {
                var objectEventNew = new _this.Scheduler.Event();
                objectEventNew.Id = parseInt(_this.TxtEventId.Text);
                objectEventNew.IdSectionOption = SectionOptions.Id;
                objectEventNew.Title = _this.TxtEventTitle.Text;
                objectEventNew.Description = _this.TxtEventDescription.Text;
                objectEventNew.ColorText = _this.objEventColorText.Color;
                objectEventNew.Background = _this.objEventBackground.Color;
                objectEventNew.TimeStart = _this.TxtEventTimeStart.Text;
                objectEventNew.TimeEnd = _this.TxtEventTimeEnd.Text;
                objectEventNew.Tag = _this.TxtEventTag.Text;
                SectionOptions.Events.push(objectEventNew);
            }
            listObject.push(SectionOptions);
            var success = _this.Scheduler.SaveSection(listObject);
            if (success)
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELEMENTADDEDCORRECTLY"));
            else
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELEMENTNOTADDED"));
        }
        else {
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ENTERACORRECTID"));
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoScheduler.js ItHelpCenter.Demo.TDemoScheduler.prototype.AddSection", e);
    }
}
ItHelpCenter.Demo.TDemoScheduler.prototype.GetSection = function () {
    var _this = this.TParent();
    try {
        if (!isNaN(_this.TxtId.Text)) {
            var getObj = _this.Scheduler.GetSectionChild(parseInt(_this.TxtId.Text));
            if (getObj) {
                _this.TxtId.Text = getObj.Id;
                _this.TxtSectionName.Text = getObj.Name;
                _this.TxtSectionDescription.Text = getObj.Description;
                _this.TxtSectionHtml.Text = getObj.Html;
                _this.TxtSectionTag.Text = getObj.Tag;
                _this.TxtEventId.Text = getObj.Events[0].Id;
                _this.TxtEventTitle.Text = getObj.Events[0].Title;
                _this.TxtEventDescription.Text = getObj.Events[0].Description;
                _this.objEventColorText.Color = getObj.Events[0].ColorText;
                _this.objEventBackground.Color = getObj.Events[0].Background;
                _this.TxtEventTimeStart.Text = getObj.Events[0].TimeStart;
                _this.TxtEventTimeEnd.Text = getObj.Events[0].TimeEnd;
                _this.TxtEventTag.Text = getObj.Events[0].Tag;
            }
            else {
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELEMENTNOTFOUND"));
            }
        }
        else {
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ENTERACORRECTID"));
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoScheduler.js ItHelpCenter.Demo.TDemoScheduler.prototype.GetSection", e);
    }
}
ItHelpCenter.Demo.TDemoScheduler.prototype.UpdateSection = function () {
    var _this = this.TParent();
    try {
        if (!isNaN(_this.TxtId.Text)) {
            var listObject = new Array();
            var SectionOptions = new _this.Scheduler.SectionOptions();
            SectionOptions.Id = parseInt(_this.TxtId.Text);
            SectionOptions.Name = _this.TxtSectionName.Text;
            SectionOptions.Description = _this.TxtSectionDescription.Text;
            SectionOptions.Html = _this.TxtSectionHtml.Text;
            SectionOptions.Tag = _this.TxtSectionTag.Text;
            if (!isNaN(_this.TxtEventId.Text)) {
                var objectEventNew = new _this.Scheduler.Event();
                objectEventNew.Id = parseInt(_this.TxtEventId.Text);
                objectEventNew.IdSectionOption = SectionOptions.Id;
                objectEventNew.Title = _this.TxtEventTitle.Text;
                objectEventNew.Description = _this.TxtEventDescription.Text;
                objectEventNew.ColorText = _this.objEventColorText.Color;
                objectEventNew.Background = _this.objEventBackground.Color;
                objectEventNew.TimeStart = _this.TxtEventTimeStart.Text;
                objectEventNew.TimeEnd = _this.TxtEventTimeEnd.Text;
                objectEventNew.Tag = _this.TxtEventTag;
                SectionOptions.Events.push(objectEventNew);
            }
            var success = _this.Scheduler.EditSection(SectionOptions, SectionOptions.Id);
            if (success)
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "EDITELEMENTCORRECTLY"));
            else
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELEMENTNOTEDITED"));
        }
        else {
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ENTERACORRECTID"));
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoScheduler.js ItHelpCenter.Demo.TDemoScheduler.prototype.UpdateSection", e);
    }
}
ItHelpCenter.Demo.TDemoScheduler.prototype.DeleteSection = function () {
    var _this = this.TParent();
    try {
        if (!isNaN(_this.TxtId.Text)) {
            var success = _this.Scheduler.DeleteSections([parseInt(_this.TxtId.Text)]);
            if (success)
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELIMINATEDELEMENTCORRECTLY"));
            else
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELEMENTNOTELIMINATED"));
        }
        else {
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ENTERACORRECTID"));
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoScheduler.js ItHelpCenter.Demo.TDemoScheduler.prototype.DeleteSection", e);
    }
}
ItHelpCenter.Demo.TDemoScheduler.prototype.ListSection = function () {
    var _this = this.TParent();
    try {
        result = ""
        for (var i = 0; i < _this.Scheduler.SectionChild.length; i++) {
            result +=
                ("-----------SECTION---------------------" +
                    "Id Section: " + _this.Scheduler.SectionChild[i].Id + "\n" +
                    "Section Name: " + _this.Scheduler.SectionChild[i].Name + "\n" +
                    "Section Description: " + _this.Scheduler.SectionChild[i].Description + "\n" +
                    "Section Html: " + _this.Scheduler.SectionChild[i].Html + "\n" +
                    "Section Tag: " + _this.Scheduler.SectionChild[i].Tag + "\n" +
                    "-----------EVENT---------------------" +
                    "Event Id: " + _this.Scheduler.SectionChild[i].Events[0].Id + "\n" +
                    "Event Section Id: " + _this.Scheduler.SectionChild[i].Events[0].IdSectionOption + "\n" +
                    "Event Title: " + _this.Scheduler.SectionChild[i].Events[0].Title + "\n" +
                    "Event Description: " + _this.Scheduler.SectionChild[i].Events[0].Description + "\n" +
                    "Event Color Text: " + _this.Scheduler.SectionChild[i].Events[0].ColorText + "\n" +
                    "Event Background: " + _this.Scheduler.SectionChild[i].Events[0].Background + "\n" +
                    "Event Time Start: " + _this.Scheduler.SectionChild[i].Events[0].TimeStart + "\n" +
                    "Event Time End: " + _this.Scheduler.SectionChild[i].Events[0].TimeEnd + "\n" +
                    "Event Tag: " + _this.Scheduler.SectionChild[i].Events[0].Tag + "\n"
                )
        }
        alert(result);

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoScheduler.js ItHelpCenter.Demo.TDemoScheduler.prototype.ListSection", e);
    }
}
ItHelpCenter.Demo.TDemoScheduler.prototype.AddEvent = function () {
    var _this = this.TParent();
    try {
        if (!isNaN(_this.TxtEventId.Text) && !isNaN(_this.TxtId.Text)) {
            var objectEventNew = new _this.Scheduler.Event();
            objectEventNew.Id = parseInt(_this.TxtEventId.Text);
            objectEventNew.IdSectionOption = parseInt(_this.TxtId.Text);
            objectEventNew.Title = _this.TxtEventTitle.Text;
            objectEventNew.Description = _this.TxtEventDescription.Text;
            objectEventNew.ColorText = _this.objEventColorText.Color;
            objectEventNew.Background = _this.objEventBackground.Color;
            objectEventNew.TimeStart = _this.TxtEventTimeStart.Text;
            objectEventNew.TimeEnd = _this.TxtEventTimeEnd.Text;
            objectEventNew.Tag = _this.TxtEventTag.Text;
            var success = _this.Scheduler.SaveEvents(new Array(objectEventNew));
            if (success)
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELEMENTADDEDCORRECTLY"));
            else
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELEMENTNOTADDED"));
        }
        else {
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ENTERACORRECTID"));
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoScheduler.js ItHelpCenter.Demo.TDemoScheduler.prototype.AddEvent", e);
    }
}
ItHelpCenter.Demo.TDemoScheduler.prototype.GetEvent = function () {
    var _this = this.TParent();
    try {
        if (!isNaN(_this.TxtEventId.Text)) {
            var getObj = _this.Scheduler.GetSectionEvent(parseInt(_this.TxtEventId.Text));
            if (getObj) {
                _this.TxtEventId.Text = getObj.Id;
                _this.TxtEventTitle.Text = getObj.Title;
                _this.TxtEventDescription.Text = getObj.Description;
                _this.objEventColorText.Color = getObj.ColorText;
                _this.objEventBackground.Color = getObj.Background;
                _this.TxtEventTimeStart.Text = getObj.TimeStart;
                _this.TxtEventTimeEnd.Text = getObj.TimeEnd;
                _this.TxtEventTag.Text = getObj.Tag;
            }
            else {
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELEMENTNOTFOUND"));
            }
        }
        else {
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ENTERACORRECTID"));
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoScheduler.js ItHelpCenter.Demo.TDemoScheduler.prototype.GetEvent", e);
    }
}
ItHelpCenter.Demo.TDemoScheduler.prototype.UpdateEvent = function () {
    var _this = this.TParent();
    try {
        if (!isNaN(_this.TxtEventId.Text)) {
            var objectEventNew = new _this.Scheduler.Event();
            objectEventNew.Id = parseInt(_this.TxtEventId.Text);
            objectEventNew.IdSectionOption = parseInt(_this.TxtId.Text);
            objectEventNew.Title = _this.TxtEventTitle.Text;
            objectEventNew.Description = _this.TxtEventDescription.Text;
            objectEventNew.ColorText = _this.objEventColorText.Color;
            objectEventNew.Background = _this.objEventBackground.Color;
            objectEventNew.TimeStart = _this.TxtEventTimeStart.Text;
            objectEventNew.TimeEnd = _this.TxtEventTimeEnd.Text;
            objectEventNew.Tag = _this.TxtEventTag.Text;
            var success = _this.Scheduler.EditEvents(objectEventNew, objectEventNew.Id);
            if (success)
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "EDITELEMENTCORRECTLY"));
            else
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELEMENTNOTEDITED"));
        }
        else {
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ENTERACORRECTID"));
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoScheduler.js ItHelpCenter.Demo.TDemoScheduler.prototype.UpdateEvent", e);
    }
}
ItHelpCenter.Demo.TDemoScheduler.prototype.DeleteEvent = function () {
    var _this = this.TParent();
    try {
        if (!isNaN(_this.TxtEventId.Text)) {
            var success = _this.Scheduler.DeleteEvents([parseInt(_this.TxtEventId.Text)]);
            if (success)
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELIMINATEDELEMENTCORRECTLY"));
            else
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELEMENTNOTELIMINATED"));
        }
        else {
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ENTERACORRECTID"));
        }

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoScheduler.js ItHelpCenter.Demo.TDemoScheduler.prototype.DeleteEvent", e);
    }
}
ItHelpCenter.Demo.TDemoScheduler.prototype.ListEvent = function () {
    var _this = this.TParent();
    try {
        result = ""
        for (var i = 0; i < _this.Scheduler.SectionEvents.length; i++) {
            result +=
                (
                    "-----------EVENT---------------------" + "\n" +
                    "Event Id: " + _this.Scheduler.SectionEvents[i].Id + "\n" +
                    "Event Section Id: " + _this.Scheduler.SectionEvents[i].IdSectionOption + "\n" +
                    "Event Title: " + _this.Scheduler.SectionEvents[i].Title + "\n" +
                    "Event Description: " + _this.Scheduler.SectionEvents[i].Description + "\n" +
                    "Event Color Text: " + _this.Scheduler.SectionEvents[i].ColorText + "\n" +
                    "Event Background: " + _this.Scheduler.SectionEvents[i].Background + "\n" +
                    "Event Time Start: " + _this.Scheduler.SectionEvents[i].TimeStart + "\n" +
                    "Event Time End: " + _this.Scheduler.SectionEvents[i].TimeEnd + "\n" +
                    "Event Tag: " + _this.Scheduler.SectionEvents[i].Tag + "\n"
                )
        }
        alert(result);

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoScheduler.js ItHelpCenter.Demo.TDemoScheduler.prototype.ListEvent", e);
    }
}
ItHelpCenter.Demo.TDemoScheduler.prototype.MoveEvent = function () {
    var _this = this.TParent();
    try {
        if (!isNaN(_this.TxtEventId.Text) && !isNaN(_this.TxtId.Text)) {
            var success = _this.Scheduler.MoveEvent(parseInt(_this.TxtEventId.Text), parseInt(_this.TxtId.Text));
            if (success)
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELEMENTMOVEDCORRECTLY"));
            else
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELEMENTNOTMOVED"));
        }
        else {
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ENTERACORRECTID"));
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoScheduler.js ItHelpCenter.Demo.TDemoScheduler.prototype.MoveEvent", e);
    }
}
ItHelpCenter.Demo.TDemoScheduler.prototype.BlockDayEvent = function () {
    var _this = this.TParent();
    try {
        var config = new _this.Scheduler.ConfigMark();
        config.StarDate = _this.TxtEventTimeStart.Text;
        config.EndDate = _this.TxtEventTimeEnd.Text;
        config.NameClass = "testLeverit";//class style para que el color del bloqueo cambio, ese valor puede estar en un archivo css
        _this.Scheduler.BlockDay(config);

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoScheduler.js ItHelpCenter.Demo.TDemoScheduler.prototype.BlockDayEvent", e);
    }
}