ItHelpCenter.Demo.TDemoListView = function (inObjectHtml, id, callback) {
    this.TParent = function () {
        return this;
    }.bind(this);
    this.ObjectHtml = inObjectHtml;
    this.Id = id;
    this.Callback = callback;
    this.ListView = new Object();
    this.Mythis = "TDemoListView";
    UsrCfg.Traslate.GetLangText(this.Mythis, "SHOWGROUPDATA", "Show Group Data");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ID", "Id");
    UsrCfg.Traslate.GetLangText(this.Mythis, "TEXT", "Text");
    UsrCfg.Traslate.GetLangText(this.Mythis, "TEXTTEXT", "Text in value");
    UsrCfg.Traslate.GetLangText(this.Mythis, "DESCRIPTION", "Description");
    UsrCfg.Traslate.GetLangText(this.Mythis, "DESCRIPTIONTEXT", "Text in description");
    UsrCfg.Traslate.GetLangText(this.Mythis, "TAG", "Tag");
    UsrCfg.Traslate.GetLangText(this.Mythis, "TEXTTAG", "Text in tag");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ADD", "Add");
    UsrCfg.Traslate.GetLangText(this.Mythis, "GET", "Get");
    UsrCfg.Traslate.GetLangText(this.Mythis, "UPDATE", "Update");
    UsrCfg.Traslate.GetLangText(this.Mythis, "DELETE", "Delete");
    UsrCfg.Traslate.GetLangText(this.Mythis, "LIST", "List");
    UsrCfg.Traslate.GetLangText(this.Mythis, "REMOVEALLELEMENTS", "Remove All Elements");
    UsrCfg.Traslate.GetLangText(this.Mythis, "SELECTELEMENT", "Select Element");
    UsrCfg.Traslate.GetLangText(this.Mythis, "GETSELECTELEMENT", "Get Select Element");
    UsrCfg.Traslate.GetLangText(this.Mythis, "GETELEMENT", "Get Element");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ELEMENTADDEDCORRECTLY", "Element Added Correctly");
    UsrCfg.Traslate.GetLangText(this.Mythis, "EDITELEMENTCORRECTLY", "Edited Element Correctly");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ELIMINATEDELEMENTCORRECTLY", "Eliminated Element Correctly");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ELIMINATSEDELEMENTCORRECTLY", "Eliminated Elements Correctly");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ELEMENTCHANGEDCORRECTLY", "Element Changed Correctly");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ELEMENTNOTADDED", "Element not Added");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ELEMENTNOTFOUND", "Element not Found");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ELEMENTNOTEDITED", "Element not edited");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ELEMENTNOTELIMINATED", "Element Not Eliminated");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ELEMENTSNOTELIMINATED", "Elemens Not Eliminated");
    this.InitializeDesigner();
    this.InitializeComponent();
}
ItHelpCenter.Demo.TDemoListView.prototype.InitializeDesigner = function () {
    var _this = this.TParent();
    try {
        var boxbtnGroupData = new TVclStackPanel(_this.ObjectHtml, "boxbtnGroupData", 1, [[12], [12], [12], [12], [12]]);
        $(boxbtnGroupData.Column[0].This)[0].className += " text-center";
        boxbtnGroupData.Column[0].This.style.marginBottom = "5px";
        var btnGroupData = new TVclInputbutton(boxbtnGroupData.Column[0].This, "btnGroupData");
        btnGroupData.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "SHOWGROUPDATA");
        btnGroupData.VCLType = TVCLType.BS;
        _this.boxInit = new TVclStackPanel(_this.ObjectHtml, "boxInit", 1, [[12], [12], [12], [12], [12]]);
        btnGroupData.onClick = function myfunction() {
            $(_this.boxInit.Column[0].This).toggle("slow");
        }
        $(_this.boxInit.Column[0].This).toggle("slow");
        //************ INICIO DE TEXTBOX ***********************************************
        var ConTextControl0 = new TVclStackPanel(_this.boxInit.Column[0].This, "boxId", 2, [[12, 12], [12, 12], [2, 10], [2, 10], [2, 10]]);
        ConTextControl0.Row.This.style.marginTop = "20px";

        var lblLabel0 = new TVcllabel(ConTextControl0.Column[0].This, "", TlabelType.H0);
        lblLabel0.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ID");
        lblLabel0.VCLType = TVCLType.BS;

        _this.TxtId = new TVclTextBox(ConTextControl0.Column[1].This, "txtId");
        _this.TxtId.Text = "1"
        _this.TxtId.This.type = "number"
        _this.TxtId.This.setAttribute("min", "1");
        _this.TxtId.VCLType = TVCLType.BS;
        //************ FIN DE TEXTBOX ***********************************************

        //************ INICIO DE TEXTBOX ***********************************************
        var ConTextControl1 = new TVclStackPanel(_this.boxInit.Column[0].This, "boxValue", 2, [[12, 12], [12, 12], [2, 10], [2, 10], [2, 10]]);
        ConTextControl1.Row.This.style.marginTop = "20px";

        var lblLabel1 = new TVcllabel(ConTextControl1.Column[0].This, "", TlabelType.H0);
        lblLabel1.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TEXT");
        lblLabel1.VCLType = TVCLType.BS;

        _this.TxtValue = new TVclTextBox(ConTextControl1.Column[1].This, "txtValue");
        _this.TxtValue.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TEXTTEXT");
        _this.TxtValue.VCLType = TVCLType.BS;
        //************ FIN DE TEXTBOX ***********************************************
        //************ INICIO DE TEXTBOX ***********************************************
        var ConTextControl2 = new TVclStackPanel(_this.boxInit.Column[0].This, "boxDescription", 2, [[12, 12], [12, 12], [2, 10], [2, 10], [2, 10]]);
        ConTextControl2.Row.This.style.marginTop = "20px";

        var lblLabel2 = new TVcllabel(ConTextControl2.Column[0].This, "", TlabelType.H0);
        lblLabel2.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "DESCRIPTION");
        lblLabel2.VCLType = TVCLType.BS;

        _this.TxtDescription = new TVclTextBox(ConTextControl2.Column[1].This, "txtDescription");
        _this.TxtDescription.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "DESCRIPTIONTEXT");
        _this.TxtDescription.VCLType = TVCLType.BS;
        //************ FIN DE TEXTBOX ***********************************************
        //************ INICIO DE TEXTBOX ***********************************************
        var ConTextControl3 = new TVclStackPanel(_this.boxInit.Column[0].This, "boxTag", 2, [[12, 12], [12, 12], [2, 10], [2, 10], [2, 10]]);
        ConTextControl3.Row.This.style.marginTop = "20px";

        var lblLabel3 = new TVcllabel(ConTextControl3.Column[0].This, "", TlabelType.H0);
        lblLabel3.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TAG");
        lblLabel3.VCLType = TVCLType.BS;

        _this.TxtTag = new TVclTextBox(ConTextControl3.Column[1].This, "txtTag");
        _this.TxtTag.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TEXTTAG");
        _this.TxtTag.VCLType = TVCLType.BS;
        //************ FIN DE TEXTBOX ***********************************************
        var ConTextControl4 = new TVclStackPanel(_this.boxInit.Column[0].This, "boxButtons", 1, [[12], [12], [12], [12], [12]]);
        ConTextControl4.Row.This.style.marginTop = "20px";
        ConTextControl4.Row.This.style.marginBottom = "20px";
        var btnButton1 = new TVclInputbutton(ConTextControl4.Column[0].This, "btnAdd");
        btnButton1.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ADD");
        btnButton1.VCLType = TVCLType.BS;
        btnButton1.onClick = function () {
            try {
                _this.Add(_this, btnButton1);
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("DemoListView.js ItHelpCenter.Demo.TDemoListView.prototype.InitializeDesigner btnButton1.onClick", e);
            }
        }
        btnButton1.This.style.marginTop = "5px";

        var btnButton2 = new TVclInputbutton(ConTextControl4.Column[0].This, "btnEdit");
        btnButton2.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "UPDATE");
        btnButton2.VCLType = TVCLType.BS;
        btnButton2.onClick = function () {
            try {
                _this.Edit(_this, btnButton2);
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("DemoListView.js ItHelpCenter.Demo.TDemoListView.prototype.InitializeDesigner btnButton2.onClick", e);

            }
        }
        btnButton2.This.style.marginLeft = "5px";
        btnButton2.This.style.marginTop = "5px";

        var btnButton3 = new TVclInputbutton(ConTextControl4.Column[0].This, "btnDelete");
        btnButton3.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "DELETE");
        btnButton3.VCLType = TVCLType.BS;
        btnButton3.onClick = function () {
            try {
                _this.Delete(_this, btnButton3);
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("DemoListView.js ItHelpCenter.Demo.TDemoListView.prototype.InitializeDesigner btnButton3.onClick", e);
            }
        }
        btnButton3.This.style.marginLeft = "5px";
        btnButton3.This.style.marginTop = "5px";

        var btnButton4 = new TVclInputbutton(ConTextControl4.Column[0].This, "btnRemoveAll");
        btnButton4.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "REMOVEALLELEMENTS");
        btnButton4.VCLType = TVCLType.BS;
        btnButton4.onClick = function () {
            try {
                _this.RemoveAll(_this, btnButton4);
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("DemoListView.js ItHelpCenter.Demo.TDemoListView.prototype.InitializeDesigner btnButton4.onClick", e);
            }
        }
        btnButton4.This.style.marginLeft = "5px";
        btnButton4.This.style.marginTop = "5px";

        var btnButton5 = new TVclInputbutton(ConTextControl4.Column[0].This, "btnElement");
        btnButton5.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "SELECTELEMENT");
        btnButton5.VCLType = TVCLType.BS;
        btnButton5.onClick = function () {
            try {
                _this.SelectElement(_this, btnButton5);
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("DemoListView.js ItHelpCenter.Demo.TDemoListView.prototype.InitializeDesigner btnButton5.onClick", e);
            }
        }
        btnButton5.This.style.marginLeft = "5px";
        btnButton5.This.style.marginTop = "5px";

        var btnButton6 = new TVclInputbutton(ConTextControl4.Column[0].This, "btnGetSelectElement");
        btnButton6.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "GETSELECTELEMENT");
        btnButton6.VCLType = TVCLType.BS;
        btnButton6.onClick = function () {
            try {
                _this.GetSelectElement(_this, btnButton6);
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("DemoListView.js ItHelpCenter.Demo.TDemoListView.prototype.InitializeDesigner btnButton6.onClick", e);
            }
        }
        btnButton6.This.style.marginLeft = "5px";
        btnButton6.This.style.marginTop = "5px";

        var btnButton7 = new TVclInputbutton(ConTextControl4.Column[0].This, "btnGetElement");
        btnButton7.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "GETELEMENT");
        btnButton7.VCLType = TVCLType.BS;
        btnButton7.onClick = function () {
            try {
                _this.GetElement(_this, btnButton6);
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("DemoListView.js ItHelpCenter.Demo.TDemoListView.prototype.InitializeDesigner btnButton7.onClick", e);
            }
        }
        btnButton7.This.style.marginLeft = "5px";
        btnButton7.This.style.marginTop = "5px";

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoListView.js ItHelpCenter.Demo.TDemoListView.prototype.InitializeDesigner", e);
    }
}
ItHelpCenter.Demo.TDemoListView.prototype.InitializeComponent = function () {
    var _this = this.TParent();
    try {
        if (Source.Menu.IsMobil) {
            _this.ContainerDemo = new TVclStackPanel(_this.ObjectHtml, "ContainerDemo" + "_" + _this.Id, 1, [[12], [12], [12], [12], [12]]);
            var listView = new Componet.ListView.TListView(_this.ContainerDemo.Column[0].This, "ListView1", null);
            _this.ListView = listView;
        }
        else {
            _this.ContainerDemo = new TVclStackPanel(_this.ObjectHtml, "ContainerDemo" + "_" + _this.Id, 1, [[12], [10], [10], [10], [10]]);
            $(_this.ContainerDemo.Column[0].This)[0].className += " center-block";
            _this.ContainerDemo.Column[0].This.style.float = "none";
            var listView = new Componet.ListView.TListView(_this.ContainerDemo.Column[0].This, "ListView1", null);
            _this.ListView = listView;
        }
        var listData = new Array();
        var data1 = new listView.Member();
        data1.Id = 1;
        data1.Value = "Value 1";
        data1.Description = "Description 1";
        data1.Tag = "Tag 1";
        listData.push(data1);
        var data2 = new listView.Member();
        data2.Id = 2;
        data2.Value = "Value 2";
        data2.Description = "Description 2";
        data2.Tag = "Tag 2";
        listData.push(data2);
        var data3 = new listView.Member();
        data3.Id = 3;
        data3.Value = "Value 3";
        data3.Description = "Description 3";
        data3.Tag = "Tag 3";
        listData.push(data3);
        listView.Template = listView._Template.Template1;
        listView.Load(listData);
        listView.OnDbClick = function (objItem) {
            _this.DbClick(objItem);
        }
        listView.OnSelectChange = function (objItem) {
            _this.ChangeElement(objItem);
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoListView.js ItHelpCenter.Demo.TDemoListView.prototype.InitializeComponent", e);
    }
}
ItHelpCenter.Demo.TDemoListView.prototype.Add = function (sender, element) {
    var _this = this.TParent();
    try {
        var data = new sender.ListView.Member();
        data.Id = sender.TxtId.Text;
        data.Value = sender.TxtValue.Text;
        data.Description = sender.TxtDescription.Text;
        data.Tag = sender.TxtTag.Text;
        var success = sender.ListView.AddElement(data);
        if (success) {
            sender.TxtId.Text = "";
            sender.TxtValue.Text = "";
            sender.TxtDescription.Text = "";
            sender.TxtTag.Text = "";
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELEMENTADDEDCORRECTLY"));
        }
        else {
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELEMENTNOTADDED"));
        }

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoListView.js ItHelpCenter.Demo.TDemoListView.prototype.Add", e);
    }
}
ItHelpCenter.Demo.TDemoListView.prototype.Edit = function (sender, element) {
    var _this = this.TParent();
    try {
        var data = new sender.ListView.Member();
        data.Id = sender.TxtId.Text;
        data.Value = sender.TxtValue.Text;
        data.Description = sender.TxtDescription.Text;
        data.Tag = sender.TxtTag.Text;
        var success = sender.ListView.EditElement(data);
        if (success)
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "EDITELEMENTCORRECTLY"));
        else
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELEMENTNOTEDITED"));
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoListView.js ItHelpCenter.Demo.TDemoListView.prototype.Edit", e);
    }
}
ItHelpCenter.Demo.TDemoListView.prototype.Delete = function (sender, element) {
    var _this = this.TParent();
    try {
        var returnElement = sender.ListView.RemoveElement(sender.TxtId.Text);
        if (returnElement)
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELIMINATEDELEMENTCORRECTLY"));
        else
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELEMENTNOTELIMINATED"));
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoListView.js ItHelpCenter.Demo.TDemoListView.prototype.Delete", e);
    }
}
ItHelpCenter.Demo.TDemoListView.prototype.GetEdit = function (sender, element) {
    var _this = this.TParent();
    try {
        var data = sender.ListView.GetElement(sender.TxtId.Text);
        if (data) {
            sender.TxtId.Text = data.Id;
            sender.TxtValue.Text = data.Value;
            sender.TxtDescription.Text = data.Description;
            sender.TxtTag.Text = data.Tag;
        }
        else
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELEMENTNOTFOUND"));
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoListView.js ItHelpCenter.Demo.TDemoListView.prototype.GetEdit", e);
    }
}
ItHelpCenter.Demo.TDemoListView.prototype.RemoveAll = function (sender, element) {
    var _this = this.TParent();
    try {
        var listData = sender.ListView.RemoveAllElement();
        if (listData.length > 0)
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELIMINATSEDELEMENTCORRECTLY"));
        else
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELEMENTSNOTELIMINATED"));
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoListView.js ItHelpCenter.Demo.TDemoListView.prototype.RemoveAll", e);
    }
}
ItHelpCenter.Demo.TDemoListView.prototype.GetSelectElement = function (sender, element) {
    var _this = this.TParent();
    try {
        var item = sender.ListView.GetSelectElement();
        if (item) {
            alert(
                "Id: " + item.Id + "\n" +
                "Value: " + item.Value + "\n" +
                "Description: " + item.Description + "\n" +
                "Tag: " + item.Tag + "\n"
            )
        }
        else {
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELEMENTNOTFOUND"));
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoListView.js ItHelpCenter.Demo.TDemoListView.prototype.SelectElement", e);
    }
}
ItHelpCenter.Demo.TDemoListView.prototype.SelectElement = function (sender, element) {
    var _this = this.TParent();
    try {
        sender.ListView.Select(sender.TxtId.Text);
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoListView.js ItHelpCenter.Demo.TDemoListView.prototype.SelectElement", e);
    }
}
ItHelpCenter.Demo.TDemoListView.prototype.GetElement = function (sender, element) {
    var _this = this.TParent();
    try {
        var element = sender.ListView.GetElement(sender.TxtId.Text);
        if (element) {
            sender.TxtId.Text = element.Id;
            sender.TxtValue.Text = element.Value;
            sender.TxtDescription.Text = element.Description;
            sender.TxtTag.Text = element.Tag;
        }
        else {
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELEMENTNOTFOUND"));
        }

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoListView.js ItHelpCenter.Demo.TDemoListView.prototype.SelectElement", e);
    }
}
ItHelpCenter.Demo.TDemoListView.prototype.ChangeElement = function (objItem) {
    var _this = this.TParent();
    try {
        alert(
            "Id: " + objItem[0].Id + "\n" +
            "Value: " + objItem[0].Value + "\n" +
            "Description: " + objItem[0].Description + "\n" +
            "Tag: " + objItem[0].Tag + "\n"
        )
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoListView.js ItHelpCenter.Demo.TDemoListView.prototype.ChangeElement", e);
    }
}
ItHelpCenter.Demo.TDemoListView.prototype.DbClick = function (objItem) {
    var _this = this.TParent();
    try {
        alert(
            "Id: " + objItem.Id + "\n" +
            "Value: " + objItem.Value + "\n" +
            "Description: " + objItem.Description + "\n" +
            "Tag: " + objItem.Tag + "\n"
        )
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoListView.js ItHelpCenter.Demo.TDemoListView.prototype.DbClick", e);
    }
}