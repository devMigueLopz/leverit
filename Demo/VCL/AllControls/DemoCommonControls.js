﻿ItHelpCenter.Demo.TDemoCommonControls = function (inObjectHtml, _this) {
    //************ INICIO TAB COMMON CONTROLS ************************************************
    //************ INICIO DE LABEL ***********************************************
    var ConLabelControl = new TVclStackPanel(inObjectHtml, "1", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
    ConLabelControl.Row.This.style.marginTop = "20px";

    var lblLabel = new TVcllabel(ConLabelControl.Column[0].This, "", TlabelType.H0);
    lblLabel.Text = "Label";
    lblLabel.VCLType = TVCLType.BS;

    var lblLabelControl = new TVcllabel(ConLabelControl.Column[1].This, "", TlabelType.H0);//CREA UN LABEL CONTROL 
    lblLabelControl.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblLabel"); // PROPIEDAD PARA ASIGNAR O EXTRAER EL TEXTO  
    lblLabelControl.VCLType = TVCLType.BS; // PROPIEDAD PARA EL ESTILO DE LA LIBRERIA UTILIZADA (BOOTSTRAP, ETC)
    //************ FIN DE LABEL ***********************************************

    //************ INICIO DE TEXTBOX ***********************************************
    var ConTextControl = new TVclStackPanel(inObjectHtml, "1", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
    ConTextControl.Row.This.style.marginTop = "20px";

    var lblLabel = new TVcllabel(ConTextControl.Column[0].This, "", TlabelType.H0);
    lblLabel.Text = "TextBox";
    lblLabel.VCLType = TVCLType.BS;

    var txtTextBox = new TVclTextBox(ConTextControl.Column[1].This, "txtExample"); //CREA UN TEXTBOX CONTROL 
    txtTextBox.Text = "text in TextBox" // PROPIEDAD PARA ASIGNAR O EXTRAER EL TEXTO 
    txtTextBox.VCLType = TVCLType.BS;   // PROPIEDAD PARA DARLE EL ESTILO DE LA LIBRERIA UTILIZADA (BOOTSTRAP, ETC)
    //************ FIN DE TEXTBOX ***********************************************

    //************ INICIO DE BUTTON ***********************************************
    var ConButtonControl = new TVclStackPanel(inObjectHtml, "1", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
    ConButtonControl.Row.This.style.marginTop = "20px";

    var lblButton = new TVcllabel(ConButtonControl.Column[0].This, "", TlabelType.H0);
    lblButton.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblButton");
    lblButton.VCLType = TVCLType.BS;

    var btnButton = new TVclInputbutton(ConButtonControl.Column[1].This, ""); //CREA UN BOTON 
    btnButton.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblButton") // PROPIEDAD PARA ASIGNAR O EXTRAER EL TEXTO 
    btnButton.VCLType = TVCLType.BS; // PROPIEDAD PARA DARLE EL ESTILO DE LA LIBRERIA UTILIZADA (BOOTSTRAP, ETC)
    btnButton.onClick = function () { // ASIGNAR EL EVENTO CUANDO SE HAGA CLIC EN EL BOTON
        alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "HelloMessage"));
    }
    //************ FIN DE BUTTON ***********************************************

    //************ INICIO DE CHECKBOX ***********************************************
    var ConCheckBoxControl = new TVclStackPanel(inObjectHtml, "1", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
    ConCheckBoxControl.Row.This.style.marginTop = "20px";

    var lblCheckBox = new TVcllabel(ConCheckBoxControl.Column[0].This, "", TlabelType.H0);
    lblCheckBox.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblcheckbox");
    lblCheckBox.VCLType = TVCLType.BS;

    var chkCheckBox = new TVclInputcheckbox(ConCheckBoxControl.Column[1].This, ""); //CREA UN CHECKBOX
    chkCheckBox.Checked = true; //PROPIEDAD PARA ASIGNARLE CHECK A TRUE O FALSE
    chkCheckBox.VCLType = TVCLType.BS; // PROPIEDAD PARA DARLE EL ESTILO DE LA LIBRERIA UTILIZADA (BOOTSTRAP, ETC)
    //************ FIN DE CHECKBOX ***********************************************

    //************ INICIO DE MEMO ***********************************************
    var ConMemoControl = new TVclStackPanel(inObjectHtml, "1", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
    ConMemoControl.Row.This.style.marginTop = "20px";

    var lblMemo = new TVcllabel(ConMemoControl.Column[0].This, "", TlabelType.H0);
    lblMemo.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblMemo");
    lblMemo.VCLType = TVCLType.BS;

    var Memo1 = new TVclMemo(ConMemoControl.Column[1].This, ""); //CREA UN MEMO CONTROL 
    Memo1.Text = "Write a text";    // PROPIEDAD PARA ASIGNAR O EXTRAER EL TEXTO
    Memo1.VCLType = TVCLType.BS;    // PROPIEDAD PARA DARLE EL ESTILO DE LA LIBRERIA UTILIZADA (BOOTSTRAP, ETC) 
    Memo1.Rows = 5;                 // PROPIEDAD PARA DARLE EL ALTO AL CONTROL 
    Memo1.Cols = 50;                // PROPIEDAD PARA DARLE EL ANCHO AL CONTROL
    //************ FIN DE MEMO ***********************************************

    //************ INICIO DE IMAGEN ***********************************************
    var ConImageControl = new TVclStackPanel(inObjectHtml, "1", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
    ConImageControl.Row.This.style.marginTop = "20px";

    var lblImagen = new TVcllabel(ConImageControl.Column[0].This, "", TlabelType.H0);
    lblImagen.Text = "Imagen";
    lblImagen.VCLType = TVCLType.BS;

    var imgImage = new TVclImagen(ConImageControl.Column[1].This, "");
    imgImage.Src = "../../../image/48/ticket-add.png"; //PROPIEDAD PARA ASIGNARLE O EXTRAER LA RUTA DE LA IMAGEN A MOSTRAS
    imgImage.Title = "esta es una imagen de prueba"; //PROPIEDAD PARA ASIGNARLE O EXTRAER EL TITULO QUE MOSTRARA LA IMAGEN
    imgImage.Cursor = "crosshair"; //PROPIEDAD PARA ASIGANAR O EXTRAER EL TIPO DE CURSOS QUE SE MOSTRARA CUANDO SE PASE EL MOUSE POR ENCIMA
    imgImage.onClick = function () { //PORPIEDAD PARA ASIGNAR EL EVENTO CUANDO SE HAGA CLIC EN LA IMAGEN
        alert("click de la imagen");
    };
    //************ FIN DE IMAGEN ***********************************************

    //************ INICIO DE COMBOBOX ***********************************************
    var ConComboBoxControl = new TVclStackPanel(inObjectHtml, "1", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
    ConComboBoxControl.Row.This.style.marginTop = "20px";

    var lblComboBox = new TVcllabel(ConComboBoxControl.Column[0].This, "", TlabelType.H0);
    lblComboBox.Text = "ComboBox";
    lblComboBox.VCLType = TVCLType.BS;

    var cbCombobox = new TVclComboBox2(ConComboBoxControl.Column[1].This, ""); //CREA UN COMBOBOX CONTROL 

    var VclComboBoxItem1 = new TVclComboBoxItem(); // CREA UN COMBOBOX ITEM PARA SER AGREGADO AL COMBO 
    VclComboBoxItem1.Value = 1; //ASIGNA O EXTRAE EL VALUE AL ITEM
    VclComboBoxItem1.Text = "Elemento 1"; // ASIGNA O EXTRAE EL TEXTO QUE MOSTRARA EL ELEMENTO EN EL COMBO 
    VclComboBoxItem1.Tag = "Tag Elemento 1"; // ASIGNA O EXTRAE UN VALOR QUE PUEDE GUARDARSE DE CUALQUIER TIPO
    cbCombobox.AddItem(VclComboBoxItem1); // AGREGA EL ITEM AL COMBOBOX

    var VclComboBoxItem2 = new TVclComboBoxItem();
    VclComboBoxItem2.Value = 2;
    VclComboBoxItem2.Text = "Elemento 2";
    VclComboBoxItem2.Tag = "Tag Elemento 2";
    cbCombobox.AddItem(VclComboBoxItem2);

    var VclComboBoxItem3 = new TVclComboBoxItem();
    VclComboBoxItem3.Value = 3;
    VclComboBoxItem3.Text = "Elemento 3";
    VclComboBoxItem3.Tag = "Tag Elemento 3";
    cbCombobox.AddItem(VclComboBoxItem3);

    cbCombobox.onChange = function () { // ASIGNA EL EVENTO CUANDO SE SELECCIONE UN ITEM
        var VclComboBoxItem = cbCombobox.Options[cbCombobox.This.selectedIndex];
        alert("Value =  " + VclComboBoxItem.Value + ", Text = " + VclComboBoxItem.Text + ", Tag = " + VclComboBoxItem.Tag);
    }

    //Seleccionar intem por value o index
    cbCombobox.Value = 3; //ASIGNA O EXTRAE LA PROPIEDAD VALUE DEL ITEM SELECCIONADO 
    //************ FIN DE COMBOBOX ***********************************************

    //************ INICIO DE RADIOBUTTON ***********************************************  
    var ContRadioButton = new TVclStackPanel(inObjectHtml, "1", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [4, 8]]);
    ContRadioButton.Row.This.style.marginTop = "20px";

    var lblRadioButton = new TVcllabel(ContRadioButton.Column[0].This, "", TlabelType.H0);
    lblRadioButton.Text = "Radio button"
    lblRadioButton.VCLType = TVCLType.BS;

    var rbLookUpOption = new TVclInputRadioButton(ContRadioButton.Column[1].This, ""); //CREA UN LOOKUPOPTION

    var VclInputRadioButtonItem1 = new TVclInputRadioButtonItem(); //CREA UN RADIO BUTTON ITEM PARA AGREGAR AL LOOKUPOPTION
    VclInputRadioButtonItem1.Id = "Opcion1"; //ASIGNA O EXTRAE UN ID QUE SE LE PUEDE ASIGNAR AL ITEM
    VclInputRadioButtonItem1.Text = "Opcion 1" // ASIGNA O EXTRAE EL TEXTO QUE MUESTRA EL ITEM
    VclInputRadioButtonItem1.Value = 1 //ASIGNA O EXTRAE EL VALUE DEL ITEM 
    rbLookUpOption.AddItem(VclInputRadioButtonItem1); //AGREGAR EL ITEM AL LOOKUPOPTION

    var VclInputRadioButtonItem2 = new TVclInputRadioButtonItem();
    VclInputRadioButtonItem2.Id = "Opcion2";
    VclInputRadioButtonItem2.Text = "Opcion 2"
    VclInputRadioButtonItem2.Value = 2
    rbLookUpOption.AddItem(VclInputRadioButtonItem2);

    var VclInputRadioButtonItem3 = new TVclInputRadioButtonItem();
    VclInputRadioButtonItem3.Id = "Opcion3";
    VclInputRadioButtonItem3.Text = "Opcion 3"
    VclInputRadioButtonItem3.Value = 3
    rbLookUpOption.AddItem(VclInputRadioButtonItem3);

    //se puede utilizar el selected index o el value
    rbLookUpOption.selectedIndex = 0; // SELECCIONAR UN ITEM POR SU INDEX
    rbLookUpOption.Value = 2 // SELECCIONA UN ITEM POR SU VALUE
    //************ FIN DE RADIOBUTTON ***********************************************

    //************ FIN TAB COMMON CONTROLS ************************************************
}

