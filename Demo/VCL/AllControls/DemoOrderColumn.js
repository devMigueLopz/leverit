﻿ItHelpCenter.Demo.TDemoOrderColumn = function (inObjectHtml, _this) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();

    var spContainer = new TVclStackPanel(inObjectHtml, "", 3, [[12, 12, 12], [5, 5, 2], [4, 4, 4], [4, 4, 4], [4, 4, 4]]);

    _this.lblDiv1 = new TVcllabel(spContainer.Column[0].This, "", TlabelType.H4);
    _this.lblDiv1.Text = "Div 1";
    _this.lblDiv2 = new TVcllabel(spContainer.Column[1].This, "", TlabelType.H4);
    _this.lblDiv2.Text = "Div 2";
    _this.lblDiv3 = new TVcllabel(spContainer.Column[2].This, "", TlabelType.H4);
    _this.lblDiv3.Text = "Div 3";

    spContainer.Column[0].This.classList.add("bg-primary");
    spContainer.Column[1].This.classList.add("bg-success");
    spContainer.Column[2].This.classList.add("bg-danger");

    spContainer.Column[0].This.classList.add("pull-right");
    spContainer.Column[1].This.classList.add("pull-right");
    spContainer.Column[2].This.classList.add("pull-left");



    var spContainer2 = new TVclStackPanel(inObjectHtml, "", 3, [[12, 12, 12], [4, 4, 4], [4, 4, 4], [4, 4, 4], [4, 4, 4]]);

    _this.lblDiv12 = new TVcllabel(spContainer2.Column[0].This, "", TlabelType.H4);
    _this.lblDiv12.Text = "Div 1";
    _this.lblDiv22 = new TVcllabel(spContainer2.Column[1].This, "", TlabelType.H4);
    _this.lblDiv22.Text = "Div 2";
    _this.lblDiv32 = new TVcllabel(spContainer2.Column[2].This, "", TlabelType.H4);
    _this.lblDiv32.Text = "Div 3";

    spContainer2.Column[0].This.classList.add("bg-primary");
    spContainer2.Column[1].This.classList.add("bg-success");
    spContainer2.Column[2].This.classList.add("bg-danger");
               
    spContainer2.Column[0].This.classList.add("col-lg-push-8");
    spContainer2.Column[2].This.classList.add("col-lg-pull-8");

    var _ejemplo = new ItHelpCenter.Demo.NewObjecto(function () {
        _ejemplo.valor1 = 1;
        _ejemplo.valor2 = 2;
    });
    var _ejemplo2 = _ejemplo.valor1;
}

ItHelpCenter.Demo.NewObjecto = function() {
    this.valor1 = null;
    this.valor2 = null;
}