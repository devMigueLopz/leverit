﻿ ItHelpCenter.Demo.TDemoUploadAndDownloadFile = function (inObjectHtml, _this) {
    var contOpenFile = new TVclStackPanel(inObjectHtml, "1", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [4, 8]]);
    contOpenFile.Row.This.style.marginTop = "20px";

    var contOpenFileData1 = new TVclStackPanel(inObjectHtml, "1", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [4, 8]]);
    var contOpenFileData2 = new TVclStackPanel(inObjectHtml, "1", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [4, 8]]);
    var contOpenFileData3 = new TVclStackPanel(inObjectHtml, "1", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [4, 8]]);

    var txtDownload1 = new TVclTextBox(contOpenFileData1.Column[1].This, "txtName");
    var txtDownload2 = new TVclTextBox(contOpenFileData2.Column[1].This, "txtRuta");
    var txtDownload3 = new TVclTextBox(contOpenFileData3.Column[1].This, "txtCodigo");

    txtDownload1.Text = "APrueba.txt";
    txtDownload1.VCLType = TVCLType.BS;

    txtDownload2.Text = "1";//"C:\LeverIT\\Discovery\\Software\\SrvPrg\\APrueba.txt";
    txtDownload2.VCLType = TVCLType.BS;

    txtDownload3.Text = "CWEIRCHW";
    txtDownload3.VCLType = TVCLType.BS;

    var lblLabelOpen = new TVcllabel(contOpenFile.Column[0].This, "", TlabelType.H0);
    lblLabelOpen.Text = "Open File";
    lblLabelOpen.VCLType = TVCLType.BS;

    var OpenFile = new TOpenFile(contOpenFile.Column[1].This, "", true);

    var btnOpen = new TVclInputbutton(contOpenFile.Column[1].This, "");
    btnOpen.Text = "Upload"
    btnOpen.VCLType = TVCLType.BS;
    btnOpen.onClick = function () {
        OpenFile.SetFile(function (sender, e) {
            GetSDfile = Comunic.AjaxJson.Client.Methods.GetSDfile(Comunic.Properties.TFileType._ServiceDesk, Comunic.Properties.TFileOperationType._FileWrite, "", sender.Bytes);

            txtDownload1.Text = sender.NameFile;
         

            txtDownload2.Text = sender.Path;
 

            txtDownload3.Text = GetSDfile.IDCode;
          
           

        })
    }

    var contSaveFile = new TVclStackPanel(inObjectHtml, "1", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [4, 8]]);
    contSaveFile.Row.This.style.marginTop = "20px";

    var lblLabelSave = new TVcllabel(contSaveFile.Column[0].This, "", TlabelType.H0);
    lblLabelSave.Text = "Download";
    lblLabelSave.VCLType = TVCLType.BS;

    var SaveFile = new TSaveFile(contSaveFile.Column[1].This, "");
    SaveFile.NameFile = txtDownload1.Text;
    SaveFile.Path = txtDownload2.Text;

    var btnSave = new TVclInputbutton(contSaveFile.Column[1].This, "");
    btnSave.Text = "Download"
    btnSave.VCLType = TVCLType.BS;
    btnSave.onClick = function () {
        GetSDfile = Comunic.AjaxJson.Client.Methods.GetSDfile(Comunic.Properties.TFileType._ServiceDesk, Comunic.Properties.TFileOperationType._FileRead, txtDownload3.Text, new Array());
        SaveFile.Bytes =  GetSDfile.ByteFile;
        SaveFile.SetFile(function (sender, e) {
            alert(sender.Path + " - " + sender.NameFile)
        })
    }
}


 