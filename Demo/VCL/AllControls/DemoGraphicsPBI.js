﻿ItHelpCenter.Demo.TDemoGraphicsPBI = function (inObjectHtml, This, incallbackModalResult) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.Object = inObjectHtml;
    this.CallbackModalResult = incallbackModalResult;
    _this.Load();
}
ItHelpCenter.Demo.TDemoGraphicsPBI.prototype.Load = function () {
    var objBars = null, objPie = null, objPieOne = null, objTreeMap = null, objFunnel = null, objTable = null;
    var _this = this.TParent();
    try {
        //FIX
        var fix = false;
        if ($("#Cont2_idmain_0").hasClass("burger_visible") && Source.Menu.IsMobil) {
            $("#Cont2_idmain_0").removeClass('burger_visible');
            fix = true;
        }
        //FIX
        var template = new Componet.TGraphicsPBI.TVclTemplate(_this.Object, "OnePage", 3, [[12, 12, 12], [6, 6, 12], [6, 6, 12], [3, 5, 4], [4, 4, 4]]);
        template.Column[0].AddRowColumn("graphic0_1", 1, [[12], [12], [12], [12], [12]]);
        template.Column[0].AddRowColumn("graphic0_2", 1, [[12], [12], [12], [12], [12]]);
        template.Column[0].AddRowColumn("graphic0_3", 1, [[12], [12], [12], [12], [12]]);
        template.Column[1].AddRowColumn("graphic1_1", 2, [[6, 6], [6, 6], [6, 6], [6, 6], [6, 6]]);
        template.Column[1].AddRowColumn("graphic1_2", 1, [[12], [12], [12], [12], [12]]);
        template.Column[1].AddRowColumn("graphic1_3", 1, [[12], [12], [12], [12], [12]]);
        template.Column[2].AddRowColumn("graphic2_1", 1, [[12], [12], [12], [12], [12]]);
        template.Column[2].AddRowColumn("graphic2_2", 1, [[12], [12], [12], [12], [12]]);
        var Img = template.Column[0].Child[0].ChildsColumn[0].This
        Img.style.backgroundImage = "url(https://c.ymcdn.com/sites/www.aceds.org/resource/resmgr/images/affiliate_folder/Compliance_DS_rgb.jpg)"
        Img.style.backgroundSize = "100% 100%";
        Img.style.height = "125.264px";
        var funnel = template.Column[0].Child[1].ChildsColumn[0].This;
        var table = template.Column[2].Child[1].ChildsColumn[0].This;
        var tarjeta = template.Column[1].Child[0].ChildsColumn[0].This;
        var PieOne = template.Column[1].Child[0].ChildsColumn[1].This;
        var Barra = template.Column[1].Child[1].ChildsColumn[0].This;
        var Pie = template.Column[1].Child[2].ChildsColumn[0].This;
        var Treemap = template.Column[2].Child[0].ChildsColumn[0].This;
        gFunnel();
        gTable();
        gCard();
        gBars();
        gPieOne();
        gPie();
        gTreeMap();
        function gCard() {
            try {
                ArrayElement = [{ id: "1x", name: "Total de Tickets", value: 81, text: "Cantidad de Casos", color: "rgba(185, 229, 243, 0.26)", colorText: ":#6a737b", colorValue: "black", colorDisplay: "#00a98f", border: "#00a98f", display: true }]
                var objCard = new Componet.TGraphicsPBI.TVclCard(tarjeta, tarjeta.id + "_Card");
                for (var i = 0; i < ArrayElement.length; i++) {
                    var Element = new Componet.TGraphicsElements.TVclCardElements();
                    Element.DisplayValue = ArrayElement[i]["name"];
                    Element.Value = ArrayElement[i]["value"];
                    Element.Text = ArrayElement[i]["text"];
                    Element.Color = ArrayElement[i]["color"];
                    Element.ColorText = ArrayElement[i]["colorText"];
                    Element.ColorValue = ArrayElement[i]["colorValue"];
                    Element.ColorDisplay = ArrayElement[i]["colorDisplay"];
                    Element.Border = ArrayElement[i]["border"];
                    Element.Display = ArrayElement[i]["display"];
                    Element.Id = ArrayElement[i]["id"];
                    Element.onClick = function (inElement) {
                        alert(inElement.DisplayValue);
                    };
                    objCard.CardElementsAdd(Element);
                }
                objCard.CreateCards();
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("DemoGraphicsPBI.js ItHelpCenter.Demo.TDemoGraphicsPBI.prototype.Load function gCard()", e);
            }
        }
        function gBars() {
            try {
                var ArrayElements01 = [{ "id": "1x", "name": "Incidentes", "value": 29, "color": "#00a98f", "text": "Incidentes", "coloroption": "white", "borderoption": "#00a98f", "displayoption": true, "colortext": "black", "backgroundcolor": "#00a98f", "bordercolor": "#00a98f", "hoverbackgroundColor": "#00a98f" },
                { "id": "2x", "name": "Proyectos", "value": 5, "color": "#6a737b", "text": "Tickets", "coloroption": "white", "borderoption": "#00a98f", "displayoption": true, "colortext": "black", "backgroundcolor": "#6a737b", "bordercolor": "#6a737b", "hoverbackgroundColor": "#6a737b" },
                { "id": "3x", "name": "Requerimientos", "value": 54, "color": "rgb(184, 135, 173)", "text": "Recomendaciones", "coloroption": "white", "borderoption": "#00a98f", "displayoption": true, "colortext": "black", "backgroundcolor": "brown", "rgb(184, 135, 173": "rgb(184, 135, 173)", "hoverbackgroundColor": "rgb(184, 135, 173)" }];
                objBars = new Componet.TGraphicsPBI.TVclBars(Barra, Barra.id + "_BarsV1");
                for (var i = 0; i < ArrayElements01.length; i++) {
                    var Element = new Componet.TGraphicsElements.TVclBarsElements();
                    Element.DisplayValue = ArrayElements01[i]["name"];
                    Element.Value = ArrayElements01[i]["value"];
                    Element.Color = ArrayElements01[i]["color"];
                    Element.Text = ArrayElements01[i]["text"];
                    Element.ColorOption = ArrayElements01[i]["coloroption"];
                    Element.BorderOption = ArrayElements01[i]["borderoption"];
                    Element.DisplayOption = ArrayElements01[i]["displayoption"];
                    Element.ColorText = ArrayElements01[i]["colortext"];
                    Element.BorderColorGraphic = ArrayElements01[i]["bordercolor"];
                    Element.HoverColorGraphic = ArrayElements01[i]["hoverbackgroundColor"];
                    Element.Id = ArrayElements01[i]["id"];
                    Element.onClick = function (inElement) {
                        alert(inElement.DisplayValue);
                    };
                    objBars.BarsElementsAdd(Element);
                }
                /*DECLARACION DE PROPIEDADES PARA PLUGIN*/
                objBars.Title = "Reportes"
                objBars.BorderWidth = 1;
                objBars.BeginAtZero = true;
                objBars.MinRotation = 40;
                objBars.MaxRotation = 40;
                objBars.HeightPlugin = "380px";
                objBars.CreateGraphics();
                objBars.onClickPlugin = function (chartData, idx) {
                    var label = chartData.labels[idx];
                    var value = chartData.datasets[0].data[idx];
                    alert(label + " --- " + value);
                }
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("DemoGraphicsPBI.js ItHelpCenter.Demo.TDemoGraphicsPBI.prototype.Load function gBars()", e);
            }
        }
        function gPieOne() {
            try {
                var ArrayElements02 = [{ "id": "1x", "name": "SI ASIGNADO", "value": 90, "color": "#00a98f", "text": "Cantidad", "coloroption": "white", "borderoption": "#00a98f", "displayoption": true, "colortext": "black", "backgroundcolor": "#00a98f", "bordercolor": "#00a98f", "hoverbackgroundColor": "#00a98f" },
                { "id": "2x", "name": "TER_SIN_CF", "value": 10, "color": "#6a737b", "text": "Cantidad", "coloroption": "white", "borderoption": "#00a98f", "displayoption": true, "colortext": "black", "backgroundcolor": "#6a737b", "bordercolor": "#6a737b", "hoverbackgroundColor": "#6a737b" }];
                objPieOne = new Componet.TGraphicsPBI.TVclPie(PieOne, PieOne.id + "_PieOne");
                for (var i = 0; i < ArrayElements02.length; i++) {
                    var Element = new Componet.TGraphicsElements.TVclPieElements();
                    Element.DisplayValue = ArrayElements02[i]["name"];
                    Element.Value = ArrayElements02[i]["value"];
                    Element.Color = ArrayElements02[i]["color"];
                    Element.Text = ArrayElements02[i]["text"];
                    Element.ColorOption = ArrayElements02[i]["coloroption"];
                    Element.BorderOption = ArrayElements02[i]["borderoption"];
                    Element.DisplayOption = ArrayElements02[i]["displayoption"];
                    Element.ColorText = ArrayElements02[i]["colortext"];
                    Element.BorderColorGraphic = ArrayElements02[i]["bordercolor"];
                    Element.HoverColorGraphic = ArrayElements02[i]["hoverbackgroundColor"];
                    Element.Id = ArrayElements02[i]["id"];
                    Element.onClick = function (inElement) {
                        alert(inElement.DisplayValue);
                    };
                    objPieOne.PieElementsAdd(Element);
                }
                objPieOne.Title = "Reportes"
                objPieOne.BorderWidth = 1;
                objPieOne.BeginAtZero = true;
                objPieOne.Option = false;
                objPieOne.HeightPlugin = "150px";
                objPieOne.CreateGraphics();
                objPieOne.onClickPlugin = function (chartData, idx) {
                    var label = chartData.labels[idx];
                    var value = chartData.datasets[0].data[idx];
                    alert(label + " --- " + value);
                }
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("DemoGraphicsPBI.js ItHelpCenter.Demo.TDemoGraphicsPBI.prototype.Load function gBars()", e);
            }
        }
        function gPie() {
            try {
                var ArrayElements03 = [{ "id": "1x", "name": "SI ASIGNADO", "value": 90, "color": "#00a98f", "text": "Cantidad", "coloroption": "white", "borderoption": "#00a98f", "displayoption": true, "colortext": "black", "backgroundcolor": "#00a98f", "bordercolor": "#00a98f", "hoverbackgroundColor": "#00a98f" },
                { "id": "2x", "name": "TER_SIN_CF", "value": 10, "color": "#6a737b", "text": "Cantidad", "coloroption": "white", "borderoption": "#00a98f", "displayoption": true, "colortext": "black", "backgroundcolor": "#6a737b", "bordercolor": "#6a737b", "hoverbackgroundColor": "#6a737b" }];
                objPie = new Componet.TGraphicsPBI.TVclPie(Pie, Pie.id + "_Pie");
                for (var i = 0; i < ArrayElements03.length; i++) {
                    var Element = new Componet.TGraphicsElements.TVclPieElements();
                    Element.DisplayValue = ArrayElements03[i]["name"];
                    Element.Value = ArrayElements03[i]["value"];
                    Element.Color = ArrayElements03[i]["color"];
                    Element.Text = ArrayElements03[i]["text"];
                    Element.ColorOption = ArrayElements03[i]["coloroption"];
                    Element.BorderOption = ArrayElements03[i]["borderoption"];
                    Element.DisplayOption = ArrayElements03[i]["displayoption"];
                    Element.ColorText = ArrayElements03[i]["colortext"];
                    Element.BorderColorGraphic = ArrayElements03[i]["bordercolor"];
                    Element.HoverColorGraphic = ArrayElements03[i]["hoverbackgroundColor"];
                    Element.Id = ArrayElements03[i]["id"];
                    Element.onClick = function (inElement) {
                        alert(inElement.DisplayValue);
                    };
                    objPie.PieElementsAdd(Element);
                }
                objPie.Title = "Reportes"
                objPie.BorderWidth = 1;
                objPie.BeginAtZero = true;
                objPie.Option = true;
                objPie.HeightPlugin = "250px";
                objPie.CreateGraphics();
                objPie.onClickPlugin = function (chartData, idx) {
                    var label = chartData.labels[idx];
                    var value = chartData.datasets[0].data[idx];
                    alert(label + " --- " + value);
                }
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("DemoGraphicsPBI.js ItHelpCenter.Demo.TDemoGraphicsPBI.prototype.Load function gPie()", e);
            }
        }
        function gTreeMap() {
            try {
                ArrayElements04 = [
                    { "id": "1x", "name": "Everit", "value": 25, "color": "rgb(1, 184, 170)" },
                    { "id": "2x", "name": "Coca-Cola", "value": 81, "color": "rgb(55, 70, 73)" },
                    { "id": "3x", "name": "Dento", "value": 37, "color": "rgb(253, 98, 94)" },
                    { "id": "4x", "name": "Samsung", "value": 63, "color": "rgb(242, 200, 15)" },
                    { "id": "5x", "name": "Apple", "value": 11, "color": "rgb(95, 107, 109)" },
                    { "id": "6x", "name": "Sony", "value": 24, "color": "rgb(138, 212, 235)" },
                    { "id": "7x", "name": "Wong", "value": 25, "color": "rgb(254, 150, 102)" },
                    { "id": "8x", "name": "Soho", "value": 78, "color": "rgb(166, 105, 153)" },
                    { "id": "9x", "name": "Rustica", "value": 69, "color": "rgb(53, 153, 184)" },
                    { "id": "10x", "name": "Jonny Walker", "value": 10, "color": "rgb(223, 191, 191)" },
                    { "id": "11x", "name": "Pilsen", "value": 49, "color": "rgb(74, 197, 187)" },
                    { "id": "12x", "name": "Don Vittorio", "value": 45, "color": "rgb(95, 107, 109)" },
                    { "id": "13x", "name": "Gloria", "value": 74, "color": "rgb(251, 130, 129)" },
                    { "id": "14x", "name": "Nestle", "value": 42, "color": "rgb(244, 210, 90)" },
                    { "id": "15x", "name": "Everit", "value": 18, "color": "rgb(127, 137, 138)" },
                    { "id": "16x", "name": "Coca-Cola", "value": 21, "color": "rgb(164, 221, 238)" },
                    { "id": "17x", "name": "Dento", "value": 10, "color": "rgb(253, 171, 137)" },
                    { "id": "18x", "name": "Samsung", "value": 51, "color": "rgb(182, 135, 172)" },
                    { "id": "19x", "name": "Apple", "value": 46, "color": "rgb(40, 115, 138)" },
                    { "id": "20x", "name": "Sony", "value": 39, "color": "rgb(167, 143, 143)" },
                    { "id": "21x", "name": "Wong", "value": 45, "color": "rgb(22, 137, 128)" },
                    { "id": "22x", "name": "Soho", "value": 83, "color": "rgb(41, 53, 55)" },
                    { "id": "23x", "name": "Rustica", "value": 50, "color": "rgb(187, 74, 74)" },
                    { "id": "24x", "name": "Jonny Walker", "value": 53, "color": "rgb(181, 149, 37)" },
                    { "id": "25x", "name": "Pilsen", "value": 37, "color": "rgb(106, 159, 176)" },
                    { "id": "26x", "name": "Don Vittorio", "value": 32, "color": "rgb(189, 113, 80)" },
                    { "id": "27x", "name": "Gloria", "value": 45, "color": "rgb(123, 79, 113)" },
                    { "id": "28x", "name": "Nestle", "value": 21, "color": "rgb(27, 77, 92)" },
                    { "id": "29x", "name": "Don Vittorio", "value": 18, "color": "rgb(112, 96, 96)" },
                    { "id": "30x", "name": "Gloria", "value": 45, "color": "rgb(15, 92, 85)" },
                    { "id": "31x", "name": "Nestle", "value": 20, "color": "rgb(28, 35, 37)" }

                ];
                objTreeMap = new Componet.TGraphicsPBI.TVclTreeMap(Treemap, Treemap.id + "_TreeMapV1");
                for (var i = 0; i < ArrayElements04.length; i++) {
                    var Element = new Componet.TGraphicsElements.TVclTreeMapElements();
                    Element.Id = ArrayElements04[i]["id"];
                    Element.Name = ArrayElements04[i]["name"];
                    Element.Color = ArrayElements04[i]["color"];
                    Element.Value = ArrayElements04[i]["value"];
                    objTreeMap.TreeMapElementsAdd(Element);
                }
                objTreeMap.HeightPlugin = "400px";
                objTreeMap.Title = "Clientes";
                objTreeMap.CreateGraphics();
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("DemoGraphicsPBI.js ItHelpCenter.Demo.TDemoGraphicsPBI.prototype.Load function gTreeMap()", e);
            }
        }
        function gFunnel() {
            try {
                var ArrayElements05 = [
                    { name: "AGarcia", value: 7, color: "rgb(1,184,170)" },
                    { name: "ARincon", value: 8, color: "rgb(254,171,133)" },
                    { name: "ARodriguez", value: 19, color: "rgb(104,159,176)" },
                    { name: "DSalazar", value: 15, color: "rgb(190,74,71)" },
                    { name: "NRojas", value: 3, color: "rgb(184,135,173)" },
                    { name: "ARincon", value: 8, color: "rgb(254,171,133)" },
                    { name: "ARodriguez", value: 19, color: "rgb(104,159,176)" },
                    { name: "DSalazar", value: 15, color: "rgb(190,74,71)" },
                    { name: "NRojas", value: 3, color: "rgb(184,135,173)" },
                    { name: "ARincon", value: 8, color: "rgb(254,171,133)" },
                    { name: "ARodriguez", value: 19, color: "rgb(104,159,176)" },
                    { name: "ARodriguez", value: 19, color: "rgb(104,159,176)" },
                    { name: "DSalazar", value: 15, color: "rgb(190,74,71)" },
                    { name: "NRojas", value: 3, color: "rgb(184,135,173)" },
                    { name: "ARincon", value: 8, color: "rgb(254,171,133)" },
                    { name: "ARodriguez", value: 19, color: "rgb(104,159,176)" },
                    { name: "DSalazar", value: 15, color: "rgb(190,74,71)" },
                    { name: "NRojas", value: 3, color: "rgb(184,135,173)" }];
                objFunnel = new Componet.TGraphicsPBI.TVclFunnel(funnel, funnel.id + '_Funnel1');
                for (var i = 0; i < ArrayElements05.length; i++) {
                    var Element = new Componet.TGraphicsElements.TVclGraphicElementFunnel();
                    Element.Label = ArrayElements05[i]["name"];
                    Element.Value = ArrayElements05[i]["value"];
                    Element.Color = ArrayElements05[i]["color"];
                    Element.Id = "Element2_" + i;
                    Element.onClick = function (inElement) {
                        if (inElement.Highlight != null) {
                            alert(inElement.Label + ': ' + inElement.Value + '\nResaltado: ' + inElement.Highlight);
                        }
                        else {
                            alert(inElement.Label + ': ' + inElement.Value);
                        }
                    };
                    objFunnel.TVclGraphicElementsAdd(Element);
                }

                objFunnel.Title = "Asesores";
                objFunnel.DisplayTitle = true;
                objFunnel.CreateGraphic();
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("DemoGraphicsPBI.js ItHelpCenter.Demo.TDemoGraphicsPBI.prototype.Load function gFunnel()", e);
            }
        }
        function gTable() {
            try {
                objTable = new Componet.TGraphicsPBI.TVclTable(table, table.id + "_Table1")
                var ArrayElements06 = [
                    { name: "Empresa1", value: 10, color: "blue", chicamaso: "Chicamaso 1", algo: "mas", otro: "mas de mas" },
                    { name: "Empresa2", value: 30, color: "orange", chicamaso: "Chicamaso 2 mas puntos suspensivos", algo: "mas", otro: "mas de mas" },
                    { name: "Empresa3", value: 60, color: "red", chicamaso: "Chicamaso 3", algo: "mas", otro: "mas de mas" },
                    { name: "Empresa4", value: 40, color: "black", chicamaso: "Chicamaso 3", algo: "mas", otro: "mas de mas" },
                    { name: "Empresa5", value: 34, color: "pink", chicamaso: "Chicamaso 4", algo: "mas", otro: "mas de mas" },
                    { name: "Empresa6", value: 54, color: "red", chicamaso: "Chicamaso 5", algo: "mas", otro: "mas de mas" },
                    { name: "Empresa7", value: 54, color: "blue", chicamaso: "Chicamaso 6", algo: "mas", otro: "mas de mas" },
                    { name: "Empresa8", value: 64, color: "green", chicamaso: "Chicamaso 7", algo: "mas", otro: "mas de mas" },
                    { name: "Empresa9", value: 74, color: "white", chicamaso: "Chicamaso 8", algo: "mas", otro: "mas de mas" },
                    { name: "Empresa10", value: 24, color: "yellow", chicamaso: "Chicamaso 9", algo: "mas", otro: "mas de mas" },
                    { name: "Empresa11", value: 34, color: "black", chicamaso: "Chicamaso 10", algo: "mas", otro: "mas de mas" },
                    { name: "Empresa12", value: 44, color: "yellow", chicamaso: "Chicamaso 11", algo: "mas", otro: "mas de mas" },
                    { name: "Empresa13", value: 54, color: "green", chicamaso: "Chicamaso 12", algo: "mas", otro: "mas de mas" },
                    { name: "Empresa14", value: 37, color: "yellow", chicamaso: "Chicamaso 13", algo: "mas", otro: "mas de mas" },
                    { name: "Empresa15", value: 38, color: "black", chicamaso: "Chicamaso 14", algo: "mas", otro: "mas de mas" },
                    { name: "Empresa16", value: 39, color: "white", chicamaso: "Chicamaso 15", algo: "mas", otro: "mas de mas" },
                    { name: "Empresa17", value: 41, color: "yellow", chicamaso: "Chicamaso 16", algo: "mas", otro: "mas de mas" },
                    { name: "Empresa18", value: 43, color: "green", chicamaso: "Chicamaso 17", algo: "mas", otro: "mas de mas" },
                    { name: "Empresa19", value: 34, color: "white", chicamaso: "Chicamaso 18", algo: "mas", otro: "mas de mas" },
                    { name: "Empresa20", value: 35, color: "yellow", chicamaso: "Chicamaso 19", algo: "mas", otro: "mas de mas" },
                    { name: "Empresa21", value: 39, color: "black", chicamaso: "Chicamaso 20", algo: "mas", otro: "mas de mas" },
                    { name: "Empresa22", value: 60, color: "white", chicamaso: "Chicamaso 21", algo: "mas", otro: "mas de mas" },
                    { name: "Empresa23", value: 65, color: "green", chicamaso: "Chicamaso 22", algo: "mas", otro: "mas de mas" }];
                for (var i = 0; i < ArrayElements06.length; i++) {
                    var Element = new Componet.TGraphicsElements.TVclGraphicElementTable();
                    Element.Id = "Element4_" + i;
                    Element.Array = ArrayElements06[i];
                    Element.onClick = function (inElement) {
                        alert(inElement.Array["name"] + ': ' + inElement.Array["value"]);
                    };
                    objTable.TVclGraphicElementsAdd(Element);
                }
                objTable.Elements[0].Highlight = false;
                objTable.Title = "Tabla de Clientes";
                objTable.DisplayTitle = true;
                objTable.CreateGraphic();
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("DemoGraphicsPBI.js ItHelpCenter.Demo.TDemoGraphicsPBI.prototype.Load function gTable()", e);
            }
        }
        Img.onclick = function () {

            try {
                objBars.Elements[0].ValueHighlight = 4;
                objBars.Elements[0].DisplayHighlight = true;
                objBars.Elements[1].ValueHighlight = 1;
                objBars.Elements[1].DisplayHighlight = true;
                objBars.Elements[2].ValueHighlight = 5;
                objBars.Elements[2].DisplayHighlight = true;

                objPie.Elements[0].ValueHighlight = 30;
                objPie.Elements[0].DisplayHighlight = true;
                objPie.Elements[1].ValueHighlight = 5;
                objPie.Elements[1].DisplayHighlight = true;

                objPieOne.Elements[0].ValueHighlight = 30;
                objPieOne.Elements[0].DisplayHighlight = true;

                objPieOne.Elements[1].ValueHighlight = 5;
                objPieOne.Elements[1].DisplayHighlight = true;

                objTreeMap.Elements[1].DisplayHighlight = true;
                objTreeMap.Elements[1].ValueHighlight = 10

                objTreeMap.Elements[2].DisplayHighlight = true;
                objTreeMap.Elements[2].ValueHighlight = 10

                objTreeMap.Elements[3].DisplayHighlight = true;
                objTreeMap.Elements[3].ValueHighlight = 10

                objTreeMap.Elements[4].DisplayHighlight = true;
                objTreeMap.Elements[4].ValueHighlight = 10

                objTreeMap.Elements[5].DisplayHighlight = true;
                objTreeMap.Elements[5].ValueHighlight = 10

                objTreeMap.Elements[6].DisplayHighlight = true;
                objTreeMap.Elements[6].ValueHighlight = 10

                objTreeMap.Elements[7].DisplayHighlight = true;
                objTreeMap.Elements[7].ValueHighlight = 10

                objTreeMap.Elements[8].DisplayHighlight = true;
                objTreeMap.Elements[8].ValueHighlight = 10

                objTreeMap.Elements[9].DisplayHighlight = true;
                objTreeMap.Elements[9].ValueHighlight = 10

                objTreeMap.Elements[10].DisplayHighlight = true;
                objTreeMap.Elements[10].ValueHighlight = 10

                objTreeMap.Elements[11].DisplayHighlight = true;
                objTreeMap.Elements[11].ValueHighlight = 10

                objTreeMap.Elements[12].DisplayHighlight = true;
                objTreeMap.Elements[12].ValueHighlight = 10

                objTreeMap.Elements[13].DisplayHighlight = true;
                objTreeMap.Elements[13].ValueHighlight = 10

                objTreeMap.Elements[14].DisplayHighlight = true;
                objTreeMap.Elements[14].ValueHighlight = 10

                objTreeMap.Elements[15].DisplayHighlight = true;
                objTreeMap.Elements[15].ValueHighlight = 10

                objTreeMap.Elements[16].DisplayHighlight = true;
                objTreeMap.Elements[16].ValueHighlight = 10

                objTreeMap.Elements[17].DisplayHighlight = true;
                objTreeMap.Elements[17].ValueHighlight = 10

                objTreeMap.Elements[18].DisplayHighlight = true;
                objTreeMap.Elements[18].ValueHighlight = 10

                objTreeMap.Elements[19].DisplayHighlight = true;
                objTreeMap.Elements[19].ValueHighlight = 10

                objTreeMap.Elements[20].DisplayHighlight = true;
                objTreeMap.Elements[20].ValueHighlight = 10

                objTreeMap.Elements[21].DisplayHighlight = true;
                objTreeMap.Elements[21].ValueHighlight = 10

                objTreeMap.Elements[22].DisplayHighlight = true;
                objTreeMap.Elements[22].ValueHighlight = 10

                objTreeMap.Elements[23].DisplayHighlight = true;
                objTreeMap.Elements[23].ValueHighlight = 10

                objTreeMap.Elements[24].DisplayHighlight = true;
                objTreeMap.Elements[24].ValueHighlight = 10

                objTreeMap.Elements[25].DisplayHighlight = true;
                objTreeMap.Elements[25].ValueHighlight = 10

                objTreeMap.Elements[26].DisplayHighlight = true;
                objTreeMap.Elements[26].ValueHighlight = 10

                objTreeMap.Elements[26].DisplayHighlight = true;
                objTreeMap.Elements[26].ValueHighlight = 10

                objTreeMap.Elements[27].DisplayHighlight = true;
                objTreeMap.Elements[27].ValueHighlight = 10

                objTreeMap.Elements[28].DisplayHighlight = true;
                objTreeMap.Elements[28].ValueHighlight = 10

                objTreeMap.Elements[29].DisplayHighlight = true;
                objTreeMap.Elements[29].ValueHighlight = 10

                objTreeMap.Elements[30].DisplayHighlight = true;
                objTreeMap.Elements[30].ValueHighlight = 10

                objFunnel.Elements[0].Highlight = 5;
                objFunnel.Elements[1].Highlight = 4;
                objFunnel.Elements[2].Highlight = 10;
                objFunnel.Elements[3].Highlight = 0;
                objFunnel.Elements[4].Highlight = 2;
                objFunnel.Elements[5].Highlight = 3;
                objFunnel.Elements[6].Highlight = 18;
                objFunnel.Elements[7].Highlight = 13;
                objFunnel.Elements[8].Highlight = 1;
                objFunnel.Elements[9].Highlight = 6;
                objFunnel.Elements[10].Highlight = 13;
                objFunnel.Elements[11].Highlight = 7;
                objFunnel.Elements[12].Highlight = 12;
                objFunnel.Elements[13].Highlight = 1;
                objFunnel.Elements[14].Highlight = 5;
                objFunnel.Elements[15].Highlight = 15;
                objFunnel.Elements[16].Highlight = 13;
                objFunnel.Elements[17].Highlight = 1;

                // objFunnel.Elements[0].Color = "red";

                objTable.Elements[0].Highlight = true;
                for (var i = 1; i < objTable.Elements.length; i++) {
                    objTable.Elements[i].Highlight = false;
                }
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("DemoGraphicsPBI.js ItHelpCenter.Demo.TDemoGraphicsPBI.prototype.Load function Img.onclick()", e);
            }

        }
        objTreeMap.Elements[0].onClick = function (t) {
            alert(t.Name);
        }
        objTreeMap.Elements[1].onClick = function (t) {
            alert(t.Name);
        }
        objTreeMap.Elements[2].onClick = function (t) {
            alert(t.Name);
        }
        objTreeMap.Elements[3].onClick = function (t) {
            alert(t.Name);
        }
        objTreeMap.Elements[4].onClick = function (t) {
            alert(t.Name);
        }
        objTreeMap.Elements[5].onClick = function (t) {
            alert(t.Name);
        }
        objTreeMap.Elements[6].onClick = function (t) {
            alert(t.Name);
        }
        objTreeMap.Elements[7].onClick = function (t) {
            alert(t.Name);
        }
        objTreeMap.Elements[8].onClick = function (t) {
            alert(t.Name);
        }
        objTreeMap.Elements[9].onClick = function (t) {
            alert(t.Name);
        }
        objTreeMap.Elements[10].onClick = function (t) {
            alert(t.Name);
        }
        objTreeMap.Elements[11].onClick = function (t) {
            alert(t.Name);
        }
        objTreeMap.Elements[12].onClick = function (t) {
            alert(t.Name);
        }
        objTreeMap.Elements[13].onClick = function (t) {
            alert(t.Name);
        }
        objTreeMap.Elements[14].onClick = function (t) {
            alert(t.Name);
        }
        objTreeMap.Elements[15].onClick = function (t) {
            alert(t.Name);
        }
        objTreeMap.Elements[16].onClick = function (t) {
            alert(t.Name);
        }
        objTreeMap.Elements[17].onClick = function (t) {
            alert(t.Name);
        }
        objTreeMap.Elements[18].onClick = function (t) {
            alert(t.Name);
        }
        objTreeMap.Elements[19].onClick = function (t) {
            alert(t.Name);
        }
        objTreeMap.Elements[20].onClick = function (t) {
            alert(t.Name);
        }
        objTreeMap.Elements[21].onClick = function (t) {
            alert(t.Name);
        }
        objTreeMap.Elements[22].onClick = function (t) {
            alert(t.Name);
        }
        objTreeMap.Elements[23].onClick = function (t) {
            alert(t.Name);
        }
        objTreeMap.Elements[24].onClick = function (t) {
            alert(t.Name);
        }
        objTreeMap.Elements[25].onClick = function (t) {
            alert(t.Name);
        }
        objTreeMap.Elements[26].onClick = function (t) {
            alert(t.Name);
        }
        objTreeMap.Elements[27].onClick = function (t) {
            alert(t.Name);
        }
        objTreeMap.Elements[28].onClick = function (t) {
            alert(t.Name);
        }
        objTreeMap.Elements[29].onClick = function (t) {
            alert(t.Name);
        }
        objTreeMap.Elements[30].onClick = function (t) {
            alert(t.Name);
        }
        //FIX
        if (fix) {
            $("#Cont2_idmain_0").addClass('burger_visible');
        }
        //FIX
        //ismobile
        if (Source.Menu.IsMobil) {
            _this.Object.style.paddingLeft = "0px";
            _this.Object.style.paddingRight = "0px";
            template.Column[0].Child[1].ChildsColumn[0].This.style.marginBottom = "10px";
            template.Column[2].Child[1].ChildsColumn[0].This.style.marginBottom = "10px";
            template.Column[1].Child[0].ChildsColumn[0].This.style.marginBottom = "10px";
            template.Column[1].Child[0].ChildsColumn[1].This.style.marginBottom = "10px";
            template.Column[1].Child[1].ChildsColumn[0].This.style.marginBottom = "10px";
            template.Column[1].Child[2].ChildsColumn[0].This.style.marginBottom = "10px";
            template.Column[2].Child[0].ChildsColumn[0].This.style.marginBottom = "10px";
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoGraphicsPBI.js ItHelpCenter.Demo.TDemoGraphicsPBI.prototype.Load", e);
    }
}