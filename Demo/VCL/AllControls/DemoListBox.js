﻿ItHelpCenter.Demo.TDemoListBox = function (inObjectHtml, _this) {
    var sp1 = new TVclStackPanel(inObjectHtml, "1", 1, [[12], [12], [12], [12], [12]]);
    sp1.Row.This.style.marginBottom = "15px";

    var btn1 = new TVclInputbutton(sp1.Column[0].This, "");
    btn1.Text = "Disabled CheckBox";
    btn1.VCLType = TVCLType.BS;
    btn1.onClick = function () {
        if (btn1.Text == "Disabled CheckBox") {
            ListBox.EnabledCheckBox = false;
            btn1.Text = "Enabled CheckBox";
        }
        else {
            ListBox.EnabledCheckBox = true;
            btn1.Text = "Disabled CheckBox";
        }
    }

    var btn2 = new TVclInputbutton(sp1.Column[0].This, "");
    btn2.Text = "Checked All";
    btn2.VCLType = TVCLType.BS;
    btn2.onClick = function () {
        ListBox.CheckedAll();
    }

    var btn3 = new TVclInputbutton(sp1.Column[0].This, "");
    btn3.Text = "UnChecked All";
    btn3.VCLType = TVCLType.BS;
    btn3.onClick = function () {
        ListBox.UnCheckedAll();
    }

    var ListBox = new TVclListBox(inObjectHtml, "");
    ListBox.EnabledCheckBox = true;
    ListBox.OnCheckedListBoxItemChange = function (EventArgs, Object) {
        alert(EventArgs.Checked);
    }

    var ListBoxItem1 = new TVclListBoxItem();
    ListBoxItem1.Text = "Item 1";
    ListBoxItem1.Index = 0;
    ListBox.AddListBoxItem(ListBoxItem1);

    var ListBoxItem2 = new TVclListBoxItem();
    ListBoxItem2.Text = "Item 2";
    ListBoxItem1.Index = 1;
    ListBox.AddListBoxItem(ListBoxItem2);

    var ListBoxItem3 = new TVclListBoxItem();
    ListBoxItem3.Text = "Item 3";
    ListBoxItem1.Index = 2;
    ListBox.AddListBoxItem(ListBoxItem3);
}