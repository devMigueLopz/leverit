﻿ItHelpCenter.Demo.TDemoStackPanel = function (inObjectHtml, _this) {

    //************ INICIO TAB STACK PANEL ************************************************   
    var spStackPanel1 = new TVclStackPanel(inObjectHtml, "1", 1, [[12], [12], [12], [12], [12]]); //CREA UN STACK PANEL 
    var spStackPanel2 = new TVclStackPanel(inObjectHtml, "1", 4, [[3, 3, 3, 3], [3, 3, 3, 3], [3, 3, 3, 3], [3, 3, 3, 3], [3, 3, 3, 3]]); //CREA UN STACK PANEL 

    var spDiv1 = spStackPanel1.Column[0].This;
    spDiv1 = spStackPanel1.Column[0].This;
    var spRowA = spStackPanel1.Row; // PROPIEDAD ROW ACCEDE O EXTRAE AL CONTENEDOR PRINCIPAL DEL STACKPANEL 
    var spDiv2 = spStackPanel2.Column[0].This; // PROPIEDAD COLUMN[I] ACCEDE O EXTRAE LA COLUMNA DE ACUERDO AL INDEX 
    var spDiv3 = spStackPanel2.Column[1].This;
    var spDiv4 = spStackPanel2.Column[2].This;
    var spDiv5 = spStackPanel2.Column[3].This;
    var spRowB = spStackPanel2.Row;
    Vcllabel(spDiv1, "", TVCLType.BS, TlabelType.H0, "Div1");
    Vcllabel(spDiv2, "", TVCLType.BS, TlabelType.H0, "Div2");
    Vcllabel(spDiv3, "", TVCLType.BS, TlabelType.H0, "Div3");
    Vcllabel(spDiv4, "", TVCLType.BS, TlabelType.H0, "Div4");
    Vcllabel(spDiv5, "", TVCLType.BS, TlabelType.H0, "Div5");
    //************ FIN TAB STACK PANEL  ************************************************
}