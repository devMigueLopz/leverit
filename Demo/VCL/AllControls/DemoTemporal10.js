﻿ItHelpCenter.Demo.TDemoTemporal10 = function (inObjectHtml, _this) {
	this.TParent = function(){
		return this;
	}.bind(this);
	this.ObjectHtml = inObjectHtml;
	this.cont = 0;
	this.DateTable = null;
	this.ElementList = new Array();
	this.VotesProfiler = new Persistence.Votes.TVotesProfiler();
	this.Load();
}

ItHelpCenter.Demo.TDemoTemporal10.prototype.Load = function (){	
	var _this = this.TParent();
	try{
		_this.LoadProfiler();

		_this.ContainerAll = new TVclStackPanel(_this.ObjectHtml, "", 1, [[12], [12], [12], [12], [12]]);
		_this.ContPreview = new TVclStackPanel(_this.ObjectHtml, "", 1, [[12], [12], [12], [12], [12]]);
		var Container = null;
		if(Source.Menu.IsMobil){
			Container = new TVclStackPanel(_this.ContainerAll.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
		}else{
			Container = new TVclStackPanel(_this.ContainerAll.Column[0].This, "", 2, [[8, 4], [7, 5], [7, 5], [7, 5], [8, 4]]);
		}

		var Container_1 = new TVclStackPanel(Container.Column[0].This, "", 2, [[5, 7], [6, 6], [6, 6], [6, 6], [6, 6]]);
		Container_1.Row.This.style.marginBottom = '10px';

		var btnCreate = new TVclButton(Container_1.Column[0].This, "");
		btnCreate.Text = 'Create';
		btnCreate.VCLType = TVCLType.BS;
		btnCreate.Src = "image/16/add.png";
		// btnCreate.ImageLocation = "BeforeText";
		btnCreate.onClick = function () {
			if(txtStartDate.Text.trim() != ''){
				if(txtFinalDate.Text.trim() != ''){
					_this.SaveVoteData(parseInt(cbVote.Value), txtStartDate.Text.trim(), txtFinalDate.Text.trim(), 'CREATED');
					txtStartDate.Text = '';
					txtFinalDate.Text = '';
				}
				else{
					alert("It's necessary to record a final date");
				}
			}
			else{
				alert("It's necessary to record a start date");
			}
		}
		btnCreate.This.style.padding = '0px';
		btnCreate.This.style.paddingLeft = '10px';

		var cbVote = new TVclComboBox2(Container_1.Column[1].This, "txtVOTE");
		cbVote.VCLType = TVCLType.BS;
		var res = _this.getQuery(query);
		for (var i = 0; i < _this.VOTEList.length; i++) {
			var ComboItem = new TVclComboBoxItem();
			ComboItem.Value = _this.VOTEList[i].IDVOTE;
			ComboItem.Text = _this.VOTEList[i].VOTE_NAME;
			ComboItem.Tag = "";
			cbVote.AddItem(ComboItem);
		}
		cbVote.selectedIndex = 0;

		var Container_2 = new TVclStackPanel(Container.Column[0].This, "", 2, [[12, 12], [6, 6], [6, 6], [6, 6], [6, 6]]);
		Container_2.Row.This.style.marginBottom = '10px';

		var ContStartDate = new TVclStackPanel(Container_2.Column[0].This, "", 2, [[5, 7], [5, 7], [5, 7], [5, 7], [5, 7]]);
		ContStartDate.Row.This.style.marginBottom = '5px';
		var lblStartDate = new TVcllabel(ContStartDate.Column[0].This, "", TlabelType.H0);
		lblStartDate.This.style.paddingTop = '5px';
		lblStartDate.Text = 'Start Date';
		lblStartDate.VCLType = TVCLType.BS;
		var txtStartDate = new TVclTextBox(ContStartDate.Column[1].This, "lblStartDateVOTE");
		txtStartDate.Text = "";
		txtStartDate.VCLType = TVCLType.BS;
		$(txtStartDate.This).datetimepicker({dateFormat:'yy-mm-dd'});

		var ContFinalDate = new TVclStackPanel(Container_2.Column[1].This, "", 2, [[5, 7], [5, 7], [5, 7], [5, 7], [5, 7]]);
		ContFinalDate.Row.This.style.marginBottom = '5px';
		var lblFinalDate = new TVcllabel(ContFinalDate.Column[0].This, "", TlabelType.H0);
		lblFinalDate.This.style.paddingTop = '5px';
		lblFinalDate.Text = 'Final Date';
		lblFinalDate.VCLType = TVCLType.BS;
		var txtFinalDate = new TVclTextBox(ContFinalDate.Column[1].This, "lblFinalDateVOTE");
		txtFinalDate.Text = "";
		txtFinalDate.VCLType = TVCLType.BS;
		$(txtFinalDate.This).datetimepicker({dateFormat:'yy-mm-dd'});

		var ContainerList = new TVclStackPanel(_this.ContainerAll.Column[0].This, "", 2, [[8, 4], [7, 5], [7, 5], [7, 5], [8, 4]]);
		ContainerList.Column[1].This.style.display = 'none';
		_this.List = ContainerList.Column[1].This;

		var DateGrid = new TGrid(ContainerList.Column[0].This, '', '');

		var Col0 = new TColumn();
		Col0.Name = ""; 
		Col0.Caption = "ID";
		Col0.Index = 0; 
		Col0.EnabledEditor = false;
		Col0.DataType = SysCfg.DB.Properties.TDataType.String;
		Col0.ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.None;
		DateGrid.AddColumn(Col0);

		var Col1 = new TColumn();
		Col1.Name = ""; 
		Col1.Caption = "Start Date";
		Col1.Index = 0; 
		Col1.EnabledEditor = false;
		Col1.DataType = SysCfg.DB.Properties.TDataType.String;
		Col1.ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.None;
		DateGrid.AddColumn(Col1);

		var Col2 = new TColumn();
		Col2.Name = "Final Date";
		Col2.Caption = "Final Date";
		Col2.Index = 1;
		Col2.EnabledEditor = false;
		Col2.DataType = SysCfg.DB.Properties.TDataType.String;
		Col2.ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.None;
		DateGrid.AddColumn(Col2);
		// DateGrid.This.th
		DateGrid.This.children[0].style.backgroundColor = '#00a65a';
		DateGrid.This.children[0].style.textAlign = 'center';
		for(var i = 0 ; i < DateGrid.This.children[0].children[0].childElementCount; i++){
			DateGrid.This.children[0].children[0].children[i].style.textAlign = 'center';
		}
		DateGrid.This.style.textAlign = 'center';
		DateGrid.EnabledResizeColumn = false;
		_this.DateTable = DateGrid;

		_this.FillDate();

		var ContBtnCreated = new TVclStackPanel(ContainerList.Column[1].This, "", 1, [[12], [12], [12], [12], [12]]);
		ContBtnCreated.Row.This.style.marginBottom = '5px';
		var btnCreated = new TVclButton(ContBtnCreated.Column[0].This, "");
		btnCreated.Text = 'Created';
		btnCreated.VCLType = TVCLType.BS;
		btnCreated.This.style.width = '98px';
		btnCreated.Src = "image/16/Accept.png";
		// btnCreated.ImageLocation = "BeforeText";
		btnCreated.onClick = function () {
			_this.UpdateVoteData(parseInt(this.dataset.id), 'CREATED');
		}
		btnCreated.This.style.padding = '0px';
		btnCreated.This.style.paddingLeft = '10px';

		var ContBtnProgress = new TVclStackPanel(ContainerList.Column[1].This, "", 1, [[12], [12], [12], [12], [12]]);
		ContBtnProgress.Row.This.style.marginBottom = '5px';
		var btnProgress = new TVclButton(ContBtnProgress.Column[0].This, "");
		btnProgress.Text = 'Progress';
		btnProgress.VCLType = TVCLType.BS;
		btnProgress.This.style.width = '98px';
		btnProgress.Src = "image/16/Rules.png";
		// btnProgress.ImageLocation = "BeforeText";
		btnProgress.onClick = function () {
			_this.UpdateVoteData(parseInt(this.dataset.id), 'PROGRESS');
		}
		btnProgress.This.style.padding = '0px';
		btnProgress.This.style.paddingLeft = '10px';

		var ContBtnClose = new TVclStackPanel(ContainerList.Column[1].This, "", 1, [[12], [12], [12], [12], [12]]);
		ContBtnClose.Row.This.style.marginBottom = '5px';
		var btnClose = new TVclButton(ContBtnClose.Column[0].This, "");
		btnClose.Text = 'Close';
		btnClose.VCLType = TVCLType.BS;
		btnClose.This.style.width = '98px';
		btnClose.Src = "image/16/delete.png";
		// btnClose.ImageLocation = "BeforeText";
		btnClose.onClick = function () {
			_this.UpdateVoteData(parseInt(this.dataset.id), 'CLOSE');

		}
		btnClose.This.style.padding = '0px';
		btnClose.This.style.paddingLeft = '10px';

		var ContBtnCancel = new TVclStackPanel(ContainerList.Column[1].This, "", 1, [[12], [12], [12], [12], [12]]);
		ContBtnCancel.Row.This.style.marginBottom = '5px';
		var btnCancel = new TVclButton(ContBtnCancel.Column[0].This, "");
		btnCancel.Text = 'Cancel';
		btnCancel.VCLType = TVCLType.BS;
		btnCancel.This.style.width = '98px';
		btnCancel.Src = "image/16/cancel.png";
		// btnCancel._ImageLocation = "BeforeText";
		btnCancel.onClick = function () {
			_this.UpdateVoteData(parseInt(this.dataset.id), 'CANCEL');
		}
		btnCancel.This.style.padding = '0px';
		btnCancel.This.style.paddingLeft = '10px';
		
		_this.ElementList = {created: btnCreated, progress: btnProgress, close: btnClose, cancel: btnCancel};

		var ContUser = new TVclStackPanel(ContainerList.Column[1].This, "", 2, [[12, 12], [12, 12], [7, 5], [7, 5], [7, 5]]);
		ContUser.Row.This.style.marginTop = '20px';

		var query = "SELECT CMDBCI.IDCMDBCI,CMDBCI.CI_GENERICNAME  FROM CMDBCI, CMDBUSER WHERE CMDBCI.IDCMDBCI = CMDBUSER.IDCMDBCI";
		
		var cbInitGraphic = new TVclComboBox2(ContUser.Column[0].This, "txtInitGraphicVOTE");
		cbInitGraphic.This.style.marginBottom = '5px';
		cbInitGraphic.VCLType = TVCLType.BS;
		var res = _this.getQuery(query);
		var _arrayUser = _this.FormatObjectQuery(res.Result);
		for (var i = 0; i < _arrayUser.length; i++) {
			var ComboItem = new TVclComboBoxItem();
			ComboItem.Value = _arrayUser[i].IDCMDBCI;
			ComboItem.Text = _arrayUser[i].CI_GENERICNAME;
			ComboItem.Tag = "Test";
			cbInitGraphic.AddItem(ComboItem);
		}
		cbInitGraphic.selectedIndex = 0;

		var btnVote = new TVclButton(ContUser.Column[1].This, "");
		btnVote.Text = 'Vote';
		btnVote.VCLType = TVCLType.BS;
		btnVote.This.style.width = '98px';	
		btnVote.onClick = function () {
			var status = true;
			if(this.innerText == 'View'){
				status = true;
			}
			else if(this.innerText == 'Vote'){
				status = false;
			}
			_this.ShowPreview(parseInt(this.dataset.id), parseInt(cbInitGraphic.Value), cbInitGraphic.Text, status);
		}
		btnVote.This.style.padding = '0px';
		btnVote.This.style.paddingLeft = '10px';
		btnVote.This.style.float = "left";

		_this.ElementList = {created: btnCreated, 
						progress: btnProgress, 
						close: btnClose, 
						cancel: btnCancel, 
						vote: btnVote, 
						initGraphic: cbInitGraphic, 
						userForm: ContUser};
	} 
	catch (e) {
		SysCfg.Log.Methods.WriteLog("DemoTemporal10.js ItHelpCenter.Demo.TDemoTemporal10.prototype.Load", e);
	}
}

ItHelpCenter.Demo.TDemoTemporal10.prototype.ShowPreview = function (idVoteData, idUser, nameUser, status){
	var _this = this.TParent();
	try{
		_this.LoadProfiler();
		var indexData = Persistence.Votes.Methods.VOTEDATA_ListGetIndex(_this.VOTEDATAList, idVoteData);
		_this.ContainerAll.Row.This.style.display = 'none';
		_this.ContPreview.Row.This.style.display = 'block';

		_this.ContPreview.Row.This.style.position = 'relative';

		var ImageBack = new TVclImagen(_this.ContPreview.Column[0].This, "");
		ImageBack.This.style.marginLeft = "5px";
		ImageBack.Title = "Back";
		ImageBack.Src = "image/24/back.png";
		ImageBack.Cursor = "pointer";
		ImageBack.onClick = function () {
			_this.ContainerAll.Row.This.style.display = 'block';
			_this.ContPreview.Row.This.style.display = 'none';
			$(_this.ContPreview.Column[0].This).html('');		
		}
		ImageBack.This.style.marginBottom = '20px';

		var Container = new TVclStackPanel(_this.ContPreview.Column[0].This, 'VotesBasic_', 1, [[12], [12], [12], [12], [12]]);
		
		if(!Source.Menu.IsMobil){
			_this.ContPreview.Row.This.style.marginTop = '10px';
			ImageBack.This.style.marginTop = '10px';
			Container.Column[0].This.style.paddingRight = '20%';
			Container.Column[0].This.style.paddingLeft = '15%';
		}

		var VotesManager = new ItHelpCenter.VotesManager.TVotesManagerBasic(Container.Column[0].This, status, _this.VOTEDATAList[indexData].VOTEDATAUSERList, idUser, nameUser, _this.VOTEDATAList[indexData].IDVOTE);
		var itemVote = VotesManager.ItemVote;
		VotesManager.SaveCommentary = function(_Value){
			_this.SaveVoteDataUser(idVoteData, _Value);
		}
	} 
	catch (e) {
		SysCfg.Log.Methods.WriteLog("DemoTemporal10.js ItHelpCenter.Demo.TDemoTemporal10.prototype.ShowPreview", e);
	}
}

ItHelpCenter.Demo.TDemoTemporal10.prototype.SaveVoteData = function(idVoteData, startDate, finalDate, status){
	var _this = this.TParent();
	try{
		var VOTEDATA = new Persistence.Votes.Properties.TVOTEDATA();
		VOTEDATA.IDVOTE = idVoteData;
		if(navigator.userAgent.indexOf('Trident/') > -1  || navigator.userAgent.indexOf('MSIE ') > -1){
			VOTEDATA.VOTEDATA_STARTDATE = new Date(formatJSONDateTime(startDate));
			VOTEDATA.VOTEDATA_FINALDATE = new Date(formatJSONDateTime(finalDate));
		}
		else{
			VOTEDATA.VOTEDATA_STARTDATE = new Date(startDate);
			VOTEDATA.VOTEDATA_FINALDATE = new Date(finalDate);
		}
		VOTEDATA.VOTEDATA_STATUS = status;
		var success = Persistence.Votes.Methods.VOTEDATA_ADD(VOTEDATA);
		if(success){
			alert('Registration saved correctly');
			for (var i = 0; i < _this.DateTable._Rows.length; i++) {
				$(_this.DateTable._Rows[i].This).remove();
			}
			_this.FillDate();
			_this.List.style.display = 'none';
		}
	} 
	catch (e) {
		SysCfg.Log.Methods.WriteLog("DemoTemporal10.js ItHelpCenter.Demo.TDemoTemporal10.prototype.SaveVoteData", e);
	}
}

ItHelpCenter.Demo.TDemoTemporal10.prototype.SaveVoteDataUser = function (idVoteData, itemVote){
	var _this = this.TParent();
	try{
		var VOTEDATAUSER = new Persistence.Votes.Properties.TVOTEDATAUSER();
		VOTEDATAUSER.IDVOTEDATA = idVoteData;
		VOTEDATAUSER.IDCMDBCI = itemVote.IDCMDBCI;
		VOTEDATAUSER.IDVOTEOPTION = itemVote.IdNumberChoose
		VOTEDATAUSER.VOTEDATAUSER_NAME = itemVote.Name;
		VOTEDATAUSER.VOTEDATAUSER_COMMENTARY = itemVote.Commentary;
		var success = Persistence.Votes.Methods.VOTEDATAUSER_ADD(VOTEDATAUSER);
		if(success){
			_this.SearchVoteData (idVoteData)
		}
	} 
	catch (e) {
		SysCfg.Log.Methods.WriteLog("DemoTemporal10.js ItHelpCenter.Demo.TDemoTemporal10.prototype.SaveVoteDataUser", e);
	}
}

ItHelpCenter.Demo.TDemoTemporal10.prototype.UpdateVoteData = function(idVoteData, status){
	var _this = this.TParent();
	try{
		_this.LoadProfiler();

		var indexData = Persistence.Votes.Methods.VOTEDATA_ListGetIndex(_this.VOTEDATAList, idVoteData);

		var VOTEDATA = new Persistence.Votes.Properties.TVOTEDATA();
		VOTEDATA.IDVOTEDATA = idVoteData;
		VOTEDATA.IDVOTE = _this.VOTEDATAList[indexData].IDVOTE;
		VOTEDATA.VOTEDATA_STARTDATE = _this.VOTEDATAList[indexData].VOTEDATA_STARTDATE;
		VOTEDATA.VOTEDATA_FINALDATE = _this.VOTEDATAList[indexData].VOTEDATA_FINALDATE;
		VOTEDATA.VOTEDATA_STATUS = status;
		var success = Persistence.Votes.Methods.VOTEDATA_UPD(VOTEDATA);
		if(success){
			_this.StatusChange(idVoteData);
		}
	} 
	catch (e) {
		SysCfg.Log.Methods.WriteLog("DemoTemporal10.js ItHelpCenter.Demo.TDemoTemporal10.prototype.UpdateVoteData", e);
	}
}

ItHelpCenter.Demo.TDemoTemporal10.prototype.FillDate = function(){
	var _this = this.TParent();
	try{
		_this.LoadProfiler();
		var RowList = new Array();
		for (var i = 0; i < _this.VOTEDATAList.length; i++) {
			var NewRow = new TRow();
			var Cell = new TCell();
			Cell.Value = _this.VOTEDATAList[i].IDVOTEDATA;
			Cell.IndexColumn = 0;
			Cell.IndexRow = i;
			NewRow.AddCell(Cell);

			Cell = new TCell();
			Cell.Value = SysCfg.DateTimeMethods.ToDateTimefull(_this.VOTEDATAList[i].VOTEDATA_STARTDATE);
			Cell.IndexColumn = 0;
			Cell.IndexRow = i;
			NewRow.AddCell(Cell);

			Cell = new TCell();
			Cell.Value = SysCfg.DateTimeMethods.ToDateTimefull(_this.VOTEDATAList[i].VOTEDATA_FINALDATE);
			Cell.IndexColumn = 1;
			Cell.IndexRow = i;
			NewRow.AddCell(Cell);

			NewRow.This.setAttribute('data-id', _this.VOTEDATAList[i].IDVOTEDATA);
			RowList[i] = {row: NewRow.This}

			_this.DateTable.AddRow(NewRow);
			RowList[i].row.onclick = function () {
				for(var i = 0 ; i < this.parentNode.childElementCount ; i++){this.parentNode.children[i].style.backgroundColor = 'transparent'}
				this.style.backgroundColor = '#BCFFB0';
				_this.List.style.display = 'block';
				_this.StatusChange(parseInt(this.dataset.id));
				_this.SearchVoteData(parseInt(this.dataset.id));
				// #38ffa5
			}
		}
	} 
	catch (e) {
		SysCfg.Log.Methods.WriteLog("DemoTemporal10.js ItHelpCenter.Demo.TDemoTemporal10.prototype.FillDate", e);
	}
}

ItHelpCenter.Demo.TDemoTemporal10.prototype.StatusChange = function(idVoteData) {
	var _this = this.TParent();
	try{
		_this.LoadProfiler();
		var indexData = Persistence.Votes.Methods.VOTEDATA_ListGetIndex(_this.VOTEDATAList, idVoteData);

		_this.ElementList.created.This.setAttribute('data-id', idVoteData);
		_this.ElementList.progress.This.setAttribute('data-id', idVoteData);
		_this.ElementList.close.This.setAttribute('data-id', idVoteData);
		_this.ElementList.cancel.This.setAttribute('data-id', idVoteData);
		_this.ElementList.vote.This.setAttribute('data-id', idVoteData);

		switch ((_this.VOTEDATAList[indexData].VOTEDATA_STATUS).toLowerCase()) {
			case 'created':
				_this.ElementList.created.This.setAttribute('disabled', '');
				_this.ElementList.progress.This.removeAttribute('disabled');
				_this.ElementList.close.This.removeAttribute('disabled');
				_this.ElementList.cancel.This.removeAttribute('disabled');
				_this.ElementList.userForm.Row.This.style.display = 'block';
				break;
			case 'progress':
				_this.ElementList.created.This.removeAttribute('disabled');
				_this.ElementList.progress.This.setAttribute('disabled', '');
				_this.ElementList.close.This.removeAttribute('disabled');
				_this.ElementList.cancel.This.removeAttribute('disabled');
				_this.ElementList.userForm.Row.This.style.display = 'block';
				break;
			case 'close':
				_this.ElementList.created.This.removeAttribute('disabled');
				_this.ElementList.progress.This.removeAttribute('disabled');
				_this.ElementList.close.This.setAttribute('disabled', '');
				_this.ElementList.cancel.This.removeAttribute('disabled');
				_this.ElementList.userForm.Row.This.style.display = 'none';
				break;
			case 'cancel':
				_this.ElementList.created.This.removeAttribute('disabled');
				_this.ElementList.progress.This.removeAttribute('disabled');
				_this.ElementList.close.This.removeAttribute('disabled');
				_this.ElementList.cancel.This.setAttribute('disabled', '');
				_this.ElementList.userForm.Row.This.style.display = 'none';
				break;
		}
	} 
	catch (e) {
		SysCfg.Log.Methods.WriteLog("DemoTemporal10.js ItHelpCenter.Demo.TDemoTemporal10.prototype.StatusChange", e);
	}
}

ItHelpCenter.Demo.TDemoTemporal10.prototype.SearchVoteData = function(idVoteData) {
	var _this = this.TParent();
	try{
		_this.LoadProfiler();
		var indexData = Persistence.Votes.Methods.VOTEDATA_ListGetIndex(_this.VOTEDATAList, idVoteData);

		var band = false;
		for(var i = 0 ; i < _this.VOTEDATAList[indexData].VOTEDATAUSERList.length; i++){
			if(parseInt(_this.ElementList.initGraphic.Value) === _this.VOTEDATAList[indexData].VOTEDATAUSERList[i].IDCMDBCI){
				band = true;
			}
		}
		if(band){
			_this.ElementList.vote.Src = "image/16/monitor.png";
			_this.ElementList.vote.Text = 'View';
		}else{
			_this.ElementList.vote.Src = "image/16/comment.png";
			_this.ElementList.vote.Text = 'Vote';
		}
		
		_this.ElementList.initGraphic.onChange = function(){
			var band = false;
			for(var i = 0 ; i < _this.VOTEDATAList[indexData].VOTEDATAUSERList.length; i++){
				if(parseInt(this.value) === _this.VOTEDATAList[indexData].VOTEDATAUSERList[i].IDCMDBCI){
					band = true;
				}
			}
			if(band){
				_this.ElementList.vote.Src = "image/16/monitor.png";
				_this.ElementList.vote.Text = 'View';
			}else{
				_this.ElementList.vote.Src = "image/16/comment.png";
				_this.ElementList.vote.Text = 'Vote';
			}

		}
	} 
	catch (e) {
		SysCfg.Log.Methods.WriteLog("DemoTemporal10.js ItHelpCenter.Demo.TDemoTemporal10.prototype.SearchVoteData", e);
	}
}

ItHelpCenter.Demo.TDemoTemporal10.prototype.LoadProfiler = function () {
	var _this = this.TParent();
	try{
		_this.VotesProfiler.Fill();
		_this.VOTEList = _this.VotesProfiler.VOTEList;
		_this.VOTEOPTIONList = _this.VotesProfiler.VOTEOPTIONList;
		_this.VOTEDATAList = _this.VotesProfiler.VOTEDATAList;
		_this.VOTEDATAUSERList = _this.VotesProfiler.VOTEDATAUSERList;
	} 
	catch (e) {
		SysCfg.Log.Methods.WriteLog("DemoTemporal10.js ItHelpCenter.Demo.TDemoTemporal10.prototype.LoadProfiler", e);
	}
}

ItHelpCenter.Demo.TDemoTemporal10.prototype.getQuery = function (StrSQL) {
	try{
		Param = new SysCfg.Stream.Properties.TParam();
		var Message = null;
		var Status = null;
		try {
			Param.Inicialize();
			OpenDataSet = SysCfg.DB.SQL.Methods.OpenDataSet(StrSQL, Param.ToBytes());
			if (OpenDataSet.ResErr.NotError) {
				if (OpenDataSet.DataSet.RecordCount > 0) {
					Status = true;
					Message = 'OK';
				}
				else {
					OpenDataSet.ResErr.NotError = true;
					Status = false;
					Message = OpenDataSet.ResErr.Mesaje;
				}
			}
			else {
				Status = false;
				Message = OpenDataSet.ResErr.Mesaje;
			}
		}
		catch (error) {
			Status = false
			Message = error;
		}
		finally {
			Param.Destroy();
		}
		return ({ Result: OpenDataSet.DataSet, Status: Status, Message: Message });
	} 
	catch (e) {
		SysCfg.Log.Methods.WriteLog("DemoTemporal10.js ItHelpCenter.Demo.TDemoTemporal10.prototype.getQuery", e);
	}
}

ItHelpCenter.Demo.TDemoTemporal10.prototype.FormatObjectQuery = function (DataSetQuery) {
	try {
		var ArrayQuery = new Array();
		for (var i = 0; i < DataSetQuery.RecordCount; i++) { 
			var ObjectQuery = {}; 
			for (var x = 0; x < DataSetQuery.Records[i].Fields.length; x++) { 
				var Column = DataSetQuery.Records[i].Fields[x].FieldDef.FieldName; 
				var Value = DataSetQuery.Records[i].Fields[x].Value; 
				ObjectQuery[Column] = Value; 
			}
			ArrayQuery.push(ObjectQuery); 
		}
		return ArrayQuery; 
	} catch (e) {
		SysCfg.Log.Methods.WriteLog("DemoTemporal10.js ItHelpCenter.Demo.TDemoTemporal10.prototype.FormatObjectQuery", e);
	}
}