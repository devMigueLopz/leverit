﻿ItHelpCenter.Demo.TDemoTabControl = function (inObjectHtml, _this) {
    //************* INICIO TAB TABCONTROL ***********************************/// 
    var ContTabControl = new TVclStackPanel(inObjectHtml, "1", 1, [[12], [12], [12], [12], [12]]);
    ContTabControl.Row.This.style.marginTop = "20px";

    var TabControl1 = new TTabControl(ContTabControl.Column[0].This, "", ""); //CREA UN TAB CONTROL 
    var TabPag1 = new TTabPage(); //CREA UN TAB PAGE PARA AGREGARSE AL TABCONTROL 
    TabPag1.Name = "TabPag1"; //ASIGNA O EXTRAE EL NAME DEL TAB PAGE
    TabPag1.Caption = "Tab Pag1"; //ASIGNA O EXTRAE EL TEXTO QUE VA A MOSTRAR EN EL TAB
    TabPag1.Active = true; //ASIGNA O EXTRAE SI EL TABPAGE ES EL ACTIVO (SE ESTA MOSTRANDO)
    TabControl1.AddTabPages(TabPag1); //ADICIONA UN TABPAGE AL TABCONTROL

    var TabPag2 = new TTabPage();
    TabPag2.Name = "TabPag2";
    TabPag2.Caption = "Tab Pag2";
    TabControl1.AddTabPages(TabPag2);

    var TabPag3 = new TTabPage();
    TabPag3.Name = "TabPag3";
    TabPag3.Caption = "Tab Pag3";
    TabControl1.AddTabPages(TabPag3);

    var lblTabPage1 = new TVcllabel(TabPag1.Page, "", TlabelType.H0); //AGREGA CONTENIDO AL TAB PAGE 
    lblTabPage1.Text = "Contenido Page 1";
    lblTabPage1.VCLType = TVCLType.BS;

    var lblTabPage2 = new TVcllabel(TabPag2.Page, "", TlabelType.H0);
    lblTabPage2.Text = "Contenido Page 2";
    lblTabPage2.VCLType = TVCLType.BS;

    var lblTabPage3 = new TVcllabel(TabPag3.Page, "", TlabelType.H0);
    lblTabPage3.Text = "Contenido Page 3";
    lblTabPage3.VCLType = TVCLType.BS;
    //************* FIN TAB TABCONTROL ***********************************///
}