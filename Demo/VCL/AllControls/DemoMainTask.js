ItHelpCenter.Demo.TDemoMainTask = function (inObjectHtml, inCallback) {
	this.TParent = function(){
		return this;
	}.bind(this);
	this.ObjectHtml = inObjectHtml;
	var _this = this.TParent();
	_this.PManagement = new Persistence.ProjectManagement.TProjectManagementProfiler();
	this.isInteger = function (num) {
		return (num ^ 0) === num;
	}

	_this.Mythis = "PMMainTask";
	UsrCfg.Traslate.GetLangText(_this.Mythis, "btnCreate.Text" , "Create");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "btnUpdate.Text", "Update");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "btnHoliday.Text", "Holiday");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "btnPreview.Text", "Preview");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "lblName.Text", "NAME");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "lblDuration.Text", "DURATION");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "lblOwner.Text", "OWNER");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "txtStartDate.Text", "START DATE");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "lblEndDate.Text", "END DATE");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "btnSave.Text", "Save");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "btnUpdate.Text", "Save");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "btnDeleteHoliday.Text", "Delete Holiday");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "ListBoxItem_Weekend.Text", "Non-working weekend");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "ListBoxItem_Sunday.Text", "Sunday not working");

	UsrCfg.Traslate.GetLangText(_this.Mythis, "ID", "ID");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "NAME", "NAME");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "OWNER", "OWNER");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "STARTDATE", "START DATE");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "ENDDATE", "END DATE");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "STATE", "STATE");

	UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1", "Select a project");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2", "There are tasks within the project that will not affect the new changes");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3", "Preview MainTask");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines4", "MainTask");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines5", "The record is not saved");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines6", "Holiday");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines7", "You want to add this date as a holiday?");

	UsrCfg.Traslate.GetLangText(_this.Mythis, "DURATION1", "Day");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "DURATION2", "Days");

	_this.InicializeComponent();
}

ItHelpCenter.Demo.TDemoMainTask.prototype.InicializeComponent = function () {
	var _this = this.TParent();
	_this.idTemp = 0
	_this.PManagement.Fill();
	_this.MAINTASKList = _this.PManagement.PMMAINTASKList;

	var Container = new TVclStackPanel(_this.ObjectHtml, "", 2, [[8, 4], [8, 4], [8, 4], [8, 4], [8, 4]]);
	var DateGrid = new TGrid(Container.Column[0].This, '', '');
	var Col0 = new TColumn();
	Col0.Name = ""; 
	Col0.Caption = UsrCfg.Traslate.GetLangText(_this.Mythis, "ID");
	Col0.Index = 0; 
	Col0.EnabledEditor = false;
	Col0.DataType = SysCfg.DB.Properties.TDataType.String;
	Col0.ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.None;
	DateGrid.AddColumn(Col0);

	var Col1 = new TColumn();
	Col1.Name = ""; 
	Col1.Caption = UsrCfg.Traslate.GetLangText(_this.Mythis, "NAME");
	Col1.Index = 1; 
	Col1.EnabledEditor = false;
	Col1.DataType = SysCfg.DB.Properties.TDataType.String;
	Col1.ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.None;
	DateGrid.AddColumn(Col1);

	var Col2 = new TColumn();
	Col2.Name = ""; 
	Col2.Caption = UsrCfg.Traslate.GetLangText(_this.Mythis, "OWNER");
	Col2.Index = 2; 
	Col2.EnabledEditor = false;
	Col2.DataType = SysCfg.DB.Properties.TDataType.String;
	Col2.ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.None;
	DateGrid.AddColumn(Col2);

	var Col3 = new TColumn();
	Col3.Name = ""; 
	Col3.Caption = UsrCfg.Traslate.GetLangText(_this.Mythis, "STARTDATE");
	Col3.Index = 3; 
	Col3.EnabledEditor = false;
	Col3.DataType = SysCfg.DB.Properties.TDataType.String;
	Col3.ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.None;
	DateGrid.AddColumn(Col3);

	var Col4 = new TColumn();
	Col4.Name = ""; 
	Col4.Caption = UsrCfg.Traslate.GetLangText(_this.Mythis, "ENDDATE");
	Col4.Index = 4; 
	Col4.EnabledEditor = false;
	Col4.DataType = SysCfg.DB.Properties.TDataType.String;
	Col4.ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.None;
	DateGrid.AddColumn(Col4);

	var Col5 = new TColumn();
	Col5.Name = ""; 
	Col5.Caption = UsrCfg.Traslate.GetLangText(_this.Mythis, "STATE");
	Col5.Index = 4; 
	Col5.EnabledEditor = false;
	Col5.DataType = SysCfg.DB.Properties.TDataType.String;
	Col5.ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.None;
	DateGrid.AddColumn(Col5);

	DateGrid.This.children[0].style.backgroundColor = '#00a65a';
	DateGrid.This.children[0].style.textAlign = 'center';
	for(var i = 0 ; i < DateGrid.This.children[0].children[0].childElementCount; i++){
		DateGrid.This.children[0].children[0].children[i].style.textAlign = 'center';
	}
	DateGrid.This.style.textAlign = 'center';
	DateGrid.EnabledResizeColumn = false;
	var RowList = new Array();

	for(var i = 0 ; i < _this.MAINTASKList.length; i++){
		var NewRow = new TRow();

		var Cell = new TCell();
		Cell.Value = _this.MAINTASKList[i].IDPMMAINTASK;
		Cell.IndexColumn = 0;
		Cell.IndexRow = i;
		NewRow.AddCell(Cell);

		Cell = new TCell();
		Cell.Value = _this.MAINTASKList[i].MAINTASK_NAME;
		Cell.IndexColumn = 1;
		Cell.IndexRow = i;
		NewRow.AddCell(Cell);

		Cell = new TCell();
		Cell.Value = _this.MAINTASKList[i].MAINTASK_OWNER;
		Cell.IndexColumn = 2;
		Cell.IndexRow = i;
		NewRow.AddCell(Cell);

		Cell = new TCell();
		Cell.Value = _this.ToDateTimeMeridian(new Date(_this.MAINTASKList[i].MAINTASK_STARTDATE));
		Cell.IndexColumn = 3;
		Cell.IndexRow = i;
		NewRow.AddCell(Cell);

		Cell = new TCell();
		Cell.Value = _this.ToDateTimeMeridian(new Date(_this.MAINTASKList[i].MAINTASK_ENDDATE));
		Cell.IndexColumn = 4;
		Cell.IndexRow = i;
		NewRow.AddCell(Cell);

		Cell = new TCell();
		Cell.Value = _this.MAINTASKList[i].MAINTASK_STATE;
		Cell.IndexColumn = 5;
		Cell.IndexRow = i;
		NewRow.AddCell(Cell);

		NewRow.This.setAttribute('data-id', _this.MAINTASKList[i].IDPMMAINTASK);
		RowList[i] = {row: NewRow.This}
		DateGrid.AddRow(NewRow);
		RowList[i].row.onclick = function () {
			for(var i = 0 ; i < this.parentNode.childElementCount ; i++){
				this.parentNode.children[i].style.backgroundColor = 'transparent';
			}
			this.style.backgroundColor = '#BCFFB0';
			_this.idTemp = parseInt(this.dataset.id)
		}
	}
	_this.DateTable = DateGrid;

	if (_this.MAINTASKList.length === 0) {
		var ContBtnDemo = new TVclStackPanel(Container.Column[1].This, "", 1, [[12], [12], [12], [12], [12]]);
		ContBtnDemo.Row.This.style.marginBottom = '5px';
		var btnDemo = new TVclButton(ContBtnDemo.Column[0].This, "");
		btnDemo.Text = UsrCfg.Traslate.GetLangText(_this.Mythis,"btnCreate.Text");
		btnDemo.VCLType = TVCLType.BS;
		btnDemo.This.style.width = '98px';
		btnDemo.Src = "image/16/Create-ticket.png";
		btnDemo.onClick = function () {
			if (_this.MAINTASKList.length === 0) {
				_this.PManagement.PMCreateAll();
				btnDemo.This.parentElement.style.display = 'none';
				_this.updateTable();
				alert('Records created in the database');
			}
			else {
				alert('Records already exist in the Database');
			}
		}
		btnDemo.This.style.padding = '0px';
		btnDemo.This.style.paddingLeft = '10px';
	}
	var ContBtnCreate = new TVclStackPanel(Container.Column[1].This, "", 1, [[12], [12], [12], [12], [12]]);
	ContBtnCreate.Row.This.style.marginBottom = '5px';
	var btnCreate = new TVclButton(ContBtnCreate.Column[0].This, "");
	btnCreate.Text = UsrCfg.Traslate.GetLangText(_this.Mythis,"btnCreate.Text");
	btnCreate.VCLType = TVCLType.BS;
	btnCreate.This.style.width = '98px';
	btnCreate.Src = "image/16/add.png";
	btnCreate.onClick = function () {
		// _this.CreateMainTask();
		_this.MainTaskForm("", "", "", "", "" ,"", "save");
	}
	btnCreate.This.style.padding = '0px';
	btnCreate.This.style.paddingLeft = '10px';

	var ContBtnUpdate = new TVclStackPanel(Container.Column[1].This, "", 1, [[12], [12], [12], [12], [12]]);
	ContBtnUpdate.Row.This.style.marginBottom = '5px';
	var btnUpdate = new TVclButton(ContBtnUpdate.Column[0].This, "");
	btnUpdate.Text = UsrCfg.Traslate.GetLangText(_this.Mythis,"btnUpdate.Text");
	btnUpdate.VCLType = TVCLType.BS;
	btnUpdate.This.style.width = '98px';
	btnUpdate.Src = "image/16/edit.png";
	btnUpdate.onClick = function () {
		if(_this.idTemp != 0){
			var index = Persistence.ProjectManagement.Methods.PMMAINTASK_ListGetIndex(_this.MAINTASKList, _this.idTemp);
			var MainTask =_this.MAINTASKList[index];
			_this.MainTaskForm(MainTask.IDPMMAINTASK, MainTask.MAINTASK_NAME, MainTask.MAINTASK_DURATION, MainTask.MAINTASK_OWNER, 
				_this.ToDateTimeMeridian(new Date(MainTask.MAINTASK_STARTDATE)), _this.ToDateTimeMeridian(new Date(MainTask.MAINTASK_ENDDATE)), "update");
		}
		else{
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1"));
		}
	}
	btnUpdate.This.style.padding = '0px';
	btnUpdate.This.style.paddingLeft = '10px';

	var ContBtnHoliday = new TVclStackPanel(Container.Column[1].This, "", 1, [[12], [12], [12], [12], [12]]);
	ContBtnHoliday.Row.This.style.marginBottom = '5px';
	var btnHoliday = new TVclButton(ContBtnHoliday.Column[0].This, "");
	btnHoliday.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnHoliday.Text");
	btnHoliday.VCLType = TVCLType.BS;
	btnHoliday.This.style.width = '98px';
	btnHoliday.Src = "image/icon-gantt/calendar(16).png";
	btnHoliday.onClick = function () {
		if(_this.idTemp != 0){
			var index = Persistence.ProjectManagement.Methods.PMMAINTASK_ListGetIndex(_this.MAINTASKList, _this.idTemp);
			var MainTask =_this.MAINTASKList[index];
			if(_this.MAINTASKList[index].PMDETAILTASKList.length > 0){
				alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2"));
			}
			_this.HolidayForm(MainTask.IDPMMAINTASK);
		}
		else{
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1"));
		}
	}
	btnHoliday.This.style.padding = '0px';
	btnHoliday.This.style.paddingLeft = '10px';

	var ContBtnPreview = new TVclStackPanel(Container.Column[1].This, "", 1, [[12], [12], [12], [12], [12]]);
	ContBtnPreview.Row.This.style.marginBottom = '5px';
	var btnPreview = new TVclButton(ContBtnPreview.Column[0].This, "");
	btnPreview.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnPreview.Text");
	btnPreview.VCLType = TVCLType.BS;
	btnPreview.This.style.width = '98px';
	btnPreview.Src = "image/16/Computer.png";
	btnPreview.onClick = function () {
		if(_this.idTemp != 0){
			var Modal = new TVclModal(document.body, null, "");
			Modal.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3"));
			var ContainerPreview =new TVclStackPanel(Modal.Body.This, "ModalDemoS", 1, [[12], [12], [12], [12], [12]]);
			ContainerPreview.Row.This.style.marginRight = '5px';
			_this.importarScript(SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoPMGantt.js", function () {
				var Demo = new ItHelpCenter.Demo.TDemoPMGantt(ContainerPreview.Column[0].This, null, _this.idTemp);
			}, function (e) {
				//script de error
			});
			Modal.ShowModal();
			Modal.FunctionAfterClose = function () {
				// $('#ui-datepicker-div').remove()
				$(this.This.This).remove();
			}
			// _this.idTemp = 0;
		}
		else{
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1"));
		}
	}
	btnPreview.This.style.padding = '0px';
	btnPreview.This.style.paddingLeft = '10px';
}

ItHelpCenter.Demo.TDemoMainTask.prototype.MainTaskForm = function (id, name, duration, owner, startDate, endDate, opc) {
	var _this = this.TParent();
	var Modal = new TVclModal(document.body, _this.ObjectHtml, "");
	Modal.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines4"));

	var Container = new TVclStackPanel(Modal.Body.This, 'MainTask_Form', 1, [[12], [12], [12], [12], [12]]);
	
	var ContTextName = new TVclStackPanel(Container.Column[0].This, "", 2, [[4, 8], [2, 10], [2, 10], [2, 10], [2, 8]]);
	ContTextName.Row.This.style.marginBottom = '10px';
	var lblName = new TVcllabel(ContTextName.Column[0].This, "", TlabelType.H0);
	lblName.Text =  UsrCfg.Traslate.GetLangText(_this.Mythis, "lblName.Text");
	lblName.VCLType = TVCLType.BS;
	var txtName = new TVclTextBox(ContTextName.Column[1].This, "");
	txtName.Text = name;
	txtName.VCLType = TVCLType.BS;

	var Container_1 = new TVclStackPanel(Container.Column[0].This, "", 2, [[12, 12], [6, 6], [6, 6], [6, 6], [6, 6]]);

	var ContTextDuration = new TVclStackPanel(Container_1.Column[0].This, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	ContTextDuration.Row.This.style.marginBottom = '10px';
	var lblDuration = new TVcllabel(ContTextDuration.Column[0].This, "", TlabelType.H0);
	lblDuration.Text =  UsrCfg.Traslate.GetLangText(_this.Mythis, "lblDuration.Text");
	lblDuration.VCLType = TVCLType.BS;
	var txtDuration = new TVclTextBox(ContTextDuration.Column[1].This, "");
	txtDuration.Text = duration;
	txtDuration.VCLType = TVCLType.BS;
	txtDuration.This.onchange = function(){
		var startDate = new Date(txtStartDate.Text);
		if(_this.isInteger(parseInt(txtDuration.Text.trim()))){
			if(parseInt(txtDuration.Text.trim()) > 1){
				txtDuration.Text = parseInt(txtDuration.Text.trim()) + ' ' + UsrCfg.Traslate.GetLangText(_this.Mythis, "DURATION2");
			}else{
				txtDuration.Text = parseInt(txtDuration.Text.trim()) + ' ' + UsrCfg.Traslate.GetLangText(_this.Mythis, "DURATION1");
			}
			if(_this.isInteger(startDate.getDate())){
				if(parseInt(txtDuration.Text.trim()) > 0){
					// var startDateTemp = new Date(startDate.getTime() + (1000*60*60*12));
					// var dateTemp = _this.CalculateEndDate(MAINTASK, PMHoliday, startDateTemp, parseInt(txtDuration.Text.trim()));
					var dateTemp = new Date(startDate.getTime() + ((1000*60*60*12)*parseInt(txtDuration.Text.trim())));
					txtEndDate.Text = _this.ToDateTimeMeridian(new Date(dateTemp));
				}else{
					txtEndDate.Text = txtStartDate.Text;
				}
			}
		}
		// else{
		// 	txtDuration.Text = "1 " + UsrCfg.Traslate.GetLangText(_this.Mythis, "DURATION1");
		// }
	}

	var ContTextOwner = new TVclStackPanel(Container_1.Column[1].This, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	ContTextOwner.Row.This.style.marginBottom = '10px';
	var lblOwner = new TVcllabel(ContTextOwner.Column[0].This, "", TlabelType.H0);
	lblOwner.Text =  UsrCfg.Traslate.GetLangText(_this.Mythis, "lblOwner.Text");
	lblOwner.VCLType = TVCLType.BS;
	var txtOwner = new TVclTextBox(ContTextOwner.Column[1].This, "");
	txtOwner.Text = owner;
	txtOwner.VCLType = TVCLType.BS;
	// $(txtOwner.This).timepicker();


	var Container_3 = new TVclStackPanel(Container.Column[0].This, "", 2, [[12, 12], [6, 6], [6, 6], [6, 6], [6, 6]]);
	Container_3.Row.This.style.marginBottom = '10px';

	var ContStartDate = new TVclStackPanel(Container_3.Column[0].This, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	ContStartDate.Row.This.style.marginBottom = '10px';
	var lblStartDate = new TVcllabel(ContStartDate.Column[0].This, "", TlabelType.H0);
	lblStartDate.This.style.paddingTop = '5px';
	lblStartDate.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "txtStartDate.Text");
	lblStartDate.VCLType = TVCLType.BS;
	var txtStartDate = new TVclTextBox(ContStartDate.Column[1].This, "txtStartDatePMGantt");
	txtStartDate.Text = startDate;
	txtStartDate.VCLType = TVCLType.BS;
	txtStartDate.This.onchange = function(){
		var startDate = new Date(txtStartDate.Text);
		if(_this.isInteger(parseInt(txtDuration.Text.trim()))){
			if(_this.isInteger(startDate.getDate())){
				if(parseInt(txtDuration.Text.trim()) > 0){
					var startDateTemp = new Date(startDate.getTime() + ((1000*60*60*12)*parseInt(txtDuration.Text.trim())));
					// var dateTemp = _this.CalculateEndDate(MAINTASK, PMHoliday, startDateTemp, parseInt(txtDuration.Text.trim()));
					txtEndDate.Text = _this.ToDateTimeMeridian(new Date(startDateTemp));
				}else{
					txtEndDate.Text = txtStartDate.Text;
				}
			}
		}
	}
	$(txtStartDate.This).datepicker({dateFormat:"yy/mm/dd", timeFormat:'hh:mm tt', hour: 7, minute:30});
	
	var ContEndDate = new TVclStackPanel(Container_3.Column[1].This, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	ContEndDate.Row.This.style.marginBottom = '5px';
	var lblEndDate = new TVcllabel(ContEndDate.Column[0].This, "", TlabelType.H0);
	lblEndDate.This.style.paddingTop = '5px';
	lblEndDate.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblEndDate.Text");
	lblEndDate.VCLType = TVCLType.BS;
	var txtEndDate = new TVclTextBox(ContEndDate.Column[1].This, "txtFinalDatePMGantt");
	txtEndDate.Text = endDate;
	txtEndDate.VCLType = TVCLType.BS;
	txtEndDate.This.onchange = function(){
		var startDate = new Date(txtStartDate.Text);
		var endDate = new Date(txtEndDate.Text);
		if(_this.isInteger(startDate.getDate()) && _this.isInteger(endDate.getDate())){
			// txtDuration.Text = _this.CalculateDuration(MAINTASK, PMHoliday, startDate, endDate);
			var diff = (endDate.getTime() + (1000*60*60*24))  - startDate.getTime();
			txtDuration.Text = diff/(1000*60*60*24);
			if(parseInt(txtDuration.Text.trim()) > 1){
				txtDuration.Text = parseInt(txtDuration.Text.trim()) + ' ' + UsrCfg.Traslate.GetLangText(_this.Mythis, "DURATION2");
			}else{
				txtDuration.Text = parseInt(txtDuration.Text.trim()) + ' ' + UsrCfg.Traslate.GetLangText(_this.Mythis, "DURATION1");
			}
		}
	}
	$(txtEndDate.This).datepicker({dateFormat:"yy/mm/dd", timeFormat:'hh:mm tt', hour: 19, minute:30});

	var Container_4 = new TVclStackPanel(Container.Column[0].This, "", 2, [[12, 12], [6, 6], [6, 6], [6, 6], [6, 6]]);
	Container_4.Row.This.style.marginBottom = '10px';

	// var ContStartTime = new TVclStackPanel(Container_4.Column[0].This, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	// ContStartTime.Row.This.style.marginBottom = '10px';
	// var lblStartTime = new TVcllabel(ContStartTime.Column[0].This, "", TlabelType.H0);
	// lblStartTime.This.style.paddingTop = '5px';
	// lblStartTime.Text = 'START TIME';
	// lblStartTime.VCLType = TVCLType.BS;
	// var txtStartTime = new TVclTextBox(ContStartTime.Column[1].This, "txtStartTimePMGantt");
	// txtStartTime.Text = "";
	// txtStartTime.VCLType = TVCLType.BS;
	// $(txtStartTime.This).timepicker({timeFormat:'hh:mm tt'});

	// var ContEndTime = new TVclStackPanel(Container_4.Column[1].This, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	// ContEndTime.Row.This.style.marginBottom = '5px';
	// var lblEndTime = new TVcllabel(ContEndTime.Column[0].This, "", TlabelType.H0);
	// lblEndTime.This.style.paddingTop = '5px';
	// lblEndTime.Text = "END TIME";
	// lblEndTime.VCLType = TVCLType.BS;
	// var txtEndTime = new TVclTextBox(ContEndTime.Column[1].This, "txtFinalDatePMGantt");
	// txtEndTime.Text = "";
	// txtEndTime.VCLType = TVCLType.BS;
	// $(txtEndTime.This).timepicker({timeFormat:'hh:mm tt'});


	if(opc == 'save'){
		var ContBtnSave = new TVclStackPanel(Container.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
		ContBtnSave.Row.This.style.marginBottom = '10px';
		var btnSave= new TVclButton(ContBtnSave.Column[0].This, "");
		btnSave.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnSave.Text");
		btnSave.VCLType = TVCLType.BS;
		btnSave.This.style.width = '98px';
		btnSave.Src = "image/16/Accept.png";
		btnSave.onClick = function () {
			var PMMAINTASK = new Persistence.ProjectManagement.Properties.TPMMAINTASK();
			PMMAINTASK.MAINTASK_NAME = txtName.Text.trim();
			PMMAINTASK.MAINTASK_OWNER = txtOwner.Text.trim();
			PMMAINTASK.MAINTASK_DURATION = txtDuration.Text.trim();
			PMMAINTASK.MAINTASK_STARTDATE = new Date(txtStartDate.Text.trim());
			PMMAINTASK.MAINTASK_ENDDATE = new Date(txtEndDate.Text.trim());
			PMMAINTASK.MAINTASK_STATE = true;

		    PMMAINTASK.MAINTASK_WEEKENDHOLIDAY = false;
		    PMMAINTASK.MAINTASK_SUNDAYHOLIDAY = false;
		    PMMAINTASK.MAINTASK_SCALEUNITS = "week";
		    PMMAINTASK.MAINTASK_SCALECOUNTER = 1;

			var success = Persistence.ProjectManagement.Methods.PMMAINTASK_ADD(PMMAINTASK);
			if(success){
				_this.updateTable();
				Modal.CloseModal();
				$(Modal.This.This).remove();
				$('#ui-datepicker-div').remove();
			}
			else{
				alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines5"));
			}
		}
		btnSave.This.style.float = 'right';
		btnSave.This.style.padding = '0px';
		btnSave.This.style.paddingLeft = '10px';
	}
	else if(opc == 'update'){
		var ContBtnUpdate = new TVclStackPanel(Container.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
		ContBtnUpdate.Row.This.style.marginBottom = '10px';
		var btnUpdate = new TVclButton(ContBtnUpdate.Column[0].This, "");
		btnUpdate.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnUpdate.Text");
		btnUpdate.VCLType = TVCLType.BS;
		btnUpdate.This.style.width = '98px';
		btnUpdate.Src = "image/16/Accept.png";
		btnUpdate.onClick = function () {
			var index = Persistence.ProjectManagement.Methods.PMMAINTASK_ListGetIndex(_this.MAINTASKList, id);
			var PMMAINTASK =_this.MAINTASKList[index];
			PMMAINTASK.MAINTASK_NAME = txtName.Text.trim();
			PMMAINTASK.MAINTASK_OWNER = txtOwner.Text.trim();
			PMMAINTASK.MAINTASK_DURATION = txtDuration.Text.trim();
			PMMAINTASK.MAINTASK_STARTDATE = new Date(txtStartDate.Text.trim());
			PMMAINTASK.MAINTASK_ENDDATE = new Date(txtEndDate.Text.trim());			
			var success = Persistence.ProjectManagement.Methods.PMMAINTASK_UPD(PMMAINTASK);
			if(success){
				_this.updateTable();
				Modal.CloseModal();
				$(Modal.This.This).remove();
				$('#ui-datepicker-div').remove();
			}
			else{
				alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines5"));
			}
		}
		btnUpdate.This.style.float = 'right';
		btnUpdate.This.style.padding = '0px';
		btnUpdate.This.style.paddingLeft = '10px';
	}

	

	Modal.ShowModal();
	Modal.FunctionAfterClose = function () {
		// $('#ui-datepicker-div').remove()
		$(this.This.This).remove();
	}
}

ItHelpCenter.Demo.TDemoMainTask.prototype.updateTable = function(){
	var _this = this.TParent();
		for (var i = 0; i < _this.DateTable._Rows.length; i++) {
			$(_this.DateTable._Rows[i].This).remove();
		}
		var RowList = new Array();
		_this.PManagement.Fill();
		_this.MAINTASKList = _this.PManagement.PMMAINTASKList;
		for(var i = 0 ; i < _this.MAINTASKList.length; i++){
			var NewRow = new TRow();

			var Cell = new TCell();
			Cell.Value = _this.MAINTASKList[i].IDPMMAINTASK;
			Cell.IndexColumn = 0;
			Cell.IndexRow = i;
			NewRow.AddCell(Cell);

			Cell = new TCell();
			Cell.Value = _this.MAINTASKList[i].MAINTASK_NAME;
			Cell.IndexColumn = 1;
			Cell.IndexRow = i;
			NewRow.AddCell(Cell);

			Cell = new TCell();
			Cell.Value = _this.MAINTASKList[i].MAINTASK_OWNER;
			Cell.IndexColumn = 2;
			Cell.IndexRow = i;
			NewRow.AddCell(Cell);

			Cell = new TCell();
			Cell.Value = _this.ToDateTimeMeridian(new Date(_this.MAINTASKList[i].MAINTASK_STARTDATE));
			Cell.IndexColumn = 3;
			Cell.IndexRow = i;
			NewRow.AddCell(Cell);

			Cell = new TCell();
			Cell.Value = _this.ToDateTimeMeridian(new Date(_this.MAINTASKList[i].MAINTASK_ENDDATE));
			Cell.IndexColumn = 4;
			Cell.IndexRow = i;
			NewRow.AddCell(Cell);

			Cell = new TCell();
			Cell.Value = _this.MAINTASKList[i].MAINTASK_STATE;
			Cell.IndexColumn = 5;
			Cell.IndexRow = i;
			NewRow.AddCell(Cell);

			NewRow.This.setAttribute('data-id', _this.MAINTASKList[i].IDPMMAINTASK);
			RowList[i] = {row: NewRow.This}
			_this.DateTable.AddRow(NewRow);
			RowList[i].row.onclick = function () {
				for(var i = 0 ; i < this.parentNode.childElementCount ; i++){
					this.parentNode.children[i].style.backgroundColor = 'transparent';
				}
				this.style.backgroundColor = '#BCFFB0';
				_this.idTemp = parseInt(this.dataset.id)
			}
		}
	}

ItHelpCenter.Demo.TDemoMainTask.prototype.HolidayForm = function (id) {
	var _this = this.TParent();
	var Modal = new TVclModal(document.body, _this.ObjectHtml, "");
	Modal.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines6"));

	var index = Persistence.ProjectManagement.Methods.PMMAINTASK_ListGetIndex(_this.MAINTASKList, id);
	var MainTask =_this.MAINTASKList[index];

	var Container = new TVclStackPanel(Modal.Body.This, 'Holiday_Form', 1, [[12], [12], [12], [12], [12]]);
	var ContainerDate = new TVclStackPanel(Container.Column[0].This, '', 2, [[12,12], [6,6], [6,6], [6,6], [6,6]]);
	ContainerDate.Column[0].This.id = "DatepickerHoliday";
	ContainerDate.Column[0].This.style.display = 'flex';
	ContainerDate.Column[0].This.style.justifyContent = 'center';
	ContainerDate.Column[0].This.marginBottom = "15px";
	
	var PMHoliday = MainTask.PMHOLIDAYList;

	var InitCalendar = function(){
		if(MainTask.MAINTASK_WEEKENDHOLIDAY){
			$("#DatepickerHoliday").datepicker({
				beforeShowDay: noWeekendsAndHolidays, 
				onSelect: function(date) { 
					SelectDate(date)
				}
			});
		}
		else if(MainTask.MAINTASK_SUNDAYHOLIDAY){
			$("#DatepickerHoliday").datepicker({
				beforeShowDay: noSundayAndHolidays, 
				onSelect: function(date) { 
					SelectDate(date)
				}
			});
		}
		else{
			$("#DatepickerHoliday").datepicker({
				beforeShowDay: holidayDates, 
				onSelect: function(date) { 
					SelectDate(date)
				}
			});
		}
	}
	InitCalendar();

	var SelectDate = function(date){
		var answer = confirm(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines7")); // ¿desea agregar esta fecha como feriado?
		if (answer) {
			var PMHOLIDAY = new Persistence.ProjectManagement.Properties.TPMHOLIDAY();
			PMHOLIDAY.IDPMMAINTASK = parseInt(id);
			PMHOLIDAY.HOLIDAY_DATE = new Date(date);
			var success = Persistence.ProjectManagement.Methods.PMHOLIDAY_ADD(PMHOLIDAY);
			if(success){
				// natDays.push(new Date(date));
				_this.PManagement.Fill();
				_this.MAINTASKList = _this.PManagement.PMMAINTASKList;
				index = Persistence.ProjectManagement.Methods.PMMAINTASK_ListGetIndex(_this.MAINTASKList, id);
				MainTask = _this.MAINTASKList[index];
				PMHoliday = MainTask.PMHOLIDAYList;
			}
			if(ListBox_Weekend.ListBoxItems[0].Checked ){
				$("#DatepickerHoliday").datepicker("destroy");
				$('#DatepickerHoliday').datepicker({
					beforeShowDay: noWeekendsAndHolidays,/*$.datepicker.noWeekends*/
					onSelect: function(date) { 
						SelectDate(date)
					}
				});
			}
			else if(ListBox_Sunday.ListBoxItems[0].Checked){
				$("#DatepickerHoliday").datepicker("destroy");
				$('#DatepickerHoliday').datepicker({
					beforeShowDay: noSundayAndHolidays, /*function(date){ var day = date.getDay(); return [(day != 0), ''];} */
					onSelect: function(date) { 
						SelectDate(date)
					}
				});
			}
			else{
				$("#DatepickerHoliday").datepicker("destroy");
				$('#DatepickerHoliday').datepicker({
					beforeShowDay: holidayDates, /*function(date){ var day = date.getDay(); return [(day != 0), ''];} */
					onSelect: function(date) { 
						SelectDate(date)
					}
				});
			}
			cbDateHoliday.ClearItems();
			for(var i = 0 ; i < PMHoliday.length; i++){
				var ComboItem = new TVclComboBoxItem();
				ComboItem.Value = PMHoliday[i].IDPMHOLIDAY;
				ComboItem.Text = _this.ToDateTimeMeridian(new Date(PMHoliday[i].HOLIDAY_DATE));
				ComboItem.Tag = "TestDate";
				cbDateHoliday.AddItem(ComboItem);
			}
			cbDateHoliday.selectedIndex = 0;
		}
	}

	function holidayDates(date) {  //PMHoliday[i].HOLIDAY_DATE
		for (var i = 0; i < PMHoliday.length; i++) {
			if (date.getMonth() == new Date(PMHoliday[i].HOLIDAY_DATE).getMonth() && date.getDate() == new Date(PMHoliday[i].HOLIDAY_DATE).getDate()) {
				return [false, '']; 
			}
		}
		return [true, '']; 
	}

	function noWeekendsAndHolidays(date) { 
		var noWeekend = $.datepicker.noWeekends(date); 
		if (noWeekend[0]) {
			return holidayDates(date);
		}
		else { 
			return noWeekend; 
		}
	} 
	
	function noSundayAndHolidays(date){
		var day = date.getDay();
		if(day != 0){
			return holidayDates(date);
		}
		else{
			return [false, ''];
		}
	}

	var cbDateHoliday = new TVclComboBox2(ContainerDate.Column[1].This, "");
	cbDateHoliday.This.style.marginBottom = '10px';
	cbDateHoliday.VCLType = TVCLType.BS;
	for(var i = 0 ; i < PMHoliday.length; i++){
		var ComboItem = new TVclComboBoxItem();
		ComboItem.Value = PMHoliday[i].IDPMHOLIDAY;
		ComboItem.Text = _this.ToDateTimeMeridian(new Date(PMHoliday[i].HOLIDAY_DATE));
		ComboItem.Tag = "TestDate";
		cbDateHoliday.AddItem(ComboItem);
	}
	cbDateHoliday.selectedIndex = 0;

	var ContainerBtnHoliday = new TVclStackPanel(ContainerDate.Column[1].This, "",  1, [[12], [12], [12], [12], [12]]);
	ContainerBtnHoliday.Row.This.style.marginBottom = '15px';
	ContainerBtnHoliday.Column[0].This.style.textAlign = 'right';
	var btnDeleteHoliday = new TVclButton(ContainerBtnHoliday.Column[0].This, "");
	btnDeleteHoliday.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnDeleteHoliday.Text");
	btnDeleteHoliday.VCLType = TVCLType.BS;
	btnDeleteHoliday.Src = "image/16/delete.png";
	btnDeleteHoliday.onClick = function () {
		var success = Persistence.ProjectManagement.Methods.PMHOLIDAY_DEL(parseInt(cbDateHoliday.Value));
		if(success){
			_this.PManagement.Fill();
			_this.MAINTASKList = _this.PManagement.PMMAINTASKList;
			index = Persistence.ProjectManagement.Methods.PMMAINTASK_ListGetIndex(_this.MAINTASKList, id);
			MainTask = _this.MAINTASKList[index];
			PMHoliday = MainTask.PMHOLIDAYList;
			cbDateHoliday.ClearItems();
			for(var i = 0 ; i < PMHoliday.length; i++){
				var ComboItem = new TVclComboBoxItem();
				ComboItem.Value = PMHoliday[i].IDPMHOLIDAY;
				ComboItem.Text = _this.ToDateTimeMeridian(new Date(PMHoliday[i].HOLIDAY_DATE));
				ComboItem.Tag = "TestDate";
				cbDateHoliday.AddItem(ComboItem);
			}
			cbDateHoliday.selectedIndex = 0;
			$("#DatepickerHoliday").datepicker("destroy");
			InitCalendar();
		}
	}
	btnDeleteHoliday.This.style.padding = '0px';
	btnDeleteHoliday.This.style.paddingLeft = '10px';

	var ContWeekend = new TVclStackPanel(ContainerDate.Column[1].This, "", 1, [[12], [12], [12], [12], [12]]);
	ContWeekend.Row.This.style.marginBottom = '15px';
	var ListBox_Weekend = new TVclListBox(ContWeekend.Column[0].This, "");
	ListBox_Weekend.EnabledCheckBox = true;
	var ListBoxItem_Weekend = new TVclListBoxItem();
	ListBoxItem_Weekend.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ListBoxItem_Weekend.Text");
	ListBoxItem_Weekend.Index = 0;
	ListBox_Weekend.AddListBoxItem(ListBoxItem_Weekend);
	ListBox_Weekend.ListBoxItems[0].This.Row.This.style.marginRight = '8px';
	ListBox_Weekend.ListBoxItems[0].This.Row.This.style.marginLeft = '5px';
	ListBox_Weekend.OnCheckedListBoxItemChange = function (EventArgs, Object) {
		if(EventArgs.Checked){
			$("#DatepickerHoliday").datepicker("destroy");
			$('#DatepickerHoliday').datepicker({
				beforeShowDay: noWeekendsAndHolidays,/*$.datepicker.noWeekends*/
				onSelect: function(date) { 
					SelectDate(date)
				}
			});
			MainTask.MAINTASK_WEEKENDHOLIDAY = true;
			MainTask.MAINTASK_SUNDAYHOLIDAY = false;
			var success = Persistence.ProjectManagement.Methods.PMMAINTASK_UPD(MainTask);
			if(success){
				ListBox_Sunday.ListBoxItems[0].Checked = false;
			}
			else{
				alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines5"));
			}
		}
		else{
			$("#DatepickerHoliday").datepicker("destroy");
			$('#DatepickerHoliday').datepicker({
				beforeShowDay: holidayDates,
				onSelect: function(date) { 
					SelectDate(date)
				}
			});
		}
	}
	ListBox_Weekend.ListBoxItems[0].Checked = MainTask.MAINTASK_WEEKENDHOLIDAY;

	var ContSunday = new TVclStackPanel(ContainerDate.Column[1].This, "", 1, [[12], [12], [12], [12], [12]]);
	ContSunday.Row.This.style.marginBottom = '15px';
	var ListBox_Sunday = new TVclListBox(ContSunday.Column[0].This, "");
	ListBox_Sunday.EnabledCheckBox = true;
	var ListBoxItem_Sunday = new TVclListBoxItem();
	ListBoxItem_Sunday.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ListBoxItem_Sunday.Text");
	ListBoxItem_Sunday.Index = 0;
	ListBox_Sunday.AddListBoxItem(ListBoxItem_Sunday);
	ListBox_Sunday.ListBoxItems[0].This.Row.This.style.marginRight = '8px';
	ListBox_Sunday.ListBoxItems[0].This.Row.This.style.marginLeft = '5px';
	ListBox_Sunday.OnCheckedListBoxItemChange = function (EventArgs, Object) {
		if(EventArgs.Checked){
			$("#DatepickerHoliday").datepicker("destroy");
			$('#DatepickerHoliday').datepicker({
				beforeShowDay: noSundayAndHolidays, /*function(date){ var day = date.getDay(); return [(day != 0), ''];} */
				onSelect: function(date) { 
					SelectDate(date)
				}
			});
			MainTask.MAINTASK_WEEKENDHOLIDAY = false;
			MainTask.MAINTASK_SUNDAYHOLIDAY = true;
			var success = Persistence.ProjectManagement.Methods.PMMAINTASK_UPD(MainTask);
			if(success){
				ListBox_Weekend.ListBoxItems[0].Checked = false;
			}
			else{
				alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines5"));
			}
		}
		else{
			$("#DatepickerHoliday").datepicker("destroy");
			$('#DatepickerHoliday').datepicker({
				beforeShowDay: holidayDates,
				onSelect: function(date) { 
					SelectDate(date)
				}
			});
		}
	}
	ListBox_Sunday.ListBoxItems[0].Checked = MainTask.MAINTASK_SUNDAYHOLIDAY;

	Modal.ShowModal();
	Modal.FunctionAfterClose = function () {
		$('#ui-datepicker-div').remove();		
		$(this.This.This).remove();
	}
}

ItHelpCenter.Demo.TDemoMainTask.prototype.ToDateTimeMeridian = function (d) {
	var month = d.getMonth() + 1;
	var day = d.getDate();
	var year = d.getFullYear();
	var hour = d.getHours();
	var min = d.getMinutes();
	var AMPM = (hour > 11) ? "pm" : "am";
	if(hour > 12) {
		hour -= 12;
	}
	else if(hour == 0) {
		hour = "12";
	}
	var date = year + "/" + SysCfg.DateTimeMethods.Pad(month, 2) + "/" + SysCfg.DateTimeMethods.Pad(day, 2) + " " + SysCfg.DateTimeMethods.Pad(hour, 2) + ":" + SysCfg.DateTimeMethods.Pad(min, 2) + " " + AMPM;
	return date;
}

ItHelpCenter.Demo.TDemoMainTask.prototype.importarScript = function (nombre, onSuccess, onError) {
	var s = document.createElement("script");
	s.onload = onSuccess;
	s.onerror = onError;
	s.src = nombre;
	document.querySelector("head").appendChild(s);
}