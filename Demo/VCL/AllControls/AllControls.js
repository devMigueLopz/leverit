﻿ItHelpCenter.Demo.TAllControls = function (inMenuObject, inObject, incallbackModalResult) {

    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.Object = inObject;
    this.MenuObject = inMenuObject;
    this.CallbackModalResult = incallbackModalResult;//CallbackModalResult(OutStr);        
    //******************************************************
    this.combovalue = ""
    this.Mythis = "TAllControls";
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lblLabel", "Text of Label");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lblMemo", "Memo");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lblButton", "Button");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnClickHere", "Click here");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "HelloMessage", "Hello");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lblcheckbox", "CheckBox");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lblForm", "form");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lblFormClose", "Close form");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lblStackPanel", "StackPanel");
    _this.loadpage();
}

ItHelpCenter.Demo.TAllControls.prototype.loadpage = function () {
    var _this = this.TParent();

    //this.OnBeforeBackPage = null;
    //this.MenuObject.OnAfterBackPage = null;
    _this.MenuObject.OnBeforeBackPage = function () {
        //alert("antes de ir atras (forma el _this esta en Allcontrol)");
    }
    var ContTabControl = new TVclStackPanel(this.Object, "1", 1, [[12], [12], [12], [12], [12]]);
    ContTabControl.Row.This.style.marginTop = "20px";

    ///INICIO DEL TREE PARA FINCAR LOS EJEMPLOS DE CONTROLES
    var ContTotalControl = new TVclStackPanel(ContTabControl.Column[0].This, "1", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
    ContTotalControl.Row.This.style.marginTop = "20px";

    // var ContIzquierda = ContTotalControl.Column[0].This;

    //en esta parte se genera el div contenedor del menu dependiendo de la variable IsMobil
    var ContIzquierda = _this.MenuObject.GetDivData(ContTotalControl.Column[0], ContTotalControl.Column[1]);
    var ContDerecha = ContTotalControl.Column[1].This;

    //se le asigna 
    if (Source.Menu.IsMobil)
    { ContIzquierda.classList.add("menuContenedorMobil"); }

    var VclTree = new TVclTree(ContIzquierda, ""); //CREA UN TREE CONTROL 

    var TreeNodeRoot = VclTree.AddTreeNodeRoot("All Controls", ""); //CREA UN TREENODE DE TIPO ROOT 
    TreeNodeRoot.Img = "image/24/Addressbook.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
    TreeNodeRoot.Tag = "Tag root"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO


    var TreeNode1 = VclTree.AddTreeNode("All Controls", "Common Controls", ""); //CREA UN TREENODE
    TreeNode1.Img = "image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
    TreeNode1.Tag = "Tag node 1"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
    //TreeNode1.Numero = "1"; //ASIGNA O EXTRAE EL NRO DE ORDEN QUE OCUPA EL NODO EN EL TREE
    TreeNode1.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
        $(ContDerecha).html("");
        _this.importarScript(SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoCommonControls.js", function () {
            var Demo = new ItHelpCenter.Demo.TDemoCommonControls(ContDerecha, _this);
        }, function (e) {
            //script de error
        });
    }

    var TreeNodeNav = VclTree.AddTreeNode("All Controls", "Nab Controls", ""); //CREA UN TREENODE
    TreeNodeNav.Img = "image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
    TreeNodeNav.Tag = "Tag node 1"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
    TreeNodeNav.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
        $(ContDerecha).html("");
        _this.importarScript(SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoNavTabControl.js", function () {
            var MenuObjetResolve = UsrCfg.Properties.MenuProfiler.NewPage(_this.MenuObject);
            var Demo = new ItHelpCenter.Demo.TDemoTabControl(MenuObjetResolve, MenuObjetResolve.ObjectDataMain, _this);

        }, function (e) {
            //script de error
        });
    }


    var TreeNode2 = VclTree.AddTreeNode("All Controls", "Stack Panel", ""); //CREA UN TREENODE
    TreeNode2.Img = "image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
    TreeNode2.Tag = "Tag node 1"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
    TreeNode2.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
        $(ContDerecha).html("");
        _this.importarScript(SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoStackPanel.js", function () {
            var Demo = new ItHelpCenter.Demo.TDemoStackPanel(ContDerecha, _this);
        }, function (e) {
            //script de error
        });
    }

    var TreeNode3 = VclTree.AddTreeNode("All Controls", "Drop Down Panel", ""); //CREA UN TREENODE
    TreeNode3.Img = "image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
    TreeNode3.Tag = "Tag node 1"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
    TreeNode3.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
        $(ContDerecha).html("");
        _this.importarScript(SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoDropDownPanel.js", function () {
            var Demo = new ItHelpCenter.Demo.TDemoDropDownPanel(ContDerecha, _this);
        }, function (e) {
            //script de error
        });
    }

    var TreeNode4 = VclTree.AddTreeNode("All Controls", "Tree Control", ""); //CREA UN TREENODE
    TreeNode4.Img = "image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
    TreeNode4.Tag = "Tag node 1"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
    TreeNode4.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
        $(ContDerecha).html("");
        _this.importarScript(SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoTreeControl.js", function () {
            var Demo = new ItHelpCenter.Demo.TDemoTreeControl(ContDerecha, _this);
        }, function (e) {
            //script de error
        });
    }

    var TreeNode5 = VclTree.AddTreeNode("All Controls", "Tab Control", ""); //CREA UN TREENODE
    TreeNode5.Img = "image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
    TreeNode5.Tag = "Tag node 1"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
    TreeNode5.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
        $(ContDerecha).html("");
        _this.importarScript(SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoTabControl.js", function () {
            var Demo = new ItHelpCenter.Demo.TDemoTabControl(ContDerecha, _this);
        }, function (e) {
            //script de error
        });
    }

    var TreeNode6 = VclTree.AddTreeNode("All Controls", "Div Control", ""); //CREA UN TREENODE
    TreeNode6.Img = "image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
    TreeNode6.Tag = "Tag node 1"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
    TreeNode6.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
        $(ContDerecha).html("");
        _this.importarScript(SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoDivControl.js", function () {
            var Demo = new ItHelpCenter.Demo.TDemoDivControl(ContDerecha, _this);
        }, function (e) {
            //script de error
        });
    }

    var TreeNode7 = VclTree.AddTreeNode("All Controls", "Grid Control", ""); //CREA UN TREENODE
    TreeNode7.Img = "image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
    TreeNode7.Tag = "Tag node 1"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
    TreeNode7.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
        $(ContDerecha).html("");
        _this.importarScript(SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoGridControl.js", function () {
            var Demo = new ItHelpCenter.Demo.TDemoGridControl(ContDerecha, _this);
        }, function (e) {
            //script de error
        });
    }

    var TreeNode8 = VclTree.AddTreeNode("All Controls", "Form Control", ""); //CREA UN TREENODE
    TreeNode8.Img = "image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
    TreeNode8.Tag = "Tag node 1"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
    TreeNode8.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
        $(ContDerecha).html("");
        _this.importarScript(SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoFormControl.js", function () {
            var Demo = new ItHelpCenter.Demo.TDemoFormControl(ContDerecha, _this);
        }, function (e) {
            //script de error
        });
    }

    var TreeNode9 = VclTree.AddTreeNode("All Controls", "Mem Table", ""); //CREA UN TREENODE
    TreeNode9.Img = "image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
    TreeNode9.Tag = "Tag node 1"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
    TreeNode9.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
        $(ContDerecha).html("");
        _this.importarScript(SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoMemTable.js", function () {
            var Demo = new ItHelpCenter.Demo.TDemoMemTable(ContDerecha, _this);
        }, function (e) {
            //script de error
        });
    }

    var TreeNode10 = VclTree.AddTreeNode("All Controls", "NavBar", ""); //CREA UN TREENODE
    TreeNode10.Img = "image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
    TreeNode10.Tag = "Tag node 1"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
    TreeNode10.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
        $(ContDerecha).html("");
        _this.importarScript(SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoNavBar.js", function () {
            var Demo = new ItHelpCenter.Demo.TDemoNavBar(ContDerecha, _this);
        }, function (e) {
            //script de error
        });
    }

    var TreeNode11 = VclTree.AddTreeNode("All Controls", "Remote Help", ""); //CREA UN TREENODE
    TreeNode11.Img = "image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
    TreeNode11.Tag = "Tag node 1"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
    TreeNode11.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
        $(ContDerecha).html("");
        //************************REMOTE HELP********************************************//
        _this.importarScript(SysCfg.App.Properties.xRaiz + "Scripts/RemoteHelpScripts/RemoteHelp.js", function () {


            $(ContDerecha).css("padding-left", "15px");
            var topbar = new TBarControls(ContDerecha, "IDRemoteHelp");
            topbar._ToolBar_Direction = false;
            var toolCont1 = new TBarControls.TToolBar(topbar, "1");
            HelpRemote = new ItHelpCenter.TUfrRemoteHelp(
            toolCont1.ToolBox,
           function () {
           }, topbar.ControlBody);


        }, function (e) {
        });
        //**************************************************************************//
    }

    var TreeNode12 = VclTree.AddTreeNode("All Controls", "Dinamic Querys", ""); //CREA UN TREENODE
    TreeNode12.Img = "image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
    TreeNode12.Tag = "Tag node 1"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
    TreeNode12.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
        $(ContDerecha).html("");
        //************************DINAMIC QUERYS********************************************//
        _this.importarScript(SysCfg.App.Properties.xRaiz + "Scripts/GPScripts/GPView.js", function () {
            var DinamicQuerys = new ItHelpCenter.TUfrGPView(
            ContDerecha,
            function () {
            }, 1);
        }, function (e) {
        });
        //**************************************************************************//
    }

    var TreeNode23 = VclTree.AddTreeNode("All Controls", "Grid CF", ""); //CREA UN TREENODE
    TreeNode23.Img = "image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
    TreeNode23.Tag = "Tag node 1"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
    TreeNode23.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
        $(ContDerecha).html("");
        _this.importarScript(SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoGridCF.js", function () {
            var Demo = new ItHelpCenter.Demo.TDemoGridCF(ContDerecha, _this);
        }, function (e) {
            SysCfg.Log.Methods.WriteLog("AllControls.js ItHelpCenter.Demo.TAllControls.prototype.loadpage  " + e);
            //script de error
        });
        //**************************************************************************//
    }

    var TreeNode13 = VclTree.AddTreeNode("All Controls", "ListBox", ""); //CREA UN TREENODE
    TreeNode13.Img = "image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
    TreeNode13.Tag = "Tag node 1"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
    TreeNode13.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
        $(ContDerecha).html("");
        _this.importarScript(SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoListBox.js", function () {
            var Demo = new ItHelpCenter.Demo.TDemoListBox(ContDerecha, _this);
        }, function (e) {
            //script de error
        });
        //**************************************************************************//
    }

    var TreeNode14 = VclTree.AddTreeNode("All Controls", "PanelService", ""); //CREA UN TREENODE
    TreeNode14.Img = "image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
    TreeNode14.Tag = "Tag node 1"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
    TreeNode14.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
        $(ContDerecha).html("");
        _this.importarScript(SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoPanelService.js", function () {
            //_this.importarStyle(SysCfg.App.Properties.xRaiz + "Componet/PanelServices/Css/PSHorizontal.css", function () {
            //_this.importarStyle(SysCfg.App.Properties.xRaiz + "Componet/PanelServices/Css/PSVertical.css", function () {
            var Demo = new ItHelpCenter.Demo.TDemoPanelService(ContDerecha, _this);
            //}, function (e) { });
            //}, function (e) { });
        }, function (e) {
            //script de error
        });
    }

    var TreeNode15 = VclTree.AddTreeNode("All Controls", "GraphicsPBI", ""); //CREA UN TREENODE
    TreeNode15.Img = "image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
    TreeNode15.Tag = "Tag node 1"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
    TreeNode15.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
        $(ContDerecha).html("");
        _this.importarScript(SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoGraphicsPBI.js", function () {
            _this.importarScript(SysCfg.App.Properties.xRaiz + "Componet/GraphicsPBI/Scripts/Plugin/Chart/Chart.bundle.min.js", function () {
                _this.importarScript(SysCfg.App.Properties.xRaiz + "Componet/GraphicsPBI/Scripts/Plugin/D3/d3.min.js", function () {
                    _this.importarStyle(SysCfg.App.Properties.xRaiz + "Componet/GraphicsPBI/Card/css/Card.css", function () {
                        _this.importarStyle(SysCfg.App.Properties.xRaiz + "Componet/GraphicsPBI/Bar/css/Bar.css", function () {
                            _this.importarStyle(SysCfg.App.Properties.xRaiz + "Componet/GraphicsPBI/Pie/css/Pie.css", function () {
                                _this.importarStyle(SysCfg.App.Properties.xRaiz + "Componet/GraphicsPBI/TreeMap/css/TreeMap.css", function () {
                                    _this.importarStyle(SysCfg.App.Properties.xRaiz + "Componet/GraphicsPBI/Table/css/GraphicTable.css", function () {
                                        _this.importarStyle(SysCfg.App.Properties.xRaiz + "Componet/GraphicsPBI/Funnel/css/GraphicFunnel.css", function () {
                                            var Demo = new ItHelpCenter.Demo.TDemoGraphicsPBI(ContDerecha, _this);
                                        }, function (e) { });
                                    }, function (e) { });
                                }, function (e) { });
                            }, function (e) { });
                        }, function (e) { });
                    }, function (e) { });
                }, function (e) { });
            }, function (e) { });
        }, function (e) {
            //script de error
        });
    }

    var TreeNode16 = VclTree.AddTreeNode("All Controls", "BarControls", ""); //CREA UN TREENODE
    TreeNode16.Img = "image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
    TreeNode16.Tag = "Tag node 1"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
    TreeNode16.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
        $(ContDerecha).html("");
        _this.importarScript(SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoBarControls.js", function () {
            var Demo = new ItHelpCenter.Demo.TDemoBarControls(ContDerecha, _this);
        }, function (e) {
            //script de error
        });
    }

    var TreeNode17 = VclTree.AddTreeNode("All Controls", "Modal", ""); //CREA UN TREENODE
    TreeNode17.Img = "image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
    TreeNode17.Tag = "Tag node 1"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
    TreeNode17.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
        $(ContDerecha).html("");
        _this.importarScript(SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoModal.js", function () {
            var Demo = new ItHelpCenter.Demo.TDemoModal(ContDerecha, _this);
        }, function (e) {
            //script de error
        });
    }

    var TreeNode18 = VclTree.AddTreeNode("All Controls", "AdvanceSearch", ""); //CREA UN TREENODE
    TreeNode18.Img = "image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
    TreeNode18.Tag = "Tag node 1"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
    TreeNode18.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
        $(ContDerecha).html("");
        _this.importarScript(SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoAdvanceSearch.js", function () {
            var Demo = new ItHelpCenter.Demo.TDemoAdvanceSearch(ContDerecha, _this);
        }, function (e) {
            //script de error
        });
    }

    var TreeNode20 = VclTree.AddTreeNode("All Controls", "PaleteColors", ""); //CREA UN TREENODE
    TreeNode20.Img = "image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
    TreeNode20.Tag = "Tag node 1"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
    TreeNode20.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
        $(ContDerecha).html("");
        _this.importarScript(SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoPaleteColors.js", function () {
            _this.importarScript(SysCfg.App.Properties.xRaiz + "Componet/Color/Plugin/spectrum.js", function () {
                _this.importarStyle(SysCfg.App.Properties.xRaiz + "Componet/Color/Plugin/spectrum.css", function () {
                    var Demo = new ItHelpCenter.Demo.TDemoPaleteColors(ContDerecha, _this);
                }, function (e) { });
            }, function (e) { });
        }, function (e) { });
    }
    var TreeNode21 = VclTree.AddTreeNode("All Controls", "Graphics Preview", ""); //CREA UN TREENODE
    TreeNode21.Img = "image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
    TreeNode21.Tag = "Tag node 1"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
    TreeNode21.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
        $(ContDerecha).html("");
        try {
            var PBIGraphics = new ItHelpCenter.GraphicsPBIManager.TGraphicsPBIManagerBasic(ContDerecha, function (_this) { }, 1);
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("AllControls.js ItHelpCenter.Demo.TAllControls.prototype.loadpage  ", e);
        }
    }

    var TreeNode21 = VclTree.AddTreeNode("All Controls", "Devices", ""); //CREA UN TREENODE
    TreeNode21.Img = "image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
    TreeNode21.Tag = "Tag node 1"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
    TreeNode21.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
        $(ContDerecha).html("");
        _this.importarScript(SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoDevices.js", function () {
            var Demo = new ItHelpCenter.Demo.TDemoTemporal9(ContDerecha, _this);
        }, function (e) {
            //script de error
        });
    }

    var TreeNode22 = VclTree.AddTreeNode("All Controls", "Temporal 7", ""); //CREA UN TREENODE
    TreeNode22.Img = "image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
    TreeNode22.Tag = "Tag node 1"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
    TreeNode22.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
        $(ContDerecha).html("");
        _this.importarScript(SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoTemporal7.js", function () {
            var Demo = new ItHelpCenter.Demo.TDemoTemporal7(ContDerecha, _this);
        }, function (e) {
            //script de error
        });
    }

    var TreeNode23 = VclTree.AddTreeNode("All Controls", "Up and Donwload File", ""); //CREA UN TREENODE
    TreeNode23.Img = "image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
    TreeNode23.Tag = "Tag node 1"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
    TreeNode23.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
        $(ContDerecha).html("");
        _this.importarScript(SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoUploadAndDownloadFile.js", function () {
            var Demo = new ItHelpCenter.Demo.TDemoUploadAndDownloadFile(ContDerecha, _this);
        }, function (e) {
            //script de error
        });
    }

    var TreeNode23 = VclTree.AddTreeNode("All Controls", "Open & Save File", ""); //CREA UN TREENODE
    TreeNode23.Img = "image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
    TreeNode23.Tag = "Tag node 1"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
    TreeNode23.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
        $(ContDerecha).html("");
        _this.importarScript(SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoOpenAndSaveFile.js", function () {
            var Demo = new ItHelpCenter.Demo.TDemoOpenAndSaveFile(ContDerecha, _this);
        }, function (e) {
            //script de error
        });
    }

    var TreeNode24 = VclTree.AddTreeNode("All Controls", "Temporal 10", ""); //CREA UN TREENODE

    TreeNode24.Img = "image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
    TreeNode24.Tag = "Tag node 1"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
    TreeNode24.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
        try {
            $(ContDerecha).html("");
            _this.importarScript(SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoTemporal10.js", function () {
                var Demo = new ItHelpCenter.Demo.TDemoTemporal10(ContDerecha, _this);
            }, function (e) {
                SysCfg.Log.Methods.WriteLog("AllControls.js ItHelpCenter.Demo.TAllControls.prototype.loadpage - TreeNode24.onClick - ItHelpCenter.Demo.TAllControls.prototype.importarScript(" + SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoTemporal10.js)", e);
            });
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("AllControls.js ItHelpCenter.Demo.TAllControls.prototype.loadpage TreeNode24.onClick", e);
        }
    }

    var TreeNode25 = VclTree.AddTreeNode("All Controls", "Timer", ""); //CREA UN TREENODE
    TreeNode25.Img = "image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
    TreeNode25.Tag = "Tag node 1"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
    TreeNode25.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
        $(ContDerecha).html("");
        _this.importarScript(SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoTimer.js", function () {
            var Demo = new ItHelpCenter.Demo.TDemoTimer(ContDerecha, _this);
        }, function (e) {
            //script de error
        });
    }

    var TreeNode26 = VclTree.AddTreeNode("All Controls", "Up and Donwload File WCF", ""); //CREA UN TREENODE
    TreeNode26.Img = "image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
    TreeNode26.Tag = "Tag node 1"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
    TreeNode26.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
        $(ContDerecha).html("");
        _this.importarScript(SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoUploadAndDownloadFileWCF.js", function () {
            var Demo = new ItHelpCenter.Demo.TDemoUploadAndDownloadFileWCF(ContDerecha, _this);
        }, function (e) {
            //script de error
        });
    }

    var TreeNode30 = VclTree.AddTreeNode("All Controls", "Order Column", ""); //CREA UN TREENODE
    TreeNode30.Img = "image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
    TreeNode30.Tag = "Tag node 1"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
    TreeNode30.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
        $(ContDerecha).html("");
        _this.importarScript(SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoOrderColumn.js", function () {
            var Demo = new ItHelpCenter.Demo.TDemoOrderColumn(ContDerecha, _this);
        }, function (e) {
            //script de error
        });
    }
    //var TreeNode22 = VclTree.AddTreeNode("All Controls", "Temporal 10", ""); //CREA UN TREENODE
    //TreeNode22.Img = "image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
    //TreeNode22.Tag = "Tag node 1"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
    //TreeNode22.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
    //    $(ContDerecha).html("");
    //    _this.importarScript(SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoTemporal10.js", function () {
    //        var Demo = new ItHelpCenter.Demo.TDemoTemporal10(ContDerecha, _this);
    //    }, function (e) {
    //        //script de error
    //    });
    //}

    var TreeNode27 = VclTree.AddTreeNode("All Controls", "Scheduler", ""); //CREA UN TREENODE
    TreeNode27.Img = "image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
    TreeNode27.Tag = "Tag node 27"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
    TreeNode27.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
        try {
            $(ContDerecha).html("");
            _this.importarStyle(SysCfg.App.Properties.xRaiz + "Componet/Scheduler/Plugin/dhtmlxscheduler_terrace.css", function () {
                _this.importarScript(SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoScheduler.js", function () {
                    _this.importarScript(SysCfg.App.Properties.xRaiz + "Componet/Color/Plugin/spectrum.js", function () {
                        _this.importarStyle(SysCfg.App.Properties.xRaiz + "Componet/Color/Plugin/spectrum.css", function () {
                            var DemoScheduler = new ItHelpCenter.Demo.TDemoScheduler(ContDerecha, "ComponetScheduler", null);
                        }, function (e) {
                            SysCfg.Log.Methods.WriteLog("AllControls.js ItHelpCenter.Demo.TAllControls.prototype.loadpage - TreeNode27.onClick - ItHelpCenter.Demo.TAllControls.prototype.importarStyle(" + SysCfg.App.Properties.xRaiz + "Componet/Color/Plugin/spectrum.css)", e);
                        });
                    }, function (e) {
                        SysCfg.Log.Methods.WriteLog("AllControls.js ItHelpCenter.Demo.TAllControls.prototype.loadpage - TreeNode27.onClick - ItHelpCenter.Demo.TAllControls.prototype.importarScript(" + SysCfg.App.Properties.xRaiz + "Componet/Color/Plugin/spectrum.js)", e);
                    });
                }, function (e) {
                    SysCfg.Log.Methods.WriteLog("AllControls.js ItHelpCenter.Demo.TAllControls.prototype.loadpage - TreeNode27.onClick - ItHelpCenter.Demo.TAllControls.prototype.importarScript(" + SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoScheduler.js)", e);
                });
            }, function (e) {
                SysCfg.Log.Methods.WriteLog("AllControls.js ItHelpCenter.Demo.TAllControls.prototype.loadpage - TreeNode27.onClick - ItHelpCenter.Demo.TAllControls.prototype.importarScript(" + SysCfg.App.Properties.xRaiz + "Componet/Scheduler/Plugin/dhtmlxscheduler_terrace.css)", e);
            });
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("AllControls.js ItHelpCenter.Demo.TAllControls.prototype.loadpage TreeNode27.onClick", e);
        }
    }

    var TreeNode28 = VclTree.AddTreeNode("All Controls", "ListView", ""); //CREA UN TREENODE
    TreeNode28.Img = "image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
    TreeNode28.Tag = "Tag node 28"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
    TreeNode28.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
        try {
            $(ContDerecha).html("");
            _this.importarStyle(SysCfg.App.Properties.xRaiz + "Componet/ListView/Plugin/skins/dhtmlxlist_dhx_web.css", function () {
                _this.importarScript(SysCfg.App.Properties.xRaiz + "Componet/ListView/Plugin/dhtmlxlist.js", function () {
                    _this.importarScript(SysCfg.App.Properties.xRaiz + "Componet/ListView/Class/ListView.js", function () {
                        _this.importarScript(SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoListView.js", function () {
                            var DemoScheduler = new ItHelpCenter.Demo.TDemoListView(ContDerecha, "ComponetListView", null);
                        }, function (e) {
                            SysCfg.Log.Methods.WriteLog("AllControls.js ItHelpCenter.Demo.TAllControls.prototype.loadpage - TreeNode28.onClick - ItHelpCenter.Demo.TAllControls.prototype.importarScript(" + SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoListView.js)", e);
                        });
                    }, function (e) {
                        SysCfg.Log.Methods.WriteLog("AllControls.js ItHelpCenter.Demo.TAllControls.prototype.loadpage - TreeNode28.onClick - ItHelpCenter.Demo.TAllControls.prototype.importarScript(" + SysCfg.App.Properties.xRaiz + "Componet/ListView/Class/ListView.js)", e);
                    });
                }, function (e) {
                    SysCfg.Log.Methods.WriteLog("AllControls.js ItHelpCenter.Demo.TAllControls.prototype.loadpage - TreeNode28.onClick - ItHelpCenter.Demo.TAllControls.prototype.importarScript(" + SysCfg.App.Properties.xRaiz + "Componet/ListView/Plugin/dhtmlxlist.js)", e);
                });
            }, function (e) {
                SysCfg.Log.Methods.WriteLog("AllControls.js ItHelpCenter.Demo.TAllControls.prototype.loadpage - TreeNode28.onClick - ItHelpCenter.Demo.TAllControls.prototype.importarStyle(" + SysCfg.App.Properties.xRaiz + "Componet/ListView/Plugin/skins/dhtmlxlist_dhx_web.css)", e);
            });
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("AllControls.js ItHelpCenter.Demo.TAllControls.prototype.loadpage TreeNode28.onClick", e);
        }
    }

    var TreeNode29 = VclTree.AddTreeNode("All Controls", "TreeView", ""); //CREA UN TREENODE
    TreeNode29.Img = "image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
    TreeNode29.Tag = "Tag node 29"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
    TreeNode29.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
        try {
            $(ContDerecha).html("");
            _this.importarScript(SysCfg.App.Properties.xRaiz + "Componet/Ucode/VCL/TreeView/TreeListBox.js", function () {
                _this.importarScript(SysCfg.App.Properties.xRaiz + "Componet/Ucode/VCL/TreeView/TreeView.js", function () {
                    _this.importarScript(SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoTreeView.js", function () {
                        _this.importarScript(SysCfg.App.Properties.xRaiz + "Componet/Color/Plugin/spectrum.js", function () {
                            _this.importarStyle(SysCfg.App.Properties.xRaiz + "Componet/Color/Plugin/spectrum.css", function () {
                                var DemoScheduler = new ItHelpCenter.Demo.TDemoTreeView(ContDerecha, "ComponetTreeView", null);
                            }, function (e) {
                                SysCfg.Log.Methods.WriteLog("AllControls.js ItHelpCenter.Demo.TAllControls.prototype.loadpage - TreeNode29.onClick - ItHelpCenter.Demo.TAllControls.prototype.importarStyle(" + SysCfg.App.Properties.xRaiz + "Componet/Color/Plugin/spectrum.css)", e);
                            });
                        }, function (e) {
                            SysCfg.Log.Methods.WriteLog("AllControls.js ItHelpCenter.Demo.TAllControls.prototype.loadpage - TreeNode29.onClick - ItHelpCenter.Demo.TAllControls.prototype.importarScript(" + SysCfg.App.Properties.xRaiz + "Componet/Color/Plugin/spectrum.js)", e);
                        });
                    }, function (e) {
                        SysCfg.Log.Methods.WriteLog("AllControls.js ItHelpCenter.Demo.TAllControls.prototype.loadpage - TreeNode29.onClick - ItHelpCenter.Demo.TAllControls.prototype.importarScript(" + SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoTreeView.js)", e);
                    });
                }, function (e) {
                    SysCfg.Log.Methods.WriteLog("AllControls.js ItHelpCenter.Demo.TAllControls.prototype.loadpage - TreeNode29.onClick - ItHelpCenter.Demo.TAllControls.prototype.importarScript(" + SysCfg.App.Properties.xRaiz + "Componet/Ucode/VCL/Tree/TreeView/TreeView.js)", e);
                });
            }, function (e) {
                SysCfg.Log.Methods.WriteLog("AllControls.js ItHelpCenter.Demo.TAllControls.prototype.loadpage - TreeNode29.onClick - ItHelpCenter.Demo.TAllControls.prototype.importarScript(" + SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoTreeView.js)", e);
            });
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("AllControls.js ItHelpCenter.Demo.TAllControls.prototype.loadpage TreeNode29.onClick", e);
        }
    }

    var TreeNode31 = VclTree.AddTreeNode("All Controls", "Tooltip", ""); //CREA UN TREENODE
    TreeNode31.Img = "image/24/key.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
    TreeNode31.Tag = "Tag node 31"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
    TreeNode31.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
        $(ContDerecha).html("");
        _this.importarScript(SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoTooltip.js", function () {
            var Demo = new ItHelpCenter.Demo.Tooltip(ContDerecha, _this);
        }, function (e) {
            //script de error
        });
    }
    
     var TreeNode32 = VclTree.AddTreeNode("All Controls", "Temporal MainTask", ""); //CREA UN TREENODE
	TreeNode32.Img = "image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
	TreeNode32.Tag = "Tag node 1"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
	TreeNode32.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
		$(ContDerecha).html("");
		_this.importarScript(SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoMainTask.js", function () {
			var Demo = new ItHelpCenter.Demo.TDemoMainTask(ContDerecha, _this);
		}, function (e) {
			//script de error
		});
	}

	var TreeNode33 = VclTree.AddTreeNode("All Controls", "Temporal PMGantt", ""); //CREA UN TREENODE
	TreeNode33.Img = "image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
	TreeNode33.Tag = "Tag node 1"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
	TreeNode33.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
		$(ContDerecha).html("");
		_this.importarScript(SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoPMGantt.js", function () {
			var Demo = new ItHelpCenter.Demo.TDemoPMGantt(ContDerecha, _this, 1);
		}, function (e) {
			//script de error
		});
	}
	var TreeNode34 = VclTree.AddTreeNode("All Controls", "Forums", ""); //CREA UN TREENODE
	TreeNode34.Img = "image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
	TreeNode34.Tag = "Tag node 30"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
	TreeNode34.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
	    try {
	        $(ContDerecha).html("");
	        _this.importarScript(SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoForums.js", function () {
	            var idForum = 1;
	            var DemoForums = new ItHelpCenter.Demo.TDemoForums(ContDerecha, idForum, "DemoForums",true, null);
	        }, function (e) {
	            SysCfg.Log.Methods.WriteLog("AllControls.js ItHelpCenter.Demo.TAllControls.prototype.loadpage - TreeNode32.onClick - ItHelpCenter.Demo.TAllControls.prototype.importarScript(" + SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoForums.js)", e);
	        });
	    } catch (e) {
	        SysCfg.Log.Methods.WriteLog("AllControls.js ItHelpCenter.Demo.TAllControls.prototype.loadpage TreeNode34.onClick", e);
	    }
	}

	var TreeNode35 = VclTree.AddTreeNode("All Controls", "Image Format", ""); //CREA UN TREENODE
	TreeNode35.Img = "image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
	TreeNode35.Tag = "Tag node 1"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
	TreeNode35.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
	    $(ContDerecha).html("");
	    _this.importarScript(SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoImageFormat.js", function () {
	        var Demo = new ItHelpCenter.Demo.TDemoImageFormat(ContDerecha, _this);
	    }, function (e) {
	        SysCfg.Log.Methods.WriteLog("AllControls.js ItHelpCenter.Demo.TAllControls.prototype.loadpage  " + e);
	        //script de error
	    });
	    //**************************************************************************//
	}

	var TreeNode36 = VclTree.AddTreeNode("All Controls", "Camera", ""); //CREA UN TREENODE
	TreeNode36.Img = "image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
	TreeNode36.Tag = "Tag node 1"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
	TreeNode36.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
	    $(ContDerecha).html("");
	    _this.importarScript(SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoCamera.js", function () {
	        var Demo = new ItHelpCenter.Demo.TDemoCamera(ContDerecha, _this);
	    }, function (e) {
	        SysCfg.Log.Methods.WriteLog("AllControls.js ItHelpCenter.Demo.TAllControls.prototype.loadpage  " + e);
	        //script de error
	    });
	    //**************************************************************************//
	}

	var TreeNode37 = VclTree.AddTreeNode("All Controls", "Maps", ""); //CREA UN TREENODE
	TreeNode37.Img = "image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
	TreeNode37.Tag = "Tag node 1"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
	TreeNode37.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
	    $(ContDerecha).html("");
	    _this.importarScript(SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoMaps.js", function () {
	        var Demo = new ItHelpCenter.Demo.TDemoMaps(ContDerecha, _this);
	    }, function (e) {
	        SysCfg.Log.Methods.WriteLog("AllControls.js ItHelpCenter.Demo.TAllControls.prototype.loadpage  " + e);
	        //script de error
	    });
	    //**************************************************************************//
	}
	var TreeNode38 = VclTree.AddTreeNode("All Controls", "Captcha", ""); //CREA UN TREENODE
	TreeNode38.Img = "image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
	TreeNode38.Tag = "Tag node 1"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
	TreeNode38.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
	    $(ContDerecha).html("");
	    _this.importarScript(SysCfg.App.Properties.xRaiz + "Demo/VCL/AllControls/DemoCaptcha.js", function () {
	        var Demo = new ItHelpCenter.Demo.TDemoCaptcha(ContDerecha, _this);
	    }, function (e) {
	        SysCfg.Log.Methods.WriteLog("AllControls.js ItHelpCenter.Demo.TAllControls.prototype.loadpage  " + e);
	        //script de error
	    });
	    //**************************************************************************//
	}
    TreeNodeRoot.Expand();
    ///FIN DEL TREE PARA FINCAR LOS EJEMPLOS DE CONTROLES
}


ItHelpCenter.Demo.TAllControls.prototype.importarScript = function (nombre, onSuccess, onError) {
    var s = document.createElement("script");
    s.onload = onSuccess;
    s.onerror = onError;
    s.src = nombre;
    document.querySelector("head").appendChild(s);
}
ItHelpCenter.Demo.TAllControls.prototype.importarStyle = function (nombre, onSuccess, onError) {
    var style = document.createElement("link");
    style.rel = "stylesheet";
    style.type = "text/css";
    style.href = nombre;
    var s = document.head.appendChild(style);
    s.onload = onSuccess;
    s.onerror = onError;
}

ItHelpCenter.Demo.GeneraQuery = function (DataSet, RecordOld, RecordNew, Status, SurceDbTable) {
    var SQLPart = "";
    var SQLPartA = "";
    var SQLPartB = "";
    if (Status == SysCfg.MemTable.Properties.TStatus.Apped) {
        for (var ContX = 0; ContX <= DataSet.FieldCount - 1; ContX++) {
            if (ContX == 0) {
                SQLPartA = DataSet.FieldDefs[ContX].FieldName;
                SQLPartB = RecordNew.Fields[ContX].Value;//SysCfg.DB.Format.objectToParam(RecordNew.Fields[ContX].Value, DataSet.FieldDefs[ContX].FieldName, DataSet.FieldDefs[ContX].DataTypetoType(), SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            }
            else {
                SQLPartA = SQLPartA + "," + DataSet.FieldDefs[ContX].FieldName;
                SQLPartB = SQLPartB + "," + RecordNew.Fields[ContX].Value;//SysCfg.DB.Format.objectToParam(RecordNew.Fields[ContX].Value, DataSet.FieldDefs[ContX].FieldName, DataSet.FieldDefs[ContX].DataTypetoType(), SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            }
        }
        SQLPart = "INSERT INTO " + SurceDbTable + " (" + SQLPartA + ") Values (" + SQLPartB + ")";
    }
    else if (DataSet.Status == SysCfg.MemTable.Properties.TStatus.Update) {
        for (var ContX = 0; ContX <= DataSet.FieldCount - 1; ContX++) {
            if (ContX == 0) {
                SQLPartA = DataSet.FieldDefs[ContX].FieldName + "=" + RecordNew.Fields[ContX].Value;//SysCfg.DB.Format.objectToParam(RecordNew.Fields[ContX].Value, DataSet.FieldDefs[ContX].FieldName, DataSet.FieldDefs[ContX].DataTypetoType(), SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                SQLPartB = DataSet.FieldDefs[ContX].FieldName + "=" + RecordOld.Fields[ContX].Value;//SysCfg.DB.Format.objectToParam(RecordOld.Fields[ContX].Value, DataSet.FieldDefs[ContX].FieldName, DataSet.FieldDefs[ContX].DataTypetoType(), SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            }
            else {
                SQLPartA = SQLPartA + "" + DataSet.FieldDefs[ContX].FieldName + "=" + RecordNew.Fields[ContX].Value;//SysCfg.DB.Format.objectToParam(RecordNew.Fields[ContX].Value, DataSet.FieldDefs[ContX].FieldName, DataSet.FieldDefs[ContX].DataTypetoType(), SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                SQLPartB = SQLPartB + "," + DataSet.FieldDefs[ContX].FieldName + "=" + RecordOld.Fields[ContX].Value;//SysCfg.DB.Format.objectToParam(RecordOld.Fields[ContX].Value, DataSet.FieldDefs[ContX].FieldName, DataSet.FieldDefs[ContX].DataTypetoType(), SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            }
        }
        SQLPart = "UPDATE " + SurceDbTable + " SET " + SQLPartA + " WHERE " + SQLPartB;
    }
    else if (DataSet.Status == SysCfg.MemTable.Properties.TStatus.Delete) {
        for (var ContX = 0; ContX <= DataSet.FieldCount - 1; ContX++) {
            if (ContX == 0) {
                SQLPartB = DataSet.FieldDefs[ContX].FieldName + "=" + RecordOld.Fields[ContX].Value;//SysCfg.DB.Format.objectToParam(RecordOld.Fields[ContX].Value, DataSet.FieldDefs[ContX].FieldName, DataSet.FieldDefs[ContX].DataTypetoType(), SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            }
            else {
                SQLPartB = SQLPartB + "," + DataSet.FieldDefs[ContX].FieldName + "=" + RecordOld.Fields[ContX].Value;//SysCfg.DB.Format.objectToParam(RecordOld.Fields[ContX].Value, DataSet.FieldDefs[ContX].FieldName, DataSet.FieldDefs[ContX].DataTypetoType(), SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            }
            SQLPart = "DELETE " + SurceDbTable + " WHERE " + SQLPartB;
        }
    }
    return (SQLPart);
}

