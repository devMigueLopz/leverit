﻿ItHelpCenter.Demo.TDemoCamera = function (inObjectHtml, _this) {

    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.VclCamera = null;
    var stackPanel = document.createElement("div");
    inObjectHtml.appendChild(stackPanel);
    var btn2 = new TVclInputbutton(stackPanel, "");
    btn2.Text = "Iniciar camara";
    btn2.VCLType = TVCLType.BS;
    btn2.onClick = function () {
        panelImage.style.display = "";
        _this.VclCamera.InitilizeCamera(panelImage);
        _this.VclCamera.OnTakeSnapshot = function (Component, data_uri) {
            panelFormat.innerHTML =
                '<h3>Imagen 64x64:</h3>' +
                '<img id="imagendata" src="' + data_uri + '"/>';
        }
    }

    var btn3 = new TVclInputbutton(stackPanel, "");
    btn3.Text = "Tomar foto";
    btn3.VCLType = TVCLType.BS;
    btn3.onClick = function () {
        panelImage.style.display = "none";
        _this.VclCamera.TakeSnapshot();
    }

    var ContControl = new TVclStackPanel(stackPanel, "1", 2, [[6, 6], [6, 6], [6, 6], [6, 6], [6, 6]]);
    var panelImage = ContControl.Column[0].This;
    var panelFormat = ContControl.Column[1].This;
    
    _this.VclCamera = new ItHelpCenter.Componet.ImageControls.TVclCamera(null, null);
    $('html, body').animate({ scrollTop: 0 }, 'slow');
}