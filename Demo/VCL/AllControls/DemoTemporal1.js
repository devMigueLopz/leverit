﻿ItHelpCenter.Demo.TDemoTemporal1 = function (inObjectHtml, _this) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();

    this.sp1 = new TVclStackPanel(inObjectHtml, "div1", 1, [[12], [12], [12], [12], [12]]);
    this.sp1.Row.This.style.marginBottom = "15px";
    this.sp1.Row.This.style.height = "200px";
    this.sp1.Row.This.style.backgroundColor = "#F57F17";
    this.etiquetasp1 = new TVcllabel(this.sp1.Column[0].This, "", TlabelType.H0);
    this.etiquetasp1.Text = "Drag element...";


    this.sp2 = new TVclStackPanel(inObjectHtml, "div1", 1, [[12], [12], [12], [12], [12]]);
    this.sp2.Row.This.style.marginBottom = "15px";
    this.sp2.Row.This.style.height = "200px";
    this.sp2.Row.This.style.backgroundColor = "#FFEB3B";
    this.etiquetasp2 = new TVcllabel(this.sp2.Column[0].This, "", TlabelType.H0);
    this.etiquetasp2.Text = "Drag element...";

    this.sp3 = new TVclStackPanel(inObjectHtml, "1", 1, [[12], [12], [12], [12], [12]]);
    this.sp3.Row.This.style.marginBottom = "15px";
    this.sp3.Row.This.style.height = "50px";

    this.Memo1 = new TVclMemo(this.sp3.Column[0].This, ""); //CREA UN MEMO CONTROL 
    this.Memo1.VCLType = TVCLType.BS;
    
    this.btnInsert = new TVclButton(this.sp3.Column[0].This, "");
    this.btnInsert.Text = "Insert";
    this.btnInsert.VCLType = TVCLType.BS;
    this.etiqueta = new TVcllabel(this.sp3.Column[0].This, "", TlabelType.H0);
    this.etiqueta.Text = "Temporal 1"
    this.txtTextBox = new TVclTextBox(this.sp3.Column[0].This, "");
           

    /*************************************DRAG************************/
    _this.Drag = new TVclDrag("");
    //Contenedor donde se arrastran los elementos
    _this.Drag.addEventContendor(this.sp1, this.sp1.Row.This, function (Content, Component, ev) {
        alert("Elemento arratrado al contenedor 1");
        var data = ev.dataTransfer.getData("text");
        _this.sp1.Row.This.appendChild(document.getElementById(data));
    });
    _this.Drag.addEventContendor(this.sp2, this.sp2.Row.This, function (Content, Component, ev) {
        alert("Elemento arratrado al contenedor 2");
        var data = ev.dataTransfer.getData("text");
        _this.sp2.Row.This.appendChild(document.getElementById(data));
    });

    //Elementos que se podran arrastrar
    _this.Drag.addDragable(this.btnInsert, this.btnInsert.This);
    _this.Drag.addDragable(this.etiqueta, this.etiqueta.This);
    _this.Drag.addDragable(this.txtTextBox, this.txtTextBox.This);
    _this.Drag.addDragable(this.Memo1, this.Memo1.This);
    

}