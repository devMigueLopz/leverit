﻿ItHelpCenter.Demo.TDemoMaps = function (inObjectHtml, _this) {

    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    var LineaPrincipal = new TVclStackPanel(inObjectHtml, "1", 1, [[12], [12], [12], [12], [12]]);
    var Linea1 = new TVclStackPanel(LineaPrincipal.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
    var btn2 = new TVclInputbutton(Linea1.Column[0].This, "");
    btn2.Text = "Load Map - Google";
    btn2.VCLType = TVCLType.BS;
    btn2.onClick = function() {
        btnGetLocation.Enabled = true;
        VclGeolocationChrome.OnGetLocation = function(position) {
            alert(position);
            VclGeolocationChrome.setMapa(position);
        }
        VclGeolocationChrome.OnDragend = function(position) {
            _this.VclTextBoxPositionLatitud.Text = position.lat();
            _this.VclTextBoxPositionLongitud.Text = position.lng();
        }        
        VclGeolocationChrome.Initilize(function(VclGeolocationA) {
        });
    }
    var btnGetLocation = new TVclInputbutton(Linea1.Column[0].This, "");
    btnGetLocation.Text = "Get Location";
    btnGetLocation.Enabled = false;
    btnGetLocation.VCLType = TVCLType.BS;
    btnGetLocation.onClick = function() {
       VclGeolocationChrome.GetLocation();
    }
    var etiqueta_br = document.createElement("br");
    var div1 = document.createElement("div");
    var div2 = document.createElement("div");
    Linea1.Column[0].This.appendChild(etiqueta_br);
    Linea1.Column[0].This.appendChild(div1);
    Linea1.Column[0].This.appendChild(div2);
    this.LabelLatitud = new TVcllabel(div1, "", TlabelType.H0);
    this.VclTextBoxPositionLatitud = new TVclTextBox(div1, "");
    this.LabelLongitud = new TVcllabel(div2, "", TlabelType.H0);
    this.VclTextBoxPositionLongitud = new TVclTextBox(div2, "");
    this.VclTextBoxPositionLatitud.Enabled = false;
    this.VclTextBoxPositionLongitud.Enabled = false;
    //this.LabelLatitud.Text = "Lat  : "
    //this.LabelLongitud.Text = "Lng : ";


    var VclGeolocationChrome = new ItHelpCenter.Componet.MapsControls.TVclGeolocationChrome(Linea1.Column[0].This, "");
    var Linea2 = new TVclStackPanel(LineaPrincipal.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
    var Linea3 = new TVclStackPanel(Linea2.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
    var Linea4 = new TVclStackPanel(Linea2.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
    var btnGetLocation2 = new TVclInputbutton(Linea3.Column[0].This, "");
    btnGetLocation2.This.style.marginTop = "50px";
    btnGetLocation2.Text = "Load Map - Open Layout";
    //btnGetLocation2.Enabled = false;
    btnGetLocation2.VCLType = TVCLType.BS;
    btnGetLocation2.onClick = function() {
        btnGetLocation2.Enabled = false;
        var VclGeolocation = new ItHelpCenter.Componet.MapsControls.TVclGeolocation(Linea4.Column[0].This, "");
        VclGeolocation.OnGetLocation = function(position) {
            alert(position);
        }
        VclGeolocation.Initilize(function(VclGeolocationA) {
            VclGeolocation.GetLocation();
        });
    }
    
    $('html, body').animate({ scrollTop: 0 }, 'slow');

}