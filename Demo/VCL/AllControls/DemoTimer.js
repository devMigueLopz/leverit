﻿ItHelpCenter.Demo.TDemoTimer = function (inObjectHtml, _this) {
    var Timer = new TVclTimer();

    var contTimer = new TVclStackPanel(inObjectHtml, "1", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [4, 8]]);
    contTimer.Row.This.style.marginTop = "20px";

    var lblLabel1 = new TVcllabel(contTimer.Column[0].This, "", TlabelType.H0);
    lblLabel1.Text = "Agregar Funciones";
    lblLabel1.VCLType = TVCLType.BS;

    var btn1 = new TVclInputbutton(contTimer.Column[1].This, "");
    btn1.Text = "Iniciar"
    btn1.VCLType = TVCLType.BS;
    btn1.onClick = function () {
        Timer.AddMethod("LanzarMensaje1", LanzarMensaje1, 2000);
        Timer.AddMethod("LanzarMensaje2", LanzarMensaje2, 2000);
        Timer.AddMethod("LanzarMensaje3", LanzarMensaje3, 2000);
        Timer.AddMethod("LanzarMensaje4", LanzarMensaje4, 2000);
    }

    var conTimer2 = new TVclStackPanel(inObjectHtml, "1", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [4, 8]]);
    conTimer2.Row.This.style.marginTop = "20px";

    var lblLabel2 = new TVcllabel(conTimer2.Column[0].This, "", TlabelType.H0);
    lblLabel2.Text = "Save File";
    lblLabel2.VCLType = TVCLType.BS;

    var btn2 = new TVclInputbutton(conTimer2.Column[1].This, "");
    btn2.Text = "Delete";
    btn2.VCLType = TVCLType.BS;
    btn2.onClick = function () {
        Timer.RemoveMethod("LanzarMensaje3");
    }

    var conTimer3 = new TVclStackPanel(inObjectHtml, "1", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [4, 8]]);

    var LanzarMensaje1 = function () {
        $(conTimer3.Column[1].This).append("Soy la funcion Nro 1 <br />");
    }

    var LanzarMensaje2 = function () {
        $(conTimer3.Column[1].This).append("Soy la funcion Nro 2 <br />");
    }

    var LanzarMensaje3 = function () {
        $(conTimer3.Column[1].This).append("Soy la funcion Nro 3 <br />");
    }

    var LanzarMensaje4 = function () {
        $(conTimer3.Column[1].This).append("Soy la funcion Nro 4 <br />");
    }
}


