﻿ItHelpCenter.Demo.TDemoCaptcha = function (inObjectHtml, _this) {

    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.Mythis = "TDemoCaptcha";
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1", "Send");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2", "Captcha correct");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3", "Captcha invalid");
    this.VclCamera = null;
    var stackPanel1 = document.createElement("div");
    inObjectHtml.appendChild(stackPanel1);
    var br1 = document.createElement("hr");
    inObjectHtml.appendChild(br1);
    var stackPanel2 = document.createElement("div");
    inObjectHtml.appendChild(stackPanel2);
    var br2 = document.createElement("hr");
    inObjectHtml.appendChild(br2);
    var stackPanel3 = document.createElement("div");
    inObjectHtml.appendChild(stackPanel3);

    var VclInputbutton = new TVclInputbutton(stackPanel1, "");
    VclInputbutton.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1");
    VclInputbutton.VCLType = TVCLType.BS;
    VclInputbutton.onClick = function () {
        var respuesta = _this.VclRecaptcha.Send();/*Enviamos captcha*/    
        _this.ShowMessage(respuesta);
    }
    var VclInputbutton2 = new TVclInputbutton(stackPanel2, "");
    VclInputbutton2.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1");
    VclInputbutton2.VCLType = TVCLType.BS;
    VclInputbutton2.onClick = function () {
        var respuesta = _this.VclRecaptcha2.Send();/*Enviamos captcha*/  
        _this.ShowMessage(respuesta);
    }

    var VclInputbutton3 = new TVclInputbutton(stackPanel3, "");
    VclInputbutton3.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1");
    VclInputbutton3.VCLType = TVCLType.BS;
    VclInputbutton3.onClick = function () {
        var respuesta = _this.VclRecaptcha3.Send();/*Enviamos captcha*/
        _this.ShowMessage(respuesta);
    }
    _this.VclRecaptcha = new ItHelpCenter.Componet.Captcha.TVclRecaptcha(stackPanel1, null);
    _this.VclRecaptcha.Initilize();

    _this.VclRecaptcha2 = new ItHelpCenter.Componet.Captcha.TVclCaptchaNative(stackPanel2, null);
    _this.VclRecaptcha2.Style = ItHelpCenter.Componet.Captcha.TStyle.StyleLetter;
    _this.VclRecaptcha2.Initilize();

    _this.VclRecaptcha3 = new ItHelpCenter.Componet.Captcha.TVclCaptchaNative(stackPanel3, null);
    _this.VclRecaptcha3.Style = ItHelpCenter.Componet.Captcha.TStyle.StyleOperation;
    _this.VclRecaptcha3.BackgroundColor ="#FBDC67";
    _this.VclRecaptcha3.Initilize();
    $('html, body').animate({ scrollTop: 0 }, 'slow');
    this.ShowMessage = function (respuesta) {
        if (respuesta) {
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2"));
        } else {
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3"));
        }
    }
}
