﻿//graficos
ItHelpCenter.Demo.TDemoPanelService = function (inObjectHtml, _this) {

    this.TParent = function () {
        return this;
    }.bind(this);

    this._ObjectHtml = inObjectHtml;

    var _this = this.TParent();
    var Container = new TVclStackPanel(_this._ObjectHtml, 'TabbedId', 1, [[12], [12], [12], [12], [12]]);

     //FIX DE PAGE
    var fix = false;
    var functionFix = null;
    if (Source.Menu.IsMobil) {
        functionFix = function () {
            if (fix) {
                $("#asideleft_idmain").show();
                $("#Cont2_idmain_0").addClass('burger_visible');
            }
        }
        if ($("#Cont2_idmain_0").hasClass("burger_visible")) {
            $("#asideleft_idmain").hide();
            $("#Cont2_idmain_0").removeClass('burger_visible');
            fix = true;
        }
        // Container.Row.This.style.marginLeft = '-20px';
    }
    //FIX DE PAGE


    var Container_1 = Container.Column[0].This;
    var PanelService = new Componet.TPanelServices(Container_1, "TabPanel", functionFix);

    //var btn2 = new TVclInputbutton(_this._ObjectHtml, "");
    //btn2.Text = "Add Data";
    //btn2.VCLType = TVCLType.BS;
    //btn2.onClick = function () {
    //    var PanelServiceProfiler = new Persistence.PanelService.TPanelServiceProfiler();
    //    PanelServiceProfiler.Fill();
    //    if (PanelServiceProfiler.PSGROUPList.length === 0) {
    //        PanelServiceProfiler.CreateAll();
    //    }
    //}
    //btn2.This.style.marginTop ="20px"
    var ArrayElements = [
	{
	    position: 1,
	    id: "1x",
	    // image: "img/img_tab/tabImage/system.png",
	    image: "img/img_tab/tabImage/01.png",
		title: "Sistemas",
	    // title: "Sistemas de manufactura para leverit y sus compañias para do lo que quieras y todo lo que gustes",// todo lo que quieras y todo lo que gustes
	    content:
		[
			{
			    contentPosition: 1,
			    id: "1_content_1x",
			    contentImage: "img/img_tab/contentImage/computer.png",
			    contentTitle: "COMPUTADORA Y ACCESORIOS",
			    contentDescription: "Soporte a equipos de escritorio, portátiles, monitores, teclado, mouse y accesorios",
			    contentPhrase: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore officia in vel quidem commodi voluptatibus molestiae"
			},
			{
			    contentPosition: 2,
			    id: "1_content_2x",
			    contentImage: "img/img_tab/contentImage/email_.png",
			    contentTitle: "CORREO ELECTRÓNICO CORP.",
			    contentDescription: "Soporte al correo corporativo",
			    contentPhrase: "DemoPhrase1_2"
			},
			{
			    contentPosition: 3,
			    id: "1_content_3x",
			    contentImage: "img/img_tab/contentImage/ms-office.png",
			    contentTitle: "APLICACIONES DE OFICINA",
			    contentDescription: "Soporte a las aplicaciones de Office",
			    contentPhrase: "DemoPhrase1_3"
			}, {
			    contentPosition: 3,
			    id: "1_content_3x",
			    contentImage: "img/img_tab/contentImage/erp.png",
			    contentTitle: "ERP EMPRESARIAL",
			    contentDescription: "Soporte al ERP Empresarial con todos sus módulos",
			    contentPhrase: "DemoPhrase1_4"
			}
		]
	},
	{
	    position: 2,
	    id: "2x",
	    image: "img/img_tab/tabImage/rrhh.png",
	    title: "RRHH",
	    content:
		[
			{
			    contentPosition: 1,
			    id: "2_content_1x",
			    contentImage: "img/img_tab/contentImage/headset.png",
			    contentTitle: "DemoTitle2_1",
			    contentDescription: "DemoDescription2_1",
			    contentPhrase: "DemoPhrase2_1"
			},
			{
			    contentPosition: 2,
			    id: "2_content_2x",
			    contentImage: "img/img_tab/contentImage/headset.png",
			    contentTitle: "DemoTitle2_2",
			    contentDescription: "DemoDescription2_2",
			    contentPhrase: "DemoPhrase2_2"
			},
			{
			    contentPosition: 3,
			    id: "2_content_3x",
			    contentImage: "img/img_tab/contentImage/headset.png",
			    contentTitle: "DemoTitle2_3",
			    contentDescription: "DemoDescription2_3",
			    contentPhrase: "DemoPhrase2_3"
			}
		]
	},
	{
	    position: 3,
	    id: "3x",
	    image: "img/img_tab/tabImage/contability.png",
	    title: "Contabilidad",
	    content:
		[
			{
			    contentPosition: 1,
			    id: "3_content_1x",
			    contentImage: "img/img_tab/contentImage/headset.png",
			    contentTitle: "DemoTitle3_1",
			    contentDescription: "DemoDescription3_1",
			    contentPhrase: "DemoPhrase3_1"
			},
			{
			    contentPosition: 2,
			    id: "3_content_2x",
			    contentImage: "img/img_tab/contentImage/headset.png",
			    contentTitle: "DemoTitle3_2",
			    contentDescription: "DemoDescription3_2",
			    contentPhrase: "DemoPhrase3_2"
			},
			{
			    contentPosition: 3,
			    id: "3_content_3x",
			    contentImage: "img/img_tab/contentImage/headset.png",
			    contentTitle: "DemoTitle3_3",
			    contentDescription: "DemoDescription3_3",
			    contentPhrase: "DemoPhrase3_3"
			}
		]
	},
	{
	    position: 4,
	    id: "4x",
	    image: "img/img_tab/tabImage/resource.png",
	    title: "Recursos Físicos",
	    content:
		[
			{
			    contentPosition: 1,
			    id: "4_content_1x",
			    contentImage: "img/img_tab/contentImage/headset.png",
			    contentTitle: "DemoTitle4_1",
			    contentDescription: "DemoDescription4_1",
			    contentPhrase: "DemoPhrase4_1"
			},
			{
			    contentPosition: 2,
			    id: "4_content_2x",
			    contentImage: "img/img_tab/contentImage/headset.png",
			    contentTitle: "DemoTitle4_2",
			    contentDescription: "DemoDescription4_2",
			    contentPhrase: "DemoPhrase4_2"
			},
			{
			    contentPosition: 3,
			    id: "4_content_3x",
			    contentImage: "img/img_tab/contentImage/headset.png",
			    contentTitle: "DemoTitle4_3",
			    contentDescription: "DemoDescription4_3",
			    contentPhrase: "DemoPhrase4_3"
			}
		]
	},
	{
	    position: 5,
	    id: "5x",
	    image: "img/img_tab/tabImage/finance.png",
	    title: "Finanzas",
	    content:
		[
			{
			    contentPosition: 1,
			    id: "5_content_1x",
			    contentImage: "img/img_tab/contentImage/headset.png",
			    contentTitle: "DemoTitle5_1",
			    contentDescription: "DemoDescription5_1",
			    contentPhrase: "DemoPhrase5_1"
			},
			{
			    contentPosition: 2,
			    id: "5_content_2x",
			    contentImage: "img/img_tab/contentImage/headset.png",
			    contentTitle: "DemoTitle5_2",
			    contentDescription: "DemoDescription5_2",
			    contentPhrase: "DemoPhrase5_2"
			},
			{
			    contentPosition: 3,
			    id: "5_content_3x",
			    contentImage: "img/img_tab/contentImage/headset.png",
			    contentTitle: "DemoTitle5_3",
			    contentDescription: "DemoDescription5_3",
			    contentPhrase: "DemoPhrase5_3"
			}
		]
	},
	{
	    position: 6,
	    id: "6x",
	    image: "img/img_tab/tabImage/sales.png",
	    title: "Ventas",
	    content:
		[
			{
			    contentPosition: 1,
			    id: "6_content_1x",
			    contentImage: "img/img_tab/contentImage/headset.png",
			    contentTitle: "DemoTitle6_1",
			    contentDescription: "DemoDescription6_1",
			    contentPhrase: "DemoPhrase6_1"
			},
			{
			    contentPosition: 2,
			    id: "6_content_2x",
			    contentImage: "img/img_tab/contentImage/headset.png",
			    contentTitle: "DemoTitle6_2",
			    contentDescription: "DemoDescription6_2",
			    contentPhrase: "DemoPhrase6_2"
			},
			{
			    contentPosition: 3,
			    id: "6_content_3x",
			    contentImage: "img/img_tab/contentImage/headset.png",
			    contentTitle: "DemoTitle6_3",
			    contentDescription: "DemoDescription6_3",
			    contentPhrase: "DemoPhrase6_3"
			}
		]
	},
	{
	    position: 7,
	    id: "7x",
	    image: "img/img_tab/tabImage/comunication.png",
	    title: "Comunicaciones",
	    content:
		[
			{
			    contentPosition: 1,
			    id: "7_content_1x",
			    contentImage: "img/img_tab/contentImage/headset.png",
			    contentTitle: "DemoTitle7_1",
			    contentDescription: "DemoDescription7_1",
			    contentPhrase: "DemoPhrase7_1"
			},
			{
			    contentPosition: 2,
			    id: "7_content_2x",
			    contentImage: "img/img_tab/contentImage/headset.png",
			    contentTitle: "DemoTitle7_2",
			    contentDescription: "DemoDescription7_2",
			    contentPhrase: "DemoPhrase7_2"
			},
			{
			    contentPosition: 3,
			    id: "7_content_3x",
			    contentImage: "img/img_tab/contentImage/headset.png",
			    contentTitle: "DemoTitle7_3",
			    contentDescription: "DemoDescription7_3",
			    contentPhrase: "DemoPhrase7_3"
			}
		]
	},
	{
	    position: 8,
	    id: "8x",
	    image: "img/img_tab/tabImage/incidents.png",
	    title: "Incidencias",
	    content:
		[
			{
			    contentPosition: 1,
			    id: "8_content_1x",
			    contentImage: "img/img_tab/contentImage/headset.png",
			    contentTitle: "DemoTitle8_1",
			    contentDescription: "DemoDescription8_1",
			    contentPhrase: "DemoPhrase8_1"
			},
			{
			    contentPosition: 2,
			    id: "8_content_2x",
			    contentImage: "img/img_tab/contentImage/headset.png",
			    contentTitle: "DemoTitle8_2",
			    contentDescription: "DemoDescription8_2",
			    contentPhrase: "DemoPhrase8_2"
			},
			{
			    contentPosition: 3,
			    id: "8_content_3x",
			    contentImage: "img/img_tab/contentImage/headset.png",
			    contentTitle: "DemoTitle8_3",
			    contentDescription: "DemoDescription8_3",
			    contentPhrase: "DemoPhrase8_3"
			}
		]
	},
	{
	    position: 9,
	    id: "9x",
	    image: "img/img_tab/tabImage/reports.png",
	    title: "Reportes",
	    content:
		[
			{
			    contentPosition: 1,
			    id: "9_content_1x",
			    contentImage: "img/img_tab/contentImage/headset.png",
			    contentTitle: "DemoTitle9_1",
			    contentDescription: "DemoDescription9_1",
			    contentPhrase: "DemoPhrase9_1"
			},
			{
			    contentPosition: 2,
			    id: "9_content_2x",
			    contentImage: "img/img_tab/contentImage/headset.png",
			    contentTitle: "DemoTitle9_2",
			    contentDescription: "DemoDescription9_2",
			    contentPhrase: "DemoPhrase9_2"
			},
			{
			    contentPosition: 3,
			    id: "9_content_3x",
			    contentImage: "img/img_tab/contentImage/headset.png",
			    contentTitle: "DemoTitle9_3",
			    contentDescription: "DemoDescription9_3",
			    contentPhrase: "DemoPhrase9_3"
			}
		]
	}
    ];
    try {
        for (var i = 0; i < ArrayElements.length; i++) {
            var Element = new Componet.TElements.TPanelServicesElementTab();
            Element.Id = ArrayElements[i]["id"];
            Element.Position = ArrayElements[i]["position"];
            Element.Image = ArrayElements[i]["image"];
            Element.Title = ArrayElements[i]["title"];
            PanelService.TPanelServicesElementsAdd(Element);
            for (var j = 0; j < ArrayElements[i].content.length; j++) {
                var ElementContent = new Componet.TElements.TPanelServicesElementTabContent();
                ElementContent.ContentId = ArrayElements[i].content[j]["id"];
                ElementContent.ContentPosition = ArrayElements[i].content[j]["contentPosition"];
                ElementContent.ContentImage = ArrayElements[i].content[j]["contentImage"];
                ElementContent.ContentTitle = ArrayElements[i].content[j]["contentTitle"];
                ElementContent.ContentDescription = ArrayElements[i].content[j]["contentDescription"];
                ElementContent.ContentPhrase = ArrayElements[i].content[j]["contentPhrase"];

                PanelService.Elements[i].TPanelServiceElementsContentAdd(ElementContent);
            }
        }
        PanelService.Orientation = PanelService.OrientationType.Horizontal;
        PanelService.Load();
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoPanelService.js ItHelpCenter.Demo.TDemoPanelService", e);
    }

    /*Agregar Texto o Componentes dentro de los Paneles*/
    // var texto = new Uspan(PService.Elements[0].This,'','Texto de Prueba dentro del Tab Panel');
    // var texto = new Uspan(PService.Elements[0].ElementsContent[0].This,'','Texto de Prueba dentro del DropDownPanel');
}