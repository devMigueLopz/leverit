﻿ItHelpCenter.Demo.TDemoGridControl = function (inObjectHtml, _this) {
    //************* INICIO TAB DATAGRID ***********************************/// 
    var ContDataGrid = new TVclStackPanel(inObjectHtml, "1", 1, [[12], [12], [12], [12], [12]]);
    ContDataGrid.Row.This.style.marginTop = "20px";

    var Grilla = new TGrid(ContDataGrid.Column[0].This, "", ""); //CREA UN GRID CONTROL

    var Col0 = new TColumn(); //CREA UNA COLUMNA PARA AGREGAR AL GRID CONTROL
    Col0.Name = "Check"; //ASIGNA O EXTRAE EL NOMBRE DE LA COLUMNA
    Col0.Caption = "Check"; //ASIGNA O EXTRAE EL TEXTO QUE SE MOSTRARA EN LA COLUMNA 
    Col0.Index = 0; // ASGINA O EXTRAE EL INDICE DE LA COLUMNA 
    Col0.EnabledEditor = true; //ASIGNA O EXTRAE UN VALO TRUE O FALSE QUE INDICA SI LA COLUMNA MOSTRARA UN EDITOR DE DATOS
    Col0.DataType = SysCfg.DB.Properties.TDataType.Boolean; //ASIGNA O EXTRAE UN VALOR DE TIPO DE DATO QUE TIENEN LA COLUMNA 
    Col0.ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.None; //ASIGNA O EXTRAE UN ESTILO DE EDITOR Q MOSTRARA EN EL CASO EL ENABLEEDITOR ESTE ACTIVADO
    Grilla.AddColumn(Col0); //AGREGA UNA COLUMNA AL GRIDCONTROL 
    var Col1 = new TColumn();
    Col1.Name = "Fecha";
    Col1.Caption = "Fecha";
    Col1.Index = 1;
    Col1.EnabledEditor = true;
    Col1.DataType = SysCfg.DB.Properties.TDataType.DateTime;
    Col1.ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.None;
    Grilla.AddColumn(Col1);

    for (var i = 0; i < 4; i++) {
        var Col = new TColumn();
        Col.Name = "Columna" + (i + 2);
        Col.Caption = "Columna " + (i + 2);
        Col.Index = i + 2;
        Col.DataType = SysCfg.DB.Properties.TDataType.String;
        Grilla.AddColumn(Col);
    }

    for (var i = 0; i < 3; i++) {
        var NewRow = new TRow(); //CREA UN ROW PARA AGREGAR AL GRIDCONTROL 

        var Cell0 = new TCell(); //CREA UNA CELDA PARA QUE SE AGREGA AL ROW
        Cell0.Value = false; // ASIGNA O EXTRAE EL VALOR QUE SE MOSTRARA EN LA CELDA DEL GRIDCONTROL 
        Cell0.IndexColumn = 0; // ASIGNA O EXTRAE EL INDEX DE LA COLUMNA 
        Cell0.IndexRow = i; // ASIGNA O EXTRAE EL INDEX DEL ROW 
        NewRow.AddCell(Cell0); // AGREGA O EXTRAE LA CELDA AL ROW

        var Cell1 = new TCell();
        Cell1.Value = "10/18/2017 12:00 am";
        Cell1.IndexColumn = 0 + 1;
        Cell1.IndexRow = i;
        NewRow.AddCell(Cell1);

        for (var j = 0; j < 4; j++) {
            var Cell = new TCell();
            if (j == 0) {
                Cell.Value = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
            } else {
                Cell.Value = "Row " + i + " | Columna " + (j + 2);
            }

            
            Cell.IndexColumn = j + 2;
            Cell.IndexRow = i;
            NewRow.AddCell(Cell);
        }
        NewRow.onclick = function () { // ASIGNA UN EVENTO CUANDO SE HACE CLICK EN EL ROW DEL GRIDCONTROL
            Grilla_rowclick(NewRow);
        }
        Grilla.AddRow(NewRow);
    }

    var Grilla_rowclick = function (inRow) {
        alert(inRow.Cells[1].Value);
    }
    //************* FIN TAB DATAGRID ***********************************/// 
}