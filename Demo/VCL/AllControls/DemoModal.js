﻿ItHelpCenter.Demo.TDemoModal = function (inObjectHtml, _this) {

    var Modal = new TVclModal(document.body, inObjectHtml, "");

    Modal.AddHeaderContent("Titulo del modal");

    var btn2 = new TVclInputbutton(inObjectHtml, "");
    btn2.Text = "Show Modal";
    btn2.VCLType = TVCLType.BS;
    btn2.onClick = function () {
        Modal.ShowModal();
    }



    //************ INICIO DE LABEL ***********************************************
    var ConLabelControl = new TVclStackPanel(Modal.Body.This, "1", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
    ConLabelControl.Row.This.style.marginTop = "20px";

    var lblLabel = new TVcllabel(ConLabelControl.Column[0].This, "", TlabelType.H0);
    lblLabel.Text = "Label";
    lblLabel.VCLType = TVCLType.BS;

    var lblLabelControl = new TVcllabel(ConLabelControl.Column[1].This, "", TlabelType.H0);//CREA UN LABEL CONTROL 
    lblLabelControl.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblLabel"); // PROPIEDAD PARA ASIGNAR O EXTRAER EL TEXTO  
    lblLabelControl.VCLType = TVCLType.BS; // PROPIEDAD PARA EL ESTILO DE LA LIBRERIA UTILIZADA (BOOTSTRAP, ETC)
    //************ FIN DE LABEL ***********************************************

    //************ INICIO DE TEXTBOX ***********************************************
    var ConTextControl = new TVclStackPanel(Modal.Body.This, "1", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
    ConTextControl.Row.This.style.marginTop = "20px";

    var lblLabel = new TVcllabel(ConTextControl.Column[0].This, "", TlabelType.H0);
    lblLabel.Text = "TextBox";
    lblLabel.VCLType = TVCLType.BS;

    var txtTextBox = new TVclTextBox(ConTextControl.Column[1].This, "txtExample"); //CREA UN TEXTBOX CONTROL 
    txtTextBox.Text = "text in TextBox" // PROPIEDAD PARA ASIGNAR O EXTRAER EL TEXTO 
    txtTextBox.VCLType = TVCLType.BS;   // PROPIEDAD PARA DARLE EL ESTILO DE LA LIBRERIA UTILIZADA (BOOTSTRAP, ETC)
    //************ FIN DE TEXTBOX ***********************************************   

    //************ INICIO DE CHECKBOX ***********************************************
    var ConCheckBoxControl = new TVclStackPanel(Modal.Body.This, "1", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
    ConCheckBoxControl.Row.This.style.marginTop = "20px";

    var lblCheckBox = new TVcllabel(ConCheckBoxControl.Column[0].This, "", TlabelType.H0);
    lblCheckBox.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblcheckbox");
    lblCheckBox.VCLType = TVCLType.BS;

    var chkCheckBox = new TVclInputcheckbox(ConCheckBoxControl.Column[1].This, ""); //CREA UN CHECKBOX
    chkCheckBox.Checked = true; //PROPIEDAD PARA ASIGNARLE CHECK A TRUE O FALSE
    chkCheckBox.VCLType = TVCLType.BS; // PROPIEDAD PARA DARLE EL ESTILO DE LA LIBRERIA UTILIZADA (BOOTSTRAP, ETC)
    //************ FIN DE CHECKBOX ***********************************************

    //************ INICIO DE MEMO ***********************************************
    var ConMemoControl = new TVclStackPanel(Modal.Body.This, "1", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
    ConMemoControl.Row.This.style.marginTop = "20px";

    var lblMemo = new TVcllabel(ConMemoControl.Column[0].This, "", TlabelType.H0);
    lblMemo.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblMemo");
    lblMemo.VCLType = TVCLType.BS;

    var Memo1 = new TVclMemo(ConMemoControl.Column[1].This, ""); //CREA UN MEMO CONTROL 
    Memo1.Text = "Write a text";    // PROPIEDAD PARA ASIGNAR O EXTRAER EL TEXTO
    Memo1.VCLType = TVCLType.BS;    // PROPIEDAD PARA DARLE EL ESTILO DE LA LIBRERIA UTILIZADA (BOOTSTRAP, ETC) 
    Memo1.Rows = 5;                 // PROPIEDAD PARA DARLE EL ALTO AL CONTROL 
    Memo1.Cols = 50;                // PROPIEDAD PARA DARLE EL ANCHO AL CONTROL
    //************ FIN DE MEMO ***********************************************

    //************ INICIO DE BUTTON **********************************************
    var btnButton = new TVclInputbutton("", ""); //CREA UN BOTON 
    btnButton.Text = "Cerrar Modal"; // PROPIEDAD PARA ASIGNAR O EXTRAER EL TEXTO
    btnButton.VCLType = TVCLType.BS; // PROPIEDAD PARA DARLE EL ESTILO DE LA LIBRERIA UTILIZADA (BOOTSTRAP, ETC)
    btnButton.onClick = function () { // ASIGNAR EL EVENTO CUANDO SE HAGA CLIC EN EL BOTON
        Modal.CloseModal();
    }

    Modal.AddFooterContent(btnButton.This);
    //************ FIN DE BUTTON ***********************************************


    var Modal2 = new TVclModal(document.body, null, "");
    Modal2.AddHeaderContent("Titulo del modal 2");
    Modal2.AddContent("Contenido del modal 2");


    //************ INICIO DE BUTTON **********************************************
    var btnButton20 = new TVclInputbutton("", ""); //CREA UN BOTON 
    btnButton20.Text = "Cerrar Modal";// PROPIEDAD PARA ASIGNAR O EXTRAER EL TEXTO
    btnButton20.VCLType = TVCLType.BS; // PROPIEDAD PARA DARLE EL ESTILO DE LA LIBRERIA UTILIZADA (BOOTSTRAP, ETC)
    btnButton20.onClick = function () { // ASIGNAR EL EVENTO CUANDO SE HAGA CLIC EN EL BOTON
        Modal2.CloseModal();
    }

    Modal2.AddFooterContent(btnButton20.This);
    //************ FIN DE BUTTON ***********************************************

    var btn20 = new TVclInputbutton(inObjectHtml, "");
    btn20.Text = "Show Modal float";
    btn20.VCLType = TVCLType.BS;
    btn20.onClick = function () {
        Modal2.ShowModal();
    }
    $(inObjectHtml).append(" <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />");
}