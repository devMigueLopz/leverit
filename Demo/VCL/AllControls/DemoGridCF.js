﻿ItHelpCenter.Demo.TDemoGridCF = function (inObjectHtml, _this) {

    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.DataSet = new SysCfg.MemTable.TDataSet();
    this.DataSet.Inicialize(true);
    //agrega definicion de campos
    this.DataSet.EnableControls = false;
    this.DataSet.AddFields("Nombre", SysCfg.DB.Properties.TDataType.String, 10);
    this.DataSet.AddFields("Mensaje", SysCfg.DB.Properties.TDataType.String, 100);
    this.DataSet.AddFields("Documento", SysCfg.DB.Properties.TDataType.String, 100);
    this.DataSet.AddFields("Edad", SysCfg.DB.Properties.TDataType.Int32, 4);
    this.DataSet.AddFields("Sueldo", SysCfg.DB.Properties.TDataType.Double, 8);
    this.DataSet.AddFields("Credito", SysCfg.DB.Properties.TDataType.Boolean, 1);
    this.DataSet.AddFields("Nacimiento", SysCfg.DB.Properties.TDataType.DateTime, 8);
    this.DataSet.EnableControls = false;
    this.DataSet.Append();
    this.DataSet.FieldsIndex(0, "Salvador");
    this.DataSet.FieldsIndex(1, "Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño.");
    this.DataSet.FieldsIndex(2, "001(1,8,8,10,20)1File_DocIWHVDUUK2018-11-07C:\fakepath\File_Doc");
    this.DataSet.FieldsIndex(3, 36);
    this.DataSet.FieldsIndex(4, 10000.00);
    this.DataSet.FieldsIndex(5, true);
    this.DataSet.FieldsIndex(6, new Date("Thu Aug 13 2018 16:58:06 GMT-0500 (hora estándar de Colombia)"));
    this.DataSet.Post();
    this.DataSet.Append();
    this.DataSet.FieldsIndex(0, "Salvador");
    this.DataSet.FieldsIndex(1, "Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño.");
    this.DataSet.FieldsIndex(2, "001(1,16,0,10,0)1File Report.docx13/07/2018");
    this.DataSet.FieldsIndex(3, 37);
    this.DataSet.FieldsIndex(4, 10000.00);
    this.DataSet.FieldsIndex(5, true);
    this.DataSet.FieldsIndex(6, new Date("Thu Aug 07 2018 14:58:06 GMT-0500 (hora estándar de Colombia)"));
    this.DataSet.Post();
    this.DataSet.Append();
    this.DataSet.FieldsIndex(0, "Felipe");
    this.DataSet.FieldsIndex(1, "Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño.");
    this.DataSet.FieldsIndex(2, "001(1,16,0,10,0)1CAS-Anexos-2.doc13/07/2018");
    this.DataSet.FieldsIndex(3, 29);
    this.DataSet.FieldsIndex(4, 4000.00);
    this.DataSet.FieldsIndex(5, false);
    this.DataSet.FieldsIndex(6, new Date("Thu Aug 09 2018 18:58:06 GMT-0500 (hora estándar de Colombia)"));
    this.DataSet.Post();
    this.DataSet.Append();
    this.DataSet.FieldsIndex(0, "David");
    this.DataSet.FieldsIndex(1, "Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño.");
    this.DataSet.FieldsIndex(2, "001(1,30,0,10,3)1anexo06-declaracion-jurada.doc13/07/2018Doc");
    this.DataSet.FieldsIndex(3, 29);
    this.DataSet.FieldsIndex(4, 5000.00);
    this.DataSet.FieldsIndex(5, false);
    this.DataSet.FieldsIndex(6, "");
    this.DataSet.Post();
    //this.DataSet.Append();
    //this.DataSet.FieldsIndex(0, "Diestra");
    //this.DataSet.FieldsIndex(1, "Trujillo");
    //this.DataSet.FieldsIndex(2, 36);
    //this.DataSet.FieldsIndex(3, 4000.00);
    //this.DataSet.FieldsIndex(4, true);
    //this.DataSet.FieldsIndex(5, "Fri Jun 24 2005 15:49:06 GMT-0700 (Pacific Daylight Time)");
    //this.DataSet.Post();
    //this.DataSet.Append();
    //this.DataSet.FieldsIndex(0, "Mauricio");
    //this.DataSet.FieldsIndex(1, "Ancash");
    //this.DataSet.FieldsIndex(2, 25);
    //this.DataSet.FieldsIndex(3, 6000.00);
    //this.DataSet.FieldsIndex(4, false);
    //this.DataSet.FieldsIndex(5, "Fri Jun 24 2005 15:49:06 GMT-0700 (Pacific Daylight Time)");
    //this.DataSet.Post();
    //this.DataSet.Append();
    //this.DataSet.FieldsIndex(0, "Carlos");
    //this.DataSet.FieldsIndex(1, "Trujillo");
    //this.DataSet.FieldsIndex(2, 25);
    //this.DataSet.FieldsIndex(3, 6000.00);
    //this.DataSet.FieldsIndex(4, false);
    //this.DataSet.FieldsIndex(5, "Fri Jun 24 2005 15:49:06 GMT-0700 (Pacific Daylight Time)");
    //this.DataSet.Post();

    //this.DataSet.Append();
    //this.DataSet.FieldsIndex(0, "Sofia");
    //this.DataSet.FieldsIndex(1, "Trujillo");
    //this.DataSet.FieldsIndex(2, 40);
    //this.DataSet.FieldsIndex(3, 6000.00);
    //this.DataSet.FieldsIndex(4, true);
    //this.DataSet.FieldsIndex(5, "Fri Jun 24 2005 15:49:06 GMT-0700 (Pacific Daylight Time)");
    //this.DataSet.Post();

    //this.DataSet.Append();
    //this.DataSet.FieldsIndex(0, "Felipe");
    //this.DataSet.FieldsIndex(1, "Lima");
    //this.DataSet.FieldsIndex(2, 40);
    //this.DataSet.FieldsIndex(3, 6000.00);
    //this.DataSet.FieldsIndex(4, false);
    //this.DataSet.FieldsIndex(5, "Fri Jun 24 2005 15:49:06 GMT-0700 (Pacific Daylight Time)");
    //this.DataSet.Post();

    //this.DataSet.Append();
    //this.DataSet.FieldsIndex(0, "Carlos");
    //this.DataSet.FieldsIndex(1, "Lima");
    //this.DataSet.FieldsIndex(2, 36);
    //this.DataSet.FieldsIndex(3, 8000.00);
    //this.DataSet.FieldsIndex(4, true);
    //this.DataSet.FieldsIndex(5, "Fri Jun 24 2005 15:49:06 GMT-0700 (Pacific Daylight Time)");
    //this.DataSet.Post();

    //this.DataSet.Append();
    //this.DataSet.FieldsIndex(0, "Alejandra");
    //this.DataSet.FieldsIndex(1, "Mexico");
    //this.DataSet.FieldsIndex(2, 36);
    //this.DataSet.FieldsIndex(3, 6000.00);
    //this.DataSet.FieldsIndex(4, false);
    //this.DataSet.FieldsIndex(5, "Fri Jun 24 2005 15:49:06 GMT-0700 (Pacific Daylight Time)");
    //this.DataSet.Post();

    //this.DataSet.Append();
    //this.DataSet.FieldsIndex(0, "Alejandra");
    //this.DataSet.FieldsIndex(1, "Trujillo");
    //this.DataSet.FieldsIndex(2, 25);
    //this.DataSet.FieldsIndex(3, 6000.00);
    //this.DataSet.FieldsIndex(4, false);
    //this.DataSet.FieldsIndex(5, "Fri Jun 24 2005 15:49:06 GMT-0700 (Pacific Daylight Time)");
    //this.DataSet.Post();

    //this.DataSet.Append();
    //this.DataSet.FieldsIndex(0, "Francisco");
    //this.DataSet.FieldsIndex(1, "Piura");
    //this.DataSet.FieldsIndex(2, 25);
    //this.DataSet.FieldsIndex(3, 6000.00);
    //this.DataSet.FieldsIndex(4, false);
    //this.DataSet.FieldsIndex(5, "Fri Jun 24 2005 15:49:06 GMT-0700 (Pacific Daylight Time)");
    //this.DataSet.Post();

    //this.DataSet.Append();
    //this.DataSet.FieldsIndex(0, "Ultimo");
    //this.DataSet.FieldsIndex(1, "Lima");
    //this.DataSet.FieldsIndex(2, 25);
    //this.DataSet.FieldsIndex(3, 6000.00);
    //this.DataSet.FieldsIndex(4, false);
    //this.DataSet.FieldsIndex(5, "Fri Jun 24 2005 15:49:06 GMT-0700 (Pacific Daylight Time)");
    //this.DataSet.Post();

    //this.DataSet.Append();
    //this.DataSet.FieldsIndex(0, "Rosa");
    //this.DataSet.FieldsIndex(1, "Lima");
    //this.DataSet.FieldsIndex(2, 30);
    //this.DataSet.FieldsIndex(3, 8000.00);
    //this.DataSet.FieldsIndex(4, false);
    //this.DataSet.FieldsIndex(5, "Fri Jun 24 2005 15:49:06 GMT-0700 (Pacific Daylight Time)");
    //this.DataSet.Post();


    var stackPanel = document.createElement("div");

    //inObjectHtml.style.backgroundColor = "red";
    //stackPanel.style.backgroundColor = "#fff";
    inObjectHtml.appendChild(stackPanel);
    //stackPanel.Row.This.style.margin = "-20px 14px";
    //stackPanel.Column[0].This.style.padding = "20px 20px";
    //stackPanel.Column[0].This.style.border = "1px solid rgba(0, 0, 0, 0.08)";
    //stackPanel.Column[0].This.style.boxShadow = " 0 0px 8px 0 rgba(0, 0, 0, 0.06), 0 1px 0px 0 rgba(0, 0, 0, 0.02);";
    if (SysCfg.App.Properties.Device != SysCfg.App.TDevice.Desktop) {

        stackPanel.style.marginTop = "-40px";
    }
    //stackPanel.Column[0].This.style.backgroundColor = "#FAFAFA";
    //var VclStackPanelPrincipal = stackPanel;
    this.GridCFView = new Componet.GridCF.TGridCFView(stackPanel, function (OutRes) { }, null);
    this.GridCFView.OnBeforeChange = function (DataSet, RecordOld, RecordNew, Status, isCancel) {//RecorOLD,RecordNew,Status,Cancel

        var message = "Desea registro ";
        if (Status == SysCfg.MemTable.Properties.TStatus.Apped) {
            message = message + "Agregar?" + _this.GeneraQuery(DataSet, RecordOld, RecordNew, DataSet.Status, "TblAutos");
        }
        else if (Status == SysCfg.MemTable.Properties.TStatus.Update) {
            message = message + "Actualizar?" + _this.GeneraQuery(DataSet, RecordOld, RecordNew, DataSet.Status, "TblAutos");
        }
        else if (Status == SysCfg.MemTable.Properties.TStatus.Delete) {
            message = message + "Eliminar OLD?:" + _this.GeneraQuery(DataSet, RecordOld, RecordNew, DataSet.Status, "TblProgramadores");
        }
        isCancel.Value = !confirm(message + " \n Are you sure to change?");//hecer pregunta sacarla por fuera

    }
    this.GridCFView.OnAfterChange = function (GridCFView, RecordOld, RecordNew) {

        var message = "Se aplico el registro ";

        if (GridCFView.DataSet.Status == SysCfg.MemTable.Properties.TStatus.Apped) {
            message = message + "Agregar" + _this.GeneraQuery(GridCFView.DataSet, RecordOld, RecordNew, GridCFView.DataSet.Status, "TblProgramadores");
        }
        else if (GridCFView.DataSet.Status == SysCfg.MemTable.Properties.TStatus.Update) {
            message = message + "Actualizar" + _this.GeneraQuery(GridCFView.DataSet, RecordOld, RecordNew, GridCFView.DataSet.Status, "TblProgramadores");
        }
        else if (GridCFView.DataSet.Status == SysCfg.MemTable.Properties.TStatus.Delete) {
            message = message + "Eliminar OLD:" + _this.GeneraQuery(GridCFView.DataSet, RecordOld, RecordNew, GridCFView.DataSet.Status, "TblProgramadores");
        }
        alert(message);
    }
    this.GridCFView.OnAfterChange = function (GridCFView, RecordOld, RecordNew) {

        //debugger;
        console.log(RecordNew);
        var message = "Se aplico el registro ";

        if (GridCFView.DataSet.Status == SysCfg.MemTable.Properties.TStatus.Apped) {
            message = message + "Agregar" + _this.GeneraQuery(GridCFView.DataSet, RecordOld, RecordNew, GridCFView.DataSet.Status, "TblProgramadores");
        }
        else if (GridCFView.DataSet.Status == SysCfg.MemTable.Properties.TStatus.Update) {
            message = message + "Actualizar" + _this.GeneraQuery(GridCFView.DataSet, RecordOld, RecordNew, GridCFView.DataSet.Status, "TblProgramadores");
        }
        else if (GridCFView.DataSet.Status == SysCfg.MemTable.Properties.TStatus.Delete) {
            message = message + "Eliminar OLD:" + _this.GeneraQuery(GridCFView.DataSet, RecordOld, RecordNew, GridCFView.DataSet.Status, "TblProgramadores");
        }
        alert(message);
    }
    this.GridCFView.OnChangeIndex = function (GridCFView, Record) {
        //console.log(Record.Fields.Value);
        //alert(Record.Fields[0].Value);
    }
    this.GridCFView.VclGridCF.DataSource = _this.DataSet;
    var VTCfgFactoryConfig = new Componet.GridCF.TGridCFView.TCfg();
    VTCfgFactoryConfig.ToolBar.ButtonImg_DeleteBar.Active = true;
    VTCfgFactoryConfig.ToolBar.ButtonImg_EditBar.Active = true;
    VTCfgFactoryConfig.ToolBar.ButtonImg_AddBar.Active = true;
    VTCfgFactoryConfig.ColumnsOptionsDef.InsertActiveEditor = true;
    VTCfgFactoryConfig.ColumnsOptionsDef.UpdateActiveEditor = true;
    VTCfgFactoryConfig.ToolBar.ButtonImg_EditToolsBar.Visible = true;

    var VTCfgFactory = new Componet.GridCF.TGridCFView.TCfg();
    VTCfgFactory.TCfgConfig = VTCfgFactoryConfig;
    // VTCfgFactory.IsAllShowValue = true;
    VTCfgFactory.ListAgrupation = ["Nombre", "Mensaje", "Edad"];
    //VTCfgFactory.Order.Type = 1;
    //VTCfgFactory.Order.ColumnName = "Edad";
    VTCfgFactory.DivitionColumnInformation = "1";
    VTCfgFactory.ToolBar.ButtonImg_DatailsViewBar.ChekedClickDown = false;
    VTCfgFactory.ToolBar.ButtonImg_EditToolsBar.ChekedClickDown = true;
    VTCfgFactory.ToolBar.ButtonImg_ColumnGroupBar.ChekedClickDown = false;
    VTCfgFactory.ToolBar.ButtonImg_NavigatorToolsBar.ChekedClickDown = false;

    var listaName = [
        {
            value: "Salvador",
            display: "Salvador",
        },
       
        {
            value: "Felipe",
            display: "Felipe Vela",
        },
        {
            value: "Carlos",
            display: "Carlos CR",
        },
        {
            value: "Mario",
            display: "Mario",
        },
        {
            value: "David",
            display: "David",
        },
       
    ]


    VTCfgFactory.Columns = [
        
    {
            Name: "Nacimiento",
            Caption: "Nacimiento",
            Show: true,
            Style: Componet.Properties.TExtrafields_ColumnStyle.None,
        },
        {
            Name: "Mensaje",
            Caption: "Mensajes",
            Show: true,
            Style: Componet.Properties.TExtrafields_ColumnStyle.Text,
        },
        {
            Name: "Documento",
            Caption: "Archivos",
            Show: true,
            Style: Componet.Properties.TExtrafields_ColumnStyle.FileSRV,
            FileType: 0

        },
        {
            Name: "Edad",
            Caption: "Mi edad",
            Show: true,
            ShowVertical: true,
            Style: Componet.Properties.TExtrafields_ColumnStyle.ProgressBar,
            OptionProgressBar: {
                "Theme":0,
                "ShowNumber": true,
                "ShowPercentage": false,
            }

        },
        {
            Name: "Nombre",
            Style: Componet.Properties.TExtrafields_ColumnStyle.LookUp,
            DataEventControl: {
                "lista": listaName,
            },
            OnEventControl: function (inTVclComboBox2, inDataEventControl) {
                var lista = inDataEventControl.lista;
                for (var i = 0; i < lista.length; i++) {
                    var valueMember = lista[i].value;
                    var displayMember = lista[i].display;
                    var VclComboBoxItem1 = new TVclComboBoxItem();
                    VclComboBoxItem1.Value = valueMember;
                    VclComboBoxItem1.Text = displayMember;
                    inTVclComboBox2.AddItem(VclComboBoxItem1);
                }
            }
        }
    ];
    _this.GridCFView.LoadTCfg(VTCfgFactory);
    $('html, body').animate({ scrollTop: 0 }, 'slow');




}
ItHelpCenter.Demo.TDemoGridCF.prototype.GeneraQuery = function (DataSet, RecordOld, RecordNew, Status, SurceDbTable) {
    return "";
    var SQLPart = "";
    var SQLPartA = "";
    var SQLPartB = "";
    if (Status == SysCfg.MemTable.Properties.TStatus.Apped) {
        for (var ContX = 0; ContX <= DataSet.FieldCount - 1; ContX++) {
            if (ContX == 0) {
                SQLPartA = DataSet.FieldDefs[ContX].FieldName;
                SQLPartB = RecordNew.Fields[ContX].Value;//SysCfg.DB.Format.objectToParam(RecordNew.Fields[ContX].Value, DataSet.FieldDefs[ContX].FieldName, DataSet.FieldDefs[ContX].DataTypetoType(), SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            }
            else {
                SQLPartA = SQLPartA + "," + DataSet.FieldDefs[ContX].FieldName;
                SQLPartB = SQLPartB + "," + RecordNew.Fields[ContX].Value;//SysCfg.DB.Format.objectToParam(RecordNew.Fields[ContX].Value, DataSet.FieldDefs[ContX].FieldName, DataSet.FieldDefs[ContX].DataTypetoType(), SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            }
        }
        SQLPart = "INSERT INTO " + SurceDbTable + " (" + SQLPartA + ") Values (" + SQLPartB + ")";
    }
    else if (DataSet.Status == SysCfg.MemTable.Properties.TStatus.Update) {
        for (var ContX = 0; ContX <= DataSet.FieldCount - 1; ContX++) {
            if (ContX == 0) {
                SQLPartA = DataSet.FieldDefs[ContX].FieldName + "=" + RecordNew.Fields[ContX].Value;//SysCfg.DB.Format.objectToParam(RecordNew.Fields[ContX].Value, DataSet.FieldDefs[ContX].FieldName, DataSet.FieldDefs[ContX].DataTypetoType(), SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                SQLPartB = DataSet.FieldDefs[ContX].FieldName + "=" + RecordOld.Fields[ContX].Value;//SysCfg.DB.Format.objectToParam(RecordOld.Fields[ContX].Value, DataSet.FieldDefs[ContX].FieldName, DataSet.FieldDefs[ContX].DataTypetoType(), SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            }
            else {
                SQLPartA = SQLPartA + "" + DataSet.FieldDefs[ContX].FieldName + "=" + RecordNew.Fields[ContX].Value;//SysCfg.DB.Format.objectToParam(RecordNew.Fields[ContX].Value, DataSet.FieldDefs[ContX].FieldName, DataSet.FieldDefs[ContX].DataTypetoType(), SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                SQLPartB = SQLPartB + "," + DataSet.FieldDefs[ContX].FieldName + "=" + RecordOld.Fields[ContX].Value;//SysCfg.DB.Format.objectToParam(RecordOld.Fields[ContX].Value, DataSet.FieldDefs[ContX].FieldName, DataSet.FieldDefs[ContX].DataTypetoType(), SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            }
        }
        SQLPart = "UPDATE " + SurceDbTable + " SET " + SQLPartA + " WHERE " + SQLPartB;
    }
    else if (DataSet.Status == SysCfg.MemTable.Properties.TStatus.Delete) {
        for (var ContX = 0; ContX <= DataSet.FieldCount - 1; ContX++) {
            if (ContX == 0) {
                SQLPartB = DataSet.FieldDefs[ContX].FieldName + "=" + RecordOld.Fields[ContX].Value;//SysCfg.DB.Format.objectToParam(RecordOld.Fields[ContX].Value, DataSet.FieldDefs[ContX].FieldName, DataSet.FieldDefs[ContX].DataTypetoType(), SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            }
            else {
                SQLPartB = SQLPartB + "," + DataSet.FieldDefs[ContX].FieldName + "=" + RecordOld.Fields[ContX].Value;//SysCfg.DB.Format.objectToParam(RecordOld.Fields[ContX].Value, DataSet.FieldDefs[ContX].FieldName, DataSet.FieldDefs[ContX].DataTypetoType(), SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            }
            SQLPart = "DELETE " + SurceDbTable + " WHERE " + SQLPartB;
        }
    }
    return (SQLPart);
}
