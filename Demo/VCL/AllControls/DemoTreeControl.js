﻿ItHelpCenter.Demo.TDemoTreeControl = function (inObjectHtml, _this) {
    //************ INICIO TAB TREE ************************************************
    var ConTreeControl = new TVclStackPanel(inObjectHtml, "1", 1, [[12], [12], [12], [12], [12]]);
    ConTreeControl.Row.This.style.marginTop = "20px";

    var VclTree = new TVclTree(ConTreeControl.Column[0].This, ""); //CREA UN TREE CONTROL 

    var TreeNodeRootExamp = VclTree.AddTreeNodeRoot("Papa\\Hijo", ""); //CREA UN TREENODE DE TIPO ROOT 
    TreeNodeRootExamp.Img = "../../../image/24/Addressbook.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
    TreeNodeRootExamp.Tag = "Tag root"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO

    var TreeNode1Examp = VclTree.AddTreeNode("Papa\\Hijo", "Tools", ""); //CREA UN TREENODE
    TreeNode1Examp.Img = "../../../image/24/My-cases.png"; // ASIGNA O EXTRAE LA RUTA DE LA IMAGEN QUE MOSTRARA EL NODO 
    TreeNode1Examp.Tag = "Tag node 1"; //ASIGNA O EXTRAE UN VALOR DE CUALQUIER TIPO
    TreeNode1Examp.Numero = "1"; //ASIGNA O EXTRAE EL NRO DE ORDEN QUE OCUPA EL NODO EN EL TREE

    var TreeNode1_1Examp = VclTree.AddTreeNode("Papa\\Hijo", "Tools", "");
    TreeNode1_1Examp.Img = "../../../image/24/Application.png";
    TreeNode1_1Examp.Tag = "Tag node 2";
    TreeNode1_1Examp.Numero = "2";

    var TreeNode1_2Examp = VclTree.AddTreeNode("Papa\\Hijo", "Tools", "");
    TreeNode1_2Examp.Img = "../../../image/24/Attachment.png";
    TreeNode1_2Examp.Tag = "Tag root 3";
    TreeNode1_2Examp.Numero = "3";
    TreeNode1_2Examp.onClick = function () { // ASIGNA EL EVENTO CUANDO SE HACE CLIC EN EL NODE
        alert(TreeNode2Examp.Tag);
    }

    var TreeNode2Examp = VclTree.AddTreeNodeRoot("Mama\\Hijo", "");
    TreeNode2Examp.Img = "../../../image/24/customer-service.png";
    TreeNode2Examp.Tag = "Tag node 2";
    TreeNode2Examp.Numero = "2";
    //************* FIN TAB TREE ***********************************///
}