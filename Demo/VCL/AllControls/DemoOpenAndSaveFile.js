﻿ItHelpCenter.Demo.TDemoOpenAndSaveFile = function (inObjectHtml, _this) {
    var contOpenFile = new TVclStackPanel(inObjectHtml, "1", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [4, 8]]);
    contOpenFile.Row.This.style.marginTop = "20px";

    var lblLabelOpen = new TVcllabel(contOpenFile.Column[0].This, "", TlabelType.H0);
    lblLabelOpen.Text = "Open File";
    lblLabelOpen.VCLType = TVCLType.BS;

    var OpenFile = new TOpenFile(contOpenFile.Column[1].This, "", true);

    var btnOpen = new TVclInputbutton(contOpenFile.Column[1].This, "");
    btnOpen.Text = "Open"
    btnOpen.VCLType = TVCLType.BS;
    btnOpen.onClick = function () {
        OpenFile.SetFile(function (sender, e) {
            alert(sender.Path + " - " + sender.NameFile)
        })
    }

    var contSaveFile = new TVclStackPanel(inObjectHtml, "1", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [4, 8]]);
    contSaveFile.Row.This.style.marginTop = "20px";

    var lblLabelSave = new TVcllabel(contSaveFile.Column[0].This, "", TlabelType.H0);
    lblLabelSave.Text = "Save File";
    lblLabelSave.VCLType = TVCLType.BS;

    var SaveFile = new TSaveFile(contSaveFile.Column[1].This, "");
    SaveFile.Path = "http://localhost:13902/File.txt";

    var btnSave = new TVclInputbutton(contSaveFile.Column[1].This, "");
    btnSave.Text = "Save"
    btnSave.VCLType = TVCLType.BS;
    btnSave.onClick = function () {
        SaveFile.SetFile(function (sender, e) {
            alert(sender.Path + " - " + sender.NameFile)
        })
    }

    
}