﻿ItHelpCenter.Demo.TDemoNavBar = function (inObjectHtml, _this) {
    //*************************** NAVBAR ***********************************///   
    var NavBar = new VclNavBar(inObjectHtml, "IDNAVBAR");
    NavBar.Title = "Nav Bar";
    $(NavBar._Container).css("width", "30%");
    $(NavBar._Container).css("margin-left", "35%");

    //Elemento para visualizar en el area de trabajo para Operaciones Remotas -------------------- 
    // Item 1
    //Objeto 1
    var Item1 = new TVclStackPanel("", "Item1", 1, [[12], [12], [12], [12], [12]]);
    var Item1C = Item1.Column[0].This;

    var Div1 = new TVclStackPanel(Item1C, "Div1", 1, [[12], [12], [12], [12], [12]]);
    var Div1C = Div1.Column[0].This;
    var btnDiv1 = new TVclButton(Div1C, "btnDiv1");
    btnDiv1.Text = "Accept";
    btnDiv1.VCLType = TVCLType.BS;
    btnDiv1.Src = "image/16/Drive.png";
    btnDiv1.onClick = function myfunction() {
        alert("Funcion");
    }
    btnDiv1.This.style.width = "80%";
    btnDiv1.This.style.marginLeft = "10%";
    Div1C.style.padding = "5px";

    //Objeto 2
    var Div2 = new TVclStackPanel(Item1C, "Div2", 1, [[12], [12], [12], [12], [12]]);
    var Div2C = Div2.Column[0].This;
    var btnDiv2 = new TVclButton(Div2C, "btnDiv2");
    btnDiv2.Text = "Accept";
    btnDiv2.VCLType = TVCLType.BS;
    btnDiv2.Src = "image/16/Drive.png";
    btnDiv2.AlignElement = "Left";
    btnDiv2.onClick = function myfunction() {
        alert("Funcion");
    }
    btnDiv2.This.style.width = "80%";
    btnDiv2.This.style.marginLeft = "10%";
    Div2C.style.padding = "5px";

    //Objeto 3
    var Div3 = new TVclStackPanel(Item1C, "Div3", 1, [[12], [12], [12], [12], [12]]);
    var Div3C = Div3.Column[0].This;
    var btnDiv3 = new TVclButton(Div3C, "btnDiv3");
    btnDiv3.Text = "Accept";
    btnDiv3.VCLType = TVCLType.BS;
    btnDiv3.Src = "image/16/Drive.png";
    btnDiv3.AlignElement = "Right";
    btnDiv3.onClick = function myfunction() {
        alert("Funcion");
    }
    btnDiv3.This.style.width = "80%";
    btnDiv3.This.style.marginLeft = "10%";
    Div3C.style.padding = "5px";

    //Objeto 4
    var Div4 = new TVclStackPanel(Item1C, "Div4", 1, [[12], [12], [12], [12], [12]]);
    var Div4C = Div4.Column[0].This;
    var btnDiv4 = new TVclButton(Div4C, "btnDiv4");
    btnDiv4.Text = "Accept";
    btnDiv4.VCLType = TVCLType.BS;
    btnDiv4.Src = "image/16/Drive.png";
    btnDiv4.ImageLocation = "BeforeText";
    btnDiv4.onClick = function myfunction() {
        alert("Funcion");
    }
    btnDiv4.This.style.width = "80%";
    btnDiv4.This.style.marginLeft = "10%";
    Div4C.style.padding = "5px";

    //Objeto 5
    var Div5 = new TVclStackPanel(Item1C, "Div5", 1, [[12], [12], [12], [12], [12]]);
    var Div5C = Div5.Column[0].This;
    var btnDiv5 = new TVclButton(Div5C, "btnDiv5");
    btnDiv5.Text = "Accept";
    btnDiv5.VCLType = TVCLType.BS;
    btnDiv5.Src = "image/16/Drive.png";
    btnDiv5.ImageLocation = "BeforeText";
    btnDiv5.AlignElement = "Right";
    btnDiv5.onClick = function myfunction() {
        alert("Funcion");
    }
    btnDiv5.This.style.width = "80%";
    btnDiv5.This.style.marginLeft = "10%";
    Div5C.style.padding = "5px";

    //Objeto 6
    var Div6 = new TVclStackPanel(Item1C, "Div6", 1, [[12], [12], [12], [12], [12]]);
    var Div6C = Div6.Column[0].This;
    var btnDiv6 = new TVclButton(Div6C, "btnDiv6");
    btnDiv6.Text = "Accept";
    btnDiv6.VCLType = TVCLType.BS;
    btnDiv6.Src = "image/16/Drive.png";
    btnDiv6.ImageLocation = "BeforeText";
    btnDiv6.AlignElement = "Left";
    btnDiv6.onClick = function myfunction() {
        alert("Funcion");
    }
    btnDiv6.This.style.width = "80%";
    btnDiv6.This.style.marginLeft = "10%";
    Div6C.style.padding = "5px";

    ///// Item 2
    //Objeto 1
    var Item2 = new TVclStackPanel("", "Item2", 1, [[12], [12], [12], [12], [12]]);
    var Item2C = Item2.Column[0].This;
    var img_Div4 = Uimg(Item2C, "img_Div4", "image/16/Web-management.png", "");
    img_Div4.style.marginRight = "5px";
    img_Div4.style.marginLeft = "10%";
    var Lab_Div4 = new Vcllabel(Item2C, "Lab_Div4", TVCLType.BS, TlabelType.H0, "Label");
    Lab_Div4.style.marginRight = "5px";
    var chkBoxDiv4 = new TVclInputcheckbox(Item2C, "chkBoxDiv4");
    chkBoxDiv4.VCLType = TVCLType.BS;

    //Agregar Items AL NAVBAR -----------------------------------------------------------------------------------
    var newItem = new Array();
    newItem.Text = "Item 1";
    newItem.Image = "image/16/Handtool.png";
    newItem.Style = "ControlContain";
    newItem.Object_Contain = Item1C;
    NavBar.AddItemNavBar(newItem);

    var newItem = new Array();
    newItem.Text = "Item 2";
    newItem.Image = "image/16/connection.png";
    newItem.Style = "ControlContain";
    newItem.Object_Contain = Item2C;
    NavBar.AddItemNavBar(newItem);

    var newItem = new Array();
    newItem.Text = "Item 3";
    newItem.Image = "image/16/Options.png";
    newItem.Style = "Default";
    newItem.onClick = function () {
        alert("Item con funcion directa");
    };
    NavBar.AddItemNavBar(newItem)
    //**************************************************************************//
}