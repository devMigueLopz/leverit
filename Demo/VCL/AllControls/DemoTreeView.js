ItHelpCenter.Demo.TDemoTreeView = function (inObjectHtml, id, callback) {
    this.TParent = function () {
        return this;
    }.bind(this);
    this.ObjectHtml = inObjectHtml;
    this.Id = id;
    this.Callback = callback;
    this.TreeView = new Object();
    this.Mythis = "TDemoTreeView";
    UsrCfg.Traslate.GetLangText(this.Mythis, "SHOWGROUPDATA", "Show Group Data");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ID", "Id");
    UsrCfg.Traslate.GetLangText(this.Mythis, "TEXT", "Text");
    UsrCfg.Traslate.GetLangText(this.Mythis, "TEXTTEXT", "Text in value");
    UsrCfg.Traslate.GetLangText(this.Mythis, "TAG", "Tag");
    UsrCfg.Traslate.GetLangText(this.Mythis, "TEXTTAG", "Text in tag");
    UsrCfg.Traslate.GetLangText(this.Mythis, "IMAGE", "Image");
    UsrCfg.Traslate.GetLangText(this.Mythis, "PATHIMAGE", "Text in path image");
    UsrCfg.Traslate.GetLangText(this.Mythis, "BACKGROUND", "Background");
    UsrCfg.Traslate.GetLangText(this.Mythis, "COLOR", "Color");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ITEMID", "Item Id");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ITEMINDEX", "Item Index");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ITEMTAG", "Item Tag");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ITEMTEXTTAG", "Text in item tag");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ITEMTEXT", "Item Text");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ITEMTEXTTEXT", "Text in item text");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ITEMIMAGE", "Item Image");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ITEMPATHIMAGE", "Text in item path image");
    UsrCfg.Traslate.GetLangText(this.Mythis, "BOTTOMSNAVMENU", "Bottoms Nav Menu");
    UsrCfg.Traslate.GetLangText(this.Mythis, "BOTTOMSITEMNAVMENU", "Bottoms Item Nav Menu");
    UsrCfg.Traslate.GetLangText(this.Mythis, "BOTTOMSCHECKMENU", "Bottoms Check Items");
    UsrCfg.Traslate.GetLangText(this.Mythis, "CHECKSMENU", "Checks Items");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ADD", "Add");
    UsrCfg.Traslate.GetLangText(this.Mythis, "GET", "Get");
    UsrCfg.Traslate.GetLangText(this.Mythis, "UPDATE", "Update");
    UsrCfg.Traslate.GetLangText(this.Mythis, "DELETE", "Delete");
    UsrCfg.Traslate.GetLangText(this.Mythis, "LIST", "List");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ELEMENTADDEDCORRECTLY", "Element Added Correctly");
    UsrCfg.Traslate.GetLangText(this.Mythis, "EDITELEMENTCORRECTLY", "Edited Element Correctly");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ELIMINATEDELEMENTCORRECTLY", "Eliminated Element Correctly");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ELEMENTCHANGEDCORRECTLY", "Element Changed Correctly");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ELEMENTNOTADDED", "Element not Added");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ELEMENTNOTFOUND", "Element not Found");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ELEMENTNOTEDITED", "Element not edited");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ELEMENTNOTELIMINATED", "Element Not Eliminated");
    UsrCfg.Traslate.GetLangText(this.Mythis, "NOCHECK", "Elements Not check");
    this.InitializeDesigner();
    this.InitializeComponent();
}
ItHelpCenter.Demo.TDemoTreeView.prototype.InitializeDesigner = function () {
    var _this = this.TParent();
    try {
        var boxbtnGroupData = new TVclStackPanel(_this.ObjectHtml, "boxbtnGroupData", 1, [[12], [12], [12], [12], [12]]);
        $(boxbtnGroupData.Column[0].This)[0].className += " text-center";
        boxbtnGroupData.Column[0].This.style.marginBottom = "5px";
        var btnGroupData = new TVclInputbutton(boxbtnGroupData.Column[0].This, "btnGroupData");
        btnGroupData.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "SHOWGROUPDATA");
        btnGroupData.VCLType = TVCLType.BS;
        _this.boxInit = new TVclStackPanel(_this.ObjectHtml, "boxInit", 1, [[12], [12], [12], [12], [12]]);
        btnGroupData.onClick = function myfunction() {
            $(_this.boxInit.Column[0].This).toggle("slow");
        }
        $(_this.boxInit.Column[0].This).toggle("slow");
        //************ INICIO DE MENU ID ***********************************************
        var ConTextControl0 = new TVclStackPanel(_this.boxInit.Column[0].This, "boxId", 2, [[12, 12], [12, 12], [2, 10], [2, 10], [2, 10]]);
        ConTextControl0.Row.This.style.marginTop = "20px";

        var lblLabel0 = new TVcllabel(ConTextControl0.Column[0].This, "", TlabelType.H0);
        lblLabel0.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ID");
        lblLabel0.VCLType = TVCLType.BS;

        _this.TxtId = new TVclTextBox(ConTextControl0.Column[1].This, "txtId");
        _this.TxtId.Text = "1"
        _this.TxtId.This.type = "number"
        _this.TxtId.This.setAttribute("min", "1");
        _this.TxtId.VCLType = TVCLType.BS;
        //************ FIN DE MENU ID ***********************************************
        //************ INICIO DE MENU TEXT ***********************************************
        var ConTextControl1 = new TVclStackPanel(_this.boxInit.Column[0].This, "boxText", 2, [[12, 12], [12, 12], [2, 10], [2, 10], [2, 10]]);
        ConTextControl1.Row.This.style.marginTop = "20px";

        var lblLabel1 = new TVcllabel(ConTextControl1.Column[0].This, "", TlabelType.H0);
        lblLabel1.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TEXT");
        lblLabel1.VCLType = TVCLType.BS;

        _this.TxtValue = new TVclTextBox(ConTextControl1.Column[1].This, "txtText");
        _this.TxtValue.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TEXTTEXT");
        _this.TxtValue.VCLType = TVCLType.BS;
        //************ FIN DE MENU TEXT ***********************************************
        //************ INICIO DE MENU TAG ***********************************************
        var ConTextControlTagMenu = new TVclStackPanel(_this.boxInit.Column[0].This, "boxTag", 2, [[12, 12], [12, 12], [2, 10], [2, 10], [2, 10]]);
        ConTextControlTagMenu.Row.This.style.marginTop = "20px";

        var lblLabelTagMenu = new TVcllabel(ConTextControlTagMenu.Column[0].This, "", TlabelType.H0);
        lblLabelTagMenu.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TAG");
        lblLabelTagMenu.VCLType = TVCLType.BS;

        _this.TxtTagMenu = new TVclTextBox(ConTextControlTagMenu.Column[1].This, "txtTag");
        _this.TxtTagMenu.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TEXTTAG");
        _this.TxtTagMenu.VCLType = TVCLType.BS;
        //************ FIN DE MENU TAG ***********************************************
        //************ INICIO DE MENU IMAGE ***********************************************
        var ConTextControl2 = new TVclStackPanel(_this.boxInit.Column[0].This, "boxImage", 2, [[12, 12], [12, 12], [2, 10], [2, 10], [2, 10]]);
        ConTextControl2.Row.This.style.marginTop = "20px";

        var lblLabel2 = new TVcllabel(ConTextControl2.Column[0].This, "", TlabelType.H0);
        lblLabel2.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "IMAGE");
        lblLabel2.VCLType = TVCLType.BS;

        _this.TxtImage = new TVclTextBox(ConTextControl2.Column[1].This, "txtImage");
        _this.TxtImage.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "PATHIMAGE");
        _this.TxtImage.VCLType = TVCLType.BS;
        //************ FIN DE MENU IMAGE ***********************************************
        //************ INICIO DE MENU BACKGROUND ***********************************************
        var ConTextControl3 = new TVclStackPanel(_this.boxInit.Column[0].This, "boxBackground", 2, [[12, 12], [12, 12], [2, 10], [2, 10], [2, 10]]);
        ConTextControl3.Row.This.style.marginTop = "20px";

        var lblLabel3 = new TVcllabel(ConTextControl3.Column[0].This, "", TlabelType.H0);
        lblLabel3.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "BACKGROUND");
        lblLabel3.VCLType = TVCLType.BS;

        _this.objBackground = new Componet.TPalleteColor(ConTextControl3.Column[1].This, "colorBackground");
        _this.objBackground.CreatePallete();
        //************ FIN DE MENU BACKGROUND ***********************************************
        //************ INICIO DE MENU COLOR ***********************************************
        var ConTextControl4 = new TVclStackPanel(_this.boxInit.Column[0].This, "boxColor", 2, [[12, 12], [12, 12], [2, 10], [2, 10], [2, 10]]);
        ConTextControl4.Row.This.style.marginTop = "20px";

        var lblLabel4 = new TVcllabel(ConTextControl4.Column[0].This, "", TlabelType.H0);
        lblLabel4.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "COLOR");
        lblLabel4.VCLType = TVCLType.BS;

        _this.objColor = new Componet.TPalleteColor(ConTextControl4.Column[1].This, "colorColor");
        _this.objColor.CreatePallete();
        //************ FIN DE MENU COLOR ***********************************************
        //************ INICIO DE ITEM MENU ID ***********************************************
        var ConTextControl5 = new TVclStackPanel(_this.boxInit.Column[0].This, "IdItemMenu", 2, [[12, 12], [12, 12], [2, 10], [2, 10], [2, 10]]);
        ConTextControl5.Row.This.style.marginTop = "20px";

        var lblLabel5 = new TVcllabel(ConTextControl5.Column[0].This, "", TlabelType.H0);
        lblLabel5.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ITEMID");
        lblLabel5.VCLType = TVCLType.BS;

        _this.TxtIdItemMenu = new TVclTextBox(ConTextControl5.Column[1].This, "txtIdItemMenu");
        _this.TxtIdItemMenu.Text = "1"
        _this.TxtIdItemMenu.This.type = "number"
        _this.TxtIdItemMenu.This.setAttribute("min", "1");
        _this.TxtIdItemMenu.VCLType = TVCLType.BS;
        //************ FIN DE ITEM MENU ID ***********************************************
        //************ INICIO DE ITEM MENU INDEX  ***********************************************
        var ConTextControl6 = new TVclStackPanel(_this.boxInit.Column[0].This, "IndexItemMenu", 2, [[12, 12], [12, 12], [2, 10], [2, 10], [2, 10]]);
        ConTextControl6.Row.This.style.marginTop = "20px";

        var lblLabel6 = new TVcllabel(ConTextControl6.Column[0].This, "", TlabelType.H0);
        lblLabel6.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ITEMINDEX");
        lblLabel6.VCLType = TVCLType.BS;

        _this.TxtIndexItemMenu = new TVclTextBox(ConTextControl6.Column[1].This, "txtIndexItemMenu");
        _this.TxtIndexItemMenu.Text = "1"
        _this.TxtIndexItemMenu.This.type = "number"
        _this.TxtIndexItemMenu.This.setAttribute("min", "1");
        _this.TxtIndexItemMenu.VCLType = TVCLType.BS;
        //************ FIN DE ITEM MENU INDEX ***********************************************
        //************ INICIO DE ITEM MENU TAG ***********************************************
        var ConTextControl7 = new TVclStackPanel(_this.boxInit.Column[0].This, "TagItemMenu", 2, [[12, 12], [12, 12], [2, 10], [2, 10], [2, 10]]);
        ConTextControl7.Row.This.style.marginTop = "20px";

        var lblLabel7 = new TVcllabel(ConTextControl7.Column[0].This, "", TlabelType.H0);
        lblLabel7.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ITEMTAG");
        lblLabel7.VCLType = TVCLType.BS;

        _this.TxtTagItemMenu = new TVclTextBox(ConTextControl7.Column[1].This, "txtTagItemMenu");
        _this.TxtTagItemMenu.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ITEMTEXTTAG");
        _this.TxtTagItemMenu.VCLType = TVCLType.BS;
        //************ FIN DE ITEM MENU TAG ***********************************************
        //************ INICIO DE ITEM MENU TEXT ***********************************************
        var ConTextControl8 = new TVclStackPanel(_this.boxInit.Column[0].This, "TextItemMenu", 2, [[12, 12], [12, 12], [2, 10], [2, 10], [2, 10]]);
        ConTextControl8.Row.This.style.marginTop = "20px";

        var lblLabel8 = new TVcllabel(ConTextControl8.Column[0].This, "", TlabelType.H0);
        lblLabel8.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ITEMTEXT");
        lblLabel8.VCLType = TVCLType.BS;

        _this.TxtTextItemMenu = new TVclTextBox(ConTextControl8.Column[1].This, "txtTextItemMenu");
        _this.TxtTextItemMenu.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ITEMTEXTTEXT");
        _this.TxtTextItemMenu.VCLType = TVCLType.BS;
        //************ FIN DE ITEM MENU TEXT ***********************************************
        //************ INICIO DE ITEM MENU IMAGE ***********************************************
        var ConTextControl8 = new TVclStackPanel(_this.boxInit.Column[0].This, "ImageItemMenu", 2, [[12, 12], [12, 12], [2, 10], [2, 10], [2, 10]]);
        ConTextControl8.Row.This.style.marginTop = "20px";

        var lblLabel8 = new TVcllabel(ConTextControl8.Column[0].This, "", TlabelType.H0);
        lblLabel8.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ITEMIMAGE");
        lblLabel8.VCLType = TVCLType.BS;

        _this.TxtImageItemMenu = new TVclTextBox(ConTextControl8.Column[1].This, "txtImageItemMenu");
        _this.TxtImageItemMenu.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ITEMPATHIMAGE");
        _this.TxtImageItemMenu.VCLType = TVCLType.BS;
        //************ FIN DE ITEM MENU IMAGE ***********************************************
        //************ INICIO BOTTOMS NAV MENU ***********************************************
        var ConTextControl9 = new TVclStackPanel(_this.boxInit.Column[0].This, "BottomsNavMenu", 2, [[12, 12], [12, 12], [2, 10], [2, 10], [2, 10]]);
        ConTextControl9.Row.This.style.marginTop = "20px";

        var lblLabel9 = new TVcllabel(ConTextControl9.Column[0].This, "", TlabelType.H0);
        lblLabel9.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "BOTTOMSNAVMENU");
        lblLabel9.VCLType = TVCLType.BS;

        var buttonAddNavMenu = new TVclInputbutton(ConTextControl9.Column[1].This, "buttonAddNavMenu");
        buttonAddNavMenu.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ADD");
        buttonAddNavMenu.VCLType = TVCLType.BS;
        buttonAddNavMenu.onClick = function () {
            try {
                _this.AddNavMenu(_this, buttonAddNavMenu);
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("DemoTreeView.js ItHelpCenter.Demo.TDemoTreeView.prototype.InitializeDesigner buttonAddNavMenu.onClick", e);
            }
        }
        buttonAddNavMenu.This.style.marginTop = "5px";
        var buttonGetNavMenu = new TVclInputbutton(ConTextControl9.Column[1].This, "buttonGetNavMenu");
        buttonGetNavMenu.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "GET");
        buttonGetNavMenu.VCLType = TVCLType.BS;
        buttonGetNavMenu.onClick = function () {
            try {
                _this.GetNavMenu(_this, buttonGetNavMenu);
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("DemoTreeView.js ItHelpCenter.Demo.TDemoTreeView.prototype.InitializeDesigner buttonGetNavMenu.onClick", e);
            }
        }
        buttonGetNavMenu.This.style.marginLeft = "5px";
        buttonGetNavMenu.This.style.marginTop = "5px";
        var buttonUpdateNavMenu = new TVclInputbutton(ConTextControl9.Column[1].This, "buttonUpdateNavMenu");
        buttonUpdateNavMenu.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "UPDATE");
        buttonUpdateNavMenu.VCLType = TVCLType.BS;
        buttonUpdateNavMenu.onClick = function () {
            try {
                _this.UpdateNavMenu(_this, buttonUpdateNavMenu);
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("DemoTreeView.js ItHelpCenter.Demo.TDemoTreeView.prototype.InitializeDesigner buttonUpdateNavMenu.onClick", e);
            }
        }
        buttonUpdateNavMenu.This.style.marginLeft = "5px";
        buttonUpdateNavMenu.This.style.marginTop = "5px";
        var buttonDeleteNavMenu = new TVclInputbutton(ConTextControl9.Column[1].This, "buttonDeleteNavMenu");
        buttonDeleteNavMenu.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "DELETE");
        buttonDeleteNavMenu.VCLType = TVCLType.BS;
        buttonDeleteNavMenu.onClick = function () {
            try {
                _this.DeleteNavMenu(_this, buttonDeleteNavMenu);
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("DemoTreeView.js ItHelpCenter.Demo.TDemoTreeView.prototype.InitializeDesigner buttonDeleteNavMenu.onClick", e);
            }
        }
        buttonDeleteNavMenu.This.style.marginLeft = "5px";
        buttonDeleteNavMenu.This.style.marginTop = "5px";
        var buttonListNavMenu = new TVclInputbutton(ConTextControl9.Column[1].This, "buttonListNavMenu");
        buttonListNavMenu.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "LIST");
        buttonListNavMenu.VCLType = TVCLType.BS;
        buttonListNavMenu.onClick = function () {
            try {
                _this.ListNavMenu(_this, buttonListNavMenu);
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("DemoTreeView.js ItHelpCenter.Demo.TDemoTreeView.prototype.InitializeDesigner buttonListNavMenu.onClick", e);
            }
        }
        buttonListNavMenu.This.style.marginLeft = "5px";
        buttonListNavMenu.This.style.marginTop = "5px";
        //************ FIN DE BOTTOMS NAV MENU  ***********************************************
        //************ INICIO BOTTOMS ITEM NAV MENU ***********************************************
        var ConTextControl10 = new TVclStackPanel(_this.boxInit.Column[0].This, "BottomsItemsNavMenu", 2, [[12, 12], [12, 12], [2, 10], [2, 10], [2, 10]]);
        ConTextControl10.Row.This.style.marginTop = "20px";

        var lblLabel10 = new TVcllabel(ConTextControl10.Column[0].This, "", TlabelType.H0);
        lblLabel10.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "BOTTOMSITEMNAVMENU");
        lblLabel10.VCLType = TVCLType.BS;

        var buttonAddItemsNavMenu = new TVclInputbutton(ConTextControl10.Column[1].This, "buttonAddItemsNavMenu");
        buttonAddItemsNavMenu.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ADD");
        buttonAddItemsNavMenu.VCLType = TVCLType.BS;
        buttonAddItemsNavMenu.onClick = function () {
            try {
                _this.AddItemsNavMenu(_this, buttonAddItemsNavMenu);
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("DemoTreeView.js ItHelpCenter.Demo.TDemoTreeView.prototype.InitializeDesigner buttonAddItemsNavMenu.onClick", e);
            }
        }
        buttonAddItemsNavMenu.This.style.marginTop = "5px";
        var buttonGetItemsNavMenu = new TVclInputbutton(ConTextControl10.Column[1].This, "buttonGetItemsNavMenu");
        buttonGetItemsNavMenu.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "GET");
        buttonGetItemsNavMenu.VCLType = TVCLType.BS;
        buttonGetItemsNavMenu.onClick = function () {
            try {
                _this.GetItemsNavMenu(_this, buttonGetItemsNavMenu);
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("DemoTreeView.js ItHelpCenter.Demo.TDemoTreeView.prototype.InitializeDesigner buttonGetItemsNavMenu.onClick", e);
            }
        }
        buttonGetItemsNavMenu.This.style.marginLeft = "5px";
        buttonGetItemsNavMenu.This.style.marginTop = "5px";
        var buttonUpdateItemsNavMenu = new TVclInputbutton(ConTextControl10.Column[1].This, "buttonUpdateItemsNavMenu");
        buttonUpdateItemsNavMenu.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "UPDATE");
        buttonUpdateItemsNavMenu.VCLType = TVCLType.BS;
        buttonUpdateItemsNavMenu.onClick = function () {
            try {
                _this.UpdateItemsNavMenu(_this, buttonUpdateItemsNavMenu);
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("DemoTreeView.js ItHelpCenter.Demo.TDemoTreeView.prototype.InitializeDesigner buttonUpdateItemsNavMenu.onClick", e);
            }
        }
        buttonUpdateItemsNavMenu.This.style.marginLeft = "5px";
        buttonUpdateItemsNavMenu.This.style.marginTop = "5px";
        var buttonDeleteItemsNavMenu = new TVclInputbutton(ConTextControl10.Column[1].This, "buttonDeleteItemsNavMenu");
        buttonDeleteItemsNavMenu.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "DELETE");
        buttonDeleteItemsNavMenu.VCLType = TVCLType.BS;
        buttonDeleteItemsNavMenu.onClick = function () {
            try {
                _this.DeleteItemsNavMenu(_this, buttonDeleteItemsNavMenu);
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("DemoTreeView.js ItHelpCenter.Demo.TDemoTreeView.prototype.InitializeDesigner buttonDeleteItemsNavMenu.onClick", e);
            }
        }
        buttonDeleteItemsNavMenu.This.style.marginLeft = "5px";
        buttonDeleteItemsNavMenu.This.style.marginTop = "5px";
        var buttonListItemsNavMenu = new TVclInputbutton(ConTextControl10.Column[1].This, "buttonListItemsNavMenu");
        buttonListItemsNavMenu.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "LIST");
        buttonListItemsNavMenu.VCLType = TVCLType.BS;
        buttonListItemsNavMenu.onClick = function () {
            try {
                _this.ListItemsNavMenu(_this, buttonListItemsNavMenu);
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("DemoTreeView.js ItHelpCenter.Demo.TDemoTreeView.prototype.InitializeDesigner buttonListItemsNavMenu.onClick", e);
            }
        }
        buttonListItemsNavMenu.This.style.marginLeft = "5px";
        buttonListItemsNavMenu.This.style.marginTop = "5px";
        //************ FIN DE BOTTOMS NAV MENU  ***********************************************
        //************ INICIO BOTTOMS ITEM NAV MENU ***********************************************
        var ConTextControl11 = new TVclStackPanel(_this.boxInit.Column[0].This, "BottomsCheckItems", 2, [[12, 12], [12, 12], [2, 10], [2, 10], [2, 10]]);
        ConTextControl11.Row.This.style.marginTop = "20px";
        ConTextControl11.Row.This.style.marginBottom = "50px";

        var lblLabel11 = new TVcllabel(ConTextControl11.Column[0].This, "", TlabelType.H0);
        lblLabel11.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "BOTTOMSCHECKMENU");
        lblLabel11.VCLType = TVCLType.BS;

        var buttonCheckItems = new TVclInputbutton(ConTextControl11.Column[1].This, "buttonCheckItems");
        buttonCheckItems.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "CHECKSMENU");
        buttonCheckItems.VCLType = TVCLType.BS;
        buttonCheckItems.onClick = function () {
            try {
                _this.CheckItemsNavMenu(_this, buttonCheckItems);
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("DemoTreeView.js ItHelpCenter.Demo.TDemoTreeView.prototype.InitializeDesigner buttonCheckItems.onClick", e);
            }
        }
        buttonCheckItems.This.style.marginTop = "5px";
        //************ FIN DE BOTTOMS NAV MENU  ***********************************************
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoListView.js ItHelpCenter.Demo.TDemoListView.prototype.InitializeDesigner", e);
    }

}
ItHelpCenter.Demo.TDemoTreeView.prototype.InitializeComponent = function () {
    var _this = this.TParent();
    try {
        if (Source.Menu.IsMobil) {
            _this.ContainerDemo = new TVclStackPanel(_this.ObjectHtml, "ContainerDemo" + "_" + _this.Id, 1, [[12], [12], [12], [12], [12]]);
            var treeMenuView = new Componet.TreeView.TTreeMenuView(_this.ContainerDemo.Column[0].This, "ListView1", null);
            _this.TreeView = treeMenuView;
        }
        else {
            _this.ContainerDemo = new TVclStackPanel(_this.ObjectHtml, "ContainerDemo" + "_" + _this.Id, 1, [[10], [10], [10], [10], [10]]);
            $(_this.ContainerDemo.Column[0].This)[0].className += " center-block";
            _this.ContainerDemo.Column[0].This.style.float = "none";
            var treeMenuView = new Componet.TreeView.TTreeMenuView(_this.ContainerDemo.Column[0].This, "ListView1", null);
            _this.TreeView = treeMenuView;
        }
        var listData = new Array();
        var view = new treeMenuView.View();
        view.Id = 1;
        view.Text = "Remote menu";
        view.Tag = "test1";
        view.Image = "image/16/right_round-32.png";
        view.Background = "#9E9E9E";
        view.Color = "#fff";
        //ChildView
        var itemView1 = new treeMenuView.ItemView();
        itemView1.Id = 1;
        itemView1.Index = 0;
        itemView1.Tag = "Item1";
        itemView1.Text = "View1 - Item1";
        itemView1.Image = "image/16/application.png";
        itemView1.Parent = view;
        view.Child.push(itemView1);
        var itemView2 = new treeMenuView.ItemView();
        itemView2.Id = 2;
        itemView2.Index = 1;
        itemView2.Tag = "Item2";
        itemView2.Text = "View1 - Item2";
        itemView2.Image = "image/16/application.png";
        itemView2.Parent = view;
        view.Child.push(itemView2);
        var itemView3 = new treeMenuView.ItemView();
        itemView3.Id = 3;
        itemView3.Index = 2;
        itemView3.Tag = "Item3";
        itemView3.Text = "View1 - Item3";
        itemView3.Image = "image/16/application.png";
        itemView3.Parent = view;
        view.Child.push(itemView3);
        listData.push(view);
        //
        var view1 = new treeMenuView.View();
        view1.Id = 2;
        view1.Text = "Remote menu 2";
        view1.Tag = "test2";
        view1.Image = "image/16/right_round-32.png";
        view1.Background = "#9E9E9E";
        view1.Color = "#fff";
        //ChildView
        var itemView4 = new treeMenuView.ItemView();
        itemView4.Id = 4;
        itemView4.Index = 0;
        itemView4.Tag = "Item4";
        itemView4.Text = "View1 - Item4";
        itemView4.Image = "image/16/application.png";
        itemView4.Parent = view1;
        view1.Child.push(itemView4);
        var itemView5 = new treeMenuView.ItemView();
        itemView5.Id = 5;
        itemView5.Index = 1;
        itemView5.Tag = "Item5";
        itemView5.Text = "View1 - Item5";
        itemView5.Image = "image/16/application.png";
        itemView5.Parent = view1;
        view1.Child.push(itemView5);
        var itemView6 = new treeMenuView.ItemView();
        itemView6.Id = 6;
        itemView6.Index = 2;
        itemView6.Tag = "Item6";
        itemView6.Text = "View1 - Item6";
        itemView6.Image = "image/16/application.png";
        itemView6.Parent = view1;
        view1.Child.push(itemView6);
        listData.push(view1);
        //
        treeMenuView.Load(listData);
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoTreeView.js ItHelpCenter.Demo.TDemoTreeView.prototype.InitializeComponent", e);
    }
}
ItHelpCenter.Demo.TDemoTreeView.prototype.AddNavMenu = function () {
    var _this = this.TParent();
    try {
        var view = new _this.TreeView.View();
        view.Id = _this.TxtId.Text;
        view.Text = _this.TxtValue.Text;
        view.Tag = _this.TxtTagMenu.Text;
        view.Image = _this.TxtImage.Text;
        view.Background = _this.objBackground.Color;
        view.Color = _this.objColor.Color;
        //ChildView
        var itemView1 = new _this.TreeView.ItemView();
        itemView1.Id = _this.TxtIdItemMenu.Text;
        itemView1.Index = _this.TxtIndexItemMenu.Text;
        itemView1.Tag = _this.TxtTagItemMenu.Text;
        itemView1.Text = _this.TxtTextItemMenu.Text;
        itemView1.Image = _this.TxtImageItemMenu.Text;
        itemView1.Parent = view;
        view.Child.push(itemView1);
        var sucess = _this.TreeView.AddNavMenu(view);
        if (sucess)
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELEMENTADDEDCORRECTLY"));
        else
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELEMENTNOTELIMINATED"));
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoListView.js ItHelpCenter.Demo.TDemoListView.prototype.AddNavMenu", e);
    }
}
ItHelpCenter.Demo.TDemoTreeView.prototype.GetNavMenu = function () {
    var _this = this.TParent();
    try {
        var objNav = _this.TreeView.GetNavMenu(_this.TxtId.Text);
        if (objNav) {
            _this.TxtId.Text = objNav.Id;
            _this.TxtValue.Text = objNav.Text;
            _this.TxtTagMenu.Text = objNav.Tag;
            _this.TxtImage.Text = objNav.Image;
            _this.objBackground.Color = objNav.Background;
            _this.objColor.Color = objNav.Color;
            _this.TxtIdItemMenu.Text = objNav.Child[0].Id;
            _this.TxtIndexItemMenu.Text = objNav.Child[0].Index;
            _this.TxtTagItemMenu.Text = objNav.Child[0].Tag;
            _this.TxtTextItemMenu.Text = objNav.Child[0].Text;
            _this.TxtImageItemMenu.Text = objNav.Child[0].Image;
        }
        else {
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELEMENTNOTFOUND"));
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoListView.js ItHelpCenter.Demo.TDemoListView.prototype.GetNavMenu", e);
    }
}
ItHelpCenter.Demo.TDemoTreeView.prototype.UpdateNavMenu = function () {
    var _this = this.TParent();
    try {
        var view = new _this.TreeView.View();
        view.Id = _this.TxtId.Text;
        view.Text = _this.TxtValue.Text;
        view.Tag = _this.TxtTagMenu.Text;
        view.Image = _this.TxtImage.Text;
        view.Background = _this.objBackground.Color;
        view.Color = _this.objColor.Color;
        //ChildView
        var itemView1 = new _this.TreeView.ItemView();
        itemView1.Id = _this.TxtIdItemMenu.Text;
        itemView1.Index = _this.TxtIndexItemMenu.Text;
        itemView1.Tag = _this.TxtTagItemMenu.Text;
        itemView1.Text = _this.TxtTextItemMenu.Text;
        itemView1.Image = _this.TxtImageItemMenu.Text;
        itemView1.Parent = view;
        view.Child.push(itemView1);
        var success = _this.TreeView.UpdateNavMenu(view);
        if (success)
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "EDITELEMENTCORRECTLY"));
        else
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELEMENTNOTEDITED"));
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoListView.js ItHelpCenter.Demo.TDemoListView.prototype.UpdateNavMenu", e);
    }
}
ItHelpCenter.Demo.TDemoTreeView.prototype.DeleteNavMenu = function () {
    var _this = this.TParent();
    try {
        var elements = _this.TreeView.DeleteNavMenu(_this.TxtId.Text);
        if (elements)
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELIMINATEDELEMENTCORRECTLY"));
        else
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELEMENTNOTELIMINATED"));
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoListView.js ItHelpCenter.Demo.TDemoListView.prototype.DeleteNavMenu", e);
    }
}
ItHelpCenter.Demo.TDemoTreeView.prototype.ListNavMenu = function () {
    var _this = this.TParent();
    try {
        var objNav = _this.TreeView.ListData;
        if (objNav) {
            var result = "";
            for (var i = 0; i < objNav.length; i++) {
                var resultChild = "";
                result += ("---------------NAV----------------" + "\n" +
                    "Id: " + objNav[i].Id + "\n" +
                    "Text: " + objNav[i].Text + "\n" +
                    "Tag: " + objNav[i].Tag + "\n" +
                    "Image: " + objNav[i].Image + "\n" +
                    "Background: " + objNav[i].Background + "\n" +
                    "Color: " + objNav[i].Color + "\n" +
                    "---------------ITEMS----------------" + "\n");
                for (var x = 0; x < objNav[i].Child.length; x++) {
                    resultChild += ("Id: " + objNav[i].Child[x].Id + "\n" +
                        "Index: " + objNav[i].Child[x].Index + "\n" +
                        "Tag: " + objNav[i].Child[x].Tag + "\n" +
                        "Text: " + objNav[i].Child[x].Text + "\n" +
                        "Image: " + objNav[i].Child[x].Image + "\n" +
                        "Parent: " + objNav[i].Child[x].Parent + "\n");
                }
                result += resultChild;
            }
            alert(result);
        }
        else {
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELEMENTNOTFOUND"));
        }

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoListView.js ItHelpCenter.Demo.TDemoListView.prototype.ListNavMenu", e);
    }
}
ItHelpCenter.Demo.TDemoTreeView.prototype.AddItemsNavMenu = function () {
    var _this = this.TParent();
    try {
        var view = new _this.TreeView.View();
        view.Id = _this.TxtId.Text;
        view.Text = _this.TxtValue.Text;
        view.Tag = _this.TxtTagMenu.Text;
        view.Image = _this.TxtImage.Text;
        view.Background = _this.objBackground.Color;
        view.Color = _this.objColor.Color;
        //ChildView
        var itemView1 = new _this.TreeView.ItemView();
        itemView1.Id = _this.TxtIdItemMenu.Text;
        itemView1.Index = _this.TxtIndexItemMenu.Text;
        itemView1.Tag = _this.TxtTagItemMenu.Text;
        itemView1.Text = _this.TxtTextItemMenu.Text;
        itemView1.Image = _this.TxtImageItemMenu.Text;
        itemView1.Parent = view;
        var sucess = _this.TreeView.AddItemNavMenu(itemView1);
        if (sucess)
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELEMENTADDEDCORRECTLY"));
        else
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELEMENTNOTADDED"));
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoListView.js ItHelpCenter.Demo.TDemoListView.prototype.AddItemsNavMenu", e);
    }
}
ItHelpCenter.Demo.TDemoTreeView.prototype.GetItemsNavMenu = function () {
    var _this = this.TParent();
    try {
        var objItemNav = _this.TreeView.GetItemNavMenu(_this.TxtIdItemMenu.Text);
        if (objItemNav) {
            _this.TxtId.Text = objItemNav.Parent.Id;
            _this.TxtValue.Text = objItemNav.Parent.Text;
            _this.TxtTagMenu.Text = objItemNav.Parent.Tag;
            _this.TxtImage.Text = objItemNav.Parent.Image;
            _this.objBackground.Color = objItemNav.Parent.Background;
            _this.objColor.Color = objItemNav.Parent.Color;
            _this.TxtIdItemMenu.Text = objItemNav.Id;
            _this.TxtIndexItemMenu.Text = objItemNav.Index;
            _this.TxtTagItemMenu.Text = objItemNav.Tag;
            _this.TxtTextItemMenu.Text = objItemNav.Text;
            _this.TxtImageItemMenu.Text = objItemNav.Image;
        }
        else {
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELEMENTNOTFOUND"));
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoListView.js ItHelpCenter.Demo.TDemoListView.prototype.GetItemsNavMenu", e);
    }
}
ItHelpCenter.Demo.TDemoTreeView.prototype.UpdateItemsNavMenu = function () {
    var _this = this.TParent();
    try {
        var itemView1 = new _this.TreeView.ItemView();
        itemView1.Id = _this.TxtIdItemMenu.Text;
        itemView1.Index = _this.TxtIndexItemMenu.Text;
        itemView1.Tag = _this.TxtTagItemMenu.Text;
        itemView1.Text = _this.TxtTextItemMenu.Text;
        itemView1.Image = _this.TxtImageItemMenu.Text;
        var success = _this.TreeView.UpdateItemNavMenu(itemView1);
        if (success)
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "EDITELEMENTCORRECTLY"));
        else
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELEMENTNOTEDITED"));

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoListView.js ItHelpCenter.Demo.TDemoListView.prototype.UpdateItemsNavMenu", e);
    }
}
ItHelpCenter.Demo.TDemoTreeView.prototype.DeleteItemsNavMenu = function () {
    var _this = this.TParent();
    try {
        var elements = _this.TreeView.DeleteItemNavMenu(_this.TxtId.Text);
        if (elements)
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELIMINATEDELEMENTCORRECTLY"));
        else
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELEMENTNOTELIMINATED"));
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoListView.js ItHelpCenter.Demo.TDemoListView.prototype.DeleteItemsNavMenu", e);
    }
}
ItHelpCenter.Demo.TDemoTreeView.prototype.ListItemsNavMenu = function () {
    var _this = this.TParent();
    try {
        var objItemsNav = _this.TreeView.ListDataChild;
        if (objItemsNav) {
            var resultChild = "";
            for (var x = 0; x < objItemsNav.length; x++) {
                resultChild += ("Id: " + objItemsNav[x].Id + "\n" +
                    "Index: " + objItemsNav[x].Index + "\n" +
                    "Tag: " + objItemsNav[x].Tag + "\n" +
                    "Text: " + objItemsNav[x].Text + "\n" +
                    "Image: " + objItemsNav[x].Image + "\n" +
                    "Parent: " + objItemsNav[x].Parent + "\n");
            }
            alert(resultChild);
        }
        else {
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ELEMENTNOTFOUND"));
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoListView.js ItHelpCenter.Demo.TDemoListView.prototype.ListItemsNavMenu", e);
    }
}
ItHelpCenter.Demo.TDemoTreeView.prototype.CheckItemsNavMenu = function () {
    var _this = this.TParent();
    try {
        var itemsCheck = new Array();
        itemsCheck = _this.TreeView.ListItemsCheck(_this.TxtId.Text);
        if (itemsCheck.length > 0) {
            var resultChild = "";
            for (var x = 0; x < itemsCheck.length; x++) {
                resultChild += ("Id: " + itemsCheck[x].Id + "\n" +
                    "Index: " + itemsCheck[x].Index + "\n" +
                    "Tag: " + itemsCheck[x].Tag + "\n" +
                    "Text: " + itemsCheck[x].Text + "\n" +
                    "Image: " + itemsCheck[x].Image + "\n" +
                    "Parent: " + itemsCheck[x].Parent + "\n");
            }
            alert(resultChild);
        }
        else {
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NOCHECK"));

        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoListView.js ItHelpCenter.Demo.TDemoListView.prototype.ListItemsNavMenu", e);
    }
}