﻿ItHelpCenter.Demo.TDemoMemTable = function (inObjectHtml, _this) {
    //************* INICIO TAB MEM TABLE ***********************************/// 
    var ContMemTable = new TVclStackPanel(inObjectHtml, "1", 1, [[12], [12], [12], [12], [12]]);
    ContMemTable.Row.This.style.marginTop = "20px";


    var ConButtons = new TVclStackPanel(inObjectHtml, "1", 9, [[1, 1, 2, 2, 2, 1, 1, 1, 1], [1, 1, 2, 2, 2, 1, 1, 1, 1], [1, 1, 2, 2, 2, 1, 1, 1, 1], [1, 1, 2, 2, 2, 1, 1, 1, 1], [1, 1, 2, 2, 2, 1, 1, 1, 1]]);
    ConButtons.Row.This.style.marginTop = "20px";

    var btn1 = new TVclInputbutton(ConButtons.Column[0].This, "");
    btn1.Text = "|<";
    btn1.VCLType = TVCLType.BS;
    btn1.onClick = function () {
        if (DataSet != null) DataSet.First();
    }

    var btn2 = new TVclInputbutton(ConButtons.Column[1].This, "");
    btn2.Text = "<";
    btn2.VCLType = TVCLType.BS;
    btn2.onClick = function () {
        if (DataSet != null) DataSet.Prior();
    }

    var btn3 = new TVclInputbutton(ConButtons.Column[2].This, "");
    btn3.Text = "Delete";
    btn3.VCLType = TVCLType.BS;
    btn3.onClick = function () {
        if (DataSet != null) DataSet.Delete();
    }

    var btn4 = new TVclInputbutton(ConButtons.Column[3].This, "");
    btn4.Text = "Insert";
    btn4.VCLType = TVCLType.BS;
    btn4.onClick = function () {
        if (DataSet != null) DataSet.Append();
        //click
    }

    var btn5 = new TVclInputbutton(ConButtons.Column[4].This, "");
    btn5.Text = "Update";
    btn5.VCLType = TVCLType.BS;
    btn5.onClick = function () {
        if (DataSet != null) DataSet.Update();
    }
    var btn6 = new TVclInputbutton(ConButtons.Column[5].This, "");
    btn6.Text = "Post";
    btn6.VCLType = TVCLType.BS;
    btn6.onClick = function () {
        var Edad = 0;
        Sueldo = 0.0;
        Nacimiento = Date.now();

        try {
            Edad = SpinEdit1.Text;
        }
        catch (e) {
            SysCfg.Log.Methods.WriteLog("DemoMemTable.js ItHelpCenter.Demo.TDemoMemTable ", e);
        }
        try {
            Sueldo = SpinEdit2.Text;
        }
        catch (e) {
            SysCfg.Log.Methods.WriteLog("DemoMemTable.js ItHelpCenter.Demo.TDemoMemTable ", e);
        }
        try {
            Nacimiento = DateEdit1.Text;
        }
        catch (e) {
            SysCfg.Log.Methods.WriteLog("DemoMemTable.js ItHelpCenter.Demo.TDemoMemTable ", e);
        }

        DataSet.FieldsName("Nombre", TexEdit1.Text);
        DataSet.FieldsName("Mensaje", MemoEdit1.Text);
        DataSet.FieldsName("Edad", Edad);
        DataSet.FieldsName("Sueldo", Sueldo);
        DataSet.FieldsName("Credito", CheckEdit1.Checked);
        DataSet.FieldsName("Nacimeinto", Nacimiento);
        if (DataSet != null) DataSet.Post();
    }
    var btn7 = new TVclInputbutton(ConButtons.Column[6].This, "");
    btn7.Text = "Cancel";
    btn7.VCLType = TVCLType.BS;
    btn7.onClick = function () {
        if (DataSet != null) DataSet.Cancel();
        //click
    }
    var btn8 = new TVclInputbutton(ConButtons.Column[7].This, "");
    btn8.Text = ">";
    btn8.VCLType = TVCLType.BS;
    btn8.onClick = function () {
        if (DataSet != null) DataSet.Next();
    }
    var btn9 = new TVclInputbutton(ConButtons.Column[8].This, "");
    btn9.Text = ">|";
    btn9.VCLType = TVCLType.BS;
    btn9.onClick = function () {
        if (DataSet != null) DataSet.Last();
    }

    var ContlabelBE = new TVclStackPanel(inObjectHtml, "", 4, [[3, 3, 3, 3], [3, 3, 3, 3], [3, 3, 3, 3], [3, 3, 3, 3], [3, 3, 3, 3]]);
    ContlabelBE.Row.This.style.marginTop = "20px";

    var LabelBof = new TVcllabel(ContlabelBE.Column[0].This, "", TlabelType.H0);
    LabelBof.Text = "...";
    LabelBof.VCLType = TVCLType.BS;

    var LabelEof = new TVcllabel(ContlabelBE.Column[1].This, "", TlabelType.H0);
    LabelEof.Text = "...";
    LabelEof.VCLType = TVCLType.BS;

    var LabelIndex = new TVcllabel(ContlabelBE.Column[2].This, "", TlabelType.H0);
    LabelIndex.Text = "...";
    LabelIndex.VCLType = TVCLType.BS;

    var LabelStatus = new TVcllabel(ContlabelBE.Column[3].This, "", TlabelType.H0);
    LabelStatus.Text = "...";
    LabelStatus.VCLType = TVCLType.BS;

    var ContEditor1 = new TVclStackPanel(inObjectHtml, "1", 3, [[2, 3, 7], [2, 3, 7], [2, 3, 7], [2, 3, 7], [2, 3, 7]]);
    ContEditor1.Row.This.style.marginTop = "20px";

    var lblNombre = new TVcllabel(ContEditor1.Column[0].This, "", TlabelType.H0);
    lblNombre.Text = "Nombre: ";
    lblNombre.VCLType = TVCLType.BS;

    var TexEdit1 = new TVclTextBox(ContEditor1.Column[1].This, "");
    TexEdit1.Text = ""
    TexEdit1.VCLType = TVCLType.BS;

    var ContEditor2 = new TVclStackPanel(inObjectHtml, "1", 3, [[2, 3, 7], [2, 3, 7], [2, 3, 7], [2, 3, 7], [2, 3, 7]]);
    ContEditor2.Row.This.style.marginTop = "20px";

    var lblMensaje = new TVcllabel(ContEditor2.Column[0].This, "", TlabelType.H0);
    lblMensaje.Text = "Mensaje: ";
    lblMensaje.VCLType = TVCLType.BS;

    var MemoEdit1 = new TVclMemo(ContEditor2.Column[1].This, "");
    MemoEdit1.Text = "";
    MemoEdit1.VCLType = TVCLType.BS;
    MemoEdit1.Rows = 5;
    MemoEdit1.Cols = 50;


    var ContEditor3 = new TVclStackPanel(inObjectHtml, "1", 3, [[2, 3, 7], [2, 3, 7], [2, 3, 7], [2, 3, 7], [2, 3, 7]]);
    ContEditor3.Row.This.style.marginTop = "20px";

    var lblEdad = new TVcllabel(ContEditor3.Column[0].This, "", TlabelType.H0);
    lblEdad.Text = "Edad: ";
    lblEdad.VCLType = TVCLType.BS;

    var SpinEdit1 = new TVclTextBox(ContEditor3.Column[1].This, "");
    SpinEdit1.Text = ""
    //SpinEdit1.VCLType = TVCLType.BS;
    $(SpinEdit1.This).spinner({
        step: 1,
        numberFormat: "n"
    });

    var ContEditor4 = new TVclStackPanel(inObjectHtml, "1", 3, [[2, 3, 7], [2, 3, 7], [2, 3, 7], [2, 3, 7], [2, 3, 7]]);
    ContEditor4.Row.This.style.marginTop = "20px";

    var lblSueldo = new TVcllabel(ContEditor4.Column[0].This, "", TlabelType.H0);
    lblSueldo.Text = "Sueldo: ";
    lblSueldo.VCLType = TVCLType.BS;

    var SpinEdit2 = new TVclTextBox(ContEditor4.Column[1].This, "");
    SpinEdit2.Text = ""
    //SpinEdit2.VCLType = TVCLType.BS;
    $(SpinEdit2.This).spinner({
        step: 1,
        numberFormat: "n"
    });

    var ContEditor5 = new TVclStackPanel(inObjectHtml, "1", 3, [[2, 3, 7], [2, 3, 7], [2, 3, 7], [2, 3, 7], [2, 3, 7]]);
    ContEditor5.Row.This.style.marginTop = "20px";

    var lblCredito = new TVcllabel(ContEditor5.Column[0].This, "", TlabelType.H0);
    lblCredito.Text = "Credito: ";
    lblCredito.VCLType = TVCLType.BS;

    var CheckEdit1 = new TVclInputcheckbox(ContEditor5.Column[1].This, "");
    CheckEdit1.Checked = false;
    CheckEdit1.VCLType = TVCLType.BS;

    var ContEditor6 = new TVclStackPanel(inObjectHtml, "1", 3, [[2, 3, 7], [2, 3, 7], [2, 3, 7], [2, 3, 7], [2, 3, 7]]);
    ContEditor6.Row.This.style.marginTop = "20px";

    var lblNacimiento = new TVcllabel(ContEditor6.Column[0].This, "", TlabelType.H0);
    lblNacimiento.Text = "Nacimiento: ";
    lblNacimiento.VCLType = TVCLType.BS;

    var DateEdit1 = new TVclTextBox(ContEditor6.Column[1].This, "");
    DateEdit1.Text = ""
    DateEdit1.VCLType = TVCLType.BS;
    $(DateEdit1.This).datetimepicker({
        showButtonPanel: true
    });

    var GrillaDataSet = new TGrid(ContMemTable.Column[0].This, "", ""); //CREA UN GRID CONTROL





    //var ColMem0 = new TColumn(); //CREA UNA COLUMNA PARA AGREGAR AL GRID CONTROL
    //ColMem0.Name = "Check"; //ASIGNA O EXTRAE EL NOMBRE DE LA COLUMNA
    //ColMem0.Caption = "Check"; //ASIGNA O EXTRAE EL TEXTO QUE SE MOSTRARA EN LA COLUMNA 
    //ColMem0.Index = 0; // ASGINA O EXTRAE EL INDICE DE LA COLUMNA 
    //ColMem0.EnabledEditor = true; //ASIGNA O EXTRAE UN VALO TRUE O FALSE QUE INDICA SI LA COLUMNA MOSTRARA UN EDITOR DE DATOS
    //ColMem0.DataType = SysCfg.DB.Properties.TDataType.Boolean; //ASIGNA O EXTRAE UN VALOR DE TIPO DE DATO QUE TIENEN LA COLUMNA 
    //ColMem0.ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.None; //ASIGNA O EXTRAE UN ESTILO DE EDITOR Q MOSTRARA EN EL CASO EL ENABLEEDITOR ESTE ACTIVADO
    //GrillaDataSet.AddColumn(ColMem0); //AGREGA UNA COLUMNA AL GRIDCONTROL 

    //var ColMem1 = new TColumn();
    //ColMem1.Name = "Fecha";
    //ColMem1.Caption = "Fecha";
    //ColMem1.Index = 1;
    //ColMem1.EnabledEditor = true;
    //ColMem1.DataType = SysCfg.DB.Properties.TDataType.DateTime;
    //ColMem1.ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.None;
    //GrillaDataSet.AddColumn(ColMem1);

    //for (var i = 0; i < 4; i++) {
    //    var Col = new TColumn();
    //    Col.Name = "Columna" + (i + 2);
    //    Col.Caption = "Columna " + (i + 2);
    //    Col.Index = i + 2;
    //    GrillaDataSet.AddColumn(Col);
    //}

    //for (var i = 0; i < 3; i++) {
    //    var NewRow = new TRow(); //CREA UN ROW PARA AGREGAR AL GRIDCONTROL 

    //    var Cell0 = new TCell(); //CREA UNA CELDA PARA QUE SE AGREGA AL ROW
    //    Cell0.Value = false; // ASIGNA O EXTRAE EL VALOR QUE SE MOSTRARA EN LA CELDA DEL GRIDCONTROL 
    //    Cell0.IndexColumn = 0; // ASIGNA O EXTRAE EL INDEX DE LA COLUMNA 
    //    Cell0.IndexRow = i; // ASIGNA O EXTRAE EL INDEX DEL ROW 
    //    NewRow.AddCell(Cell0); // AGREGA O EXTRAE LA CELDA AL ROW

    //    var Cell1 = new TCell();
    //    Cell1.Value = "10/18/2017 12:00 am";
    //    Cell1.IndexColumn = 0 + 1;
    //    Cell1.IndexRow = i;
    //    NewRow.AddCell(Cell1);

    //    for (var j = 0; j < 4; j++) {
    //        var Cell = new TCell();
    //        Cell.Value = "Row " + i + " | Columna " + (j + 2);
    //        Cell.IndexColumn = j + 2;
    //        Cell.IndexRow = i;
    //        NewRow.AddCell(Cell);
    //    }
    //    NewRow.onclick = function () { // ASIGNA UN EVENTO CUANDO SE HACE CLICK EN EL ROW DEL GRIDCONTROL
    //        Grilla_rowclick(NewRow);
    //    }
    //    GrillaDataSet.AddRow(NewRow);
    //}

    DataSet = new SysCfg.MemTable.TDataSet();

    var Grilla_rowclick2 = function (inRow) {
        DataSet.SetIndex(inRow.Index);
        alert(inRow.Cells[1].Value);
    }

    DataSet.OnRefreschRecordSet = function (Object, EventArgs) {
        var UnDataSet = Object;
        var UnRecord = UnDataSet.RecordSet;//EventArgs;
        var isnotNull = (UnDataSet != null) && (UnRecord != null);
        if (isnotNull)// para precargar tu datos 
        {
            //Inicializas tu editor de nuevo 
            //TexEdit1.ReadOnly = (UnDataSet.Status == SysCfg.MemTable.Properties.TStatus.None);
            //MemoEdit1.Enabled = (UnDataSet.Status == SysCfg.MemTable.Properties.TStatus.None);
            //SpinEdit1.Enabled = (UnDataSet.Status == SysCfg.MemTable.Properties.TStatus.None);
            //SpinEdit2.Enabled = (UnDataSet.Status == SysCfg.MemTable.Properties.TStatus.None);
            //CheckEdit1.Enabled = (UnDataSet.Status == SysCfg.MemTable.Properties.TStatus.None);
            //DateEdit1.Enabled = (UnDataSet.Status == SysCfg.MemTable.Properties.TStatus.None);

            TexEdit1.Text = UnDataSet.RecordSet.FieldName("Nombre").asString();
            MemoEdit1.Text = UnDataSet.RecordSet.FieldName("Mensaje").asText();
            SpinEdit1.Text = UnDataSet.RecordSet.FieldName("Edad").asInt32();
            SpinEdit2.Text = UnDataSet.RecordSet.FieldName("Sueldo").asDouble();
            CheckEdit1.Checked = UnDataSet.RecordSet.FieldName("Credito").asBoolean();
            DateEdit1.Text = UnDataSet.RecordSet.FieldName("Nacimeinto").asDateTime();
        }
        else {
            TexEdit1.Text = "";
            MemoEdit1.Text = "";
            SpinEdit1.Text = 0;
            SpinEdit2.Text = 0;
            CheckEdit1.Checked = false;
            DateEdit1.Text = Date.now();
        }

        MemoEdit1.Enabled = (UnDataSet.Status != SysCfg.MemTable.Properties.TStatus.None) && (isnotNull);
        SpinEdit1.Enabled = (UnDataSet.Status != SysCfg.MemTable.Properties.TStatus.None) && (isnotNull);
        SpinEdit2.Enabled = (UnDataSet.Status != SysCfg.MemTable.Properties.TStatus.None) && (isnotNull);
        CheckEdit1.Enabled = (UnDataSet.Status != SysCfg.MemTable.Properties.TStatus.None) && (isnotNull);
        DateEdit1.Enabled = (UnDataSet.Status != SysCfg.MemTable.Properties.TStatus.None) && (isnotNull);
        //TexEdit1.Enabled = (isnotNull)
        //MemoEdit1.Enabled = (isnotNull);
        //SpinEdit1.Enabled = (isnotNull);
        //SpinEdit2.Enabled = (isnotNull);
        //CheckEdit1.Enabled = (isnotNull);
        //DateEdit1.Enabled = (isnotNull);
        btn1.Enabled = UnDataSet.isFirst();
        btn2.Enabled = UnDataSet.isPrior();
        btn3.Enabled = UnDataSet.isDelete();
        btn4.Enabled = UnDataSet.isAppend();//insert
        btn5.Enabled = UnDataSet.isUpdate();//update
        btn6.Enabled = UnDataSet.isPost();
        btn7.Enabled = UnDataSet.isCancel();
        btn8.Enabled = UnDataSet.isNext();
        btn9.Enabled = UnDataSet.isLast();

        LabelBof.Text = "Bof:" + SysCfg.Str.BooleanToTxt(UnDataSet.Bof);
        LabelEof.Text = "Eof:" + SysCfg.Str.BooleanToTxt(UnDataSet.Eof);
        LabelIndex.Text = "Index:" + UnDataSet.Index;
        LabelStatus.Text = "Status:" + UnDataSet.Status.name;
    }
    DataSet.OnBeforeChange = function (Object, EventArgs) {
        var UnDataSet = Object;
        var UnRecord = UnDataSet.RecordSet;//EventArgs;
        var isnotNull = (UnDataSet != null) && (UnRecord != null);
        if (isnotNull) {
            //***************************** Calculando old values **********************************            
            var RecordOld = null;
            if ((UnDataSet.RecordCount != 0) && (!UnDataSet.Eof) && (!UnDataSet.Bof)) {
                RecordOld = UnDataSet.Records[UnDataSet.Index];
            }
            //****************************** Calculando New Nalues *********************************            
            var RecordNew = UnDataSet.RecordSet;
            //**************************************************************************************            
            var message = "Desea ";
            if (UnDataSet.Status == SysCfg.MemTable.Properties.TStatus.Apped) {
                message = message + "Agregar?" + ItHelpCenter.Demo.GeneraQuery(UnDataSet, RecordOld, RecordNew, UnDataSet.Status, "TblProgramadores");
            }
            else if (UnDataSet.Status == SysCfg.MemTable.Properties.TStatus.Update) {
                message = message + "Actualizar" + ItHelpCenter.Demo.GeneraQuery(UnDataSet, RecordOld, RecordNew, UnDataSet.Status, "TblProgramadores");
            }
            else if (UnDataSet.Status == SysCfg.MemTable.Properties.TStatus.Delete) {
                message = message + "Eliminar OLD:" + ItHelpCenter.Demo.GeneraQuery(UnDataSet, RecordOld, RecordNew, UnDataSet.Status, "TblProgramadores");
            }
            var isCancel = !confirm(message + " \n Are you sure to change?");//hecer pregunta
            if (isCancel) {
                // cancel the closure of the form.
                DataSet.Cancel();
            }
        }
    }
    DataSet.OnRefresch = function (Object, EventArgs) {
        var UnDataSet = Object;
        var UnRecord = UnDataSet.RecordSet;//EventArgs;
        var isnotNull = (UnDataSet != null)
        if (isnotNull)// para precargar tu datos 
        {
            if (UnDataSet.EnableControls) {

                //Tu funcion para borrar
                GrillaDataSet.ClearAll();

                for (var i = 0; i < UnDataSet.FieldCount; i++) {
                    var ColMem1 = new TColumn();
                    ColMem1.Name = UnDataSet.FieldDefs[i].FieldName;
                    ColMem1.Caption = UnDataSet.FieldDefs[i].FieldName;
                    ColMem1.Index = Object.FieldDefs.length - 1;
                    //ColMem1.EnabledEditor = true;
                    ColMem1.DataType = UnDataSet.FieldDefs[i].DataType;
                    ColMem1.ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.None;
                    GrillaDataSet.AddColumn(ColMem1);
                }

                if (UnDataSet.Records.length > 0) {
                    for (var e = 0; e < UnDataSet.RecordCount; e++) {
                        var NewRow = new TRow(); //CREA UN ROW PARA AGREGAR AL GRIDCONTROL 
                        for (var ContX = 0; ContX <= UnDataSet.FieldCount - 1; ContX++) {
                            //DataSet.FieldDefs[ContX].FieldName;
                            var Cell0 = new TCell(); //CREA UNA CELDA PARA QUE SE AGREGA AL ROW
                            Cell0.Value = UnDataSet.Records[e].Fields[ContX].Value; // ASIGNA O EXTRAE EL VALOR QUE SE MOSTRARA EN LA CELDA DEL GRIDCONTROL 
                            Cell0.IndexColumn = ContX; // ASIGNA O EXTRAE EL INDEX DE LA COLUMNA 
                            Cell0.IndexRow = e; // ASIGNA O EXTRAE EL INDEX DEL ROW 
                            NewRow.AddCell(Cell0); // AGREGA O EXTRAE LA CELDA AL ROW
                        }
                        NewRow.onclick = function () { // ASIGNA UN EVENTO CUANDO SE HACE CLICK EN EL ROW DEL GRIDCONTROL
                            Grilla_rowclick2(NewRow);
                        }
                        GrillaDataSet.AddRow(NewRow);
                    }
                }
            }
            else {
                //lo pones en edit disable 
            }

        }

    }
    DataSet.OnBeforeChange = function (Object, EventArgs) {
        var UnDataSet = Object;
        var UnRecord = UnDataSet.RecordSet;//EventArgs;
        var isnotNull = (UnDataSet != null) && (UnRecord != null);
        if (isnotNull) {
            //***************************** Calculando old values **********************************            
            var RecordOld = null;
            if ((UnDataSet.RecordCount != 0) && (!UnDataSet.Eof) && (!UnDataSet.Bof)) {
                RecordOld = UnDataSet.Records[UnDataSet.Index];
            }
            //****************************** Calculando New Nalues *********************************            
            var RecordNew = UnDataSet.RecordSet;
            //**************************************************************************************            
            var message = "Desea ";
            if (UnDataSet.Status == SysCfg.MemTable.Properties.TStatus.Apped) {
                message = message + "Agregar?" + ItHelpCenter.Demo.GeneraQuery(UnDataSet, RecordOld, RecordNew, UnDataSet.Status, "TblProgramadores");
            }
            else if (UnDataSet.Status == SysCfg.MemTable.Properties.TStatus.Update) {
                message = message + "Actualizar" + ItHelpCenter.Demo.GeneraQuery(UnDataSet, RecordOld, RecordNew, UnDataSet.Status, "TblProgramadores");
            }
            else if (UnDataSet.Status == SysCfg.MemTable.Properties.TStatus.Delete) {
                message = message + "Eliminar OLD:" + ItHelpCenter.Demo.GeneraQuery(UnDataSet, RecordOld, RecordNew, UnDataSet.Status, "TblProgramadores");
            }
            var isCancel = !confirm(message + " \n Are you sure to change?");//hecer pregunta
            if (isCancel) {
                // cancel the closure of the form.
                DataSet.Cancel();
            }
        }
    }
    DataSet.OnAfterChange = function (Object, EventArgs) {
        var UnDataSet = Object;
        var UnRecord = UnDataSet.RecordSet;//EventArgs;
        var isnotNull = (UnDataSet != null) && (UnRecord != null);
        if (isnotNull) {
            //***************************** Calculando old values **********************************            
            var RecordOld = null;
            if ((UnDataSet.RecordCount != 0) && (!UnDataSet.Eof) && (!UnDataSet.Bof)) {
                RecordOld = UnDataSet.Records[UnDataSet.Index];
            }
            //****************************** Calculando New Nalues *********************************            
            var RecordNew = UnDataSet.RecordSet;
            //**************************************************************************************
            var message = "Se aplico ";

            if (UnDataSet.Status == SysCfg.MemTable.Properties.TStatus.Apped) {
                message = message + "Agregar?" + ItHelpCenter.Demo.GeneraQuery(UnDataSet, RecordOld, RecordNew, UnDataSet.Status, "TblProgramadores");
            }
            else if (UnDataSet.Status == SysCfg.MemTable.Properties.TStatus.Update) {
                message = message + "Actualizar" + ItHelpCenter.Demo.GeneraQuery(UnDataSet, RecordOld, RecordNew, UnDataSet.Status, "TblProgramadores");
            }
            else if (UnDataSet.Status == SysCfg.MemTable.Properties.TStatus.Delete) {
                message = message + "Eliminar OLD:" + ItHelpCenter.Demo.GeneraQuery(UnDataSet, RecordOld, RecordNew, UnDataSet.Status, "TblProgramadores");
            }
            alert(message);
            //en el evento after agregar la actulizacion del grid
            if (UnDataSet.Status == SysCfg.MemTable.Properties.TStatus.Apped) {
                var NewRow = new TRow(); //CREA UN ROW PARA AGREGAR AL GRIDCONTROL 
                for (var ContX = 0; ContX <= UnDataSet.FieldCount - 1; ContX++) {
                    //DataSet.FieldDefs[ContX].FieldName;
                    var Cell0 = new TCell(); //CREA UNA CELDA PARA QUE SE AGREGA AL ROW
                    Cell0.Value = RecordNew.Fields[ContX].Value; // ASIGNA O EXTRAE EL VALOR QUE SE MOSTRARA EN LA CELDA DEL GRIDCONTROL 
                    Cell0.IndexColumn = ContX; // ASIGNA O EXTRAE EL INDEX DE LA COLUMNA 
                    Cell0.IndexRow = UnDataSet.RecordCount; // ASIGNA O EXTRAE EL INDEX DEL ROW 
                    NewRow.AddCell(Cell0); // AGREGA O EXTRAE LA CELDA AL ROW
                }
                NewRow.onclick = function () { // ASIGNA UN EVENTO CUANDO SE HACE CLICK EN EL ROW DEL GRIDCONTROL
                    Grilla_rowclick2(NewRow);
                }
                GrillaDataSet.AddRow(NewRow);
            }
            else if (UnDataSet.Status == SysCfg.MemTable.Properties.TStatus.Update) {
                var inRow = GrillaDataSet.FocusedRowHandle;
                for (var ContX = 0; ContX <= UnDataSet.FieldCount - 1; ContX++) {
                    if (RecordNew.Fields[ContX].Value != RecordOld.Fields[ContX].Value) {
                        var Cell0 = inRow.Cells[ContX];
                        Cell0.Value = RecordNew.Fields[ContX].Value; // ASIGNA O EXTRAE EL VALOR QUE SE MOSTRARA EN LA CELDA DEL GRIDCONTROL 
                    }
                }
            }
            else if (UnDataSet.Status == SysCfg.MemTable.Properties.TStatus.Delete) {
                GrillaDataSet.RemoveRow(UnDataSet.Index);
            }
        }
    }
    DataSet.OnAddColumn = function (Object, EventArgs) {
        var ColMem1 = new TColumn();
        ColMem1.Name = EventArgs.FieldName;
        ColMem1.Caption = EventArgs.FieldName;
        ColMem1.Index = Object.FieldDefs.length - 1;
        //ColMem1.EnabledEditor = true;
        ColMem1.DataType = EventArgs.DataType;
        ColMem1.ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.None;
        GrillaDataSet.AddColumn(ColMem1);
    }
    DataSet.OnChangeIndex = function (Object, EventArgs) {
        GrillaDataSet.IndexRow = EventArgs;
    }

    DataSet.Inicialize(true);
    //agrega definicion de campos
    DataSet.AddFields("Nombre", SysCfg.DB.Properties.TDataType.String, 10);
    DataSet.AddFields("Mensaje", SysCfg.DB.Properties.TDataType.Text, 100);
    DataSet.AddFields("Edad", SysCfg.DB.Properties.TDataType.Int32, 4);
    DataSet.AddFields("Sueldo", SysCfg.DB.Properties.TDataType.Double, 8);
    DataSet.AddFields("Credito", SysCfg.DB.Properties.TDataType.Boolean, 1);
    DataSet.AddFields("Nacimeinto", SysCfg.DB.Properties.TDataType.DateTime, 8);

    DataSet.EnableControls = false;
    DataSet.Append();
    DataSet.FieldsIndex(0, "Salvador");
    DataSet.FieldsIndex(1, "Av sub chalma 1455 col lomas de tetela cuernavaca morelos");
    DataSet.FieldsIndex(2, 36);
    DataSet.FieldsIndex(3, 10000.00);
    DataSet.FieldsIndex(4, true);
    DataSet.FieldsIndex(5, Date.now());
    DataSet.Post();
    DataSet.Append();
    DataSet.FieldsIndex(0, "Mario");
    DataSet.FieldsIndex(1, "Morelos Cuernavaca altavista");
    DataSet.FieldsIndex(2, 25);
    DataSet.FieldsIndex(3, 4000.00);
    DataSet.FieldsIndex(4, false);
    DataSet.FieldsIndex(5, Date.now());
    DataSet.Post();
    DataSet.Append();
    DataSet.FieldsIndex(0, "Diestra");
    DataSet.FieldsIndex(1, "Libetad trujillo peru");
    DataSet.FieldsIndex(2, 25);
    DataSet.FieldsIndex(3, 5000.00);
    DataSet.FieldsIndex(4, true);
    DataSet.FieldsIndex(5, Date.now());
    DataSet.Post();
    DataSet.Append();
    DataSet.FieldsIndex(0, "Mauricio");
    DataSet.FieldsIndex(1, "ancah peru");
    DataSet.FieldsIndex(2, 25);
    DataSet.FieldsIndex(3, 6000.00);
    DataSet.FieldsIndex(4, false);
    DataSet.FieldsIndex(5, Date.now());
    DataSet.Post();
    DataSet.EnableControls = true;
    DataSet.First();
    //************* FIN TAB MEM TABLE ***********************************///
}