﻿ItHelpCenter.Demo.TDemoTemporal7 = function (inObjectHtml, _this) {
	this.TParent = function () {
		return this;
	}.bind(this);

	var _this = this.TParent();
	try{

		var ObjectHtml = new TVclStackPanel(inObjectHtml, "_Mario", 1, [[12], [12], [12], [12], [12]]);

		if(!Source.Menu.IsMobil){
			ObjectHtml.Column[0].This.style.paddingRight = '20%';
			ObjectHtml.Column[0].This.style.paddingLeft = '15%';
		}


		var ContTextId = new TVclStackPanel(ObjectHtml.Column[0].This, "", 2, [[4, 8], [3, 9], [3, 9], [3, 9], [3, 9]]);
		ContTextId.Row.This.style.marginBottom = '5px';
		var ContButton = new TVclStackPanel(ObjectHtml.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
		ContButton.Row.This.classList.marginBottom = '5px';
		var Container = new TVclStackPanel(ObjectHtml.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
		

		var Votes = new Componet.TVotes(Container.Column[0].This, "", "", 44);

		var ArrayChoose = [{ id: '1', name: 'Mario',  description:'Opción 1' },
							{ id: '2', name: 'Miguel',  description:'Opción 2' },
							{ id: '3', name: 'Felipe',  description:'Opción 3' },
							{ id: '4', name: 'Salvador',  description:'Opción 4' }];

		var ArrayVotes = [{ idchoosed: '1', idvotes: '1', commentary: 'Good'},
							{ idchoosed: '2', idvotes: '4', commentary: 'Good'},
							{ idchoosed: '3', idvotes: '5', commentary: 'Good'}];

		for (var i = 0; i < ArrayVotes.length; i++){
			var ElementVotes = new Componet.TVotesElements.TItemVote();
			ElementVotes.IdNumberChoose = ArrayVotes[i]["idchoosed"];
			ElementVotes.IDCMDBCI = ArrayVotes[i]["idvotes"];
			ElementVotes.Commentary = ArrayVotes[i]["commentary"];
			Votes.TVotesItemVoteAdd(ElementVotes);
		}

		for (var i = 0; i < ArrayChoose.length; i++) {
			var ElementChoose = new Componet.TVotesElements.TItemToChoose();
			ElementChoose.Id = ArrayChoose[i]["id"];
			ElementChoose.Name = ArrayChoose[i]["name"];
			ElementChoose.Description = ArrayChoose[i]["description"];
			Votes.TVotesItemToChooseAdd(ElementChoose);
		}

		
		var btnVote = new TVclInputbutton(ContButton.Column[0].This, "");
		btnVote.Text = 'Votar';
		btnVote.VCLType = TVCLType.BS;
		btnVote.This.setAttribute('disabled', '')
		btnVote.onClick = function () {
			Votes.QuestionText = 'You, who do you want to vote for?';
			// Votes.CommentRequired = true;
			Votes.IdUser = parseInt(txtId.Text.trim());
			Votes.View();
		}
		btnVote.This.style.float = 'right';
		
		// Votes.ShowStatistic();
		
		var lblId = new TVcllabel(ContTextId.Column[0].This, "", TlabelType.H0);
		lblId.Text = 'ID';/*_this.DBTranslate.get_Text2(_this.Mythis, "ID");*/
		lblId.VCLType = TVCLType.BS;
		var txtId = new TVclTextBox(ContTextId.Column[1].This, "");
		txtId.Text = "";
		txtId.VCLType = TVCLType.BS;txtId.This.onkeyup = function(){
			if(txtId.Text.trim() != ""){
				btnVote.This.removeAttribute('disabled');
			}else{
				btnVote.This.setAttribute('disabled', '');
			}
		};
	} 
	catch (e) {
		SysCfg.Log.Methods.WriteLog("DemoTemporal7.js ItHelpCenter.Demo.TDemoTemporal7", e);
	}

	// var ContButton = new TVclStackPanel(inObjectHtml, "", 1, [[12], [12], [12], [12], [12]]);
	// var btnData = new TVclInputbutton(ContButton.Column[0].This, "");
	// btnData.Text = 'Obtener';
	// btnData.VCLType = TVCLType.BS;
	// btnData.onClick = function () {
	// 	console.log(Votes._ListItemVote);
	// }

}
