ItHelpCenter.Demo.TDemoPMGantt = function (inObjectHtml, inCallback, id) {
	this.TParent = function(){
		return this;
	}.bind(this);

	this.isInteger = function (num) {
		return (num ^ 0) === num;
	}

	var _this = this.TParent();
	_this.ID = id;
	_this.MainIndex = 0
	_this.inObjectHtml = inObjectHtml;
	_this.PManagement = new Persistence.ProjectManagement.TProjectManagementProfiler();
	_this.Element = null;
	_this.Component = null;

	var SVG = document.getElementsByTagName('svg')
	while (SVG.length != 0){ $(SVG[0]).remove();}

	 var stackPanel = new TVclStackPanel(inObjectHtml, "", 1, [[12], [12], [12], [12], [12]]);
	stackPanel.Row.This.style.marginLeft = "5px";
	stackPanel.Row.This.style.marginBottom = "5px";
	stackPanel.Column[0].This.style.padding = "15px";
	stackPanel.Column[0].This.style.border = "1px solid rgba(0, 0, 0, 0.08)";
	stackPanel.Column[0].This.style.boxShadow = " 0 0px 8px 0 rgba(0, 0, 0, 0.06), 0 1px 0px 0 rgba(0, 0, 0, 0.02);";
	stackPanel.Column[0].This.style.marginTop = "0px";
	stackPanel.Column[0].This.style.backgroundColor = "#FAFAFA";
	_this.stackPanelBody = new TVclStackPanel(stackPanel.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
	_this.ObjectHtml = _this.stackPanelBody.Column[0].This;
	_this.ObjectHtml.style.minHeight = "500px";
	
	_this.Mythis = "PMGantt";
	UsrCfg.Traslate.GetLangText(_this.Mythis, "TabPag1.Caption", "Task");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "NavBar_Add.Text", "Add");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "NavBar_Delete.Text", "Delete");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "NavBar_Update.Text", "Update");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "ItemGroupBar_Indent.Text", "Indentation");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "ItemGroupBar_CancelIndent.Text", "Cancel Indent");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "ItemGroupBar_MoveUp.Text", "Move To Up");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "ItemGroupBar_MoveDown.Text", "Move To Down");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "ItemGroupBar_Link.Text", "Link");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "ItemGroupBar_UnLink.Text", "Unlink");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "ItemGroupBar_Resource.Text", "Resource");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "ItemGroupBar_Zoom.Text", "Zoom");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "ItemGroupBar_ZoomOut.Text", "Zoom Out");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "ItemGroupBar_ZoomIn.Text", "Zoom In");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "ItemGroupBar_FitAll.Text", "Fit All");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "NavBar_BaseLine.Text", "Base Line");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "NavBar_CriticalPath.Text", "Critical Path");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "NavBar_Scale.Text", "Scale");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "NavBar_ExportExcel.Text", "Excel");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "NavBar_ExportPdf.Text", "Pdf");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "lblUnit.Text", "Unit");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "lblCount.Text", "Count");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "btnAccept.Text", "Accept");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "lblName.Text", "NAME");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "lblDuration.Text", "DURATION");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "lblPercentage.Text", "PERCENTAGE(%)");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "ListBoxItem_Estimated.Text", "ESTIMATED");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "lblPriority.Text", "PRIORITY");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "lblCost.Text", "COST");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "lblStartDate.Text", "START DATE");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "lblEndDate.Text", "END DATE");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "lblNote.Text", "NOTE");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "btnSave.Text", "Save");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "lblTitlePre.Text", "Predecessors");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "lblTaskP.Text", "Task List");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "lblTypeRelationPre.Text", "Type Relation");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "lblTitleSuc.Text", "Successors");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "lblTaskS.Text", "Task List");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "lblTypeRelationSuc.Text", "Type Relation");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "btnAdd.Text", "Add");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "btnRemove.Text", "Remove");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "lblResources.Text", "RESOURCE");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "lblUnits.Text", "UNITS");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "lblDescription.Text", "DESCRIPTION");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "btnAssign.Text", "Assign");

	UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1", "Select a Task");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2", "The selected task can not be devised");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3", "There is no project to show");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines4", "Temporary Scale");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines5", "Add New Task");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines6", "Update Task");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines7", "Can't move higher");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines8", "Can't move lower");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines9", "New Link");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines10", "Unlink");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines11", "New Resources Allocation");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines12", "Select a resource");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines13", "The date fields can not be empty");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines14", "The priority field is not a number");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines15", "It can not exceed 100%");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines16", "The percentage field is not a number");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines17", "The name field is empty");

	UsrCfg.Traslate.GetLangText(_this.Mythis, "INDEX", "INDEX");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "NAME", "NAME");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "RESOURCENAME", "RESOURCE NAME");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "UNITS", "UNITS");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "DESCRIPTION", "DESCRIPTION");

	UsrCfg.Traslate.GetLangText(_this.Mythis, "Units1", "Year");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "Units2", "Semester");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "Units3", "Quarter");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "Units4", "Month");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "Units5", "Week");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "Units6", "Day");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "Units7", "Hour");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "Units8", "Minute");

	UsrCfg.Traslate.GetLangText(_this.Mythis, "DURATION1", "Day");
	UsrCfg.Traslate.GetLangText(_this.Mythis, "DURATION2", "Days");

	_this.InicializeComponent();
}

ItHelpCenter.Demo.TDemoPMGantt.prototype.InicializeComponent = function () {
	var _this = this.TParent();
	_this.Fila1 = new TVclStackPanel(_this.ObjectHtml, "", 1, [[12], [12], [12], [12], [12]]);
	_this.Fila2 = new TVclStackPanel(_this.ObjectHtml, "", 1, [[12], [12], [12], [12], [12]]);
	_this.CreateGantt();
	_this.NavTabControls = new ItHelpCenter.Componet.NavTabControls.TNavTabControls(_this.Fila1.Column[0].This, null);
	_this.NavTabControls.AutoScroll = false;
	// _this.NavTabControls.VisibleHeaderTabControl= false;

	var TabPag1 = new TTabPage();//Tabs
	TabPag1.Name = "Task";
	TabPag1.Caption = UsrCfg.Traslate.GetLangText(_this.Mythis, "TabPag1.Caption");
	TabPag1.Active = true;
	TabPag1.Tag = "";

	_this.NavTabControls.AddTabPages(TabPag1);//Agregas tabs

	_this.NavPanelBarPanel1 = new ItHelpCenter.Componet.NavTabControls.TNavPanelBar(TabPag1);
	_this.NavPanelBarPanel1.Title = "";//"Panel 1";
	_this.NavPanelBarPanel2 = new ItHelpCenter.Componet.NavTabControls.TNavPanelBar(TabPag1);
	_this.NavPanelBarPanel2.Title = "";//"Panel 2";
	_this.NavPanelBarPanel3 = new ItHelpCenter.Componet.NavTabControls.TNavPanelBar(TabPag1);
	_this.NavPanelBarPanel3.Title = "";//"Panel 3";
	_this.NavPanelBarPanel4 = new ItHelpCenter.Componet.NavTabControls.TNavPanelBar(TabPag1);
	_this.NavPanelBarPanel4.Title = "";//"Panel 4";
	_this.NavPanelBarPanel5 = new ItHelpCenter.Componet.NavTabControls.TNavPanelBar(TabPag1);
	_this.NavPanelBarPanel5.Title = "";//"Panel 5";

	/*Creamos TNavBar = Imagen y texto Abajo*/
	_this.NavBar_Add = new ItHelpCenter.Componet.NavTabControls.TNavBar();
	_this.NavBar_Add.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "NavBar_Add.Text");
	_this.NavBar_Add.SrcImg = "image/icon-gantt/add(24).png";
	_this.NavBar_Add.ChekedButton = false;
	_this.NavBar_Add.Opacity = "0.85";
	_this.NavBar_Add.TextColor = "#757575";
	_this.NavBar_Add.onclick = function (NavBar) {
		var MAINTASK =_this.PMFill();
		var index = 0;
		if(_this.Element != null){
			index = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(MAINTASK.PMDETAILTASKList, _this.Element.Ga.id);
			_this.Insert(MAINTASK.PMDETAILTASKList[index], _this.Component);
		}
		else{
			_this.Insert(null, _this.Component);
		}
		_this.Element = null;
	}

	_this.NavBar_Delete = new ItHelpCenter.Componet.NavTabControls.TNavBar();
	_this.NavBar_Delete.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "NavBar_Delete.Text");
	_this.NavBar_Delete.SrcImg = "image/icon-gantt/delete(24).png";
	_this.NavBar_Delete.ChekedButton = false;
	_this.NavBar_Delete.Opacity = "0.85";
	_this.NavBar_Delete.TextColor = "#757575";
	_this.NavBar_Delete.onclick = function (NavBar) {
		if(_this.Element == null){
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1"));
		}
		else{
			var MAINTASK =_this.PMFill();
			var index = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(MAINTASK.PMDETAILTASKList, _this.Element.Ga.id);
			_this.Delete(MAINTASK.PMDETAILTASKList[index], _this.Component);
			_this.Element = null;
		}
	}

	_this.NavBar_Update = new ItHelpCenter.Componet.NavTabControls.TNavBar();
	_this.NavBar_Update.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "NavBar_Update.Text");
	_this.NavBar_Update.SrcImg = "image/icon-gantt/update(24).png";
	_this.NavBar_Update.ChekedButton = false;
	_this.NavBar_Update.Opacity = "0.85";
	_this.NavBar_Update.TextColor = "#757575";
	_this.NavBar_Update.onclick = function (NavBar) {
		if(_this.Element == null){
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1"));
		}
		else{
			var MAINTASK =_this.PMFill();
			var index = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(MAINTASK.PMDETAILTASKList, _this.Element.Ga.id);
			_this.Update(MAINTASK.PMDETAILTASKList[index], _this.Component);
			_this.Element = null;
		}
	}

	_this.NavGroupBar2 = new ItHelpCenter.Componet.NavTabControls.TNavGroupBar(); //Grupo Padre

	_this.ItemGroupBar_Indent = new ItHelpCenter.Componet.NavTabControls.TItemGroupBar(); //Item grupo
	_this.ItemGroupBar_Indent.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ItemGroupBar_Indent.Text");
	_this.ItemGroupBar_Indent.ChekedButton = false;
	_this.ItemGroupBar_Indent.SrcImg = "image/icon-gantt/right-indent(16).png";	
	_this.ItemGroupBar_Indent.onclick = function () {
		if(_this.Element == null){
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1"));
		}
		else{
			var MAINTASK =_this.PMFill();
			var index = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(MAINTASK.PMDETAILTASKList, _this.Element.Ga.id);
			if(index != 0){ // determinar si la tarea no es el primer padre
				var indexOrderChild = -1; 
				if(MAINTASK.PMDETAILTASKList[index].IDPMDETAILTASK_PARENT != 0){  //determinar si es parent o no
					var indexParent = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(MAINTASK.PMDETAILTASKList, MAINTASK.PMDETAILTASKList[index].IDPMDETAILTASK_PARENT);
					indexOrderChild = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(MAINTASK.PMDETAILTASKList[indexParent].PMDETAILTASK_CHILDList, MAINTASK.PMDETAILTASKList[index].IDPMDETAILTASK)
				}

				if(indexOrderChild != 0){ //determina si la tarea no es la primera de sus hermanos
					_this.Indent(MAINTASK.PMDETAILTASKList[index], _this.Component);
					_this.Element = null;
				}
				else if(indexOrderChild == 0){
					alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2"));
				}
			}
			else if(index == 0){
				alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2"));
			}
		}
	}
	_this.ItemGroupBar_Indent.Li.style.marginBottom = '5px';

	_this.ItemGroupBar_CancelIndent = new ItHelpCenter.Componet.NavTabControls.TItemGroupBar();//Item grupo - Item grupo Padre
	_this.ItemGroupBar_CancelIndent.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ItemGroupBar_CancelIndent.Text");
	_this.ItemGroupBar_CancelIndent.ChekedButton = false;
	_this.ItemGroupBar_CancelIndent.SrcImg = "image/icon-gantt/left-indent(16).png";
	_this.ItemGroupBar_CancelIndent.onclick = function () {
		if(_this.Element == null){
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1"));
		}
		else{
			var MAINTASK =_this.PMFill();
			var index = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(MAINTASK.PMDETAILTASKList, _this.Element.Ga.id);
			if(MAINTASK.PMDETAILTASKList[index].IDPMDETAILTASK_PARENT > 0){
				_this.CancelIndent(MAINTASK.PMDETAILTASKList[index], _this.Component);
				_this.Element = null;
			}
		}
	}

	_this.NavGroupBar3 = new ItHelpCenter.Componet.NavTabControls.TNavGroupBar(); //Grupo Padre

	_this.ItemGroupBar_MoveUp = new ItHelpCenter.Componet.NavTabControls.TItemGroupBar(); //Item grupo
	_this.ItemGroupBar_MoveUp.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ItemGroupBar_MoveUp.Text");
	_this.ItemGroupBar_MoveUp.ChekedButton = false;
	_this.ItemGroupBar_MoveUp.SrcImg = "image/icon-gantt/move-up(16).png";	
	_this.ItemGroupBar_MoveUp.onclick = function () {
		if(_this.Element == null){
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1"));
		}
		else{
			var MAINTASK =_this.PMFill();
			var index = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(MAINTASK.PMDETAILTASKList, _this.Element.Ga.id);
			_this.MoveUp(MAINTASK.PMDETAILTASKList[index], _this.Component);
			_this.Element = null;
		}
	}
	_this.ItemGroupBar_MoveUp.Li.style.marginBottom = '5px';

	_this.ItemGroupBar_MoveDown = new ItHelpCenter.Componet.NavTabControls.TItemGroupBar();//Item grupo - Item grupo Padre
	_this.ItemGroupBar_MoveDown.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ItemGroupBar_MoveDown.Text");
	_this.ItemGroupBar_MoveDown.ChekedButton = false;
	// _this.ItemGroupBar_MoveDown.Width = "20px";
	_this.ItemGroupBar_MoveDown.SrcImg = "image/icon-gantt/move-down(16).png";
	_this.ItemGroupBar_MoveDown.onclick = function () {
		if(_this.Element == null){
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1"));
		}
		else{
			var MAINTASK =_this.PMFill();
			var index = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(MAINTASK.PMDETAILTASKList, _this.Element.Ga.id);
			_this.MoveDown(MAINTASK.PMDETAILTASKList[index], _this.Component);
			_this.Element = null;
		}
	}

	_this.NavGroupBar4 = new ItHelpCenter.Componet.NavTabControls.TNavGroupBar();

	_this.ItemGroupBar_Link = new ItHelpCenter.Componet.NavTabControls.TItemGroupBar(); //Item grupo
	_this.ItemGroupBar_Link.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ItemGroupBar_Link.Text");
	_this.ItemGroupBar_Link.ChekedButton = false;
	_this.ItemGroupBar_Link.SrcImg = "image/icon-gantt/link(64).png";	
	_this.ItemGroupBar_Link.onclick = function () {
		if(_this.Element == null){
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1"));
		}
		else{
			var MAINTASK =_this.PMFill();
			var index = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(MAINTASK.PMDETAILTASKList, _this.Element.Ga.id);
			_this.Link(MAINTASK.PMDETAILTASKList[index], _this.Component);
			_this.Element = null;
		}
	}
	_this.ItemGroupBar_Link.Li.style.marginBottom = '5px';

	_this.ItemGroupBar_UnLink = new ItHelpCenter.Componet.NavTabControls.TItemGroupBar();//Item grupo - Item grupo Padre
	_this.ItemGroupBar_UnLink.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ItemGroupBar_UnLink.Text");
	_this.ItemGroupBar_UnLink.ChekedButton = false;
	_this.ItemGroupBar_UnLink.SrcImg = "image/icon-gantt/unlink(64).png";
	_this.ItemGroupBar_UnLink.onclick = function () {
		if(_this.Element == null){
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1"));
		}
		else{
			var MAINTASK =_this.PMFill();
			var index = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(MAINTASK.PMDETAILTASKList, _this.Element.Ga.id);
			_this.UnLink(MAINTASK.PMDETAILTASKList[index], _this.Component);
			_this.Element = null;
		}
	}

	_this.NavGroupBar5 = new ItHelpCenter.Componet.NavTabControls.TNavGroupBar();

	_this.ItemGroupBar_Resource = new ItHelpCenter.Componet.NavTabControls.TItemGroupBar(); //Item grupo
	_this.ItemGroupBar_Resource.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ItemGroupBar_Resource.Text");
	_this.ItemGroupBar_Resource.ChekedButton = false;
	_this.ItemGroupBar_Resource.SrcImg = "image/icon-gantt/resource(24).png";
	_this.ItemGroupBar_Resource.onclick = function () {
		if(_this.Element == null){
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1"));
		}
		else{
			var MAINTASK =_this.PMFill();
			var index = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(MAINTASK.PMDETAILTASKList, _this.Element.Ga.id);
			_this.ResourcesAllocation(MAINTASK.PMDETAILTASKList[index], _this.Component);
			_this.Element = null;
		}
	}
	_this.ItemGroupBar_Resource.Li.style.marginBottom = '5px';

	_this.ItemGroupBar_Zoom = new ItHelpCenter.Componet.NavTabControls.TItemGroupBar();//Item grupo - Item grupo Padre
	_this.ItemGroupBar_Zoom.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ItemGroupBar_Zoom.Text");
	_this.ItemGroupBar_Zoom.ChekedButton = false;
	_this.ItemGroupBar_Zoom.SrcImg = "image/24/Search.png";
	// _this.ItemGroupBar_Zoom.onclick = function () {}

	_this.ItemGroupBar_ZoomOut = new ItHelpCenter.Componet.NavTabControls.TItemGroupBar();//Item grupo - Item grupo Padre
	_this.ItemGroupBar_ZoomOut.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ItemGroupBar_ZoomOut.Text");
	_this.ItemGroupBar_ZoomOut.ChekedButton = false;
	_this.ItemGroupBar_ZoomOut.SrcImg = "image/24/Zoom-out.png";
	_this.ItemGroupBar_ZoomOut.onclick = function () {
		_this.Component._chart.zoomOut();
	}

	_this.ItemGroupBar_ZoomIn = new ItHelpCenter.Componet.NavTabControls.TItemGroupBar();//Item grupo - Item grupo Padre
	_this.ItemGroupBar_ZoomIn.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ItemGroupBar_ZoomIn.Text");
	_this.ItemGroupBar_ZoomIn.ChekedButton = false;
	_this.ItemGroupBar_ZoomIn.SrcImg = "image/24/Zoom-in.png";
	_this.ItemGroupBar_ZoomIn.onclick = function () {
		_this.Component._chart.zoomIn();
	}

	_this.ItemGroupBar_FitAll = new ItHelpCenter.Componet.NavTabControls.TItemGroupBar();//Item grupo - Item grupo Padre
	_this.ItemGroupBar_FitAll.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ItemGroupBar_FitAll.Text");
	_this.ItemGroupBar_FitAll.ChekedButton = false;
	_this.ItemGroupBar_FitAll.SrcImg = "image/24/Zoom-in.png";
	_this.ItemGroupBar_FitAll.onclick = function () {
		_this.Component._chart.fitAll();
	}

	_this.ItemGroupBar_Zoom.addItemGroupBar(_this.ItemGroupBar_ZoomIn);
	_this.ItemGroupBar_Zoom.addItemGroupBar(_this.ItemGroupBar_ZoomOut);
	_this.ItemGroupBar_Zoom.addItemGroupBar(_this.ItemGroupBar_FitAll);

	// _this.NavBar_Resources = new TNavBar();
	// _this.NavBar_Resources.Text = "Resource";
	// _this.NavBar_Resources.SrcImg = "image/icon-gantt/resource(24).png";
	// _this.NavBar_Resources.ChekedButton = false;
	// _this.NavBar_Resources.Opacity = "0.85";
	// _this.NavBar_Resources.TextColor = "#757575";
	// _this.NavBar_Resources.onclick = function (NavBar) {
	// 	if(_this.Element == null){
	// 		alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1"));
	// 	}
	// 	else{
	// 		var MAINTASK =_this.PMFill();
	// 		var index = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(MAINTASK.PMDETAILTASKList, _this.Element.Ga.id);
	// 		_this.ResourcesAllocation(MAINTASK.PMDETAILTASKList[index], _this.Component);
	// 		_this.Element = null;
	// 	}
	// }

	_this.CtrlBaseLine = false;
	_this.NavBar_BaseLine = new ItHelpCenter.Componet.NavTabControls.TNavBar();
	_this.NavBar_BaseLine.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "NavBar_BaseLine.Text");
	_this.NavBar_BaseLine.SrcImg = "image/icon-gantt/lineabase(64).png";
	_this.NavBar_BaseLine.ChekedButton = true;
	_this.NavBar_BaseLine.Opacity = "0.85";
	_this.NavBar_BaseLine.TextColor = "#757575";
	_this.NavBar_BaseLine.onclick = function (NavBar) {
		if(_this.CtrlBaseLine){
			if(_this.CtrlCriticalPath){
				_this.CriticalPath(_this.Component);
			}
			else{
				var MAINTASK =_this.PMFill();
				_this.Component.CleanData(); // Limpiar data y luego almacenar nuevos datos
				for(var i = 0 ;  i < MAINTASK.PMDETAILTASKPARENTList.length ; i++){
					_this.GetChild(MAINTASK.PMDETAILTASKPARENTList[i], _this.Component);
				}
				_this.Component.Refresh();
			}
			_this.CtrlBaseLine = false;
		}
		else{
			_this.Component.GenerateBaseLine();
			_this.CtrlBaseLine = true;
		}
	}

	_this.CtrlCriticalPath = false;
	_this.NavBar_CriticalPath = new ItHelpCenter.Componet.NavTabControls.TNavBar();
	_this.NavBar_CriticalPath.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "NavBar_CriticalPath.Text");
	_this.NavBar_CriticalPath.SrcImg = "image/icon-gantt/critical.png";
	_this.NavBar_CriticalPath.ChekedButton = true;
	_this.NavBar_CriticalPath.Opacity = "0.85";
	_this.NavBar_CriticalPath.TextColor = "#757575";
	_this.NavBar_CriticalPath.onclick = function (NavBar) {
		if(_this.CtrlCriticalPath){
			var MAINTASK =_this.PMFill();
			_this.Component.CleanData(); // Limpiar data y luego almacenar nuevos datos
			for(var i = 0 ;  i < MAINTASK.PMDETAILTASKPARENTList.length ; i++){
				_this.GetChild(MAINTASK.PMDETAILTASKPARENTList[i], _this.Component);
			}
			_this.Component.Refresh();
			_this.CtrlCriticalPath = false;
		}
		else{
			_this.CriticalPath(_this.Component);
			_this.CtrlCriticalPath = true;
		}
		if(_this.CtrlBaseLine){
			_this.Component.GenerateBaseLine();
		}
	}

	_this.NavBar_Fill = new ItHelpCenter.Componet.NavTabControls.TNavBar();
	_this.NavBar_Fill.Text = "Demo Fill";
	_this.NavBar_Fill.SrcImg = "image/24/Save.png";
	_this.NavBar_Fill.ChekedButton = true;
	_this.NavBar_Fill.Opacity = "0.85";
	_this.NavBar_Fill.TextColor = "#757575";
	_this.NavBar_Fill.onclick = function (NavBar) {
		_this.PManagement.Fill();
		if (_this.PManagement.PMMAINTASKList.length === 0) {
			_this.PManagement.PMCreateAll();
			_this.NavBar_Fill.divContentSubPanel.style.display = 'none';
			alert('Records created in the database');
			_this.CreateGantt();
		}
		else {
			alert('Records already exist in the Database');
		}
	}

	_this.NavBar_Scale = new ItHelpCenter.Componet.NavTabControls.TNavBar();
	_this.NavBar_Scale.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "NavBar_Scale.Text");
	_this.NavBar_Scale.SrcImg = "image/24/Place-an-order.png";
	_this.NavBar_Scale.ChekedButton = false;
	_this.NavBar_Scale.Opacity = "0.85";
	_this.NavBar_Scale.TextColor = "#757575";
	_this.NavBar_Scale.onclick = function (NavBar) {
		_this.TemporalScale(_this.Component);
	}

	_this.NavBar_ExportExcel = new ItHelpCenter.Componet.NavTabControls.TNavBar();
	_this.NavBar_ExportExcel.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "NavBar_ExportExcel.Text");
	_this.NavBar_ExportExcel.SrcImg = "image/24/Excel2.png";
	_this.NavBar_ExportExcel.ChekedButton = false;
	_this.NavBar_ExportExcel.Opacity = "0.85";
	_this.NavBar_ExportExcel.TextColor = "#757575";
	_this.NavBar_ExportExcel.onclick = function (NavBar) {
		_this.Export(_this.Component)
	}

	_this.NavBar_ExportPdf = new ItHelpCenter.Componet.NavTabControls.TNavBar();
	_this.NavBar_ExportPdf.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "NavBar_ExportPdf.Text");
	_this.NavBar_ExportPdf.SrcImg = "image/24/Pdf3.png";
	_this.NavBar_ExportPdf.ChekedButton = false;
	_this.NavBar_ExportPdf.Opacity = "0.85";
	_this.NavBar_ExportPdf.TextColor = "#757575";
	_this.NavBar_ExportPdf.onclick = function (NavBar) {
		_this.Component._chart.saveAsPdf();
	}

	_this.NavGroupBar2.addItemGroupBar(_this.ItemGroupBar_Indent);
	_this.NavGroupBar2.addItemGroupBar(_this.ItemGroupBar_CancelIndent);
	_this.NavGroupBar3.addItemGroupBar(_this.ItemGroupBar_MoveUp);
	_this.NavGroupBar3.addItemGroupBar(_this.ItemGroupBar_MoveDown);
	_this.NavGroupBar4.addItemGroupBar(_this.ItemGroupBar_Link);
	_this.NavGroupBar4.addItemGroupBar(_this.ItemGroupBar_UnLink);
	_this.NavGroupBar5.addItemGroupBar(_this.ItemGroupBar_Zoom);
	_this.NavGroupBar5.addItemGroupBar(_this.ItemGroupBar_Resource);

	_this.NavPanelBarPanel1.addItemBar(_this.NavBar_Add);
	_this.NavPanelBarPanel1.addItemBar(_this.NavBar_Delete);
	_this.NavPanelBarPanel1.addItemBar(_this.NavBar_Update);
	_this.NavPanelBarPanel2.addItemBar(_this.NavGroupBar2);
	_this.NavPanelBarPanel3.addItemBar(_this.NavGroupBar3);
	_this.NavPanelBarPanel4.addItemBar(_this.NavGroupBar4);
	_this.NavPanelBarPanel5.addItemBar(_this.NavGroupBar5);
	// _this.NavPanelBarPanel5.addItemBar(_this.NavBar_Resources);

	_this.NavPanelBarPanel5.addItemBar(_this.NavBar_BaseLine);
	_this.NavPanelBarPanel5.addItemBar(_this.NavBar_CriticalPath);
	_this.NavPanelBarPanel5.addItemBar(_this.NavBar_Scale);
	_this.NavPanelBarPanel5.addItemBar(_this.NavBar_ExportExcel);
	_this.NavPanelBarPanel5.addItemBar(_this.NavBar_ExportPdf);

	_this.PManagement.Fill();
	if (_this.PManagement.PMMAINTASKList.length === 0) {
		_this.NavPanelBarPanel5.addItemBar(_this.NavBar_Fill);
	}

	_this.NavTabControls.BackgroundHeader = "#0091FF";
}

ItHelpCenter.Demo.TDemoPMGantt.prototype.CreateGantt = function(){
	var _this = this.TParent();

	_this.PManagement = new Persistence.ProjectManagement.TProjectManagementProfiler();
	_this.PManagement.Fill();
	// var MAINTASK = _this.PMFill();
	$(_this.Fila2.Column[0].This).html('');
	var Container = new TVclStackPanel(_this.Fila2.Column[0].This, 'ContainerDemoGantt', 1, [[12], [12], [12], [12], [12]]);
	var Container_1 = Container.Column[0].This;
	var Gantt = new Componet.TGantt(Container_1, "DemoGantt", "");

	var ArrayElements = new Array();
	try{
		if(_this.PManagement.PMMAINTASKList.length != 0){
			_this.MainIndex = Persistence.ProjectManagement.Methods.PMMAINTASK_ListGetIndex(_this.PManagement.PMMAINTASKList, _this.ID)
			var DetailParent = _this.PManagement.PMMAINTASKList[_this.MainIndex].PMDETAILTASKPARENTList;

			for(var i = 0 ;  i < DetailParent.length ; i++){
				_this.GetChild(DetailParent[i], Gantt);
			}
			
			Gantt.Height = '500px';
			Gantt.SplitterPosition = 400;
			Gantt.ProjectName = _this.PManagement.PMMAINTASKList[_this.MainIndex].MAINTASK_NAME;
			Gantt.ProjectDuration = _this.PManagement.PMMAINTASKList[_this.MainIndex].MAINTASK_DURATION;
			Gantt.FitAll = true;
			Gantt.ColumnName = '';
			Gantt.ColumnDuration = '';
			Gantt.ColumnStartTime = '';
			Gantt.ColumnEndTime = '';
			Gantt.ColumnResources = '';
			Gantt.DataGrid = true;
			Gantt.BaseLine = true;
			Gantt.ZoomUnit = _this.PManagement.PMMAINTASKList[_this.MainIndex].MAINTASK_SCALEUNITS;
			Gantt.ZoomCount = _this.PManagement.PMMAINTASKList[_this.MainIndex].MAINTASK_SCALECOUNTER;
			Gantt.ScaleMinimum = _this.PManagement.PMMAINTASKList[_this.MainIndex].MAINTASK_STARTDATE;
			Gantt.ScaleMaximum = _this.PManagement.PMMAINTASKList[_this.MainIndex].MAINTASK_ENDDATE;
			Gantt.Create();
			Gantt.eventClick = function(element, data){
				_this.Element = element;
			}
			_this.Component = Gantt;
		}
		else{
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3"));
		}
	} catch (e) {
		SysCfg.Log.Methods.WriteLog("DemoPMGantt.js ItHelpCenter.Demo.TDemoPMGantt.prototype.CreateGantt", e);
	}
}

ItHelpCenter.Demo.TDemoPMGantt.prototype.Export = function(component) {
	var _this = this.TParent();
	var ContenTextArea = new TVclStackPanel(_this.inObjectHtml, "", 1, [[12], [12], [12], [12], [12]]);
	ContenTextArea.Row.This.style.display = "none";
	var textArea1 = document.createElement("iframe");
	textArea1.id = "txtArea1"
	ContenTextArea.Column[0].This.appendChild(textArea1);

	var MAINTASK = _this.PMFill();
	var tab_text = '<table border="1px" style="font-size:20px" ">';

	// the first headline of the table
	var headerTable = '<tr bgcolor="#DFDFDF">' +
						'<th>#</th>'+
						'<th>' + component.ColumnName + '</th>'+
						'<th>' + component.ColumnDuration + '</th>'+
						'<th>' + component.ColumnStartTime + '</th>'+
						'<th>' + component.ColumnEndTime + '</th>'+
						'<th>' + component.ColumnResources + '</th> </tr>';
	var bodyTable = '<tr>';

	for(var i = 0 ; i < component.data.length ; i++){     
		bodyTable += '<td>'+ i + '</td>'+
						'<td>' + component.data[i].name + '</td>'+
						'<td>' + component.data[i].duration + '</td>'+
						'<td>' + _this.ToDateTimeMeridian(new Date(component.data[i].actualStart)) + '</td>'+
						'<td>' + _this.ToDateTimeMeridian(new Date(component.data[i].actualEnd)) + '</td>'+
						'<td>' + component.data[i].resources + '</td> </tr>';
						// component.data[i].Connector 
	}
	tab_text += headerTable + bodyTable + "</table>";
	tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
	tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
	tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

	var ua = window.navigator.userAgent;
	var msie = ua.indexOf("MSIE "); 

	if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
	{
		var txtArea1 = document.getElementById('txtArea1');
		txtArea1.contentWindow.document.open("txt/html","replace");
		txtArea1.contentWindow.document.write(tab_text);
		txtArea1.contentWindow.document.close();
		txtArea1.contentWindow.focus(); 
		sa = txtArea1.contentWindow.document.execCommand("SaveAs", true, MAINTASK.MAINTASK_NAME + ".xls");
		$("#txtArea1").contents().find("body").html('');
	}  
	else {               //other browser not tested on IE 11
		// sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));
		sa = window.open('data:application/vnd.ms-excel,' + escape(tab_text)); //Correccion de caracteres
	}
	return sa;
}

ItHelpCenter.Demo.TDemoPMGantt.prototype.TemporalScale = function(component){
	var _this = this.TParent();
	var Modal = new TVclModal(document.body, null/*_this.inObjectHtml*/, "");
	Modal.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines4"));
	Modal.ShowModal();

	var Container = new TVclStackPanel(Modal.Body.This, 'TemporalScale', 1, [[12], [12], [12], [12], [12]]);
	var Container_1 = new TVclStackPanel(Container.Column[0].This, '', 2, [[12,12], [6,6], [6,6], [6,6], [6,6]]);

	var ContUnit = new TVclStackPanel(Container_1.Column[0].This, "", 2, [[4, 8], [2, 10], [2, 10], [2, 10], [2, 8]]);
	ContUnit.Row.This.style.marginBottom = '5px';
	var lblUnit = new TVcllabel(ContUnit.Column[0].This, "", TlabelType.H0);
	lblUnit.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblUnit.Text");
	lblUnit.VCLType = TVCLType.BS;
	var cbUnit = new TVclComboBox2(ContUnit.Column[1].This, "");
	cbUnit.VCLType = TVCLType.BS;
	var arrUnits = new Array();
	arrUnits.push({id:1, name: UsrCfg.Traslate.GetLangText(_this.Mythis, "Units1")}); // year - año
	arrUnits.push({id:2, name: UsrCfg.Traslate.GetLangText(_this.Mythis, "Units2")}); // semester - semestre
	arrUnits.push({id:3, name: UsrCfg.Traslate.GetLangText(_this.Mythis, "Units3")}); // quarter - trimestre
	arrUnits.push({id:4, name: UsrCfg.Traslate.GetLangText(_this.Mythis, "Units4")}); // month - mes
	arrUnits.push({id:5, name: UsrCfg.Traslate.GetLangText(_this.Mythis, "Units5")}); // week - semana
	arrUnits.push({id:6, name: UsrCfg.Traslate.GetLangText(_this.Mythis, "Units6")}); // day - dia
	arrUnits.push({id:7, name: UsrCfg.Traslate.GetLangText(_this.Mythis, "Units7")}); // hour - horas
	arrUnits.push({id:8, name: UsrCfg.Traslate.GetLangText(_this.Mythis, "Units8")}); // minute - minutos
	
	for(var i = 0 ; i < arrUnits.length; i++){
		var ComboItem = new TVclComboBoxItem();
		ComboItem.Value = arrUnits[i].id;
		ComboItem.Text = arrUnits[i].name;
		ComboItem.Tag = "TestUnit";
		cbUnit.AddItem(ComboItem);
	}
	if(component.ZoomUnit != null){
		for(var i = 0 ; i < cbUnit.Options.length ;i++){
			if( unitValue(parseInt(cbUnit.Options[i].Value)) == component.ZoomUnit){
				cbUnit.selectedIndex = i;
			}
		}
	}
	else{
		cbUnit.selectedIndex = 5;
	}

	cbUnit.onChange = function(){
		var count = parseInt(txtCount.Text.trim());
		if(count != ''){
			Gantt.ZoomTo(unitValue(parseInt(cbUnit.Value)), count);
		}
	}

	var ContTextCount = new TVclStackPanel(Container_1.Column[1].This, "", 2, [[4, 8], [2, 10], [2, 10], [2, 10], [2, 8]]);
	ContTextCount.Row.This.style.marginBottom = '10px';
	var lblCount = new TVcllabel(ContTextCount.Column[0].This, "", TlabelType.H0);
	lblCount.Text =  UsrCfg.Traslate.GetLangText(_this.Mythis, "lblCount.Text");
	lblCount.VCLType = TVCLType.BS;
	var txtCount = new TVclTextBox(ContTextCount.Column[1].This, "");
	if(component.ZoomCount != null){
		txtCount.Text = component.ZoomCount;	
	}
	else{
		txtCount.Text = "1";
	}
	txtCount.VCLType = TVCLType.BS;
	txtCount.This.onkeyup = function(){
		var count = parseInt(txtCount.Text.trim());
		if(count != ''){
			Gantt.ZoomTo(unitValue(parseInt(cbUnit.Value)), count);
		}
	}
	function unitValue(id){
		switch (id) {
			case 1:
				return 'year';
				break;
			case 2:
				return  "semester";
				break;
			case 3:
				return  "quarter";
				break;
			case 4:
				return  "month";
				break;
			case 5:
				return  "week";
				break;
			case 6:
				return  "day";
				break;
			case 7:
				return  "hour";
				break;
			case 8:
				return  "minute";
		}
	}

	var GanttContent = new TVclStackPanel(Container.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
	var GanttContent_1 = GanttContent.Column[0].This;
	var Gantt = new Componet.TGantt(GanttContent_1, "DemoGantt", "");
	
	var GanttElement = new Componet.TElements.TGanttElement();
	GanttElement.Id = 1;
	GanttElement.Index = 1;
	GanttElement.Name = 'Demo';
	GanttElement.Parent = 0;
	GanttElement.Duration = component.ProjectDuration;
	GanttElement.Resources = '';
	GanttElement.ProgressValue = '0%';
	GanttElement.ActualStart = component.ScaleMinimum;
	GanttElement.ActualEnd = component.ScaleMaximum;
	GanttElement.Connector = null;
	GanttElement.BaseLineStart = component.ScaleMinimum;
	GanttElement.BaseLineEnd = component.ScaleMaximum;
	Gantt.TGanttDataAdd(GanttElement);

	Gantt.Height = '150px';
	Gantt.SplitterPosition = 400;
	Gantt.ProjectName = component.ProjectName;
	Gantt.ProjectDuration = component.ProjectDuration;
	Gantt.FitAll = false;
	Gantt.ColumnName = '';
	Gantt.ColumnDuration = '';
	Gantt.ColumnStartTime = '';
	Gantt.ColumnEndTime = '';
	Gantt.ColumnResources = '';
	Gantt.DataGrid = false;
	Gantt.BaseLine = false;
	Gantt.ScaleMinimum = component.ScaleMinimum;
	Gantt.ScaleMaximum = component.ScaleMaximum;
	if( component.ZoomUnit!= null && component.ZoomUnit!= '' ){
		Gantt.ZoomUnit = component.ZoomUnit;
	}
	else{
		Gantt.ZoomUnit = unitValue(parseInt(cbUnit.Value));
	}
	if( component.ZoomCount!= null && component.ZoomCount!= '' ){
		Gantt.ZoomCount = component.ZoomCount;
	}
	else{
		Gantt.ZoomCount = parseInt(txtCount.Text.trim());
	}
	Gantt.Create();
	Gantt.eventClick = function(element, data){
		_this.Element = element;
	}

	var ContBtnSave = new TVclStackPanel(Container.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
	ContBtnSave.Row.This.style.marginTop = '10px';
	ContBtnSave.Row.This.style.marginBottom = '5px';
	var btnAccept = new TVclButton(ContBtnSave.Column[0].This, "");
	btnAccept.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnAccept.Text");
	btnAccept.VCLType = TVCLType.BS;
	btnAccept.This.style.width = '98px';
	btnAccept.Src = "image/16/Accept.png";
	btnAccept.onClick = function () {
		var count = parseInt(txtCount.Text.trim());
		if(count != ''){
			// component.ZoomTo(unitValue(parseInt(cbUnit.Value)), count);
			_this.SaveScale(unitValue(parseInt(cbUnit.Value)), count, component);
		}
		Modal.CloseModal();
		$(Modal.This.This).remove();
	}
	btnAccept.This.style.float = 'right';
	btnAccept.This.style.padding = '0px';
	btnAccept.This.style.paddingLeft = '10px';

	Modal.FunctionAfterClose = function () {
		$(this.This.This).remove();
	}
}

ItHelpCenter.Demo.TDemoPMGantt.prototype.SaveScale = function(units, count, component){
	var _this = this.TParent();
	var MAINTASK = _this.PMFill();
	MAINTASK.MAINTASK_SCALEUNITS = units;
	MAINTASK.MAINTASK_SCALECOUNTER = count;
	var success = Persistence.ProjectManagement.Methods.PMMAINTASK_UPD(MAINTASK);
	if(success){
		component.ZoomUnit = MAINTASK.MAINTASK_SCALEUNITS;
		component.ZoomCount = MAINTASK.MAINTASK_SCALECOUNTER;
		_this.GanttRefresh(component);
		// component.ZoomTo(units, count);
	}
}

ItHelpCenter.Demo.TDemoPMGantt.prototype.GetChild = function(detail, component){
	var _this = this.TParent();
	if( detail.PMDETAILTASK_CHILDList.length == 0 ){
		_this.SaveRecord(detail, component);
	}
	else if(detail.PMDETAILTASK_CHILDList.length > 0){
		_this.SaveRecord(detail, component);
		_this.GetChild(detail.PMDETAILTASK_CHILDList[0], component);
		for(var i = 1 ; i < detail.PMDETAILTASK_CHILDList.length ; i++){
			_this.GetChild(detail.PMDETAILTASK_CHILDList[i], component);
		}
	}
}

ItHelpCenter.Demo.TDemoPMGantt.prototype.SaveRecord = function (detail, component){
	var _this = this.TParent();
	var GanttElement = new Componet.TElements.TGanttElement();
	var res = ''; 
	var suc = new Array();
	for(var x = 0 ; x < detail.PMRESOURCESALLOCATIONList.length; x++){
		if(x == detail.PMRESOURCESALLOCATIONList.length-1){
			res += detail.PMRESOURCESALLOCATIONList[x].PMRESOURCES.RESOURCES_NAME;
		}else{
			res += detail.PMRESOURCESALLOCATIONList[x].PMRESOURCES.RESOURCES_NAME + ', ';
		}
	}
	for(var x = 0 ; x < detail.PMRELATION_SUCCESSORSList.length ; x++){
		if(detail.PMRELATION_SUCCESSORSList.length > 1){
			var sucTemp = {
				connectTo: detail.PMRELATION_SUCCESSORSList[x].IDPMDETAILTASK,
				connectorType: detail.PMRELATION_SUCCESSORSList[x].PMTYPERELATION.TYPERELATION_NAME,
				stroke: Source.Sequencial.Methods.GetColor()
			}
		}
		else{
			var sucTemp = {
				connectTo: detail.PMRELATION_SUCCESSORSList[x].IDPMDETAILTASK,
				connectorType: detail.PMRELATION_SUCCESSORSList[x].PMTYPERELATION.TYPERELATION_NAME
			}
		}
		suc.push(sucTemp);
	}
	GanttElement.Id = detail.IDPMDETAILTASK;
	GanttElement.Index = detail.DETAILTASK_INDEX;
	GanttElement.Name = detail.DETAILTASK_NAME;
	GanttElement.Parent= detail.IDPMDETAILTASK_PARENT;
	GanttElement.Duration = detail.DETAILTASK_DURATION;
	GanttElement.Resources = res;
	GanttElement.ProgressValue = detail.DETAILTASK_PERCENTCOMPLETE;
	GanttElement.ActualStart = detail.DETAILTASK_STARTDATE;
	GanttElement.ActualEnd = detail.DETAILTASK_ENDDATE;
	GanttElement.Connector = suc;
	GanttElement.BaseLineStart = detail.DETAILTASK_STARTDATETEMP;
	GanttElement.BaseLineEnd = detail.DETAILTASK_ENDDATETEMP;
	component.TGanttDataAdd(GanttElement);
}

ItHelpCenter.Demo.TDemoPMGantt.prototype.Insert = function(PMDETAILTASK, component) {
	var _this = this.TParent();
	var MAINTASK =_this.PMFill();
	var index = 0;
	var idParent = 0;
	if(PMDETAILTASK != null){
		index = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(MAINTASK.PMDETAILTASKList, PMDETAILTASK.IDPMDETAILTASK);
		idParent = MAINTASK.PMDETAILTASKList[index].IDPMDETAILTASK_PARENT;
	}
	else{
		index = MAINTASK.PMDETAILTASKList.length;
	}
	_this.FormInsert(index, idParent, component);
}

ItHelpCenter.Demo.TDemoPMGantt.prototype.FormInsert = function(index, idParent, component) {
	var _this = this.TParent();
	var Modal = new TVclModal(document.body, null, "");
	Modal.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines5"));
	function MediaQuery(x) {
		if (x.matches) { // menos de 780px
			Modal.This.This.style.paddingRight = '0%';
			Modal.This.This.style.paddingLeft = '0%';
		}
		else {// mayor de 780px
			Modal.This.This.style.paddingRight = '20%';
			Modal.This.This.style.paddingLeft = '20%';
		}
	}
	var x = window.matchMedia("(max-width: 700px)");
	MediaQuery(x);
	x.addListener(MediaQuery);

	var MAINTASK = _this.PMFill();
	var indexParent = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(MAINTASK.PMDETAILTASKList, idParent);
	var PMHoliday = MAINTASK.PMHOLIDAYList;

	var Container = new TVclStackPanel(Modal.Body.This, 'PMGantt_Form', 1, [[12], [12], [12], [12], [12]]);
	
	var ContTextName = new TVclStackPanel(Container.Column[0].This, "", 2, [[4, 8], [2, 10], [2, 10], [2, 10], [2, 8]]);
	ContTextName.Row.This.style.marginBottom = '10px';
	var lblName = new TVcllabel(ContTextName.Column[0].This, "", TlabelType.H0);
	lblName.Text =  UsrCfg.Traslate.GetLangText(_this.Mythis, "lblName.Text");
	lblName.VCLType = TVCLType.BS;
	var txtName = new TVclTextBox(ContTextName.Column[1].This, "");
	txtName.Text = "";
	txtName.VCLType = TVCLType.BS;

	var Container_1 = new TVclStackPanel(Container.Column[0].This, "", 2, [[12, 12], [6, 6], [6, 6], [6, 6], [6, 6]]);
	Container_1.Row.This.style.marginBottom = '10px';

	var ContTextDuration = new TVclStackPanel(Container_1.Column[0].This, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	ContTextDuration.Row.This.style.marginBottom = '5px';
	var lblDuration = new TVcllabel(ContTextDuration.Column[0].This, "", TlabelType.H0);
	lblDuration.Text =  UsrCfg.Traslate.GetLangText(_this.Mythis, "lblDuration.Text");
	lblDuration.VCLType = TVCLType.BS;
	var txtDuration = new TVclTextBox(ContTextDuration.Column[1].This, "");
	txtDuration.Text = "1 " + UsrCfg.Traslate.GetLangText(_this.Mythis, "DURATION1");
	txtDuration.VCLType = TVCLType.BS;
	txtDuration.This.onchange = function(){
		var startDate = new Date(txtStartDate.Text);
		if(_this.isInteger(parseInt(txtDuration.Text.trim()))){
			if(parseInt(txtDuration.Text.trim()) > 1){
				txtDuration.Text = parseInt(txtDuration.Text.trim()) + ' ' + UsrCfg.Traslate.GetLangText(_this.Mythis, "DURATION2");
			}else{
				txtDuration.Text = parseInt(txtDuration.Text.trim()) + ' ' + UsrCfg.Traslate.GetLangText(_this.Mythis, "DURATION1");
			}
			if(ListBox_Estimated.ListBoxItems[0].Checked){
				txtDuration.Text += '?';
			}
			if(_this.isInteger(startDate.getDate())){
				if(parseInt(txtDuration.Text.trim()) > 0){
					var startDateTemp = new Date(startDate.getTime() + (1000*60*60*12));
					var dateTemp = _this.CalculateEndDate(MAINTASK, PMHoliday, startDateTemp, parseInt(txtDuration.Text.trim()));
					txtEndDate.Text = _this.ToDateTimeMeridian(new Date(dateTemp));
				}else{
					txtEndDate.Text = txtStartDate.Text;
				}
			}
		}
		else{
			txtDuration.Text = "1 " + UsrCfg.Traslate.GetLangText(_this.Mythis, "DURATION1");
		}
	}

	var ContEstimated = new TVclStackPanel(Container_1.Column[1].This, "", 1, [[12], [12], [12], [12], [12]]);
	var ListBox_Estimated = new TVclListBox(ContEstimated.Column[0].This, "");
	ListBox_Estimated.EnabledCheckBox = true;
	var ListBoxItem_Estimated = new TVclListBoxItem();
	ListBoxItem_Estimated.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ListBoxItem_Estimated.Text");
	ListBoxItem_Estimated.Index = 0;
	ListBox_Estimated.AddListBoxItem(ListBoxItem_Estimated);
	ListBox_Estimated.ListBoxItems[0].This.Row.This.style.marginRight = '8px'
	ListBox_Estimated.OnCheckedListBoxItemChange = function (EventArgs, Object) {
		if(EventArgs.Checked){
			if(txtDuration.Text.trim() != ''){
				txtDuration.Text += '?';
			}
		}
		else{
			if(txtDuration.Text.trim() != ''){
				if(parseInt(txtDuration.Text.trim()) > 1){
					txtDuration.Text = parseInt(txtDuration.Text.trim()) + ' ' + UsrCfg.Traslate.GetLangText(_this.Mythis, "DURATION2");
				}else{
					txtDuration.Text = parseInt(txtDuration.Text.trim()) + ' ' + UsrCfg.Traslate.GetLangText(_this.Mythis, "DURATION1");
				}
			}
		}
	}

	var Container_2 = new TVclStackPanel(Container.Column[0].This, "", 2, [[12, 12], [6, 6], [6, 6], [6, 6], [6, 6]]);
	Container_2.Row.This.style.marginBottom = '10px';

	var ContPercentage = new TVclStackPanel(Container_2.Column[0].This, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	ContPercentage.Row.This.style.marginBottom = '5px';
	var lblPercentage = new TVcllabel(ContPercentage.Column[0].This, "", TlabelType.H0);
	lblPercentage.Text =  UsrCfg.Traslate.GetLangText(_this.Mythis, "lblPercentage.Text");
	lblPercentage.VCLType = TVCLType.BS;
	var txtPercentage = new TVclTextBox(ContPercentage.Column[1].This, "");
	txtPercentage.Text = "0";
	txtPercentage.VCLType = TVCLType.BS;
	txtPercentage.This.setAttribute("type", "number");
    txtPercentage.This.setAttribute("min", "0");

	var ContTextPriority = new TVclStackPanel(Container_2.Column[1].This, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	ContTextPriority.Row.This.style.marginBottom = '5px';
	var lblPriority = new TVcllabel(ContTextPriority.Column[0].This, "", TlabelType.H0);
	lblPriority.Text =  UsrCfg.Traslate.GetLangText(_this.Mythis, "lblPriority.Text");
	lblPriority.VCLType = TVCLType.BS;
	var txtPriority = new TVclTextBox(ContTextPriority.Column[1].This, "");
	txtPriority.Text = "0";
	txtPriority.VCLType = TVCLType.BS;
	txtPriority.This.setAttribute("type", "number");
    txtPriority.This.setAttribute("min", "0");

	var Container_3 = new TVclStackPanel(Container.Column[0].This, "", 2, [[12, 12], [6, 6], [6, 6], [6, 6], [6, 6]]);
	Container_3.Row.This.style.marginBottom = '10px';

	var ContStartDate = new TVclStackPanel(Container_3.Column[0].This, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	ContStartDate.Row.This.style.marginBottom = '5px';
	var lblStartDate = new TVcllabel(ContStartDate.Column[0].This, "", TlabelType.H0);
	lblStartDate.This.style.paddingTop = '5px';
	lblStartDate.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblStartDate.Text");
	lblStartDate.VCLType = TVCLType.BS;
	var txtStartDate = new TVclTextBox(ContStartDate.Column[1].This, "txtStartDatePMGantt");
	txtStartDate.Text = "";
	txtStartDate.VCLType = TVCLType.BS;
	txtStartDate.This.onchange = function(){
		var startDate = new Date(txtStartDate.Text);
		if(_this.isInteger(parseInt(txtDuration.Text.trim()))){
			if(_this.isInteger(startDate.getDate())){
				if(parseInt(txtDuration.Text.trim()) > 0){
					var startDateTemp = new Date(startDate.getTime() + (1000*60*60*12));
					var dateTemp = _this.CalculateEndDate(MAINTASK, PMHoliday, startDateTemp, parseInt(txtDuration.Text.trim()));
					txtEndDate.Text = _this.ToDateTimeMeridian(new Date(dateTemp));
				}else{
					txtEndDate.Text = txtStartDate.Text;
				}
			}
		}
	}
	
	var ContEndDate = new TVclStackPanel(Container_3.Column[1].This, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	ContEndDate.Row.This.style.marginBottom = '5px';
	var lblEndDate = new TVcllabel(ContEndDate.Column[0].This, "", TlabelType.H0);
	lblEndDate.This.style.paddingTop = '5px';
	lblEndDate.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblEndDate.Text");
	lblEndDate.VCLType = TVCLType.BS;
	var txtEndDate = new TVclTextBox(ContEndDate.Column[1].This, "txtFinalDatePMGantt");
	txtEndDate.Text = "";
	txtEndDate.VCLType = TVCLType.BS;
	txtEndDate.This.onchange = function(){
		var startDate = new Date(txtStartDate.Text);
		var endDate = new Date(txtEndDate.Text);
		if(_this.isInteger(startDate.getDate()) && _this.isInteger(endDate.getDate())){
			txtDuration.Text = _this.CalculateDuration(MAINTASK, PMHoliday, startDate, endDate);
			if(parseInt(txtDuration.Text.trim()) > 1){
				txtDuration.Text = parseInt(txtDuration.Text.trim()) + ' ' + UsrCfg.Traslate.GetLangText(_this.Mythis, "DURATION2");
			}else{
				txtDuration.Text = parseInt(txtDuration.Text.trim()) + ' ' + UsrCfg.Traslate.GetLangText(_this.Mythis, "DURATION1");
			}
			if(ListBox_Estimated.ListBoxItems[0].Checked){
				txtDuration.Text += '?';
			}
		}
	}

	if(idParent > 0){
		if(MAINTASK.MAINTASK_WEEKENDHOLIDAY){
			$(txtStartDate.This).datetimepicker({
				dateFormat:"yy/mm/dd", 
				timeFormat:'hh:mm tt', 
				hour: 7, 
				minute:30,
				minDateTime: MAINTASK.PMDETAILTASKList[indexParent].DETAILTASK_STARTDATE,
				beforeShowDay: noWeekendsAndHolidays
			});
			$(txtEndDate.This).datetimepicker({
				dateFormat:"yy/mm/dd",
				timeFormat:'hh:mm tt',
				hour: 19,
				minute:30,
				minDateTime: MAINTASK.PMDETAILTASKList[indexParent].DETAILTASK_STARTDATE,
				beforeShowDay: noWeekendsAndHolidays
			});
		}
		else if(MAINTASK.MAINTASK_SUNDAYHOLIDAY){
			$(txtStartDate.This).datetimepicker({
				dateFormat:"yy/mm/dd", 
				timeFormat:'hh:mm tt', 
				hour: 7, 
				minute:30,
				minDateTime: MAINTASK.PMDETAILTASKList[indexParent].DETAILTASK_STARTDATE,
				beforeShowDay: noSundayAndHolidays
			});
			$(txtEndDate.This).datetimepicker({
				dateFormat:"yy/mm/dd",
				timeFormat:'hh:mm tt',
				hour: 19,
				minute:30,
				minDateTime: MAINTASK.PMDETAILTASKList[indexParent].DETAILTASK_STARTDATE,
				beforeShowDay: noSundayAndHolidays
			});
		}
		else{
			$(txtStartDate.This).datetimepicker({
				dateFormat:"yy/mm/dd", 
				timeFormat:'hh:mm tt', 
				hour: 7, 
				minute:30,
				minDateTime: MAINTASK.PMDETAILTASKList[indexParent].DETAILTASK_STARTDATE,
				beforeShowDay: holidayDates
			});
			$(txtEndDate.This).datetimepicker({
				dateFormat:"yy/mm/dd",
				timeFormat:'hh:mm tt',
				hour: 19,
				minute:30,
				minDateTime: MAINTASK.PMDETAILTASKList[indexParent].DETAILTASK_STARTDATE,
				beforeShowDay: holidayDates
			});
		}
	}
	else{
		if(MAINTASK.MAINTASK_WEEKENDHOLIDAY){
			$(txtStartDate.This).datetimepicker({
				dateFormat:"yy/mm/dd", 
				timeFormat:'hh:mm tt', 
				hour: 7, 
				minute:30,
				beforeShowDay: noWeekendsAndHolidays
			});
			$(txtEndDate.This).datetimepicker({
				dateFormat:"yy/mm/dd",
				timeFormat:'hh:mm tt',
				hour: 19,
				minute:30,
				beforeShowDay: noWeekendsAndHolidays
			});
		}
		else if(MAINTASK.MAINTASK_SUNDAYHOLIDAY){
			$(txtStartDate.This).datetimepicker({
				dateFormat:"yy/mm/dd", 
				timeFormat:'hh:mm tt', 
				hour: 7, 
				minute:30,
				beforeShowDay: noSundayAndHolidays
			});
			$(txtEndDate.This).datetimepicker({
				dateFormat:"yy/mm/dd",
				timeFormat:'hh:mm tt',
				hour: 19,
				minute:30,
				beforeShowDay: noSundayAndHolidays
			});
		}
		else{
			$(txtStartDate.This).datetimepicker({
				dateFormat:"yy/mm/dd", 
				timeFormat:'hh:mm tt', 
				hour: 7, 
				minute:30,
				beforeShowDay: holidayDates
			});
			$(txtEndDate.This).datetimepicker({
				dateFormat:"yy/mm/dd",
				timeFormat:'hh:mm tt',
				hour: 19,
				minute:30,
				beforeShowDay: holidayDates
			});
		}
	}

	function holidayDates(date) {
		for (var i = 0; i < PMHoliday.length; i++) {
			if (date.getMonth() == new Date(PMHoliday[i].HOLIDAY_DATE).getMonth() && date.getDate() == new Date(PMHoliday[i].HOLIDAY_DATE).getDate()) {
				return [false, '']; 
			}
		}
		return [true, '']; 
	}

	function noWeekendsAndHolidays(date) { 
		var noWeekend = $.datepicker.noWeekends(date); 
		if (noWeekend[0]) {
			return holidayDates(date);
		}
		else { 
			return noWeekend; 
		}
	} 
	
	function noSundayAndHolidays(date){
		var day = date.getDay();
		if(day != 0){
			return holidayDates(date);
		}
		else{
			return [false, ''];
		}
	}

	var ContNote = new TVclStackPanel(Container.Column[0].This, "", 2, [[4, 8], [2, 10], [2, 10], [2, 10], [2, 8]]);
	ContNote.Row.This.style.marginBottom = '10px';
	var lblNote = new TVcllabel(ContNote.Column[0].This, "", TlabelType.H0);
	lblNote.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblNote.Text");
	lblNote.VCLType = TVCLType.BS;
	var memoNote = new TVclMemo(ContNote.Column[1].This, "txtNotePMGantt");
	memoNote.VCLType = TVCLType.BS;
	memoNote.This.removeAttribute("cols");
	memoNote.This.removeAttribute("rows");
	memoNote.This.classList.add("form-control");
	memoNote.Text = '';
	
	var ContBtnSave = new TVclStackPanel(Container.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
	ContBtnSave.Row.This.style.marginBottom = '5px';
	var btnSave= new TVclButton(ContBtnSave.Column[0].This, "");
	btnSave.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnSave.Text");
	btnSave.VCLType = TVCLType.BS;
	btnSave.This.style.width = '98px';
	btnSave.Src = "image/16/Accept.png";
	btnSave.onClick = function () {
		if(txtName.Text.trim() != ''){
			if(_this.isInteger(parseInt(txtPercentage.Text))){
				if(parseInt(txtPercentage.Text) <= 100){
					if(_this.isInteger(parseInt(txtPriority.Text))){
						if(txtStartDate.Text.trim() != '' && txtEndDate.Text.trim() != ''){
							_this.SaveTask( index, idParent, txtName.Text.trim(), txtDuration.Text.trim(), ListBox_Estimated.ListBoxItems[0].Checked, txtPercentage.Text.trim(), 
								txtPriority.Text.trim(), txtStartDate.Text.trim(), txtEndDate.Text.trim(), memoNote.Text);
							_this.GanttRefresh(component);
							Modal.CloseModal();
							$(Modal.This.This).remove();
							$('#ui-datepicker-div').remove()
						}
						else{
							alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines13"));
						}
					}
					else{
						alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines14"));
					}
				}
				else{
					alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines15"));
				}
			}
			else{
				alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines16"));
			}
		}
		else{
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines17"));
		}
	}
	btnSave.This.style.float = 'right';
	btnSave.This.style.padding = '0px';
	btnSave.This.style.paddingLeft = '10px';

	Modal.ShowModal();

	Modal.FunctionAfterClose = function () {
		$('#ui-datepicker-div').remove()
		$(this.This.This).remove();
	}
}

ItHelpCenter.Demo.TDemoPMGantt.prototype.SaveTask = function(index, idParent, name, duration, estimated, percentage, priority, startDate, endDate, note){
	var _this = this.TParent();
	var MAINTASK =_this.PMFill();
	var PMHoliday = MAINTASK.PMHOLIDAYList;
	var PMDETAILTASK = new Persistence.ProjectManagement.Properties.TPMDETAILTASK();
	var INDEX_TEMP = 0;
	if(MAINTASK.PMDETAILTASKList.length != 0){
		if(index == MAINTASK.PMDETAILTASKList.length){
			INDEX_TEMP = (MAINTASK.PMDETAILTASKList[index-1].DETAILTASK_INDEX) + 1;
		}else{
			INDEX_TEMP = MAINTASK.PMDETAILTASKList[index].DETAILTASK_INDEX;
		}
	}
	else{
		INDEX_TEMP = 1;
	}
	var StartDate = new Date(startDate);
	var EndDate = new Date(endDate);

	PMDETAILTASK.IDPMMAINTASK = MAINTASK.IDPMMAINTASK ;
	PMDETAILTASK.DETAILTASK_NAME = name;
	PMDETAILTASK.DETAILTASK_DURATION = duration;
	PMDETAILTASK.DETAILTASK_STARTDATE = StartDate;
	PMDETAILTASK.DETAILTASK_ENDDATE = EndDate;
	PMDETAILTASK.DETAILTASK_INDEX = INDEX_TEMP;
	PMDETAILTASK.DETAILTASK_PERCENTCOMPLETE = percentage;
	PMDETAILTASK.DETAILTASK_PRIORITY = priority;
	PMDETAILTASK.DETAILTASK_ESTIMATED = estimated;
	PMDETAILTASK.DETAILTASK_NOTE = note;
	PMDETAILTASK.IDPMDETAILTASK_PARENT = idParent;
	PMDETAILTASK.DETAILTASK_STARTDATETEMP = StartDate;
	PMDETAILTASK.DETAILTASK_ENDDATETEMP = EndDate;
	PMDETAILTASK.DETAILTASK_DURATIONTEMP = duration;
	var success = Persistence.ProjectManagement.Methods.PMDETAILTASK_ADD(PMDETAILTASK);
	if(success){
		for(var i = index ; i < MAINTASK.PMDETAILTASKList.length ; i++){
			INDEX_TEMP++;
			MAINTASK.PMDETAILTASKList[i].DETAILTASK_INDEX = INDEX_TEMP;
			Persistence.ProjectManagement.Methods.PMDETAILTASK_UPD(MAINTASK.PMDETAILTASKList[i]);
		}
		MAINTASK =_this.PMFill();
		if(idParent != 0){
			var indexParent = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(MAINTASK.PMDETAILTASKList, idParent);
			_this.ParentUpdate(MAINTASK, PMHoliday, MAINTASK.PMDETAILTASKList[indexParent]);
		}
	}
}

ItHelpCenter.Demo.TDemoPMGantt.prototype.Update = function(PMDETAILTASK, component) {
	var _this = this.TParent();
	var Modal = new TVclModal(document.body, null, "");
	Modal.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines6"));
	function MediaQuery(x) {
		if (x.matches) { // menos de 780px
			Modal.This.This.style.paddingRight = '0%';
			Modal.This.This.style.paddingLeft = '0%';
		}
		else {// mayor de 780px
			Modal.This.This.style.paddingRight = '20%';
			Modal.This.This.style.paddingLeft = '20%';
		}
	}
	var x = window.matchMedia("(max-width: 700px)");
	MediaQuery(x);
	x.addListener(MediaQuery);

	var MAINTASK = _this.PMFill();
	var indexParent = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(MAINTASK.PMDETAILTASKList, parseInt(PMDETAILTASK.IDPMDETAILTASK_PARENT));
	var PMHoliday = MAINTASK.PMHOLIDAYList;

	var Container = new TVclStackPanel(Modal.Body.This, 'GanttUpdate_Form', 1, [[12], [12], [12], [12], [12]]);
	
	var ContTextName = new TVclStackPanel(Container.Column[0].This, "", 2, [[4, 8], [2, 10], [2, 10], [2, 10], [2, 8]]);
	ContTextName.Row.This.style.marginBottom = '10px';
	var lblName = new TVcllabel(ContTextName.Column[0].This, "", TlabelType.H0);
	lblName.Text =  UsrCfg.Traslate.GetLangText(_this.Mythis, "lblName.Text");
	lblName.VCLType = TVCLType.BS;
	var txtName = new TVclTextBox(ContTextName.Column[1].This, "");
	txtName.Text = PMDETAILTASK.DETAILTASK_NAME;
	txtName.VCLType = TVCLType.BS;

	var Container_1 = new TVclStackPanel(Container.Column[0].This, "", 2, [[12, 12], [6, 6], [6, 6], [6, 6], [6, 6]]);
	Container_1.Row.This.style.marginBottom = '10px';

	var ContTextDuration = new TVclStackPanel(Container_1.Column[0].This, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	ContTextDuration.Row.This.style.marginBottom = '5px';
	var lblDuration = new TVcllabel(ContTextDuration.Column[0].This, "", TlabelType.H0);
	lblDuration.Text =  UsrCfg.Traslate.GetLangText(_this.Mythis, "lblDuration.Text");
	lblDuration.VCLType = TVCLType.BS;
	var txtDuration = new TVclTextBox(ContTextDuration.Column[1].This, "");
	txtDuration.Text = PMDETAILTASK.DETAILTASK_DURATION;
	txtDuration.VCLType = TVCLType.BS;
	txtDuration.This.onchange = function(){
		var startDate = new Date(txtStartDate.Text);
		if(_this.isInteger(parseInt(txtDuration.Text.trim()))){
			if(parseInt(txtDuration.Text.trim()) > 1){
				txtDuration.Text = parseInt(txtDuration.Text.trim()) + ' ' + UsrCfg.Traslate.GetLangText(_this.Mythis, "DURATION2");
			}else{
				txtDuration.Text = parseInt(txtDuration.Text.trim()) + ' ' + UsrCfg.Traslate.GetLangText(_this.Mythis, "DURATION1");
			}
			if(ListBox_Estimated.ListBoxItems[0].Checked){
				txtDuration.Text += '?';
			}
			if(_this.isInteger(startDate.getDate())){
				if(parseInt(txtDuration.Text.trim()) > 0){
					var startDateTemp = new Date(startDate.getTime() + (1000*60*60*12));
					var dateTemp = _this.CalculateEndDate(MAINTASK, PMHoliday, startDateTemp, parseInt(txtDuration.Text.trim()));
					txtEndDate.Text = _this.ToDateTimeMeridian(new Date(dateTemp));
				}else{
					txtEndDate.Text = txtStartDate.Text;
				}
			}
		}
		else{
			txtDuration.Text = "1 " + UsrCfg.Traslate.GetLangText(_this.Mythis, "DURATION1");
		}
	}

	var ContEstimated = new TVclStackPanel(Container_1.Column[1].This, "", 1, [[12], [12], [12], [12], [12]]);
	var ListBox_Estimated = new TVclListBox(ContEstimated.Column[0].This, "");
	ListBox_Estimated.EnabledCheckBox = true;
	var ListBoxItem_Estimated = new TVclListBoxItem();
	ListBoxItem_Estimated.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ListBoxItem_Estimated.Text");
	ListBoxItem_Estimated.Index = 0;
	ListBox_Estimated.AddListBoxItem(ListBoxItem_Estimated);
	ListBox_Estimated.ListBoxItems[0].This.Row.This.style.marginRight = '8px'
	ListBox_Estimated.OnCheckedListBoxItemChange = function (EventArgs, Object) {
		if(EventArgs.Checked){
			if(txtDuration.Text.trim() != ''){
				txtDuration.Text += '?';
			}
		}
		else{
			if(txtDuration.Text.trim() != ''){
				if(parseInt(txtDuration.Text.trim()) > 1){
					txtDuration.Text = parseInt(txtDuration.Text.trim()) + ' ' + UsrCfg.Traslate.GetLangText(_this.Mythis, "DURATION2");
				}else{
					txtDuration.Text = parseInt(txtDuration.Text.trim()) + ' ' + UsrCfg.Traslate.GetLangText(_this.Mythis, "DURATION1");
				}
			}
		}
	}
	ListBox_Estimated.ListBoxItems[0].Checked = PMDETAILTASK.DETAILTASK_ESTIMATED;

	var Container_2 = new TVclStackPanel(Container.Column[0].This, "", 2, [[12, 12], [6, 6], [6, 6], [6, 6], [6, 6]]);
	Container_2.Row.This.style.marginBottom = '10px';

	var ContPercentage = new TVclStackPanel(Container_2.Column[0].This, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	ContPercentage.Row.This.style.marginBottom = '5px';
	var lblPercentage = new TVcllabel(ContPercentage.Column[0].This, "", TlabelType.H0);
	lblPercentage.Text =  UsrCfg.Traslate.GetLangText(_this.Mythis, "lblPercentage.Text");
	lblPercentage.VCLType = TVCLType.BS;
	var txtPercentage = new TVclTextBox(ContPercentage.Column[1].This, "");
	txtPercentage.Text = PMDETAILTASK.DETAILTASK_PERCENTCOMPLETE;
	txtPercentage.VCLType = TVCLType.BS;
	txtPercentage.This.setAttribute("type", "number");
    txtPercentage.This.setAttribute("min", "0");

	var ContTextPriority = new TVclStackPanel(Container_2.Column[1].This, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	ContTextPriority.Row.This.style.marginBottom = '5px';
	var lblPriority = new TVcllabel(ContTextPriority.Column[0].This, "", TlabelType.H0);
	lblPriority.Text =  UsrCfg.Traslate.GetLangText(_this.Mythis, "lblPriority.Text");
	lblPriority.VCLType = TVCLType.BS;
	var txtPriority = new TVclTextBox(ContTextPriority.Column[1].This, "");
	txtPriority.Text = PMDETAILTASK.DETAILTASK_PRIORITY
	txtPriority.VCLType = TVCLType.BS;
	txtPriority.This.setAttribute("type", "number");
    txtPriority.This.setAttribute("min", "0");

	var Container_3 = new TVclStackPanel(Container.Column[0].This, "", 2, [[12, 12], [6, 6], [6, 6], [6, 6], [6, 6]]);
	Container_3.Row.This.style.marginBottom = '10px';

	var ContStartDate = new TVclStackPanel(Container_3.Column[0].This, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	ContStartDate.Row.This.style.marginBottom = '5px';
	var lblStartDate = new TVcllabel(ContStartDate.Column[0].This, "", TlabelType.H0);
	lblStartDate.This.style.paddingTop = '5px';
	lblStartDate.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblStartDate.Text");
	lblStartDate.VCLType = TVCLType.BS;
	var txtStartDate = new TVclTextBox(ContStartDate.Column[1].This, "txtStartDatePMGantt");
	txtStartDate.Text = _this.ToDateTimeMeridian(new Date(PMDETAILTASK.DETAILTASK_STARTDATE));
	txtStartDate.VCLType = TVCLType.BS;	
	txtStartDate.This.onchange = function(){
		var startDate = new Date(txtStartDate.Text);
		if(_this.isInteger(parseInt(txtDuration.Text.trim()))){
			if(_this.isInteger(startDate.getDate())){
				if(parseInt(txtDuration.Text.trim()) > 0){
					var startDateTemp = new Date(startDate.getTime() + (1000*60*60*12));
					var dateTemp = _this.CalculateEndDate(MAINTASK, PMHoliday, startDateTemp, parseInt(txtDuration.Text.trim()));
					txtEndDate.Text = _this.ToDateTimeMeridian(new Date(dateTemp));
				}else{
					txtEndDate.Text = txtStartDate.Text;
				}
			}
		}
	}
	
	var ContEndDate = new TVclStackPanel(Container_3.Column[1].This, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	ContEndDate.Row.This.style.marginBottom = '5px';
	var lblEndDate = new TVcllabel(ContEndDate.Column[0].This, "", TlabelType.H0);
	lblEndDate.This.style.paddingTop = '5px';
	lblEndDate.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblEndDate.Text");
	lblEndDate.VCLType = TVCLType.BS;
	var txtEndDate = new TVclTextBox(ContEndDate.Column[1].This, "txtFinalDatePMGantt");
	txtEndDate.Text = _this.ToDateTimeMeridian(new Date(PMDETAILTASK.DETAILTASK_ENDDATE));
	txtEndDate.VCLType = TVCLType.BS;
	txtEndDate.This.onchange = function(){
		var startDate = new Date(txtStartDate.Text);
		var endDate = new Date(txtEndDate.Text);
		if(_this.isInteger(startDate.getDate()) && _this.isInteger(endDate.getDate())){
			txtDuration.Text = _this.CalculateDuration(MAINTASK, PMHoliday, startDate, endDate);
			if(parseInt(txtDuration.Text.trim()) > 1){
				txtDuration.Text = parseInt(txtDuration.Text.trim()) + ' ' + UsrCfg.Traslate.GetLangText(_this.Mythis, "DURATION2");
			}else{
				txtDuration.Text = parseInt(txtDuration.Text.trim()) +' ' + UsrCfg.Traslate.GetLangText(_this.Mythis, "DURATION1");
			}
			if(ListBox_Estimated.ListBoxItems[0].Checked){
				txtDuration.Text += '?';
			}
		}
	}

	if(parseInt(PMDETAILTASK.IDPMDETAILTASK_PARENT) > 0){
		if(MAINTASK.MAINTASK_WEEKENDHOLIDAY){
			$(txtStartDate.This).datetimepicker({
				dateFormat:"yy/mm/dd", 
				timeFormat:'hh:mm tt', 
				hour: 7, 
				minute:30,
				minDateTime: MAINTASK.PMDETAILTASKList[indexParent].DETAILTASK_STARTDATE,
				beforeShowDay: noWeekendsAndHolidays
			});
			$(txtEndDate.This).datetimepicker({
				dateFormat:"yy/mm/dd",
				timeFormat:'hh:mm tt',
				hour: 19,
				minute:30,
				minDateTime: MAINTASK.PMDETAILTASKList[indexParent].DETAILTASK_STARTDATE,
				beforeShowDay: noWeekendsAndHolidays
			});
		}
		else if(MAINTASK.MAINTASK_SUNDAYHOLIDAY){
			$(txtStartDate.This).datetimepicker({
				dateFormat:"yy/mm/dd", 
				timeFormat:'hh:mm tt', 
				hour: 7, 
				minute:30,
				minDateTime: MAINTASK.PMDETAILTASKList[indexParent].DETAILTASK_STARTDATE,
				beforeShowDay: noSundayAndHolidays
			});
			$(txtEndDate.This).datetimepicker({
				dateFormat:"yy/mm/dd",
				timeFormat:'hh:mm tt',
				hour: 19,
				minute:30,
				minDateTime: MAINTASK.PMDETAILTASKList[indexParent].DETAILTASK_STARTDATE,
				beforeShowDay: noSundayAndHolidays
			});
		}
		else{
			$(txtStartDate.This).datetimepicker({
				dateFormat:"yy/mm/dd", 
				timeFormat:'hh:mm tt', 
				hour: 7, 
				minute:30,
				minDateTime: MAINTASK.PMDETAILTASKList[indexParent].DETAILTASK_STARTDATE,
				beforeShowDay: holidayDates
			});
			$(txtEndDate.This).datetimepicker({
				dateFormat:"yy/mm/dd",
				timeFormat:'hh:mm tt',
				hour: 19,
				minute:30,
				minDateTime: MAINTASK.PMDETAILTASKList[indexParent].DETAILTASK_STARTDATE,
				beforeShowDay: holidayDates
			});
		}
	}
	else{
		if(MAINTASK.MAINTASK_WEEKENDHOLIDAY){
			$(txtStartDate.This).datetimepicker({
				dateFormat:"yy/mm/dd", 
				timeFormat:'hh:mm tt', 
				hour: 7, 
				minute:30,
				beforeShowDay: noWeekendsAndHolidays
			});
			$(txtEndDate.This).datetimepicker({
				dateFormat:"yy/mm/dd",
				timeFormat:'hh:mm tt',
				hour: 19,
				minute:30,
				beforeShowDay: noWeekendsAndHolidays
			});
		}
		else if(MAINTASK.MAINTASK_SUNDAYHOLIDAY){
			$(txtStartDate.This).datetimepicker({
				dateFormat:"yy/mm/dd", 
				timeFormat:'hh:mm tt', 
				hour: 7, 
				minute:30,
				beforeShowDay: noSundayAndHolidays
			});
			$(txtEndDate.This).datetimepicker({
				dateFormat:"yy/mm/dd",
				timeFormat:'hh:mm tt',
				hour: 19,
				minute:30,
				beforeShowDay: noSundayAndHolidays
			});
		}
		else{
			$(txtStartDate.This).datetimepicker({
				dateFormat:"yy/mm/dd", 
				timeFormat:'hh:mm tt', 
				hour: 7, 
				minute:30,
				beforeShowDay: holidayDates
			});
			$(txtEndDate.This).datetimepicker({
				dateFormat:"yy/mm/dd",
				timeFormat:'hh:mm tt',
				hour: 19,
				minute:30,
				beforeShowDay: holidayDates
			});
		}
	}

	function holidayDates(date) {
		for (var i = 0; i < PMHoliday.length; i++) {
			if (date.getMonth() == new Date(PMHoliday[i].HOLIDAY_DATE).getMonth() && date.getDate() == new Date(PMHoliday[i].HOLIDAY_DATE).getDate()) {
				return [false, '']; 
			}
		}
		return [true, '']; 
	}

	function noWeekendsAndHolidays(date) { 
		var noWeekend = $.datepicker.noWeekends(date); 
		if (noWeekend[0]) {
			return holidayDates(date);
		}
		else { 
			return noWeekend; 
		}
	} 
	
	function noSundayAndHolidays(date){
		var day = date.getDay();
		if(day != 0){
			return holidayDates(date);
		}
		else{
			return [false, ''];
		}
	}

	var ContNote = new TVclStackPanel(Container.Column[0].This, "", 2, [[4, 8], [2, 10], [2, 10], [2, 10], [2, 8]]);
	ContNote.Row.This.style.marginBottom = '10px';
	var lblNote = new TVcllabel(ContNote.Column[0].This, "", TlabelType.H0);
	lblNote.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblNote.Text");
	lblNote.VCLType = TVCLType.BS;
	var memoNote = new TVclMemo(ContNote.Column[1].This, "txtNotePMGantt");
	memoNote.VCLType = TVCLType.BS;
	memoNote.This.removeAttribute("cols");
	memoNote.This.removeAttribute("rows");
	memoNote.This.classList.add("form-control");
	memoNote.Text = PMDETAILTASK.DETAILTASK_NOTE;

	var ContBtnSave = new TVclStackPanel(Container.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
	ContBtnSave.Row.This.style.marginBottom = '5px';
	var btnSave= new TVclButton(ContBtnSave.Column[0].This, "");
	btnSave.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnSave.Text");
	btnSave.VCLType = TVCLType.BS;
	btnSave.This.style.width = '98px';
	btnSave.Src = "image/16/Accept.png";
	btnSave.onClick = function () {
		if(txtName.Text.trim() != ''){
			if(_this.isInteger(parseInt(txtPercentage.Text))){
				if(parseInt(txtPercentage.Text) <= 100){
					if(_this.isInteger(parseInt(txtPriority.Text))){
						if(txtStartDate.Text.trim() != '' && txtEndDate.Text.trim() != ''){
							PMDETAILTASK.DETAILTASK_NAME = txtName.Text.trim();
							PMDETAILTASK.DETAILTASK_DURATION = txtDuration.Text.trim();
							PMDETAILTASK.DETAILTASK_PERCENTCOMPLETE = txtPercentage.Text.trim();
							PMDETAILTASK.DETAILTASK_PRIORITY = txtPriority.Text.trim();
							PMDETAILTASK.DETAILTASK_ESTIMATED = ListBox_Estimated.ListBoxItems[0].Checked;
							PMDETAILTASK.DETAILTASK_STARTDATE = new Date(txtStartDate.Text.trim());
							PMDETAILTASK.DETAILTASK_ENDDATE = new Date(txtEndDate.Text.trim());
							PMDETAILTASK.DETAILTASK_NOTE = memoNote.Text;

							var success = Persistence.ProjectManagement.Methods.PMDETAILTASK_UPD(PMDETAILTASK);
							if(success){
								MAINTASK = _this.PMFill();
								if(PMDETAILTASK.IDPMDETAILTASK_PARENT != 0){
									var indexParent = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(MAINTASK.PMDETAILTASKList, PMDETAILTASK.IDPMDETAILTASK_PARENT);
									_this.ParentUpdate(MAINTASK, PMHoliday, MAINTASK.PMDETAILTASKList[indexParent]);
								}
								_this.GanttRefresh(component);
								Modal.CloseModal();
								$(Modal.This.This).remove();
								$('#ui-datepicker-div').remove();
							}
						}
						else{
							alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines13"));
						}
					}
					else{
						alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines14"));
					}
				}
				else{
					alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines15"));
				}
			}
			else{
				alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines16"));
			}
		}
		else{
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines17"));
		}
	}
	btnSave.This.style.float = 'right';
	btnSave.This.style.padding = '0px';
	btnSave.This.style.paddingLeft = '10px';

	Modal.ShowModal();
	Modal.FunctionAfterClose = function () {
		$('#ui-datepicker-div').remove()
		$(this.This.This).remove();
	}
}

ItHelpCenter.Demo.TDemoPMGantt.prototype.ToDateTimeMeridian = function(d) {
	var month = d.getMonth() + 1;
	var day = d.getDate();
	var year = d.getFullYear();
	var hour = d.getHours();
	var min = d.getMinutes();
	var AMPM = (hour > 11) ? "pm" : "am";
	if(hour > 12) {
		hour -= 12;
	}
	else if(hour == 0) {
		hour = "12";
	}
	var date = year + "/" + SysCfg.DateTimeMethods.Pad(month, 2) + "/" + SysCfg.DateTimeMethods.Pad(day, 2) + " " + SysCfg.DateTimeMethods.Pad(hour, 2) + ":" + SysCfg.DateTimeMethods.Pad(min, 2) + " " + AMPM;
	return date;
}

ItHelpCenter.Demo.TDemoPMGantt.prototype.Indent = function(PMDETAILTASK, component) {
	var _this = this.TParent();

	var MAINTASK =_this.PMFill();
	var indexDP = null;
	var indexDC = null;

	var PMHoliday = MAINTASK.PMHOLIDAYList;

	var DETAILTASKPARENT = null;
	if(PMDETAILTASK.IDPMDETAILTASK_PARENT == 0){
		var index = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(MAINTASK.PMDETAILTASKPARENTList, PMDETAILTASK.IDPMDETAILTASK);
		DETAILTASKPARENT = MAINTASK.PMDETAILTASKPARENTList[index-1];
		PMDETAILTASK.IDPMDETAILTASK_PARENT = DETAILTASKPARENT.IDPMDETAILTASK;
		var success = Persistence.ProjectManagement.Methods.PMDETAILTASK_UPD(PMDETAILTASK);
		if(success){
			MAINTASK =_this.PMFill();
			indexDP = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(MAINTASK.PMDETAILTASKList, DETAILTASKPARENT.IDPMDETAILTASK);
			indexDC = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(MAINTASK.PMDETAILTASKList, PMDETAILTASK.IDPMDETAILTASK);
		}
	}
	else{
		var indexParent = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(MAINTASK.PMDETAILTASKList, PMDETAILTASK.IDPMDETAILTASK_PARENT);
		var PMDETAILTASKPARENT = MAINTASK.PMDETAILTASKList[indexParent]; //Padre actual
		var index = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(PMDETAILTASKPARENT.PMDETAILTASK_CHILDList, PMDETAILTASK.IDPMDETAILTASK);
		DETAILTASKPARENT = PMDETAILTASKPARENT.PMDETAILTASK_CHILDList[index-1]; //tarea hermana que se volvera en padre

		// agregar fecha final para los padres;
		PMDETAILTASK.IDPMDETAILTASK_PARENT = DETAILTASKPARENT.IDPMDETAILTASK;
		var success = Persistence.ProjectManagement.Methods.PMDETAILTASK_UPD(PMDETAILTASK);
		if(success){
			MAINTASK =_this.PMFill();
			indexDP = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(MAINTASK.PMDETAILTASKList, DETAILTASKPARENT.IDPMDETAILTASK); //nueva tarea padre
			indexDC = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(MAINTASK.PMDETAILTASKList, PMDETAILTASK.IDPMDETAILTASK); //tarea actual
		}
	}

	if(MAINTASK.PMDETAILTASKList[indexDP].PMDETAILTASK_CHILDList.length == 1){
		var durationParent = parseInt(MAINTASK.PMDETAILTASKList[indexDP].DETAILTASK_DURATION);
		var durationChild = parseInt(MAINTASK.PMDETAILTASKList[indexDC].DETAILTASK_DURATION);
		var startDate_Temp = MAINTASK.PMDETAILTASKList[indexDC].DETAILTASK_STARTDATE;
		if(durationParent >= durationChild){
			MAINTASK.PMDETAILTASKList[indexDC].DETAILTASK_STARTDATE = MAINTASK.PMDETAILTASKList[indexDP].DETAILTASK_STARTDATE;
			MAINTASK.PMDETAILTASKList[indexDC].DETAILTASK_ENDDATE = MAINTASK.PMDETAILTASKList[indexDP].DETAILTASK_ENDDATE;
			MAINTASK.PMDETAILTASKList[indexDC].DETAILTASK_DURATION = MAINTASK.PMDETAILTASKList[indexDP].DETAILTASK_DURATION;
			var success = Persistence.ProjectManagement.Methods.PMDETAILTASK_UPD(MAINTASK.PMDETAILTASKList[indexDC]);
			if(success){
				var durationDif = _this.CalculateDuration(MAINTASK, PMHoliday, MAINTASK.PMDETAILTASKList[indexDP].DETAILTASK_STARTDATE, startDate_Temp);
				for(var i = indexDC + 1 ; i < MAINTASK.PMDETAILTASKList.length ; i++){
					if(startDate_Temp <= MAINTASK.PMDETAILTASKList[i].DETAILTASK_STARTDATE){
						MAINTASK.PMDETAILTASKList[i].DETAILTASK_STARTDATE = new Date(_this.SubtractDate(MAINTASK, PMHoliday, MAINTASK.PMDETAILTASKList[i].DETAILTASK_STARTDATE, durationDif));
						MAINTASK.PMDETAILTASKList[i].DETAILTASK_ENDDATE = new Date(_this.SubtractDate(MAINTASK, PMHoliday, MAINTASK.PMDETAILTASKList[i].DETAILTASK_ENDDATE, durationDif));
						var successDif = Persistence.ProjectManagement.Methods.PMDETAILTASK_UPD(MAINTASK.PMDETAILTASKList[i]);
						if(!successDif){
							SysCfg.Log.Methods.WriteLog("DemoPMGantt.js ItHelpCenter.Demo.TDemoPMGantt.prototype.Indent (durationParent >= durationChild) successDif", successDif);
						}
					}
				}
				MAINTASK =_this.PMFill();
			}
		}
		else if(durationParent < durationChild){
			MAINTASK.PMDETAILTASKList[indexDP].DETAILTASK_ENDDATE.setDate(MAINTASK.PMDETAILTASKList[indexDP].DETAILTASK_ENDDATE.getDate() + durationChild);
			MAINTASK.PMDETAILTASKList[indexDP].DETAILTASK_DURATION = MAINTASK.PMDETAILTASKList[indexDC].DETAILTASK_DURATION;
			var success = Persistence.ProjectManagement.Methods.PMDETAILTASK_UPD(MAINTASK.PMDETAILTASKList[indexDP]);
			if(success){
				// var durationDif = (startDate_Temp.getTime() - MAINTASK.PMDETAILTASKList[indexDP].DETAILTASK_STARTDATE.getTime()) / 86400000;
				var durationDif = _this.CalculateDuration(MAINTASK, PMHoliday, MAINTASK.PMDETAILTASKList[indexDP].DETAILTASK_STARTDATE, startDate_Temp);
				for(var i = indexDC ; i < MAINTASK.PMDETAILTASKList.length ; i++){
					if(startDate_Temp <= MAINTASK.PMDETAILTASKList[i].DETAILTASK_STARTDATE){
						// MAINTASK.PMDETAILTASKList[i].DETAILTASK_STARTDATE.setDate(MAINTASK.PMDETAILTASKList[i].DETAILTASK_STARTDATE.getDate() - durationDif);
						// MAINTASK.PMDETAILTASKList[i].DETAILTASK_ENDDATE.setDate(MAINTASK.PMDETAILTASKList[i].DETAILTASK_ENDDATE.getDate() - durationDif);
						MAINTASK.PMDETAILTASKList[i].DETAILTASK_STARTDATE = new Date(_this.SubtractDate(MAINTASK, PMHoliday, MAINTASK.PMDETAILTASKList[i].DETAILTASK_STARTDATE, durationDif));
						MAINTASK.PMDETAILTASKList[i].DETAILTASK_ENDDATE = new Date(_this.SubtractDate(MAINTASK, PMHoliday, MAINTASK.PMDETAILTASKList[i].DETAILTASK_ENDDATE, durationDif));
						var successDif = Persistence.ProjectManagement.Methods.PMDETAILTASK_UPD(MAINTASK.PMDETAILTASKList[i]);
						if(!successDif){
							SysCfg.Log.Methods.WriteLog("DemoPMGantt.js ItHelpCenter.Demo.TDemoPMGantt.prototype.Indent (durationParent < durationChild) successDif", successDif);
						}
					}
				}
				MAINTASK =_this.PMFill();
			}
		}
	}
	else{
		MAINTASK.PMDETAILTASKList[indexDP].DETAILTASK_STARTDATE = _this.MinStartDate(MAINTASK.PMDETAILTASKList[indexDP].PMDETAILTASK_CHILDList);
		MAINTASK.PMDETAILTASKList[indexDP].DETAILTASK_ENDDATE = _this.MaxEndDate(MAINTASK.PMDETAILTASKList[indexDP].PMDETAILTASK_CHILDList);
		var success = Persistence.ProjectManagement.Methods.PMDETAILTASK_UPD(MAINTASK.PMDETAILTASKList[indexDP]);
		if(success){
			MAINTASK =_this.PMFill();
		}
	}

	// Funcion recursiva para actualizar las fechas de los padres
	var indexParent = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(MAINTASK.PMDETAILTASKList, DETAILTASKPARENT.IDPMDETAILTASK);
	_this.ParentUpdate(MAINTASK, PMHoliday, MAINTASK.PMDETAILTASKList[indexParent]);

	_this.GanttRefresh(component);
}

ItHelpCenter.Demo.TDemoPMGantt.prototype.MinStartDate = function(detailList){
	var res = detailList[0].DETAILTASK_STARTDATE;
	for(var i = 1 ; i < detailList.length ; i++){
		if(detailList[i].DETAILTASK_STARTDATE < res){
			res = detailList[i].DETAILTASK_STARTDATE;
		}
	}
	return res;
}

ItHelpCenter.Demo.TDemoPMGantt.prototype.MaxEndDate = function(detailList){
	var res = detailList[0].DETAILTASK_ENDDATE;
	for(var i = 1 ; i < detailList.length ; i++){
		if(detailList[i].DETAILTASK_ENDDATE > res) {
			res = detailList[i].DETAILTASK_ENDDATE;
		}
	}
	return res;
}

ItHelpCenter.Demo.TDemoPMGantt.prototype.ParentUpdate = function(MainTask, PMHoliday, detail) {
	var _this = this.TParent();

	if(detail.IDPMDETAILTASK_PARENT == 0){
		detail.DETAILTASK_STARTDATE = _this.MinStartDate(detail.PMDETAILTASK_CHILDList);
		detail.DETAILTASK_ENDDATE = _this.MaxEndDate(detail.PMDETAILTASK_CHILDList);
		// detail.DETAILTASK_DURATION =  _this.CalculateDuration(MainTask, PMHoliday, detail.DETAILTASK_STARTDATE, detail.DETAILTASK_ENDDATE) + ' Días';
		var duration = _this.CalculateDuration(MainTask, PMHoliday, detail.DETAILTASK_STARTDATE, detail.DETAILTASK_ENDDATE);
		if(parseInt(duration) > 1){
			duration += ' ' + UsrCfg.Traslate.GetLangText(_this.Mythis, "DURATION2");
		}else{
			duration += ' ' + UsrCfg.Traslate.GetLangText(_this.Mythis, "DURATION1");
		}
		detail.DETAILTASK_DURATION = duration;
		var success = Persistence.ProjectManagement.Methods.PMDETAILTASK_UPD(detail);
		if(!success){
			SysCfg.Log.Methods.WriteLog("DemoPMGantt.js ItHelpCenter.Demo.TDemoPMGantt.prototype.ParentUpdate", success);
		}
	}
	else if(detail.IDPMDETAILTASK_PARENT != 0){
		detail.DETAILTASK_STARTDATE = _this.MinStartDate(detail.PMDETAILTASK_CHILDList);
		detail.DETAILTASK_ENDDATE = _this.MaxEndDate(detail.PMDETAILTASK_CHILDList);
		// detail.DETAILTASK_DURATION = _this.CalculateDuration(MainTask, PMHoliday, detail.DETAILTASK_STARTDATE, detail.DETAILTASK_ENDDATE) + ' Días';
		var duration = _this.CalculateDuration(MainTask, PMHoliday, detail.DETAILTASK_STARTDATE, detail.DETAILTASK_ENDDATE);
		if(parseInt(duration) > 1){
			duration += ' ' + UsrCfg.Traslate.GetLangText(_this.Mythis, "DURATION2");
		}else{
			duration += ' ' + UsrCfg.Traslate.GetLangText(_this.Mythis, "DURATION1");
		}
		detail.DETAILTASK_DURATION = duration;
		var success = Persistence.ProjectManagement.Methods.PMDETAILTASK_UPD(detail);
		if(success){
			var MAINTASK =_this.PMFill();
			var indexParent = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(MAINTASK.PMDETAILTASKList, detail.IDPMDETAILTASK_PARENT);
			_this.ParentUpdate(MAINTASK, PMHoliday, MAINTASK.PMDETAILTASKList[indexParent]);
		}
	}
}

ItHelpCenter.Demo.TDemoPMGantt.prototype.CancelIndent = function(PMDETAILTASK, component) {
	var _this = this.TParent();
	var MAINTASK =_this.PMFill();
	var indexParent = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(MAINTASK.PMDETAILTASKList, PMDETAILTASK.IDPMDETAILTASK_PARENT);
	var DETAILTASKCHILDList = MAINTASK.PMDETAILTASKList[indexParent].PMDETAILTASK_CHILDList;
	var indexLastChild = DETAILTASKCHILDList.length - 1;
	var index = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(DETAILTASKCHILDList, PMDETAILTASK.IDPMDETAILTASK);
	if(DETAILTASKCHILDList[indexLastChild].IDPMDETAILTASK == PMDETAILTASK.IDPMDETAILTASK){
		// Cambiar solo Parent si es el ultimo hijo, ya que el index no cambia * y tambien la fecha del parent
		PMDETAILTASK.IDPMDETAILTASK_PARENT = MAINTASK.PMDETAILTASKList[indexParent].IDPMDETAILTASK_PARENT;
		var success = Persistence.ProjectManagement.Methods.PMDETAILTASK_UPD(PMDETAILTASK);
	}
	else{
		// Cambiar Parent e Index si no es el ultimo hijo
		var childList = new Array();
		_this.GetChildList(DETAILTASKCHILDList[index], childList);

		var childList_2 = new Array();
		for(var i = index+1 ; i < DETAILTASKCHILDList.length ; i++){
			_this.GetChildList(DETAILTASKCHILDList[i], childList_2);
		}

		var startIndex = childList[0].DETAILTASK_INDEX;
		var alterIndex = startIndex + childList_2.length;

		for(var i = 0 ; i < childList_2.length ; i++){
			var DETAILTASK = childList_2[i];
			DETAILTASK.DETAILTASK_INDEX = startIndex;
			var success = Persistence.ProjectManagement.Methods.PMDETAILTASK_UPD(DETAILTASK);
			if(success){
				startIndex++;
			}
		}
		var DETAILTASK = childList[0];
		DETAILTASK.IDPMDETAILTASK_PARENT = MAINTASK.PMDETAILTASKList[indexParent].IDPMDETAILTASK_PARENT;
		DETAILTASK.DETAILTASK_INDEX = alterIndex;
		var success = Persistence.ProjectManagement.Methods.PMDETAILTASK_UPD(DETAILTASK);
		if(success){
			alterIndex++;
		}
		for(var i = 1 ; i < childList.length ; i++){
			DETAILTASK = childList[i];
			DETAILTASK.DETAILTASK_INDEX = alterIndex;
			var success = Persistence.ProjectManagement.Methods.PMDETAILTASK_UPD(DETAILTASK);
			if(success){
				alterIndex++;
			}
		}
	}

	// Poner la fecha inicial menor y mayor al padre
	MAINTASK =_this.PMFill();
	if(MAINTASK.PMDETAILTASKList[indexParent].PMDETAILTASK_CHILDList.length > 0){
		var MinDate = MAINTASK.PMDETAILTASKList[indexParent].PMDETAILTASK_CHILDList[0].DETAILTASK_STARTDATE;
		var MaxDate = MAINTASK.PMDETAILTASKList[indexParent].PMDETAILTASK_CHILDList[0].DETAILTASK_ENDDATE;
		for(var i = 1 ; i < MAINTASK.PMDETAILTASKList[indexParent].PMDETAILTASK_CHILDList.length ; i++){
			if(MAINTASK.PMDETAILTASKList[indexParent].PMDETAILTASK_CHILDList[i].DETAILTASK_STARTDATE < MinDate){
				MinDate = MAINTASK.PMDETAILTASKList[indexParent].PMDETAILTASK_CHILDList[i].DETAILTASK_STARTDATE;
			}
			else if(MAINTASK.PMDETAILTASKList[indexParent].PMDETAILTASK_CHILDList[i].DETAILTASK_ENDDATE > MaxDate) {
				MaxDate = MAINTASK.PMDETAILTASKList[indexParent].PMDETAILTASK_CHILDList[i].DETAILTASK_ENDDATE;
			}
		}
		var durationT = 0;
		for(var i = 0 ; i < MAINTASK.PMDETAILTASKList[indexParent].PMDETAILTASK_CHILDList.length ; i++){
			durationT = durationT + parseInt(MAINTASK.PMDETAILTASKList[indexParent].PMDETAILTASK_CHILDList[i].DETAILTASK_DURATION);
		}
		if(parseInt(durationT) > 1){
			durationT += ' ' + UsrCfg.Traslate.GetLangText(_this.Mythis, "DURATION2");
		}else{
			durationT += ' ' + UsrCfg.Traslate.GetLangText(_this.Mythis, "DURATION1");
		}
		MAINTASK.PMDETAILTASKList[indexParent].DETAILTASK_STARTDATE = MinDate;
		MAINTASK.PMDETAILTASKList[indexParent].DETAILTASK_ENDDATE = MaxDate;
		MAINTASK.PMDETAILTASKList[indexParent].DETAILTASK_DURATION = durationT;
		var success = Persistence.ProjectManagement.Methods.PMDETAILTASK_UPD(MAINTASK.PMDETAILTASKList[indexParent]);
		if(success){
			MAINTASK =_this.PMFill();
		}
	}
	_this.GanttRefresh(component);
}

ItHelpCenter.Demo.TDemoPMGantt.prototype.GetChildList= function( detail, list ) {
	var _this = this.TParent();
	if( detail.PMDETAILTASK_CHILDList.length == 0 ){
		list.push(detail);
	}
	else if( detail.PMDETAILTASK_CHILDList.length > 0 ){
		list.push(detail);
		_this.GetChildList(detail.PMDETAILTASK_CHILDList[0], list);
		for(var i = 1 ; i < detail.PMDETAILTASK_CHILDList.length ; i++){
			_this.GetChildList(detail.PMDETAILTASK_CHILDList[i], list);
		}
	}
}

ItHelpCenter.Demo.TDemoPMGantt.prototype.Delete = function(PMDETAILTASK, component) {
	var _this = this.TParent();
	var MAINTASK =_this.PMFill();
	var PMHoliday = MAINTASK.PMHOLIDAYList;
	var DETAILTASKList = new Array();
	_this.GetChildList(PMDETAILTASK, DETAILTASKList);
	var indexLastChild = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex( MAINTASK.PMDETAILTASKList, DETAILTASKList[DETAILTASKList.length-1].IDPMDETAILTASK);
	
	var SuccessorsList = new Array();
	for (var i = 0 ; i < DETAILTASKList.length ; i++) {
		if(DETAILTASKList[i].PMRELATIONList.length > 0){
			for(j = 0 ; j < DETAILTASKList[i].PMRELATIONList.length; j++){
				Persistence.ProjectManagement.Methods.PMRELATION_DEL(DETAILTASKList[i].PMRELATIONList[j].IDPMRELATION);
			}
		}
		if(DETAILTASKList[i].PMRELATION_SUCCESSORSList.length > 0){
			for(j = 0 ; j < DETAILTASKList[i].PMRELATION_SUCCESSORSList.length; j++){
				Persistence.ProjectManagement.Methods.PMRELATION_DEL(DETAILTASKList[i].PMRELATION_SUCCESSORSList[j].IDPMRELATION);
				SuccessorsList.push(DETAILTASKList[i].PMRELATION_SUCCESSORSList[j].IDPMDETAILTASK);
			}
		}
		if(DETAILTASKList[i].PMRESOURCESALLOCATIONList.length > 0){
			for(j = 0 ; j < DETAILTASKList[i].PMRESOURCESALLOCATIONList.length; j++){
				Persistence.ProjectManagement.Methods.PMRESOURCESALLOCATION_DEL(DETAILTASKList[i].PMRESOURCESALLOCATIONList[j].IDPMRESOURCESALLOCATION);
			}
		}
		Persistence.ProjectManagement.Methods.PMDETAILTASK_DEL(DETAILTASKList[i].IDPMDETAILTASK);
	}

	// Adecuando los index para el ordenamiento
	var IDPMDETAIL_PARENT = DETAILTASKList[0].IDPMDETAILTASK_PARENT;
	var STARTDATE_TEMP = DETAILTASKList[0].DETAILTASK_STARTDATE;

	if(MAINTASK.PMDETAILTASKList.length > indexLastChild+1){
		var INDEX_TEMP = DETAILTASKList[0].DETAILTASK_INDEX;
		var IDPMDETAILTASK_TEMP = MAINTASK.PMDETAILTASKList[indexLastChild+1].IDPMDETAILTASK;
		MAINTASK =_this.PMFill();
		var index = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex( MAINTASK.PMDETAILTASKList, IDPMDETAILTASK_TEMP);
		for(var i = index ; i < MAINTASK.PMDETAILTASKList.length ; i++){
			MAINTASK.PMDETAILTASKList[i].DETAILTASK_INDEX = INDEX_TEMP; // nuevo index
			var success = Persistence.ProjectManagement.Methods.PMDETAILTASK_UPD(MAINTASK.PMDETAILTASKList[i]);
			if(success){
				INDEX_TEMP++;
			}
		}
	}

	MAINTASK =_this.PMFill();
	var minDate = _this.MinStartDate(MAINTASK.PMDETAILTASKList);
	// comparar con la fecha eliminada
	if(STARTDATE_TEMP < minDate){
		minDate = STARTDATE_TEMP;
	}

	// Modificar fechas 
	var listTemp = new Array(); //lista que ayudara a comparar los sucesores ya modificados para que sus fechas no se modifiquen otra vez
	if(SuccessorsList.length > 0){
		for(var i = 0 ; i < SuccessorsList.length ; i++){
			var index = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(MAINTASK.PMDETAILTASKList, SuccessorsList[i]);
			if(index != -1){
				// var STARTDATE_DIFF = (MAINTASK.PMDETAILTASKList[index].DETAILTASK_STARTDATE.getTime() - minDate.getTime()) / 86400000;
				var STARTDATE_DIFF = _this.CalculateDuration(MAINTASK, PMHoliday, minDate, MAINTASK.PMDETAILTASKList[index].DETAILTASK_STARTDATE);
				_this.UPDSuccessors(MAINTASK, MAINTASK.PMDETAILTASKList[index], STARTDATE_DIFF, listTemp);
			}
		}


	}
	// else{
	if(IDPMDETAIL_PARENT != 0){
		var indexParent = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(MAINTASK.PMDETAILTASKList, IDPMDETAIL_PARENT);
		_this.ParentUpdate(MAINTASK, PMHoliday, MAINTASK.PMDETAILTASKList[indexParent]);
	}
	// }
	// Agregar al diagrama
	_this.GanttRefresh(component);
}

ItHelpCenter.Demo.TDemoPMGantt.prototype.UPDSuccessors = function(mainList, detailSuccessor, difTemp, listTemp) {
	var _this = this.TParent();
	// var  MAINTASK =_this.PMFill();	
	var MAINTASK = mainList;
	var PMHoliday = MAINTASK.PMHOLIDAYList;
	var MAINTASK_TEMP = MAINTASK;
	var index = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex( MAINTASK.PMDETAILTASKList, detailSuccessor.IDPMDETAILTASK);
	if( MAINTASK.PMDETAILTASKList[index].PMRELATION_SUCCESSORSList.length == 0){
		var NOTREPEATED = true;
		for(var x = 0 ; x < listTemp.length; x++){
			if(listTemp[x] == detailSuccessor.IDPMDETAILTASK){
				NOTREPEATED = false;
			}
		}
		if(NOTREPEATED){
			// Evitar que las tareas tengan fecha inicial menor a sus predecesoras
			if(MAINTASK.PMDETAILTASKList[index].PMRELATIONList.length > 0){
				var Predecessors = new Array();
				var maxDate = null;
				for(var i = 0 ; i < MAINTASK.PMDETAILTASKList[index].PMRELATIONList.length ; i++){
					var IDPREDECESSORS = MAINTASK.PMDETAILTASKList[index].PMRELATIONList[i].IDPMDETAILTASK_PREDECESSORS;
					var indexPredecessor = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex( MAINTASK.PMDETAILTASKList, IDPREDECESSORS);
					if(i == 0){
						maxDate = MAINTASK.PMDETAILTASKList[indexPredecessor].DETAILTASK_ENDDATE;
					}
					else{
						if(maxDate < MAINTASK.PMDETAILTASKList[indexPredecessor].DETAILTASK_ENDDATE){
							maxDate = MAINTASK.PMDETAILTASKList[indexPredecessor].DETAILTASK_ENDDATE;
						}
					}
				}
				var DATETEMP = new Date(MAINTASK.PMDETAILTASKList[index].DETAILTASK_STARTDATE);
				if(maxDate > new Date(_this.SubtractDate(MAINTASK, PMHoliday, DATETEMP, difTemp))){
					difTemp = (difTemp - _this.CalculateDuration(MAINTASK, PMHoliday, DATETEMP, maxDate)) - 1;
				}
			}

			MAINTASK.PMDETAILTASKList[index].DETAILTASK_STARTDATE = new Date(_this.SubtractDate(MAINTASK, PMHoliday, MAINTASK.PMDETAILTASKList[index].DETAILTASK_STARTDATE, difTemp));
			MAINTASK.PMDETAILTASKList[index].DETAILTASK_ENDDATE = new Date(_this.SubtractDate(MAINTASK, PMHoliday, MAINTASK.PMDETAILTASKList[index].DETAILTASK_ENDDATE, difTemp));
			
			var success = Persistence.ProjectManagement.Methods.PMDETAILTASK_UPD(MAINTASK.PMDETAILTASKList[index]);
			if(success){
				var MAINTASK_TEMP =_this.PMFill();
				// determinar si es el ultimo hermano
				if(MAINTASK.PMDETAILTASKList[index].IDPMDETAILTASK_PARENT != 0){ //determinar si la tarea no es Parent 0 -> no tiene más padres
					var indexParent = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex( MAINTASK.PMDETAILTASKList, MAINTASK.PMDETAILTASKList[index].IDPMDETAILTASK_PARENT );
					var indexLastChild = MAINTASK.PMDETAILTASKList[indexParent].PMDETAILTASK_CHILDList.length - 1;
					if(MAINTASK.PMDETAILTASKList[indexParent].PMDETAILTASK_CHILDList[indexLastChild].IDPMDETAILTASK == MAINTASK.PMDETAILTASKList[index].IDPMDETAILTASK){
						_this.ParentUpdate(MAINTASK, PMHoliday, MAINTASK_TEMP.PMDETAILTASKList[indexParent]);
					}
				}
			}
			listTemp.push(detailSuccessor.IDPMDETAILTASK); // agregar a lista para que no se repitan
		}
	}
	else if(MAINTASK.PMDETAILTASKList[index].PMRELATION_SUCCESSORSList.length > 0){
		var NOTREPEATED = true;
		for(var x = 0 ; x < listTemp.length; x++){
			if(listTemp[x] == detailSuccessor.IDPMDETAILTASK){
				NOTREPEATED = false;
			}
		}
		if(NOTREPEATED){
			// Evitar que las tareas tengan fecha inicial menor a sus predecesoras
			if(MAINTASK.PMDETAILTASKList[index].PMRELATIONList.length > 0){
				var Predecessors = new Array();
				var maxDate = null;
				for(var i = 0 ; i < MAINTASK.PMDETAILTASKList[index].PMRELATIONList.length ; i++){
					var IDPREDECESSORS = MAINTASK.PMDETAILTASKList[index].PMRELATIONList[i].IDPMDETAILTASK_PREDECESSORS;
					var indexPredecessor = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex( MAINTASK.PMDETAILTASKList, IDPREDECESSORS);
					if(i == 0){
						maxDate = MAINTASK.PMDETAILTASKList[indexPredecessor].DETAILTASK_ENDDATE;
					}
					else{
						if(maxDate < MAINTASK.PMDETAILTASKList[indexPredecessor].DETAILTASK_ENDDATE){
							maxDate = MAINTASK.PMDETAILTASKList[indexPredecessor].DETAILTASK_ENDDATE;
						}
					}
				}
				var DATETEMP = new Date(MAINTASK.PMDETAILTASKList[index].DETAILTASK_STARTDATE);
				if(maxDate > new Date(_this.SubtractDate(MAINTASK, PMHoliday, DATETEMP, difTemp))){
					difTemp = (difTemp - _this.CalculateDuration(MAINTASK, PMHoliday, DATETEMP, maxDate)) - 1;
				}
			}

			MAINTASK.PMDETAILTASKList[index].DETAILTASK_STARTDATE = new Date(_this.SubtractDate(MAINTASK, PMHoliday, MAINTASK.PMDETAILTASKList[index].DETAILTASK_STARTDATE, difTemp));
			MAINTASK.PMDETAILTASKList[index].DETAILTASK_ENDDATE = new Date(_this.SubtractDate(MAINTASK, PMHoliday, MAINTASK.PMDETAILTASKList[index].DETAILTASK_ENDDATE, difTemp));

			var success = Persistence.ProjectManagement.Methods.PMDETAILTASK_UPD(MAINTASK.PMDETAILTASKList[index]);
			if(success){
				MAINTASK_TEMP =_this.PMFill();
				// determinar si es el ultimo hermano
				if(MAINTASK.PMDETAILTASKList[index].IDPMDETAILTASK_PARENT != 0){ //determinar si la tarea no es Parent 0 -> no tiene más padres
					var indexParent = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex( MAINTASK.PMDETAILTASKList, MAINTASK.PMDETAILTASKList[index].IDPMDETAILTASK_PARENT );
					var indexLastChild = MAINTASK.PMDETAILTASKList[indexParent].PMDETAILTASK_CHILDList.length - 1;
					if(MAINTASK.PMDETAILTASKList[indexParent].PMDETAILTASK_CHILDList[indexLastChild].IDPMDETAILTASK == MAINTASK.PMDETAILTASKList[index].IDPMDETAILTASK){
						_this.ParentUpdate(MAINTASK, PMHoliday, MAINTASK_TEMP.PMDETAILTASKList[indexParent]);
					}
				}
			}
			listTemp.push(detailSuccessor.IDPMDETAILTASK); // agregar a lista para que no se repitan
		}
		_this.UPDSuccessors(MAINTASK_TEMP ,MAINTASK_TEMP.PMDETAILTASKList[index].PMRELATION_SUCCESSORSList[0], difTemp, listTemp);

		for(var i = 1 ; i < MAINTASK_TEMP.PMDETAILTASKList[index].PMRELATION_SUCCESSORSList.length ; i++){
			_this.UPDSuccessors(MAINTASK_TEMP, MAINTASK_TEMP.PMDETAILTASKList[index].PMRELATION_SUCCESSORSList[i], difTemp, listTemp);
		}
	}
}

ItHelpCenter.Demo.TDemoPMGantt.prototype.MoveUp = function(PMDETAILTASK, component) {
	var _this = this.TParent();
	var MAINTASK = _this.PMFill();
	if(MAINTASK.PMDETAILTASKList[0].IDPMDETAILTASK  == PMDETAILTASK.IDPMDETAILTASK){
		alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines7"));
	}
	else{
		var childList = new Array();
		var previousChildList = new Array();
		if(PMDETAILTASK.IDPMDETAILTASK_PARENT == 0){
			var index = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(MAINTASK.PMDETAILTASKPARENTList, PMDETAILTASK.IDPMDETAILTASK);
			var startIndex = MAINTASK.PMDETAILTASKPARENTList[index-1].DETAILTASK_INDEX;
			_this.GetChildList(MAINTASK.PMDETAILTASKPARENTList[index], childList);
			_this.GetChildList(MAINTASK.PMDETAILTASKPARENTList[index-1], previousChildList);
			
			var alterIndex = _this.IncIndex(startIndex, childList);
			_this.IncIndex(alterIndex, previousChildList);
		}else{
			var indexParent = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(MAINTASK.PMDETAILTASKList, PMDETAILTASK.IDPMDETAILTASK_PARENT);
			var DETAILTASKPARENT = MAINTASK.PMDETAILTASKList[indexParent];
			var index = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(DETAILTASKPARENT.PMDETAILTASK_CHILDList, PMDETAILTASK.IDPMDETAILTASK);
			if(index == 0){
				alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines7"));
			}
			else{
				var startIndex = DETAILTASKPARENT.PMDETAILTASK_CHILDList[index-1].DETAILTASK_INDEX;
				_this.GetChildList(DETAILTASKPARENT.PMDETAILTASK_CHILDList[index], childList);
				_this.GetChildList(DETAILTASKPARENT.PMDETAILTASK_CHILDList[index-1], previousChildList);
				
				var alterIndex = _this.IncIndex(startIndex, childList);
				_this.IncIndex(alterIndex, previousChildList);
			}
		}
	}
	// Agregar al diagrama
	_this.GanttRefresh(component);
}

ItHelpCenter.Demo.TDemoPMGantt.prototype.MoveDown = function(PMDETAILTASK, component) {
	var _this = this.TParent();
	var MAINTASK = _this.PMFill();
	if(MAINTASK.PMDETAILTASKList[MAINTASK.PMDETAILTASKList.length-1].IDPMDETAILTASK  == PMDETAILTASK.IDPMDETAILTASK ){
		alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines8"));
	}
	else{
		var childList = new Array();
		var laterChildList = new Array();
		if(PMDETAILTASK.IDPMDETAILTASK_PARENT == 0){
			var index = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(MAINTASK.PMDETAILTASKPARENTList, PMDETAILTASK.IDPMDETAILTASK);
			var startIndex = MAINTASK.PMDETAILTASKPARENTList[index].DETAILTASK_INDEX;
			_this.GetChildList(MAINTASK.PMDETAILTASKPARENTList[index], childList);
			_this.GetChildList(MAINTASK.PMDETAILTASKPARENTList[index+1], laterChildList);
			
			var alterIndex = _this.IncIndex(startIndex, laterChildList);
			_this.IncIndex(alterIndex, childList);
		}else{
			var indexParent = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(MAINTASK.PMDETAILTASKList, PMDETAILTASK.IDPMDETAILTASK_PARENT);
			var DETAILTASKPARENT = MAINTASK.PMDETAILTASKList[indexParent];
			var index = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(DETAILTASKPARENT.PMDETAILTASK_CHILDList, PMDETAILTASK.IDPMDETAILTASK);
			if(index == DETAILTASKPARENT.PMDETAILTASK_CHILDList.length-1){
				alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines8"));
			}
			else{
				var startIndex = DETAILTASKPARENT.PMDETAILTASK_CHILDList[index].DETAILTASK_INDEX;
				_this.GetChildList(DETAILTASKPARENT.PMDETAILTASK_CHILDList[index], childList);
				_this.GetChildList(DETAILTASKPARENT.PMDETAILTASK_CHILDList[index+1], laterChildList);

				var alterIndex = _this.IncIndex(startIndex, laterChildList);
				_this.IncIndex(alterIndex, childList);
			}
		}
	}
	// Agregar al diagrama
	_this.GanttRefresh(component);
}

ItHelpCenter.Demo.TDemoPMGantt.prototype.IncIndex = function(startIndex, detailList){
	var _this = this.TParent();
	for(var i = 0 ; i < detailList.length ; i++){
		var DETAILTASK = detailList[i];
		DETAILTASK.DETAILTASK_INDEX = startIndex;
		var success = Persistence.ProjectManagement.Methods.PMDETAILTASK_UPD(DETAILTASK);
		if(success){
			startIndex++;
		}
	}
	return startIndex;
}

ItHelpCenter.Demo.TDemoPMGantt.prototype.Link = function(PMDETAILTASK, component){
	var _this = this.TParent();
	var MAINTASK =_this.PMFill();
	var Modal = new TVclModal(document.body, null, "");
	Modal.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines9"));
	function MediaQuery(x) {
		if (x.matches) { // menos de 780px
			Modal.This.This.style.paddingRight = '0%';
			Modal.This.This.style.paddingLeft = '0%';
		}
		else {// mayor de 780px
			Modal.This.This.style.paddingRight = '15%';
			Modal.This.This.style.paddingLeft = '15%';
		}
	}
	var x = window.matchMedia("(max-width: 700px)");
	MediaQuery(x);
	x.addListener(MediaQuery);

	var Container = new TVclStackPanel(Modal.Body.This, 'Link_Form', 2, [[6, 6], [6, 6], [6, 6], [6, 6], [6, 6]]);

	var ContainerTitlePre = new TVclStackPanel(Container.Column[0].This, '', 1, [[12], [12], [12], [12], [12]]);
	ContainerTitlePre.Row.This.style.textAlign = 'center';
	ContainerTitlePre.Row.This.style.marginTop = '5px';
	ContainerTitlePre.Row.This.style.marginBottom = '10px';
	ContainerTitlePre.Row.This.style.fontSize = '20px';
	Container.Column[0].This.style.borderRight = '1px solid darkgrey';
	var lblTitlePre = new TVcllabel(ContainerTitlePre.Column[0].This, '', TlabelType.H0);
	lblTitlePre.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblTitlePre.Text");
	lblTitlePre.VCLType = TVCLType.BS;

	var ContTaskPredeccessor = new TVclStackPanel(Container.Column[0].This, "1", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	ContTaskPredeccessor.Row.This.style.marginBottom = '5px';
	var lblTaskP = new TVcllabel(ContTaskPredeccessor.Column[0].This, "", TlabelType.H0);
	lblTaskP.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblTaskP.Text");
	lblTaskP.VCLType = TVCLType.BS;
	var cbTaskP = new TVclComboBox2(ContTaskPredeccessor.Column[1].This, "");
	cbTaskP.VCLType = TVCLType.BS;
	for (var i = 0; i < MAINTASK.PMDETAILTASKList.length; i++) {
		if(MAINTASK.PMDETAILTASKList[i].DETAILTASK_ENDDATE < PMDETAILTASK.DETAILTASK_STARTDATE){
			var status = true
			for(var  j = 0; j < PMDETAILTASK.PMRELATIONList.length; j++){
				if(MAINTASK.PMDETAILTASKList[i].IDPMDETAILTASK == PMDETAILTASK.PMRELATIONList[j].IDPMDETAILTASK_PREDECESSORS){
					status = false;
				}
			}
			if(status){
				var ComboItem = new TVclComboBoxItem();
				ComboItem.Id = MAINTASK.PMDETAILTASKList[i].IDPMDETAILTASK;
				ComboItem.Value = MAINTASK.PMDETAILTASKList[i].DETAILTASK_INDEX;
				ComboItem.Text = MAINTASK.PMDETAILTASKList[i].DETAILTASK_NAME;
				ComboItem.Tag = "Test";
				cbTaskP.AddItem(ComboItem);
			}
		}
	}
	cbTaskP.selectedIndex = 0;

	var ContTypeRelationPre = new TVclStackPanel(Container.Column[0].This, "1", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	ContTypeRelationPre.Row.This.style.marginBottom = '5px';
	var lblTypeRelationPre = new TVcllabel(ContTypeRelationPre.Column[0].This, "", TlabelType.H0);
	lblTypeRelationPre.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblTypeRelationPre.Text");
	lblTypeRelationPre.VCLType = TVCLType.BS;
	var cbTypeRelationPre = new TVclComboBox2(ContTypeRelationPre.Column[1].This, "");
	cbTypeRelationPre.VCLType = TVCLType.BS;
	for (var i = 0; i < _this.PManagement.PMTYPERELATIONList.length; i++) {
		var ComboItem = new TVclComboBoxItem();
		ComboItem.Value = _this.PManagement.PMTYPERELATIONList[i].IDPMTYPERELATION;
		ComboItem.Text = (_this.PManagement.PMTYPERELATIONList[i].TYPERELATION_NAME).toUpperCase();
		ComboItem.Tag = "TypeRelationPre";
		cbTypeRelationPre.AddItem(ComboItem);
	}
	cbTypeRelationPre.selectedIndex = 0;

	var ContainerBtnP = new TVclStackPanel(Container.Column[0].This, "1",  1, [[12], [12], [12], [12], [12]]);
	ContainerBtnP.Row.This.style.marginBottom = '10px';
	ContainerBtnP.Column[0].This.style.textAlign = 'right';
	var btnAddP = new TVclButton(ContainerBtnP.Column[0].This, "");
	btnAddP.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnAdd.Text");
	btnAddP.VCLType = TVCLType.BS;
	btnAddP.Src = "image/16/add.png";
	btnAddP.onClick = function () {
		var PMRELATION = new Persistence.ProjectManagement.Properties.TPMRELATION();
		PMRELATION.IDPMDETAILTASK_PREDECESSORS = parseInt(cbTaskP.Options[cbTaskP.selectedIndex].Id);
		PMRELATION.IDPMDETAILTASK = PMDETAILTASK.IDPMDETAILTASK;
		PMRELATION.IDPMTYPERELATION = parseInt(cbTypeRelationPre.Value);
		var success = Persistence.ProjectManagement.Methods.PMRELATION_ADD(PMRELATION);
		if(success){
			var NewRow = new TRow();
			var Cell = new TCell();
			Cell.Value = cbTaskP.Value;
			Cell.IndexColumn = 0;
			Cell.IndexRow = i;
			NewRow.AddCell(Cell);
			Cell = new TCell();
			Cell.Value = cbTaskP.Text;
			Cell.IndexColumn = 0;
			Cell.IndexRow = i;
			NewRow.AddCell(Cell);

			NewRow.This.setAttribute('data-id', cbTaskP.Options[cbTaskP.selectedIndex].Id);
			DateGridP.AddRow(NewRow);

			MAINTASK =_this.PMFill();
			var index = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(MAINTASK.PMDETAILTASKList, PMDETAILTASK.IDPMDETAILTASK);
			PMDETAILTASK = MAINTASK.PMDETAILTASKList[index];
			cbTaskP.ClearItems();
			for (var i = 0; i < MAINTASK.PMDETAILTASKList.length; i++) {
				if(MAINTASK.PMDETAILTASKList[i].DETAILTASK_ENDDATE < PMDETAILTASK.DETAILTASK_STARTDATE){
					var status = true
					for(var  j = 0; j < PMDETAILTASK.PMRELATIONList.length; j++){
						if(MAINTASK.PMDETAILTASKList[i].IDPMDETAILTASK == PMDETAILTASK.PMRELATIONList[j].IDPMDETAILTASK_PREDECESSORS){
							status = false;
						}
					}
					if(status){
						var ComboItem = new TVclComboBoxItem();
						ComboItem.Id = MAINTASK.PMDETAILTASKList[i].IDPMDETAILTASK;
						ComboItem.Value = MAINTASK.PMDETAILTASKList[i].DETAILTASK_INDEX;
						ComboItem.Text = MAINTASK.PMDETAILTASKList[i].DETAILTASK_NAME;
						ComboItem.Tag = "Test";
						cbTaskP.AddItem(ComboItem);
					}
				}
			}
			cbTaskP.selectedIndex = 0;
			if(cbTaskP.Options == 0){
				Container.Column[0].This.style.display = 'none';
				$(Container.Column[1].This).removeClass();
				Container.Column[1].This.classList.add('col-xs-12');
				if(cbTaskS.Options == 0){
					Modal.CloseModal();
					$(Modal.This.This).remove();
				}
			}
			_this.GanttRefresh(component);
		}
	}
	btnAddP.This.style.padding = '0px';
	btnAddP.This.style.paddingLeft = '10px';
	btnAddP.This.style.float = 'right';

	var DateGridP = new TGrid(Container.Column[0].This, '', '');

	var Col0 = new TColumn();
	Col0.Name = ""; 
	Col0.Caption = UsrCfg.Traslate.GetLangText(_this.Mythis, "INDEX");
	Col0.Index = 0; 
	Col0.EnabledEditor = false;
	Col0.DataType = SysCfg.DB.Properties.TDataType.String;
	Col0.ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.None;
	DateGridP.AddColumn(Col0);

	var Col1 = new TColumn();
	Col1.Name = ""; 
	Col1.Caption = UsrCfg.Traslate.GetLangText(_this.Mythis, "NAME");
	Col1.Index = 0; 
	Col1.EnabledEditor = false;
	Col1.DataType = SysCfg.DB.Properties.TDataType.String;
	Col1.ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.None;
	DateGridP.AddColumn(Col1);

	DateGridP.This.children[0].style.backgroundColor = '#00a65a';
	DateGridP.This.children[0].style.textAlign = 'center';
	for(var i = 0 ; i < DateGridP.This.children[0].children[0].childElementCount; i++){
		DateGridP.This.children[0].children[0].children[i].style.textAlign = 'center';
	}
	DateGridP.This.style.textAlign = 'center';
	DateGridP.EnabledResizeColumn = false;

	for (var i = 0; i < PMDETAILTASK.PMRELATIONList.length; i++) {
		var NewRow = new TRow();
		var Cell = new TCell();
		Cell.Value = PMDETAILTASK.PMRELATIONList[i].PMDETAILTASK_PREDECESSORS.DETAILTASK_INDEX;
		Cell.IndexColumn = 0;
		Cell.IndexRow = i;
		NewRow.AddCell(Cell);

		Cell = new TCell();
		Cell.Value = PMDETAILTASK.PMRELATIONList[i].PMDETAILTASK_PREDECESSORS.DETAILTASK_NAME;
		Cell.IndexColumn = 0;
		Cell.IndexRow = i;
		NewRow.AddCell(Cell);
		NewRow.Cells[0].This.style.width= '60px';
		// NewRow.This.setAttribute('data-id', PMDETAILTASK.PMRELATIONList[i].PMDETAILTASK_PREDECESSORS.IDPMDETAILTASK);
		DateGridP.AddRow(NewRow);
	}

	var ContainerTitleSuc = new TVclStackPanel(Container.Column[1].This, '', 1, [[12], [12], [12], [12], [12]]);
	ContainerTitleSuc.Row.This.style.textAlign = 'center';
	ContainerTitleSuc.Row.This.style.marginTop = '5px';
	ContainerTitleSuc.Row.This.style.marginBottom = '10px';
	ContainerTitleSuc.Row.This.style.fontSize = '20px';
	var lblTitleSuc = new TVcllabel(ContainerTitleSuc.Column[0].This, '', TlabelType.H0);
	lblTitleSuc.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblTitleSuc.Text");
	lblTitleSuc.VCLType = TVCLType.BS;

	var ContTaskSuccessor = new TVclStackPanel(Container.Column[1].This, "1", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	ContTaskSuccessor.Row.This.style.marginBottom = '5px';
	var lblTaskS = new TVcllabel(ContTaskSuccessor.Column[0].This, "", TlabelType.H0);
	lblTaskS.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblTaskS.Text");
	lblTaskS.VCLType = TVCLType.BS;
	var cbTaskS = new TVclComboBox2(ContTaskSuccessor.Column[1].This, "");
	cbTaskS.VCLType = TVCLType.BS;
	for (var i = 0; i < MAINTASK.PMDETAILTASKList.length; i++) {
		if(MAINTASK.PMDETAILTASKList[i].DETAILTASK_STARTDATE > PMDETAILTASK.DETAILTASK_ENDDATE){
			var status = true
			for(var  j = 0; j < PMDETAILTASK.PMRELATION_SUCCESSORSList.length; j++){
				if(MAINTASK.PMDETAILTASKList[i].IDPMDETAILTASK == PMDETAILTASK.PMRELATION_SUCCESSORSList[j].IDPMDETAILTASK){
					status = false;
				}
			}
			if(status){
				var ComboItem = new TVclComboBoxItem();
				ComboItem.Id = MAINTASK.PMDETAILTASKList[i].IDPMDETAILTASK;
				ComboItem.Value = MAINTASK.PMDETAILTASKList[i].DETAILTASK_INDEX;
				ComboItem.Text = MAINTASK.PMDETAILTASKList[i].DETAILTASK_NAME;
				ComboItem.Tag = "Test";
				cbTaskS.AddItem(ComboItem);
			}
		}
	}
	cbTaskS.selectedIndex = 0;

	var ContTypeRelationSuc = new TVclStackPanel(Container.Column[1].This, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	ContTypeRelationSuc.Row.This.style.marginBottom = '5px';
	var lblTypeRelationSuc = new TVcllabel(ContTypeRelationSuc.Column[0].This, "", TlabelType.H0);
	lblTypeRelationSuc.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblTypeRelationSuc.Text");
	lblTypeRelationSuc.VCLType = TVCLType.BS;
	var cbTypeRelationSuc = new TVclComboBox2(ContTypeRelationSuc.Column[1].This, "");
	cbTypeRelationSuc.VCLType = TVCLType.BS;
	for (var i = 0; i < _this.PManagement.PMTYPERELATIONList.length; i++) {
		var ComboItem = new TVclComboBoxItem();
		ComboItem.Value = _this.PManagement.PMTYPERELATIONList[i].IDPMTYPERELATION;
		ComboItem.Text = (_this.PManagement.PMTYPERELATIONList[i].TYPERELATION_NAME).toUpperCase();
		ComboItem.Tag = "TypeRelationPre";
		cbTypeRelationSuc.AddItem(ComboItem);
	}
	cbTypeRelationSuc.selectedIndex = 0;

	if(cbTaskP.Options == 0){
		Container.Column[0].This.style.display = 'none';
		$(Container.Column[1].This).removeClass();
		Container.Column[1].This.classList.add('col-xs-12');
		if(cbTaskS.Options !== 0){
			Modal.ShowModal();
		}
	}
	else if(cbTaskS.Options == 0){
		Container.Column[1].This.style.display = 'none';
		$(Container.Column[0].This).removeClass();
		Container.Column[0].This.classList.add('col-xs-12');
		Container.Column[0].This.style.marginRight = '15px';
		Container.Column[0].This.style.paddingLeft = '15px';
		Container.Column[0].This.style.borderRight = '';
		if(cbTaskP.Options !== 0){
			Modal.ShowModal();
		}
	}
	else if(cbTaskP.Options !== 0 && cbTaskS.Options !== 0){
		Modal.ShowModal();
	}

	var ContainerBtnS = new TVclStackPanel(Container.Column[1].This, "",  1, [[12], [12], [12], [12], [12]]);
	ContainerBtnS.Row.This.style.marginBottom = '10px';
	ContainerBtnS.Column[0].This.style.textAlign = 'right';
	var btnAddS = new TVclButton(ContainerBtnS.Column[0].This, "");
	btnAddS.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnAdd.Text");
	btnAddS.VCLType = TVCLType.BS;
	btnAddS.Src = "image/16/add.png";
	btnAddS.onClick = function () {
		var PMRELATION = new Persistence.ProjectManagement.Properties.TPMRELATION();
		PMRELATION.IDPMDETAILTASK_PREDECESSORS = PMDETAILTASK.IDPMDETAILTASK;
		PMRELATION.IDPMDETAILTASK = parseInt(cbTaskS.Options[cbTaskS.selectedIndex].Id);
		PMRELATION.IDPMTYPERELATION = parseInt(cbTypeRelationSuc.Value);
		var success = Persistence.ProjectManagement.Methods.PMRELATION_ADD(PMRELATION);
		if(success){
			var NewRow = new TRow();
			var Cell = new TCell();
			Cell.Value = cbTaskS.Value;
			Cell.IndexColumn = 0;
			Cell.IndexRow = i;
			NewRow.AddCell(Cell);
			Cell = new TCell();
			Cell.Value = cbTaskS.Text;
			Cell.IndexColumn = 0;
			Cell.IndexRow = i;
			NewRow.AddCell(Cell);

			NewRow.This.setAttribute('data-id', cbTaskS.Options[cbTaskS.selectedIndex].Id);
			DateGridS.AddRow(NewRow);

			$(cbTaskS.Options[cbTaskS.selectedIndex].This).remove();

			MAINTASK =_this.PMFill();
			var index = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(MAINTASK.PMDETAILTASKList, PMDETAILTASK.IDPMDETAILTASK);
			PMDETAILTASK = MAINTASK.PMDETAILTASKList[index];
			cbTaskS.ClearItems();
			for (var i = 0; i < MAINTASK.PMDETAILTASKList.length; i++) {
				if(MAINTASK.PMDETAILTASKList[i].DETAILTASK_STARTDATE > PMDETAILTASK.DETAILTASK_ENDDATE){
					var status = true
					for(var  j = 0; j < PMDETAILTASK.PMRELATION_SUCCESSORSList.length; j++){
						if(MAINTASK.PMDETAILTASKList[i].IDPMDETAILTASK == PMDETAILTASK.PMRELATION_SUCCESSORSList[j].IDPMDETAILTASK){
							status = false;
						}
					}
					if(status){
						var ComboItem = new TVclComboBoxItem();
						ComboItem.Id = MAINTASK.PMDETAILTASKList[i].IDPMDETAILTASK;
						ComboItem.Value = MAINTASK.PMDETAILTASKList[i].DETAILTASK_INDEX;
						ComboItem.Text = MAINTASK.PMDETAILTASKList[i].DETAILTASK_NAME;
						ComboItem.Tag = "Test";
						cbTaskS.AddItem(ComboItem);
					}
				}
			}
			cbTaskS.selectedIndex = 0;
			if(cbTaskS.Options == 0){
				Container.Column[1].This.style.display = 'none';
				$(Container.Column[0].This).removeClass();
				Container.Column[0].This.classList.add('col-xs-12');
				Container.Column[0].This.style.borderRight = '';
				if(cbTaskP.Options == 0){
					Modal.CloseModal();
					$(Modal.This.This).remove();
				}
			}

			_this.GanttRefresh(component);
		}
	}
	btnAddS.This.style.padding = '0px';
	btnAddS.This.style.paddingLeft = '10px';
	btnAddS.This.style.float = 'right';

	var DateGridS = new TGrid(Container.Column[1].This, '', '');
	var Col0 = new TColumn();
	Col0.Name = ""; 
	Col0.Caption = UsrCfg.Traslate.GetLangText(_this.Mythis, "INDEX");
	Col0.Index = 0; 
	Col0.EnabledEditor = false;
	Col0.DataType = SysCfg.DB.Properties.TDataType.String;
	Col0.ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.None;
	DateGridS.AddColumn(Col0);

	var Col1 = new TColumn();
	Col1.Name = ""; 
	Col1.Caption = UsrCfg.Traslate.GetLangText(_this.Mythis, "NAME");
	Col1.Index = 0; 
	Col1.EnabledEditor = false;
	Col1.DataType = SysCfg.DB.Properties.TDataType.String;
	Col1.ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.None;
	DateGridS.AddColumn(Col1);

	DateGridS.This.children[0].style.backgroundColor = '#00a65a';
	DateGridS.This.children[0].style.textAlign = 'center';
	for(var i = 0 ; i < DateGridS.This.children[0].children[0].childElementCount; i++){
		DateGridS.This.children[0].children[0].children[i].style.textAlign = 'center';
	}
	DateGridS.This.style.textAlign = 'center';
	DateGridS.EnabledResizeColumn = false;

	for (var i = 0; i < PMDETAILTASK.PMRELATION_SUCCESSORSList.length; i++) {
		var NewRow = new TRow();
		var Cell = new TCell();
		Cell.Value = PMDETAILTASK.PMRELATION_SUCCESSORSList[i].PMDETAILTASK.DETAILTASK_INDEX;
		Cell.IndexColumn = 0;
		Cell.IndexRow = i;
		NewRow.AddCell(Cell);

		Cell = new TCell();
		Cell.Value = PMDETAILTASK.PMRELATION_SUCCESSORSList[i].PMDETAILTASK.DETAILTASK_NAME;
		Cell.IndexColumn = 0;
		Cell.IndexRow = i;
		NewRow.AddCell(Cell);

		NewRow.Cells[0].This.style.width= '60px';
		NewRow.This.setAttribute('data-id', PMDETAILTASK.PMRELATION_SUCCESSORSList[i].PMDETAILTASK.IDPMDETAILTASK);
		DateGridS.AddRow(NewRow);
	}

	
	Modal.FunctionAfterClose = function () {
		$(this.This.This).remove();
	}
}

ItHelpCenter.Demo.TDemoPMGantt.prototype.UnLink = function(PMDETAILTASK, component){
	var _this = this.TParent();
	var idTempPre = 0;
	var idTempSuc = 0;
	var MAINTASK =_this.PMFill();
	var Modal = new TVclModal(document.body, null, "");
	Modal.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines10"));
	function MediaQuery(x) {
		if (x.matches) { // menos de 780px
			Modal.This.This.style.paddingRight = '0%';
			Modal.This.This.style.paddingLeft = '0%';
		}
		else {// mayor de 780px
			Modal.This.This.style.paddingRight = '15%';
			Modal.This.This.style.paddingLeft = '15%';
		}
	}
	var x = window.matchMedia("(max-width: 700px)");
	MediaQuery(x);
	x.addListener(MediaQuery);

	var Container = new TVclStackPanel(Modal.Body.This, 'UnLink_Form', 2, [[6, 6], [6, 6], [6, 6], [6, 6], [6, 6]]);

	var ContainerTitlePre = new TVclStackPanel(Container.Column[0].This, '', 1, [[12], [12], [12], [12], [12]]);
	ContainerTitlePre.Row.This.style.textAlign = 'center';
	ContainerTitlePre.Row.This.style.marginTop = '5px';
	ContainerTitlePre.Row.This.style.marginBottom = '10px';
	ContainerTitlePre.Row.This.style.fontSize = '20px';
	Container.Column[0].This.style.borderRight = '1px solid darkgrey';
	var lblTitlePre = new TVcllabel(ContainerTitlePre.Column[0].This, '', TlabelType.H0);
	lblTitlePre.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblTitlePre.Text");
	lblTitlePre.VCLType = TVCLType.BS;

	if(PMDETAILTASK.PMRELATIONList.length > 0){
		var DateGridP = new TGrid(Container.Column[0].This, '', '');
		var Col0 = new TColumn();
		Col0.Name = ""; 
		Col0.Caption = UsrCfg.Traslate.GetLangText(_this.Mythis, "INDEX");
		Col0.Index = 0; 
		Col0.EnabledEditor = false;
		Col0.DataType = SysCfg.DB.Properties.TDataType.String;
		Col0.ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.None;
		DateGridP.AddColumn(Col0);

		var Col1 = new TColumn();
		Col1.Name = ""; 
		Col1.Caption = UsrCfg.Traslate.GetLangText(_this.Mythis, "NAME");
		Col1.Index = 0; 
		Col1.EnabledEditor = false;
		Col1.DataType = SysCfg.DB.Properties.TDataType.String;
		Col1.ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.None;
		DateGridP.AddColumn(Col1);

		DateGridP.This.children[0].style.backgroundColor = '#00a65a';
		DateGridP.This.children[0].style.textAlign = 'center';
		for(var i = 0 ; i < DateGridP.This.children[0].children[0].childElementCount; i++){
			DateGridP.This.children[0].children[0].children[i].style.textAlign = 'center';
		}
		DateGridP.This.style.textAlign = 'center';
		DateGridP.EnabledResizeColumn = false;
		var RowList = new Array();
		for (var i = 0; i < PMDETAILTASK.PMRELATIONList.length; i++) {
			var NewRow = new TRow();
			var Cell = new TCell();
			Cell.Value = PMDETAILTASK.PMRELATIONList[i].PMDETAILTASK_PREDECESSORS.DETAILTASK_INDEX;
			Cell.IndexColumn = 0;
			Cell.IndexRow = i;
			NewRow.AddCell(Cell);
			Cell = new TCell();
			Cell.Value = PMDETAILTASK.PMRELATIONList[i].PMDETAILTASK_PREDECESSORS.DETAILTASK_NAME;
			Cell.IndexColumn = 0;
			Cell.IndexRow = i;
			NewRow.AddCell(Cell);
			NewRow.Cells[0].This.style.width= '60px';
			NewRow.This.setAttribute('data-id', PMDETAILTASK.PMRELATIONList[i].IDPMRELATION);
			RowList[i] = {row: NewRow.This}
			DateGridP.AddRow(NewRow);
			RowList[i].row.onclick = function () {
				for(var i = 0 ; i < this.parentNode.childElementCount ; i++){this.parentNode.children[i].style.backgroundColor = 'transparent'}
				this.style.backgroundColor = '#BCFFB0';
				idTempPre = parseInt(this.dataset.id)
			}
		}
	}
	else{
		Container.Column[0].This.style.display = 'none';
		$(Container.Column[1].This).removeClass();
		Container.Column[1].This.classList.add('col-xs-12');
		if(PMDETAILTASK.PMRELATION_SUCCESSORSList.length > 0){
			Modal.ShowModal();
		}
	}

	var ContainerBtnP = new TVclStackPanel(Container.Column[0].This, "",  1, [[12], [12], [12], [12], [12]]);
	ContainerBtnP.Row.This.style.marginBottom = '10px';
	ContainerBtnP.Column[0].This.style.textAlign = 'right';
	var btnRemoveP = new TVclButton(ContainerBtnP.Column[0].This, "");
	btnRemoveP.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnRemove.Text");
	btnRemoveP.VCLType = TVCLType.BS;
	btnRemoveP.Src = "image/16/delete.png";
	btnRemoveP.onClick = function () {
		if(idTempPre != 0){
			var success = Persistence.ProjectManagement.Methods.PMRELATION_DEL(parseInt(idTempPre));
			if(success){
				idTempPre = 0;
				var MAINTASK =_this.PMFill();
				var index = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(MAINTASK.PMDETAILTASKList, PMDETAILTASK.IDPMDETAILTASK);
				PMDETAILTASK = MAINTASK.PMDETAILTASKList[index];
				for (var i = 0; i < DateGridP._Rows.length; i++) {
					$(DateGridP._Rows[i].This).remove();
				}
				if(PMDETAILTASK.PMRELATIONList.length > 0){
					var RowList = new Array();
					for (var i = 0; i < PMDETAILTASK.PMRELATIONList.length; i++) {
						var NewRow = new TRow();
						var Cell = new TCell();
						Cell.Value = PMDETAILTASK.PMRELATIONList[i].PMDETAILTASK_PREDECESSORS.DETAILTASK_INDEX;
						Cell.IndexColumn = 0;
						Cell.IndexRow = i;
						NewRow.AddCell(Cell);
						Cell = new TCell();
						Cell.Value = PMDETAILTASK.PMRELATIONList[i].PMDETAILTASK_PREDECESSORS.DETAILTASK_NAME;
						Cell.IndexColumn = 0;
						Cell.IndexRow = i;
						NewRow.AddCell(Cell);
						NewRow.Cells[0].This.style.width= '60px';
						NewRow.This.setAttribute('data-id', PMDETAILTASK.PMRELATIONList[i].IDPMRELATION);
						RowList[i] = {row: NewRow.This}
						DateGridP.AddRow(NewRow);
						RowList[i].row.onclick = function () {
							for(var i = 0 ; i < this.parentNode.childElementCount ; i++){this.parentNode.children[i].style.backgroundColor = 'transparent'}
							this.style.backgroundColor = '#BCFFB0';
							idTempPre = parseInt(this.dataset.id)
						}
					}
				}
				else{
					Container.Column[0].This.style.display = 'none';
					$(Container.Column[1].This).removeClass();
					Container.Column[1].This.classList.add('col-xs-12');
					if(PMDETAILTASK.PMRELATION_SUCCESSORSList.length == 0){
						Modal.CloseModal();
						$(Modal.This.This).remove();
					}
				}
				_this.GanttRefresh(component);
			}
		}
		else{
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1"))
		}
	}
	btnRemoveP.This.style.padding = '0px';
	btnRemoveP.This.style.paddingLeft = '10px';
	btnRemoveP.This.style.float = 'right';

	var ContainerTitleSuc = new TVclStackPanel(Container.Column[1].This, '', 1, [[12], [12], [12], [12], [12]]);
	ContainerTitleSuc.Row.This.style.textAlign = 'center';
	ContainerTitleSuc.Row.This.style.marginTop = '5px';
	ContainerTitleSuc.Row.This.style.marginBottom = '10px';
	ContainerTitleSuc.Row.This.style.fontSize = '20px';
	var lblTitleSuc = new TVcllabel(ContainerTitleSuc.Column[0].This, '', TlabelType.H0);
	lblTitleSuc.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblTitleSuc.Text");
	lblTitleSuc.VCLType = TVCLType.BS;
	
	if(PMDETAILTASK.PMRELATION_SUCCESSORSList.length > 0){
		var DateGridS = new TGrid(Container.Column[1].This, '', '');
		var Col0 = new TColumn();
		Col0.Name = ""; 
		Col0.Caption = UsrCfg.Traslate.GetLangText(_this.Mythis, "INDEX");
		Col0.Index = 0; 
		Col0.EnabledEditor = false;
		Col0.DataType = SysCfg.DB.Properties.TDataType.String;
		Col0.ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.None;
		DateGridS.AddColumn(Col0);

		var Col1 = new TColumn();
		Col1.Name = ""; 
		Col1.Caption = UsrCfg.Traslate.GetLangText(_this.Mythis, "NAME");
		Col1.Index = 0; 
		Col1.EnabledEditor = false;
		Col1.DataType = SysCfg.DB.Properties.TDataType.String;
		Col1.ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.None;
		DateGridS.AddColumn(Col1);

		DateGridS.This.children[0].style.backgroundColor = '#00a65a';
		DateGridS.This.children[0].style.textAlign = 'center';
		for(var i = 0 ; i < DateGridS.This.children[0].children[0].childElementCount; i++){
			DateGridS.This.children[0].children[0].children[i].style.textAlign = 'center';
		}
		DateGridS.This.style.textAlign = 'center';
		DateGridS.EnabledResizeColumn = false;
		var RowList = new Array();
		for (var i = 0; i < PMDETAILTASK.PMRELATION_SUCCESSORSList.length; i++) {
			var NewRow = new TRow();
			var Cell = new TCell();
			Cell.Value = PMDETAILTASK.PMRELATION_SUCCESSORSList[i].PMDETAILTASK.DETAILTASK_INDEX;
			Cell.IndexColumn = 0;
			Cell.IndexRow = i;
			NewRow.AddCell(Cell);

			Cell = new TCell();
			Cell.Value = PMDETAILTASK.PMRELATION_SUCCESSORSList[i].PMDETAILTASK.DETAILTASK_NAME;
			Cell.IndexColumn = 0;
			Cell.IndexRow = i;
			NewRow.AddCell(Cell);

			NewRow.Cells[0].This.style.width= '60px';
			NewRow.This.setAttribute('data-id', PMDETAILTASK.PMRELATION_SUCCESSORSList[i].IDPMRELATION);
			RowList[i] = {row: NewRow.This}
			DateGridS.AddRow(NewRow);
			RowList[i].row.onclick = function () {
				for(var i = 0 ; i < this.parentNode.childElementCount ; i++){this.parentNode.children[i].style.backgroundColor = 'transparent'}
				this.style.backgroundColor = '#BCFFB0';
				idTempSuc = parseInt(this.dataset.id)
			}
		}
	}
	else{
		Container.Column[1].This.style.display = 'none';
		$(Container.Column[0].This).removeClass();
		Container.Column[0].This.classList.add('col-xs-12');
		Container.Column[0].This.style.borderRight = '';
		if(PMDETAILTASK.PMRELATIONList.length > 0){
			Modal.ShowModal();
		}
	}

	if(PMDETAILTASK.PMRELATIONList.length > 0 && PMDETAILTASK.PMRELATION_SUCCESSORSList.length > 0){
		Modal.ShowModal();
	}

	var ContainerBtnS = new TVclStackPanel(Container.Column[1].This, "",  1, [[12], [12], [12], [12], [12]]);
	ContainerBtnS.Row.This.style.marginBottom = '10px';
	ContainerBtnS.Column[0].This.style.textAlign = 'right';
	var btnRemoveS = new TVclButton(ContainerBtnS.Column[0].This, "");
	btnRemoveS.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnRemove.Text");
	btnRemoveS.VCLType = TVCLType.BS;
	btnRemoveS.Src = "image/16/delete.png";
	btnRemoveS.onClick = function () {
		if(idTempSuc != 0){
			var success = Persistence.ProjectManagement.Methods.PMRELATION_DEL(parseInt(idTempSuc));
			if(success){
				idTempSuc = 0;
				var MAINTASK =_this.PMFill();
				var index = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(MAINTASK.PMDETAILTASKList, PMDETAILTASK.IDPMDETAILTASK);
				PMDETAILTASK = MAINTASK.PMDETAILTASKList[index];
				for (var i = 0; i < DateGridS._Rows.length; i++) {
					$(DateGridS._Rows[i].This).remove();
				}
				if(PMDETAILTASK.PMRELATIONList.length > 0){
					var RowList = new Array();
					for (var i = 0; i < PMDETAILTASK.PMRELATION_SUCCESSORSList.length; i++) {
						var NewRow = new TRow();

						var Cell = new TCell();
						Cell.Value = PMDETAILTASK.PMRELATION_SUCCESSORSList[i].PMDETAILTASK.DETAILTASK_INDEX;
						Cell.IndexColumn = 0;
						Cell.IndexRow = i;
						NewRow.AddCell(Cell);

						Cell = new TCell();
						Cell.Value = PMDETAILTASK.PMRELATION_SUCCESSORSList[i].PMDETAILTASK.DETAILTASK_NAME;
						Cell.IndexColumn = 0;
						Cell.IndexRow = i;
						NewRow.AddCell(Cell);

						NewRow.Cells[0].This.style.width= '60px';
						NewRow.This.setAttribute('data-id', PMDETAILTASK.PMRELATION_SUCCESSORSList[i].IDPMRELATION);
						RowList[i] = {row: NewRow.This}
						DateGridS.AddRow(NewRow);
						RowList[i].row.onclick = function () {
							for(var i = 0 ; i < this.parentNode.childElementCount ; i++){this.parentNode.children[i].style.backgroundColor = 'transparent'}
							this.style.backgroundColor = '#BCFFB0';
							idTempSuc = parseInt(this.dataset.id)
						}
					}
				}
				else{
					Container.Column[0].This.style.display = 'none';
					$(Container.Column[1].This).removeClass();
					Container.Column[1].This.classList.add('col-xs-12');
					if(PMDETAILTASK.PMRELATION_SUCCESSORSList.length == 0){
						Modal.CloseModal();
						$(Modal.This.This).remove();
					}
				}
				_this.GanttRefresh(component);
			}
		}
		else{
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1"));
		}
	}
	btnRemoveS.This.style.padding = '0px';
	btnRemoveS.This.style.paddingLeft = '10px';
	btnRemoveS.This.style.float = 'right';

	Modal.FunctionAfterClose = function () {
		$(this.This.This).remove();
	}
}

ItHelpCenter.Demo.TDemoPMGantt.prototype.ResourcesAllocation = function(PMDETAILTASK, component){
	var _this = this.TParent();
	var idTemp = 0;
	var MAINTASK =_this.PMFill();
	var Modal = new TVclModal(document.body, null, "");
	Modal.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines11"));
	function MediaQuery(x) {
		if (x.matches) { // menos de 780px
			Modal.This.This.style.paddingRight = '0%';
			Modal.This.This.style.paddingLeft = '0%';
		}
		else {// mayor de 780px
			Modal.This.This.style.paddingRight = '20%';
			Modal.This.This.style.paddingLeft = '20%';
		}
	}
	var x = window.matchMedia("(max-width: 700px)");
	MediaQuery(x);
	x.addListener(MediaQuery);

	var Container = new TVclStackPanel(Modal.Body.This, 'Resources_Form', 1, [[12], [12], [12], [12], [12]]);
	
	var ContResources = new TVclStackPanel(Container.Column[0].This, "", 2, [[4, 8], [2, 10], [2, 10], [2, 10], [2, 8]]);
	ContResources.Row.This.style.marginBottom = '5px';
	var lblResources = new TVcllabel(ContResources.Column[0].This, "", TlabelType.H0);
	lblResources.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblResources.Text");
	lblResources.VCLType = TVCLType.BS;
	var cbResources = new TVclComboBox2(ContResources.Column[1].This, "");
	cbResources.VCLType = TVCLType.BS;
	for(var i = 0 ; i < MAINTASK.PMRESOURCESList.length; i++){
		var status = true;
		for( var j = 0 ; j < PMDETAILTASK.PMRESOURCESALLOCATIONList.length; j++ ){
			if(MAINTASK.PMRESOURCESList[i].IDPMRESOURCES == PMDETAILTASK.PMRESOURCESALLOCATIONList[j].IDPMRESOURCES){
				status = false;
			}
		}
		if(status){
			var ComboItem = new TVclComboBoxItem();
			ComboItem.Value = MAINTASK.PMRESOURCESList[i].IDPMRESOURCES;
			ComboItem.Text = MAINTASK.PMRESOURCESList[i].RESOURCES_NAME;
			ComboItem.Tag = "TestResources";
			cbResources.AddItem(ComboItem);
		}
	}
	cbResources.selectedIndex = 0;

	var ContTextUnits = new TVclStackPanel(Container.Column[0].This, "", 2, [[4, 8], [2, 10], [2, 10], [2, 10], [2, 8]]);
	ContTextUnits.Row.This.style.marginBottom = '10px';
	var lblUnits = new TVcllabel(ContTextUnits.Column[0].This, "", TlabelType.H0);
	lblUnits.Text =  UsrCfg.Traslate.GetLangText(_this.Mythis, "lblUnits.Text");
	lblUnits.VCLType = TVCLType.BS;
	var txtUnits = new TVclTextBox(ContTextUnits.Column[1].This, "");
	txtUnits.Text = "";
	txtUnits.VCLType = TVCLType.BS;

	var ContDescription = new TVclStackPanel(Container.Column[0].This, "", 2, [[4, 8], [2, 10], [2, 10], [2, 10], [2, 8]]);
	ContDescription.Row.This.style.marginBottom = '10px';
	var lblDescription = new TVcllabel(ContDescription.Column[0].This, "", TlabelType.H0);
	lblDescription.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblDescription.Text");
	lblDescription.VCLType = TVCLType.BS;
	var memoDescription = new TVclMemo(ContDescription.Column[1].This, "txtDescription");
	memoDescription.VCLType = TVCLType.BS;
	memoDescription.This.removeAttribute("cols");
	memoDescription.This.removeAttribute("rows");
	memoDescription.This.classList.add("form-control");
	memoDescription.Text = '';

	var ContainerBtnAdd = new TVclStackPanel(Container.Column[0].This, "1",  1, [[12], [12], [12], [12], [12]]);
	ContainerBtnAdd.Row.This.style.marginBottom = '5px';
	ContainerBtnAdd.Column[0].This.style.textAlign = 'right';
	var btnAssign = new TVclButton(ContainerBtnAdd.Column[0].This, "");
	btnAssign.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnAssign.Text");
	btnAssign.VCLType = TVCLType.BS;
	btnAssign.This.style.width = '98px';
	btnAssign.Src = "image/16/add.png";
	btnAssign.onClick = function () {
		var PMRESOURCESALLOCATION = new Persistence.ProjectManagement.Properties.TPMRESOURCESALLOCATION();
		PMRESOURCESALLOCATION.IDPMDETAILTASK = PMDETAILTASK.IDPMDETAILTASK;
		PMRESOURCESALLOCATION.IDPMRESOURCES = parseInt(cbResources.Value);
		PMRESOURCESALLOCATION.RESOURCESALLOCATION_UNITS = txtUnits.Text;
		PMRESOURCESALLOCATION.RESOURCESALLOCATION_DESCRIPTION = memoDescription.Text ;
		var success = Persistence.ProjectManagement.Methods.PMRESOURCESALLOCATION_ADD(PMRESOURCESALLOCATION);
		if(success){
			var MAINTASK =_this.PMFill();
			var index = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(MAINTASK.PMDETAILTASKList, PMDETAILTASK.IDPMDETAILTASK);
			PMDETAILTASK = MAINTASK.PMDETAILTASKList[index];
			for (var i = 0; i < DateGrid._Rows.length; i++) {
				$(DateGrid._Rows[i].This).remove();
			}
			var RowList = new Array();
			for(var i = 0 ; i < PMDETAILTASK.PMRESOURCESALLOCATIONList.length; i++){
				var NewRow = new TRow();

				var Cell = new TCell();
				Cell.Value = PMDETAILTASK.PMRESOURCESALLOCATIONList[i].PMRESOURCES.RESOURCES_NAME;
				Cell.IndexColumn = 0;
				Cell.IndexRow = i;
				NewRow.AddCell(Cell);

				Cell = new TCell();
				Cell.Value = PMDETAILTASK.PMRESOURCESALLOCATIONList[i].RESOURCESALLOCATION_UNITS;
				Cell.IndexColumn = 1;
				Cell.IndexRow = i;
				NewRow.AddCell(Cell);

				Cell = new TCell();
				Cell.Value = PMDETAILTASK.PMRESOURCESALLOCATIONList[i].RESOURCESALLOCATION_DESCRIPTION;
				Cell.IndexColumn = 2;
				Cell.IndexRow = i;
				NewRow.AddCell(Cell);

				NewRow.This.setAttribute('data-id', PMDETAILTASK.PMRESOURCESALLOCATIONList[i].IDPMRESOURCESALLOCATION);
				RowList[i] = {row: NewRow.This}
				DateGrid.AddRow(NewRow);
				RowList[i].row.onclick = function () {
					for(var i = 0 ; i < this.parentNode.childElementCount ; i++){
						this.parentNode.children[i].style.backgroundColor = 'transparent';
					}
					this.style.backgroundColor = '#BCFFB0';
					idTemp = parseInt(this.dataset.id)
				}
			}
			cbResources.ClearItems();

			for(var i = 0 ; i < MAINTASK.PMRESOURCESList.length; i++){
				var status = true;
				for( var j = 0 ; j < PMDETAILTASK.PMRESOURCESALLOCATIONList.length; j++ ){
					if(MAINTASK.PMRESOURCESList[i].IDPMRESOURCES == PMDETAILTASK.PMRESOURCESALLOCATIONList[j].IDPMRESOURCES){
						status = false;
					}
				}
				if(status){
					var ComboItem = new TVclComboBoxItem();
					ComboItem.Value = MAINTASK.PMRESOURCESList[i].IDPMRESOURCES;
					ComboItem.Text = MAINTASK.PMRESOURCESList[i].RESOURCES_NAME;
					ComboItem.Tag = "TestResources";
					cbResources.AddItem(ComboItem);
				}
			}
			cbResources.selectedIndex = 0;
			_this.GanttRefresh(component);
		}
	}
	btnAssign.This.style.padding = '0px';
	btnAssign.This.style.paddingLeft = '10px';

	var DateGrid = new TGrid(Container.Column[0].This, '', '');
	var Col0 = new TColumn();
	Col0.Name = ""; 
	Col0.Caption = UsrCfg.Traslate.GetLangText(_this.Mythis, "RESOURCE NAME");
	Col0.Index = 0; 
	Col0.EnabledEditor = false;
	Col0.DataType = SysCfg.DB.Properties.TDataType.String;
	Col0.ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.None;
	DateGrid.AddColumn(Col0);

	var Col1 = new TColumn();
	Col1.Name = ""; 
	Col1.Caption = UsrCfg.Traslate.GetLangText(_this.Mythis, "UNITS");
	Col1.Index = 1; 
	Col1.EnabledEditor = false;
	Col1.DataType = SysCfg.DB.Properties.TDataType.String;
	Col1.ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.None;
	DateGrid.AddColumn(Col1);

	var Col2 = new TColumn();
	Col2.Name = ""; 
	Col2.Caption = UsrCfg.Traslate.GetLangText(_this.Mythis, "DESCRIPTION");
	Col2.Index = 2; 
	Col2.EnabledEditor = false;
	Col2.DataType = SysCfg.DB.Properties.TDataType.String;
	Col2.ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.None;
	DateGrid.AddColumn(Col2);

	DateGrid.This.children[0].style.backgroundColor = '#00a65a';
	DateGrid.This.children[0].style.textAlign = 'center';
	for(var i = 0 ; i < DateGrid.This.children[0].children[0].childElementCount; i++){
		DateGrid.This.children[0].children[0].children[i].style.textAlign = 'center';
	}
	DateGrid.This.style.textAlign = 'center';
	DateGrid.EnabledResizeColumn = false;
	var RowList = new Array();
	for(var i = 0 ; i < PMDETAILTASK.PMRESOURCESALLOCATIONList.length; i++){
		var NewRow = new TRow();

		var Cell = new TCell();
		Cell.Value = PMDETAILTASK.PMRESOURCESALLOCATIONList[i].PMRESOURCES.RESOURCES_NAME;
		Cell.IndexColumn = 0;
		Cell.IndexRow = i;
		NewRow.AddCell(Cell);

		Cell = new TCell();
		Cell.Value = PMDETAILTASK.PMRESOURCESALLOCATIONList[i].RESOURCESALLOCATION_UNITS;
		Cell.IndexColumn = 1;
		Cell.IndexRow = i;
		NewRow.AddCell(Cell);

		Cell = new TCell();
		Cell.Value = PMDETAILTASK.PMRESOURCESALLOCATIONList[i].RESOURCESALLOCATION_DESCRIPTION;
		Cell.IndexColumn = 2;
		Cell.IndexRow = i;
		NewRow.AddCell(Cell);

		NewRow.This.setAttribute('data-id', PMDETAILTASK.PMRESOURCESALLOCATIONList[i].IDPMRESOURCESALLOCATION);
		RowList[i] = {row: NewRow.This}
		DateGrid.AddRow(NewRow);
		RowList[i].row.onclick = function () {
			for(var i = 0 ; i < this.parentNode.childElementCount ; i++){
				this.parentNode.children[i].style.backgroundColor = 'transparent';
			}
			this.style.backgroundColor = '#BCFFB0';
			idTemp = parseInt(this.dataset.id)
		}
	}


	var ContainerBtnRemove = new TVclStackPanel(Container.Column[0].This, "",  1, [[12], [12], [12], [12], [12]]);
	ContainerBtnRemove.Row.This.style.marginBottom = '10px';
	ContainerBtnRemove.Column[0].This.style.textAlign = 'right';
	var btnRemove = new TVclButton(ContainerBtnRemove.Column[0].This, "");
	btnRemove.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnRemove.Text");
	btnRemove.VCLType = TVCLType.BS;
	btnRemove.This.style.width = '98px';
	btnRemove.Src = "image/16/delete.png";
	btnRemove.onClick = function () {
		if(idTemp != 0){
			var success = Persistence.ProjectManagement.Methods.PMRESOURCESALLOCATION_DEL(idTemp);
			if(success){
				idTemp = 0;
				var MAINTASK =_this.PMFill();
				var index = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(MAINTASK.PMDETAILTASKList, PMDETAILTASK.IDPMDETAILTASK);
				PMDETAILTASK = MAINTASK.PMDETAILTASKList[index];
				for (var i = 0; i < DateGrid._Rows.length; i++) {
					$(DateGrid._Rows[i].This).remove();
				}
				$(cbResources.Options[cbResources.selectedIndex].This).remove();
				var RowList = new Array();
				for(var i = 0 ; i < PMDETAILTASK.PMRESOURCESALLOCATIONList.length; i++){
					var NewRow = new TRow();

					var Cell = new TCell();
					Cell.Value = PMDETAILTASK.PMRESOURCESALLOCATIONList[i].PMRESOURCES.RESOURCES_NAME;
					Cell.IndexColumn = 0;
					Cell.IndexRow = i;
					NewRow.AddCell(Cell);

					Cell = new TCell();
					Cell.Value = PMDETAILTASK.PMRESOURCESALLOCATIONList[i].RESOURCESALLOCATION_UNITS;
					Cell.IndexColumn = 1;
					Cell.IndexRow = i;
					NewRow.AddCell(Cell);

					Cell = new TCell();
					Cell.Value = PMDETAILTASK.PMRESOURCESALLOCATIONList[i].RESOURCESALLOCATION_DESCRIPTION;
					Cell.IndexColumn = 2;
					Cell.IndexRow = i;
					NewRow.AddCell(Cell);

					NewRow.This.setAttribute('data-id', PMDETAILTASK.PMRESOURCESALLOCATIONList[i].IDPMRESOURCESALLOCATION);
					RowList[i] = {row: NewRow.This}
					DateGrid.AddRow(NewRow);
					RowList[i].row.onclick = function () {
						for(var i = 0 ; i < this.parentNode.childElementCount ; i++){
							this.parentNode.children[i].style.backgroundColor = 'transparent';
						}
						this.style.backgroundColor = '#BCFFB0';
						idTemp = parseInt(this.dataset.id)
					}
				}

				cbResources.ClearItems();

				for(var i = 0 ; i < MAINTASK.PMRESOURCESList.length; i++){
					var status = true;
					for( var j = 0 ; j < PMDETAILTASK.PMRESOURCESALLOCATIONList.length; j++ ){
						if(MAINTASK.PMRESOURCESList[i].IDPMRESOURCES == PMDETAILTASK.PMRESOURCESALLOCATIONList[j].IDPMRESOURCES){
							status = false;
						}
					}
					if(status){
						var ComboItem = new TVclComboBoxItem();
						ComboItem.Value = MAINTASK.PMRESOURCESList[i].IDPMRESOURCES;
						ComboItem.Text = MAINTASK.PMRESOURCESList[i].RESOURCES_NAME;
						ComboItem.Tag = "TestResources";
						cbResources.AddItem(ComboItem);
					}
				}
				cbResources.selectedIndex = 0;
				_this.GanttRefresh(component);
			}

		}
		else{
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines12"));
		}
	}
	btnRemove.This.style.padding = '0px';
	btnRemove.This.style.paddingLeft = '10px';

	Modal.ShowModal();
	Modal.FunctionAfterClose = function () {
		$(this.This.This).remove();
	}
}

ItHelpCenter.Demo.TDemoPMGantt.prototype.CriticalPath = function(component){
	var _this = this.TParent();
	// timeES(tiempo de inicio temprano), timeEF(tiempo de terminación temprano), timeLS(tiempo de inicio tardio, timeLF(tiempo de terminación tardio)
	var MAINTASK =_this.PMFill();
	var taskList = new Array();
	var slackList = new Array();
	var criticalPath = new Array();
	var parentList = new Array();
	_this.CriticalPath_TimeEnd = 0;
	for(var i = 0; i < MAINTASK.PMDETAILTASKList.length; i++){
		if(MAINTASK.PMDETAILTASKList[i].PMDETAILTASK_CHILDList.length == 0){
			if(MAINTASK.PMDETAILTASKList[i].PMRELATIONList.length == 0){
				_this.GetEarlyTime(MAINTASK.PMDETAILTASKList[i], taskList, 0);
			}
		}
	}
	for(var i = 0; i < taskList.length; i++){
		if(taskList[i].successorTask == 'end'){
			var index = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(MAINTASK.PMDETAILTASKList, taskList[i].idTask);
			_this.GetLateTime(MAINTASK.PMDETAILTASKList[index], taskList, _this.CriticalPath_TimeEnd)
		}
	}
	// Obtener ruta critica a partir de la holgura = 0;
	for(var i = 0; i < taskList.length; i++){
		if((taskList[i].timeLF - taskList[i].timeEF) == 0){
			var index = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(MAINTASK.PMDETAILTASKList, taskList[i].idTask);
			slackList.push(MAINTASK.PMDETAILTASKList[index]);
			_this.GetParent(MAINTASK.PMDETAILTASKList, MAINTASK.PMDETAILTASKList[index], parentList);
		}
	}
	//Unir los arrays y ordenar
	criticalPath = slackList.concat(parentList);
	criticalPath.sort(function (a, b) { return a.DETAILTASK_INDEX - b.DETAILTASK_INDEX; });
	// Grafica Gantt
	component.CleanData(); // Limpiar data y luego almacenar nuevos datos
	for(var i = 0 ;  i < criticalPath.length ; i++){
		_this.SaveRecord(criticalPath[i],component);
	}
	component.Refresh();
}

ItHelpCenter.Demo.TDemoPMGantt.prototype.GetParent = function(detailList, detail, parentList) {
	var _this = this.TParent();
	if(detail.IDPMDETAILTASK_PARENT != 0){
		var index = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(detailList, detail.IDPMDETAILTASK_PARENT);
		var status = true;
		for(var i = 0; i < parentList.length ; i++){
			if(detailList[index].IDPMDETAILTASK == parentList[i].IDPMDETAILTASK){
				status = false
			}
		}
		if(status){
			parentList.push(detailList[index]);
		}

		_this.GetParent(detailList, detailList[index], parentList);
	}
}

ItHelpCenter.Demo.TDemoPMGantt.prototype.GetEarlyTime = function(task, earlyTimeList, es){
	var _this = this.TParent();
	if(task.PMRELATION_SUCCESSORSList.length == 0){
		var ef = (es + parseInt(task.DETAILTASK_DURATION));
		var status = true;
		var indexEnd = 0;
		for(var i = 0 ; i < earlyTimeList.length; i++){
			if(earlyTimeList[i].idTask == task.IDPMDETAILTASK){
				status = false;
				indexEnd = i;
			}
		}
		if(status){
			earlyTimeList.push({idTask:task.IDPMDETAILTASK, successorTask: 'end', timeES: es, timeEF: ef, timeLS: -1, timeLF: -1});
			_this.CriticalPath_TimeEnd = ef;
		}
		else{
			if(ef > earlyTimeList[indexEnd].timeEF){
				earlyTimeList[indexEnd].timeEF = ef;
				_this.CriticalPath_TimeEnd = ef;
			}
		}
	}
	else if(task.PMRELATION_SUCCESSORSList.length > 0){
		var ef = (es + parseInt(task.DETAILTASK_DURATION));
		var status = true;
		var indexEnd = 0;
		for(var i = 0 ; i < earlyTimeList.length; i++){
			if(earlyTimeList[i].idTask == task.IDPMDETAILTASK){
				status = false;
				indexEnd = i;
			}
		}
		if(status){
			earlyTimeList.push({idTask:task.IDPMDETAILTASK, successorTask: '', timeES: es, timeEF: ef, timeLS: -1, timeLF: -1});
		}
		else{
			if(ef > earlyTimeList[indexEnd].timeEF){
				earlyTimeList[indexEnd].timeEF = ef;
			}
		}

		_this.GetEarlyTime(task.PMRELATION_SUCCESSORSList[0].PMDETAILTASK, earlyTimeList, ef);
		for(var i = 1 ; i < task.PMRELATION_SUCCESSORSList.length ; i++){
			_this.GetEarlyTime(task.PMRELATION_SUCCESSORSList[i].PMDETAILTASK, earlyTimeList, ef);
		}
	}
}

ItHelpCenter.Demo.TDemoPMGantt.prototype.GetLateTime = function(task, lateTimeList, lf){
	var _this = this.TParent();
	if(task.PMRELATIONList.length == 0){
		var ls = (lf - parseInt(task.DETAILTASK_DURATION));
		for(var i = 0 ; i < lateTimeList.length; i++){
			if(lateTimeList[i].idTask == task.IDPMDETAILTASK){
				if(lateTimeList[i].timeLF == -1){
					lateTimeList[i].timeLF = lf;
					lateTimeList[i].timeLS = ls;
				}
				else{
					if(lf < lateTimeList[i].timeLF){
						lateTimeList[i].timeLF = lf;
						lateTimeList[i].timeLS = ls;
					}
				}
			}
		}
	}
	else if(task.PMRELATIONList.length > 0){
		var ls = (lf - parseInt(task.DETAILTASK_DURATION));
		for(var i = 0 ; i < lateTimeList.length; i++){
			if(lateTimeList[i].idTask == task.IDPMDETAILTASK){
				if(lateTimeList[i].timeLF == -1){
					lateTimeList[i].timeLF = lf;
					lateTimeList[i].timeLS = ls;
				}
				else{
					if(lf < lateTimeList[i].timeLF){
						lateTimeList[i].timeLF = lf;
						lateTimeList[i].timeLS = ls;
					}
				}
			}
		}
		_this.GetLateTime(task.PMRELATIONList[0].PMDETAILTASK_PREDECESSORS, lateTimeList, ls);
		for(var i = 1 ; i < task.PMRELATIONList.length ; i++){
			_this.GetLateTime(task.PMRELATIONList[i].PMDETAILTASK_PREDECESSORS, lateTimeList, ls);
		}
	}
}

ItHelpCenter.Demo.TDemoPMGantt.prototype.PMFill = function(){
	var _this = this.TParent();
	_this.PManagement.Fill();
	_this.MainIndex = Persistence.ProjectManagement.Methods.PMMAINTASK_ListGetIndex(_this.PManagement.PMMAINTASKList, _this.ID)
	return _this.PManagement.PMMAINTASKList[_this.MainIndex];
}

ItHelpCenter.Demo.TDemoPMGantt.prototype.GanttRefresh = function(component){
	var _this = this.TParent();
	_this.UPDBaseLineProject(component);
	if(_this.CtrlBaseLine){
		_this.CriticalPath(_this.Component);
	}else{
		var MAINTASK =_this.PMFill();
		component.CleanData(); // Limpiar data y luego almacenar nuevos datos
		for(var i = 0 ;  i < MAINTASK.PMDETAILTASKPARENTList.length ; i++){
			_this.GetChild(MAINTASK.PMDETAILTASKPARENTList[i], component);
		}
		component.Refresh();
	}
	if(_this.CtrlBaseLine){
		component.GenerateBaseLine();
	}
}

ItHelpCenter.Demo.TDemoPMGantt.prototype.UPDBaseLineProject = function(component){
	var _this = this.TParent();
	var MAINTASK =_this.PMFill();
	var PMHoliday = MAINTASK.PMHOLIDAYList;
	var startDate = new Date(_this.MinStartDate(MAINTASK.PMDETAILTASKPARENTList));
	var endDate = new Date(_this.MaxEndDate(MAINTASK.PMDETAILTASKPARENTList));
	// MAINTASK.MAINTASK_DURATION = duration;
	var duration = _this.CalculateDuration(MAINTASK, PMHoliday, startDate, endDate);
	if(parseInt(duration) > 1){
		duration += ' ' + UsrCfg.Traslate.GetLangText(_this.Mythis, "DURATION2");
	}else{
		duration += ' ' + UsrCfg.Traslate.GetLangText(_this.Mythis, "DURATION1");
	}

	MAINTASK.MAINTASK_DURATION = duration;
	MAINTASK.MAINTASK_STARTDATE = new Date(_this.MinStartDate(MAINTASK.PMDETAILTASKPARENTList));
	MAINTASK.MAINTASK_ENDDATE = new Date(_this.MaxEndDate(MAINTASK.PMDETAILTASKPARENTList));		
	var success = Persistence.ProjectManagement.Methods.PMMAINTASK_UPD(MAINTASK);
	if(success){
		component.ProjectDuration = MAINTASK.MAINTASK_DURATION;
		component.ScaleMinimum = MAINTASK.MAINTASK_STARTDATE;
		component.ScaleMaximum = MAINTASK.MAINTASK_ENDDATE;
	}
}

ItHelpCenter.Demo.TDemoPMGantt.prototype.CalculateDuration = function(MAINTASK, PMHoliday, startDate, endDate){
	var _this = this.TParent();
	var diff = endDate.getTime() - startDate.getTime();
	var res = diff/(1000*60*60*24);
	var duration = 0;
	var initDate = new Date(startDate);
	if(MAINTASK.MAINTASK_WEEKENDHOLIDAY){
		for(var i = 0 ; i < res ; i++){
			var statusHoliday = false;
			if(initDate.getDay() != 0 && initDate.getDay() != 6){
				for (var j = 0; j < PMHoliday.length; j++) {
					if (initDate.getMonth() == new Date(PMHoliday[j].HOLIDAY_DATE).getMonth() && initDate.getDate() == new Date(PMHoliday[j].HOLIDAY_DATE).getDate()) {
						statusHoliday = true;
					}
				}
				if(!statusHoliday){
					duration++;
				}
			}
			var dateTemp = new Date(initDate.getTime() + (1000*60*60*24));
			initDate = dateTemp;
		}
	}
	else if(MAINTASK.MAINTASK_SUNDAYHOLIDAY){
		for(var i = 0 ; i < res ; i++){
			var statusHoliday = false;
			if(initDate.getDay() != 0){
				for (var j = 0; j < PMHoliday.length; j++) {
					if (initDate.getMonth() == new Date(PMHoliday[j].HOLIDAY_DATE).getMonth() && initDate.getDate() == new Date(PMHoliday[j].HOLIDAY_DATE).getDate()) {
						statusHoliday = true;
					}
				}
				if(!statusHoliday){
					duration++;
				}
			}
			var dateTemp = new Date(initDate.getTime() + (1000*60*60*24));
			initDate = dateTemp;
		}
	}
	else{
		for(var i = 0 ; i < res ; i++){
			var statusHoliday = false;
			for (var j = 0; j < PMHoliday.length; j++) {
				if (initDate.getMonth() == new Date(PMHoliday[j].HOLIDAY_DATE).getMonth() && initDate.getDate() == new Date(PMHoliday[j].HOLIDAY_DATE).getDate()) {
					statusHoliday = true;
				}
			}
			if(!statusHoliday){
				duration++;
			}
			var dateTemp = new Date(initDate.getTime() + (1000*60*60*24));
			initDate = dateTemp;
		}
	}
	return duration;
}

ItHelpCenter.Demo.TDemoPMGantt.prototype.CalculateEndDate = function(MAINTASK, PMHoliday, startDate, duration){
	var _this = this.TParent();
	duration = duration-1;
	// var initDate = new Date(startDate.getTime() + (1000*60*60*12));
	var initDate = new Date(startDate);
	if(MAINTASK.MAINTASK_WEEKENDHOLIDAY){
		var  i = 0;
		while(i < duration){
			var statusHoliday = false;
			var dateTemp = new Date(initDate.getTime() + (1000*60*60*24));
			initDate = dateTemp;
			if(initDate.getDay() != 0 && initDate.getDay() != 6){
				for (var j = 0; j < PMHoliday.length; j++) {
					if (initDate.getMonth() == new Date(PMHoliday[j].HOLIDAY_DATE).getMonth() && initDate.getDate() == new Date(PMHoliday[j].HOLIDAY_DATE).getDate()) {
						statusHoliday = true;
					}
				}
				if(!statusHoliday){
					i++;
				}
			}
		}
	}
	else if(MAINTASK.MAINTASK_SUNDAYHOLIDAY){
		var  i = 0;
		while(i < duration){
			var statusHoliday = false;
			var dateTemp = new Date(initDate.getTime() + (1000*60*60*24));
			initDate = dateTemp;
			if(initDate.getDay() != 0){
				for (var j = 0; j < PMHoliday.length; j++) {
					if (initDate.getMonth() == new Date(PMHoliday[j].HOLIDAY_DATE).getMonth() && initDate.getDate() == new Date(PMHoliday[j].HOLIDAY_DATE).getDate()) {
						statusHoliday = true;
					}
				}
				if(!statusHoliday){
					i++;
				}
			}
		}
	}
	else{
		var  i = 0;
		while(i < duration){
			var statusHoliday = false;
			var dateTemp = new Date(initDate.getTime() + (1000*60*60*24));
			initDate = dateTemp;
			for (var j = 0; j < PMHoliday.length; j++) {
				if (initDate.getMonth() == new Date(PMHoliday[j].HOLIDAY_DATE).getMonth() && initDate.getDate() == new Date(PMHoliday[j].HOLIDAY_DATE).getDate()) {
					statusHoliday = true;
				}
			}
			if(!statusHoliday){
				i++;
			}
		}
	}
	return initDate;
}

ItHelpCenter.Demo.TDemoPMGantt.prototype.SubtractDate = function(MAINTASK, PMHoliday, startDate, duration){
	var _this = this.TParent();
	var initDate = new Date(startDate);
	if(MAINTASK.MAINTASK_WEEKENDHOLIDAY){
		var  i = 0;
		while(i < duration){
			var statusHoliday = false;
			var dateTemp = new Date(initDate.getTime() - (1000*60*60*24));
			initDate = dateTemp;
			if(initDate.getDay() != 0 && initDate.getDay() != 6){
				for (var j = 0; j < PMHoliday.length; j++) {
					if (initDate.getMonth() == new Date(PMHoliday[j].HOLIDAY_DATE).getMonth() && initDate.getDate() == new Date(PMHoliday[j].HOLIDAY_DATE).getDate()) {
						statusHoliday = true;
					}
				}
				if(!statusHoliday){
					i++;
				}
			}
		}
	}
	else if(MAINTASK.MAINTASK_SUNDAYHOLIDAY){
		var  i = 0;
		while(i < duration){
			var statusHoliday = false;
			var dateTemp = new Date(initDate.getTime() - (1000*60*60*24));
			initDate = dateTemp;
			if(initDate.getDay() != 0){
				for (var j = 0; j < PMHoliday.length; j++) {
					if (initDate.getMonth() == new Date(PMHoliday[j].HOLIDAY_DATE).getMonth() && initDate.getDate() == new Date(PMHoliday[j].HOLIDAY_DATE).getDate()) {
						statusHoliday = true;
					}
				}
				if(!statusHoliday){
					i++;
				}
			}
		}
	}
	else{
		var  i = 0;
		while(i < duration){
			var statusHoliday = false;
			var dateTemp = new Date(initDate.getTime() - (1000*60*60*24));
			initDate = dateTemp;
			for (var j = 0; j < PMHoliday.length; j++) {
				if (initDate.getMonth() == new Date(PMHoliday[j].HOLIDAY_DATE).getMonth() && initDate.getDate() == new Date(PMHoliday[j].HOLIDAY_DATE).getDate()) {
					statusHoliday = true;
				}
			}
			if(!statusHoliday){
				i++;
			}
		}
	}
	return initDate;
}