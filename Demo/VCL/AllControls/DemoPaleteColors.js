﻿ItHelpCenter.Demo.TDemoPaleteColors = function (inObjectHtml, _this) {
    this.TParent = function () {
        return this;
    }.bind(this);
    this.ObjectHtml = inObjectHtml;
    try {
        var Container1 = new TVclStackPanel(this.ObjectHtml, "ContainerInit", 1, [[12], [12], [12], [12], [12]]);
        // Pallete Color //
        var ContainerColor1 = new TVclStackPanel(Container1.Column[0].This, "ContainerColor1", 2, [[12, 12], [3, 9], [3, 9], [3, 9], [3, 9]]);
        var lblColor1 = new TVcllabel(ContainerColor1.Column[0].This, "", TlabelType.H0);
        lblColor1.Text = "Color1";
        lblColor1.VCLType = TVCLType.BS;
       //debugger;
        Source.Style.Methods.SetFontStyleInHtml(lblColor1, Source.Style.Properties.TTypeStyle.Normal);
        //lblColor1.This.style.backgroundColor = "blue";
        //lblColor1.This.style.fontFamily = "calibri";
        //lblColor1.This.style.fontStyle = "normal";
        //lblColor1.This.style.fontWeight = "normal";
        //lblColor1.This.style.fontSize = "11px";
        //lblColor1.This.style.color = "black";
        var ObjColor1 = new Componet.TPalleteColor(ContainerColor1.Column[1].This, "newColor1");
        ObjColor1.CreatePallete();
        var ContainerColorExample1 = new TVclStackPanel(Container1.Column[0].This, "ContainerColorExample1", 1, [[12], [12], [12], [12], [12]]);
        ContainerColorExample1.Row.This.style.height = "200px";
        ObjColor1.ChangeColor = function (objColor) {
            ContainerColorExample1.Row.This.style.background = objColor.Color;
        }
        /////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////
        var ContainerColor2 = new TVclStackPanel(Container1.Column[0].This, "ContainerColor2", 2, [[12, 12], [3, 9], [3, 9], [3, 9], [3, 9]]);
        var lblColor2 = new TVcllabel(ContainerColor2.Column[0].This, "", TlabelType.H0);
        lblColor2.Text = "Color2";
        lblColor2.VCLType = TVCLType.BS;
        Source.Style.Methods.SetFontStyleInHtml(lblColor2.This, Source.Style.Properties.TTypeStyle.Title1);
        var ObjColor2 = new Componet.TPalleteColor(ContainerColor2.Column[1].This, "newColor2");
        ObjColor2.FloatingPallete = true;
        ObjColor2.DeleteColor = false;
        ObjColor2.ShowButtons = false;
        ObjColor2.OptionsPallete = false;
        ObjColor2.ColorInitial = false;
        ObjColor2.CreatePallete();
        var ContainerColorExample2 = new TVclStackPanel(Container1.Column[0].This, "ContainerColorExample2", 1, [[12], [12], [12], [12], [12]]);
        ContainerColorExample2.Row.This.style.height = "200px";
        ObjColor2.MoveColor = function (objColor, Color) {
            ContainerColorExample2.Row.This.style.background = Color;
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoGraphicsPBI.js ItHelpCenter.Demo.TDemoPaleteColors", e);
    }
}