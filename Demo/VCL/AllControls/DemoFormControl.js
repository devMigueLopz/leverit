﻿ItHelpCenter.Demo.TDemoFormControl = function (inObjectHtml, _this) {
    //************* INICIO TAB FORM ***********************************/// 
    var ContForm1 = new TVclStackPanel(inObjectHtml, "1", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
    ContForm1.Row.This.style.marginTop = "20px";

    Vcllabel(ContForm1.Column[0].This, "", TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "lblForm"));

    var btnEsconde = new TVclInputbutton(ContForm1.Column[1].This, "");
    btnEsconde.Text = "Clickeame";
    btnEsconde.VCLType = TVCLType.BS;
    btnEsconde.onClick = function myfunction() {
        $(DivModal[0].This).toggle("slow");
    }

    var DivModal = VclDivitions(ContForm1.Column[1].This, "modal", new Array(1))

    $(DivModal[0].This).toggle("slow");


    var frBasic = new TfrBasic(DivModal[0].This, function (OutStr) {
        alert(OutStr);
    });
    frBasic.Initilize("inic js out");

    var ContForm2 = new TVclStackPanel(inObjectHtml, "1", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
    ContForm2.Row.This.style.marginTop = "20px";

    Vcllabel(ContForm2.Column[0].This, "", TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "lblForm"));

    var btnClickCloseform = new TVclInputbutton(ContForm2.Column[1].This, "");
    btnClickCloseform.Src = "../../../image/16/clear.png";
    btnClickCloseform.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblFormClose");
    btnClickCloseform.VCLType = TVCLType.BS;
    btnClickCloseform.onClick = function () {
        frBasic.Close();
        btnClickCloseform.Enable = false;
    }
    //************* FIN TAB FORM ***********************************/// 
}