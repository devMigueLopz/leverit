ItHelpCenter.Demo.TDemoForums = function (inObjectHtml, forumId, nameId, isManager, callback) {
    this.TParent = function () {
        return this;
    }.bind(this);
    this.ObjectHtml = inObjectHtml;
    this.ForumId = forumId;
    this.NameId = nameId;
    this.IsManager = isManager;
    this.Callback = callback;
    this.Mythis = "TDemoForums";
    UsrCfg.Traslate.GetLangText(this.Mythis, "ADDALLDATAFORUMSLEVERIT", "Add All Data Forums.");
    UsrCfg.Traslate.GetLangText(this.Mythis, "SUCCESSDB", "Forum data was added correctly.");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ERRORDB", "No Forum data was added.");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ALLVALIDATIONDB", "Information already exists in the Forum's database.");
    this.InitializeComponent();
    this.Load();
}
ItHelpCenter.Demo.TDemoForums.prototype.InitializeComponent = function () {
    var _this = this.TParent();
    try {
        var boxbtnAddData = new TVclStackPanel(_this.ObjectHtml, "boxbtnGroupData", 1, [[12], [12], [12], [12], [12]]);
        $(boxbtnAddData.Column[0].This)[0].className += " text-center";
        boxbtnAddData.Column[0].This.style.marginBottom = "5px";
        var btnAddData = new TVclInputbutton(boxbtnAddData.Column[0].This, "btnAddData");
        btnAddData.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ADDALLDATAFORUMSLEVERIT");
        btnAddData.VCLType = TVCLType.BS;
        btnAddData.onClick = function () {
            try {
                _this.CreateAllDataForums(_this, btnAddData);
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("DemoForums.js ItHelpCenter.Demo.TDemoForums.prototype.InitializeComponent btnAddData.onClick", e);
            }
        }

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoForums.js ItHelpCenter.Demo.TDemoForums.prototype.InitializeDesigner", e);
    }
}
ItHelpCenter.Demo.TDemoForums.prototype.Load = function () {
    var _this = this.TParent();
    try {
        $("#DivContrainerMainForum").remove();
        $(_this.ObjectHtml).closest("#DivContrainerMain").attr("id", "DivContrainerMainForum");
        var ForumsProfiler = new ItHelpCenter.TForumsManager.ForumsBasic(_this.ObjectHtml, _this.ForumId, _this.NameId, _this.IsManager, _this.Callback);
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoForums.js ItHelpCenter.Demo.TDemoForums.prototype.Load", e);
    }
}
ItHelpCenter.Demo.TDemoForums.prototype.CreateAllDataForums = function () {
    var _this = this.TParent();
    try {
        var ForumsProfiler = new Persistence.Forums.TForumsProfiler();
        ForumsProfiler.ValidationData();
        if (ForumsProfiler.ForumsList.length <= 0) {
            if (ForumsProfiler.CreateAllData())
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "SUCCESSDB"));
            else
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORDB"));
        }
        else
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ALLVALIDATIONDB"));
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DemoForums.js ItHelpCenter.Demo.TDemoForums.prototype.CreateAllDataForums", e);
    }
}