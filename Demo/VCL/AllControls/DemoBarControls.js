﻿ItHelpCenter.Demo.TDemoBarControls = function (inObjectHtml, _this) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.ObjectHtml = inObjectHtml;
    this.CallbackModalResult = _this;   

    //NUEVO TBarControls /////////////////////////////////////////////////////////////////////////////////////////////////////   
    var topbar = new TBarControls(inObjectHtml, "IDTopBar");   //Indicamos el Contenedor del Barcontrol, el contenedor del cuerpo y el ID del ControlBar
    $(topbar.DivHead).css("border", "1px solid blue"); //border de ejemplo solo para indicar el div   
    $(topbar.ControlBody).css("border", "1px solid grey"); //border de ejemplo solo para indicar el div
    $(topbar.ControlBody).css("min-height", "100px");

    //topbar.ControlBody // este div esta diseñado para que se ingrese todos los elementos que  se deseen
    //tenga interaccion con el BarControls

    /////////////////////////////////////////////////////////////////////////////////////////////////////
    //Este TBarControls le agregaremos 2 contenedores de items
    var toolCont1 = new TBarControls.TToolBar(topbar, "1"); //Indicamos a TBarControls pertenece este ToolBar y un ID
    // toolCont1.BorderColor = "blue"; //Indicamos el color del borde del toolBar, por defecto es grey
    toolCont1.Visible = true;
    toolCont1.Direcction = "Vertical"; //Por defecto el direccionamiento es Horizontal
    toolCont1.Image = "image/24/Home.png";//Como va a estar en direccion vertical, se debe indicar la imagen principal del toolBar

    var toolCont2 = new TBarControls.TToolBar(topbar, "2"); //Indicamos a TBarControls pertenece este ToolBar y un ID
    //toolCont2.EnabledBorder = false; //Indicar que no se vea el borde
    toolCont2.Direcction = "Vertical"; //Por defecto el direccionamiento es Horizontal
    toolCont2.Image = "image/24/link.png";//Como va a estar en direccion vertical, se debe indicar la imagen principal del toolBar
    toolCont2.Width = "150px"; // le doy un tamaño al toolbar

    var toolCont3 = new TBarControls.TToolBar(topbar, "3"); //Indicamos a TBarControls pertenece este ToolBar y un ID
    //toolCont3.EnabledBorder = false; //Indicar que no se vea el borde

    var toolCont4 = new TBarControls.TToolBar(topbar, "4"); //Indicamos a TBarControls pertenece este ToolBar y un ID
    //toolCont4.EnabledBorder = false; //Indicar que no se vea el borde
    //toolCont4.Visible = false;

    var toolCont5 = new TBarControls.TToolBar(topbar, "5"); //Indicamos a TBarControls pertenece este ToolBar y un ID
    //toolCont5.EnabledBorder = false; //Indicar que no se vea el borde
    //toolCont5.Visible = false;

    /////////////////////////////////////////////////////////////////////////////////////////////////////
    //Agregar una imagen btn al primer ToolBar   
    var Imgbtn1 = new TBarControls.TToolBar.ButtonImg(toolCont1, "ID1"); //Indicamos el toolbar, la src de la img y un ID
    Imgbtn1.Image = "image/24/Addressbook.png";
    Imgbtn1.onClick = function () {
        alert("Funcion 1");
    }
    Imgbtn1.EnabledDown = false; //EnabledDown propieda que indica si es un boton tipo check que se marque y desmarque    
    Imgbtn1.Text = "Btn"
    Imgbtn1.Enabled = false;
    //----------------------------------------------------------------------------------------------------------------

    //Agregar una imagen btn al primer ToolBar
    var Imgbtn2 = new TBarControls.TToolBar.ButtonImg(toolCont1, "ID2"); //Indicamos el toolbar, la src de la img y un ID
    //propiedades del boton
    Imgbtn2.Image = "image/24/edit.png";
    Imgbtn2.onClick = function () {
        alert("Funcion 2");
    }
    Imgbtn2.EnabledDown = true; //EnabledDown propieda que indica si es un boton tipo check que se marque y desmarque
    this._textColor = "black"; //black es el color por defecto
    this._hoverColor = "#353e41"; //#353e41 color por defecto para el color del div en el hover
    this._hoverColorLabel = "white"; //white color por defecto para el label en el hover
    //----------------------------------------------------------------------------------------------------------------

    //Agregar una imagen btn al segundo ToolBar   
    var Imgbtn3 = new TBarControls.TToolBar.ButtonImg(toolCont1, "ID3"); //Indicamos el toolbar, la src de la img y un ID
    Imgbtn3.Image = "image/24/add.png";
    Imgbtn3.onClick = function () {
        alert("Funcion 3");
    }
    Imgbtn3.EnabledDown = true; //EnabledDown propieda que indica si es un boton tipo check que se marque y desmarque    
    Imgbtn3.Text = "click me"
    //----------------------------------------------------------------------------------------------------------------

    //Agregar un edit al segundo ToolBar
    var Edit1 = new TBarControls.TToolBar.Edit(toolCont2, "ID1");
    //Edit1.Visible = false; //por defecto siempre es true
    Edit1.Width = "90px"; //por defecto viene en 50px
    Edit1.Text = "Dato"; //por defecto viene vacio
    Edit1.Caption = "Texto"; //Label para el Edit
    //----------------------------------------------------------------------------------------------------------------

    //Agregar un edit al tercer ToolBar
    var Edit2 = new TBarControls.TToolBar.Edit(toolCont2, "ID2");
    //Edit2.Visible = false; //por defecto siempre es true
    //Edit2.Width = "90px"; //por defecto viene en 50px 
    Edit2.Caption = "Otro"; //Label para el Edit
    //----------------------------------------------------------------------------------------------------------------

    //Agregar una imagen btn al segundo ToolBar   
    var Imgbtn4 = new TBarControls.TToolBar.ButtonImg(toolCont2, "ID3"); //Indicamos el toolbar, la src de la img y un ID
    Imgbtn4.Image = "image/24/Ordering.png";
    Imgbtn4.onClick = function () {
        alert("Funcion 4");
    }
    Imgbtn4.EnabledDown = true; //EnabledDown propieda que indica si es un boton tipo check que se marque y desmarque    
    Imgbtn4.Text = "click down"
    //----------------------------------------------------------------------------------------------------------------


    //Agregar un edit al tercer ToolBar
    var Edit3 = new TBarControls.TToolBar.Edit(toolCont3, "ID1");
    //Edit3.Visible = false; //por defecto siempre es true
    Edit3.Width = "90px"; //por defecto viene en 50px 
    Edit3.Caption = "labelText1"; //Label para el Edit
    //----------------------------------------------------------------------------------------------------------------


    //Agregar un divisor entre los edits
    var divition1 = new TBarControls.TToolBar.Divition(toolCont3, "D1");
    divition1.Color = "blue";
    //divition1.vidible = false;

    //Agregar un edit al tercer ToolBar
    var Edit4 = new TBarControls.TToolBar.Edit(toolCont3, "ID2");
    //Edit3.Visible = false; //por defecto siempre es true
    //Edit3.Width = "90px"; //por defecto viene en 50px 
    Edit4.Caption = "labelText2"; //Label para el Edit
    //----------------------------------------------------------------------------------------------------------------


    //Agregar un divisor entre el edit y el btn
    var divition2 = new TBarControls.TToolBar.Divition(toolCont3, "D1");
    //divition2.Color = "blue"; // color por defecto gris clarito

    //Agregar una imagen btn al segundo ToolBar   
    var Imgbtn5 = new TBarControls.TToolBar.ButtonImg(toolCont3, "ID3"); //Indicamos el toolbar, la src de la img y un ID
    Imgbtn5.Image = "image/24/export.png";
    Imgbtn5.onClick = function () {
        alert("Funcion 5");
    }
    Imgbtn5.EnabledDown = true; //EnabledDown propieda que indica si es un boton tipo check que se marque y desmarque    
    Imgbtn5.Text = "click down"
    //--------------------------------------------------------------------------------------------------------------------


    //Agregar una imagen btn al primer ToolBar   
    var Imgbtn6 = new TBarControls.TToolBar.ButtonImg(toolCont4, "ID1"); //Indicamos el toolbar, la src de la img y un ID
    Imgbtn6.Image = "image/24/Open-file.png";
    Imgbtn6.onClick = function () {
        alert("Funcion 6");
    }
    Imgbtn6.EnabledDown = false; //EnabledDown propieda que indica si es un boton tipo check que se marque y desmarque    
    Imgbtn6.Text = "Btn 6"
    //----------------------------------------------------------------------------------------------------------------

    //Agregar una imagen btn al primer ToolBar
    var Imgbtn7 = new TBarControls.TToolBar.ButtonImg(toolCont4, "ID2"); //Indicamos el toolbar, la src de la img y un ID
    Imgbtn7.Image = "image/24/siren_on.png";
    Imgbtn7.onClick = function () {
        alert("Funcion 7");
    }
    Imgbtn7.EnabledDown = true; //EnabledDown propieda que indica si es un boton tipo check que se marque y desmarque
    //----------------------------------------------------------------------------------------------------------------


    //agregamos elementos externos en un toolbar
    HelpRemote = new ItHelpCenter.TUfrRemoteHelp(toolCont5.ToolBox,

            function () {
    }, topbar.ControlBody);
}