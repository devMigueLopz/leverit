﻿ItHelpCenter.Demo.TDemoTemporal9 = function (inObjectHtml, _this) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent(); 


    ///////////////////////////////////////// DIVS
    var div1 = new TVclStackPanel(inObjectHtml, "div1", 1, [[12], [12], [12], [12], [12]]);
    var div1Cont = div1.Column[0].This;

    var div2 = new TVclStackPanel(inObjectHtml, "div2", 2, [[12, 12], [12, 12], [12, 12], [12, 12], [12, 12]]);
    var div2Cont1 = div2.Column[0].This;
    var div2Cont2 = div2.Column[1].This;


    ///////////////////////////////////////////// COMBO Para cambier de Modo de estilo
    var cbCombobox = new TVclComboBox2(div1Cont, ""); //CREA UN COMBOBOX CONTROL 

    var VclComboBoxItem1 = new TVclComboBoxItem(); // CREA UN COMBOBOX ITEM PARA SER AGREGADO AL COMBO 
    VclComboBoxItem1.Value = 1; //ASIGNA O EXTRAE EL VALUE AL ITEM
    VclComboBoxItem1.Text = "Destok"; // ASIGNA O EXTRAE EL TEXTO QUE MOSTRARA EL ELEMENTO EN EL COMBO 
    VclComboBoxItem1.Tag = "Destok"; // ASIGNA O EXTRAE UN VALOR QUE PUEDE GUARDARSE DE CUALQUIER TIPO
    cbCombobox.AddItem(VclComboBoxItem1); // AGREGA EL ITEM AL COMBOBOX

    var VclComboBoxItem2 = new TVclComboBoxItem();
    VclComboBoxItem2.Value = 2;
    VclComboBoxItem2.Text = "Tablet";
    VclComboBoxItem2.Tag = "Tablet";
    cbCombobox.AddItem(VclComboBoxItem2);

    var VclComboBoxItem3 = new TVclComboBoxItem();
    VclComboBoxItem3.Value = 3;                 //SysCfg.Properties.TDevice.Mobile;
    VclComboBoxItem3.Text = "Mobile";
    VclComboBoxItem3.Tag = "Mobile";
    cbCombobox.AddItem(VclComboBoxItem3);

    SysCfg.Properties.Device = "Destok";

    cbCombobox.onChange = function () {       
        try {
            SysCfg.Properties.Device = cbCombobox.Text;
        } catch (e) {
           
        } 
    }
    
    ///////ELEMENTO PARA EL EJEMPLO
    //************************REMOTE HELP********************************************//
    _this.importarScript(SysCfg.App.Properties.xRaiz + "Demo/DemoDevices/DemoDevices.js", function () {
        //boton
        var btnA = new TVclButton(div2Cont1, "Btn");
        btnA.Cursor = "pointer";
        btnA.Text = "Cargar Demo";
        btnA.VCLType = TVCLType.BS;
        btnA.onClick = function myfunction() {
            $(div2Cont2).html("");
            $(div2Cont2).css("padding-left", "15px");
            //cargamos el js demo
            var JsDemo = new ItHelpCenter.Demo.TDemoDevices(div2Cont2, function () { });
        }

    }, function (e) {
    });
    //**************************************************************************//


}

ItHelpCenter.Demo.TDemoTemporal9.prototype.importarScript = function (nombre, onSuccess, onError) {
    var s = document.createElement("script");
    s.onload = onSuccess;
    s.onerror = onError;
    s.src = nombre;
    document.querySelector("head").appendChild(s);
}