﻿ItHelpCenter.Demo.TDemoImageFormat = function (inObjectHtml, _this) {

    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    var LineaPrincipal = new TVclStackPanel(inObjectHtml, "1", 2, [[6, 6], [6, 6], [6, 6], [6, 6], [6, 6]]);
    var LeftLinea1 = new TVclStackPanel(LineaPrincipal.Column[0].This, "1", 2, [[6, 6], [6, 6], [6, 6], [6, 6], [6, 6]]);
    var LeftLinea1_2 = new TVclStackPanel(LineaPrincipal.Column[0].This, "1", 2, [[6, 6], [6, 6], [6, 6], [6, 6], [6, 6]]);
    var LeftLinea2 = new TVclStackPanel(LineaPrincipal.Column[0].This, "1", 2, [[6, 6], [6, 6], [6, 6], [6, 6], [6, 6]]);
    var LeftLinea3 = new TVclStackPanel(LineaPrincipal.Column[0].This, "1", 2, [[6, 6], [6, 6], [6, 6], [6, 6], [6, 6]]);
    var LeftLinea4 = new TVclStackPanel(LineaPrincipal.Column[0].This, "1", 2, [[6, 6], [6, 6], [6, 6], [6, 6], [6, 6]]);
    var LeftLinea5 = new TVclStackPanel(LineaPrincipal.Column[0].This, "1", 2, [[6, 6], [6, 6], [6, 6], [6, 6], [6, 6]]);
    var LeftLinea6 = new TVclStackPanel(LineaPrincipal.Column[0].This, "1", 2, [[6, 6], [6, 6], [6, 6], [6, 6], [6, 6]]);
    var LeftLinea7 = new TVclStackPanel(LineaPrincipal.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
    LeftLinea6.Row.This.style.display = "none";
    this.LabelSrc = new TVcllabel(LeftLinea1.Column[0].This, "", TlabelType.H0);
    this.LabelName = new TVcllabel(LeftLinea1_2.Column[0].This, "", TlabelType.H0);
    this.LabelWidth = new TVcllabel(LeftLinea2.Column[0].This, "", TlabelType.H0);
    this.LabelHeight = new TVcllabel(LeftLinea3.Column[0].This, "", TlabelType.H0);
    this.LabelFormat = new TVcllabel(LeftLinea4.Column[0].This, "", TlabelType.H0);
    this.LabelQuality = new TVcllabel(LeftLinea5.Column[0].This, "", TlabelType.H0);
    this.LabelCrop = new TVcllabel(LeftLinea6.Column[0].This, "", TlabelType.H0);
    this.LabelSrc.Text = "Src";
    this.LabelName.Text = "Name";
    this.LabelWidth.Text = "Width";
    this.LabelHeight.Text = "Height";
    this.LabelFormat.Text = "Format *(jpeg/png)";
    this.LabelQuality.Text = "Quality *(0-100)";
    this.LabelCrop.Text = "Crop";
    this.VclTextBoxSrc = new TVclTextBox(LeftLinea1.Column[1].This, "");
    this.VclTextBoxName = new TVclTextBox(LeftLinea1_2.Column[1].This, "");
    this.VclTextBoxWidth = new TVclTextBox(LeftLinea2.Column[1].This, "");
    this.VclTextBoxHeight = new TVclTextBox(LeftLinea3.Column[1].This, "");
    this.VclTextBoxFormat = new TVclTextBox(LeftLinea4.Column[1].This, "");
    this.VclTextBoxQuality = new TVclTextBox(LeftLinea5.Column[1].This, "");
    this.VclInputcheckboxCrop = new TVclInputcheckbox(LeftLinea6.Column[1].This, "");
    this.VclTextBoxSrc.Text = "image/48/connection.png";
    this.VclTextBoxName.Text = "image";
    this.VclTextBoxWidth.Text = 30;
    this.VclTextBoxHeight.Text = 30;
    this.VclTextBoxFormat.Text = "jpeg";
    this.VclTextBoxQuality.Text = 100;

    this.VclTextBoxSrc.Enabled = false;

    this.VclTextBoxWidth.Type = TImputtype.number;
    this.VclTextBoxHeight.Type = TImputtype.number;
    this.VclTextBoxQuality.Type = TImputtype.number;

    var ContControl = new TVclStackPanel(LineaPrincipal.Column[1].This, "1", 1, [[12], [12], [12], [12], [12]]);
    var panelFormat = ContControl.Column[0].This;

    var stackPanel = document.createElement("div");
    LeftLinea7.Column[0].This.appendChild(stackPanel);
    var btnLoadImage = new TVclInputbutton(stackPanel, "");
    btnLoadImage.Text = "Cargar imagen";
    btnLoadImage.VCLType = TVCLType.BS;
    btnLoadImage.onClick = function () {
        $(panelFormat).html("");
        _this.VclImageFormat = new ItHelpCenter.Componet.ImageControls.TVclImageFormat(null, null);
        _this.VclImageFormat.Src = _this.VclTextBoxSrc.Text;
        _this.VclImageFormat.Name = _this.VclTextBoxName.Text;
        _this.VclImageFormat.Height = _this.VclTextBoxHeight.Text;
        _this.VclImageFormat.Width = _this.VclTextBoxWidth.Text;
        _this.VclImageFormat.Format = _this.VclTextBoxFormat.Text;
        _this.VclImageFormat.Quality = _this.VclTextBoxQuality.Text;
        _this.VclImageFormat.Crop = _this.VclInputcheckboxCrop.Checked;

        _this.VclImageFormat.OnSave = function (VclImageFormat, DataUrl) {
            console.log(DataUrl);
        }
        _this.VclImageFormat.Save(panelFormat);
        btnDownload.Enabled = true;
    }

    var btnDownload = new TVclInputbutton(stackPanel, "");
    btnDownload.Text = "Descargar imagen";
    btnDownload.VCLType = TVCLType.BS;
    btnDownload.Enabled = false;
    btnDownload.onClick = function () {
        _this.VclImageFormat.Download();
    }

    $('html, body').animate({ scrollTop: 0 }, 'slow');



}