﻿ItHelpCenter.Demo.TDemoTabControl = function (MenuObject,inObjectHtml, _this) {

    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    var stackPanel = document.createElement("div");
    this.MenuObject = MenuObject;
    inObjectHtml.appendChild(stackPanel);
    if (SysCfg.App.Properties.Device == SysCfg.App.TDevice.Desktop) {
        //   stackPanel.style.marginTop = "20px";
        //inObjectHtml.style.backgroundColor = "red";
    }
    _this.ObjectHtml = stackPanel;
    
    _this.InicializeComponent();

}
ItHelpCenter.Demo.TDemoTabControl.prototype.InicializeComponent = function () {
    var _this = this.TParent();

    _this.Fila1 = new TVclStackPanel(_this.ObjectHtml, "1", 1, [[12], [12], [12], [12], [12]]);
    if (Source.Menu.IsMobil) {
        _this.NavTabControls = new ItHelpCenter.Componet.NavTabControls.TNavTabControls(_this.MenuObject.ObjectData, null);
        _this.NavTabControls.VerticalPosition = true;
        _this.NavTabControls.TextColorPanel = "#cccccc";
        
    } else {
        _this.NavTabControls = new ItHelpCenter.Componet.NavTabControls.TNavTabControls(_this.Fila1.Column[0].This, null);
        _this.NavTabControls.VerticalPosition = false;
        this.PaddingTopNavBar = 10;
        this.PaddingNavBar = 10;    
    }
    _this.NavTabControls.TextColor = "#fff";
    /*Creamos TTabPage*/
    var TabPag1 = new TTabPage();
    TabPag1.Name = "Menu1";
    TabPag1.Caption = "Menu 1"
    TabPag1.Active = false;
    TabPag1.Tag = "Menu 1";
    var TabPag2 = new TTabPage();
    TabPag2.Name = "Home";
    TabPag2.Caption = "Home"
    TabPag2.Active = true;
    TabPag2.Tag = "Home";
    var TabPag3 = new TTabPage();
    TabPag3.Name = "Menu2";
    TabPag3.Caption = "Menu 2"
    TabPag3.Active = false;
    TabPag3.Tag = "Menu 2";

    _this.NavTabControls.AddTabPages(TabPag2);
    _this.NavTabControls.AddTabPages(TabPag1);
    _this.NavTabControls.AddTabPages(TabPag3);
    
    _this.NavTabControls.OnChangeTab = function (TabCont, Tab) {
        if (Tab.Name == "Menu1") {
            _this.NavTabControls.BackgroundHeader = "#2B569A";
        } else {
            _this.NavTabControls.BackgroundHeader = "#227446";
        }
    };

    /*Creamos NavPanelBar
     */

    _this.NavPanelBarPanel1 = new ItHelpCenter.Componet.NavTabControls.TNavPanelBar(TabPag1);
    _this.NavPanelBarPanel1.Title = "Panel 1";
    _this.NavPanelBarPanel2 = new ItHelpCenter.Componet.NavTabControls.TNavPanelBar(TabPag1);
    _this.NavPanelBarPanel2.Title = "Panel 2";
    _this.NavPanelBarPanel3 = new ItHelpCenter.Componet.NavTabControls.TNavPanelBar(TabPag2);
    _this.NavPanelBarPanel3.Title = "Panel 3";
    _this.NavPanelBarPanel4 = new ItHelpCenter.Componet.NavTabControls.TNavPanelBar(TabPag2);
    _this.NavPanelBarPanel4.Title = "Panel 4";

    /*Creamos TNavBar = Imagen y texto Abajo
     */
    _this.NavBar_Insert = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.NavBar_Insert.Text = "Insert";
    _this.NavBar_Insert.SrcImg = "image/24/Add1.png";
    //_this.NavBar_Insert.ChekedButton = true;
    _this.NavBar_Insert.Opacity = "0.85";
    //_this.NavBar_Insert.TextColor = "#757575";
    _this.NavBar_Insert.onclick = function (NavBar) {
        //alert(NavBar.Text);
        _this.NavBar_Update.Disabled = true;
        _this.NavBar_Insert.Disabled = true;
        _this.NavBar_Delete.Disabled = true;
        _this.NavBar_Post.Disabled = false;
        _this.NavBar_Cancel.Disabled = false;

        //_this.NavBar_Insert_OnClick(_this, _this.NavBar_Insert);
    }

    _this.NavBar_Update = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.NavBar_Update.Text = "Update";
    _this.NavBar_Update.SrcImg = "image/24/edit.png";
    _this.NavBar_Update.Opacity = "0.85";
    //_this.NavBar_Update.TextColor = "#757575";
    _this.NavBar_Update.onclick = function (NavBar) {
        //alert(NavBar.Text);
        _this.NavBar_Update.Disabled = true;
        _this.NavBar_Insert.Disabled = true;
        _this.NavBar_Delete.Disabled = true;
        _this.NavBar_Post.Disabled = false;
        _this.NavBar_Cancel.Disabled = false;
        //_this.NavBar_Post.Disabled = !_this.NavBar_Post.Disabled;
        //_this.NavBar_Cancel.Disabled = !_this.NavBar_Cancel.Disabled;
    }

    _this.NavBar_Delete = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.NavBar_Delete.Text = "Delete";
    _this.NavBar_Delete.SrcImg = "image/24/Close.png";
    _this.NavBar_Delete.Opacity = "0.85";
    //_this.NavBar_Delete.TextColor = "#757575";
    _this.NavBar_Delete.onclick = function (NavBar) {
        _this.NavBar_Update.Disabled = true;
        _this.NavBar_Insert.Disabled = true;
        _this.NavBar_Delete.Disabled = true;
        _this.NavBar_Post.Disabled = false;
        _this.NavBar_Cancel.Disabled = false;
    }

    _this.NavBar_Post = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.NavBar_Post.Text = "Post";
    _this.NavBar_Post.SrcImg = "image/24/Accept.png";
    //_this.NavBar_Post.TextColor = "#757575";
    _this.NavBar_Post.Disabled = true;
    _this.NavBar_Post.onclick = function (NavBar) {
        alert(NavBar.Text);
        _this.NavBar_Update.Disabled = false;
        _this.NavBar_Insert.Disabled = false;
        _this.NavBar_Delete.Disabled = false;
        _this.NavBar_Post.Disabled = true;
        _this.NavBar_Cancel.Disabled = true;
    }

    _this.NavBar_Cancel = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.NavBar_Cancel.Text = "Cancel";
    _this.NavBar_Cancel.SrcImg = "image/24/Cancel.png";
    _this.NavBar_Cancel.Disabled = true;
    //_this.NavBar_Cancel.TextColor = "#757575";
    _this.NavBar_Cancel.onclick = function (NavBar) {
        alert(NavBar.Text);
        _this.NavBar_Update.Disabled = false;
        _this.NavBar_Insert.Disabled = false;
        _this.NavBar_Delete.Disabled = false;
        _this.NavBar_Post.Disabled = true;
        _this.NavBar_Cancel.Disabled = true;
    }

    _this.NavBar_ExporteExcel = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.NavBar_ExporteExcel.Text = "Excel";
    _this.NavBar_ExporteExcel.SrcImg = "image/24/user.png";
    _this.NavBar_ExporteExcel.Width = "28px";
    _this.NavBar_ExporteExcel.onclick = function (NavBar) {
        _this.NavBar_ExporteExcel.Text = "User";
        alert("Nuevo texto: " + NavBar.Text);
    }
    _this.NavBar_ExportWord = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.NavBar_ExportWord.Text = "Mensaje";
    _this.NavBar_ExportWord.SrcImg = "image/24/folder.png";
    _this.NavBar_ExportWord.Width = "28px";
    _this.NavBar_ExportWord.onclick = function (NavBar) {
        alert("Nuevo texto: " + NavBar.Text + " - Nuevo texto Panel");
        NavBar.Text = "Folder";
        NavBar.TextColor = "red";


        _this.NavPanelBarPanel2.Title = "Mensajeria";

    }

    /*Creamos NavGroupBar = Imagen y texto lado Derecho, tambien se puede colocar solo la imagen
     */
    _this.NavGroupBar1 = new ItHelpCenter.Componet.NavTabControls.TNavGroupBar();
    _this.NavGroupBar2 = new ItHelpCenter.Componet.NavTabControls.TNavGroupBar();
    _this.ItemGroupBar1 = new ItHelpCenter.Componet.NavTabControls.TItemGroupBar();
    _this.ItemGroupBar1.Text = "";
    _this.ItemGroupBar1.Width = "20px";
    _this.ItemGroupBar1.ChekedButton = true;
    _this.ItemGroupBar1.SrcImg = "image/24/chart.png";
    _this.ItemGroupBar1.onclick = function (ItemGroupBar) {
        //alert(ItemGroupBar.Text);
        _this.NavPanelBarPanel2.Visible = !_this.NavPanelBarPanel2.Visible;
        _this.NavGroupBar2.Visible = !_this.NavGroupBar2.Visible;
    }

    _this.ItemGroupBar2 = new ItHelpCenter.Componet.NavTabControls.TItemGroupBar();
    _this.ItemGroupBar2.Text = "";
    _this.ItemGroupBar2.Width = "20px";
    _this.ItemGroupBar2.SrcImg = "image/24/folder.png";
    _this.ItemGroupBar2.ChekedButton = true;
    _this.ItemGroupBar2.onclick = function (ItemGroupBar) {
        alert(ItemGroupBar.Cheked);
    }

    _this.ItemGroupBar3 = new ItHelpCenter.Componet.NavTabControls.TItemGroupBar();
    _this.ItemGroupBar3.Text = "Opcion 3"
    _this.ItemGroupBar3.SrcImg = "image/24/addressbook.png";
    _this.ItemGroupBar3.onclick = function (ItemGroupBar) {

        _this.ItemGroupBar4.Disabled = !_this.ItemGroupBar4.Disabled;
        if (_this.ItemGroupBar4.Disabled) {
            alert(ItemGroupBar.Text + " -         " + _this.ItemGroupBar4.Text + " : desactivada");
        } else {
            alert(ItemGroupBar.Text + " -          " + _this.ItemGroupBar4.Text + " : activa");
        }

    }

    _this.ItemGroupBar4 = new ItHelpCenter.Componet.NavTabControls.TItemGroupBar();
    _this.ItemGroupBar4.Text = "Opcion 4";
    _this.ItemGroupBar4.SrcImg = "image/24/prueba2.png";


    _this.ItemGroupBar5 = new ItHelpCenter.Componet.NavTabControls.TItemGroupBar();
    _this.ItemGroupBar5.Text = "Menu 1";
    _this.ItemGroupBar5.SrcImg = "image/24/prueba2.png";
    _this.ItemGroupBar5.onclick = function (ItemGroupBar) {
        alert(ItemGroupBar.Text);
    }

    _this.ItemGroupBar6 = new ItHelpCenter.Componet.NavTabControls.TItemGroupBar();
    _this.ItemGroupBar6.Text = "Menu 2";
    _this.ItemGroupBar6.SrcImg = "image/24/prueba2.png";
    _this.ItemGroupBar6.onclick = function (ItemGroupBar) {
        alert(ItemGroupBar.Text);
    }


    _this.ItemGroupBar7 = new ItHelpCenter.Componet.NavTabControls.TItemGroupBar();
    _this.ItemGroupBar7.Text = "";
    _this.ItemGroupBar7.Width = "20px";
    _this.ItemGroupBar7.SrcImg = "image/24/folder.png";


    _this.ItemGroupBar8 = new ItHelpCenter.Componet.NavTabControls.TItemGroupBar();
    _this.ItemGroupBar8.Text = "Excel";
    _this.ItemGroupBar8.SrcImg = "image/24/prueba2.png";
    _this.ItemGroupBar8.onclick = function (ItemGroupBar) {
        alert(ItemGroupBar.Text);
    }

    _this.ItemGroupBar9 = new ItHelpCenter.Componet.NavTabControls.TItemGroupBar();
    _this.ItemGroupBar9.Text = "Word";
    _this.ItemGroupBar9.SrcImg = "image/24/prueba2.png";
    _this.ItemGroupBar9.onclick = function (ItemGroupBar) {
        alert(ItemGroupBar.Text);
    }
    _this.ItemGroupBar4.addItemGroupBar(_this.ItemGroupBar5);
    _this.ItemGroupBar4.addItemGroupBar(_this.ItemGroupBar6);
    _this.ItemGroupBar7.addItemGroupBar(_this.ItemGroupBar8);
    _this.ItemGroupBar7.addItemGroupBar(_this.ItemGroupBar9);

    _this.NavGroupBar1.addItemGroupBar(_this.ItemGroupBar1);
    _this.NavGroupBar1.addItemGroupBar(_this.ItemGroupBar7);
    _this.NavGroupBar2.addItemGroupBar(_this.ItemGroupBar3);
    _this.NavGroupBar2.addItemGroupBar(_this.ItemGroupBar4);


    /*Creacion  de elemento Tab : Menu*/

    _this.NavBar_Skip1 = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.NavBar_Skip1.Text = "First";
    _this.NavBar_Skip1.SrcImg = "image/24/Skip-backward.png";
    _this.NavBar_Skip1.Opacity = "0.85";
    //_this.NavBar_Skip1.TextColor = "#757575";
    _this.NavBar_Skip1.onclick = function (NavBar) {      
    }
    _this.NavBar_Skip2 = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.NavBar_Skip2.Text = "Back";
    _this.NavBar_Skip2.SrcImg = "image/24/Fast-backward.png";
    _this.NavBar_Skip2.Opacity = "0.85";
    //_this.NavBar_Skip2.TextColor = "#757575";
    _this.NavBar_Skip2.onclick = function (NavBar) {
    }
    _this.NavBar_Skip3 = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.NavBar_Skip3.Text = "Next";
    _this.NavBar_Skip3.SrcImg = "image/24/Fast-forward.png";
    _this.NavBar_Skip3.Opacity = "0.85";
    //_this.NavBar_Skip3.TextColor = "#757575";
    _this.NavBar_Skip3.onclick = function (NavBar) {
    }
    _this.NavBar_Skip4 = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.NavBar_Skip4.Text = "Last";
    _this.NavBar_Skip4.SrcImg = "image/24/Skip-forward.png";
    _this.NavBar_Skip4.Opacity = "0.85";
    //_this.NavBar_Skip4.TextColor = "#757575";
    _this.NavBar_Skip4.onclick = function (NavBar) {
    }
    //_this.NavPanelBarPanel1.addItemBar(_this.NavBar_Skip1);
    //_this.NavPanelBarPanel1.addItemBar(_this.NavBar_Skip2);
    _this.NavPanelBarPanel1.addItemBar(_this.NavBar_Insert);
    _this.NavPanelBarPanel1.addItemBar(_this.NavBar_Update);
    _this.NavPanelBarPanel1.addItemBar(_this.NavBar_Delete);
    _this.NavPanelBarPanel1.addItemBar(_this.NavBar_Post);
    _this.NavPanelBarPanel1.addItemBar(_this.NavBar_Cancel);
    //_this.NavPanelBarPanel1.addItemBar(_this.NavBar_Skip3);
    //_this.NavPanelBarPanel1.addItemBar(_this.NavBar_Skip4);

    //_this.NavPanelBarPanel1.addItemBar(_this.NavGroupBar1);
    _this.NavPanelBarPanel1.addItemBar(_this.NavGroupBar2);
    _this.NavPanelBarPanel2.addItemBar(_this.NavBar_ExporteExcel);
    _this.NavPanelBarPanel2.addItemBar(_this.NavBar_ExportWord);


    _this.NavBar1 = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.NavBar1.Text = "Small";
    _this.NavBar1.Size = "normal";
    _this.NavBar1.SrcImg = "image/24/user.png";
    _this.NavBar1.onclick = function (NavBar) {
        alert(NavBar.Text);
        _this.NavTabControls.BackgroundHeader = "#b6472b";
    }
    _this.NavBar2 = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.NavBar2.Text = "Normal";
    _this.NavBar2.Size = "normal";
    _this.NavBar2.SrcImg = "image/24/folder.png";
    _this.NavBar2.onclick = function (NavBar) {
       // alert(NavBar.Text);
    }

    _this.NavBar3 = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.NavBar3.Text = "Big";
    _this.NavBar3.Size = "big";
    _this.NavBar3.SrcImg = "image/24/chart.png";
    _this.NavBar3.onclick = function (NavBar) {
      //  alert(NavBar.Text);
    }

    _this.NavBar4 = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.NavBar4.Text = "Check";
    _this.NavBar4.ChekedButton = true;
    _this.NavBar4.SrcImg = "image/24/cut.png";
    _this.NavBar4.onclick = function (NavBar) {
        alert(NavBar.Text);
    }
    _this.NavBar5 = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.NavBar5.Text = "Full";
    _this.NavBar5.Border = "full";
    _this.NavBar5.SrcImg = "image/24/cut.png";
    _this.NavBar5.onclick = function (NavBar) {
        alert(NavBar.Text);
    }
    _this.NavBar6 = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.NavBar6.Text = "Check";
    _this.NavBar6.ChekedButton = true;
    _this.NavBar6.SrcImg = "image/24/cut.png";
    _this.NavBar6.onclick = function (NavBar) {
        alert(NavBar.Text);
    }

    _this.NavGroupBarB = new ItHelpCenter.Componet.NavTabControls.TNavGroupBar();
    _this.ItemGroupBarB1 = new ItHelpCenter.Componet.NavTabControls.TItemGroupBar();
    _this.ItemGroupBarB1.Text = "Opcion 1";
    _this.ItemGroupBarB1.SrcImg = "image/24/chart.png";
    _this.ItemGroupBarB1.onclick = function (ItemGroupBar) {
        alert(ItemGroupBar.Text);
    }

    _this.ItemGroupBarB2 = new ItHelpCenter.Componet.NavTabControls.TItemGroupBar();
    _this.ItemGroupBarB2.Text = "Opcion 2";
    _this.ItemGroupBarB2.SrcImg = "image/24/folder.png";
    _this.ItemGroupBarB2.ChekedButton = true;
    _this.ItemGroupBarB2.onclick = function (ItemGroupBar) {
        alert(ItemGroupBar.Cheked);
    }

    _this.ItemGroupBarB3 = new ItHelpCenter.Componet.NavTabControls.TItemGroupBar();
    _this.ItemGroupBarB3.Text = "Opcion 3"
    _this.ItemGroupBarB3.SrcImg = "image/24/addressbook.png";
    _this.ItemGroupBarB3.onclick = function (ItemGroupBar) {
        alert(ItemGroupBar.Text);
    }
  
    _this.NavGroupBarB.addItemGroupBar(_this.ItemGroupBarB1);
    _this.NavGroupBarB.addItemGroupBar(_this.ItemGroupBarB2);
    _this.NavPanelBarPanel3.VerticalPosition = false;
    _this.NavPanelBarPanel3.addItemBar(_this.NavBar1);
    //_this.NavPanelBarPanel3.addItemBar(_this.NavBar6);
    _this.NavPanelBarPanel3.addItemBar(_this.NavGroupBarB);
    _this.NavPanelBarPanel4.addItemBar(_this.NavBar2);
    _this.NavPanelBarPanel4.addItemBar(_this.NavBar3);
    _this.NavPanelBarPanel4.addItemBar(_this.NavBar4);
    _this.NavPanelBarPanel4.addItemBar(_this.NavBar5);
    

}
ItHelpCenter.Demo.TDemoTabControl.prototype.NavBar_Insert_OnClick = function (sender, EventArgs) {
    var _this = this.TParent();
    alert(EventArgs.Cheked);
}
ItHelpCenter.Demo.TDemoTabControl.prototype.NavBar_Insert_OnClick = function (sender, EventArgs) {
    var _this = this.TParent();
    alert(EventArgs.Cheked);
}

ItHelpCenter.Demo.TDemoTabControl.prototype.CreateGrid = function () {
    var _this = this.TParent();
    $(_this.Fila2.Column[1].This).html("");
    var ContDataGrid = new TVclStackPanel(_this.Fila2.Column[1].This, "1", 1, [[12], [12], [12], [12], [12]]);
    ContDataGrid.Row.This.style.marginTop = "4px";
    var Grilla = new TGrid(ContDataGrid.Column[0].This, "", ""); //CREA UN GRID CONTROL
    var Col0 = new TColumn(); //CREA UNA COLUMNA PARA AGREGAR AL GRID CONTROL
    Col0.Name = "Check"; //ASIGNA O EXTRAE EL NOMBRE DE LA COLUMNA
    Col0.Caption = ""; //ASIGNA O EXTRAE EL TEXTO QUE SE MOSTRARA EN LA COLUMNA 
    Col0.Index = 0; // ASGINA O EXTRAE EL INDICE DE LA COLUMNA 
    Col0.EnabledEditor = true; //ASIGNA O EXTRAE UN VALO TRUE O FALSE QUE INDICA SI LA COLUMNA MOSTRARA UN EDITOR DE DATOS
    Col0.DataType = SysCfg.DB.Properties.TDataType.Boolean; //ASIGNA O EXTRAE UN VALOR DE TIPO DE DATO QUE TIENEN LA COLUMNA 
    Col0.ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.None; //ASIGNA O EXTRAE UN ESTILO DE EDITOR Q MOSTRARA EN EL CASO EL ENABLEEDITOR ESTE ACTIVADO
    Grilla.AddColumn(Col0); //AGREGA UNA COLUMNA AL GRIDCONTROL 
    for (var i = 0; i < 4; i++) {
        var Col = new TColumn();
        Col.Name = "Columna" + (i + 1);
        Col.Caption = "Columna " + (i + 1);
        Col.Index = i + 2;
        Col.DataType = SysCfg.DB.Properties.TDataType.Unknown;
        Grilla.AddColumn(Col);
    }
    for (var i = 0; i < 5; i++) {
        var NewRow = new TRow(); //CREA UN ROW PARA AGREGAR AL GRIDCONTROL 

        var Cell0 = new TCell(); //CREA UNA CELDA PARA QUE SE AGREGA AL ROW
        Cell0.Value = false; // ASIGNA O EXTRAE EL VALOR QUE SE MOSTRARA EN LA CELDA DEL GRIDCONTROL 
        Cell0.IndexColumn = 0; // ASIGNA O EXTRAE EL INDEX DE LA COLUMNA 
        Cell0.IndexRow = i; // ASIGNA O EXTRAE EL INDEX DEL ROW 
        NewRow.AddCell(Cell0); // AGREGA O EXTRAE LA CELDA AL ROW

        var Cell1 = new TCell();
        Cell1.Value = "";
        Cell1.IndexColumn = 0 + 1;
        Cell1.IndexRow = i;
        NewRow.AddCell(Cell1);

        for (var j = 0; j < 3; j++) {
            var Cell = new TCell();
            Cell.Value = "";
            Cell.IndexColumn = j + 2;
            Cell.IndexRow = i;
            NewRow.AddCell(Cell);
        }
        NewRow.onclick = function () { // ASIGNA UN EVENTO CUANDO SE HACE CLICK EN EL ROW DEL GRIDCONTROL
            Grilla_rowclick(NewRow);
        }
        Grilla.AddRow(NewRow);
    }


    /*
    for (var i = 0; i <4; i++) {
        var Col = new TColumn();
        Col.Name = "Columna" + (i + 1);
        Col.Caption = "Columna " + (i + 1);
        Col.Index = i + 2;
        Col.DataType = SysCfg.DB.Properties.TDataType.Unknown;
        Grilla.AddColumn(Col);
    }
    for (var i = 0; i < 5; i++) {
        var NewRow = new TRow(); //CREA UN ROW PARA AGREGAR AL GRIDCONTROL

        for (var j = 0; j < 4; j++) {
            var Cell = new TCell();
            Cell.Value = "&nbsp;";
            Cell.IndexColumn = j + 2;
            Cell.IndexRow = i;
            NewRow.AddCell(Cell);
        }
        NewRow.onclick = function () { // ASIGNA UN EVENTO CUANDO SE HACE CLICK EN EL ROW DEL GRIDCONTROL
            Grilla_rowclick(NewRow);
        }
        Grilla.AddRow(NewRow);
    }
    */
}


