﻿ItHelpCenter.Demo.TDemoDivControl = function (inObjectHtml, _this) {
    //************* INICIO TAB DIV CONTROL ***********************************/// 
    var ContDivControl = new TVclStackPanel(inObjectHtml, "1", 1, [[12], [12], [12], [12], [12]]);
    ContDivControl.Row.This.style.marginTop = "20px";

    var div = new TVclDiv(ContDivControl.Column[0].This);

    ContDivControl.Row.This.style.width = "95%";
    ContDivControl.Row.This.style.margin = "30px";

    var texto = new TVclTextBox(div.This, "TxBxDiv");
    texto.This.style.width = "100%";

    var VclComboBoxDIV = new TVclComboBox2(ContDivControl.Column[0].This, '');

    var VclComboBoxItem0 = new TVclComboBoxItem();
    VclComboBoxItem0.Value = 0;
    VclComboBoxItem0.Text = "BackgroundColor";
    VclComboBoxItem0.Tag = "Ejemplo 1: red, Ejemplo 2: #ff0000";
    VclComboBoxDIV.AddItem(VclComboBoxItem0);

    var VclComboBoxItem1 = new TVclComboBoxItem();
    VclComboBoxItem1.Value = 1;
    VclComboBoxItem1.Text = "Boder";
    VclComboBoxItem1.Tag = "Ejemplo 1: 1px solid red, Ejemplo 2: 1px solid #ff0000";
    VclComboBoxDIV.AddItem(VclComboBoxItem1);

    var VclComboBoxItem2 = new TVclComboBoxItem();
    VclComboBoxItem2.Value = 2;
    VclComboBoxItem2.Text = "BoderBottom";
    VclComboBoxItem2.Tag = "Ejemplo 1: 1px solid red, Ejemplo 2: 1px solid #ff0000";
    VclComboBoxDIV.AddItem(VclComboBoxItem2);

    var VclComboBoxItem3 = new TVclComboBoxItem();
    VclComboBoxItem3.Value = 3;
    VclComboBoxItem3.Text = "BoderLeft";
    VclComboBoxItem3.Tag = "Ejemplo 1: 1px solid red, Ejemplo 2: 1px solid #ff0000";
    VclComboBoxDIV.AddItem(VclComboBoxItem3);

    var VclComboBoxItem4 = new TVclComboBoxItem();
    VclComboBoxItem4.Value = 4;
    VclComboBoxItem4.Text = "BoderRight";
    VclComboBoxItem4.Tag = "Ejemplo 1: red, Ejemplo 2: #ff0000";
    VclComboBoxDIV.AddItem(VclComboBoxItem4);

    var VclComboBoxItem5 = new TVclComboBoxItem();
    VclComboBoxItem5.Value = 5;
    VclComboBoxItem5.Text = "BoderTopColor";
    VclComboBoxItem5.Tag = "Ejemplo 1: red, Ejemplo 2: #ff0000";
    VclComboBoxDIV.AddItem(VclComboBoxItem5);

    var VclComboBoxItem6 = new TVclComboBoxItem();
    VclComboBoxItem6.Value = 6;
    VclComboBoxItem6.Text = "BoderBottomColor";
    VclComboBoxItem6.Tag = "Ejemplo 1: red, Ejemplo 2: #ff0000";
    VclComboBoxDIV.AddItem(VclComboBoxItem6);

    var VclComboBoxItem7 = new TVclComboBoxItem();
    VclComboBoxItem7.Value = 7;
    VclComboBoxItem7.Text = "BoderLeftColor";
    VclComboBoxItem7.Tag = "Ejemplo 1: red, Ejemplo 2: #ff0000";
    VclComboBoxDIV.AddItem(VclComboBoxItem7);

    var VclComboBoxItem8 = new TVclComboBoxItem();
    VclComboBoxItem8.Value = 8;
    VclComboBoxItem8.Text = "BoderRightColor";
    VclComboBoxItem8.Tag = "Ejemplo 1: red, Ejemplo 2: #ff0000";
    VclComboBoxDIV.AddItem(VclComboBoxItem8);

    var VclComboBoxItem9 = new TVclComboBoxItem();
    VclComboBoxItem9.Value = 9;
    VclComboBoxItem9.Text = "BoderAllColor";
    VclComboBoxItem9.Tag = "Ejemplo 1: red, Ejemplo 2: #ff0000";
    VclComboBoxDIV.AddItem(VclComboBoxItem9);

    var VclComboBoxItem10 = new TVclComboBoxItem();
    VclComboBoxItem10.Value = 10;
    VclComboBoxItem10.Text = "Margin";
    VclComboBoxItem10.Tag = "Ejemplo: 5px, Ejemplo 2: 10%";
    VclComboBoxDIV.AddItem(VclComboBoxItem10);

    var VclComboBoxItem11 = new TVclComboBoxItem();
    VclComboBoxItem11.Value = 11;
    VclComboBoxItem11.Text = "MarginTop";
    VclComboBoxItem11.Tag = "Ejemplo: 5px, Ejemplo 2: 10%";
    VclComboBoxDIV.AddItem(VclComboBoxItem11);

    var VclComboBoxItem12 = new TVclComboBoxItem();
    VclComboBoxItem12.Value = 12;
    VclComboBoxItem12.Text = "MarginBottom";
    VclComboBoxItem12.Tag = "Ejemplo: 5px, Ejemplo 2: 10%";
    VclComboBoxDIV.AddItem(VclComboBoxItem12);

    var VclComboBoxItem13 = new TVclComboBoxItem();
    VclComboBoxItem13.Value = 13;
    VclComboBoxItem13.Text = "MarginLeft";
    VclComboBoxItem13.Tag = "Ejemplo: 5px, Ejemplo 2: 10%";
    VclComboBoxDIV.AddItem(VclComboBoxItem13);

    var VclComboBoxItem14 = new TVclComboBoxItem();
    VclComboBoxItem14.Value = 14;
    VclComboBoxItem14.Text = "MarginRight";
    VclComboBoxItem14.Tag = "Ejemplo: 5px, Ejemplo 2: 10%";
    VclComboBoxDIV.AddItem(VclComboBoxItem14);

    var VclComboBoxItem15 = new TVclComboBoxItem();
    VclComboBoxItem15.Value = 15;
    VclComboBoxItem15.Text = "Padding";
    VclComboBoxItem15.Tag = "Ejemplo: 5px, Ejemplo 2: 10%";
    VclComboBoxDIV.AddItem(VclComboBoxItem15);

    var VclComboBoxItem16 = new TVclComboBoxItem();
    VclComboBoxItem16.Value = 16;
    VclComboBoxItem16.Text = "PaddingTop";
    VclComboBoxItem16.Tag = "Ejemplo: 5px, Ejemplo 2: 10%";
    VclComboBoxDIV.AddItem(VclComboBoxItem16);

    var VclComboBoxItem17 = new TVclComboBoxItem();
    VclComboBoxItem17.Value = 17;
    VclComboBoxItem17.Text = "PaddingBottom";
    VclComboBoxItem17.Tag = "Ejemplo: 5px, Ejemplo 2: 10%";
    VclComboBoxDIV.AddItem(VclComboBoxItem17);

    var VclComboBoxItem18 = new TVclComboBoxItem();
    VclComboBoxItem18.Value = 18;
    VclComboBoxItem18.Text = "PaddingLeft";
    VclComboBoxItem18.Tag = "Ejemplo: 5px, Ejemplo 2: 10%";
    VclComboBoxDIV.AddItem(VclComboBoxItem18);

    var VclComboBoxItem19 = new TVclComboBoxItem();
    VclComboBoxItem19.Value = 19;
    VclComboBoxItem19.Text = "PaddingRight";
    VclComboBoxItem19.Tag = "Ejemplo: 5px, Ejemplo 2: 10%";
    VclComboBoxDIV.AddItem(VclComboBoxItem19);

    var VclComboBoxItem20 = new TVclComboBoxItem();
    VclComboBoxItem20.Value = 20;
    VclComboBoxItem20.Text = "Visible";
    VclComboBoxItem20.Tag = "Ejemplo: True o False";
    VclComboBoxDIV.AddItem(VclComboBoxItem20);


    var texto = new TVclTextBox(ContDivControl.Column[0].This, "TxBxStyle");
    texto.This.style.marginRight = "10px";
    var labTitle = new Vcllabel(ContDivControl.Column[0].This, "labHelp", TVCLType.BS, TlabelType.H0, "");
    labTitle.style.marginRight = "10px";

    VclComboBoxDIV.onChange = function () {
        labTitle.innerText = VclComboBoxDIV.Options[VclComboBoxDIV.Value].Tag;
        _this.combovalue = VclComboBoxDIV.Text;
    }
    VclComboBoxDIV.This.style.marginRight = "10px";

    var btnUpdate = new TVclButton(ContDivControl.Column[0].This, "btnUpdate");
    btnUpdate.Text = "Actualizar Div";
    btnUpdate.VCLType = TVCLType.BS;
    btnUpdate.onClick = function myfunction() {
        var dato = _this.combovalue;
        if (dato == "Visible") {
            if (texto.Text == "True") {
                div[dato] = true;
            } else if (texto.Text == "False") {
                div[dato] = false;
            }
        } else {
            div[dato] = texto.Text;
        }
    }
    //************* FIN TAB DIV CONTROL ***********************************/// 
}