﻿

//******************************************************
//*********  basico prototype *************************
function DiferenciaEdades1() {
    this.EdadHombre;
    this.EdadMujer;
}
DiferenciaEdades1.prototype.Diferencia = function () {//diferencia es el nombre de la funcion
    return this.EdadHombre - this.EdadMujer;
}
//uso
var oDiferenciaEdades1 = new DiferenciaEdades1();
oDiferenciaEdades1.EdadHombre = 20;
oDiferenciaEdades1.EdadMujer = 10;
alert("PE03 se ejecuto la resta " + oDiferenciaEdades1.Diferencia());

//******************************************************
//*********  basico Callback *************************
function DiferenciaEdades(a, b, _callback) {
    this.EdadHombre = a;
    this.EdadMujer = b;
    _callback(a, b);
}
//uso
var oDiferenciaEdades = new DiferenciaEdades(60, 10, function () {

    alert("PE01 Niv 1. " + (arguments[0] - arguments[1]));
    var ResNiv1 = arguments[0] - arguments[0];
    var oDiferenciaEdades2 = new DiferenciaEdades(60, 10, function (a, b) {
        alert("PE01 Niv 2  " + ((a - b) + ResNiv1));
        ResNiv2 = a - b;
        var oDiferenciaEdades3 = new DiferenciaEdades(60, 10, function (a, b) {
            alert("PE01 Niv 3 " + ((a - b) + ResNiv1 + ResNiv2));
        });
    });
});
var oDiferenciaEdades = new DiferenciaEdades(60, 10, function (a, b) {

    alert("PE01 se ejecuto la resta " + (a - b));
});
//******************************************************
