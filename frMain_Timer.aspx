﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frMain_Timer.aspx.cs" Inherits="ITHelpCenter.frMain_Timer" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/DateTime99/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Timer ID="_Timer" runat="server" OnTick="TimerElapsed"></asp:Timer>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="_Timer" EventName="Tick" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
