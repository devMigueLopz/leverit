﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="ITHelpCenter.main1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!-- 
        viewport   //Resolucion para moviles
        content="width=device-width    // Ancho del sistema a la resolucion del ancho del dispositivo
        initial-scale=1.0"            //Escala del zoom
        
        -->
    <title></title>

    <link href="Componet/BootStrap_Source/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="Componet/BootStrap_Source/bootstrap-table/src/bootstrap-table.css" rel="stylesheet" />
    <link href="Scripts/jquery-ui.min.css" rel="stylesheet" />
    <link href="Scripts/jquery-ui-timepicker-addon.css" rel="stylesheet" />
  
    <link href="Css/main.css" rel="stylesheet" />
    <link href="Css/Component/BodyMain/BodyMain.css" rel="stylesheet" />

    <link href="Css/Login.css" rel="stylesheet" />
    <link href="Css/ChangePassword.css" rel="stylesheet" /> 
    <link href="Css/LoginConfig.css" rel="stylesheet" />
    <link href="Css/Component/Tooltip.css" rel="stylesheet" />

    <script src="Scripts/jquery2-1-4min.js"></script>
    <script src="Scripts/jquery-ui.min.js"></script>
    <script src="Scripts/jquery-ui-timepicker-addon.js"></script>
    <script src="Scripts/jquery.mousewheel.js"></script>
    <script src="Scripts/jquery.slimscroll.min.js"></script>
    <script src="Componet/BootStrap_Source/bootstrap/js/bootstrap.min.js"></script>
    <script src="Componet/BootStrap_Source/bootstrap-table/src/bootstrap-table.js"></script>
    
     <%--GENERAL--%>   
   
    <script src="Scripts/namespace.js?1"></script>
    <script src="Scripts/ScriptManager.js?1"></script>  
    

    <script type="text/javascript">
        SysCfg.App.IniDB = new SysCfg.ini.DBHelper3();
        var getParameters = location.search.substring(1, location.search.length);
        ItHelpCenter.App.Application_Startup(undefined, getParameters);
    </script>
    
    <link href="SD/CaseUser/Atention/DynamicSTEFAtentionCase.css" rel="stylesheet" />   
    <link href="SD/CaseManager/Atention/CaseAtention.css" rel="stylesheet" />   
    <link href="Css/index.css" rel="stylesheet" />

    <%--temporal CSS--%>
    <link href="Css/Source/NativeSources.css" rel="stylesheet" />
    <link href="Css/Source/GlobalSources.css" rel="stylesheet" />
    <link href="Css/LoginSources.css" rel="stylesheet" />
    <link rel="stylesheet" href="Css/mainSource.css" id="linkestilo" /> 


    <link href="Css/Component/UCode/VCL/GridControlCF/ionicons/css/ionicons.css" rel="stylesheet" />
    <link href="Css/Component/UCode/VCL/GridControlCF/loader.css" rel="stylesheet" />
    <link href="Css/SystemStyle.css" rel="stylesheet" />
    
    
    <style type="text/css">
        /*menu a copiar a en alguna css*/
        .menuContenedorMobil {
            color: #cccccc !important;
        }

            .menuContenedorMobil a {
                color: #cccccc !important;
            }

            .menuContenedorMobil label {
                color: #cccccc !important;
            }

            .menuContenedorMobil button {
                background-color: transparent !important;
                border: 1px solid #cccccc !important;
            }
        /*fin de menu*/
        .ColorTab > a {
            color: #fff;
        }
        
    </style>
</head>
<body style="height: auto; min-height: 100%;">
    <div style="height: auto; min-height: 100%;"></div>
    <%--    <form id="form1" runat="server">
    <div>
    
    </div>
    </form>--%>

</body>
</html>
