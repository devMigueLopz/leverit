﻿

ItHelpCenter.SD.CaseUser.NewCase.TUfrSDCaseNew.prototype.InitializeComponent = function (ObjHtml) {
    var _this = this.TParent();
    ObjHtml.innerHTML = ""; 
    try {
        
       // $(ObjHtml).css("padding", "0px 3px");

        //ESTRUCTURA  
        //div1 se cargara los objetos de seeksearch
        var div1 = new TVclStackPanel(ObjHtml, "NC_div1" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
        var div1Cont = div1.Column[0].This;
        this.DivSeekSearch = div1Cont;
        $(div1.Row.This).css("padding-left", "0px");
        $(div1.Row.This).css("padding-right", "0px"); 
        $(div1.Row.This).css("padding-top", "0px");        

        //div2 se cargara los elementos de newcase
        var div2 = new TVclStackPanel(ObjHtml, "NC_div2" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
        var div2Cont = div2.Column[0].This;
        this.DivContCase = div2Cont;
        $(div2.Row.This).css("padding-left", "0px");
        $(div2.Row.This).css("padding-right", "0px"); 
        $(div2.Row.This).css("padding-top", "0px");

        ///divs principales de newcase (11)
        var div2_1 = new TVclStackPanel(div2Cont, "div2_1_" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
        var div2_1Cont = div2_1.Column[0].This; // Label title

        var div2_2 = new TVclStackPanel(div2Cont, "div2_2_" + _this.ID, 2, [[12, 12], [8, 4], [8, 4], [9, 3], [9, 3]]);
        var div2_2_1Cont = div2_2.Column[0].This; // labels Category
        var div2_2_2Cont = div2_2.Column[1].This; // btn seek

        ////dos paneles mas en la columna 1 del div2_2
        var div2_2_1_1 = new TVclStackPanel(div2_2_1Cont, "div2_2_1_1_" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
        var div2_2_1_1Cont = div2_2_1_1.Column[0].This; //label category 1
        var div2_2_1_2 = new TVclStackPanel(div2_2_1Cont, "div2_2_1_2_" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
        var div2_2_1_2Cont = div2_2_1_2.Column[0].This; //label category 2

        var div2_3 = new TVclStackPanel(div2Cont, "div2_3_" + _this.ID, 2, [[12, 12], [7, 5], [7, 5], [7, 5], [7, 5]]);
        var div2_3_1Cont = div2_3.Column[0].This; //contenedor de subject
        var div2_3_2Cont = div2_3.Column[1].This; //contenedor de urgency
        //paneles de cada contenedor 
        //-Subject
        var div2_3_1_1 = new TVclStackPanel(div2_3_1Cont, "div2_3_1_1_" + _this.ID, 2, [[9, 3], [9, 3], [10, 2], [11, 1], [11, 1]]);
        var div2_3_1_1_1Cont = div2_3_1_1.Column[0].This; //label   Subject
        var div2_3_1_1_2Cont = div2_3_1_1.Column[1].This; // img?
        var div2_3_1_2 = new TVclStackPanel(div2_3_1Cont, "div2_3_1_2_" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
        var div2_3_1_2_1Cont = div2_3_1_2.Column[0].This; // text Subject
        //-Urgency
        var div2_3_2_1 = new TVclStackPanel(div2_3_2Cont, "div2_3_2_1_" + _this.ID, 2, [[9, 3], [9, 3], [10, 2], [10, 2], [10, 2]]);
        var div2_3_2_1_1Cont = div2_3_2_1.Column[0].This; // label Urgency
        var div2_3_2_1_2Cont = div2_3_2_1.Column[1].This; // img ?

        var div2_3_2_2 = new TVclStackPanel(div2_3_2Cont, "div2_3_2_2_" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
        var div2_3_2_2Cont = div2_3_2_2.Column[0].This; //combo Urgency

        var div2_4 = new TVclStackPanel(div2Cont, "div2_4_" + _this.ID, 2, [[10, 2], [10, 2], [11, 1], [11, 1], [11, 1]]);
        var div2_4_1Cont = div2_4.Column[0].This; // label description
        var div2_4_2Cont = div2_4.Column[1].This; // img ?

        var div2_5 = new TVclStackPanel(div2Cont, "div2_5_" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
        var div2_5Cont = div2_5.Column[0].This;//   textarea

        var div2_6 = new TVclStackPanel(div2Cont, "div2_6_" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
        var div2_6Cont = div2_6.Column[0].This;

        var div2_7 = new TVclStackPanel(div2Cont, "div2_7_" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
        var div2_7Cont = div2_7.Column[0].This;

        var div2_8 = new TVclStackPanel(div2Cont, "div2_8_" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
        var div2_8Cont = div2_8.Column[0].This;

        var div2_9 = new TVclStackPanel(div2Cont, "div2_9_" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
        var div2_9Cont = div2_9.Column[0].This; //Botones principales
        var div2_9_1 = new TVclStackPanel(div2Cont, "div2_9_1_" + _this.ID, 3, [[0, 6, 6], [4, 4, 4], [6, 3, 3], [6, 3, 3], [8, 2, 2]]);
        var div2_9_1_1Cont = div2_9_1.Column[0].This; //vacio a la izquierda
        var div2_9_1_2Cont = div2_9_1.Column[1].This; //BTN SEND CASE
        var div2_9_1_3Cont = div2_9_1.Column[2].This; //BTN OUT
        div2_9_1.Column[1].PaddingBottom = "10px";
        div2_9_1.Column[2].PaddingBottom = "10px";

          
        //ELEMENTOS
        //titulo
        var labTitleSelect = new Vcllabel(div2_1Cont, "labTitleSelect_" + _this.ID, TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "labTitleSelect"));
        if (SysCfg.App.Properties.Device.value == SysCfg.App.TDevice.Desktop.value) {
            $(div2_1Cont).css("font-size", "20px");
        } else {
            $(div2_1Cont).css("font-size", "15px");
        }
      
        $(div2_1Cont).addClass("CaseTitle");

        //labels category detail
        var lbCategorySelect = new Vcllabel(div2_2_1_1Cont, "lbCategorySelect_" + _this.ID, TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "lbCategorySelect"));

        _this.lbCategorySelectS = new Vcllabel(div2_2_1_1Cont, "lbCategorySelectS_" + _this.ID, TVCLType.BS, TlabelType.H0, "");
        _this.lbCategorySelectS.innerText = _this.PathCategory;

        var lblDetailSelect = new Vcllabel(div2_2_1_2Cont, "lblDetailSelect_" + _this.ID, TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "lblDetailSelect"));

        _this.lblDetailSelectS = new Vcllabel(div2_2_1_2Cont, "lblDetailSelectS_" + _this.ID, TVCLType.BS, TlabelType.H0, "");
        _this.lblDetailSelectS.innerText = _this.PhraseCategory;

        
        var imgHelp0 = new TVclImagen(div2_2_2Cont, "msg_help0_" + _this.ID);
        imgHelp0.onClick = function () {
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "msg_help5"));
        }
        imgHelp0.Src = "image/ImgMono/16/information.png";
        $(imgHelp0.This).css('cursor', "Pointer");
        $(imgHelp0.This).css('height', "auto");
        $(imgHelp0.This).css('float', "right");
        $(imgHelp0.This).css('margin-top', "20px");

        //**boton seek 
        var btnSearch = new TVclButton(div2_2_2Cont, "btnSearch_" + _this.ID);
        btnSearch.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnCambiar");
        btnSearch.VCLType = TVCLType.BS;
        btnSearch.Src = "image/24/Search.png";
        btnSearch.onClick = function myfunction() {
            _this.seeksearch();
        }
        if (SysCfg.App.Properties.Device.value == SysCfg.App.TDevice.Desktop.value) {
            $(btnSearch.This).css("float", "right");
        } else {
            $(btnSearch.This).css("float", "left");
            $(btnSearch.This).css("width", "70%");
        }
        $(btnSearch.This).css('padding', "5px 10px 5px 10px");
        $(btnSearch.This).css("margin-right", "10px");

        //label subject / img help / textbox
        var labSubject = new Vcllabel(div2_3_1_1_1Cont, "labSubject_" + _this.ID, TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "labSubject"));
        var imgHelp1 = Uimg(div2_3_1_1_2Cont, "msg_help1_" + _this.ID, "image/ImgMono/16/information.png", "");
        $(imgHelp1).css('cursor', "Pointer");
        //$(imgHelp1).css('width', "14px");
        $(imgHelp1).css('height', "auto");
        $(imgHelp1).css('float', "right");
        imgHelp1.addEventListener("click", function () {
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "msg_help1"));
        });

        _this.TxtSubject = new TVclTextBox(div2_3_1_2_1Cont, "TxtSubject_" + _this.ID);
        $(_this.TxtSubject.This).css('width', "100%");
        _this.TxtSubject.This.innerText = _this.PhraseSearch;
        $(_this.TxtSubject.This).addClass("TextFormat");


        //label Urgency / img help / Combo Urgency
        var labComboUrgency = new Vcllabel(div2_3_2_1_1Cont, "labComboUrgency_" + _this.ID, TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "lbComboUrgency"));
        var imgHelp3 = Uimg(div2_3_2_1_2Cont, "msg_help3_" + _this.ID, "image/ImgMono/16/information.png", "");
        $(imgHelp3).css('cursor', "Pointer");
        //$(imgHelp3).css('width', "14px");
        $(imgHelp3).css('height', "auto");
        $(imgHelp3).css('float', "right");
        imgHelp3.addEventListener("click", function () {
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "msg_help3"));
        });

        //Cargar el combo de Urgency
        var MDURGENCYList = Persistence.Profiler.CatalogProfiler.MDURGENCYList;

        _this.CmbUrgency = new TVclComboBox2(div2_3_2_2Cont, "CmbUrgency_" + _this.ID);
        for (var i = 0; i < MDURGENCYList.length; i++) {
            var VclComboBoxItem = new TVclComboBoxItem();
            VclComboBoxItem.Value = MDURGENCYList[i].IDMDURGENCY;
            VclComboBoxItem.Text = MDURGENCYList[i].URGENCYNAME;

            _this.CmbUrgency.AddItem(VclComboBoxItem);
        }
        _this.CmbUrgency.onChange = function () {
            _this.IDMDURGENCY = _this.CmbUrgency.Value;
            _this.Show_Lifestatus();
        }
        $(_this.CmbUrgency.This).css('width', "100%");
        $(_this.CmbUrgency.This).addClass("TextFormat");

        _this.CmbUrgency.selectedIndex = MDURGENCYList.length-1;
        _this.IDMDURGENCY = MDURGENCYList[MDURGENCYList.length - 1].IDMDURGENCY;




        //label description / img help / textarea
        var labAreaText = new Vcllabel(div2_4_1Cont, "labAreaText_" + _this.ID, TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "labAreaText"));
        var imgHelp2 = Uimg(div2_4_2Cont, "msg_help2_" + _this.ID, "image/ImgMono/16/information.png", "");
        $(imgHelp2).css('cursor', "Pointer");
        // $(imgHelp2).css('width', "14px");
        $(imgHelp2).css('height', "auto");
        $(imgHelp2).css('float', "right");
        imgHelp2.addEventListener("click", function () {
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "msg_help2"));
        });
        _this.TextDescription = new TVclMemo(div2_5Cont, "AreaText_" + _this.ID)
        $(_this.TextDescription.This).css('width', "100%");
        $(_this.TextDescription.This).css('height', "120px");
        $(_this.TextDescription.This).addClass("TextFormat");

        //metodo de notificacion**************************************************************************************************************************
        var MethodHd = new TVclStackPanel(div2_6Cont, "DivMethodHd_" + _this.ID, 2, [[12, 12], [8, 4], [8, 4], [8, 4], [8, 4]]);
        var MethodHd_1Cont = MethodHd.Column[0].This;
        var MethodHd_2Cont = MethodHd.Column[1].This;  // Img adress / label 2

        var MethodHd_1_1 = new TVclStackPanel(MethodHd_1Cont, "MethodHd_1_1_" + _this.ID, 2, [[10, 2], [10, 2], [11, 1], [11, 1], [11, 1]]);
        var MethodHd_1_1_1_1Cont = MethodHd_1_1.Column[0].This; //label 1
        var MethodHd_1_1_1_2Cont = MethodHd_1_1.Column[1].This; //img help
        var MethodHd_1_2 = new TVclStackPanel(MethodHd_1Cont, "MethodHd_1_2_" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
        _this.MethodHd_1_1_2Cont = MethodHd_1_2.Column[0].This; // Combo

        var MethodHd_2_1 = new TVclStackPanel(MethodHd_2Cont, "MethodHd_2_1_" + _this.ID, 2, [[12, 12], [12, 12], [12, 12], [12, 12], [12, 12]]);
        var MethodHd_2_1_1Cont = MethodHd_2_1.Column[0].This; // img help
        var MethodHd_2_1_2Cont = MethodHd_2_1.Column[1].This; // Btn Change


        //---------------------------------------------
        _this.labContactCmb = new TVcllabel(MethodHd_1_1_1_1Cont, "labComboMetodo_" + _this.ID, TlabelType.H0);
        _this.labContactCmb.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lbComboMetodo");
        _this.labContactCmb.VCLType = TVCLType.BS;
        $(MethodHd_1_1_1_1Cont).css("max-height", "20px");
         

        _this.labContactCmbMsg = new TVcllabel(MethodHd_1_1_1_1Cont, "labContactCmbMsg" + _this.ID, TlabelType.H0);
        _this.labContactCmbMsg.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "labContactCmbMsg");
        _this.labContactCmbMsg.VCLType = TVCLType.BS;
        _this.labContactCmbMsg.Visible = false;
        _this.labContactCmbMsg.This.style.color = "#B40404";
        $(_this.labContactCmbMsg.This).css("margin-left", "15px");
        $(_this.labContactCmbMsg.This).css("font-weight", "normal");
        $(_this.labContactCmbMsg.This).css("font-style", "oblique");

        var imgHelp4 = new TVclImagen(MethodHd_1_1_1_2Cont, "msg_help4_" + _this.ID);
        imgHelp4.onClick = function () {
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "msg_help4"));
        }
        imgHelp4.Src = "image/ImgMono/16/information.png";
        $(imgHelp4.This).css('cursor', "Pointer");
        // $(imgHelp4.This).css('width', "14px");
        $(imgHelp4.This).css('height', "auto");
        $(imgHelp4.This).css('float', "right");

        var ComboMethod = _this.loadComboMetodo(_this.MethodHd_1_1_2Cont);

        //$(MethodHd_2_1_1Cont).css("border", "1px solid red");
        var imgHelp5 = new TVclImagen(MethodHd_2_1_2Cont, "msg_help5_" + _this.ID);
        imgHelp5.onClick = function () {
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "msg_help5"));
        }
        imgHelp5.Src = "image/ImgMono/16/information.png";
        $(imgHelp5.This).css('cursor', "Pointer");
        $(imgHelp5.This).css('height', "auto");
        $(imgHelp5.This).css('float', "right"); 
        $(imgHelp5.This).css('margin-top', "20px");

        var btnContact = new TVclButton(MethodHd_2_1_2Cont, "btnContact_" + _this.ID);
        btnContact.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnMethod");
        btnContact.VCLType = TVCLType.BS;
        btnContact.Src = "image/24/Addressbook.png"
        btnContact.onClick = function myfunction() {
            _this.btnContact_Click(_this, btnContact);

        }
        $(btnContact.This).css("width", "90%");

        if (SysCfg.App.Properties.Device.value == SysCfg.App.TDevice.Desktop.value) {
            $(btnContact.This).css("float", "right");
        } else {
            $(btnContact.This).css("float", "left");
        }
        $(btnContact.This).css('padding', "5px 10px 5px 10px");
        $(btnContact.This).css('margin-top', "0px");
        $(btnContact.This).css("margin-right", "10px");

        //----------------------------------------------------------------------------------------
        //LIFEESTATUS 
        $(div2_7Cont).css("display", "none");
        _this.DivLifeStatus = div2_7Cont;

        //Attach********************************************************************************************************

        var Attach = new TVclStackPanel(div2_8Cont, "Attach_" + _this.ID, 2, [[12, 12], [12, 12], [12, 12], [12, 12], [12, 12]]);
        var Attach_1Cont = Attach.Column[0].This; //img help
        var Attach_2Cont = Attach.Column[1].This;  // dropdownpanel

        $(Attach_1Cont).css("margin-bottom", "5px");
        var imgHelp6 = new TVclImagen(Attach_1Cont, "imgHelp6" + _this.ID);
        imgHelp6.onClick = function () {
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "msg_help6"));
        }
        imgHelp6.Src = "image/ImgMono/16/information.png";
        $(imgHelp6.This).css('cursor', "Pointer");
        $(imgHelp6.This).css('height', "auto");
        $(imgHelp6.This).css('float', "right");

        ///divs para el dropdownpanel
        var AttachdHd = new TVclStackPanel("", "DivAttachdHd_" + _this.ID, 2, [[5, 7], [5, 7], [5, 7], [5, 7], [5, 7]]);
        var AttachdHd_1Cont = AttachdHd.Column[0].This; // label 
        var AttachdHd_2Cont = AttachdHd.Column[1].This;  // Img attached

        var AttachdBd = new TVclStackPanel("", "DivAttachBd_" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
        var AttachdBdCont = AttachdBd.Column[0].This;  // Img attached

        //---------------------------------------------  
        var labelAttach1 = new Vcllabel(AttachdHd_1Cont, "labAttach_" + _this.ID, TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "labAttach"));
        var labelresultAttached = new Vcllabel(AttachdHd_1Cont, "labAttachS_" + _this.ID, TVCLType.BS, TlabelType.H0, "0");
        $(labelresultAttached).css('margin-left', "10px");
        labelresultAttached.innerText = 0;

        var labAttached2 = new TVcllabel(AttachdHd_2Cont, "labAttached2_", TlabelType.H0);
        labAttached2.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnAttach");
        labAttached2.VCLType = TVCLType.BS;
        $(labAttached2.This).css("float", "right");
        $(labAttached2.This).css("margin-left", "10px");

        var imgAttached = new TVclImagen(AttachdHd_2Cont, "");
        imgAttached.onClick = function () { }
        var dirimg = "image/24/attachment.png"
        imgAttached.Src = dirimg;
        $(imgAttached.This).css("float", "right");

        _this.Attach = new ItHelpCenter.SD.Shared.Attached.UfrSDAttached(AttachdBdCont, _this.ID, function () {

        }, 0, _this.IDCMDBUSER, 4, -1, false, new Array());



        //----------------------------------------------------------------------------------------
        var VclDropDownPanelAtt = new TVclDropDownPanel(Attach_2Cont, "");
        VclDropDownPanelAtt.BoderTopColor = "green";
        VclDropDownPanelAtt.BoderAllColor = "gray";
        VclDropDownPanelAtt.BackgroundColor = "white";
        VclDropDownPanelAtt.Image.This.style.marginRight = "10px";
        VclDropDownPanelAtt.FunctionOpen = function () {

        }
        VclDropDownPanelAtt.FunctionClose = function () {
            labelresultAttached.innerText = _this.Attach.NRows;
        }

        VclDropDownPanelAtt.AddToHeader(AttachdHd.Row.This);
        VclDropDownPanelAtt.AddToBody(AttachdBdCont);
        VclDropDownPanelAtt.MarginRight = "0px";
        VclDropDownPanelAtt.MarginLeft = "0px";

        //if (UsrCfg.Version.IsProduction) {
        //    $(div2_8Cont).css("display", "none");
        //}


        //----------------------------------------------------------------------------------------

        //botones***********************************************************************************************
        var btnQuit = new TVclButton(div2_9_1_3Cont, "btnQuit_" + _this.ID);
        btnQuit.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnOut");
        btnQuit.VCLType = TVCLType.BS;
        var dirimg = "image/24/Logout.png"
        btnQuit.Src = dirimg;
        btnQuit.onClick = function myfunction() {
            _this.CallbackModalResult();
        }
        $(btnQuit.This).css("float", "right");
        $(btnQuit.This).css("margin-left", "20px");
        $(btnQuit.This).css("margin-bottom", "10px");        
        $(btnQuit.This).css('width', "100%");
        $(btnQuit.This).css("max-width", "200px");

        _this.DivbtnAdd = div2_9_1_2Cont;
     
        //--------------------------------------------------------------------------------------------------
        //div extrafilds          


        //ESTILOS GENERALES 
        //$(_this.ObjectHtml).css("padding-bottom", "10px");
        //$(_this.ObjectHtml).css("padding-top", "0px");

        $(div2_2.Row.This).css("margin-top", "10px");
        $(div2_3.Row.This).css("margin-top", "10px");
        $(div2_4.Row.This).css("margin-top", "10px");
        $(div2_6.Row.This).css("margin-top", "10px");
        $(div2_7.Row.This).css("margin-top", "10px");
        $(div2_8.Row.This).css("margin-top", "10px");
        $(div2_9.Row.This).css("margin-top", "10px");

        $(".CaseTitle").css('background', "#85b9d8");
        $(".CaseTitle").css('padding-Top', "4px");
        $(".CaseTitle").css('padding-bottom', "4px");

        $(div2_3_1Cont).css("padding-top", "10px"); 
        $(div2_3_2Cont).css("padding-top", "10px");
        $(div2_4.Row.This).css("padding-top", "10px");
        $(MethodHd_1Cont).css("padding-top", "10px");
        $(MethodHd_2Cont).css("padding-top", "10px"); 
        _this.Show_Lifestatus();
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.CaseUser.NewCase.TUfrSDCaseNew.prototype.InitializeComponent ", e);
    }

}

