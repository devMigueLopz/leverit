﻿//Parametros
//inObject = objeto html donde contendra el NewCase
//inCallback = Funcion de retorno 
//inIDCategory, inPhraseCategory, inPathCategody, inPhraseSearch

ItHelpCenter.SD.CaseUser.NewCase.TUfrSDCaseNew = function (inObject, incallback, inIDCategory, inPhraseCategory, inPathCategory, inPhraseSearch) {  
    this.TParent = function () {
        return this;
    }.bind(this);

    var _this = this.TParent();
    this.ObjectHtml = inObject;
    this.CallbackModalResult = incallback;  
    this.DBTranslate = null;
    this.ID = 0;
    this.DivbtnAdd = null;
    this.contact = null;
    this.Attach = null;
    this.IDSDCASE = -1;
    this.IDMDURGENCY = 0;
    this.DivSeekSearch = null;
    this.DivContCase = null;
    this.DivLifeStatus = null;

    this.lblDetailSelectS = null;
    this.lbCategorySelectS = null;
    this.TxtSubject = null;
  
    this.IDMDCATEGORYDETAIL = inIDCategory; 
    this.PhraseCategory = inPhraseCategory;
    this.PathCategory = inPathCategory;
    this.PhraseSearch = inPhraseSearch;
    this.IDCMDBUSER = UsrCfg.Properties.UserATRole.User.IDCMDBUSER;
    this.IDCONTACTTYPE = 0;

    //TRADUCCION          
    this.Mythis = "frSDCaseNew";

    UsrCfg.Traslate.GetLangText(_this.Mythis, "labSubject", "Subject:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "labAreaText", "Describes what happened:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "msg_help1", "Enter the title for your problem");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "msg_help2", "Enter a description of your problem");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "msg_help3", "Select the urgency of your problem");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "msg_help4", "Select method of notice");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "msg_help5", "Edit your contact information");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "msg_help6", "Attach files. i.e. Screenshot of error messages");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "msgMandatory", "Unable to process this action, until you complete the following data:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnAdd", "Send Case");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnAttach", "Attach the file");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnOut", "Out");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnContact", "My contact");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "SNode1", "Home");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnMethod", "Add Notification methods");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "labTitleSelect", "Complete the following form to send case");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lblDetailSelect", "Detail:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lbCategorySelect", "Category:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lbComboUrgency", "Urgency:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lbComboMetodo", "Notification Method:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "labAttach", "Attached:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnCategoryDefaul", "I do not know what category");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "labContactCmbMsg", "Add more notification methods");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1", "Could not create Case.");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2", "We created the ticket number:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnCambiar", "Change");

    _this.LoadComponent();
     
    
}
ItHelpCenter.SD.CaseUser.NewCase.TUfrSDCaseNew.prototype.LoadComponent = function () {
    try {
        var _this = this.TParent();
        //agregar el archivos externos   
        _this.importarScript(SysCfg.App.Properties.xRaiz + "SD/Shared/Category/SeekSearch/UCategorySeekSearch.js", function (e) {
            _this.importarScript(SysCfg.App.Properties.xRaiz + "SD/Shared/Category/Basic/CategoryBasic.js", function () {
                //construir elementos de newCase                       
                _this.GET_ID_EXTRAFILDS(); //Consultar id Extrafilds                    
                _this.InitializeComponent(_this.ObjectHtml); //Construir elementos newCase                    
            }, function (e) {
            });
        }, function (e) {
        });
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.CaseUser.NewCase.TUfrSDCaseNew.prototype.LoadComponent ", e);
    }    
}

ItHelpCenter.SD.CaseUser.NewCase.TUfrSDCaseNew.prototype.GET_ID_EXTRAFILDS = function () {
    try {
        var _this = this.TParent();
        //llamar el IDCASEEF         
        direc = SysCfg.App.Properties.xRaiz + "SD/CaseUser/NewCase/SDCaseNew.svc/SDCaseEFNew";
        async: false,
        dataArray = {};
        $.ajax({
            async: false,
            type: "POST",
            url: direc,
            data: JSON.stringify(dataArray),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                _this.ID = response.d;
            },
            error: function (error) {
                //enviar a la web
                alert('error: ' + error.txtDescripcion + " \n Location: Category Seek Search, SearchID / newCategoryDetail");
            }
        });
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.CaseUser.NewCase.TUfrSDCaseNew.prototype.GET_ID_EXTRAFILDS ", e);
    }
    
}

ItHelpCenter.SD.CaseUser.NewCase.TUfrSDCaseNew.prototype.seeksearch = function () {
    try {
        var _this = this.TParent();
        var SeekNormal = false;

        $(_this.DivSeekSearch).css('display', "block");
        $(_this.DivContCase).css('display', "none");

        _this.DivSeekSearch.innerHTML = "";
        //Parameters: (6) = 1.HTML Container, 2.Function Back, 3.Modo busqueda, 4.Ver boton cambio de modo (true - False), 5.Ver mensaje resultado, 6.Frase predeterminada    
        var _Category = new ItHelpCenter.SD.Shared.Category.TCategoryManger(
           _this.DivSeekSearch,
            function (OutRes) {
                _this.IDMDCATEGORYDETAIL = OutRes.ID;
                _this.PhraseCategory = OutRes.Detail;
                _this.PathCategory = OutRes.Path;
                _this.PhraseSearch = OutRes.PhraseIn;

                _this.lblDetailSelectS.innerText = _this.PhraseCategory;
                _this.lbCategorySelectS.innerText = _this.PathCategory;
                _this.TxtSubject.This.innerText = _this.PhraseSearch;

                $(_this.DivSeekSearch).css('display', "none");
                $(_this.DivContCase).css('display', "block");

                _this.Show_Lifestatus();
            },
            ItHelpCenter.SD.Shared.Category.Mode.SeekSearch,
            true, false, "", false);
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.CaseUser.NewCase.TUfrSDCaseNew.prototype.seeksearch ", e);
    }
    
     
}

ItHelpCenter.SD.CaseUser.NewCase.TUfrSDCaseNew.prototype.Show_Lifestatus = function () {
    var _this = this.TParent();
    try {
        _this.Load_Lifestatus(_this.DivLifeStatus);
        $(_this.DivLifeStatus).css('display', "block");
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("UfrSDCaseNew.js ItHelpCenter.SD.CaseUser.NewCase.TUfrSDCaseNew.prototype.InitializeComponent", e);
        $(_this.DivLifeStatus).css('display', "none");
    }

}

ItHelpCenter.SD.CaseUser.NewCase.TUfrSDCaseNew.prototype.Load_Lifestatus = function (ElementDiv) {    //cargar lifestatus 
    try {
        ElementDiv.innerHTML = "";
        var _this = this.TParent();
        direc = SysCfg.App.Properties.xRaiz + "SD/CaseUser/NewCase/SDCaseNew.svc/GetSLAbyCATEGORY";
        var modelo = null;

        dataArray = { "IDSDCASEEF": _this.ID, "IDMDCATEGORYDETAIL": _this.IDMDCATEGORYDETAIL, "IDSDRESPONSETYPE": 0, "CASE_ISMAYOR": false, "IDMDURGENCY":  parseInt(_this.IDMDURGENCY) };
        $.ajax({
            async: false,
            type: "POST",
            url: direc,
            data: JSON.stringify(dataArray),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                //var ElementDiv = inObject;// document.getElementById("IDEXTRAFILDS");
                //_this.btnGrabarETArray;   
                try {
                    var DynamicSTEFNewCase = new ItHelpCenter.SD.CaseUser.NewCase.TDynamicSTEFNewCase(
                    ElementDiv,
                    function (_thisOut) {
                        var btnGrabarETArray = _thisOut.btnGrabarETArray;
                        //BOTON ENVIAR CASO*************************************
                        _this.DivbtnAdd.innerHTML = "";
                        var btnAdd = new TVclButton(_this.DivbtnAdd, "btnAdd_" + _this.ID);
                        btnAdd.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnAdd");
                        btnAdd.VCLType = TVCLType.BS;
                        var dirimg = "image/24/-add.png"
                        btnAdd.Src = dirimg;
                        btnAdd.onClick = function myfunction() {
                            var PuedoContinuar = true;
                            for (var i = 0; i < btnGrabarETArray.length; i++) {
                                var dis = btnGrabarETArray[i].Enabled;
                                if (dis) {
                                    btnGrabarETArray[i].Click();
                                    var OutTag = btnGrabarETArray[i].Tag;
                                    if (OutTag.isMANDATORY) {
                                        PuedoContinuar = false;
                                        var StrOut = UsrCfg.Traslate.GetLangText(_this.Mythis, "msgMandatory") + OutTag.StrOut;
                                        alert(StrOut);
                                        ///

                                    }
                                }
                            }
                            if (PuedoContinuar) _this.SendCase(_this.ID, response.d.MT_IDMDMODELTYPED, response.d.IDSLA, response.d.IDMDIMPACT);
                        }
                        $(btnAdd.This).css("float", "right");
                        $(btnAdd.This).css("font-size", "18px");
                        $(btnAdd.This).css("padding-left", "15px");
                        $(btnAdd.This).css("padding-right", "15px");
                        $(btnAdd.This).css('width', "100%");
                        $(btnAdd.This).css("max-width", "200px");
                    },//null,
                    response,
                    _this.ID
                    );
                } catch (e) {
                    SysCfg.Log.Methods.WriteLog("UfrSDCaseNew.js ItHelpCenter.SD.CaseUser.NewCase.TUfrSDCaseNew.prototype.Load_Lifestatus", e);

                }

                Modelo = response.d;
            },
            error: function (error) {
                //enviar a la web
                alert('error: ' + error.txtDescripcion + " \n Location: GetSLAbyCATEGORY");
            }
        });
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.CaseUser.NewCase.TUfrSDCaseNew.prototype.Load_Lifestatus ", e);
    }    
}


ItHelpCenter.SD.CaseUser.NewCase.TUfrSDCaseNew.prototype.loadComboMetodo = function (objeto) {
    try {
        var _this = this.TParent();

        objeto.innerHTML = "";

        var ComboMethod = new TVclComboBox2(objeto, "CmbMethod_" + _this.ID);
        $(ComboMethod.This).css('width', "100%");
        $(ComboMethod.This).addClass("TextFormat");
        ComboMethod.onChange = function () {
            _this.IDCONTACTTYPE = ComboMethod.Value;
        }

        var Param = new SysCfg.Stream.Properties.TParam();
        try {
            Param.Inicialize();
            Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBUSER.FieldName, _this.IDCMDBUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            var OpenDataSetContactType = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "LOAD_CMDBCONTACTTYPE_STYLE", Param.ToBytes());
            if (OpenDataSetContactType.DataSet.RecordCount > 0) {
                _this.labContactCmbMsg.Visible = (OpenDataSetContactType.DataSet.RecordCount == 0);

                //llenar combo
                var index = -1;
                for (var i = 0; i < OpenDataSetContactType.DataSet.RecordCount; i++) {
                    var VclComboBoxItem = new TVclComboBoxItem();
                    VclComboBoxItem.Value = OpenDataSetContactType.DataSet.Records[i].Fields[0].Value;
                    VclComboBoxItem.Text = OpenDataSetContactType.DataSet.Records[i].Fields[1].Value + " ( " + OpenDataSetContactType.DataSet.Records[i].Fields[2].Value + " ) ";

                    ComboMethod.AddItem(VclComboBoxItem);

                    if (OpenDataSetContactType.DataSet.Records[i].Fields[2].Value.trim() != "" && index == -1) {
                        index = i;
                    }
                }

                if (index != -1) {
                    ComboMethod.selectedIndex = index;
                    _this.IDCONTACTTYPE = OpenDataSetContactType.DataSet.Records[index].Fields[0].Value;
                }
            }
        } finally {
            Param.Destroy();
        }

        return ComboMethod;
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.CaseUser.NewCase.TUfrSDCaseNew.prototype.loadComboMetodo ", e);
        return null;
    }
    
}
 

ItHelpCenter.SD.CaseUser.NewCase.TUfrSDCaseNew.prototype.btnContact_Click = function (sender, e) {
    var _this = sender;

    try {
        var Modal = new TVclModal(document.body, null, "");
        if (SysCfg.App.Properties.Device.value == SysCfg.App.TDevice.Desktop.value) {
            Modal.Width = "70%";
        } else {
            Modal.Width = "100%";
        }
        Modal.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.Mythis, "lbComboMetodo"));
        Modal.Body.This.id = "Modal_Contact_" + _this.ID;
        $(Modal.Body.This).css("padding", "0px");
        _this.contact = new ItHelpCenter.SD.Shared.Contact.UfrCMDBContact(Modal.Body.This, _this.ID, function (sender) {
            Modal.CloseModal();
            _this.loadComboMetodo(_this.MethodHd_1_1_2Cont);
        }, 0, _this);
        Modal.ShowModal();
        Modal.FunctionClose = function () {
            //_this.contact.SaveContact(function (e) {
            _this.loadComboMetodo(_this.MethodHd_1_1_2Cont);
            // });                        
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.CaseUser.NewCase.TUfrSDCaseNew.prototype.btnContact_Click ", e);
    }
}

ItHelpCenter.SD.CaseUser.NewCase.TUfrSDCaseNew.prototype.SendCase = function (inIDCASEEF, inMT_IDMDMODELTYPED, inIDSLA, inIDMDIMPACT) {
    var _this = this.TParent();



    var SDCaseUserNew = new UsrCfg.SD.TSDCaseUserNew();
    SDCaseUserNew.SDCASEEF.IDSDCASEEF = inIDCASEEF;
    SDCaseUserNew.SDCASE.CASE_ISMAYOR = false;
    SDCaseUserNew.SDCASE.CASE_DESCRIPTION = _this.TextDescription.Text;
    SDCaseUserNew.SDCASE.CASE_TITLE = _this.TxtSubject.Text;
    SDCaseUserNew.SDCASEMT.IDMDCATEGORYDETAIL = parseInt(_this.IDMDCATEGORYDETAIL);
    SDCaseUserNew.SDCASEMT.IDMDURGENCY = parseInt(_this.IDMDURGENCY);
    SDCaseUserNew.SDCASE.IDCMDBCONTACTTYPE_USER = _this.IDCONTACTTYPE;
    SDCaseUserNew.SDCASEMT.MT_IDMDMODELTYPED = inMT_IDMDMODELTYPED;
    SDCaseUserNew.SDCASEMT.IDSLA = inIDSLA;
    SDCaseUserNew.SDCASEMT.IDMDIMPACT = inIDMDIMPACT;
    try {
        SDCaseUserNew = ItHelpCenter.SD.Methods.SDCaseUserNew(SDCaseUserNew);
        if (SDCaseUserNew.ResErr.NotError == true) {
            if (SDCaseUserNew.SDCASE.IDSDCASE < 0) {
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1"));
            } else {
                _this.IDSDCASE = SDCaseUserNew.SDCASE.IDSDCASE;

                //Guardar Attached
                _this.Attach.SaveAttach(_this.IDSDCASE, function () {

                });

                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2") + SDCaseUserNew.SDCASE.IDSDCASE);
                //cargar la pagina de tikets 
                _this.CallbackModalResult(_this.ObjectHtml, _this.IDSDCASE);

            }

        }
        else {
            SysCfg.App.Methods.ShowMessage(SDCaseUserNew.ResErr.Mesaje);
        }
    }
    catch (ex) {
        SysCfg.App.Methods.ShowMessage(ex.toString());
    }
    finally {
    }

    //try {
    //    //enviar datos al service de un nuevo case   
    //    dataArray = {
    //        "IDSDCASEEF": inIDCASEEF,
    //        "MT_IDMDMODELTYPED": inMT_IDMDMODELTYPED,
    //        "IDSLA": inIDSLA,
    //        "IDMDIMPACT": inIDMDIMPACT,
    //        "CASE_ISMAYOR": false,
    //        "CASE_DESCRIPTION": _this.TextDescription.Text,
    //        "CASE_TITLE": _this.TxtSubject.Text,
    //        "IDMDCATEGORYDETAIL": _this.IDMDCATEGORYDETAIL,
    //        "IDMDURGENCY": _this.IDMDURGENCY,
    //        "IDCMDBCONTACTTYPE_USER": _this.IDCONTACTTYPE
    //    };
    //    $.ajax({
    //        async: false,
    //        type: "POST",
    //        url: SysCfg.App.Properties.xRaiz + "SD/CaseUser/NewCase/SDCaseNew.svc/SDCaseUserNew",
    //        data: JSON.stringify(dataArray),
    //        contentType: "application/json; charset=utf-8",
    //        dataType: "json",
    //        success: function (response) {

    //            if (response.d < 0) {
    //                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1"));
    //            } else {
    //                _this.IDSDCASE = response.d;

    //                //Guardar Attached
    //                _this.Attach.SaveAttach(_this.IDSDCASE, function () {

    //                });

    //                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2") + response.d);
    //                //cargar la pagina de tikets 
    //                _this.CallbackModalResult(_this.ObjectHtml, _this.IDSDCASE);

    //            }

    //        },
    //        error: function (error) {

    //            alert('error: ' + error.txtDescripcion + " \n Location: Create a new Case / SDCaseUserNew");
    //        }
    //    });
    //} catch (e) {
    //    SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.CaseUser.NewCase.TUfrSDCaseNew.prototype.SendCase ", e);
    //}



}


ItHelpCenter.SD.CaseUser.NewCase.TUfrSDCaseNew.prototype.importarScript = function (nombre, onSuccess, onError) {
    var s = document.createElement("script");
    s.onload = onSuccess;
    s.onerror = onError;
    s.src = nombre;
    document.querySelector("head").appendChild(s);
}


//ID es IDSDCASEEF se confunde con el id del objeto 

// el botón debe crearse en un o ubicarlo para agregar array 
// el array de botones se declarado en la parte de _this
// lo div deben definirse como parte _this en extra field
