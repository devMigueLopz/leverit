﻿

ItHelpCenter.SD.CaseUser.NewCase.TDynamicSTEFNewCase = function (inObject, incallbackModalResult, inresponse, inIDSOURCE) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.Object = inObject;
    this.CallbackModalResult = incallbackModalResult;//CallbackModalResult(OutStr);        
    this.response = inresponse;
    this.IDSOURCE = inIDSOURCE;
    this.btnGrabarETArray;

    this.Mythis = "TDynamicSTEFNewCase";
    //Procesos de Utranslate.js   
  
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnNuevoET", "New");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnGrabarET", "Save");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnEliminarET", "Delete");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnCancelarET", "Cancel");
         
    _this.btnGrabarETArray = _this.CargarTabControlNewCase(_this.Object, _this.response, _this.IDSOURCE);
    _this.CallbackModalResult(_this);
}


ItHelpCenter.SD.CaseUser.NewCase.TDynamicSTEFNewCase.prototype.OutMandatory = function () {
    //var _this = this.TParent(); no aplica por que se hace un new 
    this.isMANDATORY = false;
    this.StrOut = "";
    
    return this;
}

ItHelpCenter.SD.CaseUser.NewCase.TDynamicSTEFNewCase.prototype.CargarTabControlNewCase = function (Objeto, response, IDSOURCE) {
    var _this = this.TParent();

    //var IdxIDSDWHOTOCASE = '_' + inIDSDWHOTOCASE;
    response = response.d;
    btnGrabarETArray = new Array();
    for (var i = 0; i < response.MDLIFESTATUSProfiler.MDCASESTEXTRATABLEList.length; i++) {
        if (response.MDLIFESTATUSProfiler.MDCASESTEXTRATABLEList[i].MDSERVICEEXTRATABLE.EXTRATABLE_IDSOURCE == 3) {
            var ExtraTableArr = new Array(1);
            ExtraTableArr[0] = new Array(1);
            var ExtraTable_udiv = VclDivitions(Objeto, "ExtraTable", ExtraTableArr);
            BootStrapCreateRow(ExtraTable_udiv[0].This);
            //ExtraTable_udiv[0].This.style.backgroundColor = "rgb(133,194,38)";
            BootStrapCreateColumn(ExtraTable_udiv[0].Child[0].This, [12, 12, 12, 12, 12]);
            ExtraTable_udiv[0].Child[0].This.style.backgroundColor = "#3e94ba";
            var Titulo_uh4 = Vcllabel(ExtraTable_udiv[0].Child[0].This, "", TVCLType.BS, TlabelType.H4, response.MDLIFESTATUSProfiler.MDCASESTEXTRATABLEList[i].MDSERVICEEXTRATABLE.EXTRATABLE_DESCRIPTION);
            Titulo_uh4.style.fontSize = "15px";
            Titulo_uh4.style.fontWeight = "700";
            Titulo_uh4.style.color = "white";
            var ContExtraTable_udiv = VclDivitions(Objeto, "", new Array(1));
            BootStrapCreateRow(ContExtraTable_udiv[0].This);
            var ColExtraTable_udiv = VclDivitions(ContExtraTable_udiv[0].This, "", new Array(1));
            BootStrapCreateColumn(ColExtraTable_udiv[0].This, [12, 12, 12, 12, 12]);
            for (var e = 0; e < response.MDSERVICEEXTRATABLEList.length; e++) {
                if (response.MDLIFESTATUSProfiler.MDCASESTEXTRATABLEList[i].MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE == response.MDSERVICEEXTRATABLEList[e].IDMDSERVICEEXTRATABLE) {
                    var lblSubTitulo = Vcllabel(ColExtraTable_udiv[0].This, "", TVCLType.BS, TlabelType.H5, response.MDLIFESTATUSProfiler.MDCASESTEXTRATABLEList[i].LSCHANGETABLE_COMMENTS);
                    lblSubTitulo.style.fontWeight = "bold";
                    var IdxIDSDWHOTOCASE = "_" + response.MDLIFESTATUSProfiler.MDCASESTEXTRATABLEList[i].IDMDCASESTEXTRATABLE;
                    Uinput(ColExtraTable_udiv[0].This, TImputtype.hidden,
                        "IDMDLIFESTATUSBEHAVIOR_" + response.MDLIFESTATUSProfiler.MDCASESTEXTRATABLEList[i].MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE + IdxIDSDWHOTOCASE, "1", false);
                    Uinput(ColExtraTable_udiv[0].This, TImputtype.hidden, "GRIDENABLE_" + response.MDLIFESTATUSProfiler.MDCASESTEXTRATABLEList[i].MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE + IdxIDSDWHOTOCASE, response.MDLIFESTATUSProfiler.MDCASESTEXTRATABLEList[i].MDSERVICEEXTRATABLE.GRIDENABLE, false);
                    Uinput(ColExtraTable_udiv[0].This, TImputtype.hidden, "READONLY_" + response.MDLIFESTATUSProfiler.MDCASESTEXTRATABLEList[i].MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE + IdxIDSDWHOTOCASE, response.MDLIFESTATUSProfiler.MDCASESTEXTRATABLEList[i].MDSERVICEEXTRATABLE.READONLY, false);

                    var btnGrabarET = this.UGetExtraFields(ColExtraTable_udiv[0].This, response.MDLIFESTATUSProfiler.MDCASESTEXTRATABLEList[i].IDMDCASESTEXTRATABLE, response.MDSERVICEEXTRATABLEList[e]);
                    btnGrabarETArray.push(btnGrabarET);
                    break;
                }
            }
        }
    }
    return btnGrabarETArray;
}

ItHelpCenter.SD.CaseUser.NewCase.TDynamicSTEFNewCase.prototype.CMDBCIDEFINEEXTRATABLE_SOURCETYPE = function (valor) {
    var _this = this.TParent();
    for (var item in UsrCfg.SD.Properties.TCMDBCIDEFINEEXTRATABLE_SOURCETYPE) {
        if (UsrCfg.SD.Properties.TCMDBCIDEFINEEXTRATABLE_SOURCETYPE[item].value == valor)
            return UsrCfg.SD.Properties.TCMDBCIDEFINEEXTRATABLE_SOURCETYPE[item];
    }
    //for (var i = 0; i < TCMDBCIDEFINEEXTRATABLE_SOURCETYPElength; i++) {
    //    if (TCMDBCIDEFINEEXTRATABLE_SOURCETYPE[0].value == valor)
    //        return TCMDBCIDEFINEEXTRATABLE_SOURCETYPE[i];
    //}
    //return TCMDBCIDEFINEEXTRATABLE_SOURCETYPE[0];
}

ItHelpCenter.SD.CaseUser.NewCase.TDynamicSTEFNewCase.prototype.UGetExtraFields = function (objContenedor, inIDSDWHOTOCASE, MDSERVICEEXTRATABLE) {
    var _this = this.TParent();
    var IdxIDSDWHOTOCASE = '_' + inIDSDWHOTOCASE;
    var FieldsIDExtraTables = new Array(2);
    FieldsIDExtraTables[0] = new Array(2);
    FieldsIDExtraTables[1] = new Array(2);

    FieldsIDExtraTables_udiv = VclDivitions(objContenedor, "ContExtraTableCtrls" + inIDSDWHOTOCASE, FieldsIDExtraTables);

    BootStrapCreateRow(FieldsIDExtraTables_udiv[0].This);
    BootStrapCreateColumns(FieldsIDExtraTables_udiv[0].Child, [[3, 9], [3, 9], [3, 9], [3, 9], [3, 9]]);
    var lblIDET = Vcllabel(FieldsIDExtraTables_udiv[0].Child[0].This, "", TVCLType.BS, TlabelType.H0, "IDMDST_EF" + MDSERVICEEXTRATABLE.EXTRATABLE_NAMETABLE);
    lblIDET.style.fontWeight = "normal";
    var FieldExtraTable_input = Uinput(FieldsIDExtraTables_udiv[0].Child[1].This, TImputtype.text, "FieldExtraTable" + MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE + IdxIDSDWHOTOCASE, "0", false);
    FieldExtraTable_input.setAttribute("data-NameField", "IDMDST_EF" + MDSERVICEEXTRATABLE.EXTRATABLE_NAMETABLE);
    FieldExtraTable_input.setAttribute("data-DataType", SysCfg.DB.Properties.TDataType.Int32.value);
    $(FieldExtraTable_input).attr("disabled", true);
    FieldsIDExtraTables_udiv[0].This.style.display = "none";

    BootStrapCreateRow(FieldsIDExtraTables_udiv[1].This);
    BootStrapCreateColumns(FieldsIDExtraTables_udiv[1].Child, [[3, 9], [3, 9], [3, 9], [3, 9], [3, 9]]);
    var lblIDCASE = Vcllabel(FieldsIDExtraTables_udiv[1].Child[0].This, "", TVCLType.BS, TlabelType.H0, this.CMDBCIDEFINEEXTRATABLE_SOURCETYPE(MDSERVICEEXTRATABLE.EXTRATABLE_IDSOURCE).name/*"IDSDCASEMT"*/);
    lblIDCASE.style.fontWeight = "normal";
    var FieldIDSDCASEMT_input = Uinput(FieldsIDExtraTables_udiv[1].Child[1].This, TImputtype.text, "FieldIDSDCASEMT" + MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE + IdxIDSDWHOTOCASE, "0", false);
    FieldIDSDCASEMT_input.setAttribute("data-NameField", this.CMDBCIDEFINEEXTRATABLE_SOURCETYPE(MDSERVICEEXTRATABLE.EXTRATABLE_IDSOURCE).name/*"IDSDCASEMT"*/);
    FieldIDSDCASEMT_input.setAttribute("data-DataType", SysCfg.DB.Properties.TDataType.Int32.value);
    $(FieldIDSDCASEMT_input).attr("disabled", true);
    FieldsIDExtraTables_udiv[1].This.style.display = "none";
    var ArrayControls = new Array(MDSERVICEEXTRATABLE.MDSERVICEEXTRAFIELDSList.length);

    for (var i = 0; i < MDSERVICEEXTRATABLE.MDSERVICEEXTRAFIELDSList.length; i++) {
        var FieldsIDExtraTablesDina = new Array(1);
        FieldsIDExtraTablesDina[0] = new Array(2);
        FieldsIDExtraTablesDina_udiv = VclDivitions(objContenedor, "ContExtraTableCtrls" + inIDSDWHOTOCASE + "_" + (i + 2), FieldsIDExtraTablesDina);
        BootStrapCreateRow(FieldsIDExtraTablesDina_udiv[0].This);
        BootStrapCreateColumns(FieldsIDExtraTablesDina_udiv[0].Child, [[4, 8], [4, 8], [4, 8], [4, 8], [4, 8]]);
        FieldsIDExtraTablesDina_udiv[0].This.style.display = this.UGetVisibility(MDSERVICEEXTRATABLE.MDSERVICEEXTRAFIELDSList[i].IDMDLIFESTATUSPERMISSION);
        var lblField = Vcllabel(FieldsIDExtraTablesDina_udiv[0].Child[0].This, "", TVCLType.BS, TlabelType.H0, MDSERVICEEXTRATABLE.MDSERVICEEXTRAFIELDSList[i].EXTRAFIELDS_DESCRIPTION);
        lblField.style.fontWeight = "normal";
        lblField.style.fontSize = "13px";
        var ctrl = this.UConstruirControl(FieldsIDExtraTablesDina_udiv[0].Child[1].This, inIDSDWHOTOCASE, MDSERVICEEXTRATABLE.MDSERVICEEXTRAFIELDSList[i], MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE)
        ArrayControls[i] = ctrl;
    }

    var ContBotonesET = new Array(1);
    ContBotonesET[0] = new Array(1);
    var ContBotonesET_udiv = VclDivitions(objContenedor, "ContBotonesET_" + MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE + IdxIDSDWHOTOCASE, ContBotonesET);
    BootStrapCreateRow(ContBotonesET_udiv[0].This);
    BootStrapCreateColumn(ContBotonesET_udiv[0].Child[0].This, [12, 12, 12, 12, 12]);
  
    var btnNuevoET = new TVclInputbutton(ContBotonesET_udiv[0].Child[0].This, "");
    btnNuevoET.Text = UsrCfg.Traslate.GetLangText(_this.Mythis,"btnNuevoET");
    btnNuevoET.VCLType = TVCLType.BS;
    btnNuevoET.This.classList.add("btnNuevoET");
    //btnNuevoET.classList.add("btnstep");
    btnNuevoET.This.setAttribute("data-IdET", MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE);
    btnNuevoET.This.setAttribute("data-IdWHOTT", inIDSDWHOTOCASE);
    btnNuevoET.onClick = function () {
        var IdET = parseInt($(this).attr('data-IdET'));
        var ArrayComtrolsTemp = ArrayControls;
        ArrayComtrolsTemp.push(FieldIDSDCASEMT_input);
        ArrayComtrolsTemp.push(FieldExtraTable_input);
        _this.ULimpiarControles(ArrayComtrolsTemp);
        _this.UHabilitaControles(true, ArrayControls)
        $('#stateForm' + MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE + IdxIDSDWHOTOCASE).val(UsrCfg.SD.Properties.StateFormulario._Nuevo);
        _this.UHabilitaBotones(btnNuevoET, btnGrabarET, btnEliminarET, btnCancelarET, false, true, false, true, MDSERVICEEXTRATABLE.GRIDENABLE, MDSERVICEEXTRATABLE.READONLY, false);
    }

    var btnGrabarET = new VclInputbutton(ContBotonesET_udiv[0].Child[0].This, "btnGrabarET_" + IdxIDSDWHOTOCASE);
    btnGrabarET.Text = UsrCfg.Traslate.GetLangText(_this.Mythis,"btnGrabarET");
    btnGrabarET.VCLType = TVCLType.BS;
    btnGrabarET.This.classList.add("btnGrabarET");
    //btnGrabarET.classList.add("btnstep");
    btnGrabarET.This.setAttribute("data-IdET", MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE);
    btnGrabarET.This.setAttribute("data-IdWHOTT", inIDSDWHOTOCASE);
    btnGrabarET.onClick = function () {
        //***************** Validacion de Mandatoy *************
        btnGrabarET.Tag = new _this.OutMandatory();
        btnGrabarET.Tag.isMANDATORY = false;
        btnGrabarET.Tag.StrOut = "";
        var IdET = parseInt($(this).attr('data-IdET'));
        var IdWHOTT = parseInt($(this).attr('data-IdWHOTT'));
        var ArrayComtrolsTemp = ArrayControls;
        for (var i = 0; i < ArrayComtrolsTemp.length; i++) {
                               
            data_NameField = $(ArrayComtrolsTemp[i]).attr('data-NameField');
         
            data_Description = $(ArrayComtrolsTemp[i]).attr('data-Description');
            //data_Description = data_NameField;//agragar el header para no confundir el usuario //UnMDSERVICEEXTRAFIELDS.EXTRAFIELDS_DESCRIPTION;
            data_Mandatory = $(ArrayComtrolsTemp[i]).attr('data-Mandatory') == 'true';
        
            data_Val = $(ArrayComtrolsTemp[i]).val();
            data_DataType = parseInt($(ArrayComtrolsTemp[i]).attr('data-DataType'));
            var idt = "\n";
            if (btnGrabarET.Tag.isMANDATORY) idt = ", ";
        
            switch (data_DataType) {
                case SysCfg.DB.Properties.TDataType.String:

                    if (data_Mandatory && $(ArrayComtrolsTemp[i]).val() == "") {
                        btnGrabarET.Tag.StrOut += idt + data_Description;
                        btnGrabarET.Tag.isMANDATORY = true;
                    }
                    break;
                case SysCfg.DB.Properties.TDataType.Int32:
                    if (data_Mandatory && $(ArrayComtrolsTemp[i]).val() == "") {
                        btnGrabarET.Tag.StrOut += idt + data_Description;
                        btnGrabarET.Tag.isMANDATORY = true;
                    }
                    break;
                case SysCfg.DB.Properties.TDataType.Boolean:
                    break;
                case SysCfg.DB.Properties.TDataType.Text:
                    if (data_Mandatory && $(ArrayComtrolsTemp[i]).val() == "") {
                        btnGrabarET.Tag.StrOut += idt + data_Description;
                        btnGrabarET.Tag.isMANDATORY = true;
                    }
                    break;
                case SysCfg.DB.Properties.TDataType.Double:
                    if (data_Mandatory && $(ArrayComtrolsTemp[i]).val() == "") {
                        btnGrabarET.Tag.StrOut += idt + data_Description;
                        btnGrabarET.Tag.isMANDATORY = true;
                    }
                    break;
                case SysCfg.DB.Properties.TDataType.DateTime:
                    if (data_Mandatory && $(ArrayComtrolsTemp[i]).val() == "") {
                        btnGrabarET.Tag.StrOut += idt + data_Description;
                        btnGrabarET.Tag.isMANDATORY = true;
                    }
                    break;
                case SysCfg.DB.Properties.TDataType.Decimal:
                    if (data_Mandatory && $(ArrayComtrolsTemp[i]).val() == "") {
                        btnGrabarET.Tag.StrOut += idt + data_Description;
                        btnGrabarET.Tag.isMANDATORY = true;
                    }
                    break;
                default:

            }
    
        }
        if (btnGrabarET.Tag.isMANDATORY) {
            return;
        }

        //***************** End Validacion de Mandatoy *************



        var IdET = parseInt($(this).attr('data-IdET'));
        var IdWHOTT = parseInt($(this).attr('data-IdWHOTT'));

        var ArrayComtrolsTemp = ArrayControls;
        ArrayComtrolsTemp.push(FieldIDSDCASEMT_input);
        ArrayComtrolsTemp.push(FieldExtraTable_input);
        var parametros = [];
        for (var i = 0; i < ArrayComtrolsTemp.length; i++) {
            var Atributos = [];
            Atributos[0] = $(ArrayComtrolsTemp[i]).attr('data-NameField');
            Atributos[1] = $(ArrayComtrolsTemp[i]).val();
            if (SysCfg.DB.Properties.TDataType.GetEnum(parseInt($(ArrayComtrolsTemp[i]).attr('data-DataType'))) == SysCfg.DB.Properties.TDataType.DateTime) {
                var _date_ef = $(ArrayComtrolsTemp[i]).val() == "" ? new Date() : new Date($(ArrayComtrolsTemp[i]).val());
                Atributos[1] = SysCfg.Str.Protocol.DateTimeToStrProtocol(_date_ef);
            }
            Atributos[2] = $(ArrayComtrolsTemp[i]).attr('data-DataType');
            parametros[i] = Atributos;
        }
        var _stateForm = $('#stateForm' + MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE + IdxIDSDWHOTOCASE).val();


        _data = JSON.stringify({
            IDSDWHOTOCASE: inIDSDWHOTOCASE,
            IDMDSERVICEEXTRATABLE: MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE,
            arrayString: parametros,
            stateForm: _stateForm
        });

        $.ajax({
            type: "POST",
            url: SysCfg.App.Properties.xRaiz + "ST/STEditor/DynamicSTEFAtentionCase.svc/GrabarExtraTable",
            contentType: "application/json; charset=utf-8",
            data: _data,
            async:false,
            success: function (response) {

                var NroADN = _this.ContarADN(MDSERVICEEXTRATABLE.MDSERVICEEXTRAFIELDSList);

                if (_stateForm == UsrCfg.SD.Properties.StateFormulario._update) {
                    if (NroADN > 0) {
                        _this.ULimpiarControles(ArrayComtrolsTemp);
                        _this.UHabilitaControles(false, ArrayControls)
                        $('#stateForm' + MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE + IdxIDSDWHOTOCASE).val(UsrCfg.SD.Properties.StateFormulario._Nuevo);
                        _this.UHabilitaBotones(btnNuevoET, btnGrabarET, btnEliminarET, btnCancelarET, true, false, false, false, MDSERVICEEXTRATABLE.GRIDENABLE, MDSERVICEEXTRATABLE.READONLY, false);
                    }
                    else {
                        $('#stateForm' + MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE + IdxIDSDWHOTOCASE).val(UsrCfg.SD.Properties.StateFormulario._update);
                        _this.UHabilitaBotones(btnNuevoET, btnGrabarET, btnEliminarET, btnCancelarET, true, true, true, false, MDSERVICEEXTRATABLE.GRIDENABLE, MDSERVICEEXTRATABLE.READONLY, false);
                    }
                } else {
                    if (_stateForm == UsrCfg.SD.Properties.StateFormulario._update) {
                        if (NroADN > 0) {
                            _this.ULimpiarControles(ArrayComtrolsTemp);
                            _this.UHabilitaControles(false, ArrayControls)
                            _this.UHabilitaBotones(btnNuevoET, btnGrabarET, btnEliminarET, btnCancelarET, true, false, false, false, MDSERVICEEXTRATABLE.GRIDENABLE, MDSERVICEEXTRATABLE.READONLY, false);
                        }
                        else {
                            _this.UHabilitaBotones(btnNuevoET, btnGrabarET, btnEliminarET, btnCancelarET, true, true, true, false, MDSERVICEEXTRATABLE.GRIDENABLE, MDSERVICEEXTRATABLE.READONLY, false);
                        }
                    }
                }
                _this.ROWEXTRATABLE_GET_update(objContenedor, inIDSDWHOTOCASE, MDSERVICEEXTRATABLE, MDSERVICEEXTRATABLE.MDSERVICEEXTRAFIELDSList);
                var NroADN = _this.ContarADN(MDSERVICEEXTRATABLE.MDSERVICEEXTRAFIELDSList);

                if (!(NroADN > 0)) {
                    btnNuevoET.Visible = false;
                    btnCancelarET.Visible = false;
                }
            },
            error: function (response) {
                SysCfg.Log.Methods.WriteLog("DynamicSTEFNewCase.js ItHelpCenter.SD.CaseUser.NewCase.TDynamicSTEFNewCase.prototype.UGetExtraFields ST/STEditor/DynamicSTEFAtentionCase.svc/GrabarExtraTable" + response.statusText);
            }
        });
    }

    //********************************************************************************************************************************************************************************************
    var btnEliminarET = new TVclInputbutton(ContBotonesET_udiv[0].Child[0].This, "");
    btnEliminarET.Text = UsrCfg.Traslate.GetLangText(_this.Mythis,"btnEliminarET");
    btnEliminarET.VCLType = TVCLType.BS;

    btnEliminarET.This.classList.add("btnEliminarET");
    //btnEliminarET.classList.add("btnstep");
    btnEliminarET.This.setAttribute("data-IdET", MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE);
    btnEliminarET.This.setAttribute("data-IdWHOTT", inIDSDWHOTOCASE);
    btnEliminarET.onClick = function () {
        var IdET = parseInt($(this).attr('data-IdET'));

        var ArrayComtrolsTemp = ArrayControls;
        ArrayComtrolsTemp.push(FieldIDSDCASEMT_input);
        ArrayComtrolsTemp.push(FieldExtraTable_input);

        var parametros = [];
        for (var i = 0; i < ArrayComtrolsTemp.length; i++) {
            var Atributos = [];
            Atributos[0] = $(ArrayComtrolsTemp[i]).attr('data-NameField');
            Atributos[1] = $(ArrayComtrolsTemp[i]).val();
            if (SysCfg.DB.Properties.TDataType.GetEnum(parseInt($(ArrayComtrolsTemp[i]).attr('data-DataType'))) == SysCfg.DB.Properties.TDataType.DateTime) {
                var _date_ef = $(ArrayComtrolsTemp[i]).val() == "" ? new Date() : new Date($(ArrayComtrolsTemp[i]).val());
                Atributos[1] = SysCfg.Str.Protocol.DateTimeToStrProtocol(_date_ef);
            }
            Atributos[2] = $(ArrayComtrolsTemp[i]).attr('data-DataType');
            parametros[i] = Atributos;
        }

        _data = JSON.stringify({
            IDSDWHOTOCASE: inIDSDWHOTOCASE, // parseInt($('#IDSDWHOTOCASE').val()),
            IDMDSERVICEEXTRATABLE: MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE,
            arrayString: parametros,
            stateForm: UsrCfg.SD.Properties.StateFormulario._Delete
        });
        //_data = JSON.stringify(NuevoObjeto);
        $.ajax({
            type: "POST",
            url: SysCfg.App.Properties.xRaiz + "ST/STEditor/DynamicSTEFAtentionCase.svc/GrabarExtraTable",
            contentType: "application/json; charset=utf-8",
            data: _data,
            success: function (response) {
                _this.ROWEXTRATABLE_GET_update(objContenedor, inIDSDWHOTOCASE, MDSERVICEEXTRATABLE, MDSERVICEEXTRATABLE.MDSERVICEEXTRAFIELDSList);
                var NroADN = _this.ContarADN(MDSERVICEEXTRATABLE.MDSERVICEEXTRAFIELDSList);

                if (!(NroADN > 0)) {
                    btnNuevoET.Visible = false;
                    btnCancelarET.Visible = false;
                }
                _this.ULimpiarControles(ArrayComtrolsTemp);
                alert("Se eliminio correctamente");
            },
            error: function (response) {
                SysCfg.Log.Methods.WriteLog("DynamicSTEFNewCase.js ItHelpCenter.SD.CaseUser.NewCase.TDynamicSTEFNewCase.prototype.CMDBCIDEFINEEXTRATABLE_SOURCETYPE ST/STEditor/DynamicSTEFAtentionCase.svc/GrabarExtraTable" + response);
            }
        });
    }
    //*****************************
    var btnCancelarET = new TVclInputbutton(ContBotonesET_udiv[0].Child[0].This, "");
    btnEliminarET.Text = UsrCfg.Traslate.GetLangText(_this.Mythis,"btnCancelarET");
    btnEliminarET.VCLType = TVCLType.BS;

    btnCancelarET.This.classList.add("btnCancelarET");
    //btnCancelarET.classList.add("btnstep");
    btnCancelarET.This.setAttribute("data-IdET", MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE);
    btnCancelarET.This.setAttribute("data-IdWHOTT", inIDSDWHOTOCASE);
    btnCancelarET.onClick = function () {
        var IdET = parseInt($(this).attr('data-IdET'));
        var ArrayComtrolsTemp = ArrayControls;
        ArrayComtrolsTemp.push(FieldIDSDCASEMT_input);
        ArrayComtrolsTemp.push(FieldExtraTable_input);
        _this.ULimpiarControles(ArrayComtrolsTemp);
        _this.UHabilitaControles(false, ArrayControls)
        $('#stateForm' + MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE + IdxIDSDWHOTOCASE).val(UsrCfg.SD.Properties.StateFormulario._Cancel);
        _this.UHabilitaBotones(btnNuevoET, btnGrabarET, btnEliminarET, btnCancelarET, true, false, false, false, MDSERVICEEXTRATABLE.GRIDENABLE, MDSERVICEEXTRATABLE.READONLY, false);
    }
    //*********************************************
    this.UROWEXTRATABLE_GET(objContenedor, inIDSDWHOTOCASE, MDSERVICEEXTRATABLE, MDSERVICEEXTRATABLE.MDSERVICEEXTRAFIELDSList);

    var NroADN = this.ContarADN(MDSERVICEEXTRATABLE.MDSERVICEEXTRAFIELDSList);

    if (!(NroADN > 0)) {
        btnNuevoET.Visible = false;
        btnCancelarET.Visible = false;
        this.UHabilitaBotones(btnNuevoET, btnGrabarET, btnEliminarET, btnCancelarET, false, true, true, false, MDSERVICEEXTRATABLE.GRIDENABLE, MDSERVICEEXTRATABLE.READONLY, true);
        this.UHabilitaControles(true, ArrayControls)
    }
    else {
        HabilitaBotones(btnNuevoET, btnGrabarET, btnEliminarET, btnCancelarET, true, false, false, false, MDSERVICEEXTRATABLE.GRIDENABLE, MDSERVICEEXTRATABLE.READONLY, false);
        this.UHabilitaControles(false, ArrayControls)
    }

    return btnGrabarET;
}

ItHelpCenter.SD.CaseUser.NewCase.TDynamicSTEFNewCase.prototype.UROWEXTRATABLE_GET = function (objContenedor, inIDSDWHOTOCASE, MDSERVICEEXTRATABLE, MDSERVICEEXTRAFIELDList) {
    var _this = this.TParent();
    var data = {
        IDSDWHOTOCASE: inIDSDWHOTOCASE, //parseInt($('#IDSDWHOTOCASE').val()),
        IDMDSERVICEEXTRATABLE: MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE
    };
    var NroADN_Result = 0;
    $.ajax({
        type: "POST",
        url: SysCfg.App.Properties.xRaiz + 'ST/STEditor/DynamicSTEFAtentionCase.svc/ROWEXTRATABLE_GET',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (response) {
            var IdxIDSDWHOTOCASE = '_' + inIDSDWHOTOCASE;
            var TableET = Utable(objContenedor, "GridFields" + MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE + IdxIDSDWHOTOCASE);
            var TableHeadET = Uthead(TableET, "");
            var TableHeadRowET = Utr(TableHeadET, "");

            for (var i = 0; i < response.Columnas.length; i++) {
                var TableHeadThET = Uth(TableHeadRowET, response.Columnas[i].IDMDSERVICEEXTRAFIELDS + IdxIDSDWHOTOCASE);
                TableHeadThET.innerHTML = response.Columnas[i].NombreColumna;
                TableHeadThET.setAttribute("data-IdEF", response.Columnas[i].IDMDSERVICEEXTRAFIELDS);
                TableHeadThET.setAttribute("data-IdET", MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE);
            }

            var TableBodyET = Utbody(TableET, "");
            for (var i = 0; i < response.Filas.length; i++) {
                var TableBodyRowET = Utr(TableBodyET, "");
                TableBodyRowET.addEventListener("click", function () {
                    _this.ClickPrimerRow(TableBodyET, IdxIDSDWHOTOCASE, TableBodyRowET);
                });
                for (var j = 0; j < response.Filas[i].Celdas.length; j++) {
                    var TableBodyTdET = Utd(TableBodyRowET, "");
                    TableBodyTdET.innerHTML = response.Filas[i].Celdas[j].Valor;
                }
            }

            var NroADN = _this.ContarADN(MDSERVICEEXTRAFIELDList);

            Uinput(objContenedor, TImputtype.hidden, "NroADN" + MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE + IdxIDSDWHOTOCASE, NroADN, false);

            var input_StateForm = Uinput(objContenedor, TImputtype.hidden, "stateForm" + MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE + IdxIDSDWHOTOCASE, UsrCfg.SD.Properties.StateFormulario._Nuevo, false);

            if (!(NroADN > 0)) {
                var row = $(TableBodyET).children('tr:first');
                var stateForm = _this.ClickPrimerRow(TableBodyET, IdxIDSDWHOTOCASE, row);
                input_StateForm.value = stateForm;
                TableET.style.display = "none";
            }
            else {
                input_StateForm.value = UsrCfg.SD.Properties.StateFormulario._Cancel;
            }
        },
        error: function (response) {
            SysCfg.Log.Methods.WriteLog("DynamicSTEFNewCase.js ItHelpCenter.SD.CaseUser.NewCase.TDynamicSTEFNewCase.prototype.UROWEXTRATABLE_GET ST/STEditor/DynamicSTEFAtentionCase.svc/ROWEXTRATABLE_GET" + response.ErrorMessage);
        }
    });
}

ItHelpCenter.SD.CaseUser.NewCase.TDynamicSTEFNewCase.prototype.ROWEXTRATABLE_GET_update = function (objContenedor, inIDSDWHOTOCASE, MDSERVICEEXTRATABLE, MDSERVICEEXTRAFIELDSList) {
    var _this = this.TParent();
    var data = {
        IDSDWHOTOCASE: inIDSDWHOTOCASE,
        'IDMDSERVICEEXTRATABLE': MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE
    };

    $.ajax({
        type: "POST",
        url: SysCfg.App.Properties.xRaiz + 'ST/STEditor/DynamicSTEFAtentionCase.svc/ROWEXTRATABLE_GET',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var IdxIDSDWHOTOCASE = '_' + inIDSDWHOTOCASE;
            $("#GridFields" + MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE + IdxIDSDWHOTOCASE).remove();

            var TableET = Utable(objContenedor, "GridFields" + MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE + IdxIDSDWHOTOCASE);
            var TableHeadET = Uthead(TableET, "");
            var TableHeadRowET = Utr(TableHeadET, "");

            for (var i = 0; i < response.Columnas.length; i++) {
                var TableHeadThET = Uth(TableHeadRowET, response.Columnas[i].IDMDSERVICEEXTRAFIELDS + IdxIDSDWHOTOCASE);
                TableHeadThET.innerHTML = response.Columnas[i].NombreColumna;
                TableHeadThET.setAttribute("data-IdEF", response.Columnas[i].IDMDSERVICEEXTRAFIELDS);
                TableHeadThET.setAttribute("data-IdET", MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE);
            }

            var TableBodyET = Utbody(TableET, "");
            for (var i = 0; i < response.Filas.length; i++) {
                var TableBodyRowET = Utr(TableBodyET, "");
                TableBodyRowET.addEventListener("click", function () {
                    _this.ClickPrimerRow(TableBodyET, IdxIDSDWHOTOCASE, TableBodyRowET);
                });
                for (var j = 0; j < response.Filas[i].Celdas.length; j++) {
                    var TableBodyTdET = Utd(TableBodyRowET, "");
                    TableBodyTdET.innerHTML = response.Filas[i].Celdas[j].Valor;
                }
            }

            var NroADN = _this.ContarADN(MDSERVICEEXTRAFIELDSList);
            $("#NroADN" + MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE + IdxIDSDWHOTOCASE).remove();
            $("#stateForm" + MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE + IdxIDSDWHOTOCASE).remove();

            Uinput(objContenedor, TImputtype.hidden, "NroADN" + MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE + IdxIDSDWHOTOCASE, NroADN, false);

            var input_StateForm = Uinput(objContenedor, TImputtype.hidden, "stateForm" + MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE + IdxIDSDWHOTOCASE, UsrCfg.SD.Properties.StateFormulario._Nuevo, false);

            if (!(NroADN > 0)) {
                var row = $(TableBodyET).children('tr:first');
                var stateForm = _this.ClickPrimerRow(TableBodyET, IdxIDSDWHOTOCASE, row);
                input_StateForm.value = stateForm;
                TableET.style.display = "none";
            }
        },
        error: function (response) {
            SysCfg.Log.Methods.WriteLog("DynamicSTEFNewCase.js ItHelpCenter.SD.CaseUser.NewCase.TDynamicSTEFNewCase.prototype.ROWEXTRATABLE_GET_update ST/STEditor/DynamicSTEFAtentionCase.svc/ROWEXTRATABLE_GET " + response.ErrorMessage);
        }
    });
}

ItHelpCenter.SD.CaseUser.NewCase.TDynamicSTEFNewCase.prototype.UConstruirLookUp = function (MDSERVICEEXTRAFIELDS, IDMDSERVICEEXTRATABLE, Objeto_Cont, inIDSDWHOTOCASE) {
    var _this = this.TParent();
    var IdxIDSDWHOTOCASE = '_' + inIDSDWHOTOCASE;
    var combo;

    var data = {
        IDSDWHOTOCASE: inIDSDWHOTOCASE,
        'IDMDSERVICEEXTRATABLE': IDMDSERVICEEXTRATABLE,
        'IDMDSERVICEEXTRAFIELDS': MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS
    };

    $.ajax({
        type: "POST",
        url: SysCfg.App.Properties.xRaiz + 'ST/STEditor/DynamicSTEFAtentionCase.svc/GetComboExtraField',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (response) {
            combo = Uselect(Objeto_Cont, "Field" + MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS + IdxIDSDWHOTOCASE);
            combo.setAttribute("data-NameField", MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME);
            combo.setAttribute("data-DataType", MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES);
            combo.setAttribute("data-Columnstyle", UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.LookUp);
            combo.setAttribute("data-Mandatory", MDSERVICEEXTRAFIELDS.MANDATORY);
            combo.setAttribute("data-Description", MDSERVICEEXTRAFIELDS.EXTRAFIELDS_DESCRIPTION);
            combo.style.display = _this.UGetVisibility(MDSERVICEEXTRAFIELDS.IDMDLIFESTATUSPERMISSION);
            combo.style.marginBottom = "4px";
            $(combo).attr("disabled", _this.UGetReadOnly(MDSERVICEEXTRAFIELDS.IDMDLIFESTATUSPERMISSION));
            for (var i = 0; i < response.length; i++) {
                Uoption(combo, "", response[i].ValueMember, response[i].DisplayMember);
            }
        },
        error: function (response) {
            SysCfg.Log.Methods.WriteLog("DynamicSTEFNewCase.js ItHelpCenter.SD.CaseUser.NewCase.TDynamicSTEFNewCase.prototype.UConstruirLookUp ST/STEditor/DynamicSTEFAtentionCase.svc/GetComboExtraField" + response.ErrorMessage);
        }
    });
    return combo;
}

ItHelpCenter.SD.CaseUser.NewCase.TDynamicSTEFNewCase.prototype.UConstruirLookUpOption = function (MDSERVICEEXTRAFIELDS, IDMDSERVICEEXTRATABLE, Objeto_Cont, inIDSDWHOTOCASE) {
    var _this = this.TParent();
    var IdxIDSDWHOTOCASE = '_' + inIDSDWHOTOCASE;
    var radiobuton;
    var data = {
        IDSDWHOTOCASE: inIDSDWHOTOCASE,
        'IDMDSERVICEEXTRATABLE': IDMDSERVICEEXTRATABLE,
        'IDMDSERVICEEXTRAFIELDS': MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS
    };

    var textito;
    $.ajax({
        type: "POST",
        url: SysCfg.App.Properties.xRaiz + 'ST/STEditor/DynamicSTEFAtentionCase.svc/GetComboExtraField',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (response) {
            //"<input name='" + NombreContenedor + "Field" + NombreContenedor + CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS + "' id='" + NombreContenedor + "Field" + NombreContenedor + CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS + "' data-NameField='" + CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_NAME + "' data-DataType='" + CMDBCIDEFINEEXTRAFIELDS.IDCMDBDBDATATYPES + "'>";
            radiobuton = new Array(response.length);
            var _div = Udiv(Objeto_Cont, '')
            _div.style.display = _this.UGetVisibility(MDSERVICEEXTRAFIELDS.IDMDLIFESTATUSPERMISSION);
            _div.style.textAlign = 'left';
            for (var i = 0; i < response.length; i++) {

                var ctr_field = Uinput(_div, TImputtype.radio, "Field" + MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS + IdxIDSDWHOTOCASE + i, response[i].ValueMember, false);
                $(ctr_field).attr("data-NameField", MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME);
                $(ctr_field).attr("data-DataType", MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES);
                $(ctr_field).attr("data-Columnstyle", UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.LookUpOption);
                $(ctr_field).attr("data-Mandatory", MDSERVICEEXTRAFIELDS.MANDATORY);
                $(ctr_field).attr("data-Description", MDSERVICEEXTRAFIELDS.EXTRAFIELDS_DESCRIPTION);
                $(ctr_field).attr("name", "Field" + MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS + IdxIDSDWHOTOCASE);
                $(ctr_field).attr("disabled", _this.UGetReadOnly(MDSERVICEEXTRAFIELDS.IDMDLIFESTATUSPERMISSION));
                ctr_field.style.marginBottom = "4px";
                ctr_field.style.marginRight = "2px";

                var lbl = Ulabel(_div, "", response[i].DisplayMember)
                lbl.style.marginRight = "9px";
                $(lbl).attr("for", ctr_field.id);

                radiobuton[i] = ctr_field;
                //var combo;

                //combo += "<input type='radio' name='" + NombreContenedor + "Field" + NombreContenedor + CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS + "' id='" + NombreContenedor + "Field" + NombreContenedor + CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS + "' data-Columnstyle='" + UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.LookUpOption + "' data-NameField='" + CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_NAME + "' data-DataType='" + CMDBCIDEFINEEXTRAFIELDS.IDCMDBDBDATATYPES + "' value='" + response[i].ValueMember + "'>"
                //combo += ;
            }
            //combo += "</select>";
            //textito = combo;
        },
        error: function (response) {
            SysCfg.Log.Methods.WriteLog("DynamicSTEFNewCase.js ItHelpCenter.SD.CaseUser.NewCase.TDynamicSTEFNewCase.prototype.UConstruirLookUpOption ST/STEditor/DynamicSTEFAtentionCase.svc/GetComboExtraField" + response.ErrorMessage);
        }
    });

    return radiobuton;
}

//**********************************************************


ItHelpCenter.SD.CaseUser.NewCase.TDynamicSTEFNewCase.prototype.ULimpiarControles = function (Controles) {
    var _this = this.TParent();
    for (var i = 0; i < Controles.length; i++) {
        $(Controles[i]).val("");
    }
}

ItHelpCenter.SD.CaseUser.NewCase.TDynamicSTEFNewCase.prototype.UHabilitaControles = function (Estado, ArryaCtrl) {
    var _this = this.TParent();
    for (var i = 0; i < ArryaCtrl.length; i++) {
        $(ArryaCtrl[i]).prop("disabled", !Estado);
    }
}

ItHelpCenter.SD.CaseUser.NewCase.TDynamicSTEFNewCase.prototype.UHabilitaBotones = function (btnNuevoET, btnGrabarET, btnEliminarET, btnCancelarET, _New, _Save, _Delete, _Cancel, GRIDENABLE, READONLY, is_CSATSurvery) {
    var _this = this.TParent();
    //is_CSATSurvery = false;
    var _NewVisible = Boolean(_New && GRIDENABLE);
    if (_NewVisible) _NewVisible = (!is_CSATSurvery);
    var _SaveVisible = Boolean(_Save && (!is_CSATSurvery));
    var _DeleteVisible = Boolean(_Delete && (!is_CSATSurvery));
    var _CancelVisible = Boolean(_Cancel && (!is_CSATSurvery));


    btnNuevoET.Enabled = _New;
    btnGrabarET.Enabled = _Save;
    btnEliminarET.Enabled = _Delete;
    btnCancelarET.Enabled = _Cancel;

    if (_NewVisible) _NewVisible = READONLY;
    if (_SaveVisible) _SaveVisible = READONLY;
    if (_DeleteVisible) _DeleteVisible = READONLY;
    if (_CancelVisible) _CancelVisible = READONLY;

    if (_NewVisible) btnNuevoET.Visible = true;
    else btnNuevoET.Visible = false;

    if (_SaveVisible) btnGrabarET.Visible = true;
    else btnGrabarET.Visible = false;

    if (_DeleteVisible) btnEliminarET.Visible = true;
    else btnEliminarET.Visible = false;

    if (_CancelVisible) btnCancelarET.Visible = true;
    else btnCancelarET.Visible = false;
}

ItHelpCenter.SD.CaseUser.NewCase.TDynamicSTEFNewCase.prototype.ContarADN = function (MDSERVICEEXTRAFIELDList) {
    var _this = this.TParent();
    var count = 0;
    for (var i = 0; i < MDSERVICEEXTRAFIELDList.length; i++) {
        if (MDSERVICEEXTRAFIELDList[i].IDCMDBKEYTYPE == 1) {
            count += 1;
        }
    }
    return count;
}

ItHelpCenter.SD.CaseUser.NewCase.TDynamicSTEFNewCase.prototype.ClickPrimerRow = function (tabla, IdxIDSDWHOTOCASE, row) {
    var _this = this.TParent();
    if (row != undefined) {
        var columnas = $("#" + $($(row).parent().parent()).attr("id") + " thead").children(':eq(' + $(row).index() + ')').children();

        if (columnas.length > 0) {
            var idET = $(columnas[0]).attr("data-IdET");
            var IdcontrolET = "#FieldExtraTable" + idET + IdxIDSDWHOTOCASE;
            var valorCeldaET = $(row).children()[1].innerHTML;
            $(IdcontrolET).val(valorCeldaET);

            var IdcontrolCI = "#FieldIDSDCASEMT" + idET + IdxIDSDWHOTOCASE;
            var valorCeldaCI = $(row).children()[0].innerHTML;
            $(IdcontrolCI).val(valorCeldaCI);
        }

        for (var i = 2; i < columnas.length; i++) {
            //var nameCol = columnas[i].innerHTML;
            var IdControl = "#Field" + columnas[i].id;
            var valorCelda = $(row).children()[i].innerHTML;
            if ($(IdControl).prop("nodeName") == "TEXTAREA") {
                $(IdControl).html("");
                $(IdControl).append(valorCelda);
            }
            else {
                $(IdControl).val(valorCelda);
            }
            $(IdControl).prop("checked", valorCelda);
        }
        if (columnas.length > 0)
            return UsrCfg.SD.Properties.StateFormulario._update;
        else
            return UsrCfg.SD.Properties.StateFormulario._Nuevo;
    }
    else
        return UsrCfg.SD.Properties.StateFormulario._Nuevo;
}

ItHelpCenter.SD.CaseUser.NewCase.TDynamicSTEFNewCase.prototype.UGetVisibility = function (IDMDLIFESTATUSPERMISSION) {
    var _this = this.TParent();
    switch (IDMDLIFESTATUSPERMISSION) {
        case UsrCfg.SD.Properties.TMDLIFESTATUSPERMISSION._Disable:
            return ("none");
        case UsrCfg.SD.Properties.TMDLIFESTATUSPERMISSION._Read:
            return ("block");
        case UsrCfg.SD.Properties.TMDLIFESTATUSPERMISSION._Write:
            return ("block");
    }
    return ("block");
}

ItHelpCenter.SD.CaseUser.NewCase.TDynamicSTEFNewCase.prototype.UGetReadOnly = function (IDMDLIFESTATUSPERMISSION) {
    var _this = this.TParent();
    switch (IDMDLIFESTATUSPERMISSION) {
        case UsrCfg.SD.Properties.TMDLIFESTATUSPERMISSION._Disable:
            return true
        case UsrCfg.SD.Properties.TMDLIFESTATUSPERMISSION._Read:
            return true;
        case UsrCfg.SD.Properties.TMDLIFESTATUSPERMISSION._Write:
            return false;
    }
    return false;
}

ItHelpCenter.SD.CaseUser.NewCase.TDynamicSTEFNewCase.prototype.UConstruirControl = function (Objeto_Cont, inIDSDWHOTOCASE, MDSERVICEEXTRAFIELDS, IDMDSERVICEEXTRATABLE) {
    var _this = this.TParent();
    var IdxIDSDWHOTOCASE = '_' + inIDSDWHOTOCASE;
    var TipoDatoEnum = SysCfg.DB.Properties.TDataType.GetEnum(MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES);
    switch (TipoDatoEnum) {
        case SysCfg.DB.Properties.TDataType.String: // SysCfg.DB.Properties.TDataType.String   
            switch (MDSERVICEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                    var ctr_field = Uinput(Objeto_Cont, TImputtype.text, "Field" + MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS + IdxIDSDWHOTOCASE, "", false);
                    $(ctr_field).attr("data-NameField", MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME);
                    $(ctr_field).attr("data-DataType", MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES);
                    $(ctr_field).attr("data-Mandatory", MDSERVICEEXTRAFIELDS.MANDATORY);
                    $(ctr_field).attr("data-Description", MDSERVICEEXTRAFIELDS.EXTRAFIELDS_DESCRIPTION);
                    $(ctr_field).attr("data-Columnstyle", UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None);
                    ctr_field.style.marginBottom = "4px";
                    ctr_field.style.width = "330px";
                    ctr_field.style.display = this.UGetVisibility(MDSERVICEEXTRAFIELDS.IDMDLIFESTATUSPERMISSION);
                    $(ctr_field).attr("disabled", this.UGetReadOnly(MDSERVICEEXTRAFIELDS.IDMDLIFESTATUSPERMISSION));
                    return ctr_field;
                    break;
                case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.TextPassword:
                    var ctr_field = Uinput(Objeto_Cont, TImputtype.password, "Field" + MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS + IdxIDSDWHOTOCASE, "", false);
                    $(ctr_field).attr("data-NameField", MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME);
                    $(ctr_field).attr("data-DataType", MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES);
                    $(ctr_field).attr("data-Mandatory", MDSERVICEEXTRAFIELDS.MANDATORY);
                    $(ctr_field).attr("data-Description", MDSERVICEEXTRAFIELDS.EXTRAFIELDS_DESCRIPTION);
                    $(ctr_field).attr("data-Columnstyle", UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.TextPassword);
                    ctr_field.style.marginBottom = "4px";
                    ctr_field.style.width = "330px";
                    ctr_field.style.display = this.UGetVisibility(MDSERVICEEXTRAFIELDS.IDMDLIFESTATUSPERMISSION);
                    $(ctr_field).attr("disabled", this.UGetReadOnly(MDSERVICEEXTRAFIELDS.IDMDLIFESTATUSPERMISSION));
                    return ctr_field;
                    break;
                case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.LookUp:
                    return this.UConstruirLookUp(MDSERVICEEXTRAFIELDS, IDMDSERVICEEXTRATABLE, Objeto_Cont, inIDSDWHOTOCASE);
                    break;
                case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.LookUpOption:
                    return this.UConstruirLookUpOption(MDSERVICEEXTRAFIELDS, IDMDSERVICEEXTRATABLE, Objeto_Cont, inIDSDWHOTOCASE);
                    break;
            }
            break;
        case SysCfg.DB.Properties.TDataType.Text: //SysCfg.DB.Properties.TDataType.Text  
            switch (MDSERVICEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                    var ctr_field = VclMemo(Objeto_Cont, "Field" + MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS + IdxIDSDWHOTOCASE, TVCLType.BS, "", 10, 50);
                    $(ctr_field).attr("data-NameField", MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME);
                    $(ctr_field).attr("data-DataType", MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES);
                    $(ctr_field).attr("data-Mandatory", MDSERVICEEXTRAFIELDS.MANDATORY);
                    $(ctr_field).attr("data-Description", MDSERVICEEXTRAFIELDS.EXTRAFIELDS_DESCRIPTION);
                    $(ctr_field).attr("data-Columnstyle", UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None);
                    ctr_field.style.marginBottom = "8px";
                    ctr_field.style.display = this.UGetVisibility(MDSERVICEEXTRAFIELDS.IDMDLIFESTATUSPERMISSION);
                    $(ctr_field).attr("disabled", this.UGetReadOnly(MDSERVICEEXTRAFIELDS.IDMDLIFESTATUSPERMISSION));
                    return ctr_field;
                    break;
            }
            //return "<textarea class='Field' id='Field" + MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS + IdxIDSDWHOTOCASE + "' data-NameField='" + MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME + "' data-DataType='" + MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES + "' " + GetVisibility(MDSERVICEEXTRAFIELDS.IDMDLIFESTATUSPERMISSION) + " " + GetReadOnly(MDSERVICEEXTRAFIELDS.IDMDLIFESTATUSPERMISSION) + " rows='10' cols='50'> </textarea>";
            break;
        case SysCfg.DB.Properties.TDataType.Int32: // SysCfg.DB.Properties.TDataType.Int32:   
            switch (MDSERVICEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                    var ctr_field = Uinput(Objeto_Cont, TImputtype.number, "Field" + MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS + IdxIDSDWHOTOCASE, "", false);
                    $(ctr_field).attr("step", 1);
                    $(ctr_field).attr("data-NameField", MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME);
                    $(ctr_field).attr("data-DataType", MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES);
                    $(ctr_field).attr("data-Mandatory", MDSERVICEEXTRAFIELDS.MANDATORY);
                    $(ctr_field).attr("data-Description", MDSERVICEEXTRAFIELDS.EXTRAFIELDS_DESCRIPTION);
                    $(ctr_field).attr("data-Columnstyle", UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None);
                    ctr_field.style.marginBottom = "4px";
                    ctr_field.style.display = this.UGetVisibility(MDSERVICEEXTRAFIELDS.IDMDLIFESTATUSPERMISSION);
                    $(ctr_field).attr("disabled", this.UGetReadOnly(MDSERVICEEXTRAFIELDS.IDMDLIFESTATUSPERMISSION));
                    return ctr_field;
                    break;
                case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.LookUp:
                    return this.UConstruirLookUp(MDSERVICEEXTRAFIELDS, IDMDSERVICEEXTRATABLE, Objeto_Cont, inIDSDWHOTOCASE);
                    break;
                case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.LookUpOption:
                    return this.UConstruirLookUpOption(MDSERVICEEXTRAFIELDS, IDMDSERVICEEXTRATABLE, Objeto_Cont, inIDSDWHOTOCASE);
                    break;
            }

            //return "<input id='Field class='Field'" + MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS + IdxIDSDWHOTOCASE + "' data-NameField='" + MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME + "' data-DataType='" + MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES + "' type='number' step='1' " + GetVisibility(MDSERVICEEXTRAFIELDS.IDMDLIFESTATUSPERMISSION) + " " + GetReadOnly(MDSERVICEEXTRAFIELDS.IDMDLIFESTATUSPERMISSION) + "/>";
            break;
        case SysCfg.DB.Properties.TDataType.Double: //SysCfg.DB.Properties.TDataType.Double:   
            switch (MDSERVICEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                    var ctr_field = Uinput(Objeto_Cont, TImputtype.number, "Field" + MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS + IdxIDSDWHOTOCASE, "", false);
                    $(ctr_field).attr("step", 1);
                    $(ctr_field).attr("data-NameField", MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME);
                    $(ctr_field).attr("data-DataType", MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES);
                    $(ctr_field).attr("data-Mandatory", MDSERVICEEXTRAFIELDS.MANDATORY);
                    $(ctr_field).attr("data-Description", MDSERVICEEXTRAFIELDS.EXTRAFIELDS_DESCRIPTION);
                    $(ctr_field).attr("data-Columnstyle", UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None);
                    ctr_field.style.marginBottom = "4px";
                    ctr_field.style.display = this.UGetVisibility(MDSERVICEEXTRAFIELDS.IDMDLIFESTATUSPERMISSION);
                    $(ctr_field).attr("disabled", this.UGetReadOnly(MDSERVICEEXTRAFIELDS.IDMDLIFESTATUSPERMISSION));
                    return ctr_field;
                    break;
            }
            //return "<input id='Field class='Field'" + MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS + IdxIDSDWHOTOCASE + "' data-NameField='" + MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME + "' data-DataType='" + MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES + "' type='number' step='1' " + GetVisibility(MDSERVICEEXTRAFIELDS.IDMDLIFESTATUSPERMISSION) + " " + GetReadOnly(MDSERVICEEXTRAFIELDS.IDMDLIFESTATUSPERMISSION) + " />";
            break;
        case SysCfg.DB.Properties.TDataType.Boolean: //SysCfg.DB.Properties.TDataType.Boolean:     
            switch (MDSERVICEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                    var ctr_field = VclInputcheckbox(Objeto_Cont, "Field" + MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS + IdxIDSDWHOTOCASE, TVCLType.BS, "", false);
                    $(ctr_field.This).attr("data-NameField", MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME);
                    $(ctr_field.This).attr("data-DataType", MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES);
                    $(ctr_field.This).attr("data-Mandatory", MDSERVICEEXTRAFIELDS.MANDATORY);
                    $(ctr_field.This).attr("data-Description", MDSERVICEEXTRAFIELDS.EXTRAFIELDS_DESCRIPTION);
                    $(ctr_field.This).attr("data-Columnstyle", UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None);
                    ctr_field.This.style.marginBottom = "4px";
                    ctr_field.This.style.display = this.UGetVisibility(MDSERVICEEXTRAFIELDS.IDMDLIFESTATUSPERMISSION);
                    $(ctr_field.This).attr("disabled", this.UGetReadOnly(MDSERVICEEXTRAFIELDS.IDMDLIFESTATUSPERMISSION));
                    return ctr_field.This;
                    break;
            }
            //return "<input id='Field class='Field'" + MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS + IdxIDSDWHOTOCASE + "' data-NameField='" + MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME + "' data-DataType='" + MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES + "' type='checkbox' " + GetVisibility(MDSERVICEEXTRAFIELDS.IDMDLIFESTATUSPERMISSION) + " " + GetReadOnly(MDSERVICEEXTRAFIELDS.IDMDLIFESTATUSPERMISSION) + "/>";
            break;
        case SysCfg.DB.Properties.TDataType.DateTime: //SysCfg.DB.Properties.TDataType.DateTime:   

            switch (MDSERVICEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                    var ctr_field = Uinput(Objeto_Cont, TImputtype.text, "Field" + MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS + IdxIDSDWHOTOCASE, "", false);
                    $(ctr_field).attr("data-NameField", MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME);
                    $(ctr_field).attr("data-DataType", MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES);
                    $(ctr_field).attr("data-Columnstyle", UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None);
                    $(ctr_field).attr("data-Mandatory", MDSERVICEEXTRAFIELDS.MANDATORY);
                    $(ctr_field).attr("data-Description", MDSERVICEEXTRAFIELDS.EXTRAFIELDS_DESCRIPTION);
                    ctr_field.style.marginBottom = "4px";
                    ctr_field.style.display = this.UGetVisibility(MDSERVICEEXTRAFIELDS.IDMDLIFESTATUSPERMISSION);
                    $(ctr_field).attr("disabled", this.UGetReadOnly(MDSERVICEEXTRAFIELDS.IDMDLIFESTATUSPERMISSION));
                    $(ctr_field).datetimepicker({
                        timeFormat: "hh:mm tt"
                    });
                    return ctr_field;
                    break;
                case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.Date:
                    var ctr_field = Uinput(Objeto_Cont, TImputtype.text, "Field" + MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS + IdxIDSDWHOTOCASE, "", false);
                    $(ctr_field).attr("data-NameField", MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME);
                    $(ctr_field).attr("data-DataType", MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES);
                    $(ctr_field).attr("data-Columnstyle", UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None);
                    $(ctr_field).attr("data-Mandatory", MDSERVICEEXTRAFIELDS.MANDATORY);
                    $(ctr_field).attr("data-Description", MDSERVICEEXTRAFIELDS.EXTRAFIELDS_DESCRIPTION);
                    ctr_field.style.marginBottom = "4px";
                    ctr_field.style.display = this.UGetVisibility(MDSERVICEEXTRAFIELDS.IDMDLIFESTATUSPERMISSION);
                    $(ctr_field).attr("disabled", this.UGetReadOnly(MDSERVICEEXTRAFIELDS.IDMDLIFESTATUSPERMISSION));
                    $(ctr_field).datepicker({ showButtonPanel: true });
                    return ctr_field;
                    break;
                case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.Time:
                    var ctr_field = Uinput(Objeto_Cont, TImputtype.text, "Field" + MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS + IdxIDSDWHOTOCASE, "", false);
                    $(ctr_field).attr("data-NameField", MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME);
                    $(ctr_field).attr("data-DataType", MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES);
                    $(ctr_field).attr("data-Columnstyle", UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None);
                    $(ctr_field).attr("data-Mandatory", MDSERVICEEXTRAFIELDS.MANDATORY);
                    $(ctr_field).attr("data-Description", MDSERVICEEXTRAFIELDS.EXTRAFIELDS_DESCRIPTION);
                    ctr_field.style.marginBottom = "4px";
                    ctr_field.style.display = this.UGetVisibility(MDSERVICEEXTRAFIELDS.IDMDLIFESTATUSPERMISSION);
                    $(ctr_field).attr("disabled", this.UGetReadOnly(MDSERVICEEXTRAFIELDS.IDMDLIFESTATUSPERMISSION));
                    $(ctr_field).timepicker({
                        timeFormat: "hh:mm tt"
                    });
                    return ctr_field;
                    break;
            }
            break
            //return "<input id='Field class='Field'" + MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS + IdxIDSDWHOTOCASE + "' data-NameField='" + MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME + "' data-DataType='" + MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES + "' type='text' " + GetVisibility(MDSERVICEEXTRAFIELDS.IDMDLIFESTATUSPERMISSION) + " " + GetReadOnly(MDSERVICEEXTRAFIELDS.IDMDLIFESTATUSPERMISSION) + "/>";
        default:
            break;
    }
    //}
}

