﻿ItHelpCenter.SD.CaseUser.Atention.TResultform = {
    GotoMain: { value: 0, name: "GotoMain" },
    Refresh: { value: 1, name: "Refresh" },
};


ItHelpCenter.SD.CaseUser.Atention.OutSMConsoleUserSwitch = function (ElementDiv, Resultform, idcase, CallbackModalResult) {
    if (Resultform == ItHelpCenter.SD.CaseUser.Atention.TResultform.Refresh) {
        ElementDiv.innerHTML = "";
        var SMConsoleUserSwitch = new ItHelpCenter.SD.CaseUser.Atention.TSMConsoleUserSwitch(
            ElementDiv,
            //CallbackModalResult,
            function (_this) {
                CallbackModalResult();
            },
            idcase
            );
    } else if (Resultform == ItHelpCenter.SD.CaseUser.Atention.TResultform.GotoMain) {
        CallbackModalResult();
        //return Resultform;        
        //Continua = false;
    }
    else {
        return Resultform;

    }
}

//****************** Principales *******************************
ItHelpCenter.SD.CaseUser.Atention.TSMConsoleUserSwitch = function (inObject, incallbackModalResult, inIDSOURCE) {

    this.TParent = function () {
        return this;
    }.bind(this);

    var _this = this.TParent();

    this.Object = inObject;
    this.CallbackModalResult = incallbackModalResult;//CallbackModalResult(OutStr);        
    this.IDSOURCE = inIDSOURCE;

    this.TfrAttached = new Array();
    this.Mythis = "TDynamicSTEFAtentionCase_User";

    UsrCfg.Traslate.GetLangText(_this.Mythis, "Uh2_Header", "Before entering the system, there are some outstanding, please resolve them");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "fecha_uh4", "Report Date: ");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lueMessage", "Message: ");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Uh4_Users", "Users:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Uh4_Message", "Send message to the manager to manage your case:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "deta1_uh4", "Detail:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "deta2_uh4", "Messages:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "deta3_uh4", "Attention:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "deta2cont_uh4", "No messages found");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "esta1_uh4", "Actual Status: ");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Label_LSEND_COMMENTSENABLE", "Write a brief summary:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Label_LSBEGIN_COMMENTSENABLE", "Type a suggestion:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnNuevoET", "New");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnGrabarET", "Save");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnEliminarET", "Delete");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnCancelarET", "Cancel");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "ButtonOkCaption", "Send");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "ButtonCancelCaption", "Cancel");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lb_Attached", "Attached");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lueAttached.ButtonCancelCaption.Text", "Cancel");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lueAttached.ButtonOkCaption.Text", "Accept");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1", "Recommendation for");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2", "Attention for");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3", "Step Summary of");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines4", "Priority:");



    var PathCSS = ItHelpCenter.ScriptManager.GetStyle("SD/CaseUser/Atention/DynamicSTEFAtentionCase.css");
    //Creamos un Id para el Css
    var IDparthCss = "TDynamicSTEFAtentionCase_User";
    //Cargamos el css pasando el Path que recibimos y el Id creado
    ItHelpCenter.ScriptManager.AddStyleFile(PathCSS, IDparthCss, function () {
        //Continuamos con las funciones 
        $.ajax({
            type: "POST", url: SysCfg.App.Properties.xRaiz + 'Service/SD/Console.svc/SDCASE_GET', data: '{}', contentType: "application/json; charset=utf-8", dataType: "json",
            success: function (response) {

                _this.loadPendingcases(response, _this.IDSOURCE);
                //_this.CallbackModalResult(_this);
            },
            error: function (response) {
                SysCfg.Log.Methods.WriteLog("DynamicSTEFAtentionCase.js ItHelpCenter.SD.CaseUser.Atention.TSMConsoleUserSwitch" + "Error no llamo al servicio");
            }
        });

    }, function (e) {
    });
}

ItHelpCenter.SD.CaseUser.Atention.TSMConsoleUserSwitch.prototype.loadPendingcases = function (responselist, idcase) {
    var _this = this.TParent();
    /*-1=pendig,0=Case list>0= el numero del caso segin el id */
    //var Body = _this.Object;//new TVclBody($("body"), "idTag");
    //************  Tagheader ************************************
    var TagHeadContDef = new Array(1);
    var TagHeadCont = VclDivitions(_this.Object, "TagHeadCont", TagHeadContDef);
    BootStrapContainer(TagHeadCont[0].This, TBootStrapContainerType.none);
    var TagHeadContRowDef = new Array(1);
    var TagHeadContRow = VclDivitions(TagHeadCont[0].This, "TagHeadContRow", TagHeadContDef);
    BootStrapCreateRow(TagHeadContRow[0].This);
    var TagHeadContColDef = new Array(2);
    var TagHeadContCol = VclDivitions(TagHeadContRow[0].This, "TagHeadContCol", TagHeadContColDef);
    BootStrapCreateColumns(TagHeadContCol, [[3, 9], [3, 9], [3, 9], [3, 9], [3, 9]]);
    if (idcase == -1) {
        Uimg(TagHeadContCol[0].This, "logo", "image/High/IT-Help-Center-Final file.png", "");
        Vcllabel(TagHeadContCol[1].This, "", TVCLType.BS, TlabelType.H2, UsrCfg.Traslate.GetLangText(_this.Mythis, "Uh2_Header"));
    }
    var contCases = 0

    var indx = 0;
    for (var i = 0; i < responselist.length; i++) {
        var dum = i;

        var IDSDWHOTOCASE = _this.GetIDSDWHOTOCASEPendingcases(responselist[i].IDSDCASE, responselist[i].IDSDCASEMT);
        if (IDSDWHOTOCASE > 0) {
            var parameters =
            {
                Reload: "true",
                IDSDWHOTOCASE: IDSDWHOTOCASE
            };
            parameters = JSON.stringify(parameters);
            $.ajax({
                type: "POST",
                url: SysCfg.App.Properties.xRaiz + 'Service/SD/Atention.svc/SDConsoleAtentionStart',
                data: parameters,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (response) {





                    var HayShowLogin = false;
                    var HayHide = false;
                    for (var Counter = 0; Counter < response.MDLIFESTATUSProfiler.MDLIFESTATUSList.length; Counter++)//Steps
                    {
                        if (response.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter].STATUSN == response.SDCONSOLEATENTION.SDCASEMTCOMPLTE.CASEMT_SET_LS_STATUSN) {
                            for (var i = 0; i < response.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter].MDLIFESTATUSTYPEUSERList.length ; i++) {
                                if (response.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter].MDLIFESTATUSTYPEUSERList[i].IDSDTYPEUSER == response.SDWHOTOCASE.IDSDTYPEUSER) {
                                    //if (response.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter].MDLIFESTATUSTYPEUSERList[i].IDMDLIFESTATUSBEHAVIOR == 1) {
                                    if (response.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter].MDLIFESTATUSTYPEUSERList[i].HERESTEP_CONFIG.Hide) {
                                        HayHide = true; //es calificacion   
                                    }
                                    else {
                                        if (response.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter].MDLIFESTATUSTYPEUSERList[i].HERESTEP_CONFIG.ShowLogin) {
                                            HayShowLogin = true; //es calificacion   
                                            contCases += 1;
                                        }
                                    }

                                }
                            }
                        }
                    }
                    if (idcase == -1) {
                    }
                    else if (idcase == 0) HayShowLogin = true;
                    else if (response.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE == idcase) HayShowLogin = true;
                    else HayHide = true;
                    if (HayHide) return;
                    if (HayShowLogin == false) return;



                    var frSDCaseAtention = new ItHelpCenter.SD.CaseUser.Atention.TfrSDCaseAtention(_this.Object, _this.CallbackModalResult, _this.IDSOURCE, response);
                    //************  Tagmain ************************************
                    UsrCfg.WaitMe.CloseWaitme();


                },
                error: function (response) {
                    SysCfg.Log.Methods.WriteLog("DynamicSTEFAtentionCase.js ItHelpCenter.SD.CaseUser.Atention.TSMConsoleUserSwitch.prototype.loadPendingcases Service/SD/Atention.svc/GetNextStep " + "Error no llamo al Servicio [SDConsoleAtentionStart]");
                }
            });
        }
    }
    if (idcase == -1) {
        if (contCases <= 0) {
            ItHelpCenter.SD.CaseUser.Atention.OutSMConsoleUserSwitch(_this.Object, ItHelpCenter.SD.CaseUser.Atention.TResultform.GotoMain, _this.IDSOURCE, _this.CallbackModalResult)
        }
    }
}

ItHelpCenter.SD.CaseUser.Atention.TSMConsoleUserSwitch.prototype.GetIDSDWHOTOCASEPendingcases = function (inIDSDCASE, inIDSDCASEMT) {
    var _this = this.TParent();
    var parameters =
    {
        IDSDCASE: inIDSDCASE,
        IDSDCASEMT: inIDSDCASEMT,
        IDSDTYPEUSER: 4
    };
    var resp = -1;
    parameters = JSON.stringify(parameters)
    $.ajax({
        type: "POST",
        url: SysCfg.App.Properties.xRaiz + 'Service/SD/Console.svc/UserAtentionStart',
        data: parameters,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (response) {
            resp = response;
        },
        error: function (response) {
            SysCfg.Log.Methods.WriteLog("DynamicSTEFAtentionCase.js ItHelpCenter.SD.CaseUser.Atention.TSMConsoleUserSwitch.prototype.GetIDSDWHOTOCASEPendingcases Service/SD/Console.svc/UserAtentionStart" + "Error no llamo al servicio [UserAtentionStart]");
        }
    });
    return resp;
}
