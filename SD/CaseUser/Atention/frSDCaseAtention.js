﻿
ItHelpCenter.SD.CaseUser.Atention.TfrSDCaseAtention = function (inObject, incallbackModalResult, inIDSOURCE, inresponse) {///Atention

    this.TParent = function () {
        return this;
    }.bind(this);

    var _this = this.TParent();

    this.Object = inObject;
    this.IDSOURCE = inIDSOURCE;
    this.CallbackModalResult = incallbackModalResult;//CallbackModalResult(OutStr);        
    var response = inresponse;

    this.TfrAttached = new Array();
    this.Mythis = "TDynamicSTEFAtentionCase_User";
    this.SDWHOTOCASE = response.SDWHOTOCASE;
    this.SDCONSOLEATENTION = response.SDCONSOLEATENTION;
    this.SDTYPEUSER = response.SDTYPEUSER;


    this.IDSDTypeUserListAll = new Array();    
    this.MDLIFESTATUSProfiler = response.MDLIFESTATUSProfiler;
    for (var i = 0; i < _this.MDLIFESTATUSProfiler.MDLIFESTATUSTYPEUSERList.length; i++) {
        var IDTYPEUSER = _this.MDLIFESTATUSProfiler.MDLIFESTATUSTYPEUSERList[i].IDSDTYPEUSER;
        this.IDSDTypeUserListAll.push(IDTYPEUSER);
    }

    

    _this.InitializeComponent();


    var MyHiddenName = "IDSDWHOTOCASE_" + _this.SDWHOTOCASE.IDSDWHOTOCASE;

    if (_this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASESTATUS == 7) _this.img_case.Src = "image/24/CaseYellow.png"

    _this.lblCaseNumber.Text = _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE;

    _this.Lbl_CASE_TITLE.Text = _this.SDCONSOLEATENTION.SDCASECOMPLTE.CASE_TITLE;

    _this.VclFechaInforme.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "fecha_uh4") + _this.formatJSONDate(response.SDCONSOLEATENTION.SDCASECOMPLTE.CASE_DATECREATE);

    _this.VclDescripcion.Text = (_this.SDCONSOLEATENTION.SDCASECOMPLTE.CASE_DESCRIPTION == "" ? "..." : _this.SDCONSOLEATENTION.SDCASECOMPLTE.CASE_DESCRIPTION)



    _this.DivStar.innerHTML = "";
    $(_this.DivStar).addClass("Divtooltip")
    var VcllabelPRIORITYNAME = new Vcllabel(_this.DivStar, "", TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines4")+ _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.PRIORITYNAME);
    $(VcllabelPRIORITYNAME).addClass("tooltiptext");

    if (response.NumStar > 5) { response.NumStar = 5 }
    
    var empty_Star = response.NumStar - _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.IDMDPRIORITY;
    var img_siren;
    for (var j = 0; j < _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.IDMDPRIORITY; j++) {
        img_siren = Uimg(_this.DivStar, "", "image/24/siren_on.png", "");
        img_siren.style.marginTop = "6px";
    }
    for (var j = 0; j < empty_Star; j++) {
        img_siren = Uimg(_this.DivStar, "", "image/24/siren_off.png", "");
        img_siren.style.marginTop = "6px";
    }


    _this.GETMsgAten();

    var mensajes = _this.GETMsgPost(_this.SDWHOTOCASE.IDSDWHOTOCASE);

    for (var Counter2 = 0; Counter2 < response.MDLIFESTATUSProfiler.MDLIFESTATUSList.length; Counter2++) {
        if (response.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].STATUSN == response.SDCONSOLEATENTION.SDCASEMTCOMPLTE.CASEMT_SET_LS_STATUSN) {
            var esta1_span = Uspan(_this.esta1_uh4.This, "", response.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].NAMESTEP);
            esta1_span.style.color = "gray";
        }
    }

    _this.btnGrabarETArray = _this.CargarTabControlAtentionCase(_this.DivGrabarETArray, _this.SDWHOTOCASE.IDSDWHOTOCASE, response);

    _this.LSEND_COMMENTSENABLE = false;
    _this.LSBEGIN_COMMENTSENABLE = false;

    for (var Counter2 = 0; Counter2 < response.MDLIFESTATUSProfiler.MDLIFESTATUSList.length; Counter2++) {

        var memos = "";
        var LSCHANGE_COMMENTS = "";
        var LSEND_COMMENTSLABEL = "";
        var LSBEGIN_COMMENTSLABEL = "";
        var LSHERESTEP_CONFIG = "";
        var HERESTEP_CONFIG;
        var LSNEXTSTEP_CONFIG = "";
        var LSNEXTSTEP_CONFIGList;
        var haymensaje = false;
        //var LSEND_COMMENTSENABLE = false;
        //var LSBEGIN_COMMENTSENABLE = false;


        var Label_LSEND_COMMENTSENABLE = UsrCfg.Traslate.GetLangText(_this.Mythis, "Label_LSEND_COMMENTSENABLE");
        var Label_LSBEGIN_COMMENTSENABLE = UsrCfg.Traslate.GetLangText(_this.Mythis, "Label_LSBEGIN_COMMENTSENABLE");
        if (response.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].STATUSN == response.SDCONSOLEATENTION.SDCASEMTCOMPLTE.CASEMT_SET_LS_STATUSN) {
            var IDMDLIFESTATUSPERMISSION = 0;
            for (var i = 0; i < response.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].MDLIFESTATUSTYPEUSERList.length; i++) {
                if (response.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].MDLIFESTATUSTYPEUSERList[i].IDSDTYPEUSER == response.SDWHOTOCASE.IDSDTYPEUSER) {
                    IDMDLIFESTATUSPERMISSION = response.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].MDLIFESTATUSTYPEUSERList[i].IDMDLIFESTATUSPERMISSION;
                    LSCHANGE_COMMENTS = response.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].MDLIFESTATUSTYPEUSERList[i].LSCHANGE_COMMENTS;
                    LSEND_COMMENTSLABEL = response.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].MDLIFESTATUSTYPEUSERList[i].LSEND_COMMENTSLABEL;
                    LSBEGIN_COMMENTSLABEL = response.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].MDLIFESTATUSTYPEUSERList[i].LSBEGIN_COMMENTSLABEL;
                    LSHERESTEP_CONFIG = response.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].MDLIFESTATUSTYPEUSERList[i].LSHERESTEP_CONFIG;
                    HERESTEP_CONFIG = response.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].MDLIFESTATUSTYPEUSERList[i].HERESTEP_CONFIG;
                    LSNEXTSTEP_CONFIG = response.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].MDLIFESTATUSTYPEUSERList[i].LSNEXTSTEP_CONFIG;
                    LSNEXTSTEP_CONFIGList = response.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].MDLIFESTATUSTYPEUSERList[i].NEXTSTEP_CONFIGList;
                    _this.LSEND_COMMENTSENABLE = response.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].MDLIFESTATUSTYPEUSERList[i].LSEND_COMMENTSENABLE;
                    _this.LSBEGIN_COMMENTSENABLE = response.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].MDLIFESTATUSTYPEUSERList[i].LSBEGIN_COMMENTSENABLE;
                    break;
                }
            }
            if (LSEND_COMMENTSLABEL != "") Label_LSEND_COMMENTSENABLE = LSEND_COMMENTSLABEL;
            if (LSEND_COMMENTSLABEL != "") Label_LSBEGIN_COMMENTSENABLE = LSBEGIN_COMMENTSLABEL;
            if (IDMDLIFESTATUSPERMISSION == 2)//permiso write
            {

                memos += "<table>";
                if (_this.LSEND_COMMENTSENABLE) memos += "<tr><td style='padding:10px;'><p>" + Label_LSEND_COMMENTSENABLE + "</p><textarea id='ta1" + _this.SDWHOTOCASE.IDSDWHOTOCASE + "' cols='100' rows='7'></textarea></td></tr>";
                if (_this.LSBEGIN_COMMENTSENABLE) memos += "<tr><td style='padding:10px;'><p>" + Label_LSBEGIN_COMMENTSENABLE + "</p><textarea id='ta2" + _this.SDWHOTOCASE.IDSDWHOTOCASE + "' cols='100' rows='7'></textarea></td></tr>";

                memos += "</table>";
                //alert('si paso');
                $(_this.DivChangedSteep0).append(memos);

                for (var c = 0; c < response.NEXTLIFESTATUSList.length; c++) {
                    var NAMESTEP = "";
                    var STATUSN = "";
                    var CAPTION = "";
                    for (var d = 0; d < response.NEXTLIFESTATUSList[c].NEXTSTEPList.length; d++) {
                        NAMESTEP += response.NEXTLIFESTATUSList[c].NEXTSTEPList[d].NAMESTEP + "-";
                        STATUSN = response.NEXTLIFESTATUSList[c].NEXTSTEPList[d].STATUSN;
                        var CAPTIONtemp = response.NEXTLIFESTATUSList[c].NEXTSTEPList[d].NAMESTEP;
                        for (var x = 0; x < LSNEXTSTEP_CONFIGList.length; x++) {
                            if (parseInt(STATUSN) == LSNEXTSTEP_CONFIGList[x].STATUSN)
                                CAPTIONtemp = LSNEXTSTEP_CONFIGList[x].Caption;
                        }
                        CAPTION += CAPTIONtemp + "-";
                    }
                    if (response.NEXTLIFESTATUSList[c].NEXTSTEPList.length > 0) {
                        if (haymensaje == false) {
                            if (LSCHANGE_COMMENTS != "") {
                                var qu_uh4 = Vcllabel(_this.DivChangedSteep0, "", TVCLType.BS, TlabelType.H4, LSCHANGE_COMMENTS);
                                qu_uh4.style.fontSize = "15px";
                                qu_uh4.style.fontWeight = "700";
                                qu_uh4.classList.add("text-right");
                            }
                            haymensaje = true;
                        }
                        NAMESTEP = NAMESTEP.substring(0, NAMESTEP.length - 1); //para quitar el -
                        CAPTION = CAPTION.substring(0, CAPTION.length - 1);//para quitar el -
                        var btnYes_uinput = new TVclInputbutton(_this.DivChangedSteep1, "");
                        btnYes_uinput.Text = CAPTION;
                        btnYes_uinput.VCLType = TVCLType.BS;
                        btnYes_uinput.This.classList.add("jmbtn");
                        btnYes_uinput.This.style.marginTop = "5px";
                        btnYes_uinput.This.style.marginBottom = "5px";
                        btnYes_uinput.This.style.marginLeft = "0px";
                        btnYes_uinput.This.style.marginRight = "0px";
                        btnYes_uinput.This.setAttribute("data-Count", c);
                        btnYes_uinput.onClick = function () {
                            var result = _this.CheckStd(5, _this.SDWHOTOCASE.IDSDWHOTOCASE, STATUSN);
                            var count_save = c;
                            if (!result.Res) {
                                $('#txtMensajeReturn' + _this.SDWHOTOCASE.IDSDWHOTOCASE).append(result.MsgInfo);
                                document.location = '#Modal' + _this.SDWHOTOCASE.IDSDWHOTOCASE;

                            }
                            else {
                                //****** for que verifica si hay clicks pendientes eb extra fields  *******************
                                //for (var i = 0; i < response.MDSERVICEEXTRATABLEList.length; i++) {
                                //    var dis = btnGrabarET.Enabled; //$(btnGrabarET.This).attr('disabled');
                                //    if (dis) btnGrabarET.Click();
                                //}
                                for (var i = 0; i < _this.btnGrabarETArray.length; i++) {
                                    var dis = _this.btnGrabarETArray[i].Enabled; //$(btnGrabarET.This).attr('disabled');
                                    if (dis) _this.btnGrabarETArray[i].Click();
                                }

                                var _OutStr1 = '';
                                var _OutStr2 = '';
                                if (_this.LSEND_COMMENTSENABLE) _OutStr1 = $('#ta1' + _this.SDWHOTOCASE.IDSDWHOTOCASE).html();
                                if (_this.LSBEGIN_COMMENTSENABLE) _OutStr2 = $('#ta2' + _this.SDWHOTOCASE.IDSDWHOTOCASE).html();
                                var parameters = {
                                    IDSDWHOTOCASE: response.SDWHOTOCASE.IDSDWHOTOCASE,
                                    nameStep: NAMESTEP,
                                    OutStr1: _OutStr1,
                                    OutStr2: _OutStr2,
                                    Reload: false,
                                    Counter: parseInt($(this).attr("data-Count"))
                                };
                                parameters = JSON.stringify(parameters);
                                $.ajax({
                                    type: "POST",
                                    url: SysCfg.App.Properties.xRaiz + 'Service/SD/Atention.svc/GetNextStep',
                                    data: parameters,
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    async: false,
                                    success: function (response) {
                                        //location.href = location.href;
                                        ItHelpCenter.SD.CaseUser.Atention.OutSMConsoleUserSwitch(_this.Object, ItHelpCenter.SD.CaseUser.Atention.TResultform.Refresh, _this.IDSOURCE, _this.CallbackModalResult)
                                    }
                                });
                            };
                        }
                    }
                }

            }
        }
    }


    //var TagMainContDef = new Array(1);
    //TagMainContDef[0] = new Array(6); //row
    //TagMainContDef[0][0] = new Array(1); //col
    //TagMainContDef[0][0][0] = new Array(1); //row
    //TagMainContDef[0][0][0][0] = new Array(4); //cols
    //TagMainContDef[0][0][0][0][0] = new Array(2); //row
    //TagMainContDef[0][0][0][0][0][0] = new Array(1); //col
    //TagMainContDef[0][0][0][0][1] = new Array(2); //row
    //TagMainContDef[0][0][0][0][1][0] = new Array(1); //col
    //TagMainContDef[0][0][0][0][1][1] = new Array(1); //col
    //TagMainContDef[0][1] = new Array(1); //col
    //TagMainContDef[0][1][0] = new Array(1); //row
    //TagMainContDef[0][1][0][0] = new Array(4); //row 
    //TagMainContDef[0][2] = new Array(2); //cols
    //TagMainContDef[0][2][0] = new Array(2); //rows
    //TagMainContDef[0][2][0][0] = new Array(1); //col
    //TagMainContDef[0][2][0][1] = new Array(1); //col
    //TagMainContDef[0][2][1] = new Array(2); //rows
    //TagMainContDef[0][2][1][0] = new Array(1); //col
    //TagMainContDef[0][2][1][1] = new Array(1); //col
    //TagMainContDef[0][3] = new Array(1); //row
    //TagMainContDef[0][3][0] = new Array(1); //col
    //TagMainContDef[0][4] = new Array(1); //row
    //TagMainContDef[0][4][0] = new Array(1); //col
    //TagMainContDef[0][5] = new Array(3); //row
    //var TagMainCont = VclDivitions(_this.Object, "TagMain" + _this.SDWHOTOCASE.IDSDWHOTOCASE, TagMainContDef);


    //BootStrapContainer(TagMainCont[0].This, TBootStrapContainerType.none);
    //TagMainCont[0].This.style.borderStyle = "solid";
    //TagMainCont[0].This.style.borderColor = "black";
    //TagMainCont[0].This.style.borderWidth = "1px";
    //TagMainCont[0].This.style.marginBottom = "20px";
    //BootStrapCreateRow(TagMainCont[0].Child[0].This);
    //TagMainCont[0].Child[0].This.style.backgroundColor = "#DDE6BD";
    //BootStrapCreateColumn(TagMainCont[0].Child[0].Child[0].This, [12, 12, 12, 12, 12]);
    //BootStrapCreateRow(TagMainCont[0].Child[0].Child[0].Child[0].This);
    //TagMainCont[0].Child[0].Child[0].Child[0].This.classList.add("row-eq-height");
    //BootStrapCreateColumns(TagMainCont[0].Child[0].Child[0].Child[0].Child, [[1, 1, 9, 1], [1, 1, 9, 1], [1, 1, 9, 1], [1, 1, 9, 1], [1, 1, 9, 1]]);

    //BootStrapCreateRow(TagMainCont[0].Child[0].Child[0].Child[0].Child[0].Child[0].This);
    //BootStrapCreateRow(TagMainCont[0].Child[0].Child[0].Child[0].Child[0].Child[1].This);
    //BootStrapCreateColumn(TagMainCont[0].Child[0].Child[0].Child[0].Child[0].Child[0].Child[0].This, [12, 12, 12, 12, 12]);

    //TagMainCont[0].Child[0].Child[0].Child[0].Child[0].Child[0].This.style.backgroundColor = "#808080";
    //TagMainCont[0].Child[0].Child[0].Child[0].Child[0].Child[0].This.style.minHeight = "45px";

    //BootStrapCreateRow(TagMainCont[0].Child[0].Child[0].Child[0].Child[1].Child[0].This);
    //BootStrapCreateRow(TagMainCont[0].Child[0].Child[0].Child[0].Child[1].Child[1].This);
    //BootStrapCreateColumn(TagMainCont[0].Child[0].Child[0].Child[0].Child[1].Child[0].Child[0].This, [12, 12, 12, 12, 12]);


    //TagMainCont[0].Child[0].Child[0].Child[0].Child[1].Child[0].This.style.backgroundColor = "#808080";
    //TagMainCont[0].Child[0].Child[0].Child[0].Child[1].Child[0].This.style.minHeight = "45px";


    //// empezamos a cuadrar el header

    //var img_case; //= document.createElement("div");
    //if (response.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASESTATUS == 7) img_case = Uimg(TagMainCont[0].Child[0].Child[0].Child[0].Child[0].Child[0].Child[0].This, "", "image/24/CaseYellow.png", "");
    //else img_case = Uimg(TagMainCont[0].Child[0].Child[0].Child[0].Child[0].Child[0].Child[0].This, "", "image/24/CaseGreen.png", "");
    //img_case.style.marginTop = "8px";
    ////TagMainCont[0].Child[0].Child[0].Child[0].Child[0].This.style.backgroundColor = "#808080";
    //var NroTicket_uh4 = Vcllabel(TagMainCont[0].Child[0].Child[0].Child[0].Child[1].Child[0].Child[0].This, "", TVCLType.BS, TlabelType.H4, response.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE);
    //NroTicket_uh4.style.fontSize = "20px";
    //NroTicket_uh4.style.fontWeight = "700";
    //NroTicket_uh4.style.color = "white";
    ////TagMainCont[0].Child[0].Child[0].Child[0].Child[1].This.style.backgroundColor = "#808080";

    //var spTitulo = new TVclStackPanel(TagMainCont[0].Child[0].Child[0].Child[0].Child[2].This, "1", 1, [[12], [12], [12], [12], [12]]);
    //var Titu_uh4 = Vcllabel(spTitulo.Column[0].This, "", TVCLType.BS, TlabelType.H4, (response.SDCONSOLEATENTION.SDCASECOMPLTE.CASE_TITLE == "" ? "..." : response.SDCONSOLEATENTION.SDCASECOMPLTE.CASE_TITLE));
    //Titu_uh4.style.fontSize = "20px";
    //Titu_uh4.style.fontWeight = "700";
    //Titu_uh4.style.color = "#0094FF";

    //var spDetalle = new TVclStackPanel(TagMainCont[0].Child[0].Child[0].Child[0].Child[2].This, "1", 1, [[12], [12], [12], [12], [12]]);
    //var deta1cont_uh4 = Vcllabel(spDetalle.Column[0].This, "", TVCLType.BS, TlabelType.H4, (response.SDCONSOLEATENTION.SDCASECOMPLTE.CASE_DESCRIPTION == "" ? "..." : response.SDCONSOLEATENTION.SDCASECOMPLTE.CASE_DESCRIPTION));

    //deta1cont_uh4.style.fontSize = "15px";

    //var img_refresh = Uimg(TagMainCont[0].Child[0].Child[0].Child[0].Child[3].This, "", "image/32/Rules.png", "");
    //img_refresh.style.cursor = "pointer";
    //img_refresh.style.marginTop = "8px";
    //img_refresh.addEventListener("click", function () {
    //    //location.href = location.href;
    //    ItHelpCenter.SD.CaseUser.Atention.OutSMConsoleUserSwitch(_this.Object, ItHelpCenter.SD.CaseUser.Atention.TResultform.Refresh, _this.IDSOURCE, _this.CallbackModalResult)

    //});


    //BootStrapCreateRow(TagMainCont[0].Child[1].This);
    //TagMainCont[0].Child[1].This.style.backgroundColor = "#DDE6BD";
    //BootStrapCreateColumn(TagMainCont[0].Child[1].Child[0].This, [12, 12, 12, 12, 12]);
    //BootStrapCreateRow(TagMainCont[0].Child[1].Child[0].Child[0].This);
    //TagMainCont[0].Child[1].Child[0].Child[0].This.classList.add("row-eq-height");
    ////BootStrapCreateColumns(TagMainCont[0].Child[1].Child[0].Child[0].Child, [[2, 7, 3], [2, 7, 3], [2, 7, 3], [2, 7, 3], [2, 7, 3]]);
    //BootStrapCreateColumns(TagMainCont[0].Child[1].Child[0].Child[0].Child, [[2, 4, 3, 3], [2, 4, 3, 3], [2, 4, 3, 3], [2, 4, 3, 3], [2, 4, 3, 3]]);


    //TagMainCont[0].Child[1].Child[0].Child[0].Child[0].This.style.top = "-30px";

    //if (response.NumStar > 5) { response.NumStar = 5 }
    //var empty_Star = response.NumStar - response.SDCONSOLEATENTION.SDCASEMTCOMPLTE.IDMDPRIORITY;
    //var img_siren;
    //for (var j = 0; j < response.SDCONSOLEATENTION.SDCASEMTCOMPLTE.IDMDPRIORITY; j++) {
    //    img_siren = Uimg(TagMainCont[0].Child[1].Child[0].Child[0].Child[0].This, "", "image/24/siren_on.png", "");
    //    img_siren.style.marginTop = "6px";
    //}
    //for (var j = 0; j < empty_Star; j++) {
    //    img_siren = Uimg(TagMainCont[0].Child[1].Child[0].Child[0].Child[0].This, "", "image/24/siren_off.png", "");
    //    img_siren.style.marginTop = "6px";
    //}
    //var fecha_uh4 = Vcllabel(TagMainCont[0].Child[1].Child[0].Child[0].Child[1].This, "", TVCLType.BS, TlabelType.H4, UsrCfg.Traslate.GetLangText(_this.Mythis, "fecha_uh4") + _this.formatJSONDate(response.SDCONSOLEATENTION.SDCASECOMPLTE.CASE_DATECREATE));
    //fecha_uh4.style.fontSize = "15px";
    //fecha_uh4.style.fontWeight = "700";
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //try {

    //    TagMainCont[0].Child[1].Child[0].Child[0].Child[2].This.classList.add("text-right");

    //    var lueMessage = UVCLRepositoryLookUp(TagMainCont[0].Child[1].Child[0].Child[0].Child[2].This, "Repositoriolook1" + _this.SDWHOTOCASE.IDSDWHOTOCASE, UsrCfg.Traslate.GetLangText(_this.Mythis, "lueMessage"), true, true, null);
    //    lueMessage.Uulli.ul.style.listStyle = "none";
    //    lueMessage.Uulli.ul.style.marginTop = "10px";
    //    lueMessage.Uulli.ul.style.marginBottom = "0px";
    //    lueMessage.Caption.style.textDecoration = "none";
    //    lueMessage.Caption.style.cursor = "pointer";
    //    lueMessage.Caption.style.fontWeight = "700";
    //    lueMessage.Caption.style.color = "#0094FF";

    //    //lueMessage.Caption.style.marginTop = "100px";
    //    BootStrapCreateRow(lueMessage.DivRootContainer.This);
    //    lueMessage.DivRootContainer.This.classList.add("message-content");
    //    lueMessage.DivRootContainer.This.style.display = "none";
    //    BootStrapCreateColumn(lueMessage.DivRootContainer.Child[0].This, [12, 12, 12, 12, 12]);
    //    BootStrapCreateRows(lueMessage.DivRootContainer.Child[0].Child);
    //    BootStrapCreateColumns(lueMessage.DivRootContainer.Child[0].Child[0].Child, [[5, 7], [5, 7], [5, 7], [5, 7], [5, 7]]);
    //    Vcllabel(lueMessage.DivRootContainer.Child[0].Child[0].Child[0].This, "", TVCLType.BS, TlabelType.H4, UsrCfg.Traslate.GetLangText(_this.Mythis, "Uh4_Users"));
    //    var user_uul = Uul(lueMessage.DivRootContainer.Child[0].Child[0].Child[1].This, "");
    //    user_uul.style.listStyle = "none";
    //    var IsVisible = _this.GetUsersForSendMessage(user_uul, _this.SDWHOTOCASE.IDSDWHOTOCASE, response.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE, response.SDTYPEUSER.IDSDTYPEUSER)
    //    if (IsVisible) $(lueMessage.DivRootContainer.Child[0].Child[0].This).show();
    //    else $(lueMessage.DivRootContainer.Child[0].Child[0].This).hide();
    //    lueMessage.DivRootContainer.Child[0].Child[0].Child[1].This.classList.add("text-left");
    //    BootStrapCreateColumns(lueMessage.DivRootContainer.Child[0].Child[1].Child, [[12], [12], [12], [12], [12]]);
    //    Vcllabel(lueMessage.DivRootContainer.Child[0].Child[1].Child[0].This, "", TVCLType.BS, TlabelType.H4, UsrCfg.Traslate.GetLangText(_this.Mythis, "Uh4_Message"));

    //    lueMessage.DivRootContainer.Child[0].Child[1].Child[0].This.classList.add("text-left");
    //    var mensaje_utexarea = new TVclMemo(lueMessage.DivRootContainer.Child[0].Child[1].Child[0].This, "TxtMessage" + _this.SDWHOTOCASE.IDSDWHOTOCASE);
    //    mensaje_utexarea.Text = "";
    //    mensaje_utexarea.VCLType = TVCLType.BS;
    //    mensaje_utexarea.Rows = 10;
    //    mensaje_utexarea.Cols = 50;
    //    BootStrapCreateColumns(lueMessage.DivRootContainer.Child[0].Child[2].Child, [[6, 6], [6, 6], [6, 6], [6, 6], [6, 6]]);
    //    lueMessage.ButtonCancelCaption.This.classList.add("jmbtn");

    //    lueMessage.ButtonCancelCaption.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ButtonCancelCaption");
    //    lueMessage.ButtonCancelCaption.onClick = function () {
    //        lueMessage.Caption.click();
    //    }
    //    lueMessage.ButtonOkCaption.This.classList.add("jmbtn");
    //    lueMessage.ButtonOkCaption.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ButtonOkCaption");
    //    lueMessage.ButtonOkCaption.onClick = function () {
    //        var listItem = user_uul.getElementsByTagName("li");
    //        var listausuarios = new Array();
    //        var idHC = 0;
    //        for (var i = 0; i < listItem.length; i++) {

    //            idHC = listItem[i].getAttribute("data-idHC");
    //            if (listItem[i].getElementsByTagName("input")[0].checked) {
    //                listausuarios.push(listItem[i].getElementsByTagName("input")[0].id);
    //            }
    //        }
    //        var parameters = {
    //            IDSDWHOTOCASE: idHC,
    //            IDSDTYPEUSERListOUT: listausuarios,
    //            Msg: mensaje_utexarea.Text
    //        };
    //        parameters = JSON.stringify(parameters);
    //        if (parameters != "") {
    //            $.ajax({
    //                type: "POST",
    //                url: SysCfg.App.Properties.xRaiz + 'Service/SD/Atention.svc/SaveMessage',
    //                data: parameters,
    //                contentType: "application/json; charset=utf-8",
    //                dataType: "json",
    //                async: false,
    //                success: function (response) {
    //                    mensaje_utexarea.Text = "";
    //                    //if (listItem.length > 1) {
    //                    //    for (var i = 0; i < listItem.length; i++) {
    //                    //        listItem[i].getElementsByTagName("input")[0].checked = false;
    //                    //    }
    //                    //}
    //                    lueMessage.Caption.click();


    //                    var mensajes = _this.GETMsgPost(_this.SDWHOTOCASE.IDSDWHOTOCASE);
    //                    //_this.DivMsg.innerHTML = "";
    //                    //if (mensajes.length > 0) {
    //                    //    for (var i = 0; i < mensajes.length; i++) {
    //                    //        var deta2cont_uh4 = Vcllabel(_this.DivMsg, "", TVCLType.BS, TlabelType.H4, mensajes[i]);
    //                    //        deta2cont_uh4.style.fontSize = "15px";
    //                    //    }
    //                    //} else {
    //                    //    var deta2cont_uh4 = Vcllabel(_this.DivMsg, "", TVCLType.BS, TlabelType.H4, UsrCfg.Traslate.GetLangText(_this.Mythis, "deta2cont_uh4"));
    //                    //    deta2cont_uh4.style.fontSize = "15px";
    //                    //}


    //                },
    //                error: function (response) {
    //                    SysCfg.Log.Methods.WriteLog("DynamicSTEFAtentionCase.js ItHelpCenter.SD.CaseUser.Atention.TSMConsoleUserSwitch.prototype.loadPendingcases Service/SD/Atention.svc/SaveMessage " + "Error no llamo al servicio");
    //                }
    //            });
    //        }
    //        else { $('#error_message').modal('show'); }
    //    }
    //} catch (e) {
    //    SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.CaseUser.Atention.TfrSDCaseAtention Message", e);
    //}




    ////////////////////////////////////////////////////////////////////////   
    //try {
    //    TagMainCont[0].Child[1].Child[0].Child[0].Child[3].This.classList.add("text-right");
    //    var lueAttached = new UVCLRepositoryLookUp(TagMainCont[0].Child[1].Child[0].Child[0].Child[3].This, "Repositoriolook2" + _this.SDWHOTOCASE.IDSDWHOTOCASE, UsrCfg.Traslate.GetLangText(_this.Mythis, "lb_Attached"), true, true, function () {
    //        if (lueAttached.Active) {
    //            _this.TfrAttached = new ItHelpCenter.SD.Shared.Attached.UfrSDAttached(lueAttached.DivRootContainer.Child[0].Child[1].Child[0].This, "Attached",
    //                function () {

    //                },
    //                response.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE,
    //                response.SDCONSOLEATENTION.SDCASECOMPLTE.IDUSER,
    //                response.SDWHOTOCASE.IDSDTYPEUSER,
    //                response.SDCONSOLEATENTION.SDCASEMTCOMPLTE.MT_IDMDSERVICETYPE,

    //                true, false, new Array()
    //            );
    //        }
    //    });
    //    lueAttached.Uulli.ul.style.listStyle = "none";
    //    lueAttached.Uulli.ul.style.marginTop = "10px";
    //    lueAttached.Uulli.ul.style.marginBottom = "0px";
    //    lueAttached.Caption.style.textDecoration = "none";
    //    lueAttached.Caption.style.cursor = "pointer";
    //    lueAttached.Caption.style.fontWeight = "700";
    //    lueAttached.Caption.style.color = "#0094FF";

    //    //_this.TfrAttached[indx] = new ItHelpCenter.SD.Shared.Attached.UfrSDAttached(lueAttached.DivRootContainer.Child[0].Child[1].Child[0].This, "Attached",
    //    //    function () {

    //    //    },
    //    //    response.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE,                        
    //    //    response.SDCONSOLEATENTION.SDCASECOMPLTE.IDUSER,                        
    //    //    response.SDWHOTOCASE.IDSDTYPEUSER, new Array()
    //    //); 
    //    //indx = indx + 1;

    //    BootStrapCreateRow(lueAttached.DivRootContainer.This);
    //    lueAttached.DivRootContainer.This.classList.add("message-content");
    //    lueAttached.DivRootContainer.This.style.display = "none";
    //    BootStrapCreateColumn(lueAttached.DivRootContainer.Child[0].This, [12, 12, 12, 12, 12]);
    //    BootStrapCreateRows(lueAttached.DivRootContainer.Child[0].Child);
    //    BootStrapCreateColumns(lueAttached.DivRootContainer.Child[0].Child[0].Child, [[5, 7], [5, 7], [5, 7], [5, 7], [5, 7]]);

    //    _this.user_uul = Uul(lueAttached.DivRootContainer.Child[0].Child[0].Child[1].This, "");
    //    _this.user_uul.style.listStyle = "none";
    //    var IsVisible = _this.GetUsersForSendMessage(_this.user_uul, _this.SDWHOTOCASE.IDSDWHOTOCASE, response.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE, response.SDTYPEUSER.IDSDTYPEUSER)
    //    if (IsVisible) $(lueAttached.DivRootContainer.Child[0].Child[0].This).show();
    //    else $(lueAttached.DivRootContainer.Child[0].Child[0].This).hide();
    //    BootStrapCreateColumns(lueAttached.DivRootContainer.Child[0].Child[1].Child, [[12], [12], [12], [12], [12]]);

    //    lueAttached.DivRootContainer.Child[0].Child[1].Child[0].This.classList.add("text-left");

    //    BootStrapCreateColumns(lueAttached.DivRootContainer.Child[0].Child[2].Child, [[6, 6], [6, 6], [6, 6], [6, 6], [6, 6]]);
    //    $(lueAttached.DivRootContainer.Child[0].Child[2].Child[0].This).css("text-align", "center");
    //    $(lueAttached.DivRootContainer.Child[0].Child[2].Child[1].This).css("text-align", "center");


    //    $(lueAttached.DivRootContainer.This).css("width", "250%");

    //    lueAttached.ButtonCancelCaption.This.classList.add("jmbtn");
    //    lueAttached.ButtonCancelCaption.This.style.width = "50%";
    //    lueAttached.ButtonCancelCaption.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lueAttached.ButtonCancelCaption.Text");
    //    lueAttached.ButtonCancelCaption.onClick = function () {
    //        lueAttached.Caption.click();
    //    }
    //    lueAttached.ButtonOkCaption.This.classList.add("jmbtn");
    //    lueAttached.ButtonOkCaption.This.style.width = "50%";
    //    lueAttached.ButtonOkCaption.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lueAttached.ButtonOkCaption.Text", "Accept");
    //    lueAttached.ButtonOkCaption.onClick = function () {
    //        _this.TfrAttached.SaveAttach(response.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE, function () {
    //            lueAttached.Caption.click();
    //        });
    //    }
    //} catch (e) {
    //    SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.CaseUser.Atention.TfrSDCaseAtention Attached", e);
    //}


    /////////////////////////////////////////////////////////////////


    //BootStrapCreateRow(TagMainCont[0].Child[2].This);
    //TagMainCont[0].Child[2].This.classList.add("row-eq-height");
    //TagMainCont[0].Child[2].This.style.backgroundColor = "white";
    //TagMainCont[0].Child[2].This.style.borderTop = "1px solid black";
    //TagMainCont[0].Child[2].This.style.borderBottom = "1px solid black";
    //BootStrapCreateColumns(TagMainCont[0].Child[2].Child, [[6, 6], [6, 6], [6, 6], [6, 6], [6, 6]]);
    //TagMainCont[0].Child[2].Child[1].This.style.borderLeft = "1px solid black";
    //BootStrapCreateRow(TagMainCont[0].Child[2].Child[0].Child[0].This);
    //BootStrapCreateColumn(TagMainCont[0].Child[2].Child[0].Child[0].Child[0].This, [12, 12, 12, 12, 12]);
    //TagMainCont[0].Child[2].Child[0].Child[0].This.style.backgroundColor = "#EEF2DE";
    ////var str = ;//"Detail:"
    //var deta3_uh4 = Vcllabel(TagMainCont[0].Child[2].Child[0].Child[0].Child[0].This, "", TVCLType.BS, TlabelType.H4, UsrCfg.Traslate.GetLangText(_this.Mythis, "deta3_uh4"));
    //deta3_uh4.style.fontSize = "15px";
    //deta3_uh4.style.fontWeight = "700";
    //BootStrapCreateRow(TagMainCont[0].Child[2].Child[0].Child[1].This);
    //TagMainCont[0].Child[2].Child[0].Child[1].This.style.backgroundColor = "white";
    //TagMainCont[0].Child[2].Child[0].Child[1].This.style.borderTop = "1px solid black";
    //BootStrapCreateColumn(TagMainCont[0].Child[2].Child[0].Child[1].Child[0].This, [12, 12, 12, 12, 12]);

    ////_this.DivAtntion = TagMainCont[0].Child[2].Child[0].Child[1].Child[0].This;
    ////_this.GETMsgAten();
    ///*
    //var deta1cont_uh4 = Vcllabel(TagMainCont[0].Child[2].Child[0].Child[1].Child[0].This, "", TVCLType.BS, TlabelType.H4, response.SDCONSOLEATENTION.SDCASECOMPLTE.CASE_DESCRIPTION);
    //deta1cont_uh4.style.fontSize = "15px";*/
    //BootStrapCreateRow(TagMainCont[0].Child[2].Child[1].Child[0].This);


    //BootStrapCreateColumn(TagMainCont[0].Child[2].Child[1].Child[0].Child[0].This, [12, 12, 12, 12, 12]);
    //TagMainCont[0].Child[2].Child[1].Child[0].Child[0].This.style.backgroundColor = "#EEF2DE";
    //var deta2_uh4 = Vcllabel(TagMainCont[0].Child[2].Child[1].Child[0].Child[0].This, "", TVCLType.BS, TlabelType.H4, UsrCfg.Traslate.GetLangText(_this.Mythis, "deta2_uh4"));
    //deta2_uh4.style.fontSize = "15px";
    //deta2_uh4.style.fontWeight = "700";
    //BootStrapCreateRow(TagMainCont[0].Child[2].Child[1].Child[1].This);
    //TagMainCont[0].Child[2].Child[1].Child[1].This.style.backgroundColor = "white";
    //TagMainCont[0].Child[2].Child[1].Child[1].This.style.borderTop = "1px solid black";

    //BootStrapCreateColumn(TagMainCont[0].Child[2].Child[1].Child[1].Child[0].This, [12, 12, 12, 12, 12]);
    ////_this.DivMsg = TagMainCont[0].Child[2].Child[1].Child[1].Child[0].This;
    ////var mensajes = _this.GETMsgPost(_this.SDWHOTOCASE.IDSDWHOTOCASE);


    ////if (mensajes.length > 0) {
    ////    for (var i = 0; i < mensajes.length; i++) {
    ////        var deta2cont_uh4 = Vcllabel(_this.DivMsg, "", TVCLType.BS, TlabelType.H4, mensajes[i]);
    ////        deta2cont_uh4.style.fontSize = "15px";
    ////    }
    ////} else {
    ////    var deta2cont_uh4 = Vcllabel(_this.DivMsg, "", TVCLType.BS, TlabelType.H4, UsrCfg.Traslate.GetLangText(_this.Mythis, "deta2cont_uh4"));
    ////    deta2cont_uh4.style.fontSize = "15px";
    ////}

    //BootStrapCreateRow(TagMainCont[0].Child[3].This);
    //TagMainCont[0].Child[3].This.style.backgroundColor = "#EEF2DE";
    //TagMainCont[0].Child[3].This.style.borderBottom = "1px solid black";
    //BootStrapCreateColumn(TagMainCont[0].Child[3].Child[0].This, [12, 12, 12, 12, 12]);
    //var esta1_uh4 = Vcllabel(TagMainCont[0].Child[3].Child[0].This, "", TVCLType.BS, TlabelType.H4, UsrCfg.Traslate.GetLangText(_this.Mythis, "esta1_uh4"));
    //esta1_uh4.style.fontSize = "15px";
    //esta1_uh4.style.fontWeight = "700";

    //for (var Counter2 = 0; Counter2 < response.MDLIFESTATUSProfiler.MDLIFESTATUSList.length; Counter2++) {
    //    if (response.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].STATUSN == response.SDCONSOLEATENTION.SDCASEMTCOMPLTE.CASEMT_SET_LS_STATUSN) {
    //        var esta1_span = Uspan(esta1_uh4, "", response.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].NAMESTEP);
    //        esta1_span.style.color = "gray";
    //    }
    //}

    //BootStrapCreateRow(TagMainCont[0].Child[4].This);
    //TagMainCont[0].Child[4].This.style.borderBottom = "1px solid black";
    //BootStrapCreateColumn(TagMainCont[0].Child[4].Child[0].This, [12, 12, 12, 12, 12]);
    //var btnGrabarETArray = _this.CargarTabControlAtentionCase(TagMainCont[0].Child[4].Child[0].This, _this.SDWHOTOCASE.IDSDWHOTOCASE, response);

    //BootStrapCreateRow(TagMainCont[0].Child[5].This);
    //TagMainCont[0].Child[5].This.style.backgroundColor = "#DDE6BD";
    //BootStrapCreateColumns(TagMainCont[0].Child[5].Child, [[8, 2, 2], [8, 2, 2], [8, 2, 2], [8, 2, 2], [8, 2, 2]]);
    ////********* Bloque de botones para cambiar de step *********************

    //_this.LSEND_COMMENTSENABLE = false;
    //_this.LSBEGIN_COMMENTSENABLE = false;

    //for (var Counter2 = 0; Counter2 < response.MDLIFESTATUSProfiler.MDLIFESTATUSList.length; Counter2++) {

    //    var memos = "";
    //    var LSCHANGE_COMMENTS = "";
    //    var LSEND_COMMENTSLABEL = "";
    //    var LSBEGIN_COMMENTSLABEL = "";
    //    var LSHERESTEP_CONFIG = "";
    //    var HERESTEP_CONFIG;
    //    var LSNEXTSTEP_CONFIG = "";
    //    var LSNEXTSTEP_CONFIGList;
    //    var haymensaje = false;
    //    //var LSEND_COMMENTSENABLE = false;
    //    //var LSBEGIN_COMMENTSENABLE = false;


    //    var Label_LSEND_COMMENTSENABLE = UsrCfg.Traslate.GetLangText(_this.Mythis, "Label_LSEND_COMMENTSENABLE");
    //    var Label_LSBEGIN_COMMENTSENABLE = UsrCfg.Traslate.GetLangText(_this.Mythis, "Label_LSBEGIN_COMMENTSENABLE");
    //    if (response.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].STATUSN == response.SDCONSOLEATENTION.SDCASEMTCOMPLTE.CASEMT_SET_LS_STATUSN) {
    //        var IDMDLIFESTATUSPERMISSION = 0;
    //        for (var i = 0; i < response.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].MDLIFESTATUSTYPEUSERList.length; i++) {
    //            if (response.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].MDLIFESTATUSTYPEUSERList[i].IDSDTYPEUSER == response.SDWHOTOCASE.IDSDTYPEUSER) {
    //                IDMDLIFESTATUSPERMISSION = response.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].MDLIFESTATUSTYPEUSERList[i].IDMDLIFESTATUSPERMISSION;
    //                LSCHANGE_COMMENTS = response.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].MDLIFESTATUSTYPEUSERList[i].LSCHANGE_COMMENTS;
    //                LSEND_COMMENTSLABEL = response.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].MDLIFESTATUSTYPEUSERList[i].LSEND_COMMENTSLABEL;
    //                LSBEGIN_COMMENTSLABEL = response.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].MDLIFESTATUSTYPEUSERList[i].LSBEGIN_COMMENTSLABEL;
    //                LSHERESTEP_CONFIG = response.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].MDLIFESTATUSTYPEUSERList[i].LSHERESTEP_CONFIG;
    //                HERESTEP_CONFIG = response.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].MDLIFESTATUSTYPEUSERList[i].HERESTEP_CONFIG;
    //                LSNEXTSTEP_CONFIG = response.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].MDLIFESTATUSTYPEUSERList[i].LSNEXTSTEP_CONFIG;
    //                LSNEXTSTEP_CONFIGList = response.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].MDLIFESTATUSTYPEUSERList[i].NEXTSTEP_CONFIGList;
    //                _this.LSEND_COMMENTSENABLE = response.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].MDLIFESTATUSTYPEUSERList[i].LSEND_COMMENTSENABLE;
    //                _this.LSBEGIN_COMMENTSENABLE = response.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].MDLIFESTATUSTYPEUSERList[i].LSBEGIN_COMMENTSENABLE;
    //                break;
    //            }
    //        }
    //        if (LSEND_COMMENTSLABEL != "") Label_LSEND_COMMENTSENABLE = LSEND_COMMENTSLABEL;
    //        if (LSEND_COMMENTSLABEL != "") Label_LSBEGIN_COMMENTSENABLE = LSBEGIN_COMMENTSLABEL;
    //        if (IDMDLIFESTATUSPERMISSION == 2)//permiso write
    //        {

    //            memos += "<table>";
    //            if (_this.LSEND_COMMENTSENABLE) memos += "<tr><td style='padding:10px;'><p>" + Label_LSEND_COMMENTSENABLE + "</p><textarea id='ta1" + _this.SDWHOTOCASE.IDSDWHOTOCASE + "' cols='100' rows='7'></textarea></td></tr>";
    //            if (_this.LSBEGIN_COMMENTSENABLE) memos += "<tr><td style='padding:10px;'><p>" + Label_LSBEGIN_COMMENTSENABLE + "</p><textarea id='ta2" + _this.SDWHOTOCASE.IDSDWHOTOCASE + "' cols='100' rows='7'></textarea></td></tr>";

    //            memos += "</table>";
    //            //alert('si paso');
    //            $(TagMainCont[0].Child[4].Child[0].This).append(memos);

    //            for (var c = 0; c < response.NEXTLIFESTATUSList.length; c++) {
    //                var NAMESTEP = "";
    //                var STATUSN = "";
    //                var CAPTION = "";
    //                for (var d = 0; d < response.NEXTLIFESTATUSList[c].NEXTSTEPList.length; d++) {
    //                    NAMESTEP += response.NEXTLIFESTATUSList[c].NEXTSTEPList[d].NAMESTEP + "-";
    //                    STATUSN = response.NEXTLIFESTATUSList[c].NEXTSTEPList[d].STATUSN;
    //                    var CAPTIONtemp = response.NEXTLIFESTATUSList[c].NEXTSTEPList[d].NAMESTEP;
    //                    for (var x = 0; x < LSNEXTSTEP_CONFIGList.length; x++) {
    //                        if (parseInt(STATUSN) == LSNEXTSTEP_CONFIGList[x].STATUSN)
    //                            CAPTIONtemp = LSNEXTSTEP_CONFIGList[x].Caption;
    //                    }
    //                    CAPTION += CAPTIONtemp + "-";
    //                }
    //                if (response.NEXTLIFESTATUSList[c].NEXTSTEPList.length > 0) {
    //                    if (haymensaje == false) {
    //                        if (LSCHANGE_COMMENTS != "") {
    //                            var qu_uh4 = Vcllabel(TagMainCont[0].Child[5].Child[0].This, "", TVCLType.BS, TlabelType.H4, LSCHANGE_COMMENTS);
    //                            qu_uh4.style.fontSize = "15px";
    //                            qu_uh4.style.fontWeight = "700";
    //                            qu_uh4.classList.add("text-right");
    //                        }
    //                        haymensaje = true;
    //                    }
    //                    NAMESTEP = NAMESTEP.substring(0, NAMESTEP.length - 1); //para quitar el -
    //                    CAPTION = CAPTION.substring(0, CAPTION.length - 1);//para quitar el -
    //                    var btnYes_uinput = new TVclInputbutton(TagMainCont[0].Child[5].Child[1].This, "");
    //                    btnYes_uinput.Text = CAPTION;
    //                    btnYes_uinput.VCLType = TVCLType.BS;
    //                    btnYes_uinput.This.classList.add("jmbtn");
    //                    btnYes_uinput.This.style.marginTop = "5px";
    //                    btnYes_uinput.This.style.marginBottom = "5px";
    //                    btnYes_uinput.This.style.marginLeft = "0px";
    //                    btnYes_uinput.This.style.marginRight = "0px";
    //                    btnYes_uinput.This.setAttribute("data-Count", c);
    //                    btnYes_uinput.onClick = function () {
    //                        var result = _this.CheckStd(5, _this.SDWHOTOCASE.IDSDWHOTOCASE, STATUSN);
    //                        var count_save = c;
    //                        if (!result.Res) {
    //                            $('#txtMensajeReturn' + _this.SDWHOTOCASE.IDSDWHOTOCASE).append(result.MsgInfo);
    //                            document.location = '#Modal' + _this.SDWHOTOCASE.IDSDWHOTOCASE;

    //                        }
    //                        else {
    //                            //****** for que verifica si hay clicks pendientes eb extra fields  *******************
    //                            //for (var i = 0; i < response.MDSERVICEEXTRATABLEList.length; i++) {
    //                            //    var dis = btnGrabarET.Enabled; //$(btnGrabarET.This).attr('disabled');
    //                            //    if (dis) btnGrabarET.Click();
    //                            //}
    //                            for (var i = 0; i < btnGrabarETArray.length; i++) {
    //                                var dis = btnGrabarETArray[i].Enabled; //$(btnGrabarET.This).attr('disabled');
    //                                if (dis) btnGrabarETArray[i].Click();
    //                            }

    //                            var _OutStr1 = '';
    //                            var _OutStr2 = '';
    //                            if (_this.LSEND_COMMENTSENABLE) _OutStr1 = $('#ta1' + _this.SDWHOTOCASE.IDSDWHOTOCASE).html();
    //                            if (_this.LSBEGIN_COMMENTSENABLE) _OutStr2 = $('#ta2' + _this.SDWHOTOCASE.IDSDWHOTOCASE).html();
    //                            var parameters = {
    //                                IDSDWHOTOCASE: response.SDWHOTOCASE.IDSDWHOTOCASE,
    //                                nameStep: NAMESTEP,
    //                                OutStr1: _OutStr1,
    //                                OutStr2: _OutStr2,
    //                                Reload: false,
    //                                Counter: parseInt($(this).attr("data-Count"))
    //                            };
    //                            parameters = JSON.stringify(parameters);
    //                            $.ajax({
    //                                type: "POST",
    //                                url: SysCfg.App.Properties.xRaiz + 'Service/SD/Atention.svc/GetNextStep',
    //                                data: parameters,
    //                                contentType: "application/json; charset=utf-8",
    //                                dataType: "json",
    //                                async: false,
    //                                success: function (response) {
    //                                    //location.href = location.href;
    //                                    ItHelpCenter.SD.CaseUser.Atention.OutSMConsoleUserSwitch(_this.Object, ItHelpCenter.SD.CaseUser.Atention.TResultform.Refresh, _this.IDSOURCE, _this.CallbackModalResult)
    //                                }
    //                            });
    //                        };
    //                    }
    //                }
    //            }

    //        }
    //    }
    //}
}

ItHelpCenter.SD.CaseUser.Atention.TfrSDCaseAtention.prototype.GETMsgPost = function (inIDSDWHOTOCASE) {

    var _this = this.TParent();
    _this.DivMsg.innerHTML = "";
    var Param = new SysCfg.Stream.Properties.TParam();
    var Param1 = false;
    try {
        Param.Inicialize();
        //*********** SDOPERATION_ATTENTION_GET ***************************** 
        Param.AddInt32(UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASE.FieldName, _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        if (Param1)
            Param.AddUnknown(UsrCfg.InternoAtisNames.SDWHOTOCASE.DATELASTREAD.FieldName, UsrCfg.InternoAtisNames.SDOPERATION_POST.NAME_TABLE + "." + UsrCfg.InternoAtisNames.SDOPERATION_POST.MESSAGE_DATE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        else
            Param.AddDateTime(UsrCfg.InternoAtisNames.SDWHOTOCASE.DATELASTREAD.FieldName, SysCfg.DateTimeMethods.formatJSONDateTimetoDate(_this.SDWHOTOCASE.DATELASTREAD), SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        Param.AddInt32(UsrCfg.InternoAtisNames.SDOPERATION_POST.POST_IDSDTYPEUSER.FieldName, _this.SDWHOTOCASE.IDSDTYPEUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var DS_SDOPERATION_POST = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "GETSDOPERATION_POST", Param.ToBytes());
        if (DS_SDOPERATION_POST.ResErr.NotError) {
            if (DS_SDOPERATION_POST.DataSet.RecordCount > 0) {
                DS_SDOPERATION_POST.DataSet.First();
                var USERNAME = "";
                var IDSDOPERATION_POST = 0;
                var MESSAGE_DATE = SysCfg.DB.Properties.SVRNOW();
                var POST_MESSAGE = "";
                var IDSDTYPEUSER = UsrCfg.SD.Properties.TSDTypeUser.GetEnum(undefined);
                var IDSDWHOTOCASETYPE = UsrCfg.SD.Properties.TIDSDWHOTOCASETYPE.GetEnum(undefined);
                var IDSDTYPEUSERNAME = "";
                var IDSDWHOTOCASETYPENAME = "";

                var fechatemp = new Date();

                _this.DivMsg.style.marginTop = "7px";

                while (!(DS_SDOPERATION_POST.DataSet.Eof)) {
                    USERNAME = DS_SDOPERATION_POST.DataSet.RecordSet.FieldName("USERNAME").asString();
                    IDSDOPERATION_POST = DS_SDOPERATION_POST.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_POST.IDSDOPERATION_POST.FieldName).asInt32();
                    IDSDTYPEUSER = UsrCfg.SD.Properties.TSDTypeUser.GetEnum(DS_SDOPERATION_POST.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDTYPEUSER.FieldName).asInt32());
                    IDSDWHOTOCASETYPE = UsrCfg.SD.Properties.TIDSDWHOTOCASETYPE.GetEnum(DS_SDOPERATION_POST.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDWHOTOCASETYPE.FieldName).asInt32());
                    IDSDTYPEUSERNAME = IDSDTYPEUSER.name;
                    IDSDWHOTOCASETYPENAME = IDSDWHOTOCASETYPE.name;

                    MESSAGE_DATE = DS_SDOPERATION_POST.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_POST.MESSAGE_DATE.FieldName).asDateTime();
                    POST_MESSAGE = DS_SDOPERATION_POST.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_POST.POST_MESSAGE.FieldName).asString();

                    if (SysCfg.DateTimeMethods.DayOfYear(MESSAGE_DATE) != SysCfg.DateTimeMethods.DayOfYear(fechatemp)) {
                        var VclStackPanel = new TVclStackPanel(_this.DivMsg, "1", 1, [[12], [12], [12], [12], [12]]);
                        var lblFecha = new TVcllabel(VclStackPanel.Column[0].This, "", TlabelType.H0);
                        lblFecha.Text = SysCfg.DateTimeMethods.ToDateString(MESSAGE_DATE);
                        lblFecha.VCLType = TVCLType.BS;
                    }

                    var VclStackPanel = new TVclStackPanel(_this.DivMsg, "3", 3, [[3, 7, 2], [3, 7, 2], [3, 7, 2], [3, 7, 2], [3, 7, 2]]);
                    VclStackPanel.Row.This.style.marginBottom = "6px";

                    var VclStackPanel2 = new TVclStackPanel(VclStackPanel.Column[0].This, "2", 2, [[3, 9], [3, 9], [3, 9], [3, 9], [3, 9]]);

                    var imgImage = new TVclImagen(VclStackPanel2.Column[0].This, "");
                    imgImage.Src = "image/24/user-blue2.png";
                    imgImage.Title = USERNAME + "(" + IDSDWHOTOCASETYPENAME + " " + IDSDTYPEUSERNAME + ")" + " says:";
                    imgImage.This.style.marginTop = "-6px";

                    var lblUser = new TVcllabel(VclStackPanel2.Column[1].This, "", TlabelType.H0);
                    lblUser.This.style.marginLeft = "5px";
                    lblUser.This.style.fontSize = "13px";
                    lblUser.This.style.fontWeight = "normal";
                    lblUser.This.style.color = "rgb(128, 128, 128)";
                    lblUser.Text = USERNAME;

                    var lblMensaje = new TVcllabel(VclStackPanel.Column[1].This, "", TlabelType.H0);

                    lblMensaje.This.style.fontSize = "15px";
                    lblMensaje.This.style.fontWeight = "normal";
                    lblMensaje.Text = POST_MESSAGE;
                    lblMensaje.VCLType = TVCLType.BS;

                    var lblFecha = new TVcllabel(VclStackPanel.Column[2].This, "", TlabelType.H0);
                    lblFecha.This.style.fontSize = "15px";
                    lblFecha.This.style.fontWeight = "normal";
                    lblFecha.Text = SysCfg.DateTimeMethods.ToTimeString(MESSAGE_DATE);
                    lblFecha.VCLType = TVCLType.BS;


                    fechatemp = MESSAGE_DATE;

                    DS_SDOPERATION_POST.DataSet.Next();
                }

            }
        }
    }
    finally {
        Param.Destroy();
    }
    return new Array();









    //var _this = this.TParent();
    //var resp = "";
    //var parameters =
    //{
    //    IDSDWHOTOCASE: inIDSDWHOTOCASE,
    //    Param1: false
    //};
    //parameters = JSON.stringify(parameters);
    //$.ajax({
    //    type: "POST",
    //    url: SysCfg.App.Properties.xRaiz + 'Service/SD/Atention.svc/GETMsgPost2',
    //    data: parameters,
    //    contentType: "application/json; charset=utf-8",
    //    dataType: "json",
    //    async: false,
    //    success: function (response) {
    //        resp = response;
    //    },
    //    error: function (response) {
    //        SysCfg.Log.Methods.WriteLog("DynamicSTEFAtentionCase.js ItHelpCenter.SD.CaseUser.Atention.TSMConsoleUserSwitch.prototype.GETMsgPost Service/SD/Atention.svc/GETMsgPost2 " + "Error no llamo al servicio (GETMsgPost)");
    //    }
    //});
    //return resp;
}

ItHelpCenter.SD.CaseUser.Atention.TfrSDCaseAtention.prototype.GETMsgAten = function () {

    var _this = this.TParent();
    _this.DivAtntion.innerHTML = "";
    _this.DivAtntion.style.minHeight = "30px";
    _this.DivAtntion.style.marginTop = "7px";
    var Param1;
    var Param2;
    var Param3;

    if (true) {
        Param1 = false; Param2 = false; Param3 = false;
    }
    else {
        Param1 = true; Param2 = true; Param3 = true;
    }
    var Param = new SysCfg.Stream.Properties.TParam();

    try {
        Param.Inicialize();
        //*********** SDOPERATION_ATTENTION_GET ***************************** 
        Param.AddInt32(UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASE.FieldName, _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        if (Param1) Param.AddInt32(UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASEMT.FieldName, _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        else Param.AddUnknown(UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASEMT.FieldName, UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE + "." + UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASEMT.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        if (Param3) Param.AddInt32(UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDWHOTOCASE.FieldName, _this.SDWHOTOCASE.IDSDWHOTOCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        else Param.AddUnknown(UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDWHOTOCASE.FieldName, UsrCfg.InternoAtisNames.SDWHOTOCASE.NAME_TABLE + "." + UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDWHOTOCASE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);


        var DS_SDOPERATION_ATTENTION = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "GETSDOPERATION_ATTENTION", Param.ToBytes());

        if (DS_SDOPERATION_ATTENTION.ResErr.NotError) {
            if (DS_SDOPERATION_ATTENTION.DataSet.RecordCount > 0) {
                DS_SDOPERATION_ATTENTION.DataSet.First();

                var fechatemp = new Date();
                var ischangestep = false;

                while (!(DS_SDOPERATION_ATTENTION.DataSet.Eof)) {
                    ischangestep = false;
                    var IDCMDBCI = DS_SDOPERATION_ATTENTION.DataSet.RecordSet.FieldName("IDCMDBCI").asInt32();
                    var USERNAME = DS_SDOPERATION_ATTENTION.DataSet.RecordSet.FieldName("USERNAME").asString();
                    var ATTENTION_DATE = DS_SDOPERATION_ATTENTION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.ATTENTION_DATE.FieldName).asDateTime();
                    var ATTENTION_MESSAGE = DS_SDOPERATION_ATTENTION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.ATTENTION_MESSAGE.FieldName).asString();
                    var IDSDOPERATION_ATTENTION = DS_SDOPERATION_ATTENTION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.IDSDOPERATION_ATTENTION.FieldName).asInt32();

                    var IDSDTYPEUSER = UsrCfg.SD.Properties.TSDTypeUser.GetEnum(DS_SDOPERATION_ATTENTION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDTYPEUSER.FieldName).asInt32());
                    var IDSDWHOTOCASETYPE = UsrCfg.SD.Properties.TIDSDWHOTOCASETYPE.GetEnum(DS_SDOPERATION_ATTENTION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDWHOTOCASETYPE.FieldName).asInt32());
                    IDSDTYPEUSERNAME = IDSDTYPEUSER.name;
                    IDSDWHOTOCASETYPENAME = IDSDWHOTOCASETYPE.name;
                    CASEMT_SET_LS_NAMESTEP = DS_SDOPERATION_ATTENTION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.CASEMT_SET_LS_NAMESTEP.FieldName).asString();
                    CASEMT_SET_LS_STATUSN = DS_SDOPERATION_ATTENTION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.CASEMT_SET_LS_STATUSN.FieldName).asInt32();
                    LS_IDSDCASESTATUS = DS_SDOPERATION_ATTENTION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.LS_IDSDCASESTATUS.FieldName).asInt32();
                    IDSDOPERATION_ATTENTIONTYPE = DS_SDOPERATION_ATTENTION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.IDSDOPERATION_ATTENTIONTYPE.FieldName).asInt32();
                    var isempty = false;
                    if (IDSDOPERATION_ATTENTIONTYPE == 4) {
                        if (ATTENTION_MESSAGE == "")
                            isempty = true;
                        else {
                            CASEMT_SET_LS_NAMESTEP = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3") + " " + CASEMT_SET_LS_NAMESTEP;
                            ATTENTION_MESSAGE = CASEMT_SET_LS_NAMESTEP + "\n" + ATTENTION_MESSAGE;
                            ischangestep = true;
                        }

                    }
                    else if (IDSDOPERATION_ATTENTIONTYPE == 2) {
                        if (ATTENTION_MESSAGE == "")
                            isempty = true;
                        else {

                            CASEMT_SET_LS_NAMESTEP = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1") + " " + CASEMT_SET_LS_NAMESTEP;
                            ATTENTION_MESSAGE = CASEMT_SET_LS_NAMESTEP + "\n" + ATTENTION_MESSAGE;
                            ischangestep = true;
                        }
                    }
                    else {
                        if (ATTENTION_MESSAGE == "")
                            isempty = true;
                        else {
                            if (_this.SDWHOTOCASE.IDSDTYPEUSER != 4) {
                                CASEMT_SET_LS_NAMESTEP = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2") + " " + CASEMT_SET_LS_NAMESTEP;
                                ATTENTION_MESSAGE = CASEMT_SET_LS_NAMESTEP + "\n" + ATTENTION_MESSAGE;
                                ischangestep = true;
                            }
                            else {
                                isempty = true;
                            }
                        }
                    }


                    if (SysCfg.DateTimeMethods.DayOfYear(ATTENTION_DATE) != SysCfg.DateTimeMethods.DayOfYear(fechatemp)) {
                        if (!isempty) {
                            var VclStackPanel = new TVclStackPanel(_this.DivAtntion, "1", 2, [[12], [12], [12], [12], [12]]);
                            var lblFecha = new TVcllabel(VclStackPanel.Column[0].This, "", TlabelType.H0);
                            lblFecha.Text = SysCfg.DateTimeMethods.ToDateString(ATTENTION_DATE);
                            lblFecha.VCLType = TVCLType.BS;
                        }
                    }

                    if (!isempty) {
                        var VclStackPanel = new TVclStackPanel(_this.DivAtntion, "3", 3, [[3, 7, 2], [3, 7, 2], [3, 7, 2], [3, 7, 2], [3, 7, 2]]);
                        VclStackPanel.Row.This.style.marginBottom = "6px";


                        var VclStackPanel2 = new TVclStackPanel(VclStackPanel.Column[0].This, "2", 2, [[3, 9], [3, 9], [3, 9], [3, 9], [3, 9]]);

                        var imgImage = new TVclImagen(VclStackPanel2.Column[0].This, "");
                        imgImage.Src = "image/24/user-blue2.png";
                        imgImage.Title = USERNAME + "(" + IDSDWHOTOCASETYPENAME + " " + IDSDTYPEUSERNAME + ")" + " says:";
                        imgImage.This.style.marginTop = "-6px";

                        var lblUser = new TVcllabel(VclStackPanel2.Column[1].This, "", TlabelType.H0);
                        lblUser.This.style.marginLeft = "5px";
                        lblUser.This.style.fontSize = "13px";
                        lblUser.This.style.fontWeight = "normal";
                        lblUser.This.style.color = "rgb(128, 128, 128)";
                        lblUser.Text = USERNAME;
                        //lblUser.VCLType = TVCLType.BS;

                        var lblMensaje = new TVcllabel(VclStackPanel.Column[1].This, "", TlabelType.H0);

                        lblMensaje.This.style.fontSize = "15px";
                        lblMensaje.This.style.fontWeight = "normal";
                        lblMensaje.Text = ATTENTION_MESSAGE;
                        lblMensaje.VCLType = TVCLType.BS;

                        var lblFecha = new TVcllabel(VclStackPanel.Column[2].This, "", TlabelType.H0);
                        lblFecha.This.style.fontSize = "15px";
                        lblFecha.This.style.fontWeight = "normal";
                        lblFecha.Text = SysCfg.DateTimeMethods.ToTimeString(ATTENTION_DATE);
                        lblFecha.VCLType = TVCLType.BS;
                    }
                    fechatemp = ATTENTION_DATE;
                    //OutStr = ATTENTION_DATE.ToString("MM/dd/yy HH:mm") + " " + USERNAME + "(" + IDSDWHOTOCASETYPENAME + " " + IDSDTYPEUSERNAME + ") " + CASEMT_SET_LS_NAMESTEP + ":" + (Char)13 + (Char)10 + ATTENTION_MESSAGE + (Char)13 + (Char)10 + OutStr;
                    DS_SDOPERATION_ATTENTION.DataSet.Next();
                }
            }
        }
    }
    finally {
        Param.Destroy();
    }

}

ItHelpCenter.SD.CaseUser.Atention.TfrSDCaseAtention.prototype.GetUsersForSendMessage = function (Objeto, inIDSDWHOTOCASE, inIDSDCASE, inIDSDTYPEUSER) {
    var _this = this.TParent();
    var IsVisible = true;
    var parameters = {
        IDSDWHOTOCASE: inIDSDWHOTOCASE,
        IDSDCASE: inIDSDCASE,
        IDSDTYPEUSER: inIDSDTYPEUSER
    };
    parameters = JSON.stringify(parameters);
    $.ajax({
        type: "POST",
        url: SysCfg.App.Properties.xRaiz + 'Service/SD/Atention.svc/GetTSDWHOTOCASEList',
        data: parameters,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (response) {
            if (response) {


                //if (response.length == 1) {
                //    var user_uli = Uli(Objeto, "userli" + inIDSDWHOTOCASE);
                //    $(user_uli).attr("data-IdHC", inIDSDWHOTOCASE);
                //    var chek = new TVclInputcheckbox(user_uli, response[0].IDSDTYPEUSER);
                //    //chek.Checked = response[0].TYPEUSERNAME;
                //    chek.VCLType = TVCLType.BS;

                //    Vcllabel(user_uli, "lblChecked" + response[0].IDSDTYPEUSER, TVCLType.BS, TlabelType.H0, response[0].TYPEUSERNAME);
                //    //$(chek.This).attr('checked', 'checked');
                //    chek.Checked = true;
                //    IsVisible = false;
                //}
                //else {

                for (var i = 0; i < response.length; i++) {
                    var user_uli = Uli(Objeto, "userli" + inIDSDWHOTOCASE);
                    $(user_uli).attr("data-IdHC", inIDSDWHOTOCASE);
                    var chek1 = new TVclInputcheckbox(user_uli, response[i].IDSDTYPEUSER);
                    chek1.VCLType = TVCLType.BS;
                    if (inIDSDTYPEUSER == 4) chek1.Checked = true;
                    Vcllabel(user_uli, "lblChecked" + response[i].IDSDTYPEUSER, TVCLType.BS, TlabelType.H0, response[i].TYPEUSERNAME);
                }
                if (inIDSDTYPEUSER == 4) IsVisible = false;
                //}
            }
            else {
                alert('Expired permission');
            }
        },
        error: function (response) {
            SysCfg.Log.Methods.WriteLog("DynamicSTEFAtentionCase.js ItHelpCenter.SD.CaseUser.Atention.TSMConsoleUserSwitch.prototype.GetUsersForSendMessage Service/SD/Atention.svc/GetTSDWHOTOCASEList " + "Error no llamo al servicio");
        }
    });
    return IsVisible;
}