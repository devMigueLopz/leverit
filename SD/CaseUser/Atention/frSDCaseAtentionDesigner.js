﻿ItHelpCenter.SD.CaseUser.Atention.TfrSDCaseAtention.prototype.InitializeComponent = function () {
    var _this = this.TParent();

    //***************************** inicio nueva parte 3 ********************************************************
    var spContainer = new TVclStackPanel(_this.Object, "", 1, [[12], [12], [12], [12], [12]]);
    spContainer.Row.This.style.border = "1px solid black";
    spContainer.Row.This.style.backgroundColor = "#DDE6BD";

    var spHeadRow1 = new TVclStackPanel(spContainer.Column[0].This, "h1", 3, [[6, 6, 12], [6, 6, 12], [6, 6, 12], [2, 2, 8], [2, 2, 8]]);

    var spHeadRow1_1 = new TVclStackPanel(spHeadRow1.Column[0].This, "h1_1", 2, [[6, 6], [6, 6], [6, 6], [6, 6], [6, 6]]); // para la img ticket y nro ticket
    var spHeadRow1_2 = new TVclStackPanel(spHeadRow1.Column[0].This, "h1_2", 1, [[12], [12], [12], [12], [12]]); //para las estrellitas
    var spHeadRow1_3 = new TVclStackPanel(spHeadRow1.Column[0].This, "h1_3", 1, [[12], [12], [12], [12], [12]]); //para fecha de reporte
    var spHeadRow1_4 = new TVclStackPanel(spHeadRow1.Column[0].This, "h1_4", 1, [[12], [12], [12], [12], [12]]); //para el timer

    var spHeadRow2_1 = new TVclStackPanel(spHeadRow1.Column[1].This, "h2_1", 1, [[12], [12], [12], [12], [12]]); // para la img refrescar 
    var spHeadRow2_2 = new TVclStackPanel(spHeadRow1.Column[1].This, "h2_2", 1, [[12], [12], [12], [12], [12]]); //para el actual status
    var spHeadRow2_3 = new TVclStackPanel(spHeadRow1.Column[1].This, "h2_3", 1, [[12], [12], [12], [12], [12]]); //para el usuario del caso
    var spHeadRow2_4 = new TVclStackPanel(spHeadRow1.Column[1].This, "h2_4", 1, [[12], [12], [12], [12], [12]]); //para el atention 

    var spHeadRow3_1 = new TVclStackPanel(spHeadRow1.Column[2].This, "h3_1", 1, [[12], [12], [12], [12], [12]]); //para el titulo del caso
    var spHeadRow3_2 = new TVclStackPanel(spHeadRow1.Column[2].This, "h3_2", 1, [[12], [12], [12], [12], [12]]); //para la descripcion del caso
    var spHeadRow3_3 = new TVclStackPanel(spHeadRow1.Column[2].This, "h3_3", 1, [[12], [12], [12], [12], [12]]); //para el path
    //var spHeadRow3_4 = new TVclStackPanel(spHeadRow1.Column[2].This, "h3_4", 1, [[12], [12], [12], [12], [12]]); 

    spHeadRow1.Column[1].This.classList.add("pull-right");

    spHeadRow3_2.Column[0].This.style.maxHeight = "84px";
    spHeadRow3_2.Column[0].This.style.overflowY = 'scroll';

    spHeadRow1_1.Column[0].This.classList.add("text-center");
    spHeadRow1_1.Column[1].This.classList.add("text-center");
    spHeadRow2_1.Column[0].This.classList.add("text-right");
    //spHeadRow2_1.Column[1].This.classList.add("text-right");
    spHeadRow1_2.Column[0].This.classList.add("text-center");



    var DivModal = new TVclStackPanel(spContainer.Column[0].This, "modal", 1, [[12], [12], [12], [12], [12]]);
    _this.DivsModal = DivModal.Column[0].This
    $(_this.DivsModal).css("display", "none");

    //spHeadRow1_1.Column[0].This ---> para la img de ticket
    //spHeadRow1_1.Column[1].This ---> para el nro de ticket
    //spHeadRow1_2.Column[0].This ---> para las estrellitas
    //spHeadRow1_3.Column[0].This ---> para fecha de reporte
    //spHeadRow1_4.Column[0].This ---> para el timer

    //spHeadRow2_1.Column[0].This ---> para el boton refrescar
    //spHeadRow2_2.Column[0].This ---> para el Attached
    //spHeadRow2_3.Column[0].This ---> para el usuario del caso
    //spHeadRow2_4.Column[0].This ---> para el atention 

    //spHeadRow3_1.Column[0].This ---> para el titulo del caso
    //spHeadRow3_2.Column[0].This ---> para la descripcion del caso
    //spHeadRow3_3.Column[0].This ---> para el path

    spHeadRow1_1.Column[0].This.style.minHeight = "42px";
    spHeadRow1_1.Column[0].This.style.backgroundColor = "#808080";
    spHeadRow1_1.Column[1].This.style.backgroundColor = "#808080";


    this.btnRefreshAll = new TVclImagen(spHeadRow2_1.Column[0].This, "");
    _this.btnRefreshAll.This.style.marginTop = "8px";
    _this.btnRefreshAll.Src = "image/32/Rules.png";
    _this.btnRefreshAll.Title = "Refresh";
    _this.btnRefreshAll.Cursor = "pointer";

    _this.btnRefreshAll.onClick = function () {
        ItHelpCenter.SD.CaseUser.Atention.OutSMConsoleUserSwitch(_this.Object, ItHelpCenter.SD.CaseUser.Atention.TResultform.Refresh, _this.IDSOURCE, _this.CallbackModalResult);
        //_this.btnRefreshAll_onClick(_this, _this.btnRefreshAll);
    };

    _this.img_case = new TVclImagen(spHeadRow1_1.Column[0].This, "");
    _this.img_case.Src = "image/24/CaseGreen.png";
    _this.img_case.Title = "";
    _this.img_case.This.style.marginTop = "8px";

    _this.lblCaseNumber = new TVcllabel(spHeadRow1_1.Column[1].This, "", TlabelType.H4);
    _this.lblCaseNumber.Text = "0000";
    _this.lblCaseNumber.This.style.fontSize = "20px";
    _this.lblCaseNumber.This.style.fontWeight = "700";
    _this.lblCaseNumber.This.style.color = "white";

    _this.Lbl_CASE_TITLE = new TVcllabel(spHeadRow3_1.Column[0].This, "", TlabelType.H4);
    _this.Lbl_CASE_TITLE.Text = "...";
    _this.Lbl_CASE_TITLE.This.style.fontSize = "20px";
    _this.Lbl_CASE_TITLE.This.style.fontWeight = "700";
    _this.Lbl_CASE_TITLE.This.style.color = "#0094FF";

    _this.VclFechaInforme = new TVcllabel(spHeadRow1_3.Column[0].This, "", TlabelType.H4);
    _this.VclFechaInforme.Text = "Actual Status: ";
    _this.VclFechaInforme.This.style.fontSize = "15px";
    _this.VclFechaInforme.This.style.fontWeight = "700";
    _this.VclFechaInforme.This.classList.add("text-center");

    _this.VclDescripcion = new TVcllabel(spHeadRow3_2.Column[0].This, "", TlabelType.H4);
    _this.VclDescripcion.Text = "...";
    _this.VclDescripcion.This.style.fontSize = "15px";

    //DivStar
    _this.DivStar = spHeadRow1_2.Column[0].This

    //DivTitle1
    //_this.DivTitle = spHeadR1.Column[6].This;
    //_this.DivTitle = spHeadRow2.Column[2].This;  

    //////////////////////////////////  ATTACHED  ////////////////////////////////////   
    try {
        spHeadRow2_2.Column[0].This.classList.add("text-right");

        _this.lueAttached = new UVCLRepositoryLookUp(spHeadRow2_2.Column[0].This, "Repositoriolook2" + _this.SDWHOTOCASE.IDSDWHOTOCASE, UsrCfg.Traslate.GetLangText(_this.Mythis, "lb_Attached"), true, true, function () {
            if (_this.lueAttached.Active) {
                _this.TfrAttached = new ItHelpCenter.SD.Shared.Attached.UfrSDAttached(_this.lueAttached.DivRootContainer.Child[0].Child[1].Child[0].This, "Attached",
                    function () { },
                    _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE,
                    _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDUSER,
                    _this.SDWHOTOCASE.IDSDTYPEUSER,
                    _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.MT_IDMDSERVICETYPE,
                    false,
                    _this.IDSDTypeUserListAll
                );
            }
        });
        _this.lueAttached.Uulli.ul.style.listStyle = "none";
        _this.lueAttached.Uulli.ul.style.marginTop = "10px";
        _this.lueAttached.Uulli.ul.style.marginBottom = "0px";
        _this.lueAttached.Caption.style.textDecoration = "none";
        _this.lueAttached.Caption.style.cursor = "pointer";
        _this.lueAttached.Caption.style.fontWeight = "700";
        _this.lueAttached.Caption.style.color = "#0094FF";

        //_this.TfrAttached[indx] = new ItHelpCenter.SD.Shared.Attached.UfrSDAttached(_this.lueAttached.DivRootContainer.Child[0].Child[1].Child[0].This, "Attached",
        //    function () {

        //    },
        //    response.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE,                        
        //    response.SDCONSOLEATENTION.SDCASECOMPLTE.IDUSER,                        
        //    response.SDWHOTOCASE.IDSDTYPEUSER, new Array()
        //); 
        //indx = indx + 1;

        BootStrapCreateRow(_this.lueAttached.DivRootContainer.This);
        _this.lueAttached.DivRootContainer.This.classList.add("message-content");
        _this.lueAttached.DivRootContainer.This.style.display = "none";
        BootStrapCreateColumn(_this.lueAttached.DivRootContainer.Child[0].This, [12, 12, 12, 12, 12]);
        BootStrapCreateRows(_this.lueAttached.DivRootContainer.Child[0].Child);
        BootStrapCreateColumns(_this.lueAttached.DivRootContainer.Child[0].Child[0].Child, [[5, 7], [5, 7], [5, 7], [5, 7], [5, 7]]);

        _this.user_uul = Uul(_this.lueAttached.DivRootContainer.Child[0].Child[0].Child[1].This, "");
        _this.user_uul.style.listStyle = "none";
        var IsVisible = _this.GetUsersForSendMessage(_this.user_uul, _this.SDWHOTOCASE.IDSDWHOTOCASE, _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE, _this.SDTYPEUSER.IDSDTYPEUSER)
        if (IsVisible) $(_this.lueAttached.DivRootContainer.Child[0].Child[0].This).show();
        else $(_this.lueAttached.DivRootContainer.Child[0].Child[0].This).hide();
        BootStrapCreateColumns(_this.lueAttached.DivRootContainer.Child[0].Child[1].Child, [[12], [12], [12], [12], [12]]);

        _this.lueAttached.DivRootContainer.Child[0].Child[1].Child[0].This.classList.add("text-left");

        BootStrapCreateColumns(_this.lueAttached.DivRootContainer.Child[0].Child[2].Child, [[6, 6], [6, 6], [6, 6], [6, 6], [6, 6]]);
        $(_this.lueAttached.DivRootContainer.Child[0].Child[2].Child[0].This).css("text-align", "center");
        $(_this.lueAttached.DivRootContainer.Child[0].Child[2].Child[1].This).css("text-align", "center");


        $(_this.lueAttached.DivRootContainer.This).css("width", "400%");

        $(_this.lueAttached.ButtonCancelCaption.This).css("display", "none");

        _this.lueAttached.ButtonOkCaption.This.classList.add("jmbtn");
        _this.lueAttached.ButtonOkCaption.This.style.width = "50%";
        _this.lueAttached.ButtonOkCaption.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lueAttached.ButtonOkCaption.Text", "Accept");
        _this.lueAttached.ButtonOkCaption.onClick = function () {
            _this.lueAttached.Caption.click();
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.CaseUser.Atention.TfrSDCaseAtention Attached", e);
    }

    //***************************** fin nueva parte 3 ********************************************************


    ////***************************** inicio nueva parte 2 ********************************************************
    //var spContainer = new TVclStackPanel(_this.Object, "", 1, [[12], [12], [12], [12], [12]]);
    //spContainer.Row.This.style.border = "1px solid black";
    //spContainer.Row.This.style.backgroundColor = "#DDE6BD";
    ////spContainer.Row.This.style.marginLeft = "8px";
    ////spContainer.Row.This.style.margin = "15px 20px 10px 10px";

    ////var spHeadRow1 = new TVclStackPanel(spContainer.Column[0].This, "h1", 3, [[12, 12, 12], [2, 3, 7], [1, 2, 9], [1, 2, 9], [1, 2, 9]]);
    //var spHeadRow1 = new TVclStackPanel(spContainer.Column[0].This, "h1", 4, [[12, 12, 12, 12], [2, 3, 0, 7], [1, 2, 0, 9], [1, 2, 0, 9], [1, 2, 0, 9]]);
    ////var spHeadRow1_1 = new TVclStackPanel(spHeadRow1.Column[0].This, "h1_1", 2, [[6, 6], [6, 6], [6, 6], [6, 6], [6, 6]]);
    //var spHeadRow1_2 = new TVclStackPanel(spHeadRow1.Column[1].This, "h1_2", 2, [[6, 6], [6, 6], [6, 6], [6, 6], [6, 6]]);

    //var spHeadRow2 = new TVclStackPanel(spContainer.Column[0].This, "h2", 3, [[12, 12, 12], [3, 3, 6], [2, 2, 8], [2, 2, 8], [2, 2, 8]]);
    //var spHeadRow2_1_1 = new TVclStackPanel(spHeadRow2.Column[0].This, "h2_1_1", 1, [[12], [12], [12], [12], [12]]);
    //var spHeadRow2_1_2 = new TVclStackPanel(spHeadRow2.Column[0].This, "h2_1_2", 1, [[12], [12], [12], [12], [12]]);

    //var spHeadRow2_2_1 = new TVclStackPanel(spHeadRow2.Column[1].This, "h2_2_1", 1, [[12], [12], [12], [12], [12]]);
    //var spHeadRow2_2_2 = new TVclStackPanel(spHeadRow2.Column[1].This, "h2_2_1", 1, [[12], [12], [12], [12], [12]]);

    //var spHeadRow3 = new TVclStackPanel(spContainer.Column[0].This, "h3", 3, [[12, 12, 12], [3, 3, 6], [2, 2, 8], [2, 2, 8], [2, 2, 8]]);

    //spHeadRow1.Column[2].This.classList.add("hidden-lg");
    //spHeadRow1.Column[2].This.classList.add("hidden-md");
    //spHeadRow1.Column[2].This.classList.add("hidden-sm");
    //spHeadRow1.Column[2].This.classList.add("visible-xs");
    //spHeadRow1.Column[2].This.classList.add("text-center");

    //spHeadRow2_1_1.Column[0].This.classList.add("visible-lg");
    //spHeadRow2_1_1.Column[0].This.classList.add("visible-md");
    //spHeadRow2_1_1.Column[0].This.classList.add("visible-sm");
    //spHeadRow2_1_1.Column[0].This.classList.add("hidden-xs");

    //spHeadRow1.Column[0].This.classList.add("pull-right");
    //spHeadRow2.Column[0].This.classList.add("pull-left");
    //spHeadRow2.Column[1].This.classList.add("pull-right");
    //spHeadRow3.Column[0].This.classList.add("pull-left");
    //spHeadRow3.Column[1].This.classList.add("pull-right");
    //spHeadRow1.Column[0].This.classList.add("text-right");

   

    //spHeadRow2_1_1.Column[0].This.classList.add("text-center");
    //spHeadRow1_2.Column[0].This.classList.add("text-center");
    //spHeadRow1_2.Column[1].This.classList.add("text-center");

    //spHeadRow2.Column[2].This.style.maxHeight = "84px";
    //spHeadRow2.Column[2].This.style.overflowY = 'scroll';

    //var DivModal = new TVclStackPanel(spContainer.Column[0].This, "modal", 1, [[12], [12], [12], [12], [12]]);
    //_this.DivsModal = DivModal.Column[0].This
    //$(_this.DivsModal).css("display", "none");

    ////spHeadRow1.Column[2].This   ---> para el titulo del caso
    ////spHeadRow1.Column[0].This ---> para el boton refrescar
    ////spHeadRow1_2.Column[0].This ---> para la img de ticket
    ////spHeadRow1_2.Column[1].This ---> para el nro de ticket

    ////spHeadRow2.Column[2].This   ---> para la descripcion del caso
    ////spHeadRow2_1_1.Column[0].This ---> para las estrellitas
    ////spHeadRow1.Column[]2.This     ---> para las estrellitas
    ////spHeadRow2_1_2.Column[0].This ---> para fecha de reporte
    ////spHeadRow2_2_1.Column[0].This ---> para el actual status
    ////spHeadRow2_2_2.Column[0].This ---> para el usuario del caso

    ////spHeadRow3.Column[0].This   ---> para el timer
    ////spHeadRow3.Column[1].This   ---> para el atention
    ////spHeadRow3.Column[2].This   ---> para el path

    //spHeadRow1_2.Column[0].This.style.minHeight = "42px";
    //spHeadRow1_2.Column[0].This.style.backgroundColor = "#808080";
    //spHeadRow1_2.Column[1].This.style.backgroundColor = "#808080";

    //this.btnRefreshAll = new TVclImagen(spHeadRow1.Column[0].This, "");
    //_this.btnRefreshAll.This.style.marginTop = "8px";
    //_this.btnRefreshAll.Src = "image/32/Rules.png";
    //_this.btnRefreshAll.Title = "Refresh";
    //_this.btnRefreshAll.Cursor = "pointer";

    //_this.btnRefreshAll.onClick = function () {
    //    ItHelpCenter.SD.CaseUser.Atention.OutSMConsoleUserSwitch(_this.Object, ItHelpCenter.SD.CaseUser.Atention.TResultform.Refresh, _this.IDSOURCE, _this.CallbackModalResult);
    //    //_this.btnRefreshAll_onClick(_this, _this.btnRefreshAll);
    //};

    //_this.img_case = new TVclImagen(spHeadRow1_2.Column[0].This, "");
    //_this.img_case.Src = "image/24/CaseGreen.png";
    //_this.img_case.Title = "";
    //_this.img_case.This.style.marginTop = "8px";

    //_this.lblCaseNumber = new TVcllabel(spHeadRow1_2.Column[1].This, "", TlabelType.H4);
    //_this.lblCaseNumber.Text = "0000";
    //_this.lblCaseNumber.This.style.fontSize = "20px";
    //_this.lblCaseNumber.This.style.fontWeight = "700";
    //_this.lblCaseNumber.This.style.color = "white";

    //_this.Lbl_CASE_TITLE = new TVcllabel(spHeadRow1.Column[3].This, "", TlabelType.H4);
    //_this.Lbl_CASE_TITLE.Text = "...";
    //_this.Lbl_CASE_TITLE.This.style.fontSize = "20px";
    //_this.Lbl_CASE_TITLE.This.style.fontWeight = "700";
    //_this.Lbl_CASE_TITLE.This.style.color = "#0094FF";

    //_this.VclFechaInforme = new TVcllabel(spHeadRow2_1_2.Column[0].This, "", TlabelType.H4);
    //_this.VclFechaInforme.Text = "Actual Status: ";
    //_this.VclFechaInforme.This.style.fontSize = "15px";
    //_this.VclFechaInforme.This.style.fontWeight = "700";
    //_this.VclFechaInforme.This.classList.add("text-center");

    //_this.VclDescripcion = new TVcllabel(spHeadRow2.Column[2].This, "", TlabelType.H4);
    //_this.VclDescripcion.Text = "...";
    //_this.VclDescripcion.This.style.fontSize = "15px";

    ////DivStar
    //_this.DivStar = spHeadRow2_1_1.Column[0].This
    //_this.DivStar2 = spHeadRow1.Column[2].This;

    ////DivTitle1
    ////_this.DivTitle = spHeadR1.Column[6].This;
    ////_this.DivTitle = spHeadRow2.Column[2].This;  

    ////////////////////////////////////  ATTACHED  ////////////////////////////////////   
    //try {
    //    spHeadRow2_2_2.Column[0].This.classList.add("text-right");

    //    _this.lueAttached = new UVCLRepositoryLookUp(spHeadRow2_2_2.Column[0].This, "Repositoriolook2" + _this.SDWHOTOCASE.IDSDWHOTOCASE, UsrCfg.Traslate.GetLangText(_this.Mythis, "lb_Attached"), true, true, function () {
    //        if (_this.lueAttached.Active) {
    //            _this.TfrAttached = new ItHelpCenter.SD.Shared.Attached.UfrSDAttached(_this.lueAttached.DivRootContainer.Child[0].Child[1].Child[0].This, "Attached",
    //                function () { },
    //                _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE,
    //                _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDUSER,
    //                _this.SDWHOTOCASE.IDSDTYPEUSER,
    //                _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.MT_IDMDSERVICETYPE,
    //                false,
    //                _this.IDSDTypeUserListAll
    //            );
    //        }
    //    });
    //    _this.lueAttached.Uulli.ul.style.listStyle = "none";
    //    _this.lueAttached.Uulli.ul.style.marginTop = "10px";
    //    _this.lueAttached.Uulli.ul.style.marginBottom = "0px";
    //    _this.lueAttached.Caption.style.textDecoration = "none";
    //    _this.lueAttached.Caption.style.cursor = "pointer";
    //    _this.lueAttached.Caption.style.fontWeight = "700";
    //    _this.lueAttached.Caption.style.color = "#0094FF";

    //    //_this.TfrAttached[indx] = new ItHelpCenter.SD.Shared.Attached.UfrSDAttached(_this.lueAttached.DivRootContainer.Child[0].Child[1].Child[0].This, "Attached",
    //    //    function () {

    //    //    },
    //    //    response.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE,                        
    //    //    response.SDCONSOLEATENTION.SDCASECOMPLTE.IDUSER,                        
    //    //    response.SDWHOTOCASE.IDSDTYPEUSER, new Array()
    //    //); 
    //    //indx = indx + 1;

    //    BootStrapCreateRow(_this.lueAttached.DivRootContainer.This);
    //    _this.lueAttached.DivRootContainer.This.classList.add("message-content");
    //    _this.lueAttached.DivRootContainer.This.style.display = "none";
    //    BootStrapCreateColumn(_this.lueAttached.DivRootContainer.Child[0].This, [12, 12, 12, 12, 12]);
    //    BootStrapCreateRows(_this.lueAttached.DivRootContainer.Child[0].Child);
    //    BootStrapCreateColumns(_this.lueAttached.DivRootContainer.Child[0].Child[0].Child, [[5, 7], [5, 7], [5, 7], [5, 7], [5, 7]]);

    //    _this.user_uul = Uul(_this.lueAttached.DivRootContainer.Child[0].Child[0].Child[1].This, "");
    //    _this.user_uul.style.listStyle = "none";
    //    var IsVisible = _this.GetUsersForSendMessage(_this.user_uul, _this.SDWHOTOCASE.IDSDWHOTOCASE, _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE, _this.SDTYPEUSER.IDSDTYPEUSER)
    //    if (IsVisible) $(_this.lueAttached.DivRootContainer.Child[0].Child[0].This).show();
    //    else $(_this.lueAttached.DivRootContainer.Child[0].Child[0].This).hide();
    //    BootStrapCreateColumns(_this.lueAttached.DivRootContainer.Child[0].Child[1].Child, [[12], [12], [12], [12], [12]]);

    //    _this.lueAttached.DivRootContainer.Child[0].Child[1].Child[0].This.classList.add("text-left");

    //    BootStrapCreateColumns(_this.lueAttached.DivRootContainer.Child[0].Child[2].Child, [[6, 6], [6, 6], [6, 6], [6, 6], [6, 6]]);
    //    $(_this.lueAttached.DivRootContainer.Child[0].Child[2].Child[0].This).css("text-align", "center");
    //    $(_this.lueAttached.DivRootContainer.Child[0].Child[2].Child[1].This).css("text-align", "center");


    //    $(_this.lueAttached.DivRootContainer.This).css("width", "400%");
      
    //    $(_this.lueAttached.ButtonCancelCaption.This).css("display", "none");

    //    _this.lueAttached.ButtonOkCaption.This.classList.add("jmbtn");
    //    _this.lueAttached.ButtonOkCaption.This.style.width = "50%";
    //    _this.lueAttached.ButtonOkCaption.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lueAttached.ButtonOkCaption.Text", "Accept");
    //    _this.lueAttached.ButtonOkCaption.onClick = function () {            
    //         _this.lueAttached.Caption.click();
    //    }
    //} catch (e) {
    //    SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.CaseUser.Atention.TfrSDCaseAtention Attached", e);
    //}
    ////******************************* fin nueva parte 2 ***********************************************

    //stack panel atenciones y mensajes
    var spAtentionMsg = new TVclStackPanel(spContainer.Column[0].This, "am", 1, [[12], [12], [12], [12], [12]]);
    spAtentionMsg.Row.This.style.backgroundColor = "white";

    var spAtentionMsg1 = new TVclStackPanel(spAtentionMsg.Column[0].This, "am1", 2, [[12, 12], [12, 12], [12, 12], [6, 6], [6, 6]]);
    // spAtentionMsg1.Row.This.classList.add("row-eq-height");
    spAtentionMsg1.Row.This.style.borderTop = "1px solid black";
    spAtentionMsg1.Column[0].This.style.borderRight = "1px solid black";


    //stackpanel atenciones    
    var spAtentionMsg1_1 = new TVclStackPanel(spAtentionMsg1.Column[0].This, "am1_1", 3, [[12], [12], [12], [12], [12]]);
    spAtentionMsg1_1.Row.This.style.backgroundColor = "#EEF2DE";
    spAtentionMsg1_1.Row.This.style.borderBottom = "1px solid black";

    _this.VclMsgAtention = new TVcllabel(spAtentionMsg1_1.Column[0].This, "", TlabelType.H4);
    _this.VclMsgAtention.This.style.fontWeight = "700";
    _this.VclMsgAtention.This.style.fontSize = "15px";
    _this.VclMsgAtention.This.style.marginTop = "10px";
    _this.VclMsgAtention.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "deta3_uh4");

    _this.spMensajesAten = new TVclStackPanel(spAtentionMsg1.Column[0].This, "am1_2", 1, [[12], [12], [12], [12], [12]]);
    _this.spMensajesAten.Row.This.style.minHeight = "30px";
    _this.DivAtntion = _this.spMensajesAten.Column[0].This;

    //stackpanel mensajes
    var spAtentionMsg2_1 = new TVclStackPanel(spAtentionMsg1.Column[1].This, "am2_1", 3, [[6, 6], [6, 6], [6, 6], [7, 5], [7, 5]]);
    spAtentionMsg2_1.Row.This.style.backgroundColor = "#EEF2DE";
    spAtentionMsg2_1.Row.This.style.borderBottom = "1px solid black";

    _this.deta2_uh4 = new TVcllabel(spAtentionMsg2_1.Column[0].This, "", TlabelType.H4);
    _this.deta2_uh4.This.style.fontWeight = "700";
    _this.deta2_uh4.This.style.fontSize = "15px";
    _this.deta2_uh4.This.style.marginTop = "10px";
    _this.deta2_uh4.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "deta2_uh4");

    _this.spMensajes = new TVclStackPanel(spAtentionMsg1.Column[1].This, "am1_2", 1, [[12], [12], [12], [12], [12]]);
    _this.spMensajes.Row.This.style.minHeight = "30px";
    _this.DivMsg = _this.spMensajes.Column[0].This;

    ///////////////////////////////// MESSAGES ////////////////////////////////////////////
    try {
        spAtentionMsg2_1.Column[1].This.style.marginTop = "-4px";
        spAtentionMsg2_1.Column[1].This.classList.add("text-right");

        _this.lueMessage = UVCLRepositoryLookUp(spAtentionMsg2_1.Column[1].This, "Repositoriolook1" + _this.SDWHOTOCASE.IDSDWHOTOCASE, UsrCfg.Traslate.GetLangText(_this.Mythis, "lueMessage"), true, true, null);
        _this.lueMessage.Uulli.ul.style.listStyle = "none";
        _this.lueMessage.Uulli.ul.style.marginTop = "10px";
        _this.lueMessage.Uulli.ul.style.marginBottom = "0px";
        _this.lueMessage.Caption.style.textDecoration = "none";
        _this.lueMessage.Caption.style.cursor = "pointer";
        _this.lueMessage.Caption.style.fontWeight = "700";
        _this.lueMessage.Caption.style.color = "#0094FF";

        BootStrapCreateRow(_this.lueMessage.DivRootContainer.This);
        _this.lueMessage.DivRootContainer.This.classList.add("message-content");
        _this.lueMessage.DivRootContainer.This.style.display = "none";
        BootStrapCreateColumn(_this.lueMessage.DivRootContainer.Child[0].This, [12, 12, 12, 12, 12]);
        BootStrapCreateRows(_this.lueMessage.DivRootContainer.Child[0].Child);
        BootStrapCreateColumns(_this.lueMessage.DivRootContainer.Child[0].Child[0].Child, [[5, 7], [5, 7], [5, 7], [5, 7], [5, 7]]);
        Vcllabel(_this.lueMessage.DivRootContainer.Child[0].Child[0].Child[0].This, "", TVCLType.BS, TlabelType.H4, UsrCfg.Traslate.GetLangText(_this.Mythis, "Uh4_Users"));
        var user_uul = Uul(_this.lueMessage.DivRootContainer.Child[0].Child[0].Child[1].This, "");
        user_uul.style.listStyle = "none";
        var IsVisible = _this.GetUsersForSendMessage(user_uul, _this.SDWHOTOCASE.IDSDWHOTOCASE, _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE, _this.SDTYPEUSER.IDSDTYPEUSER)
        if (IsVisible) $(_this.lueMessage.DivRootContainer.Child[0].Child[0].This).show();
        else $(_this.lueMessage.DivRootContainer.Child[0].Child[0].This).hide();
        _this.lueMessage.DivRootContainer.Child[0].Child[0].Child[1].This.classList.add("text-left");
        BootStrapCreateColumns(_this.lueMessage.DivRootContainer.Child[0].Child[1].Child, [[12], [12], [12], [12], [12]]);
        Vcllabel(_this.lueMessage.DivRootContainer.Child[0].Child[1].Child[0].This, "", TVCLType.BS, TlabelType.H4, UsrCfg.Traslate.GetLangText(_this.Mythis, "Uh4_Message"));

        _this.lueMessage.DivRootContainer.Child[0].Child[1].Child[0].This.classList.add("text-left");
        var mensaje_utexarea = new TVclMemo(_this.lueMessage.DivRootContainer.Child[0].Child[1].Child[0].This, "TxtMessage" + _this.SDWHOTOCASE.IDSDWHOTOCASE);
        mensaje_utexarea.Text = "";
        mensaje_utexarea.VCLType = TVCLType.BS;
        mensaje_utexarea.Rows = 10;
        mensaje_utexarea.Cols = 50;
        mensaje_utexarea.This.addEventListener("keypress", function (e) {
            tecla = (document.all) ? e.keyCode : e.which;
            if (tecla == 13) {
                _this.lueMessage.ButtonOkCaption.Click();
            }
        }, false);
        BootStrapCreateColumns(_this.lueMessage.DivRootContainer.Child[0].Child[2].Child, [[6, 6], [6, 6], [6, 6], [6, 6], [6, 6]]);
        _this.lueMessage.ButtonCancelCaption.This.classList.add("jmbtn");

        _this.lueMessage.ButtonCancelCaption.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ButtonCancelCaption");
        _this.lueMessage.ButtonCancelCaption.onClick = function () {
            _this.lueMessage.Caption.click();
        }
        _this.lueMessage.ButtonOkCaption.This.classList.add("jmbtn");
        _this.lueMessage.ButtonOkCaption.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ButtonOkCaption");
        _this.lueMessage.ButtonOkCaption.onClick = function () {
            var listItem = user_uul.getElementsByTagName("li");
            var listausuarios = new Array();
            var idHC = 0;
            for (var i = 0; i < listItem.length; i++) {

                idHC = listItem[i].getAttribute("data-idHC");
                if (listItem[i].getElementsByTagName("input")[0].checked) {
                    listausuarios.push(listItem[i].getElementsByTagName("input")[0].id);
                }
            }
            var parameters = {
                IDSDWHOTOCASE: idHC,
                IDSDTYPEUSERListOUT: listausuarios,
                Msg: mensaje_utexarea.Text
            };
            parameters = JSON.stringify(parameters);
            if (parameters != "") {
                $.ajax({
                    type: "POST",
                    url: SysCfg.App.Properties.xRaiz + 'Service/SD/Atention.svc/SaveMessage',
                    data: parameters,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                    success: function (response) {
                        mensaje_utexarea.Text = "";
                        _this.lueMessage.Caption.click();
                        var mensajes = _this.GETMsgPost(_this.SDWHOTOCASE.IDSDWHOTOCASE);
                    },
                    error: function (response) {
                        SysCfg.Log.Methods.WriteLog("DynamicSTEFAtentionCase.js ItHelpCenter.SD.CaseUser.Atention.TSMConsoleUserSwitch.prototype.loadPendingcases Service/SD/Atention.svc/SaveMessage " + "Error no llamo al servicio");
                    }
                });
            }
            else { $('#error_message').modal('show'); }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.CaseUser.Atention.TfrSDCaseAtention Message", e);
    }

    //Estado actual
    var spStatusActual = new TVclStackPanel(spContainer.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
    spStatusActual.Row.This.style.backgroundColor = "#EEF2DE";
    spStatusActual.Row.This.style.borderTop = "1px solid black";
    spStatusActual.Row.This.style.borderBottom = "1px solid black";

    _this.esta1_uh4 = new TVcllabel(spStatusActual.Column[0].This, "", TlabelType.H4);
    _this.esta1_uh4.This.style.fontWeight = "700";
    _this.esta1_uh4.This.style.fontSize = "15px";
    _this.esta1_uh4.This.style.marginTop = "10px";
    _this.esta1_uh4.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "esta1_uh4");

    //array grabar
    var spArrayButton = new TVclStackPanel(spContainer.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
    spArrayButton.Row.This.style.borderTop = "1px solid black";
    _this.DivGrabarETArray = spArrayButton.Column[0].This;

    //Bloque de botones para cambiar de step
    var spChangedSteep = new TVclStackPanel(spContainer.Column[0].This, "", 2, [[12, 12], [7, 5], [8, 4], [9, 3], [9, 3]]);
    spChangedSteep.Row.This.style.borderBottom = "1px solid black";
    spChangedSteep.Row.This.style.backgroundColor = "#DDE6BD";

    _this.DivChangedSteep0 = spChangedSteep.Column[0].This;
    _this.DivChangedSteep1 = spChangedSteep.Column[1].This;


}