﻿//************* Funciones Generales ***************
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention = function (inObject, incallbackModalResult, inSDConsoleAtentionStart, inMenuObject) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.Object = inObject;
    this.IsMenu = (inMenuObject != null);

    this.TimerTemp = new TVclTimer();


    this.CallbackModalResult = incallbackModalResult;//CallbackModalResult(OutStr);        
    this.MenuObject = inMenuObject
    this.Mythis = "TfrSDCaseAtention";
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1", "Scale next level ");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2", "Change status ");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3", " Functional");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines4", " Hierarchic");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines5", "Atis:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines6", "Enter message");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines7", "You can only edit a model in status:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines8", "You cannot start an activity without a model.");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines9", "You can only start a model in the Status:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines10", "add parent relation");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines11", "delete parent relation");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines12", "New status ");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines13", "Case update");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines14", "The model has been successfully changed.");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines15", "There are no more users to scale");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines16a", "Steps summary:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines17a", "Type a description of the actions executed in this step:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines17b", "Recommendation for new step:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines18", "Step Detail ");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines19", "General Guide ");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines20", "Details");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines21", "Return");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines22", "End Message");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines23", "Change Status (Current Status:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines24", "New Status");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines25", "Change Status Description");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines26", "Relation");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines27", "Relation Delete");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines28", "Close");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines29", "You must cancel or terminate outstanding activities with the case before relating the current case ");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines30", "Search by Relation");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines31", "Relation Add");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines32", "Maximum time: ");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines33", "Priority: ");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines34", "View Case ");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines36", "Step");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines37", "Title");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines38", "Status Activity");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines39", "Return");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines40", "Type");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines41", "Order");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines42", "Window with attentions.");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines43", "See all the attention.");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines44", "Work-Arounds no found");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1_1", "Comment");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1_2", "Caution");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1_3", "Warning");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1_4", "Change to the new Step:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines45", "Attached");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "GroupBarTimer_Maxtime.Text", "Time remaining ");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "GroupBarTimer_MaxtimeDate.Text", "Maximo Estimated ");

    UsrCfg.Traslate.GetLangText(_this.Mythis, "lb_NotificationMetod", "Notification Method:"),
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Z2_label2", "CI Affected"),
    UsrCfg.Traslate.GetLangText(_this.Mythis, "fecha_uh4", "Case Date: "),
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btn_Msg_ViewPopup", "Message: "),
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Uh4_Users", "Users:"),
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Uh4_Message", "Send message to the manager to manage your case:"),
    UsrCfg.Traslate.GetLangText(_this.Mythis, "deta1_uh4", "Detail:"),
    UsrCfg.Traslate.GetLangText(_this.Mythis, "deta2_uh4", "Add Messages:"),
    //UsrCfg.Traslate.GetLangText(_this.Mythis, "deta2cont_uh4", "No messages found"),
    UsrCfg.Traslate.GetLangText(_this.Mythis, "esta1_uh4", "Actual Status: "),
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Step1_uh4", "Actual Step: "),
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Label_LSEND_COMMENTSENABLE", "Write a brief summary:"),
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Label_LSBEGIN_COMMENTSENABLE", "Type a suggestion:"),
    //UsrCfg.Traslate.GetLangText(_this.Mythis, "btnNuevoET", "New"),
    //UsrCfg.Traslate.GetLangText(_this.Mythis, "btnGrabarET", "Save"),
    //UsrCfg.Traslate.GetLangText(_this.Mythis, "btnEliminarET", "Delete"),
    //UsrCfg.Traslate.GetLangText(_this.Mythis, "btnCancelarET", "Cancel"),
    UsrCfg.Traslate.GetLangText(_this.Mythis, "ButtonOkCaption", "Send"),
    UsrCfg.Traslate.GetLangText(_this.Mythis, "ButtonCancelCaption", "Cancel");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "ModalHeaderWorkAround", "Work Around");


    UsrCfg.Traslate.GetLangText(_this.Mythis, "TabPag1.Caption", "Home");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lblMaxTime.Title", "Timer Console");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "NavBarTimer_Information.Text", "Information");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lblCategoryGroup.Title", "Information");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnDetail.ToolTip", "Parent activity");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnDetail.Text", "Case");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnParentDetail.ToolTip", "Parent activity");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnParentDetail.Text", "Parent");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Btn_CATEGORYChange.ToolTip", "Change category priority and model.");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Btn_CATEGORYChange.Text", "Category");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnWorkArrown.ToolTip", "Work-Around");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnWorkArrown.Text", "Work-Around");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnUser.ToolTip", "User Contact");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnUser.Text", "User");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lblStatusScaleGroup.Title", "Status Scale");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnFnEscFun.ToolTip", "Scale functional");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnFnEscFun.Text", "Functional");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnFnEscHier.ToolTip", "Scale hierarchically");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnFnEscHier.Text", "Hierarchical");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnFnChangeStd.ToolTip", "Change status");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnFnChangeStd.Text", "Status");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lbllinkGroup.Title", "Link");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnCIAfectado.ToolTip", "CI Affected");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnCIAfectado.Text", "CI");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnAdjuntos.ToolTip", "Attachments");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnAdjuntos.Text", "Attach File");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnRelatedCase.ToolTip", "Related Cases.");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnRelatedCase.Text", "Related cases");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lblParentCaseGroup.Title", "Admin Equal Cases");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnFnViewRelation.ToolTip", "View Relation");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnFnViewRelation.Text", "View");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnADDRealtion.ToolTip", "Add Parent Relation");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnADDRealtion.Text", "Add");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnDELRealtions.ToolTip", "Children Relation");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnDELRealtions.Text", "Delete");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "VclMsgAtention.Text", "Attention: ");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "AtentionViewAll.Text", "View all");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Modal.AddHeaderContent", "Attention Detail Graphic 02");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines46", "Recommendation for");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines47", "Attention for");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines48", "Step Summary of");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines49", "External");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines50", "Internal");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines51", "User:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines52", "Time Remain:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines53", "Priority:");



    this.SDConsoleAtentionStart = inSDConsoleAtentionStart;
    this.Id = this.SDConsoleAtentionStart.SDWHOTOCASE.IDSDWHOTOCASE;
    //Procesos de Utranslate.js   


    _this.InitializeComponent();
    _this.Initialize(_this.SDConsoleAtentionStart);
}
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype._frRefresh = function (SDConsoleAtentionStart) {



    var _this = this.TParent();


    //spHeadRow1.Column[2].This   ---> para el titulo del caso
    //spHeadRow1_1.Column[0].This ---> para el boton refrescar
    //spHeadRow1_1.Column[1].This ---> para el boton atras
    //spHeadRow1_2.Column[0].This ---> para la img de ticket
    //spHeadRow1_2.Column[1].This ---> para el nro de ticket

    //spHeadRow2.Column[2].This   ---> para la descripcion del caso
    //spHeadRow2_1_1.Column[0].This ---> para las estrellitas
    //spHeadRow2_1_2.Column[0].This ---> para fecha de reporte
    //spHeadRow2_2_1.Column[0].This ---> para el actual status
    //spHeadRow2_2_2.Column[0].This ---> para el usuario del caso

    //spHeadRow3.Column[0].This   ---> para el timer
    //spHeadRow3.Column[1].This   ---> para el atention
    //spHeadRow3.Column[2].This   ---> para el path



    if (_this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASESTATUS == 7) _this.img_case.Src = "image/24/CaseYellow.png"

    if (_this.ModeManager) {
        //objWindow.ToolTipContent
        _this.img_case.Title = UsrCfg.InternoAtisNames.SDCASE.IDSDCASE.Header + ":" + _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE + "\n" +
              UsrCfg.InternoAtisNames.SDCASESOURCETYPE.SOURCETYPENAME.Header + ":" + _this.SDCONSOLEATENTION.SDCASECOMPLTE.SOURCETYPENAME + "\n" +
              UsrCfg.InternoAtisNames.SDCASESTATUS.CASESTATUSNAME.Header + ":" + _this.SDCONSOLEATENTION.SDCASECOMPLTE.CASESTATUSNAME + "\n" +
              UsrCfg.InternoAtisNames.MDPRIORITY.PRIORITYNAME.Header + ":" + _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.PRIORITYNAME + "\n" +
              UsrCfg.InternoAtisNames.SDCASEACTIVITIES.LS_NAMESTEP.Header + ":" + _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.CASEMT_SET_LS_NAMESTEP + "\n" +
              UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDWHOTOCASETYPE.Header + ":" + UsrCfg.SD.Properties.TIDSDWHOTOCASETYPE.GetEnum(_this.SDWHOTOCASE.IDSDWHOTOCASETYPE).name;

    }
    _this.lblCaseNumber.Text = _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE;

    _this.Lbl_CASE_TITLE.Text = _this.SDCONSOLEATENTION.SDCASECOMPLTE.CASE_TITLE;




    _this.VcllabelStatus.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "esta1_uh4") + UsrCfg.SD.Properties.TSDCaseStatus.GetEnum(_this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASESTATUS).name;//**



    if (_this.SDCONSOLEATENTION.SDCASECOMPLTE.CASE_DESCRIPTION != "") {
        _this.Txt_CASE_DESCRIPTION.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "deta1_uh4") + " " + _this.SDCONSOLEATENTION.SDCASECOMPLTE.CASE_DESCRIPTION;//***
    }

    _this.Txt_CASE_DATECREATE.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "fecha_uh4") + _this.formatJSONDate(_this.SDCONSOLEATENTION.SDCASECOMPLTE.CASE_DATECREATE);


    //var mytitle0 = "";
    //var mytitle1 = "";
    //var mytitle2 = "";
    //mytitle0 = UsrCfg.Traslate.GetLangText(_this.Mythis, "fecha_uh4") + _this.formatJSONDate(_this.SDCONSOLEATENTION.SDCASECOMPLTE.CASE_DATECREATE);
    var StrAten = "";
    if (_this.ModeManager) {
        if (_this.SDWHOTOCASE.IDSDWHOTOCASETYPE == UsrCfg.SD.Properties.TIDSDWHOTOCASETYPE._EXTERNAL.value) StrAten = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines49");
        if (_this.SDWHOTOCASE.IDSDWHOTOCASETYPE == UsrCfg.SD.Properties.TIDSDWHOTOCASETYPE._INTERNAL.value) StrAten = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines50");
    }

    if ((_this.SDWHOTOCASE.IDSDTYPEUSER == 1) ||
        (_this.SDWHOTOCASE.IDSDTYPEUSER == 2) ||
        (_this.SDWHOTOCASE.IDSDTYPEUSER == 3)) {

        _this.TxtSDTYPEUSER.Text = Persistence.Catalog.Methods.SDTYPEUSER_ListSetID(Persistence.Profiler.CatalogProfiler.SDTYPEUSERList, _this.SDWHOTOCASE.IDSDTYPEUSER).TYPEUSERNAME + " " + StrAten;
    }
    else {
        _this.TxtSDTYPEUSER.Text = Persistence.Catalog.Methods.SDTYPEUSER_ListSetID(Persistence.Profiler.CatalogProfiler.SDTYPEUSERList, _this.SDWHOTOCASE.IDSDTYPEUSER).TYPEUSERNAME;
    }



    _this.TxtUSER.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines51") + _this.SDCONSOLEATENTION.SDCASECOMPLTE.USERNAME + "(" + _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDUSER + ")";

    _this.TxtCATEGORY.Text = UsrCfg.SD.Properties.TMDSERVICETYPE.GetEnum(_this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.MT_IDMDSERVICETYPE).name + ":" + _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.CATEGORY + "\\" + _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.CATEGORYNAME;



    /*

    if (mytitle0 != "") {
        var VclSPTitle0 = new TVclStackPanel(_this.DivTitle, "1", 1, [[12], [12], [12], [12], [12]]);
        var VcllabelTitle0 = new TVcllabel(VclSPTitle0.Column[0].This, "", TlabelType.H4);
        VcllabelTitle0.Text = mytitle0;
        VcllabelTitle0.This.style.fontSize = "15px";
        VcllabelTitle0.This.style.marginTop = "3px";
        VcllabelTitle0.This.style.marginBottom = "3px";
    }
    if (mytitle1 != "") {
        var VclSPTitle1 = new TVclStackPanel(_this.DivTitle, "1", 1, [[12], [12], [12], [12], [12]]);
        var VcllabelTitle1 = new TVcllabel(VclSPTitle1.Column[0].This, "", TlabelType.H4);
        VcllabelTitle1.Text = mytitle1;
        VcllabelTitle1.This.style.fontSize = "15px";
        VcllabelTitle1.This.style.marginTop = "3px";
        VcllabelTitle1.This.style.marginBottom = "3px";
    }
    if (mytitle2 != "") {
        var VclSPTitle2 = new TVclStackPanel(_this.DivTitle, "1", 1, [[12], [12], [12], [12], [12]]);
        var VcllabelTitle2 = new TVcllabel(VclSPTitle2.Column[0].This, "", TlabelType.H4);
        VcllabelTitle2.Text = mytitle2;
        VcllabelTitle2.This.style.fontSize = "15px";
        VcllabelTitle2.This.style.marginTop = "3px";
        VcllabelTitle2.This.style.marginBottom = "3px";
    }
    */
    _this.DivStar.innerHTML = "";
    //$(_this.DivStar).addClass("Divtooltip")
    //var VcllabelPRIORITYNAME = new Vcllabel(_this.DivStar, "", TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines53", "Priority:") + _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.PRIORITYNAME);
    //$(VcllabelPRIORITYNAME).addClass("tooltiptext");

    //if (_this.SDConsoleAtentionStart.NumStar > 5) { _this.SDConsoleAtentionStart.NumStar = 5 }
    //var empty_Star = _this.SDConsoleAtentionStart.NumStar - _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.IDMDPRIORITY;
    //var img_siren;
    //for (var j = 0; j < _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.IDMDPRIORITY; j++) {
    //    img_siren = Uimg(_this.DivStar, "", "image/24/siren_on.png", "");
    //    img_siren.style.marginTop = "6px";
    //}
    //for (var j = 0; j < empty_Star; j++) {
    //    img_siren = Uimg(_this.DivStar, "", "image/24/siren_off.png", "");
    //    img_siren.style.marginTop = "6px";
    //}
    var arrColores = new Array();
    arrColores.push("#DF0101");
    arrColores.push("#FF0000");
    arrColores.push("#DF3A01");
    arrColores.push("#FF4000");
    arrColores.push("#DF7401");
    arrColores.push("#FE9A2E");
    arrColores.push("#DBA901");
    arrColores.push("#FFBF00");
    arrColores.push("#D7DF01");
    arrColores.push("#FFFF00");

    var arrColoresTemp = new Array();

    var bucles = parseInt(arrColores.length / Persistence.Profiler.CatalogProfiler.MDPRIORITYList.length);
    var indice = 0;

    //arrColoresTemp.push(arrColores[0]);
    for (var i = 0; i < Persistence.Profiler.CatalogProfiler.MDPRIORITYList.length; i++) {
        if (i + 1 == Persistence.Profiler.CatalogProfiler.MDPRIORITYList.length)
            arrColoresTemp.push(arrColores[arrColores.length - 1]);
        else {
            if (indice < arrColores.length - 1) {
                arrColoresTemp.push(arrColores[indice]);
                indice = indice + bucles;
            }
            else {
                arrColoresTemp.push(arrColores[arrColores.length - 1]);
            }
        }
    }
    //arrColoresTemp.push(arrColores[9]);

    var arrColumnas = new Array();

    var arrcoltemp = new Array();
    for (var i = 0; i < Persistence.Profiler.CatalogProfiler.MDPRIORITYList.length; i++) {
        arrcoltemp.push(parseInt(12 / Persistence.Profiler.CatalogProfiler.MDPRIORITYList.length));
    }
    arrColumnas.push(arrcoltemp);
    arrColumnas.push(arrcoltemp);
    arrColumnas.push(arrcoltemp);
    arrColumnas.push(arrcoltemp);
    arrColumnas.push(arrcoltemp);

    var spPriority = new TVclStackPanel(_this.DivStar, "prio", Persistence.Profiler.CatalogProfiler.MDPRIORITYList.length, arrColumnas);
     
    for (var i = 0; i < spPriority.Column.length; i++) {
        if (_this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.IDMDPRIORITY == i)
            spPriority.Column[i].This.style.backgroundColor = arrColoresTemp[i];
        else
            spPriority.Column[i].This.style.backgroundColor = '#D8D8D8';

        lblPriority = new TVcllabel(spPriority.Column[i].This, "", TlabelType.H4);
        lblPriority.Text = Persistence.Profiler.CatalogProfiler.MDPRIORITYList[i].PRIORITYNAME;
        lblPriority.This.style.fontSize = "13px";
        lblPriority.This.style.fontWeight = "700";
        lblPriority.This.classList.add("text-center");
        lblPriority.This.classList.add("h4-custom");
    }

}
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.InitilizeCustomCode = function () {
    var _this = this.TParent();
    var CustomCodeLocal = new _this.TCustomCodeLocal();
    switch (UsrCfg.SD.Properties.TSDCASEPERMISSION.GetEnum(_this.SDWHOTOCASE.IDSDCASEPERMISSION)) {
        case UsrCfg.SD.Properties.TSDCASEPERMISSION._VIEW:
            _this.CustomCodeLocal.vVIEW = true;
            break;
        case UsrCfg.SD.Properties.TSDCASEPERMISSION._ADDACTION:
            _this.CustomCodeLocal.vVIEW = true;
            _this.CustomCodeLocal.vADDACTION = true;
            break;
        case UsrCfg.SD.Properties.TSDCASEPERMISSION._TRANSFER:
            _this.CustomCodeLocal.vVIEW = true;
            _this.CustomCodeLocal.vADDACTION = true;
            _this.CustomCodeLocal.vTRANSFER = true;
            break;
        default:
            break;
    }

    if (!((_this.SDWHOTOCASE.IDSDTYPEUSER == 1) || (_this.SDWHOTOCASE.IDSDTYPEUSER == 2) || (_this.SDWHOTOCASE.IDSDTYPEUSER == 3))) {
        _this.CustomCodeLocal.vVIEW = false;
        _this.CustomCodeLocal.vADDACTION = false;
        _this.CustomCodeLocal.vTRANSFER = false;
        switch (UsrCfg.SD.Properties.TMDLIFESTATUSPERMISSION.GetEnum(_this.IDMDLIFESTATUSPERMISSION)) {
            case UsrCfg.SD.Properties.TMDLIFESTATUSPERMISSION._Disable:
                break;
            case UsrCfg.SD.Properties.TMDLIFESTATUSPERMISSION._Read:
                _this.CustomCodeLocal.vVIEW = true;
                break;
            case UsrCfg.SD.Properties.TMDLIFESTATUSPERMISSION._Write:
                _this.CustomCodeLocal.vVIEW = true;
                _this.CustomCodeLocal.vADDACTION = true;
                _this.CustomCodeLocal.vTRANSFER = true;
                break;
            default:
                break;
        }
    }



    /*
    for (var i = 0; i < _this.MDLIFESTATUSProfiler.MDLIFESTATUSTYPEUSERList.length; i++)
    {
        if ((_this.MDLIFESTATUSProfiler.MDLIFESTATUSTYPEUSERList[i].IDSDTYPEUSER == _this.SDWHOTOCASE.IDSDTYPEUSER) && (_this.SetTabContent().MDLIFESTATUS.STATUSN) == (_this.MDLIFESTATUSProfiler.MDLIFESTATUSTYPEUSERList[i].STATUSN))
        {
            _this.CustomCodeLocal.CUSTOMCODEStr = _this.MDLIFESTATUSProfiler.MDLIFESTATUSTYPEUSERList[i].MDINTERFACE.CUSTOMCODE;
            break;

        }
    }*/


    _this.CustomCodeSetVisibility(true);//prende una estructura
    _this.InterfaceProfiler = new Interface.TInterfaceProfiler(_this./*CustomCodeLocal.*/CUSTOMCODEStr);
    
    
    //Interface.Methods.ApplyFormbyCode(this, this.LayoutRoot, CustomCodeLocal.CUSTOMCODEStr);
    //_this.RemoveButtonLines();
    /*

Atis.MD.DiagramMap.DiagramMapProvider oDiag = new Atis.MD.DiagramMap.DiagramMapProvider();
ComponentMindFusionManagerDiagramingProvider.TDiagramMap _Diag = new ComponentMindFusionManagerDiagramingProvider.TDiagramMap();
oDiag.ConstruirMapaMDLIFESTATUS(ref _Diag, chartStep.Diagramis, MDLIFESTATUSProfiler.MDLIFESTATUSList, _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.CASEMT_SET_LS_STATUSN);//SDCONSOLEATENTION.SDCASEMTCOMPLTE.CASEMT_SET_LS_STATUSN
*/
}

ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.RemoveButtonLines = function () {
    var _this = this.TParent();
    /*
    if (spTimeHeader.Visibility == System.Windows.Visibility.Collapsed)
    {
        rayaReloj.Visibility = System.Windows.Visibility.Collapsed;
    }
    else
    {
        if (Icntimerimg.Visibility == System.Windows.Visibility.Collapsed &&
            spMaxTime.Visibility == System.Windows.Visibility.Collapsed &&
            spRemainMaxTime.Visibility == System.Windows.Visibility.Collapsed &&
            spHierRemainMaxTime.Visibility == System.Windows.Visibility.Collapsed)
        {
            rayaReloj.Visibility = System.Windows.Visibility.Collapsed;
        }
    }


    if (spCategory.Visibility == System.Windows.Visibility.Collapsed)
    {
        rayaInformation.Visibility = System.Windows.Visibility.Collapsed;
    }
    else
    {
        if (btnDetail.Visibility == System.Windows.Visibility.Collapsed &&
           btnParentDetail.Visibility == System.Windows.Visibility.Collapsed &&
           Btn_CATEGORYChange.Visibility == System.Windows.Visibility.Collapsed &&
           btnWorkArrown.Visibility == System.Windows.Visibility.Collapsed &&
           btnUser.Visibility == System.Windows.Visibility.Collapsed)
        {
            rayaInformation.Visibility = System.Windows.Visibility.Collapsed;
            spCategory.Visibility = System.Windows.Visibility.Collapsed;
        }
    }


    if (spStatusScale.Visibility == System.Windows.Visibility.Collapsed)
    {
        rayaStatus.Visibility = System.Windows.Visibility.Collapsed;
    }
    else
    {
        if (BtnFnEscFun.Visibility == System.Windows.Visibility.Collapsed &&
           BtnFnEscHier.Visibility == System.Windows.Visibility.Collapsed &&
           BtnFnChangeStd.Visibility == System.Windows.Visibility.Collapsed)
        {
            rayaStatus.Visibility = System.Windows.Visibility.Collapsed;
            spStatusScale.Visibility = System.Windows.Visibility.Collapsed;
        }
    }


    if (spLink.Visibility == System.Windows.Visibility.Collapsed)
    {
        rayaLink.Visibility = System.Windows.Visibility.Collapsed;
    }
    else
    {
        if (BtnCIAfectado.Visibility == System.Windows.Visibility.Collapsed &&
           BtnAdjuntos.Visibility == System.Windows.Visibility.Collapsed &&
           BtnRelatedCase.Visibility == System.Windows.Visibility.Collapsed)
        {
            rayaLink.Visibility = System.Windows.Visibility.Collapsed;
            spLink.Visibility = System.Windows.Visibility.Collapsed;
        }
    }


    if (spParentCase.Visibility == System.Windows.Visibility.Collapsed)
    {
        rayaParent.Visibility = System.Windows.Visibility.Collapsed;
    }
    else
    {
        if (BtnFnViewRelation.Visibility == System.Windows.Visibility.Collapsed &&
           BtnADDRealtion.Visibility == System.Windows.Visibility.Collapsed &&
           BtnDELRealtions.Visibility == System.Windows.Visibility.Collapsed)
        {
            rayaParent.Visibility = System.Windows.Visibility.Collapsed;
            spParentCase.Visibility = System.Windows.Visibility.Collapsed;
        }
    }


    if (spLayout.Visibility == System.Windows.Visibility.Collapsed)
    {
        rayaLayout.Visibility = System.Windows.Visibility.Collapsed;
    }
    else
    {
        if (BtnSaveLayout.Visibility == System.Windows.Visibility.Collapsed &&
           BtnRestoreLayout.Visibility == System.Windows.Visibility.Collapsed)
        {
            rayaLayout.Visibility = System.Windows.Visibility.Collapsed;
            spLayout.Visibility = System.Windows.Visibility.Collapsed;
        }
    }

    foreach (UsrCfg.SD.TMDLIFESTATUSProfiler.TMDLIFESTATUS item in MDLIFESTATUSProfiler.MDLIFESTATUSList)
{
                item.NEXTSTEP = item.NEXTSTEP.Replace(" ", "").Replace("-", ",").Replace("(", "").Replace(")", "");
}
*/

}


//{$R *.dfm}



//****************** Inicio y control         *******************************************
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.ActulizaTodo = function (sender) {
    var _this = sender;


    //_this.CallbackModalResult(_this, ItHelpCenter.SD.CaseManager.Atention.TResultform.Refresh);
    ItHelpCenter.SD.CaseManager.Atention.OutSMConsoleManagerSwitch(_this.Object, ItHelpCenter.SD.CaseManager.Atention.TResultform.Refresh, _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE, _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.IDSDCASEMT, _this.SDWHOTOCASE.IDSDTYPEUSER, _this.CallbackModalResult, _this.MenuObject)
    /*
    var SetSDConsoleAtentionStart = new UsrCfg.SD.TSetSDConsoleAtentionStart();
    SetSDConsoleAtentionStart.IDSDCASEMT = SDCONSOLEATENTION.SDCASEMTCOMPLTE.IDSDCASEMT;
    SetSDConsoleAtentionStart.IDSDCASE = SDCONSOLEATENTION.SDCASEMTCOMPLTE.IDSDCASE;
    SetSDConsoleAtentionStart.IDCMDBCI = SDWHOTOCASE.IDCMDBCI;
    SetSDConsoleAtentionStart.IDSDTYPEUSER = SDWHOTOCASE.IDSDTYPEUSER;
    SetSDConsoleAtentionStart.IDSDWHOTOCASETYPE = UsrCfg.SD.Properties.TIDSDWHOTOCASETYPE._INTERNAL;
    SetSDConsoleAtentionStart = Atis.SD.Methods.SetSDConsoleAtentionStart(SetSDConsoleAtentionStart);
    if (SetSDConsoleAtentionStart.ResErr.NotError == true)
    {
        if (SetSDConsoleAtentionStart.SDConsoleAtentionStart.ResultSDConsoleAtentionStart == UsrCfg.SD.Properties.TResultSDConsoleAtentionStart._IncomeAccepted)
        {
            _this.Initialize(SetSDConsoleAtentionStart.SDConsoleAtentionStart);
        }
        else
        {
            SysCfg.App.Methods.ShowMessage(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines5") + UsrCfg.SD.Properties.ResultSDConsoleAtentionStartIntToStr(SetSDConsoleAtentionStart.SDConsoleAtentionStart.ResultSDConsoleAtentionStart));
            //TaskClose(_this,false);
        }
    }
    else
    {
        ShowMessage.Message.ErrorMessage(SetSDConsoleAtentionStart.ResErr.Mesaje);
    }
    */

}
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.Initialize = function (SDConsoleAtentionStart) {
    var _this = this.TParent();
    _this.NEXTLIFESTATUSList = this.SDConsoleAtentionStart.NEXTLIFESTATUSList;

    this.IDSDTypeUserListAll = new Array();
    this.SDCONSOLEATENTION = this.SDConsoleAtentionStart.SDCONSOLEATENTION;
    this.SDWHOTOCASE = this.SDConsoleAtentionStart.SDWHOTOCASE;
    this.MDLIFESTATUSProfiler = this.SDConsoleAtentionStart.MDLIFESTATUSProfiler;
    this.ModeManager = (_this.SDWHOTOCASE.IDSDTYPEUSER != 4);
    this.CustomCodeLocal = new _this.TCustomCodeLocal();
    this.isGridRetun = false;
    this.NumberWorkArrown = 0;
    this.SDCASETIMERCOUNTProfiler = new UsrCfg.SD.SDCaseTimerCount.TSDCASETIMERCOUNTProfiler();
    this.Counter_MAXTIME = 0;
    this.Counter_NORMALTIME = 0;
    this.Counter_LEVELTIME = 0;
    this.IDMDLIFESTATUSPERMISSION = this.SDConsoleAtentionStart.IDMDLIFESTATUSPERMISSION;
    this.CUSTOMCODEStr = this.SDConsoleAtentionStart.CUSTOMCODEStr;
    this.InterfaceProfiler = null;
    _this._frRefresh();
    _this.CreateLifeStatus();
    _this.CreateRetaionsLigth();
    _this.Refresh();


    //***************************************************  ToolBar *************************************************************************************************
    var IsVisible = _this.GetUsersForSendMessage(_this.user_uul, _this.SDWHOTOCASE.IDSDWHOTOCASE, _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE, _this.SDWHOTOCASE.IDSDTYPEUSER)
    if (IsVisible) $(_this.btn_Msg_ViewPopup.DivRootContainer.Child[0].Child[0].This).show();
    else $(_this.btn_Msg_ViewPopup.DivRootContainer.Child[0].Child[0].This).hide();

    _this.GETMsgPost(_this.SDWHOTOCASE.IDSDWHOTOCASE, false);


}
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.Refresh = function () {
    var _this = this.TParent();
    _this.ShowLifeStatus();
    //ShowMyParetActivities();
    //*************  Pestaña  ****************************************                                        
    
    //ShowRelatedDirect();
    _this.GETMsgAten(_this);
    _this.GETMsgPost(_this.SDWHOTOCASE.IDSDWHOTOCASE, false);
    //CargarMatrixOfActivities();
    _this.ShowDescriptiosFixed();
    _this.ShowWorkArrown();
    _this.ShowTimeStaus();
    _this.ShowUser();
    _this.CustomCodeAndCfgShow();
    _this.RemoveButtonLines();
    _this.ShowCaseStaus();
    _this.FillInterface();
}
//------------- Create --------------------------------
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.CreateRetaionsLigth = function () {
    //Agregar el el tool
    //UnfrSDCMDBCI.Initialize(SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE, SysCfg.App.Methods.Cfg_WindowsTask.User.IDCMDBCI, (Int32)UsrCfg.SD.Properties.TSDTypeUser._Owner);
    //UnfrSDAttached.Initialize(SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE, SysCfg.App.Methods.Cfg_WindowsTask.User.IDCMDBCI, (Int32)UsrCfg.SD.Properties.TSDTypeUser._Owner);
    //UnfrSDRelatedCase.Initialize(SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE, SysCfg.App.Methods.Cfg_WindowsTask.User.IDCMDBCI, (Int32)UsrCfg.SD.Properties.TSDTypeUser._Owner);
}
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.CreateLifeStatus = function () {
    var _this = this.TParent();

    //_this.MDLIFESTATUSProfiler.StrtoLIFESTATUS(SDCONSOLEATENTION.SDCASEMTCOMPLTE.CASEMT_LIFESTATUS);

    _this.SDCASETIMERCOUNTProfiler.StrtoTIMERCOUNT(_this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.CASEMT_TIMERCOUNT);


    for (var i = 0; i < _this.MDLIFESTATUSProfiler.MDLIFESTATUSTYPEUSERList.length; i++) {
        var IDTYPEUSER = _this.MDLIFESTATUSProfiler.MDLIFESTATUSTYPEUSERList[i].IDSDTYPEUSER;
        _this.IDSDTypeUserListAll.push(IDTYPEUSER);
    }



    if (_this.ModeManager) {
        for (var Counter2 = 0; Counter2 < _this.MDLIFESTATUSProfiler.MDLIFESTATUSList.length; Counter2++) {

            var Vcllabel_NAMESTEPSET = new TVcllabel(_this.DivToolbaLifeStatus, "", TlabelType.H4);
            Vcllabel_NAMESTEPSET.Text = "...";
            Vcllabel_NAMESTEPSET.VCLType = TVCLType.BS;
            //padding: 10px; color: gray; margin-right: 5px; float: left; background-color: lightgreen;
            Vcllabel_NAMESTEPSET.This.style.padding = "10px";
            Vcllabel_NAMESTEPSET.This.style.color = "gray";
            Vcllabel_NAMESTEPSET.This.style.marginRight = "5px";
            Vcllabel_NAMESTEPSET.This.style.float = "left";
            Vcllabel_NAMESTEPSET.This.style.backgroundColor = "#DDE6BD";

            if (_this.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].STATUSN == _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.CASEMT_SET_LS_STATUSN) {
                Vcllabel_NAMESTEPSET.Text = _this.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].NAMESTEP;
                Vcllabel_NAMESTEPSET.This.style.color = "black";
                Vcllabel_NAMESTEPSET.This.style.backgroundColor = "#F4D380";
            }
            else {
                Vcllabel_NAMESTEPSET.Text = _this.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].NAMESTEP;
            }
        }
    }
    else {
        //esta1_uh4
        var esta1_uh4 = new TVcllabel(_this.DivToolbaLifeStatus, "", TlabelType.H4);
        esta1_uh4.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Step1_uh4");
        esta1_uh4.VCLType = TVCLType.BS;
        esta1_uh4.This.style.fontSize = "15px";
        esta1_uh4.This.style.fontWeight = "700";
        esta1_uh4.This.style.float = "left";

        var Vcllabel_NAMESTEPSET = new TVcllabel(_this.DivToolbaLifeStatus, "", TlabelType.H4);
        Vcllabel_NAMESTEPSET.Text = "...";
        Vcllabel_NAMESTEPSET.VCLType = TVCLType.BS;
        Vcllabel_NAMESTEPSET.This.style.color = "gray";
        for (var Counter2 = 0; Counter2 < _this.MDLIFESTATUSProfiler.MDLIFESTATUSList.length; Counter2++) {
            if (_this.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].STATUSN == _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.CASEMT_SET_LS_STATUSN) {
                Vcllabel_NAMESTEPSET.Text = _this.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].NAMESTEP;
            }
        }
    }
    //****************************************************************************************************************************************************************************
    var btnGrabarETArray = _this.CargarTabControlAtentionCase(_this.DivDynamicSTEF, _this.Id, _this.SDConsoleAtentionStart);



    //********* Bloque de botones para cambiar de step *********************
    _this.LSEND_COMMENTSENABLE = false;
    _this.LSBEGIN_COMMENTSENABLE = false;
    for (var Counter2 = 0; Counter2 < _this.MDLIFESTATUSProfiler.MDLIFESTATUSList.length; Counter2++) {
        var memos = "";
        var LSCHANGE_COMMENTS = "";
        var LSEND_COMMENTSLABEL = "";
        var LSBEGIN_COMMENTSLABEL = "";
        var LSHERESTEP_CONFIG = "";
        var HERESTEP_CONFIG = "";
        var LSNEXTSTEP_CONFIG = "";
        var LSNEXTSTEP_CONFIGList = new Array();
        var haymensaje = false;
        //var LSEND_COMMENTSENABLE = false;
        //var LSBEGIN_COMMENTSENABLE = false;
        var Label_LSEND_COMMENTSENABLE = UsrCfg.Traslate.GetLangText(_this.Mythis, "Label_LSEND_COMMENTSENABLE");
        var Label_LSBEGIN_COMMENTSENABLE = UsrCfg.Traslate.GetLangText(_this.Mythis, "Label_LSBEGIN_COMMENTSENABLE");
        if (_this.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].STATUSN == _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.CASEMT_SET_LS_STATUSN) {
            var IDMDLIFESTATUSPERMISSION = 2;
            for (var i = 0; i < _this.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].MDLIFESTATUSTYPEUSERList.length; i++) {
                if (_this.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].MDLIFESTATUSTYPEUSERList[i].IDSDTYPEUSER == _this.SDWHOTOCASE.IDSDTYPEUSER) {

                    IDMDLIFESTATUSPERMISSION = _this.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].MDLIFESTATUSTYPEUSERList[i].IDMDLIFESTATUSPERMISSION;
                    LSCHANGE_COMMENTS = _this.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].MDLIFESTATUSTYPEUSERList[i].LSCHANGE_COMMENTS;
                    LSEND_COMMENTSLABEL = _this.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].MDLIFESTATUSTYPEUSERList[i].LSEND_COMMENTSLABEL;
                    LSBEGIN_COMMENTSLABEL = _this.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].MDLIFESTATUSTYPEUSERList[i].LSBEGIN_COMMENTSLABEL;
                    LSHERESTEP_CONFIG = _this.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].MDLIFESTATUSTYPEUSERList[i].LSHERESTEP_CONFIG;
                    HERESTEP_CONFIG = _this.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].MDLIFESTATUSTYPEUSERList[i].HERESTEP_CONFIG;
                    LSNEXTSTEP_CONFIG = _this.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].MDLIFESTATUSTYPEUSERList[i].LSNEXTSTEP_CONFIG;
                    LSNEXTSTEP_CONFIGList = _this.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].MDLIFESTATUSTYPEUSERList[i].NEXTSTEP_CONFIGList;
                    _this.LSEND_COMMENTSENABLE = _this.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].MDLIFESTATUSTYPEUSERList[i].LSEND_COMMENTSENABLE;
                    _this.LSBEGIN_COMMENTSENABLE = _this.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter2].MDLIFESTATUSTYPEUSERList[i].LSBEGIN_COMMENTSENABLE;
                    break;
                }
            }

            if (LSEND_COMMENTSLABEL != "") Label_LSEND_COMMENTSENABLE = LSEND_COMMENTSLABEL;
            if (LSEND_COMMENTSLABEL != "") Label_LSBEGIN_COMMENTSENABLE = LSBEGIN_COMMENTSLABEL;


            if (LSCHANGE_COMMENTS == "") {
                LSCHANGE_COMMENTS = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1_4");

            }


            if (IDMDLIFESTATUSPERMISSION == 2)//permiso write
            {
                memos += "";
                if (_this.LSEND_COMMENTSENABLE) memos += "<p style='margin-top: 8px;'>" + Label_LSEND_COMMENTSENABLE + "</p><textarea id='ta1" + _this.SDWHOTOCASE.IDSDWHOTOCASE + "' style='height: 150px; width: 100%; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;'></textarea>";
                if (_this.LSBEGIN_COMMENTSENABLE) memos += "<p style='margin-top: 8px;'>" + Label_LSBEGIN_COMMENTSENABLE + "</p><textarea id='ta2" + _this.SDWHOTOCASE.IDSDWHOTOCASE + "' style='height: 150px; width: 100%; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;'></textarea>";
                memos += "";

                //alert('si paso');
                $(_this.DivLSEND_COMMENTS).append(memos);

                for (var c = 0; c < _this.SDConsoleAtentionStart.NEXTLIFESTATUSList.length; c++) {
                    var NAMESTEP = "";
                    var STATUSN = "";
                    var CAPTION = "";
                    for (var d = 0; d < _this.SDConsoleAtentionStart.NEXTLIFESTATUSList[c].NEXTSTEPList.length; d++) {
                        NAMESTEP += _this.SDConsoleAtentionStart.NEXTLIFESTATUSList[c].NEXTSTEPList[d].NAMESTEP + "-";
                        STATUSN = _this.SDConsoleAtentionStart.NEXTLIFESTATUSList[c].NEXTSTEPList[d].STATUSN;
                        var CAPTIONtemp = _this.SDConsoleAtentionStart.NEXTLIFESTATUSList[c].NEXTSTEPList[d].NAMESTEP;
                        for (var x = 0; x < LSNEXTSTEP_CONFIGList.length; x++) {
                            if (parseInt(STATUSN) == LSNEXTSTEP_CONFIGList[x].STATUSN)
                                CAPTIONtemp = LSNEXTSTEP_CONFIGList[x].Caption;
                        }
                        CAPTION += CAPTIONtemp + "-";
                    }
                    if (_this.SDConsoleAtentionStart.NEXTLIFESTATUSList[c].NEXTSTEPList.length > 0) {
                        if (haymensaje == false) {
                            if (LSCHANGE_COMMENTS != "") {
                                var qu_uh4 = Vcllabel(_this.DivGrabarETArray /*TagMainContToolbar[0].Child[4].Child[0].This*/, "", TVCLType.BS, TlabelType.H4, LSCHANGE_COMMENTS);
                                qu_uh4.style.fontSize = "15px";
                                qu_uh4.style.fontWeight = "700";
                                qu_uh4.classList.add("text-left");
                            }
                            haymensaje = true;
                        }
                        NAMESTEP = NAMESTEP.substring(0, NAMESTEP.length - 1); //para quitar el -
                        CAPTION = CAPTION.substring(0, CAPTION.length - 1);//para quitar el -


                        var btnYes_uinput = new TVclInputbutton(_this.DivGrabarETArray, "");
                        btnYes_uinput.Text = CAPTION;
                        btnYes_uinput.VCLType = TVCLType.BS;
                        btnYes_uinput.This.classList.add("jmbtn");
                        btnYes_uinput.This.style.marginTop = "5px";
                        btnYes_uinput.This.style.marginBottom = "5px";
                        btnYes_uinput.This.style.marginLeft = "0px";
                        btnYes_uinput.This.style.marginRight = "0px";
                        btnYes_uinput.This.setAttribute("data-Count", c);
                        btnYes_uinput.Tag = STATUSN;
                        btnYes_uinput.onClick = function () {
                            var result = _this.CheckStd(5, _this.SDWHOTOCASE.IDSDWHOTOCASE, btnYes_uinput.Tag);
                            var count_save = c;
                            if (!result.Res) {
                                $('#txtMensajeReturn' + _this.SDWHOTOCASE.IDSDWHOTOCASE).append(result.MsgInfo);
                                document.location = '#Modal' + _this.SDWHOTOCASE.IDSDWHOTOCASE;

                            }
                            else {
                                //****** for que verifica si hay clicks pendientes eb extra fields  *******************
                                //for (var i = 0; i < response.MDSERVICEEXTRATABLEList.length; i++) {
                                //    var dis = btnGrabarET.Enabled; //$(btnGrabarET.This).attr('disabled');
                                //    if (dis) btnGrabarET.Click();
                                //}
                                for (var i = 0; i < btnGrabarETArray.length; i++) {
                                    var dis = btnGrabarETArray[i].Enabled; //$(btnGrabarET.This).attr('disabled');
                                    if (dis) btnGrabarETArray[i].Click();
                                }

                                var _OutStr1 = '';
                                var _OutStr2 = '';
                                if (_this.LSEND_COMMENTSENABLE) _OutStr1 = $('#ta1' + _this.SDWHOTOCASE.IDSDWHOTOCASE).html();
                                if (_this.LSBEGIN_COMMENTSENABLE) _OutStr2 = $('#ta2' + _this.SDWHOTOCASE.IDSDWHOTOCASE).html();
                                var parameters = {
                                    IDSDWHOTOCASE: _this.SDWHOTOCASE.IDSDWHOTOCASE,
                                    nameStep: NAMESTEP,
                                    OutStr1: _OutStr1,
                                    OutStr2: _OutStr2,
                                    Reload: false,
                                    Counter: parseInt($(this).attr("data-Count"))
                                };
                                parameters = JSON.stringify(parameters);
                                $.ajax({
                                    type: "POST",
                                    url: SysCfg.App.Properties.xRaiz + 'Service/SD/Atention.svc/GetNextStep',
                                    data: parameters,
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    async: false,
                                    success: function (response) {
                                        //location.href = location.href;
                                        _this.ActulizaTodo(_this);
                                    }
                                });
                            };
                        }
                    }
                }

            }
        }
    }







}
//-------------- Show  ----------------------------------------
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.ShowLifeStatus = function () {
    var _this = this.TParent();
    //if (SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASESTATUS == UsrCfg.SD.Properties.TSDCaseStatus._Closed)
    //{
    //    tabControlLifeStatus.Visibility = System.Windows.Visibility.Collapsed;
    //    return;
    //}
    //isGridRetun = false;
    //UsrCfg.SD.TMDLIFESTATUSProfiler.TMDLIFESTATUS MDLIFESTATUS = MDLIFESTATUSProfiler.MDLIFESTATUS_ListSetID(SDCONSOLEATENTION.SDCASEMTCOMPLTE.CASEMT_SET_LS_STATUSN);//***3
    //Int32 Idx = -1;
    //for (int i = 0; i < tabControlLifeStatus.Items.Count; i++)
    //{
    //    if ((UsrCfg.SD.TMDLIFESTATUSProfiler.TMDLIFESTATUS)((DXTabItem)tabControlLifeStatus.Items[i]).Tag == MDLIFESTATUS)
    //    {
    //        if (Idx == -1) Idx = i;//desaparece
    //        ((DXTabItem)tabControlLifeStatus.Items[i]).IsEnabled = true;
    //        ContenidoStep.Child = ((frSDCaseAtentionLifeStatus)((DXTabItem)tabControlLifeStatus.Items[i]).Content).tabControl;
    //    }
    //    else
    //    {
    //        ((DXTabItem)tabControlLifeStatus.Items[i]).IsEnabled = false;
    //    }
    //}
    //if (Idx != -1) tabControlLifeStatus.SelectedIndex = Idx;//desaparece
    _this.InitilizeCustomCode();
    for (var x = 0; x < _this/*.MDLIFESTATUS*/.NEXTLIFESTATUSList.length; x++) {
        if (_this/*.MDLIFESTATUS*/.NEXTLIFESTATUSList[x].NEXTSTEPList.length == 0) {
            _this.isGridRetun = true;
            break;
        }
    }

    //tabControlLifeStatus.Visibility = System.Windows.Visibility.Visible;






    //if (MDLIFESTATUSProfiler.MDLIFESTATUSList.Count == 0) tabControlLifeStatus.Visibility = System.Windows.Visibility.Collapsed;
}





ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.ShowCaseStaus = function () {
    var _this = this.TParent();
    if (_this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASESTATUS == UsrCfg.SD.Properties.TSDCaseStatus._InProgress.value) {
        _this.BtnFnChangeStd.Image = "image/24/Continue.png";
    }
    else if (_this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASESTATUS == UsrCfg.SD.Properties.TSDCaseStatus._Paused.value) {
        _this.BtnFnChangeStd.Image = "image/24/Pause.png";
    }
    else if (_this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASESTATUS == UsrCfg.SD.Properties.TSDCaseStatus._Resolved.value) {
        _this.BtnFnChangeStd.Image = "image/32/semi-success32_yellow.png";
    }
    else if (_this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASESTATUS == UsrCfg.SD.Properties.TSDCaseStatus._Closed.value) {
        _this.BtnFnChangeStd.Image = "image/24/success.png";
    }
    else if (_this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASESTATUS == UsrCfg.SD.Properties.TSDCaseStatus._Cancelled.value) {
        _this.BtnFnChangeStd.Image = "image/24/case-remove.png";
    }
    //ToolTipService.SetToolTip(BtnFnChangeStd, UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2") + _this.SDCONSOLEATENTION.SDCASECOMPLTE.CASESTATUSNAME);


    if ((_this.BtnFnChangeStd.Visible) && (_this.isGridRetun) && (_this.SDWHOTOCASE.IDSDTYPEUSER == 1)) {
        

        var TfrSDOperationStatus = _this.CareteFnChangeStd(null);
        TfrSDOperationStatus.IsModal = false;
        

        TfrSDOperationStatus.BtnCerrar.Visible = false;
        TfrSDOperationStatus.Modal.btnClose.style.display = 'none';

        TfrSDOperationStatus.Modal.Content.This.style.border = "none";
        TfrSDOperationStatus.Modal.Content.This.style.paddingTop = "20px";
        //TfrSDOperationStatus.Modal.Content.This.style.borderTop = "5px solid rgb(0, 166, 90)";
        TfrSDOperationStatus.Modal.Content.This.style.boxShadow = "none";
        TfrSDOperationStatus.Modal.Content.This.style.backgroundColor = "transparent";
        TfrSDOperationStatus.Modal.Content.This.style.zIndex = "21474836470000";
        TfrSDOperationStatus.Modal.Content.This.style.position = "relative";
        TfrSDOperationStatus.Modal.Content.This.style.margin = "auto";

        TfrSDOperationStatus.Modal.Body.This.style.backgroundColor = "transparent";
        TfrSDOperationStatus.Modal.Body.This.style.border = "none";

        TfrSDOperationStatus.Modal.HeaderContainer.style.fontSize = "15px";
        TfrSDOperationStatus.Modal.HeaderContainer.style.fontWeight = "bold";

        $(_this.spDivSetOperationCase.Column[2].This).html(TfrSDOperationStatus.Modal.Content.This);

        /*         
        Btn_SaveRetun.Visibility = System.Windows.Visibility.Visible;
        GridRetun.Visibility = System.Windows.Visibility.Visible;            
        CmbBox_CASE_RETURN_COST.Visibility = System.Windows.Visibility.Visible;
        Lbl_CASE_RETURN_COST.Visibility = System.Windows.Visibility.Visible;
        
        if ((isGridRetun) && (SDWHOTOCASE.IDSDTYPEUSER == 1))
        {
            CmbBox_CASE_RETURN_COST.Items.Clear();
            Int32 Idex = 0;
            for (int i = 1; i <= SysCfg.Str.Methods.GetTokenCount(SDCONSOLEATENTION.SDCASEMTCOMPLTE.MT_POSSIBLERETURNS, ","); i++)
            {
                String NumSteepStr = "";
                NumSteepStr = SysCfg.Str.Methods.GetToken(SDCONSOLEATENTION.SDCASEMTCOMPLTE.MT_POSSIBLERETURNS, ",", i);
                if (SDCONSOLEATENTION.SDCASECOMPLTE.CASE_RETURN_STR == NumSteepStr) Idex = CmbBox_CASE_RETURN_COST.Items.Count;
                CmbBox_CASE_RETURN_COST.Items.Add(NumSteepStr);
            }
            if (CmbBox_CASE_RETURN_COST.Items.Count > 0)
            {                    
                CmbBox_CASE_RETURN_COST.SelectedIndex = Idex;
                SpinEdit_CASE_RETURN_STR.Value = Convert.ToInt32(SDCONSOLEATENTION.SDCASECOMPLTE.CASE_RETURN_COST);
            }
            else
            {
                CmbBox_CASE_RETURN_COST.Visibility = System.Windows.Visibility.Collapsed;
                Lbl_CASE_RETURN_COST.Visibility = System.Windows.Visibility.Collapsed;
            }
        }
        else
        {
            Btn_SaveRetun.Visibility = System.Windows.Visibility.Collapsed;
            GridRetun.Visibility = System.Windows.Visibility.Collapsed;
            CmbBox_CASE_RETURN_COST.Visibility = System.Windows.Visibility.Collapsed;
            Lbl_CASE_RETURN_COST.Visibility = System.Windows.Visibility.Collapsed;
        }
         */
    }


}
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.ShowTimeStaus = function () {
    var _this = this.TParent();
    var TimeRemainStr = "00";
    var TimeLavelFunctionalStr = "00";
    var TimeLavelFunctionalRemainStr = "00";
    var TimeLavelHierarchicalStr = "00";
    var TimeLavelHierarchicalRemainStr = "00";

    var TimeRemain = _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.MT_MAXTIME - _this.SDCONSOLEATENTION.SDCASECOMPLTE.CASE_COUNTTIME;
    TimeRemainStr = TimeRemain.toString();//"###,###"


    var MDFUNCPER = Persistence.Catalog.Methods.MDFUNCPER_ListSetID(Persistence.Profiler.CatalogProfiler.MDFUNCPERList, _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.MT_IDMDFUNCESC, _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.CASEMT_SET_FUNLAVEL + 1);
    if (MDFUNCPER.IDMDFUNCPER > 0) {
        var TimeLavelFunctional = (MDFUNCPER.PERCF / MDFUNCPER.TOTAL) * _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.MT_MAXTIME;
        var TimeLavelFunctionalRemain = _this.SDCONSOLEATENTION.SDCASECOMPLTE.CASE_COUNTTIME + TimeLavelFunctional;
        TimeLavelFunctionalStr = TimeLavelFunctional.toString();//"###,###"
        TimeLavelFunctionalRemainStr = TimeLavelFunctionalRemain.toString();//"###,###"
    }

    var MDHIERPER = Persistence.Catalog.Methods.MDHIERPER_ListSetID(Persistence.Profiler.CatalogProfiler.MDHIERPERList, _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.MT_IDMDHIERESC, _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.CASEMT_SET_HIERLAVEL + 1);
    if (MDHIERPER.IDMDHIERPER > 0) {
        var TimeLavelHierarchical = (MDHIERPER.PERCH / MDHIERPER.TOTAL) * _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.MT_MAXTIME;
        var TimeLavelHierarchicalRemain = _this.SDCONSOLEATENTION.SDCASECOMPLTE.CASE_COUNTTIME + TimeLavelHierarchical;
        TimeLavelHierarchicalStr = TimeLavelHierarchical.toString();//"###,###"
        TimeLavelHierarchicalRemainStr = TimeLavelHierarchicalRemain.toString();//"###,###"
    }
    var tooltip =
    UsrCfg.InternoAtisNames.SDCASEMT.MT_MAXTIME.Header + ":" + SysCfg.DateTimeMethods.ConvertMinutesToHours(_this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.MT_MAXTIME.toString()) + "\n" +

    UsrCfg.Traslate.GetLangTextdbExtra("MaxDate", "Max date") + ":" + SysCfg.DateTimeMethods.ToDateTimeString(_this.SDCASETIMERCOUNTProfiler.SDCASEMT_TIMER.MT_MAXTIME_DATE) + "\n" +

    UsrCfg.Traslate.GetLangTextdbExtra("NormalDate", "Normal date") + ":" + SysCfg.DateTimeMethods.ToDateTimeString(_this.SDCASETIMERCOUNTProfiler.SDCASEMT_TIMER.MT_NORMALTIME_DATE) + "\n" +

    UsrCfg.Traslate.GetLangTextdbExtra("TimeRemainStr", "Max time remaining") + ":" + SysCfg.DateTimeMethods.ConvertMinutesToHours(TimeRemainStr) + "\n" +
    UsrCfg.Traslate.GetLangTextdbExtra("TimeLavelFunctionalStr", "Functional level time") + ":" + SysCfg.DateTimeMethods.ConvertMinutesToHours(TimeLavelFunctionalStr) + "\n" +
    UsrCfg.Traslate.GetLangTextdbExtra("TimeLavelFunctionalRemainStr", "Functional level remain Time") + ":" + SysCfg.DateTimeMethods.ConvertMinutesToHours(TimeLavelFunctionalRemainStr) + "\n" +
    UsrCfg.Traslate.GetLangTextdbExtra("TimeLavelHierarchicalStr", "Hierarchical level time") + ":" + SysCfg.DateTimeMethods.ConvertMinutesToHours(TimeLavelHierarchicalStr) + "\n" +
    UsrCfg.Traslate.GetLangTextdbExtra("TimeLavelHierarchicalRemainStr", "Hierarchical level remain time") + ":" + SysCfg.DateTimeMethods.ConvertMinutesToHours(TimeLavelHierarchicalRemainStr);

    _this.NavBarTimer_Information.ToolTip = tooltip;
    _this.lblMaxTime.Visible = false;
    _this.NavBarTimer_Information.Visible = false;
    _this.GroupBarTimer_Maxtime.Visible = false;
    _this.GroupBarTimer_MaxtimeDate.Visible = false;
    _this.VclTimer_Level.Visible = false;

    if ((_this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASESTATUS == UsrCfg.SD.Properties.TSDCaseStatus._Cancelled.value) || (_this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASESTATUS == UsrCfg.SD.Properties.TSDCaseStatus._Closed.value) || (_this.SDWHOTOCASE.IDSDTYPEUSER == 4)) {

    }
    else {
        _this.lblMaxTime.Visible = true;
        _this.NavBarTimer_Information.Visible = true;
        _this.GroupBarTimer_Maxtime.Visible = true;
        _this.GroupBarTimer_MaxtimeDate.Visible = true;
        _this.Counter_MAXTIME = parseInt(_this.SDCASETIMERCOUNTProfiler.SDCASEMT_TIMER.MT_MAXTIME_DATE - SysCfg.DB.Properties.SVRNOW());//cabeza
        _this.Counter_NORMALTIME = parseInt(_this.SDCASETIMERCOUNTProfiler.SDCASEMT_TIMER.MT_NORMALTIME_DATE - SysCfg.DB.Properties.SVRNOW());//cabeza

        _this.Counter_MAXTIME = (_this.Counter_MAXTIME < 0 ? _this.Counter_MAXTIME * -1 : _this.Counter_MAXTIME);
        _this.Counter_NORMALTIME = (_this.Counter_NORMALTIME < 0 ? _this.Counter_NORMALTIME * -1 : _this.Counter_NORMALTIME);

        _this.GroupBarTimer_MaxtimeDate.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "GroupBarTimer_MaxtimeDate.Text") + "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + SysCfg.DateTimeMethods.ToDateTimeString(_this.SDCASETIMERCOUNTProfiler.SDCASEMT_TIMER.MT_MAXTIME_DATE);
        //_this.Counter_LEVELTIME = _this.Counter_NORMALTIME;
        //_this.VclTimer_Level.Visible = true;

        //SysCfg.DateTimeMethods.ConvertTimeStampToHours(_this.Counter_MAXTIME.toString());

        if (_this.SDWHOTOCASE.IDSDTYPEUSER == 2) {
            if (UsrCfg.SD.SDCaseTimerCount.Methods.MDFUNCPER_TIMER_ListGetIndex(_this.SDCASETIMERCOUNTProfiler.MDFUNCPER_TIMERList, _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.CASEMT_SET_FUNLAVEL) != -1) {
                var UnDateTime = UsrCfg.SD.SDCaseTimerCount.Methods.MDFUNCPER_TIMER_ListSetID(_this.SDCASETIMERCOUNTProfiler.MDFUNCPER_TIMERList, _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.CASEMT_SET_FUNLAVEL).FUNLAVEL_DATE;
                _this.Counter_LEVELTIME = parseInt(UnDateTime - SysCfg.DB.Properties.SVRNOW());//cabeza

                _this.Counter_LEVELTIME = (_this.Counter_LEVELTIME < 0 ? _this.Counter_LEVELTIME * -1 : _this.Counter_LEVELTIME);
                _this.VclTimer_Level.Visible = true;
                //_this.VclTimer_Level.Text = _this.Counter_LEVELTIME.toString();
                _this.VclTimer_Level.ToolTip = " of " + UnDateTime.toString();;
            }

        }
        if (_this.SDWHOTOCASE.IDSDTYPEUSER == 3) {
            if (UsrCfg.SD.SDCaseTimerCount.Methods.MDHIERPER_TIMER_ListGetIndex(_this.SDCASETIMERCOUNTProfiler.MDHIERPER_TIMERList, _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.CASEMT_SET_HIERLAVEL) != -1) {
                var UnDateTime = UsrCfg.SD.SDCaseTimerCount.Methods.MDHIERPER_TIMER_ListSetID(_this.SDCASETIMERCOUNTProfiler.MDHIERPER_TIMERList, _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.CASEMT_SET_HIERLAVEL).HIERLAVEL_DATE;
                _this.Counter_LEVELTIME = parseInt(UnDateTime - SysCfg.DB.Properties.SVRNOW());//cabeza

                _this.Counter_LEVELTIME = (_this.Counter_LEVELTIME < 0 ? _this.Counter_LEVELTIME * -1 : _this.Counter_LEVELTIME);
                _this.VclTimer_Level.Visible = true;
                //_this.VclTimer_Level.Text = _this.Counter_LEVELTIME.toString();
                _this.VclTimer_Level.ToolTip = " of " + UnDateTime.toString();;
            }
        }

        _this.TimerTemp.AddMethod("UpdateTimerCaseAtention", function () { _this.UpdateTimerCaseAtention(_this) }, 1000);
        _this.TimerTemp.Start();

    }
}

//var TimeRemain = _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.MT_MAXTIME - _this.SDCONSOLEATENTION.SDCASECOMPLTE.CASE_COUNTTIME;
//var TimeRemainStr = TimeRemain.toString();//"###,###"    
//if ((_this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASESTATUS == UsrCfg.SD.Properties.TSDCaseStatus._Cancelled) || (_this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASESTATUS == UsrCfg.SD.Properties.TSDCaseStatus._Closed) || (_this.SDWHOTOCASE.IDSDTYPEUSER == 4)) {
//    
//}
//else {
//    //spTimeHeader.Visibility = System.Windows.Visibility.Visible;
//    //spMaxTime.Visibility = System.Windows.Visibility.Visible;
//    _this.VclTimer_Level.Text =    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines52")+ SysCfg.DateTimeMethods.ConvertMinutesToHours(TimeRemain);
//    // _this.NavBarTimer_Information.ToolTip = " of " + SysCfg.DateTimeMethods.ConvertMinutesToHours(_this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.MT_MAXTIME);
//}



ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.ShowWorkArrown = function () {
    var _this = this.TParent();

    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.IDMDCATEGORYDETAIL.FieldName, _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.IDMDCATEGORYDETAIL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var OpenDataSet = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "SEARCH_COUNT_WORKARROWN", Param.ToBytes());
        if (OpenDataSet.ResErr.NotError) {
            _this.NumberWorkArrown = OpenDataSet.DataSet.Records[0].Fields[0].Value;
            _this.btnWorkArrown.Text = "Work-Arounds found " + OpenDataSet.DataSet.Records[0].Fields[0].Value;
        }
        else {
            ShowMessage.Message.ErrorMessage(OpenDataSet.ResErr.Mesaje);
        }
    }
    catch (ex) {
        alert(ex);
    }
    finally {
        Param.Destroy();
    }
}
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.ShowUser = function () {
    var _this = this.TParent();

    if (_this.SDCONSOLEATENTION.SDCASECOMPLTE.IDCMDBCONTACTTYPE_USER > 0) {
        _this.btnUser.ToolTip = _this.GETCMDBCONTACTTYPEStr(_this.SDCONSOLEATENTION.SDCASECOMPLTE.IDCMDBCONTACTTYPE_USER, _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDUSER);
    }
    else {
        _this.btnUser.Visible = false;
    }
}
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.GETCMDBCONTACTTYPEStr = function (IDCMDBCONTACTTYPE, IDCMDBCI) {
    var _this = this.TParent();

    var ResErr = new SysCfg.Error.Properties.TResErr();

    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    Param.AddInt32(UsrCfg.InternoAtisNames.CMDBCONTACTTYPE.IDCMDBCONTACTTYPE.FieldName, IDCMDBCONTACTTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCI.FieldName, IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {
        var DS_CMDBCONTACTTYPE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "GETCMDBCONTACTTYPEStr", Param.ToBytes());
        ResErr = DS_CMDBCONTACTTYPE.ResErr;
        if (ResErr.NotError) {
            if (DS_CMDBCONTACTTYPE.DataSet.RecordCount > 0) {

                var CI_GENERICNAME = DS_CMDBCONTACTTYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCI.CI_GENERICNAME.FieldName).asString();
                var NAME = DS_CMDBCONTACTTYPE.DataSet.RecordSet.FieldName("NAME").asString();
                var CONTACT = DS_CMDBCONTACTTYPE.DataSet.RecordSet.FieldName("CONTACT").asString();
                return "User " + CI_GENERICNAME + ":  " + NAME + " " + CONTACT;

            }
            else {
                DS_CMDBCONTACTTYPE.ResErr.NotError = false;
                DS_CMDBCONTACTTYPE.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return "User: " + _this.SDCONSOLEATENTION.SDCASECOMPLTE.USERNAME;
}
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.ShowDescriptiosFixed = function () {
    //Todas las operaciones que no esten en el encabezado
    /*
    ToolTipService.SetToolTip(BtnFnEscFun, UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1") + _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.CASEMT_SET_FUNLAVEL + UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3"));
    ToolTipService.SetToolTip(BtnFnEscHier, UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1") + _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.CASEMT_SET_HIERLAVEL + UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines4"));
    Lbl_MT_TITLEM.Content = _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.MT_TITLEM;
    Txt_MT_GUIDETEXT.Text = _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.MT_GUIDETEXT;
    */
}


//***************** GENERAL ********************************************************************
//----------------- Botones en top -------------------------------------------------------------
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.btnRefreshAll_onClick = function (sender, e) {
    var _this = sender;
    _this.ActulizaTodo(_this);
}
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.btnCloseAll_onClick = function (sender, e) {
    var _this = sender;
    _this.TaskClose(_this, false);
}
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.btnDetail_Click = function (sender, e) {

    var _this = sender;
    var Modal = new TVclModal(document.body, null, "IDModalAtentionDetail");
    Modal.Width = "85%";
    Modal.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.Mythis, "Modal.AddHeaderContent"));
    Modal.Body.This.id = "Modal_AtentionDetail";

    var TfrSDCaseAtentionDetail = new ItHelpCenter.SD.Shared.Atention.CaseAtentionDetail.TfrSDCaseAtentionDetail(Modal.Body.This, function (thisOut) {
        _this.Load();
    }, _this.SDCONSOLEATENTION);

    Modal.ShowModal();
    Modal.FunctionClose = function () {

    }
}
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.NavBarTimer_Information_Click = function (sender, EventArgs) {
    var _this = sender;
    var tooltip = "";
    alert(tooltip);
}
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.GroupBarTimer_Maxtime_Click = function (sender, EventArgs) {
    var _this = sender;
    var tooltip = "";
    alert(tooltip);
}
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.GroupBarTimer_MaxtimeDate_Click = function (sender, EventArgs) {
    var _this = sender;
    var tooltip = "";
    alert(tooltip);
}



//----------------- Verifica estado ------------------------------------------------------------
//public enum TOrigen{General = 1, Actividad = 2, Escalemiento = 3,MsgAtencion = 4,Step = 5,Status = 6,Relation = 6, Relations_Ligth = 6,}
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.CheckStdCase = function (Origen) {
    var _this = this.TParent();
    var CheckStdRes = ItHelpCenter.SD.Atention.CaseAtentionCheckStd.CheckStdRes(Origen, _this.SDWHOTOCASE.IDSDWHOTOCASE, /*5*/ _this.SetTabContent().MDLIFESTATUS.STATUSN);
    if (CheckStdRes.Res) {
        //div pop

        var UnfrSDCaseAtentionCheckStd = new ItHelpCenter.SD.TfrSDCaseAtentionCheckStd(/*_this.Modal.Body.This,*/"");
        UnfrSDCaseAtentionCheckStd.OnInicializeComponent = function () {
            UnfrSDCaseAtentionCheckStd.Initialize(CheckStdRes.MsgInfo, CheckStdRes.Refersh);
            UnfrSDCaseAtentionCheckStd.CierraVentana = ItHelpCenter.SD.CaseManager.Atention.frSDCaseAtentionCheckStd_EventCierraVentana;
            UnfrSDCaseAtentionCheckStd.Modal.ShowModal();
        }

    }
    //return (CheckStdRes.Res);
}
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.frSDCaseAtentionCheckStd_EventCierraVentana = function (sender, e) {
    var CheckStdResult = sender;
    switch (CheckStdResult) {
        case ItHelpCenter.SD.Atention.CaseAtentionCheckStd.TCheckStdResult.Cancel:

            break;
        case ItHelpCenter.SD.Atention.CaseAtentionCheckStd.TCheckStdResult.Refresh:
            //ActulizaTodo(_this);
            break;
        case ItHelpCenter.SD.Atention.CaseAtentionCheckStd.TCheckStdResult.Quit:

            //((IFormManager)((Atis.SD.frSDCaseAtention)this))._frDestroy();
            break;
        default:
            break;
    }
}


//************************* ACTIVIDADES Operaciones  ****************************************************** 
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.GETSetSDOperationCase = function (sender, SetSDOperationCase) {
    var _this = sender;
    SetSDOperationCase.SDCASEVERYFY.IDHANDLER = _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDHANDLER;
    SetSDOperationCase.SDCASEVERYFY.IDOWNER = _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDOWNER;
    SetSDOperationCase.SDCASEVERYFY.IDMANAGERSINFORMED = _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDMANAGERSINFORMED;
    SetSDOperationCase.SDCASEVERYFY.IDSDCASE = _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE;
    SetSDOperationCase.SDCASEVERYFY.IDSDCASEMT = _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.IDSDCASEMT;
    SetSDOperationCase.SDCASEVERYFY.IDSDCASEMTSTATUS = UsrCfg.SD.Properties.TSDCASEMTSTATUS.GetEnum(_this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.IDSDCASEMTSTATUS);//SerialWCF
    var _idcasestatus = undefined;
    if (typeof (_this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASESTATUS) == "number")
        _idcasestatus = _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASESTATUS;
    else
        _idcasestatus = _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASESTATUS.value;

    SetSDOperationCase.SDCASEVERYFY.IDSDCASESTATUS = UsrCfg.SD.Properties.TSDCaseStatus.GetEnum(_idcasestatus);//SerialWCF
    SetSDOperationCase.SDCASEVERYFY.IDSDCASE_PARENT = _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE_PARENT;
    SetSDOperationCase.SDCASEVERYFY.CASEMT_SET_LS_NAMESTEP = _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.CASEMT_SET_LS_NAMESTEP;
    SetSDOperationCase.SDCASEVERYFY.CASEMT_SET_LS_STATUSN = _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.CASEMT_SET_LS_STATUSN;
    SetSDOperationCase.SDCASEVERYFY.SDWHOTOCASE.SerialWCF(this.SDWHOTOCASE);




    SetSDOperationCase = ItHelpCenter.SD.Methods.SetSDOperationCase(SetSDOperationCase);
    if (SetSDOperationCase.ResErr.NotError == true) {
        if (SetSDOperationCase.SDCASEVERYFY.SDCASEVERYFYAction == UsrCfg.SD.Properties.TSDCASEVERYFYAction._Continuous) {
            switch (SetSDOperationCase.SDOPERATIONTYPES) {
                case UsrCfg.SD.Properties.TSDOPERATIONTYPES._ATTENTION:
                    switch (SetSDOperationCase.SDOPERATION_ATTENTION.IDSDOPERATION_ATTENTIONTYPE) {
                        case UsrCfg.SD.Properties.TIDSDOPERATION_ATTENTIONTYPE._Back:
                            break;
                        case UsrCfg.SD.Properties.TIDSDOPERATION_ATTENTIONTYPE._Message:
                            _this.GETMsgAten(_this);
                            break;
                        case UsrCfg.SD.Properties.TIDSDOPERATION_ATTENTIONTYPE._Next:
                            _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.CASEMT_SET_LS_NAMESTEP = SetSDOperationCase.SDOPERATION_ATTENTION.CASEMT_SET_LS_NAMESTEP;
                            _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.CASEMT_SET_LS_STATUSN = SetSDOperationCase.SDOPERATION_ATTENTION.CASEMT_SET_LS_STATUSN;
                            _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.CASEMT_SET_LS_NEXTSTEP = SetSDOperationCase.SDOPERATION_ATTENTION.CASEMT_SET_LS_NEXTSTEP;
                            _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.CASEMT_SET_LS_COMMENTSST = SetSDOperationCase.SDOPERATION_ATTENTION.CASEMT_SET_LS_COMMENTSST;
                            _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASESTATUS = SetSDOperationCase.SDOPERATION_ATTENTION.LS_IDSDCASESTATUS;
                            _this.SDCONSOLEATENTION.SDCASECOMPLTE.CASESTATUSNAME = UsrCfg.SD.Properties.SDCaseStatusIntToStr(SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASESTATUS);
                            _this.Refresh();
                            //_this.ShowCaseStaus();
                            //_this.ShowTimeStaus();
                            //_this.ShowLifeStatus();                                    
                            _this.GETMsgAten(_this);
                            break;
                        case UsrCfg.SD.Properties.TIDSDOPERATION_ATTENTIONTYPE._Result:
                            _this.GETMsgAten(_this);
                            break;
                    }
                    break;
                case UsrCfg.SD.Properties.TSDOPERATIONTYPES._CASEDESCRIPTION:
                    break;
                case UsrCfg.SD.Properties.TSDOPERATIONTYPES._CASETITLE:
                    break;
                case UsrCfg.SD.Properties.TSDOPERATIONTYPES._POST:
                    _this.GETMsgPost(_this.SDWHOTOCASE.IDSDWHOTOCASE, false);
                    break;

                    //GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
                    //_System: { value: 0, name: "System" },
                    //_Owner: { value: 1, name: "Owner" },
                    //_Handler: { value: 2, name:  "Handler" },
                    //_ManagersInformed: { value: 3, name: "ManagersInformed" },
                    //_User: { value: 4, name:"User" },

                case UsrCfg.SD.Properties.TSDOPERATIONTYPES._FUNCESC:
                    if (((_this.SDWHOTOCASE.IDSDTYPEUSER == UsrCfg.SD.Properties.TSDTypeUser._ManagersInformed.value) || (_this.SDWHOTOCASE.IDSDTYPEUSER == UsrCfg.SD.Properties.TSDTypeUser._Owner.value)) &&
                        (_this.SDWHOTOCASE.IDSDWHOTOCASETYPE == UsrCfg.SD.Properties.TIDSDWHOTOCASETYPE._INTERNAL)) _this.ActulizaTodo(_this);
                    else _this.TaskClose(_this, false);
                    break;
                case UsrCfg.SD.Properties.TSDOPERATIONTYPES._HIERESC:
                    if (((_this.SDWHOTOCASE.IDSDTYPEUSER == UsrCfg.SD.Properties.TSDTypeUser._Handler.value) || (_this.SDWHOTOCASE.IDSDTYPEUSER == UsrCfg.SD.Properties.TSDTypeUser._Owner.value)) &&
                        (_this.SDWHOTOCASE.IDSDWHOTOCASETYPE == UsrCfg.SD.Properties.TIDSDWHOTOCASETYPE._INTERNAL)) _this.ActulizaTodo(_this);
                    else _this.TaskClose(_this, false);
                    break;
                case UsrCfg.SD.Properties.TSDOPERATIONTYPES._PARENT:
                    switch (SetSDOperationCase.SDOPERATION_PARENT.IDSDOPERATION_PARENTTYPE) {
                        case UsrCfg.SD.Properties.TIDSDOPERATION_PARENTTYPE._ADDRELATION:
                            SysCfg.App.Methods.ShowMessage(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines10"));
                            _this.ActulizaTodo(_this);
                            break;
                        case UsrCfg.SD.Properties.TIDSDOPERATION_PARENTTYPE._DELRELATION:
                            SysCfg.App.Methods.ShowMessage(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines11"));
                            _this.ActulizaTodo(_this);
                            break;
                        default:
                            break;
                    }

                    break;
                case UsrCfg.SD.Properties.TSDOPERATIONTYPES._CASESTATUS:
                    _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASESTATUS = SetSDOperationCase.SDOPERATION_CASESTATUS.IDSDCASESTATUS;
                    _this.SDCONSOLEATENTION.SDCASECOMPLTE.CASESTATUSNAME = _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASESTATUS.name;
                    switch (SetSDOperationCase.SDOPERATION_CASESTATUS.IDSDCASESTATUS) {

                        case UsrCfg.SD.Properties.TSDCaseStatus._Start:
                            SysCfg.App.Methods.ShowMessage(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines12") + _this.SDCONSOLEATENTION.SDCASECOMPLTE.CASESTATUSNAME);
                            break;
                        case UsrCfg.SD.Properties.TSDCaseStatus._InsertMT:
                            switch (SetSDOperationCase.SDOPERATION_CASESTATUS.IDSDCASESETMODELTYPE) {
                                case UsrCfg.SD.Properties.TIDSDCASESETMODELTYPE._InsertMT:
                                    SysCfg.App.Methods.ShowMessage(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines14"));
                                    _this.TaskClose(_this, false);
                                    break;
                                case UsrCfg.SD.Properties.TIDSDCASESETMODELTYPE._UpdateMT:
                                    SysCfg.App.Methods.ShowMessage(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines13"));
                                    _this.ActulizaTodo(_this);
                                    break;
                            }
                            break;
                        case UsrCfg.SD.Properties.TSDCaseStatus._Created:
                            SysCfg.App.Methods.ShowMessage(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines12") + _this.SDCONSOLEATENTION.SDCASECOMPLTE.CASESTATUSNAME);
                            break;
                        case UsrCfg.SD.Properties.TSDCaseStatus._InProgress:
                            SysCfg.App.Methods.ShowMessage(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines12") + _this.SDCONSOLEATENTION.SDCASECOMPLTE.CASESTATUSNAME);
                            _this.Refresh();
                            break;
                        case UsrCfg.SD.Properties.TSDCaseStatus._Paused:
                            SysCfg.App.Methods.ShowMessage(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines12") + _this.SDCONSOLEATENTION.SDCASECOMPLTE.CASESTATUSNAME);
                            _this.Refresh();
                            break;
                        case UsrCfg.SD.Properties.TSDCaseStatus._Resolved:
                            SysCfg.App.Methods.ShowMessage(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines12") + _this.SDCONSOLEATENTION.SDCASECOMPLTE.CASESTATUSNAME);
                            _this.Refresh();
                            break;
                        case UsrCfg.SD.Properties.TSDCaseStatus._Closed:
                            SysCfg.App.Methods.ShowMessage(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines12") + _this.SDCONSOLEATENTION.SDCASECOMPLTE.CASESTATUSNAME);
                            _this.TaskClose(_this, false);// _this.Refresh();
                            break;
                        case UsrCfg.SD.Properties.TSDCaseStatus._Cancelled:
                            SysCfg.App.Methods.ShowMessage(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines12") + _this.SDCONSOLEATENTION.SDCASECOMPLTE.CASESTATUSNAME);
                            _this.TaskClose(_this, false);
                            break;
                    }
                    break;
                case UsrCfg.SD.Properties.TSDOPERATIONTYPES._CASEACTIVITIES:
                    if (SetSDOperationCase.SDConsoleAtentionStart.ResultSDConsoleAtentionStart == UsrCfg.SD.Properties.TResultSDConsoleAtentionStart._IncomeAccepted) {       /*
                            object form = null;
                            double width = 0;
                            double height = 0;
                            form = new Atis.SD.frSDCaseAtention();

                            ((Atis.SD.frSDCaseAtention)form).Initialize(ref SetSDOperationCase.SDConsoleAtentionStart);
                            ((Atis.SD.frSDCaseAtention)form).Background = new SolidColorBrush(Colors.White);
                            width = ((Atis.SD.frSDCaseAtention)form).Width;
                            height = ((Atis.SD.frSDCaseAtention)form).Height;
                            objWindow = new TaskManager.TaskWindow();
                            objWindow.Form = form;
                            objWindow.WindowType = UsrCfg.Properties.TaskWindowManager.GetWindowType("frSDConsoleProblemManager");//DifMAJS????//frSDCaseAtention //SdCasenew//Consolemanagaer//consule user de que estamos hablando para buscar y comparar ventanas
                            objWindow.Status = TaskManager.TaskWindow.TWindowStatus.Minimized;
                            objWindow.WindowColorType = TaskManager.TaskWindow.TWindowColorType.Alert;
                            UsrCfg.Properties.TaskWindowManager.AddTaskWindow(objWindow);
                            ((IFormManager)((Atis.SD.frSDCaseAtention)form))._frRefresh();*/
                    }
                    else if (SetSDOperationCase.SDConsoleAtentionStart.ResultSDConsoleAtentionStart == UsrCfg.SD.Properties.TResultSDConsoleAtentionStart._IncomeCreate) {       /*
                            Atis.SD.frSDCaseNew form = new Atis.SD.frSDCaseNew();//posible obj si se quiere usar iformmanager teoria ocupa la memoraia de Atis.SD.frSDCaseNew y ya no se puede convertir en ifrmmanager
                            form.ReInitialize(ref SetSDOperationCase.SDConsoleAtentionStart, true);
                            form.UpDateChanges -= ACTIVITIESUpdateChange;
                            form.UpDateChanges += ACTIVITIESUpdateChange;

                            Atis.TaskManager.TaskWindow objWindow1 = new TaskManager.TaskWindow();
                            objWindow1.Form = form;
                            objWindow1.Title = ((Atis.SD.frSDCaseNew)form).Title;
                            objWindow1.IdTask = objWindow.IconText = ((Atis.SD.frSDCaseNew)form).IDSDCASE.toString();
                            objWindow1.WindowType = UsrCfg.Properties.TaskWindowManager.GetWindowType("frSDCaseSet");
                            objWindow1.Status = TaskManager.TaskWindow.TWindowStatus.Minimized;
                            objWindow1.WindowColorType = TaskManager.TaskWindow.TWindowColorType.Normal;
                            UsrCfg.Properties.TaskWindowManager.AddTaskWindow(objWindow1);


                            TSDCASE_RELATION SDCASE_RELATION = new TSDCASE_RELATION();
                            SDCASE_RELATION.IDSDCASE = _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE;
                            SDCASE_RELATION._SDCASE_OF_TITLE = _this.SDCONSOLEATENTION.SDCASECOMPLTE.CASE_TITLE;
                            SDCASE_RELATION._SDCASE_OF_CATEGORY = _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.CATEGORY;
                            SDCASE_RELATION._SDCASE_OF_CATEGORYNAME = _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.CATEGORYNAME;
                            ((Atis.SD.frSDCaseNew)form).UnfrSDRelatedCase.ADD_Relation(SDCASE_RELATION);*/

                    }
                    else {
                        SysCfg.App.Methods.ShowMessage(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines5") + UsrCfg.SD.Properties.ResultSDConsoleAtentionStartIntToStr(SetSDOperationCase.SDConsoleAtentionStart.ResultSDConsoleAtentionStart));
                        _this.ActulizaTodo(_this);
                    }
                    break;
                default:
                    break;
            }

        }
        else if (SetSDOperationCase.SDCASEVERYFY.SDCASEVERYFYAction == UsrCfg.SD.Properties.TSDCASEVERYFYAction._ErrorOut) {
            SysCfg.App.Methods.ShowMessage("Atis:" + SetSDOperationCase.SDCASEVERYFY.Reason);
            _this.TaskClose(_this, false);
        }
        else if (SetSDOperationCase.SDCASEVERYFY.SDCASEVERYFYAction == UsrCfg.SD.Properties.TSDCASEVERYFYAction._ErrorRefresh) {
            SysCfg.App.Methods.ShowMessage(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines5") + SetSDOperationCase.SDCASEVERYFY.Reason);
            _this.ActulizaTodo(_this);
        }

    }
    else {
        alert("Not it could complete the action closing and enters again. " + SetSDOperationCase.ResErr.Mesaje);

    }
}
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.SDOperationModalResult = function (sender, e) {
    var _this = sender;
    if (_this.CheckStdCase(ItHelpCenter.SD.Atention.CaseAtentionCheckStd.TOrigen.Status)) return;
    //var _this = sender;
    var SetSDOperationCase = e;
    _this.GETSetSDOperationCase(_this, SetSDOperationCase);
}
//-----------TSDOPERATIONTYPES._FUNCESC y TSDOPERATIONTYPES._HIERESC: Encalamiento ------------------------
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.BtnFnEscFun_Click = function (sender, e) {
    var _this = sender;
    if (_this.CheckStdCase(ItHelpCenter.SD.Atention.CaseAtentionCheckStd.TOrigen.Escalemiento)) return;
    var SetSDOperationCase = new UsrCfg.SD.TSetSDOperationCase();
    SetSDOperationCase.SDOPERATIONTYPES = UsrCfg.SD.Properties.TSDOPERATIONTYPES._FUNCESC;
    SetSDOperationCase.SDOPERATION_FUNCESC.IDSDWHOTOCASE = _this.SDWHOTOCASE.IDSDWHOTOCASE;
    SetSDOperationCase.SDOPERATION_FUNCESC.FUNCESC_IDCMDBCI = 0;
    SetSDOperationCase.SDOPERATION_FUNCESC.FUNCESC_LAVEL = 0;
    SetSDOperationCase.SDOPERATION_FUNCESC.FUNCESC_MESSAGE = "";
    SetSDOperationCase.SDOPERATION_FUNCESC.IDSDSCALETYPE_FUNC = this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.IDSDSCALETYPE_FUNC;
    var frSDOperationESC = new ItHelpCenter.SD.frSDOperationESC();
    if (frSDOperationESC.Initialize(SetSDOperationCase, _this.SDCONSOLEATENTION)) {
        frSDOperationESC.OnModalResult = _this.SDOperationModalResult;
        frSDOperationESC.PageParent = _this;
        frSDOperationESC.Modal.ShowModal();

    }
    else {
        ShowMessage.Message.InfoMessage(UsrCfg.Traslate.GetLangText(this, "Lines15"));
    }
}
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.BtnFnEscHier_Click = function (sender, e) {
    var _this = sender;
    if (_this.CheckStdCase(ItHelpCenter.SD.Atention.CaseAtentionCheckStd.TOrigen.Escalemiento)) return;
    var SetSDOperationCase = new UsrCfg.SD.TSetSDOperationCase();
    SetSDOperationCase.SDOPERATIONTYPES = UsrCfg.SD.Properties.TSDOPERATIONTYPES._HIERESC;
    SetSDOperationCase.SDOPERATION_HIERESC.IDSDWHOTOCASE = _this.SDWHOTOCASE.IDSDWHOTOCASE;
    SetSDOperationCase.SDOPERATION_HIERESC.HIERESC_IDCMDBCI = 0;
    SetSDOperationCase.SDOPERATION_HIERESC.HIERESC_LAVEL = 0;
    SetSDOperationCase.SDOPERATION_HIERESC.HIERESC_MESSAGE = "";
    SetSDOperationCase.SDOPERATION_HIERESC.IDSDSCALETYPE_HIER = this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.IDSDSCALETYPE_HIER;
    var frSDOperationESC = new ItHelpCenter.SD.frSDOperationESC();
    if (frSDOperationESC.Initialize(SetSDOperationCase, _this.SDCONSOLEATENTION)) {
        frSDOperationESC.OnModalResult = _this.SDOperationModalResult;
        frSDOperationESC.PageParent = _this;
        frSDOperationESC.Modal.ShowModal();
    }
    else {
        ShowMessage.Message.InfoMessage(UsrCfg.Traslate.GetLangText(this, "Lines15"));
    }
}
//-----------TSDOPERATIONTYPES._POST o Chat----------------------------------------------------------------
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.GetUsersForSendMessage = function (Objeto, inIDSDWHOTOCASE, inIDSDCASE, inIDSDTYPEUSER) {
    var _this = this.TParent();
    var IsVisible = true;
    var parameters = {
        IDSDWHOTOCASE: inIDSDWHOTOCASE,
        IDSDCASE: inIDSDCASE,
        IDSDTYPEUSER: inIDSDTYPEUSER
    };
    parameters = JSON.stringify(parameters);
    $.ajax({
        type: "POST",
        url: SysCfg.App.Properties.xRaiz + 'Service/SD/Atention.svc/GetTSDWHOTOCASEList',
        data: parameters,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (response) {
            if (response) {
                for (var i = 0; i < response.length; i++) {
                    var ListBoxItem1 = new TVclListBoxItem();
                    ListBoxItem1.Text = response[i].TYPEUSERNAME;
                    ListBoxItem1.Index = i;
                    ListBoxItem1.Tag = response[i];
                    Objeto.AddListBoxItem(ListBoxItem1);
                }


                if (_this.SDWHOTOCASE.IDSDTYPEUSER == 4) {
                    for (var i = 0; i < Objeto.ListBoxItems.length; i++) {
                        Objeto.ListBoxItems[i].Checked = ((Objeto.ListBoxItems[i].Tag.IDSDTYPEUSER == 1) || (Objeto.ListBoxItems[i].Tag.IDSDTYPEUSER == 4));
                    }
                    IsVisible = false;
                }
            }
            else {
                alert('Expired permission');
            }
        },
        error: function (response) {
            SysCfg.Log.Methods.WriteLog("frSDCaseAtention.js ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.GetUsersForSendMessage Service/SD/Atention.svc/GetTSDWHOTOCASEList " + "Error no llamo al servicio");
        }
    });
    return IsVisible;
}
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.btn_Msg_ViewPopup_ButtonOkCaption_onClick = function () {
    var _this = this.TParent();

    var listausuarios = new Array();

    for (var i = 0; i < _this.user_uul.ListBoxItems.length; i++) {
        if (_this.user_uul.ListBoxItems[i].Checked)
        { listausuarios.push(_this.user_uul.ListBoxItems[i].Tag.IDSDTYPEUSER); }
    }


    var parameters = {
        IDSDWHOTOCASE: _this.SDWHOTOCASE.IDSDWHOTOCASE,
        IDSDTYPEUSERListOUT: listausuarios,
        Msg: _this.mensaje_utexarea.Text
    };
    parameters = JSON.stringify(parameters);
    if (parameters != "") {
        $.ajax({
            type: "POST",
            url: SysCfg.App.Properties.xRaiz + 'Service/SD/Atention.svc/SaveMessage',
            data: parameters,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (response) {
                _this.mensaje_utexarea.Text = "";
                _this.user_uul.UnCheckedAll();
                _this.btn_Msg_ViewPopup.Caption.click();
                _this.GETMsgPost(_this.SDWHOTOCASE.IDSDWHOTOCASE, false);//=response ARRGLAR EL RESPONSE POR QUE USA UN HTML CUANDO SE EELIMINE LA ANTENCION ANTIGUA

            },
            error: function (response) {
                SysCfg.Log.Methods.WriteLog("frSDCaseAtention.js ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.btn_Msg_ViewPopup_ButtonOkCaption_onClick Service/SD/Atention.svc/SaveMessage" + "Error no llamo al servicio");
            }
        });
    }
    else { $('#error_message').modal('show'); }
}
//----------TSDOPERATIONTYPES._ATTENTION:LifeStatus Mensajes Atencion--------------------------------------------------
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.SetTabContent = function () {

    var _this = this.TParent();
    return _this.SDConsoleAtentionStart;
}
//---------------- TSDOPERATIONTYPES._CASESTATUS ---------------------------------------------------------  
//........Cambia inpreogres,pausa, resolve, cancel, close .....................
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.BtnFnChangeStd_Click = function (sender, e) {
    var _this = sender;
    if (_this.CheckStdCase(ItHelpCenter.SD.Atention.CaseAtentionCheckStd.TOrigen.Status)) return;

    //Carlos crea la modal
    var TfrSDOperationStatus = _this.CareteFnChangeStd(null);
    TfrSDOperationStatus.Modal.ShowModal(); //asignarlo a un panel verdadero

}

ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.CareteFnChangeStd = function (Object)
{
    var _this = this.TParent();
    //******************************** validacion **************************
    //var SDCaseExplorerClient = new UsrCfg.SD.TSDCaseExplorer();//UsrCfg.SD.TSDCaseExplorerClient
    //SDCaseExplorerClient.GETSETCASE(_this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE, false);
    var StrPend = new SysCfg.ref("");
    var StrOut = "";
    //***********************************************************************
    var SetSDOperationCase = new UsrCfg.SD.TSetSDOperationCase();
    SetSDOperationCase.SDOPERATIONTYPES = UsrCfg.SD.Properties.TSDOPERATIONTYPES._CASESTATUS;
    var _idcasestatus = undefined;
    if (typeof (_this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASESTATUS) == "number")
        _idcasestatus = _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASESTATUS;
    else
        _idcasestatus = _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASESTATUS.value

    SetSDOperationCase.SDOPERATION_CASESTATUS.IDSDCASESTATUS = UsrCfg.SD.Properties.TSDCaseStatus.GetEnum(_idcasestatus);//SerialWCF
    SetSDOperationCase.SDOPERATION_CASESTATUS.IDSDWHOTOCASE = _this.SDWHOTOCASE.IDSDWHOTOCASE;

    //Dictionary<Int32, String> DatosCombo = new Dictionary<Int32, String>();

    var DatosCombo = new Array();
    //DatosCombo.push([UsrCfg.SD.Properties.TSDCaseStatus._Closed.value, UsrCfg.SD.Properties.TSDCaseStatus._Closed.name]);

    if (SetSDOperationCase.SDOPERATION_CASESTATUS.IDSDCASESTATUS == UsrCfg.SD.Properties.TSDCaseStatus._InProgress) {
        if (true/*!SDCaseExplorerClient.isEnableACTIVITIES(_this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE, StrPend, StrOut)*/) {
            if (_this.isGridRetun) {
                DatosCombo.push([UsrCfg.SD.Properties.TSDCaseStatus._Closed.value, UsrCfg.SD.Properties.TSDCaseStatus._Closed.name]);
            }

            DatosCombo.push([UsrCfg.SD.Properties.TSDCaseStatus._Cancelled.value, UsrCfg.SD.Properties.TSDCaseStatus._Cancelled.name]);
        }
        DatosCombo.push([UsrCfg.SD.Properties.TSDCaseStatus._Paused.value, UsrCfg.SD.Properties.TSDCaseStatus._Paused.name]);
    }
    else if (SetSDOperationCase.SDOPERATION_CASESTATUS.IDSDCASESTATUS == UsrCfg.SD.Properties.TSDCaseStatus._Paused)//SerialWCF
    {
        DatosCombo.push([UsrCfg.SD.Properties.TSDCaseStatus._InProgress.value, UsrCfg.SD.Properties.TSDCaseStatus._InProgress.name]);
        if (true/*!SDCaseExplorerClient.isEnableACTIVITIES(_this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE, StrPend, StrOut)*/) {
            DatosCombo.push([UsrCfg.SD.Properties.TSDCaseStatus._Paused.value, UsrCfg.SD.Properties.TSDCaseStatus._Paused.name]);
            DatosCombo.push([UsrCfg.SD.Properties.TSDCaseStatus._Cancelled.value, UsrCfg.SD.Properties.TSDCaseStatus._Cancelled.name]);
        }
    }
    else if (SetSDOperationCase.SDOPERATION_CASESTATUS.IDSDCASESTATUS == UsrCfg.SD.Properties.TSDCaseStatus._Resolved)//SerialWCF
    {
        if (true/*!SDCaseExplorerClient.isEnableACTIVITIES(_this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE, StrPend, StrOut)*/) {
            if (_this.isGridRetun) {
                DatosCombo.push([UsrCfg.SD.Properties.TSDCaseStatus._Closed.value, UsrCfg.SD.Properties.TSDCaseStatus._Closed.name]);
            }
            DatosCombo.push([UsrCfg.SD.Properties.TSDCaseStatus._Cancelled.value, UsrCfg.SD.Properties.TSDCaseStatus._Cancelled.name]);
        }
    }

    var TfrSDOperationStatus = new ItHelpCenter.SD.frSDOperationStatus(Object, "");
    TfrSDOperationStatus.frSDCAseAtention = this;
    var _name = "";
    if (typeof (_this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASESTATUS) == "number")
        _name = UsrCfg.SD.Properties.TSDCaseStatus.GetEnum(_this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASESTATUS).name;
    else
        _name = _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASESTATUS.name;

    TfrSDOperationStatus.Initialize(SetSDOperationCase,
        UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines23") + _name + ")",
        UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines24"),
        DatosCombo,
        UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines25"), "");

    TfrSDOperationStatus.GridRetun.Row.Visible = false;
    TfrSDOperationStatus.CmbBox_CASE_RETURN_COST.Visible = true;
    TfrSDOperationStatus.Lbl_CASE_RETURN_COST.Visible = true;
    TfrSDOperationStatus.isGridRetun = _this.isGridRetun;
    if ((_this.isGridRetun) && (_this.SDWHOTOCASE.IDSDTYPEUSER == 1)) {
        TfrSDOperationStatus.CmbBox_CASE_RETURN_COST.ClearItems();
        var Idex = 0;

        var MT_POSSIBLERETURNS_split = _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.MT_POSSIBLERETURNS.split(",");
        for (var i = 0; i < MT_POSSIBLERETURNS_split.length ; i++) {
            var NumSteepStr = MT_POSSIBLERETURNS_split[i];
            if (_this.SDCONSOLEATENTION.SDCASECOMPLTE.CASE_RETURN_STR == NumSteepStr) Idex = TfrSDOperationStatus.CmbBox_CASE_RETURN_COST.Options.length;
            var item = new TVclComboBoxItem();
            item.Value = NumSteepStr;
            item.Text = NumSteepStr;
            item.Tag = NumSteepStr;
            TfrSDOperationStatus.CmbBox_CASE_RETURN_COST.AddItem(item);

            //TfrSDOperationStatus.CmbBox_CASE_RETURN_COST.Items.Add(NumSteepStr);
        }
        if (TfrSDOperationStatus.CmbBox_CASE_RETURN_COST.Options.length > 0) {
            TfrSDOperationStatus.CmbBox_CASE_RETURN_COST.selectedIndex = Idex;
            TfrSDOperationStatus.SpinEdit_CASE_RETURN_STR.Text = _this.SDCONSOLEATENTION.SDCASECOMPLTE.CASE_RETURN_COST;
        }
        else {
            TfrSDOperationStatus.CmbBox_CASE_RETURN_COST.Visible = false;
            TfrSDOperationStatus.Lbl_CASE_RETURN_COST.Visible = false;
        }
    }
    else {
        TfrSDOperationStatus.CmbBox_CASE_RETURN_COST.Visible = false;
        TfrSDOperationStatus.Lbl_CASE_RETURN_COST.Visible = false;
    }
    TfrSDOperationStatus.CmbStatus_SelectedChanged(TfrSDOperationStatus, TfrSDOperationStatus.CmbStatus);

    TfrSDOperationStatus.ModalResult = _this.SDOperationModalResult;
    return TfrSDOperationStatus;
}
//......Informativos..........................
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.btnUser_Click = function (sender, EventArgs) {
    var _this = sender;
    var iduser = _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDUSER;
    if (iduser > 0) {

        var Modal = new TVclModal(document.body, null, "");
        if (SysCfg.App.Properties.Device.value == SysCfg.App.TDevice.Desktop.value) {
            Modal.Width = "60%";
        } else {
            Modal.Width = "100%";
        }
        Modal.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.Mythis, "lb_NotificationMetod"));
        Modal.Body.This.id = "Modal_Contact_" + _this.Id;

        var contact = new ItHelpCenter.SD.Shared.Contact.UfrCMDBContact(Modal.Body.This, _this.ID, function (sender) {
            Modal.CloseModal();
            _this.ShowUser();
        }, iduser, _this);

        Modal.ShowModal();
        Modal.FunctionClose = function () {
            _this.ShowUser();
        }



        //CMDB.frCMDBContact TfrCMDBContact = new CMDB.frCMDBContact();
        //TfrCMDBContact.Initialize(iduser);
        //TfrCMDBContact.Show();
    }
}
//.....Mensaje de atencion ...................
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.GETMsgAten = function (sender) {
    var _this = sender;
    _this.spMensajes.Column[0].This.innerHTML = "";
    var Param1;
    var Param2;
    var Param3;

    if (_this.AtentionViewAll.Checked) {
        Param1 = false; Param2 = false; Param3 = false;
    }
    else {
        Param1 = true; Param2 = true; Param3 = true;
    }
    var Param = new SysCfg.Stream.Properties.TParam();

    try {
        Param.Inicialize();
        //*********** SDOPERATION_ATTENTION_GET ***************************** 
        Param.AddInt32(UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASE.FieldName, _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        if (Param1)
            Param.AddInt32(UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASEMT.FieldName, _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        else
            Param.AddUnknown(UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASEMT.FieldName, UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE + "." + UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASEMT.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        if (Param3)
            Param.AddInt32(UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDWHOTOCASE.FieldName, _this.SDWHOTOCASE.IDSDWHOTOCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        else
            Param.AddUnknown(UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDWHOTOCASE.FieldName, UsrCfg.InternoAtisNames.SDWHOTOCASE.NAME_TABLE + "." + UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDWHOTOCASE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);


        var DS_SDOPERATION_ATTENTION = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "GETSDOPERATION_ATTENTION", Param.ToBytes());

        if (DS_SDOPERATION_ATTENTION.ResErr.NotError) {
            if (DS_SDOPERATION_ATTENTION.DataSet.RecordCount > 0) {
                DS_SDOPERATION_ATTENTION.DataSet.First();

                var fechatemp = new Date();
                var ischangestep = false;

                while (!(DS_SDOPERATION_ATTENTION.DataSet.Eof)) {
                    ischangestep = false;
                    IDCMDBCI = DS_SDOPERATION_ATTENTION.DataSet.RecordSet.FieldName("IDCMDBCI").asInt32();
                    USERNAME = DS_SDOPERATION_ATTENTION.DataSet.RecordSet.FieldName("USERNAME").asString();
                    ATTENTION_DATE = DS_SDOPERATION_ATTENTION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.ATTENTION_DATE.FieldName).asDateTime();
                    ATTENTION_MESSAGE = DS_SDOPERATION_ATTENTION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.ATTENTION_MESSAGE.FieldName).asString();
                    IDSDOPERATION_ATTENTION = DS_SDOPERATION_ATTENTION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.IDSDOPERATION_ATTENTION.FieldName).asInt32();

                    var IDSDTYPEUSER = UsrCfg.SD.Properties.TSDTypeUser.GetEnum(DS_SDOPERATION_ATTENTION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDTYPEUSER.FieldName).asInt32());
                    var IDSDWHOTOCASETYPE = UsrCfg.SD.Properties.TIDSDWHOTOCASETYPE.GetEnum(DS_SDOPERATION_ATTENTION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDWHOTOCASETYPE.FieldName).asInt32());
                    IDSDTYPEUSERNAME = IDSDTYPEUSER.name;
                    IDSDWHOTOCASETYPENAME = IDSDWHOTOCASETYPE.name;

                    CASEMT_SET_LS_NAMESTEP = DS_SDOPERATION_ATTENTION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.CASEMT_SET_LS_NAMESTEP.FieldName).asString();
                    CASEMT_SET_LS_STATUSN = DS_SDOPERATION_ATTENTION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.CASEMT_SET_LS_STATUSN.FieldName).asInt32();
                    LS_IDSDCASESTATUS = DS_SDOPERATION_ATTENTION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.LS_IDSDCASESTATUS.FieldName).asInt32();
                    IDSDOPERATION_ATTENTIONTYPE = DS_SDOPERATION_ATTENTION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.IDSDOPERATION_ATTENTIONTYPE.FieldName).asInt32();
                    var isempty = false;
                    if (IDSDOPERATION_ATTENTIONTYPE == 4) {
                        if (ATTENTION_MESSAGE == "")
                            isempty = true;
                        else {
                            CASEMT_SET_LS_NAMESTEP = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines48") + " " + CASEMT_SET_LS_NAMESTEP;
                            ATTENTION_MESSAGE = CASEMT_SET_LS_NAMESTEP + "\n" + ATTENTION_MESSAGE;
                            ischangestep = true;
                        }

                    }
                    else if (IDSDOPERATION_ATTENTIONTYPE == 2) {
                        if (ATTENTION_MESSAGE == "")
                            isempty = true;
                        else {
                            CASEMT_SET_LS_NAMESTEP = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines46") + " " + CASEMT_SET_LS_NAMESTEP;
                            ATTENTION_MESSAGE = CASEMT_SET_LS_NAMESTEP + "\n" + ATTENTION_MESSAGE;
                            ischangestep = true;
                        }
                    }
                    else {
                        if (ATTENTION_MESSAGE == "")
                            isempty = true;
                        else {

                            CASEMT_SET_LS_NAMESTEP = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines47") + " " + CASEMT_SET_LS_NAMESTEP;
                            ATTENTION_MESSAGE = CASEMT_SET_LS_NAMESTEP + "\n" + ATTENTION_MESSAGE;
                            ischangestep = true;
                        }
                    }


                    if (SysCfg.DateTimeMethods.DayOfYear(ATTENTION_DATE) != SysCfg.DateTimeMethods.DayOfYear(fechatemp)) {
                        if (!isempty) {
                            var VclStackPanel = new TVclStackPanel(_this.spMensajes.Column[0].This, "1", 2, [[12], [12], [12], [12], [12]]);
                            var lblFecha = new TVcllabel(VclStackPanel.Column[0].This, "", TlabelType.H0);
                            lblFecha.Text = SysCfg.DateTimeMethods.ToDateString(ATTENTION_DATE);
                            lblFecha.VCLType = TVCLType.BS;
                        }
                    }

                    if (!isempty) {
                        var VclStackPanel = new TVclStackPanel(_this.spMensajes.Column[0].This, "3", 3, [[3, 7, 2], [3, 7, 2], [3, 7, 2], [3, 7, 2], [3, 7, 2]]);
                        VclStackPanel.Row.This.style.marginBottom = "6px";


                        var VclStackPanel2 = new TVclStackPanel(VclStackPanel.Column[0].This, "2", 2, [[3, 9], [3, 9], [3, 9], [3, 9], [3, 9]]);

                        var imgImage = new TVclImagen(VclStackPanel2.Column[0].This, "");
                        imgImage.Src = "image/24/user-blue2.png";
                        imgImage.Title = USERNAME + "(" + IDSDWHOTOCASETYPENAME + " " + IDSDTYPEUSERNAME + ")" + " says:";
                        imgImage.This.style.marginTop = "-6px";

                        var lblUser = new TVcllabel(VclStackPanel2.Column[1].This, "", TlabelType.H0);
                        lblUser.This.style.marginLeft = "5px";
                        lblUser.This.style.fontSize = "13px";
                        lblUser.This.style.fontWeight = "normal";
                        lblUser.This.style.color = "rgb(128, 128, 128)";
                        lblUser.Text = USERNAME;
                        //lblUser.VCLType = TVCLType.BS;

                        var lblMensaje = new TVcllabel(VclStackPanel.Column[1].This, "", TlabelType.H0);

                        lblMensaje.This.style.fontSize = "15px";
                        lblMensaje.This.style.fontWeight = "normal";
                        lblMensaje.Text = ATTENTION_MESSAGE;
                        lblMensaje.VCLType = TVCLType.BS;

                        var lblFecha = new TVcllabel(VclStackPanel.Column[2].This, "", TlabelType.H0);
                        lblFecha.This.style.fontSize = "15px";
                        lblFecha.This.style.fontWeight = "normal";
                        lblFecha.Text = SysCfg.DateTimeMethods.ToTimeString(ATTENTION_DATE);
                        lblFecha.VCLType = TVCLType.BS;
                    }
                    fechatemp = ATTENTION_DATE;

                    //OutStr = ATTENTION_DATE.ToString("MM/dd/yy HH:mm") + " " + USERNAME + "(" + IDSDWHOTOCASETYPENAME + " " + IDSDTYPEUSERNAME + ") " + CASEMT_SET_LS_NAMESTEP + ":" + (Char)13 + (Char)10 + ATTENTION_MESSAGE + (Char)13 + (Char)10 + OutStr;
                    DS_SDOPERATION_ATTENTION.DataSet.Next();
                }
            }
        }
    }
    finally {
        Param.Destroy();
    }
}
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.btn_MsgAten_Refresh_onClick = function (sender, e) {
    var _this = sender;
    _this.GETMsgAten(_this);
}
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.AtentionViewAll_OnCheckedChange = function (sender, e) {
    var _this = sender;
    _this.GETMsgAten(_this);
}
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.btn_MsgAten_Add_onClick = function (sender, e) {
    var _this = sender;
    if (_this.CheckStdCase(ItHelpCenter.SD.Atention.CaseAtentionCheckStd.TOrigen.MsgAtencion))
        return;
    if (_this.txtAtentionMessage.Text/*.Trim()*/ != "") {
        var SetSDOperationCase = new UsrCfg.SD.TSetSDOperationCase();
        SetSDOperationCase.SDOPERATION_ATTENTION.CASEMT_SET_LS_NAMESTEP = _this.SetTabContent().MDLIFESTATUS.NAMESTEP;
        SetSDOperationCase.SDOPERATION_ATTENTION.CASEMT_SET_LS_STATUSN = _this.SetTabContent().MDLIFESTATUS.STATUSN;
        SetSDOperationCase.SDOPERATION_ATTENTION.IDSDWHOTOCASE = _this.SDWHOTOCASE.IDSDWHOTOCASE;
        SetSDOperationCase.SDOPERATION_ATTENTION.IDSDOPERATION_ATTENTIONTYPE = UsrCfg.SD.Properties.TIDSDOPERATION_ATTENTIONTYPE._Message;
        SetSDOperationCase.SDOPERATION_ATTENTION.LS_IDSDCASESTATUS = _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASESTATUS;
        SetSDOperationCase.SDOPERATIONTYPES = UsrCfg.SD.Properties.TSDOPERATIONTYPES._ATTENTION;
        SetSDOperationCase.SDOPERATION_ATTENTION.ATTENTION_MESSAGE = _this.txtAtentionMessage.Text;
        _this.GETSetSDOperationCase(_this, SetSDOperationCase);
        _this.txtAtentionMessage.Text = "";
        _this.GETMsgAten(_this);
    }
}
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.GETMsgPost = function (inIDSDWHOTOCASE) {
    var _this = this.TParent();
    _this.DivMsgPost.innerHTML = "";
    var Param = new SysCfg.Stream.Properties.TParam();
    var Param1 = false;
    try {
        Param.Inicialize();
        //*********** SDOPERATION_ATTENTION_GET ***************************** 
        Param.AddInt32(UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASE.FieldName, _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        if (Param1)
            Param.AddUnknown(UsrCfg.InternoAtisNames.SDWHOTOCASE.DATELASTREAD.FieldName, UsrCfg.InternoAtisNames.SDOPERATION_POST.NAME_TABLE + "." + UsrCfg.InternoAtisNames.SDOPERATION_POST.MESSAGE_DATE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        else
            Param.AddDateTime(UsrCfg.InternoAtisNames.SDWHOTOCASE.DATELASTREAD.FieldName, SysCfg.DateTimeMethods.formatJSONDateTimetoDate(_this.SDWHOTOCASE.DATELASTREAD), SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        Param.AddInt32(UsrCfg.InternoAtisNames.SDOPERATION_POST.POST_IDSDTYPEUSER.FieldName, _this.SDWHOTOCASE.IDSDTYPEUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var DS_SDOPERATION_POST = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "GETSDOPERATION_POST", Param.ToBytes());
        if (DS_SDOPERATION_POST.ResErr.NotError) {
            if (DS_SDOPERATION_POST.DataSet.RecordCount > 0) {
                DS_SDOPERATION_POST.DataSet.First();
                var USERNAME = "";
                var IDSDOPERATION_POST = 0;
                var MESSAGE_DATE = SysCfg.DB.Properties.SVRNOW();
                var POST_MESSAGE = "";
                var IDSDTYPEUSER = UsrCfg.SD.Properties.TSDTypeUser.GetEnum(undefined);
                var IDSDWHOTOCASETYPE = UsrCfg.SD.Properties.TIDSDWHOTOCASETYPE.GetEnum(undefined);
                var IDSDTYPEUSERNAME = "";
                var IDSDWHOTOCASETYPENAME = "";

                var fechatemp = new Date();

                _this.DivMsgPost.style.marginTop = "7px";

                while (!(DS_SDOPERATION_POST.DataSet.Eof)) {
                    USERNAME = DS_SDOPERATION_POST.DataSet.RecordSet.FieldName("USERNAME").asString();
                    IDSDOPERATION_POST = DS_SDOPERATION_POST.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_POST.IDSDOPERATION_POST.FieldName).asInt32();
                    IDSDTYPEUSER = UsrCfg.SD.Properties.TSDTypeUser.GetEnum(DS_SDOPERATION_POST.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDTYPEUSER.FieldName).asInt32());
                    IDSDWHOTOCASETYPE = UsrCfg.SD.Properties.TIDSDWHOTOCASETYPE.GetEnum(DS_SDOPERATION_POST.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDWHOTOCASETYPE.FieldName).asInt32());
                    IDSDTYPEUSERNAME = IDSDTYPEUSER.name;
                    IDSDWHOTOCASETYPENAME = IDSDWHOTOCASETYPE.name;

                    MESSAGE_DATE = DS_SDOPERATION_POST.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_POST.MESSAGE_DATE.FieldName).asDateTime();
                    POST_MESSAGE = DS_SDOPERATION_POST.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_POST.POST_MESSAGE.FieldName).asString();

                    if (SysCfg.DateTimeMethods.DayOfYear(MESSAGE_DATE) != SysCfg.DateTimeMethods.DayOfYear(fechatemp)) {
                        var VclStackPanel = new TVclStackPanel(_this.DivMsgPost, "1", 1, [[12], [12], [12], [12], [12]]);
                        var lblFecha = new TVcllabel(VclStackPanel.Column[0].This, "", TlabelType.H0);
                        lblFecha.Text = SysCfg.DateTimeMethods.ToDateString(MESSAGE_DATE);
                        lblFecha.VCLType = TVCLType.BS;
                    }

                    var VclStackPanel = new TVclStackPanel(_this.DivMsgPost, "3", 3, [[3, 7, 2], [3, 7, 2], [3, 7, 2], [3, 7, 2], [3, 7, 2]]);
                    VclStackPanel.Row.This.style.marginBottom = "6px";

                    var VclStackPanel2 = new TVclStackPanel(VclStackPanel.Column[0].This, "2", 2, [[3, 9], [3, 9], [3, 9], [3, 9], [3, 9]]);

                    var imgImage = new TVclImagen(VclStackPanel2.Column[0].This, "");
                    imgImage.Src = "image/24/user-blue2.png";
                    imgImage.Title = USERNAME + "(" + IDSDWHOTOCASETYPENAME + " " + IDSDTYPEUSERNAME + ")" + " says:";
                    imgImage.This.style.marginTop = "-6px";

                    var lblUser = new TVcllabel(VclStackPanel2.Column[1].This, "", TlabelType.H0);
                    lblUser.This.style.marginLeft = "5px";
                    lblUser.This.style.fontSize = "13px";
                    lblUser.This.style.fontWeight = "normal";
                    lblUser.This.style.color = "rgb(128, 128, 128)";
                    lblUser.Text = USERNAME;

                    var lblMensaje = new TVcllabel(VclStackPanel.Column[1].This, "", TlabelType.H0);

                    lblMensaje.This.style.fontSize = "15px";
                    lblMensaje.This.style.fontWeight = "normal";
                    lblMensaje.Text = POST_MESSAGE;
                    lblMensaje.VCLType = TVCLType.BS;

                    var lblFecha = new TVcllabel(VclStackPanel.Column[2].This, "", TlabelType.H0);
                    lblFecha.This.style.fontSize = "15px";
                    lblFecha.This.style.fontWeight = "normal";
                    lblFecha.Text = SysCfg.DateTimeMethods.ToTimeString(MESSAGE_DATE);
                    lblFecha.VCLType = TVCLType.BS;


                    fechatemp = MESSAGE_DATE;

                    DS_SDOPERATION_POST.DataSet.Next();
                }

            }
        }
    }
    finally {
        Param.Destroy();
    }
    return new Array();
}
//.....Cambio de Step.........................
//---------------- TSDOPERATIONTYPES._CASESTATUS ---------------------------------------------------------  
//........Cambia inpreogres,pausa, resolve, cancel, close .....................
//........Cambia de modelo categoria prioridad sla, etc
//----------------TSDOPERATIONTYPES._PARENT:-----------------------------------------------------------------


//*************************  Relaciones del case sin actividad (suaves directas) ******************************************************        
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.BtnCIAfectado_Click_1 = function (sender, e) {
    var _this = sender;
    var Modal = new TVclModal(document.body, null, "");
    Modal.Width = "75%";
    Modal.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.Mythis, "Z2_label2"));
    Modal.Body.This.id = "ModalTfrSDCMDBCI" + _this.Id;
    _this.TfrSDCMDBCI = new ItHelpCenter.SD.Shared.Case_CMDBCI.TfrSDCMDBCI(_this.MenuObject, Modal.Body.This, Modal, function (sender2) {
        var _this2 = sender2;
        _this2.Initialize(_this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE, UsrCfg.Properties.UserATRole.User.IDCMDBUSER, _this.SDWHOTOCASE.IDSDTYPEUSER, function () {

        });
    });

    Modal.ShowModal();
    Modal.FunctionClose = function () {
    }
}
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.btnWorkArrown_Click = function (sender, e) {

    var _this = sender;


    if (_this.NumberWorkArrown > 0) {
        var Modal = new TVclModal(document.body, null, "IDModal");
        //Modal.Width = "60%";       

        Modal.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.Mythis, "ModalHeaderWorkAround"));
        Modal.Body.This.id = "Modal_SearchDataSet_" + _this.ID;
        var frSMKEWAConteiner = new ItHelpCenter.frSMKEWAConteiner(Modal.Body.This, _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE, _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDMDCATEGORYDETAIL, function () {
        });
        Modal.ShowModal();
        Modal.FunctionClose = function () {

        }
    }
    else {
        SysCfg.App.Methods.ShowMessage(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines44"));
    }

    //frSMKEWAConteiner TfrSMKEWAConteiner = new frSMKEWAConteiner();
    //TfrSMKEWAConteiner.Initialize(SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE, SDCONSOLEATENTION.SDCASEMTCOMPLTE.IDMDCATEGORYDETAIL);
    //TfrSMKEWAConteiner.Show();



}
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.BtnAdjuntos_Click_1 = function (sender, EventArgs) {
    var _this = sender;
    $(_this.DivsModal).css("display", "display");
    var Modal = new TVclModal(document.body, null, "");
    Modal.Width = "60%";
    Modal.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines45"));
    Modal.Body.This.id = "Modal_Attached_" + _this.ID;

    _this.TfrAttached = new ItHelpCenter.SD.Shared.Attached.UfrSDAttached(Modal.Body.This, "Attached", function () {

    }, _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE,
       _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDUSER,
       _this.SDWHOTOCASE.IDSDTYPEUSER,
       _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.MT_IDMDSERVICETYPE,
       false, _this.IDSDTypeUserListAll);

    Modal.ShowModal();
    Modal.FunctionClose = function () {
        $(_this.DivsModal).css("display", "none");
    }

}


//************* Actividades ********************************************************************        
//Actividad Refrescar
//Actividad Mensaje
//Actividad Ver
//Actividad Editar
//Actividad New
//Actividad Start Iniciar actividad 


//*****************  M   E  N  U  ***************************************************
//*****************  M   E  N  U  ***************************************************
//*****************  M   E  N  U  ***************************************************
/*ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.NavBarTimer_Information_Click = function (sender, EventArgs) {
    var _this = sender;
    alert(EventArgs.Text);
}*/
/*
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.btnDetail_Click = function (sender, EventArgs) {
    var _this = sender;
    alert(EventArgs.Text);
}*/
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.btnParentDetail_Click_1 = function (sender, EventArgs) {
    var _this = sender;
    alert(EventArgs.Text);
}
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.Btn_CATEGORYChange_Click = function (sender, EventArgs) {
    var _this = sender;
    //JAIMES 07/08/2018 
    //if (CheckStd(Atis.SD.Atention.CaseAtentionCheckStd.TOrigen.Status)) return;
    var MenuObjetNewCase = UsrCfg.Properties.MenuProfiler.NewPage(_this.MenuObject);
    _this.TfrSDCaseAtentionChangeSLA = new ItHelpCenter.SD.Shared.Atention.frSDCaseAtentionChangeSLA(MenuObjetNewCase, MenuObjetNewCase.ObjectDataMain, "ID", function (_THIS) {
        var SetSDOperationCase = new UsrCfg.SD.TSetSDOperationCase();
        SetSDOperationCase.SDOPERATIONTYPES = UsrCfg.SD.Properties.TSDOPERATIONTYPES._CASESTATUS;
        SetSDOperationCase.SDOPERATION_CASESTATUS.IDSDWHOTOCASE = _this.SDWHOTOCASE.IDSDWHOTOCASE;
        SetSDOperationCase.SDOPERATION_CASESTATUS.IDSDCASESTATUS = UsrCfg.SD.Properties.TSDCaseStatus._InsertMT;// inicia a un estado de partida             
        SetSDOperationCase.SDOPERATION_CASESTATUS.MT_IDMDMODELTYPED = _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.MT_IDMDMODELTYPED;
        SetSDOperationCase.SDOPERATION_CASESTATUS.IDMDMODELTYPED = _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.MT_IDMDMODELTYPED;
        SetSDOperationCase.SDOPERATION_CASESTATUS.IDSDCASE = _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.IDSDCASE;
        SetSDOperationCase.SDOPERATION_CASESTATUS.IDSDCASEMT = _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.IDSDCASEMT;
        SetSDOperationCase.SDOPERATION_CASESTATUS.IDMDCATEGORYDETAIL = _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.IDMDCATEGORYDETAIL;
        SetSDOperationCase.SDOPERATION_CASESTATUS.IDSLA = _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.IDSLA;
        SetSDOperationCase.SDOPERATION_CASESTATUS.IDMDIMPACT = _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.IDMDIMPACT;
        SetSDOperationCase.SDOPERATION_CASESTATUS.IDMDPRIORITY = _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.IDMDPRIORITY;
        SetSDOperationCase.SDOPERATION_CASESTATUS.IDMDURGENCY = _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.IDMDURGENCY;
        SetSDOperationCase.SDOPERATION_CASESTATUS.CASE_ISMAYOR = _this.SDCONSOLEATENTION.SDCASECOMPLTE.CASE_ISMAYOR;
        SetSDOperationCase.SDOPERATION_CASESTATUS.CASE_TITLE = _this.SDCONSOLEATENTION.SDCASECOMPLTE.CASE_TITLE;
        SetSDOperationCase.SDOPERATION_CASESTATUS.CASE_DESCRIPTION = _this.SDCONSOLEATENTION.SDCASECOMPLTE.CASE_DESCRIPTION;
        _this.TfrSDCaseAtentionChangeSLA.Initialize(
            SetSDOperationCase,
            _this.SDCONSOLEATENTION,
            UsrCfg.SD.Properties.TIDSDCASETYPE._NORMAL,
            _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDRESPONSETYPE,
            _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.CATEGORY,
            _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.CATEGORYNAME,
            _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDUSER,
            function (SetSDOperationCase, _ThisP, sender) {
                if (SetSDOperationCase != null) {
                    _ThisP.GETSetSDOperationCase(_ThisP, SetSDOperationCase);
                    sender.MenuObject.BackPageOnlyParent();
                }

                //ItHelpCenter.SD.CaseManager.Atention.OutSMConsoleManagerSwitch(_ThisP.Object, ItHelpCenter.SD.CaseManager.Atention.TResultform.Refresh, _ThisP.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE, _ThisP.SDCONSOLEATENTION.SDCASEMTCOMPLTE.IDSDCASEMT, _ThisP.SDWHOTOCASE.IDSDTYPEUSER, _ThisP.CallbackModalResult, _ThisP.MenuObject)
            }
        );
    }, _this);
}
/*ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.btnWorkArrown_Click = function (sender, EventArgs) {
    var _this = sender;
    alert(EventArgs.Text);
}*/
/*ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.btnUser_Click = function (sender, EventArgs) {
    var _this = sender;
    alert(EventArgs.Text);
}*/
/*ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.BtnFnEscFun_Click = function (sender, EventArgs) {
    var _this = sender;
    alert(EventArgs.Text);
}*/
/*ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.BtnFnEscHier_Click = function (sender, EventArgs) {
    var _this = sender;
    alert(EventArgs.Text);
}*/
/*ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.BtnFnChangeStd_Click = function (sender, EventArgs) {
    var _this = sender;
    alert(EventArgs.Text);
}*/
/*ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.BtnCIAfectado_Click_1 = function (sender, EventArgs) {
    var _this = sender;
    alert(EventArgs.Text);
}*/
/*ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.BtnAdjuntos_Click_1 = function (sender, EventArgs) {
    var _this = sender;
    alert(EventArgs.Text);
}*/
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.BtnRelatedCase_Click_1 = function (sender, EventArgs) {
    var _this = sender;
    try {
        var Modal = new TVclModal(document.body, null, "ModalLinkFile");
        Modal.Width = "80%";

        Modal.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnRelatedCase.Text"));
        Modal.Body.This.id = "BtnRelatedCase";
        _this.frSDRelatedCase = new ItHelpCenter.SD.Shared.RelatedCase.frSDRelatedCase(null, Modal.Body.This, "ModalLink" + _this.ID,
              function (myThis) {
                  Modal.CloseModal();
              },
             _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE,
             _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDUSER,
             _this.SDWHOTOCASE.IDSDTYPEUSER,
             _this
          );
        Modal.ShowModal();
        Modal.FunctionClose = function () {
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("frSDCaseNew.js ItHelpCenter.SD.CaseManager.NewCase.frSDCMCaseNew.prototype.BtnRelatedCase_Click", e);
    }
}

ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.BtnFnViewRelation_Click = function (sender, EventArgs) {
    var _this = sender;
    alert(EventArgs.Text);
}
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.BtnADDRealtion_Click = function (sender, EventArgs) {
    var _this = sender;
    alert(EventArgs.Text);
}
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.BtnDELRealtions_Click = function (sender, EventArgs) {
    var _this = sender;
    alert(EventArgs.Text);
}

ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.btnStepGraphics_onClick = function (sender, EventArgs) {
    var _this = sender;
    var Modal = new TVclModal(document.body, null, "IDModalAtentionDetailGraphic02");
    Modal.Width = "85%";
    Modal.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.Mythis, "Modal.AddHeaderContent"));
    Modal.Body.This.id = "Modal_AtentionDetailStatus02";
    for (var i = 0; i < _this.MDLIFESTATUSProfiler.MDLIFESTATUSList.length; i++) {
        var item = _this.MDLIFESTATUSProfiler.MDLIFESTATUSList[i];
        item.NEXTSTEP = item.NEXTSTEP.replace(" ", "").replace("-", ",").replace("(", "").replace(")", "");
    }
    var graphicStatusModel = new ItHelpCenter.MD.TUcGraphicStatusModel(document.getElementsByTagName("BODY")[0], "GraphicModelStatus02", _this.MDLIFESTATUSProfiler.MDLIFESTATUSList, _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.CASEMT_SET_LS_STATUSN, null);
    $(Modal.Body.This).append(graphicStatusModel.container.Row.This);
    Modal.ShowModal();
    Modal.FunctionClose = function () { }

}

