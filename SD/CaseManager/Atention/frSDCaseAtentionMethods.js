﻿ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.TCustomCodeLocal = function ()
{
    
    this.CUSTOMCODEStr = "001001(1)0";
    this.vVIEW = false;
    this.vADDACTION = false;
    this.vTRANSFER = false;
    this.btnRefreshAll = false;
    this.btnDetail = false;
    this.btnParentDetail = false;
    this.btnUser = false;

    //****Scale
    this.BtnFnEscFun = false;
    this.BtnFnEscHier = false;
    //****Antencion 
    this.btn_Msg_ViewPopup = false;
    this.btn_Msg_Add = false;
    this.btn_Msg_Verify = false;
    this.btn_Msg_Refresh = false;
    //*****Steep
    this.BtnViewChartStep = false;
    this.btn_MsgAten_Refresh = false;
    this.btn_MsgAten_ViewPopup = false;
    this.btn_MsgAten_Add = false;
    //*****Staus
    this.Btn_SaveRetun = false;
    this.BtnFnChangeStd = false;
    this.Btn_CATEGORYChange = false;
    //******Related
    this.BtnDELRealtions = false;
    this.BtnADDRealtion = false;
    this.BtnFnViewRelation = false;
    //******RelationsLigth
    this.BtnCIAfectado = false;
    this.BtnAdjuntos = false;
    this.BtnRelatedCase = false;
    this.btnWorkArrown = false;
    //******
    this.BtnRefreshMatrix = false;
    this.BtnFnSndMsg = false;
    this.BtnFnView = false;
    this.BtnFnEdit = false;
    this.BtnFnNew = false;
    this.BtnFnStart = false;
    
}
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.VisibletoBoolean = function (UnVisible)
{
    var _this = this.TParent();
    /*
    return (UnVisible == System.Windows.Visibility.Visible);
    */
}
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.BooleantoVisible = function (UnBoolean)
{
    var _this = this.TParent();
    /*
    if (UnBoolean) return System.Windows.Visibility.Visible;
    else return System.Windows.Visibility.Collapsed;
    */
}


ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.FillInterface = function () {
    var _this = this.TParent();

    _this.btnDetail.Visible = _this.InterfaceProfiler.GetInterfaceVisible(Interface.Properties.TPROPERTYNAME._ATNREP_Information_Case, Interface.Properties.TPROPERTYCLASS._Visible, _this.btnDetail.Visible);
    _this.btnParentDetail.Visible = _this.InterfaceProfiler.GetInterfaceVisible(Interface.Properties.TPROPERTYNAME._ATNREP_Information_Parent, Interface.Properties.TPROPERTYCLASS._Visible, _this.btnParentDetail.Visible);
    _this.Btn_CATEGORYChange.Visible = _this.InterfaceProfiler.GetInterfaceVisible(Interface.Properties.TPROPERTYNAME._ATNREP_Information_Category, Interface.Properties.TPROPERTYCLASS._Visible, _this.Btn_CATEGORYChange.Visible);
    _this.btnWorkArrown.Visible = _this.InterfaceProfiler.GetInterfaceVisible(Interface.Properties.TPROPERTYNAME._ATNREP_Information_WorkAround, Interface.Properties.TPROPERTYCLASS._Visible, _this.btnWorkArrown.Visible);
    _this.btnUser.Visible = _this.InterfaceProfiler.GetInterfaceVisible(Interface.Properties.TPROPERTYNAME._ATNREP_Information_User, Interface.Properties.TPROPERTYCLASS._Visible, _this.btnUser.Visible);
    _this.BtnFnEscFun.Visible = _this.InterfaceProfiler.GetInterfaceVisible(Interface.Properties.TPROPERTYNAME._ATNREP_StatusScale_Functional, Interface.Properties.TPROPERTYCLASS._Visible, _this.BtnFnEscFun.Visible);
    _this.BtnFnEscHier.Visible = _this.InterfaceProfiler.GetInterfaceVisible(Interface.Properties.TPROPERTYNAME._ATNREP_StatusScale_Hierarchical, Interface.Properties.TPROPERTYCLASS._Visible, _this.BtnFnEscHier.Visible);
    _this.BtnFnChangeStd.Visible = _this.InterfaceProfiler.GetInterfaceVisible(Interface.Properties.TPROPERTYNAME._ATNREP_StatusScale_Status, Interface.Properties.TPROPERTYCLASS._Visible, _this.BtnFnChangeStd.Visible);
    _this.BtnCIAfectado.Visible = _this.InterfaceProfiler.GetInterfaceVisible(Interface.Properties.TPROPERTYNAME._ATNREP_Link_CI, Interface.Properties.TPROPERTYCLASS._Visible, _this.BtnCIAfectado.Visible);
    _this.BtnAdjuntos.Visible = _this.InterfaceProfiler.GetInterfaceVisible(Interface.Properties.TPROPERTYNAME._ATNREP_Link_AttachFile, Interface.Properties.TPROPERTYCLASS._Visible, _this.BtnAdjuntos.Visible);
    _this.BtnRelatedCase.Visible = _this.InterfaceProfiler.GetInterfaceVisible(Interface.Properties.TPROPERTYNAME._ATNREP_Link_RelatedCases, Interface.Properties.TPROPERTYCLASS._Visible, _this.BtnRelatedCase.Visible);
    _this.BtnFnViewRelation.Visible = _this.InterfaceProfiler.GetInterfaceVisible(Interface.Properties.TPROPERTYNAME._ATNREP_AdminEqualCases_View, Interface.Properties.TPROPERTYCLASS._Visible, _this.BtnFnViewRelation.Visible);
    _this.BtnADDRealtion.Visible = _this.InterfaceProfiler.GetInterfaceVisible(Interface.Properties.TPROPERTYNAME._ATNREP_AdminEqualCases_Add, Interface.Properties.TPROPERTYCLASS._Visible, _this.BtnADDRealtion.Visible);
    _this.BtnDELRealtions.Visible = _this.InterfaceProfiler.GetInterfaceVisible(Interface.Properties.TPROPERTYNAME._ATNREP_AdminEqualCases_Delete, Interface.Properties.TPROPERTYCLASS._Visible, _this.BtnDELRealtions.Visible);
    //----_this.LayoutPanel_Description.Visible = _this.InterfaceProfiler.GetInterfaceVisible(Interface.Properties.TPROPERTYNAME._ATNREP_Panel_DescriptionCase, Interface.Properties.TPROPERTYCLASS._Visible, _this.LayoutPanel_Description.Visible);
    //----_this.LayoutPanel_ChartStep.Visible = _this.InterfaceProfiler.GetInterfaceVisible(Interface.Properties.TPROPERTYNAME._ATNREP_Panel_StepsDiagram, Interface.Properties.TPROPERTYCLASS._Visible, _this.LayoutPanel_ChartStep.Visible);
    //----_this.LayoutPanel_GuiaAyudaGeneral.Visible = _this.InterfaceProfiler.GetInterfaceVisible(Interface.Properties.TPROPERTYNAME._ATNREP_Panel_GeneralCaseGuide, Interface.Properties.TPROPERTYCLASS._Visible, _this.LayoutPanel_GuiaAyudaGeneral.Visible);
    //_this.LayoutPanel_Atention.Visible = _this.InterfaceProfiler.GetInterfaceVisible(Interface.Properties.TPROPERTYNAME._ATNREP_Panel_Atention, Interface.Properties.TPROPERTYCLASS._Visible, _this.LayoutPanel_Atention.Visible);
    //_this.LayoutPanel_Mensajes.Visible = _this.InterfaceProfiler.GetInterfaceVisible(Interface.Properties.TPROPERTYNAME._ATNREP_Panel_Messages, Interface.Properties.TPROPERTYCLASS._Visible, _this.LayoutPanel_Mensajes.Visible);
    //-----_this.LayoutPanel_MatrixOfActivities.Visible = _this.InterfaceProfiler.GetInterfaceVisible(Interface.Properties.TPROPERTYNAME._ATNREP_Panel_MatrixOfActivities, Interface.Properties.TPROPERTYCLASS._Visible, _this.LayoutPanel_MatrixOfActivities.Visible);
    //_this.LayoutPanel_Steps.Visible = _this.InterfaceProfiler.GetInterfaceVisible(Interface.Properties.TPROPERTYNAME._ATNREP_Panel_LifeStatusTab, Interface.Properties.TPROPERTYCLASS._Visible, _this.LayoutPanel_Steps.Visible);
    //_this.LayoutPanel_Status.Visible = _this.InterfaceProfiler.GetInterfaceVisible(Interface.Properties.TPROPERTYNAME._ATNREP_Panel_Status, Interface.Properties.TPROPERTYCLASS._Visible, _this.LayoutPanel_Status.Visible);
    //_this.LayoutPanel_StepSet.Visible = _this.InterfaceProfiler.GetInterfaceVisible(Interface.Properties.TPROPERTYNAME._ATNREP_Panel_StepEditor, Interface.Properties.TPROPERTYCLASS._Visible, _this.LayoutPanel_StepSet.Visible);


}







ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.CustomCodeAndCfgShow = function () {
    var _this = this.TParent();
    //img_refresh
    /*VclImagen*/_this.btnRefreshAll.Visible = (_this.CustomCodeLocal.btnRefreshAll && _this.btnRefreshAll.Visible && _this.CustomCodeLocal.vVIEW);//NO

    /*ToolBar_ButtonImg*/_this.btnDetail.Visible = (_this.CustomCodeLocal.btnDetail && _this.btnDetail.Visible && _this.CustomCodeLocal.vVIEW);//OK
    _this.btnParentDetail.Visible = (_this.CustomCodeLocal.btnParentDetail && _this.btnParentDetail.Visible && _this.CustomCodeLocal.vVIEW);
    /*ToolBar_ButtonImg*/_this.btnUser.Visible = (_this.CustomCodeLocal.btnUser && _this.btnUser.Visible && _this.CustomCodeLocal.btnUser);//OK

    //****Scale
    _this.BtnFnEscFun.Visible = (_this.CustomCodeLocal.BtnFnEscFun && _this.BtnFnEscFun.Visible && _this.CustomCodeLocal.vTRANSFER);
    _this.BtnFnEscHier.Visible = (_this.CustomCodeLocal.BtnFnEscHier && _this.BtnFnEscHier.Visible && _this.CustomCodeLocal.vTRANSFER);

    //****Antencion
    _this.btn_Msg_ViewPopup.Visible = (_this.CustomCodeLocal.btn_Msg_ViewPopup && _this.btn_Msg_ViewPopup.Visible && _this.CustomCodeLocal.vVIEW);//OK
    _this.btn_Msg_Add.Visible = (_this.CustomCodeLocal.btn_Msg_Add && _this.btn_Msg_Add.Visible && _this.CustomCodeLocal.vVIEW);//OK
    //_this.btn_Msg_Verify.Visible = (_this.CustomCodeLocal.btn_Msg_Verify && _this.btn_Msg_Verify.Visible && _this.CustomCodeLocal.vVIEW);
    _this.btn_Msg_Refresh.Visible = (_this.CustomCodeLocal.btn_Msg_Refresh && _this.btn_Msg_Refresh.Visible && _this.CustomCodeLocal.vVIEW);//OK

    //*****Steep
    //_this.BtnViewChartStep.Visible = (_this.CustomCodeLocal.BtnViewChartStep && _this.BtnViewChartStep.Visible && _this.CustomCodeLocal.vADDACTION);
    /*ToolBar_ButtonImg(New)*///_this.ToolBar_ButtonImg_MsgAten_View.visible = (_this.CustomCodeLocal.btn_MsgAten_Refresh && _this.btn_MsgAten_Refresh.Visible && _this.CustomCodeLocal.vVIEW);
    /*TVclImagen*/_this.btn_MsgAten_Refresh.Visible = (_this.CustomCodeLocal.btn_MsgAten_Refresh && _this.btn_MsgAten_Refresh.Visible && _this.CustomCodeLocal.vVIEW);
    /*TVclInputcheckbox*/_this.AtentionViewAll.Visible = (_this.CustomCodeLocal.btn_MsgAten_ViewPopup && _this.AtentionViewAll.Visible && _this.CustomCodeLocal.vVIEW);
    /*TVclButton*/_this.btn_MsgAten_Add.Visible = (_this.CustomCodeLocal.btn_MsgAten_Add && _this.btn_MsgAten_Add.Visible && _this.CustomCodeLocal.vADDACTION);//_this.CustomCodeLocal.vADDACTION//antes isaac
    ///*TVclButton*/Div_MsgAten_add = (_this.CustomCodeLocal.btn_MsgAten_Add && _this.btn_MsgAten_Add.Visible && _this.CustomCodeLocal.vADDACTION);//_this.CustomCodeLocal.vADDACTION//antes isaac
    _this.spAtentionMsg1_3.Visible = (_this.CustomCodeLocal.btn_MsgAten_Add && _this.btn_MsgAten_Add.Visible && _this.CustomCodeLocal.vADDACTION);
    //*****Staus
    //_this.Btn_SaveRetun.Visible = (_this.CustomCodeLocal.Btn_SaveRetun && _this.Btn_SaveRetun.Visible && _this.CustomCodeLocal.vTRANSFER);
    //_this.GridRetun.Visible = Btn_SaveRetun.Visible;
    _this.BtnFnChangeStd.Visible = (_this.CustomCodeLocal.BtnFnChangeStd && _this.BtnFnChangeStd.Visible && _this.CustomCodeLocal.vVIEW && _this.CustomCodeLocal.vTRANSFER && ((_this.SDWHOTOCASE.IDSDTYPEUSER == 1) || ((_this.SDWHOTOCASE.IDSDTYPEUSER == 2) || (_this.SDWHOTOCASE.IDSDTYPEUSER == 3))));//Isaac
    // _this.BtnFnChangeStd.Enabled = (_this.CustomCodeLocal.vTRANSFER);

    _this.Btn_CATEGORYChange.Visible = (_this.CustomCodeLocal.Btn_CATEGORYChange && _this.Btn_CATEGORYChange.Visible && _this.CustomCodeLocal.vTRANSFER);

    //******Related
    _this.BtnDELRealtions.Visible = (_this.CustomCodeLocal.BtnDELRealtions && _this.BtnDELRealtions.Visible && _this.CustomCodeLocal.vADDACTION);
    _this.BtnADDRealtion.Visible = (_this.CustomCodeLocal.BtnADDRealtion && _this.BtnADDRealtion.Visible && _this.CustomCodeLocal.vADDACTION);
    _this.BtnFnViewRelation.Visible = (_this.CustomCodeLocal.BtnFnViewRelation && _this.BtnFnViewRelation.Visible && _this.CustomCodeLocal.vADDACTION);

    //******RelationsLigth
    _this.BtnCIAfectado.Visible = (_this.CustomCodeLocal.BtnCIAfectado && _this.BtnCIAfectado.Visible && _this.CustomCodeLocal.vADDACTION);
    _this.BtnCIAfectado.Visible = (_this.CustomCodeLocal.BtnCIAfectado && _this.BtnCIAfectado.Visible && _this.CustomCodeLocal.vADDACTION);
    _this.BtnAdjuntos.Visible = (_this.CustomCodeLocal.BtnAdjuntos && _this.BtnAdjuntos.Visible && _this.CustomCodeLocal.vADDACTION);
    _this.BtnRelatedCase.Visible = (_this.CustomCodeLocal.BtnRelatedCase && _this.BtnRelatedCase.Visible && _this.CustomCodeLocal.vADDACTION);
    _this.btnWorkArrown.Visible = (_this.CustomCodeLocal.btnWorkArrown && _this.btnWorkArrown.Visible && _this.CustomCodeLocal.vADDACTION);//OK

    //******
    //_this.BtnRefreshMatrix.Visible = (_this.CustomCodeLocal.BtnRefreshMatrix && _this.BtnRefreshMatrix.Visible && _this.CustomCodeLocal.vADDACTION);
    //_this.BtnFnSndMsg.Visible = (_this.CustomCodeLocal.BtnFnSndMsg && _this.BtnFnSndMsg.Visible && _this.CustomCodeLocal.vADDACTION);
    //_this.BtnFnView.Visible = (_this.CustomCodeLocal.BtnFnView && _this.BtnFnView.Visible && _this.CustomCodeLocal.vVIEW);
    //_this.BtnFnEdit.Visible = (_this.CustomCodeLocal.BtnFnEdit && _this.BtnFnEdit.Visible && _this.CustomCodeLocal.vADDACTION);
    //_this.BtnFnNew.Visible = (_this.CustomCodeLocal.BtnFnNew && _this.BtnFnNew.Visible && _this.CustomCodeLocal.vADDACTION);
    //_this.BtnFnStart.Visible = (_this.CustomCodeLocal.BtnFnStart && _this.BtnFnStart.Visible && _this.CustomCodeLocal.vADDACTION);

}
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.CustomCodeSetVisibility = function (UnVisibility)
{
    var _this = this.TParent();

    /*
    _this.CustomCodeLocal.vVIEW = UnVisibility;
    _this.CustomCodeLocal.vADDACTION = UnVisibility;
    _this.CustomCodeLocal.vTRANSFER = UnVisibility;
    */
    _this.CustomCodeLocal.btnRefreshAll = UnVisibility;
    _this.CustomCodeLocal.btnDetail = UnVisibility;
    _this.CustomCodeLocal.btnParentDetail = UnVisibility;
    _this.CustomCodeLocal.btnUser = UnVisibility;
            
    //****Scale
    _this.CustomCodeLocal.BtnFnEscFun = UnVisibility;
    _this.CustomCodeLocal.BtnFnEscHier = UnVisibility;
    //****Antencion 
    _this.CustomCodeLocal.btn_Msg_ViewPopup = UnVisibility;
    _this.CustomCodeLocal.btn_Msg_Add = UnVisibility;
    _this.CustomCodeLocal.btn_Msg_Verify = UnVisibility;
    _this.CustomCodeLocal.btn_Msg_Refresh = UnVisibility;
    //*****Steep
    _this.CustomCodeLocal.BtnViewChartStep = UnVisibility;
    _this.CustomCodeLocal.btn_MsgAten_Refresh = UnVisibility;
    _this.CustomCodeLocal.btn_MsgAten_ViewPopup = UnVisibility;
    _this.CustomCodeLocal.btn_MsgAten_Add = UnVisibility;
    //*****Staus
    _this.CustomCodeLocal.Btn_SaveRetun = UnVisibility;
    _this.CustomCodeLocal.BtnFnChangeStd = UnVisibility;
    _this.CustomCodeLocal.Btn_CATEGORYChange = UnVisibility;
    //******Related
    _this.CustomCodeLocal.BtnDELRealtions = UnVisibility;
    _this.CustomCodeLocal.BtnADDRealtion = UnVisibility;
    _this.CustomCodeLocal.BtnFnViewRelation = UnVisibility;
    //******RelationsLigth
    _this.CustomCodeLocal.BtnCIAfectado = UnVisibility;
    _this.CustomCodeLocal.BtnAdjuntos = UnVisibility;
    _this.CustomCodeLocal.BtnRelatedCase = UnVisibility;
    _this.CustomCodeLocal.btnWorkArrown = UnVisibility;
    //******
    _this.CustomCodeLocal.BtnRefreshMatrix = UnVisibility;
    _this.CustomCodeLocal.BtnFnSndMsg = UnVisibility;
    _this.CustomCodeLocal.BtnFnView = UnVisibility;
    _this.CustomCodeLocal.BtnFnEdit = UnVisibility;
    _this.CustomCodeLocal.BtnFnNew = UnVisibility;
    _this.CustomCodeLocal.BtnFnStart = UnVisibility;
}
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.TaskClose = function(sender, promt)
{
    var _this = sender;
    //_this.CallbackModalResult(_this, ItHelpCenter.SD.CaseManager.Atention.TResultform.GotoMain);

    ItHelpCenter.SD.CaseManager.Atention.OutSMConsoleManagerSwitch(_this.Object, ItHelpCenter.SD.CaseManager.Atention.TResultform.GotoMain, _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE, _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.IDSDCASEMT, _this.SDWHOTOCASE.IDSDTYPEUSER, _this.CallbackModalResult, _this.MenuObject);
}
ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.UpdateTimerCaseAtention = function (sender) {
    var _this = sender;
    var Nro = 0;
    if (_this.Counter_MAXTIME <= 0)
    {
        _this.GroupBarTimer_Maxtime.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "GroupBarTimer_Maxtime.Text") + "00:00:00";
        Nro += 1;
    }
    else
        _this.GroupBarTimer_Maxtime.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "GroupBarTimer_Maxtime.Text") + SysCfg.DateTimeMethods.ConvertTimeStampToHours(_this.Counter_MAXTIME.toString());

    //if (_this.Counter_NORMALTIME <= 0)
    //{ _this.GroupBarTimer_MaxtimeDate.Text = "Normal 00:00:00"; Nro += 1; }
    //else
    //    _this.GroupBarTimer_MaxtimeDate.Text = "Normal " + SysCfg.DateTimeMethods.ConvertTimeStampToHours(_this.Counter_NORMALTIME.toString());


    if (_this.Counter_MAXTIME <= 0)
    { _this.VclTimer_Level.Text = "00:00:00"; Nro += 1; }
    else
        _this.VclTimer_Level.Text = SysCfg.DateTimeMethods.ConvertTimeStampToHours(_this.Counter_LEVELTIME.toString());

    if (Nro == 3)
        _this.TimerTemp.RemoveMethod("UpdateTimerCaseAtention");
    else {
        _this.Counter_MAXTIME -= 1000;
        _this.Counter_NORMALTIME -= 1000;
        _this.Counter_LEVELTIME -= 1000;
    }
}




