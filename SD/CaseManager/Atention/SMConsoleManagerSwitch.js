﻿
//bibliografia 
// https://www.w3schools.com/html/html5_semantic_elements.asp
// https://developer.mozilla.org/en-US/docs/Web/API/Document/createElement

ItHelpCenter.SD.CaseManager.Atention.TResultform = {
    GotoMain: { value: 0, name: "GotoMain" },
    Refresh: { value: 1, name: "Refresh" },
};



ItHelpCenter.SD.CaseManager.Atention.OutSMConsoleManagerSwitch = function (ElementDiv, Resultform, IDSDCASE, IDSDCASEMT, IDSDTYPEUSER, CallbackModalResult, MenuObject) {
    var isMenu = (MenuObject != null);


    if (Resultform == ItHelpCenter.SD.CaseManager.Atention.TResultform.Refresh) {
        ElementDiv.innerHTML = "";
        var SMConsoleManagerSwitch = new ItHelpCenter.SD.CaseManager.Atention.TSMConsoleManagerSwitch(
            MenuObject,
            ElementDiv,
            //CallbackModalResult,
            function (_this) {
                CallbackModalResult();
            },
            IDSDCASE,
            IDSDCASEMT,
            IDSDTYPEUSER
            );
    } else if (Resultform == ItHelpCenter.SD.CaseManager.Atention.TResultform.GotoMain) {

        //Carlos 
        
        if (isMenu) MenuObject.BackPageOnlyParent();
        else CallbackModalResult();

        //return Resultform;        
        //Continua = false;
    }
    else {
        return Resultform;
    }
}


ItHelpCenter.SD.CaseManager.Atention.TSMConsoleManagerSwitch = function (inMenuObject, inObject, incallbackModalResult, inIDSDCASE, inIDSDCASEMT, inIDSDTYPEUSER) {
    this.TParent = function () {
        return this;
    }.bind(this);

    var _this = this.TParent();
    this.isMenu = (inMenuObject != null);
    this.Object = inObject;
    this.MenuObject = inMenuObject


   // this.Object.innerHTML = "";
    this.CallbackModalResult = incallbackModalResult;//CallbackModalResult(OutStr);        
    this.IDSDCASE = inIDSDCASE;
    this.IDSDCASEMT = inIDSDCASEMT;
    this.IDSDTYPEUSER = inIDSDTYPEUSER;
    if (_this.IDSDCASE == -1) {
        _this.loadPendingcases();
    }
    else {
       _this.GetSDConsoleAtentionStartComplete(_this.IDSDCASE, _this.IDSDCASEMT, _this.IDSDTYPEUSER);
    }

    

}
ItHelpCenter.SD.CaseManager.Atention.TSMConsoleManagerSwitch.prototype.loadPendingcases = function () {
    var _this = this.TParent();

    this.Mythis = "TDynamicSTEFAtentionCase_manager";
     
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Uh2_Header", "Before entering the system, there are some outstanding, please resolve them");

    var PathCSS = ItHelpCenter.ScriptManager.GetStyle("SD/CaseManager/Atention/CaseAtention.css");
    //Creamos un Id para el Css
    var IDparthCss = "TDynamicSTEFAtentionCase_manager";
    //Cargamos el css pasando el Path que recibimos y el Id creado
    ItHelpCenter.ScriptManager.AddStyleFile(PathCSS, IDparthCss, function () {
        //Continuamos con las funciones 
        $.ajax({

            type: "POST", url: SysCfg.App.Properties.xRaiz + 'Service/SD/Console.svc/SDCASE_GET_ConsoleManager', data: JSON.stringify({ IDSDCASE: parseInt(_this.IDSDCASE) }), contentType: "application/json; charset=utf-8", dataType: "json",
            success: function (response) {


                /*-1=pendig,0=Case list>0= el numero del caso segin el id */
                //var Body = _this.Object;//new TVclBody($("body"), "idTag");
                //************  Tagheader ************************************
                var TagHeadContDef = new Array(1);
                var TagHeadCont = VclDivitions(_this.Object, "TagHeadCont", TagHeadContDef);
                BootStrapContainer(TagHeadCont[0].This, TBootStrapContainerType.none);
                var TagHeadContRowDef = new Array(1);
                var TagHeadContRow = VclDivitions(TagHeadCont[0].This, "TagHeadContRow", TagHeadContDef);
                BootStrapCreateRow(TagHeadContRow[0].This);
                var TagHeadContColDef = new Array(2);
                var TagHeadContCol = VclDivitions(TagHeadContRow[0].This, "TagHeadContCol", TagHeadContColDef);
                BootStrapCreateColumns(TagHeadContCol, [[3, 9], [3, 9], [3, 9], [3, 9], [3, 9]]);

                Uimg(TagHeadContCol[0].This, "logo", "image/High/IT-Help-Center-Final file.png", "");
                Vcllabel(TagHeadContCol[1].This, "", TVCLType.BS, TlabelType.H2,  UsrCfg.Traslate.GetLangText(_this.Mythis, "Uh2_Header"));

                var contCases = 0
                var unShowLogin = false;
                for (var i = 0; i < response.length; i++) {
                    var code = "";
                    var IDSDWHOTOCASE = _this.GetIDSDWHOTOCASEPendingcases(response[i].IDSDCASE, response[i].IDSDCASEMT, _this.IDSDTYPEUSER);
                    if (IDSDWHOTOCASE > 0) {
                        var Atention = _this.GetSDConsoleAtentionStart(IDSDWHOTOCASE);
                        if (Atention != undefined) {
                            if (_this.ShowSDConsoleAtentionStartPending(Atention)); unShowLogin = true;
                        }
                    }
                }

                if (!unShowLogin) {
                    ItHelpCenter.SD.CaseManager.Atention.OutSMConsoleManagerSwitch(_this.Object, ItHelpCenter.SD.CaseManager.Atention.TResultform.GotoMain, _this.IDSDCASE, _this.IDSDCASEMT, _this.IDSDTYPEUSER, _this.CallbackModalResult, _this.MenuObject)
                }

                UsrCfg.WaitMe.CloseWaitme();


            },
            error: function (response) {
                SysCfg.Log.Methods.WriteLog("SMConsoleManagerSwitch.js ItHelpCenter.SD.CaseManager.Atention.TSMConsoleManagerSwitch.prototype.loadPendingcases Service/SD/Console.svc/SDCASE_GET_ConsoleManager" + "Error no llamo al servicio");
            }
        });

    }, function (e) {
    });




}

ItHelpCenter.SD.CaseManager.Atention.TSMConsoleManagerSwitch.prototype.ShowSDConsoleAtentionStartPending = function (Atention) {
    var _this = this.TParent();
    if (Atention.ResultSDConsoleAtentionStart == UsrCfg.SD.Properties.TResultSDConsoleAtentionStart._IncomeAccepted.value) {
        //********************     Verifica si se debe ocultar  *********************************************************
        var HayShowLogin = false;
        var HayHide = false;
        for (var Counter = 0; Counter < Atention.MDLIFESTATUSProfiler.MDLIFESTATUSList.length; Counter++)//Steps
        {
            if (Atention.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter].STATUSN == Atention.SDCONSOLEATENTION.SDCASEMTCOMPLTE.CASEMT_SET_LS_STATUSN) {
                for (var i = 0; i < Atention.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter].MDLIFESTATUSTYPEUSERList.length ; i++) {
                    if (Atention.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter].MDLIFESTATUSTYPEUSERList[i].IDSDTYPEUSER == Atention.SDWHOTOCASE.IDSDTYPEUSER) {
                        //if (_this.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter].MDLIFESTATUSTYPEUSERList[i].IDMDLIFESTATUSBEHAVIOR == 1) {
                        if (Atention.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter].MDLIFESTATUSTYPEUSERList[i].HERESTEP_CONFIG.Hide) {
                            HayHide = true; //es calificacion   
                        }
                        else {
                            if (Atention.MDLIFESTATUSProfiler.MDLIFESTATUSList[Counter].MDLIFESTATUSTYPEUSERList[i].HERESTEP_CONFIG.ShowLogin) {
                                HayShowLogin = true; //es calificacion   
                                contCases += 1;
                            }
                        }
                    }
                }
            }
        }
        if (Atention.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE == -1) {
        }
        else if (Atention.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE == 0) HayShowLogin = true;
        else if (Atention.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE == Atention.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE) HayShowLogin = true;
        else HayHide = true;
        if (HayHide) return;
        //if (HayShowLogin == false) return;
        var spCase = new TVclStackPanel(this.Object, "", 1, [[12], [12], [12], [12], [12]]);
        var DivCase = spCase.Column[0].This
        if (HayShowLogin) {
            var frSDCaseAtention = new ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention(
            DivCase,
            _this.CallbackModalResult,
            Atention,
            _this.MenuObject
            );
            return true;
        }
    }
    return false
}

ItHelpCenter.SD.CaseManager.Atention.TSMConsoleManagerSwitch.prototype.GetSDConsoleAtentionStartComplete = function (inIDSDCASE, inIDSDCASEMT, inIDSDTYPEUSER) {
    var _this = this.TParent();
    var parameters =
    {      
        IDSDCASE: inIDSDCASE,
        IDSDCASEMT: inIDSDCASEMT,
        IDSDTYPEUSER: inIDSDTYPEUSER
    };
    parameters = JSON.stringify(parameters);
    $.ajax({
        type: "POST",
        url: SysCfg.App.Properties.xRaiz + 'Service/SD/Atention.svc/SDConsoleAtentionStartComplete',
        data: parameters,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (response) {
            //***************************************************************************************************
            var Atention = response;
            if (Atention != undefined) {
                if (Atention.ResErr.NotError == true) {
                    if (Atention.ResultSDConsoleAtentionStart == UsrCfg.SD.Properties.TResultSDConsoleAtentionStart._IncomeAccepted.value) {
                        var frSDCaseAtention = new ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention(
                        _this.Object,
                        _this.CallbackModalResult,
                        Atention,
                       _this.MenuObject
                            
                        );
                    }
                    else if (Atention.ResultSDConsoleAtentionStart == UsrCfg.SD.Properties.TResultSDConsoleAtentionStart._IncomeCreate.value)////DifMAJS
                    {
                        var MenuObjetNewCase = _this.MenuObject;// UsrCfg.Properties.MenuProfiler.NewPage(_this.MenuObject);
                        $(_this.Object).html("");
                        try {
                            var Newcase = new ItHelpCenter.SD.CaseManager.NewCase.frSDCMCaseNew(MenuObjetNewCase, _this.Object, function () {
                                Newcase.ReInitialize(Atention, function (thisOut, ResultSDCaseChangeStatus, IDSDCASE, IDSDCASEMT, IDSDTYPEUSER) {
                                    if (thisOut.chkShowConsole.Checked) {

                                        if (ResultSDCaseChangeStatus == UsrCfg.SD.Properties.TResultSDCaseChangeStatus._CSSuccessfullyCreate) {
                                            _this.MenuObject.ObjectData.innerHTML = "";
                                            _this.MenuObject._ObjectDataMain.innerHTML = "";
                                            _this.MenuObject.OnBeforeBackPage = null;
                                            ItHelpCenter.SD.CaseManager.Atention.OutSMConsoleManagerSwitch(_this.Object, ItHelpCenter.SD.CaseManager.Atention.TResultform.Refresh, IDSDCASE, IDSDCASEMT, IDSDTYPEUSER.value, _this.CallbackModalResult, _this.MenuObject);
                                        }
                                        else {
                                            ItHelpCenter.SD.CaseManager.Atention.OutSMConsoleManagerSwitch(_this.Object, ItHelpCenter.SD.CaseManager.Atention.TResultform.GotoMain, IDSDCASE, IDSDCASEMT, IDSDTYPEUSER.value, _this.CallbackModalResult, _this.MenuObject);
                                        }
                                    }
                                    else {
                                        //_this.MenuObject.BackPageOnlyParent(_this);
                                        ItHelpCenter.SD.CaseManager.Atention.OutSMConsoleManagerSwitch(_this.Object, ItHelpCenter.SD.CaseManager.Atention.TResultform.GotoMain, IDSDCASE, IDSDCASEMT, IDSDTYPEUSER.value, _this.CallbackModalResult, _this.MenuObject)
                                    }
                                });
                            });
                        } catch (e) {
                           SysCfg.Log.Methods.WriteLog("",e);
                        }

                        //_This.MenuObjet.OnAfterBackPage = function (sender, e) {
                            
                        //};

                        //frSDCaseNew.ReInitialize(Atention.SDConsoleAtentionStart, false);
                    }
                }
                else {

                    ItHelpCenter.SD.CaseManager.Atention.OutSMConsoleManagerSwitch(_this.Object, ItHelpCenter.SD.CaseManager.Atention.TResultform.GotoMain, _this.IDSDCASE, _this.IDSDCASEMT, _this.IDSDTYPEUSER, _this.CallbackModalResult, _this.MenuObject)
                }


            }
            else {

                ItHelpCenter.SD.CaseManager.Atention.OutSMConsoleManagerSwitch(_this.Object, ItHelpCenter.SD.CaseManager.Atention.TResultform.GotoMain, _this.IDSDCASE, _this.IDSDCASEMT, _this.IDSDTYPEUSER, _this.CallbackModalResult, _this.MenuObject)
            }

            UsrCfg.WaitMe.CloseWaitme();
        },
        error: function (response) {

            SysCfg.Log.Methods.WriteLog("SMConsoleManagerSwitch.js ItHelpCenter.SD.CaseManager.Atention.TSMConsoleManagerSwitch.prototype.GetSDConsoleAtentionStartComplete Service/SD/Atention.svc/SDConsoleAtentionStartComplete " + "Error no llamo al Servicio [SDConsoleAtentionStart]");
           
        }
    });
   
}




ItHelpCenter.SD.CaseManager.Atention.TSMConsoleManagerSwitch.prototype.GetSDConsoleAtentionStart = function (inIDSDWHOTOCASE) {
    var ret = undefined;
    var parameters =
    {
        Reload: "true",
        IDSDWHOTOCASE: inIDSDWHOTOCASE
    };
    parameters = JSON.stringify(parameters);
    $.ajax({
        type: "POST",
        url: SysCfg.App.Properties.xRaiz + 'Service/SD/Atention.svc/SDConsoleAtentionStart',
        data: parameters,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (response) {
            //***************************************************************************************************
            ret = response;
        },
        error: function (response) {
            SysCfg.Log.Methods.WriteLog("SMConsoleManagerSwitch.js ItHelpCenter.SD.CaseManager.Atention.TSMConsoleManagerSwitch.prototype.GetSDConsoleAtentionStart Service/SD/Atention.svc/SDConsoleAtentionStart " + "Error no llamo al Servicio [SDConsoleAtentionStart]");
        }
    });
    return ret;
    
}


ItHelpCenter.SD.CaseManager.Atention.TSMConsoleManagerSwitch.prototype.GetIDSDWHOTOCASEPendingcases = function (inIDSDCASE, inIDSDCASEMT, inIDSDTYPEUSER) {
    
    var _this = this.TParent();
    var parameters =
    {
        IDSDCASE: inIDSDCASE,
        IDSDCASEMT: inIDSDCASEMT,
        IDSDTYPEUSER: inIDSDTYPEUSER
    };
    var resp = -1;
    parameters = JSON.stringify(parameters)
    $.ajax({
        type: "POST",
        url: SysCfg.App.Properties.xRaiz + 'Service/SD/Console.svc/UserAtentionStart',
        data: parameters,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (response) {
            resp = response;
        },
        error: function (response) {
            SysCfg.Log.Methods.WriteLog("SMConsoleManagerSwitch.js ItHelpCenter.SD.CaseManager.Atention.TSMConsoleManagerSwitch.prototype.GetIDSDWHOTOCASEPendingcases Service/SD/Console.svc/UserAtentionStart" + "Error no llamo al servicio [UserAtentionStart]");
        }
    });
    return resp;
}







/*
TDynamicSTEFAtentionCase.prototype.InitializeAtentionCase = function(idcase) {
    var _this = this.TParent();
    $.ajax({
        type: "POST", url: SysCfg.App.Properties.xRaiz + 'Service/SD/Console.svc/SDCASE_GET', data: '{}', contentType: "application/json; charset=utf-8", dataType: "json",
        success: function (response) {

            _this.loadPendingcases(response, idcase);
        },
        error: function (response) {
            alert("Error no llamo al servicio");
        }
    });
}*/


