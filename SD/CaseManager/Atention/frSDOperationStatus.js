ItHelpCenter.SD.frSDOperationStatus = function (inObjectHtml, _this) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.ObjectHtml = null;
    this.IsModal = false;
    this.Modal = new TVclModal(document.body, null, "");
    this.Modal.Body.This.innerHTML = "";
    this.ObjectHtml = this.Modal.Body.This;
    this.IsModal = true;
    //if (inObjectHtml == undefined || inObjectHtml == null || inObjectHtml == "") {
    //    this.ObjectHtml = this.Modal.Body.This;
    //    this.IsModal = true;
    //}
    //else {
    //    inObjectHtml.innerHTML = "";
    //    this.Modal.HeaderContainer.innerHTML = "";
    //    this.Modal.FooterContainer.innerHTML = "";
    //    $(inObjectHtml).html(this.Modal.Content.This);
    //    this.ObjectHtml = this.Modal.Body.This;
    //    this.IsModal = false;
    //}
    
    //this.SetSDOperationCase = new UsrCfg.SD.TSetSDOperationCase();
    this.ModalResult = null;
    this.isGridRetun = false;
    this.frSDCAseAtention = null;
    this.name = "frSDOperationStatus";
    this.InicializeComponent();
    
    //<Lines><numerolinea> es para textos de mensages,alertas ext
    //UsrCfg.Traslate.GetLangText(this.name, "Lines1", "");
    UsrCfg.Traslate.GetLangText(_this.name, "Line1", "Did you verify that all the final data of the record is correct? (Example: Category, Priority, is major)");
    //<Nombredelcomponente>.<Porpiedad> 
    UsrCfg.Traslate.GetLangText(_this.name, "LblStatus.Text", "Label");
    UsrCfg.Traslate.GetLangText(_this.name, "LblDescripcion.Text", "Label");
    UsrCfg.Traslate.GetLangText(_this.name, "Lbl_CASE_RETURN_COST.Text", "Result: ");
    UsrCfg.Traslate.GetLangText(_this.name, "Lbl_CASE_RETURN_STR.Text", "Cost Return: ");
    //UsrCfg.Traslate.GetLangText(_this.name, "VCLbuttonSave.Text", "Save");
    //UsrCfg.Traslate.Form(_this.name, _this.Object);

}.Implements(new Source.Page.Properties.PageModal())

ItHelpCenter.SD.frSDOperationStatus.prototype.InicializeComponent = function () {
    var _this = this.TParent();

    _this.Modal.Width = "21%";
    var spPrincipal = new TVclStackPanel(_this.ObjectHtml, "", 1, [[12], [12], [12], [12], [12]]);

    var spLblStatus = new TVclStackPanel(spPrincipal.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);

    _this.LblStatus = new TVcllabel(spLblStatus.Column[0].This, "", TlabelType.H0);
    _this.LblStatus.Text = UsrCfg.Traslate.GetLangText(_this.name, "LblStatus.Text");
    _this.LblStatus.VCLType = TVCLType.BS;

    var spCmbStatus = new TVclStackPanel(spPrincipal.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
    _this.CmbStatus = new TVclComboBox2(spCmbStatus.Column[0].This, "");
    _this.CmbStatus.This.classList.add("form-control");
    _this.CmbStatus.onChange = function () {
        _this.CmbStatus_SelectedChanged(_this, _this.CmbStatus);
    }
    var spLblDescripcion = new TVclStackPanel(spPrincipal.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);

    _this.LblDescripcion = new TVcllabel(spLblDescripcion.Column[0].This, "", TlabelType.H0);
    _this.LblDescripcion.Text = UsrCfg.Traslate.GetLangText(_this.name, "LblDescripcion.Text");
    _this.LblDescripcion.VCLType = TVCLType.BS;

    var spTxtDescripcion = new TVclStackPanel(spPrincipal.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);

    _this.TxtDescripcion = new TVclMemo(spTxtDescripcion.Column[0].This, "");
    _this.TxtDescripcion.Text = "";
    _this.TxtDescripcion.VCLType = TVCLType.BS;
    _this.TxtDescripcion.This.classList.add("form-control");
    _this.TxtDescripcion.Rows = 5;
    _this.TxtDescripcion.Cols = 50;

    _this.GridRetun = new TVclStackPanel(_this.ObjectHtml, "", 1, [[12], [12], [12], [12], [12]]);

    var spSecundario1 = new TVclStackPanel(_this.GridRetun.Column[0].This, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [4, 8]]);

    _this.Lbl_CASE_RETURN_COST = new TVcllabel(spSecundario1.Column[0].This, "", TlabelType.H0);
    _this.Lbl_CASE_RETURN_COST.Text = UsrCfg.Traslate.GetLangText(_this.name, "Lbl_CASE_RETURN_COST.Text");
    _this.Lbl_CASE_RETURN_COST.VCLType = TVCLType.BS;

    _this.CmbBox_CASE_RETURN_COST = new TVclComboBox2(spSecundario1.Column[1].This, "");
    _this.CmbBox_CASE_RETURN_COST.This.classList.add("form-control");

    var spSecundario2 = new TVclStackPanel(_this.GridRetun.Column[0].This, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [4, 8]]);

    _this.Lbl_CASE_RETURN_STR = new TVcllabel(spSecundario2.Column[0].This, "", TlabelType.H0);
    _this.Lbl_CASE_RETURN_STR.Text = UsrCfg.Traslate.GetLangText(_this.name, "Lbl_CASE_RETURN_STR.Text");
    _this.Lbl_CASE_RETURN_STR.VCLType = TVCLType.BS;

    _this.SpinEdit_CASE_RETURN_STR = new TVclTextBox(spSecundario2.Column[1].This, "");
    _this.SpinEdit_CASE_RETURN_STR.Text = "0"
    _this.SpinEdit_CASE_RETURN_STR.VCLType = TVCLType.BS;

    var spSecundario3 = new TVclStackPanel(_this.ObjectHtml, "", 3, [[8, 2, 2], [8, 2, 2], [8, 2, 2], [8, 2, 2], [8, 2, 2]]);
    spSecundario3.Row.This.style.marginTop = "10px";
    _this.BtnCerrar = new TVclImagen(spSecundario3.Column[1].This, "");
    _this.BtnCerrar.Src = "image/24/delete.png";
    _this.BtnCerrar.Title = "Cerrar";
    _this.BtnCerrar.Cursor = "pointer";
    _this.BtnCerrar.onClick = function () {
        _this.BtnCerrar_OnClick(_this, _this.BtnCerrar);
    }

    _this.BtnOk = new TVclImagen(spSecundario3.Column[2].This, "");
    _this.BtnOk.Src = "image/24/success.png";
    _this.BtnOk.Title = "Aceptar";
    _this.BtnOk.Cursor = "pointer";
    _this.BtnOk.onClick = function () {
        _this.BtnOk_OnClick(_this, _this.BtnOk);
    }

}

ItHelpCenter.SD.frSDOperationStatus.prototype.Initialize = function (inSetSDOperationCase, inTitle, inLblStatus, inCmbStatus, inLblDescripcion, inTxtDescripcion) {
    var _this = this.TParent();

    _this.SetSDOperationCase = inSetSDOperationCase;

    _this.Modal.AddHeaderContent(inTitle);
    _this.LblStatus.Text = inLblStatus;
    //CmbStatus.Items.Add(inCmbStatus);
    for (var i = 0; i < inCmbStatus.length; i++) {
        var VclComboBoxItem2 = new TVclComboBoxItem();
        VclComboBoxItem2.Value = inCmbStatus[i][0];
        VclComboBoxItem2.Text = inCmbStatus[i][1];
        VclComboBoxItem2.Tag = inCmbStatus[i];
        _this.CmbStatus.AddItem(VclComboBoxItem2);
    }
    _this.CmbStatus.selectedIndex = 0;
    _this.LblDescripcion.Text = inLblDescripcion;
    _this.TxtDescripcion.Text = inTxtDescripcion;
}

ItHelpCenter.SD.frSDOperationStatus.prototype.BtnCerrar_OnClick = function (sender, EventArgs) {
    var _this = sender;
    _this.Modal.CloseModal();
    _this.Close();
}

ItHelpCenter.SD.frSDOperationStatus.prototype.BtnOk_OnClick = function (sender, EventArgs) {
    var _this = sender;

    if (_this.CmbStatus.Value == UsrCfg.SD.Properties.TSDCaseStatus._Closed.value) {
        var customMessage = confirm(UsrCfg.Traslate.GetLangText(_this.name, "Line1"));
        if (customMessage) {

            //Item itm = (Item)CmbStatus.SelectedItem;
            //SetSDOperationCase.SDOPERATION_CASESTATUS.IDSDCASESTATUS = (UsrCfg.SD.Properties.TSDCaseStatus)itm.Key;
            _this.SetSDOperationCase.SDOPERATION_CASESTATUS.IDSDCASESTATUS = UsrCfg.SD.Properties.TSDCaseStatus.GetEnum(parseInt(_this.CmbStatus.Value));
            _this.SetSDOperationCase.SDOPERATION_CASESTATUS.CASESTATUS_MESSAGE = _this.TxtDescripcion.Text;
            if (_this.ModalResult != null)
                _this.ModalResult(_this.frSDCAseAtention, _this.SetSDOperationCase);
            if (_this.isGridRetun) {
                _this.SetSDOperationCase.SDOPERATION_CASESTATUS.CASE_RETURN_STR = _this.CmbBox_CASE_RETURN_COST.Options[_this.CmbBox_CASE_RETURN_COST.selectedIndex].Text; 
                _this.SetSDOperationCase.SDOPERATION_CASESTATUS.CASE_RETURN_COST = parseFloat(_this.SpinEdit_CASE_RETURN_STR.Text);
            }
            _this.DialogResult = true;
            if (_this.IsModal) {
                _this.Modal.CloseModal();                
            }
            _this.Close();
        }
    }
    else {
        //Item itm = (Item)CmbStatus.SelectedItem;
        //SetSDOperationCase.SDOPERATION_CASESTATUS.IDSDCASESTATUS = (UsrCfg.SD.Properties.TSDCaseStatus)itm.Key;
        _this.SetSDOperationCase.SDOPERATION_CASESTATUS.IDSDCASESTATUS = UsrCfg.SD.Properties.TSDCaseStatus.GetEnum(parseInt(_this.CmbStatus.Value));
        _this.SetSDOperationCase.SDOPERATION_CASESTATUS.CASESTATUS_MESSAGE = _this.TxtDescripcion.Text;
        if (_this.ModalResult != null)
            _this.ModalResult(_this.frSDCAseAtention, _this.SetSDOperationCase);
        _this.DialogResult = true;
        if (_this.IsModal) {
            _this.Modal.CloseModal();            
        }
        _this.Close();
    }
}

ItHelpCenter.SD.frSDOperationStatus.prototype.CmbStatus_SelectedChanged = function (sender, EventArgs) {
    var _this = sender;

    _this.GridRetun.Row.Visible = false;
    if ((_this.CmbStatus.Value == UsrCfg.SD.Properties.TSDCaseStatus._Closed.value) && (_this.isGridRetun)) {
        _this.GridRetun.Row.Visible = true;
    }
    //isGridRetun
    if (_this.CmbBox_CASE_RETURN_COST.Options.length > 0)
        _this.SetSDOperationCase.SDOPERATION_CASESTATUS.CASE_RETURN_STR = _this.CmbBox_CASE_RETURN_COST.Options[_this.CmbBox_CASE_RETURN_COST.selectedIndex].Text;
    _this.SetSDOperationCase.SDOPERATION_CASESTATUS.CASE_RETURN_COST = parseFloat(_this.SpinEdit_CASE_RETURN_STR.Text);
                                                                       
}