﻿ItHelpCenter.SD.CaseManager.Atention.TfrSDCaseAtention.prototype.InitializeComponent = function () {
    var _this = this.TParent();

    //***************************** inicio nueva parte 3 ********************************************************
    var spContainer = new TVclStackPanel(_this.Object, "", 1, [[12], [12], [12], [12], [12]]);
    spContainer.Row.This.style.border = "1px solid black";
    spContainer.Row.This.style.backgroundColor = "#DDE6BD";

    _this.spContainerCase = spContainer;

    var spHeadRow1 = new TVclStackPanel(spContainer.Column[0].This, "h1", 3, [[6, 6, 12], [6, 6, 12], [6, 6, 12], [2, 2, 8], [2, 2, 8]]);

    var spHeadRow1_1 = new TVclStackPanel(spHeadRow1.Column[0].This, "h1_1", 2, [[3, 9], [3, 9], [6, 6], [6, 6], [6, 6]]); // para la img ticket y nro ticket
    var spHeadRow1_2 = new TVclStackPanel(spHeadRow1.Column[0].This, "h1_2", 1, [[12], [12], [12], [12], [12]]); //para las estrellitas
    var spHeadRow1_3 = new TVclStackPanel(spHeadRow1.Column[0].This, "h1_3", 1, [[12], [12], [12], [12], [12]]); //para fecha de reporte
    var spHeadRow1_4 = new TVclStackPanel(spHeadRow1.Column[0].This, "h1_4", 1, [[12], [12], [12], [12], [12]]); //para el timer

    var spHeadRow2_1 = new TVclStackPanel(spHeadRow1.Column[1].This, "h2_1", 2, [[9, 3], [9, 3], [6, 6], [6, 6], [6, 6]]); // para la img refrescar y atras
    var spHeadRow2_2 = new TVclStackPanel(spHeadRow1.Column[1].This, "h2_2", 1, [[12], [12], [12], [12], [12]]); //para el actual status
    var spHeadRow2_3 = new TVclStackPanel(spHeadRow1.Column[1].This, "h2_3", 1, [[12], [12], [12], [12], [12]]); //para el usuario del caso
    var spHeadRow2_4 = new TVclStackPanel(spHeadRow1.Column[1].This, "h2_4", 1, [[12], [12], [12], [12], [12]]); //para el atention 

    var spHeadRow3_1 = new TVclStackPanel(spHeadRow1.Column[2].This, "h3_1", 1, [[12], [12], [12], [12], [12]]); //para el titulo del caso
    var spHeadRow3_2 = new TVclStackPanel(spHeadRow1.Column[2].This, "h3_2", 1, [[12], [12], [12], [12], [12]]); //para la descripcion del caso
    var spHeadRow3_3 = new TVclStackPanel(spHeadRow1.Column[2].This, "h3_3", 1, [[12], [12], [12], [12], [12]]); //para el path
    //var spHeadRow3_4 = new TVclStackPanel(spHeadRow1.Column[2].This, "h3_4", 1, [[12], [12], [12], [12], [12]]); 

    spHeadRow1.Column[1].This.classList.add("pull-right");

    //spHeadRow3_2.Column[0].This.style.maxHeight = "84px";
    //spHeadRow3_2.Column[0].This.style.overflowY = 'scroll';

    spHeadRow1_1.Column[0].This.classList.add("text-center");
    spHeadRow1_1.Column[1].This.classList.add("text-center");
    spHeadRow2_1.Column[0].This.classList.add("text-right");
    spHeadRow2_1.Column[1].This.classList.add("text-right");
    spHeadRow1_2.Column[0].This.classList.add("text-center");


    spHeadRow1_4.Row.This.classList.add("row-xs-custom");
    spHeadRow2_4.Row.This.classList.add("row-xs-custom");

    $(spHeadRow3_2.Column[0].This).slimscroll({
        color: '#227446',
        height: '84px'
    });

    var DivModal = new TVclStackPanel(spContainer.Column[0].This, "modal", 1, [[12], [12], [12], [12], [12]]);
    _this.DivsModal = DivModal.Column[0].This
    $(_this.DivsModal).css("display", "none");

    spHeadRow1_2.Column[0].This.classList.add("col-xs-custom");
    spHeadRow1_3.Column[0].This.classList.add("col-xs-custom");
    spHeadRow1_4.Column[0].This.classList.add("col-xs-custom");
    

    spHeadRow2_2.Column[0].This.classList.add("col-xs-custom");
    spHeadRow2_3.Column[0].This.classList.add("col-xs-custom");
    spHeadRow2_4.Column[0].This.classList.add("col-xs-custom");

    //spHeadRow1_1.Column[0].This ---> para la img de ticket
    //spHeadRow1_1.Column[1].This ---> para el nro de ticket
    //spHeadRow1_2.Column[0].This ---> para las estrellitas
    //spHeadRow1_3.Column[0].This ---> para fecha de reporte
    //spHeadRow1_4.Column[0].This ---> para el timer

    //spHeadRow2_1.Column[0].This ---> para el boton refrescar
    //spHeadRow2_1.Column[1].This ---> para el boton atras
    //spHeadRow2_2.Column[0].This ---> para el actual status
    //spHeadRow2_3.Column[0].This ---> para el usuario del caso
    //spHeadRow2_4.Column[0].This ---> para el atention 

    //spHeadRow3_1.Column[0].This ---> para el titulo del caso
    //spHeadRow3_2.Column[0].This ---> para la descripcion del caso
    //spHeadRow3_3.Column[0].This ---> para el path

    spHeadRow2_1.Column[0].This.style.minHeight = "42px";

    spHeadRow1_1.Column[0].This.style.minHeight = "42px";
    spHeadRow1_1.Column[0].This.style.backgroundColor = "#808080";
    spHeadRow1_1.Column[1].This.style.backgroundColor = "#808080";


    _this.btnRefreshAll = new TVclImagen(spHeadRow2_1.Column[0].This, "");
    _this.btnRefreshAll.This.style.marginTop = "8px";
    _this.btnRefreshAll.Src = "image/32/Rules.png";
    _this.btnRefreshAll.Title = "Refresh";
    _this.btnRefreshAll.Cursor = "pointer";

    _this.btnRefreshAll.onClick = function () {
        _this.btnRefreshAll_onClick(_this, _this.btnRefreshAll);
    };

    _this.btnCloseAll = new TVclImagen(spHeadRow2_1.Column[1].This, "");
    _this.btnCloseAll.This.style.marginTop = "8px";
    _this.btnCloseAll.Src = "image/32/Go-back.png";
    _this.btnCloseAll.Title = "Close";
    _this.btnCloseAll.Cursor = "pointer";

    _this.btnCloseAll.onClick = function () {
        _this.btnCloseAll_onClick(_this, _this.btnCloseAll);
    };

    _this.img_case = new TVclImagen(spHeadRow1_1.Column[0].This, "");
    _this.img_case.Src = "image/24/CaseGreen.png";
    _this.img_case.Title = "";
    _this.img_case.This.style.marginTop = "8px";

    _this.lblCaseNumber = new TVcllabel(spHeadRow1_1.Column[1].This, "", TlabelType.H4);
    _this.lblCaseNumber.Text = "0000";
    _this.lblCaseNumber.This.style.fontSize = "20px";
    _this.lblCaseNumber.This.style.fontWeight = "700";
    _this.lblCaseNumber.This.style.color = "white";

    _this.Lbl_CASE_TITLE = new TVcllabel(spHeadRow3_1.Column[0].This, "", TlabelType.H4);
    _this.Lbl_CASE_TITLE.Text = "...";
    _this.Lbl_CASE_TITLE.This.style.fontSize = "20px";
    _this.Lbl_CASE_TITLE.This.style.fontWeight = "700";
    _this.Lbl_CASE_TITLE.This.style.color = "#0094FF";

    //seccion para la creacion del priority
    //DivStar
    _this.DivStar = spHeadRow1_2.Column[0].This;

    //DivTitle1
    //_this.DivTitle = spHeadRow3_2.Column[0].This;   

    _this.VcllabelStatus = new TVcllabel(spHeadRow2_2.Column[0].This, "", TlabelType.H4);
    _this.VcllabelStatus.Text = "Actual Status: ";
    _this.VcllabelStatus.This.style.fontSize = "13px";
    _this.VcllabelStatus.This.style.fontWeight = "700";
    _this.VcllabelStatus.This.classList.add("text-left");
    _this.VcllabelStatus.This.classList.add("h4-custom");

    _this.Txt_CASE_DESCRIPTION = new TVcllabel(spHeadRow3_2.Column[0].This, "", TlabelType.H4);
    _this.Txt_CASE_DESCRIPTION.Text = "...";
    _this.Txt_CASE_DESCRIPTION.This.style.fontSize = "15px";

    _this.Txt_CASE_DATECREATE = new TVcllabel(spHeadRow1_3.Column[0].This, "", TlabelType.H4);
    _this.Txt_CASE_DATECREATE.Text = "00/00/00 00:00:00";
    _this.Txt_CASE_DATECREATE.This.style.fontSize = "13px";
    _this.Txt_CASE_DATECREATE.This.style.fontWeight = "700";
    _this.Txt_CASE_DATECREATE.This.classList.add("text-left");
    _this.Txt_CASE_DATECREATE.This.classList.add("h4-custom");

    _this.TxtSDTYPEUSER = new TVcllabel(spHeadRow2_4.Column[0].This, "", TlabelType.H4);
    _this.TxtSDTYPEUSER.Text = "none";
    _this.TxtSDTYPEUSER.This.style.fontSize = "13px";
    _this.TxtSDTYPEUSER.This.style.fontWeight = "700";
    _this.TxtSDTYPEUSER.This.classList.add("text-left");
    _this.TxtSDTYPEUSER.This.classList.add("h4-custom");

    _this.TxtUSER = new TVcllabel(spHeadRow2_3.Column[0].This, "", TlabelType.H4);
    _this.TxtUSER.Text = "...";
    _this.TxtUSER.This.style.fontSize = "13px";
    _this.TxtUSER.This.style.fontWeight = "700";
    _this.TxtUSER.This.classList.add("text-left");
    _this.TxtUSER.This.classList.add("h4-custom");

    _this.TxtCATEGORY = new TVcllabel(spHeadRow3_3.Column[0].This, "", TlabelType.H4);
    _this.TxtCATEGORY.Text = "...";
    _this.TxtCATEGORY.This.style.fontSize = "15px";

    _this.VclTimer_Level = new TVcllabel(spHeadRow1_4.Column[0].This, "", TlabelType.H4);
    _this.VclTimer_Level.Text = "00:00:00";
    _this.VclTimer_Level.This.style.fontSize = "13px";
    _this.VclTimer_Level.This.style.fontWeight = "700";
    _this.VclTimer_Level.This.classList.add("text-left");
    _this.VclTimer_Level.This.classList.add("h4-custom");

    //***************************** fin nueva parte 3 ********************************************************


    //***************************** inicio nueva parte 2 ********************************************************

    //var spContainer = new TVclStackPanel(_this.Object, "", 1, [[12], [12], [12], [12], [12]]);
    //spContainer.Row.This.style.border = "1px solid black";
    //spContainer.Row.This.style.backgroundColor = "#DDE6BD";
    ////spContainer.Row.This.style.marginLeft = "4px";
    ////spContainer.Row.This.style.margin = "15px 20px 10px 10px";

    ////var spHeadRow1 = new TVclStackPanel(spContainer.Column[0].This, "h1", 3, [[12, 12, 12], [3, 3, 6], [2, 2, 8], [2, 2, 8], [2, 2, 8]]);
    //var spHeadRow1 = new TVclStackPanel(spContainer.Column[0].This, "h1", 4, [[12, 12, 12, 12], [2, 3, 0, 7], [1, 2, 0, 9], [1, 2, 0, 9], [1, 2, 0, 9]]);

    //var spHeadRow1_1 = new TVclStackPanel(spHeadRow1.Column[0].This, "h1_1", 2, [[6, 6], [6, 6], [6, 6], [6, 6], [6, 6]]);
    //var spHeadRow1_2 = new TVclStackPanel(spHeadRow1.Column[1].This, "h1_2", 2, [[6, 6], [6, 6], [6, 6], [6, 6], [6, 6]]);

    //var spHeadRow2 = new TVclStackPanel(spContainer.Column[0].This, "h2", 3, [[12, 12, 12], [3, 3, 6], [2, 2, 8], [2, 2, 8], [2, 2, 8]]);
    //var spHeadRow2_1_1 = new TVclStackPanel(spHeadRow2.Column[0].This, "h2_1_1", 1, [[12], [12], [12], [12], [12]]);
    //var spHeadRow2_1_2 = new TVclStackPanel(spHeadRow2.Column[0].This, "h2_1_2", 1, [[12], [12], [12], [12], [12]]);

    //var spHeadRow2_2_1 = new TVclStackPanel(spHeadRow2.Column[1].This, "h2_2_1", 1, [[12], [12], [12], [12], [12]]);
    //var spHeadRow2_2_2 = new TVclStackPanel(spHeadRow2.Column[1].This, "h2_2_1", 1, [[12], [12], [12], [12], [12]]);

    //var spHeadRow3 = new TVclStackPanel(spContainer.Column[0].This, "h3", 3, [[12, 12, 12], [3, 3, 6], [2, 2, 8], [2, 2, 8], [2, 2, 8]]);

    //spHeadRow1.Column[2].This.classList.add("hidden-lg");
    //spHeadRow1.Column[2].This.classList.add("hidden-md");
    //spHeadRow1.Column[2].This.classList.add("hidden-sm");
    //spHeadRow1.Column[2].This.classList.add("visible-xs");
    //spHeadRow1.Column[2].This.classList.add("text-center");

    //spHeadRow2_1_1.Column[0].This.classList.add("visible-lg");
    //spHeadRow2_1_1.Column[0].This.classList.add("visible-md");
    //spHeadRow2_1_1.Column[0].This.classList.add("visible-sm");
    //spHeadRow2_1_1.Column[0].This.classList.add("hidden-xs");


    //spHeadRow1.Column[0].This.classList.add("pull-right");
    //spHeadRow2.Column[0].This.classList.add("pull-left");
    //spHeadRow2.Column[1].This.classList.add("pull-right");
    //spHeadRow3.Column[0].This.classList.add("pull-left");
    //spHeadRow3.Column[1].This.classList.add("pull-right");


    //spHeadRow1_2.Column[0].This.classList.add("text-center");
    //spHeadRow1_2.Column[1].This.classList.add("text-center");
    //spHeadRow1_1.Column[0].This.classList.add("text-right");
    //spHeadRow1_1.Column[1].This.classList.add("text-right");
    //spHeadRow2_1_1.Column[0].This.classList.add("text-center");
    //spHeadRow2.Column[2].This.style.maxHeight = "84px";
    //spHeadRow2.Column[2].This.style.overflowY = 'scroll';

    //var DivModal = new TVclStackPanel(spContainer.Column[0].This, "modal", 1, [[12], [12], [12], [12], [12]]);
    //_this.DivsModal = DivModal.Column[0].This
    //$(_this.DivsModal).css("display", "none");

    ////spHeadRow1.Column[2].This   ---> para el titulo del caso
    ////spHeadRow1_1.Column[0].This ---> para el boton refrescar
    ////spHeadRow1_1.Column[1].This ---> para el boton atras
    ////spHeadRow1_2.Column[0].This ---> para la img de ticket
    ////spHeadRow1_2.Column[1].This ---> para el nro de ticket

    ////spHeadRow2.Column[2].This   ---> para la descripcion del caso
    ////spHeadRow2_1_1.Column[0].This ---> para las estrellitas
    ////spHeadRow1.Column[2].This     ---> para las estrellitas
    ////spHeadRow2_1_2.Column[0].This ---> para fecha de reporte
    ////spHeadRow2_2_1.Column[0].This ---> para el actual status
    ////spHeadRow2_2_2.Column[0].This ---> para el usuario del caso

    ////spHeadRow3.Column[0].This   ---> para el timer
    ////spHeadRow3.Column[1].This   ---> para el atention
    ////spHeadRow3.Column[2].This   ---> para el path

    //spHeadRow1_2.Column[0].This.style.minHeight = "42px";
    //spHeadRow1_2.Column[0].This.style.backgroundColor = "#808080";
    //spHeadRow1_2.Column[1].This.style.backgroundColor = "#808080";

    //this.btnRefreshAll = new TVclImagen(spHeadRow1_1.Column[0].This, "");
    //_this.btnRefreshAll.This.style.marginTop = "8px";
    //_this.btnRefreshAll.Src = "image/32/Rules.png";
    //_this.btnRefreshAll.Title = "Refresh";
    //_this.btnRefreshAll.Cursor = "pointer";

    //_this.btnRefreshAll.onClick = function () {
    //    _this.btnRefreshAll_onClick(_this, _this.btnRefreshAll);
    //};

    //_this.btnCloseAll = new TVclImagen(spHeadRow1_1.Column[1].This, "");
    //_this.btnCloseAll.This.style.marginTop = "8px";
    //_this.btnCloseAll.Src = "image/32/Go-back.png";
    //_this.btnCloseAll.Title = "Close";
    //_this.btnCloseAll.Cursor = "pointer";

    //_this.btnCloseAll.onClick = function () {
    //    _this.btnCloseAll_onClick(_this, _this.btnCloseAll);
    //};

    //_this.img_case = new TVclImagen(spHeadRow1_2.Column[0].This, "");
    //_this.img_case.Src = "image/24/CaseGreen.png";
    //_this.img_case.Title = "";
    //_this.img_case.This.style.marginTop = "8px";

    //_this.lblCaseNumber = new TVcllabel(spHeadRow1_2.Column[1].This, "", TlabelType.H4);
    //_this.lblCaseNumber.Text = "0000";
    //_this.lblCaseNumber.This.style.fontSize = "20px";
    //_this.lblCaseNumber.This.style.fontWeight = "700";
    //_this.lblCaseNumber.This.style.color = "white";

    //_this.Lbl_CASE_TITLE = new TVcllabel(spHeadRow1.Column[3].This, "", TlabelType.H4);
    //_this.Lbl_CASE_TITLE.Text = "...";
    //_this.Lbl_CASE_TITLE.This.style.fontSize = "20px";
    //_this.Lbl_CASE_TITLE.This.style.fontWeight = "700";
    //_this.Lbl_CASE_TITLE.This.style.color = "#0094FF";

    ////DivStar
    ////_this.DivStar = spHeadR1.Column[2].This
    //_this.DivStar = spHeadRow2_1_1.Column[0].This;
    //_this.DivStar2 = spHeadRow1.Column[2].This;

    ////DivTitle1
    ////_this.DivTitle = spHeadR1.Column[6].This;
    //_this.DivTitle = spHeadRow2.Column[2].This;

    //_this.VcllabelStatus = new TVcllabel(spHeadRow2_2_1.Column[0].This, "", TlabelType.H4);
    //_this.VcllabelStatus.Text = "Actual Status: ";
    //_this.VcllabelStatus.This.style.fontSize = "15px";
    //_this.VcllabelStatus.This.style.fontWeight = "700";
    //_this.VcllabelStatus.This.classList.add("text-left");

    //_this.Txt_CASE_DESCRIPTION = new TVcllabel(spHeadRow2.Column[2].This, "", TlabelType.H4);
    //_this.Txt_CASE_DESCRIPTION.Text = "...";
    //_this.Txt_CASE_DESCRIPTION.This.style.fontSize = "15px";

    //_this.Txt_CASE_DATECREATE = new TVcllabel(spHeadRow2_1_2.Column[0].This, "", TlabelType.H4);
    //_this.Txt_CASE_DATECREATE.Text = "00/00/00 00:00:00";
    //_this.Txt_CASE_DATECREATE.This.style.fontSize = "15px";
    //_this.Txt_CASE_DATECREATE.This.style.fontWeight = "700";
    //_this.Txt_CASE_DATECREATE.This.classList.add("text-left");

    //_this.TxtSDTYPEUSER = new TVcllabel(spHeadRow3.Column[1].This, "", TlabelType.H4);
    //_this.TxtSDTYPEUSER.Text = "nones";
    //_this.TxtSDTYPEUSER.This.style.fontSize = "15px";
    //_this.TxtSDTYPEUSER.This.style.fontWeight = "700";
    //_this.TxtSDTYPEUSER.This.classList.add("text-left");

    //_this.TxtUSER = new TVcllabel(spHeadRow2_2_2.Column[0].This, "", TlabelType.H4);
    //_this.TxtUSER.Text = "...";
    //_this.TxtUSER.This.style.fontSize = "15px";
    //_this.TxtUSER.This.style.fontWeight = "700";
    //_this.TxtUSER.This.classList.add("text-left");    

    //_this.TxtCATEGORY = new TVcllabel(spHeadRow3.Column[2].This, "", TlabelType.H4);
    //_this.TxtCATEGORY.Text = "...";
    //_this.TxtCATEGORY.This.style.fontSize = "15px";

    //_this.VclTimer_Level = new TVcllabel(spHeadRow3.Column[0].This, "", TlabelType.H4);
    //_this.VclTimer_Level.Text = "00:00";
    //_this.VclTimer_Level.This.style.fontSize = "15px";
    //_this.VclTimer_Level.This.style.fontWeight = "700";
    //_this.VclTimer_Level.This.classList.add("text-left");
    //***************************** fin nueva parte 2 ********************************************************

    //***************************** inicio nueva parte *******************************************************************
    //var spContainer = new TVclStackPanel(_this.Object, "", 1, [[12], [12], [12], [12], [12]]);
    //spContainer.Row.This.style.border = "1px solid black";
    //spContainer.Row.This.style.backgroundColor = "#DDE6BD";
    //spContainer.Row.This.style.margin = "15px 20px 10px 10px";


    ////var Container = new TVclDiv(Udiv(_this.Object, ""))
    ////BootStrapContainer(Container.This, TBootStrapContainerType.none);
    ////Container.This.style.border = "1px solid black";
    ////Container.This.style.backgroundColor = "#DDE6BD";
    ////Container.This.style.marginTop = "10px";

    //var DivModal = new TVclStackPanel(spContainer.Column[0].This, "modal", 1, [[12], [12], [12], [12], [12]]);
    //_this.DivsModal = DivModal.Column[0].This
    //$(_this.DivsModal).css("display", "none");

    //var spHead = new TVclStackPanel(spContainer.Column[0].This, "h", 1, [[12], [12], [12], [12], [12]]);
    //var spHead1 = new TVclStackPanel(spHead.Column[0].This, "h1", 4, [[12, 12, 12, 12], [12, 12, 12, 12], [1, 1, 8, 2], [1, 1, 9, 1], [1, 1, 9, 1]]);

    ////spHead1.Row.This.classList.add("row-eq-height");

    //spHead1.Column[0].This.style.backgroundColor = "#808080";
    //spHead1.Column[1].This.style.backgroundColor = "#808080";

    ////img_case 
    //spHead1.Column[0].This.style.minHeight = "42px";
    //_this.img_case = new TVclImagen(spHead1.Column[0].This, "");
    //_this.img_case.Src = "image/24/CaseGreen.png";
    //_this.img_case.Title = "";
    //_this.img_case.This.style.marginTop = "8px";

    //_this.lblCaseNumber = new TVcllabel(spHead1.Column[1].This, "", TlabelType.H4);
    //_this.lblCaseNumber.Text = "0000";
    //_this.lblCaseNumber.This.style.fontSize = "20px";
    //_this.lblCaseNumber.This.style.fontWeight = "700";
    //_this.lblCaseNumber.This.style.color = "white";

    //_this.Lbl_CASE_TITLE = new TVcllabel(spHead1.Column[2].This, "", TlabelType.H4);
    //_this.Lbl_CASE_TITLE.Text = "...";
    //_this.Lbl_CASE_TITLE.This.style.fontSize = "20px";
    //_this.Lbl_CASE_TITLE.This.style.fontWeight = "700";
    //_this.Lbl_CASE_TITLE.This.style.color = "#0094FF";

    //var spControl = new TVclStackPanel(spHead1.Column[3].This, "", 2, [[12, 12], [12, 12], [12, 12], [6, 6], [6, 6]]);

    //_this.btnRefreshAll = new TVclImagen(spControl.Column[0].This, "");
    //_this.btnRefreshAll.This.style.marginTop = "8px";
    //_this.btnRefreshAll.Src = "image/32/Rules.png";
    //_this.btnRefreshAll.Title = "Refresh";
    //_this.btnRefreshAll.Cursor = "pointer";

    //_this.btnRefreshAll.onClick = function () {
    //    _this.btnRefreshAll_onClick(_this, _this.btnRefreshAll);
    //};

    //_this.btnCloseAll = new TVclImagen(spControl.Column[1].This, "");
    //_this.btnCloseAll.This.style.marginTop = "8px";
    //_this.btnCloseAll.Src = "image/32/Go-back.png";
    //_this.btnCloseAll.Title = "Close";
    //_this.btnCloseAll.Cursor = "pointer";

    //_this.btnCloseAll.onClick = function () {
    //    _this.btnCloseAll_onClick(_this, _this.btnCloseAll);
    //};

    ///////////////////////////////////////

    //var spHead2 = new TVclStackPanel(spHead.Column[0].This, "h2", 3, [[12, 12, 12], [12, 12, 12], [2, 7, 3], [2, 7, 3], [2, 7, 3]]);

    //////DivStar
    ////_this.DivStar = spHead2.Column[0].This;

    //////DivTitle1
    ////_this.DivTitle = spHead2.Column[1].This;


    //_this.VcllabelStatus = new TVcllabel(spHead2.Column[2].This, "", TlabelType.H4);
    //_this.VcllabelStatus.Text = "Actual Status: ";
    //_this.VcllabelStatus.This.style.fontSize = "15px";
    //_this.VcllabelStatus.This.style.fontWeight = "700";
    //_this.VcllabelStatus.This.classList.add("text-right");




    _this.ContentNavBar = new TVclStackPanel(spContainer.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
    //_this.ContentNavBar.Column[0].This.style.padding = "0px";
    //********************************************************************************************************************

    if ((Source.Menu.IsMobil) && (_this.MenuObject != null)) {
        _this.NavTabControls = new ItHelpCenter.Componet.NavTabControls.TNavTabControls(_this.MenuObject.ObjectData, null);
        _this.NavTabControls.VerticalPosition = true;
        _this.NavTabControls.TextColorPanel = "#FFFFFF";


    } else {

        _this.NavTabControls = new ItHelpCenter.Componet.NavTabControls.TNavTabControls(_this.ContentNavBar.Column[0].This, null);
        _this.NavTabControls.VerticalPosition = false;
        if (Source.Menu.IsMobil) {
            _this.NavTabControls.PaddingTopNavBar = 3;
            _this.NavTabControls.PaddingNavBar = 3;
        }
        else {
            _this.NavTabControls.PaddingTopNavBar = 10;
            _this.NavTabControls.PaddingNavBar = 10;
        }


    }


    var TabPag1 = new TTabPage();
    TabPag1.Name = "Home";
    TabPag1.Caption = UsrCfg.Traslate.GetLangText(_this.Mythis, "TabPag1.Caption");
    TabPag1.Active = true;
    TabPag1.Tag = "Home";
    _this.NavTabControls.AddTabPages(TabPag1);
    //****************************************       
    _this.lblMaxTime = new ItHelpCenter.Componet.NavTabControls.TNavPanelBar(TabPag1);
    _this.lblMaxTime.Title = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblMaxTime.Title");

    //----------------------------------------
    //*****************
    _this.NavBarTimer_Information = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.NavBarTimer_Information.ToolTip = "";
    _this.NavBarTimer_Information.onclick = function (NavBar) {
        _this.NavBarTimer_Information_Click(_this, _this.NavBarTimer_Information);
    }
    _this.NavBarTimer_Information.SrcImg = "image/32/information.png";//SysCfg.App.Properties.xRaiz+
    _this.NavBarTimer_Information.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "NavBarTimer_Information.Text");
    _this.NavBarTimer_Information.TextColor = "#757575";
    _this.lblMaxTime.addItemBar(_this.NavBarTimer_Information);

    //*****************
    _this.NavGroupBarTimer = new ItHelpCenter.Componet.NavTabControls.TNavGroupBar();
    //********
    _this.GroupBarTimer_Maxtime = new ItHelpCenter.Componet.NavTabControls.TItemGroupBar();
    _this.GroupBarTimer_Maxtime.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "GroupBarTimer_Maxtime.Text") + "--:--";
    _this.GroupBarTimer_Maxtime.SrcImg = "image/16/Alarm-clock.png";//alert-clock.png
    _this.GroupBarTimer_Maxtime.onclick = function (ItemGroupBar) {

        _this.GroupBarTimer_Maxtime_Click(_this, _this.GroupBarTimer_Maxtime);
    }
    _this.NavGroupBarTimer.addItemGroupBar(_this.GroupBarTimer_Maxtime);
    //********
    _this.GroupBarTimer_MaxtimeDate = new ItHelpCenter.Componet.NavTabControls.TItemGroupBar();
    _this.GroupBarTimer_MaxtimeDate.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "GroupBarTimer_MaxtimeDate.Text") + "\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;00/00/00  00:00:00";
    _this.GroupBarTimer_MaxtimeDate.SrcImg = "image/16/CalendarC.png";//calendarB.png
    _this.GroupBarTimer_MaxtimeDate.onclick = function (ItemGroupBar) {
        _this.GroupBarTimer_Maxtime_Click(_this, _this.GroupBarTimer_MaxtimeDate);
    }
    _this.NavGroupBarTimer.addItemGroupBar(_this.GroupBarTimer_MaxtimeDate);
    //********
    //----------------------------------------
    _this.lblMaxTime.addItemBar(_this.NavGroupBarTimer);
    //----------------------------------------




    //****************************************
    _this.lblCategoryGroup = new ItHelpCenter.Componet.NavTabControls.TNavPanelBar(TabPag1);
    _this.lblCategoryGroup.Title = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblCategoryGroup.Title");
    //----------------------------------------
    _this.btnDetail = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.btnDetail.ToolTip = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnDetail.ToolTip");
    _this.btnDetail.onclick = function (NavBar) {
        _this.btnDetail_Click(_this, _this.btnDetail);
        //_this.btnDetail_onClick(_this, _this.btnDetail);
    }
    _this.btnDetail.SrcImg = "image/24/File-info.png";
    _this.btnDetail.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnDetail.Text");
    _this.btnDetail.TextColor = "#757575";
    _this.btnDetail.Visible = true;
    _this.lblCategoryGroup.addItemBar(_this.btnDetail);


    _this.btnParentDetail = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.btnParentDetail.ToolTip = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnParentDetail.ToolTip");
    _this.btnParentDetail.onclick = function (NavBar) {
        _this.btnParentDetail_Click_1(_this, _this.btnParentDetail);
    }
    _this.btnParentDetail.SrcImg = "image/24/Home.png";
    _this.btnParentDetail.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnParentDetail.Text");
    _this.btnParentDetail.TextColor = "#757575";
    _this.btnParentDetail.Visible = (!UsrCfg.Version.IsProduction);
    _this.lblCategoryGroup.addItemBar(_this.btnParentDetail);

    _this.Btn_CATEGORYChange = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.Btn_CATEGORYChange.ToolTip = UsrCfg.Traslate.GetLangText(_this.Mythis, "Btn_CATEGORYChange.ToolTip");
    _this.Btn_CATEGORYChange.onclick = function (NavBar) {
        _this.Btn_CATEGORYChange_Click(_this, _this.Btn_CATEGORYChange);
    }
    _this.Btn_CATEGORYChange.Orientation = "Vertical";
    _this.Btn_CATEGORYChange.SrcImg = "image/24/Application.png";
    _this.Btn_CATEGORYChange.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Btn_CATEGORYChange.Text");
    _this.Btn_CATEGORYChange.TextColor = "#757575";
    _this.Btn_CATEGORYChange.Visible = true;//(!UsrCfg.Version.IsProduction);
    _this.lblCategoryGroup.addItemBar(_this.Btn_CATEGORYChange);

    _this.btnWorkArrown = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.btnWorkArrown.ToolTip = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnWorkArrown.ToolTip");
    _this.btnWorkArrown.onclick = function (NavBar) {
        _this.btnWorkArrown_Click(_this, _this.btnWorkArrown);
        //_this.btnWorkArrown_Click(_this, _this.BtnFnChangeStd);
    }
    _this.btnWorkArrown.SrcImg = "image/24/Product-documentation.png";
    _this.btnWorkArrown.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnWorkArrown.Text");
    _this.btnWorkArrown.TextColor = "#757575";
    _this.btnWorkArrown.Visible = true;
    _this.lblCategoryGroup.addItemBar(_this.btnWorkArrown);

    _this.btnUser = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.btnUser.ToolTip = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnUser.ToolTip");
    _this.btnUser.onclick = function (NavBar) {
        _this.btnUser_Click(_this, _this.btnUser);
        //_this.btnUser_onClick();
    }
    _this.btnUser.SrcImg = "image/24/user-blue.png";
    _this.btnUser.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnUser.Text");
    _this.btnUser.TextColor = "#757575";
    _this.btnUser.Visible = true;
    _this.lblCategoryGroup.addItemBar(_this.btnUser);
    //****************************************
    _this.lblStatusScaleGroup = new ItHelpCenter.Componet.NavTabControls.TNavPanelBar(TabPag1);
    _this.lblStatusScaleGroup.Title = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblStatusScaleGroup.Title");
    //----------------------------------------
    _this.BtnFnEscFun = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.BtnFnEscFun.ToolTip = UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnFnEscFun.ToolTip");
    _this.BtnFnEscFun.onclick = function (NavBar) {
        _this.BtnFnEscFun_Click(_this, _this.BtnFnEscFun);
    }
    _this.BtnFnEscFun.SrcImg = "image/24/Forward.png";
    _this.BtnFnEscFun.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnFnEscFun.Text");
    _this.BtnFnEscFun.TextColor = "#757575";
    _this.BtnFnEscFun.Visible = true;
    _this.lblStatusScaleGroup.addItemBar(_this.BtnFnEscFun);

    _this.BtnFnEscHier = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.BtnFnEscHier.ToolTip = UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnFnEscHier.ToolTip");
    _this.BtnFnEscHier.onclick = function (NavBar) {
        _this.BtnFnEscHier_Click(_this, _this.BtnFnEscHier);
    }
    _this.BtnFnEscHier.SrcImg = "image/24/Up.png";
    _this.BtnFnEscHier.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnFnEscHier.Text");
    _this.BtnFnEscHier.TextColor = "#757575";
    _this.BtnFnEscHier.Visible = true;
    _this.lblStatusScaleGroup.addItemBar(_this.BtnFnEscHier);

    _this.BtnFnChangeStd = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.BtnFnChangeStd.ToolTip = UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnFnChangeStd.ToolTip");
    _this.BtnFnChangeStd.onclick = function (NavBar) {
        _this.BtnFnChangeStd_Click(_this, _this.BtnFnChangeStd);
        //_this.BtnFnChangeStd_onClick(_this, _this.BtnFnChangeStd);
    }
    _this.BtnFnChangeStd.SrcImg = "image/24/Continue.png"
    _this.BtnFnChangeStd.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnFnChangeStd.Text");
    _this.BtnFnChangeStd.TextColor = "#757575";
    _this.BtnFnChangeStd.Visible = true;
    _this.lblStatusScaleGroup.addItemBar(_this.BtnFnChangeStd);

    //****************************************
    _this.lbllinkGroup = new ItHelpCenter.Componet.NavTabControls.TNavPanelBar(TabPag1);
    _this.lbllinkGroup.Title = UsrCfg.Traslate.GetLangText(_this.Mythis, "lbllinkGroup.Title");
    //----------------------------------------
    _this.BtnCIAfectado = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.BtnCIAfectado.ToolTip = UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnCIAfectado.ToolTip");
    _this.BtnCIAfectado.onclick = function (NavBar) {
        _this.BtnCIAfectado_Click_1(_this, _this.BtnCIAfectado);
    }
    _this.BtnCIAfectado.SrcImg = "image/24/Inventory-maintenance.png";
    _this.BtnCIAfectado.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnCIAfectado.Text");
    _this.BtnCIAfectado.TextColor = "#757575";
    _this.BtnCIAfectado.Visible = true;
    _this.lbllinkGroup.addItemBar(_this.BtnCIAfectado);

    _this.BtnAdjuntos = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.BtnAdjuntos.ToolTip = UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnAdjuntos.ToolTip");
    _this.BtnAdjuntos.onclick = function (NavBar) {
        _this.BtnAdjuntos_Click_1(_this, _this.BtnAdjuntos);
    }
    _this.BtnAdjuntos.SrcImg = "image/24/Attachment.png";
    _this.BtnAdjuntos.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnAdjuntos.Text");
    _this.BtnAdjuntos.TextColor = "#757575";
    _this.BtnAdjuntos.Visible = true;
    _this.lbllinkGroup.addItemBar(_this.BtnAdjuntos);

    _this.BtnRelatedCase = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.BtnRelatedCase.ToolTip = UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnRelatedCase.ToolTip");
    _this.BtnRelatedCase.onclick = function (NavBar) {
        _this.BtnRelatedCase_Click_1(_this, _this.BtnRelatedCase);
    }
    _this.BtnRelatedCase.SrcImg = "image/24/Link.png";
    _this.BtnRelatedCase.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnRelatedCase.Text");
    _this.BtnRelatedCase.TextColor = "#757575";
    _this.BtnRelatedCase.Visible = true;//(!UsrCfg.Version.IsProduction);
    _this.lbllinkGroup.addItemBar(_this.BtnRelatedCase);

    //****************************************
    _this.lblParentCaseGroup = new ItHelpCenter.Componet.NavTabControls.TNavPanelBar(TabPag1);
    _this.lblParentCaseGroup.Title = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblParentCaseGroup.Title");
    //----------------------------------------
    _this.BtnFnViewRelation = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.BtnFnViewRelation.ToolTip = UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnFnViewRelation.ToolTip");
    _this.BtnFnViewRelation.onclick = function (NavBar) {
        _this.BtnFnViewRelation_Click(_this, _this.BtnFnViewRelation);
    }
    _this.BtnFnViewRelation.SrcImg = "image/24/Question-type-multiple-correct.png";
    _this.BtnFnViewRelation.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnFnViewRelation.Text");
    _this.BtnFnViewRelation.TextColor = "#757575";
    _this.BtnFnViewRelation.Visible = (!UsrCfg.Version.IsProduction);
    _this.lblParentCaseGroup.addItemBar(_this.BtnFnViewRelation);

    _this.BtnADDRealtion = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.BtnADDRealtion.ToolTip = UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnADDRealtion.ToolTip");
    _this.BtnADDRealtion.onclick = function (NavBar) {
        _this.BtnADDRealtion_Click(_this, _this.BtnADDRealtion);
    }
    _this.BtnADDRealtion.SrcImg = "image/24/Bookmark-add.png";
    _this.BtnADDRealtion.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnADDRealtion.Text");
    _this.BtnADDRealtion.TextColor = "#757575";
    _this.BtnADDRealtion.Visible = (!UsrCfg.Version.IsProduction);
    _this.lblParentCaseGroup.addItemBar(_this.BtnADDRealtion);

    _this.BtnDELRealtions = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.BtnDELRealtions.ToolTip = UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnDELRealtions.ToolTip");
    _this.BtnDELRealtions.onclick = function (NavBar) {
        _this.BtnDELRealtions_Click(_this, _this.BtnDELRealtions);
    }
    _this.BtnDELRealtions.SrcImg = "image/24/Bookmark-delete.png";
    _this.BtnDELRealtions.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnDELRealtions.Text");
    _this.BtnDELRealtions.TextColor = "#757575";
    _this.BtnDELRealtions.Visible = (!UsrCfg.Version.IsProduction);
    _this.lblParentCaseGroup.addItemBar(_this.BtnDELRealtions);

    if ((!_this.BtnFnViewRelation.Visible) && (!_this.BtnADDRealtion.Visible) && (!_this.BtnDELRealtions.Visible))
        _this.lblParentCaseGroup.Visible = false;
    //********************************************************************************************************************

    //stack panel atenciones y mensajes
    var spAtentionMsg = new TVclStackPanel(spContainer.Column[0].This, "am", 1, [[12], [12], [12], [12], [12]]);
    spAtentionMsg.Row.This.style.backgroundColor = "white";

    var spAtentionMsg1 = new TVclStackPanel(spAtentionMsg.Column[0].This, "am1", 2, [[12, 12], [12, 12], [12, 12], [6, 6], [6, 6]]);
    // spAtentionMsg1.Row.This.classList.add("row-eq-height");
    spAtentionMsg1.Column[0].This.style.borderRight = "1px solid black";


    //stackpanel atenciones    
    var spAtentionMsg1_1 = new TVclStackPanel(spAtentionMsg1.Column[0].This, "am1_1", 3, [[5, 5, 2], [5, 5, 2], [8, 3, 1], [8, 3, 1], [8, 3, 1]]);
    spAtentionMsg1_1.Row.This.style.backgroundColor = "#EEF2DE";
    spAtentionMsg1_1.Row.This.style.borderBottom = "1px solid black";

    _this.VclMsgAtention = new TVcllabel(spAtentionMsg1_1.Column[0].This, "", TlabelType.H4);
    _this.VclMsgAtention.This.style.fontWeight = "700";
    _this.VclMsgAtention.This.style.fontSize = "15px";
    _this.VclMsgAtention.This.style.marginTop = "10px";
    _this.VclMsgAtention.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "VclMsgAtention.Text");

    //AtentionViewAll
    _this.AtentionViewAll = new TVclInputcheckbox(spAtentionMsg1_1.Column[1].This, "");
    _this.AtentionViewAll.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "AtentionViewAll.Text");
    _this.AtentionViewAll.This.style.marginTop = "10px";
    _this.AtentionViewAll.Checked = false;
    _this.AtentionViewAll.OnCheckedChange = function () {
        _this.AtentionViewAll_OnCheckedChange(_this, _this.AtentionViewAll);
    }

    //btnAtentionRefrescar_onClick
    _this.btn_MsgAten_Refresh = new TVclImagen(spAtentionMsg1_1.Column[2].This, "");
    _this.btn_MsgAten_Refresh.Src = "image/16/refresh.png";
    _this.btn_MsgAten_Refresh.This.style.marginTop = "10px";
    _this.btn_MsgAten_Refresh.Cursor = "pointer";
    _this.btn_MsgAten_Refresh.onClick = function () {
        _this.btn_MsgAten_Refresh_onClick(_this, _this.btn_MsgAten_Refresh);

    }

    _this.spMensajes = new TVclStackPanel(spAtentionMsg1.Column[0].This, "am1_2", 1, [[12], [12], [12], [12], [12]]);
    _this.spMensajes.Row.This.style.marginTop = "10px";

    _this.spAtentionMsg1_3 = new TVclStackPanel(spAtentionMsg1.Column[0].This, "am1_3", 2, [[11, 1], [11, 1], [11, 1], [11, 1], [11, 1]]);
    _this.spAtentionMsg1_3.Row.This.style.width = "100%";
    _this.spAtentionMsg1_3.Row.This.style.bottom = "5px";
    _this.spAtentionMsg1_3.Row.This.style.marginTop = "5px";
    _this.spAtentionMsg1_3.Row.This.style.marginBottom = "5px";
    _this.txtAtentionMessage = new TVclTextBox(_this.spAtentionMsg1_3.Column[0].This, "");
    _this.txtAtentionMessage.VCLType = TVCLType.BS;
    _this.txtAtentionMessage.This.addEventListener("keypress", function (e) {
        tecla = (document.all) ? e.keyCode : e.which;
        if (tecla == 13) {
            _this.btn_MsgAten_Add.Click();
        }
    }, false);

    ////btn_MsgAten_Add
    _this.btn_MsgAten_Add = new TVclImagen(_this.spAtentionMsg1_3.Column[1].This, "");
    _this.btn_MsgAten_Add.Src = "image/24/send-text-message.png";
    _this.btn_MsgAten_Add.This.style.marginTop = "5px";
    _this.btn_MsgAten_Add.Cursor = "pointer";
    //btn_MsgAten_Add_onClick
    _this.btn_MsgAten_Add.onClick = function () {
        _this.btn_MsgAten_Add_onClick(_this, _this.btn_MsgAten_Add);
    }

    //stackpanel Mensajes    
    var spAtentionMsg2_1 = new TVclStackPanel(spAtentionMsg1.Column[1].This, "am2_1", 3, [[5, 5, 2], [5, 5, 2], [8, 3, 1], [8, 3, 1], [8, 3, 1]]);
    spAtentionMsg2_1.Row.This.style.backgroundColor = "#EEF2DE";
    spAtentionMsg2_1.Row.This.style.borderBottom = "1px solid black";


    var deta2_uh4 = Vcllabel(spAtentionMsg2_1.Column[0].This, "", TVCLType.BS, TlabelType.H4, UsrCfg.Traslate.GetLangText(_this.Mythis, "deta2_uh4"));
    deta2_uh4.style.fontSize = "15px";
    deta2_uh4.style.fontWeight = "700";

    _this.btn_Msg_ViewPopup = UVCLRepositoryLookUp(spAtentionMsg2_1.Column[1].This, "Repositoriolook1" + _this.Id, UsrCfg.Traslate.GetLangText(_this.Mythis, "btn_Msg_ViewPopup"), true, true);
    _this.btn_Msg_ViewPopup.Uulli.ul.style.listStyle = "none";
    _this.btn_Msg_ViewPopup.Uulli.ul.style.marginTop = "7px";
    _this.btn_Msg_ViewPopup.Uulli.ul.style.marginBottom = "0px";
    _this.btn_Msg_ViewPopup.Caption.style.textDecoration = "none";
    _this.btn_Msg_ViewPopup.Caption.style.cursor = "pointer";
    _this.btn_Msg_ViewPopup.Caption.style.fontWeight = "700";
    _this.btn_Msg_ViewPopup.Caption.style.color = "#0094FF";

    BootStrapCreateRow(_this.btn_Msg_ViewPopup.DivRootContainer.This);
    _this.btn_Msg_ViewPopup.DivRootContainer.This.classList.add("message-content");
    _this.btn_Msg_ViewPopup.DivRootContainer.This.style.display = "none";
    BootStrapCreateColumn(_this.btn_Msg_ViewPopup.DivRootContainer.Child[0].This, [12, 12, 12, 12, 12]);
    BootStrapCreateRows(_this.btn_Msg_ViewPopup.DivRootContainer.Child[0].Child);
    BootStrapCreateColumns(_this.btn_Msg_ViewPopup.DivRootContainer.Child[0].Child[0].Child, [[5, 7], [5, 7], [5, 7], [5, 7], [5, 7]]);
    Vcllabel(_this.btn_Msg_ViewPopup.DivRootContainer.Child[0].Child[0].Child[0].This, "", TVCLType.BS, TlabelType.H4, UsrCfg.Traslate.GetLangText(_this.Mythis, "Uh4_Users"));

    //user_uul
    _this.user_uul = new TVclListBox(_this.btn_Msg_ViewPopup.DivRootContainer.Child[0].Child[0].Child[1].This, "");
    _this.user_uul.EnabledCheckBox = true;

    _this.btn_Msg_ViewPopup.DivRootContainer.Child[0].Child[0].Child[1].This.classList.add("text-left");
    BootStrapCreateColumns(_this.btn_Msg_ViewPopup.DivRootContainer.Child[0].Child[1].Child, [[12], [12], [12], [12], [12]]);
    Vcllabel(_this.btn_Msg_ViewPopup.DivRootContainer.Child[0].Child[1].Child[0].This, "", TVCLType.BS, TlabelType.H4, UsrCfg.Traslate.GetLangText(_this.Mythis, "Uh4_Message"));
    _this.btn_Msg_ViewPopup.DivRootContainer.Child[0].Child[1].Child[0].This.classList.add("text-left");

    //mensaje_utexarea
    _this.mensaje_utexarea = new TVclMemo(_this.btn_Msg_ViewPopup.DivRootContainer.Child[0].Child[1].Child[0].This, "TxtMessage" + _this.Id);
    _this.mensaje_utexarea.Text = "";
    _this.mensaje_utexarea.VCLType = TVCLType.BS;
    _this.mensaje_utexarea.Rows = 10;
    _this.mensaje_utexarea.Cols = 50;
    _this.mensaje_utexarea.This.addEventListener("keypress", function (e) {
        tecla = (document.all) ? e.keyCode : e.which;
        if (tecla == 13) {
            _this.btn_Msg_Add.Click();
        }
    }, false);
    BootStrapCreateColumns(_this.btn_Msg_ViewPopup.DivRootContainer.Child[0].Child[2].Child, [[6, 6], [6, 6], [6, 6], [6, 6], [6, 6]]);
    _this.btn_Msg_ViewPopup.ButtonCancelCaption.This.classList.add("jmbtn");
    _this.btn_Msg_ViewPopup.ButtonCancelCaption.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ButtonCancelCaption");
    _this.btn_Msg_ViewPopup.ButtonCancelCaption.onClick = function () { _this.btn_Msg_ViewPopup.Caption.click(); }

    //btn_Msg_Add
    _this.btn_Msg_Add = _this.btn_Msg_ViewPopup.ButtonOkCaption;

    _this.btn_Msg_ViewPopup.ButtonOkCaption.This.classList.add("jmbtn");
    _this.btn_Msg_ViewPopup.ButtonOkCaption.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ButtonOkCaption");
    //btn_Msg_ViewPopup_ButtonOkCaption_onClick
    _this.btn_Msg_ViewPopup.ButtonOkCaption.onClick = function () {
        _this.btn_Msg_ViewPopup_ButtonOkCaption_onClick();
    }

    _this.btn_Msg_Refresh = new TVclImagen(spAtentionMsg2_1.Column[2].This, "");
    _this.btn_Msg_Refresh.Src = "image/16/refresh.png";
    _this.btn_Msg_Refresh.Title = "Refresh Messages";
    _this.btn_Msg_Refresh.Cursor = "pointer";
    _this.btn_Msg_Refresh.This.style.marginTop = "10px";
    _this.btn_Msg_Refresh.onClick = function () {
        _this.GETMsgPost(_this.SDWHOTOCASE.IDSDWHOTOCASE, false);
    };

    var spAtentionMsg2_2 = new TVclStackPanel(spAtentionMsg1.Column[1].This, "am2_2", 1, [[12], [12], [12], [12], [12]]);
    //DivMsgPost
    _this.DivMsgPost = spAtentionMsg2_2.Column[0].This;

    var spAtentionMsg2_3 = new TVclStackPanel(spAtentionMsg1.Column[1].This, "am2_3", 2, [[10, 2], [10, 2], [10, 2], [10, 2], [10, 2]]);

    //stackPanel life status
    var DivScroll = new Udiv(spContainer.Column[0].This, "");
    DivScroll.classList.add("life-status-contenedor");

    var spLifeStatus = new TVclStackPanel(DivScroll, "ls", 2, [[1, 11], [1, 11], [1, 11], [1, 11], [1, 11]]);
    spLifeStatus.Row.This.classList.add("life-status-scroll");
    spLifeStatus.Row.This.style.backgroundColor = "#EEF2DE";
    spLifeStatus.Row.This.style.borderTop = "1px solid black";

    //DivToolbaLifeStatus    
    _this.DivToolbaLifeStatusIcon = spLifeStatus.Column[0].This;
    _this.DivToolbaLifeStatus = spLifeStatus.Column[1].This;

    ////btn_MsgAten_Add
    _this.btnStepGraphics = new TVclImagen(_this.DivToolbaLifeStatusIcon, "");
    _this.btnStepGraphics.Src = "image/24/share.png";
    _this.btnStepGraphics.This.style.marginTop = "15px";
    _this.btnStepGraphics.Cursor = "pointer";

    //btnStepGraphics_onClick
    _this.btnStepGraphics.onClick = function () {
        _this.btnStepGraphics_onClick(_this, _this.btnStepGraphics);
    }

    //stackpanel extra fields
    var spDivDynamicSTEF = new TVclStackPanel(spContainer.Column[0].This, "det", 1, [[12], [12], [12], [12], [12]]);
    spDivDynamicSTEF.Row.This.style.borderTop = "1px solid black";
    //DivDynamicSTEF
    _this.DivDynamicSTEF = spDivDynamicSTEF.Column[0].This;

    //stackpanel lendcomments y array grabar
    var spLendsArrayButton = new TVclStackPanel(spContainer.Column[0].This, "lab", 2, [[12, 12], [12, 12], [8, 4], [8, 4], [8, 4]]);
    spLendsArrayButton.Row.This.style.borderTop = "1px solid black";
    spLendsArrayButton.Row.This.style.paddingBottom = "10px";
    _this.DivLSEND_COMMENTS = spLendsArrayButton.Column[0].This;
    _this.DivGrabarETArray = spLendsArrayButton.Column[1].This;

    _this.spDivSetOperationCase = new TVclStackPanel(spContainer.Column[0].This, "setOpeCase", 3, [[12, 12, 12], [3, 3, 6], [3, 3, 6], [4, 4, 4], [4, 4, 4]]);
    _this.spDivSetOperationCase.Row.This.style.borderTop = "1px solid black";
    //_this.spDivSetOperationCase.Column[0].This.style.borderRight = "1px solid black";

    _this.LayoutPanel_Description = spHeadRow3_2.Column[0]; //cjrc 25/09/2018
    _this.LayoutPanel_Atention = spAtentionMsg1.Column[0]; //cjrc 25/09/2018
    _this.LayoutPanel_Mensajes = spAtentionMsg1.Column[1]; //cjrc 25/09/2018
    _this.LayoutPanel_Steps = spLifeStatus.Row; //cjrc 25/09/2018
    _this.LayoutPanel_StepSet = spLendsArrayButton.Row; //cjrc 25/09/2018
    _this.LayoutPanel_Status = _this.spDivSetOperationCase.Row; //cjrc 25/09/2018
}



