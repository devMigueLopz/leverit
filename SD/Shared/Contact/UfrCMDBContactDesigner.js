﻿ 
ItHelpCenter.SD.Shared.Contact.UfrCMDBContact.prototype.InitializeComponent = function () {

    try {
        var _this = this.TParent();
        var ObjHtml = _this.ObjectHtml;
        ObjHtml.innerHTML = "";     
        $(ObjHtml).css("padding", "0px");

        //ESTRUCTURA  
        var Container = new TVclStackPanel(ObjHtml, "Container_UfrSDAttached" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
        divCont = Container.Column[0].This;
        $(Container.Row.This).css("margin", "auto");
        if (SysCfg.App.Properties.Device.value == SysCfg.App.TDevice.Desktop.value) {
            $(Container.Row.This).css("padding", "10px");
        } else {
            $(Container.Row.This).css("padding", "0px 5px");
        }

        //div1 EL grid
        var div1 = new TVclStackPanel(divCont, "Contact_div1" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
        var div1Cont = div1.Column[0].This;
         
        //div2 Textos de contacto
        var div2 = new TVclStackPanel(ObjHtml, "Contact_div2" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
        var div2Cont = div2.Column[0].This;
        if (SysCfg.App.Properties.Device.value == SysCfg.App.TDevice.Desktop.value) {
            $(div2Cont).addClass("StackPanelContainer");
            $(div2.Row.This).css("padding", "0px 20px 20px 20px");
        } else {
            $(div2Cont).css("padding", "0px");
            $(div2.Row.This).css("padding", "10px");
        }       
        

        //Div boton + 
        var divAdd = new TVclStackPanel(div1Cont, "div1" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
        _this.divAddCont = divAdd.Column[0].This;
        if (SysCfg.App.Properties.Device.value == SysCfg.App.TDevice.Desktop.value) {
            $(_this.divAddCont).addClass("StackPanelContainer");
        } else {
            $(_this.divAddCont).css("padding","0px");
        }       
        $(_this.divAddCont).css("padding-bottom", "0px");
        $(divAdd.Row.This).css("padding-bottom", "0px");

        //Header del Grid
        var div1_Hd = new TVclStackPanel(div1Cont, "Contact_div1_Hd" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
        if (SysCfg.App.Properties.Device.value == SysCfg.App.TDevice.Desktop.value) {
            $(_this.div1_HdCont).addClass("StackPanelContainer");
        } else {
            $(_this.div1_HdCont).css("padding", "0px");
        }
        _this.div1_HdCont = div1_Hd.Column[0].This; 
        $(_this.div1_HdCont).css("margin-top", "0px");
        $(div1_Hd.Row.This).css("padding-bottom", "0px");
        $(div1_Hd.Row.This).css("padding-top", "0px");
        $(div1_Hd.Row.This).css("margin-top", "0px"); 


        //Body del Grid
        var div1_Bd = new TVclStackPanel(div1Cont, "Contact_div1_Bd" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
        _this.div1_BdCont = div1_Bd.Column[0].This;
        $(div1_Bd.Row.This).css("padding-top", "0px");
        if (SysCfg.App.Properties.Device.value == SysCfg.App.TDevice.Desktop.value) {
            $(_this.div1_BdCont).addClass("StackPanelContainer");
        } else {
            $(_this.div1_BdCont).css("padding", "0px");
            $(div2Cont).css("padding", "0px 10px");
        } 
        $(_this.div1_BdCont).css("margin-top", "0px"); 

        //Divs para los textos de contacto
        var div2_1 = new TVclStackPanel(div2Cont, "div2_1_" + _this.ID, 3, [[0, 12, 12], [3, 4, 5], [4, 3, 5], [4, 3, 5], [4, 3, 5]]);
        var div2_1_1Cont = div2_1.Column[1].This; //Label                                                          
        var div2_1_2Cont = div2_1.Column[2].This; //Text                                                           
        var div2_2 = new TVclStackPanel(div2Cont, "div2_2_" + _this.ID, 3, [[0, 12, 12], [3, 4, 5], [4, 3, 5], [4, 3, 5], [4, 3, 5]]);
        var div2_2_1Cont = div2_2.Column[1].This; //Label                                                           
        var div2_2_2Cont = div2_2.Column[2].This; //Text                                                            
        var div2_3 = new TVclStackPanel(div2Cont, "div2_3_" + _this.ID, 3, [[0, 12, 12], [3, 4, 5], [4, 3, 5], [4, 3, 5], [4, 3, 5]]);
        var div2_3_1Cont = div2_3.Column[1].This; //Label                                                           
        var div2_3_2Cont = div2_3.Column[2].This; //Text                                                            
        var div2_4 = new TVclStackPanel(div2Cont, "div2_4_" + _this.ID, 3, [[0, 12, 12], [3, 4, 5], [4, 3, 5], [4, 3, 5], [4, 3, 5]]);
        var div2_4_1Cont = div2_4.Column[1].This; //Label                                                           
        var div2_4_2Cont = div2_4.Column[2].This; //Text                                                            
        var div2_5 = new TVclStackPanel(div2Cont, "div2_5_" + _this.ID, 3, [[0, 12, 12], [3, 4, 5], [4, 3, 5], [4, 3, 5], [4, 3, 5]]);
        var div2_5_1Cont = div2_5.Column[1].This; //Label 
        var div2_5_2Cont = div2_5.Column[2].This; //Text
        var div2_6 = new TVclStackPanel(div2Cont, "div2_5_" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
        var div2_6_1Cont = div2_6.Column[0].This; //Botono aceptar 

        $(div2_1.Row.This).css("padding-bottom", "10px"); 
        $(div2_2.Row.This).css("padding-bottom", "10px"); 
        $(div2_3.Row.This).css("padding-bottom", "10px"); 
        $(div2_4.Row.This).css("padding-bottom", "10px"); 
        $(div2_5.Row.This).css("padding-bottom", "10px");         
         
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.Shared.Contact.UfrCMDBContact.prototype.InitializeComponent (1) ", e);
    }
     
    try {
        //BTN ADD
        var btnAdd = new TVclImagen(_this.divAddCont, "btnAdd_" + _this.ID);
        btnAdd.Src = "image/24/add.png";
        btnAdd.Cursor = "pointer";
        btnAdd.onClick = function myfunction() {
            _this.btnAdd(_this, btnAdd, _this.div1_BdCont);
        }
        btnAdd.This.style.width = "18px";
        btnAdd.This.style.height = "18px";

        var LabBtnAdd = new Vcllabel(_this.divAddCont, "GridAttached_Hd_" + ID + "1", TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "lblbtnAdd"));
        $(LabBtnAdd).css("margin-left", "10px");

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.Shared.Contact.UfrCMDBContact.prototype.InitializeComponent (2) ", e);
    }


    /////////////////////////////////////
    //texts  
    try {
        var lab = new Vcllabel(div2_1_1Cont, "lbltxtStreet_" + _this.ID, TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "lbltxtStreet"));
        lab.classList.add("xlabel");

        _this.Text1 = new Uinput(div2_1_2Cont, TImputtype.text, "TxtAddress1_" + _this.ID, "0", false);
        $(_this.Text1).addClass("TextFormat");
        $(_this.Text1).css("width","100%");
        _this.Text1.classList.add("text");
        _this.Text1.value = "";

        var lblcmbCountry = new Vcllabel(div2_2_1Cont, "lblcmbCountry_" + _this.ID, TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "lblcmbCountry"));
        lblcmbCountry.classList.add("xlabel");

        var OpenDataSetAddress2 = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "LOAD_CMDBCOUNTRY", new Array());
        _this.Text2 = new TVclComboBox2(div2_2_2Cont, "selectCmbAddress2_" + _this.ID)
        $(_this.Text2.This).addClass("TextFormat");
        $(_this.Text2.This).css("width", "100%");
        if (OpenDataSetAddress2.ResErr.NotError && OpenDataSetAddress2.DataSet.RecordCount > 0) {
            for (var i = 0; i < OpenDataSetAddress2.DataSet.RecordCount; i++) {
                if (OpenDataSetAddress2.DataSet.Records[i].Fields[1].Value != "None") {
                    var VclComboBoxItem = new TVclComboBoxItem();
                    VclComboBoxItem.Value = OpenDataSetAddress2.DataSet.Records[i].Fields[0].Value;
                    VclComboBoxItem.Text = OpenDataSetAddress2.DataSet.Records[i].Fields[1].Value;
                    VclComboBoxItem.Tag = OpenDataSetAddress2.DataSet.Records[i].Fields[1].Value;
                    _this.Text2.AddItem(VclComboBoxItem);
                }
            }
        }
        _this.Text2.selectedIndex = 0;


        var lbltxtProvince = new Vcllabel(div2_3_1Cont, "lbltxtProvince_" + _this.ID, TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "lbltxtProvince"));
        lbltxtProvince.classList.add("xlabel");
        _this.Text3 = new Uinput(div2_3_2Cont, TImputtype.text, "TxtAddress3_" + _this.ID, "0", false);
        _this.Text3.classList.add("text");
        $(_this.Text3).css("width", "100%");
        $(_this.Text3).addClass("TextFormat");
        _this.Text3.value = "";

        var lbltxtPostal = new Vcllabel(div2_4_1Cont, "lbltxtPostal_" + _this.ID, TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "lbltxtPostal"));
        lbltxtPostal.classList.add("xlabel");
        _this.Text4 = new Uinput(div2_4_2Cont, TImputtype.text, "TxtAddress6_" + _this.ID, "0", false);
        _this.Text4.classList.add("text");
        $(_this.Text4).addClass("TextFormat");
        $(_this.Text4).css("width", "100%");
        _this.Text4.value = "";

        var lblcmbStatus = new Vcllabel(div2_5_1Cont, "lblcmbStatus_" + _this.ID, TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "lblcmbStatus"));
        lblcmbStatus.classList.add("xlabel");

        OpenDataSetSystemStatus = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDU_004", new Array());
        _this.Text5 = new TVclComboBox2(div2_5_2Cont, "selectCmbSystemStatus_" + _this.ID);
        $(_this.Text5.This).addClass("TextFormat");
        $(_this.Text5.This).css("width", "100%");
        if (OpenDataSetSystemStatus.ResErr.NotError && OpenDataSetSystemStatus.DataSet.RecordCount > 0) {
            for (var i = 0; i < OpenDataSetSystemStatus.DataSet.RecordCount; i++) {
                if (OpenDataSetSystemStatus.DataSet.Records[i].Fields[1].Value != "None") {
                    var VclComboBoxItem = new TVclComboBoxItem();
                    VclComboBoxItem.Value = OpenDataSetSystemStatus.DataSet.Records[i].Fields[0].Value;
                    VclComboBoxItem.Text = OpenDataSetSystemStatus.DataSet.Records[i].Fields[1].Value;
                    VclComboBoxItem.Tag = OpenDataSetSystemStatus.DataSet.Records[i].Fields[1].Value;
                    _this.Text5.AddItem(VclComboBoxItem);
                }
            }
        }
        _this.Text5.selectedIndex = 0;
        $(div2_5_1Cont).css("display", "none");
        $(div2_5_2Cont).css("display", "none");

        //Boton aceptar
        var btnAcept = new TVclButton(div2_6_1Cont, "btnAcept_" + _this.ID);
        btnAcept.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnAcept");
        btnAcept.VCLType = TVCLType.BS;
        btnAcept.onClick = function myfunction() {
            _this.SaveContact(_this, btnAcept);
        }

        $(btnAcept.This).css("float", "right");
        $(btnAcept.This).css("margin", "5px");

 
        

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.Shared.Contact.UfrCMDBContact.prototype.InitializeComponent (4) ", e);
    }
    
    _this.Initialize();
}


ItHelpCenter.SD.Shared.Contact.UfrCMDBContact.prototype.HeaderGrid = function (Object, ID) {
    var _this = this.TParent();

    try {
        $(Object).html(""); 

        var DivsHead = new TVclStackPanel(Object, "GridContact_Hd_" + ID, 4, [[3, 7, 1, 1], [4, 6, 1, 1], [4, 6, 1, 1], [4, 6, 1, 1], [4, 6, 1, 1]]);
        var DivH1 = DivsHead.Column[0].This;
        var DivH2 = DivsHead.Column[1].This;
        var DivH3 = DivsHead.Column[2].This;
        var DivH4 = DivsHead.Column[3].This;

        var GridContact_Hd1 = new Vcllabel(DivH1, "GridContact_Hd" + ID + "1", TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "headerType"));
        var GridContact_Hd2 = new Vcllabel(DivH2, "GridContact_Hd" + ID + "2", TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "headerContact")); 

        $(Object).css("padding", "0px");

        _this.DIVhdEdit = DivH3; 

        $(DivH3).css("text-align", "center");
        $(DivH4).css("text-align", "center");

        $(DivH1).css("overflow", "hidden");
        $(DivH2).css("overflow", "hidden");
        $(DivH3).css("overflow", "hidden");
        $(DivH4).css("overflow", "hidden");

        $(DivH1).css("Padding-left", "0px");
        $(DivH2).css("Padding-left", "0px");
        $(DivH3).css("Padding-left", "0px");
        $(DivH4).css("Padding-left", "0px");
        $(DivH1).css("Padding-right", "0px");
        $(DivH2).css("Padding-right", "0px");
        $(DivH3).css("Padding-right", "0px");
        $(DivH4).css("Padding-right", "0px");

        $("#" + DivsHead.Row.This.id + " div").css('padding', "0px");
        $("#" + DivsHead.Row.This.id + " .control-label").css('font-weight', "normal");

        $(DivsHead.Row.This).css('padding', "0xp");
        $(DivsHead.Row.This).css('width', "100%");
        $(DivsHead.Row.This).css('margin', "0px");
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("UfrCMDBContact.js ItHelpCenter.SD.Shared.Contact.UfrCMDBContact.prototype.HeaderGrid", e);
    } 
}

ItHelpCenter.SD.Shared.Contact.UfrCMDBContact.prototype.NewGridFile = function (Object, IDx, inIDCONTACT, inType, inContact) {
    var _this = this.TParent(); 

    try {
        idx = 'row_' + IDx;
        StackPanelDef = new Array(1);
        StackPanelDef[0] = new Array(4);
        var StackPanel = VclDivitions(Object, idx, StackPanelDef);
        BootStrapCreateRow(StackPanel[0].This);
        BootStrapCreateColumns(StackPanel[0].Child, [[3, 7, 1, 1], [4,6, 1, 1], [4, 6, 1, 1], [4, 6, 1, 1], [4, 6, 1, 1]]);
        
        $(StackPanel[0].Child[0].This).css("padding-Top", "3px");
        $(StackPanel[0].Child[1].This).css("padding-Top", "3px");
        $(StackPanel[0].Child[2].This).css("padding-Top", "6px");
        $(StackPanel[0].Child[3].This).css("padding-Top", "6px");

        $(StackPanel[0].Child[0].This).css("padding-left", "0px");
        $(StackPanel[0].Child[1].This).css("padding-left", "0px");
        $(StackPanel[0].Child[2].This).css("padding-left", "0px");
        $(StackPanel[0].Child[3].This).css("padding-left", "0px");

        $(StackPanel[0].Child[0].This).css("padding-right", "0px");
        $(StackPanel[0].Child[1].This).css("padding-right", "0px");
        $(StackPanel[0].Child[2].This).css("padding-right", "0px");
        $(StackPanel[0].Child[3].This).css("padding-right", "0px"); 

        var ContactText1 = new TVclComboBox2(StackPanel[0].Child[0].This, "Con_Text1_" + idx);
        if (SysCfg.App.Properties.Device.value == SysCfg.App.TDevice.Desktop.value) {
            $(ContactText1.This).css("width", "98%");
        } else {
            $(ContactText1.This).css("width", "100%");
        }       
        $(ContactText1.This).addClass("TextFormat");

        var indexCombo = 0;
        OpenDataSetContactType = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "LOAD_CMDBCONTACTTYPE", new Array());
        if (OpenDataSetContactType.ResErr.NotError && OpenDataSetContactType.DataSet.RecordCount > 0)
        {
            for (var i = 0; i < OpenDataSetContactType.DataSet.RecordCount; i++)
            {
                if (OpenDataSetContactType.DataSet.Records[i].Fields[1].Value != "None")
                {  
                    var VclComboBoxItem = new TVclComboBoxItem();
                    VclComboBoxItem.Value = OpenDataSetContactType.DataSet.Records[i].Fields[0].Value;
                    VclComboBoxItem.Text = OpenDataSetContactType.DataSet.Records[i].Fields[1].Value;
                    VclComboBoxItem.Tag = OpenDataSetContactType.DataSet.Records[i].Fields[1].Value;
                    ContactText1.AddItem(VclComboBoxItem);

                    if (inType == OpenDataSetContactType.DataSet.Records[i].Fields[1].Value) {
                        indexCombo = ContactText1.Options.length - 1;
                    }
                } 
            }
        }
        ContactText1.selectedIndex = indexCombo;
        

        ContactText1.onChange = function () {
            _this.ContactText1Change(_this, ContactText1, StackPanel[0].Child[2].This, inType, inIDCONTACT);
        }


        var ContactText2 = new TVclTextBox(StackPanel[0].Child[1].This, "Con_Text2_" + idx, "", false);
        ContactText2.This.value = inContact;
        if (SysCfg.App.Properties.Device.value == SysCfg.App.TDevice.Desktop.value) {
            $(ContactText2.This).css("width", "98%");
        } else {
            $(ContactText2.This).css("width", "100%");
        } 
        $(ContactText2.This).addClass("TextFormat");
        ContactText2.This.onkeypress = function validar(e) {
            $(StackPanel[0].Child[2].This).css("display", "");
        }
         
        _this.Table[IDx] = [[ContactText1], [ContactText2.This], inIDCONTACT];

        $(StackPanel[0].Child[2].This).addClass("Divtooltip");

        if (inIDCONTACT > 0) {
            //BTN EDIT
            var btnEdit = new TVclImagen(StackPanel[0].Child[2].This, "btnEdit_" + idx);
            btnEdit.Src = "image/24/Save-as.png";
            btnEdit.Cursor = "pointer";
            btnEdit.onClick = function myfunction() {
                _this.btnEdit(_this, btnEdit, IDx, inIDCONTACT);
                $(StackPanel[0].Child[2].This).css("display", "none");
            }

            $(StackPanel[0].Child[2].This).css("text-align", "center");
            btnEdit.This.style.width = "18px";
            btnEdit.This.style.height = "18px";

            var LabBtnEdit = new Vcllabel(StackPanel[0].Child[2].This, "LabBtnEdit" + ID + "1", TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnEdit"));
            $(LabBtnEdit).addClass("tooltiptext");
            $(LabBtnEdit).css("margin-left", "10px");

            $(StackPanel[0].Child[2].This).css("display", "none");
        } else {
            //BTN ADD
            var btnSave = new TVclImagen(StackPanel[0].Child[2].This, "btnEdit_" + idx);
            btnSave.Src = "image/24/Save.png";
            btnSave.Cursor = "pointer";
            btnSave.onClick = function myfunction() {
                _this.btnSave(_this, btnSave, IDx);
                $(StackPanel[0].Child[2].This).css("display", "none");
            }

            $(StackPanel[0].Child[2].This).css("text-align", "center");
            btnSave.This.style.width = "18px";
            btnSave.This.style.height = "18px";

            var LabbtnSave = new Vcllabel(StackPanel[0].Child[2].This, "LabBtnEdit" + ID + "1", TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnAdd"));
            $(LabbtnSave).addClass("tooltiptext");
            $(LabbtnSave).css("margin-left", "10px");
        }


        $(StackPanel[0].Child[3].This).addClass("Divtooltip");
        //BTN DEL
        var btnDel = new TVclImagen(StackPanel[0].Child[3].This, "btnDel_" + idx);
        btnDel.Src = "image/24/delete.png";
        btnDel.Cursor = "pointer";
        btnDel.onClick = function myfunction() {
            _this.btnDel(_this, btnDel, IDx, inIDCONTACT);

            StackPanel[0].This.innerHTML = "";
            $(StackPanel[0].This).css("display", "none");
        }
        btnDel.This.style.width = "18px";
        btnDel.This.style.height = "18px";

        var LabBtnDel = new Vcllabel(StackPanel[0].Child[3].This, "LabBtnEdit" + ID + "1", TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnDel"));
        $(LabBtnDel).addClass("tooltiptext");
        $(LabBtnDel).css("margin-left", "10px");

        $(StackPanel[0].Child[3].This).css("text-align", "center");

        $(StackPanel[0].This).css('width', "100%");        
        $(StackPanel[0].This).css('margin', "0px");        
        $(StackPanel[0].This).css('margin-bottom', "5px");
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("UfrCMDBContact.js ItHelpCenter.SD.Shared.Contact.UfrCMDBContact.prototype.NewGridFile ", e);
    }

}
