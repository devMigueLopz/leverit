﻿ItHelpCenter.SD.Shared.Contact.UfrCMDBContact = function (inObject, inID, incallback, inID_User, inThisParent, inValida) {
    this.ObjectHtml = inObject;
    this.Callback = incallback;
    this.IDCategoryDetail = 0;
    this.divshow = null;
    this.ID = inID;
    this.NRows = 0;
    this.NRowsIni = 0;
    this.NRowsFin = 0;
    this.Table = new Array();
    this.Controls = new Array();
    this.THISParent = inThisParent;
    this.Flag_SAVEEDIT = false;
    this.Valida = inValida; //validar que exista un eMail antes de salir

    this.IDCMDBUSER = UsrCfg.Properties.UserATRole.User.IDCMDBUSER;
    if (inID_User > 0 ) {
        this.IDCMDBUSER = inID_User;
    }
    this.DataTable = null;

    this.TParent = function () {
        return this;
    }.bind(this);

    var _this = this.TParent();
    //TRADUCCION     
    this.Mythis = "frCMDBContact";
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lblbtnAdd", "Add");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lblbtnEdit", "Edit");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lblbtnDelete", "Delete");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lbltxtStreet", "Street Address");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lblcmbCountry", "Country");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lbltxtProvince", "Province/State/City");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lbltxtPostal", "Zip/Postal Code");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lblcmbStatus", "System Status");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lblbtnSave", "Save");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lblbtnCancel", "Cancel");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "headerType", "TYPE");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "headerContact", "CONTACT");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnEdit", "UPDATE CHANGES");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnDel", "DELETE CONTACT");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnAdd", "SAVE CONTACT");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "AlertEdit", "File Edit Succesful");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnAcept", "Out");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lines1", "field description is empty");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2", "There must be at least one email");

    _this.InitializeComponent();
}.Implements(new Source.Page.Properties.Page());


ItHelpCenter.SD.Shared.Contact.UfrCMDBContact.prototype.Initialize = function () {
    try {
        var _this = this.TParent();

        //LOAD CONTACT TYPES
        _this.HeaderGrid(_this.div1_HdCont, _this.ID);

        var Param = new SysCfg.Stream.Properties.TParam();
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBUSER.FieldName, _this.IDCMDBUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        _this.DataTable = SysCfg.DB.SQL.Methods.Open("Atis", "LOAD_DATAGRID_CONTACTTYPE", Param.ToBytes());

        if ((_this.DataTable.ResErr.NotError) && (_this.DataTable.DataSet.RecordCount > 0)) {
            datos = true;
            _this.NRows = _this.DataTable.DataSet.RecordCount;
            _this.NRowsIni = _this.DataTable.DataSet.RecordCount;
            _this.NRowsFin = _this.DataTable.DataSet.RecordCount;

            for (var i = 0; i < _this.DataTable.DataSet.RecordCount; i++) {
                try {
                    _this.NewGridFile(_this.div1_BdCont, i,
                        _this.DataTable.DataSet.Records[i].Fields[0].Value,
                        _this.DataTable.DataSet.Records[i].Fields[1].Value,
                        _this.DataTable.DataSet.Records[i].Fields[2].Value);
                } catch (e) {
                    SysCfg.Log.Methods.WriteLog("UfrCMDBContact.js ItHelpCenter.SD.Shared.Contact.UfrCMDBContact.prototype.InitializeComponent", e);
                }
            }
        }
        else {
            $(_this.div1_BdCont).html("");
        }


        //LOAD ADDRESS DATA 
        var Param = new SysCfg.Stream.Properties.TParam();
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBUSER.FieldName, _this.IDCMDBUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        _this.DataTableAdress = SysCfg.DB.SQL.Methods.Open("Atis", "LOAD_DATA_ADDRESS", Param.ToBytes());

        if (_this.DataTableAdress.DataSet.RecordCount > 0) {
            _this.Flag_SAVEEDIT = false;
            _this.Text1.value = _this.DataTableAdress.DataSet.Records[0].Fields[2].Value;

            for (j = 0; j < _this.Text2.Options.length; j++) {
                if (parseInt(_this.Text2.Options[j].Value) == _this.DataTableAdress.DataSet.Records[0].Fields[3].Value) {
                    _this.Text2.selectedIndex = j;
                    j = _this.Text2.Options.length;
                }
            }
            _this.Text3.value = _this.DataTableAdress.DataSet.Records[0].Fields[4].Value;

            _this.Text4.value = _this.DataTableAdress.DataSet.Records[0].Fields[7].Value;

            for (j = 0; j < _this.Text5.Options.length; j++) {
                if (parseInt(_this.Text5.Options[j].Value) == _this.DataTableAdress.DataSet.Records[0].Fields[8].Value) {
                    _this.Text5.selectedIndex = j;
                    j = _this.Text5.Options.length;
                }
            }
        } else {
            _this.Flag_SAVEEDIT = true;
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("UfrCMDBContact.js ItHelpCenter.SD.Shared.Contact.UfrCMDBContact.prototype.Initialize (1) ", e);
    } 
}
 
ItHelpCenter.SD.Shared.Contact.UfrCMDBContact.prototype.Valida_Contact = function () {
    //validar que el contacto tenga minimo un eMail

    var _this = this.TParent();

    try {
        var Find_eMail = false;

        var Param = new SysCfg.Stream.Properties.TParam();
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBUSER.FieldName, _this.IDCMDBUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var DTCONTACTTYPE = SysCfg.DB.SQL.Methods.Open("Atis", "LOAD_DATAGRID_CONTACTTYPE", Param.ToBytes());
        if (DTCONTACTTYPE.ResErr.NotError) {
            if (DTCONTACTTYPE.DataSet.RecordCount > 0) {               
                var Contacto = "";
                for (var i = 0; i < DTCONTACTTYPE.DataSet.RecordCount; i++) {
                    if (DTCONTACTTYPE.DataSet.Records[i].Fields[1].Value == "eMail") {
                        Find_eMail = true;
                        i = DTCONTACTTYPE.DataSet.RecordCount;
                    }
                } 
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("TMain.prototype.ValidaContactType ", e);
    } finally {
        Param.Destroy();
    }

    return Find_eMail;
}

ItHelpCenter.SD.Shared.Contact.UfrCMDBContact.prototype.SaveContact = function (sender, e) {
    var _this = sender;

    for (var i = 0; i < _this.Table.length; i++) {
        if (_this.Table[i][2] == 0) {
            _this.btnSave(_this, null, i);
        }
    }
   

    ///////////////////////////////////////////////////
    try {
        try {
            var Address1 = _this.Text1.value;
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("UfrCMDBContact.js ItHelpCenter.SD.Shared.Contact.UfrCMDBContact.prototype.SaveContact", e);
            var Address1 = "0";
        }
        try {
            var Address2 = _this.Text2.Value;
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("UfrCMDBContact.js ItHelpCenter.SD.Shared.Contact.UfrCMDBContact.prototype.SaveContact", e);
            var Address2 = "0";
        }
        try {
            var Address3 = _this.Text3.value;
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("UfrCMDBContact.js ItHelpCenter.SD.Shared.Contact.UfrCMDBContact.prototype.SaveContact", e);
            var Address3 = "0";
        }
        try {
            var Address6 = _this.Text4.value;
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("UfrCMDBContact.js ItHelpCenter.SD.Shared.Contact.UfrCMDBContact.prototype.SaveContact", e);
            var Address6 = "0";
        }
        try {
            var SystemStatus = _this.Text5.Value;
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("UfrCMDBContact.js ItHelpCenter.SD.Shared.Contact.UfrCMDBContact.prototype.SaveContact", e);
            var SystemStatus = "0";
        }
          
        var Param = new SysCfg.Stream.Properties.TParam();
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.IDCMDBUSER.FieldName, _this.IDCMDBUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS1.FieldName, Address1, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS2.FieldName, parseInt(Address2), SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS3.FieldName, Address3, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS4.FieldName, "", SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS5.FieldName, "", SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS6.FieldName, Address6, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.IDSYSTEMSTATUS.FieldName, parseInt(SystemStatus), SysCfg.DB.Properties.TMotor.OLE, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.IDSYSTEMSTATUS.FieldName, GetIDSystemStatus(), SysCfg.DB.Properties.TMotor.OLE, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        if (_this.Flag_SAVEEDIT) {
            Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "SAVE_CMDBUSERADDRESS", Param.ToBytes());
        } else {
            Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "EDIT_CMDBUSERADDRESS", Param.ToBytes());
        }
        
        if (!Exec.ResErr.NotError) {
            SysCfg.Log.Methods.WriteLog("UfrCMDBContact.js ItHelpCenter.SD.Shared.Contact.UfrCMDBContact.prototype.SaveContact", Exec.ResErr.Mesaje);
        } 
         
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("UfrCMDBContact.js ItHelpCenter.SD.Shared.Contact.UfrCMDBContact.prototype.SaveContact ", e);
    } finally {
        Param.Destroy();
    }

    if (_this.Valida == true) {
        var result = _this.Valida_Contact();
        if (result == false) {
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2"));
            return;
        }         
    }
    _this.Callback(_this.THISParent);
}

ItHelpCenter.SD.Shared.Contact.UfrCMDBContact.prototype.ContactText1Change = function (sender, e, Object, inType, inIDCONTACT) {
    _this = sender;
    try {
        if (inIDCONTACT > 0) {
            if (e.Text != inType) {
                $(Object).css("display", "");
            } else {
                $(Object).css("display", "none");
            }
        } else {
            $(Object).css("display", "");
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.Shared.Contact.UfrCMDBContact.prototype.ContactText1Change ", e);
    }
    
}

 
//BOTONES
ItHelpCenter.SD.Shared.Contact.UfrCMDBContact.prototype.btnAdd = function (sender, e, Object) {
    _this = sender;

    try {
        _this.NRows = _this.NRows + 1; // mide las lines que hay en x momento
        _this.NRowsFin = _this.NRowsFin + 1; // mide el maximo de lines que llegan a haber 
        _this.NewGridFile(Object, (_this.NRowsFin - 1), 0, "", "");
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("UfrCMDBContact.js ItHelpCenter.SD.Shared.Contact.UfrCMDBContact.prototype.btnAdd", e);
    } 
}

ItHelpCenter.SD.Shared.Contact.UfrCMDBContact.prototype.btnSave = function (sender, e, IDx) {
    _this = sender;
    try {
        var IDCMDBCONTACTTYPE = _this.Table[IDx][0][0].Value;
        var txtContact = _this.Table[IDx][1][0].value;
        if (txtContact != null && txtContact != "") {

            var Param = new SysCfg.Stream.Properties.TParam();
            try {
                var IDCMDBUSERCONTACTTYPE = 0;
                var IDSYSTEMSTATUS = 0;
                var CONTACTDEFINE = txtContact;

                Param.Inicialize();

                Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBUSERCONTACTTYPE.FieldName, IDCMDBUSERCONTACTTYPE, SysCfg.DB.Properties.TMotor.OLE, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBUSER.FieldName, _this.IDCMDBUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBCONTACTTYPE.FieldName, IDCMDBCONTACTTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDSYSTEMSTATUS.FieldName, IDSYSTEMSTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                Param.AddString(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.CONTACTDEFINE.FieldName, CONTACTDEFINE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

                Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "SAVE_CMDBUSERCONTACTTYPE", Param.ToBytes());
                if (!Exec.ResErr.NotError) {
                    alert(Exec.ResErr.Mesaje);
                }
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.Shared.Contact.UfrCMDBContact.prototype.SaveContact (1) ", e);
            }
            finally {
                Param.Destroy();
            }
        }
    } catch (e) {

    } SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.Shared.Contact.UfrCMDBContact.prototype.SaveContact (2) ", e);

}

ItHelpCenter.SD.Shared.Contact.UfrCMDBContact.prototype.btnEdit = function (sender, e, IDx, inIDCONTACT) {
    _this = sender;
     
    try {
        var CmbContactType = _this.Table[IDx][0][0].Value;
        var txtContact = _this.Table[IDx][1][0].value;
        if (txtContact.trim() == "") {
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "lines1"));
            return;
        }


        var Param = new SysCfg.Stream.Properties.TParam();
        try {
            var IDCMDBCONTACTTYPE = CmbContactType;
            var IDCMDBUSERCONTACTTYPE = inIDCONTACT;
            var IDSYSTEMSTATUS = 0;
            var CONTACTDEFINE = txtContact;

            Param.Inicialize();

            Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBUSERCONTACTTYPE.FieldName, IDCMDBUSERCONTACTTYPE, SysCfg.DB.Properties.TMotor.OLE, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBUSER.FieldName, _this.IDCMDBUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBCONTACTTYPE.FieldName, IDCMDBCONTACTTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDSYSTEMSTATUS.FieldName, IDSYSTEMSTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            Param.AddString(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.CONTACTDEFINE.FieldName, CONTACTDEFINE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

            Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "EDIT_CMDBUSERCONTACTTYPE", Param.ToBytes());
            if (!Exec.ResErr.NotError) {
                alert(Exec.ResErr.Mesaje);
            } else {
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "AlertEdit"));
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.Shared.Contact.UfrCMDBContact.prototype.SaveContact (1) ", e);
        }
        finally {
            Param.Destroy();
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.Shared.Contact.UfrCMDBContact.prototype.SaveContact (2) ", e);
    } 
}

ItHelpCenter.SD.Shared.Contact.UfrCMDBContact.prototype.btnDel = function (sender, e, IDx, IDCMDBUSERCONTACTTYPE) {
    _this = sender;
     
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBUSERCONTACTTYPE.FieldName, IDCMDBUSERCONTACTTYPE, SysCfg.DB.Properties.TMotor.OLE, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "DELETE_CMDBUSERCONTACTTYPE", Param.ToBytes());
        if (Exec.ResErr.NotError)
        {
            _this.Table[IDx] = [];
            _this.NRows = _this.NRows - 1;
            if (_this.NRows < 1) {
                _this.NRows = 1;
                _this.NewGridFile(_this.div1_BdCont, (_this.NRowsFin - 1), 0, "", "");
            }
        }
        else
        {
            SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.Shared.Contact.UfrCMDBContact.prototype.btnDel ", Exec.ResErr.Mesaje);
        }
    }catch (e) {
        SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.Shared.Contact.UfrCMDBContact.prototype.btnDel ", e);
    }
    finally
    {
        Param.Destroy();
    } 
 
}
