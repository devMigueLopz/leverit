﻿ 
ItHelpCenter.SD.Shared.RelatedCase.frSDRelatedCase = function (inMenuObject, inObject, inID, incallback, inIDSDCASE, inIDUSER, inIDSDTYPEUSER, inThisParent) {
    this.TParent = function () {
        return this;
    }.bind(this);

    var _this = this.TParent();

    this.ObjectHtml = inObject;
    this.MenuObject = inMenuObject;
    this.CallbackModalResult = incallback;   
    this.ID = inID;
    this.ThisParent = inThisParent;

    this.SDCASE_RELATION = null;  
    this.OpenCIAfectados = null;
    this.DataGridManager = null;        
       
    this.IDSDCASE = inIDSDCASE;
    this.IDUSER = inIDUSER;
    this.IDSDTYPEUSER = inIDSDTYPEUSER;     

    this.TSDCASE_RELATION = function()
    {
        this.IDCMDBCI = 0;
        this.IDSDCASE = 0;
        //this.IDSDCASE_OF = 0; 
        this.IDSDCASE_RELATION = 0;
        this.IDSDCASE_RELATIONTYPE = 0;
        //this.IDSDCASE_THIS = 0;
        this.IDSDTYPEUSER = 0;
        this.IDSYSTEMSTATUS = 1;
        this.RELATIONS_DESCRIPTION = "";
        this.RELATIONS_TITLE = "";
        this._SDCASE_OF_TITLE = "";
        this._SDCASE_OF_CATEGORY = "";
        this._SDCASE_OF_CATEGORYNAME = "";
    }

    //TRADUCCION          
    this.Mythis = "frSDRelatedCase";

    UsrCfg.Traslate.GetLangText(_this.Mythis, "lbl01.Text", "Related Cases.");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lbl001.Text", "Edit");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lbl002.Text", "Delete");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lbl003.Text", "Add");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lbl004.Text", "View");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3", "Are you sure you want to delete the record from the database?");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines4", "Are you sure you want to delete this item?");

    
    _this.InitializeComponent();

}

ItHelpCenter.SD.Shared.RelatedCase.frSDRelatedCase.prototype.Initialize = function () {
    var _this = this.TParent();
    _this.SDCASE_RELATION = new _this.TSDCASE_RELATION();
    _this.CargarDatosGrid();
}


ItHelpCenter.SD.Shared.RelatedCase.frSDRelatedCase.prototype.CargarDatosGrid = function () {
    var _this = this.TParent();
    try {
        var Param = new SysCfg.Stream.Properties.TParam();
        try
        {
            Param.Inicialize();
            Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE_RELATION.IDSDCASE_THIS.FieldName, _this.IDSDCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE_RELATION.IDSDCASE_OF.FieldName, _this.IDSDCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            var OpenCIAfectados = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "SDCASE_RELATIONGRID", Param.ToBytes());
                                    
            if (OpenCIAfectados.ResErr.NotError)
            {
                _this.OpentoTableView(OpenCIAfectados);
            }
        }
        finally
        {
            Param.Destroy();
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.Shared.RelatedCase.frSDRelatedCase.prototype.CargarDatosGrid ", e);
    }
}

ItHelpCenter.SD.Shared.RelatedCase.frSDRelatedCase.prototype.OpentoTableView = function (OpenDataSet) {
    var _this = this.TParent();
    try {
        _this.GRID_DIV.innerHTML = "";
        var Grilla = new TGrid(_this.GRID_DIV, "", ""); 
        Grilla.AutoTranslate = true;
        Grilla.DataSource = OpenDataSet.DataSet;
   
        Grilla.OnRowClick = function (sender, EventArgs) {
            _this.RowSelected = EventArgs; 
             
            _this.SDCASE_RELATION.IDSDCASE_RELATION = _this.RowSelected.Cells[0].Value;
            _this.SDCASE_RELATION.IDSDCASE = _this.RowSelected.Cells[2].Value;
            _this.SDCASE_RELATION.RELATIONS_TITLE = _this.RowSelected.Cells[3].Value;
            _this.SDCASE_RELATION.RELATIONS_DESCRIPTION = _this.RowSelected.Cells[4].Value;
            _this.SDCASE_RELATION._SDCASE_OF_TITLE = _this.RowSelected.Cells[5].Value;
            _this.SDCASE_RELATION._SDCASE_OF_CATEGORY = _this.RowSelected.Cells[6].Value;
            _this.SDCASE_RELATION._SDCASE_OF_CATEGORYNAME = _this.RowSelected.Cells[7].Value;
            _this.SDCASE_RELATION.IDCMDBCI = _this.RowSelected.Cells[10].Value;
            _this.SDCASE_RELATION.IDSDTYPEUSER = _this.RowSelected.Cells[11].Value;
            _this.SDCASE_RELATION.IDSYSTEMSTATUS = _this.RowSelected.Cells[12].Value;
        }

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.Shared.RelatedCase.frSDRelatedCase.prototype.OpentoTableView ", e);
    }
}
 

ItHelpCenter.SD.Shared.RelatedCase.frSDRelatedCase.prototype.SDRelatedCaseEditorAdd = function () {
    var _this = this.TParent();
    try {
        var Param = new SysCfg.Stream.Properties.TParam();
        try
        {
            Param.Inicialize();
            Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE_RELATION.IDCMDBCI.FieldName, _this.SDCASE_RELATION.IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE_RELATION.IDSDCASE_OF.FieldName, _this.SDCASE_RELATION.IDSDCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE_RELATION.IDSDCASE_RELATIONTYPE.FieldName, _this.SDCASE_RELATION.IDSDCASE_RELATIONTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE_RELATION.IDSDCASE_THIS.FieldName, _this.IDSDCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE_RELATION.IDSDTYPEUSER.FieldName, _this.SDCASE_RELATION.IDSDTYPEUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE_RELATION.IDSYSTEMSTATUS.FieldName, _this.SDCASE_RELATION.IDSYSTEMSTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            Param.AddText(UsrCfg.InternoAtisNames.SDCASE_RELATION.RELATIONS_DESCRIPTION.FieldName, _this.SDCASE_RELATION.RELATIONS_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            Param.AddText(UsrCfg.InternoAtisNames.SDCASE_RELATION.RELATIONS_TITLE.FieldName, _this.SDCASE_RELATION.RELATIONS_TITLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "SDCASE_RELATION_ADD", Param.ToBytes());
            if (!Exec.ResErr.NotError)
            {
                SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.Shared.RelatedCase.frSDRelatedCase.prototype.SDRelatedCaseEditorAdd ", Exec.ResErr.Mesaje);
            }
        }
        finally
        {
            Param.Destroy();
            _this.CargarDatosGrid();
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.Shared.RelatedCase.frSDRelatedCase.prototype.SDRelatedCaseEditorAdd ", e);
    }
}

ItHelpCenter.SD.Shared.RelatedCase.frSDRelatedCase.prototype.SDRelatedCaseEditorUpd = function () {
    var _this = this.TParent();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE_RELATION.IDSDCASE_RELATION.FieldName, _this.SDCASE_RELATION.IDSDCASE_RELATION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other                                
        Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE_RELATION.IDSDCASE_RELATIONTYPE.FieldName, _this.SDCASE_RELATION.IDSDCASE_RELATIONTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);                
        Param.AddText(UsrCfg.InternoAtisNames.SDCASE_RELATION.RELATIONS_DESCRIPTION.FieldName, _this.SDCASE_RELATION.RELATIONS_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.SDCASE_RELATION.RELATIONS_TITLE.FieldName, _this.SDCASE_RELATION.RELATIONS_TITLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal); 
        var Open = SysCfg.DB.SQL.Methods.Open("Atis", "SDCASE_RELATION_UPD", Param.ToBytes());
        if (Open.ResErr.NotError)
        {
            _this.CargarDatosGrid();
        }
        else
        {
            SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.Shared.RelatedCase.frSDRelatedCase.prototype.SDRelatedCaseEditorUpd ", Exec.ResErr.Mesaje);
        }
    }
    finally
    {
        Param.Destroy();
    }
}

ItHelpCenter.SD.Shared.RelatedCase.frSDRelatedCase.prototype.ADD_Relation = function (inSDCASE_RELATION) {
    var _this = this.TParent(); 
    try {
        $(_this.DivMian).css("display", "none");
        $(_this.DivChild).css("display", "");
        _this.DivChild.innerHTML = "";
         
        var frSDRelatedCaseEditor = new ItHelpCenter.SD.Shared.RelatedCase.frSDRelatedCaseEditor(_this.DivChild, function (outRest) {

            $(_this.DivMian).css("display", "");
            $(_this.DivChild).css("display", "none");

            if (outRest != null) {
                _this.SDCASE_RELATION = outRest;
                _this.SDRelatedCaseEditorAdd()
            }
        }, inSDCASE_RELATION);

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.Shared.RelatedCase.frSDRelatedCase.prototype.btnVerRelated_Click ", e);
    }
}


//BOTONES
ItHelpCenter.SD.Shared.RelatedCase.frSDRelatedCase.prototype.btnEditarRelated_Click = function (sender, e) {
    var _this = sender;
    try {
        $(_this.DivMian).css("display", "none");
        $(_this.DivChild).css("display", "");
        _this.DivChild.innerHTML = "";

        var frSDRelatedCaseEditor = new ItHelpCenter.SD.Shared.RelatedCase.frSDRelatedCaseEditor(_this.DivChild, function (outRest) {

            $(_this.DivMian).css("display", "");
            $(_this.DivChild).css("display", "none");
         
            if (outRest != null) {
                _this.SDCASE_RELATION = outRest;
                _this.SDRelatedCaseEditorUpd()
            }
        }, _this.SDCASE_RELATION);

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.Shared.RelatedCase.frSDRelatedCase.prototype.btnEditarRelated_Click ", e);
    }
}

ItHelpCenter.SD.Shared.RelatedCase.frSDRelatedCase.prototype.btnEliminarRelated_Click = function (sender, e) {
    var _this = sender;
    try {
        var DialogResult = confirm(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3"));
        if (DialogResult == true) {  
            if (_this.SDCASE_RELATION.IDSDCASE_RELATION > 0) {
                var Param = new SysCfg.Stream.Properties.TParam();
                try
                {
                    Param.Inicialize();
                    Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE_RELATION.IDSDCASE_RELATION.FieldName, _this.SDCASE_RELATION.IDSDCASE_RELATION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                    var Open = SysCfg.DB.SQL.Methods.Open("Atis", "SDCASE_RELATION_DEL", Param.ToBytes());
                    if (Open.ResErr.NotError)
                    {
                        _this.CargarDatosGrid();
                    }
                    else
                    {
                        SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.Shared.RelatedCase.frSDRelatedCase.prototype.btnEliminarRelated_Click ", Exec.ResErr.Mesaje);
                    }
                }
                finally
                {
                    Param.Destroy();
                }
            }  else {
                ShowMessage.Message.ErrorMessage(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines4"));
            }       
        }  
       
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.Shared.RelatedCase.frSDRelatedCase.prototype.btnEliminarRelated_Click ", e);
    }
}

ItHelpCenter.SD.Shared.RelatedCase.frSDRelatedCase.prototype.btnVerRelated_Click = function (sender, e) {
    var _this = sender;
    try {        
        $(_this.DivMian).css("display", "none");
        $(_this.DivChild).css("display", "");          
        _this.DivChild.innerHTML = "";

        
        var SDCASE_RELATION = new _this.TSDCASE_RELATION();

        SDCASE_RELATION.IDSDTYPEUSER = _this.IDSDTYPEUSER;
        SDCASE_RELATION.IDCMDBCI = _this.IDUSER;


        var frSDRelatedCaseEditor = new ItHelpCenter.SD.Shared.RelatedCase.frSDRelatedCaseEditor(_this.DivChild, function (outRest) {
            
            $(_this.DivMian).css("display", "");
            $(_this.DivChild).css("display", "none");

            if (outRest != null) {
                _this.SDCASE_RELATION = outRest;
                _this.SDRelatedCaseEditorAdd()
            }
        }, SDCASE_RELATION);
         
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.Shared.RelatedCase.frSDRelatedCase.prototype.btnVerRelated_Click ", e);
    }
}

ItHelpCenter.SD.Shared.RelatedCase.frSDRelatedCase.prototype.btnVerRelatedData_Click = function (sender, e) {
    var _this = sender;
    try {
        if (_this.SDCASE_RELATION.IDSDCASE_RELATION > 0)
        {
            //var SetSDConsoleInfo = new UsrCfg.SD.TSetSDConsoleInfo();                    
            //SetSDConsoleInfo.IDSDCASE = _this.SDCASE_RELATION.IDSDCASE;
            //SetSDConsoleInfo.IDCMDBCI = SysCfg.App.Methods.Cfg_WindowsTask.User.IDCMDBCI;
            //SetSDConsoleInfo.IDSDTYPEUSER = _this.IDSDTYPEUSER;
            //SetSDConsoleInfo = ItHelpCenter.SD.Methods.SetSDConsoleInfo(SetSDConsoleInfo);
            //if (SetSDConsoleInfo.ResErr.NotError == true)
            //{
            //    if (SetSDConsoleInfo.SDConsoleAtentionStart.ResultSDConsoleAtentionStart == UsrCfg.SD.Properties.TResultSDConsoleAtentionStart._IncomeAccepted)
            //    {
            //        $(_this.DivMian).css("display", "none");
            //        $(_this.DivChild).css("display", "");
            //        _this.DivChild.innerHTML = "";

            //        var TfrSDCaseAtentionDetail = new ItHelpCenter.SD.Shared.Atention.CaseAtentionDetail.TfrSDCaseAtentionDetail(_this.DivChild, function (thisOut) {
            //            $(_this.DivMian).css("display", "");
            //            $(_this.DivChild).css("display", "none");
            //        }, SetSDConsoleInfo.SDConsoleAtentionStart.SDCONSOLEATENTION);
            //    }
            //}


        var parameters =
        {
            IDSDCASE: _this.SDCASE_RELATION.IDSDCASE,
            IDCMDBCI: SysCfg.App.Methods.Cfg_WindowsTask.User.IDCMDBCI
        };
            parameters = JSON.stringify(parameters);
            $.ajax({
                type: "POST",
                url: SysCfg.App.Properties.xRaiz + 'Service/SD/Atention.svc/SDConsoleInfo',
                data: parameters,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (response) {
                    //***************************************************************************************************
                    var Atention = response;

                    if (Atention.ResErr.NotError == true) {
                        if (Atention.ResultSDConsoleAtentionStart == UsrCfg.SD.Properties.TResultSDConsoleAtentionStart._IncomeAccepted.value) {

                            $(_this.DivMian).css("display", "none");
                            $(_this.DivChild).css("display", "");
                            _this.DivChild.innerHTML = "";
                            
                            var TfrSDCaseAtentionDetail = new ItHelpCenter.SD.Shared.Atention.CaseAtentionDetail.TfrSDCaseAtentionDetail(_this.DivChild, function (thisOut) {
                                $(_this.DivMian).css("display", "");
                                $(_this.DivChild).css("display", "none");
                            }, Atention.SDCONSOLEATENTION);
                           
                        }
                        else if (Atention.ResultSDConsoleAtentionStart == UsrCfg.SD.Properties.TResultSDConsoleAtentionStart._IncomeCreate.value) {
                        }
                    }
                    else {
                    }

                },
                error: function (response) {
                    SysCfg.Log.Methods.WriteLog("frSDCaseSet.js ItHelpCenter.SD.frSDCaseSet.prototype.BtnViewCaseDetail_Onclick Service/SD/Atention.svc/SDConsoleInfo " + "Error no llamo al Servicio [SDConsoleAtentionStart]");
                }
            });
           
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.Shared.RelatedCase.frSDRelatedCase.prototype.btnVerRelatedData_Click ", e);
    }
}
