﻿ItHelpCenter.SD.Shared.RelatedCase.frSDRelatedCaseEditor.prototype.InitializeComponent = function () {
    var _this = this.TParent();
    try {
       
        var DivMian = new TVclStackPanel(_this.ObjectHtml, "DivMian", 1, [[12], [12], [12], [12], [12]]);
        _this.DivMian = DivMian.Column[0].This;

        var DivChild = new TVclStackPanel(_this.ObjectHtml, "DivChild", 1, [[12], [12], [12], [12], [12]]);
        _this.DivChild = DivChild.Column[0].This;
        $(_this.DivChild).css("display", "none");

         
        var div1 = new TVclStackPanel(_this.DivMian, "div1", 1, [[12], [12], [12], [12], [12]]);
        var div1C = div1.Column[0].This;
        $(div1.Row.This).css("padding", "10px");
        if (SysCfg.App.Properties.Device.value != SysCfg.App.TDevice.Desktop.value) {
            $(div1.Row.This).css("padding", "5px 0px");
        }

        var div2 = new TVclStackPanel(_this.DivMian, "div2", 3, [[4, 6, 2], [4, 6, 2], [3, 7, 2], [3, 8, 1], [3, 8, 1]]);
        var div2_1C = div2.Column[0].This;
        var div2_2C = div2.Column[1].This;
        var div2_3C = div2.Column[2].This;
        $(div2.Row.This).css("padding", "10px");
        if (SysCfg.App.Properties.Device.value != SysCfg.App.TDevice.Desktop.value) {
            $(div2.Row.This).css("padding", "5px 0px");
        }

        var div3 = new TVclStackPanel(_this.DivMian, "div3", 2, [[4, 8], [4, 8], [3, 9], [3, 9], [3, 9]]);
        var div3_1C = div3.Column[0].This;                                                
        var div3_2C = div3.Column[1].This;                                                
        $(div3.Row.This).css("padding", "10px");                                          
        if (SysCfg.App.Properties.Device.value != SysCfg.App.TDevice.Desktop.value) {
            $(div3.Row.This).css("padding", "5px 0px");
        }

        var div4 = new TVclStackPanel(_this.DivMian, "div4", 2, [[4, 8], [4, 8], [3, 9], [3, 9], [3, 9]]);
        var div4_1C = div4.Column[0].This;
        var div4_2C = div4.Column[1].This;
        $(div4.Row.This).css("padding", "10px");
        if (SysCfg.App.Properties.Device.value != SysCfg.App.TDevice.Desktop.value) {
            $(div4.Row.This).css("padding", "5px 0px");
        }

        var div5 = new TVclStackPanel(_this.DivMian, "div5", 1, [[12], [12], [12], [12], [12]]);
        var div5C = div5.Column[0].This;
        $(div5.Row.This).css("padding", "10px 10px 0px 10px");
        if (SysCfg.App.Properties.Device.value != SysCfg.App.TDevice.Desktop.value) {
            $(div5.Row.This).css("padding", "0px 5px");
        }

        var div6 = new TVclStackPanel(_this.DivMian, "div6", 1, [[12], [12], [12], [12], [12]]);
        var div6C = div6.Column[0].This;
        $(div6.Row.This).css("padding", "10px 10px 10px 10px");
        if (SysCfg.App.Properties.Device.value != SysCfg.App.TDevice.Desktop.value) {
            $(div6.Row.This).css("padding", "5px 0px 7px 0px");
        }

        var div7 = new TVclStackPanel(_this.DivMian, "div7", 1, [[12], [12], [12], [12], [12]]);
        var div7C = div7.Column[0].This;
        $(div7.Row.This).css("padding", "10px 10px 0px 10px");
        if (SysCfg.App.Properties.Device.value != SysCfg.App.TDevice.Desktop.value) {
            $(div7.Row.This).css("padding", "0px 5px");
        }

        var div8 = new TVclStackPanel(_this.DivMian, "div8", 1, [[12], [12], [12], [12], [12]]);
        var div8C = div8.Column[0].This;
        $(div8.Row.This).css("padding", "10px 10px 10px 10px");
        if (SysCfg.App.Properties.Device.value != SysCfg.App.TDevice.Desktop.value) {
            $(div8.Row.This).css("padding", "5px 0px 7px 0px");
        }

        var div9 = new TVclStackPanel(_this.DivMian, "div9", 1, [[12], [12], [12], [12], [12]]);
        var div9C = div9.Column[0].This;
        $(div9.Row.This).css("padding", "10px");
        if (SysCfg.App.Properties.Device.value != SysCfg.App.TDevice.Desktop.value) {
            $(div9.Row.This).css("padding", "0px 5px");
        }

        var lbl01 = new TVcllabel(div1C, "", TlabelType.H0);
        lbl01.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lbl01.Text");
        lbl01.VCLType = TVCLType.BS;

        var lbl02 = new TVcllabel(div2_1C, "", TlabelType.H0);
        lbl02.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lbl02.Text");
        lbl02.VCLType = TVCLType.BS;

        _this.txtSeleccionarCase = new TVclTextBox(div2_2C, "txtSeleccionarCase");
        $(_this.txtSeleccionarCase.This).css('width', "100%");
        $(_this.txtSeleccionarCase.This).addClass("TextFormat");
        _this.txtSeleccionarCase.This.readOnly = true;
        
        var btnSeleccionarCase = new TVclImagen(div2_3C, "btnSearch");
        btnSeleccionarCase.Src = "image/24/Search.png"
        btnSeleccionarCase.Cursor = "pointer";
        btnSeleccionarCase.onClick = function () {
            _this.btnSeleccionarCase_Click(_this, btnSeleccionarCase);
        }
        $(div2_3C).css("padding-top", "10px");
        btnSeleccionarCase.This.style.float = "left";

        var lbl03 = new TVcllabel(div3_1C, "", TlabelType.H0);
        lbl03.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lbl03.Text");
        lbl03.VCLType = TVCLType.BS;   
        
        _this.txtSeleccionarCaseCategory = new TVclTextBox(div3_2C, "txtSeleccionarCaseCategory");
        $(_this.txtSeleccionarCaseCategory.This).css('width', "100%");
        $(_this.txtSeleccionarCaseCategory.This).addClass("TextFormat");
        _this.txtSeleccionarCaseCategory.This.readOnly = true;

        var lbl04 = new TVcllabel(div4_1C, "", TlabelType.H0);
        lbl04.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lbl04.Text");
        lbl04.VCLType = TVCLType.BS;

        //combo
        _this.cmbPriority = new TVclComboBox2(div4_2C, "cmbPriority");
        $(_this.cmbPriority.This).css('width', "100%");
        $(_this.cmbPriority.This).addClass("TextFormat");
        _this.cmbPriority.onChange = function () {
            _this.IDMDPRIORITY = _this.cmbPriority.Value;
        }

        var lbl05 = new TVcllabel(div5C, "", TlabelType.H0);
        lbl05.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lbl05.Text");
        lbl05.VCLType = TVCLType.BS;

        _this.txtTitulo = new TVclMemo(div6C, "txtTitulo");
        $(_this.txtTitulo.This).css('width', "100%");
        $(_this.txtTitulo.This).css('height', "50px");
        $(_this.txtTitulo.This).addClass("TextFormat");

        var lbl06 = new TVcllabel(div7C, "", TlabelType.H0);
        lbl06.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lbl06.Text");
        lbl06.VCLType = TVCLType.BS;

        _this.txtDescripcion = new TVclMemo(div8C, "txtTitulo");
        $(_this.txtDescripcion.This).css('width', "100%");
        $(_this.txtDescripcion.This).css('height', "50px");
        $(_this.txtDescripcion.This).addClass("TextFormat");

        $(div9C).css("padding-top", "10px");
        $(div9C).css("text-align", "right");

        _this.BtnOk = new TVclButton(div9C, "btnAceptar" + _this.ID);
        _this.BtnOk.VCLType = TVCLType.BS;
        _this.BtnOk.Src = "image/24/accept24.png"
        _this.BtnOk.Cursor = "pointer";
        _this.BtnOk.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnOk");
        _this.BtnOk.onClick = function () {
            _this.BtnOk_Click(_this, _this.BtnOk);
        }
        $(_this.BtnOk.This).css("float", "right");
        $(_this.BtnOk.This).css("margin", "10px");
        $(_this.BtnOk.This).css("cursor", "pointer");

        _this.BtnCerrar = new TVclButton(div9C, "btnCancel" + _this.ID);
        _this.BtnCerrar.VCLType = TVCLType.BS;
        _this.BtnCerrar.Src = "image/24/close24.png"
        _this.BtnCerrar.Cursor = "pointer";
        _this.BtnCerrar.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnCerrar");
        _this.BtnCerrar.onClick = function () {
            _this.BtnCerrar_Click(_this, _this.BtnCerrar);
        }
        $(_this.BtnCerrar.This).css("float", "right");
        $(_this.BtnCerrar.This).css("margin", "10px");
        $(_this.BtnCerrar.This).css("cursor", "pointer");
         

        _this.Initialize();

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.Shared.RelatedCase.frSDRelatedCaseEditor.prototype.InitializeComponent ", e);
    }
}