﻿ItHelpCenter.SD.Shared.RelatedCase.frSDRelatedCase.prototype.InitializeComponent = function () {
    var _this = this.TParent();
    try {
       
        var DivMian = new TVclStackPanel(_this.ObjectHtml, "DivMian" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
        _this.DivMian = DivMian.Column[0].This;       

        var DivChild = new TVclStackPanel(_this.ObjectHtml, "DivChild" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
        _this.DivChild = DivChild.Column[0].This;
        $(_this.DivChild).css("display", "none");

        ////////////////////////////// DivMian
        var div1 = new TVclStackPanel(_this.DivMian, "div1" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
        div1C = div1.Column[0].This;
        $(div1C).css("padding-left", "25px");

        var div2 = new TVclStackPanel(_this.DivMian, "div2" + _this.ID, 2, [[12, 12], [7, 5], [7, 5], [8, 4], [9, 3]])
        _this.GRID_DIV = div2.Column[0].This;
        var div2_2C = div2.Column[1].This; 
        $(_this.GRID_DIV).css("min-height", "250px");
        $(div2_2C).css("min-height", "250px");         

        if (SysCfg.App.Properties.Device.value == SysCfg.App.TDevice.Desktop.value) {
            $(_this.GRID_DIV).addClass("StackPanelContainer");
            $(div2_2C).addClass("StackPanelContainer");
        } else {
            $(_this.DivMian).css("padding", "0px 10px");
            $(_this.ObjectHtml).css("padding", "0px 10px");
            $(_this.DivChild).css("padding", "0px 10px");
            $(_this.GRID_DIV).css("padding", "0px 10px");
        }

        var div2Sub1 = new TVclStackPanel(div2_2C, "div2Sub1" + _this.ID, 2, [[8, 4], [8, 4], [8, 4], [8, 4], [9, 3]]);
        var div2_1_1C = div2Sub1.Column[0].This;                                                         
        var div2_1_2C = div2Sub1.Column[1].This;                                                         
                                                                                                      
        var div2Sub2 = new TVclStackPanel(div2_2C, "div2Sub2" + _this.ID, 2, [[8, 4], [8, 4], [8, 4], [8, 4], [9, 3]]);
        var div2_2_1C = div2Sub2.Column[0].This;                                                          
        var div2_2_2C = div2Sub2.Column[1].This;                                                          
                                                                                                     
        var div2Sub3 = new TVclStackPanel(div2_2C, "div2Sub3" + _this.ID, 2, [[8, 4], [8, 4], [8, 4], [8, 4], [9, 3]]);
        var div2_3_1C = div2Sub3.Column[0].This;                                                          
        var div2_3_2C = div2Sub3.Column[1].This;                                                          
                                                                                                    
        var div2Sub4 = new TVclStackPanel(div2_2C, "div2Sub4" + _this.ID, 2, [[8, 4], [8, 4], [8, 4], [8, 4], [9, 3]]);
        var div2_4_1C = div2Sub4.Column[0].This;                                                          
        var div2_4_2C = div2Sub4.Column[1].This;

        $(div1.Row.This).css("padding", "5px");
        $(div2.Row.This).css("padding", "10px 20px");
    

        var lbl01 = new TVcllabel(div1C, "", TlabelType.H0);
        lbl01.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lbl01.Text");
        lbl01.VCLType = TVCLType.BS;
       
        var lbl001 = new TVcllabel(div2_1_1C, "", TlabelType.H0);
        lbl001.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lbl001.Text");
        lbl001.VCLType = TVCLType.BS;

        var lbl002 = new TVcllabel(div2_2_1C, "", TlabelType.H0);
        lbl002.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lbl002.Text");
        lbl002.VCLType = TVCLType.BS;

        var lbl003 = new TVcllabel(div2_3_1C, "", TlabelType.H0);
        lbl003.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lbl003.Text");
        lbl003.VCLType = TVCLType.BS;

        var lbl004 = new TVcllabel(div2_4_1C, "", TlabelType.H0);
        lbl004.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lbl004.Text");
        lbl004.VCLType = TVCLType.BS;

 
        _this.btnEditarRelated = new TVclButton(div2_1_2C, "btnEditarRelated" + _this.ID);
        _this.btnEditarRelated.VCLType = TVCLType.BS;
        _this.btnEditarRelated.Src = "image/24/Edit.png"
        _this.btnEditarRelated.Cursor = "pointer";
        _this.btnEditarRelated.This.style.minWidth = "100%";
        _this.btnEditarRelated.This.classList.add("btn-xs");
        _this.btnEditarRelated.onClick = function () {
            _this.btnEditarRelated_Click(_this, _this.btnEditarRelated);
        }

        _this.btnEliminarRelated = new TVclButton(div2_2_2C, "btnEliminarRelated" + _this.ID);
        _this.btnEliminarRelated.VCLType = TVCLType.BS;
        _this.btnEliminarRelated.Src = "image/24/case-remove.png"
        _this.btnEliminarRelated.Cursor = "pointer";
        _this.btnEliminarRelated.This.style.minWidth = "100%";
        _this.btnEliminarRelated.This.classList.add("btn-xs");
        _this.btnEliminarRelated.onClick = function () {
            _this.btnEliminarRelated_Click(_this, _this.btnEliminarRelated);
        }

        _this.btnVerRelated = new TVclButton(div2_3_2C, "btnVerRelated" + _this.ID);
        _this.btnVerRelated.VCLType = TVCLType.BS;
        _this.btnVerRelated.Src = "image/24/case-add.png"
        _this.btnVerRelated.Cursor = "pointer";
        _this.btnVerRelated.This.style.minWidth = "100%";
        _this.btnVerRelated.This.classList.add("btn-xs");
        _this.btnVerRelated.onClick = function () {
            _this.btnVerRelated_Click(_this, _this.btnVerRelated);
        }

        _this.btnVerRelatedData = new TVclButton(div2_4_2C, "btnVerRelatedData" + _this.ID);
        _this.btnVerRelatedData.VCLType = TVCLType.BS;
        _this.btnVerRelatedData.Src = "image/24/File-info.png"
        _this.btnVerRelatedData.Cursor = "pointer";
        _this.btnVerRelatedData.This.style.minWidth = "100%";
        _this.btnVerRelatedData.This.classList.add("btn-xs");
        _this.btnVerRelatedData.onClick = function () {
            _this.btnVerRelatedData_Click(_this, _this.btnVerRelatedData);
        }

        _this.Initialize();
        
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.Shared.RelatedCase.frSDRelatedCase.prototype.InitializeComponent ", e);
    }
}