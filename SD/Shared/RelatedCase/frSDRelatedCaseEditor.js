﻿ItHelpCenter.SD.Shared.RelatedCase.frSDRelatedCaseEditor = function (inObject, incallback, inSDCASE_RELATION) {
    this.TParent = function () {
        return this;
    }.bind(this);

    var _this = this.TParent();
    this.ObjectHtml = inObject; 
    this.CallbackModalResult = incallback;
    this.SDCASE_RELATION = inSDCASE_RELATION;    

    this.Mythis = "frSDRelatedCaseEditor";
        
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lbl01.Text", "Select case and fill all data");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lbl02.Text", "Select Case: ");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lbl03.Text", "Category: ");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lbl04.Text", "Case: ");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lbl05.Text", "Title: ");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lbl06.Text", "Description: ");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnOk","Accept");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnCerrar", "Cancel");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1", "Select Case");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2", "Type Title");

    _this.InitializeComponent(); 
}


ItHelpCenter.SD.Shared.RelatedCase.frSDRelatedCaseEditor.prototype.Initialize = function () {
    var _this = this.TParent();      
     
    _this.txtTitulo.Text = _this.SDCASE_RELATION.RELATIONS_TITLE;
    _this.txtDescripcion.Text = _this.SDCASE_RELATION.RELATIONS_DESCRIPTION;
    _this.CargarComboRelationType();
    _this.SetDisplayRelationType(_this.SDCASE_RELATION.IDSDCASE_RELATIONTYPE);
    _this.UpdCase();
}

ItHelpCenter.SD.Shared.RelatedCase.frSDRelatedCaseEditor.prototype.UpdCase = function () {
    var _this = this.TParent();

    _this.txtSeleccionarCase.Text = _this.SDCASE_RELATION.IDSDCASE + " " + _this.SDCASE_RELATION._SDCASE_OF_TITLE;
    _this.txtSeleccionarCaseCategory.Text = _this.SDCASE_RELATION._SDCASE_OF_CATEGORY + "/" + _this.SDCASE_RELATION._SDCASE_OF_CATEGORYNAME;

}

ItHelpCenter.SD.Shared.RelatedCase.frSDRelatedCaseEditor.prototype.CargarComboRelationType = function () {
    var _this = this.TParent();
    try
    {
        _this.cmbPriority.Options.lenght = 0;
        _this.DS_SDCASE_RELATIONTYPE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "SDCASE_RELATIONTYPE_GET", new Array());
        if (_this.DS_SDCASE_RELATIONTYPE.ResErr.NotError && _this.DS_SDCASE_RELATIONTYPE.DataSet.RecordCount > 0)
        {
            for (var i = 0; i < _this.DS_SDCASE_RELATIONTYPE.DataSet.RecordCount; i++)
            {
                var VclComboBoxItem = new TVclComboBoxItem();
                VclComboBoxItem.Value = _this.DS_SDCASE_RELATIONTYPE.DataSet.Records[i].Fields[0].Value;
                VclComboBoxItem.Text = _this.DS_SDCASE_RELATIONTYPE.DataSet.Records[i].Fields[1].Value;

                _this.cmbPriority.AddItem(VclComboBoxItem);
            } 
        }
        else
        {
            ShowMessage.Message.ErrorMessage(_this.DS_SDCASE_RELATIONTYPE.ResErr.Mesaje);
        }
    }
    catch (ex)
    {
        ShowMessage.Message.ErrorMessage(ex.toString());
    }
}

ItHelpCenter.SD.Shared.RelatedCase.frSDRelatedCaseEditor.prototype.GetIDRelationType = function () {
    var _this = this.TParent();
    
    if (_this.cmbPriority.Text != null && _this.cmbPriority.Text != "")
    {
        for (var i = 0; i < _this.DS_SDCASE_RELATIONTYPE.DataSet.RecordCount; i++)
        {
            if (_this.DS_SDCASE_RELATIONTYPE.DataSet.Records[i].Fields[1].Value == _this.cmbPriority.Text)
            {
                return (_this.DS_SDCASE_RELATIONTYPE.DataSet.Records[i].Fields[0].Value);
            }
        }
    }
    return -1;
}

ItHelpCenter.SD.Shared.RelatedCase.frSDRelatedCaseEditor.prototype.GetDisplayRelationType = function (IDPriority) {
    var _this = this.TParent();

}

ItHelpCenter.SD.Shared.RelatedCase.frSDRelatedCaseEditor.prototype.SetDisplayRelationType = function (IDPriority) {
    var _this = this.TParent();

    for (var i = 0; i < _this.DS_SDCASE_RELATIONTYPE.DataSet.RecordCount; i++)
    {
        if (_this.DS_SDCASE_RELATIONTYPE.DataSet.Records[i].Fields[0].Value == IDPriority)
        {
            for (var e = 0; e < _this.cmbPriority.Options.length; e++)
            {
                if (_this.DS_SDCASE_RELATIONTYPE.DataSet.Records[i].Fields[1].Value == _this.cmbPriority.Options[e].Text)
                {
                    _this.cmbPriority.selectedIndex = e;
                    return;
                }
            }
        }
    }
}

ItHelpCenter.SD.Shared.RelatedCase.frSDRelatedCaseEditor.prototype.BtnCerrar_Click = function (sender, e) {
    var _this = sender;     
   
    _this.CallbackModalResult(null);
}

ItHelpCenter.SD.Shared.RelatedCase.frSDRelatedCaseEditor.prototype.BtnOk_Click = function (sender, e) {
    var _this = sender;

    _this.SDCASE_RELATION.RELATIONS_TITLE = _this.txtTitulo.Text;
    _this.SDCASE_RELATION.RELATIONS_DESCRIPTION = _this.txtDescripcion.Text;
    _this.SDCASE_RELATION.IDSDCASE_RELATIONTYPE = _this.GetIDRelationType();

    if (_this.SDCASE_RELATION.IDSDCASE == 0) {
        alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1"));
        return;
    }
    if (_this.SDCASE_RELATION.RELATIONS_TITLE == "") {
        alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2"));
        return;
    }
     
    _this.CallbackModalResult(_this.SDCASE_RELATION);
}

ItHelpCenter.SD.Shared.RelatedCase.frSDRelatedCaseEditor.prototype.btnSeleccionarCase_Click = function (sender, e) {
    var _this = sender;

    $(_this.DivMian).css("display", "none");
    $(_this.DivChild).css("display", "");

    var query = SysCfg.DB.SQL.Methods.GetSQLPrefixIDBuild("Atis", "SDCASE_RELATIONSELECT", new Array()).StrSQL;

    var objAdvSearchManager = new ItHelpCenter.SD.AdvSearch.TAdvSearchManager(_this.DivChild, function (sender2) {
        var _this2 = sender2;
        _this2.Inicialize("COD_01", query, function (outRest, sender3) {
            var _this3 = sender3;

            $(_this.DivMian).css("display", "");
            $(_this.DivChild).css("display", "none");
            
            _this3.SDCASE_RELATION.IDSDCASE = outRest.Cells[0].Value;
            _this3.SDCASE_RELATION._SDCASE_OF_TITLE = outRest.Cells[1].Value;
            _this3.SDCASE_RELATION._SDCASE_OF_CATEGORY = outRest.Cells[3].Value;
            _this3.SDCASE_RELATION._SDCASE_OF_CATEGORYNAME = outRest.Cells[4].Value;
            _this3.UpdCase();
        });
    }, _this);
 
}

