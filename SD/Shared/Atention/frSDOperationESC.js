﻿ItHelpCenter.SD.frSDOperationESC = function (inObject, inid, inOnModalResult) {
    this.TParent = function () {
        return this;
    }.bind(this);

    var _this = this.TParent();
    this.Object = this.Modal.Body.This;
    this.Modal.HeaderContainer.innerHTML = "";
    this.Object.innerHTML = "";
    _this.Modal._inObjectHTMLBack = inObject;
    _this.Modal.Width = "65%";
    //this.CallbackModalResult = inCallbackModalResult;
    this.OnModalResult = inOnModalResult;
    _this.name = "frSDOperationESC";
    _this.id = inid;


    //************ <Lines><numerolinea> es para textos de mensages,alertas o componetes run time *********
    UsrCfg.Traslate.GetLangText(_this.name, "Lines1", "Functional Escalation");
    UsrCfg.Traslate.GetLangText(_this.name, "Lines2", "Hierarchical Escalation");
    UsrCfg.Traslate.GetLangText(_this.name, "Lines3", "Record Count = 0");
    UsrCfg.Traslate.GetLangText(_this.name, "Lines4", "You must select at least one way to scale");
                                
    UsrCfg.Traslate.GetLangText(_this.name, "lblSCALETYPE_FUNC.Text", "Functional Scale:");
    UsrCfg.Traslate.GetLangText(_this.name, "lblSCALETYPE_HIER.Text", "Hieristical Scale:");
    UsrCfg.Traslate.GetLangText(_this.name, "Chkbx_ASSIGNEDNEXTUSER.Text", "Go to the next user");
    UsrCfg.Traslate.GetLangText(_this.name, "ChkbxASSIGNEDNEXTLAVEL.Text", "Go to the next level");
    UsrCfg.Traslate.GetLangText(_this.name, "ChkbxASSIGNEDTOUSER.Text", "Direct assignation");
    UsrCfg.Traslate.GetLangText(_this.name, "ChkbxNOSCALE.Text", "No Scale");
    UsrCfg.Traslate.GetLangText(_this.name, "lblUsers.Text", "Users");
    UsrCfg.Traslate.GetLangText(_this.name, "LblMensaje.Text", "Reason for action");
    UsrCfg.Traslate.GetLangText(_this.name, "BtnCerrar.Title", "Close");
    UsrCfg.Traslate.GetLangText(_this.name, "BtnOk.Title", "Accept");

    this.InitializeComponent();


    //*********** <Nombredelcomponente>.<Porpiedad> ******************************************************
    //UsrCfg.Traslate.GetLangText(_this.name, "VCLxxx.Text", "Press Button to save");

    //*********** <Variable publicas>     ****************************************************************
    _this.ModalResult = Source.Page.Properties.TModalResult.Create;

    this.SDCONSOLEATENTION = null; //UsrCfg.SD.Properties.TSDCONSOLEATENTION        
    this.DS_MDHIERESC = null; //SysCfg.DB.SQL.Properties.TOpenDataSet
    this.DS_MDFUNCESC = null; //SysCfg.DB.SQL.Properties.TOpenDataSet
    this.SetSDOperationCase = new UsrCfg.SD.TSetSDOperationCase();
    this.IDLEVEL = 0;


}.Implements(new Source.Page.Properties.PageModal());

ItHelpCenter.SD.frSDOperationESC.prototype.Initialize = function (/*UsrCfg.SD.TSetSDOperationCase */inSetSDOperationCase, /*UsrCfg.SD.Properties.TSDCONSOLEATENTION*/ inSDCONSOLEATENTION) {
    var _this = this.TParent();

    _this.ChkbxASSIGNEDTOUSER.Enabled = false;
    _this.Chkbx_ASSIGNEDNEXTUSER.Enabled = false;
    _this.ChkbxASSIGNEDNEXTLAVEL.Enabled = false;
    _this.ChkbxNOSCALE.IsEnabled = true;
    _this.ChkbxNOSCALE.IsChecked = true;
    _this.SetSDOperationCase = inSetSDOperationCase;
    _this.SDCONSOLEATENTION = inSDCONSOLEATENTION;
    var NoNextLavel = true;
    var NoNextUser = true;
    var Param = new SysCfg.Stream.Properties.TParam();
    try {

        Param.Inicialize();
        if (_this.SetSDOperationCase.SDOPERATIONTYPES == UsrCfg.SD.Properties.TSDOPERATIONTYPES._FUNCESC) {
            _this.Modal.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.name, "Lines1"));
            var IDMDFUNCESC;
            var FUNLAVEL;
            var IDCMDBCI;
            var PERCF;
            var TOTAL;
            Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCESC.FieldName, _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.MT_IDMDFUNCESC, SysCfg.DB.Properties.TMotor.OLE, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCPER.FUNLAVEL.FieldName, _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.CASEMT_SET_FUNLAVEL, SysCfg.DB.Properties.TMotor.OLE, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            Param.AddInt32(UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDCASEMT.FieldName, _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.IDSDCASEMT, SysCfg.DB.Properties.TMotor.OLE, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            Param.AddInt32(UsrCfg.InternoAtisNames.MDGROUPUSER.IDCMDBCI.FieldName, SysCfg.App.Methods.Cfg_WindowsTask.User.IDCMDBCI, SysCfg.DB.Properties.TMotor.OLE, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            _this.DS_MDFUNCESC = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "ASGNATIONFUNC", Param.ToBytes());
            if ((_this.DS_MDFUNCESC.ResErr.NotError) && (_this.DS_MDFUNCESC.DataSet.RecordCount > 0)) {
                _this.DS_MDFUNCESC.DataSet.First();
                while (!(_this.DS_MDFUNCESC.DataSet.Eof)) {
                    IDMDFUNCESC = _this.DS_MDFUNCESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDFUNCESC.IDMDFUNCESC.FieldName).asInt32();
                    FUNLAVEL = _this.DS_MDFUNCESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDFUNCPER.FUNLAVEL.FieldName).asInt32();
                    IDCMDBCI = _this.DS_MDFUNCESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDGROUPUSER.IDCMDBCI.FieldName).asInt32();
                    PERCF = _this.DS_MDFUNCESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDFUNCPER.PERCF.FieldName).asDouble();
                    TOTAL = _this.DS_MDFUNCESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDFUNCPER.TOTAL.FieldName).asDouble();
                    if ((_this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.CASEMT_SET_FUNLAVEL == FUNLAVEL) && (NoNextUser)) NoNextUser = false;
                    if ((_this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.CASEMT_SET_FUNLAVEL != FUNLAVEL) && (NoNextLavel)) NoNextLavel = false;
                    _this.DS_MDFUNCESC.DataSet.Next();
                }

            }
        }
        if (_this.SetSDOperationCase.SDOPERATIONTYPES == UsrCfg.SD.Properties.TSDOPERATIONTYPES._HIERESC) {
            //this.Title = UsrCfg.Traslate.GetLangText(this, "Lines2");
            _this.Modal.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.name, "Lines2"));
            var IDMDHIERESC;
            var HIERLAVEL;
            var IDCMDBCI;
            var PERMISSIONH;
            var PERCH;
            var TOTAL;
            Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERESC.FieldName, _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.MT_IDMDHIERESC, SysCfg.DB.Properties.TMotor.OLE, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.HIERLAVEL.FieldName, _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.CASEMT_SET_HIERLAVEL, SysCfg.DB.Properties.TMotor.OLE, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            Param.AddInt32(UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDCASEMT.FieldName, _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.IDSDCASEMT, SysCfg.DB.Properties.TMotor.OLE, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            //Open = SysCfg.DB.SQL.Methods.OpenDataGridSet(@"Atis", @"ASGNATIONHIER", Param.ToBytes());
            _this.DS_MDHIERESC = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "ASGNATIONHIER", Param.ToBytes());
            if ((_this.DS_MDHIERESC.ResErr.NotError) && (_this.DS_MDHIERESC.DataSet.RecordCount > 0)) {
                _this.DS_MDHIERESC.DataSet.First();
                while (!(_this.DS_MDHIERESC.DataSet.Eof)) {
                    IDMDHIERESC = _this.DS_MDHIERESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERESC.IDMDHIERESC.FieldName).asInt32();
                    HIERLAVEL = _this.DS_MDHIERESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.HIERLAVEL.FieldName).asInt32();
                    IDCMDBCI = _this.DS_MDHIERESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDGROUPUSER.IDCMDBCI.FieldName).asInt32();
                    PERMISSIONH = _this.DS_MDHIERESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.PERMISSIONH.FieldName).asInt32();
                    PERCH = _this.DS_MDHIERESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.PERCH.FieldName).asDouble();
                    TOTAL = _this.DS_MDHIERESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.TOTAL.FieldName).asDouble();
                    if ((_this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.CASEMT_SET_HIERLAVEL == HIERLAVEL) && (NoNextUser)) NoNextUser = false;
                    if ((_this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.CASEMT_SET_HIERLAVEL != HIERLAVEL) && (NoNextLavel)) NoNextLavel = false;
                    _this.DS_MDHIERESC.DataSet.Next();
                }
            }
        }
        _this.Chkbx_ASSIGNEDNEXTUSER.Enabled = !NoNextUser;
        _this.ChkbxASSIGNEDNEXTLAVEL.Enabled = !NoNextLavel;
    }
    finally {
        Param.Destroy();
    }

    _this.CargarCombo();

    if (_this.SetSDOperationCase.SDOPERATIONTYPES == UsrCfg.SD.Properties.TSDOPERATIONTYPES._FUNCESC) {
        _this._CargarComboScaleCalendar1();
        _this.lblSCALETYPE_FUNC.Visible = true;
        _this.cmbSCALETYPE_FUNC.Visible = true;
        _this.spFunctional.Visible = true;

        _this.lblSCALETYPE_HIER.Visible = false;
        _this.cmbSCALETYPE_HIER.Visible = false;
        _this.spHierarchical.Visible = false;
    }

    if (_this.SetSDOperationCase.SDOPERATIONTYPES == UsrCfg.SD.Properties.TSDOPERATIONTYPES._HIERESC) {
        _this._CargarComboScaleCalendar2();
        _this.lblSCALETYPE_FUNC.Visible = false;
        _this.cmbSCALETYPE_FUNC.Visible = false;
        _this.spFunctional.Visible = false;

        _this.lblSCALETYPE_HIER.Visible = true;
        _this.cmbSCALETYPE_HIER.Visible = true;
        _this.spHierarchical.Visible = true;
    }

    return ((_this.ChkbxASSIGNEDTOUSER.Enabled) || (_this.Chkbx_ASSIGNEDNEXTUSER.Enabled) || (_this.ChkbxASSIGNEDNEXTLAVEL.Enabled));
}

ItHelpCenter.SD.frSDOperationESC.prototype.CargarCombo = function () {
    var _this = this.TParent();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        _this.CmbUsers.ClearItems();
        Param.Inicialize();
        if (_this.SetSDOperationCase.SDOPERATIONTYPES == UsrCfg.SD.Properties.TSDOPERATIONTYPES._FUNCESC) {
            var IDMDFUNCESC;
            var FUNLAVEL;
            var IDCMDBCI;
            var USERNAME;
            var PERCF;
            var TOTAL;
            var Str;
            Param.AddInt32(UsrCfg.InternoAtisNames.SDCASEMT.MT_IDMDFUNCESC.FieldName, _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.MT_IDMDFUNCESC, SysCfg.DB.Properties.TMotor.OLE, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCI.FieldName, SysCfg.App.Methods.Cfg_WindowsTask.User.IDCMDBCI, SysCfg.DB.Properties.TMotor.OLE, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            //*********** GET_GETID ***************************** 
            _this.DS_MDFUNCESC = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "EscalarFuncional", Param.ToBytes());
            if ((_this.DS_MDFUNCESC.ResErr.NotError) && (_this.DS_MDFUNCESC.DataSet.RecordCount > 0)) {
                _this.DS_MDFUNCESC.DataSet.First();
                while (!(_this.DS_MDFUNCESC.DataSet.Eof)) {
                    IDMDFUNCESC = _this.DS_MDFUNCESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCESC.FieldName).asInt32();
                    FUNLAVEL = _this.DS_MDFUNCESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDFUNCPER.FUNLAVEL.FieldName).asInt32();
                    IDCMDBCI = _this.DS_MDFUNCESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDGROUPUSER.IDCMDBCI.FieldName).asInt32();
                    USERNAME = _this.DS_MDFUNCESC.DataSet.RecordSet.FieldName("USERNAME").asString();
                    PERCF = _this.DS_MDFUNCESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDFUNCPER.PERCF.FieldName).asDouble();
                    TOTAL = _this.DS_MDFUNCESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDFUNCPER.TOTAL.FieldName).asDouble();
                    Str = FUNLAVEL + " (" + IDCMDBCI + ")" + USERNAME + " " + PERCF + " " + TOTAL;

                    var Item = new TVclComboBoxItem();
                    Item.Value = Str;
                    Item.Text = Str;

                    _this.CmbUsers.AddItem(Item);
                    _this.DS_MDFUNCESC.DataSet.Next();
                }
            }
            else {
                _this.DS_MDFUNCESC.ResErr.NotError = false;
                _this.DS_MDFUNCESC.ResErr.Mesaje = UsrCfg.Traslate.GetLangText(this, "Lines3");
            }
        }
        if (_this.SetSDOperationCase.SDOPERATIONTYPES == UsrCfg.SD.Properties.TSDOPERATIONTYPES._HIERESC) {
            var IDMDHIERESC;
            var HIERLAVEL;
            var IDCMDBCI;
            var USERNAME;
            var PERMISSIONH;
            var PERCH;
            var TOTAL;
            var Str;
            Param.AddInt32(UsrCfg.InternoAtisNames.SDCASEMT.MT_IDMDHIERESC.FieldName, _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.MT_IDMDHIERESC, SysCfg.DB.Properties.TMotor.OLE, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCI.FieldName, SysCfg.App.Methods.Cfg_WindowsTask.User.IDCMDBCI, SysCfg.DB.Properties.TMotor.OLE, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            //*********** MDHIERESC_GET ***************************** 
            _this.DS_MDHIERESC = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "EscalarJerarquico", Param.ToBytes());
            if ((_this.DS_MDHIERESC.ResErr.NotError) && (_this.DS_MDHIERESC.DataSet.RecordCount > 0)) {
                _this.DS_MDHIERESC.DataSet.First();
                while (!(_this.DS_MDHIERESC.DataSet.Eof)) {
                    IDMDHIERESC = _this.DS_MDHIERESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERESC.FieldName).asInt32();
                    HIERLAVEL = _this.DS_MDHIERESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.HIERLAVEL.FieldName).asInt32();
                    IDCMDBCI = _this.DS_MDHIERESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDGROUPUSER.IDCMDBCI.FieldName).asInt32();
                    USERNAME = _this.DS_MDHIERESC.DataSet.RecordSet.FieldName("USERNAME").asString();
                    PERMISSIONH = _this.DS_MDHIERESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.PERMISSIONH.FieldName).asInt32();
                    PERCH = _this.DS_MDHIERESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.PERCH.FieldName).asDouble();
                    TOTAL = _this.DS_MDHIERESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.TOTAL.FieldName).asDouble();
                    Str = HIERLAVEL + " (" + IDCMDBCI + ")" + USERNAME + " " + PERCH + " " + TOTAL;

                    var Item = new TVclComboBoxItem();
                    Item.Value = Str;
                    Item.Text = Str;

                    _this.CmbUsers.AddItem(Item);
                    _this.DS_MDHIERESC.DataSet.Next();
                }
            }
            else {
                _this.DS_MDHIERESC.ResErr.NotError = false;
                _this.DS_MDHIERESC.ResErr.Mesaje = UsrCfg.Traslate.GetLangText(this, "Lines3");
            }
        }
    }
    finally {
        Param.Destroy();
    }
    if (_this.CmbUsers.Options.lenght > 0) {
        _this.ChkbxASSIGNEDTOUSER.Enabled = true;
        _this.CmbUsers.SelectedIndex = 0;
    }
    else {
        _this.ChkbxASSIGNEDTOUSER.Enabled = true;
    }

}

ItHelpCenter.SD.frSDOperationESC.prototype.BtnOk_Click = function (sender, e) {
    var _this = sender;
    if (_this.SetSDOperationCase.SDOPERATIONTYPES == UsrCfg.SD.Properties.TSDOPERATIONTYPES._FUNCESC) {
        _this.SetSDOperationCase.SDOPERATION_FUNCESC.FUNCESC_MESSAGE = _this.TxtDescripcion.Text;
        _this.SetSDOperationCase.SDOPERATION_FUNCESC.IDSDSCALETYPE_FUNC = UsrCfg.SD.Properties.TIDSDSCALETYPE_FUNC.GetEnum(parseInt(_this.cmbSCALETYPE_FUNC.Value));
        var IDMDFUNCESC;
        var FUNLAVEL = 0;
        var IDCMDBCI = 0;
        var USERNAME;
        var PERCF;
        var TOTAL;
        var Str;
        if ((_this.DS_MDFUNCESC.ResErr.NotError) && (_this.DS_MDFUNCESC.DataSet.RecordCount > 0)) {
            _this.DS_MDFUNCESC.DataSet.First();
            while (!(_this.DS_MDFUNCESC.DataSet.Eof)) {
                IDMDFUNCESC = _this.DS_MDFUNCESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCESC.FieldName).asInt32();
                FUNLAVEL = _this.DS_MDFUNCESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDFUNCPER.FUNLAVEL.FieldName).asInt32();
                IDCMDBCI = _this.DS_MDFUNCESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDGROUPUSER.IDCMDBCI.FieldName).asInt32();
                USERNAME = _this.DS_MDFUNCESC.DataSet.RecordSet.FieldName("USERNAME").asString();
                PERCF = _this.DS_MDFUNCESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDFUNCPER.PERCF.FieldName).asDouble();
                TOTAL = _this.DS_MDFUNCESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDFUNCPER.TOTAL.FieldName).asDouble();
                Str = FUNLAVEL + " (" + IDCMDBCI + ")" + USERNAME + " " + PERCF + " " + TOTAL;
                if (_this.CmbUsers.Text == Str) break;
                _this.DS_MDFUNCESC.DataSet.Next();
            }
        }
        else {
            _this.DS_MDFUNCESC.ResErr.NotError = false;
            _this.DS_MDFUNCESC.ResErr.Mesaje = UsrCfg.Traslate.GetLangText(this, "Lines3");
        }

        if (_this.Chkbx_ASSIGNEDNEXTUSER.Checked == true) {
            _this.SetSDOperationCase.SDOPERATION_FUNCESC.IDSDOPERATION_ESCTYPE_FUNC = UsrCfg.SD.Properties.TIDSDOPERATION_ESCTYPE._ASSIGNEDNEXTUSER;
            if (_this.OnModalResult != null)
                _this.OnModalResult(_this.PageParent, _this.SetSDOperationCase);
            _this.DialogResult = true;
            _this.Modal.CloseModal();
        }
        else if (_this.ChkbxASSIGNEDNEXTLAVEL.Checked == true) {
            _this.SetSDOperationCase.SDOPERATION_FUNCESC.IDSDOPERATION_ESCTYPE_FUNC = UsrCfg.SD.Properties.TIDSDOPERATION_ESCTYPE._ASSIGNEDNEXTLAVEL;
            if (_this.OnModalResult != null)
                _this.OnModalResult(_this.PageParent, _this.SetSDOperationCase);
            _this.DialogResult = true;
            _this.Modal.CloseModal();
        }
        else if (_this.ChkbxASSIGNEDTOUSER.Checked == true) {
            _this.SetSDOperationCase.SDOPERATION_FUNCESC.IDSDOPERATION_ESCTYPE_FUNC = UsrCfg.SD.Properties.TIDSDOPERATION_ESCTYPE._ASSIGNEDUSER;
            _this.SetSDOperationCase.SDOPERATION_FUNCESC.FUNCESC_LAVEL = FUNLAVEL;
            _this.SetSDOperationCase.SDOPERATION_FUNCESC.FUNCESC_IDCMDBCI = IDCMDBCI;
            if (_this.OnModalResult != null)
                _this.OnModalResult(_this.PageParent, _this.SetSDOperationCase);
            _this.DialogResult = true;
            _this.Modal.CloseModal();
        }
        else if (_this.ChkbxNOSCALE.Checked == true) {
            _this.SetSDOperationCase.SDOPERATION_FUNCESC.IDSDOPERATION_ESCTYPE_FUNC = UsrCfg.SD.Properties.TIDSDOPERATION_ESCTYPE._NOSCALE;
            if (_this.OnModalResult != null)
                _this.OnModalResult(_this.PageParent, _this.SetSDOperationCase);
            _this.DialogResult = true;
            _this.Modal.CloseModal();
        }
        else  {
            ShowMessage.Message.InfoMessage(UsrCfg.Traslate.GetLangText(this, "Lines4"));
        }
    }
    if (_this.SetSDOperationCase.SDOPERATIONTYPES == UsrCfg.SD.Properties.TSDOPERATIONTYPES._HIERESC) {

        _this.SetSDOperationCase.SDOPERATION_HIERESC.HIERESC_MESSAGE = _this.TxtDescripcion.Text;
        _this.SetSDOperationCase.SDOPERATION_HIERESC.IDSDSCALETYPE_HIER = UsrCfg.SD.Properties.TIDSDSCALETYPE_HIER.GetEnum(parseInt(_this.cmbSCALETYPE_HIER.Value));

        var IDMDHIERESC;
        var HIERLAVEL = 0;
        var IDCMDBCI = 0;
        var USERNAME;
        var PERMISSIONH;
        var PERCH;
        var TOTAL;
        var Str;
        if ((_this.DS_MDHIERESC.ResErr.NotError) && (_this.DS_MDHIERESC.DataSet.RecordCount > 0)) {
            _this.DS_MDHIERESC.DataSet.First();
            while (!(_this.DS_MDHIERESC.DataSet.Eof)) {
                IDMDHIERESC = _this.DS_MDHIERESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERESC.FieldName).asInt32();
                HIERLAVEL = _this.DS_MDHIERESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.HIERLAVEL.FieldName).asInt32();
                IDCMDBCI = _this.DS_MDHIERESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDGROUPUSER.IDCMDBCI.FieldName).asInt32();
                USERNAME = _this.DS_MDHIERESC.DataSet.RecordSet.FieldName("USERNAME").asString();
                PERMISSIONH = _this.DS_MDHIERESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.PERMISSIONH.FieldName).asInt32();
                PERCH = _this.DS_MDHIERESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.PERCH.FieldName).asDouble();
                TOTAL = _this.DS_MDHIERESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.TOTAL.FieldName).asDouble();
                Str = HIERLAVEL + " (" + IDCMDBCI + ")" + USERNAME + " " + PERCH + " " + TOTAL;
                if (_this.CmbUsers.Text == Str) break;
                _this.DS_MDHIERESC.DataSet.Next();
            }
        }
        else {
            _this.DS_MDHIERESC.ResErr.NotError = false;
            _this.DS_MDHIERESC.ResErr.Mesaje = UsrCfg.Traslate.GetLangText(this, "Lines3");
        }
        if (_this.Chkbx_ASSIGNEDNEXTUSER.Checked == true) {
            _this.SetSDOperationCase.SDOPERATION_HIERESC.IDSDOPERATION_ESCTYPE_HIER = UsrCfg.SD.Properties.TIDSDOPERATION_ESCTYPE._ASSIGNEDNEXTUSER;
            if (_this.OnModalResult != null)
                _this.OnModalResult(_this.PageParent, _this.SetSDOperationCase);

            _this.DialogResult = true;
            _this.Modal.CloseModal();
        }
        else if (_this.ChkbxASSIGNEDNEXTLAVEL.Checked == true) {
            _this.SetSDOperationCase.SDOPERATION_HIERESC.IDSDOPERATION_ESCTYPE_HIER = UsrCfg.SD.Properties.TIDSDOPERATION_ESCTYPE._ASSIGNEDNEXTLAVEL;
            if (_this.OnModalResult != null)
                _this.OnModalResult(_this.PageParent, _this.SetSDOperationCase);
            _this.DialogResult = true;
            _this.Modal.CloseModal();
        }
        else if (_this.ChkbxASSIGNEDTOUSER.Checked == true) {
            _this.SetSDOperationCase.SDOPERATION_HIERESC.IDSDOPERATION_ESCTYPE_HIER = UsrCfg.SD.Properties.TIDSDOPERATION_ESCTYPE._ASSIGNEDUSER;
            _this.SetSDOperationCase.SDOPERATION_HIERESC.HIERESC_LAVEL = HIERLAVEL;
            _this.SetSDOperationCase.SDOPERATION_HIERESC.HIERESC_IDCMDBCI = IDCMDBCI;
            if (_this.OnModalResult != null)
                _this.OnModalResult(_this.PageParent, _this.SetSDOperationCase);
            _this.DialogResult = true;
            _this.Modal.CloseModal();
        }
        else if (_this.ChkbxNOSCALE.Checked == true) {
            _this.SetSDOperationCase.SDOPERATION_HIERESC.IDSDOPERATION_ESCTYPE_HIER = UsrCfg.SD.Properties.TIDSDOPERATION_ESCTYPE._NOSCALE;
            if (_this.OnModalResult != null)
                _this.OnModalResult(_this.PageParent, _this.SetSDOperationCase);

            _this.DialogResult = true;
            _this.Modal.CloseModal();
        }
        else  {
            ShowMessage.Message.InfoMessage(UsrCfg.Traslate.GetLangText(this, "Lines4"));
        }
    }
}

ItHelpCenter.SD.frSDOperationESC.prototype.BtnCerrar_Click = function (sender, e) {
    var _this = sender;
    _this.DialogResult = false;
    _this.Modal.CloseModal();
}

ItHelpCenter.SD.frSDOperationESC.prototype.ChkbxASSIGNEDTOUSER_Click = function (sender, e) {
    var _this = sender;
    if (_this.ChkbxASSIGNEDTOUSER.Checked == true) {
        _this.ZonaCmbUsers.Visible = true;
        _this.Chkbx_ASSIGNEDNEXTUSER.Checked = false;
        _this.ChkbxASSIGNEDNEXTLAVEL.Checked = false;
        _this.ChkbxNOSCALE.Checked == false;
    }
    else {
        _this.ZonaCmbUsers.Visible = false;
    }
}

ItHelpCenter.SD.frSDOperationESC.prototype.Chkbx_ASSIGNEDNEXTUSER_Click = function (sender, e) {
    var _this = sender;
    if (_this.Chkbx_ASSIGNEDNEXTUSER.Checked == true) {
        _this.ZonaCmbUsers.Visible = true;
        _this.ChkbxASSIGNEDTOUSER.Checked = false;
        _this.ChkbxASSIGNEDNEXTLAVEL.Checked = false;
        _this.ZonaCmbUsers.Visible = false;
        _this.ChkbxNOSCALE.Checked == false;
    }
}

ItHelpCenter.SD.frSDOperationESC.prototype.ChkbxASSIGNEDNEXTLAVEL_Click = function (sender, e) {
    var _this = sender;
    if (_this.ChkbxASSIGNEDNEXTLAVEL.Checked == true) {
        _this.ZonaCmbUsers.Visible = true;
        _this.ChkbxASSIGNEDTOUSER.Checked = false;
        _this.Chkbx_ASSIGNEDNEXTUSER.Checked = false;
        _this.ZonaCmbUsers.Visible = false;
        _this.ChkbxNOSCALE.Checked == false;
    }
}

ItHelpCenter.SD.frSDOperationESC.prototype.ChkbxNOSCALE_Click = function (sender, e) {
    var _this = sender;
    if (_this.ChkbxNOSCALE.Checked == true) {
        _this.ZonaCmbUsers.Visible = true;
        _this.ChkbxASSIGNEDTOUSER.Checked = false;
        _this.Chkbx_ASSIGNEDNEXTUSER.Checked = false;
        _this.ZonaCmbUsers.Visible = false;
        _this.ChkbxASSIGNEDNEXTLAVEL.Checked = false
    }
}


ItHelpCenter.SD.frSDOperationESC.prototype._CargarComboScaleCalendar1 = function () {
    var _this = this.TParent();

    for (var item in UsrCfg.SD.Properties.TIDSDSCALETYPE_FUNC) {
        if (item != "GetEnum") {
            //ret = Objet[item];//retorma el primero  
            var VclComboBoxItem2 = new TVclComboBoxItem();
            VclComboBoxItem2.Value = parseInt(UsrCfg.SD.Properties.TIDSDSCALETYPE_FUNC[item].value);
            VclComboBoxItem2.Text = UsrCfg.SD.Properties.TIDSDSCALETYPE_FUNC[item].name;
            VclComboBoxItem2.Tag = UsrCfg.SD.Properties.TIDSDSCALETYPE_FUNC[item];
            _this.cmbSCALETYPE_FUNC.AddItem(VclComboBoxItem2)
        }
    }
    _this.cmbSCALETYPE_FUNC.Value = parseInt(_this.SetSDOperationCase.SDOPERATION_FUNCESC.IDSDSCALETYPE_FUNC);
}

ItHelpCenter.SD.frSDOperationESC.prototype._CargarComboScaleCalendar2 = function () {
    var _this = this.TParent();

    for (var item in UsrCfg.SD.Properties.TIDSDSCALETYPE_HIER) {
        if (item != "GetEnum") {
            //ret = Objet[item];//retorma el primero  
            var VclComboBoxItem2 = new TVclComboBoxItem();
            VclComboBoxItem2.Value = parseInt(UsrCfg.SD.Properties.TIDSDSCALETYPE_HIER[item].value);
            VclComboBoxItem2.Text = UsrCfg.SD.Properties.TIDSDSCALETYPE_HIER[item].name;
            VclComboBoxItem2.Tag = UsrCfg.SD.Properties.TIDSDSCALETYPE_HIER[item];
            _this.cmbSCALETYPE_HIER.AddItem(VclComboBoxItem2)
        }
    }
    _this.cmbSCALETYPE_HIER.Value = parseInt(_this.SetSDOperationCase.SDOPERATION_HIERESC.IDSDSCALETYPE_HIER);
}

