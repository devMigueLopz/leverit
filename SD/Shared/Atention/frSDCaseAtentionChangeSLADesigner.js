﻿ItHelpCenter.SD.Shared.Atention.frSDCaseAtentionChangeSLA.prototype.InitializeComponent = function () {
    var _this = this.TParent();
    try {

        var Div0 = new TVclStackPanel(_this.Object, "DivModal", 2, [[12, 12], [6, 6], [6, 6], [5, 7], [5, 7]]);
        _this.DivModal = Div0.Column[0].This;
        $(_this.DivModal).css("display", "none");

        var spPrincipal = new TVclStackPanel(_this.Object, "", 1, [[12], [12], [12], [12], [12]]);

        var Divs = new TVclStackPanel(spPrincipal.Column[0].This, "", 2, [[12, 12], [8, 4], [8, 4], [8, 4], [8, 4]]);

        var DivIzqC = new TVclStackPanel(Divs.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
        var DivIzq = DivIzqC.Column[0].This;

        var DivDerC = new TVclStackPanel(Divs.Column[1].This, "", 1, [[12], [12], [12], [12], [12]]);
        var DivDer = DivDerC.Column[0].This;

        $(DivIzqC.Row.This).css("padding", "10px 20px");
        $(DivDerC.Row.This).css("padding", "10px 20px");

        $(DivIzq).addClass("StackPanelContainer");
        $(DivDer).addClass("StackPanelContainer");
    

        var DivIzq1 = new TVclStackPanel(DivIzq, "", 1, [[12], [12], [12], [12], [12]]);
        var DivIzq2 = new TVclStackPanel(DivIzq, "", 1, [[12], [12], [12], [12], [12]]);
        var DivIzq3 = new TVclStackPanel(DivIzq, "", 1, [[12], [12], [12], [12], [12]]);

        var DivDer1 = new TVclStackPanel(DivDer, "", 1, [[12], [12], [12], [12], [12]]);
        var DivDer2 = new TVclStackPanel(DivDer, "", 2, [[12, 12], [7, 5], [7, 5], [7, 5], [7, 5]]);
        var DivDer3 = new TVclStackPanel(DivDer, "", 2, [[12, 12], [7, 5], [7, 5], [7, 5], [7, 5]]);

        $(DivIzq1.Column[0].This).css("padding-top", "10px");
        $(DivIzq2.Column[0].This).css("padding-top", "10px");
        $(DivIzq3.Column[0].This).css("padding-top", "10px");
        

        
        //ESTRUCTURA SAME MODEL
        _this.SameModelDiv = new TVclStackPanel("", "SameModel", 1, [[12], [12], [12], [12], [12]]);
  
        var Div1 = new TVclStackPanel(_this.SameModelDiv.Column[0].This, "Div1_" + _this.ID, 2, [[12, 12], [6, 6], [6, 6], [5, 7], [5, 7]]);
        var Div1C1 = Div1.Column[0].This;

        var Div2 = new TVclStackPanel(_this.SameModelDiv.Column[0].This, "Div2_" + _this.ID, 3, [[12, 9, 3], [4, 5, 3], [5, 5, 2], [5, 5, 2], [5, 5, 2]]);
        var Div2C1 = Div2.Column[0].This;
        var Div2C2 = Div2.Column[1].This;
        var Div2C3 = Div2.Column[2].This;

        var Div3 = new TVclStackPanel(_this.SameModelDiv.Column[0].This, "Div3_" + _this.ID, 2, [[12, 12], [6, 6], [6, 6], [5, 7], [5, 7]]);
        var Div3C1 = Div3.Column[0].This;
        var Div3C2 = Div3.Column[1].This;

        var Div4 = new TVclStackPanel(_this.SameModelDiv.Column[0].This, "Div4_" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
        var Div4C1 = Div4.Column[0].This;

        var Div5 = new TVclStackPanel(_this.SameModelDiv.Column[0].This, "Div8_" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
        var Div5C = Div5.Column[0].This; ;//container detail
        $(Div5C).css("border", "1px solid grey");
        $(Div5.Row.This).css("padding-top", "0px");
        $(Div5C).css("padding-bottom", "10px");

        ////
        var Div5_1 = new TVclStackPanel(Div5C, "Div5_1_" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
        var Div5_1C1 = Div5_1.Column[0].This;

        var Div5_2 = new TVclStackPanel(Div5C, "Div5_2_" + _this.ID, 2, [[12, 12], [5, 7], [5, 7], [4, 8], [4, 8]]);
        var Div5_2C1 = Div5_2.Column[0].This;
        var Div5_2C2 = Div5_2.Column[1].This;
        _this.DivComboPriority = Div5_2C2;

        var Div5_3 = new TVclStackPanel(Div5C, "Div5_3_" + _this.ID, 2, [[12, 12], [5, 7], [5, 7], [4, 8], [4, 8]]);
        var Div5_3C1 = Div5_3.Column[0].This;
        var Div5_3C2 = Div5_3.Column[1].This;

        var Div5_4 = new TVclStackPanel(Div5C, "Div5_4_" + _this.ID, 2, [[12, 12], [5, 7], [5, 7], [4, 8], [4, 8]]);
        var Div5_4C1 = Div5_4.Column[0].This;
        var Div5_4C2 = Div5_4.Column[1].This;

        var Div5_5 = new TVclStackPanel(Div5C, "Div5_5_" + _this.ID, 2, [[12, 12], [5, 7], [5, 7], [4, 8], [4, 8]]);
        var Div5_5C1 = Div5_5.Column[0].This;
        var Div5_5C2 = Div5_5.Column[1].This;

        ///////////////////// 
        var lbl003 = new Vcllabel(Div1C1, "lbl003" + _this.ID, TVCLType.BS, TlabelType.H0, "");
        lbl003.innerText = UsrCfg.Traslate.GetLangText(_this.name, "lbl003");

        var lblCategoria = new Vcllabel(Div2C1, "lblCategoria" + _this.ID, TVCLType.BS, TlabelType.H0, "");
        lblCategoria.innerText = UsrCfg.Traslate.GetLangText(_this.name, "lblCategoria");

        _this.txtCategoria = new Vcllabel(Div2C2, "txtCategoria" + _this.ID, TVCLType.BS, TlabelType.H0, "");

        _this.btnDetalle = new TVclImagen(Div2C3, "imgSearchCategory_" + _this.ID);
        _this.btnDetalle.Src = "image/24/Search.png"
        _this.btnDetalle.Cursor = "pointer";
        _this.btnDetalle.onClick = function () {
            _this.btnDetalle_Click(_this, _this.btnDetalle);
        }

        _this.lblDetalle = new Vcllabel(Div3C1, "lblDetalle" + _this.ID, TVCLType.BS, TlabelType.H0, "");
        _this.lblDetalle.innerText = UsrCfg.Traslate.GetLangText(_this.name, "lblDetalle");

        _this.txtDetalle = new Vcllabel(Div3C2, "lb_DetailsText_" + _this.ID, TVCLType.BS, TlabelType.H0, "");

        ////subtitle
        var tabItemDescription = new Vcllabel(Div4C1, "tabItemDescription" + _this.ID, TVCLType.BS, TlabelType.H0, "");
        tabItemDescription.innerText = UsrCfg.Traslate.GetLangText(_this.name, "tabItemDescription");
        $(Div4C1).css("width", "auto");
        $(Div4C1).css("background", "rgba(236, 240, 245, 1)");
        $(Div4C1).css("margin-bottom", "0px");
        $(Div4C1).css("border-radius", "0px 12p 0px 0px");
        $(Div4C1).css("border-top", "1px solid grey");
        $(Div4C1).css("border-right", "1px solid grey");
        $(Div4C1).css("border-left", "1px solid grey");

   
        var lbl004 = new Vcllabel(Div5_1C1, "lbl004" + _this.ID, TVCLType.BS, TlabelType.H0, "");
        lbl004.innerText = UsrCfg.Traslate.GetLangText(_this.name, "lbl004");
        
        var lb_Priority = new Vcllabel(Div5_2C1, "lb_Priority_" + _this.ID, TVCLType.BS, TlabelType.H0, "");
        lb_Priority.innerText = UsrCfg.Traslate.GetLangText(_this.name, "lb_Priority");

        _this.LoadComboPriority();
        
        var lb_isMayor = new Vcllabel(Div5_3C1, "lb_isMayor_" + _this.ID, TVCLType.BS, TlabelType.H0, "");
        lb_isMayor.innerText = UsrCfg.Traslate.GetLangText(_this.name, "lb_isMayor");

        _this.CheckisMayor = new TVclInputcheckbox(Div5_3C2, "CheckisMayor_" + _this.ID);
        _this.CheckisMayor.VCLType = TVCLType.BS;
        _this.CheckisMayor.onClick = function myfunction() {
        }
        $(_this.CheckisMayor.This).css("float", "left");

        var lb_Title = new Vcllabel(Div5_4C1, "lb_Title_" + _this.ID, TVCLType.BS, TlabelType.H0, "");
        lb_Title.innerText = UsrCfg.Traslate.GetLangText(_this.name,  "lb_Title");

        if (!UsrCfg.Version.IsProduction) {
            _this.CheckTitle = new TVclInputcheckbox(Div5_4C1, "CheckisMayor_" + _this.ID);
            _this.CheckTitle.VCLType = TVCLType.BS;
            _this.CheckTitle.onClick = function myfunction() {

            }
            $(_this.CheckTitle.This).css("float", "right");
        }


        _this.txtTitulo = new TVclTextBox(Div5_4C2, "TxtSubject_" + _this.ID);
        $(_this.txtTitulo.This).css('width', "100%");
        $(_this.txtTitulo.This).addClass("TextFormat");


        var lb_Description = new Vcllabel(Div5_5C1, "lb_Priority_" + _this.ID, TVCLType.BS, TlabelType.H0, "");
        lb_Description.innerText = UsrCfg.Traslate.GetLangText(_this.name, "lb_Description");

        if (!UsrCfg.Version.IsProduction) {
            _this.CheckDescription = new TVclInputcheckbox(Div5_5C1, "CheckDescription_" + _this.ID);
            _this.CheckDescription.VCLType = TVCLType.BS;
            _this.CheckDescription.onClick = function myfunction() {

            }
            $(_this.CheckDescription.This).css("float", "right");
        }

        _this.txtDescripcion = new TVclMemo(Div5_5C2, "AreaTextDescription_" + _this.ID)
        $(_this.txtDescripcion.This).css('width', "100%");
        $(_this.txtDescripcion.This).css('height', "70px");
        $(_this.txtDescripcion.This).addClass("TextFormat");


        //ELEMENTS /////////////////////////////////
        _this.LblDescripcion = new TVcllabel(DivIzq1.Column[0].This, "LblDescripcion_" + _this.ID, TlabelType.H0);
        _this.LblDescripcion.Text = UsrCfg.Traslate.GetLangText(_this.name, "LblDescripcion");
        _this.LblDescripcion.VCLType = TVCLType.BS;

        _this.TxtDescripcion = new TVclMemo(DivIzq2.Column[0].This, "AreaTextDescription_" + _this.ID)
        $(_this.TxtDescripcion.This).css('width', "100%");
        $(_this.TxtDescripcion.This).css('height', "70px");
        $(_this.TxtDescripcion.This).addClass("TextFormat");
        

        _this.TabPrincipal = new TTabControl(DivIzq3.Column[0].This, "", "");

        _this.TabSamemodel = new TTabPage();
        _this.TabSamemodel.Name = "Samemodel";
        _this.TabSamemodel.Caption = UsrCfg.Traslate.GetLangText(_this.name, "TabSamemodel");
        _this.TabPrincipal.AddTabPages(_this.TabSamemodel);

        _this.TabNewmodel = new TTabPage();
        _this.TabNewmodel.Name = "Newmodel";
        _this.TabNewmodel.Caption = UsrCfg.Traslate.GetLangText(_this.name, "TabNewmodel");
        _this.TabPrincipal.AddTabPages(_this.TabNewmodel);

        $(_this.TabSamemodel.Page).css("border", "1px solid #c4c4c4");
        $(_this.TabSamemodel.Page).css("padding", "10px 20px");
        $(_this.TabNewmodel.Page).css("border", "1px solid #c4c4c4");
        $(_this.TabNewmodel.Page).css("padding", "10px 20px");


        //BOTONES
        var lbl101 = new Vcllabel(DivDer2.Column[0].This, "lbl101_" + _this.ID, TVCLType.BS, TlabelType.H0, "");
        lbl101.innerText = UsrCfg.Traslate.GetLangText(_this.name, "lbl101");
        DivDer2.Column[0].This.style = "display: flex;justify-content: center;align-content: center;flex-direction: column;";
        $(DivDer2.Column[0].This).addClass("LabelBoton");

        _this.btnCancel = new TVclButton(DivDer2.Column[1].This, "btnCancel_" + _this.ID);
        _this.btnCancel.VCLType = TVCLType.BS;
        _this.btnCancel.Src = "image/24/delete.png"
        _this.btnCancel.This.style.minWidth = "90%";
        _this.btnCancel.This.classList.add("btn-xs");
        _this.btnCancel.onClick = function myfunction() {
            _this.btnCancel_Click(_this, _this.btnCancel);
        }
        $(DivDer2.Column[1].This).css("padding","5px 0px");

        var lbl102 = new Vcllabel(DivDer3.Column[0].This, "lbl102_" + _this.ID, TVCLType.BS, TlabelType.H0, "");
        lbl102.innerText = UsrCfg.Traslate.GetLangText(_this.name, "lbl102");
        DivDer3.Column[0].This.style = "display: flex;justify-content: center;align-content: center;flex-direction: column;";
        $(DivDer3.Column[0].This).addClass("LabelBoton");

        _this.btnAccept = new TVclButton(DivDer3.Column[1].This, "btnAccept_" + _this.ID);
        _this.btnAccept.VCLType = TVCLType.BS;
        _this.btnAccept.Src = "image/24/success.png"
        _this.btnAccept.This.style.minWidth = "90%";
        _this.btnAccept.This.classList.add("btn-xs");
        _this.btnAccept.onClick = function myfunction() {
            _this.btnAccept_Click(_this, _this.btnAccept);
        }
        $(DivDer3.Column[1].This).css("padding", "5px 0px");


        _this.TabSamemodel.Active = true;

        _this.Load_SameModel(_this);
        _this.Load_NewModel(_this, function () {
            _this.OnModalResult(_this.ParentThis);
        });     

    } catch (e) {
       //debugger;
    }
   
}

ItHelpCenter.SD.Shared.Atention.frSDCaseAtentionChangeSLA.prototype.Load_SameModel = function (sender) {
    var _this = sender;
    $(_this.TabSamemodel.Page).append(_this.SameModelDiv.Row.This);
}

ItHelpCenter.SD.Shared.Atention.frSDCaseAtentionChangeSLA.prototype.Load_NewModel = function (sender, inCallback) {
    var _this = sender;

    _this.TfrSDSLASelect = new ItHelpCenter.SD.Shared.SLASelect.TfrSDSLASelect(_this.TabNewmodel.Page, function (_load, this_out) {
        if (_load) { //proceso al terminar el Initialize de frSDSLASelect
            inCallback();
        } else { //proceso cuando se optiene el ID de la categoria
            _this.IDMDCATEGORYDETAIL = this_out.IDMDCATEGORYDETAIL;
        }
    });
}

ItHelpCenter.SD.Shared.Atention.frSDCaseAtentionChangeSLA.prototype.LoadComboPriority = function () {
    var _this = this.TParent();


    var MDPRIORITYList = Persistence.Profiler.CatalogProfiler.MDPRIORITYList;

    _this.CmbPriority = new TVclComboBox2(_this.DivComboPriority, "CmbPriority_" + _this.ID);
    for (var i = 0; i < MDPRIORITYList.length; i++) {
        var VclComboBoxItem = new TVclComboBoxItem();
        VclComboBoxItem.Value = MDPRIORITYList[i].IDMDPRIORITY;
        VclComboBoxItem.Text = MDPRIORITYList[i].PRIORITYNAME;

        _this.CmbPriority.AddItem(VclComboBoxItem);
    }
    _this.CmbPriority.onChange = function () {
        _this.IDPRIORITY = _this.CmbPriority.Value;
    }
    $(_this.CmbPriority.This).css('width', "100%");
    $(_this.CmbPriority.This).addClass("TextFormat");

    _this.CmbPriority.selectedIndex = 0;
    _this.IDPRIORITY = MDPRIORITYList[0].IDMDPRIORITY;

     
}

