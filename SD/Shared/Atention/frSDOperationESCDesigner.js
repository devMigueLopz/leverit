﻿ItHelpCenter.SD.frSDOperationESC.prototype.InitializeComponent = function () {
    var _this = this.TParent();

    var spPrincipal = new TVclStackPanel(_this.Object, "", 1, [[12], [12], [12], [12], [12]]);

    _this.spFunctional = new TVclStackPanel(spPrincipal.Column[0].This, "", 2, [[12, 12], [12, 12], [4, 8], [4, 8], [4, 8]]);
    _this.lblSCALETYPE_FUNC = new TVcllabel(_this.spFunctional.Column[0].This, "", TlabelType.H0);
    _this.lblSCALETYPE_FUNC.Text = UsrCfg.Traslate.GetLangText(_this.name, "lblSCALETYPE_FUNC.Text");
    _this.lblSCALETYPE_FUNC.VCLType = TVCLType.BS;

    _this.cmbSCALETYPE_FUNC = new TVclComboBox2(_this.spFunctional.Column[1].This, "");
    _this.cmbSCALETYPE_FUNC.This.classList.add("form-control");

    _this.spHierarchical = new TVclStackPanel(spPrincipal.Column[0].This, "", 2, [[12, 12], [12, 12], [4, 8], [4, 8], [4, 8]]);
    _this.lblSCALETYPE_HIER = new TVcllabel(_this.spHierarchical.Column[0].This, "", TlabelType.H0);
    _this.lblSCALETYPE_HIER.Text = UsrCfg.Traslate.GetLangText(_this.name, "lblSCALETYPE_HIER.Text");
    _this.lblSCALETYPE_HIER.VCLType = TVCLType.BS;

    _this.cmbSCALETYPE_HIER = new TVclComboBox2(_this.spHierarchical.Column[1].This, "");
    _this.cmbSCALETYPE_HIER.This.classList.add("form-control");

    var spBloque1 = new TVclStackPanel(spPrincipal.Column[0].This, "", 4, [[3, 3, 3, 3], [3, 3, 3, 3], [3, 3, 3, 3], [3, 3, 3, 3], [3, 3, 3, 3]]);

    _this.Chkbx_ASSIGNEDNEXTUSER = new TVclInputcheckbox(spBloque1.Column[0].This, "");
    _this.Chkbx_ASSIGNEDNEXTUSER.Checked = false;
    _this.Chkbx_ASSIGNEDNEXTUSER.VCLType = TVCLType.BS;
    _this.Chkbx_ASSIGNEDNEXTUSER.Text = UsrCfg.Traslate.GetLangText(_this.name, "Chkbx_ASSIGNEDNEXTUSER.Text");
    _this.Chkbx_ASSIGNEDNEXTUSER.OnCheckedChanged = function (EventArgs) {
        _this.Chkbx_ASSIGNEDNEXTUSER_Click(_this, EventArgs);
    }
    

    _this.ChkbxASSIGNEDNEXTLAVEL = new TVclInputcheckbox(spBloque1.Column[1].This, "");
    _this.ChkbxASSIGNEDNEXTLAVEL.Checked = false;
    _this.ChkbxASSIGNEDNEXTLAVEL.VCLType = TVCLType.BS;
    _this.ChkbxASSIGNEDNEXTLAVEL.Text = UsrCfg.Traslate.GetLangText(_this.name, "ChkbxASSIGNEDNEXTLAVEL.Text");
    _this.ChkbxASSIGNEDNEXTLAVEL.OnCheckedChanged = function (EventArgs) {
        _this.ChkbxASSIGNEDNEXTLAVEL_Click(_this, EventArgs);
    }

    _this.ChkbxASSIGNEDTOUSER = new TVclInputcheckbox(spBloque1.Column[2].This, "");
    _this.ChkbxASSIGNEDTOUSER.Checked = false;
    _this.ChkbxASSIGNEDTOUSER.VCLType = TVCLType.BS;
    _this.ChkbxASSIGNEDTOUSER.Text = UsrCfg.Traslate.GetLangText(_this.name, "ChkbxASSIGNEDTOUSER.Text");
    _this.ChkbxASSIGNEDTOUSER.OnCheckedChanged = function (EventArgs) {
        _this.ChkbxASSIGNEDTOUSER_Click(_this, EventArgs);
    }

    _this.ChkbxNOSCALE = new TVclInputcheckbox(spBloque1.Column[3].This, "");
    _this.ChkbxNOSCALE.Checked = false;
    _this.ChkbxNOSCALE.VCLType = TVCLType.BS;
    _this.ChkbxNOSCALE.Text = UsrCfg.Traslate.GetLangText(_this.name, "ChkbxNOSCALE.Text");
    _this.ChkbxNOSCALE.OnCheckedChanged = function (EventArgs) {
        _this.ChkbxNOSCALE_Click(_this, EventArgs);
    }

    _this.ZonaCmbUsers = new TVclStackPanel(spPrincipal.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
    _this.ZonaCmbUsers.Visible = false;

    var spBloque2_1 = new TVclStackPanel(_this.ZonaCmbUsers.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
    _this.lblUsers = new TVcllabel(spBloque2_1.Column[0].This, "", TlabelType.H0);
    _this.lblUsers.Text = UsrCfg.Traslate.GetLangText(_this.name, "lblUsers.Text");
    _this.lblUsers.VCLType = TVCLType.BS;

    var spBloque2_2 = new TVclStackPanel(_this.ZonaCmbUsers.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
    _this.CmbUsers = new TVclComboBox2(spBloque2_2.Column[0].This, "");
    _this.CmbUsers.This.classList.add("form-control");

    var spBloque3 = new TVclStackPanel(spPrincipal.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);

    _this.LblMensaje = new TVcllabel(spBloque3.Column[0].This, "", TlabelType.H0);
    _this.LblMensaje.Text = UsrCfg.Traslate.GetLangText(_this.name, "LblMensaje.Text");
    _this.LblMensaje.VCLType = TVCLType.BS;

    var spBloque4 = new TVclStackPanel(spPrincipal.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
    _this.TxtDescripcion = new TVclMemo(spBloque4.Column[0].This, "");
    _this.TxtDescripcion.Text = "";
    _this.TxtDescripcion.VCLType = TVCLType.BS;
    _this.TxtDescripcion.This.classList.add("form-control");

    var spBloque4 = new TVclStackPanel(spPrincipal.Column[0].This, "", 2, [[6, 6], [6, 6], [6, 6], [6, 6], [6, 6]]);
    spBloque4.Row.This.style.marginTop = "10px";
    _this.BtnCerrar = new TVclImagen(spBloque4.Column[0].This, "");
    _this.BtnCerrar.Src = "image/24/delete.png";
    _this.BtnCerrar.Title = UsrCfg.Traslate.GetLangText(_this.name, "BtnCerrar.Title");
    _this.BtnCerrar.Cursor = "pointer";
    _this.BtnCerrar.onClick = function () {
        _this.BtnCerrar_Click(_this, _this.BtnCerrar);
    }

    _this.BtnOk = new TVclImagen(spBloque4.Column[1].This, "");
    _this.BtnOk.Src = "image/24/success.png";
    _this.BtnOk.Title = UsrCfg.Traslate.GetLangText(_this.name, "BtnOk.Title");
    _this.BtnOk.Cursor = "pointer";
    _this.BtnOk.onClick = function () {
        _this.BtnOk_Click(_this, _this.BtnOk);
    }
}