﻿ItHelpCenter.SD.Shared.Atention.frSDCaseAtentionChangeSLA = function (inMenuObject, inObject, inid, inOnModalResult, inParentThis) {
    this.TParent = function () {
        return this;
    }.bind(this);

    var _this = this.TParent();
    this.Object = inObject;
    this.MenuObject = inMenuObject;

    this.OnModalResult = inOnModalResult;
    _this.name = "frSDCaseAtentionChangeSLA";
    _this.id = inid;
    this.ParentThis = inParentThis;

    //************ <Lines><numerolinea> es para textos de mensages,alertas o componetes run time *********
    UsrCfg.Traslate.GetLangText(this.name, "LblDescripcion", "Reason for change");
    UsrCfg.Traslate.GetLangText(this.name, "TabSamemodel", "Same model");
    UsrCfg.Traslate.GetLangText(this.name, "TabNewmodel", "New model");
    UsrCfg.Traslate.GetLangText(this.name, "lbl101", "Cancel");
    UsrCfg.Traslate.GetLangText(this.name, "lbl102", "Add");
    UsrCfg.Traslate.GetLangText(this.name, "lbl003", "Select the category to be changed");
    UsrCfg.Traslate.GetLangText(this.name, "lblCategoria", "Category:");
    UsrCfg.Traslate.GetLangText(this.name, "lblDetalle", "Details:");
    UsrCfg.Traslate.GetLangText(this.name, "tabItemDescription", "Description");
    UsrCfg.Traslate.GetLangText(this.name, "lbl004", "Select the Priority to be changed");
    UsrCfg.Traslate.GetLangText(this.name, "lb_Priority", "Priority:");
    UsrCfg.Traslate.GetLangText(this.name, "lb_isMayor", "is Mayor:");
    UsrCfg.Traslate.GetLangText(this.name, "lb_Title", "Title:");
    UsrCfg.Traslate.GetLangText(this.name, "lb_Description", "Description:");
    UsrCfg.Traslate.GetLangText(this.name, "lb_SelectCategory", "Category Detail");
    UsrCfg.Traslate.GetLangText(this.name, "Lines1", "Are you sure you want to exit?");
    UsrCfg.Traslate.GetLangText(this.name, "Lines2", "To continue editing a reason.");
    

    //*********** <Variable publicas>     ****************************************************************
    //_this.ModalResult = Source.Page.Properties.TModalResult.Create;

    _this.TfrSDSLASelect = null;
    _this.OpenDataSetPriority = new SysCfg.DB.SQL.Properties.TOpenDataSet;
    _this.IDSDCASETYPE = UsrCfg.SD.Properties.TIDSDCASETYPE;
    _this.IDSDRESPONSETYPE = UsrCfg.SD.Properties.TSDRESPONSETYPE;
    _this.SDCONSOLEATENTION = UsrCfg.SD.Properties.TSDCONSOLEATENTION ;

    _this._IDMDCATEGORYDETAIL;
    _this.IDMDCATEGORYDETAIL
    
    _this.IDMDMODELTYPED;
    _this.CATEGORY = "";
    _this.CATEGORYNAME = "";
    _this.IDUSER = 0;
    _this.IDSDCASE = 0;
    _this.IDSDCASEEF = 0;
    _this.IDSDCASEMT = 0;

    _this.CASEMT_LIFESTATUS = "";
    //_this.MDLIFESTATUSProfiler = UsrCfg.SD.TMDLIFESTATUSProfiler();

    _this.importarScript(SysCfg.App.Properties.xRaiz + "Scripts/SD/Shared/SLASelect/frSDSLASelect.js", function () {
        _this.InitializeComponent();
    }, function (e) {
    });

}.Implements(new Source.Page.Properties.Page());

ItHelpCenter.SD.Shared.Atention.frSDCaseAtentionChangeSLA.prototype.Initialize = function (
 
            inSetSDOperationCase, 
            inSDCONSOLEATENTION,
            inIDSDCASETYPE, 
            inIDSDRESPONSETYPE,
            inCATEGORY,
            inCATEGORYNAME,
            inIDUSER,
            inOnModalResult)
{
    var _this = this.TParent();

    _this.OnModalResult = inOnModalResult;

    _this.SetSDOperationCase = inSetSDOperationCase;
    _this.IDUSER = inIDUSER;
    _this.IDSDCASE = inSetSDOperationCase.SDOPERATION_CASESTATUS.IDSDCASE;

    _this.IDSDCASEMT = inSDCONSOLEATENTION.SDCASEMTCOMPLTE.IDSDCASEMT;
    _this.IDSDCASEEF = inSDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASEEF;
    _this.SDCONSOLEATENTION = inSDCONSOLEATENTION; 
    _this.IDSDCASETYPE = inIDSDCASETYPE;
    _this.IDSDRESPONSETYPE = inIDSDRESPONSETYPE;
    _this.CATEGORY = inCATEGORY;
    _this.CATEGORYNAME = inCATEGORYNAME;
    _this.IDPRIORITY = inSetSDOperationCase.SDOPERATION_CASESTATUS.IDMDPRIORITY;

    switch (inIDSDCASETYPE) {
        case UsrCfg.SD.Properties.TIDSDCASETYPE._ACTIVITY_NEW:
        case UsrCfg.SD.Properties.TIDSDCASETYPE._NORMAL:
            _this.IDMDCATEGORYDETAIL = inSetSDOperationCase.SDOPERATION_CASESTATUS.IDMDCATEGORYDETAIL;
            _this.IDMDMODELTYPED = inSetSDOperationCase.SDOPERATION_CASESTATUS.IDMDMODELTYPED;
            break;
        case UsrCfg.SD.Properties.TIDSDCASETYPE._ACTIVITY_MODEL:
            _this.IDMDCATEGORYDETAIL = inSetSDOperationCase.SDOPERATION_CASESTATUS.IDMDCATEGORYDETAIL;
            _this.IDMDMODELTYPED = inSetSDOperationCase.SDOPERATION_CASESTATUS.IDMDMODELTYPED;
            break;
        default:
            break;
    }

    _this.TfrSDSLASelect.IDSDCASETYPE = inIDSDCASETYPE;
    _this.TfrSDSLASelect.IDMDCATEGORYDETAIL = _this.IDMDCATEGORYDETAIL;
    _this.TfrSDSLASelect.IDMDMODELTYPED = _this.IDMDMODELTYPED;
    _this.TfrSDSLASelect.IDSDRESPONSETYPE = inIDSDRESPONSETYPE;
    _this.TfrSDSLASelect.CargarComboPriority(_this.IDPRIORITY);
     
    _this.UpdCase();    
    _this.CargarComboPriority();
    
    _this.CheckisMayor.Checked = inSetSDOperationCase.SDOPERATION_CASESTATUS.CASE_ISMAYOR;
    _this.txtTitulo.Text = inSetSDOperationCase.SDOPERATION_CASESTATUS.CASE_TITLE;
    _this.txtDescripcion.Text = inSetSDOperationCase.SDOPERATION_CASESTATUS.CASE_DESCRIPTION;  


    _this.TfrSDSLASelect.CheckisMayor.Checked = inSetSDOperationCase.SDOPERATION_CASESTATUS.CASE_ISMAYOR;
    _this.TfrSDSLASelect.txtTitulo.Text = inSetSDOperationCase.SDOPERATION_CASESTATUS.CASE_TITLE;
    _this.TfrSDSLASelect.txtDescripcion.Text = inSetSDOperationCase.SDOPERATION_CASESTATUS.CASE_DESCRIPTION;
    _this.TfrSDSLASelect.IDUSER = _this.IDUSER;
    _this.TfrSDSLASelect.IDSDCASE = inSetSDOperationCase.SDOPERATION_CASESTATUS.IDSDCASE;
    _this.TfrSDSLASelect.IDSDCASEEF = _this.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASEEF;
    _this.TfrSDSLASelect.txtCategoria.innerText = _this.CATEGORY;
    _this.TfrSDSLASelect.TextDetails.innerText = _this.CATEGORYNAME;
    _this.TfrSDSLASelect.CargarDetalleWorkArrown();
    _this.TfrSDSLASelect.CargarDatosGridDetalle();
    


    _this.CASEMT_LIFESTATUS = _this.SDCONSOLEATENTION.SDCASEMTCOMPLTE.CASEMT_LIFESTATUS;
    


    // _this.MDLIFESTATUSProfiler = new UsrCfg.SD.TMDLIFESTATUSProfiler();
    // _this.MDLIFESTATUSProfiler.StrtoLIFESTATUS(CASEMT_LIFESTATUS);
    
    
}

ItHelpCenter.SD.Shared.Atention.frSDCaseAtentionChangeSLA.prototype.UpdCase = function () {
    var _this = this.TParent();
    _this.txtCategoria.innerText = _this.CATEGORY;
    _this.txtDetalle.innerText = _this.CATEGORYNAME;     
}

ItHelpCenter.SD.Shared.Atention.frSDCaseAtentionChangeSLA.prototype.CargarComboPriority = function (IDMDPRIORITY) {
    var _this = this.TParent();
   
    for (var i = 0; i < _this.CmbPriority.Options.length; i++) {
        if (_this.CmbPriority.Options[i].Value == _this.IDPRIORITY) {
            _this.CmbPriority.selectedIndex = i;
        }
    }      
}

ItHelpCenter.SD.Shared.Atention.frSDCaseAtentionChangeSLA.prototype.btnDetalle_Click = function (sender, e) {
    var _this = sender;

    try {
        $(_this.DivModal).css("display", "");
        var Modal = new TVclModal(_this.DivModal, null, "IDModal_" + _this.ID);
        Modal.Width = "60%";
        Modal.Body.This.style.minHeight = "300px";
        Modal.Top = 0;
        Modal.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.name, "lb_SelectCategory"));
        Modal.Body.This.id = "Modal_SearchCategory_" + _this.ID;

        //Parameters: (6) = 1.HTML Container, 2.Function Back, 3.Modo busqueda, 4.Ver boton cambio de modo (true - False), 5.Ver mensaje resultado, 6.Frase predeterminada    
        var _Category = new ItHelpCenter.SD.Shared.Category.TCategoryManger(
           Modal.Body.This,
            function (OutRes) {
                if (OutRes.ID > 0) {
                    _this.IDMDCATEGORYDETAIL = OutRes.ID;
                    _this.Category = OutRes.Detail;
                    _this.PathCategory = OutRes.Path;
                     
                } else {                   
                    _this.IDMDCATEGORYDETAIL = 0;
                    _this.Category = "";
                    _this.PathCategory = "";                     
                }


                _this.CATEGORY = _this.PathCategory;
                _this.CATEGORYNAME = _this.Category;

                _this.UpdCase();

                Modal.CloseModal();
                $(_this.DivModal).css("display", "none");
            },
            ItHelpCenter.SD.Shared.Category.Mode.SeekSearch,
            true, false, "", false);

        Modal.ShowModal();
        Modal.FunctionClose = function () {
            $(_this.DivModal).css("display", "none");
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.Shared.Atention.frSDCaseAtentionChangeSLA.prototype.btnDetalle_Click ", e);
        //debugger;
    }
}

ItHelpCenter.SD.Shared.Atention.frSDCaseAtentionChangeSLA.prototype.btnCancel_Click = function (sender, e) {
    _this = sender;

    var DialogResult = confirm(UsrCfg.Traslate.GetLangText(_this.name, "Lines1"));
    if (DialogResult == true) {
        _this.MenuObject.BackPageOnlyParent();
    }
}

ItHelpCenter.SD.Shared.Atention.frSDCaseAtentionChangeSLA.prototype.btnAccept_Click = function (sender, e) {
    _this = sender;

    if (_this.TxtDescripcion.Text != "")
    {
     
        if (this.TabPrincipal.TabPageSelect.Index == 0)
        { 
            _this.SetSDOperationCase.SDOPERATION_CASESTATUS.IDSDCASESETMODELTYPE = UsrCfg.SD.Properties.TIDSDCASESETMODELTYPE._UpdateMT;
            _this.SetSDOperationCase.SDOPERATION_CASESTATUS.CASESTATUS_MESSAGE = _this.TxtDescripcion.Text;
            // _this.SetSDOperationCase.SDCASEMT.MT_IDMDMODELTYPED = _this.IDMDMODELTYPED;
            _this.SetSDOperationCase.SDOPERATION_CASESTATUS.IDMDCATEGORYDETAIL = _this.IDMDCATEGORYDETAIL;
            // _this.SetSDOperationCase.SDCASEMT.IDSLA = _this.IDSLA;
            // _this.SetSDOperationCase.SDCASEMT.IDMDIMPACT = _this.IDMDIMPACT;
            _this.SetSDOperationCase.SDOPERATION_CASESTATUS.IDMDPRIORITY = _this.IDPRIORITY;
            // _this.SetSDOperationCase.SDCASEMT.IDMDURGENCY = _this.IDMDURGENCY;
            _this.SetSDOperationCase.SDOPERATION_CASESTATUS.CASE_ISMAYOR = _this.CheckisMayor.Checked;
            _this.SetSDOperationCase.SDOPERATION_CASESTATUS.CASE_TITLE = _this.txtTitulo.Text;
            _this.SetSDOperationCase.SDOPERATION_CASESTATUS.CASE_DESCRIPTION = _this.txtDescripcion.Text;

            /*
            SDCONSOLEATENTION.SDCASECOMPLTE.CASE_ISMAYOR=SetSDOperationCase.SDOPERATION_CASESTATUS.CASE_ISMAYOR;
            SDCONSOLEATENTION.SDCASEMTCOMPLTE.IDMDPRIORITY = SetSDOperationCase.SDOPERATION_CASESTATUS.IDMDPRIORITY;
            SDCONSOLEATENTION.SDCASEMTCOMPLTE.PRIORITYNAME = GetDisplayPriority(SetSDOperationCase.SDOPERATION_CASESTATUS.IDMDPRIORITY);
            SDCONSOLEATENTION.SDCASEMTCOMPLTE.CATEGORY = CATEGORY;
            SDCONSOLEATENTION.SDCASEMTCOMPLTE.CATEGORYNAME = CATEGORYNAME;
             */ 
        }
        else
        {
            _this.SetSDOperationCase.SDOPERATION_CASESTATUS.IDSDCASESETMODELTYPE = UsrCfg.SD.Properties.TIDSDCASESETMODELTYPE._InsertMT;
            _this.SetSDOperationCase.SDOPERATION_CASESTATUS.CASESTATUS_MESSAGE = _this.TxtDescripcion.Text;
            _this.SetSDOperationCase.SDOPERATION_CASESTATUS.MT_IDMDMODELTYPED = _this.TfrSDSLASelect.MT_IDMDMODELTYPED;
            _this.SetSDOperationCase.SDOPERATION_CASESTATUS.IDMDMODELTYPED = _this.TfrSDSLASelect.MT_IDMDMODELTYPED;
            _this.SetSDOperationCase.SDOPERATION_CASESTATUS.IDMDCATEGORYDETAIL = _this.TfrSDSLASelect.IDMDCATEGORYDETAIL;
            _this.SetSDOperationCase.SDOPERATION_CASESTATUS.IDSLA = _this.TfrSDSLASelect.IDSLA;
            _this.SetSDOperationCase.SDOPERATION_CASESTATUS.IDMDIMPACT =  _this.TfrSDSLASelect.IDIMPACT;
            _this.SetSDOperationCase.SDOPERATION_CASESTATUS.IDMDPRIORITY = _this.TfrSDSLASelect.IDPRIORITY;
            _this.SetSDOperationCase.SDOPERATION_CASESTATUS.IDMDURGENCY = _this.TfrSDSLASelect.IDURGENCY;
            _this.SetSDOperationCase.SDOPERATION_CASESTATUS.CASE_ISMAYOR = _this.TfrSDSLASelect.CheckisMayor.Checked;
            _this.SetSDOperationCase.SDOPERATION_CASESTATUS.CASE_TITLE = _this.TfrSDSLASelect.txtTitulo.Text;
            _this.SetSDOperationCase.SDOPERATION_CASESTATUS.CASE_DESCRIPTION = _this.TfrSDSLASelect.txtDescripcion.Text;
             
        }
        
        _this.OnModalResult(_this.SetSDOperationCase, _this.ParentThis, _this);

    }
    else
    {
        alert(  UsrCfg.Traslate.GetLangText(this.name, "Lines2"));
    }
}

ItHelpCenter.SD.Shared.Atention.frSDCaseAtentionChangeSLA.prototype.importarScript = function (nombre, onSuccess, onError) {
    var s = document.createElement("script");
    s.onload = onSuccess;
    s.onerror = onError;
    s.src = nombre;
    document.querySelector("head").appendChild(s);
}