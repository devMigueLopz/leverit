﻿ItHelpCenter.SD.CaseUser.NewCase.TBasic_SeekSearch = function (inObject, incallback, inPhrase, inShowMsg) {    
    var _this = this;

    this.ObjectHtml = inObject; //div donde se mostrara la bsuqueda  
    this.Callback = incallback;
    this.ShowMsg = false;
    this.Phrase = inPhrase; //frase a buscar     
    this.Options = {
        ID: {},
        Detail: {},
        Path: {},
        PhraseIn: {}
    }
 
    this.ShowMsg = inShowMsg;


    $(_this.ObjectHtml).html = "";
    //$(_this.ObjectHtml).css("border", "1px solid red");
    //$(_this.ObjectHtml).css("margin-left", "15px");    
         
        //Parameters: HTML Cont, Function Back, ID, Frase a buscar, Mostrar Texto de busqueda (true - false), mostrar label resultado (true - false), si no hay resultado regresarse (true - false)
        var frSeek = new ItHelpCenter.SD.Shared.Category.TfrSeekSearch(_this.ObjectHtml, function (IDCategory, PhraseCategory, pathCategory, PhraseIn) {                    
            if (IDCategory > 0) {
                //cargar newcase    
                _this.Options.ID = IDCategory;
                _this.Options.Detail = PhraseCategory;
                _this.Options.Path = pathCategory;
                _this.Options.PhraseIn = _this.Phrase;
                _this.Callback(_this);
            } else {               
                //mostrar busqueda por categorias
                
                    _this.ObjectHtml.innerHTML = "";                   
                    var IDSDTYPEUSER = 4;
                    try {
                        var CategoryBasic = new ItHelpCenter.SD.Shared.Category.TCategoryBasic(
                        _this.ObjectHtml,
                        function (OutRes) {
                            //cargar newcase   
                            _this.Options.ID = OutRes.ID;
                            _this.Options.Detail = OutRes.Detail;
                            _this.Options.Path = OutRes.Path;
                            _this.Options.PhraseIn = _this.Phrase;
                            _this.Callback(_this);
                        },
                        IDSDTYPEUSER
                        );
                    } catch (e) {
                        SysCfg.Log.Methods.WriteLog("Basic_SeekSearch.js ItHelpCenter.SD.CaseUser.NewCase.TBasic_SeekSearch", e);
                        //location.reload();
                    };
                 
            }
        }, "1", _this.Phrase, false, _this.ShowMsg, true);

    
}

