﻿
ItHelpCenter.SD.Shared.Category.TCategoryBasic = function (inObject, incallbackModalResult, inIDSDTYPEUSER) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.Object = inObject;
    this.CallbackModalResult = incallbackModalResult;//CallbackModalResult(OutStr);        
    //******************************************************
    this.IDSDTYPEUSER = inIDSDTYPEUSER;
    this.IdCategory = 0;//MDCATEGORYDETAIL.IDMDCATEGORY;
    this.IdCategoryDetail = 0;//MDCATEGORYDETAIL.IDMDCATEGORYDETAIL;
    this.CategoryName = "None";//MDCATEGORYDETAIL.CATEGORYNAME;
    this.CategoryDescription = "None";//MDCATEGORYDETAIL.CATEGORYDESCRIPTION;
    this.Path = "None";//MDCATEGORYDETAIL.MDCATEGORY.ToPath();    
    this.Mythis = "TCategoryBasic";
                  
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Category1", "Category 1");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Category2", "Category 2");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Category3", "Category 3");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Category4", "Category 4");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Category5", "Category 5");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Category6", "Category 6");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Category7", "Category 7");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Category8", "Category 8");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2", "Cancel");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lbltextSeek", "Select your category");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnResult", "Use this resulty");
       
    this.importStyle = function (nombre, onSuccess, onError) { //LFVA: Carga estilo
        var style = document.createElement("link");
        style.rel = "stylesheet";
        style.type = "text/css";
        style.href = nombre;
        var s = document.head.appendChild(style);
        s.onload = onSuccess;
        s.onerror = onError;
    }

    this.importStyle(SysCfg.App.Properties.xRaiz + "Css/Component/UCode/VCL/GridControlCF/GridToolTip.css", function () {

    }, function (e) {
    });

    var parameters = {
        IDSDTYPEUSER: _this.IDSDTYPEUSER
    };

    $.ajax({
        type: "POST",
        url: SysCfg.App.Properties.xRaiz + 'SD/Shared/Category/Basic/CategoryBasic.svc/GETSDTYPEUSER_CATEGORYDETAILList',
        data: JSON.stringify(parameters),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {                 
                                   
            //Title
            var div1 = new TVclStackPanel(_this.Object, "divBasic1" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
            var div1Cont = div1.Column[0].This;
            //Body 
            var div2 = new TVclStackPanel(_this.Object, "divBasic2" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
            var div2Cont = div2.Column[0].This;

            var div3 = new TVclStackPanel(_this.Object, "divBasic3" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
            var div3Cont = div3.Column[0].This;

            //LABEL TITLE
            var labelTitle = new TVcllabel(div1Cont, "", TlabelType.H0);
            labelTitle.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lbltextSeek");
            $(div1Cont).css('text-align', "center");
            $(labelTitle.This).css('font-size', "14px");
            $(labelTitle.This).css('margin-bottom', "8px");
                    
            //BODY - Combos
            _this.SETCATEGORYNODE(response.SDTYPEUSER_CATEGORYDETAIL.CATEGORYNODEBeginList, div2Cont, div3Cont);
        },
        error: function (response) {
            SysCfg.Log.Methods.WriteLog("CategoryBasic.js ItHelpCenter.SD.Shared.Category.TCategoryBasic SD/Shared/Category/Basic/CategoryBasic.svc/GETSDTYPEUSER_CATEGORYDETAILList " + "Error no llamo al servicio");
        }
    });
         
}

ItHelpCenter.SD.Shared.Category.TCategoryBasic.prototype.SETCATEGORYNODE = function (CATEGORYList, TagMainCont, GridDiv) {//List<Persistence.Model.Properties.TCATEGORYNODE>
    var _this = this.TParent();
    var divbd = new TVclStackPanel(TagMainCont, "divbd" + _this.ID, 4, [[0, 12, 12, 0], [1, 5, 5, 1], [2, 2, 4, 4], [2, 2, 4, 4], [2, 2, 4, 4]]);
    var divbdLabel = divbd.Column[1].This; //Label 
    var divbdCombo = divbd.Column[2].This; //Combo    
     

    var label1 = new TVcllabel(divbdLabel, "", TlabelType.H0);
    label1.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Category"+TagMainCont.childNodes.length);
    label1.VCLType = TVCLType.BS;
     
    var VclComboBox2 = new TVclComboBox2(divbdCombo, '');
    $(VclComboBox2.This).css("width", "100%");
    VclComboBox2.Tag = null;
    for (var i = 0; i < CATEGORYList.length; i++) {
        var VclComboBoxItem = new TVclComboBoxItem();
        VclComboBoxItem.Value = CATEGORYList[i].Index;
        VclComboBoxItem.Text = CATEGORYList[i].NODENAME;
        VclComboBoxItem.Tag = CATEGORYList[i];
        VclComboBox2.AddItem(VclComboBoxItem);
    }
    $(VclComboBox2).css("width", "100%");
    VclComboBox2.onChange = function () {
        $(_this.Object).css('margin-top', "10px");

        if (VclComboBox2.Tag != null)
        {
            _this.DeleteCascade(VclComboBox2.Tag);
            //var cmb = $(VclComboBox2.This.parentNode.parentNode).nextAll().remove();
        }
        CATEGORYNODE = VclComboBox2.Options[VclComboBox2.This.selectedIndex].Tag;
        if (CATEGORYNODE.CATEGORYNODEList.length != 0) {    
            $(GridDiv).html("");
            VclComboBox2.Tag = _this.SETCATEGORYNODE(CATEGORYNODE.CATEGORYNODEList, TagMainCont, GridDiv);
        }
        else {
            if ((CATEGORYNODE.MDCATEGORY != null) && (CATEGORYNODE.IsEndNode)) {               
                _this.FillGrid(CATEGORYNODE.MDCATEGORY, GridDiv);
            }
        }
    }
    return VclComboBox2;
}

ItHelpCenter.SD.Shared.Category.TCategoryBasic.prototype.ToPath = function (MDCATEGORY)
{
    var _this = this.TParent();
    var path = "";
    path = (MDCATEGORY.CATEGORY1 != "" ? MDCATEGORY.CATEGORY1 : "");
    path += (MDCATEGORY.CATEGORY2 != "" ? "\\" + MDCATEGORY.CATEGORY2 : "");
    path += (MDCATEGORY.CATEGORY3 != "" ? "\\" + MDCATEGORY.CATEGORY3 : "");
    path += (MDCATEGORY.CATEGORY4 != "" ? "\\" + MDCATEGORY.CATEGORY4 : "");
    path += (MDCATEGORY.CATEGORY5 != "" ? "\\" + MDCATEGORY.CATEGORY5 : "");
    path += (MDCATEGORY.CATEGORY6 != "" ? "\\" + MDCATEGORY.CATEGORY6 : "");
    path += (MDCATEGORY.CATEGORY7 != "" ? "\\" + MDCATEGORY.CATEGORY7 : "");
    path += (MDCATEGORY.CATEGORY8 != "" ? "\\" + MDCATEGORY.CATEGORY8 : "");
    return path;
}

ItHelpCenter.SD.Shared.Category.TCategoryBasic.prototype.FillGrid = function (MDCATEGORY, GridDiv) {
    var _this = this.TParent();   
     

    _this.HeaderGrid(GridDiv, _this.ID);
    for (var i = 0; i < MDCATEGORY.MDCATEGORYDETAILList.length; i++) {
        try {
            var ID = MDCATEGORY.MDCATEGORYDETAILList[i].IDMDCATEGORYDETAIL;
            var Descript =  MDCATEGORY.MDCATEGORYDETAILList[i].CATEGORYDESCRIPTION;
            var Name = MDCATEGORY.MDCATEGORYDETAILList[i].CATEGORYNAME;
            var Path = _this.ToPath(MDCATEGORY);
            _this.NewGridFile(GridDiv, i, ID, Descript, Name, Path);
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("CategoryBasic.js ItHelpCenter.SD.Shared.Category.TCategoryBasic.prototype.FillGrid", e);
        }
    }

    //var Table = Utable(GridDiv, "");
    //var TableHead = Uthead(Table, "");
    //var TablaHeadRow = Utr(TableHead, "");
    //$(Uth(TablaHeadRow, "")).append("CATEGORYDESCRIPTION");
    //$(Uth(TablaHeadRow, "")).append("CATEGORYNAME");
    //$(Uth(TablaHeadRow, "")).append("CATEGORYSTATUS");
    ////$(Uth(TablaHeadRow, "")).append("CUSTOMCODE");
    //$(Uth(TablaHeadRow, "")).append("IDMDCATEGORY");
    //$(Uth(TablaHeadRow, "")).append("IDMDCATEGORYDETAIL");
    //var TableBody = Utbody(Table, "");
    //for (var i = 0; i < MDCATEGORY.MDCATEGORYDETAILList.length; i++) {
    //    var TablaBodyRow = Utr(TableBody, "");
    //    TablaBodyRow.onclick = function () {
    //        _this.IdCategory = parseInt(this.cells[3].innerHTML);//MDCATEGORYDETAIL.IDMDCATEGORY;
    //        _this.IdCategoryDetail = parseInt(this.cells[4].innerHTML);//MDCATEGORYDETAIL.IDMDCATEGORYDETAIL;
    //        _this.CategoryName = this.cells[1].innerHTML;//MDCATEGORYDETAIL.CATEGORYNAME;
    //        _this.CategoryDescription = this.cells[0].innerHTML;//MDCATEGORYDETAIL.CATEGORYDESCRIPTION;
    //        _this.Path = _this.ToPath(MDCATEGORY);//MDCATEGORYDETAIL.MDCATEGORY.ToPath();
    //        _this.CallbackModalResult(_this);
    //    };
    //    $(Utd(TablaBodyRow, "")).append(MDCATEGORY.MDCATEGORYDETAILList[i].CATEGORYDESCRIPTION);//0
    //    $(Utd(TablaBodyRow, "")).append(MDCATEGORY.MDCATEGORYDETAILList[i].CATEGORYNAME);//1
    //    $(Utd(TablaBodyRow, "")).append(MDCATEGORY.MDCATEGORYDETAILList[i].CATEGORYSTATUS);//2
    //    //$(Utd(TablaBodyRow, "")).append(MDCATEGORY.MDCATEGORYDETAILList[i].CUSTOMCODE);//3
    //    $(Utd(TablaBodyRow, "")).append(MDCATEGORY.MDCATEGORYDETAILList[i].IDMDCATEGORY);//3
    //    $(Utd(TablaBodyRow, "")).append(MDCATEGORY.MDCATEGORYDETAILList[i].IDMDCATEGORYDETAIL);//4
    //}

    $(GridDiv).css("border-top", "1px solid #c4c4c4");
    $(GridDiv).css("border-bottom", "1px solid #c4c4c4");
    
}

ItHelpCenter.SD.Shared.Category.TCategoryBasic.prototype.DeleteCascade = function (VclComboBox2) {
    var _this = this.TParent();
    if (VclComboBox2.Tag != null)
    {                
        _this.DeleteCascade(VclComboBox2.Tag);
    }
    VclComboBox2.This.parentNode.parentNode.parentNode.removeChild(VclComboBox2.This.parentNode.parentNode);
}


ItHelpCenter.SD.Shared.Category.TCategoryBasic.prototype.HeaderGrid = function (Object, ID) {
    var _this = this.TParent();
    StackPanelDef = new Array(1);
    StackPanelDef[0] = new Array(3);
    var StackPanel = VclDivitions(Object, "GridCategory_Hd_" + ID, StackPanelDef);
    BootStrapCreateRow(StackPanel[0].This);
    BootStrapCreateColumns(StackPanel[0].Child, [[5, 4, 3], [5, 5, 2], [5, 5, 2], [5, 5, 2], [5, 5, 2]]);

    $(StackPanel[0].Child[0].This).css("text-align", "left");
    $(StackPanel[0].Child[1].This).css("text-align", "left");
    $(StackPanel[0].Child[2].This).css("text-align", "center");
     

    var GridCategory_Hd1 = new Vcllabel(StackPanel[0].Child[0].This, "GridCategory_Hd" + ID + "1", TVCLType.BS, TlabelType.H0, "CATEGORY DESCRIPTION");
    var GridCategory_Hd2 = new Vcllabel(StackPanel[0].Child[1].This, "GridCategory_Hd" + ID + "2", TVCLType.BS, TlabelType.H0, "CATEGORY NAME");
    var GridCategory_Hd3 = new Vcllabel(StackPanel[0].Child[2].This, "GridCategory_Hd" + ID + "3", TVCLType.BS, TlabelType.H0, "ACTION");
  
    
    $("#" + StackPanel[0].This.id + " .control-label").css('font-weight', "normal");
    $(StackPanel[0].Child[2].This).css("text-align", "center");   
    $(StackPanel[0].This).css('border-bottom', "1px solid #c4c4c4"); 
    $(StackPanel[0].This).css('width', "100%");
    $(StackPanel[0].This).css('margin', "0px");
}

ItHelpCenter.SD.Shared.Category.TCategoryBasic.prototype.NewGridFile = function (Object, IDx, inID, inDescript, inName, inPath) {
    var _this = this.TParent();

    idx = 'row_' + IDx;
    StackPanelDef = new Array(1);
    StackPanelDef[0] = new Array(3);
    var StackPanel = VclDivitions(Object, idx, StackPanelDef);
    BootStrapCreateRow(StackPanel[0].This);

    BootStrapCreateColumns(StackPanel[0].Child, [[5, 4, 3], [5, 5, 2], [5, 5, 2], [5, 5, 2], [5, 5, 2]]);
  
    $(StackPanel[0].Child[0].This).css("text-align", "left");
    $(StackPanel[0].Child[1].This).css("text-align", "left");
    $(StackPanel[0].Child[2].This).css("text-align", "center");

    $(StackPanel[0].Child[0].This).css("overflow", "hidden");
    $(StackPanel[0].Child[1].This).css("overflow", "hidden");
    $(StackPanel[0].Child[2].This).css("overflow", "hidden");

    var LabelHd1 = new Vcllabel(StackPanel[0].Child[0].This, "labGrid1_" + _this.ID, TVCLType.BS, TlabelType.H0, inDescript);

    if (SysCfg.App.Properties.Device.value == SysCfg.App.TDevice.Desktop.value) {
        var LabelHd2 = new Vcllabel(StackPanel[0].Child[1].This, "labGrid2_" + _this.ID, TVCLType.BS, TlabelType.H0, inPath);
    } else {
        try {  
            var linkCadena = document.createElement("a");
            linkCadena.innerHTML = inPath;
            linkCadena.onclick = function (e) {
                var textAreaStrech = document.createElement("textarea");
                textAreaStrech.cols = "20";
                textAreaStrech.rows = "6";
                textAreaStrech.innerHTML = inPath;
                var modalFV = new Componet.GridCF.TGridCFView.TModalFV(textAreaStrech, linkCadena);
                var evt = e ? e : window.event;
                if (evt.stopPropagation) evt.stopPropagation();
                if (evt.cancelBubble != null) evt.cancelBubble = true;
            }

            // StackPanel[0].Child[1].This.style.maxWidth = 300 + "px";
            StackPanel[0].Child[1].This.style.overflow = "hidden";
            StackPanel[0].Child[1].This.style.textOverflow = "ellipsis";
            StackPanel[0].Child[1].This.style.whiteSpace = "nowrap";
            StackPanel[0].Child[1].This.appendChild(linkCadena);

        } catch (e) {
            debugger
        }
    } 
    

    //BTN Use
    var btnUSE = new TVclButton(StackPanel[0].Child[2].This, "btnUSE_" + _this.ID + idx);
    btnUSE.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnResult");
    btnUSE.VCLType = TVCLType.BS;
    btnUSE.onClick = function myfunction() {
        var OutRes = { "ID": inID, "Name": inName, "Detail": inDescript, "Path": inPath };
        _this.CallbackModalResult(OutRes);
    }   
    $(btnUSE.This).css("max-Width", "95%");
    $(btnUSE.This).css("overflow", "hidden");
    $(btnUSE.This).css("white-space", "initial");

    $(StackPanel[0].This).css('width', "100%");    
    $(StackPanel[0].This).css('margin', "0px"); 
    $(StackPanel[0].This).css('margin-bottom', "5px"); 

    $(StackPanel[0].Child[0].This).css('padding-top', "10px");
    $(StackPanel[0].Child[1].This).css('padding-top', "10px");
    $(StackPanel[0].Child[2].This).css('padding-top', "5px");
}



//anexo
/*
CATEGORY1                CATEGORY2        CATEGORY3      CATEGORY4    CATEGORY5    CATEGORY6    CATEGORY7    CATEGORY8    IDMDCATEGORY  
Servicio de Seguridad    Desconocido                                                                                      1             
Servicio de Seguridad    Firewall         Desconocido                                                                     2             
Servicio de Seguridad    Antivirus        Servidor                                                                        3             
Servicio de Seguridad    Antivirus        Personal                                                                        4             
Servicio de Impresion    Desconocido                                                                                      5             
Servicio de Impresion    Impresion Web    Desconocido                                                                     6             
Servicio de Nomina       Desconocido                                                                                      7             
Servicio de Nomina       Servidor                                                                                         8             
*/

/*Manda a web sercice 2
 
*/

