﻿ItHelpCenter.SD.Shared.Category.Mode = {
    SeekSearch: { value: 0, name: "SeekSearch" },
    CategoryBasic: { value: 1, name: "CategoryBasic" },
    Basic_SeekSearch: { value: 2, name: "Basic_SeekSearch" }
};

//Parameters: (6) = 1.HTML Container, 2.Function Back, 3.Modo busqueda, 4.Ver boton cambio de modo (true - False), 5.Ver mensaje resultado, 6.Frase predeterminada    
ItHelpCenter.SD.Shared.Category.TCategoryManger = function (inObject, incallback, inCategory_Mode, inShowButton, inShowMsgResult, inPhrase, inExactPhrase) {
    this.TParent = function () {
        return this;
    }.bind(this);    

    var _this = this.TParent();
    this.ObjectHtml_in = inObject;
    this.ObjectHtml = null;

    
    this.CallbackModalResult = incallback;
    this.Category_Mode = inCategory_Mode.value;   
    this.ShowButton = inShowButton; //false - true //Mostrar boton de cambio de modo
    this.ShowMsgResult = inShowMsgResult;//false - true // Mostrar mensaje de resultado
    this.Phrase = inPhrase; // frase predeterminada para la busqueda
    this.ExactPhrase = inExactPhrase; //chek de frase exacta

    this.Options = {
        ID: {},
        Detail: {},
        Path: {},
        PhraseIn: {}
    }

    //TRADUCCION          
    this.Mythis = "TfrCategoryManager";
                      
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnChangeSeek1", "Normal Search");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnChangeSeek2", "Search for Category");                            


    _this.importarScript(SysCfg.App.Properties.xRaiz + "SD/Shared/Category/SeekSearch/UCategorySeekSearch.js", function () {
        _this.importarScript(SysCfg.App.Properties.xRaiz + "SD/Shared/Category/Basic/CategoryBasic.js", function () {
            _this.importarScript(SysCfg.App.Properties.xRaiz + "SD/Shared/Category/Basic_SeekSearch/Basic_SeekSearch.js", function () {
                _this.Load();//Funcion principal del archivo actual
            }, function (e) {
            });
        }, function (e) {
        });
    }, function (e) {
    });
 
}

ItHelpCenter.SD.Shared.Category.TCategoryManger.prototype.Load = function () {
    var _this = this.TParent();


    _this.ObjectHtml = _this.ObjectHtml_in;
    //var ObjectHtmlR = new TVclStackPanel(_this.ObjectHtml_in, "CONT_CategoryManager", 1, [[12], [12], [12], [12], [12]]);
    //_this.ObjectHtml = ObjectHtmlR.Column[0].This; //CONTAINER   

   // $(_this.ObjectHtml_in).css("border", "1px solid red");
    //$(_this.ObjectHtml).css("border", "1px solid blue");

    $(_this.ObjectHtml).html("");
   // $(_this.ObjectHtml).css("padding", "5px 15px");

    //estructura de la busqueda   
    var divSeek1 = new TVclStackPanel(_this.ObjectHtml, "divSeek1", 1, [[12], [12], [12], [12], [12]]);
    var divSeek1Cont = divSeek1.Column[0].This; //ZONA de la busqueda

    var divSeek2 = new TVclStackPanel(_this.ObjectHtml, "divSeek2", 1, [[12], [12], [12], [12], [12]]);
    var divSeek2Cont = divSeek2.Column[0].This; //Boton 


    //Boton para cambiar la forma de busqueda
    if (_this.Category_Mode != 2) {        
        var btnChangeSearch = new TVclButton(divSeek2Cont, "btnChangeSearch_" + _this.ID);
        if (_this.Category_Mode == 1) {
            btnChangeSearch.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnChangeSeek1");
        } else {
            btnChangeSearch.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnChangeSeek2");
        }

        btnChangeSearch.VCLType = TVCLType.BS;
        btnChangeSearch.Src = "";
        btnChangeSearch.onClick = function myfunction() {
            if (_this.Category_Mode == 1) {
                btnChangeSearch.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnChangeSeek2");
                _this.Category_Mode = 0;
                _this.SeekSearchNormal(divSeek1Cont);
            } else {
                btnChangeSearch.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnChangeSeek1");
                _this.Category_Mode = 1;
                _this.SeekSearchBasic(divSeek1Cont);
            }
        }
        $(btnChangeSearch.This).css("margin", "5px");
        $(divSeek2Cont).css("text-align", "center");
    }
    
   

    //CARGAR LA BUSQUEDA      
    if (_this.Category_Mode == 0) {
        _this.SeekSearchNormal(divSeek1Cont);
    } else if (_this.Category_Mode == 1) {
        _this.SeekSearchBasic(divSeek1Cont);
    } else if (_this.Category_Mode == 2) {
        _this.Basic_Seeksearch(divSeek1Cont);
    } else {
        _this.SeekSearchNormal(divSeek1Cont);
    }

    
}

//MODOS
ItHelpCenter.SD.Shared.Category.TCategoryManger.prototype.SeekSearchNormal = function (objectDiv) {
    var _this = this.TParent();
    $(objectDiv).html("");

    //Parameters: HTML Cont, Function Back, ID, Frase a buscar, Mostrar Texto de busqueda (true - false), mostrar label resultado (true - false), si no hay resultado regresarse (true - false)
                                                                    
    var frSeek = new ItHelpCenter.SD.Shared.Category.TfrSeekSearch(objectDiv, function (IDCategory, PhraseCategory, pathCategory, PhraseIn) {       
            _this.Options.ID = IDCategory;
            _this.Options.Detail = PhraseCategory;
            _this.Options.Path = pathCategory;
            _this.Options.PhraseIn = PhraseIn;
            _this.CallbackModalResult(_this.Options);
    }, "1", "", true, _this.ShowMsgResult, false, _this.ObjectHtml_in, _this.ExactPhrase);
}

ItHelpCenter.SD.Shared.Category.TCategoryManger.prototype.SeekSearchBasic = function (objectDiv) {
    var _this = this.TParent();    
    $(objectDiv).html("");

    var IDSDTYPEUSER = 4;
    try {
        var CategoryBasic = new ItHelpCenter.SD.Shared.Category.TCategoryBasic(
        objectDiv,
        function (OutRes) {
            _this.Options.ID = OutRes.ID;
            _this.Options.Detail = OutRes.Detail;
            _this.Options.Path = OutRes.Path;
            _this.Options.PhraseIn = "";

            _this.CallbackModalResult(_this.Options);
             
        },
        IDSDTYPEUSER
        );
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("CategoryManager.js ItHelpCenter.SD.Shared.Category.TCategoryManger.prototype.SeekSearchBasic", e);
        //location.reload();
    };
}

ItHelpCenter.SD.Shared.Category.TCategoryManger.prototype.Basic_Seeksearch = function (objectDiv) {
    var _this = this.TParent();
    $(objectDiv).html("");

    var BasicSeek = new ItHelpCenter.SD.CaseUser.NewCase.TBasic_SeekSearch(
      objectDiv,       
       function (_OutRes) {
           _this.CallbackModalResult(_OutRes.Options);
       }
        ,_this.Phrase, _this.ShowMsgResult
    );
     
}

ItHelpCenter.SD.Shared.Category.TCategoryManger.prototype.importarScript = function (nombre, onSuccess, onError) {
    var s = document.createElement("script");
    s.onload = onSuccess;
    s.onerror = onError;
    s.src = nombre;
    document.querySelector("head").appendChild(s);
}