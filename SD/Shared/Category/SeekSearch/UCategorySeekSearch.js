﻿//Parameters: HTML Cont, Function Back, ID, Frase a buscar, Mostrar Texto de busqueda (true - false), mostrar label resultado (true - false), si no hay resultado regresarse (true - false)
ItHelpCenter.SD.Shared.Category.TfrSeekSearch = function (inObject, incallback, inID, inPhrase, inSHowText, inShowMsgResult, inReturnFunction, inManagerDiv, inExactPhrase) {
    this.ObjectHtml = inObject;
    this.Callback = incallback;
    this.IDCategoryDetail = 0;
    this.ID = inID;   
    this.xAlto = 0;
    this.ManagerDiv = inManagerDiv;

   // this.HeightScreem = $(window).height();
   // this.xAlto = (this.HeightScreem / 3);
    
    try {
        this.HeightDiv = inManagerDiv.offsetHeight;
        if (this.HeightDiv > 200) {
            this.xAlto = (this.HeightDiv / 2) - 100;
        }   
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("UCategorySeekSearch.js ItHelpCenter.SD.Shared.Category.TfrSeekSearch", e);

    } 

    this.ShowTextSearch = inSHowText;
    this.DirecctPhrase = inPhrase;
    this.SeekPhrase = "";
    this.textoSearch = null;
    this.ShowMsgResult = inShowMsgResult;
    this.ReturnFunction = inReturnFunction;
    this.ExactPhrase = inExactPhrase;

    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();

    this.Mythis = "UcategorySeekSearch";
    this.Check = false;
    this.LabelResult = null; //informaciopn del resultado
    this.ContLabel = null; //contenedor del label de resultado
    this.ContGrid = null; //contenedor del grid resutlado

    //$(this.ObjectHtml).css('border', "1px solid red");    
    $(this.ObjectHtml).css('margin-top', _this.xAlto + "px");

    //llamar funciones de Traslate.js   
    var _this = this.TParent();
     
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lbltextSeek", "Type the words or phrases that best describe your application");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnResult", "Use this result");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "AlertEmpty", "Enter a phrase");                    
    UsrCfg.Traslate.GetLangText(_this.Mythis, "GridHeader1", "Category Detail");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "GridHeader2", "Category");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "GridHeader3", "Action");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "ChekLabel", "Exact Phrase");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "DResultstitle", "Select the phrase that more it seems to your report");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "NotFound", "No results found.");
   

    this.importStyle = function (nombre, onSuccess, onError) { //LFVA: Carga estilo
        var style = document.createElement("link");
        style.rel = "stylesheet";
        style.type = "text/css";
        style.href = nombre;
        var s = document.head.appendChild(style);
        s.onload = onSuccess;
        s.onerror = onError;
    }

    this.importStyle(SysCfg.App.Properties.xRaiz + "Css/Component/UCode/VCL/GridControlCF/GridToolTip.css", function () {

    }, function (e) {
    });

    _this.InitializeComponent();
    
}

ItHelpCenter.SD.Shared.Category.TfrSeekSearch.prototype.InitializeComponent = function () {
    var _this = this.TParent();
    var ObjHtml = _this.ObjectHtml;

   // $(ObjHtml).css("border", "1px solid blue");

    //construir el contenido
    var div1 = new TVclStackPanel(ObjHtml, "Seek_div1_" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
    var div1Cont = div1.Column[0].This;
    var div2 = new TVclStackPanel(ObjHtml, "Seek_div2_" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
    var div2Cont = div2.Column[0].This;
    var div2 = new TVclStackPanel(ObjHtml, "Seek_div2_" + _this.ID, 4, [[0, 10, 2, 0], [3, 6, 1, 2], [3, 6, 1, 2], [3, 6, 1, 2], [3, 6, 1, 2]]);
    var div2_1Cont = div2.Column[1].This;//Textbox
    var div2_2Cont = div2.Column[2].This;//lupa
    var div3 = new TVclStackPanel(ObjHtml, "Seek_div3_" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
    var div3Cont = div3.Column[0].This;
    var div4 = new TVclStackPanel(ObjHtml, "Seek_div4_" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
    var div4Cont = div4.Column[0].This;
    var div5 = new TVclStackPanel(ObjHtml, "Seek_div5_" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
    var div5Cont = div5.Column[0].This;
    _this.ContGrid = div5Cont;
  
    var labTitle = new Vcllabel(div1Cont, "labTitle_" + _this.ID, TVCLType.BS, TlabelType.H0,UsrCfg.Traslate.GetLangText(_this.Mythis,"lbltextSeek"));
    $(div1Cont).css('text-align', "center");
    $("#labTitle").css('font-size', "18px");
    $("#labTitle").css('margin-bottom', "8px");

    //Textbox 
    _this.textoSearch = new TVclTextBox(div2_1Cont, "TxBxSearch_" + _this.ID);
    _this.textoSearch.onKeyPress = function (e) {
        tecla = (document.all) ? e.keyCode : e.which;
        if (tecla == 13) {
            _this.GetID(_this.textoSearch.This.value, div5Cont);
        }
    }
    //$(_this.textoSearch.This).css('border', "1px solid #000000");
    $(_this.textoSearch.This).css('width', "100%");
    $(div2).css('padding-bottom', "10px");
    $(div2).css('width', "50%");
    $(div2).css('font-size', "16px");
    $(_this.textoSearch.This).addClass("TextFormat");

    //Boton      
    var imgSearch = new TVclImagen(div2_2Cont, "btnSearch_" + _this.ID);
    imgSearch.Src = "image/24/Search.png"
    imgSearch.Cursor = "pointer";
    imgSearch.onClick = function () {
        _this.GetID(_this.textoSearch.This.value, div5Cont);
    }  
    $(div2_2Cont).css("padding-top", "10px");
    imgSearch.This.style.float = "left";

    //Chekbox
    var CheckExact = new TVclInputcheckbox(div3Cont, "checkExact");
    CheckExact.Checked = false;
    CheckExact.VCLType = TVCLType.BS;
    CheckExact.onClick = function () {     
        if (_this.textoSearch.This.value != null && _this.textoSearch.This.value != "") {
            _this.GetID(_this.textoSearch.This.value, div5Cont);
        }
    }   
    _this.Check = CheckExact;

    //Label chekbox 
    var LabCheck = new Vcllabel(div3Cont, "LabCheck_" + _this.ID, TVCLType.BS, TlabelType.H0,UsrCfg.Traslate.GetLangText(_this.Mythis, "ChekLabel"));
    $(LabCheck).css("margin-left", "5px");

    //Label 2   
    _this.LabelResult = new TVcllabel(div4Cont, "LabRes_" + _this.ID, TlabelType.H0);
    _this.LabelResult.Text =UsrCfg.Traslate.GetLangText(_this.Mythis, "DResultstitle");
    _this.LabelResult.VCLType = TVCLType.BS;
    _this.ContLabel = div4Cont;
    $(_this.ContLabel).css("display", "none");
    $(_this.ContLabel).css("text-align", "center");

    $(div3Cont).css("text-align", "center");
    //$(div3Cont).css('display', "none");
    //$(div3Cont).css('text-align', "center");
    $("#LabRes_" + _this.ID).css('margin-bottom', "10px");
    $("#LabRes_" + _this.ID).css('font-weight', "normal");
    $("#LabRes_" + _this.ID).css('font-size', "16px");
    $(_this.ContGrid).css("display", "none");
      
    //Validar si se raliza una busqueda directa      
    if (_this.ShowTextSearch == false) {
        //ocultar divs
        $(div1Cont).css("display", "none");
        $(div2_1Cont).css("display", "none");
        $(div2_2Cont).css("display", "none");
        $(div3Cont).css("display", "none");
        $(_this.ObjectHtml).css('margin-top', "0px");
        //enviar frase a buscar      
        _this.GetID(_this.DirecctPhrase, div5Cont);
    } else {
        _this.textoSearch.This.value = _this.DirecctPhrase;         
    }

    if (_this.ExactPhrase == true) {
        CheckExact.Checked = true;
    }
   
};

ItHelpCenter.SD.Shared.Category.TfrSeekSearch.prototype.GetID = function (inFrase, inObejct) {
    var _this = this.TParent();    
 
    _this.SeekPhrase = inFrase;

    //validar  frase vacia
    inObejct.innerHTML = "";
    $("#GridMainCont_" + _this.ID + "_0_2").css('display', "none");// label
    
    if (_this.ShowTextSearch == true) {
        $(_this.ObjectHtml).css('margin-top', _this.xAlto + "px");
    }   

    if (_this.SeekPhrase.trim() == "") {
        alert( UsrCfg.Traslate.GetLangText(_this.Mythis, "AlertEmpty")); 
        return;
    }

    //construir estructura de la respuesta
    var divRes1 = new TVclStackPanel(inObejct, "Seek_divRes1_" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
    var divRes1Cont = divRes1.Column[0].This;
    $(divRes1.Row.This).css("padding-left", "0px");
    $(divRes1.Row.This).css("padding-right", "0px");
    $(divRes1.Row.This).css("padding-botom", "0px");
    $(divRes1Cont).addClass("StackPanelContainer");

    $(divRes1Cont).css("padding-left", "0px");
    $(divRes1Cont).css("padding-right", "0px"); 

    if (SysCfg.App.Properties.Device.value == SysCfg.App.TDevice.Desktop.value) {
        $(divRes1Cont).css("padding-left", "10px");
        $(divRes1Cont).css("padding-right", "10px");
        $(divRes1.Row.This).css("padding-left", "20px");
        $(divRes1.Row.This).css("padding-right", "20px");
    }

    var divRes2 = new TVclStackPanel(inObejct, "Seek_divRes2_" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
    var divRes2Cont = divRes2.Column[0].This;
    $(divRes2.Row.This).css("padding-top", "0px");
    $(divRes2.Row.This).css("padding-left", "0px");
    $(divRes2.Row.This).css("padding-right", "0px");
    $(divRes2Cont).addClass("StackPanelContainer");
    $(divRes2Cont).css("margin-top", "0px"); 
    $(divRes2Cont).css("padding-left", "0px");
    $(divRes2Cont).css("padding-right", "0px"); 

    if (SysCfg.App.Properties.Device.value == SysCfg.App.TDevice.Desktop.value) {
        $(divRes2Cont).css("padding-left", "10px");
        $(divRes2Cont).css("padding-right", "10px");         
        $(divRes2.Row.This).css("padding-left", "20px");
        $(divRes2.Row.This).css("padding-right", "20px");
    }

    var dataArray = {
        'frase': _this.SeekPhrase, 'like': _this.Check.Checked
    }

   
    direc = SysCfg.App.Properties.xRaiz + 'SD/Shared/Category/SeekSearch/SeekSerchService.svc/DoSearch';
    $.ajax({
        type: "POST",
        url: direc,
        data: JSON.stringify(dataArray),
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            
            var lista = (typeof response.d) == 'string' ?
                              eval('(' + response.d + ')') :
                              response.d;            
             
            _this.HeaderGrid(divRes1Cont, _this.ID);
              
            for (var i = 0; i < lista.Filas.length; i++) {               
                _this.NewGridFile(divRes2Cont, i, lista.Filas[i].Celdas[1].Valor, lista.Filas[i].Celdas[2].Valor, lista.Filas[i].Celdas[3].Valor);
            }
              
            if (lista.Filas.length > 0) {
                $(_this.ContLabel).css("display", "block");
                $(_this.ContGrid).css("display", "block");
              
                _this.LabelResult.Text =UsrCfg.Traslate.GetLangText(_this.Mythis, "DResultstitle");
                if (_this.ShowMsgResult == false) {
                    _this.LabelResult.Visible = false;
                }                               
            } else {
                $(_this.ContLabel).css("display", "block");
                $(_this.ContGrid).css("display", "none");
                              
                if (_this.ReturnFunction == true) {
                    _this.Callback(0, "", "", _this.SeekPhrase);
                    _this.LabelResult.Text =UsrCfg.Traslate.GetLangText(_this.Mythis, "NotFound");
                   
                    if (_this.ShowMsgResult == false) {
                        _this.LabelResult.Visible = false;
                    }
                } else {
                    //repit seek
                }                
            }
           
            if (_this.ShowTextSearch == true) {
                $(_this.ObjectHtml).css('margin-top', "5px"); //reajustar margin del contenedor
            }            
        },
        error: function (response) {
            SysCfg.Log.Methods.WriteLog("UCategorySeekSearch.js ItHelpCenter.SD.Shared.Category.TfrSeekSearch.prototype.GetID SD/Shared/Category/SeekSearch/SeekSerchService.svc/DoSearch " + response.ErrorMessage);
        }
    });
}

ItHelpCenter.SD.Shared.Category.TfrSeekSearch.prototype.HeaderGrid = function (Object, ID) {
    var _this = this.TParent();
   
    var DivHd = new TVclStackPanel(Object, "GridSeekHd_" + ID, 3, [[4, 4, 4], [5, 4, 3], [4, 5, 3], [4, 5, 3], [4, 6, 2]]);
 
    var DivHd_1Cont = DivHd.Column[0].This;  
    var DivHd_2Cont = DivHd.Column[1].This;  
    var DivHd_3Cont = DivHd.Column[2].This;
    $(DivHd.Row.This).css("Padding", "0px");
     
    $(DivHd_1Cont).css("padding-left", "3px");
    $(DivHd_2Cont).css("padding-left", "3px");
    $(DivHd_3Cont).css("padding-left", "3px");
    $(DivHd_1Cont).css("padding-right", "3px");
    $(DivHd_2Cont).css("padding-right", "3px");
    $(DivHd_3Cont).css("padding-right", "3px");

    var Grid_Hd1 = new Vcllabel(DivHd_1Cont, "GridSeek_Hd" + ID + "_1", TVCLType.BS, TlabelType.H0,UsrCfg.Traslate.GetLangText(_this.Mythis, "GridHeader1"));
    var Grid_Hd2 = new Vcllabel(DivHd_2Cont, "GridSeek_Hd" + ID + "_2", TVCLType.BS, TlabelType.H0,UsrCfg.Traslate.GetLangText(_this.Mythis, "GridHeader2"));
    var Grid_Hd3 = new Vcllabel(DivHd_3Cont, "GridSeek_Hd" + ID + "_3", TVCLType.BS, TlabelType.H0,UsrCfg.Traslate.GetLangText(_this.Mythis, "GridHeader3"));

    $("#" + DivHd.Row.This.id + " .control-label").css('font-weight', "normal");
    $(DivHd.Row.This).css("text-align", "left");

    DivHd.Row.BoderBottom = "1px solid rgba(0, 0, 0, 0.08)";
    $(DivHd.Row.This).css("margin","Auto");
}

ItHelpCenter.SD.Shared.Category.TfrSeekSearch.prototype.NewGridFile = function (Object, IDx, inID, inDetail, inPath) {
    var _this = this.TParent();    
    idx = 'row_' + IDx;

   
    var DivBd = new TVclStackPanel(Object, "GridSeekBd_" + idx, 3, [[4, 4, 4], [5, 4, 3], [4, 5, 3], [4, 5, 3], [4, 6, 2]]);

    var DivBd_1Cont = DivBd.Column[0].This;
    var DivBd_2Cont = DivBd.Column[1].This;
    var DivBd_3Cont = DivBd.Column[2].This;
    
    $(DivBd_1Cont).css("overflow", "hidden");
    $(DivBd_2Cont).css("overflow", "hidden");
    $(DivBd_3Cont).css("overflow", "hidden");

     
    $(DivBd.Row.This).css("Padding", "0px");  

    $(DivBd_1Cont).css("padding-left", "3px");
    $(DivBd_2Cont).css("padding-left", "3px");
    $(DivBd_3Cont).css("padding-left", "6px");
    $(DivBd_1Cont).css("padding-right", "3px");
    $(DivBd_2Cont).css("padding-right", "3px");
    $(DivBd_3Cont).css("padding-right", "3px");

    $(DivBd_1Cont).css("overflow", "hidden");
    $(DivBd_2Cont).css("overflow", "hidden");
    //$(DivBd_3Cont).css("overflow", "hidden");

    var LabelCol1 = new Vcllabel(DivBd_1Cont, "GridSeek_Col1_" + idx, TVCLType.BS, TlabelType.H0, inDetail);
    $(LabelCol1).css("margin-top", "5px");
     
    if (SysCfg.App.Properties.Device.value == SysCfg.App.TDevice.Desktop.value) {
        var LabelCol2 = new Vcllabel(DivBd_2Cont, "GridSeek_Col2_" + idx, TVCLType.BS, TlabelType.H0, inPath);
        $(LabelCol2).css("margin-top", "5px");        
    } else {
        try {  
            var linkCadena = document.createElement("a");
            linkCadena.innerHTML = inPath;
            linkCadena.onclick = function (e) {
                var textAreaStrech = document.createElement("textarea");
                textAreaStrech.cols = "20";
                textAreaStrech.rows = "6";
                textAreaStrech.innerHTML = inPath;
                var modalFV = new Componet.GridCF.TGridCFView.TModalFV(textAreaStrech, linkCadena);
                var evt = e ? e : window.event;
                if (evt.stopPropagation) evt.stopPropagation();
                if (evt.cancelBubble != null) evt.cancelBubble = true;
                } 

            //DivBd_2Cont.style.maxWidth = 300 + "px";
            DivBd_2Cont.style.overflow = "hidden";
            DivBd_2Cont.style.textOverflow = "ellipsis";
            DivBd_2Cont.style.whiteSpace = "nowrap";
            DivBd_2Cont.appendChild(linkCadena); 
                  
            //}
        } catch (e) {
            debugger
        }
    }  

    //BTN ACTION
    var btnAct = new TVclButton(DivBd_3Cont, "btnAct_" + idx);
    $(btnAct.This).css("max-Width", "95%");
    $(btnAct.This).css("overflow", "hidden");
    $(btnAct.This).css("white-space", "initial");  
    btnAct.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnResult");
    btnAct.VCLType = TVCLType.BS;
    btnAct.onClick = function myfunction() {        
        _this.IDCategoryDetail = inID;         
        try {
            _this.Callback(_this.IDCategoryDetail, inDetail, inPath, _this.SeekPhrase);
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("UCategorySeekSearch.js ItHelpCenter.SD.Shared.Category.TfrSeekSearch.prototype.NewGridFile", e);
            _this.Callback(_this.IDCategoryDetail,"","","");
        }
    } 
    $("#" + DivBd.Row.This.id + " .control-label").css('font-weight', "normal");
    $(DivBd.Row.This).css("text-align", "left");  
    DivBd.Row.BoderBottom = "1px solid rgba(0, 0, 0, 0.08)";
    DivBd.Row.Padding = "2px";
    $(DivBd.Row.This).css("margin", "Auto"); 
    
}



