﻿ItHelpCenter.SD.Shared.Attached.frPermissions.prototype.InitializeComponent = function () {
    var _this = this.TParent();

   var ObjHtml = _this.ObjectHtml;
    ObjHtml.innerHTML = "";
    $(ObjHtml).css("padding", "20px 40px");

    var DivTitle = new TVclStackPanel(ObjHtml, "DivTitle", 1, [[12], [12], [12], [12], [12]]);
    DivTitleC = DivTitle.Column[0].This;
    $(DivTitleC).css("background-color", "#227446");
    $(DivTitleC).css("color", "#FFF");
    $(DivTitleC).css("font-weight", "bold");
    $(DivTitleC).css("padding-top", "5px");

    var LabHeader1 = new Vcllabel(DivTitleC, "LabHeader1", TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1"));

    var DivHeader = new TVclStackPanel(ObjHtml, "DivHeader", 3, [[6, 3, 3], [6, 3, 3], [6, 3, 3], [6, 3, 3], [6, 3, 3]]);
    DivHeaderC1 = DivHeader.Column[0].This;
    DivHeaderC2 = DivHeader.Column[1].This;
    DivHeaderC3 = DivHeader.Column[2].This;
    $(DivHeader.Row.This).css("background-color", "#c4c4c4");
    $(DivHeader.Row.This).css("font-weight", "bold");

    $(DivHeaderC1).css("border", "1px solid #c4c4c4");
    $(DivHeaderC2).css("border", "1px solid #c4c4c4");
    $(DivHeaderC3).css("border", "1px solid #c4c4c4");
    $(DivHeaderC2).css("text-align", "center");
    $(DivHeaderC3).css("text-align", "center");
    $(DivHeaderC1).css("padding-top", "5px");
    $(DivHeaderC2).css("padding-top", "5px");
    $(DivHeaderC3).css("padding-top", "5px");


    var LabHeader1 = new Vcllabel(DivHeaderC1, "LabHeader1", TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2"));
    var LabHeader2 = new Vcllabel(DivHeaderC2, "LabHeader2", TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3"));
    var LabHeader3 = new Vcllabel(DivHeaderC3, "LabHeader3", TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines4"));


    for (var i = 0; i < _this.RWXList.length; i++) {
        _this.NewRow(_this, i);
    }


    var DivBtn = new TVclStackPanel(ObjHtml, "DivBtn", 1, [[12], [12], [12], [12], [12]]);
    DivBtnC = DivBtn.Column[0].This;
    $(DivBtnC).css("border", "1px solid #c4c4c4");
    $(DivBtnC).css("text-align", "center");
    $(DivBtnC).css("padding-top", "5px");
    $(DivBtnC).css("padding-bottom", "5px");

    var btnAccept = new TVclButton(DivBtnC, "btnAccept");
    btnAccept.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnAccept.Text");
    btnAccept.VCLType = TVCLType.BS;
    btnAccept.Src = "image/24/success.png";
    btnAccept.onClick = function myfunction() {
        _this.btnAcceptonClick(_this, btnAccept);
    }

    _this.Initialize();

}

ItHelpCenter.SD.Shared.Attached.frPermissions.prototype.NewRow = function (_this, index) {
    try {
        var ObjHtml = _this.ObjectHtml;

        var DivUser = new TVclStackPanel(ObjHtml, "DivUser" + i, 3, [[6, 3, 3], [6, 3, 3], [6, 3, 3], [6, 3, 3], [6, 3, 3]]);
        DivUserC1 = DivUser.Column[0].This;
        DivUserC2 = DivUser.Column[1].This;
        DivUserC3 = DivUser.Column[2].This;
        $(DivUser.Row.This).css("border", "1px solid #c4c4c4");
        $(DivUserC1).css("border-left", "1px solid #c4c4c4");
        $(DivUserC2).css("border-left", "1px solid #c4c4c4");
        $(DivUserC3).css("border-left", "1px solid #c4c4c4");
        $(DivUserC1).css("padding-top", "5px");
        $(DivUserC2).css("padding-top", "5px");
        $(DivUserC3).css("padding-top", "5px");
        $(DivUserC2).css("padding-bottom", "4px");
        $(DivUserC3).css("padding-bottom", "4px");

        $(DivUserC2).css("text-align", "center");
        $(DivUserC3).css("text-align", "center");

        // var Username = UsrCfg.SD.Properties.TSDTypeUser.GetEnum(_this.RWXList[index].IDSDTYPEUSER).name);
        var Username = Persistence.Catalog.Methods.SDTYPEUSER_ListSetID(Persistence.Profiler.CatalogProfiler.SDTYPEUSERList, _this.RWXList[index].IDSDTYPEUSER).TYPEUSERNAME;

        var LabUser = new Vcllabel(DivUserC1, "LabUser" + i, TVCLType.BS, TlabelType.H0, Username);


        var chkCheckBoxRead = new TVclInputcheckbox(DivUserC2, "chkCheckBoxRead");
        if (_this.RWXList[index].R_READ == 1) {
            chkCheckBoxRead.Checked = true;
        } else {
            chkCheckBoxRead.Checked = false;
        }
        chkCheckBoxRead.Tag = index;
        chkCheckBoxRead.VCLType = TVCLType.BS;
        chkCheckBoxRead.OnCheckedChange = function () {
            _this.chkCheckBoxReadOnCheckedChange(_this, chkCheckBoxRead, chkCheckBoxWrite, chkCheckBoxRead.Tag);
        }


        var chkCheckBoxWrite = new TVclInputcheckbox(DivUserC3, "chkCheckBoxWrite");
        if (_this.RWXList[index].W_READ == 1) {
            chkCheckBoxWrite.Checked = true;
        } else {
            chkCheckBoxWrite.Checked = false;
        }
        chkCheckBoxWrite.Tag = index;
        chkCheckBoxWrite.VCLType = TVCLType.BS
        chkCheckBoxWrite.OnCheckedChange = function () {
            //var _thisChek = this;
            _this.chkCheckBoxWriteOnCheckedChange(_this, chkCheckBoxWrite, chkCheckBoxRead, chkCheckBoxWrite.Tag);
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.Shared.Attached.frPermissions.prototype.NewRow ", e);
    }

}
