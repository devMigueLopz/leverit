﻿ItHelpCenter.SD.Shared.Attached.UfrSDAttached.prototype.InitializeComponent = function () {
    try {
        var _this = this.TParent();
        var ObjHtml = _this.ObjectHtml;
        ObjHtml.innerHTML = "";    

  
        if (SysCfg.App.Properties.Device.value != SysCfg.App.TDevice.Desktop.value) {
            $(ObjHtml).css("padding", "0px 3px"); 
        }       

        var DivPermissions = new TVclStackPanel(ObjHtml, "DivPermissions" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
        DivPermissionsC = DivPermissions.Column[0].This;
        $(DivPermissionsC).css("display", "none"); 
        $(DivPermissions.Row.This).css("padding", "0px");

        var Container = new TVclStackPanel(ObjHtml, "Container_UfrSDAttached" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
        divCont = Container.Column[0].This;
        $(Container.Row.This).css("margin", "auto");
        $(Container.Row.This).css("padding", "0px");         
        
        //Div boton + 
        var div1 = new TVclStackPanel(divCont, "div1" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
        _this.div1Cont = div1.Column[0].This;  
        if (SysCfg.App.Properties.Device.value == SysCfg.App.TDevice.Desktop.value) {
            $(_this.div1Cont).addClass("StackPanelContainer");
            $(_this.div1Cont).css("padding-bottom", "10px");
            $(div1.Row.This).css("padding-bottom", "0px");
        }

        //Header del Grid
        var div1_Hd = new TVclStackPanel(divCont, "Attach_Hd" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
        _this.div1_HdCont = div1_Hd.Column[0].This;        
        $(_this.div1_HdCont).css("margin-top", "0px");
        $(div1_Hd.Row.This).css("padding-bottom", "0px");
        $(div1_Hd.Row.This).css("padding-top", "0px");
        $(div1_Hd.Row.This).css("margin-top", "0px");
        if (SysCfg.App.Properties.Device.value == SysCfg.App.TDevice.Desktop.value) {
            $(_this.div1_HdCont).addClass("StackPanelContainer");
        }

        //Body del Grid
        var div1_Bd = new TVclStackPanel(divCont, "Attach_Bd" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
        _this.div1_BdCont = div1_Bd.Column[0].This;
        $(div1_Bd.Row.This).css("padding-top", "0px");       
        $(_this.div1_BdCont).css("margin-top", "0px");
        if (SysCfg.App.Properties.Device.value == SysCfg.App.TDevice.Desktop.value) {
            $(_this.div1_BdCont).addClass("StackPanelContainer");
        }

        _this.Initialize();
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("UfrSDAttached.js ItHelpCenter.SD.Shared.Attached.UfrSDAttached.prototype.InitializeComponent", e);
    }
}


//BUILD HEADER GRID
ItHelpCenter.SD.Shared.Attached.UfrSDAttached.prototype.HeaderGrid = function (Object, ID) {
    var _this = this.TParent();

    $(Object).css('padding-left', "0px");

    try {  
        if (_this.OnlyUp) {
            StackPanelDef = new Array(1);
            StackPanelDef[0] = new Array(5);
            var StackPanel = VclDivitions(Object, "GridAttached_Hd_" + ID, StackPanelDef);
            BootStrapCreateRow(StackPanel[0].This);
            BootStrapCreateColumns(StackPanel[0].Child, [[5, 5, 1, 1, 0], [4, 6, 1, 1, 0], [4, 6, 1, 1, 0], [4, 6, 1, 1, 0], [4, 6, 1, 1, 0]]);
        } else {
            if (_this.IDSDTYPEUSER == 4) {
                StackPanelDef = new Array(1);
                StackPanelDef[0] = new Array(5);
                var StackPanel = VclDivitions(Object, "GridAttached_Hd_" + ID, StackPanelDef);
                BootStrapCreateRow(StackPanel[0].This);
                BootStrapCreateColumns(StackPanel[0].Child, [[5, 6, 0, 1, 0], [4, 6, 0, 1, 0], [5, 6, 0, 1, 0], [5, 6, 0, 1, 0], [5, 6, 0, 1, 0]]);
            } else {
                StackPanelDef = new Array(1);
                StackPanelDef[0] = new Array(6);
                var StackPanel = VclDivitions(Object, "GridAttached_Hd_" + ID, StackPanelDef);
                BootStrapCreateRow(StackPanel[0].This);
                BootStrapCreateColumns(StackPanel[0].Child, [[5, 7, 3, 3, 3, 3], [4, 4, 1, 1, 1, 1], [4, 4, 1, 1, 1, 1], [4, 4, 1, 1, 1, 1], [4, 4, 1, 1, 1, 1]]);
            }
            
        }
        

        var GridAttached_Hd1 = new Vcllabel(StackPanel[0].Child[0].This, "GridAttached_Hd_" + ID + "1", TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "headerFileName"));
        var GridAttached_Hd2 = new Vcllabel(StackPanel[0].Child[1].This, "GridAttached_Hd_" + ID + "2", TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "headerDescription"));
         
        _this.DivHdEdit = StackPanel[0].Child[2].This;

        $("#" + StackPanel[0].This.id + " .control-label").css('font-weight', "normal");
        
        $(StackPanel[0].Child[0].This).css('padding-left', "5px");
        $(StackPanel[0].Child[1].This).css('padding-left', "5px");


        $(StackPanel[0].Child[2].This).css("overflow", "hidden");
        $(StackPanel[0].Child[3].This).css("overflow", "hidden");
        $(StackPanel[0].Child[4].This).css("overflow", "hidden");

        $(StackPanel[0].Child[2].This).css("Padding-left", "0px");
        $(StackPanel[0].Child[3].This).css("Padding-left", "0px");
        $(StackPanel[0].Child[4].This).css("Padding-left", "0px");
        $(StackPanel[0].Child[2].This).css("Padding-right", "0px");
        $(StackPanel[0].Child[3].This).css("Padding-right", "0px");
        $(StackPanel[0].Child[4].This).css("Padding-right", "0px");

        $(StackPanel[0].This).css('width', "100%");
        $(StackPanel[0].This).css('margin', "0px");

          
        //$(Object).css("padding-left", "30px");
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("UfrSDAttached.js ItHelpCenter.SD.Shared.Attached.UfrSDAttached.prototype.HeaderGrid", e);
    }

    try {
        //BTN ADD
        $(_this.div1Cont).css("padding", "0px 0px 7px 5px");

        _this.div1Cont.innerHTML = "";
        var btnAdd = new TVclButton(_this.div1Cont, "btnAdd");
        btnAdd.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnAdd");
        btnAdd.VCLType = TVCLType.BS;
        btnAdd.Src = "image/24/add.png";
        btnAdd.Cursor = "pointer";
        btnAdd.onClick = function myfunction() {
            _this.btnAdd(_this, btnAdd, _this.div1_BdCont, _this.ID);
        }
        $(btnAdd.This).css("padding", "0px 10px");
        
         
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("UfrSDAttached.js ItHelpCenter.SD.Shared.Attached.UfrSDAttached.prototype.HeaderGrid", e);
    }

}

//BUILD NEW FILE GRID // 11 parameters
ItHelpCenter.SD.Shared.Attached.UfrSDAttached.prototype.NewGridFile = function
    (Object, IDx, inIDSDATTACHED, inFile, inDescript, inFILENAME,
    inFILEDATE, inIDFILE, inPERRMISSIONSWRITE, inSDTYPEUSER_RWXList, Exe) {

     
    var _this = this.TParent(); 
    $(Object).css("padding-left", "5px");
    $(Object).css("padding-right", "5px");
    $(Object).css("padding-bottom", "10px"); 
    $(Object).css("padding-top", "0px");     

    try {
        idx = 'row_' + IDx;
        _this.NFile = _this.NFile + 1;
 
      
        if (_this.OnlyUp) {
            var StackPanel = new TVclStackPanel(Object, "StackPanel" + _this.ID, 5, [[5, 5, 1, 1, 0], [4, 6, 1, 1, 0], [4, 6, 1, 1, 0], [4, 6, 1, 1, 0], [4, 6, 1, 1, 0]]);
        } else {
            if (_this.IDSDTYPEUSER == 4) {
                var StackPanel = new TVclStackPanel(Object, "StackPanel" + _this.ID, 5, [[5, 6, 0, 1, 0], [4, 6, 0, 1, 0], [5, 6, 0, 1, 0], [5, 6, 0, 1, 0], [5, 6, 0, 1, 0]]);
            } else {
                var StackPanel = new TVclStackPanel(Object, "StackPanel" + _this.ID, 6, [[5, 7, 3, 3, 3, 3], [4, 4, 1, 1, 1, 1], [4, 4, 1, 1, 1, 1], [4, 4, 1, 1, 1, 1], [4, 4, 1, 1, 1, 1]]);
            }            
        }
         
        try {
            $(StackPanel.Row.This).css("padding-bottom", "10px");
            $(StackPanel.Column[0].This).css("height", "33px");
            $(StackPanel.Column[1].This).css("height", "33px"); 
        } catch (e) {

        } 


        if (Exe == "Add") { //////////////////////////////////////////////////////////////////////////////////////// 
            var div1 = new TVclStackPanel(StackPanel.Column[0].This, "div1" + _this.ID, 2, [[9, 3], [9, 3], [10, 2], [10, 2], [11, 1]]);
            var divtextC = div1.Column[0].This;
            var divBtnC = div1.Column[1].This;
            $(divtextC).css("padding-right", "3px");
            $(divtextC).css("padding-left", "0px");
            $(divBtnC).addClass("Divtooltip");
            $(divBtnC).css("padding", "0px"); 
            $(divBtnC).css("padding-left", "3px"); 

            //txt Path file
            var ContactText1 = new Uinput(divtextC, TImputtype.text, "Con_Text2_" + idx, "", false);
            ContactText1.value = "";
            ContactText1.readOnly = true;
            $(ContactText1).css("width", "100%");
            $(ContactText1).addClass("TextFormat");  
             
            //BTN SEARCH            
            var btnSearch = new TVclButton(divBtnC, "btnAttSearch_" + _this.ID);
            btnSearch.Text = "...";
            btnSearch.VCLType = TVCLType.BS;
            btnSearch.onClick = function myfunction() {
                _this.btnSearch(_this, btnSearch, OpenFile, ContactText1, ContactText2, IDx);
            }
             
            //$(btnSearch.This).css("float", "left");
            $(btnSearch.This).css("padding", "1px 4px");
            $(btnSearch.This).css("margin-right", "7px"); 
            $(btnSearch.This).css("height", "33px");

            var LabBtnSearch = new Vcllabel(divBtnC, "LabBtnSearch" + ID + "1", TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnSearch"));
            $(LabBtnSearch).addClass("tooltiptext");
            $(LabBtnSearch).css("float", "right");

            var DOpen = new TVclStackPanel(StackPanel.Column[0].This, "" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
            var DivOpen = DOpen.Column[0].This;
            $(DivOpen).css("display", "none");
            var OpenFile = new TOpenFile(StackPanel.Column[0].This, "", true);
             

        } else { ///////////////////////////////////////////////////////////////////////////////////////////////   
            $(StackPanel.Column[0].This).css("padding-left", "0px");
            $(StackPanel.Column[0].This).css("padding-right", "10px");

            var ContactText1 = new Uinput(StackPanel.Column[0].This, TImputtype.text, "Con_Text2_" + idx, "", false);
            ContactText1.value = inFILENAME;
            $(ContactText1).css("width", "100%");
            ContactText1.readOnly = true;
            $(ContactText1).addClass("TextFormat");
        }//////////////////////////////////////////////////////////////////////////////////////////////////////////////

        ///descripcion 
        $(StackPanel.Column[1].This).css("padding", "0px");
        var ContactText2 = new TVclTextBox(StackPanel.Column[1].This, "Con_Text2_" + idx, "", false);
        ContactText2.This.value = inDescript
        ContactText2.This.readOnly = _this.OnlyRead;
        $(ContactText2.This).css("width", "98%");
        $(ContactText2.This).addClass("TextFormat");
        var descripcion = ContactText2.This.value;
        ContactText2.This.onkeypress = function validar(e) {
            _this.ContactTextonkeypress(_this, ContactText2.This, descripcion, StackPanel.Column[2].This, inPERRMISSIONSWRITE);
        } 
        

        //Arreglo de rows  // 0 File /1 path / 2 Description /3 Name  /4 Date / 
        _this.Table[IDx] = [[], [ContactText1], [ContactText2.This], [], []];
       
        
        $(StackPanel.Column[2].This).addClass("Divtooltip"); 
        if (Exe != "Add") {////////////////////////////////////////////////////////////////////////////////////
            //BTN EDIT
            var btnEdit = new TVclImagen(StackPanel.Column[2].This, "btnEdit_" + idx);
            btnEdit.Src = "image/24/Save-as.png";
            btnEdit.Cursor = "pointer"; 
            btnEdit.onClick = function myfunction() {
                //editar registro
                _this.btnEdit(_this, btnEdit, ContactText1, ContactText2,
                    StackPanel.Column[2].This, inIDSDATTACHED, inFILEDATE, inIDFILE, SDTYPEUSER_RWXList, IDx);
            }
            $(StackPanel.Column[2].This).css("text-align", "center");
            $(StackPanel.Column[2].This).css("padding-top", "4px");
                       

            var LabBtnEdit = new Vcllabel(StackPanel.Column[2].This, "LabBtnEdit" + ID + "1", TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnEdit"));
            $(LabBtnEdit).addClass("tooltiptext");
        } else {/////////////////////////////////////////////////////////////////////////////////////////////
            if (_this.IDSDCASE > 0) {
                //BTN SAVE
                var btnSave = new TVclImagen(StackPanel.Column[2].This, "btnEdit_" + idx);
                btnSave.Src = "image/24/Save.png";
                btnSave.Cursor = "pointer";
                btnSave.onClick = function myfunction() {
                    //editar registro
                    _this.btnSave(_this, btnSave, IDx);
                }
                $(StackPanel.Column[2].This).css("text-align", "center");
                $(StackPanel.Column[2].This).css("padding-top", "4px");

                var LabBtnSave = new Vcllabel(StackPanel.Column[2].This, "LabBtnSave" + ID + "1", TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnSave"));
                $(LabBtnSave).addClass("tooltiptext");
            } 
        }/////////////////////////////////////////////////////////////////////////////////////////////////////////

      
        $(StackPanel.Column[3].This).addClass("Divtooltip");
        //BTN DEL 
        var btnDel = new TVclImagen(StackPanel.Column[3].This, "btnDel_" + idx);
        btnDel.Src = "image/24/delete.png";
        btnDel.Cursor = "pointer";
        btnDel.onClick = function myfunction() {
            _this.btnDel(_this, btnDel, inIDSDATTACHED, StackPanel.Row.This, Object, IDx);
        }
        $(StackPanel.Column[3].This).css("text-align", "center");
        $(StackPanel.Column[3].This).css("padding", "0px"); 
        $(StackPanel.Column[3].This).css("padding-top", "4px");

        var LabBtnDel = new Vcllabel(StackPanel.Column[3].This, "LabBtnSearch" + ID + "1", TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnDel"));
        $(LabBtnDel).addClass("tooltiptext");
        

        if (!this.OnlyUp && Exe != "Add") {
            //BTN DOWNLOAD 
            $(StackPanel.Column[4].This).addClass("Divtooltip");
            var btnDownLoad = new TVclImagen(StackPanel.Column[4].This, "btnDownLoad" + idx);
            btnDownLoad.Src = "image/24/Download1.png";
            btnDownLoad.Cursor = "pointer";
            btnDownLoad.onClick = function myfunction() {
                _this.btnDownLoad(_this, btnDownLoad, inIDFILE, inFILENAME);
            }
            $(StackPanel.Column[4].This).css("text-align", "center");
            $(StackPanel.Column[4].This).css("padding-top", "4px");

            var LabBtnDown = new Vcllabel(StackPanel.Column[4].This, "LabBtnSearch" + ID + "1", TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnDown"));
            $(LabBtnDown).addClass("tooltiptext");
            
        }
 
        if (inPERRMISSIONSWRITE == 0) {        
            $(StackPanel.Column[2].This).css("display", "none"); //SAVE / EDIT
            $(StackPanel.Column[3].This).css("display", "none"); //DELE 
            ContactText2.readOnly = true;
        }

        if ((_this.IDSDTYPEUSER != 4) && (Exe != "Add") && (inPERRMISSIONSWRITE == 1)) {

            var SDTYPEUSER_RWXList = new Array()
            SDTYPEUSER_RWXList = inSDTYPEUSER_RWXList;

            //BTN KEY PERMISSIONS
            $(StackPanel.Column[5].This).addClass("Divtooltip");
            var btnKey = new TVclImagen(StackPanel.Column[5].This, "btnKey" + idx);
            btnKey.Src = "image/24/Key.png";
            btnKey.Cursor = "pointer";
            btnKey.onClick = function myfunction() {
                _this.btnKey(_this, btnKey, inIDSDATTACHED, SDTYPEUSER_RWXList);
            }
            $(StackPanel.Column[5].This).css("text-align", "center");
            $(StackPanel.Column[5].This).css("padding-top", "4px");

            var LabBtnKey = new Vcllabel(StackPanel.Column[5].This, "LabBtnKey" + ID + "1", TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnKey"));
            $(LabBtnKey).addClass("tooltiptext");
        }

        $(StackPanel.Row.This).css('margin', "0px");
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("UfrSDAttached.js ItHelpCenter.SD.Shared.Attached.UfrSDAttached.prototype.NewGridFile", e);
    }

}