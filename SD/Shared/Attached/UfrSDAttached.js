﻿
ItHelpCenter.SD.Shared.Attached.UfrSDAttached = function (inObject, inID, incallback, inIDSDCASE, inIDUSER, inIDSDTYPEUSER, inMT_IDMDSERVICETYPE, inOnlyUp, inIDSDTypeUserListAll) {
    this.ObjectHtml = inObject;
    this.Callback = incallback;
    this.ID = inID;
    this.NRows = 0; // numero de archivos adjutnados
    this.NRowsIni = 0;// numero de arhcivos iniciales
    this.NRowsFin = 0; // maximo numero de archivos en algun momento
    this.NFile = 0; //NUmero de campos para archivos, minimo debe existir 1
    this.MT_IDMDSERVICETYPE = inMT_IDMDSERVICETYPE;
    this.Table = new Array();
    this.OnlyUp = inOnlyUp;
    this.IDSDTypeUserListAll = inIDSDTypeUserListAll;

    this.IDSDCASE = inIDSDCASE;
    this.IDUSER = inIDUSER;

    this.IDSDTYPEUSER = inIDSDTYPEUSER;
    this.IDCode = null;
     

    this.TParent = function () {
        return this;
    }.bind(this);

    var _this = this.TParent();

    //TRADUCCION  
    this.Mythis = "frSDAttached";
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnAdd", "ADD");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "headerDel", "DEL.");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "headerDown", "DOWN.");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "headerEdit", "EDIT");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnDel", "DELETE");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnDown", "DOWNLOAD");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnEdit", "EDIT");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnSave", "Save Attach");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnSearch", "Open File");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BtnKey", "Permissions");    
    UsrCfg.Traslate.GetLangText(_this.Mythis, "headerFileName", "FILE");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "headerDescription", "DESCRIPTION");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "AlertEdit", "File Edit Successful");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lines1", "File Upload Successful"); 
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lines3", "Files Uploaded: ");    
    
    _this.InitializeComponent();
} 

  
 
ItHelpCenter.SD.Shared.Attached.UfrSDAttached.prototype.Initialize = function (Object, ID) {
    var _this = this.TParent();

    if (this.IDSDTYPEUSER < 1) {
        SysCfg.Log.Methods.WriteLog("No se ingreso por parametro IDSDTYPEUSER");
    }

    try {
        if (_this.LetLoad == false) {
            _this.HeaderGrid(_this.div1_HdCont, _this.ID);
            _this.NRowsFin = 1;
            _this.NRows = 0;
            _this.NRowsIni = 0;
            _this.NewGridFile(_this.div1_BdCont, 0, 0, "", "", "", "", "", 1, new Array(), "Add");
            //$(_this.div0Cont).css("display", "none");
        } else {
            _this.CargarDatosGridAttached();
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("UfrSDAttached.js ItHelpCenter.SD.Shared.Attached.UfrSDAttached.prototype.Initialize", e);
    }
}

ItHelpCenter.SD.Shared.Attached.UfrSDAttached.prototype.CargarDatosGridAttached = function (Dataset) {
    var _this = this.TParent();
     

    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.IDSDCASE.FieldName, _this.IDSDCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
         
        OpenAttached = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "SDCASE_ATTACHMENT_001", Param.ToBytes());
        if (OpenAttached.ResErr.NotError) {
            _this.OpentoTableView(OpenAttached.DataSet);
        }
        Param.Destroy();
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("UfrSDAttached.js ItHelpCenter.SD.Shared.Attached.UfrSDAttached.prototype.CargarDatosGridAttached", e);
    }
}

ItHelpCenter.SD.Shared.Attached.UfrSDAttached.prototype.OpentoTableView = function (Dataset) {
    var _this = this.TParent();

    try {
         
        $(_this.div1_BdCont).css("padding-top", "10px");

        _this.NRowsIni = Dataset.RecordCount;
        _this.NRowsFin = Dataset.RecordCount;
        _this.NRows = Dataset.RecordCount;


        //_this.div0Cont.innerHTML = "";
        _this.div1_HdCont.innerHTML = "";
        _this.div1_BdCont.innerHTML = "";

      
        _this.HeaderGrid(_this.div1_HdCont, _this.ID);
        if (Dataset.RecordCount > 0) {
            var NfilesPermit = 0;
            for (var i = 0; i < Dataset.RecordCount; i++) {
                var SDTYPEUSER_RWXList = new Array();

                try {
                    var PERRMISSIONSREAD = 0;
                    var PERRMISSIONSWRITE = 0;

                    //PROTOCOLO PERMISSIONS
                    var PERMISSIONS = Dataset.Records[i].Fields[6].Value;                   

                    UsrCfg.SD.TYPEUSER_RWX.Methods.StrtoSDTYPEUSER_RWX(PERMISSIONS, SDTYPEUSER_RWXList);
                    if (SDTYPEUSER_RWXList.length > 0) {
                        for (var j = 0; j < SDTYPEUSER_RWXList.length; j++) {
                            if (SDTYPEUSER_RWXList[j].IDSDTYPEUSER == _this.IDSDTYPEUSER) {
                                PERRMISSIONSREAD = SDTYPEUSER_RWXList[j].R_READ;
                                PERRMISSIONSWRITE = SDTYPEUSER_RWXList[j].W_READ;
                            }
                        }
                    }

                    //PROTOCOLO ATTACH          
                    var ATTACH = Dataset.Records[i].Fields[5].Value;
                    var lst2 = new Array();
                    Persistence.GP.Methods.StrtoFILESRV(ATTACH, lst2);
                    if (lst2.length > 0) {
                        var fileSrv = lst2[0];
                        var FILENAME = fileSrv.FILENAME;
                        var DATE = fileSrv.ATTACHNETDATE;
                        var DESCRIPTION = fileSrv.DESCRIPTION;
                    }  
                     

                    if (PERRMISSIONSREAD == 1) {
                        NfilesPermit = NfilesPermit + 1;
                        _this.NewGridFile(
                            _this.div1_BdCont,
                            i,
                            Dataset.Records[i].Fields[0].Value,
                            fileSrv,
                            DESCRIPTION,
                            FILENAME,
                            DATE,
                            fileSrv.IDFILE,
                            PERRMISSIONSWRITE,
                            SDTYPEUSER_RWXList,
                            "Load"
                        );
                    }
                    
                } catch (e) {
                    SysCfg.Log.Methods.WriteLog("UfrSDAttached.js ItHelpCenter.SD.Shared.Attached.UfrSDAttached.prototype.OpentoTableView", e);
                }
            }

            
        } else {
            var SDTYPEUSER_RWXList = new Array();
            if (_this.OnlyRead == false) {
                _this.NRowsFin = 1;
                _this.NRows = 0;
                _this.NRowsIni = 0;
                _this.NewGridFile(_this.div1_BdCont, 0, 0, "", "", "", "", "", 1, SDTYPEUSER_RWXList, "Add");
            }
        }
         
        $(_this.ObjectHtml).css('margin-top', "5px"); //reajustar margin del contenedor
        //$(_this.ObjectHtml).css('padding', "0px");
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("UfrSDAttached.js ItHelpCenter.SD.Shared.Attached.UfrSDAttached.prototype.OpentoTableView", e);
    }
    
}

ItHelpCenter.SD.Shared.Attached.UfrSDAttached.prototype.SaveAttach = function (inIDSDCASE, inCallback) {
    var _this = this.TParent();

    try {
        _this.IDSDCASE = inIDSDCASE;
        if (_this.IDSDCASE > 0) {
            //validar si hay nuevos registros 
            var NFilesUp = 0;
            if (_this.NRowsIni != _this.NRowsFin) {
                for (var i = (_this.NRowsIni) ; i < _this.Table.length; i++) {
                    if (_this.Table[i].length > 0) {
                        if (_this.Table[i][3] != "") {
                            //subir archivo   
                            GetSDfile = Comunic.Ashx.Client.Methods.GetSDfile(Comunic.Properties.TFileType._ServiceDesk, Comunic.Properties.TFileOperationType._FileWrite, "", _this.Table[i][3], _this.Table[i][0]);
                            IDCode = GetSDfile.IDCode;

                            //inIDCode, inDateFile, inNameFile, inDescripcionFile
                            _this.GuardarDatosAttached(IDCode, _this.Table[i][4],
                                _this.Table[i][3], _this.Table[i][2][0].value, false);

                            NFilesUp = NFilesUp + 1;
                        } else {
                            _this.NRows = _this.NRows - 1;
                        }
                    }
                }
            } 

            _this.ObjectHtml.innerHTML = "";
            inCallback(NFilesUp);
        } else {
            SysCfg.Log.Methods.WriteLog("UfrSDAttached.js ItHelpCenter.SD.Shared.Attached.UfrSDAttached.prototype.SaveAttach IDCASE < 1");
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("UfrSDAttached.js ItHelpCenter.SD.Shared.Attached.UfrSDAttached.prototype.SaveAttach", e);
    }

}



ItHelpCenter.SD.Shared.Attached.UfrSDAttached.prototype.GuardarDatosAttached = function (inIDCode, inDateFile, inNameFile, inDescripcionFile, Load) {
    var _this = this.TParent();
    
    //PROTOCOLO PARA ATTACH
    var fileSrv = new Persistence.GP.Properties.TFILESRV();
    fileSrv.FILENAME = inNameFile;
    fileSrv.IDFILE = inIDCode;
    fileSrv.ATTACHNETDATE = inDateFile;
    fileSrv.DESCRIPTION = inDescripcionFile;
    var lista = new Array();
    lista.push(fileSrv);
    var protocolo = Persistence.GP.Methods.FILESRVListtoStr(lista);


    //PROTOCOLO PERMISSIONS
    var SDTYPEUSER_RWXList = Array();   
    UsrCfg.SD.TYPEUSER_RWX.Methods.GetSDTYPEUSER_RWXListDefult(_this.IDSDTYPEUSER, SDTYPEUSER_RWXList, _this.IDSDTypeUserListAll);

    var Permissions = UsrCfg.SD.TYPEUSER_RWX.Methods.SDTYPEUSER_RWXListtoStr(SDTYPEUSER_RWXList);


    /////////////
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.IDSDCASE.FieldName, _this.IDSDCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Param.AddString(UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.FILENAME.FieldName, inNameFile, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Param.AddString(UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.IDFILE.FieldName, inIDCode, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Param.AddDateTime(UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.ATTACHMENTDATE.FieldName, SysCfg.DB.Properties.SVRNOW(), SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Param.AddString(UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.DESCRIPTION.FieldName, inDescripcionFile[0].value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.IDCMDBCI.FieldName, _this.IDUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal); // LOGIN
        Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.IDSDTYPEUSER.FieldName, _this.IDSDTYPEUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal); // Owner, ...
        Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.IDSYSTEMSTATUS.FieldName, 1, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.ATTACH.FieldName, protocolo, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.PERMISSIONS.FieldName, Permissions, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "SDCASE_ATTACHMENT_002", Param.ToBytes());
        if (!Exec.ResErr.NotError) {
            alert(Exec.ResErr.Mesaje);
        }
        //Persistence.Notify.Methods.GetNotify_ADD_byTypeEvent(5, _this.MT_IDMDSERVICETYPE, _this.IDSDCASE);

        if (Load = true) {
            _this.CargarDatosGridAttached();
        }
        Param.Destroy();
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("UfrSDAttached.js ItHelpCenter.SD.Shared.Attached.UfrSDAttached.prototype.GuardarDatosAttached", e);
    }
    finally {
        Param.Destroy();
    }
     
    
}


ItHelpCenter.SD.Shared.Attached.UfrSDAttached.prototype.ContactTextonkeypress = function (sender, e, descripcion, DivObj, inPERRMISSIONSWRITE) {
    _this = sender;
 
    //    if ((inPERRMISSIONSWRITE == 1)) {
    //    if (descripcion != "") {
    //        if (e.value != descripcion) {
    //            $(DivObj).css("display", "");
    //        } else {
    //            $(DivObj).css("display", "none");

    //        }
    //    }
    //}
    
}


/////// BOTONES
ItHelpCenter.SD.Shared.Attached.UfrSDAttached.prototype.btnSearch = function (sender, e, OpenFile, ContactText1, ContactText2, IDx) {
    var _this = sender;
    try {
        OpenFile.SetFile(function (sender, e) {
            if (sender.Path != "") {
                ContactText1.value = sender.Path;
                ContactText2.value = "";
                _this.Table[IDx][0] = sender.File//sender.Bytes;
                _this.Table[IDx][3] = sender.NameFile;

                var today = new Date();
                var Fecha = today.getDate() + "/" + (today.getMonth()+1) + "/" + today.getFullYear();

                _this.Table[IDx][4] = Fecha;
                _this.NRows = _this.NRows + 1;
            }
        })
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("UfrSDAttached.js ItHelpCenter.SD.Shared.Attached.UfrSDAttached.prototype.btnSearch", e);
    }
}

ItHelpCenter.SD.Shared.Attached.UfrSDAttached.prototype.btnSave = function (sender, e, IDx) {
    _this = sender;
    try { 
        if (_this.IDSDCASE > 0) {  
            if (_this.Table[IDx][3] != "") {
                //subir archivo   
                GetSDfile = Comunic.Ashx.Client.Methods.GetSDfile(Comunic.Properties.TFileType._ServiceDesk, Comunic.Properties.TFileOperationType._FileWrite, "", _this.Table[IDx][3], _this.Table[IDx][0]);
                IDCode = GetSDfile.IDCode;

                //inIDCode, inDateFile, inNameFile, inDescripcionFile
                _this.GuardarDatosAttached(IDCode, _this.Table[IDx][4],
                    _this.Table[IDx][3], _this.Table[IDx][2][0].value, true);
            }
           
        } else {
            SysCfg.Log.Methods.WriteLog("UfrSDAttached.js ItHelpCenter.SD.Shared.Attached.UfrSDAttached.prototype.btnSave IDCASE < 1");
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("UfrSDAttached.js ItHelpCenter.SD.Shared.Attached.UfrSDAttached.prototype.btnSave", e);
    }
}

ItHelpCenter.SD.Shared.Attached.UfrSDAttached.prototype.btnEdit = function (sender, e, ContactText1, ContactText2, DivEdit, inIDSDATTACHED, inFILEDATE, inIDFILE) {
    var _this = sender;
    try {
        if (_this.IDSDCASE > 0) {   

            //PROTOCOLO ATTACH
            var fileSrv = new Persistence.GP.Properties.TFILESRV();
            fileSrv.FILENAME = ContactText1.value;
            fileSrv.IDFILE = inIDFILE;
            fileSrv.ATTACHNETDATE = inFILEDATE;
            fileSrv.DESCRIPTION = ContactText2.This.value;

            var lista = new Array();
            lista.push(fileSrv);
            var protocolo = Persistence.GP.Methods.FILESRVListtoStr(lista);


            var Param = new SysCfg.Stream.Properties.TParam();
            try {
                Param.Inicialize();
                Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.IDSDCASE_ATTACHMENT.FieldName, inIDSDATTACHED, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                Param.AddString(UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.ATTACH.FieldName, protocolo, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);                 
                Open = SysCfg.DB.SQL.Methods.Open("Atis", "SDCASE_ATTACHMENT_004", Param.ToBytes());
                if (Open.ResErr.NotError) {
                    _this.CargarDatosGridAttached();
                }
                else {
                    alert(Open.ResErr.Mesaje);
                }
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("UfrSDAttached.js ItHelpCenter.SD.Shared.Attached.UfrSDAttached.prototype.btnEdit", e);
            }
            finally {
                Param.Destroy();
            }            
           
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("UfrSDAttached.js ItHelpCenter.SD.Shared.Attached.UfrSDAttached.prototype.btnEdit", e);
    }
}

ItHelpCenter.SD.Shared.Attached.UfrSDAttached.prototype.btnAdd = function (sender, e, Object, ID) {
    var _this = sender;
    try {
        _this.NRowsFin = _this.NRowsFin + 1;
        _this.NewGridFile(Object, (_this.NRowsFin - 1), 0, "", "", "", "", "", 1,  new Array(), "Add");
        //_this.div0Cont.innerHTML = "";
        //var LaTitle = new Vcllabel(_this.div0Cont, "LaTitle" + ID, TVCLType.BS, TlabelType.H0, _this.NRows + UsrCfg.Traslate.GetLangText(_this.Mythis, "lines2"));
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("UfrSDAttached.js ItHelpCenter.SD.Shared.Attached.UfrSDAttached.prototype.btnAdd", e);
    }
}

ItHelpCenter.SD.Shared.Attached.UfrSDAttached.prototype.btnDel = function (sender, e, inIDSDATTACHED, inDIV, Object, IDx) {
    var _this = sender;

    try {
        if (_this.Table[IDx][3] != "") {
            _this.NRows = _this.NRows - 1;
        }

        inDIV.innerHTML = "";
        $(inDIV).css("display", "none");
        _this.Table[IDx] = [];

        _this.NFile = _this.NFile - 1;
        if (_this.NFile < 1) {
            _this.NRowsFin = 1;
            _this.NRows = 0;
            _this.NRowsIni = 0;
            _this.NewGridFile(Object, 0, 0, "", "", "", "", "", 1,  new Array(), "Add");
        }

        if (_this.IDSDCASE > 0 && inIDSDATTACHED > 0) {
            var Param = new SysCfg.Stream.Properties.TParam();
            try {
                Param.Inicialize();
                Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.IDSDCASE_ATTACHMENT.FieldName, inIDSDATTACHED, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                var Open = SysCfg.DB.SQL.Methods.Open("Atis", "SDCASE_ATTACHMENT_003", Param.ToBytes());
                if (Open.ResErr.NotError) {
                   // _this.CargarDatosGridAttached();
                }
                else {
                    alert(Open.ResErr.Mesaje);
                }
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("UfrSDAttached.js ItHelpCenter.SD.Shared.Attached.UfrSDAttached.prototype.btnDel", e);
            } finally {
                Param.Destroy();
            }

        }
        //_this.div0Cont.innerHTML = "";
        //var LaTitle = new Vcllabel(_this.div0Cont, "LaTitle" + ID, TVCLType.BS, TlabelType.H0, _this.NRows + UsrCfg.Traslate.GetLangText(_this.Mythis, "lines2"));
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("UfrSDAttached.js ItHelpCenter.SD.Shared.Attached.UfrSDAttached.prototype.btnDel", e);
    }
}

ItHelpCenter.SD.Shared.Attached.UfrSDAttached.prototype.btnDownLoad = function (sender, e, inIDCOUDE, inFILENAME) {
    var _this = sender;
    try {
        GetSDfile = Comunic.Ashx.Client.Methods.GetSDfile(Comunic.Properties.TFileType._ServiceDesk, Comunic.Properties.TFileOperationType._FileRead, inIDCOUDE, inFILENAME, new Array());
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("UfrSDAttached.js ItHelpCenter.SD.Shared.Attached.UfrSDAttached.prototype.btnDownLoad", e);
    }
   
}

ItHelpCenter.SD.Shared.Attached.UfrSDAttached.prototype.btnKey = function (sender, e, inIDSDATTACHED, inSDTYPEUSER_RWXList) {
    var _this = sender;
    try {
        $(DivPermissionsC).css("display", "");
        $(divCont).css("display", "none");

        var Permisions = new ItHelpCenter.SD.Shared.Attached.frPermissions(DivPermissionsC, function (e) {
            $(DivPermissionsC).css("display", "none");
            $(divCont).css("display", "");

            //PROTOCOLO PERMISSIONS 
            var Permissions = UsrCfg.SD.TYPEUSER_RWX.Methods.SDTYPEUSER_RWXListtoStr(e);


            var Param = new SysCfg.Stream.Properties.TParam();
            try {
                Param.Inicialize();
                Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.IDSDCASE_ATTACHMENT.FieldName, inIDSDATTACHED, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);                 
                Param.AddString(UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.PERMISSIONS.FieldName, Permissions, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                Open = SysCfg.DB.SQL.Methods.Open("Atis", "SDCASE_ATTACHMENT_005", Param.ToBytes());
                if (Open.ResErr.NotError) {
                    _this.CargarDatosGridAttached();
                }
                else {
                    alert(Open.ResErr.Mesaje);
                }
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.Shared.Attached.UfrSDAttached.prototype.btnKey ", e);
            }
            finally {
                Param.Destroy();
            }
        }, inSDTYPEUSER_RWXList);
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("UfrSDAttached.js ItHelpCenter.SD.Shared.Attached.UfrSDAttached.prototype.btnKey", e);
    }

}


