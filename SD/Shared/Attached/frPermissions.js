﻿ItHelpCenter.SD.Shared.Attached.frPermissions = function (inObject, incallback, inRWXListDefult) {
    this.TParent = function () {
        return this;
    }.bind(this);

    this.ObjectHtml = inObject;
    this.Callback = incallback;
    this.RWXList = inRWXListDefult;



    var _this = this.TParent();

    //TRADUCCION  
    this.Mythis = "frPermissions";
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1", "Attached Permissions");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2", "User Type");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3", "Read");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines4", "Write");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnAccept.Text", "Accept");


    _this.InitializeComponent();

}

ItHelpCenter.SD.Shared.Attached.frPermissions.prototype.Initialize = function () {
    var _this = this.TParent();

    try {

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.Shared.Attached.frPermissions.prototype.Initialize ", e);
    }
}

ItHelpCenter.SD.Shared.Attached.frPermissions.prototype.chkCheckBoxReadOnCheckedChange = function (sender, e, chkBxWrite, index) {
    _this = sender;

    try {
        if (e.Checked) {
            _this.RWXList[index].R_READ = 1;
        } else {
            _this.RWXList[index].R_READ = 0;
            if (_this.RWXList[index].W_READ) {
                _this.RWXList[index].W_READ = 0;
                chkBxWrite.Checked = false;
            }
        }

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.Shared.Attached.frPermissions.prototype.chkCheckBoxReadOnCheckedChange  ", e);
    }
}

ItHelpCenter.SD.Shared.Attached.frPermissions.prototype.chkCheckBoxWriteOnCheckedChange = function (sender, e, chkBxRead, index) {
    _this = sender;

    try {
        if (e.Checked) {
            _this.RWXList[index].W_READ = 1;

            if (!_this.RWXList[index].R_READ) {
                _this.RWXList[index].R_READ = 1;
                chkBxRead.Checked = true;
            }            
        } else {
            _this.RWXList[index].W_READ = 0;
        }

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.Shared.Attached.frPermissions.prototype.chkCheckBoxWriteOnCheckedChange ", e);
    }

}

ItHelpCenter.SD.Shared.Attached.frPermissions.prototype.btnAcceptonClick = function (sender, e) {
    _this = sender;

    try {
        _this.Callback(_this.RWXList);
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.Shared.Attached.frPermissions.prototype.btnAcceptonClick ", e);
    }

}

