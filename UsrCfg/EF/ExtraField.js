﻿TExtrafields_ColumnStyle =
{
    None: 0,
    TextPassword: 1,
    Date: 2,
    Time: 3,
    ProgressBar: 4,
    LookUp: 5,
    LookUpOption: 6
}

TMDLIFESTATUSPERMISSION = {
    _Disable: 0,
    _Read: 1,
    _Write: 2
}

StateFormulario = {
    _None: 0,
    _Insert: 1,
    _update: 2,
    _Delete: 3,
    _Nuevo: 4,
    _Cancel: 5
}

TCMDBCIMP_TYPE = {
    MODE_NORMAL: 1,
    MODE_VIRTUAL: 2,
    MODE_INCOMING: 3
}

TDataType = {
    Unknown: 0,
    String: 1,
    Int32: 2,
    Boolean: 3,
    AutoInc: 4,
    Text: 5,
    Double: 6,
    DateTime: 7,
    Object: 8,
    Decimal: 9
}

TKeyType = {
    EXT: 0,
    ADN: 1,
    VAR: 2,
    NEW: 3,
    INS: 4,
    LOG: 5,
}