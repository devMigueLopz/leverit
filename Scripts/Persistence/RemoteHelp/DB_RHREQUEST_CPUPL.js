﻿//TreduceDB
//Persistence.RemoteHelp.Properties
//Persistence.RemoteHelp.Methods

Persistence.RemoteHelp.Properties.TRHREQUEST_CPUPL = function () {
    this.HOSTLIST = "";
    this.HOSTVALID = "";
    this.IDCPU = "";
    this.IDRHGROUPPL = 0;
    this.IDRHREQUEST = 0;
    this.IDRHREQUEST_CPUPL = 0;
    this.PERMISSION = ""; 

    this.REQUEST_CPUPL_STATUS = Persistence.RemoteHelp.Properties.TREQUEST_CPUPL_STATUS.GetEnum(undefined);  
    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.HOSTLIST);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.HOSTVALID);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.IDCPU);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDRHGROUPPL);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDRHREQUEST);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDRHREQUEST_CPUPL);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.PERMISSION);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, parseInt(this.REQUEST_CPUPL_STATUS.Value));
    }
    this.ByteTo = function (MemStream) {
        this.HOSTLIST = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.HOSTVALID = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDCPU = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDRHGROUPPL = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDRHREQUEST = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDRHREQUEST_CPUPL = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.PERMISSION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.REQUEST_CPUPL_STATUS = Persistence.RemoteHelp.Properties.TREQUEST_CPUPL_STATUS.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteStr(this.HOSTLIST, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.HOSTVALID, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.IDCPU, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDRHGROUPPL, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDRHREQUEST, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDRHREQUEST_CPUPL, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.PERMISSION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(parseInt(this.REQUEST_CPUPL_STATUS.value), Longitud, Texto);
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.HOSTLIST = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.HOSTVALID = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.IDCPU = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.IDRHGROUPPL = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDRHREQUEST = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDRHREQUEST_CPUPL = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.PERMISSION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.REQUEST_CPUPL_STATUS = Persistence.RemoteHelp.Properties.TREQUEST_CPUPL_STATUS.GetEnum(SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto));
    }

}
    

//**********************   METHODS server  ********************************************************************************************

Persistence.RemoteHelp.Methods.RHREQUEST_CPUPL_Fill = function (RHREQUEST_CPUPLList, StrIDRHREQUEST_CPUPL/* IDRHREQUEST_CPUPL*/) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    RHREQUEST_CPUPLList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDRHREQUEST_CPUPL == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.IDRHREQUEST_CPUPL.FieldName, " = " + UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.NAME_TABLE + "." + UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.IDRHREQUEST_CPUPL.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDRHREQUEST_CPUPL = " IN (" + StrIDRHREQUEST_CPUPL + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.IDRHREQUEST_CPUPL.FieldName, StrIDRHREQUEST_CPUPL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //Param.AddInt32(UsrCfg.InternoDataLinkNames.RHREQUEST_CPUPL.IDRHREQUEST_CPUPL.FieldName, IDRHREQUEST_CPUPL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {
        var DS_RHREQUEST_CPUPL = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "RHREQUEST_CPUPL_GET", Param.ToBytes());
        ResErr = DS_RHREQUEST_CPUPL.ResErr;
        if (ResErr.NotError) {
            if (DS_RHREQUEST_CPUPL.DataSet.RecordCount > 0) {
                DS_RHREQUEST_CPUPL.DataSet.First();
                while (!(DS_RHREQUEST_CPUPL.DataSet.Eof)) {
                    var RHREQUEST_CPUPL = new Persistence.RemoteHelp.Properties.TRHREQUEST_CPUPL();
                    RHREQUEST_CPUPL.IDRHREQUEST_CPUPL = DS_RHREQUEST_CPUPL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.IDRHREQUEST_CPUPL.FieldName).asInt32();
                    //Other
                    RHREQUEST_CPUPL.IDCPU = DS_RHREQUEST_CPUPL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.IDCPU.FieldName).asString();
                    RHREQUEST_CPUPL.IDRHGROUPPL = DS_RHREQUEST_CPUPL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.IDRHGROUPPL.FieldName).asInt32();
                    RHREQUEST_CPUPL.IDRHREQUEST = DS_RHREQUEST_CPUPL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.IDRHREQUEST.FieldName).asInt32();
                    RHREQUEST_CPUPL.PERMISSION = DS_RHREQUEST_CPUPL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.PERMISSION.FieldName).asString();
                    RHREQUEST_CPUPL.REQUEST_CPUPL_STATUS = DS_RHREQUEST_CPUPL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.REQUEST_CPUPL_STATUS.FieldName).asInt32();


                    var Agrega = true;
                    //if (DATALINK) {
                    //    Agrega = !UsrCfg.Properties.CloseOldRequests;
                    //}
                    if (Agrega) {
                        this.RHREQUEST_CPUPLList.push(RHREQUEST_CPUPL);
                    }
                    DS_RHREQUEST_CPUPL.DataSet.Next();
                }
            }
            else {
                DS_RHREQUEST_CPUPL.ResErr.NotError = false;
                DS_RHREQUEST_CPUPL.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHREQUEST_CPUPL_GETID = function (RHREQUEST_CPUPL) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddString(UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.IDCPU.FieldName, RHREQUEST_CPUPL.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.IDRHREQUEST.FieldName, RHREQUEST_CPUPL.IDRHREQUEST, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var DS_RHREQUEST_CPUPL = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "RHREQUEST_CPUPL_GETID", Param.ToBytes());
        ResErr = DS_RHREQUEST_CPUPL.ResErr;
        if (ResErr.NotError) {
            if (DS_RHREQUEST_CPUPL.DataSet.RecordCount > 0) {
                DS_RHREQUEST_CPUPL.DataSet.First();
                RHREQUEST_CPUPL.IDRHREQUEST_CPUPL = DS_RHREQUEST_CPUPL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.IDRHREQUEST_CPUPL.FieldName).asInt32();
            }
            else {
                DS_RHREQUEST_CPUPL.ResErr.NotError = false;
                DS_RHREQUEST_CPUPL.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHREQUEST_CPUPL_ADD = function (RHREQUEST_CPUPL) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.HOSTLIST.FieldName, RHREQUEST_CPUPL.HOSTLIST, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.HOSTVALID.FieldName, RHREQUEST_CPUPL.HOSTVALID, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.IDCPU.FieldName, RHREQUEST_CPUPL.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.IDRHGROUPPL.FieldName, RHREQUEST_CPUPL.IDRHGROUPPL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.IDRHREQUEST.FieldName, RHREQUEST_CPUPL.IDRHREQUEST, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.PERMISSION.FieldName, RHREQUEST_CPUPL.PERMISSION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.REQUEST_CPUPL_STATUS.FieldName, RHREQUEST_CPUPL.REQUEST_CPUPL_STATUS.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHREQUEST_CPUPL_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.RemoteHelp.Methods.RHREQUEST_CPUPL_GETID(RHREQUEST_CPUPL);
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHREQUEST_CPUPL_UPD = function (RHREQUEST_CPUPL) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.IDRHREQUEST_CPUPL.FieldName, RHREQUEST_CPUPL.IDRHREQUEST_CPUPL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.HOSTLIST.FieldName, RHREQUEST_CPUPL.HOSTLIST, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.HOSTVALID.FieldName, RHREQUEST_CPUPL.HOSTVALID, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.IDRHGROUPPL.FieldName, RHREQUEST_CPUPL.IDRHGROUPPL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.PERMISSION.FieldName, RHREQUEST_CPUPL.PERMISSION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.REQUEST_CPUPL_STATUS.FieldName, RHREQUEST_CPUPL.REQUEST_CPUPL_STATUS.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHREQUEST_CPUPL_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.RemoteHelp.Methods.RHREQUEST_CPUPL_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.RemoteHelp.Methods.RHREQUEST_CPUPL_DELIDRHREQUEST(/* StrIDRHREQUEST_CPUPL*/Param);
    }
    else {
        Res = Persistence.RemoteHelp.Methods.RHREQUEST_CPUPL_DELRHREQUEST_CPUPL(Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.RHREQUEST_CPUPL_DELIDRHREQUEST = function (/* StrIDRHREQUEST_CPUPL*/IDRHREQUEST) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDRHREQUEST_CPUPL == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.IDRHREQUEST_CPUPL.FieldName, " = " + UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.NAME_TABLE + "." + UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.IDRHREQUEST_CPUPL.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDRHREQUEST_CPUPL = " IN (" + StrIDRHREQUEST_CPUPL + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.IDRHREQUEST_CPUPL.FieldName, StrIDRHREQUEST_CPUPL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.IDRHREQUEST.FieldName, IDRHREQUEST, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHREQUEST_CPUPL_DEL", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHREQUEST_CPUPL_DELRHREQUEST_CPUPL = function (RHREQUEST_CPUPL) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.IDRHREQUEST.FieldName, RHREQUEST_CPUPL.IDRHREQUEST, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHREQUEST_CPUPL_DEL", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}


//**********************   METHODS   ********************************************************************************************

Persistence.RemoteHelp.Methods.RHREQUEST_CPUPL_ListSetID = function (RHREQUEST_CPUPLList, IDRHREQUEST_CPUPL) {
    for (var i = 0; i < RHREQUEST_CPUPLList.length; i++) {

        if (IDRHREQUEST_CPUPL == RHREQUEST_CPUPLList[i].IDRHREQUEST_CPUPL)
            return (RHREQUEST_CPUPLList[i]);

    }
    var UnRHREQUEST_CPUPL = new Persistence.RemoteHelp.Properties.TRHREQUEST_CPUPL();
    UnRHREQUEST_CPUPL.IDRHREQUEST_CPUPL = IDRHREQUEST_CPUPL;
    return (UnRHREQUEST_CPUPL);
}
Persistence.RemoteHelp.Methods.RHREQUEST_CPUPL_ListAdd = function (RHREQUEST_CPUPLList, RHREQUEST_CPUPL) {
    var i = Persistence.RemoteHelp.Methods.RHREQUEST_CPUPL_ListGetIndex(RHREQUEST_CPUPLList, RHREQUEST_CPUPL);
    if (i == -1) RHREQUEST_CPUPLList.push(RHREQUEST_CPUPL);
    else {
        RHREQUEST_CPUPLList[i].HOSTLIST = RHREQUEST_CPUPL.HOSTLIST;
        RHREQUEST_CPUPLList[i].HOSTVALID = RHREQUEST_CPUPL.HOSTVALID;
        RHREQUEST_CPUPLList[i].IDCPU = RHREQUEST_CPUPL.IDCPU;
        RHREQUEST_CPUPLList[i].IDRHGROUPPL = RHREQUEST_CPUPL.IDRHGROUPPL;
        RHREQUEST_CPUPLList[i].IDRHREQUEST = RHREQUEST_CPUPL.IDRHREQUEST;
        RHREQUEST_CPUPLList[i].IDRHREQUEST_CPUPL = RHREQUEST_CPUPL.IDRHREQUEST_CPUPL;
        RHREQUEST_CPUPLList[i].PERMISSION = RHREQUEST_CPUPL.PERMISSION;
        RHREQUEST_CPUPLList[i].REQUEST_CPUPL_STATUS = RHREQUEST_CPUPL.REQUEST_CPUPL_STATUS;
    }
}
Persistence.RemoteHelp.Methods.RHREQUEST_CPUPL_Copy = function (RHREQUEST_CPUPL_Source, RHREQUEST_CPUPL) {

    RHREQUEST_CPUPL.HOSTLIST = RHREQUEST_CPUPL_Source.HOSTLIST;
    RHREQUEST_CPUPL.HOSTVALID = RHREQUEST_CPUPL_Source.HOSTVALID;
    RHREQUEST_CPUPL.IDCPU = RHREQUEST_CPUPL_Source.IDCPU;
    RHREQUEST_CPUPL.IDRHGROUPPL = RHREQUEST_CPUPL_Source.IDRHGROUPPL;
    RHREQUEST_CPUPL.IDRHREQUEST = RHREQUEST_CPUPL_Source.IDRHREQUEST;
    RHREQUEST_CPUPL.IDRHREQUEST_CPUPL = RHREQUEST_CPUPL_Source.IDRHREQUEST_CPUPL;
    RHREQUEST_CPUPL.PERMISSION = RHREQUEST_CPUPL_Source.PERMISSION;
    RHREQUEST_CPUPL.REQUEST_CPUPL_STATUS = RHREQUEST_CPUPL_Source.REQUEST_CPUPL_STATUS;
}

//CJRC_26072018
Persistence.RemoteHelp.Methods.RHREQUEST_CPUPL_ListGetIndex = function (RHREQUEST_CPUPLList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.RemoteHelp.Methods.RHREQUEST_CPUPL_ListGetIndexIDRHREQUEST_CPUPL(RHREQUEST_CPUPLList, Param);

    }
    else {
        Res = Persistence.RemoteHelp.Methods.RHREQUEST_CPUPL_ListGetIndexRHREQUEST_CPUPL(RHREQUEST_CPUPLList, Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.RHREQUEST_CPUPL_ListGetIndexRHREQUEST_CPUPL = function (RHREQUEST_CPUPLList, RHREQUEST_CPUPL) {
    for (var i = 0; i < RHREQUEST_CPUPLList.length; i++) {
        if (RHREQUEST_CPUPL.IDRHREQUEST_CPUPL == RHREQUEST_CPUPLList[i].IDRHREQUEST_CPUPL)
            return (i);
    }
    return (-1);
}
Persistence.RemoteHelp.Methods.RHREQUEST_CPUPL_ListGetIndexIDRHREQUEST_CPUPL = function (RHREQUEST_CPUPLList, IDRHREQUEST_CPUPL) {
    for (var i = 0; i < RHREQUEST_CPUPLList.length; i++) {
        if (IDRHREQUEST_CPUPL == RHREQUEST_CPUPLList[i].IDRHREQUEST_CPUPL)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.RemoteHelp.Methods.RHREQUEST_CPUPLListtoStr = function (RHREQUEST_CPUPLList) {
    var Res = Persistence.RemoteHelp.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(RHREQUEST_CPUPLList.length, Longitud, Texto);
        for (Counter = 0; Counter < RHREQUEST_CPUPLList.length; Counter++) {
            var UnRHREQUEST_CPUPL = RHREQUEST_CPUPLList[Counter];
            UnRHREQUEST_CPUPL.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.RemoteHelp.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.RemoteHelp.Methods.StrtoRHREQUEST_CPUPL = function (ProtocoloStr, RHREQUEST_CPUPLList) {
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        RHREQUEST_CPUPLList.length = 0;
        if (Persistence.RemoteHelp.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnRHREQUEST_CPUPL = new Persistence.Demo.Properties.TRHREQUEST_CPUPL(); //Mode new row
                UnRHREQUEST_CPUPL.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.RHREQUEST_CPUPL_ListAdd(RHREQUEST_CPUPLList, UnRHREQUEST_CPUPL);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}

//Socket Function 
Persistence.RemoteHelp.Methods.RHREQUEST_CPUPLListToByte = function (RHREQUEST_CPUPLList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, RHREQUEST_CPUPLList.length - idx);
    for (var i = idx; i < RHREQUEST_CPUPLList.length; i++) {
        RHREQUEST_CPUPLList[i].ToByte(MemStream);
    }
}
Persistence.RemoteHelp.Methods.ByteToRHREQUEST_CPUPLList = function (RHREQUEST_CPUPLList, MemStream) {
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (var i = 0; i < Count; i++) {
        var UnRHREQUEST_CPUPL = new Persistence.RemoteHelp.Properties.TRHREQUEST_CPUPL();
        UnRHREQUEST_CPUPL.ByteTo(MemStream);
        Persistence.RemoteHelp.Methods.RHREQUEST_CPUPL_ListAdd(RHREQUEST_CPUPLList, UnRHREQUEST_CPUPL);
    }
}