﻿//TreduceDB
//Persistence.RemoteHelp.Properties
//Persistence.RemoteHelp.Methods

Persistence.RemoteHelp.Properties.TRHREQUEST_CPUCR = function () {
    this.HOSTLIST = "";
    this.HOSTVALID = ""; 
    this.IDCPU = "";
    this.IDRHGROUPCR = 0;
    this.IDRHGROUPCRPERMISSION =  Persistence.RemoteHelp.Properties.TRHGROUPCRPERMISSION_TYPE.GetEnum(undefined);  
    this.IDRHREQUEST = 0;
    this.IDRHREQUEST_CPUCR = 0;
    this.REQUEST_CPUCR_STATUS = Persistence.RemoteHelp.Properties.TREQUEST_CPUCR_STATUS.GetEnum(undefined);  

    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.HOSTLIST);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.HOSTVALID);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.IDCPU);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDRHGROUPCR);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, parseInt(this.IDRHGROUPCRPERMISSION.Value));
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDRHREQUEST);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDRHREQUEST_CPUCR);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, parseInt(this.REQUEST_CPUCR_STATUS.Value));
    }
    this.ByteTo = function (MemStream) {
        this.HOSTLIST = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.HOSTVALID = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDCPU = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDRHGROUPCR = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDRHGROUPCRPERMISSION = Persistence.RemoteHelp.Properties.TRHGROUPCRPERMISSION_TYPE.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));

        this.IDRHREQUEST = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDRHREQUEST_CPUCR = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.REQUEST_CPUCR_STATUS = Persistence.RemoteHelp.Properties.TREQUEST_CPUCR_STATUS.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteStr(this.HOSTLIST, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.HOSTVALID, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.IDCPU, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDRHGROUPCR, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(parseInt(this.IDRHGROUPCRPERMISSION.Value), Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDRHREQUEST, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDRHREQUEST_CPUCR, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(parseInt(this.REQUEST_CPUCR_STATUS.value), Longitud, Texto);
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.HOSTLIST = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.HOSTVALID = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.IDCPU = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.IDRHGROUPCR = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDRHGROUPCRPERMISSION = Persistence.RemoteHelp.Properties.TRHGROUPCRPERMISSION_TYPE.GetEnum(SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto));
        this.IDRHREQUEST = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDRHREQUEST_CPUCR = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.REQUEST_CPUCR_STATUS = Persistence.RemoteHelp.Properties.TREQUEST_CPUCR_STATUS.GetEnum(SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto));
    }

}

//**********************   METHODS server  ********************************************************************************************

Persistence.RemoteHelp.Methods.RHREQUEST_CPUCR_Fill = function (RHREQUEST_CPUCRList, StrIDRHREQUEST_CPUCR/*IDRHREQUEST_CPUCR*/) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    RHREQUEST_CPUCRList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDRHREQUEST_CPUCR == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDRHREQUEST_CPUCR.FieldName, " = " + UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.NAME_TABLE + "." + UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDRHREQUEST_CPUCR.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDRHREQUEST_CPUCR = " IN (" + StrIDRHREQUEST_CPUCR + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDRHREQUEST_CPUCR.FieldName, StrIDRHREQUEST_CPUCR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //Param.AddInt32(UsrCfg.InternoDataLinkNames.RHREQUEST_CPUCR.IDRHREQUEST_CPUCR.FieldName, IDRHREQUEST_CPUCR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {
        var DS_RHREQUEST_CPUCR = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "RHREQUEST_CPUCR_GET", Param.ToBytes());
        ResErr = DS_RHREQUEST_CPUCR.ResErr;
        if (ResErr.NotError) {
            if (DS_RHREQUEST_CPUCR.DataSet.RecordCount > 0) {
                DS_RHREQUEST_CPUCR.DataSet.First();
                while (!(DS_RHREQUEST_CPUCR.DataSet.Eof)) {
                    var RHREQUEST_CPUCR = new Persistence.RemoteHelp.Properties.TRHREQUEST_CPUCR();
                    RHREQUEST_CPUCR.IDRHREQUEST_CPUCR = DS_RHREQUEST_CPUCR.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDRHREQUEST_CPUCR.FieldName).asInt32();
                    //Other
                    RHREQUEST_CPUCR.HOSTLIST = DS_RHREQUEST_CPUCR.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.HOSTLIST.FieldName).asText();
                    RHREQUEST_CPUCR.HOSTVALID = DS_RHREQUEST_CPUCR.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.HOSTVALID.FieldName).asText();
                    RHREQUEST_CPUCR.IDCPU = DS_RHREQUEST_CPUCR.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDCPU.FieldName).asString();
                    RHREQUEST_CPUCR.IDRHGROUPCR = DS_RHREQUEST_CPUCR.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDRHGROUPCR.FieldName).asInt32();
                    RHREQUEST_CPUCR.IDRHGROUPCRPERMISSION = DS_RHREQUEST_CPUCR.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDRHGROUPCRPERMISSION.FieldName).asInt32();
                    RHREQUEST_CPUCR.IDRHREQUEST = DS_RHREQUEST_CPUCR.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDRHREQUEST.FieldName).asInt32();
                    RHREQUEST_CPUCR.REQUEST_CPUCR_STATUS = DS_RHREQUEST_CPUCR.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.REQUEST_CPUCR_STATUS.FieldName).asInt32();
                    var Agrega = true;
                    //if (DATALINK) { 
                    //    Agrega = !UsrCfg.Properties.CloseOldRequests;
                    //}
                    if (Agrega) {
                        this.RHREQUEST_CPUCRList.push(RHREQUEST_CPUCR);
                    }
                    

                    DS_RHREQUEST_CPUCR.DataSet.Next();
                }
            }
            else {
                DS_RHREQUEST_CPUCR.ResErr.NotError = false;
                DS_RHREQUEST_CPUCR.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHREQUEST_CPUCR_GETID = function (RHREQUEST_CPUCR) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddString(UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDCPU.FieldName, RHREQUEST_CPUCR.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDRHREQUEST.FieldName, RHREQUEST_CPUCR.IDRHREQUEST, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var DS_RHREQUEST_CPUCR = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "RHREQUEST_CPUCR_GETID", Param.ToBytes());
        ResErr = DS_RHREQUEST_CPUCR.ResErr;
        if (ResErr.NotError) {
            if (DS_RHREQUEST_CPUCR.DataSet.RecordCount > 0) {
                DS_RHREQUEST_CPUCR.DataSet.First();
                RHREQUEST_CPUCR.IDRHREQUEST_CPUCR = DS_RHREQUEST_CPUCR.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDRHREQUEST_CPUCR.FieldName).asInt32();
            }
            else {
                DS_RHREQUEST_CPUCR.ResErr.NotError = false;
                DS_RHREQUEST_CPUCR.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHREQUEST_CPUCR_ADD = function (RHREQUEST_CPUCR) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddText(UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.HOSTLIST.FieldName, RHREQUEST_CPUCR.HOSTLIST, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.HOSTVALID.FieldName, RHREQUEST_CPUCR.HOSTVALID, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDCPU.FieldName, RHREQUEST_CPUCR.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDRHGROUPCR.FieldName, RHREQUEST_CPUCR.IDRHGROUPCR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDRHGROUPCRPERMISSION.FieldName, RHREQUEST_CPUCR.IDRHGROUPCRPERMISSION.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDRHREQUEST.FieldName, RHREQUEST_CPUCR.IDRHREQUEST, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.REQUEST_CPUCR_STATUS.FieldName, RHREQUEST_CPUCR.REQUEST_CPUCR_STATUS.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHREQUEST_CPUCR_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.RemoteHelp.Methods.RHREQUEST_CPUCR_GETID(RHREQUEST_CPUCR);
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHREQUEST_CPUCR_UPD = function (RHREQUEST_CPUCR) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDRHREQUEST_CPUCR.FieldName, RHREQUEST_CPUCR.IDRHREQUEST_CPUCR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.HOSTLIST.FieldName, RHREQUEST_CPUCR.HOSTLIST, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.HOSTVALID.FieldName, RHREQUEST_CPUCR.HOSTVALID, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDRHGROUPCR.FieldName, RHREQUEST_CPUCR.IDRHGROUPCR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDRHGROUPCRPERMISSION.FieldName, RHREQUEST_CPUCR.IDRHGROUPCRPERMISSION.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.REQUEST_CPUCR_STATUS.FieldName,  RHREQUEST_CPUCR.REQUEST_CPUCR_STATUS.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHREQUEST_CPUCR_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.RemoteHelp.Methods.RHREQUEST_CPUCR_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.RemoteHelp.Methods.RHREQUEST_CPUCR_DELIDRHREQUEST(/*StrIDRHREQUEST_CPUCR*/Param);
    }
    else {
        Res = Persistence.RemoteHelp.Methods.RHREQUEST_CPUCR_DELRHREQUEST_CPUCR(Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.RHREQUEST_CPUCR_DELIDRHREQUEST = function (/*StrIDRHREQUEST_CPUCR*/IDRHREQUEST) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDRHREQUEST_CPUCR == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDRHREQUEST_CPUCR.FieldName, " = " + UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.NAME_TABLE + "." + UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDRHREQUEST_CPUCR.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDRHREQUEST_CPUCR = " IN (" + StrIDRHREQUEST_CPUCR + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDRHREQUEST_CPUCR.FieldName, StrIDRHREQUEST_CPUCR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDRHREQUEST.FieldName, IDRHREQUEST, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHREQUEST_CPUCR_DEL", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHREQUEST_CPUCR_DELRHREQUEST_CPUCR = function (RHREQUEST_CPUCR) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDRHREQUEST.FieldName, RHREQUEST_CPUCR.IDRHREQUEST, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHREQUEST_CPUCR_DEL", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}


//**********************   METHODS   ********************************************************************************************

Persistence.RemoteHelp.Methods.RHREQUEST_CPUCR_ListSetID = function (RHREQUEST_CPUCRList, IDRHREQUEST_CPUCR) {
    for (var i = 0; i < RHREQUEST_CPUCRList.length; i++) {

        if (IDRHREQUEST_CPUCR == RHREQUEST_CPUCRList[i].IDRHREQUEST_CPUCR)
            return (RHREQUEST_CPUCRList[i]);

    }
    var UnRHREQUEST_CPUCR = new Persistence.RemoteHelp.Properties.TRHREQUEST_CPUCR();
    UnRHREQUEST_CPUCR.IDRHREQUEST_CPUCR = IDRHREQUEST_CPUCR;
    return (UnRHREQUEST_CPUCR);
}
Persistence.RemoteHelp.Methods.RHREQUEST_CPUCR_ListAdd = function (RHREQUEST_CPUCRList, RHREQUEST_CPUCR) {
    var i = Persistence.RemoteHelp.Methods.RHREQUEST_CPUCR_ListGetIndex(RHREQUEST_CPUCRList, RHREQUEST_CPUCR);
    if (i == -1) RHREQUEST_CPUCRList.push(RHREQUEST_CPUCR);
    else {
        RHREQUEST_CPUCRList[i].HOSTLIST = RHREQUEST_CPUCR.HOSTLIST;
        RHREQUEST_CPUCRList[i].HOSTVALID = RHREQUEST_CPUCR.HOSTVALID;
        RHREQUEST_CPUCRList[i].IDCPU = RHREQUEST_CPUCR.IDCPU;
        RHREQUEST_CPUCRList[i].IDRHGROUPCR = RHREQUEST_CPUCR.IDRHGROUPCR;
        RHREQUEST_CPUCRList[i].IDRHGROUPCRPERMISSION = RHREQUEST_CPUCR.IDRHGROUPCRPERMISSION;
        RHREQUEST_CPUCRList[i].IDRHREQUEST = RHREQUEST_CPUCR.IDRHREQUEST;
        RHREQUEST_CPUCRList[i].IDRHREQUEST_CPUCR = RHREQUEST_CPUCR.IDRHREQUEST_CPUCR;
        RHREQUEST_CPUCRList[i].REQUEST_CPUCR_STATUS = RHREQUEST_CPUCR.REQUEST_CPUCR_STATUS;
    }
}

Persistence.RemoteHelp.Methods.RHREQUEST_CPUCR_Copy = function (RHREQUEST_CPUCR_Source, RHREQUEST_CPUCR) {
    RHREQUEST_CPUCR.HOSTLIST = RHREQUEST_CPUCR_Source.HOSTLIST;
    RHREQUEST_CPUCR.HOSTVALID = RHREQUEST_CPUCR_Source.HOSTVALID;
    RHREQUEST_CPUCR.IDCPU = RHREQUEST_CPUCR_Source.IDCPU;
    RHREQUEST_CPUCR.IDRHGROUPCR = RHREQUEST_CPUCR_Source.IDRHGROUPCR;
    RHREQUEST_CPUCR.IDRHGROUPCRPERMISSION = RHREQUEST_CPUCR_Source.IDRHGROUPCRPERMISSION;
    RHREQUEST_CPUCR.IDRHREQUEST = RHREQUEST_CPUCR_Source.IDRHREQUEST;
    RHREQUEST_CPUCR.IDRHREQUEST_CPUCR = RHREQUEST_CPUCR_Source.IDRHREQUEST_CPUCR;
    RHREQUEST_CPUCR.REQUEST_CPUCR_STATUS = RHREQUEST_CPUCR_Source.REQUEST_CPUCR_STATUS;
}

//CJRC_26072018
Persistence.RemoteHelp.Methods.RHREQUEST_CPUCR_ListGetIndex = function (RHREQUEST_CPUCRList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.RemoteHelp.Methods.RHREQUEST_CPUCR_ListGetIndexIDRHREQUEST_CPUCR(RHREQUEST_CPUCRList, Param);

    }
    else {
        Res = Persistence.RemoteHelp.Methods.RHREQUEST_CPUCR_ListGetIndexRHREQUEST_CPUCR(RHREQUEST_CPUCRList, Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.RHREQUEST_CPUCR_ListGetIndexRHREQUEST_CPUCR = function (RHREQUEST_CPUCRList, RHREQUEST_CPUCR) {
    for (var i = 0; i < RHREQUEST_CPUCRList.length; i++) {
        if (RHREQUEST_CPUCR.IDRHREQUEST_CPUCR == RHREQUEST_CPUCRList[i].IDRHREQUEST_CPUCR)
            return (i);
    }
    return (-1);
}
Persistence.RemoteHelp.Methods.RHREQUEST_CPUCR_ListGetIndexIDRHREQUEST_CPUCR = function (RHREQUEST_CPUCRList, IDRHREQUEST_CPUCR) {
    for (var i = 0; i < RHREQUEST_CPUCRList.length; i++) {
        if (IDRHREQUEST_CPUCR == RHREQUEST_CPUCRList[i].IDRHREQUEST_CPUCR)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.RemoteHelp.Methods.RHREQUEST_CPUCRListtoStr = function (RHREQUEST_CPUCRList) {
    var Res = Persistence.RemoteHelp.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(RHREQUEST_CPUCRList.length, Longitud, Texto);
        for (Counter = 0; Counter < RHREQUEST_CPUCRList.length; Counter++) {
            var UnRHREQUEST_CPUCR = RHREQUEST_CPUCRList[Counter];
            UnRHREQUEST_CPUCR.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.RemoteHelp.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.RemoteHelp.Methods.StrtoRHREQUEST_CPUCR = function (ProtocoloStr, RHREQUEST_CPUCRList) {
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        RHREQUEST_CPUCRList.length = 0;
        if (Persistence.RemoteHelp.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnRHREQUEST_CPUCR = new Persistence.Demo.Properties.TRHREQUEST_CPUCR(); //Mode new row
                UnRHREQUEST_CPUCR.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.RHREQUEST_CPUCR_ListAdd(RHREQUEST_CPUCRList, UnRHREQUEST_CPUCR);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}

//Socket Function 
Persistence.RemoteHelp.Methods.RHREQUEST_CPUCRListToByte = function (RHREQUEST_CPUCRList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, RHREQUEST_CPUCRList.length - idx);
    for (var i = idx; i < RHREQUEST_CPUCRList.length; i++) {
        RHREQUEST_CPUCRList[i].ToByte(MemStream);
    }
}
Persistence.RemoteHelp.Methods.ByteToRHREQUEST_CPUCRList = function (RHREQUEST_CPUCRList, MemStream) {
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (var i = 0; i < Count; i++) {
        var UnRHREQUEST_CPUCR = new Persistence.RemoteHelp.Properties.TRHREQUEST_CPUCR();
        UnRHREQUEST_CPUCR.ByteTo(MemStream);
        Persistence.RemoteHelp.Methods.RHREQUEST_CPUCR_ListAdd(RHREQUEST_CPUCRList, UnRHREQUEST_CPUCR);
    }
}

