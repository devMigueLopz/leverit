﻿//TreduceDB
//Persistence.RemoteHelp.Properties
//Persistence.RemoteHelp.Methods


Persistence.RemoteHelp.Properties.TRHGROUPCR = function () {
    this.IDRHGROUPCR = 0;
    this.RHGROUPCR_DESCRIPTION = "";
    this.RHGROUPCR_NAME = "";
    //Virtual
    this.RHGROUPCRATROLEPERMISSIONList = new Array(); //Persistence.RemoteHelp.Properties.TRHGROUPCRATROLEPERMISSION

    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDRHGROUPCR);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.RHGROUPCR_DESCRIPTION);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.RHGROUPCR_NAME);
    }
    this.ByteTo = function (MemStream) {
        this.IDRHGROUPCR = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.RHGROUPCR_DESCRIPTION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.RHGROUPCR_NAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDRHGROUPCR, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.RHGROUPCR_DESCRIPTION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.RHGROUPCR_NAME, Longitud, Texto);
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDRHGROUPCR = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.RHGROUPCR_DESCRIPTION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.RHGROUPCR_NAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
    }
}


//**********************   METHODS server  ********************************************************************************************

Persistence.RemoteHelp.Methods.RHGROUPCR_Fill = function (RHGROUPCRList) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    RHGROUPCRList.length = 0;
    try {
        var DS_RHGROUPCR = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "RHGROUPCR_GET", new Array());
        ResErr = DS_RHGROUPCR.ResErr;
        if (ResErr.NotError) {
            if (DS_RHGROUPCR.DataSet.RecordCount > 0) {
                DS_RHGROUPCR.DataSet.First();
                while (!(DS_RHGROUPCR.DataSet.Eof)) {
                    var RHGROUPCR = new Persistence.RemoteHelp.Properties.TRHGROUPCR();
                    RHGROUPCR.IDRHGROUPCR = DS_RHGROUPCR.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPCR.IDRHGROUPCR.FieldName).asInt32();
                    RHGROUPCR.RHGROUPCR_DESCRIPTION = DS_RHGROUPCR.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPCR.RHGROUPCR_DESCRIPTION.FieldName).asString();
                    RHGROUPCR.RHGROUPCR_NAME = DS_RHGROUPCR.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPCR.RHGROUPCR_NAME.FieldName).asString();
                    RHGROUPCRList.push(RHGROUPCR);

                    DS_RHGROUPCR.DataSet.Next();
                }
            }
            else {
                DS_RHGROUPCR.ResErr.NotError = false;
                DS_RHGROUPCR.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPCR_GETID = function (RHGROUPCR) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPCR.RHGROUPCR_DESCRIPTION.FieldName, RHGROUPCR.RHGROUPCR_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPCR.RHGROUPCR_NAME.FieldName, RHGROUPCR.RHGROUPCR_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var DS_RHGROUPCR = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "RHGROUPCR_GETID", Param.ToBytes());
        ResErr = DS_RHGROUPCR.ResErr;
        if (ResErr.NotError) {
            if (DS_RHGROUPCR.DataSet.RecordCount > 0) {
                DS_RHGROUPCR.DataSet.First();
                RHGROUPCR.IDRHGROUPCR = DS_RHGROUPCR.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPCR.IDRHGROUPCR.FieldName).asInt32();
                RHGROUPCR.RHGROUPCR_DESCRIPTION = DS_RHGROUPCR.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPCR.RHGROUPCR_DESCRIPTION.FieldName).asString();
                RHGROUPCR.RHGROUPCR_NAME = DS_RHGROUPCR.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPCR.RHGROUPCR_NAME.FieldName).asString();
            }
            else {
                DS_RHGROUPCR.ResErr.NotError = false;
                DS_RHGROUPCR.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPCR_GETBYID = function (RHGROUPCR, id) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPCR.IDRHGROUPCR.FieldName, id, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var DS_RHGROUPCR = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "RHGROUPCR_GETBYID", Param.ToBytes());
        ResErr = DS_RHGROUPCR.ResErr;
        if (ResErr.NotError) {
            if (DS_RHGROUPCR.DataSet.RecordCount > 0) {
                DS_RHGROUPCR.DataSet.First();
                RHGROUPCR.IDRHGROUPCR = DS_RHGROUPCR.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPCR.IDRHGROUPCR.FieldName).asInt32();
                RHGROUPCR.RHGROUPCR_DESCRIPTION = DS_RHGROUPCR.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPCR.RHGROUPCR_DESCRIPTION.FieldName).asString();
                RHGROUPCR.RHGROUPCR_NAME = DS_RHGROUPCR.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPCR.RHGROUPCR_NAME.FieldName).asString();
            }
            else {
                DS_RHGROUPCR.ResErr.NotError = false;
                DS_RHGROUPCR.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPCR_GETBYNAME = function (RHGROUPCR, name) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPCR.RHGROUPCR_NAME.FieldName, name, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var DS_RHGROUPCR = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "RHGROUPCR_GETBYNAME", Param.ToBytes());
        ResErr = DS_RHGROUPCR.ResErr;
        if (ResErr.NotError) {
            if (DS_RHGROUPCR.DataSet.RecordCount > 0) {
                DS_RHGROUPCR.DataSet.First();
                RHGROUPCR.IDRHGROUPCR = DS_RHGROUPCR.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPCR.IDRHGROUPCR.FieldName).asInt32();
                RHGROUPCR.RHGROUPCR_DESCRIPTION = DS_RHGROUPCR.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPCR.RHGROUPCR_DESCRIPTION.FieldName).asString();
                RHGROUPCR.RHGROUPCR_NAME = DS_RHGROUPCR.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPCR.RHGROUPCR_NAME.FieldName).asString();
            }
            else {
                DS_RHGROUPCR.ResErr.NotError = false;
                DS_RHGROUPCR.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.EXIST_RHGROUPCR = function (id, name) {
    var resp = 0;
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPCR.IDRHGROUPCR.FieldName, id, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPCR.RHGROUPCR_NAME.FieldName, name, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var DS_RHGROUPCR = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "EXIST_RHGROUPCR", Param.ToBytes());
        ResErr = DS_RHGROUPCR.ResErr;
        if (ResErr.NotError) {
            if (DS_RHGROUPCR.DataSet.RecordCount > 0) {
                DS_RHGROUPCR.DataSet.First();
                resp = parseInt(DS_RHGROUPCR.DataSet.RecordSet.FieldName("TOTAL").Value.toString()); parseInt
            }
            else {
                DS_RHGROUPCR.ResErr.NotError = false;
                DS_RHGROUPCR.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (resp);
}

Persistence.RemoteHelp.Methods.RHGROUPCR_ADD = function (RHGROUPCR) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPCR.RHGROUPCR_DESCRIPTION.FieldName, RHGROUPCR.RHGROUPCR_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPCR.RHGROUPCR_NAME.FieldName, RHGROUPCR.RHGROUPCR_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHGROUPCR_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.RemoteHelp.Methods.RHGROUPCR_GETID(RHGROUPCR);
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPCR_UPD = function (RHGROUPCR) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPCR.IDRHGROUPCR.FieldName, RHGROUPCR.IDRHGROUPCR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPCR.RHGROUPCR_DESCRIPTION.FieldName, RHGROUPCR.RHGROUPCR_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPCR.RHGROUPCR_NAME.FieldName, RHGROUPCR.RHGROUPCR_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHGROUPCR_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.RemoteHelp.Methods.RHGROUPCR_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.RemoteHelp.Methods.RHGROUPCR_DELIDRHGROUPCR(Param);
    }
    else {
        Res = Persistence.RemoteHelp.Methods.RHGROUPCR_DELRHGROUPCR(Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.RHGROUPCR_DELIDRHGROUPCR = function (IDRHGROUPCR) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPCR.IDRHGROUPCR.FieldName, IDRHGROUPCR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHGROUPCR_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPCR_DELRHGROUPCR = function (RHGROUPCR) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPCR.IDRHGROUPCR.FieldName, RHGROUPCR.IDRHGROUPCR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPCR.RHGROUPCR_DESCRIPTION.FieldName, RHGROUPCR.RHGROUPCR_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPCR.RHGROUPCR_NAME.FieldName, RHGROUPCR.RHGROUPCR_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHGROUPCR_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//**********************   METHODS   ********************************************************************************************

Persistence.RemoteHelp.Methods.RHGROUPCR_ListSetID = function (RHGROUPCRList, IDRHGROUPCR) {
    for (var i = 0; i < RHGROUPCRList.length; i++) {

        if (IDRHGROUPCR == RHGROUPCRList[i].IDRHGROUPCR)
            return (RHGROUPCRList[i]);

    }
    var UnRHGROUPCR = new Persistence.RemoteHelp.Properties.TRHGROUPCR();
    UnRHGROUPCR.IDRHGROUPCR = IDRHGROUPCR;
    return (UnRHGROUPCR);
}
Persistence.RemoteHelp.Methods.RHGROUPCR_ListAdd = function (RHGROUPCRList, RHGROUPCR) {
    var i = Persistence.RemoteHelp.Methods.RHGROUPCR_ListGetIndex(RHGROUPCRList, RHGROUPCR);
    if (i == -1) RHGROUPCRList.push(RHGROUPCR);
    else {
        RHGROUPCRList[i].IDRHGROUPCR = RHGROUPCR.IDRHGROUPCR;
        RHGROUPCRList[i].RHGROUPCR_DESCRIPTION = RHGROUPCR.RHGROUPCR_DESCRIPTION;
        RHGROUPCRList[i].RHGROUPCR_NAME = RHGROUPCR.RHGROUPCR_NAME;
    }
}

//CJRC_26072018
Persistence.RemoteHelp.Methods.RHGROUPCR_ListGetIndex = function (RHGROUPCRList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.RemoteHelp.Methods.RHGROUPCR_ListGetIndexIDRHGROUPCR(RHGROUPCRList, Param);

    }
    else {
        Res = Persistence.RemoteHelp.Methods.RHGROUPCR_ListGetIndexRHGROUPCR(RHGROUPCRList, Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.RHGROUPCR_ListGetIndexRHGROUPCR = function (RHGROUPCRList, RHGROUPCR) {
    for (var i = 0; i < RHGROUPCRList.length; i++) {
        if (RHGROUPCR.IDRHGROUPCR == RHGROUPCRList[i].IDRHGROUPCR)
            return (i);
    }
    return (-1);
}
Persistence.RemoteHelp.Methods.RHGROUPCR_ListGetIndexIDRHGROUPCR = function (RHGROUPCRList, IDRHGROUPCR) {
    for (var i = 0; i < RHGROUPCRList.length; i++)
    {
        if (IDRHGROUPCR == RHGROUPCRList[i].IDRHGROUPCR)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.RemoteHelp.Methods.RHGROUPCRListtoStr = function (RHGROUPCRList) {
    var Res = Persistence.RemoteHelp.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(RHGROUPCRList.length, Longitud, Texto);
        for (Counter = 0; Counter < RHGROUPCRList.length; Counter++) {
            var UnRHGROUPCR = RHGROUPCRList[Counter];
            UnRHGROUPCR.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.RemoteHelp.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.RemoteHelp.Methods.StrtoRHGROUPCR = function (ProtocoloStr, RHGROUPCRList) {
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        RHGROUPCRList.length = 0;
        if (Persistence.RemoteHelp.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnRHGROUPCR = new Persistence.Demo.Properties.TRHGROUPCR(); //Mode new row
                UnRHGROUPCR.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.RHGROUPCR_ListAdd(RHGROUPCRList, UnRHGROUPCR);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}

//Socket Function 
Persistence.RemoteHelp.Methods.RHGROUPCRListToByte = function (RHGROUPCRList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, RHGROUPCRList.length - idx);
    for (var i = idx; i < RHGROUPCRList.length; i++) {
        RHGROUPCRList[i].ToByte(MemStream);
    }
}
Persistence.RemoteHelp.Methods.ByteToRHGROUPCRList = function (RHGROUPCRList, MemStream) {
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (var i = 0; i < Count; i++) {
        UnRHGROUPCR = new Persistence.RemoteHelp.Properties.TRHGROUPCR();
        UnRHGROUPCR.ByteTo(MemStream);
        Persistence.RemoteHelp.Methods.RHGROUPCR_ListAdd(RHGROUPCRList, UnRHGROUPCR);
    }
}
