﻿//TreduceDB
//Persistence.RemoteHelp.Properties
//Persistence.RemoteHelp.Methods

Persistence.RemoteHelp.Properties.TRHGROUPCRCPU = function () {
    this.IDCPU = "";
    this.IDRHGROUPCR = 0;
    this.IDRHGROUPCRCPU = 0;

    this.NOMBRE_ESTACION = "";
    this.NOMBRE = "";
    this.APELLIDO = "";
    this.DIRECCION = "";
    //virtual 
    this.RHGROUPCR = new Persistence.RemoteHelp.Properties.TRHGROUPCR();
    //this.CPUList = new Array(); //Persistence.RemoteHelp.Properties.TCPU


    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.IDCPU);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDRHGROUPCR);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDRHGROUPCRCPU);
    }
    this.ByteTo = function (MemStream) {
        this.IDCPU = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDRHGROUPCR = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDRHGROUPCRCPU = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteStr(this.IDCPU, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDRHGROUPCR, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDRHGROUPCRCPU, Longitud, Texto);
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDCPU = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.IDRHGROUPCR = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDRHGROUPCRCPU = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
    }
}

//**********************   METHODS server  ********************************************************************************************

Persistence.RemoteHelp.Methods.RHGROUPCRCPU_Fill = function (RHGROUPCRCPUList, IDRHGROUPCRCPU) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    RHGROUPCRCPUList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPCRCPU.IDRHGROUPCRCPU.FieldName, IDRHGROUPCRCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {
        var DS_RHGROUPCRCPU = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "RHGROUPCRCPU_GET", Param.ToBytes());
        ResErr = DS_RHGROUPCRCPU.ResErr;
        if (ResErr.NotError) {
            if (DS_RHGROUPCRCPU.DataSet.RecordCount > 0) {
                DS_RHGROUPCRCPU.DataSet.First();
                while (!(DS_RHGROUPCRCPU.DataSet.Eof)) {
                    var RHGROUPCRCPU = new Persistence.RemoteHelp.Properties.TRHGROUPCRCPU();
                    RHGROUPCRCPU.IDRHGROUPCRCPU = DS_RHGROUPCRCPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPCRCPU.IDRHGROUPCRCPU.FieldName).asInt32();
                    RHGROUPCRCPU.IDCPU = DS_RHGROUPCRCPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPCRCPU.IDCPU.FieldName).asString();
                    RHGROUPCRCPU.IDRHGROUPCR = DS_RHGROUPCRCPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPCRCPU.IDRHGROUPCR.FieldName).asInt32();
                    RHGROUPCRCPUList.push(RHGROUPCRCPU);

                    DS_RHGROUPCRCPU.DataSet.Next();
                }
            }
            else {
                DS_RHGROUPCRCPU.ResErr.NotError = false;
                DS_RHGROUPCRCPU.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPCRCPU_GETBYIDGROUP = function (RHGROUPCRCPUList, IDRHGROUPCR) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    RHGROUPCRCPUList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPCRCPU.IDRHGROUPCR.FieldName, IDRHGROUPCR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {
        var DS_RHGROUPCRCPU = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "RHGROUPCRCPU_GETBYIDGROUP", Param.ToBytes());
        ResErr = DS_RHGROUPCRCPU.ResErr;
        if (ResErr.NotError) {
            if (DS_RHGROUPCRCPU.DataSet.RecordCount > 0) {
                DS_RHGROUPCRCPU.DataSet.First();
                while (!(DS_RHGROUPCRCPU.DataSet.Eof)) {
                    var RHGROUPCRCPU = new Persistence.RemoteHelp.Properties.TRHGROUPCRCPU();
                    RHGROUPCRCPU.IDRHGROUPCRCPU = DS_RHGROUPCRCPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPCRCPU.IDRHGROUPCRCPU.FieldName).asInt32();
                    RHGROUPCRCPU.IDCPU = DS_RHGROUPCRCPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPCRCPU.IDCPU.FieldName).asString();
                    RHGROUPCRCPU.IDRHGROUPCR = DS_RHGROUPCRCPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPCRCPU.IDRHGROUPCR.FieldName).asInt32();

                    RHGROUPCRCPU.NOMBRE_ESTACION = DS_RHGROUPCRCPU.DataSet.RecordSet.FieldName("NOMBRE_ESTACION").asString();
                    RHGROUPCRCPU.NOMBRE = DS_RHGROUPCRCPU.DataSet.RecordSet.FieldName("NOMBREE").asString();
                    RHGROUPCRCPU.APELLIDO = DS_RHGROUPCRCPU.DataSet.RecordSet.FieldName("APELLIDOE").asString();
                    RHGROUPCRCPU.DIRECCION = DS_RHGROUPCRCPU.DataSet.RecordSet.FieldName("DIRECCION").asString();
                    RHGROUPCRCPUList.push(RHGROUPCRCPU);

                    DS_RHGROUPCRCPU.DataSet.Next();
                }
            }
            else {
                DS_RHGROUPCRCPU.ResErr.NotError = false;
                DS_RHGROUPCRCPU.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPCRCPU_GETID = function (RHGROUPCRCPU) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPCRCPU.IDRHGROUPCRCPU.FieldName, RHGROUPCRCPU.IDRHGROUPCRCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPCRCPU.IDCPU.FieldName, RHGROUPCRCPU.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPCRCPU.IDRHGROUPCR.FieldName, RHGROUPCRCPU.IDRHGROUPCR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var DS_RHGROUPCRCPU = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "RHGROUPCRCPU_GETID", Param.ToBytes());
        ResErr = DS_RHGROUPCRCPU.ResErr;
        if (ResErr.NotError) {
            if (DS_RHGROUPCRCPU.DataSet.RecordCount > 0) {
                DS_RHGROUPCRCPU.DataSet.First();
                RHGROUPCRCPU.IDRHGROUPCRCPU = DS_RHGROUPCRCPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPCRCPU.IDRHGROUPCRCPU.FieldName).asInt32();
                //Other
                RHGROUPCRCPU.IDCPU = DS_RHGROUPCRCPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPCRCPU.IDCPU.FieldName).asString();
                RHGROUPCRCPU.IDRHGROUPCR = DS_RHGROUPCRCPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPCRCPU.IDRHGROUPCR.FieldName).asInt32();
            }
            else {
                DS_RHGROUPCRCPU.ResErr.NotError = false;
                DS_RHGROUPCRCPU.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

Persistence.RemoteHelp.Methods.RHGROUPCRCPU_ADD = function (RHGROUPCRCPU) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPCRCPU.IDCPU.FieldName, RHGROUPCRCPU.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPCRCPU.IDRHGROUPCR.FieldName, RHGROUPCRCPU.IDRHGROUPCR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHGROUPCRCPU_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.RemoteHelp.Methods.RHGROUPCRCPU_GETID(RHGROUPCRCPU);
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPCRCPU_UPD = function (RHGROUPCRCPU) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPCRCPU.IDRHGROUPCRCPU.FieldName, RHGROUPCRCPU.IDRHGROUPCRCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPCRCPU.IDCPU.FieldName, RHGROUPCRCPU.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPCRCPU.IDRHGROUPCR.FieldName, RHGROUPCRCPU.IDRHGROUPCR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHGROUPCRCPU_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.RemoteHelp.Methods.RHGROUPCRCPU_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.RemoteHelp.Methods.RHGROUPCRCPU_DELIDRHGROUPCRCPU(Param);
    }
    else {
        Res = Persistence.RemoteHelp.Methods.RHGROUPCRCPU_DELRHGROUPCRCPU(Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.RHGROUPCRCPU_DELIDRHGROUPCRCPU = function (IDRHGROUPCRCPU) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPCRCPU.IDRHGROUPCRCPU.FieldName, IDRHGROUPCRCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHGROUPCRCPU_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPCRCPU_DELRHGROUPCRCPU = function (RHGROUPCRCPU) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPCRCPU.IDRHGROUPCRCPU.FieldName, RHGROUPCRCPU.IDRHGROUPCRCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPCRCPU.IDCPU.FieldName, RHGROUPCRCPU.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPCRCPU.IDRHGROUPCR.FieldName, RHGROUPCRCPU.IDRHGROUPCR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHGROUPCRCPU_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}


//**********************   METHODS   ********************************************************************************************

Persistence.RemoteHelp.Methods.RHGROUPCRCPU_ListSetID = function (RHGROUPCRCPUList, IDRHGROUPCRCPU) {
    for (var i = 0; i < RHGROUPCRCPUList.length; i++) {

        if (IDRHGROUPCRCPU == RHGROUPCRCPUList[i].IDRHGROUPCRCPU)
            return (RHGROUPCRCPUList[i]);

    }
    var UnRHGROUPCRCPU = new Persistence.RemoteHelp.Properties.TRHGROUPCRCPU();
    UnRHGROUPCRCPU.IDRHGROUPCRCPU = IDRHGROUPCRCPU;
    return (UnRHGROUPCRCPU);
}
Persistence.RemoteHelp.Methods.RHGROUPCRCPU_ListAdd = function (RHGROUPCRCPUList, RHGROUPCRCPU) {
    var i = Persistence.RemoteHelp.Methods.RHGROUPCRCPU_ListGetIndex(RHGROUPCRCPUList, RHGROUPCRCPU);
    if (i == -1) RHGROUPCRCPUList.push(RHGROUPCRCPU);
    else {
        RHGROUPCRCPUList[i].IDCPU = RHGROUPCRCPU.IDCPU;
        RHGROUPCRCPUList[i].IDRHGROUPCR = RHGROUPCRCPU.IDRHGROUPCR;
        RHGROUPCRCPUList[i].IDRHGROUPCRCPU = RHGROUPCRCPU.IDRHGROUPCRCPU;
    }
}

//CJRC_26072018
Persistence.RemoteHelp.Methods.RHGROUPCRCPU_ListGetIndex = function (RHGROUPCRCPUList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.RemoteHelp.Methods.RHGROUPCRCPU_ListGetIndexIDRHGROUPCRCPU(RHGROUPCRCPUList, Param);

    }
    else {
        Res = Persistence.RemoteHelp.Methods.RHGROUPCRCPU_ListGetIndexRHGROUPCRCPU(RHGROUPCRCPUList, Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.RHGROUPCRCPU_ListGetIndexRHGROUPCRCPU = function (RHGROUPCRCPUList, RHGROUPCRCPU) {
    for (var i = 0; i < RHGROUPCRCPUList.length; i++) {
        if (RHGROUPCRCPU.IDRHGROUPCRCPU == RHGROUPCRCPUList[i].IDRHGROUPCRCPU)
            return (i);
    }
    return (-1);
}
Persistence.RemoteHelp.Methods.RHGROUPCRCPU_ListGetIndexIDRHGROUPCRCPU = function (RHGROUPCRCPUList, IDRHGROUPCRCPU) {
    for (var i = 0; i < RHGROUPCRCPUList.length; i++) {
        if (IDRHGROUPCRCPU == RHGROUPCRCPUList[i].IDRHGROUPCRCPU)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.RemoteHelp.Methods.RHGROUPCRCPUListtoStr = function (RHGROUPCRCPUList) {
    var Res = Persistence.RemoteHelp.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(RHGROUPCRCPUList.length, Longitud, Texto);
        for (Counter = 0; Counter < RHGROUPCRCPUList.length; Counter++) {
            var UnRHGROUPCRCPU = RHGROUPCRCPUList[Counter];
            UnRHGROUPCRCPU.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.RemoteHelp.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;

}
Persistence.RemoteHelp.Methods.StrtoRHGROUPCRCPU = function (ProtocoloStr, RHGROUPCRCPUList) {
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        RHGROUPCRCPUList.length = 0;
        if (Persistence.RemoteHelp.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnRHGROUPCRCPU = new Persistence.Demo.Properties.TRHGROUPCRCPU(); //Mode new row
                UnRHGROUPCRCPU.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.RHGROUPCRCPU_ListAdd(RHGROUPCRCPUList, UnRHGROUPCRCPU);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}

//Socket Function 
Persistence.RemoteHelp.Methods.RHGROUPCRCPUListToByte = function (RHGROUPCRCPUList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, RHGROUPCRCPUList.length - idx);
    for (var i = idx; i < RHGROUPCRCPUList.length; i++) {
        RHGROUPCRCPUList[i].ToByte(MemStream);
    }
}
Persistence.RemoteHelp.Methods.ByteToRHGROUPCRCPUList = function (RHGROUPCRCPUList, MemStream) {
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (var i = 0; i < Count; i++) {
        var UnRHGROUPCRCPU = new Persistence.RemoteHelp.Properties.TRHGROUPCRCPU();
        UnRHGROUPCRCPU.ByteTo(MemStream);
        Persistence.RemoteHelp.Methods.RHGROUPCRCPU_ListAdd(RHGROUPCRCPUList, UnRHGROUPCRCPU);
    }
}
