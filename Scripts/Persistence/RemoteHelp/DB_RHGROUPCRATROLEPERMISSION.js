﻿//TreduceDB
//Persistence.RemoteHelp.Properties
//Persistence.RemoteHelp.Methods

Persistence.RemoteHelp.Properties.TRHGROUPCRATROLEPERMISSION = function () {
    this.IDRHGROUPCRATROLEPERMISSION = 0;
    this.IDATROLE = 0;
    this.IDRHGROUPCR = 0;
    this.IDRHGROUPCRPERMISSION = 0;
    this.ROLE_NAME = "";
    this.GROUP_NAME = "";
    this.PERMISSION_NAME = "";
    //virtual
    //var RHGROUPCR = new Persistence.RemoteHelp.Properties.TRHGROUPCR();



    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDATROLE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDRHGROUPCR);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDRHGROUPCRATROLEPERMISSION);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDRHGROUPCRPERMISSION);
    }
    this.ByteTo = function (MemStream) {
        this.IDATROLE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDRHGROUPCR = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDRHGROUPCRATROLEPERMISSION = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDRHGROUPCRPERMISSION = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDATROLE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDRHGROUPCR, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDRHGROUPCRATROLEPERMISSION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDRHGROUPCRPERMISSION, Longitud, Texto);
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDATROLE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDRHGROUPCR = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDRHGROUPCRATROLEPERMISSION = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDRHGROUPCRPERMISSION = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
    }
}

//**********************   METHODS server  ********************************************************************************************

Persistence.RemoteHelp.Methods.RHGROUPCRATROLEPERMISSION_Fill = function (RHGROUPCRATROLEPERMISSIONList) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    RHGROUPCRATROLEPERMISSIONList.length = 0
    try {
        var DS_RHGROUPCRATROLEPERMISSION = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "RHGROUPCRATROLEPERMISSION_GET", new Array());
        ResErr = DS_RHGROUPCRATROLEPERMISSION.ResErr;
        if (ResErr.NotError) {
            if (DS_RHGROUPCRATROLEPERMISSION.DataSet.RecordCount > 0) {
                DS_RHGROUPCRATROLEPERMISSION.DataSet.First();
                while (!(DS_RHGROUPCRATROLEPERMISSION.DataSet.Eof)) {
                    var RHGROUPCRATROLEPERMISSION = new Persistence.RemoteHelp.Properties.TRHGROUPCRATROLEPERMISSION();
                    RHGROUPCRATROLEPERMISSION.IDRHGROUPCRATROLEPERMISSION = DS_RHGROUPCRATROLEPERMISSION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDRHGROUPCRATROLEPERMISSION.FieldName).asInt32();
                    RHGROUPCRATROLEPERMISSION.IDATROLE = DS_RHGROUPCRATROLEPERMISSION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDATROLE.FieldName).asInt32();
                    RHGROUPCRATROLEPERMISSION.IDRHGROUPCR = DS_RHGROUPCRATROLEPERMISSION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDRHGROUPCR.FieldName).asInt32();
                    RHGROUPCRATROLEPERMISSION.IDRHGROUPCRPERMISSION = DS_RHGROUPCRATROLEPERMISSION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDRHGROUPCRPERMISSION.FieldName).asInt32();

                    RHGROUPCRATROLEPERMISSION.ROLE_NAME = DS_RHGROUPCRATROLEPERMISSION.DataSet.RecordSet.FieldName("ROLENAME").asString();
                    RHGROUPCRATROLEPERMISSION.GROUP_NAME = DS_RHGROUPCRATROLEPERMISSION.DataSet.RecordSet.FieldName("RHGROUPCR_NAME").asString();
                    RHGROUPCRATROLEPERMISSION.PERMISSION_NAME = DS_RHGROUPCRATROLEPERMISSION.DataSet.RecordSet.FieldName("PERMISSION").asString();

                    RHGROUPCRATROLEPERMISSIONList.push(RHGROUPCRATROLEPERMISSION);

                    DS_RHGROUPCRATROLEPERMISSION.DataSet.Next();
                }
            }
            else {
                DS_RHGROUPCRATROLEPERMISSION.ResErr.NotError = false;
                DS_RHGROUPCRATROLEPERMISSION.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPCRATROLEPERMISSION_GETID = function (RHGROUPCRATROLEPERMISSION) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDRHGROUPCRATROLEPERMISSION.FieldName, RHGROUPCRATROLEPERMISSION.IDRHGROUPCRATROLEPERMISSION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDATROLE.FieldName, RHGROUPCRATROLEPERMISSION.IDATROLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDRHGROUPCR.FieldName, RHGROUPCRATROLEPERMISSION.IDRHGROUPCR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDRHGROUPCRPERMISSION.FieldName, RHGROUPCRATROLEPERMISSION.IDRHGROUPCRPERMISSION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var DS_RHGROUPCRATROLEPERMISSION = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "RHGROUPCRATROLEPERMISSION_GETID", Param.ToBytes());
        ResErr = DS_RHGROUPCRATROLEPERMISSION.ResErr;
        if (ResErr.NotError) {
            if (DS_RHGROUPCRATROLEPERMISSION.DataSet.RecordCount > 0) {
                DS_RHGROUPCRATROLEPERMISSION.DataSet.First();
                RHGROUPCRATROLEPERMISSION.IDRHGROUPCRATROLEPERMISSION = DS_RHGROUPCRATROLEPERMISSION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDRHGROUPCRATROLEPERMISSION.FieldName).asInt32();
                //Other
                RHGROUPCRATROLEPERMISSION.IDATROLE = DS_RHGROUPCRATROLEPERMISSION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDATROLE.FieldName).asInt32();
                RHGROUPCRATROLEPERMISSION.IDRHGROUPCR = DS_RHGROUPCRATROLEPERMISSION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDRHGROUPCR.FieldName).asInt32();
                RHGROUPCRATROLEPERMISSION.IDRHGROUPCRPERMISSION = DS_RHGROUPCRATROLEPERMISSION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDRHGROUPCRPERMISSION.FieldName).asInt32();
            }
            else {
                DS_RHGROUPCRATROLEPERMISSION.ResErr.NotError = false;
                DS_RHGROUPCRATROLEPERMISSION.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPCRATROLEPERMISSION_ADD = function (RHGROUPCRATROLEPERMISSION) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDRHGROUPCRATROLEPERMISSION.FieldName, RHGROUPCRATROLEPERMISSION.IDRHGROUPCRATROLEPERMISSION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDATROLE.FieldName, RHGROUPCRATROLEPERMISSION.IDATROLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDRHGROUPCR.FieldName, RHGROUPCRATROLEPERMISSION.IDRHGROUPCR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDRHGROUPCRPERMISSION.FieldName, RHGROUPCRATROLEPERMISSION.IDRHGROUPCRPERMISSION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHGROUPCRATROLEPERMISSION_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.RemoteHelp.Methods.RHGROUPCRATROLEPERMISSION_GETID(RHGROUPCRATROLEPERMISSION);
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPCRATROLEPERMISSION_UPD = function (RHGROUPCRATROLEPERMISSION) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDRHGROUPCRATROLEPERMISSION.FieldName, RHGROUPCRATROLEPERMISSION.IDRHGROUPCRATROLEPERMISSION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDATROLE.FieldName, RHGROUPCRATROLEPERMISSION.IDATROLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDRHGROUPCR.FieldName, RHGROUPCRATROLEPERMISSION.IDRHGROUPCR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDRHGROUPCRPERMISSION.FieldName, RHGROUPCRATROLEPERMISSION.IDRHGROUPCRPERMISSION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHGROUPCRATROLEPERMISSION_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.RemoteHelp.Methods.RHGROUPCRATROLEPERMISSION_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.RemoteHelp.Methods.RHGROUPCRATROLEPERMISSION_DELIDRHGROUPCRATROLEPERMISSION(/*String StrIDRHGROUPCRATROLEPERMISSION*/Param);
    }
    else {
        Res = Persistence.RemoteHelp.Methods.RHGROUPCRATROLEPERMISSION_DELRHGROUPCRATROLEPERMISSION(Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.RHGROUPCRATROLEPERMISSION_DELIDRHGROUPCRATROLEPERMISSION = function (/*String StrIDRHGROUPCRATROLEPERMISSION*/IDRHGROUPCRATROLEPERMISSION) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDRHGROUPCRATROLEPERMISSION.FieldName, IDRHGROUPCRATROLEPERMISSION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHGROUPCRATROLEPERMISSION_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPCRATROLEPERMISSION_DELRHGROUPCRATROLEPERMISSION = function (RHGROUPCRATROLEPERMISSION) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDRHGROUPCRATROLEPERMISSION.FieldName, RHGROUPCRATROLEPERMISSION.IDRHGROUPCRATROLEPERMISSION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDATROLE.FieldName, RHGROUPCRATROLEPERMISSION.IDATROLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDRHGROUPCR.FieldName, RHGROUPCRATROLEPERMISSION.IDRHGROUPCR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDRHGROUPCRPERMISSION.FieldName, RHGROUPCRATROLEPERMISSION.IDRHGROUPCRPERMISSION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHGROUPCRATROLEPERMISSION_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}


//**********************   METHODS   ********************************************************************************************

Persistence.RemoteHelp.Methods.RHGROUPCRATROLEPERMISSION_ListSetID = function (RHGROUPCRATROLEPERMISSIONList, IDRHGROUPCRATROLEPERMISSION) {
    for (var i = 0; i < RHGROUPCRATROLEPERMISSIONList.length; i++) {

        if (IDRHGROUPCRATROLEPERMISSION == RHGROUPCRATROLEPERMISSIONList[i].IDRHGROUPCRATROLEPERMISSION)
            return (RHGROUPCRATROLEPERMISSIONList[i]);

    }
    var UnRHGROUPCRATROLEPERMISSION = new Persistence.RemoteHelp.Properties.TRHGROUPCRATROLEPERMISSION();
    UnRHGROUPCRATROLEPERMISSION.IDRHGROUPCRATROLEPERMISSION = IDRHGROUPCRATROLEPERMISSION
    return (UnRHGROUPCRATROLEPERMISSION);
}


Persistence.RemoteHelp.Methods.RHGROUPCRATROLEPERMISSION_ListAdd = function (RHGROUPCRATROLEPERMISSIONList, RHGROUPCRATROLEPERMISSION) {
    var i = Persistence.RemoteHelp.Methods.RHGROUPCRATROLEPERMISSION_ListGetIndex(RHGROUPCRATROLEPERMISSIONList, RHGROUPCRATROLEPERMISSION);
    if (i == -1) RHGROUPCRATROLEPERMISSIONList.push(RHGROUPCRATROLEPERMISSION);
    else {
        RHGROUPCRATROLEPERMISSIONList[i].IDATROLE = RHGROUPCRATROLEPERMISSION.IDATROLE;
        RHGROUPCRATROLEPERMISSIONList[i].IDRHGROUPCR = RHGROUPCRATROLEPERMISSION.IDRHGROUPCR;
        RHGROUPCRATROLEPERMISSIONList[i].IDRHGROUPCRATROLEPERMISSION = RHGROUPCRATROLEPERMISSION.IDRHGROUPCRATROLEPERMISSION;
        RHGROUPCRATROLEPERMISSIONList[i].IDRHGROUPCRPERMISSION = RHGROUPCRATROLEPERMISSION.IDRHGROUPCRPERMISSION;
    }
}

//CJRC_26072018
Persistence.RemoteHelp.Methods.RHGROUPCRATROLEPERMISSION_ListGetIndex = function (RHGROUPCRATROLEPERMISSIONList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.RemoteHelp.Methods.RHGROUPCRATROLEPERMISSION_ListGetIndexIDRHGROUPCRATROLEPERMISSION(RHGROUPCRATROLEPERMISSIONList, Param);

    }
    else {
        Res = Persistence.RemoteHelp.Methods.RHGROUPCRATROLEPERMISSION_ListGetIndexRHGROUPCRATROLEPERMISSION(RHGROUPCRATROLEPERMISSIONList, Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.RHGROUPCRATROLEPERMISSION_ListGetIndexRHGROUPCRATROLEPERMISSION = function (RHGROUPCRATROLEPERMISSIONList, RHGROUPCRATROLEPERMISSION) {
    for (var i = 0; i < RHGROUPCRATROLEPERMISSIONList.length; i++) {
        if (RHGROUPCRATROLEPERMISSION.IDRHGROUPCRATROLEPERMISSION == RHGROUPCRATROLEPERMISSIONList[i].IDRHGROUPCRATROLEPERMISSION)
            return (i);
    }
    return (-1);
}
Persistence.RemoteHelp.Methods.RHGROUPCRATROLEPERMISSION_ListGetIndexIDRHGROUPCRATROLEPERMISSION = function (RHGROUPCRATROLEPERMISSIONList, IDRHGROUPCRATROLEPERMISSION) {
    for (var i = 0; i < RHGROUPCRATROLEPERMISSIONList.length; i++) {
        if (IDRHGROUPCRATROLEPERMISSION == RHGROUPCRATROLEPERMISSIONList[i].IDRHGROUPCRATROLEPERMISSION)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.RemoteHelp.Methods.RHGROUPCRATROLEPERMISSIONListtoStr = function (RHGROUPCRATROLEPERMISSIONList) {
    var Res = Persistence.RemoteHelp.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(RHGROUPCRATROLEPERMISSIONList.length, Longitud, Texto);
        for (Counter = 0; Counter < RHGROUPCRATROLEPERMISSIONList.length; Counter++) {
            var UnRHGROUPCRATROLEPERMISSION = RHGROUPCRATROLEPERMISSIONList[Counter];
            UnRHGROUPCRATROLEPERMISSION.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.RemoteHelp.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;

}
Persistence.RemoteHelp.Methods.StrtoRHGROUPCRATROLEPERMISSION = function (ProtocoloStr, RHGROUPCRATROLEPERMISSIONList) {
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        RHGROUPCRATROLEPERMISSIONList.length = 0;
        if (Persistence.RemoteHelp.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnRHGROUPCRATROLEPERMISSION = new Persistence.Demo.Properties.TRHGROUPCRATROLEPERMISSION(); //Mode new row
                UnRHGROUPCRATROLEPERMISSION.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.RHGROUPCRATROLEPERMISSION_ListAdd(RHGROUPCRATROLEPERMISSIONList, UnRHGROUPCRATROLEPERMISSION);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}

//Socket Function 
Persistence.RemoteHelp.Methods.RHGROUPCRATROLEPERMISSIONListToByte = function (RHGROUPCRATROLEPERMISSIONList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, RHGROUPCRATROLEPERMISSIONList.length - idx);
    for (var i = idx; i < RHGROUPCRATROLEPERMISSIONList.length; i++) {
        RHGROUPCRATROLEPERMISSIONList[i].ToByte(MemStream);
    }
}
Persistence.RemoteHelp.Methods.ByteToRHGROUPCRATROLEPERMISSIONList = function (RHGROUPCRATROLEPERMISSIONList, MemStream) {
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (var i = 0; i < Count; i++) {
        var UnRHGROUPCRATROLEPERMISSION = new Persistence.RemoteHelp.Properties.TRHGROUPCRATROLEPERMISSION();
        UnRHGROUPCRATROLEPERMISSION.ByteTo(MemStream);
        Persistence.RemoteHelp.Methods.RHGROUPCRATROLEPERMISSION_ListAdd(RHGROUPCRATROLEPERMISSIONList, UnRHGROUPCRATROLEPERMISSION);
    }
}