﻿//TreduceDB
//Persistence.RemoteHelp.Properties
//Persistence.RemoteHelp.Methods

Persistence.RemoteHelp.Properties.TUSERS = function () {
    this.APELLIDOE = "";
    this.CEMPLEADO = "";
    this.DIRECCION = "";
    this.IDCPU = "";
    this.MAIL = "";
    this.NOMBREE = "";
    this.TELEFONO = "";
    this.UBICACION = "";

    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.APELLIDOE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CEMPLEADO);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.DIRECCION);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.IDCPU);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.MAIL);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.NOMBREE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.TELEFONO);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.UBICACION);
    }
    this.ByteTo = function (MemStream) {
        this.APELLIDOE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CEMPLEADO = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.DIRECCION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDCPU = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.MAIL = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.NOMBREE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.TELEFONO = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.UBICACION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteStr(this.APELLIDOE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.CEMPLEADO, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.DIRECCION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.IDCPU, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.MAIL, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.NOMBREE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.TELEFONO, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.UBICACION, Longitud, Texto);
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.APELLIDOE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.CEMPLEADO = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.DIRECCION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.IDCPU = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.MAIL = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.NOMBREE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.TELEFONO = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.UBICACION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
    }

}

//**********************   METHODS server  ********************************************************************************************

Persistence.RemoteHelp.Methods.USERS_Fill = function (USERSList, StrIDCPU) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    USERSList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDCPU == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.USERS.IDCPU.FieldName, " = " + UsrCfg.InternoAtisNames.USERS.NAME_TABLE + "." + UsrCfg.InternoAtisNames.USERS.IDCPU.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDCPU = " IN (" + StrIDCPU + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.USERS.IDCPU.FieldName, StrIDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    try {
        var DS_USERS = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "USERS_GET", Param.ToBytes());
        ResErr = DS_USERS.ResErr;
        if (ResErr.NotError) {
            if (DS_USERS.DataSet.RecordCount > 0) {
                DS_USERS.DataSet.First();
                while (!(DS_USERS.DataSet.Eof)) {
                    var USERS = new Persistence.RemoteHelp.Properties.TUSERS();
                    //Other
                    USERS.APELLIDOE = DS_USERS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.USERS.APELLIDOE.FieldName).asString();
                    USERS.CEMPLEADO = DS_USERS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.USERS.CEMPLEADO.FieldName).asString();
                    USERS.DIRECCION = DS_USERS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.USERS.DIRECCION.FieldName).asString();
                    USERS.IDCPU = DS_USERS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.USERS.IDCPU.FieldName).asString();
                    USERS.MAIL = DS_USERS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.USERS.MAIL.FieldName).asString();
                    USERS.NOMBREE = DS_USERS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.USERS.NOMBREE.FieldName).asString();
                    USERS.TELEFONO = DS_USERS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.USERS.TELEFONO.FieldName).asString();
                    USERS.UBICACION = DS_USERS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.USERS.UBICACION.FieldName).asString();
                    USERSList.push(USERS);

                    DS_USERS.DataSet.Next();
                }
            }
            else {
                DS_USERS.ResErr.NotError = false;
                DS_USERS.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.USERS_GETID = function (USERS) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.USERS.APELLIDOE.FieldName, USERS.APELLIDOE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.USERS.CEMPLEADO.FieldName, USERS.CEMPLEADO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.USERS.DIRECCION.FieldName, USERS.DIRECCION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.USERS.IDCPU.FieldName, USERS.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.USERS.MAIL.FieldName, USERS.MAIL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.USERS.NOMBREE.FieldName, USERS.NOMBREE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.USERS.TELEFONO.FieldName, USERS.TELEFONO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.USERS.UBICACION.FieldName, USERS.UBICACION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var DS_USERS = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "USERS_GETID", Param.ToBytes());
        ResErr = DS_USERS.ResErr;
        if (ResErr.NotError) {
            if (DS_USERS.DataSet.RecordCount > 0) {
                DS_USERS.DataSet.First();
                //Other
                USERS.APELLIDOE = DS_USERS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.USERS.APELLIDOE.FieldName).asString();
                USERS.CEMPLEADO = DS_USERS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.USERS.CEMPLEADO.FieldName).asString();
                USERS.DIRECCION = DS_USERS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.USERS.DIRECCION.FieldName).asString();
                USERS.IDCPU = DS_USERS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.USERS.IDCPU.FieldName).asString();
                USERS.MAIL = DS_USERS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.USERS.MAIL.FieldName).asString();
                USERS.NOMBREE = DS_USERS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.USERS.NOMBREE.FieldName).asString();
                USERS.TELEFONO = DS_USERS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.USERS.TELEFONO.FieldName).asString();
                USERS.UBICACION = DS_USERS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.USERS.UBICACION.FieldName).asString();
            }
            else {
                DS_USERS.ResErr.NotError = false;
                DS_USERS.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.USERS_ADD = function (USERS) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.USERS.APELLIDOE.FieldName, USERS.APELLIDOE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.USERS.CEMPLEADO.FieldName, USERS.CEMPLEADO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.USERS.DIRECCION.FieldName, USERS.DIRECCION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.USERS.IDCPU.FieldName, USERS.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.USERS.MAIL.FieldName, USERS.MAIL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.USERS.NOMBREE.FieldName, USERS.NOMBREE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.USERS.TELEFONO.FieldName, USERS.TELEFONO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.USERS.UBICACION.FieldName, USERS.UBICACION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "USERS_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.RemoteHelp.Methods.USERS_GETID(USERS);
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.USERS_UPD = function (USERS) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.USERS.APELLIDOE.FieldName, USERS.APELLIDOE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.USERS.CEMPLEADO.FieldName, USERS.CEMPLEADO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.USERS.DIRECCION.FieldName, USERS.DIRECCION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.USERS.IDCPU.FieldName, USERS.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.USERS.MAIL.FieldName, USERS.MAIL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.USERS.NOMBREE.FieldName, USERS.NOMBREE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.USERS.TELEFONO.FieldName, USERS.TELEFONO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.USERS.UBICACION.FieldName, USERS.UBICACION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "USERS_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.RemoteHelp.Methods.USERS_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.RemoteHelp.Methods.USERS_DELIDCPU(/*String StrIDCPU*/Param);
    }
    else {
        Res = Persistence.RemoteHelp.Methods.USERS_DELUSERS(Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.USERS_DELIDCPU = function (/*String StrIDCPU*/IDCPU) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDCPU == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.USERS.IDCPU.FieldName, " = " + UsrCfg.InternoAtisNames.USERS.NAME_TABLE + "." + UsrCfg.InternoAtisNames.USERS.IDCPU.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDCPU = " IN (" + StrIDCPU + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.USERS.IDCPU.FieldName, StrIDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.USERS.IDCPU.FieldName, IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "USERS_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.USERS_DELUSERS = function (USERS) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.USERS.APELLIDOE.FieldName, USERS.APELLIDOE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.USERS.CEMPLEADO.FieldName, USERS.CEMPLEADO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.USERS.DIRECCION.FieldName, USERS.DIRECCION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.USERS.IDCPU.FieldName, USERS.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.USERS.MAIL.FieldName, USERS.MAIL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.USERS.NOMBREE.FieldName, USERS.NOMBREE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.USERS.TELEFONO.FieldName, USERS.TELEFONO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.USERS.UBICACION.FieldName, USERS.UBICACION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "USERS_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}


//**********************   METHODS   ********************************************************************************************

Persistence.RemoteHelp.Methods.USERS_ListSetID = function (USERSList, IDCPU) {
    for (var i = 0; i < USERSList.length; i++) {

        if (IDCPU == USERSList[i].IDCPU)
            return (USERSList[i]);

    }
    var UnUSERS = new Persistence.RemoteHelp.Properties.TUSERS();
    UnUSERS.IDCPU = IDCPU;
    return (UnUSERS);
}
Persistence.RemoteHelp.Methods.USERS_ListAdd = function (USERSList, USERS) {
    var i = Persistence.RemoteHelp.Methods.USERS_ListGetIndex(USERSList, USERS);
    if (i == -1) USERSList.push(USERS);
    else {
        USERSList[i].APELLIDOE = USERS.APELLIDOE;
        USERSList[i].CEMPLEADO = USERS.CEMPLEADO;
        USERSList[i].DIRECCION = USERS.DIRECCION;
        USERSList[i].IDCPU = USERS.IDCPU;
        USERSList[i].MAIL = USERS.MAIL;
        USERSList[i].NOMBREE = USERS.NOMBREE;
        USERSList[i].TELEFONO = USERS.TELEFONO;
        USERSList[i].UBICACION = USERS.UBICACION;
    }
}

//CJRC_26072018
Persistence.RemoteHelp.Methods.USERS_ListGetIndex = function (USERSList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.RemoteHelp.Methods.USERS_ListGetIndexIDCPU(USERSList, Param);

    }
    else {
        Res = Persistence.RemoteHelp.Methods.USERS_ListGetIndexUSERS(USERSList, Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.USERS_ListGetIndexUSERS = function (USERSList, USERS) {
    for (var i = 0; i < USERSList.length; i++) {
        if (USERS.IDCPU == USERSList[i].IDCPU)
            return (i);
    }
    return (-1);
}
Persistence.RemoteHelp.Methods.USERS_ListGetIndexIDCPU = function (USERSList, IDCPU) {
    for (var i = 0; i < USERSList.length; i++) {
        if (IDCPU == USERSList[i].IDCPU)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.RemoteHelp.Methods.USERSListtoStr = function (USERSList) {
    var Res = Persistence.RemoteHelp.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(USERSList.length, Longitud, Texto);
        for (Counter = 0; Counter < USERSList.length; Counter++) {
            var UnUSERS = USERSList[Counter];
            UnUSERS.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.RemoteHelp.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.RemoteHelp.Methods.StrtoUSERS = function (ProtocoloStr, USERSList) {
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        USERSList.length = 0;
        if (Persistence.RemoteHelp.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnUSERS = new Persistence.Demo.Properties.TUSERS(); //Mode new row
                UnUSERS.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.USERS_ListAdd(USERSList, UnUSERS);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}

//Socket Function 
Persistence.RemoteHelp.Methods.USERSListToByte = function (USERSList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, USERSList.length - idx);
    for (var i = idx; i < USERSList.length; i++) {
        USERSList[i].ToByte(MemStream);
    }
}
Persistence.RemoteHelp.Methods.ByteToUSERSList = function (USERSList, MemStream) {
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (var i = 0; i < Count; i++) {
        var UnUSERS = new Persistence.RemoteHelp.Properties.TUSERS();
        UnUSERS.ByteTo(MemStream);
        Persistence.RemoteHelp.Methods.USERS_ListAdd(USERSList, UnUSERS);
    }
}
