﻿//TreduceDB
//Persistence.RemoteHelp.Properties
//Persistence.RemoteHelp.Methods

Persistence.RemoteHelp.Properties.TRHGROUPPL = function () {
    this.GROUPPL_DESCRIPTION = "";
    this.GROUPPL_NAME = "";
    this.IDRHGROUPPL = 0;
    this.IDRHGROUPPL_CFGBACKUP = 0;
    this.IDRHGROUPPL_CLI_CONFIG = 0;
    this.IDRHGROUPPL_POLICY = 0;
    //virtual
    this.RHGROUPPLATROLEList = new Array(); //Persistence.RemoteHelp.Properties.TRHGROUPPLATROLE


    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.GROUPPL_DESCRIPTION);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.GROUPPL_NAME);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDRHGROUPPL);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDRHGROUPPL_CFGBACKUP);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDRHGROUPPL_CLI_CONFIG);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDRHGROUPPL_POLICY);
    }
    this.ByteTo = function (MemStream) {
        this.GROUPPL_DESCRIPTION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.GROUPPL_NAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDRHGROUPPL = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDRHGROUPPL_CFGBACKUP = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDRHGROUPPL_CLI_CONFIG = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDRHGROUPPL_POLICY = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteStr(this.GROUPPL_DESCRIPTION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.GROUPPL_NAME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDRHGROUPPL, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDRHGROUPPL_CFGBACKUP, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDRHGROUPPL_CLI_CONFIG, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDRHGROUPPL_POLICY, Longitud, Texto);
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.GROUPPL_DESCRIPTION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.GROUPPL_NAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.IDRHGROUPPL = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDRHGROUPPL_CFGBACKUP = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDRHGROUPPL_CLI_CONFIG = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDRHGROUPPL_POLICY = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
    }
}

//**********************   METHODS server  ********************************************************************************************

Persistence.RemoteHelp.Methods.RHGROUPPL_Fill = function (RHGROUPPLList) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    RHGROUPPLList.length = 0;
    try {
        var DS_RHGROUPPL = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "RHGROUPPL_GET", new Array());
        ResErr = DS_RHGROUPPL.ResErr;
        if (ResErr.NotError) {
            if (DS_RHGROUPPL.DataSet.RecordCount > 0) {
                DS_RHGROUPPL.DataSet.First();
                while (!(DS_RHGROUPPL.DataSet.Eof)) {
                    var RHGROUPPL = new Persistence.RemoteHelp.Properties.TRHGROUPPL();
                    RHGROUPPL.IDRHGROUPPL = DS_RHGROUPPL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL.FieldName).asInt32();
                    RHGROUPPL.GROUPPL_DESCRIPTION = DS_RHGROUPPL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL.GROUPPL_DESCRIPTION.FieldName).asString();
                    RHGROUPPL.GROUPPL_NAME = DS_RHGROUPPL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL.GROUPPL_NAME.FieldName).asString();
                    RHGROUPPL.IDRHGROUPPL_CFGBACKUP = DS_RHGROUPPL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL_CFGBACKUP.FieldName).asInt32();
                    RHGROUPPL.IDRHGROUPPL_CLI_CONFIG = DS_RHGROUPPL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL_CLI_CONFIG.FieldName).asInt32();
                    RHGROUPPL.IDRHGROUPPL_POLICY = DS_RHGROUPPL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL_POLICY.FieldName).asInt32();
                    RHGROUPPLList.push(RHGROUPPL);

                    DS_RHGROUPPL.DataSet.Next();
                }
            }
            else {
                DS_RHGROUPPL.ResErr.NotError = false;
                DS_RHGROUPPL.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPPL_GETID = function (RHGROUPPL) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPPL.GROUPPL_DESCRIPTION.FieldName, RHGROUPPL.GROUPPL_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPPL.GROUPPL_NAME.FieldName, RHGROUPPL.GROUPPL_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL_CFGBACKUP.FieldName, RHGROUPPL.IDRHGROUPPL_CFGBACKUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL_CLI_CONFIG.FieldName, RHGROUPPL.IDRHGROUPPL_CLI_CONFIG, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL_POLICY.FieldName, RHGROUPPL.IDRHGROUPPL_POLICY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var DS_RHGROUPPL = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "RHGROUPPL_GETID", Param.ToBytes());
        ResErr = DS_RHGROUPPL.ResErr;
        if (ResErr.NotError) {
            if (DS_RHGROUPPL.DataSet.RecordCount > 0) {
                DS_RHGROUPPL.DataSet.First();
                RHGROUPPL.IDRHGROUPPL = DS_RHGROUPPL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL.FieldName).asInt32();
                RHGROUPPL.GROUPPL_DESCRIPTION = DS_RHGROUPPL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL.GROUPPL_DESCRIPTION.FieldName).asString();
                RHGROUPPL.GROUPPL_NAME = DS_RHGROUPPL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL.GROUPPL_NAME.FieldName).asString();
                RHGROUPPL.IDRHGROUPPL_CFGBACKUP = DS_RHGROUPPL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL_CFGBACKUP.FieldName).asInt32();
                RHGROUPPL.IDRHGROUPPL_CLI_CONFIG = DS_RHGROUPPL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL_CLI_CONFIG.FieldName).asInt32();
                RHGROUPPL.IDRHGROUPPL_POLICY = DS_RHGROUPPL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL_POLICY.FieldName).asInt32();
            }
            else {
                DS_RHGROUPPL.ResErr.NotError = false;
                DS_RHGROUPPL.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPPL_GETBYID = function (RHGROUPPL, id) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL.FieldName, id, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var DS_RHGROUPPL = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "RHGROUPPL_GETBYID", Param.ToBytes());
        ResErr = DS_RHGROUPPL.ResErr;
        if (ResErr.NotError) {
            if (DS_RHGROUPPL.DataSet.RecordCount > 0) {
                DS_RHGROUPPL.DataSet.First();
                RHGROUPPL.IDRHGROUPPL = DS_RHGROUPPL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL.FieldName).asInt32();
                RHGROUPPL.GROUPPL_DESCRIPTION = DS_RHGROUPPL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL.GROUPPL_DESCRIPTION.FieldName).asString();
                RHGROUPPL.GROUPPL_NAME = DS_RHGROUPPL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL.GROUPPL_NAME.FieldName).asString();
                RHGROUPPL.IDRHGROUPPL_CFGBACKUP = DS_RHGROUPPL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL_CFGBACKUP.FieldName).asInt32();
                RHGROUPPL.IDRHGROUPPL_CLI_CONFIG = DS_RHGROUPPL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL_CLI_CONFIG.FieldName).asInt32();
                RHGROUPPL.IDRHGROUPPL_POLICY = DS_RHGROUPPL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL_POLICY.FieldName).asInt32();
            }
            else {
                DS_RHGROUPPL.ResErr.NotError = false;
                DS_RHGROUPPL.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPPL_GETBYNAME = function (RHGROUPPL, name) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPPL.GROUPPL_NAME.FieldName, name, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var DS_RHGROUPPL = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "RHGROUPPL_GETBYNAME", Param.ToBytes());
        ResErr = DS_RHGROUPPL.ResErr;
        if (ResErr.NotError) {
            if (DS_RHGROUPPL.DataSet.RecordCount > 0) {
                DS_RHGROUPPL.DataSet.First();
                RHGROUPPL.IDRHGROUPPL = DS_RHGROUPPL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL.FieldName).asInt32();
                RHGROUPPL.GROUPPL_DESCRIPTION = DS_RHGROUPPL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL.GROUPPL_DESCRIPTION.FieldName).asString();
                RHGROUPPL.GROUPPL_NAME = DS_RHGROUPPL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL.GROUPPL_NAME.FieldName).asString();
                RHGROUPPL.IDRHGROUPPL_CFGBACKUP = DS_RHGROUPPL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL_CFGBACKUP.FieldName).asInt32();
                RHGROUPPL.IDRHGROUPPL_CLI_CONFIG = DS_RHGROUPPL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL_CLI_CONFIG.FieldName).asInt32();
                RHGROUPPL.IDRHGROUPPL_POLICY = DS_RHGROUPPL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL_POLICY.FieldName).asInt32();
            }
            else {
                DS_RHGROUPPL.ResErr.NotError = false;
                DS_RHGROUPPL.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.EXIST_RHGROUPPL = function (id, name) {
    var resp = 0;
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL.FieldName, id, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPPL.GROUPPL_NAME.FieldName, name, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var DS_RHGROUPPL = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "EXIST_RHGROUPPL", Param.ToBytes());
        ResErr = DS_RHGROUPPL.ResErr;
        if (ResErr.NotError) {
            if (DS_RHGROUPPL.DataSet.RecordCount > 0) {
                DS_RHGROUPPL.DataSet.First();
                resp = parseInt(DS_RHGROUPPL.DataSet.RecordSet.FieldName("TOTAL").Value.toString());
            }
            else {
                DS_RHGROUPPL.ResErr.NotError = false;
                DS_RHGROUPPL.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (resp);
}

Persistence.RemoteHelp.Methods.RHGROUPPL_ADD = function (RHGROUPPL) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPPL.GROUPPL_DESCRIPTION.FieldName, RHGROUPPL.GROUPPL_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPPL.GROUPPL_NAME.FieldName, RHGROUPPL.GROUPPL_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL_CFGBACKUP.FieldName, RHGROUPPL.IDRHGROUPPL_CFGBACKUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL_CLI_CONFIG.FieldName, RHGROUPPL.IDRHGROUPPL_CLI_CONFIG, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL_POLICY.FieldName, RHGROUPPL.IDRHGROUPPL_POLICY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHGROUPPL_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.RemoteHelp.Methods.RHGROUPPL_GETID(RHGROUPPL);
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPPL_UPD = function (RHGROUPPL) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL.FieldName, RHGROUPPL.IDRHGROUPPL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPPL.GROUPPL_DESCRIPTION.FieldName, RHGROUPPL.GROUPPL_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPPL.GROUPPL_NAME.FieldName, RHGROUPPL.GROUPPL_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL_CFGBACKUP.FieldName, RHGROUPPL.IDRHGROUPPL_CFGBACKUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL_CLI_CONFIG.FieldName, RHGROUPPL.IDRHGROUPPL_CLI_CONFIG, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL_POLICY.FieldName, RHGROUPPL.IDRHGROUPPL_POLICY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHGROUPPL_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.RemoteHelp.Methods.RHGROUPPL_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.RemoteHelp.Methods.RHGROUPPL_DELIDRHGROUPPL(Param);
    }
    else {
        Res = Persistence.RemoteHelp.Methods.RHGROUPPL_DELRHGROUPPL(Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.RHGROUPPL_DELIDRHGROUPPL = function (IDRHGROUPPL) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL.FieldName, IDRHGROUPPL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHGROUPPL_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPPL_DELRHGROUPPL = function (RHGROUPPL) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL.FieldName, RHGROUPPL.IDRHGROUPPL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPPL.GROUPPL_DESCRIPTION.FieldName, RHGROUPPL.GROUPPL_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPPL.GROUPPL_NAME.FieldName, RHGROUPPL.GROUPPL_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL_CFGBACKUP.FieldName, RHGROUPPL.IDRHGROUPPL_CFGBACKUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL_CLI_CONFIG.FieldName, RHGROUPPL.IDRHGROUPPL_CLI_CONFIG, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL_POLICY.FieldName, RHGROUPPL.IDRHGROUPPL_POLICY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHGROUPPL_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//**********************   METHODS   ********************************************************************************************

Persistence.RemoteHelp.Methods.RHGROUPPL_ListSetID = function (RHGROUPPLList, IDRHGROUPPL) {
    for (var i = 0; i < RHGROUPPLList.length; i++) {

        if (IDRHGROUPPL == RHGROUPPLList[i].IDRHGROUPPL)
            return (RHGROUPPLList[i]);

    }
    var UnRHGROUPPL = new Persistence.RemoteHelp.Properties.TRHGROUPPL();
    UnRHGROUPPL.IDRHGROUPPL = IDRHGROUPPL;
    return (UnRHGROUPPL);
}
Persistence.RemoteHelp.Methods.RHGROUPPL_ListAdd = function (RHGROUPPLList, RHGROUPPL) {
    var i = Persistence.RemoteHelp.Methods.RHGROUPPL_ListGetIndex(RHGROUPPLList, RHGROUPPL);
    if (i == -1) RHGROUPPLList.push(RHGROUPPL);
    else {
        RHGROUPPLList[i].GROUPPL_DESCRIPTION = RHGROUPPL.GROUPPL_DESCRIPTION;
        RHGROUPPLList[i].GROUPPL_NAME = RHGROUPPL.GROUPPL_NAME;
        RHGROUPPLList[i].IDRHGROUPPL = RHGROUPPL.IDRHGROUPPL;
        RHGROUPPLList[i].IDRHGROUPPL_CFGBACKUP = RHGROUPPL.IDRHGROUPPL_CFGBACKUP;
        RHGROUPPLList[i].IDRHGROUPPL_CLI_CONFIG = RHGROUPPL.IDRHGROUPPL_CLI_CONFIG;
        RHGROUPPLList[i].IDRHGROUPPL_POLICY = RHGROUPPL.IDRHGROUPPL_POLICY;
    }
}

//CJRC_26072018
Persistence.RemoteHelp.Methods.RHGROUPPL_ListGetIndex = function (RHGROUPPLList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.RemoteHelp.Methods.RHGROUPPL_ListGetIndexIDRHGROUPPL(RHGROUPPLList, Param);

    }
    else {
        Res = Persistence.RemoteHelp.Methods.RHGROUPPL_ListGetIndexRHGROUPPL(RHGROUPPLList, Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.RHGROUPPL_ListGetIndexRHGROUPPL = function (RHGROUPPLList, RHGROUPPL) {
    for (var i = 0; i < RHGROUPPLList.length; i++) {
        if (RHGROUPPL.IDRHGROUPPL == RHGROUPPLList[i].IDRHGROUPPL)
            return (i);
    }
    return (-1);
}
Persistence.RemoteHelp.Methods.RHGROUPPL_ListGetIndexIDRHGROUPPL = function (RHGROUPPLList, IDRHGROUPPL) {
    for (var i = 0; i < RHGROUPPLList.length; i++) {
        if (IDRHGROUPPL == RHGROUPPLList[i].IDRHGROUPPL)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.RemoteHelp.Methods.RHGROUPPLListtoStr = function (RHGROUPPLList) {
    var Res = Persistence.RemoteHelp.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(RHGROUPPLList.length, Longitud, Texto);
        for (Counter = 0; Counter < RHGROUPPLList.length; Counter++) {
            var UnRHGROUPPL = RHGROUPPLList[Counter];
            UnRHGROUPPL.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.RemoteHelp.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.RemoteHelp.Methods.StrtoRHGROUPPL = function (ProtocoloStr, RHGROUPPLList) {
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        RHGROUPPLList.length = 0;
        if (Persistence.RemoteHelp.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnRHGROUPPL = new Persistence.Demo.Properties.TRHGROUPPL(); //Mode new row
                UnRHGROUPPL.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.RHGROUPPL_ListAdd(RHGROUPPLList, UnRHGROUPPL);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}

//Socket Function 
Persistence.RemoteHelp.Methods.RHGROUPPLListToByte = function (RHGROUPPLList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, RHGROUPPLList.length - idx);
    for (var i = idx; i < RHGROUPPLList.length; i++) {
        RHGROUPPLList[i].ToByte(MemStream);
    }
}
Persistence.RemoteHelp.Methods.ByteToRHGROUPPLList = function (RHGROUPPLList, MemStream) {
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (var i = 0; i < Count; i++) {
        var UnRHGROUPPL = new Persistence.RemoteHelp.Properties.TRHGROUPPL();
        UnRHGROUPPL.ByteTo(MemStream);
        Persistence.RemoteHelp.Methods.RHGROUPPL_ListAdd(RHGROUPPLList, UnRHGROUPPL);
    }
}
