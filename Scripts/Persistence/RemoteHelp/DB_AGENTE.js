﻿//TreduceDB
//Persistence.RemoteHelp.Properties
//Persistence.RemoteHelp.Methods


Persistence.RemoteHelp.Properties.TAGENTE = function () {
    this.A_CONNECTED = "";
    this.AG_STATUS = "";
    this.ALL_DATE = SysCfg.DB.Properties.SVRNOW();
    this.CLIENTTYPE = "";
    this.COMPANY = "";
    this.DIRS_DATE = SysCfg.DB.Properties.SVRNOW();
    this.ID_COMPANY_L = "";
    this.IDCPU = "";
    this.NINVENT = 0;
    this.SLICENSE = "";
    this.SSLSERVER = "";
    this.ULTIMA_GENERACION = SysCfg.DB.Properties.SVRNOW();
    this.ULTIMA_RECEPCION = SysCfg.DB.Properties.SVRNOW();
    this.VERSION = "";
    this.VERSION_HEAR = "";

    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.A_CONNECTED);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.AG_STATUS);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.ALL_DATE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CLIENTTYPE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.COMPANY);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.DIRS_DATE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.ID_COMPANY_L);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.IDCPU);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.NINVENT);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.SLICENSE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.SSLSERVER);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.ULTIMA_GENERACION);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.ULTIMA_RECEPCION);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.VERSION);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.VERSION_HEAR);
    }
    this.ByteTo = function (MemStream) {
        this.A_CONNECTED = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.AG_STATUS = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.ALL_DATE = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.CLIENTTYPE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.COMPANY = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.DIRS_DATE = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.ID_COMPANY_L = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDCPU = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.NINVENT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.SLICENSE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.SSLSERVER = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.ULTIMA_GENERACION = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.ULTIMA_RECEPCION = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.VERSION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.VERSION_HEAR = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteStr(this.A_CONNECTED, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.AG_STATUS, Longitud, Texto);
        SysCfg.Str.Protocol.WriteDateTime(this.ALL_DATE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.CLIENTTYPE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.COMPANY, Longitud, Texto);
        SysCfg.Str.Protocol.WriteDateTime(this.DIRS_DATE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.ID_COMPANY_L, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.IDCPU, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.NINVENT, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.SLICENSE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.SSLSERVER, Longitud, Texto);
        SysCfg.Str.Protocol.WriteDateTime(this.ULTIMA_GENERACION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteDateTime(this.ULTIMA_RECEPCION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.VERSION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.VERSION_HEAR, Longitud, Texto);
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto)
    {
        this.A_CONNECTED = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.AG_STATUS = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.ALL_DATE = SysCfg.Str.Protocol.ReadDateTime(Pos, Index, LongitudArray, Texto);
        this.CLIENTTYPE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.COMPANY = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.DIRS_DATE = SysCfg.Str.Protocol.ReadDateTime(Pos, Index, LongitudArray, Texto);
        this.ID_COMPANY_L = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.IDCPU = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.NINVENT = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.SLICENSE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.SSLSERVER = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.ULTIMA_GENERACION = SysCfg.Str.Protocol.ReadDateTime(Pos, Index, LongitudArray, Texto);
        this.ULTIMA_RECEPCION = SysCfg.Str.Protocol.ReadDateTime(Pos, Index, LongitudArray, Texto);
        this.VERSION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.VERSION_HEAR = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
    }

}

//**********************   METHODS server  ********************************************************************************************

Persistence.RemoteHelp.Methods.AGENTE_Fill = function (AGENTEList, StrIDCPU) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    AGENTEList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDCPU == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.AGENTE.IDCPU.FieldName, " = " + UsrCfg.InternoAtisNames.AGENTE.NAME_TABLE + "." + UsrCfg.InternoAtisNames.AGENTE.IDCPU.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDCPU = " IN (" + StrIDCPU + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.AGENTE.IDCPU.FieldName, StrIDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    try {
        var DS_AGENTE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "AGENTE_GET", Param.ToBytes());
        ResErr = DS_AGENTE.ResErr;
        if (ResErr.NotError) {
            if (DS_AGENTE.DataSet.RecordCount > 0) {
                DS_AGENTE.DataSet.First();
                while (!(DS_AGENTE.DataSet.Eof)) {
                    var AGENTE = new Persistence.RemoteHelp.Properties.TAGENTE();
                    //Other
                    AGENTE.A_CONNECTED = DS_AGENTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.AGENTE.A_CONNECTED.FieldName).asString();
                    AGENTE.AG_STATUS = DS_AGENTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.AGENTE.AG_STATUS.FieldName).asString();
                    AGENTE.ALL_DATE = DS_AGENTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.AGENTE.ALL_DATE.FieldName).asDateTime();
                    AGENTE.CLIENTTYPE = DS_AGENTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.AGENTE.CLIENTTYPE.FieldName).asString();
                    AGENTE.COMPANY = DS_AGENTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.AGENTE.COMPANY.FieldName).asString();
                    AGENTE.DIRS_DATE = DS_AGENTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.AGENTE.DIRS_DATE.FieldName).asDateTime();
                    AGENTE.ID_COMPANY_L = DS_AGENTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.AGENTE.ID_COMPANY_L.FieldName).asString();
                    AGENTE.IDCPU = DS_AGENTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.AGENTE.IDCPU.FieldName).asString();
                    AGENTE.NINVENT = DS_AGENTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.AGENTE.NINVENT.FieldName).asInt32();
                    AGENTE.SLICENSE = DS_AGENTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.AGENTE.SLICENSE.FieldName).asString();
                    AGENTE.SSLSERVER = DS_AGENTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.AGENTE.SSLSERVER.FieldName).asString();
                    AGENTE.ULTIMA_GENERACION = DS_AGENTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.AGENTE.ULTIMA_GENERACION.FieldName).asDateTime();
                    AGENTE.ULTIMA_RECEPCION = DS_AGENTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.AGENTE.ULTIMA_RECEPCION.FieldName).asDateTime();
                    AGENTE.VERSION = DS_AGENTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.AGENTE.VERSION.FieldName).asString();
                    AGENTE.VERSION_HEAR = DS_AGENTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.AGENTE.VERSION_HEAR.FieldName).asString();
                    AGENTEList.push(AGENTE);

                    DS_AGENTE.DataSet.Next();
                }
            }
            else {
                DS_AGENTE.ResErr.NotError = false;
                DS_AGENTE.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

Persistence.RemoteHelp.Methods.AGENTE_GETID = function (AGENTE) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.AGENTE.A_CONNECTED.FieldName, AGENTE.A_CONNECTED, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.AGENTE.AG_STATUS.FieldName, AGENTE.AG_STATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoAtisNames.AGENTE.ALL_DATE.FieldName, AGENTE.ALL_DATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.AGENTE.CLIENTTYPE.FieldName, AGENTE.CLIENTTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.AGENTE.COMPANY.FieldName, AGENTE.COMPANY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoAtisNames.AGENTE.DIRS_DATE.FieldName, AGENTE.DIRS_DATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.AGENTE.ID_COMPANY_L.FieldName, AGENTE.ID_COMPANY_L, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.AGENTE.IDCPU.FieldName, AGENTE.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.AGENTE.NINVENT.FieldName, AGENTE.NINVENT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.AGENTE.SLICENSE.FieldName, AGENTE.SLICENSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.AGENTE.SSLSERVER.FieldName, AGENTE.SSLSERVER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoAtisNames.AGENTE.ULTIMA_GENERACION.FieldName, AGENTE.ULTIMA_GENERACION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoAtisNames.AGENTE.ULTIMA_RECEPCION.FieldName, AGENTE.ULTIMA_RECEPCION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.AGENTE.VERSION.FieldName, AGENTE.VERSION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.AGENTE.VERSION_HEAR.FieldName, AGENTE.VERSION_HEAR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        var DS_AGENTE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "AGENTE_GETID", Param.ToBytes());
        ResErr = DS_AGENTE.ResErr;
        if (ResErr.NotError) {
            if (DS_AGENTE.DataSet.RecordCount > 0) {
                DS_AGENTE.DataSet.First();
                //Other
                AGENTE.A_CONNECTED = DS_AGENTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.AGENTE.A_CONNECTED.FieldName).asString();
                AGENTE.AG_STATUS = DS_AGENTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.AGENTE.AG_STATUS.FieldName).asString();
                AGENTE.ALL_DATE = DS_AGENTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.AGENTE.ALL_DATE.FieldName).asDateTime();
                AGENTE.CLIENTTYPE = DS_AGENTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.AGENTE.CLIENTTYPE.FieldName).asString();
                AGENTE.COMPANY = DS_AGENTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.AGENTE.COMPANY.FieldName).asString();
                AGENTE.DIRS_DATE = DS_AGENTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.AGENTE.DIRS_DATE.FieldName).asDateTime();
                AGENTE.ID_COMPANY_L = DS_AGENTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.AGENTE.ID_COMPANY_L.FieldName).asString();
                AGENTE.IDCPU = DS_AGENTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.AGENTE.IDCPU.FieldName).asString();
                AGENTE.NINVENT = DS_AGENTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.AGENTE.NINVENT.FieldName).asInt32();
                AGENTE.SLICENSE = DS_AGENTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.AGENTE.SLICENSE.FieldName).asString();
                AGENTE.SSLSERVER = DS_AGENTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.AGENTE.SSLSERVER.FieldName).asString();
                AGENTE.ULTIMA_GENERACION = DS_AGENTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.AGENTE.ULTIMA_GENERACION.FieldName).asDateTime();
                AGENTE.ULTIMA_RECEPCION = DS_AGENTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.AGENTE.ULTIMA_RECEPCION.FieldName).asDateTime();
                AGENTE.VERSION = DS_AGENTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.AGENTE.VERSION.FieldName).asString();
                AGENTE.VERSION_HEAR = DS_AGENTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.AGENTE.VERSION_HEAR.FieldName).asString();
            }
            else {
                DS_AGENTE.ResErr.NotError = false;
                DS_AGENTE.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

Persistence.RemoteHelp.Methods.AGENTE_ADD = function (AGENTE) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.AGENTE.A_CONNECTED.FieldName, AGENTE.A_CONNECTED, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.AGENTE.AG_STATUS.FieldName, AGENTE.AG_STATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoAtisNames.AGENTE.ALL_DATE.FieldName, AGENTE.ALL_DATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.AGENTE.CLIENTTYPE.FieldName, AGENTE.CLIENTTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.AGENTE.COMPANY.FieldName, AGENTE.COMPANY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoAtisNames.AGENTE.DIRS_DATE.FieldName, AGENTE.DIRS_DATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.AGENTE.ID_COMPANY_L.FieldName, AGENTE.ID_COMPANY_L, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.AGENTE.IDCPU.FieldName, AGENTE.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.AGENTE.NINVENT.FieldName, AGENTE.NINVENT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.AGENTE.SLICENSE.FieldName, AGENTE.SLICENSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.AGENTE.SSLSERVER.FieldName, AGENTE.SSLSERVER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoAtisNames.AGENTE.ULTIMA_GENERACION.FieldName, AGENTE.ULTIMA_GENERACION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoAtisNames.AGENTE.ULTIMA_RECEPCION.FieldName, AGENTE.ULTIMA_RECEPCION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.AGENTE.VERSION.FieldName, AGENTE.VERSION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.AGENTE.VERSION_HEAR.FieldName, AGENTE.VERSION_HEAR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "AGENTE_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.RemoteHelp.Methods.AGENTE_GETID(AGENTE);
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

Persistence.RemoteHelp.Methods.AGENTE_UPD = function (AGENTE) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.AGENTE.A_CONNECTED.FieldName, AGENTE.A_CONNECTED, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.AGENTE.AG_STATUS.FieldName, AGENTE.AG_STATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoAtisNames.AGENTE.ALL_DATE.FieldName, AGENTE.ALL_DATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.AGENTE.CLIENTTYPE.FieldName, AGENTE.CLIENTTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.AGENTE.COMPANY.FieldName, AGENTE.COMPANY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoAtisNames.AGENTE.DIRS_DATE.FieldName, AGENTE.DIRS_DATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.AGENTE.ID_COMPANY_L.FieldName, AGENTE.ID_COMPANY_L, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.AGENTE.IDCPU.FieldName, AGENTE.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.AGENTE.NINVENT.FieldName, AGENTE.NINVENT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.AGENTE.SLICENSE.FieldName, AGENTE.SLICENSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.AGENTE.SSLSERVER.FieldName, AGENTE.SSLSERVER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoAtisNames.AGENTE.ULTIMA_GENERACION.FieldName, AGENTE.ULTIMA_GENERACION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoAtisNames.AGENTE.ULTIMA_RECEPCION.FieldName, AGENTE.ULTIMA_RECEPCION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.AGENTE.VERSION.FieldName, AGENTE.VERSION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.AGENTE.VERSION_HEAR.FieldName, AGENTE.VERSION_HEAR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "AGENTE_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}


//CJRC_27072018
Persistence.RemoteHelp.Methods.AGENTE_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.RemoteHelp.Methods.AGENTE_DELIDCPU(/*String StrIDCPU*/ Param);
    }
    else {
        Res = Persistence.RemoteHelp.Methods.AGENTE_DELAGENTE(Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.AGENTE_DELIDCPU = function (/*String StrIDCPU*/ IDCPU) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDCPU == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.AGENTE.IDCPU.FieldName, " = " + UsrCfg.InternoAtisNames.AGENTE.NAME_TABLE + "." + UsrCfg.InternoAtisNames.AGENTE.IDCPU.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDCPU = " IN (" + StrIDCPU + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.AGENTE.IDCPU.FieldName, StrIDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.AGENTE.IDCPU.FieldName, IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "AGENTE_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

Persistence.RemoteHelp.Methods.AGENTE_DELAGENTE = function (AGENTE) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.AGENTE.A_CONNECTED.FieldName, AGENTE.A_CONNECTED, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.AGENTE.AG_STATUS.FieldName, AGENTE.AG_STATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoAtisNames.AGENTE.ALL_DATE.FieldName, AGENTE.ALL_DATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.AGENTE.CLIENTTYPE.FieldName, AGENTE.CLIENTTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.AGENTE.COMPANY.FieldName, AGENTE.COMPANY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoAtisNames.AGENTE.DIRS_DATE.FieldName, AGENTE.DIRS_DATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.AGENTE.ID_COMPANY_L.FieldName, AGENTE.ID_COMPANY_L, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.AGENTE.IDCPU.FieldName, AGENTE.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.AGENTE.NINVENT.FieldName, AGENTE.NINVENT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.AGENTE.SLICENSE.FieldName, AGENTE.SLICENSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.AGENTE.SSLSERVER.FieldName, AGENTE.SSLSERVER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoAtisNames.AGENTE.ULTIMA_GENERACION.FieldName, AGENTE.ULTIMA_GENERACION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoAtisNames.AGENTE.ULTIMA_RECEPCION.FieldName, AGENTE.ULTIMA_RECEPCION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.AGENTE.VERSION.FieldName, AGENTE.VERSION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.AGENTE.VERSION_HEAR.FieldName, AGENTE.VERSION_HEAR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "AGENTE_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}


//**********************   METHODS   ********************************************************************************************

Persistence.RemoteHelp.Methods.AGENTE_ListSetID = function (AGENTEList, IDCPU) {
    for (i = 0; i < AGENTEList.length; i++) {
        if (IDCPU == AGENTEList[i].IDCPU)
            return (AGENTEList[i]);

    }
    var UnAGENTE = new Persistence.RemoteHelp.Properties.TAGENTE();
    UnAGENTE.IDCPU = IDCPU;
    return (UnAGENTE);
}

Persistence.RemoteHelp.Methods.AGENTE_ListAdd = function (AGENTEList, AGENTE) {
    var i = Persistence.RemoteHelp.Methods.AGENTE_ListGetIndex(AGENTEList, AGENTE);
    if (i == -1) AGENTEList.push(AGENTE);
    else {
        AGENTEList[i].A_CONNECTED = AGENTE.A_CONNECTED;
        AGENTEList[i].AG_STATUS = AGENTE.AG_STATUS;
        AGENTEList[i].ALL_DATE = AGENTE.ALL_DATE;
        AGENTEList[i].CLIENTTYPE = AGENTE.CLIENTTYPE;
        AGENTEList[i].COMPANY = AGENTE.COMPANY;
        AGENTEList[i].DIRS_DATE = AGENTE.DIRS_DATE;
        AGENTEList[i].ID_COMPANY_L = AGENTE.ID_COMPANY_L;
        AGENTEList[i].IDCPU = AGENTE.IDCPU;
        AGENTEList[i].NINVENT = AGENTE.NINVENT;
        AGENTEList[i].SLICENSE = AGENTE.SLICENSE;
        AGENTEList[i].SSLSERVER = AGENTE.SSLSERVER;
        AGENTEList[i].ULTIMA_GENERACION = AGENTE.ULTIMA_GENERACION;
        AGENTEList[i].ULTIMA_RECEPCION = AGENTE.ULTIMA_RECEPCION;
        AGENTEList[i].VERSION = AGENTE.VERSION;
        AGENTEList[i].VERSION_HEAR = AGENTE.VERSION_HEAR;
    }
}

//CJRC_26072018
Persistence.RemoteHelp.Methods.AGENTE_ListGetIndex = function (AGENTEList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.RemoteHelp.Methods.AGENTE_ListGetIndexIDCPU(AGENTEList, Param);

    }
    else {
        Res = Persistence.RemoteHelp.Methods.AGENTE_ListGetIndexAGENTE(AGENTEList, Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.AGENTE_ListGetIndexAGENTE = function (AGENTEList, AGENTE) {
    for (i = 0; i < AGENTEList.length; i++) {
        if (AGENTE.IDCPU == AGENTEList[i].IDCPU)
            return (i);
    }
    return (-1);
}

Persistence.RemoteHelp.Methods.AGENTE_ListGetIndexIDCPU = function (AGENTEList, IDCPU) {
    for (i = 0; i < AGENTEList.length; i++) {
        if (IDCPU == AGENTEList[i].IDCPU)
            return (i);
    }
    return (-1);
}

//String Function

Persistence.RemoteHelp.Methods.AGENTEListtoStr = function (AGENTEList) {
    var Res = Persistence.RemoteHelp.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(AGENTEList.length, Longitud, Texto);
        for (Counter = 0; Counter < AGENTEList.length; Counter++) {
            var UnAGENTE = AGENTEList[Counter];
            UnAGENTE.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.RemoteHelp.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}

Persistence.RemoteHelp.Methods.StrtoAGENTE = function (ProtocoloStr, AGENTEList) {
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        AGENTEList.length = 0;
        if (Persistence.RemoteHelp.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnAGENTE = new Persistence.Demo.Properties.TAGENTE(); //Mode new row
                UnAGENTE.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.AGENTE_ListAdd(AGENTEList, UnAGENTE);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}

//Socket Function 
Persistence.RemoteHelp.Methods.AGENTEListToByte = function (AGENTEList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, AGENTEList.length - idx);
    for (var i = idx; i < AGENTEList.length; i++)
    {
        AGENTEList[i].ToByte(MemStream);
    }
}

Persistence.RemoteHelp.Methods.ByteToAGENTEList = function (AGENTEList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (var i = 0; i < Count; i++)
    {
        var UnAGENTE = new Persistence.RemoteHelp.Properties.TAGENTE();
        UnAGENTE.ByteTo(MemStream);
        Persistence.RemoteHelp.Methods.AGENTE_ListAdd(AGENTEList, UnAGENTE);
    }
}
