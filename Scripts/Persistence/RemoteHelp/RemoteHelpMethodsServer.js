﻿//TreduceDB
//Persistence.RemoteHelp






Persistence.RemoteHelp.Methods.GetRHREQUEST_CREATE = function (RHREQUEST) {





    var ResErr = new SysCfg.Error.Properties.TResErr();
    SysCfg.Error.Properties.ResErrfill(ResErr);
    ResErr = Persistence.RemoteHelp.Methods.RHREQUEST_ADD(RHREQUEST);
    var idxCR = 0;
    var idxPL = 0;

    for (var i = 0; i < Persistence.Profiler.RemoteHelpProfiler.CPUList.lenght; i++) {
        for (var x = idxCR; x < RHREQUEST.RHREQUEST_CPUCRList.lenght; x++) {
            idxCR = x;
            var comp = string.Compare(RHREQUEST.RHREQUEST_CPUCRList[x].IDCPU, Persistence.Profiler.RemoteHelpProfiler.CPUList[i].IDCPU, true);
            if (comp == 0) {
                for (var e = 0; e < Persistence.Profiler.RemoteHelpProfiler.CPUList[i].RHGROUPCRCPUList.length; e++) {
                    for (var m = 0; m < Persistence.Profiler.RemoteHelpProfiler.CPUList[i].RHGROUPCRCPUList[e].RHGROUPCR.RHGROUPCRATROLEPERMISSIONList.length; m++) {
                        if (RHREQUEST.IDATROLE == Persistence.Profiler.RemoteHelpProfiler.CPUList[i].RHGROUPCRCPUList[e].RHGROUPCR.RHGROUPCRATROLEPERMISSIONList[m].IDATROLE) {
                            var RHREQUESTCR = new Persistence.RemoteHelp.Properties.TRHREQUESTCR();

                            RHREQUESTCR.IDCPU = RHREQUEST.RHREQUEST_CPUCRList[x].IDCPU;
                            RHREQUESTCR.HOST_NAME = Persistence.Profiler.RemoteHelpProfiler.CPUList[i].ESTACION_RED.HOST_NAME;
                            RHREQUESTCR.IDRHGROUPCRPERMISSION = Properties.TRHGROUPCRPERMISSION_TYPE.GetEnum(Persistence.Profiler.RemoteHelpProfiler.CPUList[i].RHGROUPCRCPUList[e].RHGROUPCR.RHGROUPCRATROLEPERMISSIONList[m].IDRHGROUPCRPERMISSION);
                            RHREQUESTCR.PERMISSION = Persistence.Profiler.RemoteHelpProfiler.CPUList[i].RHGROUPCRCPUList[e].RHGROUPCR.RHGROUPCRATROLEPERMISSIONList[m].PERMISSION_NAME;
                            RHREQUESTCR.IDRHGROUPCR = Persistence.Profiler.RemoteHelpProfiler.CPUList[i].RHGROUPCRCPUList[e].RHGROUPCR.RHGROUPCRATROLEPERMISSIONList[m].IDRHGROUPCR;
                            RHREQUESTCR.RHGROUPCR_NAME = Persistence.Profiler.RemoteHelpProfiler.CPUList[i].RHGROUPCRCPUList[e].RHGROUPCR.RHGROUPCRATROLEPERMISSIONList[m].GROUP_NAME;
                            RHREQUESTCR.TIPO = Persistence.Profiler.RemoteHelpProfiler.CPUList[i].RHGROUPCRCPUList[e].RHGROUPCR.RHGROUPCRATROLEPERMISSIONList[m].ROLE_NAME;
                            RHREQUESTCR.WINDOWS = Persistence.Profiler.RemoteHelpProfiler.CPUList[i].WINDOWS.WINDOWS;
                            RHREQUESTCR.VPROIP = Persistence.Profiler.RemoteHelpProfiler.CPUList[i].VPRO.VPROIP;
                            RHREQUESTCR.ID_COMPANY_L = Persistence.Profiler.RemoteHelpProfiler.CPUList[i].AGENTE.ID_COMPANY_L;
                            for (var o = 0; o < Persistence.Profiler.RemoteHelpProfiler.CPUList[i].IPList.length; o++) {
                                var RHREQUESTIP = new Persistence.RemoteHelp.Properties.TRHREQUESTIP();
                                RHREQUESTIP.DIRECCION_IP = Persistence.Profiler.RemoteHelpProfiler.CPUList[i].IPList[o].DIRECCION_IP;
                                RHREQUESTIP.MAC_IP = Persistence.Profiler.RemoteHelpProfiler.CPUList[i].IPList[o].MAC_IP;
                                RHREQUESTCR.RHREQUESTIPList.push(RHREQUESTIP);
                            }
                            RHREQUEST.RHREQUEST_CPUCRList[x].HOSTLIST = Persistence.RemoteHelp.Methods.RHREQUESTCRtoStr(RHREQUESTCR);
                            if (!UsrCfg.Version.IsProduction) {
                                var RHREQUESTCRTemp = new Persistence.RemoteHelp.Properties.TRHREQUESTCR();
                                Persistence.RemoteHelp.Methods.StrtoRHREQUESTCR(RHREQUEST.RHREQUEST_CPUCRList[x].HOSTLIST, RHREQUESTCRTemp);
                                if (!(RHREQUESTCRTemp.RHREQUESTIPList.length > 0)) {
                                    SysCfg.Log.Methods.WriteLog("RemoteHelpMethodsServer.GetRHREQUEST_CREATE:" + "Error No Start, ListkbmMemARG.RHREQUESTIPList.length=0 " + RHREQUEST.RHREQUEST_CPUCRList[x].HOSTLIST);
                                }

                            }
                            RHREQUEST.RHREQUEST_CPUCRList[x].IDRHREQUEST = RHREQUEST.IDRHREQUEST;
                            RHREQUEST.RHREQUEST_CPUCRList[x].IDRHGROUPCR = Persistence.Profiler.RemoteHelpProfiler.CPUList[i].RHGROUPCRCPUList[e].RHGROUPCR.RHGROUPCRATROLEPERMISSIONList[m].IDRHGROUPCR;
                            RHREQUEST.RHREQUEST_CPUCRList[x].REQUEST_CPUCR_STATUS = Properties.TREQUEST_CPUCR_STATUS._CREATE;
                            RHREQUEST.RHREQUEST_CPUCRList[x].IDRHGROUPCRPERMISSION = RHREQUESTCR.IDRHGROUPCRPERMISSION;
                            ResErr = Persistence.RemoteHelp.Methods.RHREQUEST_CPUCR_ADD(RHREQUEST.RHREQUEST_CPUCRList[x]);

                        }
                    }
                }
                break;
            }
            if (comp > 0) {
                break;
            }
            if (comp < 0) {
            }
        }
        for (var x = idxPL; x < RHREQUEST.RHREQUEST_CPUPLList.length; x++) {
            idxPL = x;
            var comp = string.Compare(RHREQUEST.RHREQUEST_CPUPLList[x].IDCPU, Persistence.Profiler.RemoteHelpProfiler.CPUList[i].IDCPU, true);
            if (comp == 0) {
                for (var e = 0; e < Persistence.Profiler.RemoteHelpProfiler.CPUList[i].RHGROUPPLCPUList.length; e++) {
                    for (var m = 0; m < Persistence.Profiler.RemoteHelpProfiler.CPUList[i].RHGROUPPLCPUList[e].RHGROUPPL.RHGROUPPLATROLEList.length; m++) {
                        if (RHREQUEST.IDATROLE == Persistence.Profiler.RemoteHelpProfiler.CPUList[i].RHGROUPPLCPUList[e].RHGROUPPL.RHGROUPPLATROLEList[m].IDATROLE) {
                            RHREQUEST.RHREQUEST_CPUPLList[x].IDRHREQUEST = RHREQUEST.IDRHREQUEST;
                            var RHREQUESTPL = new Persistence.RemoteHelp.Properties.TRHREQUESTPL();
                            RHREQUESTPL.IDCPU = RHREQUEST.RHREQUEST_CPUPLList[x].IDCPU;
                            RHREQUESTPL.HOST_NAME = Persistence.Profiler.RemoteHelpProfiler.CPUList[i].ESTACION_RED.HOST_NAME;
                            RHREQUESTPL.IDRHGROUPPL = Persistence.Profiler.RemoteHelpProfiler.CPUList[i].RHGROUPPLCPUList[e].RHGROUPPL.RHGROUPPLATROLEList[m].IDRHGROUPPL;
                            RHREQUESTPL.RHGROUPPL_NAME = Persistence.Profiler.RemoteHelpProfiler.CPUList[i].RHGROUPPLCPUList[e].RHGROUPPL.RHGROUPPLATROLEList[m].GROUP_NAME;
                            RHREQUESTPL.TIPO = Persistence.Profiler.RemoteHelpProfiler.CPUList[i].RHGROUPPLCPUList[e].RHGROUPPL.RHGROUPPLATROLEList[m].ROLE_NAME;
                            RHREQUESTPL.WINDOWS = Persistence.Profiler.RemoteHelpProfiler.CPUList[i].WINDOWS.WINDOWS;
                            RHREQUESTPL.VPROIP = Persistence.Profiler.RemoteHelpProfiler.CPUList[i].VPRO.VPROIP;
                            RHREQUESTPL.ID_COMPANY_L = Persistence.Profiler.RemoteHelpProfiler.CPUList[i].AGENTE.ID_COMPANY_L;
                            for (var o = 0; o < Persistence.Profiler.RemoteHelpProfiler.CPUList[i].IPList.length; o++) {
                                var RHREQUESTIP = new Persistence.RemoteHelp.Properties.TRHREQUESTIP();
                                RHREQUESTIP.DIRECCION_IP = Persistence.Profiler.RemoteHelpProfiler.CPUList[i].IPList[o].DIRECCION_IP;
                                RHREQUESTIP.MAC_IP = Persistence.Profiler.RemoteHelpProfiler.CPUList[i].IPList[o].MAC_IP;
                                RHREQUESTPL.RHREQUESTIPList.push(RHREQUESTIP);
                            }
                            RHREQUEST.RHREQUEST_CPUPLList[x].HOSTLIST = Persistence.RemoteHelp.Methods.RHREQUESTPLtoStr(RHREQUESTPL);
                            RHREQUEST.RHREQUEST_CPUPLList[x].IDRHREQUEST = RHREQUEST.IDRHREQUEST;
                            RHREQUEST.RHREQUEST_CPUPLList[x].IDRHGROUPPL = Persistence.Profiler.RemoteHelpProfiler.CPUList[i].RHGROUPPLCPUList[e].RHGROUPPL.RHGROUPPLATROLEList[m].IDRHGROUPPL;
                            RHREQUEST.RHREQUEST_CPUPLList[x].REQUEST_CPUPL_STATUS = Properties.TREQUEST_CPUPL_STATUS._CREATE;
                            RHREQUEST.RHREQUEST_CPUPLList[x].PERMISSION = RHREQUESTPL.PERMISSION;
                            ResErr = Persistence.RemoteHelp.Methods.RHREQUEST_CPUPL_ADD(RHREQUEST.RHREQUEST_CPUPLList[x]);
                        }
                    }
                }


                break;
            }
            if (comp > 0) {
                break;
            }
            if (comp < 0) {
            }
        }
    }

    if ((RHREQUEST.RHREQUEST_CPUCRList.length == 0) && (RHREQUEST.RHREQUEST_CPUPLList.length == 0)) {
        RHREQUEST.REQUEST_STATUS = Properties.TREQUEST_STATUS._CANCELLED;
        ResErr = RHREQUEST_UPD(RHREQUEST);
    }



    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
    }
    catch (Excep) {
        ResErr.Mesaje = Excep.Message.toString();
    }
    finally {
        Param.Destroy();

    }
    return ResErr;
}



