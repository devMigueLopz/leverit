﻿//TreduceDB
//Persistence.RemoteHelp.Properties
//Persistence.RemoteHelp.Methods


//   Persistence.RemoteHelp.Properties.TRHREQUEST = function()
//    {
//        this.IDRHREQUEST = 0;
//        this.IDATROLE = 0;
//        this.IDCMDBCI = 0;
//        this.IDCMDBUSER = 0;
//        this.EVENTDATE = new Date(1970, 0, 1, 0, 0, 0);

//        this.RemoteHelpFuntion = 0;//public TRemoteHelpFuntion RemoteHelpFuntion = TRemoteHelpFuntion._None;

//        this.IDCPUList = new Array();
//        this.Aux= "";              
//        this.ConnectbyHost = true; //ConectarPorHost        :=MyBtn.REG_ConectarPorHost;
//        this.InternetConnection = true;//ConectarPorInternet    :=MyBtn.REG_ConectarPorInternet;
//        this.JustSee = true; //CRSoloVer              :=MyBtn.REG_CRSoloVer;
//        this.DonotAskforPermission = true;//CRNopidaPermiso        :=MyBtn.REG_CRNopidaPermiso;
//        this.LockKeyboard = true;//CRBloquearTecladoMouse :=MyBtn.REG_CRBloquearTecladoMouse;
//        this.LowBandwidth = true;//BajoColor              :=MyBtn.REG_BajoColor;
//        this.RecordSession = true;//ConVideo

//        this.GetRHMode = function() 
//        {
//            if (this.RemoteHelpFuntion == TRemoteHelpFuntion._None)
//            {
//                return TRHMode._None;

//            }
//            else if ((this.RemoteHelpFuntion== TRemoteHelpFuntion._UpdatePolicies) || (this.RemoteHelpFuntion ==  TRemoteHelpFuntion._RestartAllowed))
//            {
//                return TRHMode._PL;
//            }
//            else  
//            {              
//                return TRHMode._CL;
//            }
//        }
//        this.TRHREQUEST = function(inRemoteHelpFuntion)
//        {
//            this.IDATROLE = -1;
//            this.IDCMDBCI = -1;
//            if (CLIENT && ASPX){
//              this.IDCMDBCI = UsrCfg.Properties.Session().UserATRole.User.IDCMDBCI;
//              this.IDATROLE = UsrCfg.Properties.Session().UserATRole.User.IDATROLE;
//            }
//          if (CLIENT && SILVERLIGHT){
//            this.IDCMDBCI = SysCfg.App.Methods.Cfg_WindowsTask.User.IDCMDBCI;
//            this.IDATROLE = SysCfg.App.Methods.Cfg_WindowsTask.User.IDATROLE;
//          }
//          if (CLIENT && WIN)
//            this.IDCMDBCI = SysCfg.App.Methods.Cfg_WindowsTask.User.IDCMDBCI;
//            this.IDATROLE = SysCfg.App.Methods.Cfg_WindowsTask.User.IDATROLE;
//          }
//          if (SERVER && WIN)
//            this.IDCMDBCI = parseInt(UsrCfg.SD.Properties.TSDTypeUser._System);//SysCfg.App.Methods.Cfg_WindowsTask.User.IDCMDBCI;
//            this.IDATROLE = -1;
//          }
//            this.EVENTDATE = SysCfg.DB.Properties.SVRNOW();
//            this.RemoteHelpFuntion = inRemoteHelpFuntion;
//            this.IDCPUList.length = 0;                       
//            this.Aux = "";
//            this.ConnectbyHost = false;
//            this.InternetConnection = false;
//            this.JustSee = false;
//            this.DonotAskforPermission = false;
//            this.LockKeyboard = false;
//            this.LowBandwidth = false;
//            this.RecordSession = false;
//        }

//        this.ToByte = function(MemStream)
//        {
//            SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDATROLE);
//            SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDCMDBCI);
//            SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDCMDBUSER);
//            SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.EVENTDATE);

//            SysCfg.Stream.Methods.WriteStreamInt16(MemStream, parseInt(RemoteHelpFuntion));
//            SysCfg.Stream.Methods.WriteStreamInt16(MemStream, parseInt(IDCPUList.length));
//            for (var i = 0; i < IDCPUList.length; i++)
//            {
//                SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, IDCPUList[i]);
//            }
//            SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.Aux);
//            SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.ConnectbyHost);
//            SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.InternetConnection);
//            SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.JustSee);
//            SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.DonotAskforPermission);
//            SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.LockKeyboard);
//            SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.LowBandwidth);
//            SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.RecordSession);//ConVideo   

//        }
//        this.ByteTo = function(MemStream)
//        {
//            this.IDATROLE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
//            this.IDCMDBCI = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
//            this.IDCMDBUSER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
//            this.EVENTDATE = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
//            this.RemoteHelpFuntion = SysCfg.Stream.Methods.ReadStreamInt16(MemStream);
//            var Count =SysCfg.Stream.Methods.ReadStreamInt16(MemStream);
//            for (var i = 0; i < Count; i++)
//            {
//                IDCPUList.push(SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream));
//            }
//            this.Aux = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
//            this.ConnectbyHost = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
//            this.InternetConnection = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
//            this.JustSee = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
//            this.DonotAskforPermission = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
//            this.LockKeyboard = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
//            this.LowBandwidth = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
//            this.RecordSession = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
//        }
//     }

//**********************   PROPERTIES   *****************************************************************************************

Persistence.RemoteHelp.Properties.TRHREQUEST = function () {
    this.AUX = "";
    this.CONNECTBYHOST = false;
    this.DONOTASKFORPERMISSION = false;
    this.EVENTDATE = SysCfg.DB.Properties.SVRNOW();
    //#if (CLIENT && SILVERLIGHT)
    this.IDATROLE = SysCfg.App.Methods.Cfg_WindowsTask.User.IDATROLE;
    this.IDCMDBCI = SysCfg.App.Methods.Cfg_WindowsTask.User.IDCMDBCI;
    this.IDCMDBUSER = SysCfg.App.Methods.Cfg_WindowsTask.User.IDCMDBUSER;
    //#endif
    this.IDRHREQUEST = -1;
    this.INTERNETCONNECTION = false;
    this.JUSTSEE = false;//View Only
    this.LOCKKEYBOARD = false;//Block Keyboard
    this.LOWBANDWIDTH = false;
    this.RECORDSESSION = false;//Record Session

    this.REMOTEHELPFUNTION = Persistence.RemoteHelp.Properties.TRemoteHelpFuntion._None;
    this.REQUEST_STATUS = Persistence.RemoteHelp.Properties.TREQUEST_STATUS._CREATE;
    this.IDRHMODULE_EXECUTE = Persistence.RemoteHelp.Properties.TRHMODULE._Default;
    this.IDRHMODULE_CREATE = Persistence.RemoteHelp.Properties.TRHMODULE._Default;
    this.IDRHTYPEEXECUTE = Persistence.RemoteHelp.Properties.TRHTYPEEXECUTE._None;

    //Virtual
    this.RHREQUEST_CPUPLList = new Array(); //Persistence.RemoteHelp.Properties.TRHREQUEST_CPUPL
    this.RHREQUEST_CPUCRList = new Array(); //Persistence.RemoteHelp.Properties.TRHREQUEST_CPUCR

    this.GetRHMode = function () {
        if (this.REMOTEHELPFUNTION == Persistence.RemoteHelp.Properties.TRemoteHelpFuntion._None) {
            return Persistence.RemoteHelp.Properties.TRHMode._None;

        }
        else if ((REMOTEHELPFUNTION == Persistence.RemoteHelp.Properties.TRemoteHelpFuntion._UpdatePolicies) || (REMOTEHELPFUNTION == Persistence.RemoteHelp.Properties.TRemoteHelpFuntion._RestartAllowed)) {
            return Persistence.RemoteHelp.Properties.TRHMode._PL;
        }
        else {
            return Persistence.RemoteHelp.Properties.TRHMode._CR;
        }
    }

    this.FillRHREQUEST_CPU = function(IDCPUList)
    {

        IDCPUList.sort();
        var LastCpu = "";
        this.RHREQUEST_CPUCRList.length = 0;
        if (GetRHMode() == TRHMode._CR) {
            for (var i = 0; i < IDCPUList.length; i++) {
                if (LastCpu != IDCPUList[i]) {
                    var RHREQUEST_CPUCR = new TRHREQUEST_CPUCR();
                    RHREQUEST_CPUCR.IDCPU = IDCPUList[i];
                    RHREQUEST_CPUCRList.push(RHREQUEST_CPUCR);
                }
                LastCpu = IDCPUList[i];
            }


        }

        this.RHREQUEST_CPUCRList = SysCfg.CopyList(RHREQUEST_CPUCRList).sort(function (a, b) { return a.IDCPU - b.IDCPU; });////RHREQUEST_CPUCRList = RHREQUEST_CPUCRList.OrderBy(x => x.IDCPU).ToList();




        LastCpu = "";
        this.RHREQUEST_CPUPLList.length = 0;
        if (GetRHMode() == TRHMode._PL) {
            for (var i = 0; i < IDCPUList.length; i++) {
                if (LastCpu != IDCPUList[i]) {

                    var RHREQUEST_CPUPL = new TRHREQUEST_CPUPL();
                    RHREQUEST_CPUPL.IDCPU = IDCPUList[i];
                    this.RHREQUEST_CPUPLList.push(RHREQUEST_CPUPL);
                }
                LastCpu = IDCPUList[i];
            }
        }
        this.RHREQUEST_CPUPLList = SysCfg.CopyList(this.RHREQUEST_CPUPLList).sort(function (a, b) { return a.IDCPU - b.IDCPU; });
    }





    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.AUX);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.CONNECTBYHOST);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.DONOTASKFORPERMISSION);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.EVENTDATE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDATROLE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDCMDBCI);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDCMDBUSER);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDRHREQUEST);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.INTERNETCONNECTION);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.JUSTSEE);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.LOCKKEYBOARD);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.LOWBANDWIDTH);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.RECORDSESSION);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.REMOTEHELPFUNTION.Value);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.REQUEST_STATUS.Value);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDRHMODULE_EXECUTE.Value);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDRHMODULE_CREATE.Value);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDRHTYPEEXECUTE.Value);


        SysCfg.Stream.Methods.WriteStreamInt16(MemStream, parseInt(this.RHREQUEST_CPUPLList.length));
        for (var i = 0; i < this.RHREQUEST_CPUPLList.length; i++) {
            this.RHREQUEST_CPUPLList[i].ToByte(MemStream);
        }
        SysCfg.Stream.Methods.WriteStreamInt16(MemStream, parseInt(this.RHREQUEST_CPUCRList.length));
        for (var i = 0; i < this.RHREQUEST_CPUCRList.length; i++) {
            this.RHREQUEST_CPUCRList[i].ToByte(MemStream);
        }
    }


    this.ByteTo = function (MemStream) {
        this.AUX = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CONNECTBYHOST = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.DONOTASKFORPERMISSION = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.EVENTDATE = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.IDATROLE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDCMDBCI = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDCMDBUSER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDRHREQUEST = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.INTERNETCONNECTION = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.JUSTSEE = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.LOCKKEYBOARD = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.LOWBANDWIDTH = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.RECORDSESSION = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.REMOTEHELPFUNTION = Persistence.RemoteHelp.Properties.TRemoteHelpFuntion.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream)); // (TRemoteHelpFuntion)
        this.REQUEST_STATUS = Persistence.RemoteHelp.Properties.TREQUEST_STATUS.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream)); //(TREQUEST_STATUS)
        this.IDRHMODULE_EXECUTE = Persistence.RemoteHelp.Properties.TRHMODULE.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));  //(TRHMODULE) 
        this.IDRHMODULE_CREATE = Persistence.RemoteHelp.Properties.TRHMODULE.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));  //(TRHMODULE) 
        this.IDRHTYPEEXECUTE = Persistence.RemoteHelp.Properties.TRHTYPEEXECUTE.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream)); //(TRHTYPEEXECUTE) 

        var Count1 = SysCfg.Stream.Methods.ReadStreamInt16(MemStream);
        this.RHREQUEST_CPUPLList.length = 0;
        for (var i = 0; i < Count1; i++) {
            var RHREQUEST_CPUPL = new Persistence.RemoteHelp.Properties.TRHREQUEST_CPUPL();
            RHREQUEST_CPUPL.ByteTo(MemStream);
            this.RHREQUEST_CPUPLList.push(RHREQUEST_CPUPL);
        }
        var Count2 = SysCfg.Stream.Methods.ReadStreamInt16(MemStream);
        this.RHREQUEST_CPUCRList.length = 0;
        for (var i = 0; i < Count2; i++) {
            var RHREQUEST_CPUCR = new Persistence.RemoteHelp.Properties.TRHREQUEST_CPUCR();
            RHREQUEST_CPUCR.ByteTo(MemStream);
            this.RHREQUEST_CPUCRList.push(RHREQUEST_CPUCR);
        }
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteStr(this.AUX, Longitud, Texto);
        SysCfg.Str.Protocol.WriteBool(this.CONNECTBYHOST, Longitud, Texto);
        SysCfg.Str.Protocol.WriteBool(this.DONOTASKFORPERMISSION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteDateTime(this.EVENTDATE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDATROLE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDCMDBCI, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDCMDBUSER, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDRHREQUEST, Longitud, Texto);
        SysCfg.Str.Protocol.WriteBool(this.INTERNETCONNECTION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteBool(this.JUSTSEE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteBool(this.LOCKKEYBOARD, Longitud, Texto);
        SysCfg.Str.Protocol.WriteBool(this.LOWBANDWIDTH, Longitud, Texto);
        SysCfg.Str.Protocol.WriteBool(this.RECORDSESSION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.REMOTEHELPFUNTION.Value, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.REQUEST_STATUS.Value, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDRHMODULE_EXECUTE.Value, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDRHMODULE_CREATE.Value, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDRHTYPEEXECUTE.Value, Longitud, Texto);

    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.AUX = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.CONNECTBYHOST = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);
        this.DONOTASKFORPERMISSION = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);
        this.EVENTDATE = SysCfg.Str.Protocol.ReadDateTime(Pos, Index, LongitudArray, Texto);
        this.IDATROLE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDCMDBCI = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDCMDBUSER = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDRHREQUEST = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.INTERNETCONNECTION = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);
        this.JUSTSEE = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);
        this.LOCKKEYBOARD = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);
        this.LOWBANDWIDTH = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);
        this.RECORDSESSION = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);
        this.REMOTEHELPFUNTION = Persistence.RemoteHelp.Properties.TRemoteHelpFuntion.GetEnum(SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto)); //(TRemoteHelpFuntion)
        this.REQUEST_STATUS = Persistence.RemoteHelp.Properties.TREQUEST_STATUS.GetEnum(SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto)); // (TREQUEST_STATUS)
        this.IDRHMODULE_EXECUTE = Persistence.RemoteHelp.Properties.TRHMODULE.GetEnum(SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto)); //  (TRHMODULE)
        this.IDRHMODULE_CREATE = Persistence.RemoteHelp.Properties.TRHMODULE.GetEnum(SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto)); //(TRHMODULE) 
        this.IDRHTYPEEXECUTE = Persistence.RemoteHelp.Properties.TRHTYPEEXECUTE.GetEnum(SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto)); //(TRHTYPEEXECUTE)
    }

}


//**********************   METHODS server  ********************************************************************************************

Persistence.RemoteHelp.Methods.RHREQUEST_Fill = function (RHREQUESTList, StrIDRHREQUEST/* IDRHREQUEST*/) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    RHREQUESTList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDRHREQUEST == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.RHREQUEST.IDRHREQUEST.FieldName, " = " + UsrCfg.InternoAtisNames.RHREQUEST.NAME_TABLE + "." + UsrCfg.InternoAtisNames.RHREQUEST.IDRHREQUEST.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDRHREQUEST = " IN (" + StrIDRHREQUEST + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.RHREQUEST.IDRHREQUEST.FieldName, StrIDRHREQUEST, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //Param.AddInt32(UsrCfg.InternoDataLinkNames.RHREQUEST.IDRHREQUEST.FieldName, IDRHREQUEST, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {
        var DS_RHREQUEST = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "RHREQUEST_GET", Param.ToBytes());
        ResErr = DS_RHREQUEST.ResErr;
        if (ResErr.NotError) {
            if (DS_RHREQUEST.DataSet.RecordCount > 0) {
                DS_RHREQUEST.DataSet.First();
                while (!(DS_RHREQUEST.DataSet.Eof)) {
                    var RHREQUEST = new Persistence.RemoteHelp.Properties.TRHREQUEST();
                    RHREQUEST.IDRHREQUEST = DS_RHREQUEST.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHREQUEST.IDRHREQUEST.FieldName).asInt32();
                    //Other
                    RHREQUEST.CONNECTBYHOST = DS_RHREQUEST.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHREQUEST.CONNECTBYHOST.FieldName).asBoolean();
                    RHREQUEST.DONOTASKFORPERMISSION = DS_RHREQUEST.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHREQUEST.DONOTASKFORPERMISSION.FieldName).asBoolean();
                    RHREQUEST.EVENTDATE = DS_RHREQUEST.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHREQUEST.EVENTDATE.FieldName).asDateTime();
                    RHREQUEST.IDATROLE = DS_RHREQUEST.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHREQUEST.IDATROLE.FieldName).asInt32();
                    RHREQUEST.IDCMDBCI = DS_RHREQUEST.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHREQUEST.IDCMDBCI.FieldName).asInt32();
                    RHREQUEST.IDCMDBUSER = DS_RHREQUEST.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHREQUEST.IDCMDBUSER.FieldName).asInt32();
                    RHREQUEST.INTERNETCONNECTION = DS_RHREQUEST.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHREQUEST.INTERNETCONNECTION.FieldName).asBoolean();
                    RHREQUEST.JUSTSEE = DS_RHREQUEST.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHREQUEST.JUSTSEE.FieldName).asBoolean();
                    RHREQUEST.LOCKKEYBOARD = DS_RHREQUEST.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHREQUEST.LOCKKEYBOARD.FieldName).asBoolean();
                    RHREQUEST.LOWBANDWIDTH = DS_RHREQUEST.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHREQUEST.LOWBANDWIDTH.FieldName).asBoolean();
                    RHREQUEST.RECORDSESSION = DS_RHREQUEST.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHREQUEST.RECORDSESSION.FieldName).asBoolean();
                    RHREQUEST.REMOTEHELPFUNTION = Persistence.RemoteHelp.Properties.TRemoteHelpFuntion.GetEnum(DS_RHREQUEST.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHREQUEST.REMOTEHELPFUNTION.FieldName).asInt32());
                    RHREQUEST.REQUEST_STATUS = Persistence.RemoteHelp.Properties.TREQUEST_STATUS.GetEnum(DS_RHREQUEST.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHREQUEST.REQUEST_STATUS.FieldName).asInt32());
                    RHREQUEST.IDRHMODULE_EXECUTE = Persistence.RemoteHelp.Properties.TRHMODULE.GetEnum(DS_RHREQUEST.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHREQUEST.IDRHMODULE_EXECUTE.FieldName).asInt32());
                    RHREQUEST.IDRHMODULE_CREATE = Persistence.RemoteHelp.Properties.TRHMODULE.GetEnum(DS_RHREQUEST.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHREQUEST.IDRHMODULE_CREATE.FieldName).asInt32());
                    RHREQUEST.IDRHTYPEEXECUTE = Persistence.RemoteHelp.Properties.TRHTYPEEXECUTE.GetEnum(DS_RHREQUEST.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHREQUEST.IDRHTYPEEXECUTE.FieldName).asInt32());






                    var Agrega = true;

                    //if (DATALINK) {
                    //    Agrega = !UsrCfg.Properties.CloseOldRequests;
                    //    if (UsrCfg.Properties.CloseOldRequests) {
                    //        RHREQUEST.REQUEST_STATUS = this.TREQUEST_STATUS._CANCELLED;
                    //        RHREQUEST_UPD(RHREQUEST);
                    //    }
                    //}
                    if (Agrega) {
                        this.RHREQUESTList.push(RHREQUEST);
                    }
                    DS_RHREQUEST.DataSet.Next();
                }
            }
            else {
                DS_RHREQUEST.ResErr.NotError = false;
                DS_RHREQUEST.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHREQUEST_GETID = function (RHREQUEST) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddBoolean(UsrCfg.InternoAtisNames.RHREQUEST.CONNECTBYHOST.FieldName, RHREQUEST.CONNECTBYHOST, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddBoolean(UsrCfg.InternoAtisNames.RHREQUEST.DONOTASKFORPERMISSION.FieldName, RHREQUEST.DONOTASKFORPERMISSION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddDateTime(UsrCfg.InternoAtisNames.RHREQUEST.EVENTDATE.FieldName, RHREQUEST.EVENTDATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHREQUEST.IDATROLE.FieldName, RHREQUEST.IDATROLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHREQUEST.IDCMDBCI.FieldName, RHREQUEST.IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHREQUEST.IDCMDBUSER.FieldName, RHREQUEST.IDCMDBUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.RHREQUEST.INTERNETCONNECTION.FieldName, RHREQUEST.INTERNETCONNECTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddBoolean(UsrCfg.InternoAtisNames.RHREQUEST.JUSTSEE.FieldName, RHREQUEST.JUSTSEE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddBoolean(UsrCfg.InternoAtisNames.RHREQUEST.LOCKKEYBOARD.FieldName, RHREQUEST.LOCKKEYBOARD, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddBoolean(UsrCfg.InternoAtisNames.RHREQUEST.LOWBANDWIDTH.FieldName, RHREQUEST.LOWBANDWIDTH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddBoolean(UsrCfg.InternoAtisNames.RHREQUEST.RECORDSESSION.FieldName, RHREQUEST.RECORDSESSION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHREQUEST.REMOTEHELPFUNTION.FieldName, RHREQUEST.REMOTEHELPFUNTION.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHREQUEST.REQUEST_STATUS.FieldName, RHREQUEST.REQUEST_STATUS.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHREQUEST.IDRHMODULE_EXECUTE.FieldName, RHREQUEST.IDRHMODULE_EXECUTE.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHREQUEST.IDRHMODULE_CREATE.FieldName, RHREQUEST.IDRHMODULE_CREATE.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHREQUEST.IDRHTYPEEXECUTE.FieldName, RHREQUEST.IDRHTYPEEXECUTE.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);



        var DS_RHREQUEST = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "RHREQUEST_GETID", Param.ToBytes());
        ResErr = DS_RHREQUEST.ResErr;
        if (ResErr.NotError) {
            if (DS_RHREQUEST.DataSet.RecordCount > 0) {
                DS_RHREQUEST.DataSet.First();
                RHREQUEST.IDRHREQUEST = DS_RHREQUEST.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHREQUEST.IDRHREQUEST.FieldName).asInt32();
            }
            else {
                DS_RHREQUEST.ResErr.NotError = false;
                DS_RHREQUEST.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHREQUEST_ADD = function (RHREQUEST) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddText(UsrCfg.InternoAtisNames.RHREQUEST.AUX.FieldName, RHREQUEST.AUX, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.RHREQUEST.CONNECTBYHOST.FieldName, RHREQUEST.CONNECTBYHOST, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.RHREQUEST.DONOTASKFORPERMISSION.FieldName, RHREQUEST.DONOTASKFORPERMISSION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoAtisNames.RHREQUEST.EVENTDATE.FieldName, RHREQUEST.EVENTDATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHREQUEST.IDATROLE.FieldName, RHREQUEST.IDATROLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHREQUEST.IDCMDBCI.FieldName, RHREQUEST.IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHREQUEST.IDCMDBUSER.FieldName, RHREQUEST.IDCMDBUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.RHREQUEST.INTERNETCONNECTION.FieldName, RHREQUEST.INTERNETCONNECTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.RHREQUEST.JUSTSEE.FieldName, RHREQUEST.JUSTSEE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.RHREQUEST.LOCKKEYBOARD.FieldName, RHREQUEST.LOCKKEYBOARD, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.RHREQUEST.LOWBANDWIDTH.FieldName, RHREQUEST.LOWBANDWIDTH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.RHREQUEST.RECORDSESSION.FieldName, RHREQUEST.RECORDSESSION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHREQUEST.REMOTEHELPFUNTION.FieldName, RHREQUEST.REMOTEHELPFUNTION.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHREQUEST.REQUEST_STATUS.FieldName, RHREQUEST.REQUEST_STATUS.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHREQUEST.IDRHMODULE_EXECUTE.FieldName, RHREQUEST.IDRHMODULE_EXECUTE.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHREQUEST.IDRHMODULE_CREATE.FieldName, RHREQUEST.IDRHMODULE_CREATE.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHREQUEST.IDRHTYPEEXECUTE.FieldName, RHREQUEST.IDRHTYPEEXECUTE.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHREQUEST_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.RemoteHelp.Methods.RHREQUEST_GETID(RHREQUEST);
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHREQUEST_UPD = function (RHREQUEST) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHREQUEST.IDRHREQUEST.FieldName, RHREQUEST.IDRHREQUEST, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHREQUEST.REQUEST_STATUS.FieldName, parseInt(RHREQUEST.REQUEST_STATUS), SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHREQUEST_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}


//CJRC_27072018
Persistence.RemoteHelp.Methods.RHREQUEST_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.RemoteHelp.Methods.RHREQUEST_DELIDRHREQUEST(Param/* IDRHREQUEST*/);
    }
    else {
        Res = Persistence.RemoteHelp.Methods.RHREQUEST_DELREQUEST_STATUS(Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.RHREQUEST_DELIDRHREQUEST = function (StrIDRHREQUEST/* IDRHREQUEST*/) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        if (StrIDRHREQUEST == "") {
            Param.AddUnknown(UsrCfg.InternoAtisNames.RHREQUEST.IDRHREQUEST.FieldName, " = " + UsrCfg.InternoAtisNames.RHREQUEST.NAME_TABLE + "." + UsrCfg.InternoAtisNames.RHREQUEST.IDRHREQUEST.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else {
            StrIDRHREQUEST = " IN (" + StrIDRHREQUEST + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.RHREQUEST.IDRHREQUEST.FieldName, StrIDRHREQUEST, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }
        //Param.AddInt32(UsrCfg.InternoAtisNames.RHREQUEST.IDRHREQUEST.FieldName, IDRHREQUEST, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHREQUEST_DEL", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHREQUEST_DELREQUEST_STATUS = function (REQUEST_STATUS) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHREQUEST.REQUEST_STATUS.FieldName, parseInt(REQUEST_STATUS), SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHREQUEST_DEL", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//**********************   METHODS   ********************************************************************************************

Persistence.RemoteHelp.Methods.RHREQUEST_ListSetID = function (RHREQUESTList, IDRHREQUEST) {
    for (var i = 0; i < RHREQUESTList.length; i++) {

        if (IDRHREQUEST == RHREQUESTList[i].IDRHREQUEST)
            return (RHREQUESTList[i]);

    }
    var UnRHREQUEST = new Persistence.RemoteHelp.Properties.TRHREQUEST();
    UnRHREQUEST.IDRHREQUEST = IDRHREQUEST;
    return (UnRHREQUEST);
}
Persistence.RemoteHelp.Methods.RHREQUEST_ListAdd = function (RHREQUESTList, RHREQUEST) {
    var i = Persistence.RemoteHelp.Methods.RHREQUEST_ListGetIndex(RHREQUESTList, RHREQUEST);
    if (i == -1) RHREQUESTList.push(RHREQUEST);
    else {
        RHREQUESTList[i].AUX = RHREQUEST.AUX;
        RHREQUESTList[i].CONNECTBYHOST = RHREQUEST.CONNECTBYHOST;
        RHREQUESTList[i].DONOTASKFORPERMISSION = RHREQUEST.DONOTASKFORPERMISSION;
        RHREQUESTList[i].EVENTDATE = RHREQUEST.EVENTDATE;
        RHREQUESTList[i].IDATROLE = RHREQUEST.IDATROLE;
        RHREQUESTList[i].IDCMDBCI = RHREQUEST.IDCMDBCI;
        RHREQUESTList[i].IDCMDBUSER = RHREQUEST.IDCMDBUSER;
        RHREQUESTList[i].IDRHREQUEST = RHREQUEST.IDRHREQUEST;
        RHREQUESTList[i].INTERNETCONNECTION = RHREQUEST.INTERNETCONNECTION;
        RHREQUESTList[i].JUSTSEE = RHREQUEST.JUSTSEE;
        RHREQUESTList[i].LOCKKEYBOARD = RHREQUEST.LOCKKEYBOARD;
        RHREQUESTList[i].LOWBANDWIDTH = RHREQUEST.LOWBANDWIDTH;
        RHREQUESTList[i].RECORDSESSION = RHREQUEST.RECORDSESSION;
        RHREQUESTList[i].REMOTEHELPFUNTION = RHREQUEST.REMOTEHELPFUNTION;
        RHREQUESTList[i].REQUEST_STATUS = RHREQUEST.REQUEST_STATUS;
        RHREQUESTList[i].IDRHMODULE_EXECUTE = RHREQUEST.IDRHMODULE_EXECUTE;
        RHREQUESTList[i].IDRHMODULE_CREATE = RHREQUEST.IDRHMODULE_CREATE;
        RHREQUESTList[i].IDRHTYPEEXECUTE = RHREQUEST.IDRHTYPEEXECUTE;

    }
}
Persistence.RemoteHelp.Methods.RHREQUEST_Copy = function (RHREQUEST_Source, RHREQUEST) {
    RHREQUEST.AUX = RHREQUEST_Source.AUX;
    RHREQUEST.CONNECTBYHOST = RHREQUEST_Source.CONNECTBYHOST;
    RHREQUEST.DONOTASKFORPERMISSION = RHREQUEST_Source.DONOTASKFORPERMISSION;
    RHREQUEST.EVENTDATE = RHREQUEST_Source.EVENTDATE;
    RHREQUEST.IDATROLE = RHREQUEST_Source.IDATROLE;
    RHREQUEST.IDCMDBCI = RHREQUEST_Source.IDCMDBCI;
    RHREQUEST.IDCMDBUSER = RHREQUEST_Source.IDCMDBUSER;
    RHREQUEST.IDRHREQUEST = RHREQUEST_Source.IDRHREQUEST;
    RHREQUEST.INTERNETCONNECTION = RHREQUEST_Source.INTERNETCONNECTION;
    RHREQUEST.JUSTSEE = RHREQUEST_Source.JUSTSEE;
    RHREQUEST.LOCKKEYBOARD = RHREQUEST_Source.LOCKKEYBOARD;
    RHREQUEST.LOWBANDWIDTH = RHREQUEST_Source.LOWBANDWIDTH;
    RHREQUEST.RECORDSESSION = RHREQUEST_Source.RECORDSESSION;
    RHREQUEST.REMOTEHELPFUNTION = RHREQUEST_Source.REMOTEHELPFUNTION;
    RHREQUEST.REQUEST_STATUS = RHREQUEST_Source.REQUEST_STATUS;
    RHREQUEST.IDRHMODULE_EXECUTE = RHREQUEST_Source.IDRHMODULE_EXECUTE;
    RHREQUEST.IDRHMODULE_CREATE = RHREQUEST_Source.IDRHMODULE_CREATE;
    RHREQUEST.IDRHTYPEEXECUTE = RHREQUEST_Source.IDRHTYPEEXECUTE;
}

//CJRC_26072018
Persistence.RemoteHelp.Methods.RHREQUEST_ListGetIndex = function (RHREQUESTList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.RemoteHelp.Methods.RHREQUEST_ListGetIndexIDRHREQUEST(RHREQUESTList, Param);

    }
    else {
        Res = Persistence.RemoteHelp.Methods.RHREQUEST_ListGetIndexRHREQUEST(RHREQUESTList, Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.RHREQUEST_ListGetIndexRHREQUEST = function (RHREQUESTList, RHREQUEST) {
    for (var i = 0; i < RHREQUESTList.length; i++) {
        if (RHREQUEST.IDRHREQUEST == RHREQUESTList[i].IDRHREQUEST)
            return (i);
    }
    return (-1);
}
Persistence.RemoteHelp.Methods.RHREQUEST_ListGetIndexIDRHREQUEST = function (RHREQUESTList, IDRHREQUEST) {
    for (var i = 0; i < RHREQUESTList.length; i++) {
        if (IDRHREQUEST == RHREQUESTList[i].IDRHREQUEST)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.RemoteHelp.Methods.RHREQUESTListtoStr = function (RHREQUESTList) {
    var Res = Persistence.RemoteHelp.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(RHREQUESTList.length, Longitud, Texto);
        for (Counter = 0; Counter < RHREQUESTList.length; Counter++) {
            var UnRHREQUEST = RHREQUESTList[Counter];
            UnRHREQUEST.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.RemoteHelp.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;

}
Persistence.RemoteHelp.Methods.StrtoRHREQUEST = function (ProtocoloStr, RHREQUESTList) {
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        RHREQUESTList.length = 0;
        if (Persistence.RemoteHelp.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnRHREQUEST = new Persistence.Demo.Properties.TRHREQUEST(); //Mode new row
                UnRHREQUEST.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.RHREQUEST_ListAdd(RHREQUESTList, UnRHREQUEST);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}

//Socket Function 
Persistence.RemoteHelp.Methods.RHREQUESTListToByte = function (RHREQUESTList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, RHREQUESTList.length - idx);
    for (var i = idx; i < RHREQUESTList.length; i++) {
        RHREQUESTList[i].ToByte(MemStream);
    }
}
Persistence.RemoteHelp.Methods.ByteToRHREQUESTList = function (RHREQUESTList, MemStream) {
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (var i = 0; i < Count; i++) {
        var UnRHREQUEST = new Persistence.RemoteHelp.Properties.TRHREQUEST();
        UnRHREQUEST.ByteTo(MemStream);
        Persistence.RemoteHelp.Methods.RHREQUEST_ListAdd(RHREQUESTList, UnRHREQUEST);
    }
}