﻿//TreduceDB
//Persistence.RemoteHelp.Properties
//Persistence.RemoteHelp.Methods

Persistence.RemoteHelp.Properties.TVPRO = function () {

    this.IDCPU = "";
    this.IDVPRO = 0;
    this.VPROCOMMENT = "";
    this.VPROIP = "";
    this.VPRONAME = "";

    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.IDCPU);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDVPRO);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.VPROCOMMENT);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.VPROIP);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.VPRONAME);
    }
    this.ByteTo = function (MemStream) {
        this.IDCPU = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDVPRO = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.VPROCOMMENT = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.VPROIP = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.VPRONAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteStr(this.IDCPU, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDVPRO, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.VPROCOMMENT, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.VPROIP, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.VPRONAME, Longitud, Texto);
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDCPU = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.IDVPRO = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.VPROCOMMENT = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.VPROIP = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.VPRONAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
    }

}

//**********************   METHODS server  ********************************************************************************************

Persistence.RemoteHelp.Methods.VPRO_Fill = function (VPROList, StrIDCPU) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    VPROList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDCPU == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.VPRO.IDCPU.FieldName, " = " + UsrCfg.InternoAtisNames.VPRO.NAME_TABLE + "." + UsrCfg.InternoAtisNames.VPRO.IDCPU.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDCPU = " IN (" + StrIDCPU + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.VPRO.IDCPU.FieldName, StrIDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    try {
        var DS_VPRO = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "VPRO_GET", Param.ToBytes());
        ResErr = DS_VPRO.ResErr;
        if (ResErr.NotError) {
            if (DS_VPRO.DataSet.RecordCount > 0) {
                DS_VPRO.DataSet.First();
                while (!(DS_VPRO.DataSet.Eof)) {
                    var VPRO = new Persistence.RemoteHelp.Properties.TVPRO();
                    VPRO.IDVPRO = DS_VPRO.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.VPRO.IDVPRO.FieldName).asInt32();
                    //Other
                    VPRO.IDCPU = DS_VPRO.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.VPRO.IDCPU.FieldName).asString();
                    VPRO.VPROCOMMENT = DS_VPRO.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.VPRO.VPROCOMMENT.FieldName).asString();
                    VPRO.VPROIP = DS_VPRO.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.VPRO.VPROIP.FieldName).asString();
                    VPRO.VPRONAME = DS_VPRO.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.VPRO.VPRONAME.FieldName).asString();
                    VPROList.push(VPRO);

                    DS_VPRO.DataSet.Next();
                }
            }
            else {
                DS_VPRO.ResErr.NotError = false;
                DS_VPRO.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.VPRO_GETID = function (VPRO) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.VPRO.IDVPRO.FieldName, VPRO.IDVPRO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.VPRO.IDCPU.FieldName, VPRO.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.VPRO.VPROCOMMENT.FieldName, VPRO.VPROCOMMENT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.VPRO.VPROIP.FieldName, VPRO.VPROIP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.VPRO.VPRONAME.FieldName, VPRO.VPRONAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        var DS_VPRO = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "VPRO_GETID", Param.ToBytes());
        ResErr = DS_VPRO.ResErr;
        if (ResErr.NotError) {
            if (DS_VPRO.DataSet.RecordCount > 0) {
                DS_VPRO.DataSet.First();
                VPRO.IDVPRO = DS_VPRO.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.VPRO.IDVPRO.FieldName).asInt32();
                //Other
                VPRO.IDCPU = DS_VPRO.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.VPRO.IDCPU.FieldName).asString();
                VPRO.VPROCOMMENT = DS_VPRO.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.VPRO.VPROCOMMENT.FieldName).asString();
                VPRO.VPROIP = DS_VPRO.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.VPRO.VPROIP.FieldName).asString();
                VPRO.VPRONAME = DS_VPRO.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.VPRO.VPRONAME.FieldName).asString();
            }
            else {
                DS_VPRO.ResErr.NotError = false;
                DS_VPRO.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.VPRO_ADD = function (VPRO) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.VPRO.IDVPRO.FieldName, VPRO.IDVPRO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.VPRO.IDCPU.FieldName, VPRO.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.VPRO.VPROCOMMENT.FieldName, VPRO.VPROCOMMENT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.VPRO.VPROIP.FieldName, VPRO.VPROIP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.VPRO.VPRONAME.FieldName, VPRO.VPRONAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "VPRO_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.RemoteHelp.Methods.VPRO_GETID(VPRO);
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.VPRO_UPD = function (VPRO) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.VPRO.IDVPRO.FieldName, VPRO.IDVPRO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.VPRO.IDCPU.FieldName, VPRO.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.VPRO.VPROCOMMENT.FieldName, VPRO.VPROCOMMENT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.VPRO.VPROIP.FieldName, VPRO.VPROIP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.VPRO.VPRONAME.FieldName, VPRO.VPRONAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "VPRO_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.RemoteHelp.Methods.VPRO_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.RemoteHelp.Methods.VPRO_DELIDVPRO(/*String StrIDVPRO*/Param);
    }
    else {
        Res = Persistence.RemoteHelp.Methods.VPRO_DELVPRO(Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.VPRO_DELIDVPRO = function (/*String StrIDVPRO*/IDVPRO) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDVPRO == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.VPRO.IDVPRO.FieldName, " = " + UsrCfg.InternoAtisNames.VPRO.NAME_TABLE + "." + UsrCfg.InternoAtisNames.VPRO.IDVPRO.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDVPRO = " IN (" + StrIDVPRO + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.VPRO.IDVPRO.FieldName, StrIDVPRO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.VPRO.IDVPRO.FieldName, IDVPRO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "VPRO_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.VPRO_DELVPRO = function (VPRO) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.VPRO.IDVPRO.FieldName, VPRO.IDVPRO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.VPRO.IDCPU.FieldName, VPRO.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.VPRO.VPROCOMMENT.FieldName, VPRO.VPROCOMMENT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.VPRO.VPROIP.FieldName, VPRO.VPROIP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.VPRO.VPRONAME.FieldName, VPRO.VPRONAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "VPRO_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}



//**********************   METHODS   ********************************************************************************************

Persistence.RemoteHelp.Methods.VPRO_ListSetID = function (VPROList, IDVPRO) {
    for (var i = 0; i < VPROList.length; i++) {
        if (IDVPRO == VPROList[i].IDVPRO)
            return (VPROList[i]);

    }
    var UnVPRO = new Persistence.RemoteHelp.Properties.TVPRO();
    UnVPRO.IDVPRO = IDVPRO;
    return (UnVPRO);
}
Persistence.RemoteHelp.Methods.VPRO_ListAdd = function (VPROList, VPRO) {
    var i = Persistence.RemoteHelp.Methods.VPRO_ListGetIndex(VPROList, VPRO);
    if (i == -1) VPROList.push(VPRO);
    else {
        VPROList[i].IDCPU = VPRO.IDCPU;
        VPROList[i].IDVPRO = VPRO.IDVPRO;
        VPROList[i].VPROCOMMENT = VPRO.VPROCOMMENT;
        VPROList[i].VPROIP = VPRO.VPROIP;
        VPROList[i].VPRONAME = VPRO.VPRONAME;
    }
}

//CJRC_26072018
Persistence.RemoteHelp.Methods.VPRO_ListGetIndex = function (VPROList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.RemoteHelp.Methods.VPRO_ListGetIndexIDVPRO(VPROList, Param);

    }
    else {
        Res = Persistence.RemoteHelp.Methods.VPRO_ListGetIndexVPRO(VPROList, Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.VPRO_ListGetIndexVPRO = function (VPROList, VPRO) {
    for (var i = 0; i < VPROList.length; i++) {
        if (VPRO.IDVPRO == VPROList[i].IDVPRO)
            return (i);
    }
    return (-1);
}
Persistence.RemoteHelp.Methods.VPRO_ListGetIndexIDVPRO = function (VPROList, IDVPRO) {
    for (var i = 0; i < VPROList.length; i++) {
        if (IDVPRO == VPROList[i].IDVPRO)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.RemoteHelp.Methods.VPROListtoStr = function (VPROList) {
    var Res = Persistence.RemoteHelp.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(VPROList.length, Longitud, Texto);
        for (Counter = 0; Counter < VPROList.length; Counter++) {
            var UnVPRO = VPROList[Counter];
            UnVPRO.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.RemoteHelp.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.RemoteHelp.Methods.StrtoVPRO = function (ProtocoloStr, VPROList) {
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        VPROList.length = 0;
        if (Persistence.RemoteHelp.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnVPRO = new Persistence.Demo.Properties.TVPRO(); //Mode new row
                UnVPRO.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.VPRO_ListAdd(VPROList, UnVPRO);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}

//Socket Function 
Persistence.RemoteHelp.Methods.VPROListToByte = function (VPROList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, VPROList.length - idx);
    for (var i = idx; i < VPROList.length; i++) {
        VPROList[i].ToByte(MemStream);
    }
}
Persistence.RemoteHelp.Methods.ByteToVPROList = function (VPROList, MemStream) {
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (var i = 0; i < Count; i++) {
        var UnVPRO = new Persistence.RemoteHelp.Properties.TVPRO();
        UnVPRO.ByteTo(MemStream);
        Persistence.RemoteHelp.Methods.VPRO_ListAdd(VPROList, UnVPRO);
    }
}
