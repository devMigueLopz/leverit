﻿//TreduceDB
//Persistence.RemoteHelp.Properties
//Persistence.RemoteHelp.Methods

Persistence.RemoteHelp.Properties.TRHGROUPPL_POLICY = function () {
    this.IDRHGROUPPL_POLICY = 0;
    this.POLICY_NAME = "";
    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDRHGROUPPL_POLICY);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.POLICY_NAME);
    }
    this.ByteTo = function (MemStream) {
        this.IDRHGROUPPL_POLICY = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.POLICY_NAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDRHGROUPPL_POLICY, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.POLICY_NAME, Longitud, Texto);
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDRHGROUPPL_POLICY = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.POLICY_NAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
    }
}

//**********************   METHODS server  ********************************************************************************************

Persistence.RemoteHelp.Methods.RHGROUPPL_POLICY_Fill = function (RHGROUPPL_POLICYList) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    RHGROUPPL_POLICYList.length = 0;
    try {
        var DS_RHGROUPPL_POLICY = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "RHGROUPPL_POLICY_GET", new Array());
        ResErr = DS_RHGROUPPL_POLICY.ResErr;
        if (ResErr.NotError) {
            if (DS_RHGROUPPL_POLICY.DataSet.RecordCount > 0) {
                DS_RHGROUPPL_POLICY.DataSet.First();
                while (!(DS_RHGROUPPL_POLICY.DataSet.Eof)) {
                    var RHGROUPPL_POLICY = new Persistence.RemoteHelp.Properties.TRHGROUPPL_POLICY();
                    RHGROUPPL_POLICY.IDRHGROUPPL_POLICY = DS_RHGROUPPL_POLICY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL_POLICY.IDRHGROUPPL_POLICY.FieldName).asInt32();
                    RHGROUPPL_POLICY.POLICY_NAME = DS_RHGROUPPL_POLICY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL_POLICY.POLICY_NAME.FieldName).asString();
                    RHGROUPPL_POLICYList.push(RHGROUPPL_POLICY);

                    DS_RHGROUPPL_POLICY.DataSet.Next();
                }
            }
            else {
                DS_RHGROUPPL_POLICY.ResErr.NotError = false;
                DS_RHGROUPPL_POLICY.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPPL_POLICY_GETID = function (RHGROUPPL_POLICY) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPPL_POLICY.POLICY_NAME.FieldName, RHGROUPPL_POLICY.POLICY_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var DS_RHGROUPPL_POLICY = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "RHGROUPPL_POLICY_GETID", Param.ToBytes());
        ResErr = DS_RHGROUPPL_POLICY.ResErr;
        if (ResErr.NotError) {
            if (DS_RHGROUPPL_POLICY.DataSet.RecordCount > 0) {
                DS_RHGROUPPL_POLICY.DataSet.First();
                RHGROUPPL_POLICY.IDRHGROUPPL_POLICY = DS_RHGROUPPL_POLICY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL_POLICY.IDRHGROUPPL_POLICY.FieldName).asInt32();
                RHGROUPPL_POLICY.POLICY_NAME = DS_RHGROUPPL_POLICY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL_POLICY.POLICY_NAME.FieldName).asString();
            }
            else {
                DS_RHGROUPPL_POLICY.ResErr.NotError = false;
                DS_RHGROUPPL_POLICY.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPPL_POLICY_GETBYID = function (RHGROUPPL_POLICY, Id) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPL_POLICY.IDRHGROUPPL_POLICY.FieldName, Id, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var DS_RHGROUPPL_POLICY = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "RHGROUPPL_POLICY_GETBYID", Param.ToBytes());
        ResErr = DS_RHGROUPPL_POLICY.ResErr;
        if (ResErr.NotError) {
            if (DS_RHGROUPPL_POLICY.DataSet.RecordCount > 0) {
                DS_RHGROUPPL_POLICY.DataSet.First();
                RHGROUPPL_POLICY.IDRHGROUPPL_POLICY = DS_RHGROUPPL_POLICY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL_POLICY.IDRHGROUPPL_POLICY.FieldName).asInt32();
                RHGROUPPL_POLICY.POLICY_NAME = DS_RHGROUPPL_POLICY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL_POLICY.POLICY_NAME.FieldName).asString();
            }
            else {
                DS_RHGROUPPL_POLICY.ResErr.NotError = false;
                DS_RHGROUPPL_POLICY.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

Persistence.RemoteHelp.Methods.RHGROUPPL_POLICY_ADD = function (RHGROUPPL_POLICY) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPL_POLICY.IDRHGROUPPL_POLICY.FieldName, RHGROUPPL_POLICY.IDRHGROUPPL_POLICY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPPL_POLICY.POLICY_NAME.FieldName, RHGROUPPL_POLICY.POLICY_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHGROUPPL_POLICY_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.RemoteHelp.Methods.RHGROUPPL_POLICY_GETID(RHGROUPPL_POLICY);
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPPL_POLICY_UPD = function (RHGROUPPL_POLICY) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPL_POLICY.IDRHGROUPPL_POLICY.FieldName, RHGROUPPL_POLICY.IDRHGROUPPL_POLICY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPPL_POLICY.POLICY_NAME.FieldName, RHGROUPPL_POLICY.POLICY_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHGROUPPL_POLICY_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.RemoteHelp.Methods.RHGROUPPL_POLICY_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.RemoteHelp.Methods.RHGROUPPL_POLICY_DELIDRHGROUPPL_POLICY(Param);
    }
    else {
        Res = Persistence.RemoteHelp.Methods.RHGROUPPL_POLICY_DELRHGROUPPL_POLICY(Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.RHGROUPPL_POLICY_DELIDRHGROUPPL_POLICY = function (IDRHGROUPPL_POLICY) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPL_POLICY.IDRHGROUPPL_POLICY.FieldName, IDRHGROUPPL_POLICY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHGROUPPL_POLICY_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPPL_POLICY_DELRHGROUPPL_POLICY = function (RHGROUPPL_POLICY) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPL_POLICY.IDRHGROUPPL_POLICY.FieldName, RHGROUPPL_POLICY.IDRHGROUPPL_POLICY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPPL_POLICY.POLICY_NAME.FieldName, RHGROUPPL_POLICY.POLICY_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHGROUPPL_POLICY_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}


//**********************   METHODS   ********************************************************************************************


//List Function 
Persistence.RemoteHelp.Methods.RHGROUPPL_POLICY_ListSetID = function (RHGROUPPL_POLICYList, IDRHGROUPPL_POLICY) {
    for (var i = 0; i < RHGROUPPL_POLICYList.length; i++) {

        if (IDRHGROUPPL_POLICY == RHGROUPPL_POLICYList[i].IDRHGROUPPL_POLICY)
            return (RHGROUPPL_POLICYList[i]);

    }
    var UnRHGROUPPL_POLICY = new Persistence.RemoteHelp.Properties.TRHGROUPPL_POLICY();
    UnRHGROUPPL_POLICY.IDRHGROUPPL_POLICY = IDRHGROUPPL_POLICY;
    return (UnRHGROUPPL_POLICY);
}
Persistence.RemoteHelp.Methods.RHGROUPPL_POLICY_ListAdd = function (RHGROUPPL_POLICYList, RHGROUPPL_POLICY) {
    var i = Persistence.RemoteHelp.Methods.RHGROUPPL_POLICY_ListGetIndex(RHGROUPPL_POLICYList, RHGROUPPL_POLICY);
    if (i == -1) RHGROUPPL_POLICYList.push(RHGROUPPL_POLICY);
    else {
        RHGROUPPL_POLICYList[i].IDRHGROUPPL_POLICY = RHGROUPPL_POLICY.IDRHGROUPPL_POLICY;
        RHGROUPPL_POLICYList[i].POLICY_NAME = RHGROUPPL_POLICY.POLICY_NAME;
    }
}

//CJRC_26072018
Persistence.RemoteHelp.Methods.RHGROUPPL_POLICY_ListGetIndex = function (RHGROUPPL_POLICYList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.RemoteHelp.Methods.RHGROUPPL_POLICY_ListGetIndexIDRHGROUPPL_POLICY(RHGROUPPL_POLICYList, Param);

    }
    else {
        Res = Persistence.RemoteHelp.Methods.RHGROUPPL_POLICY_ListGetIndexRHGROUPPL_POLICY(RHGROUPPL_POLICYList, Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.RHGROUPPL_POLICY_ListGetIndexRHGROUPPL_POLICY = function (RHGROUPPL_POLICYList, RHGROUPPL_POLICY) {
    for (var i = 0; i < RHGROUPPL_POLICYList.length; i++) {
        if (RHGROUPPL_POLICY.IDRHGROUPPL_POLICY == RHGROUPPL_POLICYList[i].IDRHGROUPPL_POLICY)
            return (i);
    }
    return (-1);
}
Persistence.RemoteHelp.Methods.RHGROUPPL_POLICY_ListGetIndexIDRHGROUPPL_POLICY = function (RHGROUPPL_POLICYList, IDRHGROUPPL_POLICY) {
    for (var i = 0; i < RHGROUPPL_POLICYList.length; i++) {
        if (IDRHGROUPPL_POLICY == RHGROUPPL_POLICYList[i].IDRHGROUPPL_POLICY)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.RemoteHelp.Methods.RHGROUPPL_POLICYListtoStr = function (RHGROUPPL_POLICYList) {
    var Res = Persistence.RemoteHelp.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(RHGROUPPL_POLICYList.length, Longitud, Texto);
        for (Counter = 0; Counter < RHGROUPPL_POLICYList.length; Counter++) {
            var UnRHGROUPPL_POLICY = RHGROUPPL_POLICYList[Counter];
            UnRHGROUPPL_POLICY.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.RemoteHelp.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.RemoteHelp.Methods.StrtoRHGROUPPL_POLICY = function (ProtocoloStr, RHGROUPPL_POLICYList) {
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        RHGROUPPL_POLICYList.length = 0;
        if (Persistence.RemoteHelp.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnRHGROUPPL_POLICY = new Persistence.Demo.Properties.TRHGROUPPL_POLICY(); //Mode new row
                UnRHGROUPPL_POLICY.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.RHGROUPPL_POLICY_ListAdd(RHGROUPPL_POLICYList, UnRHGROUPPL_POLICY);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}

//Socket Function 
Persistence.RemoteHelp.Methods.RHGROUPPL_POLICYListToByte = function (RHGROUPPL_POLICYList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, RHGROUPPL_POLICYList.length - idx);
    for (var i = idx; i < RHGROUPPL_POLICYList.length; i++) {
        RHGROUPPL_POLICYList[i].ToByte(MemStream);
    }
}
Persistence.RemoteHelp.Methods.ByteToRHGROUPPL_POLICYList = function (RHGROUPPL_POLICYList, MemStream) {
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (var i = 0; i < Count; i++) {
        var UnRHGROUPPL_POLICY = new Persistence.RemoteHelp.Properties.TRHGROUPPL_POLICY();
        UnRHGROUPPL_POLICY.ByteTo(MemStream);
        Persistence.RemoteHelp.Methods.RHGROUPPL_POLICY_ListAdd(RHGROUPPL_POLICYList, UnRHGROUPPL_POLICY);
    }
}