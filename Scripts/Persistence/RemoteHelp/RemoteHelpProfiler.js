﻿//TreduceDB
//Persistence.RemoteHelp


Persistence.RemoteHelp.TRemoteHelpProfiler = function () {
    this.RHGROUPCRList = new Array();//Persistence.RemoteHelp.Properties.TRHGROUPCR
    this.RHGROUPCRATROLEPERMISSIONList = new Array(); //Persistence.RemoteHelp.Properties.TRHGROUPCRATROLEPERMISSION
    this.RHGROUPCRCPUList = new Array();//Persistence.RemoteHelp.Properties.TRHGROUPCRCPU
    this.RHGROUPCRPERMISSIONList = new Array();//Persistence.RemoteHelp.Properties.TRHGROUPCRPERMISSION
    this.RHGROUPPLList = new Array();//Persistence.RemoteHelp.Properties.TRHGROUPPL
    this.RHGROUPPL_CFGBACKUPList = new Array();//Persistence.RemoteHelp.Properties.TRHGROUPPL_CFGBACKUP
    this.RHGROUPPL_CLI_CONFIGList = new Array();//ersistence.RemoteHelp.Properties.TRHGROUPPL_CLI_CONFIG
    this.RHGROUPPLCPUList = new Array();//Persistence.RemoteHelp.Properties.TRHGROUPPLCPU

    this.RHGROUPPL_POLICYList = new Array();//Persistence.RemoteHelp.Properties.TRHGROUPPL_POLICY
    this.RHGROUPPLATROLEList = new Array();//Persistence.RemoteHelp.Properties.TRHGROUPPLATROLE

    this.IPList = new Array();//Persistence.RemoteHelp.Properties.TIP
    this.AGENTEList = new Array(); //Persistence.RemoteHelp.Properties.TAGENTE       
    this.ESTACION_REDList = new Array();//Persistence.RemoteHelp.Properties.TESTACION_RED
    this.EXTRADATAList = new Array();//Persistence.RemoteHelp.Properties.TEXTRADATA
    this.USERSList = new Array();//Persistence.RemoteHelp.Properties.TUSERS
    this.VPROList = new Array();//<Persistence.RemoteHelp.Properties.TVPRO
    this.WINDOWSList = new Array();//Persistence.RemoteHelp.Properties.TWINDOWS
    this.CPUList = new Array();//Persistence.RemoteHelp.Properties.TCPU
    this.IP_SERVERSList = new Array();//Persistence.RemoteHelp.Properties.TIP_SERVERS



    this.RHREQUESTList = new Array();//<Persistence.RemoteHelp.Properties.TRHREQUEST
    this.RHREQUEST_CPUPLList = new Array();//Persistence.RemoteHelp.Properties.TRHREQUEST_CPUPL
    this.RHREQUEST_CPUCRList = new Array();//Persistence.RemoteHelp.Properties.TRHREQUEST_CPUCR
    this.RHMODULE_FuntionList = new Array();//Persistence.RemoteHelp.Properties.TRHMODULE_Funtion
    this.RHMODULE_ACTIVEList = new Array();//Persistence.RemoteHelp.Properties.TRHMODULE_ACTIVE




    this.Fill = function (UserFunctionsList) {
        //llena las tablas 
        //*****************************
        var RHMODULE_Funtion = new Persistence.RemoteHelp.Properties.TRHMODULE_Funtion();
        RHMODULE_Funtio.RemoteHelpFuntion = Properties.TRemoteHelpFuntion._None;
        RHMODULE_Funtio.RHMODULE_CREATE = Properties.TRHMODULE._Default;
        RHMODULE_Funtio.Enable = false, ATIS = false, ITHelpCenter = false;
        RHMODULE_Funtio.CMDB = false;
        RHMODULE_Funtio.RemoteHelp = false;
        this.RHMODULE_FuntionList.push(RHMODULE_Funtion);
        //*****************************
        RHMODULE_Funtion = new Persistence.RemoteHelp.Properties.TRHMODULE_Funtion();
        RHMODULE_Funtion.RemoteHelpFuntion = Properties.TRemoteHelpFuntion._None;
        RHMODULE_Funtion.RHMODULE_CREATE = Properties.TRHMODULE._Default;
        RHMODULE_Funtion.Enable = false;
        RHMODULE_Funtion.ATIS = false;
        RHMODULE_Funtion.ITHelpCenter = false;
        RHMODULE_Funtion.CMDB = false;
        RHMODULE_Funtion.RemoteHelp = false
        this.RHMODULE_FuntionList.push(RHMODULE_Funtion);

        RHMODULE_Funtion = new Persistence.RemoteHelp.Properties.TRHMODULE_Funtion();
        RHMODULE_Funtion.RemoteHelpFuntion = Properties.TRemoteHelpFuntion._SendMessage;
        RHMODULE_Funtion.RHMODULE_CREATE = Properties.TRHMODULE._Default;
        RHMODULE_Funtion.Enable = false;
        RHMODULE_Funtion.ATIS = false;
        RHMODULE_Funtion.ITHelpCenter = false;
        RHMODULE_Funtion.CMDB = false; RemoteHelp = false
        this.RHMODULE_FuntionList.push(RHMODULE_Funtion);

        RHMODULE_Funtion = new Persistence.RemoteHelp.Properties.TRHMODULE_Funtion();
        RHMODULE_Funtion.RemoteHelpFuntion = Properties.TRemoteHelpFuntion._Chat;
        RHMODULE_Funtion.RHMODULE_CREATE = Properties.TRHMODULE._Default;
        RHMODULE_Funtion.Enable = false;
        RHMODULE_Funtion.ATIS = false;
        RHMODULE_Funtion.ITHelpCenter = false;
        RHMODULE_Funtion.CMDB = false;
        RHMODULE_Funtion.RemoteHelp = false
        this.RHMODULE_FuntionList.push(RHMODULE_Funtion);

        RHMODULE_Funtion = new Persistence.RemoteHelp.Properties.TRHMODULE_Funtion();
        RHMODULE_Funtion.RemoteHelpFuntion = Properties.TRemoteHelpFuntion._RemoteControl;
        RHMODULE_Funtion.RHMODULE_CREATE = Properties.TRHMODULE._Default;
        RHMODULE_Funtion.Enable = false;
        RHMODULE_Funtion.ATIS = false;
        RHMODULE_Funtion.ITHelpCenter = false;
        RHMODULE_Funtion.CMDB = false;
        RHMODULE_Funtion.RemoteHelp = false
        this.RHMODULE_FuntionList.push(RHMODULE_Funtion);

        RHMODULE_Funtion = new Persistence.RemoteHelp.Properties.TRHMODULE_Funtion();
        RHMODULE_Funtion.RemoteHelpFuntion = Properties.TRemoteHelpFuntion._GenerateInventory;
        RHMODULE_Funtion.RHMODULE_CREATE = Properties.TRHMODULE._Default;
        RHMODULE_Funtion.Enable = false;
        RHMODULE_Funtion.ATIS = false;
        RHMODULE_Funtion.ITHelpCenter = false;
        RHMODULE_Funtion.CMDB = false;
        RHMODULE_Funtion.RemoteHelp = false
        this.RHMODULE_FuntionList.push(RHMODULE_Funtion);

        RHMODULE_Funtion = new Persistence.RemoteHelp.Properties.TRHMODULE_Funtion();
        RHMODULE_Funtion.RemoteHelpFuntion = Properties.TRemoteHelpFuntion._UpdateAgent;
        RHMODULE_Funtion.RHMODULE_CREATE = Properties.TRHMODULE._Default;
        RHMODULE_Funtion.Enable = false;
        RHMODULE_Funtion.ATIS = false;
        RHMODULE_Funtion.ITHelpCenter = false;
        RHMODULE_Funtion.CMDB = false;
        RHMODULE_Funtion.RemoteHelp = false
        this.RHMODULE_FuntionList.push(RHMODULE_Funtion);

        RHMODULE_Funtion = new Persistence.RemoteHelp.Properties.TRHMODULE_Funtion();
        RHMODULE_Funtion.RemoteHelpFuntion = Properties.TRemoteHelpFuntion._ChangeID;
        RHMODULE_Funtion.RHMODULE_CREATE = Properties.TRHMODULE._Default;
        RHMODULE_Funtion.Enable = false;
        RHMODULE_Funtion.ATIS = false;
        RHMODULE_Funtion.ITHelpCenter = false;
        RHMODULE_Funtion.CMDB = false;
        RHMODULE_Funtion.RemoteHelp = false
        this.RHMODULE_FuntionList.push(RHMODULE_Funtion);

        RHMODULE_Funtion = new Persistence.RemoteHelp.Properties.TRHMODULE_Funtion();
        RHMODULE_Funtion.RemoteHelpFuntion = Properties.TRemoteHelpFuntion._LookDangerous;
        RHMODULE_Funtion.RHMODULE_CREATE = Properties.TRHMODULE._Default;
        RHMODULE_Funtion.Enable = false;
        RHMODULE_Funtion.ATIS = false;
        RHMODULE_Funtion.ITHelpCenter = false;
        RHMODULE_Funtion.CMDB = false;
        RHMODULE_Funtion.RemoteHelp = false
        this.RHMODULE_FuntionList.push(RHMODULE_Funtion);

        RHMODULE_Funtion = new Persistence.RemoteHelp.Properties.TRHMODULE_Funtion();
        RHMODULE_Funtion.RemoteHelpFuntion = Properties.TRemoteHelpFuntion._SendingFilesandOrders;
        RHMODULE_Funtion.RHMODULE_CREATE = Properties.TRHMODULE._Default;
        RHMODULE_Funtion.Enable = false;
        RHMODULE_Funtion.ATIS = false;
        RHMODULE_Funtion.ITHelpCenter = false;
        RHMODULE_Funtion.CMDB = false;
        RHMODULE_Funtion.RemoteHelp = false
        this.RHMODULE_FuntionList.push(RHMODULE_Funtion);

        RHMODULE_Funtion = new Persistence.RemoteHelp.Properties.TRHMODULE_Funtion();
        RHMODULE_Funtion.RemoteHelpFuntion = Properties.TRemoteHelpFuntion._RunLocally;
        RHMODULE_Funtion.RHMODULE_CREATE = Properties.TRHMODULE._Default;
        RHMODULE_Funtion.Enable = false;
        RHMODULE_Funtion.ATIS = false;
        RHMODULE_Funtion.ITHelpCenter = false;
        RHMODULE_Funtion.CMDB = false;
        RHMODULE_Funtion.RemoteHelp = false
        this.RHMODULE_FuntionList.push(RHMODULE_Funtion);

        RHMODULE_Funtion = new Persistence.RemoteHelp.Properties.TRHMODULE_Funtion();
        RHMODULE_Funtion.RemoteHelpFuntion = Properties.TRemoteHelpFuntion._UpdatePolicies;
        RHMODULE_Funtion.RHMODULE_CREATE = Properties.TRHMODULE._Default;
        RHMODULE_Funtion.Enable = false;
        RHMODULE_Funtion.ATIS = false;
        RHMODULE_Funtion.ITHelpCenter = false;
        RHMODULE_Funtion.CMDB = false;
        RHMODULE_Funtion.RemoteHelp = false
        this.RHMODULE_FuntionList.push(RHMODULE_Funtion);

        RHMODULE_Funtion = new Persistence.RemoteHelp.Properties.TRHMODULE_Funtion();
        RHMODULE_Funtion.RemoteHelpFuntion = Properties.TRemoteHelpFuntion._RestartAllowed;
        RHMODULE_Funtion.RHMODULE_CREATE = Properties.TRHMODULE._Default;
        RHMODULE_Funtion.Enable = false;
        RHMODULE_Funtion.ATIS = false;
        RHMODULE_Funtion.ITHelpCenter = false;
        RHMODULE_Funtion.CMDB = false;
        RHMODULE_Funtion.RemoteHelp = false
        this.RHMODULE_FuntionList.push(RHMODULE_Funtion);

        RHMODULE_Funtion = new Persistence.RemoteHelp.Properties.TRHMODULE_Funtion();
        RHMODULE_Funtion.RemoteHelpFuntion = Properties.TRemoteHelpFuntion._UseBrowser;
        RHMODULE_Funtion.RHMODULE_CREATE = Properties.TRHMODULE._Default;
        RHMODULE_Funtion.Enable = false;
        RHMODULE_Funtion.ATIS = false;
        RHMODULE_Funtion.ITHelpCenter = false;
        RHMODULE_Funtion.CMDB = false;
        RHMODULE_Funtion.RemoteHelp = false
        this.RHMODULE_FuntionList.push(RHMODULE_Funtion);

        RHMODULE_Funtion = new Persistence.RemoteHelp.Properties.TRHMODULE_Funtion();
        RHMODULE_Funtion.RemoteHelpFuntion = Properties.TRemoteHelpFuntion._Survey;
        RHMODULE_Funtion.RHMODULE_CREATE = Properties.TRHMODULE._Default;
        RHMODULE_Funtion.Enable = false;
        RHMODULE_Funtion.ATIS = false;
        RHMODULE_Funtion.ITHelpCenter = false;
        RHMODULE_Funtion.CMDB = false;
        RHMODULE_Funtion.RemoteHelp = false
        this.RHMODULE_FuntionList.push(RHMODULE_Funtion);

        RHMODULE_Funtion = new Persistence.RemoteHelp.Properties.TRHMODULE_Funtion();
        RHMODULE_Funtion.RemoteHelpFuntion = Properties.TRemoteHelpFuntion._PoweronPC;
        RHMODULE_Funtion.RHMODULE_CREATE = Properties.TRHMODULE._Default;
        RHMODULE_Funtion.Enable = false;
        RHMODULE_Funtion.ATIS = false;
        RHMODULE_Funtion.ITHelpCenter = false;
        RHMODULE_Funtion.CMDB = false;
        RHMODULE_Funtion.RemoteHelp = false
        this.RHMODULE_FuntionList.push(RHMODULE_Funtion);

        RHMODULE_Funtion = new Persistence.RemoteHelp.Properties.TRHMODULE_Funtion();
        RHMODULE_Funtion.RemoteHelpFuntion = Properties.TRemoteHelpFuntion._Voice;
        RHMODULE_Funtion.RHMODULE_CREATE = Properties.TRHMODULE._Default;
        RHMODULE_Funtion.Enable = false;
        RHMODULE_Funtion.ATIS = false;
        RHMODULE_Funtion.ITHelpCenter = false;
        RHMODULE_Funtion.CMDB = false;
        RHMODULE_Funtion.RemoteHelp = false
        this.RHMODULE_FuntionList.push(RHMODULE_Funtion);

        RHMODULE_Funtion = new Persistence.RemoteHelp.Properties.TRHMODULE_Funtion();
        RHMODULE_Funtion.RemoteHelpFuntion = Properties.TRemoteHelpFuntion._IntelvProtechnology;
        RHMODULE_Funtion.RHMODULE_CREATE = Properties.TRHMODULE._Default;
        RHMODULE_Funtion.Enable = false;
        RHMODULE_Funtion.ATIS = false;
        RHMODULE_Funtion.ITHelpCenter = false;
        RHMODULE_Funtion.CMDB = false;
        RHMODULE_Funtion.RemoteHelp = false
        this.RHMODULE_FuntionList.push(RHMODULE_Funtion);

        RHMODULE_Funtion = new Persistence.RemoteHelp.Properties.TRHMODULE_Funtion();
        RHMODULE_Funtion.RemoteHelpFuntion = Properties.TRemoteHelpFuntion._DeletePC;
        RHMODULE_Funtion.RHMODULE_CREATE = Properties.TRHMODULE._Default;
        RHMODULE_Funtion.Enable = false;
        RHMODULE_Funtion.ATIS = false;
        RHMODULE_Funtion.ITHelpCenter = false;
        RHMODULE_Funtion.CMDB = false;
        RHMODULE_Funtion.RemoteHelp = false
        this.RHMODULE_FuntionList.push(RHMODULE_Funtion);

        RHMODULE_Funtion = new Persistence.RemoteHelp.Properties.TRHMODULE_Funtion();
        RHMODULE_Funtion.RemoteHelpFuntion = Properties.TRemoteHelpFuntion._SendMessage;
        RHMODULE_Funtion.RHMODULE_CREATE = Properties.TRHMODULE._ATIS;
        RHMODULE_Funtion.Enable = false;
        RHMODULE_Funtion.ATIS = false;
        RHMODULE_Funtion.ITHelpCenter = false;
        RHMODULE_Funtion.CMDB = false;
        RHMODULE_Funtion.RemoteHelp = false
        this.RHMODULE_FuntionList.push(RHMODULE_Funtion);

        RHMODULE_Funtion = new Persistence.RemoteHelp.Properties.TRHMODULE_Funtion();
        RHMODULE_Funtion.RemoteHelpFuntion = Properties.TRemoteHelpFuntion._SendMessage;
        RHMODULE_Funtion.RHMODULE_CREATE = Properties.TRHMODULE._CMDB;
        RHMODULE_Funtion.Enable = false;
        RHMODULE_Funtion.ATIS = false;
        RHMODULE_Funtion.ITHelpCenter = false;
        RHMODULE_Funtion.CMDB = false;
        RHMODULE_Funtion.RemoteHelp = false
        this.RHMODULE_FuntionList.push(RHMODULE_Funtion);

        RHMODULE_Funtion = new Persistence.RemoteHelp.Properties.TRHMODULE_Funtion();
        RHMODULE_Funtion.RemoteHelpFuntion = Properties.TRemoteHelpFuntion._SendMessage;
        RHMODULE_Funtion.RHMODULE_CREATE = Properties.TRHMODULE._ITHelpCenter;
        RHMODULE_Funtion.Enable = false;
        RHMODULE_Funtion.ATIS = false;
        RHMODULE_Funtion.ITHelpCenter = false;
        RHMODULE_Funtion.CMDB = false;
        RHMODULE_Funtion.RemoteHelp = false
        this.RHMODULE_FuntionList.push(RHMODULE_Funtion);

        RHMODULE_Funtion = new Persistence.RemoteHelp.Properties.TRHMODULE_Funtion();
        RHMODULE_Funtion.RemoteHelpFuntion = Properties.TRemoteHelpFuntion._SendMessage;
        RHMODULE_Funtion.RHMODULE_CREATE = Properties.TRHMODULE._RemoteHelp;
        RHMODULE_Funtion.Enable = false;
        RHMODULE_Funtion.ATIS = false;
        RHMODULE_Funtion.ITHelpCenter = false;
        RHMODULE_Funtion.CMDB = false;
        RHMODULE_Funtion.RemoteHelp = false
        this.RHMODULE_FuntionList.push(RHMODULE_Funtion);


        //*****************************



        Persistence.RemoteHelp.Methods.RHGROUPCR_Fill(this.RHGROUPCRList);//***
        Persistence.RemoteHelp.Methods.RHGROUPCRATROLEPERMISSION_Fill(this.RHGROUPCRATROLEPERMISSIONList);
        Persistence.RemoteHelp.Methods.RHGROUPCRCPU_Fill(this.RHGROUPCRCPUList, 0);
        Persistence.RemoteHelp.Methods.RHGROUPCRPERMISSION_Fill(this.RHGROUPCRPERMISSIONList);
        Persistence.RemoteHelp.Methods.RHGROUPPL_Fill(this.RHGROUPPLList);//***
        Persistence.RemoteHelp.Methods.RHGROUPPL_CFGBACKUP_Fill(this.RHGROUPPL_CFGBACKUPList);
        Persistence.RemoteHelp.Methods.RHGROUPPL_CLI_CONFIG_Fill(this.RHGROUPPL_CLI_CONFIGList);
        Persistence.RemoteHelp.Methods.RHGROUPPLCPU_Fill(this.RHGROUPPLCPUList, "");
        Persistence.RemoteHelp.Methods.RHGROUPPL_POLICY_Fill(this.RHGROUPPL_POLICYList);
        Persistence.RemoteHelp.Methods.RHGROUPPLATROLE_Fill(this.RHGROUPPLATROLEList);


        Persistence.RemoteHelp.Methods.IP_SERVERS_Fill(this.IP_SERVERSList, "");//***
        Persistence.RemoteHelp.Methods.CPU_Fill(this.CPUList, "");
        Persistence.RemoteHelp.Methods.AGENTE_Fill(this.AGENTEList, "");
        Persistence.RemoteHelp.Methods.IP_Fill(this.IPList, "");

        Persistence.RemoteHelp.Methods.ESTACION_RED_Fill(this.ESTACION_REDList, "");

        Persistence.RemoteHelp.Methods.EXTRADATA_Fill(this.EXTRADATAList, "");
        Persistence.RemoteHelp.Methods.USERS_Fill(this.USERSList, "");
        Persistence.RemoteHelp.Methods.VPRO_Fill(this.VPROList, "");
        Persistence.RemoteHelp.Methods.WINDOWS_Fill(this.WINDOWSList, "");

        Persistence.RemoteHelp.Methods.RHREQUEST_Fill(this.RHREQUESTList, "");
        Persistence.RemoteHelp.Methods.RHREQUEST_CPUPL_Fill(this.RHREQUEST_CPUPLList, "");
        Persistence.RemoteHelp.Methods.RHREQUEST_CPUCR_Fill(this.RHREQUEST_CPUCRList, "");
        Persistence.RemoteHelp.Methods.StartReationRemoteHelp(this, UserFunctionsList, -1, "");

    }
}
