﻿//TreduceDB
//Persistence.RemoteHelp.Properties
//Persistence.RemoteHelp.Methods

Persistence.RemoteHelp.Properties.TRHGROUPPLCPU = function () {
    this.IDCPU = "";
    this.IDRHGROUPPL = 0;
    this.IDRHGROUPPLCPU = 0;

    this.NOMBRE_ESTACION = "";
    this.NOMBRE = "";
    this.APELLIDO = "";
    this.DIRECCION = "";

    //virtuales
    this.RHGROUPPL = new Persistence.RemoteHelp.Properties.TRHGROUPPL();
    //this.CPUList = new Array();

    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.IDCPU);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDRHGROUPPL);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDRHGROUPPLCPU);
    }
    this.ByteTo = function (MemStream) {
        this.IDCPU = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDRHGROUPPL = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDRHGROUPPLCPU = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteStr(this.IDCPU, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDRHGROUPPL, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDRHGROUPPLCPU, Longitud, Texto);
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDCPU = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.IDRHGROUPPL = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDRHGROUPPLCPU = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
    }
}

//**********************   METHODS server  ********************************************************************************************

Persistence.RemoteHelp.Methods.RHGROUPPLCPU_Fill = function (RHGROUPPLCPUList, StrIDCPU/*IDRHGROUPPLCPU*/) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    RHGROUPPLCPUList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();

    if (StrIDCPU == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.RHGROUPPLCPU.IDCPU.FieldName, " = " + UsrCfg.InternoAtisNames.RHGROUPPLCPU.NAME_TABLE + "." + UsrCfg.InternoAtisNames.RHGROUPPLCPU.IDRHGROUPPLCPU.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDCPU = " IN (" + StrIDCPU + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.RHGROUPPLCPU.IDRHGROUPPLCPU.FieldName, StrIDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //Param.AddInt32(UsrCfg.InternoDataLinkNames.RHGROUPPLCPU.IDRHGROUPPLCPU.FieldName, IDRHGROUPPLCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);



    try {
        var DS_RHGROUPPLCPU = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "RHGROUPPLCPU_GET", Param.ToBytes());
        ResErr = DS_RHGROUPPLCPU.ResErr;
        if (ResErr.NotError) {
            if (DS_RHGROUPPLCPU.DataSet.RecordCount > 0) {
                DS_RHGROUPPLCPU.DataSet.First();
                while (!(DS_RHGROUPPLCPU.DataSet.Eof)) {
                    var RHGROUPPLCPU = new Persistence.RemoteHelp.Properties.TRHGROUPPLCPU();
                    RHGROUPPLCPU.IDRHGROUPPLCPU = DS_RHGROUPPLCPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPLCPU.IDRHGROUPPLCPU.FieldName).asInt32();
                    RHGROUPPLCPU.IDCPU = DS_RHGROUPPLCPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPLCPU.IDCPU.FieldName).asString();
                    RHGROUPPLCPU.IDRHGROUPPL = DS_RHGROUPPLCPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPLCPU.IDRHGROUPPL.FieldName).asInt32();
                    RHGROUPPLCPUList.push(RHGROUPPLCPU);

                    DS_RHGROUPPLCPU.DataSet.Next();
                }
            }
            else {
                DS_RHGROUPPLCPU.ResErr.NotError = false;
                DS_RHGROUPPLCPU.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

Persistence.RemoteHelp.Methods.RHGROUPPLCPU_GETBYIDGROUP = function (RHGROUPPLCPUList, IDRHGROUPPL) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    RHGROUPPLCPUList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPLCPU.IDRHGROUPPL.FieldName, IDRHGROUPPL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {
        var DS_RHGROUPPLCPU = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "RHGROUPPLCPU_GETBYIDGROUP", Param.ToBytes());
        ResErr = DS_RHGROUPPLCPU.ResErr;
        if (ResErr.NotError) {
            if (DS_RHGROUPPLCPU.DataSet.RecordCount > 0) {
                DS_RHGROUPPLCPU.DataSet.First();
                while (!(DS_RHGROUPPLCPU.DataSet.Eof)) {
                    var RHGROUPPLCPU = new Persistence.RemoteHelp.Properties.TRHGROUPPLCPU();
                    RHGROUPPLCPU.IDRHGROUPPLCPU = DS_RHGROUPPLCPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPLCPU.IDRHGROUPPLCPU.FieldName).asInt32();
                    RHGROUPPLCPU.IDCPU = DS_RHGROUPPLCPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPLCPU.IDCPU.FieldName).asString();
                    RHGROUPPLCPU.IDRHGROUPPL = DS_RHGROUPPLCPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPLCPU.IDRHGROUPPL.FieldName).asInt32();

                    RHGROUPPLCPU.NOMBRE_ESTACION = DS_RHGROUPPLCPU.DataSet.RecordSet.FieldName("NOMBRE_ESTACION").asString();
                    RHGROUPPLCPU.NOMBRE = DS_RHGROUPPLCPU.DataSet.RecordSet.FieldName("NOMBREE").asString();
                    RHGROUPPLCPU.APELLIDO = DS_RHGROUPPLCPU.DataSet.RecordSet.FieldName("APELLIDOE").asString();
                    RHGROUPPLCPU.DIRECCION = DS_RHGROUPPLCPU.DataSet.RecordSet.FieldName("DIRECCION").asString();
                    RHGROUPPLCPUList.push(RHGROUPPLCPU);

                    DS_RHGROUPPLCPU.DataSet.Next();
                }
            }
            else {
                DS_RHGROUPPLCPU.ResErr.NotError = false;
                DS_RHGROUPPLCPU.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPPLCPU_GETID = function (RHGROUPPLCPU) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPLCPU.IDRHGROUPPLCPU.FieldName, RHGROUPPLCPU.IDRHGROUPPLCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPPLCPU.IDCPU.FieldName, RHGROUPPLCPU.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPLCPU.IDRHGROUPPL.FieldName, RHGROUPPLCPU.IDRHGROUPPL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var DS_RHGROUPPLCPU = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "RHGROUPPLCPU_GETID", Param.ToBytes());
        ResErr = DS_RHGROUPPLCPU.ResErr;
        if (ResErr.NotError) {
            if (DS_RHGROUPPLCPU.DataSet.RecordCount > 0) {
                DS_RHGROUPPLCPU.DataSet.First();
                RHGROUPPLCPU.IDRHGROUPPLCPU = DS_RHGROUPPLCPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPLCPU.IDRHGROUPPLCPU.FieldName).asInt32();
                //Other
                RHGROUPPLCPU.IDCPU = DS_RHGROUPPLCPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPLCPU.IDCPU.FieldName).asString();
                RHGROUPPLCPU.IDRHGROUPPL = DS_RHGROUPPLCPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPLCPU.IDRHGROUPPL.FieldName).asInt32();
            }
            else {
                DS_RHGROUPPLCPU.ResErr.NotError = false;
                DS_RHGROUPPLCPU.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

Persistence.RemoteHelp.Methods.RHGROUPPLCPU_ADD = function (RHGROUPPLCPU) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPPLCPU.IDCPU.FieldName, RHGROUPPLCPU.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPLCPU.IDRHGROUPPL.FieldName, RHGROUPPLCPU.IDRHGROUPPL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHGROUPPLCPU_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.RemoteHelp.Methods.RHGROUPPLCPU_GETID(RHGROUPPLCPU);
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPPLCPU_UPD = function (RHGROUPPLCPU) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPLCPU.IDRHGROUPPLCPU.FieldName, RHGROUPPLCPU.IDRHGROUPPLCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPPLCPU.IDCPU.FieldName, RHGROUPPLCPU.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPLCPU.IDRHGROUPPL.FieldName, RHGROUPPLCPU.IDRHGROUPPL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHGROUPPLCPU_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.RemoteHelp.Methods.RHGROUPPLCPU_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.RemoteHelp.Methods.RHGROUPPLCPU_DELIDRHGROUPPLCPU(Param);
    }
    else {
        Res = Persistence.RemoteHelp.Methods.RHGROUPPLCPU_DELRHGROUPPLCPU(Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.RHGROUPPLCPU_DELIDRHGROUPPLCPU = function (IDRHGROUPPLCPU) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPLCPU.IDRHGROUPPLCPU.FieldName, IDRHGROUPPLCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHGROUPPLCPU_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPPLCPU_DELRHGROUPPLCPU = function (RHGROUPPLCPU) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPLCPU.IDRHGROUPPLCPU.FieldName, RHGROUPPLCPU.IDRHGROUPPLCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPPLCPU.IDCPU.FieldName, RHGROUPPLCPU.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPLCPU.IDRHGROUPPL.FieldName, RHGROUPPLCPU.IDRHGROUPPL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHGROUPPLCPU_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}


//**********************   METHODS   ********************************************************************************************

Persistence.RemoteHelp.Methods.RHGROUPPLCPU_ListSetID = function (RHGROUPPLCPUList, IDRHGROUPPLCPU) {
    for (var i = 0; i < RHGROUPPLCPUList.length; i++) {

        if (IDRHGROUPPLCPU == RHGROUPPLCPUList[i].IDRHGROUPPLCPU)
            return (RHGROUPPLCPUList[i]);

    }
    var UnRHGROUPPLCPU = new Persistence.RemoteHelp.Properties.TRHGROUPPLCPU();
    UnRHGROUPPLCPU.IDRHGROUPPLCPU = IDRHGROUPPLCPU;
    return (UnRHGROUPPLCPU);
}
Persistence.RemoteHelp.Methods.RHGROUPPLCPU_ListAdd = function (RHGROUPPLCPUList, RHGROUPPLCPU) {
    var i = Persistence.RemoteHelp.Methods.RHGROUPPLCPU_ListGetIndex(RHGROUPPLCPUList, RHGROUPPLCPU);
    if (i == -1) RHGROUPPLCPUList.push(RHGROUPPLCPU);
    else {
        RHGROUPPLCPUList[i].IDCPU = RHGROUPPLCPU.IDCPU;
        RHGROUPPLCPUList[i].IDRHGROUPPL = RHGROUPPLCPU.IDRHGROUPPL;
        RHGROUPPLCPUList[i].IDRHGROUPPLCPU = RHGROUPPLCPU.IDRHGROUPPLCPU;
    }
}

//CJRC_26072018
Persistence.RemoteHelp.Methods.RHGROUPPLCPU_ListGetIndex = function (RHGROUPPLCPUList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.RemoteHelp.Methods.RHGROUPPLCPU_ListGetIndexIDRHGROUPPLCPU(RHGROUPPLCPUList, Param);

    }
    else {
        Res = Persistence.RemoteHelp.Methods.RHGROUPPLCPU_ListGetIndexRHGROUPPLCPU(RHGROUPPLCPUList, Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.RHGROUPPLCPU_ListGetIndexRHGROUPPLCPU = function (RHGROUPPLCPUList, RHGROUPPLCPU) {
    for (var i = 0; i < RHGROUPPLCPUList.length; i++) {
        if (RHGROUPPLCPU.IDRHGROUPPLCPU == RHGROUPPLCPUList[i].IDRHGROUPPLCPU)
            return (i);
    }
    return (-1);
}
Persistence.RemoteHelp.Methods.RHGROUPPLCPU_ListGetIndexIDRHGROUPPLCPU = function (RHGROUPPLCPUList, IDRHGROUPPLCPU) {
    for (var i = 0; i < RHGROUPPLCPUList.length; i++) {
        if (IDRHGROUPPLCPU == RHGROUPPLCPUList[i].IDRHGROUPPLCPU)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.RemoteHelp.Methods.RHGROUPPLCPUListtoStr = function (RHGROUPPLCPUList) {
    var Res = Persistence.RemoteHelp.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(RHGROUPPLCPUList.length, Longitud, Texto);
        for (Counter = 0; Counter < RHGROUPPLCPUList.length; Counter++) {
            var UnRHGROUPPLCPU = RHGROUPPLCPUList[Counter];
            UnRHGROUPPLCPU.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.RemoteHelp.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.RemoteHelp.Methods.StrtoRHGROUPPLCPU = function (ProtocoloStr, RHGROUPPLCPUList) {
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        RHGROUPPLCPUList.length = 0;
        if (Persistence.RemoteHelp.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnRHGROUPPLCPU = new Persistence.Demo.Properties.TRHGROUPPLCPU(); //Mode new row
                UnRHGROUPPLCPU.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.RHGROUPPLCPU_ListAdd(RHGROUPPLCPUList, UnRHGROUPPLCPU);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}

//Socket Function 
Persistence.RemoteHelp.Methods.RHGROUPPLCPUListToByte = function (RHGROUPPLCPUList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, RHGROUPPLCPUList.length - idx);
    for (var i = idx; i < RHGROUPPLCPUList.length; i++) {
        RHGROUPPLCPUList[i].ToByte(MemStream);
    }
}
Persistence.RemoteHelp.Methods.ByteToRHGROUPPLCPUList = function (RHGROUPPLCPUList, MemStream) {
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (var i = 0; i < Count; i++) {
        var UnRHGROUPPLCPU = new Persistence.RemoteHelp.Properties.TRHGROUPPLCPU();
        UnRHGROUPPLCPU.ByteTo(MemStream);
        Persistence.RemoteHelp.Methods.RHGROUPPLCPU_ListAdd(RHGROUPPLCPUList, UnRHGROUPPLCPU);
    }
}

