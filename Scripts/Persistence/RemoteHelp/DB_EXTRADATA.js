﻿//TreduceDB
//Persistence.RemoteHelp.Properties
//Persistence.RemoteHelp.Methods

Persistence.RemoteHelp.Properties.TEXTRADATA = function () {
    this.APELLIDOE = "";
    this.AYUNOMBRE = "";
    this.AYUPROBLEMA = "";
    this.AYUTELEFONO = "";
    this.CEMPLEADO = "";
    this.DIRECCION = "";
    this.EXTRA_DATE = new Date(1970, 0, 1, 0, 0, 0);
    this.IDCPU = "";
    this.MAIL = "";
    this.NOMBREE = "";
    this.TELEFONO = "";
    this.UBICACION = "";
    this.USERS = "";

    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.APELLIDOE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.AYUNOMBRE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.AYUPROBLEMA);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.AYUTELEFONO);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CEMPLEADO);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.DIRECCION);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.EXTRA_DATE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.IDCPU);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.MAIL);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.NOMBREE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.TELEFONO);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.UBICACION);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.USERS);
    }
    this.ByteTo = function (MemStream) {
        this.APELLIDOE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.AYUNOMBRE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.AYUPROBLEMA = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.AYUTELEFONO = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CEMPLEADO = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.DIRECCION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.EXTRA_DATE = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.IDCPU = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.MAIL = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.NOMBREE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.TELEFONO = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.UBICACION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.USERS = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteStr(this.APELLIDOE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.AYUNOMBRE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.AYUPROBLEMA, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.AYUTELEFONO, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.CEMPLEADO, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.DIRECCION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteDateTime(this.EXTRA_DATE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.IDCPU, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.MAIL, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.NOMBREE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.TELEFONO, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.UBICACION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.USERS, Longitud, Texto);
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.APELLIDOE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.AYUNOMBRE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.AYUPROBLEMA = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.AYUTELEFONO = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.CEMPLEADO = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.DIRECCION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.EXTRA_DATE = SysCfg.Str.Protocol.ReadDateTime(Pos, Index, LongitudArray, Texto);
        this.IDCPU = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.MAIL = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.NOMBREE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.TELEFONO = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.UBICACION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.USERS = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
    }

}

//**********************   METHODS server  ********************************************************************************************

Persistence.RemoteHelp.Methods.EXTRADATA_Fill = function (EXTRADATAList, StrIDCPU) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    EXTRADATAList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDCPU == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.EXTRADATA.IDCPU.FieldName, " = " + UsrCfg.InternoAtisNames.EXTRADATA.NAME_TABLE + "." + UsrCfg.InternoAtisNames.EXTRADATA.IDCPU.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDCPU = " IN (" + StrIDCPU + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.EXTRADATA.IDCPU.FieldName, StrIDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    try {
        var DS_EXTRADATA = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "EXTRADATA_GET", Param.ToBytes());
        ResErr = DS_EXTRADATA.ResErr;
        if (ResErr.NotError) {
            if (DS_EXTRADATA.DataSet.RecordCount > 0) {
                DS_EXTRADATA.DataSet.First();
                while (!(DS_EXTRADATA.DataSet.Eof)) {
                    var EXTRADATA = new Persistence.RemoteHelp.Properties.TEXTRADATA();
                    //Other
                    EXTRADATA.APELLIDOE = DS_EXTRADATA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.EXTRADATA.APELLIDOE.FieldName).asString();
                    EXTRADATA.AYUNOMBRE = DS_EXTRADATA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.EXTRADATA.AYUNOMBRE.FieldName).asString();
                    EXTRADATA.AYUPROBLEMA = DS_EXTRADATA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.EXTRADATA.AYUPROBLEMA.FieldName).asString();
                    EXTRADATA.AYUTELEFONO = DS_EXTRADATA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.EXTRADATA.AYUTELEFONO.FieldName).asString();
                    EXTRADATA.CEMPLEADO = DS_EXTRADATA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.EXTRADATA.CEMPLEADO.FieldName).asString();
                    EXTRADATA.DIRECCION = DS_EXTRADATA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.EXTRADATA.DIRECCION.FieldName).asString();
                    EXTRADATA.EXTRA_DATE = DS_EXTRADATA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.EXTRADATA.EXTRA_DATE.FieldName).asDateTime();
                    EXTRADATA.IDCPU = DS_EXTRADATA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.EXTRADATA.IDCPU.FieldName).asString();
                    EXTRADATA.MAIL = DS_EXTRADATA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.EXTRADATA.MAIL.FieldName).asString();
                    EXTRADATA.NOMBREE = DS_EXTRADATA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.EXTRADATA.NOMBREE.FieldName).asString();
                    EXTRADATA.TELEFONO = DS_EXTRADATA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.EXTRADATA.TELEFONO.FieldName).asString();
                    EXTRADATA.UBICACION = DS_EXTRADATA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.EXTRADATA.UBICACION.FieldName).asString();
                    EXTRADATA.USERS = DS_EXTRADATA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.EXTRADATA.USERS.FieldName).asString();
                    EXTRADATAList.push(EXTRADATA);

                    DS_EXTRADATA.DataSet.Next();
                }
            }
            else {
                DS_EXTRADATA.ResErr.NotError = false;
                DS_EXTRADATA.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

Persistence.RemoteHelp.Methods.EXTRADATA_GETID = function (EXTRADATA) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.APELLIDOE.FieldName, EXTRADATA.APELLIDOE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.AYUNOMBRE.FieldName, EXTRADATA.AYUNOMBRE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.AYUPROBLEMA.FieldName, EXTRADATA.AYUPROBLEMA, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.AYUTELEFONO.FieldName, EXTRADATA.AYUTELEFONO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.CEMPLEADO.FieldName, EXTRADATA.CEMPLEADO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.DIRECCION.FieldName, EXTRADATA.DIRECCION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoAtisNames.EXTRADATA.EXTRA_DATE.FieldName, EXTRADATA.EXTRA_DATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.IDCPU.FieldName, EXTRADATA.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.MAIL.FieldName, EXTRADATA.MAIL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.NOMBREE.FieldName, EXTRADATA.NOMBREE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.TELEFONO.FieldName, EXTRADATA.TELEFONO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.UBICACION.FieldName, EXTRADATA.UBICACION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.USERS.FieldName, EXTRADATA.USERS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var DS_EXTRADATA = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "EXTRADATA_GETID", Param.ToBytes());
        ResErr = DS_EXTRADATA.ResErr;
        if (ResErr.NotError) {
            if (DS_EXTRADATA.DataSet.RecordCount > 0) {
                DS_EXTRADATA.DataSet.First();
                //Other
                EXTRADATA.APELLIDOE = DS_EXTRADATA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.EXTRADATA.APELLIDOE.FieldName).asString();
                EXTRADATA.AYUNOMBRE = DS_EXTRADATA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.EXTRADATA.AYUNOMBRE.FieldName).asString();
                EXTRADATA.AYUPROBLEMA = DS_EXTRADATA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.EXTRADATA.AYUPROBLEMA.FieldName).asString();
                EXTRADATA.AYUTELEFONO = DS_EXTRADATA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.EXTRADATA.AYUTELEFONO.FieldName).asString();
                EXTRADATA.CEMPLEADO = DS_EXTRADATA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.EXTRADATA.CEMPLEADO.FieldName).asString();
                EXTRADATA.DIRECCION = DS_EXTRADATA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.EXTRADATA.DIRECCION.FieldName).asString();
                EXTRADATA.EXTRA_DATE = DS_EXTRADATA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.EXTRADATA.EXTRA_DATE.FieldName).asDateTime();
                EXTRADATA.IDCPU = DS_EXTRADATA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.EXTRADATA.IDCPU.FieldName).asString();
                EXTRADATA.MAIL = DS_EXTRADATA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.EXTRADATA.MAIL.FieldName).asString();
                EXTRADATA.NOMBREE = DS_EXTRADATA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.EXTRADATA.NOMBREE.FieldName).asString();
                EXTRADATA.TELEFONO = DS_EXTRADATA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.EXTRADATA.TELEFONO.FieldName).asString();
                EXTRADATA.UBICACION = DS_EXTRADATA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.EXTRADATA.UBICACION.FieldName).asString();
                EXTRADATA.USERS = DS_EXTRADATA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.EXTRADATA.USERS.FieldName).asString();
            }
            else {
                DS_EXTRADATA.ResErr.NotError = false;
                DS_EXTRADATA.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

Persistence.RemoteHelp.Methods.EXTRADATA_ADD = function (EXTRADATA) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.APELLIDOE.FieldName, EXTRADATA.APELLIDOE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.AYUNOMBRE.FieldName, EXTRADATA.AYUNOMBRE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.AYUPROBLEMA.FieldName, EXTRADATA.AYUPROBLEMA, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.AYUTELEFONO.FieldName, EXTRADATA.AYUTELEFONO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.CEMPLEADO.FieldName, EXTRADATA.CEMPLEADO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.DIRECCION.FieldName, EXTRADATA.DIRECCION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoAtisNames.EXTRADATA.EXTRA_DATE.FieldName, EXTRADATA.EXTRA_DATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.IDCPU.FieldName, EXTRADATA.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.MAIL.FieldName, EXTRADATA.MAIL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.NOMBREE.FieldName, EXTRADATA.NOMBREE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.TELEFONO.FieldName, EXTRADATA.TELEFONO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.UBICACION.FieldName, EXTRADATA.UBICACION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.USERS.FieldName, EXTRADATA.USERS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "EXTRADATA_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.RemoteHelp.Methods.EXTRADATA_GETID(EXTRADATA);
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

Persistence.RemoteHelp.Methods.EXTRADATA_UPD = function (EXTRADATA) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.APELLIDOE.FieldName, EXTRADATA.APELLIDOE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.AYUNOMBRE.FieldName, EXTRADATA.AYUNOMBRE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.AYUPROBLEMA.FieldName, EXTRADATA.AYUPROBLEMA, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.AYUTELEFONO.FieldName, EXTRADATA.AYUTELEFONO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.CEMPLEADO.FieldName, EXTRADATA.CEMPLEADO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.DIRECCION.FieldName, EXTRADATA.DIRECCION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoAtisNames.EXTRADATA.EXTRA_DATE.FieldName, EXTRADATA.EXTRA_DATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.IDCPU.FieldName, EXTRADATA.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.MAIL.FieldName, EXTRADATA.MAIL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.NOMBREE.FieldName, EXTRADATA.NOMBREE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.TELEFONO.FieldName, EXTRADATA.TELEFONO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.UBICACION.FieldName, EXTRADATA.UBICACION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.USERS.FieldName, EXTRADATA.USERS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "EXTRADATA_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}


//CJRC_27072018
Persistence.RemoteHelp.Methods.EXTRADATA_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.RemoteHelp.Methods.EXTRADATA_DELIDCPU(/*String StrIDCPU*/ Param);
    }
    else {
        Res = Persistence.RemoteHelp.Methods.EXTRADATA_DELEXTRADATA(Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.EXTRADATA_DELIDCPU = function (/*String StrIDCPU*/ IDCPU)
{
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDCPU == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.EXTRADATA.IDCPU.FieldName, " = " + UsrCfg.InternoAtisNames.EXTRADATA.NAME_TABLE + "." + UsrCfg.InternoAtisNames.EXTRADATA.IDCPU.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDCPU = " IN (" + StrIDCPU + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.EXTRADATA.IDCPU.FieldName, StrIDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.EXTRADATA.IDCPU.FieldName, IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "EXTRADATA_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

Persistence.RemoteHelp.Methods.EXTRADATA_DELEXTRADATA = function (EXTRADATA) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.APELLIDOE.FieldName, EXTRADATA.APELLIDOE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.AYUNOMBRE.FieldName, EXTRADATA.AYUNOMBRE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.AYUPROBLEMA.FieldName, EXTRADATA.AYUPROBLEMA, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.AYUTELEFONO.FieldName, EXTRADATA.AYUTELEFONO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.CEMPLEADO.FieldName, EXTRADATA.CEMPLEADO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.DIRECCION.FieldName, EXTRADATA.DIRECCION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoAtisNames.EXTRADATA.EXTRA_DATE.FieldName, EXTRADATA.EXTRA_DATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.IDCPU.FieldName, EXTRADATA.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.MAIL.FieldName, EXTRADATA.MAIL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.NOMBREE.FieldName, EXTRADATA.NOMBREE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.TELEFONO.FieldName, EXTRADATA.TELEFONO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.UBICACION.FieldName, EXTRADATA.UBICACION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.EXTRADATA.USERS.FieldName, EXTRADATA.USERS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "EXTRADATA_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

    

//**********************   METHODS   ********************************************************************************************

//List Function 
Persistence.RemoteHelp.Methods.EXTRADATA_ListSetID = function (EXTRADATAList, IDCPU) {
    for (var i = 0; i < EXTRADATAList.length; i++) {

        if (IDCPU == EXTRADATAList[i].IDCPU)
            return (EXTRADATAList[i]);

    }
    var UnEXTRADATA = new Persistence.RemoteHelp.Properties.TEXTRADATA();
    UnEXTRADATA.IDCPU = IDCPU;
    return (UnEXTRADATA);
}

Persistence.RemoteHelp.Methods.EXTRADATA_ListAdd = function (EXTRADATAList, EXTRADATA) {
    var i = Persistence.RemoteHelp.Methods.EXTRADATA_ListGetIndex(EXTRADATAList, EXTRADATA);
    if (i == -1) EXTRADATAList.push(EXTRADATA);
    else {
        EXTRADATAList[i].APELLIDOE = EXTRADATA.APELLIDOE;
        EXTRADATAList[i].AYUNOMBRE = EXTRADATA.AYUNOMBRE;
        EXTRADATAList[i].AYUPROBLEMA = EXTRADATA.AYUPROBLEMA;
        EXTRADATAList[i].AYUTELEFONO = EXTRADATA.AYUTELEFONO;
        EXTRADATAList[i].CEMPLEADO = EXTRADATA.CEMPLEADO;
        EXTRADATAList[i].DIRECCION = EXTRADATA.DIRECCION;
        EXTRADATAList[i].EXTRA_DATE = EXTRADATA.EXTRA_DATE;
        EXTRADATAList[i].IDCPU = EXTRADATA.IDCPU;
        EXTRADATAList[i].MAIL = EXTRADATA.MAIL;
        EXTRADATAList[i].NOMBREE = EXTRADATA.NOMBREE;
        EXTRADATAList[i].TELEFONO = EXTRADATA.TELEFONO;
        EXTRADATAList[i].UBICACION = EXTRADATA.UBICACION;
        EXTRADATAList[i].USERS = EXTRADATA.USERS;
    }
}

//CJRC_26072018
Persistence.RemoteHelp.Methods.EXTRADATA_ListGetIndex = function (EXTRADATAList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.RemoteHelp.Methods.EXTRADATA_ListGetIndexIDCPU(EXTRADATAList, Param);

    }
    else {
        Res = Persistence.RemoteHelp.Methods.EXTRADATA_ListGetIndexEXTRADATA(EXTRADATAList, Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.EXTRADATA_ListGetIndexEXTRADATA = function (EXTRADATAList, EXTRADATA) {
    for (var i = 0; i < EXTRADATAList.length; i++) {
        if (EXTRADATA.IDCPU == EXTRADATAList[i].IDCPU)
            return (i);
    }
    return (-1);
}
Persistence.RemoteHelp.Methods.EXTRADATA_ListGetIndexIDCPU = function (EXTRADATAList, IDCPU) {
    for (var i = 0; i < EXTRADATAList.length; i++) {
        if (IDCPU == EXTRADATAList[i].IDCPU)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.RemoteHelp.Methods.EXTRADATAListtoStr = function (EXTRADATAList) {
    var Res = Persistence.RemoteHelp.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(EXTRADATAList.length, Longitud, Texto);
        for (Counter = 0; Counter < EXTRADATAList.length; Counter++) {
            var UnEXTRADATA = EXTRADATAList[Counter];
            UnEXTRADATA.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.RemoteHelp.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.RemoteHelp.Methods.StrtoEXTRADATA = function (ProtocoloStr, EXTRADATAList) {
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        EXTRADATAList.length = 0;
        if (Persistence.RemoteHelp.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnEXTRADATA = new Persistence.Demo.Properties.TEXTRADATA(); //Mode new row
                UnEXTRADATA.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.EXTRADATA_ListAdd(EXTRADATAList, UnEXTRADATA);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}

//Socket Function 
Persistence.RemoteHelp.Methods.EXTRADATAListToByte = function (EXTRADATAList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, EXTRADATAList.length - idx);
    for (var i = idx; i < EXTRADATAList.length; i++) {
        EXTRADATAList[i].ToByte(MemStream);
    }
}
Persistence.RemoteHelp.Methods.ByteToEXTRADATAList = function (EXTRADATAList, MemStream) {
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (var i = 0; i < Count; i++) {
        UnEXTRADATA = new Persistence.RemoteHelp.Properties.TEXTRADATA();
        UnEXTRADATA.ByteTo(MemStream);
        Persistence.RemoteHelp.Methods.EXTRADATA_ListAdd(EXTRADATAList, UnEXTRADATA);
    }
}
