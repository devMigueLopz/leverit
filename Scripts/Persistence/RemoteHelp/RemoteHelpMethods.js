﻿//TreduceDB
//Persistence.RemoteHelp.Methods
Persistence.RemoteHelp.Methods.StartReationRemoteHelp = function(RemoteHelpProfiler, UserFunctionsList, DRHREQUEST, IDCPU)
{

    //***************************************************************************************************************************************************************************
    //*************************** DISCOVERY *************************************************************************************************************************************

                
    var CPUListOrder = SysCfg.CopyList(RemoteHelpProfiler.CPUList).sort(function (a, b) { return a.IDCPU - b.IDCPU; });//Persistence.RemoteHelp.Properties.TCPU
    RemoteHelpProfiler.CPUList = CPUListOrder;
    var IPListOrder = SysCfg.CopyList(RemoteHelpProfiler.IPList).sort(function (a, b) { return a.IDCPU - b.IDCPU; });//Persistence.RemoteHelp.Properties.TIP
    var AGENTEListOrder = SysCfg.CopyList(RemoteHelpProfiler.AGENTEList).sort(function (a, b) { return a.IDCPU - b.IDCPU; });//Persistence.RemoteHelp.Properties.TAGENTE
    var ESTACION_REDListOrder = SysCfg.CopyList(RemoteHelpProfiler.ESTACION_REDList).sort(function (a, b) { return a.IDCPU - b.IDCPU; });//Persistence.RemoteHelp.Properties.TESTACION_RED
    var EXTRADATAListOrder = SysCfg.CopyList(RemoteHelpProfiler.EXTRADATAList).sort(function (a, b) { return a.IDCPU - b.IDCPU; });//Persistence.RemoteHelp.Properties.TEXTRADATA
    var USERSListOrder = SysCfg.CopyList(RemoteHelpProfiler.USERSList).sort(function (a, b) { return a.IDCPU - b.IDCPU; });//Persistence.RemoteHelp.Properties.TUSERS
    var VPROListOrder = SysCfg.CopyList(RemoteHelpProfiler.VPROList).sort(function (a, b) { return a.IDCPU - b.IDCPU; });//Persistence.RemoteHelp.Properties.TVPRO
    var WINDOWSListOrder = SysCfg.CopyList(RemoteHelpProfiler.WINDOWSList).sort(function (a, b) { return a.IDCPU - b.IDCPU; });//Persistence.RemoteHelp.Properties.TWINDOWS
    var RHGROUPCRCPUListOrder = SysCfg.CopyList(RemoteHelpProfiler.RHGROUPCRCPUList).sort(function (a, b) { return a.IDCPU - b.IDCPU; });//Persistence.RemoteHelp.Properties.TRHGROUPCRCPU
    var RHGROUPPLCPUListOrder = SysCfg.CopyList(RemoteHelpProfiler.RHGROUPPLCPUList).sort(function (a, b) { return a.IDCPU - b.IDCPU; });//Persistence.RemoteHelp.Properties.TRHGROUPPLCPU


             
            

    var idxIP = 0;
    var idxAGENTE = 0;
    var idxESTACION_RED = 0;
    var idxEXTRADATA = 0;
    var idxUSERS = 0;
    var idxVPRO = 0;
    var idxWINDOWS = 0;
    var idxRHGROUPCRCPU = 0;
    var idxRHGROUPPLCPU = 0;

    for (var i = 0; i < CPUListOrder.Count; i++)
    {
        if ((IDCPU == "") || (IDCPU == CPUListOrder[i].IDCPU))
        {
            for (var x = idxIP; x < IPListOrder.Count; x++)
            {
                idxIP = x;
                var comp = string.Compare(IPListOrder[x].IDCPU, CPUListOrder[i].IDCPU, true);
                if (comp == 0)
                {
                    RemoteHelp.Methods.IP_ListAdd(CPUListOrder[i].IPList, IPListOrder[x]);
                    //break;
                }
                if (comp > 0)
                {
                    break;
                }
                if (comp < 0)
                {
                }
            }


            for (var x = idxAGENTE; x < AGENTEListOrder.Count; x++)
            {
                idxAGENTE = x;
                var comp = string.Compare(AGENTEListOrder[x].IDCPU, CPUListOrder[i].IDCPU, true);
                if (comp == 0)
                {
                    CPUListOrder[i].AGENTE = AGENTEListOrder[x];
                    //break;
                }
                if (comp > 0)
                {
                    break;
                }
                if (comp < 0)
                {
                }
            }
            for (var x = idxESTACION_RED; x < ESTACION_REDListOrder.Count; x++)
            {
                idxESTACION_RED = x;
                var comp = string.Compare(ESTACION_REDListOrder[x].IDCPU, CPUListOrder[i].IDCPU, true);

                if (comp == 0)
                {
                    CPUListOrder[i].ESTACION_RED = ESTACION_REDListOrder[x];
                    //break;
                }
                if (comp > 0)
                {
                    break;
                }
                if (comp < 0)
                {
                }
            }
            for (var x = idxEXTRADATA; x < EXTRADATAListOrder.Count; x++)
            {
                idxEXTRADATA = x;
                var comp = string.Compare(EXTRADATAListOrder[x].IDCPU, CPUListOrder[i].IDCPU, true);

                if (comp == 0)
                {
                    CPUListOrder[i].EXTRADATA = EXTRADATAListOrder[x];
                    //break;
                }
                if (comp > 0)
                {
                    break;
                }
                if (comp < 0)
                {
                }
            }
            for (var x = idxUSERS; x < USERSListOrder.Count; x++)
            {
                idxUSERS = x;
                var comp = string.Compare(USERSListOrder[x].IDCPU, CPUListOrder[i].IDCPU);

                if (comp == 0)
                {
                    CPUListOrder[i].USERS = USERSListOrder[x];
                    //break;
                }
                if (comp > 0)
                {
                    break;
                }
                if (comp < 0)
                {
                }
            }
            for (var x = idxVPRO; x < VPROListOrder.Count; x++)
            {
                idxVPRO = x;
                var comp = string.Compare(VPROListOrder[x].IDCPU, CPUListOrder[i].IDCPU, true);

                if (comp == 0)
                {
                    CPUListOrder[i].VPRO = VPROListOrder[x];
                    //break;
                }
                if (comp > 0)
                {
                    break;
                }
                if (comp < 0)
                {
                }
            }
                    

            for (var x = idxWINDOWS; x < WINDOWSListOrder.Count; x++)
            {
                idxWINDOWS = x;
                var comp = string.Compare(WINDOWSListOrder[x].IDCPU, CPUListOrder[i].IDCPU, true);

                if (comp == 0)
                {
                    CPUListOrder[i].WINDOWS = WINDOWSListOrder[x];
                    //break;
                }
                if (comp > 0)
                {
                    break;
                }
                if (comp < 0)
                {
                }
            }

            for (var x = idxRHGROUPCRCPU; x < RHGROUPCRCPUListOrder.Count; x++)
            {
                idxRHGROUPCRCPU = x;
                var comp = string.Compare(RHGROUPCRCPUListOrder[x].IDCPU, CPUListOrder[i].IDCPU, true);

                if (comp == 0)
                {
                    RemoteHelp.Methods.RHGROUPCRCPU_ListAdd(CPUListOrder[i].RHGROUPCRCPUList, RHGROUPCRCPUListOrder[x]);
                    //RemoteHelp.Methods.CPU_ListAdd(RHGROUPCRCPUListOrder[x].CPUList, CPUListOrder[i]);
                    //break;
                }
                if (comp > 0)
                {
                    break;
                }
                if (comp < 0)
                {
                }
            }
            for (var x = idxRHGROUPPLCPU; x < RHGROUPPLCPUListOrder.Count; x++)
            {
                idxRHGROUPPLCPU = x;
                var comp = string.Compare(RHGROUPPLCPUListOrder[x].IDCPU, CPUListOrder[i].IDCPU, true);

                if (comp == 0)
                {
                    RemoteHelp.Methods.RHGROUPPLCPU_ListAdd(CPUListOrder[i].RHGROUPPLCPUList, RHGROUPPLCPUListOrder[x]);
                    //RemoteHelp.Methods.CPU_ListAdd(RHGROUPPLCPUListOrder[x].CPUList, CPUListOrder[i]);
                    //break;
                }
                if (comp > 0)
                {
                    break;
                }
                if (comp < 0)
                {
                }
            }
        }
    }


    //***************************************************************************************************************************************************************************
    //*************************** RHGROUP** *************************************************************************************************************************************


    var RHGROUPCRListOrder = SysCfg.CopyList(RemoteHelpProfiler.RHGROUPCRList).sort(function (a, b) { return a.IDRHGROUPCR - b.IDRHGROUPCR; });//Persistence.RemoteHelp.Properties.TRHGROUPCR
    var RHGROUPCRATROLEPERMISSIONListOrder = SysCfg.CopyList(RemoteHelpProfiler.RHGROUPCRATROLEPERMISSIONList).sort(function (a, b) { return a.IDRHGROUPCR - b.IDRHGROUPCR; });//Persistence.RemoteHelp.Properties.TRHGROUPCRATROLEPERMISSION
    var RHGROUPCRCPUListOrder = SysCfg.CopyList(RemoteHelpProfiler.RHGROUPCRCPUList).sort(function (a, b) { return a.IDRHGROUPCR - b.IDRHGROUPCR; });


    var idxRHGROUPCRATROLEPERMISSION = 0;
    idxRHGROUPCRCPU = 0;

    for (var i = 0; i < RHGROUPCRListOrder.Count; i++)
    {

        for (var x = idxRHGROUPCRATROLEPERMISSION; x <  RHGROUPCRATROLEPERMISSIONListOrder.Count; x++)
        {
            idxRHGROUPCRATROLEPERMISSION = x;

            if (RHGROUPCRATROLEPERMISSIONListOrder[x].IDRHGROUPCR == RHGROUPCRListOrder[i].IDRHGROUPCR)
            {
                RemoteHelp.Methods.RHGROUPCRATROLEPERMISSION_ListAdd(RHGROUPCRListOrder[i].RHGROUPCRATROLEPERMISSIONList, RHGROUPCRATROLEPERMISSIONListOrder[x]);

            }
            if (RHGROUPCRATROLEPERMISSIONListOrder[x].IDRHGROUPCR > RHGROUPCRListOrder[i].IDRHGROUPCR)
            {
                break;
            }
        }
        for (var x = idxRHGROUPCRCPU; x < RHGROUPCRCPUListOrder.Count; x++)
        {
            idxRHGROUPCRCPU = x;

            if (RHGROUPCRCPUListOrder[x].IDRHGROUPCR == RHGROUPCRListOrder[i].IDRHGROUPCR)
            {
                RHGROUPCRCPUListOrder[x].RHGROUPCR = RHGROUPCRListOrder[i];

            }
            if (RHGROUPCRCPUListOrder[x].IDRHGROUPCR > RHGROUPCRListOrder[i].IDRHGROUPCR)
            {
                break;
            }
        }

    }

            

    var RHGROUPPLListOrder = SysCfg.CopyList(RemoteHelpProfiler.RHGROUPPLList).sort(function (a, b) { return a.IDRHGROUPPL - b.IDRHGROUPPL; });//Persistence.RemoteHelp.Properties.TRHGROUPPL
    var RHGROUPPLATROLEListOrder = SysCfg.CopyList(RemoteHelpProfiler.RHGROUPPLATROLEList).sort(function (a, b) { return a.IDRHGROUPPL - b.IDRHGROUPPL; });//Persistence.RemoteHelp.Properties.TRHGROUPPLATROLE
    RHGROUPPLCPUListOrder = SysCfg.CopyList(RemoteHelpProfiler.RHGROUPPLCPUList).sort(function (a, b) { return a.IDRHGROUPPL - b.IDRHGROUPPL; });
    var idxRHGROUPPLATROLE = 0;
    idxRHGROUPPLCPU = 0;
    for (var i = 0; i < RHGROUPPLListOrder.Count; i++)
    {
        for (var x = idxRHGROUPPLATROLE; x < RHGROUPPLATROLEListOrder.Count; x++)
        {
            idxRHGROUPPLATROLE = x;
            if (RHGROUPPLATROLEListOrder[x].IDRHGROUPPL == RHGROUPPLListOrder[i].IDRHGROUPPL)
            {
                RemoteHelp.Methods.RHGROUPPLATROLE_ListAdd(RHGROUPPLListOrder[i].RHGROUPPLATROLEList, RHGROUPPLATROLEListOrder[x]);

            }
            if (RHGROUPPLATROLEListOrder[x].IDRHGROUPPL > RHGROUPPLListOrder[i].IDRHGROUPPL)
            {
                break;
            }
        }
        for (var x = idxRHGROUPPLCPU; x < RHGROUPPLCPUListOrder.Count; x++)
        {
            idxRHGROUPPLCPU = x;
            if (RHGROUPPLCPUListOrder[x].IDRHGROUPPL == RHGROUPPLListOrder[i].IDRHGROUPPL)
            {
                RHGROUPPLCPUListOrder[x].RHGROUPPL = RHGROUPPLListOrder[i];                        
            }
            if (RHGROUPPLCPUListOrder[x].IDRHGROUPPL > RHGROUPPLListOrder[i].IDRHGROUPPL)
            {
                break;
            }
        }
    }
        

    //***************************************************************************************************************************************************************************
    //*************************** REQUEST ***************************************************************************************************************************************              
    var RHREQUESTListOrder = SysCfg.CopyList(RemoteHelpProfiler.RHREQUESTList).sort(function (a, b) { return a.IDRHREQUEST - b.IDRHREQUEST; });//Persistence.RemoteHelp.Properties.TRHREQUEST
    var RHREQUEST_CPUPLListOrder = SysCfg.CopyList(RemoteHelpProfiler.RHREQUEST_CPUPLList).sort(function (a, b) { return a.IDRHREQUEST_CPUPL - b.IDRHREQUEST_CPUPL; });//Persistence.RemoteHelp.Properties.TRHREQUEST_CPUPL
    var RHREQUEST_CPUCRListOrder = SysCfg.CopyList(RemoteHelpProfiler.RHREQUEST_CPUCRList).sort(function (a, b) { return a.IDRHREQUEST_CPUCR - b.IDRHREQUEST_CPUCR; });//Persistence.RemoteHelp.Properties.TRHREQUEST_CPUCR

    var idxCR = 0;
    var idxPL = 0;
    for (var i = 0; i < RHREQUESTListOrder.Count; i++)
    {
        if ((IDRHREQUEST == -1) || (IDRHREQUEST == RHREQUESTListOrder[i].IDRHREQUEST))
        {
            try
            {
                for (var x = idxCR; x < RHREQUEST_CPUCRListOrder.Count; x++)
                {
                    idxCR = x;
                    if (RHREQUEST_CPUCRListOrder[x].IDRHREQUEST == RHREQUESTListOrder[i].IDRHREQUEST)
                    {
                        RemoteHelp.Methods.RHREQUEST_CPUCR_ListAdd(RHREQUESTListOrder[i].RHREQUEST_CPUCRList, RHREQUEST_CPUCRListOrder[x]);
                    }
                    if (RHREQUEST_CPUCRListOrder[x].IDRHREQUEST > RHREQUESTListOrder[i].IDRHREQUEST)
                    {
                        break;
                    }
                }
                for (var x = idxPL; x < RHREQUEST_CPUPLListOrder.Count; x++)
                {
                    idxPL = x;
                    if (RHREQUEST_CPUPLListOrder[x].IDRHREQUEST == RHREQUEST_CPUPLListOrder[i].IDRHREQUEST)
                    {
                        RemoteHelp.Methods.RHREQUEST_CPUPL_ListAdd(RHREQUESTListOrder[i].RHREQUEST_CPUPLList, RHREQUEST_CPUPLListOrder[x]);

                    }
                    if (RHREQUEST_CPUPLListOrder[x].IDRHREQUEST > RHREQUESTListOrder[i].IDRHREQUEST)
                    {
                        break;
                    }
                }

            }
            catch (e)
            {
                SysCfg.Log.Methods.WriteLog("RemoteHelpMethods.StartReationRemoteHelp RemoteHelp index ", e);
                Throw;
            }

        }

    }

            
    var UserFunctionsListOrder = SysCfg.CopyList(UserFunctionsList).sort(function (a, b) { return a.IDCMDBCI - b.IDCMDBCI; });//TUserFunctions
    RHREQUESTListOrder = SysCfg.CopyList(RemoteHelpProfiler.RHREQUESTList).sort(function (a, b) { return a.IDCMDBCI - b.IDCMDBCI; });       
    var idxRHREQUEST = 0;
    for (var i = 0; i < UserFunctionsListOrder.Count; i++)
    {
        for (var x = idxRHREQUEST; x < RHREQUESTListOrder.Count; x++)
        {
            idxRHREQUEST = x;
            if (RHREQUESTListOrder[x].IDCMDBCI == UserFunctionsListOrder[i].IDCMDBCI)
            {
                RemoteHelp.Methods.RHREQUEST_ListAdd(UserFunctionsListOrder[i].RHREQUESTList, RHREQUESTListOrder[x]);
            }
            if (RHREQUESTListOrder[x].IDCMDBCI > UserFunctionsListOrder[i].IDCMDBCI)
            {
                break;
            }
        }
    }
}

Persistence.RemoteHelp.Methods.RHREQUESTCRtoStr = function( RHREQUESTCR)
{
    var Res = Persistence.RemoteHelp.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(RHREQUESTCRList.length, Longitud, Texto);
        for (Counter = 0; Counter < RHREQUESTCRList.length; Counter++) {
            var UnRHREQUESTCR = RHREQUESTCRList[Counter];
            UnRHREQUESTCR.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.RemoteHelp.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
}

Persistence.RemoteHelp.Methods.StrtoRHREQUESTCR = function(ProtocoloStr, RHREQUESTCR)
{
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        RHREQUESTCRList.length = 0;
        if (Persistence.RemoteHelp.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnRHREQUESTCR = new Persistence.Demo.Properties.TRHREQUESTCR(); //Mode new row
                UnRHREQUESTCR.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.RemoteHelp.Methods.RHREQUESTCR_ListAdd(RHREQUESTCRList, UnRHREQUESTCR);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.RHREQUESTPLtoStr = function(RHREQUESTPL)
{
    var Res = Persistence.RemoteHelp.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(RHREQUESTPLList.length, Longitud, Texto);
        for (Counter = 0; Counter < RHREQUESTPLList.length; Counter++) {
            var UnRHREQUESTPL = RHREQUESTPLList[Counter];
            UnRHREQUESTPL.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.RemoteHelp.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
}

Persistence.RemoteHelp.Methods.StrtoRHREQUESTPL = function(ProtocoloStr, RHREQUESTPL)
{
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        RHREQUESTPLList.length = 0;
        if (Persistence.RemoteHelp.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnRHREQUESTPL = new Persistence.Demo.Properties.TRHREQUESTPL(); //Mode new row
                UnRHREQUESTPL.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.RemoteHelp.Methods.RHREQUESTPL_ListAdd(RHREQUESTPLList, UnRHREQUESTPL);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}



Persistence.RemoteHelp.Methods.RHMODULE_ACTIVE_ListSetID = function(RHMODULE_ACTIVEList,IDCMDBCI,RHMODULE/*, Boolean IDRHMODULE_ACTIVE, DateTime IDRHMODULE_DATE*/)
{
    for (var i = 0; i < RHMODULE_ACTIVEList.Count; i++)
    {

        if ((RHMODULE == RHMODULE_ACTIVEList[i].IDRHMODULE) && (IDCMDBCI == RHMODULE_ACTIVEList[i].IDCMDBCI)/* && (IDRHMODULE_ACTIVE == RHMODULE_ACTIVEList[i].IDRHMODULE_ACTIVE) && (IDRHMODULE_DATE == RHMODULE_ACTIVEList[i].IDRHMODULE_DATE)*/)
            return (RHMODULE_ACTIVEList[i]);

    }
    var RHMODULE_ACTIVE = new Persistence.RemoteHelp.Properties.TRHMODULE_ACTIVE()
    RHMODULE_ACTIVE.IDRHMODULE = RHMODULE
    return (RHMODULE_ACTIVE);
}

Persistence.RemoteHelp.Methods.RHMODULE_ACTIVE_ListAdd = function(RHMODULE_ACTIVEList, RHMODULE_ACTIVE)
{
    var i = Persistence.RemoteHelp.Methods.RHMODULE_ACTIVE_ListGetIndex(RHMODULE_ACTIVEList, RHMODULE_ACTIVE);
    if (i == -1) RHMODULE_ACTIVEList.push(RHMODULE_ACTIVE);
    else
    {
        RHMODULE_ACTIVEList[i].IDRHMODULE = RHMODULE_ACTIVE.IDRHMODULE;
        RHMODULE_ACTIVEList[i].Enable = RHMODULE_ACTIVE.Enable;
        RHMODULE_ACTIVEList[i].LastUpdate = RHMODULE_ACTIVE.LastUpdate;
        RHMODULE_ACTIVEList[i].Counter = RHMODULE_ACTIVE.Counter;
        RHMODULE_ACTIVEList[i].IDRHMODULE_ACTIVE = RHMODULE_ACTIVE.IDRHMODULE_ACTIVE;
        RHMODULE_ACTIVEList[i].IDRHMODULE_DATE = RHMODULE_ACTIVE.IDRHMODULE_DATE;
        RHMODULE_ACTIVEList[i].IDCMDBCI = RHMODULE_ACTIVE.IDCMDBCI;
               
    }
}

Persistence.RemoteHelp.Methods.RHMODULE_ACTIVE_Copy = function(RHMODULE_ACTIVE_Source, RHMODULE_ACTIVE)
{
    RHMODULE_ACTIVE_Source.IDRHMODULE = RHMODULE_ACTIVE.IDRHMODULE;
    RHMODULE_ACTIVE_Source.Enable = RHMODULE_ACTIVE.Enable;
    RHMODULE_ACTIVE_Source.LastUpdate = RHMODULE_ACTIVE.LastUpdate;
    RHMODULE_ACTIVE_Source.Counter = RHMODULE_ACTIVE.Counter;
    RHMODULE_ACTIVE_Source.IDRHMODULE_ACTIVE = RHMODULE_ACTIVE.IDRHMODULE_ACTIVE;
    RHMODULE_ACTIVE_Source.IDRHMODULE_DATE = RHMODULE_ACTIVE.IDRHMODULE_DATE;
    RHMODULE_ACTIVE_Source.IDCMDBCI = RHMODULE_ACTIVE.IDCMDBCI;
}
 
//CJRC_26072018
Persistence.RemoteHelp.Methods.RHMODULE_ACTIVE_ListGetIndex = function (RHMODULE_ACTIVEList, Param1, Param2) {
    var Res = -1
    if (typeof (Param1) == 'number') {
        Res = Persistence.RemoteHelp.Methods.RHMODULE_ACTIVE_ListGetIndexRHMODULE(RHMODULE_ACTIVEList, Param1, Param2);

    }
    else {
        Res = Persistence.RemoteHelp.Methods.RHMODULE_ACTIVE_ListGetIndexRHMODULE_ACTIVE(RHMODULE_ACTIVEList, Param1);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.RHMODULE_ACTIVE_ListGetIndexRHMODULE_ACTIVE = function (RHMODULE_ACTIVEList, RHMODULE_ACTIVE)
{
    for (var i = 0; i < RHMODULE_ACTIVEList.Count; i++)
    {
        if ((RHMODULE_ACTIVE.IDRHMODULE == RHMODULE_ACTIVEList[i].IDRHMODULE) && (RHMODULE_ACTIVE.IDCMDBCI == RHMODULE_ACTIVEList[i].IDCMDBCI)/* && (RHMODULE_ACTIVE.IDRHMODULE_ACTIVE == RHMODULE_ACTIVEList[i].IDRHMODULE_ACTIVE) && (RHMODULE_ACTIVE.IDRHMODULE_DATE == RHMODULE_ACTIVEList[i].IDRHMODULE_DATE)*/)
            return (i);
    }
    return (-1);
}


Persistence.RemoteHelp.Methods.RHMODULE_ACTIVE_ListGetIndexRHMODULE = function (RHMODULE_ACTIVEList, IDCMDBCI, RHMODULE)
{
    for (var i = 0; i < RHMODULE_ACTIVEList.Count; i++)
    {
        if ((RHMODULE == RHMODULE_ACTIVEList[i].IDRHMODULE) && (IDCMDBCI == RHMODULE_ACTIVEList[i].IDCMDBCI))
            return (i);
    }
    return (-1);
}


//IDRHMODULE,IDRHMODULE_ACTIVE,IDRHMODULE_SET,IDRHMODULE_DATE
//Persistence.RemoteHelp.Methods.RHMODULE_ACTIVE_ListAddActive = function(TUserFunctions UserFunctions, Persistence.RemoteHelp.TRemoteHelpProfiler RemoteHelpProfiler, Int32 IDCMDBCI, Int32 IDRHMODULE, Boolean IDRHMODULE_ACTIVE, Int32 IDRHMODULE_SET, ref DateTime IDRHMODULE_DATE)
Persistence.RemoteHelp.Methods.RHMODULE_ACTIVE_ListAddActive = function(UserFunctions, RemoteHelpProfiler, IDCMDBCI, IDRHMODULE, IDRHMODULE_ACTIVE, IDRHMODULE_SET, IDRHMODULE_DATE)
{
            
    var RHMODULE_ACTIVE = new Properties.TRHMODULE_ACTIVE();
    RHMODULE_ACTIVE.IDRHMODULE = Properties.TRHMODULE.GetEnum(IDRHMODULE_SET);
    RHMODULE_ACTIVE.IDRHMODULE_ACTIVE = IDRHMODULE_ACTIVE;
    RHMODULE_ACTIVE.IDRHMODULE_DATE = IDRHMODULE_DATE;

    if (RHMODULE_ACTIVE.IDRHMODULE_ACTIVE)
    {
        var i = Persistence.RemoteHelp.Methods.RHMODULE_ACTIVE_ListGetIndex(UserFunctions.RHMODULE_ACTIVEList, RHMODULE_ACTIVE);
        if (i > -1)
        {
            if (RHMODULE_ACTIVE.IDRHMODULE_DATE > UserFunctions.RHMODULE_ACTIVEList[i].IDRHMODULE_DATE)
            {
                Persistence.RemoteHelp.Methods.RHMODULE_ACTIVE_ListAdd(UserFunctions.RHMODULE_ACTIVEList, RHMODULE_ACTIVE);
                Persistence.RemoteHelp.Methods.RHMODULE_ACTIVE_ListAdd(RemoteHelpProfiler.RHMODULE_ACTIVEList, RHMODULE_ACTIVE);
                        
            }
            else if (RHMODULE_ACTIVE.IDRHMODULE_DATE == UserFunctions.RHMODULE_ACTIVEList[i].IDRHMODULE_DATE)
            {
                RHMODULE_ACTIVE.Counter = 0;
                RHMODULE_ACTIVE.Enable = true;
                Persistence.RemoteHelp.Methods.RHMODULE_ACTIVE_ListAdd(UserFunctions.RHMODULE_ACTIVEList, RHMODULE_ACTIVE);

            }
            else
            {
                IDRHMODULE_DATE = UserFunctions.RHMODULE_ACTIVEList[i].IDRHMODULE_DATE;
                //no hace nada por que esta caduco 
            }

        }
        else
        {
            Persistence.RemoteHelp.Methods.RHMODULE_ACTIVE_ListAdd(UserFunctions.RHMODULE_ACTIVEList, RHMODULE_ACTIVE);
            Persistence.RemoteHelp.Methods.RHMODULE_ACTIVE_ListAdd(RemoteHelpProfiler.RHMODULE_ACTIVEList, RHMODULE_ACTIVE);
        }

    }
    /*var i = Persistence.RemoteHelp.Methods.RHMODULE_ACTIVE_ListGetIndex(RHMODULE_ACTIVEList, RHMODULE_ACTIVE);
      if (i == -1) RHMODULE_ACTIVEList.push(RHMODULE_ACTIVE);
      else
      {
          RHMODULE_ACTIVEList[i].IDRHMODULE = RHMODULE_ACTIVE.IDRHMODULE;
          RHMODULE_ACTIVEList[i].Enable = RHMODULE_ACTIVE.Enable;
          RHMODULE_ACTIVEList[i].LastUpdate = RHMODULE_ACTIVE.LastUpdate;
          RHMODULE_ACTIVEList[i].Counter = RHMODULE_ACTIVE.Counter;

      }*/
}

//public static void RHMODULE_ACTIVE_ListSeModuleActive(List<Persistence.RemoteHelp.Properties.TRHMODULE_ACTIVE> RHMODULE_ACTIVEList,Int32 IDCMDBCI, ref bool ITHelpcenterVisible, ref bool AtisVisible, ref bool RemoteHelpVisible)
Persistence.RemoteHelp.Methods.RHMODULE_ACTIVE_ListSeModuleActive = function(RHMODULE_ACTIVEList,IDCMDBCI,ITHelpcenterVisible,AtisVisible,RemoteHelpVisible)
{
    ITHelpcenterVisible = false;
    AtisVisible = false;
    RemoteHelpVisible = false;
    for (var i = 0; i < RHMODULE_ACTIVEList.Count; i++)
    {
        switch (RHMODULE_ACTIVEList[i].IDRHMODULE)
        {
            //case Properties.TRHMODULE._Default:
            //    break;
            case Properties.TRHMODULE._ATIS:
                AtisVisible = RHMODULE_ACTIVEList[i].Enable;
                break;
            case Properties.TRHMODULE._ITHelpCenter:
                ITHelpcenterVisible = RHMODULE_ACTIVEList[i].Enable;
                break;
                //case Properties.TRHMODULE._CMDB:
                //    break;
            case Properties.TRHMODULE._RemoteHelp:
                RemoteHelpVisible = RHMODULE_ACTIVEList[i].Enable;
                break;
            default:
                break;
        }
                
    }
}
        
//Socket Function 
Persistence.RemoteHelp.Methods.RHMODULE_ACTIVEListToByte = function(RHMODULE_ACTIVEList,MemStream, idx)
{
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, RHMODULE_ACTIVEList.Count - idx);
    for (var i = idx; i < RHMODULE_ACTIVEList.Count; i++)
    {
        RHMODULE_ACTIVEList[i].ToByte(MemStream);
    }
}

Persistence.RemoteHelp.Methods.ByteToRHMODULE_ACTIVEList = function(RHMODULE_ACTIVEList, MemStream)
{
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (var i = 0; i < Count; i++) {
        var UnRHMODULE_ACTIVE = new Persistence.RemoteHelp.Properties.TRHMODULE_ACTIVE();
        UnRHMODULE_ACTIVE.ByteTo(MemStream);
        Persistence.RemoteHelp.Methods.RHMODULE_ACTIVE_ListAdd(RHMODULE_ACTIVEList, UnRHMODULE_ACTIVE);
    }
}

 
