﻿//Persistence.RemoteHelp
Persistence.RemoteHelp.TRemoteHelpGroup =
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    RemoteOperations: { value: 1, name: "RemoteOperations" },
    OtherOptions: { value: 2, name: "OtherOptions" },
    Policies: { value: 3, name: "Policies" },
    File: { value: 4, name: "File" },
    RemoteControl: { value: 5, name: "RemoteControl" },
    Messaging: { value: 6, name: "Messaging" },
}


Persistence.RemoteHelp.RHCONFIG = function () {
    //RemoteOperations		
    this.isIntelVProTechnology = false;
    this.isSurvey = false;
    this.isGenerateInventory = false;
    this.isUpdateAgent = false;
    this.isChangeID = false;
    this.isLookDangerous = false;

    //OtherOptions		
    this.isDeletePC = false;
    this.isInformation = false;
    this.isRunSDFile = false;

    //Policies			
    this.isUpdatePolicies = false;
    this.isRestartAllowed = false;

    //File				
    this.isSendingFilesandOrders = false;
    this.isRunLocally = false;

    //RemoteControl	
    this.isPoweronPC = false;
    this.isUseBrowser = false;
    this.isTakeControl = false;
    this.isJustSee = false;
    this.isLockKeyboard = false;
    this.isDonotAskforPermission = false;
    this.isLowBandwidth = false;
    this.isRecordSession = false;

    //Messaging			
    this.isChat = false;
    this.isVoice = false;
    this.isSendMessage = false;

    this.hasPermissions = false;


    this.RHCONFIG = function (REMOTEHELPCONFIG) {
        if (REMOTEHELPCONFIG != "") {
            var grupos = REMOTEHELPCONFIG.Split(';');

            //RemoteOperations		
            this.isIntelVProTechnology = GetConfigValue(grupos, "IntelVProTechnology");
            this.isSurvey = GetConfigValue(grupos, "Survey");
            this.isGenerateInventory = GetConfigValue(grupos, "GenerateInventory");
            this.isUpdateAgent = GetConfigValue(grupos, "UpdateAgent");
            this.isChangeID = GetConfigValue(grupos, "ChangeID");
            this.isLookDangerous = GetConfigValue(grupos, "LookDangerous");

            //OtherOptions		
            this.isDeletePC = GetConfigValue(grupos, "DeletePC");
            this.isInformation = GetConfigValue(grupos, "Information");
            this.isRunSDFile = GetConfigValue(grupos, "RunSDFile");

            //Policies			
            this.isUpdatePolicies = GetConfigValue(grupos, "UpdatePolicies");
            this.isRestartAllowed = GetConfigValue(grupos, "RestartAllowed");

            //File				
            this.isSendingFilesandOrders = GetConfigValue(grupos, "SendingFilesandOrders");
            this.isRunLocally = GetConfigValue(grupos, "RunLocally");

            //RemoteControl	
            this.isPoweronPC = GetConfigValue(grupos, "PoweronPC");
            this.isUseBrowser = GetConfigValue(grupos, "UseBrowser");
            this.isTakeControl = GetConfigValue(grupos, "TakeControl");
            this.isJustSee = GetConfigValue(grupos, "JustSee");
            this.isLockKeyboard = GetConfigValue(grupos, "LockKeyboard");
            this.isDonotAskforPermission = GetConfigValue(grupos, "DonotAskforPermission");
            this.isLowBandwidth = GetConfigValue(grupos, "LowBandwidth");
            this.isRecordSession = GetConfigValue(grupos, "RecordSession");

            //Messaging			
            this.isChat = GetConfigValue(grupos, "Chat");
            this.isVoice = GetConfigValue(grupos, "Voice");
            this.isSendMessage = GetConfigValue(grupos, "SendMessage");


            if (isIntelVProTechnology == true ||
                isSurvey == true ||
                isGenerateInventory == true ||
                isUpdateAgent == true ||
                isChangeID == true ||
                isLookDangerous == true ||
                isDeletePC == true ||
                isInformation == true ||
                isRunSDFile == true ||
                isUpdatePolicies == true ||
                isRestartAllowed == true ||
                isSendingFilesandOrders == true ||
                isRunLocally == true ||
                isPoweronPC == true ||
                isUseBrowser == true ||
                isTakeControl == true ||
                isJustSee == true ||
                isLockKeyboard == true ||
                isDonotAskforPermission == true ||
                isLowBandwidth == true ||
                isRecordSession == true ||
                isChat == true ||
                isVoice == true ||
                isSendMessage == true) {
                this.hasPermissions = true;
            }

        }
    }

    this.GetConfigValue = function (grupos, nombrePropiedad) {
        var resp = false;
        for (var i = 0; i < grupos.Length; i++) {
            var opciones = grupos[i].Split('=');
            if (opciones[0].ToLower() == nombrePropiedad.ToLower()) {
                resp = (opciones[1] == "0" ? false : true);
                break;
            }
        }
        return resp;
    }

    this.IsGroupVisible = function (remoteHelpGroup) {
        var resp = true;
        switch (remoteHelpGroup) {
            case Persistence.RemoteHelp.TRemoteHelpGroup.RemoteOperations:
                if (isIntelVProTechnology == false &&
                        isSurvey == false &&
                        isGenerateInventory == false &&
                        isUpdateAgent == false &&
                        isChangeID == false &&
                        isLookDangerous == false)
                    resp = false;
                break;
            case Persistence.RemoteHelp.TRemoteHelpGroup.OtherOptions:
                if (isDeletePC == false &&
                        isInformation == false &&
                        isRunSDFile == false)
                    resp = false;
                break;
            case Persistence.RemoteHelp.TRemoteHelpGroup.Policies:
                if (isUpdatePolicies == false &&
                        isRestartAllowed == false)
                    resp = false;
                break;
            case Persistence.RemoteHelp.TRemoteHelpGroup.File:
                if (isSendingFilesandOrders == false &&
                        isRunLocally == false)
                    resp = false;
                break;
            case Persistence.RemoteHelp.TRemoteHelpGroup.RemoteControl:
                if (isPoweronPC == false &&
                        isUseBrowser == false &&
                        isTakeControl == false &&
                        isJustSee == false &&
                        isLockKeyboard == false &&
                        isDonotAskforPermission == false &&
                        isLowBandwidth == false &&
                        isRecordSession == false)
                    resp = false;
                break;
            case Persistence.RemoteHelp.TRemoteHelpGroup.Messaging:
                if (isChat == false &&
                        isVoice == false &&
                        isSendMessage == false)
                    resp = false;
                break;
            default:
                break;
        }
        return resp;
    }
}