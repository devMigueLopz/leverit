﻿//TreduceDB
//Persistence.RemoteHelp.Properties
//Persistence.RemoteHelp.Methods

Persistence.RemoteHelp.Properties.TRHGROUPPL_CFGBACKUP = function () {
    this.IDRHGROUPPL_CFGBACKUP = 0;
    this.CFGBACKUP_NAME = "";
    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDRHGROUPPL_CFGBACKUP);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CFGBACKUP_NAME);
    }
    this.ByteTo = function (MemStream) {
        this.IDRHGROUPPL_CFGBACKUP = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.CFGBACKUP_NAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDRHGROUPPL_CFGBACKUP, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.CFGBACKUP_NAME, Longitud, Texto);
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDRHGROUPPL_CFGBACKUP = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.CFGBACKUP_NAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
    }
}

//**********************   METHODS server  ********************************************************************************************

Persistence.RemoteHelp.Methods.RHGROUPPL_CFGBACKUP_Fill = function (RHGROUPPL_CFGBACKUPList) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    RHGROUPPL_CFGBACKUPList.length = 0;
    try {
        var DS_RHGROUPPL_CFGBACKUP = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "RHGROUPPL_CFGBACKUP_GET", new Array());
        ResErr = DS_RHGROUPPL_CFGBACKUP.ResErr;
        if (ResErr.NotError) {
            if (DS_RHGROUPPL_CFGBACKUP.DataSet.RecordCount > 0) {
                DS_RHGROUPPL_CFGBACKUP.DataSet.First();
                while (!(DS_RHGROUPPL_CFGBACKUP.DataSet.Eof)) {
                    var RHGROUPPL_CFGBACKUP = new Persistence.RemoteHelp.Properties.TRHGROUPPL_CFGBACKUP();
                    RHGROUPPL_CFGBACKUP.IDRHGROUPPL_CFGBACKUP = DS_RHGROUPPL_CFGBACKUP.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL_CFGBACKUP.IDRHGROUPPL_CFGBACKUP.FieldName).asInt32();
                    RHGROUPPL_CFGBACKUP.CFGBACKUP_NAME = DS_RHGROUPPL_CFGBACKUP.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL_CFGBACKUP.CFGBACKUP_NAME.FieldName).asString();
                    RHGROUPPL_CFGBACKUPList.push(RHGROUPPL_CFGBACKUP);

                    DS_RHGROUPPL_CFGBACKUP.DataSet.Next();
                }
            }
            else {
                DS_RHGROUPPL_CFGBACKUP.ResErr.NotError = false;
                DS_RHGROUPPL_CFGBACKUP.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPPL_CFGBACKUP_GETID = function (RHGROUPPL_CFGBACKUP) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPPL_CFGBACKUP.CFGBACKUP_NAME.FieldName, RHGROUPPL_CFGBACKUP.CFGBACKUP_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var DS_RHGROUPPL_CFGBACKUP = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "RHGROUPPL_CFGBACKUP_GETID", Param.ToBytes());
        ResErr = DS_RHGROUPPL_CFGBACKUP.ResErr;
        if (ResErr.NotError) {
            if (DS_RHGROUPPL_CFGBACKUP.DataSet.RecordCount > 0) {
                DS_RHGROUPPL_CFGBACKUP.DataSet.First();
                RHGROUPPL_CFGBACKUP.IDRHGROUPPL_CFGBACKUP = DS_RHGROUPPL_CFGBACKUP.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL_CFGBACKUP.IDRHGROUPPL_CFGBACKUP.FieldName).asInt32();
                //Other
                RHGROUPPL_CFGBACKUP.CFGBACKUP_NAME = DS_RHGROUPPL_CFGBACKUP.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL_CFGBACKUP.CFGBACKUP_NAME.FieldName).asString();
            }
            else {
                DS_RHGROUPPL_CFGBACKUP.ResErr.NotError = false;
                DS_RHGROUPPL_CFGBACKUP.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPPL_CFGBACKUP_GETBYID = function (RHGROUPPL_CFGBACKUP, Id) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPL_CFGBACKUP.IDRHGROUPPL_CFGBACKUP.FieldName, Id, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var DS_RHGROUPPL_CFGBACKUP = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "RHGROUPPL_CFGBACKUP_GETBYID", Param.ToBytes());
        ResErr = DS_RHGROUPPL_CFGBACKUP.ResErr;
        if (ResErr.NotError) {
            if (DS_RHGROUPPL_CFGBACKUP.DataSet.RecordCount > 0) {
                DS_RHGROUPPL_CFGBACKUP.DataSet.First();
                RHGROUPPL_CFGBACKUP.IDRHGROUPPL_CFGBACKUP = DS_RHGROUPPL_CFGBACKUP.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL_CFGBACKUP.IDRHGROUPPL_CFGBACKUP.FieldName).asInt32();
                RHGROUPPL_CFGBACKUP.CFGBACKUP_NAME = DS_RHGROUPPL_CFGBACKUP.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL_CFGBACKUP.CFGBACKUP_NAME.FieldName).asString();
            }
            else {
                DS_RHGROUPPL_CFGBACKUP.ResErr.NotError = false;
                DS_RHGROUPPL_CFGBACKUP.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPPL_CFGBACKUP_ADD = function (RHGROUPPL_CFGBACKUP) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPL_CFGBACKUP.IDRHGROUPPL_CFGBACKUP.FieldName, RHGROUPPL_CFGBACKUP.IDRHGROUPPL_CFGBACKUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPPL_CFGBACKUP.CFGBACKUP_NAME.FieldName, RHGROUPPL_CFGBACKUP.CFGBACKUP_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHGROUPPL_CFGBACKUP_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.RemoteHelp.Methods.RHGROUPPL_CFGBACKUP_GETID(RHGROUPPL_CFGBACKUP);
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPPL_CFGBACKUP_UPD = function (RHGROUPPL_CFGBACKUP) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPL_CFGBACKUP.IDRHGROUPPL_CFGBACKUP.FieldName, RHGROUPPL_CFGBACKUP.IDRHGROUPPL_CFGBACKUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPPL_CFGBACKUP.CFGBACKUP_NAME.FieldName, RHGROUPPL_CFGBACKUP.CFGBACKUP_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHGROUPPL_CFGBACKUP_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.RemoteHelp.Methods.RHGROUPPL_CFGBACKUP_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.RemoteHelp.Methods.RHGROUPPL_CFGBACKUP_DELIDRHGROUPPL_CFGBACKUP(Param);
    }
    else {
        Res = Persistence.RemoteHelp.Methods.RHGROUPPL_CFGBACKUP_DELRHGROUPPL_CFGBACKUP(Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.RHGROUPPL_CFGBACKUP_DELIDRHGROUPPL_CFGBACKUP = function (IDRHGROUPPL_CFGBACKUP) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPL_CFGBACKUP.IDRHGROUPPL_CFGBACKUP.FieldName, IDRHGROUPPL_CFGBACKUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHGROUPPL_CFGBACKUP_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPPL_CFGBACKUP_DELRHGROUPPL_CFGBACKUP = function (RHGROUPPL_CFGBACKUP) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPL_CFGBACKUP.IDRHGROUPPL_CFGBACKUP.FieldName, RHGROUPPL_CFGBACKUP.IDRHGROUPPL_CFGBACKUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPPL_CFGBACKUP.CFGBACKUP_NAME.FieldName, RHGROUPPL_CFGBACKUP.CFGBACKUP_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHGROUPPL_CFGBACKUP_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}


//**********************   METHODS   ********************************************************************************************

Persistence.RemoteHelp.Methods.RHGROUPPL_CFGBACKUP_ListSetID = function (RHGROUPPL_CFGBACKUPList, IDRHGROUPPL_CFGBACKUP) {
    for (var i = 0; i < RHGROUPPL_CFGBACKUPList.length; i++) {

        if (IDRHGROUPPL_CFGBACKUP == RHGROUPPL_CFGBACKUPList[i].IDRHGROUPPL_CFGBACKUP)
            return (RHGROUPPL_CFGBACKUPList[i]);

    }
    var UnRHGROUPPL_CFGBACKUP = new Persistence.RemoteHelp.Properties.TRHGROUPPL_CFGBACKUP();
    UnRHGROUPPL_CFGBACKUP.IDRHGROUPPL_CFGBACKUP = IDRHGROUPPL_CFGBACKUP;

    return (UnRHGROUPPL_CFGBACKUP);
}
Persistence.RemoteHelp.Methods.RHGROUPPL_CFGBACKUP_ListAdd = function (RHGROUPPL_CFGBACKUPList, RHGROUPPL_CFGBACKUP) {
    var i = Persistence.RemoteHelp.Methods.RHGROUPPL_CFGBACKUP_ListGetIndex(RHGROUPPL_CFGBACKUPList, RHGROUPPL_CFGBACKUP);
    if (i == -1) RHGROUPPL_CFGBACKUPList.push(RHGROUPPL_CFGBACKUP);
    else {
        RHGROUPPL_CFGBACKUPList[i].IDRHGROUPPL_CFGBACKUP = RHGROUPPL_CFGBACKUP.IDRHGROUPPL_CFGBACKUP;
        RHGROUPPL_CFGBACKUPList[i].CFGBACKUP_NAME = RHGROUPPL_CFGBACKUP.CFGBACKUP_NAME;
    }
}

//CJRC_26072018
Persistence.RemoteHelp.Methods.RHGROUPPL_CFGBACKUP_ListGetIndex = function (RHGROUPPL_CFGBACKUPList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.RemoteHelp.Methods.RHGROUPPL_CFGBACKUP_ListGetIndexIDRHGROUPPL_CFGBACKUP(RHGROUPPL_CFGBACKUPList, Param) ;

    }
    else {
        Res = Persistence.RemoteHelp.Methods.RHGROUPPL_CFGBACKUP_ListGetIndexRHGROUPPL_CFGBACKUP(RHGROUPPL_CFGBACKUPList, Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.RHGROUPPL_CFGBACKUP_ListGetIndexRHGROUPPL_CFGBACKUP = function (RHGROUPPL_CFGBACKUPList, RHGROUPPL_CFGBACKUP) {
    for (var i = 0; i < RHGROUPPL_CFGBACKUPList.length; i++) {
        if (RHGROUPPL_CFGBACKUP.IDRHGROUPPL_CFGBACKUP == RHGROUPPL_CFGBACKUPList[i].IDRHGROUPPL_CFGBACKUP)
            return (i);
    }
    return (-1);
}
Persistence.RemoteHelp.Methods.RHGROUPPL_CFGBACKUP_ListGetIndexIDRHGROUPPL_CFGBACKUP = function (RHGROUPPL_CFGBACKUPList, IDRHGROUPPL_CFGBACKUP) {
    for (var i = 0; i < RHGROUPPL_CFGBACKUPList.length; i++) {
        if (IDRHGROUPPL_CFGBACKUP == RHGROUPPL_CFGBACKUPList[i].IDRHGROUPPL_CFGBACKUP)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.RemoteHelp.Methods.RHGROUPPL_CFGBACKUPListtoStr = function (RHGROUPPL_CFGBACKUPList) {
    var Res = Persistence.RemoteHelp.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(RHGROUPPL_CFGBACKUPList.length, Longitud, Texto);
        for (Counter = 0; Counter < RHGROUPPL_CFGBACKUPList.length; Counter++) {
            var UnRHGROUPPL_CFGBACKUP = RHGROUPPL_CFGBACKUPList[Counter];
            UnRHGROUPPL_CFGBACKUP.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.RemoteHelp.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.RemoteHelp.Methods.StrtoRHGROUPPL_CFGBACKUP = function (ProtocoloStr, RHGROUPPL_CFGBACKUPList) {
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        RHGROUPPL_CFGBACKUPList.length = 0;
        if (Persistence.RemoteHelp.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnRHGROUPPL_CFGBACKUP = new Persistence.Demo.Properties.TRHGROUPPL_CFGBACKUP(); //Mode new row
                UnRHGROUPPL_CFGBACKUP.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.RHGROUPPL_CFGBACKUP_ListAdd(RHGROUPPL_CFGBACKUPList, UnRHGROUPPL_CFGBACKUP);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}

//Socket Function 
Persistence.RemoteHelp.Methods.RHGROUPPL_CFGBACKUPListToByte = function (RHGROUPPL_CFGBACKUPList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, RHGROUPPL_CFGBACKUPList.length - idx);
    for (var i = idx; i < RHGROUPPL_CFGBACKUPList.length; i++) {
        RHGROUPPL_CFGBACKUPList[i].ToByte(MemStream);
    }
}
Persistence.RemoteHelp.Methods.ByteToRHGROUPPL_CFGBACKUPList = function (RHGROUPPL_CFGBACKUPList, MemStream) {
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (var i = 0; i < Count; i++) {
        var UnRHGROUPPL_CFGBACKUP = new Persistence.RemoteHelp.Properties.TRHGROUPPL_CFGBACKUP();
        UnRHGROUPPL_CFGBACKUP.ByteTo(MemStream);
        Persistence.RemoteHelp.Methods.RHGROUPPL_CFGBACKUP_ListAdd(RHGROUPPL_CFGBACKUPList, UnRHGROUPPL_CFGBACKUP);
    }
}
