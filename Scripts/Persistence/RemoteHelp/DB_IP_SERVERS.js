﻿//TreduceDB
//Persistence.RemoteHelp.Properties
//Persistence.RemoteHelp.Methods

Persistence.RemoteHelp.Properties.TIP_SERVERS = function () {
    this.COMPANY_NAME = "";
    this.IP_MIRROR = "";
    this.IP_SECURE_SERVER = "";
    this.IP_CONTROLR = "";
    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.COMPANY_NAME);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.IP_MIRROR);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.IP_SECURE_SERVER);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.IP_CONTROLR);
    }
    this.ByteTo = function (MemStream) {
        this.COMPANY_NAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IP_MIRROR = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IP_SECURE_SERVER = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IP_CONTROLR = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteStr(this.COMPANY_NAME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.IP_MIRROR, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.IP_SECURE_SERVER, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.IP_CONTROLR, Longitud, Texto);
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.COMPANY_NAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.IP_MIRROR = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.IP_SECURE_SERVER = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.IP_CONTROLR = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
    }

}

//**********************   METHODS server  ********************************************************************************************

Persistence.RemoteHelp.Methods.IP_SERVERS_Fill = function (IP_SERVERSList, StrCOMPANY_NAME/*COMPANY_NAME*/) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    IP_SERVERSList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrCOMPANY_NAME == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.IP_SERVERS.COMPANY_NAME.FieldName, " = " + UsrCfg.InternoAtisNames.IP_SERVERS.NAME_TABLE + "." + UsrCfg.InternoAtisNames.IP_SERVERS.COMPANY_NAME.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrCOMPANY_NAME = " IN (" + StrCOMPANY_NAME + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.IP_SERVERS.COMPANY_NAME.FieldName, StrCOMPANY_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //Param.AddInt32(UsrCfg.InternoAtisNames.IP_SERVERS.COMPANY_NAME.FieldName, COMPANY_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {
        /*




        */
        var DS_IP_SERVERS = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "IP_SERVERS_GET", Param.ToBytes());
        ResErr = DS_IP_SERVERS.ResErr;
        if (ResErr.NotError) {
            if (DS_IP_SERVERS.DataSet.RecordCount > 0) {
                DS_IP_SERVERS.DataSet.First();
                while (!(DS_IP_SERVERS.DataSet.Eof)) {
                    var IP_SERVERS = new Persistence.RemoteHelp.Properties.TIP_SERVERS();
                    //Other
                    IP_SERVERS.COMPANY_NAME = DS_IP_SERVERS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.IP_SERVERS.COMPANY_NAME.FieldName).asString();
                    IP_SERVERS.IP_MIRROR = DS_IP_SERVERS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.IP_SERVERS.IP_MIRROR.FieldName).asString();
                    IP_SERVERS.IP_SECURE_SERVER = DS_IP_SERVERS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.IP_SERVERS.IP_SECURE_SERVER.FieldName).asString();
                    IP_SERVERS.IP_CONTROLR = DS_IP_SERVERS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.IP_SERVERS.IP_CONTROLR.FieldName).asString();
                    IP_SERVERSList.push(IP_SERVERS);

                    DS_IP_SERVERS.DataSet.Next();
                }
            }
            else {
                DS_IP_SERVERS.ResErr.NotError = false;
                DS_IP_SERVERS.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.IP_SERVERS_GETID = function (IP_SERVERS) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.IP_SERVERS.COMPANY_NAME.FieldName, IP_SERVERS.COMPANY_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP_SERVERS.IP_MIRROR.FieldName, IP_SERVERS.IP_MIRROR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP_SERVERS.IP_SECURE_SERVER.FieldName, IP_SERVERS.IP_SECURE_SERVER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP_SERVERS.IP_CONTROLR.FieldName, IP_SERVERS.IP_CONTROLR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   IP_SERVERS_GETID   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"IP_SERVERS_GETID", @"IP_SERVERS_GETID description.");
    UnSQL.SQL.Add(@" SELECT  ");
    UnSQL.SQL.Add(@"  IP_SERVERS.COMPANY_NAME, ");
    UnSQL.SQL.Add(@"  IP_SERVERS.IP_MIRROR, ");
    UnSQL.SQL.Add(@"  IP_SERVERS.IP_SECURE_SERVER, ");
    UnSQL.SQL.Add(@"  IP_SERVERS.IP_CONTROLR ");
    UnSQL.SQL.Add(@" FROM  IP_SERVERS ");
    UnSQL.SQL.Add(@" WHERE   ");
    UnSQL.SQL.Add(@"  IP_SERVERS.COMPANY_NAME=@[COMPANY_NAME] and  ");
    UnSQL.SQL.Add(@"  IP_SERVERS.IP_MIRROR=@[IP_MIRROR] and  ");
    UnSQL.SQL.Add(@"  IP_SERVERS.IP_SECURE_SERVER=@[IP_SECURE_SERVER] and ");
    UnSQL.SQL.Add(@"  IP_SERVERS.IP_CONTROLR=@[IP_CONTROLR] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_IP_SERVERS = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "IP_SERVERS_GETID", Param.ToBytes());
        ResErr = DS_IP_SERVERS.ResErr;
        if (ResErr.NotError) {
            if (DS_IP_SERVERS.DataSet.RecordCount > 0) {
                DS_IP_SERVERS.DataSet.First();
                //Other
                IP_SERVERS.COMPANY_NAME = DS_IP_SERVERS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.IP_SERVERS.COMPANY_NAME.FieldName).asString();
                IP_SERVERS.IP_MIRROR = DS_IP_SERVERS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.IP_SERVERS.IP_MIRROR.FieldName).asString();
                IP_SERVERS.IP_SECURE_SERVER = DS_IP_SERVERS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.IP_SERVERS.IP_SECURE_SERVER.FieldName).asString();
                IP_SERVERS.IP_CONTROLR = DS_IP_SERVERS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.IP_SERVERS.IP_CONTROLR.FieldName).asString();
            }
            else {
                DS_IP_SERVERS.ResErr.NotError = false;
                DS_IP_SERVERS.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.IP_SERVERS_ADD = function (IP_SERVERS) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.IP_SERVERS.COMPANY_NAME.FieldName, IP_SERVERS.COMPANY_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP_SERVERS.IP_MIRROR.FieldName, IP_SERVERS.IP_MIRROR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP_SERVERS.IP_SECURE_SERVER.FieldName, IP_SERVERS.IP_SECURE_SERVER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP_SERVERS.IP_CONTROLR.FieldName, IP_SERVERS.IP_CONTROLR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   IP_SERVERS_ADD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"IP_SERVERS_ADD", @"IP_SERVERS_ADD description.");
    UnSQL.SQL.Add(@" INSERT INTO IP_SERVERS( ");
    UnSQL.SQL.Add(@"  COMPANY_NAME,  ");
    UnSQL.SQL.Add(@"  IP_MIRROR,  ");
    UnSQL.SQL.Add(@"  IP_SECURE_SERVER,  ");
    UnSQL.SQL.Add(@"  IP_CONTROLR ");
    UnSQL.SQL.Add(@" )  ");
    UnSQL.SQL.Add(@" VALUES ( ");
    UnSQL.SQL.Add(@"  @[COMPANY_NAME], ");
    UnSQL.SQL.Add(@"  @[IP_MIRROR], ");
    UnSQL.SQL.Add(@"  @[IP_SECURE_SERVER], ");
    UnSQL.SQL.Add(@"  @[IP_CONTROLR] ");
    UnSQL.SQL.Add(@" ) ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "IP_SERVERS_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.RemoteHelp.Methods.IP_SERVERS_GETID(IP_SERVERS);
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.IP_SERVERS_UPD = function (IP_SERVERS) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.IP_SERVERS.COMPANY_NAME.FieldName, IP_SERVERS.COMPANY_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP_SERVERS.IP_MIRROR.FieldName, IP_SERVERS.IP_MIRROR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP_SERVERS.IP_SECURE_SERVER.FieldName, IP_SERVERS.IP_SECURE_SERVER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP_SERVERS.IP_CONTROLR.FieldName, IP_SERVERS.IP_CONTROLR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   IP_SERVERS_UPD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"IP_SERVERS_UPD", @"IP_SERVERS_UPD description.");
    UnSQL.SQL.Add(@" UPDATE IP_SERVERS SET ");
    UnSQL.SQL.Add(@"  COMPANY_NAME=@[COMPANY_NAME],  ");
    UnSQL.SQL.Add(@"  IP_MIRROR=@[IP_MIRROR],  ");
    UnSQL.SQL.Add(@"  IP_SECURE_SERVER=@[IP_SECURE_SERVER],  ");
    UnSQL.SQL.Add(@"  IP_CONTROLR=@[IP_CONTROLR] ");
    UnSQL.SQL.Add(@" WHERE   ");
    UnSQL.SQL.Add(@"  COMPANY_NAME=@[COMPANY_NAME] AND  ");
    UnSQL.SQL.Add(@"  IP_MIRROR=@[IP_MIRROR] AND  ");
    UnSQL.SQL.Add(@"  IP_SECURE_SERVER=@[IP_SECURE_SERVER] AND  ");
    UnSQL.SQL.Add(@"  IP_CONTROLR=@[IP_CONTROLR] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "IP_SERVERS_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.RemoteHelp.Methods.IP_SERVERS_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.RemoteHelp.Methods.IP_SERVERS_DELCOMPANY_NAME(/*String StrCOMPANY_NAME*/Param);
    }
    else {
        Res = Persistence.RemoteHelp.Methods.IP_SERVERS_DELIP_SERVERS(Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.IP_SERVERS_DELCOMPANY_NAME = function (/*String StrCOMPANY_NAME*/COMPANY_NAME) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrCOMPANY_NAME == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.IP_SERVERS.COMPANY_NAME.FieldName, " = " + UsrCfg.InternoAtisNames.IP_SERVERS.NAME_TABLE + "." + UsrCfg.InternoAtisNames.IP_SERVERS.COMPANY_NAME.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrCOMPANY_NAME = " IN (" + StrCOMPANY_NAME + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.IP_SERVERS.COMPANY_NAME.FieldName, StrCOMPANY_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.IP_SERVERS.COMPANY_NAME.FieldName, COMPANY_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   IP_SERVERS_DEL_1   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"IP_SERVERS_DEL_1", @"IP_SERVERS_DEL_1 description.");
    UnSQL.SQL.Add(@" DELETE IP_SERVERS  ");
    UnSQL.SQL.Add(@" WHERE   ");
    UnSQL.SQL.Add(@"  COMPANY_NAME=@[COMPANY_NAME] AND  ");
    UnSQL.SQL.Add(@"  IP_MIRROR=@[IP_MIRROR] AND  ");
    UnSQL.SQL.Add(@"  IP_SECURE_SERVER=@[IP_SECURE_SERVER] AND  ");
    UnSQL.SQL.Add(@"  IP_CONTROLR=@[IP_CONTROLR] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "IP_SERVERS_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.IP_SERVERS_DELIP_SERVERS = function (IP_SERVERS) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.IP_SERVERS.COMPANY_NAME.FieldName, IP_SERVERS.COMPANY_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP_SERVERS.IP_MIRROR.FieldName, IP_SERVERS.IP_MIRROR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP_SERVERS.IP_SECURE_SERVER.FieldName, IP_SERVERS.IP_SECURE_SERVER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP_SERVERS.IP_CONTROLR.FieldName, IP_SERVERS.IP_CONTROLR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   IP_SERVERS_DEL_2   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"IP_SERVERS_DEL_2", @"IP_SERVERS_DEL_2 description.");
    UnSQL.SQL.Add(@" DELETE IP_SERVERS  ");
    UnSQL.SQL.Add(@" WHERE   ");
    UnSQL.SQL.Add(@"  COMPANY_NAME=@[COMPANY_NAME] AND  ");
    UnSQL.SQL.Add(@"  IP_MIRROR=@[IP_MIRROR] AND  ");
    UnSQL.SQL.Add(@"  IP_SECURE_SERVER=@[IP_SECURE_SERVER] AND  ");
    UnSQL.SQL.Add(@"  IP_CONTROLR=@[IP_CONTROLR] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "IP_SERVERS_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}


//**********************   METHODS   ********************************************************************************************

//List Function 
Persistence.RemoteHelp.Methods.IP_SERVERS_ListSetID = function (IP_SERVERSList, COMPANY_NAME) {
    for (var i = 0; i < IP_SERVERSList.length; i++) {

        if (COMPANY_NAME == IP_SERVERSList[i].COMPANY_NAME)
            return (IP_SERVERSList[i]);

    }
    var UnIP_SERVERS = new Persistence.RemoteHelp.Properties.TIP_SERVERS();
    UnIP_SERVERS.COMPANY_NAME = COMPANY_NAME;
    return (UnIP_SERVERS);
}
Persistence.RemoteHelp.Methods.IP_SERVERS_ListAdd = function (IP_SERVERSList, IP_SERVERS) {
    var i = Persistence.RemoteHelp.Methods.IP_SERVERS_ListGetIndex(IP_SERVERSList, IP_SERVERS);
    if (i == -1) IP_SERVERSList.push(IP_SERVERS);
    else {
        IP_SERVERSList[i].COMPANY_NAME = IP_SERVERS.COMPANY_NAME;
        IP_SERVERSList[i].IP_MIRROR = IP_SERVERS.IP_MIRROR;
        IP_SERVERSList[i].IP_SECURE_SERVER = IP_SERVERS.IP_SECURE_SERVER;
        IP_SERVERSList[i].IP_CONTROLR = IP_SERVERS.IP_CONTROLR;

    }
}

//CJRC_26072018
Persistence.RemoteHelp.Methods.IP_SERVERS_ListGetIndex = function (IP_SERVERSList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.RemoteHelp.Methods.IP_SERVERS_ListGetIndexCOMPANY_NAME(IP_SERVERSList, Param);

    }
    else {
        Res = Persistence.RemoteHelp.Methods.IP_SERVERS_ListGetIndexIP_SERVERS(IP_SERVERSList, Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.IP_SERVERS_ListGetIndexIP_SERVERS = function (IP_SERVERSList, IP_SERVERS) {
    for (var i = 0; i < IP_SERVERSList.length; i++) {
        if (IP_SERVERS.COMPANY_NAME == IP_SERVERSList[i].COMPANY_NAME)
            return (i);
    }
    return (-1);
}
Persistence.RemoteHelp.Methods.IP_SERVERS_ListGetIndexCOMPANY_NAME = function (IP_SERVERSList, COMPANY_NAME) {
    for (var i = 0; i < IP_SERVERSList.length; i++) {
        if (COMPANY_NAME == IP_SERVERSList[i].COMPANY_NAME)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.RemoteHelp.Methods.IP_SERVERSListtoStr = function (IP_SERVERSList) {
    var Res = Persistence.RemoteHelp.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(IP_SERVERSList.length, Longitud, Texto);
        for (Counter = 0; Counter < IP_SERVERSList.length; Counter++) {
            var UnIP_SERVERS = IP_SERVERSList[Counter];
            UnIP_SERVERS.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.RemoteHelp.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.RemoteHelp.Methods.StrtoIP_SERVERS = function (ProtocoloStr, IP_SERVERSList) {
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        IP_SERVERSList.length = 0;
        if (Persistence.RemoteHelp.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnIP_SERVERS = new Persistence.Demo.Properties.TIP_SERVERS(); //Mode new row
                UnIP_SERVERS.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.IP_SERVERS_ListAdd(IP_SERVERSList, UnIP_SERVERS);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}

//Socket Function 
Persistence.RemoteHelp.Methods.IP_SERVERSListToByte = function (IP_SERVERSList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, IP_SERVERSList.length - idx);
    for (var i = idx; i < IP_SERVERSList.length; i++) {
        IP_SERVERSList[i].ToByte(MemStream);
    }
}
Persistence.RemoteHelp.Methods.ByteToIP_SERVERSList = function (IP_SERVERSList, MemStream) {
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (var i = 0; i < Count; i++) {
        var UnIP_SERVERS = new Persistence.RemoteHelp.Properties.TIP_SERVERS();
        UnIP_SERVERS.ByteTo(MemStream);
        Persistence.RemoteHelp.Methods.IP_SERVERS_ListAdd(IP_SERVERSList, UnIP_SERVERS);
    }
}
