﻿//TreduceDB
//Persistence.RemoteHelp.Properties
//Persistence.RemoteHelp.Methods

//**********************   PROPERTIES   *****************************************************************************************

Persistence.RemoteHelp.Properties.TIP = function () {
    this.ADAPTADOR_IP = "";
    this.DHCP_IP = "";
    this.DHCP_ON = "";
    this.DIRECCION_IP = "";
    this.DNS1 = "";
    this.DNS2 = "";
    this.GATEWAY_IP = "";
    this.IDCPU = "";
    this.MAC_IP = "";
    this.MASK_IP = "";
    this.NETCARDTYPE_IP = "";

    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.ADAPTADOR_IP);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.DHCP_IP);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.DHCP_ON);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.DIRECCION_IP);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.DNS1);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.DNS2);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.GATEWAY_IP);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.IDCPU);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.MAC_IP);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.MASK_IP);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.NETCARDTYPE_IP);
    }
    this.ByteTo = function (MemStream) {
        this.ADAPTADOR_IP = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.DHCP_IP = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.DHCP_ON = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.DIRECCION_IP = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.DNS1 = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.DNS2 = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.GATEWAY_IP = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDCPU = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.MAC_IP = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.MASK_IP = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.NETCARDTYPE_IP = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteStr(this.ADAPTADOR_IP, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.DHCP_IP, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.DHCP_ON, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.DIRECCION_IP, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.DNS1, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.DNS2, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.GATEWAY_IP, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.IDCPU, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.MAC_IP, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.MASK_IP, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.NETCARDTYPE_IP, Longitud, Texto);
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.ADAPTADOR_IP = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.DHCP_IP = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.DHCP_ON = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.DIRECCION_IP = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.DNS1 = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.DNS2 = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.GATEWAY_IP = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.IDCPU = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.MAC_IP = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.MASK_IP = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.NETCARDTYPE_IP = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
    }
}

//**********************   METHODS server  ********************************************************************************************

Persistence.RemoteHelp.Methods.IP_Fill = function (IPList, StrIDCPU/*IDCPU*/) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    IPList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDCPU == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.IP.IDCPU.FieldName, " = " + UsrCfg.InternoAtisNames.IP.NAME_TABLE + "." + UsrCfg.InternoAtisNames.IP.IDCPU.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDCPU = " IN (" + StrIDCPU + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.IP.IDCPU.FieldName, StrIDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //Param.AddInt32(UsrCfg.InternoDataLinkNames.IP.IDCPU.FieldName, IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {
        /*

    //********   IP_GET   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"IP_GET", @"IP_GET description.");
    UnSQL.SQL.Add(@" SELECT  ");
    UnSQL.SQL.Add(@"  IP.ADAPTADOR_IP, ");
    UnSQL.SQL.Add(@"  IP.DHCP_IP, ");
    UnSQL.SQL.Add(@"  IP.DHCP_ON, ");
    UnSQL.SQL.Add(@"  IP.DIRECCION_IP, ");
    UnSQL.SQL.Add(@"  IP.DNS1, ");
    UnSQL.SQL.Add(@"  IP.DNS2, ");
    UnSQL.SQL.Add(@"  IP.GATEWAY_IP, ");
    UnSQL.SQL.Add(@"  IP.IDCPU, ");
    UnSQL.SQL.Add(@"  IP.MAC_IP, ");
    UnSQL.SQL.Add(@"  IP.MASK_IP, ");
    UnSQL.SQL.Add(@"  IP.NETCARDTYPE_IP ");
    UnSQL.SQL.Add(@" FROM  IP ");
    UnSQL.SQL.Add(@" WHERE   ");
    UnSQL.SQL.Add(@"  IP.IDCPU @[IDCPU] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_IP = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "IP_GET", Param.ToBytes());
        ResErr = DS_IP.ResErr;
        if (ResErr.NotError) {
            if (DS_IP.DataSet.RecordCount > 0) {
                DS_IP.DataSet.First();
                while (!(DS_IP.DataSet.Eof)) {
                    var IP = new Persistence.RemoteHelp.Properties.TIP();
                    //Other
                    IP.ADAPTADOR_IP = DS_IP.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.IP.ADAPTADOR_IP.FieldName).asString();
                    IP.DHCP_IP = DS_IP.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.IP.DHCP_IP.FieldName).asString();
                    IP.DHCP_ON = DS_IP.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.IP.DHCP_ON.FieldName).asString();
                    IP.DIRECCION_IP = DS_IP.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.IP.DIRECCION_IP.FieldName).asString();
                    IP.DNS1 = DS_IP.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.IP.DNS1.FieldName).asString();
                    IP.DNS2 = DS_IP.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.IP.DNS2.FieldName).asString();
                    IP.GATEWAY_IP = DS_IP.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.IP.GATEWAY_IP.FieldName).asString();
                    IP.IDCPU = DS_IP.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.IP.IDCPU.FieldName).asString();
                    IP.MAC_IP = DS_IP.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.IP.MAC_IP.FieldName).asString();
                    IP.MASK_IP = DS_IP.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.IP.MASK_IP.FieldName).asString();
                    IP.NETCARDTYPE_IP = DS_IP.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.IP.NETCARDTYPE_IP.FieldName).asString();
                    IPList.push(IP);

                    DS_IP.DataSet.Next();
                }
            }
            else {
                DS_IP.ResErr.NotError = false;
                DS_IP.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.IP_GETID = function (IP) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.IP.ADAPTADOR_IP.FieldName, IP.ADAPTADOR_IP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP.DHCP_IP.FieldName, IP.DHCP_IP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP.DHCP_ON.FieldName, IP.DHCP_ON, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP.DIRECCION_IP.FieldName, IP.DIRECCION_IP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP.DNS1.FieldName, IP.DNS1, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP.DNS2.FieldName, IP.DNS2, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP.GATEWAY_IP.FieldName, IP.GATEWAY_IP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP.IDCPU.FieldName, IP.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP.MAC_IP.FieldName, IP.MAC_IP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP.MASK_IP.FieldName, IP.MASK_IP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP.NETCARDTYPE_IP.FieldName, IP.NETCARDTYPE_IP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   IP_GETID   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"IP_GETID", @"IP_GETID description.");
    UnSQL.SQL.Add(@" SELECT  ");
    UnSQL.SQL.Add(@"  IP.ADAPTADOR_IP, ");
    UnSQL.SQL.Add(@"  IP.DHCP_IP, ");
    UnSQL.SQL.Add(@"  IP.DHCP_ON, ");
    UnSQL.SQL.Add(@"  IP.DIRECCION_IP, ");
    UnSQL.SQL.Add(@"  IP.DNS1, ");
    UnSQL.SQL.Add(@"  IP.DNS2, ");
    UnSQL.SQL.Add(@"  IP.GATEWAY_IP, ");
    UnSQL.SQL.Add(@"  IP.IDCPU, ");
    UnSQL.SQL.Add(@"  IP.MAC_IP, ");
    UnSQL.SQL.Add(@"  IP.MASK_IP, ");
    UnSQL.SQL.Add(@"  IP.NETCARDTYPE_IP ");
    UnSQL.SQL.Add(@" FROM  IP ");
    UnSQL.SQL.Add(@" WHERE   ");
    UnSQL.SQL.Add(@"  IP.ADAPTADOR_IP=@[ADAPTADOR_IP] and  ");
    UnSQL.SQL.Add(@"  IP.DHCP_IP=@[DHCP_IP] and  ");
    UnSQL.SQL.Add(@"  IP.DHCP_ON=@[DHCP_ON] and  ");
    UnSQL.SQL.Add(@"  IP.DIRECCION_IP=@[DIRECCION_IP] and  ");
    UnSQL.SQL.Add(@"  IP.DNS1=@[DNS1] and  ");
    UnSQL.SQL.Add(@"  IP.DNS2=@[DNS2] and  ");
    UnSQL.SQL.Add(@"  IP.GATEWAY_IP=@[GATEWAY_IP] and  ");
    UnSQL.SQL.Add(@"  IP.IDCPU=@[IDCPU] and  ");
    UnSQL.SQL.Add(@"  IP.MAC_IP=@[MAC_IP] and  ");
    UnSQL.SQL.Add(@"  IP.MASK_IP=@[MASK_IP] and  ");
    UnSQL.SQL.Add(@"  IP.NETCARDTYPE_IP=@[NETCARDTYPE_IP] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_IP = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "IP_GETID", Param.ToBytes());
        ResErr = DS_IP.ResErr;
        if (ResErr.NotError) {
            if (DS_IP.DataSet.RecordCount > 0) {
                DS_IP.DataSet.First();
                //Other
                IP.ADAPTADOR_IP = DS_IP.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.IP.ADAPTADOR_IP.FieldName).asString();
                IP.DHCP_IP = DS_IP.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.IP.DHCP_IP.FieldName).asString();
                IP.DHCP_ON = DS_IP.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.IP.DHCP_ON.FieldName).asString();
                IP.DIRECCION_IP = DS_IP.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.IP.DIRECCION_IP.FieldName).asString();
                IP.DNS1 = DS_IP.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.IP.DNS1.FieldName).asString();
                IP.DNS2 = DS_IP.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.IP.DNS2.FieldName).asString();
                IP.GATEWAY_IP = DS_IP.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.IP.GATEWAY_IP.FieldName).asString();
                IP.IDCPU = DS_IP.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.IP.IDCPU.FieldName).asString();
                IP.MAC_IP = DS_IP.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.IP.MAC_IP.FieldName).asString();
                IP.MASK_IP = DS_IP.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.IP.MASK_IP.FieldName).asString();
                IP.NETCARDTYPE_IP = DS_IP.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.IP.NETCARDTYPE_IP.FieldName).asString();
            }
            else {
                DS_IP.ResErr.NotError = false;
                DS_IP.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.IP_ADD = function (IP) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.IP.ADAPTADOR_IP.FieldName, IP.ADAPTADOR_IP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP.DHCP_IP.FieldName, IP.DHCP_IP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP.DHCP_ON.FieldName, IP.DHCP_ON, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP.DIRECCION_IP.FieldName, IP.DIRECCION_IP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP.DNS1.FieldName, IP.DNS1, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP.DNS2.FieldName, IP.DNS2, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP.GATEWAY_IP.FieldName, IP.GATEWAY_IP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP.IDCPU.FieldName, IP.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP.MAC_IP.FieldName, IP.MAC_IP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP.MASK_IP.FieldName, IP.MASK_IP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP.NETCARDTYPE_IP.FieldName, IP.NETCARDTYPE_IP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   IP_ADD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"IP_ADD", @"IP_ADD description.");
    UnSQL.SQL.Add(@" INSERT INTO IP( ");
    UnSQL.SQL.Add(@"  ADAPTADOR_IP,  ");
    UnSQL.SQL.Add(@"  DHCP_IP,  ");
    UnSQL.SQL.Add(@"  DHCP_ON,  ");
    UnSQL.SQL.Add(@"  DIRECCION_IP,  ");
    UnSQL.SQL.Add(@"  DNS1,  ");
    UnSQL.SQL.Add(@"  DNS2,  ");
    UnSQL.SQL.Add(@"  GATEWAY_IP,  ");
    UnSQL.SQL.Add(@"  IDCPU,  ");
    UnSQL.SQL.Add(@"  MAC_IP,  ");
    UnSQL.SQL.Add(@"  MASK_IP,  ");
    UnSQL.SQL.Add(@"  NETCARDTYPE_IP ");
    UnSQL.SQL.Add(@" )  ");
    UnSQL.SQL.Add(@" VALUES ( ");
    UnSQL.SQL.Add(@"  @[ADAPTADOR_IP], ");
    UnSQL.SQL.Add(@"  @[DHCP_IP], ");
    UnSQL.SQL.Add(@"  @[DHCP_ON], ");
    UnSQL.SQL.Add(@"  @[DIRECCION_IP], ");
    UnSQL.SQL.Add(@"  @[DNS1], ");
    UnSQL.SQL.Add(@"  @[DNS2], ");
    UnSQL.SQL.Add(@"  @[GATEWAY_IP], ");
    UnSQL.SQL.Add(@"  @[IDCPU], ");
    UnSQL.SQL.Add(@"  @[MAC_IP], ");
    UnSQL.SQL.Add(@"  @[MASK_IP], ");
    UnSQL.SQL.Add(@"  @[NETCARDTYPE_IP] ");
    UnSQL.SQL.Add(@" ) ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "IP_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.RemoteHelp.Methods.IP_GETID(IP);
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.IP_UPD = function (IP) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.IP.ADAPTADOR_IP.FieldName, IP.ADAPTADOR_IP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP.DHCP_IP.FieldName, IP.DHCP_IP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP.DHCP_ON.FieldName, IP.DHCP_ON, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP.DIRECCION_IP.FieldName, IP.DIRECCION_IP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP.DNS1.FieldName, IP.DNS1, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP.DNS2.FieldName, IP.DNS2, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP.GATEWAY_IP.FieldName, IP.GATEWAY_IP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP.IDCPU.FieldName, IP.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP.MAC_IP.FieldName, IP.MAC_IP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP.MASK_IP.FieldName, IP.MASK_IP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP.NETCARDTYPE_IP.FieldName, IP.NETCARDTYPE_IP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   IP_UPD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"IP_UPD", @"IP_UPD description.");
    UnSQL.SQL.Add(@" UPDATE IP SET ");
    UnSQL.SQL.Add(@"  ADAPTADOR_IP=@[ADAPTADOR_IP],  ");
    UnSQL.SQL.Add(@"  DHCP_IP=@[DHCP_IP],  ");
    UnSQL.SQL.Add(@"  DHCP_ON=@[DHCP_ON],  ");
    UnSQL.SQL.Add(@"  DIRECCION_IP=@[DIRECCION_IP],  ");
    UnSQL.SQL.Add(@"  DNS1=@[DNS1],  ");
    UnSQL.SQL.Add(@"  DNS2=@[DNS2],  ");
    UnSQL.SQL.Add(@"  GATEWAY_IP=@[GATEWAY_IP],  ");
    UnSQL.SQL.Add(@"  IDCPU=@[IDCPU],  ");
    UnSQL.SQL.Add(@"  MAC_IP=@[MAC_IP],  ");
    UnSQL.SQL.Add(@"  MASK_IP=@[MASK_IP],  ");
    UnSQL.SQL.Add(@"  NETCARDTYPE_IP=@[NETCARDTYPE_IP] ");
    UnSQL.SQL.Add(@" WHERE   ");
    UnSQL.SQL.Add(@"  ADAPTADOR_IP=@[ADAPTADOR_IP] AND  ");
    UnSQL.SQL.Add(@"  DHCP_IP=@[DHCP_IP] AND  ");
    UnSQL.SQL.Add(@"  DHCP_ON=@[DHCP_ON] AND  ");
    UnSQL.SQL.Add(@"  DIRECCION_IP=@[DIRECCION_IP] AND  ");
    UnSQL.SQL.Add(@"  DNS1=@[DNS1] AND  ");
    UnSQL.SQL.Add(@"  DNS2=@[DNS2] AND  ");
    UnSQL.SQL.Add(@"  GATEWAY_IP=@[GATEWAY_IP] AND  ");
    UnSQL.SQL.Add(@"  IDCPU=@[IDCPU] AND  ");
    UnSQL.SQL.Add(@"  MAC_IP=@[MAC_IP] AND  ");
    UnSQL.SQL.Add(@"  MASK_IP=@[MASK_IP] AND  ");
    UnSQL.SQL.Add(@"  NETCARDTYPE_IP=@[NETCARDTYPE_IP] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "IP_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.RemoteHelp.Methods.IP_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.RemoteHelp.Methods.IP_DELIDCPU(/*String StrIDCPU*/ Param);
    }
    else {
        Res = Persistence.RemoteHelp.Methods.IP_DELIP(Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.IP_DELIDCPU = function (/*String StrIDCPU*/ IDCPU) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDCPU == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.IP.IDCPU.FieldName, " = " + UsrCfg.InternoAtisNames.IP.NAME_TABLE + "." + UsrCfg.InternoAtisNames.IP.IDCPU.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDCPU = " IN (" + StrIDCPU + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.IP.IDCPU.FieldName, StrIDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.IP.IDCPU.FieldName, IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   IP_DEL_1   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"IP_DEL_1", @"IP_DEL_1 description.");
    UnSQL.SQL.Add(@" DELETE IP  ");
    UnSQL.SQL.Add(@" WHERE   ");
    UnSQL.SQL.Add(@"  ADAPTADOR_IP=@[ADAPTADOR_IP] AND  ");
    UnSQL.SQL.Add(@"  DHCP_IP=@[DHCP_IP] AND  ");
    UnSQL.SQL.Add(@"  DHCP_ON=@[DHCP_ON] AND  ");
    UnSQL.SQL.Add(@"  DIRECCION_IP=@[DIRECCION_IP] AND  ");
    UnSQL.SQL.Add(@"  DNS1=@[DNS1] AND  ");
    UnSQL.SQL.Add(@"  DNS2=@[DNS2] AND  ");
    UnSQL.SQL.Add(@"  GATEWAY_IP=@[GATEWAY_IP] AND  ");
    UnSQL.SQL.Add(@"  IDCPU=@[IDCPU] AND  ");
    UnSQL.SQL.Add(@"  MAC_IP=@[MAC_IP] AND  ");
    UnSQL.SQL.Add(@"  MASK_IP=@[MASK_IP] AND  ");
    UnSQL.SQL.Add(@"  NETCARDTYPE_IP=@[NETCARDTYPE_IP] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "IP_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.IP_DELIP = function (IP) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.IP.ADAPTADOR_IP.FieldName, IP.ADAPTADOR_IP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP.DHCP_IP.FieldName, IP.DHCP_IP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP.DHCP_ON.FieldName, IP.DHCP_ON, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP.DIRECCION_IP.FieldName, IP.DIRECCION_IP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP.DNS1.FieldName, IP.DNS1, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP.DNS2.FieldName, IP.DNS2, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP.GATEWAY_IP.FieldName, IP.GATEWAY_IP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP.IDCPU.FieldName, IP.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP.MAC_IP.FieldName, IP.MAC_IP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP.MASK_IP.FieldName, IP.MASK_IP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.IP.NETCARDTYPE_IP.FieldName, IP.NETCARDTYPE_IP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   IP_DEL_2   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"IP_DEL_2", @"IP_DEL_2 description.");
    UnSQL.SQL.Add(@" DELETE IP  ");
    UnSQL.SQL.Add(@" WHERE   ");
    UnSQL.SQL.Add(@"  ADAPTADOR_IP=@[ADAPTADOR_IP] AND  ");
    UnSQL.SQL.Add(@"  DHCP_IP=@[DHCP_IP] AND  ");
    UnSQL.SQL.Add(@"  DHCP_ON=@[DHCP_ON] AND  ");
    UnSQL.SQL.Add(@"  DIRECCION_IP=@[DIRECCION_IP] AND  ");
    UnSQL.SQL.Add(@"  DNS1=@[DNS1] AND  ");
    UnSQL.SQL.Add(@"  DNS2=@[DNS2] AND  ");
    UnSQL.SQL.Add(@"  GATEWAY_IP=@[GATEWAY_IP] AND  ");
    UnSQL.SQL.Add(@"  IDCPU=@[IDCPU] AND  ");
    UnSQL.SQL.Add(@"  MAC_IP=@[MAC_IP] AND  ");
    UnSQL.SQL.Add(@"  MASK_IP=@[MASK_IP] AND  ");
    UnSQL.SQL.Add(@"  NETCARDTYPE_IP=@[NETCARDTYPE_IP] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "IP_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//Persistence.RemoteHelp.
//Persistence.RemoteHelp.Properties.
//**********************   METHODS   ********************************************************************************************

//List Function 
Persistence.RemoteHelp.Methods.IP_ListSetID = function (IPList, IDCPU) {
    for (var i = 0; i < IPList.length; i++) {

        if (IDCPU == IPList[i].IDCPU)
            return (IPList[i]);

    }
    var UnIP = new Persistence.RemoteHelp.Properties.TIP();
    UnIP.IDCPU = IDCPU;
    return (UnIP);
}
Persistence.RemoteHelp.Methods.IP_ListAdd = function (IPList, IP) {
    var i = Persistence.RemoteHelp.Methods.IP_ListGetIndex(IPList, IP);
    if (i == -1) IPList.push(IP);
    else {
        IPList[i].ADAPTADOR_IP = IP.ADAPTADOR_IP;
        IPList[i].DHCP_IP = IP.DHCP_IP;
        IPList[i].DHCP_ON = IP.DHCP_ON;
        IPList[i].DIRECCION_IP = IP.DIRECCION_IP;
        IPList[i].DNS1 = IP.DNS1;
        IPList[i].DNS2 = IP.DNS2;
        IPList[i].GATEWAY_IP = IP.GATEWAY_IP;
        IPList[i].IDCPU = IP.IDCPU;
        IPList[i].MAC_IP = IP.MAC_IP;
        IPList[i].MASK_IP = IP.MASK_IP;
        IPList[i].NETCARDTYPE_IP = IP.NETCARDTYPE_IP;
    }
}

//CJRC_26072018
Persistence.RemoteHelp.Methods.IP_ListGetIndex = function (IPList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.RemoteHelp.Methods.IP_ListGetIndexIDCPU(IPList, Param);

    }
    else {
        Res = Persistence.RemoteHelp.Methods.IP_ListGetIndexIP(IPList, Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.IP_ListGetIndexIP = function (IPList, IP) {
    for (var i = 0; i < IPList.length; i++) {
        if (IP.IDCPU == IPList[i].IDCPU)
            return (i);
    }
    return (-1);
}
Persistence.RemoteHelp.Methods.IP_ListGetIndexIDCPU = function (IPList, IDCPU) {
    for (var i = 0; i < IPList.length; i++)
    {
        if (IDCPU == IPList[i].IDCPU)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.RemoteHelp.Methods.IPListtoStr = function (IPList) {
    var Res = Persistence.RemoteHelp.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(IPList.length, Longitud, Texto);
        for (Counter = 0; Counter < IPList.length; Counter++) {
            var UnIP = IPList[Counter];
            UnIP.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.RemoteHelp.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.RemoteHelp.Methods.StrtoIP = function (ProtocoloStr, IPList) {
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        IPList.length = 0;
        if (Persistence.RemoteHelp.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnIP = new Persistence.Demo.Properties.TIP(); //Mode new row
                UnIP.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.IP_ListAdd(IPList, UnIP);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}

//Socket Function 
Persistence.RemoteHelp.Methods.IPListToByte = function (IPList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, IPList.length - idx);
    for (var i = idx; i < IPList.length; i++) {
        IPList[i].ToByte(MemStream);
    }
}
Persistence.RemoteHelp.Methods.ByteToIPList = function (IPList, MemStream) {
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (var i = 0; i < Count; i++) {
        UnIP = new Persistence.RemoteHelp.Properties.TIP();
        UnIP.ByteTo(MemStream);
        Persistence.RemoteHelp.Methods.IP_ListAdd(IPList, UnIP);
    }
}

    //*******************************************************************************************************************************

