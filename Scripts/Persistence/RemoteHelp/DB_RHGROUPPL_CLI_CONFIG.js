﻿//TreduceDB
//Persistence.RemoteHelp.Properties
//Persistence.RemoteHelp.Methods

Persistence.RemoteHelp.Properties.TRHGROUPPL_CLI_CONFIG = function () {
    this.IDRHGROUPPL_CLI_CONFIG = 0;
    this.CLI_CONFIG_NAME = "";
    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDRHGROUPPL_CLI_CONFIG);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CLI_CONFIG_NAME);
    }
    this.ByteTo = function (MemStream) {
        this.IDRHGROUPPL_CLI_CONFIG = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.CLI_CONFIG_NAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDRHGROUPPL_CLI_CONFIG, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.CLI_CONFIG_NAME, Longitud, Texto);
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDRHGROUPPL_CLI_CONFIG = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.CLI_CONFIG_NAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
    }
}

//**********************   METHODS server  ********************************************************************************************

Persistence.RemoteHelp.Methods.RHGROUPPL_CLI_CONFIG_Fill = function (RHGROUPPL_CLI_CONFIGList) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    RHGROUPPL_CLI_CONFIGList.length = 0;
    try {
        var DS_RHGROUPPL_CLI_CONFIG = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "RHGROUPPL_CLI_CONFIG_GET", new Array());
        ResErr = DS_RHGROUPPL_CLI_CONFIG.ResErr;
        if (ResErr.NotError) {
            if (DS_RHGROUPPL_CLI_CONFIG.DataSet.RecordCount > 0) {
                DS_RHGROUPPL_CLI_CONFIG.DataSet.First();
                while (!(DS_RHGROUPPL_CLI_CONFIG.DataSet.Eof)) {
                    var RHGROUPPL_CLI_CONFIG = new Persistence.RemoteHelp.Properties.TRHGROUPPL_CLI_CONFIG();
                    RHGROUPPL_CLI_CONFIG.IDRHGROUPPL_CLI_CONFIG = DS_RHGROUPPL_CLI_CONFIG.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL_CLI_CONFIG.IDRHGROUPPL_CLI_CONFIG.FieldName).asInt32();
                    RHGROUPPL_CLI_CONFIG.CLI_CONFIG_NAME = DS_RHGROUPPL_CLI_CONFIG.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL_CLI_CONFIG.CLI_CONFIG_NAME.FieldName).asString();
                    RHGROUPPL_CLI_CONFIGList.push(RHGROUPPL_CLI_CONFIG);

                    DS_RHGROUPPL_CLI_CONFIG.DataSet.Next();
                }
            }
            else {
                DS_RHGROUPPL_CLI_CONFIG.ResErr.NotError = false;
                DS_RHGROUPPL_CLI_CONFIG.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPPL_CLI_CONFIG_GETID = function (RHGROUPPL_CLI_CONFIG) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPPL_CLI_CONFIG.CLI_CONFIG_NAME.FieldName, RHGROUPPL_CLI_CONFIG.CLI_CONFIG_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var DS_RHGROUPPL_CLI_CONFIG = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "RHGROUPPL_CLI_CONFIG_GETID", Param.ToBytes());
        ResErr = DS_RHGROUPPL_CLI_CONFIG.ResErr;
        if (ResErr.NotError) {
            if (DS_RHGROUPPL_CLI_CONFIG.DataSet.RecordCount > 0) {
                DS_RHGROUPPL_CLI_CONFIG.DataSet.First();
                RHGROUPPL_CLI_CONFIG.IDRHGROUPPL_CLI_CONFIG = DS_RHGROUPPL_CLI_CONFIG.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL_CLI_CONFIG.IDRHGROUPPL_CLI_CONFIG.FieldName).asInt32();
                //Other
                RHGROUPPL_CLI_CONFIG.CLI_CONFIG_NAME = DS_RHGROUPPL_CLI_CONFIG.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL_CLI_CONFIG.CLI_CONFIG_NAME.FieldName).asString();
            }
            else {
                DS_RHGROUPPL_CLI_CONFIG.ResErr.NotError = false;
                DS_RHGROUPPL_CLI_CONFIG.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPPL_CLI_CONFIG_GETBYID = function (RHGROUPPL_CLI_CONFIG, Id) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPL_CLI_CONFIG.IDRHGROUPPL_CLI_CONFIG.FieldName, Id, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var DS_RHGROUPPL_CLI_CONFIG = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "RHGROUPPL_CLI_CONFIG_GETBYID", Param.ToBytes());
        ResErr = DS_RHGROUPPL_CLI_CONFIG.ResErr;
        if (ResErr.NotError) {
            if (DS_RHGROUPPL_CLI_CONFIG.DataSet.RecordCount > 0) {
                DS_RHGROUPPL_CLI_CONFIG.DataSet.First();
                RHGROUPPL_CLI_CONFIG.IDRHGROUPPL_CLI_CONFIG = DS_RHGROUPPL_CLI_CONFIG.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL_CLI_CONFIG.IDRHGROUPPL_CLI_CONFIG.FieldName).asInt32();
                RHGROUPPL_CLI_CONFIG.CLI_CONFIG_NAME = DS_RHGROUPPL_CLI_CONFIG.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPL_CLI_CONFIG.CLI_CONFIG_NAME.FieldName).asString();
            }
            else {
                DS_RHGROUPPL_CLI_CONFIG.ResErr.NotError = false;
                DS_RHGROUPPL_CLI_CONFIG.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPPL_CLI_CONFIG_ADD = function (RHGROUPPL_CLI_CONFIG) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPL_CLI_CONFIG.IDRHGROUPPL_CLI_CONFIG.FieldName, RHGROUPPL_CLI_CONFIG.IDRHGROUPPL_CLI_CONFIG, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPPL_CLI_CONFIG.CLI_CONFIG_NAME.FieldName, RHGROUPPL_CLI_CONFIG.CLI_CONFIG_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHGROUPPL_CLI_CONFIG_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.RemoteHelp.Methods.RHGROUPPL_CLI_CONFIG_GETID(RHGROUPPL_CLI_CONFIG);
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPPL_CLI_CONFIG_UPD = function (RHGROUPPL_CLI_CONFIG) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPL_CLI_CONFIG.IDRHGROUPPL_CLI_CONFIG.FieldName, RHGROUPPL_CLI_CONFIG.IDRHGROUPPL_CLI_CONFIG, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPPL_CLI_CONFIG.CLI_CONFIG_NAME.FieldName, RHGROUPPL_CLI_CONFIG.CLI_CONFIG_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHGROUPPL_CLI_CONFIG_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.RemoteHelp.Methods.RHGROUPPL_CLI_CONFIG_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.RemoteHelp.Methods.RHGROUPPL_CLI_CONFIG_DELIDRHGROUPPL_CLI_CONFIG(Param);
    }
    else {
        Res = Persistence.RemoteHelp.Methods.RHGROUPPL_CLI_CONFIG_DELRHGROUPPL_CLI_CONFIG(Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.RHGROUPPL_CLI_CONFIG_DELIDRHGROUPPL_CLI_CONFIG = function (IDRHGROUPPL_CLI_CONFIG) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPL_CLI_CONFIG.IDRHGROUPPL_CLI_CONFIG.FieldName, IDRHGROUPPL_CLI_CONFIG, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHGROUPPL_CLI_CONFIG_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPPL_CLI_CONFIG_DELRHGROUPPL_CLI_CONFIG = function (RHGROUPPL_CLI_CONFIG) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPL_CLI_CONFIG.IDRHGROUPPL_CLI_CONFIG.FieldName, RHGROUPPL_CLI_CONFIG.IDRHGROUPPL_CLI_CONFIG, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPPL_CLI_CONFIG.CLI_CONFIG_NAME.FieldName, RHGROUPPL_CLI_CONFIG.CLI_CONFIG_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHGROUPPL_CLI_CONFIG_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}


//**********************   METHODS   ********************************************************************************************

Persistence.RemoteHelp.Methods.RHGROUPPL_CLI_CONFIG_ListSetID = function (RHGROUPPL_CLI_CONFIGList, IDRHGROUPPL_CLI_CONFIG) {
    for (var i = 0; i < RHGROUPPL_CLI_CONFIGList.length; i++) {

        if (IDRHGROUPPL_CLI_CONFIG == RHGROUPPL_CLI_CONFIGList[i].IDRHGROUPPL_CLI_CONFIG)
            return (RHGROUPPL_CLI_CONFIGList[i]);

    }
    var UnRHGROUPPL_CLI_CONFIG = new Persistence.RemoteHelp.Properties.TRHGROUPPL_CLI_CONFIG();
    UnRHGROUPPL_CLI_CONFIG.IDRHGROUPPL_CLI_CONFIG = IDRHGROUPPL_CLI_CONFIG;
    return (UnRHGROUPPL_CLI_CONFIG);
}
Persistence.RemoteHelp.Methods.RHGROUPPL_CLI_CONFIG_ListAdd = function (RHGROUPPL_CLI_CONFIGList, RHGROUPPL_CLI_CONFIG) {
    var i = Persistence.RemoteHelp.Methods.RHGROUPPL_CLI_CONFIG_ListGetIndex(RHGROUPPL_CLI_CONFIGList, RHGROUPPL_CLI_CONFIG);
    if (i == -1) RHGROUPPL_CLI_CONFIGList.push(RHGROUPPL_CLI_CONFIG);
    else {
        RHGROUPPL_CLI_CONFIGList[i].IDRHGROUPPL_CLI_CONFIG = RHGROUPPL_CLI_CONFIG.IDRHGROUPPL_CLI_CONFIG;
        RHGROUPPL_CLI_CONFIGList[i].CLI_CONFIG_NAME = RHGROUPPL_CLI_CONFIG.CLI_CONFIG_NAME;
    }
}

//CJRC_26072018
Persistence.RemoteHelp.Methods.RHGROUPPL_CLI_CONFIG_ListGetIndex = function (RHGROUPPL_CLI_CONFIGList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.RemoteHelp.Methods.RHGROUPPL_CLI_CONFIG_ListGetIndexIDRHGROUPPL_CLI_CONFIG(RHGROUPPL_CLI_CONFIGList, Param);

    }
    else {
        Res = Persistence.RemoteHelp.Methods.RHGROUPPL_CLI_CONFIG_ListGetIndexRHGROUPPL_CLI_CONFIG(RHGROUPPL_CLI_CONFIGList, Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.RHGROUPPL_CLI_CONFIG_ListGetIndexRHGROUPPL_CLI_CONFIG = function (RHGROUPPL_CLI_CONFIGList, RHGROUPPL_CLI_CONFIG) {
    for (var i = 0; i < RHGROUPPL_CLI_CONFIGList.length; i++) {
        if (RHGROUPPL_CLI_CONFIG.IDRHGROUPPL_CLI_CONFIG == RHGROUPPL_CLI_CONFIGList[i].IDRHGROUPPL_CLI_CONFIG)
            return (i);
    }
    return (-1);
}
Persistence.RemoteHelp.Methods.RHGROUPPL_CLI_CONFIG_ListGetIndexIDRHGROUPPL_CLI_CONFIG = function (RHGROUPPL_CLI_CONFIGList, IDRHGROUPPL_CLI_CONFIG) {
    for (var i = 0; i < RHGROUPPL_CLI_CONFIGList.length; i++) {
        if (IDRHGROUPPL_CLI_CONFIG == RHGROUPPL_CLI_CONFIGList[i].IDRHGROUPPL_CLI_CONFIG)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.RemoteHelp.Methods.RHGROUPPL_CLI_CONFIGListtoStr = function (RHGROUPPL_CLI_CONFIGList) {
    var Res = Persistence.RemoteHelp.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(RHGROUPPL_CLI_CONFIGList.length, Longitud, Texto);
        for (Counter = 0; Counter < RHGROUPPL_CLI_CONFIGList.length; Counter++) {
            var UnRHGROUPPL_CLI_CONFIG = RHGROUPPL_CLI_CONFIGList[Counter];
            UnRHGROUPPL_CLI_CONFIG.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.RemoteHelp.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.RemoteHelp.Methods.StrtoRHGROUPPL_CLI_CONFIG = function (ProtocoloStr, RHGROUPPL_CLI_CONFIGList) {
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        RHGROUPPL_CLI_CONFIGList.length = 0;
        if (Persistence.RemoteHelp.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnRHGROUPPL_CLI_CONFIG = new Persistence.Demo.Properties.TRHGROUPPL_CLI_CONFIG(); //Mode new row
                UnRHGROUPPL_CLI_CONFIG.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.RHGROUPPL_CLI_CONFIG_ListAdd(RHGROUPPL_CLI_CONFIGList, UnRHGROUPPL_CLI_CONFIG);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}

//Socket Function 
Persistence.RemoteHelp.Methods.RHGROUPPL_CLI_CONFIGListToByte = function (RHGROUPPL_CLI_CONFIGList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, RHGROUPPL_CLI_CONFIGList.length - idx);
    for (var i = idx; i < RHGROUPPL_CLI_CONFIGList.length; i++) {
        RHGROUPPL_CLI_CONFIGList[i].ToByte(MemStream);
    }
}
Persistence.RemoteHelp.Methods.ByteToRHGROUPPL_CLI_CONFIGList = function (RHGROUPPL_CLI_CONFIGList, MemStream) {
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (var i = 0; i < Count; i++) {
        var UnRHGROUPPL_CLI_CONFIG = new Persistence.RemoteHelp.Properties.TRHGROUPPL_CLI_CONFIG();
        UnRHGROUPPL_CLI_CONFIG.ByteTo(MemStream);
        Persistence.RemoteHelp.Methods.RHGROUPPL_CLI_CONFIG_ListAdd(RHGROUPPL_CLI_CONFIGList, UnRHGROUPPL_CLI_CONFIG);
    }
}

