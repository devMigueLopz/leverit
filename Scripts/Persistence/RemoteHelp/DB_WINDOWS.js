﻿//TreduceDB
//Persistence.RemoteHelp.Properties
//Persistence.RemoteHelp.Methods

Persistence.RemoteHelp.Properties.TWINDOWS = function () {
    this.EXTRA_DATA = "";
    this.IDCPU = "";
    this.IDIOM = "";
    this.INSTALL_DATE = SysCfg.DB.Properties.SVRNOW();
    this.KERNEL = "";
    this.SERIAL_WIN = "";
    this.SUITE = "";
    this.TIPO_SO = "";
    this.VERSION_WINDOWS = "";
    this.WINDOWS = "";
    this.WINSOCK = "";

    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.EXTRA_DATA);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.IDCPU);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.IDIOM);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.INSTALL_DATE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.KERNEL);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.SERIAL_WIN);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.SUITE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.TIPO_SO);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.VERSION_WINDOWS);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.WINDOWS);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.WINSOCK);
    }
    this.ByteTo = function (MemStream) {
        this.EXTRA_DATA = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDCPU = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDIOM = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.INSTALL_DATE = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.KERNEL = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.SERIAL_WIN = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.SUITE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.TIPO_SO = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.VERSION_WINDOWS = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.WINDOWS = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.WINSOCK = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteStr(this.EXTRA_DATA, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.IDCPU, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.IDIOM, Longitud, Texto);
        SysCfg.Str.Protocol.WriteDateTime(this.INSTALL_DATE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.KERNEL, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.SERIAL_WIN, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.SUITE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.TIPO_SO, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.VERSION_WINDOWS, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.WINDOWS, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.WINSOCK, Longitud, Texto);
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.EXTRA_DATA = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.IDCPU = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.IDIOM = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.INSTALL_DATE = SysCfg.Str.Protocol.ReadDateTime(Pos, Index, LongitudArray, Texto);
        this.KERNEL = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.SERIAL_WIN = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.SUITE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.TIPO_SO = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.VERSION_WINDOWS = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.WINDOWS = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.WINSOCK = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
    }

}

//**********************   METHODS server  ********************************************************************************************

Persistence.RemoteHelp.Methods.WINDOWS_Fill = function (WINDOWSList, StrIDCPU) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    WINDOWSList.length = 0;
    varParam = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDCPU == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.WINDOWS.IDCPU.FieldName, " = " + UsrCfg.InternoAtisNames.WINDOWS.NAME_TABLE + "." + UsrCfg.InternoAtisNames.WINDOWS.IDCPU.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDCPU = " IN (" + StrIDCPU + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.WINDOWS.IDCPU.FieldName, StrIDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    try {
        var DS_WINDOWS = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "WINDOWS_GET", Param.ToBytes());
        ResErr = DS_WINDOWS.ResErr;
        if (ResErr.NotError) {
            if (DS_WINDOWS.DataSet.RecordCount > 0) {
                DS_WINDOWS.DataSet.First();
                while (!(DS_WINDOWS.DataSet.Eof)) {
                    var WINDOWS = new Persistence.RemoteHelp.Properties.TWINDOWS();
                    //Other
                    WINDOWS.EXTRA_DATA = DS_WINDOWS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.WINDOWS.EXTRA_DATA.FieldName).asString();
                    WINDOWS.IDCPU = DS_WINDOWS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.WINDOWS.IDCPU.FieldName).asString();
                    WINDOWS.IDIOM = DS_WINDOWS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.WINDOWS.IDIOM.FieldName).asString();
                    WINDOWS.INSTALL_DATE = DS_WINDOWS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.WINDOWS.INSTALL_DATE.FieldName).asDateTime();
                    WINDOWS.KERNEL = DS_WINDOWS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.WINDOWS.KERNEL.FieldName).asString();
                    WINDOWS.SERIAL_WIN = DS_WINDOWS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.WINDOWS.SERIAL_WIN.FieldName).asString();
                    WINDOWS.SUITE = DS_WINDOWS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.WINDOWS.SUITE.FieldName).asString();
                    WINDOWS.TIPO_SO = DS_WINDOWS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.WINDOWS.TIPO_SO.FieldName).asString();
                    WINDOWS.VERSION_WINDOWS = DS_WINDOWS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.WINDOWS.VERSION_WINDOWS.FieldName).asString();
                    WINDOWS.WINDOWS = DS_WINDOWS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.WINDOWS.WINDOWS.FieldName).asString();
                    WINDOWS.WINSOCK = DS_WINDOWS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.WINDOWS.WINSOCK.FieldName).asString();
                    WINDOWSList.push(WINDOWS);

                    DS_WINDOWS.DataSet.Next();
                }
            }
            else {
                DS_WINDOWS.ResErr.NotError = false;
                DS_WINDOWS.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.WINDOWS_GETID = function (WINDOWS) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.WINDOWS.EXTRA_DATA.FieldName, WINDOWS.EXTRA_DATA, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.WINDOWS.IDCPU.FieldName, WINDOWS.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.WINDOWS.IDIOM.FieldName, WINDOWS.IDIOM, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoAtisNames.WINDOWS.INSTALL_DATE.FieldName, WINDOWS.INSTALL_DATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.WINDOWS.KERNEL.FieldName, WINDOWS.KERNEL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.WINDOWS.SERIAL_WIN.FieldName, WINDOWS.SERIAL_WIN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.WINDOWS.SUITE.FieldName, WINDOWS.SUITE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.WINDOWS.TIPO_SO.FieldName, WINDOWS.TIPO_SO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.WINDOWS.VERSION_WINDOWS.FieldName, WINDOWS.VERSION_WINDOWS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.WINDOWS.WINDOWS.FieldName, WINDOWS.WINDOWS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.WINDOWS.WINSOCK.FieldName, WINDOWS.WINSOCK, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var DS_WINDOWS = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "WINDOWS_GETID", Param.ToBytes());
        ResErr = DS_WINDOWS.ResErr;
        if (ResErr.NotError) {
            if (DS_WINDOWS.DataSet.RecordCount > 0) {
                DS_WINDOWS.DataSet.First();
                //Other
                WINDOWS.EXTRA_DATA = DS_WINDOWS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.WINDOWS.EXTRA_DATA.FieldName).asString();
                WINDOWS.IDCPU = DS_WINDOWS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.WINDOWS.IDCPU.FieldName).asString();
                WINDOWS.IDIOM = DS_WINDOWS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.WINDOWS.IDIOM.FieldName).asString();
                WINDOWS.INSTALL_DATE = DS_WINDOWS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.WINDOWS.INSTALL_DATE.FieldName).asDateTime();
                WINDOWS.KERNEL = DS_WINDOWS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.WINDOWS.KERNEL.FieldName).asString();
                WINDOWS.SERIAL_WIN = DS_WINDOWS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.WINDOWS.SERIAL_WIN.FieldName).asString();
                WINDOWS.SUITE = DS_WINDOWS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.WINDOWS.SUITE.FieldName).asString();
                WINDOWS.TIPO_SO = DS_WINDOWS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.WINDOWS.TIPO_SO.FieldName).asString();
                WINDOWS.VERSION_WINDOWS = DS_WINDOWS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.WINDOWS.VERSION_WINDOWS.FieldName).asString();
                WINDOWS.WINDOWS = DS_WINDOWS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.WINDOWS.WINDOWS.FieldName).asString();
                WINDOWS.WINSOCK = DS_WINDOWS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.WINDOWS.WINSOCK.FieldName).asString();
            }
            else {
                DS_WINDOWS.ResErr.NotError = false;
                DS_WINDOWS.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.WINDOWS_ADD = function (WINDOWS) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.WINDOWS.EXTRA_DATA.FieldName, WINDOWS.EXTRA_DATA, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.WINDOWS.IDCPU.FieldName, WINDOWS.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.WINDOWS.IDIOM.FieldName, WINDOWS.IDIOM, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoAtisNames.WINDOWS.INSTALL_DATE.FieldName, WINDOWS.INSTALL_DATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.WINDOWS.KERNEL.FieldName, WINDOWS.KERNEL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.WINDOWS.SERIAL_WIN.FieldName, WINDOWS.SERIAL_WIN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.WINDOWS.SUITE.FieldName, WINDOWS.SUITE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.WINDOWS.TIPO_SO.FieldName, WINDOWS.TIPO_SO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.WINDOWS.VERSION_WINDOWS.FieldName, WINDOWS.VERSION_WINDOWS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.WINDOWS.WINDOWS.FieldName, WINDOWS.WINDOWS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.WINDOWS.WINSOCK.FieldName, WINDOWS.WINSOCK, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "WINDOWS_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.RemoteHelp.Methods.WINDOWS_GETID(WINDOWS);
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.WINDOWS_UPD = function (WINDOWS) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.WINDOWS.EXTRA_DATA.FieldName, WINDOWS.EXTRA_DATA, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.WINDOWS.IDCPU.FieldName, WINDOWS.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.WINDOWS.IDIOM.FieldName, WINDOWS.IDIOM, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoAtisNames.WINDOWS.INSTALL_DATE.FieldName, WINDOWS.INSTALL_DATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.WINDOWS.KERNEL.FieldName, WINDOWS.KERNEL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.WINDOWS.SERIAL_WIN.FieldName, WINDOWS.SERIAL_WIN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.WINDOWS.SUITE.FieldName, WINDOWS.SUITE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.WINDOWS.TIPO_SO.FieldName, WINDOWS.TIPO_SO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.WINDOWS.VERSION_WINDOWS.FieldName, WINDOWS.VERSION_WINDOWS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.WINDOWS.WINDOWS.FieldName, WINDOWS.WINDOWS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.WINDOWS.WINSOCK.FieldName, WINDOWS.WINSOCK, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "WINDOWS_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.RemoteHelp.Methods.WINDOWS_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.RemoteHelp.Methods.WINDOWS_DELIDCPU(/*StrIDCPU*/Param);
    }
    else {
        Res = Persistence.RemoteHelp.Methods.WINDOWS_DELWINDOWS(Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.WINDOWS_DELIDCPU = function (/*StrIDCPU*/IDCPU) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDCPU == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.WINDOWS.IDCPU.FieldName, " = " + UsrCfg.InternoAtisNames.WINDOWS.NAME_TABLE + "." + UsrCfg.InternoAtisNames.WINDOWS.IDCPU.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDCPU = " IN (" + StrIDCPU + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.WINDOWS.IDCPU.FieldName, StrIDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.WINDOWS.IDCPU.FieldName, IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "WINDOWS_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.WINDOWS_DELWINDOWS = function (WINDOWS) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.WINDOWS.EXTRA_DATA.FieldName, WINDOWS.EXTRA_DATA, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.WINDOWS.IDCPU.FieldName, WINDOWS.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.WINDOWS.IDIOM.FieldName, WINDOWS.IDIOM, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoAtisNames.WINDOWS.INSTALL_DATE.FieldName, WINDOWS.INSTALL_DATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.WINDOWS.KERNEL.FieldName, WINDOWS.KERNEL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.WINDOWS.SERIAL_WIN.FieldName, WINDOWS.SERIAL_WIN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.WINDOWS.SUITE.FieldName, WINDOWS.SUITE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.WINDOWS.TIPO_SO.FieldName, WINDOWS.TIPO_SO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.WINDOWS.VERSION_WINDOWS.FieldName, WINDOWS.VERSION_WINDOWS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.WINDOWS.WINDOWS.FieldName, WINDOWS.WINDOWS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.WINDOWS.WINSOCK.FieldName, WINDOWS.WINSOCK, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "WINDOWS_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}


//**********************   METHODS   ********************************************************************************************

Persistence.RemoteHelp.Methods.WINDOWS_ListSetID = function (WINDOWSList, IDCPU) {
    for (var i = 0; i < WINDOWSList.length; i++) {

        if (IDCPU == WINDOWSList[i].IDCPU)
            return (WINDOWSList[i]);

    }
    var UnWINDOWS = new Persistence.RemoteHelp.Properties.TWINDOWS();
    UnWINDOWS.IDCPU = IDCPU;
    return (UnWINDOWS);
}
Persistence.RemoteHelp.Methods.WINDOWS_ListAdd = function (WINDOWSList, WINDOWS) {
    var i = Persistence.RemoteHelp.Methods.WINDOWS_ListGetIndex(WINDOWSList, WINDOWS);
    if (i == -1) WINDOWSList.push(WINDOWS);
    else {
        WINDOWSList[i].EXTRA_DATA = WINDOWS.EXTRA_DATA;
        WINDOWSList[i].IDCPU = WINDOWS.IDCPU;
        WINDOWSList[i].IDIOM = WINDOWS.IDIOM;
        WINDOWSList[i].INSTALL_DATE = WINDOWS.INSTALL_DATE;
        WINDOWSList[i].KERNEL = WINDOWS.KERNEL;
        WINDOWSList[i].SERIAL_WIN = WINDOWS.SERIAL_WIN;
        WINDOWSList[i].SUITE = WINDOWS.SUITE;
        WINDOWSList[i].TIPO_SO = WINDOWS.TIPO_SO;
        WINDOWSList[i].VERSION_WINDOWS = WINDOWS.VERSION_WINDOWS;
        WINDOWSList[i].WINDOWS = WINDOWS.WINDOWS;
        WINDOWSList[i].WINSOCK = WINDOWS.WINSOCK;
    }
}

//CJRC_26072018
Persistence.RemoteHelp.Methods.WINDOWS_ListGetIndex = function (WINDOWSList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.RemoteHelp.Methods.WINDOWS_ListGetIndexIDCPU(WINDOWSList, Param);

    }
    else {
        Res = Persistence.RemoteHelp.Methods.WINDOWS_ListGetIndexWINDOWS(WINDOWSList, Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.WINDOWS_ListGetIndexWINDOWS = function (WINDOWSList, WINDOWS) {
    for (var i = 0; i < WINDOWSList.length; i++) {
        if (WINDOWS.IDCPU == WINDOWSList[i].IDCPU)
            return (i);
    }
    return (-1);
}
Persistence.RemoteHelp.Methods.WINDOWS_ListGetIndexIDCPU = function (WINDOWSList, IDCPU) {
    for (var i = 0; i < WINDOWSList.length; i++) {
        if (IDCPU == WINDOWSList[i].IDCPU)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.RemoteHelp.Methods.WINDOWSListtoStr = function (WINDOWSList) {
    var Res = Persistence.RemoteHelp.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(WINDOWSList.length, Longitud, Texto);
        for (Counter = 0; Counter < WINDOWSList.length; Counter++) {
            var UnWINDOWS = WINDOWSList[Counter];
            UnWINDOWS.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.RemoteHelp.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.RemoteHelp.Methods.StrtoWINDOWS = function (ProtocoloStr, WINDOWSList) {
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        WINDOWSList.length = 0;
        if (Persistence.RemoteHelp.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnWINDOWS = new Persistence.Demo.Properties.TWINDOWS(); //Mode new row
                UnWINDOWS.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.RemoteHelp.Methods.WINDOWS_ListAdd(WINDOWSList, UnWINDOWS);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}

//Socket Function 
Persistence.RemoteHelp.Methods.WINDOWSListToByte = function (WINDOWSList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, WINDOWSList.length - idx);
    for (var i = idx; i < WINDOWSList.length; i++) {
        WINDOWSList[i].ToByte(MemStream);
    }
}
Persistence.RemoteHelp.Methods.ByteToWINDOWSList = function (WINDOWSList, MemStream) {
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (var i = 0; i < Count; i++) {
        var UnWINDOWS = new Persistence.RemoteHelp.Properties.TWINDOWS();
        UnWINDOWS.ByteTo(MemStream);
        Persistence.RemoteHelp.Methods.WINDOWS_ListAdd(WINDOWSList, UnWINDOWS);
    }
}

