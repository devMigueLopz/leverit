﻿//TreduceDB
//Persistence.RemoteHelp.Properties
//Persistence.RemoteHelp.Methods

Persistence.RemoteHelp.Properties.TRHGROUPPLATROLE = function () {
    this.IDATROLE = 0;
    this.IDRHGROUPPL = 0;
    this.IDRHGROUPPLATROLE = 0;

    this.ROLE_NAME = "";
    this.GROUP_NAME = "";

    //virtual
    //this.RHGROUPPL = new Persistence.RemoteHelp.Properties.TRHGROUPCR();

    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDATROLE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDRHGROUPPL);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDRHGROUPPLATROLE);
    }
    this.ByteTo = function (MemStream) {
        this.IDATROLE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDRHGROUPPL = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDRHGROUPPLATROLE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDATROLE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDRHGROUPPL, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDRHGROUPPLATROLE, Longitud, Texto);
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDATROLE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDRHGROUPPL = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDRHGROUPPLATROLE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
    }
}

//**********************   METHODS server  ********************************************************************************************

Persistence.RemoteHelp.Methods.RHGROUPPLATROLE_Fill = function (RHGROUPPLATROLEList) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    RHGROUPPLATROLEList.length = 0;
    try {
        var DS_RHGROUPPLATROLE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "RHGROUPPLATROLE_GET", new Array());
        ResErr = DS_RHGROUPPLATROLE.ResErr;
        if (ResErr.NotError) {
            if (DS_RHGROUPPLATROLE.DataSet.RecordCount > 0) {
                DS_RHGROUPPLATROLE.DataSet.First();
                while (!(DS_RHGROUPPLATROLE.DataSet.Eof)) {
                    var RHGROUPPLATROLE = new Persistence.RemoteHelp.Properties.TRHGROUPPLATROLE();
                    RHGROUPPLATROLE.IDRHGROUPPLATROLE = DS_RHGROUPPLATROLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPLATROLE.IDRHGROUPPLATROLE.FieldName).asInt32();
                    RHGROUPPLATROLE.IDATROLE = DS_RHGROUPPLATROLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPLATROLE.IDATROLE.FieldName).asInt32();
                    RHGROUPPLATROLE.IDRHGROUPPL = DS_RHGROUPPLATROLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPLATROLE.IDRHGROUPPL.FieldName).asInt32();

                    RHGROUPPLATROLE.ROLE_NAME = DS_RHGROUPPLATROLE.DataSet.RecordSet.FieldName("ROLENAME").asString();
                    RHGROUPPLATROLE.GROUP_NAME = DS_RHGROUPPLATROLE.DataSet.RecordSet.FieldName("GROUPPL_NAME").asString();

                    RHGROUPPLATROLEList.push(RHGROUPPLATROLE);

                    DS_RHGROUPPLATROLE.DataSet.Next();
                }
            }
            else {
                DS_RHGROUPPLATROLE.ResErr.NotError = false;
                DS_RHGROUPPLATROLE.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPPLATROLE_GETBYIDGROUP = function (RHGROUPPLATROLEList, IDRHGROUPPL) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    RHGROUPPLATROLEList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPLATROLE.IDRHGROUPPL.FieldName, IDRHGROUPPL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {
        var DS_RHGROUPPLATROLE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "RHGROUPPLATROLE_GETBYIDGROUP", Param.ToBytes());
        ResErr = DS_RHGROUPPLATROLE.ResErr;
        if (ResErr.NotError) {
            if (DS_RHGROUPPLATROLE.DataSet.RecordCount > 0) {
                DS_RHGROUPPLATROLE.DataSet.First();
                while (!(DS_RHGROUPPLATROLE.DataSet.Eof)) {
                    var RHGROUPPLATROLE = new Persistence.RemoteHelp.Properties.TRHGROUPPLATROLE();
                    RHGROUPPLATROLE.IDRHGROUPPLATROLE = DS_RHGROUPPLATROLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPLATROLE.IDRHGROUPPLATROLE.FieldName).asInt32();
                    RHGROUPPLATROLE.IDATROLE = DS_RHGROUPPLATROLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPLATROLE.IDATROLE.FieldName).asInt32();
                    RHGROUPPLATROLE.IDRHGROUPPL = DS_RHGROUPPLATROLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPLATROLE.IDRHGROUPPL.FieldName).asInt32();

                    RHGROUPPLATROLE.ROLE_NAME = DS_RHGROUPPLATROLE.DataSet.RecordSet.FieldName("ROLE_NAME").asString();
                    RHGROUPPLATROLE.GROUP_NAME = DS_RHGROUPPLATROLE.DataSet.RecordSet.FieldName("GROUP_NAMEE").asString();
                    RHGROUPPLATROLEList.push(RHGROUPPLATROLE);

                    DS_RHGROUPPLATROLE.DataSet.Next();
                }
            }
            else {
                DS_RHGROUPPLATROLE.ResErr.NotError = false;
                DS_RHGROUPPLATROLE.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPPLATROLE_GETID = function (RHGROUPPLATROLE) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPLATROLE.IDRHGROUPPLATROLE.FieldName, RHGROUPPLATROLE.IDRHGROUPPLATROLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPLATROLE.IDATROLE.FieldName, RHGROUPPLATROLE.IDATROLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPLATROLE.IDRHGROUPPL.FieldName, RHGROUPPLATROLE.IDRHGROUPPL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var DS_RHGROUPPLATROLE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "RHGROUPPLATROLE_GETID", Param.ToBytes());
        ResErr = DS_RHGROUPPLATROLE.ResErr;
        if (ResErr.NotError) {
            if (DS_RHGROUPPLATROLE.DataSet.RecordCount > 0) {
                DS_RHGROUPPLATROLE.DataSet.First();
                RHGROUPPLATROLE.IDRHGROUPPLATROLE = DS_RHGROUPPLATROLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPLATROLE.IDRHGROUPPLATROLE.FieldName).asInt32();
                //Other
                RHGROUPPLATROLE.IDATROLE = DS_RHGROUPPLATROLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPLATROLE.IDATROLE.FieldName).asInt32();
                RHGROUPPLATROLE.IDRHGROUPPL = DS_RHGROUPPLATROLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPPLATROLE.IDRHGROUPPL.FieldName).asInt32();
            }
            else {
                DS_RHGROUPPLATROLE.ResErr.NotError = false;
                DS_RHGROUPPLATROLE.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

Persistence.RemoteHelp.Methods.RHGROUPPLATROLE_ADD = function (RHGROUPPLATROLE) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPLATROLE.IDATROLE.FieldName, RHGROUPPLATROLE.IDATROLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPLATROLE.IDRHGROUPPL.FieldName, RHGROUPPLATROLE.IDRHGROUPPL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHGROUPPLATROLE_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.RemoteHelp.Methods.RHGROUPPLATROLE_GETID(RHGROUPPLATROLE);
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPPLATROLE_UPD = function (RHGROUPPLATROLE) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPLATROLE.IDRHGROUPPLATROLE.FieldName, RHGROUPPLATROLE.IDRHGROUPPLATROLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPLATROLE.IDATROLE.FieldName, RHGROUPPLATROLE.IDATROLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPLATROLE.IDRHGROUPPL.FieldName, RHGROUPPLATROLE.IDRHGROUPPL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHGROUPPLATROLE_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.RemoteHelp.Methods.RHGROUPPLATROLE_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.RemoteHelp.Methods.RHGROUPPLATROLE_DELIDRHGROUPPLATROLE(Param);
    }
    else {
        Res = Persistence.RemoteHelp.Methods.RHGROUPPLATROLE_DELRHGROUPPLATROLE(Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.RHGROUPPLATROLE_DELIDRHGROUPPLATROLE = function (IDRHGROUPPLATROLE) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPLATROLE.IDRHGROUPPLATROLE.FieldName, IDRHGROUPPLATROLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHGROUPPLATROLE_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPPLATROLE_DELRHGROUPPLATROLE = function (RHGROUPPLATROLE) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPLATROLE.IDRHGROUPPLATROLE.FieldName, RHGROUPPLATROLE.IDRHGROUPPLATROLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPLATROLE.IDATROLE.FieldName, RHGROUPPLATROLE.IDATROLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPPLATROLE.IDRHGROUPPL.FieldName, RHGROUPPLATROLE.IDRHGROUPPL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHGROUPPLATROLE_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}


//**********************   METHODS   ********************************************************************************************

Persistence.RemoteHelp.Methods.RHGROUPPLATROLE_ListSetID = function (RHGROUPPLATROLEList, IDRHGROUPPLATROLE) {
    for (var i = 0; i < RHGROUPPLATROLEList.length; i++) {

        if (IDRHGROUPPLATROLE == RHGROUPPLATROLEList[i].IDRHGROUPPLATROLE)
            return (RHGROUPPLATROLEList[i]);

    }
    var UnRHGROUPPLATROLE = new Persistence.RemoteHelp.Properties.TRHGROUPPLATROLE();
    UnRHGROUPPLATROLE.IDRHGROUPPLATROLE = IDRHGROUPPLATROLE
    return (UnRHGROUPPLATROLE);
}
Persistence.RemoteHelp.Methods.RHGROUPPLATROLE_ListAdd = function (RHGROUPPLATROLEList, RHGROUPPLATROLE) {
    var i = Persistence.RemoteHelp.Methods.RHGROUPPLATROLE_ListGetIndex(RHGROUPPLATROLEList, RHGROUPPLATROLE);
    if (i == -1) RHGROUPPLATROLEList.push(RHGROUPPLATROLE);
    else {
        RHGROUPPLATROLEList[i].IDATROLE = RHGROUPPLATROLE.IDATROLE;
        RHGROUPPLATROLEList[i].IDRHGROUPPL = RHGROUPPLATROLE.IDRHGROUPPL;
        RHGROUPPLATROLEList[i].IDRHGROUPPLATROLE = RHGROUPPLATROLE.IDRHGROUPPLATROLE;
    }
}

//CJRC_26072018
Persistence.RemoteHelp.Methods.RHGROUPPLATROLE_ListGetIndex = function (RHGROUPPLATROLEList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.RemoteHelp.Methods.RHGROUPPLATROLE_ListGetIndexIDRHGROUPPLATROLE(RHGROUPPLATROLEList, Param);

    }
    else {
        Res = Persistence.RemoteHelp.Methods.RHGROUPPLATROLE_ListGetIndexRHGROUPPLATROLE(RHGROUPPLATROLEList, Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.RHGROUPPLATROLE_ListGetIndexRHGROUPPLATROLE = function (RHGROUPPLATROLEList, RHGROUPPLATROLE) {
    for (var i = 0; i < RHGROUPPLATROLEList.length; i++) {
        if (RHGROUPPLATROLE.IDRHGROUPPLATROLE == RHGROUPPLATROLEList[i].IDRHGROUPPLATROLE)
            return (i);
    }
    return (-1);
}
Persistence.RemoteHelp.Methods.RHGROUPPLATROLE_ListGetIndexIDRHGROUPPLATROLE = function (RHGROUPPLATROLEList, IDRHGROUPPLATROLE) {
    for (var i = 0; i < RHGROUPPLATROLEList.length; i++) {
        if (IDRHGROUPPLATROLE == RHGROUPPLATROLEList[i].IDRHGROUPPLATROLE)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.RemoteHelp.Methods.RHGROUPPLATROLEListtoStr = function (RHGROUPPLATROLEList) {
    var Res = Persistence.RemoteHelp.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(RHGROUPPLATROLEList.length, Longitud, Texto);
        for (Counter = 0; Counter < RHGROUPPLATROLEList.length; Counter++) {
            var UnRHGROUPPLATROLE = RHGROUPPLATROLEList[Counter];
            UnRHGROUPPLATROLE.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.RemoteHelp.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.RemoteHelp.Methods.StrtoRHGROUPPLATROLE = function (ProtocoloStr, RHGROUPPLATROLEList) {
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        RHGROUPPLATROLEList.length = 0;
        if (Persistence.RemoteHelp.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnRHGROUPPLATROLE = new Persistence.Demo.Properties.TRHGROUPPLATROLE(); //Mode new row
                UnRHGROUPPLATROLE.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.RHGROUPPLATROLE_ListAdd(RHGROUPPLATROLEList, UnRHGROUPPLATROLE);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}

//Socket Function 
Persistence.RemoteHelp.Methods.RHGROUPPLATROLEListToByte = function (RHGROUPPLATROLEList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, RHGROUPPLATROLEList.length - idx);
    for (var i = idx; i < RHGROUPPLATROLEList.length; i++) {
        RHGROUPPLATROLEList[i].ToByte(MemStream);
    }
}
Persistence.RemoteHelp.Methods.ByteToRHGROUPPLATROLEList = function (RHGROUPPLATROLEList, MemStream)
{
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (var i = 0; i < Count; i++) {
        var UnRHGROUPPLATROLE = new Persistence.RemoteHelp.Properties.TRHGROUPPLATROLE();
        UnRHGROUPPLATROLE.ByteTo(MemStream);
        Persistence.RemoteHelp.Methods.RHGROUPPLATROLE_ListAdd(RHGROUPPLATROLEList, UnRHGROUPPLATROLE);
    }
}