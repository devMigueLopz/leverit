﻿//TreduceDB
//Persistence.RemoteHelp.Properties
//Persistence.RemoteHelp.Methods

Persistence.RemoteHelp.Properties.TCPU = function () {
    this.BRANDID = "";
    this.CRONO_PROCESADOR = 0;
    this.FABRICANTE_DEL_PROCESADOR = "";
    this.FECHA_DE_BIOS = "";
    this.HYPER = "";
    this.IDCPU = "";
    this.MARCA_DE_CPU = "";
    this.MEMORIA_DISPONIBLE = 0;
    this.MEMORIA_TOTAL_CPU = 0;
    this.MEMORIA_TOTAL_SIST = 0;
    this.MEMORIA_VIRTUAL = 0;
    this.MEMORIA_VIRTUAL_DISPONIBLE = 0;
    this.MODELO_DE_CPU = "";
    this.NOMBRE_DE_BIOS = "";
    this.NOMBRE_DEL_PROCESADOR = "";
    this.NUMERO_DE_PROCESADORES = 0;
    this.PROCESADOR = "";
    this.SERIAL_DE_CPU = "";
    this.TIENE_CD = "";
    this.TIPO_DEL_PROCESADOR = "";
    this.VELOCIDAD_PROCESADOR = 0;
    this.VERSION_DE_BIOS = "";

    //Virtual 
    this.IPList = new Array();//Persistence.RemoteHelp.Properties.TUSERS
    this.AGENTE = new Persistence.RemoteHelp.Properties.TAGENTE(); 
    this.ESTACION_RED = new Persistence.RemoteHelp.Properties.TESTACION_RED();
    this.EXTRADATA = new Persistence.RemoteHelp.Properties.TEXTRADATA();
    this.USERS = new Persistence.RemoteHelp.Properties.TUSERS();
    this.VPRO = new Persistence.RemoteHelp.Properties.TVPRO();
    this.WINDOWS = new Persistence.RemoteHelp.Properties.TWINDOWS();

    this.RHGROUPPLCPUList = new Array();//Persistence.RemoteHelp.Properties.TRHGROUPPLCPU
    this.RHGROUPCRCPUList = new Array();//Persistence.RemoteHelp.Properties.TRHGROUPCRCPU

    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.BRANDID);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.CRONO_PROCESADOR);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.FABRICANTE_DEL_PROCESADOR);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.FECHA_DE_BIOS);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.HYPER);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.IDCPU);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.MARCA_DE_CPU);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.MEMORIA_DISPONIBLE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.MEMORIA_TOTAL_CPU);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.MEMORIA_TOTAL_SIST);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.MEMORIA_VIRTUAL);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.MEMORIA_VIRTUAL_DISPONIBLE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.MODELO_DE_CPU);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.NOMBRE_DE_BIOS);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.NOMBRE_DEL_PROCESADOR);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.NUMERO_DE_PROCESADORES);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.PROCESADOR);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.SERIAL_DE_CPU);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.TIENE_CD);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.TIPO_DEL_PROCESADOR);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.VELOCIDAD_PROCESADOR);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.VERSION_DE_BIOS);
    }
    this.ByteTo = function (MemStream) {
        this.BRANDID = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CRONO_PROCESADOR = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.FABRICANTE_DEL_PROCESADOR = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.FECHA_DE_BIOS = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.HYPER = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDCPU = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.this.MARCA_DE_CPU = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.MEMORIA_DISPONIBLE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.MEMORIA_TOTAL_CPU = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.MEMORIA_TOTAL_SIST = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.MEMORIA_VIRTUAL = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.MEMORIA_VIRTUAL_DISPONIBLE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.MODELO_DE_CPU = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.NOMBRE_DE_BIOS = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.NOMBRE_DEL_PROCESADOR = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.NUMERO_DE_PROCESADORES = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.PROCESADOR = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.SERIAL_DE_CPU = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.TIENE_CD = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.TIPO_DEL_PROCESADOR = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.VELOCIDAD_PROCESADOR = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.VERSION_DE_BIOS = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteStr(this.BRANDID, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.CRONO_PROCESADOR, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.FABRICANTE_DEL_PROCESADOR, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.FECHA_DE_BIOS, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.HYPER, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.IDCPU, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.MARCA_DE_CPU, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.MEMORIA_DISPONIBLE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.MEMORIA_TOTAL_CPU, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.MEMORIA_TOTAL_SIST, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.MEMORIA_VIRTUAL, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.MEMORIA_VIRTUAL_DISPONIBLE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.MODELO_DE_CPU, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.NOMBRE_DE_BIOS, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.NOMBRE_DEL_PROCESADOR, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.NUMERO_DE_PROCESADORES, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.PROCESADOR, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.SERIAL_DE_CPU, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.TIENE_CD, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.TIPO_DEL_PROCESADOR, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.VELOCIDAD_PROCESADOR, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.VERSION_DE_BIOS, Longitud, Texto);
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.BRANDID = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.CRONO_PROCESADOR = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.FABRICANTE_DEL_PROCESADOR = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.FECHA_DE_BIOS = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.HYPER = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.IDCPU = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.MARCA_DE_CPU = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.MEMORIA_DISPONIBLE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.MEMORIA_TOTAL_CPU = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.MEMORIA_TOTAL_SIST = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.MEMORIA_VIRTUAL = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.MEMORIA_VIRTUAL_DISPONIBLE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.MODELO_DE_CPU = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.NOMBRE_DE_BIOS = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.NOMBRE_DEL_PROCESADOR = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.NUMERO_DE_PROCESADORES = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.PROCESADOR = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.SERIAL_DE_CPU = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.TIENE_CD = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.TIPO_DEL_PROCESADOR = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.VELOCIDAD_PROCESADOR = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.VERSION_DE_BIOS = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
    }
}

//**********************   METHODS server  ********************************************************************************************
//DataSet Function 

Persistence.RemoteHelp.Methods.CPU_Fill = function (CPUList, StrIDCPU/*IDCPU*/) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    CPUList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDCPU == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.CPU.IDCPU.FieldName, " = " + UsrCfg.InternoAtisNames.CPU.NAME_TABLE + "." + UsrCfg.InternoAtisNames.CPU.IDCPU.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDCPU = " IN (" + StrIDCPU + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.CPU.IDCPU.FieldName, StrIDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //Param.AddInt32(UsrCfg.InternoDataLinkNames.CPU.IDCPU.FieldName, IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {
        var DS_CPU = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "CPU_GET", Param.ToBytes());
        ResErr = DS_CPU.ResErr;
        if (ResErr.NotError) {
            if (DS_CPU.DataSet.RecordCount > 0) {
                DS_CPU.DataSet.First();
                while (!(DS_CPU.DataSet.Eof)) {
                    var CPU = new Persistence.RemoteHelp.Properties.TCPU();
                    CPU.IDCPU = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.IDCPU.FieldName).asString();
                    //Other
                    CPU.BRANDID = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.BRANDID.FieldName).asString();
                    CPU.CRONO_PROCESADOR = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.CRONO_PROCESADOR.FieldName).asInt32();
                    CPU.FABRICANTE_DEL_PROCESADOR = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.FABRICANTE_DEL_PROCESADOR.FieldName).asString();
                    CPU.FECHA_DE_BIOS = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.FECHA_DE_BIOS.FieldName).asString();
                    CPU.HYPER = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.HYPER.FieldName).asString();
                    CPU.MARCA_DE_CPU = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.MARCA_DE_CPU.FieldName).asString();
                    CPU.MEMORIA_DISPONIBLE = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.MEMORIA_DISPONIBLE.FieldName).asInt32();
                    CPU.MEMORIA_TOTAL_CPU = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.MEMORIA_TOTAL_CPU.FieldName).asInt32();
                    CPU.MEMORIA_TOTAL_SIST = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.MEMORIA_TOTAL_SIST.FieldName).asInt32();
                    CPU.MEMORIA_VIRTUAL = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.MEMORIA_VIRTUAL.FieldName).asInt32();
                    CPU.MEMORIA_VIRTUAL_DISPONIBLE = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.MEMORIA_VIRTUAL_DISPONIBLE.FieldName).asInt32();
                    CPU.MODELO_DE_CPU = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.MODELO_DE_CPU.FieldName).asString();
                    CPU.NOMBRE_DE_BIOS = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.NOMBRE_DE_BIOS.FieldName).asString();
                    CPU.NOMBRE_DEL_PROCESADOR = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.NOMBRE_DEL_PROCESADOR.FieldName).asString();
                    CPU.NUMERO_DE_PROCESADORES = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.NUMERO_DE_PROCESADORES.FieldName).asInt32();
                    CPU.PROCESADOR = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.PROCESADOR.FieldName).asString();
                    CPU.SERIAL_DE_CPU = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.SERIAL_DE_CPU.FieldName).asString();
                    CPU.TIENE_CD = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.TIENE_CD.FieldName).asString();
                    CPU.TIPO_DEL_PROCESADOR = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.TIPO_DEL_PROCESADOR.FieldName).asString();
                    CPU.VELOCIDAD_PROCESADOR = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.VELOCIDAD_PROCESADOR.FieldName).asInt32();
                    CPU.VERSION_DE_BIOS = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.VERSION_DE_BIOS.FieldName).asString();
                    CPUList.push(CPU);

                    DS_CPU.DataSet.Next();
                }
            }
            else {
                DS_CPU.ResErr.NotError = false;
                DS_CPU.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

Persistence.RemoteHelp.Methods.CPU_GETID = function (CPU) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddString(UsrCfg.InternoAtisNames.CPU.IDCPU.FieldName, CPU.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.CPU.BRANDID.FieldName, CPU.BRANDID, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CPU.CRONO_PROCESADOR.FieldName, CPU.CRONO_PROCESADOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.FABRICANTE_DEL_PROCESADOR.FieldName, CPU.FABRICANTE_DEL_PROCESADOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.FECHA_DE_BIOS.FieldName, CPU.FECHA_DE_BIOS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.HYPER.FieldName, CPU.HYPER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.MARCA_DE_CPU.FieldName, CPU.MARCA_DE_CPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CPU.MEMORIA_DISPONIBLE.FieldName, CPU.MEMORIA_DISPONIBLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CPU.MEMORIA_TOTAL_CPU.FieldName, CPU.MEMORIA_TOTAL_CPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CPU.MEMORIA_TOTAL_SIST.FieldName, CPU.MEMORIA_TOTAL_SIST, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CPU.MEMORIA_VIRTUAL.FieldName, CPU.MEMORIA_VIRTUAL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CPU.MEMORIA_VIRTUAL_DISPONIBLE.FieldName, CPU.MEMORIA_VIRTUAL_DISPONIBLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.MODELO_DE_CPU.FieldName, CPU.MODELO_DE_CPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.NOMBRE_DE_BIOS.FieldName, CPU.NOMBRE_DE_BIOS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.NOMBRE_DEL_PROCESADOR.FieldName, CPU.NOMBRE_DEL_PROCESADOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CPU.NUMERO_DE_PROCESADORES.FieldName, CPU.NUMERO_DE_PROCESADORES, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.PROCESADOR.FieldName, CPU.PROCESADOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.SERIAL_DE_CPU.FieldName, CPU.SERIAL_DE_CPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.TIENE_CD.FieldName, CPU.TIENE_CD, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.TIPO_DEL_PROCESADOR.FieldName, CPU.TIPO_DEL_PROCESADOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CPU.VELOCIDAD_PROCESADOR.FieldName, CPU.VELOCIDAD_PROCESADOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.VERSION_DE_BIOS.FieldName, CPU.VERSION_DE_BIOS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        var DS_CPU = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "CPU_GETID", Param.ToBytes());
        ResErr = DS_CPU.ResErr;
        if (ResErr.NotError) {
            if (DS_CPU.DataSet.RecordCount > 0) {
                DS_CPU.DataSet.First();
                CPU.IDCPU = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.IDCPU.FieldName).asString();
                //Other
                CPU.BRANDID = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.BRANDID.FieldName).asString();
                CPU.CRONO_PROCESADOR = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.CRONO_PROCESADOR.FieldName).asInt32();
                CPU.FABRICANTE_DEL_PROCESADOR = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.FABRICANTE_DEL_PROCESADOR.FieldName).asString();
                CPU.FECHA_DE_BIOS = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.FECHA_DE_BIOS.FieldName).asString();
                CPU.HYPER = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.HYPER.FieldName).asString();
                CPU.MARCA_DE_CPU = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.MARCA_DE_CPU.FieldName).asString();
                CPU.MEMORIA_DISPONIBLE = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.MEMORIA_DISPONIBLE.FieldName).asInt32();
                CPU.MEMORIA_TOTAL_CPU = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.MEMORIA_TOTAL_CPU.FieldName).asInt32();
                CPU.MEMORIA_TOTAL_SIST = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.MEMORIA_TOTAL_SIST.FieldName).asInt32();
                CPU.MEMORIA_VIRTUAL = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.MEMORIA_VIRTUAL.FieldName).asInt32();
                CPU.MEMORIA_VIRTUAL_DISPONIBLE = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.MEMORIA_VIRTUAL_DISPONIBLE.FieldName).asInt32();
                CPU.MODELO_DE_CPU = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.MODELO_DE_CPU.FieldName).asString();
                CPU.NOMBRE_DE_BIOS = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.NOMBRE_DE_BIOS.FieldName).asString();
                CPU.NOMBRE_DEL_PROCESADOR = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.NOMBRE_DEL_PROCESADOR.FieldName).asString();
                CPU.NUMERO_DE_PROCESADORES = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.NUMERO_DE_PROCESADORES.FieldName).asInt32();
                CPU.PROCESADOR = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.PROCESADOR.FieldName).asString();
                CPU.SERIAL_DE_CPU = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.SERIAL_DE_CPU.FieldName).asString();
                CPU.TIENE_CD = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.TIENE_CD.FieldName).asString();
                CPU.TIPO_DEL_PROCESADOR = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.TIPO_DEL_PROCESADOR.FieldName).asString();
                CPU.VELOCIDAD_PROCESADOR = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.VELOCIDAD_PROCESADOR.FieldName).asInt32();
                CPU.VERSION_DE_BIOS = DS_CPU.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CPU.VERSION_DE_BIOS.FieldName).asString();
            }
            else {
                DS_CPU.ResErr.NotError = false;
                DS_CPU.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

Persistence.RemoteHelp.Methods.CPU_ADD = function (CPU) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddString(UsrCfg.InternoAtisNames.CPU.IDCPU.FieldName, CPU.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.CPU.BRANDID.FieldName, CPU.BRANDID, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CPU.CRONO_PROCESADOR.FieldName, CPU.CRONO_PROCESADOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.FABRICANTE_DEL_PROCESADOR.FieldName, CPU.FABRICANTE_DEL_PROCESADOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.FECHA_DE_BIOS.FieldName, CPU.FECHA_DE_BIOS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.HYPER.FieldName, CPU.HYPER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.MARCA_DE_CPU.FieldName, CPU.MARCA_DE_CPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CPU.MEMORIA_DISPONIBLE.FieldName, CPU.MEMORIA_DISPONIBLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CPU.MEMORIA_TOTAL_CPU.FieldName, CPU.MEMORIA_TOTAL_CPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CPU.MEMORIA_TOTAL_SIST.FieldName, CPU.MEMORIA_TOTAL_SIST, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CPU.MEMORIA_VIRTUAL.FieldName, CPU.MEMORIA_VIRTUAL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CPU.MEMORIA_VIRTUAL_DISPONIBLE.FieldName, CPU.MEMORIA_VIRTUAL_DISPONIBLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.MODELO_DE_CPU.FieldName, CPU.MODELO_DE_CPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.NOMBRE_DE_BIOS.FieldName, CPU.NOMBRE_DE_BIOS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.NOMBRE_DEL_PROCESADOR.FieldName, CPU.NOMBRE_DEL_PROCESADOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CPU.NUMERO_DE_PROCESADORES.FieldName, CPU.NUMERO_DE_PROCESADORES, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.PROCESADOR.FieldName, CPU.PROCESADOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.SERIAL_DE_CPU.FieldName, CPU.SERIAL_DE_CPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.TIENE_CD.FieldName, CPU.TIENE_CD, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.TIPO_DEL_PROCESADOR.FieldName, CPU.TIPO_DEL_PROCESADOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CPU.VELOCIDAD_PROCESADOR.FieldName, CPU.VELOCIDAD_PROCESADOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.VERSION_DE_BIOS.FieldName, CPU.VERSION_DE_BIOS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "CPU_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.RemoteHelp.Methods.CPU_GETID(CPU);
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

Persistence.RemoteHelp.Methods.CPU_UPD = function (CPU) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddString(UsrCfg.InternoAtisNames.CPU.IDCPU.FieldName, CPU.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.CPU.BRANDID.FieldName, CPU.BRANDID, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CPU.CRONO_PROCESADOR.FieldName, CPU.CRONO_PROCESADOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.FABRICANTE_DEL_PROCESADOR.FieldName, CPU.FABRICANTE_DEL_PROCESADOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.FECHA_DE_BIOS.FieldName, CPU.FECHA_DE_BIOS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.HYPER.FieldName, CPU.HYPER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.MARCA_DE_CPU.FieldName, CPU.MARCA_DE_CPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CPU.MEMORIA_DISPONIBLE.FieldName, CPU.MEMORIA_DISPONIBLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CPU.MEMORIA_TOTAL_CPU.FieldName, CPU.MEMORIA_TOTAL_CPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CPU.MEMORIA_TOTAL_SIST.FieldName, CPU.MEMORIA_TOTAL_SIST, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CPU.MEMORIA_VIRTUAL.FieldName, CPU.MEMORIA_VIRTUAL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CPU.MEMORIA_VIRTUAL_DISPONIBLE.FieldName, CPU.MEMORIA_VIRTUAL_DISPONIBLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.MODELO_DE_CPU.FieldName, CPU.MODELO_DE_CPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.NOMBRE_DE_BIOS.FieldName, CPU.NOMBRE_DE_BIOS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.NOMBRE_DEL_PROCESADOR.FieldName, CPU.NOMBRE_DEL_PROCESADOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CPU.NUMERO_DE_PROCESADORES.FieldName, CPU.NUMERO_DE_PROCESADORES, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.PROCESADOR.FieldName, CPU.PROCESADOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.SERIAL_DE_CPU.FieldName, CPU.SERIAL_DE_CPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.TIENE_CD.FieldName, CPU.TIENE_CD, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.TIPO_DEL_PROCESADOR.FieldName, CPU.TIPO_DEL_PROCESADOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CPU.VELOCIDAD_PROCESADOR.FieldName, CPU.VELOCIDAD_PROCESADOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.VERSION_DE_BIOS.FieldName, CPU.VERSION_DE_BIOS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "CPU_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.RemoteHelp.Methods.CPU_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.RemoteHelp.Methods.CPU_DELIDCPU(/*StrIDCPU*/Param);
    }
    else {
        Res = Persistence.RemoteHelp.Methods.CPU_DELCPU(Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.CPU_DELIDCPU = function (/*StrIDCPU*/IDCPU) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDCPU == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.CPU.IDCPU.FieldName, " = " + UsrCfg.InternoAtisNames.CPU.NAME_TABLE + "." + UsrCfg.InternoAtisNames.CPU.IDCPU.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDCPU = " IN (" + StrIDCPU + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.CPU.IDCPU.FieldName, StrIDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.CPU.IDCPU.FieldName, IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "CPU_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

Persistence.RemoteHelp.Methods.CPU_DELCPU = function (CPU) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddString(UsrCfg.InternoAtisNames.CPU.IDCPU.FieldName, CPU.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.CPU.BRANDID.FieldName, CPU.BRANDID, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CPU.CRONO_PROCESADOR.FieldName, CPU.CRONO_PROCESADOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.FABRICANTE_DEL_PROCESADOR.FieldName, CPU.FABRICANTE_DEL_PROCESADOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.FECHA_DE_BIOS.FieldName, CPU.FECHA_DE_BIOS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.HYPER.FieldName, CPU.HYPER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.MARCA_DE_CPU.FieldName, CPU.MARCA_DE_CPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CPU.MEMORIA_DISPONIBLE.FieldName, CPU.MEMORIA_DISPONIBLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CPU.MEMORIA_TOTAL_CPU.FieldName, CPU.MEMORIA_TOTAL_CPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CPU.MEMORIA_TOTAL_SIST.FieldName, CPU.MEMORIA_TOTAL_SIST, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CPU.MEMORIA_VIRTUAL.FieldName, CPU.MEMORIA_VIRTUAL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CPU.MEMORIA_VIRTUAL_DISPONIBLE.FieldName, CPU.MEMORIA_VIRTUAL_DISPONIBLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.MODELO_DE_CPU.FieldName, CPU.MODELO_DE_CPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.NOMBRE_DE_BIOS.FieldName, CPU.NOMBRE_DE_BIOS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.NOMBRE_DEL_PROCESADOR.FieldName, CPU.NOMBRE_DEL_PROCESADOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CPU.NUMERO_DE_PROCESADORES.FieldName, CPU.NUMERO_DE_PROCESADORES, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.PROCESADOR.FieldName, CPU.PROCESADOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.SERIAL_DE_CPU.FieldName, CPU.SERIAL_DE_CPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.TIENE_CD.FieldName, CPU.TIENE_CD, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.TIPO_DEL_PROCESADOR.FieldName, CPU.TIPO_DEL_PROCESADOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CPU.VELOCIDAD_PROCESADOR.FieldName, CPU.VELOCIDAD_PROCESADOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CPU.VERSION_DE_BIOS.FieldName, CPU.VERSION_DE_BIOS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "CPU_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//**********************   METHODS   ********************************************************************************************

//List Function 
Persistence.RemoteHelp.Methods.CPU_ListSetID = function (CPUList, IDCPU) {
    for (i = 0; i < CPUList.length; i++) {
        if (IDCPU == CPUList[i].IDCPU)
            return (CPUList[i]);

    }
    var UnCPU = new Persistence.RemoteHelp.Properties.TCPU();
    UnCPU.IDCPU = IDCPU;
    return (UnCPU);
}

Persistence.RemoteHelp.Methods.CPU_ListAdd = function (CPUList, CPU) {
    var i = Persistence.RemoteHelp.Methods.CPU_ListGetIndex(CPUList, CPU);
    if (i == -1) CPUList.push(CPU);
    else {
        CPUList[i].BRANDID = CPU.BRANDID;
        CPUList[i].CRONO_PROCESADOR = CPU.CRONO_PROCESADOR;
        CPUList[i].FABRICANTE_DEL_PROCESADOR = CPU.FABRICANTE_DEL_PROCESADOR;
        CPUList[i].FECHA_DE_BIOS = CPU.FECHA_DE_BIOS;
        CPUList[i].HYPER = CPU.HYPER;
        CPUList[i].IDCPU = CPU.IDCPU;
        CPUList[i].MARCA_DE_CPU = CPU.MARCA_DE_CPU;
        CPUList[i].MEMORIA_DISPONIBLE = CPU.MEMORIA_DISPONIBLE;
        CPUList[i].MEMORIA_TOTAL_CPU = CPU.MEMORIA_TOTAL_CPU;
        CPUList[i].MEMORIA_TOTAL_SIST = CPU.MEMORIA_TOTAL_SIST;
        CPUList[i].MEMORIA_VIRTUAL = CPU.MEMORIA_VIRTUAL;
        CPUList[i].MEMORIA_VIRTUAL_DISPONIBLE = CPU.MEMORIA_VIRTUAL_DISPONIBLE;
        CPUList[i].MODELO_DE_CPU = CPU.MODELO_DE_CPU;
        CPUList[i].NOMBRE_DE_BIOS = CPU.NOMBRE_DE_BIOS;
        CPUList[i].NOMBRE_DEL_PROCESADOR = CPU.NOMBRE_DEL_PROCESADOR;
        CPUList[i].NUMERO_DE_PROCESADORES = CPU.NUMERO_DE_PROCESADORES;
        CPUList[i].PROCESADOR = CPU.PROCESADOR;
        CPUList[i].SERIAL_DE_CPU = CPU.SERIAL_DE_CPU;
        CPUList[i].TIENE_CD = CPU.TIENE_CD;
        CPUList[i].TIPO_DEL_PROCESADOR = CPU.TIPO_DEL_PROCESADOR;
        CPUList[i].VELOCIDAD_PROCESADOR = CPU.VELOCIDAD_PROCESADOR;
        CPUList[i].VERSION_DE_BIOS = CPU.VERSION_DE_BIOS;
    }
}

//CJRC_26072018
Persistence.RemoteHelp.Methods.CPU_ListGetIndex = function (CPUList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.RemoteHelp.Methods.CPU_ListGetIndexIDCPU(CPUList, Param);

    }
    else {
        Res = Persistence.RemoteHelp.Methods.CPU_ListGetIndexCPU(CPUList, Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.CPU_ListGetIndexCPU = function (CPUList, CPU) {
    for (i = 0; i < CPUList.length; i++) {
        if (CPU.IDCPU == CPUList[i].IDCPU)
            return (i);
    }
    return (-1);
}

Persistence.RemoteHelp.Methods.CPU_ListGetIndexIDCPU = function (CPUList, IDCPU) {
    for (i = 0; i < CPUList.length; i++) {
        if (IDCPU == CPUList[i].IDCPU)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.RemoteHelp.Methods.CPUListtoStr = function (CPUList) {
    var Res = Persistence.RemoteHelp.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(CPUList.length, Longitud, Texto);
        for (Counter = 0; Counter < CPUList.length; Counter++) {
            var UnCPU = CPUList[Counter];
            UnCPU.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.RemoteHelp.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.RemoteHelp.Methods.StrtoCPU = function (ProtocoloStr, CPUList) {
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        CPUList.length = 0;
        if (Persistence.RemoteHelp.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnCPU = new Persistence.Demo.Properties.TCPU(); //Mode new row
                UnCPU.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.CPU_ListAdd(CPUList, UnCPU);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}

//Socket Function
Persistence.RemoteHelp.Methods.CPUListToByte = function(CPUList, MemStream, idx)
{
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, CPUList.length - idx);
    for (i = idx; i < CPUList.length; i++) {
        CPUList[i].ToByte(MemStream);
    }
}

Persistence.RemoteHelp.Methods.ByteToCPUList = function (CPUList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnCPU = new Persistence.RemoteHelp.Properties.TCPU();
        UnCPU.ByteTo(MemStream);
        Persistence.RemoteHelp.Methods.CPU_ListAdd(CPUList, UnCPU);
    }
}

//*******************************************************************************************************************************