﻿
Persistence.RemoteHelp.Properties._Version = "001";

Persistence.RemoteHelp.Properties.TREQUEST_RESULTTYPE = 
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _None: { value:0, name:"None"},
    OK_1: { value:1, name:"OK"},
    NO_2: { value:2, name:"NO"}
}

Persistence.RemoteHelp.Properties.TRHGROUPCRPERMISSION_TYPE = {
        
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _CannotEnter: { value:0, name:"NO PUEDE ENTRAR"},
    _YouCanEnterWithPermission: { value:1, name:"PUEDE ENTRAR CON PERMISO"},
    _YouCanEnterWithoutPermission: { value:2, name:"PUEDE ENTRAR SIN PERMISO"}
}

Persistence.RemoteHelp.Properties.TREQUEST_STATUS = {        
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _CREATE: { value:0, name:"CREATE"                          },
    _ADDCONFIG: { value:1, name:"ADDCONFIG"                },
    _CONFIGURE: { value:2, name:"CONFIGURE"            },
    _INPROGREESS: { value:3, name:"INPROGREESS"    },
    _CLOSE: { value:4, name:"CLOSE"            },
    _CANCELLED: { value:5, name:"CANCELLED"}
}

Persistence.RemoteHelp.Properties.TREQUEST_CPUPL_STATUS = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _CREATE: { value:0, name:"CREATE"                           },
    _ADDCONFIG: { value:1, name:"ADDCONFIG"                 },
    _CONFIGURE: { value:2, name:"CONFIGURE"             },
    _INPROGREESS: { value:3, name:"INPROGREESS"     },       
    _SUCCESFULLY: { value:4, name:"SUCCESFULLY" },           
    _FAILED: { value:5, name:"FAILED"       },     
    _RELOAD: { value:6, name:"RELOAD"   }
}

Persistence.RemoteHelp.Properties.TREQUEST_CPUCR_STATUS = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _CREATE: { value:0, name:"CREATE"                           },
    _ADDCONFIG: { value:1, name:"ADDCONFIG"                 },
    _CONFIGURE: { value:2, name:"CONFIGURE"             },
    _INPROGREESS: { value:3, name:"INPROGREESS"     },
    _SUCCESFULLY: { value:4, name:"SUCCESFULLY" },
    _FAILED: { value:5, name:"FAILED"       },
    _RELOAD: { value:6, name:"RELOAD"   }
}     



Persistence.RemoteHelp.Properties.TRemoteHelpFuntion = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _None: { value: -1, name: "_None" },
    _SendMessage: { value: 0, name: "_SendMessage" },
    _Chat: { value: 1, name: "_Chat" },
    _RemoteControl: { value: 2, name: "_RemoteControl" },
    _GenerateInventory: { value: 3, name: "_GenerateInventory" },
    _UpdateAgent: { value: 4, name: "_UpdateAgent" },
    _ChangeID: { value: 5, name: "_ChangeID" },
    _LookDangerous: { value: 6, name: "_LookDangerous" },
    _SendingFilesandOrders: { value: 7, name: "_SendingFilesandOrders" },
    _RunLocally: { value: 8, name: "_RunLocally" },
    _UpdatePolicies: { value: 9, name: "_UpdatePolicies" },
    _RestartAllowed: { value: 10, name: "_RestartAllowed" },
    _UseBrowser: { value: 11, name: "_UseBrowser" },
    _Survey: { value: 12, name: "_Survey" },
    _PoweronPC: { value: 13, name: "_PoweronPC" },
    _Voice: { value: 14, name: "_Voice" },
    _IntelvProtechnology: { value: 15, name: "_IntelvProtechnology" },
    _DeletePC: { value: 16, name: "_DeletePC" }
}

Persistence.RemoteHelp.Properties.TRHMODULE_Funtion = function()
{
    this.RemoteHelpFuntion = undefined;//Persistence.RemoteHelp.Properties.TRemoteHelpFuntion
    this.RHMODULE_CREATE = undefined;//Persistence.RemoteHelp.Properties.TRHMODULE
    this.Enable = false;
    this.ATIS = false;
    this.ITHelpCenter = false;
    this.CMDB = false;
    this.RemoteHelp = false;
}

Persistence.RemoteHelp.Properties.TRHMODULE_ACTIVE = function()
{
    this.IDRHMODULE = Persistence.RemoteHelp.Properties.TRHMODULE.GetEnum(undefined);
    this.Enable = false;
    this.LastUpdate = new Date(1970, 0, 1, 0, 0, 0);
    this.Counter = 0;
    this.IDRHMODULE_ACTIVE = false;
    this.IDRHMODULE_DATE = new Date(1970, 0, 1, 0, 0, 0);
    this.IDCMDBCI = 0;

    //Socket IO Properties
    this.ToByte = function(MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamInt32(IDRHMODULE.value);
        SysCfg.Stream.Methods.WriteStreamBoolean(Enable);
        SysCfg.Stream.Methods.WriteStreamDateTime(LastUpdate);
        SysCfg.Stream.Methods.WriteStreamInt32(Counter);
        SysCfg.Stream.Methods.WriteStreamBoolean(IDRHMODULE_ACTIVE);
        SysCfg.Stream.Methods.WriteStreamDateTime(IDRHMODULE_DATE);
        SysCfg.Stream.Methods.WriteStreamInt32(IDCMDBCI);
    }
    this.ByteTo = function(MemStream)
    {
        IDRHMODULE =  Persistence.RemoteHelp.Properties.TRHMODULE.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        Enable = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        LastUpdate = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        Counter = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        IDRHMODULE_ACTIVE = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        IDRHMODULE_DATE = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        IDCMDBCI = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    }


    this.ToStr = function(Longitud,Texto)
    {
        SysCfg.Str.Protocol.WriteInt(IDRHMODULE.value,Longitud,Texto);
        SysCfg.Str.Protocol.WriteBool(Enable,Longitud,Texto);
        SysCfg.Str.Protocol.WriteDateTime(LastUpdate,Longitud,Texto);
        SysCfg.Str.Protocol.WriteInt(Counter,Longitud,Texto);
        SysCfg.Str.Protocol.WriteBool(IDRHMODULE_ACTIVE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteDateTime(IDRHMODULE_DATE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(IDCMDBCI, Longitud, Texto);

    }
    this.StrTo = function(Pos,Index,LongitudArray,Texto)
    {
        IDRHMODULE =  Persistence.RemoteHelp.Properties.TRHMODULE.GetEnum(SysCfg.Str.Protocol.ReadInt(Pos,Index, LongitudArray, Texto));
        Enable = SysCfg.Str.Protocol.ReadBool(Pos,Index, LongitudArray, Texto);
        LastUpdate = SysCfg.Str.Protocol.ReadDateTime(Pos,Index, LongitudArray, Texto);
        Counter = SysCfg.Str.Protocol.ReadInt(Pos,Index, LongitudArray, Texto);
        IDRHMODULE_ACTIVE = SysCfg.Str.Protocol.ReadBool(Pos,Index, LongitudArray, Texto);
        IDRHMODULE_DATE = SysCfg.Str.Protocol.ReadDateTime(Pos,Index, LongitudArray, Texto);
        IDCMDBCI = SysCfg.Str.Protocol.ReadInt(Pos,Index, LongitudArray, Texto);

    }
}

Persistence.RemoteHelp.Properties.TRHMODULE = {

    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _Default: { value: 0, name: "Default" },
    _ATIS: { value: 1, name: "ATIS" },
    _ITHelpCenter: { value: 2, name: "ITHelpCenter" },
    _CMDB: { value: 3, name: "CMDB" },
    _RemoteHelp: { value: 4, name: "RemoteHelp" } 
}


Persistence.RemoteHelp.Properties.TRHTYPEEXECUTE = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _None: { value: 0, name: "None" },
    _WIN32: { value: 1, name: "WIN32" },
    _BROWSER: { value: 2, name: "BROWSER" }
}

Persistence.RemoteHelp.Properties.TRHMode = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _None: { value:0, name:"none"        },
    _CR : { value:1, name:"Control"  },
    _PL: { value:2, name:"Policy"}
}

Persistence.RemoteHelp.Properties.TResultPing = {

    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _None: { value:0, name:"none"                 },
    _IP: { value:1, name:"IP"                 },
    _HostName: { value:2, name:"HostName" },
    _IDCPU: { value:3, name:"IDCPU"   }
}



//Persistence.RemoteHelp.Properties.TRHREQUESTRESULTTYPE = {
//    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
//    _None = 0, name:"None"
//    _IPADDRESS = 0, name:"IPADDRESS"
//    _HOSTNAME = 1, name:"HOSTNAME"
//}


//Persistence.RemoteHelp.Properties.TRHREQUESTRESULT = {
//    this.TRHREQUESTRESULTTYPE = Persistence.RemoteHelp.Properties.TRHREQUESTRESULTTYPE._None;
//}

Persistence.RemoteHelp.Properties.TRHREQUESTIP = function()
{
    this.DIRECCION_IP = "";
    this.MAC_IP = "";

    this.ToStr = function(Longitud,Texto)
    {
        SysCfg.Str.Protocol.WriteStr(this.DIRECCION_IP, Longitud,Texto);
        SysCfg.Str.Protocol.WriteStr(this.MAC_IP, Longitud,Texto);

    }
    this.StrTo = function(Pos,Index,LongitudArray,Texto)
    {
        this.DIRECCION_IP = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.MAC_IP = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
    }
}


Persistence.RemoteHelp.Properties.TRHREQUESTCR = function()
{            
    this.IDCPU  = "";
    this.HOST_NAME = "";
    this.IDRHGROUPCRPERMISSION = Persistence.RemoteHelp.Properties.TRHGROUPCRPERMISSION_TYPE._CannotEnter;
    this.PERMISSION = "";
    this.IDRHGROUPCR = 0;
    this.RHGROUPCR_NAME = "";
    this.TIPO = "";
    this.WINDOWS = "";
    this.VPROIP = "";
    this.ID_COMPANY_L = "";
    this.ResultPing =  Persistence.RemoteHelp.Properties.TResultPing._None;
    this.ResultPingStr = "";
    this.GENPARAM = "";
    this.GENPATH = "";
    this.GENERATE = false;
    this.SUCCESSFULLY = false;
    this.RHREQUESTIPList = new Array();//Persistence.RemoteHelp.Properties.TRHREQUESTIP

    this.ToStr = function (Longitud, Texto)
    {
        SysCfg.Str.Protocol.WriteStr(this.IDCPU,Longitud,Texto);
        SysCfg.Str.Protocol.WriteStr(this.HOST_NAME,Longitud,Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDRHGROUPCRPERMISSION.value,Longitud,Texto);
        SysCfg.Str.Protocol.WriteStr(this.PERMISSION,Longitud,Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDRHGROUPCR,Longitud,Texto);
        SysCfg.Str.Protocol.WriteStr(this.RHGROUPCR_NAME,Longitud,Texto);
        SysCfg.Str.Protocol.WriteStr(this.TIPO,Longitud,Texto);
        SysCfg.Str.Protocol.WriteStr(this.WINDOWS,Longitud,Texto);
        SysCfg.Str.Protocol.WriteStr(this.VPROIP,Longitud,Texto);
        SysCfg.Str.Protocol.WriteStr(this.ID_COMPANY_L,Longitud,Texto);
        SysCfg.Str.Protocol.WriteInt(this.ResultPing.value,Longitud,Texto);
        SysCfg.Str.Protocol.WriteStr(this.ResultPingStr,Longitud,Texto);
        SysCfg.Str.Protocol.WriteStr(this.GENPARAM,Longitud,Texto);
        SysCfg.Str.Protocol.WriteStr(this.GENPATH,Longitud,Texto);
        SysCfg.Str.Protocol.WriteBool(this.GENERATE,Longitud,Texto);
        SysCfg.Str.Protocol.WriteBool(this.SUCCESSFULLY,Longitud,Texto);
        SysCfg.Str.Protocol.WriteInt(this.RHREQUESTIPList.Count,Longitud,Texto);
        for (var i = 0; i < this.RHREQUESTIPList.Count; i++)
        {
            SysCfg.Str.Protocol.WriteStr(this.RHREQUESTIPList[i].DIRECCION_IP,Longitud,Texto);
            SysCfg.Str.Protocol.WriteStr(this.RHREQUESTIPList[i].MAC_IP,Longitud,Texto);
        }
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto)
    {
        this.IDCPU = SysCfg.Str.Protocol.ReadStr(Pos,Index, LongitudArray, Texto);
        this.HOST_NAME = SysCfg.Str.Protocol.ReadStr(Pos,Index, LongitudArray, Texto);
        this.IDRHGROUPCRPERMISSION = Persistence.RemoteHelp.Properties.TRHGROUPCRPERMISSION_TYPE.GetEnum(SysCfg.Str.Protocol.ReadInt(Pos,Index, LongitudArray, Texto));
        this.PERMISSION = SysCfg.Str.Protocol.ReadStr(Pos,Index, LongitudArray, Texto);
        this.IDRHGROUPCR = SysCfg.Str.Protocol.ReadInt(Pos,Index, LongitudArray, Texto);
        this.RHGROUPCR_NAME = SysCfg.Str.Protocol.ReadStr(Pos,Index, LongitudArray, Texto);
        this.TIPO = SysCfg.Str.Protocol.ReadStr(Pos,Index, LongitudArray, Texto);
        this.WINDOWS = SysCfg.Str.Protocol.ReadStr(Pos,Index, LongitudArray, Texto);
        this.VPROIP = SysCfg.Str.Protocol.ReadStr(Pos,Index, LongitudArray, Texto);
        this.ID_COMPANY_L = SysCfg.Str.Protocol.ReadStr(Pos,Index, LongitudArray, Texto);
        this.ResultPing = Persistence.RemoteHelp.Properties.TResultPing.GetEnum( SysCfg.Str.Protocol.ReadInt(Pos,Index, LongitudArray, Texto));
        this.ResultPingStr = SysCfg.Str.Protocol.ReadStr(Pos,Index, LongitudArray, Texto);
        this.GENPARAM = SysCfg.Str.Protocol.ReadStr(Pos,Index, LongitudArray, Texto);
        this.GENPATH = SysCfg.Str.Protocol.ReadStr(Pos,Index, LongitudArray, Texto);
        this.GENERATE = SysCfg.Str.Protocol.ReadBool(Pos,Index, LongitudArray, Texto);
        this.SUCCESSFULLY = SysCfg.Str.Protocol.ReadBool(Pos,Index, LongitudArray, Texto);
        var Count = SysCfg.Str.Protocol.ReadInt(Pos,Index, LongitudArray, Texto);
        this.RHREQUESTIPList.Clear();
        for (var i = 0; i < Count; i++)
        {
            var RHREQUESTIP = new Persistence.RemoteHelp.Properties.TRHREQUESTIP();
            RHREQUESTIP.DIRECCION_IP = SysCfg.Str.Protocol.ReadStr(Pos,Index, LongitudArray, Texto);
            RHREQUESTIP.MAC_IP = SysCfg.Str.Protocol.ReadStr(Pos,Index, LongitudArray, Texto);
            this.RHREQUESTIPList.push(RHREQUESTIP);
        }
    }
}

Persistence.RemoteHelp.Properties.TRHREQUESTPL = function()
{

    this.IDCPU = "";
    this.HOST_NAME= "";
    this.PERMISSION = "";////ConfigBdd.OTROSDATOS.OK
    this.IDRHGROUPPL= 0;
    this.RHGROUPPL_NAME = "";//virtual 
    this.WINDOWS = "";
    this.TIPO = "";
    this.VPROIP = "";
    this.ID_COMPANY_L = "";
    this.RHREQUESTIPList = new Array();//Persistence.RemoteHelp.Properties.TRHREQUESTIP

    this.ToStr = function (Longitud, Texto)
    {
        SysCfg.Str.Protocol.WriteStr(this.IDCPU, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.HOST_NAME, Longitud, Texto);                
        SysCfg.Str.Protocol.WriteStr(this.PERMISSION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDRHGROUPPL, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.RHGROUPPL_NAME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.TIPO, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.WINDOWS, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.VPROIP, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.ID_COMPANY_L, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.RHREQUESTIPList.Count, Longitud, Texto);
        for (var i = 0; i < this.RHREQUESTIPList.Count; i++)
        {
            SysCfg.Str.Protocol.WriteStr(this.RHREQUESTIPList[i].DIRECCION_IP, Longitud, Texto);
            SysCfg.Str.Protocol.WriteStr(this.RHREQUESTIPList[i].MAC_IP, Longitud, Texto);
        }
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto)
    {
        this.IDCPU = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.HOST_NAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);                
        this.PERMISSION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.IDRHGROUPPL = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.RHGROUPPL_NAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.TIPO = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.WINDOWS = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.VPROIP = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.ID_COMPANY_L = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        var Count = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.RHREQUESTIPList.Clear();
        for (var i = 0; i < Count; i++)
        {
            var RHREQUESTIP = new Persistence.RemoteHelp.Properties.TRHREQUESTIP();
            RHREQUESTIP.DIRECCION_IP = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
            RHREQUESTIP.MAC_IP = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
            this.RHREQUESTIPList.push(RHREQUESTIP);
        }

    }
}



////Str IO Properties
//this.ToStr = function (Longitud,Texto)
//{
//    //SysCfg.Str.Protocol.WriteInt(this.IDRHGROUPCR, Longitud, Texto);
//    //SysCfg.Str.Protocol.WriteStr(this.RHGROUPCR_DESCRIPTION, Longitud, Texto);
//    //SysCfg.Str.Protocol.WriteStr(this.RHGROUPCR_NAME, Longitud, Texto);
//}
//this.StrTo = function (Pos,Index,LongitudArray,Texto)
//{
//    //this.IDRHGROUPCR = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
//    //this.RHGROUPCR_DESCRIPTION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
//    //this.RHGROUPCR_NAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
//}



    
