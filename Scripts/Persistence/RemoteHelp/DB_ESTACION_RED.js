﻿//TreduceDB
//Persistence.RemoteHelp.Properties
//Persistence.RemoteHelp.Methods

Persistence.RemoteHelp.Properties.TESTACION_RED = function () {
    this.DESCRIPCION = "";
    this.DOMINIO = "";
    this.GRUPO_DE_TRABAJO = "";
    this.HOST_NAME = "";
    this.IDCPU = "";
    this.NOMBRE_ESTACION = "";
    this.NOMBRE_USUARIO = "";
    this.ORGANIZACION = "";
    this.REGISTRADO_POR = "";
    this.SERVIDOR = "";

    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.DESCRIPCION);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.DOMINIO);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.GRUPO_DE_TRABAJO);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.HOST_NAME);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.IDCPU);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.NOMBRE_ESTACION);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.NOMBRE_USUARIO);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.ORGANIZACION);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.REGISTRADO_POR);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.SERVIDOR);
    }
    this.ByteTo = function (MemStream) {
        this.DESCRIPCION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.DOMINIO = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.GRUPO_DE_TRABAJO = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.HOST_NAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDCPU = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.NOMBRE_ESTACION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.NOMBRE_USUARIO = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.ORGANIZACION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.REGISTRADO_POR = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.SERVIDOR = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteStr(this.DESCRIPCION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.DOMINIO, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.GRUPO_DE_TRABAJO, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.HOST_NAME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.IDCPU, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.NOMBRE_ESTACION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.NOMBRE_USUARIO, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.ORGANIZACION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.REGISTRADO_POR, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.SERVIDOR, Longitud, Texto);
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto)
    {
        this.DESCRIPCION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.DOMINIO = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.GRUPO_DE_TRABAJO = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.HOST_NAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.IDCPU = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.NOMBRE_ESTACION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.NOMBRE_USUARIO = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.ORGANIZACION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.REGISTRADO_POR = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.SERVIDOR = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
    }
}


//**********************   METHODS server  ********************************************************************************************

Persistence.RemoteHelp.Methods.ESTACION_RED_Fill = function (ESTACION_REDList, StrIDCPU) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    ESTACION_REDList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDCPU == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.ESTACION_RED.IDCPU.FieldName, " = " + UsrCfg.InternoAtisNames.ESTACION_RED.NAME_TABLE + "." + UsrCfg.InternoAtisNames.ESTACION_RED.IDCPU.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDCPU = " IN (" + StrIDCPU + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.ESTACION_RED.IDCPU.FieldName, StrIDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    try {
        var DS_ESTACION_RED = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "ESTACION_RED_GET", Param.ToBytes());
        ResErr = DS_ESTACION_RED.ResErr;
        if (ResErr.NotError) {
            if (DS_ESTACION_RED.DataSet.RecordCount > 0) {
                DS_ESTACION_RED.DataSet.First();
                while (!(DS_ESTACION_RED.DataSet.Eof)) {
                    var ESTACION_RED = new Persistence.RemoteHelp.Properties.TESTACION_RED();
                    //Other
                    ESTACION_RED.DESCRIPCION = DS_ESTACION_RED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ESTACION_RED.DESCRIPCION.FieldName).asString();
                    ESTACION_RED.DOMINIO = DS_ESTACION_RED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ESTACION_RED.DOMINIO.FieldName).asString();
                    ESTACION_RED.GRUPO_DE_TRABAJO = DS_ESTACION_RED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ESTACION_RED.GRUPO_DE_TRABAJO.FieldName).asString();
                    ESTACION_RED.HOST_NAME = DS_ESTACION_RED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ESTACION_RED.HOST_NAME.FieldName).asString();
                    ESTACION_RED.IDCPU = DS_ESTACION_RED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ESTACION_RED.IDCPU.FieldName).asString();
                    ESTACION_RED.NOMBRE_ESTACION = DS_ESTACION_RED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ESTACION_RED.NOMBRE_ESTACION.FieldName).asString();
                    ESTACION_RED.NOMBRE_USUARIO = DS_ESTACION_RED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ESTACION_RED.NOMBRE_USUARIO.FieldName).asString();
                    ESTACION_RED.ORGANIZACION = DS_ESTACION_RED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ESTACION_RED.ORGANIZACION.FieldName).asString();
                    ESTACION_RED.REGISTRADO_POR = DS_ESTACION_RED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ESTACION_RED.REGISTRADO_POR.FieldName).asString();
                    ESTACION_RED.SERVIDOR = DS_ESTACION_RED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ESTACION_RED.SERVIDOR.FieldName).asString();
                    ESTACION_REDList.push(ESTACION_RED);

                    DS_ESTACION_RED.DataSet.Next();
                }
            }
            else {
                DS_ESTACION_RED.ResErr.NotError = false;
                DS_ESTACION_RED.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.ESTACION_RED_GETID = function (ESTACION_RED) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.ESTACION_RED.DESCRIPCION.FieldName, ESTACION_RED.DESCRIPCION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.ESTACION_RED.DOMINIO.FieldName, ESTACION_RED.DOMINIO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.ESTACION_RED.GRUPO_DE_TRABAJO.FieldName, ESTACION_RED.GRUPO_DE_TRABAJO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.ESTACION_RED.HOST_NAME.FieldName, ESTACION_RED.HOST_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.ESTACION_RED.IDCPU.FieldName, ESTACION_RED.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.ESTACION_RED.NOMBRE_ESTACION.FieldName, ESTACION_RED.NOMBRE_ESTACION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.ESTACION_RED.NOMBRE_USUARIO.FieldName, ESTACION_RED.NOMBRE_USUARIO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.ESTACION_RED.ORGANIZACION.FieldName, ESTACION_RED.ORGANIZACION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.ESTACION_RED.REGISTRADO_POR.FieldName, ESTACION_RED.REGISTRADO_POR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.ESTACION_RED.SERVIDOR.FieldName, ESTACION_RED.SERVIDOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var DS_ESTACION_RED = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "ESTACION_RED_GETID", Param.ToBytes());
        ResErr = DS_ESTACION_RED.ResErr;
        if (ResErr.NotError) {
            if (DS_ESTACION_RED.DataSet.RecordCount > 0) {
                DS_ESTACION_RED.DataSet.First();
                //Other
                ESTACION_RED.DESCRIPCION = DS_ESTACION_RED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ESTACION_RED.DESCRIPCION.FieldName).asString();
                ESTACION_RED.DOMINIO = DS_ESTACION_RED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ESTACION_RED.DOMINIO.FieldName).asString();
                ESTACION_RED.GRUPO_DE_TRABAJO = DS_ESTACION_RED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ESTACION_RED.GRUPO_DE_TRABAJO.FieldName).asString();
                ESTACION_RED.HOST_NAME = DS_ESTACION_RED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ESTACION_RED.HOST_NAME.FieldName).asString();
                ESTACION_RED.IDCPU = DS_ESTACION_RED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ESTACION_RED.IDCPU.FieldName).asString();
                ESTACION_RED.NOMBRE_ESTACION = DS_ESTACION_RED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ESTACION_RED.NOMBRE_ESTACION.FieldName).asString();
                ESTACION_RED.NOMBRE_USUARIO = DS_ESTACION_RED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ESTACION_RED.NOMBRE_USUARIO.FieldName).asString();
                ESTACION_RED.ORGANIZACION = DS_ESTACION_RED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ESTACION_RED.ORGANIZACION.FieldName).asString();
                ESTACION_RED.REGISTRADO_POR = DS_ESTACION_RED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ESTACION_RED.REGISTRADO_POR.FieldName).asString();
                ESTACION_RED.SERVIDOR = DS_ESTACION_RED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ESTACION_RED.SERVIDOR.FieldName).asString();
            }
            else {
                DS_ESTACION_RED.ResErr.NotError = false;
                DS_ESTACION_RED.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.ESTACION_RED_ADD = function (ESTACION_RED) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.ESTACION_RED.DESCRIPCION.FieldName, ESTACION_RED.DESCRIPCION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.ESTACION_RED.DOMINIO.FieldName, ESTACION_RED.DOMINIO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.ESTACION_RED.GRUPO_DE_TRABAJO.FieldName, ESTACION_RED.GRUPO_DE_TRABAJO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.ESTACION_RED.HOST_NAME.FieldName, ESTACION_RED.HOST_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.ESTACION_RED.IDCPU.FieldName, ESTACION_RED.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.ESTACION_RED.NOMBRE_ESTACION.FieldName, ESTACION_RED.NOMBRE_ESTACION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.ESTACION_RED.NOMBRE_USUARIO.FieldName, ESTACION_RED.NOMBRE_USUARIO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.ESTACION_RED.ORGANIZACION.FieldName, ESTACION_RED.ORGANIZACION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.ESTACION_RED.REGISTRADO_POR.FieldName, ESTACION_RED.REGISTRADO_POR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.ESTACION_RED.SERVIDOR.FieldName, ESTACION_RED.SERVIDOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "ESTACION_RED_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.RemoteHelp.Methods.ESTACION_RED_GETID(ESTACION_RED);
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

Persistence.RemoteHelp.Methods.ESTACION_RED_UPD = function (ESTACION_RED) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.ESTACION_RED.DESCRIPCION.FieldName, ESTACION_RED.DESCRIPCION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.ESTACION_RED.DOMINIO.FieldName, ESTACION_RED.DOMINIO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.ESTACION_RED.GRUPO_DE_TRABAJO.FieldName, ESTACION_RED.GRUPO_DE_TRABAJO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.ESTACION_RED.HOST_NAME.FieldName, ESTACION_RED.HOST_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.ESTACION_RED.IDCPU.FieldName, ESTACION_RED.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.ESTACION_RED.NOMBRE_ESTACION.FieldName, ESTACION_RED.NOMBRE_ESTACION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.ESTACION_RED.NOMBRE_USUARIO.FieldName, ESTACION_RED.NOMBRE_USUARIO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.ESTACION_RED.ORGANIZACION.FieldName, ESTACION_RED.ORGANIZACION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.ESTACION_RED.REGISTRADO_POR.FieldName, ESTACION_RED.REGISTRADO_POR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.ESTACION_RED.SERVIDOR.FieldName, ESTACION_RED.SERVIDOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "ESTACION_RED_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}


//CJRC_27072018
Persistence.RemoteHelp.Methods.ESTACION_RED_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.RemoteHelp.Methods.ESTACION_RED_DELIDCPU(/*String StrIDCPU*/ Param);
    }
    else {
        Res = Persistence.RemoteHelp.Methods.ESTACION_RED_DELESTACION_RED(Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.ESTACION_RED_DELIDCPU = function (/*String StrIDCPU*/ IDCPU) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDCPU == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.ESTACION_RED.IDCPU.FieldName, " = " + UsrCfg.InternoAtisNames.ESTACION_RED.NAME_TABLE + "." + UsrCfg.InternoAtisNames.ESTACION_RED.IDCPU.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDCPU = " IN (" + StrIDCPU + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.ESTACION_RED.IDCPU.FieldName, StrIDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.ESTACION_RED.IDCPU.FieldName, IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "ESTACION_RED_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

Persistence.RemoteHelp.Methods.ESTACION_RED_DELESTACION_RED = function (ESTACION_RED) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.ESTACION_RED.DESCRIPCION.FieldName, ESTACION_RED.DESCRIPCION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.ESTACION_RED.DOMINIO.FieldName, ESTACION_RED.DOMINIO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.ESTACION_RED.GRUPO_DE_TRABAJO.FieldName, ESTACION_RED.GRUPO_DE_TRABAJO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.ESTACION_RED.HOST_NAME.FieldName, ESTACION_RED.HOST_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.ESTACION_RED.IDCPU.FieldName, ESTACION_RED.IDCPU, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.ESTACION_RED.NOMBRE_ESTACION.FieldName, ESTACION_RED.NOMBRE_ESTACION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.ESTACION_RED.NOMBRE_USUARIO.FieldName, ESTACION_RED.NOMBRE_USUARIO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.ESTACION_RED.ORGANIZACION.FieldName, ESTACION_RED.ORGANIZACION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.ESTACION_RED.REGISTRADO_POR.FieldName, ESTACION_RED.REGISTRADO_POR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.ESTACION_RED.SERVIDOR.FieldName, ESTACION_RED.SERVIDOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "ESTACION_RED_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}


//**********************   METHODS   ********************************************************************************************

//List Function 
Persistence.RemoteHelp.Methods.ESTACION_RED_ListSetID = function (ESTACION_REDList, IDCPU) {
    for (i = 0; i < ESTACION_REDList.length; i++) {
        if (IDCPU == ESTACION_REDList[i].IDCPU)
            return (ESTACION_REDList[i]);

    }
    var UnESTACION_RED = new Persistence.RemoteHelp.Properties.TESTACION_RED();
    UnESTACION_RED.IDCPU = IDCPU;
    return (UnESTACION_RED);
}

Persistence.RemoteHelp.Methods.ESTACION_RED_ListAdd = function (ESTACION_REDList, ESTACION_RED) {
    var i = Persistence.RemoteHelp.Methods.ESTACION_RED_ListGetIndex(ESTACION_REDList, ESTACION_RED);
    if (i == -1) ESTACION_REDList.push(ESTACION_RED);
    else {
        ESTACION_REDList[i].DESCRIPCION = ESTACION_RED.DESCRIPCION;
        ESTACION_REDList[i].DOMINIO = ESTACION_RED.DOMINIO;
        ESTACION_REDList[i].GRUPO_DE_TRABAJO = ESTACION_RED.GRUPO_DE_TRABAJO;
        ESTACION_REDList[i].HOST_NAME = ESTACION_RED.HOST_NAME;
        ESTACION_REDList[i].IDCPU = ESTACION_RED.IDCPU;
        ESTACION_REDList[i].NOMBRE_ESTACION = ESTACION_RED.NOMBRE_ESTACION;
        ESTACION_REDList[i].NOMBRE_USUARIO = ESTACION_RED.NOMBRE_USUARIO;
        ESTACION_REDList[i].ORGANIZACION = ESTACION_RED.ORGANIZACION;
        ESTACION_REDList[i].REGISTRADO_POR = ESTACION_RED.REGISTRADO_POR;
        ESTACION_REDList[i].SERVIDOR = ESTACION_RED.SERVIDOR;
    }
}

//CJRC_26072018
Persistence.RemoteHelp.Methods.ESTACION_RED_ListGetIndex = function (ESTACION_REDList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.RemoteHelp.Methods.ESTACION_RED_ListGetIndexIDCPU(ESTACION_REDList, Param);

    }
    else {
        Res = Persistence.RemoteHelp.Methods.ESTACION_RED_ListGetIndexESTACION_RED(ESTACION_REDList, Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.ESTACION_RED_ListGetIndexESTACION_RED = function (ESTACION_REDList, ESTACION_RED) {
    for (var i = 0; i < ESTACION_REDList.length; i++) {
        if (ESTACION_RED.IDCPU == ESTACION_REDList[i].IDCPU)
            return (i);
    }
    return (-1);
}

Persistence.RemoteHelp.Methods.ESTACION_RED_ListGetIndexIDCPU = function (ESTACION_REDList, IDCPU) {
    for (var i = 0; i < ESTACION_REDList.length; i++) {
        if (IDCPU == ESTACION_REDList[i].IDCPU)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.RemoteHelp.Methods.ESTACION_REDListtoStr = function (ESTACION_REDList) {
    var Res = Persistence.RemoteHelp.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(ESTACION_REDList.length, Longitud, Texto);
        for (Counter = 0; Counter < ESTACION_REDList.length; Counter++) {
            var UnESTACION_RED = ESTACION_REDList[Counter];
            UnESTACION_RED.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.RemoteHelp.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.RemoteHelp.Methods.StrtoESTACION_RED = function (ProtocoloStr, ESTACION_REDList) {
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        ESTACION_REDList.length = 0;
        if (Persistence.RemoteHelp.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnESTACION_RED = new Persistence.Demo.Properties.TESTACION_RED(); //Mode new row
                UnESTACION_RED.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.ESTACION_RED_ListAdd(ESTACION_REDList, UnESTACION_RED);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}

//Socket Function 
Persistence.RemoteHelp.Methods.ESTACION_REDListToByte = function (ESTACION_REDList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, ESTACION_REDList.length - idx);
    for (var i = idx; i < ESTACION_REDList.length; i++) {
        ESTACION_REDList[i].ToByte(MemStream);
    }
}

Persistence.RemoteHelp.Methods.ByteToESTACION_REDList = function (ESTACION_REDList, MemStream) {
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (var i = 0; i < Count; i++) {
        UnESTACION_RED = new Persistence.RemoteHelp.Properties.TESTACION_RED();
        UnESTACION_RED.ByteTo(MemStream);
        Persistence.RemoteHelp.Methods.ESTACION_RED_ListAdd(ESTACION_REDList, UnESTACION_RED);
    }
}