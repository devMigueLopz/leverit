﻿//TreduceDB
//Persistence.RemoteHelp.Properties
//Persistence.RemoteHelp.Methods

Persistence.RemoteHelp.Properties.TRHGROUPCRPERMISSION = function () {
    this.IDRHGROUPCRPERMISSION = 0;
    this.PERMISSION = "";
    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDRHGROUPCRPERMISSION);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.PERMISSION);
    }
    this.ByteTo = function (MemStream) {
        this.IDRHGROUPCRPERMISSION = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.PERMISSION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDRHGROUPCRPERMISSION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.PERMISSION, Longitud, Texto);
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDRHGROUPCRPERMISSION = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.PERMISSION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
    }
}

//**********************   METHODS server  ********************************************************************************************

Persistence.RemoteHelp.Methods.RHGROUPCRPERMISSION_Fill = function (RHGROUPCRPERMISSIONList) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    RHGROUPCRPERMISSIONList.length = 0;
    try {
        var DS_RHGROUPCRPERMISSION = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "RHGROUPCRPERMISSION_GET", new Array());
        ResErr = DS_RHGROUPCRPERMISSION.ResErr;
        if (ResErr.NotError) {
            if (DS_RHGROUPCRPERMISSION.DataSet.RecordCount > 0) {
                DS_RHGROUPCRPERMISSION.DataSet.First();
                while (!(DS_RHGROUPCRPERMISSION.DataSet.Eof)) {
                    var RHGROUPCRPERMISSION = new Persistence.RemoteHelp.Properties.TRHGROUPCRPERMISSION();
                    RHGROUPCRPERMISSION.IDRHGROUPCRPERMISSION = DS_RHGROUPCRPERMISSION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPCRPERMISSION.IDRHGROUPCRPERMISSION.FieldName).asInt32();
                    RHGROUPCRPERMISSION.PERMISSION = DS_RHGROUPCRPERMISSION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPCRPERMISSION.PERMISSION.FieldName).asString();
                    RHGROUPCRPERMISSIONList.push(RHGROUPCRPERMISSION);

                    DS_RHGROUPCRPERMISSION.DataSet.Next();
                }
            }
            else {
                DS_RHGROUPCRPERMISSION.ResErr.NotError = false;
                DS_RHGROUPCRPERMISSION.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPCRPERMISSION_GETID = function (RHGROUPCRPERMISSION) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPCRPERMISSION.IDRHGROUPCRPERMISSION.FieldName, RHGROUPCRPERMISSION.IDRHGROUPCRPERMISSION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPCRPERMISSION.PERMISSION.FieldName, RHGROUPCRPERMISSION.PERMISSION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var DS_RHGROUPCRPERMISSION = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "RHGROUPCRPERMISSION_GETID", Param.ToBytes());
        ResErr = DS_RHGROUPCRPERMISSION.ResErr;
        if (ResErr.NotError) {
            if (DS_RHGROUPCRPERMISSION.DataSet.RecordCount > 0) {
                DS_RHGROUPCRPERMISSION.DataSet.First();
                RHGROUPCRPERMISSION.IDRHGROUPCRPERMISSION = DS_RHGROUPCRPERMISSION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPCRPERMISSION.IDRHGROUPCRPERMISSION.FieldName).asInt32();
                //Other
                RHGROUPCRPERMISSION.PERMISSION = DS_RHGROUPCRPERMISSION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.RHGROUPCRPERMISSION.PERMISSION.FieldName).asString();
            }
            else {
                DS_RHGROUPCRPERMISSION.ResErr.NotError = false;
                DS_RHGROUPCRPERMISSION.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPCRPERMISSION_ADD = function (RHGROUPCRPERMISSION) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPCRPERMISSION.IDRHGROUPCRPERMISSION.FieldName, RHGROUPCRPERMISSION.IDRHGROUPCRPERMISSION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPCRPERMISSION.PERMISSION.FieldName, RHGROUPCRPERMISSION.PERMISSION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHGROUPCRPERMISSION_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.RemoteHelp.Methods.RHGROUPCRPERMISSION_GETID(RHGROUPCRPERMISSION);
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPCRPERMISSION_UPD = function (RHGROUPCRPERMISSION) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPCRPERMISSION.IDRHGROUPCRPERMISSION.FieldName, RHGROUPCRPERMISSION.IDRHGROUPCRPERMISSION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPCRPERMISSION.PERMISSION.FieldName, RHGROUPCRPERMISSION.PERMISSION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHGROUPCRPERMISSION_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.RemoteHelp.Methods.RHGROUPCRPERMISSION_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.RemoteHelp.Methods.RHGROUPCRPERMISSION_DELIDRHGROUPCRPERMISSION(Param);
    }
    else {
        Res = Persistence.RemoteHelp.Methods.RHGROUPCRPERMISSION_DELRHGROUPCRPERMISSION(Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.RHGROUPCRPERMISSION_DELIDRHGROUPCRPERMISSION = function (IDRHGROUPCRPERMISSION) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPCRPERMISSION.IDRHGROUPCRPERMISSION.FieldName, IDRHGROUPCRPERMISSION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHGROUPCRPERMISSION_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.RemoteHelp.Methods.RHGROUPCRPERMISSION_DELRHGROUPCRPERMISSION = function (RHGROUPCRPERMISSION) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.RHGROUPCRPERMISSION.IDRHGROUPCRPERMISSION.FieldName, RHGROUPCRPERMISSION.IDRHGROUPCRPERMISSION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.RHGROUPCRPERMISSION.PERMISSION.FieldName, RHGROUPCRPERMISSION.PERMISSION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "RHGROUPCRPERMISSION_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}


//**********************   METHODS   ********************************************************************************************

Persistence.RemoteHelp.Methods.RHGROUPCRPERMISSION_ListSetID = function (RHGROUPCRPERMISSIONList, IDRHGROUPCRPERMISSION) {
    for (var i = 0; i < RHGROUPCRPERMISSIONList.length; i++) {

        if (IDRHGROUPCRPERMISSION == RHGROUPCRPERMISSIONList[i].IDRHGROUPCRPERMISSION)
            return (RHGROUPCRPERMISSIONList[i]);

    }
    var UnRHGROUPCRPERMISSION = new Persistence.RemoteHelp.Properties.TRHGROUPCRPERMISSION();
    UnRHGROUPCRPERMISSION.IDRHGROUPCRPERMISSION = IDRHGROUPCRPERMISSION;
    return (UnRHGROUPCRPERMISSION);
}
Persistence.RemoteHelp.Methods.RHGROUPCRPERMISSION_ListAdd = function (RHGROUPCRPERMISSIONList, RHGROUPCRPERMISSION) {
    var i = Persistence.RemoteHelp.Methods.RHGROUPCRPERMISSION_ListGetIndex(RHGROUPCRPERMISSIONList, RHGROUPCRPERMISSION);
    if (i == -1) RHGROUPCRPERMISSIONList.push(RHGROUPCRPERMISSION);
    else {
        RHGROUPCRPERMISSIONList[i].IDRHGROUPCRPERMISSION = RHGROUPCRPERMISSION.IDRHGROUPCRPERMISSION;
        RHGROUPCRPERMISSIONList[i].PERMISSION = RHGROUPCRPERMISSION.PERMISSION;
    }
}

//CJRC_26072018
Persistence.RemoteHelp.Methods.RHGROUPCRPERMISSION_ListGetIndex = function (RHGROUPCRPERMISSIONList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.RemoteHelp.Methods.RHGROUPCRPERMISSION_ListGetIndexIDRHGROUPCRPERMISSION(RHGROUPCRPERMISSIONList, Param);

    }
    else {
        Res = Persistence.RemoteHelp.Methods.RHGROUPCRPERMISSION_ListGetIndexRHGROUPCRPERMISSION(RHGROUPCRPERMISSIONList, Param);
    }
    return (Res);
}

Persistence.RemoteHelp.Methods.RHGROUPCRPERMISSION_ListGetIndexRHGROUPCRPERMISSION = function (RHGROUPCRPERMISSIONList, RHGROUPCRPERMISSION) {
    for (var i = 0; i < RHGROUPCRPERMISSIONList.length; i++) {
        if (RHGROUPCRPERMISSION.IDRHGROUPCRPERMISSION == RHGROUPCRPERMISSIONList[i].IDRHGROUPCRPERMISSION)
            return (i);
    }
    return (-1);
}
Persistence.RemoteHelp.Methods.RHGROUPCRPERMISSION_ListGetIndexIDRHGROUPCRPERMISSION = function (RHGROUPCRPERMISSIONList, IDRHGROUPCRPERMISSION) {
    for (var i = 0; i < RHGROUPCRPERMISSIONList.length; i++) {
        if (IDRHGROUPCRPERMISSION == RHGROUPCRPERMISSIONList[i].IDRHGROUPCRPERMISSION)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.RemoteHelp.Methods.RHGROUPCRPERMISSIONListtoStr = function (RHGROUPCRPERMISSIONList) {
    var Res = Persistence.RemoteHelp.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(RHGROUPCRPERMISSIONList.length, Longitud, Texto);
        for (Counter = 0; Counter < RHGROUPCRPERMISSIONList.length; Counter++) {
            var UnRHGROUPCRPERMISSION = RHGROUPCRPERMISSIONList[Counter];
            UnRHGROUPCRPERMISSION.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.RemoteHelp.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.RemoteHelp.Methods.StrtoRHGROUPCRPERMISSION = function (ProtocoloStr, RHGROUPCRPERMISSIONList) {
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        RHGROUPCRPERMISSIONList.length = 0;
        if (Persistence.RemoteHelp.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnRHGROUPCRPERMISSION = new Persistence.Demo.Properties.TRHGROUPCRPERMISSION(); //Mode new row
                UnRHGROUPCRPERMISSION.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.RHGROUPCRPERMISSION_ListAdd(RHGROUPCRPERMISSIONList, UnRHGROUPCRPERMISSION);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}

//Socket Function 
Persistence.RemoteHelp.Methods.RHGROUPCRPERMISSIONListToByte = function (RHGROUPCRPERMISSIONList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, RHGROUPCRPERMISSIONList.length - idx);
    for (var i = idx; i < RHGROUPCRPERMISSIONList.length; i++) {
        RHGROUPCRPERMISSIONList[i].ToByte(MemStream);
    }
}
Persistence.RemoteHelp.Methods.ByteToRHGROUPCRPERMISSIONList = function (RHGROUPCRPERMISSIONList, MemStream) {
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (var i = 0; i < Count; i++) {
        var UnRHGROUPCRPERMISSION = new Persistence.RemoteHelp.Properties.TRHGROUPCRPERMISSION();
        UnRHGROUPCRPERMISSION.ByteTo(MemStream);
        Persistence.RemoteHelp.Methods.RHGROUPCRPERMISSION_ListAdd(RHGROUPCRPERMISSIONList, UnRHGROUPCRPERMISSION);
    }
}