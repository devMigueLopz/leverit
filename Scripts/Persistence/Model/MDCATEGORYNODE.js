﻿//TreduceDB
//Persistence.Model.Properties
//Persistence.Model.Methods

/*
  CATEGORY1                CATEGORY2        CATEGORY3      CATEGORY4    CATEGORY5    CATEGORY6    CATEGORY7    CATEGORY8    IDMDCATEGORY  
  Servicio de Seguridad    Desconocido                                                                                      1             
  Servicio de Seguridad    Firewall         Desconocido                                                                     2             
  Servicio de Seguridad    Antivirus        Servidor                                                                        3             
  Servicio de Seguridad    Antivirus        Personal                                                                        4             
  Servicio de Impresion    Desconocido                                                                                      5             
  Servicio de Impresion    Impresion Web    Desconocido                                                                     6             
  Servicio de Nomina       Desconocido                                                                                      7             
  Servicio de Nomina       Servidor                                                                                         8             
*/

/*Manda a web sercice 2*/

Persistence.Model.Properties.TCATEGORYNODE = function () {
    this.NODENAME = "";
    this.CATEGORYNODEList = new Array();//Persistence.Model.Properties.TCATEGORYNODE   // si este queda en 0
    //** Cuando es ultimo 
    this.IsEndNode = false;
    this.Index = 0;
    this.MDCATEGORY = new Persistence.Model.Properties.TMDCATEGORY();
}

Persistence.Model.Methods.GETCATEGORYNODE = function (CATEGORYNODEAllList, CATEGORYNODEBeginList, MDCATEGORY, StringList, idx) {
    var CATEGORYNODE = Persistence.Model.Methods.CATEGORYNODE_ListSetID(CATEGORYNODEBeginList, StringList[idx]);
    if (Persistence.Model.Methods.CATEGORYNODE_ListAdd(CATEGORYNODEBeginList, CATEGORYNODE)) {
        CATEGORYNODEAllList.push(CATEGORYNODE);
        CATEGORYNODE.Index = CATEGORYNODEAllList.Count - 1;
    }
    if (idx == StringList.Count - 1) {
        CATEGORYNODE.IsEndNode = true;
        CATEGORYNODE.MDCATEGORY = MDCATEGORY;
    }
    else {
        idx++;
        GETCATEGORYNODE(CATEGORYNODEAllList, CATEGORYNODE.CATEGORYNODEList, MDCATEGORY, StringList, idx);
    }
    return CATEGORYNODE;
}

Persistence.Model.Methods.FillCATEGORYNODE = function (CATEGORYNODEAllList, CATEGORYNODEBeginList, MDCATEGORYList) {
    for (var i = 0; i < MDCATEGORYList.Count; i++) {
        var StringList = MDCATEGORYList[i].ToStringList();
        if (StringList.Count > 0) GETCATEGORYNODE(CATEGORYNODEAllList, CATEGORYNODEBeginList, MDCATEGORYList[i], StringList, 0);
    }
}

            /*
            Persistence.Model.Properties.TCATEGORYNODE CATEGORYNODE = null;
            Persistence.Model.Properties.TCATEGORYNODE CATEGORYNODEPrima = null;
            CATEGORYNODE =  Persistence.Model.Methods.CATEGORYNODE_ListSetID(CATEGORYNODEBeginList, "Servicio de Seguridad");            
            CATEGORYNODEPrima =  Persistence.Model.Methods.CATEGORYNODE_ListSetID(CATEGORYNODE.CATEGORYNODEList, "Desconocido");
            CATEGORYNODEPrima.IsEndNode = true;
            CATEGORYNODEPrima.MDCATEGORY = MDCATEGORYList[0];
            Persistence.Model.Methods.CATEGORYNODE_ListAdd(CATEGORYNODE.CATEGORYNODEList, CATEGORYNODEPrima);
             Persistence.Model.Methods.CATEGORYNODE_ListAddIndex(CATEGORYNODEAllList, CATEGORYNODEPrima);
             Persistence.Model.Methods.CATEGORYNODE_ListAddIndex(CATEGORYNODEAllList, CATEGORYNODEPrima);
             Persistence.Model.Methods.CATEGORYNODE_ListAdd(CATEGORYNODEBeginList, CATEGORYNODE);
             Persistence.Model.Methods.CATEGORYNODE_ListAddIndex(CATEGORYNODEAllList, CATEGORYNODE); //Mantine lista  de todos
            CATEGORYNODE =  Persistence.Model.Methods.CATEGORYNODE_ListSetID(CATEGORYNODEBeginList, "Servicio de Seguridad");  
             Persistence.Model.Methods.CATEGORYNODE_ListAdd(CATEGORYNODEBeginList, CATEGORYNODE);
             Persistence.Model.Methods.CATEGORYNODE_ListAddIndex(CATEGORYNODEAllList, CATEGORYNODE); //Mantine lista  de todos
            CATEGORYNODE =  Persistence.Model.Methods.CATEGORYNODE_ListSetID(CATEGORYNODEBeginList, "Servicio de Nomina"); 
             Persistence.Model.Methods.CATEGORYNODE_ListAdd(CATEGORYNODEBeginList, CATEGORYNODE);
             Persistence.Model.Methods.CATEGORYNODE_ListAddIndex(CATEGORYNODEAllList, CATEGORYNODE);  //Mantine lista  de todos
            CATEGORYNODE =  Persistence.Model.Methods.CATEGORYNODE_ListSetID(CATEGORYNODEBeginList, "Impresion");
             Persistence.Model.Methods.CATEGORYNODE_ListAdd(CATEGORYNODEBeginList, CATEGORYNODE);
             Persistence.Model.Methods.CATEGORYNODE_ListAddIndex(CATEGORYNODEAllList, CATEGORYNODE);  //Mantine lista  de todos
             */


Persistence.Model.Methods.CATEGORYNODE_ListSetID = function (CATEGORYNODEList, NODENAME) {
    for (var i = 0; i < CATEGORYNODEList.Count; i++) {
        if (NODENAME == CATEGORYNODEList[i].NODENAME)
            return (CATEGORYNODEList[i]);
    }
    var CATEGORYNODE = Persistence.Model.Properties.TCATEGORYNODE();
    CATEGORYNODE.NODENAME = NODENAME;
    return (CATEGORYNODE);
}

Persistence.Model.Methods.CATEGORYNODE_ListAdd = function (CATEGORYNODEList, CATEGORYNODE) {
    var i = Persistence.Model.Methods.CATEGORYNODE_ListGetIndex(CATEGORYNODEList, CATEGORYNODE);
    if (i == -1) {
        CATEGORYNODEList.push(CATEGORYNODE);
        return true;
    }
    else {
        CATEGORYNODEList[i].NODENAME = CATEGORYNODE.NODENAME;
    }
    return false;
}

/*Persistence.Model.Methods.CATEGORYNODE_ListAddIndex = funtion(CATEGORYNODEList, CATEGORYNODE)
{
    var i = Persistence.Model.Methods.CATEGORYNODE_ListGetIndex(CATEGORYNODEList, CATEGORYNODE);
    if (i == -1)
    {
        CATEGORYNODEList.push(CATEGORYNODE);
        CATEGORYNODE.Index = CATEGORYNODEList.Count-1;
    }
    else
    {
        CATEGORYNODEList[i].NODENAME = CATEGORYNODE.NODENAME;
    }
}*/

//CJRC_26072018
Persistence.Model.Methods.CATEGORYNODE_ListGetIndex = function (CATEGORYNODEList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.Model.Methods.CATEGORYNODE_ListGetIndexNODENAME(CATEGORYNODEList, Param);

    }
    else {
        Res = Persistence.Model.Methods.CATEGORYNODE_ListGetIndexCATEGORYNODE(CATEGORYNODEList, Param);
    }
    return (Res);
}

Persistence.Model.Methods.CATEGORYNODE_ListGetIndexCATEGORYNODE = function (CATEGORYNODEList, CATEGORYNODE) {
    for (var i = 0; i < CATEGORYNODEList.Count; i++) {
        if (CATEGORYNODE.NODENAME == CATEGORYNODEList[i].NODENAME)
            return (i);
    }
    return (-1);
}

Persistence.Model.Methods.CATEGORYNODE_ListGetIndexNODENAME = function (CATEGORYNODEList, NODENAME) {
    for (var i = 0; i < CATEGORYNODEList.Count; i++) {
        if (NODENAME == CATEGORYNODEList[i].NODENAME)
            return (i);
    }
    return (-1);
}

// this.CATEGORYNODEList = new Array();//Persistence.Model.Properties.TCATEGORYNODE 
