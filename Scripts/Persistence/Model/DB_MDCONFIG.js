﻿//TreduceDB
//Persistence.Model.Properties
//Persistence.Model.Methods

 
/*

class TMDCONFIGProfiler
{
public var ResErr = SysCfg.Error.Properties.TResErr();
        
public TMDCONFIGProfiler()
{
    Persistence.Model.Properties.TMDCONFIG MDCONFIG = new Persistence.Model.Properties.TMDCONFIG();
    MDCONFIG.IDMDCATEGORYDETAIL_DEFAULT = 0;
    MDCONFIG.IDMDCONFIG = 0;
    MDCONFIG.IDMDURGENCY_DEFAULT = 0;
}
}*/

//**********************   PROPERTIES   *****************************************************************************************

  

Persistence.Model.Properties.TMDCONFIG = function()
{
    this.IDMDCATEGORYDETAIL_DEFAULT = 0;//este guardas
    this.MDCATEGORYDETAIL_DEFAULT = new Persistence.Model.Properties.TMDCATEGORYDETAIL();//Persistence.Profiler.ModelProfiler.MDCATEGORYList
    this.IDMDCONFIG = 0;
    this.IDMDURGENCY_DEFAULT  = 0;//Guardas
    this.MDURGENCY_DEFAULT = new Persistence.Catalog.Properties.TMDURGENCY();//Persistence.Profiler.CatalogProfiler.MDURGENCYList

    //Socket IO Properties
    this.ToByte = function(MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream,  this.IDMDCATEGORYDETAIL_DEFAULT);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream,  this.IDMDCONFIG);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream,  this.IDMDURGENCY_DEFAULT);
    }
    this.ByteTo = function(MemStream)
    {
        this.IDMDCATEGORYDETAIL_DEFAULT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDCONFIG = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDURGENCY_DEFAULT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    }
    //Str IO Properties
    this.ToStr = function(Longitud, Texto)
    {
        SysCfg.Str.Protocol.WriteInt( this.IDMDCATEGORYDETAIL_DEFAULT, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt( this.IDMDCONFIG, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt( this.IDMDURGENCY_DEFAULT, Longitud, Texto);
    }
    this.StrTo = function(Pos, Index, LongitudArray, Texto)
    {
        this.IDMDCATEGORYDETAIL_DEFAULT = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDCONFIG = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDURGENCY_DEFAULT = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
    }

    this.TMDCONFIG = function()
    {
        this.IDMDCONFIG = -1;
        this.IDMDCATEGORYDETAIL_DEFAULT = -1;
        this.IDMDURGENCY_DEFAULT = -1;
    }

}
        
  
//**********************   METHODS server  ********************************************************************************************

//DataSet Function 
Persistence.Model.Methods.MDCONFIG_Fill= function(MDCONFIGList,IDMDCONFIG)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    MDCONFIGList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    Param.AddInt32(UsrCfg.InternoAtisNames.MDCONFIG.IDMDCONFIG.FieldName, IDMDCONFIG, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try
    {
        var DS_MDCONFIG = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDCONFIG_GET", Param.ToBytes());
        ResErr = DS_MDCONFIG.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDCONFIG.DataSet.RecordCount > 0)
            {
                DS_MDCONFIG.DataSet.First();
                var MDCONFIG = new Persistence.Model.Properties.TMDCONFIG();
                if (!DS_MDCONFIG.DataSet.Eof)
                {                            
                    MDCONFIG.IDMDCONFIG = DS_MDCONFIG.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCONFIG.IDMDCONFIG.FieldName).asInt32();
                    //Other
                    MDCONFIG.IDMDCATEGORYDETAIL_DEFAULT = DS_MDCONFIG.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCONFIG.IDMDCATEGORYDETAIL_DEFAULT.FieldName).asInt32();
                    MDCONFIG.IDMDURGENCY_DEFAULT = DS_MDCONFIG.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCONFIG.IDMDURGENCY_DEFAULT.FieldName).asInt32();
                            
                    DS_MDCONFIG.DataSet.Next();
                }
                MDCONFIGList.push(MDCONFIG);
            }
            else
            {
                DS_MDCONFIG.ResErr.NotError = false;
                DS_MDCONFIG.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
        
Persistence.Model.Methods.MDCONFIG_ADD= function( MDCONFIG)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCONFIG.IDMDCONFIG.FieldName, MDCONFIG.IDMDCONFIG, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCONFIG.IDMDCATEGORYDETAIL_DEFAULT.FieldName, MDCONFIG.IDMDCATEGORYDETAIL_DEFAULT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCONFIG.IDMDURGENCY_DEFAULT.FieldName, MDCONFIG.IDMDURGENCY_DEFAULT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDCONFIG_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDCONFIG_UPD= function(MDCONFIG)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCONFIG.IDMDCONFIG.FieldName, MDCONFIG.IDMDCONFIG, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCONFIG.IDMDCATEGORYDETAIL_DEFAULT.FieldName, MDCONFIG.IDMDCATEGORYDETAIL_DEFAULT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCONFIG.IDMDURGENCY_DEFAULT.FieldName, MDCONFIG.IDMDURGENCY_DEFAULT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDCONFIG_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            //GET(); 
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDCONFIG_DEL= function(/*StrIDMDCONFIG*/IDMDCONFIG)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCONFIG.IDMDCONFIG.FieldName, IDMDCONFIG, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);



        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDCONFIG_DEL", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDCONFIG_DEL= function(MDCONFIG)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCONFIG.IDMDCONFIG.FieldName, MDCONFIG.IDMDCONFIG, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDCONFIG_DEL", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}

     
//**********************   METHODS   ********************************************************************************************

     
//DataSet Function 

//List Function 
Persistence.Model.Methods.MDCONFIG_ListSetID=function(MDCONFIGList, IDMDCONFIG)
{
    for (i = 0; i < MDCONFIGList.length; i++)
    {

        if (IDMDCONFIG == MDCONFIGList[i].IDMDCONFIG)
            return (MDCONFIGList[i]);

    }
     
    var TMDCONFIG = new Persistence.Properties.TMDCONFIG();
    TMDCONFIG.IDMDCONFIG = IDMDCONFIG; 
    return (TMDCONFIG);
}

Persistence.Model.Methods.MDCONFIG_ListAdd=function(MDCONFIGList,MDCONFIG)
{
    var i = Persistence.Model.Methods.MDCONFIG_ListGetIndex(MDCONFIGList, MDCONFIG);
    if (i == -1) MDCONFIGList.push(MDCONFIG);
    else
    {
        MDCONFIGList[i].IDMDCATEGORYDETAIL_DEFAULT = MDCONFIG.IDMDCATEGORYDETAIL_DEFAULT;
        MDCONFIGList[i].IDMDCONFIG = MDCONFIG.IDMDCONFIG;
        MDCONFIGList[i].IDMDURGENCY_DEFAULT = MDCONFIG.IDMDURGENCY_DEFAULT;
    }
}
Persistence.Model.Methods.MDCONFIG_ListGetIndex=function(MDCONFIGList, MDCONFIG)
{
    for (i = 0; i < MDCONFIGList.length; i++)
    {
        if (MDCONFIG.IDMDCONFIG == MDCONFIGList[i].IDMDCONFIG)
            return (i);
    }
    return (-1);
}
Persistence.Model.Methods.MDCONFIG_ListGetIndex=function(MDCONFIGList, IDMDCONFIG)
{
    for (i = 0; i < MDCONFIGList.length; i++)
    {
        if (IDMDCONFIG == MDCONFIGList[i].IDMDCONFIG)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.Model.Methods.MDCONFIGListtoStr=function(MDCONFIGList)
{
    var Res = Persistence.Model.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(MDCONFIGList.length, Longitud, Texto);
        for (Counter = 0; Counter < MDCONFIGList.length; Counter++) {
            var UnMDCONFIG = MDCONFIGList[Counter];
            UnMDCONFIG.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Model.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.Model.Methods.StrtoMDCONFIG=function(ProtocoloStr, MDCONFIGList)
{
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        MDCONFIGList.length = 0;
        if (Persistence.Model.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnMDCONFIG = new Persistence.Demo.Properties.TMDCONFIG(); //Mode new row
                UnMDCONFIG.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.MDCONFIG_ListAdd(MDCONFIGList, UnMDCONFIG);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
    
}

//Socket Function 
Persistence.Model.Methods.MDCONFIGListToByte=function(MDCONFIGList, MemStream, idx)
{
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, MDCONFIGList.length - idx);
    for (i = idx; i < MDCONFIGList.length; i++)
    {
        MDCONFIGList[i].ToByte(MemStream);
    }
}
Persistence.Model.Methods.ByteToMDCONFIGList=function(MDCONFIGList, MemStream)
{
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++)
    {
        var UnMDCONFIG = new Persistence.Model.Properties.TMDCONFIG();
        UnMDCONFIG.ByteTo(MemStream);
        Persistence.Model.Methods.MDCONFIG_ListAdd(MDCONFIGList, UnMDCONFIG);
    }
}
    

//*******************************************************************************************************************************
 
