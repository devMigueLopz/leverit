﻿//TreduceDB
//Persistence.Model.Properties
//Persistence.Model.Methods
        
Persistence.Model.Properties.TMDMATRIXMODELS = function()
{
    this.COMMENTSL = "";
    this.GUIDET = "";
    this.IDMDLIFESTATUS  = 0;
    this.IDMDMATRIXMODELS  = 0;
    this.IDMDMODELTYPED1  = 0;
    this.IDMDMODELTYPED2  = "";
    this.MODELVALIDATE  = false;
    this.POSINSTEP  = 0;
    //Socket IO Properties
    this.ToByte = function(MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.COMMENTSL);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.GUIDET);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDLIFESTATUS);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDMATRIXMODELS);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDMODELTYPED1);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.IDMDMODELTYPED2);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.MODELVALIDATE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.POSINSTEP);
    }
    this.ByteTo = function(MemStream)
    {
        this.COMMENTSL = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.GUIDET = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDMDLIFESTATUS = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDMATRIXMODELS = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDMODELTYPED1 = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDMODELTYPED2 = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.MODELVALIDATE = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.POSINSTEP = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    }
    //Str IO Properties
    this.ToStr = function( Longitud,  Texto)
    {
        SysCfg.Str.Protocol.WriteStr(this.COMMENTSL, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.GUIDET, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDLIFESTATUS, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDMATRIXMODELS, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDMODELTYPED1, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.IDMDMODELTYPED2, Longitud, Texto);
        SysCfg.Str.Protocol.WriteBool(this.MODELVALIDATE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.POSINSTEP, Longitud, Texto);
    }
    this.StrTo = function(Pos, Index, LongitudArray, Texto)
    {
        this.COMMENTSL = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.GUIDET = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.IDMDLIFESTATUS = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDMATRIXMODELS = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDMODELTYPED1 = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDMODELTYPED2 = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.MODELVALIDATE = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);
        this.POSINSTEP = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
    }
}
   

//**********************   METHODS server  ********************************************************************************************

    
//DataSet Function 
Persistence.Model.Methods.MDMATRIXMODELS_Fill = function(MDMATRIXMODELSList, StrIDMDMATRIXMODELS/* IDMDMATRIXMODELS*/)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    MDMATRIXMODELSList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDMDMATRIXMODELS == "")
    {
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDMATRIXMODELS.FieldName, " = " + UsrCfg.InternoAtisNames.MDMATRIXMODELS.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDMATRIXMODELS.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else
    {
        StrIDMDMATRIXMODELS = " IN (" + StrIDMDMATRIXMODELS + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDMATRIXMODELS.FieldName, StrIDMDMATRIXMODELS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //Param.AddInt32(UsrCfg.InternoDataLinkNames.MDMATRIXMODELS.IDMDMATRIXMODELS.FieldName, IDMDMATRIXMODELS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try
    {
        /*

    //********   MDMATRIXMODELS_GET   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDMATRIXMODELS_GET", "MDMATRIXMODELS_GET description.");
    UnSQL.SQL.Add(" SELECT  ");
    UnSQL.SQL.Add("  MDMATRIXMODELS.COMMENTSL, ");
    UnSQL.SQL.Add("  MDMATRIXMODELS.GUIDET, ");
    UnSQL.SQL.Add("  MDMATRIXMODELS.IDMDLIFESTATUS, ");
    UnSQL.SQL.Add("  MDMATRIXMODELS.IDMDMATRIXMODELS, ");
    UnSQL.SQL.Add("  MDMATRIXMODELS.IDMDMODELTYPED1, ");
    UnSQL.SQL.Add("  MDMATRIXMODELS.IDMDMODELTYPED2, ");
    UnSQL.SQL.Add("  MDMATRIXMODELS.MODELVALIDATE, ");
    UnSQL.SQL.Add("  MDMATRIXMODELS.POSINSTEP ");
    UnSQL.SQL.Add(" FROM  MDMATRIXMODELS ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  MDMATRIXMODELS.COMMENTSL=[COMMENTSL] and  ");
    UnSQL.SQL.Add("  MDMATRIXMODELS.GUIDET=[GUIDET] and  ");
    UnSQL.SQL.Add("  MDMATRIXMODELS.IDMDLIFESTATUS=[IDMDLIFESTATUS] and  ");
    UnSQL.SQL.Add("  MDMATRIXMODELS.IDMDMATRIXMODELS=[IDMDMATRIXMODELS] and  ");
    UnSQL.SQL.Add("  MDMATRIXMODELS.IDMDMODELTYPED1=[IDMDMODELTYPED1] and  ");
    UnSQL.SQL.Add("  MDMATRIXMODELS.IDMDMODELTYPED2=[IDMDMODELTYPED2] and  ");
    UnSQL.SQL.Add("  MDMATRIXMODELS.MODELVALIDATE=[MODELVALIDATE] and  ");
    UnSQL.SQL.Add("  MDMATRIXMODELS.POSINSTEP=[POSINSTEP] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_MDMATRIXMODELS = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDMATRIXMODELS_GET", Param.ToBytes());
        ResErr = DS_MDMATRIXMODELS.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDMATRIXMODELS.DataSet.RecordCount > 0)
            {
                DS_MDMATRIXMODELS.DataSet.First();
                while (!(DS_MDMATRIXMODELS.DataSet.Eof))
                {
                    var MDMATRIXMODELS = new Persistence.Model.Properties.TMDMATRIXMODELS();
                    MDMATRIXMODELS.IDMDMATRIXMODELS = DS_MDMATRIXMODELS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDMATRIXMODELS.FieldName).asInt32();
                    //Other
                    MDMATRIXMODELS.IDMDLIFESTATUS = DS_MDMATRIXMODELS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDLIFESTATUS.FieldName).asInt32();
                    MDMATRIXMODELS.IDMDMODELTYPED1 = DS_MDMATRIXMODELS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDMODELTYPED1.FieldName).asInt32();
                    MDMATRIXMODELS.IDMDMODELTYPED2 = DS_MDMATRIXMODELS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDMODELTYPED2.FieldName).asString();
                    MDMATRIXMODELS.MODELVALIDATE = DS_MDMATRIXMODELS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDMATRIXMODELS.MODELVALIDATE.FieldName).asBoolean();
                    MDMATRIXMODELS.POSINSTEP = DS_MDMATRIXMODELS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDMATRIXMODELS.POSINSTEP.FieldName).asInt32();
                    MDMATRIXMODELSList.push(MDMATRIXMODELS);

                    DS_MDMATRIXMODELS.DataSet.Next();
                }
            }
            else
            {
                DS_MDMATRIXMODELS.ResErr.NotError = false;
                DS_MDMATRIXMODELS.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDMATRIXMODELS_GETID=function(MDMATRIXMODELS)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDMATRIXMODELS.FieldName, MDMATRIXMODELS.IDMDMATRIXMODELS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDMATRIXMODELS.COMMENTSL.FieldName, MDMATRIXMODELS.COMMENTSL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.MDMATRIXMODELS.GUIDET.FieldName, MDMATRIXMODELS.GUIDET, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDLIFESTATUS.FieldName, MDMATRIXMODELS.IDMDLIFESTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDMODELTYPED1.FieldName, MDMATRIXMODELS.IDMDMODELTYPED1, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDMODELTYPED2.FieldName, MDMATRIXMODELS.IDMDMODELTYPED2, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.MDMATRIXMODELS.MODELVALIDATE.FieldName, MDMATRIXMODELS.MODELVALIDATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMATRIXMODELS.POSINSTEP.FieldName, MDMATRIXMODELS.POSINSTEP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDMATRIXMODELS_GETID   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDMATRIXMODELS_GETID", "MDMATRIXMODELS_GETID description.");
    UnSQL.SQL.Add(" SELECT  ");
    UnSQL.SQL.Add("  MDMATRIXMODELS.COMMENTSL, ");
    UnSQL.SQL.Add("  MDMATRIXMODELS.GUIDET, ");
    UnSQL.SQL.Add("  MDMATRIXMODELS.IDMDLIFESTATUS, ");
    UnSQL.SQL.Add("  MDMATRIXMODELS.IDMDMATRIXMODELS, ");
    UnSQL.SQL.Add("  MDMATRIXMODELS.IDMDMODELTYPED1, ");
    UnSQL.SQL.Add("  MDMATRIXMODELS.IDMDMODELTYPED2, ");
    UnSQL.SQL.Add("  MDMATRIXMODELS.MODELVALIDATE, ");
    UnSQL.SQL.Add("  MDMATRIXMODELS.POSINSTEP ");
    UnSQL.SQL.Add(" FROM  MDMATRIXMODELS ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  MDMATRIXMODELS.COMMENTSL=[COMMENTSL] and  ");
    UnSQL.SQL.Add("  MDMATRIXMODELS.GUIDET=[GUIDET] and  ");
    UnSQL.SQL.Add("  MDMATRIXMODELS.IDMDLIFESTATUS=[IDMDLIFESTATUS] and  ");
    UnSQL.SQL.Add("  MDMATRIXMODELS.IDMDMATRIXMODELS=[IDMDMATRIXMODELS] and  ");
    UnSQL.SQL.Add("  MDMATRIXMODELS.IDMDMODELTYPED1=[IDMDMODELTYPED1] and  ");
    UnSQL.SQL.Add("  MDMATRIXMODELS.IDMDMODELTYPED2=[IDMDMODELTYPED2] and  ");
    UnSQL.SQL.Add("  MDMATRIXMODELS.MODELVALIDATE=[MODELVALIDATE] and  ");
    UnSQL.SQL.Add("  MDMATRIXMODELS.POSINSTEP=[POSINSTEP] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_MDMATRIXMODELS = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDMATRIXMODELS_GETID", Param.ToBytes());
        ResErr = DS_MDMATRIXMODELS.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDMATRIXMODELS.DataSet.RecordCount > 0)
            {
                DS_MDMATRIXMODELS.DataSet.First();
                MDMATRIXMODELS.IDMDMATRIXMODELS = DS_MDMATRIXMODELS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDMATRIXMODELS.FieldName).asInt32();
                //Other
                MDMATRIXMODELS.IDMDLIFESTATUS = DS_MDMATRIXMODELS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDLIFESTATUS.FieldName).asInt32();
                MDMATRIXMODELS.IDMDMODELTYPED1 = DS_MDMATRIXMODELS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDMODELTYPED1.FieldName).asInt32();
                MDMATRIXMODELS.IDMDMODELTYPED2 = DS_MDMATRIXMODELS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDMODELTYPED2.FieldName).asString();
                MDMATRIXMODELS.MODELVALIDATE = DS_MDMATRIXMODELS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDMATRIXMODELS.MODELVALIDATE.FieldName).asBoolean();
                MDMATRIXMODELS.POSINSTEP = DS_MDMATRIXMODELS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDMATRIXMODELS.POSINSTEP.FieldName).asInt32();
            }
            else
            {
                DS_MDMATRIXMODELS.ResErr.NotError = false;
                DS_MDMATRIXMODELS.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDMATRIXMODELS_ADD=function( MDMATRIXMODELS)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDMATRIXMODELS.FieldName, MDMATRIXMODELS.IDMDMATRIXMODELS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDMATRIXMODELS.COMMENTSL.FieldName, MDMATRIXMODELS.COMMENTSL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.MDMATRIXMODELS.GUIDET.FieldName, MDMATRIXMODELS.GUIDET, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDLIFESTATUS.FieldName, MDMATRIXMODELS.IDMDLIFESTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDMODELTYPED1.FieldName, MDMATRIXMODELS.IDMDMODELTYPED1, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDMODELTYPED2.FieldName, MDMATRIXMODELS.IDMDMODELTYPED2, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.MDMATRIXMODELS.MODELVALIDATE.FieldName, MDMATRIXMODELS.MODELVALIDATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMATRIXMODELS.POSINSTEP.FieldName, MDMATRIXMODELS.POSINSTEP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDMATRIXMODELS_ADD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDMATRIXMODELS_ADD", "MDMATRIXMODELS_ADD description.");
    UnSQL.SQL.Add(" INSERT INTO MDMATRIXMODELS( ");
    UnSQL.SQL.Add("  COMMENTSL,  ");
    UnSQL.SQL.Add("  GUIDET,  ");
    UnSQL.SQL.Add("  IDMDLIFESTATUS,  ");
    UnSQL.SQL.Add("  IDMDMATRIXMODELS,  ");
    UnSQL.SQL.Add("  IDMDMODELTYPED1,  ");
    UnSQL.SQL.Add("  IDMDMODELTYPED2,  ");
    UnSQL.SQL.Add("  MODELVALIDATE,  ");
    UnSQL.SQL.Add("  POSINSTEP ");
    UnSQL.SQL.Add(" )  ");
    UnSQL.SQL.Add(" VALUES ( ");
    UnSQL.SQL.Add("  [COMMENTSL], ");
    UnSQL.SQL.Add("  [GUIDET], ");
    UnSQL.SQL.Add("  [IDMDLIFESTATUS], ");
    UnSQL.SQL.Add("  [IDMDMATRIXMODELS], ");
    UnSQL.SQL.Add("  [IDMDMODELTYPED1], ");
    UnSQL.SQL.Add("  [IDMDMODELTYPED2], ");
    UnSQL.SQL.Add("  [MODELVALIDATE], ");
    UnSQL.SQL.Add("  [POSINSTEP] ");
    UnSQL.SQL.Add(" ) ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDMATRIXMODELS_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            MDMATRIXMODELS_GETID(MDMATRIXMODELS);
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDMATRIXMODELS_UPD=function(MDMATRIXMODELS)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDMATRIXMODELS.FieldName, MDMATRIXMODELS.IDMDMATRIXMODELS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDMATRIXMODELS.COMMENTSL.FieldName, MDMATRIXMODELS.COMMENTSL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.MDMATRIXMODELS.GUIDET.FieldName, MDMATRIXMODELS.GUIDET, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDLIFESTATUS.FieldName, MDMATRIXMODELS.IDMDLIFESTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDMODELTYPED1.FieldName, MDMATRIXMODELS.IDMDMODELTYPED1, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDMODELTYPED2.FieldName, MDMATRIXMODELS.IDMDMODELTYPED2, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.MDMATRIXMODELS.MODELVALIDATE.FieldName, MDMATRIXMODELS.MODELVALIDATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMATRIXMODELS.POSINSTEP.FieldName, MDMATRIXMODELS.POSINSTEP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDMATRIXMODELS_UPD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDMATRIXMODELS_UPD", "MDMATRIXMODELS_UPD description.");
    UnSQL.SQL.Add(" UPDATE MDMATRIXMODELS SET ");
    UnSQL.SQL.Add("  COMMENTSL=[COMMENTSL],  ");
    UnSQL.SQL.Add("  GUIDET=[GUIDET],  ");
    UnSQL.SQL.Add("  IDMDLIFESTATUS=[IDMDLIFESTATUS],  ");
    UnSQL.SQL.Add("  IDMDMATRIXMODELS=[IDMDMATRIXMODELS],  ");
    UnSQL.SQL.Add("  IDMDMODELTYPED1=[IDMDMODELTYPED1],  ");
    UnSQL.SQL.Add("  IDMDMODELTYPED2=[IDMDMODELTYPED2],  ");
    UnSQL.SQL.Add("  MODELVALIDATE=[MODELVALIDATE],  ");
    UnSQL.SQL.Add("  POSINSTEP=[POSINSTEP] ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  COMMENTSL=[COMMENTSL] AND  ");
    UnSQL.SQL.Add("  GUIDET=[GUIDET] AND  ");
    UnSQL.SQL.Add("  IDMDLIFESTATUS=[IDMDLIFESTATUS] AND  ");
    UnSQL.SQL.Add("  IDMDMATRIXMODELS=[IDMDMATRIXMODELS] AND  ");
    UnSQL.SQL.Add("  IDMDMODELTYPED1=[IDMDMODELTYPED1] AND  ");
    UnSQL.SQL.Add("  IDMDMODELTYPED2=[IDMDMODELTYPED2] AND  ");
    UnSQL.SQL.Add("  MODELVALIDATE=[MODELVALIDATE] AND  ");
    UnSQL.SQL.Add("  POSINSTEP=[POSINSTEP] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDMATRIXMODELS_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            //GET(); 
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDMATRIXMODELS_DEL=function(/* StrIDMDMATRIXMODELS*/ IDMDMATRIXMODELS)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        /*if (StrIDMDMATRIXMODELS == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDMATRIXMODELS.FieldName, " = " + UsrCfg.InternoAtisNames.MDMATRIXMODELS.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDMATRIXMODELS.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDMDMATRIXMODELS = " IN (" + StrIDMDMATRIXMODELS + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDMATRIXMODELS.FieldName, StrIDMDMATRIXMODELS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDMATRIXMODELS.FieldName, IDMDMATRIXMODELS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDMATRIXMODELS_DEL_1   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDMATRIXMODELS_DEL_1", "MDMATRIXMODELS_DEL_1 description.");
    UnSQL.SQL.Add(" DELETE MDMATRIXMODELS  ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  COMMENTSL=[COMMENTSL] AND  ");
    UnSQL.SQL.Add("  GUIDET=[GUIDET] AND  ");
    UnSQL.SQL.Add("  IDMDLIFESTATUS=[IDMDLIFESTATUS] AND  ");
    UnSQL.SQL.Add("  IDMDMATRIXMODELS=[IDMDMATRIXMODELS] AND  ");
    UnSQL.SQL.Add("  IDMDMODELTYPED1=[IDMDMODELTYPED1] AND  ");
    UnSQL.SQL.Add("  IDMDMODELTYPED2=[IDMDMODELTYPED2] AND  ");
    UnSQL.SQL.Add("  MODELVALIDATE=[MODELVALIDATE] AND  ");
    UnSQL.SQL.Add("  POSINSTEP=[POSINSTEP] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDMATRIXMODELS_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDMATRIXMODELS_DEL=function(MDMATRIXMODELS)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDMATRIXMODELS.FieldName, MDMATRIXMODELS.IDMDMATRIXMODELS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDMATRIXMODELS.COMMENTSL.FieldName, MDMATRIXMODELS.COMMENTSL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.MDMATRIXMODELS.GUIDET.FieldName, MDMATRIXMODELS.GUIDET, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDLIFESTATUS.FieldName, MDMATRIXMODELS.IDMDLIFESTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDMODELTYPED1.FieldName, MDMATRIXMODELS.IDMDMODELTYPED1, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDMODELTYPED2.FieldName, MDMATRIXMODELS.IDMDMODELTYPED2, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.MDMATRIXMODELS.MODELVALIDATE.FieldName, MDMATRIXMODELS.MODELVALIDATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMATRIXMODELS.POSINSTEP.FieldName, MDMATRIXMODELS.POSINSTEP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDMATRIXMODELS_DEL_2   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDMATRIXMODELS_DEL_2", "MDMATRIXMODELS_DEL_2 description.");
    UnSQL.SQL.Add(" DELETE MDMATRIXMODELS  ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  COMMENTSL=[COMMENTSL] AND  ");
    UnSQL.SQL.Add("  GUIDET=[GUIDET] AND  ");
    UnSQL.SQL.Add("  IDMDLIFESTATUS=[IDMDLIFESTATUS] AND  ");
    UnSQL.SQL.Add("  IDMDMATRIXMODELS=[IDMDMATRIXMODELS] AND  ");
    UnSQL.SQL.Add("  IDMDMODELTYPED1=[IDMDMODELTYPED1] AND  ");
    UnSQL.SQL.Add("  IDMDMODELTYPED2=[IDMDMODELTYPED2] AND  ");
    UnSQL.SQL.Add("  MODELVALIDATE=[MODELVALIDATE] AND  ");
    UnSQL.SQL.Add("  POSINSTEP=[POSINSTEP] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDMATRIXMODELS_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}

    

//**********************   METHODS   ********************************************************************************************

    
//DataSet Function 

//List Function 
Persistence.Model.Methods.MDMATRIXMODELS_ListSetID=function(MDMATRIXMODELSList, IDMDMATRIXMODELS)
{
    for (i = 0; i < MDMATRIXMODELSList.length; i++)
    {

        if (IDMDMATRIXMODELS == MDMATRIXMODELSList[i].IDMDMATRIXMODELS)
            return (MDMATRIXMODELSList[i]);

    }
    

    var TMDMATRIXMODELS = new Persistence.Properties.TMDMATRIXMODELS();
    TMDMATRIXMODELS.IDMDMATRIXMODELS = IDMDMATRIXMODELS; 
    return (TMDMATRIXMODELS); 
}
Persistence.Model.Methods.MDMATRIXMODELS_ListAdd=function(MDMATRIXMODELSList, MDMATRIXMODELS)
{
    var i = Persistence.Model.Methods.MDMATRIXMODELS_ListGetIndex(MDMATRIXMODELSList, MDMATRIXMODELS);
    if (i == -1) MDMATRIXMODELSList.push(MDMATRIXMODELS);
    else
    {
        MDMATRIXMODELSList[i].COMMENTSL = MDMATRIXMODELS.COMMENTSL;
        MDMATRIXMODELSList[i].GUIDET = MDMATRIXMODELS.GUIDET;
        MDMATRIXMODELSList[i].IDMDLIFESTATUS = MDMATRIXMODELS.IDMDLIFESTATUS;
        MDMATRIXMODELSList[i].IDMDMATRIXMODELS = MDMATRIXMODELS.IDMDMATRIXMODELS;
        MDMATRIXMODELSList[i].IDMDMODELTYPED1 = MDMATRIXMODELS.IDMDMODELTYPED1;
        MDMATRIXMODELSList[i].IDMDMODELTYPED2 = MDMATRIXMODELS.IDMDMODELTYPED2;
        MDMATRIXMODELSList[i].MODELVALIDATE = MDMATRIXMODELS.MODELVALIDATE;
        MDMATRIXMODELSList[i].POSINSTEP = MDMATRIXMODELS.POSINSTEP;
    }
}
Persistence.Model.Methods.MDMATRIXMODELS_ListGetIndex=function(MDMATRIXMODELSList, MDMATRIXMODELS)
{
    for (i = 0; i < MDMATRIXMODELSList.length; i++)
    {
        if (MDMATRIXMODELS.IDMDMATRIXMODELS == MDMATRIXMODELSList[i].IDMDMATRIXMODELS)
            return (i);
    }
    return (-1);
}
Persistence.Model.Methods.MDMATRIXMODELS_ListGetIndex=function(MDMATRIXMODELSList, IDMDMATRIXMODELS)
{
    for (i = 0; i < MDMATRIXMODELSList.length; i++)
    {
        if (IDMDMATRIXMODELS == MDMATRIXMODELSList[i].IDMDMATRIXMODELS)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.Model.Methods.MDMATRIXMODELSListtoStr=function(MDMATRIXMODELSList)
{
    var Res = Persistence.Model.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(MDMATRIXMODELSList.length, Longitud, Texto);
        for (Counter = 0; Counter < MDMATRIXMODELSList.length; Counter++) {
            var UnMDMATRIXMODELS = MDMATRIXMODELSList[Counter];
            UnMDMATRIXMODELS.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Model.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.Model.Methods.StrtoMDMATRIXMODELS=function( ProtocoloStr, MDMATRIXMODELSList)
{
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        MDMATRIXMODELSList.length = 0;
        if (Persistence.Model.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnMDMATRIXMODELS = new Persistence.Demo.Properties.TMDMATRIXMODELS(); //Mode new row
                UnMDMATRIXMODELS.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.MDMATRIXMODELS_ListAdd(MDMATRIXMODELSList, UnMDMATRIXMODELS);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
    
}

//Socket Function 
Persistence.Model.Methods.MDMATRIXMODELSListToByte=function(MDMATRIXMODELSList,  MemStream,  idx)
{
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, MDMATRIXMODELSList.length - idx);
    for (i = idx; i < MDMATRIXMODELSList.length; i++)
    {
        MDMATRIXMODELSList[i].ToByte(MemStream);
    }
}
Persistence.Model.Methods.ByteToMDMATRIXMODELSList=function(MDMATRIXMODELSList, MemStream)
{
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++)
    {
        var UnMDMATRIXMODELS = new Persistence.Model.Properties.TMDMATRIXMODELS();
        UnMDMATRIXMODELS.ByteTo(MemStream);
        Persistence.Model.Methods.MDMATRIXMODELS_ListAdd(MDMATRIXMODELSList, UnMDMATRIXMODELS);
    }
}
     
