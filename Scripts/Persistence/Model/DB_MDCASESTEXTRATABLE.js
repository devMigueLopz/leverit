//TreduceDB
//Persistence.Model.Properties
//Persistence.Model.Methods
 
     
Persistence.Model.PropertiesTMDCASESTEXTRATABLE= function() 
{
    this.IDMDCASESTEXTRATABLE = 0;
    this.IDMDINTERFACE = 0;
    this.IDMDMODELTYPED = 0;
    this.IDMDSERVICEEXTRATABLE= 0;
    this.IDMDSERVICETYPE= 0;
    this.LSCHANGETABLE_COMMENTS= "";
    //Socket IO Properties
    this.ToByte = function(MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDCASESTEXTRATABLE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDINTERFACE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDMODELTYPED);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDSERVICEEXTRATABLE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDSERVICETYPE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.LSCHANGETABLE_COMMENTS);
    }
    this.ByteTo = function(MemStream)
    {
        this.IDMDCASESTEXTRATABLE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDINTERFACE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDMODELTYPED = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDSERVICEEXTRATABLE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDSERVICETYPE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.LSCHANGETABLE_COMMENTS = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    }
    //Str IO Properties
    this.ToStr = function(Longitud, Texto)
    {
        SysCfg.Str.Protocol.WriteInt(this.IDMDCASESTEXTRATABLE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDINTERFACE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDMODELTYPED, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDSERVICEEXTRATABLE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDSERVICETYPE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.LSCHANGETABLE_COMMENTS, Longitud, Texto);
    }
    this.StrTo = function(Pos, Index, LongitudArray, Texto)
    {
        this.IDMDCASESTEXTRATABLE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDINTERFACE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDMODELTYPED = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDSERVICEEXTRATABLE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDSERVICETYPE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.LSCHANGETABLE_COMMENTS = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
    }
}
   

//**********************   METHODS server  ********************************************************************************************
    
//DataSet Function 
Persistence.Model.Methods.MDCASESTEXTRATABLE_Fill = function (MDCASESTEXTRATABLEList, IDMDMODELTYPED)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    MDCASESTEXTRATABLEList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    Param.AddInt32(UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDMODELTYPED.FieldName, IDMDMODELTYPED, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try
    {
        var DS_MDCASESTEXTRATABLE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDCASESTEXTRATABLE_GET", Param.ToBytes());
        ResErr = DS_MDCASESTEXTRATABLE.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDCASESTEXTRATABLE.DataSet.RecordCount > 0)
            {
                DS_MDCASESTEXTRATABLE.DataSet.First();
                while (!(DS_MDCASESTEXTRATABLE.DataSet.Eof))
                {
                    var MDCASESTEXTRATABLE = new Persistence.Model.Properties.TMDCASESTEXTRATABLE();
                    MDCASESTEXTRATABLE.IDMDCASESTEXTRATABLE = DS_MDCASESTEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDCASESTEXTRATABLE.FieldName).asInt32();
                    //Other
                    MDCASESTEXTRATABLE.IDMDINTERFACE = DS_MDCASESTEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDINTERFACE.FieldName).asInt32();
                    MDCASESTEXTRATABLE.IDMDMODELTYPED = DS_MDCASESTEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDMODELTYPED.FieldName).asInt32();
                    MDCASESTEXTRATABLE.IDMDSERVICEEXTRATABLE = DS_MDCASESTEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDSERVICEEXTRATABLE.FieldName).asInt32();
                    MDCASESTEXTRATABLE.IDMDSERVICETYPE = DS_MDCASESTEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDSERVICETYPE.FieldName).asInt32();
                    MDCASESTEXTRATABLEList.push(MDCASESTEXTRATABLE);

                    DS_MDCASESTEXTRATABLE.DataSet.Next();
                }
            }
            else
            {
                DS_MDCASESTEXTRATABLE.ResErr.NotError = false;
                DS_MDCASESTEXTRATABLE.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDCASESTEXTRATABLE_GETID = function (MDCASESTEXTRATABLE, IDMDCASESTEXTRATABLE)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDCASESTEXTRATABLE.FieldName, IDMDCASESTEXTRATABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var DS_MDCASESTEXTRATABLE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDCASESTEXTRATABLE_GETID", Param.ToBytes());
        ResErr = DS_MDCASESTEXTRATABLE.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDCASESTEXTRATABLE.DataSet.RecordCount > 0)
            {
                DS_MDCASESTEXTRATABLE.DataSet.First();
                MDCASESTEXTRATABLE.IDMDMODELTYPED = DS_MDCASESTEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDMODELTYPED.FieldName).asInt32();
                MDCASESTEXTRATABLE.IDMDSERVICETYPE = DS_MDCASESTEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDSERVICETYPE.FieldName).asInt32();
                MDCASESTEXTRATABLE.IDMDINTERFACE = DS_MDCASESTEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDINTERFACE.FieldName).asInt32();
                MDCASESTEXTRATABLE.IDMDSERVICEEXTRATABLE = DS_MDCASESTEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDSERVICEEXTRATABLE.FieldName).asInt32();
                MDCASESTEXTRATABLE.LSCHANGETABLE_COMMENTS = DS_MDCASESTEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.LSCHANGETABLE_COMMENTS.FieldName).asString();
            }
            else
            {
                DS_MDCASESTEXTRATABLE.ResErr.NotError = false;
                DS_MDCASESTEXTRATABLE.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDCASESTEXTRATABLE_ADD = function (MDCASESTEXTRATABLE)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        //Param.AddInt32(UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDCASESTEXTRATABLE.FieldName, MDCASESTEXTRATABLE.IDMDCASESTEXTRATABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDMODELTYPED.FieldName, MDCASESTEXTRATABLE.IDMDMODELTYPED, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDSERVICETYPE.FieldName, MDCASESTEXTRATABLE.IDMDSERVICETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDINTERFACE.FieldName, MDCASESTEXTRATABLE.IDMDINTERFACE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDSERVICEEXTRATABLE.FieldName, MDCASESTEXTRATABLE.IDMDSERVICEEXTRATABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.LSCHANGETABLE_COMMENTS.FieldName, MDCASESTEXTRATABLE.LSCHANGETABLE_COMMENTS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDCASESTEXTRATABLE_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            //MDCASESTEXTRATABLE_GETID(MDCASESTEXTRATABLE);
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDCASESTEXTRATABLE_UPD = function (MDCASESTEXTRATABLE)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDCASESTEXTRATABLE.FieldName, MDCASESTEXTRATABLE.IDMDCASESTEXTRATABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDMODELTYPED.FieldName, MDCASESTEXTRATABLE.IDMDMODELTYPED, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDSERVICETYPE.FieldName, MDCASESTEXTRATABLE.IDMDSERVICETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDINTERFACE.FieldName, MDCASESTEXTRATABLE.IDMDINTERFACE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDSERVICEEXTRATABLE.FieldName, MDCASESTEXTRATABLE.IDMDSERVICEEXTRATABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.LSCHANGETABLE_COMMENTS.FieldName, MDCASESTEXTRATABLE.LSCHANGETABLE_COMMENTS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDCASESTEXTRATABLE_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            //GET(); 
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDCASESTEXTRATABLE_DEL = function (IDMDCASESTEXTRATABLE)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDCASESTEXTRATABLE.FieldName, IDMDCASESTEXTRATABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDCASESTEXTRATABLE_DEL", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
     
//**********************   METHODS   ********************************************************************************************
    
//List Function 
Persistence.Model.Methods.MDCASESTEXTRATABLE_ListSetID = function (MDCASESTEXTRATABLEList, IDMDCASESTEXTRATABLE)
{
    for (i = 0; i < MDCASESTEXTRATABLEList.length; i++)
    {

        if (IDMDCASESTEXTRATABLE == MDCASESTEXTRATABLEList[i].IDMDCASESTEXTRATABLE)
            return (MDCASESTEXTRATABLEList[i]);

    }

    var TMDCASESTEXTRATABLE = new Persistence.Model.Properties.TMDCASESTEXTRATABLE();
    TMDCASESTEXTRATABLE.IDMDCASESTEXTRATABLE = IDMDCASESTEXTRATABLE;
    return (TMDCASESTEXTRATABLE); 
}

Persistence.Model.Methods.MDCASESTEXTRATABLE_ListAdd = function (MDCASESTEXTRATABLEList, MDCASESTEXTRATABLE)
{
    var i = Persistence.Model.Methods.MDCASESTEXTRATABLE_ListGetIndex(MDCASESTEXTRATABLEList, MDCASESTEXTRATABLE);
    if (i == -1) MDCASESTEXTRATABLEList.push(MDCASESTEXTRATABLE);
    else
    {
        MDCASESTEXTRATABLEList[i].IDMDCASESTEXTRATABLE = MDCASESTEXTRATABLE.IDMDCASESTEXTRATABLE;
        MDCASESTEXTRATABLEList[i].IDMDINTERFACE = MDCASESTEXTRATABLE.IDMDINTERFACE;
        MDCASESTEXTRATABLEList[i].IDMDMODELTYPED = MDCASESTEXTRATABLE.IDMDMODELTYPED;
        MDCASESTEXTRATABLEList[i].IDMDSERVICEEXTRATABLE = MDCASESTEXTRATABLE.IDMDSERVICEEXTRATABLE;
        MDCASESTEXTRATABLEList[i].IDMDSERVICETYPE = MDCASESTEXTRATABLE.IDMDSERVICETYPE;
    }
}

//CJRC_26072018
Persistence.Model.Methods.MDCASESTEXTRATABLE_ListGetIndex = function (MDCASESTEXTRATABLEList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.Model.Methods.MDCASESTEXTRATABLE_ListGetIndexIDMDCASESTEXTRATABLE(MDCASESTEXTRATABLEList, Param);

    }
    else {
        Res = Persistence.Model.Methods.MDCASESTEXTRATABLE_ListGetIndexMDCASESTEXTRATABLE(MDCASESTEXTRATABLEList, Param);
    }
    return (Res);
}

Persistence.Model.Methods.MDCASESTEXTRATABLE_ListGetIndexMDCASESTEXTRATABLE = function (MDCASESTEXTRATABLEList, MDCASESTEXTRATABLE)
{
    for (i = 0; i < MDCASESTEXTRATABLEList.length; i++)
    {
        if (MDCASESTEXTRATABLE.IDMDCASESTEXTRATABLE == MDCASESTEXTRATABLEList[i].IDMDCASESTEXTRATABLE)
            return (i);
    }
    return (-1);
}
Persistence.Model.Methods.MDCASESTEXTRATABLE_ListGetIndexIDMDCASESTEXTRATABLE = function (MDCASESTEXTRATABLEList, IDMDCASESTEXTRATABLE)
{
    for (i = 0; i < MDCASESTEXTRATABLEList.length; i++)
    {
        if (IDMDCASESTEXTRATABLE == MDCASESTEXTRATABLEList[i].IDMDCASESTEXTRATABLE)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.Model.Methods.MDCASESTEXTRATABLEListtoStr = function (MDCASESTEXTRATABLEList)
{
    var Res = Persistence.Model.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(MDCASESTEXTRATABLEList.length, Longitud, Texto);
        for (Counter = 0; Counter < MDCASESTEXTRATABLEList.length; Counter++) {
            var UnMDCASESTEXTRATABLE = MDCASESTEXTRATABLEList[Counter];
            UnMDCASESTEXTRATABLE.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Model.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.Model.Methods.StrtoMDCASESTEXTRATABLE = function (ProtocoloStr, MDCASESTEXTRATABLEList)
{
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        MDCASESTEXTRATABLEList.length = 0;
        if (Persistence.Model.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnMDCASESTEXTRATABLE = new Persistence.Demo.Properties.TMDCASESTEXTRATABLE(); //Mode new row
                UnMDCASESTEXTRATABLE.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.MDCASESTEXTRATABLE_ListAdd(MDCASESTEXTRATABLEList, UnMDCASESTEXTRATABLE);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
    
}

//Socket Function 
    Persistence.Model.Methods.MDCASESTEXTRATABLEListToByte = function (MDCASESTEXTRATABLEList, MemStream)
{
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, MDCASESTEXTRATABLEList.length);
    for (i = 0; i < MDCASESTEXTRATABLEList.length; i++)
    {
        MDCASESTEXTRATABLEList[i].ToByte(MemStream);
    }
}
    Persistence.Model.Methods.ByteToMDCASESTEXTRATABLEList = function (MDCASESTEXTRATABLEList, MemStream)
{
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++)
    {
        var UnMDCASESTEXTRATABLE = new Persistence.Model.Properties.TMDCASESTEXTRATABLE();
        MDCASESTEXTRATABLEList[i].ToByte(MemStream);
    }
}
    
 