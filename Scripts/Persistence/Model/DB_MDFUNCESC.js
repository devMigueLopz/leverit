﻿//TreduceDB
//Persistence.Model.Properties
//Persistence.Model.Methods
 
 
Persistence.Model.PropertiesTMDFUNCESC = function()
{
    this.COMMENTSFE = "";
    this.IDMDFUNCESC = 0;
    this.TITLEFUNCESC = "";
    //Socket IO Properties
    this.ToByte= function(MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.COMMENTSFE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDFUNCESC);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.TITLEFUNCESC);
    }
    this.ByteTo= function(MemStream)
    {
        this.COMMENTSFE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDMDFUNCESC = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.TITLEFUNCESC = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    }
    //Str IO Properties
    this.ToStr= function(Longitud, Texto)
    {
        SysCfg.Str.Protocol.WriteStr(this.COMMENTSFE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDFUNCESC, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.TITLEFUNCESC, Longitud, Texto);
    }
    this.StrTo= function( Pos,  Index, LongitudArray, Texto)
    {
        this.COMMENTSFE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.IDMDFUNCESC = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.TITLEFUNCESC = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
    }
}
    
//**********************   METHODS server  ********************************************************************************************

     
//DataSet Function 
Persistence.Model.Methods.MDFUNCESC_Fill=function(MDFUNCESCList, StrIDMDFUNCESC/*IDMDFUNCESC*/)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    MDFUNCESCList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDMDFUNCESC == "")
    {
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDFUNCESC.IDMDFUNCESC.FieldName, " = " + UsrCfg.InternoAtisNames.MDFUNCESC.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDFUNCESC.IDMDFUNCESC.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else
    {
        StrIDMDFUNCESC = " IN (" + StrIDMDFUNCESC + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDFUNCESC.IDMDFUNCESC.FieldName, StrIDMDFUNCESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCESC.IDMDFUNCESC.FieldName, IDMDFUNCESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try
    {
        /*

    //********   MDFUNCESC_GET   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDFUNCESC_GET", "MDFUNCESC_GET description.");
    UnSQL.SQL.Add(" SELECT  ");
    UnSQL.SQL.Add("  MDFUNCESC.COMMENTSFE, ");
    UnSQL.SQL.Add("  MDFUNCESC.IDMDFUNCESC, ");
    UnSQL.SQL.Add("  MDFUNCESC.TITLEFUNCESC ");
    UnSQL.SQL.Add(" FROM  MDFUNCESC ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  MDFUNCESC.COMMENTSFE=[COMMENTSFE] and  ");
    UnSQL.SQL.Add("  MDFUNCESC.IDMDFUNCESC=[IDMDFUNCESC] and  ");
    UnSQL.SQL.Add("  MDFUNCESC.TITLEFUNCESC=[TITLEFUNCESC] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_MDFUNCESC = SysCfg.DB.SQL.Methods.OpenDataSet("DataLink", "MDFUNCESC_GET", Param.ToBytes());
        ResErr = DS_MDFUNCESC.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDFUNCESC.DataSet.RecordCount > 0)
            {
                DS_MDFUNCESC.DataSet.First();
                while (!(DS_MDFUNCESC.DataSet.Eof))
                {
                    var MDFUNCESC = new Persistence.Model.Properties.TMDFUNCESC();
                    MDFUNCESC.IDMDFUNCESC = DS_MDFUNCESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDFUNCESC.IDMDFUNCESC.FieldName).asInt32();
                    //Other
                    MDFUNCESC.TITLEFUNCESC = DS_MDFUNCESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDFUNCESC.TITLEFUNCESC.FieldName).asString();
                    MDFUNCESCList.push(MDFUNCESC);

                    DS_MDFUNCESC.DataSet.Next();
                }
            }
            else
            {
                DS_MDFUNCESC.ResErr.NotError = false;
                DS_MDFUNCESC.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDFUNCESC_GETID=function( MDFUNCESC)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCESC.IDMDFUNCESC.FieldName, MDFUNCESC.IDMDFUNCESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDFUNCESC.COMMENTSFE.FieldName, MDFUNCESC.COMMENTSFE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDFUNCESC.TITLEFUNCESC.FieldName, MDFUNCESC.TITLEFUNCESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDFUNCESC_GETID   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDFUNCESC_GETID", "MDFUNCESC_GETID description.");
    UnSQL.SQL.Add(" SELECT  ");
    UnSQL.SQL.Add("  MDFUNCESC.COMMENTSFE, ");
    UnSQL.SQL.Add("  MDFUNCESC.IDMDFUNCESC, ");
    UnSQL.SQL.Add("  MDFUNCESC.TITLEFUNCESC ");
    UnSQL.SQL.Add(" FROM  MDFUNCESC ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  MDFUNCESC.COMMENTSFE=[COMMENTSFE] and  ");
    UnSQL.SQL.Add("  MDFUNCESC.IDMDFUNCESC=[IDMDFUNCESC] and  ");
    UnSQL.SQL.Add("  MDFUNCESC.TITLEFUNCESC=[TITLEFUNCESC] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_MDFUNCESC = SysCfg.DB.SQL.Methods.OpenDataSet("DataLink", "MDFUNCESC_GETID", Param.ToBytes());
        ResErr = DS_MDFUNCESC.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDFUNCESC.DataSet.RecordCount > 0)
            {
                DS_MDFUNCESC.DataSet.First();
                MDFUNCESC.IDMDFUNCESC = DS_MDFUNCESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDFUNCESC.IDMDFUNCESC.FieldName).asInt32();
                //Other
                MDFUNCESC.TITLEFUNCESC = DS_MDFUNCESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDFUNCESC.TITLEFUNCESC.FieldName).asString();
            }
            else
            {
                DS_MDFUNCESC.ResErr.NotError = false;
                DS_MDFUNCESC.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDFUNCESC_ADD=function(MDFUNCESC)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCESC.IDMDFUNCESC.FieldName, MDFUNCESC.IDMDFUNCESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDFUNCESC.COMMENTSFE.FieldName, MDFUNCESC.COMMENTSFE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDFUNCESC.TITLEFUNCESC.FieldName, MDFUNCESC.TITLEFUNCESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDFUNCESC_ADD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDFUNCESC_ADD", "MDFUNCESC_ADD description.");
    UnSQL.SQL.Add(" INSERT INTO MDFUNCESC( ");
    UnSQL.SQL.Add("  COMMENTSFE,  ");
    UnSQL.SQL.Add("  IDMDFUNCESC,  ");
    UnSQL.SQL.Add("  TITLEFUNCESC ");
    UnSQL.SQL.Add(" )  ");
    UnSQL.SQL.Add(" VALUES ( ");
    UnSQL.SQL.Add("  [COMMENTSFE], ");
    UnSQL.SQL.Add("  [IDMDFUNCESC], ");
    UnSQL.SQL.Add("  [TITLEFUNCESC] ");
    UnSQL.SQL.Add(" ) ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("DataLink", "MDFUNCESC_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            MDFUNCESC_GETID(MDFUNCESC);
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDFUNCESC_UPD=function(MDFUNCESC)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCESC.IDMDFUNCESC.FieldName, MDFUNCESC.IDMDFUNCESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDFUNCESC.COMMENTSFE.FieldName, MDFUNCESC.COMMENTSFE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDFUNCESC.TITLEFUNCESC.FieldName, MDFUNCESC.TITLEFUNCESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDFUNCESC_UPD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDFUNCESC_UPD", "MDFUNCESC_UPD description.");
    UnSQL.SQL.Add(" UPDATE MDFUNCESC SET ");
    UnSQL.SQL.Add("  COMMENTSFE=[COMMENTSFE],  ");
    UnSQL.SQL.Add("  IDMDFUNCESC=[IDMDFUNCESC],  ");
    UnSQL.SQL.Add("  TITLEFUNCESC=[TITLEFUNCESC] ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  COMMENTSFE=[COMMENTSFE] AND  ");
    UnSQL.SQL.Add("  IDMDFUNCESC=[IDMDFUNCESC] AND  ");
    UnSQL.SQL.Add("  TITLEFUNCESC=[TITLEFUNCESC] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("DataLink", "MDFUNCESC_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            //GET(); 
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDFUNCESC_DEL=function(/* StrIDMDFUNCESC*/ IDMDFUNCESC)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        /*if (StrIDMDFUNCESC == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDFUNCESC.IDMDFUNCESC.FieldName, " = " + UsrCfg.InternoAtisNames.MDFUNCESC.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDFUNCESC.IDMDFUNCESC.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDMDFUNCESC = " IN (" + StrIDMDFUNCESC + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDFUNCESC.IDMDFUNCESC.FieldName, StrIDMDFUNCESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCESC.IDMDFUNCESC.FieldName, IDMDFUNCESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDFUNCESC_DEL_1   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDFUNCESC_DEL_1", "MDFUNCESC_DEL_1 description.");
    UnSQL.SQL.Add(" DELETE MDFUNCESC  ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  COMMENTSFE=[COMMENTSFE] AND  ");
    UnSQL.SQL.Add("  IDMDFUNCESC=[IDMDFUNCESC] AND  ");
    UnSQL.SQL.Add("  TITLEFUNCESC=[TITLEFUNCESC] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("DataLink", "MDFUNCESC_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDFUNCESC_DEL=function(MDFUNCESC)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCESC.IDMDFUNCESC.FieldName, MDFUNCESC.IDMDFUNCESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDFUNCESC.COMMENTSFE.FieldName, MDFUNCESC.COMMENTSFE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDFUNCESC.TITLEFUNCESC.FieldName, MDFUNCESC.TITLEFUNCESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDFUNCESC_DEL_2   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDFUNCESC_DEL_2", "MDFUNCESC_DEL_2 description.");
    UnSQL.SQL.Add(" DELETE MDFUNCESC  ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  COMMENTSFE=[COMMENTSFE] AND  ");
    UnSQL.SQL.Add("  IDMDFUNCESC=[IDMDFUNCESC] AND  ");
    UnSQL.SQL.Add("  TITLEFUNCESC=[TITLEFUNCESC] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("DataLink", "MDFUNCESC_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
     
//**********************   METHODS   ********************************************************************************************

    
//List Function 
Persistence.Model.Methods.MDFUNCESC_ListSetID=function(MDFUNCESCList, IDMDFUNCESC)
{
    for (i = 0; i < MDFUNCESCList.length; i++)
    {

        if (IDMDFUNCESC == MDFUNCESCList[i].IDMDFUNCESC)
            return (MDFUNCESCList[i]);

    }

    var TMDFUNCESC = new Persistence.Properties.TMDFUNCESC();
    TMDFUNCESC.IDMDFUNCESC = IDMDFUNCESC; 
    return (TMDFUNCESC); 
}

Persistence.Model.Methods.MDFUNCESC_ListAdd=function(MDFUNCESCList,MDFUNCESC)
{
    var i = Persistence.Model.Methods.MDFUNCESC_ListGetIndex(MDFUNCESCList, MDFUNCESC);
    if (i == -1) MDFUNCESCList.push(MDFUNCESC);
    else
    {
        MDFUNCESCList[i].COMMENTSFE = MDFUNCESC.COMMENTSFE;
        MDFUNCESCList[i].IDMDFUNCESC = MDFUNCESC.IDMDFUNCESC;
        MDFUNCESCList[i].TITLEFUNCESC = MDFUNCESC.TITLEFUNCESC;
    }
}
Persistence.Model.Methods.MDFUNCESC_ListGetIndex=function(MDFUNCESCList, MDFUNCESC)
{
    for (i = 0; i < MDFUNCESCList.length; i++)
    {
        if (MDFUNCESC.IDMDFUNCESC == MDFUNCESCList[i].IDMDFUNCESC)
            return (i);
    }
    return (-1);
}
Persistence.Model.Methods.MDFUNCESC_ListGetIndex=function(MDFUNCESCList, IDMDFUNCESC)
{
    for (i = 0; i < MDFUNCESCList.length; i++)
    {
        if (IDMDFUNCESC == MDFUNCESCList[i].IDMDFUNCESC)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.Model.Methods.MDFUNCESCListtoStr=function(MDFUNCESCList)
{
    var Res = Persistence.Model.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(MDFUNCESCList.length, Longitud, Texto);
        for (Counter = 0; Counter < MDFUNCESCList.length; Counter++) {
            var UnMDFUNCESC = MDFUNCESCList[Counter];
            UnMDFUNCESC.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Model.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.Model.Methods.StrtoMDFUNCESC=function(ProtocoloStr, MDFUNCESCList)
{
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        MDFUNCESCList.length = 0;
        if (Persistence.Model.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnMDFUNCESC = new Persistence.Demo.Properties.TMDFUNCESC(); //Mode new row
                UnMDFUNCESC.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.MDFUNCESC_ListAdd(MDFUNCESCList, UnMDFUNCESC);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
    
}

//Socket Function 
Persistence.Model.Methods.MDFUNCESCListToByte=function(MDFUNCESCList, MemStream)
{
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, MDFUNCESCList.length);
    for (i = 0; i < MDFUNCESCList.length; i++)
    {
        MDFUNCESCList[i].ToByte(MemStream);
    }
}
Persistence.Model.Methods.ByteToMDFUNCESCList=function(MDFUNCESCList, MemStream)
{
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++)
    {
        var UnMDFUNCESC = new Persistence.Model.Properties.TMDFUNCESC();
        MDFUNCESCList[i].ToByte(MemStream);
    }
}
     