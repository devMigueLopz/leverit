﻿//TreduceDB
//Persistence.Model.Properties
//Persistence.Model.Methods

   


Persistence.Model.Properties.TMDCATEGORYDETAIL_TYPEUSER = function()
{
    this.IDMDCATEGORYDETAIL= 0;
    this.IDMDCATEGORYDETAIL_TYPEUSER= 0;
    this.IDSDTYPEUSER = 0;
    this.IDMDCATEGORY = 0;

    this.SDTYPEUSER = new Persistence.Catalog.Properties.TSDTYPEUSER();

    this.MDCATEGORYDETAIL = new Persistence.Model.Properties.TMDCATEGORYDETAIL();

      
            
    //Socket IO Properties
    this.ToByte = function(MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDCATEGORYDETAIL);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDCATEGORYDETAIL_TYPEUSER);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDTYPEUSER);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDCATEGORY);
    }                                                     
    this.ByteTo = function(MemStream)
    {
        this.IDMDCATEGORYDETAIL = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDCATEGORYDETAIL_TYPEUSER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDTYPEUSER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDCATEGORY = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    }
    //Str IO Properties
    this.ToStr = function(Longitud, Texto)
    {
        SysCfg.Str.Protocol.WriteInt(this.IDMDCATEGORYDETAIL, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDCATEGORYDETAIL_TYPEUSER, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDSDTYPEUSER, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDCATEGORY, Longitud, Texto);
    }
    this.StrTo = function(Pos, Index, LongitudArray, Texto)
    {
        this.IDMDCATEGORYDETAIL = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDCATEGORYDETAIL_TYPEUSER = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDSDTYPEUSER = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDCATEGORY = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
    }

}
   

//**********************   METHODS server  ********************************************************************************************

//DataSet Function 
Persistence.Model.Methods.MDCATEGORYDETAIL_TYPEUSER_Fill=function(MDCATEGORYDETAIL_TYPEUSERList,  StrIDTYPEUSER,  IDMDCATEGORYDETAIL)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    MDCATEGORYDETAIL_TYPEUSERList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDTYPEUSER == "")
    {
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.IDSDTYPEUSER.FieldName, " = " + UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.IDSDTYPEUSER.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else
    {
        StrIDTYPEUSER = " IN (" + StrIDTYPEUSER + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.IDSDTYPEUSER.FieldName, StrIDTYPEUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }

    if (IDMDCATEGORYDETAIL == -1)
    {
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.IDMDCATEGORYDETAIL.FieldName, " = " + UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.IDMDCATEGORYDETAIL.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else
    {

        Param.AddInt32(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.IDMDCATEGORYDETAIL.FieldName, IDMDCATEGORYDETAIL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }            


    try
    {
        var DS_MDCATEGORYDETAIL_TYPEUSER = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDCATEGORYDETAIL_TYPEUSER_GET", Param.ToBytes());
        ResErr = DS_MDCATEGORYDETAIL_TYPEUSER.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDCATEGORYDETAIL_TYPEUSER.DataSet.RecordCount > 0)
            {
                DS_MDCATEGORYDETAIL_TYPEUSER.DataSet.First();
                while (!(DS_MDCATEGORYDETAIL_TYPEUSER.DataSet.Eof))
                {
                    var MDCATEGORYDETAIL_TYPEUSER = new Persistence.Model.Properties.TMDCATEGORYDETAIL_TYPEUSER();
                    MDCATEGORYDETAIL_TYPEUSER.IDMDCATEGORYDETAIL_TYPEUSER = DS_MDCATEGORYDETAIL_TYPEUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.IDMDCATEGORYDETAIL_TYPEUSER.FieldName).asInt32();
                    //Other
                    MDCATEGORYDETAIL_TYPEUSER.IDMDCATEGORYDETAIL = DS_MDCATEGORYDETAIL_TYPEUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.IDMDCATEGORYDETAIL.FieldName).asInt32();
                    MDCATEGORYDETAIL_TYPEUSER.IDSDTYPEUSER = DS_MDCATEGORYDETAIL_TYPEUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.IDSDTYPEUSER.FieldName).asInt32();
                    MDCATEGORYDETAIL_TYPEUSER.IDMDCATEGORY = DS_MDCATEGORYDETAIL_TYPEUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.IDMDCATEGORY.FieldName).asInt32();
                    MDCATEGORYDETAIL_TYPEUSERList.push(MDCATEGORYDETAIL_TYPEUSER);

                    DS_MDCATEGORYDETAIL_TYPEUSER.DataSet.Next();
                }
            }
            else
            {
                DS_MDCATEGORYDETAIL_TYPEUSER.ResErr.NotError = false;
                DS_MDCATEGORYDETAIL_TYPEUSER.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}

Persistence.Model.Methods.MDCATEGORYDETAIL_TYPEUSER_GETID = function(MDCATEGORYDETAIL_TYPEUSER)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.IDMDCATEGORYDETAIL.FieldName, MDCATEGORYDETAIL_TYPEUSER.IDMDCATEGORYDETAIL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.IDSDTYPEUSER.FieldName, MDCATEGORYDETAIL_TYPEUSER.IDSDTYPEUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var DS_MDCATEGORYDETAIL_TYPEUSER = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDCATEGORYDETAIL_TYPEUSER_GETID", Param.ToBytes());
        ResErr = DS_MDCATEGORYDETAIL_TYPEUSER.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDCATEGORYDETAIL_TYPEUSER.DataSet.RecordCount > 0)
            {
                DS_MDCATEGORYDETAIL_TYPEUSER.DataSet.First();
                MDCATEGORYDETAIL_TYPEUSER.IDMDCATEGORYDETAIL_TYPEUSER = DS_MDCATEGORYDETAIL_TYPEUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.IDMDCATEGORYDETAIL_TYPEUSER.FieldName).asInt32();
                //Other
                MDCATEGORYDETAIL_TYPEUSER.IDMDCATEGORYDETAIL = DS_MDCATEGORYDETAIL_TYPEUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.IDMDCATEGORYDETAIL.FieldName).asInt32();
                MDCATEGORYDETAIL_TYPEUSER.IDSDTYPEUSER = DS_MDCATEGORYDETAIL_TYPEUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.IDSDTYPEUSER.FieldName).asInt32();
                MDCATEGORYDETAIL_TYPEUSER.IDMDCATEGORY = DS_MDCATEGORYDETAIL_TYPEUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.IDMDCATEGORY.FieldName).asInt32();
            }
            else
            {
                DS_MDCATEGORYDETAIL_TYPEUSER.ResErr.NotError = false;
                DS_MDCATEGORYDETAIL_TYPEUSER.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}

Persistence.Model.Methods.MDCATEGORYDETAIL_TYPEUSER_ADD = function(MDCATEGORYDETAIL_TYPEUSER)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.IDMDCATEGORYDETAIL.FieldName, MDCATEGORYDETAIL_TYPEUSER.IDMDCATEGORYDETAIL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.IDSDTYPEUSER.FieldName, MDCATEGORYDETAIL_TYPEUSER.IDSDTYPEUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDCATEGORYDETAIL_TYPEUSER_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            MDCATEGORYDETAIL_TYPEUSER_GETID(MDCATEGORYDETAIL_TYPEUSER);
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}

//hans
Persistence.Model.Methods.MDCATEGORYDETAIL_TYPEUSER_ADDALL = function(MDCATEGORYDETAIL_TYPEUSER,  StrIDTYPEUSER, MDCATEGORY,  StrIDMDCATEGORYDETAIL, CATEGORYNAME)
{
    //[IDMDCATEGORYDETAIL]=AND MDCATEGORYDETAIL.IDMDCATEGORYDETAIL=1
    //[MDCATEGORY]= AND MDCATEGORY.CATEGORY1='SERVICIO DE IMPRESION'
    //[SDTYPEUSER]= NOT IN (4) SI QUIERES BORRAR TODOS  "=SDTYPEUSER.IDSDTYPEUSER"
    //[CATEGORYNAME]= Like..."
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        if (StrIDTYPEUSER == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.SDTYPEUSER.IDSDTYPEUSER.FieldName, " = " + UsrCfg.InternoAtisNames.SDTYPEUSER.NAME_TABLE + "." + UsrCfg.InternoAtisNames.SDTYPEUSER.IDSDTYPEUSER.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDTYPEUSER = " IN (" + StrIDTYPEUSER + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.SDTYPEUSER.IDSDTYPEUSER.FieldName, StrIDTYPEUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }
        if (StrIDMDCATEGORYDETAIL == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.IDMDCATEGORYDETAIL.FieldName, "", SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }
        else
        {
            StrIDMDCATEGORYDETAIL = "MDCATEGORYDETAIL.IDMDCATEGORYDETAIL IN (" + StrIDMDCATEGORYDETAIL + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.IDMDCATEGORYDETAIL.FieldName, StrIDTYPEUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }




        //String[] CATEGORY = new string[9];
        var StrCATEGORY1 = "";
        var StrCATEGORY2 = "";
        var StrCATEGORY3 = "";
        var StrCATEGORY4 = "";
        var StrCATEGORY5 = "";
        var StrCATEGORY6 = "";
        var StrCATEGORY7 = "";
        var StrCATEGORY8 = "";
        if (MDCATEGORY.CATEGORY1 != "") { StrCATEGORY1 = " AND " + UsrCfg.InternoAtisNames.MDCATEGORY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY1.FieldName + "='" + MDCATEGORY.CATEGORY1 + "'"; };
        if (MDCATEGORY.CATEGORY2 != "") { StrCATEGORY2 = " AND " + UsrCfg.InternoAtisNames.MDCATEGORY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY2.FieldName + "='" + MDCATEGORY.CATEGORY2 + "'"; };
        if (MDCATEGORY.CATEGORY3 != "") { StrCATEGORY3 = " AND " + UsrCfg.InternoAtisNames.MDCATEGORY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY3.FieldName + "='" + MDCATEGORY.CATEGORY3 + "'"; };
        if (MDCATEGORY.CATEGORY4 != "") { StrCATEGORY4 = " AND " + UsrCfg.InternoAtisNames.MDCATEGORY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY4.FieldName + "='" + MDCATEGORY.CATEGORY4 + "'"; };
        if (MDCATEGORY.CATEGORY5 != "") { StrCATEGORY5 = " AND " + UsrCfg.InternoAtisNames.MDCATEGORY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY5.FieldName + "='" + MDCATEGORY.CATEGORY5 + "'"; };
        if (MDCATEGORY.CATEGORY6 != "") { StrCATEGORY6 = " AND " + UsrCfg.InternoAtisNames.MDCATEGORY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY6.FieldName + "='" + MDCATEGORY.CATEGORY6 + "'"; };
        if (MDCATEGORY.CATEGORY7 != "") { StrCATEGORY7 = " AND " + UsrCfg.InternoAtisNames.MDCATEGORY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY7.FieldName + "='" + MDCATEGORY.CATEGORY7 + "'"; };
        if (MDCATEGORY.CATEGORY8 != "") { StrCATEGORY8 = " AND " + UsrCfg.InternoAtisNames.MDCATEGORY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY8.FieldName + "='" + MDCATEGORY.CATEGORY8 + "'"; };
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDCATEGORY.NAME_TABLE, StrCATEGORY1 + StrCATEGORY2 + StrCATEGORY3 + StrCATEGORY4 + StrCATEGORY5 + StrCATEGORY6 + StrCATEGORY7 + StrCATEGORY8, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDCATEGORYDETAIL_TYPEUSER_ADDALL", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            MDCATEGORYDETAIL_TYPEUSER_GETID(MDCATEGORYDETAIL_TYPEUSER);
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}


Persistence.Model.Methods.MDCATEGORYDETAIL_TYPEUSER_UPD = function(MDCATEGORYDETAIL_TYPEUSER)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.IDMDCATEGORYDETAIL_TYPEUSER.FieldName, MDCATEGORYDETAIL_TYPEUSER.IDMDCATEGORYDETAIL_TYPEUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.IDMDCATEGORYDETAIL.FieldName, MDCATEGORYDETAIL_TYPEUSER.IDMDCATEGORYDETAIL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.IDSDTYPEUSER.FieldName, MDCATEGORYDETAIL_TYPEUSER.IDSDTYPEUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDCATEGORYDETAIL_TYPEUSER_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            //GET(); 
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.Model.Methods.MDCATEGORYDETAIL_TYPEUSER_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.Model.Methods.MDCATEGORYDETAIL_TYPEUSER_DELIDMDCATEGORYDETAIL_TYPEUSER(Param);
    }
    else {
        Res = Persistence.Model.Methods.MDCATEGORYDETAIL_TYPEUSER_DELMDCATEGORYDETAIL_TYPEUSER(Param);
    }
    return (Res);
}

Persistence.Model.Methods.MDCATEGORYDETAIL_TYPEUSER_DELIDMDCATEGORYDETAIL_TYPEUSER = function (IDMDCATEGORYDETAIL_TYPEUSER)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.IDMDCATEGORYDETAIL_TYPEUSER.FieldName, IDMDCATEGORYDETAIL_TYPEUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDCATEGORYDETAIL_TYPEUSER_DEL", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDCATEGORYDETAIL_TYPEUSER_DELMDCATEGORYDETAIL_TYPEUSER = function (MDCATEGORYDETAIL_TYPEUSER)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.IDMDCATEGORYDETAIL_TYPEUSER.FieldName, MDCATEGORYDETAIL_TYPEUSER.IDMDCATEGORYDETAIL_TYPEUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDCATEGORYDETAIL_TYPEUSER_DEL", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}

//hans
Persistence.Model.Methods.MDCATEGORYDETAIL_TYPEUSER_DELALL = function(MDCATEGORYDETAIL_TYPEUSER,  StrIDTYPEUSER, MDCATEGORY,  StrIDMDCATEGORYDETAIL)
{
    //[IDMDCATEGORYDETAIL]=AND MDCATEGORYDETAIL.IDMDCATEGORYDETAIL=1
    //[MDCATEGORY]= AND MDCATEGORY.CATEGORY1='SERVICIO DE IMPRESION'
    //[SDTYPEUSER]= NOT IN (4) SI QUIERES BORRAR TODOS  "=MDCATEGORYDETAIL_TYPEUSER.IDSDTYPEUSER"
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        if (StrIDTYPEUSER == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.IDSDTYPEUSER.FieldName, " = " + UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.IDSDTYPEUSER.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDTYPEUSER = " NOT IN (" + StrIDTYPEUSER + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.IDSDTYPEUSER.FieldName, StrIDTYPEUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }
        if (StrIDMDCATEGORYDETAIL == "")
        {
            StrIDMDCATEGORYDETAIL = " IN (" + StrIDMDCATEGORYDETAIL + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.IDMDCATEGORYDETAIL.FieldName, StrIDTYPEUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }

        //String[] CATEGORY = new string[8];
        var StrCATEGORY1 = "";
        var StrCATEGORY2 = "";
        var StrCATEGORY3 = "";
        var StrCATEGORY4 = "";
        var StrCATEGORY5 = "";
        var StrCATEGORY6 = "";
        var StrCATEGORY7 = "";
        var StrCATEGORY8 = "";
        if (MDCATEGORY.CATEGORY1 != "") { StrCATEGORY1 = " AND " + UsrCfg.InternoAtisNames.MDCATEGORY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY1.FieldName + "='" + MDCATEGORY.CATEGORY1 + "'"; };
        if (MDCATEGORY.CATEGORY2 != "") { StrCATEGORY2 = " AND " + UsrCfg.InternoAtisNames.MDCATEGORY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY2.FieldName + "='" + MDCATEGORY.CATEGORY2 + "'"; };
        if (MDCATEGORY.CATEGORY3 != "") { StrCATEGORY3 = " AND " + UsrCfg.InternoAtisNames.MDCATEGORY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY3.FieldName + "='" + MDCATEGORY.CATEGORY3 + "'"; };
        if (MDCATEGORY.CATEGORY4 != "") { StrCATEGORY4 = " AND " + UsrCfg.InternoAtisNames.MDCATEGORY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY4.FieldName + "='" + MDCATEGORY.CATEGORY4 + "'"; };
        if (MDCATEGORY.CATEGORY5 != "") { StrCATEGORY5 = " AND " + UsrCfg.InternoAtisNames.MDCATEGORY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY5.FieldName + "='" + MDCATEGORY.CATEGORY5 + "'"; };
        if (MDCATEGORY.CATEGORY6 != "") { StrCATEGORY6 = " AND " + UsrCfg.InternoAtisNames.MDCATEGORY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY6.FieldName + "='" + MDCATEGORY.CATEGORY6 + "'"; };
        if (MDCATEGORY.CATEGORY7 != "") { StrCATEGORY7 = " AND " + UsrCfg.InternoAtisNames.MDCATEGORY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY7.FieldName + "='" + MDCATEGORY.CATEGORY7 + "'"; };
        if (MDCATEGORY.CATEGORY8 != "") { StrCATEGORY8 = " AND " + UsrCfg.InternoAtisNames.MDCATEGORY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY8.FieldName + "='" + MDCATEGORY.CATEGORY8 + "'"; };
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDCATEGORY.NAME_TABLE, StrCATEGORY1 + StrCATEGORY2 + StrCATEGORY3 + StrCATEGORY4 + StrCATEGORY5 + StrCATEGORY6 + StrCATEGORY7 + StrCATEGORY8, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDCATEGORYDETAIL_TYPEUSER_ADDALL", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            MDCATEGORYDETAIL_TYPEUSER_GETID(MDCATEGORYDETAIL_TYPEUSER);
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}



   

//**********************   METHODS   ********************************************************************************************

//DataSet Function 

//List Function 
Persistence.Model.Methods.MDCATEGORYDETAIL_TYPEUSER_ListSetID= function(MDCATEGORYDETAIL_TYPEUSERList, IDMDCATEGORYDETAIL_TYPEUSER)
{
    for (i = 0; i < MDCATEGORYDETAIL_TYPEUSERList.length; i++)
    {            
        if (IDMDCATEGORYDETAIL_TYPEUSER == MDCATEGORYDETAIL_TYPEUSERList[i].IDMDCATEGORYDETAIL_TYPEUSER)
            return (MDCATEGORYDETAIL_TYPEUSERList[i]);

    }

    var TMDCATEGORYDETAIL_TYPEUSER = new Persistence.Properties.TMDCATEGORYDETAIL_TYPEUSER();
    TMDCATEGORYDETAIL_TYPEUSER.IDMDCATEGORYDETAIL_TYPEUSER = IDMDCATEGORYDETAIL_TYPEUSER; 
    return (TMDCATEGORYDETAIL_TYPEUSER);    
}

Persistence.Model.Methods.MDCATEGORYDETAIL_TYPEUSER_ListAdd= function(MDCATEGORYDETAIL_TYPEUSERList, MDCATEGORYDETAIL_TYPEUSER)
{
    var i = Persistence.Model.Methods.MDCATEGORYDETAIL_TYPEUSER_ListGetIndex(MDCATEGORYDETAIL_TYPEUSERList, MDCATEGORYDETAIL_TYPEUSER);
    if (i == -1) MDCATEGORYDETAIL_TYPEUSERList.push(MDCATEGORYDETAIL_TYPEUSER);
    else
    {
        MDCATEGORYDETAIL_TYPEUSERList[i].IDMDCATEGORYDETAIL = MDCATEGORYDETAIL_TYPEUSER.IDMDCATEGORYDETAIL;
        MDCATEGORYDETAIL_TYPEUSERList[i].IDMDCATEGORYDETAIL_TYPEUSER = MDCATEGORYDETAIL_TYPEUSER.IDMDCATEGORYDETAIL_TYPEUSER;
        MDCATEGORYDETAIL_TYPEUSERList[i].IDSDTYPEUSER = MDCATEGORYDETAIL_TYPEUSER.IDSDTYPEUSER;
    }
}
Persistence.Model.Methods.MDCATEGORYDETAIL_TYPEUSER_ListGetIndex= function(MDCATEGORYDETAIL_TYPEUSERList, MDCATEGORYDETAIL_TYPEUSER)
{
    for (i = 0; i < MDCATEGORYDETAIL_TYPEUSERList.length; i++)
    {
        if (MDCATEGORYDETAIL_TYPEUSER.IDMDCATEGORYDETAIL_TYPEUSER == MDCATEGORYDETAIL_TYPEUSERList[i].IDMDCATEGORYDETAIL_TYPEUSER)
            return (i);
    }
    return (-1);
}
Persistence.Model.Methods.MDCATEGORYDETAIL_TYPEUSER_ListGetIndex= function(MDCATEGORYDETAIL_TYPEUSERList, IDMDCATEGORYDETAIL_TYPEUSER)
{
    for (i = 0; i < MDCATEGORYDETAIL_TYPEUSERList.length; i++)
    {
        if (IDMDCATEGORYDETAIL_TYPEUSER == MDCATEGORYDETAIL_TYPEUSERList[i].IDMDCATEGORYDETAIL_TYPEUSER)
            return (i);
    }
    return (-1);
}
//agragrar MDCATEGORYDETAIL_TYPEUSER_ListDELItem
//String Function 
Persistence.Model.Methods.MDCATEGORYDETAIL_TYPEUSERListtoStr= function(MDCATEGORYDETAIL_TYPEUSERList)
{
    var Res = Persistence.Model.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(MDCATEGORYDETAIL_TYPEUSERList.length, Longitud, Texto);
        for (Counter = 0; Counter < MDCATEGORYDETAIL_TYPEUSERList.length; Counter++) {
            var UnMDCATEGORYDETAIL_TYPEUSER = MDCATEGORYDETAIL_TYPEUSERList[Counter];
            UnMDCATEGORYDETAIL_TYPEUSER.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Model.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.Model.Methods.StrtoMDCATEGORYDETAIL_TYPEUSER= function( ProtocoloStr, MDCATEGORYDETAIL_TYPEUSERList)
{
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        MDCATEGORYDETAIL_TYPEUSERList.length = 0;
        if (Persistence.Model.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnMDCATEGORYDETAIL_TYPEUSER = new Persistence.Demo.Properties.TMDCATEGORYDETAIL_TYPEUSER(); //Mode new row
                UnMDCATEGORYDETAIL_TYPEUSER.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.MDCATEGORYDETAIL_TYPEUSER_ListAdd(MDCATEGORYDETAIL_TYPEUSERList, UnMDCATEGORYDETAIL_TYPEUSER);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
    
}

//Socket Function 
Persistence.Model.Methods.MDCATEGORYDETAIL_TYPEUSERListToByte= function(MDCATEGORYDETAIL_TYPEUSERList, MemStream, idx)
{
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, MDCATEGORYDETAIL_TYPEUSERList.length - idx);
    for (i = idx; i < MDCATEGORYDETAIL_TYPEUSERList.length; i++)
    {
        MDCATEGORYDETAIL_TYPEUSERList[i].ToByte(MemStream);
    }
}
Persistence.Model.Methods.ByteToMDCATEGORYDETAIL_TYPEUSERList= function(MDCATEGORYDETAIL_TYPEUSERList, MemStream)
{
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++)
    {
        var UnMDCATEGORYDETAIL_TYPEUSER = new Persistence.Model.Properties.TMDCATEGORYDETAIL_TYPEUSER();
        UnMDCATEGORYDETAIL_TYPEUSER.ByteTo(MemStream);
        Persistence.Model.Methods.MDCATEGORYDETAIL_TYPEUSER_ListAdd(MDCATEGORYDETAIL_TYPEUSERList, UnMDCATEGORYDETAIL_TYPEUSER);
    }
}
  