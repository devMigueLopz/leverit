//TreduceDB
//Persistence.Model.Properties
//Persistence.Model.Methods
 
  
Persistence.Model.PropertiesTMDCASESTEXTRAFIELDS= function()
{
    this.IDMDCASESTEXTRAFIELDS = 0;
    this.IDMDCASESTEXTRATABLE = 0;
    this.IDMDSERVICEEXTRAFIELDS = 0;
    this.IDMDLIFESTATUSPERMISSION = 0;
    this.MANDATORY = false;
    //Socket IO Properties

    this. Persistence.Model.PropertiesToByte= function(MemStream) 
    {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDCASESTEXTRAFIELDS);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDCASESTEXTRATABLE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDSERVICEEXTRAFIELDS);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDLIFESTATUSPERMISSION);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.MANDATORY);
    }

    this.Persistence.Model.PropertiesByteTo= function(MemStream) 
    {
        this.IDMDCASESTEXTRAFIELDS = SysCfg.Stream.Methods.ReadStreamInt32( MemStream);
        this.IDMDCASESTEXTRATABLE = SysCfg.Stream.Methods.ReadStreamInt32( MemStream);
        this.IDMDSERVICEEXTRAFIELDS = SysCfg.Stream.Methods.ReadStreamInt32( MemStream);
        this.IDMDLIFESTATUSPERMISSION = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.MANDATORY = SysCfg.Stream.Methods.ReadStreamBoolean(  MemStream);
    }
    //Str IO Properties
    this.Persistence.Model.PropertiesToStr= function( Longitud, Texto)
    {
        SysCfg.Str.Protocol.WriteInt(this.IDMDCASESTEXTRAFIELDS, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDCASESTEXTRATABLE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDSERVICEEXTRAFIELDS, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDLIFESTATUSPERMISSION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteBool(this.MANDATORY, Longitud, Texto);
    }
    this.Persistence.Model.PropertiesStrTo= function( Pos, Index, LongitudArray, Texto)
    {
        IDMDCASESTEXTRAFIELDS = SysCfg.Str.Protocol.ReadInt( Pos, Index, LongitudArray, Texto);
        IDMDCASESTEXTRATABLE = SysCfg.Str.Protocol.ReadInt( Pos, Index, LongitudArray, Texto);
        IDMDSERVICEEXTRAFIELDS = SysCfg.Str.Protocol.ReadInt( Pos, Index, LongitudArray, Texto);
        IDMDLIFESTATUSPERMISSION = SysCfg.Str.Protocol.ReadInt( Pos, Index, LongitudArray, Texto);
        MANDATORY = SysCfg.Str.Protocol.ReadBool( Pos, Index, LongitudArray, Texto);
    }
}



//**********************   METHODS server  ********************************************************************************************

//DataSet Function 
Persistence.Model.Methods.MDCASESTEXTRAFIELDS_Fill= function(MDCASESTEXTRAFIELDSList, StrIDMDCASESTEXTRAFIELDS/*Int32 IDMDCASESTEXTRAFIELDS*/)
{
    var ResErr = new SysCfg.Error.Properties.TResErr();
    MDCASESTEXTRAFIELDSList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDMDCASESTEXTRAFIELDS == "")
    {
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.IDMDCASESTEXTRAFIELDS.FieldName, " = " + UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.IDMDCASESTEXTRAFIELDS.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else
    {
        StrIDMDCASESTEXTRAFIELDS = " IN (" + StrIDMDCASESTEXTRAFIELDS + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.IDMDCASESTEXTRAFIELDS.FieldName, StrIDMDCASESTEXTRAFIELDS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //Param.AddInt32(UsrCfg.InternoDataLinkNames.MDCASESTEXTRAFIELDS.IDMDCASESTEXTRAFIELDS.FieldName, IDMDCASESTEXTRAFIELDS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try
    {
              
        DS_MDCASESTEXTRAFIELDS = SysCfg.DB.SQL.Methods.OpenDataSet( "Atis",  "MDCASESTEXTRAFIELDS_GET", Param.ToBytes());
        ResErr = DS_MDCASESTEXTRAFIELDS.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDCASESTEXTRAFIELDS.DataSet.RecordCount > 0)
            {
                DS_MDCASESTEXTRAFIELDS.DataSet.First();
                while (!(DS_MDCASESTEXTRAFIELDS.DataSet.Eof))
                {
                    MDCASESTEXTRAFIELDS = new Persistence.Model.Properties.TMDCASESTEXTRAFIELDS();
                    MDCASESTEXTRAFIELDS.IDMDCASESTEXTRAFIELDS = DS_MDCASESTEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.IDMDCASESTEXTRAFIELDS.FieldName).asInt32();
                    //Other
                    MDCASESTEXTRAFIELDS.IDMDCASESTEXTRATABLE = DS_MDCASESTEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.IDMDCASESTEXTRATABLE.FieldName).asInt32();
                    MDCASESTEXTRAFIELDS.IDMDLIFESTATUSPERMISSION = DS_MDCASESTEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.IDMDLIFESTATUSPERMISSION.FieldName).asInt32();
                    MDCASESTEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS = DS_MDCASESTEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS.FieldName).asInt32();
                    MDCASESTEXTRAFIELDS.MANDATORY = DS_MDCASESTEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.MANDATORY.FieldName).asBoolean();
                    MDCASESTEXTRAFIELDSList.push(MDCASESTEXTRAFIELDS);

                    DS_MDCASESTEXTRAFIELDS.DataSet.Next();
                }
            }
            else
            {
                DS_MDCASESTEXTRAFIELDS.ResErr.NotError = false;
                DS_MDCASESTEXTRAFIELDS.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}     
  
Persistence.Model.Methods.MDCASESTEXTRAFIELDS_GETID= function(MDCASESTEXTRAFIELDS, IDMDCASESTEXTRAFIELDS)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.IDMDCASESTEXTRAFIELDS.FieldName, IDMDCASESTEXTRAFIELDS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                
        DS_MDCASESTEXTRAFIELDS = SysCfg.DB.SQL.Methods.OpenDataSet( "Atis",  "MDCASESTEXTRAFIELDS_GETID", Param.ToBytes());
        ResErr = DS_MDCASESTEXTRAFIELDS.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDCASESTEXTRAFIELDS.DataSet.RecordCount > 0)
            {
                DS_MDCASESTEXTRAFIELDS.DataSet.First();
                MDCASESTEXTRAFIELDS.IDMDCASESTEXTRAFIELDS = DS_MDCASESTEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.IDMDCASESTEXTRAFIELDS.FieldName).asInt32();
                MDCASESTEXTRAFIELDS.IDMDCASESTEXTRATABLE = DS_MDCASESTEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.IDMDCASESTEXTRATABLE.FieldName).asInt32();
                MDCASESTEXTRAFIELDS.IDMDLIFESTATUSPERMISSION = DS_MDCASESTEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.IDMDLIFESTATUSPERMISSION.FieldName).asInt32();
                MDCASESTEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS = DS_MDCASESTEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS.FieldName).asInt32();
                MDCASESTEXTRAFIELDS.MANDATORY = DS_MDCASESTEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.MANDATORY.FieldName).asBoolean();
            }
            else
            {
                DS_MDCASESTEXTRAFIELDS.ResErr.NotError = false;
                DS_MDCASESTEXTRAFIELDS.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}

Persistence.Model.Methods.MDCASESTEXTRAFIELDS_ADD= function(MDCASESTEXTRAFIELDS)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.IDMDCASESTEXTRATABLE.FieldName, MDCASESTEXTRAFIELDS.IDMDCASESTEXTRATABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.IDMDLIFESTATUSPERMISSION.FieldName, MDCASESTEXTRAFIELDS.IDMDLIFESTATUSPERMISSION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS.FieldName, MDCASESTEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.MANDATORY.FieldName, MDCASESTEXTRAFIELDS.MANDATORY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        Exec = SysCfg.DB.SQL.Methods.Exec( "Atis",  "MDCASESTEXTRAFIELDS_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            //MDCASESTEXTRAFIELDS_GETID(MDCASESTEXTRAFIELDS);
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
        
Persistence.Model.Methods.MDCASESTEXTRAFIELDS_UPD= function(MDCASESTEXTRAFIELDS)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.IDMDCASESTEXTRAFIELDS.FieldName, MDCASESTEXTRAFIELDS.IDMDCASESTEXTRAFIELDS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.IDMDCASESTEXTRATABLE.FieldName, MDCASESTEXTRAFIELDS.IDMDCASESTEXTRATABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.IDMDLIFESTATUSPERMISSION.FieldName, MDCASESTEXTRAFIELDS.IDMDLIFESTATUSPERMISSION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS.FieldName, MDCASESTEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.MANDATORY.FieldName, MDCASESTEXTRAFIELDS.MANDATORY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        var Exec = SysCfg.DB.SQL.Methods.Exec( "Atis",  "MDCASESTEXTRAFIELDS_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            //GET(); 
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}

Persistence.Model.Methods.MDCASESTEXTRAFIELDS_DEL= function(IDMDCASESTEXTRAFIELDS)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.IDMDCASESTEXTRAFIELDS.FieldName, IDMDCASESTEXTRAFIELDS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Exec = SysCfg.DB.SQL.Methods.Exec( "Atis",  "MDCASESTEXTRAFIELDS_DEL", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
        
Persistence.Model.Methods.MDCASESTEXTRAFIELDS_DEL_ALL= function(IDMDCASESTEXTRATABLE)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.IDMDCASESTEXTRATABLE.FieldName, IDMDCASESTEXTRATABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec( "Atis",  "MDCASESTEXTRAFIELDS_DEL_ALL", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
 

//**********************   METHODS   ********************************************************************************************
    
 
//List Function 
Persistence.Model.Methods.MDCASESTEXTRAFIELDS_ListSetID= function(MDCASESTEXTRAFIELDSList, IDMDCASESTEXTRAFIELDS)
{
    for (i = 0; i < MDCASESTEXTRAFIELDSList.length; i++)
    {

        if (IDMDCASESTEXTRAFIELDS == MDCASESTEXTRAFIELDSList[i].IDMDCASESTEXTRAFIELDS)
            return (MDCASESTEXTRAFIELDSList[i]);

    }
    
    var TMDCASESTEXTRAFIELDS = new Persistence.Model.Properties.TMDCASESTEXTRAFIELDS();
    TMDCASESTEXTRAFIELDS.IDMDCASESTEXTRAFIELDS = IDMDCASESTEXTRAFIELDS;
    return (TMDCASESTEXTRAFIELDS); 

}

Persistence.Model.Methods.MDCASESTEXTRAFIELDS_ListAdd= function(MDCASESTEXTRAFIELDSList, MDCASESTEXTRAFIELDS)
{
    var i = Persistence.Model.Methods.MDCASESTEXTRAFIELDS_ListGetIndex(MDCASESTEXTRAFIELDSList, MDCASESTEXTRAFIELDS);
    if (i == -1) MDCASESTEXTRAFIELDSList.push(MDCASESTEXTRAFIELDS);
    else
    {
        MDCASESTEXTRAFIELDSList[i].IDMDCASESTEXTRAFIELDS = MDCASESTEXTRAFIELDS.IDMDCASESTEXTRAFIELDS;
        MDCASESTEXTRAFIELDSList[i].IDMDCASESTEXTRATABLE = MDCASESTEXTRAFIELDS.IDMDCASESTEXTRATABLE;
        MDCASESTEXTRAFIELDSList[i].IDMDLIFESTATUSPERMISSION = MDCASESTEXTRAFIELDS.IDMDLIFESTATUSPERMISSION;
        MDCASESTEXTRAFIELDSList[i].IDMDSERVICEEXTRAFIELDS = MDCASESTEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS;
        MDCASESTEXTRAFIELDSList[i].MANDATORY = MDCASESTEXTRAFIELDS.MANDATORY;
    }
}


Persistence.Model.Methods.MDCASESTEXTRAFIELDS_ListGetIndex= function(MDCASESTEXTRAFIELDSList,  MDCASESTEXTRAFIELDS)
{
    for (i = 0; i < MDCASESTEXTRAFIELDSList.length; i++)
    {
        if (MDCASESTEXTRAFIELDS.IDMDCASESTEXTRAFIELDS == MDCASESTEXTRAFIELDSList[i].IDMDCASESTEXTRAFIELDS)
            return (i);
    }
    return (-1);
}

Persistence.Model.Methods.MDCASESTEXTRAFIELDS_ListGetIndex= function(MDCASESTEXTRAFIELDSList, IDMDCASESTEXTRAFIELDS)
{
    for (i = 0; i < MDCASESTEXTRAFIELDSList.length; i++)
    {
        if (IDMDCASESTEXTRAFIELDS == MDCASESTEXTRAFIELDSList[i].IDMDCASESTEXTRAFIELDS)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.Model.Methods.MDCASESTEXTRAFIELDSListtoStr= function(MDCASESTEXTRAFIELDSList)
{
    var Res = Persistence.Model.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(MDCASESTEXTRAFIELDSList.length, Longitud, Texto);
        for (Counter = 0; Counter < MDCASESTEXTRAFIELDSList.length; Counter++) {
            var UnMDCASESTEXTRAFIELDS = MDCASESTEXTRAFIELDSList[Counter];
            UnMDCASESTEXTRAFIELDS.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Model.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.Model.Methods.StrtoMDCASESTEXTRAFIELDS= function(ProtocoloStr, MDCASESTEXTRAFIELDSList)
{
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        MDCASESTEXTRAFIELDSList.length = 0;
        if (Persistence.Model.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnMDCASESTEXTRAFIELDS = new Persistence.Demo.Properties.TMDCASESTEXTRAFIELDS(); //Mode new row
                UnMDCASESTEXTRAFIELDS.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.MDCASESTEXTRAFIELDS_ListAdd(MDCASESTEXTRAFIELDSList, UnMDCASESTEXTRAFIELDS);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
    
}

//Socket Function 
Persistence.Model.Methods.MDCASESTEXTRAFIELDSListToByte= function(MDCASESTEXTRAFIELDSList, MemStream)
{
    SysCfg.Stream.Methods.WriteStreamInt32(  MemStream, MDCASESTEXTRAFIELDSList.length);
    for (i = 0; i < MDCASESTEXTRAFIELDSList.length; i++)
    {
        MDCASESTEXTRAFIELDSList[i].ToByte(MemStream);
    }
}
Persistence.Model.Methods.ByteToMDCASESTEXTRAFIELDSList= function(MDCASESTEXTRAFIELDSList, MemStream)
{
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (var i = 0; i < Count; i++)
    {
        var UnMDCASESTEXTRAFIELDS = new Persistence.Model.Properties.TMDCASESTEXTRAFIELDS();
        MDCASESTEXTRAFIELDSList[i].ToByte(  MemStream);
    }
} 

//*******************************************************************************************************************************