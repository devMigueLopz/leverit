﻿//**********************   PROPERTIES   *****************************************************************************************
Persistence.Model.Properties.TMDINTERFACE = function ()
{
    this.CUSTOMCODE = "";
    this.IDMDINTERFACE = 0;
    this.IDMDINTERFACETYPE = 0;
    this.INTERFACE_NAME = "";

    //Socket IO Properties
    this.ToByte = function (MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CUSTOMCODE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDINTERFACE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDINTERFACETYPE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.INTERFACE_NAME);
    }
    this.ByteTo = function (MemStream)
    {
        this.CUSTOMCODE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDMDINTERFACE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDINTERFACETYPE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.INTERFACE_NAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto)
    {
        SysCfg.Str.Protocol.WriteStr(this.CUSTOMCODE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDINTERFACE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDINTERFACETYPE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.INTERFACE_NAME, Longitud, Texto);
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto)
    {
        this.CUSTOMCODE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.IDMDINTERFACE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDINTERFACETYPE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.INTERFACE_NAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
    }
}

Persistence.Model.Properties.TINTERFACEROW = function ()
{
    this.FORMNAME = "";
    this.PROPERTYNAME = "";
    this.PROPERTYTYPE = "";
    this.PROPERTYVALUE = "";
    this.PROPERTYCLASS = "";

    //Socket IO Properties
    this.ToByte = function (MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.FORMNAME);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.PROPERTYNAME);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.PROPERTYTYPE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.PROPERTYVALUE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.PROPERTYCLASS);
    }
    this.ByteTo = function (MemStream)
    {
        this.FORMNAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.PROPERTYNAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.PROPERTYTYPE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.PROPERTYVALUE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.PROPERTYCLASS = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto)
    {
        SysCfg.Str.Protocol.WriteStr(this.FORMNAME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.PROPERTYNAME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.PROPERTYTYPE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.PROPERTYVALUE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.PROPERTYCLASS, Longitud, Texto);
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto)
    {
        this.FORMNAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.PROPERTYNAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.PROPERTYTYPE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.PROPERTYVALUE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.PROPERTYCLASS = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
    }
}

//**********************   METHODS server  ********************************************************************************************


//DataSet Function 
Persistence.Model.Methods.MDINTERFACE_Fill = function (MDINTERFACEList, StrIDMDINTERFACE/*IDMDINTERFACE*/)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    MDINTERFACEList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDMDINTERFACE == "")
    {
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDINTERFACE.IDMDINTERFACE.FieldName, " = " + UsrCfg.InternoAtisNames.MDINTERFACE.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDINTERFACE.IDMDINTERFACE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else
    {
        StrIDMDINTERFACE = " IN (" + StrIDMDINTERFACE + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDINTERFACE.IDMDINTERFACE.FieldName, StrIDMDINTERFACE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //Param.AddInt32(UsrCfg.InternoDataLinkNames.MDINTERFACE.IDMDINTERFACE.FieldName, IDMDINTERFACE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try
    {
        /*

    //********   MDINTERFACE_GET   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDINTERFACE_GET", "MDINTERFACE_GET description.");
    UnSQL.SQL.Add(" SELECT  ");
    UnSQL.SQL.Add("  MDINTERFACE.CUSTOMCODE, ");
    UnSQL.SQL.Add("  MDINTERFACE.IDMDINTERFACE, ");
    UnSQL.SQL.Add("  MDINTERFACE.IDMDINTERFACETYPE, ");
    UnSQL.SQL.Add("  MDINTERFACE.INTERFACE_NAME ");
    UnSQL.SQL.Add(" FROM  MDINTERFACE ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  MDINTERFACE.CUSTOMCODE=[CUSTOMCODE] and  ");
    UnSQL.SQL.Add("  MDINTERFACE.IDMDINTERFACE=[IDMDINTERFACE] and  ");
    UnSQL.SQL.Add("  MDINTERFACE.IDMDINTERFACETYPE=[IDMDINTERFACETYPE] and  ");
    UnSQL.SQL.Add("  MDINTERFACE.INTERFACE_NAME=[INTERFACE_NAME] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_MDINTERFACE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDINTERFACE_GET", Param.ToBytes());
        ResErr = DS_MDINTERFACE.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDINTERFACE.DataSet.RecordCount > 0)
            {
                DS_MDINTERFACE.DataSet.First();
                while (!(DS_MDINTERFACE.DataSet.Eof))
                {
                    var MDINTERFACE = new Persistence.Model.Properties.TMDINTERFACE();
                    MDINTERFACE.IDMDINTERFACE = DS_MDINTERFACE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDINTERFACE.IDMDINTERFACE.FieldName).asInt32();
                    //Other
                    MDINTERFACE.IDMDINTERFACETYPE = DS_MDINTERFACE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDINTERFACE.IDMDINTERFACETYPE.FieldName).asInt32();
                    MDINTERFACE.INTERFACE_NAME = DS_MDINTERFACE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDINTERFACE.INTERFACE_NAME.FieldName).asString();
                    MDINTERFACEList.push(MDINTERFACE);

                    DS_MDINTERFACE.DataSet.Next();
                }
            }
            else
            {
                DS_MDINTERFACE.ResErr.NotError = false;
                DS_MDINTERFACE.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDINTERFACE_GETID = function (MDINTERFACE)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDINTERFACE.IDMDINTERFACE.FieldName, MDINTERFACE.IDMDINTERFACE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDINTERFACE.CUSTOMCODE.FieldName, MDINTERFACE.CUSTOMCODE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDINTERFACE.IDMDINTERFACETYPE.FieldName, MDINTERFACE.IDMDINTERFACETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDINTERFACE.INTERFACE_NAME.FieldName, MDINTERFACE.INTERFACE_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDINTERFACE_GETID   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDINTERFACE_GETID", "MDINTERFACE_GETID description.");
    UnSQL.SQL.Add(" SELECT  ");
    UnSQL.SQL.Add("  MDINTERFACE.CUSTOMCODE, ");
    UnSQL.SQL.Add("  MDINTERFACE.IDMDINTERFACE, ");
    UnSQL.SQL.Add("  MDINTERFACE.IDMDINTERFACETYPE, ");
    UnSQL.SQL.Add("  MDINTERFACE.INTERFACE_NAME ");
    UnSQL.SQL.Add(" FROM  MDINTERFACE ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  MDINTERFACE.CUSTOMCODE=[CUSTOMCODE] and  ");
    UnSQL.SQL.Add("  MDINTERFACE.IDMDINTERFACE=[IDMDINTERFACE] and  ");
    UnSQL.SQL.Add("  MDINTERFACE.IDMDINTERFACETYPE=[IDMDINTERFACETYPE] and  ");
    UnSQL.SQL.Add("  MDINTERFACE.INTERFACE_NAME=[INTERFACE_NAME] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_MDINTERFACE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDINTERFACE_GETID", Param.ToBytes());
        ResErr = DS_MDINTERFACE.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDINTERFACE.DataSet.RecordCount > 0)
            {
                DS_MDINTERFACE.DataSet.First();
                MDINTERFACE.IDMDINTERFACE = DS_MDINTERFACE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDINTERFACE.IDMDINTERFACE.FieldName).asInt32();
                //Other
                MDINTERFACE.IDMDINTERFACETYPE = DS_MDINTERFACE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDINTERFACE.IDMDINTERFACETYPE.FieldName).asInt32();
                MDINTERFACE.INTERFACE_NAME = DS_MDINTERFACE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDINTERFACE.INTERFACE_NAME.FieldName).asString();
            }
            else
            {
                DS_MDINTERFACE.ResErr.NotError = false;
                DS_MDINTERFACE.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDINTERFACE_ADD = function (MDINTERFACE)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDINTERFACE.IDMDINTERFACE.FieldName, MDINTERFACE.IDMDINTERFACE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDINTERFACE.CUSTOMCODE.FieldName, MDINTERFACE.CUSTOMCODE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDINTERFACE.IDMDINTERFACETYPE.FieldName, MDINTERFACE.IDMDINTERFACETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDINTERFACE.INTERFACE_NAME.FieldName, MDINTERFACE.INTERFACE_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDINTERFACE_ADD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDINTERFACE_ADD", "MDINTERFACE_ADD description.");
    UnSQL.SQL.Add(" INSERT INTO MDINTERFACE( ");
    UnSQL.SQL.Add("  CUSTOMCODE,  ");
    UnSQL.SQL.Add("  IDMDINTERFACE,  ");
    UnSQL.SQL.Add("  IDMDINTERFACETYPE,  ");
    UnSQL.SQL.Add("  INTERFACE_NAME ");
    UnSQL.SQL.Add(" )  ");
    UnSQL.SQL.Add(" VALUES ( ");
    UnSQL.SQL.Add("  [CUSTOMCODE], ");
    UnSQL.SQL.Add("  [IDMDINTERFACE], ");
    UnSQL.SQL.Add("  [IDMDINTERFACETYPE], ");
    UnSQL.SQL.Add("  [INTERFACE_NAME] ");
    UnSQL.SQL.Add(" ) ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDINTERFACE_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            MDINTERFACE_GETID(MDINTERFACE);
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDINTERFACE_UPD = function (MDINTERFACE)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDINTERFACE.IDMDINTERFACE.FieldName, MDINTERFACE.IDMDINTERFACE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDINTERFACE.CUSTOMCODE.FieldName, MDINTERFACE.CUSTOMCODE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDINTERFACE.IDMDINTERFACETYPE.FieldName, MDINTERFACE.IDMDINTERFACETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDINTERFACE.INTERFACE_NAME.FieldName, MDINTERFACE.INTERFACE_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDINTERFACE_UPD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDINTERFACE_UPD", "MDINTERFACE_UPD description.");
    UnSQL.SQL.Add(" UPDATE MDINTERFACE SET ");
    UnSQL.SQL.Add("  CUSTOMCODE=[CUSTOMCODE],  ");
    UnSQL.SQL.Add("  IDMDINTERFACE=[IDMDINTERFACE],  ");
    UnSQL.SQL.Add("  IDMDINTERFACETYPE=[IDMDINTERFACETYPE],  ");
    UnSQL.SQL.Add("  INTERFACE_NAME=[INTERFACE_NAME] ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  CUSTOMCODE=[CUSTOMCODE] AND  ");
    UnSQL.SQL.Add("  IDMDINTERFACE=[IDMDINTERFACE] AND  ");
    UnSQL.SQL.Add("  IDMDINTERFACETYPE=[IDMDINTERFACETYPE] AND  ");
    UnSQL.SQL.Add("  INTERFACE_NAME=[INTERFACE_NAME] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDINTERFACE_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            //GET(); 
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.Model.Methods.MDINTERFACE_DEL = function (Param)
{
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string'))
    {
        Res = Persistence.Model.Methods.MDINTERFACE_DELIDMDINTERFACE(/* StrIDMDINTERFACE*/ Param);
    }
    else
    {
        Res = Persistence.Model.Methods.MDINTERFACE_DELMDINTERFACE(Param);
    }
    return (Res);
}

Persistence.Model.Methods.MDINTERFACE_DELIDMDINTERFACE = function (/* StrIDMDINTERFACE*/ IDMDINTERFACE)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        /*if (StrIDMDINTERFACE == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDINTERFACE.IDMDINTERFACE.FieldName, " = " + UsrCfg.InternoAtisNames.MDINTERFACE.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDINTERFACE.IDMDINTERFACE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDMDINTERFACE = " IN (" + StrIDMDINTERFACE + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDINTERFACE.IDMDINTERFACE.FieldName, StrIDMDINTERFACE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.MDINTERFACE.IDMDINTERFACE.FieldName, IDMDINTERFACE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDINTERFACE_DEL_1   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDINTERFACE_DEL_1", "MDINTERFACE_DEL_1 description.");
    UnSQL.SQL.Add(" DELETE MDINTERFACE  ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  CUSTOMCODE=[CUSTOMCODE] AND  ");
    UnSQL.SQL.Add("  IDMDINTERFACE=[IDMDINTERFACE] AND  ");
    UnSQL.SQL.Add("  IDMDINTERFACETYPE=[IDMDINTERFACETYPE] AND  ");
    UnSQL.SQL.Add("  INTERFACE_NAME=[INTERFACE_NAME] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDINTERFACE_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDINTERFACE_DELMDINTERFACE = function (MDINTERFACE)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDINTERFACE.IDMDINTERFACE.FieldName, MDINTERFACE.IDMDINTERFACE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDINTERFACE.CUSTOMCODE.FieldName, MDINTERFACE.CUSTOMCODE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDINTERFACE.IDMDINTERFACETYPE.FieldName, MDINTERFACE.IDMDINTERFACETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDINTERFACE.INTERFACE_NAME.FieldName, MDINTERFACE.INTERFACE_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDINTERFACE_DEL_2   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDINTERFACE_DEL_2", "MDINTERFACE_DEL_2 description.");
    UnSQL.SQL.Add(" DELETE MDINTERFACE  ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  CUSTOMCODE=[CUSTOMCODE] AND  ");
    UnSQL.SQL.Add("  IDMDINTERFACE=[IDMDINTERFACE] AND  ");
    UnSQL.SQL.Add("  IDMDINTERFACETYPE=[IDMDINTERFACETYPE] AND  ");
    UnSQL.SQL.Add("  INTERFACE_NAME=[INTERFACE_NAME] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDINTERFACE_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}


//**********************   METHODS   ********************************************************************************************


//DataSet Function 

//List Function 
Persistence.Model.Methods.MDINTERFACE_ListSetID = function (MDINTERFACEList, IDMDINTERFACE)
{
    for (i = 0; i < MDINTERFACEList.length; i++)
    {

        if (IDMDINTERFACE == MDINTERFACEList[i].IDMDINTERFACE)
            return (MDINTERFACEList[i]);

    }

    var TMDINTERFACE = new Persistence.Properties.TMDINTERFACE();
    TMDINTERFACE.IDMDINTERFACE = IDMDINTERFACE;
    return (TMDINTERFACE);
}

Persistence.Model.Methods.MDINTERFACE_ListAdd = function (MDINTERFACEList, MDINTERFACE)
{
    var i = Persistence.Model.Methods.MDINTERFACE_ListGetIndex(MDINTERFACEList, MDINTERFACE);
    if (i == -1) MDINTERFACEList.push(MDINTERFACE);
    else
    {
        MDINTERFACEList[i].CUSTOMCODE = MDINTERFACE.CUSTOMCODE;
        MDINTERFACEList[i].IDMDINTERFACE = MDINTERFACE.IDMDINTERFACE;
        MDINTERFACEList[i].IDMDINTERFACETYPE = MDINTERFACE.IDMDINTERFACETYPE;
        MDINTERFACEList[i].INTERFACE_NAME = MDINTERFACE.INTERFACE_NAME;
    }
}
//CJRC_26072018
Persistence.Model.Methods.MDINTERFACE_ListGetIndex = function (MDINTERFACEList, Param)
{
    var Res = -1
    if (typeof (Param) == 'number')
    {
        Res = Persistence.Model.Methods.MDINTERFACE_ListGetIndexIDMDINTERFACE(MDINTERFACEList, Param);

    }
    else
    {
        Res = Persistence.Model.Methods.MDINTERFACE_ListGetIndexMDINTERFACE(MDINTERFACEList, Param);
    }
    return (Res);
}

Persistence.Model.Methods.MDINTERFACE_ListGetIndexMDINTERFACE = function (MDINTERFACEList, MDINTERFACE)
{
    for (i = 0; i < MDINTERFACEList.length; i++)
    {
        if (MDINTERFACE.IDMDINTERFACE == MDINTERFACEList[i].IDMDINTERFACE)
            return (i);
    }
    return (-1);
}
Persistence.Model.Methods.MDINTERFACE_ListGetIndexIDMDINTERFACE = function (MDINTERFACEList, IDMDINTERFACE)
{
    for (i = 0; i < MDINTERFACEList.length; i++)
    {
        if (IDMDINTERFACE == MDINTERFACEList[i].IDMDINTERFACE)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.Model.Methods.MDINTERFACEListtoStr = function (MDINTERFACEList)
{
    var Res = Persistence.Model.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try
    {
        SysCfg.Str.Protocol.WriteInt(MDINTERFACEList.length, Longitud, Texto);
        for (Counter = 0; Counter < MDINTERFACEList.length; Counter++)
        {
            var UnMDINTERFACE = MDINTERFACEList[Counter];
            UnMDINTERFACE.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Model.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e)
    {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;

}
Persistence.Model.Methods.StrtoMDINTERFACE = function (ProtocoloStr, MDINTERFACEList)
{
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try
    {
        MDINTERFACEList.length = 0;
        if (Persistence.Model.Properties._Version == ProtocoloStr.substring(0, 3))
        {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++)
            {
                UnMDINTERFACE = new Persistence.Model.Properties.TMDINTERFACE(); //Mode new row
                UnMDINTERFACE.StrTo(Pos, Index, LongitudArray, Texto);
                MDINTERFACEList.push(UnMDINTERFACE);
            }
        }
    }
    catch (e)
    {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);

}

//Socket Function 
Persistence.Model.Methods.MDINTERFACEListToByte = function (MDINTERFACEList, MemStream)
{
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, MDINTERFACEList.length);
    for (i = 0; i < MDINTERFACEList.length; i++)
    {
        MDINTERFACEList[i].ToByte(MemStream);
    }
}
Persistence.Model.Methods.ByteToMDINTERFACEList = function (MDINTERFACEList, MemStream)
{
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++)
    {
        var UnMDINTERFACE = new Persistence.Model.Properties.TMDINTERFACE();
        MDINTERFACEList[i].ToByte(MemStream);
    }
}

Persistence.Model.Methods.StrtoCUSTOMCODE = function(ProtocoloStr, MDINTERFACEList)
{            
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try
    {
        MDINTERFACEList.length = 0;
        if (Persistence.Model.Properties._Version == ProtocoloStr.substring(0, 3))
        {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++)
            {
                UnINTERFACEROW = new Persistence.Model.Properties.TINTERFACEROW();
                UnINTERFACEROW.StrTo(Pos, Index, LongitudArray, Texto);
                //UnINTERFACEROW.FORMNAME = SysCfg.Str.Protocol.ReadStr(ref Pos, ref Index, LongitudArray, Texto);
                //UnINTERFACEROW.PROPERTYNAME = SysCfg.Str.Protocol.ReadStr(ref Pos, ref Index, LongitudArray, Texto);
                //UnINTERFACEROW.PROPERTYTYPE = (Properties.TPROPERTYTYPE)SysCfg.Str.Protocol.ReadInt(ref Pos, ref Index, LongitudArray, Texto);
                //UnINTERFACEROW.PROPERTYVALUE = SysCfg.Str.Protocol.ReadStr(ref Pos, ref Index, LongitudArray, Texto);
                //UnINTERFACEROW.PROPERTYCLASS = SysCfg.Str.Protocol.ReadStr(ref Pos, ref Index, LongitudArray, Texto);
                MDINTERFACEList.push(UnINTERFACEROW);
            }
        }
    }
    catch (e)
    {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);

}
