﻿//TreduceDB
//Persistence.Model.Properties
//Persistence.Model.Methods

       
Persistence.Model.Properties.TMDMODELTYPED = function()
{
    this.COMMENTSM = "";
    this.GUIDETEXT = "";
    this.IDCMDBCIDEFINE = 0;
    this.IDMDFUNCESC = 0;
    this.IDMDHIERESC = 0;
    this.IDMDMODELTYPED = 0;
    this.IDMDMODELTYPED1 = 0;
    this.IDMDSERVICETYPE = 0;
    this.MAXTIME = 0;
    this.NORMALTIME = 0;
    this.OPEARTIONM = "";
    this.POSSIBLERETURNS = "";
    this.STEEPTYPEUSERENABLE = false;
    this.STEPVALIDATE = false;
    this.TIMEENABLE = false;
    this.TITLEM = "";

    this.MDMATRIXMODELSList = new Array(); //Persistence.Model.Properties.TMDMATRIXMODELS
    //Socket IO Properties
    this.ToByte = function(MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.COMMENTSM);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.GUIDETEXT);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDCMDBCIDEFINE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDFUNCESC);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDHIERESC);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDMODELTYPED);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDMODELTYPED1);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDSERVICETYPE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.MAXTIME);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.NORMALTIME);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.OPEARTIONM);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.POSSIBLERETURNS);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.STEEPTYPEUSERENABLE);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.STEPVALIDATE);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.TIMEENABLE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.TITLEM);
    }
    this.ByteTo = function(MemStream)
    {
        this.COMMENTSM = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.GUIDETEXT = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDCMDBCIDEFINE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDFUNCESC = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDHIERESC = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDMODELTYPED = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDMODELTYPED1 = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDSERVICETYPE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.MAXTIME = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.NORMALTIME = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.OPEARTIONM = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.POSSIBLERETURNS = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.STEEPTYPEUSERENABLE = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.STEPVALIDATE = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.TIMEENABLE = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.TITLEM = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    }
    //Str IO Properties
    this.ToStr = function(Longitud, Texto)
    {
        SysCfg.Str.Protocol.WriteStr(this.COMMENTSM, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.GUIDETEXT, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDCMDBCIDEFINE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDFUNCESC, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDHIERESC, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDMODELTYPED, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDMODELTYPED1, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDSERVICETYPE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.MAXTIME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.NORMALTIME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.OPEARTIONM, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.POSSIBLERETURNS, Longitud, Texto);
        SysCfg.Str.Protocol.WriteBool(this.STEEPTYPEUSERENABLE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteBool(this.STEPVALIDATE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteBool(this.TIMEENABLE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.TITLEM, Longitud, Texto);
    }
    this.StrTo = function(Pos, Index, LongitudArray, Texto)
    {
        this.COMMENTSM = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.GUIDETEXT = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.IDCMDBCIDEFINE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDFUNCESC = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDHIERESC = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDMODELTYPED = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDMODELTYPED1 = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDSERVICETYPE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.MAXTIME = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.NORMALTIME = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.OPEARTIONM = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.POSSIBLERETURNS = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.STEEPTYPEUSERENABLE = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);
        this.STEPVALIDATE = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);
        this.TIMEENABLE = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);
        this.TITLEM = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
    }
}
     
   
//**********************   METHODS server  ********************************************************************************************
//DataSet Function 
Persistence.Model.Methods.MDMODELTYPED_Fill=function(MDMODELTYPEDList, StrIDMDMODELTYPED/* IDMDMODELTYPED*/)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    MDMODELTYPEDList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDMDMODELTYPED == "")
    {
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDMODELTYPED.FieldName, " = " + UsrCfg.InternoAtisNames.MDMODELTYPED.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDMODELTYPED.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else
    {
        StrIDMDMODELTYPED = " IN (" + StrIDMDMODELTYPED + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDMODELTYPED.FieldName, StrIDMDMODELTYPED, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //Param.AddInt32(UsrCfg.InternoDataLinkNames.MDMODELTYPED.IDMDMODELTYPED.FieldName, IDMDMODELTYPED, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try
    {
        /*

    //********   MDMODELTYPED_GET   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDMODELTYPED_GET", "MDMODELTYPED_GET description.");
    UnSQL.SQL.Add(" SELECT  ");
    UnSQL.SQL.Add("  MDMODELTYPED.COMMENTSM, ");
    UnSQL.SQL.Add("  MDMODELTYPED.GUIDETEXT, ");
    UnSQL.SQL.Add("  MDMODELTYPED.IDCMDBCIDEFINE, ");
    UnSQL.SQL.Add("  MDMODELTYPED.IDMDFUNCESC, ");
    UnSQL.SQL.Add("  MDMODELTYPED.IDMDHIERESC, ");
    UnSQL.SQL.Add("  MDMODELTYPED.IDMDMODELTYPED, ");
    UnSQL.SQL.Add("  MDMODELTYPED.IDMDMODELTYPED1, ");
    UnSQL.SQL.Add("  MDMODELTYPED.IDMDSERVICETYPE, ");
    UnSQL.SQL.Add("  MDMODELTYPED.MAXTIME, ");
    UnSQL.SQL.Add("  MDMODELTYPED.NORMALTIME, ");
    UnSQL.SQL.Add("  MDMODELTYPED.OPEARTIONM, ");
    UnSQL.SQL.Add("  MDMODELTYPED.POSSIBLERETURNS, ");
    UnSQL.SQL.Add("  MDMODELTYPED.STEEPTYPEUSERENABLE, ");
    UnSQL.SQL.Add("  MDMODELTYPED.STEPVALIDATE, ");
    UnSQL.SQL.Add("  MDMODELTYPED.TIMEENABLE, ");
    UnSQL.SQL.Add("  MDMODELTYPED.TITLEM ");
    UnSQL.SQL.Add(" FROM  MDMODELTYPED ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  MDMODELTYPED.COMMENTSM=[COMMENTSM] and  ");
    UnSQL.SQL.Add("  MDMODELTYPED.GUIDETEXT=[GUIDETEXT] and  ");
    UnSQL.SQL.Add("  MDMODELTYPED.IDCMDBCIDEFINE=[IDCMDBCIDEFINE] and  ");
    UnSQL.SQL.Add("  MDMODELTYPED.IDMDFUNCESC=[IDMDFUNCESC] and  ");
    UnSQL.SQL.Add("  MDMODELTYPED.IDMDHIERESC=[IDMDHIERESC] and  ");
    UnSQL.SQL.Add("  MDMODELTYPED.IDMDMODELTYPED=[IDMDMODELTYPED] and  ");
    UnSQL.SQL.Add("  MDMODELTYPED.IDMDMODELTYPED1=[IDMDMODELTYPED1] and  ");
    UnSQL.SQL.Add("  MDMODELTYPED.IDMDSERVICETYPE=[IDMDSERVICETYPE] and  ");
    UnSQL.SQL.Add("  MDMODELTYPED.MAXTIME=[MAXTIME] and  ");
    UnSQL.SQL.Add("  MDMODELTYPED.NORMALTIME=[NORMALTIME] and  ");
    UnSQL.SQL.Add("  MDMODELTYPED.OPEARTIONM=[OPEARTIONM] and  ");
    UnSQL.SQL.Add("  MDMODELTYPED.POSSIBLERETURNS=[POSSIBLERETURNS] and  ");
    UnSQL.SQL.Add("  MDMODELTYPED.STEEPTYPEUSERENABLE=[STEEPTYPEUSERENABLE] and  ");
    UnSQL.SQL.Add("  MDMODELTYPED.STEPVALIDATE=[STEPVALIDATE] and  ");
    UnSQL.SQL.Add("  MDMODELTYPED.TIMEENABLE=[TIMEENABLE] and  ");
    UnSQL.SQL.Add("  MDMODELTYPED.TITLEM=[TITLEM] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_MDMODELTYPED = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDMODELTYPED_GET", Param.ToBytes());
        ResErr = DS_MDMODELTYPED.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDMODELTYPED.DataSet.RecordCount > 0)
            {
                DS_MDMODELTYPED.DataSet.First();
                while (!(DS_MDMODELTYPED.DataSet.Eof))
                {
                    var MDMODELTYPED = new Persistence.Model.Properties.TMDMODELTYPED();
                    MDMODELTYPED.IDMDMODELTYPED = DS_MDMODELTYPED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDMODELTYPED.FieldName).asInt32();
                    //Other
                    MDMODELTYPED.IDCMDBCIDEFINE = DS_MDMODELTYPED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDMODELTYPED.IDCMDBCIDEFINE.FieldName).asInt32();
                    MDMODELTYPED.IDMDFUNCESC = DS_MDMODELTYPED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDFUNCESC.FieldName).asInt32();
                    MDMODELTYPED.IDMDHIERESC = DS_MDMODELTYPED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDHIERESC.FieldName).asInt32();
                    MDMODELTYPED.IDMDMODELTYPED1 = DS_MDMODELTYPED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDMODELTYPED1.FieldName).asInt32();
                    MDMODELTYPED.IDMDSERVICETYPE = DS_MDMODELTYPED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDSERVICETYPE.FieldName).asInt32();
                    MDMODELTYPED.MAXTIME = DS_MDMODELTYPED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDMODELTYPED.MAXTIME.FieldName).asInt32();
                    MDMODELTYPED.NORMALTIME = DS_MDMODELTYPED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDMODELTYPED.NORMALTIME.FieldName).asInt32();
                    MDMODELTYPED.OPEARTIONM = DS_MDMODELTYPED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDMODELTYPED.OPEARTIONM.FieldName).asString();
                    MDMODELTYPED.STEEPTYPEUSERENABLE = DS_MDMODELTYPED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDMODELTYPED.STEEPTYPEUSERENABLE.FieldName).asBoolean();
                    MDMODELTYPED.STEPVALIDATE = DS_MDMODELTYPED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDMODELTYPED.STEPVALIDATE.FieldName).asBoolean();
                    MDMODELTYPED.TIMEENABLE = DS_MDMODELTYPED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDMODELTYPED.TIMEENABLE.FieldName).asBoolean();
                    MDMODELTYPED.TITLEM = DS_MDMODELTYPED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDMODELTYPED.TITLEM.FieldName).asString();
                    MDMODELTYPEDList.push(MDMODELTYPED);

                    DS_MDMODELTYPED.DataSet.Next();
                }
            }
            else
            {
                DS_MDMODELTYPED.ResErr.NotError = false;
                DS_MDMODELTYPED.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDMODELTYPED_GETID=function( MDMODELTYPED)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDMODELTYPED.FieldName, MDMODELTYPED.IDMDMODELTYPED, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDMODELTYPED.COMMENTSM.FieldName, MDMODELTYPED.COMMENTSM, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.MDMODELTYPED.GUIDETEXT.FieldName, MDMODELTYPED.GUIDETEXT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMODELTYPED.IDCMDBCIDEFINE.FieldName, MDMODELTYPED.IDCMDBCIDEFINE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDFUNCESC.FieldName, MDMODELTYPED.IDMDFUNCESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDHIERESC.FieldName, MDMODELTYPED.IDMDHIERESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDMODELTYPED1.FieldName, MDMODELTYPED.IDMDMODELTYPED1, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDSERVICETYPE.FieldName, MDMODELTYPED.IDMDSERVICETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMODELTYPED.MAXTIME.FieldName, MDMODELTYPED.MAXTIME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMODELTYPED.NORMALTIME.FieldName, MDMODELTYPED.NORMALTIME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDMODELTYPED.OPEARTIONM.FieldName, MDMODELTYPED.OPEARTIONM, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.MDMODELTYPED.POSSIBLERETURNS.FieldName, MDMODELTYPED.POSSIBLERETURNS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.MDMODELTYPED.STEEPTYPEUSERENABLE.FieldName, MDMODELTYPED.STEEPTYPEUSERENABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.MDMODELTYPED.STEPVALIDATE.FieldName, MDMODELTYPED.STEPVALIDATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.MDMODELTYPED.TIMEENABLE.FieldName, MDMODELTYPED.TIMEENABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDMODELTYPED.TITLEM.FieldName, MDMODELTYPED.TITLEM, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDMODELTYPED_GETID   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDMODELTYPED_GETID", "MDMODELTYPED_GETID description.");
    UnSQL.SQL.Add(" SELECT  ");
    UnSQL.SQL.Add("  MDMODELTYPED.COMMENTSM, ");
    UnSQL.SQL.Add("  MDMODELTYPED.GUIDETEXT, ");
    UnSQL.SQL.Add("  MDMODELTYPED.IDCMDBCIDEFINE, ");
    UnSQL.SQL.Add("  MDMODELTYPED.IDMDFUNCESC, ");
    UnSQL.SQL.Add("  MDMODELTYPED.IDMDHIERESC, ");
    UnSQL.SQL.Add("  MDMODELTYPED.IDMDMODELTYPED, ");
    UnSQL.SQL.Add("  MDMODELTYPED.IDMDMODELTYPED1, ");
    UnSQL.SQL.Add("  MDMODELTYPED.IDMDSERVICETYPE, ");
    UnSQL.SQL.Add("  MDMODELTYPED.MAXTIME, ");
    UnSQL.SQL.Add("  MDMODELTYPED.NORMALTIME, ");
    UnSQL.SQL.Add("  MDMODELTYPED.OPEARTIONM, ");
    UnSQL.SQL.Add("  MDMODELTYPED.POSSIBLERETURNS, ");
    UnSQL.SQL.Add("  MDMODELTYPED.STEEPTYPEUSERENABLE, ");
    UnSQL.SQL.Add("  MDMODELTYPED.STEPVALIDATE, ");
    UnSQL.SQL.Add("  MDMODELTYPED.TIMEENABLE, ");
    UnSQL.SQL.Add("  MDMODELTYPED.TITLEM ");
    UnSQL.SQL.Add(" FROM  MDMODELTYPED ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  MDMODELTYPED.COMMENTSM=[COMMENTSM] and  ");
    UnSQL.SQL.Add("  MDMODELTYPED.GUIDETEXT=[GUIDETEXT] and  ");
    UnSQL.SQL.Add("  MDMODELTYPED.IDCMDBCIDEFINE=[IDCMDBCIDEFINE] and  ");
    UnSQL.SQL.Add("  MDMODELTYPED.IDMDFUNCESC=[IDMDFUNCESC] and  ");
    UnSQL.SQL.Add("  MDMODELTYPED.IDMDHIERESC=[IDMDHIERESC] and  ");
    UnSQL.SQL.Add("  MDMODELTYPED.IDMDMODELTYPED=[IDMDMODELTYPED] and  ");
    UnSQL.SQL.Add("  MDMODELTYPED.IDMDMODELTYPED1=[IDMDMODELTYPED1] and  ");
    UnSQL.SQL.Add("  MDMODELTYPED.IDMDSERVICETYPE=[IDMDSERVICETYPE] and  ");
    UnSQL.SQL.Add("  MDMODELTYPED.MAXTIME=[MAXTIME] and  ");
    UnSQL.SQL.Add("  MDMODELTYPED.NORMALTIME=[NORMALTIME] and  ");
    UnSQL.SQL.Add("  MDMODELTYPED.OPEARTIONM=[OPEARTIONM] and  ");
    UnSQL.SQL.Add("  MDMODELTYPED.POSSIBLERETURNS=[POSSIBLERETURNS] and  ");
    UnSQL.SQL.Add("  MDMODELTYPED.STEEPTYPEUSERENABLE=[STEEPTYPEUSERENABLE] and  ");
    UnSQL.SQL.Add("  MDMODELTYPED.STEPVALIDATE=[STEPVALIDATE] and  ");
    UnSQL.SQL.Add("  MDMODELTYPED.TIMEENABLE=[TIMEENABLE] and  ");
    UnSQL.SQL.Add("  MDMODELTYPED.TITLEM=[TITLEM] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_MDMODELTYPED = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDMODELTYPED_GETID", Param.ToBytes());
        ResErr = DS_MDMODELTYPED.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDMODELTYPED.DataSet.RecordCount > 0)
            {
                DS_MDMODELTYPED.DataSet.First();
                MDMODELTYPED.IDMDMODELTYPED = DS_MDMODELTYPED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDMODELTYPED.FieldName).asInt32();
                //Other
                MDMODELTYPED.IDCMDBCIDEFINE = DS_MDMODELTYPED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDMODELTYPED.IDCMDBCIDEFINE.FieldName).asInt32();
                MDMODELTYPED.IDMDFUNCESC = DS_MDMODELTYPED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDFUNCESC.FieldName).asInt32();
                MDMODELTYPED.IDMDHIERESC = DS_MDMODELTYPED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDHIERESC.FieldName).asInt32();
                MDMODELTYPED.IDMDMODELTYPED1 = DS_MDMODELTYPED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDMODELTYPED1.FieldName).asInt32();
                MDMODELTYPED.IDMDSERVICETYPE = DS_MDMODELTYPED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDSERVICETYPE.FieldName).asInt32();
                MDMODELTYPED.MAXTIME = DS_MDMODELTYPED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDMODELTYPED.MAXTIME.FieldName).asInt32();
                MDMODELTYPED.NORMALTIME = DS_MDMODELTYPED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDMODELTYPED.NORMALTIME.FieldName).asInt32();
                MDMODELTYPED.OPEARTIONM = DS_MDMODELTYPED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDMODELTYPED.OPEARTIONM.FieldName).asString();
                MDMODELTYPED.STEEPTYPEUSERENABLE = DS_MDMODELTYPED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDMODELTYPED.STEEPTYPEUSERENABLE.FieldName).asBoolean();
                MDMODELTYPED.STEPVALIDATE = DS_MDMODELTYPED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDMODELTYPED.STEPVALIDATE.FieldName).asBoolean();
                MDMODELTYPED.TIMEENABLE = DS_MDMODELTYPED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDMODELTYPED.TIMEENABLE.FieldName).asBoolean();
                MDMODELTYPED.TITLEM = DS_MDMODELTYPED.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDMODELTYPED.TITLEM.FieldName).asString();
            }
            else
            {
                DS_MDMODELTYPED.ResErr.NotError = false;
                DS_MDMODELTYPED.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDMODELTYPED_ADD=function(MDMODELTYPED)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDMODELTYPED.FieldName, MDMODELTYPED.IDMDMODELTYPED, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDMODELTYPED.COMMENTSM.FieldName, MDMODELTYPED.COMMENTSM, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.MDMODELTYPED.GUIDETEXT.FieldName, MDMODELTYPED.GUIDETEXT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMODELTYPED.IDCMDBCIDEFINE.FieldName, MDMODELTYPED.IDCMDBCIDEFINE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDFUNCESC.FieldName, MDMODELTYPED.IDMDFUNCESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDHIERESC.FieldName, MDMODELTYPED.IDMDHIERESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDMODELTYPED1.FieldName, MDMODELTYPED.IDMDMODELTYPED1, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDSERVICETYPE.FieldName, MDMODELTYPED.IDMDSERVICETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMODELTYPED.MAXTIME.FieldName, MDMODELTYPED.MAXTIME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMODELTYPED.NORMALTIME.FieldName, MDMODELTYPED.NORMALTIME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDMODELTYPED.OPEARTIONM.FieldName, MDMODELTYPED.OPEARTIONM, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.MDMODELTYPED.POSSIBLERETURNS.FieldName, MDMODELTYPED.POSSIBLERETURNS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.MDMODELTYPED.STEEPTYPEUSERENABLE.FieldName, MDMODELTYPED.STEEPTYPEUSERENABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.MDMODELTYPED.STEPVALIDATE.FieldName, MDMODELTYPED.STEPVALIDATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.MDMODELTYPED.TIMEENABLE.FieldName, MDMODELTYPED.TIMEENABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDMODELTYPED.TITLEM.FieldName, MDMODELTYPED.TITLEM, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDMODELTYPED_ADD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDMODELTYPED_ADD", "MDMODELTYPED_ADD description.");
    UnSQL.SQL.Add(" INSERT INTO MDMODELTYPED( ");
    UnSQL.SQL.Add("  COMMENTSM,  ");
    UnSQL.SQL.Add("  GUIDETEXT,  ");
    UnSQL.SQL.Add("  IDCMDBCIDEFINE,  ");
    UnSQL.SQL.Add("  IDMDFUNCESC,  ");
    UnSQL.SQL.Add("  IDMDHIERESC,  ");
    UnSQL.SQL.Add("  IDMDMODELTYPED,  ");
    UnSQL.SQL.Add("  IDMDMODELTYPED1,  ");
    UnSQL.SQL.Add("  IDMDSERVICETYPE,  ");
    UnSQL.SQL.Add("  MAXTIME,  ");
    UnSQL.SQL.Add("  NORMALTIME,  ");
    UnSQL.SQL.Add("  OPEARTIONM,  ");
    UnSQL.SQL.Add("  POSSIBLERETURNS,  ");
    UnSQL.SQL.Add("  STEEPTYPEUSERENABLE,  ");
    UnSQL.SQL.Add("  STEPVALIDATE,  ");
    UnSQL.SQL.Add("  TIMEENABLE,  ");
    UnSQL.SQL.Add("  TITLEM ");
    UnSQL.SQL.Add(" )  ");
    UnSQL.SQL.Add(" VALUES ( ");
    UnSQL.SQL.Add("  [COMMENTSM], ");
    UnSQL.SQL.Add("  [GUIDETEXT], ");
    UnSQL.SQL.Add("  [IDCMDBCIDEFINE], ");
    UnSQL.SQL.Add("  [IDMDFUNCESC], ");
    UnSQL.SQL.Add("  [IDMDHIERESC], ");
    UnSQL.SQL.Add("  [IDMDMODELTYPED], ");
    UnSQL.SQL.Add("  [IDMDMODELTYPED1], ");
    UnSQL.SQL.Add("  [IDMDSERVICETYPE], ");
    UnSQL.SQL.Add("  [MAXTIME], ");
    UnSQL.SQL.Add("  [NORMALTIME], ");
    UnSQL.SQL.Add("  [OPEARTIONM], ");
    UnSQL.SQL.Add("  [POSSIBLERETURNS], ");
    UnSQL.SQL.Add("  [STEEPTYPEUSERENABLE], ");
    UnSQL.SQL.Add("  [STEPVALIDATE], ");
    UnSQL.SQL.Add("  [TIMEENABLE], ");
    UnSQL.SQL.Add("  [TITLEM] ");
    UnSQL.SQL.Add(" ) ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDMODELTYPED_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            MDMODELTYPED_GETID(MDMODELTYPED);
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDMODELTYPED_UPD=function(MDMODELTYPED)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDMODELTYPED.FieldName, MDMODELTYPED.IDMDMODELTYPED, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDMODELTYPED.COMMENTSM.FieldName, MDMODELTYPED.COMMENTSM, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.MDMODELTYPED.GUIDETEXT.FieldName, MDMODELTYPED.GUIDETEXT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMODELTYPED.IDCMDBCIDEFINE.FieldName, MDMODELTYPED.IDCMDBCIDEFINE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDFUNCESC.FieldName, MDMODELTYPED.IDMDFUNCESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDHIERESC.FieldName, MDMODELTYPED.IDMDHIERESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDMODELTYPED1.FieldName, MDMODELTYPED.IDMDMODELTYPED1, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDSERVICETYPE.FieldName, MDMODELTYPED.IDMDSERVICETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMODELTYPED.MAXTIME.FieldName, MDMODELTYPED.MAXTIME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMODELTYPED.NORMALTIME.FieldName, MDMODELTYPED.NORMALTIME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDMODELTYPED.OPEARTIONM.FieldName, MDMODELTYPED.OPEARTIONM, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.MDMODELTYPED.POSSIBLERETURNS.FieldName, MDMODELTYPED.POSSIBLERETURNS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.MDMODELTYPED.STEEPTYPEUSERENABLE.FieldName, MDMODELTYPED.STEEPTYPEUSERENABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.MDMODELTYPED.STEPVALIDATE.FieldName, MDMODELTYPED.STEPVALIDATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.MDMODELTYPED.TIMEENABLE.FieldName, MDMODELTYPED.TIMEENABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDMODELTYPED.TITLEM.FieldName, MDMODELTYPED.TITLEM, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDMODELTYPED_UPD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDMODELTYPED_UPD", "MDMODELTYPED_UPD description.");
    UnSQL.SQL.Add(" UPDATE MDMODELTYPED SET ");
    UnSQL.SQL.Add("  COMMENTSM=[COMMENTSM],  ");
    UnSQL.SQL.Add("  GUIDETEXT=[GUIDETEXT],  ");
    UnSQL.SQL.Add("  IDCMDBCIDEFINE=[IDCMDBCIDEFINE],  ");
    UnSQL.SQL.Add("  IDMDFUNCESC=[IDMDFUNCESC],  ");
    UnSQL.SQL.Add("  IDMDHIERESC=[IDMDHIERESC],  ");
    UnSQL.SQL.Add("  IDMDMODELTYPED=[IDMDMODELTYPED],  ");
    UnSQL.SQL.Add("  IDMDMODELTYPED1=[IDMDMODELTYPED1],  ");
    UnSQL.SQL.Add("  IDMDSERVICETYPE=[IDMDSERVICETYPE],  ");
    UnSQL.SQL.Add("  MAXTIME=[MAXTIME],  ");
    UnSQL.SQL.Add("  NORMALTIME=[NORMALTIME],  ");
    UnSQL.SQL.Add("  OPEARTIONM=[OPEARTIONM],  ");
    UnSQL.SQL.Add("  POSSIBLERETURNS=[POSSIBLERETURNS],  ");
    UnSQL.SQL.Add("  STEEPTYPEUSERENABLE=[STEEPTYPEUSERENABLE],  ");
    UnSQL.SQL.Add("  STEPVALIDATE=[STEPVALIDATE],  ");
    UnSQL.SQL.Add("  TIMEENABLE=[TIMEENABLE],  ");
    UnSQL.SQL.Add("  TITLEM=[TITLEM] ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  COMMENTSM=[COMMENTSM] AND  ");
    UnSQL.SQL.Add("  GUIDETEXT=[GUIDETEXT] AND  ");
    UnSQL.SQL.Add("  IDCMDBCIDEFINE=[IDCMDBCIDEFINE] AND  ");
    UnSQL.SQL.Add("  IDMDFUNCESC=[IDMDFUNCESC] AND  ");
    UnSQL.SQL.Add("  IDMDHIERESC=[IDMDHIERESC] AND  ");
    UnSQL.SQL.Add("  IDMDMODELTYPED=[IDMDMODELTYPED] AND  ");
    UnSQL.SQL.Add("  IDMDMODELTYPED1=[IDMDMODELTYPED1] AND  ");
    UnSQL.SQL.Add("  IDMDSERVICETYPE=[IDMDSERVICETYPE] AND  ");
    UnSQL.SQL.Add("  MAXTIME=[MAXTIME] AND  ");
    UnSQL.SQL.Add("  NORMALTIME=[NORMALTIME] AND  ");
    UnSQL.SQL.Add("  OPEARTIONM=[OPEARTIONM] AND  ");
    UnSQL.SQL.Add("  POSSIBLERETURNS=[POSSIBLERETURNS] AND  ");
    UnSQL.SQL.Add("  STEEPTYPEUSERENABLE=[STEEPTYPEUSERENABLE] AND  ");
    UnSQL.SQL.Add("  STEPVALIDATE=[STEPVALIDATE] AND  ");
    UnSQL.SQL.Add("  TIMEENABLE=[TIMEENABLE] AND  ");
    UnSQL.SQL.Add("  TITLEM=[TITLEM] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDMODELTYPED_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            //GET(); 
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDMODELTYPED_DEL=function(/* StrIDMDMODELTYPED*/ IDMDMODELTYPED)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        /*if (StrIDMDMODELTYPED == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDMODELTYPED.FieldName, " = " + UsrCfg.InternoAtisNames.MDMODELTYPED.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDMODELTYPED.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDMDMODELTYPED = " IN (" + StrIDMDMODELTYPED + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDMODELTYPED.FieldName, StrIDMDMODELTYPED, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDMODELTYPED.FieldName, IDMDMODELTYPED, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDMODELTYPED_DEL_1   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDMODELTYPED_DEL_1", "MDMODELTYPED_DEL_1 description.");
    UnSQL.SQL.Add(" DELETE MDMODELTYPED  ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  COMMENTSM=[COMMENTSM] AND  ");
    UnSQL.SQL.Add("  GUIDETEXT=[GUIDETEXT] AND  ");
    UnSQL.SQL.Add("  IDCMDBCIDEFINE=[IDCMDBCIDEFINE] AND  ");
    UnSQL.SQL.Add("  IDMDFUNCESC=[IDMDFUNCESC] AND  ");
    UnSQL.SQL.Add("  IDMDHIERESC=[IDMDHIERESC] AND  ");
    UnSQL.SQL.Add("  IDMDMODELTYPED=[IDMDMODELTYPED] AND  ");
    UnSQL.SQL.Add("  IDMDMODELTYPED1=[IDMDMODELTYPED1] AND  ");
    UnSQL.SQL.Add("  IDMDSERVICETYPE=[IDMDSERVICETYPE] AND  ");
    UnSQL.SQL.Add("  MAXTIME=[MAXTIME] AND  ");
    UnSQL.SQL.Add("  NORMALTIME=[NORMALTIME] AND  ");
    UnSQL.SQL.Add("  OPEARTIONM=[OPEARTIONM] AND  ");
    UnSQL.SQL.Add("  POSSIBLERETURNS=[POSSIBLERETURNS] AND  ");
    UnSQL.SQL.Add("  STEEPTYPEUSERENABLE=[STEEPTYPEUSERENABLE] AND  ");
    UnSQL.SQL.Add("  STEPVALIDATE=[STEPVALIDATE] AND  ");
    UnSQL.SQL.Add("  TIMEENABLE=[TIMEENABLE] AND  ");
    UnSQL.SQL.Add("  TITLEM=[TITLEM] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDMODELTYPED_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDMODELTYPED_DEL=function(MDMODELTYPED)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDMODELTYPED.FieldName, MDMODELTYPED.IDMDMODELTYPED, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDMODELTYPED.COMMENTSM.FieldName, MDMODELTYPED.COMMENTSM, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.MDMODELTYPED.GUIDETEXT.FieldName, MDMODELTYPED.GUIDETEXT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMODELTYPED.IDCMDBCIDEFINE.FieldName, MDMODELTYPED.IDCMDBCIDEFINE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDFUNCESC.FieldName, MDMODELTYPED.IDMDFUNCESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDHIERESC.FieldName, MDMODELTYPED.IDMDHIERESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDMODELTYPED1.FieldName, MDMODELTYPED.IDMDMODELTYPED1, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDSERVICETYPE.FieldName, MDMODELTYPED.IDMDSERVICETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMODELTYPED.MAXTIME.FieldName, MDMODELTYPED.MAXTIME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMODELTYPED.NORMALTIME.FieldName, MDMODELTYPED.NORMALTIME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDMODELTYPED.OPEARTIONM.FieldName, MDMODELTYPED.OPEARTIONM, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.MDMODELTYPED.POSSIBLERETURNS.FieldName, MDMODELTYPED.POSSIBLERETURNS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.MDMODELTYPED.STEEPTYPEUSERENABLE.FieldName, MDMODELTYPED.STEEPTYPEUSERENABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.MDMODELTYPED.STEPVALIDATE.FieldName, MDMODELTYPED.STEPVALIDATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.MDMODELTYPED.TIMEENABLE.FieldName, MDMODELTYPED.TIMEENABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDMODELTYPED.TITLEM.FieldName, MDMODELTYPED.TITLEM, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDMODELTYPED_DEL_2   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDMODELTYPED_DEL_2", "MDMODELTYPED_DEL_2 description.");
    UnSQL.SQL.Add(" DELETE MDMODELTYPED  ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  COMMENTSM=[COMMENTSM] AND  ");
    UnSQL.SQL.Add("  GUIDETEXT=[GUIDETEXT] AND  ");
    UnSQL.SQL.Add("  IDCMDBCIDEFINE=[IDCMDBCIDEFINE] AND  ");
    UnSQL.SQL.Add("  IDMDFUNCESC=[IDMDFUNCESC] AND  ");
    UnSQL.SQL.Add("  IDMDHIERESC=[IDMDHIERESC] AND  ");
    UnSQL.SQL.Add("  IDMDMODELTYPED=[IDMDMODELTYPED] AND  ");
    UnSQL.SQL.Add("  IDMDMODELTYPED1=[IDMDMODELTYPED1] AND  ");
    UnSQL.SQL.Add("  IDMDSERVICETYPE=[IDMDSERVICETYPE] AND  ");
    UnSQL.SQL.Add("  MAXTIME=[MAXTIME] AND  ");
    UnSQL.SQL.Add("  NORMALTIME=[NORMALTIME] AND  ");
    UnSQL.SQL.Add("  OPEARTIONM=[OPEARTIONM] AND  ");
    UnSQL.SQL.Add("  POSSIBLERETURNS=[POSSIBLERETURNS] AND  ");
    UnSQL.SQL.Add("  STEEPTYPEUSERENABLE=[STEEPTYPEUSERENABLE] AND  ");
    UnSQL.SQL.Add("  STEPVALIDATE=[STEPVALIDATE] AND  ");
    UnSQL.SQL.Add("  TIMEENABLE=[TIMEENABLE] AND  ");
    UnSQL.SQL.Add("  TITLEM=[TITLEM] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDMODELTYPED_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}

//DataSet Function 

//List Function 
Persistence.Model.Methods.MDMODELTYPED_ListSetID =function(MDMODELTYPEDList, IDMDMODELTYPED)
{
    for (i = 0; i < MDMODELTYPEDList.length; i++)
    {

        if (IDMDMODELTYPED == MDMODELTYPEDList[i].IDMDMODELTYPED)
            return (MDMODELTYPEDList[i]);

    } 
    
    var TMDMODELTYPED = new Persistence.Properties.TMDMODELTYPED();
    TMDMODELTYPED.IDMDMODELTYPED = IDMDMODELTYPED; 
    return (TMDMODELTYPED); 
}
Persistence.Model.Methods.MDMODELTYPED_ListAdd=function(MDMODELTYPEDList, MDMODELTYPED)
{
    var i = Persistence.Model.Methods.MDMODELTYPED_ListGetIndex(MDMODELTYPEDList, MDMODELTYPED);
    if (i == -1) MDMODELTYPEDList.push(MDMODELTYPED);
    else
    {
        MDMODELTYPEDList[i].COMMENTSM = MDMODELTYPED.COMMENTSM;
        MDMODELTYPEDList[i].GUIDETEXT = MDMODELTYPED.GUIDETEXT;
        MDMODELTYPEDList[i].IDCMDBCIDEFINE = MDMODELTYPED.IDCMDBCIDEFINE;
        MDMODELTYPEDList[i].IDMDFUNCESC = MDMODELTYPED.IDMDFUNCESC;
        MDMODELTYPEDList[i].IDMDHIERESC = MDMODELTYPED.IDMDHIERESC;
        MDMODELTYPEDList[i].IDMDMODELTYPED = MDMODELTYPED.IDMDMODELTYPED;
        MDMODELTYPEDList[i].IDMDMODELTYPED1 = MDMODELTYPED.IDMDMODELTYPED1;
        MDMODELTYPEDList[i].IDMDSERVICETYPE = MDMODELTYPED.IDMDSERVICETYPE;
        MDMODELTYPEDList[i].MAXTIME = MDMODELTYPED.MAXTIME;
        MDMODELTYPEDList[i].NORMALTIME = MDMODELTYPED.NORMALTIME;
        MDMODELTYPEDList[i].OPEARTIONM = MDMODELTYPED.OPEARTIONM;
        MDMODELTYPEDList[i].POSSIBLERETURNS = MDMODELTYPED.POSSIBLERETURNS;
        MDMODELTYPEDList[i].STEEPTYPEUSERENABLE = MDMODELTYPED.STEEPTYPEUSERENABLE;
        MDMODELTYPEDList[i].STEPVALIDATE = MDMODELTYPED.STEPVALIDATE;
        MDMODELTYPEDList[i].TIMEENABLE = MDMODELTYPED.TIMEENABLE;
        MDMODELTYPEDList[i].TITLEM = MDMODELTYPED.TITLEM;
    }
}
Persistence.Model.Methods.MDMODELTYPED_ListGetIndex=function(MDMODELTYPEDList, MDMODELTYPED)
{
    for (i = 0; i < MDMODELTYPEDList.length; i++)
    {
        if (MDMODELTYPED.IDMDMODELTYPED == MDMODELTYPEDList[i].IDMDMODELTYPED)
            return (i);
    }
    return (-1);
}
Persistence.Model.Methods.MDMODELTYPED_ListGetIndex=function(MDMODELTYPEDList, IDMDMODELTYPED)
{
    for (i = 0; i < MDMODELTYPEDList.length; i++)
    {
        if (IDMDMODELTYPED == MDMODELTYPEDList[i].IDMDMODELTYPED)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.Model.Methods.MDMODELTYPEDListtoStr=function(MDMODELTYPEDList)
{
    var Res = Persistence.Model.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(MDMODELTYPEDList.length, Longitud, Texto);
        for (Counter = 0; Counter < MDMODELTYPEDList.length; Counter++) {
            var UnMDMODELTYPED = MDMODELTYPEDList[Counter];
            UnMDMODELTYPED.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Model.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;  
}
Persistence.Model.Methods.StrtoMDMODELTYPED=function(ProtocoloStr, MDMODELTYPEDList)
{
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        MDMODELTYPEDList.length = 0;
        if (Persistence.Catalog.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnMDMODELTYPED = new Persistence.Demo.Properties.TMDMODELTYPED(); //Mode new row
                UnMDMODELTYPED.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.MDMODELTYPED_ListAdd(MDMODELTYPEDList, UnMDMODELTYPED);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
    
}

//Socket Function 
Persistence.Model.Methods.MDMODELTYPEDListToByte=function(MDMODELTYPEDList, MemStream,  idx)
{
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, MDMODELTYPEDList.length - idx);
    for (i = idx; i < MDMODELTYPEDList.length; i++)
    {
        MDMODELTYPEDList[i].ToByte(MemStream);
    }
}
Persistence.Model.Methods.ByteToMDMODELTYPEDList=function(MDMODELTYPEDList,  MemStream)
{
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++)
    {
       var UnMDMODELTYPED = new Persistence.Model.Properties.TMDMODELTYPED();
        UnMDMODELTYPED.ByteTo(MemStream);
        Persistence.Model.Methods.MDMODELTYPED_ListAdd(MDMODELTYPEDList, UnMDMODELTYPED);
    }
}
   
