﻿//TreduceDB
//Persistence.Model

Persistence.Model.TModelProfiler = function () {
    //como esta en la bdd
    this.ResErr = SysCfg.Error.Properties.TResErr();
    this.MDMODELTYPEDList = new Array();                 //Persistence.Model.Properties.TMDMODELTYPED  
    this.MDMATRIXMODELSList = new Array();               //Persistence.Model.Properties.TMDMATRIXMODELS
    this.MDCATEGORYList = new Array();                   //Persistence.Model.Properties.TMDCATEGORY
    this.MDCATEGORYDETAILList = new Array();             //Persistence.Model.Properties.TMDCATEGORYDETAIL
    this.MDCATEGORYDETAIL_TYPEUSERList = new Array();    //Persistence.Model.Properties.TMDCATEGORYDETAIL_TYPEUSER
    this.MDCONFIGList = new Array();                      //Persistence.Model.Properties.TMDCONFIG

    this.SDTYPEUSER_CATEGORYDETAILList = new Array();//virtual no existe la tabla//Persistence.Model.Properties.TSDTYPEUSER_CATEGORYDETAIL

    this.GETSDTYPEUSER_CATEGORYDETAILList = function (IDSDTYPEUSER) {
        //GETSDTYPEUSER_CATEGORYDETAILList((Int32)UsrCfg.SD.Properties.TSDTypeUser._Owner);
        var Res = new Properties.TSDTYPEUSER_CATEGORYDETAIL();
        for (var i = 0; i < SDTYPEUSER_CATEGORYDETAILList.length; i++) {
            if (SDTYPEUSER_CATEGORYDETAILList[i].IDSDTYPEUSER == IDSDTYPEUSER) {
                Res = SDTYPEUSER_CATEGORYDETAILList[i];
            }
        }
        return Res;
    }
    this.SeekSearchProfiler = new Array();    // ItHelpCenter.Componet.SeekSearch.TSeekSearchProfiler
    this.CATEGORYNODEAllList = new Array();   //Persistence.Model.Properties.TCATEGORYNODE
    this.CATEGORYNODEBeginList = new Array(); //Persistence.Model.Properties.TCATEGORYNODE



    this.Fill = function () {
        //llena las tablas 
        //Persistence.Model.Methods.MDMODELTYPED_Fill(MDMODELTYPEDList, "");//llena completa cuando la llenes la ordenes por IDMDMODELTYPED
        //Persistence.Model.Methods.MDMATRIXMODELS_Fill(MDMATRIXMODELSList, "");//llena completa ordenmas IDMDMODELTYPED
        //las relaciona 
        Persistence.Model.Methods.MDCATEGORY_Fill(this.MDCATEGORYList, "");
        Persistence.Model.Methods.MDCATEGORYDETAIL_Fill(this.MDCATEGORYDETAILList, "");
        Persistence.Model.Methods.MDCATEGORYDETAIL_TYPEUSER_Fill(this.MDCATEGORYDETAIL_TYPEUSERList, "", -1);
        Persistence.Model.Methods.MDCONFIG_Fill(this.MDCONFIGList, 1);

        Persistence.Model.Methods.StartReationModel(this);
        Persistence.Model.Methods.FillCATEGORYNODE(this.CATEGORYNODEAllList, this.CATEGORYNODEBeginList, this.MDCATEGORYList);
        //Persistence.Model.Methods.CATEGORYSTR(this);
        var SDTYPEUSER_CATEGORYDETAIL = Persistence.Model.Methods.SDTYPEUSER_CATEGORYDETAIL_ListSetID(this.SDTYPEUSER_CATEGORYDETAILList, 1);
        SysCfg.Log.Methods.WriteLog(SDTYPEUSER_CATEGORYDETAIL.MDCATEGORYDETAILList.length.toString(), null);
        ItHelpCenter.Componet.SeekSearch.Methods.FillSeekSearchByCategory(this.MDCATEGORYDETAILList, this.SeekSearchProfiler);

    }

    this.FillbyID = function () {
        /*
        //llena las tablas 
        Persistence.Model.Methods.MDMODELTYPED_Fill(this.MDMODELTYPEDList, "");//llena completa cuando la llenes la ordenes por IDMDMODELTYPED
        Persistence.Model.Methods.MDMATRIXMODELS_Fill(this.MDMATRIXMODELSList, "");//llena completa ordenmas IDMDMODELTYPED
        //las relaciona 
        Persistence.Model.Methods.StartReationModel(this);
        */
    }
}

