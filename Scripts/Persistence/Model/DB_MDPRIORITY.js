﻿//TreduceDB
//Persistence.Model.Properties
//Persistence.Model.Methods
 
Persistence.Model.Properties.TMDPRIORITY = function()
{
    this.IDMDPRIORITY = 0;
    this.PRIORITYNAME = "";
    //Socket IO Properties
    this.ToByte = function( MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDPRIORITY);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.PRIORITYNAME);
    }
    this.ByteTo = function( MemStream)
    {
        this.IDMDPRIORITY = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.PRIORITYNAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    }
    //Str IO Properties
    this.ToStr = function( Longitud,  Texto)
    {
        SysCfg.Str.Protocol.WriteInt(this.IDMDPRIORITY, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.PRIORITYNAME, Longitud, Texto);
    }
    this.StrTo = function(Pos, Index, LongitudArray, Texto)
    {
        this.IDMDPRIORITY = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.PRIORITYNAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
    }
}
 

//**********************   METHODS server  ********************************************************************************************
 
//DataSet Function 
Persistence.Model.Methods.MDPRIORITY_Fill=function(MDPRIORITYList, StrIDMDPRIORITY/* IDMDPRIORITY*/)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    MDPRIORITYList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDMDPRIORITY == "")
    {
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName, " = " + UsrCfg.InternoAtisNames.MDPRIORITY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else
    {
        StrIDMDPRIORITY = " IN (" + StrIDMDPRIORITY + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName, StrIDMDPRIORITY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //Param.AddInt32(UsrCfg.InternoDataLinkNames.MDPRIORITY.IDMDPRIORITY.FieldName, IDMDPRIORITY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try
    {
        /*

    //********   MDPRIORITY_GET   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDPRIORITY_GET", "MDPRIORITY_GET description.");
    UnSQL.SQL.Add(" SELECT  ");
    UnSQL.SQL.Add("  MDPRIORITY.IDMDPRIORITY, ");
    UnSQL.SQL.Add("  MDPRIORITY.PRIORITYNAME ");
    UnSQL.SQL.Add(" FROM  MDPRIORITY ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  MDPRIORITY.IDMDPRIORITY=[IDMDPRIORITY] and  ");
    UnSQL.SQL.Add("  MDPRIORITY.PRIORITYNAME=[PRIORITYNAME] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_MDPRIORITY = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDPRIORITY_GET", Param.ToBytes());
        ResErr = DS_MDPRIORITY.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDPRIORITY.DataSet.RecordCount > 0)
            {
                DS_MDPRIORITY.DataSet.First();
                while (!(DS_MDPRIORITY.DataSet.Eof))
                {
                    var MDPRIORITY = new Persistence.Model.Properties.TMDPRIORITY();
                    MDPRIORITY.IDMDPRIORITY = DS_MDPRIORITY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName).asInt32();
                    //Other
                    MDPRIORITY.PRIORITYNAME = DS_MDPRIORITY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDPRIORITY.PRIORITYNAME.FieldName).asString();
                    MDPRIORITYList.push(MDPRIORITY);

                    DS_MDPRIORITY.DataSet.Next();
                }
            }
            else
            {
                DS_MDPRIORITY.ResErr.NotError = false;
                DS_MDPRIORITY.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDPRIORITY_GETID=function(MDPRIORITY)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName, MDPRIORITY.IDMDPRIORITY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.MDPRIORITY.PRIORITYNAME.FieldName, MDPRIORITY.PRIORITYNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDPRIORITY_GETID   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDPRIORITY_GETID", "MDPRIORITY_GETID description.");
    UnSQL.SQL.Add(" SELECT  ");
    UnSQL.SQL.Add("  MDPRIORITY.IDMDPRIORITY, ");
    UnSQL.SQL.Add("  MDPRIORITY.PRIORITYNAME ");
    UnSQL.SQL.Add(" FROM  MDPRIORITY ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  MDPRIORITY.IDMDPRIORITY=[IDMDPRIORITY] and  ");
    UnSQL.SQL.Add("  MDPRIORITY.PRIORITYNAME=[PRIORITYNAME] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_MDPRIORITY = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDPRIORITY_GETID", Param.ToBytes());
        ResErr = DS_MDPRIORITY.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDPRIORITY.DataSet.RecordCount > 0)
            {
                DS_MDPRIORITY.DataSet.First();
                MDPRIORITY.IDMDPRIORITY = DS_MDPRIORITY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName).asInt32();
                //Other
                MDPRIORITY.PRIORITYNAME = DS_MDPRIORITY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDPRIORITY.PRIORITYNAME.FieldName).asString();
            }
            else
            {
                DS_MDPRIORITY.ResErr.NotError = false;
                DS_MDPRIORITY.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDPRIORITY_ADD=function(MDPRIORITY)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName, MDPRIORITY.IDMDPRIORITY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.MDPRIORITY.PRIORITYNAME.FieldName, MDPRIORITY.PRIORITYNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDPRIORITY_ADD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDPRIORITY_ADD", "MDPRIORITY_ADD description.");
    UnSQL.SQL.Add(" INSERT INTO MDPRIORITY( ");
    UnSQL.SQL.Add("  IDMDPRIORITY,  ");
    UnSQL.SQL.Add("  PRIORITYNAME ");
    UnSQL.SQL.Add(" )  ");
    UnSQL.SQL.Add(" VALUES ( ");
    UnSQL.SQL.Add("  [IDMDPRIORITY], ");
    UnSQL.SQL.Add("  [PRIORITYNAME] ");
    UnSQL.SQL.Add(" ) ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDPRIORITY_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            MDPRIORITY_GETID(MDPRIORITY);
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDPRIORITY_UPD=function( MDPRIORITY)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName, MDPRIORITY.IDMDPRIORITY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.MDPRIORITY.PRIORITYNAME.FieldName, MDPRIORITY.PRIORITYNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDPRIORITY_UPD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDPRIORITY_UPD", "MDPRIORITY_UPD description.");
    UnSQL.SQL.Add(" UPDATE MDPRIORITY SET ");
    UnSQL.SQL.Add("  IDMDPRIORITY=[IDMDPRIORITY],  ");
    UnSQL.SQL.Add("  PRIORITYNAME=[PRIORITYNAME] ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  IDMDPRIORITY=[IDMDPRIORITY] AND  ");
    UnSQL.SQL.Add("  PRIORITYNAME=[PRIORITYNAME] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDPRIORITY_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            //GET(); 
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDPRIORITY_DEL = function(/* StrIDMDPRIORITY*/ IDMDPRIORITY)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        /*if (StrIDMDPRIORITY == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName, " = " + UsrCfg.InternoAtisNames.MDPRIORITY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDMDPRIORITY = " IN (" + StrIDMDPRIORITY + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName, StrIDMDPRIORITY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName, IDMDPRIORITY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDPRIORITY_DEL_1   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDPRIORITY_DEL_1", "MDPRIORITY_DEL_1 description.");
    UnSQL.SQL.Add(" DELETE MDPRIORITY  ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  IDMDPRIORITY=[IDMDPRIORITY] AND  ");
    UnSQL.SQL.Add("  PRIORITYNAME=[PRIORITYNAME] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDPRIORITY_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDPRIORITY_DEL=function(MDPRIORITY)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName, MDPRIORITY.IDMDPRIORITY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.MDPRIORITY.PRIORITYNAME.FieldName, MDPRIORITY.PRIORITYNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDPRIORITY_DEL_2   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDPRIORITY_DEL_2", "MDPRIORITY_DEL_2 description.");
    UnSQL.SQL.Add(" DELETE MDPRIORITY  ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  IDMDPRIORITY=[IDMDPRIORITY] AND  ");
    UnSQL.SQL.Add("  PRIORITYNAME=[PRIORITYNAME] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDPRIORITY_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
    
//**********************   METHODS   ********************************************************************************************

    
//DataSet Function 

//List Function 
Persistence.Model.Methods.MDPRIORITY_ListSetID = function (MDPRIORITYList, IDMDPRIORITY)
{
    for (i = 0; i < MDPRIORITYList.length; i++)
    {

        if (IDMDPRIORITY == MDPRIORITYList[i].IDMDPRIORITY)
            return (MDPRIORITYList[i]);

    }
    
    var TMDPRIORITY = new Persistence.Properties.TMDPRIORITY();
    TMDPRIORITY.IDMDPRIORITY = IDMDPRIORITY; 
    return (TMDPRIORITY); 
}
Persistence.Model.Methods.MDPRIORITY_ListAdd=function(MDPRIORITYList,MDPRIORITY)
{
    var i = Persistence.Model.Methods.MDPRIORITY_ListGetIndex(MDPRIORITYList, MDPRIORITY);
    if (i == -1) MDPRIORITYList.push(MDPRIORITY);
    else
    {
        MDPRIORITYList[i].IDMDPRIORITY = MDPRIORITY.IDMDPRIORITY;
        MDPRIORITYList[i].PRIORITYNAME = MDPRIORITY.PRIORITYNAME;
    }
}
Persistence.Model.Methods.MDPRIORITY_ListGetIndex=function(MDPRIORITYList, MDPRIORITY)
{
    for (i = 0; i < MDPRIORITYList.length; i++)
    {
        if (MDPRIORITY.IDMDPRIORITY == MDPRIORITYList[i].IDMDPRIORITY)
            return (i);
    }
    return (-1);
}
Persistence.Model.Methods.MDPRIORITY_ListGetIndex=function(MDPRIORITYList, IDMDPRIORITY)
{
    for (i = 0; i < MDPRIORITYList.length; i++)
    {
        if (IDMDPRIORITY == MDPRIORITYList[i].IDMDPRIORITY)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.Model.Methods.MDPRIORITYListtoStr=function(MDPRIORITYList)
{
    var Res = Persistence.Model.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(MDPRIORITYList.length, Longitud, Texto);
        for (Counter = 0; Counter < MDPRIORITYList.length; Counter++) {
            var UnMDPRIORITY = MDPRIORITYList[Counter];
            UnMDPRIORITY.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Model.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.Model.Methods.StrtoMDPRIORITY = function( ProtocoloStr, MDPRIORITYList)
{
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        MDPRIORITYList.length = 0;
        if (Persistence.Model.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnMDPRIORITY = new Persistence.Demo.Properties.TMDPRIORITY(); //Mode new row
                UnMDPRIORITY.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.MDPRIORITY_ListAdd(MDPRIORITYList, UnMDPRIORITY);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
    
}

//Socket Function 
Persistence.Model.Methods.MDPRIORITYListToByte=function(MDPRIORITYList, MemStream)
{
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, MDPRIORITYList.length);
    for (i = 0; i < MDPRIORITYList.length; i++)
    {
        MDPRIORITYList[i].ToByte(MemStream);
    }
}
Persistence.Model.Methods.ByteToMDPRIORITYList=function(MDPRIORITYList, MemStream)
{
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++)
    {
        var UnMDPRIORITY = new Persistence.Model.Properties.TMDPRIORITY();
        MDPRIORITYList[i].ToByte(MemStream);
    }
}
 