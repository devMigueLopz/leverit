//TreduceDB
//Persistence.Model.Properties
//Persistence.Model.Methods

 
Persistence.Model.Properties.TMDLIFESTATUS = function()
{
    this.CAUTIONSTEP = "";
    this.COMMENTSST  = "";
    this.ENDSTEP  = "";
    this.IDMDLIFESTATUS  = 0;
    this.IDMDMODELTYPED  = 0;
    this.LS_IDSDCASESTATUS  = 0;
    this.NAMESTEP  = "";
    this.NEXTSTEP  = "";
    this.STATUSN  = 0;
    this.WARNINGSTEP = "";
    //Socket IO Properties
    this.ToByte = function(MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream,  this.CAUTIONSTEP);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream,  this.COMMENTSST);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream,  this.ENDSTEP);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream,  this.IDMDLIFESTATUS);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream,  this.IDMDMODELTYPED);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream,  this.LS_IDSDCASESTATUS);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream,  this.NAMESTEP);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream,  this.NEXTSTEP);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream,  this.STATUSN);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream,  this.WARNINGSTEP);
    }
    this.ByteTo = function(MemStream)
    {
        this.CAUTIONSTEP = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.COMMENTSST = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.ENDSTEP = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDMDLIFESTATUS = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDMODELTYPED = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.LS_IDSDCASESTATUS = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.NAMESTEP = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.NEXTSTEP = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.STATUSN = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.WARNINGSTEP = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    }
    //Str IO Properties
    this.ToStr = function(Longitud, Texto)
    {
        SysCfg.Str.Protocol.WriteStr( this.CAUTIONSTEP, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr( this.COMMENTSST, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr( this.ENDSTEP, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt( this.IDMDLIFESTATUS, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt( this.IDMDMODELTYPED, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt( this.LS_IDSDCASESTATUS, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr( this.NAMESTEP, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr( this.NEXTSTEP, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt( this.STATUSN, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr( this.WARNINGSTEP, Longitud, Texto);
    }
    this.StrTo = function(Pos, Index, LongitudArray, Texto)
    {
        this.CAUTIONSTEP = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.COMMENTSST = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.ENDSTEP = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.IDMDLIFESTATUS = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDMODELTYPED = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.LS_IDSDCASESTATUS = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.NAMESTEP = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.NEXTSTEP = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.STATUSN = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.WARNINGSTEP = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
    }
}
     

//**********************   METHODS server  ********************************************************************************************

    
//DataSet Function 
Persistence.Model.Methods.MDLIFESTATUS_Fill=function(MDLIFESTATUSList, StrIDMDLIFESTATUS/* IDMDLIFESTATUS*/)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    MDLIFESTATUSList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDMDLIFESTATUS == "")
    {
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDLIFESTATUS.IDMDLIFESTATUS.FieldName, " = " + UsrCfg.InternoAtisNames.MDLIFESTATUS.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDLIFESTATUS.IDMDLIFESTATUS.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else
    {
        StrIDMDLIFESTATUS = " IN (" + StrIDMDLIFESTATUS + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDLIFESTATUS.IDMDLIFESTATUS.FieldName, StrIDMDLIFESTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //Param.AddInt32(UsrCfg.InternoDataLinkNames.MDLIFESTATUS.IDMDLIFESTATUS.FieldName, IDMDLIFESTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try
    {
        /*

    //********   MDLIFESTATUS_GET   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDLIFESTATUS_GET", "MDLIFESTATUS_GET description.");
    UnSQL.SQL.Add(" SELECT  ");
    UnSQL.SQL.Add("  MDLIFESTATUS.CAUTIONSTEP, ");
    UnSQL.SQL.Add("  MDLIFESTATUS.COMMENTSST, ");
    UnSQL.SQL.Add("  MDLIFESTATUS.ENDSTEP, ");
    UnSQL.SQL.Add("  MDLIFESTATUS.IDMDLIFESTATUS, ");
    UnSQL.SQL.Add("  MDLIFESTATUS.IDMDMODELTYPED, ");
    UnSQL.SQL.Add("  MDLIFESTATUS.LS_IDSDCASESTATUS, ");
    UnSQL.SQL.Add("  MDLIFESTATUS.NAMESTEP, ");
    UnSQL.SQL.Add("  MDLIFESTATUS.NEXTSTEP, ");
    UnSQL.SQL.Add("  MDLIFESTATUS.STATUSN, ");
    UnSQL.SQL.Add("  MDLIFESTATUS.WARNINGSTEP ");
    UnSQL.SQL.Add(" FROM  MDLIFESTATUS ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  MDLIFESTATUS.CAUTIONSTEP=[CAUTIONSTEP] and  ");
    UnSQL.SQL.Add("  MDLIFESTATUS.COMMENTSST=[COMMENTSST] and  ");
    UnSQL.SQL.Add("  MDLIFESTATUS.ENDSTEP=[ENDSTEP] and  ");
    UnSQL.SQL.Add("  MDLIFESTATUS.IDMDLIFESTATUS=[IDMDLIFESTATUS] and  ");
    UnSQL.SQL.Add("  MDLIFESTATUS.IDMDMODELTYPED=[IDMDMODELTYPED] and  ");
    UnSQL.SQL.Add("  MDLIFESTATUS.LS_IDSDCASESTATUS=[LS_IDSDCASESTATUS] and  ");
    UnSQL.SQL.Add("  MDLIFESTATUS.NAMESTEP=[NAMESTEP] and  ");
    UnSQL.SQL.Add("  MDLIFESTATUS.NEXTSTEP=[NEXTSTEP] and  ");
    UnSQL.SQL.Add("  MDLIFESTATUS.STATUSN=[STATUSN] and  ");
    UnSQL.SQL.Add("  MDLIFESTATUS.WARNINGSTEP=[WARNINGSTEP] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_MDLIFESTATUS = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDLIFESTATUS_GET", Param.ToBytes());
        ResErr = DS_MDLIFESTATUS.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDLIFESTATUS.DataSet.RecordCount > 0)
            {
                DS_MDLIFESTATUS.DataSet.First();
                while (!(DS_MDLIFESTATUS.DataSet.Eof))
                {
                    var MDLIFESTATUS = new Persistence.Model.Properties.TMDLIFESTATUS();
                    MDLIFESTATUS.IDMDLIFESTATUS = DS_MDLIFESTATUS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDLIFESTATUS.IDMDLIFESTATUS.FieldName).asInt32();
                    //Other
                    MDLIFESTATUS.ENDSTEP = DS_MDLIFESTATUS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDLIFESTATUS.ENDSTEP.FieldName).asString();
                    MDLIFESTATUS.IDMDMODELTYPED = DS_MDLIFESTATUS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDLIFESTATUS.IDMDMODELTYPED.FieldName).asInt32();
                    MDLIFESTATUS.LS_IDSDCASESTATUS = DS_MDLIFESTATUS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDLIFESTATUS.LS_IDSDCASESTATUS.FieldName).asInt32();
                    MDLIFESTATUS.NAMESTEP = DS_MDLIFESTATUS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDLIFESTATUS.NAMESTEP.FieldName).asString();
                    MDLIFESTATUS.NEXTSTEP = DS_MDLIFESTATUS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDLIFESTATUS.NEXTSTEP.FieldName).asString();
                    MDLIFESTATUS.STATUSN = DS_MDLIFESTATUS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDLIFESTATUS.STATUSN.FieldName).asInt32();
                    MDLIFESTATUSList.push(MDLIFESTATUS);

                    DS_MDLIFESTATUS.DataSet.Next();
                }
            }
            else
            {
                DS_MDLIFESTATUS.ResErr.NotError = false;
                DS_MDLIFESTATUS.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDLIFESTATUS_GETID=function( MDLIFESTATUS)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDLIFESTATUS.IDMDLIFESTATUS.FieldName, MDLIFESTATUS.IDMDLIFESTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDLIFESTATUS.CAUTIONSTEP.FieldName, MDLIFESTATUS.CAUTIONSTEP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.MDLIFESTATUS.COMMENTSST.FieldName, MDLIFESTATUS.COMMENTSST, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDLIFESTATUS.ENDSTEP.FieldName, MDLIFESTATUS.ENDSTEP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDLIFESTATUS.IDMDMODELTYPED.FieldName, MDLIFESTATUS.IDMDMODELTYPED, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDLIFESTATUS.LS_IDSDCASESTATUS.FieldName, MDLIFESTATUS.LS_IDSDCASESTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDLIFESTATUS.NAMESTEP.FieldName, MDLIFESTATUS.NAMESTEP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDLIFESTATUS.NEXTSTEP.FieldName, MDLIFESTATUS.NEXTSTEP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDLIFESTATUS.STATUSN.FieldName, MDLIFESTATUS.STATUSN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.MDLIFESTATUS.WARNINGSTEP.FieldName, MDLIFESTATUS.WARNINGSTEP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDLIFESTATUS_GETID   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDLIFESTATUS_GETID", "MDLIFESTATUS_GETID description.");
    UnSQL.SQL.Add(" SELECT  ");
    UnSQL.SQL.Add("  MDLIFESTATUS.CAUTIONSTEP, ");
    UnSQL.SQL.Add("  MDLIFESTATUS.COMMENTSST, ");
    UnSQL.SQL.Add("  MDLIFESTATUS.ENDSTEP, ");
    UnSQL.SQL.Add("  MDLIFESTATUS.IDMDLIFESTATUS, ");
    UnSQL.SQL.Add("  MDLIFESTATUS.IDMDMODELTYPED, ");
    UnSQL.SQL.Add("  MDLIFESTATUS.LS_IDSDCASESTATUS, ");
    UnSQL.SQL.Add("  MDLIFESTATUS.NAMESTEP, ");
    UnSQL.SQL.Add("  MDLIFESTATUS.NEXTSTEP, ");
    UnSQL.SQL.Add("  MDLIFESTATUS.STATUSN, ");
    UnSQL.SQL.Add("  MDLIFESTATUS.WARNINGSTEP ");
    UnSQL.SQL.Add(" FROM  MDLIFESTATUS ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  MDLIFESTATUS.CAUTIONSTEP=[CAUTIONSTEP] and  ");
    UnSQL.SQL.Add("  MDLIFESTATUS.COMMENTSST=[COMMENTSST] and  ");
    UnSQL.SQL.Add("  MDLIFESTATUS.ENDSTEP=[ENDSTEP] and  ");
    UnSQL.SQL.Add("  MDLIFESTATUS.IDMDLIFESTATUS=[IDMDLIFESTATUS] and  ");
    UnSQL.SQL.Add("  MDLIFESTATUS.IDMDMODELTYPED=[IDMDMODELTYPED] and  ");
    UnSQL.SQL.Add("  MDLIFESTATUS.LS_IDSDCASESTATUS=[LS_IDSDCASESTATUS] and  ");
    UnSQL.SQL.Add("  MDLIFESTATUS.NAMESTEP=[NAMESTEP] and  ");
    UnSQL.SQL.Add("  MDLIFESTATUS.NEXTSTEP=[NEXTSTEP] and  ");
    UnSQL.SQL.Add("  MDLIFESTATUS.STATUSN=[STATUSN] and  ");
    UnSQL.SQL.Add("  MDLIFESTATUS.WARNINGSTEP=[WARNINGSTEP] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_MDLIFESTATUS = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDLIFESTATUS_GETID", Param.ToBytes());
        ResErr = DS_MDLIFESTATUS.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDLIFESTATUS.DataSet.RecordCount > 0)
            {
                DS_MDLIFESTATUS.DataSet.First();
                MDLIFESTATUS.IDMDLIFESTATUS = DS_MDLIFESTATUS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDLIFESTATUS.IDMDLIFESTATUS.FieldName).asInt32();
                //Other
                MDLIFESTATUS.ENDSTEP = DS_MDLIFESTATUS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDLIFESTATUS.ENDSTEP.FieldName).asString();
                MDLIFESTATUS.IDMDMODELTYPED = DS_MDLIFESTATUS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDLIFESTATUS.IDMDMODELTYPED.FieldName).asInt32();
                MDLIFESTATUS.LS_IDSDCASESTATUS = DS_MDLIFESTATUS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDLIFESTATUS.LS_IDSDCASESTATUS.FieldName).asInt32();
                MDLIFESTATUS.NAMESTEP = DS_MDLIFESTATUS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDLIFESTATUS.NAMESTEP.FieldName).asString();
                MDLIFESTATUS.NEXTSTEP = DS_MDLIFESTATUS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDLIFESTATUS.NEXTSTEP.FieldName).asString();
                MDLIFESTATUS.STATUSN = DS_MDLIFESTATUS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDLIFESTATUS.STATUSN.FieldName).asInt32();
            }
            else
            {
                DS_MDLIFESTATUS.ResErr.NotError = false;
                DS_MDLIFESTATUS.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDLIFESTATUS_ADD=function( MDLIFESTATUS)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDLIFESTATUS.IDMDLIFESTATUS.FieldName, MDLIFESTATUS.IDMDLIFESTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDLIFESTATUS.CAUTIONSTEP.FieldName, MDLIFESTATUS.CAUTIONSTEP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.MDLIFESTATUS.COMMENTSST.FieldName, MDLIFESTATUS.COMMENTSST, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDLIFESTATUS.ENDSTEP.FieldName, MDLIFESTATUS.ENDSTEP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDLIFESTATUS.IDMDMODELTYPED.FieldName, MDLIFESTATUS.IDMDMODELTYPED, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDLIFESTATUS.LS_IDSDCASESTATUS.FieldName, MDLIFESTATUS.LS_IDSDCASESTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDLIFESTATUS.NAMESTEP.FieldName, MDLIFESTATUS.NAMESTEP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDLIFESTATUS.NEXTSTEP.FieldName, MDLIFESTATUS.NEXTSTEP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDLIFESTATUS.STATUSN.FieldName, MDLIFESTATUS.STATUSN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.MDLIFESTATUS.WARNINGSTEP.FieldName, MDLIFESTATUS.WARNINGSTEP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDLIFESTATUS_ADD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDLIFESTATUS_ADD", "MDLIFESTATUS_ADD description.");
    UnSQL.SQL.Add(" INSERT INTO MDLIFESTATUS( ");
    UnSQL.SQL.Add("  CAUTIONSTEP,  ");
    UnSQL.SQL.Add("  COMMENTSST,  ");
    UnSQL.SQL.Add("  ENDSTEP,  ");
    UnSQL.SQL.Add("  IDMDLIFESTATUS,  ");
    UnSQL.SQL.Add("  IDMDMODELTYPED,  ");
    UnSQL.SQL.Add("  LS_IDSDCASESTATUS,  ");
    UnSQL.SQL.Add("  NAMESTEP,  ");
    UnSQL.SQL.Add("  NEXTSTEP,  ");
    UnSQL.SQL.Add("  STATUSN,  ");
    UnSQL.SQL.Add("  WARNINGSTEP ");
    UnSQL.SQL.Add(" )  ");
    UnSQL.SQL.Add(" VALUES ( ");
    UnSQL.SQL.Add("  [CAUTIONSTEP], ");
    UnSQL.SQL.Add("  [COMMENTSST], ");
    UnSQL.SQL.Add("  [ENDSTEP], ");
    UnSQL.SQL.Add("  [IDMDLIFESTATUS], ");
    UnSQL.SQL.Add("  [IDMDMODELTYPED], ");
    UnSQL.SQL.Add("  [LS_IDSDCASESTATUS], ");
    UnSQL.SQL.Add("  [NAMESTEP], ");
    UnSQL.SQL.Add("  [NEXTSTEP], ");
    UnSQL.SQL.Add("  [STATUSN], ");
    UnSQL.SQL.Add("  [WARNINGSTEP] ");
    UnSQL.SQL.Add(" ) ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDLIFESTATUS_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            MDLIFESTATUS_GETID(MDLIFESTATUS);
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDLIFESTATUS_UPD=function(MDLIFESTATUS)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDLIFESTATUS.IDMDLIFESTATUS.FieldName, MDLIFESTATUS.IDMDLIFESTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDLIFESTATUS.CAUTIONSTEP.FieldName, MDLIFESTATUS.CAUTIONSTEP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.MDLIFESTATUS.COMMENTSST.FieldName, MDLIFESTATUS.COMMENTSST, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDLIFESTATUS.ENDSTEP.FieldName, MDLIFESTATUS.ENDSTEP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDLIFESTATUS.IDMDMODELTYPED.FieldName, MDLIFESTATUS.IDMDMODELTYPED, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDLIFESTATUS.LS_IDSDCASESTATUS.FieldName, MDLIFESTATUS.LS_IDSDCASESTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDLIFESTATUS.NAMESTEP.FieldName, MDLIFESTATUS.NAMESTEP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDLIFESTATUS.NEXTSTEP.FieldName, MDLIFESTATUS.NEXTSTEP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDLIFESTATUS.STATUSN.FieldName, MDLIFESTATUS.STATUSN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.MDLIFESTATUS.WARNINGSTEP.FieldName, MDLIFESTATUS.WARNINGSTEP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDLIFESTATUS_UPD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDLIFESTATUS_UPD", "MDLIFESTATUS_UPD description.");
    UnSQL.SQL.Add(" UPDATE MDLIFESTATUS SET ");
    UnSQL.SQL.Add("  CAUTIONSTEP=[CAUTIONSTEP],  ");
    UnSQL.SQL.Add("  COMMENTSST=[COMMENTSST],  ");
    UnSQL.SQL.Add("  ENDSTEP=[ENDSTEP],  ");
    UnSQL.SQL.Add("  IDMDLIFESTATUS=[IDMDLIFESTATUS],  ");
    UnSQL.SQL.Add("  IDMDMODELTYPED=[IDMDMODELTYPED],  ");
    UnSQL.SQL.Add("  LS_IDSDCASESTATUS=[LS_IDSDCASESTATUS],  ");
    UnSQL.SQL.Add("  NAMESTEP=[NAMESTEP],  ");
    UnSQL.SQL.Add("  NEXTSTEP=[NEXTSTEP],  ");
    UnSQL.SQL.Add("  STATUSN=[STATUSN],  ");
    UnSQL.SQL.Add("  WARNINGSTEP=[WARNINGSTEP] ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  CAUTIONSTEP=[CAUTIONSTEP] AND  ");
    UnSQL.SQL.Add("  COMMENTSST=[COMMENTSST] AND  ");
    UnSQL.SQL.Add("  ENDSTEP=[ENDSTEP] AND  ");
    UnSQL.SQL.Add("  IDMDLIFESTATUS=[IDMDLIFESTATUS] AND  ");
    UnSQL.SQL.Add("  IDMDMODELTYPED=[IDMDMODELTYPED] AND  ");
    UnSQL.SQL.Add("  LS_IDSDCASESTATUS=[LS_IDSDCASESTATUS] AND  ");
    UnSQL.SQL.Add("  NAMESTEP=[NAMESTEP] AND  ");
    UnSQL.SQL.Add("  NEXTSTEP=[NEXTSTEP] AND  ");
    UnSQL.SQL.Add("  STATUSN=[STATUSN] AND  ");
    UnSQL.SQL.Add("  WARNINGSTEP=[WARNINGSTEP] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDLIFESTATUS_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            //GET(); 
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDLIFESTATUS_DEL=function(/* StrIDMDLIFESTATUS*/ IDMDLIFESTATUS)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        /*if (StrIDMDLIFESTATUS == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDLIFESTATUS.IDMDLIFESTATUS.FieldName, " = " + UsrCfg.InternoAtisNames.MDLIFESTATUS.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDLIFESTATUS.IDMDLIFESTATUS.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDMDLIFESTATUS = " IN (" + StrIDMDLIFESTATUS + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDLIFESTATUS.IDMDLIFESTATUS.FieldName, StrIDMDLIFESTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.MDLIFESTATUS.IDMDLIFESTATUS.FieldName, IDMDLIFESTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDLIFESTATUS_DEL_1   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDLIFESTATUS_DEL_1", "MDLIFESTATUS_DEL_1 description.");
    UnSQL.SQL.Add(" DELETE MDLIFESTATUS  ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  CAUTIONSTEP=[CAUTIONSTEP] AND  ");
    UnSQL.SQL.Add("  COMMENTSST=[COMMENTSST] AND  ");
    UnSQL.SQL.Add("  ENDSTEP=[ENDSTEP] AND  ");
    UnSQL.SQL.Add("  IDMDLIFESTATUS=[IDMDLIFESTATUS] AND  ");
    UnSQL.SQL.Add("  IDMDMODELTYPED=[IDMDMODELTYPED] AND  ");
    UnSQL.SQL.Add("  LS_IDSDCASESTATUS=[LS_IDSDCASESTATUS] AND  ");
    UnSQL.SQL.Add("  NAMESTEP=[NAMESTEP] AND  ");
    UnSQL.SQL.Add("  NEXTSTEP=[NEXTSTEP] AND  ");
    UnSQL.SQL.Add("  STATUSN=[STATUSN] AND  ");
    UnSQL.SQL.Add("  WARNINGSTEP=[WARNINGSTEP] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDLIFESTATUS_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDLIFESTATUS_DEL=function(MDLIFESTATUS)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDLIFESTATUS.IDMDLIFESTATUS.FieldName, MDLIFESTATUS.IDMDLIFESTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDLIFESTATUS.CAUTIONSTEP.FieldName, MDLIFESTATUS.CAUTIONSTEP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.MDLIFESTATUS.COMMENTSST.FieldName, MDLIFESTATUS.COMMENTSST, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDLIFESTATUS.ENDSTEP.FieldName, MDLIFESTATUS.ENDSTEP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDLIFESTATUS.IDMDMODELTYPED.FieldName, MDLIFESTATUS.IDMDMODELTYPED, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDLIFESTATUS.LS_IDSDCASESTATUS.FieldName, MDLIFESTATUS.LS_IDSDCASESTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDLIFESTATUS.NAMESTEP.FieldName, MDLIFESTATUS.NAMESTEP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDLIFESTATUS.NEXTSTEP.FieldName, MDLIFESTATUS.NEXTSTEP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDLIFESTATUS.STATUSN.FieldName, MDLIFESTATUS.STATUSN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.MDLIFESTATUS.WARNINGSTEP.FieldName, MDLIFESTATUS.WARNINGSTEP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDLIFESTATUS_DEL_2   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDLIFESTATUS_DEL_2", "MDLIFESTATUS_DEL_2 description.");
    UnSQL.SQL.Add(" DELETE MDLIFESTATUS  ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  CAUTIONSTEP=[CAUTIONSTEP] AND  ");
    UnSQL.SQL.Add("  COMMENTSST=[COMMENTSST] AND  ");
    UnSQL.SQL.Add("  ENDSTEP=[ENDSTEP] AND  ");
    UnSQL.SQL.Add("  IDMDLIFESTATUS=[IDMDLIFESTATUS] AND  ");
    UnSQL.SQL.Add("  IDMDMODELTYPED=[IDMDMODELTYPED] AND  ");
    UnSQL.SQL.Add("  LS_IDSDCASESTATUS=[LS_IDSDCASESTATUS] AND  ");
    UnSQL.SQL.Add("  NAMESTEP=[NAMESTEP] AND  ");
    UnSQL.SQL.Add("  NEXTSTEP=[NEXTSTEP] AND  ");
    UnSQL.SQL.Add("  STATUSN=[STATUSN] AND  ");
    UnSQL.SQL.Add("  WARNINGSTEP=[WARNINGSTEP] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDLIFESTATUS_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
     

//**********************   METHODS   ********************************************************************************************

  
//DataSet Function 

//List Function 
Persistence.Model.Methods.MDLIFESTATUS_ListSetID=function(MDLIFESTATUSList, IDMDLIFESTATUS)
{
    for (i = 0; i < MDLIFESTATUSList.length; i++)
    {

        if (IDMDLIFESTATUS == MDLIFESTATUSList[i].IDMDLIFESTATUS)
            return (MDLIFESTATUSList[i]);

    }
     
    var TMDLIFESTATUS = new Persistence.Properties.TMDLIFESTATUS();
    TMDLIFESTATUS.IDMDLIFESTATUS = IDMDLIFESTATUS; 
    return (TMDLIFESTATUS); 
}
Persistence.Model.Methods.MDLIFESTATUS_ListAdd=function(MDLIFESTATUSList, MDLIFESTATUS)
{
    var i = Persistence.Model.Methods.MDLIFESTATUS_ListGetIndex(MDLIFESTATUSList, MDLIFESTATUS);
    if (i == -1) MDLIFESTATUSList.push(MDLIFESTATUS);
    else
    {
        MDLIFESTATUSList[i].CAUTIONSTEP = MDLIFESTATUS.CAUTIONSTEP;
        MDLIFESTATUSList[i].COMMENTSST = MDLIFESTATUS.COMMENTSST;
        MDLIFESTATUSList[i].ENDSTEP = MDLIFESTATUS.ENDSTEP;
        MDLIFESTATUSList[i].IDMDLIFESTATUS = MDLIFESTATUS.IDMDLIFESTATUS;
        MDLIFESTATUSList[i].IDMDMODELTYPED = MDLIFESTATUS.IDMDMODELTYPED;
        MDLIFESTATUSList[i].LS_IDSDCASESTATUS = MDLIFESTATUS.LS_IDSDCASESTATUS;
        MDLIFESTATUSList[i].NAMESTEP = MDLIFESTATUS.NAMESTEP;
        MDLIFESTATUSList[i].NEXTSTEP = MDLIFESTATUS.NEXTSTEP;
        MDLIFESTATUSList[i].STATUSN = MDLIFESTATUS.STATUSN;
        MDLIFESTATUSList[i].WARNINGSTEP = MDLIFESTATUS.WARNINGSTEP;
    }
}
Persistence.Model.Methods.MDLIFESTATUS_ListGetIndex=function(MDLIFESTATUSList, MDLIFESTATUS)
{
    for (i = 0; i < MDLIFESTATUSList.length; i++)
    {
        if (MDLIFESTATUS.IDMDLIFESTATUS == MDLIFESTATUSList[i].IDMDLIFESTATUS)
            return (i);
    }
    return (-1);
}
Persistence.Model.Methods.MDLIFESTATUS_ListGetIndex=function(MDLIFESTATUSList, IDMDLIFESTATUS)
{
    for (i = 0; i < MDLIFESTATUSList.length; i++)
    {
        if (IDMDLIFESTATUS == MDLIFESTATUSList[i].IDMDLIFESTATUS)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.Model.Methods.MDLIFESTATUSListtoStr=function(MDLIFESTATUSList)
{
    var Res = Persistence.Model.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(MDLIFESTATUSList.length, Longitud, Texto);
        for (Counter = 0; Counter < MDLIFESTATUSList.length; Counter++) {
            var UnMDLIFESTATUS = MDLIFESTATUSList[Counter];
            UnMDLIFESTATUS.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Model.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.Model.Methods.StrtoMDLIFESTATUS=function(ProtocoloStr, MDLIFESTATUSList)
{
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        MDLIFESTATUSList.length = 0;
        if (Persistence.Model.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnMDLIFESTATUS = new Persistence.Demo.Properties.TMDLIFESTATUS(); //Mode new row
                UnMDLIFESTATUS.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.MDLIFESTATUS_ListAdd(MDLIFESTATUSList, UnMDLIFESTATUS);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
    
}

//Socket Function 
Persistence.Model.Methods.MDLIFESTATUSListToByte=function(MDLIFESTATUSList, MemStream)
{
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, MDLIFESTATUSList.length);
    for (i = 0; i < MDLIFESTATUSList.length; i++)
    {
        MDLIFESTATUSList[i].ToByte(MemStream);
    }
}
Persistence.Model.Methods.ByteToMDLIFESTATUSList=function(MDLIFESTATUSList, MemStream)
{
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++)
    {
        var UnMDLIFESTATUS = new Persistence.Model.Properties.TMDLIFESTATUS();
        MDLIFESTATUSList[i].ToByte(MemStream);
    }
}
 