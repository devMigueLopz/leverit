﻿//TreduceDB
//Persistence.Model.Properties
//Persistence.Model.Methods

 
 
Persistence.Model.Properties.TMDFUNCPER= function()
{
    this. COMMENTSF = "";
    this.FUNLAVEL = 0;
    this.IDMDFUNCESC = 0;
    this.IDMDFUNCPER = 0;
    this.IDMDGROUP = 0;
    this.PERCF = 0;
    this.TOTAL = 0;
    //Socket IO Properties
    this.ToByte = function (MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.COMMENTSF);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.FUNLAVEL);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDFUNCESC);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDFUNCPER);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDGROUP);
        SysCfg.Stream.Methods.WriteStreamDouble(MemStream, this.PERCF);
        SysCfg.Stream.Methods.WriteStreamDouble(MemStream, this.TOTAL);
    }
    this.ByteTo = function (MemStream)
    {
        this.COMMENTSF = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.FUNLAVEL = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDFUNCESC = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDFUNCPER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDGROUP = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.PERCF = SysCfg.Stream.Methods.ReadStreamDouble(MemStream);
        this.TOTAL = SysCfg.Stream.Methods.ReadStreamDouble(MemStream);
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto)
    {
        SysCfg.Str.Protocol.WriteStr(this.COMMENTSF, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.FUNLAVEL, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDFUNCESC, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDFUNCPER, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDGROUP, Longitud, Texto);
        SysCfg.Str.Protocol.WriteDouble(this.PERCF, Longitud, Texto);
        SysCfg.Str.Protocol.WriteDouble(this.TOTAL, Longitud, Texto);
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto)
    {
        this.COMMENTSF = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.FUNLAVEL = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDFUNCESC = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDFUNCPER = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDGROUP = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.PERCF = SysCfg.Str.Protocol.ReadDouble(Pos, Index, LongitudArray, Texto);
        this.TOTAL = SysCfg.Str.Protocol.ReadDouble(Pos, Index, LongitudArray, Texto);
    }
}
     

//**********************   METHODS server  ********************************************************************************************
    
//DataSet Function 
Persistence.Model.Methods.MDFUNCPER_Fill=function(MDFUNCPERList,  StrIDMDFUNCPER/* IDMDFUNCPER*/)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    MDFUNCPERList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDMDFUNCPER == "")
    {
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCPER.FieldName, " = " + UsrCfg.InternoAtisNames.MDFUNCPER.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCPER.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else
    {
        StrIDMDFUNCPER = " IN (" + StrIDMDFUNCPER + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCPER.FieldName, StrIDMDFUNCPER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //Param.AddInt32(UsrCfg.InternoDataLinkNames.MDFUNCPER.IDMDFUNCPER.FieldName, IDMDFUNCPER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try
    {
        /*

    //********   MDFUNCPER_GET   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDFUNCPER_GET", "MDFUNCPER_GET description.");
    UnSQL.SQL.Add(" SELECT  ");
    UnSQL.SQL.Add("  MDFUNCPER.COMMENTSF, ");
    UnSQL.SQL.Add("  MDFUNCPER.FUNLAVEL, ");
    UnSQL.SQL.Add("  MDFUNCPER.IDMDFUNCESC, ");
    UnSQL.SQL.Add("  MDFUNCPER.IDMDFUNCPER, ");
    UnSQL.SQL.Add("  MDFUNCPER.IDMDGROUP, ");
    UnSQL.SQL.Add("  MDFUNCPER.PERCF, ");
    UnSQL.SQL.Add("  MDFUNCPER.TOTAL ");
    UnSQL.SQL.Add(" FROM  MDFUNCPER ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  MDFUNCPER.COMMENTSF=[COMMENTSF] and  ");
    UnSQL.SQL.Add("  MDFUNCPER.FUNLAVEL=[FUNLAVEL] and  ");
    UnSQL.SQL.Add("  MDFUNCPER.IDMDFUNCESC=[IDMDFUNCESC] and  ");
    UnSQL.SQL.Add("  MDFUNCPER.IDMDFUNCPER=[IDMDFUNCPER] and  ");
    UnSQL.SQL.Add("  MDFUNCPER.IDMDGROUP=[IDMDGROUP] and  ");
    UnSQL.SQL.Add("  MDFUNCPER.PERCF=[PERCF] and  ");
    UnSQL.SQL.Add("  MDFUNCPER.TOTAL=[TOTAL] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_MDFUNCPER = SysCfg.DB.SQL.Methods.OpenDataSet("DataLink", "MDFUNCPER_GET", Param.ToBytes());
        ResErr = DS_MDFUNCPER.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDFUNCPER.DataSet.RecordCount > 0)
            {
                DS_MDFUNCPER.DataSet.First();
                while (!(DS_MDFUNCPER.DataSet.Eof))
                {
                    var MDFUNCPER = new Persistence.Model.Properties.TMDFUNCPER();
                    MDFUNCPER.IDMDFUNCPER = DS_MDFUNCPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCPER.FieldName).asInt32();
                    //Other
                    MDFUNCPER.FUNLAVEL = DS_MDFUNCPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDFUNCPER.FUNLAVEL.FieldName).asInt32();
                    MDFUNCPER.IDMDFUNCESC = DS_MDFUNCPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCESC.FieldName).asInt32();
                    MDFUNCPER.IDMDGROUP = DS_MDFUNCPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDGROUP.FieldName).asInt32();
                    MDFUNCPER.PERCF = DS_MDFUNCPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDFUNCPER.PERCF.FieldName).asDouble();
                    MDFUNCPER.TOTAL = DS_MDFUNCPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDFUNCPER.TOTAL.FieldName).asDouble();
                    MDFUNCPERList.push(MDFUNCPER);

                    DS_MDFUNCPER.DataSet.Next();
                }
            }
            else
            {
                DS_MDFUNCPER.ResErr.NotError = false;
                DS_MDFUNCPER.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDFUNCPER_GETID=function( MDFUNCPER)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCPER.FieldName, MDFUNCPER.IDMDFUNCPER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDFUNCPER.COMMENTSF.FieldName, MDFUNCPER.COMMENTSF, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCPER.FUNLAVEL.FieldName, MDFUNCPER.FUNLAVEL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCESC.FieldName, MDFUNCPER.IDMDFUNCESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDGROUP.FieldName, MDFUNCPER.IDMDGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDouble(UsrCfg.InternoAtisNames.MDFUNCPER.PERCF.FieldName, MDFUNCPER.PERCF, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDouble(UsrCfg.InternoAtisNames.MDFUNCPER.TOTAL.FieldName, MDFUNCPER.TOTAL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDFUNCPER_GETID   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDFUNCPER_GETID", "MDFUNCPER_GETID description.");
    UnSQL.SQL.Add(" SELECT  ");
    UnSQL.SQL.Add("  MDFUNCPER.COMMENTSF, ");
    UnSQL.SQL.Add("  MDFUNCPER.FUNLAVEL, ");
    UnSQL.SQL.Add("  MDFUNCPER.IDMDFUNCESC, ");
    UnSQL.SQL.Add("  MDFUNCPER.IDMDFUNCPER, ");
    UnSQL.SQL.Add("  MDFUNCPER.IDMDGROUP, ");
    UnSQL.SQL.Add("  MDFUNCPER.PERCF, ");
    UnSQL.SQL.Add("  MDFUNCPER.TOTAL ");
    UnSQL.SQL.Add(" FROM  MDFUNCPER ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  MDFUNCPER.COMMENTSF=[COMMENTSF] and  ");
    UnSQL.SQL.Add("  MDFUNCPER.FUNLAVEL=[FUNLAVEL] and  ");
    UnSQL.SQL.Add("  MDFUNCPER.IDMDFUNCESC=[IDMDFUNCESC] and  ");
    UnSQL.SQL.Add("  MDFUNCPER.IDMDFUNCPER=[IDMDFUNCPER] and  ");
    UnSQL.SQL.Add("  MDFUNCPER.IDMDGROUP=[IDMDGROUP] and  ");
    UnSQL.SQL.Add("  MDFUNCPER.PERCF=[PERCF] and  ");
    UnSQL.SQL.Add("  MDFUNCPER.TOTAL=[TOTAL] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_MDFUNCPER = SysCfg.DB.SQL.Methods.OpenDataSet("DataLink", "MDFUNCPER_GETID", Param.ToBytes());
        ResErr = DS_MDFUNCPER.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDFUNCPER.DataSet.RecordCount > 0)
            {
                DS_MDFUNCPER.DataSet.First();
                MDFUNCPER.IDMDFUNCPER = DS_MDFUNCPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCPER.FieldName).asInt32();
                //Other
                MDFUNCPER.FUNLAVEL = DS_MDFUNCPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDFUNCPER.FUNLAVEL.FieldName).asInt32();
                MDFUNCPER.IDMDFUNCESC = DS_MDFUNCPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCESC.FieldName).asInt32();
                MDFUNCPER.IDMDGROUP = DS_MDFUNCPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDGROUP.FieldName).asInt32();
                MDFUNCPER.PERCF = DS_MDFUNCPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDFUNCPER.PERCF.FieldName).asDouble();
                MDFUNCPER.TOTAL = DS_MDFUNCPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDFUNCPER.TOTAL.FieldName).asDouble();
            }
            else
            {
                DS_MDFUNCPER.ResErr.NotError = false;
                DS_MDFUNCPER.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDFUNCPER_ADD=function(MDFUNCPER)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCPER.FieldName, MDFUNCPER.IDMDFUNCPER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDFUNCPER.COMMENTSF.FieldName, MDFUNCPER.COMMENTSF, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCPER.FUNLAVEL.FieldName, MDFUNCPER.FUNLAVEL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCESC.FieldName, MDFUNCPER.IDMDFUNCESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDGROUP.FieldName, MDFUNCPER.IDMDGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDouble(UsrCfg.InternoAtisNames.MDFUNCPER.PERCF.FieldName, MDFUNCPER.PERCF, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDouble(UsrCfg.InternoAtisNames.MDFUNCPER.TOTAL.FieldName, MDFUNCPER.TOTAL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDFUNCPER_ADD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDFUNCPER_ADD", "MDFUNCPER_ADD description.");
    UnSQL.SQL.Add(" INSERT INTO MDFUNCPER( ");
    UnSQL.SQL.Add("  COMMENTSF,  ");
    UnSQL.SQL.Add("  FUNLAVEL,  ");
    UnSQL.SQL.Add("  IDMDFUNCESC,  ");
    UnSQL.SQL.Add("  IDMDFUNCPER,  ");
    UnSQL.SQL.Add("  IDMDGROUP,  ");
    UnSQL.SQL.Add("  PERCF,  ");
    UnSQL.SQL.Add("  TOTAL ");
    UnSQL.SQL.Add(" )  ");
    UnSQL.SQL.Add(" VALUES ( ");
    UnSQL.SQL.Add("  [COMMENTSF], ");
    UnSQL.SQL.Add("  [FUNLAVEL], ");
    UnSQL.SQL.Add("  [IDMDFUNCESC], ");
    UnSQL.SQL.Add("  [IDMDFUNCPER], ");
    UnSQL.SQL.Add("  [IDMDGROUP], ");
    UnSQL.SQL.Add("  [PERCF], ");
    UnSQL.SQL.Add("  [TOTAL] ");
    UnSQL.SQL.Add(" ) ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("DataLink", "MDFUNCPER_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            MDFUNCPER_GETID(MDFUNCPER);
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDFUNCPER_UPD=function(MDFUNCPER)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCPER.FieldName, MDFUNCPER.IDMDFUNCPER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDFUNCPER.COMMENTSF.FieldName, MDFUNCPER.COMMENTSF, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCPER.FUNLAVEL.FieldName, MDFUNCPER.FUNLAVEL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCESC.FieldName, MDFUNCPER.IDMDFUNCESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDGROUP.FieldName, MDFUNCPER.IDMDGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDouble(UsrCfg.InternoAtisNames.MDFUNCPER.PERCF.FieldName, MDFUNCPER.PERCF, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDouble(UsrCfg.InternoAtisNames.MDFUNCPER.TOTAL.FieldName, MDFUNCPER.TOTAL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDFUNCPER_UPD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDFUNCPER_UPD", "MDFUNCPER_UPD description.");
    UnSQL.SQL.Add(" UPDATE MDFUNCPER SET ");
    UnSQL.SQL.Add("  COMMENTSF=[COMMENTSF],  ");
    UnSQL.SQL.Add("  FUNLAVEL=[FUNLAVEL],  ");
    UnSQL.SQL.Add("  IDMDFUNCESC=[IDMDFUNCESC],  ");
    UnSQL.SQL.Add("  IDMDFUNCPER=[IDMDFUNCPER],  ");
    UnSQL.SQL.Add("  IDMDGROUP=[IDMDGROUP],  ");
    UnSQL.SQL.Add("  PERCF=[PERCF],  ");
    UnSQL.SQL.Add("  TOTAL=[TOTAL] ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  COMMENTSF=[COMMENTSF] AND  ");
    UnSQL.SQL.Add("  FUNLAVEL=[FUNLAVEL] AND  ");
    UnSQL.SQL.Add("  IDMDFUNCESC=[IDMDFUNCESC] AND  ");
    UnSQL.SQL.Add("  IDMDFUNCPER=[IDMDFUNCPER] AND  ");
    UnSQL.SQL.Add("  IDMDGROUP=[IDMDGROUP] AND  ");
    UnSQL.SQL.Add("  PERCF=[PERCF] AND  ");
    UnSQL.SQL.Add("  TOTAL=[TOTAL] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("DataLink", "MDFUNCPER_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            //GET(); 
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDFUNCPER_DEL=function(/* StrIDMDFUNCPER*/ IDMDFUNCPER)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        /*if (StrIDMDFUNCPER == "")
        {
            Param.AddUnknown(UsrCfg.InternoDataLinkNames.MDFUNCPER.IDMDFUNCPER.FieldName, " = " + UsrCfg.InternoDataLinkNames.MDFUNCPER.NAME_TABLE + "." + UsrCfg.InternoDataLinkNames.MDFUNCPER.IDMDFUNCPER.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDMDFUNCPER = " IN (" + StrIDMDFUNCPER + ")";
            Param.AddUnknown(UsrCfg.InternoDataLinkNames.MDFUNCPER.IDMDFUNCPER.FieldName, StrIDMDFUNCPER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCPER.FieldName, IDMDFUNCPER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDFUNCPER_DEL_1   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDFUNCPER_DEL_1", "MDFUNCPER_DEL_1 description.");
    UnSQL.SQL.Add(" DELETE MDFUNCPER  ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  COMMENTSF=[COMMENTSF] AND  ");
    UnSQL.SQL.Add("  FUNLAVEL=[FUNLAVEL] AND  ");
    UnSQL.SQL.Add("  IDMDFUNCESC=[IDMDFUNCESC] AND  ");
    UnSQL.SQL.Add("  IDMDFUNCPER=[IDMDFUNCPER] AND  ");
    UnSQL.SQL.Add("  IDMDGROUP=[IDMDGROUP] AND  ");
    UnSQL.SQL.Add("  PERCF=[PERCF] AND  ");
    UnSQL.SQL.Add("  TOTAL=[TOTAL] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("DataLink", "MDFUNCPER_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDFUNCPER_DEL=function(MDFUNCPER)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCPER.FieldName, MDFUNCPER.IDMDFUNCPER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDFUNCPER.COMMENTSF.FieldName, MDFUNCPER.COMMENTSF, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCPER.FUNLAVEL.FieldName, MDFUNCPER.FUNLAVEL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCESC.FieldName, MDFUNCPER.IDMDFUNCESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDGROUP.FieldName, MDFUNCPER.IDMDGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDouble(UsrCfg.InternoAtisNames.MDFUNCPER.PERCF.FieldName, MDFUNCPER.PERCF, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDouble(UsrCfg.InternoAtisNames.MDFUNCPER.TOTAL.FieldName, MDFUNCPER.TOTAL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDFUNCPER_DEL_2   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDFUNCPER_DEL_2", "MDFUNCPER_DEL_2 description.");
    UnSQL.SQL.Add(" DELETE MDFUNCPER  ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  COMMENTSF=[COMMENTSF] AND  ");
    UnSQL.SQL.Add("  FUNLAVEL=[FUNLAVEL] AND  ");
    UnSQL.SQL.Add("  IDMDFUNCESC=[IDMDFUNCESC] AND  ");
    UnSQL.SQL.Add("  IDMDFUNCPER=[IDMDFUNCPER] AND  ");
    UnSQL.SQL.Add("  IDMDGROUP=[IDMDGROUP] AND  ");
    UnSQL.SQL.Add("  PERCF=[PERCF] AND  ");
    UnSQL.SQL.Add("  TOTAL=[TOTAL] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("DataLink", "MDFUNCPER_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
     
//**********************   METHODS   ********************************************************************************************

//DataSet Function 
//List Function 
Persistence.Model.Methods.MDFUNCPER_ListSetID=function(MDFUNCPERList, IDMDFUNCPER)
{
    for (i = 0; i < MDFUNCPERList.length; i++)
    {

        if (IDMDFUNCPER == MDFUNCPERList[i].IDMDFUNCPER)
            return (MDFUNCPERList[i]);

    }

    var TMDFUNCPER = new Persistence.Properties.TMDFUNCPER();
    TMDFUNCPER.IDMDFUNCPER = IDMDFUNCPER; 
    return (TMDFUNCPER); 
}
Persistence.Model.Methods.MDFUNCPER_ListAdd=function(MDFUNCPERList,MDFUNCPER)
{
    var i = Persistence.Model.Methods.MDFUNCPER_ListGetIndex(MDFUNCPERList, MDFUNCPER);
    if (i == -1) MDFUNCPERList.push(MDFUNCPER);
    else
    {
        MDFUNCPERList[i].COMMENTSF = MDFUNCPER.COMMENTSF;
        MDFUNCPERList[i].FUNLAVEL = MDFUNCPER.FUNLAVEL;
        MDFUNCPERList[i].IDMDFUNCESC = MDFUNCPER.IDMDFUNCESC;
        MDFUNCPERList[i].IDMDFUNCPER = MDFUNCPER.IDMDFUNCPER;
        MDFUNCPERList[i].IDMDGROUP = MDFUNCPER.IDMDGROUP;
        MDFUNCPERList[i].PERCF = MDFUNCPER.PERCF;
        MDFUNCPERList[i].TOTAL = MDFUNCPER.TOTAL;
    }
}
Persistence.Model.Methods.MDFUNCPER_ListGetIndex=function(MDFUNCPERList, MDFUNCPER)
{
    for (i = 0; i < MDFUNCPERList.length; i++)
    {
        if (MDFUNCPER.IDMDFUNCPER == MDFUNCPERList[i].IDMDFUNCPER)
            return (i);
    }
    return (-1);
}
Persistence.Model.Methods.MDFUNCPER_ListGetIndex=function(MDFUNCPERList, IDMDFUNCPER)
{
    for (i = 0; i < MDFUNCPERList.length; i++)
    {
        if (IDMDFUNCPER == MDFUNCPERList[i].IDMDFUNCPER)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.Model.Methods.MDFUNCPERListtoStr=function(MDFUNCPERList)
{
    var Res = Persistence.Model.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(MDFUNCPERList.length, Longitud, Texto);
        for (Counter = 0; Counter < MDFUNCPERList.length; Counter++) {
            var UnMDFUNCPER = MDFUNCPERList[Counter];
            UnMDFUNCPER.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Model.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.Model.Methods.StrtoMDFUNCPER=function(ProtocoloStr, MDFUNCPERList)
{
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        MDFUNCPERList.length = 0;
        if (Persistence.Model.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnMDFUNCPER = new Persistence.Demo.Properties.TMDFUNCPER(); //Mode new row
                UnMDFUNCPER.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.MDFUNCPER_ListAdd(MDFUNCPERList, UnMDFUNCPER);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
    
}

//Socket Function 
Persistence.Model.Methods.MDFUNCPERListToByte=function(MDFUNCPERList, MemStream)
{
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, MDFUNCPERList.length);
    for (i = 0; i < MDFUNCPERList.length; i++)
    {
        MDFUNCPERList[i].ToByte(MemStream);
    }
}
Persistence.Model.Methods.ByteToMDFUNCPERList=function(MDFUNCPERList, MemStream)
{
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++)
    {
        var UnMDFUNCPER = new Persistence.Model.Properties.TMDFUNCPER();
        MDFUNCPERList[i].ToByte(MemStream);
    }
}
 