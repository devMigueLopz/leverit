﻿//TreduceDB
//Persistence.Model.Properties
//Persistence.Model.Methods

 
Persistence.Model.Properties.TMDIMPACT = function()
{
    this.IDMDIMPACT = 0;
    this.IMPACTNAME = "";
    //Socket IO Properties
    this.ToByte = function(MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDIMPACT);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.IMPACTNAME);
    }
    this.ByteTo = function(MemStream)
    {
        this.IDMDIMPACT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IMPACTNAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    }
    //Str IO Properties
    this.ToStr = function(Longitud, Texto)
    {
        SysCfg.Str.Protocol.WriteInt(this.IDMDIMPACT, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.IMPACTNAME, Longitud, Texto);
    }
    this.StrTo = function(Pos, Index, LongitudArray, Texto)
    {
        this.IDMDIMPACT = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IMPACTNAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
    }
}
    
//**********************   METHODS server  ********************************************************************************************

//DataSet Function 
Persistence.Model.Methods.MDIMPACT_Fill=function(MDIMPACTList, StrIDMDIMPACT/*IDMDIMPACT*/)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    MDIMPACTList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDMDIMPACT == "")
    {
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.FieldName, " = " + UsrCfg.InternoAtisNames.MDIMPACT.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else
    {
        StrIDMDIMPACT = " IN (" + StrIDMDIMPACT + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.FieldName, StrIDMDIMPACT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //Param.AddInt32(UsrCfg.InternoDataLinkNames.MDIMPACT.IDMDIMPACT.FieldName, IDMDIMPACT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try
    {
        /*

    //********   MDIMPACT_GET   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDIMPACT_GET", "MDIMPACT_GET description.");
    UnSQL.SQL.Add(" SELECT  ");
    UnSQL.SQL.Add("  MDIMPACT.IDMDIMPACT, ");
    UnSQL.SQL.Add("  MDIMPACT.IMPACTNAME ");
    UnSQL.SQL.Add(" FROM  MDIMPACT ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  MDIMPACT.IDMDIMPACT=[IDMDIMPACT] and  ");
    UnSQL.SQL.Add("  MDIMPACT.IMPACTNAME=[IMPACTNAME] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_MDIMPACT = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDIMPACT_GET", Param.ToBytes());
        ResErr = DS_MDIMPACT.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDIMPACT.DataSet.RecordCount > 0)
            {
                DS_MDIMPACT.DataSet.First();
                while (!(DS_MDIMPACT.DataSet.Eof))
                {
                    var MDIMPACT = new Persistence.Model.Properties.TMDIMPACT();
                    MDIMPACT.IDMDIMPACT = DS_MDIMPACT.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.FieldName).asInt32();
                    //Other
                    MDIMPACT.IMPACTNAME = DS_MDIMPACT.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDIMPACT.IMPACTNAME.FieldName).asString();
                    MDIMPACTList.push(MDIMPACT);

                    DS_MDIMPACT.DataSet.Next();
                }
            }
            else
            {
                DS_MDIMPACT.ResErr.NotError = false;
                DS_MDIMPACT.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDIMPACT_GETID=function(MDIMPACT)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.FieldName, MDIMPACT.IDMDIMPACT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.MDIMPACT.IMPACTNAME.FieldName, MDIMPACT.IMPACTNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDIMPACT_GETID   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDIMPACT_GETID", "MDIMPACT_GETID description.");
    UnSQL.SQL.Add(" SELECT  ");
    UnSQL.SQL.Add("  MDIMPACT.IDMDIMPACT, ");
    UnSQL.SQL.Add("  MDIMPACT.IMPACTNAME ");
    UnSQL.SQL.Add(" FROM  MDIMPACT ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  MDIMPACT.IDMDIMPACT=[IDMDIMPACT] and  ");
    UnSQL.SQL.Add("  MDIMPACT.IMPACTNAME=[IMPACTNAME] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_MDIMPACT = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDIMPACT_GETID", Param.ToBytes());
        ResErr = DS_MDIMPACT.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDIMPACT.DataSet.RecordCount > 0)
            {
                DS_MDIMPACT.DataSet.First();
                MDIMPACT.IDMDIMPACT = DS_MDIMPACT.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.FieldName).asInt32();
                //Other
                MDIMPACT.IMPACTNAME = DS_MDIMPACT.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDIMPACT.IMPACTNAME.FieldName).asString();
            }
            else
            {
                DS_MDIMPACT.ResErr.NotError = false;
                DS_MDIMPACT.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDIMPACT_ADD=function(MDIMPACT)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.FieldName, MDIMPACT.IDMDIMPACT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.MDIMPACT.IMPACTNAME.FieldName, MDIMPACT.IMPACTNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDIMPACT_ADD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDIMPACT_ADD", "MDIMPACT_ADD description.");
    UnSQL.SQL.Add(" INSERT INTO MDIMPACT( ");
    UnSQL.SQL.Add("  IDMDIMPACT,  ");
    UnSQL.SQL.Add("  IMPACTNAME ");
    UnSQL.SQL.Add(" )  ");
    UnSQL.SQL.Add(" VALUES ( ");
    UnSQL.SQL.Add("  [IDMDIMPACT], ");
    UnSQL.SQL.Add("  [IMPACTNAME] ");
    UnSQL.SQL.Add(" ) ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDIMPACT_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            MDIMPACT_GETID(MDIMPACT);
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDIMPACT_UPD=function( MDIMPACT)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.FieldName, MDIMPACT.IDMDIMPACT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.MDIMPACT.IMPACTNAME.FieldName, MDIMPACT.IMPACTNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDIMPACT_UPD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDIMPACT_UPD", "MDIMPACT_UPD description.");
    UnSQL.SQL.Add(" UPDATE MDIMPACT SET ");
    UnSQL.SQL.Add("  IDMDIMPACT=[IDMDIMPACT],  ");
    UnSQL.SQL.Add("  IMPACTNAME=[IMPACTNAME] ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  IDMDIMPACT=[IDMDIMPACT] AND  ");
    UnSQL.SQL.Add("  IMPACTNAME=[IMPACTNAME] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDIMPACT_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            //GET(); 
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDIMPACT_DEL=function(/* StrIDMDIMPACT*/ IDMDIMPACT)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        /*if (StrIDMDIMPACT == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.FieldName, " = " + UsrCfg.InternoAtisNames.MDIMPACT.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDMDIMPACT = " IN (" + StrIDMDIMPACT + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.FieldName, StrIDMDIMPACT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.FieldName, IDMDIMPACT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDIMPACT_DEL_1   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDIMPACT_DEL_1", "MDIMPACT_DEL_1 description.");
    UnSQL.SQL.Add(" DELETE MDIMPACT  ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  IDMDIMPACT=[IDMDIMPACT] AND  ");
    UnSQL.SQL.Add("  IMPACTNAME=[IMPACTNAME] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDIMPACT_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDIMPACT_DEL=function(MDIMPACT)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.FieldName, MDIMPACT.IDMDIMPACT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.MDIMPACT.IMPACTNAME.FieldName, MDIMPACT.IMPACTNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDIMPACT_DEL_2   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDIMPACT_DEL_2", "MDIMPACT_DEL_2 description.");
    UnSQL.SQL.Add(" DELETE MDIMPACT  ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  IDMDIMPACT=[IDMDIMPACT] AND  ");
    UnSQL.SQL.Add("  IMPACTNAME=[IMPACTNAME] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDIMPACT_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
    

//**********************   METHODS   ********************************************************************************************

   
//DataSet Function 

//List Function 
Persistence.Model.Methods.MDIMPACT_ListSetID=function(MDIMPACTList, IDMDIMPACT)
{
    for (i = 0; i < MDIMPACTList.length; i++)
    {

        if (IDMDIMPACT == MDIMPACTList[i].IDMDIMPACT)
            return (MDIMPACTList[i]);

    }

    var TMDIMPACT = new Persistence.Properties.TMDIMPACT();
    TMDIMPACT.IDMDIMPACT = IDMDIMPACT; 
    return (TMDIMPACT); 

}

Persistence.Model.Methods.MDIMPACT_ListAdd=function(MDIMPACTList, MDIMPACT)
{
    var i = Persistence.Model.Methods.MDIMPACT_ListGetIndex(MDIMPACTList, MDIMPACT);
    if (i == -1) MDIMPACTList.push(MDIMPACT);
    else
    {
        MDIMPACTList[i].IDMDIMPACT = MDIMPACT.IDMDIMPACT;
        MDIMPACTList[i].IMPACTNAME = MDIMPACT.IMPACTNAME;
    }
}
Persistence.Model.Methods.MDIMPACT_ListGetIndex=function(MDIMPACTList, MDIMPACT)
{
    for (i = 0; i < MDIMPACTList.length; i++)
    {
        if (MDIMPACT.IDMDIMPACT == MDIMPACTList[i].IDMDIMPACT)
            return (i);
    }
    return (-1);
}
Persistence.Model.Methods.MDIMPACT_ListGetIndex=function(MDIMPACTList, IDMDIMPACT)
{
    for (i = 0; i < MDIMPACTList.length; i++)
    {
        if (IDMDIMPACT == MDIMPACTList[i].IDMDIMPACT)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.Model.Methods.MDIMPACTListtoStr=function(MDIMPACTList)
{
    var Res = Persistence.Model.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(MDIMPACTList.length, Longitud, Texto);
        for (Counter = 0; Counter < MDIMPACTList.length; Counter++) {
            var UnMDIMPACT = MDIMPACTList[Counter];
            UnMDIMPACT.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Model.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.Model.Methods.StrtoMDIMPACT=function(ProtocoloStr, MDIMPACTList)
{
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        MDIMPACTList.length = 0;
        if (Persistence.Model.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnMDIMPACT = new Persistence.Demo.Properties.TMDIMPACT(); //Mode new row
                UnMDIMPACT.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.MDIMPACT_ListAdd(MDIMPACTList, UnMDIMPACT);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
    
}

//Socket Function 
Persistence.Model.Methods.MDIMPACTListToByte=function(MDIMPACTList, MemStream)
{
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, MDIMPACTList.length);
    for (i = 0; i < MDIMPACTList.length; i++)
    {
        MDIMPACTList[i].ToByte(MemStream);
    }
}
Persistence.Model.Methods.ByteToMDIMPACTList=function(MDIMPACTList, MemStream)
{
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++)
    {
        var UnMDIMPACT = new Persistence.Model.Properties.TMDIMPACT();
        MDIMPACTList[i].ToByte(MemStream);
    }
}
    