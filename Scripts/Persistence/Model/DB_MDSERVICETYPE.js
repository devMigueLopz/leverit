﻿//TreduceDB
//Persistence.Model.Properties
//Persistence.Model.Methods

 
Persistence.Model.Properties.TMDSERVICETYPE = function()
{
    this.IDMDSERVICETYPE =0;
    this.SERVICETYPEDESCRIPTION ="";
    this.SERVICETYPENAME ="";
    //Socket IO Properties
    this.ToByte = function(MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDSERVICETYPE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.SERVICETYPEDESCRIPTION);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.SERVICETYPENAME);
    }
    this.ByteTo = function( MemStream)
    {
        this.IDMDSERVICETYPE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.SERVICETYPEDESCRIPTION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.SERVICETYPENAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    }
    //Str IO Properties
    this.ToStr = function( Longitud, Texto)
    {
        SysCfg.Str.Protocol.WriteInt(this.IDMDSERVICETYPE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.SERVICETYPEDESCRIPTION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.SERVICETYPENAME, Longitud, Texto);
    }
    this.StrTo = function(Pos, Index, LongitudArray, Texto)
    {
        this.IDMDSERVICETYPE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.SERVICETYPEDESCRIPTION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.SERVICETYPENAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
    }

}
    

//**********************   METHODS server  ********************************************************************************************

    
//DataSet Function 
Persistence.Model.Methods.MDSERVICETYPE_Fill=function(MDSERVICETYPEList,  StrIDMDSERVICETYPE/* IDMDSERVICETYPE*/)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    MDSERVICETYPEList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDMDSERVICETYPE == "")
    {
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDSERVICETYPE.IDMDSERVICETYPE.FieldName, " = " + UsrCfg.InternoAtisNames.MDSERVICETYPE.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDSERVICETYPE.IDMDSERVICETYPE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else
    {
        StrIDMDSERVICETYPE = " IN (" + StrIDMDSERVICETYPE + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDSERVICETYPE.IDMDSERVICETYPE.FieldName, StrIDMDSERVICETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //Param.AddInt32(UsrCfg.InternoDataLinkNames.MDSERVICETYPE.IDMDSERVICETYPE.FieldName, IDMDSERVICETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try
    {
        /*

    //********   MDSERVICETYPE_GET   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDSERVICETYPE_GET", "MDSERVICETYPE_GET description.");
    UnSQL.SQL.Add(" SELECT  ");
    UnSQL.SQL.Add("  MDSERVICETYPE.IDMDSERVICETYPE, ");
    UnSQL.SQL.Add("  MDSERVICETYPE.SERVICETYPEDESCRIPTION, ");
    UnSQL.SQL.Add("  MDSERVICETYPE.SERVICETYPENAME ");
    UnSQL.SQL.Add(" FROM  MDSERVICETYPE ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  MDSERVICETYPE.IDMDSERVICETYPE=[IDMDSERVICETYPE] and  ");
    UnSQL.SQL.Add("  MDSERVICETYPE.SERVICETYPEDESCRIPTION=[SERVICETYPEDESCRIPTION] and  ");
    UnSQL.SQL.Add("  MDSERVICETYPE.SERVICETYPENAME=[SERVICETYPENAME] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_MDSERVICETYPE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDSERVICETYPE_GET", Param.ToBytes());
        ResErr = DS_MDSERVICETYPE.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDSERVICETYPE.DataSet.RecordCount > 0)
            {
                DS_MDSERVICETYPE.DataSet.First();
                while (!(DS_MDSERVICETYPE.DataSet.Eof))
                {
                    var MDSERVICETYPE = new Persistence.Model.Properties.TMDSERVICETYPE();
                    MDSERVICETYPE.IDMDSERVICETYPE = DS_MDSERVICETYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICETYPE.IDMDSERVICETYPE.FieldName).asInt32();
                    //Other
                    MDSERVICETYPE.SERVICETYPEDESCRIPTION = DS_MDSERVICETYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICETYPE.SERVICETYPEDESCRIPTION.FieldName).asString();
                    MDSERVICETYPE.SERVICETYPENAME = DS_MDSERVICETYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICETYPE.SERVICETYPENAME.FieldName).asString();
                    MDSERVICETYPEList.push(MDSERVICETYPE);

                    DS_MDSERVICETYPE.DataSet.Next();
                }
            }
            else
            {
                DS_MDSERVICETYPE.ResErr.NotError = false;
                DS_MDSERVICETYPE.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDSERVICETYPE_GETID=function( MDSERVICETYPE)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICETYPE.IDMDSERVICETYPE.FieldName, MDSERVICETYPE.IDMDSERVICETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICETYPE.SERVICETYPEDESCRIPTION.FieldName, MDSERVICETYPE.SERVICETYPEDESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICETYPE.SERVICETYPENAME.FieldName, MDSERVICETYPE.SERVICETYPENAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDSERVICETYPE_GETID   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDSERVICETYPE_GETID", "MDSERVICETYPE_GETID description.");
    UnSQL.SQL.Add(" SELECT  ");
    UnSQL.SQL.Add("  MDSERVICETYPE.IDMDSERVICETYPE, ");
    UnSQL.SQL.Add("  MDSERVICETYPE.SERVICETYPEDESCRIPTION, ");
    UnSQL.SQL.Add("  MDSERVICETYPE.SERVICETYPENAME ");
    UnSQL.SQL.Add(" FROM  MDSERVICETYPE ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  MDSERVICETYPE.IDMDSERVICETYPE=[IDMDSERVICETYPE] and  ");
    UnSQL.SQL.Add("  MDSERVICETYPE.SERVICETYPEDESCRIPTION=[SERVICETYPEDESCRIPTION] and  ");
    UnSQL.SQL.Add("  MDSERVICETYPE.SERVICETYPENAME=[SERVICETYPENAME] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_MDSERVICETYPE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDSERVICETYPE_GETID", Param.ToBytes());
        ResErr = DS_MDSERVICETYPE.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDSERVICETYPE.DataSet.RecordCount > 0)
            {
                DS_MDSERVICETYPE.DataSet.First();
                MDSERVICETYPE.IDMDSERVICETYPE = DS_MDSERVICETYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICETYPE.IDMDSERVICETYPE.FieldName).asInt32();
                //Other
                MDSERVICETYPE.SERVICETYPEDESCRIPTION = DS_MDSERVICETYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICETYPE.SERVICETYPEDESCRIPTION.FieldName).asString();
                MDSERVICETYPE.SERVICETYPENAME = DS_MDSERVICETYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICETYPE.SERVICETYPENAME.FieldName).asString();
            }
            else
            {
                DS_MDSERVICETYPE.ResErr.NotError = false;
                DS_MDSERVICETYPE.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDSERVICETYPE_ADD=function(MDSERVICETYPE)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICETYPE.IDMDSERVICETYPE.FieldName, MDSERVICETYPE.IDMDSERVICETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICETYPE.SERVICETYPEDESCRIPTION.FieldName, MDSERVICETYPE.SERVICETYPEDESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICETYPE.SERVICETYPENAME.FieldName, MDSERVICETYPE.SERVICETYPENAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDSERVICETYPE_ADD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDSERVICETYPE_ADD", "MDSERVICETYPE_ADD description.");
    UnSQL.SQL.Add(" INSERT INTO MDSERVICETYPE( ");
    UnSQL.SQL.Add("  IDMDSERVICETYPE,  ");
    UnSQL.SQL.Add("  SERVICETYPEDESCRIPTION,  ");
    UnSQL.SQL.Add("  SERVICETYPENAME ");
    UnSQL.SQL.Add(" )  ");
    UnSQL.SQL.Add(" VALUES ( ");
    UnSQL.SQL.Add("  [IDMDSERVICETYPE], ");
    UnSQL.SQL.Add("  [SERVICETYPEDESCRIPTION], ");
    UnSQL.SQL.Add("  [SERVICETYPENAME] ");
    UnSQL.SQL.Add(" ) ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDSERVICETYPE_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            MDSERVICETYPE_GETID(MDSERVICETYPE);
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDSERVICETYPE_UPD=function(MDSERVICETYPE)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICETYPE.IDMDSERVICETYPE.FieldName, MDSERVICETYPE.IDMDSERVICETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICETYPE.SERVICETYPEDESCRIPTION.FieldName, MDSERVICETYPE.SERVICETYPEDESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICETYPE.SERVICETYPENAME.FieldName, MDSERVICETYPE.SERVICETYPENAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDSERVICETYPE_UPD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDSERVICETYPE_UPD", "MDSERVICETYPE_UPD description.");
    UnSQL.SQL.Add(" UPDATE MDSERVICETYPE SET ");
    UnSQL.SQL.Add("  IDMDSERVICETYPE=[IDMDSERVICETYPE],  ");
    UnSQL.SQL.Add("  SERVICETYPEDESCRIPTION=[SERVICETYPEDESCRIPTION],  ");
    UnSQL.SQL.Add("  SERVICETYPENAME=[SERVICETYPENAME] ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  IDMDSERVICETYPE=[IDMDSERVICETYPE] AND  ");
    UnSQL.SQL.Add("  SERVICETYPEDESCRIPTION=[SERVICETYPEDESCRIPTION] AND  ");
    UnSQL.SQL.Add("  SERVICETYPENAME=[SERVICETYPENAME] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDSERVICETYPE_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            //GET(); 
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDSERVICETYPE_DEL=function(/* StrIDMDSERVICETYPE*/ IDMDSERVICETYPE)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        /*if (StrIDMDSERVICETYPE == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDSERVICETYPE.IDMDSERVICETYPE.FieldName, " = " + UsrCfg.InternoAtisNames.MDSERVICETYPE.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDSERVICETYPE.IDMDSERVICETYPE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDMDSERVICETYPE = " IN (" + StrIDMDSERVICETYPE + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDSERVICETYPE.IDMDSERVICETYPE.FieldName, StrIDMDSERVICETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICETYPE.IDMDSERVICETYPE.FieldName, IDMDSERVICETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDSERVICETYPE_DEL_1   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDSERVICETYPE_DEL_1", "MDSERVICETYPE_DEL_1 description.");
    UnSQL.SQL.Add(" DELETE MDSERVICETYPE  ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  IDMDSERVICETYPE=[IDMDSERVICETYPE] AND  ");
    UnSQL.SQL.Add("  SERVICETYPEDESCRIPTION=[SERVICETYPEDESCRIPTION] AND  ");
    UnSQL.SQL.Add("  SERVICETYPENAME=[SERVICETYPENAME] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDSERVICETYPE_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDSERVICETYPE_DEL=function(MDSERVICETYPE)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICETYPE.IDMDSERVICETYPE.FieldName, MDSERVICETYPE.IDMDSERVICETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICETYPE.SERVICETYPEDESCRIPTION.FieldName, MDSERVICETYPE.SERVICETYPEDESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICETYPE.SERVICETYPENAME.FieldName, MDSERVICETYPE.SERVICETYPENAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDSERVICETYPE_DEL_2   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDSERVICETYPE_DEL_2", "MDSERVICETYPE_DEL_2 description.");
    UnSQL.SQL.Add(" DELETE MDSERVICETYPE  ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  IDMDSERVICETYPE=[IDMDSERVICETYPE] AND  ");
    UnSQL.SQL.Add("  SERVICETYPEDESCRIPTION=[SERVICETYPEDESCRIPTION] AND  ");
    UnSQL.SQL.Add("  SERVICETYPENAME=[SERVICETYPENAME] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDSERVICETYPE_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
     

//**********************   METHODS   ********************************************************************************************

    
//DataSet Function 

//List Function 
Persistence.Model.Methods.MDSERVICETYPE_ListSetID=function(MDSERVICETYPEList, IDMDSERVICETYPE)
{
    for (i = 0; i < MDSERVICETYPEList.length; i++)
    {

        if (IDMDSERVICETYPE == MDSERVICETYPEList[i].IDMDSERVICETYPE)
            return (MDSERVICETYPEList[i]);

    }
     
    var TMDSERVICETYPE = new Persistence.Properties.TMDSERVICETYPE();
    TMDSERVICETYPE.IDMDSERVICETYPE = IDMDSERVICETYPE; 
    return (TMDSERVICETYPE); 
}
Persistence.Model.Methods.MDSERVICETYPE_ListAdd=function(MDSERVICETYPEList,MDSERVICETYPE)
{
    var i = Persistence.Model.Methods.MDSERVICETYPE_ListGetIndex(MDSERVICETYPEList, MDSERVICETYPE);
    if (i == -1) MDSERVICETYPEList.push(MDSERVICETYPE);
    else
    {
        MDSERVICETYPEList[i].IDMDSERVICETYPE = MDSERVICETYPE.IDMDSERVICETYPE;
        MDSERVICETYPEList[i].SERVICETYPEDESCRIPTION = MDSERVICETYPE.SERVICETYPEDESCRIPTION;
        MDSERVICETYPEList[i].SERVICETYPENAME = MDSERVICETYPE.SERVICETYPENAME;
    }
}
Persistence.Model.Methods.MDSERVICETYPE_ListGetIndex=function(MDSERVICETYPEList, MDSERVICETYPE)
{
    for (i = 0; i < MDSERVICETYPEList.length; i++)
    {
        if (MDSERVICETYPE.IDMDSERVICETYPE == MDSERVICETYPEList[i].IDMDSERVICETYPE)
            return (i);
    }
    return (-1);
}
Persistence.Model.Methods.MDSERVICETYPE_ListGetIndex=function(MDSERVICETYPEList, IDMDSERVICETYPE)
{
    for (i = 0; i < MDSERVICETYPEList.length; i++)
    {
        if (IDMDSERVICETYPE == MDSERVICETYPEList[i].IDMDSERVICETYPE)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.Model.Methods.MDSERVICETYPEListtoStr=function(MDSERVICETYPEList)
{
    var Res = Persistence.Model.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(MDSERVICETYPEList.length, Longitud, Texto);
        for (Counter = 0; Counter < MDSERVICETYPEList.length; Counter++) {
            var UnMDSERVICETYPE = MDSERVICETYPEList[Counter];
            UnMDSERVICETYPE.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Model.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.Model.Methods.StrtoMDSERVICETYPE=function(ProtocoloStr, MDSERVICETYPEList)
{
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        MDSERVICETYPEList.length = 0;
        if (Persistence.Model.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnMDSERVICETYPE = new Persistence.Demo.Properties.TMDSERVICETYPE(); //Mode new row
                UnMDSERVICETYPE.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.MDSERVICETYPE_ListAdd(MDSERVICETYPEList, UnMDSERVICETYPE);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
    
}

//Socket Function 
Persistence.Model.Methods.MDSERVICETYPEListToByte=function(MDSERVICETYPEList, MemStream)
{
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, MDSERVICETYPEList.length);
    for (i = 0; i < MDSERVICETYPEList.length; i++)
    {
        MDSERVICETYPEList[i].ToByte(MemStream);
    }
}
Persistence.Model.Methods.ByteToMDSERVICETYPEList=function(MDSERVICETYPEList, MemStream)
{
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++)
    {
        var UnMDSERVICETYPE = new Persistence.Model.Properties.TMDSERVICETYPE();
        MDSERVICETYPEList[i].ToByte(MemStream);
    }
}
 