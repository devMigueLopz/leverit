﻿//TreduceDB
//Persistence.Model.Properties
//Persistence.Model.Methods

 
Persistence.Model.PropertiesTMDHIERESC = function()
{
    this.COMMENTSJE = "";
    this.IDMDHIERESC = 0;
    this.TITLEHIERESC = "";
    //Socket IO Properties
    this.ToByte = function(MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.COMMENTSJE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDHIERESC);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.TITLEHIERESC);
    }
    this.ByteTo = function (MemStream)
    {
        this.COMMENTSJE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDMDHIERESC = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.TITLEHIERESC = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto)
    {
        SysCfg.Str.Protocol.WriteStr(this.COMMENTSJE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDHIERESC, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.TITLEHIERESC, Longitud, Texto);
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto)
    {
        this.COMMENTSJE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.IDMDHIERESC = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.TITLEHIERESC = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
    }
}
     

//**********************   METHODS server  ********************************************************************************************

    
//DataSet Function 
Persistence.Model.Methods.MDHIERESC_Fill= function(MDHIERESCList, StrIDMDHIERESC/*IDMDHIERESC*/)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    MDHIERESCList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDMDHIERESC == "")
    {
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDHIERESC.IDMDHIERESC.FieldName, " = " + UsrCfg.InternoAtisNames.MDHIERESC.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDHIERESC.IDMDHIERESC.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else
    {
        StrIDMDHIERESC = " IN (" + StrIDMDHIERESC + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDHIERESC.IDMDHIERESC.FieldName, StrIDMDHIERESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //Param.AddInt32(UsrCfg.InternoDataLinkNames.MDHIERESC.IDMDHIERESC.FieldName, IDMDHIERESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try
    {
        /*

    //********   MDHIERESC_GET   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDHIERESC_GET", "MDHIERESC_GET description.");
    UnSQL.SQL.Add(" SELECT  ");
    UnSQL.SQL.Add("  MDHIERESC.COMMENTSJE, ");
    UnSQL.SQL.Add("  MDHIERESC.IDMDHIERESC, ");
    UnSQL.SQL.Add("  MDHIERESC.TITLEHIERESC ");
    UnSQL.SQL.Add(" FROM  MDHIERESC ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  MDHIERESC.COMMENTSJE=[COMMENTSJE] and  ");
    UnSQL.SQL.Add("  MDHIERESC.IDMDHIERESC=[IDMDHIERESC] and  ");
    UnSQL.SQL.Add("  MDHIERESC.TITLEHIERESC=[TITLEHIERESC] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_MDHIERESC = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDHIERESC_GET", Param.ToBytes());
        ResErr = DS_MDHIERESC.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDHIERESC.DataSet.RecordCount > 0)
            {
                DS_MDHIERESC.DataSet.First();
                while (!(DS_MDHIERESC.DataSet.Eof))
                {
                   var MDHIERESC = new Persistence.Model.Properties.TMDHIERESC();
                    MDHIERESC.IDMDHIERESC = DS_MDHIERESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERESC.IDMDHIERESC.FieldName).asInt32();
                    //Other
                    MDHIERESC.TITLEHIERESC = DS_MDHIERESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERESC.TITLEHIERESC.FieldName).asString();
                    MDHIERESCList.push(MDHIERESC);

                    DS_MDHIERESC.DataSet.Next();
                }
            }
            else
            {
                DS_MDHIERESC.ResErr.NotError = false;
                DS_MDHIERESC.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDHIERESC_GETID=function(MDHIERESC)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERESC.IDMDHIERESC.FieldName, MDHIERESC.IDMDHIERESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDHIERESC.COMMENTSJE.FieldName, MDHIERESC.COMMENTSJE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDHIERESC.TITLEHIERESC.FieldName, MDHIERESC.TITLEHIERESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDHIERESC_GETID   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDHIERESC_GETID", "MDHIERESC_GETID description.");
    UnSQL.SQL.Add(" SELECT  ");
    UnSQL.SQL.Add("  MDHIERESC.COMMENTSJE, ");
    UnSQL.SQL.Add("  MDHIERESC.IDMDHIERESC, ");
    UnSQL.SQL.Add("  MDHIERESC.TITLEHIERESC ");
    UnSQL.SQL.Add(" FROM  MDHIERESC ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  MDHIERESC.COMMENTSJE=[COMMENTSJE] and  ");
    UnSQL.SQL.Add("  MDHIERESC.IDMDHIERESC=[IDMDHIERESC] and  ");
    UnSQL.SQL.Add("  MDHIERESC.TITLEHIERESC=[TITLEHIERESC] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_MDHIERESC = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDHIERESC_GETID", Param.ToBytes());
        ResErr = DS_MDHIERESC.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDHIERESC.DataSet.RecordCount > 0)
            {
                DS_MDHIERESC.DataSet.First();
                MDHIERESC.IDMDHIERESC = DS_MDHIERESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERESC.IDMDHIERESC.FieldName).asInt32();
                //Other
                MDHIERESC.TITLEHIERESC = DS_MDHIERESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERESC.TITLEHIERESC.FieldName).asString();
            }
            else
            {
                DS_MDHIERESC.ResErr.NotError = false;
                DS_MDHIERESC.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDHIERESC_ADD=function(MDHIERESC)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERESC.IDMDHIERESC.FieldName, MDHIERESC.IDMDHIERESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDHIERESC.COMMENTSJE.FieldName, MDHIERESC.COMMENTSJE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDHIERESC.TITLEHIERESC.FieldName, MDHIERESC.TITLEHIERESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDHIERESC_ADD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDHIERESC_ADD", "MDHIERESC_ADD description.");
    UnSQL.SQL.Add(" INSERT INTO MDHIERESC( ");
    UnSQL.SQL.Add("  COMMENTSJE,  ");
    UnSQL.SQL.Add("  IDMDHIERESC,  ");
    UnSQL.SQL.Add("  TITLEHIERESC ");
    UnSQL.SQL.Add(" )  ");
    UnSQL.SQL.Add(" VALUES ( ");
    UnSQL.SQL.Add("  [COMMENTSJE], ");
    UnSQL.SQL.Add("  [IDMDHIERESC], ");
    UnSQL.SQL.Add("  [TITLEHIERESC] ");
    UnSQL.SQL.Add(" ) ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDHIERESC_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            MDHIERESC_GETID(MDHIERESC);
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDHIERESC_UPD=function(MDHIERESC)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERESC.IDMDHIERESC.FieldName, MDHIERESC.IDMDHIERESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDHIERESC.COMMENTSJE.FieldName, MDHIERESC.COMMENTSJE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDHIERESC.TITLEHIERESC.FieldName, MDHIERESC.TITLEHIERESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDHIERESC_UPD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDHIERESC_UPD", "MDHIERESC_UPD description.");
    UnSQL.SQL.Add(" UPDATE MDHIERESC SET ");
    UnSQL.SQL.Add("  COMMENTSJE=[COMMENTSJE],  ");
    UnSQL.SQL.Add("  IDMDHIERESC=[IDMDHIERESC],  ");
    UnSQL.SQL.Add("  TITLEHIERESC=[TITLEHIERESC] ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  COMMENTSJE=[COMMENTSJE] AND  ");
    UnSQL.SQL.Add("  IDMDHIERESC=[IDMDHIERESC] AND  ");
    UnSQL.SQL.Add("  TITLEHIERESC=[TITLEHIERESC] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDHIERESC_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            //GET(); 
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDHIERESC_DEL=function(/* StrIDMDHIERESC*/ IDMDHIERESC)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        /*if (StrIDMDHIERESC == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDHIERESC.IDMDHIERESC.FieldName, " = " + UsrCfg.InternoAtisNames.MDHIERESC.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDHIERESC.IDMDHIERESC.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDMDHIERESC = " IN (" + StrIDMDHIERESC + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDHIERESC.IDMDHIERESC.FieldName, StrIDMDHIERESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERESC.IDMDHIERESC.FieldName, IDMDHIERESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDHIERESC_DEL_1   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDHIERESC_DEL_1", "MDHIERESC_DEL_1 description.");
    UnSQL.SQL.Add(" DELETE MDHIERESC  ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  COMMENTSJE=[COMMENTSJE] AND  ");
    UnSQL.SQL.Add("  IDMDHIERESC=[IDMDHIERESC] AND  ");
    UnSQL.SQL.Add("  TITLEHIERESC=[TITLEHIERESC] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDHIERESC_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDHIERESC_DEL=function(MDHIERESC)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERESC.IDMDHIERESC.FieldName, MDHIERESC.IDMDHIERESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDHIERESC.COMMENTSJE.FieldName, MDHIERESC.COMMENTSJE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDHIERESC.TITLEHIERESC.FieldName, MDHIERESC.TITLEHIERESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDHIERESC_DEL_2   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDHIERESC_DEL_2", "MDHIERESC_DEL_2 description.");
    UnSQL.SQL.Add(" DELETE MDHIERESC  ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  COMMENTSJE=[COMMENTSJE] AND  ");
    UnSQL.SQL.Add("  IDMDHIERESC=[IDMDHIERESC] AND  ");
    UnSQL.SQL.Add("  TITLEHIERESC=[TITLEHIERESC] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDHIERESC_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
    
//**********************   METHODS   ********************************************************************************************

//DataSet Function 

//List Function 
Persistence.Model.Methods.MDHIERESC_ListSetID=function(MDHIERESCList, IDMDHIERESC)
{
    for (i = 0; i < MDHIERESCList.length; i++)
    {

        if (IDMDHIERESC == MDHIERESCList[i].IDMDHIERESC)
            return (MDHIERESCList[i]);

    }

    var TMDHIERESC = new Persistence.Properties.TMDHIERESC();
    TMDHIERESC.IDMDHIERESC = IDMDHIERESC; 
    return (TMDHIERESC); 
     
}

Persistence.Model.Methods.MDHIERESC_ListAdd=function(MDHIERESCList, MDHIERESC)
{
    var i = Persistence.Model.Methods.MDHIERESC_ListGetIndex(MDHIERESCList, MDHIERESC);
    if (i == -1) MDHIERESCList.push(MDHIERESC);
    else
    {
        MDHIERESCList[i].COMMENTSJE = MDHIERESC.COMMENTSJE;
        MDHIERESCList[i].IDMDHIERESC = MDHIERESC.IDMDHIERESC;
        MDHIERESCList[i].TITLEHIERESC = MDHIERESC.TITLEHIERESC;
    }
}
Persistence.Model.Methods.MDHIERESC_ListGetIndex=function(MDHIERESCList, MDHIERESC)
{
    for (i = 0; i < MDHIERESCList.length; i++)
    {
        if (MDHIERESC.IDMDHIERESC == MDHIERESCList[i].IDMDHIERESC)
            return (i);
    }
    return (-1);
}
Persistence.Model.Methods.MDHIERESC_ListGetIndex=function(MDHIERESCList, IDMDHIERESC)
{
    for (i = 0; i < MDHIERESCList.length; i++)
    {
        if (IDMDHIERESC == MDHIERESCList[i].IDMDHIERESC)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.Model.Methods.MDHIERESCListtoStr=function(MDHIERESCList)
{
    var Res = Persistence.Model.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(MDHIERESCList.length, Longitud, Texto);
        for (Counter = 0; Counter < MDHIERESCList.length; Counter++) {
            var UnMDHIERESC = MDHIERESCList[Counter];
            UnMDHIERESC.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Model.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.Model.Methods.StrtoMDHIERESC=function(ProtocoloStr, MDHIERESCList)
{
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        MDHIERESCList.length = 0;
        if (Persistence.Model.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnMDHIERESC = new Persistence.Demo.Properties.TMDHIERESC(); //Mode new row
                UnMDHIERESC.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.MDHIERESC_ListAdd(MDHIERESCList, UnMDHIERESC);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
    
}

//Socket Function 
Persistence.Model.Methods.MDHIERESCListToByte=function(MDHIERESCList, MemStream)
{
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, MDHIERESCList.length);
    for (i = 0; i < MDHIERESCList.length; i++)
    {
        MDHIERESCList[i].ToByte(MemStream);
    }
}
Persistence.Model.Methods.yteToMDHIERESCList=function(MDHIERESCList, MemStream)
{
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++)
    {
        var UnMDHIERESC = new Persistence.Model.Properties.TMDHIERESC();
        MDHIERESCList[i].ToByte(MemStream);
    }
}
 