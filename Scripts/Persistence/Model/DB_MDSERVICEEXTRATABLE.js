﻿//TreduceDB
//Persistence.Model.Properties
//Persistence.Model.Methods

 
Persistence.Model.Properties.TMDSERVICEEXTRATABLE = function()
{
    this.EXTRATABLE_DESCRIPTION = "";
    this.EXTRATABLE_ENABLED  = false;
    this.EXTRATABLE_NAME  = "";
    this.EXTRATABLE_NAMETABLE  = "";
    this.EXTRATABLE_IDSOURCE = UsrCfg.CMDB.Properties.TCMDBCIDEFINEEXTRATABLE_SOURCETYPE.GetEnum(undefined);
    this.EXTRATABLE_LOG  = false;
    this.EXTRATABLE_ORDER  = 0;
    this.IDMDSERVICEEXTRATABLE  = 0;
    this.IDMDSERVICETYPE  = 0;
    //Socket IO Properties
    this.ToByte = function(MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.EXTRATABLE_DESCRIPTION);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.EXTRATABLE_ENABLED);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.EXTRATABLE_NAME);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.EXTRATABLE_NAMETABLE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.EXTRATABLE_IDSOURCE.value);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.EXTRATABLE_LOG);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.EXTRATABLE_ORDER);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDSERVICEEXTRATABLE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDSERVICETYPE);
    }
    this.ByteTo = function(MemStream)
    {
        this.EXTRATABLE_DESCRIPTION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.EXTRATABLE_ENABLED = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.EXTRATABLE_NAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.EXTRATABLE_NAMETABLE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.EXTRATABLE_IDSOURCE =  UsrCfg.CMDB.Properties.TCMDBCIDEFINEEXTRATABLE_SOURCETYPE.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        this.EXTRATABLE_LOG = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.EXTRATABLE_ORDER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDSERVICEEXTRATABLE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDSERVICETYPE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    }
    //Str IO Properties
    this.ToStr = function(Longitud, Texto)
    {
        SysCfg.Str.Protocol.WriteStr(this.EXTRATABLE_DESCRIPTION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteBool(this.EXTRATABLE_ENABLED, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.EXTRATABLE_NAME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.EXTRATABLE_NAMETABLE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.EXTRATABLE_IDSOURCE.value, Longitud, Texto);
        SysCfg.Str.Protocol.WriteBool(this.EXTRATABLE_LOG, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.EXTRATABLE_ORDER, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDSERVICEEXTRATABLE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDSERVICETYPE, Longitud, Texto);
    }
    this.StrTo = function(Pos, Index, LongitudArray, Texto)
    {
        this.EXTRATABLE_DESCRIPTION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.EXTRATABLE_ENABLED = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);
        this.EXTRATABLE_NAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.EXTRATABLE_NAMETABLE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.EXTRATABLE_IDSOURCE = UsrCfg.CMDB.Properties.TCMDBCIDEFINEEXTRATABLE_SOURCETYPE.GetEnum(SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto));
        this.EXTRATABLE_LOG = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);
        this.EXTRATABLE_ORDER = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDSERVICEEXTRATABLE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDSERVICETYPE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
    }
}   

//**********************   METHODS server  ********************************************************************************************

    
//DataSet Function 
Persistence.Model.Methods.MDSERVICEEXTRATABLE_Fill=function(MDSERVICEEXTRATABLEList, StrIDMDSERVICEEXTRATABLE/* IDMDSERVICEEXTRATABLE*/)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    MDSERVICEEXTRATABLEList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDMDSERVICEEXTRATABLE == "")
    {
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE.FieldName, " = " + UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else
    {
        StrIDMDSERVICEEXTRATABLE = " IN (" + StrIDMDSERVICEEXTRATABLE + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE.FieldName, StrIDMDSERVICEEXTRATABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //Param.AddInt32(UsrCfg.InternoDataLinkNames.MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE.FieldName, IDMDSERVICEEXTRATABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try
    {
        /*

    //********   MDSERVICEEXTRATABLE_GET   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDSERVICEEXTRATABLE_GET", "MDSERVICEEXTRATABLE_GET description.");
    UnSQL.SQL.Add(" SELECT  ");
    UnSQL.SQL.Add("  MDSERVICEEXTRATABLE.EXTRATABLE_DESCRIPTION, ");
    UnSQL.SQL.Add("  MDSERVICEEXTRATABLE.EXTRATABLE_ENABLED, ");
    UnSQL.SQL.Add("  MDSERVICEEXTRATABLE.EXTRATABLE_NAME, ");
    UnSQL.SQL.Add("  MDSERVICEEXTRATABLE.EXTRATABLE_NAMETABLE, ");
    UnSQL.SQL.Add("  MDSERVICEEXTRATABLE.EXTRATABLE_IDSOURCE, ");* 
    UnSQL.SQL.Add("  MDSERVICEEXTRATABLE.EXTRATABLE_LOG, ");* * 
    UnSQL.SQL.Add("  MDSERVICEEXTRATABLE.EXTRATABLE_ORDER, ");
    UnSQL.SQL.Add("  MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE, ");
    UnSQL.SQL.Add("  MDSERVICEEXTRATABLE.IDMDSERVICETYPE ");
    UnSQL.SQL.Add(" FROM  MDSERVICEEXTRATABLE ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  MDSERVICEEXTRATABLE.EXTRATABLE_DESCRIPTION=[EXTRATABLE_DESCRIPTION] and  ");
    UnSQL.SQL.Add("  MDSERVICEEXTRATABLE.EXTRATABLE_ENABLED=[EXTRATABLE_ENABLED] and  ");
    UnSQL.SQL.Add("  MDSERVICEEXTRATABLE.EXTRATABLE_NAME=[EXTRATABLE_NAME] and  ");
    UnSQL.SQL.Add("  MDSERVICEEXTRATABLE.EXTRATABLE_NAMETABLE=[EXTRATABLE_NAMETABLE] and  ");
    UnSQL.SQL.Add("  MDSERVICEEXTRATABLE.EXTRATABLE_IDSOURCE=[EXTRATABLE_IDSOURCE] and ");
    UnSQL.SQL.Add("  MDSERVICEEXTRATABLE.EXTRATABLE_LOG=[EXTRATABLE_LOG] and ");* 
    UnSQL.SQL.Add("  MDSERVICEEXTRATABLE.EXTRATABLE_ORDER=[EXTRATABLE_ORDER] and  ");
    UnSQL.SQL.Add("  MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE=[IDMDSERVICEEXTRATABLE] and  ");
    UnSQL.SQL.Add("  MDSERVICEEXTRATABLE.IDMDSERVICETYPE=[IDMDSERVICETYPE] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_MDSERVICEEXTRATABLE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDSERVICEEXTRATABLE_GET", Param.ToBytes());
        ResErr = DS_MDSERVICEEXTRATABLE.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDSERVICEEXTRATABLE.DataSet.RecordCount > 0)
            {
                DS_MDSERVICEEXTRATABLE.DataSet.First();
                while (!(DS_MDSERVICEEXTRATABLE.DataSet.Eof))
                {
                    var MDSERVICEEXTRATABLE = new Persistence.Model.Properties.TMDSERVICEEXTRATABLE();
                    MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE = DS_MDSERVICEEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE.FieldName).asInt32();
                    //Other
                    MDSERVICEEXTRATABLE.EXTRATABLE_DESCRIPTION = DS_MDSERVICEEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_DESCRIPTION.FieldName).asString();
                    MDSERVICEEXTRATABLE.EXTRATABLE_ENABLED = DS_MDSERVICEEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_ENABLED.FieldName).asBoolean();
                    MDSERVICEEXTRATABLE.EXTRATABLE_NAME = DS_MDSERVICEEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_NAME.FieldName).asString();
                    MDSERVICEEXTRATABLE.EXTRATABLE_NAMETABLE = DS_MDSERVICEEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_NAMETABLE.FieldName).asString();
                    MDSERVICEEXTRATABLE.EXTRATABLE_IDSOURCE = UsrCfg.CMDB.Properties.TCMDBCIDEFINEEXTRATABLE_SOURCETYPE.GetEnum(DS_MDSERVICEEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_IDSOURCE.FieldName).asInt32());
                    MDSERVICEEXTRATABLE.EXTRATABLE_LOG = DS_MDSERVICEEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_LOG.FieldName).asBoolean();
                    MDSERVICEEXTRATABLE.EXTRATABLE_ORDER = DS_MDSERVICEEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_ORDER.FieldName).asInt32();
                    MDSERVICEEXTRATABLE.IDMDSERVICETYPE = DS_MDSERVICEEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.IDMDSERVICETYPE.FieldName).asInt32();
                    MDSERVICEEXTRATABLEList.push(MDSERVICEEXTRATABLE);

                    DS_MDSERVICEEXTRATABLE.DataSet.Next();
                }
            }
            else
            {
                DS_MDSERVICEEXTRATABLE.ResErr.NotError = false;
                DS_MDSERVICEEXTRATABLE.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDSERVICEEXTRATABLE_GETID=function( MDSERVICEEXTRATABLE)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE.FieldName, MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_DESCRIPTION.FieldName, MDSERVICEEXTRATABLE.EXTRATABLE_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_ENABLED.FieldName, MDSERVICEEXTRATABLE.EXTRATABLE_ENABLED, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_NAME.FieldName, MDSERVICEEXTRATABLE.EXTRATABLE_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_NAMETABLE.FieldName, MDSERVICEEXTRATABLE.EXTRATABLE_NAMETABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);                
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_ORDER.FieldName, MDSERVICEEXTRATABLE.EXTRATABLE_ORDER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.IDMDSERVICETYPE.FieldName, MDSERVICEEXTRATABLE.IDMDSERVICETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDSERVICEEXTRATABLE_GETID   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDSERVICEEXTRATABLE_GETID", "MDSERVICEEXTRATABLE_GETID description.");
    UnSQL.SQL.Add(" SELECT  ");
    UnSQL.SQL.Add("  MDSERVICEEXTRATABLE.EXTRATABLE_DESCRIPTION, ");
    UnSQL.SQL.Add("  MDSERVICEEXTRATABLE.EXTRATABLE_ENABLED, ");
    UnSQL.SQL.Add("  MDSERVICEEXTRATABLE.EXTRATABLE_NAME, ");
    UnSQL.SQL.Add("  MDSERVICEEXTRATABLE.EXTRATABLE_NAMETABLE, ");
    UnSQL.SQL.Add("  MDSERVICEEXTRATABLE.EXTRATABLE_IDSOURCE, ");* 
    UnSQL.SQL.Add("  MDSERVICEEXTRATABLE.EXTRATABLE_LOG, ");* * 
    UnSQL.SQL.Add("  MDSERVICEEXTRATABLE.EXTRATABLE_ORDER, ");
    UnSQL.SQL.Add("  MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE, ");
    UnSQL.SQL.Add("  MDSERVICEEXTRATABLE.IDMDSERVICETYPE ");
    UnSQL.SQL.Add(" FROM  MDSERVICEEXTRATABLE ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  MDSERVICEEXTRATABLE.EXTRATABLE_DESCRIPTION=[EXTRATABLE_DESCRIPTION] and  ");
    UnSQL.SQL.Add("  MDSERVICEEXTRATABLE.EXTRATABLE_ENABLED=[EXTRATABLE_ENABLED] and  ");
    UnSQL.SQL.Add("  MDSERVICEEXTRATABLE.EXTRATABLE_NAME=[EXTRATABLE_NAME] and  ");
    UnSQL.SQL.Add("  MDSERVICEEXTRATABLE.EXTRATABLE_NAMETABLE=[EXTRATABLE_NAMETABLE] and  ");            
    UnSQL.SQL.Add("  MDSERVICEEXTRATABLE.EXTRATABLE_ORDER=[EXTRATABLE_ORDER] and  ");
    UnSQL.SQL.Add("  MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE=[IDMDSERVICEEXTRATABLE] and  ");
    UnSQL.SQL.Add("  MDSERVICEEXTRATABLE.IDMDSERVICETYPE=[IDMDSERVICETYPE] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_MDSERVICEEXTRATABLE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDSERVICEEXTRATABLE_GETID", Param.ToBytes());
        ResErr = DS_MDSERVICEEXTRATABLE.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDSERVICEEXTRATABLE.DataSet.RecordCount > 0)
            {
                DS_MDSERVICEEXTRATABLE.DataSet.First();
                MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE = DS_MDSERVICEEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE.FieldName).asInt32();
                //Other
                MDSERVICEEXTRATABLE.EXTRATABLE_DESCRIPTION = DS_MDSERVICEEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_DESCRIPTION.FieldName).asString();
                MDSERVICEEXTRATABLE.EXTRATABLE_ENABLED = DS_MDSERVICEEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_ENABLED.FieldName).asBoolean();
                MDSERVICEEXTRATABLE.EXTRATABLE_NAME = DS_MDSERVICEEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_NAME.FieldName).asString();
                MDSERVICEEXTRATABLE.EXTRATABLE_NAMETABLE = DS_MDSERVICEEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_NAMETABLE.FieldName).asString();
                MDSERVICEEXTRATABLE.EXTRATABLE_IDSOURCE = UsrCfg.CMDB.Properties.TCMDBCIDEFINEEXTRATABLE_SOURCETYPE.GetEnum(DS_MDSERVICEEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_IDSOURCE.FieldName).asInt32());
                MDSERVICEEXTRATABLE.EXTRATABLE_LOG = DS_MDSERVICEEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_LOG.FieldName).asBoolean();
                MDSERVICEEXTRATABLE.EXTRATABLE_ORDER = DS_MDSERVICEEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_ORDER.FieldName).asInt32();
                MDSERVICEEXTRATABLE.IDMDSERVICETYPE = DS_MDSERVICEEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.IDMDSERVICETYPE.FieldName).asInt32();
            }
            else
            {
                DS_MDSERVICEEXTRATABLE.ResErr.NotError = false;
                DS_MDSERVICEEXTRATABLE.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDSERVICEEXTRATABLE_ADD=function(MDSERVICEEXTRATABLE)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE.FieldName, MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_DESCRIPTION.FieldName, MDSERVICEEXTRATABLE.EXTRATABLE_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_ENABLED.FieldName, MDSERVICEEXTRATABLE.EXTRATABLE_ENABLED, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_NAME.FieldName, MDSERVICEEXTRATABLE.EXTRATABLE_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_NAMETABLE.FieldName, MDSERVICEEXTRATABLE.EXTRATABLE_NAMETABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_IDSOURCE.FieldName, MDSERVICEEXTRATABLE.EXTRATABLE_IDSOURCE.value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_LOG.FieldName, MDSERVICEEXTRATABLE.EXTRATABLE_LOG, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_ORDER.FieldName, MDSERVICEEXTRATABLE.EXTRATABLE_ORDER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.IDMDSERVICETYPE.FieldName, MDSERVICEEXTRATABLE.IDMDSERVICETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDSERVICEEXTRATABLE_ADD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDSERVICEEXTRATABLE_ADD", "MDSERVICEEXTRATABLE_ADD description.");
    UnSQL.SQL.Add(" INSERT INTO MDSERVICEEXTRATABLE( ");
    UnSQL.SQL.Add("  EXTRATABLE_DESCRIPTION,  ");
    UnSQL.SQL.Add("  EXTRATABLE_ENABLED,  ");
    UnSQL.SQL.Add("  EXTRATABLE_NAME,  ");
    UnSQL.SQL.Add("  EXTRATABLE_NAMETABLE,  ");
    UnSQL.SQL.Add("  EXTRATABLE_IDSOURCE,  ");                 
    UnSQL.SQL.Add("  EXTRATABLE_LOG,  ");                 * 
    UnSQL.SQL.Add("  EXTRATABLE_ORDER,  ");
    UnSQL.SQL.Add("  IDMDSERVICEEXTRATABLE,  ");
    UnSQL.SQL.Add("  IDMDSERVICETYPE ");
    UnSQL.SQL.Add(" )  ");
    UnSQL.SQL.Add(" VALUES ( ");
    UnSQL.SQL.Add("  [EXTRATABLE_DESCRIPTION], ");
    UnSQL.SQL.Add("  [EXTRATABLE_ENABLED], ");
    UnSQL.SQL.Add("  [EXTRATABLE_NAME], ");
    UnSQL.SQL.Add("  [EXTRATABLE_NAMETABLE], ");
    UnSQL.SQL.Add("  [EXTRATABLE_IDSOURCE], ");
    UnSQL.SQL.Add("  [EXTRATABLE_LOG], ");* 
    UnSQL.SQL.Add("  [EXTRATABLE_ORDER], ");
    UnSQL.SQL.Add("  [IDMDSERVICEEXTRATABLE], ");
    UnSQL.SQL.Add("  [IDMDSERVICETYPE] ");
    UnSQL.SQL.Add(" ) ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDSERVICEEXTRATABLE_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            MDSERVICEEXTRATABLE_GETID(MDSERVICEEXTRATABLE);
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDSERVICEEXTRATABLE_UPD=function(MDSERVICEEXTRATABLE)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE.FieldName, MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_DESCRIPTION.FieldName, MDSERVICEEXTRATABLE.EXTRATABLE_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_ENABLED.FieldName, MDSERVICEEXTRATABLE.EXTRATABLE_ENABLED, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_NAME.FieldName, MDSERVICEEXTRATABLE.EXTRATABLE_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_IDSOURCE.FieldName, MDSERVICEEXTRATABLE.EXTRATABLE_IDSOURCE.value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_LOG.FieldName, MDSERVICEEXTRATABLE.EXTRATABLE_LOG, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_NAMETABLE.FieldName, MDSERVICEEXTRATABLE.EXTRATABLE_NAMETABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_ORDER.FieldName, MDSERVICEEXTRATABLE.EXTRATABLE_ORDER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.IDMDSERVICETYPE.FieldName, MDSERVICEEXTRATABLE.IDMDSERVICETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDSERVICEEXTRATABLE_UPD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDSERVICEEXTRATABLE_UPD", "MDSERVICEEXTRATABLE_UPD description.");
    UnSQL.SQL.Add(" UPDATE MDSERVICEEXTRATABLE SET ");
    UnSQL.SQL.Add("  EXTRATABLE_DESCRIPTION=[EXTRATABLE_DESCRIPTION],  ");
    UnSQL.SQL.Add("  EXTRATABLE_ENABLED=[EXTRATABLE_ENABLED],  ");
    UnSQL.SQL.Add("  EXTRATABLE_NAME=[EXTRATABLE_NAME],  ");
    UnSQL.SQL.Add("  EXTRATABLE_NAMETABLE=[EXTRATABLE_NAMETABLE],  ");
    UnSQL.SQL.Add("  EXTRATABLE_IDSOURCE=[EXTRATABLE_IDSOURCE],  ");
    UnSQL.SQL.Add("  EXTRATABLE_LOG=[EXTRATABLE_LOG],  ");* 
    UnSQL.SQL.Add("  EXTRATABLE_ORDER=[EXTRATABLE_ORDER],  ");
    UnSQL.SQL.Add("  IDMDSERVICEEXTRATABLE=[IDMDSERVICEEXTRATABLE],  ");
    UnSQL.SQL.Add("  IDMDSERVICETYPE=[IDMDSERVICETYPE] ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  EXTRATABLE_DESCRIPTION=[EXTRATABLE_DESCRIPTION] AND  ");
    UnSQL.SQL.Add("  EXTRATABLE_ENABLED=[EXTRATABLE_ENABLED] AND  ");
    UnSQL.SQL.Add("  EXTRATABLE_NAME=[EXTRATABLE_NAME] AND  ");            
    UnSQL.SQL.Add("  EXTRATABLE_NAMETABLE=[EXTRATABLE_NAMETABLE] AND  ");
    UnSQL.SQL.Add("  EXTRATABLE_IDSOURCE=[EXTRATABLE_IDSOURCE] AND  "); 
    UnSQL.SQL.Add("  EXTRATABLE_LOG=[EXTRATABLE_LOG] AND  ");  
    UnSQL.SQL.Add("  EXTRATABLE_ORDER=[EXTRATABLE_ORDER] AND  ");
    UnSQL.SQL.Add("  IDMDSERVICEEXTRATABLE=[IDMDSERVICEEXTRATABLE] AND  ");
    UnSQL.SQL.Add("  IDMDSERVICETYPE=[IDMDSERVICETYPE] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDSERVICEEXTRATABLE_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            //GET(); 
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDSERVICEEXTRATABLE_DEL=function(/* StrIDMDSERVICEEXTRATABLE*/ IDMDSERVICEEXTRATABLE)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        /*if (StrIDMDSERVICEEXTRATABLE == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE.FieldName, " = " + UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDMDSERVICEEXTRATABLE = " IN (" + StrIDMDSERVICEEXTRATABLE + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE.FieldName, StrIDMDSERVICEEXTRATABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE.FieldName, IDMDSERVICEEXTRATABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDSERVICEEXTRATABLE_DEL_1   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDSERVICEEXTRATABLE_DEL_1", "MDSERVICEEXTRATABLE_DEL_1 description.");
    UnSQL.SQL.Add(" DELETE MDSERVICEEXTRATABLE  ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  EXTRATABLE_DESCRIPTION=[EXTRATABLE_DESCRIPTION] AND  ");
    UnSQL.SQL.Add("  EXTRATABLE_ENABLED=[EXTRATABLE_ENABLED] AND  ");
    UnSQL.SQL.Add("  EXTRATABLE_NAME=[EXTRATABLE_NAME] AND  ");
    UnSQL.SQL.Add("  EXTRATABLE_NAMETABLE=[EXTRATABLE_NAMETABLE] AND  ");
    UnSQL.SQL.Add("  EXTRATABLE_IDSOURCE=[EXTRATABLE_IDSOURCE] AND  ");
    UnSQL.SQL.Add("  EXTRATABLE_LOG=[EXTRATABLE_LOG] AND  "); 
    UnSQL.SQL.Add("  EXTRATABLE_ORDER=[EXTRATABLE_ORDER] AND  ");
    UnSQL.SQL.Add("  IDMDSERVICEEXTRATABLE=[IDMDSERVICEEXTRATABLE] AND  ");
    UnSQL.SQL.Add("  IDMDSERVICETYPE=[IDMDSERVICETYPE] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDSERVICEEXTRATABLE_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDSERVICEEXTRATABLE_DEL=function(MDSERVICEEXTRATABLE)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE.FieldName, MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_DESCRIPTION.FieldName, MDSERVICEEXTRATABLE.EXTRATABLE_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_ENABLED.FieldName, MDSERVICEEXTRATABLE.EXTRATABLE_ENABLED, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_NAME.FieldName, MDSERVICEEXTRATABLE.EXTRATABLE_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_NAMETABLE.FieldName, MDSERVICEEXTRATABLE.EXTRATABLE_NAMETABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_ORDER.FieldName, MDSERVICEEXTRATABLE.EXTRATABLE_ORDER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.IDMDSERVICETYPE.FieldName, MDSERVICEEXTRATABLE.IDMDSERVICETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDSERVICEEXTRATABLE_DEL_2   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDSERVICEEXTRATABLE_DEL_2", "MDSERVICEEXTRATABLE_DEL_2 description.");
    UnSQL.SQL.Add(" DELETE MDSERVICEEXTRATABLE  ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  EXTRATABLE_DESCRIPTION=[EXTRATABLE_DESCRIPTION] AND  ");
    UnSQL.SQL.Add("  EXTRATABLE_ENABLED=[EXTRATABLE_ENABLED] AND  ");
    UnSQL.SQL.Add("  EXTRATABLE_NAME=[EXTRATABLE_NAME] AND  ");
    UnSQL.SQL.Add("  EXTRATABLE_NAMETABLE=[EXTRATABLE_NAMETABLE] AND  ");
    UnSQL.SQL.Add("  EXTRATABLE_IDSOURCE=[EXTRATABLE_IDSOURCE] AND  ");
    UnSQL.SQL.Add("  EXTRATABLE_LOG=[EXTRATABLE_LOG] AND  "); 
    UnSQL.SQL.Add("  EXTRATABLE_ORDER=[EXTRATABLE_ORDER] AND  ");
    UnSQL.SQL.Add("  IDMDSERVICEEXTRATABLE=[IDMDSERVICEEXTRATABLE] AND  ");
    UnSQL.SQL.Add("  IDMDSERVICETYPE=[IDMDSERVICETYPE] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDSERVICEEXTRATABLE_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
    

//**********************   METHODS   ********************************************************************************************

    
//DataSet Function 

//List Function 
Persistence.Model.Methods.MDSERVICEEXTRATABLE_ListSetID=function(MDSERVICEEXTRATABLEList, IDMDSERVICEEXTRATABLE)
{
    for (i = 0; i < MDSERVICEEXTRATABLEList.length; i++)
    {

        if (IDMDSERVICEEXTRATABLE == MDSERVICEEXTRATABLEList[i].IDMDSERVICEEXTRATABLE)
            return (MDSERVICEEXTRATABLEList[i]);

    }
     
    var TMDSERVICEEXTRATABLE = new Persistence.Properties.TMDSERVICEEXTRATABLE();
    TMDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE = IDMDSERVICEEXTRATABLE; 
    return (TMDSERVICEEXTRATABLE); 
}
Persistence.Model.Methods.MDSERVICEEXTRATABLE_ListAdd=function(MDSERVICEEXTRATABLEList, MDSERVICEEXTRATABLE)
{
    var i = Persistence.Model.Methods.MDSERVICEEXTRATABLE_ListGetIndex(MDSERVICEEXTRATABLEList, MDSERVICEEXTRATABLE);
    if (i == -1) MDSERVICEEXTRATABLEList.push(MDSERVICEEXTRATABLE);
    else
    {
        MDSERVICEEXTRATABLEList[i].EXTRATABLE_DESCRIPTION = MDSERVICEEXTRATABLE.EXTRATABLE_DESCRIPTION;
        MDSERVICEEXTRATABLEList[i].EXTRATABLE_ENABLED = MDSERVICEEXTRATABLE.EXTRATABLE_ENABLED;
        MDSERVICEEXTRATABLEList[i].EXTRATABLE_NAME = MDSERVICEEXTRATABLE.EXTRATABLE_NAME;
        MDSERVICEEXTRATABLEList[i].EXTRATABLE_NAMETABLE = MDSERVICEEXTRATABLE.EXTRATABLE_NAMETABLE;
        MDSERVICEEXTRATABLEList[i].EXTRATABLE_IDSOURCE = MDSERVICEEXTRATABLE.EXTRATABLE_IDSOURCE;
        MDSERVICEEXTRATABLEList[i].EXTRATABLE_LOG = MDSERVICEEXTRATABLE.EXTRATABLE_LOG;
        MDSERVICEEXTRATABLEList[i].EXTRATABLE_ORDER = MDSERVICEEXTRATABLE.EXTRATABLE_ORDER;
        MDSERVICEEXTRATABLEList[i].IDMDSERVICEEXTRATABLE = MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE;
        MDSERVICEEXTRATABLEList[i].IDMDSERVICETYPE = MDSERVICEEXTRATABLE.IDMDSERVICETYPE;
    }
}
Persistence.Model.Methods.MDSERVICEEXTRATABLE_ListGetIndex=function(MDSERVICEEXTRATABLEList,MDSERVICEEXTRATABLE)
{
    for (i = 0; i < MDSERVICEEXTRATABLEList.length; i++)
    {
        if (MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE == MDSERVICEEXTRATABLEList[i].IDMDSERVICEEXTRATABLE)
            return (i);
    }
    return (-1);
}
Persistence.Model.Methods.MDSERVICEEXTRATABLE_ListGetIndex=function(MDSERVICEEXTRATABLEList,  IDMDSERVICEEXTRATABLE)
{
    for (i = 0; i < MDSERVICEEXTRATABLEList.length; i++)
    {
        if (IDMDSERVICEEXTRATABLE == MDSERVICEEXTRATABLEList[i].IDMDSERVICEEXTRATABLE)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.Model.Methods.MDSERVICEEXTRATABLEListtoStr=function(MDSERVICEEXTRATABLEList)
{
    var Res = Persistence.Model.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(MDSERVICEEXTRATABLEList.length, Longitud, Texto);
        for (Counter = 0; Counter < MDSERVICEEXTRATABLEList.length; Counter++) {
            var UnMDSERVICEEXTRATABLE = MDSERVICEEXTRATABLEList[Counter];
            UnMDSERVICEEXTRATABLE.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Model.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.Model.Methods.StrtoMDSERVICEEXTRATABLE=function(ProtocoloStr, MDSERVICEEXTRATABLEList)
{
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        MDSERVICEEXTRATABLEList.length = 0;
        if (Persistence.Model.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnMDSERVICEEXTRATABLE = new Persistence.Demo.Properties.TMDSERVICEEXTRATABLE(); //Mode new row
                UnMDSERVICEEXTRATABLE.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.MDSERVICEEXTRATABLE_ListAdd(MDSERVICEEXTRATABLEList, UnMDSERVICEEXTRATABLE);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
    
}

//Socket Function 
Persistence.Model.Methods.MDSERVICEEXTRATABLEListToByte=function(MDSERVICEEXTRATABLEList, MemStream)
{
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, MDSERVICEEXTRATABLEList.length);
    for (i = 0; i < MDSERVICEEXTRATABLEList.length; i++)
    {
        MDSERVICEEXTRATABLEList[i].ToByte(MemStream);
    }
}
Persistence.Model.Methods.ByteToMDSERVICEEXTRATABLEList=function(MDSERVICEEXTRATABLEList, MemStream)
{
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++)
    {
        var UnMDSERVICEEXTRATABLE = new Persistence.Model.Properties.TMDSERVICEEXTRATABLE();
        MDSERVICEEXTRATABLEList[i].ToByte(MemStream);
    }
}
 