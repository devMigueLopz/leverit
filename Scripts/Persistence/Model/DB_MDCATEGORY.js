﻿//TreduceDB
//Persistence.Model.Properties
//Persistence.Model.Methods

Persistence.Model.Properties.TMDCATEGORY = function()
{
     
    this.CATEGORY1 = "";  
    this.CATEGORY2 = ""; 
    this.CATEGORY3 = "";  
    this.CATEGORY4 = "";  
    this.CATEGORY5 = ""; 
    this.CATEGORY6 = "";  
    this.CATEGORY7 = ""; 
    this.CATEGORY8 = ""; 
    this.IDMDCATEGORY = 0;
            
    this.ToPath = function()
    {
        var path = "";
        path = (this.CATEGORY1 != "" ? this.CATEGORY1 : "");
        path += (this.CATEGORY2 != "" ? "\\" + this.CATEGORY2 : "");
        path += (this.CATEGORY3 != "" ? "\\" + this.CATEGORY3 : "");
        path += (this.CATEGORY4 != "" ? "\\" + this.CATEGORY4 : "");
        path += (this.CATEGORY5 != "" ? "\\" + this.CATEGORY5 : "");
        path += (this.CATEGORY6 != "" ? "\\" + this.CATEGORY6 : "");
        path += (this.CATEGORY7 != "" ? "\\" + this.CATEGORY7 : "");
        path += (this.CATEGORY8 != "" ? "\\" + this.CATEGORY8 : "");
        return path;
    }

    this.ToStringList = function()
    {
        var StringList = new Array(); 
        if (this.CATEGORY1 != "") StringList.push(this.CATEGORY1); else return(StringList);
        if (this.CATEGORY2 != "") StringList.push(this.CATEGORY2); else return(StringList);
        if (this.CATEGORY3 != "") StringList.push(this.CATEGORY3); else return(StringList);
        if (this.CATEGORY4 != "") StringList.push(this.CATEGORY4); else return(StringList);
        if (this.CATEGORY5 != "") StringList.push(this.CATEGORY5); else return(StringList);
        if (this.CATEGORY6 != "") StringList.push(this.CATEGORY6); else return(StringList);
        if (this.CATEGORY7 != "") StringList.push(this.CATEGORY7); else return(StringList);
        if (this.CATEGORY8 != "") StringList.push(this.CATEGORY8); else return(StringList);
        return (StringList);
    }

    
    this.MDCATEGORYDETAILList = new Array(); //Persistence.Model.Properties.TMDCATEGORYDETAIL
    //Socket IO Properties
    this.ToByte = function(MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CATEGORY1);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CATEGORY2);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CATEGORY3);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CATEGORY4);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CATEGORY5);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CATEGORY6);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CATEGORY7);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CATEGORY8);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDCATEGORY);
    }
    this.ByteTo = function(MemStream)
    {
        CATEGORY1 = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        CATEGORY2 = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        CATEGORY3 = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        CATEGORY4 = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        CATEGORY5 = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        CATEGORY6 = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        CATEGORY7 = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        CATEGORY8 = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        IDMDCATEGORY = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    }
    //Str IO Properties
    this.ToStr = function(Longitud, Texto)
    {
        SysCfg.Str.Protocol.WriteStr(CATEGORY1, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(CATEGORY2, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(CATEGORY3, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(CATEGORY4, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(CATEGORY5, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(CATEGORY6, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(CATEGORY7, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(CATEGORY8, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(IDMDCATEGORY, Longitud, Texto);
    }
    this.StrTo = function(Pos, Index, LongitudArray, Texto)
    {
        CATEGORY1 = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        CATEGORY2 = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        CATEGORY3 = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        CATEGORY4 = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        CATEGORY5 = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        CATEGORY6 = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        CATEGORY7 = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        CATEGORY8 = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        IDMDCATEGORY = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
    }

}
     
//**********************   METHODS server  ********************************************************************************************

   
//DataSet Function 
Persistence.Model.Methods.MDCATEGORY_Fill = function(MDCATEGORYList, StrIDMDCATEGORY/*IDMDCATEGORY*/)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    MDCATEGORYList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
            
    if (StrIDMDCATEGORY == "")
    {
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDCATEGORY.IDMDCATEGORY.FieldName, " = " + UsrCfg.InternoAtisNames.MDCATEGORY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDCATEGORY.IDMDCATEGORY.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else
    {
        StrIDMDCATEGORY = " IN (" + StrIDMDCATEGORY + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDCATEGORY.IDMDCATEGORY.FieldName, StrIDMDCATEGORY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //Param.AddInt32(UsrCfg.InternoAtisNames.MDCATEGORY.IDMDCATEGORY.FieldName, IDMDCATEGORY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try
    {
        var DS_MDCATEGORY = SysCfg.DB.SQL.Methods.OpenDataSet( "Atis",  "MDCATEGORY_GET", Param.ToBytes());
        ResErr = DS_MDCATEGORY.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDCATEGORY.DataSet.RecordCount > 0)
            {
                DS_MDCATEGORY.DataSet.First();
                while (!(DS_MDCATEGORY.DataSet.Eof))
                {
                    var MDCATEGORY = new Persistence.Model.Properties.TMDCATEGORY();

                    MDCATEGORY.IDMDCATEGORY = DS_MDCATEGORY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCATEGORY.IDMDCATEGORY.FieldName).asInt32();
                    //Other
                    MDCATEGORY.CATEGORY1 = DS_MDCATEGORY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY1.FieldName).asString();
                    MDCATEGORY.CATEGORY2 = DS_MDCATEGORY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY2.FieldName).asString();
                    MDCATEGORY.CATEGORY3 = DS_MDCATEGORY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY3.FieldName).asString();
                    MDCATEGORY.CATEGORY4 = DS_MDCATEGORY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY4.FieldName).asString();
                    MDCATEGORY.CATEGORY5 = DS_MDCATEGORY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY5.FieldName).asString();
                    MDCATEGORY.CATEGORY6 = DS_MDCATEGORY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY6.FieldName).asString();
                    MDCATEGORY.CATEGORY7 = DS_MDCATEGORY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY7.FieldName).asString();
                    MDCATEGORY.CATEGORY8 = DS_MDCATEGORY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY8.FieldName).asString();
                    MDCATEGORYList.push(MDCATEGORY);
                    DS_MDCATEGORY.DataSet.Next();
                }
            }
            else
            {
                DS_MDCATEGORY.ResErr.NotError = false;
                DS_MDCATEGORY.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}


Persistence.Model.Methods.MDCATEGORY_GETID = function(MDCATEGORY)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCATEGORY.IDMDCATEGORY.FieldName, MDCATEGORY.IDMDCATEGORY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY1.FieldName, MDCATEGORY.CATEGORY1, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY2.FieldName, MDCATEGORY.CATEGORY2, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY3.FieldName, MDCATEGORY.CATEGORY3, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY4.FieldName, MDCATEGORY.CATEGORY4, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY5.FieldName, MDCATEGORY.CATEGORY5, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY6.FieldName, MDCATEGORY.CATEGORY6, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY7.FieldName, MDCATEGORY.CATEGORY7, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY8.FieldName, MDCATEGORY.CATEGORY8, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDCATEGORY_GETID   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL( "MDCATEGORY_GETID",  "MDCATEGORY_GETID description.");
    UnSQL.SQL.Add( " SELECT  ");
    UnSQL.SQL.Add( "  MDCATEGORY.CATEGORY1, ");
    UnSQL.SQL.Add( "  MDCATEGORY.CATEGORY2, ");
    UnSQL.SQL.Add( "  MDCATEGORY.CATEGORY3, ");
    UnSQL.SQL.Add( "  MDCATEGORY.CATEGORY4, ");
    UnSQL.SQL.Add( "  MDCATEGORY.CATEGORY5, ");
    UnSQL.SQL.Add( "  MDCATEGORY.CATEGORY6, ");
    UnSQL.SQL.Add( "  MDCATEGORY.CATEGORY7, ");
    UnSQL.SQL.Add( "  MDCATEGORY.CATEGORY8, ");
    UnSQL.SQL.Add( "  MDCATEGORY.IDMDCATEGORY ");
    UnSQL.SQL.Add( " FROM  MDCATEGORY ");
    UnSQL.SQL.Add( " WHERE   ");
    UnSQL.SQL.Add( "  MDCATEGORY.CATEGORY1= [CATEGORY1] and  ");
    UnSQL.SQL.Add( "  MDCATEGORY.CATEGORY2= [CATEGORY2] and  ");
    UnSQL.SQL.Add( "  MDCATEGORY.CATEGORY3= [CATEGORY3] and  ");
    UnSQL.SQL.Add( "  MDCATEGORY.CATEGORY4= [CATEGORY4] and  ");
    UnSQL.SQL.Add( "  MDCATEGORY.CATEGORY5= [CATEGORY5] and  ");
    UnSQL.SQL.Add( "  MDCATEGORY.CATEGORY6= [CATEGORY6] and  ");
    UnSQL.SQL.Add( "  MDCATEGORY.CATEGORY7= [CATEGORY7] and  ");
    UnSQL.SQL.Add( "  MDCATEGORY.CATEGORY8= [CATEGORY8] and  ");
    UnSQL.SQL.Add( "  MDCATEGORY.IDMDCATEGORY= [IDMDCATEGORY] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var  DS_MDCATEGORY = SysCfg.DB.SQL.Methods.OpenDataSet( "Atis",  "MDCATEGORY_GETID", Param.ToBytes());
        ResErr = DS_MDCATEGORY.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDCATEGORY.DataSet.RecordCount > 0)
            {
                DS_MDCATEGORY.DataSet.First();
                MDCATEGORY.IDMDCATEGORY = DS_MDCATEGORY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCATEGORY.IDMDCATEGORY.FieldName).asInt32();
                //Other
                MDCATEGORY.CATEGORY1 = DS_MDCATEGORY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY1.FieldName).asString();
                MDCATEGORY.CATEGORY2 = DS_MDCATEGORY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY2.FieldName).asString();
                MDCATEGORY.CATEGORY3 = DS_MDCATEGORY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY3.FieldName).asString();
                MDCATEGORY.CATEGORY4 = DS_MDCATEGORY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY4.FieldName).asString();
                MDCATEGORY.CATEGORY5 = DS_MDCATEGORY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY5.FieldName).asString();
                MDCATEGORY.CATEGORY6 = DS_MDCATEGORY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY6.FieldName).asString();
                MDCATEGORY.CATEGORY7 = DS_MDCATEGORY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY7.FieldName).asString();
                MDCATEGORY.CATEGORY8 = DS_MDCATEGORY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY8.FieldName).asString();
            }
            else
            {
                DS_MDCATEGORY.ResErr.NotError = false;
                DS_MDCATEGORY.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDCATEGORY_ADD = function( MDCATEGORY)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCATEGORY.IDMDCATEGORY.FieldName, MDCATEGORY.IDMDCATEGORY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY1.FieldName, MDCATEGORY.CATEGORY1, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY2.FieldName, MDCATEGORY.CATEGORY2, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY3.FieldName, MDCATEGORY.CATEGORY3, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY4.FieldName, MDCATEGORY.CATEGORY4, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY5.FieldName, MDCATEGORY.CATEGORY5, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY6.FieldName, MDCATEGORY.CATEGORY6, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY7.FieldName, MDCATEGORY.CATEGORY7, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY8.FieldName, MDCATEGORY.CATEGORY8, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDCATEGORY_ADD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL( "MDCATEGORY_ADD",  "MDCATEGORY_ADD description.");
    UnSQL.SQL.Add( " INSERT INTO MDCATEGORY( ");
    UnSQL.SQL.Add( "  CATEGORY1,  ");
    UnSQL.SQL.Add( "  CATEGORY2,  ");
    UnSQL.SQL.Add( "  CATEGORY3,  ");
    UnSQL.SQL.Add( "  CATEGORY4,  ");
    UnSQL.SQL.Add( "  CATEGORY5,  ");
    UnSQL.SQL.Add( "  CATEGORY6,  ");
    UnSQL.SQL.Add( "  CATEGORY7,  ");
    UnSQL.SQL.Add( "  CATEGORY8,  ");
    UnSQL.SQL.Add( "  IDMDCATEGORY ");
    UnSQL.SQL.Add( " )  ");
    UnSQL.SQL.Add( " VALUES ( ");
    UnSQL.SQL.Add( "   [CATEGORY1], ");
    UnSQL.SQL.Add( "   [CATEGORY2], ");
    UnSQL.SQL.Add( "   [CATEGORY3], ");
    UnSQL.SQL.Add( "   [CATEGORY4], ");
    UnSQL.SQL.Add( "   [CATEGORY5], ");
    UnSQL.SQL.Add( "   [CATEGORY6], ");
    UnSQL.SQL.Add( "   [CATEGORY7], ");
    UnSQL.SQL.Add( "   [CATEGORY8], ");
    UnSQL.SQL.Add( "   [IDMDCATEGORY] ");
    UnSQL.SQL.Add( " ) ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec( "Atis",  "MDCATEGORY_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            MDCATEGORY_GETID(MDCATEGORY);
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDCATEGORY_UPD = function(MDCATEGORY)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCATEGORY.IDMDCATEGORY.FieldName, MDCATEGORY.IDMDCATEGORY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY1.FieldName, MDCATEGORY.CATEGORY1, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY2.FieldName, MDCATEGORY.CATEGORY2, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY3.FieldName, MDCATEGORY.CATEGORY3, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY4.FieldName, MDCATEGORY.CATEGORY4, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY5.FieldName, MDCATEGORY.CATEGORY5, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY6.FieldName, MDCATEGORY.CATEGORY6, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY7.FieldName, MDCATEGORY.CATEGORY7, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY8.FieldName, MDCATEGORY.CATEGORY8, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDCATEGORY_UPD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL( "MDCATEGORY_UPD",  "MDCATEGORY_UPD description.");
    UnSQL.SQL.Add( " UPDATE MDCATEGORY SET ");
    UnSQL.SQL.Add( "  CATEGORY1= [CATEGORY1],  ");
    UnSQL.SQL.Add( "  CATEGORY2= [CATEGORY2],  ");
    UnSQL.SQL.Add( "  CATEGORY3= [CATEGORY3],  ");
    UnSQL.SQL.Add( "  CATEGORY4= [CATEGORY4],  ");
    UnSQL.SQL.Add( "  CATEGORY5= [CATEGORY5],  ");
    UnSQL.SQL.Add( "  CATEGORY6= [CATEGORY6],  ");
    UnSQL.SQL.Add( "  CATEGORY7= [CATEGORY7],  ");
    UnSQL.SQL.Add( "  CATEGORY8= [CATEGORY8],  ");
    UnSQL.SQL.Add( "  IDMDCATEGORY= [IDMDCATEGORY] ");
    UnSQL.SQL.Add( " WHERE   ");
    UnSQL.SQL.Add( "  CATEGORY1= [CATEGORY1] AND  ");
    UnSQL.SQL.Add( "  CATEGORY2= [CATEGORY2] AND  ");
    UnSQL.SQL.Add( "  CATEGORY3= [CATEGORY3] AND  ");
    UnSQL.SQL.Add( "  CATEGORY4= [CATEGORY4] AND  ");
    UnSQL.SQL.Add( "  CATEGORY5= [CATEGORY5] AND  ");
    UnSQL.SQL.Add( "  CATEGORY6= [CATEGORY6] AND  ");
    UnSQL.SQL.Add( "  CATEGORY7= [CATEGORY7] AND  ");
    UnSQL.SQL.Add( "  CATEGORY8= [CATEGORY8] AND  ");
    UnSQL.SQL.Add( "  IDMDCATEGORY= [IDMDCATEGORY] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec( "Atis",  "MDCATEGORY_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            //GET(); 
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.Model.Methods.MDCATEGORY_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.Model.Methods.MDCATEGORY_DELIDMDCATEGORY(/*StrIDMDCATEGORY*/Param);
    }
    else {
        Res = Persistence.Model.Methods.MDCATEGORY_DELMDCATEGORY(Param);
    }
    return (Res);
}

Persistence.Model.Methods.MDCATEGORY_DELIDMDCATEGORY = function (/*StrIDMDCATEGORY*/IDMDCATEGORY)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        /*if (StrIDMDCATEGORY == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDCATEGORY.IDMDCATEGORY.FieldName, " = " + UsrCfg.InternoAtisNames.MDCATEGORY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDCATEGORY.IDMDCATEGORY.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDMDCATEGORY = " IN (" + StrIDMDCATEGORY + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDCATEGORY.IDMDCATEGORY.FieldName, StrIDMDCATEGORY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCATEGORY.IDMDCATEGORY.FieldName, IDMDCATEGORY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDCATEGORY_DEL_1   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL( "MDCATEGORY_DEL_1",  "MDCATEGORY_DEL_1 description.");
    UnSQL.SQL.Add( " DELETE MDCATEGORY  ");
    UnSQL.SQL.Add( " WHERE   ");
    UnSQL.SQL.Add( "  CATEGORY1= [CATEGORY1] AND  ");
    UnSQL.SQL.Add( "  CATEGORY2= [CATEGORY2] AND  ");
    UnSQL.SQL.Add( "  CATEGORY3= [CATEGORY3] AND  ");
    UnSQL.SQL.Add( "  CATEGORY4= [CATEGORY4] AND  ");
    UnSQL.SQL.Add( "  CATEGORY5= [CATEGORY5] AND  ");
    UnSQL.SQL.Add( "  CATEGORY6= [CATEGORY6] AND  ");
    UnSQL.SQL.Add( "  CATEGORY7= [CATEGORY7] AND  ");
    UnSQL.SQL.Add( "  CATEGORY8= [CATEGORY8] AND  ");
    UnSQL.SQL.Add( "  IDMDCATEGORY= [IDMDCATEGORY] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec( "Atis",  "MDCATEGORY_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDCATEGORY_DELMDCATEGORY = function (MDCATEGORY)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCATEGORY.IDMDCATEGORY.FieldName, MDCATEGORY.IDMDCATEGORY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY1.FieldName, MDCATEGORY.CATEGORY1, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY2.FieldName, MDCATEGORY.CATEGORY2, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY3.FieldName, MDCATEGORY.CATEGORY3, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY4.FieldName, MDCATEGORY.CATEGORY4, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY5.FieldName, MDCATEGORY.CATEGORY5, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY6.FieldName, MDCATEGORY.CATEGORY6, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY7.FieldName, MDCATEGORY.CATEGORY7, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY8.FieldName, MDCATEGORY.CATEGORY8, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDCATEGORY_DEL_2   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL( "MDCATEGORY_DEL_2",  "MDCATEGORY_DEL_2 description.");
    UnSQL.SQL.Add( " DELETE MDCATEGORY  ");
    UnSQL.SQL.Add( " WHERE   ");
    UnSQL.SQL.Add( "  CATEGORY1= [CATEGORY1] AND  ");
    UnSQL.SQL.Add( "  CATEGORY2= [CATEGORY2] AND  ");
    UnSQL.SQL.Add( "  CATEGORY3= [CATEGORY3] AND  ");
    UnSQL.SQL.Add( "  CATEGORY4= [CATEGORY4] AND  ");
    UnSQL.SQL.Add( "  CATEGORY5= [CATEGORY5] AND  ");
    UnSQL.SQL.Add( "  CATEGORY6= [CATEGORY6] AND  ");
    UnSQL.SQL.Add( "  CATEGORY7= [CATEGORY7] AND  ");
    UnSQL.SQL.Add( "  CATEGORY8= [CATEGORY8] AND  ");
    UnSQL.SQL.Add( "  IDMDCATEGORY= [IDMDCATEGORY] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec( "Atis",  "MDCATEGORY_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}

    
//**********************   METHODS   ********************************************************************************************

//DataSet Function 

//List Function 
Persistence.Model.Methods.MDCATEGORY_ListSetID = function(MDCATEGORYList, IDMDCATEGORY)
{
    for (i = 0; i < MDCATEGORYList.length; i++)
    {

        if (IDMDCATEGORY == MDCATEGORYList[i].IDMDCATEGORY)
            return (MDCATEGORYList[i]);

    }
    var MDCATEGORY = new Persistence.Model.Properties.TMDCATEGORY();
    MDCATEGORY.IDMDCATEGORY = IDMDCATEGORY;

    return (MDCATEGORY);
}
Persistence.Model.Methods.MDCATEGORY_ListAdd = function(MDCATEGORYList,MDCATEGORY)
{
    var  i = Persistence.Model.Methods.MDCATEGORY_ListGetIndex(MDCATEGORYList, MDCATEGORY);
    if (i == -1) MDCATEGORYList.push(MDCATEGORY);
    else
    {
        MDCATEGORYList[i].CATEGORY1 = MDCATEGORY.CATEGORY1;
        MDCATEGORYList[i].CATEGORY2 = MDCATEGORY.CATEGORY2;
        MDCATEGORYList[i].CATEGORY3 = MDCATEGORY.CATEGORY3;
        MDCATEGORYList[i].CATEGORY4 = MDCATEGORY.CATEGORY4;
        MDCATEGORYList[i].CATEGORY5 = MDCATEGORY.CATEGORY5;
        MDCATEGORYList[i].CATEGORY6 = MDCATEGORY.CATEGORY6;
        MDCATEGORYList[i].CATEGORY7 = MDCATEGORY.CATEGORY7;
        MDCATEGORYList[i].CATEGORY8 = MDCATEGORY.CATEGORY8;
        MDCATEGORYList[i].IDMDCATEGORY = MDCATEGORY.IDMDCATEGORY;                
    }
}
Persistence.Model.Methods.MDCATEGORY_COPY = function(MDCATEGORYSource)
{

    var MDCATEGORY = new Persistence.Model.Properties.TMDCATEGORY();
    MDCATEGORY.CATEGORY1 = MDCATEGORYSource.CATEGORY1;
    MDCATEGORY.CATEGORY2 = MDCATEGORYSource.CATEGORY2;
    MDCATEGORY.CATEGORY3 = MDCATEGORYSource.CATEGORY3;
    MDCATEGORY.CATEGORY4 = MDCATEGORYSource.CATEGORY4;
    MDCATEGORY.CATEGORY5 = MDCATEGORYSource.CATEGORY5;
    MDCATEGORY.CATEGORY6 = MDCATEGORYSource.CATEGORY6;
    MDCATEGORY.CATEGORY7 = MDCATEGORYSource.CATEGORY7;
    MDCATEGORY.CATEGORY8 = MDCATEGORYSource.CATEGORY8;
    MDCATEGORY.IDMDCATEGORY = MDCATEGORYSource.IDMDCATEGORY;            
    return MDCATEGORY;      
}

//CJRC_26072018
Persistence.Model.Methods.MDCATEGORY_ListGetIndex = function (MDCATEGORYList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.Model.Methods.MDCATEGORY_ListGetIndexIDMDCATEGORY(MDCATEGORYList, Param);

    }
    else {
        Res = Persistence.Model.Methods.MDCATEGORY_ListGetIndexMDCATEGORY(MDCATEGORYList, Param);
    }
    return (Res);
}

Persistence.Model.Methods.MDCATEGORY_ListGetIndexMDCATEGORY = function (MDCATEGORYList, MDCATEGORY)
{
    for (i = 0; i < MDCATEGORYList.length; i++)
    {
        if (MDCATEGORY.IDMDCATEGORY == MDCATEGORYList[i].IDMDCATEGORY)
            return (i);
    }
    return (-1);
}
Persistence.Model.Methods.MDCATEGORY_ListGetIndexIDMDCATEGORY = function (MDCATEGORYList, IDMDCATEGORY)
{
    for (i = 0; i < MDCATEGORYList.length; i++)
    {
        if (IDMDCATEGORY == MDCATEGORYList[i].IDMDCATEGORY)
            return (i);
    }
    return (-1);
}

//Function 
Persistence.Model.Methods.MDCATEGORYListtoStr = function(MDCATEGORYList)
{
    var Res = Persistence.Model.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(MDCATEGORYList.length, Longitud, Texto);
        for (Counter = 0; Counter < MDCATEGORYList.length; Counter++) {
            var UnMDCATEGORY = MDCATEGORYList[Counter];
            UnMDCATEGORY.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Model.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.Model.Methods.StrtoMDCATEGORY = function(ProtocoloStr,MDCATEGORYList)
{
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        MDCATEGORYList.length = 0;
        if (Persistence.Model.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnMDCATEGORY = new Persistence.Demo.Properties.TMDCATEGORY(); //Mode new row
                UnMDCATEGORY.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.MDCATEGORY_ListAdd(MDCATEGORYList, UnMDCATEGORY);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
    
}

//Socket Function 
Persistence.Model.Methods.MDCATEGORYListToByte = function(MDCATEGORYList, MemStream, idx)
{
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, MDCATEGORYList.length - idx);
    for (i = idx; i < MDCATEGORYList.length; i++)
    {
        MDCATEGORYList[i].ToByte(MemStream);
    }
}
Persistence.Model.Methods.ByteToMDCATEGORYList = function( MDCATEGORYList, MemStream)
{
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++)
    {
        var UnMDCATEGORY = new Persistence.Model.Properties.TMDCATEGORY();
        UnMDCATEGORY.ByteTo(MemStream);
        Persistence.Model.Methods.MDCATEGORY_ListAdd(MDCATEGORYList, UnMDCATEGORY);
    }
}
  