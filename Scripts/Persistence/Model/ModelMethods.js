﻿//TreduceDB
//Persistence.Model.Methods

// GPQUERY
Persistence.Model.Methods.GETSDCASE_USER = function (inIDUSER) {
    var ResErr = new SysCfg.Error.Properties.TResErr();

    var Res = "";
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE.IDUSER.FieldName, inIDUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {
        var DS_USER = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "GETAUTOCASE_USER", Param.ToBytes());
        ResErr = DS_USER.ResErr;
        if (ResErr.NotError) {
            if (DS_USER.DataSet.RecordCount > 0) {
                DS_USER.DataSet.First();
                if (!(DS_USER.DataSet.Eof)) {
                    Res = DS_USER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASE.IDUSER.FieldName).asString();
                }
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (Res);
}
Persistence.Model.Methods.GETSDCASE_TITLE = function (inIDUSER, inIDMDCATEGORYDETAIL, inIDSDCASE) {
    var ResErr = SysCfg.Error.Properties.TResErr();

    var Res = "";
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();


    Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE.IDSDCASE.FieldName, inIDSDCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    Param.AddInt32(UsrCfg.InternoAtisNames.SDCASEMT.IDMDCATEGORYDETAIL.FieldName, inIDMDCATEGORYDETAIL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE.IDUSER.FieldName, inIDUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {
        var DS_SDCASE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "GETAUTOCASE_TITLE", Param.ToBytes());
        ResErr = DS_SDCASE.ResErr;
        if (ResErr.NotError) {
            if (DS_SDCASE.DataSet.RecordCount > 0) {
                DS_SDCASE.DataSet.First();
                if (!(DS_SDCASE.DataSet.Eof)) {
                    Res = DS_SDCASE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASE.CASE_TITLE.FieldName).asString();
                }
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (Res);
}
Persistence.Model.Methods.GETSDCASE_DESCRIPTION = function (inIDUSER, inIDMDCATEGORYDETAIL, inIDSDCASE) {
    var ResErr = SysCfg.Error.Properties.TResErr();

    var Res = "";
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();


    Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE.IDSDCASE.FieldName, inIDSDCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    Param.AddInt32(UsrCfg.InternoAtisNames.SDCASEMT.IDMDCATEGORYDETAIL.FieldName, inIDMDCATEGORYDETAIL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE.IDUSER.FieldName, inIDUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {
        var DS_SDCASE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "GETAUTOCASE_DESCRIPTION", Param.ToBytes());
        ResErr = DS_SDCASE.ResErr;
        if (ResErr.NotError) {
            if (DS_SDCASE.DataSet.RecordCount > 0) {
                DS_SDCASE.DataSet.First();
                if (!(DS_SDCASE.DataSet.Eof)) {
                    Res = DS_SDCASE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASE.CASE_DESCRIPTION.FieldName).asString();
                }
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (Res);
}
Persistence.Model.Methods.StartReationModel = function (ModelProfiler) {
    /*
    var idx = 0;
    for (var i = 0; i < ModelProfiler.MDCATEGORYList; i++)
   {

       for (var e = idx; e < ModelProfiler.MDCATEGORYDETAILList.length; e++)//0
       {
           idx = e;

           if (ModelProfiler.MDCATEGORYDETAILList[e].IDMDCATEGORY == ModelProfiler.MDCATEGORYList[i].IDMDCATEGORY)
           {
               Persistence.Model.Methods.MDCATEGORYDETAIL_ListAdd(ModelProfiler.MDCATEGORYList[i].MDCATEGORYDETAILList, ModelProfiler.MDCATEGORYDETAILList[e]);
               ModelProfiler.MDCATEGORYDETAILList[e].MDCATEGORY = ModelProfiler.MDCATEGORYList[i];
           }
           else
           {
               if (ModelProfiler.MDCATEGORYDETAILList[e].IDMDCATEGORY > ModelProfiler.MDCATEGORYList[i].IDMDCATEGORY)
               {
                   break;
               }
           }

       }

   }
  */
    var idxC = 0;

    for (var i = 0; i < ModelProfiler.MDCATEGORYList.length; i++) {
        for (var e = idxC; e < ModelProfiler.MDCATEGORYDETAILList.length; e++)//0
        {
            idxC = e;
            if (ModelProfiler.MDCATEGORYDETAILList[e].IDMDCATEGORY == ModelProfiler.MDCATEGORYList[i].IDMDCATEGORY) {
                Persistence.Model.Methods.MDCATEGORYDETAIL_ListAdd(ModelProfiler.MDCATEGORYList[i].MDCATEGORYDETAILList, ModelProfiler.MDCATEGORYDETAILList[e]);
                ModelProfiler.MDCATEGORYDETAILList[e].MDCATEGORY = ModelProfiler.MDCATEGORYList[i];
            }
            if (ModelProfiler.MDCATEGORYDETAILList[e].IDMDCATEGORY > ModelProfiler.MDCATEGORYList[i].IDMDCATEGORY) {
                break;
            }
        }
    }

    var MDCATEGORYDETAILListOrder = SysCfg.CopyList(ModelProfiler.MDCATEGORYDETAILList).sort(function (a, b) { return a.IDMDCATEGORYDETAIL - b.IDMDCATEGORYDETAIL; });//Persistence.Model.Properties.TMDCATEGORYDETAIL            
    var CATEGORYDETAIL_TYPEUSERListOrder = SysCfg.CopyList(ModelProfiler.MDCATEGORYDETAIL_TYPEUSERList).sort(function (a, b) { return a.IDMDCATEGORYDETAIL - b.IDMDCATEGORYDETAIL; });//Persistence.Model.Properties.TMDCATEGORYDETAIL_TYPEUSER
    var idxCT = 0;
    for (var i = 0; i < MDCATEGORYDETAILListOrder.length; i++) {
        for (var e = idxCT; e < CATEGORYDETAIL_TYPEUSERListOrder.length; e++)//0
        {
            idxCT = e;
            if (CATEGORYDETAIL_TYPEUSERListOrder[e].IDMDCATEGORYDETAIL == MDCATEGORYDETAILListOrder[i].IDMDCATEGORYDETAIL) {
                if (CATEGORYDETAIL_TYPEUSERListOrder[e].IDMDCATEGORYDETAIL == 33) {
                    SysCfg.Log.Methods.WriteLog("ss", null);
                }

                Persistence.Model.Methods.MDCATEGORYDETAIL_TYPEUSER_ListAdd(MDCATEGORYDETAILListOrder[i].MDCATEGORYDETAIL_TYPEUSERList, CATEGORYDETAIL_TYPEUSERListOrder[e]);
                CATEGORYDETAIL_TYPEUSERListOrder[e].MDCATEGORYDETAIL = MDCATEGORYDETAILListOrder[i];

                var SDTYPEUSER_CATEGORYDETAIL = Persistence.Model.Methods.SDTYPEUSER_CATEGORYDETAIL_ListSetID(ModelProfiler.SDTYPEUSER_CATEGORYDETAILList, CATEGORYDETAIL_TYPEUSERListOrder[e].IDSDTYPEUSER);
                Persistence.Model.Methods.SDTYPEUSER_CATEGORYDETAIL_ListAdd(ModelProfiler.SDTYPEUSER_CATEGORYDETAILList, SDTYPEUSER_CATEGORYDETAIL);


                SDTYPEUSER_CATEGORYDETAIL.SDTYPEUSER = Persistence.Catalog.Methods.SDTYPEUSER_ListSetID(Persistence.Profiler.CatalogProfiler.SDTYPEUSERList, CATEGORYDETAIL_TYPEUSERListOrder[e].IDSDTYPEUSER);
                CATEGORYDETAIL_TYPEUSERListOrder[e].SDTYPEUSER = SDTYPEUSER_CATEGORYDETAIL.SDTYPEUSER;

                //SDTYPEUSER_CATEGORYDETAIL = Persistence.Model.Methods.SDTYPEUSER_CATEGORYDETAIL_ListSetID(ModelProfiler.SDTYPEUSER_CATEGORYDETAILList, SDTYPEUSER_CATEGORYDETAIL.IDSDTYPEUSER);
                //new

                //**
                var MDCATEGORYDETAILCopy = Persistence.Model.Methods.MDCATEGORYDETAIL_COPY(MDCATEGORYDETAILListOrder[i]);
                Persistence.Model.Methods.MDCATEGORYDETAIL_ListAdd(SDTYPEUSER_CATEGORYDETAIL.MDCATEGORYDETAILList, MDCATEGORYDETAILCopy);
                MDCATEGORYDETAILCopy = Persistence.Model.Methods.MDCATEGORYDETAIL_ListSetID(SDTYPEUSER_CATEGORYDETAIL.MDCATEGORYDETAILList, MDCATEGORYDETAILCopy.IDMDCATEGORYDETAIL);
                //***
                var MDCATEGORYCopy = Persistence.Model.Methods.MDCATEGORY_COPY(MDCATEGORYDETAILListOrder[i].MDCATEGORY);
                Persistence.Model.Methods.MDCATEGORY_ListAdd(SDTYPEUSER_CATEGORYDETAIL.MDCATEGORYList, MDCATEGORYCopy);
                MDCATEGORYCopy = Persistence.Model.Methods.MDCATEGORY_ListSetID(SDTYPEUSER_CATEGORYDETAIL.MDCATEGORYList, MDCATEGORYCopy.IDMDCATEGORY);


                Persistence.Model.Methods.MDCATEGORYDETAIL_ListAdd(MDCATEGORYCopy.MDCATEGORYDETAILList, MDCATEGORYDETAILCopy);
                MDCATEGORYDETAILCopy.MDCATEGORY = MDCATEGORYCopy;
                //endnew
            }
            if (CATEGORYDETAIL_TYPEUSERListOrder[e].IDMDCATEGORYDETAIL > MDCATEGORYDETAILListOrder[i].IDMDCATEGORYDETAIL) {
                break;
            }
        }
    }

    for (var e = 0; e < ModelProfiler.SDTYPEUSER_CATEGORYDETAILList.length; e++)//0
    {
        Persistence.Model.Methods.FillCATEGORYNODE(ModelProfiler.SDTYPEUSER_CATEGORYDETAILList[e].CATEGORYNODEAllList, ModelProfiler.SDTYPEUSER_CATEGORYDETAILList[e].CATEGORYNODEBeginList, ModelProfiler.SDTYPEUSER_CATEGORYDETAILList[e].MDCATEGORYList);
        ItHelpCenter.Componet.SeekSearch.Methods.FillSeekSearchByCategory(ModelProfiler.SDTYPEUSER_CATEGORYDETAILList[e].MDCATEGORYDETAILList, ModelProfiler.SDTYPEUSER_CATEGORYDETAILList[e].SeekSearchProfiler);
    }

    idxC = 0;
    for (var i = 0; i < ModelProfiler.MDMODELTYPEDList.length; i++) {

        for (var e = idxC; e < ModelProfiler.MDMATRIXMODELSList.length; e++) {
            idxC = e;

            if (ModelProfiler.MDMATRIXMODELSList[e].IDMDMODELTYPED1 == ModelProfiler.MDMODELTYPEDList[i].IDMDMODELTYPED) {
                Persistence.Model.Methods.MDMATRIXMODELS_ListAdd(ModelProfiler.MDMODELTYPEDList[i].MDMATRIXMODELSList, ModelProfiler.MDMATRIXMODELSList[e]);
            }
            else {
                if (ModelProfiler.MDMATRIXMODELSList[e].IDMDMODELTYPED1 > ModelProfiler.MDMODELTYPEDList[i].IDMDMODELTYPED) {
                    break;
                }
            }

        }

    }

    if (ModelProfiler.MDCONFIGList[0].IDMDCATEGORYDETAIL_DEFAULT > -1) {
        ModelProfiler.MDCONFIGList[0].MDCATEGORYDETAIL_DEFAULT = Persistence.Model.Methods.MDCATEGORYDETAIL_ListSetID(ModelProfiler.MDCATEGORYDETAILList, ModelProfiler.MDCONFIGList[0].IDMDCATEGORYDETAIL_DEFAULT);
    }
    if (ModelProfiler.MDCONFIGList[0].IDMDURGENCY_DEFAULT > -1) {
        ModelProfiler.MDCONFIGList[0].MDURGENCY_DEFAULT = Persistence.Catalog.Methods.MDURGENCY_ListSetID(Persistence.Profiler.CatalogProfiler.MDURGENCYList, ModelProfiler.MDCONFIGList[0].IDMDURGENCY_DEFAULT);

    }

}

// SDTYPEUSER_CATEGORYDETAIL tabla virtua
//DataSet Function 
//List Function 
Persistence.Model.Methods.SDTYPEUSER_CATEGORYDETAIL_ListSetID = function (SDTYPEUSER_CATEGORYDETAILList, IDSDTYPEUSER) {
    for (var i = 0; i < SDTYPEUSER_CATEGORYDETAILList.length; i++) {

        if (IDSDTYPEUSER == SDTYPEUSER_CATEGORYDETAILList[i].IDSDTYPEUSER)
            return (SDTYPEUSER_CATEGORYDETAILList[i]);

    }
    var SDTYPEUSER_CATEGORYDETAIL = new Persistence.Model.Properties.TSDTYPEUSER_CATEGORYDETAIL();
    SDTYPEUSER_CATEGORYDETAIL.IDSDTYPEUSER = IDSDTYPEUSER
    return (SDTYPEUSER_CATEGORYDETAIL);
}
Persistence.Model.Methods.SDTYPEUSER_CATEGORYDETAIL_ListAdd = function (SDTYPEUSER_CATEGORYDETAILList, SDTYPEUSER_CATEGORYDETAIL) {
    var i = Persistence.Model.Methods.SDTYPEUSER_CATEGORYDETAIL_ListGetIndex(SDTYPEUSER_CATEGORYDETAILList, SDTYPEUSER_CATEGORYDETAIL);
    if (i == -1) SDTYPEUSER_CATEGORYDETAILList.push(SDTYPEUSER_CATEGORYDETAIL);
    else {
        SDTYPEUSER_CATEGORYDETAILList[i].IDSDTYPEUSER = SDTYPEUSER_CATEGORYDETAIL.IDSDTYPEUSER;
        SDTYPEUSER_CATEGORYDETAILList[i].MDCATEGORYDETAILList = SDTYPEUSER_CATEGORYDETAIL.MDCATEGORYDETAILList;
        SDTYPEUSER_CATEGORYDETAILList[i].MDCATEGORYList = SDTYPEUSER_CATEGORYDETAIL.MDCATEGORYList;
        SDTYPEUSER_CATEGORYDETAILList[i].SDTYPEUSER = SDTYPEUSER_CATEGORYDETAIL.SDTYPEUSER;
    }
}
//CJRC_26072018
Persistence.Model.Methods.SDTYPEUSER_CATEGORYDETAIL_ListGetIndex = function (SDTYPEUSER_CATEGORYDETAILList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.Model.Methods.SDTYPEUSER_CATEGORYDETAIL_ListGetIndexIDSDTYPEUSER(SDTYPEUSER_CATEGORYDETAILList, Param);

    }
    else {
        Res = Persistence.Model.Methods.SDTYPEUSER_CATEGORYDETAIL_ListGetIndexSDTYPEUSER_CATEGORYDETAIL(SDTYPEUSER_CATEGORYDETAILList, Param) ;
    }
    return (Res);
}

Persistence.Model.Methods.SDTYPEUSER_CATEGORYDETAIL_ListGetIndexSDTYPEUSER_CATEGORYDETAIL = function (SDTYPEUSER_CATEGORYDETAILList, SDTYPEUSER_CATEGORYDETAIL) {
    for (var i = 0; i < SDTYPEUSER_CATEGORYDETAILList.length; i++) {
        if (SDTYPEUSER_CATEGORYDETAIL.IDSDTYPEUSER == SDTYPEUSER_CATEGORYDETAILList[i].IDSDTYPEUSER)
            return (i);
    }
    return (-1);
}
Persistence.Model.Methods.SDTYPEUSER_CATEGORYDETAIL_ListGetIndexIDSDTYPEUSER = function (SDTYPEUSER_CATEGORYDETAILList, IDSDTYPEUSER) {
    for (var i = 0; i < SDTYPEUSER_CATEGORYDETAILList.length; i++) {
        if (IDSDTYPEUSER == SDTYPEUSER_CATEGORYDETAILList[i].IDSDTYPEUSER)
            return (i);
    }
    return (-1);
}

