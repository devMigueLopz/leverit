﻿//TreduceDB
//Persistence.Model.Properties
//Persistence.Model.Methods

 
Persistence.Model.Properties.TMDINTERFACETYPE = function()
{
    this.IDMDINTERFACETYPE = 0;
    this.INTERFACETYPE_NAME = "";
    //Socket IO Properties
    this.ToByte = function (MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDINTERFACETYPE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.INTERFACETYPE_NAME);
    }
    this.ByteTo = function (MemStream)
    {
        this.IDMDINTERFACETYPE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.INTERFACETYPE_NAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto)
    {
        SysCfg.Str.Protocol.WriteInt(this.IDMDINTERFACETYPE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.INTERFACETYPE_NAME, Longitud, Texto);
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto)
    {
        this.IDMDINTERFACETYPE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
    this.INTERFACETYPE_NAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
    }
}    

//**********************   METHODS server  ********************************************************************************************

       
//DataSet Function 
Persistence.Model.Methods.MDINTERFACETYPE_Fill= function(MDINTERFACETYPEList, StrIDMDINTERFACETYPE/* IDMDINTERFACETYPE*/)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    MDINTERFACETYPEList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDMDINTERFACETYPE == "")
    {
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDINTERFACETYPE.IDMDINTERFACETYPE.FieldName, " = " + UsrCfg.InternoAtisNames.MDINTERFACETYPE.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDINTERFACETYPE.IDMDINTERFACETYPE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else
    {
        StrIDMDINTERFACETYPE = " IN (" + StrIDMDINTERFACETYPE + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDINTERFACETYPE.IDMDINTERFACETYPE.FieldName, StrIDMDINTERFACETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //Param.AddInt32(UsrCfg.InternoDataLinkNames.MDINTERFACETYPE.IDMDINTERFACETYPE.FieldName, IDMDINTERFACETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try
    {
        /*

    //********   MDINTERFACETYPE_GET   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDINTERFACETYPE_GET", "MDINTERFACETYPE_GET description.");
    UnSQL.SQL.Add(" SELECT  ");
    UnSQL.SQL.Add("  MDINTERFACETYPE.IDMDINTERFACETYPE, ");
    UnSQL.SQL.Add("  MDINTERFACETYPE.INTERFACETYPE_NAME ");
    UnSQL.SQL.Add(" FROM  MDINTERFACETYPE ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  MDINTERFACETYPE.IDMDINTERFACETYPE=[IDMDINTERFACETYPE] and  ");
    UnSQL.SQL.Add("  MDINTERFACETYPE.INTERFACETYPE_NAME=[INTERFACETYPE_NAME] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_MDINTERFACETYPE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDINTERFACETYPE_GET", Param.ToBytes());
        ResErr = DS_MDINTERFACETYPE.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDINTERFACETYPE.DataSet.RecordCount > 0)
            {
                DS_MDINTERFACETYPE.DataSet.First();
                while (!(DS_MDINTERFACETYPE.DataSet.Eof))
                {
                    var MDINTERFACETYPE = new Persistence.Model.Properties.TMDINTERFACETYPE();
                    MDINTERFACETYPE.IDMDINTERFACETYPE = DS_MDINTERFACETYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDINTERFACETYPE.IDMDINTERFACETYPE.FieldName).asInt32();
                    //Other
                    MDINTERFACETYPE.INTERFACETYPE_NAME = DS_MDINTERFACETYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDINTERFACETYPE.INTERFACETYPE_NAME.FieldName).asString();
                    MDINTERFACETYPEList.push(MDINTERFACETYPE);

                    DS_MDINTERFACETYPE.DataSet.Next();
                }
            }
            else
            {
                DS_MDINTERFACETYPE.ResErr.NotError = false;
                DS_MDINTERFACETYPE.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDINTERFACETYPE_GETID=function(MDINTERFACETYPE)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDINTERFACETYPE.IDMDINTERFACETYPE.FieldName, MDINTERFACETYPE.IDMDINTERFACETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.MDINTERFACETYPE.INTERFACETYPE_NAME.FieldName, MDINTERFACETYPE.INTERFACETYPE_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDINTERFACETYPE_GETID   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDINTERFACETYPE_GETID", "MDINTERFACETYPE_GETID description.");
    UnSQL.SQL.Add(" SELECT  ");
    UnSQL.SQL.Add("  MDINTERFACETYPE.IDMDINTERFACETYPE, ");
    UnSQL.SQL.Add("  MDINTERFACETYPE.INTERFACETYPE_NAME ");
    UnSQL.SQL.Add(" FROM  MDINTERFACETYPE ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  MDINTERFACETYPE.IDMDINTERFACETYPE=[IDMDINTERFACETYPE] and  ");
    UnSQL.SQL.Add("  MDINTERFACETYPE.INTERFACETYPE_NAME=[INTERFACETYPE_NAME] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_MDINTERFACETYPE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDINTERFACETYPE_GETID", Param.ToBytes());
        ResErr = DS_MDINTERFACETYPE.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDINTERFACETYPE.DataSet.RecordCount > 0)
            {
                DS_MDINTERFACETYPE.DataSet.First();
                MDINTERFACETYPE.IDMDINTERFACETYPE = DS_MDINTERFACETYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDINTERFACETYPE.IDMDINTERFACETYPE.FieldName).asInt32();
                //Other
                MDINTERFACETYPE.INTERFACETYPE_NAME = DS_MDINTERFACETYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDINTERFACETYPE.INTERFACETYPE_NAME.FieldName).asString();
            }
            else
            {
                DS_MDINTERFACETYPE.ResErr.NotError = false;
                DS_MDINTERFACETYPE.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDINTERFACETYPE_ADD=function(MDINTERFACETYPE)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDINTERFACETYPE.IDMDINTERFACETYPE.FieldName, MDINTERFACETYPE.IDMDINTERFACETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.MDINTERFACETYPE.INTERFACETYPE_NAME.FieldName, MDINTERFACETYPE.INTERFACETYPE_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDINTERFACETYPE_ADD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDINTERFACETYPE_ADD", "MDINTERFACETYPE_ADD description.");
    UnSQL.SQL.Add(" INSERT INTO MDINTERFACETYPE( ");
    UnSQL.SQL.Add("  IDMDINTERFACETYPE,  ");
    UnSQL.SQL.Add("  INTERFACETYPE_NAME ");
    UnSQL.SQL.Add(" )  ");
    UnSQL.SQL.Add(" VALUES ( ");
    UnSQL.SQL.Add("  [IDMDINTERFACETYPE], ");
    UnSQL.SQL.Add("  [INTERFACETYPE_NAME] ");
    UnSQL.SQL.Add(" ) ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDINTERFACETYPE_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            MDINTERFACETYPE_GETID(MDINTERFACETYPE);
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDINTERFACETYPE_UPD=function(MDINTERFACETYPE)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDINTERFACETYPE.IDMDINTERFACETYPE.FieldName, MDINTERFACETYPE.IDMDINTERFACETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.MDINTERFACETYPE.INTERFACETYPE_NAME.FieldName, MDINTERFACETYPE.INTERFACETYPE_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDINTERFACETYPE_UPD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDINTERFACETYPE_UPD", "MDINTERFACETYPE_UPD description.");
    UnSQL.SQL.Add(" UPDATE MDINTERFACETYPE SET ");
    UnSQL.SQL.Add("  IDMDINTERFACETYPE=[IDMDINTERFACETYPE],  ");
    UnSQL.SQL.Add("  INTERFACETYPE_NAME=[INTERFACETYPE_NAME] ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  IDMDINTERFACETYPE=[IDMDINTERFACETYPE] AND  ");
    UnSQL.SQL.Add("  INTERFACETYPE_NAME=[INTERFACETYPE_NAME] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDINTERFACETYPE_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            //GET(); 
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDINTERFACETYPE_DEL=function(/* StrIDMDINTERFACETYPE*/ IDMDINTERFACETYPE)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        /*if (StrIDMDINTERFACETYPE == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDINTERFACETYPE.IDMDINTERFACETYPE.FieldName, " = " + UsrCfg.InternoAtisNames.MDINTERFACETYPE.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDINTERFACETYPE.IDMDINTERFACETYPE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDMDINTERFACETYPE = " IN (" + StrIDMDINTERFACETYPE + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDINTERFACETYPE.IDMDINTERFACETYPE.FieldName, StrIDMDINTERFACETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.MDINTERFACETYPE.IDMDINTERFACETYPE.FieldName, IDMDINTERFACETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDINTERFACETYPE_DEL_1   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDINTERFACETYPE_DEL_1", "MDINTERFACETYPE_DEL_1 description.");
    UnSQL.SQL.Add(" DELETE MDINTERFACETYPE  ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  IDMDINTERFACETYPE=[IDMDINTERFACETYPE] AND  ");
    UnSQL.SQL.Add("  INTERFACETYPE_NAME=[INTERFACETYPE_NAME] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDINTERFACETYPE_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDINTERFACETYPE_DEL=function(MDINTERFACETYPE)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDINTERFACETYPE.IDMDINTERFACETYPE.FieldName, MDINTERFACETYPE.IDMDINTERFACETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.MDINTERFACETYPE.INTERFACETYPE_NAME.FieldName, MDINTERFACETYPE.INTERFACETYPE_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDINTERFACETYPE_DEL_2   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDINTERFACETYPE_DEL_2", "MDINTERFACETYPE_DEL_2 description.");
    UnSQL.SQL.Add(" DELETE MDINTERFACETYPE  ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  IDMDINTERFACETYPE=[IDMDINTERFACETYPE] AND  ");
    UnSQL.SQL.Add("  INTERFACETYPE_NAME=[INTERFACETYPE_NAME] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDINTERFACETYPE_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
         
//**********************   METHODS   ********************************************************************************************

        
//DataSet Function 

//List Function 
Persistence.Model.Methods.MDINTERFACETYPE_ListSetID=function(MDINTERFACETYPEList, IDMDINTERFACETYPE)
{
    for (i = 0; i < MDINTERFACETYPEList.length; i++)
    {

        if (IDMDINTERFACETYPE == MDINTERFACETYPEList[i].IDMDINTERFACETYPE)
            return (MDINTERFACETYPEList[i]);

    }
     
    var TMDINTERFACETYPE = new Persistence.Properties.TMDINTERFACETYPE();
    TMDINTERFACETYPE.IDMDINTERFACETYPE = IDMDINTERFACETYPE; 
    return (TMDINTERFACETYPE); 

}
Persistence.Model.Methods.MDINTERFACETYPE_ListAdd=function(MDINTERFACETYPEList, MDINTERFACETYPE)
{
    var i = Persistence.Model.Methods.MDINTERFACETYPE_ListGetIndex(MDINTERFACETYPEList, MDINTERFACETYPE);
    if (i == -1) MDINTERFACETYPEList.push(MDINTERFACETYPE);
    else
    {
        MDINTERFACETYPEList[i].IDMDINTERFACETYPE = MDINTERFACETYPE.IDMDINTERFACETYPE;
        MDINTERFACETYPEList[i].INTERFACETYPE_NAME = MDINTERFACETYPE.INTERFACETYPE_NAME;
    }
}
Persistence.Model.Methods.MDINTERFACETYPE_ListGetIndex=function(MDINTERFACETYPEList, MDINTERFACETYPE)
{
    for (i = 0; i < MDINTERFACETYPEList.length; i++)
    {
        if (MDINTERFACETYPE.IDMDINTERFACETYPE == MDINTERFACETYPEList[i].IDMDINTERFACETYPE)
            return (i);
    }
    return (-1);
}
Persistence.Model.Methods.MDINTERFACETYPE_ListGetIndex=function(MDINTERFACETYPEList, IDMDINTERFACETYPE)
{
    for (i = 0; i < MDINTERFACETYPEList.length; i++)
    {
        if (IDMDINTERFACETYPE == MDINTERFACETYPEList[i].IDMDINTERFACETYPE)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.Model.Methods.MDINTERFACETYPEListtoStr=function(MDINTERFACETYPEList)
{
    var Res = Persistence.Model.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(MDINTERFACETYPEList.length, Longitud, Texto);
        for (Counter = 0; Counter < MDINTERFACETYPEList.length; Counter++) {
            var UnMDINTERFACETYPE = MDINTERFACETYPEList[Counter];
            UnMDINTERFACETYPE.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Model.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.Model.Methods.StrtoMDINTERFACETYPE=function(ProtocoloStr, MDINTERFACETYPEList)
{
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        MDINTERFACETYPEList.length = 0;
        if (Persistence.Model.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnMDINTERFACETYPE = new Persistence.Demo.Properties.TMDINTERFACETYPE(); //Mode new row
                UnMDINTERFACETYPE.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.MDINTERFACETYPE_ListAdd(MDINTERFACETYPEList, UnMDINTERFACETYPE);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
    
}

//Socket Function 
Persistence.Model.Methods.MDINTERFACETYPEListToByte=function(MDINTERFACETYPEList, MemStream)
{
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, MDINTERFACETYPEList.length);
    for (i = 0; i < MDINTERFACETYPEList.length; i++)
    {
        MDINTERFACETYPEList[i].ToByte(MemStream);
    }
}
Persistence.Model.Methods.ByteToMDINTERFACETYPEList=function(MDINTERFACETYPEList, MemStream)
{
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++)
    {
        var UnMDINTERFACETYPE = new Persistence.Model.Properties.TMDINTERFACETYPE();
        MDINTERFACETYPEList[i].ToByte(MemStream);
    }
}
 