﻿//TreduceDB
//Persistence.Model.Properties
//Persistence.Model.Methods

 
Persistence.Model.Properties.TMDPRIORITYMATRIX = function()
{
    this.IDMDIMPACT = 0;
    this.IDMDPRIORITY  = 0;
    this.IDMDURGENCY  = 0;
    //Socket IO Properties
    this.ToByte = function(MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream,  this.IDMDIMPACT);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream,  this.IDMDPRIORITY);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream,  this.IDMDURGENCY);
    }
    this.ByteTo = function(MemStream)
    {
        this.IDMDIMPACT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDPRIORITY = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDURGENCY = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    }
    //Str IO Properties
    this.ToStr = function(Longitud, Texto)
    {
        SysCfg.Str.Protocol.WriteInt( this.IDMDIMPACT, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt( this.IDMDPRIORITY, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt( this.IDMDURGENCY, Longitud, Texto);
    }
    this.StrTo = function( Pos,  Index, LongitudArray, Texto)
    {
        this.IDMDIMPACT = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDPRIORITY = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDURGENCY = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
    }
}
     

//**********************   METHODS server  ********************************************************************************************

  
//DataSet Function 
Persistence.Model.Methods.MDPRIORITYMATRIX_Fill=function(MDPRIORITYMATRIXList, StrIDMDPRIORITYMATRIX/* IDMDPRIORITYMATRIX*/)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    MDPRIORITYMATRIXList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDMDPRIORITYMATRIX == "")
    {
        //Param.AddUnknown(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDPRIORITYMATRIX.FieldName, " = " + UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDPRIORITYMATRIX.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else
    {
        StrIDMDPRIORITYMATRIX = " IN (" + StrIDMDPRIORITYMATRIX + ")";
        //Param.AddUnknown(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDPRIORITYMATRIX.FieldName, StrIDMDPRIORITYMATRIX, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //Param.AddInt32(UsrCfg.InternoDataLinkNames.MDPRIORITYMATRIX.IDMDPRIORITYMATRIX.FieldName, IDMDPRIORITYMATRIX, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try
    {
        /*

    //********   MDPRIORITYMATRIX_GET   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDPRIORITYMATRIX_GET", "MDPRIORITYMATRIX_GET description.");
    UnSQL.SQL.Add(" SELECT  ");
    UnSQL.SQL.Add("  MDPRIORITYMATRIX.IDMDIMPACT, ");
    UnSQL.SQL.Add("  MDPRIORITYMATRIX.IDMDPRIORITY, ");
    UnSQL.SQL.Add("  MDPRIORITYMATRIX.IDMDURGENCY ");
    UnSQL.SQL.Add(" FROM  MDPRIORITYMATRIX ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  MDPRIORITYMATRIX.IDMDIMPACT=[IDMDIMPACT] and  ");
    UnSQL.SQL.Add("  MDPRIORITYMATRIX.IDMDPRIORITY=[IDMDPRIORITY] and  ");
    UnSQL.SQL.Add("  MDPRIORITYMATRIX.IDMDURGENCY=[IDMDURGENCY] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_MDPRIORITYMATRIX = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDPRIORITYMATRIX_GET", Param.ToBytes());
        ResErr = DS_MDPRIORITYMATRIX.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDPRIORITYMATRIX.DataSet.RecordCount > 0)
            {
                DS_MDPRIORITYMATRIX.DataSet.First();
                while (!(DS_MDPRIORITYMATRIX.DataSet.Eof))
                {
                    var MDPRIORITYMATRIX = new Persistence.Model.Properties.TMDPRIORITYMATRIX();
                    //Other
                    MDPRIORITYMATRIX.IDMDIMPACT = DS_MDPRIORITYMATRIX.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDIMPACT.FieldName).asInt32();
                    MDPRIORITYMATRIX.IDMDPRIORITY = DS_MDPRIORITYMATRIX.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDPRIORITY.FieldName).asInt32();
                    MDPRIORITYMATRIX.IDMDURGENCY = DS_MDPRIORITYMATRIX.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDURGENCY.FieldName).asInt32();
                    MDPRIORITYMATRIXList.push(MDPRIORITYMATRIX);

                    DS_MDPRIORITYMATRIX.DataSet.Next();
                }
            }
            else
            {
                DS_MDPRIORITYMATRIX.ResErr.NotError = false;
                DS_MDPRIORITYMATRIX.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDPRIORITYMATRIX_GETID=function(MDPRIORITYMATRIX)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDIMPACT.FieldName, MDPRIORITYMATRIX.IDMDIMPACT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDPRIORITY.FieldName, MDPRIORITYMATRIX.IDMDPRIORITY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDURGENCY.FieldName, MDPRIORITYMATRIX.IDMDURGENCY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDPRIORITYMATRIX_GETID   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDPRIORITYMATRIX_GETID", "MDPRIORITYMATRIX_GETID description.");
    UnSQL.SQL.Add(" SELECT  ");
    UnSQL.SQL.Add("  MDPRIORITYMATRIX.IDMDIMPACT, ");
    UnSQL.SQL.Add("  MDPRIORITYMATRIX.IDMDPRIORITY, ");
    UnSQL.SQL.Add("  MDPRIORITYMATRIX.IDMDURGENCY ");
    UnSQL.SQL.Add(" FROM  MDPRIORITYMATRIX ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  MDPRIORITYMATRIX.IDMDIMPACT=[IDMDIMPACT] and  ");
    UnSQL.SQL.Add("  MDPRIORITYMATRIX.IDMDPRIORITY=[IDMDPRIORITY] and  ");
    UnSQL.SQL.Add("  MDPRIORITYMATRIX.IDMDURGENCY=[IDMDURGENCY] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_MDPRIORITYMATRIX = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDPRIORITYMATRIX_GETID", Param.ToBytes());
        ResErr = DS_MDPRIORITYMATRIX.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDPRIORITYMATRIX.DataSet.RecordCount > 0)
            {
                DS_MDPRIORITYMATRIX.DataSet.First();
                //Other
                MDPRIORITYMATRIX.IDMDIMPACT = DS_MDPRIORITYMATRIX.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDIMPACT.FieldName).asInt32();
                MDPRIORITYMATRIX.IDMDPRIORITY = DS_MDPRIORITYMATRIX.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDPRIORITY.FieldName).asInt32();
                MDPRIORITYMATRIX.IDMDURGENCY = DS_MDPRIORITYMATRIX.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDURGENCY.FieldName).asInt32();
            }
            else
            {
                DS_MDPRIORITYMATRIX.ResErr.NotError = false;
                DS_MDPRIORITYMATRIX.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDPRIORITYMATRIX_ADD=function( MDPRIORITYMATRIX)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDIMPACT.FieldName, MDPRIORITYMATRIX.IDMDIMPACT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDPRIORITY.FieldName, MDPRIORITYMATRIX.IDMDPRIORITY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDURGENCY.FieldName, MDPRIORITYMATRIX.IDMDURGENCY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDPRIORITYMATRIX_ADD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDPRIORITYMATRIX_ADD", "MDPRIORITYMATRIX_ADD description.");
    UnSQL.SQL.Add(" INSERT INTO MDPRIORITYMATRIX( ");
    UnSQL.SQL.Add("  IDMDIMPACT,  ");
    UnSQL.SQL.Add("  IDMDPRIORITY,  ");
    UnSQL.SQL.Add("  IDMDURGENCY ");
    UnSQL.SQL.Add(" )  ");
    UnSQL.SQL.Add(" VALUES ( ");
    UnSQL.SQL.Add("  [IDMDIMPACT], ");
    UnSQL.SQL.Add("  [IDMDPRIORITY], ");
    UnSQL.SQL.Add("  [IDMDURGENCY] ");
    UnSQL.SQL.Add(" ) ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDPRIORITYMATRIX_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            MDPRIORITYMATRIX_GETID(MDPRIORITYMATRIX);
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDPRIORITYMATRIX_UPD =function( MDPRIORITYMATRIX)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDIMPACT.FieldName, MDPRIORITYMATRIX.IDMDIMPACT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDPRIORITY.FieldName, MDPRIORITYMATRIX.IDMDPRIORITY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDURGENCY.FieldName, MDPRIORITYMATRIX.IDMDURGENCY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDPRIORITYMATRIX_UPD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDPRIORITYMATRIX_UPD", "MDPRIORITYMATRIX_UPD description.");
    UnSQL.SQL.Add(" UPDATE MDPRIORITYMATRIX SET ");
    UnSQL.SQL.Add("  IDMDIMPACT=[IDMDIMPACT],  ");
    UnSQL.SQL.Add("  IDMDPRIORITY=[IDMDPRIORITY],  ");
    UnSQL.SQL.Add("  IDMDURGENCY=[IDMDURGENCY] ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  IDMDIMPACT=[IDMDIMPACT] AND  ");
    UnSQL.SQL.Add("  IDMDPRIORITY=[IDMDPRIORITY] AND  ");
    UnSQL.SQL.Add("  IDMDURGENCY=[IDMDURGENCY] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDPRIORITYMATRIX_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            //GET(); 
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDPRIORITYMATRIX_DEL=function(MDPRIORITYMATRIX)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDIMPACT.FieldName, MDPRIORITYMATRIX.IDMDIMPACT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDPRIORITY.FieldName, MDPRIORITYMATRIX.IDMDPRIORITY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDURGENCY.FieldName, MDPRIORITYMATRIX.IDMDURGENCY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDPRIORITYMATRIX_DEL_2   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDPRIORITYMATRIX_DEL_2", "MDPRIORITYMATRIX_DEL_2 description.");
    UnSQL.SQL.Add(" DELETE MDPRIORITYMATRIX  ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  IDMDIMPACT=[IDMDIMPACT] AND  ");
    UnSQL.SQL.Add("  IDMDPRIORITY=[IDMDPRIORITY] AND  ");
    UnSQL.SQL.Add("  IDMDURGENCY=[IDMDURGENCY] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDPRIORITYMATRIX_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
     

//**********************   METHODS   ********************************************************************************************

   
//DataSet Function 

//List Function 
Persistence.Model.Methods.MDPRIORITYMATRIX_ListSetID=function(MDPRIORITYMATRIXList,  IDMDPRIORITYMATRIX)
{
    //for (i = 0; i < MDPRIORITYMATRIXList.length; i++)
    //{

    //    if (IDMDPRIORITYMATRIX == MDPRIORITYMATRIXList[i].IDMDPRIORITYMATRIX)
    //        return (MDPRIORITYMATRIXList[i]);

    //}
    
    //var TMDPRIORITYMATRIX = new Persistence.Properties.TMDPRIORITYMATRIX();
    //TMDPRIORITYMATRIX.IDMDPRIORITYMATRIX = IDMDPRIORITYMATRIX; 
    //return (TMDPRIORITYMATRIX); 
    return null;
}
Persistence.Model.Methods.MDPRIORITYMATRIX_ListAdd=function(MDPRIORITYMATRIXList, MDPRIORITYMATRIX)
{
    var i = Persistence.Model.Methods.MDPRIORITYMATRIX_ListGetIndex(MDPRIORITYMATRIXList, MDPRIORITYMATRIX);
    if (i == -1) MDPRIORITYMATRIXList.push(MDPRIORITYMATRIX);
    else
    {
        MDPRIORITYMATRIXList[i].IDMDIMPACT = MDPRIORITYMATRIX.IDMDIMPACT;
        MDPRIORITYMATRIXList[i].IDMDPRIORITY = MDPRIORITYMATRIX.IDMDPRIORITY;
        MDPRIORITYMATRIXList[i].IDMDURGENCY = MDPRIORITYMATRIX.IDMDURGENCY;
    }
}
Persistence.Model.Methods.MDPRIORITYMATRIX_ListGetIndex=function(MDPRIORITYMATRIXList, MDPRIORITYMATRIX)
{
    //for (i = 0; i < MDPRIORITYMATRIXList.length; i++)
    //{
    //    if (MDPRIORITYMATRIX.IDMDPRIORITYMATRIX == MDPRIORITYMATRIXList[i].IDMDPRIORITYMATRIX)
    //        return (i);
    //}
    return (-1);
}
Persistence.Model.Methods.MDPRIORITYMATRIX_ListGetIndex=function(MDPRIORITYMATRIXList, IDMDPRIORITYMATRIX)
{
    //for (i = 0; i < MDPRIORITYMATRIXList.length; i++)
    //{
    //    if (IDMDPRIORITYMATRIX == MDPRIORITYMATRIXList[i].IDMDPRIORITYMATRIX)
    //        return (i);
    //}
    return (-1);
}

//String Function 
Persistence.Model.Methods.MDPRIORITYMATRIXListtoStr=function(MDPRIORITYMATRIXList)
{
    var Res = Persistence.Model.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(MDPRIORITYMATRIXList.length, Longitud, Texto);
        for (Counter = 0; Counter < MDPRIORITYMATRIXList.length; Counter++) {
            var UnMDPRIORITYMATRIX = MDPRIORITYMATRIXList[Counter];
            UnMDPRIORITYMATRIX.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Model.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.Model.Methods.StrtoMDPRIORITYMATRIX=function(ProtocoloStr, MDPRIORITYMATRIXList)
{
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        MDPRIORITYMATRIXList.length = 0;
        if (Persistence.Model.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnMDPRIORITYMATRIX = new Persistence.Demo.Properties.TMDPRIORITYMATRIX(); //Mode new row
                UnMDPRIORITYMATRIX.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.MDPRIORITYMATRIX_ListAdd(MDPRIORITYMATRIXList, UnMDPRIORITYMATRIX);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
    
}

//Socket Function 
Persistence.Model.Methods.MDPRIORITYMATRIXListToByte=function(MDPRIORITYMATRIXList, MemStream)
{
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, MDPRIORITYMATRIXList.length);
    for (i = 0; i < MDPRIORITYMATRIXList.length; i++)
    {
        MDPRIORITYMATRIXList[i].ToByte(MemStream);
    }
}
Persistence.Model.Methods.ByteToMDPRIORITYMATRIXList=function(MDPRIORITYMATRIXList, MemStream)
{
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++)
    {
        var UnMDPRIORITYMATRIX = new Persistence.Model.Properties.TMDPRIORITYMATRIX();
        MDPRIORITYMATRIXList[i].ToByte(MemStream);
    }
}
 