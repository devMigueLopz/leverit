﻿//TreduceDB
//Persistence.Model.Properties
//Persistence.Model.Methods


Persistence.Model.Properties.TMDSLA = function()
{
    this.IDCALEDAYDATE = 0;
    this.IDMDIMPACT  = 0;
    this.IDMDSLA  = 0;
    this.SLACONDITION = "";
    this.SLADESCRIPTION  = "";
    this.SLANAME  = "";
    this.SLAORDER  = 0;
    this.SLAPARENT  = 0;
    this.SLASTATUS  = 0;
    this.SLA_MAXTIME = 0;
    this.SLA_NORMALTIME = 0;
    this.IDSDSCALETYPE_FUNC = UsrCfg.SD.Properties.TIDSDSCALETYPE_FUNC.GetEnum(undefined);//te posiciona el primer enumerado
    this.IDSDSCALETYPE_HIER = UsrCfg.SD.Properties.TIDSDSCALETYPE_HIER.GetEnum(undefined);//te posiciona el primer enumerado
    //Socket IO Properties
    this.ToByte = function(MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDCALEDAYDATE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDIMPACT);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDSLA);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.SLACONDITION);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.SLADESCRIPTION);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.SLANAME);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.SLAORDER);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.SLAPARENT);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.SLASTATUS);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.SLA_MAXTIME);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.SLA_NORMALTIME);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDSCALETYPE_FUNC.value);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDSCALETYPE_HIER.value);

    }
    this.ByteTo = function(MemStream)
    {
        this.IDCALEDAYDATE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDIMPACT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDSLA = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.SLACONDITION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.SLADESCRIPTION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.SLANAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.SLAORDER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.SLAPARENT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.SLASTATUS = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.SLA_MAXTIME = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.SLA_NORMALTIME = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDSCALETYPE_FUNC = UsrCfg.SD.Properties.TIDSDSCALETYPE_FUNC.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        this.IDSDSCALETYPE_HIER = UsrCfg.SD.Properties.TIDSDSCALETYPE_HIER.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
    }
    //Str IO Properties
    this.ToStr = function(Longitud, Texto)
    {
        SysCfg.Str.Protocol.WriteInt(this.IDCALEDAYDATE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDIMPACT, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDSLA, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.SLACONDITION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.SLADESCRIPTION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.SLANAME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.SLAORDER, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.SLAPARENT, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.SLASTATUS, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.SLA_MAXTIME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.SLA_NORMALTIME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDSDSCALETYPE_FUNC.value, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDSDSCALETYPE_HIER.value, Longitud, Texto);
    }
    this.StrTo = function(Pos, Index, LongitudArray, Texto)
    {
        this.IDCALEDAYDATE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDIMPACT = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDSLA = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.SLACONDITION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.SLADESCRIPTION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.SLANAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.SLAORDER = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.SLAPARENT = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.SLASTATUS = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.SLA_MAXTIME = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.SLA_NORMALTIME = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDSDSCALETYPE_FUNC = UsrCfg.SD.Properties.TIDSDSCALETYPE_FUNC.GetEnum(SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto));
        this.IDSDSCALETYPE_HIER = UsrCfg.SD.Properties.TIDSDSCALETYPE_HIER.GetEnum(SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto));
    }
}
    

//**********************   METHODS server  ********************************************************************************************

    
//DataSet Function 
Persistence.Model.Methods.MDSLA_Fill=function(MDSLAList, StrIDMDSLA/* IDMDSLA*/)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    MDSLAList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDMDSLA == "")
    {
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDSLA.IDMDSLA.FieldName, " = " + UsrCfg.InternoAtisNames.MDSLA.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDSLA.IDMDSLA.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else
    {
        StrIDMDSLA = " IN (" + StrIDMDSLA + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDSLA.IDMDSLA.FieldName, StrIDMDSLA, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //Param.AddInt32(UsrCfg.InternoDataLinkNames.MDSLA.IDMDSLA.FieldName, IDMDSLA, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try
    {
        /*

    //********   MDSLA_GET   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDSLA_GET", "MDSLA_GET description.");
    UnSQL.SQL.Add(" SELECT  ");
    UnSQL.SQL.Add("  MDSLA.IDCALEDAYDATE, ");
    UnSQL.SQL.Add("  MDSLA.IDMDIMPACT, ");
    UnSQL.SQL.Add("  MDSLA.IDMDSLA, ");
    UnSQL.SQL.Add("  MDSLA.SLACONDITION, ");
    UnSQL.SQL.Add("  MDSLA.SLADESCRIPTION, ");
    UnSQL.SQL.Add("  MDSLA.SLANAME, ");
    UnSQL.SQL.Add("  MDSLA.SLAORDER, ");
    UnSQL.SQL.Add("  MDSLA.SLAPARENT, ");
    UnSQL.SQL.Add("  MDSLA.SLASTATUS, ");
    UnSQL.SQL.Add("  MDSLA.SLA_MAXTIME, ");
    UnSQL.SQL.Add("  MDSLA.SLA_NORMALTIME, ");
    UnSQL.SQL.Add("  MDSLA.IDSDSCALETYPE_FUNC, ");
    UnSQL.SQL.Add("  MDSLA.IDSDSCALETYPE_HIER ");
    UnSQL.SQL.Add(" FROM  MDSLA ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  MDSLA.IDCALEDAYDATE=[IDCALEDAYDATE] and  ");
    UnSQL.SQL.Add("  MDSLA.IDMDIMPACT=[IDMDIMPACT] and  ");
    UnSQL.SQL.Add("  MDSLA.IDMDSLA=[IDMDSLA] and  ");
    UnSQL.SQL.Add("  MDSLA.SLACONDITION=[SLACONDITION] and  ");
    UnSQL.SQL.Add("  MDSLA.SLADESCRIPTION=[SLADESCRIPTION] and  ");
    UnSQL.SQL.Add("  MDSLA.SLANAME=[SLANAME] and  ");
    UnSQL.SQL.Add("  MDSLA.SLAORDER=[SLAORDER] and  ");
    UnSQL.SQL.Add("  MDSLA.SLAPARENT=[SLAPARENT] and  ");
    UnSQL.SQL.Add("  MDSLA.SLASTATUS=[SLASTATUS] and  ");
    UnSQL.SQL.Add("  MDSLA.SLA_MAXTIME=[SLA_MAXTIME] and ");
    UnSQL.SQL.Add("  MDSLA.SLA_NORMALTIME=[SLA_NORMALTIME] and ");
    UnSQL.SQL.Add("  MDSLA.IDSDSCALETYPE_FUNC=[IDSDSCALETYPE_FUNC] and ");
    UnSQL.SQL.Add("  MDSLA.IDSDSCALETYPE_HIER=[IDSDSCALETYPE_HIER] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_MDSLA = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDSLA_GET", Param.ToBytes());
        ResErr = DS_MDSLA.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDSLA.DataSet.RecordCount > 0)
            {
                DS_MDSLA.DataSet.First();
                while (!(DS_MDSLA.DataSet.Eof))
                {
                    var MDSLA = new Persistence.Model.Properties.TMDSLA();
                    MDSLA.IDMDSLA = DS_MDSLA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSLA.IDMDSLA.FieldName).asInt32();
                    //Other
                    MDSLA.IDCALEDAYDATE = DS_MDSLA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSLA.IDCALEDAYDATE.FieldName).asInt32();
                    MDSLA.IDMDIMPACT = DS_MDSLA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSLA.IDMDIMPACT.FieldName).asInt32();
                    MDSLA.SLANAME = DS_MDSLA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSLA.SLANAME.FieldName).asString();
                    MDSLA.SLAORDER = DS_MDSLA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSLA.SLAORDER.FieldName).asInt32();
                    MDSLA.SLAPARENT = DS_MDSLA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSLA.SLAPARENT.FieldName).asInt32();
                    MDSLA.SLASTATUS = DS_MDSLA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSLA.SLASTATUS.FieldName).asInt32();
                    MDSLA.SLA_MAXTIME = DS_MDSLA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSLA.SLA_MAXTIME.FieldName).asInt32();
                    MDSLA.SLA_NORMALTIME = DS_MDSLA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSLA._NORMALTIME.FieldName).asInt32();
                    MDSLA.IDSDSCALETYPE_FUNC = UsrCfg.SD.Properties.TIDSDSCALETYPE_FUNC.GetEnum(DS_MDSLA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSLA.IDSDSCALETYPE_FUNC.FieldName).asInt32());
                    MDSLA.IDSDSCALETYPE_HIER = UsrCfg.SD.Properties.TIDSDSCALETYPE_HIER.GetEnum(DS_MDSLA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSLA.IDSDSCALETYPE_HIER.FieldName).asInt32());
                    MDSLAList.push(MDSLA);

                    DS_MDSLA.DataSet.Next();
                }
            }
            else
            {
                DS_MDSLA.ResErr.NotError = false;
                DS_MDSLA.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDSLA_GETID = function(MDSLA)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSLA.IDMDSLA.FieldName, MDSLA.IDMDSLA, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSLA.IDCALEDAYDATE.FieldName, MDSLA.IDCALEDAYDATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSLA.IDMDIMPACT.FieldName, MDSLA.IDMDIMPACT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.MDSLA.SLACONDITION.FieldName, MDSLA.SLACONDITION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.MDSLA.SLADESCRIPTION.FieldName, MDSLA.SLADESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSLA.SLANAME.FieldName, MDSLA.SLANAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSLA.SLAORDER.FieldName, MDSLA.SLAORDER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSLA.SLAPARENT.FieldName, MDSLA.SLAPARENT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSLA.SLASTATUS.FieldName, MDSLA.SLASTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSLA.SLA_MAXTIME.FieldName, MDSLA.SLA_MAXTIME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSLA.SLA_NORMALTIME.FieldName, MDSLA.SLA_NORMALTIME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSLA.IDSDSCALETYPE_FUNC.FieldName, MDSLA.IDSDSCALETYPE_FUNC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSLA.IDSDSCALETYPE_HIER.FieldName, MDSLA.IDSDSCALETYPE_HIER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDSLA_GETID   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDSLA_GETID", "MDSLA_GETID description.");
    UnSQL.SQL.Add(" SELECT  ");
    UnSQL.SQL.Add("  MDSLA.IDCALEDAYDATE, ");
    UnSQL.SQL.Add("  MDSLA.IDMDIMPACT, ");
    UnSQL.SQL.Add("  MDSLA.IDMDSLA, ");
    UnSQL.SQL.Add("  MDSLA.SLACONDITION, ");
    UnSQL.SQL.Add("  MDSLA.SLADESCRIPTION, ");
    UnSQL.SQL.Add("  MDSLA.SLANAME, ");
    UnSQL.SQL.Add("  MDSLA.SLAORDER, ");
    UnSQL.SQL.Add("  MDSLA.SLAPARENT, ");
    UnSQL.SQL.Add("  MDSLA.SLASTATUS, ");
    UnSQL.SQL.Add("  MDSLA.SLA_MAXTIME, ");
    UnSQL.SQL.Add("  MDSLA.SLA_NORMALTIME, ");
    UnSQL.SQL.Add("  MDSLA.IDSDSCALETYPE_FUNC, ");
    UnSQL.SQL.Add("  MDSLA.IDSDSCALETYPE_HIER ");
    UnSQL.SQL.Add(" FROM  MDSLA ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  MDSLA.IDCALEDAYDATE=[IDCALEDAYDATE] and  ");
    UnSQL.SQL.Add("  MDSLA.IDMDIMPACT=[IDMDIMPACT] and  ");
    UnSQL.SQL.Add("  MDSLA.IDMDSLA=[IDMDSLA] and  ");
    UnSQL.SQL.Add("  MDSLA.SLACONDITION=[SLACONDITION] and  ");
    UnSQL.SQL.Add("  MDSLA.SLADESCRIPTION=[SLADESCRIPTION] and  ");
    UnSQL.SQL.Add("  MDSLA.SLANAME=[SLANAME] and  ");
    UnSQL.SQL.Add("  MDSLA.SLAORDER=[SLAORDER] and  ");
    UnSQL.SQL.Add("  MDSLA.SLAPARENT=[SLAPARENT] and  ");
    UnSQL.SQL.Add("  MDSLA.SLASTATUS=[SLASTATUS] and  ");
    UnSQL.SQL.Add("  MDSLA.SLA_MAXTIME=[SLA_MAXTIME] and ");
    UnSQL.SQL.Add("  MDSLA.SLA_NORMALTIME=[SLA_NORMALTIME] and ");
    UnSQL.SQL.Add("  MDSLA.IDSDSCALETYPE_FUNC=[IDSDSCALETYPE_FUNC] and ");
    UnSQL.SQL.Add("  MDSLA.IDSDSCALETYPE_HIER=[IDSDSCALETYPE_HIER] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_MDSLA = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDSLA_GETID", Param.ToBytes());
        ResErr = DS_MDSLA.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDSLA.DataSet.RecordCount > 0)
            {
                DS_MDSLA.DataSet.First();
                MDSLA.IDMDSLA = DS_MDSLA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSLA.IDMDSLA.FieldName).asInt32();
                //Other
                MDSLA.IDCALEDAYDATE = DS_MDSLA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSLA.IDCALEDAYDATE.FieldName).asInt32();
                MDSLA.IDMDIMPACT = DS_MDSLA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSLA.IDMDIMPACT.FieldName).asInt32();
                MDSLA.SLANAME = DS_MDSLA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSLA.SLANAME.FieldName).asString();
                MDSLA.SLAORDER = DS_MDSLA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSLA.SLAORDER.FieldName).asInt32();
                MDSLA.SLAPARENT = DS_MDSLA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSLA.SLAPARENT.FieldName).asInt32();
                MDSLA.SLASTATUS = DS_MDSLA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSLA.SLASTATUS.FieldName).asInt32();
                MDSLA.SLA_MAXTIME = DS_MDSLA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSLA.SLA_MAXTIME.FieldName).asInt32();
                MDSLA.SLA_NORMALTIME = DS_MDSLA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSLA.SLA_NORMALTIME.FieldName).asInt32();
                MDSLA.IDSDSCALETYPE_FUNC = UsrCfg.SD.Properties.TIDSDSCALETYPE_FUNC.GetEnum(DS_MDSLA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSLA.IDSDSCALETYPE_FUNC.FieldName).asInt32());
                MDSLA.IDSDSCALETYPE_HIER = UsrCfg.SD.Properties.TIDSDSCALETYPE_HIER.GetEnum(DS_MDSLA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSLA.IDSDSCALETYPE_HIER.FieldName).asInt32());
            }
            else
            {
                DS_MDSLA.ResErr.NotError = false;
                DS_MDSLA.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDSLA_ADD= function( MDSLA)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSLA.IDMDSLA.FieldName, MDSLA.IDMDSLA, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSLA.IDCALEDAYDATE.FieldName, MDSLA.IDCALEDAYDATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSLA.IDMDIMPACT.FieldName, MDSLA.IDMDIMPACT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.MDSLA.SLACONDITION.FieldName, MDSLA.SLACONDITION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.MDSLA.SLADESCRIPTION.FieldName, MDSLA.SLADESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSLA.SLANAME.FieldName, MDSLA.SLANAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSLA.SLAORDER.FieldName, MDSLA.SLAORDER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSLA.SLAPARENT.FieldName, MDSLA.SLAPARENT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSLA.SLASTATUS.FieldName, MDSLA.SLASTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSLA.SLA_MAXTIME.FieldName, MDSLA.SLA_MAXTIME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSLA.SLA_NORMALTIME.FieldName, MDSLA.SLA_NORMALTIME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSLA.IDSDSCALETYPE_FUNC.FieldName, MDSLA.IDSDSCALETYPE_FUNC.value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSLA.IDSDSCALETYPE_HIER.FieldName, MDSLA.IDSDSCALETYPE_HIER.value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDSLA_ADD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDSLA_ADD", "MDSLA_ADD description.");
    UnSQL.SQL.Add(" INSERT INTO MDSLA( ");
    UnSQL.SQL.Add("  IDCALEDAYDATE,  ");
    UnSQL.SQL.Add("  IDMDIMPACT,  ");
    UnSQL.SQL.Add("  IDMDSLA,  ");
    UnSQL.SQL.Add("  SLACONDITION,  ");
    UnSQL.SQL.Add("  SLADESCRIPTION,  ");
    UnSQL.SQL.Add("  SLANAME,  ");
    UnSQL.SQL.Add("  SLAORDER,  ");
    UnSQL.SQL.Add("  SLAPARENT,  ");
    UnSQL.SQL.Add("  SLASTATUS,  ");
    UnSQL.SQL.Add("  SLA_MAXTIME, ");
    UnSQL.SQL.Add("  SLA_NORMALTIME, ");
    UnSQL.SQL.Add("  IDSDSCALETYPE_FUNC, ");
    UnSQL.SQL.Add("  IDSDSCALETYPE_HIER ");
    UnSQL.SQL.Add(" )  ");
    UnSQL.SQL.Add(" VALUES ( ");
    UnSQL.SQL.Add("  [IDCALEDAYDATE], ");
    UnSQL.SQL.Add("  [IDMDIMPACT], ");
    UnSQL.SQL.Add("  [IDMDSLA], ");
    UnSQL.SQL.Add("  [SLACONDITION], ");
    UnSQL.SQL.Add("  [SLADESCRIPTION], ");
    UnSQL.SQL.Add("  [SLANAME], ");
    UnSQL.SQL.Add("  [SLAORDER], ");
    UnSQL.SQL.Add("  [SLAPARENT], ");
    UnSQL.SQL.Add("  [SLASTATUS], ");
    UnSQL.SQL.Add("  [SLA_MAXTIME], ");
    UnSQL.SQL.Add("  [SLA_NORMALTIME], ");
    UnSQL.SQL.Add("  [IDSDSCALETYPE_FUNC], ");
    UnSQL.SQL.Add("  [IDSDSCALETYPE_HIER] ");
    UnSQL.SQL.Add(" ) ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDSLA_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            MDSLA_GETID(MDSLA);
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDSLA_UPD = function( MDSLA)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSLA.IDMDSLA.FieldName, MDSLA.IDMDSLA, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSLA.IDCALEDAYDATE.FieldName, MDSLA.IDCALEDAYDATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSLA.IDMDIMPACT.FieldName, MDSLA.IDMDIMPACT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.MDSLA.SLACONDITION.FieldName, MDSLA.SLACONDITION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.MDSLA.SLADESCRIPTION.FieldName, MDSLA.SLADESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSLA.SLANAME.FieldName, MDSLA.SLANAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSLA.SLAORDER.FieldName, MDSLA.SLAORDER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSLA.SLAPARENT.FieldName, MDSLA.SLAPARENT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSLA.SLASTATUS.FieldName, MDSLA.SLASTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSLA.SLA_MAXTIME.FieldName, MDSLA.SLA_MAXTIME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSLA.SLA_NORMALTIME.FieldName, MDSLA.SLA_NORMALTIME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSLA.IDSDSCALETYPE_FUNC.FieldName, MDSLA.IDSDSCALETYPE_FUNC.value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSLA.IDSDSCALETYPE_HIER.FieldName, MDSLA.IDSDSCALETYPE_HIER.value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDSLA_UPD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDSLA_UPD", "MDSLA_UPD description.");
    UnSQL.SQL.Add(" UPDATE MDSLA SET ");
    UnSQL.SQL.Add("  IDCALEDAYDATE=[IDCALEDAYDATE],  ");
    UnSQL.SQL.Add("  IDMDIMPACT=[IDMDIMPACT],  ");
    UnSQL.SQL.Add("  IDMDSLA=[IDMDSLA],  ");
    UnSQL.SQL.Add("  SLACONDITION=[SLACONDITION],  ");
    UnSQL.SQL.Add("  SLADESCRIPTION=[SLADESCRIPTION],  ");
    UnSQL.SQL.Add("  SLANAME=[SLANAME],  ");
    UnSQL.SQL.Add("  SLAORDER=[SLAORDER],  ");
    UnSQL.SQL.Add("  SLAPARENT=[SLAPARENT],  ");
    UnSQL.SQL.Add("  SLASTATUS=[SLASTATUS],  ");
    UnSQL.SQL.Add("  SLA_MAXTIME=[SLA_MAXTIME], ");
    UnSQL.SQL.Add("  SLA_NORMALTIME=[SLA_NORMALTIME], ");
    UnSQL.SQL.Add("  IDSDSCALETYPE_FUNC=[IDSDSCALETYPE_FUNC], ");
    UnSQL.SQL.Add("  IDSDSCALETYPE_HIER=[IDSDSCALETYPE_HIER] ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  IDCALEDAYDATE=[IDCALEDAYDATE] AND  ");
    UnSQL.SQL.Add("  IDMDIMPACT=[IDMDIMPACT] AND  ");
    UnSQL.SQL.Add("  IDMDSLA=[IDMDSLA] AND  ");
    UnSQL.SQL.Add("  SLACONDITION=[SLACONDITION] AND  ");
    UnSQL.SQL.Add("  SLADESCRIPTION=[SLADESCRIPTION] AND  ");
    UnSQL.SQL.Add("  SLANAME=[SLANAME] AND  ");
    UnSQL.SQL.Add("  SLAORDER=[SLAORDER] AND  ");
    UnSQL.SQL.Add("  SLAPARENT=[SLAPARENT] AND  ");
    UnSQL.SQL.Add("  SLASTATUS=[SLASTATUS] AND  ");
    UnSQL.SQL.Add("  SLA_MAXTIME=[SLA_MAXTIME] and ");
    UnSQL.SQL.Add("  SLA_NORMALTIME=[SLA_NORMALTIME] and ");
    UnSQL.SQL.Add("  IDSDSCALETYPE_FUNC=[IDSDSCALETYPE_FUNC] and ");
    UnSQL.SQL.Add("  IDSDSCALETYPE_HIER=[IDSDSCALETYPE_HIER] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDSLA_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            //GET(); 
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDSLA_DEL= function(/* StrIDMDSLA*/ IDMDSLA)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        /*if (StrIDMDSLA == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDSLA.IDMDSLA.FieldName, " = " + UsrCfg.InternoAtisNames.MDSLA.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDSLA.IDMDSLA.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDMDSLA = " IN (" + StrIDMDSLA + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDSLA.IDMDSLA.FieldName, StrIDMDSLA, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSLA.IDMDSLA.FieldName, IDMDSLA, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDSLA_DEL_1   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDSLA_DEL_1", "MDSLA_DEL_1 description.");
    UnSQL.SQL.Add(" DELETE MDSLA  ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  IDCALEDAYDATE=[IDCALEDAYDATE] AND  ");
    UnSQL.SQL.Add("  IDMDIMPACT=[IDMDIMPACT] AND  ");
    UnSQL.SQL.Add("  IDMDSLA=[IDMDSLA] AND  ");
    UnSQL.SQL.Add("  SLACONDITION=[SLACONDITION] AND  ");
    UnSQL.SQL.Add("  SLADESCRIPTION=[SLADESCRIPTION] AND  ");
    UnSQL.SQL.Add("  SLANAME=[SLANAME] AND  ");
    UnSQL.SQL.Add("  SLAORDER=[SLAORDER] AND  ");
    UnSQL.SQL.Add("  SLAPARENT=[SLAPARENT] AND  ");
    UnSQL.SQL.Add("  SLASTATUS=[SLASTATUS] AND  ");
    UnSQL.SQL.Add("  SLA_MAXTIME=[SLA_MAXTIME] and ");
    UnSQL.SQL.Add("  SLA_NORMALTIME=[SLA_NORMALTIME] and ");
    UnSQL.SQL.Add("  IDSDSCALETYPE_FUNC=[IDSDSCALETYPE_FUNC] and ");
    UnSQL.SQL.Add("  IDSDSCALETYPE_HIER=[IDSDSCALETYPE_HIER] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDSLA_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDSLA_DEL=function(MDSLA)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSLA.IDMDSLA.FieldName, MDSLA.IDMDSLA, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSLA.IDCALEDAYDATE.FieldName, MDSLA.IDCALEDAYDATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSLA.IDMDIMPACT.FieldName, MDSLA.IDMDIMPACT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.MDSLA.SLACONDITION.FieldName, MDSLA.SLACONDITION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.MDSLA.SLADESCRIPTION.FieldName, MDSLA.SLADESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSLA.SLANAME.FieldName, MDSLA.SLANAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSLA.SLAORDER.FieldName, MDSLA.SLAORDER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSLA.SLAPARENT.FieldName, MDSLA.SLAPARENT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSLA.SLASTATUS.FieldName, MDSLA.SLASTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSLA.SLA_MAXTIME.FieldName, MDSLA.SLA_MAXTIME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSLA.SLA_NORMALTIME.FieldName, MDSLA.SLA_NORMALTIME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSLA.IDSDSCALETYPE_FUNC.FieldName, MDSLA.IDSDSCALETYPE_FUNC.value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSLA.IDSDSCALETYPE_HIER.FieldName, MDSLA.IDSDSCALETYPE_HIER.value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDSLA_DEL_2   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDSLA_DEL_2", "MDSLA_DEL_2 description.");
    UnSQL.SQL.Add(" DELETE MDSLA  ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  IDCALEDAYDATE=[IDCALEDAYDATE] AND  ");
    UnSQL.SQL.Add("  IDMDIMPACT=[IDMDIMPACT] AND  ");
    UnSQL.SQL.Add("  IDMDSLA=[IDMDSLA] AND  ");
    UnSQL.SQL.Add("  SLACONDITION=[SLACONDITION] AND  ");
    UnSQL.SQL.Add("  SLADESCRIPTION=[SLADESCRIPTION] AND  ");
    UnSQL.SQL.Add("  SLANAME=[SLANAME] AND  ");
    UnSQL.SQL.Add("  SLAORDER=[SLAORDER] AND  ");
    UnSQL.SQL.Add("  SLAPARENT=[SLAPARENT] AND  ");
    UnSQL.SQL.Add("  SLASTATUS=[SLASTATUS] AND  ");
    UnSQL.SQL.Add("  SLA_MAXTIME=[SLA_MAXTIME] and");
    UnSQL.SQL.Add("  SLA_NORMALTIME=[SLA_NORMALTIME] and");
    UnSQL.SQL.Add("  IDSDSCALETYPE_FUNC=[IDSDSCALETYPE_FUNC] and");
    UnSQL.SQL.Add("  IDSDSCALETYPE_HIER=[IDSDSCALETYPE_HIER] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDSLA_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
    
//**********************   METHODS   ********************************************************************************************

    
//DataSet Function 

//List Function 
Persistence.Model.Methods.MDSLA_ListSetID = function(MDSLAList, IDMDSLA)
{
    for (i = 0; i < MDSLAList.length; i++)
    {

        if (IDMDSLA == MDSLAList[i].IDMDSLA)
            return (MDSLAList[i]);

    } 

    var TMDSLA = new Persistence.Properties.TMDSLA();
    TMDSLA.IDMDSLA = IDMDSLA; 
    return (TMDSLA); 
}
Persistence.Model.Methods.MDSLA_ListAdd= function(MDSLAList, MDSLA)
{
    var i = Persistence.Model.Methods.MDSLA_ListGetIndex(MDSLAList, MDSLA);
    if (i == -1) MDSLAList.push(MDSLA);
    else
    {
        MDSLAList[i].IDCALEDAYDATE = MDSLA.IDCALEDAYDATE;
        MDSLAList[i].IDMDIMPACT = MDSLA.IDMDIMPACT;
        MDSLAList[i].IDMDSLA = MDSLA.IDMDSLA;
        MDSLAList[i].SLACONDITION = MDSLA.SLACONDITION;
        MDSLAList[i].SLADESCRIPTION = MDSLA.SLADESCRIPTION;
        MDSLAList[i].SLANAME = MDSLA.SLANAME;
        MDSLAList[i].SLAORDER = MDSLA.SLAORDER;
        MDSLAList[i].SLAPARENT = MDSLA.SLAPARENT;
        MDSLAList[i].SLASTATUS = MDSLA.SLASTATUS;
        MDSLAList[i].SLA_MAXTIME = MDSLA.SLA_MAXTIME;
        MDSLAList[i].SLA_NORMALTIME = MDSLA.SLA_NORMALTIME;
        MDSLAList[i].IDSDSCALETYPE_FUNC = MDSLA.IDSDSCALETYPE_FUNC.value;
        MDSLAList[i].IDSDSCALETYPE_HIER = MDSLA.IDSDSCALETYPE_HIER.value;
    }
}
//CJRC_26072018
Persistence.Model.Methods.MDSLA_ListGetIndex = function (MDSLAList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.Model.Methods.MDSLA_ListGetIndexIDMDSLA(MDSLAList, Param);

    }
    else {
        Res = Persistence.Model.Methods.MDSLA_ListGetIndexMDSLA(MDSLAList, Param);
    }
    return (Res);
}

Persistence.Model.Methods.MDSLA_ListGetIndexMDSLA = function (MDSLAList, MDSLA)
{
    for (i = 0; i < MDSLAList.length; i++)
    {
        if (MDSLA.IDMDSLA == MDSLAList[i].IDMDSLA)
            return (i);
    }
    return (-1);
}
Persistence.Model.Methods.MDSLA_ListGetIndexIDMDSLA = function (MDSLAList, IDMDSLA)
{
    for (i = 0; i < MDSLAList.length; i++)
    {
        if (IDMDSLA == MDSLAList[i].IDMDSLA)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.Model.Methods.MDSLAListtoStr = function(MDSLAList)
{
    var Res = Persistence.Model.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(MDSLAList.length, Longitud, Texto);
        for (Counter = 0; Counter < MDSLAList.length; Counter++) {
            var UnMDSLA = MDSLAList[Counter];
            UnMDSLA.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Model.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.Model.Methods.StrtoMDSLA = function(ProtocoloStr, MDSLAList)
{
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        MDSLAList.length = 0;
        if (Persistence.Model.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnMDSLA = new Persistence.Demo.Properties.TMDSLA(); //Mode new row
                UnMDSLA.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.MDSLA_ListAdd(MDSLAList, UnMDSLA);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
    
}

//Socket Function 
Persistence.Model.Methods.MDSLAListToByte = function(MDSLAList, MemStream)
{
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, MDSLAList.length);
    for (i = 0; i < MDSLAList.length; i++)
    {
        MDSLAList[i].ToByte(MemStream);
    }
}
Persistence.Model.Methods.ByteToMDSLAList = function(MDSLAList, MemStream)
{
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++)
    {
        var UnMDSLA = new Persistence.Model.Properties.TMDSLA();
        MDSLAList[i].ToByte(MemStream);
    }
}
    
 
