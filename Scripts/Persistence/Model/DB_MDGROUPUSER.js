﻿//TreduceDB
//Persistence.Model.Properties
//Persistence.Model.Methods

 
Persistence.Model.Properties.TMDGROUPUSER = function()
{
    this.IDCMDBCI = 0;
    this.IDMDGROUP  = 0;
    this.IDMDGROUPUSER  = 0;
    this.IDSYSTEMSTATUS  = 0;
    //Socket IO Properties
    this.ToByte = function(MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDCMDBCI);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDGROUP);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDGROUPUSER);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSYSTEMSTATUS);
    }
    this.ByteTo = function(MemStream)
    {
        this.IDCMDBCI = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDGROUP = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDGROUPUSER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSYSTEMSTATUS = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    }
    //Str IO Properties
    this.ToStr = function(Longitud, Texto)
    {
        SysCfg.Str.Protocol.WriteInt(this.IDCMDBCI, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDGROUP, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDGROUPUSER, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDSYSTEMSTATUS, Longitud, Texto);
    }
    this.StrTo = function( Pos,  Index,  LongitudArray,  Texto)
    {
        this.IDCMDBCI = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDGROUP = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDGROUPUSER = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDSYSTEMSTATUS = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
    }
}
   

//**********************   METHODS server  ********************************************************************************************

    
//DataSet Function 
Persistence.Model.Methods.MDGROUPUSER_Fill=function(MDGROUPUSERList,  StrIDMDGROUPUSER/* IDMDGROUPUSER*/)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    MDGROUPUSERList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDMDGROUPUSER == "")
    {
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDGROUPUSER.IDMDGROUPUSER.FieldName, " = " + UsrCfg.InternoAtisNames.MDGROUPUSER.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDGROUPUSER.IDMDGROUPUSER.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else
    {
        StrIDMDGROUPUSER = " IN (" + StrIDMDGROUPUSER + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDGROUPUSER.IDMDGROUPUSER.FieldName, StrIDMDGROUPUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //Param.AddInt32(UsrCfg.InternoDataLinkNames.MDGROUPUSER.IDMDGROUPUSER.FieldName, IDMDGROUPUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try
    {
        /*

    //********   MDGROUPUSER_GET   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDGROUPUSER_GET", "MDGROUPUSER_GET description.");
    UnSQL.SQL.Add(" SELECT  ");
    UnSQL.SQL.Add("  MDGROUPUSER.IDCMDBCI, ");
    UnSQL.SQL.Add("  MDGROUPUSER.IDMDGROUP, ");
    UnSQL.SQL.Add("  MDGROUPUSER.IDMDGROUPUSER, ");
    UnSQL.SQL.Add("  MDGROUPUSER.IDSYSTEMSTATUS ");
    UnSQL.SQL.Add(" FROM  MDGROUPUSER ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  MDGROUPUSER.IDCMDBCI=[IDCMDBCI] and  ");
    UnSQL.SQL.Add("  MDGROUPUSER.IDMDGROUP=[IDMDGROUP] and  ");
    UnSQL.SQL.Add("  MDGROUPUSER.IDMDGROUPUSER=[IDMDGROUPUSER] and  ");
    UnSQL.SQL.Add("  MDGROUPUSER.IDSYSTEMSTATUS=[IDSYSTEMSTATUS] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_MDGROUPUSER = SysCfg.DB.SQL.Methods.OpenDataSet("DataLink", "MDGROUPUSER_GET", Param.ToBytes());
        ResErr = DS_MDGROUPUSER.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDGROUPUSER.DataSet.RecordCount > 0)
            {
                DS_MDGROUPUSER.DataSet.First();
                while (!(DS_MDGROUPUSER.DataSet.Eof))
                {
                    var MDGROUPUSER = new Persistence.Model.Properties.TMDGROUPUSER();
                    MDGROUPUSER.IDMDGROUPUSER = DS_MDGROUPUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDGROUPUSER.IDMDGROUPUSER.FieldName).asInt32();
                    //Other
                    MDGROUPUSER.IDCMDBCI = DS_MDGROUPUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDGROUPUSER.IDCMDBCI.FieldName).asInt32();
                    MDGROUPUSER.IDMDGROUP = DS_MDGROUPUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDGROUPUSER.IDMDGROUP.FieldName).asInt32();
                    MDGROUPUSER.IDSYSTEMSTATUS = DS_MDGROUPUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDGROUPUSER.IDSYSTEMSTATUS.FieldName).asInt32();
                    MDGROUPUSERList.push(MDGROUPUSER);

                    DS_MDGROUPUSER.DataSet.Next();
                }
            }
            else
            {
                DS_MDGROUPUSER.ResErr.NotError = false;
                DS_MDGROUPUSER.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDGROUPUSER_GETID = function(MDGROUPUSER)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDGROUPUSER.IDMDGROUPUSER.FieldName, MDGROUPUSER.IDMDGROUPUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.MDGROUPUSER.IDCMDBCI.FieldName, MDGROUPUSER.IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDGROUPUSER.IDMDGROUP.FieldName, MDGROUPUSER.IDMDGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDGROUPUSER.IDSYSTEMSTATUS.FieldName, MDGROUPUSER.IDSYSTEMSTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDGROUPUSER_GETID   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDGROUPUSER_GETID", "MDGROUPUSER_GETID description.");
    UnSQL.SQL.Add(" SELECT  ");
    UnSQL.SQL.Add("  MDGROUPUSER.IDCMDBCI, ");
    UnSQL.SQL.Add("  MDGROUPUSER.IDMDGROUP, ");
    UnSQL.SQL.Add("  MDGROUPUSER.IDMDGROUPUSER, ");
    UnSQL.SQL.Add("  MDGROUPUSER.IDSYSTEMSTATUS ");
    UnSQL.SQL.Add(" FROM  MDGROUPUSER ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  MDGROUPUSER.IDCMDBCI=[IDCMDBCI] and  ");
    UnSQL.SQL.Add("  MDGROUPUSER.IDMDGROUP=[IDMDGROUP] and  ");
    UnSQL.SQL.Add("  MDGROUPUSER.IDMDGROUPUSER=[IDMDGROUPUSER] and  ");
    UnSQL.SQL.Add("  MDGROUPUSER.IDSYSTEMSTATUS=[IDSYSTEMSTATUS] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_MDGROUPUSER = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDGROUPUSER_GETID", Param.ToBytes());
        ResErr = DS_MDGROUPUSER.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDGROUPUSER.DataSet.RecordCount > 0)
            {
                DS_MDGROUPUSER.DataSet.First();
                MDGROUPUSER.IDMDGROUPUSER = DS_MDGROUPUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDGROUPUSER.IDMDGROUPUSER.FieldName).asInt32();
                //Other
                MDGROUPUSER.IDCMDBCI = DS_MDGROUPUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDGROUPUSER.IDCMDBCI.FieldName).asInt32();
                MDGROUPUSER.IDMDGROUP = DS_MDGROUPUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDGROUPUSER.IDMDGROUP.FieldName).asInt32();
                MDGROUPUSER.IDSYSTEMSTATUS = DS_MDGROUPUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDGROUPUSER.IDSYSTEMSTATUS.FieldName).asInt32();
            }
            else
            {
                DS_MDGROUPUSER.ResErr.NotError = false;
                DS_MDGROUPUSER.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDGROUPUSER_ADD=function(MDGROUPUSER)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDGROUPUSER.IDMDGROUPUSER.FieldName, MDGROUPUSER.IDMDGROUPUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.MDGROUPUSER.IDCMDBCI.FieldName, MDGROUPUSER.IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDGROUPUSER.IDMDGROUP.FieldName, MDGROUPUSER.IDMDGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDGROUPUSER.IDSYSTEMSTATUS.FieldName, MDGROUPUSER.IDSYSTEMSTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDGROUPUSER_ADD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDGROUPUSER_ADD", "MDGROUPUSER_ADD description.");
    UnSQL.SQL.Add(" INSERT INTO MDGROUPUSER( ");
    UnSQL.SQL.Add("  IDCMDBCI,  ");
    UnSQL.SQL.Add("  IDMDGROUP,  ");
    UnSQL.SQL.Add("  IDMDGROUPUSER,  ");
    UnSQL.SQL.Add("  IDSYSTEMSTATUS ");
    UnSQL.SQL.Add(" )  ");
    UnSQL.SQL.Add(" VALUES ( ");
    UnSQL.SQL.Add("  [IDCMDBCI], ");
    UnSQL.SQL.Add("  [IDMDGROUP], ");
    UnSQL.SQL.Add("  [IDMDGROUPUSER], ");
    UnSQL.SQL.Add("  [IDSYSTEMSTATUS] ");
    UnSQL.SQL.Add(" ) ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDGROUPUSER_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            MDGROUPUSER_GETID(MDGROUPUSER);
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDGROUPUSER_UPD=function(MDGROUPUSER)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDGROUPUSER.IDMDGROUPUSER.FieldName, MDGROUPUSER.IDMDGROUPUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.MDGROUPUSER.IDCMDBCI.FieldName, MDGROUPUSER.IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDGROUPUSER.IDMDGROUP.FieldName, MDGROUPUSER.IDMDGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDGROUPUSER.IDSYSTEMSTATUS.FieldName, MDGROUPUSER.IDSYSTEMSTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDGROUPUSER_UPD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDGROUPUSER_UPD", "MDGROUPUSER_UPD description.");
    UnSQL.SQL.Add(" UPDATE MDGROUPUSER SET ");
    UnSQL.SQL.Add("  IDCMDBCI=[IDCMDBCI],  ");
    UnSQL.SQL.Add("  IDMDGROUP=[IDMDGROUP],  ");
    UnSQL.SQL.Add("  IDMDGROUPUSER=[IDMDGROUPUSER],  ");
    UnSQL.SQL.Add("  IDSYSTEMSTATUS=[IDSYSTEMSTATUS] ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  IDCMDBCI=[IDCMDBCI] AND  ");
    UnSQL.SQL.Add("  IDMDGROUP=[IDMDGROUP] AND  ");
    UnSQL.SQL.Add("  IDMDGROUPUSER=[IDMDGROUPUSER] AND  ");
    UnSQL.SQL.Add("  IDSYSTEMSTATUS=[IDSYSTEMSTATUS] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDGROUPUSER_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            //GET(); 
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDGROUPUSER_DEL=function(/* StrIDMDGROUPUSER*/ IDMDGROUPUSER)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        /*if (StrIDMDGROUPUSER == "")
        {
            Param.AddUnknown(UsrCfg.InternoDataLinkNames.MDGROUPUSER.IDMDGROUPUSER.FieldName, " = " + UsrCfg.InternoDataLinkNames.MDGROUPUSER.NAME_TABLE + "." + UsrCfg.InternoDataLinkNames.MDGROUPUSER.IDMDGROUPUSER.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDMDGROUPUSER = " IN (" + StrIDMDGROUPUSER + ")";
            Param.AddUnknown(UsrCfg.InternoDataLinkNames.MDGROUPUSER.IDMDGROUPUSER.FieldName, StrIDMDGROUPUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.MDGROUPUSER.IDMDGROUPUSER.FieldName, IDMDGROUPUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDGROUPUSER_DEL_1   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDGROUPUSER_DEL_1", "MDGROUPUSER_DEL_1 description.");
    UnSQL.SQL.Add(" DELETE MDGROUPUSER  ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  IDCMDBCI=[IDCMDBCI] AND  ");
    UnSQL.SQL.Add("  IDMDGROUP=[IDMDGROUP] AND  ");
    UnSQL.SQL.Add("  IDMDGROUPUSER=[IDMDGROUPUSER] AND  ");
    UnSQL.SQL.Add("  IDSYSTEMSTATUS=[IDSYSTEMSTATUS] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDGROUPUSER_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDGROUPUSER_DEL=function(MDGROUPUSER)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDGROUPUSER.IDMDGROUPUSER.FieldName, MDGROUPUSER.IDMDGROUPUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.MDGROUPUSER.IDCMDBCI.FieldName, MDGROUPUSER.IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDGROUPUSER.IDMDGROUP.FieldName, MDGROUPUSER.IDMDGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDGROUPUSER.IDSYSTEMSTATUS.FieldName, MDGROUPUSER.IDSYSTEMSTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDGROUPUSER_DEL_2   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDGROUPUSER_DEL_2", "MDGROUPUSER_DEL_2 description.");
    UnSQL.SQL.Add(" DELETE MDGROUPUSER  ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  IDCMDBCI=[IDCMDBCI] AND  ");
    UnSQL.SQL.Add("  IDMDGROUP=[IDMDGROUP] AND  ");
    UnSQL.SQL.Add("  IDMDGROUPUSER=[IDMDGROUPUSER] AND  ");
    UnSQL.SQL.Add("  IDSYSTEMSTATUS=[IDSYSTEMSTATUS] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDGROUPUSER_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
    

//**********************   METHODS   ********************************************************************************************

     
//DataSet Function 

//List Function 
Persistence.Model.Methods.MDGROUPUSER_ListSetID=function(MDGROUPUSERList, IDMDGROUPUSER)
{
    for (i = 0; i < MDGROUPUSERList.length; i++)
    {

        if (IDMDGROUPUSER == MDGROUPUSERList[i].IDMDGROUPUSER)
            return (MDGROUPUSERList[i]);

    }

    var TMDGROUPUSER = new Persistence.Properties.TMDGROUPUSER();
    TMDGROUPUSER.IDMDGROUPUSER = IDMDGROUPUSER; 
    return (TMDGROUPUSER); 
}
Persistence.Model.Methods.MDGROUPUSER_ListAdd=function(MDGROUPUSERList, MDGROUPUSER)
{
    var i = Persistence.Model.Methods.MDGROUPUSER_ListGetIndex(MDGROUPUSERList, MDGROUPUSER);
    if (i == -1) MDGROUPUSERList.push(MDGROUPUSER);
    else
    {
        MDGROUPUSERList[i].IDCMDBCI = MDGROUPUSER.IDCMDBCI;
        MDGROUPUSERList[i].IDMDGROUP = MDGROUPUSER.IDMDGROUP;
        MDGROUPUSERList[i].IDMDGROUPUSER = MDGROUPUSER.IDMDGROUPUSER;
        MDGROUPUSERList[i].IDSYSTEMSTATUS = MDGROUPUSER.IDSYSTEMSTATUS;
    }
}
Persistence.Model.Methods.MDGROUPUSER_ListGetIndex=function(MDGROUPUSERList, MDGROUPUSER)
{
    for (i = 0; i < MDGROUPUSERList.length; i++)
    {
        if (MDGROUPUSER.IDMDGROUPUSER == MDGROUPUSERList[i].IDMDGROUPUSER)
            return (i);
    }
    return (-1);
}
Persistence.Model.Methods.MDGROUPUSER_ListGetIndex=function(MDGROUPUSERList, IDMDGROUPUSER)
{
    for (i = 0; i < MDGROUPUSERList.length; i++)
    {
        if (IDMDGROUPUSER == MDGROUPUSERList[i].IDMDGROUPUSER)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.Model.Methods.MDGROUPUSERListtoStr=function(MDGROUPUSERList)
{
    var Res = Persistence.Model.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(MDGROUPUSERList.length, Longitud, Texto);
        for (Counter = 0; Counter < MDGROUPUSERList.length; Counter++) {
            var UnMDGROUPUSER = MDGROUPUSERList[Counter];
            UnMDGROUPUSER.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Model.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.Model.Methods.StrtoMDGROUPUSER=function(ProtocoloStr, MDGROUPUSERList)
{
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        MDGROUPUSERList.length = 0;
        if (Persistence.Model.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnMDGROUPUSER = new Persistence.Demo.Properties.TMDGROUPUSER(); //Mode new row
                UnMDGROUPUSER.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.MDGROUPUSER_ListAdd(MDGROUPUSERList, UnMDGROUPUSER);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
    
}

//Socket Function 
Persistence.Model.Methods.MDGROUPUSERListToByte=function(MDGROUPUSERList, MemStream)
{
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, MDGROUPUSERList.length);
    for (i = 0; i < MDGROUPUSERList.length; i++)
    {
        MDGROUPUSERList[i].ToByte(MemStream);
    }
}
Persistence.Model.Methods.ByteToMDGROUPUSERList=function(MDGROUPUSERList, MemStream)
{
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++)
    {
        var UnMDGROUPUSER = new Persistence.Model.Properties.TMDGROUPUSER();
        MDGROUPUSERList[i].ToByte(MemStream);
    }
}
  