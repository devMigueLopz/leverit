﻿//TreduceDB
//Persistence.Model.Properties
//Persistence.Model.Methods

 
 
Persistence.Model.Properties.TMDGROUP = function()
{
    this.COMMENTSG = "";
    this.IDMDGROUP= 0;
    this.IDSYSTEMSTATUS= 0;
    this.NAMEGROUP = "";
    //Socket IO Properties
    this.ToByte = function(MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.COMMENTSG);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDGROUP);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSYSTEMSTATUS);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.NAMEGROUP);
    }
    this.ByteTo = function(MemStream)
    {
        this.COMMENTSG = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDMDGROUP = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSYSTEMSTATUS = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.NAMEGROUP = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    }
    //Str IO Properties
    this.ToStr = function(Longitud, Texto)
    {
        SysCfg.Str.Protocol.WriteStr(this.COMMENTSG, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDGROUP, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDSYSTEMSTATUS, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.NAMEGROUP, Longitud, Texto);
    }
    this.StrTo = function(Pos, Index, LongitudArray, Texto)
    {
        this.COMMENTSG = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.IDMDGROUP = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDSYSTEMSTATUS = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.NAMEGROUP = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
    }
}
    

//**********************   METHODS server  ********************************************************************************************

   
//DataSet Function 
Persistence.Model.Methods.MDGROUP_Fill=function(MDGROUPList, StrIDMDGROUP/*IDMDGROUP*/)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    MDGROUPList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDMDGROUP == "")
    {
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDGROUP.IDMDGROUP.FieldName, " = " + UsrCfg.InternoAtisNames.MDGROUP.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDGROUP.IDMDGROUP.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else
    {
        StrIDMDGROUP = " IN (" + StrIDMDGROUP + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDGROUP.IDMDGROUP.FieldName, StrIDMDGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //Param.AddInt32(UsrCfg.InternoDataLinkNames.MDGROUP.IDMDGROUP.FieldName, IDMDGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try
    {
        /*

    //********   MDGROUP_GET   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDGROUP_GET", "MDGROUP_GET description.");
    UnSQL.SQL.Add(" SELECT  ");
    UnSQL.SQL.Add("  MDGROUP.COMMENTSG, ");
    UnSQL.SQL.Add("  MDGROUP.IDMDGROUP, ");
    UnSQL.SQL.Add("  MDGROUP.IDSYSTEMSTATUS, ");
    UnSQL.SQL.Add("  MDGROUP.NAMEGROUP ");
    UnSQL.SQL.Add(" FROM  MDGROUP ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  MDGROUP.COMMENTSG=[COMMENTSG] and  ");
    UnSQL.SQL.Add("  MDGROUP.IDMDGROUP=[IDMDGROUP] and  ");
    UnSQL.SQL.Add("  MDGROUP.IDSYSTEMSTATUS=[IDSYSTEMSTATUS] and  ");
    UnSQL.SQL.Add("  MDGROUP.NAMEGROUP=[NAMEGROUP] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_MDGROUP = SysCfg.DB.SQL.Methods.OpenDataSet("DataLink", "MDGROUP_GET", Param.ToBytes());
        ResErr = DS_MDGROUP.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDGROUP.DataSet.RecordCount > 0)
            {
                DS_MDGROUP.DataSet.First();
                while (!(DS_MDGROUP.DataSet.Eof))
                {
                    var MDGROUP = new Persistence.Model.Properties.TMDGROUP();
                    MDGROUP.IDMDGROUP = DS_MDGROUP.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDGROUP.IDMDGROUP.FieldName).asInt32();
                    //Other
                    MDGROUP.IDSYSTEMSTATUS = DS_MDGROUP.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDGROUP.IDSYSTEMSTATUS.FieldName).asInt32();
                    MDGROUP.NAMEGROUP = DS_MDGROUP.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDGROUP.NAMEGROUP.FieldName).asString();
                    MDGROUPList.push(MDGROUP);

                    DS_MDGROUP.DataSet.Next();
                }
            }
            else
            {
                DS_MDGROUP.ResErr.NotError = false;
                DS_MDGROUP.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDGROUP_GETID=function(MDGROUP)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDGROUP.IDMDGROUP.FieldName, MDGROUP.IDMDGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDGROUP.COMMENTSG.FieldName, MDGROUP.COMMENTSG, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDGROUP.IDSYSTEMSTATUS.FieldName, MDGROUP.IDSYSTEMSTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDGROUP.NAMEGROUP.FieldName, MDGROUP.NAMEGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDGROUP_GETID   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDGROUP_GETID", "MDGROUP_GETID description.");
    UnSQL.SQL.Add(" SELECT  ");
    UnSQL.SQL.Add("  MDGROUP.COMMENTSG, ");
    UnSQL.SQL.Add("  MDGROUP.IDMDGROUP, ");
    UnSQL.SQL.Add("  MDGROUP.IDSYSTEMSTATUS, ");
    UnSQL.SQL.Add("  MDGROUP.NAMEGROUP ");
    UnSQL.SQL.Add(" FROM  MDGROUP ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  MDGROUP.COMMENTSG=[COMMENTSG] and  ");
    UnSQL.SQL.Add("  MDGROUP.IDMDGROUP=[IDMDGROUP] and  ");
    UnSQL.SQL.Add("  MDGROUP.IDSYSTEMSTATUS=[IDSYSTEMSTATUS] and  ");
    UnSQL.SQL.Add("  MDGROUP.NAMEGROUP=[NAMEGROUP] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_MDGROUP = SysCfg.DB.SQL.Methods.OpenDataSet("DataLink", "MDGROUP_GETID", Param.ToBytes());
        ResErr = DS_MDGROUP.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDGROUP.DataSet.RecordCount > 0)
            {
                DS_MDGROUP.DataSet.First();
                MDGROUP.IDMDGROUP = DS_MDGROUP.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDGROUP.IDMDGROUP.FieldName).asInt32();
                //Other
                MDGROUP.IDSYSTEMSTATUS = DS_MDGROUP.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDGROUP.IDSYSTEMSTATUS.FieldName).asInt32();
                MDGROUP.NAMEGROUP = DS_MDGROUP.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDGROUP.NAMEGROUP.FieldName).asString();
            }
            else
            {
                DS_MDGROUP.ResErr.NotError = false;
                DS_MDGROUP.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDGROUP_ADD=function(MDGROUP)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDGROUP.IDMDGROUP.FieldName, MDGROUP.IDMDGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDGROUP.COMMENTSG.FieldName, MDGROUP.COMMENTSG, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDGROUP.IDSYSTEMSTATUS.FieldName, MDGROUP.IDSYSTEMSTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDGROUP.NAMEGROUP.FieldName, MDGROUP.NAMEGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDGROUP_ADD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDGROUP_ADD", "MDGROUP_ADD description.");
    UnSQL.SQL.Add(" INSERT INTO MDGROUP( ");
    UnSQL.SQL.Add("  COMMENTSG,  ");
    UnSQL.SQL.Add("  IDMDGROUP,  ");
    UnSQL.SQL.Add("  IDSYSTEMSTATUS,  ");
    UnSQL.SQL.Add("  NAMEGROUP ");
    UnSQL.SQL.Add(" )  ");
    UnSQL.SQL.Add(" VALUES ( ");
    UnSQL.SQL.Add("  [COMMENTSG], ");
    UnSQL.SQL.Add("  [IDMDGROUP], ");
    UnSQL.SQL.Add("  [IDSYSTEMSTATUS], ");
    UnSQL.SQL.Add("  [NAMEGROUP] ");
    UnSQL.SQL.Add(" ) ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("DataLink", "MDGROUP_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            MDGROUP_GETID(MDGROUP);
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDGROUP_UPD=function(MDGROUP)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDGROUP.IDMDGROUP.FieldName, MDGROUP.IDMDGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDGROUP.COMMENTSG.FieldName, MDGROUP.COMMENTSG, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDGROUP.IDSYSTEMSTATUS.FieldName, MDGROUP.IDSYSTEMSTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDGROUP.NAMEGROUP.FieldName, MDGROUP.NAMEGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDGROUP_UPD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDGROUP_UPD", "MDGROUP_UPD description.");
    UnSQL.SQL.Add(" UPDATE MDGROUP SET ");
    UnSQL.SQL.Add("  COMMENTSG=[COMMENTSG],  ");
    UnSQL.SQL.Add("  IDMDGROUP=[IDMDGROUP],  ");
    UnSQL.SQL.Add("  IDSYSTEMSTATUS=[IDSYSTEMSTATUS],  ");
    UnSQL.SQL.Add("  NAMEGROUP=[NAMEGROUP] ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  COMMENTSG=[COMMENTSG] AND  ");
    UnSQL.SQL.Add("  IDMDGROUP=[IDMDGROUP] AND  ");
    UnSQL.SQL.Add("  IDSYSTEMSTATUS=[IDSYSTEMSTATUS] AND  ");
    UnSQL.SQL.Add("  NAMEGROUP=[NAMEGROUP] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("DataLink", "MDGROUP_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            //GET(); 
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDGROUP_DEL=function(/* StrIDMDGROUP*/ IDMDGROUP)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        /*if (StrIDMDGROUP == "")
        {
            Param.AddUnknown(UsrCfg.InternoDataLinkNames.MDGROUP.IDMDGROUP.FieldName, " = " + UsrCfg.InternoDataLinkNames.MDGROUP.NAME_TABLE + "." + UsrCfg.InternoDataLinkNames.MDGROUP.IDMDGROUP.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDMDGROUP = " IN (" + StrIDMDGROUP + ")";
            Param.AddUnknown(UsrCfg.InternoDataLinkNames.MDGROUP.IDMDGROUP.FieldName, StrIDMDGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.MDGROUP.IDMDGROUP.FieldName, IDMDGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDGROUP_DEL_1   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDGROUP_DEL_1", "MDGROUP_DEL_1 description.");
    UnSQL.SQL.Add(" DELETE MDGROUP  ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  COMMENTSG=[COMMENTSG] AND  ");
    UnSQL.SQL.Add("  IDMDGROUP=[IDMDGROUP] AND  ");
    UnSQL.SQL.Add("  IDSYSTEMSTATUS=[IDSYSTEMSTATUS] AND  ");
    UnSQL.SQL.Add("  NAMEGROUP=[NAMEGROUP] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("DataLink", "MDGROUP_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDGROUP_DEL=function(MDGROUP)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDGROUP.IDMDGROUP.FieldName, MDGROUP.IDMDGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDGROUP.COMMENTSG.FieldName, MDGROUP.COMMENTSG, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDGROUP.IDSYSTEMSTATUS.FieldName, MDGROUP.IDSYSTEMSTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDGROUP.NAMEGROUP.FieldName, MDGROUP.NAMEGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDGROUP_DEL_2   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDGROUP_DEL_2", "MDGROUP_DEL_2 description.");
    UnSQL.SQL.Add(" DELETE MDGROUP  ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  COMMENTSG=[COMMENTSG] AND  ");
    UnSQL.SQL.Add("  IDMDGROUP=[IDMDGROUP] AND  ");
    UnSQL.SQL.Add("  IDSYSTEMSTATUS=[IDSYSTEMSTATUS] AND  ");
    UnSQL.SQL.Add("  NAMEGROUP=[NAMEGROUP] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("DataLink", "MDGROUP_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
     
//**********************   METHODS   ********************************************************************************************

   
//DataSet Function 

//List Function 
Persistence.Model.Methods.MDGROUP_ListSetID=function(MDGROUPList, IDMDGROUP)
{
    for (i = 0; i < MDGROUPList.length; i++)
    {

        if (IDMDGROUP == MDGROUPList[i].IDMDGROUP)
            return (MDGROUPList[i]);

    }

    var TMDGROUP = new Persistence.Properties.TMDGROUP();
    TMDGROUP.IDMDGROUP = IDMDGROUP; 
    return (TMDGROUP); 

}

Persistence.Model.Methods.MDGROUP_ListAdd=function(MDGROUPList, MDGROUP)
{
    var i = Persistence.Model.Methods.MDGROUP_ListGetIndex(MDGROUPList, MDGROUP);
    if (i == -1) MDGROUPList.push(MDGROUP);
    else
    {
        MDGROUPList[i].COMMENTSG = MDGROUP.COMMENTSG;
        MDGROUPList[i].IDMDGROUP = MDGROUP.IDMDGROUP;
        MDGROUPList[i].IDSYSTEMSTATUS = MDGROUP.IDSYSTEMSTATUS;
        MDGROUPList[i].NAMEGROUP = MDGROUP.NAMEGROUP;
    }
}
Persistence.Model.Methods.MDGROUP_ListGetIndex=function(MDGROUPList, MDGROUP)
{
    for (i = 0; i < MDGROUPList.length; i++)
    {
        if (MDGROUP.IDMDGROUP == MDGROUPList[i].IDMDGROUP)
            return (i);
    }
    return (-1);
}
Persistence.Model.Methods.MDGROUP_ListGetIndex=function(MDGROUPList, IDMDGROUP)
{
    for (i = 0; i < MDGROUPList.length; i++)
    {
        if (IDMDGROUP == MDGROUPList[i].IDMDGROUP)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.Model.Methods.MDGROUPListtoStr=function(MDGROUPList)
{
    var Res = Persistence.Model.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(MDGROUPList.length, Longitud, Texto);
        for (Counter = 0; Counter < MDGROUPList.length; Counter++) {
            var UnMDGROUP = MDGROUPList[Counter];
            UnMDGROUP.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Model.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.Model.Methods.StrtoMDGROUP=function(ProtocoloStr, MDGROUPList)
{
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        MDGROUPList.length = 0;
        if (Persistence.Model.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnMDGROUP = new Persistence.Demo.Properties.TMDGROUP(); //Mode new row
                UnMDGROUP.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.MDGROUP_ListAdd(MDGROUPList, UnMDGROUP);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
    
}

//Socket Function 
Persistence.Model.Methods.MDGROUPListToByte=function(MDGROUPList, MemStream)
{
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, MDGROUPList.length);
    for (i = 0; i < MDGROUPList.length; i++)
    {
        MDGROUPList[i].ToByte(MemStream);
    }
}
Persistence.Model.Methods.ByteToMDGROUPList=function(MDGROUPList, MemStream)
{
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++)
    {
        var UnMDGROUP = new Persistence.Model.Properties.TMDGROUP();
        MDGROUPList[i].ToByte(MemStream);
    }
}
 