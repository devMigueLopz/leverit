﻿//TreduceDB
//Persistence.Model.Properties
//Persistence.Model.Methods
 
Persistence.Model.Properties.TMDHIERPER = function()
{
    this.COMMENTSH = "";
    this.HIERLAVEL = 0;
    this.IDMDGROUP = 0;
    this.IDMDHIERESC = 0;
    this.IDMDHIERPER = 0;
    this.PERCH = 0;
    this.PERMISSIONH = 0;
    this.TOTAL= 0;
    //Socket IO Properties
    this.ToByte= function(MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.COMMENTSH);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.HIERLAVEL);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDGROUP);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDHIERESC);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDHIERPER);
        SysCfg.Stream.Methods.WriteStreamDouble(MemStream, this.PERCH);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.PERMISSIONH);
        SysCfg.Stream.Methods.WriteStreamDouble(MemStream, this.TOTAL);
    }
    this.ByteTo= function(MemStream)
    {
        this.COMMENTSH = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.HIERLAVEL = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDGROUP = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDHIERESC = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDHIERPER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.PERCH = SysCfg.Stream.Methods.ReadStreamDouble(MemStream);
        this.PERMISSIONH = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.TOTAL = SysCfg.Stream.Methods.ReadStreamDouble(MemStream);
    }
    //Str IO Properties
    this.ToStr= function(Longitud, Texto)
    {
        SysCfg.Str.Protocol.WriteStr(this.COMMENTSH, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.HIERLAVEL, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDGROUP, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDHIERESC, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDHIERPER, Longitud, Texto);
        SysCfg.Str.Protocol.WriteDouble(this.PERCH, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.PERMISSIONH, Longitud, Texto);
        SysCfg.Str.Protocol.WriteDouble(this.TOTAL, Longitud, Texto);
    }
    this.StrTo= function( Pos, Index, LongitudArray, Texto)
    {
        this.COMMENTSH = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.HIERLAVEL = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDGROUP = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDHIERESC = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDHIERPER = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.PERCH = SysCfg.Str.Protocol.ReadDouble(Pos, Index, LongitudArray, Texto);
        this.PERMISSIONH = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.TOTAL = SysCfg.Str.Protocol.ReadDouble(Pos, Index, LongitudArray, Texto);
    }
}
   

//**********************   METHODS server  ********************************************************************************************

    
//DataSet Function 
Persistence.Model.Methods.MDHIERPER_Fill = function(MDHIERPERList, StrIDMDHIERPER/*IDMDHIERPER*/)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    MDHIERPERList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDMDHIERPER == "")
    {
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERPER.FieldName, " = " + UsrCfg.InternoAtisNames.MDHIERPER.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERPER.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else
    {
        StrIDMDHIERPER = " IN (" + StrIDMDHIERPER + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERPER.FieldName, StrIDMDHIERPER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //Param.AddInt32(UsrCfg.InternoDataLinkNames.MDHIERPER.IDMDHIERPER.FieldName, IDMDHIERPER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try
    {
        /*

    //********   MDHIERPER_GET   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDHIERPER_GET", "MDHIERPER_GET description.");
    UnSQL.SQL.Add(" SELECT  ");
    UnSQL.SQL.Add("  MDHIERPER.COMMENTSH, ");
    UnSQL.SQL.Add("  MDHIERPER.HIERLAVEL, ");
    UnSQL.SQL.Add("  MDHIERPER.IDMDGROUP, ");
    UnSQL.SQL.Add("  MDHIERPER.IDMDHIERESC, ");
    UnSQL.SQL.Add("  MDHIERPER.IDMDHIERPER, ");
    UnSQL.SQL.Add("  MDHIERPER.PERCH, ");
    UnSQL.SQL.Add("  MDHIERPER.PERMISSIONH, ");
    UnSQL.SQL.Add("  MDHIERPER.TOTAL ");
    UnSQL.SQL.Add(" FROM  MDHIERPER ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  MDHIERPER.COMMENTSH=[COMMENTSH] and  ");
    UnSQL.SQL.Add("  MDHIERPER.HIERLAVEL=[HIERLAVEL] and  ");
    UnSQL.SQL.Add("  MDHIERPER.IDMDGROUP=[IDMDGROUP] and  ");
    UnSQL.SQL.Add("  MDHIERPER.IDMDHIERESC=[IDMDHIERESC] and  ");
    UnSQL.SQL.Add("  MDHIERPER.IDMDHIERPER=[IDMDHIERPER] and  ");
    UnSQL.SQL.Add("  MDHIERPER.PERCH=[PERCH] and  ");
    UnSQL.SQL.Add("  MDHIERPER.PERMISSIONH=[PERMISSIONH] and  ");
    UnSQL.SQL.Add("  MDHIERPER.TOTAL=[TOTAL] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_MDHIERPER = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDHIERPER_GET", Param.ToBytes());
        ResErr = DS_MDHIERPER.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDHIERPER.DataSet.RecordCount > 0)
            {
                DS_MDHIERPER.DataSet.First();
                while (!(DS_MDHIERPER.DataSet.Eof))
                {
                    var MDHIERPER = new Persistence.Model.Properties.TMDHIERPER();
                    MDHIERPER.IDMDHIERPER = DS_MDHIERPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERPER.FieldName).asInt32();
                    //Other
                    MDHIERPER.HIERLAVEL = DS_MDHIERPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.HIERLAVEL.FieldName).asInt32();
                    MDHIERPER.IDMDGROUP = DS_MDHIERPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.IDMDGROUP.FieldName).asInt32();
                    MDHIERPER.IDMDHIERESC = DS_MDHIERPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERESC.FieldName).asInt32();
                    MDHIERPER.PERCH = DS_MDHIERPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.PERCH.FieldName).asDouble();
                    MDHIERPER.PERMISSIONH = DS_MDHIERPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.PERMISSIONH.FieldName).asInt32();
                    MDHIERPER.TOTAL = DS_MDHIERPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.TOTAL.FieldName).asDouble();
                    MDHIERPERList.push(MDHIERPER);

                    DS_MDHIERPER.DataSet.Next();
                }
            }
            else
            {
                DS_MDHIERPER.ResErr.NotError = false;
                DS_MDHIERPER.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}

Persistence.Model.Methods.MDHIERPER_GETID=function(MDHIERPER)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERPER.FieldName, MDHIERPER.IDMDHIERPER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDHIERPER.COMMENTSH.FieldName, MDHIERPER.COMMENTSH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.HIERLAVEL.FieldName, MDHIERPER.HIERLAVEL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.IDMDGROUP.FieldName, MDHIERPER.IDMDGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERESC.FieldName, MDHIERPER.IDMDHIERESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDouble(UsrCfg.InternoAtisNames.MDHIERPER.PERCH.FieldName, MDHIERPER.PERCH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.PERMISSIONH.FieldName, MDHIERPER.PERMISSIONH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDouble(UsrCfg.InternoAtisNames.MDHIERPER.TOTAL.FieldName, MDHIERPER.TOTAL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDHIERPER_GETID   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDHIERPER_GETID", "MDHIERPER_GETID description.");
    UnSQL.SQL.Add(" SELECT  ");
    UnSQL.SQL.Add("  MDHIERPER.COMMENTSH, ");
    UnSQL.SQL.Add("  MDHIERPER.HIERLAVEL, ");
    UnSQL.SQL.Add("  MDHIERPER.IDMDGROUP, ");
    UnSQL.SQL.Add("  MDHIERPER.IDMDHIERESC, ");
    UnSQL.SQL.Add("  MDHIERPER.IDMDHIERPER, ");
    UnSQL.SQL.Add("  MDHIERPER.PERCH, ");
    UnSQL.SQL.Add("  MDHIERPER.PERMISSIONH, ");
    UnSQL.SQL.Add("  MDHIERPER.TOTAL ");
    UnSQL.SQL.Add(" FROM  MDHIERPER ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  MDHIERPER.COMMENTSH=[COMMENTSH] and  ");
    UnSQL.SQL.Add("  MDHIERPER.HIERLAVEL=[HIERLAVEL] and  ");
    UnSQL.SQL.Add("  MDHIERPER.IDMDGROUP=[IDMDGROUP] and  ");
    UnSQL.SQL.Add("  MDHIERPER.IDMDHIERESC=[IDMDHIERESC] and  ");
    UnSQL.SQL.Add("  MDHIERPER.IDMDHIERPER=[IDMDHIERPER] and  ");
    UnSQL.SQL.Add("  MDHIERPER.PERCH=[PERCH] and  ");
    UnSQL.SQL.Add("  MDHIERPER.PERMISSIONH=[PERMISSIONH] and  ");
    UnSQL.SQL.Add("  MDHIERPER.TOTAL=[TOTAL] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_MDHIERPER = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDHIERPER_GETID", Param.ToBytes());
        ResErr = DS_MDHIERPER.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDHIERPER.DataSet.RecordCount > 0)
            {
                DS_MDHIERPER.DataSet.First();
                MDHIERPER.IDMDHIERPER = DS_MDHIERPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERPER.FieldName).asInt32();
                //Other
                MDHIERPER.HIERLAVEL = DS_MDHIERPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.HIERLAVEL.FieldName).asInt32();
                MDHIERPER.IDMDGROUP = DS_MDHIERPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.IDMDGROUP.FieldName).asInt32();
                MDHIERPER.IDMDHIERESC = DS_MDHIERPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERESC.FieldName).asInt32();
                MDHIERPER.PERCH = DS_MDHIERPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.PERCH.FieldName).asDouble();
                MDHIERPER.PERMISSIONH = DS_MDHIERPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.PERMISSIONH.FieldName).asInt32();
                MDHIERPER.TOTAL = DS_MDHIERPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.TOTAL.FieldName).asDouble();
            }
            else
            {
                DS_MDHIERPER.ResErr.NotError = false;
                DS_MDHIERPER.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDHIERPER_ADD=function( MDHIERPER)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERPER.FieldName, MDHIERPER.IDMDHIERPER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDHIERPER.COMMENTSH.FieldName, MDHIERPER.COMMENTSH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.HIERLAVEL.FieldName, MDHIERPER.HIERLAVEL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.IDMDGROUP.FieldName, MDHIERPER.IDMDGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERESC.FieldName, MDHIERPER.IDMDHIERESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDouble(UsrCfg.InternoAtisNames.MDHIERPER.PERCH.FieldName, MDHIERPER.PERCH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.PERMISSIONH.FieldName, MDHIERPER.PERMISSIONH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDouble(UsrCfg.InternoAtisNames.MDHIERPER.TOTAL.FieldName, MDHIERPER.TOTAL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDHIERPER_ADD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDHIERPER_ADD", "MDHIERPER_ADD description.");
    UnSQL.SQL.Add(" INSERT INTO MDHIERPER( ");
    UnSQL.SQL.Add("  COMMENTSH,  ");
    UnSQL.SQL.Add("  HIERLAVEL,  ");
    UnSQL.SQL.Add("  IDMDGROUP,  ");
    UnSQL.SQL.Add("  IDMDHIERESC,  ");
    UnSQL.SQL.Add("  IDMDHIERPER,  ");
    UnSQL.SQL.Add("  PERCH,  ");
    UnSQL.SQL.Add("  PERMISSIONH,  ");
    UnSQL.SQL.Add("  TOTAL ");
    UnSQL.SQL.Add(" )  ");
    UnSQL.SQL.Add(" VALUES ( ");
    UnSQL.SQL.Add("  [COMMENTSH], ");
    UnSQL.SQL.Add("  [HIERLAVEL], ");
    UnSQL.SQL.Add("  [IDMDGROUP], ");
    UnSQL.SQL.Add("  [IDMDHIERESC], ");
    UnSQL.SQL.Add("  [IDMDHIERPER], ");
    UnSQL.SQL.Add("  [PERCH], ");
    UnSQL.SQL.Add("  [PERMISSIONH], ");
    UnSQL.SQL.Add("  [TOTAL] ");
    UnSQL.SQL.Add(" ) ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDHIERPER_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            MDHIERPER_GETID(MDHIERPER);
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDHIERPER_UPD=function( MDHIERPER)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERPER.FieldName, MDHIERPER.IDMDHIERPER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDHIERPER.COMMENTSH.FieldName, MDHIERPER.COMMENTSH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.HIERLAVEL.FieldName, MDHIERPER.HIERLAVEL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.IDMDGROUP.FieldName, MDHIERPER.IDMDGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERESC.FieldName, MDHIERPER.IDMDHIERESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDouble(UsrCfg.InternoAtisNames.MDHIERPER.PERCH.FieldName, MDHIERPER.PERCH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.PERMISSIONH.FieldName, MDHIERPER.PERMISSIONH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDouble(UsrCfg.InternoAtisNames.MDHIERPER.TOTAL.FieldName, MDHIERPER.TOTAL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDHIERPER_UPD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDHIERPER_UPD", "MDHIERPER_UPD description.");
    UnSQL.SQL.Add(" UPDATE MDHIERPER SET ");
    UnSQL.SQL.Add("  COMMENTSH=[COMMENTSH],  ");
    UnSQL.SQL.Add("  HIERLAVEL=[HIERLAVEL],  ");
    UnSQL.SQL.Add("  IDMDGROUP=[IDMDGROUP],  ");
    UnSQL.SQL.Add("  IDMDHIERESC=[IDMDHIERESC],  ");
    UnSQL.SQL.Add("  IDMDHIERPER=[IDMDHIERPER],  ");
    UnSQL.SQL.Add("  PERCH=[PERCH],  ");
    UnSQL.SQL.Add("  PERMISSIONH=[PERMISSIONH],  ");
    UnSQL.SQL.Add("  TOTAL=[TOTAL] ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  COMMENTSH=[COMMENTSH] AND  ");
    UnSQL.SQL.Add("  HIERLAVEL=[HIERLAVEL] AND  ");
    UnSQL.SQL.Add("  IDMDGROUP=[IDMDGROUP] AND  ");
    UnSQL.SQL.Add("  IDMDHIERESC=[IDMDHIERESC] AND  ");
    UnSQL.SQL.Add("  IDMDHIERPER=[IDMDHIERPER] AND  ");
    UnSQL.SQL.Add("  PERCH=[PERCH] AND  ");
    UnSQL.SQL.Add("  PERMISSIONH=[PERMISSIONH] AND  ");
    UnSQL.SQL.Add("  TOTAL=[TOTAL] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDHIERPER_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            //GET(); 
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDHIERPER_DEL=function(/* StrIDMDHIERPER*/ IDMDHIERPER)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        /*if (StrIDMDHIERPER == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERPER.FieldName, " = " + UsrCfg.InternoAtisNames.MDHIERPER.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERPER.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDMDHIERPER = " IN (" + StrIDMDHIERPER + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERPER.FieldName, StrIDMDHIERPER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERPER.FieldName, IDMDHIERPER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDHIERPER_DEL_1   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDHIERPER_DEL_1", "MDHIERPER_DEL_1 description.");
    UnSQL.SQL.Add(" DELETE MDHIERPER  ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  COMMENTSH=[COMMENTSH] AND  ");
    UnSQL.SQL.Add("  HIERLAVEL=[HIERLAVEL] AND  ");
    UnSQL.SQL.Add("  IDMDGROUP=[IDMDGROUP] AND  ");
    UnSQL.SQL.Add("  IDMDHIERESC=[IDMDHIERESC] AND  ");
    UnSQL.SQL.Add("  IDMDHIERPER=[IDMDHIERPER] AND  ");
    UnSQL.SQL.Add("  PERCH=[PERCH] AND  ");
    UnSQL.SQL.Add("  PERMISSIONH=[PERMISSIONH] AND  ");
    UnSQL.SQL.Add("  TOTAL=[TOTAL] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDHIERPER_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDHIERPER_DEL=function(MDHIERPER)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERPER.FieldName, MDHIERPER.IDMDHIERPER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDHIERPER.COMMENTSH.FieldName, MDHIERPER.COMMENTSH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.HIERLAVEL.FieldName, MDHIERPER.HIERLAVEL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.IDMDGROUP.FieldName, MDHIERPER.IDMDGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERESC.FieldName, MDHIERPER.IDMDHIERESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDouble(UsrCfg.InternoAtisNames.MDHIERPER.PERCH.FieldName, MDHIERPER.PERCH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.PERMISSIONH.FieldName, MDHIERPER.PERMISSIONH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDouble(UsrCfg.InternoAtisNames.MDHIERPER.TOTAL.FieldName, MDHIERPER.TOTAL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDHIERPER_DEL_2   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDHIERPER_DEL_2", "MDHIERPER_DEL_2 description.");
    UnSQL.SQL.Add(" DELETE MDHIERPER  ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  COMMENTSH=[COMMENTSH] AND  ");
    UnSQL.SQL.Add("  HIERLAVEL=[HIERLAVEL] AND  ");
    UnSQL.SQL.Add("  IDMDGROUP=[IDMDGROUP] AND  ");
    UnSQL.SQL.Add("  IDMDHIERESC=[IDMDHIERESC] AND  ");
    UnSQL.SQL.Add("  IDMDHIERPER=[IDMDHIERPER] AND  ");
    UnSQL.SQL.Add("  PERCH=[PERCH] AND  ");
    UnSQL.SQL.Add("  PERMISSIONH=[PERMISSIONH] AND  ");
    UnSQL.SQL.Add("  TOTAL=[TOTAL] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDHIERPER_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
    

//**********************   METHODS   ********************************************************************************************

   
//DataSet Function 

//List Function 
Persistence.Model.Methods.MDHIERPER_ListSetID =function(MDHIERPERList, IDMDHIERPER)
{
    for (i = 0; i < MDHIERPERList.length; i++)
    {

        if (IDMDHIERPER == MDHIERPERList[i].IDMDHIERPER)
            return (MDHIERPERList[i]);

    }

    var TMDHIERPER = new Persistence.Properties.TMDHIERPER();
    TMDHIERPER.IDMDHIERPER = IDMDHIERPER; 
    return (TMDHIERPER);   
}

Persistence.Model.Methods.MDHIERPER_ListAdd=function(MDHIERPERList,  MDHIERPER)
{
    var i = Persistence.Model.Methods.MDHIERPER_ListGetIndex(MDHIERPERList, MDHIERPER);
    if (i == -1) MDHIERPERList.push(MDHIERPER);
    else
    {
        MDHIERPERList[i].COMMENTSH = MDHIERPER.COMMENTSH;
        MDHIERPERList[i].HIERLAVEL = MDHIERPER.HIERLAVEL;
        MDHIERPERList[i].IDMDGROUP = MDHIERPER.IDMDGROUP;
        MDHIERPERList[i].IDMDHIERESC = MDHIERPER.IDMDHIERESC;
        MDHIERPERList[i].IDMDHIERPER = MDHIERPER.IDMDHIERPER;
        MDHIERPERList[i].PERCH = MDHIERPER.PERCH;
        MDHIERPERList[i].PERMISSIONH = MDHIERPER.PERMISSIONH;
        MDHIERPERList[i].TOTAL = MDHIERPER.TOTAL;
    }
}
Persistence.Model.Methods.MDHIERPER_ListGetIndex=function(MDHIERPERList, MDHIERPER)
{
    for (i = 0; i < MDHIERPERList.length; i++)
    {
        if (MDHIERPER.IDMDHIERPER == MDHIERPERList[i].IDMDHIERPER)
            return (i);
    }
    return (-1);
}
Persistence.Model.Methods.MDHIERPER_ListGetIndex=function(MDHIERPERList, IDMDHIERPER)
{
    for (i = 0; i < MDHIERPERList.length; i++)
    {
        if (IDMDHIERPER == MDHIERPERList[i].IDMDHIERPER)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.Model.Methods.MDHIERPERListtoStr=function(MDHIERPERList)
{
    var Res = Persistence.Model.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(MDHIERPERList.length, Longitud, Texto);
        for (Counter = 0; Counter < MDHIERPERList.length; Counter++) {
            var UnMDHIERPER = MDHIERPERList[Counter];
            UnMDHIERPER.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Model.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.Model.Methods.StrtoMDHIERPER=function(ProtocoloStr, MDHIERPERList)
{
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        MDHIERPERList.length = 0;
        if (Persistence.Model.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnMDHIERPER = new Persistence.Demo.Properties.TMDHIERPER(); //Mode new row
                UnMDHIERPER.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.MDHIERPER_ListAdd(MDHIERPERList, UnMDHIERPER);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
    
}

//Socket Function 
Persistence.Model.Methods.MDHIERPERListToByte=function(MDHIERPERList, MemStream)
{
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, MDHIERPERList.length);
    for (i = 0; i < MDHIERPERList.length; i++)
    {
        MDHIERPERList[i].ToByte(MemStream);
    }
}
Persistence.Model.Methods.ByteToMDHIERPERList=function(MDHIERPERList,  MemStream)
{
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++)
    {
        var UnMDHIERPER = new Persistence.Model.Properties.TMDHIERPER();
        MDHIERPERList[i].ToByte(MemStream);
    }
}
 