﻿//TreduceDB
//Persistence.Model.Properties
//Persistence.Model.Methods

 
Persistence.Model.Properties.TMDLIFESTATUSPERMISSION = function()
{
    this.IDMDLIFESTATUSPERMISSION = 0;
    this.LIFESTATUSPERMISSION_NAME = "";
    //Socket IO Properties
    this.ToByte= function(MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDLIFESTATUSPERMISSION);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.LIFESTATUSPERMISSION_NAME);
    }
    this.ByteTo= function( MemStream)
    {
        this.IDMDLIFESTATUSPERMISSION = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.LIFESTATUSPERMISSION_NAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    }
    //Str IO Properties
    this.ToStr= function(Longitud, Texto)
    {
        SysCfg.Str.Protocol.WriteInt(this.IDMDLIFESTATUSPERMISSION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.LIFESTATUSPERMISSION_NAME, Longitud, Texto);
    }
    this.StrTo= function(Pos, Index, LongitudArray, Texto)
    {
        this.IDMDLIFESTATUSPERMISSION = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.LIFESTATUSPERMISSION_NAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
    }
}
     

//**********************   METHODS server  ********************************************************************************************

     
//DataSet Function 
Persistence.Model.Methods.MDLIFESTATUSPERMISSION_Fill = function(MDLIFESTATUSPERMISSIONList, StrIDMDLIFESTATUSPERMISSION/* IDMDLIFESTATUSPERMISSION*/)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    MDLIFESTATUSPERMISSIONList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDMDLIFESTATUSPERMISSION == "")
    {
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDLIFESTATUSPERMISSION.IDMDLIFESTATUSPERMISSION.FieldName, " = " + UsrCfg.InternoAtisNames.MDLIFESTATUSPERMISSION.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDLIFESTATUSPERMISSION.IDMDLIFESTATUSPERMISSION.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else
    {
        StrIDMDLIFESTATUSPERMISSION = " IN (" + StrIDMDLIFESTATUSPERMISSION + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDLIFESTATUSPERMISSION.IDMDLIFESTATUSPERMISSION.FieldName, StrIDMDLIFESTATUSPERMISSION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //Param.AddInt32(UsrCfg.InternoDataLinkNames.MDLIFESTATUSPERMISSION.IDMDLIFESTATUSPERMISSION.FieldName, IDMDLIFESTATUSPERMISSION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try
    {
        /*

    //********   MDLIFESTATUSPERMISSION_GET   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDLIFESTATUSPERMISSION_GET", "MDLIFESTATUSPERMISSION_GET description.");
    UnSQL.SQL.Add(" SELECT  ");
    UnSQL.SQL.Add("  MDLIFESTATUSPERMISSION.IDMDLIFESTATUSPERMISSION, ");
    UnSQL.SQL.Add("  MDLIFESTATUSPERMISSION.LIFESTATUSPERMISSION_NAME ");
    UnSQL.SQL.Add(" FROM  MDLIFESTATUSPERMISSION ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  MDLIFESTATUSPERMISSION.IDMDLIFESTATUSPERMISSION=[IDMDLIFESTATUSPERMISSION] and  ");
    UnSQL.SQL.Add("  MDLIFESTATUSPERMISSION.LIFESTATUSPERMISSION_NAME=[LIFESTATUSPERMISSION_NAME] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_MDLIFESTATUSPERMISSION = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDLIFESTATUSPERMISSION_GET", Param.ToBytes());
        ResErr = DS_MDLIFESTATUSPERMISSION.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDLIFESTATUSPERMISSION.DataSet.RecordCount > 0)
            {
                DS_MDLIFESTATUSPERMISSION.DataSet.First();
                while (!(DS_MDLIFESTATUSPERMISSION.DataSet.Eof))
                {
                    var MDLIFESTATUSPERMISSION = new Persistence.Model.Properties.TMDLIFESTATUSPERMISSION();
                    MDLIFESTATUSPERMISSION.IDMDLIFESTATUSPERMISSION = DS_MDLIFESTATUSPERMISSION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDLIFESTATUSPERMISSION.IDMDLIFESTATUSPERMISSION.FieldName).asInt32();
                    //Other
                    MDLIFESTATUSPERMISSION.LIFESTATUSPERMISSION_NAME = DS_MDLIFESTATUSPERMISSION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDLIFESTATUSPERMISSION.LIFESTATUSPERMISSION_NAME.FieldName).asString();
                    MDLIFESTATUSPERMISSIONList.push(MDLIFESTATUSPERMISSION);

                    DS_MDLIFESTATUSPERMISSION.DataSet.Next();
                }
            }
            else
            {
                DS_MDLIFESTATUSPERMISSION.ResErr.NotError = false;
                DS_MDLIFESTATUSPERMISSION.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDLIFESTATUSPERMISSION_GETID=function( MDLIFESTATUSPERMISSION)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDLIFESTATUSPERMISSION.IDMDLIFESTATUSPERMISSION.FieldName, MDLIFESTATUSPERMISSION.IDMDLIFESTATUSPERMISSION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.MDLIFESTATUSPERMISSION.LIFESTATUSPERMISSION_NAME.FieldName, MDLIFESTATUSPERMISSION.LIFESTATUSPERMISSION_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDLIFESTATUSPERMISSION_GETID   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDLIFESTATUSPERMISSION_GETID", "MDLIFESTATUSPERMISSION_GETID description.");
    UnSQL.SQL.Add(" SELECT  ");
    UnSQL.SQL.Add("  MDLIFESTATUSPERMISSION.IDMDLIFESTATUSPERMISSION, ");
    UnSQL.SQL.Add("  MDLIFESTATUSPERMISSION.LIFESTATUSPERMISSION_NAME ");
    UnSQL.SQL.Add(" FROM  MDLIFESTATUSPERMISSION ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  MDLIFESTATUSPERMISSION.IDMDLIFESTATUSPERMISSION=[IDMDLIFESTATUSPERMISSION] and  ");
    UnSQL.SQL.Add("  MDLIFESTATUSPERMISSION.LIFESTATUSPERMISSION_NAME=[LIFESTATUSPERMISSION_NAME] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_MDLIFESTATUSPERMISSION = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDLIFESTATUSPERMISSION_GETID", Param.ToBytes());
        ResErr = DS_MDLIFESTATUSPERMISSION.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDLIFESTATUSPERMISSION.DataSet.RecordCount > 0)
            {
                DS_MDLIFESTATUSPERMISSION.DataSet.First();
                MDLIFESTATUSPERMISSION.IDMDLIFESTATUSPERMISSION = DS_MDLIFESTATUSPERMISSION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDLIFESTATUSPERMISSION.IDMDLIFESTATUSPERMISSION.FieldName).asInt32();
                //Other
                MDLIFESTATUSPERMISSION.LIFESTATUSPERMISSION_NAME = DS_MDLIFESTATUSPERMISSION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDLIFESTATUSPERMISSION.LIFESTATUSPERMISSION_NAME.FieldName).asString();
            }
            else
            {
                DS_MDLIFESTATUSPERMISSION.ResErr.NotError = false;
                DS_MDLIFESTATUSPERMISSION.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDLIFESTATUSPERMISSION_ADD=function(MDLIFESTATUSPERMISSION)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDLIFESTATUSPERMISSION.IDMDLIFESTATUSPERMISSION.FieldName, MDLIFESTATUSPERMISSION.IDMDLIFESTATUSPERMISSION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.MDLIFESTATUSPERMISSION.LIFESTATUSPERMISSION_NAME.FieldName, MDLIFESTATUSPERMISSION.LIFESTATUSPERMISSION_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDLIFESTATUSPERMISSION_ADD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDLIFESTATUSPERMISSION_ADD", "MDLIFESTATUSPERMISSION_ADD description.");
    UnSQL.SQL.Add(" INSERT INTO MDLIFESTATUSPERMISSION( ");
    UnSQL.SQL.Add("  IDMDLIFESTATUSPERMISSION,  ");
    UnSQL.SQL.Add("  LIFESTATUSPERMISSION_NAME ");
    UnSQL.SQL.Add(" )  ");
    UnSQL.SQL.Add(" VALUES ( ");
    UnSQL.SQL.Add("  [IDMDLIFESTATUSPERMISSION], ");
    UnSQL.SQL.Add("  [LIFESTATUSPERMISSION_NAME] ");
    UnSQL.SQL.Add(" ) ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDLIFESTATUSPERMISSION_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            MDLIFESTATUSPERMISSION_GETID(MDLIFESTATUSPERMISSION);
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDLIFESTATUSPERMISSION_UPD=function(MDLIFESTATUSPERMISSION)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDLIFESTATUSPERMISSION.IDMDLIFESTATUSPERMISSION.FieldName, MDLIFESTATUSPERMISSION.IDMDLIFESTATUSPERMISSION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.MDLIFESTATUSPERMISSION.LIFESTATUSPERMISSION_NAME.FieldName, MDLIFESTATUSPERMISSION.LIFESTATUSPERMISSION_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDLIFESTATUSPERMISSION_UPD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDLIFESTATUSPERMISSION_UPD", "MDLIFESTATUSPERMISSION_UPD description.");
    UnSQL.SQL.Add(" UPDATE MDLIFESTATUSPERMISSION SET ");
    UnSQL.SQL.Add("  IDMDLIFESTATUSPERMISSION=[IDMDLIFESTATUSPERMISSION],  ");
    UnSQL.SQL.Add("  LIFESTATUSPERMISSION_NAME=[LIFESTATUSPERMISSION_NAME] ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  IDMDLIFESTATUSPERMISSION=[IDMDLIFESTATUSPERMISSION] AND  ");
    UnSQL.SQL.Add("  LIFESTATUSPERMISSION_NAME=[LIFESTATUSPERMISSION_NAME] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDLIFESTATUSPERMISSION_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            //GET(); 
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDLIFESTATUSPERMISSION_DEL=function(/* StrIDMDLIFESTATUSPERMISSION*/ IDMDLIFESTATUSPERMISSION)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        /*if (StrIDMDLIFESTATUSPERMISSION == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDLIFESTATUSPERMISSION.IDMDLIFESTATUSPERMISSION.FieldName, " = " + UsrCfg.InternoAtisNames.MDLIFESTATUSPERMISSION.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDLIFESTATUSPERMISSION.IDMDLIFESTATUSPERMISSION.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDMDLIFESTATUSPERMISSION = " IN (" + StrIDMDLIFESTATUSPERMISSION + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDLIFESTATUSPERMISSION.IDMDLIFESTATUSPERMISSION.FieldName, StrIDMDLIFESTATUSPERMISSION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.MDLIFESTATUSPERMISSION.IDMDLIFESTATUSPERMISSION.FieldName, IDMDLIFESTATUSPERMISSION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDLIFESTATUSPERMISSION_DEL_1   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDLIFESTATUSPERMISSION_DEL_1", "MDLIFESTATUSPERMISSION_DEL_1 description.");
    UnSQL.SQL.Add(" DELETE MDLIFESTATUSPERMISSION  ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  IDMDLIFESTATUSPERMISSION=[IDMDLIFESTATUSPERMISSION] AND  ");
    UnSQL.SQL.Add("  LIFESTATUSPERMISSION_NAME=[LIFESTATUSPERMISSION_NAME] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDLIFESTATUSPERMISSION_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDLIFESTATUSPERMISSION_DEL=function(MDLIFESTATUSPERMISSION)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDLIFESTATUSPERMISSION.IDMDLIFESTATUSPERMISSION.FieldName, MDLIFESTATUSPERMISSION.IDMDLIFESTATUSPERMISSION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.MDLIFESTATUSPERMISSION.LIFESTATUSPERMISSION_NAME.FieldName, MDLIFESTATUSPERMISSION.LIFESTATUSPERMISSION_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDLIFESTATUSPERMISSION_DEL_2   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDLIFESTATUSPERMISSION_DEL_2", "MDLIFESTATUSPERMISSION_DEL_2 description.");
    UnSQL.SQL.Add(" DELETE MDLIFESTATUSPERMISSION  ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  IDMDLIFESTATUSPERMISSION=[IDMDLIFESTATUSPERMISSION] AND  ");
    UnSQL.SQL.Add("  LIFESTATUSPERMISSION_NAME=[LIFESTATUSPERMISSION_NAME] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDLIFESTATUSPERMISSION_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
     

//**********************   METHODS   ********************************************************************************************

    
//DataSet Function 

//List Function 
Persistence.Model.Methods.MDLIFESTATUSPERMISSION_ListSetID=function(MDLIFESTATUSPERMISSIONList, IDMDLIFESTATUSPERMISSION)
{
    for (i = 0; i < MDLIFESTATUSPERMISSIONList.length; i++)
    {

        if (IDMDLIFESTATUSPERMISSION == MDLIFESTATUSPERMISSIONList[i].IDMDLIFESTATUSPERMISSION)
            return (MDLIFESTATUSPERMISSIONList[i]);

    }

    var TMDLIFESTATUSPERMISSION = new Persistence.Properties.TMDLIFESTATUSPERMISSION();
    TMDLIFESTATUSPERMISSION.IDMDLIFESTATUSPERMISSION = IDMDLIFESTATUSPERMISSION; 
    return (TMDLIFESTATUSPERMISSION); 
 
}
Persistence.Model.Methods.MDLIFESTATUSPERMISSION_ListAdd=function(MDLIFESTATUSPERMISSIONList, MDLIFESTATUSPERMISSION)
{
    var i = Persistence.Model.Methods.MDLIFESTATUSPERMISSION_ListGetIndex(MDLIFESTATUSPERMISSIONList, MDLIFESTATUSPERMISSION);
    if (i == -1) MDLIFESTATUSPERMISSIONList.push(MDLIFESTATUSPERMISSION);
    else
    {
        MDLIFESTATUSPERMISSIONList[i].IDMDLIFESTATUSPERMISSION = MDLIFESTATUSPERMISSION.IDMDLIFESTATUSPERMISSION;
        MDLIFESTATUSPERMISSIONList[i].LIFESTATUSPERMISSION_NAME = MDLIFESTATUSPERMISSION.LIFESTATUSPERMISSION_NAME;
    }
}
Persistence.Model.Methods.MDLIFESTATUSPERMISSION_ListGetIndex=function(MDLIFESTATUSPERMISSIONList, MDLIFESTATUSPERMISSION)
{
    for (i = 0; i < MDLIFESTATUSPERMISSIONList.length; i++)
    {
        if (MDLIFESTATUSPERMISSION.IDMDLIFESTATUSPERMISSION == MDLIFESTATUSPERMISSIONList[i].IDMDLIFESTATUSPERMISSION)
            return (i);
    }
    return (-1);
}
Persistence.Model.Methods.MDLIFESTATUSPERMISSION_ListGetIndex=function(MDLIFESTATUSPERMISSIONList, IDMDLIFESTATUSPERMISSION)
{
    for (i = 0; i < MDLIFESTATUSPERMISSIONList.length; i++)
    {
        if (IDMDLIFESTATUSPERMISSION == MDLIFESTATUSPERMISSIONList[i].IDMDLIFESTATUSPERMISSION)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.Model.Methods.MDLIFESTATUSPERMISSIONListtoStr=function(MDLIFESTATUSPERMISSIONList)
{
    var Res = Persistence.Model.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(MDLIFESTATUSPERMISSIONList.length, Longitud, Texto);
        for (Counter = 0; Counter < MDLIFESTATUSPERMISSIONList.length; Counter++) {
            var UnMDLIFESTATUSPERMISSION = MDLIFESTATUSPERMISSIONList[Counter];
            UnMDLIFESTATUSPERMISSION.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Model.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.Model.Methods.StrtoMDLIFESTATUSPERMISSION=function( ProtocoloStr, MDLIFESTATUSPERMISSIONList)
{
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        MDLIFESTATUSPERMISSIONList.length = 0;
        if (Persistence.Model.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnMDLIFESTATUSPERMISSION = new Persistence.Demo.Properties.TMDLIFESTATUSPERMISSION(); //Mode new row
                UnMDLIFESTATUSPERMISSION.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.MDLIFESTATUSPERMISSION_ListAdd(MDLIFESTATUSPERMISSIONList, UnMDLIFESTATUSPERMISSION);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
    
}

//Socket Function 
Persistence.Model.Methods.MDLIFESTATUSPERMISSIONListToByte=function(MDLIFESTATUSPERMISSIONList, MemStream)
{
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, MDLIFESTATUSPERMISSIONList.length);
    for (i = 0; i < MDLIFESTATUSPERMISSIONList.length; i++)
    {
        MDLIFESTATUSPERMISSIONList[i].ToByte(MemStream);
    }
}
Persistence.Model.Methods.ByteToMDLIFESTATUSPERMISSIONList=function(MDLIFESTATUSPERMISSIONList, MemStream)
{
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++)
    {
        var UnMDLIFESTATUSPERMISSION = new Persistence.Model.Properties.TMDLIFESTATUSPERMISSION();
        MDLIFESTATUSPERMISSIONList[i].ToByte(MemStream);
    }
}
 