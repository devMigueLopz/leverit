﻿//TreduceDB
//Persistence.Model.Properties
//Persistence.Model.Methods

 
Persistence.Model.Properties.TMDSERVICEEXTRAFIELDS = function()
{
    this.DBDATATYPES_SIZE = 0;
    this.DBKEYTYPE_NAME = "";
    this.EXTRAFIELDS_DESCRIPTION = "";
    this.EXTRAFIELDS_LOOKUPDISPLAY = "";
    this.EXTRAFIELDS_COLUMNSTYLE = 0;
    this.EXTRAFIELDS_LOOKUPID = "";
    this.EXTRAFIELDS_LOOKUPSQL = "";
    this.EXTRAFIELDS_NAME = "";
    this.EXTRAFIELDS_ORDER = 0;
    this.IDCMDBDBDATATYPES = 0;
    this.IDCMDBKEYTYPE = 0;
    this.IDMDSERVICEEXTRAFIELDS = 0;
    this.IDMDSERVICEEXTRATABLE = 0;
    this.IDMDSERVICETYPE = 0;
    //Socket IO Properties
    this.ToByte = function(MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.DBDATATYPES_SIZE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream,this.DBKEYTYPE_NAME);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream,this.EXTRAFIELDS_DESCRIPTION);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream,this.EXTRAFIELDS_LOOKUPDISPLAY);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.EXTRAFIELDS_COLUMNSTYLE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.EXTRAFIELDS_LOOKUPID);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.EXTRAFIELDS_LOOKUPSQL);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.EXTRAFIELDS_NAME);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.EXTRAFIELDS_ORDER);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDCMDBDBDATATYPES);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDCMDBKEYTYPE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDSERVICEEXTRAFIELDS);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDSERVICEEXTRATABLE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDSERVICETYPE);
    }
    this.ByteTo = function(MemStream)
    {
        this.DBDATATYPES_SIZE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.DBKEYTYPE_NAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.EXTRAFIELDS_DESCRIPTION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.EXTRAFIELDS_LOOKUPDISPLAY = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.EXTRAFIELDS_COLUMNSTYLE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.EXTRAFIELDS_LOOKUPID = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.EXTRAFIELDS_LOOKUPSQL = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.EXTRAFIELDS_NAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.EXTRAFIELDS_ORDER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDCMDBDBDATATYPES = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDCMDBKEYTYPE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDSERVICEEXTRAFIELDS = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDSERVICEEXTRATABLE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDSERVICETYPE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    }
    //Str IO Properties
    pthis.ToStr = function(Longitud, Texto)
    {
        SysCfg.Str.Protocol.WriteInt(this.DBDATATYPES_SIZE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.DBKEYTYPE_NAME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.EXTRAFIELDS_DESCRIPTION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.EXTRAFIELDS_LOOKUPDISPLAY, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.EXTRAFIELDS_COLUMNSTYLE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.EXTRAFIELDS_LOOKUPID, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.EXTRAFIELDS_LOOKUPSQL, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.EXTRAFIELDS_NAME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.EXTRAFIELDS_ORDER, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDCMDBDBDATATYPES, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDCMDBKEYTYPE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDSERVICEEXTRAFIELDS, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDSERVICEEXTRATABLE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDSERVICETYPE, Longitud, Texto);
    }
    this.StrTo = function(Pos, Index, LongitudArray, Texto)
    {
        this.DBDATATYPES_SIZE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.DBKEYTYPE_NAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.EXTRAFIELDS_DESCRIPTION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.EXTRAFIELDS_LOOKUPDISPLAY = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.EXTRAFIELDS_COLUMNSTYLE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.EXTRAFIELDS_LOOKUPID = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.EXTRAFIELDS_LOOKUPSQL = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.EXTRAFIELDS_NAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.EXTRAFIELDS_ORDER = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDCMDBDBDATATYPES = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDCMDBKEYTYPE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDSERVICEEXTRAFIELDS = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDSERVICEEXTRATABLE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDSERVICETYPE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
    }
}
    

//**********************   METHODS server  ********************************************************************************************

     
//DataSet Function 
Persistence.Model.Methods.MDSERVICEEXTRAFIELDS_Fill=function(MDSERVICEEXTRAFIELDSList, StrIDMDSERVICEEXTRAFIELDS/* IDMDSERVICEEXTRAFIELDS*/)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    MDSERVICEEXTRAFIELDSList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDMDSERVICEEXTRAFIELDS == "")
    {
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS.FieldName, " = " + UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else
    {
        StrIDMDSERVICEEXTRAFIELDS = " IN (" + StrIDMDSERVICEEXTRAFIELDS + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS.FieldName, StrIDMDSERVICEEXTRAFIELDS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //Param.AddInt32(UsrCfg.InternoDataLinkNames.MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS.FieldName, IDMDSERVICEEXTRAFIELDS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try
    {
        /*

    //********   MDSERVICEEXTRAFIELDS_GET   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDSERVICEEXTRAFIELDS_GET", "MDSERVICEEXTRAFIELDS_GET description.");
    UnSQL.SQL.Add(" SELECT  ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.DBDATATYPES_SIZE, ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.DBKEYTYPE_NAME, ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.EXTRAFIELDS_DESCRIPTION, ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPDISPLAY, ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE, ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPID, ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPSQL, ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME, ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.EXTRAFIELDS_ORDER, ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES, ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.IDCMDBKEYTYPE, ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS, ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRATABLE, ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.IDMDSERVICETYPE ");
    UnSQL.SQL.Add(" FROM  MDSERVICEEXTRAFIELDS ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.DBDATATYPES_SIZE=[DBDATATYPES_SIZE] and  ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.DBKEYTYPE_NAME=[DBKEYTYPE_NAME] and  ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.EXTRAFIELDS_DESCRIPTION=[EXTRAFIELDS_DESCRIPTION] and  ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPDISPLAY=[EXTRAFIELDS_LOOKUPDISPLAY] and  ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE=[EXTRAFIELDS_COLUMNSTYLE] and  ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPID=[EXTRAFIELDS_LOOKUPID] and  ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPSQL=[EXTRAFIELDS_LOOKUPSQL] and  ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME=[EXTRAFIELDS_NAME] and  ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.EXTRAFIELDS_ORDER=[EXTRAFIELDS_ORDER] and  ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES=[IDCMDBDBDATATYPES] and  ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.IDCMDBKEYTYPE=[IDCMDBKEYTYPE] and  ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS=[IDMDSERVICEEXTRAFIELDS] and  ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRATABLE=[IDMDSERVICEEXTRATABLE] and  ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.IDMDSERVICETYPE=[IDMDSERVICETYPE] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_MDSERVICEEXTRAFIELDS = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDSERVICEEXTRAFIELDS_GET", Param.ToBytes());
        ResErr = DS_MDSERVICEEXTRAFIELDS.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDSERVICEEXTRAFIELDS.DataSet.RecordCount > 0)
            {
                DS_MDSERVICEEXTRAFIELDS.DataSet.First();
                while (!(DS_MDSERVICEEXTRAFIELDS.DataSet.Eof))
                {
                    var MDSERVICEEXTRAFIELDS = new Persistence.Model.Properties.TMDSERVICEEXTRAFIELDS();
                    MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS = DS_MDSERVICEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS.FieldName).asInt32();
                    //Other
                    MDSERVICEEXTRAFIELDS.DBDATATYPES_SIZE = DS_MDSERVICEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.DBDATATYPES_SIZE.FieldName).asInt32();
                    MDSERVICEEXTRAFIELDS.DBKEYTYPE_NAME = DS_MDSERVICEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.DBKEYTYPE_NAME.FieldName).asString();
                    MDSERVICEEXTRAFIELDS.EXTRAFIELDS_DESCRIPTION = DS_MDSERVICEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_DESCRIPTION.FieldName).asString();
                    MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPDISPLAY = DS_MDSERVICEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPDISPLAY.FieldName).asString();
                    MDSERVICEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE = DS_MDSERVICEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE.FieldName).asInt32();
                    MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPID = DS_MDSERVICEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPID.FieldName).asString();
                    MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME = DS_MDSERVICEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME.FieldName).asString();
                    MDSERVICEEXTRAFIELDS.EXTRAFIELDS_ORDER = DS_MDSERVICEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_ORDER.FieldName).asInt32();
                    MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES = DS_MDSERVICEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES.FieldName).asInt32();
                    MDSERVICEEXTRAFIELDS.IDCMDBKEYTYPE = DS_MDSERVICEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDCMDBKEYTYPE.FieldName).asInt32();
                    MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRATABLE = DS_MDSERVICEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRATABLE.FieldName).asInt32();
                    MDSERVICEEXTRAFIELDS.IDMDSERVICETYPE = DS_MDSERVICEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICETYPE.FieldName).asInt32();
                    MDSERVICEEXTRAFIELDSList.push(MDSERVICEEXTRAFIELDS);

                    DS_MDSERVICEEXTRAFIELDS.DataSet.Next();
                }
            }
            else
            {
                DS_MDSERVICEEXTRAFIELDS.ResErr.NotError = false;
                DS_MDSERVICEEXTRAFIELDS.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDSERVICEEXTRAFIELDS_GETID=function( MDSERVICEEXTRAFIELDS)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS.FieldName, MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.DBDATATYPES_SIZE.FieldName, MDSERVICEEXTRAFIELDS.DBDATATYPES_SIZE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.DBKEYTYPE_NAME.FieldName, MDSERVICEEXTRAFIELDS.DBKEYTYPE_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_DESCRIPTION.FieldName, MDSERVICEEXTRAFIELDS.EXTRAFIELDS_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPDISPLAY.FieldName, MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPDISPLAY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE.FieldName, MDSERVICEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPID.FieldName, MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPID, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPSQL.FieldName, MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPSQL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME.FieldName, MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_ORDER.FieldName, MDSERVICEEXTRAFIELDS.EXTRAFIELDS_ORDER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES.FieldName, MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDCMDBKEYTYPE.FieldName, MDSERVICEEXTRAFIELDS.IDCMDBKEYTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRATABLE.FieldName, MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRATABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICETYPE.FieldName, MDSERVICEEXTRAFIELDS.IDMDSERVICETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDSERVICEEXTRAFIELDS_GETID   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDSERVICEEXTRAFIELDS_GETID", "MDSERVICEEXTRAFIELDS_GETID description.");
    UnSQL.SQL.Add(" SELECT  ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.DBDATATYPES_SIZE, ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.DBKEYTYPE_NAME, ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.EXTRAFIELDS_DESCRIPTION, ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPDISPLAY, ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE, ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPID, ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPSQL, ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME, ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.EXTRAFIELDS_ORDER, ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES, ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.IDCMDBKEYTYPE, ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS, ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRATABLE, ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.IDMDSERVICETYPE ");
    UnSQL.SQL.Add(" FROM  MDSERVICEEXTRAFIELDS ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.DBDATATYPES_SIZE=[DBDATATYPES_SIZE] and  ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.DBKEYTYPE_NAME=[DBKEYTYPE_NAME] and  ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.EXTRAFIELDS_DESCRIPTION=[EXTRAFIELDS_DESCRIPTION] and  ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPDISPLAY=[EXTRAFIELDS_LOOKUPDISPLAY] and  ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE=[EXTRAFIELDS_COLUMNSTYLE] and  ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPID=[EXTRAFIELDS_LOOKUPID] and  ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPSQL=[EXTRAFIELDS_LOOKUPSQL] and  ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME=[EXTRAFIELDS_NAME] and  ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.EXTRAFIELDS_ORDER=[EXTRAFIELDS_ORDER] and  ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES=[IDCMDBDBDATATYPES] and  ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.IDCMDBKEYTYPE=[IDCMDBKEYTYPE] and  ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS=[IDMDSERVICEEXTRAFIELDS] and  ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRATABLE=[IDMDSERVICEEXTRATABLE] and  ");
    UnSQL.SQL.Add("  MDSERVICEEXTRAFIELDS.IDMDSERVICETYPE=[IDMDSERVICETYPE] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_MDSERVICEEXTRAFIELDS = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDSERVICEEXTRAFIELDS_GETID", Param.ToBytes());
        ResErr = DS_MDSERVICEEXTRAFIELDS.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDSERVICEEXTRAFIELDS.DataSet.RecordCount > 0)
            {
                DS_MDSERVICEEXTRAFIELDS.DataSet.First();
                MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS = DS_MDSERVICEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS.FieldName).asInt32();
                //Other
                MDSERVICEEXTRAFIELDS.DBDATATYPES_SIZE = DS_MDSERVICEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.DBDATATYPES_SIZE.FieldName).asInt32();
                MDSERVICEEXTRAFIELDS.DBKEYTYPE_NAME = DS_MDSERVICEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.DBKEYTYPE_NAME.FieldName).asString();
                MDSERVICEEXTRAFIELDS.EXTRAFIELDS_DESCRIPTION = DS_MDSERVICEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_DESCRIPTION.FieldName).asString();
                MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPDISPLAY = DS_MDSERVICEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPDISPLAY.FieldName).asString();
                MDSERVICEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE = DS_MDSERVICEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE.FieldName).asInt32();
                MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPID = DS_MDSERVICEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPID.FieldName).asString();
                MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME = DS_MDSERVICEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME.FieldName).asString();
                MDSERVICEEXTRAFIELDS.EXTRAFIELDS_ORDER = DS_MDSERVICEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_ORDER.FieldName).asInt32();
                MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES = DS_MDSERVICEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES.FieldName).asInt32();
                MDSERVICEEXTRAFIELDS.IDCMDBKEYTYPE = DS_MDSERVICEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDCMDBKEYTYPE.FieldName).asInt32();
                MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRATABLE = DS_MDSERVICEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRATABLE.FieldName).asInt32();
                MDSERVICEEXTRAFIELDS.IDMDSERVICETYPE = DS_MDSERVICEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICETYPE.FieldName).asInt32();
            }
            else
            {
                DS_MDSERVICEEXTRAFIELDS.ResErr.NotError = false;
                DS_MDSERVICEEXTRAFIELDS.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDSERVICEEXTRAFIELDS_ADD=function(MDSERVICEEXTRAFIELDS)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS.FieldName, MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.DBDATATYPES_SIZE.FieldName, MDSERVICEEXTRAFIELDS.DBDATATYPES_SIZE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.DBKEYTYPE_NAME.FieldName, MDSERVICEEXTRAFIELDS.DBKEYTYPE_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_DESCRIPTION.FieldName, MDSERVICEEXTRAFIELDS.EXTRAFIELDS_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPDISPLAY.FieldName, MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPDISPLAY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE.FieldName, MDSERVICEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPID.FieldName, MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPID, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPSQL.FieldName, MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPSQL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME.FieldName, MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_ORDER.FieldName, MDSERVICEEXTRAFIELDS.EXTRAFIELDS_ORDER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES.FieldName, MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDCMDBKEYTYPE.FieldName, MDSERVICEEXTRAFIELDS.IDCMDBKEYTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRATABLE.FieldName, MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRATABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICETYPE.FieldName, MDSERVICEEXTRAFIELDS.IDMDSERVICETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDSERVICEEXTRAFIELDS_ADD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDSERVICEEXTRAFIELDS_ADD", "MDSERVICEEXTRAFIELDS_ADD description.");
    UnSQL.SQL.Add(" INSERT INTO MDSERVICEEXTRAFIELDS( ");
    UnSQL.SQL.Add("  DBDATATYPES_SIZE,  ");
    UnSQL.SQL.Add("  DBKEYTYPE_NAME,  ");
    UnSQL.SQL.Add("  EXTRAFIELDS_DESCRIPTION,  ");
    UnSQL.SQL.Add("  EXTRAFIELDS_LOOKUPDISPLAY,  ");
    UnSQL.SQL.Add("  EXTRAFIELDS_COLUMNSTYLE,  ");
    UnSQL.SQL.Add("  EXTRAFIELDS_LOOKUPID,  ");
    UnSQL.SQL.Add("  EXTRAFIELDS_LOOKUPSQL,  ");
    UnSQL.SQL.Add("  EXTRAFIELDS_NAME,  ");
    UnSQL.SQL.Add("  EXTRAFIELDS_ORDER,  ");
    UnSQL.SQL.Add("  IDCMDBDBDATATYPES,  ");
    UnSQL.SQL.Add("  IDCMDBKEYTYPE,  ");
    UnSQL.SQL.Add("  IDMDSERVICEEXTRAFIELDS,  ");
    UnSQL.SQL.Add("  IDMDSERVICEEXTRATABLE,  ");
    UnSQL.SQL.Add("  IDMDSERVICETYPE ");
    UnSQL.SQL.Add(" )  ");
    UnSQL.SQL.Add(" VALUES ( ");
    UnSQL.SQL.Add("  [DBDATATYPES_SIZE], ");
    UnSQL.SQL.Add("  [DBKEYTYPE_NAME], ");
    UnSQL.SQL.Add("  [EXTRAFIELDS_DESCRIPTION], ");
    UnSQL.SQL.Add("  [EXTRAFIELDS_LOOKUPDISPLAY], ");
    UnSQL.SQL.Add("  [EXTRAFIELDS_COLUMNSTYLE], ");
    UnSQL.SQL.Add("  [EXTRAFIELDS_LOOKUPID], ");
    UnSQL.SQL.Add("  [EXTRAFIELDS_LOOKUPSQL], ");
    UnSQL.SQL.Add("  [EXTRAFIELDS_NAME], ");
    UnSQL.SQL.Add("  [EXTRAFIELDS_ORDER], ");
    UnSQL.SQL.Add("  [IDCMDBDBDATATYPES], ");
    UnSQL.SQL.Add("  [IDCMDBKEYTYPE], ");
    UnSQL.SQL.Add("  [IDMDSERVICEEXTRAFIELDS], ");
    UnSQL.SQL.Add("  [IDMDSERVICEEXTRATABLE], ");
    UnSQL.SQL.Add("  [IDMDSERVICETYPE] ");
    UnSQL.SQL.Add(" ) ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDSERVICEEXTRAFIELDS_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            MDSERVICEEXTRAFIELDS_GETID(MDSERVICEEXTRAFIELDS);
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDSERVICEEXTRAFIELDS_UPD=function(MDSERVICEEXTRAFIELDS)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS.FieldName, MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.DBDATATYPES_SIZE.FieldName, MDSERVICEEXTRAFIELDS.DBDATATYPES_SIZE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.DBKEYTYPE_NAME.FieldName, MDSERVICEEXTRAFIELDS.DBKEYTYPE_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_DESCRIPTION.FieldName, MDSERVICEEXTRAFIELDS.EXTRAFIELDS_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPDISPLAY.FieldName, MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPDISPLAY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE.FieldName, MDSERVICEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPID.FieldName, MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPID, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPSQL.FieldName, MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPSQL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME.FieldName, MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_ORDER.FieldName, MDSERVICEEXTRAFIELDS.EXTRAFIELDS_ORDER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES.FieldName, MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDCMDBKEYTYPE.FieldName, MDSERVICEEXTRAFIELDS.IDCMDBKEYTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRATABLE.FieldName, MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRATABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICETYPE.FieldName, MDSERVICEEXTRAFIELDS.IDMDSERVICETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDSERVICEEXTRAFIELDS_UPD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDSERVICEEXTRAFIELDS_UPD", "MDSERVICEEXTRAFIELDS_UPD description.");
    UnSQL.SQL.Add(" UPDATE MDSERVICEEXTRAFIELDS SET ");
    UnSQL.SQL.Add("  DBDATATYPES_SIZE=[DBDATATYPES_SIZE],  ");
    UnSQL.SQL.Add("  DBKEYTYPE_NAME=[DBKEYTYPE_NAME],  ");
    UnSQL.SQL.Add("  EXTRAFIELDS_DESCRIPTION=[EXTRAFIELDS_DESCRIPTION],  ");
    UnSQL.SQL.Add("  EXTRAFIELDS_LOOKUPDISPLAY=[EXTRAFIELDS_LOOKUPDISPLAY],  ");
    UnSQL.SQL.Add("  EXTRAFIELDS_COLUMNSTYLE=[EXTRAFIELDS_COLUMNSTYLE],  ");
    UnSQL.SQL.Add("  EXTRAFIELDS_LOOKUPID=[EXTRAFIELDS_LOOKUPID],  ");
    UnSQL.SQL.Add("  EXTRAFIELDS_LOOKUPSQL=[EXTRAFIELDS_LOOKUPSQL],  ");
    UnSQL.SQL.Add("  EXTRAFIELDS_NAME=[EXTRAFIELDS_NAME],  ");
    UnSQL.SQL.Add("  EXTRAFIELDS_ORDER=[EXTRAFIELDS_ORDER],  ");
    UnSQL.SQL.Add("  IDCMDBDBDATATYPES=[IDCMDBDBDATATYPES],  ");
    UnSQL.SQL.Add("  IDCMDBKEYTYPE=[IDCMDBKEYTYPE],  ");
    UnSQL.SQL.Add("  IDMDSERVICEEXTRAFIELDS=[IDMDSERVICEEXTRAFIELDS],  ");
    UnSQL.SQL.Add("  IDMDSERVICEEXTRATABLE=[IDMDSERVICEEXTRATABLE],  ");
    UnSQL.SQL.Add("  IDMDSERVICETYPE=[IDMDSERVICETYPE] ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  DBDATATYPES_SIZE=[DBDATATYPES_SIZE] AND  ");
    UnSQL.SQL.Add("  DBKEYTYPE_NAME=[DBKEYTYPE_NAME] AND  ");
    UnSQL.SQL.Add("  EXTRAFIELDS_DESCRIPTION=[EXTRAFIELDS_DESCRIPTION] AND  ");
    UnSQL.SQL.Add("  EXTRAFIELDS_LOOKUPDISPLAY=[EXTRAFIELDS_LOOKUPDISPLAY] AND  ");
    UnSQL.SQL.Add("  EXTRAFIELDS_COLUMNSTYLE=[EXTRAFIELDS_COLUMNSTYLE] AND  ");
    UnSQL.SQL.Add("  EXTRAFIELDS_LOOKUPID=[EXTRAFIELDS_LOOKUPID] AND  ");
    UnSQL.SQL.Add("  EXTRAFIELDS_LOOKUPSQL=[EXTRAFIELDS_LOOKUPSQL] AND  ");
    UnSQL.SQL.Add("  EXTRAFIELDS_NAME=[EXTRAFIELDS_NAME] AND  ");
    UnSQL.SQL.Add("  EXTRAFIELDS_ORDER=[EXTRAFIELDS_ORDER] AND  ");
    UnSQL.SQL.Add("  IDCMDBDBDATATYPES=[IDCMDBDBDATATYPES] AND  ");
    UnSQL.SQL.Add("  IDCMDBKEYTYPE=[IDCMDBKEYTYPE] AND  ");
    UnSQL.SQL.Add("  IDMDSERVICEEXTRAFIELDS=[IDMDSERVICEEXTRAFIELDS] AND  ");
    UnSQL.SQL.Add("  IDMDSERVICEEXTRATABLE=[IDMDSERVICEEXTRATABLE] AND  ");
    UnSQL.SQL.Add("  IDMDSERVICETYPE=[IDMDSERVICETYPE] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDSERVICEEXTRAFIELDS_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            //GET(); 
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDSERVICEEXTRAFIELDS_DEL=function(/* StrIDMDSERVICEEXTRAFIELDS*/ IDMDSERVICEEXTRAFIELDS)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        /*if (StrIDMDSERVICEEXTRAFIELDS == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS.FieldName, " = " + UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDMDSERVICEEXTRAFIELDS = " IN (" + StrIDMDSERVICEEXTRAFIELDS + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS.FieldName, StrIDMDSERVICEEXTRAFIELDS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS.FieldName, IDMDSERVICEEXTRAFIELDS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDSERVICEEXTRAFIELDS_DEL_1   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDSERVICEEXTRAFIELDS_DEL_1", "MDSERVICEEXTRAFIELDS_DEL_1 description.");
    UnSQL.SQL.Add(" DELETE MDSERVICEEXTRAFIELDS  ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  DBDATATYPES_SIZE=[DBDATATYPES_SIZE] AND  ");
    UnSQL.SQL.Add("  DBKEYTYPE_NAME=[DBKEYTYPE_NAME] AND  ");
    UnSQL.SQL.Add("  EXTRAFIELDS_DESCRIPTION=[EXTRAFIELDS_DESCRIPTION] AND  ");
    UnSQL.SQL.Add("  EXTRAFIELDS_LOOKUPDISPLAY=[EXTRAFIELDS_LOOKUPDISPLAY] AND  ");
    UnSQL.SQL.Add("  EXTRAFIELDS_COLUMNSTYLE=[EXTRAFIELDS_COLUMNSTYLE] AND  ");
    UnSQL.SQL.Add("  EXTRAFIELDS_LOOKUPID=[EXTRAFIELDS_LOOKUPID] AND  ");
    UnSQL.SQL.Add("  EXTRAFIELDS_LOOKUPSQL=[EXTRAFIELDS_LOOKUPSQL] AND  ");
    UnSQL.SQL.Add("  EXTRAFIELDS_NAME=[EXTRAFIELDS_NAME] AND  ");
    UnSQL.SQL.Add("  EXTRAFIELDS_ORDER=[EXTRAFIELDS_ORDER] AND  ");
    UnSQL.SQL.Add("  IDCMDBDBDATATYPES=[IDCMDBDBDATATYPES] AND  ");
    UnSQL.SQL.Add("  IDCMDBKEYTYPE=[IDCMDBKEYTYPE] AND  ");
    UnSQL.SQL.Add("  IDMDSERVICEEXTRAFIELDS=[IDMDSERVICEEXTRAFIELDS] AND  ");
    UnSQL.SQL.Add("  IDMDSERVICEEXTRATABLE=[IDMDSERVICEEXTRATABLE] AND  ");
    UnSQL.SQL.Add("  IDMDSERVICETYPE=[IDMDSERVICETYPE] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDSERVICEEXTRAFIELDS_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDSERVICEEXTRAFIELDS_DEL=function(MDSERVICEEXTRAFIELDS)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS.FieldName, MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.DBDATATYPES_SIZE.FieldName, MDSERVICEEXTRAFIELDS.DBDATATYPES_SIZE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.DBKEYTYPE_NAME.FieldName, MDSERVICEEXTRAFIELDS.DBKEYTYPE_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_DESCRIPTION.FieldName, MDSERVICEEXTRAFIELDS.EXTRAFIELDS_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPDISPLAY.FieldName, MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPDISPLAY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE.FieldName, MDSERVICEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPID.FieldName, MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPID, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPSQL.FieldName, MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPSQL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME.FieldName, MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_ORDER.FieldName, MDSERVICEEXTRAFIELDS.EXTRAFIELDS_ORDER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES.FieldName, MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDCMDBKEYTYPE.FieldName, MDSERVICEEXTRAFIELDS.IDCMDBKEYTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRATABLE.FieldName, MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRATABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICETYPE.FieldName, MDSERVICEEXTRAFIELDS.IDMDSERVICETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDSERVICEEXTRAFIELDS_DEL_2   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDSERVICEEXTRAFIELDS_DEL_2", "MDSERVICEEXTRAFIELDS_DEL_2 description.");
    UnSQL.SQL.Add(" DELETE MDSERVICEEXTRAFIELDS  ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  DBDATATYPES_SIZE=[DBDATATYPES_SIZE] AND  ");
    UnSQL.SQL.Add("  DBKEYTYPE_NAME=[DBKEYTYPE_NAME] AND  ");
    UnSQL.SQL.Add("  EXTRAFIELDS_DESCRIPTION=[EXTRAFIELDS_DESCRIPTION] AND  ");
    UnSQL.SQL.Add("  EXTRAFIELDS_LOOKUPDISPLAY=[EXTRAFIELDS_LOOKUPDISPLAY] AND  ");
    UnSQL.SQL.Add("  EXTRAFIELDS_COLUMNSTYLE=[EXTRAFIELDS_COLUMNSTYLE] AND  ");
    UnSQL.SQL.Add("  EXTRAFIELDS_LOOKUPID=[EXTRAFIELDS_LOOKUPID] AND  ");
    UnSQL.SQL.Add("  EXTRAFIELDS_LOOKUPSQL=[EXTRAFIELDS_LOOKUPSQL] AND  ");
    UnSQL.SQL.Add("  EXTRAFIELDS_NAME=[EXTRAFIELDS_NAME] AND  ");
    UnSQL.SQL.Add("  EXTRAFIELDS_ORDER=[EXTRAFIELDS_ORDER] AND  ");
    UnSQL.SQL.Add("  IDCMDBDBDATATYPES=[IDCMDBDBDATATYPES] AND  ");
    UnSQL.SQL.Add("  IDCMDBKEYTYPE=[IDCMDBKEYTYPE] AND  ");
    UnSQL.SQL.Add("  IDMDSERVICEEXTRAFIELDS=[IDMDSERVICEEXTRAFIELDS] AND  ");
    UnSQL.SQL.Add("  IDMDSERVICEEXTRATABLE=[IDMDSERVICEEXTRATABLE] AND  ");
    UnSQL.SQL.Add("  IDMDSERVICETYPE=[IDMDSERVICETYPE] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDSERVICEEXTRAFIELDS_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
    
//**********************   METHODS   ********************************************************************************************

   
//DataSet Function 

//List Function 
Persistence.Model.Methods.MDSERVICEEXTRAFIELDS_ListSetID=function(MDSERVICEEXTRAFIELDSList, IDMDSERVICEEXTRAFIELDS)
{
    for (i = 0; i < MDSERVICEEXTRAFIELDSList.length; i++)
    {

        if (IDMDSERVICEEXTRAFIELDS == MDSERVICEEXTRAFIELDSList[i].IDMDSERVICEEXTRAFIELDS)
            return (MDSERVICEEXTRAFIELDSList[i]);

    }

    var TMDSERVICEEXTRAFIELDS = new Persistence.Properties.TMDSERVICEEXTRAFIELDS();
    TMDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS = IDMDSERVICEEXTRAFIELDS; 
    return (TMDSERVICEEXTRAFIELDS); 
}
Persistence.Model.Methods.MDSERVICEEXTRAFIELDS_ListAdd=function(MDSERVICEEXTRAFIELDSList,MDSERVICEEXTRAFIELDS)
{
    var i = Persistence.Model.Methods.MDSERVICEEXTRAFIELDS_ListGetIndex(MDSERVICEEXTRAFIELDSList, MDSERVICEEXTRAFIELDS);
    if (i == -1) MDSERVICEEXTRAFIELDSList.push(MDSERVICEEXTRAFIELDS);
    else
    {
        MDSERVICEEXTRAFIELDSList[i].DBDATATYPES_SIZE = MDSERVICEEXTRAFIELDS.DBDATATYPES_SIZE;
        MDSERVICEEXTRAFIELDSList[i].DBKEYTYPE_NAME = MDSERVICEEXTRAFIELDS.DBKEYTYPE_NAME;
        MDSERVICEEXTRAFIELDSList[i].EXTRAFIELDS_DESCRIPTION = MDSERVICEEXTRAFIELDS.EXTRAFIELDS_DESCRIPTION;
        MDSERVICEEXTRAFIELDSList[i].EXTRAFIELDS_LOOKUPDISPLAY = MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPDISPLAY;
        MDSERVICEEXTRAFIELDSList[i].EXTRAFIELDS_COLUMNSTYLE = MDSERVICEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE;
        MDSERVICEEXTRAFIELDSList[i].EXTRAFIELDS_LOOKUPID = MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPID;
        MDSERVICEEXTRAFIELDSList[i].EXTRAFIELDS_LOOKUPSQL = MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPSQL;
        MDSERVICEEXTRAFIELDSList[i].EXTRAFIELDS_NAME = MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME;
        MDSERVICEEXTRAFIELDSList[i].EXTRAFIELDS_ORDER = MDSERVICEEXTRAFIELDS.EXTRAFIELDS_ORDER;
        MDSERVICEEXTRAFIELDSList[i].IDCMDBDBDATATYPES = MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES;
        MDSERVICEEXTRAFIELDSList[i].IDCMDBKEYTYPE = MDSERVICEEXTRAFIELDS.IDCMDBKEYTYPE;
        MDSERVICEEXTRAFIELDSList[i].IDMDSERVICEEXTRAFIELDS = MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS;
        MDSERVICEEXTRAFIELDSList[i].IDMDSERVICEEXTRATABLE = MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRATABLE;
        MDSERVICEEXTRAFIELDSList[i].IDMDSERVICETYPE = MDSERVICEEXTRAFIELDS.IDMDSERVICETYPE;
    }
}
Persistence.Model.Methods.MDSERVICEEXTRAFIELDS_ListGetIndex=function(MDSERVICEEXTRAFIELDSList, MDSERVICEEXTRAFIELDS)
{
    for (i = 0; i < MDSERVICEEXTRAFIELDSList.length; i++)
    {
        if (MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS == MDSERVICEEXTRAFIELDSList[i].IDMDSERVICEEXTRAFIELDS)
            return (i);
    }
    return (-1);
}
Persistence.Model.Methods.MDSERVICEEXTRAFIELDS_ListGetIndex=function(MDSERVICEEXTRAFIELDSList, IDMDSERVICEEXTRAFIELDS)
{
    for (i = 0; i < MDSERVICEEXTRAFIELDSList.length; i++)
    {
        if (IDMDSERVICEEXTRAFIELDS == MDSERVICEEXTRAFIELDSList[i].IDMDSERVICEEXTRAFIELDS)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.Model.Methods.MDSERVICEEXTRAFIELDSListtoStr=function(MDSERVICEEXTRAFIELDSList)
{
    var Res = Persistence.Model.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(MDSERVICEEXTRAFIELDSList.length, Longitud, Texto);
        for (Counter = 0; Counter < MDSERVICEEXTRAFIELDSList.length; Counter++) {
            var UnMDSERVICEEXTRAFIELDS = MDSERVICEEXTRAFIELDSList[Counter];
            UnMDSERVICEEXTRAFIELDS.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Model.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.Model.Methods.StrtoMDSERVICEEXTRAFIELDS=function(ProtocoloStr, MDSERVICEEXTRAFIELDSList)
{
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        MDSERVICEEXTRAFIELDSList.length = 0;
        if (Persistence.Model.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnMDSERVICEEXTRAFIELDS = new Persistence.Demo.Properties.TMDSERVICEEXTRAFIELDS(); //Mode new row
                UnMDSERVICEEXTRAFIELDS.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.MDSERVICEEXTRAFIELDS_ListAdd(MDSERVICEEXTRAFIELDSList, UnMDSERVICEEXTRAFIELDS);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
   
}

//Socket Function 
Persistence.Model.Methods.MDSERVICEEXTRAFIELDSListToByte=function(MDSERVICEEXTRAFIELDSList, MemStream)
{
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, MDSERVICEEXTRAFIELDSList.length);
    for (i = 0; i < MDSERVICEEXTRAFIELDSList.length; i++)
    {
        MDSERVICEEXTRAFIELDSList[i].ToByte(MemStream);
    }
}
Persistence.Model.Methods.ByteToMDSERVICEEXTRAFIELDSList=function(MDSERVICEEXTRAFIELDSList, MemStream)
{
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++)
    {
        var UnMDSERVICEEXTRAFIELDS = new Persistence.Model.Properties.TMDSERVICEEXTRAFIELDS();
        MDSERVICEEXTRAFIELDSList[i].ToByte(MemStream);
    }
}
 