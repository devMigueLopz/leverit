﻿//TreduceDB
//Persistence.Model.Properties
//Persistence.Model.Methods

 

  
Persistence.Model.Properties.TMDCATEGORYDETAIL = function()
{
     
    this.CATEGORYDESCRIPTION = "";
    this.CATEGORYNAME  = "";
    this.CATEGORYSTATUS  = 0;
    this.CUSTOMCODE  = "";
    this.IDMDCATEGORY  = 0;
    this.IDMDCATEGORYDETAIL = 0;           
    this.MDCATEGORY = new Persistence.Model.Properties.TMDCATEGORY();
    this.MDCATEGORYDETAIL_TYPEUSERList = new Array(); //Persistence.Model.Properties.TMDCATEGORYDETAIL_TYPEUSER

    //Socket IO Properties
    this.ToByte=function(MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, CATEGORYDESCRIPTION);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, CATEGORYNAME);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, CATEGORYSTATUS);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, CUSTOMCODE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, IDMDCATEGORY);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, IDMDCATEGORYDETAIL);
    }
    this.ByteTo=function(MemStream)
    {
        CATEGORYDESCRIPTION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        CATEGORYNAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        CATEGORYSTATUS = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        CUSTOMCODE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        IDMDCATEGORY = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        IDMDCATEGORYDETAIL = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    }
    //Str IO Properties
    this.ToStr=function(Longitud, Texto)
    {
        SysCfg.Str.Protocol.WriteStr(CATEGORYDESCRIPTION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(CATEGORYNAME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(CATEGORYSTATUS, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(CUSTOMCODE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(IDMDCATEGORY, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(IDMDCATEGORYDETAIL, Longitud, Texto);
    }
    this.StrTo=function( Pos, Index, SLongitudArray, Texto)
    {
        CATEGORYDESCRIPTION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        CATEGORYNAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        CATEGORYSTATUS = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        CUSTOMCODE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        IDMDCATEGORY = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        IDMDCATEGORYDETAIL = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
    }

}
 

//**********************   METHODS server  ********************************************************************************************

//DataSet Function 
Persistence.Model.Methods.MDCATEGORYDETAIL_Fill=function(MDCATEGORYDETAILList, StrIDMDCATEGORYDETAIL/*IDMDCATEGORYDETAIL*/)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    MDCATEGORYDETAILList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDMDCATEGORYDETAIL == "")
    {
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.IDMDCATEGORYDETAIL.FieldName, " = " + UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.IDMDCATEGORYDETAIL.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else
    {
        StrIDMDCATEGORYDETAIL = " IN (" + StrIDMDCATEGORYDETAIL + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.IDMDCATEGORYDETAIL.FieldName, StrIDMDCATEGORYDETAIL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //Param.AddInt32(UsrCfg.InternoDataLinkNames.MDCATEGORYDETAIL.IDMDCATEGORYDETAIL.FieldName, IDMDCATEGORYDETAIL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try
    {
     
        var DS_MDCATEGORYDETAIL = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDCATEGORYDETAIL_GET", Param.ToBytes());
        ResErr = DS_MDCATEGORYDETAIL.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDCATEGORYDETAIL.DataSet.RecordCount > 0)
            {
                DS_MDCATEGORYDETAIL.DataSet.First();
                while (!(DS_MDCATEGORYDETAIL.DataSet.Eof))
                {
                    var MDCATEGORYDETAIL = new Persistence.Model.Properties.TMDCATEGORYDETAIL();
                    MDCATEGORYDETAIL.IDMDCATEGORYDETAIL = DS_MDCATEGORYDETAIL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.IDMDCATEGORYDETAIL.FieldName).asInt32();
                    //Other
                    MDCATEGORYDETAIL.CATEGORYDESCRIPTION = DS_MDCATEGORYDETAIL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CATEGORYDESCRIPTION.FieldName).asString();
                    MDCATEGORYDETAIL.CATEGORYNAME = DS_MDCATEGORYDETAIL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CATEGORYNAME.FieldName).asString();
                    MDCATEGORYDETAIL.CATEGORYSTATUS = DS_MDCATEGORYDETAIL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CATEGORYSTATUS.FieldName).asInt32();
                    MDCATEGORYDETAIL.IDMDCATEGORY = DS_MDCATEGORYDETAIL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.IDMDCATEGORY.FieldName).asInt32();
                    MDCATEGORYDETAIL.CUSTOMCODE = DS_MDCATEGORYDETAIL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CUSTOMCODE.FieldName).asString();
                    MDCATEGORYDETAILList.push(MDCATEGORYDETAIL);

                    DS_MDCATEGORYDETAIL.DataSet.Next();
                }
            }
            else
            {
                DS_MDCATEGORYDETAIL.ResErr.NotError = false;
                DS_MDCATEGORYDETAIL.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDCATEGORYDETAIL_GETID = function(MDCATEGORYDETAIL)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.IDMDCATEGORYDETAIL.FieldName, MDCATEGORYDETAIL.IDMDCATEGORYDETAIL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CATEGORYDESCRIPTION.FieldName, MDCATEGORYDETAIL.CATEGORYDESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CATEGORYNAME.FieldName, MDCATEGORYDETAIL.CATEGORYNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CATEGORYSTATUS.FieldName, MDCATEGORYDETAIL.CATEGORYSTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //   Param.AddText(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CUSTOMCODE.FieldName, MDCATEGORYDETAIL.CUSTOMCODE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.IDMDCATEGORY.FieldName, MDCATEGORYDETAIL.IDMDCATEGORY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDCATEGORYDETAIL_GETID   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDCATEGORYDETAIL_GETID", "MDCATEGORYDETAIL_GETID description.");
    UnSQL.SQL.Add(" SELECT  ");
    UnSQL.SQL.Add("  MDCATEGORYDETAIL.CATEGORYDESCRIPTION, ");
    UnSQL.SQL.Add("  MDCATEGORYDETAIL.CATEGORYNAME, ");
    UnSQL.SQL.Add("  MDCATEGORYDETAIL.CATEGORYSTATUS, ");
    UnSQL.SQL.Add("  MDCATEGORYDETAIL.CUSTOMCODE, ");
    UnSQL.SQL.Add("  MDCATEGORYDETAIL.IDMDCATEGORY, ");
    UnSQL.SQL.Add("  MDCATEGORYDETAIL.IDMDCATEGORYDETAIL ");
    UnSQL.SQL.Add(" FROM  MDCATEGORYDETAIL ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  MDCATEGORYDETAIL.CATEGORYDESCRIPTION=[CATEGORYDESCRIPTION] and  ");
    UnSQL.SQL.Add("  MDCATEGORYDETAIL.CATEGORYNAME=[CATEGORYNAME] and  ");
    UnSQL.SQL.Add("  MDCATEGORYDETAIL.CATEGORYSTATUS=[CATEGORYSTATUS] and  ");
    UnSQL.SQL.Add("  MDCATEGORYDETAIL.CUSTOMCODE=[CUSTOMCODE] and  ");
    UnSQL.SQL.Add("  MDCATEGORYDETAIL.IDMDCATEGORY=[IDMDCATEGORY] and  ");
    UnSQL.SQL.Add("  MDCATEGORYDETAIL.IDMDCATEGORYDETAIL=[IDMDCATEGORYDETAIL] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var DS_MDCATEGORYDETAIL = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDCATEGORYDETAIL_GETID", Param.ToBytes());
        ResErr = DS_MDCATEGORYDETAIL.ResErr;
        if (ResErr.NotError)
        {
            if (DS_MDCATEGORYDETAIL.DataSet.RecordCount > 0)
            {
                DS_MDCATEGORYDETAIL.DataSet.First();
                MDCATEGORYDETAIL.IDMDCATEGORYDETAIL = DS_MDCATEGORYDETAIL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.IDMDCATEGORYDETAIL.FieldName).asInt32();
                //Other
                MDCATEGORYDETAIL.CATEGORYNAME = DS_MDCATEGORYDETAIL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CATEGORYNAME.FieldName).asString();
                MDCATEGORYDETAIL.CATEGORYSTATUS = DS_MDCATEGORYDETAIL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CATEGORYSTATUS.FieldName).asInt32();
                MDCATEGORYDETAIL.IDMDCATEGORY = DS_MDCATEGORYDETAIL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.IDMDCATEGORY.FieldName).asInt32();
            }
            else
            {
                DS_MDCATEGORYDETAIL.ResErr.NotError = false;
                DS_MDCATEGORYDETAIL.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDCATEGORYDETAIL_ADD=function(MDCATEGORYDETAIL)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.IDMDCATEGORYDETAIL.FieldName, MDCATEGORYDETAIL.IDMDCATEGORYDETAIL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CATEGORYDESCRIPTION.FieldName, MDCATEGORYDETAIL.CATEGORYDESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CATEGORYNAME.FieldName, MDCATEGORYDETAIL.CATEGORYNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CATEGORYSTATUS.FieldName, MDCATEGORYDETAIL.CATEGORYSTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        // Param.AddText(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CUSTOMCODE.FieldName, MDCATEGORYDETAIL.CUSTOMCODE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.IDMDCATEGORY.FieldName, MDCATEGORYDETAIL.IDMDCATEGORY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDCATEGORYDETAIL_ADD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDCATEGORYDETAIL_ADD", "MDCATEGORYDETAIL_ADD description.");
    UnSQL.SQL.Add(" INSERT INTO MDCATEGORYDETAIL( ");
    UnSQL.SQL.Add("  CATEGORYDESCRIPTION,  ");
    UnSQL.SQL.Add("  CATEGORYNAME,  ");
    UnSQL.SQL.Add("  CATEGORYSTATUS,  ");
    UnSQL.SQL.Add("  CUSTOMCODE,  ");
    UnSQL.SQL.Add("  IDMDCATEGORY,  ");
    UnSQL.SQL.Add("  IDMDCATEGORYDETAIL ");
    UnSQL.SQL.Add(" )  ");
    UnSQL.SQL.Add(" VALUES ( ");
    UnSQL.SQL.Add("  [CATEGORYDESCRIPTION], ");
    UnSQL.SQL.Add("  [CATEGORYNAME], ");
    UnSQL.SQL.Add("  [CATEGORYSTATUS], ");
    UnSQL.SQL.Add("  [CUSTOMCODE], ");
    UnSQL.SQL.Add("  [IDMDCATEGORY], ");
    UnSQL.SQL.Add("  [IDMDCATEGORYDETAIL] ");
    UnSQL.SQL.Add(" ) ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDCATEGORYDETAIL_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            MDCATEGORYDETAIL_GETID(MDCATEGORYDETAIL);
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDCATEGORYDETAIL_UPD=function(MDCATEGORYDETAIL)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.IDMDCATEGORYDETAIL.FieldName, MDCATEGORYDETAIL.IDMDCATEGORYDETAIL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CATEGORYDESCRIPTION.FieldName, MDCATEGORYDETAIL.CATEGORYDESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CATEGORYNAME.FieldName, MDCATEGORYDETAIL.CATEGORYNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CATEGORYSTATUS.FieldName, MDCATEGORYDETAIL.CATEGORYSTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        // Param.AddText(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CUSTOMCODE.FieldName, MDCATEGORYDETAIL.CUSTOMCODE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.IDMDCATEGORY.FieldName, MDCATEGORYDETAIL.IDMDCATEGORY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDCATEGORYDETAIL_UPD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDCATEGORYDETAIL_UPD", "MDCATEGORYDETAIL_UPD description.");
    UnSQL.SQL.Add(" UPDATE MDCATEGORYDETAIL SET ");
    UnSQL.SQL.Add("  CATEGORYDESCRIPTION=[CATEGORYDESCRIPTION],  ");
    UnSQL.SQL.Add("  CATEGORYNAME=[CATEGORYNAME],  ");
    UnSQL.SQL.Add("  CATEGORYSTATUS=[CATEGORYSTATUS],  ");
    UnSQL.SQL.Add("  CUSTOMCODE=[CUSTOMCODE],  ");
    UnSQL.SQL.Add("  IDMDCATEGORY=[IDMDCATEGORY],  ");
    UnSQL.SQL.Add("  IDMDCATEGORYDETAIL=[IDMDCATEGORYDETAIL] ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  CATEGORYDESCRIPTION=[CATEGORYDESCRIPTION] AND  ");
    UnSQL.SQL.Add("  CATEGORYNAME=[CATEGORYNAME] AND  ");
    UnSQL.SQL.Add("  CATEGORYSTATUS=[CATEGORYSTATUS] AND  ");
    UnSQL.SQL.Add("  CUSTOMCODE=[CUSTOMCODE] AND  ");
    UnSQL.SQL.Add("  IDMDCATEGORY=[IDMDCATEGORY] AND  ");
    UnSQL.SQL.Add("  IDMDCATEGORYDETAIL=[IDMDCATEGORYDETAIL] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDCATEGORYDETAIL_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            //GET(); 
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDCATEGORYDETAIL_DEL=function(/*String StrIDMDCATEGORYDETAIL*/IDMDCATEGORYDETAIL)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        /*if (StrIDMDCATEGORYDETAIL == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.IDMDCATEGORYDETAIL.FieldName, " = " + UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.IDMDCATEGORYDETAIL.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDMDCATEGORYDETAIL = " IN (" + StrIDMDCATEGORYDETAIL + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.IDMDCATEGORYDETAIL.FieldName, StrIDMDCATEGORYDETAIL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.IDMDCATEGORYDETAIL.FieldName, IDMDCATEGORYDETAIL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDCATEGORYDETAIL_DEL_1   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDCATEGORYDETAIL_DEL_1", "MDCATEGORYDETAIL_DEL_1 description.");
    UnSQL.SQL.Add(" DELETE MDCATEGORYDETAIL  ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  CATEGORYDESCRIPTION=[CATEGORYDESCRIPTION] AND  ");
    UnSQL.SQL.Add("  CATEGORYNAME=[CATEGORYNAME] AND  ");
    UnSQL.SQL.Add("  CATEGORYSTATUS=[CATEGORYSTATUS] AND  ");
    UnSQL.SQL.Add("  CUSTOMCODE=[CUSTOMCODE] AND  ");
    UnSQL.SQL.Add("  IDMDCATEGORY=[IDMDCATEGORY] AND  ");
    UnSQL.SQL.Add("  IDMDCATEGORYDETAIL=[IDMDCATEGORYDETAIL] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDCATEGORYDETAIL_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Model.Methods.MDCATEGORYDETAIL_DEL=function(MDCATEGORYDETAIL)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.IDMDCATEGORYDETAIL.FieldName, MDCATEGORYDETAIL.IDMDCATEGORYDETAIL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CATEGORYDESCRIPTION.FieldName, MDCATEGORYDETAIL.CATEGORYDESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CATEGORYNAME.FieldName, MDCATEGORYDETAIL.CATEGORYNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CATEGORYSTATUS.FieldName, MDCATEGORYDETAIL.CATEGORYSTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        // Param.AddText(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CUSTOMCODE.FieldName, MDCATEGORYDETAIL.CUSTOMCODE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.IDMDCATEGORY.FieldName, MDCATEGORYDETAIL.IDMDCATEGORY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*

    //********   MDCATEGORYDETAIL_DEL_2   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL("MDCATEGORYDETAIL_DEL_2", "MDCATEGORYDETAIL_DEL_2 description.");
    UnSQL.SQL.Add(" DELETE MDCATEGORYDETAIL  ");
    UnSQL.SQL.Add(" WHERE   ");
    UnSQL.SQL.Add("  CATEGORYDESCRIPTION=[CATEGORYDESCRIPTION] AND  ");
    UnSQL.SQL.Add("  CATEGORYNAME=[CATEGORYNAME] AND  ");
    UnSQL.SQL.Add("  CATEGORYSTATUS=[CATEGORYSTATUS] AND  ");
    UnSQL.SQL.Add("  CUSTOMCODE=[CUSTOMCODE] AND  ");
    UnSQL.SQL.Add("  IDMDCATEGORY=[IDMDCATEGORY] AND  ");
    UnSQL.SQL.Add("  IDMDCATEGORYDETAIL=[IDMDCATEGORYDETAIL] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************


        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDCATEGORYDETAIL_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}

    

//**********************   METHODS   ********************************************************************************************

   
//DataSet Function 

//List Function 
Persistence.Model.Methods.MDCATEGORYDETAIL_ListSetID=function(MDCATEGORYDETAILList, IDMDCATEGORYDETAIL)
{
    for (var i = 0; i < MDCATEGORYDETAILList.length; i++)
    {

        if (IDMDCATEGORYDETAIL == MDCATEGORYDETAILList[i].IDMDCATEGORYDETAIL)
            return (MDCATEGORYDETAILList[i]);

    }
    var MDCATEGORYDETAIL = new Persistence.Model.Properties.TMDCATEGORYDETAIL();
    MDCATEGORYDETAIL.IDMDCATEGORYDETAIL = IDMDCATEGORYDETAIL;

    return (MDCATEGORYDETAIL);
}

Persistence.Model.Methods.MDCATEGORYDETAIL_ListAdd=function(MDCATEGORYDETAILList, MDCATEGORYDETAIL)
{
    var i = Persistence.Model.Methods.MDCATEGORYDETAIL_ListGetIndex(MDCATEGORYDETAILList, MDCATEGORYDETAIL);
    if (i == -1) MDCATEGORYDETAILList.push(MDCATEGORYDETAIL);
    else
    {
        MDCATEGORYDETAILList[i].CATEGORYDESCRIPTION = MDCATEGORYDETAIL.CATEGORYDESCRIPTION;
        MDCATEGORYDETAILList[i].CATEGORYNAME = MDCATEGORYDETAIL.CATEGORYNAME;
        MDCATEGORYDETAILList[i].CATEGORYSTATUS = MDCATEGORYDETAIL.CATEGORYSTATUS;
        MDCATEGORYDETAILList[i].CUSTOMCODE = MDCATEGORYDETAIL.CUSTOMCODE;
        MDCATEGORYDETAILList[i].IDMDCATEGORY = MDCATEGORYDETAIL.IDMDCATEGORY;
        MDCATEGORYDETAILList[i].IDMDCATEGORYDETAIL = MDCATEGORYDETAIL.IDMDCATEGORYDETAIL;
    }
}

Persistence.Model.Methods.MDCATEGORYDETAIL_COPY=function(MDCATEGORYDETAILSource)
{
    var MDCATEGORYDETAIL = new Persistence.Model.Properties.TMDCATEGORYDETAIL();
        MDCATEGORYDETAIL.CATEGORYDESCRIPTION = MDCATEGORYDETAILSource.CATEGORYDESCRIPTION;
        MDCATEGORYDETAIL.CATEGORYNAME = MDCATEGORYDETAILSource.CATEGORYNAME;
        MDCATEGORYDETAIL.CATEGORYSTATUS = MDCATEGORYDETAILSource.CATEGORYSTATUS;
        MDCATEGORYDETAIL.CUSTOMCODE = MDCATEGORYDETAILSource.CUSTOMCODE;
        MDCATEGORYDETAIL.IDMDCATEGORY = MDCATEGORYDETAILSource.IDMDCATEGORY;
        MDCATEGORYDETAIL.IDMDCATEGORYDETAIL = MDCATEGORYDETAILSource.IDMDCATEGORYDETAIL;
        return MDCATEGORYDETAIL;
}


Persistence.Model.Methods.MDCATEGORYDETAIL_ListGetIndex=function(MDCATEGORYDETAILList, MDCATEGORYDETAIL)
{
    for (var i = 0; i < MDCATEGORYDETAILList.length; i++)
    {
        if (MDCATEGORYDETAIL.IDMDCATEGORYDETAIL == MDCATEGORYDETAILList[i].IDMDCATEGORYDETAIL)
            return (i);
    }
    return (-1);
}
Persistence.Model.Methods.MDCATEGORYDETAIL_ListGetIndex=function(MDCATEGORYDETAILList, IDMDCATEGORYDETAIL)
{
    for (var i = 0; i < MDCATEGORYDETAILList.length; i++)
    {
        if (IDMDCATEGORYDETAIL == MDCATEGORYDETAILList[i].IDMDCATEGORYDETAIL)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.Model.Methods.MDCATEGORYDETAILListtoStr=function(MDCATEGORYDETAILList)
{
    var Res = Persistence.Model.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(MDCATEGORYDETAILList.length, Longitud, Texto);
        for (Counter = 0; Counter < MDCATEGORYDETAILList.length; Counter++) {
            var UnMDCATEGORYDETAIL = MDCATEGORYDETAILList[Counter];
            UnMDCATEGORYDETAIL.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Model.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.Model.Methods.StrtoMDCATEGORYDETAIL=function(ProtocoloStr, MDCATEGORYDETAILList)
{    
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        MDCATEGORYDETAILList.length = 0;
        if (Persistence.Model.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnMDCATEGORYDETAIL = new Persistence.Demo.Properties.TMDCATEGORYDETAIL(); //Mode new row
                UnMDCATEGORYDETAIL.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.MDCATEGORYDETAIL_ListAdd(MDCATEGORYDETAILList, UnMDCATEGORYDETAIL);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
    
}

//Socket Function 
Persistence.Model.Methods.MDCATEGORYDETAILListToByte = function(MDCATEGORYDETAILList, MemStream, idx)
{
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, MDCATEGORYDETAILList.length - idx);
    for ( i = idx; i < MDCATEGORYDETAILList.length; i++)
    {
        MDCATEGORYDETAILList[i].ToByte(MemStream);
    }
}
Persistence.Model.Methods.ByteToMDCATEGORYDETAILList= function(MDCATEGORYDETAILList,  MemStream)
{
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for ( i = 0; i < Count; i++)
    {
        var UnMDCATEGORYDETAIL = new Persistence.Model.Properties.TMDCATEGORYDETAIL();
        UnMDCATEGORYDETAIL.ByteTo(MemStream);
        Persistence.Model.Methods.MDCATEGORYDETAIL_ListAdd(MDCATEGORYDETAILList, UnMDCATEGORYDETAIL);
    }
}
   