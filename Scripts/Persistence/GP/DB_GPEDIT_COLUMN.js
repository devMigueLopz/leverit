﻿//**********************   PROPERTIES   *****************************************************************************************
Persistence.GP.Properties.TGPEDIT_COLUMN = function () {
    this.IDGPEDIT_COLUMN = 0;
    this.IDGPQUERY = 0;
    this.EDIT_COLUMNNAME = "";
    this.EDIT_COLUMNTYPE = "";
    this.MYCONSTRAINT = 0;
    this.AUTOINCTABLENAME = "";
    this.COLUMNSTYLE = 0;
    this.FILETYPE = 0;
    this.LOOKUPSQL = "";
    this.LOOKUPDISPLAYCOLUMN = "";
    this.LOOKUPDISPLAYCOLUMNLIST = "";
    this.ACTIVE = "";

    this.GPQUERY = new Persistence.GP.Properties.TGPQUERY();


    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDGPEDIT_COLUMN);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDGPQUERY);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.EDIT_COLUMNNAME);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.EDIT_COLUMNTYPE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.MYCONSTRAINT);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.AUTOINCTABLENAME);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.COLUMNSTYLE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.FILETYPE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.LOOKUPSQL);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.LOOKUPDISPLAYCOLUMN);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.LOOKUPDISPLAYCOLUMNLIST);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.ACTIVE);

    }
    this.ByteTo = function (MemStream) {
        this.IDGPEDIT_COLUMN = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDGPQUERY = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.EDIT_COLUMNNAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.EDIT_COLUMNTYPE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.MYCONSTRAINT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.AUTOINCTABLENAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.COLUMNSTYLE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.FILETYPE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.LOOKUPSQL = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.LOOKUPDISPLAYCOLUMN = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.LOOKUPDISPLAYCOLUMNLIST = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.ACTIVE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);

    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDGPEDIT_COLUMN, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDGPQUERY, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.EDIT_COLUMNNAME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.EDIT_COLUMNTYPE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.MYCONSTRAINT, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.AUTOINCTABLENAME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.COLUMNSTYLE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.FILETYPE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.LOOKUPSQL, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.LOOKUPDISPLAYCOLUMN, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.LOOKUPDISPLAYCOLUMNLIST, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.ACTIVE, Longitud, Texto);

    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDGPEDIT_COLUMN = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDGPQUERY = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.EDIT_COLUMNNAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.EDIT_COLUMNTYPE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.MYCONSTRAINT = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.AUTOINCTABLENAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.COLUMNSTYLE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.FILETYPE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.LOOKUPSQL = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.LOOKUPDISPLAYCOLUMN = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.LOOKUPDISPLAYCOLUMNLIST = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.ACTIVE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);

    }
}

//**********************   METHODS server  ********************************************************************************************
Persistence.GP.Methods.GPEDIT_COLUMN_Fill = function (GPEDIT_COLUMNList, StrIDGPEDIT_COLUMN/*noin IDGPEDIT_COLUMN*/) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    GPEDIT_COLUMNList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDGPEDIT_COLUMN == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.IDGPEDIT_COLUMN.FieldName, " = " + UsrCfg.InternoAtisNames.GPEDIT_COLUMN.NAME_TABLE + "." + UsrCfg.InternoAtisNames.GPEDIT_COLUMN.IDGPEDIT_COLUMN.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDGPEDIT_COLUMN = " IN (" + StrIDGPEDIT_COLUMN + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.IDGPEDIT_COLUMN.FieldName, StrIDGPEDIT_COLUMN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //noin Param.AddInt32(UsrCfg.InternoDemoNames.GPEDIT_COLUMN.IDGPEDIT_COLUMN.FieldName, IDGPEDIT_COLUMN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {

        var DS_GPEDIT_COLUMN = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "GPEDIT_COLUMN_GET", Param.ToBytes());
        ResErr = DS_GPEDIT_COLUMN.ResErr;
        if (ResErr.NotError) {
            if (DS_GPEDIT_COLUMN.DataSet.RecordCount > 0) {
                DS_GPEDIT_COLUMN.DataSet.First();
                while (!(DS_GPEDIT_COLUMN.DataSet.Eof)) {
                    var GPEDIT_COLUMN = new Persistence.GP.Properties.TGPEDIT_COLUMN();
                    GPEDIT_COLUMN.IDGPEDIT_COLUMN = DS_GPEDIT_COLUMN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.IDGPEDIT_COLUMN.FieldName).asInt32();
                    GPEDIT_COLUMN.IDGPQUERY = DS_GPEDIT_COLUMN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.IDGPQUERY.FieldName).asInt32();
                    GPEDIT_COLUMN.EDIT_COLUMNNAME = DS_GPEDIT_COLUMN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.EDIT_COLUMNNAME.FieldName).asString();
                    GPEDIT_COLUMN.EDIT_COLUMNTYPE = DS_GPEDIT_COLUMN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.EDIT_COLUMNTYPE.FieldName).asString();
                    GPEDIT_COLUMN.MYCONSTRAINT = DS_GPEDIT_COLUMN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.MYCONSTRAINT.FieldName).asInt32();
                    GPEDIT_COLUMN.AUTOINCTABLENAME = DS_GPEDIT_COLUMN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.AUTOINCTABLENAME.FieldName).asString();
                    GPEDIT_COLUMN.COLUMNSTYLE = DS_GPEDIT_COLUMN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.COLUMNSTYLE.FieldName).asInt32();
                    GPEDIT_COLUMN.FILETYPE = DS_GPEDIT_COLUMN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.FILETYPE.FieldName).asInt32();
                    GPEDIT_COLUMN.LOOKUPSQL = DS_GPEDIT_COLUMN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.LOOKUPSQL.FieldName).asText();
                    GPEDIT_COLUMN.LOOKUPDISPLAYCOLUMN = DS_GPEDIT_COLUMN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.LOOKUPDISPLAYCOLUMN.FieldName).asString();
                    GPEDIT_COLUMN.LOOKUPDISPLAYCOLUMNLIST = DS_GPEDIT_COLUMN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.LOOKUPDISPLAYCOLUMNLIST.FieldName).asText();
                    GPEDIT_COLUMN.ACTIVE = DS_GPEDIT_COLUMN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.ACTIVE.FieldName).asString();

                    //Other
                    GPEDIT_COLUMNList.push(GPEDIT_COLUMN);
                    DS_GPEDIT_COLUMN.DataSet.Next();
                }
            }
            else {
                DS_GPEDIT_COLUMN.ResErr.NotError = false;
                DS_GPEDIT_COLUMN.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPEDIT_COLUMN_GETID = function (GPEDIT_COLUMN) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.IDGPEDIT_COLUMN.FieldName, GPEDIT_COLUMN.IDGPEDIT_COLUMN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.IDGPQUERY.FieldName, GPEDIT_COLUMN.IDGPQUERY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.EDIT_COLUMNNAME.FieldName, GPEDIT_COLUMN.EDIT_COLUMNNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.EDIT_COLUMNTYPE.FieldName, GPEDIT_COLUMN.EDIT_COLUMNTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.MYCONSTRAINT.FieldName, GPEDIT_COLUMN.MYCONSTRAINT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.AUTOINCTABLENAME.FieldName, GPEDIT_COLUMN.AUTOINCTABLENAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.COLUMNSTYLE.FieldName, GPEDIT_COLUMN.COLUMNSTYLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.FILETYPE.FieldName, GPEDIT_COLUMN.FILETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddText(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.LOOKUPSQL.FieldName, GPEDIT_COLUMN.LOOKUPSQL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.LOOKUPDISPLAYCOLUMN.FieldName, GPEDIT_COLUMN.LOOKUPDISPLAYCOLUMN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddText(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.LOOKUPDISPLAYCOLUMNLIST.FieldName, GPEDIT_COLUMN.LOOKUPDISPLAYCOLUMNLIST, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.ACTIVE.FieldName, GPEDIT_COLUMN.ACTIVE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

        var DS_GPEDIT_COLUMN = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "GPEDIT_COLUMN_GETID", Param.ToBytes());
        ResErr = DS_GPEDIT_COLUMN.ResErr;
        if (ResErr.NotError) {
            if (DS_GPEDIT_COLUMN.DataSet.RecordCount > 0) {
                DS_GPEDIT_COLUMN.DataSet.First();
                GPEDIT_COLUMN.IDGPEDIT_COLUMN = DS_GPEDIT_COLUMN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.IDGPEDIT_COLUMN.FieldName).asInt32();
            }
            else {
                DS_GPEDIT_COLUMN.ResErr.NotError = false;
                DS_GPEDIT_COLUMN.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPEDIT_COLUMN_ADD = function (GPEDIT_COLUMN) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.IDGPEDIT_COLUMN.FieldName, GPEDIT_COLUMN.IDGPEDIT_COLUMN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.IDGPQUERY.FieldName, GPEDIT_COLUMN.IDGPQUERY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.EDIT_COLUMNNAME.FieldName, GPEDIT_COLUMN.EDIT_COLUMNNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.EDIT_COLUMNTYPE.FieldName, GPEDIT_COLUMN.EDIT_COLUMNTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.MYCONSTRAINT.FieldName, GPEDIT_COLUMN.MYCONSTRAINT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.AUTOINCTABLENAME.FieldName, GPEDIT_COLUMN.AUTOINCTABLENAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.COLUMNSTYLE.FieldName, GPEDIT_COLUMN.COLUMNSTYLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.FILETYPE.FieldName, GPEDIT_COLUMN.FILETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.LOOKUPSQL.FieldName, GPEDIT_COLUMN.LOOKUPSQL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.LOOKUPDISPLAYCOLUMN.FieldName, GPEDIT_COLUMN.LOOKUPDISPLAYCOLUMN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.LOOKUPDISPLAYCOLUMNLIST.FieldName, GPEDIT_COLUMN.LOOKUPDISPLAYCOLUMNLIST, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.ACTIVE.FieldName, GPEDIT_COLUMN.ACTIVE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPEDIT_COLUMN_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.GP.Methods.GPEDIT_COLUMN_GETID(GPEDIT_COLUMN);
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPEDIT_COLUMN_UPD = function (GPEDIT_COLUMN) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.IDGPEDIT_COLUMN.FieldName, GPEDIT_COLUMN.IDGPEDIT_COLUMN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.IDGPQUERY.FieldName, GPEDIT_COLUMN.IDGPQUERY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.EDIT_COLUMNNAME.FieldName, GPEDIT_COLUMN.EDIT_COLUMNNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.EDIT_COLUMNTYPE.FieldName, GPEDIT_COLUMN.EDIT_COLUMNTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.MYCONSTRAINT.FieldName, GPEDIT_COLUMN.MYCONSTRAINT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.AUTOINCTABLENAME.FieldName, GPEDIT_COLUMN.AUTOINCTABLENAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.COLUMNSTYLE.FieldName, GPEDIT_COLUMN.COLUMNSTYLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.FILETYPE.FieldName, GPEDIT_COLUMN.FILETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.LOOKUPSQL.FieldName, GPEDIT_COLUMN.LOOKUPSQL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.LOOKUPDISPLAYCOLUMN.FieldName, GPEDIT_COLUMN.LOOKUPDISPLAYCOLUMN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.LOOKUPDISPLAYCOLUMNLIST.FieldName, GPEDIT_COLUMN.LOOKUPDISPLAYCOLUMNLIST, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.ACTIVE.FieldName, GPEDIT_COLUMN.ACTIVE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPEDIT_COLUMN_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.GP.Methods.GPEDIT_COLUMN_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.GP.Methods.GPEDIT_COLUMN_DELIDGPEDIT_COLUMN(/*String StrIDGPEDIT_COLUMN*/Param);
    }
    else {
        Res = Persistence.GP.Methods.GPEDIT_COLUMN_DELGPEDIT_COLUMN(Param/*GPEDIT_COLUMNList*/);
    }
    return (Res);
}

Persistence.GP.Methods.GPEDIT_COLUMN_DELIDGPEDIT_COLUMN = function (/*String StrIDGPEDIT_COLUMN*/IDGPEDIT_COLUMN) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDGPEDIT_COLUMN == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.IDGPEDIT_COLUMN.FieldName, " = " + UsrCfg.InternoAtisNames.GPEDIT_COLUMN.NAME_TABLE + "." + UsrCfg.InternoAtisNames.GPEDIT_COLUMN.IDGPEDIT_COLUMN.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDGPEDIT_COLUMN = " IN (" + StrIDGPEDIT_COLUMN + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.IDGPEDIT_COLUMN.FieldName, StrIDGPEDIT_COLUMN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.IDGPEDIT_COLUMN.FieldName, IDGPEDIT_COLUMN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPEDIT_COLUMN_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPEDIT_COLUMN_DELGPEDIT_COLUMN = function (GPEDIT_COLUMN/*GPEDIT_COLUMNList*/) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        /*
        StrIDGPEDIT_COLUMN = "";
        StrIDOTRO = "";
        for (var i = 0; i < GPEDIT_COLUMNList.length; i++) {
        StrIDGPEDIT_COLUMN += GPEDIT_COLUMNList[i].IDGPEDIT_COLUMN + ",";
        StrIDOTRO += GPEDIT_COLUMNList[i].IDOTRO + ",";
        }
        StrIDGPEDIT_COLUMN = StrIDGPEDIT_COLUMN.substring(0, StrIDGPEDIT_COLUMN.length - 1);
        StrIDOTRO = StrIDOTRO.substring(0, StrIDOTRO.length - 1);
        
        
        if (StrIDGPEDIT_COLUMN == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.IDGPEDIT_COLUMN.FieldName, " = " + UsrCfg.InternoAtisNames.GPEDIT_COLUMN.NAME_TABLE + "." + UsrCfg.InternoAtisNames.GPEDIT_COLUMN.IDGPEDIT_COLUMN.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDGPEDIT_COLUMN = " IN (" + StrIDGPEDIT_COLUMN + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.IDGPEDIT_COLUMN.FieldName, StrIDGPEDIT_COLUMN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/


        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.IDGPEDIT_COLUMN.FieldName, GPEDIT_COLUMN.IDGPEDIT_COLUMN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPEDIT_COLUMN_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

Persistence.GP.Methods.GPEDIT_COLUMN_GET_BY_GPQUERY = function (GPEDIT_COLUMNList, IDGPQUERY)
{
    var ResErr = new SysCfg.Error.Properties.TResErr();
    GPEDIT_COLUMNList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();

    Param.AddInt32(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.IDGPQUERY.FieldName, IDGPQUERY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

    try
    {
        var DS_GPEDIT_COLUMN = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "GPEDIT_COLUMN_SELECTBYGPQUERY", Param.ToBytes());
        ResErr = DS_GPEDIT_COLUMN.ResErr;
        if (ResErr.NotError)
        {
            if (DS_GPEDIT_COLUMN.DataSet.RecordCount > 0)
            {
                DS_GPEDIT_COLUMN.DataSet.First();
                while (!(DS_GPEDIT_COLUMN.DataSet.Eof))
                {
                    var GPEDIT_COLUMN = new Persistence.GP.Properties.TGPEDIT_COLUMN();
                    GPEDIT_COLUMN.IDGPEDIT_COLUMN = DS_GPEDIT_COLUMN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.IDGPEDIT_COLUMN.FieldName).asInt32();
                    GPEDIT_COLUMN.IDGPQUERY = DS_GPEDIT_COLUMN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.IDGPQUERY.FieldName).asInt32();
                    GPEDIT_COLUMN.EDIT_COLUMNNAME = DS_GPEDIT_COLUMN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.EDIT_COLUMNNAME.FieldName).asString();
                    GPEDIT_COLUMN.EDIT_COLUMNTYPE = DS_GPEDIT_COLUMN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.EDIT_COLUMNTYPE.FieldName).asString();
                    GPEDIT_COLUMN.MYCONSTRAINT = DS_GPEDIT_COLUMN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.MYCONSTRAINT.FieldName).asInt32();
                    GPEDIT_COLUMN.AUTOINCTABLENAME = DS_GPEDIT_COLUMN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.AUTOINCTABLENAME.FieldName).asString();
                    GPEDIT_COLUMN.COLUMNSTYLE = DS_GPEDIT_COLUMN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.COLUMNSTYLE.FieldName).asInt32();
                    GPEDIT_COLUMN.FILETYPE = DS_GPEDIT_COLUMN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.FILETYPE.FieldName).asInt32();
                    GPEDIT_COLUMN.LOOKUPSQL = DS_GPEDIT_COLUMN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.LOOKUPSQL.FieldName).asText();
                    GPEDIT_COLUMN.LOOKUPDISPLAYCOLUMN = DS_GPEDIT_COLUMN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.LOOKUPDISPLAYCOLUMN.FieldName).asString();
                    GPEDIT_COLUMN.LOOKUPDISPLAYCOLUMNLIST = DS_GPEDIT_COLUMN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.LOOKUPDISPLAYCOLUMNLIST.FieldName).asText();
                    GPEDIT_COLUMN.ACTIVE = DS_GPEDIT_COLUMN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.ACTIVE.FieldName).asString();

                    //Other
                    GPEDIT_COLUMNList.push(GPEDIT_COLUMN);
                    DS_GPEDIT_COLUMN.DataSet.Next();
                }
            }
            else
            {
                DS_GPEDIT_COLUMN.ResErr.NotError = false;
                DS_GPEDIT_COLUMN.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}

//**********************   METHODS   ********************************************************************************************
Persistence.GP.Methods.GPEDIT_COLUMN_ListSetID = function (GPEDIT_COLUMNList, IDGPEDIT_COLUMN) {
    for (i = 0; i < GPEDIT_COLUMNList.length; i++) {
        if (IDGPEDIT_COLUMN == GPEDIT_COLUMNList[i].IDGPEDIT_COLUMN)
            return (GPEDIT_COLUMNList[i]);
    }
    var UnGPEDIT_COLUMN = new Persistence.Properties.TGPEDIT_COLUMN;
    UnGPEDIT_COLUMN.IDGPEDIT_COLUMN = IDGPEDIT_COLUMN;
    return (UnGPEDIT_COLUMN);
}
Persistence.GP.Methods.GPEDIT_COLUMN_ListAdd = function (GPEDIT_COLUMNList, GPEDIT_COLUMN) {
    var i = Persistence.GP.Methods.GPEDIT_COLUMN_ListGetIndex(GPEDIT_COLUMNList, GPEDIT_COLUMN);
    if (i == -1) GPEDIT_COLUMNList.push(GPEDIT_COLUMN);
    else {
        GPEDIT_COLUMNList[i].IDGPEDIT_COLUMN = GPEDIT_COLUMN.IDGPEDIT_COLUMN;
        GPEDIT_COLUMNList[i].IDGPQUERY = GPEDIT_COLUMN.IDGPQUERY;
        GPEDIT_COLUMNList[i].EDIT_COLUMNNAME = GPEDIT_COLUMN.EDIT_COLUMNNAME;
        GPEDIT_COLUMNList[i].EDIT_COLUMNTYPE = GPEDIT_COLUMN.EDIT_COLUMNTYPE;
        GPEDIT_COLUMNList[i].MYCONSTRAINT = GPEDIT_COLUMN.MYCONSTRAINT;
        GPEDIT_COLUMNList[i].AUTOINCTABLENAME = GPEDIT_COLUMN.AUTOINCTABLENAME;
        GPEDIT_COLUMNList[i].COLUMNSTYLE = GPEDIT_COLUMN.COLUMNSTYLE;
        GPEDIT_COLUMNList[i].FILETYPE = GPEDIT_COLUMN.FILETYPE;
        GPEDIT_COLUMNList[i].LOOKUPSQL = GPEDIT_COLUMN.LOOKUPSQL;
        GPEDIT_COLUMNList[i].LOOKUPDISPLAYCOLUMN = GPEDIT_COLUMN.LOOKUPDISPLAYCOLUMN;
        GPEDIT_COLUMNList[i].LOOKUPDISPLAYCOLUMNLIST = GPEDIT_COLUMN.LOOKUPDISPLAYCOLUMNLIST;
        GPEDIT_COLUMNList[i].ACTIVE = GPEDIT_COLUMN.ACTIVE;

    }
}
//CJRC_26072018
Persistence.GP.Methods.GPEDIT_COLUMN_ListGetIndex = function (GPEDIT_COLUMNList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.GP.Methods.GPEDIT_COLUMN_ListGetIndexIDGPEDIT_COLUMN(GPEDIT_COLUMNList, Param);

    }
    else {
        Res = Persistence.GP.Methods.GPEDIT_COLUMN_ListGetIndexGPEDIT_COLUMN(GPEDIT_COLUMNList, Param);
    }
    return (Res);
}

Persistence.GP.Methods.GPEDIT_COLUMN_ListGetIndexGPEDIT_COLUMN = function (GPEDIT_COLUMNList, GPEDIT_COLUMN) {
    for (i = 0; i < GPEDIT_COLUMNList.length; i++) {
        if (GPEDIT_COLUMN.IDGPEDIT_COLUMN == GPEDIT_COLUMNList[i].IDGPEDIT_COLUMN)
            return (i);
    }
    return (-1);
}
Persistence.GP.Methods.GPEDIT_COLUMN_ListGetIndexIDGPEDIT_COLUMN = function (GPEDIT_COLUMNList, IDGPEDIT_COLUMN) {
    for (i = 0; i < GPEDIT_COLUMNList.length; i++) {
        if (IDGPEDIT_COLUMN == GPEDIT_COLUMNList[i].IDGPEDIT_COLUMN)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.GP.Methods.GPEDIT_COLUMNListtoStr = function (GPEDIT_COLUMNList) {
    var Res = Persistence.Properties._Version + "(1)0";
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(GPEDIT_COLUMNList.length, Longitud, Texto);
        for (Counter = 0; Counter < GPEDIT_COLUMNList.length; Counter++) {
            var UnGPEDIT_COLUMN = GPEDIT_COLUMNList[Counter];
            UnGPEDIT_COLUMN.ToStr(Longitud, Texto);
        }

        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
}
Persistence.GP.Methods.StrtoGPEDIT_COLUMN = function (ProtocoloStr, GPEDIT_COLUMNList) {

    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        GPEDIT_COLUMNList.length = 0;
        if (Persistence.GP.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")") - ProtocoloStr.indexOf("(") - 1);
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.Length - ProtocoloStr.indexOf(")") - 1);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnGPEDIT_COLUMN = new Persistence.Properties.TGPEDIT_COLUMN(); //Mode new row
                UnGPEDIT_COLUMN.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.GP.GPEDIT_COLUMN_ListAdd(GPEDIT_COLUMNList, UnGPEDIT_COLUMN);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}


//Socket Function 
Persistence.GP.Methods.GPEDIT_COLUMNListToByte = function (GPEDIT_COLUMNList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, GPEDIT_COLUMNList.Count - idx);
    for (i = idx; i < GPEDIT_COLUMNList.length; i++) {
        GPEDIT_COLUMNList[i].ToByte(MemStream);
    }
}
Persistence.GP.Methods.ByteToGPEDIT_COLUMNList = function (GPEDIT_COLUMNList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnGPEDIT_COLUMN = new Persistence.Properties.TGPEDIT_COLUMN();
        UnGPEDIT_COLUMN.ByteTo(MemStream);
        Methods.GPEDIT_COLUMN_ListAdd(GPEDIT_COLUMNList, UnGPEDIT_COLUMN);
    }
}