﻿Persistence.GP.Methods.StartReation = function (PersistenceProfiler, IDGPQUERY)
{
    var DB_GPQUERY_ListOrder = SysCfg.CopyList(PersistenceProfiler.GPQUERY_List).sort(function (a, b) { return a.IDGPQUERY - b.IDGPQUERY; });
    var DB_GPVIEWListOrder = SysCfg.CopyList(PersistenceProfiler.GPVIEWList).sort(function (a, b) { return a.IDGPQUERY - b.IDGPQUERY; });
    var DB_GPCOLUMNVIEWListOrder = SysCfg.CopyList(PersistenceProfiler.GPCOLUMNVIEWList).sort(function (a, b) { return a.IDGPVIEW - b.IDGPVIEW; });
    var DB_GPEDIT_COLUMN_ListOrder = SysCfg.CopyList(PersistenceProfiler.GPEDIT_COLUMN_List).sort(function (a, b) { return a.IDGPQUERY - b.IDGPQUERY; });
    var DB_GPEDIT_TABLE_ListOrder = SysCfg.CopyList(PersistenceProfiler.GPEDIT_TABLE_List).sort(function (a, b) { return a.IDGPQUERY - b.IDGPQUERY; });
    var DB_GPCHART_ListOrder = SysCfg.CopyList(PersistenceProfiler.GPCHART_List).sort(function (a, b) { return a.IDGPQUERY - b.IDGPQUERY; });
    var DB_GPREPORTSQL_ListOrder = SysCfg.CopyList(PersistenceProfiler.GPREPORTSQL_List).sort(function (a, b) { return a.IDGPQUERY - b.IDGPQUERY; });
    var DB_GPREPORTTEMPLATE_ListOrder = SysCfg.CopyList(PersistenceProfiler.GPREPORTTEMPLATE_List).sort(function (a, b) { return a.IDGPQUERY - b.IDGPQUERY; });

    //GPQUERY -> GPVIEW
    var idxMN = 0;
    for (var i = 0; i < DB_GPQUERY_ListOrder.length; i++) {
        for (var x = idxMN; x < DB_GPVIEWListOrder.length; x++) {
            idxMN = x;
            if (DB_GPVIEWListOrder[x].IDGPQUERY == DB_GPQUERY_ListOrder[i].IDGPQUERY) {
                DB_GPQUERY_ListOrder[i].GPVIEWList.push(DB_GPVIEWListOrder[x]);
                DB_GPVIEWListOrder[x].GPQUERY = DB_GPQUERY_ListOrder[i];
            }
            if (DB_GPVIEWListOrder[x].IDGPQUERY > DB_GPQUERY_ListOrder[i].IDGPQUERY) {
                break;
            }
        }
    }

    //GPVIEW -> GPCOLUMNVIEW
    var idxMN = 0;
    for (var i = 0; i < DB_GPVIEWListOrder.length; i++)
    {
        for (var x = idxMN; x < DB_GPCOLUMNVIEWListOrder.length; x++)
        {
            idxMN = x;
            if (DB_GPCOLUMNVIEWListOrder[x].IDGPVIEW == DB_GPVIEWListOrder[i].IDGPVIEW)
            {
                DB_GPVIEWListOrder[i].GPCOLUMNVIEWList.push(DB_GPCOLUMNVIEWListOrder[x]);
                DB_GPCOLUMNVIEWListOrder[x].GPVIEW = DB_GPVIEWListOrder[i];
            }
            if (DB_GPCOLUMNVIEWListOrder[x].IDGPVIEW > DB_GPVIEWListOrder[i].IDGPVIEW)
            {
                break;
            }
        }
    }

    //GPQUERY -> GPGPEDIT_COLUMN
    var idxMN = 0;
    for (var i = 0; i < DB_GPQUERY_ListOrder.length; i++)
    {
        for (var x = idxMN; x < DB_GPEDIT_COLUMN_ListOrder.length; x++)
        {
            idxMN = x;
            if (DB_GPEDIT_COLUMN_ListOrder[x].IDGPQUERY == DB_GPQUERY_ListOrder[i].IDGPQUERY)
            {
                DB_GPQUERY_ListOrder[i].GPEDIT_COLUMN_List.push(DB_GPEDIT_COLUMN_ListOrder[x]);
                DB_GPEDIT_COLUMN_ListOrder[x].GPQUERY = DB_GPQUERY_ListOrder[i];
            }
            if (DB_GPEDIT_COLUMN_ListOrder[x].IDGPQUERY > DB_GPQUERY_ListOrder[i].IDGPQUERY)
            {
                break;
            }
        }
    }

    //GPQUERY -> GPEDIT_TABLE
    var idxMN = 0;
    for (var i = 0; i < DB_GPQUERY_ListOrder.length; i++) {
        for (var x = idxMN; x < DB_GPEDIT_TABLE_ListOrder.length; x++) {
            idxMN = x;
            if (DB_GPEDIT_TABLE_ListOrder[x].IDGPQUERY == DB_GPQUERY_ListOrder[i].IDGPQUERY) {
                DB_GPQUERY_ListOrder[i].GPEDIT_TABLE_List.push(DB_GPEDIT_TABLE_ListOrder[x]);
                DB_GPEDIT_TABLE_ListOrder[x].GPQUERY = DB_GPQUERY_ListOrder[i];
            }
            if (DB_GPEDIT_TABLE_ListOrder[x].IDGPQUERY > DB_GPQUERY_ListOrder[i].IDGPQUERY) {
                break;
            }
        }
    }

    //GPQUERY -> GPGPCHART
    var idxMN = 0;
    for (var i = 0; i < DB_GPQUERY_ListOrder.length; i++) {
        for (var x = idxMN; x < DB_GPCHART_ListOrder.length; x++) {
            idxMN = x;
            if (DB_GPCHART_ListOrder[x].IDGPQUERY == DB_GPQUERY_ListOrder[i].IDGPQUERY) {
                DB_GPQUERY_ListOrder[i].GPCHART_List.push(DB_GPCHART_ListOrder[x]);
                DB_GPCHART_ListOrder[x].GPQUERY = DB_GPQUERY_ListOrder[i];
            }
            if (DB_GPCHART_ListOrder[x].IDGPQUERY > DB_GPQUERY_ListOrder[i].IDGPQUERY) {
                break;
            }
        }
    }

    //GPQUERY -> GPGPREPORTSQL
    var idxMN = 0;
    for (var i = 0; i < DB_GPQUERY_ListOrder.length; i++) {
        for (var x = idxMN; x < DB_GPREPORTSQL_ListOrder.length; x++) {
            idxMN = x;
            if (DB_GPREPORTSQL_ListOrder[x].IDGPQUERY == DB_GPQUERY_ListOrder[i].IDGPQUERY) {
                DB_GPQUERY_ListOrder[i].GPREPORTSQL_List.push(DB_GPREPORTSQL_ListOrder[x]);
                DB_GPREPORTSQL_ListOrder[x].GPQUERY = DB_GPQUERY_ListOrder[i];
            }
            if (DB_GPREPORTSQL_ListOrder[x].IDGPQUERY > DB_GPQUERY_ListOrder[i].IDGPQUERY) {
                break;
            }
        }
    }

    //GPQUERY -> GPGPREPORTTEMPLATE
    var idxMN = 0;
    for (var i = 0; i < DB_GPQUERY_ListOrder.length; i++) {
        for (var x = idxMN; x < DB_GPREPORTTEMPLATE_ListOrder.length; x++) {
            idxMN = x;
            if (DB_GPREPORTTEMPLATE_ListOrder[x].IDGPQUERY == DB_GPQUERY_ListOrder[i].IDGPQUERY) {
                DB_GPQUERY_ListOrder[i].GPREPORTTEMPLATE_List.push(DB_GPREPORTTEMPLATE_ListOrder[x]);
                DB_GPREPORTTEMPLATE_ListOrder[x].GPQUERY = DB_GPQUERY_ListOrder[i];
            }
            if (DB_GPREPORTTEMPLATE_ListOrder[x].IDGPQUERY > DB_GPQUERY_ListOrder[i].IDGPQUERY) {
                break;
            }
        }
    }

    ////////////////////////////////////
    ////////////////////////////////////

    var DB_GPEDIT_TABLE_ListOrder = SysCfg.CopyList(PersistenceProfiler.GPEDIT_TABLE_List).sort(function (a, b) { return a.IDGPEDIT_TABLE - b.IDGPEDIT_TABLE; });
    var DB_GPEDIT_COLUMNEVENT_ListOrder = SysCfg.CopyList(PersistenceProfiler.GPEDIT_COLUMNEVENT_List).sort(function (a, b) { return a.IDGPEDIT_TABLE - b.IDGPEDIT_TABLE; });

    //GPEDIT_TABLE -> GPEDIT_COLUMNEVENT
    var idxMN = 0;
    for (var i = 0; i < DB_GPEDIT_TABLE_ListOrder.length; i++) {
        for (var x = idxMN; x < DB_GPEDIT_COLUMNEVENT_ListOrder.length; x++) {
            idxMN = x;
            if (DB_GPEDIT_COLUMNEVENT_ListOrder[x].IDGPEDIT_TABLE == DB_GPEDIT_TABLE_ListOrder[i].IDGPEDIT_TABLE)
            {
                DB_GPEDIT_TABLE_ListOrder[i].GPEDIT_COLUMNEVENT_List.push(DB_GPEDIT_COLUMNEVENT_ListOrder[x]);
                DB_GPEDIT_COLUMNEVENT_ListOrder[x].GPEDIT_TABLE = DB_GPEDIT_TABLE_ListOrder[i];
            }
            if (DB_GPEDIT_COLUMNEVENT_ListOrder[x].IDGPEDIT_TABLE > DB_GPEDIT_TABLE_ListOrder[i].IDGPEDIT_TABLE) {
                break;
            }
        }
    }


    //DB_PSMAINListOrder = SysCfg.CopyList(PersistenceProfiler.PSMAINList).sort(function (a, b) { return a.IDPSMAIN - b.IDPSMAIN; });
    //var DB_PSSECUNDARYListOrder = SysCfg.CopyList(PersistenceProfiler.PSSECUNDARYList).sort(function (a, b) { return a.IDPSMAIN - b.IDPSMAIN; });

    //idxMN = 0;
    //for (var i = 0; i < DB_PSMAINListOrder.length; i++) {
    //    // if ((IDGROUP == "") || (IDGROUP == DB_PSMAINListOrder[i].IDPSGROUP)) {
    //    for (var x = idxMN; x < DB_PSSECUNDARYListOrder.length; x++) {
    //        idxMN = x;
    //        if (DB_PSSECUNDARYListOrder[x].IDPSMAIN == DB_PSMAINListOrder[i].IDPSMAIN) {
    //            DB_PSMAINListOrder[i].PSSECUNDARYList.push(DB_PSSECUNDARYListOrder[x]);
    //            DB_PSSECUNDARYListOrder[x].PSMAIN = DB_PSMAINListOrder[i];
    //            //RemoteHelp.Methods.RHREQUEST_CPUCR_ListAdd(DB_PSGROUPListOrder[i].GROUPList, DB_PSMAINListOrder[x]);                    
    //        }
    //        if (DB_PSSECUNDARYListOrder[x].IDPSMAIN > DB_PSMAINListOrder[i].IDPSMAIN) {
    //            //  if (DB_PSSECUNDARYListOrder[x].IDPSMAIN > DB_PSMAINListOrder[i].IDPSMAIN) {"
    //            break;
    //        }
    //    }
    //    // }
    //}

    return DB_GPQUERY_ListOrder;

};




/*
si es un id compuesto tienen que concatenerlo campoid1+campoid2= idconcatena 


//Ordenar  
if (AAAListOrder[x].IDXXX == XXXListOrder[i].IDXXX)
{
	RemoteHelp.Methods.AAA_ListAdd(XXXListOrder[i].AAAList, AAAListOrder[x]);
}
if (AAAListOrder[xList<Persistence.YYY.Properties.TXXX> XXXListOrder = XXXProfiler.XXXList.OrderBy(x => x.IDXXX).ToList();
List<Persistence.YYY.Properties.TAAAA> AAAListOrder = YYYProfiler.AAAList.OrderBy(x => x.IDXXX).ToList();

//Especcificar 
//****************************** Eejemplo de indexaccion de id numerico 
Int32 IDXXX = -1; //Todos o id solo actuyalzia seleccionado 
Int32 idxAAA = 0;
//... otros

for (int i = 0; i < XXXListOrder.Count; i++)
{
	if ((IDXXX == "") || (IDXXX == XXXListOrder[i].IDXXX))
{
		for (int x = idxAAA; x < AAAListOrder.Count; x++)
{
			idxAAA = x;
].IDXXX > XXXListOrder[i].IDXXX)
{
				break;
}
}
	//... otros
}
}

//****************************** Eejemplo de indexaccion de id cadena
String IDXXX = ""; //Todos o id solo actuyalzia seleccionado 
Int32 idxAAA = 0;
//... otros
for (int i = 0; i < XXXListOrder.Count; i++)
{
	if ((IDXXX == "") || (IDXXX == XXXListOrder[i].IDXXX))
{
		for (int x = idxAAA; x < AAAListOrder.Count; x++)
{
			idxAAA = x;
			int comp = string.Compare(AAAListOrder[x].IDXXX, XXXListOrder[i].IDXXX, true);

			if (comp == 0)
{
				RemoteHelp.Methods.AAA_ListAdd(XXXListOrder[i].AAAList, AAAListOrder[x]);                            
}
			if (comp > 0)
{
				break;
}
			if (comp < 0)
{
}
}
	//... otros
}
}
*/
