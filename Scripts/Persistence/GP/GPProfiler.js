﻿Persistence.GP.Properties._Version = '001';

Persistence.GP.TGPProfiler = function () {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    ResErr = new SysCfg.Error.Properties.TResErr();
    this.GPQUERY_List = new Array();
    this.GPVIEWList = new Array();
    this.GPCOLUMNVIEWList = new Array();
    this.GPEDIT_COLUMN_List = new Array();
    this.GPEDIT_TABLE_List = new Array();
    this.GPEDIT_COLUMNEVENT_List = new Array();
    this.GPCHART_List = new Array();
    this.GPREPORTSQL_List = new Array();
    this.GPREPORTTEMPLATE_List = new Array();

    this.Fill = function ()
    {
        Persistence.GP.Methods.GPQUERY_Fill(this.GPQUERY_List, "");
        Persistence.GP.Methods.GPVIEW_Fill(this.GPVIEWList, "");
        Persistence.GP.Methods.GPCOLUMNVIEW_Fill(this.GPCOLUMNVIEWList, "");
        Persistence.GP.Methods.GPEDIT_COLUMN_Fill(this.GPEDIT_COLUMN_List, "");
        Persistence.GP.Methods.GPEDIT_TABLE_Fill(this.GPEDIT_TABLE_List, "");
        Persistence.GP.Methods.GPCHART_Fill(this.GPCHART_List, "");
        Persistence.GP.Methods.GPREPORTSQL_Fill(this.GPREPORTSQL_List, "");
        Persistence.GP.Methods.GPREPORTTEMPLATE_Fill(this.GPREPORTTEMPLATE_List, "");
        Persistence.GP.Methods.GPEDIT_COLUMNEVENT_Fill(this.GPEDIT_COLUMNEVENT_List, "");
        Persistence.GP.Methods.StartReation(this, -1);
    }
}