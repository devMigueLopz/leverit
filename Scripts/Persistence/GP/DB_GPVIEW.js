﻿//**********************   PROPERTIES   *****************************************************************************************
Persistence.GP.Properties.TGPVIEW = function () {
    this.IDGPVIEW = 0;
    this.IDGPQUERY = 0;
    this.GPVIEW_NAME = "";
    this.GPVIEW_DESCRIPTION = "";
    this.GPVIEW_ISACTIVE = "";
    this.GPVIEW_ISPUBLIC = "";
    this.IDCMDBUSER = 0;
    this.GPVIEW_FILTER = "";

    this.GPQUERY = new Persistence.GP.Properties.TGPQUERY();

    this.GPCOLUMNVIEWList = new Array();

    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDGPVIEW);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDGPQUERY);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.GPVIEW_NAME);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.GPVIEW_DESCRIPTION);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.GPVIEW_ISACTIVE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.GPVIEW_ISPUBLIC);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDCMDBUSER);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.GPVIEW_FILTER);

    }
    this.ByteTo = function (MemStream) {
        this.IDGPVIEW = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDGPQUERY = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.GPVIEW_NAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.GPVIEW_DESCRIPTION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.GPVIEW_ISACTIVE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.GPVIEW_ISPUBLIC = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDCMDBUSER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.GPVIEW_FILTER = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);

    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDGPVIEW, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDGPQUERY, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.GPVIEW_NAME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.GPVIEW_DESCRIPTION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.GPVIEW_ISACTIVE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.GPVIEW_ISPUBLIC, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDCMDBUSER, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.GPVIEW_FILTER, Longitud, Texto);

    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDGPVIEW = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDGPQUERY = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.GPVIEW_NAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.GPVIEW_DESCRIPTION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.GPVIEW_ISACTIVE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.GPVIEW_ISPUBLIC = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.IDCMDBUSER = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.GPVIEW_FILTER = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);

    }
}

//**********************   METHODS server  ********************************************************************************************
Persistence.GP.Methods.GPVIEW_Fill = function (GPVIEWList, StrIDGPVIEW/*noin IDGPVIEW*/) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    GPVIEWList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDGPVIEW == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPVIEW.IDGPVIEW.FieldName, " = " + UsrCfg.InternoAtisNames.GPVIEW.NAME_TABLE + "." + UsrCfg.InternoAtisNames.GPVIEW.IDGPVIEW.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDGPVIEW = " IN (" + StrIDGPVIEW + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPVIEW.IDGPVIEW.FieldName, StrIDGPVIEW, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //noin Param.AddInt32(UsrCfg.InternoDemoNames.GPVIEW.IDGPVIEW.FieldName, IDGPVIEW, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {
        /*
        //********   GPVIEW_GET   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPVIEW_GET", @"GPVIEW_GET description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   IDGPVIEW, "); 
         UnSQL.SQL.Add(@"   IDGPQUERY, "); 
         UnSQL.SQL.Add(@"   GPVIEW_NAME, "); 
         UnSQL.SQL.Add(@"   GPVIEW_DESCRIPTION, "); 
         UnSQL.SQL.Add(@"   GPVIEW_ISACTIVE, "); 
         UnSQL.SQL.Add(@"   GPVIEW_ISPUBLIC, "); 
         UnSQL.SQL.Add(@"   IDCMDBUSER, "); 
         UnSQL.SQL.Add(@"   GPVIEW_FILTER "); 
         UnSQL.SQL.Add(@"   FROM GPVIEW "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPVIEW=@[IDGPVIEW] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_GPVIEW = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "GPVIEW_GET", Param.ToBytes());
        ResErr = DS_GPVIEW.ResErr;
        if (ResErr.NotError) {
            if (DS_GPVIEW.DataSet.RecordCount > 0) {
                DS_GPVIEW.DataSet.First();
                while (!(DS_GPVIEW.DataSet.Eof)) {
                    var GPVIEW = new Persistence.GP.Properties.TGPVIEW();
                    GPVIEW.IDGPVIEW = DS_GPVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPVIEW.IDGPVIEW.FieldName).asInt32();
                    GPVIEW.IDGPQUERY = DS_GPVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPVIEW.IDGPQUERY.FieldName).asInt32();
                    GPVIEW.GPVIEW_NAME = DS_GPVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_NAME.FieldName).asString();
                    GPVIEW.GPVIEW_DESCRIPTION = DS_GPVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_DESCRIPTION.FieldName).asText();
                    GPVIEW.GPVIEW_ISACTIVE = DS_GPVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_ISACTIVE.FieldName).asString();
                    GPVIEW.GPVIEW_ISPUBLIC = DS_GPVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_ISPUBLIC.FieldName).asString();
                    GPVIEW.IDCMDBUSER = DS_GPVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPVIEW.IDCMDBUSER.FieldName).asInt32();
                    GPVIEW.GPVIEW_FILTER = DS_GPVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_FILTER.FieldName).asText();

                    //Other
                    GPVIEWList.push(GPVIEW);
                    DS_GPVIEW.DataSet.Next();
                }
            }
            else {
                DS_GPVIEW.ResErr.NotError = false;
                DS_GPVIEW.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPVIEW_GETID = function (GPVIEW) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPVIEW.IDGPVIEW.FieldName, GPVIEW.IDGPVIEW, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPVIEW.IDGPQUERY.FieldName, GPVIEW.IDGPQUERY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_NAME.FieldName, GPVIEW.GPVIEW_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddText(UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_DESCRIPTION.FieldName, GPVIEW.GPVIEW_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_ISACTIVE.FieldName, GPVIEW.GPVIEW_ISACTIVE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_ISPUBLIC.FieldName, GPVIEW.GPVIEW_ISPUBLIC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.GPVIEW.IDCMDBUSER.FieldName, GPVIEW.IDCMDBUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddText(UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_FILTER.FieldName, GPVIEW.GPVIEW_FILTER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

        /*
        //********   GPVIEW_GETID   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPVIEW_GETID", @"GPVIEW_GETID description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   FROM GPVIEW "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPQUERY=@[IDGPQUERY] And "); 
         UnSQL.SQL.Add(@"   GPVIEW_NAME=@[GPVIEW_NAME] And "); 
         UnSQL.SQL.Add(@"   GPVIEW_ISACTIVE=@[GPVIEW_ISACTIVE] And "); 
         UnSQL.SQL.Add(@"   GPVIEW_ISPUBLIC=@[GPVIEW_ISPUBLIC] And "); 
         UnSQL.SQL.Add(@"   IDCMDBUSER=@[IDCMDBUSER] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_GPVIEW = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "GPVIEW_GETID", Param.ToBytes());
        ResErr = DS_GPVIEW.ResErr;
        if (ResErr.NotError) {
            if (DS_GPVIEW.DataSet.RecordCount > 0) {
                DS_GPVIEW.DataSet.First();
                GPVIEW.IDGPVIEW = DS_GPVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPVIEW.IDGPVIEW.FieldName).asInt32();
            }
            else {
                DS_GPVIEW.ResErr.NotError = false;
                DS_GPVIEW.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPVIEW_ADD = function (GPVIEW) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPVIEW.IDGPVIEW.FieldName, GPVIEW.IDGPVIEW, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPVIEW.IDGPQUERY.FieldName, GPVIEW.IDGPQUERY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_NAME.FieldName, GPVIEW.GPVIEW_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_DESCRIPTION.FieldName, GPVIEW.GPVIEW_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_ISACTIVE.FieldName, GPVIEW.GPVIEW_ISACTIVE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_ISPUBLIC.FieldName, GPVIEW.GPVIEW_ISPUBLIC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.GPVIEW.IDCMDBUSER.FieldName, GPVIEW.IDCMDBUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_FILTER.FieldName, GPVIEW.GPVIEW_FILTER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        /* 
        //********   GPVIEW_ADD   ************************* 
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPVIEW_ADD", @"GPVIEW_ADD description."); 
        UnSQL.SQL.Add(@"   INSERT INTO GPVIEW( "); 
         UnSQL.SQL.Add(@"   IDGPQUERY, "); 
         UnSQL.SQL.Add(@"   GPVIEW_NAME, "); 
         UnSQL.SQL.Add(@"   GPVIEW_DESCRIPTION, "); 
         UnSQL.SQL.Add(@"   GPVIEW_ISACTIVE, "); 
         UnSQL.SQL.Add(@"   GPVIEW_ISPUBLIC, "); 
         UnSQL.SQL.Add(@"   IDCMDBUSER, "); 
         UnSQL.SQL.Add(@"   GPVIEW_FILTER "); 
         UnSQL.SQL.Add(@"   ) Values ( ");  
        UnSQL.SQL.Add(@"   @[IDGPQUERY], "); 
         UnSQL.SQL.Add(@"   @[GPVIEW_NAME], "); 
         UnSQL.SQL.Add(@"   @[GPVIEW_DESCRIPTION], "); 
         UnSQL.SQL.Add(@"   @[GPVIEW_ISACTIVE], "); 
         UnSQL.SQL.Add(@"   @[GPVIEW_ISPUBLIC], "); 
         UnSQL.SQL.Add(@"   @[IDCMDBUSER], "); 
         UnSQL.SQL.Add(@"   @[GPVIEW_FILTER] "); 
         UnSQL.SQL.Add(@"   )  "); 
        UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPVIEW_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.GP.Methods.GPVIEW_GETID(GPVIEW);
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPVIEW_UPD = function (GPVIEW) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPVIEW.IDGPVIEW.FieldName, GPVIEW.IDGPVIEW, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPVIEW.IDGPQUERY.FieldName, GPVIEW.IDGPQUERY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_NAME.FieldName, GPVIEW.GPVIEW_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_DESCRIPTION.FieldName, GPVIEW.GPVIEW_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_ISACTIVE.FieldName, GPVIEW.GPVIEW_ISACTIVE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_ISPUBLIC.FieldName, GPVIEW.GPVIEW_ISPUBLIC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.GPVIEW.IDCMDBUSER.FieldName, GPVIEW.IDCMDBUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_FILTER.FieldName, GPVIEW.GPVIEW_FILTER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);


        /*   
        //********   GPVIEW_UPD   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPVIEW_UPD", @"GPVIEW_UPD description.");  
        UnSQL.SQL.Add(@"  UPDATE GPVIEW Set "); 
         UnSQL.SQL.Add(@"   IDGPQUERY=@[IDGPQUERY], "); 
         UnSQL.SQL.Add(@"   GPVIEW_NAME=@[GPVIEW_NAME], "); 
         UnSQL.SQL.Add(@"   GPVIEW_DESCRIPTION=@[GPVIEW_DESCRIPTION], "); 
         UnSQL.SQL.Add(@"   GPVIEW_ISACTIVE=@[GPVIEW_ISACTIVE], "); 
         UnSQL.SQL.Add(@"   GPVIEW_ISPUBLIC=@[GPVIEW_ISPUBLIC], "); 
         UnSQL.SQL.Add(@"   IDCMDBUSER=@[IDCMDBUSER], "); 
         UnSQL.SQL.Add(@"   GPVIEW_FILTER=@[GPVIEW_FILTER] "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPVIEW=@[IDGPVIEW] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPVIEW_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.GP.Methods.GPVIEW_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.GP.Methods.GPVIEW_DELIDGPVIEW(/*String StrIDGPVIEW*/Param);
    }
    else {
        Res = Persistence.GP.Methods.GPVIEW_DELGPVIEW(Param/*GPVIEWList*/);
    }
    return (Res);
}

Persistence.GP.Methods.GPVIEW_DELIDGPVIEW = function (/*String StrIDGPVIEW*/IDGPVIEW) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDGPVIEW == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPVIEW.IDGPVIEW.FieldName, " = " + UsrCfg.InternoAtisNames.GPVIEW.NAME_TABLE + "." + UsrCfg.InternoAtisNames.GPVIEW.IDGPVIEW.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDGPVIEW = " IN (" + StrIDGPVIEW + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPVIEW.IDGPVIEW.FieldName, StrIDGPVIEW, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.GPVIEW.IDGPVIEW.FieldName, IDGPVIEW, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   GPVIEW_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPVIEW_DEL", @"GPVIEW_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE GPVIEW "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPVIEW=@[IDGPVIEW]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPVIEW_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPVIEW_DELGPVIEW = function (GPVIEW/*GPVIEWList*/) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        /*
        StrIDGPVIEW = "";
        StrIDOTRO = "";
        for (var i = 0; i < GPVIEWList.length; i++) {
        StrIDGPVIEW += GPVIEWList[i].IDGPVIEW + ",";
        StrIDOTRO += GPVIEWList[i].IDOTRO + ",";
        }
        StrIDGPVIEW = StrIDGPVIEW.substring(0, StrIDGPVIEW.length - 1);
        StrIDOTRO = StrIDOTRO.substring(0, StrIDOTRO.length - 1);
        
        
        if (StrIDGPVIEW == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPVIEW.IDGPVIEW.FieldName, " = " + UsrCfg.InternoAtisNames.GPVIEW.NAME_TABLE + "." + UsrCfg.InternoAtisNames.GPVIEW.IDGPVIEW.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDGPVIEW = " IN (" + StrIDGPVIEW + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPVIEW.IDGPVIEW.FieldName, StrIDGPVIEW, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/


        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.GPVIEW.IDGPVIEW.FieldName, GPVIEW.IDGPVIEW, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*    
        //********   GPVIEW_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPVIEW_DEL", @"GPVIEW_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE GPVIEW "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPVIEW=@[IDGPVIEW]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPVIEW_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

Persistence.GP.Methods.GPVIEW_GET_BY_GPQUERY = function (IDGPQUERY)
{
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var GPVIEWList = new Array();
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();

    Param.AddInt32(UsrCfg.InternoAtisNames.GPVIEW.IDGPQUERY.FieldName, IDGPQUERY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

    try
    {
        var DS_GPVIEW = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "GPVIEW_SELECTBYIDQUERY", Param.ToBytes());
        ResErr = DS_GPVIEW.ResErr;
        if (ResErr.NotError)
        {
            if (DS_GPVIEW.DataSet.RecordCount > 0)
            {
                DS_GPVIEW.DataSet.First();
                while (!(DS_GPVIEW.DataSet.Eof))
                {
                    var GPVIEW = new Persistence.GP.Properties.TGPVIEW();
                    GPVIEW.IDGPVIEW = DS_GPVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPVIEW.IDGPVIEW.FieldName).asInt32();
                    GPVIEW.IDGPQUERY = DS_GPVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPVIEW.IDGPQUERY.FieldName).asInt32();
                    GPVIEW.GPVIEW_NAME = DS_GPVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_NAME.FieldName).asString();
                    GPVIEW.GPVIEW_DESCRIPTION = DS_GPVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_DESCRIPTION.FieldName).asText();
                    GPVIEW.GPVIEW_ISACTIVE = DS_GPVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_ISACTIVE.FieldName).asString();
                    GPVIEW.GPVIEW_ISPUBLIC = DS_GPVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_ISPUBLIC.FieldName).asString();
                    GPVIEW.IDCMDBUSER = DS_GPVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPVIEW.IDCMDBUSER.FieldName).asInt32();
                    GPVIEW.GPVIEW_FILTER = DS_GPVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_FILTER.FieldName).asText();

                    //Other
                    GPVIEWList.push(GPVIEW);
                    DS_GPVIEW.DataSet.Next();
                }
            }
            else
            {
                DS_GPVIEW.ResErr.NotError = false;
                DS_GPVIEW.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (GPVIEWList);
}

//**********************   METHODS   ********************************************************************************************
Persistence.GP.Methods.GPVIEW_ListSetID = function (GPVIEWList, IDGPVIEW) {
    for (i = 0; i < GPVIEWList.length; i++) {
        if (IDGPVIEW == GPVIEWList[i].IDGPVIEW)
            return (GPVIEWList[i]);
    }
    var UnGPVIEW = new Persistence.Properties.TGPVIEW;
    UnGPVIEW.IDGPVIEW = IDGPVIEW;
    return (UnGPVIEW);
}
Persistence.GP.Methods.GPVIEW_ListAdd = function (GPVIEWList, GPVIEW) {
    var i = Persistence.GP.Methods.GPVIEW_ListGetIndex(GPVIEWList, GPVIEW);
    if (i == -1) GPVIEWList.push(GPVIEW);
    else {
        GPVIEWList[i].IDGPVIEW = GPVIEW.IDGPVIEW;
        GPVIEWList[i].IDGPQUERY = GPVIEW.IDGPQUERY;
        GPVIEWList[i].GPVIEW_NAME = GPVIEW.GPVIEW_NAME;
        GPVIEWList[i].GPVIEW_DESCRIPTION = GPVIEW.GPVIEW_DESCRIPTION;
        GPVIEWList[i].GPVIEW_ISACTIVE = GPVIEW.GPVIEW_ISACTIVE;
        GPVIEWList[i].GPVIEW_ISPUBLIC = GPVIEW.GPVIEW_ISPUBLIC;
        GPVIEWList[i].IDCMDBUSER = GPVIEW.IDCMDBUSER;
        GPVIEWList[i].GPVIEW_FILTER = GPVIEW.GPVIEW_FILTER;

    }
}
//CJRC_26072018
Persistence.GP.Methods.GPVIEW_ListGetIndex = function (GPVIEWList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.GP.Methods.GPVIEW_ListGetIndexIDGPVIEW(GPVIEWList, Param) ;

    }
    else {
        Res = Persistence.GP.Methods.GPVIEW_ListGetIndexGPVIEW(GPVIEWList, Param);
    }
    return (Res);
}

Persistence.GP.Methods.GPVIEW_ListGetIndexGPVIEW = function (GPVIEWList, GPVIEW) {
    for (i = 0; i < GPVIEWList.length; i++) {
        if (GPVIEW.IDGPVIEW == GPVIEWList[i].IDGPVIEW)
            return (i);
    }
    return (-1);
}
Persistence.GP.Methods.GPVIEW_ListGetIndexIDGPVIEW = function (GPVIEWList, IDGPVIEW) {
    for (i = 0; i < GPVIEWList.length; i++) {
        if (IDGPVIEW == GPVIEWList[i].IDGPVIEW)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.GP.Methods.GPVIEWListtoStr = function (GPVIEWList) {
    var Res = Persistence.Properties._Version + "(1)0";
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(GPVIEWList.length, Longitud, Texto);
        for (Counter = 0; Counter < GPVIEWList.length; Counter++) {
            var UnGPVIEW = GPVIEWList[Counter];
            UnGPVIEW.ToStr(Longitud, Texto);
        }

        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
}
Persistence.GP.Methods.StrtoGPVIEW = function (ProtocoloStr, GPVIEWList) {

    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        GPVIEWList.length = 0;
        if (Persistence.GP.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")") - ProtocoloStr.indexOf("(") - 1);
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.Length - ProtocoloStr.indexOf(")") - 1);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnGPVIEW = new Persistence.Properties.TGPVIEW(); //Mode new row
                UnGPVIEW.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.GP.GPVIEW_ListAdd(GPVIEWList, UnGPVIEW);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}


//Socket Function 
Persistence.GP.Methods.GPVIEWListToByte = function (GPVIEWList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, GPVIEWList.Count - idx);
    for (i = idx; i < GPVIEWList.length; i++) {
        GPVIEWList[i].ToByte(MemStream);
    }
}
Persistence.GP.Methods.ByteToGPVIEWList = function (GPVIEWList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnGPVIEW = new Persistence.Properties.TGPVIEW();
        UnGPVIEW.ByteTo(MemStream);
        Methods.GPVIEW_ListAdd(GPVIEWList, UnGPVIEW);
    }
}
