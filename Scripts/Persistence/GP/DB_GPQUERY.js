﻿//**********************   PROPERTIES   *****************************************************************************************
Persistence.GP.Properties.TGPQUERY = function () {
    this.IDGPQUERY = 0;
    this.IDGPQUERYTYPE = 0;
    this.GPQUERY_NAME = "";
    this.GPQUERY_DESCRIPTION = "";
    this.STRSQL = "";
    this.PATH = "";
    this.OPEN_TYPENAME = "";
    this.OPEN_PREFIX = "";
    this.OPEN_ID = "";
    this.IDATUSER = 0;
    this.START = 0;

    this.GPVIEWList = new Array();
    this.GPEDIT_COLUMN_List = new Array();
    this.GPEDIT_TABLE_List = new Array();
    this.GPCHART_List = new Array();
    this.GPREPORTSQL_List = new Array();
    this.GPREPORTTEMPLATE_List = new Array();


    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDGPQUERY);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDGPQUERYTYPE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.GPQUERY_NAME);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.GPQUERY_DESCRIPTION);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.STRSQL);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.PATH);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.OPEN_TYPENAME);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.OPEN_PREFIX);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.OPEN_ID);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDATUSER);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.START);

    }
    this.ByteTo = function (MemStream) {
        this.IDGPQUERY = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDGPQUERYTYPE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.GPQUERY_NAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.GPQUERY_DESCRIPTION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.STRSQL = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.PATH = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.OPEN_TYPENAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.OPEN_PREFIX = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.OPEN_ID = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDATUSER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.START = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);

    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDGPQUERY, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDGPQUERYTYPE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.GPQUERY_NAME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.GPQUERY_DESCRIPTION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.STRSQL, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.PATH, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.OPEN_TYPENAME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.OPEN_PREFIX, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.OPEN_ID, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDATUSER, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.START, Longitud, Texto);

    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDGPQUERY = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDGPQUERYTYPE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.GPQUERY_NAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.GPQUERY_DESCRIPTION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.STRSQL = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.PATH = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.OPEN_TYPENAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.OPEN_PREFIX = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.OPEN_ID = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.IDATUSER = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.START = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);

    }
}

//**********************   METHODS server  ********************************************************************************************
Persistence.GP.Methods.GPQUERY_Fill = function (GPQUERYList, StrIDGPQUERY) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    GPQUERYList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDGPQUERY == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPQUERY.IDGPQUERY.FieldName, " = " + UsrCfg.InternoAtisNames.GPQUERY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.GPQUERY.IDGPQUERY.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDGPQUERY = " IN (" + StrIDGPQUERY + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPQUERY.IDGPQUERY.FieldName, StrIDGPQUERY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    try {
        var DS_GPQUERY = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "GPQUERY_GET", Param.ToBytes());
        ResErr = DS_GPQUERY.ResErr;
        if (ResErr.NotError) {
            if (DS_GPQUERY.DataSet.RecordCount > 0) {
                DS_GPQUERY.DataSet.First();
                while (!(DS_GPQUERY.DataSet.Eof)) {
                    var GPQUERY = new Persistence.GP.Properties.TGPQUERY();
                    GPQUERY.IDGPQUERY = DS_GPQUERY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPQUERY.IDGPQUERY.FieldName).asInt32();
                    GPQUERY.IDGPQUERYTYPE = DS_GPQUERY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPQUERY.IDGPQUERYTYPE.FieldName).asInt32();
                    GPQUERY.GPQUERY_NAME = DS_GPQUERY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPQUERY.GPQUERY_NAME.FieldName).asString();
                    GPQUERY.GPQUERY_DESCRIPTION = DS_GPQUERY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPQUERY.GPQUERY_DESCRIPTION.FieldName).asText();
                    GPQUERY.STRSQL = DS_GPQUERY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPQUERY.STRSQL.FieldName).asText();
                    GPQUERY.PATH = DS_GPQUERY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPQUERY.PATH.FieldName).asText();
                    GPQUERY.OPEN_TYPENAME = DS_GPQUERY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPQUERY.OPEN_TYPENAME.FieldName).asString();
                    GPQUERY.OPEN_PREFIX = DS_GPQUERY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPQUERY.OPEN_PREFIX.FieldName).asString();
                    GPQUERY.OPEN_ID = DS_GPQUERY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPQUERY.OPEN_ID.FieldName).asString();
                    GPQUERY.IDATUSER = DS_GPQUERY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPQUERY.IDATUSER.FieldName).asInt32();
                    GPQUERY.START = DS_GPQUERY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPQUERY.START.FieldName).asInt32();

                    //Other
                    GPQUERYList.push(GPQUERY);
                    DS_GPQUERY.DataSet.Next();
                }
            }
            else {
                DS_GPQUERY.ResErr.NotError = false;
                DS_GPQUERY.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPQUERY_GETBYID = function (IDGPQUERY)
{
    var GPQUERY = new Persistence.GP.Properties.TGPQUERY;
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.GPQUERY.IDGPQUERY.FieldName, IDGPQUERY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var DS_GPQUERY = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "GPQUERY_SELECTBYID", Param.ToBytes());
        if (DS_GPQUERY.ResErr.NotError)
        {
            if (DS_GPQUERY.DataSet.RecordCount > 0)
            {
                DS_GPQUERY.DataSet.First();
                while (!(DS_GPQUERY.DataSet.Eof))
                {
                    GPQUERY.GPQUERY_DESCRIPTION = DS_GPQUERY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPQUERY.GPQUERY_DESCRIPTION.FieldName).asString();
                    GPQUERY.GPQUERY_NAME = DS_GPQUERY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPQUERY.GPQUERY_NAME.FieldName).asString();
                    GPQUERY.IDATUSER = DS_GPQUERY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPQUERY.IDATUSER.FieldName).asInt32();
                    GPQUERY.IDGPQUERY = DS_GPQUERY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPQUERY.IDGPQUERY.FieldName).asInt32();
                    GPQUERY.IDGPQUERYTYPE = DS_GPQUERY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPQUERY.IDGPQUERYTYPE.FieldName).asInt32();
                    GPQUERY.OPEN_ID = DS_GPQUERY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPQUERY.OPEN_ID.FieldName).asString();
                    GPQUERY.OPEN_PREFIX = DS_GPQUERY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPQUERY.OPEN_PREFIX.FieldName).asString();
                    GPQUERY.OPEN_TYPENAME = DS_GPQUERY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPQUERY.OPEN_TYPENAME.FieldName).asString();
                    GPQUERY.PATH = DS_GPQUERY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPQUERY.PATH.FieldName).asString();
                    GPQUERY.STRSQL = DS_GPQUERY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPQUERY.STRSQL.FieldName).asString();

                    DS_GPQUERY.DataSet.Next();
                }
            }
            else
            {
                DS_GPQUERY.ResErr.NotError = false;
                DS_GPQUERY.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return GPQUERY;
}
Persistence.GP.Methods.GPQUERY_GETID = function (GPQUERY)
{
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPQUERY.IDGPQUERY.FieldName, GPQUERY.IDGPQUERY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.GPQUERY.IDGPQUERYTYPE.FieldName, GPQUERY.IDGPQUERYTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPQUERY.GPQUERY_NAME.FieldName, GPQUERY.GPQUERY_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddText(UsrCfg.InternoAtisNames.GPQUERY.GPQUERY_DESCRIPTION.FieldName, GPQUERY.GPQUERY_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddText(UsrCfg.InternoAtisNames.GPQUERY.STRSQL.FieldName, GPQUERY.STRSQL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddText(UsrCfg.InternoAtisNames.GPQUERY.PATH.FieldName, GPQUERY.PATH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPQUERY.OPEN_TYPENAME.FieldName, GPQUERY.OPEN_TYPENAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPQUERY.OPEN_PREFIX.FieldName, GPQUERY.OPEN_PREFIX, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPQUERY.OPEN_ID.FieldName, GPQUERY.OPEN_ID, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.GPQUERY.IDATUSER.FieldName, GPQUERY.IDATUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.GPQUERY.START.FieldName, GPQUERY.START, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

        /*
        //********   GPQUERY_GETID   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPQUERY_GETID", @"GPQUERY_GETID description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   FROM GPQUERY "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPQUERYTYPE=@[IDGPQUERYTYPE] And "); 
         UnSQL.SQL.Add(@"   GPQUERY_NAME=@[GPQUERY_NAME] And "); 
         UnSQL.SQL.Add(@"   OPEN_TYPENAME=@[OPEN_TYPENAME] And "); 
         UnSQL.SQL.Add(@"   OPEN_PREFIX=@[OPEN_PREFIX] And "); 
         UnSQL.SQL.Add(@"   OPEN_ID=@[OPEN_ID] And "); 
         UnSQL.SQL.Add(@"   IDATUSER=@[IDATUSER] And "); 
         UnSQL.SQL.Add(@"   START=@[START] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_GPQUERY = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "GPQUERY_GETID", Param.ToBytes());
        ResErr = DS_GPQUERY.ResErr;
        if (ResErr.NotError) {
            if (DS_GPQUERY.DataSet.RecordCount > 0) {
                DS_GPQUERY.DataSet.First();
                GPQUERY.IDGPQUERY = DS_GPQUERY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPQUERY.IDGPQUERY.FieldName).asInt32();
            }
            else {
                DS_GPQUERY.ResErr.NotError = false;
                DS_GPQUERY.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPQUERY_ADD = function (GPQUERY) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPQUERY.IDGPQUERY.FieldName, GPQUERY.IDGPQUERY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.GPQUERY.IDGPQUERYTYPE.FieldName, GPQUERY.IDGPQUERYTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPQUERY.GPQUERY_NAME.FieldName, GPQUERY.GPQUERY_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.GPQUERY.GPQUERY_DESCRIPTION.FieldName, GPQUERY.GPQUERY_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.GPQUERY.STRSQL.FieldName, GPQUERY.STRSQL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.GPQUERY.PATH.FieldName, GPQUERY.PATH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPQUERY.OPEN_TYPENAME.FieldName, GPQUERY.OPEN_TYPENAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPQUERY.OPEN_PREFIX.FieldName, GPQUERY.OPEN_PREFIX, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPQUERY.OPEN_ID.FieldName, GPQUERY.OPEN_ID, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.GPQUERY.IDATUSER.FieldName, GPQUERY.IDATUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.GPQUERY.START.FieldName, GPQUERY.START, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        /* 
        //********   GPQUERY_ADD   ************************* 
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPQUERY_ADD", @"GPQUERY_ADD description."); 
        UnSQL.SQL.Add(@"   INSERT INTO GPQUERY( "); 
         UnSQL.SQL.Add(@"   IDGPQUERYTYPE, "); 
         UnSQL.SQL.Add(@"   GPQUERY_NAME, "); 
         UnSQL.SQL.Add(@"   GPQUERY_DESCRIPTION, "); 
         UnSQL.SQL.Add(@"   STRSQL, "); 
         UnSQL.SQL.Add(@"   PATH, "); 
         UnSQL.SQL.Add(@"   OPEN_TYPENAME, "); 
         UnSQL.SQL.Add(@"   OPEN_PREFIX, "); 
         UnSQL.SQL.Add(@"   OPEN_ID, "); 
         UnSQL.SQL.Add(@"   IDATUSER, "); 
         UnSQL.SQL.Add(@"   START "); 
         UnSQL.SQL.Add(@"   ) Values ( ");  
        UnSQL.SQL.Add(@"   @[IDGPQUERYTYPE], "); 
         UnSQL.SQL.Add(@"   @[GPQUERY_NAME], "); 
         UnSQL.SQL.Add(@"   @[GPQUERY_DESCRIPTION], "); 
         UnSQL.SQL.Add(@"   @[STRSQL], "); 
         UnSQL.SQL.Add(@"   @[PATH], "); 
         UnSQL.SQL.Add(@"   @[OPEN_TYPENAME], "); 
         UnSQL.SQL.Add(@"   @[OPEN_PREFIX], "); 
         UnSQL.SQL.Add(@"   @[OPEN_ID], "); 
         UnSQL.SQL.Add(@"   @[IDATUSER], "); 
         UnSQL.SQL.Add(@"   @[START] "); 
         UnSQL.SQL.Add(@"   )  "); 
        UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPQUERY_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.GP.Methods.GPQUERY_GETID(GPQUERY);
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPQUERY_UPD = function (GPQUERY) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPQUERY.IDGPQUERY.FieldName, GPQUERY.IDGPQUERY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.GPQUERY.IDGPQUERYTYPE.FieldName, GPQUERY.IDGPQUERYTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPQUERY.GPQUERY_NAME.FieldName, GPQUERY.GPQUERY_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.GPQUERY.GPQUERY_DESCRIPTION.FieldName, GPQUERY.GPQUERY_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.GPQUERY.STRSQL.FieldName, GPQUERY.STRSQL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.GPQUERY.PATH.FieldName, GPQUERY.PATH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPQUERY.OPEN_TYPENAME.FieldName, GPQUERY.OPEN_TYPENAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPQUERY.OPEN_PREFIX.FieldName, GPQUERY.OPEN_PREFIX, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPQUERY.OPEN_ID.FieldName, GPQUERY.OPEN_ID, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.GPQUERY.IDATUSER.FieldName, GPQUERY.IDATUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.GPQUERY.START.FieldName, GPQUERY.START, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);


        /*   
        //********   GPQUERY_UPD   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPQUERY_UPD", @"GPQUERY_UPD description.");  
        UnSQL.SQL.Add(@"  UPDATE GPQUERY Set "); 
         UnSQL.SQL.Add(@"   IDGPQUERYTYPE=@[IDGPQUERYTYPE], "); 
         UnSQL.SQL.Add(@"   GPQUERY_NAME=@[GPQUERY_NAME], "); 
         UnSQL.SQL.Add(@"   GPQUERY_DESCRIPTION=@[GPQUERY_DESCRIPTION], "); 
         UnSQL.SQL.Add(@"   STRSQL=@[STRSQL], "); 
         UnSQL.SQL.Add(@"   PATH=@[PATH], "); 
         UnSQL.SQL.Add(@"   OPEN_TYPENAME=@[OPEN_TYPENAME], "); 
         UnSQL.SQL.Add(@"   OPEN_PREFIX=@[OPEN_PREFIX], "); 
         UnSQL.SQL.Add(@"   OPEN_ID=@[OPEN_ID], "); 
         UnSQL.SQL.Add(@"   IDATUSER=@[IDATUSER], "); 
         UnSQL.SQL.Add(@"   START=@[START] "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPQUERY=@[IDGPQUERY] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPQUERY_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.GP.Methods.GPQUERY_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.GP.Methods.GPQUERY_DELIDGPQUERY(/*String StrIDGPQUERY*/Param);
    }
    else {
        Res = Persistence.GP.Methods.GPQUERY_DELGPQUERY(Param/*GPQUERYList*/);
    }
    return (Res);
}

Persistence.GP.Methods.GPQUERY_DELIDGPQUERY = function (/*String StrIDGPQUERY*/IDGPQUERY) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDGPQUERY == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPQUERY.IDGPQUERY.FieldName, " = " + UsrCfg.InternoAtisNames.GPQUERY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.GPQUERY.IDGPQUERY.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDGPQUERY = " IN (" + StrIDGPQUERY + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPQUERY.IDGPQUERY.FieldName, StrIDGPQUERY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.GPQUERY.IDGPQUERY.FieldName, IDGPQUERY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   GPQUERY_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPQUERY_DEL", @"GPQUERY_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE GPQUERY "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPQUERY=@[IDGPQUERY]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPQUERY_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPQUERY_DELGPQUERY = function (GPQUERY/*GPQUERYList*/) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        /*
        StrIDGPQUERY = "";
        StrIDOTRO = "";
        for (var i = 0; i < GPQUERYList.length; i++) {
        StrIDGPQUERY += GPQUERYList[i].IDGPQUERY + ",";
        StrIDOTRO += GPQUERYList[i].IDOTRO + ",";
        }
        StrIDGPQUERY = StrIDGPQUERY.substring(0, StrIDGPQUERY.length - 1);
        StrIDOTRO = StrIDOTRO.substring(0, StrIDOTRO.length - 1);
        
        
        if (StrIDGPQUERY == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPQUERY.IDGPQUERY.FieldName, " = " + UsrCfg.InternoAtisNames.GPQUERY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.GPQUERY.IDGPQUERY.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDGPQUERY = " IN (" + StrIDGPQUERY + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPQUERY.IDGPQUERY.FieldName, StrIDGPQUERY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/


        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.GPQUERY.IDGPQUERY.FieldName, GPQUERY.IDGPQUERY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*    
        //********   GPQUERY_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPQUERY_DEL", @"GPQUERY_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE GPQUERY "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPQUERY=@[IDGPQUERY]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPQUERY_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

Persistence.GP.Methods.GPQUERY_COMPLETE = function (GPQUERY)
{
    var GPVIEWList = Persistence.GP.Methods.GPVIEW_GET_BY_GPQUERY(GPQUERY.IDGPQUERY);
    for (var i = 0; i < GPVIEWList.length; i++)
    {
        var GPCOLUMNVIEWList = new Array();
        Persistence.GP.Methods.GPCOLUMNVIEW_GET_BY_GPVIEW(GPCOLUMNVIEWList, GPVIEWList[i].IDGPVIEW);

        for (var j = 0; j < GPCOLUMNVIEWList.length; j++)
        {
            var GPVALUECOLORList = new Array();
            Persistence.GP.Methods.GPVALUECOLOR_GET_BY_GPCOLUMNVIEW(GPVALUECOLORList, GPCOLUMNVIEWList[j].IDGPCOLUMNVIEW);
            GPCOLUMNVIEWList[j].GPVALUECOLORList = GPVALUECOLORList;
        }
        GPVIEWList[i].GPCOLUMNVIEWList = GPCOLUMNVIEWList;
    }

    GPQUERY.GPVIEWList = GPVIEWList;

    /////////////////////////////////////////////////

    var GPEDIT_COLUMN_List = new Array();
    Persistence.GP.Methods.GPEDIT_COLUMN_GET_BY_GPQUERY(GPEDIT_COLUMN_List, GPQUERY.IDGPQUERY);

    GPQUERY.GPEDIT_COLUMN_List = GPEDIT_COLUMN_List;

    ///////////////////////////////////////////////////

    var GPEDIT_TABLE_List = new Array();
    Persistence.GP.Methods.GPEDIT_TABLE_GET_BY_GPQUERY(GPEDIT_TABLE_List, GPQUERY.IDGPQUERY);
    for (var i = 0; i < GPEDIT_TABLE_List.length; i++)
    {
        var GPEDIT_COLUMNEVENT_List = new Array();
        Persistence.GP.Methods.GPEDIT_COLUMNEVENT_GET_BY_GPEDIT_TABLE(GPEDIT_COLUMNEVENT_List, GPEDIT_TABLE_List[i].IDGPEDIT_TABLE);
        GPEDIT_TABLE_List[i].GPEDIT_COLUMNEVENT_List = GPEDIT_COLUMNEVENT_List;
    }

    GPQUERY.GPEDIT_TABLE_List = GPEDIT_TABLE_List;

    /////////////////////////////////////////////////

    return GPQUERY;
}


//**********************   METHODS   ********************************************************************************************
Persistence.GP.Methods.GPQUERY_ListSetID = function (GPQUERYList, IDGPQUERY) {
    for (i = 0; i < GPQUERYList.length; i++) {
        if (IDGPQUERY == GPQUERYList[i].IDGPQUERY)
            return (GPQUERYList[i]);
    }
    var UnGPQUERY = new Persistence.Properties.TGPQUERY;
    UnGPQUERY.IDGPQUERY = IDGPQUERY;
    return (UnGPQUERY);
}
Persistence.GP.Methods.GPQUERY_ListAdd = function (GPQUERYList, GPQUERY) {
    var i = Persistence.GP.Methods.GPQUERY_ListGetIndex(GPQUERYList, GPQUERY);
    if (i == -1) GPQUERYList.push(GPQUERY);
    else {
        GPQUERYList[i].IDGPQUERY = GPQUERY.IDGPQUERY;
        GPQUERYList[i].IDGPQUERYTYPE = GPQUERY.IDGPQUERYTYPE;
        GPQUERYList[i].GPQUERY_NAME = GPQUERY.GPQUERY_NAME;
        GPQUERYList[i].GPQUERY_DESCRIPTION = GPQUERY.GPQUERY_DESCRIPTION;
        GPQUERYList[i].STRSQL = GPQUERY.STRSQL;
        GPQUERYList[i].PATH = GPQUERY.PATH;
        GPQUERYList[i].OPEN_TYPENAME = GPQUERY.OPEN_TYPENAME;
        GPQUERYList[i].OPEN_PREFIX = GPQUERY.OPEN_PREFIX;
        GPQUERYList[i].OPEN_ID = GPQUERY.OPEN_ID;
        GPQUERYList[i].IDATUSER = GPQUERY.IDATUSER;
        GPQUERYList[i].START = GPQUERY.START;

    }
}
//CJRC_26072018
Persistence.GP.Methods.GPQUERY_ListGetIndex = function (GPQUERYList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.GP.Methods.GPQUERY_ListGetIndexIDGPQUERY(GPQUERYList, Param);

    }
    else {
        Res = Persistence.GP.Methods.GPQUERY_ListGetIndexGPQUERY(GPQUERYList, Param);
    }
    return (Res);
}

Persistence.GP.Methods.GPQUERY_ListGetIndexGPQUERY = function (GPQUERYList, GPQUERY) {
    for (i = 0; i < GPQUERYList.length; i++) {
        if (GPQUERY.IDGPQUERY == GPQUERYList[i].IDGPQUERY)
            return (i);
    }
    return (-1);
}
Persistence.GP.Methods.GPQUERY_ListGetIndexIDGPQUERY = function (GPQUERYList, IDGPQUERY) {
    for (i = 0; i < GPQUERYList.length; i++) {
        if (IDGPQUERY == GPQUERYList[i].IDGPQUERY)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.GP.Methods.GPQUERYListtoStr = function (GPQUERYList) {
    var Res = Persistence.Properties._Version + "(1)0";
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(GPQUERYList.length, Longitud, Texto);
        for (Counter = 0; Counter < GPQUERYList.length; Counter++) {
            var UnGPQUERY = GPQUERYList[Counter];
            UnGPQUERY.ToStr(Longitud, Texto);
        }

        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
}
Persistence.GP.Methods.StrtoGPQUERY = function (ProtocoloStr, GPQUERYList) {

    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        GPQUERYList.length = 0;
        if (Persistence.GP.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")") - ProtocoloStr.indexOf("(") - 1);
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.Length - ProtocoloStr.indexOf(")") - 1);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnGPQUERY = new Persistence.Properties.TGPQUERY(); //Mode new row
                UnGPQUERY.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.GP.GPQUERY_ListAdd(GPQUERYList, UnGPQUERY);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}


//Socket Function 
Persistence.GP.Methods.GPQUERYListToByte = function (GPQUERYList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, GPQUERYList.Count - idx);
    for (i = idx; i < GPQUERYList.length; i++) {
        GPQUERYList[i].ToByte(MemStream);
    }
}
Persistence.GP.Methods.ByteToGPQUERYList = function (GPQUERYList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnGPQUERY = new Persistence.Properties.TGPQUERY();
        UnGPQUERY.ByteTo(MemStream);
        Methods.GPQUERY_ListAdd(GPQUERYList, UnGPQUERY);
    }
}
