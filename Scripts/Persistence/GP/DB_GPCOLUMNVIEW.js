﻿//**********************   PROPERTIES   *****************************************************************************************
Persistence.GP.Properties.TGPCOLUMNVIEW = function () {
    this.IDGPCOLUMNVIEW = 0;
    this.IDGPVIEW = 0;
    this.TABLE_NAME = "";
    this.FIELD_NAME = "";
    this.HEADER = "";
    this.POSITION = 0;
    this.SIZE = 0;
    this.ISVISIBLE = "";
    this.ISVISIBLEDETAIL = "";
    this.COLOR = "";
    this.ISGROUP = "";
    this.GROUP_POSITION = 0;
    this.SORT = "";

    this.GPVALUECOLORList = new Array();


    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDGPCOLUMNVIEW);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDGPVIEW);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.TABLE_NAME);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.FIELD_NAME);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.HEADER);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.POSITION);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.SIZE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.ISVISIBLE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.ISVISIBLEDETAIL);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.COLOR);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.ISGROUP);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.GROUP_POSITION);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.SORT);

    }
    this.ByteTo = function (MemStream) {
        this.IDGPCOLUMNVIEW = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDGPVIEW = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.TABLE_NAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.FIELD_NAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.HEADER = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.POSITION = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.SIZE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.ISVISIBLE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.ISVISIBLEDETAIL = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.COLOR = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.ISGROUP = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.GROUP_POSITION = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.SORT = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);

    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDGPCOLUMNVIEW, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDGPVIEW, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.TABLE_NAME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.FIELD_NAME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.HEADER, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.POSITION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.SIZE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.ISVISIBLE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.ISVISIBLEDETAIL, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.COLOR, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.ISGROUP, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.GROUP_POSITION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.SORT, Longitud, Texto);

    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDGPCOLUMNVIEW = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDGPVIEW = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.TABLE_NAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.FIELD_NAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.HEADER = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.POSITION = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.SIZE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.ISVISIBLE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.ISVISIBLEDETAIL = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.COLOR = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.ISGROUP = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.GROUP_POSITION = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.SORT = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);

    }
}

Persistence.GP.Properties.TGPCOLUMNVIEW = function (COLOR, FIELD_NAME, GROUP_POSITION, HEADER, ISGROUP, ISVISIBLE, ISVISIBLEDETAIL, POSITION, SIZE, SORT, VISIBLE, VISIBLEDETAIL)
{
    this.IDGPCOLUMNVIEW = 0;
    this.IDGPVIEW = 0;
    this.TABLE_NAME = "";
    this.FIELD_NAME = FIELD_NAME;
    this.HEADER = HEADER;
    this.POSITION = POSITION;
    this.SIZE = SIZE;
    this.ISVISIBLE = ISVISIBLE;
    this.ISVISIBLEDETAIL = ISVISIBLEDETAIL;
    this.COLOR = COLOR;
    this.ISGROUP = ISGROUP;
    this.GROUP_POSITION = GROUP_POSITION;
    this.SORT = SORT;

    this.GPVALUECOLORList = new Array();
}

//**********************   METHODS server  ********************************************************************************************
Persistence.GP.Methods.GPCOLUMNVIEW_Fill = function (GPCOLUMNVIEWList, StrIDGPCOLUMNVIEW/*noin IDGPCOLUMNVIEW*/) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    GPCOLUMNVIEWList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDGPCOLUMNVIEW == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.IDGPCOLUMNVIEW.FieldName, " = " + UsrCfg.InternoAtisNames.GPCOLUMNVIEW.NAME_TABLE + "." + UsrCfg.InternoAtisNames.GPCOLUMNVIEW.IDGPCOLUMNVIEW.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDGPCOLUMNVIEW = " IN (" + StrIDGPCOLUMNVIEW + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.IDGPCOLUMNVIEW.FieldName, StrIDGPCOLUMNVIEW, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //noin Param.AddInt32(UsrCfg.InternoDemoNames.GPCOLUMNVIEW.IDGPCOLUMNVIEW.FieldName, IDGPCOLUMNVIEW, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {
        /*
        //********   GPCOLUMNVIEW_GET   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPCOLUMNVIEW_GET", @"GPCOLUMNVIEW_GET description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   IDGPCOLUMNVIEW, "); 
         UnSQL.SQL.Add(@"   IDGPVIEW, "); 
         UnSQL.SQL.Add(@"   TABLE_NAME, "); 
         UnSQL.SQL.Add(@"   FIELD_NAME, "); 
         UnSQL.SQL.Add(@"   HEADER, "); 
         UnSQL.SQL.Add(@"   POSITION, "); 
         UnSQL.SQL.Add(@"   SIZE, "); 
         UnSQL.SQL.Add(@"   ISVISIBLE, "); 
         UnSQL.SQL.Add(@"   ISVISIBLEDETAIL, "); 
         UnSQL.SQL.Add(@"   COLOR, "); 
         UnSQL.SQL.Add(@"   ISGROUP, "); 
         UnSQL.SQL.Add(@"   GROUP_POSITION, "); 
         UnSQL.SQL.Add(@"   SORT "); 
         UnSQL.SQL.Add(@"   FROM GPCOLUMNVIEW "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPCOLUMNVIEW=@[IDGPCOLUMNVIEW] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_GPCOLUMNVIEW = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "GPCOLUMNVIEW_GET", Param.ToBytes());
        ResErr = DS_GPCOLUMNVIEW.ResErr;
        if (ResErr.NotError) {
            if (DS_GPCOLUMNVIEW.DataSet.RecordCount > 0) {
                DS_GPCOLUMNVIEW.DataSet.First();
                while (!(DS_GPCOLUMNVIEW.DataSet.Eof)) {
                    var GPCOLUMNVIEW = new Persistence.GP.Properties.TGPCOLUMNVIEW();
                    GPCOLUMNVIEW.IDGPCOLUMNVIEW = DS_GPCOLUMNVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.IDGPCOLUMNVIEW.FieldName).asInt32();
                    GPCOLUMNVIEW.IDGPVIEW = DS_GPCOLUMNVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.IDGPVIEW.FieldName).asInt32();
                    GPCOLUMNVIEW.TABLE_NAME = DS_GPCOLUMNVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.TABLE_NAME.FieldName).asString();
                    GPCOLUMNVIEW.FIELD_NAME = DS_GPCOLUMNVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.FIELD_NAME.FieldName).asString();
                    GPCOLUMNVIEW.HEADER = DS_GPCOLUMNVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.HEADER.FieldName).asString();
                    GPCOLUMNVIEW.POSITION = DS_GPCOLUMNVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.POSITION.FieldName).asInt32();
                    GPCOLUMNVIEW.SIZE = DS_GPCOLUMNVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.SIZE.FieldName).asInt32();
                    GPCOLUMNVIEW.ISVISIBLE = DS_GPCOLUMNVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.ISVISIBLE.FieldName).asString();
                    GPCOLUMNVIEW.ISVISIBLEDETAIL = DS_GPCOLUMNVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.ISVISIBLEDETAIL.FieldName).asString();
                    GPCOLUMNVIEW.COLOR = DS_GPCOLUMNVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.COLOR.FieldName).asString();
                    GPCOLUMNVIEW.ISGROUP = DS_GPCOLUMNVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.ISGROUP.FieldName).asString();
                    GPCOLUMNVIEW.GROUP_POSITION = DS_GPCOLUMNVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.GROUP_POSITION.FieldName).asInt32();
                    GPCOLUMNVIEW.SORT = DS_GPCOLUMNVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.SORT.FieldName).asString();

                    //Other
                    GPCOLUMNVIEWList.push(GPCOLUMNVIEW);
                    DS_GPCOLUMNVIEW.DataSet.Next();
                }
            }
            else {
                DS_GPCOLUMNVIEW.ResErr.NotError = false;
                DS_GPCOLUMNVIEW.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPCOLUMNVIEW_GETID = function (GPCOLUMNVIEW) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.IDGPCOLUMNVIEW.FieldName, GPCOLUMNVIEW.IDGPCOLUMNVIEW, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.IDGPVIEW.FieldName, GPCOLUMNVIEW.IDGPVIEW, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.TABLE_NAME.FieldName, GPCOLUMNVIEW.TABLE_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.FIELD_NAME.FieldName, GPCOLUMNVIEW.FIELD_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.HEADER.FieldName, GPCOLUMNVIEW.HEADER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.POSITION.FieldName, GPCOLUMNVIEW.POSITION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.SIZE.FieldName, GPCOLUMNVIEW.SIZE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.ISVISIBLE.FieldName, GPCOLUMNVIEW.ISVISIBLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.ISVISIBLEDETAIL.FieldName, GPCOLUMNVIEW.ISVISIBLEDETAIL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.COLOR.FieldName, GPCOLUMNVIEW.COLOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.ISGROUP.FieldName, GPCOLUMNVIEW.ISGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.GROUP_POSITION.FieldName, GPCOLUMNVIEW.GROUP_POSITION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.SORT.FieldName, GPCOLUMNVIEW.SORT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

        /*
        //********   GPCOLUMNVIEW_GETID   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPCOLUMNVIEW_GETID", @"GPCOLUMNVIEW_GETID description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   FROM GPCOLUMNVIEW "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPVIEW=@[IDGPVIEW] And "); 
         UnSQL.SQL.Add(@"   TABLE_NAME=@[TABLE_NAME] And "); 
         UnSQL.SQL.Add(@"   FIELD_NAME=@[FIELD_NAME] And "); 
         UnSQL.SQL.Add(@"   HEADER=@[HEADER] And "); 
         UnSQL.SQL.Add(@"   POSITION=@[POSITION] And "); 
         UnSQL.SQL.Add(@"   SIZE=@[SIZE] And "); 
         UnSQL.SQL.Add(@"   ISVISIBLE=@[ISVISIBLE] And "); 
         UnSQL.SQL.Add(@"   ISVISIBLEDETAIL=@[ISVISIBLEDETAIL] And "); 
         UnSQL.SQL.Add(@"   COLOR=@[COLOR] And "); 
         UnSQL.SQL.Add(@"   ISGROUP=@[ISGROUP] And "); 
         UnSQL.SQL.Add(@"   GROUP_POSITION=@[GROUP_POSITION] And "); 
         UnSQL.SQL.Add(@"   SORT=@[SORT] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_GPCOLUMNVIEW = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "GPCOLUMNVIEW_GETID", Param.ToBytes());
        ResErr = DS_GPCOLUMNVIEW.ResErr;
        if (ResErr.NotError) {
            if (DS_GPCOLUMNVIEW.DataSet.RecordCount > 0) {
                DS_GPCOLUMNVIEW.DataSet.First();
                GPCOLUMNVIEW.IDGPCOLUMNVIEW = DS_GPCOLUMNVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.IDGPCOLUMNVIEW.FieldName).asInt32();
            }
            else {
                DS_GPCOLUMNVIEW.ResErr.NotError = false;
                DS_GPCOLUMNVIEW.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPCOLUMNVIEW_ADD = function (GPCOLUMNVIEW) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.IDGPCOLUMNVIEW.FieldName, GPCOLUMNVIEW.IDGPCOLUMNVIEW, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.IDGPVIEW.FieldName, GPCOLUMNVIEW.IDGPVIEW, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.TABLE_NAME.FieldName, GPCOLUMNVIEW.TABLE_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.FIELD_NAME.FieldName, GPCOLUMNVIEW.FIELD_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.HEADER.FieldName, GPCOLUMNVIEW.HEADER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.POSITION.FieldName, GPCOLUMNVIEW.POSITION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.SIZE.FieldName, GPCOLUMNVIEW.SIZE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.ISVISIBLE.FieldName, GPCOLUMNVIEW.ISVISIBLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.ISVISIBLEDETAIL.FieldName, GPCOLUMNVIEW.ISVISIBLEDETAIL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.COLOR.FieldName, GPCOLUMNVIEW.COLOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.ISGROUP.FieldName, GPCOLUMNVIEW.ISGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.GROUP_POSITION.FieldName, GPCOLUMNVIEW.GROUP_POSITION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.SORT.FieldName, GPCOLUMNVIEW.SORT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        /* 
        //********   GPCOLUMNVIEW_ADD   ************************* 
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPCOLUMNVIEW_ADD", @"GPCOLUMNVIEW_ADD description."); 
        UnSQL.SQL.Add(@"   INSERT INTO GPCOLUMNVIEW( "); 
         UnSQL.SQL.Add(@"   IDGPCOLUMNVIEW, "); 
         UnSQL.SQL.Add(@"   IDGPVIEW, "); 
         UnSQL.SQL.Add(@"   TABLE_NAME, "); 
         UnSQL.SQL.Add(@"   FIELD_NAME, "); 
         UnSQL.SQL.Add(@"   HEADER, "); 
         UnSQL.SQL.Add(@"   POSITION, "); 
         UnSQL.SQL.Add(@"   SIZE, "); 
         UnSQL.SQL.Add(@"   ISVISIBLE, "); 
         UnSQL.SQL.Add(@"   ISVISIBLEDETAIL, "); 
         UnSQL.SQL.Add(@"   COLOR, "); 
         UnSQL.SQL.Add(@"   ISGROUP, "); 
         UnSQL.SQL.Add(@"   GROUP_POSITION, "); 
         UnSQL.SQL.Add(@"   SORT "); 
         UnSQL.SQL.Add(@"   ) Values ( ");  
        UnSQL.SQL.Add(@"   @[IDGPCOLUMNVIEW], "); 
         UnSQL.SQL.Add(@"   @[IDGPVIEW], "); 
         UnSQL.SQL.Add(@"   @[TABLE_NAME], "); 
         UnSQL.SQL.Add(@"   @[FIELD_NAME], "); 
         UnSQL.SQL.Add(@"   @[HEADER], "); 
         UnSQL.SQL.Add(@"   @[POSITION], "); 
         UnSQL.SQL.Add(@"   @[SIZE], "); 
         UnSQL.SQL.Add(@"   @[ISVISIBLE], "); 
         UnSQL.SQL.Add(@"   @[ISVISIBLEDETAIL], "); 
         UnSQL.SQL.Add(@"   @[COLOR], "); 
         UnSQL.SQL.Add(@"   @[ISGROUP], "); 
         UnSQL.SQL.Add(@"   @[GROUP_POSITION], "); 
         UnSQL.SQL.Add(@"   @[SORT] "); 
         UnSQL.SQL.Add(@"   )  "); 
        UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPCOLUMNVIEW_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.GP.Methods.GPCOLUMNVIEW_GETID(GPCOLUMNVIEW);
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPCOLUMNVIEW_UPD = function (GPCOLUMNVIEW) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.IDGPCOLUMNVIEW.FieldName, GPCOLUMNVIEW.IDGPCOLUMNVIEW, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.IDGPVIEW.FieldName, GPCOLUMNVIEW.IDGPVIEW, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.TABLE_NAME.FieldName, GPCOLUMNVIEW.TABLE_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.FIELD_NAME.FieldName, GPCOLUMNVIEW.FIELD_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.HEADER.FieldName, GPCOLUMNVIEW.HEADER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.POSITION.FieldName, GPCOLUMNVIEW.POSITION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.SIZE.FieldName, GPCOLUMNVIEW.SIZE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.ISVISIBLE.FieldName, GPCOLUMNVIEW.ISVISIBLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.ISVISIBLEDETAIL.FieldName, GPCOLUMNVIEW.ISVISIBLEDETAIL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.COLOR.FieldName, GPCOLUMNVIEW.COLOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.ISGROUP.FieldName, GPCOLUMNVIEW.ISGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.GROUP_POSITION.FieldName, GPCOLUMNVIEW.GROUP_POSITION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.SORT.FieldName, GPCOLUMNVIEW.SORT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);


        /*   
        //********   GPCOLUMNVIEW_UPD   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPCOLUMNVIEW_UPD", @"GPCOLUMNVIEW_UPD description.");  
        UnSQL.SQL.Add(@"  UPDATE GPCOLUMNVIEW Set "); 
         UnSQL.SQL.Add(@"   IDGPVIEW=@[IDGPVIEW], "); 
         UnSQL.SQL.Add(@"   TABLE_NAME=@[TABLE_NAME], "); 
         UnSQL.SQL.Add(@"   FIELD_NAME=@[FIELD_NAME], "); 
         UnSQL.SQL.Add(@"   HEADER=@[HEADER], "); 
         UnSQL.SQL.Add(@"   POSITION=@[POSITION], "); 
         UnSQL.SQL.Add(@"   SIZE=@[SIZE], "); 
         UnSQL.SQL.Add(@"   ISVISIBLE=@[ISVISIBLE], "); 
         UnSQL.SQL.Add(@"   ISVISIBLEDETAIL=@[ISVISIBLEDETAIL], "); 
         UnSQL.SQL.Add(@"   COLOR=@[COLOR], "); 
         UnSQL.SQL.Add(@"   ISGROUP=@[ISGROUP], "); 
         UnSQL.SQL.Add(@"   GROUP_POSITION=@[GROUP_POSITION], "); 
         UnSQL.SQL.Add(@"   SORT=@[SORT] "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPCOLUMNVIEW=@[IDGPCOLUMNVIEW] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPCOLUMNVIEW_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.GP.Methods.GPCOLUMNVIEW_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.GP.Methods.GPCOLUMNVIEW_DELIDGPCOLUMNVIEW(/*String StrIDGPCOLUMNVIEW*/Param);
    }
    else {
        Res = Persistence.GP.Methods.GPCOLUMNVIEW_DELGPCOLUMNVIEW(Param/*GPCOLUMNVIEWList*/);
    }
    return (Res);
}

Persistence.GP.Methods.GPCOLUMNVIEW_DELIDGPCOLUMNVIEW = function (/*String StrIDGPCOLUMNVIEW*/IDGPCOLUMNVIEW) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDGPCOLUMNVIEW == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.IDGPCOLUMNVIEW.FieldName, " = " + UsrCfg.InternoAtisNames.GPCOLUMNVIEW.NAME_TABLE + "." + UsrCfg.InternoAtisNames.GPCOLUMNVIEW.IDGPCOLUMNVIEW.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDGPCOLUMNVIEW = " IN (" + StrIDGPCOLUMNVIEW + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.IDGPCOLUMNVIEW.FieldName, StrIDGPCOLUMNVIEW, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.IDGPCOLUMNVIEW.FieldName, IDGPCOLUMNVIEW, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   GPCOLUMNVIEW_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPCOLUMNVIEW_DEL", @"GPCOLUMNVIEW_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE GPCOLUMNVIEW "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPCOLUMNVIEW=@[IDGPCOLUMNVIEW]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPCOLUMNVIEW_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPCOLUMNVIEW_DELGPCOLUMNVIEW = function (GPCOLUMNVIEW/*GPCOLUMNVIEWList*/) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        /*
        StrIDGPCOLUMNVIEW = "";
        StrIDOTRO = "";
        for (var i = 0; i < GPCOLUMNVIEWList.length; i++) {
        StrIDGPCOLUMNVIEW += GPCOLUMNVIEWList[i].IDGPCOLUMNVIEW + ",";
        StrIDOTRO += GPCOLUMNVIEWList[i].IDOTRO + ",";
        }
        StrIDGPCOLUMNVIEW = StrIDGPCOLUMNVIEW.substring(0, StrIDGPCOLUMNVIEW.length - 1);
        StrIDOTRO = StrIDOTRO.substring(0, StrIDOTRO.length - 1);
        
        
        if (StrIDGPCOLUMNVIEW == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.IDGPCOLUMNVIEW.FieldName, " = " + UsrCfg.InternoAtisNames.GPCOLUMNVIEW.NAME_TABLE + "." + UsrCfg.InternoAtisNames.GPCOLUMNVIEW.IDGPCOLUMNVIEW.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDGPCOLUMNVIEW = " IN (" + StrIDGPCOLUMNVIEW + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.IDGPCOLUMNVIEW.FieldName, StrIDGPCOLUMNVIEW, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/


        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.IDGPCOLUMNVIEW.FieldName, GPCOLUMNVIEW.IDGPCOLUMNVIEW, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*    
        //********   GPCOLUMNVIEW_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPCOLUMNVIEW_DEL", @"GPCOLUMNVIEW_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE GPCOLUMNVIEW "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPCOLUMNVIEW=@[IDGPCOLUMNVIEW]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPCOLUMNVIEW_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

Persistence.GP.Methods.GPCOLUMNVIEW_GET_BY_GPVIEW = function (GPCOLUMNVIEWList, IDGPVIEW)
{
    var ResErr = new SysCfg.Error.Properties.TResErr();
    GPCOLUMNVIEWList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();

    Param.AddInt32(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.IDGPVIEW.FieldName, IDGPVIEW, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

    try
    {
        var DS_GPCOLUMNVIEW = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "GPCOLUMNVIEW_SELECTBYVIEW", Param.ToBytes());
        ResErr = DS_GPCOLUMNVIEW.ResErr;
        if (ResErr.NotError)
        {
            if (DS_GPCOLUMNVIEW.DataSet.RecordCount > 0)
            {
                DS_GPCOLUMNVIEW.DataSet.First();
                while (!(DS_GPCOLUMNVIEW.DataSet.Eof))
                {
                    var GPCOLUMNVIEW = new Persistence.GP.Properties.TGPCOLUMNVIEW();
                    GPCOLUMNVIEW.IDGPCOLUMNVIEW = DS_GPCOLUMNVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.IDGPCOLUMNVIEW.FieldName).asInt32();
                    GPCOLUMNVIEW.IDGPVIEW = DS_GPCOLUMNVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.IDGPVIEW.FieldName).asInt32();
                    GPCOLUMNVIEW.TABLE_NAME = DS_GPCOLUMNVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.TABLE_NAME.FieldName).asString();
                    GPCOLUMNVIEW.FIELD_NAME = DS_GPCOLUMNVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.FIELD_NAME.FieldName).asString();
                    GPCOLUMNVIEW.HEADER = DS_GPCOLUMNVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.HEADER.FieldName).asString();
                    GPCOLUMNVIEW.POSITION = DS_GPCOLUMNVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.POSITION.FieldName).asInt32();
                    GPCOLUMNVIEW.SIZE = DS_GPCOLUMNVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.SIZE.FieldName).asInt32();
                    GPCOLUMNVIEW.ISVISIBLE = DS_GPCOLUMNVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.ISVISIBLE.FieldName).asString();
                    GPCOLUMNVIEW.ISVISIBLEDETAIL = DS_GPCOLUMNVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.ISVISIBLEDETAIL.FieldName).asString();
                    GPCOLUMNVIEW.COLOR = DS_GPCOLUMNVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.COLOR.FieldName).asString();
                    GPCOLUMNVIEW.ISGROUP = DS_GPCOLUMNVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.ISGROUP.FieldName).asString();
                    GPCOLUMNVIEW.GROUP_POSITION = DS_GPCOLUMNVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.GROUP_POSITION.FieldName).asInt32();
                    GPCOLUMNVIEW.SORT = DS_GPCOLUMNVIEW.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.SORT.FieldName).asString();

                    //Other
                    GPCOLUMNVIEWList.push(GPCOLUMNVIEW);
                    DS_GPCOLUMNVIEW.DataSet.Next();
                }
            }
            else
            {
                DS_GPCOLUMNVIEW.ResErr.NotError = false;
                DS_GPCOLUMNVIEW.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}

//**********************   METHODS   ********************************************************************************************
Persistence.GP.Methods.GPCOLUMNVIEW_ListSetID = function (GPCOLUMNVIEWList, IDGPCOLUMNVIEW) {
    for (i = 0; i < GPCOLUMNVIEWList.length; i++) {
        if (IDGPCOLUMNVIEW == GPCOLUMNVIEWList[i].IDGPCOLUMNVIEW)
            return (GPCOLUMNVIEWList[i]);
    }
    var UnGPCOLUMNVIEW = new Persistence.Properties.TGPCOLUMNVIEW;
    UnGPCOLUMNVIEW.IDGPCOLUMNVIEW = IDGPCOLUMNVIEW;
    return (UnGPCOLUMNVIEW);
}
Persistence.GP.Methods.GPCOLUMNVIEW_ListAdd = function (GPCOLUMNVIEWList, GPCOLUMNVIEW) {
    var i = Persistence.GP.Methods.GPCOLUMNVIEW_ListGetIndex(GPCOLUMNVIEWList, GPCOLUMNVIEW);
    if (i == -1) GPCOLUMNVIEWList.push(GPCOLUMNVIEW);
    else {
        GPCOLUMNVIEWList[i].IDGPCOLUMNVIEW = GPCOLUMNVIEW.IDGPCOLUMNVIEW;
        GPCOLUMNVIEWList[i].IDGPVIEW = GPCOLUMNVIEW.IDGPVIEW;
        GPCOLUMNVIEWList[i].TABLE_NAME = GPCOLUMNVIEW.TABLE_NAME;
        GPCOLUMNVIEWList[i].FIELD_NAME = GPCOLUMNVIEW.FIELD_NAME;
        GPCOLUMNVIEWList[i].HEADER = GPCOLUMNVIEW.HEADER;
        GPCOLUMNVIEWList[i].POSITION = GPCOLUMNVIEW.POSITION;
        GPCOLUMNVIEWList[i].SIZE = GPCOLUMNVIEW.SIZE;
        GPCOLUMNVIEWList[i].ISVISIBLE = GPCOLUMNVIEW.ISVISIBLE;
        GPCOLUMNVIEWList[i].ISVISIBLEDETAIL = GPCOLUMNVIEW.ISVISIBLEDETAIL;
        GPCOLUMNVIEWList[i].COLOR = GPCOLUMNVIEW.COLOR;
        GPCOLUMNVIEWList[i].ISGROUP = GPCOLUMNVIEW.ISGROUP;
        GPCOLUMNVIEWList[i].GROUP_POSITION = GPCOLUMNVIEW.GROUP_POSITION;
        GPCOLUMNVIEWList[i].SORT = GPCOLUMNVIEW.SORT;

    }
}
//CJRC_26072018
Persistence.GP.Methods.GPCOLUMNVIEW_ListGetIndex = function (GPCOLUMNVIEWList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.GP.Methods.GPCOLUMNVIEW_ListGetIndexIDGPCOLUMNVIEW(GPCOLUMNVIEWList, Param);

    }
    else {
        Res = Persistence.GP.Methods.GPCOLUMNVIEW_ListGetIndexGPCOLUMNVIEW(GPCOLUMNVIEWList, Param);
    }
    return (Res);
}

Persistence.GP.Methods.GPCOLUMNVIEW_ListGetIndexGPCOLUMNVIEW = function (GPCOLUMNVIEWList, GPCOLUMNVIEW) {
    for (i = 0; i < GPCOLUMNVIEWList.length; i++) {
        if (GPCOLUMNVIEW.IDGPCOLUMNVIEW == GPCOLUMNVIEWList[i].IDGPCOLUMNVIEW)
            return (i);
    }
    return (-1);
}
Persistence.GP.Methods.GPCOLUMNVIEW_ListGetIndexIDGPCOLUMNVIEW = function (GPCOLUMNVIEWList, IDGPCOLUMNVIEW) {
    for (i = 0; i < GPCOLUMNVIEWList.length; i++) {
        if (IDGPCOLUMNVIEW == GPCOLUMNVIEWList[i].IDGPCOLUMNVIEW)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.GP.Methods.GPCOLUMNVIEWListtoStr = function (GPCOLUMNVIEWList) {
    var Res = Persistence.Properties._Version + "(1)0";
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(GPCOLUMNVIEWList.length, Longitud, Texto);
        for (Counter = 0; Counter < GPCOLUMNVIEWList.length; Counter++) {
            var UnGPCOLUMNVIEW = GPCOLUMNVIEWList[Counter];
            UnGPCOLUMNVIEW.ToStr(Longitud, Texto);
        }

        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
}
Persistence.GP.Methods.StrtoGPCOLUMNVIEW = function (ProtocoloStr, GPCOLUMNVIEWList) {

    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        GPCOLUMNVIEWList.length = 0;
        if (Persistence.GP.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")") - ProtocoloStr.indexOf("(") - 1);
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.Length - ProtocoloStr.indexOf(")") - 1);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnGPCOLUMNVIEW = new Persistence.Properties.TGPCOLUMNVIEW(); //Mode new row
                UnGPCOLUMNVIEW.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.GP.GPCOLUMNVIEW_ListAdd(GPCOLUMNVIEWList, UnGPCOLUMNVIEW);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}


//Socket Function 
Persistence.GP.Methods.GPCOLUMNVIEWListToByte = function (GPCOLUMNVIEWList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, GPCOLUMNVIEWList.Count - idx);
    for (i = idx; i < GPCOLUMNVIEWList.length; i++) {
        GPCOLUMNVIEWList[i].ToByte(MemStream);
    }
}
Persistence.GP.Methods.ByteToGPCOLUMNVIEWList = function (GPCOLUMNVIEWList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnGPCOLUMNVIEW = new Persistence.Properties.TGPCOLUMNVIEW();
        UnGPCOLUMNVIEW.ByteTo(MemStream);
        Methods.GPCOLUMNVIEW_ListAdd(GPCOLUMNVIEWList, UnGPCOLUMNVIEW);
    }
}
