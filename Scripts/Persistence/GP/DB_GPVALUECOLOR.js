﻿//**********************   PROPERTIES   *****************************************************************************************
Persistence.GP.Properties.TGPVALUECOLOR = function () {
    this.IDGPVALUECOLOR = 0;
    this.IDGPCOLUMNVIEW = 0;
    this.NAME = "";
    this.CONDITION = "";
    this.COLOR = "";

    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDGPVALUECOLOR);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDGPCOLUMNVIEW);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.NAME);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CONDITION);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.COLOR);

    }
    this.ByteTo = function (MemStream) {
        this.IDGPVALUECOLOR = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDGPCOLUMNVIEW = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.NAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CONDITION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.COLOR = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);

    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDGPVALUECOLOR, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDGPCOLUMNVIEW, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.NAME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.CONDITION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.COLOR, Longitud, Texto);

    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDGPVALUECOLOR = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDGPCOLUMNVIEW = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.NAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.CONDITION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.COLOR = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);

    }
}

//**********************   METHODS server  ********************************************************************************************
Persistence.GP.Methods.GPVALUECOLOR_Fill = function (GPVALUECOLORList, StrIDGPVALUECOLOR/*noin IDGPVALUECOLOR*/) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    GPVALUECOLORList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDGPVALUECOLOR == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPVALUECOLOR.IDGPVALUECOLOR.FieldName, " = " + UsrCfg.InternoAtisNames.GPVALUECOLOR.NAME_TABLE + "." + UsrCfg.InternoAtisNames.GPVALUECOLOR.IDGPVALUECOLOR.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDGPVALUECOLOR = " IN (" + StrIDGPVALUECOLOR + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPVALUECOLOR.IDGPVALUECOLOR.FieldName, StrIDGPVALUECOLOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //noin Param.AddInt32(UsrCfg.InternoDemoNames.GPVALUECOLOR.IDGPVALUECOLOR.FieldName, IDGPVALUECOLOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {
        /*
        //********   GPVALUECOLOR_GET   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPVALUECOLOR_GET", @"GPVALUECOLOR_GET description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   IDGPVALUECOLOR, "); 
         UnSQL.SQL.Add(@"   IDGPCOLUMNVIEW, "); 
         UnSQL.SQL.Add(@"   NAME, "); 
         UnSQL.SQL.Add(@"   CONDITION, "); 
         UnSQL.SQL.Add(@"   COLOR "); 
         UnSQL.SQL.Add(@"   FROM GPVALUECOLOR "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPVALUECOLOR=@[IDGPVALUECOLOR] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_GPVALUECOLOR = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "GPVALUECOLOR_GET", Param.ToBytes());
        ResErr = DS_GPVALUECOLOR.ResErr;
        if (ResErr.NotError) {
            if (DS_GPVALUECOLOR.DataSet.RecordCount > 0) {
                DS_GPVALUECOLOR.DataSet.First();
                while (!(DS_GPVALUECOLOR.DataSet.Eof)) {
                    var GPVALUECOLOR = new Persistence.GP.Properties.TGPVALUECOLOR();
                    GPVALUECOLOR.IDGPVALUECOLOR = DS_GPVALUECOLOR.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPVALUECOLOR.IDGPVALUECOLOR.FieldName).asInt32();
                    GPVALUECOLOR.IDGPCOLUMNVIEW = DS_GPVALUECOLOR.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPVALUECOLOR.IDGPCOLUMNVIEW.FieldName).asInt32();
                    GPVALUECOLOR.NAME = DS_GPVALUECOLOR.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPVALUECOLOR.NAME.FieldName).asString();
                    GPVALUECOLOR.CONDITION = DS_GPVALUECOLOR.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPVALUECOLOR.CONDITION.FieldName).asString();
                    GPVALUECOLOR.COLOR = DS_GPVALUECOLOR.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPVALUECOLOR.COLOR.FieldName).asString();

                    //Other
                    GPVALUECOLORList.push(GPVALUECOLOR);
                    DS_GPVALUECOLOR.DataSet.Next();
                }
            }
            else {
                DS_GPVALUECOLOR.ResErr.NotError = false;
                DS_GPVALUECOLOR.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPVALUECOLOR_GETID = function (GPVALUECOLOR) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPVALUECOLOR.IDGPVALUECOLOR.FieldName, GPVALUECOLOR.IDGPVALUECOLOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPVALUECOLOR.IDGPCOLUMNVIEW.FieldName, GPVALUECOLOR.IDGPCOLUMNVIEW, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPVALUECOLOR.NAME.FieldName, GPVALUECOLOR.NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPVALUECOLOR.CONDITION.FieldName, GPVALUECOLOR.CONDITION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPVALUECOLOR.COLOR.FieldName, GPVALUECOLOR.COLOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

        /*
        //********   GPVALUECOLOR_GETID   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPVALUECOLOR_GETID", @"GPVALUECOLOR_GETID description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   FROM GPVALUECOLOR "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPCOLUMNVIEW=@[IDGPCOLUMNVIEW] And "); 
         UnSQL.SQL.Add(@"   NAME=@[NAME] And "); 
         UnSQL.SQL.Add(@"   CONDITION=@[CONDITION] And "); 
         UnSQL.SQL.Add(@"   COLOR=@[COLOR] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_GPVALUECOLOR = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "GPVALUECOLOR_GETID", Param.ToBytes());
        ResErr = DS_GPVALUECOLOR.ResErr;
        if (ResErr.NotError) {
            if (DS_GPVALUECOLOR.DataSet.RecordCount > 0) {
                DS_GPVALUECOLOR.DataSet.First();
                GPVALUECOLOR.IDGPVALUECOLOR = DS_GPVALUECOLOR.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPVALUECOLOR.IDGPVALUECOLOR.FieldName).asInt32();
            }
            else {
                DS_GPVALUECOLOR.ResErr.NotError = false;
                DS_GPVALUECOLOR.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPVALUECOLOR_ADD = function (GPVALUECOLOR) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPVALUECOLOR.IDGPVALUECOLOR.FieldName, GPVALUECOLOR.IDGPVALUECOLOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPVALUECOLOR.IDGPCOLUMNVIEW.FieldName, GPVALUECOLOR.IDGPCOLUMNVIEW, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPVALUECOLOR.NAME.FieldName, GPVALUECOLOR.NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPVALUECOLOR.CONDITION.FieldName, GPVALUECOLOR.CONDITION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPVALUECOLOR.COLOR.FieldName, GPVALUECOLOR.COLOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        /* 
        //********   GPVALUECOLOR_ADD   ************************* 
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPVALUECOLOR_ADD", @"GPVALUECOLOR_ADD description."); 
        UnSQL.SQL.Add(@"   INSERT INTO GPVALUECOLOR( "); 
         UnSQL.SQL.Add(@"   IDGPCOLUMNVIEW, "); 
         UnSQL.SQL.Add(@"   NAME, "); 
         UnSQL.SQL.Add(@"   CONDITION, "); 
         UnSQL.SQL.Add(@"   COLOR "); 
         UnSQL.SQL.Add(@"   ) Values ( ");  
        UnSQL.SQL.Add(@"   @[IDGPCOLUMNVIEW], "); 
         UnSQL.SQL.Add(@"   @[NAME], "); 
         UnSQL.SQL.Add(@"   @[CONDITION], "); 
         UnSQL.SQL.Add(@"   @[COLOR] "); 
         UnSQL.SQL.Add(@"   )  "); 
        UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPVALUECOLOR_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.GP.Methods.GPVALUECOLOR_GETID(GPVALUECOLOR);
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPVALUECOLOR_UPD = function (GPVALUECOLOR) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPVALUECOLOR.IDGPVALUECOLOR.FieldName, GPVALUECOLOR.IDGPVALUECOLOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPVALUECOLOR.IDGPCOLUMNVIEW.FieldName, GPVALUECOLOR.IDGPCOLUMNVIEW, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPVALUECOLOR.NAME.FieldName, GPVALUECOLOR.NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPVALUECOLOR.CONDITION.FieldName, GPVALUECOLOR.CONDITION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPVALUECOLOR.COLOR.FieldName, GPVALUECOLOR.COLOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);


        /*   
        //********   GPVALUECOLOR_UPD   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPVALUECOLOR_UPD", @"GPVALUECOLOR_UPD description.");  
        UnSQL.SQL.Add(@"  UPDATE GPVALUECOLOR Set "); 
         UnSQL.SQL.Add(@"   IDGPCOLUMNVIEW=@[IDGPCOLUMNVIEW], "); 
         UnSQL.SQL.Add(@"   NAME=@[NAME], "); 
         UnSQL.SQL.Add(@"   CONDITION=@[CONDITION], "); 
         UnSQL.SQL.Add(@"   COLOR=@[COLOR] "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPVALUECOLOR=@[IDGPVALUECOLOR] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPVALUECOLOR_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.GP.Methods.GPVALUECOLOR_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.GP.Methods.GPVALUECOLOR_DELIDGPVALUECOLOR(/*String StrIDGPVALUECOLOR*/Param);
    }
    else {
        Res = Persistence.GP.Methods.GPVALUECOLOR_DELGPVALUECOLOR(Param/*GPVALUECOLORList*/);
    }
    return (Res);
}

Persistence.GP.Methods.GPVALUECOLOR_DELIDGPVALUECOLOR = function (/*String StrIDGPVALUECOLOR*/IDGPVALUECOLOR) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDGPVALUECOLOR == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPVALUECOLOR.IDGPVALUECOLOR.FieldName, " = " + UsrCfg.InternoAtisNames.GPVALUECOLOR.NAME_TABLE + "." + UsrCfg.InternoAtisNames.GPVALUECOLOR.IDGPVALUECOLOR.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDGPVALUECOLOR = " IN (" + StrIDGPVALUECOLOR + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPVALUECOLOR.IDGPVALUECOLOR.FieldName, StrIDGPVALUECOLOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.GPVALUECOLOR.IDGPVALUECOLOR.FieldName, IDGPVALUECOLOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   GPVALUECOLOR_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPVALUECOLOR_DEL", @"GPVALUECOLOR_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE GPVALUECOLOR "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPVALUECOLOR=@[IDGPVALUECOLOR]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPVALUECOLOR_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPVALUECOLOR_DELGPVALUECOLOR = function (GPVALUECOLOR/*GPVALUECOLORList*/) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        /*
        StrIDGPVALUECOLOR = "";
        StrIDOTRO = "";
        for (var i = 0; i < GPVALUECOLORList.length; i++) {
        StrIDGPVALUECOLOR += GPVALUECOLORList[i].IDGPVALUECOLOR + ",";
        StrIDOTRO += GPVALUECOLORList[i].IDOTRO + ",";
        }
        StrIDGPVALUECOLOR = StrIDGPVALUECOLOR.substring(0, StrIDGPVALUECOLOR.length - 1);
        StrIDOTRO = StrIDOTRO.substring(0, StrIDOTRO.length - 1);
        
        
        if (StrIDGPVALUECOLOR == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPVALUECOLOR.IDGPVALUECOLOR.FieldName, " = " + UsrCfg.InternoAtisNames.GPVALUECOLOR.NAME_TABLE + "." + UsrCfg.InternoAtisNames.GPVALUECOLOR.IDGPVALUECOLOR.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDGPVALUECOLOR = " IN (" + StrIDGPVALUECOLOR + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPVALUECOLOR.IDGPVALUECOLOR.FieldName, StrIDGPVALUECOLOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/


        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.GPVALUECOLOR.IDGPVALUECOLOR.FieldName, GPVALUECOLOR.IDGPVALUECOLOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*    
        //********   GPVALUECOLOR_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPVALUECOLOR_DEL", @"GPVALUECOLOR_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE GPVALUECOLOR "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPVALUECOLOR=@[IDGPVALUECOLOR]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPVALUECOLOR_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

Persistence.GP.Methods.GPVALUECOLOR_GET_BY_GPCOLUMNVIEW = function (GPVALUECOLORList, IDGPCOLUMNVIEW)
{
    var ResErr = new SysCfg.Error.Properties.TResErr();
    GPVALUECOLORList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();

    Param.AddInt32(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.IDGPCOLUMNVIEW.FieldName, IDGPCOLUMNVIEW, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

    try
    {
        var DS_GPVALUECOLOR = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "GPVALUECOLOR_SELECTBYCOLUMNVIEW", Param.ToBytes());
        ResErr = DS_GPVALUECOLOR.ResErr;
        if (ResErr.NotError)
        {
            if (DS_GPVALUECOLOR.DataSet.RecordCount > 0)
            {
                DS_GPVALUECOLOR.DataSet.First();
                while (!(DS_GPVALUECOLOR.DataSet.Eof))
                {
                    var GPVALUECOLOR = new Persistence.GP.Properties.TGPVALUECOLOR();
                    GPVALUECOLOR.IDGPVALUECOLOR = DS_GPVALUECOLOR.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPVALUECOLOR.IDGPVALUECOLOR.FieldName).asInt32();
                    GPVALUECOLOR.IDGPCOLUMNVIEW = DS_GPVALUECOLOR.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPVALUECOLOR.IDGPCOLUMNVIEW.FieldName).asInt32();
                    GPVALUECOLOR.NAME = DS_GPVALUECOLOR.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPVALUECOLOR.NAME.FieldName).asString();
                    GPVALUECOLOR.CONDITION = DS_GPVALUECOLOR.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPVALUECOLOR.CONDITION.FieldName).asString();
                    GPVALUECOLOR.COLOR = DS_GPVALUECOLOR.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPVALUECOLOR.COLOR.FieldName).asString();

                    //Other
                    GPVALUECOLORList.push(GPVALUECOLOR);
                    DS_GPVALUECOLOR.DataSet.Next();
                }
            }
            else
            {
                DS_GPVALUECOLOR.ResErr.NotError = false;
                DS_GPVALUECOLOR.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}

//**********************   METHODS   ********************************************************************************************
Persistence.GP.Methods.GPVALUECOLOR_ListSetID = function (GPVALUECOLORList, IDGPVALUECOLOR) {
    for (i = 0; i < GPVALUECOLORList.length; i++) {
        if (IDGPVALUECOLOR == GPVALUECOLORList[i].IDGPVALUECOLOR)
            return (GPVALUECOLORList[i]);
    }
    var UnGPVALUECOLOR = new Persistence.Properties.TGPVALUECOLOR;
    UnGPVALUECOLOR.IDGPVALUECOLOR = IDGPVALUECOLOR;
    return (UnGPVALUECOLOR);
}
Persistence.GP.Methods.GPVALUECOLOR_ListAdd = function (GPVALUECOLORList, GPVALUECOLOR) {
    var i = Persistence.GP.Methods.GPVALUECOLOR_ListGetIndex(GPVALUECOLORList, GPVALUECOLOR);
    if (i == -1) GPVALUECOLORList.push(GPVALUECOLOR);
    else {
        GPVALUECOLORList[i].IDGPVALUECOLOR = GPVALUECOLOR.IDGPVALUECOLOR;
        GPVALUECOLORList[i].IDGPCOLUMNVIEW = GPVALUECOLOR.IDGPCOLUMNVIEW;
        GPVALUECOLORList[i].NAME = GPVALUECOLOR.NAME;
        GPVALUECOLORList[i].CONDITION = GPVALUECOLOR.CONDITION;
        GPVALUECOLORList[i].COLOR = GPVALUECOLOR.COLOR;

    }
}
//CJRC_26072018
Persistence.GP.Methods.GPVALUECOLOR_ListGetIndex = function (GPVALUECOLORList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.GP.Methods.GPVALUECOLOR_ListGetIndexIDGPVALUECOLOR(GPVALUECOLORList, Param);

    }
    else {
        Res = Persistence.GP.Methods.GPVALUECOLOR_ListGetIndexGPVALUECOLOR(GPVALUECOLORList, Param) ;
    }
    return (Res);
}

Persistence.GP.Methods.GPVALUECOLOR_ListGetIndexGPVALUECOLOR = function (GPVALUECOLORList, GPVALUECOLOR) {
    for (i = 0; i < GPVALUECOLORList.length; i++) {
        if (GPVALUECOLOR.IDGPVALUECOLOR == GPVALUECOLORList[i].IDGPVALUECOLOR)
            return (i);
    }
    return (-1);
}
Persistence.GP.Methods.GPVALUECOLOR_ListGetIndexIDGPVALUECOLOR = function (GPVALUECOLORList, IDGPVALUECOLOR) {
    for (i = 0; i < GPVALUECOLORList.length; i++) {
        if (IDGPVALUECOLOR == GPVALUECOLORList[i].IDGPVALUECOLOR)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.GP.Methods.GPVALUECOLORListtoStr = function (GPVALUECOLORList) {
    var Res = Persistence.Properties._Version + "(1)0";
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(GPVALUECOLORList.length, Longitud, Texto);
        for (Counter = 0; Counter < GPVALUECOLORList.length; Counter++) {
            var UnGPVALUECOLOR = GPVALUECOLORList[Counter];
            UnGPVALUECOLOR.ToStr(Longitud, Texto);
        }

        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
}
Persistence.GP.Methods.StrtoGPVALUECOLOR = function (ProtocoloStr, GPVALUECOLORList) {

    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        GPVALUECOLORList.length = 0;
        if (Persistence.GP.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")") - ProtocoloStr.indexOf("(") - 1);
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.Length - ProtocoloStr.indexOf(")") - 1);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnGPVALUECOLOR = new Persistence.Properties.TGPVALUECOLOR(); //Mode new row
                UnGPVALUECOLOR.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.GP.GPVALUECOLOR_ListAdd(GPVALUECOLORList, UnGPVALUECOLOR);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}


//Socket Function 
Persistence.GP.Methods.GPVALUECOLORListToByte = function (GPVALUECOLORList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, GPVALUECOLORList.Count - idx);
    for (i = idx; i < GPVALUECOLORList.length; i++) {
        GPVALUECOLORList[i].ToByte(MemStream);
    }
}
Persistence.GP.Methods.ByteToGPVALUECOLORList = function (GPVALUECOLORList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnGPVALUECOLOR = new Persistence.Properties.TGPVALUECOLOR();
        UnGPVALUECOLOR.ByteTo(MemStream);
        Methods.GPVALUECOLOR_ListAdd(GPVALUECOLORList, UnGPVALUECOLOR);
    }
}
