﻿//**********************   PROPERTIES   *****************************************************************************************
Persistence.GP.Properties.TGPREPORTSQL = function () {
    this.IDGPREPORTSQL = 0;
    this.IDGPQUERY = 0;
    this.NAME = "";
    this.DESCRIPTION = "";
    this.TABLENAME = "";
    this.STRSQL = "";

    this.GPQUERY = new Persistence.GP.Properties.TGPQUERY();

    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDGPREPORTSQL);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDGPQUERY);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.NAME);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.DESCRIPTION);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.TABLENAME);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.STRSQL);

    }
    this.ByteTo = function (MemStream) {
        this.IDGPREPORTSQL = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDGPQUERY = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.NAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.DESCRIPTION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.TABLENAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.STRSQL = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);

    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDGPREPORTSQL, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDGPQUERY, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.NAME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.DESCRIPTION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.TABLENAME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.STRSQL, Longitud, Texto);

    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDGPREPORTSQL = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDGPQUERY = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.NAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.DESCRIPTION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.TABLENAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.STRSQL = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);

    }
}

//**********************   METHODS server  ********************************************************************************************
Persistence.GP.Methods.GPREPORTSQL_Fill = function (GPREPORTSQLList, StrIDGPREPORTSQL/*noin IDGPREPORTSQL*/) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    GPREPORTSQLList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDGPREPORTSQL == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPREPORTSQL.IDGPREPORTSQL.FieldName, " = " + UsrCfg.InternoAtisNames.GPREPORTSQL.NAME_TABLE + "." + UsrCfg.InternoAtisNames.GPREPORTSQL.IDGPREPORTSQL.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDGPREPORTSQL = " IN (" + StrIDGPREPORTSQL + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPREPORTSQL.IDGPREPORTSQL.FieldName, StrIDGPREPORTSQL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //noin Param.AddInt32(UsrCfg.InternoDemoNames.GPREPORTSQL.IDGPREPORTSQL.FieldName, IDGPREPORTSQL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {
        /*
        //********   GPREPORTSQL_GET   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPREPORTSQL_GET", @"GPREPORTSQL_GET description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   IDGPREPORTSQL, "); 
         UnSQL.SQL.Add(@"   IDGPQUERY, "); 
         UnSQL.SQL.Add(@"   NAME, "); 
         UnSQL.SQL.Add(@"   DESCRIPTION, "); 
         UnSQL.SQL.Add(@"   TABLENAME, "); 
         UnSQL.SQL.Add(@"   STRSQL "); 
         UnSQL.SQL.Add(@"   FROM GPREPORTSQL "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPREPORTSQL=@[IDGPREPORTSQL] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_GPREPORTSQL = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "GPREPORTSQL_GET", Param.ToBytes());
        ResErr = DS_GPREPORTSQL.ResErr;
        if (ResErr.NotError) {
            if (DS_GPREPORTSQL.DataSet.RecordCount > 0) {
                DS_GPREPORTSQL.DataSet.First();
                while (!(DS_GPREPORTSQL.DataSet.Eof)) {
                    var GPREPORTSQL = new Persistence.GP.Properties.TGPREPORTSQL();
                    GPREPORTSQL.IDGPREPORTSQL = DS_GPREPORTSQL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPREPORTSQL.IDGPREPORTSQL.FieldName).asInt32();
                    GPREPORTSQL.IDGPQUERY = DS_GPREPORTSQL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPREPORTSQL.IDGPQUERY.FieldName).asInt32();
                    GPREPORTSQL.NAME = DS_GPREPORTSQL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPREPORTSQL.NAME.FieldName).asString();
                    GPREPORTSQL.DESCRIPTION = DS_GPREPORTSQL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPREPORTSQL.DESCRIPTION.FieldName).asText();
                    GPREPORTSQL.TABLENAME = DS_GPREPORTSQL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPREPORTSQL.TABLENAME.FieldName).asText();
                    GPREPORTSQL.STRSQL = DS_GPREPORTSQL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPREPORTSQL.STRSQL.FieldName).asText();

                    //Other
                    GPREPORTSQLList.push(GPREPORTSQL);
                    DS_GPREPORTSQL.DataSet.Next();
                }
            }
            else {
                DS_GPREPORTSQL.ResErr.NotError = false;
                DS_GPREPORTSQL.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPREPORTSQL_GETID = function (GPREPORTSQL) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPREPORTSQL.IDGPREPORTSQL.FieldName, GPREPORTSQL.IDGPREPORTSQL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPREPORTSQL.IDGPQUERY.FieldName, GPREPORTSQL.IDGPQUERY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPREPORTSQL.NAME.FieldName, GPREPORTSQL.NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddText(UsrCfg.InternoAtisNames.GPREPORTSQL.DESCRIPTION.FieldName, GPREPORTSQL.DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddText(UsrCfg.InternoAtisNames.GPREPORTSQL.TABLENAME.FieldName, GPREPORTSQL.TABLENAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddText(UsrCfg.InternoAtisNames.GPREPORTSQL.STRSQL.FieldName, GPREPORTSQL.STRSQL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

        /*
        //********   GPREPORTSQL_GETID   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPREPORTSQL_GETID", @"GPREPORTSQL_GETID description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   FROM GPREPORTSQL "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPQUERY=@[IDGPQUERY] And "); 
         UnSQL.SQL.Add(@"   NAME=@[NAME] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_GPREPORTSQL = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "GPREPORTSQL_GETID", Param.ToBytes());
        ResErr = DS_GPREPORTSQL.ResErr;
        if (ResErr.NotError) {
            if (DS_GPREPORTSQL.DataSet.RecordCount > 0) {
                DS_GPREPORTSQL.DataSet.First();
                GPREPORTSQL.IDGPREPORTSQL = DS_GPREPORTSQL.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPREPORTSQL.IDGPREPORTSQL.FieldName).asInt32();
            }
            else {
                DS_GPREPORTSQL.ResErr.NotError = false;
                DS_GPREPORTSQL.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPREPORTSQL_ADD = function (GPREPORTSQL) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPREPORTSQL.IDGPREPORTSQL.FieldName, GPREPORTSQL.IDGPREPORTSQL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPREPORTSQL.IDGPQUERY.FieldName, GPREPORTSQL.IDGPQUERY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPREPORTSQL.NAME.FieldName, GPREPORTSQL.NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.GPREPORTSQL.DESCRIPTION.FieldName, GPREPORTSQL.DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.GPREPORTSQL.TABLENAME.FieldName, GPREPORTSQL.TABLENAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.GPREPORTSQL.STRSQL.FieldName, GPREPORTSQL.STRSQL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        /* 
        //********   GPREPORTSQL_ADD   ************************* 
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPREPORTSQL_ADD", @"GPREPORTSQL_ADD description."); 
        UnSQL.SQL.Add(@"   INSERT INTO GPREPORTSQL( "); 
         UnSQL.SQL.Add(@"   IDGPQUERY, "); 
         UnSQL.SQL.Add(@"   NAME, "); 
         UnSQL.SQL.Add(@"   DESCRIPTION, "); 
         UnSQL.SQL.Add(@"   TABLENAME, "); 
         UnSQL.SQL.Add(@"   STRSQL "); 
         UnSQL.SQL.Add(@"   ) Values ( ");  
        UnSQL.SQL.Add(@"   @[IDGPQUERY], "); 
         UnSQL.SQL.Add(@"   @[NAME], "); 
         UnSQL.SQL.Add(@"   @[DESCRIPTION], "); 
         UnSQL.SQL.Add(@"   @[TABLENAME], "); 
         UnSQL.SQL.Add(@"   @[STRSQL] "); 
         UnSQL.SQL.Add(@"   )  "); 
        UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPREPORTSQL_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.GP.Methods.GPREPORTSQL_GETID(GPREPORTSQL);
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPREPORTSQL_UPD = function (GPREPORTSQL) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPREPORTSQL.IDGPREPORTSQL.FieldName, GPREPORTSQL.IDGPREPORTSQL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPREPORTSQL.IDGPQUERY.FieldName, GPREPORTSQL.IDGPQUERY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPREPORTSQL.NAME.FieldName, GPREPORTSQL.NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.GPREPORTSQL.DESCRIPTION.FieldName, GPREPORTSQL.DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.GPREPORTSQL.TABLENAME.FieldName, GPREPORTSQL.TABLENAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.GPREPORTSQL.STRSQL.FieldName, GPREPORTSQL.STRSQL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);


        /*   
        //********   GPREPORTSQL_UPD   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPREPORTSQL_UPD", @"GPREPORTSQL_UPD description.");  
        UnSQL.SQL.Add(@"  UPDATE GPREPORTSQL Set "); 
         UnSQL.SQL.Add(@"   IDGPQUERY=@[IDGPQUERY], "); 
         UnSQL.SQL.Add(@"   NAME=@[NAME], "); 
         UnSQL.SQL.Add(@"   DESCRIPTION=@[DESCRIPTION], "); 
         UnSQL.SQL.Add(@"   TABLENAME=@[TABLENAME], "); 
         UnSQL.SQL.Add(@"   STRSQL=@[STRSQL] "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPREPORTSQL=@[IDGPREPORTSQL] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPREPORTSQL_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.GP.Methods.GPREPORTSQL_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.GP.Methods.GPREPORTSQL_DELIDGPREPORTSQL(/*String StrIDGPREPORTSQL*/Param);
    }
    else {
        Res = Persistence.GP.Methods.GPREPORTSQL_DELGPREPORTSQL(Param/*GPREPORTSQLList*/);
    }
    return (Res);
}

Persistence.GP.Methods.GPREPORTSQL_DELIDGPREPORTSQL = function (/*String StrIDGPREPORTSQL*/IDGPREPORTSQL) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDGPREPORTSQL == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPREPORTSQL.IDGPREPORTSQL.FieldName, " = " + UsrCfg.InternoAtisNames.GPREPORTSQL.NAME_TABLE + "." + UsrCfg.InternoAtisNames.GPREPORTSQL.IDGPREPORTSQL.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDGPREPORTSQL = " IN (" + StrIDGPREPORTSQL + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPREPORTSQL.IDGPREPORTSQL.FieldName, StrIDGPREPORTSQL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.GPREPORTSQL.IDGPREPORTSQL.FieldName, IDGPREPORTSQL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   GPREPORTSQL_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPREPORTSQL_DEL", @"GPREPORTSQL_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE GPREPORTSQL "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPREPORTSQL=@[IDGPREPORTSQL]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPREPORTSQL_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPREPORTSQL_DELGPREPORTSQL = function (GPREPORTSQL/*GPREPORTSQLList*/) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        /*
        StrIDGPREPORTSQL = "";
        StrIDOTRO = "";
        for (var i = 0; i < GPREPORTSQLList.length; i++) {
        StrIDGPREPORTSQL += GPREPORTSQLList[i].IDGPREPORTSQL + ",";
        StrIDOTRO += GPREPORTSQLList[i].IDOTRO + ",";
        }
        StrIDGPREPORTSQL = StrIDGPREPORTSQL.substring(0, StrIDGPREPORTSQL.length - 1);
        StrIDOTRO = StrIDOTRO.substring(0, StrIDOTRO.length - 1);
        
        
        if (StrIDGPREPORTSQL == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPREPORTSQL.IDGPREPORTSQL.FieldName, " = " + UsrCfg.InternoAtisNames.GPREPORTSQL.NAME_TABLE + "." + UsrCfg.InternoAtisNames.GPREPORTSQL.IDGPREPORTSQL.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDGPREPORTSQL = " IN (" + StrIDGPREPORTSQL + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPREPORTSQL.IDGPREPORTSQL.FieldName, StrIDGPREPORTSQL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/


        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.GPREPORTSQL.IDGPREPORTSQL.FieldName, GPREPORTSQL.IDGPREPORTSQL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*    
        //********   GPREPORTSQL_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPREPORTSQL_DEL", @"GPREPORTSQL_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE GPREPORTSQL "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPREPORTSQL=@[IDGPREPORTSQL]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPREPORTSQL_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}



//**********************   METHODS   ********************************************************************************************
Persistence.GP.Methods.GPREPORTSQL_ListSetID = function (GPREPORTSQLList, IDGPREPORTSQL) {
    for (i = 0; i < GPREPORTSQLList.length; i++) {
        if (IDGPREPORTSQL == GPREPORTSQLList[i].IDGPREPORTSQL)
            return (GPREPORTSQLList[i]);
    }
    var UnGPREPORTSQL = new Persistence.Properties.TGPREPORTSQL;
    UnGPREPORTSQL.IDGPREPORTSQL = IDGPREPORTSQL;
    return (UnGPREPORTSQL);
}
Persistence.GP.Methods.GPREPORTSQL_ListAdd = function (GPREPORTSQLList, GPREPORTSQL) {
    var i = Persistence.GP.Methods.GPREPORTSQL_ListGetIndex(GPREPORTSQLList, GPREPORTSQL);
    if (i == -1) GPREPORTSQLList.push(GPREPORTSQL);
    else {
        GPREPORTSQLList[i].IDGPREPORTSQL = GPREPORTSQL.IDGPREPORTSQL;
        GPREPORTSQLList[i].IDGPQUERY = GPREPORTSQL.IDGPQUERY;
        GPREPORTSQLList[i].NAME = GPREPORTSQL.NAME;
        GPREPORTSQLList[i].DESCRIPTION = GPREPORTSQL.DESCRIPTION;
        GPREPORTSQLList[i].TABLENAME = GPREPORTSQL.TABLENAME;
        GPREPORTSQLList[i].STRSQL = GPREPORTSQL.STRSQL;

    }
}
//CJRC_26072018
Persistence.GP.Methods.GPREPORTSQL_ListGetIndex = function (GPREPORTSQLList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.GP.Methods.GPREPORTSQL_ListGetIndexIDGPREPORTSQL(GPREPORTSQLList, IDGPREPORTSQL);

    }
    else {
        Res = Persistence.GP.Methods.GPREPORTSQL_ListGetIndexGPREPORTSQL(GPREPORTSQLList, GPREPORTSQL);
    }
    return (Res);
}

Persistence.GP.Methods.GPREPORTSQL_ListGetIndexGPREPORTSQL = function (GPREPORTSQLList, GPREPORTSQL) {
    for (i = 0; i < GPREPORTSQLList.length; i++) {
        if (GPREPORTSQL.IDGPREPORTSQL == GPREPORTSQLList[i].IDGPREPORTSQL)
            return (i);
    }
    return (-1);
}
Persistence.GP.Methods.GPREPORTSQL_ListGetIndexIDGPREPORTSQL = function (GPREPORTSQLList, IDGPREPORTSQL) {
    for (i = 0; i < GPREPORTSQLList.length; i++) {
        if (IDGPREPORTSQL == GPREPORTSQLList[i].IDGPREPORTSQL)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.GP.Methods.GPREPORTSQLListtoStr = function (GPREPORTSQLList) {
    var Res = Persistence.Properties._Version + "(1)0";
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(GPREPORTSQLList.length, Longitud, Texto);
        for (Counter = 0; Counter < GPREPORTSQLList.length; Counter++) {
            var UnGPREPORTSQL = GPREPORTSQLList[Counter];
            UnGPREPORTSQL.ToStr(Longitud, Texto);
        }

        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
}
Persistence.GP.Methods.StrtoGPREPORTSQL = function (ProtocoloStr, GPREPORTSQLList) {

    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        GPREPORTSQLList.length = 0;
        if (Persistence.GP.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")") - ProtocoloStr.indexOf("(") - 1);
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length - ProtocoloStr.indexOf(")") - 1);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnGPREPORTSQL = new Persistence.Properties.TGPREPORTSQL(); //Mode new row
                UnGPREPORTSQL.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.GP.GPREPORTSQL_ListAdd(GPREPORTSQLList, UnGPREPORTSQL);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}


//Socket Function 
Persistence.GP.Methods.GPREPORTSQLListToByte = function (GPREPORTSQLList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, GPREPORTSQLList.Count - idx);
    for (i = idx; i < GPREPORTSQLList.length; i++) {
        GPREPORTSQLList[i].ToByte(MemStream);
    }
}
Persistence.GP.Methods.ByteToGPREPORTSQLList = function (GPREPORTSQLList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnGPREPORTSQL = new Persistence.Properties.TGPREPORTSQL();
        UnGPREPORTSQL.ByteTo(MemStream);
        Methods.GPREPORTSQL_ListAdd(GPREPORTSQLList, UnGPREPORTSQL);
    }
}
