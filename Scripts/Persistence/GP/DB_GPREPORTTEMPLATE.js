﻿//**********************   PROPERTIES   *****************************************************************************************
Persistence.GP.Properties.TGPREPORTTEMPLATE = function () {
    this.IDGPREPORTTEMPLATE = 0;
    this.IDGPQUERY = 0;
    this.NAME = "";
    this.DESCRIPTION = "";
    this.CODE = "";
    this.FILENAME = "";
    this.MULTIPLEFILE = false;

    this.GPQUERY = new Persistence.GP.Properties.TGPQUERY();

    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDGPREPORTTEMPLATE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDGPQUERY);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.NAME);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.DESCRIPTION);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CODE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.FILENAME);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.MULTIPLEFILE);

    }
    this.ByteTo = function (MemStream) {
        this.IDGPREPORTTEMPLATE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDGPQUERY = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.NAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.DESCRIPTION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CODE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.FILENAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.MULTIPLEFILE = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);

    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDGPREPORTTEMPLATE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDGPQUERY, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.NAME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.DESCRIPTION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.CODE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.FILENAME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteBool(this.MULTIPLEFILE, Longitud, Texto);

    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDGPREPORTTEMPLATE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDGPQUERY = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.NAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.DESCRIPTION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.CODE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.FILENAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.MULTIPLEFILE = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);

    }
}

//**********************   METHODS server  ********************************************************************************************
Persistence.GP.Methods.GPREPORTTEMPLATE_Fill = function (GPREPORTTEMPLATEList, StrIDGPREPORTTEMPLATE/*noin IDGPREPORTTEMPLATE*/) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    GPREPORTTEMPLATEList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDGPREPORTTEMPLATE == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.IDGPREPORTTEMPLATE.FieldName, " = " + UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.NAME_TABLE + "." + UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.IDGPREPORTTEMPLATE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDGPREPORTTEMPLATE = " IN (" + StrIDGPREPORTTEMPLATE + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.IDGPREPORTTEMPLATE.FieldName, StrIDGPREPORTTEMPLATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //noin Param.AddInt32(UsrCfg.InternoDemoNames.GPREPORTTEMPLATE.IDGPREPORTTEMPLATE.FieldName, IDGPREPORTTEMPLATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {
        /*
        //********   GPREPORTTEMPLATE_GET   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPREPORTTEMPLATE_GET", @"GPREPORTTEMPLATE_GET description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   IDGPREPORTTEMPLATE, "); 
         UnSQL.SQL.Add(@"   IDGPQUERY, "); 
         UnSQL.SQL.Add(@"   NAME, "); 
         UnSQL.SQL.Add(@"   DESCRIPTION, "); 
         UnSQL.SQL.Add(@"   CODE, "); 
         UnSQL.SQL.Add(@"   FILENAME, "); 
         UnSQL.SQL.Add(@"   MULTIPLEFILE "); 
         UnSQL.SQL.Add(@"   FROM GPREPORTTEMPLATE "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPREPORTTEMPLATE=@[IDGPREPORTTEMPLATE] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_GPREPORTTEMPLATE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "GPREPORTTEMPLATE_GET", Param.ToBytes());
        ResErr = DS_GPREPORTTEMPLATE.ResErr;
        if (ResErr.NotError) {
            if (DS_GPREPORTTEMPLATE.DataSet.RecordCount > 0) {
                DS_GPREPORTTEMPLATE.DataSet.First();
                while (!(DS_GPREPORTTEMPLATE.DataSet.Eof)) {
                    var GPREPORTTEMPLATE = new Persistence.GP.Properties.TGPREPORTTEMPLATE();
                    GPREPORTTEMPLATE.IDGPREPORTTEMPLATE = DS_GPREPORTTEMPLATE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.IDGPREPORTTEMPLATE.FieldName).asInt32();
                    GPREPORTTEMPLATE.IDGPQUERY = DS_GPREPORTTEMPLATE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.IDGPQUERY.FieldName).asInt32();
                    GPREPORTTEMPLATE.NAME = DS_GPREPORTTEMPLATE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.NAME.FieldName).asString();
                    GPREPORTTEMPLATE.DESCRIPTION = DS_GPREPORTTEMPLATE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.DESCRIPTION.FieldName).asText();
                    GPREPORTTEMPLATE.CODE = DS_GPREPORTTEMPLATE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.CODE.FieldName).asString();
                    GPREPORTTEMPLATE.FILENAME = DS_GPREPORTTEMPLATE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.FILENAME.FieldName).asString();
                    GPREPORTTEMPLATE.MULTIPLEFILE = DS_GPREPORTTEMPLATE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.MULTIPLEFILE.FieldName).asBoolean();

                    //Other
                    GPREPORTTEMPLATEList.push(GPREPORTTEMPLATE);
                    DS_GPREPORTTEMPLATE.DataSet.Next();
                }
            }
            else {
                DS_GPREPORTTEMPLATE.ResErr.NotError = false;
                DS_GPREPORTTEMPLATE.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPREPORTTEMPLATE_GETID = function (GPREPORTTEMPLATE) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.IDGPREPORTTEMPLATE.FieldName, GPREPORTTEMPLATE.IDGPREPORTTEMPLATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.IDGPQUERY.FieldName, GPREPORTTEMPLATE.IDGPQUERY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.NAME.FieldName, GPREPORTTEMPLATE.NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddText(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.DESCRIPTION.FieldName, GPREPORTTEMPLATE.DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.CODE.FieldName, GPREPORTTEMPLATE.CODE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.FILENAME.FieldName, GPREPORTTEMPLATE.FILENAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddBoolean(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.MULTIPLEFILE.FieldName, GPREPORTTEMPLATE.MULTIPLEFILE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

        /*
        //********   GPREPORTTEMPLATE_GETID   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPREPORTTEMPLATE_GETID", @"GPREPORTTEMPLATE_GETID description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   FROM GPREPORTTEMPLATE "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPQUERY=@[IDGPQUERY] And "); 
         UnSQL.SQL.Add(@"   NAME=@[NAME] And "); 
         UnSQL.SQL.Add(@"   CODE=@[CODE] And "); 
         UnSQL.SQL.Add(@"   FILENAME=@[FILENAME] And "); 
         UnSQL.SQL.Add(@"   MULTIPLEFILE=@[MULTIPLEFILE] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_GPREPORTTEMPLATE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "GPREPORTTEMPLATE_GETID", Param.ToBytes());
        ResErr = DS_GPREPORTTEMPLATE.ResErr;
        if (ResErr.NotError) {
            if (DS_GPREPORTTEMPLATE.DataSet.RecordCount > 0) {
                DS_GPREPORTTEMPLATE.DataSet.First();
                GPREPORTTEMPLATE.IDGPREPORTTEMPLATE = DS_GPREPORTTEMPLATE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.IDGPREPORTTEMPLATE.FieldName).asInt32();
            }
            else {
                DS_GPREPORTTEMPLATE.ResErr.NotError = false;
                DS_GPREPORTTEMPLATE.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPREPORTTEMPLATE_ADD = function (GPREPORTTEMPLATE) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.IDGPREPORTTEMPLATE.FieldName, GPREPORTTEMPLATE.IDGPREPORTTEMPLATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.IDGPQUERY.FieldName, GPREPORTTEMPLATE.IDGPQUERY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.NAME.FieldName, GPREPORTTEMPLATE.NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.DESCRIPTION.FieldName, GPREPORTTEMPLATE.DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.CODE.FieldName, GPREPORTTEMPLATE.CODE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.FILENAME.FieldName, GPREPORTTEMPLATE.FILENAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.MULTIPLEFILE.FieldName, GPREPORTTEMPLATE.MULTIPLEFILE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        /* 
        //********   GPREPORTTEMPLATE_ADD   ************************* 
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPREPORTTEMPLATE_ADD", @"GPREPORTTEMPLATE_ADD description."); 
        UnSQL.SQL.Add(@"   INSERT INTO GPREPORTTEMPLATE( "); 
         UnSQL.SQL.Add(@"   IDGPQUERY, "); 
         UnSQL.SQL.Add(@"   NAME, "); 
         UnSQL.SQL.Add(@"   DESCRIPTION, "); 
         UnSQL.SQL.Add(@"   CODE, "); 
         UnSQL.SQL.Add(@"   FILENAME, "); 
         UnSQL.SQL.Add(@"   MULTIPLEFILE "); 
         UnSQL.SQL.Add(@"   ) Values ( ");  
        UnSQL.SQL.Add(@"   @[IDGPQUERY], "); 
         UnSQL.SQL.Add(@"   @[NAME], "); 
         UnSQL.SQL.Add(@"   @[DESCRIPTION], "); 
         UnSQL.SQL.Add(@"   @[CODE], "); 
         UnSQL.SQL.Add(@"   @[FILENAME], "); 
         UnSQL.SQL.Add(@"   @[MULTIPLEFILE] "); 
         UnSQL.SQL.Add(@"   )  "); 
        UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPREPORTTEMPLATE_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.GP.Methods.GPREPORTTEMPLATE_GETID(GPREPORTTEMPLATE);
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPREPORTTEMPLATE_UPD = function (GPREPORTTEMPLATE) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.IDGPREPORTTEMPLATE.FieldName, GPREPORTTEMPLATE.IDGPREPORTTEMPLATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.IDGPQUERY.FieldName, GPREPORTTEMPLATE.IDGPQUERY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.NAME.FieldName, GPREPORTTEMPLATE.NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.DESCRIPTION.FieldName, GPREPORTTEMPLATE.DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.CODE.FieldName, GPREPORTTEMPLATE.CODE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.FILENAME.FieldName, GPREPORTTEMPLATE.FILENAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.MULTIPLEFILE.FieldName, GPREPORTTEMPLATE.MULTIPLEFILE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);


        /*   
        //********   GPREPORTTEMPLATE_UPD   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPREPORTTEMPLATE_UPD", @"GPREPORTTEMPLATE_UPD description.");  
        UnSQL.SQL.Add(@"  UPDATE GPREPORTTEMPLATE Set "); 
         UnSQL.SQL.Add(@"   IDGPQUERY=@[IDGPQUERY], "); 
         UnSQL.SQL.Add(@"   NAME=@[NAME], "); 
         UnSQL.SQL.Add(@"   DESCRIPTION=@[DESCRIPTION], "); 
         UnSQL.SQL.Add(@"   CODE=@[CODE], "); 
         UnSQL.SQL.Add(@"   FILENAME=@[FILENAME], "); 
         UnSQL.SQL.Add(@"   MULTIPLEFILE=@[MULTIPLEFILE] "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPREPORTTEMPLATE=@[IDGPREPORTTEMPLATE] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPREPORTTEMPLATE_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.GP.Methods.GPREPORTTEMPLATE_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.GP.Methods.GPREPORTTEMPLATE_DELIDGPREPORTTEMPLATE(/*String StrIDGPREPORTTEMPLATE*/Param);
    }
    else {
        Res = Persistence.GP.Methods.GPREPORTTEMPLATE_DELGPREPORTTEMPLATE(Param/*GPREPORTTEMPLATEList*/);
    }
    return (Res);
}

Persistence.GP.Methods.GPREPORTTEMPLATE_DELIDGPREPORTTEMPLATE = function (/*String StrIDGPREPORTTEMPLATE*/IDGPREPORTTEMPLATE) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDGPREPORTTEMPLATE == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.IDGPREPORTTEMPLATE.FieldName, " = " + UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.NAME_TABLE + "." + UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.IDGPREPORTTEMPLATE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDGPREPORTTEMPLATE = " IN (" + StrIDGPREPORTTEMPLATE + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.IDGPREPORTTEMPLATE.FieldName, StrIDGPREPORTTEMPLATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.IDGPREPORTTEMPLATE.FieldName, IDGPREPORTTEMPLATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   GPREPORTTEMPLATE_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPREPORTTEMPLATE_DEL", @"GPREPORTTEMPLATE_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE GPREPORTTEMPLATE "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPREPORTTEMPLATE=@[IDGPREPORTTEMPLATE]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPREPORTTEMPLATE_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPREPORTTEMPLATE_DELGPREPORTTEMPLATE = function (GPREPORTTEMPLATE/*GPREPORTTEMPLATEList*/) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        /*
        StrIDGPREPORTTEMPLATE = "";
        StrIDOTRO = "";
        for (var i = 0; i < GPREPORTTEMPLATEList.length; i++) {
        StrIDGPREPORTTEMPLATE += GPREPORTTEMPLATEList[i].IDGPREPORTTEMPLATE + ",";
        StrIDOTRO += GPREPORTTEMPLATEList[i].IDOTRO + ",";
        }
        StrIDGPREPORTTEMPLATE = StrIDGPREPORTTEMPLATE.substring(0, StrIDGPREPORTTEMPLATE.length - 1);
        StrIDOTRO = StrIDOTRO.substring(0, StrIDOTRO.length - 1);
        
        
        if (StrIDGPREPORTTEMPLATE == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.IDGPREPORTTEMPLATE.FieldName, " = " + UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.NAME_TABLE + "." + UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.IDGPREPORTTEMPLATE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDGPREPORTTEMPLATE = " IN (" + StrIDGPREPORTTEMPLATE + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.IDGPREPORTTEMPLATE.FieldName, StrIDGPREPORTTEMPLATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/


        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.IDGPREPORTTEMPLATE.FieldName, GPREPORTTEMPLATE.IDGPREPORTTEMPLATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*    
        //********   GPREPORTTEMPLATE_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPREPORTTEMPLATE_DEL", @"GPREPORTTEMPLATE_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE GPREPORTTEMPLATE "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPREPORTTEMPLATE=@[IDGPREPORTTEMPLATE]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPREPORTTEMPLATE_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//**********************   METHODS   ********************************************************************************************
Persistence.GP.Methods.GPREPORTTEMPLATE_ListSetID = function (GPREPORTTEMPLATEList, IDGPREPORTTEMPLATE) {
    for (i = 0; i < GPREPORTTEMPLATEList.length; i++) {
        if (IDGPREPORTTEMPLATE == GPREPORTTEMPLATEList[i].IDGPREPORTTEMPLATE)
            return (GPREPORTTEMPLATEList[i]);
    }
    var UnGPREPORTTEMPLATE = new Persistence.Properties.TGPREPORTTEMPLATE;
    UnGPREPORTTEMPLATE.IDGPREPORTTEMPLATE = IDGPREPORTTEMPLATE;
    return (UnGPREPORTTEMPLATE);
}
Persistence.GP.Methods.GPREPORTTEMPLATE_ListAdd = function (GPREPORTTEMPLATEList, GPREPORTTEMPLATE) {
    var i = Persistence.GP.Methods.GPREPORTTEMPLATE_ListGetIndex(GPREPORTTEMPLATEList, GPREPORTTEMPLATE);
    if (i == -1) GPREPORTTEMPLATEList.push(GPREPORTTEMPLATE);
    else {
        GPREPORTTEMPLATEList[i].IDGPREPORTTEMPLATE = GPREPORTTEMPLATE.IDGPREPORTTEMPLATE;
        GPREPORTTEMPLATEList[i].IDGPQUERY = GPREPORTTEMPLATE.IDGPQUERY;
        GPREPORTTEMPLATEList[i].NAME = GPREPORTTEMPLATE.NAME;
        GPREPORTTEMPLATEList[i].DESCRIPTION = GPREPORTTEMPLATE.DESCRIPTION;
        GPREPORTTEMPLATEList[i].CODE = GPREPORTTEMPLATE.CODE;
        GPREPORTTEMPLATEList[i].FILENAME = GPREPORTTEMPLATE.FILENAME;
        GPREPORTTEMPLATEList[i].MULTIPLEFILE = GPREPORTTEMPLATE.MULTIPLEFILE;

    }
}
//CJRC_26072018
Persistence.GP.Methods.GPREPORTTEMPLATE_ListGetIndex = function (GPREPORTTEMPLATEList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.GP.Methods.GPREPORTTEMPLATE_ListGetIndexIDGPREPORTTEMPLATE(GPREPORTTEMPLATEList, Param);

    }
    else {
        Res = Persistence.GP.Methods.GPREPORTTEMPLATE_ListGetIndexGPREPORTTEMPLATE(GPREPORTTEMPLATEList, Param);
    }
    return (Res);
}

Persistence.GP.Methods.GPREPORTTEMPLATE_ListGetIndexGPREPORTTEMPLATE = function (GPREPORTTEMPLATEList, GPREPORTTEMPLATE) {
    for (i = 0; i < GPREPORTTEMPLATEList.length; i++) {
        if (GPREPORTTEMPLATE.IDGPREPORTTEMPLATE == GPREPORTTEMPLATEList[i].IDGPREPORTTEMPLATE)
            return (i);
    }
    return (-1);
}
Persistence.GP.Methods.GPREPORTTEMPLATE_ListGetIndexIDGPREPORTTEMPLATE = function (GPREPORTTEMPLATEList, IDGPREPORTTEMPLATE) {
    for (i = 0; i < GPREPORTTEMPLATEList.length; i++) {
        if (IDGPREPORTTEMPLATE == GPREPORTTEMPLATEList[i].IDGPREPORTTEMPLATE)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.GP.Methods.GPREPORTTEMPLATEListtoStr = function (GPREPORTTEMPLATEList) {
    var Res = Persistence.Properties._Version + "(1)0";
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(GPREPORTTEMPLATEList.length, Longitud, Texto);
        for (Counter = 0; Counter < GPREPORTTEMPLATEList.length; Counter++) {
            var UnGPREPORTTEMPLATE = GPREPORTTEMPLATEList[Counter];
            UnGPREPORTTEMPLATE.ToStr(Longitud, Texto);
        }

        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
}
Persistence.GP.Methods.StrtoGPREPORTTEMPLATE = function (ProtocoloStr, GPREPORTTEMPLATEList) {

    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        GPREPORTTEMPLATEList.length = 0;
        if (Persistence.GP.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")") - ProtocoloStr.indexOf("(") - 1);
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.Length - ProtocoloStr.indexOf(")") - 1);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnGPREPORTTEMPLATE = new Persistence.Properties.TGPREPORTTEMPLATE(); //Mode new row
                UnGPREPORTTEMPLATE.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.GP.GPREPORTTEMPLATE_ListAdd(GPREPORTTEMPLATEList, UnGPREPORTTEMPLATE);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}


//Socket Function 
Persistence.GP.Methods.GPREPORTTEMPLATEListToByte = function (GPREPORTTEMPLATEList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, GPREPORTTEMPLATEList.Count - idx);
    for (i = idx; i < GPREPORTTEMPLATEList.length; i++) {
        GPREPORTTEMPLATEList[i].ToByte(MemStream);
    }
}
Persistence.GP.Methods.ByteToGPREPORTTEMPLATEList = function (GPREPORTTEMPLATEList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnGPREPORTTEMPLATE = new Persistence.Properties.TGPREPORTTEMPLATE();
        UnGPREPORTTEMPLATE.ByteTo(MemStream);
        Methods.GPREPORTTEMPLATE_ListAdd(GPREPORTTEMPLATEList, UnGPREPORTTEMPLATE);
    }
}
