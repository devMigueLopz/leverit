﻿//**********************   PROPERTIES   *****************************************************************************************
Persistence.GP.Properties.TGPQUERYTYPE = function () {
    this.IDGPQUERYTYPE = 0;
    this.QUERYTYPENAME = "";

    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDGPQUERYTYPE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.QUERYTYPENAME);

    }
    this.ByteTo = function (MemStream) {
        this.IDGPQUERYTYPE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.QUERYTYPENAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);

    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDGPQUERYTYPE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.QUERYTYPENAME, Longitud, Texto);

    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDGPQUERYTYPE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.QUERYTYPENAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);

    }
}


//**********************   METHODS server  ********************************************************************************************
Persistence.GP.Methods.GPQUERYTYPE_Fill = function (GPQUERYTYPEList, StrIDGPQUERYTYPE/*noin IDGPQUERYTYPE*/) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    GPQUERYTYPEList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDGPQUERYTYPE == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPQUERYTYPE.IDGPQUERYTYPE.FieldName, " = " + UsrCfg.InternoAtisNames.GPQUERYTYPE.NAME_TABLE + "." + UsrCfg.InternoAtisNames.GPQUERYTYPE.IDGPQUERYTYPE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDGPQUERYTYPE = " IN (" + StrIDGPQUERYTYPE + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPQUERYTYPE.IDGPQUERYTYPE.FieldName, StrIDGPQUERYTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //noin Param.AddInt32(UsrCfg.InternoDemoNames.GPQUERYTYPE.IDGPQUERYTYPE.FieldName, IDGPQUERYTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {
        /*
        //********   GPQUERYTYPE_GET   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPQUERYTYPE_GET", @"GPQUERYTYPE_GET description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   IDGPQUERYTYPE, "); 
         UnSQL.SQL.Add(@"   QUERYTYPENAME "); 
         UnSQL.SQL.Add(@"   FROM GPQUERYTYPE "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPQUERYTYPE=@[IDGPQUERYTYPE] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_GPQUERYTYPE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "GPQUERYTYPE_GET", Param.ToBytes());
        ResErr = DS_GPQUERYTYPE.ResErr;
        if (ResErr.NotError) {
            if (DS_GPQUERYTYPE.DataSet.RecordCount > 0) {
                DS_GPQUERYTYPE.DataSet.First();
                while (!(DS_GPQUERYTYPE.DataSet.Eof)) {
                    var GPQUERYTYPE = new Persistence.GP.Properties.TGPQUERYTYPE();
                    GPQUERYTYPE.IDGPQUERYTYPE = DS_GPQUERYTYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPQUERYTYPE.IDGPQUERYTYPE.FieldName).asInt32();
                    GPQUERYTYPE.QUERYTYPENAME = DS_GPQUERYTYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPQUERYTYPE.QUERYTYPENAME.FieldName).asString();

                    //Other
                    GPQUERYTYPEList.push(GPQUERYTYPE);
                    DS_GPQUERYTYPE.DataSet.Next();
                }
            }
            else {
                DS_GPQUERYTYPE.ResErr.NotError = false;
                DS_GPQUERYTYPE.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPQUERYTYPE_GETID = function (GPQUERYTYPE) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.GPQUERYTYPE.IDGPQUERYTYPE.FieldName, GPQUERYTYPE.IDGPQUERYTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPQUERYTYPE.QUERYTYPENAME.FieldName, GPQUERYTYPE.QUERYTYPENAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

        /*
        //********   GPQUERYTYPE_GETID   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPQUERYTYPE_GETID", @"GPQUERYTYPE_GETID description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   FROM GPQUERYTYPE "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   QUERYTYPENAME=@[QUERYTYPENAME] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_GPQUERYTYPE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "GPQUERYTYPE_GETID", Param.ToBytes());
        ResErr = DS_GPQUERYTYPE.ResErr;
        if (ResErr.NotError) {
            if (DS_GPQUERYTYPE.DataSet.RecordCount > 0) {
                DS_GPQUERYTYPE.DataSet.First();
                GPQUERYTYPE.IDGPQUERYTYPE = DS_GPQUERYTYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPQUERYTYPE.IDGPQUERYTYPE.FieldName).asInt32();
            }
            else {
                DS_GPQUERYTYPE.ResErr.NotError = false;
                DS_GPQUERYTYPE.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPQUERYTYPE_ADD = function (GPQUERYTYPE) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.GPQUERYTYPE.IDGPQUERYTYPE.FieldName, GPQUERYTYPE.IDGPQUERYTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPQUERYTYPE.QUERYTYPENAME.FieldName, GPQUERYTYPE.QUERYTYPENAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        /* 
        //********   GPQUERYTYPE_ADD   ************************* 
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPQUERYTYPE_ADD", @"GPQUERYTYPE_ADD description."); 
        UnSQL.SQL.Add(@"   INSERT INTO GPQUERYTYPE( "); 
         UnSQL.SQL.Add(@"   IDGPQUERYTYPE, "); 
         UnSQL.SQL.Add(@"   QUERYTYPENAME "); 
         UnSQL.SQL.Add(@"   ) Values ( ");  
        UnSQL.SQL.Add(@"   @[IDGPQUERYTYPE], "); 
         UnSQL.SQL.Add(@"   @[QUERYTYPENAME] "); 
         UnSQL.SQL.Add(@"   )  "); 
        UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPQUERYTYPE_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.GP.Methods.GPQUERYTYPE_GETID(GPQUERYTYPE);
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPQUERYTYPE_UPD = function (GPQUERYTYPE) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.GPQUERYTYPE.IDGPQUERYTYPE.FieldName, GPQUERYTYPE.IDGPQUERYTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPQUERYTYPE.QUERYTYPENAME.FieldName, GPQUERYTYPE.QUERYTYPENAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);


        /*   
        //********   GPQUERYTYPE_UPD   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPQUERYTYPE_UPD", @"GPQUERYTYPE_UPD description.");  
        UnSQL.SQL.Add(@"  UPDATE GPQUERYTYPE Set "); 
         UnSQL.SQL.Add(@"   QUERYTYPENAME=@[QUERYTYPENAME] "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPQUERYTYPE=@[IDGPQUERYTYPE] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPQUERYTYPE_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.GP.Methods.GPQUERYTYPE_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.GP.Methods.GPQUERYTYPE_DELIDGPQUERYTYPE(/*String StrIDGPQUERYTYPE*/Param);
    }
    else {
        Res = Persistence.GP.Methods.GPQUERYTYPE_DELGPQUERYTYPE(Param/*GPQUERYTYPEList*/);
    }
    return (Res);
}

Persistence.GP.Methods.GPQUERYTYPE_DELIDGPQUERYTYPE = function (/*String StrIDGPQUERYTYPE*/IDGPQUERYTYPE) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDGPQUERYTYPE == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPQUERYTYPE.IDGPQUERYTYPE.FieldName, " = " + UsrCfg.InternoAtisNames.GPQUERYTYPE.NAME_TABLE + "." + UsrCfg.InternoAtisNames.GPQUERYTYPE.IDGPQUERYTYPE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDGPQUERYTYPE = " IN (" + StrIDGPQUERYTYPE + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPQUERYTYPE.IDGPQUERYTYPE.FieldName, StrIDGPQUERYTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.GPQUERYTYPE.IDGPQUERYTYPE.FieldName, IDGPQUERYTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   GPQUERYTYPE_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPQUERYTYPE_DEL", @"GPQUERYTYPE_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE GPQUERYTYPE "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPQUERYTYPE=@[IDGPQUERYTYPE]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPQUERYTYPE_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPQUERYTYPE_DELGPQUERYTYPE = function (GPQUERYTYPE/*GPQUERYTYPEList*/) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        /*
        StrIDGPQUERYTYPE = "";
        StrIDOTRO = "";
        for (var i = 0; i < GPQUERYTYPEList.length; i++) {
        StrIDGPQUERYTYPE += GPQUERYTYPEList[i].IDGPQUERYTYPE + ",";
        StrIDOTRO += GPQUERYTYPEList[i].IDOTRO + ",";
        }
        StrIDGPQUERYTYPE = StrIDGPQUERYTYPE.substring(0, StrIDGPQUERYTYPE.length - 1);
        StrIDOTRO = StrIDOTRO.substring(0, StrIDOTRO.length - 1);
        
        
        if (StrIDGPQUERYTYPE == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPQUERYTYPE.IDGPQUERYTYPE.FieldName, " = " + UsrCfg.InternoAtisNames.GPQUERYTYPE.NAME_TABLE + "." + UsrCfg.InternoAtisNames.GPQUERYTYPE.IDGPQUERYTYPE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDGPQUERYTYPE = " IN (" + StrIDGPQUERYTYPE + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPQUERYTYPE.IDGPQUERYTYPE.FieldName, StrIDGPQUERYTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/


        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.GPQUERYTYPE.IDGPQUERYTYPE.FieldName, GPQUERYTYPE.IDGPQUERYTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*    
        //********   GPQUERYTYPE_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPQUERYTYPE_DEL", @"GPQUERYTYPE_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE GPQUERYTYPE "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPQUERYTYPE=@[IDGPQUERYTYPE]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPQUERYTYPE_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//**********************   METHODS   ********************************************************************************************
Persistence.GP.Methods.GPQUERYTYPE_ListSetID = function (GPQUERYTYPEList, IDGPQUERYTYPE) {
    for (i = 0; i < GPQUERYTYPEList.length; i++) {
        if (IDGPQUERYTYPE == GPQUERYTYPEList[i].IDGPQUERYTYPE)
            return (GPQUERYTYPEList[i]);
    }
    var UnGPQUERYTYPE = new Persistence.Properties.TGPQUERYTYPE;
    UnGPQUERYTYPE.IDGPQUERYTYPE = IDGPQUERYTYPE;
    return (UnGPQUERYTYPE);
}
Persistence.GP.Methods.GPQUERYTYPE_ListAdd = function (GPQUERYTYPEList, GPQUERYTYPE) {
    var i = Persistence.GP.Methods.GPQUERYTYPE_ListGetIndex(GPQUERYTYPEList, GPQUERYTYPE);
    if (i == -1) GPQUERYTYPEList.push(GPQUERYTYPE);
    else {
        GPQUERYTYPEList[i].IDGPQUERYTYPE = GPQUERYTYPE.IDGPQUERYTYPE;
        GPQUERYTYPEList[i].QUERYTYPENAME = GPQUERYTYPE.QUERYTYPENAME;

    }
}
//CJRC_26072018
Persistence.GP.Methods.GPQUERYTYPE_ListGetIndex = function (GPQUERYTYPEList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.GP.Methods.GPQUERYTYPE_ListGetIndexIDGPQUERYTYPE(GPQUERYTYPEList, Param) ;

    }
    else {
        Res = Persistence.GP.Methods.GPQUERYTYPE_ListGetIndexGPQUERYTYPE(GPQUERYTYPEList, Param);
    }
    return (Res);
}

Persistence.GP.Methods.GPQUERYTYPE_ListGetIndexGPQUERYTYPE = function (GPQUERYTYPEList, GPQUERYTYPE) {
    for (i = 0; i < GPQUERYTYPEList.length; i++) {
        if (GPQUERYTYPE.IDGPQUERYTYPE == GPQUERYTYPEList[i].IDGPQUERYTYPE)
            return (i);
    }
    return (-1);
}
Persistence.GP.Methods.GPQUERYTYPE_ListGetIndexIDGPQUERYTYPE = function (GPQUERYTYPEList, IDGPQUERYTYPE) {
    for (i = 0; i < GPQUERYTYPEList.length; i++) {
        if (IDGPQUERYTYPE == GPQUERYTYPEList[i].IDGPQUERYTYPE)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.GP.Methods.GPQUERYTYPEListtoStr = function (GPQUERYTYPEList) {
    var Res = Persistence.Properties._Version + "(1)0";
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(GPQUERYTYPEList.length, Longitud, Texto);
        for (Counter = 0; Counter < GPQUERYTYPEList.length; Counter++) {
            var UnGPQUERYTYPE = GPQUERYTYPEList[Counter];
            UnGPQUERYTYPE.ToStr(Longitud, Texto);
        }

        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
}
Persistence.GP.Methods.StrtoGPQUERYTYPE = function (ProtocoloStr, GPQUERYTYPEList) {

    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        GPQUERYTYPEList.length = 0;
        if (Persistence.GP.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")") - ProtocoloStr.indexOf("(") - 1);
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.Length - ProtocoloStr.indexOf(")") - 1);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnGPQUERYTYPE = new Persistence.Properties.TGPQUERYTYPE(); //Mode new row
                UnGPQUERYTYPE.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.GP.GPQUERYTYPE_ListAdd(GPQUERYTYPEList, UnGPQUERYTYPE);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}


//Socket Function 
Persistence.GP.Methods.GPQUERYTYPEListToByte = function (GPQUERYTYPEList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, GPQUERYTYPEList.Count - idx);
    for (i = idx; i < GPQUERYTYPEList.length; i++) {
        GPQUERYTYPEList[i].ToByte(MemStream);
    }
}
Persistence.GP.Methods.ByteToGPQUERYTYPEList = function (GPQUERYTYPEList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnGPQUERYTYPE = new Persistence.Properties.TGPQUERYTYPE();
        UnGPQUERYTYPE.ByteTo(MemStream);
        Methods.GPQUERYTYPE_ListAdd(GPQUERYTYPEList, UnGPQUERYTYPE);
    }
}

