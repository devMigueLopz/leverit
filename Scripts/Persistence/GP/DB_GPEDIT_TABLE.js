﻿//**********************   PROPERTIES   *****************************************************************************************
Persistence.GP.Properties.TGPEDIT_TABLE = function () {
    this.IDGPEDIT_TABLE = 0;
    this.IDGPQUERY = 0;
    this.EDIT_TABLENAME = "";
    this.EDIT_TABLEORDER = 0;

    this.GPQUERY = new Persistence.GP.Properties.TGPQUERY();
    this.GPEDIT_COLUMNEVENT_List = new Array();

    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDGPEDIT_TABLE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDGPQUERY);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.EDIT_TABLENAME);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.EDIT_TABLEORDER);

    }
    this.ByteTo = function (MemStream) {
        this.IDGPEDIT_TABLE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDGPQUERY = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.EDIT_TABLENAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.EDIT_TABLEORDER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);

    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDGPEDIT_TABLE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDGPQUERY, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.EDIT_TABLENAME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.EDIT_TABLEORDER, Longitud, Texto);

    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDGPEDIT_TABLE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDGPQUERY = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.EDIT_TABLENAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.EDIT_TABLEORDER = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);

    }
}

//**********************   METHODS server  ********************************************************************************************
Persistence.GP.Methods.GPEDIT_TABLE_Fill = function (GPEDIT_TABLEList, StrIDGPEDIT_TABLE/*noin IDGPEDIT_TABLE*/) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    GPEDIT_TABLEList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDGPEDIT_TABLE == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPEDIT_TABLE.IDGPEDIT_TABLE.FieldName, " = " + UsrCfg.InternoAtisNames.GPEDIT_TABLE.NAME_TABLE + "." + UsrCfg.InternoAtisNames.GPEDIT_TABLE.IDGPEDIT_TABLE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDGPEDIT_TABLE = " IN (" + StrIDGPEDIT_TABLE + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPEDIT_TABLE.IDGPEDIT_TABLE.FieldName, StrIDGPEDIT_TABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //noin Param.AddInt32(UsrCfg.InternoDemoNames.GPEDIT_TABLE.IDGPEDIT_TABLE.FieldName, IDGPEDIT_TABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {
        /*
        //********   GPEDIT_TABLE_GET   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPEDIT_TABLE_GET", @"GPEDIT_TABLE_GET description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   IDGPEDIT_TABLE, "); 
         UnSQL.SQL.Add(@"   IDGPQUERY, "); 
         UnSQL.SQL.Add(@"   EDIT_TABLENAME, "); 
         UnSQL.SQL.Add(@"   EDIT_TABLEORDER "); 
         UnSQL.SQL.Add(@"   FROM GPEDIT_TABLE "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPEDIT_TABLE=@[IDGPEDIT_TABLE] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_GPEDIT_TABLE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "GPEDIT_TABLE_GET", Param.ToBytes());
        ResErr = DS_GPEDIT_TABLE.ResErr;
        if (ResErr.NotError) {
            if (DS_GPEDIT_TABLE.DataSet.RecordCount > 0) {
                DS_GPEDIT_TABLE.DataSet.First();
                while (!(DS_GPEDIT_TABLE.DataSet.Eof)) {
                    var GPEDIT_TABLE = new Persistence.GP.Properties.TGPEDIT_TABLE();
                    GPEDIT_TABLE.IDGPEDIT_TABLE = DS_GPEDIT_TABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_TABLE.IDGPEDIT_TABLE.FieldName).asInt32();
                    GPEDIT_TABLE.IDGPQUERY = DS_GPEDIT_TABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_TABLE.IDGPQUERY.FieldName).asInt32();
                    GPEDIT_TABLE.EDIT_TABLENAME = DS_GPEDIT_TABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_TABLE.EDIT_TABLENAME.FieldName).asString();
                    GPEDIT_TABLE.EDIT_TABLEORDER = DS_GPEDIT_TABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_TABLE.EDIT_TABLEORDER.FieldName).asInt32();

                    //Other
                    GPEDIT_TABLEList.push(GPEDIT_TABLE);
                    DS_GPEDIT_TABLE.DataSet.Next();
                }
            }
            else {
                DS_GPEDIT_TABLE.ResErr.NotError = false;
                DS_GPEDIT_TABLE.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPEDIT_TABLE_GETID = function (GPEDIT_TABLE) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPEDIT_TABLE.IDGPEDIT_TABLE.FieldName, GPEDIT_TABLE.IDGPEDIT_TABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPEDIT_TABLE.IDGPQUERY.FieldName, GPEDIT_TABLE.IDGPQUERY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPEDIT_TABLE.EDIT_TABLENAME.FieldName, GPEDIT_TABLE.EDIT_TABLENAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.GPEDIT_TABLE.EDIT_TABLEORDER.FieldName, GPEDIT_TABLE.EDIT_TABLEORDER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

        /*
        //********   GPEDIT_TABLE_GETID   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPEDIT_TABLE_GETID", @"GPEDIT_TABLE_GETID description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   FROM GPEDIT_TABLE "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPQUERY=@[IDGPQUERY] And "); 
         UnSQL.SQL.Add(@"   EDIT_TABLENAME=@[EDIT_TABLENAME] And "); 
         UnSQL.SQL.Add(@"   EDIT_TABLEORDER=@[EDIT_TABLEORDER] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_GPEDIT_TABLE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "GPEDIT_TABLE_GETID", Param.ToBytes());
        ResErr = DS_GPEDIT_TABLE.ResErr;
        if (ResErr.NotError) {
            if (DS_GPEDIT_TABLE.DataSet.RecordCount > 0) {
                DS_GPEDIT_TABLE.DataSet.First();
                GPEDIT_TABLE.IDGPEDIT_TABLE = DS_GPEDIT_TABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_TABLE.IDGPEDIT_TABLE.FieldName).asInt32();
            }
            else {
                DS_GPEDIT_TABLE.ResErr.NotError = false;
                DS_GPEDIT_TABLE.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPEDIT_TABLE_ADD = function (GPEDIT_TABLE) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPEDIT_TABLE.IDGPEDIT_TABLE.FieldName, GPEDIT_TABLE.IDGPEDIT_TABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPEDIT_TABLE.IDGPQUERY.FieldName, GPEDIT_TABLE.IDGPQUERY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPEDIT_TABLE.EDIT_TABLENAME.FieldName, GPEDIT_TABLE.EDIT_TABLENAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.GPEDIT_TABLE.EDIT_TABLEORDER.FieldName, GPEDIT_TABLE.EDIT_TABLEORDER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        /* 
        //********   GPEDIT_TABLE_ADD   ************************* 
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPEDIT_TABLE_ADD", @"GPEDIT_TABLE_ADD description."); 
        UnSQL.SQL.Add(@"   INSERT INTO GPEDIT_TABLE( "); 
         UnSQL.SQL.Add(@"   IDGPQUERY, "); 
         UnSQL.SQL.Add(@"   EDIT_TABLENAME, "); 
         UnSQL.SQL.Add(@"   EDIT_TABLEORDER "); 
         UnSQL.SQL.Add(@"   ) Values ( ");  
        UnSQL.SQL.Add(@"   @[IDGPQUERY], "); 
         UnSQL.SQL.Add(@"   @[EDIT_TABLENAME], "); 
         UnSQL.SQL.Add(@"   @[EDIT_TABLEORDER] "); 
         UnSQL.SQL.Add(@"   )  "); 
        UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPEDIT_TABLE_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.GP.Methods.GPEDIT_TABLE_GETID(GPEDIT_TABLE);
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPEDIT_TABLE_UPD = function (GPEDIT_TABLE) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPEDIT_TABLE.IDGPEDIT_TABLE.FieldName, GPEDIT_TABLE.IDGPEDIT_TABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPEDIT_TABLE.IDGPQUERY.FieldName, GPEDIT_TABLE.IDGPQUERY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPEDIT_TABLE.EDIT_TABLENAME.FieldName, GPEDIT_TABLE.EDIT_TABLENAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.GPEDIT_TABLE.EDIT_TABLEORDER.FieldName, GPEDIT_TABLE.EDIT_TABLEORDER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);


        /*   
        //********   GPEDIT_TABLE_UPD   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPEDIT_TABLE_UPD", @"GPEDIT_TABLE_UPD description.");  
        UnSQL.SQL.Add(@"  UPDATE GPEDIT_TABLE Set "); 
         UnSQL.SQL.Add(@"   IDGPQUERY=@[IDGPQUERY], "); 
         UnSQL.SQL.Add(@"   EDIT_TABLENAME=@[EDIT_TABLENAME], "); 
         UnSQL.SQL.Add(@"   EDIT_TABLEORDER=@[EDIT_TABLEORDER] "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPEDIT_TABLE=@[IDGPEDIT_TABLE] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPEDIT_TABLE_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
//CJRC_27072018
Persistence.GP.Methods.GPEDIT_TABLE_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.GP.Methods.GPEDIT_TABLE_DELIDGPEDIT_TABLE(/*String StrIDGPEDIT_TABLE*/Param);
    }
    else {
        Res = Persistence.GP.Methods.GPEDIT_TABLE_DELGPEDIT_TABLE(Param/*GPEDIT_TABLEList*/);
    }
    return (Res);
}

Persistence.GP.Methods.GPEDIT_TABLE_DELIDGPEDIT_TABLE = function (/*String StrIDGPEDIT_TABLE*/IDGPEDIT_TABLE) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDGPEDIT_TABLE == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPEDIT_TABLE.IDGPEDIT_TABLE.FieldName, " = " + UsrCfg.InternoAtisNames.GPEDIT_TABLE.NAME_TABLE + "." + UsrCfg.InternoAtisNames.GPEDIT_TABLE.IDGPEDIT_TABLE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDGPEDIT_TABLE = " IN (" + StrIDGPEDIT_TABLE + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPEDIT_TABLE.IDGPEDIT_TABLE.FieldName, StrIDGPEDIT_TABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.GPEDIT_TABLE.IDGPEDIT_TABLE.FieldName, IDGPEDIT_TABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   GPEDIT_TABLE_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPEDIT_TABLE_DEL", @"GPEDIT_TABLE_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE GPEDIT_TABLE "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPEDIT_TABLE=@[IDGPEDIT_TABLE]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPEDIT_TABLE_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPEDIT_TABLE_DELGPEDIT_TABLE = function (GPEDIT_TABLE/*GPEDIT_TABLEList*/) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        /*
        StrIDGPEDIT_TABLE = "";
        StrIDOTRO = "";
        for (var i = 0; i < GPEDIT_TABLEList.length; i++) {
        StrIDGPEDIT_TABLE += GPEDIT_TABLEList[i].IDGPEDIT_TABLE + ",";
        StrIDOTRO += GPEDIT_TABLEList[i].IDOTRO + ",";
        }
        StrIDGPEDIT_TABLE = StrIDGPEDIT_TABLE.substring(0, StrIDGPEDIT_TABLE.length - 1);
        StrIDOTRO = StrIDOTRO.substring(0, StrIDOTRO.length - 1);
        
        
        if (StrIDGPEDIT_TABLE == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPEDIT_TABLE.IDGPEDIT_TABLE.FieldName, " = " + UsrCfg.InternoAtisNames.GPEDIT_TABLE.NAME_TABLE + "." + UsrCfg.InternoAtisNames.GPEDIT_TABLE.IDGPEDIT_TABLE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDGPEDIT_TABLE = " IN (" + StrIDGPEDIT_TABLE + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPEDIT_TABLE.IDGPEDIT_TABLE.FieldName, StrIDGPEDIT_TABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/


        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.GPEDIT_TABLE.IDGPEDIT_TABLE.FieldName, GPEDIT_TABLE.IDGPEDIT_TABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*    
        //********   GPEDIT_TABLE_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPEDIT_TABLE_DEL", @"GPEDIT_TABLE_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE GPEDIT_TABLE "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPEDIT_TABLE=@[IDGPEDIT_TABLE]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPEDIT_TABLE_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

Persistence.GP.Methods.GPEDIT_TABLE_GET_BY_GPQUERY = function (GPEDIT_TABLEList, IDGPQUERY)
{
    var ResErr = new SysCfg.Error.Properties.TResErr();
    GPEDIT_TABLEList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();

    Param.AddInt32(UsrCfg.InternoAtisNames.GPEDIT_TABLE.IDGPQUERY.FieldName, IDGPQUERY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

    try
    {
        var DS_GPEDIT_TABLE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "GPEDIT_TABLE_SELECTBYGPQUERY", Param.ToBytes());
        ResErr = DS_GPEDIT_TABLE.ResErr;
        if (ResErr.NotError)
        {
            if (DS_GPEDIT_TABLE.DataSet.RecordCount > 0)
            {
                DS_GPEDIT_TABLE.DataSet.First();
                while (!(DS_GPEDIT_TABLE.DataSet.Eof))
                {
                    var GPEDIT_TABLE = new Persistence.GP.Properties.TGPEDIT_TABLE();
                    GPEDIT_TABLE.IDGPEDIT_TABLE = DS_GPEDIT_TABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_TABLE.IDGPEDIT_TABLE.FieldName).asInt32();
                    GPEDIT_TABLE.IDGPQUERY = DS_GPEDIT_TABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_TABLE.IDGPQUERY.FieldName).asInt32();
                    GPEDIT_TABLE.EDIT_TABLENAME = DS_GPEDIT_TABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_TABLE.EDIT_TABLENAME.FieldName).asString();
                    GPEDIT_TABLE.EDIT_TABLEORDER = DS_GPEDIT_TABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_TABLE.EDIT_TABLEORDER.FieldName).asInt32();

                    //Other
                    GPEDIT_TABLEList.push(GPEDIT_TABLE);
                    DS_GPEDIT_TABLE.DataSet.Next();
                }
            }
            else
            {
                DS_GPEDIT_TABLE.ResErr.NotError = false;
                DS_GPEDIT_TABLE.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}

//**********************   METHODS   ********************************************************************************************
Persistence.GP.Methods.GPEDIT_TABLE_ListSetID = function (GPEDIT_TABLEList, IDGPEDIT_TABLE) {
    for (i = 0; i < GPEDIT_TABLEList.length; i++) {
        if (IDGPEDIT_TABLE == GPEDIT_TABLEList[i].IDGPEDIT_TABLE)
            return (GPEDIT_TABLEList[i]);
    }
    var UnGPEDIT_TABLE = new Persistence.Properties.TGPEDIT_TABLE;
    UnGPEDIT_TABLE.IDGPEDIT_TABLE = IDGPEDIT_TABLE;
    return (UnGPEDIT_TABLE);
}
Persistence.GP.Methods.GPEDIT_TABLE_ListAdd = function (GPEDIT_TABLEList, GPEDIT_TABLE) {
    var i = Persistence.GP.Methods.GPEDIT_TABLE_ListGetIndex(GPEDIT_TABLEList, GPEDIT_TABLE);
    if (i == -1) GPEDIT_TABLEList.push(GPEDIT_TABLE);
    else {
        GPEDIT_TABLEList[i].IDGPEDIT_TABLE = GPEDIT_TABLE.IDGPEDIT_TABLE;
        GPEDIT_TABLEList[i].IDGPQUERY = GPEDIT_TABLE.IDGPQUERY;
        GPEDIT_TABLEList[i].EDIT_TABLENAME = GPEDIT_TABLE.EDIT_TABLENAME;
        GPEDIT_TABLEList[i].EDIT_TABLEORDER = GPEDIT_TABLE.EDIT_TABLEORDER;

    }
}
//CJRC_26072018
Persistence.GP.Methods.GPEDIT_TABLE_ListGetIndex = function (GPEDIT_TABLEList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.GP.Methods.GPEDIT_TABLE_ListGetIndexIDGPEDIT_TABLE(GPEDIT_TABLEList, Param);

    }
    else {
        Res = Persistence.GP.Methods.GPEDIT_TABLE_ListGetIndexGPEDIT_TABLE(GPEDIT_TABLEList, Param);
    }
    return (Res);
}

Persistence.GP.Methods.GPEDIT_TABLE_ListGetIndexGPEDIT_TABLE = function (GPEDIT_TABLEList, GPEDIT_TABLE) {
    for (i = 0; i < GPEDIT_TABLEList.length; i++) {
        if (GPEDIT_TABLE.IDGPEDIT_TABLE == GPEDIT_TABLEList[i].IDGPEDIT_TABLE)
            return (i);
    }
    return (-1);
}
Persistence.GP.Methods.GPEDIT_TABLE_ListGetIndexIDGPEDIT_TABLE = function (GPEDIT_TABLEList, IDGPEDIT_TABLE) {
    for (i = 0; i < GPEDIT_TABLEList.length; i++) {
        if (IDGPEDIT_TABLE == GPEDIT_TABLEList[i].IDGPEDIT_TABLE)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.GP.Methods.GPEDIT_TABLEListtoStr = function (GPEDIT_TABLEList) {
    var Res = Persistence.Properties._Version + "(1)0";
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(GPEDIT_TABLEList.length, Longitud, Texto);
        for (Counter = 0; Counter < GPEDIT_TABLEList.length; Counter++) {
            var UnGPEDIT_TABLE = GPEDIT_TABLEList[Counter];
            UnGPEDIT_TABLE.ToStr(Longitud, Texto);
        }

        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
}
Persistence.GP.Methods.StrtoGPEDIT_TABLE = function (ProtocoloStr, GPEDIT_TABLEList) {

    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        GPEDIT_TABLEList.length = 0;
        if (Persistence.GP.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")") - ProtocoloStr.indexOf("(") - 1);
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.Length - ProtocoloStr.indexOf(")") - 1);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnGPEDIT_TABLE = new Persistence.Properties.TGPEDIT_TABLE(); //Mode new row
                UnGPEDIT_TABLE.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.GP.GPEDIT_TABLE_ListAdd(GPEDIT_TABLEList, UnGPEDIT_TABLE);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}


//Socket Function 
Persistence.GP.Methods.GPEDIT_TABLEListToByte = function (GPEDIT_TABLEList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, GPEDIT_TABLEList.Count - idx);
    for (i = idx; i < GPEDIT_TABLEList.length; i++) {
        GPEDIT_TABLEList[i].ToByte(MemStream);
    }
}
Persistence.GP.Methods.ByteToGPEDIT_TABLEList = function (GPEDIT_TABLEList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnGPEDIT_TABLE = new Persistence.Properties.TGPEDIT_TABLE();
        UnGPEDIT_TABLE.ByteTo(MemStream);
        Methods.GPEDIT_TABLE_ListAdd(GPEDIT_TABLEList, UnGPEDIT_TABLE);
    }
}
