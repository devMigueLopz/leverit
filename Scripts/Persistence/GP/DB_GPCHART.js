﻿//**********************   PROPERTIES   *****************************************************************************************
Persistence.GP.Properties.TGPCHART = function () {
    this.IDGPCHART = 0;
    this.IDGPQUERY = 0;
    this.NAME = "";
    this.DESCRIPTION = "";
    this.STRSQL = "";
    this.XFIELDNAME = "";
    this.YFIELDNAME = "";
    this.SERIESGROUP = "";
    this.ISGAUGE = "";
    this.ISCHART = "";
    this.MINVALUE = "";
    this.MAXVALUE = "";
    this.GAUGERANGE = "";
    this.GAUGETYPE = "";
    this.CONFIGURATION = "";

    this.GPQUERY = new Persistence.GP.Properties.TGPQUERY();

    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDGPCHART);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDGPQUERY);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.NAME);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.DESCRIPTION);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.STRSQL);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.XFIELDNAME);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.YFIELDNAME);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.SERIESGROUP);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.ISGAUGE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.ISCHART);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.MINVALUE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.MAXVALUE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.GAUGERANGE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.GAUGETYPE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CONFIGURATION);

    }
    this.ByteTo = function (MemStream) {
        this.IDGPCHART = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDGPQUERY = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.NAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.DESCRIPTION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.STRSQL = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.XFIELDNAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.YFIELDNAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.SERIESGROUP = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.ISGAUGE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.ISCHART = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.MINVALUE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.MAXVALUE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.GAUGERANGE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.GAUGETYPE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CONFIGURATION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);

    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDGPCHART, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDGPQUERY, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.NAME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.DESCRIPTION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.STRSQL, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.XFIELDNAME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.YFIELDNAME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.SERIESGROUP, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.ISGAUGE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.ISCHART, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.MINVALUE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.MAXVALUE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.GAUGERANGE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.GAUGETYPE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.CONFIGURATION, Longitud, Texto);

    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDGPCHART = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDGPQUERY = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.NAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.DESCRIPTION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.STRSQL = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.XFIELDNAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.YFIELDNAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.SERIESGROUP = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.ISGAUGE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.ISCHART = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.MINVALUE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.MAXVALUE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.GAUGERANGE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.GAUGETYPE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.CONFIGURATION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);

    }
}

//**********************   METHODS server  ********************************************************************************************
Persistence.GP.Methods.GPCHART_Fill = function (GPCHARTList, StrIDGPCHART/*noin IDGPCHART*/) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    GPCHARTList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDGPCHART == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPCHART.IDGPCHART.FieldName, " = " + UsrCfg.InternoAtisNames.GPCHART.NAME_TABLE + "." + UsrCfg.InternoAtisNames.GPCHART.IDGPCHART.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDGPCHART = " IN (" + StrIDGPCHART + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPCHART.IDGPCHART.FieldName, StrIDGPCHART, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //noin Param.AddInt32(UsrCfg.InternoDemoNames.GPCHART.IDGPCHART.FieldName, IDGPCHART, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {
        var DS_GPCHART = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "GPCHART_GET", Param.ToBytes());
        ResErr = DS_GPCHART.ResErr;
        if (ResErr.NotError) {
            if (DS_GPCHART.DataSet.RecordCount > 0) {
                DS_GPCHART.DataSet.First();
                while (!(DS_GPCHART.DataSet.Eof)) {
                    var GPCHART = new Persistence.GP.Properties.TGPCHART();
                    GPCHART.IDGPCHART = DS_GPCHART.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCHART.IDGPCHART.FieldName).asInt32();
                    GPCHART.IDGPQUERY = DS_GPCHART.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCHART.IDGPQUERY.FieldName).asInt32();
                    GPCHART.NAME = DS_GPCHART.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCHART.NAME.FieldName).asString();
                    GPCHART.DESCRIPTION = DS_GPCHART.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCHART.DESCRIPTION.FieldName).asText();
                    GPCHART.STRSQL = DS_GPCHART.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCHART.STRSQL.FieldName).asText();
                    GPCHART.XFIELDNAME = DS_GPCHART.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCHART.XFIELDNAME.FieldName).asString();
                    GPCHART.YFIELDNAME = DS_GPCHART.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCHART.YFIELDNAME.FieldName).asString();
                    GPCHART.SERIESGROUP = DS_GPCHART.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCHART.SERIESGROUP.FieldName).asString();
                    GPCHART.ISGAUGE = DS_GPCHART.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCHART.ISGAUGE.FieldName).asString();
                    GPCHART.ISCHART = DS_GPCHART.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCHART.ISCHART.FieldName).asString();
                    GPCHART.MINVALUE = DS_GPCHART.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCHART.MINVALUE.FieldName).asString();
                    GPCHART.MAXVALUE = DS_GPCHART.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCHART.MAXVALUE.FieldName).asString();
                    GPCHART.GAUGERANGE = DS_GPCHART.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCHART.GAUGERANGE.FieldName).asString();
                    GPCHART.GAUGETYPE = DS_GPCHART.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCHART.GAUGETYPE.FieldName).asString();
                    GPCHART.CONFIGURATION = DS_GPCHART.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCHART.CONFIGURATION.FieldName).asText();

                    //Other
                    GPCHARTList.push(GPCHART);
                    DS_GPCHART.DataSet.Next();
                }
            }
            else {
                DS_GPCHART.ResErr.NotError = false;
                DS_GPCHART.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPCHART_GETID = function (GPCHART) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPCHART.IDGPCHART.FieldName, GPCHART.IDGPCHART, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPCHART.IDGPQUERY.FieldName, GPCHART.IDGPQUERY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPCHART.NAME.FieldName, GPCHART.NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddText(UsrCfg.InternoAtisNames.GPCHART.DESCRIPTION.FieldName, GPCHART.DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddText(UsrCfg.InternoAtisNames.GPCHART.STRSQL.FieldName, GPCHART.STRSQL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPCHART.XFIELDNAME.FieldName, GPCHART.XFIELDNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPCHART.YFIELDNAME.FieldName, GPCHART.YFIELDNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPCHART.SERIESGROUP.FieldName, GPCHART.SERIESGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPCHART.ISGAUGE.FieldName, GPCHART.ISGAUGE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPCHART.ISCHART.FieldName, GPCHART.ISCHART, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPCHART.MINVALUE.FieldName, GPCHART.MINVALUE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPCHART.MAXVALUE.FieldName, GPCHART.MAXVALUE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPCHART.GAUGERANGE.FieldName, GPCHART.GAUGERANGE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPCHART.GAUGETYPE.FieldName, GPCHART.GAUGETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddText(UsrCfg.InternoAtisNames.GPCHART.CONFIGURATION.FieldName, GPCHART.CONFIGURATION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

        /*
        //********   GPCHART_GETID   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPCHART_GETID", @"GPCHART_GETID description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   FROM GPCHART "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPQUERY=@[IDGPQUERY] And "); 
         UnSQL.SQL.Add(@"   NAME=@[NAME] And "); 
         UnSQL.SQL.Add(@"   XFIELDNAME=@[XFIELDNAME] And "); 
         UnSQL.SQL.Add(@"   YFIELDNAME=@[YFIELDNAME] And "); 
         UnSQL.SQL.Add(@"   SERIESGROUP=@[SERIESGROUP] And "); 
         UnSQL.SQL.Add(@"   ISGAUGE=@[ISGAUGE] And "); 
         UnSQL.SQL.Add(@"   ISCHART=@[ISCHART] And "); 
         UnSQL.SQL.Add(@"   MINVALUE=@[MINVALUE] And "); 
         UnSQL.SQL.Add(@"   MAXVALUE=@[MAXVALUE] And "); 
         UnSQL.SQL.Add(@"   GAUGERANGE=@[GAUGERANGE] And "); 
         UnSQL.SQL.Add(@"   GAUGETYPE=@[GAUGETYPE] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_GPCHART = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "GPCHART_GETID", Param.ToBytes());
        ResErr = DS_GPCHART.ResErr;
        if (ResErr.NotError) {
            if (DS_GPCHART.DataSet.RecordCount > 0) {
                DS_GPCHART.DataSet.First();
                GPCHART.IDGPCHART = DS_GPCHART.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPCHART.IDGPCHART.FieldName).asInt32();
            }
            else {
                DS_GPCHART.ResErr.NotError = false;
                DS_GPCHART.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPCHART_ADD = function (GPCHART) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPCHART.IDGPCHART.FieldName, GPCHART.IDGPCHART, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPCHART.IDGPQUERY.FieldName, GPCHART.IDGPQUERY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPCHART.NAME.FieldName, GPCHART.NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.GPCHART.DESCRIPTION.FieldName, GPCHART.DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.GPCHART.STRSQL.FieldName, GPCHART.STRSQL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPCHART.XFIELDNAME.FieldName, GPCHART.XFIELDNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPCHART.YFIELDNAME.FieldName, GPCHART.YFIELDNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPCHART.SERIESGROUP.FieldName, GPCHART.SERIESGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPCHART.ISGAUGE.FieldName, GPCHART.ISGAUGE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPCHART.ISCHART.FieldName, GPCHART.ISCHART, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPCHART.MINVALUE.FieldName, GPCHART.MINVALUE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPCHART.MAXVALUE.FieldName, GPCHART.MAXVALUE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPCHART.GAUGERANGE.FieldName, GPCHART.GAUGERANGE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPCHART.GAUGETYPE.FieldName, GPCHART.GAUGETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.GPCHART.CONFIGURATION.FieldName, GPCHART.CONFIGURATION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        /* 
        //********   GPCHART_ADD   ************************* 
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPCHART_ADD", @"GPCHART_ADD description."); 
        UnSQL.SQL.Add(@"   INSERT INTO GPCHART( "); 
         UnSQL.SQL.Add(@"   IDGPQUERY, "); 
         UnSQL.SQL.Add(@"   NAME, "); 
         UnSQL.SQL.Add(@"   DESCRIPTION, "); 
         UnSQL.SQL.Add(@"   STRSQL, "); 
         UnSQL.SQL.Add(@"   XFIELDNAME, "); 
         UnSQL.SQL.Add(@"   YFIELDNAME, "); 
         UnSQL.SQL.Add(@"   SERIESGROUP, "); 
         UnSQL.SQL.Add(@"   ISGAUGE, "); 
         UnSQL.SQL.Add(@"   ISCHART, "); 
         UnSQL.SQL.Add(@"   MINVALUE, "); 
         UnSQL.SQL.Add(@"   MAXVALUE, "); 
         UnSQL.SQL.Add(@"   GAUGERANGE, "); 
         UnSQL.SQL.Add(@"   GAUGETYPE, "); 
         UnSQL.SQL.Add(@"   CONFIGURATION "); 
         UnSQL.SQL.Add(@"   ) Values ( ");  
        UnSQL.SQL.Add(@"   @[IDGPQUERY], "); 
         UnSQL.SQL.Add(@"   @[NAME], "); 
         UnSQL.SQL.Add(@"   @[DESCRIPTION], "); 
         UnSQL.SQL.Add(@"   @[STRSQL], "); 
         UnSQL.SQL.Add(@"   @[XFIELDNAME], "); 
         UnSQL.SQL.Add(@"   @[YFIELDNAME], "); 
         UnSQL.SQL.Add(@"   @[SERIESGROUP], "); 
         UnSQL.SQL.Add(@"   @[ISGAUGE], "); 
         UnSQL.SQL.Add(@"   @[ISCHART], "); 
         UnSQL.SQL.Add(@"   @[MINVALUE], "); 
         UnSQL.SQL.Add(@"   @[MAXVALUE], "); 
         UnSQL.SQL.Add(@"   @[GAUGERANGE], "); 
         UnSQL.SQL.Add(@"   @[GAUGETYPE], "); 
         UnSQL.SQL.Add(@"   @[CONFIGURATION] "); 
         UnSQL.SQL.Add(@"   )  "); 
        UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPCHART_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.GP.Methods.GPCHART_GETID(GPCHART);
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPCHART_UPD = function (GPCHART) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPCHART.IDGPCHART.FieldName, GPCHART.IDGPCHART, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPCHART.IDGPQUERY.FieldName, GPCHART.IDGPQUERY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPCHART.NAME.FieldName, GPCHART.NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.GPCHART.DESCRIPTION.FieldName, GPCHART.DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.GPCHART.STRSQL.FieldName, GPCHART.STRSQL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPCHART.XFIELDNAME.FieldName, GPCHART.XFIELDNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPCHART.YFIELDNAME.FieldName, GPCHART.YFIELDNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPCHART.SERIESGROUP.FieldName, GPCHART.SERIESGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPCHART.ISGAUGE.FieldName, GPCHART.ISGAUGE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPCHART.ISCHART.FieldName, GPCHART.ISCHART, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPCHART.MINVALUE.FieldName, GPCHART.MINVALUE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPCHART.MAXVALUE.FieldName, GPCHART.MAXVALUE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPCHART.GAUGERANGE.FieldName, GPCHART.GAUGERANGE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPCHART.GAUGETYPE.FieldName, GPCHART.GAUGETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.GPCHART.CONFIGURATION.FieldName, GPCHART.CONFIGURATION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);


        /*   
        //********   GPCHART_UPD   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPCHART_UPD", @"GPCHART_UPD description.");  
        UnSQL.SQL.Add(@"  UPDATE GPCHART Set "); 
         UnSQL.SQL.Add(@"   IDGPQUERY=@[IDGPQUERY], "); 
         UnSQL.SQL.Add(@"   NAME=@[NAME], "); 
         UnSQL.SQL.Add(@"   DESCRIPTION=@[DESCRIPTION], "); 
         UnSQL.SQL.Add(@"   STRSQL=@[STRSQL], "); 
         UnSQL.SQL.Add(@"   XFIELDNAME=@[XFIELDNAME], "); 
         UnSQL.SQL.Add(@"   YFIELDNAME=@[YFIELDNAME], "); 
         UnSQL.SQL.Add(@"   SERIESGROUP=@[SERIESGROUP], "); 
         UnSQL.SQL.Add(@"   ISGAUGE=@[ISGAUGE], "); 
         UnSQL.SQL.Add(@"   ISCHART=@[ISCHART], "); 
         UnSQL.SQL.Add(@"   MINVALUE=@[MINVALUE], "); 
         UnSQL.SQL.Add(@"   MAXVALUE=@[MAXVALUE], "); 
         UnSQL.SQL.Add(@"   GAUGERANGE=@[GAUGERANGE], "); 
         UnSQL.SQL.Add(@"   GAUGETYPE=@[GAUGETYPE], "); 
         UnSQL.SQL.Add(@"   CONFIGURATION=@[CONFIGURATION] "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPCHART=@[IDGPCHART] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPCHART_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.GP.Methods.GPCHART_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.GP.Methods.GPCHART_DELIDGPCHART(/*String StrIDGPCHART*/Param);
    }
    else {
        Res = Persistence.GP.Methods.GPCHART_DELGPCHART(Param/*GPCHARTList*/);
    }
    return (Res);
}

Persistence.GP.Methods.GPCHART_DELIDGPCHART = function (/*String StrIDGPCHART*/IDGPCHART) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDGPCHART == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPCHART.IDGPCHART.FieldName, " = " + UsrCfg.InternoAtisNames.GPCHART.NAME_TABLE + "." + UsrCfg.InternoAtisNames.GPCHART.IDGPCHART.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDGPCHART = " IN (" + StrIDGPCHART + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPCHART.IDGPCHART.FieldName, StrIDGPCHART, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.GPCHART.IDGPCHART.FieldName, IDGPCHART, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   GPCHART_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPCHART_DEL", @"GPCHART_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE GPCHART "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPCHART=@[IDGPCHART]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPCHART_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPCHART_DELGPCHART = function (GPCHART/*GPCHARTList*/) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        /*
        StrIDGPCHART = "";
        StrIDOTRO = "";
        for (var i = 0; i < GPCHARTList.length; i++) {
        StrIDGPCHART += GPCHARTList[i].IDGPCHART + ",";
        StrIDOTRO += GPCHARTList[i].IDOTRO + ",";
        }
        StrIDGPCHART = StrIDGPCHART.substring(0, StrIDGPCHART.length - 1);
        StrIDOTRO = StrIDOTRO.substring(0, StrIDOTRO.length - 1);
        
        
        if (StrIDGPCHART == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPCHART.IDGPCHART.FieldName, " = " + UsrCfg.InternoAtisNames.GPCHART.NAME_TABLE + "." + UsrCfg.InternoAtisNames.GPCHART.IDGPCHART.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDGPCHART = " IN (" + StrIDGPCHART + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPCHART.IDGPCHART.FieldName, StrIDGPCHART, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/


        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.GPCHART.IDGPCHART.FieldName, GPCHART.IDGPCHART, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*    
        //********   GPCHART_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPCHART_DEL", @"GPCHART_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE GPCHART "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPCHART=@[IDGPCHART]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPCHART_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//**********************   METHODS   ********************************************************************************************
Persistence.GP.Methods.GPCHART_ListSetID = function (GPCHARTList, IDGPCHART) {
    for (i = 0; i < GPCHARTList.length; i++) {
        if (IDGPCHART == GPCHARTList[i].IDGPCHART)
            return (GPCHARTList[i]);
    }
    var UnGPCHART = new Persistence.Properties.TGPCHART;
    UnGPCHART.IDGPCHART = IDGPCHART;
    return (UnGPCHART);
}
Persistence.GP.Methods.GPCHART_ListAdd = function (GPCHARTList, GPCHART) {
    var i = Persistence.GP.Methods.GPCHART_ListGetIndex(GPCHARTList, GPCHART);
    if (i == -1) GPCHARTList.push(GPCHART);
    else {
        GPCHARTList[i].IDGPCHART = GPCHART.IDGPCHART;
        GPCHARTList[i].IDGPQUERY = GPCHART.IDGPQUERY;
        GPCHARTList[i].NAME = GPCHART.NAME;
        GPCHARTList[i].DESCRIPTION = GPCHART.DESCRIPTION;
        GPCHARTList[i].STRSQL = GPCHART.STRSQL;
        GPCHARTList[i].XFIELDNAME = GPCHART.XFIELDNAME;
        GPCHARTList[i].YFIELDNAME = GPCHART.YFIELDNAME;
        GPCHARTList[i].SERIESGROUP = GPCHART.SERIESGROUP;
        GPCHARTList[i].ISGAUGE = GPCHART.ISGAUGE;
        GPCHARTList[i].ISCHART = GPCHART.ISCHART;
        GPCHARTList[i].MINVALUE = GPCHART.MINVALUE;
        GPCHARTList[i].MAXVALUE = GPCHART.MAXVALUE;
        GPCHARTList[i].GAUGERANGE = GPCHART.GAUGERANGE;
        GPCHARTList[i].GAUGETYPE = GPCHART.GAUGETYPE;
        GPCHARTList[i].CONFIGURATION = GPCHART.CONFIGURATION;

    }
}
//CJRC_26072018
Persistence.GP.Methods.GPCHART_ListGetIndex = function (GPCHARTList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.GP.Methods.GPCHART_ListGetIndexIDGPCHART(GPCHARTList, Param);

    }
    else {
        Res = Persistence.GP.Methods.GPCHART_ListGetIndexGPCHART(GPCHARTList, Param);
    }
    return (Res);
}

Persistence.GP.Methods.GPCHART_ListGetIndexGPCHART = function (GPCHARTList, GPCHART) {
    for (i = 0; i < GPCHARTList.length; i++) {
        if (GPCHART.IDGPCHART == GPCHARTList[i].IDGPCHART)
            return (i);
    }
    return (-1);
}
Persistence.GP.Methods.GPCHART_ListGetIndexIDGPCHART = function (GPCHARTList, IDGPCHART) {
    for (i = 0; i < GPCHARTList.length; i++) {
        if (IDGPCHART == GPCHARTList[i].IDGPCHART)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.GP.Methods.GPCHARTListtoStr = function (GPCHARTList) {
    var Res = Persistence.Properties._Version + "(1)0";
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(GPCHARTList.length, Longitud, Texto);
        for (Counter = 0; Counter < GPCHARTList.length; Counter++) {
            var UnGPCHART = GPCHARTList[Counter];
            UnGPCHART.ToStr(Longitud, Texto);
        }

        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
}
Persistence.GP.Methods.StrtoGPCHART = function (ProtocoloStr, GPCHARTList) {

    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        GPCHARTList.length = 0;
        if (Persistence.GP.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")") - ProtocoloStr.indexOf("(") - 1);
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.Length - ProtocoloStr.indexOf(")") - 1);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnGPCHART = new Persistence.Properties.TGPCHART(); //Mode new row
                UnGPCHART.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.GP.GPCHART_ListAdd(GPCHARTList, UnGPCHART);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}


//Socket Function 
Persistence.GP.Methods.GPCHARTListToByte = function (GPCHARTList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, GPCHARTList.Count - idx);
    for (i = idx; i < GPCHARTList.length; i++) {
        GPCHARTList[i].ToByte(MemStream);
    }
}
Persistence.GP.Methods.ByteToGPCHARTList = function (GPCHARTList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnGPCHART = new Persistence.Properties.TGPCHART();
        UnGPCHART.ByteTo(MemStream);
        Methods.GPCHART_ListAdd(GPCHARTList, UnGPCHART);
    }
}
