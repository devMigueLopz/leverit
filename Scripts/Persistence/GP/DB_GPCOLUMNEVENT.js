﻿//**********************   PROPERTIES   *****************************************************************************************
Persistence.GP.Properties.TGPEDIT_COLUMNEVENT = function () {
    this.IDGPEDIT_COLUMNEVENT = 0;
    this.IDGPEDIT_COLUMN = 0;
    this.IDGPEDIT_TABLE = 0;
    this.EVINSERT = "";
    this.EVDELETE = "";
    this.EVUPDATESET = "";
    this.EVUPDATEWHERE = "";

    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDGPEDIT_COLUMNEVENT);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDGPEDIT_COLUMN);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDGPEDIT_TABLE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.EVINSERT);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.EVDELETE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.EVUPDATESET);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.EVUPDATEWHERE);

    }
    this.ByteTo = function (MemStream) {
        this.IDGPEDIT_COLUMNEVENT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDGPEDIT_COLUMN = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDGPEDIT_TABLE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.EVINSERT = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.EVDELETE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.EVUPDATESET = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.EVUPDATEWHERE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);

    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDGPEDIT_COLUMNEVENT, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDGPEDIT_COLUMN, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDGPEDIT_TABLE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.EVINSERT, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.EVDELETE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.EVUPDATESET, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.EVUPDATEWHERE, Longitud, Texto);

    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDGPEDIT_COLUMNEVENT = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDGPEDIT_COLUMN = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDGPEDIT_TABLE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.EVINSERT = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.EVDELETE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.EVUPDATESET = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.EVUPDATEWHERE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);

    }
}

//**********************   METHODS server  ********************************************************************************************
Persistence.GP.Methods.GPEDIT_COLUMNEVENT_Fill = function (GPEDIT_COLUMNEVENTList, StrIDGPEDIT_COLUMNEVENT/*noin IDGPEDIT_COLUMNEVENT*/) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    GPEDIT_COLUMNEVENTList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDGPEDIT_COLUMNEVENT == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMNEVENT.FieldName, " = " + UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.NAME_TABLE + "." + UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMNEVENT.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDGPEDIT_COLUMNEVENT = " IN (" + StrIDGPEDIT_COLUMNEVENT + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMNEVENT.FieldName, StrIDGPEDIT_COLUMNEVENT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //noin Param.AddInt32(UsrCfg.InternoDemoNames.GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMNEVENT.FieldName, IDGPEDIT_COLUMNEVENT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {
        /*
        //********   GPEDIT_COLUMNEVENT_GET   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPEDIT_COLUMNEVENT_GET", @"GPEDIT_COLUMNEVENT_GET description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   IDGPEDIT_COLUMNEVENT, "); 
         UnSQL.SQL.Add(@"   IDGPEDIT_COLUMN, "); 
         UnSQL.SQL.Add(@"   IDGPEDIT_TABLE, "); 
         UnSQL.SQL.Add(@"   EVINSERT, "); 
         UnSQL.SQL.Add(@"   EVDELETE, "); 
         UnSQL.SQL.Add(@"   EVUPDATESET, "); 
         UnSQL.SQL.Add(@"   EVUPDATEWHERE "); 
         UnSQL.SQL.Add(@"   FROM GPEDIT_COLUMNEVENT "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPEDIT_COLUMNEVENT=@[IDGPEDIT_COLUMNEVENT] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_GPEDIT_COLUMNEVENT = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "GPEDIT_COLUMNEVENT_GET", Param.ToBytes());
        ResErr = DS_GPEDIT_COLUMNEVENT.ResErr;
        if (ResErr.NotError) {
            if (DS_GPEDIT_COLUMNEVENT.DataSet.RecordCount > 0) {
                DS_GPEDIT_COLUMNEVENT.DataSet.First();
                while (!(DS_GPEDIT_COLUMNEVENT.DataSet.Eof)) {
                    var GPEDIT_COLUMNEVENT = new Persistence.GP.Properties.TGPEDIT_COLUMNEVENT();
                    GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMNEVENT = DS_GPEDIT_COLUMNEVENT.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMNEVENT.FieldName).asInt32();
                    GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMN = DS_GPEDIT_COLUMNEVENT.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMN.FieldName).asInt32();
                    GPEDIT_COLUMNEVENT.IDGPEDIT_TABLE = DS_GPEDIT_COLUMNEVENT.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_TABLE.FieldName).asInt32();
                    GPEDIT_COLUMNEVENT.EVINSERT = DS_GPEDIT_COLUMNEVENT.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.EVINSERT.FieldName).asString();
                    GPEDIT_COLUMNEVENT.EVDELETE = DS_GPEDIT_COLUMNEVENT.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.EVDELETE.FieldName).asString();
                    GPEDIT_COLUMNEVENT.EVUPDATESET = DS_GPEDIT_COLUMNEVENT.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.EVUPDATESET.FieldName).asString();
                    GPEDIT_COLUMNEVENT.EVUPDATEWHERE = DS_GPEDIT_COLUMNEVENT.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.EVUPDATEWHERE.FieldName).asString();

                    //Other
                    GPEDIT_COLUMNEVENTList.push(GPEDIT_COLUMNEVENT);
                    DS_GPEDIT_COLUMNEVENT.DataSet.Next();
                }
            }
            else {
                DS_GPEDIT_COLUMNEVENT.ResErr.NotError = false;
                DS_GPEDIT_COLUMNEVENT.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPEDIT_COLUMNEVENT_GETID = function (GPEDIT_COLUMNEVENT) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMNEVENT.FieldName, GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMNEVENT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMN.FieldName, GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_TABLE.FieldName, GPEDIT_COLUMNEVENT.IDGPEDIT_TABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.EVINSERT.FieldName, GPEDIT_COLUMNEVENT.EVINSERT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.EVDELETE.FieldName, GPEDIT_COLUMNEVENT.EVDELETE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.EVUPDATESET.FieldName, GPEDIT_COLUMNEVENT.EVUPDATESET, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.EVUPDATEWHERE.FieldName, GPEDIT_COLUMNEVENT.EVUPDATEWHERE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

        /*
        //********   GPEDIT_COLUMNEVENT_GETID   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPEDIT_COLUMNEVENT_GETID", @"GPEDIT_COLUMNEVENT_GETID description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   FROM GPEDIT_COLUMNEVENT "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPEDIT_COLUMN=@[IDGPEDIT_COLUMN] And "); 
         UnSQL.SQL.Add(@"   IDGPEDIT_TABLE=@[IDGPEDIT_TABLE] And "); 
         UnSQL.SQL.Add(@"   EVINSERT=@[EVINSERT] And "); 
         UnSQL.SQL.Add(@"   EVDELETE=@[EVDELETE] And "); 
         UnSQL.SQL.Add(@"   EVUPDATESET=@[EVUPDATESET] And "); 
         UnSQL.SQL.Add(@"   EVUPDATEWHERE=@[EVUPDATEWHERE] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_GPEDIT_COLUMNEVENT = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "GPEDIT_COLUMNEVENT_GETID", Param.ToBytes());
        ResErr = DS_GPEDIT_COLUMNEVENT.ResErr;
        if (ResErr.NotError) {
            if (DS_GPEDIT_COLUMNEVENT.DataSet.RecordCount > 0) {
                DS_GPEDIT_COLUMNEVENT.DataSet.First();
                GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMNEVENT = DS_GPEDIT_COLUMNEVENT.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMNEVENT.FieldName).asInt32();
            }
            else {
                DS_GPEDIT_COLUMNEVENT.ResErr.NotError = false;
                DS_GPEDIT_COLUMNEVENT.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPEDIT_COLUMNEVENT_ADD = function (GPEDIT_COLUMNEVENT) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMNEVENT.FieldName, GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMNEVENT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMN.FieldName, GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_TABLE.FieldName, GPEDIT_COLUMNEVENT.IDGPEDIT_TABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.EVINSERT.FieldName, GPEDIT_COLUMNEVENT.EVINSERT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.EVDELETE.FieldName, GPEDIT_COLUMNEVENT.EVDELETE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.EVUPDATESET.FieldName, GPEDIT_COLUMNEVENT.EVUPDATESET, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.EVUPDATEWHERE.FieldName, GPEDIT_COLUMNEVENT.EVUPDATEWHERE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        /* 
        //********   GPEDIT_COLUMNEVENT_ADD   ************************* 
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPEDIT_COLUMNEVENT_ADD", @"GPEDIT_COLUMNEVENT_ADD description."); 
        UnSQL.SQL.Add(@"   INSERT INTO GPEDIT_COLUMNEVENT( "); 
         UnSQL.SQL.Add(@"   IDGPEDIT_COLUMN, "); 
         UnSQL.SQL.Add(@"   IDGPEDIT_TABLE, "); 
         UnSQL.SQL.Add(@"   EVINSERT, "); 
         UnSQL.SQL.Add(@"   EVDELETE, "); 
         UnSQL.SQL.Add(@"   EVUPDATESET, "); 
         UnSQL.SQL.Add(@"   EVUPDATEWHERE "); 
         UnSQL.SQL.Add(@"   ) Values ( ");  
        UnSQL.SQL.Add(@"   @[IDGPEDIT_COLUMN], "); 
         UnSQL.SQL.Add(@"   @[IDGPEDIT_TABLE], "); 
         UnSQL.SQL.Add(@"   @[EVINSERT], "); 
         UnSQL.SQL.Add(@"   @[EVDELETE], "); 
         UnSQL.SQL.Add(@"   @[EVUPDATESET], "); 
         UnSQL.SQL.Add(@"   @[EVUPDATEWHERE] "); 
         UnSQL.SQL.Add(@"   )  "); 
        UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPEDIT_COLUMNEVENT_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.GP.Methods.GPEDIT_COLUMNEVENT_GETID(GPEDIT_COLUMNEVENT);
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPEDIT_COLUMNEVENT_UPD = function (GPEDIT_COLUMNEVENT) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMNEVENT.FieldName, GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMNEVENT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMN.FieldName, GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddAutoInc(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_TABLE.FieldName, GPEDIT_COLUMNEVENT.IDGPEDIT_TABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.EVINSERT.FieldName, GPEDIT_COLUMNEVENT.EVINSERT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.EVDELETE.FieldName, GPEDIT_COLUMNEVENT.EVDELETE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.EVUPDATESET.FieldName, GPEDIT_COLUMNEVENT.EVUPDATESET, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.EVUPDATEWHERE.FieldName, GPEDIT_COLUMNEVENT.EVUPDATEWHERE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);


        /*   
        //********   GPEDIT_COLUMNEVENT_UPD   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPEDIT_COLUMNEVENT_UPD", @"GPEDIT_COLUMNEVENT_UPD description.");  
        UnSQL.SQL.Add(@"  UPDATE GPEDIT_COLUMNEVENT Set "); 
         UnSQL.SQL.Add(@"   IDGPEDIT_COLUMN=@[IDGPEDIT_COLUMN], "); 
         UnSQL.SQL.Add(@"   IDGPEDIT_TABLE=@[IDGPEDIT_TABLE], "); 
         UnSQL.SQL.Add(@"   EVINSERT=@[EVINSERT], "); 
         UnSQL.SQL.Add(@"   EVDELETE=@[EVDELETE], "); 
         UnSQL.SQL.Add(@"   EVUPDATESET=@[EVUPDATESET], "); 
         UnSQL.SQL.Add(@"   EVUPDATEWHERE=@[EVUPDATEWHERE] "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPEDIT_COLUMNEVENT=@[IDGPEDIT_COLUMNEVENT] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPEDIT_COLUMNEVENT_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.GP.Methods.GPEDIT_COLUMNEVENT_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.GP.Methods.GPEDIT_COLUMNEVENT_DELIDGPEDIT_COLUMNEVENT(/*String StrIDGPEDIT_COLUMNEVENT*/Param);
    }
    else {
        Res = Persistence.GP.Methods.GPEDIT_COLUMNEVENT_DELGPEDIT_COLUMNEVENT(Param/*GPEDIT_COLUMNEVENTList*/);
    }
    return (Res);
}

Persistence.GP.Methods.GPEDIT_COLUMNEVENT_DELIDGPEDIT_COLUMNEVENT = function (/*String StrIDGPEDIT_COLUMNEVENT*/IDGPEDIT_COLUMNEVENT) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDGPEDIT_COLUMNEVENT == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMNEVENT.FieldName, " = " + UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.NAME_TABLE + "." + UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMNEVENT.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDGPEDIT_COLUMNEVENT = " IN (" + StrIDGPEDIT_COLUMNEVENT + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMNEVENT.FieldName, StrIDGPEDIT_COLUMNEVENT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMNEVENT.FieldName, IDGPEDIT_COLUMNEVENT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   GPEDIT_COLUMNEVENT_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPEDIT_COLUMNEVENT_DEL", @"GPEDIT_COLUMNEVENT_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE GPEDIT_COLUMNEVENT "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPEDIT_COLUMNEVENT=@[IDGPEDIT_COLUMNEVENT]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPEDIT_COLUMNEVENT_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.GP.Methods.GPEDIT_COLUMNEVENT_DELGPEDIT_COLUMNEVENT = function (GPEDIT_COLUMNEVENT/*GPEDIT_COLUMNEVENTList*/) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        /*
        StrIDGPEDIT_COLUMNEVENT = "";
        StrIDOTRO = "";
        for (var i = 0; i < GPEDIT_COLUMNEVENTList.length; i++) {
        StrIDGPEDIT_COLUMNEVENT += GPEDIT_COLUMNEVENTList[i].IDGPEDIT_COLUMNEVENT + ",";
        StrIDOTRO += GPEDIT_COLUMNEVENTList[i].IDOTRO + ",";
        }
        StrIDGPEDIT_COLUMNEVENT = StrIDGPEDIT_COLUMNEVENT.substring(0, StrIDGPEDIT_COLUMNEVENT.length - 1);
        StrIDOTRO = StrIDOTRO.substring(0, StrIDOTRO.length - 1);
        
        
        if (StrIDGPEDIT_COLUMNEVENT == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMNEVENT.FieldName, " = " + UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.NAME_TABLE + "." + UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMNEVENT.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDGPEDIT_COLUMNEVENT = " IN (" + StrIDGPEDIT_COLUMNEVENT + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMNEVENT.FieldName, StrIDGPEDIT_COLUMNEVENT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/


        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMNEVENT.FieldName, GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMNEVENT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*    
        //********   GPEDIT_COLUMNEVENT_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"GPEDIT_COLUMNEVENT_DEL", @"GPEDIT_COLUMNEVENT_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE GPEDIT_COLUMNEVENT "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDGPEDIT_COLUMNEVENT=@[IDGPEDIT_COLUMNEVENT]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "GPEDIT_COLUMNEVENT_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

Persistence.GP.Methods.GPEDIT_COLUMNEVENT_GET_BY_GPEDIT_TABLE = function (GPEDIT_COLUMNEVENTList, IDGPEDIT_TABLE)
{
    var ResErr = new SysCfg.Error.Properties.TResErr();
    GPEDIT_COLUMNEVENTList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();

    Param.AddInt32(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_TABLE.FieldName, IDGPEDIT_TABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

    try
    {
        var DS_GPEDIT_COLUMNEVENT = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "GPEDIT_COLUMNEVENT_SELECTBYTABLE", Param.ToBytes());
        ResErr = DS_GPEDIT_COLUMNEVENT.ResErr;
        if (ResErr.NotError)
        {
            if (DS_GPEDIT_COLUMNEVENT.DataSet.RecordCount > 0)
            {
                DS_GPEDIT_COLUMNEVENT.DataSet.First();
                while (!(DS_GPEDIT_COLUMNEVENT.DataSet.Eof))
                {
                    var GPEDIT_COLUMNEVENT = new Persistence.GP.Properties.TGPEDIT_COLUMNEVENT();
                    GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMNEVENT = DS_GPEDIT_COLUMNEVENT.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMNEVENT.FieldName).asInt32();
                    GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMN = DS_GPEDIT_COLUMNEVENT.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMN.FieldName).asInt32();
                    GPEDIT_COLUMNEVENT.IDGPEDIT_TABLE = DS_GPEDIT_COLUMNEVENT.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_TABLE.FieldName).asInt32();
                    GPEDIT_COLUMNEVENT.EVINSERT = DS_GPEDIT_COLUMNEVENT.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.EVINSERT.FieldName).asString();
                    GPEDIT_COLUMNEVENT.EVDELETE = DS_GPEDIT_COLUMNEVENT.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.EVDELETE.FieldName).asString();
                    GPEDIT_COLUMNEVENT.EVUPDATESET = DS_GPEDIT_COLUMNEVENT.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.EVUPDATESET.FieldName).asString();
                    GPEDIT_COLUMNEVENT.EVUPDATEWHERE = DS_GPEDIT_COLUMNEVENT.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.EVUPDATEWHERE.FieldName).asString();

                    //Other
                    GPEDIT_COLUMNEVENTList.push(GPEDIT_COLUMNEVENT);
                    DS_GPEDIT_COLUMNEVENT.DataSet.Next();
                }
            }
            else
            {
                DS_GPEDIT_COLUMNEVENT.ResErr.NotError = false;
                DS_GPEDIT_COLUMNEVENT.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}

//**********************   METHODS   ********************************************************************************************
Persistence.GP.Methods.GPEDIT_COLUMNEVENT_ListSetID = function (GPEDIT_COLUMNEVENTList, IDGPEDIT_COLUMNEVENT) {
    for (i = 0; i < GPEDIT_COLUMNEVENTList.length; i++) {
        if (IDGPEDIT_COLUMNEVENT == GPEDIT_COLUMNEVENTList[i].IDGPEDIT_COLUMNEVENT)
            return (GPEDIT_COLUMNEVENTList[i]);
    }
    var UnGPEDIT_COLUMNEVENT = new Persistence.Properties.TGPEDIT_COLUMNEVENT;
    UnGPEDIT_COLUMNEVENT.IDGPEDIT_COLUMNEVENT = IDGPEDIT_COLUMNEVENT;
    return (UnGPEDIT_COLUMNEVENT);
}
Persistence.GP.Methods.GPEDIT_COLUMNEVENT_ListAdd = function (GPEDIT_COLUMNEVENTList, GPEDIT_COLUMNEVENT) {
    var i = Persistence.GP.Methods.GPEDIT_COLUMNEVENT_ListGetIndex(GPEDIT_COLUMNEVENTList, GPEDIT_COLUMNEVENT);
    if (i == -1) GPEDIT_COLUMNEVENTList.push(GPEDIT_COLUMNEVENT);
    else {
        GPEDIT_COLUMNEVENTList[i].IDGPEDIT_COLUMNEVENT = GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMNEVENT;
        GPEDIT_COLUMNEVENTList[i].IDGPEDIT_COLUMN = GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMN;
        GPEDIT_COLUMNEVENTList[i].IDGPEDIT_TABLE = GPEDIT_COLUMNEVENT.IDGPEDIT_TABLE;
        GPEDIT_COLUMNEVENTList[i].EVINSERT = GPEDIT_COLUMNEVENT.EVINSERT;
        GPEDIT_COLUMNEVENTList[i].EVDELETE = GPEDIT_COLUMNEVENT.EVDELETE;
        GPEDIT_COLUMNEVENTList[i].EVUPDATESET = GPEDIT_COLUMNEVENT.EVUPDATESET;
        GPEDIT_COLUMNEVENTList[i].EVUPDATEWHERE = GPEDIT_COLUMNEVENT.EVUPDATEWHERE;

    }
}
//CJRC_26072018
Persistence.GP.Methods.GPEDIT_COLUMNEVENT_ListGetIndex = function (GPEDIT_COLUMNEVENTList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.GP.Methods.GPEDIT_COLUMNEVENT_ListGetIndexIDGPEDIT_COLUMNEVENT(GPEDIT_COLUMNEVENTList, Param);

    }
    else {
        Res = Persistence.GP.Methods.GPEDIT_COLUMNEVENT_ListGetIndexGPEDIT_COLUMNEVENT(GPEDIT_COLUMNEVENTList, Param);
    }
    return (Res);
}

Persistence.GP.Methods.GPEDIT_COLUMNEVENT_ListGetIndexGPEDIT_COLUMNEVENT = function (GPEDIT_COLUMNEVENTList, GPEDIT_COLUMNEVENT) {
    for (i = 0; i < GPEDIT_COLUMNEVENTList.length; i++) {
        if (GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMNEVENT == GPEDIT_COLUMNEVENTList[i].IDGPEDIT_COLUMNEVENT)
            return (i);
    }
    return (-1);
}
Persistence.GP.Methods.GPEDIT_COLUMNEVENT_ListGetIndexIDGPEDIT_COLUMNEVENT = function (GPEDIT_COLUMNEVENTList, IDGPEDIT_COLUMNEVENT) {
    for (i = 0; i < GPEDIT_COLUMNEVENTList.length; i++) {
        if (IDGPEDIT_COLUMNEVENT == GPEDIT_COLUMNEVENTList[i].IDGPEDIT_COLUMNEVENT)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.GP.Methods.GPEDIT_COLUMNEVENTListtoStr = function (GPEDIT_COLUMNEVENTList) {
    var Res = Persistence.Properties._Version + "(1)0";
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(GPEDIT_COLUMNEVENTList.length, Longitud, Texto);
        for (Counter = 0; Counter < GPEDIT_COLUMNEVENTList.length; Counter++) {
            var UnGPEDIT_COLUMNEVENT = GPEDIT_COLUMNEVENTList[Counter];
            UnGPEDIT_COLUMNEVENT.ToStr(Longitud, Texto);
        }

        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
}
Persistence.GP.Methods.StrtoGPEDIT_COLUMNEVENT = function (ProtocoloStr, GPEDIT_COLUMNEVENTList) {

    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        GPEDIT_COLUMNEVENTList.length = 0;
        if (Persistence.GP.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")") - ProtocoloStr.indexOf("(") - 1);
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.Length - ProtocoloStr.indexOf(")") - 1);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnGPEDIT_COLUMNEVENT = new Persistence.Properties.TGPEDIT_COLUMNEVENT(); //Mode new row
                UnGPEDIT_COLUMNEVENT.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.GP.GPEDIT_COLUMNEVENT_ListAdd(GPEDIT_COLUMNEVENTList, UnGPEDIT_COLUMNEVENT);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}


//Socket Function 
Persistence.GP.Methods.GPEDIT_COLUMNEVENTListToByte = function (GPEDIT_COLUMNEVENTList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, GPEDIT_COLUMNEVENTList.Count - idx);
    for (i = idx; i < GPEDIT_COLUMNEVENTList.length; i++) {
        GPEDIT_COLUMNEVENTList[i].ToByte(MemStream);
    }
}
Persistence.GP.Methods.ByteToGPEDIT_COLUMNEVENTList = function (GPEDIT_COLUMNEVENTList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnGPEDIT_COLUMNEVENT = new Persistence.Properties.TGPEDIT_COLUMNEVENT();
        UnGPEDIT_COLUMNEVENT.ByteTo(MemStream);
        Methods.GPEDIT_COLUMNEVENT_ListAdd(GPEDIT_COLUMNEVENTList, UnGPEDIT_COLUMNEVENT);
    }
}
