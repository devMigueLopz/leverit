﻿//**********************   PROPERTIES   *****************************************************************************************
Persistence.GP.Properties.TCUSTOMFILTER = function ()
{
    this.COLUMNNAME = "";
    this.COLUMNTYPE = "";
    this.OPERATOR = "";
    this.VALUE = "";
    this.ANDOR = "";

    //Socket IO Properties
    this.ToByte = function (MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.COLUMNNAME);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.COLUMNTYPE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.OPERATOR);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.VALUE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.ANDOR);

    }
    this.ByteTo = function (MemStream)
    {
        this.COLUMNNAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.COLUMNTYPE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.OPERATOR = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.VALUE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.ANDOR = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);

    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto)
    {
        SysCfg.Str.Protocol.WriteStr(this.COLUMNNAME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.COLUMNTYPE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.OPERATOR, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.VALUE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.ANDOR, Longitud, Texto);

    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto)
    {
        this.COLUMNNAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.COLUMNTYPE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.OPERATOR = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.VALUE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.ANDOR = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);

    }
}

//**********************   METHODS   ********************************************************************************************
Persistence.GP.Methods.CUSTOMFILTER_ListSetID = function (CUSTOMFILTERList, IDCUSTOMFILTER)
{
    for (i = 0; i < CUSTOMFILTERList.length; i++)
    {
        if (IDCUSTOMFILTER == CUSTOMFILTERList[i].IDCUSTOMFILTER)
            return (CUSTOMFILTERList[i]);
    }
    var UnCUSTOMFILTER = new Persistence.Properties.TCUSTOMFILTER;
    UnCUSTOMFILTER.IDCUSTOMFILTER = IDCUSTOMFILTER;
    return (UnCUSTOMFILTER);
}
Persistence.GP.Methods.CUSTOMFILTER_ListAdd = function (CUSTOMFILTERList, CUSTOMFILTER)
{
    var i = Persistence.GP.Methods.CUSTOMFILTER_ListGetIndex(CUSTOMFILTERList, CUSTOMFILTER);
    if (i == -1) CUSTOMFILTERList.push(CUSTOMFILTER);
    else
    {
        CUSTOMFILTERList[i].COLUMNNAME = CUSTOMFILTER.COLUMNNAME;
        CUSTOMFILTERList[i].COLUMNTYPE = CUSTOMFILTER.COLUMNTYPE;
        CUSTOMFILTERList[i].OPERATOR = CUSTOMFILTER.OPERATOR;
        CUSTOMFILTERList[i].VALUE = CUSTOMFILTER.VALUE;
        CUSTOMFILTERList[i].ANDOR = CUSTOMFILTER.ANDOR;

    }
}
//CJRC_26072018
Persistence.GP.Methods.CUSTOMFILTER_ListGetIndex = function (CUSTOMFILTERList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.GP.Methods.CUSTOMFILTER_ListGetIndexIDCUSTOMFILTER(CUSTOMFILTERList, Param);

    }
    else {
        Res = Persistence.GP.Methods.CUSTOMFILTER_ListGetIndexCUSTOMFILTER(CUSTOMFILTERList, Param);
    }
    return (Res);
}

Persistence.GP.Methods.CUSTOMFILTER_ListGetIndexCUSTOMFILTER = function (CUSTOMFILTERList, CUSTOMFILTER)
{
    for (i = 0; i < CUSTOMFILTERList.length; i++)
    {
        if (CUSTOMFILTER.IDCUSTOMFILTER == CUSTOMFILTERList[i].IDCUSTOMFILTER)
            return (i);
    }
    return (-1);
}
Persistence.GP.Methods.CUSTOMFILTER_ListGetIndexIDCUSTOMFILTER = function (CUSTOMFILTERList, IDCUSTOMFILTER)
{
    for (i = 0; i < CUSTOMFILTERList.length; i++)
    {
        if (IDCUSTOMFILTER == CUSTOMFILTERList[i].IDCUSTOMFILTER)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.GP.Methods.CUSTOMFILTERListtoStr = function (CUSTOMFILTERList)
{
    var Res = Persistence.Properties._Version + "(1)0";
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try
    {
        SysCfg.Str.Protocol.WriteInt(CUSTOMFILTERList.length, Longitud, Texto);
        for (Counter = 0; Counter < CUSTOMFILTERList.length; Counter++)
        {
            var UnCUSTOMFILTER = CUSTOMFILTERList[Counter];
            UnCUSTOMFILTER.ToStr(Longitud, Texto);
        }

        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e)
    {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
}
Persistence.GP.Methods.StrtoCUSTOMFILTER = function (ProtocoloStr, CUSTOMFILTERList)
{

    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try
    {
        CUSTOMFILTERList.length = 0;
        if (Persistence.GP.Properties._Version == ProtocoloStr.substring(0, 3))
        {
            //Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")") - ProtocoloStr.indexOf("(") - 1);
            //Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.Length - ProtocoloStr.indexOf(")") - 1);
            //LongitudArray = Longitud.split(",");
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++)
            {
                UnCUSTOMFILTER = new Persistence.GP.Properties.TCUSTOMFILTER(); 
                UnCUSTOMFILTER.StrTo(Pos, Index, LongitudArray, Texto);
                CUSTOMFILTERList.push(UnCUSTOMFILTER);
            }
        }
    }
    catch (e)
    {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}


//Socket Function 
Persistence.GP.Methods.CUSTOMFILTERListToByte = function (CUSTOMFILTERList, MemStream, idx)
{
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, CUSTOMFILTERList.Count - idx);
    for (i = idx; i < CUSTOMFILTERList.Count; i++)
    {
        CUSTOMFILTERList[i].ToByte(MemStream);
    }
}
Persistence.GP.Methods.ByteToCUSTOMFILTERList = function (CUSTOMFILTERList, MemStream)
{
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++)
    {
        UnCUSTOMFILTER = new Persistence.Properties.TCUSTOMFILTER();
        UnCUSTOMFILTER.ByteTo(MemStream);
        Methods.CUSTOMFILTER_ListAdd(CUSTOMFILTERList, UnCUSTOMFILTER);
    }
}
