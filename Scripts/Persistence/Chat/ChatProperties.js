﻿//TreduceDB
//Persistence.Chat

Persistence.Chat._Version = "001";
Persistence.Chat.Properties.TSMCHATASSIGNEDTYPE =
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    ALL: { value: 0, name: "ALL" },
    REPORTE: { value: 1, name: "REPORTE" },
    SEGUIMIENTO: { value: 2, name: "SEGUIMIENTO" },
    INVITATION: { value: 3, name: "INVITATION" },
    CONTACT: { value: 4, name: "CONTACT" }
}

Persistence.Chat.Properties.TSMCHAT = function () {
    this.CHATMESSAGES = "";
    this.EVENTDATE = new Date(1970, 0, 1, 0, 0, 0);
    this.IDCMDBCI_SEND = 0;
    this.IDSMCHAT = 0;
    this.IDSMCHATASSIGNED = 0;
    this.SMCHATASSIGNEDMEMBER = new Persistence.Chat.Properties.TSMCHATASSIGNEDMEMBER(); //ChatRS
    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CHATMESSAGES);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.EVENTDATE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDCMDBCI_SEND);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSMCHAT);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSMCHATASSIGNED);
    }
    this.ByteTo = function (MemStream) {
        this.CHATMESSAGES = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.EVENTDATE = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.IDCMDBCI_SEND = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSMCHAT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSMCHATASSIGNED = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteStr(this.CHATMESSAGES, Longitud, Texto);
        SysCfg.Str.Protocol.WriteDateTime(this.EVENTDATE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDCMDBCI_SEND, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDSMCHAT, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDSMCHATASSIGNED, Longitud, Texto);
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.CHATMESSAGES = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.EVENTDATE = SysCfg.Str.Protocol.ReadDateTime(Pos, Index, LongitudArray, Texto);
        this.IDCMDBCI_SEND = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDSMCHAT = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDSMCHATASSIGNED = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
    }
}

Persistence.Chat.Properties.TSMCHATASSIGNED = function () {
    this.IDSMCHATASSIGNED = 0;
    this.ASSIGNED_ENABLE = false;
    this.ASSIGNED_TITLE = "";
    this.ASSIGNED_DATE = new Date(1970, 0, 1, 0, 0, 0);
    this.Waiting_Users = 0;
    this.IDSMCHATASSIGNEDTYPE = Persistence.Chat.Properties.TSMCHATASSIGNEDTYPE.GetEnum(undefined)
    this.SMCHATList = new Array();//Chat.Properties.TSMCHAT   //ChatRS         
    this.SMCHATASSIGNEDMEMBERList = new Array();//Chat.Properties.TSMCHATASSIGNEDMEMBER //ChatRS
    //Socket IO Properties
    this.ToByte = function (MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, IDSMCHATASSIGNED);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ASSIGNED_ENABLE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, ASSIGNED_TITLE);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, ASSIGNED_DATE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, Waiting_Users);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, IDSMCHATASSIGNEDTYPE.value);
    }
    this.ByteTo = function (MemStream)
    {
        IDSMCHATASSIGNED = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        ASSIGNED_ENABLE = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        ASSIGNED_TITLE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        ASSIGNED_DATE = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        Waiting_Users = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        IDSMCHATASSIGNEDTYPE = Persistence.Chat.Properties.TSMCHATASSIGNEDTYPE.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto)
    {
        SysCfg.Str.Protocol.WriteInt(IDSMCHATASSIGNED, Longitud, Texto);
        SysCfg.Str.Protocol.WriteBool(ASSIGNED_ENABLE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(ASSIGNED_TITLE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteDateTime(ASSIGNED_DATE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(Waiting_Users, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(IDSMCHATASSIGNEDTYPE.value, Longitud, Texto);
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto)
    {
        IDSMCHATASSIGNED = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        ASSIGNED_ENABLE = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);
        ASSIGNED_TITLE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        ASSIGNED_DATE = SysCfg.Str.Protocol.ReadDateTime(Pos, Index, LongitudArray, Texto);
        Waiting_Users = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        IDSMCHATASSIGNEDTYPE = Persistence.Chat.Properties.TSMCHATASSIGNEDTYPE.GetEnum(SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto));
    }
}

Persistence.Chat.Properties.TSMCHATASSIGNEDMEMBER = function () {
    this.DATETIME_MEMBER = new Date(1970, 0, 1, 0, 0, 0);
    this.ENABLE_MEMBER = false;
    this.CREATE_MEMBER = false;
    this.IDCMDBCI_MEMBER = 0;
    this.CI_GENERICNAME = "";
    this.IDSMCHATASSIGNED = 0;
    this.IDSMCHATASSIGNEDMEMBER = 0;
    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, DATETIME_MEMBER);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ENABLE_MEMBER);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, CREATE_MEMBER);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, IDCMDBCI_MEMBER);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, CI_GENERICNAME);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, IDSMCHATASSIGNED);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, IDSMCHATASSIGNEDMEMBER);
    }
    this.ByteTo = function (MemStream) {
        DATETIME_MEMBER = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        ENABLE_MEMBER = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        CREATE_MEMBER = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        IDCMDBCI_MEMBER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        CI_GENERICNAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        IDSMCHATASSIGNED = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        IDSMCHATASSIGNEDMEMBER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    }
    //Str IO Properties
    this.ToStr = function(Longitud, Texto)
    {
        SysCfg.Str.Protocol.WriteDateTime(DATETIME_MEMBER, Longitud, Texto);
        SysCfg.Str.Protocol.WriteBool(ENABLE_MEMBER, Longitud, Texto);
        SysCfg.Str.Protocol.WriteBool(CREATE_MEMBER, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(IDCMDBCI_MEMBER, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(CI_GENERICNAME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(IDSMCHATASSIGNED, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(IDSMCHATASSIGNEDMEMBER, Longitud, Texto);
    }
    this.StrTo = function(Pos, Index, LongitudArray, Texto)
    {
        DATETIME_MEMBER = SysCfg.Str.Protocol.ReadDateTime(Pos, Index, LongitudArray, Texto);
        ENABLE_MEMBER = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);
        CREATE_MEMBER = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);
        IDCMDBCI_MEMBER = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        CI_GENERICNAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        IDSMCHATASSIGNED = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        IDSMCHATASSIGNEDMEMBER = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
    }

}

/*
Persistence.Chat.Properties.TLoginServiceType =
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    UpdatePW: { value: 1, name: "UpdatePW" },
}
Persistence.Chat.Properties.TChatServiceType =
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    REPORTE: { value: 1, name: "REPORTE" },
}
Persistence.Chat.Properties.TNotifyServiceType =
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    SELECT_ALL: { value: 1, name: "SELECT_ALL" },
    AFTER_DATE: { value: 2, name: "AFTER_DATE" },
}
*/
