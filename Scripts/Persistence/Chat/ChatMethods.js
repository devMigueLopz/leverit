﻿//TreduceDB
//Persistence.Chat.Properties
//Persistence.Chat.Methods

// SMCHAT
//List Function 
Persistence.Chat.Methods.SMCHAT_ListSetID = function( SMCHATList, IDSMCHAT)
{
    for (var i = 0; i < SMCHATList.length; i++)
    {

        if (IDSMCHAT == SMCHATList[i].IDSMCHAT)
            return (SMCHATList[i]);

    }
    var TSMCHAT = new Persistence.Chat.Properties.TSMCHAT();
    TSMCHAT.IDSMCHAT = IDSMCHAT;
    return (TSMCHAT);
}
Persistence.Chat.Methods.SMCHAT_ListAdd = function( SMCHATList,  SMCHAT)
{
    var i = Persistence.Chat.Methods.SMCHAT_ListGetIndex(SMCHATList, SMCHAT);
    if (i == -1) SMCHATList.push(SMCHAT);
    else
    {
        SMCHATList[i].CHATMESSAGES = SMCHAT.CHATMESSAGES;
        SMCHATList[i].EVENTDATE = SMCHAT.EVENTDATE;
        SMCHATList[i].IDCMDBCI_SEND = SMCHAT.IDCMDBCI_SEND;
        SMCHATList[i].IDSMCHAT = SMCHAT.IDSMCHAT;
        SMCHATList[i].IDSMCHATASSIGNED = SMCHAT.IDSMCHATASSIGNED;
    }
}



Persistence.Chat.Methods.SMCHAT_ListGetIndex = function (SMCHATList, Param)
{
    var Res= -1
    if (typeof (Param) != 'number') {
        Res = Persistence.Chat.Methods.SMCHAT_ListGetIndexIDSMCHAT(SMCHATList, Param);       
    }
    else {
        Res = Persistence.Chat.Methods.SMCHAT_ListGetIndexSMCHAT(SMCHATList, Param);
    }
    return (Res);
}


Persistence.Chat.Methods.SMCHAT_ListGetIndexSMCHAT = function (SMCHATList, SMCHAT)
{
    for (var i = 0; i < SMCHATList.length; i++)
    {
        if (SMCHAT.IDSMCHAT == SMCHATList[i].IDSMCHAT)
            return (i);
    }
    return (-1);
}
Persistence.Chat.Methods.SMCHAT_ListGetIndexIDSMCHAT = function (SMCHATList, IDSMCHAT)
{
    for (var i = 0; i < SMCHATList.length; i++)
    {
        if (IDSMCHAT == SMCHATList[i].IDSMCHAT)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.Chat.Methods.SMCHATListtoStr = function( SMCHATList)
{
    var Res = Chat.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(SMCHATList.length, Longitud, Texto);
        for (Counter = 0; Counter < SMCHATList.length; Counter++) {
            var UnSMCHAT = SMCHATList[Counter];
            UnSMCHAT.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Catalog.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;

}
Persistence.Chat.Methods.StrtoSMCHAT = function(ProtocoloStr,  SMCHATList)
{
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        SMCHATList.length = 0;
        if (Persistence.Chat.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnSMCHAT = new Persistence.Demo.Properties.TSMCHAT(); //Mode new row
                UnSMCHAT.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Chat.Methods.SMCHAT_ListAdd(SMCHATList, UnSMCHAT);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}

//Socket Function 
Persistence.Chat.Methods.SMCHATListToByte = function( SMCHATList, MemStream, idx)
{
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, SMCHATList.length - idx);
    for (var i = idx; i < SMCHATList.length; i++)
    {
        SMCHATList[i].ToByte(MemStream);
    }
}

Persistence.Chat.Methods.ByteToSMCHATList = function( SMCHATList, MemStream)
{
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (var i = 0; i < Count; i++)
    {
        var UnSMCHAT = new Persistence.Chat.Properties.TSMCHAT();
        UnSMCHAT.ByteTo(MemStream);
        Persistence.Chat.Methods.SMCHAT_ListAdd(SMCHATList, UnSMCHAT);
    }
}
    
// SMCHATASSIGNED    
//List Function 
Persistence.Chat.Methods.SMCHATASSIGNED_ListSetID = function(SMCHATASSIGNEDList, IDSMCHATASSIGNED)
{
    for (var i = 0; i < SMCHATASSIGNEDList.length; i++)
    {

        if (IDSMCHATASSIGNED == SMCHATASSIGNEDList[i].IDSMCHATASSIGNED)
            return (SMCHATASSIGNEDList[i]);

    }
    var SMCHATASSIGNED = new Persistence.Chat.Properties.TSMCHATASSIGNED();
    SMCHATASSIGNED.IDSMCHATASSIGNED = IDSMCHATASSIGNED;
    return (SMCHATASSIGNED);
}

Persistence.Chat.Methods.SMCHATASSIGNED_ListAdd = function( SMCHATASSIGNEDList, SMCHATASSIGNED)
{
    var i = Persistence.Chat.Methods.SMCHATASSIGNED_ListGetIndex(SMCHATASSIGNEDList, SMCHATASSIGNED);
    if (i == -1) SMCHATASSIGNEDList.push(SMCHATASSIGNED);
    else
    {
        SMCHATASSIGNEDList[i].IDSMCHATASSIGNED = SMCHATASSIGNED.IDSMCHATASSIGNED;
        SMCHATASSIGNEDList[i].ASSIGNED_ENABLE = SMCHATASSIGNED.ASSIGNED_ENABLE;
        SMCHATASSIGNEDList[i].ASSIGNED_TITLE = SMCHATASSIGNED.ASSIGNED_TITLE;
        SMCHATASSIGNEDList[i].ASSIGNED_DATE = SMCHATASSIGNED.ASSIGNED_DATE;
        SMCHATASSIGNEDList[i].Waiting_Users = SMCHATASSIGNED.Waiting_Users;
        SMCHATASSIGNEDList[i].IDSMCHATASSIGNEDTYPE = SMCHATASSIGNED.IDSMCHATASSIGNEDTYPE;
    }
}
//CJRC_26072018
Persistence.Chat.Methods.SMCHATASSIGNED_ListGetIndex = function( SMCHATASSIGNEDList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.Chat.Methods.SMCHATASSIGNED_ListGetIndexIDSMCHATASSIGNED(SMCHATASSIGNEDList, Param);

    }
    else {
        Res = Persistence.Chat.Methods.SMCHATASSIGNED_ListGetIndexSMCHATASSIGNED(SMCHATASSIGNEDList, Param);
    }
    return (Res);
}

Persistence.Chat.Methods.SMCHATASSIGNED_ListGetIndexSMCHATASSIGNED = function (SMCHATASSIGNEDList, SMCHATASSIGNED)
{
    for (var i = 0; i < SMCHATASSIGNEDList.length; i++)
    {
        if (SMCHATASSIGNED.IDSMCHATASSIGNED == SMCHATASSIGNEDList[i].IDSMCHATASSIGNED)
            return (i);
    }
    return (-1);
}
Persistence.Chat.Methods.SMCHATASSIGNED_ListGetIndexIDSMCHATASSIGNED = function (SMCHATASSIGNEDList, IDSMCHATASSIGNED)
{
    for (var i = 0; i < SMCHATASSIGNEDList.length; i++)
    {
        if (IDSMCHATASSIGNED == SMCHATASSIGNEDList[i].IDSMCHATASSIGNED)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.Chat.Methods.SMCHATASSIGNEDListtoStr = function( SMCHATASSIGNEDList)
{

    var Res = Persistence.Chat.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(SMCHATASSIGNEDList.length, Longitud, Texto);
        for (Counter = 0; Counter < SMCHATASSIGNEDList.length; Counter++) {
            var UnSMCHATASSIGNED = SMCHATASSIGNEDList[Counter];
            UnSMCHATASSIGNED.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Catalog.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;




}
Persistence.Chat.Methods.StrtoSMCHATASSIGNED = function(ProtocoloStr,  SMCHATASSIGNEDList)
{
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        SMCHATASSIGNEDList.length = 0;
        if (Persistence.Chat.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnSMCHATASSIGNED = new Persistence.Demo.Properties.TSMCHATASSIGNED(); //Mode new row
                UnSMCHATASSIGNED.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Chat.Methods.SMCHATASSIGNED_ListAdd(SMCHATASSIGNEDList, UnSMCHATASSIGNED);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}

//Socket Function 
Persistence.Chat.Methods.SMCHATASSIGNEDListToByte = function( SMCHATASSIGNEDList, MemStream, idx)
{
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, SMCHATASSIGNEDList.length-idx);
    for (var i = idx; i < SMCHATASSIGNEDList.length; i++)
    {
        SMCHATASSIGNEDList[i].ToByte(MemStream);
    }
}
Persistence.Chat.Methods.ByteToSMCHATASSIGNEDList = function( SMCHATASSIGNEDList, MemStream)
{
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (var i = 0; i < Count; i++)
    {
        var UnSMCHATASSIGNED = new Persistence.Chat.Properties.TSMCHATASSIGNED();
        UnSMCHATASSIGNED.ByteTo(MemStream);
        Persistence.Chat.Methods.SMCHATASSIGNED_ListAdd(SMCHATASSIGNEDList, UnSMCHATASSIGNED);
    }
}
    
//SMCHATASSIGNEDMEMBER
//List Function 
Persistence.Chat.Methods.SMCHATASSIGNEDMEMBER_ListSetID = function(  SMCHATASSIGNEDMEMBERList, IDSMCHATASSIGNEDMEMBER)
{
    for (var i = 0; i < SMCHATASSIGNEDMEMBERList.length; i++)
    {

        if (IDSMCHATASSIGNEDMEMBER == SMCHATASSIGNEDMEMBERList[i].IDSMCHATASSIGNEDMEMBER)
            return (SMCHATASSIGNEDMEMBERList[i]);

    }
    var SMCHATASSIGNEDMEMBER = new Persistence.Chat.Properties.TSMCHATASSIGNEDMEMBER();
    SMCHATASSIGNEDMEMBER.IDSMCHATASSIGNEDMEMBER = IDSMCHATASSIGNEDMEMBER;
    return (SMCHATASSIGNEDMEMBER);
}
Persistence.Chat.Methods.SMCHATASSIGNEDMEMBER_ListAdd = function(  SMCHATASSIGNEDMEMBERList, SMCHATASSIGNEDMEMBER)
{
    var i = Persistence.Chat.Methods.SMCHATASSIGNEDMEMBER_ListGetIndex(SMCHATASSIGNEDMEMBERList, SMCHATASSIGNEDMEMBER);
    if (i == -1) SMCHATASSIGNEDMEMBERList.push(SMCHATASSIGNEDMEMBER);
    else
    {
        SMCHATASSIGNEDMEMBERList[i].DATETIME_MEMBER = SMCHATASSIGNEDMEMBER.DATETIME_MEMBER;
        SMCHATASSIGNEDMEMBERList[i].ENABLE_MEMBER = SMCHATASSIGNEDMEMBER.ENABLE_MEMBER;
        SMCHATASSIGNEDMEMBERList[i].CREATE_MEMBER = SMCHATASSIGNEDMEMBER.CREATE_MEMBER;
        SMCHATASSIGNEDMEMBERList[i].CI_GENERICNAME = SMCHATASSIGNEDMEMBER.CI_GENERICNAME;
        SMCHATASSIGNEDMEMBERList[i].IDCMDBCI_MEMBER = SMCHATASSIGNEDMEMBER.IDCMDBCI_MEMBER;
        SMCHATASSIGNEDMEMBERList[i].IDSMCHATASSIGNED = SMCHATASSIGNEDMEMBER.IDSMCHATASSIGNED;
        SMCHATASSIGNEDMEMBERList[i].IDSMCHATASSIGNEDMEMBER = SMCHATASSIGNEDMEMBER.IDSMCHATASSIGNEDMEMBER;
                
                
    }
}
//CJRC_26072018
Persistence.Chat.Methods.SMCHATASSIGNEDMEMBER_ListGetIndex = function (SMCHATASSIGNEDMEMBERList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.Chat.Methods.SMCHATASSIGNEDMEMBER_ListGetIndexIDSMCHATASSIGNEDMEMBER(SMCHATASSIGNEDMEMBERList, Param);

    }
    else {
        Res = Persistence.Chat.Methods.SMCHATASSIGNEDMEMBER_ListGetIndexSMCHATASSIGNEDMEMBER(SMCHATASSIGNEDMEMBERList, Param);
    }
    return (Res);
}

Persistence.Chat.Methods.SMCHATASSIGNEDMEMBER_ListGetIndexSMCHATASSIGNEDMEMBER = function (SMCHATASSIGNEDMEMBERList, SMCHATASSIGNEDMEMBER)
{
    for (var i = 0; i < SMCHATASSIGNEDMEMBERList.length; i++)
    {
        if (SMCHATASSIGNEDMEMBER.IDSMCHATASSIGNEDMEMBER == SMCHATASSIGNEDMEMBERList[i].IDSMCHATASSIGNEDMEMBER)
            return (i);
    }
    return (-1);
}
Persistence.Chat.Methods.SMCHATASSIGNEDMEMBER_ListGetIndexIDSMCHATASSIGNEDMEMBER = function (SMCHATASSIGNEDMEMBERList, IDSMCHATASSIGNEDMEMBER)
{
    for (var i = 0; i < SMCHATASSIGNEDMEMBERList.length; i++)
    {
        if (IDSMCHATASSIGNEDMEMBER == SMCHATASSIGNEDMEMBERList[i].IDSMCHATASSIGNEDMEMBER)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.Chat.Methods.SMCHATASSIGNEDMEMBERListtoStr = function(  SMCHATASSIGNEDMEMBERList)
{
    var Res = Persistence.Chat.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(SMCHATASSIGNEDMEMBERList.length, Longitud, Texto);
        for (Counter = 0; Counter < SMCHATASSIGNEDMEMBERList.length; Counter++) {
            var UnSMCHATASSIGNEDMEMBER = SMCHATASSIGNEDMEMBERList[Counter];
            UnSMCHATASSIGNEDMEMBER.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Catalog.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;

}
Persistence.Chat.Methods.StrtoSMCHATASSIGNEDMEMBER = function(ProtocoloStr,   SMCHATASSIGNEDMEMBERList)
{
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        SMCHATASSIGNEDMEMBERList.length = 0;
        if (Persistence.Chat.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnSMCHATASSIGNEDMEMBER = new Persistence.Demo.Properties.TSMCHATASSIGNEDMEMBER(); //Mode new row
                UnSMCHATASSIGNEDMEMBER.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Chat.Methods.SMCHATASSIGNEDMEMBER_ListAdd(SMCHATASSIGNEDMEMBERList, UnSMCHATASSIGNEDMEMBER);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);


}

//Socket Function 
Persistence.Chat.Methods.SMCHATASSIGNEDMEMBERListToByte = function(  SMCHATASSIGNEDMEMBERList, MemStream, idx)
{
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, SMCHATASSIGNEDMEMBERList.length - idx);
    for (var i = idx; i < SMCHATASSIGNEDMEMBERList.length; i++)
    {
        SMCHATASSIGNEDMEMBERList[i].ToByte(MemStream);
    }
}
Persistence.Chat.Methods.ByteToSMCHATASSIGNEDMEMBERList = function(  SMCHATASSIGNEDMEMBERList, MemStream)
{
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (var i = 0; i < Count; i++)
    {
        var UnSMCHATASSIGNEDMEMBER = new Persistence.Chat.Properties.TSMCHATASSIGNEDMEMBER();

        UnSMCHATASSIGNEDMEMBER.ByteTo(MemStream);
        Persistence.Chat.Methods.SMCHATASSIGNEDMEMBER_ListAdd(SMCHATASSIGNEDMEMBERList, UnSMCHATASSIGNEDMEMBER);
                
    }
}





