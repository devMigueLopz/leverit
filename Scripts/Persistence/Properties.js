﻿
Persistence.TUserFunctions = function(inIDCMDBCI)
{
    this.TimeCounter = 0;
    this.IDCMDBCI = inIDCMDBCI;
    this.CaseCounter = 0;
    this.isAtention = false;
    this.User = new SysCfg.App.TUser();
    //****************** Begin SMCIProfiler ***************************
    //this.SMCIProfiler = new SMCI.TSMCIProfiler();
    this.CMDBUSERCONTACTTYPEList = new Array();//Persistence.SMCI.Properties.TCMDBUSERCONTACTTYPE
    this.SDGROUPSERVICEUSERCIList = new Array();//Persistence.SMCI.TSDGROUPSERVICEUSERCI;
    this.CMDBUSER = new Persistence.SMCI.Properties.TCMDBUSER();
    this.CMDBUSERSETTINGS = new  Persistence.SMCI.Properties.TCMDBUSERSETTINGS();
    this.CMDBUSERADDRESS = new  Persistence.SMCI.Properties.TCMDBUSERADDRESS();
    //****************** End SMCIProfiler ***************************

    this.ChatProfiler = new  Persistence.Chat.TChatProfiler();
    this.NotifyProfiler = new  Persistence.Notify.TNotifyProfiler();
    this.SMCHATASSIGNEDList = new Array();//Persistence.Chat.Properties.TSMCHATASSIGNED //ChatRS   //repositorio para exportar a Chatprofiler        
    this.RHREQUESTList = new Array();//Persistence.RemoteHelp.Properties.TRHREQUEST
    this.RHMODULE_ACTIVEList = new Array();//Persistence.RemoteHelp.Properties.TRHMODULE_ACTIVE
}

Persistence.TPERSISTENCE_MODE = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _Local: { value: 0, name: "Local" },
    _Server_DataBase: { value: 1, name: "Server_DataBase" },
    _Server_Memory: { value: 2, name: "Server_Memory" }
}


