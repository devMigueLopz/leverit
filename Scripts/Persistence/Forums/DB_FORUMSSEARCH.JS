//**********************   PROPERTIES   *****************************************************************************************
Persistence.Forums.Properties.TFORUMSSEARCH = function () {
    this.IDFORUMSSEARCH = 0;
    this.IDFORUMS = 0;
    this.NAMEFORUMSSEARCH = "";
    this.IMAGEFORUMSSEARCH = "";
    this.QUERYFORUMSSEARCH = "";
    //********************** VIRTUALES **********************
    this.FORUMS = new Object();
    //********************** END VIRTUALES **********************
    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDFORUMSSEARCH);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDFORUMS);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.NAMEFORUMSSEARCH);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.IMAGEFORUMSSEARCH);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.QUERYFORUMSSEARCH);

    }
    this.ByteTo = function (MemStream) {
        this.IDFORUMSSEARCH = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDFORUMS = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.NAMEFORUMSSEARCH = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IMAGEFORUMSSEARCH = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.QUERYFORUMSSEARCH = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);

    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDFORUMSSEARCH, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDFORUMS, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.NAMEFORUMSSEARCH, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.IMAGEFORUMSSEARCH, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.QUERYFORUMSSEARCH, Longitud, Texto);

    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDFORUMSSEARCH = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDFORUMS = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.NAMEFORUMSSEARCH = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.IMAGEFORUMSSEARCH = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.QUERYFORUMSSEARCH = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);

    }
}

//**********************   METHODS server  ********************************************************************************************
Persistence.Forums.Methods.FORUMSSEARCH_Fill = function (FORUMSSEARCHList, StrIDFORUMSSEARCH/*noin IDFORUMSSEARCH*/) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    FORUMSSEARCHList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDFORUMSSEARCH == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.FORUMSSEARCH.IDFORUMSSEARCH.FieldName, " = " + UsrCfg.InternoAtisNames.FORUMSSEARCH.NAME_TABLE + "." + UsrCfg.InternoAtisNames.FORUMSSEARCH.IDFORUMSSEARCH.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDFORUMSSEARCH = " IN (" + StrIDFORUMSSEARCH + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.FORUMSSEARCH.IDFORUMSSEARCH.FieldName, StrIDFORUMSSEARCH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //noin Param.AddInt32(UsrCfg.InternoDemoNames.FORUMSSEARCH.IDFORUMSSEARCH.FieldName, IDFORUMSSEARCH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {
        /*
        //********   FORUMSSEARCH_GET   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"FORUMSSEARCH_GET", @"FORUMSSEARCH_GET description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   IDFORUMSSEARCH, "); 
         UnSQL.SQL.Add(@"   IDFORUMS, "); 
         UnSQL.SQL.Add(@"   NAMEFORUMSSEARCH, "); 
         UnSQL.SQL.Add(@"   IMAGEFORUMSSEARCH, "); 
         UnSQL.SQL.Add(@"   QUERYFORUMSSEARCH "); 
         UnSQL.SQL.Add(@"   FROM FORUMSSEARCH "); 
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_FORUMSSEARCH = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "FORUMSSEARCH_GET", Param.ToBytes());
        ResErr = DS_FORUMSSEARCH.ResErr;
        if (ResErr.NotError) {
            if (DS_FORUMSSEARCH.DataSet.RecordCount > 0) {
                DS_FORUMSSEARCH.DataSet.First();
                while (!(DS_FORUMSSEARCH.DataSet.Eof)) {
                    var FORUMSSEARCH = new Persistence.Forums.Properties.TFORUMSSEARCH();
                    FORUMSSEARCH.IDFORUMSSEARCH = DS_FORUMSSEARCH.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.FORUMSSEARCH.IDFORUMSSEARCH.FieldName).asInt32();
                    FORUMSSEARCH.IDFORUMS = DS_FORUMSSEARCH.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.FORUMSSEARCH.IDFORUMS.FieldName).asInt32();
                    FORUMSSEARCH.NAMEFORUMSSEARCH = DS_FORUMSSEARCH.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.FORUMSSEARCH.NAMEFORUMSSEARCH.FieldName).asString();
                    FORUMSSEARCH.IMAGEFORUMSSEARCH = DS_FORUMSSEARCH.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.FORUMSSEARCH.IMAGEFORUMSSEARCH.FieldName).asString();
                    FORUMSSEARCH.QUERYFORUMSSEARCH = DS_FORUMSSEARCH.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.FORUMSSEARCH.QUERYFORUMSSEARCH.FieldName).asText();

                    //Other
                    FORUMSSEARCHList.push(FORUMSSEARCH);
                    DS_FORUMSSEARCH.DataSet.Next();
                }
            }
            else {
                DS_FORUMSSEARCH.ResErr.NotError = false;
                DS_FORUMSSEARCH.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Forums.Methods.FORUMSSEARCH_GETID = function (FORUMSSEARCH) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.FORUMSSEARCH.IDFORUMSSEARCH.FieldName, FORUMSSEARCH.IDFORUMSSEARCH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.FORUMSSEARCH.IDFORUMS.FieldName, FORUMSSEARCH.IDFORUMS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.FORUMSSEARCH.NAMEFORUMSSEARCH.FieldName, FORUMSSEARCH.NAMEFORUMSSEARCH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.FORUMSSEARCH.IMAGEFORUMSSEARCH.FieldName, FORUMSSEARCH.IMAGEFORUMSSEARCH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddText(UsrCfg.InternoAtisNames.FORUMSSEARCH.QUERYFORUMSSEARCH.FieldName, FORUMSSEARCH.QUERYFORUMSSEARCH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

        /*
        //********   FORUMSSEARCH_GETID   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"FORUMSSEARCH_GETID", @"FORUMSSEARCH_GETID description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   IDFORUMSSEARCH "); 
         UnSQL.SQL.Add(@"   FROM FORUMSSEARCH "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDFORUMS=@[IDFORUMS] And "); 
         UnSQL.SQL.Add(@"   NAMEFORUMSSEARCH=@[NAMEFORUMSSEARCH] And "); 
         UnSQL.SQL.Add(@"   IMAGEFORUMSSEARCH=@[IMAGEFORUMSSEARCH] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_FORUMSSEARCH = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "FORUMSSEARCH_GETID", Param.ToBytes());
        ResErr = DS_FORUMSSEARCH.ResErr;
        if (ResErr.NotError) {
            if (DS_FORUMSSEARCH.DataSet.RecordCount > 0) {
                DS_FORUMSSEARCH.DataSet.First();
                FORUMSSEARCH.IDFORUMSSEARCH = DS_FORUMSSEARCH.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.FORUMSSEARCH.IDFORUMSSEARCH.FieldName).asInt32();
            }
            else {
                DS_FORUMSSEARCH.ResErr.NotError = false;
                DS_FORUMSSEARCH.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Forums.Methods.FORUMSSEARCH_ADD = function (FORUMSSEARCH) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.FORUMSSEARCH.IDFORUMSSEARCH.FieldName, FORUMSSEARCH.IDFORUMSSEARCH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.FORUMSSEARCH.IDFORUMS.FieldName, FORUMSSEARCH.IDFORUMS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.FORUMSSEARCH.NAMEFORUMSSEARCH.FieldName, FORUMSSEARCH.NAMEFORUMSSEARCH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.FORUMSSEARCH.IMAGEFORUMSSEARCH.FieldName, FORUMSSEARCH.IMAGEFORUMSSEARCH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.FORUMSSEARCH.QUERYFORUMSSEARCH.FieldName, FORUMSSEARCH.QUERYFORUMSSEARCH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        /* 
        //********   FORUMSSEARCH_ADD   ************************* 
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"FORUMSSEARCH_ADD", @"FORUMSSEARCH_ADD description."); 
        UnSQL.SQL.Add(@"   INSERT INTO FORUMSSEARCH( "); 
         UnSQL.SQL.Add(@"   IDFORUMS, "); 
         UnSQL.SQL.Add(@"   NAMEFORUMSSEARCH, "); 
         UnSQL.SQL.Add(@"   IMAGEFORUMSSEARCH, "); 
         UnSQL.SQL.Add(@"   QUERYFORUMSSEARCH "); 
         UnSQL.SQL.Add(@"   ) Values ( ");  
        UnSQL.SQL.Add(@"   @[IDFORUMS], "); 
         UnSQL.SQL.Add(@"   @[NAMEFORUMSSEARCH], "); 
         UnSQL.SQL.Add(@"   @[IMAGEFORUMSSEARCH], "); 
         UnSQL.SQL.Add(@"   @[QUERYFORUMSSEARCH] "); 
         UnSQL.SQL.Add(@"   )  "); 
        UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "FORUMSSEARCH_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.Forums.Methods.FORUMSSEARCH_GETID(FORUMSSEARCH);
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Forums.Methods.FORUMSSEARCH_UPD = function (FORUMSSEARCH) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.FORUMSSEARCH.IDFORUMSSEARCH.FieldName, FORUMSSEARCH.IDFORUMSSEARCH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.FORUMSSEARCH.IDFORUMS.FieldName, FORUMSSEARCH.IDFORUMS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.FORUMSSEARCH.NAMEFORUMSSEARCH.FieldName, FORUMSSEARCH.NAMEFORUMSSEARCH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.FORUMSSEARCH.IMAGEFORUMSSEARCH.FieldName, FORUMSSEARCH.IMAGEFORUMSSEARCH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.FORUMSSEARCH.QUERYFORUMSSEARCH.FieldName, FORUMSSEARCH.QUERYFORUMSSEARCH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);


        /*   
        //********   FORUMSSEARCH_UPD   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"FORUMSSEARCH_UPD", @"FORUMSSEARCH_UPD description.");  
        UnSQL.SQL.Add(@"  UPDATE FORUMSSEARCH Set "); 
         UnSQL.SQL.Add(@"   IDFORUMSSEARCH=@[IDFORUMSSEARCH], "); 
         UnSQL.SQL.Add(@"   IDFORUMS=@[IDFORUMS], "); 
         UnSQL.SQL.Add(@"   NAMEFORUMSSEARCH=@[NAMEFORUMSSEARCH], "); 
         UnSQL.SQL.Add(@"   IMAGEFORUMSSEARCH=@[IMAGEFORUMSSEARCH], "); 
         UnSQL.SQL.Add(@"   QUERYFORUMSSEARCH=@[QUERYFORUMSSEARCH] "); 
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "FORUMSSEARCH_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Forums.Methods.FORUMSSEARCH_DEL = function (/*String StrIDFORUMSSEARCH*/IDFORUMSSEARCH) {
var Res = -1;
if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
    Res = Persistence.Forums.Methods.FORUMSSEARCH_DELIDFORUMSSEARCH(/*String StrIDFORUMSSEARCH*/Param);
}
else {
    Res = Persistence.Forums.Methods.FORUMSSEARCH_DELFORUMSSEARCH(Param);
}
return (Res);
}
Persistence.Forums.Methods.FORUMSSEARCH_DELIDFORUMSSEARCH = function (/*String StrIDFORUMSSEARCH*/IDFORUMSSEARCH) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDFORUMSSEARCH == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.FORUMSSEARCH.IDFORUMSSEARCH.FieldName, " = " + UsrCfg.InternoAtisNames.FORUMSSEARCH.NAME_TABLE + "." + UsrCfg.InternoAtisNames.FORUMSSEARCH.IDFORUMSSEARCH.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDFORUMSSEARCH = " IN (" + StrIDFORUMSSEARCH + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.FORUMSSEARCH.IDFORUMSSEARCH.FieldName, StrIDFORUMSSEARCH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.FORUMSSEARCH.IDFORUMSSEARCH.FieldName, IDFORUMSSEARCH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   FORUMSSEARCH_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"FORUMSSEARCH_DEL", @"FORUMSSEARCH_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE FORUMSSEARCH "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDFORUMSSEARCH=@[IDFORUMSSEARCH]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "FORUMSSEARCH_DEL", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Forums.Methods.FORUMSSEARCH_DELFORUMSSEARCH = function (FORUMSSEARCH/*FORUMSSEARCHList*/) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        /*
        StrIDFORUMSSEARCH = "";
        StrIDOTRO = "";
        for (var i = 0; i < FORUMSSEARCHList.length; i++) {
        StrIDFORUMSSEARCH += FORUMSSEARCHList[i].IDFORUMSSEARCH + ",";
        StrIDOTRO += FORUMSSEARCHList[i].IDOTRO + ",";
        }
        StrIDFORUMSSEARCH = StrIDFORUMSSEARCH.substring(0, StrIDFORUMSSEARCH.length - 1);
        StrIDOTRO = StrIDOTRO.substring(0, StrIDOTRO.length - 1);
        
        
        if (StrIDFORUMSSEARCH == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.FORUMSSEARCH.IDFORUMSSEARCH.FieldName, " = " + UsrCfg.InternoAtisNames.FORUMSSEARCH.NAME_TABLE + "." + UsrCfg.InternoAtisNames.FORUMSSEARCH.IDFORUMSSEARCH.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDFORUMSSEARCH = " IN (" + StrIDFORUMSSEARCH + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.FORUMSSEARCH.IDFORUMSSEARCH.FieldName, StrIDFORUMSSEARCH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/


        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.FORUMSSEARCH.IDFORUMSSEARCH.FieldName, FORUMSSEARCH.IDFORUMSSEARCH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*    
        //********   FORUMSSEARCH_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"FORUMSSEARCH_DEL", @"FORUMSSEARCH_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE FORUMSSEARCH "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDFORUMSSEARCH=@[IDFORUMSSEARCH]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "FORUMSSEARCH_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//**********************   METHODS   ********************************************************************************************
Persistence.Forums.Methods.FORUMSSEARCH_ListSetID = function (FORUMSSEARCHList, IDFORUMSSEARCH) {
    for (i = 0; i < FORUMSSEARCHList.length; i++) {
        if (IDFORUMSSEARCH == FORUMSSEARCHList[i].IDFORUMSSEARCH)
            return (FORUMSSEARCHList[i]);
    }
    var UnFORUMSSEARCH = new Persistence.Properties.TFORUMSSEARCH;
    UnFORUMSSEARCH.IDFORUMSSEARCH = IDFORUMSSEARCH;
    return (UnFORUMSSEARCH);
}
Persistence.Forums.Methods.FORUMSSEARCH_ListAdd = function (FORUMSSEARCHList, FORUMSSEARCH) {
    var i = Persistence.Forums.Methods.FORUMSSEARCH_ListGetIndex(FORUMSSEARCHList, FORUMSSEARCH);
    if (i == -1) FORUMSSEARCHList.push(FORUMSSEARCH);
    else {
        FORUMSSEARCHList[i].IDFORUMSSEARCH = FORUMSSEARCH.IDFORUMSSEARCH;
        FORUMSSEARCHList[i].IDFORUMS = FORUMSSEARCH.IDFORUMS;
        FORUMSSEARCHList[i].NAMEFORUMSSEARCH = FORUMSSEARCH.NAMEFORUMSSEARCH;
        FORUMSSEARCHList[i].IMAGEFORUMSSEARCH = FORUMSSEARCH.IMAGEFORUMSSEARCH;
        FORUMSSEARCHList[i].QUERYFORUMSSEARCH = FORUMSSEARCH.QUERYFORUMSSEARCH;

    }
}
Persistence.Forums.Methods.FORUMSSEARCH_ListGetIndex = function (FORUMSSEARCHList, Param) {
    var Res = -1;
    if (typeof (Param) == 'number') {
        Res = Persistence.Forums.Methods.FORUMSSEARCH_ListGetIndexIDFORUMSSEARCH(FORUMSSEARCHList, Param);
    }
    else {
        Res = Persistence.Forums.Methods.FORUMSSEARCH_ListGetIndexFORUMSSEARCH(FORUMSSEARCHList, Param);
    }
    return (Res);
}
Persistence.Forums.Methods.FORUMSSEARCH_ListGetIndexFORUMSSEARCH = function (FORUMSSEARCHList, FORUMSSEARCH) {
    for (i = 0; i < FORUMSSEARCHList.length; i++) {
        if (FORUMSSEARCH.IDFORUMSSEARCH == FORUMSSEARCHList[i].IDFORUMSSEARCH)
            return (i);
    }
    return (-1);
}
Persistence.Forums.Methods.FORUMSSEARCH_ListGetIndexIDFORUMSSEARCH = function (FORUMSSEARCHList, IDFORUMSSEARCH) {
    for (i = 0; i < FORUMSSEARCHList.length; i++) {
        if (IDFORUMSSEARCH == FORUMSSEARCHList[i].IDFORUMSSEARCH)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.Forums.Methods.FORUMSSEARCHListtoStr = function (FORUMSSEARCHList) {
    var Res = Persistence.Properties._Version + "(1)0";
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(FORUMSSEARCHList.length, Longitud, Texto);
        for (Counter = 0; Counter < FORUMSSEARCHList.length; Counter++) {
            var UnFORUMSSEARCH = FORUMSSEARCHList[Counter];
            UnFORUMSSEARCH.ToStr(Longitud, Texto);
        }

        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
}
Persistence.Forums.Methods.StrtoFORUMSSEARCH = function (ProtocoloStr, FORUMSSEARCHList) {

    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        FORUMSSEARCHList.length = 0;
        if (Persistence.Forums.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.Length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnFORUMSSEARCH = new Persistence.Properties.TFORUMSSEARCH(); //Mode new row
                UnFORUMSSEARCH.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Forums.FORUMSSEARCH_ListAdd(FORUMSSEARCHList, UnFORUMSSEARCH);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}


//Socket Function 
Persistence.Forums.Methods.FORUMSSEARCHListToByte = function (FORUMSSEARCHList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, FORUMSSEARCHList.Count - idx);
    for (i = idx; i < FORUMSSEARCHList.Count; i++) {
        FORUMSSEARCHList[i].ToByte(MemStream);
    }
}
Persistence.Forums.Methods.ByteToFORUMSSEARCHList = function (FORUMSSEARCHList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnFORUMSSEARCH = new Persistence.Properties.TFORUMSSEARCH();
        UnFORUMSSEARCH.ByteTo(MemStream);
        Methods.FORUMSSEARCH_ListAdd(FORUMSSEARCHList, UnFORUMSSEARCH);
    }
}
