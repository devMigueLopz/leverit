Persistence.Forums.Properties._Version = '001';
Persistence.Forums.TForumsProfiler = function () {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.ResErr = new SysCfg.Error.Properties.TResErr();
    this.ForumsList = new Array();
    this.ForumsSearchList = new Array();
    this.ForumsCategoryList = new Array();
    this.TopicsList = new Array();
    this.TopicsResponseList = new Array();
    this.TopicsResponseVotesList = new Array();
    this.TopicsResponseAbuseList = new Array();
    this.CmdbCiList = new Array();
    this.MdCategoryDetailList = new Array();
    this.Fill = function () {
        try {
            Persistence.Forums.Methods.FORUMS_Fill(this.ForumsList, "");
            Persistence.Forums.Methods.FORUMSSEARCH_Fill(this.ForumsSearchList, "");
            Persistence.Forums.Methods.FORUMSCATEGORY_Fill(this.ForumsCategoryList, "");
            Persistence.Forums.Methods.TOPICS_Fill(this.TopicsList, "");
            Persistence.Forums.Methods.TOPICSRESPONSE_Fill(this.TopicsResponseList, "");
            Persistence.Forums.Methods.TOPICSRESPONSEVOTES_Fill(this.TopicsResponseVotesList, "");
            Persistence.Forums.Methods.TOPICSRESPONSEABUSE_Fill(this.TopicsResponseAbuseList, "");
            Persistence.Forums.Methods.TQUERYS_EXECUTE(this.CmdbCiList,"SELECT CMDBCI.IDCMDBCI,CMDBCI.IDCMDBCISTATE,CMDBCI.CI_GENERICNAME,CMDB_EFPEOPLEINFORMATION.FIRSTNAME,CMDB_EFPEOPLEINFORMATION.LASTNAME,CMDB_EFPEOPLEINFORMATION.MIDDLENAME FROM  CMDBCI LEFT JOIN CMDB_EFPEOPLEINFORMATION ON CMDB_EFPEOPLEINFORMATION.IDCMDBCI = CMDBCI.IDCMDBCI");
            Persistence.Forums.Methods.TQUERYS_EXECUTE(this.MdCategoryDetailList, "SELECT MDCATEGORYDETAIL.IDMDCATEGORY, MDCATEGORYDETAIL.IDMDCATEGORYDETAIL, MDCATEGORYDETAIL.CATEGORYDESCRIPTION,MDCATEGORYDETAIL.CATEGORYNAME,MDCATEGORYDETAIL.CATEGORYSTATUS, TBLW.CATEGORY " +
            "FROM  MDCATEGORYDETAIL LEFT JOIN (SELECT MDCATEGORY.IDMDCATEGORY,(Case When (MDCATEGORY.CATEGORY1 is null) then '' else (Case When (MDCATEGORY.CATEGORY2 is null) then CATEGORY1 else (Case When (MDCATEGORY.CATEGORY3 is null) then CATEGORY1+ '-'+ CATEGORY2 else (Case When (MDCATEGORY.CATEGORY4 is null) then CATEGORY1+ '-'+ CATEGORY2+'-'+ CATEGORY3 else " + 
            "(Case When (MDCATEGORY.CATEGORY5 is null) then CATEGORY1+ '-'+ CATEGORY2+'-'+ CATEGORY3+'-'+ CATEGORY4 else (Case When (MDCATEGORY.CATEGORY6 is null) then CATEGORY1+ '-'+ CATEGORY2+'-'+ CATEGORY3+'-'+ CATEGORY4+'-'+CATEGORY5 else (Case When (MDCATEGORY.CATEGORY7 is null) then CATEGORY1+ '-'+ CATEGORY2+'-'+ CATEGORY3+'-'+ CATEGORY4+'-'+CATEGORY5+'-'+CATEGORY6 else (Case When (MDCATEGORY.CATEGORY8 is null) then CATEGORY1+ '-'+ CATEGORY2+'-'+ CATEGORY3+'-'+ CATEGORY4+'-'+CATEGORY5+'-'+CATEGORY6+'-'+CATEGORY7 else "+
            "CATEGORY1+ '-'+CATEGORY2+'-'+CATEGORY3+'-'+CATEGORY4+'-'+CATEGORY5+'-'+CATEGORY6+'-'+CATEGORY7+'-'+CATEGORY8 end)end)end)end)end)end)end)end) CATEGORY FROM MDCATEGORY) TBLW ON MDCATEGORYDETAIL.IDMDCATEGORY = TBLW.IDMDCATEGORY WHERE MDCATEGORYDETAIL.CATEGORYSTATUS = 1");
            Persistence.Forums.Methods.StartReation(this, -1);
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("ForumsProfiler.js Persistence.Forums.TForumsProfile this.Fill", e);
        }
    }
    this.CreateAllData = function () {
        try {
            //**********************   ADD FORUMS   *****************************************************************************************
            var FORUMS1 = new Persistence.Forums.Properties.TFORUMS();
            FORUMS1.IDFORUMS = 0;
            FORUMS1.IDCMDBCI = 1;
            FORUMS1.NAMEFORUMS = "FORO 1";
            FORUMS1.DESCRIPTIONFORUMS = "DESCRIPTION FORO 1";
            Persistence.Forums.Methods.FORUMS_ADD(FORUMS1);
            var FORUMS2 = new Persistence.Forums.Properties.TFORUMS();
            FORUMS2.IDFORUMS = 0;
            FORUMS2.IDCMDBCI = 2;
            FORUMS2.NAMEFORUMS = "FORO 2";
            FORUMS2.DESCRIPTIONFORUMS = "DESCRIPTION FORO 2";
            Persistence.Forums.Methods.FORUMS_ADD(FORUMS2);
            //*******************************************************************************************************************************
            //********************** ADD FORUMS CATEGORY ************************************************************************************
            var FORUMSCATEGORY1 = new Persistence.Forums.Properties.TFORUMSCATEGORY();
            FORUMSCATEGORY1.IDFORUMSCATEGORY = 0;
            FORUMSCATEGORY1.IDMDCATEGORYDETAIL = 1;
            FORUMSCATEGORY1.IDFORUMS = FORUMS1.IDFORUMS;
            FORUMSCATEGORY1.DESCRIPTIONFORUMSCATEGORY = "CATEGORY 1";
            FORUMSCATEGORY1.TOPICSFORUMSCATEGORY_COUNT = 0;
            FORUMSCATEGORY1.RESPONSEFORUMSCATEGORY_COUNT = 0;
            FORUMSCATEGORY1.MEMBERSFORUMSCATEGORY_COUNT = 0;
            FORUMSCATEGORY1.IMAGEFORUMSCATEGORY = "img/img_tab/imgTabs/body_01.png";
            FORUMSCATEGORY1.STATUSFORUMSCATEGORY = true;
            Persistence.Forums.Methods.FORUMSCATEGORY_ADD(FORUMSCATEGORY1);
            var FORUMSCATEGORY2 = new Persistence.Forums.Properties.TFORUMSCATEGORY();
            FORUMSCATEGORY2.IDFORUMSCATEGORY = 0;
            FORUMSCATEGORY2.IDMDCATEGORYDETAIL = 2;
            FORUMSCATEGORY2.IDFORUMS = FORUMS2.IDFORUMS;
            FORUMSCATEGORY2.DESCRIPTIONFORUMSCATEGORY = "CATEGORY 2";
            FORUMSCATEGORY2.TOPICSFORUMSCATEGORY_COUNT = 0;
            FORUMSCATEGORY2.RESPONSEFORUMSCATEGORY_COUNT = 0;
            FORUMSCATEGORY2.MEMBERSFORUMSCATEGORY_COUNT = 0;
            FORUMSCATEGORY2.IMAGEFORUMSCATEGORY = "img/img_tab/imgTabs/body_01.png";
            FORUMSCATEGORY2.STATUSFORUMSCATEGORY = true;
            Persistence.Forums.Methods.FORUMSCATEGORY_ADD(FORUMSCATEGORY2);
            //*****************************************ADD TOPICS****************************************************************************
            var TOPIC1 = new Persistence.Forums.Properties.TTOPICS();
            TOPIC1.IDTOPICS = 0;
            TOPIC1.IDFORUMSCATEGORY = FORUMSCATEGORY1.IDFORUMSCATEGORY;
            TOPIC1.IDCMDBCI = 1;
            TOPIC1.NAMETOPICS = "Topic 1";
            TOPIC1.DESCRIPTIONTOPICS = "Description Topic 1";
            TOPIC1.RESPONSETOPICS_COUNT = 0;
            TOPIC1.VIEWSTOPICS_COUNT = 0;
            TOPIC1.IMAGETOPICS = "img/img_tab/imgTabs/body_01.png";
            TOPIC1.STATUSTOPICS = true;
            TOPIC1.CREATEDDATETOPICS = new Date(1970, 0, 1, 0, 0, 0);
            TOPIC1.KEYWORDSTOPICS = "Topic1 _topic1 Topic1 1";
            Persistence.Forums.Methods.TOPICS_ADD(TOPIC1);

            var TOPIC2 = new Persistence.Forums.Properties.TTOPICS();
            TOPIC2.IDTOPICS = 0;
            TOPIC2.IDFORUMSCATEGORY = FORUMSCATEGORY1.IDFORUMSCATEGORY;
            TOPIC2.IDCMDBCI = 1;
            TOPIC2.NAMETOPICS = "Topic 2";
            TOPIC2.DESCRIPTIONTOPICS = "Description Topic 2";
            TOPIC2.RESPONSETOPICS_COUNT = 0;
            TOPIC2.VIEWSTOPICS_COUNT = 0;
            TOPIC2.IMAGETOPICS = "img/img_tab/imgTabs/body_01.png";
            TOPIC2.STATUSTOPICS = true;
            TOPIC2.CREATEDDATETOPICS = new Date(1970, 0, 1, 0, 0, 0);
            TOPIC2.KEYWORDSTOPICS = "Topic2 _topic2 Topic2 2";
            Persistence.Forums.Methods.TOPICS_ADD(TOPIC2);

            var TOPIC3 = new Persistence.Forums.Properties.TTOPICS();
            TOPIC3.IDTOPICS = 0;
            TOPIC3.IDFORUMSCATEGORY = FORUMSCATEGORY1.IDFORUMSCATEGORY;
            TOPIC3.IDCMDBCI = 1;
            TOPIC3.NAMETOPICS = "Topic 3";
            TOPIC3.DESCRIPTIONTOPICS = "Description Topic 3";
            TOPIC3.RESPONSETOPICS_COUNT = 0;
            TOPIC3.VIEWSTOPICS_COUNT = 0;
            TOPIC3.IMAGETOPICS = "img/img_tab/imgTabs/body_01.png";
            TOPIC3.STATUSTOPICS = true;
            TOPIC3.CREATEDDATETOPICS = new Date(1970, 0, 1, 0, 0, 0);
            TOPIC3.KEYWORDSTOPICS = "Topic3 _topic3 Topic3 3";
            Persistence.Forums.Methods.TOPICS_ADD(TOPIC3);

            var TOPIC4 = new Persistence.Forums.Properties.TTOPICS();
            TOPIC4.IDTOPICS = 0;
            TOPIC4.IDFORUMSCATEGORY = FORUMSCATEGORY2.IDFORUMSCATEGORY;
            TOPIC4.IDCMDBCI = 1;
            TOPIC4.NAMETOPICS = "Topic 4";
            TOPIC4.DESCRIPTIONTOPICS = "Description Topic 4";
            TOPIC4.RESPONSETOPICS_COUNT = 0;
            TOPIC4.VIEWSTOPICS_COUNT = 0;
            TOPIC4.IMAGETOPICS = "img/img_tab/imgTabs/body_01.png";
            TOPIC4.STATUSTOPICS = true;
            TOPIC4.CREATEDDATETOPICS = new Date(1970, 0, 1, 0, 0, 0);
            TOPIC4.KEYWORDSTOPICS = "Topic4 _topic4 Topic4 4";
            Persistence.Forums.Methods.TOPICS_ADD(TOPIC4);
            var TOPIC5 = new Persistence.Forums.Properties.TTOPICS();
            TOPIC5.IDTOPICS = 0;
            TOPIC5.IDFORUMSCATEGORY = FORUMSCATEGORY2.IDFORUMSCATEGORY;
            TOPIC5.IDCMDBCI = 1;
            TOPIC5.NAMETOPICS = "Topic 5";
            TOPIC5.DESCRIPTIONTOPICS = "Description Topic 5";
            TOPIC5.RESPONSETOPICS_COUNT = 0;
            TOPIC5.VIEWSTOPICS_COUNT = 0;
            TOPIC5.IMAGETOPICS = "img/img_tab/imgTabs/body_01.png";
            TOPIC5.STATUSTOPICS = true;
            TOPIC5.CREATEDDATETOPICS = new Date(1970, 0, 1, 0, 0, 0);
            TOPIC5.KEYWORDSTOPICS = "Topic5 _topic5 Topic5 5";
            Persistence.Forums.Methods.TOPICS_ADD(TOPIC5);
            var TOPIC6 = new Persistence.Forums.Properties.TTOPICS();
            TOPIC6.IDTOPICS = 0;
            TOPIC6.IDFORUMSCATEGORY = FORUMSCATEGORY2.IDFORUMSCATEGORY;
            TOPIC6.IDCMDBCI = 1;
            TOPIC6.NAMETOPICS = "Topic 6";
            TOPIC6.DESCRIPTIONTOPICS = "Description Topic 6";
            TOPIC6.RESPONSETOPICS_COUNT = 0;
            TOPIC6.VIEWSTOPICS_COUNT = 0;
            TOPIC6.IMAGETOPICS = "img/img_tab/imgTabs/body_01.png";
            TOPIC6.STATUSTOPICS = true;
            TOPIC6.CREATEDDATETOPICS = new Date(1970, 0, 1, 0, 0, 0);
            TOPIC6.KEYWORDSTOPICS = "Topic6 _topic6 Topic6 6";
            Persistence.Forums.Methods.TOPICS_ADD(TOPIC6);
            //*******************************************************************************************************************************
            ////********************** ADD TOPICS RESPONSE ************************************************************************************
            var TOPICSRESPONSE1 = new Persistence.Forums.Properties.TTOPICSRESPONSE();
            TOPICSRESPONSE1.IDTOPICSRESPONSE = 0;
            TOPICSRESPONSE1.IDTOPICS = TOPIC1.IDTOPICS;
            TOPICSRESPONSE1.IDCMDBCI = 1;
            TOPICSRESPONSE1.IDTOPICSRESPONSE_PARENT = 0;
            TOPICSRESPONSE1.TITLETOPICSRESPONSE = "Title Response Topic 1";
            TOPICSRESPONSE1.SUBJECTTOPICSRESPONSE = "Content Response Topic 1";
            TOPICSRESPONSE1.STATUSTOPICSRESPOSE = true;
            TOPICSRESPONSE1.CREATEDDATETOPICSRESPONSE = new Date(1970, 0, 1, 0, 0, 0);
            TOPICSRESPONSE1.LIKETOPICSRESPONSE_COUNT = 0;
            TOPICSRESPONSE1.NOTLIKETOPICSRESPONSE_COUNT = 0;
            TOPICSRESPONSE1.ABUSETOPICSRESPONSE_COUNT = 0;
            Persistence.Forums.Methods.TOPICSRESPONSE_ADD(TOPICSRESPONSE1);
            var TOPICSRESPONSE2 = new Persistence.Forums.Properties.TTOPICSRESPONSE();
            TOPICSRESPONSE2.IDTOPICSRESPONSE = 0;
            TOPICSRESPONSE2.IDTOPICS = TOPIC2.IDTOPICS;
            TOPICSRESPONSE2.IDCMDBCI = 1;
            TOPICSRESPONSE2.IDTOPICSRESPONSE_PARENT = 0;
            TOPICSRESPONSE2.TITLETOPICSRESPONSE = "Title Response Topic 2";
            TOPICSRESPONSE2.SUBJECTTOPICSRESPONSE = "Content Response Topic 2";
            TOPICSRESPONSE2.STATUSTOPICSRESPOSE = true;
            TOPICSRESPONSE2.CREATEDDATETOPICSRESPONSE = new Date(1970, 0, 1, 0, 0, 0);
            TOPICSRESPONSE2.LIKETOPICSRESPONSE_COUNT = 0;
            TOPICSRESPONSE2.NOTLIKETOPICSRESPONSE_COUNT = 0;
            TOPICSRESPONSE2.ABUSETOPICSRESPONSE_COUNT = 0;
            Persistence.Forums.Methods.TOPICSRESPONSE_ADD(TOPICSRESPONSE2);
            var TOPICSRESPONSE3 = new Persistence.Forums.Properties.TTOPICSRESPONSE();
            TOPICSRESPONSE3.IDTOPICSRESPONSE = 0;
            TOPICSRESPONSE3.IDTOPICS = TOPIC3.IDTOPICS;
            TOPICSRESPONSE3.IDCMDBCI = 1;
            TOPICSRESPONSE3.IDTOPICSRESPONSE_PARENT = 0;
            TOPICSRESPONSE3.TITLETOPICSRESPONSE = "Title Response Topic 3";
            TOPICSRESPONSE3.SUBJECTTOPICSRESPONSE = "Content Response Topic 3";
            TOPICSRESPONSE3.STATUSTOPICSRESPOSE = true;
            TOPICSRESPONSE3.CREATEDDATETOPICSRESPONSE = new Date(1970, 0, 1, 0, 0, 0);
            TOPICSRESPONSE3.LIKETOPICSRESPONSE_COUNT = 0;
            TOPICSRESPONSE3.NOTLIKETOPICSRESPONSE_COUNT = 0;
            TOPICSRESPONSE3.ABUSETOPICSRESPONSE_COUNT = 0;
            Persistence.Forums.Methods.TOPICSRESPONSE_ADD(TOPICSRESPONSE3);
            var TOPICSRESPONSE4 = new Persistence.Forums.Properties.TTOPICSRESPONSE();
            TOPICSRESPONSE4.IDTOPICSRESPONSE = 0;
            TOPICSRESPONSE4.IDTOPICS = TOPIC4.IDTOPICS;
            TOPICSRESPONSE4.IDCMDBCI = 1;
            TOPICSRESPONSE4.IDTOPICSRESPONSE_PARENT = 0;
            TOPICSRESPONSE4.TITLETOPICSRESPONSE = "Title Response Topic 4";
            TOPICSRESPONSE4.SUBJECTTOPICSRESPONSE = "Content Response Topic 4";
            TOPICSRESPONSE4.STATUSTOPICSRESPOSE = true;
            TOPICSRESPONSE4.CREATEDDATETOPICSRESPONSE = new Date(1970, 0, 1, 0, 0, 0);
            TOPICSRESPONSE4.LIKETOPICSRESPONSE_COUNT = 0;
            TOPICSRESPONSE4.NOTLIKETOPICSRESPONSE_COUNT = 0;
            TOPICSRESPONSE4.ABUSETOPICSRESPONSE_COUNT = 0;
            Persistence.Forums.Methods.TOPICSRESPONSE_ADD(TOPICSRESPONSE4);
            var TOPICSRESPONSE5 = new Persistence.Forums.Properties.TTOPICSRESPONSE();
            TOPICSRESPONSE5.IDTOPICSRESPONSE = 0;
            TOPICSRESPONSE5.IDTOPICS = TOPIC5.IDTOPICS;
            TOPICSRESPONSE5.IDCMDBCI = 1;
            TOPICSRESPONSE5.IDTOPICSRESPONSE_PARENT = 0;
            TOPICSRESPONSE5.TITLETOPICSRESPONSE = "Title Response Topic 5";
            TOPICSRESPONSE5.SUBJECTTOPICSRESPONSE = "Content Response Topic 5";
            TOPICSRESPONSE5.STATUSTOPICSRESPOSE = true;
            TOPICSRESPONSE5.CREATEDDATETOPICSRESPONSE = new Date(1970, 0, 1, 0, 0, 0);
            TOPICSRESPONSE5.LIKETOPICSRESPONSE_COUNT = 0;
            TOPICSRESPONSE5.NOTLIKETOPICSRESPONSE_COUNT = 0;
            TOPICSRESPONSE5.ABUSETOPICSRESPONSE_COUNT = 0;
            Persistence.Forums.Methods.TOPICSRESPONSE_ADD(TOPICSRESPONSE5);
            var TOPICSRESPONSE6 = new Persistence.Forums.Properties.TTOPICSRESPONSE();
            TOPICSRESPONSE6.IDTOPICSRESPONSE = 0;
            TOPICSRESPONSE6.IDTOPICS = TOPIC6.IDTOPICS;
            TOPICSRESPONSE6.IDCMDBCI = 1;
            TOPICSRESPONSE6.IDTOPICSRESPONSE_PARENT = 0;
            TOPICSRESPONSE6.TITLETOPICSRESPONSE = "Title Response Topic 6";
            TOPICSRESPONSE6.SUBJECTTOPICSRESPONSE = "Content Response Topic 6";
            TOPICSRESPONSE6.STATUSTOPICSRESPOSE = true;
            TOPICSRESPONSE6.CREATEDDATETOPICSRESPONSE = new Date(1970, 0, 1, 0, 0, 0);
            TOPICSRESPONSE6.LIKETOPICSRESPONSE_COUNT = 0;
            TOPICSRESPONSE6.NOTLIKETOPICSRESPONSE_COUNT = 0;
            TOPICSRESPONSE6.ABUSETOPICSRESPONSE_COUNT = 0;
            Persistence.Forums.Methods.TOPICSRESPONSE_ADD(TOPICSRESPONSE6);
            ////********************** FORUMS SEARCH ************************************************************************************
            var FORUMSEARCH1 = new Persistence.Forums.Properties.TFORUMSSEARCH();
            FORUMSEARCH1.IDFORUMSSEARCH = 0;
            FORUMSEARCH1.IDFORUMS = FORUMS1.IDFORUMS;
            FORUMSEARCH1.NAMEFORUMSSEARCH = "Most  Reported";
            FORUMSEARCH1.IMAGEFORUMSSEARCH = "image/16/add.png";
            FORUMSEARCH1.QUERYFORUMSSEARCH = "select * from(select (select count(IDTOPICSRESPONSE) from TOPICSRESPONSE where IDTOPICS = [T].IDTOPICS and ABUSETOPICSRESPONSE_COUNT=1) as COUNTABUSE , * FROM TOPICS [T] WHERE [T].IDFORUMSCATEGORY= {IDFORUMSCATEGORY}) TOPICTOTAL order by COUNTABUSE desc";
            Persistence.Forums.Methods.FORUMSSEARCH_ADD(FORUMSEARCH1);
            var FORUMSEARCH2 = new Persistence.Forums.Properties.TFORUMSSEARCH();
            FORUMSEARCH2.IDFORUMSSEARCH = 0;
            FORUMSEARCH2.IDFORUMS = FORUMS1.IDFORUMS;
            FORUMSEARCH2.NAMEFORUMSSEARCH = "Most Viewed";
            FORUMSEARCH2.IMAGEFORUMSSEARCH = "image/16/add.png";
            FORUMSEARCH2.QUERYFORUMSSEARCH = "select * from TOPICS where IDFORUMSCATEGORY = {IDFORUMSCATEGORY} order by VIEWSTOPICS_COUNT desc";
            Persistence.Forums.Methods.FORUMSSEARCH_ADD(FORUMSEARCH2);
            var FORUMSEARCH3 = new Persistence.Forums.Properties.TFORUMSSEARCH();
            FORUMSEARCH3.IDFORUMSSEARCH = 0;
            FORUMSEARCH3.IDFORUMS = FORUMS1.IDFORUMS;
            FORUMSEARCH3.NAMEFORUMSSEARCH = "Most Popular";
            FORUMSEARCH3.IMAGEFORUMSSEARCH = "image/16/add.png";
            FORUMSEARCH3.QUERYFORUMSSEARCH = "select * from(select (select count(IDTOPICSRESPONSE) from TOPICSRESPONSE where IDTOPICS = [T].IDTOPICS and LIKETOPICSRESPONSE_COUNT=1) as COUNTLIKE , * FROM TOPICS [T] WHERE [T].IDFORUMSCATEGORY= {IDFORUMSCATEGORY}) TOPICTOTAL order by COUNTLIKE desc";
            Persistence.Forums.Methods.FORUMSSEARCH_ADD(FORUMSEARCH3);
            var FORUMSEARCH4 = new Persistence.Forums.Properties.TFORUMSSEARCH();
            FORUMSEARCH4.IDFORUMSSEARCH = 0;
            FORUMSEARCH4.IDFORUMS = FORUMS1.IDFORUMS;
            FORUMSEARCH4.NAMEFORUMSSEARCH = "Most Conflictive";
            FORUMSEARCH4.IMAGEFORUMSSEARCH = "image/16/add.png";
            FORUMSEARCH4.QUERYFORUMSSEARCH = "select * from(select (select count(IDTOPICSRESPONSE) from TOPICSRESPONSE where IDTOPICS = [T].IDTOPICS and NOTLIKETOPICSRESPONSE_COUNT=1) as COUNTNOTLIKE , * FROM TOPICS [T] WHERE [T].IDFORUMSCATEGORY= {IDFORUMSCATEGORY}) TOPICTOTAL order by COUNTNOTLIKE desc";
            Persistence.Forums.Methods.FORUMSSEARCH_ADD(FORUMSEARCH4);
            var FORUMSEARCH5 = new Persistence.Forums.Properties.TFORUMSSEARCH();
            FORUMSEARCH5.IDFORUMSSEARCH = 0;
            FORUMSEARCH5.IDFORUMS = FORUMS1.IDFORUMS;
            FORUMSEARCH5.NAMEFORUMSSEARCH = "Most Answered";
            FORUMSEARCH5.IMAGEFORUMSSEARCH = "image/16/add.png";
            FORUMSEARCH5.QUERYFORUMSSEARCH = "select * from TOPICS where IDFORUMSCATEGORY = {IDFORUMSCATEGORY} order by RESPONSETOPICS_COUNT desc";
            Persistence.Forums.Methods.FORUMSSEARCH_ADD(FORUMSEARCH5);
            var FORUMSEARCH6 = new Persistence.Forums.Properties.TFORUMSSEARCH();
            FORUMSEARCH6.IDFORUMSSEARCH = 0;
            FORUMSEARCH6.IDFORUMS = FORUMS1.IDFORUMS;
            FORUMSEARCH6.NAMEFORUMSSEARCH = "Most Recent";
            FORUMSEARCH6.IMAGEFORUMSSEARCH = "image/16/add.png";
            FORUMSEARCH6.QUERYFORUMSSEARCH = "select * from(select (select TOP 1 CREATEDDATETOPICSRESPONSE from TOPICSRESPONSE where IDTOPICS = [T].IDTOPICS and STATUSTOPICSRESPOSE=1 order by CREATEDDATETOPICSRESPONSE desc) as CREATETOPICRESPONSEDATE , * FROM TOPICS [T] WHERE [T].IDFORUMSCATEGORY= {IDFORUMSCATEGORY}) TOPICTOTAL order by CREATETOPICRESPONSEDATE desc";
            Persistence.Forums.Methods.FORUMSSEARCH_ADD(FORUMSEARCH6);
            ////*************************************************************************************************************************************
            ////********************** ADD TOPICS RESPONSE ABUSE ************************************************************************************
            //var TOPICSRESPONSEABUSE1 = new Persistence.Forums.Properties.TTOPICSRESPONSEABUSE();
            //TOPICSRESPONSEABUSE1.IDTOPICSRESPONSEABUSE = 0;
            //TOPICSRESPONSEABUSE1.IDTOPICSRESPONSE = TOPICSRESPONSE8.IDTOPICSRESPONSE;
            //TOPICSRESPONSEABUSE1.IDCMDBCI = 1;
            //TOPICSRESPONSEABUSE1.LIKETOPICSRESPONSEABUSE = false;
            //TOPICSRESPONSEABUSE1.NOTLIKETOPICSRESPONSEABUSE = false;
            //Persistence.Forums.Methods.TOPICSRESPONSEABUSE_ADD(TOPICSRESPONSEABUSE1);
            //var TOPICSRESPONSEABUSE2 = new Persistence.Forums.Properties.TTOPICSRESPONSEABUSE();
            //TOPICSRESPONSEABUSE2.IDTOPICSRESPONSEABUSE = 0;
            //TOPICSRESPONSEABUSE2.IDTOPICSRESPONSE = TOPICSRESPONSE9.IDTOPICSRESPONSE;
            //TOPICSRESPONSEABUSE2.IDCMDBCI = 1;
            //TOPICSRESPONSEABUSE2.LIKETOPICSRESPONSEABUSE = false;
            //TOPICSRESPONSEABUSE2.NOTLIKETOPICSRESPONSEABUSE = false;
            //Persistence.Forums.Methods.TOPICSRESPONSEABUSE_ADD(TOPICSRESPONSEABUSE2);
            //var TOPICSRESPONSEABUSE3 = new Persistence.Forums.Properties.TTOPICSRESPONSEABUSE();
            //TOPICSRESPONSEABUSE3.IDTOPICSRESPONSEABUSE = 0;
            //TOPICSRESPONSEABUSE3.IDTOPICSRESPONSE = TOPICSRESPONSE8.IDTOPICSRESPONSE;
            //TOPICSRESPONSEABUSE3.IDCMDBCI = 1;
            //TOPICSRESPONSEABUSE3.LIKETOPICSRESPONSEABUSE = false;
            //TOPICSRESPONSEABUSE3.NOTLIKETOPICSRESPONSEABUSE = false;
            //Persistence.Forums.Methods.TOPICSRESPONSEABUSE_ADD(TOPICSRESPONSEABUSE3);
            ////*************************************************************************************************************************************
            ////********************** ADD TOPICS RESPONSE VOTES ************************************************************************************
            //var TOPICSRESPONSEVOTES1 = new Persistence.Forums.Properties.TTOPICSRESPONSEVOTES();
            //TOPICSRESPONSEVOTES1.IDTOPICSRESPONSEVOTES = 0;
            //TOPICSRESPONSEVOTES1.IDTOPICSRESPONSE = TOPICSRESPONSE1.IDTOPICSRESPONSE;
            //TOPICSRESPONSEVOTES1.IDCMDBCI = 1;
            //TOPICSRESPONSEVOTES1.LIKETOPICSRESPONSE = false;
            //TOPICSRESPONSEVOTES1.NOTLIKETOPICSRESPONSE = false;
            //Persistence.Forums.Methods.TOPICSRESPONSEVOTES_ADD(TOPICSRESPONSEVOTES1);
            //var TOPICSRESPONSEVOTES2 = new Persistence.Forums.Properties.TTOPICSRESPONSEVOTES();
            //TOPICSRESPONSEVOTES2.IDTOPICSRESPONSEVOTES = 0;
            //TOPICSRESPONSEVOTES2.IDTOPICSRESPONSE = TOPICSRESPONSE2.IDTOPICSRESPONSE;
            //TOPICSRESPONSEVOTES2.IDCMDBCI = 1;
            //TOPICSRESPONSEVOTES2.LIKETOPICSRESPONSE = false;
            //TOPICSRESPONSEVOTES2.NOTLIKETOPICSRESPONSE = false;
            //Persistence.Forums.Methods.TOPICSRESPONSEVOTES_ADD(TOPICSRESPONSEVOTES2);
            //var TOPICSRESPONSEVOTES3 = new Persistence.Forums.Properties.TTOPICSRESPONSEVOTES();
            //TOPICSRESPONSEVOTES3.IDTOPICSRESPONSEVOTES = 0;
            //TOPICSRESPONSEVOTES3.IDTOPICSRESPONSE = TOPICSRESPONSE5.IDTOPICSRESPONSE;
            //TOPICSRESPONSEVOTES3.IDCMDBCI = 1;
            //TOPICSRESPONSEVOTES3.LIKETOPICSRESPONSE = false;
            //TOPICSRESPONSEVOTES3.NOTLIKETOPICSRESPONSE = false;
            //Persistence.Forums.Methods.TOPICSRESPONSEVOTES_ADD(TOPICSRESPONSEVOTES3);
            return (true)
            //*************************************************************************************************************************************
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("ForumsProfiler.js Persistence.Forums.TForumsProfile this.CreateAllData", e);
            return (false)
        }
    }
    this.ValidationData = function () {
        try {
            Persistence.Forums.Methods.FORUMS_Fill (this.ForumsList, "");
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("ForumsProfiler.js Persistence.Forums.TForumsProfile this.ValidationData", e);
        }
    }
    //**********************   CRUD FORUMS   *****************************************************************************************
    this.getFORUMS = function () {
        var _this = this.TParent();
        var newListForums = new Array();
        try {
            Persistence.Forums.Methods.FORUMS_Fill(newListForums, "");
        }
        catch (e) {
            SysCfg.Log.Methods.WriteLog("ForumsProfiler.js Persistence.Forums.TForumsProfile this.getFORUMS", e);
        }
        return (newListForums);
    }
    this.UpdFORUMS = function (objForums) {
        var _this = this.TParent();
        var objResponse = { Response: false, Validation: null, Result: null, Message: null };
        try {
            if (SysCfg.Str.Methods.IsNullOrEmpity(objForums.IDFORUMS) && objForums.IDFORUMS <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDFORUMS" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objForums.IDCMDBCI) && objForums.IDCMDBCI<=0) 
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDCMDBCI" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objForums.NAMEFORUMS))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORNAMEFORUMS" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objForums.DESCRIPTIONFORUMS))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORDESCRIPTIONFORUMS" });
            var success = Persistence.Forums.Methods.FORUMS_UPD(objForums);
            if (success.NotError) {
                var newListForums = _this.getFORUMS();
                var result = Persistence.Forums.Methods.FORUMS_ListSetID(newListForums, objForums.IDFORUMS)
                return (objResponse = { Response: true, Validation: false, Result: result, Message: "SUCCESSUPDATE" });
            }
            else {
                return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORUPDATE" });
            }
        }
        catch (e) {
            SysCfg.Log.Methods.WriteLog("ForumsProfiler.js Persistence.Forums.TForumsProfile this.UpdFORUMS", e);
            return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORUPDATE" });
        }
    }
    this.AddFORUMS = function (objForums) {
        var _this = this.TParent();
        var objResponse = { Response: false, Validation: null, Result: null, Message: null };
        try {
            if (SysCfg.Str.Methods.IsNullOrEmpity(objForums.IDFORUMS) && objForums.IDFORUMS <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDFORUMS" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objForums.IDCMDBCI) && objForums.IDCMDBCI <= 0) 
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDCMDBCI" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objForums.NAMEFORUMS))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORNAMEFORUMS" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objForums.DESCRIPTIONFORUMS))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "DESCRIPTIONFORUMS" });
            var success = Persistence.Forums.Methods.FORUMS_ADD(objForums);
            if (success.NotError) {
                return (objResponse = { Response: true, Validation: false, Result: result, Message: "SUCCESSADD" });
            }
            else {
                return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORADD" });
            }
        }
        catch (e) {
            SysCfg.Log.Methods.WriteLog("ForumsProfiler.js Persistence.Forums.TForumsProfile this.AddFORUMS", e);
            return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORADD" });
        }
    }
    //********************************************************************************************************************************
    //**********************   CRUD FORUMS CATEGORY   ********************************************************************************
    this.getFORUMSCATEGORY = function () {
        var _this = this.TParent();
        var newListForumsCategory = new Array();
        try {
            Persistence.Forums.Methods.FORUMSCATEGORY_Fill(newListForumsCategory, "");
        }
        catch (e) {
            SysCfg.Log.Methods.WriteLog("ForumsProfiler.js Persistence.Forums.TForumsProfile this.getFORUMSCATEGORY", e);
        }
        return (newListForumsCategory);
    }
    this.UpdFORUMSCATEGORY = function (objForumsCategory) {
        var _this = this.TParent();
        var objResponse = { Response: false, Validation: null, Result: null, Message: null };
        try {
            if (SysCfg.Str.Methods.IsNullOrEmpity(objForumsCategory.IDFORUMSCATEGORY) && objForumsCategory.IDFORUMSCATEGORY  <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDFORUMSCATEGORY" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objForumsCategory.IDMDCATEGORYDETAIL) && objForumsCategory.IDMDCATEGORYDETAIL <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDMDCATEGORYDETAIL" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objForumsCategory.IDFORUMS) && objForumsCategory.IDFORUMS <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDFORUMS" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objForumsCategory.DESCRIPTIONFORUMSCATEGORY ))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORDESCRIPTIONFORUMSCATEGORY" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objForumsCategory.TOPICSFORUMSCATEGORY_COUNT) && objForumsCategory.IDFORUMSCATEGORY <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORTOPICSFORUMSCATEGORYCOUNT" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objForumsCategory.RESPONSEFORUMSCATEGORY_COUNT) && objForumsCategory.IDFORUMSCATEGORY <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORRESPONSEFORUMSCATEGORYCOUNT" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objForumsCategory.MEMBERSFORUMSCATEGORY_COUNT) && objForumsCategory.IDFORUMSCATEGORY <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORMEMBERSFORUMSCATEGORYCOUNT" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objForumsCategory.IMAGEFORUMSCATEGORY))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIMAGEFORUMSCATEGORY" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objForumsCategory.STATUSFORUMSCATEGORY))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORSTATUSFORUMSCATEGORY" });
            var success = Persistence.Forums.Methods.FORUMSCATEGORY_UPD(objForumsCategory);
            if (success.NotError) {
                var newListForumsCategory = _this.getFORUMSCATEGORY();
                var result = Persistence.Forums.Methods.FORUMSCATEGORY_ListSetID(newListForumsCategory, objForumsCategory.IDFORUMSCATEGORY)
                return (objResponse = { Response: true, Validation: false, Result: result, Message: "SUCCESSUPDATE" });
            }
            else {
                return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORUPDATE" });
            }
        }
        catch (e) {
            SysCfg.Log.Methods.WriteLog("ForumsProfiler.js Persistence.Forums.TForumsProfile this.UpdFORUMSCATEGORY", e);
            return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORUPDATE" });
        }
    }
    this.AddFORUMSCATEGORY = function (objForumsCategory) {
        var _this = this.TParent();
        var objResponse = { Response: false, Validation: null, Result: null, Message: null };
        try {
            if (SysCfg.Str.Methods.IsNullOrEmpity(objForumsCategory.IDFORUMSCATEGORY) && objForumsCategory.IDFORUMSCATEGORY <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDFORUMSCATEGORY" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objForumsCategory.IDMDCATEGORYDETAIL) && objForumsCategory.IDMDCATEGORYDETAIL <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDMDCATEGORYDETAIL" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objForumsCategory.IDFORUMS) && objForumsCategory.IDFORUMS <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDFORUMS" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objForumsCategory.DESCRIPTIONFORUMSCATEGORY))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORDESCRIPTIONFORUMSCATEGORY" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objForumsCategory.TOPICSFORUMSCATEGORY_COUNT) && objForumsCategory.IDFORUMSCATEGORY <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORTOPICSFORUMSCATEGORYCOUNT" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objForumsCategory.RESPONSEFORUMSCATEGORY_COUNT) && objForumsCategory.IDFORUMSCATEGORY <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORRESPONSEFORUMSCATEGORYCOUNT" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objForumsCategory.MEMBERSFORUMSCATEGORY_COUNT) && objForumsCategory.IDFORUMSCATEGORY <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORMEMBERSFORUMSCATEGORYCOUNT" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objForumsCategory.IMAGEFORUMSCATEGORY))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIMAGEFORUMSCATEGORY" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objForumsCategory.STATUSFORUMSCATEGORY))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORSTATUSFORUMSCATEGORY" });
            var success = Persistence.Forums.Methods.FORUMSCATEGORY_ADD(objForumsCategory);
            if (success.NotError) {
                return (objResponse = { Response: true, Validation: false, Result: result, Message: "SUCCESSADD" });
            }
            else {
                return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORADD" });
            }
        }
        catch (e) {
            SysCfg.Log.Methods.WriteLog("ForumsProfiler.js Persistence.Forums.TForumsProfile this.AddFORUMSCATEGORY", e);
            return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORADD" });
        }
    }
    //********************************************************************************************************************************
    //**********************   CRUD TOPICS   *****************************************************************************************
    this.getTOPICS = function () {
        var _this = this.TParent();
        var newListTopics = new Array();
        try {
            Persistence.Forums.Methods.TOPICS_Fill(newListTopics, "");
        }
        catch (e) {
            SysCfg.Log.Methods.WriteLog("ForumsProfiler.js Persistence.Forums.TForumsProfile this.getTOPICS", e);
        }
        return (newListTopics);
    }
    this.UpdTOPICS = function (objTopics) {
        var _this = this.TParent();
        var objResponse = { Response: false, Validation: null, Result: null, Message: null };
        try {
            if (SysCfg.Str.Methods.IsNullOrEmpity(objTopics.IDTOPICS) || objTopics.IDTOPICS <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDTOPICS" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopics.IDFORUMSCATEGORY) || objTopics.IDFORUMSCATEGORY <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDFORUMSCATEGORY" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopics.IDCMDBCI) || objTopics.IDCMDBCI <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDCMDBCI" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopics.NAMETOPICS))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORNAMETOPICS" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopics.DESCRIPTIONTOPICS))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORDESCRIPTIONTOPICS" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopics.RESPONSETOPICS_COUNT) || isNaN(objTopics.RESPONSETOPICS_COUNT))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORRESPONSETOPICSCOUNT" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopics.VIEWSTOPICS_COUNT) || isNaN(objTopics.VIEWSTOPICS_COUNT))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORVIEWSTOPICSCOUNT" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopics.IMAGETOPICS))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIMAGETOPICS" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopics.CREATEDDATETOPICS))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORCREATEDDATETOPICS" });
            var success = Persistence.Forums.Methods.TOPICS_UPD(objTopics);
            if (success.NotError) {
                return (objResponse = { Response: true, Validation: false, Result: objTopics, Message: "SUCCESSUPDATE" });
            }
            else {
                return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORUPDATE" });
            }
        }
        catch (e) {
            SysCfg.Log.Methods.WriteLog("ForumsProfiler.js Persistence.Forums.TForumsProfile this.UpdTOPICS", e);
            return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORUPDATE" });
        }
    }
    this.DeleteTOPICSALL = function (objTopics) {
        var _this = this.TParent();
        var objResponse = { Response: false, Validation: null, Result: null, Message: null };
        try {
            if (SysCfg.Str.Methods.IsNullOrEmpity(objTopics.IDTOPICS) || objTopics.IDTOPICS <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDTOPICS" });
            var newListTopics = _this.getTOPICS();
            var result = Persistence.Forums.Methods.TOPICS_ListSetID(newListTopics, objTopics.IDTOPICS)
            var success = Persistence.Forums.Methods.TOPICS_DELTOPICS_ALL(objTopics);
            if (success.NotError) {
                _this.Fill();
                return (objResponse = { Response: true, Validation: false, Result: result, Message: "SUCCESSDELETE" });
            }
            else {
                return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORDELETE" });
            }
        }
        catch (e) {
            SysCfg.Log.Methods.WriteLog("ForumsProfiler.js Persistence.Forums.TForumsProfile this.DeleteTOPICS", e);
            return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORDELETE" });
        }
    }
    this.UpdTOPICSVIEW = function (idTopic) {
        var _this = this.TParent();
        var objResponse = { Response: false, Validation: null, Result: null, Message: null };
        try {
            if (SysCfg.Str.Methods.IsNullOrEmpity(idTopic) || idTopic <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDTOPICS" });
            if (Persistence.Forums.Methods.TOPICS_UPDVIEWS(idTopic)) {
                var newListTopics = _this.getTOPICS();
                var result = Persistence.Forums.Methods.TOPICS_ListSetID(newListTopics, idTopic)
                return (objResponse = { Response: true, Validation: false, Result: result, Message: "SUCCESSUPDATE" });
            }
            else {
                return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORUPDATE" });
            }
        }
        catch (e) {
            SysCfg.Log.Methods.WriteLog("ForumsProfiler.js Persistence.Forums.TForumsProfile this.UpdTOPICSVIEW", e);
            return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORUPDATE" });
        }
    }
    this.AddTOPICS = function (objTopics) {
        var _this = this.TParent();
        var objResponse = { Response: false, Validation: null, Result: null, Message: null };
        try {
            if (SysCfg.Str.Methods.IsNullOrEmpity(objTopics.IDTOPICS) && objTopics.IDTOPICS <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDTOPICS" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopics.IDFORUMSCATEGORY) && objTopics.IDFORUMSCATEGORY <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDFORUMSCATEGORY" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopics.IDCMDBCI) && objTopics.IDCMDBCI <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDCMDBCI" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopics.NAMETOPICS))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORNAMETOPICS" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopics.DESCRIPTIONTOPICS))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORDESCRIPTIONTOPICS" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopics.RESPONSETOPICS_COUNT) && isNaN(objTopics.RESPONSETOPICS_COUNT))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORRESPONSETOPICS_COUNT" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopics.VIEWSTOPICS_COUNT) && isNaN(objTopics.VIEWSTOPICS_COUNT))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORVIEWSTOPICS_COUNT" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopics.IMAGETOPICS))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIMAGETOPICS" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopics.STATUSTOPICS))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORSTATUSTOPICS" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopics.CREATEDDATETOPICS))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORCREATEDDATETOPICS" });
            var success = Persistence.Forums.Methods.TOPICS_ADD(objTopics);
            if (success.NotError) {
                _this.Fill();
                return (objResponse = { Response: true, Validation: false, Result: objTopics, Message: "SUCCESSADD" });
            }
            else {
                return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORADD" });
            }
        }
        catch (e) {
            SysCfg.Log.Methods.WriteLog("ForumsProfiler.js Persistence.Forums.TForumsProfile this.AddTOPICS", e);
            return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORADD" });
        }
    }
    //********************************************************************************************************************************
    //**********************   CRUD TOPICS REPONSE   *********************************************************************************
    this.getTOPICSRESPONSE = function () {
        var _this = this.TParent();
        var newListTopicsResponse= new Array();
        try {
            Persistence.Forums.Methods.TOPICSRESPONSE_Fill(newListTopicsResponse, "");
        }
        catch (e) {
            SysCfg.Log.Methods.WriteLog("ForumsProfiler.js Persistence.Forums.TForumsProfile this.getTOPICSRESPONSE", e);
        }
        return (newListTopicsResponse);
    }
    this.UpdTOPICSRESPONSE = function (objTopicsResponse) {
        var _this = this.TParent();
        var objResponse = { Response: false, Validation: null, Result: null, Message: null };
        try {
            if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponse.IDTOPICSRESPONSE) || objTopicsResponse.IDTOPICSRESPONSE <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDTOPICSRESPONSE" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponse.IDTOPICS) || objTopicsResponse.IDTOPICS  <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDTOPICS" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponse.IDCMDBCI) || objTopicsResponse.IDCMDBCI <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDCMDBCI" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponse.IDTOPICSRESPONSE_PARENT) || isNaN(objTopicsResponse.IDTOPICSRESPONSE_PARENT))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDTOPICSRESPONSEPARENT" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponse.TITLETOPICSRESPONSE))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORTITLETOPICSRESPONSE" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponse.SUBJECTTOPICSRESPONSE))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORSUBJECTTOPICSRESPONSE" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponse.STATUSTOPICSRESPOSE))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORSTATUSTOPICSRESPOSE" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponse.CREATEDDATETOPICSRESPONSE))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORCREATEDDATETOPICSRESPONSE" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponse.LIKETOPICSRESPONSE_COUNT) || isNaN(objTopicsResponse.LIKETOPICSRESPONSE_COUNT))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORLIKETOPICSRESPONSECOUNT" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponse.NOTLIKETOPICSRESPONSE_COUNT) || isNaN(objTopicsResponse.NOTLIKETOPICSRESPONSE_COUNT ))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORNOTLIKETOPICSRESPONSECOUNT" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponse.ABUSETOPICSRESPONSE_COUNT) || isNaN(objTopicsResponse.ABUSETOPICSRESPONSE_COUNT))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORABUSETOPICSRESPONSECOUNT" });
            var success = Persistence.Forums.Methods.TOPICSRESPONSE_UPD(objTopicsResponse);
            if (success.NotError) {
                _this.Fill();
                return (objResponse = { Response: true, Validation: false, Result: objTopicsResponse, Message: "SUCCESSUPDATE" });
            }
            else {
                return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORUPDATE" });
            }
        }
        catch (e) {
            SysCfg.Log.Methods.WriteLog("ForumsProfiler.js Persistence.Forums.TForumsProfile this.UpdTOPICSRESPONSE", e);
            return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORUPDATE" });
        }
    }
    this.DeleteTOPICSRESPONSEALL = function (queryTopicResponse, objTopicsResponse) {
        var _this = this.TParent();
        var objResponse = { Response: false, Validation: null, Result: null, Message: null };
        try {
            if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponse.IDTOPICSRESPONSE) || objTopicsResponse.IDTOPICSRESPONSE <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDTOPICSRESPONSE" });
            var success = Persistence.Forums.Methods.TQUERYS_DEL(queryTopicResponse);
            if (success.NotError) {
                _this.Fill();
                return (objResponse = { Response: true, Validation: false, Result: result, Message: "SUCCESSDELETE" });
            }
            else {
                return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORDELETE" });
            }
        }
        catch (e) {
            SysCfg.Log.Methods.WriteLog("ForumsProfiler.js Persistence.Forums.TForumsProfile this.DeleteTOPICSRESPONSEALL", e);
            return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORDELETE" });
        }
    }
    this.AddTOPICSRESPONSE = function (objTopicsResponse) {
        var _this = this.TParent();
        var objResponse = { Response: false, Validation: null, Result: null, Message: null };
        try {
            if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponse.IDTOPICSRESPONSE) && objTopicsResponse.IDTOPICSRESPONSE <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDTOPICSRESPONSE" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponse.IDTOPICS) && objTopicsResponse.IDTOPICS <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDTOPICS" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponse.IDCMDBCI) && objTopicsResponse.IDCMDBCI <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDCMDBCI" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponse.IDTOPICSRESPONSE_PARENT) && isNaN(objTopicsResponse.IDTOPICSRESPONSE_PARENT))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDTOPICSRESPONSE_PARENT" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponse.TITLETOPICSRESPONSE))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORTITLETOPICSRESPONSE" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponse.SUBJECTTOPICSRESPONSE))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORSUBJECTTOPICSRESPONSE" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponse.STATUSTOPICSRESPOSE))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORSTATUSTOPICSRESPOSE" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponse.CREATEDDATETOPICSRESPONSE))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORCREATEDDATETOPICSRESPONSE" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponse.LIKETOPICSRESPONSE_COUNT) && isNaN(objTopicsResponse.LIKETOPICSRESPONSE_COUNT))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORLIKETOPICSRESPONSE_COUNT" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponse.NOTLIKETOPICSRESPONSE_COUNT) && isNaN(objTopicsResponse.NOTLIKETOPICSRESPONSE_COUNT))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORNOTLIKETOPICSRESPONSE_COUNT" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponse.ABUSETOPICSRESPONSE_COUNT) && isNaN(objTopicsResponse.ABUSETOPICSRESPONSE_COUNT))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORABUSETOPICSRESPONSE_COUNT" });
            var success = Persistence.Forums.Methods.TOPICSRESPONSE_ADD(objTopicsResponse);
            if (success.NotError) {
                _this.Fill();
                return (objResponse = { Response: true, Validation: false, Result: objTopicsResponse, Message: "SUCCESSADD" });
            }
            else {
                return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORADD" });
            }
        }
        catch (e) {
            SysCfg.Log.Methods.WriteLog("ForumsProfiler.js Persistence.Forums.TForumsProfile this.AddTOPICSRESPONSE", e);
            return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORADD" });
        }
    }
    //********************************************************************************************************************************
    //**********************   CRUD TOPICS RESPONSE VOTES   **************************************************************************
    this.getTOPICSRESPONSEVOTES = function () {
        var _this = this.TParent();
        var newListTopicsResponseVotes =  new Array();
        try {
            Persistence.Forums.Methods.TOPICSRESPONSEVOTES_Fill(newListTopicsResponseVotes, "");
        }
        catch (e) {
            SysCfg.Log.Methods.WriteLog("ForumsProfiler.js Persistence.Forums.TForumsProfile this.getTOPICSRESPONSEVOTES", e);
        }
        return (newListTopicsResponseVotes);
    }
    this.UpdTOPICSRESPONSEVOTES = function (objTopicsResponseVotes) {
        var _this = this.TParent();
        var objResponse = { Response: false, Validation: null, Result: null, Message: null };
        try {
            if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponseVotes.IDTOPICSRESPONSEVOTES) || objTopicsResponseVotes.IDTOPICSRESPONSEVOTES <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDTOPICSRESPONSEVOTES" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponseVotes.IDTOPICSRESPONSE) || objTopicsResponseVotes.IDTOPICSRESPONSE <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDTOPICSRESPONSE" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponseVotes.IDCMDBCI) || objTopicsResponseVotes.IDCMDBCI <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDCMDBCI" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponseVotes.LIKETOPICSRESPONSE))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "LIKETOPICSRESPONSE" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponseVotes.NOTLIKETOPICSRESPONSE))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORNOTLIKETOPICSRESPONSE" });
            var success = Persistence.Forums.Methods.TOPICSRESPONSEVOTES_UPD(objTopicsResponseVotes);
            if (success.NotError) {
                _this.Fill();
                return (objResponse = { Response: true, Validation: false, Result: objTopicsResponseVotes, Message: "SUCCESSUPDATE" });
            }
            else {
                return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORUPDATE" });
            }
        }
        catch (e) {
            SysCfg.Log.Methods.WriteLog("ForumsProfiler.js Persistence.Forums.TForumsProfile this.UpdTOPICSRESPONSEVOTES", e);
            return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORUPDATE" });
        }
    }
    this.DeleteTOPICSRESPONSEVOTESALL = function (objTopicsResponseVotes) {
        var _this = this.TParent();
        var objResponse = { Response: false, Validation: null, Result: null, Message: null };
        try {
            if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponseVotes.IDTOPICSRESPONSEVOTES) || objTopicsResponseVotes.IDTOPICSRESPONSEVOTES <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDTOPICSRESPONSEVOTES" });
            var result = Persistence.Forums.Methods.TOPICSRESPONSEVOTES_ListSetID(_this.getTOPICSRESPONSEVOTES(), objTopicsResponseVotes.IDTOPICSRESPONSEVOTES)
            var success = Persistence.Forums.Methods.TOPICSRESPONSEVOTES_DELTOPICSRESPONSEVOTES(objTopicsResponseVotes);
            if (success.NotError) {
                _this.Fill();
                return (objResponse = { Response: true, Validation: false, Result: result, Message: "SUCCESSDELETE" });
            }
            else {
                return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORDELETE" });
            }
        }
        catch (e) {
            SysCfg.Log.Methods.WriteLog("ForumsProfiler.js Persistence.Forums.TForumsProfile this.DeleteTOPICSRESPONSEVOTESALL", e);
            return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORDELETE" });
        }
    }
    this.UpdTopicsResponseLike = function (objTopicsResponseVotes) {
        var _this = this.TParent();
        var objResponse = { Response: false, Validation: null, Result: null, Message: null };
        try {
            if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponseVotes.IDTOPICSRESPONSEVOTES) || objTopicsResponseVotes.IDTOPICSRESPONSEVOTES < 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDTOPICSRESPONSEVOTES" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponseVotes.IDTOPICSRESPONSE) || objTopicsResponseVotes.IDTOPICSRESPONSE <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDTOPICSRESPONSE" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponseVotes.IDCMDBCI) || objTopicsResponseVotes.IDCMDBCI <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDCMDBCI" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponseVotes.LIKETOPICSRESPONSE))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "LIKETOPICSRESPONSE" });
            var success = Persistence.Forums.Methods.TOPICSRESPONSEVOTES_UPDLIKE(objTopicsResponseVotes);
            if (success.NotError) {
                _this.Fill();
                var newListTopicsResponseVotes = _this.getTOPICSRESPONSEVOTES();
                var result = Persistence.Forums.Methods.TOPICSRESPONSEVOTES_ListSetID(newListTopicsResponseVotes, objTopicsResponseVotes.IDTOPICSRESPONSEVOTES)
                return (objResponse = { Response: true, Validation: false, Result: result, Message: "SUCCESSUPDATE" });
            }
            else {
                return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORUPDATE" });
            }
        }
        catch (e) {
            SysCfg.Log.Methods.WriteLog("ForumsProfiler.js Persistence.Forums.TForumsProfile this.UpdTOPICSRESPONSEVOTES", e);
            return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORUPDATE" });
        }
    }
    this.UpdTopicsResponseNotLike = function (objTopicsResponseVotes) {
        var _this = this.TParent();
        var objResponse = { Response: false, Validation: null, Result: null, Message: null };
        try {
            if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponseVotes.IDTOPICSRESPONSEVOTES) || objTopicsResponseVotes.IDTOPICSRESPONSEVOTES < 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDTOPICSRESPONSEVOTES" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponseVotes.IDTOPICSRESPONSE) || objTopicsResponseVotes.IDTOPICSRESPONSE <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDTOPICSRESPONSE" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponseVotes.IDCMDBCI) || objTopicsResponseVotes.IDCMDBCI <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDCMDBCI" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponseVotes.NOTLIKETOPICSRESPONSE))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORNOTLIKETOPICSRESPONSE" });
            var success = Persistence.Forums.Methods.TOPICSRESPONSEVOTES_UPDNOTLIKE(objTopicsResponseVotes);
            if (success.NotError) {
                _this.Fill();
                var newListTopicsResponseVotes = _this.getTOPICSRESPONSEVOTES();
                var result = Persistence.Forums.Methods.TOPICSRESPONSEVOTES_ListSetID(newListTopicsResponseVotes, objTopicsResponseVotes.IDTOPICSRESPONSEVOTES)
                return (objResponse = { Response: true, Validation: false, Result: result, Message: "SUCCESSUPDATE" });
            }
            else {
                return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORUPDATE" });
            }
        }
        catch (e) {
            SysCfg.Log.Methods.WriteLog("ForumsProfiler.js Persistence.Forums.TForumsProfile this.UpdTOPICSRESPONSEVOTES", e);
            return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORUPDATE" });
        }
    }
    this.AddTOPICSRESPONSEVOTES = function (objTopicsResponseVotes) {
        var _this = this.TParent();
        var objResponse = { Response: false, Validation: null, Result: null, Message: null };
        try {
            if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponseVotes.IDTOPICSRESPONSEVOTES) && objTopicsResponseVotes.IDTOPICSRESPONSEVOTES <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDTOPICSRESPONSEVOTES" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponseVotes.this.IDTOPICSRESPONSE) && objTopicsResponseVotes.this.IDTOPICSRESPONSE <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDTOPICSRESPONSE" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponseVotes.IDCMDBCI) && objTopicsResponseVotes.IDCMDBCI <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDCMDBCI" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponseVotes.LIKETOPICSRESPONSE))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "LIKETOPICSRESPONSE" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponseVotes.NOTLIKETOPICSRESPONSE))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORNOTLIKETOPICSRESPONSE" });
            var success = Persistence.Forums.Methods.TOPICSRESPONSEVOTES_ADD(objTopicsResponseVotes);
            if (success.NotError) {
                _this.Fill();
                return (objResponse = { Response: true, Validation: false, Result: result, Message: "SUCCESSADD" });
            }
            else {
                return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORADD" });
            }
        }
        catch (e) {
            SysCfg.Log.Methods.WriteLog("ForumsProfiler.js Persistence.Forums.TForumsProfile this.AddTOPICSRESPONSEVOTES", e);
            return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORADD" });
        }
    }
    //********************************************************************************************************************************
    //**********************   CRUD TOPICS RESPONSE ABUSE   **************************************************************************
    this.getTOPICSRESPONSEABUSE = function () {
        var _this = this.TParent();
        var newListTopicsResponseAbuse = new Array();
        try {
            Persistence.Forums.Methods.TOPICSRESPONSEABUSE_Fill(newListTopicsResponseAbuse, "");
        }
        catch (e) {
            SysCfg.Log.Methods.WriteLog("ForumsProfiler.js Persistence.Forums.TForumsProfile this.getTOPICSRESPONSEABUSE", e);
        }
        return (newListTopicsResponseAbuse);
    }
    this.UpdTOPICSRESPONSEABUSE = function (objTopicsResponseAbuse) {
        var _this = this.TParent();
        var objResponse = { Response: false, Validation: null, Result: null, Message: null };
        try {
            if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponseAbuse.IDTOPICSRESPONSEABUSE) || objTopicsResponseAbuse.IDTOPICSRESPONSEABUSE <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDTOPICSRESPONSEABUSE" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponseAbuse.IDTOPICSRESPONSE) || objTopicsResponseAbuse.IDTOPICSRESPONSE <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDTOPICSRESPONSE" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponseAbuse.IDCMDBCI) || objTopicsResponseAbuse.IDCMDBCI <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDCMDBCI" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponseAbuse.LIKETOPICSRESPONSEABUSE))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORLIKETOPICSRESPONSEABUSE" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponseAbuse.NOTLIKETOPICSRESPONSEABUSE))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORNOTLIKETOPICSRESPONSEABUSE" });
            var success = Persistence.Forums.Methods.TOPICSRESPONSEABUSE_UPD(objTopicsResponseAbuse);
            if (success.NotError) {
                _this.Fill();
                return (objResponse = { Response: true, Validation: false, Result: objTopicsResponseAbuse, Message: "SUCCESSUPDATE" });
            }
            else {
                return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORUPDATE" });
            }
        }
        catch (e) {
            SysCfg.Log.Methods.WriteLog("ForumsProfiler.js Persistence.Forums.TForumsProfile this.UpdTOPICSRESPONSEABUSE", e);
            return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORUPDATE" });
        }
    }
    this.DeleteTOPICSRESPONSEABUSEALL = function (objTopicsResponseAbuse) {
        var _this = this.TParent();
        var objResponse = { Response: false, Validation: null, Result: null, Message: null };
        try {
            if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponseAbuse.IDTOPICSRESPONSEABUSE) || objTopicsResponseAbuse.IDTOPICSRESPONSEABUSE <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDTOPICSRESPONSEABUSE" });
            var result = Persistence.Forums.Methods.TOPICSRESPONSEABUSE_ListSetID(_this.getTOPICSRESPONSEABUSE(), objTopicsResponseAbuse.IDTOPICSRESPONSEABUSE)
            var success = Persistence.Forums.Methods.TOPICSRESPONSEABUSE_DELTOPICSRESPONSEABUSE(objTopicsResponseAbuse);
            if (success.NotError) {
                _this.Fill();
                return (objResponse = { Response: true, Validation: false, Result: result, Message: "SUCCESSDELETE" });
            }
            else {
                return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORDELETE" });
            }
        }
        catch (e) {
            SysCfg.Log.Methods.WriteLog("ForumsProfiler.js Persistence.Forums.TForumsProfile this.DeleteTOPICSRESPONSEABUSEALL", e);
            return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORDELETE" });
        }
    }
    this.AddTOPICSRESPONSEABUSE = function (objTopicsResponseAbuse) {
        var _this = this.TParent();
        var objResponse = { Response: false, Validation: null, Result: null, Message: null };
        try {
            if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponseAbuse.IDTOPICSRESPONSEABUSE) && objTopicsResponseAbuse.IDTOPICSRESPONSEABUSE <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDTOPICSRESPONSEABUSE" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponseAbuse.this.IDTOPICSRESPONSE) && objTopicsResponseAbuse.this.IDTOPICSRESPONSE <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDTOPICSRESPONSE" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponseAbuse.IDCMDBCI) && objTopicsResponseAbuse.IDCMDBCI <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDCMDBCI" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponseAbuse.LIKETOPICSRESPONSEABUSE))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORLIKETOPICSRESPONSEABUSE" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponseAbuse.NOTLIKETOPICSRESPONSEABUSE))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORNOTLIKETOPICSRESPONSEABUSE" });
            var success = Persistence.Forums.Methods.TOPICSRESPONSEABUSE_ADD(objTopicsResponseAbuse);
            if (success.NotError) {
                return (objResponse = { Response: true, Validation: false, Result: result, Message: "SUCCESSADD" });
            }
            else {
                return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORADD" });
            }
        }
        catch (e) {
            SysCfg.Log.Methods.WriteLog("ForumsProfiler.js Persistence.Forums.TForumsProfile this.AddTOPICSRESPONSEABUSE", e);
            return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORADD" });
        }
    }
    this.UpdTopicsResponseAbuseLike = function (objTopicsResponseAbuse) {
        var _this = this.TParent();
        var objResponse = { Response: false, Validation: null, Result: null, Message: null };
        try {
            if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponseAbuse.IDTOPICSRESPONSEABUSE) || objTopicsResponseAbuse.IDTOPICSRESPONSEABUSE < 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDTOPICSRESPONSEABUSE" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponseAbuse.IDTOPICSRESPONSE) || objTopicsResponseAbuse.IDTOPICSRESPONSE <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDTOPICSRESPONSE" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponseAbuse.IDCMDBCI) || objTopicsResponseAbuse.IDCMDBCI <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDCMDBCI" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objTopicsResponseAbuse.LIKETOPICSRESPONSEABUSE))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORLIKETOPICSRESPONSEABUSE" });
            var success = Persistence.Forums.Methods.TOPICSRESPONSEABUSE_UPDLIKE(objTopicsResponseAbuse);
            if (success.NotError) {
                _this.Fill();
                var newListTopicsResponseAbuse = _this.getTOPICSRESPONSEABUSE();
                var result = Persistence.Forums.Methods.TOPICSRESPONSEABUSE_ListSetID(newListTopicsResponseAbuse, objTopicsResponseAbuse.IDTOPICSRESPONSEABUSE)
                return (objResponse = { Response: true, Validation: false, Result: result, Message: "SUCCESSUPDATE" });
            }
            else {
                return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORUPDATE" });
            }
        }
        catch (e) {
            SysCfg.Log.Methods.WriteLog("ForumsProfiler.js Persistence.Forums.TForumsProfile this.UpdTOPICSRESPONSEABUSE", e);
            return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORUPDATE" });
        }
    }
    //********************************************************************************************************************************
    this.AddFORUMSSEARCH = function (objForumsSearch) {
        var _this = this.TParent();
        var objResponse = { Response: false, Validation: null, Result: null, Message: null };
        try {
            if (SysCfg.Str.Methods.IsNullOrEmpity(objForumsSearch.IDFORUMSSEARCH) && objForumsSearch.IDFORUMSSEARCH <= 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDFORUMSSEARCH" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objForumsSearch.IDFORUMS) && isNaN(objForumsSearch.IDFORUMS))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDFORUMS" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objForumsSearch.NAMEFORUMSSEARCH))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORFORUMSSEARCHNAME" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objForumsSearch.IMAGEFORUMSSEARCH))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORFORUMSSEARCHIMAGE" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objForumsSearch.QUERYFORUMSSEARCH))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORFORUMSSEARCHQUERY" });
            var success = Persistence.Forums.Methods.FORUMSSEARCH_ADD(objTopicsResponseAbuse);
            if (success.NotError) {
                return (objResponse = { Response: true, Validation: false, Result: objForumsSearch, Message: "SUCCESSADD" });
            }
            else {
                return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORADD" });
            }
        }
        catch (e) {
            SysCfg.Log.Methods.WriteLog("ForumsProfiler.js Persistence.Forums.TForumsProfile this.AddFORUMSSEARCH", e);
            return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORADD" });
        }
    }
    this.UpdFORUMSSEARCH = function (objForumsSearch) {
        var _this = this.TParent();
        var objResponse = { Response: false, Validation: null, Result: null, Message: null };
        try {
            if (SysCfg.Str.Methods.IsNullOrEmpity(objForumsSearch.IDFORUMSSEARCH) && isNaN(objForumsSearch.IDFORUMSSEARCH) && objForumsSearch.IDFORUMSSEARCH > 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDFORUMSSEARCH" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objForumsSearch.IDFORUMS) && isNaN(objForumsSearch.IDFORUMS) && objForumsSearch.IDFORUMS > 0)
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDFORUMS" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objForumsSearch.NAMEFORUMSSEARCH))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORFORUMSSEARCHNAME" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objForumsSearch.IMAGEFORUMSSEARCH))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORFORUMSSEARCHIMAGE" });
            else if (SysCfg.Str.Methods.IsNullOrEmpity(objForumsSearch.QUERYFORUMSSEARCH))
                return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORFORUMSSEARCHQUERY" });
            var success = Persistence.Forums.Methods.FORUMSSEARCH_UPD(objTopicsResponseAbuse);
            if (success.NotError) {
                return (objResponse = { Response: true, Validation: false, Result: objForumsSearch, Message: "SUCCESSUPDATE" });
            }
            else {
                return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORUPDATE" });
            }
        }
        catch (e) {
            SysCfg.Log.Methods.WriteLog("ForumsProfiler.js Persistence.Forums.TForumsProfile this.UpdFORUMSSEARCH", e);
            return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORUPDATE" });
        }
    }


}