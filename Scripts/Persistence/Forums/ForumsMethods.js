Persistence.Forums.Methods.StartReation = function (ForumsProfiler, inIDFORUMS){
    try {
        //********************** FORUMS ************************************************************************************************
        var FORUMSListOrder = SysCfg.CopyList(ForumsProfiler.ForumsList).sort(function (a, b) { return a.IDFORUMS - b.IDFORUMS; });
        var FORUMSCATEGORYListOrder = SysCfg.CopyList(ForumsProfiler.ForumsCategoryList).sort(function (a, b) { return a.IDFORUMS - b.IDFORUMS; });
        //var FORUMSCATEGORYListOrder = SysCfg.CopyList(ForumsProfiler.ForumsCategoryList).sort(function (a, b) { return a.IDFORUMSCATEGORY - b.IDFORUMSCATEGORY; });
        var FORUMSSEARCHListOrder = SysCfg.CopyList(ForumsProfiler.ForumsSearchList).sort(function (a, b) { return a.IDFORUMS - b.IDFORUMS; });

        var IDFORUMS = inIDFORUMS;
        var idxFORUMSCATEGORY = 0;
        var idxFORUMSSEARCH = 0;
        for (var i = 0; i < FORUMSListOrder.length; i++) {
            if ((IDFORUMS == -1) || (IDFORUMS == FORUMSListOrder[i].IDFORUMS)) {
                for (var x = idxFORUMSCATEGORY; x < FORUMSCATEGORYListOrder.length; x++) {
                    idxFORUMSCATEGORY = x;
                    if (FORUMSCATEGORYListOrder[x].IDFORUMS == FORUMSListOrder[i].IDFORUMS) {
                        FORUMSListOrder[i].FORUMSCATEGORYList.push(FORUMSCATEGORYListOrder[x]);
                        FORUMSCATEGORYListOrder[x].FORUMS = FORUMSListOrder[i];
                    }
                    if (FORUMSCATEGORYListOrder[x].IDFORUMS > FORUMSListOrder[i].IDFORUMS) {
                        break;
                    }
                }
                for (var x = idxFORUMSSEARCH; x < FORUMSSEARCHListOrder.length; x++) {
                    idxFORUMSSEARCH = x;
                    if (FORUMSSEARCHListOrder[x].IDFORUMS == FORUMSListOrder[i].IDFORUMS) {
                        FORUMSListOrder[i].FORUMSSEARCHList.push(FORUMSSEARCHListOrder[x]);
                        FORUMSSEARCHListOrder[x].FORUMS = FORUMSListOrder[i];
                    }
                    if (FORUMSSEARCHListOrder[x].IDFORUMS > FORUMSListOrder[i].IDFORUMS) {
                        break;
                    }
                }
            }
        }
        var CMDBCIListOrder = SysCfg.CopyList(ForumsProfiler.CmdbCiList).sort(function (a, b) { return a.IDCMDBCI - b.IDCMDBCI; });
        var FORUMSListOrder = SysCfg.CopyList(ForumsProfiler.ForumsList).sort(function (a, b) { return a.IDCMDBCI - b.IDCMDBCI; });
        var idxFORUMS = 0;
        for (var i = 0; i < CMDBCIListOrder.length; i++) {
            for (var x = idxFORUMS; x < FORUMSListOrder.length; x++) {
                idxFORUMS = x;
                if (FORUMSListOrder[x].IDCMDBCI == CMDBCIListOrder[i].IDCMDBCI) {
                    FORUMSListOrder[x].CMDBCI = CMDBCIListOrder[i]; 
                }
                if (FORUMSListOrder[x].IDCMDBCI > CMDBCIListOrder[i].IDCMDBCI) {
                    break;
                }
            }
        }
        //*******************************************************************************************************************************
        //********************** FORUMS CATEGORY ****************************************************************************************
        var FORUMSCATEGORYListOrder = SysCfg.CopyList(ForumsProfiler.ForumsCategoryList).sort(function (a, b) { return a.IDFORUMSCATEGORY - b.IDFORUMSCATEGORY; });
        var TOPICSListOrder = SysCfg.CopyList(ForumsProfiler.TopicsList).sort(function (a, b) { return a.IDFORUMSCATEGORY - b.IDFORUMSCATEGORY; });
        var idxTOPICS = 0;
        for (var i = 0; i < FORUMSCATEGORYListOrder.length; i++) {
            for (var x = idxTOPICS; x < TOPICSListOrder.length; x++) {
                idxTOPICS = x;
                if (TOPICSListOrder[x].IDFORUMSCATEGORY == FORUMSCATEGORYListOrder[i].IDFORUMSCATEGORY) {
                    FORUMSCATEGORYListOrder[i].TOPICSList.push(TOPICSListOrder[x]);
                    TOPICSListOrder[x].FORUMSCATEGORY = FORUMSCATEGORYListOrder[i];
                }
                if (TOPICSListOrder[x].IDFORUMSCATEGORY > FORUMSCATEGORYListOrder[i].IDFORUMSCATEGORY) {
                    break;
                }
            }
        }
        var MDCATEGORYDETAILListOrder = SysCfg.CopyList(ForumsProfiler.MdCategoryDetailList).sort(function (a, b) { return a.IDMDCATEGORYDETAIL - b.IDMDCATEGORYDETAIL; });
        var FORUMSCATEGORYListOrder = SysCfg.CopyList(ForumsProfiler.ForumsCategoryList).sort(function (a, b) { return a.IDMDCATEGORYDETAIL - b.IDMDCATEGORYDETAIL; });
        var idxFORUMSCATEGORY = 0;
        for (var i = 0; i < MDCATEGORYDETAILListOrder.length; i++) {
            for (var x = idxFORUMSCATEGORY; x < FORUMSCATEGORYListOrder.length; x++) {
                idxFORUMSCATEGORY = x;
                if (FORUMSCATEGORYListOrder[x].IDMDCATEGORYDETAIL == MDCATEGORYDETAILListOrder[i].IDMDCATEGORYDETAIL) {
                    FORUMSCATEGORYListOrder[x].MDCATEGORYDETAIL = MDCATEGORYDETAILListOrder[i];
                }
                if (FORUMSCATEGORYListOrder[x].IDMDCATEGORYDETAIL > MDCATEGORYDETAILListOrder[i].IDMDCATEGORYDETAIL) {
                    break;
                }
            }
        }
        //*******************************************************************************************************************************
        //********************** TOPICS *************************************************************************************************
        var TOPICSListOrder = SysCfg.CopyList(ForumsProfiler.TopicsList).sort(function (a, b) { return a.IDTOPICS - b.IDTOPICS; });
        var TOPICSRESPONSEListOrder = SysCfg.CopyList(ForumsProfiler.TopicsResponseList).sort(function (a, b) { return a.IDTOPICS - b.IDTOPICS; });
        var idxTOPICSRESPONSE = 0;
        for (var i = 0; i < TOPICSListOrder.length; i++) {
            for (var x = idxTOPICSRESPONSE; x < TOPICSRESPONSEListOrder.length; x++) {
                idxTOPICSRESPONSE = x;
                if (TOPICSRESPONSEListOrder[x].IDTOPICS == TOPICSListOrder[i].IDTOPICS) {
                    TOPICSListOrder[i].TOPICSRESPONSEList.push(TOPICSRESPONSEListOrder[x]);
                    TOPICSRESPONSEListOrder[x].TOPICS = TOPICSListOrder[i];
                    if (TOPICSRESPONSEListOrder[x].IDTOPICSRESPONSE_PARENT == 0) {
                        TOPICSListOrder[i].TOPICSRESPONSEPARENTList.push(TOPICSRESPONSEListOrder[x]);
                    }
                }
                if (TOPICSRESPONSEListOrder[x].IDTOPICS > TOPICSListOrder[i].IDTOPICS) {
                    break;
                }
            }
        }
        var CMDBCIListOrder = SysCfg.CopyList(ForumsProfiler.CmdbCiList).sort(function (a, b) { return a.IDCMDBCI - b.IDCMDBCI; });
        var TOPICSListOrder = SysCfg.CopyList(ForumsProfiler.TopicsList).sort(function (a, b) { return a.IDCMDBCI - b.IDCMDBCI; });
        var idxTOPICS = 0;
        for (var i = 0; i < CMDBCIListOrder.length; i++) {
            for (var x = idxTOPICS; x < TOPICSListOrder.length; x++) {
                idxTOPICS = x;
                if (TOPICSListOrder[x].IDCMDBCI == CMDBCIListOrder[i].IDCMDBCI) {
                    TOPICSListOrder[x].CMDBCI = CMDBCIListOrder[i];
                }
                if (TOPICSListOrder[x].IDCMDBCI > CMDBCIListOrder[i].IDCMDBCI) {
                    break;
                }
            }
        }
        //*******************************************************************************************************************************
        //********************** TOPICS RESPONSE ****************************************************************************************
        var TOPICSRESPONSEListOrder = SysCfg.CopyList(ForumsProfiler.TopicsResponseList).sort(function (a, b) { return a.IDTOPICSRESPONSE - b.IDTOPICSRESPONSE; });
        var TOPICSRESPONSEPARENTListOrder = SysCfg.CopyList(TOPICSRESPONSEListOrder).sort(function (a, b) { return a.IDTOPICSRESPONSE_PARENT - b.IDTOPICSRESPONSE_PARENT });
        var idxTOPICSRESPONSEPARENT = 0;
        var TOPICSRESPONSEVOTESListOrder = SysCfg.CopyList(ForumsProfiler.TopicsResponseVotesList).sort(function (a, b) { return a.IDTOPICSRESPONSE - b.IDTOPICSRESPONSE });
        var idxTOPICSRESPONSEVOTES = 0;
        var TOPICSRESPONSEABUSEListOrder = SysCfg.CopyList(ForumsProfiler.TopicsResponseAbuseList).sort(function (a, b) { return a.IDTOPICSRESPONSE - b.IDTOPICSRESPONSE });
        var idxTOPICSRESPONSEABUSE = 0;
        for (var i = 0; i < TOPICSRESPONSEListOrder.length; i++) {
            for (var x = idxTOPICSRESPONSEPARENT; x < TOPICSRESPONSEPARENTListOrder.length; x++) {
                idxTOPICSRESPONSEPARENT = x;
                if (TOPICSRESPONSEPARENTListOrder[x].IDTOPICSRESPONSE_PARENT == TOPICSRESPONSEListOrder[i].IDTOPICSRESPONSE) {
                    TOPICSRESPONSEListOrder[i].TOPICSRESPONSEChildList.push(TOPICSRESPONSEPARENTListOrder[x]);
                    TOPICSRESPONSEPARENTListOrder[x].TOPICSRESPONSE_PARENT = TOPICSRESPONSEListOrder[i];
                }
                if (TOPICSRESPONSEPARENTListOrder[x].IDTOPICSRESPONSE_PARENT > TOPICSRESPONSEListOrder[i].IDTOPICSRESPONSE) {
                    break;
                }
            }
            for (var x = idxTOPICSRESPONSEVOTES; x < TOPICSRESPONSEVOTESListOrder.length; x++) {
                idxTOPICSRESPONSEVOTES = x;
                if (TOPICSRESPONSEVOTESListOrder[x].IDTOPICSRESPONSE == TOPICSRESPONSEListOrder[i].IDTOPICSRESPONSE) {
                    TOPICSRESPONSEListOrder[i].TOPICSRESPONSEVOTESList.push(TOPICSRESPONSEVOTESListOrder[x]);
                    TOPICSRESPONSEVOTESListOrder[x].TOPICSRESPONSE = TOPICSRESPONSEListOrder[i];
                }
                if (TOPICSRESPONSEVOTESListOrder[x].IDTOPICSRESPONSE > TOPICSRESPONSEListOrder[i].IDTOPICSRESPONSE) {
                    break;
                }
            }
            for (var x = idxTOPICSRESPONSEABUSE; x < TOPICSRESPONSEABUSEListOrder.length; x++) {
                idxTOPICSRESPONSEABUSE = x;
                if (TOPICSRESPONSEABUSEListOrder[x].IDTOPICSRESPONSE == TOPICSRESPONSEListOrder[i].IDTOPICSRESPONSE) {
                    TOPICSRESPONSEListOrder[i].TOPICSRESPONSEABUSEList.push(TOPICSRESPONSEABUSEListOrder[x]);
                    TOPICSRESPONSEABUSEListOrder[x].TOPICSRESPONSE = TOPICSRESPONSEListOrder[i];
                }
                if (TOPICSRESPONSEABUSEListOrder[x].IDTOPICSRESPONSE > TOPICSRESPONSEListOrder[i].IDTOPICSRESPONSE) {
                    break;
                }
            }
        }
        var CMDBCIListOrder = SysCfg.CopyList(ForumsProfiler.CmdbCiList).sort(function (a, b) { return a.IDCMDBCI - b.IDCMDBCI; });
        var TOPICSRESPONSEListOrder = SysCfg.CopyList(ForumsProfiler.TopicsResponseList).sort(function (a, b) { return a.IDCMDBCI - b.IDCMDBCI });
        var idxTOPICSRESPONSE = 0;
        for (var i = 0; i < CMDBCIListOrder.length; i++) {
            for (var x = idxTOPICSRESPONSE; x < TOPICSRESPONSEListOrder.length; x++) {
                idxTOPICSRESPONSE = x;
                if (TOPICSRESPONSEListOrder[x].IDCMDBCI == CMDBCIListOrder[i].IDCMDBCI) {
                    TOPICSRESPONSEListOrder[x].CMDBCI = CMDBCIListOrder[i];
                }
                if (TOPICSRESPONSEListOrder[x].IDCMDBCI > CMDBCIListOrder[i].IDCMDBCI) {
                    break;
                }
            }
        }
        //*******************************************************************************************************************************
        //********************** TOPICS RESPONSE VOTES **********************************************************************************
        var CMDBCIListOrder = SysCfg.CopyList(ForumsProfiler.CmdbCiList).sort(function (a, b) { return a.IDCMDBCI - b.IDCMDBCI; });
        var TOPICSRESPONSEVOTESListOrder = SysCfg.CopyList(ForumsProfiler.TopicsResponseVotesList).sort(function (a, b) { return a.IDCMDBCI - b.IDCMDBCI });
        var idxTOPICSRESPONSEVOTES = 0;
        for (var i = 0; i < CMDBCIListOrder.length; i++) {
            for (var x = idxTOPICSRESPONSEVOTES; x < TOPICSRESPONSEVOTESListOrder.length; x++) {
                idxTOPICSRESPONSEVOTES = x;
                if (TOPICSRESPONSEVOTESListOrder[x].IDCMDBCI == CMDBCIListOrder[i].IDCMDBCI) {
                    TOPICSRESPONSEVOTESListOrder[x].CMDBCI = CMDBCIListOrder[i];
                }
                if (TOPICSRESPONSEVOTESListOrder[x].IDCMDBCI > CMDBCIListOrder[i].IDCMDBCI) {
                    break;
                }
            }
        }
        //*******************************************************************************************************************************
       //********************** TOPICS RESPONSE ABUSE ***********************************************************************************
        var CMDBCIListOrder = SysCfg.CopyList(ForumsProfiler.CmdbCiList).sort(function (a, b) { return a.IDCMDBCI - b.IDCMDBCI; });
        var TOPICSRESPONSEABUSEListOrder = SysCfg.CopyList(ForumsProfiler.TopicsResponseAbuseList).sort(function (a, b) { return a.IDCMDBCI - b.IDCMDBCI; });
        var idxTOPICSRESPONSEABUSE = 0;
        for (var i = 0; i < CMDBCIListOrder.length; i++) {
            for (var x = idxTOPICSRESPONSEABUSE; x < TOPICSRESPONSEABUSEListOrder.length; x++) {
                idxTOPICSRESPONSEABUSE = x;
                if (TOPICSRESPONSEABUSEListOrder[x].IDCMDBCI == CMDBCIListOrder[i].IDCMDBCI) {
                    TOPICSRESPONSEABUSEListOrder[x].CMDBCI = CMDBCIListOrder[i];
                }
                if (TOPICSRESPONSEABUSEListOrder[x].IDCMDBCI > CMDBCIListOrder[i].IDCMDBCI) {
                    break;
                }
            }
        }
        //*******************************************************************************************************************************
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsMethods.js Persistence.Forums.Methods.StartReation", e);

    }
}