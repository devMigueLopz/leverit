//**********************   PROPERTIES   *****************************************************************************************
Persistence.Forums.Properties.TTOPICSRESPONSE = function () {
	this.IDTOPICSRESPONSE = 0;
	this.IDTOPICS = 0;
	this.IDCMDBCI = 0;
	this.IDTOPICSRESPONSE_PARENT = 0;
	this.TITLETOPICSRESPONSE = "";
	this.SUBJECTTOPICSRESPONSE = "";
	this.STATUSTOPICSRESPOSE = false;
	this.CREATEDDATETOPICSRESPONSE = new Date(1970, 0, 1, 0, 0, 0);
	this.LIKETOPICSRESPONSE_COUNT = 0;
	this.NOTLIKETOPICSRESPONSE_COUNT = 0;
    this.ABUSETOPICSRESPONSE_COUNT = 0;
    //********************** VIRTUALES **********************
    this.TOPICSRESPONSEVOTESList = new Array();
    this.TOPICSRESPONSEABUSEList = new Array();
    this.TOPICSRESPONSEChildList = new Array();
    this.TOPICSRESPONSE_PARENT = new Object();
    this.TOPICS = new Object();
    this.CMDBCI = new Object();
    //********************** END VIRTUALES **********************
	//Socket IO Properties
	this.ToByte = function (MemStream) {
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDTOPICSRESPONSE);
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDTOPICS);
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDCMDBCI);
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDTOPICSRESPONSE_PARENT);
		SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.TITLETOPICSRESPONSE);
		SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.SUBJECTTOPICSRESPONSE);
		SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.STATUSTOPICSRESPOSE);
		SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.CREATEDDATETOPICSRESPONSE);
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.LIKETOPICSRESPONSE_COUNT);
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.NOTLIKETOPICSRESPONSE_COUNT);
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.ABUSETOPICSRESPONSE_COUNT);

	}
	this.ByteTo = function (MemStream) {
		this.IDTOPICSRESPONSE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
		this.IDTOPICS = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
		this.IDCMDBCI = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
		this.IDTOPICSRESPONSE_PARENT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
		this.TITLETOPICSRESPONSE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
		this.SUBJECTTOPICSRESPONSE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
		this.STATUSTOPICSRESPOSE = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
		this.CREATEDDATETOPICSRESPONSE = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
		this.LIKETOPICSRESPONSE_COUNT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
		this.NOTLIKETOPICSRESPONSE_COUNT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
		this.ABUSETOPICSRESPONSE_COUNT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);

	}
	//Str IO Properties
	this.ToStr = function (Longitud, Texto) {
		SysCfg.Str.Protocol.WriteInt(this.IDTOPICSRESPONSE, Longitud, Texto);
		SysCfg.Str.Protocol.WriteInt(this.IDTOPICS, Longitud, Texto);
		SysCfg.Str.Protocol.WriteInt(this.IDCMDBCI, Longitud, Texto);
		SysCfg.Str.Protocol.WriteInt(this.IDTOPICSRESPONSE_PARENT, Longitud, Texto);
		SysCfg.Str.Protocol.WriteStr(this.TITLETOPICSRESPONSE, Longitud, Texto);
		SysCfg.Str.Protocol.WriteStr(this.SUBJECTTOPICSRESPONSE, Longitud, Texto);
		SysCfg.Str.Protocol.WriteBool(this.STATUSTOPICSRESPOSE, Longitud, Texto);
		SysCfg.Str.Protocol.WriteDateTime(this.CREATEDDATETOPICSRESPONSE, Longitud, Texto);
		SysCfg.Str.Protocol.WriteInt(this.LIKETOPICSRESPONSE_COUNT, Longitud, Texto);
		SysCfg.Str.Protocol.WriteInt(this.NOTLIKETOPICSRESPONSE_COUNT, Longitud, Texto);
		SysCfg.Str.Protocol.WriteInt(this.ABUSETOPICSRESPONSE_COUNT, Longitud, Texto);

	}
	this.StrTo = function (Pos, Index, LongitudArray, Texto) {
		this.IDTOPICSRESPONSE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
		this.IDTOPICS = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
		this.IDCMDBCI = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
		this.IDTOPICSRESPONSE_PARENT = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
		this.TITLETOPICSRESPONSE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
		this.SUBJECTTOPICSRESPONSE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
		this.STATUSTOPICSRESPOSE = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);
		this.CREATEDDATETOPICSRESPONSE = SysCfg.Str.Protocol.ReadDateTime(Pos, Index, LongitudArray, Texto);
		this.LIKETOPICSRESPONSE_COUNT = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
		this.NOTLIKETOPICSRESPONSE_COUNT = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
		this.ABUSETOPICSRESPONSE_COUNT = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);

	}
}
//**********************   METHODS server  ********************************************************************************************
Persistence.Forums.Methods.TOPICSRESPONSE_Fill = function (TOPICSRESPONSEList, StrIDTOPICSRESPONSE/*noin IDTOPICSRESPONSE*/) {
	var ResErr = new SysCfg.Error.Properties.TResErr();
	TOPICSRESPONSEList.length = 0;
	var Param = new SysCfg.Stream.Properties.TParam();
	Param.Inicialize();
	if (StrIDTOPICSRESPONSE == "") {
		Param.AddUnknown(UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICSRESPONSE.FieldName, " = " + UsrCfg.InternoAtisNames.TOPICSRESPONSE.NAME_TABLE + "." + UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICSRESPONSE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
	}
	else {
		StrIDTOPICSRESPONSE = " IN (" + StrIDTOPICSRESPONSE + ")";
		Param.AddUnknown(UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICSRESPONSE.FieldName, StrIDTOPICSRESPONSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
	}
	//noin Param.AddInt32(UsrCfg.InternoForumsNames.TOPICSRESPONSE.IDTOPICSRESPONSE.FieldName, IDTOPICSRESPONSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
	try {
		/*
		//********   TOPICSRESPONSE_GET   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"TOPICSRESPONSE_GET", @"TOPICSRESPONSE_GET description.");  
		UnSQL.SQL.Add(@"   SELECT "); 
		 UnSQL.SQL.Add(@"   IDTOPICS, "); 
		 UnSQL.SQL.Add(@"   IDCMDBCI, "); 
		 UnSQL.SQL.Add(@"   IDTOPICSRESPONSE_PARENT, "); 
		 UnSQL.SQL.Add(@"   TITLETOPICSRESPONSE, "); 
		 UnSQL.SQL.Add(@"   SUBJECTTOPICSRESPONSE, "); 
		 UnSQL.SQL.Add(@"   STATUSTOPICSRESPOSE, "); 
		 UnSQL.SQL.Add(@"   CREATEDDATETOPICSRESPONSE, "); 
		 UnSQL.SQL.Add(@"   LIKETOPICSRESPONSE_COUNT, "); 
		 UnSQL.SQL.Add(@"   NOTLIKETOPICSRESPONSE_COUNT, "); 
		 UnSQL.SQL.Add(@"   ABUSETOPICSRESPONSE_COUNT "); 
		 UnSQL.SQL.Add(@"   FROM TOPICSRESPONSE "); 
		 UnConfigSQL.SQL.Add(UnSQL); 
		 
		
		*/
		var DS_TOPICSRESPONSE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "TOPICSRESPONSE_GET", Param.ToBytes());
		ResErr = DS_TOPICSRESPONSE.ResErr;
		if (ResErr.NotError) {
			if (DS_TOPICSRESPONSE.DataSet.RecordCount > 0) {
				DS_TOPICSRESPONSE.DataSet.First();
				while (!(DS_TOPICSRESPONSE.DataSet.Eof)) {
                    var TOPICSRESPONSE = new Persistence.Forums.Properties.TTOPICSRESPONSE();
                    TOPICSRESPONSE.IDTOPICSRESPONSE = DS_TOPICSRESPONSE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICSRESPONSE.FieldName).asInt32();
					TOPICSRESPONSE.IDTOPICS = DS_TOPICSRESPONSE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICS.FieldName).asInt32();
					TOPICSRESPONSE.IDCMDBCI = DS_TOPICSRESPONSE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDCMDBCI.FieldName).asInt32();
					TOPICSRESPONSE.IDTOPICSRESPONSE_PARENT = DS_TOPICSRESPONSE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICSRESPONSE_PARENT.FieldName).asInt32();
					TOPICSRESPONSE.TITLETOPICSRESPONSE = DS_TOPICSRESPONSE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSE.TITLETOPICSRESPONSE.FieldName).asString();
					TOPICSRESPONSE.SUBJECTTOPICSRESPONSE = DS_TOPICSRESPONSE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSE.SUBJECTTOPICSRESPONSE.FieldName).asText();
					TOPICSRESPONSE.STATUSTOPICSRESPOSE = DS_TOPICSRESPONSE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSE.STATUSTOPICSRESPOSE.FieldName).asBoolean();
					TOPICSRESPONSE.CREATEDDATETOPICSRESPONSE = DS_TOPICSRESPONSE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSE.CREATEDDATETOPICSRESPONSE.FieldName).asDateTime();
					TOPICSRESPONSE.LIKETOPICSRESPONSE_COUNT = DS_TOPICSRESPONSE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSE.LIKETOPICSRESPONSE_COUNT.FieldName).asInt32();
					TOPICSRESPONSE.NOTLIKETOPICSRESPONSE_COUNT = DS_TOPICSRESPONSE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSE.NOTLIKETOPICSRESPONSE_COUNT.FieldName).asInt32();
					TOPICSRESPONSE.ABUSETOPICSRESPONSE_COUNT = DS_TOPICSRESPONSE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSE.ABUSETOPICSRESPONSE_COUNT.FieldName).asInt32();

					//Other
					TOPICSRESPONSEList.push(TOPICSRESPONSE);
					DS_TOPICSRESPONSE.DataSet.Next();
				}
			}
			else {
				DS_TOPICSRESPONSE.ResErr.NotError = false;
				DS_TOPICSRESPONSE.ResErr.Mesaje = "RECORDCOUNT = 0";
			}
		}
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_TOPICSRESPONSE.js Persistence.Forums.Methods.TOPICSRESPONSE_GET", e);
    }
	finally {
		Param.Destroy();
	}
	return (ResErr);
}

Persistence.Forums.Methods.TOPICSRESPONSE_GETID = function (TOPICSRESPONSE) {
	var ResErr = new SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try {
		Param.Inicialize();
		//Other
		Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICSRESPONSE.FieldName, TOPICSRESPONSE.IDTOPICSRESPONSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICS.FieldName, TOPICSRESPONSE.IDTOPICS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDCMDBCI.FieldName, TOPICSRESPONSE.IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICSRESPONSE_PARENT.FieldName, TOPICSRESPONSE.IDTOPICSRESPONSE_PARENT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddString(UsrCfg.InternoAtisNames.TOPICSRESPONSE.TITLETOPICSRESPONSE.FieldName, TOPICSRESPONSE.TITLETOPICSRESPONSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddText(UsrCfg.InternoAtisNames.TOPICSRESPONSE.SUBJECTTOPICSRESPONSE.FieldName, TOPICSRESPONSE.SUBJECTTOPICSRESPONSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddBoolean(UsrCfg.InternoAtisNames.TOPICSRESPONSE.STATUSTOPICSRESPOSE.FieldName, TOPICSRESPONSE.STATUSTOPICSRESPOSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddDateTime(UsrCfg.InternoAtisNames.TOPICSRESPONSE.CREATEDDATETOPICSRESPONSE.FieldName, TOPICSRESPONSE.CREATEDDATETOPICSRESPONSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSE.LIKETOPICSRESPONSE_COUNT.FieldName, TOPICSRESPONSE.LIKETOPICSRESPONSE_COUNT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSE.NOTLIKETOPICSRESPONSE_COUNT.FieldName, TOPICSRESPONSE.NOTLIKETOPICSRESPONSE_COUNT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSE.ABUSETOPICSRESPONSE_COUNT.FieldName, TOPICSRESPONSE.ABUSETOPICSRESPONSE_COUNT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

		/*
		//********   TOPICSRESPONSE_GETID   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"TOPICSRESPONSE_GETID", @"TOPICSRESPONSE_GETID description.");  
		UnSQL.SQL.Add(@"   SELECT "); 
		 UnSQL.SQL.Add(@"   IDTOPICSRESPONSE "); 
		 UnSQL.SQL.Add(@"   FROM TOPICSRESPONSE "); 
		 UnSQL.SQL.Add(@"   Where "); 
		 UnSQL.SQL.Add(@"   IDTOPICS=@[IDTOPICS] And "); 
		 UnSQL.SQL.Add(@"   IDCMDBCI=@[IDCMDBCI] And "); 
		 UnSQL.SQL.Add(@"   IDTOPICSRESPONSE_PARENT=@[IDTOPICSRESPONSE_PARENT] And "); 
		 UnSQL.SQL.Add(@"   TITLETOPICSRESPONSE=@[TITLETOPICSRESPONSE] And "); 
		 UnSQL.SQL.Add(@"   STATUSTOPICSRESPOSE=@[STATUSTOPICSRESPOSE] And "); 
		 UnSQL.SQL.Add(@"   CREATEDDATETOPICSRESPONSE=@[CREATEDDATETOPICSRESPONSE] And "); 
		 UnSQL.SQL.Add(@"   LIKETOPICSRESPONSE_COUNT=@[LIKETOPICSRESPONSE_COUNT] And "); 
		 UnSQL.SQL.Add(@"   NOTLIKETOPICSRESPONSE_COUNT=@[NOTLIKETOPICSRESPONSE_COUNT] And "); 
		 UnSQL.SQL.Add(@"   ABUSETOPICSRESPONSE_COUNT=@[ABUSETOPICSRESPONSE_COUNT] ");
		 UnConfigSQL.SQL.Add(UnSQL); 
		 
		
		*/
		var DS_TOPICSRESPONSE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "TOPICSRESPONSE_GETID", Param.ToBytes());
		ResErr = DS_TOPICSRESPONSE.ResErr;
		if (ResErr.NotError) {
			if (DS_TOPICSRESPONSE.DataSet.RecordCount > 0) {
				DS_TOPICSRESPONSE.DataSet.First();
				TOPICSRESPONSE.IDTOPICSRESPONSE = DS_TOPICSRESPONSE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICSRESPONSE.FieldName).asInt32();
			}
			else {
				DS_TOPICSRESPONSE.ResErr.NotError = false;
				DS_TOPICSRESPONSE.ResErr.Mesaje = "RECORDCOUNT = 0";
			}
		}
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_TOPICSRESPONSE.js Persistence.Forums.Methods.TOPICSRESPONSE_GETID", e);
    }
	finally {
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.Forums.Methods.TOPICSRESPONSE_GETLAST = function (id,page,enumPage) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    var objResult = null;
    try {
        Param.Inicialize();
        //Other
        switch (page) {
            case enumPage.Index:
                Param.AddInt32(UsrCfg.InternoAtisNames.FORUMS.IDFORUMS.FieldName, id, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
                var DS_TOPICSRESPONSE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "TOPICSRESPONSE_GETLASTBYFORUM", Param.ToBytes());
                break;
            case enumPage.ForumCategory:
                Param.AddInt32(UsrCfg.InternoAtisNames.FORUMSCATEGORY.IDFORUMSCATEGORY.FieldName, id, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
                var DS_TOPICSRESPONSE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "TOPICSRESPONSE_GETLASTBYFORUMCATEGORY", Param.ToBytes());
                break;
            case enumPage.Topic:
                Param.AddInt32(UsrCfg.InternoAtisNames.TOPICS.IDTOPICS.FieldName, id, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
                var DS_TOPICSRESPONSE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "TOPICSRESPONSE_GETLASTBYTOPIC", Param.ToBytes());
                break;
        }
        ResErr = DS_TOPICSRESPONSE.ResErr;
        if (ResErr.NotError) {
            if (DS_TOPICSRESPONSE.DataSet.RecordCount > 0) {
                DS_TOPICSRESPONSE.DataSet.First();
                while (!(DS_TOPICSRESPONSE.DataSet.Eof)) {
                    var TOPICSRESPONSE = new Persistence.Forums.Properties.TTOPICSRESPONSE();
                    TOPICSRESPONSE.IDTOPICSRESPONSE = DS_TOPICSRESPONSE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICSRESPONSE.FieldName).asInt32();
                    TOPICSRESPONSE.IDTOPICS = DS_TOPICSRESPONSE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICS.FieldName).asInt32();
                    TOPICSRESPONSE.IDCMDBCI = DS_TOPICSRESPONSE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDCMDBCI.FieldName).asInt32();
                    TOPICSRESPONSE.IDTOPICSRESPONSE_PARENT = DS_TOPICSRESPONSE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICSRESPONSE_PARENT.FieldName).asInt32();
                    TOPICSRESPONSE.TITLETOPICSRESPONSE = DS_TOPICSRESPONSE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSE.TITLETOPICSRESPONSE.FieldName).asString();
                    TOPICSRESPONSE.SUBJECTTOPICSRESPONSE = DS_TOPICSRESPONSE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSE.SUBJECTTOPICSRESPONSE.FieldName).asText();
                    TOPICSRESPONSE.STATUSTOPICSRESPOSE = DS_TOPICSRESPONSE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSE.STATUSTOPICSRESPOSE.FieldName).asBoolean();
                    TOPICSRESPONSE.CREATEDDATETOPICSRESPONSE = DS_TOPICSRESPONSE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSE.CREATEDDATETOPICSRESPONSE.FieldName).asDateTime();
                    TOPICSRESPONSE.LIKETOPICSRESPONSE_COUNT = DS_TOPICSRESPONSE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSE.LIKETOPICSRESPONSE_COUNT.FieldName).asInt32();
                    TOPICSRESPONSE.NOTLIKETOPICSRESPONSE_COUNT = DS_TOPICSRESPONSE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSE.NOTLIKETOPICSRESPONSE_COUNT.FieldName).asInt32();
                    TOPICSRESPONSE.ABUSETOPICSRESPONSE_COUNT = DS_TOPICSRESPONSE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSE.ABUSETOPICSRESPONSE_COUNT.FieldName).asInt32();
                    objResult = TOPICSRESPONSE;
                    DS_TOPICSRESPONSE.DataSet.Next();
                }
            }
            else {
                DS_TOPICSRESPONSE.ResErr.NotError = false;
                DS_TOPICSRESPONSE.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_TOPICSRESPONSE.js Persistence.Forums.Methods.TOPICSRESPONSE_GETLAST", e);
    }
    finally {
        Param.Destroy();
    }
    return (objResult);
}

Persistence.Forums.Methods.TOPICSRESPONSE_ADD = function (TOPICSRESPONSE) {
	var ResErr = new SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try {
		Param.Inicialize();
		//Other
		Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICSRESPONSE.FieldName, TOPICSRESPONSE.IDTOPICSRESPONSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICS.FieldName, TOPICSRESPONSE.IDTOPICS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDCMDBCI.FieldName, TOPICSRESPONSE.IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICSRESPONSE_PARENT.FieldName, TOPICSRESPONSE.IDTOPICSRESPONSE_PARENT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.TOPICSRESPONSE.TITLETOPICSRESPONSE.FieldName, TOPICSRESPONSE.TITLETOPICSRESPONSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddText(UsrCfg.InternoAtisNames.TOPICSRESPONSE.SUBJECTTOPICSRESPONSE.FieldName, TOPICSRESPONSE.SUBJECTTOPICSRESPONSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddBoolean(UsrCfg.InternoAtisNames.TOPICSRESPONSE.STATUSTOPICSRESPOSE.FieldName, TOPICSRESPONSE.STATUSTOPICSRESPOSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddDateTime(UsrCfg.InternoAtisNames.TOPICSRESPONSE.CREATEDDATETOPICSRESPONSE.FieldName, TOPICSRESPONSE.CREATEDDATETOPICSRESPONSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSE.LIKETOPICSRESPONSE_COUNT.FieldName, TOPICSRESPONSE.LIKETOPICSRESPONSE_COUNT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSE.NOTLIKETOPICSRESPONSE_COUNT.FieldName, TOPICSRESPONSE.NOTLIKETOPICSRESPONSE_COUNT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSE.ABUSETOPICSRESPONSE_COUNT.FieldName, TOPICSRESPONSE.ABUSETOPICSRESPONSE_COUNT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

		/* 
		//********   TOPICSRESPONSE_ADD   ************************* 
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"TOPICSRESPONSE_ADD", @"TOPICSRESPONSE_ADD description."); 
		UnSQL.SQL.Add(@"   INSERT INTO TOPICSRESPONSE( "); 
		 UnSQL.SQL.Add(@"   IDTOPICS, "); 
		 UnSQL.SQL.Add(@"   IDCMDBCI, "); 
		 UnSQL.SQL.Add(@"   IDTOPICSRESPONSE_PARENT, "); 
		 UnSQL.SQL.Add(@"   TITLETOPICSRESPONSE, "); 
		 UnSQL.SQL.Add(@"   SUBJECTTOPICSRESPONSE, "); 
		 UnSQL.SQL.Add(@"   STATUSTOPICSRESPOSE, "); 
		 UnSQL.SQL.Add(@"   CREATEDDATETOPICSRESPONSE, "); 
		 UnSQL.SQL.Add(@"   LIKETOPICSRESPONSE_COUNT, "); 
		 UnSQL.SQL.Add(@"   NOTLIKETOPICSRESPONSE_COUNT, "); 
		 UnSQL.SQL.Add(@"   ABUSETOPICSRESPONSE_COUNT "); 
		 UnSQL.SQL.Add(@"   ) Values ( ");  
		UnSQL.SQL.Add(@"   @[IDTOPICS], "); 
		 UnSQL.SQL.Add(@"   @[IDCMDBCI], "); 
		 UnSQL.SQL.Add(@"   @[IDTOPICSRESPONSE_PARENT], "); 
		 UnSQL.SQL.Add(@"   @[TITLETOPICSRESPONSE], "); 
		 UnSQL.SQL.Add(@"   @[SUBJECTTOPICSRESPONSE], "); 
		 UnSQL.SQL.Add(@"   @[STATUSTOPICSRESPOSE], "); 
		 UnSQL.SQL.Add(@"   @[CREATEDDATETOPICSRESPONSE], "); 
		 UnSQL.SQL.Add(@"   @[LIKETOPICSRESPONSE_COUNT], "); 
		 UnSQL.SQL.Add(@"   @[NOTLIKETOPICSRESPONSE_COUNT], "); 
		 UnSQL.SQL.Add(@"   @[ABUSETOPICSRESPONSE_COUNT] "); 
		 UnSQL.SQL.Add(@"   )  "); 
		UnConfigSQL.SQL.Add(UnSQL); 
		 
		
		*/
		this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "TOPICSRESPONSE_ADD", Param.ToBytes());
		ResErr = Exec.ResErr;
		if (ResErr.NotError) {
			Persistence.Forums.Methods.TOPICSRESPONSE_GETID(TOPICSRESPONSE);
		}
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_TOPICSRESPONSE.js Persistence.Forums.Methods.TOPICSRESPONSE_ADD", e);
    }
	finally {
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.Forums.Methods.TOPICSRESPONSE_UPD = function (TOPICSRESPONSE) {
	var ResErr = SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try {
		Param.Inicialize();
		Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICSRESPONSE.FieldName, TOPICSRESPONSE.IDTOPICSRESPONSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICS.FieldName, TOPICSRESPONSE.IDTOPICS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDCMDBCI.FieldName, TOPICSRESPONSE.IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICSRESPONSE_PARENT.FieldName, TOPICSRESPONSE.IDTOPICSRESPONSE_PARENT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.TOPICSRESPONSE.TITLETOPICSRESPONSE.FieldName, TOPICSRESPONSE.TITLETOPICSRESPONSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddText(UsrCfg.InternoAtisNames.TOPICSRESPONSE.SUBJECTTOPICSRESPONSE.FieldName, TOPICSRESPONSE.SUBJECTTOPICSRESPONSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddBoolean(UsrCfg.InternoAtisNames.TOPICSRESPONSE.STATUSTOPICSRESPOSE.FieldName, TOPICSRESPONSE.STATUSTOPICSRESPOSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddDateTime(UsrCfg.InternoAtisNames.TOPICSRESPONSE.CREATEDDATETOPICSRESPONSE.FieldName, TOPICSRESPONSE.CREATEDDATETOPICSRESPONSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSE.LIKETOPICSRESPONSE_COUNT.FieldName, TOPICSRESPONSE.LIKETOPICSRESPONSE_COUNT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSE.NOTLIKETOPICSRESPONSE_COUNT.FieldName, TOPICSRESPONSE.NOTLIKETOPICSRESPONSE_COUNT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSE.ABUSETOPICSRESPONSE_COUNT.FieldName, TOPICSRESPONSE.ABUSETOPICSRESPONSE_COUNT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);


		/*   
		//********   TOPICSRESPONSE_UPD   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"TOPICSRESPONSE_UPD", @"TOPICSRESPONSE_UPD description.");  
		UnSQL.SQL.Add(@"  UPDATE TOPICSRESPONSE Set "); 
		 UnSQL.SQL.Add(@"   IDTOPICSRESPONSE=@[IDTOPICSRESPONSE], "); 
		 UnSQL.SQL.Add(@"   IDTOPICS=@[IDTOPICS], "); 
		 UnSQL.SQL.Add(@"   IDCMDBCI=@[IDCMDBCI], "); 
		 UnSQL.SQL.Add(@"   IDTOPICSRESPONSE_PARENT=@[IDTOPICSRESPONSE_PARENT], "); 
		 UnSQL.SQL.Add(@"   TITLETOPICSRESPONSE=@[TITLETOPICSRESPONSE], "); 
		 UnSQL.SQL.Add(@"   SUBJECTTOPICSRESPONSE=@[SUBJECTTOPICSRESPONSE], "); 
		 UnSQL.SQL.Add(@"   STATUSTOPICSRESPOSE=@[STATUSTOPICSRESPOSE], "); 
		 UnSQL.SQL.Add(@"   CREATEDDATETOPICSRESPONSE=@[CREATEDDATETOPICSRESPONSE], "); 
		 UnSQL.SQL.Add(@"   LIKETOPICSRESPONSE_COUNT=@[LIKETOPICSRESPONSE_COUNT], "); 
		 UnSQL.SQL.Add(@"   NOTLIKETOPICSRESPONSE_COUNT=@[NOTLIKETOPICSRESPONSE_COUNT], "); 
		 UnSQL.SQL.Add(@"   ABUSETOPICSRESPONSE_COUNT=@[ABUSETOPICSRESPONSE_COUNT] "); 
		 UnSQL.SQL.Add(@"   Where "); 
		 UnSQL.SQL.Add(@"   IDTOPICSRESPONSE=@[IDTOPICSRESPONSE] ");
		 UnConfigSQL.SQL.Add(UnSQL); 
		 
		
		*/
		var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "TOPICSRESPONSE_UPD", Param.ToBytes());
		ResErr = Exec.ResErr;
		if (ResErr.NotError) {
			//GET(); 
		}
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_TOPICSRESPONSE.js Persistence.Forums.Methods.TOPICSRESPONSE_UPD", e);
    }
	finally {
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.Forums.Methods.TOPICSRESPONSE_DEL = function (/*String StrIDTOPICSRESPONSE*/IDTOPICSRESPONSE) {
	var Res = -1;
	if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
		Res = Persistence.Forums.Methods.TOPICSRESPONSE_DELIDTOPICSRESPONSE(/*String StrIDTOPICSRESPONSE*/Param);
	}
	else {
		Res = Persistence.Forums.Methods.TOPICSRESPONSE_DELTOPICSRESPONSE(Param);
	}
	return (Res);
}
Persistence.Forums.Methods.TOPICSRESPONSE_DELIDTOPICSRESPONSE = function (/*String StrIDTOPICSRESPONSE*/IDTOPICSRESPONSE) {
	var ResErr = SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try {
		Param.Inicialize();
		/*if (StrIDTOPICSRESPONSE == "")
		{
		Param.AddUnknown(UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICSRESPONSE.FieldName, " = " + UsrCfg.InternoAtisNames.TOPICSRESPONSE.NAME_TABLE + "." + UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICSRESPONSE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
		}
		else
		{
		StrIDTOPICSRESPONSE = " IN (" + StrIDTOPICSRESPONSE + ")";
		Param.AddUnknown(UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICSRESPONSE.FieldName, StrIDTOPICSRESPONSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		}*/
		Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICSRESPONSE.FieldName, IDTOPICSRESPONSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		/*
		//********   TOPICSRESPONSE_DEL   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"TOPICSRESPONSE_DEL", @"TOPICSRESPONSE_DEL description.");  
		UnSQL.SQL.Add(@"   DELETE TOPICSRESPONSE "); 
		 UnSQL.SQL.Add(@"   Where "); 
		 UnSQL.SQL.Add(@"   IDTOPICSRESPONSE=@[IDTOPICSRESPONSE]   ");
		 UnConfigSQL.SQL.Add(UnSQL); 
		 
		
		*/
		var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "TOPICSRESPONSE_DEL", Param.ToBytes());
		ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_TOPICSRESPONSE.js Persistence.Forums.Methods.TOPICSRESPONSE_DELIDTOPICSRESPONSE", e);
    }
	finally {
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.Forums.Methods.TOPICSRESPONSE_DELTOPICSRESPONSE = function (TOPICSRESPONSE/*TOPICSRESPONSEList*/) {
	var ResErr = SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try {
		/*
		StrIDTOPICSRESPONSE = "";
		StrIDOTRO = "";
		for (var i = 0; i < TOPICSRESPONSEList.length; i++) {
		StrIDTOPICSRESPONSE += TOPICSRESPONSEList[i].IDTOPICSRESPONSE + ",";
		StrIDOTRO += TOPICSRESPONSEList[i].IDOTRO + ",";
		}
		StrIDTOPICSRESPONSE = StrIDTOPICSRESPONSE.substring(0, StrIDTOPICSRESPONSE.length - 1);
		StrIDOTRO = StrIDOTRO.substring(0, StrIDOTRO.length - 1);
		
		
		if (StrIDTOPICSRESPONSE == "")
		{
		Param.AddUnknown(UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICSRESPONSE.FieldName, " = " + UsrCfg.InternoAtisNames.TOPICSRESPONSE.NAME_TABLE + "." + UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICSRESPONSE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
		}
		else
		{
		StrIDTOPICSRESPONSE = " IN (" + StrIDTOPICSRESPONSE + ")";
		Param.AddUnknown(UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICSRESPONSE.FieldName, StrIDTOPICSRESPONSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		}*/


		Param.Inicialize();
		Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICSRESPONSE.FieldName, TOPICSRESPONSE.IDTOPICSRESPONSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		/*    
		//********   TOPICSRESPONSE_DEL   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"TOPICSRESPONSE_DEL", @"TOPICSRESPONSE_DEL description.");  
		UnSQL.SQL.Add(@"   DELETE TOPICSRESPONSE "); 
		 UnSQL.SQL.Add(@"   Where "); 
		 UnSQL.SQL.Add(@"   IDTOPICSRESPONSE=@[IDTOPICSRESPONSE]   ");
		 UnConfigSQL.SQL.Add(UnSQL); 
		 
		
		*/
		var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "TOPICSRESPONSE_DEL", Param.ToBytes());
		ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_TOPICSRESPONSE.js Persistence.Forums.Methods.TOPICSRESPONSE_DELTOPICSRESPONSE", e);
    }
	finally {
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.Forums.Methods.TOPICSRESPONSE_DEL_ALL = function (TOPICSRESPONSE/*TOPICSRESPONSEList*/) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICSRESPONSE.FieldName, TOPICSRESPONSE.IDTOPICSRESPONSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "TOPICSRESPONSE_DEL_ALL", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_TOPICSRESPONSE.js Persistence.Forums.Methods.TOPICSRESPONSE_DELTOPICSRESPONSE", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
//**********************   METHODS   ********************************************************************************************
Persistence.Forums.Methods.TOPICSRESPONSE_ListSetID = function (TOPICSRESPONSEList, IDTOPICSRESPONSE) {
	for (i = 0; i < TOPICSRESPONSEList.length; i++) {
		if (IDTOPICSRESPONSE == TOPICSRESPONSEList[i].IDTOPICSRESPONSE)
			return (TOPICSRESPONSEList[i]);
	}
    var UnTOPICSRESPONSE = new Persistence.Forums.Properties.TTOPICSRESPONSE();
	UnTOPICSRESPONSE.IDTOPICSRESPONSE = IDTOPICSRESPONSE;
	return (UnTOPICSRESPONSE);
}
Persistence.Forums.Methods.TOPICSRESPONSE_ListAdd = function (TOPICSRESPONSEList, TOPICSRESPONSE) {
	var i = Persistence.Forums.Methods.TOPICSRESPONSE_ListGetIndex(TOPICSRESPONSEList, TOPICSRESPONSE);
	if (i == -1) TOPICSRESPONSEList.push(TOPICSRESPONSE);
	else {
		TOPICSRESPONSEList[i].IDTOPICSRESPONSE = TOPICSRESPONSE.IDTOPICSRESPONSE;
		TOPICSRESPONSEList[i].IDTOPICS = TOPICSRESPONSE.IDTOPICS;
		TOPICSRESPONSEList[i].IDCMDBCI = TOPICSRESPONSE.IDCMDBCI;
		TOPICSRESPONSEList[i].IDTOPICSRESPONSE_PARENT = TOPICSRESPONSE.IDTOPICSRESPONSE_PARENT;
		TOPICSRESPONSEList[i].TITLETOPICSRESPONSE = TOPICSRESPONSE.TITLETOPICSRESPONSE;
		TOPICSRESPONSEList[i].SUBJECTTOPICSRESPONSE = TOPICSRESPONSE.SUBJECTTOPICSRESPONSE;
		TOPICSRESPONSEList[i].STATUSTOPICSRESPOSE = TOPICSRESPONSE.STATUSTOPICSRESPOSE;
		TOPICSRESPONSEList[i].CREATEDDATETOPICSRESPONSE = TOPICSRESPONSE.CREATEDDATETOPICSRESPONSE;
		TOPICSRESPONSEList[i].LIKETOPICSRESPONSE_COUNT = TOPICSRESPONSE.LIKETOPICSRESPONSE_COUNT;
		TOPICSRESPONSEList[i].NOTLIKETOPICSRESPONSE_COUNT = TOPICSRESPONSE.NOTLIKETOPICSRESPONSE_COUNT;
		TOPICSRESPONSEList[i].ABUSETOPICSRESPONSE_COUNT = TOPICSRESPONSE.ABUSETOPICSRESPONSE_COUNT;

	}
}
Persistence.Forums.Methods.TOPICSRESPONSE_ListGetIndex = function (TOPICSRESPONSEList, Param) {
	var Res = -1;
	if (typeof (Param) == 'number') {
		Res = Persistence.Forums.Methods.TOPICSRESPONSE_ListGetIndexIDTOPICSRESPONSE(TOPICSRESPONSEList, Param);
	}
	else {
		Res = Persistence.Forums.Methods.TOPICSRESPONSE_ListGetIndexTOPICSRESPONSE(TOPICSRESPONSEList, Param);
	}
	return (Res);
}
Persistence.Forums.Methods.TOPICSRESPONSE_ListGetIndexTOPICSRESPONSE = function (TOPICSRESPONSEList, TOPICSRESPONSE) {
	for (i = 0; i < TOPICSRESPONSEList.length; i++) {
		if (TOPICSRESPONSE.IDTOPICSRESPONSE == TOPICSRESPONSEList[i].IDTOPICSRESPONSE)
			return (i);
	}
	return (-1);
}
Persistence.Forums.Methods.TOPICSRESPONSE_ListGetIndexIDTOPICSRESPONSE = function (TOPICSRESPONSEList, IDTOPICSRESPONSE) {
	for (i = 0; i < TOPICSRESPONSEList.length; i++) {
		if (IDTOPICSRESPONSE == TOPICSRESPONSEList[i].IDTOPICSRESPONSE)
			return (i);
	}
	return (-1);
}
//STRING FUNCTION 
Persistence.Forums.Methods.TOPICSRESPONSEListtoStr = function (TOPICSRESPONSEList) {
	var Res = Persistence.Properties._Version + "(1)0";
	var Longitud = new SysCfg.ref("");
	var Texto = new SysCfg.ref("");
	try {
		SysCfg.Str.Protocol.WriteInt(TOPICSRESPONSEList.length, Longitud, Texto);
		for (Counter = 0; Counter < TOPICSRESPONSEList.length; Counter++) {
			var UnTOPICSRESPONSE = TOPICSRESPONSEList[Counter];
			UnTOPICSRESPONSE.ToStr(Longitud, Texto);
		}

		Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
		Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
	}
	catch (e) {
		var ResErr = new SysCfg.Error.Properties.TResErr();
		ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
        SysCfg.Log.Methods.WriteLog("DB_TOPICSRESPONSE.js Persistence.Forums.Methods.TOPICSRESPONSEListtoStr", e);
	}
	return Res;
}
Persistence.Forums.Methods.StrtoTOPICSRESPONSE = function (ProtocoloStr, TOPICSRESPONSEList) {

	var Res = false;
	var Longitud, Texto;
	var Index = new SysCfg.ref(0);
	var Pos = new SysCfg.ref(0);
	try {
		TOPICSRESPONSEList.length = 0;
		if (Persistence.Forums.Properties._Version == ProtocoloStr.substring(0, 3)) {
			Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
			Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.Length);
			LongitudArray = Longitud.split(",");
			LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
			for (i = 0; i < LSCount; i++) {
				UnTOPICSRESPONSE = new Persistence.Properties.TTOPICSRESPONSE(); //Mode new row
				UnTOPICSRESPONSE.StrTo(Pos, Index, LongitudArray, Texto);
				Persistence.Forums.TOPICSRESPONSE_ListAdd(TOPICSRESPONSEList, UnTOPICSRESPONSE);
			}
		}
	}
	catch (e) {
		var ResErr = SysCfg.Error.Properties.TResErr;
		ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
        SysCfg.Log.Methods.WriteLog("DB_TOPICSRESPONSE.js Persistence.Forums.Methods.StrtoTOPICSRESPONSE", e);

	}
	return (Res);
}
//SOCKET FUNCTION 
Persistence.Forums.Methods.TOPICSRESPONSEListToByte = function (TOPICSRESPONSEList, MemStream, idx) {
	SysCfg.Stream.Methods.WriteStreamInt32(MemStream, TOPICSRESPONSEList.Count - idx);
	for (i = idx; i < TOPICSRESPONSEList.Count; i++) {
		TOPICSRESPONSEList[i].ToByte(MemStream);
	}
}
Persistence.Forums.Methods.ByteToTOPICSRESPONSEList = function (TOPICSRESPONSEList, MemStream) {
	Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
	for (i = 0; i < Count; i++) {
		UnTOPICSRESPONSE = new Persistence.Properties.TTOPICSRESPONSE();
		UnTOPICSRESPONSE.ByteTo(MemStream);
		Methods.TOPICSRESPONSE_ListAdd(TOPICSRESPONSEList, UnTOPICSRESPONSE);
	}
}
