//**********************   PROPERTIES   *****************************************************************************************
Persistence.Forums.Properties.TTOPICSRESPONSEABUSE = function () {
    this.IDTOPICSRESPONSEABUSE = 0;
    this.IDTOPICSRESPONSE = 0;
    this.IDCMDBCI = 0;
    this.LIKETOPICSRESPONSEABUSE = false;
    this.NOTLIKETOPICSRESPONSEABUSE = false;
    //********************** VIRTUALES **********************
    this.TOPICSRESPONSE = new Object();
    this.CMDBCI = new Object();
    //********************** END VIRTUALES **********************
    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDTOPICSRESPONSEABUSE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDTOPICSRESPONSE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDCMDBCI);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.LIKETOPICSRESPONSEABUSE);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.NOTLIKETOPICSRESPONSEABUSE);

    }
    this.ByteTo = function (MemStream) {
        this.IDTOPICSRESPONSEABUSE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDTOPICSRESPONSE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDCMDBCI = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.LIKETOPICSRESPONSEABUSE = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.NOTLIKETOPICSRESPONSEABUSE = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);

    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDTOPICSRESPONSEABUSE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDTOPICSRESPONSE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDCMDBCI, Longitud, Texto);
        SysCfg.Str.Protocol.WriteBool(this.LIKETOPICSRESPONSEABUSE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteBool(this.NOTLIKETOPICSRESPONSEABUSE, Longitud, Texto);

    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDTOPICSRESPONSEABUSE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDTOPICSRESPONSE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDCMDBCI = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.LIKETOPICSRESPONSEABUSE = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);
        this.NOTLIKETOPICSRESPONSEABUSE = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);

    }
}
//**********************   METHODS server  ********************************************************************************************
Persistence.Forums.Methods.TOPICSRESPONSEABUSE_Fill = function (TOPICSRESPONSEABUSEList, StrIDTOPICSRESPONSEABUSE/*noin IDTOPICSRESPONSEABUSE*/) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    TOPICSRESPONSEABUSEList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDTOPICSRESPONSEABUSE == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDTOPICSRESPONSEABUSE.FieldName, " = " + UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.NAME_TABLE + "." + UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDTOPICSRESPONSEABUSE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDTOPICSRESPONSEABUSE = " IN (" + StrIDTOPICSRESPONSEABUSE + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDTOPICSRESPONSEABUSE.FieldName, StrIDTOPICSRESPONSEABUSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //noin Param.AddInt32(UsrCfg.InternoForumsNames.TOPICSRESPONSEABUSE.IDTOPICSRESPONSEABUSE.FieldName, IDTOPICSRESPONSEABUSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {
        /*
        //********   TOPICSRESPONSEABUSE_GET   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"TOPICSRESPONSEABUSE_GET", @"TOPICSRESPONSEABUSE_GET description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   IDTOPICSRESPONSEABUSE, "); 
         UnSQL.SQL.Add(@"   IDTOPICSRESPONSE, "); 
         UnSQL.SQL.Add(@"   IDCMDBCI, "); 
         UnSQL.SQL.Add(@"   LIKETOPICSRESPONSEABUSE, "); 
         UnSQL.SQL.Add(@"   NOTLIKETOPICSRESPONSEABUSE "); 
         UnSQL.SQL.Add(@"   FROM TOPICSRESPONSEABUSE "); 
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_TOPICSRESPONSEABUSE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "TOPICSRESPONSEABUSE_GET", Param.ToBytes());
        ResErr = DS_TOPICSRESPONSEABUSE.ResErr;
        if (ResErr.NotError) {
            if (DS_TOPICSRESPONSEABUSE.DataSet.RecordCount > 0) {
                DS_TOPICSRESPONSEABUSE.DataSet.First();
                while (!(DS_TOPICSRESPONSEABUSE.DataSet.Eof)) {
                    var TOPICSRESPONSEABUSE = new Persistence.Forums.Properties.TTOPICSRESPONSEABUSE();
                    TOPICSRESPONSEABUSE.IDTOPICSRESPONSEABUSE = DS_TOPICSRESPONSEABUSE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDTOPICSRESPONSEABUSE.FieldName).asInt32();
                    TOPICSRESPONSEABUSE.IDTOPICSRESPONSE = DS_TOPICSRESPONSEABUSE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDTOPICSRESPONSE.FieldName).asInt32();
                    TOPICSRESPONSEABUSE.IDCMDBCI = DS_TOPICSRESPONSEABUSE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDCMDBCI.FieldName).asInt32();
                    TOPICSRESPONSEABUSE.LIKETOPICSRESPONSEABUSE = DS_TOPICSRESPONSEABUSE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.LIKETOPICSRESPONSEABUSE.FieldName).asBoolean();
                    TOPICSRESPONSEABUSE.NOTLIKETOPICSRESPONSEABUSE = DS_TOPICSRESPONSEABUSE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.NOTLIKETOPICSRESPONSEABUSE.FieldName).asBoolean();

                    //Other
                    TOPICSRESPONSEABUSEList.push(TOPICSRESPONSEABUSE);
                    DS_TOPICSRESPONSEABUSE.DataSet.Next();
                }
            }
            else {
                DS_TOPICSRESPONSEABUSE.ResErr.NotError = false;
                DS_TOPICSRESPONSEABUSE.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_TOPICSRESPONSEABUSE.js Persistence.Forums.Methods.TOPICSRESPONSEABUSE_Fill", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Forums.Methods.TOPICSRESPONSEABUSE_GETID = function (TOPICSRESPONSEABUSE) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDTOPICSRESPONSEABUSE.FieldName, TOPICSRESPONSEABUSE.IDTOPICSRESPONSEABUSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDTOPICSRESPONSE.FieldName, TOPICSRESPONSEABUSE.IDTOPICSRESPONSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDCMDBCI.FieldName, TOPICSRESPONSEABUSE.IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddBoolean(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.LIKETOPICSRESPONSEABUSE.FieldName, TOPICSRESPONSEABUSE.LIKETOPICSRESPONSEABUSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddBoolean(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.NOTLIKETOPICSRESPONSEABUSE.FieldName, TOPICSRESPONSEABUSE.NOTLIKETOPICSRESPONSEABUSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

        /*
        //********   TOPICSRESPONSEABUSE_GETID   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"TOPICSRESPONSEABUSE_GETID", @"TOPICSRESPONSEABUSE_GETID description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   IDTOPICSRESPONSEABUSE "); 
         UnSQL.SQL.Add(@"   FROM TOPICSRESPONSEABUSE "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDTOPICSRESPONSE=@[IDTOPICSRESPONSE] And "); 
         UnSQL.SQL.Add(@"   IDCMDBCI=@[IDCMDBCI] And "); 
         UnSQL.SQL.Add(@"   LIKETOPICSRESPONSEABUSE=@[LIKETOPICSRESPONSEABUSE] And "); 
         UnSQL.SQL.Add(@"   NOTLIKETOPICSRESPONSEABUSE=@[NOTLIKETOPICSRESPONSEABUSE] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_TOPICSRESPONSEABUSE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "TOPICSRESPONSEABUSE_GETID", Param.ToBytes());
        ResErr = DS_TOPICSRESPONSEABUSE.ResErr;
        if (ResErr.NotError) {
            if (DS_TOPICSRESPONSEABUSE.DataSet.RecordCount > 0) {
                DS_TOPICSRESPONSEABUSE.DataSet.First();
                TOPICSRESPONSEABUSE.IDTOPICSRESPONSEABUSE = DS_TOPICSRESPONSEABUSE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDTOPICSRESPONSEABUSE.FieldName).asInt32();
            }
            else {
                DS_TOPICSRESPONSEABUSE.ResErr.NotError = false;
                DS_TOPICSRESPONSEABUSE.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_TOPICSRESPONSEABUSE.js Persistence.Forums.Methods.TOPICSRESPONSEABUSE_GETID", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

Persistence.Forums.Methods.TOPICSRESPONSEABUSE_GETABUSEANDNOTABUSE = function (userId, TopicId) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    var objResult = new Array();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICS.FieldName, TopicId, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDCMDBCI.FieldName, userId, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        var DS_TOPICSRESPONSEABUSE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "TOPICSRESPONSEABUSE_GETABUSEANDNOTABUSE", Param.ToBytes());
        ResErr = DS_TOPICSRESPONSEABUSE.ResErr;
        if (ResErr.NotError) {
            if (DS_TOPICSRESPONSEABUSE.DataSet.RecordCount > 0) {
                DS_TOPICSRESPONSEABUSE.DataSet.First();
                while (!(DS_TOPICSRESPONSEABUSE.DataSet.Eof)) {
                    var TOPICSRESPONSEABUSE = new Persistence.Forums.Properties.TTOPICSRESPONSEABUSE();
                    TOPICSRESPONSEABUSE.IDTOPICSRESPONSEABUSE = DS_TOPICSRESPONSEABUSE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDTOPICSRESPONSEABUSE.FieldName).asInt32();
                    TOPICSRESPONSEABUSE.IDTOPICSRESPONSE = DS_TOPICSRESPONSEABUSE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDTOPICSRESPONSE.FieldName).asInt32();
                    TOPICSRESPONSEABUSE.IDCMDBCI = DS_TOPICSRESPONSEABUSE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDCMDBCI.FieldName).asInt32();
                    TOPICSRESPONSEABUSE.LIKETOPICSRESPONSEABUSE = DS_TOPICSRESPONSEABUSE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.LIKETOPICSRESPONSEABUSE.FieldName).asBoolean();
                    TOPICSRESPONSEABUSE.NOTLIKETOPICSRESPONSEABUSE = DS_TOPICSRESPONSEABUSE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.NOTLIKETOPICSRESPONSEABUSE.FieldName).asBoolean();

                    //Other
                    objResult.push(TOPICSRESPONSEABUSE);
                    DS_TOPICSRESPONSEABUSE.DataSet.Next();
                }
            }
            else {
                DS_TOPICSRESPONSEABUSE.ResErr.NotError = false;
                DS_TOPICSRESPONSEABUSE.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_TOPICSRESPONSE.js Persistence.Forums.Methods.TOPICSRESPONSEABUSE_GETABUSEANDNOTABUSE", e);
    }
    finally {
        Param.Destroy();
    }
    return (objResult);
}


Persistence.Forums.Methods.TOPICSRESPONSEABUSE_ADD = function (TOPICSRESPONSEABUSE) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDTOPICSRESPONSEABUSE.FieldName, TOPICSRESPONSEABUSE.IDTOPICSRESPONSEABUSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDTOPICSRESPONSE.FieldName, TOPICSRESPONSEABUSE.IDTOPICSRESPONSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDCMDBCI.FieldName, TOPICSRESPONSEABUSE.IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.LIKETOPICSRESPONSEABUSE.FieldName, TOPICSRESPONSEABUSE.LIKETOPICSRESPONSEABUSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.NOTLIKETOPICSRESPONSEABUSE.FieldName, TOPICSRESPONSEABUSE.NOTLIKETOPICSRESPONSEABUSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        /* 
        //********   TOPICSRESPONSEABUSE_ADD   ************************* TOPICSRESPONSEABUSE_ADD
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"TOPICSRESPONSEABUSE_ADD", @"TOPICSRESPONSEABUSE_ADD description."); 
        UnSQL.SQL.Add(@"   INSERT INTO TOPICSRESPONSEABUSE( "); 
         UnSQL.SQL.Add(@"   IDTOPICSRESPONSE, "); 
         UnSQL.SQL.Add(@"   IDCMDBCI, "); 
         UnSQL.SQL.Add(@"   LIKETOPICSRESPONSEABUSE, "); 
         UnSQL.SQL.Add(@"   NOTLIKETOPICSRESPONSEABUSE "); 
         UnSQL.SQL.Add(@"   ) Values ( ");  
        UnSQL.SQL.Add(@"   @[IDTOPICSRESPONSE], "); 
         UnSQL.SQL.Add(@"   @[IDCMDBCI], "); 
         UnSQL.SQL.Add(@"   @[LIKETOPICSRESPONSEABUSE], "); 
         UnSQL.SQL.Add(@"   @[NOTLIKETOPICSRESPONSEABUSE] "); 
         UnSQL.SQL.Add(@"   )  "); 
        UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "TOPICSRESPONSEABUSE_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.Forums.Methods.TOPICSRESPONSEABUSE_GETID(TOPICSRESPONSEABUSE);
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_TOPICSRESPONSEABUSE.js Persistence.Forums.Methods.TOPICSRESPONSEABUSE_ADD", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Forums.Methods.TOPICSRESPONSEABUSE_UPD = function (TOPICSRESPONSEABUSE) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDTOPICSRESPONSEABUSE.FieldName, TOPICSRESPONSEABUSE.IDTOPICSRESPONSEABUSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDTOPICSRESPONSE.FieldName, TOPICSRESPONSEABUSE.IDTOPICSRESPONSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDCMDBCI.FieldName, TOPICSRESPONSEABUSE.IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.LIKETOPICSRESPONSEABUSE.FieldName, TOPICSRESPONSEABUSE.LIKETOPICSRESPONSEABUSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.NOTLIKETOPICSRESPONSEABUSE.FieldName, TOPICSRESPONSEABUSE.NOTLIKETOPICSRESPONSEABUSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);


        /*   
        //********   TOPICSRESPONSEABUSE_UPD   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"TOPICSRESPONSEABUSE_UPD", @"TOPICSRESPONSEABUSE_UPD description.");  
        UnSQL.SQL.Add(@"  UPDATE TOPICSRESPONSEABUSE Set "); 
         UnSQL.SQL.Add(@"   IDTOPICSRESPONSE=@[IDTOPICSRESPONSE], "); 
         UnSQL.SQL.Add(@"   IDCMDBCI=@[IDCMDBCI], "); 
         UnSQL.SQL.Add(@"   LIKETOPICSRESPONSEABUSE=@[LIKETOPICSRESPONSEABUSE], "); 
         UnSQL.SQL.Add(@"   NOTLIKETOPICSRESPONSEABUSE=@[NOTLIKETOPICSRESPONSEABUSE] "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDTOPICSRESPONSEABUSE=@[IDTOPICSRESPONSEABUSE] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "TOPICSRESPONSEABUSE_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_TOPICSRESPONSEABUSE.js Persistence.Forums.Methods.TOPICSRESPONSEABUSE_UPD", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

Persistence.Forums.Methods.TOPICSRESPONSEABUSE_UPDLIKE = function (TOPICSRESPONSEABUSE) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDTOPICSRESPONSEABUSE.FieldName, TOPICSRESPONSEABUSE.IDTOPICSRESPONSEABUSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDTOPICSRESPONSE.FieldName, TOPICSRESPONSEABUSE.IDTOPICSRESPONSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDCMDBCI.FieldName, TOPICSRESPONSEABUSE.IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.LIKETOPICSRESPONSEABUSE.FieldName, TOPICSRESPONSEABUSE.LIKETOPICSRESPONSEABUSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "TOPICSRESPONSEABUSE_UPDLIKE", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_TOPICSRESPONSEABUSE.js Persistence.Forums.Methods.TOPICSRESPONSEABUSE_UPDLIKE", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

Persistence.Forums.Methods.TOPICSRESPONSEABUSE_DEL = function (/*String StrIDTOPICSRESPONSEABUSE*/IDTOPICSRESPONSEABUSE) {
    var Res = -1;
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.Forums.Methods.TOPICSRESPONSEABUSE_DELIDTOPICSRESPONSEABUSE(/*String StrIDTOPICSRESPONSEABUSE*/Param);
    }
    else {
        Res = Persistence.Forums.Methods.TOPICSRESPONSEABUSE_DELTOPICSRESPONSEABUSE(Param);
    }
    return (Res);
}
Persistence.Forums.Methods.TOPICSRESPONSEABUSE_DELIDTOPICSRESPONSEABUSE = function (/*String StrIDTOPICSRESPONSEABUSE*/IDTOPICSRESPONSEABUSE) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDTOPICSRESPONSEABUSE == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDTOPICSRESPONSEABUSE.FieldName, " = " + UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.NAME_TABLE + "." + UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDTOPICSRESPONSEABUSE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDTOPICSRESPONSEABUSE = " IN (" + StrIDTOPICSRESPONSEABUSE + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDTOPICSRESPONSEABUSE.FieldName, StrIDTOPICSRESPONSEABUSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDTOPICSRESPONSEABUSE.FieldName, IDTOPICSRESPONSEABUSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   TOPICSRESPONSEABUSE_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"TOPICSRESPONSEABUSE_DEL", @"TOPICSRESPONSEABUSE_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE TOPICSRESPONSEABUSE "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDTOPICSRESPONSEABUSE=@[IDTOPICSRESPONSEABUSE]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "TOPICSRESPONSEABUSE_DEL", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_TOPICSRESPONSEABUSE.js Persistence.Forums.Methods.TOPICSRESPONSEABUSE_DELIDTOPICSRESPONSEABUSE", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Forums.Methods.TOPICSRESPONSEABUSE_DELTOPICSRESPONSEABUSE = function (TOPICSRESPONSEABUSE/*TOPICSRESPONSEABUSEList*/) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        /*
        StrIDTOPICSRESPONSEABUSE = "";
        StrIDOTRO = "";
        for (var i = 0; i < TOPICSRESPONSEABUSEList.length; i++) {
        StrIDTOPICSRESPONSEABUSE += TOPICSRESPONSEABUSEList[i].IDTOPICSRESPONSEABUSE + ",";
        StrIDOTRO += TOPICSRESPONSEABUSEList[i].IDOTRO + ",";
        }
        StrIDTOPICSRESPONSEABUSE = StrIDTOPICSRESPONSEABUSE.substring(0, StrIDTOPICSRESPONSEABUSE.length - 1);
        StrIDOTRO = StrIDOTRO.substring(0, StrIDOTRO.length - 1);
        
        
        if (StrIDTOPICSRESPONSEABUSE == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDTOPICSRESPONSEABUSE.FieldName, " = " + UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.NAME_TABLE + "." + UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDTOPICSRESPONSEABUSE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDTOPICSRESPONSEABUSE = " IN (" + StrIDTOPICSRESPONSEABUSE + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDTOPICSRESPONSEABUSE.FieldName, StrIDTOPICSRESPONSEABUSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/


        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDTOPICSRESPONSEABUSE.FieldName, TOPICSRESPONSEABUSE.IDTOPICSRESPONSEABUSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*    
        //********   TOPICSRESPONSEABUSE_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"TOPICSRESPONSEABUSE_DEL", @"TOPICSRESPONSEABUSE_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE TOPICSRESPONSEABUSE "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDTOPICSRESPONSEABUSE=@[IDTOPICSRESPONSEABUSE]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "TOPICSRESPONSEABUSE_DEL", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_TOPICSRESPONSEABUSE.js Persistence.Forums.Methods.TOPICSRESPONSEABUSE_DELTOPICSRESPONSEABUSE", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
//**********************   METHODS   ********************************************************************************************
Persistence.Forums.Methods.TOPICSRESPONSEABUSE_ListSetID = function (TOPICSRESPONSEABUSEList, IDTOPICSRESPONSEABUSE) {
    for (i = 0; i < TOPICSRESPONSEABUSEList.length; i++) {
        if (IDTOPICSRESPONSEABUSE == TOPICSRESPONSEABUSEList[i].IDTOPICSRESPONSEABUSE)
            return (TOPICSRESPONSEABUSEList[i]);
    }
    var UnTOPICSRESPONSEABUSE = new Persistence.Forums.Properties.TTOPICSRESPONSEABUSE;
    UnTOPICSRESPONSEABUSE.IDTOPICSRESPONSEABUSE = IDTOPICSRESPONSEABUSE;
    return (UnTOPICSRESPONSEABUSE);
}
Persistence.Forums.Methods.TOPICSRESPONSEABUSE_ListAdd = function (TOPICSRESPONSEABUSEList, TOPICSRESPONSEABUSE) {
    var i = Persistence.Forums.Methods.TOPICSRESPONSEABUSE_ListGetIndex(TOPICSRESPONSEABUSEList, TOPICSRESPONSEABUSE);
    if (i == -1) TOPICSRESPONSEABUSEList.push(TOPICSRESPONSEABUSE);
    else {
        TOPICSRESPONSEABUSEList[i].IDTOPICSRESPONSEABUSE = TOPICSRESPONSEABUSE.IDTOPICSRESPONSEABUSE;
        TOPICSRESPONSEABUSEList[i].IDTOPICSRESPONSE = TOPICSRESPONSEABUSE.IDTOPICSRESPONSE;
        TOPICSRESPONSEABUSEList[i].IDCMDBCI = TOPICSRESPONSEABUSE.IDCMDBCI;
        TOPICSRESPONSEABUSEList[i].LIKETOPICSRESPONSEABUSE = TOPICSRESPONSEABUSE.LIKETOPICSRESPONSEABUSE;
        TOPICSRESPONSEABUSEList[i].NOTLIKETOPICSRESPONSEABUSE = TOPICSRESPONSEABUSE.NOTLIKETOPICSRESPONSEABUSE;

    }
}
Persistence.Forums.Methods.TOPICSRESPONSEABUSE_ListGetIndex = function (TOPICSRESPONSEABUSEList, Param) {
    var Res = -1;
    if (typeof (Param) == 'number') {
        Res = Persistence.Forums.Methods.TOPICSRESPONSEABUSE_ListGetIndexIDTOPICSRESPONSEABUSE(TOPICSRESPONSEABUSEList, Param);
    }
    else {
        Res = Persistence.Forums.Methods.TOPICSRESPONSEABUSE_ListGetIndexTOPICSRESPONSEABUSE(TOPICSRESPONSEABUSEList, Param);
    }
    return (Res);
}
Persistence.Forums.Methods.TOPICSRESPONSEABUSE_ListGetIndexTOPICSRESPONSEABUSE = function (TOPICSRESPONSEABUSEList, TOPICSRESPONSEABUSE) {
    for (i = 0; i < TOPICSRESPONSEABUSEList.length; i++) {
        if (TOPICSRESPONSEABUSE.IDTOPICSRESPONSEABUSE == TOPICSRESPONSEABUSEList[i].IDTOPICSRESPONSEABUSE)
            return (i);
    }
    return (-1);
}
Persistence.Forums.Methods.TOPICSRESPONSEABUSE_ListGetIndexIDTOPICSRESPONSEABUSE = function (TOPICSRESPONSEABUSEList, IDTOPICSRESPONSEABUSE) {
    for (i = 0; i < TOPICSRESPONSEABUSEList.length; i++) {
        if (IDTOPICSRESPONSEABUSE == TOPICSRESPONSEABUSEList[i].IDTOPICSRESPONSEABUSE)
            return (i);
    }
    return (-1);
}
//STRING FUNCTION 
Persistence.Forums.Methods.TOPICSRESPONSEABUSEListtoStr = function (TOPICSRESPONSEABUSEList) {
    var Res = Persistence.Properties._Version + "(1)0";
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(TOPICSRESPONSEABUSEList.length, Longitud, Texto);
        for (Counter = 0; Counter < TOPICSRESPONSEABUSEList.length; Counter++) {
            var UnTOPICSRESPONSEABUSE = TOPICSRESPONSEABUSEList[Counter];
            UnTOPICSRESPONSEABUSE.ToStr(Longitud, Texto);
        }

        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
        SysCfg.Log.Methods.WriteLog("DB_TOPICSRESPONSEABUSE.js Persistence.Forums.Methods.TOPICSRESPONSEABUSEListtoStr", e);
    }
    return Res;
}
Persistence.Forums.Methods.StrtoTOPICSRESPONSEABUSE = function (ProtocoloStr, TOPICSRESPONSEABUSEList) {

    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        TOPICSRESPONSEABUSEList.length = 0;
        if (Persistence.Forums.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.Length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnTOPICSRESPONSEABUSE = new Persistence.Properties.TTOPICSRESPONSEABUSE(); //Mode new row
                UnTOPICSRESPONSEABUSE.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Forums.TOPICSRESPONSEABUSE_ListAdd(TOPICSRESPONSEABUSEList, UnTOPICSRESPONSEABUSE);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
        SysCfg.Log.Methods.WriteLog("DB_TOPICSRESPONSEABUSE.js Persistence.Forums.Methods.StrtoTOPICSRESPONSEABUSE", e);
    }
    return (Res);
}
//SOCKET FUNCTION 
Persistence.Forums.Methods.TOPICSRESPONSEABUSEListToByte = function (TOPICSRESPONSEABUSEList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, TOPICSRESPONSEABUSEList.Count - idx);
    for (i = idx; i < TOPICSRESPONSEABUSEList.Count; i++) {
        TOPICSRESPONSEABUSEList[i].ToByte(MemStream);
    }
}
Persistence.Forums.Methods.ByteToTOPICSRESPONSEABUSEList = function (TOPICSRESPONSEABUSEList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnTOPICSRESPONSEABUSE = new Persistence.Properties.TTOPICSRESPONSEABUSE();
        UnTOPICSRESPONSEABUSE.ByteTo(MemStream);
        Methods.TOPICSRESPONSEABUSE_ListAdd(TOPICSRESPONSEABUSEList, UnTOPICSRESPONSEABUSE);
    }
}
