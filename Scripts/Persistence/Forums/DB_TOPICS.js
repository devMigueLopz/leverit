//**********************   PROPERTIES   *****************************************************************************************
Persistence.Forums.Properties.TTOPICS = function () {
    this.IDTOPICS = 0;
    this.IDFORUMSCATEGORY = 0;
    this.IDCMDBCI = 0;
    this.NAMETOPICS = "";
    this.DESCRIPTIONTOPICS = "";
    this.RESPONSETOPICS_COUNT = 0;
    this.VIEWSTOPICS_COUNT = 0;
    this.IMAGETOPICS = "";
    this.STATUSTOPICS = false;
    this.CREATEDDATETOPICS = new Date(1970, 0, 1, 0, 0, 0);
    this.KEYWORDSTOPICS = "";
     //********************** VIRTUALES **********************
    this.TOPICSRESPONSEList = new Array();
    this.TOPICSRESPONSEPARENTList = new Array();
    this.FORUMSCATEGORY = new Object();
    this.CMDBCI = new Object();
    //********************** END VIRTUALES **********************
    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDTOPICS);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDFORUMSCATEGORY);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDCMDBCI);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.NAMETOPICS);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.DESCRIPTIONTOPICS);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.RESPONSETOPICS_COUNT);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.VIEWSTOPICS_COUNT);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.IMAGETOPICS);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.STATUSTOPICS);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.CREATEDDATETOPICS);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.KEYWORDSTOPICS);

    }
    this.ByteTo = function (MemStream) {
        this.IDTOPICS = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDFORUMSCATEGORY = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDCMDBCI = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.NAMETOPICS = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.DESCRIPTIONTOPICS = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.RESPONSETOPICS_COUNT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.VIEWSTOPICS_COUNT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IMAGETOPICS = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.STATUSTOPICS = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.CREATEDDATETOPICS = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.KEYWORDSTOPICS = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);

    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDTOPICS, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDFORUMSCATEGORY, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDCMDBCI, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.NAMETOPICS, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.DESCRIPTIONTOPICS, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.RESPONSETOPICS_COUNT, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.VIEWSTOPICS_COUNT, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.IMAGETOPICS, Longitud, Texto);
        SysCfg.Str.Protocol.WriteBool(this.STATUSTOPICS, Longitud, Texto);
        SysCfg.Str.Protocol.WriteDateTime(this.CREATEDDATETOPICS, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.KEYWORDSTOPICS, Longitud, Texto);

    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDTOPICS = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDFORUMSCATEGORY = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDCMDBCI = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.NAMETOPICS = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.DESCRIPTIONTOPICS = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.RESPONSETOPICS_COUNT = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.VIEWSTOPICS_COUNT = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IMAGETOPICS = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.STATUSTOPICS = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);
        this.CREATEDDATETOPICS = SysCfg.Str.Protocol.ReadDateTime(Pos, Index, LongitudArray, Texto);
        this.KEYWORDSTOPICS = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);

    }
}
//**********************   METHODS server  ********************************************************************************************
Persistence.Forums.Methods.TOPICS_Fill = function (TOPICSList, StrIDTOPICS/*noin IDTOPICS*/) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    TOPICSList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDTOPICS == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.TOPICS.IDTOPICS.FieldName, " = " + UsrCfg.InternoAtisNames.TOPICS.NAME_TABLE + "." + UsrCfg.InternoAtisNames.TOPICS.IDTOPICS.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDTOPICS = " IN (" + StrIDTOPICS + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.TOPICS.IDTOPICS.FieldName, StrIDTOPICS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //noin Param.AddInt32(UsrCfg.InternoForumsNames.TOPICS.IDTOPICS.FieldName, IDTOPICS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {
        /*
        //********   TOPICS_GET   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"TOPICS_GET", @"TOPICS_GET description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   IDTOPICS, "); 
         UnSQL.SQL.Add(@"   IDFORUMSCATEGORY, "); 
         UnSQL.SQL.Add(@"   IDCMDBCI, "); 
         UnSQL.SQL.Add(@"   NAMETOPICS, "); 
         UnSQL.SQL.Add(@"   DESCRIPTIONTOPICS, "); 
         UnSQL.SQL.Add(@"   RESPONSETOPICS_COUNT, "); 
         UnSQL.SQL.Add(@"   VIEWSTOPICS_COUNT, "); 
         UnSQL.SQL.Add(@"   IMAGETOPICS, "); 
         UnSQL.SQL.Add(@"   STATUSTOPICS, "); 
         UnSQL.SQL.Add(@"   CREATEDDATETOPICS, "); 
         UnSQL.SQL.Add(@"   KEYWORDSTOPICS "); 
         UnSQL.SQL.Add(@"   FROM TOPICS "); 
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_TOPICS = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "TOPICS_GET", Param.ToBytes());
        ResErr = DS_TOPICS.ResErr;
        if (ResErr.NotError) {
            if (DS_TOPICS.DataSet.RecordCount > 0) {
                DS_TOPICS.DataSet.First();
                while (!(DS_TOPICS.DataSet.Eof)) {
                    var TOPICS = new Persistence.Forums.Properties.TTOPICS();
                    TOPICS.IDTOPICS = DS_TOPICS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICS.IDTOPICS.FieldName).asInt32();
                    TOPICS.IDFORUMSCATEGORY = DS_TOPICS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICS.IDFORUMSCATEGORY.FieldName).asInt32();
                    TOPICS.IDCMDBCI = DS_TOPICS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICS.IDCMDBCI.FieldName).asInt32();
                    TOPICS.NAMETOPICS = DS_TOPICS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICS.NAMETOPICS.FieldName).asString();
                    TOPICS.DESCRIPTIONTOPICS = DS_TOPICS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICS.DESCRIPTIONTOPICS.FieldName).asText();
                    TOPICS.RESPONSETOPICS_COUNT = DS_TOPICS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICS.RESPONSETOPICS_COUNT.FieldName).asInt32();
                    TOPICS.VIEWSTOPICS_COUNT = DS_TOPICS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICS.VIEWSTOPICS_COUNT.FieldName).asInt32();
                    TOPICS.IMAGETOPICS = DS_TOPICS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICS.IMAGETOPICS.FieldName).asString();
                    TOPICS.STATUSTOPICS = DS_TOPICS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICS.STATUSTOPICS.FieldName).asBoolean();
                    TOPICS.CREATEDDATETOPICS = DS_TOPICS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICS.CREATEDDATETOPICS.FieldName).asDateTime();
                    TOPICS.KEYWORDSTOPICS = DS_TOPICS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICS.KEYWORDSTOPICS.FieldName).asString();

                    //Other
                    TOPICSList.push(TOPICS);
                    DS_TOPICS.DataSet.Next();
                }
            }
            else {
                DS_TOPICS.ResErr.NotError = false;
                DS_TOPICS.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_TOPICS.js Persistence.Forums.Methods.TOPICS_Fill", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Forums.Methods.TOPICS_GETID = function (TOPICS) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICS.IDTOPICS.FieldName, TOPICS.IDTOPICS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICS.IDFORUMSCATEGORY.FieldName, TOPICS.IDFORUMSCATEGORY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICS.IDCMDBCI.FieldName, TOPICS.IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.TOPICS.NAMETOPICS.FieldName, TOPICS.NAMETOPICS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddText(UsrCfg.InternoAtisNames.TOPICS.DESCRIPTIONTOPICS.FieldName, TOPICS.DESCRIPTIONTOPICS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICS.RESPONSETOPICS_COUNT.FieldName, TOPICS.RESPONSETOPICS_COUNT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICS.VIEWSTOPICS_COUNT.FieldName, TOPICS.VIEWSTOPICS_COUNT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.TOPICS.IMAGETOPICS.FieldName, TOPICS.IMAGETOPICS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddBoolean(UsrCfg.InternoAtisNames.TOPICS.STATUSTOPICS.FieldName, TOPICS.STATUSTOPICS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddDateTime(UsrCfg.InternoAtisNames.TOPICS.CREATEDDATETOPICS.FieldName, TOPICS.CREATEDDATETOPICS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.TOPICS.KEYWORDSTOPICS.FieldName, TOPICS.KEYWORDSTOPICS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

        /*
        //********   TOPICS_GETID   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"TOPICS_GETID", @"TOPICS_GETID description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   IDTOPICS "); 
         UnSQL.SQL.Add(@"   FROM TOPICS "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDFORUMSCATEGORY=@[IDFORUMSCATEGORY] And "); 
         UnSQL.SQL.Add(@"   IDCMDBCI=@[IDCMDBCI] And "); 
         UnSQL.SQL.Add(@"   NAMETOPICS=@[NAMETOPICS] And "); 
         UnSQL.SQL.Add(@"   RESPONSETOPICS_COUNT=@[RESPONSETOPICS_COUNT] And "); 
         UnSQL.SQL.Add(@"   VIEWSTOPICS_COUNT=@[VIEWSTOPICS_COUNT] And "); 
         UnSQL.SQL.Add(@"   IMAGETOPICS=@[IMAGETOPICS] And "); 
         UnSQL.SQL.Add(@"   STATUSTOPICS=@[STATUSTOPICS] And "); 
         UnSQL.SQL.Add(@"   CREATEDDATETOPICS=@[CREATEDDATETOPICS] And "); 
         UnSQL.SQL.Add(@"   KEYWORDSTOPICS=@[KEYWORDSTOPICS] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_TOPICS = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "TOPICS_GETID", Param.ToBytes());
        ResErr = DS_TOPICS.ResErr;
        if (ResErr.NotError) {
            if (DS_TOPICS.DataSet.RecordCount > 0) {
                DS_TOPICS.DataSet.First();
                TOPICS.IDTOPICS = DS_TOPICS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICS.IDTOPICS.FieldName).asInt32();
            }
            else {
                DS_TOPICS.ResErr.NotError = false;
                DS_TOPICS.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_TOPICS.js Persistence.Forums.Methods.TOPICS_GETID", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Forums.Methods.TOPICS_ADD = function (TOPICS) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICS.IDTOPICS.FieldName, TOPICS.IDTOPICS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICS.IDFORUMSCATEGORY.FieldName, TOPICS.IDFORUMSCATEGORY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICS.IDCMDBCI.FieldName, TOPICS.IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.TOPICS.NAMETOPICS.FieldName, TOPICS.NAMETOPICS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.TOPICS.DESCRIPTIONTOPICS.FieldName, TOPICS.DESCRIPTIONTOPICS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICS.RESPONSETOPICS_COUNT.FieldName, TOPICS.RESPONSETOPICS_COUNT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICS.VIEWSTOPICS_COUNT.FieldName, TOPICS.VIEWSTOPICS_COUNT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.TOPICS.IMAGETOPICS.FieldName, TOPICS.IMAGETOPICS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.TOPICS.STATUSTOPICS.FieldName, TOPICS.STATUSTOPICS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoAtisNames.TOPICS.CREATEDDATETOPICS.FieldName, TOPICS.CREATEDDATETOPICS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.TOPICS.KEYWORDSTOPICS.FieldName, TOPICS.KEYWORDSTOPICS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        /* 
        //********   TOPICS_ADD   ************************* 
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"TOPICS_ADD", @"TOPICS_ADD description."); 
        UnSQL.SQL.Add(@"   INSERT INTO TOPICS( "); 
         UnSQL.SQL.Add(@"   IDFORUMSCATEGORY, "); 
         UnSQL.SQL.Add(@"   IDCMDBCI, "); 
         UnSQL.SQL.Add(@"   NAMETOPICS, "); 
         UnSQL.SQL.Add(@"   DESCRIPTIONTOPICS, "); 
         UnSQL.SQL.Add(@"   RESPONSETOPICS_COUNT, "); 
         UnSQL.SQL.Add(@"   VIEWSTOPICS_COUNT, "); 
         UnSQL.SQL.Add(@"   IMAGETOPICS, "); 
         UnSQL.SQL.Add(@"   STATUSTOPICS, "); 
         UnSQL.SQL.Add(@"   CREATEDDATETOPICS, "); 
         UnSQL.SQL.Add(@"   KEYWORDSTOPICS "); 
         UnSQL.SQL.Add(@"   ) Values ( ");  
        UnSQL.SQL.Add(@"   @[IDFORUMSCATEGORY], "); 
         UnSQL.SQL.Add(@"   @[IDCMDBCI], "); 
         UnSQL.SQL.Add(@"   @[NAMETOPICS], "); 
         UnSQL.SQL.Add(@"   @[DESCRIPTIONTOPICS], "); 
         UnSQL.SQL.Add(@"   @[RESPONSETOPICS_COUNT], "); 
         UnSQL.SQL.Add(@"   @[VIEWSTOPICS_COUNT], "); 
         UnSQL.SQL.Add(@"   @[IMAGETOPICS], "); 
         UnSQL.SQL.Add(@"   @[STATUSTOPICS], "); 
         UnSQL.SQL.Add(@"   @[CREATEDDATETOPICS], "); 
         UnSQL.SQL.Add(@"   @[KEYWORDSTOPICS] "); 
         UnSQL.SQL.Add(@"   )  "); 
        UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "TOPICS_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.Forums.Methods.TOPICS_GETID(TOPICS);
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_TOPICS.js Persistence.Forums.Methods.TOPICS_ADD", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Forums.Methods.TOPICS_UPD = function (TOPICS) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICS.IDTOPICS.FieldName, TOPICS.IDTOPICS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICS.IDFORUMSCATEGORY.FieldName, TOPICS.IDFORUMSCATEGORY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICS.IDCMDBCI.FieldName, TOPICS.IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.TOPICS.NAMETOPICS.FieldName, TOPICS.NAMETOPICS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.TOPICS.DESCRIPTIONTOPICS.FieldName, TOPICS.DESCRIPTIONTOPICS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICS.RESPONSETOPICS_COUNT.FieldName, TOPICS.RESPONSETOPICS_COUNT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICS.VIEWSTOPICS_COUNT.FieldName, TOPICS.VIEWSTOPICS_COUNT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.TOPICS.IMAGETOPICS.FieldName, TOPICS.IMAGETOPICS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.TOPICS.STATUSTOPICS.FieldName, TOPICS.STATUSTOPICS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoAtisNames.TOPICS.CREATEDDATETOPICS.FieldName, TOPICS.CREATEDDATETOPICS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.TOPICS.KEYWORDSTOPICS.FieldName, TOPICS.KEYWORDSTOPICS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);


        /*   
        //********   TOPICS_UPD   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"TOPICS_UPD", @"TOPICS_UPD description.");  
        UnSQL.SQL.Add(@"  UPDATE TOPICS Set "); 
         UnSQL.SQL.Add(@"   IDFORUMSCATEGORY=@[IDFORUMSCATEGORY], "); 
         UnSQL.SQL.Add(@"   IDCMDBCI=@[IDCMDBCI], "); 
         UnSQL.SQL.Add(@"   NAMETOPICS=@[NAMETOPICS], "); 
         UnSQL.SQL.Add(@"   DESCRIPTIONTOPICS=@[DESCRIPTIONTOPICS], "); 
         UnSQL.SQL.Add(@"   RESPONSETOPICS_COUNT=@[RESPONSETOPICS_COUNT], "); 
         UnSQL.SQL.Add(@"   VIEWSTOPICS_COUNT=@[VIEWSTOPICS_COUNT], "); 
         UnSQL.SQL.Add(@"   IMAGETOPICS=@[IMAGETOPICS], "); 
         UnSQL.SQL.Add(@"   STATUSTOPICS=@[STATUSTOPICS], "); 
         UnSQL.SQL.Add(@"   CREATEDDATETOPICS=@[CREATEDDATETOPICS], "); 
         UnSQL.SQL.Add(@"   KEYWORDSTOPICS=@[KEYWORDSTOPICS] "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDTOPICS=@[IDTOPICS] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "TOPICS_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_TOPICS.js Persistence.Forums.Methods.TOPICS_UPD", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Forums.Methods.TOPICS_DEL = function (/*String StrIDTOPICS*/IDTOPICS) {
    var Res = -1;
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.Forums.Methods.TOPICS_DELIDTOPICS(/*String StrIDTOPICS*/Param);
    }
    else {
        Res = Persistence.Forums.Methods.TOPICS_DELTOPICS(Param);
    }
    return (Res);
}
Persistence.Forums.Methods.TOPICS_DELIDTOPICS = function (/*String StrIDTOPICS*/IDTOPICS) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDTOPICS == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.TOPICS.IDTOPICS.FieldName, " = " + UsrCfg.InternoAtisNames.TOPICS.NAME_TABLE + "." + UsrCfg.InternoAtisNames.TOPICS.IDTOPICS.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDTOPICS = " IN (" + StrIDTOPICS + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.TOPICS.IDTOPICS.FieldName, StrIDTOPICS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICS.IDTOPICS.FieldName, IDTOPICS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   TOPICS_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"TOPICS_DEL", @"TOPICS_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE TOPICS "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDTOPICS=@[IDTOPICS]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "TOPICS_DEL", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_TOPICS.js Persistence.Forums.Methods.TOPICS_DELIDTOPICS", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Forums.Methods.TOPICS_DELTOPICS = function (TOPICS/*TOPICSList*/) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        /*
        StrIDTOPICS = "";
        StrIDOTRO = "";
        for (var i = 0; i < TOPICSList.length; i++) {
        StrIDTOPICS += TOPICSList[i].IDTOPICS + ",";
        StrIDOTRO += TOPICSList[i].IDOTRO + ",";
        }
        StrIDTOPICS = StrIDTOPICS.substring(0, StrIDTOPICS.length - 1);
        StrIDOTRO = StrIDOTRO.substring(0, StrIDOTRO.length - 1);
        
        
        if (StrIDTOPICS == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.TOPICS.IDTOPICS.FieldName, " = " + UsrCfg.InternoAtisNames.TOPICS.NAME_TABLE + "." + UsrCfg.InternoAtisNames.TOPICS.IDTOPICS.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDTOPICS = " IN (" + StrIDTOPICS + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.TOPICS.IDTOPICS.FieldName, StrIDTOPICS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/


        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICS.IDTOPICS.FieldName, TOPICS.IDTOPICS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*    
        //********   TOPICS_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"TOPICS_DEL", @"TOPICS_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE TOPICS "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDTOPICS=@[IDTOPICS]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "TOPICS_DEL", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_TOPICS.js Persistence.Forums.Methods.TOPICS_DELTOPICS", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Forums.Methods.TOPICS_DELTOPICS_ALL = function (TOPICS) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICS.IDTOPICS.FieldName, TOPICS.IDTOPICS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "TOPICS_DEL_ALL", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_TOPICS.js Persistence.Forums.Methods.TOPICS_DELTOPICS_ALL", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Forums.Methods.TOPICS_UPDVIEWS = function (IDTOPICS) {
    var UpdViews = false;
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICS.IDTOPICS.FieldName, IDTOPICS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "TOPICS_UPDVIEWS", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            UpdViews = true;
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_TOPICS.js Persistence.Forums.Methods.TOPICS_UPDVIEWS", e);
    }
    finally {
        Param.Destroy();
    }
    return (UpdViews);
}

//**********************   METHODS   ********************************************************************************************
Persistence.Forums.Methods.TOPICS_ListSetID = function (TOPICSList, IDTOPICS) {
    for (i = 0; i < TOPICSList.length; i++) {
        if (IDTOPICS == TOPICSList[i].IDTOPICS)
            return (TOPICSList[i]);
    }
    var UnTOPICS = new Persistence.Forums.Properties.TTOPICS;
    UnTOPICS.IDTOPICS = IDTOPICS;
    return (UnTOPICS);
}
Persistence.Forums.Methods.TOPICS_ListAdd = function (TOPICSList, TOPICS) {
    var i = Persistence.Forums.Methods.TOPICS_ListGetIndex(TOPICSList, TOPICS);
    if (i == -1) TOPICSList.push(TOPICS);
    else {
        TOPICSList[i].IDTOPICS = TOPICS.IDTOPICS;
        TOPICSList[i].IDFORUMSCATEGORY = TOPICS.IDFORUMSCATEGORY;
        TOPICSList[i].IDCMDBCI = TOPICS.IDCMDBCI;
        TOPICSList[i].NAMETOPICS = TOPICS.NAMETOPICS;
        TOPICSList[i].DESCRIPTIONTOPICS = TOPICS.DESCRIPTIONTOPICS;
        TOPICSList[i].RESPONSETOPICS_COUNT = TOPICS.RESPONSETOPICS_COUNT;
        TOPICSList[i].VIEWSTOPICS_COUNT = TOPICS.VIEWSTOPICS_COUNT;
        TOPICSList[i].IMAGETOPICS = TOPICS.IMAGETOPICS;
        TOPICSList[i].STATUSTOPICS = TOPICS.STATUSTOPICS;
        TOPICSList[i].CREATEDDATETOPICS = TOPICS.CREATEDDATETOPICS;
        TOPICSList[i].KEYWORDSTOPICS = TOPICS.KEYWORDSTOPICS;

    }
}
Persistence.Forums.Methods.TOPICS_ListGetIndex = function (TOPICSList, Param) {
    var Res = -1;
    if (typeof (Param) == 'number') {
        Res = Persistence.Forums.Methods.TOPICS_ListGetIndexIDTOPICS(TOPICSList, Param);
    }
    else {
        Res = Persistence.Forums.Methods.TOPICS_ListGetIndexTOPICS(TOPICSList, Param);
    }
    return (Res);
}
Persistence.Forums.Methods.TOPICS_ListGetIndexTOPICS = function (TOPICSList, TOPICS) {
    for (i = 0; i < TOPICSList.length; i++) {
        if (TOPICS.IDTOPICS == TOPICSList[i].IDTOPICS)
            return (i);
    }
    return (-1);
}
Persistence.Forums.Methods.TOPICS_ListGetIndexIDTOPICS = function (TOPICSList, IDTOPICS) {
    for (i = 0; i < TOPICSList.length; i++) {
        if (IDTOPICS == TOPICSList[i].IDTOPICS)
            return (i);
    }
    return (-1);
}
//STRING FUNCTION 
Persistence.Forums.Methods.TOPICSListtoStr = function (TOPICSList) {
    var Res = Persistence.Properties._Version + "(1)0";
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(TOPICSList.length, Longitud, Texto);
        for (Counter = 0; Counter < TOPICSList.length; Counter++) {
            var UnTOPICS = TOPICSList[Counter];
            UnTOPICS.ToStr(Longitud, Texto);
        }

        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
        SysCfg.Log.Methods.WriteLog("DB_TOPICS.js Persistence.Forums.Methods.TOPICSListtoStr", e);
    }
    return Res;
}
Persistence.Forums.Methods.StrtoTOPICS = function (ProtocoloStr, TOPICSList) {

    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        TOPICSList.length = 0;
        if (Persistence.Forums.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.Length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnTOPICS = new Persistence.Properties.TTOPICS(); //Mode new row
                UnTOPICS.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Forums.TOPICS_ListAdd(TOPICSList, UnTOPICS);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
        SysCfg.Log.Methods.WriteLog("DB_TOPICS.js Persistence.Forums.Methods.StrtoTOPICS", e);
    }
    return (Res);
}
//SOCKET FUNCTION 
Persistence.Forums.Methods.TOPICSListToByte = function (TOPICSList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, TOPICSList.Count - idx);
    for (i = idx; i < TOPICSList.Count; i++) {
        TOPICSList[i].ToByte(MemStream);
    }
}
Persistence.Forums.Methods.ByteToTOPICSList = function (TOPICSList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnTOPICS = new Persistence.Properties.TTOPICS();
        UnTOPICS.ByteTo(MemStream);
        Methods.TOPICS_ListAdd(TOPICSList, UnTOPICS);
    }
}
