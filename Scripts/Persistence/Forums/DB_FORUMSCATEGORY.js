//**********************   PROPERTIES   *****************************************************************************************
Persistence.Forums.Properties.TFORUMSCATEGORY = function () {
	this.IDFORUMSCATEGORY = 0;
	this.IDMDCATEGORYDETAIL = 0;
	this.IDFORUMS = 0;
	this.DESCRIPTIONFORUMSCATEGORY = "";
	this.TOPICSFORUMSCATEGORY_COUNT = 0;
	this.RESPONSEFORUMSCATEGORY_COUNT = 0;
	this.MEMBERSFORUMSCATEGORY_COUNT = 0;
	this.IMAGEFORUMSCATEGORY = "";
    this.STATUSFORUMSCATEGORY = false;
    //********************** VIRTUALES **********************
    this.TOPICSList = new Array();
    this.FORUMS = new Object();
    this.MDCATEGORYDETAIL = new Object();
    //********************** END VIRTUALES **********************
	//Socket IO Properties
	this.ToByte = function (MemStream) {
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDFORUMSCATEGORY);
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDCATEGORYDETAIL);
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDFORUMS);
		SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.DESCRIPTIONFORUMSCATEGORY);
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.TOPICSFORUMSCATEGORY_COUNT);
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.RESPONSEFORUMSCATEGORY_COUNT);
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.MEMBERSFORUMSCATEGORY_COUNT);
		SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.IMAGEFORUMSCATEGORY);
		SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.STATUSFORUMSCATEGORY);

	}
	this.ByteTo = function (MemStream) {
		this.IDFORUMSCATEGORY = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
		this.IDMDCATEGORYDETAIL = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
		this.IDFORUMS = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
		this.DESCRIPTIONFORUMSCATEGORY = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
		this.TOPICSFORUMSCATEGORY_COUNT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
		this.RESPONSEFORUMSCATEGORY_COUNT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
		this.MEMBERSFORUMSCATEGORY_COUNT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
		this.IMAGEFORUMSCATEGORY = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
		this.STATUSFORUMSCATEGORY = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);

	}
	//Str IO Properties
	this.ToStr = function (Longitud, Texto) {
		SysCfg.Str.Protocol.WriteInt(this.IDFORUMSCATEGORY, Longitud, Texto);
		SysCfg.Str.Protocol.WriteInt(this.IDMDCATEGORYDETAIL, Longitud, Texto);
		SysCfg.Str.Protocol.WriteInt(this.IDFORUMS, Longitud, Texto);
		SysCfg.Str.Protocol.WriteStr(this.DESCRIPTIONFORUMSCATEGORY, Longitud, Texto);
		SysCfg.Str.Protocol.WriteInt(this.TOPICSFORUMSCATEGORY_COUNT, Longitud, Texto);
		SysCfg.Str.Protocol.WriteInt(this.RESPONSEFORUMSCATEGORY_COUNT, Longitud, Texto);
		SysCfg.Str.Protocol.WriteInt(this.MEMBERSFORUMSCATEGORY_COUNT, Longitud, Texto);
		SysCfg.Str.Protocol.WriteStr(this.IMAGEFORUMSCATEGORY, Longitud, Texto);
		SysCfg.Str.Protocol.WriteBool(this.STATUSFORUMSCATEGORY, Longitud, Texto);

	}
	this.StrTo = function (Pos, Index, LongitudArray, Texto) {
		this.IDFORUMSCATEGORY = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
		this.IDMDCATEGORYDETAIL = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
		this.IDFORUMS = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
		this.DESCRIPTIONFORUMSCATEGORY = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
		this.TOPICSFORUMSCATEGORY_COUNT = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
		this.RESPONSEFORUMSCATEGORY_COUNT = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
		this.MEMBERSFORUMSCATEGORY_COUNT = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
		this.IMAGEFORUMSCATEGORY = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
		this.STATUSFORUMSCATEGORY = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);

	}
}
//**********************   METHODS server  ********************************************************************************************
Persistence.Forums.Methods.FORUMSCATEGORY_Fill = function (FORUMSCATEGORYList, StrIDFORUMSCATEGORY/*noin IDFORUMSCATEGORY*/) {
	var ResErr = new SysCfg.Error.Properties.TResErr();
	FORUMSCATEGORYList.length = 0;
	var Param = new SysCfg.Stream.Properties.TParam();
	Param.Inicialize();
	if (StrIDFORUMSCATEGORY == "") {
		Param.AddUnknown(UsrCfg.InternoAtisNames.FORUMSCATEGORY.IDFORUMSCATEGORY.FieldName, " = " + UsrCfg.InternoAtisNames.FORUMSCATEGORY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.FORUMSCATEGORY.IDFORUMSCATEGORY.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
	}
	else {
		StrIDFORUMSCATEGORY = " IN (" + StrIDFORUMSCATEGORY + ")";
		Param.AddUnknown(UsrCfg.InternoAtisNames.FORUMSCATEGORY.IDFORUMSCATEGORY.FieldName, StrIDFORUMSCATEGORY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
	}
	//noin Param.AddInt32(UsrCfg.InternoForumsNames.FORUMSCATEGORY.IDFORUMSCATEGORY.FieldName, IDFORUMSCATEGORY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
	try {
		/*
		//********   FORUMSCATEGORY_GET   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"FORUMSCATEGORY_GET", @"FORUMSCATEGORY_GET description.");  
		UnSQL.SQL.Add(@"   SELECT "); 
		 UnSQL.SQL.Add(@"   IDFORUMSCATEGORY, "); 
		 UnSQL.SQL.Add(@"   IDMDCATEGORYDETAIL, "); 
		 UnSQL.SQL.Add(@"   IDFORUMS, "); 
		 UnSQL.SQL.Add(@"   DESCRIPTIONFORUMSCATEGORY, "); 
		 UnSQL.SQL.Add(@"   TOPICSFORUMSCATEGORY_COUNT, "); 
		 UnSQL.SQL.Add(@"   RESPONSEFORUMSCATEGORY_COUNT, "); 
		 UnSQL.SQL.Add(@"   MEMBERSFORUMSCATEGORY_COUNT, "); 
		 UnSQL.SQL.Add(@"   IMAGEFORUMSCATEGORY, "); 
		 UnSQL.SQL.Add(@"   STATUSFORUMSCATEGORY "); 
		 UnSQL.SQL.Add(@"   FROM FORUMSCATEGORY "); 
		 UnConfigSQL.SQL.Add(UnSQL); 
		 
		
		*/
		var DS_FORUMSCATEGORY = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "FORUMSCATEGORY_GET", Param.ToBytes());
		ResErr = DS_FORUMSCATEGORY.ResErr;
		if (ResErr.NotError) {
			if (DS_FORUMSCATEGORY.DataSet.RecordCount > 0) {
				DS_FORUMSCATEGORY.DataSet.First();
				while (!(DS_FORUMSCATEGORY.DataSet.Eof)) {
					var FORUMSCATEGORY = new Persistence.Forums.Properties.TFORUMSCATEGORY();
					FORUMSCATEGORY.IDFORUMSCATEGORY = DS_FORUMSCATEGORY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.FORUMSCATEGORY.IDFORUMSCATEGORY.FieldName).asInt32();
					FORUMSCATEGORY.IDMDCATEGORYDETAIL = DS_FORUMSCATEGORY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.FORUMSCATEGORY.IDMDCATEGORYDETAIL.FieldName).asInt32();
					FORUMSCATEGORY.IDFORUMS = DS_FORUMSCATEGORY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.FORUMSCATEGORY.IDFORUMS.FieldName).asInt32();
					FORUMSCATEGORY.DESCRIPTIONFORUMSCATEGORY = DS_FORUMSCATEGORY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.FORUMSCATEGORY.DESCRIPTIONFORUMSCATEGORY.FieldName).asText();
					FORUMSCATEGORY.TOPICSFORUMSCATEGORY_COUNT = DS_FORUMSCATEGORY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.FORUMSCATEGORY.TOPICSFORUMSCATEGORY_COUNT.FieldName).asInt32();
					FORUMSCATEGORY.RESPONSEFORUMSCATEGORY_COUNT = DS_FORUMSCATEGORY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.FORUMSCATEGORY.RESPONSEFORUMSCATEGORY_COUNT.FieldName).asInt32();
					FORUMSCATEGORY.MEMBERSFORUMSCATEGORY_COUNT = DS_FORUMSCATEGORY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.FORUMSCATEGORY.MEMBERSFORUMSCATEGORY_COUNT.FieldName).asInt32();
					FORUMSCATEGORY.IMAGEFORUMSCATEGORY = DS_FORUMSCATEGORY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.FORUMSCATEGORY.IMAGEFORUMSCATEGORY.FieldName).asString();
					FORUMSCATEGORY.STATUSFORUMSCATEGORY = DS_FORUMSCATEGORY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.FORUMSCATEGORY.STATUSFORUMSCATEGORY.FieldName).asBoolean();

					//Other
					FORUMSCATEGORYList.push(FORUMSCATEGORY);
					DS_FORUMSCATEGORY.DataSet.Next();
				}
			}
			else {
				DS_FORUMSCATEGORY.ResErr.NotError = false;
				DS_FORUMSCATEGORY.ResErr.Mesaje = "RECORDCOUNT = 0";
			}
		}
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_FORUMSCATEGORY.js Persistence.Forums.Methods.FORUMSCATEGORY_Fill", e);
    }
	finally {
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.Forums.Methods.FORUMSCATEGORY_GETID = function (FORUMSCATEGORY) {
	var ResErr = new SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try {
		Param.Inicialize();
		//Other
		Param.AddInt32(UsrCfg.InternoAtisNames.FORUMSCATEGORY.IDFORUMSCATEGORY.FieldName, FORUMSCATEGORY.IDFORUMSCATEGORY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddInt32(UsrCfg.InternoAtisNames.FORUMSCATEGORY.IDMDCATEGORYDETAIL.FieldName, FORUMSCATEGORY.IDMDCATEGORYDETAIL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddInt32(UsrCfg.InternoAtisNames.FORUMSCATEGORY.IDFORUMS.FieldName, FORUMSCATEGORY.IDFORUMS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddText(UsrCfg.InternoAtisNames.FORUMSCATEGORY.DESCRIPTIONFORUMSCATEGORY.FieldName, FORUMSCATEGORY.DESCRIPTIONFORUMSCATEGORY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddInt32(UsrCfg.InternoAtisNames.FORUMSCATEGORY.TOPICSFORUMSCATEGORY_COUNT.FieldName, FORUMSCATEGORY.TOPICSFORUMSCATEGORY_COUNT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddInt32(UsrCfg.InternoAtisNames.FORUMSCATEGORY.RESPONSEFORUMSCATEGORY_COUNT.FieldName, FORUMSCATEGORY.RESPONSEFORUMSCATEGORY_COUNT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddInt32(UsrCfg.InternoAtisNames.FORUMSCATEGORY.MEMBERSFORUMSCATEGORY_COUNT.FieldName, FORUMSCATEGORY.MEMBERSFORUMSCATEGORY_COUNT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddString(UsrCfg.InternoAtisNames.FORUMSCATEGORY.IMAGEFORUMSCATEGORY.FieldName, FORUMSCATEGORY.IMAGEFORUMSCATEGORY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddBoolean(UsrCfg.InternoAtisNames.FORUMSCATEGORY.STATUSFORUMSCATEGORY.FieldName, FORUMSCATEGORY.STATUSFORUMSCATEGORY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

		/*
		//********   FORUMSCATEGORY_GETID   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"FORUMSCATEGORY_GETID", @"FORUMSCATEGORY_GETID description.");  
		UnSQL.SQL.Add(@"   SELECT "); 
		 UnSQL.SQL.Add(@"   IDFORUMSCATEGORY "); 
		 UnSQL.SQL.Add(@"   FROM FORUMSCATEGORY "); 
		 UnSQL.SQL.Add(@"   Where "); 
		 UnSQL.SQL.Add(@"   IDMDCATEGORYDETAIL=@[IDMDCATEGORYDETAIL] And "); 
		 UnSQL.SQL.Add(@"   IDFORUMS=@[IDFORUMS] And "); 
		 UnSQL.SQL.Add(@"   TOPICSFORUMSCATEGORY_COUNT=@[TOPICSFORUMSCATEGORY_COUNT] And "); 
		 UnSQL.SQL.Add(@"   RESPONSEFORUMSCATEGORY_COUNT=@[RESPONSEFORUMSCATEGORY_COUNT] And "); 
		 UnSQL.SQL.Add(@"   MEMBERSFORUMSCATEGORY_COUNT=@[MEMBERSFORUMSCATEGORY_COUNT] And "); 
		 UnSQL.SQL.Add(@"   IMAGEFORUMSCATEGORY=@[IMAGEFORUMSCATEGORY] And "); 
		 UnSQL.SQL.Add(@"   STATUSFORUMSCATEGORY=@[STATUSFORUMSCATEGORY] ");
		 UnConfigSQL.SQL.Add(UnSQL); 
		 
		
		*/
		var DS_FORUMSCATEGORY = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "FORUMSCATEGORY_GETID", Param.ToBytes());
		ResErr = DS_FORUMSCATEGORY.ResErr;
		if (ResErr.NotError) {
			if (DS_FORUMSCATEGORY.DataSet.RecordCount > 0) {
				DS_FORUMSCATEGORY.DataSet.First();
				FORUMSCATEGORY.IDFORUMSCATEGORY = DS_FORUMSCATEGORY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.FORUMSCATEGORY.IDFORUMSCATEGORY.FieldName).asInt32();
			}
			else {
				DS_FORUMSCATEGORY.ResErr.NotError = false;
				DS_FORUMSCATEGORY.ResErr.Mesaje = "RECORDCOUNT = 0";
			}
		}
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_FORUMSCATEGORY.js Persistence.Forums.Methods.FORUMSCATEGORY_GETID", e);
    }
	finally {
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.Forums.Methods.FORUMSCATEGORY_ADD = function (FORUMSCATEGORY) {
	var ResErr = new SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try {
		Param.Inicialize();
		//Other
		Param.AddInt32(UsrCfg.InternoAtisNames.FORUMSCATEGORY.IDFORUMSCATEGORY.FieldName, FORUMSCATEGORY.IDFORUMSCATEGORY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.FORUMSCATEGORY.IDMDCATEGORYDETAIL.FieldName, FORUMSCATEGORY.IDMDCATEGORYDETAIL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.FORUMSCATEGORY.IDFORUMS.FieldName, FORUMSCATEGORY.IDFORUMS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddText(UsrCfg.InternoAtisNames.FORUMSCATEGORY.DESCRIPTIONFORUMSCATEGORY.FieldName, FORUMSCATEGORY.DESCRIPTIONFORUMSCATEGORY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.FORUMSCATEGORY.TOPICSFORUMSCATEGORY_COUNT.FieldName, FORUMSCATEGORY.TOPICSFORUMSCATEGORY_COUNT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.FORUMSCATEGORY.RESPONSEFORUMSCATEGORY_COUNT.FieldName, FORUMSCATEGORY.RESPONSEFORUMSCATEGORY_COUNT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.FORUMSCATEGORY.MEMBERSFORUMSCATEGORY_COUNT.FieldName, FORUMSCATEGORY.MEMBERSFORUMSCATEGORY_COUNT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.FORUMSCATEGORY.IMAGEFORUMSCATEGORY.FieldName, FORUMSCATEGORY.IMAGEFORUMSCATEGORY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddBoolean(UsrCfg.InternoAtisNames.FORUMSCATEGORY.STATUSFORUMSCATEGORY.FieldName, FORUMSCATEGORY.STATUSFORUMSCATEGORY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

		/* 
		//********   FORUMSCATEGORY_ADD   ************************* 
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"FORUMSCATEGORY_ADD", @"FORUMSCATEGORY_ADD description."); 
		UnSQL.SQL.Add(@"   INSERT INTO FORUMSCATEGORY( "); 
		 UnSQL.SQL.Add(@"   IDMDCATEGORYDETAIL, "); 
		 UnSQL.SQL.Add(@"   IDFORUMS, "); 
		 UnSQL.SQL.Add(@"   DESCRIPTIONFORUMSCATEGORY, "); 
		 UnSQL.SQL.Add(@"   TOPICSFORUMSCATEGORY_COUNT, "); 
		 UnSQL.SQL.Add(@"   RESPONSEFORUMSCATEGORY_COUNT, "); 
		 UnSQL.SQL.Add(@"   MEMBERSFORUMSCATEGORY_COUNT, "); 
		 UnSQL.SQL.Add(@"   IMAGEFORUMSCATEGORY, "); 
		 UnSQL.SQL.Add(@"   STATUSFORUMSCATEGORY "); 
		 UnSQL.SQL.Add(@"   ) Values ( ");  
		UnSQL.SQL.Add(@"   @[IDMDCATEGORYDETAIL], "); 
		 UnSQL.SQL.Add(@"   @[IDFORUMS], "); 
		 UnSQL.SQL.Add(@"   @[DESCRIPTIONFORUMSCATEGORY], "); 
		 UnSQL.SQL.Add(@"   @[TOPICSFORUMSCATEGORY_COUNT], "); 
		 UnSQL.SQL.Add(@"   @[RESPONSEFORUMSCATEGORY_COUNT], "); 
		 UnSQL.SQL.Add(@"   @[MEMBERSFORUMSCATEGORY_COUNT], "); 
		 UnSQL.SQL.Add(@"   @[IMAGEFORUMSCATEGORY], "); 
		 UnSQL.SQL.Add(@"   @[STATUSFORUMSCATEGORY] "); 
		 UnSQL.SQL.Add(@"   )  "); 
		UnConfigSQL.SQL.Add(UnSQL); 
		 
		
		*/
		this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "FORUMSCATEGORY_ADD", Param.ToBytes());
		ResErr = Exec.ResErr;
		if (ResErr.NotError) {
			Persistence.Forums.Methods.FORUMSCATEGORY_GETID(FORUMSCATEGORY);
		}
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_FORUMSCATEGORY.js Persistence.Forums.Methods.FORUMSCATEGORY_ADD", e);
    }
	finally {
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.Forums.Methods.FORUMSCATEGORY_UPD = function (FORUMSCATEGORY) {
	var ResErr = SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try {
		Param.Inicialize();
		Param.AddInt32(UsrCfg.InternoAtisNames.FORUMSCATEGORY.IDFORUMSCATEGORY.FieldName, FORUMSCATEGORY.IDFORUMSCATEGORY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.FORUMSCATEGORY.IDMDCATEGORYDETAIL.FieldName, FORUMSCATEGORY.IDMDCATEGORYDETAIL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.FORUMSCATEGORY.IDFORUMS.FieldName, FORUMSCATEGORY.IDFORUMS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddText(UsrCfg.InternoAtisNames.FORUMSCATEGORY.DESCRIPTIONFORUMSCATEGORY.FieldName, FORUMSCATEGORY.DESCRIPTIONFORUMSCATEGORY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.FORUMSCATEGORY.TOPICSFORUMSCATEGORY_COUNT.FieldName, FORUMSCATEGORY.TOPICSFORUMSCATEGORY_COUNT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.FORUMSCATEGORY.RESPONSEFORUMSCATEGORY_COUNT.FieldName, FORUMSCATEGORY.RESPONSEFORUMSCATEGORY_COUNT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.FORUMSCATEGORY.MEMBERSFORUMSCATEGORY_COUNT.FieldName, FORUMSCATEGORY.MEMBERSFORUMSCATEGORY_COUNT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.FORUMSCATEGORY.IMAGEFORUMSCATEGORY.FieldName, FORUMSCATEGORY.IMAGEFORUMSCATEGORY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddBoolean(UsrCfg.InternoAtisNames.FORUMSCATEGORY.STATUSFORUMSCATEGORY.FieldName, FORUMSCATEGORY.STATUSFORUMSCATEGORY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);


		/*   
		//********   FORUMSCATEGORY_UPD   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"FORUMSCATEGORY_UPD", @"FORUMSCATEGORY_UPD description.");  
		UnSQL.SQL.Add(@"  UPDATE FORUMSCATEGORY Set "); 
		 UnSQL.SQL.Add(@"   IDMDCATEGORYDETAIL=@[IDMDCATEGORYDETAIL], "); 
		 UnSQL.SQL.Add(@"   IDFORUMS=@[IDFORUMS], "); 
		 UnSQL.SQL.Add(@"   DESCRIPTIONFORUMSCATEGORY=@[DESCRIPTIONFORUMSCATEGORY], "); 
		 UnSQL.SQL.Add(@"   TOPICSFORUMSCATEGORY_COUNT=@[TOPICSFORUMSCATEGORY_COUNT], "); 
		 UnSQL.SQL.Add(@"   RESPONSEFORUMSCATEGORY_COUNT=@[RESPONSEFORUMSCATEGORY_COUNT], "); 
		 UnSQL.SQL.Add(@"   MEMBERSFORUMSCATEGORY_COUNT=@[MEMBERSFORUMSCATEGORY_COUNT], "); 
		 UnSQL.SQL.Add(@"   IMAGEFORUMSCATEGORY=@[IMAGEFORUMSCATEGORY], "); 
		 UnSQL.SQL.Add(@"   STATUSFORUMSCATEGORY=@[STATUSFORUMSCATEGORY] "); 
		 UnSQL.SQL.Add(@"   Where "); 
		 UnSQL.SQL.Add(@"   IDFORUMSCATEGORY=@[IDFORUMSCATEGORY] ");
		 UnConfigSQL.SQL.Add(UnSQL); 
		 
		
		*/
		var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "FORUMSCATEGORY_UPD", Param.ToBytes());
		ResErr = Exec.ResErr;
		if (ResErr.NotError) {
			//GET(); 
		}
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_FORUMSCATEGORY.js Persistence.Forums.Methods.FORUMSCATEGORY_UPD", e);
    }
	finally {
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.Forums.Methods.FORUMSCATEGORY_DEL = function (/*String StrIDFORUMSCATEGORY*/IDFORUMSCATEGORY) {
	var Res = -1;
	if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
		Res = Persistence.Forums.Methods.FORUMSCATEGORY_DELIDFORUMSCATEGORY(/*String StrIDFORUMSCATEGORY*/Param);
	}
	else {
		Res = Persistence.Forums.Methods.FORUMSCATEGORY_DELFORUMSCATEGORY(Param);
	}
	return (Res);
}
Persistence.Forums.Methods.FORUMSCATEGORY_DELIDFORUMSCATEGORY = function (/*String StrIDFORUMSCATEGORY*/IDFORUMSCATEGORY) {
	var ResErr = SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try {
		Param.Inicialize();
		/*if (StrIDFORUMSCATEGORY == "")
		{
		Param.AddUnknown(UsrCfg.InternoAtisNames.FORUMSCATEGORY.IDFORUMSCATEGORY.FieldName, " = " + UsrCfg.InternoAtisNames.FORUMSCATEGORY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.FORUMSCATEGORY.IDFORUMSCATEGORY.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
		}
		else
		{
		StrIDFORUMSCATEGORY = " IN (" + StrIDFORUMSCATEGORY + ")";
		Param.AddUnknown(UsrCfg.InternoAtisNames.FORUMSCATEGORY.IDFORUMSCATEGORY.FieldName, StrIDFORUMSCATEGORY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		}*/
		Param.AddInt32(UsrCfg.InternoAtisNames.FORUMSCATEGORY.IDFORUMSCATEGORY.FieldName, IDFORUMSCATEGORY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		/*
		//********   FORUMSCATEGORY_DEL   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"FORUMSCATEGORY_DEL", @"FORUMSCATEGORY_DEL description.");  
		UnSQL.SQL.Add(@"   DELETE FORUMSCATEGORY "); 
		 UnSQL.SQL.Add(@"   Where "); 
		 UnSQL.SQL.Add(@"   IDFORUMSCATEGORY=@[IDFORUMSCATEGORY]   ");
		 UnConfigSQL.SQL.Add(UnSQL); 
		 
		
		*/
		var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "FORUMSCATEGORY_DEL", Param.ToBytes());
		ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_FORUMSCATEGORY.js Persistence.Forums.Methods.FORUMSCATEGORY_DELIDFORUMSCATEGORY", e);
    }
	finally {
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.Forums.Methods.FORUMSCATEGORY_DELFORUMSCATEGORY = function (FORUMSCATEGORY/*FORUMSCATEGORYList*/) {
	var ResErr = SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try {
		/*
		StrIDFORUMSCATEGORY = "";
		StrIDOTRO = "";
		for (var i = 0; i < FORUMSCATEGORYList.length; i++) {
		StrIDFORUMSCATEGORY += FORUMSCATEGORYList[i].IDFORUMSCATEGORY + ",";
		StrIDOTRO += FORUMSCATEGORYList[i].IDOTRO + ",";
		}
		StrIDFORUMSCATEGORY = StrIDFORUMSCATEGORY.substring(0, StrIDFORUMSCATEGORY.length - 1);
		StrIDOTRO = StrIDOTRO.substring(0, StrIDOTRO.length - 1);
		
		
		if (StrIDFORUMSCATEGORY == "")
		{
		Param.AddUnknown(UsrCfg.InternoAtisNames.FORUMSCATEGORY.IDFORUMSCATEGORY.FieldName, " = " + UsrCfg.InternoAtisNames.FORUMSCATEGORY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.FORUMSCATEGORY.IDFORUMSCATEGORY.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
		}
		else
		{
		StrIDFORUMSCATEGORY = " IN (" + StrIDFORUMSCATEGORY + ")";
		Param.AddUnknown(UsrCfg.InternoAtisNames.FORUMSCATEGORY.IDFORUMSCATEGORY.FieldName, StrIDFORUMSCATEGORY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		}*/


		Param.Inicialize();
		Param.AddInt32(UsrCfg.InternoAtisNames.FORUMSCATEGORY.IDFORUMSCATEGORY.FieldName, FORUMSCATEGORY.IDFORUMSCATEGORY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		/*    
		//********   FORUMSCATEGORY_DEL   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"FORUMSCATEGORY_DEL", @"FORUMSCATEGORY_DEL description.");  
		UnSQL.SQL.Add(@"   DELETE FORUMSCATEGORY "); 
		 UnSQL.SQL.Add(@"   Where "); 
		 UnSQL.SQL.Add(@"   IDFORUMSCATEGORY=@[IDFORUMSCATEGORY]   ");
		 UnConfigSQL.SQL.Add(UnSQL); 
		 
		
		*/
		var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "FORUMSCATEGORY_DEL", Param.ToBytes());
		ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_FORUMSCATEGORY.js Persistence.Forums.Methods.FORUMSCATEGORY_DELFORUMSCATEGORY", e);
    }
	finally {
		Param.Destroy();
	}
	return (ResErr);
}
//**********************   METHODS   ********************************************************************************************
Persistence.Forums.Methods.FORUMSCATEGORY_ListSetID = function (FORUMSCATEGORYList, IDFORUMSCATEGORY) {
	for (i = 0; i < FORUMSCATEGORYList.length; i++) {
		if (IDFORUMSCATEGORY == FORUMSCATEGORYList[i].IDFORUMSCATEGORY)
			return (FORUMSCATEGORYList[i]);
	}
	var UnFORUMSCATEGORY = new Persistence.Properties.TFORUMSCATEGORY;
	UnFORUMSCATEGORY.IDFORUMSCATEGORY = IDFORUMSCATEGORY;
	return (UnFORUMSCATEGORY);
}
Persistence.Forums.Methods.FORUMSCATEGORY_ListAdd = function (FORUMSCATEGORYList, FORUMSCATEGORY) {
	var i = Persistence.Forums.Methods.FORUMSCATEGORY_ListGetIndex(FORUMSCATEGORYList, FORUMSCATEGORY);
	if (i == -1) FORUMSCATEGORYList.push(FORUMSCATEGORY);
	else {
		FORUMSCATEGORYList[i].IDFORUMSCATEGORY = FORUMSCATEGORY.IDFORUMSCATEGORY;
		FORUMSCATEGORYList[i].IDMDCATEGORYDETAIL = FORUMSCATEGORY.IDMDCATEGORYDETAIL;
		FORUMSCATEGORYList[i].IDFORUMS = FORUMSCATEGORY.IDFORUMS;
		FORUMSCATEGORYList[i].DESCRIPTIONFORUMSCATEGORY = FORUMSCATEGORY.DESCRIPTIONFORUMSCATEGORY;
		FORUMSCATEGORYList[i].TOPICSFORUMSCATEGORY_COUNT = FORUMSCATEGORY.TOPICSFORUMSCATEGORY_COUNT;
		FORUMSCATEGORYList[i].RESPONSEFORUMSCATEGORY_COUNT = FORUMSCATEGORY.RESPONSEFORUMSCATEGORY_COUNT;
		FORUMSCATEGORYList[i].MEMBERSFORUMSCATEGORY_COUNT = FORUMSCATEGORY.MEMBERSFORUMSCATEGORY_COUNT;
		FORUMSCATEGORYList[i].IMAGEFORUMSCATEGORY = FORUMSCATEGORY.IMAGEFORUMSCATEGORY;
		FORUMSCATEGORYList[i].STATUSFORUMSCATEGORY = FORUMSCATEGORY.STATUSFORUMSCATEGORY;

	}
}
Persistence.Forums.Methods.FORUMSCATEGORY_ListGetIndex = function (FORUMSCATEGORYList, Param) {
	var Res = -1;
	if (typeof (Param) == 'number') {
		Res = Persistence.Forums.Methods.FORUMSCATEGORY_ListGetIndexIDFORUMSCATEGORY(FORUMSCATEGORYList, Param);
	}
	else {
		Res = Persistence.Forums.Methods.FORUMSCATEGORY_ListGetIndexFORUMSCATEGORY(FORUMSCATEGORYList, Param);
	}
	return (Res);
}
Persistence.Forums.Methods.FORUMSCATEGORY_ListGetIndexFORUMSCATEGORY = function (FORUMSCATEGORYList, FORUMSCATEGORY) {
	for (i = 0; i < FORUMSCATEGORYList.length; i++) {
		if (FORUMSCATEGORY.IDFORUMSCATEGORY == FORUMSCATEGORYList[i].IDFORUMSCATEGORY)
			return (i);
	}
	return (-1);
}
Persistence.Forums.Methods.FORUMSCATEGORY_ListGetIndexIDFORUMSCATEGORY = function (FORUMSCATEGORYList, IDFORUMSCATEGORY) {
	for (i = 0; i < FORUMSCATEGORYList.length; i++) {
		if (IDFORUMSCATEGORY == FORUMSCATEGORYList[i].IDFORUMSCATEGORY)
			return (i);
	}
	return (-1);
}
//STRING FUNCTION 
Persistence.Forums.Methods.FORUMSCATEGORYListtoStr = function (FORUMSCATEGORYList) {
	var Res = Persistence.Properties._Version + "(1)0";
	var Longitud = new SysCfg.ref("");
	var Texto = new SysCfg.ref("");
	try {
		SysCfg.Str.Protocol.WriteInt(FORUMSCATEGORYList.length, Longitud, Texto);
		for (Counter = 0; Counter < FORUMSCATEGORYList.length; Counter++) {
			var UnFORUMSCATEGORY = FORUMSCATEGORYList[Counter];
			UnFORUMSCATEGORY.ToStr(Longitud, Texto);
		}

		Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
		Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
	}
	catch (e) {
		var ResErr = new SysCfg.Error.Properties.TResErr();
		ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
        SysCfg.Log.Methods.WriteLog("DB_FORUMSCATEGORY.js Persistence.Forums.Methods.FORUMSCATEGORYListtoStr ", e);
	}
	return Res;
}
Persistence.Forums.Methods.StrtoFORUMSCATEGORY = function (ProtocoloStr, FORUMSCATEGORYList) {

	var Res = false;
	var Longitud, Texto;
	var Index = new SysCfg.ref(0);
	var Pos = new SysCfg.ref(0);
	try {
		FORUMSCATEGORYList.length = 0;
		if (Persistence.Forums.Properties._Version == ProtocoloStr.substring(0, 3)) {
			Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
			Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.Length);
			LongitudArray = Longitud.split(",");
			LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
			for (i = 0; i < LSCount; i++) {
				UnFORUMSCATEGORY = new Persistence.Properties.TFORUMSCATEGORY(); //Mode new row
				UnFORUMSCATEGORY.StrTo(Pos, Index, LongitudArray, Texto);
				Persistence.Forums.FORUMSCATEGORY_ListAdd(FORUMSCATEGORYList, UnFORUMSCATEGORY);
			}
		}
	}
	catch (e) {
		var ResErr = SysCfg.Error.Properties.TResErr;
		ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
        SysCfg.Log.Methods.WriteLog("DB_FORUMSCATEGORY.js Persistence.Forums.Methods.StrtoFORUMSCATEGORY ", e);

	}
	return (Res);
}
//SOCKET FUNCTION 
Persistence.Forums.Methods.FORUMSCATEGORYListToByte = function (FORUMSCATEGORYList, MemStream, idx) {
	SysCfg.Stream.Methods.WriteStreamInt32(MemStream, FORUMSCATEGORYList.Count - idx);
	for (i = idx; i < FORUMSCATEGORYList.Count; i++) {
		FORUMSCATEGORYList[i].ToByte(MemStream);
	}
}
Persistence.Forums.Methods.ByteToFORUMSCATEGORYList = function (FORUMSCATEGORYList, MemStream) {
	Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
	for (i = 0; i < Count; i++) {
		UnFORUMSCATEGORY = new Persistence.Properties.TFORUMSCATEGORY();
		UnFORUMSCATEGORY.ByteTo(MemStream);
		Methods.FORUMSCATEGORY_ListAdd(FORUMSCATEGORYList, UnFORUMSCATEGORY);
	}
}
