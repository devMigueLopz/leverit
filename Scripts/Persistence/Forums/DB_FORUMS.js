//**********************   PROPERTIES   *****************************************************************************************
Persistence.Forums.Properties.TFORUMS = function () {
    this.IDFORUMS = 0;
    this.IDCMDBCI = 0;
    this.NAMEFORUMS = "";
    this.DESCRIPTIONFORUMS = "";
    //********************** VIRTUALES **********************
    this.FORUMSCATEGORYList = new Array();
    this.FORUMSSEARCHList = new Array();
    this.CMDBCI = new Object();
    //********************** END VIRTUALES **********************
    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDFORUMS);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDCMDBCI);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.NAMEFORUMS);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.DESCRIPTIONFORUMS);

    }
    this.ByteTo = function (MemStream) {
        this.IDFORUMS = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDCMDBCI = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.NAMEFORUMS = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.DESCRIPTIONFORUMS = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);

    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDFORUMS, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDCMDBCI, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.NAMEFORUMS, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.DESCRIPTIONFORUMS, Longitud, Texto);

    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDFORUMS = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDCMDBCI = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.NAMEFORUMS = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.DESCRIPTIONFORUMS = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);

    }
}
//**********************   METHODS server  ********************************************************************************************
Persistence.Forums.Methods.FORUMS_Fill = function (FORUMSList, StrIDFORUMS/*noin IDFORUMS*/) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    FORUMSList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDFORUMS == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.FORUMS.IDFORUMS.FieldName, " = " + UsrCfg.InternoAtisNames.FORUMS.NAME_TABLE + "." + UsrCfg.InternoAtisNames.FORUMS.IDFORUMS.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDFORUMS = " IN (" + StrIDFORUMS + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.FORUMS.IDFORUMS.FieldName, StrIDFORUMS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //noin Param.AddInt32(UsrCfg.InternoDemoNames.FORUMS.IDFORUMS.FieldName, IDFORUMS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {
        /*
        //********   FORUMS_GET   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"FORUMS_GET", @"FORUMS_GET description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   IDFORUMS, "); 
         UnSQL.SQL.Add(@"   IDCMDBCI, "); 
         UnSQL.SQL.Add(@"   NAMEFORUMS, "); 
         UnSQL.SQL.Add(@"   DESCRIPTIONFORUMS "); 
         UnSQL.SQL.Add(@"   FROM FORUMS "); 
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_FORUMS = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "FORUMS_GET", Param.ToBytes());
        ResErr = DS_FORUMS.ResErr;
        if (ResErr.NotError) {
            if (DS_FORUMS.DataSet.RecordCount > 0) {
                DS_FORUMS.DataSet.First();
                while (!(DS_FORUMS.DataSet.Eof)) {
                    var FORUMS = new Persistence.Forums.Properties.TFORUMS();
                    FORUMS.IDFORUMS = DS_FORUMS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.FORUMS.IDFORUMS.FieldName).asInt32();
                    FORUMS.IDCMDBCI = DS_FORUMS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.FORUMS.IDCMDBCI.FieldName).asInt32();
                    FORUMS.NAMEFORUMS = DS_FORUMS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.FORUMS.NAMEFORUMS.FieldName).asString();
                    FORUMS.DESCRIPTIONFORUMS = DS_FORUMS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.FORUMS.DESCRIPTIONFORUMS.FieldName).asText();

                    //Other
                    FORUMSList.push(FORUMS);
                    DS_FORUMS.DataSet.Next();
                }
            }
            else {
                DS_FORUMS.ResErr.NotError = false;
                DS_FORUMS.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_FORUMS.js Persistence.FORUMS.Methods.FORUMS_Fill ", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Forums.Methods.FORUMS_GETID = function (FORUMS) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.FORUMS.IDFORUMS.FieldName, FORUMS.IDFORUMS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.FORUMS.IDCMDBCI.FieldName, FORUMS.IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.FORUMS.NAMEFORUMS.FieldName, FORUMS.NAMEFORUMS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddText(UsrCfg.InternoAtisNames.FORUMS.DESCRIPTIONFORUMS.FieldName, FORUMS.DESCRIPTIONFORUMS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

        /*
        //********   FORUMS_GETID   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"FORUMS_GETID", @"FORUMS_GETID description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   IDFORUMS "); 
         UnSQL.SQL.Add(@"   FROM FORUMS "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDCMDBCI=@[IDCMDBCI] And "); 
         UnSQL.SQL.Add(@"   NAMEFORUMS=@[NAMEFORUMS] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_FORUMS = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "FORUMS_GETID", Param.ToBytes());
        ResErr = DS_FORUMS.ResErr;
        if (ResErr.NotError) {
            if (DS_FORUMS.DataSet.RecordCount > 0) {
                DS_FORUMS.DataSet.First();
                FORUMS.IDFORUMS = DS_FORUMS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.FORUMS.IDFORUMS.FieldName).asInt32();
            }
            else {
                DS_FORUMS.ResErr.NotError = false;
                DS_FORUMS.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_FORUMS.js Persistence.FORUMS.Methods.FORUMS_GETID ", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Forums.Methods.FORUMS_ADD = function (FORUMS) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.FORUMS.IDFORUMS.FieldName, FORUMS.IDFORUMS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.FORUMS.IDCMDBCI.FieldName, FORUMS.IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.FORUMS.NAMEFORUMS.FieldName, FORUMS.NAMEFORUMS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.FORUMS.DESCRIPTIONFORUMS.FieldName, FORUMS.DESCRIPTIONFORUMS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        /* 
        //********   FORUMS_ADD   ************************* 
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"FORUMS_ADD", @"FORUMS_ADD description."); 
        UnSQL.SQL.Add(@"   INSERT INTO FORUMS( "); 
         UnSQL.SQL.Add(@"   IDCMDBCI, "); 
         UnSQL.SQL.Add(@"   NAMEFORUMS, "); 
         UnSQL.SQL.Add(@"   DESCRIPTIONFORUMS "); 
         UnSQL.SQL.Add(@"   ) Values ( ");  
        UnSQL.SQL.Add(@"   @[IDCMDBCI], "); 
         UnSQL.SQL.Add(@"   @[NAMEFORUMS], "); 
         UnSQL.SQL.Add(@"   @[DESCRIPTIONFORUMS] "); 
         UnSQL.SQL.Add(@"   )  "); 
        UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "FORUMS_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.Forums.Methods.FORUMS_GETID(FORUMS);
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_FORUMS.js Persistence.FORUMS.Methods.FORUMS_ADD ", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Forums.Methods.FORUMS_UPD = function (FORUMS) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.FORUMS.IDFORUMS.FieldName, FORUMS.IDFORUMS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.FORUMS.IDCMDBCI.FieldName, FORUMS.IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.FORUMS.NAMEFORUMS.FieldName, FORUMS.NAMEFORUMS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.FORUMS.DESCRIPTIONFORUMS.FieldName, FORUMS.DESCRIPTIONFORUMS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);


        /*   
        //********   FORUMS_UPD   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"FORUMS_UPD", @"FORUMS_UPD description.");  
        UnSQL.SQL.Add(@"  UPDATE FORUMS Set "); 
         UnSQL.SQL.Add(@"   IDCMDBCI=@[IDCMDBCI], "); 
         UnSQL.SQL.Add(@"   NAMEFORUMS=@[NAMEFORUMS], "); 
         UnSQL.SQL.Add(@"   DESCRIPTIONFORUMS=@[DESCRIPTIONFORUMS] "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDFORUMS=@[IDFORUMS] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "FORUMS_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_FORUMS.js Persistence.FORUMS.Methods.FORUMS_UPD ", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Forums.Methods.FORUMS_DEL = function (/*String StrIDFORUMS*/IDFORUMS) {
    var Res = -1;
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.Forums.Methods.FORUMS_DELIDFORUMS(/*String StrIDFORUMS*/Param);
    }
    else {
        Res = Persistence.Forums.Methods.FORUMS_DELFORUMS(Param);
    }
    return (Res);
}
Persistence.Forums.Methods.FORUMS_DELIDFORUMS = function (/*String StrIDFORUMS*/IDFORUMS) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDFORUMS == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.FORUMS.IDFORUMS.FieldName, " = " + UsrCfg.InternoAtisNames.FORUMS.NAME_TABLE + "." + UsrCfg.InternoAtisNames.FORUMS.IDFORUMS.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDFORUMS = " IN (" + StrIDFORUMS + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.FORUMS.IDFORUMS.FieldName, StrIDFORUMS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.FORUMS.IDFORUMS.FieldName, IDFORUMS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   FORUMS_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"FORUMS_DEL", @"FORUMS_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE FORUMS "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDFORUMS=@[IDFORUMS]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "FORUMS_DEL", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_FORUMS.js Persistence.FORUMS.Methods.FORUMS_DELIDFORUMS ", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Forums.Methods.FORUMS_DELFORUMS = function (FORUMS/*FORUMSList*/) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        /*
        StrIDFORUMS = "";
        StrIDOTRO = "";
        for (var i = 0; i < FORUMSList.length; i++) {
        StrIDFORUMS += FORUMSList[i].IDFORUMS + ",";
        StrIDOTRO += FORUMSList[i].IDOTRO + ",";
        }
        StrIDFORUMS = StrIDFORUMS.substring(0, StrIDFORUMS.length - 1);
        StrIDOTRO = StrIDOTRO.substring(0, StrIDOTRO.length - 1);
        
        
        if (StrIDFORUMS == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.FORUMS.IDFORUMS.FieldName, " = " + UsrCfg.InternoAtisNames.FORUMS.NAME_TABLE + "." + UsrCfg.InternoAtisNames.FORUMS.IDFORUMS.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDFORUMS = " IN (" + StrIDFORUMS + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.FORUMS.IDFORUMS.FieldName, StrIDFORUMS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/


        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.FORUMS.IDFORUMS.FieldName, FORUMS.IDFORUMS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*    
        //********   FORUMS_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"FORUMS_DEL", @"FORUMS_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE FORUMS "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDFORUMS=@[IDFORUMS]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "FORUMS_DEL", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_FORUMS.js Persistence.FORUMS.Methods.FORUMS_DELFORUMS ", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
//**********************   METHODS   ********************************************************************************************
Persistence.Forums.Methods.FORUMS_ListSetID = function (FORUMSList, IDFORUMS) {
    for (i = 0; i < FORUMSList.length; i++) {
        if (IDFORUMS == FORUMSList[i].IDFORUMS)
            return (FORUMSList[i]);
    }
    var UnFORUMS = new Persistence.Forums.Properties.TFORUMS();;
    UnFORUMS.IDFORUMS = IDFORUMS;
    return (UnFORUMS);
}
Persistence.Forums.Methods.FORUMS_ListAdd = function (FORUMSList, FORUMS) {
    var i = Persistence.Forums.Methods.FORUMS_ListGetIndex(FORUMSList, FORUMS);
    if (i == -1) FORUMSList.push(FORUMS);
    else {
        FORUMSList[i].IDFORUMS = FORUMS.IDFORUMS;
        FORUMSList[i].IDCMDBCI = FORUMS.IDCMDBCI;
        FORUMSList[i].NAMEFORUMS = FORUMS.NAMEFORUMS;
        FORUMSList[i].DESCRIPTIONFORUMS = FORUMS.DESCRIPTIONFORUMS;

    }
}
Persistence.Forums.Methods.FORUMS_ListGetIndex = function (FORUMSList, Param) {
    var Res = -1;
    if (typeof (Param) == 'number') {
        Res = Persistence.Forums.Methods.FORUMS_ListGetIndexIDFORUMS(FORUMSList, Param);
    }
    else {
        Res = Persistence.Forums.Methods.FORUMS_ListGetIndexFORUMS(FORUMSList, Param);
    }
    return (Res);
}
Persistence.Forums.Methods.FORUMS_ListGetIndexFORUMS = function (FORUMSList, FORUMS) {
    for (i = 0; i < FORUMSList.length; i++) {
        if (FORUMS.IDFORUMS == FORUMSList[i].IDFORUMS)
            return (i);
    }
    return (-1);
}
Persistence.Forums.Methods.FORUMS_ListGetIndexIDFORUMS = function (FORUMSList, IDFORUMS) {
    for (i = 0; i < FORUMSList.length; i++) {
        if (IDFORUMS == FORUMSList[i].IDFORUMS)
            return (i);
    }
    return (-1);
}
//STRING FUNCTION 
Persistence.Forums.Methods.FORUMSListtoStr = function (FORUMSList) {
    var Res = Persistence.Properties._Version + "(1)0";
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(FORUMSList.length, Longitud, Texto);
        for (Counter = 0; Counter < FORUMSList.length; Counter++) {
            var UnFORUMS = FORUMSList[Counter];
            UnFORUMS.ToStr(Longitud, Texto);
        }

        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
}
Persistence.Forums.Methods.StrtoFORUMS = function (ProtocoloStr, FORUMSList) {

    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        FORUMSList.length = 0;
        if (Persistence.Forums.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.Length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnFORUMS = new Persistence.Properties.TFORUMS(); //Mode new row
                UnFORUMS.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Forums.FORUMS_ListAdd(FORUMSList, UnFORUMS);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
        SysCfg.Log.Methods.WriteLog("DB_FORUMS.js Persistence.Forums.Methods.StrtoFORUMS ", e);

    }
    return (Res);
}
//SOCKET FUNCTION 
Persistence.Forums.Methods.FORUMSListToByte = function (FORUMSList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, FORUMSList.Count - idx);
    for (i = idx; i < FORUMSList.Count; i++) {
        FORUMSList[i].ToByte(MemStream);
    }
}
Persistence.Forums.Methods.ByteToFORUMSList = function (FORUMSList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnFORUMS = new Persistence.Properties.TFORUMS();
        UnFORUMS.ByteTo(MemStream);
        Methods.FORUMS_ListAdd(FORUMSList, UnFORUMS);
    }
}
