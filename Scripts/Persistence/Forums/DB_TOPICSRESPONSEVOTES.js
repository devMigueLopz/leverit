//**********************   PROPERTIES   *****************************************************************************************
Persistence.Forums.Properties.TTOPICSRESPONSEVOTES = function () {
    this.IDTOPICSRESPONSEVOTES = 0;
    this.IDTOPICSRESPONSE = 0;
    this.IDCMDBCI = 0;
    this.LIKETOPICSRESPONSE = false;
    this.NOTLIKETOPICSRESPONSE = false;
    //********************** VIRTUALES **********************
    this.TOPICSRESPONSE = new Object();
    this.CMDBCI = new Object();
    //********************** END VIRTUALES **********************
    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDTOPICSRESPONSEVOTES);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDTOPICSRESPONSE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDCMDBCI);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.LIKETOPICSRESPONSE);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.NOTLIKETOPICSRESPONSE);

    }
    this.ByteTo = function (MemStream) {
        this.IDTOPICSRESPONSEVOTES = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDTOPICSRESPONSE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDCMDBCI = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.LIKETOPICSRESPONSE = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.NOTLIKETOPICSRESPONSE = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);

    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDTOPICSRESPONSEVOTES, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDTOPICSRESPONSE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDCMDBCI, Longitud, Texto);
        SysCfg.Str.Protocol.WriteBool(this.LIKETOPICSRESPONSE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteBool(this.NOTLIKETOPICSRESPONSE, Longitud, Texto);

    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDTOPICSRESPONSEVOTES = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDTOPICSRESPONSE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDCMDBCI = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.LIKETOPICSRESPONSE = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);
        this.NOTLIKETOPICSRESPONSE = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);

    }
}
//**********************   METHODS server  ********************************************************************************************
Persistence.Forums.Methods.TOPICSRESPONSEVOTES_Fill = function (TOPICSRESPONSEVOTESList, StrIDTOPICSRESPONSEVOTES/*noin IDTOPICSRESPONSEVOTES*/) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    TOPICSRESPONSEVOTESList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDTOPICSRESPONSEVOTES == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDTOPICSRESPONSEVOTES.FieldName, " = " + UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.NAME_TABLE + "." + UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDTOPICSRESPONSEVOTES.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDTOPICSRESPONSEVOTES = " IN (" + StrIDTOPICSRESPONSEVOTES + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDTOPICSRESPONSEVOTES.FieldName, StrIDTOPICSRESPONSEVOTES, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //noin Param.AddInt32(UsrCfg.InternoDemoNames.TOPICSRESPONSEVOTES.IDTOPICSRESPONSEVOTES.FieldName, IDTOPICSRESPONSEVOTES, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {
        /*
        //********   TOPICSRESPONSEVOTES_GET   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"TOPICSRESPONSEVOTES_GET", @"TOPICSRESPONSEVOTES_GET description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   IDTOPICSRESPONSEVOTES, "); 
         UnSQL.SQL.Add(@"   IDTOPICSRESPONSE, "); 
         UnSQL.SQL.Add(@"   IDCMDBCI, "); 
         UnSQL.SQL.Add(@"   LIKETOPICSRESPONSE, "); 
         UnSQL.SQL.Add(@"   NOTLIKETOPICSRESPONSE "); 
         UnSQL.SQL.Add(@"   FROM TOPICSRESPONSEVOTES "); 
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_TOPICSRESPONSEVOTES = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "TOPICSRESPONSEVOTES_GET", Param.ToBytes());
        ResErr = DS_TOPICSRESPONSEVOTES.ResErr;
        if (ResErr.NotError) {
            if (DS_TOPICSRESPONSEVOTES.DataSet.RecordCount > 0) {
                DS_TOPICSRESPONSEVOTES.DataSet.First();
                while (!(DS_TOPICSRESPONSEVOTES.DataSet.Eof)) {
                    var TOPICSRESPONSEVOTES = new Persistence.Forums.Properties.TTOPICSRESPONSEVOTES();
                    TOPICSRESPONSEVOTES.IDTOPICSRESPONSEVOTES = DS_TOPICSRESPONSEVOTES.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDTOPICSRESPONSEVOTES.FieldName).asInt32();
                    TOPICSRESPONSEVOTES.IDTOPICSRESPONSE = DS_TOPICSRESPONSEVOTES.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDTOPICSRESPONSE.FieldName).asInt32();
                    TOPICSRESPONSEVOTES.IDCMDBCI = DS_TOPICSRESPONSEVOTES.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDCMDBCI.FieldName).asInt32();
                    TOPICSRESPONSEVOTES.LIKETOPICSRESPONSE = DS_TOPICSRESPONSEVOTES.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.LIKETOPICSRESPONSE.FieldName).asBoolean();
                    TOPICSRESPONSEVOTES.NOTLIKETOPICSRESPONSE = DS_TOPICSRESPONSEVOTES.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.NOTLIKETOPICSRESPONSE.FieldName).asBoolean();

                    //Other
                    TOPICSRESPONSEVOTESList.push(TOPICSRESPONSEVOTES);
                    DS_TOPICSRESPONSEVOTES.DataSet.Next();
                }
            }
            else {
                DS_TOPICSRESPONSEVOTES.ResErr.NotError = false;
                DS_TOPICSRESPONSEVOTES.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_TOPICSRESPONSEVOTES.js Persistence.Forums.Methods.TOPICSRESPONSEVOTES_Fill", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Forums.Methods.TOPICSRESPONSEVOTES_GETID = function (TOPICSRESPONSEVOTES) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDTOPICSRESPONSEVOTES.FieldName, TOPICSRESPONSEVOTES.IDTOPICSRESPONSEVOTES, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDTOPICSRESPONSE.FieldName, TOPICSRESPONSEVOTES.IDTOPICSRESPONSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDCMDBCI.FieldName, TOPICSRESPONSEVOTES.IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddBoolean(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.LIKETOPICSRESPONSE.FieldName, TOPICSRESPONSEVOTES.LIKETOPICSRESPONSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddBoolean(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.NOTLIKETOPICSRESPONSE.FieldName, TOPICSRESPONSEVOTES.NOTLIKETOPICSRESPONSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

        /*
        //********   TOPICSRESPONSEVOTES_GETID   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"TOPICSRESPONSEVOTES_GETID", @"TOPICSRESPONSEVOTES_GETID description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   IDTOPICSRESPONSEVOTES "); 
         UnSQL.SQL.Add(@"   FROM TOPICSRESPONSEVOTES "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDTOPICSRESPONSE=@[IDTOPICSRESPONSE] And "); 
         UnSQL.SQL.Add(@"   IDCMDBCI=@[IDCMDBCI] And "); 
         UnSQL.SQL.Add(@"   LIKETOPICSRESPONSE=@[LIKETOPICSRESPONSE] And "); 
         UnSQL.SQL.Add(@"   NOTLIKETOPICSRESPONSE=@[NOTLIKETOPICSRESPONSE] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_TOPICSRESPONSEVOTES = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "TOPICSRESPONSEVOTES_GETID", Param.ToBytes());
        ResErr = DS_TOPICSRESPONSEVOTES.ResErr;
        if (ResErr.NotError) {
            if (DS_TOPICSRESPONSEVOTES.DataSet.RecordCount > 0) {
                DS_TOPICSRESPONSEVOTES.DataSet.First();
                TOPICSRESPONSEVOTES.IDTOPICSRESPONSEVOTES = DS_TOPICSRESPONSEVOTES.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDTOPICSRESPONSEVOTES.FieldName).asInt32();
            }
            else {
                DS_TOPICSRESPONSEVOTES.ResErr.NotError = false;
                DS_TOPICSRESPONSEVOTES.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_TOPICSRESPONSEVOTES.js Persistence.Forums.Methods.TOPICSRESPONSEVOTES_GETID", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Forums.Methods.TOPICSRESPONSEVOTES_GETLIKEANDNOTLIKE = function (userId, TopicId) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    var objResult = new Array();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICS.FieldName, TopicId, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDCMDBCI.FieldName, userId, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

        var DS_TOPICSRESPONSEVOTES = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "TOPICSRESPONSEVOTES_GETLIKEANDNOTLIKE", Param.ToBytes());
        ResErr = DS_TOPICSRESPONSEVOTES.ResErr;
        if (ResErr.NotError) {
            if (DS_TOPICSRESPONSEVOTES.DataSet.RecordCount > 0) {
                DS_TOPICSRESPONSEVOTES.DataSet.First();
                while (!(DS_TOPICSRESPONSEVOTES.DataSet.Eof)) {
                    var TOPICSRESPONSEVOTES = new Persistence.Forums.Properties.TTOPICSRESPONSEVOTES();
                    TOPICSRESPONSEVOTES.IDTOPICSRESPONSEVOTES = DS_TOPICSRESPONSEVOTES.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDTOPICSRESPONSEVOTES.FieldName).asInt32();
                    TOPICSRESPONSEVOTES.IDTOPICSRESPONSE = DS_TOPICSRESPONSEVOTES.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDTOPICSRESPONSE.FieldName).asInt32();
                    TOPICSRESPONSEVOTES.IDCMDBCI = DS_TOPICSRESPONSEVOTES.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDCMDBCI.FieldName).asInt32();
                    TOPICSRESPONSEVOTES.LIKETOPICSRESPONSE = DS_TOPICSRESPONSEVOTES.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.LIKETOPICSRESPONSE.FieldName).asBoolean();
                    TOPICSRESPONSEVOTES.NOTLIKETOPICSRESPONSE = DS_TOPICSRESPONSEVOTES.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.NOTLIKETOPICSRESPONSE.FieldName).asBoolean();
                    objResult.push(TOPICSRESPONSEVOTES);
                    DS_TOPICSRESPONSEVOTES.DataSet.Next();
                }
            }
            else {
                DS_TOPICSRESPONSEVOTES.ResErr.NotError = false;
                DS_TOPICSRESPONSEVOTES.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_TOPICSRESPONSE.js Persistence.Forums.Methods.TOPICSRESPONSEVOTES_GETLIKEANDNOTLIKE", e);
    }
    finally {
        Param.Destroy();
    }
    return (objResult);
}
Persistence.Forums.Methods.TOPICSRESPONSEVOTES_ADD = function (TOPICSRESPONSEVOTES) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDTOPICSRESPONSEVOTES.FieldName, TOPICSRESPONSEVOTES.IDTOPICSRESPONSEVOTES, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDTOPICSRESPONSE.FieldName, TOPICSRESPONSEVOTES.IDTOPICSRESPONSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDCMDBCI.FieldName, TOPICSRESPONSEVOTES.IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.LIKETOPICSRESPONSE.FieldName, TOPICSRESPONSEVOTES.LIKETOPICSRESPONSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.NOTLIKETOPICSRESPONSE.FieldName, TOPICSRESPONSEVOTES.NOTLIKETOPICSRESPONSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        /* 
        //********   TOPICSRESPONSEVOTES_ADD   ************************* 
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"TOPICSRESPONSEVOTES_ADD", @"TOPICSRESPONSEVOTES_ADD description."); 
        UnSQL.SQL.Add(@"   INSERT INTO TOPICSRESPONSEVOTES( "); 
         UnSQL.SQL.Add(@"   IDTOPICSRESPONSE, "); 
         UnSQL.SQL.Add(@"   IDCMDBCI, "); 
         UnSQL.SQL.Add(@"   LIKETOPICSRESPONSE, "); 
         UnSQL.SQL.Add(@"   NOTLIKETOPICSRESPONSE "); 
         UnSQL.SQL.Add(@"   ) Values ( ");  
        UnSQL.SQL.Add(@"   @[IDTOPICSRESPONSE], "); 
         UnSQL.SQL.Add(@"   @[IDCMDBCI], "); 
         UnSQL.SQL.Add(@"   @[LIKETOPICSRESPONSE], "); 
         UnSQL.SQL.Add(@"   @[NOTLIKETOPICSRESPONSE] "); 
         UnSQL.SQL.Add(@"   )  "); 
        UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "TOPICSRESPONSEVOTES_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.Forums.Methods.TOPICSRESPONSEVOTES_GETID(TOPICSRESPONSEVOTES);
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_TOPICSRESPONSEVOTES.js Persistence.Forums.Methods.TOPICSRESPONSEVOTES_ADD", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Forums.Methods.TOPICSRESPONSEVOTES_UPD = function (TOPICSRESPONSEVOTES) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDTOPICSRESPONSEVOTES.FieldName, TOPICSRESPONSEVOTES.IDTOPICSRESPONSEVOTES, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDTOPICSRESPONSE.FieldName, TOPICSRESPONSEVOTES.IDTOPICSRESPONSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDCMDBCI.FieldName, TOPICSRESPONSEVOTES.IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.LIKETOPICSRESPONSE.FieldName, TOPICSRESPONSEVOTES.LIKETOPICSRESPONSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.NOTLIKETOPICSRESPONSE.FieldName, TOPICSRESPONSEVOTES.NOTLIKETOPICSRESPONSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);


        /*   
        //********   TOPICSRESPONSEVOTES_UPD   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"TOPICSRESPONSEVOTES_UPD", @"TOPICSRESPONSEVOTES_UPD description.");  
        UnSQL.SQL.Add(@"  UPDATE TOPICSRESPONSEVOTES Set "); 
         UnSQL.SQL.Add(@"   IDTOPICSRESPONSE=@[IDTOPICSRESPONSE], "); 
         UnSQL.SQL.Add(@"   IDCMDBCI=@[IDCMDBCI], "); 
         UnSQL.SQL.Add(@"   LIKETOPICSRESPONSE=@[LIKETOPICSRESPONSE], "); 
         UnSQL.SQL.Add(@"   NOTLIKETOPICSRESPONSE=@[NOTLIKETOPICSRESPONSE] "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDTOPICSRESPONSEVOTES=@[IDTOPICSRESPONSEVOTES] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "TOPICSRESPONSEVOTES_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_TOPICSRESPONSEVOTES.js Persistence.Forums.Methods.TOPICSRESPONSEVOTES_UPD", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}


Persistence.Forums.Methods.TOPICSRESPONSEVOTES_UPDNOTLIKE = function (TOPICSRESPONSEVOTES) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDTOPICSRESPONSEVOTES.FieldName, TOPICSRESPONSEVOTES.IDTOPICSRESPONSEVOTES, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDTOPICSRESPONSE.FieldName, TOPICSRESPONSEVOTES.IDTOPICSRESPONSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDCMDBCI.FieldName, TOPICSRESPONSEVOTES.IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.NOTLIKETOPICSRESPONSE.FieldName, TOPICSRESPONSEVOTES.NOTLIKETOPICSRESPONSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "TOPICSRESPONSEVOTES_UPDNOTLIKE", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_TOPICSRESPONSEVOTES.js Persistence.Forums.Methods.TOPICSRESPONSEVOTES_UPDNOTLIKE", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

Persistence.Forums.Methods.TOPICSRESPONSEVOTES_UPDLIKE = function (TOPICSRESPONSEVOTES){
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDTOPICSRESPONSEVOTES.FieldName, TOPICSRESPONSEVOTES.IDTOPICSRESPONSEVOTES, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDTOPICSRESPONSE.FieldName, TOPICSRESPONSEVOTES.IDTOPICSRESPONSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDCMDBCI.FieldName, TOPICSRESPONSEVOTES.IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.LIKETOPICSRESPONSE.FieldName, TOPICSRESPONSEVOTES.LIKETOPICSRESPONSE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "TOPICSRESPONSEVOTES_UPDLIKE", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_TOPICSRESPONSEVOTES.js Persistence.Forums.Methods.TOPICSRESPONSEVOTES_UPDLIKE", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Forums.Methods.TOPICSRESPONSEVOTES_DEL = function (/*String StrIDTOPICSRESPONSEVOTES*/IDTOPICSRESPONSEVOTES) {
    var Res = -1;
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.Forums.Methods.TOPICSRESPONSEVOTES_DELIDTOPICSRESPONSEVOTES(/*String StrIDTOPICSRESPONSEVOTES*/Param);
    }
    else {
        Res = Persistence.Forums.Methods.TOPICSRESPONSEVOTES_DELTOPICSRESPONSEVOTES(Param);
    }
    return (Res);
}
Persistence.Forums.Methods.TOPICSRESPONSEVOTES_DELIDTOPICSRESPONSEVOTES = function (/*String StrIDTOPICSRESPONSEVOTES*/IDTOPICSRESPONSEVOTES) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDTOPICSRESPONSEVOTES == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDTOPICSRESPONSEVOTES.FieldName, " = " + UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.NAME_TABLE + "." + UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDTOPICSRESPONSEVOTES.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDTOPICSRESPONSEVOTES = " IN (" + StrIDTOPICSRESPONSEVOTES + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDTOPICSRESPONSEVOTES.FieldName, StrIDTOPICSRESPONSEVOTES, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDTOPICSRESPONSEVOTES.FieldName, IDTOPICSRESPONSEVOTES, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   TOPICSRESPONSEVOTES_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"TOPICSRESPONSEVOTES_DEL", @"TOPICSRESPONSEVOTES_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE TOPICSRESPONSEVOTES "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDTOPICSRESPONSEVOTES=@[IDTOPICSRESPONSEVOTES]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "TOPICSRESPONSEVOTES_DEL", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_TOPICSRESPONSEVOTES.js Persistence.Forums.Methods.TOPICSRESPONSEVOTES_DELIDTOPICSRESPONSEVOTES", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Forums.Methods.TOPICSRESPONSEVOTES_DELTOPICSRESPONSEVOTES = function (TOPICSRESPONSEVOTES/*TOPICSRESPONSEVOTESList*/) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        /*
        StrIDTOPICSRESPONSEVOTES = "";
        StrIDOTRO = "";
        for (var i = 0; i < TOPICSRESPONSEVOTESList.length; i++) {
        StrIDTOPICSRESPONSEVOTES += TOPICSRESPONSEVOTESList[i].IDTOPICSRESPONSEVOTES + ",";
        StrIDOTRO += TOPICSRESPONSEVOTESList[i].IDOTRO + ",";
        }
        StrIDTOPICSRESPONSEVOTES = StrIDTOPICSRESPONSEVOTES.substring(0, StrIDTOPICSRESPONSEVOTES.length - 1);
        StrIDOTRO = StrIDOTRO.substring(0, StrIDOTRO.length - 1);
        
        
        if (StrIDTOPICSRESPONSEVOTES == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDTOPICSRESPONSEVOTES.FieldName, " = " + UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.NAME_TABLE + "." + UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDTOPICSRESPONSEVOTES.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDTOPICSRESPONSEVOTES = " IN (" + StrIDTOPICSRESPONSEVOTES + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDTOPICSRESPONSEVOTES.FieldName, StrIDTOPICSRESPONSEVOTES, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/


        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDTOPICSRESPONSEVOTES.FieldName, TOPICSRESPONSEVOTES.IDTOPICSRESPONSEVOTES, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*    
        //********   TOPICSRESPONSEVOTES_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"TOPICSRESPONSEVOTES_DEL", @"TOPICSRESPONSEVOTES_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE TOPICSRESPONSEVOTES "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDTOPICSRESPONSEVOTES=@[IDTOPICSRESPONSEVOTES]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "TOPICSRESPONSEVOTES_DEL", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_TOPICSRESPONSEVOTES.js Persistence.Forums.Methods.TOPICSRESPONSEVOTES_DELTOPICSRESPONSEVOTES", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
//**********************   METHODS   ********************************************************************************************
Persistence.Forums.Methods.TOPICSRESPONSEVOTES_ListSetID = function (TOPICSRESPONSEVOTESList, IDTOPICSRESPONSEVOTES) {
    for (i = 0; i < TOPICSRESPONSEVOTESList.length; i++) {
        if (IDTOPICSRESPONSEVOTES == TOPICSRESPONSEVOTESList[i].IDTOPICSRESPONSEVOTES)
            return (TOPICSRESPONSEVOTESList[i]);
    }
    var UnTOPICSRESPONSEVOTES = new Persistence.Forums.Properties.TTOPICSRESPONSEVOTES();
    UnTOPICSRESPONSEVOTES.IDTOPICSRESPONSEVOTES = IDTOPICSRESPONSEVOTES;
    return (UnTOPICSRESPONSEVOTES);
}
Persistence.Forums.Methods.TOPICSRESPONSEVOTES_ListAdd = function (TOPICSRESPONSEVOTESList, TOPICSRESPONSEVOTES) {
    var i = Persistence.Forums.Methods.TOPICSRESPONSEVOTES_ListGetIndex(TOPICSRESPONSEVOTESList, TOPICSRESPONSEVOTES);
    if (i == -1) TOPICSRESPONSEVOTESList.push(TOPICSRESPONSEVOTES);
    else {
        TOPICSRESPONSEVOTESList[i].IDTOPICSRESPONSEVOTES = TOPICSRESPONSEVOTES.IDTOPICSRESPONSEVOTES;
        TOPICSRESPONSEVOTESList[i].IDTOPICSRESPONSE = TOPICSRESPONSEVOTES.IDTOPICSRESPONSE;
        TOPICSRESPONSEVOTESList[i].IDCMDBCI = TOPICSRESPONSEVOTES.IDCMDBCI;
        TOPICSRESPONSEVOTESList[i].LIKETOPICSRESPONSE = TOPICSRESPONSEVOTES.LIKETOPICSRESPONSE;
        TOPICSRESPONSEVOTESList[i].NOTLIKETOPICSRESPONSE = TOPICSRESPONSEVOTES.NOTLIKETOPICSRESPONSE;

    }
}
Persistence.Forums.Methods.TOPICSRESPONSEVOTES_ListGetIndex = function (TOPICSRESPONSEVOTESList, Param) {
    var Res = -1;
    if (typeof (Param) == 'number') {
        Res = Persistence.Forums.Methods.TOPICSRESPONSEVOTES_ListGetIndexIDTOPICSRESPONSEVOTES(TOPICSRESPONSEVOTESList, Param);
    }
    else {
        Res = Persistence.Forums.Methods.TOPICSRESPONSEVOTES_ListGetIndexTOPICSRESPONSEVOTES(TOPICSRESPONSEVOTESList, Param);
    }
    return (Res);
}
Persistence.Forums.Methods.TOPICSRESPONSEVOTES_ListGetIndexTOPICSRESPONSEVOTES = function (TOPICSRESPONSEVOTESList, TOPICSRESPONSEVOTES) {
    for (i = 0; i < TOPICSRESPONSEVOTESList.length; i++) {
        if (TOPICSRESPONSEVOTES.IDTOPICSRESPONSEVOTES == TOPICSRESPONSEVOTESList[i].IDTOPICSRESPONSEVOTES)
            return (i);
    }
    return (-1);
}
Persistence.Forums.Methods.TOPICSRESPONSEVOTES_ListGetIndexIDTOPICSRESPONSEVOTES = function (TOPICSRESPONSEVOTESList, IDTOPICSRESPONSEVOTES) {
    for (i = 0; i < TOPICSRESPONSEVOTESList.length; i++) {
        if (IDTOPICSRESPONSEVOTES == TOPICSRESPONSEVOTESList[i].IDTOPICSRESPONSEVOTES)
            return (i);
    }
    return (-1);
}
//STRING FUNCTION 
Persistence.Forums.Methods.TOPICSRESPONSEVOTESListtoStr = function (TOPICSRESPONSEVOTESList) {
    var Res = Persistence.Properties._Version + "(1)0";
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(TOPICSRESPONSEVOTESList.length, Longitud, Texto);
        for (Counter = 0; Counter < TOPICSRESPONSEVOTESList.length; Counter++) {
            var UnTOPICSRESPONSEVOTES = TOPICSRESPONSEVOTESList[Counter];
            UnTOPICSRESPONSEVOTES.ToStr(Longitud, Texto);
        }

        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
        SysCfg.Log.Methods.WriteLog("DB_TOPICSRESPONSEVOTES.js Persistence.Forums.Methods.TOPICSRESPONSEVOTESListtoStr", e);
    }
    return Res;
}
Persistence.Forums.Methods.StrtoTOPICSRESPONSEVOTES = function (ProtocoloStr, TOPICSRESPONSEVOTESList) {

    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        TOPICSRESPONSEVOTESList.length = 0;
        if (Persistence.Forums.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.Length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnTOPICSRESPONSEVOTES = new Persistence.Properties.TTOPICSRESPONSEVOTES(); //Mode new row
                UnTOPICSRESPONSEVOTES.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Forums.TOPICSRESPONSEVOTES_ListAdd(TOPICSRESPONSEVOTESList, UnTOPICSRESPONSEVOTES);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
        SysCfg.Log.Methods.WriteLog("DB_TOPICSRESPONSEVOTES.js Persistence.Forums.Methods.StrtoTOPICSRESPONSEVOTES", e);

    }
    return (Res);
}
//SOCKET FUNCTION 
Persistence.Forums.Methods.TOPICSRESPONSEVOTESListToByte = function (TOPICSRESPONSEVOTESList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, TOPICSRESPONSEVOTESList.Count - idx);
    for (i = idx; i < TOPICSRESPONSEVOTESList.Count; i++) {
        TOPICSRESPONSEVOTESList[i].ToByte(MemStream);
    }
}
Persistence.Forums.Methods.ByteToTOPICSRESPONSEVOTESList = function (TOPICSRESPONSEVOTESList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnTOPICSRESPONSEVOTES = new Persistence.Properties.TTOPICSRESPONSEVOTES();
        UnTOPICSRESPONSEVOTES.ByteTo(MemStream);
        Methods.TOPICSRESPONSEVOTES_ListAdd(TOPICSRESPONSEVOTESList, UnTOPICSRESPONSEVOTES);
    }
}
