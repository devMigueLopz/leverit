//**********************   PROPERTIES   *****************************************************************************************
Persistence.Forums.Properties.TQUERYS = function () {
}
Persistence.Forums.Methods.TQUERYS_EXECUTE = function (newList, StrSQL) {
    Param = new SysCfg.Stream.Properties.TParam();
    var Message = null;
    var Status = null;
    try {
        Param.Inicialize();
        OpenDataSet = SysCfg.DB.SQL.Methods.OpenDataSet(StrSQL, Param.ToBytes());
        newList.length =0;
        if (OpenDataSet.ResErr.NotError) {
            if (OpenDataSet.DataSet.RecordCount > 0) {
                var DataSetQueryTemplate = OpenDataSet.DataSet;
                for (var i = 0; i < DataSetQueryTemplate.RecordCount; i++) {
                    var ObjectQueryTemplate = {};
                    for (var x = 0; x < DataSetQueryTemplate.Records[i].Fields.length; x++) {
                        var Column = DataSetQueryTemplate.Records[i].Fields[x].FieldDef.FieldName; 
                        var Value = DataSetQueryTemplate.Records[i].Fields[x].Value; 
                        ObjectQueryTemplate[Column] = Value;
                    }
                    newList.push(ObjectQueryTemplate);
                }
            }
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_QUERYS.js Persistence.Forums.Methods.TQUERYS_EXECUTE",e);
    }
    finally {
        Param.Destroy();
    }
    return (newList);
}
Persistence.Forums.Methods.TQUERYS_DEL = function (StrSQL) {
    Param = new SysCfg.Stream.Properties.TParam();
    result = false;   
    try {
        Param.Inicialize();
        OpenDataSet = SysCfg.DB.SQL.Methods.OpenDataSet(StrSQL, Param.ToBytes());
        result = OpenDataSet.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_QUERYS.js Persistence.Forums.Methods.TQUERYS_DEL", e);
    }
    finally {
        Param.Destroy();
    }
    return (result);
}
