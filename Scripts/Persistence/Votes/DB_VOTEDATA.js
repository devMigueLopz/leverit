//**********************   PROPERTIES   *****************************************************************************************
Persistence.Votes.Properties.TVOTEDATA = function() {
	this.IDVOTEDATA = 0;
	this.IDVOTE = 0;
	this.VOTEDATA_STARTDATE = new Date(1970, 0, 1, 0, 0, 0);
	this.VOTEDATA_FINALDATE = new Date(1970, 0, 1, 0, 0, 0);
	this.VOTEDATA_STATUS = "";

	// VIRTUALES
	this.VOTE = new Persistence.Votes.Properties.TVOTE();
	this.VOTEDATAUSERList = new Array();

	//Socket IO Properties
	this.ToByte = function(MemStream){
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDVOTEDATA); 
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDVOTE); 
		SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.VOTEDATA_STARTDATE); 
		SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.VOTEDATA_FINALDATE); 
		SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.VOTEDATA_STATUS); 
	}
	this.ByteTo = function(MemStream)
	{
		this.IDVOTEDATA = SysCfg.Stream.Methods.ReadStreamInt32(MemStream); 
		this.IDVOTE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream); 
		this.VOTEDATA_STARTDATE = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream); 
		this.VOTEDATA_FINALDATE = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream); 
		this.VOTEDATA_STATUS = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream); 

	}
	//Str IO Properties
	this.ToStr = function(Longitud,Texto)
	{
		SysCfg.Str.Protocol.WriteInt(this.IDVOTEDATA, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteInt(this.IDVOTE, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteDateTime(this.VOTEDATA_STARTDATE, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteDateTime(this.VOTEDATA_FINALDATE, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteStr(this.VOTEDATA_STATUS, Longitud, Texto); 

	}
	this.StrTo = function(Pos,Index,LongitudArray,Texto)
	{
		this.IDVOTEDATA = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto); 
		this.IDVOTE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto); 
		this.VOTEDATA_STARTDATE = SysCfg.Str.Protocol.ReadDateTime(Pos, Index, LongitudArray, Texto); 
		this.VOTEDATA_FINALDATE = SysCfg.Str.Protocol.ReadDateTime(Pos, Index, LongitudArray, Texto); 
		this.VOTEDATA_STATUS = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto); 

	}
}

//**********************   METHODS server  ********************************************************************************************
Persistence.Votes.Methods.VOTEDATA_Fill = function(VOTEDATAList,StrIDVOTEDATA/*noin IDVOTEDATA*/)
{
	var ResErr = new SysCfg.Error.Properties.TResErr();
	VOTEDATAList.length = 0;
	var Param = new SysCfg.Stream.Properties.TParam();
	Param.Inicialize();
	if (StrIDVOTEDATA == "")
	{
		Param.AddUnknown(UsrCfg.InternoAtisNames.VOTEDATA.IDVOTEDATA.FieldName, " = " + UsrCfg.InternoAtisNames.VOTEDATA.NAME_TABLE + "." + UsrCfg.InternoAtisNames.VOTEDATA.IDVOTEDATA.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
	}
	else
	{
		StrIDVOTEDATA = " IN (" + StrIDVOTEDATA + ")";
		Param.AddUnknown(UsrCfg.InternoAtisNames.VOTEDATA.IDVOTEDATA.FieldName, StrIDVOTEDATA, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
	}
	//noin Param.AddInt32(UsrCfg.InternoDemoNames.VOTEDATA.IDVOTEDATA.FieldName, IDVOTEDATA, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
	try
	{
		/*
		//********   VOTEDATA_GET   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"VOTEDATA_GET", @"VOTEDATA_GET description.");  
		UnSQL.SQL.Add(@"   SELECT "); 
		 UnSQL.SQL.Add(@"   IDVOTEDATA, "); 
		 UnSQL.SQL.Add(@"   IDVOTE, "); 
		 UnSQL.SQL.Add(@"   VOTEDATA_STARTDATE, "); 
		 UnSQL.SQL.Add(@"   VOTEDATA_FINALDATE, "); 
		 UnSQL.SQL.Add(@"   VOTEDATA_STATUS "); 
		 UnSQL.SQL.Add(@"   FROM VOTEDATA "); 
		 UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		var DS_VOTEDATA = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "VOTEDATA_GET", Param.ToBytes());
		ResErr = DS_VOTEDATA.ResErr;
		if (ResErr.NotError)
		{
			if (DS_VOTEDATA.DataSet.RecordCount > 0)
			{
				DS_VOTEDATA.DataSet.First();
				while (!(DS_VOTEDATA.DataSet.Eof))
				{
					var VOTEDATA = new Persistence.Votes.Properties.TVOTEDATA();
					VOTEDATA.IDVOTEDATA = DS_VOTEDATA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.VOTEDATA.IDVOTEDATA.FieldName).asInt32();
					VOTEDATA.IDVOTE = DS_VOTEDATA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.VOTEDATA.IDVOTE.FieldName).asInt32();
					VOTEDATA.VOTEDATA_STARTDATE = DS_VOTEDATA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.VOTEDATA.VOTEDATA_STARTDATE.FieldName).asDateTime();
					VOTEDATA.VOTEDATA_FINALDATE = DS_VOTEDATA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.VOTEDATA.VOTEDATA_FINALDATE.FieldName).asDateTime();
					VOTEDATA.VOTEDATA_STATUS = DS_VOTEDATA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.VOTEDATA.VOTEDATA_STATUS.FieldName).asString();
					 
					//Other
					VOTEDATAList.push(VOTEDATA);
					DS_VOTEDATA.DataSet.Next();
				}
			}
			else
			{
				DS_VOTEDATA.ResErr.NotError = false;
				DS_VOTEDATA.ResErr.Mesaje = "RECORDCOUNT = 0";
			}
		}
	}
	catch (e){
		SysCfg.Log.Methods.WriteLog("DB_VOTEDATA.js Persistence.Votes.Methods.VOTEDATA_Fill", e);
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.Votes.Methods.VOTEDATA_GETID = function(VOTEDATA)
{
	var ResErr = new SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try
	{
		Param.Inicialize();
		//Other
		Param.AddInt32(UsrCfg.InternoAtisNames.VOTEDATA.IDVOTEDATA.FieldName, VOTEDATA.IDVOTEDATA, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddInt32(UsrCfg.InternoAtisNames.VOTEDATA.IDVOTE.FieldName, VOTEDATA.IDVOTE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddDateTime(UsrCfg.InternoAtisNames.VOTEDATA.VOTEDATA_STARTDATE.FieldName, VOTEDATA.VOTEDATA_STARTDATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddDateTime(UsrCfg.InternoAtisNames.VOTEDATA.VOTEDATA_FINALDATE.FieldName, VOTEDATA.VOTEDATA_FINALDATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddString(UsrCfg.InternoAtisNames.VOTEDATA.VOTEDATA_STATUS.FieldName, VOTEDATA.VOTEDATA_STATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

		/*
		//********   VOTEDATA_GETID   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"VOTEDATA_GETID", @"VOTEDATA_GETID description.");  
		UnSQL.SQL.Add(@"   SELECT "); 
		 UnSQL.SQL.Add(@"   IDVOTEDATA "); 
		 UnSQL.SQL.Add(@"   FROM VOTEDATA "); 
		 UnSQL.SQL.Add(@"   Where "); 
		 UnSQL.SQL.Add(@"   IDVOTE=@[IDVOTE] And "); 
		 UnSQL.SQL.Add(@"   VOTEDATA_STARTDATE=@[VOTEDATA_STARTDATE] And "); 
		 UnSQL.SQL.Add(@"   VOTEDATA_FINALDATE=@[VOTEDATA_FINALDATE] And "); 
		 UnSQL.SQL.Add(@"   VOTEDATA_STATUS=@[VOTEDATA_STATUS] ");
		 UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		var DS_VOTEDATA = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "VOTEDATA_GETID", Param.ToBytes());
		ResErr = DS_VOTEDATA.ResErr;
		if (ResErr.NotError)
		{
			if (DS_VOTEDATA.DataSet.RecordCount > 0)
			{
			    DS_VOTEDATA.DataSet.First();
			    VOTEDATA.IDVOTEDATA = DS_VOTEDATA.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.VOTEDATA.IDVOTEDATA.FieldName).asInt32();
			}
			else
			{
			    DS_VOTEDATA.ResErr.NotError = false;
			    DS_VOTEDATA.ResErr.Mesaje = "RECORDCOUNT = 0";
			}
		}
	}
	catch (e){
		SysCfg.Log.Methods.WriteLog("DB_VOTEDATA.js Persistence.Votes.Methods.VOTEDATA_GETID", e);
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.Votes.Methods.VOTEDATA_ADD = function(VOTEDATA)
{
	var ResErr = new SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try
	{
		Param.Inicialize();
		//Other
		Param.AddInt32(UsrCfg.InternoAtisNames.VOTEDATA.IDVOTEDATA.FieldName, VOTEDATA.IDVOTEDATA, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.VOTEDATA.IDVOTE.FieldName, VOTEDATA.IDVOTE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddDateTime(UsrCfg.InternoAtisNames.VOTEDATA.VOTEDATA_STARTDATE.FieldName, VOTEDATA.VOTEDATA_STARTDATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddDateTime(UsrCfg.InternoAtisNames.VOTEDATA.VOTEDATA_FINALDATE.FieldName, VOTEDATA.VOTEDATA_FINALDATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.VOTEDATA.VOTEDATA_STATUS.FieldName, VOTEDATA.VOTEDATA_STATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		 
		/* 
		//********   VOTEDATA_ADD   ************************* 
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"VOTEDATA_ADD", @"VOTEDATA_ADD description."); 
		UnSQL.SQL.Add(@"   INSERT INTO VOTEDATA( "); 
		 UnSQL.SQL.Add(@"   IDVOTE, "); 
		 UnSQL.SQL.Add(@"   VOTEDATA_STARTDATE, "); 
		 UnSQL.SQL.Add(@"   VOTEDATA_FINALDATE, "); 
		 UnSQL.SQL.Add(@"   VOTEDATA_STATUS "); 
		 UnSQL.SQL.Add(@"   ) Values ( ");  
		UnSQL.SQL.Add(@"   @[IDVOTE], "); 
		 UnSQL.SQL.Add(@"   @[VOTEDATA_STARTDATE], "); 
		 UnSQL.SQL.Add(@"   @[VOTEDATA_FINALDATE], "); 
		 UnSQL.SQL.Add(@"   @[VOTEDATA_STATUS] "); 
		 UnSQL.SQL.Add(@"   )  "); 
		UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "VOTEDATA_ADD", Param.ToBytes());
		ResErr = Exec.ResErr;
		if (ResErr.NotError)
		{
			Persistence.Votes.Methods.VOTEDATA_GETID(VOTEDATA);
		}
	}
	catch (e){
		SysCfg.Log.Methods.WriteLog("DB_VOTEDATA.js Persistence.Votes.Methods.VOTEDATA_ADD", e);
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.Votes.Methods.VOTEDATA_UPD = function(VOTEDATA)
{
	var ResErr = SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try
	{
		Param.Inicialize();
		Param.AddInt32(UsrCfg.InternoAtisNames.VOTEDATA.IDVOTEDATA.FieldName, VOTEDATA.IDVOTEDATA, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.VOTEDATA.IDVOTE.FieldName, VOTEDATA.IDVOTE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddDateTime(UsrCfg.InternoAtisNames.VOTEDATA.VOTEDATA_STARTDATE.FieldName, VOTEDATA.VOTEDATA_STARTDATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddDateTime(UsrCfg.InternoAtisNames.VOTEDATA.VOTEDATA_FINALDATE.FieldName, VOTEDATA.VOTEDATA_FINALDATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.VOTEDATA.VOTEDATA_STATUS.FieldName, VOTEDATA.VOTEDATA_STATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		 

		/*   
		//********   VOTEDATA_UPD   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"VOTEDATA_UPD", @"VOTEDATA_UPD description.");  
		UnSQL.SQL.Add(@"  UPDATE VOTEDATA Set "); 
		 UnSQL.SQL.Add(@"   IDVOTE=@[IDVOTE], "); 
		 UnSQL.SQL.Add(@"   VOTEDATA_STARTDATE=@[VOTEDATA_STARTDATE], "); 
		 UnSQL.SQL.Add(@"   VOTEDATA_FINALDATE=@[VOTEDATA_FINALDATE], "); 
		 UnSQL.SQL.Add(@"   VOTEDATA_STATUS=@[VOTEDATA_STATUS] "); 
		 UnSQL.SQL.Add(@"   Where "); 
		 UnSQL.SQL.Add(@"   IDVOTEDATA=@[IDVOTEDATA] ");
		 UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "VOTEDATA_UPD", Param.ToBytes());
		ResErr = Exec.ResErr;
		if (ResErr.NotError)
		{
			//GET(); 
		}
	}
	catch (e){
		SysCfg.Log.Methods.WriteLog("DB_VOTEDATA.js Persistence.Votes.Methods.VOTEDATA_UPD", e);
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
	}
Persistence.Votes.Methods.VOTEDATA_DEL = function(/*String StrIDVOTEDATA*/IDVOTEDATA)
{
	var ResErr = SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try
	{
		Param.Inicialize();
		/*if (StrIDVOTEDATA == "")
		{
		Param.AddUnknown(UsrCfg.InternoAtisNames.VOTEDATA.IDVOTEDATA.FieldName, " = " + UsrCfg.InternoAtisNames.VOTEDATA.NAME_TABLE + "." + UsrCfg.InternoAtisNames.VOTEDATA.IDVOTEDATA.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
		}
		else
		{
		StrIDVOTEDATA = " IN (" + StrIDVOTEDATA + ")";
		Param.AddUnknown(UsrCfg.InternoAtisNames.VOTEDATA.IDVOTEDATA.FieldName, StrIDVOTEDATA, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		}*/
		Param.AddInt32(UsrCfg.InternoAtisNames.VOTEDATA.IDVOTEDATA.FieldName, IDVOTEDATA, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		/*
		//********   VOTEDATA_DEL   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"VOTEDATA_DEL", @"VOTEDATA_DEL description.");  
		UnSQL.SQL.Add(@"   DELETE VOTEDATA "); 
		 UnSQL.SQL.Add(@"   Where "); 
		 UnSQL.SQL.Add(@"   IDVOTEDATA=@[IDVOTEDATA]   ");
		 UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "VOTEDATA_DEL", Param.ToBytes());
		ResErr = Exec.ResErr;
	}
	catch (e){
		SysCfg.Log.Methods.WriteLog("DB_VOTEDATA.js Persistence.Votes.Methods.VOTEDATA_DEL", e);
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}


//**********************   METHODS   ********************************************************************************************
Persistence.Votes.Methods.VOTEDATA_ListSetID = function(VOTEDATAList,IDVOTEDATA)
{
	for (i = 0; i < VOTEDATAList.length; i++)
	{
		if (IDVOTEDATA == VOTEDATAList[i].IDVOTEDATA)
			return (VOTEDATAList[i]);
	}
	var UnVOTEDATA = new Persistence.Properties.TVOTEDATA;
	UnVOTEDATA.IDVOTEDATA = IDVOTEDATA; 
	return (UnVOTEDATA);
}
Persistence.Votes.Methods.VOTEDATA_ListAdd = function(VOTEDATAList,VOTEDATA)
{
	var i = Persistence.Votes.Methods.VOTEDATA_ListGetIndex(VOTEDATAList, VOTEDATA);
	if (i == -1) VOTEDATAList.push(VOTEDATA);
	else
	{
		VOTEDATAList[i].IDVOTEDATA = VOTEDATA.IDVOTEDATA;
		VOTEDATAList[i].IDVOTE = VOTEDATA.IDVOTE;
		VOTEDATAList[i].VOTEDATA_STARTDATE = VOTEDATA.VOTEDATA_STARTDATE;
		VOTEDATAList[i].VOTEDATA_FINALDATE = VOTEDATA.VOTEDATA_FINALDATE;
		VOTEDATAList[i].VOTEDATA_STATUS = VOTEDATA.VOTEDATA_STATUS;

	}
}
Persistence.Votes.Methods.VOTEDATA_ListGetIndex = function(VOTEDATAList, VOTEDATA)
{
	for (i = 0; i < VOTEDATAList.length; i++)
	{
		if (VOTEDATA.IDVOTEDATA == VOTEDATAList[i].IDVOTEDATA)
			return (i);
	}
	return (-1);
}
Persistence.Votes.Methods.VOTEDATA_ListGetIndex = function(VOTEDATAList, IDVOTEDATA)
{
	for (i = 0; i < VOTEDATAList.length; i++)
	{
		if (IDVOTEDATA == VOTEDATAList[i].IDVOTEDATA)
			return (i);
	}
	return (-1);
}

//String Function 
Persistence.Votes.Methods.VOTEDATAListtoStr = function(VOTEDATAList)
{
	var Res = Persistence.Properties._Version + "(1)0";
	var Longitud = new SysCfg.ref("");
	var Texto = new SysCfg.ref("");
	try
	{
		SysCfg.Str.Protocol.WriteInt(VOTEDATAList.length,Longitud,Texto);
		for (Counter = 0; Counter < VOTEDATAList.length; Counter++)
		{
			var UnVOTEDATA = VOTEDATAList[Counter];
			UnVOTEDATA.ToStr(Longitud,Texto);
		}

		Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
		Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
	}
	catch (e)
	{
		var ResErr = new SysCfg.Error.Properties.TResErr();
		ResErr.NotError = false;
		ResErr.Mesaje = "LIFESTATUStoStr:" + e;
	}
	return Res;
}
Persistence.Votes.Methods.StrtoVOTEDATA = function(ProtocoloStr, VOTEDATAList)
{

	var Res = false;
	var Longitud, Texto;
	var Index =  new SysCfg.ref(0);
	var Pos =  new SysCfg.ref(0);
	try
	{
		VOTEDATAList.length = 0;   
		if (Persistence.Votes.Properties._Version == ProtocoloStr.substring(0, 3))
		{
			Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")") - ProtocoloStr.indexOf("(") - 1);
			Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.Length - ProtocoloStr.indexOf(")") - 1);
			LongitudArray = Longitud.split(",");
			LSCount = SysCfg.Str.Protocol.ReadInt(Pos,Index,LongitudArray,Texto);
			for (i = 0; i < LSCount; i++)
			{
			    UnVOTEDATA = new Persistence.Properties.TVOTEDATA(); //Mode new row
			    UnVOTEDATA.StrTo(Pos,Index, LongitudArray, Texto);
			    Persistence.Votes.VOTEDATA_ListAdd(VOTEDATAList, UnVOTEDATA);
			}
		}
	}
	catch (e)
	{
		var ResErr = SysCfg.Error.Properties.TResErr;
		ResErr.NotError = false;
		ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
	}
	return (Res);
}


//Socket Function 
Persistence.Votes.Methods.VOTEDATAListToByte = function(VOTEDATAList, MemStream, idx)
{
	SysCfg.Stream.Methods.WriteStreamInt32(MemStream, VOTEDATAList.Count-idx);
	for (i = idx; i < VOTEDATAList.Count; i++)
	{
		VOTEDATAList[i].ToByte(MemStream);
	}
}
Persistence.Votes.Methods.ByteToVOTEDATAList = function (VOTEDATAList, MemStream)
{
	Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
	for (i = 0; i < Count; i++)
	{
		UnVOTEDATA = new Persistence.Properties.TVOTEDATA();
		UnVOTEDATA.ByteTo(MemStream);
		Methods.VOTEDATA_ListAdd(VOTEDATAList,UnVOTEDATA);
	}
}
