Persistence.Votes.Methods.StartReation = function (PersistenceProfiler, IDVOTE) {
	try{
		var DB_VOTEListOrder = SysCfg.CopyList(PersistenceProfiler.VOTEList).sort(function (a, b) { return a.IDVOTE - b.IDVOTE; });
		var DB_VOTEOPTIONListOrder = SysCfg.CopyList(PersistenceProfiler.VOTEOPTIONList).sort(function (a, b) { return a.IDVOTE - b.IDVOTE; });
		var idxOption = 0;
		var DB_VOTEDATAListOrder = SysCfg.CopyList(PersistenceProfiler.VOTEDATAList).sort(function (a, b) { return a.IDVOTE - b.IDVOTE; });
		var idxData = 0;
		for (var i = 0; i < DB_VOTEListOrder.length; i++) {
			for (var x = idxOption; x < DB_VOTEOPTIONListOrder.length; x++) {
				idxOption = x;
				if (DB_VOTEOPTIONListOrder[x].IDVOTE == DB_VOTEListOrder[i].IDVOTE) {
					DB_VOTEListOrder[i].VOTEOPTIONList.push(DB_VOTEOPTIONListOrder[x]);
					DB_VOTEOPTIONListOrder[x].VOTE = DB_VOTEListOrder[i];
				}
				if (DB_VOTEOPTIONListOrder[x].IDVOTE > DB_VOTEListOrder[i].IDVOTE) {
					break;
				}
			}
			for (var x = idxData; x < DB_VOTEDATAListOrder.length; x++) {
				idxData = x;
				if (DB_VOTEDATAListOrder[x].IDVOTE == DB_VOTEListOrder[i].IDVOTE) {
					DB_VOTEListOrder[i].VOTEDATAList.push(DB_VOTEDATAListOrder[x]);
					DB_VOTEDATAListOrder[x].VOTE = DB_VOTEListOrder[i];
				}
				if (DB_VOTEDATAListOrder[x].IDVOTE > DB_VOTEListOrder[i].IDVOTE) {
					break;
				}
			}
		}

		DB_VOTEOPTIONListOrder = SysCfg.CopyList(PersistenceProfiler.VOTEOPTIONList).sort(function (a, b) { return a.IDVOTEOPTION - b.IDVOTEOPTION; });
		var DB_VOTEDATAUSERListOrder = SysCfg.CopyList(PersistenceProfiler.VOTEDATAUSERList).sort(function (a, b) { return a.IDVOTEOPTION - b.IDVOTEOPTION; });
		var idxDataUser = 0;
		for (var i = 0; i < DB_VOTEOPTIONListOrder.length; i++) {
			for (var x = idxDataUser; x < DB_VOTEDATAUSERListOrder.length; x++) {
				idxDataUser = x;
				if(DB_VOTEDATAUSERListOrder[x].IDVOTEOPTION == DB_VOTEOPTIONListOrder[i].IDVOTEOPTION){
					DB_VOTEOPTIONListOrder[i].VOTEDATAUSERList.push(DB_VOTEDATAUSERListOrder[x]);
					DB_VOTEDATAUSERListOrder[x].VOTEOPTION = DB_VOTEOPTIONListOrder[i];
				}
				if(DB_VOTEDATAUSERListOrder[x].IDVOTEOPTION > DB_VOTEOPTIONListOrder[i].IDVOTEOPTION){
					break;
				}
			}
		}



		DB_VOTEDATAListOrder = SysCfg.CopyList(PersistenceProfiler.VOTEDATAList).sort(function (a, b) { return a.IDVOTEDATA - b.IDVOTEDATA; });
		DB_VOTEDATAUSERListOrder = SysCfg.CopyList(PersistenceProfiler.VOTEDATAUSERList).sort(function (a, b) { return a.IDVOTEDATA - b.IDVOTEDATA; });
		idxDataUser = 0;
		for(var i = 0; i < DB_VOTEDATAListOrder.length; i++){
			for(var x = idxDataUser; x < DB_VOTEDATAUSERListOrder.length; x++){
				idxDataUser = x;	
				if(DB_VOTEDATAUSERListOrder[x].IDVOTEDATA == DB_VOTEDATAListOrder[i].IDVOTEDATA){
					DB_VOTEDATAListOrder[i].VOTEDATAUSERList.push(DB_VOTEDATAUSERListOrder[x]);
					DB_VOTEDATAUSERListOrder[x].VOTEDATA = DB_VOTEDATAListOrder[i];
				}
				if(DB_VOTEDATAUSERListOrder[x].IDVOTEDATA > DB_VOTEDATAListOrder[i].IDVOTEDATA){
					break;
				}
			}
		}
	}
	catch (e) {
		SysCfg.Log.Methods.WriteLog("VotesMethods.js Persistence.Votes.Methods.StartReation ", e);
	}
}