//**********************   PROPERTIES   *****************************************************************************************
Persistence.Votes.Properties.TVOTE = function() {
	this.IDVOTE = 0;
	this.VOTE_NAME = "";
	this.VOTE_GROUP = 0;
	this.VOTE_QUESTION = "";
	this.VOTE_COMMENTREQUIRED = false;
	this.VOTE_PROGRESSCOLOR = "";
	this.VOTE_PROGRESSTITLE = "";
	this.VOTE_TOTALPROGRESSTITLE = "";
	this.VOTE_INITGRAPHIC = 0;
	this.VOTE_SHOWCOMMENT = false;
	this.VOTE_SHOWCOMMENTGROUP = false;

	//VIRTUALES
	this.VOTEOPTIONList = new Array();
	this.VOTEDATAList = new Array();

	//Socket IO Properties
	this.ToByte = function(MemStream){
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDVOTE); 
		SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.VOTE_NAME); 
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.VOTE_GROUP); 
		SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.VOTE_QUESTION); 
		SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.VOTE_COMMENTREQUIRED); 
		SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.VOTE_PROGRESSCOLOR); 
		SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.VOTE_PROGRESSTITLE); 
		SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.VOTE_TOTALPROGRESSTITLE); 
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.VOTE_INITGRAPHIC); 
		SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.VOTE_SHOWCOMMENT); 
		SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.VOTE_SHOWCOMMENTGROUP); 

	}
	this.ByteTo = function(MemStream)
	{
		this.IDVOTE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream); 
		this.VOTE_NAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream); 
		this.VOTE_GROUP = SysCfg.Stream.Methods.ReadStreamInt32(MemStream); 
		this.VOTE_QUESTION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream); 
		this.VOTE_COMMENTREQUIRED = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream); 
		this.VOTE_PROGRESSCOLOR = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream); 
		this.VOTE_PROGRESSTITLE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream); 
		this.VOTE_TOTALPROGRESSTITLE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream); 
		this.VOTE_INITGRAPHIC = SysCfg.Stream.Methods.ReadStreamInt32(MemStream); 
		this.VOTE_SHOWCOMMENT = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream); 
		this.VOTE_SHOWCOMMENTGROUP = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream); 

	}
	//Str IO Properties
	this.ToStr = function(Longitud,Texto)
	{
		SysCfg.Str.Protocol.WriteInt(this.IDVOTE, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteStr(this.VOTE_NAME, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteInt(this.VOTE_GROUP, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteStr(this.VOTE_QUESTION, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteBool(this.VOTE_COMMENTREQUIRED, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteStr(this.VOTE_PROGRESSCOLOR, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteStr(this.VOTE_PROGRESSTITLE, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteStr(this.VOTE_TOTALPROGRESSTITLE, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteInt(this.VOTE_INITGRAPHIC, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteBool(this.VOTE_SHOWCOMMENT, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteBool(this.VOTE_SHOWCOMMENTGROUP, Longitud, Texto); 

	}
	this.StrTo = function(Pos,Index,LongitudArray,Texto)
	{
		this.IDVOTE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto); 
		this.VOTE_NAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto); 
		this.VOTE_GROUP = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto); 
		this.VOTE_QUESTION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto); 
		this.VOTE_COMMENTREQUIRED = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto); 
		this.VOTE_PROGRESSCOLOR = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto); 
		this.VOTE_PROGRESSTITLE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto); 
		this.VOTE_TOTALPROGRESSTITLE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto); 
		this.VOTE_INITGRAPHIC = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto); 
		this.VOTE_SHOWCOMMENT = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto); 
		this.VOTE_SHOWCOMMENTGROUP = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto); 

	}
}

//**********************   METHODS server  ********************************************************************************************
Persistence.Votes.Methods.VOTE_Fill = function(VOTEList,StrIDVOTE/*noin IDVOTE*/)
{
	var ResErr = new SysCfg.Error.Properties.TResErr();
	VOTEList.length = 0;
	var Param = new SysCfg.Stream.Properties.TParam();
	Param.Inicialize();
	if (StrIDVOTE == "")
	{
		Param.AddUnknown(UsrCfg.InternoAtisNames.VOTE.IDVOTE.FieldName, " = " + UsrCfg.InternoAtisNames.VOTE.NAME_TABLE + "." + UsrCfg.InternoAtisNames.VOTE.IDVOTE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
	}
	else
	{
		StrIDVOTE = " IN (" + StrIDVOTE + ")";
		Param.AddUnknown(UsrCfg.InternoAtisNames.VOTE.IDVOTE.FieldName, StrIDVOTE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
	}
	//noin Param.AddInt32(UsrCfg.InternoDemoNames.VOTE.IDVOTE.FieldName, IDVOTE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
	try
	{
		/*
		//********   VOTE_GET   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"VOTE_GET", @"VOTE_GET description.");  
		UnSQL.SQL.Add(@"   SELECT "); 
		 UnSQL.SQL.Add(@"   IDVOTE, "); 
		 UnSQL.SQL.Add(@"   VOTE_NAME, "); 
		 UnSQL.SQL.Add(@"   VOTE_GROUP, "); 
		 UnSQL.SQL.Add(@"   VOTE_QUESTION, "); 
		 UnSQL.SQL.Add(@"   VOTE_COMMENTREQUIRED, "); 
		 UnSQL.SQL.Add(@"   VOTE_PROGRESSCOLOR, "); 
		 UnSQL.SQL.Add(@"   VOTE_PROGRESSTITLE, "); 
		 UnSQL.SQL.Add(@"   VOTE_TOTALPROGRESSTITLE, "); 
		 UnSQL.SQL.Add(@"   VOTE_INITGRAPHIC, "); 
		 UnSQL.SQL.Add(@"   VOTE_SHOWCOMMENT, "); 
		 UnSQL.SQL.Add(@"   VOTE_SHOWCOMMENTGROUP "); 
		 UnSQL.SQL.Add(@"   FROM VOTE "); 
		 UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		var DS_VOTE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "VOTE_GET", Param.ToBytes());
		ResErr = DS_VOTE.ResErr;
		if (ResErr.NotError)
		{
			if (DS_VOTE.DataSet.RecordCount > 0)
			{
				DS_VOTE.DataSet.First();
				while (!(DS_VOTE.DataSet.Eof))
				{
					var VOTE = new Persistence.Votes.Properties.TVOTE();
					VOTE.IDVOTE = DS_VOTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.VOTE.IDVOTE.FieldName).asInt32();
					VOTE.VOTE_NAME = DS_VOTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.VOTE.VOTE_NAME.FieldName).asString();
					VOTE.VOTE_GROUP = DS_VOTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.VOTE.VOTE_GROUP.FieldName).asInt32();
					VOTE.VOTE_QUESTION = DS_VOTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.VOTE.VOTE_QUESTION.FieldName).asString();
					VOTE.VOTE_COMMENTREQUIRED = DS_VOTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.VOTE.VOTE_COMMENTREQUIRED.FieldName).asBoolean();
					VOTE.VOTE_PROGRESSCOLOR = DS_VOTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.VOTE.VOTE_PROGRESSCOLOR.FieldName).asString();
					VOTE.VOTE_PROGRESSTITLE = DS_VOTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.VOTE.VOTE_PROGRESSTITLE.FieldName).asString();
					VOTE.VOTE_TOTALPROGRESSTITLE = DS_VOTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.VOTE.VOTE_TOTALPROGRESSTITLE.FieldName).asString();
					VOTE.VOTE_INITGRAPHIC = DS_VOTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.VOTE.VOTE_INITGRAPHIC.FieldName).asInt32();
					VOTE.VOTE_SHOWCOMMENT = DS_VOTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.VOTE.VOTE_SHOWCOMMENT.FieldName).asBoolean();
					VOTE.VOTE_SHOWCOMMENTGROUP = DS_VOTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.VOTE.VOTE_SHOWCOMMENTGROUP.FieldName).asBoolean();
					 
					//Other
					VOTEList.push(VOTE);
					DS_VOTE.DataSet.Next();
				}
			}
			else
			{
				DS_VOTE.ResErr.NotError = false;
				DS_VOTE.ResErr.Mesaje = "RECORDCOUNT = 0";
			}
		}
	}
	catch (e){
		SysCfg.Log.Methods.WriteLog("DB_VOTE.js Persistence.Votes.Methods.VOTE_Fill", e);
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.Votes.Methods.VOTE_GETID = function(VOTE)
{
	var ResErr = new SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try
	{
		Param.Inicialize();
		//Other
		Param.AddInt32(UsrCfg.InternoAtisNames.VOTE.IDVOTE.FieldName, VOTE.IDVOTE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddString(UsrCfg.InternoAtisNames.VOTE.VOTE_NAME.FieldName, VOTE.VOTE_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddInt32(UsrCfg.InternoAtisNames.VOTE.VOTE_GROUP.FieldName, VOTE.VOTE_GROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddString(UsrCfg.InternoAtisNames.VOTE.VOTE_QUESTION.FieldName, VOTE.VOTE_QUESTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddBoolean(UsrCfg.InternoAtisNames.VOTE.VOTE_COMMENTREQUIRED.FieldName, VOTE.VOTE_COMMENTREQUIRED, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddString(UsrCfg.InternoAtisNames.VOTE.VOTE_PROGRESSCOLOR.FieldName, VOTE.VOTE_PROGRESSCOLOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddString(UsrCfg.InternoAtisNames.VOTE.VOTE_PROGRESSTITLE.FieldName, VOTE.VOTE_PROGRESSTITLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddString(UsrCfg.InternoAtisNames.VOTE.VOTE_TOTALPROGRESSTITLE.FieldName, VOTE.VOTE_TOTALPROGRESSTITLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddInt32(UsrCfg.InternoAtisNames.VOTE.VOTE_INITGRAPHIC.FieldName, VOTE.VOTE_INITGRAPHIC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddBoolean(UsrCfg.InternoAtisNames.VOTE.VOTE_SHOWCOMMENT.FieldName, VOTE.VOTE_SHOWCOMMENT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddBoolean(UsrCfg.InternoAtisNames.VOTE.VOTE_SHOWCOMMENTGROUP.FieldName, VOTE.VOTE_SHOWCOMMENTGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

		/*
		//********   VOTE_GETID   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"VOTE_GETID", @"VOTE_GETID description.");  
		UnSQL.SQL.Add(@"   SELECT "); 
		 UnSQL.SQL.Add(@"   IDVOTE "); 
		 UnSQL.SQL.Add(@"   FROM VOTE "); 
		 UnSQL.SQL.Add(@"   Where "); 
		 UnSQL.SQL.Add(@"   VOTE_NAME=@[VOTE_NAME] And "); 
		 UnSQL.SQL.Add(@"   VOTE_GROUP=@[VOTE_GROUP] And "); 
		 UnSQL.SQL.Add(@"   VOTE_QUESTION=@[VOTE_QUESTION] And "); 
		 UnSQL.SQL.Add(@"   VOTE_COMMENTREQUIRED=@[VOTE_COMMENTREQUIRED] And "); 
		 UnSQL.SQL.Add(@"   VOTE_PROGRESSCOLOR=@[VOTE_PROGRESSCOLOR] And "); 
		 UnSQL.SQL.Add(@"   VOTE_PROGRESSTITLE=@[VOTE_PROGRESSTITLE] And "); 
		 UnSQL.SQL.Add(@"   VOTE_TOTALPROGRESSTITLE=@[VOTE_TOTALPROGRESSTITLE] And "); 
		 UnSQL.SQL.Add(@"   VOTE_INITGRAPHIC=@[VOTE_INITGRAPHIC] And "); 
		 UnSQL.SQL.Add(@"   VOTE_SHOWCOMMENT=@[VOTE_SHOWCOMMENT] And "); 
		 UnSQL.SQL.Add(@"   VOTE_SHOWCOMMENTGROUP=@[VOTE_SHOWCOMMENTGROUP] ");
		 UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		var DS_VOTE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "VOTE_GETID", Param.ToBytes());
		ResErr = DS_VOTE.ResErr;
		if (ResErr.NotError)
		{
			if (DS_VOTE.DataSet.RecordCount > 0)
			{
				DS_VOTE.DataSet.First();
				VOTE.IDVOTE = DS_VOTE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.VOTE.IDVOTE.FieldName).asInt32();
			}
			else
			{
				DS_VOTE.ResErr.NotError = false;
				DS_VOTE.ResErr.Mesaje = "RECORDCOUNT = 0";
			}
		}
	}
	catch (e){
		SysCfg.Log.Methods.WriteLog("DB_VOTE.js Persistence.Votes.Methods.VOTE_GETID", e);
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.Votes.Methods.VOTE_ADD = function(VOTE)
{
	var ResErr = new SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try
	{
		Param.Inicialize();
		//Other
		Param.AddInt32(UsrCfg.InternoAtisNames.VOTE.IDVOTE.FieldName, VOTE.IDVOTE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.VOTE.VOTE_NAME.FieldName, VOTE.VOTE_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.VOTE.VOTE_GROUP.FieldName, VOTE.VOTE_GROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.VOTE.VOTE_QUESTION.FieldName, VOTE.VOTE_QUESTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddBoolean(UsrCfg.InternoAtisNames.VOTE.VOTE_COMMENTREQUIRED.FieldName, VOTE.VOTE_COMMENTREQUIRED, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.VOTE.VOTE_PROGRESSCOLOR.FieldName, VOTE.VOTE_PROGRESSCOLOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.VOTE.VOTE_PROGRESSTITLE.FieldName, VOTE.VOTE_PROGRESSTITLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.VOTE.VOTE_TOTALPROGRESSTITLE.FieldName, VOTE.VOTE_TOTALPROGRESSTITLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.VOTE.VOTE_INITGRAPHIC.FieldName, VOTE.VOTE_INITGRAPHIC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddBoolean(UsrCfg.InternoAtisNames.VOTE.VOTE_SHOWCOMMENT.FieldName, VOTE.VOTE_SHOWCOMMENT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddBoolean(UsrCfg.InternoAtisNames.VOTE.VOTE_SHOWCOMMENTGROUP.FieldName, VOTE.VOTE_SHOWCOMMENTGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		 
		/* 
		//********   VOTE_ADD   ************************* 
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"VOTE_ADD", @"VOTE_ADD description."); 
		UnSQL.SQL.Add(@"   INSERT INTO VOTE( "); 
		 UnSQL.SQL.Add(@"   VOTE_NAME, "); 
		 UnSQL.SQL.Add(@"   VOTE_GROUP, "); 
		 UnSQL.SQL.Add(@"   VOTE_QUESTION, "); 
		 UnSQL.SQL.Add(@"   VOTE_COMMENTREQUIRED, "); 
		 UnSQL.SQL.Add(@"   VOTE_PROGRESSCOLOR, "); 
		 UnSQL.SQL.Add(@"   VOTE_PROGRESSTITLE, "); 
		 UnSQL.SQL.Add(@"   VOTE_TOTALPROGRESSTITLE, "); 
		 UnSQL.SQL.Add(@"   VOTE_INITGRAPHIC, "); 
		 UnSQL.SQL.Add(@"   VOTE_SHOWCOMMENT, "); 
		 UnSQL.SQL.Add(@"   VOTE_SHOWCOMMENTGROUP "); 
		 UnSQL.SQL.Add(@"   ) Values ( ");  
		UnSQL.SQL.Add(@"   @[VOTE_NAME], "); 
		 UnSQL.SQL.Add(@"   @[VOTE_GROUP], "); 
		 UnSQL.SQL.Add(@"   @[VOTE_QUESTION], "); 
		 UnSQL.SQL.Add(@"   @[VOTE_COMMENTREQUIRED], "); 
		 UnSQL.SQL.Add(@"   @[VOTE_PROGRESSCOLOR], "); 
		 UnSQL.SQL.Add(@"   @[VOTE_PROGRESSTITLE], "); 
		 UnSQL.SQL.Add(@"   @[VOTE_TOTALPROGRESSTITLE], "); 
		 UnSQL.SQL.Add(@"   @[VOTE_INITGRAPHIC], "); 
		 UnSQL.SQL.Add(@"   @[VOTE_SHOWCOMMENT], "); 
		 UnSQL.SQL.Add(@"   @[VOTE_SHOWCOMMENTGROUP] "); 
		 UnSQL.SQL.Add(@"   )  "); 
		UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "VOTE_ADD", Param.ToBytes());
		ResErr = Exec.ResErr;
		if (ResErr.NotError)
		{
			Persistence.Votes.Methods.VOTE_GETID(VOTE);
		}
	}
	catch (e){
		SysCfg.Log.Methods.WriteLog("DB_VOTE.js Persistence.Votes.Methods.VOTE_ADD", e);
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.Votes.Methods.VOTE_UPD = function(VOTE)
{
	var ResErr = SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try
	{
		Param.Inicialize();
		Param.AddInt32(UsrCfg.InternoAtisNames.VOTE.IDVOTE.FieldName, VOTE.IDVOTE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.VOTE.VOTE_NAME.FieldName, VOTE.VOTE_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.VOTE.VOTE_GROUP.FieldName, VOTE.VOTE_GROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.VOTE.VOTE_QUESTION.FieldName, VOTE.VOTE_QUESTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddBoolean(UsrCfg.InternoAtisNames.VOTE.VOTE_COMMENTREQUIRED.FieldName, VOTE.VOTE_COMMENTREQUIRED, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.VOTE.VOTE_PROGRESSCOLOR.FieldName, VOTE.VOTE_PROGRESSCOLOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.VOTE.VOTE_PROGRESSTITLE.FieldName, VOTE.VOTE_PROGRESSTITLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.VOTE.VOTE_TOTALPROGRESSTITLE.FieldName, VOTE.VOTE_TOTALPROGRESSTITLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.VOTE.VOTE_INITGRAPHIC.FieldName, VOTE.VOTE_INITGRAPHIC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddBoolean(UsrCfg.InternoAtisNames.VOTE.VOTE_SHOWCOMMENT.FieldName, VOTE.VOTE_SHOWCOMMENT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddBoolean(UsrCfg.InternoAtisNames.VOTE.VOTE_SHOWCOMMENTGROUP.FieldName, VOTE.VOTE_SHOWCOMMENTGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		 

		/*   
		//********   VOTE_UPD   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"VOTE_UPD", @"VOTE_UPD description.");  
		UnSQL.SQL.Add(@"  UPDATE VOTE Set "); 
		 UnSQL.SQL.Add(@"   VOTE_NAME=@[VOTE_NAME], "); 
		 UnSQL.SQL.Add(@"   VOTE_GROUP=@[VOTE_GROUP], "); 
		 UnSQL.SQL.Add(@"   VOTE_QUESTION=@[VOTE_QUESTION], "); 
		 UnSQL.SQL.Add(@"   VOTE_COMMENTREQUIRED=@[VOTE_COMMENTREQUIRED], "); 
		 UnSQL.SQL.Add(@"   VOTE_PROGRESSCOLOR=@[VOTE_PROGRESSCOLOR], "); 
		 UnSQL.SQL.Add(@"   VOTE_PROGRESSTITLE=@[VOTE_PROGRESSTITLE], "); 
		 UnSQL.SQL.Add(@"   VOTE_TOTALPROGRESSTITLE=@[VOTE_TOTALPROGRESSTITLE], "); 
		 UnSQL.SQL.Add(@"   VOTE_INITGRAPHIC=@[VOTE_INITGRAPHIC], "); 
		 UnSQL.SQL.Add(@"   VOTE_SHOWCOMMENT=@[VOTE_SHOWCOMMENT], "); 
		 UnSQL.SQL.Add(@"   VOTE_SHOWCOMMENTGROUP=@[VOTE_SHOWCOMMENTGROUP] "); 
		 UnSQL.SQL.Add(@"   Where "); 
		 UnSQL.SQL.Add(@"   IDVOTE=@[IDVOTE] ");
		 UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "VOTE_UPD", Param.ToBytes());
		ResErr = Exec.ResErr;
		if (ResErr.NotError)
		{
		//GET(); 
		}
	}
	catch (e){
		SysCfg.Log.Methods.WriteLog("DB_VOTE.js Persistence.Votes.Methods.VOTE_UPD", e);
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.Votes.Methods.VOTE_DEL = function(/*String StrIDVOTE*/IDVOTE)
{
	var ResErr = SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try
	{
		Param.Inicialize();
		/*if (StrIDVOTE == "")
		{
		Param.AddUnknown(UsrCfg.InternoAtisNames.VOTE.IDVOTE.FieldName, " = " + UsrCfg.InternoAtisNames.VOTE.NAME_TABLE + "." + UsrCfg.InternoAtisNames.VOTE.IDVOTE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
		}
		else
		{
		StrIDVOTE = " IN (" + StrIDVOTE + ")";
		Param.AddUnknown(UsrCfg.InternoAtisNames.VOTE.IDVOTE.FieldName, StrIDVOTE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		}*/
		Param.AddInt32(UsrCfg.InternoAtisNames.VOTE.IDVOTE.FieldName, IDVOTE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		/*
		//********   VOTE_DEL   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"VOTE_DEL", @"VOTE_DEL description.");  
		UnSQL.SQL.Add(@"   DELETE VOTE "); 
		 UnSQL.SQL.Add(@"   Where "); 
		 UnSQL.SQL.Add(@"   IDVOTE=@[IDVOTE]   ");
		 UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "VOTE_DEL", Param.ToBytes());
		ResErr = Exec.ResErr;
	}
	catch (e){
		SysCfg.Log.Methods.WriteLog("DB_VOTE.js Persistence.Votes.Methods.VOTE_DEL", e);
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}

//**********************   METHODS   ********************************************************************************************
Persistence.Votes.Methods.VOTE_ListSetID = function(VOTEList,IDVOTE)
{
	for (i = 0; i < VOTEList.length; i++)
	{
		if (IDVOTE == VOTEList[i].IDVOTE)
		return (VOTEList[i]);
	}
	var UnVOTE = new Persistence.Properties.TVOTE;
	UnVOTE.IDVOTE = IDVOTE; 
	return (UnVOTE);
	}
Persistence.Votes.Methods.VOTE_ListAdd = function(VOTEList,VOTE)
{
	var i = Persistence.Votes.Methods.VOTE_ListGetIndex(VOTEList, VOTE);
	if (i == -1) VOTEList.push(VOTE);
	else
	{
		VOTEList[i].IDVOTE = VOTE.IDVOTE;
		VOTEList[i].VOTE_NAME = VOTE.VOTE_NAME;
		VOTEList[i].VOTE_GROUP = VOTE.VOTE_GROUP;
		VOTEList[i].VOTE_QUESTION = VOTE.VOTE_QUESTION;
		VOTEList[i].VOTE_COMMENTREQUIRED = VOTE.VOTE_COMMENTREQUIRED;
		VOTEList[i].VOTE_PROGRESSCOLOR = VOTE.VOTE_PROGRESSCOLOR;
		VOTEList[i].VOTE_PROGRESSTITLE = VOTE.VOTE_PROGRESSTITLE;
		VOTEList[i].VOTE_TOTALPROGRESSTITLE = VOTE.VOTE_TOTALPROGRESSTITLE;
		VOTEList[i].VOTE_INITGRAPHIC = VOTE.VOTE_INITGRAPHIC;
		VOTEList[i].VOTE_SHOWCOMMENT = VOTE.VOTE_SHOWCOMMENT;
		VOTEList[i].VOTE_SHOWCOMMENTGROUP = VOTE.VOTE_SHOWCOMMENTGROUP;

	}
}
Persistence.Votes.Methods.VOTE_ListGetIndex = function(VOTEList, VOTE)
{
	for (i = 0; i < VOTEList.length; i++)
	{
		if (VOTE.IDVOTE == VOTEList[i].IDVOTE)
			return (i);
	}
	return (-1);
}
Persistence.Votes.Methods.VOTE_ListGetIndex = function(VOTEList, IDVOTE)
{
	for (i = 0; i < VOTEList.length; i++)
	{
		if (IDVOTE == VOTEList[i].IDVOTE)
			return (i);
	}
	return (-1);
}

//String Function 
Persistence.Votes.Methods.VOTEListtoStr = function(VOTEList)
{
	var Res = Persistence.Properties._Version + "(1)0";
	var Longitud = new SysCfg.ref("");
	var Texto = new SysCfg.ref("");
	try
	{
		SysCfg.Str.Protocol.WriteInt(VOTEList.length,Longitud,Texto);
		for (Counter = 0; Counter < VOTEList.length; Counter++)
		{
			var UnVOTE = VOTEList[Counter];
			UnVOTE.ToStr(Longitud,Texto);
		}

		Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
		Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
	}
	catch (e)
	{
		var ResErr = new SysCfg.Error.Properties.TResErr();
		ResErr.NotError = false;
		ResErr.Mesaje = "LIFESTATUStoStr:" + e;
	}
	return Res;
}
Persistence.Votes.Methods.StrtoVOTE = function(ProtocoloStr, VOTEList)
{

	var Res = false;
	var Longitud, Texto;
	var Index =  new SysCfg.ref(0);
	var Pos =  new SysCfg.ref(0);
	try
	{
		VOTEList.length = 0;   
		if (Persistence.Votes.Properties._Version == ProtocoloStr.substring(0, 3))
		{
			Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")") - ProtocoloStr.indexOf("(") - 1);
			Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.Length - ProtocoloStr.indexOf(")") - 1);
			LongitudArray = Longitud.split(",");
			LSCount = SysCfg.Str.Protocol.ReadInt(Pos,Index,LongitudArray,Texto);
			for (i = 0; i < LSCount; i++)
			{
				UnVOTE = new Persistence.Properties.TVOTE(); //Mode new row
				UnVOTE.StrTo(Pos,Index, LongitudArray, Texto);
				Persistence.Votes.VOTE_ListAdd(VOTEList, UnVOTE);
			}
		}
	}
	catch (e)
	{
		var ResErr = SysCfg.Error.Properties.TResErr;
		ResErr.NotError = false;
		ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
	}
	return (Res);
}


//Socket Function 
Persistence.Votes.Methods.VOTEListToByte = function(VOTEList, MemStream, idx)
{
	SysCfg.Stream.Methods.WriteStreamInt32(MemStream, VOTEList.Count-idx);
	for (i = idx; i < VOTEList.Count; i++)
	{
		VOTEList[i].ToByte(MemStream);
	}
}
Persistence.Votes.Methods.ByteToVOTEList = function (VOTEList, MemStream)
{
	Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
	for (i = 0; i < Count; i++)
	{
		UnVOTE = new Persistence.Properties.TVOTE();
		UnVOTE.ByteTo(MemStream);
		Methods.VOTE_ListAdd(VOTEList,UnVOTE);
	}
}
