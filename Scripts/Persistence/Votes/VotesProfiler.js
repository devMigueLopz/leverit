Persistence.Votes.Properties._Version = '001';
Persistence.Votes.TVotesProfiler = function () {
	this.TParent = function () {
		return this;
	}.bind(this);
	var _this = this.TParent();
	ResErr = new SysCfg.Error.Properties.TResErr();
	this.VOTEList = new Array();
	this.VOTEOPTIONList = new Array();
	this.VOTEDATAList = new Array();
	this.VOTEDATAUSERList = new Array();

	this.Fill = function () {
		try{
			Persistence.Votes.Methods.VOTE_Fill(this.VOTEList, '');
			Persistence.Votes.Methods.VOTEOPTION_Fill(this.VOTEOPTIONList, '');
			Persistence.Votes.Methods.VOTEDATA_Fill(this.VOTEDATAList, '');
			Persistence.Votes.Methods.VOTEDATAUSER_Fill(this.VOTEDATAUSERList, '');
			Persistence.Votes.Methods.StartReation(this, -1);
		} catch (e) {
			SysCfg.Log.Methods.WriteLog("VotesProfiler.js Persistence.Votes.TVotesProfiler this.Fill", e);
		}
	}
	this.CreateAll = function () {
		try{
			var VOTE = new Persistence.Votes.Properties.TVOTE();
			VOTE.VOTE_NAME = 'ELECTORAL 1';
			VOTE.VOTE_GROUP = 23;
			VOTE.VOTE_QUESTION = 'You, who do you want to vote for?';
			VOTE.VOTE_COMMENTREQUIRED = true;
			VOTE.VOTE_PROGRESSCOLOR = 'rgb(255, 99, 80)';
			VOTE.VOTE_PROGRESSTITLE = 'Progress Votes';
			VOTE.VOTE_TOTALPROGRESSTITLE = 'Number Voters';
			VOTE.VOTE_INITGRAPHIC = 1;
			VOTE.VOTE_SHOWCOMMENT = true;
			VOTE.VOTE_SHOWCOMMENTGROUP = true;
			Persistence.Votes.Methods.VOTE_ADD(VOTE);

			var VOTEOPTION = new Persistence.Votes.Properties.TVOTEOPTION();
			VOTEOPTION.VOTEOPTION_NAME = 'MAURICIO';
			VOTEOPTION.VOTEOPTION_DESCRIPTION = 'OPTION 1';
			VOTEOPTION.IDVOTE = VOTE.IDVOTE;
			Persistence.Votes.Methods.VOTEOPTION_ADD(VOTEOPTION);

			VOTEOPTION = new Persistence.Votes.Properties.TVOTEOPTION();
			VOTEOPTION.VOTEOPTION_NAME = 'FELIPE';
			VOTEOPTION.VOTEOPTION_DESCRIPTION = 'OPTION 2';
			VOTEOPTION.IDVOTE = VOTE.IDVOTE;
			Persistence.Votes.Methods.VOTEOPTION_ADD(VOTEOPTION);

			VOTEOPTION = new Persistence.Votes.Properties.TVOTEOPTION();
			VOTEOPTION.VOTEOPTION_NAME = 'CARLOS';
			VOTEOPTION.VOTEOPTION_DESCRIPTION = 'OPTION 3';
			VOTEOPTION.IDVOTE = VOTE.IDVOTE;
			Persistence.Votes.Methods.VOTEOPTION_ADD(VOTEOPTION);

			alert("Everything is created correctly");
		} catch (e) {
			SysCfg.Log.Methods.WriteLog("VotesProfiler.js Persistence.Votes.TVotesProfiler this.CreateAll", e);
		}
	}
}
