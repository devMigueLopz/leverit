//**********************   PROPERTIES   *****************************************************************************************
Persistence.Votes.Properties.TVOTEDATAUSER = function() {
	this.IDVOTEDATAUSER = 0;
	this.IDVOTEDATA = 0;
	this.IDCMDBCI = 0;
	this.IDVOTEOPTION = 0;
	this.VOTEDATAUSER_NAME = "";
	this.VOTEDATAUSER_COMMENTARY = "";

	// Virtuales
	this.VOTEOPTION = new Persistence.Votes.Properties.TVOTEOPTION();
	this.VOTEDATA = new Persistence.Votes.Properties.TVOTEDATA();

	//Socket IO Properties
	this.ToByte = function(MemStream){
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDVOTEDATAUSER); 
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDVOTEDATA); 
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDCMDBCI); 
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDVOTEOPTION); 
		SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.VOTEDATAUSER_NAME); 
		SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.VOTEDATAUSER_COMMENTARY); 

	}
	this.ByteTo = function(MemStream)
	{
		this.IDVOTEDATAUSER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream); 
		this.IDVOTEDATA = SysCfg.Stream.Methods.ReadStreamInt32(MemStream); 
		this.IDCMDBCI = SysCfg.Stream.Methods.ReadStreamInt32(MemStream); 
		this.IDVOTEOPTION = SysCfg.Stream.Methods.ReadStreamInt32(MemStream); 
		this.VOTEDATAUSER_NAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream); 
		this.VOTEDATAUSER_COMMENTARY = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream); 

	}
	//Str IO Properties
	this.ToStr = function(Longitud,Texto)
	{
		SysCfg.Str.Protocol.WriteInt(this.IDVOTEDATAUSER, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteInt(this.IDVOTEDATA, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteInt(this.IDCMDBCI, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteInt(this.IDVOTEOPTION, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteStr(this.VOTEDATAUSER_NAME, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteStr(this.VOTEDATAUSER_COMMENTARY, Longitud, Texto); 

	}
	this.StrTo = function(Pos,Index,LongitudArray,Texto)
	{
		this.IDVOTEDATAUSER = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto); 
		this.IDVOTEDATA = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto); 
		this.IDCMDBCI = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto); 
		this.IDVOTEOPTION = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto); 
		this.VOTEDATAUSER_NAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto); 
		this.VOTEDATAUSER_COMMENTARY = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto); 

	}
}

//**********************   METHODS server  ********************************************************************************************
Persistence.Votes.Methods.VOTEDATAUSER_Fill = function(VOTEDATAUSERList,StrIDVOTEDATAUSER/*noin IDVOTEDATAUSER*/)
{
	var ResErr = new SysCfg.Error.Properties.TResErr();
	VOTEDATAUSERList.length = 0;
	var Param = new SysCfg.Stream.Properties.TParam();
	Param.Inicialize();
	if (StrIDVOTEDATAUSER == "")
	{
		Param.AddUnknown(UsrCfg.InternoAtisNames.VOTEDATAUSER.IDVOTEDATAUSER.FieldName, " = " + UsrCfg.InternoAtisNames.VOTEDATAUSER.NAME_TABLE + "." + UsrCfg.InternoAtisNames.VOTEDATAUSER.IDVOTEDATAUSER.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
	}
	else
	{
		StrIDVOTEDATAUSER = " IN (" + StrIDVOTEDATAUSER + ")";
		Param.AddUnknown(UsrCfg.InternoAtisNames.VOTEDATAUSER.IDVOTEDATAUSER.FieldName, StrIDVOTEDATAUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
	}
	//noin Param.AddInt32(UsrCfg.InternoDemoNames.VOTEDATAUSER.IDVOTEDATAUSER.FieldName, IDVOTEDATAUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
	try
	{
		/*
		//********   VOTEDATAUSER_GET   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"VOTEDATAUSER_GET", @"VOTEDATAUSER_GET description.");  
		UnSQL.SQL.Add(@"   SELECT "); 
		 UnSQL.SQL.Add(@"   IDVOTEDATAUSER, "); 
		 UnSQL.SQL.Add(@"   IDVOTEDATA, "); 
		 UnSQL.SQL.Add(@"   IDCMDBCI, "); 
		 UnSQL.SQL.Add(@"   IDVOTEOPTION, "); 
		 UnSQL.SQL.Add(@"   VOTEDATAUSER_NAME, "); 
		 UnSQL.SQL.Add(@"   VOTEDATAUSER_COMMENTARY "); 
		 UnSQL.SQL.Add(@"   FROM VOTEDATAUSER "); 
		 UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		var DS_VOTEDATAUSER = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "VOTEDATAUSER_GET", Param.ToBytes());
		ResErr = DS_VOTEDATAUSER.ResErr;
		if (ResErr.NotError)
		{
			if (DS_VOTEDATAUSER.DataSet.RecordCount > 0)
			{
				DS_VOTEDATAUSER.DataSet.First();
				while (!(DS_VOTEDATAUSER.DataSet.Eof))
				{
					var VOTEDATAUSER = new Persistence.Votes.Properties.TVOTEDATAUSER();
					VOTEDATAUSER.IDVOTEDATAUSER = DS_VOTEDATAUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.VOTEDATAUSER.IDVOTEDATAUSER.FieldName).asInt32();
					VOTEDATAUSER.IDVOTEDATA = DS_VOTEDATAUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.VOTEDATAUSER.IDVOTEDATA.FieldName).asInt32();
					VOTEDATAUSER.IDCMDBCI = DS_VOTEDATAUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.VOTEDATAUSER.IDCMDBCI.FieldName).asInt32();
					VOTEDATAUSER.IDVOTEOPTION = DS_VOTEDATAUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.VOTEDATAUSER.IDVOTEOPTION.FieldName).asInt32();
					VOTEDATAUSER.VOTEDATAUSER_NAME = DS_VOTEDATAUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.VOTEDATAUSER.VOTEDATAUSER_NAME.FieldName).asString();
					VOTEDATAUSER.VOTEDATAUSER_COMMENTARY = DS_VOTEDATAUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.VOTEDATAUSER.VOTEDATAUSER_COMMENTARY.FieldName).asText();
					 
					//Other
					VOTEDATAUSERList.push(VOTEDATAUSER);
					DS_VOTEDATAUSER.DataSet.Next();
				}
			}
			else
			{
				DS_VOTEDATAUSER.ResErr.NotError = false;
				DS_VOTEDATAUSER.ResErr.Mesaje = "RECORDCOUNT = 0";
			}
		}
	}
	catch (e){
		SysCfg.Log.Methods.WriteLog("DB_VOTEDATAUSER.js Persistence.Votes.Methods.VOTEDATAUSER_Fill", e);
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.Votes.Methods.VOTEDATAUSER_GETID = function(VOTEDATAUSER)
{
	var ResErr = new SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try
	{
		Param.Inicialize();
		//Other
		Param.AddInt32(UsrCfg.InternoAtisNames.VOTEDATAUSER.IDVOTEDATAUSER.FieldName, VOTEDATAUSER.IDVOTEDATAUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddInt32(UsrCfg.InternoAtisNames.VOTEDATAUSER.IDVOTEDATA.FieldName, VOTEDATAUSER.IDVOTEDATA, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddInt32(UsrCfg.InternoAtisNames.VOTEDATAUSER.IDCMDBCI.FieldName, VOTEDATAUSER.IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddInt32(UsrCfg.InternoAtisNames.VOTEDATAUSER.IDVOTEOPTION.FieldName, VOTEDATAUSER.IDVOTEOPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddString(UsrCfg.InternoAtisNames.VOTEDATAUSER.VOTEDATAUSER_NAME.FieldName, VOTEDATAUSER.VOTEDATAUSER_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddText(UsrCfg.InternoAtisNames.VOTEDATAUSER.VOTEDATAUSER_COMMENTARY.FieldName, VOTEDATAUSER.VOTEDATAUSER_COMMENTARY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

		/*
		//********   VOTEDATAUSER_GETID   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"VOTEDATAUSER_GETID", @"VOTEDATAUSER_GETID description.");  
		UnSQL.SQL.Add(@"   SELECT "); 
		 UnSQL.SQL.Add(@"   IDVOTEDATAUSER "); 
		 UnSQL.SQL.Add(@"   FROM VOTEDATAUSER "); 
		 UnSQL.SQL.Add(@"   Where "); 
		 UnSQL.SQL.Add(@"   IDVOTEDATA=@[IDVOTEDATA] And "); 
		 UnSQL.SQL.Add(@"   IDCMDBCI=@[IDCMDBCI] And "); 
		 UnSQL.SQL.Add(@"   IDVOTEOPTION=@[IDVOTEOPTION] And "); 
		 UnSQL.SQL.Add(@"   VOTEDATAUSER_NAME=@[VOTEDATAUSER_NAME] ");
		 UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		var DS_VOTEDATAUSER = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "VOTEDATAUSER_GETID", Param.ToBytes());
		ResErr = DS_VOTEDATAUSER.ResErr;
		if (ResErr.NotError)
		{
			if (DS_VOTEDATAUSER.DataSet.RecordCount > 0)
			{
			    DS_VOTEDATAUSER.DataSet.First();
			    VOTEDATAUSER.IDVOTEDATAUSER = DS_VOTEDATAUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.VOTEDATAUSER.IDVOTEDATAUSER.FieldName).asInt32();
			}
			else
			{
			    DS_VOTEDATAUSER.ResErr.NotError = false;
			    DS_VOTEDATAUSER.ResErr.Mesaje = "RECORDCOUNT = 0";
			}
		}
	}
	catch (e){
		SysCfg.Log.Methods.WriteLog("DB_VOTEDATAUSER.js Persistence.Votes.Methods.VOTEDATAUSER_GETID", e);
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.Votes.Methods.VOTEDATAUSER_ADD = function(VOTEDATAUSER)
{
	var ResErr = new SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try
	{
		Param.Inicialize();
		//Other
		Param.AddInt32(UsrCfg.InternoAtisNames.VOTEDATAUSER.IDVOTEDATAUSER.FieldName, VOTEDATAUSER.IDVOTEDATAUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.VOTEDATAUSER.IDVOTEDATA.FieldName, VOTEDATAUSER.IDVOTEDATA, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.VOTEDATAUSER.IDCMDBCI.FieldName, VOTEDATAUSER.IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.VOTEDATAUSER.IDVOTEOPTION.FieldName, VOTEDATAUSER.IDVOTEOPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.VOTEDATAUSER.VOTEDATAUSER_NAME.FieldName, VOTEDATAUSER.VOTEDATAUSER_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddText(UsrCfg.InternoAtisNames.VOTEDATAUSER.VOTEDATAUSER_COMMENTARY.FieldName, VOTEDATAUSER.VOTEDATAUSER_COMMENTARY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		 
		/* 
		//********   VOTEDATAUSER_ADD   ************************* 
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"VOTEDATAUSER_ADD", @"VOTEDATAUSER_ADD description."); 
		UnSQL.SQL.Add(@"   INSERT INTO VOTEDATAUSER( "); 
		 UnSQL.SQL.Add(@"   IDVOTEDATA, "); 
		 UnSQL.SQL.Add(@"   IDCMDBCI, "); 
		 UnSQL.SQL.Add(@"   IDVOTEOPTION, "); 
		 UnSQL.SQL.Add(@"   VOTEDATAUSER_NAME, "); 
		 UnSQL.SQL.Add(@"   VOTEDATAUSER_COMMENTARY "); 
		 UnSQL.SQL.Add(@"   ) Values ( ");  
		UnSQL.SQL.Add(@"   @[IDVOTEDATA], "); 
		 UnSQL.SQL.Add(@"   @[IDCMDBCI], "); 
		 UnSQL.SQL.Add(@"   @[IDVOTEOPTION], "); 
		 UnSQL.SQL.Add(@"   @[VOTEDATAUSER_NAME], "); 
		 UnSQL.SQL.Add(@"   @[VOTEDATAUSER_COMMENTARY] "); 
		 UnSQL.SQL.Add(@"   )  "); 
		UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "VOTEDATAUSER_ADD", Param.ToBytes());
		ResErr = Exec.ResErr;
		if (ResErr.NotError)
		{
			Persistence.Votes.Methods.VOTEDATAUSER_GETID(VOTEDATAUSER);
		}
	}
	catch (e){
		SysCfg.Log.Methods.WriteLog("DB_VOTEDATAUSER.js Persistence.Votes.Methods.VOTEDATAUSER_ADD", e);
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.Votes.Methods.VOTEDATAUSER_UPD = function(VOTEDATAUSER)
{
	var ResErr = SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try
	{
		Param.Inicialize();
		Param.AddInt32(UsrCfg.InternoAtisNames.VOTEDATAUSER.IDVOTEDATAUSER.FieldName, VOTEDATAUSER.IDVOTEDATAUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.VOTEDATAUSER.IDVOTEDATA.FieldName, VOTEDATAUSER.IDVOTEDATA, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.VOTEDATAUSER.IDCMDBCI.FieldName, VOTEDATAUSER.IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.VOTEDATAUSER.IDVOTEOPTION.FieldName, VOTEDATAUSER.IDVOTEOPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.VOTEDATAUSER.VOTEDATAUSER_NAME.FieldName, VOTEDATAUSER.VOTEDATAUSER_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddText(UsrCfg.InternoAtisNames.VOTEDATAUSER.VOTEDATAUSER_COMMENTARY.FieldName, VOTEDATAUSER.VOTEDATAUSER_COMMENTARY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		 

		/*   
		//********   VOTEDATAUSER_UPD   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"VOTEDATAUSER_UPD", @"VOTEDATAUSER_UPD description.");  
		UnSQL.SQL.Add(@"  UPDATE VOTEDATAUSER Set "); 
		 UnSQL.SQL.Add(@"   IDVOTEDATA=@[IDVOTEDATA], "); 
		 UnSQL.SQL.Add(@"   IDCMDBCI=@[IDCMDBCI], "); 
		 UnSQL.SQL.Add(@"   IDVOTEOPTION=@[IDVOTEOPTION], "); 
		 UnSQL.SQL.Add(@"   VOTEDATAUSER_NAME=@[VOTEDATAUSER_NAME], "); 
		 UnSQL.SQL.Add(@"   VOTEDATAUSER_COMMENTARY=@[VOTEDATAUSER_COMMENTARY] "); 
		 UnSQL.SQL.Add(@"   Where "); 
		 UnSQL.SQL.Add(@"   IDVOTEDATAUSER=@[IDVOTEDATAUSER] ");
		 UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "VOTEDATAUSER_UPD", Param.ToBytes());
		ResErr = Exec.ResErr;
		if (ResErr.NotError)
		{
			//GET(); 
		}
	}
	catch (e){
		SysCfg.Log.Methods.WriteLog("DB_VOTEDATAUSER.js Persistence.Votes.Methods.VOTEDATAUSER_UPD", e);
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.Votes.Methods.VOTEDATAUSER_DEL = function(/*String StrIDVOTEDATAUSER*/IDVOTEDATAUSER)
{
	var ResErr = SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try
	{
		Param.Inicialize();
		/*if (StrIDVOTEDATAUSER == "")
		{
		Param.AddUnknown(UsrCfg.InternoAtisNames.VOTEDATAUSER.IDVOTEDATAUSER.FieldName, " = " + UsrCfg.InternoAtisNames.VOTEDATAUSER.NAME_TABLE + "." + UsrCfg.InternoAtisNames.VOTEDATAUSER.IDVOTEDATAUSER.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
		}
		else
		{
		StrIDVOTEDATAUSER = " IN (" + StrIDVOTEDATAUSER + ")";
		Param.AddUnknown(UsrCfg.InternoAtisNames.VOTEDATAUSER.IDVOTEDATAUSER.FieldName, StrIDVOTEDATAUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		}*/
		Param.AddInt32(UsrCfg.InternoAtisNames.VOTEDATAUSER.IDVOTEDATAUSER.FieldName, IDVOTEDATAUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		/*
		//********   VOTEDATAUSER_DEL   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"VOTEDATAUSER_DEL", @"VOTEDATAUSER_DEL description.");  
		UnSQL.SQL.Add(@"   DELETE VOTEDATAUSER "); 
		 UnSQL.SQL.Add(@"   Where "); 
		 UnSQL.SQL.Add(@"   IDVOTEDATAUSER=@[IDVOTEDATAUSER]   ");
		 UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "VOTEDATAUSER_DEL", Param.ToBytes());
		ResErr = Exec.ResErr;
	}
	catch (e){
		SysCfg.Log.Methods.WriteLog("DB_VOTEDATAUSER.js Persistence.Votes.Methods.VOTEDATAUSER_DEL", e);
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}

//**********************   METHODS   ********************************************************************************************
Persistence.Votes.Methods.VOTEDATAUSER_ListSetID = function(VOTEDATAUSERList,IDVOTEDATAUSER)
{
	for (i = 0; i < VOTEDATAUSERList.length; i++)
	{
		if (IDVOTEDATAUSER == VOTEDATAUSERList[i].IDVOTEDATAUSER)
			return (VOTEDATAUSERList[i]);
	}
	var UnVOTEDATAUSER = new Persistence.Properties.TVOTEDATAUSER;
	UnVOTEDATAUSER.IDVOTEDATAUSER = IDVOTEDATAUSER; 
	return (UnVOTEDATAUSER);
}
Persistence.Votes.Methods.VOTEDATAUSER_ListAdd = function(VOTEDATAUSERList,VOTEDATAUSER)
{
	var i = Persistence.Votes.Methods.VOTEDATAUSER_ListGetIndex(VOTEDATAUSERList, VOTEDATAUSER);
	if (i == -1) VOTEDATAUSERList.push(VOTEDATAUSER);
	else
	{
		VOTEDATAUSERList[i].IDVOTEDATAUSER = VOTEDATAUSER.IDVOTEDATAUSER;
		VOTEDATAUSERList[i].IDVOTEDATA = VOTEDATAUSER.IDVOTEDATA;
		VOTEDATAUSERList[i].IDCMDBCI = VOTEDATAUSER.IDCMDBCI;
		VOTEDATAUSERList[i].IDVOTEOPTION = VOTEDATAUSER.IDVOTEOPTION;
		VOTEDATAUSERList[i].VOTEDATAUSER_NAME = VOTEDATAUSER.VOTEDATAUSER_NAME;
		VOTEDATAUSERList[i].VOTEDATAUSER_COMMENTARY = VOTEDATAUSER.VOTEDATAUSER_COMMENTARY;

	}
}
Persistence.Votes.Methods.VOTEDATAUSER_ListGetIndex = function(VOTEDATAUSERList, VOTEDATAUSER)
{
	for (i = 0; i < VOTEDATAUSERList.length; i++)
	{
		if (VOTEDATAUSER.IDVOTEDATAUSER == VOTEDATAUSERList[i].IDVOTEDATAUSER)
			return (i);
	}
	return (-1);
}
Persistence.Votes.Methods.VOTEDATAUSER_ListGetIndex = function(VOTEDATAUSERList, IDVOTEDATAUSER)
{
	for (i = 0; i < VOTEDATAUSERList.length; i++)
	{
		if (IDVOTEDATAUSER == VOTEDATAUSERList[i].IDVOTEDATAUSER)
			return (i);
	}
	return (-1);
}

//String Function 
Persistence.Votes.Methods.VOTEDATAUSERListtoStr = function(VOTEDATAUSERList)
{
	var Res = Persistence.Properties._Version + "(1)0";
	var Longitud = new SysCfg.ref("");
	var Texto = new SysCfg.ref("");
	try
	{
		SysCfg.Str.Protocol.WriteInt(VOTEDATAUSERList.length,Longitud,Texto);
		for (Counter = 0; Counter < VOTEDATAUSERList.length; Counter++)
		{
			var UnVOTEDATAUSER = VOTEDATAUSERList[Counter];
			UnVOTEDATAUSER.ToStr(Longitud,Texto);
		}

		Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
		Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
	}
	catch (e)
	{
		var ResErr = new SysCfg.Error.Properties.TResErr();
		ResErr.NotError = false;
		ResErr.Mesaje = "LIFESTATUStoStr:" + e;
	}
	return Res;
}
Persistence.Votes.Methods.StrtoVOTEDATAUSER = function(ProtocoloStr, VOTEDATAUSERList)
{

	var Res = false;
	var Longitud, Texto;
	var Index =  new SysCfg.ref(0);
	var Pos =  new SysCfg.ref(0);
	try
	{
		VOTEDATAUSERList.length = 0;   
		if (Persistence.Votes.Properties._Version == ProtocoloStr.substring(0, 3))
		{
			Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")") - ProtocoloStr.indexOf("(") - 1);
			Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.Length - ProtocoloStr.indexOf(")") - 1);
			LongitudArray = Longitud.split(",");
			LSCount = SysCfg.Str.Protocol.ReadInt(Pos,Index,LongitudArray,Texto);
			for (i = 0; i < LSCount; i++)
			{
			    UnVOTEDATAUSER = new Persistence.Properties.TVOTEDATAUSER(); //Mode new row
			    UnVOTEDATAUSER.StrTo(Pos,Index, LongitudArray, Texto);
			    Persistence.Votes.VOTEDATAUSER_ListAdd(VOTEDATAUSERList, UnVOTEDATAUSER);
			}
		}
	}
	catch (e)
	{
		var ResErr = SysCfg.Error.Properties.TResErr;
		ResErr.NotError = false;
		ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
	}
	return (Res);
}


//Socket Function 
Persistence.Votes.Methods.VOTEDATAUSERListToByte = function(VOTEDATAUSERList, MemStream, idx)
{
	SysCfg.Stream.Methods.WriteStreamInt32(MemStream, VOTEDATAUSERList.Count-idx);
	for (i = idx; i < VOTEDATAUSERList.Count; i++)
	{
		VOTEDATAUSERList[i].ToByte(MemStream);
	}
}
Persistence.Votes.Methods.ByteToVOTEDATAUSERList = function (VOTEDATAUSERList, MemStream)
{
	Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
	for (i = 0; i < Count; i++)
	{
		UnVOTEDATAUSER = new Persistence.Properties.TVOTEDATAUSER();
		UnVOTEDATAUSER.ByteTo(MemStream);
		Methods.VOTEDATAUSER_ListAdd(VOTEDATAUSERList,UnVOTEDATAUSER);
	}
}
