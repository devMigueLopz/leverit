//**********************   PROPERTIES   *****************************************************************************************
Persistence.Votes.Properties.TVOTEOPTION = function() {
	this.IDVOTEOPTION = 0;
	this.IDVOTE = 0;
	this.VOTEOPTION_NAME = "";
	this.VOTEOPTION_DESCRIPTION = "";

	//VIRTUALES
    this.VOTE = new Persistence.Votes.Properties.TVOTE();
    this.VOTEDATAUSERList = new Array();

	//Socket IO Properties
	this.ToByte = function(MemStream){
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDVOTEOPTION); 
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDVOTE); 
		SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.VOTEOPTION_NAME); 
		SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.VOTEOPTION_DESCRIPTION); 

	}

	this.ByteTo = function(MemStream)
	{
		this.IDVOTEOPTION = SysCfg.Stream.Methods.ReadStreamInt32(MemStream); 
		this.IDVOTE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream); 
		this.VOTEOPTION_NAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream); 
		this.VOTEOPTION_DESCRIPTION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream); 

	}
	//Str IO Properties
	this.ToStr = function(Longitud,Texto)
	{
		SysCfg.Str.Protocol.WriteInt(this.IDVOTEOPTION, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteInt(this.IDVOTE, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteStr(this.VOTEOPTION_NAME, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteStr(this.VOTEOPTION_DESCRIPTION, Longitud, Texto); 

	}
	this.StrTo = function(Pos,Index,LongitudArray,Texto)
	{
		this.IDVOTEOPTION = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto); 
		this.IDVOTE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto); 
		this.VOTEOPTION_NAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto); 
		this.VOTEOPTION_DESCRIPTION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto); 

	}
}

//**********************   METHODS server  ********************************************************************************************
Persistence.Votes.Methods.VOTEOPTION_Fill = function(VOTEOPTIONList,StrIDVOTEOPTION/*noin IDVOTEOPTION*/)
{
	var ResErr = new SysCfg.Error.Properties.TResErr();
	VOTEOPTIONList.length = 0;
	var Param = new SysCfg.Stream.Properties.TParam();
	Param.Inicialize();
	if (StrIDVOTEOPTION == "")
	{
		Param.AddUnknown(UsrCfg.InternoAtisNames.VOTEOPTION.IDVOTEOPTION.FieldName, " = " + UsrCfg.InternoAtisNames.VOTEOPTION.NAME_TABLE + "." + UsrCfg.InternoAtisNames.VOTEOPTION.IDVOTEOPTION.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
	}
	else
	{
		StrIDVOTEOPTION = " IN (" + StrIDVOTEOPTION + ")";
		Param.AddUnknown(UsrCfg.InternoAtisNames.VOTEOPTION.IDVOTEOPTION.FieldName, StrIDVOTEOPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
	}
	//noin Param.AddInt32(UsrCfg.InternoDemoNames.VOTEOPTION.IDVOTEOPTION.FieldName, IDVOTEOPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
	try
	{
		/*
		//********   VOTEOPTION_GET   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"VOTEOPTION_GET", @"VOTEOPTION_GET description.");  
		UnSQL.SQL.Add(@"   SELECT "); 
		 UnSQL.SQL.Add(@"   IDVOTEOPTION, "); 
		 UnSQL.SQL.Add(@"   IDVOTE, "); 
		 UnSQL.SQL.Add(@"   VOTEOPTION_NAME, "); 
		 UnSQL.SQL.Add(@"   VOTEOPTION_DESCRIPTION "); 
		 UnSQL.SQL.Add(@"   FROM VOTEOPTION "); 
		 UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		var DS_VOTEOPTION = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "VOTEOPTION_GET", Param.ToBytes());
		ResErr = DS_VOTEOPTION.ResErr;
		if (ResErr.NotError)
		{
			if (DS_VOTEOPTION.DataSet.RecordCount > 0)
			{
				DS_VOTEOPTION.DataSet.First();
				while (!(DS_VOTEOPTION.DataSet.Eof))
				{
					var VOTEOPTION = new Persistence.Votes.Properties.TVOTEOPTION();
					VOTEOPTION.IDVOTEOPTION = DS_VOTEOPTION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.VOTEOPTION.IDVOTEOPTION.FieldName).asInt32();
					VOTEOPTION.IDVOTE = DS_VOTEOPTION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.VOTEOPTION.IDVOTE.FieldName).asInt32();
					VOTEOPTION.VOTEOPTION_NAME = DS_VOTEOPTION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.VOTEOPTION.VOTEOPTION_NAME.FieldName).asString();
					VOTEOPTION.VOTEOPTION_DESCRIPTION = DS_VOTEOPTION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.VOTEOPTION.VOTEOPTION_DESCRIPTION.FieldName).asString();
					 
					//Other
					VOTEOPTIONList.push(VOTEOPTION);
					DS_VOTEOPTION.DataSet.Next();
				}
			}
			else
			{
				DS_VOTEOPTION.ResErr.NotError = false;
				DS_VOTEOPTION.ResErr.Mesaje = "RECORDCOUNT = 0";
			}
		}
	}
	catch (e){
		SysCfg.Log.Methods.WriteLog("DB_VOTEOPTION.js Persistence.Votes.Methods.VOTEOPTION_Fill", e);
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.Votes.Methods.VOTEOPTION_GETID = function(VOTEOPTION)
{
	var ResErr = new SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try
	{
		Param.Inicialize();
		//Other
		Param.AddInt32(UsrCfg.InternoAtisNames.VOTEOPTION.IDVOTEOPTION.FieldName, VOTEOPTION.IDVOTEOPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddInt32(UsrCfg.InternoAtisNames.VOTEOPTION.IDVOTE.FieldName, VOTEOPTION.IDVOTE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddString(UsrCfg.InternoAtisNames.VOTEOPTION.VOTEOPTION_NAME.FieldName, VOTEOPTION.VOTEOPTION_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddString(UsrCfg.InternoAtisNames.VOTEOPTION.VOTEOPTION_DESCRIPTION.FieldName, VOTEOPTION.VOTEOPTION_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

		/*
		//********   VOTEOPTION_GETID   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"VOTEOPTION_GETID", @"VOTEOPTION_GETID description.");  
		UnSQL.SQL.Add(@"   SELECT "); 
		 UnSQL.SQL.Add(@"   IDVOTEOPTION "); 
		 UnSQL.SQL.Add(@"   FROM VOTEOPTION "); 
		 UnSQL.SQL.Add(@"   Where "); 
		 UnSQL.SQL.Add(@"   IDVOTE=@[IDVOTE] And "); 
		 UnSQL.SQL.Add(@"   VOTEOPTION_NAME=@[VOTEOPTION_NAME] And "); 
		 UnSQL.SQL.Add(@"   VOTEOPTION_DESCRIPTION=@[VOTEOPTION_DESCRIPTION] ");
		 UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		var DS_VOTEOPTION = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "VOTEOPTION_GETID", Param.ToBytes());
		ResErr = DS_VOTEOPTION.ResErr;
		if (ResErr.NotError)
		{
			if (DS_VOTEOPTION.DataSet.RecordCount > 0)
			{
			    DS_VOTEOPTION.DataSet.First();
		    	VOTEOPTION.IDVOTEOPTION = DS_VOTEOPTION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.VOTEOPTION.IDVOTEOPTION.FieldName).asInt32();
			}
			else
			{
			    DS_VOTEOPTION.ResErr.NotError = false;
		    	DS_VOTEOPTION.ResErr.Mesaje = "RECORDCOUNT = 0";
			}
		}
	}
	catch (e){
		SysCfg.Log.Methods.WriteLog("DB_VOTEOPTION.js Persistence.Votes.Methods.VOTEOPTION_GETID", e);
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.Votes.Methods.VOTEOPTION_ADD = function(VOTEOPTION)
{
	var ResErr = new SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try
	{
		Param.Inicialize();
		//Other
		Param.AddInt32(UsrCfg.InternoAtisNames.VOTEOPTION.IDVOTEOPTION.FieldName, VOTEOPTION.IDVOTEOPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.VOTEOPTION.IDVOTE.FieldName, VOTEOPTION.IDVOTE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.VOTEOPTION.VOTEOPTION_NAME.FieldName, VOTEOPTION.VOTEOPTION_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.VOTEOPTION.VOTEOPTION_DESCRIPTION.FieldName, VOTEOPTION.VOTEOPTION_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		 
		/* 
		//********   VOTEOPTION_ADD   ************************* 
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"VOTEOPTION_ADD", @"VOTEOPTION_ADD description."); 
		UnSQL.SQL.Add(@"   INSERT INTO VOTEOPTION( "); 
		 UnSQL.SQL.Add(@"   IDVOTE, "); 
		 UnSQL.SQL.Add(@"   VOTEOPTION_NAME, "); 
		 UnSQL.SQL.Add(@"   VOTEOPTION_DESCRIPTION "); 
		 UnSQL.SQL.Add(@"   ) Values ( ");  
		UnSQL.SQL.Add(@"   @[IDVOTE], "); 
		 UnSQL.SQL.Add(@"   @[VOTEOPTION_NAME], "); 
		 UnSQL.SQL.Add(@"   @[VOTEOPTION_DESCRIPTION] "); 
		 UnSQL.SQL.Add(@"   )  "); 
		UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "VOTEOPTION_ADD", Param.ToBytes());
		ResErr = Exec.ResErr;
		if (ResErr.NotError)
		{
			Persistence.Votes.Methods.VOTEOPTION_GETID(VOTEOPTION);
		}
	}
	catch (e){
		SysCfg.Log.Methods.WriteLog("DB_VOTEOPTION.js Persistence.Votes.Methods.VOTEOPTION_ADD", e);
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.Votes.Methods.VOTEOPTION_UPD = function(VOTEOPTION)
{
	var ResErr = SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try
	{
		Param.Inicialize();
		Param.AddInt32(UsrCfg.InternoAtisNames.VOTEOPTION.IDVOTEOPTION.FieldName, VOTEOPTION.IDVOTEOPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.VOTEOPTION.IDVOTE.FieldName, VOTEOPTION.IDVOTE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.VOTEOPTION.VOTEOPTION_NAME.FieldName, VOTEOPTION.VOTEOPTION_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.VOTEOPTION.VOTEOPTION_DESCRIPTION.FieldName, VOTEOPTION.VOTEOPTION_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		 

		/*   
		//********   VOTEOPTION_UPD   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"VOTEOPTION_UPD", @"VOTEOPTION_UPD description.");  
		UnSQL.SQL.Add(@"  UPDATE VOTEOPTION Set "); 
		 UnSQL.SQL.Add(@"   IDVOTEOPTION=@[IDVOTEOPTION], "); 
		 UnSQL.SQL.Add(@"   IDVOTE=@[IDVOTE], "); 
		 UnSQL.SQL.Add(@"   VOTEOPTION_NAME=@[VOTEOPTION_NAME], "); 
		 UnSQL.SQL.Add(@"   VOTEOPTION_DESCRIPTION=@[VOTEOPTION_DESCRIPTION] "); 
		 UnSQL.SQL.Add(@"   Where "); 
		 UnSQL.SQL.Add(@"   IDVOTEOPTION=@[IDVOTEOPTION] ");
		 UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "VOTEOPTION_UPD", Param.ToBytes());
		ResErr = Exec.ResErr;
		if (ResErr.NotError)
		{
			//GET(); 
		}
	}
	catch (e){
		SysCfg.Log.Methods.WriteLog("DB_VOTEOPTION.js Persistence.Votes.Methods.VOTEOPTION_UPD", e);
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.Votes.Methods.VOTEOPTION_DEL = function(/*String StrIDVOTEOPTION*/IDVOTEOPTION)
{
	var ResErr = SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try
	{
		Param.Inicialize();
		/*if (StrIDVOTEOPTION == "")
		{
		Param.AddUnknown(UsrCfg.InternoAtisNames.VOTEOPTION.IDVOTEOPTION.FieldName, " = " + UsrCfg.InternoAtisNames.VOTEOPTION.NAME_TABLE + "." + UsrCfg.InternoAtisNames.VOTEOPTION.IDVOTEOPTION.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
		}
		else
		{
		StrIDVOTEOPTION = " IN (" + StrIDVOTEOPTION + ")";
		Param.AddUnknown(UsrCfg.InternoAtisNames.VOTEOPTION.IDVOTEOPTION.FieldName, StrIDVOTEOPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		}*/
		Param.AddInt32(UsrCfg.InternoAtisNames.VOTEOPTION.IDVOTEOPTION.FieldName, IDVOTEOPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		/*
		//********   VOTEOPTION_DEL   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"VOTEOPTION_DEL", @"VOTEOPTION_DEL description.");  
		UnSQL.SQL.Add(@"   DELETE VOTEOPTION "); 
		 UnSQL.SQL.Add(@"   Where "); 
		 UnSQL.SQL.Add(@"   IDVOTEOPTION=@[IDVOTEOPTION]   ");
		 UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "VOTEOPTION_DEL", Param.ToBytes());
		ResErr = Exec.ResErr;
	}
	catch (e){
		SysCfg.Log.Methods.WriteLog("DB_VOTEOPTION.js Persistence.Votes.Methods.VOTEOPTION_DEL", e);
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}

//**********************   METHODS   ********************************************************************************************
Persistence.Votes.Methods.VOTEOPTION_ListSetID = function(VOTEOPTIONList,IDVOTEOPTION)
{
	for (i = 0; i < VOTEOPTIONList.length; i++)
	{
		if (IDVOTEOPTION == VOTEOPTIONList[i].IDVOTEOPTION)
		return (VOTEOPTIONList[i]);
	}
	var UnVOTEOPTION = new Persistence.Properties.TVOTEOPTION;
	UnVOTEOPTION.IDVOTEOPTION = IDVOTEOPTION; 
	return (UnVOTEOPTION);
}
Persistence.Votes.Methods.VOTEOPTION_ListAdd = function(VOTEOPTIONList,VOTEOPTION)
{
	var i = Persistence.Votes.Methods.VOTEOPTION_ListGetIndex(VOTEOPTIONList, VOTEOPTION);
	if (i == -1) VOTEOPTIONList.push(VOTEOPTION);
	else
	{
		VOTEOPTIONList[i].IDVOTEOPTION = VOTEOPTION.IDVOTEOPTION;
		VOTEOPTIONList[i].IDVOTE = VOTEOPTION.IDVOTE;
		VOTEOPTIONList[i].VOTEOPTION_NAME = VOTEOPTION.VOTEOPTION_NAME;
		VOTEOPTIONList[i].VOTEOPTION_DESCRIPTION = VOTEOPTION.VOTEOPTION_DESCRIPTION;

	}
}
Persistence.Votes.Methods.VOTEOPTION_ListGetIndex = function(VOTEOPTIONList, VOTEOPTION)
{
	for (i = 0; i < VOTEOPTIONList.length; i++)
	{
		if (VOTEOPTION.IDVOTEOPTION == VOTEOPTIONList[i].IDVOTEOPTION)
		return (i);
	}
	return (-1);
}
Persistence.Votes.Methods.VOTEOPTION_ListGetIndex = function(VOTEOPTIONList, IDVOTEOPTION)
{
	for (i = 0; i < VOTEOPTIONList.length; i++)
	{
		if (IDVOTEOPTION == VOTEOPTIONList[i].IDVOTEOPTION)
		return (i);
	}
	return (-1);
}

//String Function 
Persistence.Votes.Methods.VOTEOPTIONListtoStr = function(VOTEOPTIONList)
{
	var Res = Persistence.Properties._Version + "(1)0";
	var Longitud = new SysCfg.ref("");
	var Texto = new SysCfg.ref("");
	try
	{
		SysCfg.Str.Protocol.WriteInt(VOTEOPTIONList.length,Longitud,Texto);
		for (Counter = 0; Counter < VOTEOPTIONList.length; Counter++)
		{
			var UnVOTEOPTION = VOTEOPTIONList[Counter];
			UnVOTEOPTION.ToStr(Longitud,Texto);
		}

		Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
		Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
	}
	catch (e)
	{
		var ResErr = new SysCfg.Error.Properties.TResErr();
		ResErr.NotError = false;
		ResErr.Mesaje = "LIFESTATUStoStr:" + e;
	}
	return Res;
}
Persistence.Votes.Methods.StrtoVOTEOPTION = function(ProtocoloStr, VOTEOPTIONList)
{

	var Res = false;
	var Longitud, Texto;
	var Index =  new SysCfg.ref(0);
	var Pos =  new SysCfg.ref(0);
	try
	{
		VOTEOPTIONList.length = 0;   
		if (Persistence.Votes.Properties._Version == ProtocoloStr.substring(0, 3))
		{
			Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")") - ProtocoloStr.indexOf("(") - 1);
			Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.Length - ProtocoloStr.indexOf(")") - 1);
			LongitudArray = Longitud.split(",");
			LSCount = SysCfg.Str.Protocol.ReadInt(Pos,Index,LongitudArray,Texto);
			for (i = 0; i < LSCount; i++)
			{
			    UnVOTEOPTION = new Persistence.Properties.TVOTEOPTION(); //Mode new row
			    UnVOTEOPTION.StrTo(Pos,Index, LongitudArray, Texto);
			    Persistence.Votes.VOTEOPTION_ListAdd(VOTEOPTIONList, UnVOTEOPTION);
			}
		}
	}
	catch (e)
	{
		var ResErr = SysCfg.Error.Properties.TResErr;
		ResErr.NotError = false;
		ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
	}
	return (Res);
}


//Socket Function 
Persistence.Votes.Methods.VOTEOPTIONListToByte = function(VOTEOPTIONList, MemStream, idx)
{
	SysCfg.Stream.Methods.WriteStreamInt32(MemStream, VOTEOPTIONList.Count-idx);
	for (i = idx; i < VOTEOPTIONList.Count; i++)
	{
		VOTEOPTIONList[i].ToByte(MemStream);
	}
}
Persistence.Votes.Methods.ByteToVOTEOPTIONList = function (VOTEOPTIONList, MemStream)
{
	Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
	for (i = 0; i < Count; i++)
	{
		UnVOTEOPTION = new Persistence.Properties.TVOTEOPTION();
		UnVOTEOPTION.ByteTo(MemStream);
		Methods.VOTEOPTION_ListAdd(VOTEOPTIONList,UnVOTEOPTION);
	}
}
