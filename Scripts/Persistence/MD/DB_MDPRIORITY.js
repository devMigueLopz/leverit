﻿//**********************   PROPERTIES   *****************************************************************************************
Persistence.MD.Properties.TMDPRIORITY = function () {
    this.IDMDPRIORITY = 0;
    this.PRIORITYNAME = "";

    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDPRIORITY);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.PRIORITYNAME);

    }
    this.ByteTo = function (MemStream) {
        this.IDMDPRIORITY = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.PRIORITYNAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);

    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDMDPRIORITY, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.PRIORITYNAME, Longitud, Texto);

    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDMDPRIORITY = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.PRIORITYNAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);

    }
}

//**********************   METHODS server  ********************************************************************************************
Persistence.MD.Methods.MDPRIORITY_Fill = function (MDPRIORITYList, StrIDMDPRIORITY/*noin IDMDPRIORITY*/) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var DS_MDPRIORITY;
    MDPRIORITYList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDMDPRIORITY == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName, " = " + UsrCfg.InternoAtisNames.MDPRIORITY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDMDPRIORITY = " IN (" + StrIDMDPRIORITY + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName, StrIDMDPRIORITY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //noin Param.AddInt32(UsrCfg.InternoDemoNames.MDPRIORITY.IDMDPRIORITY.FieldName, IDMDPRIORITY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {
     
        DS_MDPRIORITY = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDPRIORITY_GET", Param.ToBytes());
        ResErr = DS_MDPRIORITY.ResErr;
        if (ResErr.NotError) {
            if (DS_MDPRIORITY.DataSet.RecordCount > 0) {
                DS_MDPRIORITY.DataSet.First();
                while (!(DS_MDPRIORITY.DataSet.Eof)) {
                    var MDPRIORITY = new Persistence.MD.Properties.TMDPRIORITY();
                    MDPRIORITY.IDMDPRIORITY = DS_MDPRIORITY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName).asInt32();
                    MDPRIORITY.PRIORITYNAME = DS_MDPRIORITY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDPRIORITY.PRIORITYNAME.FieldName).asString();

                    //Other
                    MDPRIORITYList.push(MDPRIORITY);
                    DS_MDPRIORITY.DataSet.Next();
                }
            }
            else {
                DS_MDPRIORITY.ResErr.NotError = false;
                DS_MDPRIORITY.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (DS_MDPRIORITY);
}
Persistence.MD.Methods.MDPRIORITY_GETID = function (MDPRIORITY) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName, MDPRIORITY.IDMDPRIORITY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.MDPRIORITY.PRIORITYNAME.FieldName, MDPRIORITY.PRIORITYNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

        /*
        //********   MDPRIORITY_GETID   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDPRIORITY_GETID", @"MDPRIORITY_GETID description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   FROM MDPRIORITY "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   PRIORITYNAME=@[PRIORITYNAME] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_MDPRIORITY = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDPRIORITY_GETID", Param.ToBytes());
        ResErr = DS_MDPRIORITY.ResErr;
        if (ResErr.NotError) {
            if (DS_MDPRIORITY.DataSet.RecordCount > 0) {
                DS_MDPRIORITY.DataSet.First();
                MDPRIORITY.IDMDPRIORITY = DS_MDPRIORITY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName).asInt32();
            }
            else {
                DS_MDPRIORITY.ResErr.NotError = false;
                DS_MDPRIORITY.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.MD.Methods.MDPRIORITY_ADD = function (MDPRIORITY) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName, MDPRIORITY.IDMDPRIORITY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDPRIORITY.PRIORITYNAME.FieldName, MDPRIORITY.PRIORITYNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDPRIORITY_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.MD.Methods.MDPRIORITY_UPD = function (MDPRIORITY) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName, MDPRIORITY.IDMDPRIORITY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDPRIORITY.PRIORITYNAME.FieldName, MDPRIORITY.PRIORITYNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDPRIORITY_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.MD.Methods.MDPRIORITY_DEL_BY_ID = function (IDMDPRIORITY) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName, IDMDPRIORITY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDPRIORITY_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

Persistence.MD.Methods.MDPRIORITY_DEL = function (MDPRIORITY/*MDPRIORITYList*/) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        /*
        StrIDMDPRIORITY = "";
        StrIDOTRO = "";
        for (var i = 0; i < MDPRIORITYList.length; i++) {
        StrIDMDPRIORITY += MDPRIORITYList[i].IDMDPRIORITY + ",";
        StrIDOTRO += MDPRIORITYList[i].IDOTRO + ",";
        }
        StrIDMDPRIORITY = StrIDMDPRIORITY.substring(0, StrIDMDPRIORITY.length - 1);
        StrIDOTRO = StrIDOTRO.substring(0, StrIDOTRO.length - 1);
        
        
        if (StrIDMDPRIORITY == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName, " = " + UsrCfg.InternoAtisNames.MDPRIORITY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDMDPRIORITY = " IN (" + StrIDMDPRIORITY + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName, StrIDMDPRIORITY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/


        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName, MDPRIORITY.IDMDPRIORITY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*    
        //********   MDPRIORITY_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDPRIORITY_DEL", @"MDPRIORITY_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE MDPRIORITY "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDMDPRIORITY=@[IDMDPRIORITY]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDPRIORITY_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//**********************   METHODS   ********************************************************************************************
Persistence.MD.Methods.MDPRIORITY_ListSetID = function (MDPRIORITYList, IDMDPRIORITY) {
    for (i = 0; i < MDPRIORITYList.length; i++) {
        if (IDMDPRIORITY == MDPRIORITYList[i].IDMDPRIORITY)
            return (MDPRIORITYList[i]);
    }
    var UnMDPRIORITY = new Persistence.Properties.TMDPRIORITY;
    UnMDPRIORITY.IDMDPRIORITY = IDMDPRIORITY;
    return (UnMDPRIORITY);
}
Persistence.MD.Methods.MDPRIORITY_ListAdd = function (MDPRIORITYList, MDPRIORITY) {
    var i = Persistence.MD.Methods.MDPRIORITY_ListGetIndex(MDPRIORITYList, MDPRIORITY);
    if (i == -1) MDPRIORITYList.push(MDPRIORITY);
    else {
        MDPRIORITYList[i].IDMDPRIORITY = MDPRIORITY.IDMDPRIORITY;
        MDPRIORITYList[i].PRIORITYNAME = MDPRIORITY.PRIORITYNAME;

    }
}
//CJRC_26072018
Persistence.MD.Methods.MDPRIORITY_ListGetIndex = function (MDPRIORITYList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.MD.Methods.MDPRIORITY_ListGetIndexIDMDPRIORITY(MDPRIORITYList, Param);

    }
    else {
        Res = Persistence.MD.Methods.MDPRIORITY_ListGetIndexMDPRIORITY(MDPRIORITYList, Param);
    }
    return (Res);
}

Persistence.MD.Methods.MDPRIORITY_ListGetIndexMDPRIORITY = function (MDPRIORITYList, MDPRIORITY) {
    for (i = 0; i < MDPRIORITYList.length; i++) {
        if (MDPRIORITY.IDMDPRIORITY == MDPRIORITYList[i].IDMDPRIORITY)
            return (i);
    }
    return (-1);
}
Persistence.MD.Methods.MDPRIORITY_ListGetIndexIDMDPRIORITY = function (MDPRIORITYList, IDMDPRIORITY) {
    for (i = 0; i < MDPRIORITYList.length; i++) {
        if (IDMDPRIORITY == MDPRIORITYList[i].IDMDPRIORITY)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.MD.Methods.MDPRIORITYListtoStr = function (MDPRIORITYList) {
    var Res = Persistence.Properties._Version + "(1)0";
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(MDPRIORITYList.length, Longitud, Texto);
        for (Counter = 0; Counter < MDPRIORITYList.length; Counter++) {
            var UnMDPRIORITY = MDPRIORITYList[Counter];
            UnMDPRIORITY.ToStr(Longitud, Texto);
        }

        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
}
Persistence.MD.Methods.StrtoMDPRIORITY = function (ProtocoloStr, MDPRIORITYList) {

    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        MDPRIORITYList.length = 0;
        if (Persistence.MD.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")") - ProtocoloStr.indexOf("(") - 1);
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.Length - ProtocoloStr.indexOf(")") - 1);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnMDPRIORITY = new Persistence.Properties.TMDPRIORITY(); //Mode new row
                UnMDPRIORITY.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.MD.MDPRIORITY_ListAdd(MDPRIORITYList, UnMDPRIORITY);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}


//Socket Function 
Persistence.MD.Methods.MDPRIORITYListToByte = function (MDPRIORITYList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, MDPRIORITYList.Count - idx);
    for (i = idx; i < MDPRIORITYList.length; i++) {
        MDPRIORITYList[i].ToByte(MemStream);
    }
}
Persistence.MD.Methods.ByteToMDPRIORITYList = function (MDPRIORITYList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnMDPRIORITY = new Persistence.Properties.TMDPRIORITY();
        UnMDPRIORITY.ByteTo(MemStream);
        Methods.MDPRIORITY_ListAdd(MDPRIORITYList, UnMDPRIORITY);
    }
}
