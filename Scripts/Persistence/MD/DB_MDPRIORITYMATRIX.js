﻿//**********************   PROPERTIES   *****************************************************************************************
Persistence.MD.Properties.TMDPRIORITYMATRIX = function () {
    this.IDMDIMPACT = 0;
    this.IDMDURGENCY = 0;
    this.IDMDPRIORITY = 0;

    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDIMPACT);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDURGENCY);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDPRIORITY);

    }
    this.ByteTo = function (MemStream) {
        this.IDMDIMPACT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDURGENCY = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDPRIORITY = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);

    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDMDIMPACT, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDURGENCY, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDPRIORITY, Longitud, Texto);

    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDMDIMPACT = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDURGENCY = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDPRIORITY = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);

    }
}

//**********************   METHODS server  ********************************************************************************************
Persistence.MD.Methods.MDPRIORITYMATRIX_Fill = function (MDPRIORITYMATRIXList, StrIDMDPRIORITYMATRIX/*noin IDMDPRIORITYMATRIX*/) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    MDPRIORITYMATRIXList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
 
    try {
         var DS_MDPRIORITYMATRIX = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDPRIORITYMATRIX_GET", Param.ToBytes());
        ResErr = DS_MDPRIORITYMATRIX.ResErr;
        if (ResErr.NotError) {
            if (DS_MDPRIORITYMATRIX.DataSet.RecordCount > 0) {
                DS_MDPRIORITYMATRIX.DataSet.First();
                while (!(DS_MDPRIORITYMATRIX.DataSet.Eof)) {
                    var MDPRIORITYMATRIX = new Persistence.MD.Properties.TMDPRIORITYMATRIX();
                    MDPRIORITYMATRIX.IDMDIMPACT = DS_MDPRIORITYMATRIX.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDIMPACT.FieldName).asInt32();
                    MDPRIORITYMATRIX.IDMDURGENCY = DS_MDPRIORITYMATRIX.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDURGENCY.FieldName).asInt32();
                    MDPRIORITYMATRIX.IDMDPRIORITY = DS_MDPRIORITYMATRIX.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDPRIORITY.FieldName).asInt32();

                    //Other
                    MDPRIORITYMATRIXList.push(MDPRIORITYMATRIX);
                    DS_MDPRIORITYMATRIX.DataSet.Next();
                }
            }
            else {
                DS_MDPRIORITYMATRIX.ResErr.NotError = false;
                DS_MDPRIORITYMATRIX.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (DS_MDPRIORITYMATRIX);
}
Persistence.MD.Methods.MDPRIORITYMATRIX_GETID = function (MDPRIORITYMATRIX) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDIMPACT.FieldName, MDPRIORITYMATRIX.IDMDIMPACT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDURGENCY.FieldName, MDPRIORITYMATRIX.IDMDURGENCY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDPRIORITY.FieldName, MDPRIORITYMATRIX.IDMDPRIORITY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

        /*
        //********   MDPRIORITYMATRIX_GETID   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDPRIORITYMATRIX_GETID", @"MDPRIORITYMATRIX_GETID description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   FROM MDPRIORITYMATRIX "); 
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_MDPRIORITYMATRIX = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDPRIORITYMATRIX_GETID", Param.ToBytes());
        ResErr = DS_MDPRIORITYMATRIX.ResErr;
        if (ResErr.NotError) {
            if (DS_MDPRIORITYMATRIX.DataSet.RecordCount > 0) {
                DS_MDPRIORITYMATRIX.DataSet.First();
                MDPRIORITYMATRIX.IDMDPRIORITYMATRIX = DS_MDPRIORITYMATRIX.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDPRIORITYMATRIX.FieldName).asInt32();
            }
            else {
                DS_MDPRIORITYMATRIX.ResErr.NotError = false;
                DS_MDPRIORITYMATRIX.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.MD.Methods.MDPRIORITYMATRIX_ADD = function (MDPRIORITYMATRIX) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDIMPACT.FieldName, MDPRIORITYMATRIX.IDMDIMPACT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDURGENCY.FieldName, MDPRIORITYMATRIX.IDMDURGENCY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDPRIORITY.FieldName, MDPRIORITYMATRIX.IDMDPRIORITY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        /* 
        //********   MDPRIORITYMATRIX_ADD   ************************* 
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDPRIORITYMATRIX_ADD", @"MDPRIORITYMATRIX_ADD description."); 
        UnSQL.SQL.Add(@"   INSERT INTO MDPRIORITYMATRIX( "); 
         UnSQL.SQL.Add(@"   IDMDIMPACT, "); 
         UnSQL.SQL.Add(@"   IDMDURGENCY, "); 
         UnSQL.SQL.Add(@"   IDMDPRIORITY "); 
         UnSQL.SQL.Add(@"   ) Values ( ");  
        UnSQL.SQL.Add(@"   @[IDMDIMPACT], "); 
         UnSQL.SQL.Add(@"   @[IDMDURGENCY], "); 
         UnSQL.SQL.Add(@"   @[IDMDPRIORITY] "); 
         UnSQL.SQL.Add(@"   )  "); 
        UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDPRIORITYMATRIX_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.MD.Methods.MDPRIORITYMATRIX_GETID(MDPRIORITYMATRIX);
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.MD.Methods.MDPRIORITYMATRIX_UPD = function (MDPRIORITYMATRIX) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDIMPACT.FieldName, MDPRIORITYMATRIX.IDMDIMPACT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDURGENCY.FieldName, MDPRIORITYMATRIX.IDMDURGENCY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDPRIORITY.FieldName, MDPRIORITYMATRIX.IDMDPRIORITY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);


        /*   
        //********   MDPRIORITYMATRIX_UPD   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDPRIORITYMATRIX_UPD", @"MDPRIORITYMATRIX_UPD description.");  
        UnSQL.SQL.Add(@"  UPDATE MDPRIORITYMATRIX Set "); 
         UnSQL.SQL.Add(@"   IDMDIMPACT=@[IDMDIMPACT], "); 
         UnSQL.SQL.Add(@"   IDMDURGENCY=@[IDMDURGENCY], "); 
         UnSQL.SQL.Add(@"   IDMDPRIORITY=@[IDMDPRIORITY] "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDMDIMPACT=@[IDMDIMPACT] And "); 
         UnSQL.SQL.Add(@"   IDMDURGENCY=@[IDMDURGENCY] And "); 
         UnSQL.SQL.Add(@"   IDMDPRIORITY=@[IDMDPRIORITY] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDPRIORITYMATRIX_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.MD.Methods.MDPRIORITYMATRIX_DEL = function (MDPRIORITYMATRIX/*MDPRIORITYMATRIXList*/) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDPRIORITYMATRIX.FieldName, MDPRIORITYMATRIX.IDMDPRIORITYMATRIX, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*    
        //********   MDPRIORITYMATRIX_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDPRIORITYMATRIX_DEL", @"MDPRIORITYMATRIX_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE MDPRIORITYMATRIX "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDMDIMPACT=@[IDMDIMPACT], And  "); 
         UnSQL.SQL.Add(@"   IDMDURGENCY=@[IDMDURGENCY], And  "); 
         UnSQL.SQL.Add(@"   IDMDPRIORITY=@[IDMDPRIORITY]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDPRIORITYMATRIX_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//**********************   METHODS   ********************************************************************************************
Persistence.MD.Methods.MDPRIORITYMATRIX_ListSetID = function (MDPRIORITYMATRIXList, IDMDPRIORITYMATRIX) {
    for (i = 0; i < MDPRIORITYMATRIXList.length; i++) {
        if (IDMDPRIORITYMATRIX == MDPRIORITYMATRIXList[i].IDMDPRIORITYMATRIX)
            return (MDPRIORITYMATRIXList[i]);
    }
    var UnMDPRIORITYMATRIX = new Persistence.Properties.TMDPRIORITYMATRIX;
    UnMDPRIORITYMATRIX.IDMDPRIORITYMATRIX = IDMDPRIORITYMATRIX;
    return (UnMDPRIORITYMATRIX);
}
Persistence.MD.Methods.MDPRIORITYMATRIX_ListAdd = function (MDPRIORITYMATRIXList, MDPRIORITYMATRIX) {
    var i = Persistence.MD.Methods.MDPRIORITYMATRIX_ListGetIndex(MDPRIORITYMATRIXList, MDPRIORITYMATRIX);
    if (i == -1) MDPRIORITYMATRIXList.push(MDPRIORITYMATRIX);
    else {
        MDPRIORITYMATRIXList[i].IDMDIMPACT = MDPRIORITYMATRIX.IDMDIMPACT;
        MDPRIORITYMATRIXList[i].IDMDURGENCY = MDPRIORITYMATRIX.IDMDURGENCY;
        MDPRIORITYMATRIXList[i].IDMDPRIORITY = MDPRIORITYMATRIX.IDMDPRIORITY;

    }
}
//CJRC_26072018
Persistence.MD.Methods.MDPRIORITYMATRIX_ListGetIndex = function (MDPRIORITYMATRIXList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.MD.Methods.MDPRIORITYMATRIX_ListGetIndexIDMDPRIORITYMATRIX(MDPRIORITYMATRIXList, Param);

    }
    else {
        Res = Persistence.MD.Methods.MDPRIORITYMATRIX_ListGetIndexMDPRIORITYMATRIX(MDPRIORITYMATRIXList, Param);
    }
    return (Res);
}

Persistence.MD.Methods.MDPRIORITYMATRIX_ListGetIndexMDPRIORITYMATRIX = function (MDPRIORITYMATRIXList, MDPRIORITYMATRIX) {
    for (i = 0; i < MDPRIORITYMATRIXList.length; i++) {
        if (MDPRIORITYMATRIX.IDMDPRIORITYMATRIX == MDPRIORITYMATRIXList[i].IDMDPRIORITYMATRIX)
            return (i);
    }
    return (-1);
}
Persistence.MD.Methods.MDPRIORITYMATRIX_ListGetIndexIDMDPRIORITYMATRIX = function (MDPRIORITYMATRIXList, IDMDPRIORITYMATRIX) {
    for (i = 0; i < MDPRIORITYMATRIXList.length; i++) {
        if (IDMDPRIORITYMATRIX == MDPRIORITYMATRIXList[i].IDMDPRIORITYMATRIX)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.MD.Methods.MDPRIORITYMATRIXListtoStr = function (MDPRIORITYMATRIXList) {
    var Res = Persistence.Properties._Version + "(1)0";
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(MDPRIORITYMATRIXList.length, Longitud, Texto);
        for (Counter = 0; Counter < MDPRIORITYMATRIXList.length; Counter++) {
            var UnMDPRIORITYMATRIX = MDPRIORITYMATRIXList[Counter];
            UnMDPRIORITYMATRIX.ToStr(Longitud, Texto);
        }

        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
}
Persistence.MD.Methods.StrtoMDPRIORITYMATRIX = function (ProtocoloStr, MDPRIORITYMATRIXList) {

    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        MDPRIORITYMATRIXList.length = 0;
        if (Persistence.MD.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")") - ProtocoloStr.indexOf("(") - 1);
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.Length - ProtocoloStr.indexOf(")") - 1);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnMDPRIORITYMATRIX = new Persistence.Properties.TMDPRIORITYMATRIX(); //Mode new row
                UnMDPRIORITYMATRIX.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.MD.MDPRIORITYMATRIX_ListAdd(MDPRIORITYMATRIXList, UnMDPRIORITYMATRIX);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}


//Socket Function 
Persistence.MD.Methods.MDPRIORITYMATRIXListToByte = function (MDPRIORITYMATRIXList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, MDPRIORITYMATRIXList.Count - idx);
    for (i = idx; i < MDPRIORITYMATRIXList.length; i++) {
        MDPRIORITYMATRIXList[i].ToByte(MemStream);
    }
}
Persistence.MD.Methods.ByteToMDPRIORITYMATRIXList = function (MDPRIORITYMATRIXList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnMDPRIORITYMATRIX = new Persistence.Properties.TMDPRIORITYMATRIX();
        UnMDPRIORITYMATRIX.ByteTo(MemStream);
        Methods.MDPRIORITYMATRIX_ListAdd(MDPRIORITYMATRIXList, UnMDPRIORITYMATRIX);
    }
}
