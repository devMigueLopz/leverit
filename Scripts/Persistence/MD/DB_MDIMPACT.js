﻿//**********************   PROPERTIES   *****************************************************************************************
Persistence.MD.Properties.TMDIMPACT = function () {
    this.IDMDIMPACT = 0;
    this.IMPACTNAME = "";

    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDIMPACT);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.IMPACTNAME);

    }
    this.ByteTo = function (MemStream) {
        this.IDMDIMPACT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IMPACTNAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);

    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDMDIMPACT, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.IMPACTNAME, Longitud, Texto);

    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDMDIMPACT = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IMPACTNAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);

    }
}

//**********************   METHODS server  ********************************************************************************************
Persistence.MD.Methods.MDIMPACT_Fill = function (MDIMPACTList, StrIDMDIMPACT/*noin IDMDIMPACT*/) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var DS_MDIMPACT;
    MDIMPACTList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDMDIMPACT == "") {
     //   Param.AddUnknown(UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName, " = " + UsrCfg.InternoAtisNames.MDPRIORITY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.FieldName, " = " + UsrCfg.InternoAtisNames.MDIMPACT.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDMDIMPACT = " IN (" + StrIDMDIMPACT + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.FieldName, StrIDMDIMPACT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //noin Param.AddInt32(UsrCfg.InternoDemoNames.MDIMPACT.IDMDIMPACT.FieldName, IDMDIMPACT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {
     
        DS_MDIMPACT = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDIMPACT_GET", Param.ToBytes());
        ResErr = DS_MDIMPACT.ResErr;
        if (ResErr.NotError) {
            if (DS_MDIMPACT.DataSet.RecordCount > 0) {
                DS_MDIMPACT.DataSet.First();
                while (!(DS_MDIMPACT.DataSet.Eof)) {
                    var MDIMPACT = new Persistence.MD.Properties.TMDIMPACT();
                    MDIMPACT.IDMDIMPACT = DS_MDIMPACT.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.FieldName).asInt32();
                    MDIMPACT.IMPACTNAME = DS_MDIMPACT.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDIMPACT.IMPACTNAME.FieldName).asString();

                    //Other
                    MDIMPACTList.push(MDIMPACT);
                    DS_MDIMPACT.DataSet.Next();
                }
            }
            else {
                DS_MDIMPACT.ResErr.NotError = false;
                DS_MDIMPACT.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (DS_MDIMPACT);
}
Persistence.MD.Methods.MDIMPACT_GETID = function (MDIMPACT) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.FieldName, MDIMPACT.IDMDIMPACT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.MDIMPACT.IMPACTNAME.FieldName, MDIMPACT.IMPACTNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

        /*
        //********   MDIMPACT_GETID   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDIMPACT_GETID", @"MDIMPACT_GETID description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   FROM MDIMPACT "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IMPACTNAME=@[IMPACTNAME] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_MDIMPACT = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDIMPACT_GETID", Param.ToBytes());
        ResErr = DS_MDIMPACT.ResErr;
        if (ResErr.NotError) {
            if (DS_MDIMPACT.DataSet.RecordCount > 0) {
                DS_MDIMPACT.DataSet.First();
                MDIMPACT.IDMDIMPACT = DS_MDIMPACT.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.FieldName).asInt32();
            }
            else {
                DS_MDIMPACT.ResErr.NotError = false;
                DS_MDIMPACT.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.MD.Methods.MDIMPACT_ADD = function (MDIMPACT) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.FieldName, MDIMPACT.IDMDIMPACT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDIMPACT.IMPACTNAME.FieldName, MDIMPACT.IMPACTNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDIMPACT_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.MD.Methods.MDIMPACT_UPD = function (MDIMPACT) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.FieldName, MDIMPACT.IDMDIMPACT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDIMPACT.IMPACTNAME.FieldName, MDIMPACT.IMPACTNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDIMPACT_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.MD.Methods.MDIMPACT_DEL_BY_ID = function (IDMDIMPACT) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.FieldName, IDMDIMPACT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDIMPACT_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

Persistence.MD.Methods.MDIMPACT_DEL = function (MDIMPACT/*MDIMPACTList*/) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        /*
        StrIDMDIMPACT = "";
        StrIDOTRO = "";
        for (var i = 0; i < MDIMPACTList.length; i++) {
        StrIDMDIMPACT += MDIMPACTList[i].IDMDIMPACT + ",";
        StrIDOTRO += MDIMPACTList[i].IDOTRO + ",";
        }
        StrIDMDIMPACT = StrIDMDIMPACT.substring(0, StrIDMDIMPACT.length - 1);
        StrIDOTRO = StrIDOTRO.substring(0, StrIDOTRO.length - 1);
        
        
        if (StrIDMDIMPACT == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.FieldName, " = " + UsrCfg.InternoAtisNames.MDIMPACT.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDMDIMPACT = " IN (" + StrIDMDIMPACT + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.FieldName, StrIDMDIMPACT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/


        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.FieldName, MDIMPACT.IDMDIMPACT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*    
        //********   MDIMPACT_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDIMPACT_DEL", @"MDIMPACT_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE MDIMPACT "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDMDIMPACT=@[IDMDIMPACT]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDIMPACT_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//**********************   METHODS   ********************************************************************************************
Persistence.MD.Methods.MDIMPACT_ListSetID = function (MDIMPACTList, IDMDIMPACT) {
    for (i = 0; i < MDIMPACTList.length; i++) {
        if (IDMDIMPACT == MDIMPACTList[i].IDMDIMPACT)
            return (MDIMPACTList[i]);
    }
    var UnMDIMPACT = new Persistence.Properties.TMDIMPACT;
    UnMDIMPACT.IDMDIMPACT = IDMDIMPACT;
    return (UnMDIMPACT);
}
Persistence.MD.Methods.MDIMPACT_ListAdd = function (MDIMPACTList, MDIMPACT) {
    var i = Persistence.MD.Methods.MDIMPACT_ListGetIndex(MDIMPACTList, MDIMPACT);
    if (i == -1) MDIMPACTList.push(MDIMPACT);
    else {
        MDIMPACTList[i].IDMDIMPACT = MDIMPACT.IDMDIMPACT;
        MDIMPACTList[i].IMPACTNAME = MDIMPACT.IMPACTNAME;

    }
}
//CJRC_26072018
Persistence.MD.Methods.MDIMPACT_ListGetIndex = function (MDIMPACTList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.MD.Methods.MDIMPACT_ListGetIndexIDMDIMPACT(MDIMPACTList, Param);

    }
    else {
        Res = Persistence.MD.Methods.MDIMPACT_ListGetIndexMDIMPACT(MDIMPACTList, Param);
    }
    return (Res);
}

Persistence.MD.Methods.MDIMPACT_ListGetIndexMDIMPACT = function (MDIMPACTList, MDIMPACT) {
    for (i = 0; i < MDIMPACTList.length; i++) {
        if (MDIMPACT.IDMDIMPACT == MDIMPACTList[i].IDMDIMPACT)
            return (i);
    }
    return (-1);
}
Persistence.MD.Methods.MDIMPACT_ListGetIndexIDMDIMPACT = function (MDIMPACTList, IDMDIMPACT) {
    for (i = 0; i < MDIMPACTList.length; i++) {
        if (IDMDIMPACT == MDIMPACTList[i].IDMDIMPACT)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.MD.Methods.MDIMPACTListtoStr = function (MDIMPACTList) {
    var Res = Persistence.Properties._Version + "(1)0";
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(MDIMPACTList.length, Longitud, Texto);
        for (Counter = 0; Counter < MDIMPACTList.length; Counter++) {
            var UnMDIMPACT = MDIMPACTList[Counter];
            UnMDIMPACT.ToStr(Longitud, Texto);
        }

        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
}
Persistence.MD.Methods.StrtoMDIMPACT = function (ProtocoloStr, MDIMPACTList) {

    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        MDIMPACTList.length = 0;
        if (Persistence.MD.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")") - ProtocoloStr.indexOf("(") - 1);
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.Length - ProtocoloStr.indexOf(")") - 1);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnMDIMPACT = new Persistence.Properties.TMDIMPACT(); //Mode new row
                UnMDIMPACT.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.MD.MDIMPACT_ListAdd(MDIMPACTList, UnMDIMPACT);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}


//Socket Function 
Persistence.MD.Methods.MDIMPACTListToByte = function (MDIMPACTList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, MDIMPACTList.Count - idx);
    for (i = idx; i < MDIMPACTList.length; i++) {
        MDIMPACTList[i].ToByte(MemStream);
    }
}
Persistence.MD.Methods.ByteToMDIMPACTList = function (MDIMPACTList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnMDIMPACT = new Persistence.Properties.TMDIMPACT();
        UnMDIMPACT.ByteTo(MemStream);
        Methods.MDIMPACT_ListAdd(MDIMPACTList, UnMDIMPACT);
    }
}
