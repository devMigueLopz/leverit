﻿Persistence.MD.Properties._Version = '001';
Persistence.MD.TMDProfiler = function () {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    ResErr = new SysCfg.Error.Properties.TResErr();
    this.MDPRIORITY_List = new Array();

    this.Fill = function () {
        Persistence.MD.Methods.MDPRIORITY_Fill(this.MDQUERY_List, "");

        Persistence.MD.Methods.StartReation(this, -1);
    }
}