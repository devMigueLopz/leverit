﻿//**********************   PROPERTIES   *****************************************************************************************
Persistence.MD.Properties.TMDURGENCY = function () {
    this.IDMDURGENCY = 0;
    this.URGENCYNAME = "";

    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDURGENCY);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.URGENCYNAME);

    }
    this.ByteTo = function (MemStream) {
        this.IDMDURGENCY = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.URGENCYNAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);

    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDMDURGENCY, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.URGENCYNAME, Longitud, Texto);

    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDMDURGENCY = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.URGENCYNAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);

    }
}

//**********************   METHODS server  ********************************************************************************************
Persistence.MD.Methods.MDURGENCY_Fill = function (MDURGENCYList, StrIDMDURGENCY/*noin IDMDURGENCY*/) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    MDURGENCYList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDMDURGENCY == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDURGENCY.IDMDURGENCY.FieldName, " = " + UsrCfg.InternoAtisNames.MDURGENCY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDURGENCY.IDMDURGENCY.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDMDURGENCY = " IN (" + StrIDMDURGENCY + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDURGENCY.IDMDURGENCY.FieldName, StrIDMDURGENCY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //noin Param.AddInt32(UsrCfg.InternoDemoNames.MDURGENCY.IDMDURGENCY.FieldName, IDMDURGENCY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {
        /*
        //********   MDURGENCY_GET   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDURGENCY_GET", @"MDURGENCY_GET description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   IDMDURGENCY, "); 
         UnSQL.SQL.Add(@"   URGENCYNAME "); 
         UnSQL.SQL.Add(@"   FROM MDURGENCY "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDMDURGENCY=@[IDMDURGENCY] And "); 
         UnSQL.SQL.Add(@"   URGENCYNAME=@[URGENCYNAME] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_MDURGENCY = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDURGENCY_GET", Param.ToBytes());
        ResErr = DS_MDURGENCY.ResErr;
        if (ResErr.NotError) {
            if (DS_MDURGENCY.DataSet.RecordCount > 0) {
                DS_MDURGENCY.DataSet.First();
                while (!(DS_MDURGENCY.DataSet.Eof)) {
                    var MDURGENCY = new Persistence.MD.Properties.TMDURGENCY();
                    MDURGENCY.IDMDURGENCY = DS_MDURGENCY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDURGENCY.IDMDURGENCY.FieldName).asInt32();
                    MDURGENCY.URGENCYNAME = DS_MDURGENCY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDURGENCY.URGENCYNAME.FieldName).asString();

                    //Other
                    MDURGENCYList.push(MDURGENCY);
                    DS_MDURGENCY.DataSet.Next();
                }
            }
            else {
                DS_MDURGENCY.ResErr.NotError = false;
                DS_MDURGENCY.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (DS_MDURGENCY);
}
Persistence.MD.Methods.MDURGENCY_GETID = function (MDURGENCY) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.MDURGENCY.IDMDURGENCY.FieldName, MDURGENCY.IDMDURGENCY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.MDURGENCY.URGENCYNAME.FieldName, MDURGENCY.URGENCYNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

        /*
        //********   MDURGENCY_GETID   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDURGENCY_GETID", @"MDURGENCY_GETID description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   FROM MDURGENCY "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   URGENCYNAME=@[URGENCYNAME] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_MDURGENCY = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDURGENCY_GETID", Param.ToBytes());
        ResErr = DS_MDURGENCY.ResErr;
        if (ResErr.NotError) {
            if (DS_MDURGENCY.DataSet.RecordCount > 0) {
                DS_MDURGENCY.DataSet.First();
                MDURGENCY.IDMDURGENCY = DS_MDURGENCY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDURGENCY.IDMDURGENCY.FieldName).asInt32();
            }
            else {
                DS_MDURGENCY.ResErr.NotError = false;
                DS_MDURGENCY.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.MD.Methods.MDURGENCY_ADD = function (MDURGENCY) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.MDURGENCY.IDMDURGENCY.FieldName, MDURGENCY.IDMDURGENCY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDURGENCY.URGENCYNAME.FieldName, MDURGENCY.URGENCYNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        /* 
        //********   MDURGENCY_ADD   ************************* 
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDURGENCY_ADD", @"MDURGENCY_ADD description."); 
        UnSQL.SQL.Add(@"   INSERT INTO MDURGENCY( "); 
         UnSQL.SQL.Add(@"   IDMDURGENCY, "); 
         UnSQL.SQL.Add(@"   URGENCYNAME "); 
         UnSQL.SQL.Add(@"   ) Values ( ");  
        UnSQL.SQL.Add(@"   @[IDMDURGENCY], "); 
         UnSQL.SQL.Add(@"   @[URGENCYNAME] "); 
         UnSQL.SQL.Add(@"   )  "); 
        UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDURGENCY_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.MD.Methods.MDURGENCY_GETID(MDURGENCY);
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.MD.Methods.MDURGENCY_UPD = function (MDURGENCY) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDURGENCY.IDMDURGENCY.FieldName, MDURGENCY.IDMDURGENCY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDURGENCY.URGENCYNAME.FieldName, MDURGENCY.URGENCYNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);


        /*   
        //********   MDURGENCY_UPD   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDURGENCY_UPD", @"MDURGENCY_UPD description.");  
        UnSQL.SQL.Add(@"  UPDATE MDURGENCY Set "); 
         UnSQL.SQL.Add(@"   IDMDURGENCY=@[IDMDURGENCY], "); 
         UnSQL.SQL.Add(@"   URGENCYNAME=@[URGENCYNAME] "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDMDURGENCY=@[IDMDURGENCY] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDURGENCY_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.MD.Methods.MDURGENCY_DEL_BY_ID = function (/*String StrIDMDURGENCY*/IDMDURGENCY) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDMDURGENCY == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDURGENCY.IDMDURGENCY.FieldName, " = " + UsrCfg.InternoAtisNames.MDURGENCY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDURGENCY.IDMDURGENCY.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDMDURGENCY = " IN (" + StrIDMDURGENCY + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDURGENCY.IDMDURGENCY.FieldName, StrIDMDURGENCY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.MDURGENCY.IDMDURGENCY.FieldName, IDMDURGENCY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   MDURGENCY_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDURGENCY_DEL", @"MDURGENCY_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE MDURGENCY "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDMDURGENCY=@[IDMDURGENCY]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDURGENCY_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.MD.Methods.MDURGENCY_DEL = function (MDURGENCY/*MDURGENCYList*/) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        /*
        StrIDMDURGENCY = "";
        StrIDOTRO = "";
        for (var i = 0; i < MDURGENCYList.length; i++) {
        StrIDMDURGENCY += MDURGENCYList[i].IDMDURGENCY + ",";
        StrIDOTRO += MDURGENCYList[i].IDOTRO + ",";
        }
        StrIDMDURGENCY = StrIDMDURGENCY.substring(0, StrIDMDURGENCY.length - 1);
        StrIDOTRO = StrIDOTRO.substring(0, StrIDOTRO.length - 1);
        
        
        if (StrIDMDURGENCY == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDURGENCY.IDMDURGENCY.FieldName, " = " + UsrCfg.InternoAtisNames.MDURGENCY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDURGENCY.IDMDURGENCY.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDMDURGENCY = " IN (" + StrIDMDURGENCY + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDURGENCY.IDMDURGENCY.FieldName, StrIDMDURGENCY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/


        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDURGENCY.IDMDURGENCY.FieldName, MDURGENCY.IDMDURGENCY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*    
        //********   MDURGENCY_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDURGENCY_DEL", @"MDURGENCY_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE MDURGENCY "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDMDURGENCY=@[IDMDURGENCY]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDURGENCY_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//**********************   METHODS   ********************************************************************************************
Persistence.MD.Methods.MDURGENCY_ListSetID = function (MDURGENCYList, IDMDURGENCY) {
    for (i = 0; i < MDURGENCYList.length; i++) {
        if (IDMDURGENCY == MDURGENCYList[i].IDMDURGENCY)
            return (MDURGENCYList[i]);
    }
    var UnMDURGENCY = new Persistence.Properties.TMDURGENCY;
    UnMDURGENCY.IDMDURGENCY = IDMDURGENCY;
    return (UnMDURGENCY);
}
Persistence.MD.Methods.MDURGENCY_ListAdd = function (MDURGENCYList, MDURGENCY) {
    var i = Persistence.MD.Methods.MDURGENCY_ListGetIndex(MDURGENCYList, MDURGENCY);
    if (i == -1) MDURGENCYList.push(MDURGENCY);
    else {
        MDURGENCYList[i].IDMDURGENCY = MDURGENCY.IDMDURGENCY;
        MDURGENCYList[i].URGENCYNAME = MDURGENCY.URGENCYNAME;

    }
}
//CJRC_26072018
Persistence.MD.Methods.MDURGENCY_ListGetIndex = function (MDURGENCYList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.MD.Methods.MDURGENCY_ListGetIndexIDMDURGENCY(MDURGENCYList, Param);

    }
    else {
        Res = Persistence.MD.Methods.MDURGENCY_ListGetIndexMDURGENCY(MDURGENCYList, Param);
    }
    return (Res);
}

Persistence.MD.Methods.MDURGENCY_ListGetIndexMDURGENCY = function (MDURGENCYList, MDURGENCY) {
    for (i = 0; i < MDURGENCYList.length; i++) {
        if (MDURGENCY.IDMDURGENCY == MDURGENCYList[i].IDMDURGENCY)
            return (i);
    }
    return (-1);
}
Persistence.MD.Methods.MDURGENCY_ListGetIndexIDMDURGENCY = function (MDURGENCYList, IDMDURGENCY) {
    for (i = 0; i < MDURGENCYList.length; i++) {
        if (IDMDURGENCY == MDURGENCYList[i].IDMDURGENCY)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.MD.Methods.MDURGENCYListtoStr = function (MDURGENCYList) {
    var Res = Persistence.Properties._Version + "(1)0";
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(MDURGENCYList.length, Longitud, Texto);
        for (Counter = 0; Counter < MDURGENCYList.length; Counter++) {
            var UnMDURGENCY = MDURGENCYList[Counter];
            UnMDURGENCY.ToStr(Longitud, Texto);
        }

        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
}
Persistence.MD.Methods.StrtoMDURGENCY = function (ProtocoloStr, MDURGENCYList) {

    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        MDURGENCYList.length = 0;
        if (Persistence.MD.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")") - ProtocoloStr.indexOf("(") - 1);
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.Length - ProtocoloStr.indexOf(")") - 1);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnMDURGENCY = new Persistence.Properties.TMDURGENCY(); //Mode new row
                UnMDURGENCY.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.MD.MDURGENCY_ListAdd(MDURGENCYList, UnMDURGENCY);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}


//Socket Function 
Persistence.MD.Methods.MDURGENCYListToByte = function (MDURGENCYList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, MDURGENCYList.Count - idx);
    for (i = idx; i < MDURGENCYList.length; i++) {
        MDURGENCYList[i].ToByte(MemStream);
    }
}
Persistence.MD.Methods.ByteToMDURGENCYList = function (MDURGENCYList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnMDURGENCY = new Persistence.Properties.TMDURGENCY();
        UnMDURGENCY.ByteTo(MemStream);
        Methods.MDURGENCY_ListAdd(MDURGENCYList, UnMDURGENCY);
    }
}
