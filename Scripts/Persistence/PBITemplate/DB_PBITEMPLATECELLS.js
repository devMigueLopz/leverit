//**********************   PROPERTIES   *****************************************************************************************
Persistence.PBITemplate.Properties.TPBITEMPLATECELLS = function () {
    this.IDPBITEMPLATECELLS = 0;
    this.IDPBITEMPLATE = 0;
    this.IDPBITEMPLATECELLS_PARENT = 0;
    this.CELLS_POSITION = 0;
    this.CELLS_END = false;
    this.CELLS_NAME = "";
    /*VIRTUALES*/
    this.PBITEMPLATE = {}; /* new Persistence.PBITemplate.Properties.TPBITEMPLATE(); 1 celda puede tener 1 template (objeto)*/
    this.PBITEMPLATECELLSDESIGN = {};/* new Persistence.PBITemplate.Properties.TPBITEMPLATECELLSDESIGN(); 1 celda puede tener un solo dise�o de boostrap*/
    this.PBITEMPLATECELLGRAPHIC = null;/*TPBITEMPLATECELLS 1 celda puede tener 1 grafico como no puede tener graficos.*/
    this.PBITEMPLATECELLS_PARENT = null;/*TPBITEMPLATECELLS 1 celda puede tener 1 celda padre como no puede tener celdra padre.*/
    this.PBITEMPLATECELLS_CHILDList = new Array();/*PBITEMPLATECELLS_CHILDList 1 celda puede tener lista de hijos como ningun hijo */;
    /*END VIRTUALES*/
    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDPBITEMPLATECELLS);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDPBITEMPLATE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDPBITEMPLATECELLS_PARENT);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.CELLS_POSITION);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.CELLS_END);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CELLS_NAME);

    }
    this.ByteTo = function (MemStream) {
        this.IDPBITEMPLATECELLS = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDPBITEMPLATE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDPBITEMPLATECELLS_PARENT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.CELLS_POSITION = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.CELLS_END = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.CELLS_NAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);

    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDPBITEMPLATECELLS, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDPBITEMPLATE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDPBITEMPLATECELLS_PARENT, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.CELLS_POSITION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteBool(this.CELLS_END, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.CELLS_NAME, Longitud, Texto);

    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDPBITEMPLATECELLS = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDPBITEMPLATE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDPBITEMPLATECELLS_PARENT = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.CELLS_POSITION = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.CELLS_END = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);
        this.CELLS_NAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);

    }
}
//**********************   METHODS SERVER  ********************************************************************************************
Persistence.PBITemplate.Methods.PBITEMPLATECELLS_Fill = function (PBITEMPLATECELLSList, StrIDPBITEMPLATECELLS/*noin IDPBITEMPLATECELLS*/) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    PBITEMPLATECELLSList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDPBITEMPLATECELLS == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.PBITEMPLATECELLS.IDPBITEMPLATECELLS.FieldName, " = " + UsrCfg.InternoAtisNames.PBITEMPLATECELLS.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PBITEMPLATECELLS.IDPBITEMPLATECELLS.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDPBITEMPLATECELLS = " IN (" + StrIDPBITEMPLATECELLS + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.PBITEMPLATECELLS.IDPBITEMPLATECELLS.FieldName, StrIDPBITEMPLATECELLS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //noin Param.AddInt32(UsrCfg.InternoDemoNames.PBITEMPLATECELLS.IDPBITEMPLATECELLS.FieldName, IDPBITEMPLATECELLS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {
        /*
        //********   PBITEMPLATECELLS_GET   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PBITEMPLATECELLS_GET", @"PBITEMPLATECELLS_GET description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   IDPBITEMPLATECELLS, "); 
         UnSQL.SQL.Add(@"   IDPBITEMPLATE, "); 
         UnSQL.SQL.Add(@"   IDPBITEMPLATECELLS_PARENT, "); 
         UnSQL.SQL.Add(@"   CELLS_POSITION, "); 
         UnSQL.SQL.Add(@"   CELLS_END, "); 
         UnSQL.SQL.Add(@"   CELLS_NAME "); 
         UnSQL.SQL.Add(@"   FROM PBITEMPLATECELLS "); 
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_PBITEMPLATECELLS = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "PBITEMPLATECELLS_GET", Param.ToBytes());
        ResErr = DS_PBITEMPLATECELLS.ResErr;
        if (ResErr.NotError) {
            if (DS_PBITEMPLATECELLS.DataSet.RecordCount > 0) {
                DS_PBITEMPLATECELLS.DataSet.First();
                while (!(DS_PBITEMPLATECELLS.DataSet.Eof)) {
                    var PBITEMPLATECELLS = new Persistence.PBITemplate.Properties.TPBITEMPLATECELLS();
                    PBITEMPLATECELLS.IDPBITEMPLATECELLS = DS_PBITEMPLATECELLS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATECELLS.IDPBITEMPLATECELLS.FieldName).asInt32();
                    PBITEMPLATECELLS.IDPBITEMPLATE = DS_PBITEMPLATECELLS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATECELLS.IDPBITEMPLATE.FieldName).asInt32();
                    PBITEMPLATECELLS.IDPBITEMPLATECELLS_PARENT = DS_PBITEMPLATECELLS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATECELLS.IDPBITEMPLATECELLS_PARENT.FieldName).asInt32();
                    PBITEMPLATECELLS.CELLS_POSITION = DS_PBITEMPLATECELLS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATECELLS.CELLS_POSITION.FieldName).asInt32();
                    PBITEMPLATECELLS.CELLS_END = DS_PBITEMPLATECELLS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATECELLS.CELLS_END.FieldName).asBoolean();
                    PBITEMPLATECELLS.CELLS_NAME = DS_PBITEMPLATECELLS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATECELLS.CELLS_NAME.FieldName).asString();

                    //Other
                    PBITEMPLATECELLSList.push(PBITEMPLATECELLS);
                    DS_PBITEMPLATECELLS.DataSet.Next();
                }
            }
            else {
                DS_PBITEMPLATECELLS.ResErr.NotError = false;
                DS_PBITEMPLATECELLS.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_PBITEMPLATECELLS.js Persistence.PBITemplate.Methods.PBITEMPLATECELLS_Fill", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.PBITemplate.Methods.PBITEMPLATECELLS_GETID = function (PBITEMPLATECELLS) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLS.IDPBITEMPLATECELLS.FieldName, PBITEMPLATECELLS.IDPBITEMPLATECELLS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLS.IDPBITEMPLATE.FieldName, PBITEMPLATECELLS.IDPBITEMPLATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLS.IDPBITEMPLATECELLS_PARENT.FieldName, PBITEMPLATECELLS.IDPBITEMPLATECELLS_PARENT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLS.CELLS_POSITION.FieldName, PBITEMPLATECELLS.CELLS_POSITION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddBoolean(UsrCfg.InternoAtisNames.PBITEMPLATECELLS.CELLS_END.FieldName, PBITEMPLATECELLS.CELLS_END, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.PBITEMPLATECELLS.CELLS_NAME.FieldName, PBITEMPLATECELLS.CELLS_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

        /*
        //********   PBITEMPLATECELLS_GETID   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PBITEMPLATECELLS_GETID", @"PBITEMPLATECELLS_GETID description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   IDPBITEMPLATECELLS "); 
         UnSQL.SQL.Add(@"   FROM PBITEMPLATECELLS "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDPBITEMPLATE=@[IDPBITEMPLATE] And "); 
         UnSQL.SQL.Add(@"   IDPBITEMPLATECELLS_PARENT=@[IDPBITEMPLATECELLS_PARENT] And "); 
         UnSQL.SQL.Add(@"   CELLS_POSITION=@[CELLS_POSITION] And "); 
         UnSQL.SQL.Add(@"   CELLS_END=@[CELLS_END] And "); 
         UnSQL.SQL.Add(@"   CELLS_NAME=@[CELLS_NAME] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_PBITEMPLATECELLS = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "PBITEMPLATECELLS_GETID", Param.ToBytes());
        ResErr = DS_PBITEMPLATECELLS.ResErr;
        if (ResErr.NotError) {
            if (DS_PBITEMPLATECELLS.DataSet.RecordCount > 0) {
                DS_PBITEMPLATECELLS.DataSet.First();
                PBITEMPLATECELLS.IDPBITEMPLATECELLS = DS_PBITEMPLATECELLS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATECELLS.IDPBITEMPLATECELLS.FieldName).asInt32();
            }
            else {
                DS_PBITEMPLATECELLS.ResErr.NotError = false;
                DS_PBITEMPLATECELLS.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_PBITEMPLATECELLS.js Persistence.PBITemplate.Methods.PBITEMPLATECELLS_GETID", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.PBITemplate.Methods.PBITEMPLATECELLS_ADD = function (PBITEMPLATECELLS) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLS.IDPBITEMPLATECELLS.FieldName, PBITEMPLATECELLS.IDPBITEMPLATECELLS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLS.IDPBITEMPLATE.FieldName, PBITEMPLATECELLS.IDPBITEMPLATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLS.IDPBITEMPLATECELLS_PARENT.FieldName, PBITEMPLATECELLS.IDPBITEMPLATECELLS_PARENT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLS.CELLS_POSITION.FieldName, PBITEMPLATECELLS.CELLS_POSITION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.PBITEMPLATECELLS.CELLS_END.FieldName, PBITEMPLATECELLS.CELLS_END, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PBITEMPLATECELLS.CELLS_NAME.FieldName, PBITEMPLATECELLS.CELLS_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        /* 
        //********   PBITEMPLATECELLS_ADD   ************************* 
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PBITEMPLATECELLS_ADD", @"PBITEMPLATECELLS_ADD description."); 
        UnSQL.SQL.Add(@"   INSERT INTO PBITEMPLATECELLS( "); 
         UnSQL.SQL.Add(@"   IDPBITEMPLATE, "); 
         UnSQL.SQL.Add(@"   IDPBITEMPLATECELLS_PARENT, "); 
         UnSQL.SQL.Add(@"   CELLS_POSITION, "); 
         UnSQL.SQL.Add(@"   CELLS_END, "); 
         UnSQL.SQL.Add(@"   CELLS_NAME "); 
         UnSQL.SQL.Add(@"   ) Values ( ");  
        UnSQL.SQL.Add(@"   @[IDPBITEMPLATE], "); 
         UnSQL.SQL.Add(@"   @[IDPBITEMPLATECELLS_PARENT], "); 
         UnSQL.SQL.Add(@"   @[CELLS_POSITION], "); 
         UnSQL.SQL.Add(@"   @[CELLS_END], "); 
         UnSQL.SQL.Add(@"   @[CELLS_NAME] "); 
         UnSQL.SQL.Add(@"   )  "); 
        UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PBITEMPLATECELLS_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.PBITemplate.Methods.PBITEMPLATECELLS_GETID(PBITEMPLATECELLS);
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_PBITEMPLATECELLS.js Persistence.PBITemplate.Methods.PBITEMPLATECELLS_ADD", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.PBITemplate.Methods.PBITEMPLATECELLS_UPD = function (PBITEMPLATECELLS) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLS.IDPBITEMPLATECELLS.FieldName, PBITEMPLATECELLS.IDPBITEMPLATECELLS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLS.IDPBITEMPLATE.FieldName, PBITEMPLATECELLS.IDPBITEMPLATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLS.IDPBITEMPLATECELLS_PARENT.FieldName, PBITEMPLATECELLS.IDPBITEMPLATECELLS_PARENT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLS.CELLS_POSITION.FieldName, PBITEMPLATECELLS.CELLS_POSITION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.PBITEMPLATECELLS.CELLS_END.FieldName, PBITEMPLATECELLS.CELLS_END, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PBITEMPLATECELLS.CELLS_NAME.FieldName, PBITEMPLATECELLS.CELLS_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);


        /*   
        //********   PBITEMPLATECELLS_UPD   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PBITEMPLATECELLS_UPD", @"PBITEMPLATECELLS_UPD description.");  
        UnSQL.SQL.Add(@"  UPDATE PBITEMPLATECELLS Set "); 
         UnSQL.SQL.Add(@"   IDPBITEMPLATE=@[IDPBITEMPLATE], "); 
         UnSQL.SQL.Add(@"   IDPBITEMPLATECELLS_PARENT=@[IDPBITEMPLATECELLS_PARENT], "); 
         UnSQL.SQL.Add(@"   CELLS_POSITION=@[CELLS_POSITION], "); 
         UnSQL.SQL.Add(@"   CELLS_END=@[CELLS_END], "); 
         UnSQL.SQL.Add(@"   CELLS_NAME=@[CELLS_NAME] "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDPBITEMPLATECELLS=@[IDPBITEMPLATECELLS] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PBITEMPLATECELLS_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_PBITEMPLATECELLS.js Persistence.PBITemplate.Methods.PBITEMPLATECELLS_UPD", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.PBITemplate.Methods.PBITEMPLATECELLS_DEL = function (IDPBITEMPLATECELLS) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDPBITEMPLATECELLS == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.PBITEMPLATECELLS.IDPBITEMPLATECELLS.FieldName, " = " + UsrCfg.InternoAtisNames.PBITEMPLATECELLS.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PBITEMPLATECELLS.IDPBITEMPLATECELLS.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDPBITEMPLATECELLS = " IN (" + StrIDPBITEMPLATECELLS + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.PBITEMPLATECELLS.IDPBITEMPLATECELLS.FieldName, StrIDPBITEMPLATECELLS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLS.IDPBITEMPLATECELLS.FieldName, IDPBITEMPLATECELLS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   PBITEMPLATECELLS_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PBITEMPLATECELLS_DEL", @"PBITEMPLATECELLS_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE PBITEMPLATECELLS "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDPBITEMPLATECELLS=@[IDPBITEMPLATECELLS]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PBITEMPLATECELLS_DEL", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_PBITEMPLATECELLS.js Persistence.PBITemplate.Methods.PBITEMPLATECELLS_DEL", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
//**********************   METHODS   ********************************************************************************************
Persistence.PBITemplate.Methods.PBITEMPLATECELLS_ListSetID = function (PBITEMPLATECELLSList, IDPBITEMPLATECELLS) {
    for (i = 0; i < PBITEMPLATECELLSList.length; i++) {
        if (IDPBITEMPLATECELLS == PBITEMPLATECELLSList[i].IDPBITEMPLATECELLS)
            return (PBITEMPLATECELLSList[i]);
    }
    var UnPBITEMPLATECELLS = new Persistence.Properties.TPBITEMPLATECELLS;
    UnPBITEMPLATECELLS.IDPBITEMPLATECELLS = IDPBITEMPLATECELLS;
    return (UnPBITEMPLATECELLS);
}
Persistence.PBITemplate.Methods.PBITEMPLATECELLS_ListAdd = function (PBITEMPLATECELLSList, PBITEMPLATECELLS) {
    var i = Persistence.PBITemplate.Methods.PBITEMPLATECELLS_ListGetIndex(PBITEMPLATECELLSList, PBITEMPLATECELLS);
    if (i == -1) PBITEMPLATECELLSList.push(PBITEMPLATECELLS);
    else {
        PBITEMPLATECELLSList[i].IDPBITEMPLATECELLS = PBITEMPLATECELLS.IDPBITEMPLATECELLS;
        PBITEMPLATECELLSList[i].IDPBITEMPLATE = PBITEMPLATECELLS.IDPBITEMPLATE;
        PBITEMPLATECELLSList[i].IDPBITEMPLATECELLS_PARENT = PBITEMPLATECELLS.IDPBITEMPLATECELLS_PARENT;
        PBITEMPLATECELLSList[i].CELLS_POSITION = PBITEMPLATECELLS.CELLS_POSITION;
        PBITEMPLATECELLSList[i].CELLS_END = PBITEMPLATECELLS.CELLS_END;
        PBITEMPLATECELLSList[i].CELLS_NAME = PBITEMPLATECELLS.CELLS_NAME;

    }
}
//CJRC_26072018
Persistence.PBITemplate.Methods.PBITEMPLATECELLS_ListGetIndex = function (PBITEMPLATECELLSList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.PBITemplate.Methods.PBITEMPLATECELLS_ListGetIndexIDPBITEMPLATECELLS(PBITEMPLATECELLSList, Param);

    }
    else {
        Res = Persistence.PBITemplate.Methods.PBITEMPLATECELLS_ListGetIndexPBITEMPLATECELLS(PBITEMPLATECELLSList, Param) ;
    }
    return (Res);
}

Persistence.PBITemplate.Methods.PBITEMPLATECELLS_ListGetIndexPBITEMPLATECELLS = function (PBITEMPLATECELLSList, PBITEMPLATECELLS) {
    for (i = 0; i < PBITEMPLATECELLSList.length; i++) {
        if (PBITEMPLATECELLS.IDPBITEMPLATECELLS == PBITEMPLATECELLSList[i].IDPBITEMPLATECELLS)
            return (i);
    }
    return (-1);
}
Persistence.PBITemplate.Methods.PBITEMPLATECELLS_ListGetIndexIDPBITEMPLATECELLS = function (PBITEMPLATECELLSList, IDPBITEMPLATECELLS) {
    for (i = 0; i < PBITEMPLATECELLSList.length; i++) {
        if (IDPBITEMPLATECELLS == PBITEMPLATECELLSList[i].IDPBITEMPLATECELLS)
            return (i);
    }
    return (-1);
}
//STRING FUNCTION 
Persistence.PBITemplate.Methods.PBITEMPLATECELLSListtoStr = function (PBITEMPLATECELLSList) {
    var Res = Persistence.Properties._Version + "(1)0";
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(PBITEMPLATECELLSList.length, Longitud, Texto);
        for (Counter = 0; Counter < PBITEMPLATECELLSList.length; Counter++) {
            var UnPBITEMPLATECELLS = PBITEMPLATECELLSList[Counter];
            UnPBITEMPLATECELLS.ToStr(Longitud, Texto);
        }

        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
        SysCfg.Log.Methods.WriteLog("DB_PBITEMPLATECELLS.js Persistence.PBITemplate.Methods.PBITEMPLATECELLSListtoStr ", e);
    }
    return Res;
}
Persistence.PBITemplate.Methods.StrtoPBITEMPLATECELLS = function (ProtocoloStr, PBITEMPLATECELLSList) {

    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        PBITEMPLATECELLSList.length = 0;
        if (Persistence.PBITemplate.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")") - ProtocoloStr.indexOf("(") - 1);
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length - ProtocoloStr.indexOf(")") - 1);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnPBITEMPLATECELLS = new Persistence.Properties.TPBITEMPLATECELLS(); //Mode new row
                UnPBITEMPLATECELLS.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.PBITemplate.PBITEMPLATECELLS_ListAdd(PBITEMPLATECELLSList, UnPBITEMPLATECELLS);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
        SysCfg.Log.Methods.WriteLog("DB_PBITEMPLATECELLS.js Persistence.PBITemplate.Methods.StrtoPBITEMPLATECELLS ", e);
    }
    return (Res);
}
//SOCKET FUNCTION 
Persistence.PBITemplate.Methods.PBITEMPLATECELLSListToByte = function (PBITEMPLATECELLSList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, PBITEMPLATECELLSList.Count - idx);
    for (i = idx; i < PBITEMPLATECELLSList.length; i++) {
        PBITEMPLATECELLSList[i].ToByte(MemStream);
    }
}
Persistence.PBITemplate.Methods.ByteToPBITEMPLATECELLSList = function (PBITEMPLATECELLSList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnPBITEMPLATECELLS = new Persistence.Properties.TPBITEMPLATECELLS();
        UnPBITEMPLATECELLS.ByteTo(MemStream);
        Methods.PBITEMPLATECELLS_ListAdd(PBITEMPLATECELLSList, UnPBITEMPLATECELLS);
    }
}