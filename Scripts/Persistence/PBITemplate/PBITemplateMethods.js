﻿Persistence.PBITemplate.Methods.StartReation = function (PBITemplateProfiler, inIDPBITEMPLATE) {
    try {
        var PBITEMPLATEListOrder = SysCfg.CopyList(PBITemplateProfiler.PBITEMPLATEList).sort(function (a, b) { return a.IDPBITEMPLATE - b.IDPBITEMPLATE; }); //ORDENA POR ID TEMPLATE
        var PBITEMPLATECELLSListOrder = SysCfg.CopyList(PBITemplateProfiler.PBITEMPLATECELLSList).sort(function (a, b) { return a.IDPBITEMPLATE - b.IDPBITEMPLATE; }); //ORDENA POR ID TEMPLATECELL
        var IDPBITEMPLATE = inIDPBITEMPLATE; //Todos o id solo actualiza seleccionado. todos los id -1. en caso pase el id ordena segun el id  
        var idxPBITEMPLATECELLS = 0;
        for (var i = 0; i < PBITEMPLATEListOrder.length; i++) {
            if ((IDPBITEMPLATE == -1) || (IDPBITEMPLATE == PBITEMPLATEListOrder[i].IDPBITEMPLATE)) {//--1 siempre entra al if
                for (var x = idxPBITEMPLATECELLS; x < PBITEMPLATECELLSListOrder.length; x++) {//recorre el objeto del las celdas
                    idxPBITEMPLATECELLS = x;//asigna la posicion del creccorrdio al idxPBITEMPLATECELLS para saber donde se ha quedado
                    if (PBITEMPLATECELLSListOrder[x].IDPBITEMPLATE == PBITEMPLATEListOrder[i].IDPBITEMPLATE) { //if PBITEMPLATECELLSListOrder(referencia) == PBITEMPLATEListOrder (referencia)
                        PBITEMPLATEListOrder[i].PBITEMPLATECELLSList.push(PBITEMPLATECELLSListOrder[x]);//voy agregar las celdas a PBITEMPLATECELLSList
                        PBITEMPLATECELLSListOrder[x].PBITEMPLATE = PBITEMPLATEListOrder[i]; //agrego el objeto
                        if (PBITEMPLATECELLSListOrder[x].IDPBITEMPLATECELLS_PARENT == 0) {
                            PBITEMPLATEListOrder[i].PBITEMPLATECELLSPARENTList.push(PBITEMPLATECELLSListOrder[x]); //agrego las celdas padres;
                        }
                    }
                    if (PBITEMPLATECELLSListOrder[x].IDPBITEMPLATE > PBITEMPLATEListOrder[i].IDPBITEMPLATE) {
                        break;
                    }
                }
            }
        }
        var PBITEMPLATECELLSListOrder = SysCfg.CopyList(PBITemplateProfiler.PBITEMPLATECELLSList).sort(function (a, b) { return a.IDPBITEMPLATECELLS - b.IDPBITEMPLATECELLS; });
        var PBITEMPLATECELLSPARENTListOrder = SysCfg.CopyList(PBITEMPLATECELLSListOrder).sort(function (a, b) { return a.IDPBITEMPLATECELLS_PARENT - b.IDPBITEMPLATECELLS_PARENT || a.CELLS_POSITION - b.CELLS_POSITION; });
        var idxPBITEMPLATEPARENT = 0;
        var PBITEMPLATECELLSDESIGNListOrder = SysCfg.CopyList(PBITemplateProfiler.PBITEMPLATECELLSDESIGNList).sort(function (a, b) { return a.IDPBITEMPLATECELLS - b.IDPBITEMPLATECELLS; });
        var idxPBITEMPLATECELLSDESIGN = 0;
        var PBITEMPLATECELLGRAPHICListOrder = SysCfg.CopyList(PBITemplateProfiler.PBITEMPLATECELLGRAPHICList).sort(function (a, b) { return a.IDPBITEMPLATECELLS - b.IDPBITEMPLATECELLS; });
        var idxPBITEMPLATECELLGRAPHIC = 0;
        for (var i = 0; i < PBITEMPLATECELLSListOrder.length; i++) {
            for (var x = idxPBITEMPLATEPARENT; x < PBITEMPLATECELLSPARENTListOrder.length; x++) {
                idxPBITEMPLATEPARENT = x;
                if (PBITEMPLATECELLSPARENTListOrder[x].IDPBITEMPLATECELLS_PARENT == PBITEMPLATECELLSListOrder[i].IDPBITEMPLATECELLS) {
                    PBITEMPLATECELLSListOrder[i].PBITEMPLATECELLS_CHILDList.push(PBITEMPLATECELLSPARENTListOrder[x]);
                    PBITEMPLATECELLSPARENTListOrder[x].PBITEMPLATECELLS_PARENT = PBITEMPLATECELLSListOrder[i];
                }
                if (PBITEMPLATECELLSPARENTListOrder[x].IDPBITEMPLATECELLS_PARENT > PBITEMPLATECELLSListOrder[i].IDPBITEMPLATECELLS) {
                    break;
                }
            }
            for (var x = idxPBITEMPLATECELLSDESIGN; x < PBITEMPLATECELLSDESIGNListOrder.length; x++) {
                idxPBITEMPLATECELLSDESIGN = x;
                if (PBITEMPLATECELLSDESIGNListOrder[x].IDPBITEMPLATECELLS == PBITEMPLATECELLSListOrder[i].IDPBITEMPLATECELLS) {
                    PBITEMPLATECELLSListOrder[i].PBITEMPLATECELLSDESIGN = PBITEMPLATECELLSDESIGNListOrder[x];
                    PBITEMPLATECELLSDESIGNListOrder[x].PBITEMPLATECELLS = PBITEMPLATECELLSListOrder[i];
                }
                if (PBITEMPLATECELLSDESIGNListOrder[x].IDPBITEMPLATECELLS > PBITEMPLATECELLSListOrder[i].IDPBITEMPLATECELLS) {
                    break;
                }
            }
            for (var x = idxPBITEMPLATECELLGRAPHIC; x < PBITEMPLATECELLGRAPHICListOrder.length; x++) {
                idxPBITEMPLATECELLGRAPHIC = x;
                if (PBITEMPLATECELLGRAPHICListOrder[x].IDPBITEMPLATECELLS == PBITEMPLATECELLSListOrder[i].IDPBITEMPLATECELLS) {
                    PBITEMPLATECELLSListOrder[i].PBITEMPLATECELLGRAPHIC = PBITEMPLATECELLGRAPHICListOrder[x];
                    PBITEMPLATECELLGRAPHICListOrder[x].PBITEMPLATECELLS = PBITEMPLATECELLSListOrder[i];
                }
                if (PBITEMPLATECELLGRAPHICListOrder[x].IDPBITEMPLATECELLS > PBITEMPLATECELLSListOrder[i].IDPBITEMPLATECELLS) {
                    break;
                }
            }
        }
        var PBITEMPLATECELLGRAPHICListOrder = SysCfg.CopyList(PBITemplateProfiler.PBITEMPLATECELLGRAPHICList).sort(function (a, b) { return a.IDPBITEMPLATECELLGRAPHIC - b.IDPBITEMPLATECELLGRAPHIC; });
        var PBITEMPLATECELLGRAPHIC_COLUMNListOrder = SysCfg.CopyList(PBITemplateProfiler.PBITEMPLATECELLGRAPHIC_COLUMNList).sort(function (a, b) { return a.IDPBITEMPLATECELLGRAPHIC - b.IDPBITEMPLATECELLGRAPHIC || a.IDPBITEMPLATECELLGRAPHIC_COLUMN - b.IDPBITEMPLATECELLGRAPHIC_COLUMN; });
        var idxPBITEMPLATECELLGRAPHIC_COLUMN = 0;
        for (var i = 0; i < PBITEMPLATECELLGRAPHICListOrder.length; i++) {
            for (var x = idxPBITEMPLATECELLGRAPHIC_COLUMN; x < PBITEMPLATECELLGRAPHIC_COLUMNListOrder.length; x++) {
                idxPBITEMPLATECELLGRAPHIC_COLUMN = x;
                if (PBITEMPLATECELLGRAPHIC_COLUMNListOrder[x].IDPBITEMPLATECELLGRAPHIC == PBITEMPLATECELLGRAPHICListOrder[i].IDPBITEMPLATECELLGRAPHIC) {
                    PBITEMPLATECELLGRAPHICListOrder[i].PBITEMPLATECELLGRAPHIC_COLUMNList.push(PBITEMPLATECELLGRAPHIC_COLUMNListOrder[x]);
                    PBITEMPLATECELLGRAPHIC_COLUMNListOrder[x].PBITEMPLATECELLGRAPHIC = PBITEMPLATECELLGRAPHICListOrder[i];

                }
                if (PBITEMPLATECELLGRAPHIC_COLUMNListOrder[x].IDPBITEMPLATECELLGRAPHIC > PBITEMPLATECELLGRAPHICListOrder[i].IDPBITEMPLATECELLGRAPHIC) {
                    break;
                }
            }
        }
        var PBIGRAPHICTYPEListOrder = SysCfg.CopyList(PBITemplateProfiler.PBIGRAPHICTYPEList).sort(function (a, b) { return a.IDPBIGRAPHICTYPE - b.IDPBIGRAPHICTYPE; });
        var PBITEMPLATECELLGRAPHICListOrder = SysCfg.CopyList(PBITemplateProfiler.PBITEMPLATECELLGRAPHICList).sort(function (a, b) { return a.IDPBIGRAPHICTYPE - b.IDPBIGRAPHICTYPE; });
        var idxPBITEMPLATECELLGRAPHIC = 0;
        for (var i = 0; i < PBIGRAPHICTYPEListOrder.length; i++) {
            for (var x = idxPBITEMPLATECELLGRAPHIC; x < PBITEMPLATECELLGRAPHICListOrder.length; x++) {
                idxPBITEMPLATECELLGRAPHIC = x;
                if (PBITEMPLATECELLGRAPHICListOrder[x].IDPBIGRAPHICTYPE == PBIGRAPHICTYPEListOrder[i].IDPBIGRAPHICTYPE) {
                    PBIGRAPHICTYPEListOrder[i].PBITEMPLATECELLGRAPHICList.push(PBITEMPLATECELLGRAPHICListOrder[x]);
                    PBITEMPLATECELLGRAPHICListOrder[x].PBIGRAPHICTYPE = PBIGRAPHICTYPEListOrder[i];

                }
                if (PBITEMPLATECELLGRAPHICListOrder[x].IDPBIGRAPHICTYPE > PBIGRAPHICTYPEListOrder[i].IDPBIGRAPHICTYPE) {
                    break;
                }
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("PBITemplateMethods.js Persistence.PBITemplate.Methods.StartReation ", e);
    }
};


