//**********************   PROPERTIES   *****************************************************************************************
Persistence.PBITemplate.Properties.TPBITEMPLATECELLGRAPHIC_COLUMN = function () {
    this.IDPBITEMPLATECELLGRAPHIC_COLUMN = 0;
    this.IDPBITEMPLATECELLGRAPHIC = 0;
    this.COLUMN_NAME = "";
    /*VIRTUALES*/
    this.PBITEMPLATECELLGRAPHIC = null;
    /*END VIRTUALES*/
    //SOCKET IO PROPERTIES
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDPBITEMPLATECELLGRAPHIC_COLUMN);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDPBITEMPLATECELLGRAPHIC);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.COLUMN_NAME);

    }
    this.ByteTo = function (MemStream) {
        this.IDPBITEMPLATECELLGRAPHIC_COLUMN = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDPBITEMPLATECELLGRAPHIC = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.COLUMN_NAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);

    }
    //STR IO PROPERTIES
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDPBITEMPLATECELLGRAPHIC_COLUMN, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDPBITEMPLATECELLGRAPHIC, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.COLUMN_NAME, Longitud, Texto);

    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDPBITEMPLATECELLGRAPHIC_COLUMN = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDPBITEMPLATECELLGRAPHIC = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.COLUMN_NAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);

    }
}
//**********************   METHODS server  ********************************************************************************************
Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_COLUMN_Fill = function (PBITEMPLATECELLGRAPHIC_COLUMNList, StrIDPBITEMPLATECELLGRAPHIC_COLUMN/*noin IDPBITEMPLATECELLGRAPHIC_COLUMN*/) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    PBITEMPLATECELLGRAPHIC_COLUMNList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDPBITEMPLATECELLGRAPHIC_COLUMN == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC_COLUMN.FieldName, " = " + UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC_COLUMN.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC_COLUMN.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDPBITEMPLATECELLGRAPHIC_COLUMN = " IN (" + StrIDPBITEMPLATECELLGRAPHIC_COLUMN + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC_COLUMN.FieldName, StrIDPBITEMPLATECELLGRAPHIC_COLUMN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //noin Param.AddInt32(UsrCfg.InternoPBITemplateNames.PBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC_COLUMN.FieldName, IDPBITEMPLATECELLGRAPHIC_COLUMN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {
        /*
        //********   PBITEMPLATECELLGRAPHIC_COLUMN_GET   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PBITEMPLATECELLGRAPHIC_COLUMN_GET", @"PBITEMPLATECELLGRAPHIC_COLUMN_GET description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   IDPBITEMPLATECELLGRAPHIC_COLUMN, "); 
         UnSQL.SQL.Add(@"   IDPBITEMPLATECELLGRAPHIC, "); 
         UnSQL.SQL.Add(@"   COLUMN_NAME "); 
         UnSQL.SQL.Add(@"   FROM PBITEMPLATECELLGRAPHIC_COLUMN "); 
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_PBITEMPLATECELLGRAPHIC_COLUMN = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "PBITEMPLATECELLGRAPHIC_COLUMN_GET", Param.ToBytes());
        ResErr = DS_PBITEMPLATECELLGRAPHIC_COLUMN.ResErr;
        if (ResErr.NotError) {
            if (DS_PBITEMPLATECELLGRAPHIC_COLUMN.DataSet.RecordCount > 0) {
                DS_PBITEMPLATECELLGRAPHIC_COLUMN.DataSet.First();
                while (!(DS_PBITEMPLATECELLGRAPHIC_COLUMN.DataSet.Eof)) {
                    var PBITEMPLATECELLGRAPHIC_COLUMN = new Persistence.PBITemplate.Properties.TPBITEMPLATECELLGRAPHIC_COLUMN();
                    PBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC_COLUMN = DS_PBITEMPLATECELLGRAPHIC_COLUMN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC_COLUMN.FieldName).asInt32();
                    PBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC = DS_PBITEMPLATECELLGRAPHIC_COLUMN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC.FieldName).asInt32();
                    PBITEMPLATECELLGRAPHIC_COLUMN.COLUMN_NAME = DS_PBITEMPLATECELLGRAPHIC_COLUMN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC_COLUMN.COLUMN_NAME.FieldName).asString();

                    //Other
                    PBITEMPLATECELLGRAPHIC_COLUMNList.push(PBITEMPLATECELLGRAPHIC_COLUMN);
                    DS_PBITEMPLATECELLGRAPHIC_COLUMN.DataSet.Next();
                }
            }
            else {
                DS_PBITEMPLATECELLGRAPHIC_COLUMN.ResErr.NotError = false;
                DS_PBITEMPLATECELLGRAPHIC_COLUMN.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    catch (e) { SysCfg.Log.Methods.WriteLog("DB_PBITEMPLATECELLGRAPHIC_COLUMN.js Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_COLUMN_Fill", e);} finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_COLUMN_GETID = function (PBITEMPLATECELLGRAPHIC_COLUMN) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC_COLUMN.FieldName, PBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC_COLUMN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC.FieldName, PBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC_COLUMN.COLUMN_NAME.FieldName, PBITEMPLATECELLGRAPHIC_COLUMN.COLUMN_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

        /*
        //********   PBITEMPLATECELLGRAPHIC_COLUMN_GETID   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PBITEMPLATECELLGRAPHIC_COLUMN_GETID", @"PBITEMPLATECELLGRAPHIC_COLUMN_GETID description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   IDPBITEMPLATECELLGRAPHIC_COLUMN "); 
         UnSQL.SQL.Add(@"   FROM PBITEMPLATECELLGRAPHIC_COLUMN "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDPBITEMPLATECELLGRAPHIC=@[IDPBITEMPLATECELLGRAPHIC] And "); 
         UnSQL.SQL.Add(@"   COLUMN_NAME=@[COLUMN_NAME] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_PBITEMPLATECELLGRAPHIC_COLUMN = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "PBITEMPLATECELLGRAPHIC_COLUMN_GETID", Param.ToBytes());
        ResErr = DS_PBITEMPLATECELLGRAPHIC_COLUMN.ResErr;
        if (ResErr.NotError) {
            if (DS_PBITEMPLATECELLGRAPHIC_COLUMN.DataSet.RecordCount > 0) {
                DS_PBITEMPLATECELLGRAPHIC_COLUMN.DataSet.First();
                PBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC_COLUMN = DS_PBITEMPLATECELLGRAPHIC_COLUMN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC_COLUMN.FieldName).asInt32();
            }
            else {
                DS_PBITEMPLATECELLGRAPHIC_COLUMN.ResErr.NotError = false;
                DS_PBITEMPLATECELLGRAPHIC_COLUMN.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    catch (e) { SysCfg.Log.Methods.WriteLog("DB_PBITEMPLATECELLGRAPHIC_COLUMN.js Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_COLUMN_GETID", e);} finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_COLUMN_ADD = function (PBITEMPLATECELLGRAPHIC_COLUMN) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC_COLUMN.FieldName, PBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC_COLUMN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC.FieldName, PBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC_COLUMN.COLUMN_NAME.FieldName, PBITEMPLATECELLGRAPHIC_COLUMN.COLUMN_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        /* 
        //********   PBITEMPLATECELLGRAPHIC_COLUMN_ADD   ************************* 
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PBITEMPLATECELLGRAPHIC_COLUMN_ADD", @"PBITEMPLATECELLGRAPHIC_COLUMN_ADD description."); 
        UnSQL.SQL.Add(@"   INSERT INTO PBITEMPLATECELLGRAPHIC_COLUMN( "); 
         UnSQL.SQL.Add(@"   IDPBITEMPLATECELLGRAPHIC, "); 
         UnSQL.SQL.Add(@"   COLUMN_NAME "); 
         UnSQL.SQL.Add(@"   ) Values ( ");  
        UnSQL.SQL.Add(@"   @[IDPBITEMPLATECELLGRAPHIC], "); 
         UnSQL.SQL.Add(@"   @[COLUMN_NAME] "); 
         UnSQL.SQL.Add(@"   )  "); 
        UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PBITEMPLATECELLGRAPHIC_COLUMN_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_COLUMN_GETID(PBITEMPLATECELLGRAPHIC_COLUMN);
        }
    }
    catch (e) { SysCfg.Log.Methods.WriteLog("DB_PBITEMPLATECELLGRAPHIC_COLUMN.js Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_COLUMN_ADD", e);} finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_COLUMN_UPD = function (PBITEMPLATECELLGRAPHIC_COLUMN) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC_COLUMN.FieldName, PBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC_COLUMN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC.FieldName, PBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC_COLUMN.COLUMN_NAME.FieldName, PBITEMPLATECELLGRAPHIC_COLUMN.COLUMN_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);


        /*   
        //********   PBITEMPLATECELLGRAPHIC_COLUMN_UPD   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PBITEMPLATECELLGRAPHIC_COLUMN_UPD", @"PBITEMPLATECELLGRAPHIC_COLUMN_UPD description.");  
        UnSQL.SQL.Add(@"  UPDATE PBITEMPLATECELLGRAPHIC_COLUMN Set "); 
         UnSQL.SQL.Add(@"   IDPBITEMPLATECELLGRAPHIC=@[IDPBITEMPLATECELLGRAPHIC], "); 
         UnSQL.SQL.Add(@"   COLUMN_NAME=@[COLUMN_NAME] "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDPBITEMPLATECELLGRAPHIC_COLUMN=@[IDPBITEMPLATECELLGRAPHIC_COLUMN] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PBITEMPLATECELLGRAPHIC_COLUMN_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    catch (e) { SysCfg.Log.Methods.WriteLog("DB_PBITEMPLATECELLGRAPHIC_COLUMN.js Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_COLUMN_UPD", e);} finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_COLUMN_DEL = function (IDPBITEMPLATECELLGRAPHIC_COLUMN) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDPBITEMPLATECELLGRAPHIC_COLUMN == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC_COLUMN.FieldName, " = " + UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC_COLUMN.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC_COLUMN.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDPBITEMPLATECELLGRAPHIC_COLUMN = " IN (" + StrIDPBITEMPLATECELLGRAPHIC_COLUMN + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC_COLUMN.FieldName, StrIDPBITEMPLATECELLGRAPHIC_COLUMN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC_COLUMN.FieldName, IDPBITEMPLATECELLGRAPHIC_COLUMN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   PBITEMPLATECELLGRAPHIC_COLUMN_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PBITEMPLATECELLGRAPHIC_COLUMN_DEL", @"PBITEMPLATECELLGRAPHIC_COLUMN_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE PBITEMPLATECELLGRAPHIC_COLUMN "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDPBITEMPLATECELLGRAPHIC_COLUMN=@[IDPBITEMPLATECELLGRAPHIC_COLUMN]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PBITEMPLATECELLGRAPHIC_COLUMN_DEL", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e) { SysCfg.Log.Methods.WriteLog("DB_PBITEMPLATECELLGRAPHIC_COLUMN.js Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_COLUMN_DEL", e);} finally {
        Param.Destroy();
    }
    return (ResErr);
}
//**********************   METHODS   ********************************************************************************************
Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_COLUMN_ListSetID = function (PBITEMPLATECELLGRAPHIC_COLUMNList, IDPBITEMPLATECELLGRAPHIC_COLUMN) {
    for (i = 0; i < PBITEMPLATECELLGRAPHIC_COLUMNList.length; i++) {
        if (IDPBITEMPLATECELLGRAPHIC_COLUMN == PBITEMPLATECELLGRAPHIC_COLUMNList[i].IDPBITEMPLATECELLGRAPHIC_COLUMN)
            return (PBITEMPLATECELLGRAPHIC_COLUMNList[i]);
    }
    var UnPBITEMPLATECELLGRAPHIC_COLUMN = new Persistence.PBITemplate.Properties.TPBITEMPLATECELLGRAPHIC_COLUMN;
    UnPBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC_COLUMN = IDPBITEMPLATECELLGRAPHIC_COLUMN;
    return (UnPBITEMPLATECELLGRAPHIC_COLUMN);
}
Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_COLUMN_ListAdd = function (PBITEMPLATECELLGRAPHIC_COLUMNList, PBITEMPLATECELLGRAPHIC_COLUMN) {
    var i = Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_COLUMN_ListGetIndex(PBITEMPLATECELLGRAPHIC_COLUMNList, PBITEMPLATECELLGRAPHIC_COLUMN);
    if (i == -1) PBITEMPLATECELLGRAPHIC_COLUMNList.push(PBITEMPLATECELLGRAPHIC_COLUMN);
    else {
        PBITEMPLATECELLGRAPHIC_COLUMNList[i].IDPBITEMPLATECELLGRAPHIC_COLUMN = PBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC_COLUMN;
        PBITEMPLATECELLGRAPHIC_COLUMNList[i].IDPBITEMPLATECELLGRAPHIC = PBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC;
        PBITEMPLATECELLGRAPHIC_COLUMNList[i].COLUMN_NAME = PBITEMPLATECELLGRAPHIC_COLUMN.COLUMN_NAME;

    }
}
//CJRC_26072018
Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_COLUMN_ListGetIndex = function (PBITEMPLATECELLGRAPHIC_COLUMNList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_COLUMN_ListGetIndexIDPBITEMPLATECELLGRAPHIC_COLUMN(PBITEMPLATECELLGRAPHIC_COLUMNList, Param);

    }
    else {
        Res = Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_COLUMN_ListGetIndexPBITEMPLATECELLGRAPHIC_COLUMN(PBITEMPLATECELLGRAPHIC_COLUMNList, Param);
    }
    return (Res);
}

Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_COLUMN_ListGetIndexPBITEMPLATECELLGRAPHIC_COLUMN = function (PBITEMPLATECELLGRAPHIC_COLUMNList, PBITEMPLATECELLGRAPHIC_COLUMN) {
    for (i = 0; i < PBITEMPLATECELLGRAPHIC_COLUMNList.length; i++) {
        if (PBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC_COLUMN == PBITEMPLATECELLGRAPHIC_COLUMNList[i].IDPBITEMPLATECELLGRAPHIC_COLUMN)
            return (i);
    }
    return (-1);
}
Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_COLUMN_ListGetIndexIDPBITEMPLATECELLGRAPHIC_COLUMN = function (PBITEMPLATECELLGRAPHIC_COLUMNList, IDPBITEMPLATECELLGRAPHIC_COLUMN) {
    for (i = 0; i < PBITEMPLATECELLGRAPHIC_COLUMNList.length; i++) {
        if (IDPBITEMPLATECELLGRAPHIC_COLUMN == PBITEMPLATECELLGRAPHIC_COLUMNList[i].IDPBITEMPLATECELLGRAPHIC_COLUMN)
            return (i);
    }
    return (-1);
}
//STRING FUNCTION 
Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_COLUMNListtoStr = function (PBITEMPLATECELLGRAPHIC_COLUMNList) {
    var Res = Persistence.Properties._Version + "(1)0";
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(PBITEMPLATECELLGRAPHIC_COLUMNList.length, Longitud, Texto);
        for (Counter = 0; Counter < PBITEMPLATECELLGRAPHIC_COLUMNList.length; Counter++) {
            var UnPBITEMPLATECELLGRAPHIC_COLUMN = PBITEMPLATECELLGRAPHIC_COLUMNList[Counter];
            UnPBITEMPLATECELLGRAPHIC_COLUMN.ToStr(Longitud, Texto);
        }

        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
        SysCfg.Log.Methods.WriteLog("DB_PBITEMPLATECELLGRAPHIC_COLUMN.js Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_COLUMNListtoStr ", e);
    }
    return Res;
}
Persistence.PBITemplate.Methods.StrtoPBITEMPLATECELLGRAPHIC_COLUMN = function (ProtocoloStr, PBITEMPLATECELLGRAPHIC_COLUMNList) {

    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        PBITEMPLATECELLGRAPHIC_COLUMNList.length = 0;
        if (Persistence.PBITemplate.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")") - ProtocoloStr.indexOf("(") - 1);
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length - ProtocoloStr.indexOf(")") - 1);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnPBITEMPLATECELLGRAPHIC_COLUMN = new Persistence.PBITemplate.Properties.TPBITEMPLATECELLGRAPHIC_COLUMN(); //Mode new row
                UnPBITEMPLATECELLGRAPHIC_COLUMN.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.PBITemplate.PBITEMPLATECELLGRAPHIC_COLUMN_ListAdd(PBITEMPLATECELLGRAPHIC_COLUMNList, UnPBITEMPLATECELLGRAPHIC_COLUMN);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
        SysCfg.Log.Methods.WriteLog("DB_PBITEMPLATECELLGRAPHIC_COLUMN.js Persistence.PBITemplate.Methods.StrtoPBITEMPLATECELLGRAPHIC_COLUMN ", e);
    }
    return (Res);
}
//SOCKET FUNCTION 
Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_COLUMNListToByte = function (PBITEMPLATECELLGRAPHIC_COLUMNList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, PBITEMPLATECELLGRAPHIC_COLUMNList.Count - idx);
    for (i = idx; i < PBITEMPLATECELLGRAPHIC_COLUMNList.length; i++) {
        PBITEMPLATECELLGRAPHIC_COLUMNList[i].ToByte(MemStream);
    }
}
Persistence.PBITemplate.Methods.ByteToPBITEMPLATECELLGRAPHIC_COLUMNList = function (PBITEMPLATECELLGRAPHIC_COLUMNList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnPBITEMPLATECELLGRAPHIC_COLUMN = new Persistence.PBITemplate.Properties.TPBITEMPLATECELLGRAPHIC_COLUMN();
        UnPBITEMPLATECELLGRAPHIC_COLUMN.ByteTo(MemStream);
        Methods.PBITEMPLATECELLGRAPHIC_COLUMN_ListAdd(PBITEMPLATECELLGRAPHIC_COLUMNList, UnPBITEMPLATECELLGRAPHIC_COLUMN);
    }
}