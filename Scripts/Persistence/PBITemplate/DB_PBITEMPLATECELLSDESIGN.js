//**********************   PROPERTIES   *****************************************************************************************
Persistence.PBITemplate.Properties.TPBITEMPLATECELLSDESIGN = function () {
    this.IDPBITEMPLATECELLSDESIGN = 0;
    this.IDPBITEMPLATECELLS = 0;
    this.DESIGN_XS = 0;
    this.DESIGN_S = 0;
    this.DESIGN_M = 0;
    this.DESIGN_L = 0;
    this.DESIGN_XL = 0;
    this.DESIGN_ROW = false;
    this.DESIGN_COLUMN = false;
    /*VIRTUALES*/
    this.PBITEMPLATECELLS = {};/*new Persistence.PBITemplate.Properties.TPBITEMPLATECELLS();/*1 dise�o pertence 1 celda y 1 celda pertenece a un dise�o*/
    /*END VIRTUALES*/
    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDPBITEMPLATECELLSDESIGN);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDPBITEMPLATECELLS);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.DESIGN_XS);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.DESIGN_S);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.DESIGN_M);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.DESIGN_L);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.DESIGN_XL);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.DESIGN_ROW);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.DESIGN_COLUMN);

    }
    this.ByteTo = function (MemStream) {
        this.IDPBITEMPLATECELLSDESIGN = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDPBITEMPLATECELLS = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.DESIGN_XS = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.DESIGN_S = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.DESIGN_M = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.DESIGN_L = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.DESIGN_XL = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.DESIGN_ROW = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.DESIGN_COLUMN = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);

    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDPBITEMPLATECELLSDESIGN, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDPBITEMPLATECELLS, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.DESIGN_XS, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.DESIGN_S, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.DESIGN_M, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.DESIGN_L, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.DESIGN_XL, Longitud, Texto);
        SysCfg.Str.Protocol.WriteBool(this.DESIGN_ROW, Longitud, Texto);
        SysCfg.Str.Protocol.WriteBool(this.DESIGN_COLUMN, Longitud, Texto);

    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDPBITEMPLATECELLSDESIGN = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDPBITEMPLATECELLS = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.DESIGN_XS = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.DESIGN_S = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.DESIGN_M = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.DESIGN_L = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.DESIGN_XL = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.DESIGN_ROW = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);
        this.DESIGN_COLUMN = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);

    }
}
//**********************   METHODS server  ********************************************************************************************
Persistence.PBITemplate.Methods.PBITEMPLATECELLSDESIGN_Fill = function (PBITEMPLATECELLSDESIGNList, StrIDPBITEMPLATECELLSDESIGN/*noin IDPBITEMPLATECELLSDESIGN*/) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    PBITEMPLATECELLSDESIGNList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDPBITEMPLATECELLSDESIGN == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLSDESIGN.FieldName, " = " + UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLSDESIGN.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDPBITEMPLATECELLSDESIGN = " IN (" + StrIDPBITEMPLATECELLSDESIGN + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLSDESIGN.FieldName, StrIDPBITEMPLATECELLSDESIGN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //noin Param.AddInt32(UsrCfg.InternoDemoNames.PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLSDESIGN.FieldName, IDPBITEMPLATECELLSDESIGN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {
        /*
        //********   PBITEMPLATECELLSDESIGN_GET   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PBITEMPLATECELLSDESIGN_GET", @"PBITEMPLATECELLSDESIGN_GET description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   IDPBITEMPLATECELLSDESIGN, "); 
         UnSQL.SQL.Add(@"   IDPBITEMPLATECELLS, "); 
         UnSQL.SQL.Add(@"   DESIGN_XS, "); 
         UnSQL.SQL.Add(@"   DESIGN_S, "); 
         UnSQL.SQL.Add(@"   DESIGN_M, "); 
         UnSQL.SQL.Add(@"   DESIGN_L, "); 
         UnSQL.SQL.Add(@"   DESIGN_XL, "); 
         UnSQL.SQL.Add(@"   DESIGN_ROW, "); 
         UnSQL.SQL.Add(@"   DESIGN_COLUMN "); 
         UnSQL.SQL.Add(@"   FROM PBITEMPLATECELLSDESIGN "); 
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_PBITEMPLATECELLSDESIGN = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "PBITEMPLATECELLSDESIGN_GET", Param.ToBytes());
        ResErr = DS_PBITEMPLATECELLSDESIGN.ResErr;
        if (ResErr.NotError) {
            if (DS_PBITEMPLATECELLSDESIGN.DataSet.RecordCount > 0) {
                DS_PBITEMPLATECELLSDESIGN.DataSet.First();
                while (!(DS_PBITEMPLATECELLSDESIGN.DataSet.Eof)) {
                    var PBITEMPLATECELLSDESIGN = new Persistence.PBITemplate.Properties.TPBITEMPLATECELLSDESIGN();
                    PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLSDESIGN = DS_PBITEMPLATECELLSDESIGN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLSDESIGN.FieldName).asInt32();
                    PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLS = DS_PBITEMPLATECELLSDESIGN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLS.FieldName).asInt32();
                    PBITEMPLATECELLSDESIGN.DESIGN_XS = DS_PBITEMPLATECELLSDESIGN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_XS.FieldName).asInt32();
                    PBITEMPLATECELLSDESIGN.DESIGN_S = DS_PBITEMPLATECELLSDESIGN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_S.FieldName).asInt32();
                    PBITEMPLATECELLSDESIGN.DESIGN_M = DS_PBITEMPLATECELLSDESIGN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_M.FieldName).asInt32();
                    PBITEMPLATECELLSDESIGN.DESIGN_L = DS_PBITEMPLATECELLSDESIGN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_L.FieldName).asInt32();
                    PBITEMPLATECELLSDESIGN.DESIGN_XL = DS_PBITEMPLATECELLSDESIGN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_XL.FieldName).asInt32();
                    PBITEMPLATECELLSDESIGN.DESIGN_ROW = DS_PBITEMPLATECELLSDESIGN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_ROW.FieldName).asBoolean();
                    PBITEMPLATECELLSDESIGN.DESIGN_COLUMN = DS_PBITEMPLATECELLSDESIGN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_COLUMN.FieldName).asBoolean();

                    //Other
                    PBITEMPLATECELLSDESIGNList.push(PBITEMPLATECELLSDESIGN);
                    DS_PBITEMPLATECELLSDESIGN.DataSet.Next();
                }
            }
            else {
                DS_PBITEMPLATECELLSDESIGN.ResErr.NotError = false;
                DS_PBITEMPLATECELLSDESIGN.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_PBITEMPLATECELLSDESIGN.js Persistence.PBITemplate.Methods.PBITEMPLATECELLSDESIGN_Fill", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.PBITemplate.Methods.PBITEMPLATECELLSDESIGN_GETID = function (PBITEMPLATECELLSDESIGN) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLSDESIGN.FieldName, PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLSDESIGN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLS.FieldName, PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_XS.FieldName, PBITEMPLATECELLSDESIGN.DESIGN_XS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_S.FieldName, PBITEMPLATECELLSDESIGN.DESIGN_S, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_M.FieldName, PBITEMPLATECELLSDESIGN.DESIGN_M, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_L.FieldName, PBITEMPLATECELLSDESIGN.DESIGN_L, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_XL.FieldName, PBITEMPLATECELLSDESIGN.DESIGN_XL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddBoolean(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_ROW.FieldName, PBITEMPLATECELLSDESIGN.DESIGN_ROW, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddBoolean(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_COLUMN.FieldName, PBITEMPLATECELLSDESIGN.DESIGN_COLUMN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

        /*
        //********   PBITEMPLATECELLSDESIGN_GETID   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PBITEMPLATECELLSDESIGN_GETID", @"PBITEMPLATECELLSDESIGN_GETID description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   IDPBITEMPLATECELLSDESIGN "); 
         UnSQL.SQL.Add(@"   FROM PBITEMPLATECELLSDESIGN "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDPBITEMPLATECELLS=@[IDPBITEMPLATECELLS] And "); 
         UnSQL.SQL.Add(@"   DESIGN_XS=@[DESIGN_XS] And "); 
         UnSQL.SQL.Add(@"   DESIGN_S=@[DESIGN_S] And "); 
         UnSQL.SQL.Add(@"   DESIGN_M=@[DESIGN_M] And "); 
         UnSQL.SQL.Add(@"   DESIGN_L=@[DESIGN_L] And "); 
         UnSQL.SQL.Add(@"   DESIGN_XL=@[DESIGN_XL] And "); 
         UnSQL.SQL.Add(@"   DESIGN_ROW=@[DESIGN_ROW] And "); 
         UnSQL.SQL.Add(@"   DESIGN_COLUMN=@[DESIGN_COLUMN] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_PBITEMPLATECELLSDESIGN = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "PBITEMPLATECELLSDESIGN_GETID", Param.ToBytes());
        ResErr = DS_PBITEMPLATECELLSDESIGN.ResErr;
        if (ResErr.NotError) {
            if (DS_PBITEMPLATECELLSDESIGN.DataSet.RecordCount > 0) {
                DS_PBITEMPLATECELLSDESIGN.DataSet.First();
                PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLSDESIGN = DS_PBITEMPLATECELLSDESIGN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLSDESIGN.FieldName).asInt32();
            }
            else {
                DS_PBITEMPLATECELLSDESIGN.ResErr.NotError = false;
                DS_PBITEMPLATECELLSDESIGN.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_PBITEMPLATECELLSDESIGN.js Persistence.PBITemplate.Methods.PBITEMPLATECELLSDESIGN_GETID", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.PBITemplate.Methods.PBITEMPLATECELLSDESIGN_ADD = function (PBITEMPLATECELLSDESIGN) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLSDESIGN.FieldName, PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLSDESIGN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLS.FieldName, PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_XS.FieldName, PBITEMPLATECELLSDESIGN.DESIGN_XS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_S.FieldName, PBITEMPLATECELLSDESIGN.DESIGN_S, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_M.FieldName, PBITEMPLATECELLSDESIGN.DESIGN_M, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_L.FieldName, PBITEMPLATECELLSDESIGN.DESIGN_L, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_XL.FieldName, PBITEMPLATECELLSDESIGN.DESIGN_XL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_ROW.FieldName, PBITEMPLATECELLSDESIGN.DESIGN_ROW, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_COLUMN.FieldName, PBITEMPLATECELLSDESIGN.DESIGN_COLUMN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        /* 
        //********   PBITEMPLATECELLSDESIGN_ADD   ************************* 
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PBITEMPLATECELLSDESIGN_ADD", @"PBITEMPLATECELLSDESIGN_ADD description."); 
        UnSQL.SQL.Add(@"   INSERT INTO PBITEMPLATECELLSDESIGN( "); 
         UnSQL.SQL.Add(@"   IDPBITEMPLATECELLS, "); 
         UnSQL.SQL.Add(@"   DESIGN_XS, "); 
         UnSQL.SQL.Add(@"   DESIGN_S, "); 
         UnSQL.SQL.Add(@"   DESIGN_M, "); 
         UnSQL.SQL.Add(@"   DESIGN_L, "); 
         UnSQL.SQL.Add(@"   DESIGN_XL, "); 
         UnSQL.SQL.Add(@"   DESIGN_ROW, "); 
         UnSQL.SQL.Add(@"   DESIGN_COLUMN "); 
         UnSQL.SQL.Add(@"   ) Values ( ");  
        UnSQL.SQL.Add(@"   @[IDPBITEMPLATECELLS], "); 
         UnSQL.SQL.Add(@"   @[DESIGN_XS], "); 
         UnSQL.SQL.Add(@"   @[DESIGN_S], "); 
         UnSQL.SQL.Add(@"   @[DESIGN_M], "); 
         UnSQL.SQL.Add(@"   @[DESIGN_L], "); 
         UnSQL.SQL.Add(@"   @[DESIGN_XL], "); 
         UnSQL.SQL.Add(@"   @[DESIGN_ROW], "); 
         UnSQL.SQL.Add(@"   @[DESIGN_COLUMN] "); 
         UnSQL.SQL.Add(@"   )  "); 
        UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PBITEMPLATECELLSDESIGN_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.PBITemplate.Methods.PBITEMPLATECELLSDESIGN_GETID(PBITEMPLATECELLSDESIGN);
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_PBITEMPLATECELLSDESIGN.js Persistence.PBITemplate.Methods.PBITEMPLATECELLSDESIGN_ADD", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.PBITemplate.Methods.PBITEMPLATECELLSDESIGN_UPD = function (PBITEMPLATECELLSDESIGN) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLSDESIGN.FieldName, PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLSDESIGN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLS.FieldName, PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_XS.FieldName, PBITEMPLATECELLSDESIGN.DESIGN_XS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_S.FieldName, PBITEMPLATECELLSDESIGN.DESIGN_S, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_M.FieldName, PBITEMPLATECELLSDESIGN.DESIGN_M, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_L.FieldName, PBITEMPLATECELLSDESIGN.DESIGN_L, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_XL.FieldName, PBITEMPLATECELLSDESIGN.DESIGN_XL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_ROW.FieldName, PBITEMPLATECELLSDESIGN.DESIGN_ROW, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_COLUMN.FieldName, PBITEMPLATECELLSDESIGN.DESIGN_COLUMN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);


        /*   
        //********   PBITEMPLATECELLSDESIGN_UPD   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PBITEMPLATECELLSDESIGN_UPD", @"PBITEMPLATECELLSDESIGN_UPD description.");  
        UnSQL.SQL.Add(@"  UPDATE PBITEMPLATECELLSDESIGN Set "); 
         UnSQL.SQL.Add(@"   IDPBITEMPLATECELLS=@[IDPBITEMPLATECELLS], "); 
         UnSQL.SQL.Add(@"   DESIGN_XS=@[DESIGN_XS], "); 
         UnSQL.SQL.Add(@"   DESIGN_S=@[DESIGN_S], "); 
         UnSQL.SQL.Add(@"   DESIGN_M=@[DESIGN_M], "); 
         UnSQL.SQL.Add(@"   DESIGN_L=@[DESIGN_L], "); 
         UnSQL.SQL.Add(@"   DESIGN_XL=@[DESIGN_XL], "); 
         UnSQL.SQL.Add(@"   DESIGN_ROW=@[DESIGN_ROW], "); 
         UnSQL.SQL.Add(@"   DESIGN_COLUMN=@[DESIGN_COLUMN] "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDPBITEMPLATECELLSDESIGN=@[IDPBITEMPLATECELLSDESIGN] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PBITEMPLATECELLSDESIGN_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_PBITEMPLATECELLSDESIGN.js Persistence.PBITemplate.Methods.PBITEMPLATECELLSDESIGN_UPD", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.PBITemplate.Methods.PBITEMPLATECELLSDESIGN_DEL = function (/*String StrIDPBITEMPLATECELLSDESIGN*/IDPBITEMPLATECELLSDESIGN) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDPBITEMPLATECELLSDESIGN == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLSDESIGN.FieldName, " = " + UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLSDESIGN.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDPBITEMPLATECELLSDESIGN = " IN (" + StrIDPBITEMPLATECELLSDESIGN + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLSDESIGN.FieldName, StrIDPBITEMPLATECELLSDESIGN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLSDESIGN.FieldName, IDPBITEMPLATECELLSDESIGN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   PBITEMPLATECELLSDESIGN_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PBITEMPLATECELLSDESIGN_DEL", @"PBITEMPLATECELLSDESIGN_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE PBITEMPLATECELLSDESIGN "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDPBITEMPLATECELLSDESIGN=@[IDPBITEMPLATECELLSDESIGN]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PBITEMPLATECELLSDESIGN_DEL", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_PBITEMPLATECELLSDESIGN.js Persistence.PBITemplate.Methods.PBITEMPLATECELLSDESIGN_DEL", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
//**********************   METHODS   ********************************************************************************************
Persistence.PBITemplate.Methods.PBITEMPLATECELLSDESIGN_ListSetID = function (PBITEMPLATECELLSDESIGNList, IDPBITEMPLATECELLSDESIGN) {
    for (i = 0; i < PBITEMPLATECELLSDESIGNList.length; i++) {
        if (IDPBITEMPLATECELLSDESIGN == PBITEMPLATECELLSDESIGNList[i].IDPBITEMPLATECELLSDESIGN)
            return (PBITEMPLATECELLSDESIGNList[i]);
    }
    var UnPBITEMPLATECELLSDESIGN = new Persistence.PBITemplate.Properties.TPBITEMPLATECELLSDESIGN;
    UnPBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLSDESIGN = IDPBITEMPLATECELLSDESIGN;
    return (UnPBITEMPLATECELLSDESIGN);
}
Persistence.PBITemplate.Methods.PBITEMPLATECELLSDESIGN_ListAdd = function (PBITEMPLATECELLSDESIGNList, PBITEMPLATECELLSDESIGN) {
    var i = Persistence.PBITemplate.Methods.PBITEMPLATECELLSDESIGN_ListGetIndex(PBITEMPLATECELLSDESIGNList, PBITEMPLATECELLSDESIGN);
    if (i == -1) PBITEMPLATECELLSDESIGNList.push(PBITEMPLATECELLSDESIGN);
    else {
        PBITEMPLATECELLSDESIGNList[i].IDPBITEMPLATECELLSDESIGN = PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLSDESIGN;
        PBITEMPLATECELLSDESIGNList[i].IDPBITEMPLATECELLS = PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLS;
        PBITEMPLATECELLSDESIGNList[i].DESIGN_XS = PBITEMPLATECELLSDESIGN.DESIGN_XS;
        PBITEMPLATECELLSDESIGNList[i].DESIGN_S = PBITEMPLATECELLSDESIGN.DESIGN_S;
        PBITEMPLATECELLSDESIGNList[i].DESIGN_M = PBITEMPLATECELLSDESIGN.DESIGN_M;
        PBITEMPLATECELLSDESIGNList[i].DESIGN_L = PBITEMPLATECELLSDESIGN.DESIGN_L;
        PBITEMPLATECELLSDESIGNList[i].DESIGN_XL = PBITEMPLATECELLSDESIGN.DESIGN_XL;
        PBITEMPLATECELLSDESIGNList[i].DESIGN_ROW = PBITEMPLATECELLSDESIGN.DESIGN_ROW;
        PBITEMPLATECELLSDESIGNList[i].DESIGN_COLUMN = PBITEMPLATECELLSDESIGN.DESIGN_COLUMN;

    }
}
//CJRC_26072018
Persistence.PBITemplate.Methods.PBITEMPLATECELLSDESIGN_ListGetIndex = function (PBITEMPLATECELLSDESIGNList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.PBITemplate.Methods.PBITEMPLATECELLSDESIGN_ListGetIndexIDPBITEMPLATECELLSDESIGN(PBITEMPLATECELLSDESIGNList, Param);

    }
    else {
        Res = Persistence.PBITemplate.Methods.PBITEMPLATECELLSDESIGN_ListGetIndexPBITEMPLATECELLSDESIGN(PBITEMPLATECELLSDESIGNList, Param);
    }
    return (Res);
}

Persistence.PBITemplate.Methods.PBITEMPLATECELLSDESIGN_ListGetIndexPBITEMPLATECELLSDESIGN = function (PBITEMPLATECELLSDESIGNList, PBITEMPLATECELLSDESIGN) {
    for (i = 0; i < PBITEMPLATECELLSDESIGNList.length; i++) {
        if (PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLSDESIGN == PBITEMPLATECELLSDESIGNList[i].IDPBITEMPLATECELLSDESIGN)
            return (i);
    }
    return (-1);
}
Persistence.PBITemplate.Methods.PBITEMPLATECELLSDESIGN_ListGetIndexIDPBITEMPLATECELLSDESIGN = function (PBITEMPLATECELLSDESIGNList, IDPBITEMPLATECELLSDESIGN) {
    for (i = 0; i < PBITEMPLATECELLSDESIGNList.length; i++) {
        if (IDPBITEMPLATECELLSDESIGN == PBITEMPLATECELLSDESIGNList[i].IDPBITEMPLATECELLSDESIGN)
            return (i);
    }
    return (-1);
}
//String Function 
Persistence.PBITemplate.Methods.PBITEMPLATECELLSDESIGNListtoStr = function (PBITEMPLATECELLSDESIGNList) {
    var Res = Persistence.Properties._Version + "(1)0";
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(PBITEMPLATECELLSDESIGNList.length, Longitud, Texto);
        for (Counter = 0; Counter < PBITEMPLATECELLSDESIGNList.length; Counter++) {
            var UnPBITEMPLATECELLSDESIGN = PBITEMPLATECELLSDESIGNList[Counter];
            UnPBITEMPLATECELLSDESIGN.ToStr(Longitud, Texto);
        }

        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
        SysCfg.Log.Methods.WriteLog("DB_PBITEMPLATECELLSDESIGN.js Persistence.PBITemplate.Methods.PBITEMPLATECELLSDESIGNListtoStr ", e);
    }
    return Res;
}
Persistence.PBITemplate.Methods.StrtoPBITEMPLATECELLSDESIGN = function (ProtocoloStr, PBITEMPLATECELLSDESIGNList) {

    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        PBITEMPLATECELLSDESIGNList.length = 0;
        if (Persistence.PBITemplate.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")") - ProtocoloStr.indexOf("(") - 1);
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length - ProtocoloStr.indexOf(")") - 1);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnPBITEMPLATECELLSDESIGN = new Persistence.PBITemplate.Properties.TPBITEMPLATECELLSDESIGN(); //Mode new row
                UnPBITEMPLATECELLSDESIGN.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.PBITemplate.PBITEMPLATECELLSDESIGN_ListAdd(PBITEMPLATECELLSDESIGNList, UnPBITEMPLATECELLSDESIGN);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
        SysCfg.Log.Methods.WriteLog("DB_PBITEMPLATECELLSDESIGN.js Persistence.PBITemplate.Methods.StrtoPBITEMPLATECELLSDESIGN ", e);
    }
    return (Res);
}
//Socket Function 
Persistence.PBITemplate.Methods.PBITEMPLATECELLSDESIGNListToByte = function (PBITEMPLATECELLSDESIGNList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, PBITEMPLATECELLSDESIGNList.Count - idx);
    for (i = idx; i < PBITEMPLATECELLSDESIGNList.length; i++) {
        PBITEMPLATECELLSDESIGNList[i].ToByte(MemStream);
    }
}
Persistence.PBITemplate.Methods.ByteToPBITEMPLATECELLSDESIGNList = function (PBITEMPLATECELLSDESIGNList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnPBITEMPLATECELLSDESIGN = new Persistence.PBITemplate.Properties.TPBITEMPLATECELLSDESIGN();
        UnPBITEMPLATECELLSDESIGN.ByteTo(MemStream);
        Methods.PBITEMPLATECELLSDESIGN_ListAdd(PBITEMPLATECELLSDESIGNList, UnPBITEMPLATECELLSDESIGN);
    }
}
