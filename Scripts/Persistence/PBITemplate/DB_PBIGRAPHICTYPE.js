//**********************   PROPERTIES   *****************************************************************************************
Persistence.PBITemplate.Properties.TPBIGRAPHICTYPE = function () {
    this.IDPBIGRAPHICTYPE = 0;
    this.GRAPHIC_NAME = "";
    this.GRAPHIC_DESCRIPTION = "";
    /*VIRTUALES*/
    this.PBITEMPLATECELLGRAPHICList = new Array();/*1 tipo de grafico pertence a muchos celdas de grafico 1 celda de grafico le pertenece 1 celda grafico*/
    /*END VIRTUALES*/
    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDPBIGRAPHICTYPE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.GRAPHIC_NAME);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.GRAPHIC_DESCRIPTION);

    }
    this.ByteTo = function (MemStream) {
        this.IDPBIGRAPHICTYPE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.GRAPHIC_NAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.GRAPHIC_DESCRIPTION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);

    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDPBIGRAPHICTYPE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.GRAPHIC_NAME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.GRAPHIC_DESCRIPTION, Longitud, Texto);

    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDPBIGRAPHICTYPE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.GRAPHIC_NAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.GRAPHIC_DESCRIPTION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);

    }
}
//**********************   METHODS server  ********************************************************************************************
Persistence.PBITemplate.Methods.PBIGRAPHICTYPE_Fill = function (PBIGRAPHICTYPEList, StrIDPBIGRAPHICTYPE/*noin IDPBIGRAPHICTYPE*/) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    PBIGRAPHICTYPEList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDPBIGRAPHICTYPE == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.PBIGRAPHICTYPE.IDPBIGRAPHICTYPE.FieldName, " = " + UsrCfg.InternoAtisNames.PBIGRAPHICTYPE.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PBIGRAPHICTYPE.IDPBIGRAPHICTYPE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDPBIGRAPHICTYPE = " IN (" + StrIDPBIGRAPHICTYPE + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.PBIGRAPHICTYPE.IDPBIGRAPHICTYPE.FieldName, StrIDPBIGRAPHICTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //noin Param.AddInt32(UsrCfg.InternoDemoNames.PBIGRAPHICTYPE.IDPBIGRAPHICTYPE.FieldName, IDPBIGRAPHICTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {
        /*
        //********   PBIGRAPHICTYPE_GET   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PBIGRAPHICTYPE_GET", @"PBIGRAPHICTYPE_GET description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   IDPBIGRAPHICTYPE, "); 
         UnSQL.SQL.Add(@"   GRAPHIC_NAME, "); 
         UnSQL.SQL.Add(@"   GRAPHIC_DESCRIPTION "); 
         UnSQL.SQL.Add(@"   FROM PBIGRAPHICTYPE "); 
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_PBIGRAPHICTYPE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "PBIGRAPHICTYPE_GET", Param.ToBytes());
        ResErr = DS_PBIGRAPHICTYPE.ResErr;
        if (ResErr.NotError) {
            if (DS_PBIGRAPHICTYPE.DataSet.RecordCount > 0) {
                DS_PBIGRAPHICTYPE.DataSet.First();
                while (!(DS_PBIGRAPHICTYPE.DataSet.Eof)) {
                    var PBIGRAPHICTYPE = new Persistence.PBITemplate.Properties.TPBIGRAPHICTYPE();
                    PBIGRAPHICTYPE.IDPBIGRAPHICTYPE = DS_PBIGRAPHICTYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBIGRAPHICTYPE.IDPBIGRAPHICTYPE.FieldName).asInt32();
                    PBIGRAPHICTYPE.GRAPHIC_NAME = DS_PBIGRAPHICTYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBIGRAPHICTYPE.GRAPHIC_NAME.FieldName).asString();
                    PBIGRAPHICTYPE.GRAPHIC_DESCRIPTION = DS_PBIGRAPHICTYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBIGRAPHICTYPE.GRAPHIC_DESCRIPTION.FieldName).asText();

                    //Other
                    PBIGRAPHICTYPEList.push(PBIGRAPHICTYPE);
                    DS_PBIGRAPHICTYPE.DataSet.Next();
                }
            }
            else {
                DS_PBIGRAPHICTYPE.ResErr.NotError = false;
                DS_PBIGRAPHICTYPE.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    catch (e){
        SysCfg.Log.Methods.WriteLog("DB_PBIGRAPHICTYPE.js Persistence.PBITemplate.Methods.PBIGRAPHICTYPE_Fill", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.PBITemplate.Methods.PBIGRAPHICTYPE_GETID = function (PBIGRAPHICTYPE) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.PBIGRAPHICTYPE.IDPBIGRAPHICTYPE.FieldName, PBIGRAPHICTYPE.IDPBIGRAPHICTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.PBIGRAPHICTYPE.GRAPHIC_NAME.FieldName, PBIGRAPHICTYPE.GRAPHIC_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddText(UsrCfg.InternoAtisNames.PBIGRAPHICTYPE.GRAPHIC_DESCRIPTION.FieldName, PBIGRAPHICTYPE.GRAPHIC_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

        /*
        //********   PBIGRAPHICTYPE_GETID   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PBIGRAPHICTYPE_GETID", @"PBIGRAPHICTYPE_GETID description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   IDPBIGRAPHICTYPE "); 
         UnSQL.SQL.Add(@"   FROM PBIGRAPHICTYPE "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   GRAPHIC_NAME=@[GRAPHIC_NAME] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_PBIGRAPHICTYPE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "PBIGRAPHICTYPE_GETID", Param.ToBytes());
        ResErr = DS_PBIGRAPHICTYPE.ResErr;
        if (ResErr.NotError) {
            if (DS_PBIGRAPHICTYPE.DataSet.RecordCount > 0) {
                DS_PBIGRAPHICTYPE.DataSet.First();
                PBIGRAPHICTYPE.IDPBIGRAPHICTYPE = DS_PBIGRAPHICTYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBIGRAPHICTYPE.IDPBIGRAPHICTYPE.FieldName).asInt32();
            }
            else {
                DS_PBIGRAPHICTYPE.ResErr.NotError = false;
                DS_PBIGRAPHICTYPE.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_PBIGRAPHICTYPE.js Persistence.PBITemplate.Methods.PBIGRAPHICTYPE_GETID", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.PBITemplate.Methods.PBIGRAPHICTYPE_ADD = function (PBIGRAPHICTYPE) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.PBIGRAPHICTYPE.IDPBIGRAPHICTYPE.FieldName, PBIGRAPHICTYPE.IDPBIGRAPHICTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PBIGRAPHICTYPE.GRAPHIC_NAME.FieldName, PBIGRAPHICTYPE.GRAPHIC_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.PBIGRAPHICTYPE.GRAPHIC_DESCRIPTION.FieldName, PBIGRAPHICTYPE.GRAPHIC_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        /* 
        //********   PBIGRAPHICTYPE_ADD   ************************* 
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PBIGRAPHICTYPE_ADD", @"PBIGRAPHICTYPE_ADD description."); 
        UnSQL.SQL.Add(@"   INSERT INTO PBIGRAPHICTYPE( "); 
         UnSQL.SQL.Add(@"   GRAPHIC_NAME, "); 
         UnSQL.SQL.Add(@"   GRAPHIC_DESCRIPTION "); 
         UnSQL.SQL.Add(@"   ) Values ( ");  
        UnSQL.SQL.Add(@"   @[GRAPHIC_NAME], "); 
         UnSQL.SQL.Add(@"   @[GRAPHIC_DESCRIPTION] "); 
         UnSQL.SQL.Add(@"   )  "); 
        UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PBIGRAPHICTYPE_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.PBITemplate.Methods.PBIGRAPHICTYPE_GETID(PBIGRAPHICTYPE);
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_PBIGRAPHICTYPE.js Persistence.PBITemplate.Methods.PBIGRAPHICTYPE_ADD", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.PBITemplate.Methods.PBIGRAPHICTYPE_UPD = function (PBIGRAPHICTYPE) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.PBIGRAPHICTYPE.IDPBIGRAPHICTYPE.FieldName, PBIGRAPHICTYPE.IDPBIGRAPHICTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PBIGRAPHICTYPE.GRAPHIC_NAME.FieldName, PBIGRAPHICTYPE.GRAPHIC_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.PBIGRAPHICTYPE.GRAPHIC_DESCRIPTION.FieldName, PBIGRAPHICTYPE.GRAPHIC_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);


        /*   
        //********   PBIGRAPHICTYPE_UPD   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PBIGRAPHICTYPE_UPD", @"PBIGRAPHICTYPE_UPD description.");  
        UnSQL.SQL.Add(@"  UPDATE PBIGRAPHICTYPE Set "); 
         UnSQL.SQL.Add(@"   GRAPHIC_NAME=@[GRAPHIC_NAME], "); 
         UnSQL.SQL.Add(@"   GRAPHIC_DESCRIPTION=@[GRAPHIC_DESCRIPTION] "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDPBIGRAPHICTYPE=@[IDPBIGRAPHICTYPE] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PBIGRAPHICTYPE_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_PBIGRAPHICTYPE.js Persistence.PBITemplate.Methods.PBIGRAPHICTYPE_UPD", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.PBITemplate.Methods.PBIGRAPHICTYPE_DEL = function (/*String StrIDPBIGRAPHICTYPE*/IDPBIGRAPHICTYPE) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDPBIGRAPHICTYPE == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.PBIGRAPHICTYPE.IDPBIGRAPHICTYPE.FieldName, " = " + UsrCfg.InternoAtisNames.PBIGRAPHICTYPE.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PBIGRAPHICTYPE.IDPBIGRAPHICTYPE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDPBIGRAPHICTYPE = " IN (" + StrIDPBIGRAPHICTYPE + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.PBIGRAPHICTYPE.IDPBIGRAPHICTYPE.FieldName, StrIDPBIGRAPHICTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.PBIGRAPHICTYPE.IDPBIGRAPHICTYPE.FieldName, IDPBIGRAPHICTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   PBIGRAPHICTYPE_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PBIGRAPHICTYPE_DEL", @"PBIGRAPHICTYPE_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE PBIGRAPHICTYPE "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDPBIGRAPHICTYPE=@[IDPBIGRAPHICTYPE]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PBIGRAPHICTYPE_DEL", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_PBIGRAPHICTYPE.js Persistence.PBITemplate.Methods.PBIGRAPHICTYPE_DEL", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
//**********************   METHODS   ********************************************************************************************
/*ENVIA LA LISTA Y EL ID, EN CASO ENCUENTRE DEVUELVE EL OBJETO, EN CASO NO ENCUENTRE  CREA UN OBJETO SETEANDO EL ID PASADO  Y LO DEVUELVE.*/
Persistence.PBITemplate.Methods.PBIGRAPHICTYPE_ListSetID = function (PBIGRAPHICTYPEList, IDPBIGRAPHICTYPE) {
    for (i = 0; i < PBIGRAPHICTYPEList.length; i++) {
        if (IDPBIGRAPHICTYPE == PBIGRAPHICTYPEList[i].IDPBIGRAPHICTYPE)
            return (PBIGRAPHICTYPEList[i]);
    }
    var UnPBIGRAPHICTYPE = new Persistence.Properties.TPBIGRAPHICTYPE;
    UnPBIGRAPHICTYPE.IDPBIGRAPHICTYPE = IDPBIGRAPHICTYPE;
    return (UnPBIGRAPHICTYPE);
}

//AGREGA A A LA LISTA SIN GUARDAR EN LA BASE DE DATOS.//AGREGA A LA LISTA SI NO EXISTE, EN CASO EXISTE ACTUALIZA EL OBJETO.
Persistence.PBITemplate.Methods.PBIGRAPHICTYPE_ListAdd = function (PBIGRAPHICTYPEList, PBIGRAPHICTYPE) {//LISTA Y OBJETO
    var i = Persistence.PBITemplate.Methods.PBIGRAPHICTYPE_ListGetIndex(PBIGRAPHICTYPEList, PBIGRAPHICTYPE);//VERIFICA SI EXISTE EL OBJETOEN LA LISTA MEDIANTE EL ID
    if (i == -1) PBIGRAPHICTYPEList.push(PBIGRAPHICTYPE);// EN CASO NO EXISTE AGREGA A LA LITSA
    else {
        PBIGRAPHICTYPEList[i].IDPBIGRAPHICTYPE = PBIGRAPHICTYPE.IDPBIGRAPHICTYPE;
        PBIGRAPHICTYPEList[i].GRAPHIC_NAME = PBIGRAPHICTYPE.GRAPHIC_NAME;
        PBIGRAPHICTYPEList[i].GRAPHIC_DESCRIPTION = PBIGRAPHICTYPE.GRAPHIC_DESCRIPTION;

    }
}
//CJRC_26072018
Persistence.PBITemplate.Methods.PBIGRAPHICTYPE_ListGetIndex = function (PBIGRAPHICTYPEList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.PBITemplate.Methods.PBIGRAPHICTYPE_ListGetIndexIDPBIGRAPHICTYPE(PBIGRAPHICTYPEList, Param);

    }
    else {
        Res = Persistence.PBITemplate.Methods.PBIGRAPHICTYPE_ListGetIndexPBIGRAPHICTYPE(PBIGRAPHICTYPEList, Param);
    }
    return (Res);
}

Persistence.PBITemplate.Methods.PBIGRAPHICTYPE_ListGetIndexPBIGRAPHICTYPE = function (PBIGRAPHICTYPEList, PBIGRAPHICTYPE) {
    for (i = 0; i < PBIGRAPHICTYPEList.length; i++) {
        if (PBIGRAPHICTYPE.IDPBIGRAPHICTYPE == PBIGRAPHICTYPEList[i].IDPBIGRAPHICTYPE)
            return (i);
    }
    return (-1);
}//VERIFICA SI EXISTE EL OBJETOEN LA LISTA MEDIANTE EL ID
Persistence.PBITemplate.Methods.PBIGRAPHICTYPE_ListGetIndexIDPBIGRAPHICTYPE = function (PBIGRAPHICTYPEList, IDPBIGRAPHICTYPE) {
    for (i = 0; i < PBIGRAPHICTYPEList.length; i++) {
        if (IDPBIGRAPHICTYPE == PBIGRAPHICTYPEList[i].IDPBIGRAPHICTYPE)
            return (i);
    }
    return (-1);
}//VERIFICA SI EL ID EXISTE EN LA LISTA
//String Function 
Persistence.PBITemplate.Methods.PBIGRAPHICTYPEListtoStr = function (PBIGRAPHICTYPEList) {
    var Res = Persistence.Properties._Version + "(1)0";
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(PBIGRAPHICTYPEList.length, Longitud, Texto);
        for (Counter = 0; Counter < PBIGRAPHICTYPEList.length; Counter++) {
            var UnPBIGRAPHICTYPE = PBIGRAPHICTYPEList[Counter];
            UnPBIGRAPHICTYPE.ToStr(Longitud, Texto);
        }

        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
        SysCfg.Log.Methods.WriteLog("DB_PBIGRAPHICTYPE.js Persistence.PBITemplate.Methods.PBIGRAPHICTYPEListtoStr ", e);
    }
    return Res;
}
Persistence.PBITemplate.Methods.StrtoPBIGRAPHICTYPE = function (ProtocoloStr, PBIGRAPHICTYPEList) {

    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        PBIGRAPHICTYPEList.length = 0;
        if (Persistence.PBITemplate.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")") - ProtocoloStr.indexOf("(") - 1);
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length - ProtocoloStr.indexOf(")") - 1);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnPBIGRAPHICTYPE = new Persistence.Properties.TPBIGRAPHICTYPE(); //Mode new row
                UnPBIGRAPHICTYPE.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.PBITemplate.PBIGRAPHICTYPE_ListAdd(PBIGRAPHICTYPEList, UnPBIGRAPHICTYPE);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
        SysCfg.Log.Methods.WriteLog("DB_PBIGRAPHICTYPE.js Persistence.PBITemplate.Methods.StrtoPBIGRAPHICTYPE ", e);
    }
    return (Res);
}
//Socket Function 
Persistence.PBITemplate.Methods.PBIGRAPHICTYPEListToByte = function (PBIGRAPHICTYPEList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, PBIGRAPHICTYPEList.Count - idx);
    for (i = idx; i < PBIGRAPHICTYPEList.length; i++) {
        PBIGRAPHICTYPEList[i].ToByte(MemStream);
    }
}
Persistence.PBITemplate.Methods.ByteToPBIGRAPHICTYPEList = function (PBIGRAPHICTYPEList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnPBIGRAPHICTYPE = new Persistence.Properties.TPBIGRAPHICTYPE();
        UnPBIGRAPHICTYPE.ByteTo(MemStream);
        Methods.PBIGRAPHICTYPE_ListAdd(PBIGRAPHICTYPEList, UnPBIGRAPHICTYPE);
    }
}
