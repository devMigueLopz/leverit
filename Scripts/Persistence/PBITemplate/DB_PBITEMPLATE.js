//**********************   PROPERTIES   *****************************************************************************************
Persistence.PBITemplate.Properties.TPBITEMPLATE = function () {
    this.IDPBITEMPLATE = 0;
    this.TEMPLATE_NAME = "";
    this.TEMPLATE_DESCRIPTION = "";
    this.TEMPLATE_DATASQL = "";
    this.TEMPLATE_REFRESHTIMEENABLE = false;
    this.TEMPLATE_REFRESHTIME = 0;
    this.TEMPLATE_PATH = "";
    /*VIRTUALES*/
    this.PBITEMPLATECELLSList = new Array(); /*CELL CHILD 1 TEMPLATE PUEDE TENER MUCHAS CELDAS Y 1 DETERMINADA CELDA PUEDE ESTAR EN UN TEMPLATE*/
    this.PBITEMPLATECELLSPARENTList = new Array();/*CELL PARENT 1 TEMPLATE PUEDE TENER MUCHAS CELDAS PADRES Y 1 DETERMINADA CELDA PADRE PUEDE ESTAR EN UN TEMPLATE*/
    /*END VIRTUALES*/
    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDPBITEMPLATE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.TEMPLATE_NAME);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.TEMPLATE_DESCRIPTION);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.TEMPLATE_DATASQL);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.TEMPLATE_REFRESHTIMEENABLE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.TEMPLATE_REFRESHTIME);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.TEMPLATE_PATH);

    }
    this.ByteTo = function (MemStream) {
        this.IDPBITEMPLATE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.TEMPLATE_NAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.TEMPLATE_DESCRIPTION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.TEMPLATE_DATASQL = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.TEMPLATE_REFRESHTIMEENABLE = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.TEMPLATE_REFRESHTIME = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.TEMPLATE_PATH = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);

    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDPBITEMPLATE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.TEMPLATE_NAME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.TEMPLATE_DESCRIPTION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.TEMPLATE_DATASQL, Longitud, Texto);
        SysCfg.Str.Protocol.WriteBool(this.TEMPLATE_REFRESHTIMEENABLE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.TEMPLATE_REFRESHTIME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.TEMPLATE_PATH, Longitud, Texto);

    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDPBITEMPLATE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.TEMPLATE_NAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.TEMPLATE_DESCRIPTION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.TEMPLATE_DATASQL = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.TEMPLATE_REFRESHTIMEENABLE = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);
        this.TEMPLATE_REFRESHTIME = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.TEMPLATE_PATH = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);

    }
}
//**********************   METHODS SERVER  ********************************************************************************************
Persistence.PBITemplate.Methods.PBITEMPLATE_Fill = function (PBITEMPLATEList, StrIDPBITEMPLATE/*noin IDPBITEMPLATE*/) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    PBITEMPLATEList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDPBITEMPLATE == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.PBITEMPLATE.IDPBITEMPLATE.FieldName, " = " + UsrCfg.InternoAtisNames.PBITEMPLATE.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PBITEMPLATE.IDPBITEMPLATE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDPBITEMPLATE = " IN (" + StrIDPBITEMPLATE + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.PBITEMPLATE.IDPBITEMPLATE.FieldName, StrIDPBITEMPLATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //noin Param.AddInt32(UsrCfg.InternoDemoNames.PBITEMPLATE.IDPBITEMPLATE.FieldName, IDPBITEMPLATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {
        /*
        //********   PBITEMPLATE_GET   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PBITEMPLATE_GET", @"PBITEMPLATE_GET description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   IDPBITEMPLATE, "); 
         UnSQL.SQL.Add(@"   TEMPLATE_NAME, "); 
         UnSQL.SQL.Add(@"   TEMPLATE_DESCRIPTION, "); 
         UnSQL.SQL.Add(@"   TEMPLATE_DATASQL, "); 
         UnSQL.SQL.Add(@"   TEMPLATE_REFRESHTIMEENABLE, "); 
         UnSQL.SQL.Add(@"   TEMPLATE_REFRESHTIME, "); 
         UnSQL.SQL.Add(@"   TEMPLATE_PATH "); 
         UnSQL.SQL.Add(@"   FROM PBITEMPLATE "); 
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_PBITEMPLATE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "PBITEMPLATE_GET", Param.ToBytes());
        ResErr = DS_PBITEMPLATE.ResErr;
        if (ResErr.NotError) {
            if (DS_PBITEMPLATE.DataSet.RecordCount > 0) {
                DS_PBITEMPLATE.DataSet.First();
                while (!(DS_PBITEMPLATE.DataSet.Eof)) {
                    var PBITEMPLATE = new Persistence.PBITemplate.Properties.TPBITEMPLATE();
                    PBITEMPLATE.IDPBITEMPLATE = DS_PBITEMPLATE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATE.IDPBITEMPLATE.FieldName).asInt32();
                    PBITEMPLATE.TEMPLATE_NAME = DS_PBITEMPLATE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_NAME.FieldName).asString();
                    PBITEMPLATE.TEMPLATE_DESCRIPTION = DS_PBITEMPLATE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_DESCRIPTION.FieldName).asText();
                    PBITEMPLATE.TEMPLATE_DATASQL = DS_PBITEMPLATE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_DATASQL.FieldName).asText();
                    PBITEMPLATE.TEMPLATE_REFRESHTIMEENABLE = DS_PBITEMPLATE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_REFRESHTIMEENABLE.FieldName).asBoolean();
                    PBITEMPLATE.TEMPLATE_REFRESHTIME = DS_PBITEMPLATE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_REFRESHTIME.FieldName).asInt32();
                    PBITEMPLATE.TEMPLATE_PATH = DS_PBITEMPLATE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_PATH.FieldName).asString();

                    //Other
                    PBITEMPLATEList.push(PBITEMPLATE);
                    DS_PBITEMPLATE.DataSet.Next();
                }
            }
            else {
                DS_PBITEMPLATE.ResErr.NotError = false;
                DS_PBITEMPLATE.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_PBITEMPLATE.js Persistence.PBITemplate.Methods.PBITEMPLATE_Fill", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.PBITemplate.Methods.PBITEMPLATE_GETID = function (PBITEMPLATE) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATE.IDPBITEMPLATE.FieldName, PBITEMPLATE.IDPBITEMPLATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_NAME.FieldName, PBITEMPLATE.TEMPLATE_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddText(UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_DESCRIPTION.FieldName, PBITEMPLATE.TEMPLATE_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddText(UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_DATASQL.FieldName, PBITEMPLATE.TEMPLATE_DATASQL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddBoolean(UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_REFRESHTIMEENABLE.FieldName, PBITEMPLATE.TEMPLATE_REFRESHTIMEENABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_REFRESHTIME.FieldName, PBITEMPLATE.TEMPLATE_REFRESHTIME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_PATH.FieldName, PBITEMPLATE.TEMPLATE_PATH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

        /*
        //********   PBITEMPLATE_GETID   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PBITEMPLATE_GETID", @"PBITEMPLATE_GETID description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   IDPBITEMPLATE "); 
         UnSQL.SQL.Add(@"   FROM PBITEMPLATE "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   TEMPLATE_NAME=@[TEMPLATE_NAME] And "); 
         UnSQL.SQL.Add(@"   TEMPLATE_REFRESHTIMEENABLE=@[TEMPLATE_REFRESHTIMEENABLE] And "); 
         UnSQL.SQL.Add(@"   TEMPLATE_REFRESHTIME=@[TEMPLATE_REFRESHTIME] And "); 
         UnSQL.SQL.Add(@"   TEMPLATE_PATH=@[TEMPLATE_PATH] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_PBITEMPLATE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "PBITEMPLATE_GETID", Param.ToBytes());
        ResErr = DS_PBITEMPLATE.ResErr;
        if (ResErr.NotError) {
            if (DS_PBITEMPLATE.DataSet.RecordCount > 0) {
                DS_PBITEMPLATE.DataSet.First();
                PBITEMPLATE.IDPBITEMPLATE = DS_PBITEMPLATE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATE.IDPBITEMPLATE.FieldName).asInt32();
            }
            else {
                DS_PBITEMPLATE.ResErr.NotError = false;
                DS_PBITEMPLATE.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_PBITEMPLATE.js Persistence.PBITemplate.Methods.PBITEMPLATE_GETID", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.PBITemplate.Methods.PBITEMPLATE_ADD = function (PBITEMPLATE) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATE.IDPBITEMPLATE.FieldName, PBITEMPLATE.IDPBITEMPLATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_NAME.FieldName, PBITEMPLATE.TEMPLATE_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_DESCRIPTION.FieldName, PBITEMPLATE.TEMPLATE_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_DATASQL.FieldName, PBITEMPLATE.TEMPLATE_DATASQL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_REFRESHTIMEENABLE.FieldName, PBITEMPLATE.TEMPLATE_REFRESHTIMEENABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_REFRESHTIME.FieldName, PBITEMPLATE.TEMPLATE_REFRESHTIME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_PATH.FieldName, PBITEMPLATE.TEMPLATE_PATH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        /* 
        //********   PBITEMPLATE_ADD   ************************* 
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PBITEMPLATE_ADD", @"PBITEMPLATE_ADD description."); 
        UnSQL.SQL.Add(@"   INSERT INTO PBITEMPLATE( "); 
         UnSQL.SQL.Add(@"   TEMPLATE_NAME, "); 
         UnSQL.SQL.Add(@"   TEMPLATE_DESCRIPTION, "); 
         UnSQL.SQL.Add(@"   TEMPLATE_DATASQL, "); 
         UnSQL.SQL.Add(@"   TEMPLATE_REFRESHTIMEENABLE, "); 
         UnSQL.SQL.Add(@"   TEMPLATE_REFRESHTIME, "); 
         UnSQL.SQL.Add(@"   TEMPLATE_PATH "); 
         UnSQL.SQL.Add(@"   ) Values ( ");  
        UnSQL.SQL.Add(@"   @[TEMPLATE_NAME], "); 
         UnSQL.SQL.Add(@"   @[TEMPLATE_DESCRIPTION], "); 
         UnSQL.SQL.Add(@"   @[TEMPLATE_DATASQL], "); 
         UnSQL.SQL.Add(@"   @[TEMPLATE_REFRESHTIMEENABLE], "); 
         UnSQL.SQL.Add(@"   @[TEMPLATE_REFRESHTIME], "); 
         UnSQL.SQL.Add(@"   @[TEMPLATE_PATH] "); 
         UnSQL.SQL.Add(@"   )  "); 
        UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PBITEMPLATE_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.PBITemplate.Methods.PBITEMPLATE_GETID(PBITEMPLATE);
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_PBITEMPLATE.js Persistence.PBITemplate.Methods.PBITEMPLATE_ADD", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.PBITemplate.Methods.PBITEMPLATE_UPD = function (PBITEMPLATE) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATE.IDPBITEMPLATE.FieldName, PBITEMPLATE.IDPBITEMPLATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_NAME.FieldName, PBITEMPLATE.TEMPLATE_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_DESCRIPTION.FieldName, PBITEMPLATE.TEMPLATE_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_DATASQL.FieldName, PBITEMPLATE.TEMPLATE_DATASQL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_REFRESHTIMEENABLE.FieldName, PBITEMPLATE.TEMPLATE_REFRESHTIMEENABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_REFRESHTIME.FieldName, PBITEMPLATE.TEMPLATE_REFRESHTIME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_PATH.FieldName, PBITEMPLATE.TEMPLATE_PATH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);


        /*   
        //********   PBITEMPLATE_UPD   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PBITEMPLATE_UPD", @"PBITEMPLATE_UPD description.");  
        UnSQL.SQL.Add(@"  UPDATE PBITEMPLATE Set "); 
         UnSQL.SQL.Add(@"   TEMPLATE_NAME=@[TEMPLATE_NAME], "); 
         UnSQL.SQL.Add(@"   TEMPLATE_DESCRIPTION=@[TEMPLATE_DESCRIPTION], "); 
         UnSQL.SQL.Add(@"   TEMPLATE_DATASQL=@[TEMPLATE_DATASQL], "); 
         UnSQL.SQL.Add(@"   TEMPLATE_REFRESHTIMEENABLE=@[TEMPLATE_REFRESHTIMEENABLE], "); 
         UnSQL.SQL.Add(@"   TEMPLATE_REFRESHTIME=@[TEMPLATE_REFRESHTIME], "); 
         UnSQL.SQL.Add(@"   TEMPLATE_PATH=@[TEMPLATE_PATH] "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDPBITEMPLATE=@[IDPBITEMPLATE] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PBITEMPLATE_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_PBITEMPLATE.js Persistence.PBITemplate.Methods.PBITEMPLATE_UPD", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.PBITemplate.Methods.PBITEMPLATE_DEL = function (IDPBITEMPLATE) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDPBITEMPLATE == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.PBITEMPLATE.IDPBITEMPLATE.FieldName, " = " + UsrCfg.InternoAtisNames.PBITEMPLATE.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PBITEMPLATE.IDPBITEMPLATE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDPBITEMPLATE = " IN (" + StrIDPBITEMPLATE + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.PBITEMPLATE.IDPBITEMPLATE.FieldName, StrIDPBITEMPLATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATE.IDPBITEMPLATE.FieldName, IDPBITEMPLATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   PBITEMPLATE_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PBITEMPLATE_DEL", @"PBITEMPLATE_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE PBITEMPLATE "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDPBITEMPLATE=@[IDPBITEMPLATE]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PBITEMPLATE_DEL", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_PBITEMPLATE.js Persistence.PBITemplate.Methods.PBITEMPLATE_DEL", e);
    }finally {
        Param.Destroy();
    }
    return (ResErr);
}
//**********************   METHODS   ********************************************************************************************
Persistence.PBITemplate.Methods.PBITEMPLATE_ListSetID = function (PBITEMPLATEList, IDPBITEMPLATE) {
    for (i = 0; i < PBITEMPLATEList.length; i++) {
        if (IDPBITEMPLATE == PBITEMPLATEList[i].IDPBITEMPLATE)
            return (PBITEMPLATEList[i]);
    }
    var UnPBITEMPLATE = new Persistence.PBITemplate.Properties.TPBITEMPLATE;
    UnPBITEMPLATE.IDPBITEMPLATE = IDPBITEMPLATE;
    return (UnPBITEMPLATE);
}
Persistence.PBITemplate.Methods.PBITEMPLATE_ListAdd = function (PBITEMPLATEList, PBITEMPLATE) {
    var i = Persistence.PBITemplate.Methods.PBITEMPLATE_ListGetIndex(PBITEMPLATEList, PBITEMPLATE);
    if (i == -1) PBITEMPLATEList.push(PBITEMPLATE);
    else {
        PBITEMPLATEList[i].IDPBITEMPLATE = PBITEMPLATE.IDPBITEMPLATE;
        PBITEMPLATEList[i].TEMPLATE_NAME = PBITEMPLATE.TEMPLATE_NAME;
        PBITEMPLATEList[i].TEMPLATE_DESCRIPTION = PBITEMPLATE.TEMPLATE_DESCRIPTION;
        PBITEMPLATEList[i].TEMPLATE_DATASQL = PBITEMPLATE.TEMPLATE_DATASQL;
        PBITEMPLATEList[i].TEMPLATE_REFRESHTIMEENABLE = PBITEMPLATE.TEMPLATE_REFRESHTIMEENABLE;
        PBITEMPLATEList[i].TEMPLATE_REFRESHTIME = PBITEMPLATE.TEMPLATE_REFRESHTIME;
        PBITEMPLATEList[i].TEMPLATE_PATH = PBITEMPLATE.TEMPLATE_PATH;

    }
}
//CJRC_26072018
Persistence.PBITemplate.Methods.PBITEMPLATE_ListGetIndex = function (PBITEMPLATEList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.PBITemplate.Methods.PBITEMPLATE_ListGetIndexIDPBITEMPLATE(PBITEMPLATEList, Param);

    }
    else {
        Res = Persistence.PBITemplate.Methods.PBITEMPLATE_ListGetIndexPBITEMPLATE(PBITEMPLATEList, Param);
    }
    return (Res);
}

Persistence.PBITemplate.Methods.PBITEMPLATE_ListGetIndexPBITEMPLATE = function (PBITEMPLATEList, PBITEMPLATE) {
    for (i = 0; i < PBITEMPLATEList.length; i++) {
        if (PBITEMPLATE.IDPBITEMPLATE == PBITEMPLATEList[i].IDPBITEMPLATE)
            return (i);
    }
    return (-1);
}
Persistence.PBITemplate.Methods.PBITEMPLATE_ListGetIndexIDPBITEMPLATE = function (PBITEMPLATEList, IDPBITEMPLATE) {
    for (i = 0; i < PBITEMPLATEList.length; i++) {
        if (IDPBITEMPLATE == PBITEMPLATEList[i].IDPBITEMPLATE)
            return (i);
    }
    return (-1);
}
//STRING FUNCTION 
Persistence.PBITemplate.Methods.PBITEMPLATEListtoStr = function (PBITEMPLATEList) {
    var Res = Persistence.Properties._Version + "(1)0";
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(PBITEMPLATEList.length, Longitud, Texto);
        for (Counter = 0; Counter < PBITEMPLATEList.length; Counter++) {
            var UnPBITEMPLATE = PBITEMPLATEList[Counter];
            UnPBITEMPLATE.ToStr(Longitud, Texto);
        }

        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
        SysCfg.Log.Methods.WriteLog("DB_PBITEMPLATE.js Persistence.PBITemplate.Methods.PBITEMPLATEListtoStr ", e);
    }
    return Res;
}
Persistence.PBITemplate.Methods.StrtoPBITEMPLATE = function (ProtocoloStr, PBITEMPLATEList) {

    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        PBITEMPLATEList.length = 0;
        if (Persistence.PBITemplate.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")") - ProtocoloStr.indexOf("(") - 1);
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length - ProtocoloStr.indexOf(")") - 1);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnPBITEMPLATE = new Persistence.Properties.TPBITEMPLATE(); //Mode new row
                UnPBITEMPLATE.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.PBITemplate.PBITEMPLATE_ListAdd(PBITEMPLATEList, UnPBITEMPLATE);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
        SysCfg.Log.Methods.WriteLog("DB_PBITEMPLATE.js Persistence.PBITemplate.Methods.StrtoPBITEMPLATE ", e);
    }
    return (Res);
}
//SOCKET FUNCTION 
Persistence.PBITemplate.Methods.PBITEMPLATEListToByte = function (PBITEMPLATEList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, PBITEMPLATEList.Count - idx);
    for (i = idx; i < PBITEMPLATEList.length; i++) {
        PBITEMPLATEList[i].ToByte(MemStream);
    }
}
Persistence.PBITemplate.Methods.ByteToPBITEMPLATEList = function (PBITEMPLATEList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnPBITEMPLATE = new Persistence.Properties.TPBITEMPLATE();
        UnPBITEMPLATE.ByteTo(MemStream);
        Methods.PBITEMPLATE_ListAdd(PBITEMPLATEList, UnPBITEMPLATE);
    }
}