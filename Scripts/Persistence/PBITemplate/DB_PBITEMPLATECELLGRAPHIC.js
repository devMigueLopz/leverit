//**********************   PROPERTIES   *****************************************************************************************
Persistence.PBITemplate.Properties.TPBITEMPLATECELLGRAPHIC = function () {
    this.IDPBITEMPLATECELLGRAPHIC = 0;
    this.IDPBITEMPLATECELLS = 0;
    this.IDPBIGRAPHICTYPE = 0;
    this.CELLGRAPHIC_VISIBLE = false;
    this.CELLGRAPHIC_TITLE = "";
    this.CELLGRAPHIC_VISIBLETITLE = false;
    this.CELLGRAPHIC_COLORTITLE = "";
    this.CELLGRAPHIC_SIZETITLE = "";
    this.CELLGRAPHIC_ALIGNTITLE = "";
    /*VIRTUALES*/
    this.PBITEMPLATECELLS = {}; /* new Persistence.PBITemplate.Properties.TPBITEMPLATECELLS();1 cellgraphics tiene 1 pbitmeplatecell;*/
    this.PBIGRAPHICTYPE = {}; /*new Persistence.PBITemplate.Properties.TPBIGRAPHICTYPE();/*1 cellgraphics tiene un 1 PBIGRAPHICTYPE;*/
    this.PBITEMPLATECELLGRAPHIC_COLUMNList = new Array();
    /*END VIRTUALES*/
    //SOCKET IO PROPERTIES
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDPBITEMPLATECELLGRAPHIC);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDPBITEMPLATECELLS);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDPBIGRAPHICTYPE);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.CELLGRAPHIC_VISIBLE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CELLGRAPHIC_TITLE);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.CELLGRAPHIC_VISIBLETITLE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CELLGRAPHIC_COLORTITLE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CELLGRAPHIC_SIZETITLE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CELLGRAPHIC_ALIGNTITLE);

    }
    this.ByteTo = function (MemStream) {
        this.IDPBITEMPLATECELLGRAPHIC = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDPBITEMPLATECELLS = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDPBIGRAPHICTYPE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.CELLGRAPHIC_VISIBLE = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.CELLGRAPHIC_TITLE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CELLGRAPHIC_VISIBLETITLE = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.CELLGRAPHIC_COLORTITLE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CELLGRAPHIC_SIZETITLE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CELLGRAPHIC_ALIGNTITLE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);

    }
    //STR IO PROPERTIES
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDPBITEMPLATECELLGRAPHIC, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDPBITEMPLATECELLS, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDPBIGRAPHICTYPE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteBool(this.CELLGRAPHIC_VISIBLE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.CELLGRAPHIC_TITLE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteBool(this.CELLGRAPHIC_VISIBLETITLE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.CELLGRAPHIC_COLORTITLE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.CELLGRAPHIC_SIZETITLE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.CELLGRAPHIC_ALIGNTITLE, Longitud, Texto);

    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDPBITEMPLATECELLGRAPHIC = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDPBITEMPLATECELLS = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDPBIGRAPHICTYPE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.CELLGRAPHIC_VISIBLE = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);
        this.CELLGRAPHIC_TITLE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.CELLGRAPHIC_VISIBLETITLE = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);
        this.CELLGRAPHIC_COLORTITLE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.CELLGRAPHIC_SIZETITLE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.CELLGRAPHIC_ALIGNTITLE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);

    }
}
//**********************   METHODS server  ********************************************************************************************
Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_Fill = function (PBITEMPLATECELLGRAPHICList, StrIDPBITEMPLATECELLGRAPHIC/*noin IDPBITEMPLATECELLGRAPHIC*/) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    PBITEMPLATECELLGRAPHICList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDPBITEMPLATECELLGRAPHIC == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLGRAPHIC.FieldName, " = " + UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLGRAPHIC.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDPBITEMPLATECELLGRAPHIC = " IN (" + StrIDPBITEMPLATECELLGRAPHIC + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLGRAPHIC.FieldName, StrIDPBITEMPLATECELLGRAPHIC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //noin Param.AddInt32(UsrCfg.InternoPBITemplateNames.PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLGRAPHIC.FieldName, IDPBITEMPLATECELLGRAPHIC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {
        /*
        //********   PBITEMPLATECELLGRAPHIC_GET   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PBITEMPLATECELLGRAPHIC_GET", @"PBITEMPLATECELLGRAPHIC_GET description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   IDPBITEMPLATECELLGRAPHIC, "); 
         UnSQL.SQL.Add(@"   IDPBITEMPLATECELLS, "); 
         UnSQL.SQL.Add(@"   IDPBIGRAPHICTYPE, "); 
         UnSQL.SQL.Add(@"   CELLGRAPHIC_VISIBLE, "); 
         UnSQL.SQL.Add(@"   CELLGRAPHIC_TITLE, "); 
         UnSQL.SQL.Add(@"   CELLGRAPHIC_VISIBLETITLE, "); 
         UnSQL.SQL.Add(@"   CELLGRAPHIC_COLORTITLE, "); 
         UnSQL.SQL.Add(@"   CELLGRAPHIC_SIZETITLE, "); 
         UnSQL.SQL.Add(@"   CELLGRAPHIC_ALIGNTITLE "); 
         UnSQL.SQL.Add(@"   FROM PBITEMPLATECELLGRAPHIC "); 
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_PBITEMPLATECELLGRAPHIC = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "PBITEMPLATECELLGRAPHIC_GET", Param.ToBytes());
        ResErr = DS_PBITEMPLATECELLGRAPHIC.ResErr;
        if (ResErr.NotError) {
            if (DS_PBITEMPLATECELLGRAPHIC.DataSet.RecordCount > 0) {
                DS_PBITEMPLATECELLGRAPHIC.DataSet.First();
                while (!(DS_PBITEMPLATECELLGRAPHIC.DataSet.Eof)) {
                    var PBITEMPLATECELLGRAPHIC = new Persistence.PBITemplate.Properties.TPBITEMPLATECELLGRAPHIC();
                    PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLGRAPHIC = DS_PBITEMPLATECELLGRAPHIC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLGRAPHIC.FieldName).asInt32();
                    PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLS = DS_PBITEMPLATECELLGRAPHIC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLS.FieldName).asInt32();
                    PBITEMPLATECELLGRAPHIC.IDPBIGRAPHICTYPE = DS_PBITEMPLATECELLGRAPHIC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.IDPBIGRAPHICTYPE.FieldName).asInt32();
                    PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_VISIBLE = DS_PBITEMPLATECELLGRAPHIC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_VISIBLE.FieldName).asBoolean();
                    PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_TITLE = DS_PBITEMPLATECELLGRAPHIC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_TITLE.FieldName).asString();
                    PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_VISIBLETITLE = DS_PBITEMPLATECELLGRAPHIC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_VISIBLETITLE.FieldName).asBoolean();
                    PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_COLORTITLE = DS_PBITEMPLATECELLGRAPHIC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_COLORTITLE.FieldName).asString();
                    PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_SIZETITLE = DS_PBITEMPLATECELLGRAPHIC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_SIZETITLE.FieldName).asString();
                    PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_ALIGNTITLE = DS_PBITEMPLATECELLGRAPHIC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_ALIGNTITLE.FieldName).asString();

                    //Other
                    PBITEMPLATECELLGRAPHICList.push(PBITEMPLATECELLGRAPHIC);
                    DS_PBITEMPLATECELLGRAPHIC.DataSet.Next();
                }
            }
            else {
                DS_PBITEMPLATECELLGRAPHIC.ResErr.NotError = false;
                DS_PBITEMPLATECELLGRAPHIC.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_PBITEMPLATECELLGRAPHIC.js Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_Fill", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_GETID = function (PBITEMPLATECELLGRAPHIC) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLGRAPHIC.FieldName, PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLGRAPHIC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLS.FieldName, PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.IDPBIGRAPHICTYPE.FieldName, PBITEMPLATECELLGRAPHIC.IDPBIGRAPHICTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddBoolean(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_VISIBLE.FieldName, PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_VISIBLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_TITLE.FieldName, PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_TITLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddBoolean(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_VISIBLETITLE.FieldName, PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_VISIBLETITLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_COLORTITLE.FieldName, PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_COLORTITLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_SIZETITLE.FieldName, PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_SIZETITLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_ALIGNTITLE.FieldName, PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_ALIGNTITLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

        /*
        //********   PBITEMPLATECELLGRAPHIC_GETID   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PBITEMPLATECELLGRAPHIC_GETID", @"PBITEMPLATECELLGRAPHIC_GETID description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   IDPBITEMPLATECELLGRAPHIC "); 
         UnSQL.SQL.Add(@"   FROM PBITEMPLATECELLGRAPHIC "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDPBITEMPLATECELLS=@[IDPBITEMPLATECELLS] And "); 
         UnSQL.SQL.Add(@"   IDPBIGRAPHICTYPE=@[IDPBIGRAPHICTYPE] And "); 
         UnSQL.SQL.Add(@"   CELLGRAPHIC_VISIBLE=@[CELLGRAPHIC_VISIBLE] And "); 
         UnSQL.SQL.Add(@"   CELLGRAPHIC_TITLE=@[CELLGRAPHIC_TITLE] And "); 
         UnSQL.SQL.Add(@"   CELLGRAPHIC_VISIBLETITLE=@[CELLGRAPHIC_VISIBLETITLE] And "); 
         UnSQL.SQL.Add(@"   CELLGRAPHIC_COLORTITLE=@[CELLGRAPHIC_COLORTITLE] And "); 
         UnSQL.SQL.Add(@"   CELLGRAPHIC_SIZETITLE=@[CELLGRAPHIC_SIZETITLE] And "); 
         UnSQL.SQL.Add(@"   CELLGRAPHIC_ALIGNTITLE=@[CELLGRAPHIC_ALIGNTITLE] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_PBITEMPLATECELLGRAPHIC = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "PBITEMPLATECELLGRAPHIC_GETID", Param.ToBytes());
        ResErr = DS_PBITEMPLATECELLGRAPHIC.ResErr;
        if (ResErr.NotError) {
            if (DS_PBITEMPLATECELLGRAPHIC.DataSet.RecordCount > 0) {
                DS_PBITEMPLATECELLGRAPHIC.DataSet.First();
                PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLGRAPHIC = DS_PBITEMPLATECELLGRAPHIC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLGRAPHIC.FieldName).asInt32();
            }
            else {
                DS_PBITEMPLATECELLGRAPHIC.ResErr.NotError = false;
                DS_PBITEMPLATECELLGRAPHIC.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_PBITEMPLATECELLGRAPHIC.js Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_GETID", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_ADD = function (PBITEMPLATECELLGRAPHIC) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLGRAPHIC.FieldName, PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLGRAPHIC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLS.FieldName, PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.IDPBIGRAPHICTYPE.FieldName, PBITEMPLATECELLGRAPHIC.IDPBIGRAPHICTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_VISIBLE.FieldName, PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_VISIBLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_TITLE.FieldName, PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_TITLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_VISIBLETITLE.FieldName, PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_VISIBLETITLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_COLORTITLE.FieldName, PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_COLORTITLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_SIZETITLE.FieldName, PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_SIZETITLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_ALIGNTITLE.FieldName, PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_ALIGNTITLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        /* 
        //********   PBITEMPLATECELLGRAPHIC_ADD   ************************* 
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PBITEMPLATECELLGRAPHIC_ADD", @"PBITEMPLATECELLGRAPHIC_ADD description."); 
        UnSQL.SQL.Add(@"   INSERT INTO PBITEMPLATECELLGRAPHIC( "); 
         UnSQL.SQL.Add(@"   IDPBITEMPLATECELLS, "); 
         UnSQL.SQL.Add(@"   IDPBIGRAPHICTYPE, "); 
         UnSQL.SQL.Add(@"   CELLGRAPHIC_VISIBLE, "); 
         UnSQL.SQL.Add(@"   CELLGRAPHIC_TITLE, "); 
         UnSQL.SQL.Add(@"   CELLGRAPHIC_VISIBLETITLE, "); 
         UnSQL.SQL.Add(@"   CELLGRAPHIC_COLORTITLE, "); 
         UnSQL.SQL.Add(@"   CELLGRAPHIC_SIZETITLE, "); 
         UnSQL.SQL.Add(@"   CELLGRAPHIC_ALIGNTITLE "); 
         UnSQL.SQL.Add(@"   ) Values ( ");  
        UnSQL.SQL.Add(@"   @[IDPBITEMPLATECELLS], "); 
         UnSQL.SQL.Add(@"   @[IDPBIGRAPHICTYPE], "); 
         UnSQL.SQL.Add(@"   @[CELLGRAPHIC_VISIBLE], "); 
         UnSQL.SQL.Add(@"   @[CELLGRAPHIC_TITLE], "); 
         UnSQL.SQL.Add(@"   @[CELLGRAPHIC_VISIBLETITLE], "); 
         UnSQL.SQL.Add(@"   @[CELLGRAPHIC_COLORTITLE], "); 
         UnSQL.SQL.Add(@"   @[CELLGRAPHIC_SIZETITLE], "); 
         UnSQL.SQL.Add(@"   @[CELLGRAPHIC_ALIGNTITLE] "); 
         UnSQL.SQL.Add(@"   )  "); 
        UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PBITEMPLATECELLGRAPHIC_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_GETID(PBITEMPLATECELLGRAPHIC);
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_PBITEMPLATECELLGRAPHIC.js Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_ADD", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_UPD = function (PBITEMPLATECELLGRAPHIC) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLGRAPHIC.FieldName, PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLGRAPHIC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLS.FieldName, PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.IDPBIGRAPHICTYPE.FieldName, PBITEMPLATECELLGRAPHIC.IDPBIGRAPHICTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_VISIBLE.FieldName, PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_VISIBLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_TITLE.FieldName, PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_TITLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_VISIBLETITLE.FieldName, PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_VISIBLETITLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_COLORTITLE.FieldName, PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_COLORTITLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_SIZETITLE.FieldName, PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_SIZETITLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_ALIGNTITLE.FieldName, PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_ALIGNTITLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);


        /*   
        //********   PBITEMPLATECELLGRAPHIC_UPD   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PBITEMPLATECELLGRAPHIC_UPD", @"PBITEMPLATECELLGRAPHIC_UPD description.");  
        UnSQL.SQL.Add(@"  UPDATE PBITEMPLATECELLGRAPHIC Set "); 
         UnSQL.SQL.Add(@"   IDPBITEMPLATECELLS=@[IDPBITEMPLATECELLS], "); 
         UnSQL.SQL.Add(@"   IDPBIGRAPHICTYPE=@[IDPBIGRAPHICTYPE], "); 
         UnSQL.SQL.Add(@"   CELLGRAPHIC_VISIBLE=@[CELLGRAPHIC_VISIBLE], "); 
         UnSQL.SQL.Add(@"   CELLGRAPHIC_TITLE=@[CELLGRAPHIC_TITLE], "); 
         UnSQL.SQL.Add(@"   CELLGRAPHIC_VISIBLETITLE=@[CELLGRAPHIC_VISIBLETITLE], "); 
         UnSQL.SQL.Add(@"   CELLGRAPHIC_COLORTITLE=@[CELLGRAPHIC_COLORTITLE], "); 
         UnSQL.SQL.Add(@"   CELLGRAPHIC_SIZETITLE=@[CELLGRAPHIC_SIZETITLE], "); 
         UnSQL.SQL.Add(@"   CELLGRAPHIC_ALIGNTITLE=@[CELLGRAPHIC_ALIGNTITLE] "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDPBITEMPLATECELLGRAPHIC=@[IDPBITEMPLATECELLGRAPHIC] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PBITEMPLATECELLGRAPHIC_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_PBITEMPLATECELLGRAPHIC.js Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_UPD", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_DEL = function (IDPBITEMPLATECELLGRAPHIC) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDPBITEMPLATECELLGRAPHIC == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLGRAPHIC.FieldName, " = " + UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLGRAPHIC.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDPBITEMPLATECELLGRAPHIC = " IN (" + StrIDPBITEMPLATECELLGRAPHIC + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLGRAPHIC.FieldName, StrIDPBITEMPLATECELLGRAPHIC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLGRAPHIC.FieldName, IDPBITEMPLATECELLGRAPHIC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   PBITEMPLATECELLGRAPHIC_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PBITEMPLATECELLGRAPHIC_DEL", @"PBITEMPLATECELLGRAPHIC_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE PBITEMPLATECELLGRAPHIC "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDPBITEMPLATECELLGRAPHIC=@[IDPBITEMPLATECELLGRAPHIC]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PBITEMPLATECELLGRAPHIC_DEL", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_PBITEMPLATECELLGRAPHIC.js Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_DEL", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
//**********************   METHODS   ********************************************************************************************
Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_ListSetID = function (PBITEMPLATECELLGRAPHICList, IDPBITEMPLATECELLGRAPHIC) {
    for (i = 0; i < PBITEMPLATECELLGRAPHICList.length; i++) {
        if (IDPBITEMPLATECELLGRAPHIC == PBITEMPLATECELLGRAPHICList[i].IDPBITEMPLATECELLGRAPHIC)
            return (PBITEMPLATECELLGRAPHICList[i]);
    }
    var UnPBITEMPLATECELLGRAPHIC = new Persistence.PBITemplate.Properties.TPBITEMPLATECELLGRAPHIC;
    UnPBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLGRAPHIC = IDPBITEMPLATECELLGRAPHIC;
    return (UnPBITEMPLATECELLGRAPHIC);
}
Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_ListAdd = function (PBITEMPLATECELLGRAPHICList, PBITEMPLATECELLGRAPHIC) {
    var i = Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_ListGetIndex(PBITEMPLATECELLGRAPHICList, PBITEMPLATECELLGRAPHIC);
    if (i == -1) PBITEMPLATECELLGRAPHICList.push(PBITEMPLATECELLGRAPHIC);
    else {
        PBITEMPLATECELLGRAPHICList[i].IDPBITEMPLATECELLGRAPHIC = PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLGRAPHIC;
        PBITEMPLATECELLGRAPHICList[i].IDPBITEMPLATECELLS = PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLS;
        PBITEMPLATECELLGRAPHICList[i].IDPBIGRAPHICTYPE = PBITEMPLATECELLGRAPHIC.IDPBIGRAPHICTYPE;
        PBITEMPLATECELLGRAPHICList[i].CELLGRAPHIC_VISIBLE = PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_VISIBLE;
        PBITEMPLATECELLGRAPHICList[i].CELLGRAPHIC_TITLE = PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_TITLE;
        PBITEMPLATECELLGRAPHICList[i].CELLGRAPHIC_VISIBLETITLE = PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_VISIBLETITLE;
        PBITEMPLATECELLGRAPHICList[i].CELLGRAPHIC_COLORTITLE = PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_COLORTITLE;
        PBITEMPLATECELLGRAPHICList[i].CELLGRAPHIC_SIZETITLE = PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_SIZETITLE;
        PBITEMPLATECELLGRAPHICList[i].CELLGRAPHIC_ALIGNTITLE = PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_ALIGNTITLE;

    }
}
//CJRC_26072018
Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_ListGetIndex = function (PBITEMPLATECELLGRAPHICList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_ListGetIndexIDPBITEMPLATECELLGRAPHIC(PBITEMPLATECELLGRAPHICList, Param);

    }
    else {
        Res = Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_ListGetIndexPBITEMPLATECELLGRAPHIC(PBITEMPLATECELLGRAPHICList, Param);
    }
    return (Res);
}

Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_ListGetIndexPBITEMPLATECELLGRAPHIC = function (PBITEMPLATECELLGRAPHICList, PBITEMPLATECELLGRAPHIC) {
    for (i = 0; i < PBITEMPLATECELLGRAPHICList.length; i++) {
        if (PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLGRAPHIC == PBITEMPLATECELLGRAPHICList[i].IDPBITEMPLATECELLGRAPHIC)
            return (i);
    }
    return (-1);
}
Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_ListGetIndexIDPBITEMPLATECELLGRAPHIC = function (PBITEMPLATECELLGRAPHICList, IDPBITEMPLATECELLGRAPHIC) {
    for (i = 0; i < PBITEMPLATECELLGRAPHICList.length; i++) {
        if (IDPBITEMPLATECELLGRAPHIC == PBITEMPLATECELLGRAPHICList[i].IDPBITEMPLATECELLGRAPHIC)
            return (i);
    }
    return (-1);
}
//STRING FUNCTION 
Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHICListtoStr = function (PBITEMPLATECELLGRAPHICList) {
    var Res = Persistence.Properties._Version + "(1)0";
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(PBITEMPLATECELLGRAPHICList.length, Longitud, Texto);
        for (Counter = 0; Counter < PBITEMPLATECELLGRAPHICList.length; Counter++) {
            var UnPBITEMPLATECELLGRAPHIC = PBITEMPLATECELLGRAPHICList[Counter];
            UnPBITEMPLATECELLGRAPHIC.ToStr(Longitud, Texto);
        }

        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
        SysCfg.Log.Methods.WriteLog("DB_PBITEMPLATECELLGRAPHIC.js Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHICListtoStr ", e);
    }
    return Res;
}
Persistence.PBITemplate.Methods.StrtoPBITEMPLATECELLGRAPHIC = function (ProtocoloStr, PBITEMPLATECELLGRAPHICList) {

    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        PBITEMPLATECELLGRAPHICList.length = 0;
        if (Persistence.PBITemplate.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")") - ProtocoloStr.indexOf("(") - 1);
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length - ProtocoloStr.indexOf(")") - 1);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnPBITEMPLATECELLGRAPHIC = new Persistence.PBITemplate.Properties.TPBITEMPLATECELLGRAPHIC(); //Mode new row
                UnPBITEMPLATECELLGRAPHIC.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.PBITemplate.PBITEMPLATECELLGRAPHIC_ListAdd(PBITEMPLATECELLGRAPHICList, UnPBITEMPLATECELLGRAPHIC);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
        SysCfg.Log.Methods.WriteLog("DB_PBITEMPLATECELLGRAPHIC.js Persistence.PBITemplate.Methods.StrtoPBITEMPLATECELLGRAPHIC ", e);
    }
    return (Res);
}
//SOCKET FUNCTION 
Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHICListToByte = function (PBITEMPLATECELLGRAPHICList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, PBITEMPLATECELLGRAPHICList.Count - idx);
    for (i = idx; i < PBITEMPLATECELLGRAPHICList.length; i++) {
        PBITEMPLATECELLGRAPHICList[i].ToByte(MemStream);
    }
}
Persistence.PBITemplate.Methods.ByteToPBITEMPLATECELLGRAPHICList = function (PBITEMPLATECELLGRAPHICList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnPBITEMPLATECELLGRAPHIC = new Persistence.PBITemplate.Properties.TPBITEMPLATECELLGRAPHIC();
        UnPBITEMPLATECELLGRAPHIC.ByteTo(MemStream);
        Methods.PBITEMPLATECELLGRAPHIC_ListAdd(PBITEMPLATECELLGRAPHICList, UnPBITEMPLATECELLGRAPHIC);
    }
}
