﻿//**********************   PROPERTIES   *****************************************************************************************
Persistence.Demo.Properties.TDMTABLA = function() {
    this.FDBINARIO = false;
    this.FDCADENA = "";
    this.FDDATETIME = new Date(1970, 0, 1, 0, 0, 0);
    this.FDENTRO = 0;
    this.FDFLOTANTE = 0;
    this.FDTEXTO = "";
    this.IDDMTABLA = 0;
    //Socket IO Properties
    this.ToByte = function(MemStream){
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.FDBINARIO); 
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.FDCADENA); 
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.FDDATETIME); 
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.FDENTRO); 
        SysCfg.Stream.Methods.WriteStreamDouble(MemStream, this.FDFLOTANTE); 
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.FDTEXTO); 
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDDMTABLA); 
    }
    this.ByteTo = function(MemStream)
    {
        this.FDBINARIO = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream); 
        this.FDCADENA = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream); 
        this.FDDATETIME = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream); 
        this.FDENTRO = SysCfg.Stream.Methods.ReadStreamInt32(MemStream); 
        this.FDFLOTANTE = SysCfg.Stream.Methods.ReadStreamDouble(MemStream); 
        this.FDTEXTO = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream); 
        this.IDDMTABLA = SysCfg.Stream.Methods.ReadStreamInt32(MemStream); 
    }
    //Str IO Properties
    this.ToStr = function(Longitud,Texto)
    {
        SysCfg.Str.Protocol.WriteBool(this.FDBINARIO, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.FDCADENA,Longitud,Texto);
        SysCfg.Str.Protocol.WriteDateTime(this.FDDATETIME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.FDENTRO,Longitud,Texto);
        SysCfg.Str.Protocol.WriteDouble(this.FDFLOTANTE,Longitud,Texto);
        SysCfg.Str.Protocol.WriteStr(this.FDTEXTO,Longitud,Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDDMTABLA,Longitud,Texto);
    }
    this.StrTo = function(Pos,Index,LongitudArray,Texto)
    {
        this.FDBINARIO = SysCfg.Str.Protocol.ReadBool(Pos,Index,LongitudArray, Texto);
        this.FDCADENA = SysCfg.Str.Protocol.ReadStr(Pos,Index, LongitudArray, Texto);
        this.FDDATETIME = SysCfg.Str.Protocol.ReadDateTime(Pos,Index, LongitudArray, Texto);
        this.FDENTRO = SysCfg.Str.Protocol.ReadInt(Pos,Index, LongitudArray, Texto);
        this.FDFLOTANTE = SysCfg.Str.Protocol.ReadDouble(Pos,Index, LongitudArray, Texto);
        this.FDTEXTO = SysCfg.Str.Protocol.ReadStr(Pos,Index, LongitudArray, Texto);
        this.IDDMTABLA = SysCfg.Str.Protocol.ReadInt(Pos,Index, LongitudArray, Texto);
    }
}


//**********************   METHODS server  ********************************************************************************************
Persistence.Demo.Methods.DMTABLA_Fill = function(DMTABLAList,StrIDDMTABLA/*noin IDDMTABLA*/)
{
    var ResErr = new SysCfg.Error.Properties.TResErr();
    DMTABLAList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDDMTABLA == "")
    {
        Param.AddUnknown(UsrCfg.InternoDemoNames.DMTABLA.IDDMTABLA.FieldName, " = " + UsrCfg.InternoDemoNames.DMTABLA.NAME_TABLE + "." + UsrCfg.InternoDemoNames.DMTABLA.IDDMTABLA.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else
    {
        StrIDDMTABLA = " IN (" + StrIDDMTABLA + ")";
        Param.AddUnknown(UsrCfg.InternoDemoNames.DMTABLA.IDDMTABLA.FieldName, StrIDDMTABLA, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //noin Param.AddInt32(UsrCfg.InternoDataLinkNames.DMTABLA.IDDMTABLA.FieldName, IDDMTABLA, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try
    {
    /*
    //********   DMTABLA_GET   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"DMTABLA_GET", @"DMTABLA_GET description.");
    UnSQL.SQL.Add(@" SELECT  ");
    UnSQL.SQL.Add(@"  DMTABLA.FDBINARIO, ");
    UnSQL.SQL.Add(@"  DMTABLA.FDCADENA, ");
    UnSQL.SQL.Add(@"  DMTABLA.FDDATETIME, ");
    UnSQL.SQL.Add(@"  DMTABLA.FDENTRO, ");
    UnSQL.SQL.Add(@"  DMTABLA.FDFLOTANTE, ");
    UnSQL.SQL.Add(@"  DMTABLA.FDTEXTO, ");
    UnSQL.SQL.Add(@"  DMTABLA.IDDMTABLA ");
    UnSQL.SQL.Add(@" FROM  DMTABLA ");
    UnSQL.SQL.Add(@" WHERE   ");
    UnSQL.SQL.Add(@"  DMTABLA.IDDMTABLA @[IDDMTABLA] ");//noin DMTABLA.IDDMTABLA = @[IDDMTABLA]
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************
    */
        var DS_DMTABLA = SysCfg.DB.SQL.Methods.OpenDataSet("Demo", "DMTABLA_GET", Param.ToBytes());
        ResErr = DS_DMTABLA.ResErr;
        if (ResErr.NotError)
        {
            if (DS_DMTABLA.DataSet.RecordCount > 0)
            {
                DS_DMTABLA.DataSet.First();
                while (!(DS_DMTABLA.DataSet.Eof))
                {
                    var DMTABLA = new Persistence.Demo.Properties.TDMTABLA();
                    //var DMTABLA = DMTABLA_ListSetID(DMTABLAList, DS_DMTABLA.DataSet.RecordSet.FieldName(UsrCfg.InternoDemoNames.DMTABLA.IDDMTABLA.FieldName).asInt32());
                    DMTABLA.IDDMTABLA = DS_DMTABLA.DataSet.RecordSet.FieldName(UsrCfg.InternoDemoNames.DMTABLA.IDDMTABLA.FieldName).asInt32();
                    //Other
                    DMTABLA.FDBINARIO = DS_DMTABLA.DataSet.RecordSet.FieldName(UsrCfg.InternoDemoNames.DMTABLA.FDBINARIO.FieldName).asBoolean(); 
                    DMTABLA.FDCADENA = DS_DMTABLA.DataSet.RecordSet.FieldName(UsrCfg.InternoDemoNames.DMTABLA.FDCADENA.FieldName).asString(); 
                    DMTABLA.FDDATETIME = DS_DMTABLA.DataSet.RecordSet.FieldName(UsrCfg.InternoDemoNames.DMTABLA.FDDATETIME.FieldName).asDateTime(); 
                    DMTABLA.FDENTRO = DS_DMTABLA.DataSet.RecordSet.FieldName(UsrCfg.InternoDemoNames.DMTABLA.FDENTRO.FieldName).asInt32(); 
                    DMTABLA.FDFLOTANTE = DS_DMTABLA.DataSet.RecordSet.FieldName(UsrCfg.InternoDemoNames.DMTABLA.FDFLOTANTE.FieldName).asDouble();
                    DMTABLA.FDTEXTO = DS_DMTABLA.DataSet.RecordSet.FieldName(UsrCfg.InternoDemoNames.DMTABLA.FDTEXTO.FieldName).asText();
                    DMTABLAList.push(DMTABLA);
                    //Persistence.Demo.Methods.DMTABLA_ListAdd(DMTABLAList, DMTABLA)
                    DS_DMTABLA.DataSet.Next();
                }
            }
            else
            {
                DS_DMTABLA.ResErr.NotError = false;
                DS_DMTABLA.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Demo.Methods.DMTABLA_GETID = function(DMTABLA)
{
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        //Other
        Param.AddBoolean(UsrCfg.InternoDemoNames.DMTABLA.FDBINARIO.FieldName, DMTABLA.FDBINARIO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoDemoNames.DMTABLA.FDCADENA.FieldName, DMTABLA.FDCADENA, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal); 
        Param.AddDateTime(UsrCfg.InternoDemoNames.DMTABLA.FDDATETIME.FieldName, DMTABLA.FDDATETIME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal); 
        Param.AddInt32(UsrCfg.InternoDemoNames.DMTABLA.FDENTRO.FieldName, DMTABLA.FDENTRO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal); 
        Param.AddDouble(UsrCfg.InternoDemoNames.DMTABLA.FDFLOTANTE.FieldName, DMTABLA.FDFLOTANTE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal); 
        //NoWhere Param.AddText(UsrCfg.InternoDemoNames.DMTABLA.FDTEXTO.FieldName, DMTABLA.FDTEXTO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal); 
        /*
    //********   DMTABLA_GETID   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"DMTABLA_GETID", @"DMTABLA_GETID description.");
    UnSQL.SQL.Add(@" SELECT  ");
    UnSQL.SQL.Add(@"  DMTABLA.IDDMTABLA ");
    UnSQL.SQL.Add(@" FROM  DMTABLA ");
    UnSQL.SQL.Add(@" WHERE   ");
    UnSQL.SQL.Add(@"  DMTABLA.FDBINARIO=@[FDBINARIO] and  ");
    UnSQL.SQL.Add(@"  DMTABLA.FDCADENA=@[FDCADENA] and  ");
    UnSQL.SQL.Add(@"  DMTABLA.FDDATETIME=@[FDDATETIME] and  ");
    UnSQL.SQL.Add(@"  DMTABLA.FDENTRO=@[FDENTRO] and  ");
    UnSQL.SQL.Add(@"  DMTABLA.FDFLOTANTE=@[FDFLOTANTE]
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************
       */
        var DS_DMTABLA = SysCfg.DB.SQL.Methods.OpenDataSet("Demo", "DMTABLA_GETID", Param.ToBytes());
        ResErr = DS_DMTABLA.ResErr;
        if (ResErr.NotError)
        {
            if (DS_DMTABLA.DataSet.RecordCount > 0)
            {
                DS_DMTABLA.DataSet.First();
                DMTABLA.IDDMTABLA = DS_DMTABLA.DataSet.RecordSet.FieldName(UsrCfg.InternoDemoNames.DMTABLA.IDDMTABLA.FieldName).asInt32(); 
            }
            else
            {
                DS_DMTABLA.ResErr.NotError = false;
                DS_DMTABLA.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Demo.Methods.DMTABLA_ADD = function(DMTABLA)
{
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        //Other
        Param.AddBoolean(UsrCfg.InternoDemoNames.DMTABLA.FDBINARIO.FieldName, DMTABLA.FDBINARIO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal); 
        Param.AddString(UsrCfg.InternoDemoNames.DMTABLA.FDCADENA.FieldName, DMTABLA.FDCADENA, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal); 
        Param.AddDateTime(UsrCfg.InternoDemoNames.DMTABLA.FDDATETIME.FieldName, DMTABLA.FDDATETIME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal); 
        Param.AddInt32(UsrCfg.InternoDemoNames.DMTABLA.FDENTRO.FieldName, DMTABLA.FDENTRO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal); 
        Param.AddDouble(UsrCfg.InternoDemoNames.DMTABLA.FDFLOTANTE.FieldName, DMTABLA.FDFLOTANTE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal); 
        Param.AddText(UsrCfg.InternoDemoNames.DMTABLA.FDTEXTO.FieldName, DMTABLA.FDTEXTO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal); 
        /*    
    //********   DMTABLA_ADD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"DMTABLA_ADD", @"DMTABLA_ADD description.");
    UnSQL.SQL.Add(@" INSERT INTO DMTABLA( ");
    UnSQL.SQL.Add(@"  FDBINARIO,  ");
    UnSQL.SQL.Add(@"  FDCADENA,  ");
    UnSQL.SQL.Add(@"  FDDATETIME,  ");
    UnSQL.SQL.Add(@"  FDENTRO,  ");
    UnSQL.SQL.Add(@"  FDFLOTANTE,  ");
    UnSQL.SQL.Add(@"  FDTEXTO  ");
    UnSQL.SQL.Add(@" )  ");
    UnSQL.SQL.Add(@" VALUES ( ");
    UnSQL.SQL.Add(@"  @[FDBINARIO], ");
    UnSQL.SQL.Add(@"  @[FDCADENA], ");
    UnSQL.SQL.Add(@"  @[FDDATETIME], ");
    UnSQL.SQL.Add(@"  @[FDENTRO], ");
    UnSQL.SQL.Add(@"  @[FDFLOTANTE], ");
    UnSQL.SQL.Add(@"  @[FDTEXTO] ");
    UnSQL.SQL.Add(@" ) ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************
        */
        this.Exec = SysCfg.DB.SQL.Methods.Exec("Demo", "DMTABLA_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            Persistence.Demo.Methods.DMTABLA_GETID(DMTABLA);
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Demo.Methods.DMTABLA_UPD = function(DMTABLA)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoDemoNames.DMTABLA.IDDMTABLA.FieldName, DMTABLA.IDDMTABLA, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoDemoNames.DMTABLA.FDBINARIO.FieldName, DMTABLA.FDBINARIO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoDemoNames.DMTABLA.FDCADENA.FieldName, DMTABLA.FDCADENA, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoDemoNames.DMTABLA.FDDATETIME.FieldName, DMTABLA.FDDATETIME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoDemoNames.DMTABLA.FDENTRO.FieldName, DMTABLA.FDENTRO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDouble(UsrCfg.InternoDemoNames.DMTABLA.FDFLOTANTE.FieldName, DMTABLA.FDFLOTANTE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoDemoNames.DMTABLA.FDTEXTO.FieldName, DMTABLA.FDTEXTO, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        /*   
    //********   DMTABLA_UPD   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"DMTABLA_UPD", @"DMTABLA_UPD description.");
    UnSQL.SQL.Add(@" UPDATE DMTABLA SET ");
    UnSQL.SQL.Add(@"  FDBINARIO=@[FDBINARIO],  ");
    UnSQL.SQL.Add(@"  FDCADENA=@[FDCADENA],  ");
    UnSQL.SQL.Add(@"  FDDATETIME=@[FDDATETIME],  ");
    UnSQL.SQL.Add(@"  FDENTRO=@[FDENTRO],  ");
    UnSQL.SQL.Add(@"  FDFLOTANTE=@[FDFLOTANTE],  ");
    UnSQL.SQL.Add(@"  FDTEXTO=@[FDTEXTO] ");
    UnSQL.SQL.Add(@" WHERE   ");
    UnSQL.SQL.Add(@"  IDDMTABLA=@[IDDMTABLA] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Demo", "DMTABLA_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            //GET(); 
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.Demo.Methods.DMTABLA_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.Demo.Methods.DMTABLA_DELIDDMTABLA(/*String StrIDDMTABLA*/Param);
    }
    else {
        Res = Persistence.Demo.Methods.DMTABLA_DELDMTABLA(Param/*DMTABLAList*/);
    }
    return (Res);
}

Persistence.Demo.Methods.DMTABLA_DELIDDMTABLA = function (/*String StrIDDMTABLA*/IDDMTABLA)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        /*if (StrIDDMTABLA == "")
        {
            Param.AddUnknown(UsrCfg.InternoDemoNames.DMTABLA.IDDMTABLA.FieldName, " = " + UsrCfg.InternoDemoNames.DMTABLA.NAME_TABLE + "." + UsrCfg.InternoDemoNames.DMTABLA.IDDMTABLA.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDDMTABLA = " IN (" + StrIDDMTABLA + ")";
            Param.AddUnknown(UsrCfg.InternoDemoNames.DMTABLA.IDDMTABLA.FieldName, StrIDDMTABLA, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoDemoNames.DMTABLA.IDDMTABLA.FieldName, IDDMTABLA, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
    
    //********   DMTABLA_DEL_1   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"DMTABLA_DEL_1", @"DMTABLA_DEL_1 description.");
    UnSQL.SQL.Add(@" DELETE DMTABLA  ");
    UnSQL.SQL.Add(@" WHERE   ");
    UnSQL.SQL.Add(@"  IDDMTABLA=@[IDDMTABLA] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Demo", "DMTABLA_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Demo.Methods.DMTABLA_DELDMTABLA = function (DMTABLA/*DMTABLAList*/)
{
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        /*
        StrIDDMTABLA = "";
        StrIDOTRO = "";
        for (var i = 0; i < DMTABLAList.length; i++) {
            StrIDDMTABLA += DMTABLAList[i].IDDMTABLA + ",";
            StrIDOTRO += DMTABLAList[i].IDOTRO + ",";;
        }
        StrIDDMTABLA = StrIDDMTABLA.substring(0, StrIDDMTABLA.length - 1);
        StrIDOTRO = StrIDOTRO.substring(0, StrIDOTRO.length - 1);


        if (StrIDDMTABLA == "")
        {
            Param.AddUnknown(UsrCfg.InternoDemoNames.DMTABLA.IDDMTABLA.FieldName, " = " + UsrCfg.InternoDemoNames.DMTABLA.NAME_TABLE + "." + UsrCfg.InternoDemoNames.DMTABLA.IDDMTABLA.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDDMTABLA = " IN (" + StrIDDMTABLA + ")";
            Param.AddUnknown(UsrCfg.InternoDemoNames.DMTABLA.IDDMTABLA.FieldName, StrIDDMTABLA, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        

        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoDemoNames.DMTABLA.IDDMTABLA.FieldName, DMTABLA.IDDMTABLA, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal); 
        /*    
    //********   DMTABLA_DEL_2   *************************
    UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"DMTABLA_DEL_2", @"DMTABLA_DEL_2 description.");
    UnSQL.SQL.Add(@" DELETE DMTABLA  ");
    UnSQL.SQL.Add(@" WHERE   ");
    UnSQL.SQL.Add(@"  IDDMTABLA=@[IDDMTABLA] ");
    UnConfigSQL.SQL.Add(UnSQL);
    //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Demo", "DMTABLA_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}

//**********************   METHODS   ********************************************************************************************
Persistence.Demo.Methods.DMTABLA_ListSetID = function(DMTABLAList,IDDMTABLA)
{
    for (i = 0; i < DMTABLAList.length; i++)
    {
        if (IDDMTABLA == DMTABLAList[i].IDDMTABLA)
            return (DMTABLAList[i]);
    }
    var UnDMTABLA = new Persistence.Properties.TDMTABLA;
    UnDMTABLA.IDDMTABLA = IDDMTABLA; 
    return (UnDMTABLA);
}
Persistence.Demo.Methods.DMTABLA_ListAdd = function(DMTABLAList,DMTABLA)
{
    var i = Persistence.Demo.Methods.DMTABLA_ListGetIndex(DMTABLAList, DMTABLA);
    if (i == -1) DMTABLAList.push(DMTABLA);
    else
    {
        DMTABLAList[i].FDBINARIO = DMTABLA.FDBINARIO;
        DMTABLAList[i].FDCADENA = DMTABLA.FDCADENA;
        DMTABLAList[i].FDDATETIME = DMTABLA.FDDATETIME;
        DMTABLAList[i].FDENTRO = DMTABLA.FDENTRO;
        DMTABLAList[i].FDFLOTANTE = DMTABLA.FDFLOTANTE;
        DMTABLAList[i].FDTEXTO = DMTABLA.FDTEXTO;
        DMTABLAList[i].IDDMTABLA = DMTABLA.IDDMTABLA;
    }
}
//CJRC_26072018
Persistence.Demo.Methods.DMTABLA_ListGetIndex = function (DMTABLAList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.Demo.Methods.DMTABLA_ListGetIndexIDDMTABLA(DMTABLAList, Param);

    }
    else {
        Res = Persistence.Demo.Methods.DMTABLA_ListGetIndexDMTABLA(DMTABLAList, Param);
    }
    return (Res);
}

Persistence.Demo.Methods.DMTABLA_ListGetIndexDMTABLA = function (DMTABLAList, DMTABLA)
{
    for (i = 0; i < DMTABLAList.length; i++)
    {
        if (DMTABLA.IDDMTABLA == DMTABLAList[i].IDDMTABLA)
            return (i);
    }
    return (-1);
}
Persistence.Demo.Methods.DMTABLA_ListGetIndexIDDMTABLA = function (DMTABLAList, IDDMTABLA)
{
    for (i = 0; i < DMTABLAList.length; i++)
    {
        if (IDDMTABLA == DMTABLAList[i].IDDMTABLA)
            return (i);
    }
    return (-1);
}

//String Function 

Persistence.Demo.Methods.DMTABLAListtoStr = function (DMTABLAList) {
    var Res = Persistence.Demo.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(DMTABLAList.length, Longitud, Texto);
        for (Counter = 0; Counter < DMTABLAList.length; Counter++) {
            var UnDMTABLA = DMTABLAList[Counter];
            UnDMTABLA.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Demo.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
}
Persistence.Demo.Methods.StrtoDMTABLA = function (ProtocoloStr, DMTABLAList) {

    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        DMTABLAList.length = 0;
        if (Persistence.Demo.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnDMTABLA = new Persistence.Demo.Properties.TDMTABLA(); //Mode new row
                UnDMTABLA.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.DMTABLA_ListAdd(DMTABLAList, UnDMTABLA);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}


//Socket Function 
Persistence.Demo.Methods.DMTABLAListToByte = function(DMTABLAList, MemStream, idx)
{
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, DMTABLAList.Count-idx);
    for (i = idx; i < DMTABLAList.length; i++)
    {
        DMTABLAList[i].ToByte(MemStream);
    }
}
Persistence.Demo.Methods.ByteToDMTABLAList = function (DMTABLAList, MemStream)
{
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++)
    {
        UnDMTABLA = new Persistence.Properties.TDMTABLA();
        UnDMTABLA.ByteTo(MemStream);
        Methods.DMTABLA_ListAdd(DMTABLAList,UnDMTABLA);
    }
}
//*******************************************************************************************************************************