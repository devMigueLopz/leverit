//TreduceDB
//Persistence.Notify

Persistence.Notify.Properties._Version = "001";

Persistence.Notify.Properties.TSDNOTIFY = function () {
    this.CHANGE_DESCRIPTION = "";
    this.CHANGE_EMAILSQL = "";
    this.EVENTDATE = new Date(1970, 0, 1, 0, 0, 0);
    this.IDCMDBCI = 0;
    this.IDSDNOTIFY = 0;
    this.IDSDNOTIFYTEMPLATE = 0;
    this.IDSDCASE = 0;
    this.IDSDTYPEUSER = 0;
    this.ISSENDCONSOLE = false;
    this.ISSENDEMAIL = false;
    this.TYPEUSERNAME = "";
    this.IDSDCASEMT = 0;
    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CHANGE_DESCRIPTION);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CHANGE_EMAILSQL);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.EVENTDATE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDCMDBCI);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDNOTIFY);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDNOTIFYTEMPLATE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDTYPEUSER);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.ISSENDCONSOLE);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.ISSENDEMAIL);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.TYPEUSERNAME);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASEMT);
    }
    this.ByteTo = function (MemStream) {
        this.CHANGE_DESCRIPTION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CHANGE_EMAILSQL = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.EVENTDATE = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.IDCMDBCI = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDNOTIFY = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDNOTIFYTEMPLATE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDCASE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDTYPEUSER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.ISSENDCONSOLE = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.ISSENDEMAIL = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.TYPEUSERNAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDSDCASEMT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteStr(this.CHANGE_DESCRIPTION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.CHANGE_EMAILSQL, Longitud, Texto);
        SysCfg.Str.Protocol.WriteDateTime(this.EVENTDATE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDCMDBCI, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDSDNOTIFY, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDSDNOTIFYTEMPLATE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDSDCASE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDSDTYPEUSER, Longitud, Texto);
        SysCfg.Str.Protocol.WriteBool(this.ISSENDCONSOLE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteBool(this.ISSENDEMAIL, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.TYPEUSERNAME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDSDCASEMT, Longitud, Texto);
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.CHANGE_DESCRIPTION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.CHANGE_EMAILSQL = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.EVENTDATE = SysCfg.Str.Protocol.ReadDateTime(Pos, Index, LongitudArray, Texto);
        this.IDCMDBCI = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDSDNOTIFY = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDSDNOTIFYTEMPLATE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDSDCASE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDSDTYPEUSER = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.ISSENDCONSOLE = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);
        this.ISSENDEMAIL = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);
        this.TYPEUSERNAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.IDSDCASEMT = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
    }
}


// SDNOTIFYTEMPLATE

Persistence.Notify.Properties.TSDNOTIFYSEND = function () {
    this.SDNOTIFYMailList = new Array();//TSDNOTIFYMail
    this.EMAILSQL = "";
}

Persistence.Notify.Properties.TSDNOTIFYMail = function () {
    this.StrMailList = new Array();//String

}

Persistence.Notify.Properties.TSDNOTIFYTEMPLATE = function () //_XYZ
{
    this.DESCRIPTION = "";
    this.IDSDNOTIFYTEMPLATE = 0;
    this.STATUS = false;
    this.EMAILSQL = "";
    this.EMAILTEMPLATE = "";
    this.ISEMAILSQL = false;
    this.EMAILTEMPLATEStr = "";
    this.EMAILTEMPLATEList = new Array();//String
    this.TSDNOTIFYTEMPLATE_TABLEList = new Array(); //TSDNOTIFYTEMPLATE_TABLE
    this.TSDNOTIFYTEMPLATE_FIELDList = new Array(); //TSDNOTIFYTEMPLATE_FIELD
    //this.SDNOTIFYTEMPLATE_TYPEUSERList = new Array();//Notify.Properties.TSDNOTIFYTEMPLATE_TYPEUSER
}

Persistence.Notify.Properties.TSDNOTIFYTEMPLATE_TABLE = function () {
    this.CODE = "";
    this.FieldsName = new Array(); //String
}

Persistence.Notify.Properties.TSDNOTIFYTEMPLATE_FIELD = function () {
    this.CODE = "";
    this.FieldName = "";
}





// SDNOTIFYTEMPLATE_TYPEUSER

Persistence.Notify.Properties.TSDNOTIFYTEMPLATE_TYPEUSER = function () {
    this.IDSDNOTIFYTEMPLATE = 0;
    this.IDSDNOTIFYTEMPLATE_TYPEUSER = 0;
    this.IDSDTYPEUSER = UsrCfg.SD.Properties.TSDTypeUser.GetEnum(undefined);
    this.IDMDSERVICETYPE = 0;
    this.IDSDNOTIFYTYPEEVENT = 0;
    this.TYPEEVENTNAME = "";
    this.SENDCONSOLE = false;
    this.SENDEMAIL = false;
}



// MDLIFESTATUSTYPEUSER

Persistence.Notify.Properties.TMDLIFESTATUSTYPEUSER = function () {
    this.IDMDMODELTYPED = 0;
    this.IDMDLIFESTATUS = 0;
    this.IDSDTYPEUSER = 0;
    this.STATUSN = 0;
    this.NAMESTEP = "";
    this.IDMDLIFESTATUSPERMISSION = 0;
    this.IDSDNOTIFYTEMPLATE_CONSOLE = 0;
    this.IDSDNOTIFYTEMPLATE_MAIL = 0;
}

