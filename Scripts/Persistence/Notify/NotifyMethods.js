//TreduceDB
//Persistence.Notify.Methods



//List Function 
Persistence.Notify.Methods.SDNOTIFY_ListSetID = function (SDNOTIFYList, IDSDNOTIFY) {
    for (var i = 0; i < SDNOTIFYList.length; i++) {

        if (IDSDNOTIFY == SDNOTIFYList[i].IDSDNOTIFY)
            return (SDNOTIFYList[i]);

    }
    var SDNOTIFY = new Persistence.Notify.Properties.TSDNOTIFY();
    SDNOTIFY.IDSDNOTIFY = IDSDNOTIFY
    return (SDNOTIFY);
}
Persistence.Notify.Methods.SDNOTIFY_ListAdd = function (SDNOTIFYList, SDNOTIFY) {

    var i = Persistence.Notify.Methods.SDNOTIFY_ListGetIndex(SDNOTIFYList, SDNOTIFY);
    if (i == -1) SDNOTIFYList.push(SDNOTIFY);
    else {
        SDNOTIFYList[i].CHANGE_DESCRIPTION = SDNOTIFY.CHANGE_DESCRIPTION;
        SDNOTIFYList[i].CHANGE_EMAILSQL = SDNOTIFY.CHANGE_EMAILSQL;
        SDNOTIFYList[i].EVENTDATE = SDNOTIFY.EVENTDATE;
        SDNOTIFYList[i].IDCMDBCI = SDNOTIFY.IDCMDBCI;
        SDNOTIFYList[i].IDSDNOTIFY = SDNOTIFY.IDSDNOTIFY;
        SDNOTIFYList[i].IDSDNOTIFYTEMPLATE = SDNOTIFY.IDSDNOTIFYTEMPLATE;
        SDNOTIFYList[i].IDSDCASE = SDNOTIFY.IDSDCASE;
        SDNOTIFYList[i].IDSDTYPEUSER = SDNOTIFY.IDSDTYPEUSER;
        SDNOTIFYList[i].ISSENDCONSOLE = SDNOTIFY.ISSENDCONSOLE;
        SDNOTIFYList[i].ISSENDEMAIL = SDNOTIFY.ISSENDEMAIL;
        SDNOTIFYList[i].TYPEUSERNAME = SDNOTIFY.TYPEUSERNAME;
        SDNOTIFYList[i].IDSDCASEMT = SDNOTIFY.IDSDCASEMT;
    }
}
//CJRC_26072018
Persistence.Notify.Methods.SDNOTIFY_ListGetIndex = function (SDNOTIFYList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.Notify.Methods.SDNOTIFY_ListGetIndexIDSDNOTIFY(SDNOTIFYList, Param);

    }
    else {
        Res = Persistence.Notify.Methods.SDNOTIFY_ListGetIndexSDNOTIFY(SDNOTIFYList, Param);
    }
    return (Res);
}

Persistence.Notify.Methods.SDNOTIFY_ListGetIndexSDNOTIFY = function (SDNOTIFYList, SDNOTIFY) {
    for (var i = 0; i < SDNOTIFYList.length; i++) {
        if (SDNOTIFY.IDSDNOTIFY == SDNOTIFYList[i].IDSDNOTIFY)
            return (i);
    }
    return (-1);
}
Persistence.Notify.Methods.SDNOTIFY_ListGetIndexIDSDNOTIFY = function (SDNOTIFYList, IDSDNOTIFY) {
    for (var i = 0; i < SDNOTIFYList.length; i++) {
        if (IDSDNOTIFY == SDNOTIFYList[i].IDSDNOTIFY)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.Notify.Methods.SDNOTIFYListtoStr = function (SDNOTIFYList) {
    var Res = Persistence.Notify.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(SDNOTIFYList.length, Longitud, Texto);
        for (Counter = 0; Counter < SDNOTIFYList.length; Counter++) {
            var UnSDNOTIFY = SDNOTIFYList[Counter];
            UnSDNOTIFY.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Notify.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;


}
Persistence.Notify.Methods.StrtoSDNOTIFY = function (ProtocoloStr, SDNOTIFYList) {
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        SDNOTIFYList.length = 0;
        if (Persistence.Notify.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnSDNOTIFY = new Persistence.Demo.Properties.TSDNOTIFY(); //Mode new row
                UnSDNOTIFY.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Notify.Methods.SDNOTIFY_ListAdd(SDNOTIFYList, UnSDNOTIFY);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}

//Socket Function 
Persistence.Notify.Methods.SDNOTIFYListToByte = function(SDNOTIFYList, MemStream, idx)
{
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, SDNOTIFYList.length - idx);
    for (var i = idx; i < SDNOTIFYList.length; i++) {
        SDNOTIFYList[i].ToByte(MemStream);
    }
}

Persistence.Notify.Methods.ByteToSDNOTIFYList = function(SDNOTIFYList, MemStream)
{
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (var i = 0; i < Count; i++) {
        var UnSDNOTIFY = new Persistence.Notify.Properties.TSDNOTIFY();
        UnSDNOTIFY.ByteTo(MemStream);
        Persistence.Notify.Methods.SDNOTIFY_ListAdd(SDNOTIFYList, UnSDNOTIFY);
    }
}




