﻿Persistence.Notify.Methods.GetNotify_ADD_bySteep = function(inIDMDMODELTYPED,inSTATUSN,inIDSDCASE)
{
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE.IDSDCASE.FieldName, inIDSDCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Persistence.Profiler.GetNotify_ADD_bySteep(inIDMDMODELTYPED, inSTATUSN, inIDSDCASE, inIDSDCASE.toString(), Param.ToBytes());
    }
    finally
    {
        Param.Destroy();
    }
}

Persistence.Notify.Methods.GetNotify_ADD_byTypeEvent = function (inIDSDNOTIFYTYPEEVENT, inIDMDSERVICETYPE, inIDSDCASE) {
    //return;
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE.IDSDCASE.FieldName, inIDSDCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Persistence.Profiler.GetNotify_ADD_byTypeEvent(inIDSDNOTIFYTYPEEVENT, inIDMDSERVICETYPE, inIDSDCASE, inIDSDCASE.toString(), Param.ToBytes());
    }
    finally {
        Param.Destroy();
    }
}