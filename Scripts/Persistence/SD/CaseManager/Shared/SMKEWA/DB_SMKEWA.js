﻿//**********************   PROPERTIES   *****************************************************************************************
Persistence.SD.Properties.TSMKEWA = function ()
{
    this.IDMDCATEGORYDETAIL = 0;
    this.KE_IDCMDBCI = 0;
    this.KE_CI_SERIALNUMBER = 0;
    this.KE_CI_GENERICNAME = "";
    this.KE_CI_DESCRIPTION = "";
    this.KE_IDCMDBCIDEFINE = 0;
    this.KE_KNOWNERRORSINFORMATION = "";
    this.WA_IDCMDBCI = 0;
    this.WA_CI_SERIALNUMBER = 0;
    this.WA_CI_GENERICNAME = "";
    this.WA_CI_DESCRIPTION = "";
    this.WA_IDCMDBCIDEFINE = 0;
    this.WA_WORKAROUNDINFORMATION = "";

    this.SDCASEList = new Array();
}

Persistence.SD.Properties.TKE_CI = function ()
{
    this.IDMDCATEGORYDETAIL = 0;
    this.KE_IDCMDBCI = 0;
    this.KE_CI_SERIALNUMBER = 0;
    this.KE_CI_GENERICNAME = "";
    this.KE_CI_DESCRIPTION = "";
    this.KE_IDCMDBCIDEFINE = 0;
    this.KE_KNOWNERRORSINFORMATION = "";
    this.WA_CIList = new Array();
}

Persistence.SD.Properties.TWA_CI = function ()
{
    this.WA_IDCMDBCI = 0;
    this.WA_CI_SERIALNUMBER = 0;
    this.WA_CI_GENERICNAME = "";
    this.WA_CI_DESCRIPTION = "";
    this.WA_IDCMDBCIDEFINE = 0;
    this.WA_WORKAROUNDINFORMATION = "";
    this.IDMDCATEGORYDETAIL_SDCASE = 0;
    this.IDCMDBCI = 0;
    this.CI_SERIALNUMBER = 0;
    this.CI_GENERICNAME = "";
    this.CI_DESCRIPTION = "";
    this.IDSMWAREVIEW = 0;
    this.SMWAREVIEW_NAME = "";
    this.WORKAROUNDINFORMATION = "";
    this.ISCHECKED = false;
}

Persistence.SD.Properties.TSDCASE= function ()
{
    this.IDMDCATEGORYDETAIL_SDCASE = 0;
    this.IDMDCATEGORYDETAIL = 0;
    this.IDCMDBCI = 0;
    this.CI_SERIALNUMBER = 0;
    this.CI_GENERICNAME = "";
    this.CI_DESCRIPTION = "";
    this.IDSMWAREVIEW = 0;
    this.SMWAREVIEW_NAME = "";
    this.WORKAROUNDINFORMATION = "";
}


//**********************   METHODS server  ********************************************************************************************

Persistence.SD.Methods.SMKEWA_GET = function(SMKEWAList, IDMDCATEGORYDETAIL)
{
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_CMDBCI.IDMDCATEGORYDETAIL.FieldName, IDMDCATEGORYDETAIL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var DS_SMKEWA = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "SMKEWA_GET", Param.ToBytes());
        ResErr = DS_SMKEWA.ResErr;
        if (ResErr.NotError)
        {
            if (DS_SMKEWA.DataSet.RecordCount > 0) 
            {
                DS_SMKEWA.DataSet.First();
                while (!(DS_SMKEWA.DataSet.Eof))
                {
                    var SMKEWA = Persistence.SD.Methods.SMKEWA_ListSetID(SMKEWAList, DS_SMKEWA.DataSet.RecordSet.FieldName("IDMDCATEGORYDETAIL").asInt32(), DS_SMKEWA.DataSet.RecordSet.FieldName("KE_IDCMDBCI").asInt32(), DS_SMKEWA.DataSet.RecordSet.FieldName("WA_IDCMDBCI").asInt32());
                    SMKEWA.IDMDCATEGORYDETAIL = DS_SMKEWA.DataSet.RecordSet.FieldName("IDMDCATEGORYDETAIL").asInt32();
                    SMKEWA.KE_IDCMDBCI = DS_SMKEWA.DataSet.RecordSet.FieldName("KE_IDCMDBCI").asInt32();
                    SMKEWA.KE_CI_SERIALNUMBER = DS_SMKEWA.DataSet.RecordSet.FieldName("KE_CI_SERIALNUMBER").asInt32();
                    SMKEWA.KE_CI_GENERICNAME = DS_SMKEWA.DataSet.RecordSet.FieldName("KE_CI_GENERICNAME").asString();
                    SMKEWA.KE_CI_DESCRIPTION = DS_SMKEWA.DataSet.RecordSet.FieldName("KE_CI_DESCRIPTION").asString();
                    SMKEWA.KE_IDCMDBCIDEFINE = DS_SMKEWA.DataSet.RecordSet.FieldName("KE_IDCMDBCIDEFINE").asInt32();
                    SMKEWA.KE_KNOWNERRORSINFORMATION = DS_SMKEWA.DataSet.RecordSet.FieldName("KE_KNOWNERRORSINFORMATION").asString();
                    SMKEWA.WA_IDCMDBCI = DS_SMKEWA.DataSet.RecordSet.FieldName("WA_IDCMDBCI").asInt32();
                    SMKEWA.WA_CI_SERIALNUMBER = DS_SMKEWA.DataSet.RecordSet.FieldName("WA_CI_SERIALNUMBER").asInt32();
                    SMKEWA.WA_CI_GENERICNAME = DS_SMKEWA.DataSet.RecordSet.FieldName("WA_CI_GENERICNAME").asString();
                    SMKEWA.WA_CI_DESCRIPTION = DS_SMKEWA.DataSet.RecordSet.FieldName("WA_CI_DESCRIPTION").asString();
                    SMKEWA.WA_IDCMDBCIDEFINE = DS_SMKEWA.DataSet.RecordSet.FieldName("WA_IDCMDBCIDEFINE").asInt32();
                    SMKEWA.WA_WORKAROUNDINFORMATION = DS_SMKEWA.DataSet.RecordSet.FieldName("WA_WORKAROUNDINFORMATION").asString();

                    Persistence.SD.Methods.SMKEWA_ListAdd(SMKEWAList, SMKEWA);
                    DS_SMKEWA.DataSet.Next();
                }
            }
            else 
            {
                DS_SMKEWA.ResErr.NotError = false;
                DS_SMKEWA.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}

Persistence.SD.Methods.SMKEWA_ListSetID = function(SMKEWAList, IDMDCATEGORYDETAIL, KE_IDCMDBCI, WA_IDCMDBCI)
{
    for (var i = 0; i < SMKEWAList.length; i++)
    {
        if ((IDMDCATEGORYDETAIL == SMKEWAList[i].IDMDCATEGORYDETAIL) && (KE_IDCMDBCI == SMKEWAList[i].KE_IDCMDBCI) && (WA_IDCMDBCI == SMKEWAList[i].WA_IDCMDBCI))
            return (SMKEWAList[i]);
    }
    var SMKEWA = new Persistence.SD.Properties.TSMKEWA();
    SMKEWA.IDMDCATEGORYDETAIL = IDMDCATEGORYDETAIL;
    SMKEWA.WA_IDCMDBCI = WA_IDCMDBCI;
    SMKEWA.KE_IDCMDBCI = KE_IDCMDBCI;
    return SMKEWA;
}

Persistence.SD.Methods.SMKEWA_ListAdd = function (SMKEWAList, SMKEWA)
{
    var i = Persistence.SD.Methods.SMKEWA_ListGetIndex(SMKEWAList, SMKEWA);
    if (i == -1) 
        SMKEWAList.push(SMKEWA);
    else
    {
        SMKEWAList[i].IDMDCATEGORYDETAIL = SMKEWA.IDMDCATEGORYDETAIL;
        SMKEWAList[i].KE_IDCMDBCI = SMKEWA.KE_IDCMDBCI;
        SMKEWAList[i].KE_CI_SERIALNUMBER = SMKEWA.KE_CI_SERIALNUMBER;
        SMKEWAList[i].KE_CI_GENERICNAME = SMKEWA.KE_CI_GENERICNAME;
        SMKEWAList[i].KE_CI_DESCRIPTION = SMKEWA.KE_CI_DESCRIPTION;
        SMKEWAList[i].KE_IDCMDBCIDEFINE = SMKEWA.KE_IDCMDBCIDEFINE;
        SMKEWAList[i].KE_KNOWNERRORSINFORMATION = SMKEWA.KE_KNOWNERRORSINFORMATION;
        SMKEWAList[i].WA_IDCMDBCI = SMKEWA.WA_IDCMDBCI;
        SMKEWAList[i].WA_CI_SERIALNUMBER = SMKEWA.WA_CI_SERIALNUMBER;
        SMKEWAList[i].WA_CI_GENERICNAME = SMKEWA.WA_CI_GENERICNAME;
        SMKEWAList[i].WA_CI_DESCRIPTION = SMKEWA.WA_CI_DESCRIPTION;
        SMKEWAList[i].WA_IDCMDBCIDEFINE = SMKEWA.WA_IDCMDBCIDEFINE;
        SMKEWAList[i].WA_WORKAROUNDINFORMATION = SMKEWA.WA_WORKAROUNDINFORMATION;
    }
}

Persistence.SD.Methods.SMKEWA_ListGetIndex = function (SMKEWAList, SMKEWA)
{
    for (var i = 0; i < SMKEWAList.length; i++)
    {
        if ((SMKEWA.IDMDCATEGORYDETAIL == SMKEWAList[i].IDMDCATEGORYDETAIL) && (SMKEWA.KE_IDCMDBCI == SMKEWAList[i].KE_IDCMDBCI)&& (SMKEWA.WA_IDCMDBCI == SMKEWAList[i].WA_IDCMDBCI))
            return (i);
    }
    return -1;
}

Persistence.SD.Methods.SMKEWA_ADD = function(IDMDCATEGORYDETAIL, IDSDCASE, IDSDCMDBCI)
{
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_SDCASE.IDMDCATEGORYDETAIL.FieldName, IDMDCATEGORYDETAIL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_SDCASE.IDSDCASE.FieldName, IDSDCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_SDCASE.IDCMDBCI.FieldName, IDSDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "SAVE_WORKARROWN", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}

Persistence.SD.Methods.SMKEWA_DEL = function (IDMDCATEGORYDETAIL_SDCASE)
{
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_SDCASE.IDMDCATEGORYDETAIL_SDCASE.FieldName, IDMDCATEGORYDETAIL_SDCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "DELETE_WORKARROWN", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}



Persistence.SD.Methods.SDCASE_GET = function(SDCASEList, IDSDCASE)
{
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_SDCASE.IDSDCASE.FieldName, IDSDCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        var Open = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDCATEGORYDETAIL_SDCASE_GET", Param.ToBytes());
        ResErr = Open.ResErr;
        if (ResErr.NotError)
        {
            if (Open.DataSet.RecordCount > 0)
            {   
                Open.DataSet.First();
                while (!(Open.DataSet.Eof))
                {
                    var SDCASE = Persistence.SD.Methods.SDCASE_ListSetID(SDCASEList, Open.DataSet.RecordSet.FieldName("IDMDCATEGORYDETAIL").asInt32(), Open.DataSet.RecordSet.FieldName("IDCMDBCI").asInt32());
                    SDCASE.IDMDCATEGORYDETAIL = Open.DataSet.RecordSet.FieldName("IDMDCATEGORYDETAIL").asInt32();
                    SDCASE.IDMDCATEGORYDETAIL_SDCASE = Open.DataSet.RecordSet.FieldName("IDMDCATEGORYDETAIL_SDCASE").asInt32();
                    SDCASE.IDCMDBCI = Open.DataSet.RecordSet.FieldName("IDCMDBCI").asInt32();
                    SDCASE.CI_SERIALNUMBER = Open.DataSet.RecordSet.FieldName("CI_SERIALNUMBER").asInt32();
                    SDCASE.CI_GENERICNAME = Open.DataSet.RecordSet.FieldName("CI_GENERICNAME").asString();
                    SDCASE.CI_DESCRIPTION = Open.DataSet.RecordSet.FieldName("CI_DESCRIPTION").asString();
                    SDCASE.IDSMWAREVIEW = Open.DataSet.RecordSet.FieldName("IDSMWAREVIEW").asInt32();
                    SDCASE.SMWAREVIEW_NAME = Open.DataSet.RecordSet.FieldName("SMWAREVIEW_NAME").asString();
                    SDCASE.WORKAROUNDINFORMATION = Open.DataSet.RecordSet.FieldName("WORKAROUNDINFORMATION").asString();
                    
                    Persistence.SD.Methods.SDCASE_ListAdd(SDCASEList, SDCASE);

                    Open.DataSet.Next();
                }
            }
        }
        else
        {
            Open.ResErr.NotError = false;
            Open.ResErr.Mesaje = "Record Count = 0";
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}

Persistence.SD.Methods.SDCASE_ListSetID = function(SDCASEList, IDMDCATEGORYDETAIL, IDCMDBCI)
{
    for (var i = 0; i < SDCASEList.length; i++)
    {
        if ((IDMDCATEGORYDETAIL == SDCASEList[i].IDMDCATEGORYDETAIL)&&(IDCMDBCI == SDCASEList[i].IDCMDBCI))
            return (SDCASEList[i]);
    }
    var SDCASE = new Persistence.SD.Properties.TSDCASE();
    SDCASE.IDMDCATEGORYDETAIL = IDMDCATEGORYDETAIL;
    SDCASE.IDCMDBCI = IDCMDBCI;
    return SDCASE;
}

Persistence.SD.Methods.SDCASE_ListAdd = function(SDCASEList, SDCASE)
{
    var i = Persistence.SD.Methods.SDCASE_ListGetIndex(SDCASEList, SDCASE);
    if (i == -1) 
        SDCASEList.push(SDCASE);
    else
    {
        SDCASEList[i].IDMDCATEGORYDETAIL_SDCASE = SDCASE.IDMDCATEGORYDETAIL_SDCASE;
        SDCASEList[i].IDCMDBCI = SDCASE.IDCMDBCI;
        SDCASEList[i].CI_SERIALNUMBER = SDCASE.CI_SERIALNUMBER;
        SDCASEList[i].CI_GENERICNAME = SDCASE.CI_GENERICNAME;
        SDCASEList[i].CI_DESCRIPTION = SDCASE.CI_DESCRIPTION;
        SDCASEList[i].IDSMWAREVIEW = SDCASE.IDSMWAREVIEW;
        SDCASEList[i].SMWAREVIEW_NAME = SDCASE.SMWAREVIEW_NAME;
        SDCASEList[i].WORKAROUNDINFORMATION = SDCASE.WORKAROUNDINFORMATION;
    }
}

Persistence.SD.Methods.SDCASE_ListGetIndex = function(SDCASEList, SDCASE)
{
    for (var i = 0; i < SDCASEList.length; i++)
    {
        if ((SDCASE.IDMDCATEGORYDETAIL == SDCASEList[i].IDMDCATEGORYDETAIL)&&(SDCASE.IDCMDBCI == SDCASEList[i].IDCMDBCI))
            return (i);
    }
    return -1;
}

Persistence.SD.Methods.SDCASE_UPD = function(IDMDCATEGORYDETAIL_SDCASE, IDSMWAREVIEW)
{
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_SDCASE.IDMDCATEGORYDETAIL_SDCASE.FieldName, IDMDCATEGORYDETAIL_SDCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_SDCASE.IDSMWAREVIEW.FieldName, IDSMWAREVIEW, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "UPDATE_WORKARROWN", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}



Persistence.SD.Methods.INDEXAR_KE_WA = function(SMKEWAList, SDCASEList, KE_CI, WA_CI)
{
    for (var i = 0; i < SMKEWAList.length; i++)//PaddingCount
    {
        var KE_CI_Temp = Persistence.SD.Methods.KE_CI_ListSetID(KE_CI,SMKEWAList[i].KE_IDCMDBCI);
        KE_CI_Temp.IDMDCATEGORYDETAIL = SMKEWAList[i].IDMDCATEGORYDETAIL;
        KE_CI_Temp.KE_IDCMDBCI = SMKEWAList[i].KE_IDCMDBCI;
        KE_CI_Temp.KE_CI_SERIALNUMBER = SMKEWAList[i].KE_CI_SERIALNUMBER;
        KE_CI_Temp.KE_CI_GENERICNAME = SMKEWAList[i].KE_CI_GENERICNAME;
        KE_CI_Temp.KE_CI_DESCRIPTION = SMKEWAList[i].KE_CI_DESCRIPTION;
        KE_CI_Temp.KE_IDCMDBCIDEFINE = SMKEWAList[i].KE_IDCMDBCIDEFINE;
        KE_CI_Temp.KE_KNOWNERRORSINFORMATION = SMKEWAList[i].KE_KNOWNERRORSINFORMATION;

        var WA_CI_Temp = Persistence.SD.Methods.WA_CI_ListSetID(WA_CI, SMKEWAList[i].WA_IDCMDBCI);
        WA_CI_Temp.WA_IDCMDBCI = SMKEWAList[i].WA_IDCMDBCI;
        WA_CI_Temp.WA_CI_SERIALNUMBER = SMKEWAList[i].WA_CI_SERIALNUMBER;
        WA_CI_Temp.WA_CI_GENERICNAME = SMKEWAList[i].WA_CI_GENERICNAME;
        WA_CI_Temp.WA_CI_DESCRIPTION = SMKEWAList[i].WA_CI_DESCRIPTION;
        WA_CI_Temp.WA_IDCMDBCIDEFINE = SMKEWAList[i].WA_IDCMDBCIDEFINE;
        WA_CI_Temp.WA_WORKAROUNDINFORMATION = SMKEWAList[i].WA_WORKAROUNDINFORMATION;

        //********

        var SDCASE = Persistence.SD.Methods.SDCASE_ListSetID(SDCASEList, KE_CI_Temp.IDMDCATEGORYDETAIL, WA_CI_Temp.WA_IDCMDBCI);
        WA_CI_Temp.IDMDCATEGORYDETAIL_SDCASE = SDCASE.IDMDCATEGORYDETAIL_SDCASE;
        WA_CI_Temp.IDSMWAREVIEW = SDCASE.IDSMWAREVIEW;
        WA_CI_Temp.SMWAREVIEW_NAME = SDCASE.SMWAREVIEW_NAME;
        WA_CI_Temp.WORKAROUNDINFORMATION = SDCASE.WORKAROUNDINFORMATION;
        WA_CI_Temp.ISCHECKED = (SDCASE.IDMDCATEGORYDETAIL_SDCASE > 0);

        Persistence.SD.Methods.WA_CI_ListAdd(KE_CI_Temp.WA_CIList, WA_CI_Temp);
        Persistence.SD.Methods.WA_CI_ListAdd(WA_CI, WA_CI_Temp);
        Persistence.SD.Methods.KE_CI_ListAdd(KE_CI, KE_CI_Temp);
    }
}



Persistence.SD.Methods.KE_CI_ListSetID = function(KE_CIList, KE_IDCMDBCI)
{
    for (var i = 0; i < KE_CIList.length; i++)
    {
        if (KE_IDCMDBCI == KE_CIList[i].KE_IDCMDBCI)
            return (KE_CIList[i]);
    }
    var KE_CI = new Persistence.SD.Properties.TKE_CI();
    KE_CI.KE_IDCMDBCI = KE_IDCMDBCI;
    return KE_CI;
}
Persistence.SD.Methods.KE_CI_ListAdd = function(KE_CIList, KE_CI)
{
    var i = Persistence.SD.Methods.KE_CI_ListGetIndex(KE_CIList, KE_CI);
    if (i == -1) 
        KE_CIList.push(KE_CI);
    else
    {
        KE_CIList[i].IDMDCATEGORYDETAIL = KE_CI.IDMDCATEGORYDETAIL;
        KE_CIList[i].KE_IDCMDBCI = KE_CI.KE_IDCMDBCI;
        KE_CIList[i].KE_CI_SERIALNUMBER = KE_CI.KE_CI_SERIALNUMBER;
        KE_CIList[i].KE_CI_GENERICNAME = KE_CI.KE_CI_GENERICNAME;
        KE_CIList[i].KE_CI_DESCRIPTION = KE_CI.KE_CI_DESCRIPTION;
        KE_CIList[i].KE_IDCMDBCIDEFINE = KE_CI.KE_IDCMDBCIDEFINE;
        KE_CIList[i].KE_KNOWNERRORSINFORMATION = KE_CI.KE_KNOWNERRORSINFORMATION;
    }
}
Persistence.SD.Methods.KE_CI_ListGetIndex = function(KE_CIList, KE_CI)
{
    for (var i = 0; i < KE_CIList.length; i++)
    {
        if (KE_CI.KE_IDCMDBCI == KE_CIList[i].KE_IDCMDBCI)
            return (i);
    }
    return -1;
}



Persistence.SD.Methods.WA_CI_ListSetID = function(WA_CIList, WA_IDCMDBCI)
{
    for (var i = 0; i < WA_CIList.length; i++)
    {
        if (WA_IDCMDBCI == WA_CIList[i].WA_IDCMDBCI)
            return (WA_CIList[i]);
    }
    var WA_CI = new Persistence.SD.Properties.TWA_CI();
    WA_CI.WA_IDCMDBCI = WA_IDCMDBCI;
    return WA_CI;
}
Persistence.SD.Methods.WA_CI_ListAdd = function(WA_CIList, WA_CI)
{
    var i = Persistence.SD.Methods.WA_CI_ListGetIndex(WA_CIList, WA_CI);
    if (i == -1) 
        WA_CIList.push(WA_CI);
    else
    {
        WA_CIList[i].WA_CI_SERIALNUMBER = WA_CI.WA_CI_SERIALNUMBER;
        WA_CIList[i].WA_CI_GENERICNAME = WA_CI.WA_CI_GENERICNAME;
        WA_CIList[i].WA_CI_DESCRIPTION = WA_CI.WA_CI_DESCRIPTION;
        WA_CIList[i].WA_IDCMDBCI = WA_CI.WA_IDCMDBCI;
        WA_CIList[i].WA_CI_SERIALNUMBER = WA_CI.WA_CI_SERIALNUMBER;
        WA_CIList[i].WA_CI_GENERICNAME = WA_CI.WA_CI_GENERICNAME;
        WA_CIList[i].WA_CI_DESCRIPTION = WA_CI.WA_CI_DESCRIPTION;
        WA_CIList[i].WA_IDCMDBCIDEFINE = WA_CI.WA_IDCMDBCIDEFINE;
        WA_CIList[i].WA_WORKAROUNDINFORMATION = WA_CI.WA_WORKAROUNDINFORMATION;
        WA_CIList[i].IDMDCATEGORYDETAIL_SDCASE = WA_CI.IDMDCATEGORYDETAIL_SDCASE;
        WA_CIList[i].IDCMDBCI = WA_CI.IDCMDBCI;
        WA_CIList[i].CI_SERIALNUMBER = WA_CI.CI_SERIALNUMBER;
        WA_CIList[i].CI_GENERICNAME = WA_CI.CI_GENERICNAME;
        WA_CIList[i].CI_DESCRIPTION = WA_CI.CI_DESCRIPTION;
        WA_CIList[i].IDSMWAREVIEW = WA_CI.IDSMWAREVIEW;
        WA_CIList[i].SMWAREVIEW_NAME = WA_CI.SMWAREVIEW_NAME;
        WA_CIList[i].WORKAROUNDINFORMATION = WA_CI.WORKAROUNDINFORMATION;
        WA_CIList[i].ISCHECKED = WA_CI.ISCHECKED;
    }
}
Persistence.SD.Methods.WA_CI_ListGetIndex = function(WA_CIList, WA_CI)
{
    for (var i = 0; i < WA_CIList.length; i++)
    {
        if (WA_CI.WA_IDCMDBCI == WA_CIList[i].WA_IDCMDBCI)
            return (i);
    }
    return -1;
}



