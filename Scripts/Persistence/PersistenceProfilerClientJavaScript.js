﻿//bibliografia https://stackoverflow.com/questions/5346694/how-to-implement-a-lock-in-javascript

Persistence.Profiler.ResErr = new SysCfg.Error.Properties.TResErr();

Persistence.Profiler.UserFunctions = new Persistence.TUserFunctions(-1);
Persistence.Profiler.ReciveChatProfiler = new Persistence.Chat.TChatProfiler();
Persistence.Profiler.ReciveNotifyProfiler = new Persistence.Notify.TNotifyProfiler();
Persistence.Profiler.CatalogProfiler = new Persistence.Catalog.TCatalogProfiler();
Persistence.Profiler.ModelProfiler = new Persistence.Model.TModelProfiler();
Persistence.Profiler.RemoteHelpProfiler = new Persistence.RemoteHelp.TRemoteHelpProfiler();
Persistence.Profiler.Callback = null;

//Persistence.Profiler.thisLock = new Object();
//Persistence.Profiler.functionLock = false;
//Persistence.Profiler.functionCallbacks = [];
//Persistence.Profiler.lockingFunction = function (callback) {
//    if (Persistence.Profiler.functionLock) {
//        Persistence.Profiler.functionCallbacks.push(callback);
//    } else {
//        $.longRunning(function(response) {
//            while (Persistence.Profiler.functionCallbacks.length) {
//                var thisCallback = Persistence.Profiler.functionCallbacks.pop();
//                thisCallback(response);
//            }
//        });
//    }
//}

//******** Funciones generales *************
Persistence.Profiler.Initialize = function(UserATRole)
{
    Persistence.Profiler.UserFunctions = new Persistence.TUserFunctions(UserATRole.User.IDCMDBCI);
    Persistence.Profiler.GetUser(UserATRole.User.IDCMDBUSER);
    Persistence.Profiler.GetCatalog(UserATRole.User.IDCMDBCI);
    Persistence.Profiler.ModelProfiler.Fill();

}



Persistence.Profiler.GetUser = function(IDCMDBUSER)
{
    //Persistence.SMCI.Methods.SDGROUPSERVICEUSERCI_Fill(Persistence.Profiler.UserFunctions.SDGROUPSERVICEUSERCIList, "");
    //Persistence.SMCI.Methods.CMDBUSERCONTACTTYPE_Fill(Persistence.Profiler.UserFunctions.CMDBUSERCONTACTTYPEList, "");
    //Persistence.SMCI.Methods.CMDBUSER_Fill(Persistence.Profiler.UserFunctions.CMDBUSER, "");
    var CMDBUSERSETTINGSList = new Array();//Persistence.SMCI.Properties.TCMDBUSERSETTINGS
    Persistence.SMCI.Methods.CMDBUSERSETTINGS_Fill(CMDBUSERSETTINGSList, IDCMDBUSER.toString());
    if (CMDBUSERSETTINGSList.Count > 0) Profiler.UserFunctions.CMDBUSERSETTINGS = CMDBUSERSETTINGSList[0];
    else
    {
        Persistence.Profiler.UserFunctions.CMDBUSERSETTINGS.IDCMDBUSERSETTINGS = -1;
        Persistence.Profiler.UserFunctions.CMDBUSERSETTINGS.IDCMDBUSER = IDCMDBUSER;
        Persistence.Profiler.UserFunctions.CMDBUSERSETTINGS.IDLANGUAGE = SysCfg.App.Properties.Config_Language.LanguageITF;
        Persistence.Profiler.UserFunctions.CMDBUSERSETTINGS.IDSKINATIS = 0;
        Persistence.Profiler.UserFunctions.CMDBUSERSETTINGS.IDSKINITHELPCENTER = 1;
        Persistence.Profiler.UserFunctions.CMDBUSERSETTINGS.ATENLAYOUT_IDFILE_ATIS = "DefaultLayout";
        Persistence.Profiler.UserFunctions.CMDBUSERSETTINGS.CONSOLEDETAIL_COLUMN_ATIS = 2;
    }
    //Persistence.SMCI.Methods.CMDBUSERADDRESS_Fill(Persistence.Profiler.UserFunctions.CMDBUSERADDRESS, "");   
}


//********************** GetLiveChange *****************************
Persistence.Profiler.ByteToLiveChange = function(GetLiveChange)
{
    var MemStream = new SysCfg.Stream.TMemoryStream();//Solo Se usa este en caso de que no sea tcp
    try
    {
        MemStream.Write(GetLiveChange.ByteLiveChange, 0, GetLiveChange.ByteLiveChange.length);
        MemStream.Position = 0;
        Persistence.Chat.Methods.ByteToSMCHATList(Persistence.Profiler.ReciveChatProfiler.SMCHATList, MemStream);
        Persistence.Chat.Methods.ByteToSMCHATASSIGNEDList(Persistence.Profiler.ReciveChatProfiler.SMCHATASSIGNEDList, MemStream);
        Persistence.Chat.Methods.ByteToSMCHATASSIGNEDMEMBERList(Persistence.Profiler.ReciveChatProfiler.SMCHATASSIGNEDMEMBERList, MemStream);
        Persistence.Profiler.ReciveNotifyProfiler.SDNOTIFYList.length = 0;
        Persistence.Notify.Methods.ByteToSDNOTIFYList(Persistence.Profiler.ReciveNotifyProfiler.SDNOTIFYList, MemStream);
        //lock (Persistence.Profiler.thisLock)
        //{
        Persistence.Profiler.RemoteHelpProfiler.RHREQUESTList.length = 0;
            Persistence.RemoteHelp.Methods.ByteToRHREQUESTList(Persistence.Profiler.RemoteHelpProfiler.RHREQUESTList, MemStream);//Se podria traer tabla por tabla pero no se ve necesario depues se peude cambia a por table mientras byte to lo hace para traerce pc y cr
        //}       
        //Persistence.Profiler.lockingFunction(function () {
        //    Persistence.Profiler.RemoteHelpProfiler.RHREQUESTList.Clear();
        //    Persistence.RemoteHelp.Methods.ByteToRHREQUESTList(Persistence.Profiler.RemoteHelpProfiler.RHREQUESTList, MemStream);//Se podria traer tabla por tabla pero no se ve necesario depues se peude cambia a por table mientras byte to lo hace para traerce pc y cr
        //});
        
        
        


        Persistence.RemoteHelp.Methods.ByteToRHMODULE_ACTIVEList(Persistence.Profiler.RemoteHelpProfiler.RHMODULE_ACTIVEList, MemStream);
    }
    finally
    {
        MemStream.Close();
        MemStream.Dispose();
    }
}




Persistence.Profiler.RelateLiveChange = function()
{

    for (var i = 0; i < Persistence.Profiler.ReciveChatProfiler.SMCHATASSIGNEDList.length; i++)
    {
        var SMCHATASSIGNED = Persistence.Profiler.ReciveChatProfiler.SMCHATASSIGNEDList[i];
        Persistence.Chat.Methods.SMCHATASSIGNED_ListAdd(Persistence.Profiler.UserFunctions.ChatProfiler.SMCHATASSIGNEDList, SMCHATASSIGNED);
        if (SMCHATASSIGNED.IDSMCHATASSIGNEDTYPE == Persistence.Chat.Properties.TSMCHATASSIGNEDTYPE.REPORTE) Persistence.Profiler.UserFunctions.CaseCounter++;
    }

    for (var e = 0; e < Persistence.Profiler.ReciveChatProfiler.SMCHATASSIGNEDMEMBERList.length; e++)
    {
        var SMCHATASSIGNEDMEMBER = Persistence.Profiler.ReciveChatProfiler.SMCHATASSIGNEDMEMBERList[e];
        var SMCHATASSIGNED = Persistence.Chat.Methods.SMCHATASSIGNED_ListSetID(Persistence.Profiler.UserFunctions.ChatProfiler.SMCHATASSIGNEDList, SMCHATASSIGNEDMEMBER.IDSMCHATASSIGNED);
        Persistence.Chat.Methods.SMCHATASSIGNEDMEMBER_ListAdd(SMCHATASSIGNED.SMCHATASSIGNEDMEMBERList, SMCHATASSIGNEDMEMBER);
    }

    for (var e = 0; e < Persistence.Profiler.ReciveChatProfiler.SMCHATList.length; e++)
    {
        var SMCHAT = Persistence.Profiler.ReciveChatProfiler.SMCHATList[e];
        var SMCHATASSIGNED = Persistence.Chat.Methods.SMCHATASSIGNED_ListSetID(Persistence.Profiler.UserFunctions.ChatProfiler.SMCHATASSIGNEDList, SMCHAT.IDSMCHATASSIGNED);
        Persistence.Chat.Methods.SMCHAT_ListAdd(SMCHATASSIGNED.SMCHATList, SMCHAT);
        for (var w = 0; w < SMCHATASSIGNED.SMCHATASSIGNEDMEMBERList.length; w++)
        {
            if (SMCHAT.IDCMDBCI_SEND == SMCHATASSIGNED.SMCHATASSIGNEDMEMBERList[w].IDCMDBCI_MEMBER)
            {
                SMCHAT.SMCHATASSIGNEDMEMBER = SMCHATASSIGNED.SMCHATASSIGNEDMEMBERList[w];
            }

        }

    }
    for (var e = 0; e < Persistence.Profiler.ReciveNotifyProfiler.SDNOTIFYList.length; e++)
    {
        var SDNOTIFY = Persistence.Profiler.ReciveNotifyProfiler.SDNOTIFYList[e];
        Persistence.Notify.Methods.SDNOTIFY_ListAdd(Persistence.Profiler.UserFunctions.NotifyProfiler.SDNOTIFYList, SDNOTIFY);
    }

    //#if (ATIS)
    //lock (Persistence.Profiler.thisLock)
    //{
    var Salir = false;
    for (var e = 0; e < Persistence.Profiler.RemoteHelpProfiler.RHREQUESTList.length; e++)
    {
        var inRHREQUEST = Persistence.Profiler.RemoteHelpProfiler.RHREQUESTList[e];
        if ((inRHREQUEST.IDRHMODULE_EXECUTE == Persistence.RemoteHelp.Properties.TRHMODULE._ATIS) && (inRHREQUEST.REQUEST_STATUS == Persistence.RemoteHelp.Properties.TREQUEST_STATUS._CONFIGURE))
        {
            var idx = Persistence.RemoteHelp.Methods.RHREQUEST_ListGetIndex(Persistence.Profiler.UserFunctions.RHREQUESTList, inRHREQUEST);
            if (idx == -1)
            {
                var RHREQUEST = new Persistence.RemoteHelp.Properties.TRHREQUEST();
                Persistence.RemoteHelp.Methods.RHREQUEST_Copy(inRHREQUEST, RHREQUEST);
                for (var i = 0; i < inRHREQUEST.RHREQUEST_CPUCRList.length; i++)
                {
                    var RHREQUEST_CPUCR = new Persistence.RemoteHelp.Properties.TRHREQUEST_CPUCR();

                    if (inRHREQUEST.RHREQUEST_CPUCRList[i].REQUEST_CPUCR_STATUS != Persistence.RemoteHelp.Properties.TREQUEST_CPUCR_STATUS._CONFIGURE)
                        Salir = true;

                    Persistence.RemoteHelp.Methods.RHREQUEST_CPUCR_Copy(inRHREQUEST.RHREQUEST_CPUCRList[i], RHREQUEST_CPUCR);
                    Persistence.RemoteHelp.Methods.RHREQUEST_CPUCR_ListAdd(RHREQUEST.RHREQUEST_CPUCRList, RHREQUEST_CPUCR);
                }
                for (var i = 0; i < inRHREQUEST.RHREQUEST_CPUPLList.length; i++)
                {
                    var RHREQUEST_CPUPL = new Persistence.RemoteHelp.Properties.TRHREQUEST_CPUPL();
                    Persistence.RemoteHelp.Methods.RHREQUEST_CPUPL_Copy(inRHREQUEST.RHREQUEST_CPUPLList[i], RHREQUEST_CPUPL);
                    Persistence.RemoteHelp.Methods.RHREQUEST_CPUPL_ListAdd(RHREQUEST.RHREQUEST_CPUPLList, RHREQUEST_CPUPL);
                }
                if (!Salir)
                    Persistence.RemoteHelp.Methods.RHREQUEST_ListAdd(Persistence.Profiler.UserFunctions.RHREQUESTList, RHREQUEST);
            }
        }

    }
    //}
    //#endif
    for (var e = 0; e < Persistence.Profiler.RemoteHelpProfiler.RHMODULE_ACTIVEList.length; e++)
    {
        var RHMODULE_ACTIVE = Persistence.Profiler.RemoteHelpProfiler.RHMODULE_ACTIVEList[e];
        Persistence.RemoteHelp.Methods.RHMODULE_ACTIVE_ListAdd(Persistence.Profiler.UserFunctions.RHMODULE_ACTIVEList, RHMODULE_ACTIVE);
    }
}
// Live change 
Persistence.Profiler.GetLiveChange = function()
{
    var GetLiveChange = new Comunic.Properties.TGetLiveChange();
    var IDSMCHAT = Persistence.Profiler.UserFunctions.ChatProfiler.SMCHATList.length;
    var IDSMCHATASSIGNED = 0;//UserFunctions.ChatProfiler.SMCHATASSIGNEDList.Count;
    var IDSMCHATASSIGNEDMEMBER = 0; //UserFunctions.ChatProfiler.SMCHATASSIGNEDMEMBERList.Count; ;
    var IDSDNOTIFY = 0; // UserFunctions.NotifyProfiler.SDNOTIFYList.Count;
    var IDRHREQUEST = 0;
    var IDRHMODULE = 0;
    var IDRHMODULE_ACTIVE = true;
                         
    var IDRHMODULE_SET = Persistence.RemoteHelp.Properties.TRHMODULE._ATIS.value;
    var IDRHMODULE_DATE = SysCfg.DB.Properties.Cfg_Bdd.NumDateTimeServer;//parseFloat(SysCfg.DB.Properties.Cfg_Bdd.NumDateTimeServer);// DateTime.FromOADate(SysCfg.DB.Properties.Cfg_Bdd.NumDateTimeServer);
    GetLiveChange = Comunic.AjaxJson.Client.Methods.GetLiveChange(
        Comunic.Properties.TLiveChangeType.OnlyLive,
        Persistence.Profiler.UserFunctions.IDCMDBCI,
        IDSMCHAT,
        IDSMCHATASSIGNED,
        IDSMCHATASSIGNEDMEMBER,
        IDSDNOTIFY,
        IDRHREQUEST,
        IDRHMODULE,
        IDRHMODULE_ACTIVE,
        IDRHMODULE_SET,
        IDRHMODULE_DATE
        );

    

    Persistence.Profiler.OnCompletedGetLiveChange(GetLiveChange, null);
}

Persistence.Profiler.OnCompletedGetLiveChange = function(sender, e)
{
    var GetLiveChange = sender;//Comunic.Sockets.Client.Properties.TGetLiveChange
    if (GetLiveChange != null)
    {

        if (GetLiveChange.ResErr.NotError)
        {
            if (!GetLiveChange.IDRHMODULE_ACTIVE)
            {
                System.Windows.Browser.HtmlPage.Window.Navigate(System.Windows.Browser.HtmlPage.Document.DocumentUri);
            }
            Persistence.Profiler.ByteToLiveChange(GetLiveChange);
            Persistence.Profiler.RelateLiveChange();
            //if (OnTimerTick != null) OnTimerTick(null, null);

            Persistence.Profiler.Callback(Persistence.Profiler, e);
            Persistence.Profiler.NoRunThread = true;
        }
        else
        {
            //System.Windows.Browser.HtmlPage.Window.Navigate(System.Windows.Browser.HtmlPage.Document.DocumentUri);
            ItHelpCenter.PageManager.GoToPageType(ItHelpCenter.PageManager.TGoToPageType.CloseSession);
        }
    }
}


//********************** GetChat *****************************        
// Reportar un problema 
Persistence.Profiler.GetChatASSIGNED_OWNER = function(MESSAGE,TITLE)
{
    var GetChat = Comunic.Sockets.Client.Methods.GetChat(Comunic.Properties.TChatType.ASSIGNED_OWNER, Persistence.Profiler.UserFunctions.IDCMDBCI, MESSAGE, TITLE, -1, -1, -1);
    OnCompletedGetChatASSIGNED_OWNER(GetChat, null);
}
Persistence.Profiler.OnCompletedGetChatASSIGNED_OWNER = function(sender, e)
{
    var GetChat = sender;//Comunic.Sockets.Client.Properties.TGetChat
    if (GetChat != null)
    {
        if (GetChat.ResErr.NotError)
        {
            UsrCfg.Properties.frMain.chatWindow.IDSMCHATASSIGNED_PRENDE = GetChat.IDSMCHATASSIGNED;
        }
    }
}

// Cerrar asignacion 
Persistence.Profiler.GetChatASSIGNED_DISABLE = function(IDSMCHATASSIGNED)
{
    var GetChat = Comunic.Sockets.Client.Methods.GetChat(Comunic.Properties.TChatType.ASSIGNED_DISABLE, Persistence.Profiler.UserFunctions.IDCMDBCI, "", "", IDSMCHATASSIGNED, -1, -1);
    OnCompletedGetChatASSIGNED_DISABLE(GetChat, null);
}
Persistence.Profiler.OnCompletedGetChatASSIGNED_DISABLE = function(sender, e)
{
    var GetChat = sender;//Comunic.Sockets.Client.Properties.TGetChat
    if (GetChat != null)
    {
        if (GetChat.ResErr.NotError)
        {
        }
    }
}

// Cerrar miebro (ocultar) 
Persistence.Profiler.GetChatMEMBER_DISABLE = function(IDSMCHATASSIGNED,IDSMCHATASSIGNEDMEMBER)
{
    var GetChat = Comunic.Sockets.Client.Methods.GetChat(Comunic.Properties.TChatType.MEMBER_DISABLE, Persistence.Profiler.UserFunctions.IDCMDBCI, "", "", IDSMCHATASSIGNED, IDSMCHATASSIGNEDMEMBER, -1);
    OnCompletedGetChatMEMBER_DISABLE(GetChat, null);
}

Persistence.Profiler.OnCompletedGetChatMEMBER_DISABLE = function(sender, e)
{
    var GetChat = sender;//Comunic.Sockets.Client.Properties.TGetChat
    if (GetChat != null)
    {
        if (GetChat.ResErr.NotError)
        {
        }
    }
}

// Mandar un mensaje 
Persistence.Profiler.GetChatSENDS_MESSAGE = function(IDSMCHATASSIGNED, IDSMCHATASSIGNEDMEMBER,  MESSAGE)
{
    var GetChat = Comunic.Sockets.Client.Methods.GetChat(Comunic.Properties.TChatType.SENDS_MESSAGE, Persistence.Profiler.UserFunctions.IDCMDBCI, MESSAGE, "None", IDSMCHATASSIGNED, IDSMCHATASSIGNEDMEMBER, -1);
    OnCompletedGetChatSENDS_MESSAGE(GetChat, null);
}
Persistence.Profiler.OnCompletedGetChatSENDS_MESSAGE = function(sender, e)
{
    var GetChat = sender;//Comunic.Sockets.Client.Properties.TGetChat
    if (GetChat != null)
    {
        if (GetChat.ResErr.NotError)
        {
        }
    }
}

//********************** GetHHH *****************************
//traer UUU
Persistence.Profiler.GetHHH_UUU = function(HHHType,StrHHH1)
{
    var MemStrm_Request = new SysCfg.Stream.TMemoryStream();
    MemStrm_Request.Position = 0;
    SysCfg.Stream.Methods.WriteStreamStrInt16(MemStrm_Request, StrHHH1);
    Byte_Request = new Array();
    Byte_Request = MemStrm_Request.ToArray();
    var GetHHH = Comunic.Sockets.Client.Methods.GetHHH(Comunic.Properties.THHHType._UUU, Byte_Request);
    OnCompletedGetHHH_UUU(GetHHH, null);
}
Persistence.Profiler.OnCompletedGetHHH_UUU = function(sender, e)
{
    Persistence.Profiler.GetHHH = sender;//Comunic.Sockets.Client.Properties.TGetHHH
    if (GetHHH != null)
    {
        if (GetHHH.ResErr.NotError)
        {
            var MemStream = new SysCfg.Stream.TMemoryStream();//Solo Se usa este en caso de que no sea tcp
            try
            {
                MemStream.Write(GetHHH.Byte_Response, 0, GetHHH.Byte_Response.length);
                MemStream.Position = 0;
            }
            finally
            {
                MemStream.Close();
                MemStream.Dispose();
            }

        }
    }
}

//********************** GetCatalog *****************************
//traer UUU
Persistence.Profiler.GetCatalog = function(IDCMDBCI)
{
    var MemStrm_Request = new SysCfg.Stream.TMemoryStream();
    MemStrm_Request.Position = 0;
    SysCfg.Stream.Methods.WriteStreamInt32(MemStrm_Request, IDCMDBCI);
    var Byte_Request = new Array(MemStrm_Request.Length());
    Byte_Request = MemStrm_Request.ToArray();
    var GetCatalog = Comunic.AjaxJson.Client.Methods.GetCatalog(Comunic.Properties.TCatalogType._All, Byte_Request);
    Persistence.Profiler.OnCompletedGetCatalog(GetCatalog, null);
}
Persistence.Profiler.OnCompletedGetCatalog = function(sender, e)
{
    var GetCatalog = sender;//Comunic.Sockets.Client.Properties.TGetCatalog
    if (GetCatalog != null)
    {
        if (GetCatalog.ResErr.NotError)
        {
            var MemStream = new SysCfg.Stream.TMemoryStream();//Solo Se usa este en caso de que no sea tcp
            try
            {
                MemStream.Write(GetCatalog.Byte_Response, 0, GetCatalog.Byte_Response.length);
                MemStream.Position = 0;
                Persistence.Catalog.Methods.ByteToSDTYPEUSERList(Persistence.Profiler.CatalogProfiler.SDTYPEUSERList,  MemStream);
                Persistence.Catalog.Methods.ByteToMDSERVICETYPEList(Persistence.Profiler.CatalogProfiler.MDSERVICETYPEList,  MemStream);
                Persistence.Catalog.Methods.ByteToMDPRIORITYList(Persistence.Profiler.CatalogProfiler.MDPRIORITYList,  MemStream);
                Persistence.Catalog.Methods.ByteToMDHIERPERList(Persistence.Profiler.CatalogProfiler.MDHIERPERList,  MemStream);
                Persistence.Catalog.Methods.ByteToMDFUNCPERList(Persistence.Profiler.CatalogProfiler.MDFUNCPERList,  MemStream);
                Persistence.Catalog.Methods.ByteToMDIMPACTList(Persistence.Profiler.CatalogProfiler.MDIMPACTList,  MemStream);
                Persistence.Catalog.Methods.ByteToMDURGENCYList(Persistence.Profiler.CatalogProfiler.MDURGENCYList,  MemStream);
                Persistence.Catalog.Methods.ByteToMDPRIORITYMATRIXList(Persistence.Profiler.CatalogProfiler.MDPRIORITYMATRIXList,  MemStream);
                Persistence.RemoteHelp.Methods.ByteToIP_SERVERSList(Persistence.Profiler.RemoteHelpProfiler.IP_SERVERSList,  MemStream);
            }
            finally
            {
                MemStream.Close();
                MemStream.Dispose();
            }

        }
    }
}


Persistence.Profiler.GetRHREQUEST = function(RHREQUESTType, RHREQUEST)
{
    var MemStrm_Request= new SysCfg.Stream.TMemoryStream();
    MemStrm_Request.Position = 0;
    RHREQUEST.ToByte(MemStrm_Request);
    var Byte_Request = new Array(MemStrm_Request.Length());
    Byte_Request = MemStrm_Request.ToArray();
    var GetRHREQUEST = Comunic.Sockets.Client.Methods.GetRHREQUEST(RHREQUESTType, Byte_Request);
    OnCompletedGetRHREQUEST(GetRHREQUEST, null);
}
Persistence.Profiler.OnCompletedGetRHREQUEST = function(sender,e)
{
    var GetRHREQUEST = sender;//Comunic.Sockets.Client.Properties.TGetRHREQUEST
    if (GetRHREQUEST != null)
    {
        if (GetRHREQUEST.ResErr.NotError)
        {
            var MemStream = new SysCfg.Stream.TMemoryStream();//Solo Se usa este en caso de que no sea tcp
            try
            {
                MemStream.Write(GetRHREQUEST.Byte_Response, 0, GetRHREQUEST.Byte_Response.length);
                MemStream.Position = 0;
                var RHREQUEST = new RemoteHelp.Properties.TRHREQUEST();
                RHREQUEST.ByteTo(MemStream);
                if ((RHREQUEST.REQUEST_STATUS == RemoteHelp.Properties.TREQUEST_STATUS._CANCELLED) && (GetRHREQUEST.RHREQUESTType == Comunic.Properties.TRHREQUESTType.CREATE))
                {
                    SysCfg.App.Methods.ShowMessage("No permissions were found available, and the request was cancelled");
                }
            }
            finally
            {
                MemStream.Close();
                MemStream.Dispose();
            }

        }
    }
}

//********************** GetNotify *****************************        
Persistence.Profiler.GetNotify_ADD_bySteep = function(IDMDMODELTYPED,STATUSN,IDSDCASE,STRParam,SQLParam)
{
    var MemStrm_Request= new SysCfg.Stream.TMemoryStream();
    MemStrm_Request.Position = 0;
    SysCfg.Stream.Methods.WriteStreamInt32(MemStrm_Request, IDMDMODELTYPED);
    SysCfg.Stream.Methods.WriteStreamInt32(MemStrm_Request, STATUSN);
    SysCfg.Stream.Methods.WriteStreamInt32(MemStrm_Request, IDSDCASE);
    SysCfg.Stream.Methods.WriteStreamStrInt16(MemStrm_Request, STRParam);
    SysCfg.Stream.Methods.WriteStreamByteInt32(MemStrm_Request, SQLParam);
    var Byte_Request = new Array(MemStrm_Request.Length());
    Byte_Request = MemStrm_Request.ToArray();
    var GetNotify = Comunic.AjaxJson.Client.Methods.GetNotify(Comunic.Properties.TNotifyType._ADDbySteep, Byte_Request);
    OnCompletedGetNotify_ADD(GetNotify, null);
}
Persistence.Profiler.GetNotify_ADD_byTypeEvent = function(IDSDNOTIFYTYPEEVENT,  IDMDSERVICETYPE, IDSDCASE, STRParam, SQLParam)//IDSDNOTIFYTEMPLATE,
{
    if (IDMDSERVICETYPE != -1) {
        var MemStrm_Request = new SysCfg.Stream.TMemoryStream();
        MemStrm_Request.Position = 0;
        //SysCfg.Stream.Methods.WriteStreamInt32(MemStrm_Request, IDSDNOTIFYTEMPLATE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStrm_Request, IDSDNOTIFYTYPEEVENT);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStrm_Request, IDMDSERVICETYPE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStrm_Request, IDSDCASE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStrm_Request, STRParam);
        SysCfg.Stream.Methods.WriteStreamByteInt32(MemStrm_Request, SQLParam);
        var Byte_Request = new Array(MemStrm_Request.Length());
        Byte_Request = MemStrm_Request.ToArray();
        var GetNotify = Comunic.AjaxJson.Client.Methods.GetNotify(Comunic.Properties.TNotifyType._ADDbyTypeEvent, Byte_Request);
        OnCompletedGetNotify_ADD(GetNotify, null);
    }
}
Persistence.Profiler.OnCompletedGetNotify_ADD = function(sender, e)
{
    var GetNotify = sender;//Comunic.Sockets.Client.Properties.TGetNotify
    if (GetNotify != null)
    {
        if (GetNotify.ResErr.NotError)
        {
            var MemStream = new SysCfg.Stream.TMemoryStream();//Solo Se usa este en caso de que no sea tcp
            try
            {
                MemStream.Write(GetNotify.Byte_Response, 0, GetNotify.Byte_Response.length);
                MemStream.Position = 0;
                var SDNOTIFY_Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
                SysCfg.App.Methods.ShowMessage(SDNOTIFY_Count.toString());
            }
            finally
            {
                MemStream.Close();
                MemStream.Dispose();
            }

        }
    }
}


//traer ISSENDCONSOLE
Persistence.Profiler.GetNotify_ISSENDCONSOLE = function(IDSDNOTIFY)
{
    var MemStrm_Request = new SysCfg.Stream.TMemoryStream();
    MemStrm_Request.Position = 0;
    SysCfg.Stream.Methods.WriteStreamInt32(MemStrm_Request, IDSDNOTIFY);
    var Byte_Request = new Byte[MemStrm_Request.Length()];
    Byte_Request = MemStrm_Request.ToArray();
    var GetNotify = Comunic.Sockets.Client.Methods.GetNotify(Comunic.Properties.TNotifyType._ISSENDCONSOLE, Byte_Request);
    OnCompletedGetNotify_ISSENDCONSOLE(GetNotify, null);
}

Persistence.Profiler.OnCompletedGetNotify_ISSENDCONSOLE = function(sender, e)
{
    var GetNotify = sender;//Comunic.Sockets.Client.Properties.TGetNotify
    if (GetNotify != null)
    {
        if (GetNotify.ResErr.NotError)
        {
            var MemStream = new SysCfg.Stream.TMemoryStream();//Solo Se usa este en caso de que no sea tcp
            try
            {
                MemStream.Write(GetNotify.Byte_Response, 0, GetNotify.Byte_Response.length);
                MemStream.Position = 0;
                var SDNOTIFY = new Persistence.Notify.Properties.TSDNOTIFY();
                SDNOTIFY.ByteTo(MemStream);
                //SysCfg.App.Methods.ShowMessage(SDNOTIFY.ISSENDCONSOLE.toString());
            }
            finally
            {
                MemStream.Close();
                MemStream.Dispose();
            }
        }
    }
}



/*
Persistence.Profiler.SDNOTIFY_ADD = function(IDSDNOTIFYTEMPLATE,IDSDCASE,IDCMDBCI,IDSDTYPEUSER,STRParam,SQLParam)//NFElimina
{
    var SDNOTIFY_ADD = new UsrCfg.SD.TSDNOTIFY_ADD();
    SDNOTIFY_ADD.SDNOTIFY.IDSDNOTIFYTEMPLATE = IDSDNOTIFYTEMPLATE;
    SDNOTIFY_ADD.SDNOTIFY.IDSDCASE = IDSDCASE;
    SDNOTIFY_ADD.SDNOTIFY.IDCMDBCI = IDCMDBCI;
    SDNOTIFY_ADD.SDNOTIFY.IDSDTYPEUSER = IDSDTYPEUSER;
    SDNOTIFY_ADD.SDNOTIFY.ParamStr = STRParam;
    SDNOTIFY_ADD.SDNOTIFY.ParamSql = SQLParam;
    var GetSDFunction = new UsrCfg.SD.TGetSDFunction();
    GetSDFunction.SDFunction = UsrCfg.SD.Properties.TSDFunction._ADDSDNOTIFY;
    GetSDFunction = Comunic.Sockets.Client.Methods.GetSDFunction(GetSDFunction, SDNOTIFY_ADD);
    return (SDNOTIFY_ADD);
}
*/



//*************** Timer  **************************
Persistence.Profiler.Abort = false;
Persistence.Profiler._Timer = null;
Persistence.Profiler.NoRunThread = true;
Persistence.Profiler.OnTimerTick;
Persistence.Profiler.Start = function (inCallback)
{
    Persistence.Profiler.Callback = inCallback;
    Persistence.Profiler.Abort = false;
    Persistence.Profiler.End();
    Persistence.Profiler._Timer = new TVclTimer();    
    Persistence.Profiler._Timer.Enabled = false;

    Persistence.Profiler._Timer.AddMethod("TimerElapsedPersistence", Persistence.Profiler.TimerElapsedPersistence, 5000); 
    if ((Persistence.Profiler._Timer.Enabled == false))
    {
        Persistence.Profiler._Timer.Start();
    }
}
Persistence.Profiler.End = function()
{
    if (Persistence.Profiler._Timer != null)
    {
        Persistence.Profiler._Timer.Destroy();
        Persistence.Profiler._Timer = null;
        Persistence.Profiler.Abort = true;
    }
}
Persistence.Profiler.TimerElapsedPersistence = function(/*sender, e*/)
{
    if ((Persistence.Profiler.NoRunThread) && (!Persistence.Profiler.Abort))
    {
        Persistence.Profiler.NoRunThread = false;
        Persistence.Profiler.GetLiveChange();
    }
}




