﻿//**********************   PROPERTIES   *****************************************************************************************
Persistence.SMCI.Properties.PERSISTENCE_MODE = Persistence.TPERSISTENCE_MODE._Local;
Persistence.SMCI.Properties.TCMDBUSERSETTINGS = function () {
    this.IDCMDBUSER = 0;
    this.IDCMDBUSERSETTINGS = 0;
    this.IDLANGUAGE = SysCfg.App.Properties.TLanguage.GetEnum(undefined);
    this.IDSKINATIS = 0;
    this.IDSKINITHELPCENTER = 0;
    this.ATENLAYOUT_IDFILE_ATIS = "";
    this.CONSOLEDETAIL_COLUMN_ATIS = 0;
    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDCMDBUSER);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDCMDBUSERSETTINGS);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, parseInt(this.IDLANGUAGE.Value));
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSKINATIS);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSKINITHELPCENTER);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.ATENLAYOUT_IDFILE_ATIS);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.CONSOLEDETAIL_COLUMN_ATIS);
    }
    this.ByteTo = function (MemStream) {
        this.IDCMDBUSER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDCMDBUSERSETTINGS = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDLANGUAGE =  SysCfg.App.Properties.TLanguage.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        this.IDSKINATIS = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSKINITHELPCENTER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.ATENLAYOUT_IDFILE_ATIS = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CONSOLEDETAIL_COLUMN_ATIS = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDCMDBUSER, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDCMDBUSERSETTINGS, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(parseInt(this.IDLANGUAGE.value), Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDSKINATIS, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDSKINITHELPCENTER, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.ATENLAYOUT_IDFILE_ATIS, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.CONSOLEDETAIL_COLUMN_ATIS, Longitud, Texto);
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDCMDBUSER = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDCMDBUSERSETTINGS = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDLANGUAGE =  SysCfg.App.Properties.TLanguage.GetEnum(SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto));
        this.IDSKINATIS = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDSKINITHELPCENTER = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.ATENLAYOUT_IDFILE_ATIS = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.CONSOLEDETAIL_COLUMN_ATIS = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
    }
}
//**********************   METHODS server  ********************************************************************************************
Persistence.SMCI.Methods_DataBase.CMDBUSERSETTINGS_Fill = function (CMDBUSERSETTINGSList, StrIDCMDBUSER) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    CMDBUSERSETTINGSList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDCMDBUSER == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDCMDBUSER.FieldName, " = " + UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.NAME_TABLE + "." + UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDCMDBUSER.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDCMDBUSER = " IN (" + StrIDCMDBUSER + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDCMDBUSER.FieldName, StrIDCMDBUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    try {
        var DS_CMDBUSERSETTINGS = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "CMDBUSERSETTINGS_GET", Param.ToBytes());
        ResErr = DS_CMDBUSERSETTINGS.ResErr;
        if (ResErr.NotError) {
            if (DS_CMDBUSERSETTINGS.DataSet.RecordCount > 0) {
                DS_CMDBUSERSETTINGS.DataSet.First();
                while (!(DS_CMDBUSERSETTINGS.DataSet.Eof)) {
                    var CMDBUSERSETTINGS = new Persistence.SMCI.Properties.TCMDBUSERSETTINGS();
                    CMDBUSERSETTINGS.IDCMDBUSERSETTINGS = DS_CMDBUSERSETTINGS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDCMDBUSERSETTINGS.FieldName).asInt32();
                    //Other
                    CMDBUSERSETTINGS.IDCMDBUSER = DS_CMDBUSERSETTINGS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDCMDBUSER.FieldName).asInt32();
                    CMDBUSERSETTINGS.IDLANGUAGE = SysCfg.App.Properties.TLanguage.GetEnum(DS_CMDBUSERSETTINGS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDLANGUAGE.FieldName).asInt32());
                    CMDBUSERSETTINGS.IDSKINATIS = DS_CMDBUSERSETTINGS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDSKINATIS.FieldName).asInt32();
                    CMDBUSERSETTINGS.IDSKINITHELPCENTER = DS_CMDBUSERSETTINGS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDSKINITHELPCENTER.FieldName).asInt32();
                    CMDBUSERSETTINGS.ATENLAYOUT_IDFILE_ATIS = DS_CMDBUSERSETTINGS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.ATENLAYOUT_IDFILE_ATIS.FieldName).asString();
                    CMDBUSERSETTINGS.CONSOLEDETAIL_COLUMN_ATIS = DS_CMDBUSERSETTINGS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.CONSOLEDETAIL_COLUMN_ATIS.FieldName).asInt32();
                    //Other
                    CMDBUSERSETTINGSList.push(CMDBUSERSETTINGS);
                    DS_CMDBUSERSETTINGS.DataSet.Next();
                }
            }
            else {
                DS_CMDBUSERSETTINGS.ResErr.NotError = false;
                DS_CMDBUSERSETTINGS.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_CMDBUSERSETTINGS.js Persistence.SMCI.Methods.CMDBUSERSETTINGS_Fill", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.SMCI.Methods_DataBase.CMDBUSERSETTINGS_GETID = function (CMDBUSERSETTINGS) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDCMDBUSER.FieldName, CMDBUSERSETTINGS.IDCMDBUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDLANGUAGE.FieldName, CMDBUSERSETTINGS.IDLANGUAGE.value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDSKINATIS.FieldName, CMDBUSERSETTINGS.IDSKINATIS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDSKINITHELPCENTER.FieldName, CMDBUSERSETTINGS.IDSKINITHELPCENTER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.ATENLAYOUT_IDFILE_ATIS.FieldName, CMDBUSERSETTINGS.ATENLAYOUT_IDFILE_ATIS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.CONSOLEDETAIL_COLUMN_ATIS.FieldName, CMDBUSERSETTINGS.CONSOLEDETAIL_COLUMN_ATIS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var DS_CMDBUSERSETTINGS = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "CMDBUSERSETTINGS_GETID", Param.ToBytes());
        ResErr = DS_CMDBUSERSETTINGS.ResErr;
        if (ResErr.NotError) {
            if (DS_CMDBUSERSETTINGS.DataSet.RecordCount > 0) {
                DS_CMDBUSERSETTINGS.DataSet.First();
                CMDBUSERSETTINGS.IDCMDBUSERSETTINGS = DS_CMDBUSERSETTINGS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDCMDBUSERSETTINGS.FieldName).asInt32();
            }
            else {
                DS_CMDBUSERSETTINGS.ResErr.NotError = false;
                DS_CMDBUSERSETTINGS.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_CMDBUSERSETTINGS.js Persistence.SMCI.Methods.CMDBUSERSETTINGS_GETID", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.SMCI.Methods_DataBase.CMDBUSERSETTINGS_ADD = function (CMDBUSERSETTINGS) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDCMDBUSER.FieldName, CMDBUSERSETTINGS.IDCMDBUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDLANGUAGE.FieldName, CMDBUSERSETTINGS.IDLANGUAGE.value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDSKINATIS.FieldName, CMDBUSERSETTINGS.IDSKINATIS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDSKINITHELPCENTER.FieldName, CMDBUSERSETTINGS.IDSKINITHELPCENTER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.ATENLAYOUT_IDFILE_ATIS.FieldName, CMDBUSERSETTINGS.ATENLAYOUT_IDFILE_ATIS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.CONSOLEDETAIL_COLUMN_ATIS.FieldName, CMDBUSERSETTINGS.CONSOLEDETAIL_COLUMN_ATIS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "CMDBUSERSETTINGS_ADD", Param.ToBytes());
        var ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.SMCI.Methods.CMDBUSERSETTINGS_GETID(CMDBUSERSETTINGS);
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_CMDBUSERSETTINGS.js Persistence.SMCI.Methods.CMDBUSERSETTINGS_ADD", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.SMCI.Methods_DataBase.CMDBUSERSETTINGS_UPD = function (CMDBUSERSETTINGS) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDCMDBUSERSETTINGS.FieldName, CMDBUSERSETTINGS.IDCMDBUSERSETTINGS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDCMDBUSER.FieldName, CMDBUSERSETTINGS.IDCMDBUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDLANGUAGE.FieldName, CMDBUSERSETTINGS.IDLANGUAGE.value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDSKINATIS.FieldName, CMDBUSERSETTINGS.IDSKINATIS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDSKINITHELPCENTER.FieldName, CMDBUSERSETTINGS.IDSKINITHELPCENTER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.ATENLAYOUT_IDFILE_ATIS.FieldName, CMDBUSERSETTINGS.ATENLAYOUT_IDFILE_ATIS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.CONSOLEDETAIL_COLUMN_ATIS.FieldName, CMDBUSERSETTINGS.CONSOLEDETAIL_COLUMN_ATIS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "CMDBUSERSETTINGS_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_CMDBUSERSETTINGS.js Persistence.SMCI.Methods.CMDBUSERSETTINGS_UPD", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.SMCI.Methods_DataBase.CMDBUSERSETTINGS_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.SMCI.Methods_DataBase.CMDBUSERSETTINGS_DELIDCMDBUSERSETTINGS(/*String StrIDCMDBUSERSETTINGS*/Param);
    }
    else {
        Res = Persistence.SMCI.Methods_DataBase.CMDBUSERSETTINGS_DELCMDBUSERSETTINGS(Param);
    }
    return (Res);
}

Persistence.SMCI.Methods_DataBase.CMDBUSERSETTINGS_DELIDCMDBUSERSETTINGS = function (/*String StrIDCMDBUSERSETTINGS*/IDCMDBUSERSETTINGS) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDCMDBUSERSETTINGS == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDCMDBUSERSETTINGS.FieldName, " = " + UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.NAME_TABLE + "." + UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDCMDBUSERSETTINGS.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDCMDBUSERSETTINGS = " IN (" + StrIDCMDBUSERSETTINGS + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDCMDBUSERSETTINGS.FieldName, StrIDCMDBUSERSETTINGS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDCMDBUSERSETTINGS.FieldName, IDCMDBUSERSETTINGS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   CMDBUSERSETTINGS_DEL_1   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"CMDBUSERSETTINGS_DEL_1", @"CMDBUSERSETTINGS_DEL_1 description.");
        UnSQL.SQL.Add(@" DELETE CMDBUSERSETTINGS  ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  IDCMDBUSER=@[IDCMDBUSER] AND  ");
        UnSQL.SQL.Add(@"  IDCMDBUSERSETTINGS=@[IDCMDBUSERSETTINGS] AND  ");
        UnSQL.SQL.Add(@"  IDLANGUAGE=@[IDLANGUAGE] AND  ");
        UnSQL.SQL.Add(@"  IDSKINATIS=@[IDSKINATIS] AND  ");
        UnSQL.SQL.Add(@"  ATENLAYOUT_IDFILE_ATIS=@[ATENLAYOUT_IDFILE_ATIS] AND  ");
        UnSQL.SQL.Add(@"  CONSOLEDETAIL_COLUMN_ATIS=@[CONSOLEDETAIL_COLUMN_ATIS] ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "CMDBUSERSETTINGS_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_CMDBUSERSETTINGS.js Persistence.SMCI.Methods.CMDBUSERSETTINGS_DEL", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.SMCI.Methods_DataBase.CMDBUSERSETTINGS_DELCMDBUSERSETTINGS = function (CMDBUSERSETTINGS) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDCMDBUSERSETTINGS.FieldName, CMDBUSERSETTINGS.IDCMDBUSERSETTINGS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDCMDBUSER.FieldName, CMDBUSERSETTINGS.IDCMDBUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDLANGUAGE.FieldName, CMDBUSERSETTINGS.IDLANGUAGE.value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDSKINATIS.FieldName, CMDBUSERSETTINGS.IDSKINATIS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDSKINITHELPCENTER.FieldName, CMDBUSERSETTINGS.IDSKINITHELPCENTER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.ATENLAYOUT_IDFILE_ATIS.FieldName, CMDBUSERSETTINGS.ATENLAYOUT_IDFILE_ATIS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.CONSOLEDETAIL_COLUMN_ATIS.FieldName, CMDBUSERSETTINGS.CONSOLEDETAIL_COLUMN_ATIS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   CMDBUSERSETTINGS_DEL_2   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"CMDBUSERSETTINGS_DEL_2", @"CMDBUSERSETTINGS_DEL_2 description.");
        UnSQL.SQL.Add(@" DELETE CMDBUSERSETTINGS  ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  IDCMDBUSER=@[IDCMDBUSER] AND  ");
        UnSQL.SQL.Add(@"  IDCMDBUSERSETTINGS=@[IDCMDBUSERSETTINGS] AND  ");
        UnSQL.SQL.Add(@"  IDLANGUAGE=@[IDLANGUAGE] AND  ");
        UnSQL.SQL.Add(@"  IDSKINATIS=@[IDSKINATIS] AND  ");
        UnSQL.SQL.Add(@"  IDSKINITHELPCENTER=@[IDSKINITHELPCENTER] AND  ");
        UnSQL.SQL.Add(@"  ATENLAYOUT_IDFILE_ATIS=@[ATENLAYOUT_IDFILE_ATIS] AND  ");
        UnSQL.SQL.Add(@"  CONSOLEDETAIL_COLUMN_ATIS=@[CONSOLEDETAIL_COLUMN_ATIS] ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "CMDBUSERSETTINGS_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_CMDBUSERSETTINGS.js Persistence.SMCI.Methods.CMDBUSERSETTINGS_DEL", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
//**********************   METHODS   ********************************************************************************************
Persistence.SMCI.Methods.CMDBUSERSETTINGS_ListSetID = function (CMDBUSERSETTINGSList, IDCMDBUSERSETTINGS) {
    for (i = 0; i < CMDBUSERSETTINGSList.length; i++) {
        if (IDCMDBUSERSETTINGS == CMDBUSERSETTINGSList[i].IDCMDBUSERSETTINGSR)
        return (CMDBUSERSETTINGSList[i]);
    }
    var UnCMDBUSERSETTINGS = new Persistence.SMCI.Properties.TCMDBUSERSETTINGS();
    UnCMDBUSERSETTINGS.IDCMDBUSERSETTINGS = IDCMDBUSERSETTINGS;
    return (UnCMDBUSERSETTINGS);
}
Persistence.SMCI.Methods.CMDBUSERSETTINGS_ListAdd = function (CMDBUSERSETTINGSList, CMDBUSERSETTINGS) {
    var i = Persistence.SMCI.Methods.CMDBUSERSETTINGS_ListGetIndex(CMDBUSERSETTINGSList, CMDBUSERSETTINGS);
    if (i == -1) CMDBUSERSETTINGSList.push(CMDBUSERSETTINGS);
    else {
        CMDBUSERSETTINGSList[i].IDCMDBUSER = CMDBUSERSETTINGS.IDCMDBUSER;
        CMDBUSERSETTINGSList[i].IDCMDBUSERSETTINGS = CMDBUSERSETTINGS.IDCMDBUSERSETTINGS;
        CMDBUSERSETTINGSList[i].IDLANGUAGE = CMDBUSERSETTINGS.IDLANGUAGE;
        CMDBUSERSETTINGSList[i].IDSKINATIS = CMDBUSERSETTINGS.IDSKINATIS;
        CMDBUSERSETTINGSList[i].IDSKINITHELPCENTER = CMDBUSERSETTINGS.IDSKINITHELPCENTER;
        CMDBUSERSETTINGSList[i].ATENLAYOUT_IDFILE_ATIS = CMDBUSERSETTINGS.ATENLAYOUT_IDFILE_ATIS;
        CMDBUSERSETTINGSList[i].CONSOLEDETAIL_COLUMN_ATIS = CMDBUSERSETTINGS.CONSOLEDETAIL_COLUMN_ATIS;

    }
}
//CJRC_26072018
Persistence.SMCI.Methods.CMDBUSERSETTINGS_ListGetIndex = function (CMDBUSERSETTINGSList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.SMCI.Methods.CMDBUSERSETTINGS_ListGetIndexIDCMDBUSERSETTINGS(CMDBUSERSETTINGSList, Param);

    }
    else {
        Res = Persistence.SMCI.Methods.CMDBUSERSETTINGS_ListGetIndexCMDBUSERSETTINGS(CMDBUSERSETTINGSList, Param);
    }
    return (Res);
}

Persistence.SMCI.Methods.CMDBUSERSETTINGS_ListGetIndexCMDBUSERSETTINGS = function (CMDBUSERSETTINGSList, CMDBUSERSETTINGS) {
    for (i = 0; i < CMDBUSERSETTINGSList.length; i++) {
        if (CMDBUSERSETTINGS.IDCMDBUSERSETTINGS == CMDBUSERSETTINGSList[i].IDCMDBUSERSETTINGS)
        return (i);
    }
    return (-1);
}
Persistence.SMCI.Methods.CMDBUSERSETTINGS_ListGetIndexIDCMDBUSERSETTINGS = function (CMDBUSERSETTINGSList, IDCMDBUSERSETTINGS) {
    for (i = 0; i < CMDBUSERSETTINGSList.length; i++) {
        if (IDCMDBUSERSETTINGS == CMDBUSERSETTINGSList[i].IDCMDBUSERSETTINGS)
        return (i);
    }
    return (-1);
}
//String Function 
Persistence.SMCI.Methods.CMDBUSERSETTINGSListtoStr = function (CMDBUSERSETTINGSList) {
    var Res = Persistence.SMCI.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(CMDBUSERSETTINGSList.length, Longitud, Texto);
        for (Counter = 0; Counter < CMDBUSERSETTINGSList.length; Counter++) {
            var UnCMDBUSERSETTINGS = CMDBUSERSETTINGSList[Counter];
            UnCMDBUSERSETTINGS.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.SMCI.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
}
Persistence.SMCI.Methods.StrtoCMDBUSERSETTINGS = function (ProtocoloStr, CMDBUSERSETTINGSList) {
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        CMDBUSERSETTINGSList.length = 0;
        if (Persistence.SMCI.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnCMDBUSERSETTINGS = new Persistence.Demo.Properties.TCMDBUSERSETTINGS(); //Mode new row
                UnCMDBUSERSETTINGS.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.CMDBUSERSETTINGS_ListAdd(CMDBUSERSETTINGSList, UnCMDBUSERSETTINGS);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
   
}
//Socket Function 
Persistence.SMCI.Methods.CMDBUSERSETTINGSListToByte = function (CMDBUSERSETTINGSList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, CMDBUSERSETTINGSList.length - idx);
    for (i = idx; i < CMDBUSERSETTINGSList.length; i++) {
        CMDBUSERSETTINGSList[i].ToByte(MemStream);
    }
}
Persistence.SMCI.Methods.ByteToCMDBUSERSETTINGSList = function (CMDBUSERSETTINGSList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnCMDBUSERSETTINGS = new Persistence.SMCI.Properties.TCMDBUSERSETTINGS();
        UnCMDBUSERSETTINGS.ByteTo(MemStream);
        Persistence.SMCI.Methods.CMDBUSERSETTINGS_ListAdd(CMDBUSERSETTINGSList, UnCMDBUSERSETTINGS);
    }
}
Persistence.SMCI.Methods.CMDBUSERSETTINGS_Fill = function (CMDBUSERSETTINGSList, StrIDCMDBUSER) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    switch (Persistence.SMCI.Properties.PERSISTENCE_MODE) {
        case Persistence.TPERSISTENCE_MODE._Local:
            ResErr = Persistence.SMCI.Methods_DataBase.CMDBUSERSETTINGS_Fill(CMDBUSERSETTINGSList, StrIDCMDBUSER/*IDCMDBUSERSETTINGS*/);
            break;
        case Persistence.TPERSISTENCE_MODE._Server_DataBase:
            ResErr = Persistence.SMCI.Methods_Cmd.CMDBUSERSETTINGS_Fill(CMDBUSERSETTINGSList, StrIDCMDBUSER/*IDCMDBUSERSETTINGS*/, Comunic.Properties.TPersistenceSource._DataBase);
            break;
        case Persistence.TPERSISTENCE_MODE._Server_Memory:
            ResErr = Persistence.SMCI.Methods_Cmd.CMDBUSERSETTINGS_Fill(CMDBUSERSETTINGSList, StrIDCMDBUSER/*IDCMDBUSERSETTINGS*/, Comunic.Properties.TPersistenceSource._Memmory);
            break;
    }
    return ResErr;
}
Persistence.SMCI.Methods.CMDBUSERSETTINGS_GETID = function (CMDBUSERSETTINGS) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    switch (Persistence.SMCI.Properties.PERSISTENCE_MODE) {
        case Persistence.TPERSISTENCE_MODE._Local:
            ResErr = Methods_DataBase.CMDBUSERSETTINGS_GETID(CMDBUSERSETTINGS);
            break;
        case Persistence.TPERSISTENCE_MODE._Server_DataBase:
            ResErr = Methods_Cmd.CMDBUSERSETTINGS_GETID(CMDBUSERSETTINGS, Comunic.Properties.TPersistenceSource._DataBase);
            break;
        case Persistence.TPERSISTENCE_MODE._Server_Memory:
            ResErr = Methods_Cmd.CMDBUSERSETTINGS_GETID(CMDBUSERSETTINGS, Comunic.Properties.TPersistenceSource._Memmory);
            break;
    }
    return ResErr;
}
Persistence.SMCI.Methods.CMDBUSERSETTINGS_ADD = function (CMDBUSERSETTINGS) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    switch (Persistence.SMCI.Properties.PERSISTENCE_MODE) {
        case Persistence.TPERSISTENCE_MODE._Local:
            ResErr = Methods_DataBase.CMDBUSERSETTINGS_ADD(CMDBUSERSETTINGS);
            break;
        case Persistence.TPERSISTENCE_MODE._Server_DataBase:
            ResErr = Methods_Cmd.CMDBUSERSETTINGS_ADD(CMDBUSERSETTINGS, Comunic.Properties.TPersistenceSource._DataBase);
            break;
        case Persistence.TPERSISTENCE_MODE._Server_Memory:
            ResErr = Methods_Cmd.CMDBUSERSETTINGS_ADD(CMDBUSERSETTINGS, Comunic.Properties.TPersistenceSource._Memmory);
            break;
    }
    return ResErr;
}
Persistence.SMCI.Methods.CMDBUSERSETTINGS_UPD = function (CMDBUSERSETTINGS) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    switch (Persistence.SMCI.Properties.PERSISTENCE_MODE) {
        case Persistence.TPERSISTENCE_MODE._Local:
            ResErr = Persistence.SMCI.Methods_DataBase.CMDBUSERSETTINGS_UPD(CMDBUSERSETTINGS);
            break;
        case Persistence.Persistence.TPERSISTENCE_MODE._Server_DataBase:
            ResErr = Persistence.SMCI.Methods_Cmd.CMDBUSERSETTINGS_UPD(CMDBUSERSETTINGS, Comunic.Properties.TPersistenceSource._DataBase);
            break;
        case Persistence.TPERSISTENCE_MODE._Server_Memory:
            ResErr = Persistence.SMCI.Methods_Cmd.CMDBUSERSETTINGS_UPD(CMDBUSERSETTINGS, Comunic.Properties.TPersistenceSource._Memmory);
            break;
    }
    return ResErr;
}

//CJRC_27072018
Persistence.SMCI.Methods.CMDBUSERSETTINGS_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.SMCI.Methods.CMDBUSERSETTINGS_DELIDCMDBUSERSETTINGS(Param);
    }
    else {
        Res = Persistence.SMCI.Methods.CMDBUSERSETTINGS_DELCMDBUSERSETTINGS(Param);
    }
    return (Res);
}

Persistence.SMCI.Methods.CMDBUSERSETTINGS_DELIDCMDBUSERSETTINGS = function (IDCMDBUSERSETTINGS) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    switch (Persistence.SMCI.Properties.PERSISTENCE_MODE) {
        case Persistence.TPERSISTENCE_MODE._Local:
            ResErr = Methods_DataBase.CMDBUSERSETTINGS_DEL(IDCMDBUSERSETTINGS);
            break;
        case Persistence.TPERSISTENCE_MODE._Server_DataBase:
            ResErr = Methods_Cmd.CMDBUSERSETTINGS_DEL(IDCMDBUSERSETTINGS, Comunic.Properties.TPersistenceSource._DataBase);
            break;
        case Persistence.TPERSISTENCE_MODE._Server_Memory:
            ResErr = Methods_Cmd.CMDBUSERSETTINGS_DEL(IDCMDBUSERSETTINGS, Comunic.Properties.TPersistenceSource._Memmory);
            break;
    }
    return ResErr;
}
Persistence.SMCI.Methods.CMDBUSERSETTINGS_DELCMDBUSERSETTINGS = function (CMDBUSERSETTINGS) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    switch (Persistence.SMCI.Properties.PERSISTENCE_MODE) {
        case Persistence.TPERSISTENCE_MODE._Local:
            ResErr = Methods_DataBase.CMDBUSERSETTINGS_DEL(CMDBUSERSETTINGS);
            break;
        case Persistence.TPERSISTENCE_MODE._Server_DataBase:
            ResErr = Methods_Cmd.CMDBUSERSETTINGS_DEL(CMDBUSERSETTINGS, Comunic.Properties.TPersistenceSource._DataBase);
            break;
        case Persistence.TPERSISTENCE_MODE._Server_Memory:
            ResErr = Methods_Cmd.CMDBUSERSETTINGS_DEL(CMDBUSERSETTINGS, Comunic.Properties.TPersistenceSource._Memmory);
            break;
    }
    return ResErr;
}
Persistence.SMCI.Methods_Cmd.CMDBUSERSETTINGS_Fill = function (CMDBUSERSETTINGSList, StrIDCMDBUSER, PersistenceSource) {
    var  PersistenceProfiler = new Persistence.Methods.TPersistenceProfiler();
    PersistenceProfiler.PersistenceSource = PersistenceSource;
    PersistenceProfiler.PersistenceType = Comunic.Properties.TPersistenceType._CMDBUSERSETTINGS;
    PersistenceProfiler.PersistenceCmd = Comunic.Properties.TPersistenceCmd._Fill;
    Persistence.SMCI.Methods.CMDBUSERSETTINGSListToByte(CMDBUSERSETTINGSList,PersistenceProfiler.MemStrm_Request, 0);
    SysCfg.Stream.Methods.WriteStreamStrInt16(PersistenceProfiler.MemStrm_Request, StrIDCMDBUSER);
    PersistenceProfiler.GetPersistence();
    Persistence.SMCI.Methods.ByteToCMDBUSERSETTINGSList(CMDBUSERSETTINGSList,PersistenceProfiler.MemStrm_Response);
    var ResErr = new SysCfg.Error.Properties.TResErr();
    SysCfg.Error.Properties.ResErrfill(ResErr);
    return (ResErr);
}
Persistence.SMCI.Methods_Cmd.CMDBUSERSETTINGS_GETID = function (CMDBUSERSETTINGS, PersistenceSource) {
    var PersistenceProfiler = new Persistence.Methods.TPersistenceProfiler();
    PersistenceProfiler.PersistenceSource = PersistenceSource;
    PersistenceProfiler.PersistenceType = Comunic.Properties.TPersistenceType._CMDBUSERSETTINGS;
    PersistenceProfiler.PersistenceCmd = Comunic.Properties.TPersistenceCmd._GETID;
    CMDBUSERSETTINGS.ToByte(PersistenceProfiler.MemStrm_Request);
    PersistenceProfiler.GetPersistence();
    CMDBUSERSETTINGS.ByteTo(PersistenceProfiler.MemStrm_Request);
    var ResErr = new SysCfg.Error.Properties.TResErr();
    SysCfg.Error.Properties.ResErrfill(ResErr);
    return (ResErr);
}
Persistence.SMCI.Methods_Cmd.CMDBUSERSETTINGS_ADD = function (CMDBUSERSETTINGS, PersistenceSource) {
    var PersistenceProfiler = new Persistence.Methods.TPersistenceProfiler();
    PersistenceProfiler.PersistenceSource = PersistenceSource;
    PersistenceProfiler.PersistenceType = Comunic.Properties.TPersistenceType._CMDBUSERSETTINGS;
    PersistenceProfiler.PersistenceCmd = Comunic.Properties.TPersistenceCmd._ADD;
    CMDBUSERSETTINGS.ToByte(PersistenceProfiler.MemStrm_Request);
    PersistenceProfiler.GetPersistence();
    CMDBUSERSETTINGS.ByteTo(PersistenceProfiler.MemStrm_Request);
    var ResErr = new SysCfg.Error.Properties.TResErr();
    SysCfg.Error.Properties.ResErrfill(ResErr);
    return (ResErr);
}
Persistence.SMCI.Methods_Cmd.CMDBUSERSETTINGS_UPD = function (CMDBUSERSETTINGS, PersistenceSource) {
    var PersistenceProfiler = new Persistence.Methods.TPersistenceProfiler();
    PersistenceProfiler.PersistenceSource = PersistenceSource;
    PersistenceProfiler.PersistenceType = Comunic.Properties.TPersistenceType._CMDBUSERSETTINGS;
    PersistenceProfiler.PersistenceCmd = Comunic.Properties.TPersistenceCmd._UPD;
    CMDBUSERSETTINGS.ToByte(PersistenceProfiler.MemStrm_Request);
    PersistenceProfiler.GetPersistence();
    CMDBUSERSETTINGS.ByteTo(PersistenceProfiler.MemStrm_Request);
    var ResErr = new SysCfg.Error.Properties.TResErr();
    SysCfg.Error.Properties.ResErrfill(ResErr);
    return (ResErr);
}

//CJRC_27072018
Persistence.SMCI.Methods_Cmd.CMDBUSERSETTINGS_DEL = function (Param1, Param2) {
    var Res = -1
    if ((typeof (Param1) == 'number') || (typeof (Param1) == 'string')) {
        Res = Persistence.SMCI.Methods_Cmd.CMDBUSERSETTINGS_DELIDCMDBUSERSETTINGS(Param1, Param2);
    }
    else {
        Res = Persistence.SMCI.Methods_Cmd.CMDBUSERSETTINGS_DELCMDBUSERSETTINGS(Param1, Param2);
    }
    return (Res);
}

Persistence.SMCI.Methods_Cmd.CMDBUSERSETTINGS_DELIDCMDBUSERSETTINGS = function (IDCMDBUSERSETTINGS, PersistenceSource) {
    var PersistenceProfiler = new Persistence.Methods.TPersistenceProfiler();
    PersistenceProfiler.PersistenceSource = PersistenceSource;
    PersistenceProfiler.PersistenceType = Comunic.Properties.TPersistenceType._CMDBUSERSETTINGS;
    PersistenceProfiler.PersistenceCmd = Comunic.Properties.TPersistenceCmd._DEL1;
    SysCfg.Stream.Methods.WriteStreamInt32(PersistenceProfiler.MemStrm_Request, IDCMDBUSERSETTINGS);
    PersistenceProfiler.GetPersistence();
    IDCMDBUSERSETTINGS = SysCfg.Stream.Methods.ReadStreamInt32(PersistenceProfiler.MemStrm_Request);
    var ResErr = new SysCfg.Error.Properties.TResErr();
    SysCfg.Error.Properties.ResErrfill(ResErr);
    return (ResErr);
}
Persistence.SMCI.Methods_Cmd.CMDBUSERSETTINGS_DELCMDBUSERSETTINGS = function (CMDBUSERSETTINGS, PersistenceSource) {
    var PersistenceProfiler = new Persistence.Methods.TPersistenceProfiler();
    PersistenceProfiler.PersistenceSource = PersistenceSource;
    PersistenceProfiler.PersistenceType = Comunic.Properties.TPersistenceType._CMDBUSERSETTINGS;
    PersistenceProfiler.PersistenceCmd = Comunic.Properties.TPersistenceCmd._DEL2;
    CMDBUSERSETTINGS.ToByte(PersistenceProfiler.MemStrm_Request);
    PersistenceProfiler.GetPersistence();
    CMDBUSERSETTINGS.ByteTo(PersistenceProfiler.MemStrm_Request);
    var ResErr = new SysCfg.Error.Properties.TResErr();
    SysCfg.Error.Properties.ResErrfill(ResErr);
    return (ResErr);
}
Persistence.SMCI.Methods_Cmd.SetPersistenceCmd = function (GetPersistence, MemStrm_Request, MemStrm_Response, ResErr) {
    var CMDBUSERSETTINGSList = new Array();
    var CMDBUSERSETTINGS = new Properties.TCMDBUSERSETTINGS();
    switch (GetPersistence.PersistenceCmd) {
        case Comunic.Properties.TPersistenceCmd._None:
            break;
        case Comunic.Properties.TPersistenceCmd._Fill:
            Persistence.SMCI.Methods.ByteToCMDBUSERSETTINGSList(CMDBUSERSETTINGSList,MemStrm_Request);
            var StrIDCMDBUSER = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStrm_Request);
            if (GetPersistence.PersistenceSource == Comunic.Properties.TPersistenceSource._DataBase) ResErr = Persistence.SMCI.Methods_DataBase.CMDBUSERSETTINGS_Fill(CMDBUSERSETTINGSList, StrIDCMDBUSER);
            if (GetPersistence.PersistenceSource == Comunic.Properties.TPersistenceSource._Memmory) ResErr = Persistence.SMCI.Methods_DataBase.CMDBUSERSETTINGS_Fill(CMDBUSERSETTINGSList, StrIDCMDBUSER);
            Persistence.SMCI.Methods.CMDBUSERSETTINGSListToByte(CMDBUSERSETTINGSList,MemStrm_Response, 0);
            break;
        case Comunic.Properties.TPersistenceCmd._GETID:
            CMDBUSERSETTINGS.ByteTo(MemStrm_Request);
            ResErr = Persistence.SMCI.Methods_DataBase.CMDBUSERSETTINGS_GETID(CMDBUSERSETTINGS);
            CMDBUSERSETTINGS.ToByte(MemStrm_Response);
            break;
        case Comunic.Properties.TPersistenceCmd._ADD:
            CMDBUSERSETTINGS.ByteTo(MemStrm_Request);
            ResErr = Persistence.SMCI.Methods_DataBase.CMDBUSERSETTINGS_ADD(CMDBUSERSETTINGS);
            CMDBUSERSETTINGS.ToByte(MemStrm_Response);
            break;
        case Comunic.Properties.TPersistenceCmd._UPD:
            CMDBUSERSETTINGS.ByteTo(MemStrm_Request);
            ResErr = Persistence.SMCI.Methods_DataBase.CMDBUSERSETTINGS_UPD(CMDBUSERSETTINGS);
            CMDBUSERSETTINGS.ToByte(MemStrm_Response);
            break;
        case Comunic.Properties.TPersistenceCmd._DEL1:
            Persistence.SMCI.Methods.ByteToCMDBUSERSETTINGSList(CMDBUSERSETTINGSList,MemStrm_Request);
            var IDCMDBUSERSETTINGS = SysCfg.Stream.Methods.ReadStreamInt32(MemStrm_Request);
            ResErr = Persistence.SMCI.Methods_DataBase.CMDBUSERSETTINGS_DEL(IDCMDBUSERSETTINGS);
            SysCfg.Stream.Methods.WriteStreamInt32(MemStrm_Response, IDCMDBUSERSETTINGS);
            break;
        case Comunic.Properties.TPersistenceCmd._DEL2:
            CMDBUSERSETTINGS.ByteTo(MemStrm_Request);
            ResErr = Persistence.SMCI.Methods_DataBase.CMDBUSERSETTINGS_UPD(CMDBUSERSETTINGS);
            CMDBUSERSETTINGS.ToByte(MemStrm_Response);
            break;
        default:
            break;
    }
}

