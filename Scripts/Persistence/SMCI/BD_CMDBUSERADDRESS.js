﻿//**********************   PROPERTIES   *****************************************************************************************
Persistence.SMCI.Properties.TCMDBUSERADDRESS = function () {
    this.ADDRESS1 = "";
    this.ADDRESS2 = 0
    this.ADDRESS3 = "";
    this.ADDRESS4 = "";
    this.ADDRESS5 = "";
    this.ADDRESS6 = "";
    this.IDCMDBUSER = 0;
    this.IDCMDBUSERADDRESS = 0;
    this.IDSYSTEMSTATUS = 0;
    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.ADDRESS1);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.ADDRESS2);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.ADDRESS3);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.ADDRESS4);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.ADDRESS5);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.ADDRESS6);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDCMDBUSER);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDCMDBUSERADDRESS);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSYSTEMSTATUS);
    }
    this.ByteTo = function (MemStream) {
        this.ADDRESS1 = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.ADDRESS2 = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.ADDRESS3 = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.ADDRESS4 = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.ADDRESS5 = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.ADDRESS6 = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDCMDBUSER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDCMDBUSERADDRESS = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSYSTEMSTATUS = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteStr(this.ADDRESS1, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.ADDRESS2, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.ADDRESS3, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.ADDRESS4, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.ADDRESS5, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.ADDRESS6, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDCMDBUSER, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDCMDBUSERADDRESS, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDSYSTEMSTATUS, Longitud, Texto);
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.ADDRESS1 = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.ADDRESS2 = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.ADDRESS3 = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.ADDRESS4 = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.ADDRESS5 = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.ADDRESS6 = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.IDCMDBUSER = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDCMDBUSERADDRESS = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDSYSTEMSTATUS = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
    }
}
//**********************   METHODS server  ********************************************************************************************
Persistence.SMCI.Methods.CMDBUSERADDRESS_Fill = function (CMDBUSERADDRESSList, StrIDCMDBUSER) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    CMDBUSERADDRESSList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDCMDBUSER == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.CMDBUSER.IDCMDBUSER.FieldName, " = " + UsrCfg.InternoAtisNames.CMDBUSERADDRESS.NAME_TABLE + "." + UsrCfg.InternoAtisNames.CMDBUSER.IDCMDBUSER.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDCMDBUSER = " IN (" + StrIDCMDBUSER + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.CMDBUSER.IDCMDBUSER.FieldName, StrIDCMDBUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    try {
        var DS_CMDBUSERADDRESS = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "CMDBUSERADDRESS_GET", Param.ToBytes());
        ResErr = DS_CMDBUSERADDRESS.ResErr;
        if (ResErr.NotError) {
            if (DS_CMDBUSERADDRESS.DataSet.RecordCount > 0) {
                DS_CMDBUSERADDRESS.DataSet.First();
                while (!(DS_CMDBUSERADDRESS.DataSet.Eof)) {
                    var CMDBUSERADDRESS = new Persistence.SMCI.Properties.TCMDBUSERADDRESS();
                    CMDBUSERADDRESS.IDCMDBUSERADDRESS = DS_CMDBUSERADDRESS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.IDCMDBUSERADDRESS.FieldName).asInt32();
                    //Other
                    CMDBUSERADDRESS.ADDRESS1 = DS_CMDBUSERADDRESS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS1.FieldName).asString();
                    CMDBUSERADDRESS.ADDRESS2 = DS_CMDBUSERADDRESS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS2.FieldName).asInt32();
                    CMDBUSERADDRESS.ADDRESS3 = DS_CMDBUSERADDRESS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS3.FieldName).asString();
                    CMDBUSERADDRESS.ADDRESS4 = DS_CMDBUSERADDRESS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS4.FieldName).asString();
                    CMDBUSERADDRESS.ADDRESS5 = DS_CMDBUSERADDRESS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS5.FieldName).asString();
                    CMDBUSERADDRESS.ADDRESS6 = DS_CMDBUSERADDRESS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS6.FieldName).asString();
                    CMDBUSERADDRESS.IDCMDBUSER = DS_CMDBUSERADDRESS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.IDCMDBUSER.FieldName).asInt32();
                    CMDBUSERADDRESS.IDSYSTEMSTATUS = DS_CMDBUSERADDRESS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.IDSYSTEMSTATUS.FieldName).asInt32();
                    //Other
                    CMDBUSERADDRESSList.push(CMDBUSERADDRESS);
                    DS_CMDBUSERADDRESS.DataSet.Next();
                }
            }
            else {
                DS_CMDBUSERADDRESS.ResErr.NotError = false;
                DS_CMDBUSERADDRESS.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_CMDBUSERADDRESS.js Persistence.SMCI.Methods.CMDBUSERADDRESS_Fill", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.SMCI.Methods.CMDBUSERADDRESS_GETID = function (CMDBUSERADDRESS) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.IDCMDBUSERADDRESS.FieldName, CMDBUSERADDRESS.IDCMDBUSERADDRESS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS1.FieldName, CMDBUSERADDRESS.ADDRESS1, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS2.FieldName, CMDBUSERADDRESS.ADDRESS2, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS3.FieldName, CMDBUSERADDRESS.ADDRESS3, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS4.FieldName, CMDBUSERADDRESS.ADDRESS4, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS5.FieldName, CMDBUSERADDRESS.ADDRESS5, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS6.FieldName, CMDBUSERADDRESS.ADDRESS6, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.IDCMDBUSER.FieldName, CMDBUSERADDRESS.IDCMDBUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.IDSYSTEMSTATUS.FieldName, CMDBUSERADDRESS.IDSYSTEMSTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   CMDBUSERADDRESS_GETID   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"CMDBUSERADDRESS_GETID", @"CMDBUSERADDRESS_GETID description.");
        UnSQL.SQL.Add(@" SELECT  ");
        UnSQL.SQL.Add(@"  CMDBUSERADDRESS.ADDRESS1, ");
        UnSQL.SQL.Add(@"  CMDBUSERADDRESS.ADDRESS2, ");
        UnSQL.SQL.Add(@"  CMDBUSERADDRESS.ADDRESS3, ");
        UnSQL.SQL.Add(@"  CMDBUSERADDRESS.ADDRESS4, ");
        UnSQL.SQL.Add(@"  CMDBUSERADDRESS.ADDRESS5, ");
        UnSQL.SQL.Add(@"  CMDBUSERADDRESS.ADDRESS6, ");
        UnSQL.SQL.Add(@"  CMDBUSERADDRESS.IDCMDBUSER, ");
        UnSQL.SQL.Add(@"  CMDBUSERADDRESS.IDCMDBUSERADDRESS, ");
        UnSQL.SQL.Add(@"  CMDBUSERADDRESS.IDSYSTEMSTATUS ");
        UnSQL.SQL.Add(@" FROM  CMDBUSERADDRESS ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  CMDBUSERADDRESS.ADDRESS1=@[ADDRESS1] and  ");
        UnSQL.SQL.Add(@"  CMDBUSERADDRESS.ADDRESS2=@[ADDRESS2] and  ");
        UnSQL.SQL.Add(@"  CMDBUSERADDRESS.ADDRESS3=@[ADDRESS3] and  ");
        UnSQL.SQL.Add(@"  CMDBUSERADDRESS.ADDRESS4=@[ADDRESS4] and  ");
        UnSQL.SQL.Add(@"  CMDBUSERADDRESS.ADDRESS5=@[ADDRESS5] and  ");
        UnSQL.SQL.Add(@"  CMDBUSERADDRESS.ADDRESS6=@[ADDRESS6] and  ");
        UnSQL.SQL.Add(@"  CMDBUSERADDRESS.IDCMDBUSER=@[IDCMDBUSER] and  ");
        UnSQL.SQL.Add(@"  CMDBUSERADDRESS.IDCMDBUSERADDRESS=@[IDCMDBUSERADDRESS] and  ");
        UnSQL.SQL.Add(@"  CMDBUSERADDRESS.IDSYSTEMSTATUS=@[IDSYSTEMSTATUS] ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var DS_CMDBUSERADDRESS = SysCfg.DB.SQL.Methods.OpenDataSet("DataLink", "CMDBUSERADDRESS_GETID", Param.ToBytes());
        ResErr = DS_CMDBUSERADDRESS.ResErr;
        if (ResErr.NotError) {
            if (DS_CMDBUSERADDRESS.DataSet.RecordCount > 0) {
                DS_CMDBUSERADDRESS.DataSet.First();
                CMDBUSERADDRESS.IDCMDBUSERADDRESS = DS_CMDBUSERADDRESS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.IDCMDBUSERADDRESS.FieldName).asInt32();
                //Other
                CMDBUSERADDRESS.ADDRESS1 = DS_CMDBUSERADDRESS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS1.FieldName).asString();
                CMDBUSERADDRESS.ADDRESS2 = DS_CMDBUSERADDRESS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS2.FieldName).asInt32();
                CMDBUSERADDRESS.ADDRESS3 = DS_CMDBUSERADDRESS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS3.FieldName).asString();
                CMDBUSERADDRESS.ADDRESS4 = DS_CMDBUSERADDRESS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS4.FieldName).asString();
                CMDBUSERADDRESS.ADDRESS5 = DS_CMDBUSERADDRESS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS5.FieldName).asString();
                CMDBUSERADDRESS.ADDRESS6 = DS_CMDBUSERADDRESS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS6.FieldName).asString();
                CMDBUSERADDRESS.IDCMDBUSER = DS_CMDBUSERADDRESS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.IDCMDBUSER.FieldName).asInt32();
                CMDBUSERADDRESS.IDSYSTEMSTATUS = DS_CMDBUSERADDRESS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.IDSYSTEMSTATUS.FieldName).asInt32();
            }
            else {
                DS_CMDBUSERADDRESS.ResErr.NotError = false;
                DS_CMDBUSERADDRESS.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_CMDBUSERADDRESS.js Persistence.SMCI.Methods.CMDBUSERADDRESS_GETID", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.SMCI.Methods.CMDBUSERADDRESS_ADD = function (CMDBUSERADDRESS) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.IDCMDBUSERADDRESS.FieldName, CMDBUSERADDRESS.IDCMDBUSERADDRESS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS1.FieldName, CMDBUSERADDRESS.ADDRESS1, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS2.FieldName, CMDBUSERADDRESS.ADDRESS2, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS3.FieldName, CMDBUSERADDRESS.ADDRESS3, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS4.FieldName, CMDBUSERADDRESS.ADDRESS4, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS5.FieldName, CMDBUSERADDRESS.ADDRESS5, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS6.FieldName, CMDBUSERADDRESS.ADDRESS6, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.IDCMDBUSER.FieldName, CMDBUSERADDRESS.IDCMDBUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.IDSYSTEMSTATUS.FieldName, CMDBUSERADDRESS.IDSYSTEMSTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   CMDBUSERADDRESS_ADD   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"CMDBUSERADDRESS_ADD", @"CMDBUSERADDRESS_ADD description.");
        UnSQL.SQL.Add(@" INSERT INTO CMDBUSERADDRESS( ");
        UnSQL.SQL.Add(@"  ADDRESS1,  ");
        UnSQL.SQL.Add(@"  ADDRESS2,  ");
        UnSQL.SQL.Add(@"  ADDRESS3,  ");
        UnSQL.SQL.Add(@"  ADDRESS4,  ");
        UnSQL.SQL.Add(@"  ADDRESS5,  ");
        UnSQL.SQL.Add(@"  ADDRESS6,  ");
        UnSQL.SQL.Add(@"  IDCMDBUSER,  ");
        UnSQL.SQL.Add(@"  IDCMDBUSERADDRESS,  ");
        UnSQL.SQL.Add(@"  IDSYSTEMSTATUS ");
        UnSQL.SQL.Add(@" )  ");
        UnSQL.SQL.Add(@" VALUES ( ");
        UnSQL.SQL.Add(@"  @[ADDRESS1], ");
        UnSQL.SQL.Add(@"  @[ADDRESS2], ");
        UnSQL.SQL.Add(@"  @[ADDRESS3], ");
        UnSQL.SQL.Add(@"  @[ADDRESS4], ");
        UnSQL.SQL.Add(@"  @[ADDRESS5], ");
        UnSQL.SQL.Add(@"  @[ADDRESS6], ");
        UnSQL.SQL.Add(@"  @[IDCMDBUSER], ");
        UnSQL.SQL.Add(@"  @[IDCMDBUSERADDRESS], ");
        UnSQL.SQL.Add(@"  @[IDSYSTEMSTATUS] ");
        UnSQL.SQL.Add(@" ) ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("DataLink", "CMDBUSERADDRESS_ADD", Param.ToBytes());
        var ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.SMCI.Methods.CMDBUSERADDRESS_GETID(CMDBUSERADDRESS);
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_CMDBUSERADDRESS.js Persistence.SMCI.Methods.CMDBUSERADDRESS_ADD", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.SMCI.Methods.CMDBUSERADDRESS_UPD = function (CMDBUSERADDRESS) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.IDCMDBUSERADDRESS.FieldName, CMDBUSERADDRESS.IDCMDBUSERADDRESS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS1.FieldName, CMDBUSERADDRESS.ADDRESS1, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS2.FieldName, CMDBUSERADDRESS.ADDRESS2, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS3.FieldName, CMDBUSERADDRESS.ADDRESS3, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS4.FieldName, CMDBUSERADDRESS.ADDRESS4, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS5.FieldName, CMDBUSERADDRESS.ADDRESS5, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS6.FieldName, CMDBUSERADDRESS.ADDRESS6, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.IDCMDBUSER.FieldName, CMDBUSERADDRESS.IDCMDBUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.IDSYSTEMSTATUS.FieldName, CMDBUSERADDRESS.IDSYSTEMSTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   CMDBUSERADDRESS_UPD   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"CMDBUSERADDRESS_UPD", @"CMDBUSERADDRESS_UPD description.");
        UnSQL.SQL.Add(@" UPDATE CMDBUSERADDRESS SET ");
        UnSQL.SQL.Add(@"  ADDRESS1=@[ADDRESS1],  ");
        UnSQL.SQL.Add(@"  ADDRESS2=@[ADDRESS2],  ");
        UnSQL.SQL.Add(@"  ADDRESS3=@[ADDRESS3],  ");
        UnSQL.SQL.Add(@"  ADDRESS4=@[ADDRESS4],  ");
        UnSQL.SQL.Add(@"  ADDRESS5=@[ADDRESS5],  ");
        UnSQL.SQL.Add(@"  ADDRESS6=@[ADDRESS6],  ");
        UnSQL.SQL.Add(@"  IDCMDBUSER=@[IDCMDBUSER],  ");
        UnSQL.SQL.Add(@"  IDCMDBUSERADDRESS=@[IDCMDBUSERADDRESS],  ");
        UnSQL.SQL.Add(@"  IDSYSTEMSTATUS=@[IDSYSTEMSTATUS] ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  ADDRESS1=@[ADDRESS1] AND  ");
        UnSQL.SQL.Add(@"  ADDRESS2=@[ADDRESS2] AND  ");
        UnSQL.SQL.Add(@"  ADDRESS3=@[ADDRESS3] AND  ");
        UnSQL.SQL.Add(@"  ADDRESS4=@[ADDRESS4] AND  ");
        UnSQL.SQL.Add(@"  ADDRESS5=@[ADDRESS5] AND  ");
        UnSQL.SQL.Add(@"  ADDRESS6=@[ADDRESS6] AND  ");
        UnSQL.SQL.Add(@"  IDCMDBUSER=@[IDCMDBUSER] AND  ");
        UnSQL.SQL.Add(@"  IDCMDBUSERADDRESS=@[IDCMDBUSERADDRESS] AND  ");
        UnSQL.SQL.Add(@"  IDSYSTEMSTATUS=@[IDSYSTEMSTATUS] ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("DataLink", "CMDBUSERADDRESS_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_CMDBUSERADDRESS.js Persistence.SMCI.Methods.CMDBUSERADDRESS_UPD", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.SMCI.Methods.CMDBUSERADDRESS_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.SMCI.Methods.CMDBUSERADDRESS_DELIDCMDBUSERADDRESS(/*String StrIDCMDBUSERADDRESS*/Param);
    }
    else {
        Res = Persistence.SMCI.Methods.CMDBUSERADDRESS_DELCMDBUSERADDRESS(Param);
    }
    return (Res);
}

Persistence.SMCI.Methods.CMDBUSERADDRESS_DELIDCMDBUSERADDRESS = function (/*String StrIDCMDBUSERADDRESS*/IDCMDBUSERADDRESS) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDCMDBUSERADDRESS == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.IDCMDBUSERADDRESS.FieldName, " = " + UsrCfg.InternoAtisNames.CMDBUSERADDRESS.NAME_TABLE + "." + UsrCfg.InternoAtisNames.CMDBUSERADDRESS.IDCMDBUSERADDRESS.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDCMDBUSERADDRESS = " IN (" + StrIDCMDBUSERADDRESS + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.IDCMDBUSERADDRESS.FieldName, StrIDCMDBUSERADDRESS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.IDCMDBUSERADDRESS.FieldName, IDCMDBUSERADDRESS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   CMDBUSERADDRESS_DEL_1   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"CMDBUSERADDRESS_DEL_1", @"CMDBUSERADDRESS_DEL_1 description.");
        UnSQL.SQL.Add(@" DELETE CMDBUSERADDRESS  ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  ADDRESS1=@[ADDRESS1] AND  ");
        UnSQL.SQL.Add(@"  ADDRESS2=@[ADDRESS2] AND  ");
        UnSQL.SQL.Add(@"  ADDRESS3=@[ADDRESS3] AND  ");
        UnSQL.SQL.Add(@"  ADDRESS4=@[ADDRESS4] AND  ");
        UnSQL.SQL.Add(@"  ADDRESS5=@[ADDRESS5] AND  ");
        UnSQL.SQL.Add(@"  ADDRESS6=@[ADDRESS6] AND  ");
        UnSQL.SQL.Add(@"  IDCMDBUSER=@[IDCMDBUSER] AND  ");
        UnSQL.SQL.Add(@"  IDCMDBUSERADDRESS=@[IDCMDBUSERADDRESS] AND  ");
        UnSQL.SQL.Add(@"  IDSYSTEMSTATUS=@[IDSYSTEMSTATUS] ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("DataLink", "CMDBUSERADDRESS_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_CMDBUSERADDRESS.js Persistence.SMCI.Methods.CMDBUSERADDRESS_DEL", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.SMCI.Methods.CMDBUSERADDRESS_DELCMDBUSERADDRESS = function (CMDBUSERADDRESS) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.IDCMDBUSERADDRESS.FieldName, CMDBUSERADDRESS.IDCMDBUSERADDRESS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS1.FieldName, CMDBUSERADDRESS.ADDRESS1, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS2.FieldName, CMDBUSERADDRESS.ADDRESS2, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS3.FieldName, CMDBUSERADDRESS.ADDRESS3, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS4.FieldName, CMDBUSERADDRESS.ADDRESS4, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS5.FieldName, CMDBUSERADDRESS.ADDRESS5, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS6.FieldName, CMDBUSERADDRESS.ADDRESS6, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.IDCMDBUSER.FieldName, CMDBUSERADDRESS.IDCMDBUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.IDSYSTEMSTATUS.FieldName, CMDBUSERADDRESS.IDSYSTEMSTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   CMDBUSERADDRESS_DEL_2   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"CMDBUSERADDRESS_DEL_2", @"CMDBUSERADDRESS_DEL_2 description.");
        UnSQL.SQL.Add(@" DELETE CMDBUSERADDRESS  ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  ADDRESS1=@[ADDRESS1] AND  ");
        UnSQL.SQL.Add(@"  ADDRESS2=@[ADDRESS2] AND  ");
        UnSQL.SQL.Add(@"  ADDRESS3=@[ADDRESS3] AND  ");
        UnSQL.SQL.Add(@"  ADDRESS4=@[ADDRESS4] AND  ");
        UnSQL.SQL.Add(@"  ADDRESS5=@[ADDRESS5] AND  ");
        UnSQL.SQL.Add(@"  ADDRESS6=@[ADDRESS6] AND  ");
        UnSQL.SQL.Add(@"  IDCMDBUSER=@[IDCMDBUSER] AND  ");
        UnSQL.SQL.Add(@"  IDCMDBUSERADDRESS=@[IDCMDBUSERADDRESS] AND  ");
        UnSQL.SQL.Add(@"  IDSYSTEMSTATUS=@[IDSYSTEMSTATUS] ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("DataLink", "CMDBUSERADDRESS_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_CMDBUSERADDRESS.js Persistence.SMCI.Methods.CMDBUSERADDRESS_DEL", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
//**********************   METHODS   ********************************************************************************************
Persistence.SMCI.Methods.CMDBUSERADDRESS_ListSetID = function (CMDBUSERADDRESSList, IDCMDBUSERADDRESS) {
    for (i = 0; i < CMDBUSERADDRESSList.length; i++) {
        if (IDCMDBUSERADDRESS == CMDBUSERADDRESSList[i].IDCMDBUSERADDRESSR)
        return (CMDBUSERADDRESSList[i]);
    }
    var UnCMDBUSERADDRESS = new Persistence.SMCI.Properties.TCMDBUSERADDRESS();
    UnCMDBUSERADDRESS.IDCMDBUSERADDRESS = IDCMDBUSERADDRESS;
    return (UnCMDBUSERADDRESS);
}
Persistence.SMCI.Methods.CMDBUSERADDRESS_ListAdd = function (CMDBUSERADDRESSList, CMDBUSERADDRESS) {
    var i = Persistence.SMCI.Methods.CMDBUSERADDRESS_ListGetIndex(CMDBUSERADDRESSList, CMDBUSERADDRESS);
    if (i == -1) CMDBUSERADDRESSList.push(CMDBUSERADDRESS);
    else {
        CMDBUSERADDRESSList[i].ADDRESS1 = CMDBUSERADDRESS.ADDRESS1;
        CMDBUSERADDRESSList[i].ADDRESS2 = CMDBUSERADDRESS.ADDRESS2;
        CMDBUSERADDRESSList[i].ADDRESS3 = CMDBUSERADDRESS.ADDRESS3;
        CMDBUSERADDRESSList[i].ADDRESS4 = CMDBUSERADDRESS.ADDRESS4;
        CMDBUSERADDRESSList[i].ADDRESS5 = CMDBUSERADDRESS.ADDRESS5;
        CMDBUSERADDRESSList[i].ADDRESS6 = CMDBUSERADDRESS.ADDRESS6;
        CMDBUSERADDRESSList[i].IDCMDBUSER = CMDBUSERADDRESS.IDCMDBUSER;
        CMDBUSERADDRESSList[i].IDCMDBUSERADDRESS = CMDBUSERADDRESS.IDCMDBUSERADDRESS;
        CMDBUSERADDRESSList[i].IDSYSTEMSTATUS = CMDBUSERADDRESS.IDSYSTEMSTATUS;
    }
}

//CJRC_26072018
Persistence.SMCI.Methods.CMDBUSERADDRESS_ListGetIndex = function (CMDBUSERADDRESSList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.SMCI.Methods.CMDBUSERADDRESS_ListGetIndexIDCMDBUSERADDRESS(CMDBUSERADDRESSList, Param);

    }
    else {
        Res = Persistence.SMCI.Methods.CMDBUSERADDRESS_ListGetIndexCMDBUSERADDRESS(CMDBUSERADDRESSList, Param);
    }
    return (Res);
}

Persistence.SMCI.Methods.CMDBUSERADDRESS_ListGetIndexCMDBUSERADDRESS = function (CMDBUSERADDRESSList, CMDBUSERADDRESS) {
    for (i = 0; i < CMDBUSERADDRESSList.length; i++) {
        if (CMDBUSERADDRESS.IDCMDBUSERADDRESS == CMDBUSERADDRESSList[i].IDCMDBUSERADDRESS)
        return (i);
    }
    return (-1);
}
Persistence.SMCI.Methods.CMDBUSERADDRESS_ListGetIndexIDCMDBUSERADDRESS = function (CMDBUSERADDRESSList, IDCMDBUSERADDRESS) {
    for (i = 0; i < CMDBUSERADDRESSList.length; i++) {
        if (IDCMDBUSERADDRESS == CMDBUSERADDRESSList[i].IDCMDBUSERADDRESS)
        return (i);
    }
    return (-1);
}
//String Function 
Persistence.SMCI.Methods.CMDBUSERADDRESSListtoStr = function (CMDBUSERADDRESSList) {
    var Res = Persistence.SMCI.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(CMDBUSERADDRESSList.length, Longitud, Texto);
        for (Counter = 0; Counter < CMDBUSERADDRESSList.length; Counter++) {
            var UnCMDBUSERADDRESS = CMDBUSERADDRESSList[Counter];
            UnCMDBUSERADDRESS.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.SMCI.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.SMCI.Methods.StrtoCMDBUSERADDRESS = function (ProtocoloStr, CMDBUSERADDRESSList) {
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        CMDBUSERADDRESSList.length = 0;
        if (Persistence.SMCI.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnCMDBUSERADDRESS = new Persistence.Demo.Properties.TCMDBUSERADDRESS(); //Mode new row
                UnCMDBUSERADDRESS.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.CMDBUSERADDRESS_ListAdd(CMDBUSERADDRESSList, UnCMDBUSERADDRESS);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}
//Socket Function 
Persistence.SMCI.Methods.CMDBUSERADDRESSListToByte = function (CMDBUSERADDRESSList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, CMDBUSERADDRESSList.length - idx);
    for (i = idx; i < CMDBUSERADDRESSList.length; i++) {
        CMDBUSERADDRESSList[i].ToByte(MemStream);
    }
}
Persistence.SMCI.Methods.ByteToCMDBUSERADDRESSList = function (CMDBUSERADDRESSList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnCMDBUSERADDRESS = new Persistence.SMCI.Properties.TCMDBUSERADDRESS();
        CMDBUSERADDRESSList[i].ToByte(MemStream);
    }
}

