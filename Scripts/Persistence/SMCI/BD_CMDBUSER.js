﻿//**********************   PROPERTIES   *****************************************************************************************
Persistence.SMCI.Properties.TCMDBUSER = function () {
    this.IDATROLE = 0;
    this.IDCMDBCI = 0;
    this.IDCMDBUSER =0;
    this.LASTPWDCHANGE = new Date();
    this.IDMDCALENDARYDATE = 0;
    this.PASSWORD = "";
    this.PASSWORD01 = "";
    this.PASSWORD02 = "";
    this.PASSWORD03 = "";
    this.PASSWORD04 = "";
    this.PASSWORD05 = "";
    this.PASSWORD06 = "";
    this.PASSWORD07 = "";
    this.PASSWORD08 = "";
    this.PASSWORD09 = "";
    this.PASSWORD10 = "";
    this.CMDBUSERADDRESSList = new Array(); //Persistence.SMCI.Properties.TCMDBUSERADDRESS
    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDATROLE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDCMDBCI);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDCMDBUSER);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.LASTPWDCHANGE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDCALENDARYDATE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.PASSWORD);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.PASSWORD01);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.PASSWORD02);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.PASSWORD03);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.PASSWORD04);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.PASSWORD05);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.PASSWORD06);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.PASSWORD07);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.PASSWORD08);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.PASSWORD09);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.PASSWORD10);
    }
    this.ByteTo = function (MemStream) {
        this.IDATROLE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDCMDBCI = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDCMDBUSER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.LASTPWDCHANGE = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.IDMDCALENDARYDATE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.PASSWORD = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.PASSWORD01 = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.PASSWORD02 = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.PASSWORD03 = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.PASSWORD04 = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.PASSWORD05 = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.PASSWORD06 = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.PASSWORD07 = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.PASSWORD08 = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.PASSWORD09 = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.PASSWORD10 = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        {
            SysCfg.Str.Protocol.WriteInt(this.IDATROLE, Longitud, Texto);
            SysCfg.Str.Protocol.WriteInt(this.IDCMDBCI, Longitud, Texto);
            SysCfg.Str.Protocol.WriteInt(this.IDCMDBUSER, Longitud, Texto);
            SysCfg.Str.Protocol.WriteDateTime(this.LASTPWDCHANGE, Longitud, Texto);
            SysCfg.Str.Protocol.WriteInt(this.IDMDCALENDARYDATE, Longitud, Texto);
            SysCfg.Str.Protocol.WriteStr(this.PASSWORD, Longitud, Texto);
            SysCfg.Str.Protocol.WriteStr(this.PASSWORD01, Longitud, Texto);
            SysCfg.Str.Protocol.WriteStr(this.PASSWORD02, Longitud, Texto);
            SysCfg.Str.Protocol.WriteStr(this.PASSWORD03, Longitud, Texto);
            SysCfg.Str.Protocol.WriteStr(this.PASSWORD04, Longitud, Texto);
            SysCfg.Str.Protocol.WriteStr(this.PASSWORD05, Longitud, Texto);
            SysCfg.Str.Protocol.WriteStr(this.PASSWORD06, Longitud, Texto);
            SysCfg.Str.Protocol.WriteStr(this.PASSWORD07, Longitud, Texto);
            SysCfg.Str.Protocol.WriteStr(this.PASSWORD08, Longitud, Texto);
            SysCfg.Str.Protocol.WriteStr(this.PASSWORD09, Longitud, Texto);
            SysCfg.Str.Protocol.WriteStr(this.PASSWORD10, Longitud, Texto);
        }
        this.StrTo = function (Pos, Index, LongitudArray, Texto) {
            this.IDATROLE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            this.IDCMDBCI = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            this.IDCMDBUSER = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            this.LASTPWDCHANGE = SysCfg.Str.Protocol.ReadDateTime(Pos, Index, LongitudArray, Texto);
            this.IDMDCALENDARYDATE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            this.PASSWORD = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
            this.PASSWORD01 = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
            this.PASSWORD02 = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
            this.PASSWORD03 = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
            this.PASSWORD04 = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
            this.PASSWORD05 = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
            this.PASSWORD06 = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
            this.PASSWORD07 = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
            this.PASSWORD08 = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
            this.PASSWORD09 = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
            this.PASSWORD10 = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        }
    }
}
//**********************   METHODS server  ********************************************************************************************
Persistence.SMCI.Methods.CMDBUSER_Fill = function (CMDBUSERList, StrIDCMDBUSER) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    CMDBUSERList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDCMDBUSER == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.CMDBUSER.IDCMDBUSER.FieldName, " = " + UsrCfg.InternoAtisNames.CMDBUSER.NAME_TABLE + "." + UsrCfg.InternoAtisNames.CMDBUSER.IDCMDBUSER.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDCMDBUSER = " IN (" + StrIDCMDBUSER + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.CMDBUSER.IDCMDBUSER.FieldName, StrIDCMDBUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    try {
        var DS_CMDBUSER = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "CMDBUSER_GET", Param.ToBytes());
        ResErr = DS_CMDBUSER.ResErr;
        if (ResErr.NotError) {
            if (DS_CMDBUSER.DataSet.RecordCount > 0) {
                DS_CMDBUSER.DataSet.First();
                while (!(DS_CMDBUSER.DataSet.Eof)) {
                    var CMDBUSER = new Persistence.SMCI.Properties.TCMDBUSER();
                    CMDBUSER.IDCMDBUSER = DS_CMDBUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSER.IDCMDBUSER.FieldName).asInt32();
                    CMDBUSER.IDATROLE = DS_CMDBUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSER.IDATROLE.FieldName).asInt32();
                    CMDBUSER.IDCMDBCI = DS_CMDBUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSER.IDCMDBCI.FieldName).asInt32();
                    CMDBUSER.LASTPWDCHANGE = DS_CMDBUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSER.LASTPWDCHANGE.FieldName).asDateTime();
                    CMDBUSER.IDMDCALENDARYDATE = DS_CMDBUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSER.IDMDCALENDARYDATE.FieldName).asInt32();
                    CMDBUSER.PASSWORD = DS_CMDBUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD.FieldName).asString();
                    CMDBUSER.PASSWORD01 = DS_CMDBUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD01.FieldName).asString();
                    CMDBUSER.PASSWORD02 = DS_CMDBUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD02.FieldName).asString();
                    CMDBUSER.PASSWORD03 = DS_CMDBUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD03.FieldName).asString();
                    CMDBUSER.PASSWORD04 = DS_CMDBUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD04.FieldName).asString();
                    CMDBUSER.PASSWORD05 = DS_CMDBUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD05.FieldName).asString();
                    CMDBUSER.PASSWORD06 = DS_CMDBUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD06.FieldName).asString();
                    CMDBUSER.PASSWORD07 = DS_CMDBUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD07.FieldName).asString();
                    CMDBUSER.PASSWORD08 = DS_CMDBUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD08.FieldName).asString();
                    CMDBUSER.PASSWORD09 = DS_CMDBUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD09.FieldName).asString();
                    CMDBUSER.PASSWORD10 = DS_CMDBUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD10.FieldName).asString();
                    //Lento CMDBUSER_ListAdd(CMDBUSERList, CMDBUSER);
                    //Other
                    CMDBUSERList.push(CMDBUSER);
                    DS_CMDBUSER.DataSet.Next();
                }
            }
            else {
                DS_CMDBUSER.ResErr.NotError = false;
                DS_CMDBUSER.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_CMDBUSER.js Persistence.SMCI.Methods.CMDBUSER_Fill", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.SMCI.Methods.CMDBUSER_GETID = function (CMDBUSER) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSER.IDCMDBUSER.FieldName, CMDBUSER.IDCMDBUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSER.IDATROLE.FieldName, CMDBUSER.IDATROLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSER.IDCMDBCI.FieldName, CMDBUSER.IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoAtisNames.CMDBUSER.LASTPWDCHANGE.FieldName, CMDBUSER.LASTPWDCHANGE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSER.IDMDCALENDARYDATE.FieldName, CMDBUSER.IDMDCALENDARYDATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD.FieldName, CMDBUSER.PASSWORD, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD01.FieldName, CMDBUSER.PASSWORD01, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD02.FieldName, CMDBUSER.PASSWORD02, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD03.FieldName, CMDBUSER.PASSWORD03, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD04.FieldName, CMDBUSER.PASSWORD04, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD05.FieldName, CMDBUSER.PASSWORD05, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD06.FieldName, CMDBUSER.PASSWORD06, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD07.FieldName, CMDBUSER.PASSWORD07, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD08.FieldName, CMDBUSER.PASSWORD08, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD09.FieldName, CMDBUSER.PASSWORD09, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD10.FieldName, CMDBUSER.PASSWORD10, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   CMDBUSER_GETID   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"CMDBUSER_GETID", @"CMDBUSER_GETID description.");
        UnSQL.SQL.Add(@" SELECT  ");
        UnSQL.SQL.Add(@"  CMDBUSER.IDATROLE, ");
        UnSQL.SQL.Add(@"  CMDBUSER.IDCMDBCI, ");
        UnSQL.SQL.Add(@"  CMDBUSER.IDCMDBUSER, ");
        UnSQL.SQL.Add(@"  CMDBUSER.LASTPWDCHANGE, ");
        UnSQL.SQL.Add(@"  CMDBUSER.IDMDCALENDARYDATE, ");
        UnSQL.SQL.Add(@"  CMDBUSER.PASSWORD, ");
        UnSQL.SQL.Add(@"  CMDBUSER.PASSWORD01, ");
        UnSQL.SQL.Add(@"  CMDBUSER.PASSWORD02, ");
        UnSQL.SQL.Add(@"  CMDBUSER.PASSWORD03, ");
        UnSQL.SQL.Add(@"  CMDBUSER.PASSWORD04, ");
        UnSQL.SQL.Add(@"  CMDBUSER.PASSWORD05, ");
        UnSQL.SQL.Add(@"  CMDBUSER.PASSWORD06, ");
        UnSQL.SQL.Add(@"  CMDBUSER.PASSWORD07, ");
        UnSQL.SQL.Add(@"  CMDBUSER.PASSWORD08, ");
        UnSQL.SQL.Add(@"  CMDBUSER.PASSWORD09, ");
        UnSQL.SQL.Add(@"  CMDBUSER.PASSWORD10 ");
        UnSQL.SQL.Add(@" FROM  CMDBUSER ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  CMDBUSER.IDATROLE=@[IDATROLE] and  ");
        UnSQL.SQL.Add(@"  CMDBUSER.IDCMDBCI=@[IDCMDBCI] and  ");
        UnSQL.SQL.Add(@"  CMDBUSER.IDCMDBUSER=@[IDCMDBUSER] and  ");
        UnSQL.SQL.Add(@"  CMDBUSER.LASTPWDCHANGE=@[LASTPWDCHANGE] and  ");
        UnSQL.SQL.Add(@"  CMDBUSER.IDMDCALENDARYDATE=@[IDMDCALENDARYDATE] and  ");
        UnSQL.SQL.Add(@"  CMDBUSER.PASSWORD=@[PASSWORD] and  ");
        UnSQL.SQL.Add(@"  CMDBUSER.PASSWORD01=@[PASSWORD01] and  ");
        UnSQL.SQL.Add(@"  CMDBUSER.PASSWORD02=@[PASSWORD02] and  ");
        UnSQL.SQL.Add(@"  CMDBUSER.PASSWORD03=@[PASSWORD03] and  ");
        UnSQL.SQL.Add(@"  CMDBUSER.PASSWORD04=@[PASSWORD04] and  ");
        UnSQL.SQL.Add(@"  CMDBUSER.PASSWORD05=@[PASSWORD05] and  ");
        UnSQL.SQL.Add(@"  CMDBUSER.PASSWORD06=@[PASSWORD06] and  ");
        UnSQL.SQL.Add(@"  CMDBUSER.PASSWORD07=@[PASSWORD07] and  ");
        UnSQL.SQL.Add(@"  CMDBUSER.PASSWORD08=@[PASSWORD08] and  ");
        UnSQL.SQL.Add(@"  CMDBUSER.PASSWORD09=@[PASSWORD09] and  ");
        UnSQL.SQL.Add(@"  CMDBUSER.PASSWORD10=@[PASSWORD10] ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var DS_CMDBUSER = SysCfg.DB.SQL.Methods.OpenDataSet("DataLink", "CMDBUSER_GETID", Param.ToBytes());
        ResErr = DS_CMDBUSER.ResErr;
        if (ResErr.NotError) {
            if (DS_CMDBUSER.DataSet.RecordCount > 0) {
                DS_CMDBUSER.DataSet.First();
                CMDBUSER.IDCMDBUSER = DS_CMDBUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSER.IDCMDBUSER.FieldName).asInt32();
                //Other
                CMDBUSER.IDATROLE = DS_CMDBUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSER.IDATROLE.FieldName).asInt32();
                CMDBUSER.IDCMDBCI = DS_CMDBUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSER.IDCMDBCI.FieldName).asInt32();
                CMDBUSER.LASTPWDCHANGE = DS_CMDBUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSER.LASTPWDCHANGE.FieldName).asDateTime();
                CMDBUSER.IDMDCALENDARYDATE = DS_CMDBUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSER.IDMDCALENDARYDATE.FieldName).asInt32();
                CMDBUSER.PASSWORD = DS_CMDBUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD.FieldName).asString();
                CMDBUSER.PASSWORD01 = DS_CMDBUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD01.FieldName).asString();
                CMDBUSER.PASSWORD02 = DS_CMDBUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD02.FieldName).asString();
                CMDBUSER.PASSWORD03 = DS_CMDBUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD03.FieldName).asString();
                CMDBUSER.PASSWORD04 = DS_CMDBUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD04.FieldName).asString();
                CMDBUSER.PASSWORD05 = DS_CMDBUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD05.FieldName).asString();
                CMDBUSER.PASSWORD06 = DS_CMDBUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD06.FieldName).asString();
                CMDBUSER.PASSWORD07 = DS_CMDBUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD07.FieldName).asString();
                CMDBUSER.PASSWORD08 = DS_CMDBUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD08.FieldName).asString();
                CMDBUSER.PASSWORD09 = DS_CMDBUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD09.FieldName).asString();
                CMDBUSER.PASSWORD10 = DS_CMDBUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD10.FieldName).asString();
            }
            else {
                DS_CMDBUSER.ResErr.NotError = false;
                DS_CMDBUSER.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_CMDBUSER.js Persistence.SMCI.Methods.CMDBUSER_GETID", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.SMCI.Methods.CMDBUSER_ADD = function (CMDBUSER) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSER.IDCMDBUSER.FieldName, CMDBUSER.IDCMDBUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSER.IDATROLE.FieldName, CMDBUSER.IDATROLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSER.IDCMDBCI.FieldName, CMDBUSER.IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoAtisNames.CMDBUSER.LASTPWDCHANGE.FieldName, CMDBUSER.LASTPWDCHANGE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSER.IDMDCALENDARYDATE.FieldName, CMDBUSER.IDMDCALENDARYDATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD.FieldName, CMDBUSER.PASSWORD, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD01.FieldName, CMDBUSER.PASSWORD01, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD02.FieldName, CMDBUSER.PASSWORD02, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD03.FieldName, CMDBUSER.PASSWORD03, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD04.FieldName, CMDBUSER.PASSWORD04, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD05.FieldName, CMDBUSER.PASSWORD05, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD06.FieldName, CMDBUSER.PASSWORD06, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD07.FieldName, CMDBUSER.PASSWORD07, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD08.FieldName, CMDBUSER.PASSWORD08, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD09.FieldName, CMDBUSER.PASSWORD09, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD10.FieldName, CMDBUSER.PASSWORD10, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   CMDBUSER_ADD   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"CMDBUSER_ADD", @"CMDBUSER_ADD description.");
        UnSQL.SQL.Add(@" INSERT INTO CMDBUSER( ");
        UnSQL.SQL.Add(@"  IDATROLE,  ");
        UnSQL.SQL.Add(@"  IDCMDBCI,  ");
        UnSQL.SQL.Add(@"  IDCMDBUSER,  ");
        UnSQL.SQL.Add(@"  LASTPWDCHANGE,  ");
        UnSQL.SQL.Add(@"  IDMDCALENDARYDATE,  ");
        UnSQL.SQL.Add(@"  PASSWORD,  ");
        UnSQL.SQL.Add(@"  PASSWORD01,  ");
        UnSQL.SQL.Add(@"  PASSWORD02,  ");
        UnSQL.SQL.Add(@"  PASSWORD03,  ");
        UnSQL.SQL.Add(@"  PASSWORD04,  ");
        UnSQL.SQL.Add(@"  PASSWORD05,  ");
        UnSQL.SQL.Add(@"  PASSWORD06,  ");
        UnSQL.SQL.Add(@"  PASSWORD07,  ");
        UnSQL.SQL.Add(@"  PASSWORD08,  ");
        UnSQL.SQL.Add(@"  PASSWORD09,  ");
        UnSQL.SQL.Add(@"  PASSWORD10 ");
        UnSQL.SQL.Add(@" )  ");
        UnSQL.SQL.Add(@" VALUES ( ");
        UnSQL.SQL.Add(@"  @[IDATROLE], ");
        UnSQL.SQL.Add(@"  @[IDCMDBCI], ");
        UnSQL.SQL.Add(@"  @[IDCMDBUSER], ");
        UnSQL.SQL.Add(@"  @[LASTPWDCHANGE], ");
        UnSQL.SQL.Add(@"  @[IDMDCALENDARYDATE], ");
        UnSQL.SQL.Add(@"  @[PASSWORD], ");
        UnSQL.SQL.Add(@"  @[PASSWORD01], ");
        UnSQL.SQL.Add(@"  @[PASSWORD02], ");
        UnSQL.SQL.Add(@"  @[PASSWORD03], ");
        UnSQL.SQL.Add(@"  @[PASSWORD04], ");
        UnSQL.SQL.Add(@"  @[PASSWORD05], ");
        UnSQL.SQL.Add(@"  @[PASSWORD06], ");
        UnSQL.SQL.Add(@"  @[PASSWORD07], ");
        UnSQL.SQL.Add(@"  @[PASSWORD08], ");
        UnSQL.SQL.Add(@"  @[PASSWORD09], ");
        UnSQL.SQL.Add(@"  @[PASSWORD10] ");
        UnSQL.SQL.Add(@" ) ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("DataLink", "CMDBUSER_ADD", Param.ToBytes());
        var ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.SMCI.Methods.CMDBUSER_GETID(CMDBUSER);
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_CMDBUSER.js Persistence.SMCI.Methods.CMDBUSER_ADD", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.SMCI.Methods.CMDBUSER_UPD = function (CMDBUSER) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSER.IDCMDBUSER.FieldName, CMDBUSER.IDCMDBUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSER.IDATROLE.FieldName, CMDBUSER.IDATROLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSER.IDCMDBCI.FieldName, CMDBUSER.IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoAtisNames.CMDBUSER.LASTPWDCHANGE.FieldName, CMDBUSER.LASTPWDCHANGE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSER.IDMDCALENDARYDATE.FieldName, CMDBUSER.IDMDCALENDARYDATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD.FieldName, CMDBUSER.PASSWORD, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD01.FieldName, CMDBUSER.PASSWORD01, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD02.FieldName, CMDBUSER.PASSWORD02, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD03.FieldName, CMDBUSER.PASSWORD03, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD04.FieldName, CMDBUSER.PASSWORD04, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD05.FieldName, CMDBUSER.PASSWORD05, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD06.FieldName, CMDBUSER.PASSWORD06, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD07.FieldName, CMDBUSER.PASSWORD07, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD08.FieldName, CMDBUSER.PASSWORD08, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD09.FieldName, CMDBUSER.PASSWORD09, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD10.FieldName, CMDBUSER.PASSWORD10, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   CMDBUSER_UPD   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"CMDBUSER_UPD", @"CMDBUSER_UPD description.");
        UnSQL.SQL.Add(@" UPDATE CMDBUSER SET ");
        UnSQL.SQL.Add(@"  IDATROLE=@[IDATROLE],  ");
        UnSQL.SQL.Add(@"  IDCMDBCI=@[IDCMDBCI],  ");
        UnSQL.SQL.Add(@"  IDCMDBUSER=@[IDCMDBUSER],  ");
        UnSQL.SQL.Add(@"  LASTPWDCHANGE=@[LASTPWDCHANGE],  ");
        UnSQL.SQL.Add(@"  IDMDCALENDARYDATE=@[IDMDCALENDARYDATE],  ");
        UnSQL.SQL.Add(@"  PASSWORD=@[PASSWORD],  ");
        UnSQL.SQL.Add(@"  PASSWORD01=@[PASSWORD01],  ");
        UnSQL.SQL.Add(@"  PASSWORD02=@[PASSWORD02],  ");
        UnSQL.SQL.Add(@"  PASSWORD03=@[PASSWORD03],  ");
        UnSQL.SQL.Add(@"  PASSWORD04=@[PASSWORD04],  ");
        UnSQL.SQL.Add(@"  PASSWORD05=@[PASSWORD05],  ");
        UnSQL.SQL.Add(@"  PASSWORD06=@[PASSWORD06],  ");
        UnSQL.SQL.Add(@"  PASSWORD07=@[PASSWORD07],  ");
        UnSQL.SQL.Add(@"  PASSWORD08=@[PASSWORD08],  ");
        UnSQL.SQL.Add(@"  PASSWORD09=@[PASSWORD09],  ");
        UnSQL.SQL.Add(@"  PASSWORD10=@[PASSWORD10] ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  IDATROLE=@[IDATROLE] AND  ");
        UnSQL.SQL.Add(@"  IDCMDBCI=@[IDCMDBCI] AND  ");
        UnSQL.SQL.Add(@"  IDCMDBUSER=@[IDCMDBUSER] AND  ");
        UnSQL.SQL.Add(@"  LASTPWDCHANGE=@[LASTPWDCHANGE] AND  ");
        UnSQL.SQL.Add(@"  IDMDCALENDARYDATE=@[IDMDCALENDARYDATE] AND  ");
        UnSQL.SQL.Add(@"  PASSWORD=@[PASSWORD] AND  ");
        UnSQL.SQL.Add(@"  PASSWORD01=@[PASSWORD01] AND  ");
        UnSQL.SQL.Add(@"  PASSWORD02=@[PASSWORD02] AND  ");
        UnSQL.SQL.Add(@"  PASSWORD03=@[PASSWORD03] AND  ");
        UnSQL.SQL.Add(@"  PASSWORD04=@[PASSWORD04] AND  ");
        UnSQL.SQL.Add(@"  PASSWORD05=@[PASSWORD05] AND  ");
        UnSQL.SQL.Add(@"  PASSWORD06=@[PASSWORD06] AND  ");
        UnSQL.SQL.Add(@"  PASSWORD07=@[PASSWORD07] AND  ");
        UnSQL.SQL.Add(@"  PASSWORD08=@[PASSWORD08] AND  ");
        UnSQL.SQL.Add(@"  PASSWORD09=@[PASSWORD09] AND  ");
        UnSQL.SQL.Add(@"  PASSWORD10=@[PASSWORD10] ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("DataLink", "CMDBUSER_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_CMDBUSER.js Persistence.SMCI.Methods.CMDBUSER_UPD", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.SMCI.Methods.CMDBUSER_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.SMCI.Methods.CMDBUSER_DELIDCMDBUSER(/*String StrIDCMDBUSER*/Param);
    }
    else {
        Res = Persistence.SMCI.Methods.CMDBUSER_DELCMDBUSER(Param);
    }
    return (Res);
}

Persistence.SMCI.Methods.CMDBUSER_DELIDCMDBUSER = function (/*String StrIDCMDBUSER*/IDCMDBUSER) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDCMDBUSER == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.CMDBUSER.IDCMDBUSER.FieldName, " = " + UsrCfg.InternoAtisNames.CMDBUSER.NAME_TABLE + "." + UsrCfg.InternoAtisNames.CMDBUSER.IDCMDBUSER.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDCMDBUSER = " IN (" + StrIDCMDBUSER + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.CMDBUSER.IDCMDBUSER.FieldName, StrIDCMDBUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSER.IDCMDBUSER.FieldName, IDCMDBUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   CMDBUSER_DEL_1   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"CMDBUSER_DEL_1", @"CMDBUSER_DEL_1 description.");
        UnSQL.SQL.Add(@" DELETE CMDBUSER  ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  IDATROLE=@[IDATROLE] AND  ");
        UnSQL.SQL.Add(@"  IDCMDBCI=@[IDCMDBCI] AND  ");
        UnSQL.SQL.Add(@"  IDCMDBUSER=@[IDCMDBUSER] AND  ");
        UnSQL.SQL.Add(@"  LASTPWDCHANGE=@[LASTPWDCHANGE] AND  ");
        UnSQL.SQL.Add(@"  IDMDCALENDARYDATE=@[IDMDCALENDARYDATE] AND  ");
        UnSQL.SQL.Add(@"  PASSWORD=@[PASSWORD] AND  ");
        UnSQL.SQL.Add(@"  PASSWORD01=@[PASSWORD01] AND  ");
        UnSQL.SQL.Add(@"  PASSWORD02=@[PASSWORD02] AND  ");
        UnSQL.SQL.Add(@"  PASSWORD03=@[PASSWORD03] AND  ");
        UnSQL.SQL.Add(@"  PASSWORD04=@[PASSWORD04] AND  ");
        UnSQL.SQL.Add(@"  PASSWORD05=@[PASSWORD05] AND  ");
        UnSQL.SQL.Add(@"  PASSWORD06=@[PASSWORD06] AND  ");
        UnSQL.SQL.Add(@"  PASSWORD07=@[PASSWORD07] AND  ");
        UnSQL.SQL.Add(@"  PASSWORD08=@[PASSWORD08] AND  ");
        UnSQL.SQL.Add(@"  PASSWORD09=@[PASSWORD09] AND  ");
        UnSQL.SQL.Add(@"  PASSWORD10=@[PASSWORD10] ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("DataLink", "CMDBUSER_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_CMDBUSER.js Persistence.SMCI.Methods.CMDBUSER_DEL", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.SMCI.Methods.CMDBUSER_DELCMDBUSER = function (CMDBUSER) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSER.IDCMDBUSER.FieldName, CMDBUSER.IDCMDBUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSER.IDATROLE.FieldName, CMDBUSER.IDATROLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSER.IDCMDBCI.FieldName, CMDBUSER.IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoAtisNames.CMDBUSER.LASTPWDCHANGE.FieldName, CMDBUSER.LASTPWDCHANGE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSER.IDMDCALENDARYDATE.FieldName, CMDBUSER.IDMDCALENDARYDATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD.FieldName, CMDBUSER.PASSWORD, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD01.FieldName, CMDBUSER.PASSWORD01, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD02.FieldName, CMDBUSER.PASSWORD02, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD03.FieldName, CMDBUSER.PASSWORD03, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD04.FieldName, CMDBUSER.PASSWORD04, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD05.FieldName, CMDBUSER.PASSWORD05, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD06.FieldName, CMDBUSER.PASSWORD06, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD07.FieldName, CMDBUSER.PASSWORD07, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD08.FieldName, CMDBUSER.PASSWORD08, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD09.FieldName, CMDBUSER.PASSWORD09, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD10.FieldName, CMDBUSER.PASSWORD10, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   CMDBUSER_DEL_2   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"CMDBUSER_DEL_2", @"CMDBUSER_DEL_2 description.");
        UnSQL.SQL.Add(@" DELETE CMDBUSER  ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  IDATROLE=@[IDATROLE] AND  ");
        UnSQL.SQL.Add(@"  IDCMDBCI=@[IDCMDBCI] AND  ");
        UnSQL.SQL.Add(@"  IDCMDBUSER=@[IDCMDBUSER] AND  ");
        UnSQL.SQL.Add(@"  LASTPWDCHANGE=@[LASTPWDCHANGE] AND  ");
        UnSQL.SQL.Add(@"  IDMDCALENDARYDATE=@[IDMDCALENDARYDATE] AND  ");
        UnSQL.SQL.Add(@"  PASSWORD=@[PASSWORD] AND  ");
        UnSQL.SQL.Add(@"  PASSWORD01=@[PASSWORD01] AND  ");
        UnSQL.SQL.Add(@"  PASSWORD02=@[PASSWORD02] AND  ");
        UnSQL.SQL.Add(@"  PASSWORD03=@[PASSWORD03] AND  ");
        UnSQL.SQL.Add(@"  PASSWORD04=@[PASSWORD04] AND  ");
        UnSQL.SQL.Add(@"  PASSWORD05=@[PASSWORD05] AND  ");
        UnSQL.SQL.Add(@"  PASSWORD06=@[PASSWORD06] AND  ");
        UnSQL.SQL.Add(@"  PASSWORD07=@[PASSWORD07] AND  ");
        UnSQL.SQL.Add(@"  PASSWORD08=@[PASSWORD08] AND  ");
        UnSQL.SQL.Add(@"  PASSWORD09=@[PASSWORD09] AND  ");
        UnSQL.SQL.Add(@"  PASSWORD10=@[PASSWORD10] ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("DataLink", "CMDBUSER_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_CMDBUSER.js Persistence.SMCI.Methods.CMDBUSER_DEL", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
//**********************   METHODS   ********************************************************************************************
Persistence.SMCI.Methods.CMDBUSER_ListSetID = function (CMDBUSERList, IDCMDBUSER) {
    for (i = 0; i < CMDBUSERList.length; i++) {
        if (IDCMDBUSER == CMDBUSERList[i].IDCMDBUSERR)
            return (CMDBUSERList[i]);
    }
    var UnCMDBUSER = new Persistence.SMCI.Properties.TCMDBUSER();
    UnCMDBUSER.IDCMDBUSER = IDCMDBUSER;
    return (UnCMDBUSER);
}
Persistence.SMCI.Methods.CMDBUSER_ListAdd = function (CMDBUSERList, CMDBUSER) {
    var i = Persistence.SMCI.Methods.CMDBUSER_ListGetIndex(CMDBUSERList, CMDBUSER);
    if (i == -1) CMDBUSERList.push(CMDBUSER);
    else {
        CMDBUSERList[i].IDATROLE = CMDBUSER.IDATROLE;
        CMDBUSERList[i].IDCMDBCI = CMDBUSER.IDCMDBCI;
        CMDBUSERList[i].IDCMDBUSER = CMDBUSER.IDCMDBUSER;
        CMDBUSERList[i].LASTPWDCHANGE = CMDBUSER.LASTPWDCHANGE;
        CMDBUSERList[i].IDMDCALENDARYDATE = CMDBUSER.IDMDCALENDARYDATE;
        CMDBUSERList[i].PASSWORD = CMDBUSER.PASSWORD;
        CMDBUSERList[i].PASSWORD01 = CMDBUSER.PASSWORD01;
        CMDBUSERList[i].PASSWORD02 = CMDBUSER.PASSWORD02;
        CMDBUSERList[i].PASSWORD03 = CMDBUSER.PASSWORD03;
        CMDBUSERList[i].PASSWORD04 = CMDBUSER.PASSWORD04;
        CMDBUSERList[i].PASSWORD05 = CMDBUSER.PASSWORD05;
        CMDBUSERList[i].PASSWORD06 = CMDBUSER.PASSWORD06;
        CMDBUSERList[i].PASSWORD07 = CMDBUSER.PASSWORD07;
        CMDBUSERList[i].PASSWORD08 = CMDBUSER.PASSWORD08;
        CMDBUSERList[i].PASSWORD09 = CMDBUSER.PASSWORD09;
        CMDBUSERList[i].PASSWORD10 = CMDBUSER.PASSWORD10;

    }
}

//CJRC_26072018
Persistence.SMCI.Methods.CMDBUSER_ListGetIndex = function (CMDBUSERList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.SMCI.Methods.CMDBUSER_ListGetIndexIDCMDBUSER(CMDBUSERList, Param);

    }
    else {
        Res = Persistence.SMCI.Methods.CMDBUSER_ListGetIndexCMDBUSER(CMDBUSERList, Param);
    }
    return (Res);
}

Persistence.SMCI.Methods.CMDBUSER_ListGetIndexCMDBUSER = function (CMDBUSERList, CMDBUSER) {
    for (i = 0; i < CMDBUSERList.length; i++) {
        if (CMDBUSER.IDCMDBUSER == CMDBUSERList[i].IDCMDBUSER)
            return (i);
    }
    return (-1);
}
Persistence.SMCI.Methods.CMDBUSER_ListGetIndexIDCMDBUSER = function (CMDBUSERList, IDCMDBUSER) {
    for (i = 0; i < CMDBUSERList.length; i++) {
        if (IDCMDBUSER == CMDBUSERList[i].IDCMDBUSER)
            return (i);
    }
    return (-1);
}
//String Function 
Persistence.SMCI.Methods.CMDBUSERListtoStr = function (CMDBUSERList) {
    var Res = Persistence.SMCI.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(CMDBUSERList.length, Longitud, Texto);
        for (Counter = 0; Counter < CMDBUSERList.length; Counter++) {
            var UnCMDBUSER = CMDBUSERList[Counter];
            UnCMDBUSER.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.SMCI.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.SMCI.Methods.StrtoCMDBUSER = function (ProtocoloStr, CMDBUSERList) {
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        CMDBUSERList.length = 0;
        if (Persistence.SMCI.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnCMDBUSER = new Persistence.Demo.Properties.TCMDBUSER(); //Mode new row
                UnCMDBUSER.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.CMDBUSER_ListAdd(CMDBUSERList, UnCMDBUSER);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}
//Socket Function 
Persistence.SMCI.Methods.CMDBUSERListToByte = function (CMDBUSERList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, CMDBUSERList.length - idx);
    for (i = idx; i < CMDBUSERList.length; i++) {
        CMDBUSERList[i].ToByte(MemStream);
    }
}
Persistence.SMCI.Methods.ByteToCMDBUSERList = function (CMDBUSERList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnCMDBUSER = new Persistence.SMCI.Properties.TCMDBUSER();
        CMDBUSERList[i].ToByte(MemStream);
    }
}