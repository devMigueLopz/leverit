﻿//TreduceDB
//Persistence.SMCI

Persistence.SMCI.Properties._Version = "001";

Persistence.SMCI.Properties.TCMDBCONTACTTYPE =
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _None: { value: 1, name: "None" },
    _Mobile: { value: 2, name: "Mobile" },
    _Phone: { value: 3, name: "Phone" },
    _eMail: { value: 4, name: "eMail" },
}

