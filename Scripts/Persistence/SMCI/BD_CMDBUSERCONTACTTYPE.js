﻿//**********************   PROPERTIES   *****************************************************************************************
Persistence.SMCI.Properties.TCMDBUSERCONTACTTYPE = function () {
    this.CONTACTDEFINE = "";
    this.IDCMDBCONTACTTYPE = 0;//quien lo coloca 
    this.IDCMDBUSER = 0;// aquien va dirigido
    this.IDCMDBUSERCONTACTTYPE = 0; //tipo de contacto 
    this.IDSYSTEMSTATUS = 0;
    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CONTACTDEFINE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDCMDBCONTACTTYPE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDCMDBUSER);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, parseInt(this.IDCMDBUSERCONTACTTYPE));
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSYSTEMSTATUS);
    }
    this.ByteTo = function (MemStream) {
        this.CONTACTDEFINE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDCMDBCONTACTTYPE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDCMDBUSER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDCMDBUSERCONTACTTYPE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSYSTEMSTATUS = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteStr(this.CONTACTDEFINE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDCMDBCONTACTTYPE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDCMDBUSER, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(parseInt(this.IDCMDBUSERCONTACTTYPE), Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDSYSTEMSTATUS, Longitud, Texto);
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.CONTACTDEFINE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.IDCMDBCONTACTTYPE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDCMDBUSER = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDCMDBUSERCONTACTTYPE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDSYSTEMSTATUS = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
    }
}
//**********************   METHODS server  ********************************************************************************************
Persistence.SMCI.Methods.CMDBUSERCONTACTTYPE_Fill = function (CMDBUSERCONTACTTYPEList, StrIDCMDBUSER) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    CMDBUSERCONTACTTYPEList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDCMDBUSER == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBUSER.FieldName, " = " + UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.NAME_TABLE + "." + UsrCfg.InternoAtisNames.CMDBUSER.IDCMDBUSER.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDCMDBUSER = " IN (" + StrIDCMDBUSER + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBUSER.FieldName, StrIDCMDBUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    try {
        var DS_CMDBUSERCONTACTTYPE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "CMDBUSERCONTACTTYPE_GET", Param.ToBytes());
        ResErr = DS_CMDBUSERCONTACTTYPE.ResErr;
        if (ResErr.NotError) {
            if (DS_CMDBUSERCONTACTTYPE.DataSet.RecordCount > 0) {
                DS_CMDBUSERCONTACTTYPE.DataSet.First();
                while (!(DS_CMDBUSERCONTACTTYPE.DataSet.Eof)) {
                    var CMDBUSERCONTACTTYPE = new Persistence.SMCI.Properties.TCMDBUSERCONTACTTYPE();
                    CMDBUSERCONTACTTYPE.IDCMDBUSERCONTACTTYPE = DS_CMDBUSERCONTACTTYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBUSERCONTACTTYPE.FieldName).asInt32();
                    //Other
                    CMDBUSERCONTACTTYPE.CONTACTDEFINE = DS_CMDBUSERCONTACTTYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.CONTACTDEFINE.FieldName).asString();
                    CMDBUSERCONTACTTYPE.IDCMDBCONTACTTYPE = DS_CMDBUSERCONTACTTYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBCONTACTTYPE.FieldName).asInt32();
                    CMDBUSERCONTACTTYPE.IDCMDBUSER = DS_CMDBUSERCONTACTTYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBUSER.FieldName).asInt32();
                    CMDBUSERCONTACTTYPE.IDSYSTEMSTATUS = DS_CMDBUSERCONTACTTYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDSYSTEMSTATUS.FieldName).asInt32();
                    //Other
                    CMDBUSERCONTACTTYPEList.push(CMDBUSERCONTACTTYPE);
                    DS_CMDBUSERCONTACTTYPE.DataSet.Next();
                }
            }
            else {
                DS_CMDBUSERCONTACTTYPE.ResErr.NotError = false;
                DS_CMDBUSERCONTACTTYPE.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_CMDBUSERCONTACTTYPE.js Persistence.SMCI.Methods.CMDBUSERCONTACTTYPE_Fill", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.SMCI.Methods.CMDBUSERCONTACTTYPE_GETID = function (CMDBUSERCONTACTTYPE) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBUSERCONTACTTYPE.FieldName, CMDBUSERCONTACTTYPE.IDCMDBUSERCONTACTTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.CONTACTDEFINE.FieldName, CMDBUSERCONTACTTYPE.CONTACTDEFINE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBCONTACTTYPE.FieldName, CMDBUSERCONTACTTYPE.IDCMDBCONTACTTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBUSER.FieldName, CMDBUSERCONTACTTYPE.IDCMDBUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDSYSTEMSTATUS.FieldName, CMDBUSERCONTACTTYPE.IDSYSTEMSTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   CMDBUSERCONTACTTYPE_GETID   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"CMDBUSERCONTACTTYPE_GETID", @"CMDBUSERCONTACTTYPE_GETID description.");
        UnSQL.SQL.Add(@" SELECT  ");
        UnSQL.SQL.Add(@"  CMDBUSERCONTACTTYPE.CONTACTDEFINE, ");
        UnSQL.SQL.Add(@"  CMDBUSERCONTACTTYPE.IDCMDBCONTACTTYPE, ");
        UnSQL.SQL.Add(@"  CMDBUSERCONTACTTYPE.IDCMDBUSER, ");
        UnSQL.SQL.Add(@"  CMDBUSERCONTACTTYPE.IDCMDBUSERCONTACTTYPE, ");
        UnSQL.SQL.Add(@"  CMDBUSERCONTACTTYPE.IDSYSTEMSTATUS ");
        UnSQL.SQL.Add(@" FROM  CMDBUSERCONTACTTYPE ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  CMDBUSERCONTACTTYPE.CONTACTDEFINE=@[CONTACTDEFINE] and  ");
        UnSQL.SQL.Add(@"  CMDBUSERCONTACTTYPE.IDCMDBCONTACTTYPE=@[IDCMDBCONTACTTYPE] and  ");
        UnSQL.SQL.Add(@"  CMDBUSERCONTACTTYPE.IDCMDBUSER=@[IDCMDBUSER] and  ");
        UnSQL.SQL.Add(@"  CMDBUSERCONTACTTYPE.IDCMDBUSERCONTACTTYPE=@[IDCMDBUSERCONTACTTYPE] and  ");
        UnSQL.SQL.Add(@"  CMDBUSERCONTACTTYPE.IDSYSTEMSTATUS=@[IDSYSTEMSTATUS] ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var DS_CMDBUSERCONTACTTYPE = SysCfg.DB.SQL.Methods.OpenDataSet("DataLink", "CMDBUSERCONTACTTYPE_GETID", Param.ToBytes());
        ResErr = DS_CMDBUSERCONTACTTYPE.ResErr;
        if (ResErr.NotError) {
            if (DS_CMDBUSERCONTACTTYPE.DataSet.RecordCount > 0) {
                DS_CMDBUSERCONTACTTYPE.DataSet.First();
                CMDBUSERCONTACTTYPE.IDCMDBUSERCONTACTTYPE = DS_CMDBUSERCONTACTTYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBUSERCONTACTTYPE.FieldName).asInt32();
                //Other
                CMDBUSERCONTACTTYPE.CONTACTDEFINE = DS_CMDBUSERCONTACTTYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.CONTACTDEFINE.FieldName).asString();
                CMDBUSERCONTACTTYPE.IDCMDBCONTACTTYPE = DS_CMDBUSERCONTACTTYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBCONTACTTYPE.FieldName).asInt32();
                CMDBUSERCONTACTTYPE.IDCMDBUSER = DS_CMDBUSERCONTACTTYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBUSER.FieldName).asInt32();
                CMDBUSERCONTACTTYPE.IDSYSTEMSTATUS = DS_CMDBUSERCONTACTTYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDSYSTEMSTATUS.FieldName).asInt32();
            }
            else {
                DS_CMDBUSERCONTACTTYPE.ResErr.NotError = false;
                DS_CMDBUSERCONTACTTYPE.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_CMDBUSERCONTACTTYPE.js Persistence.SMCI.Methods.CMDBUSERCONTACTTYPE_GETID", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.SMCI.Methods.CMDBUSERCONTACTTYPE_ADD = function (CMDBUSERCONTACTTYPE) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBUSERCONTACTTYPE.FieldName, CMDBUSERCONTACTTYPE.IDCMDBUSERCONTACTTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.CONTACTDEFINE.FieldName, CMDBUSERCONTACTTYPE.CONTACTDEFINE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBCONTACTTYPE.FieldName, CMDBUSERCONTACTTYPE.IDCMDBCONTACTTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBUSER.FieldName, CMDBUSERCONTACTTYPE.IDCMDBUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDSYSTEMSTATUS.FieldName, CMDBUSERCONTACTTYPE.IDSYSTEMSTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   CMDBUSERCONTACTTYPE_ADD   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"CMDBUSERCONTACTTYPE_ADD", @"CMDBUSERCONTACTTYPE_ADD description.");
        UnSQL.SQL.Add(@" INSERT INTO CMDBUSERCONTACTTYPE( ");
        UnSQL.SQL.Add(@"  CONTACTDEFINE,  ");
        UnSQL.SQL.Add(@"  IDCMDBCONTACTTYPE,  ");
        UnSQL.SQL.Add(@"  IDCMDBUSER,  ");
        UnSQL.SQL.Add(@"  IDCMDBUSERCONTACTTYPE,  ");
        UnSQL.SQL.Add(@"  IDSYSTEMSTATUS ");
        UnSQL.SQL.Add(@" )  ");
        UnSQL.SQL.Add(@" VALUES ( ");
        UnSQL.SQL.Add(@"  @[CONTACTDEFINE], ");
        UnSQL.SQL.Add(@"  @[IDCMDBCONTACTTYPE], ");
        UnSQL.SQL.Add(@"  @[IDCMDBUSER], ");
        UnSQL.SQL.Add(@"  @[IDCMDBUSERCONTACTTYPE], ");
        UnSQL.SQL.Add(@"  @[IDSYSTEMSTATUS] ");
        UnSQL.SQL.Add(@" ) ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("DataLink", "CMDBUSERCONTACTTYPE_ADD", Param.ToBytes());
        var ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.SMCI.Methods.CMDBUSERCONTACTTYPE_GETID(CMDBUSERCONTACTTYPE);
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_CMDBUSERCONTACTTYPE.js Persistence.SMCI.Methods.CMDBUSERCONTACTTYPE_ADD", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.SMCI.Methods.CMDBUSERCONTACTTYPE_UPD = function (CMDBUSERCONTACTTYPE) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBUSERCONTACTTYPE.FieldName, CMDBUSERCONTACTTYPE.IDCMDBUSERCONTACTTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.CONTACTDEFINE.FieldName, CMDBUSERCONTACTTYPE.CONTACTDEFINE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBCONTACTTYPE.FieldName, CMDBUSERCONTACTTYPE.IDCMDBCONTACTTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBUSER.FieldName, CMDBUSERCONTACTTYPE.IDCMDBUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDSYSTEMSTATUS.FieldName, CMDBUSERCONTACTTYPE.IDSYSTEMSTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   CMDBUSERCONTACTTYPE_UPD   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"CMDBUSERCONTACTTYPE_UPD", @"CMDBUSERCONTACTTYPE_UPD description.");
        UnSQL.SQL.Add(@" UPDATE CMDBUSERCONTACTTYPE SET ");
        UnSQL.SQL.Add(@"  CONTACTDEFINE=@[CONTACTDEFINE],  ");
        UnSQL.SQL.Add(@"  IDCMDBCONTACTTYPE=@[IDCMDBCONTACTTYPE],  ");
        UnSQL.SQL.Add(@"  IDCMDBUSER=@[IDCMDBUSER],  ");
        UnSQL.SQL.Add(@"  IDCMDBUSERCONTACTTYPE=@[IDCMDBUSERCONTACTTYPE],  ");
        UnSQL.SQL.Add(@"  IDSYSTEMSTATUS=@[IDSYSTEMSTATUS] ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  CONTACTDEFINE=@[CONTACTDEFINE] AND  ");
        UnSQL.SQL.Add(@"  IDCMDBCONTACTTYPE=@[IDCMDBCONTACTTYPE] AND  ");
        UnSQL.SQL.Add(@"  IDCMDBUSER=@[IDCMDBUSER] AND  ");
        UnSQL.SQL.Add(@"  IDCMDBUSERCONTACTTYPE=@[IDCMDBUSERCONTACTTYPE] AND  ");
        UnSQL.SQL.Add(@"  IDSYSTEMSTATUS=@[IDSYSTEMSTATUS] ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("DataLink", "CMDBUSERCONTACTTYPE_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_CMDBUSERCONTACTTYPE.js Persistence.SMCI.Methods.CMDBUSERCONTACTTYPE_UPD", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.SMCI.Methods.CMDBUSERCONTACTTYPE_DEL  = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.SMCI.Methods.CMDBUSERCONTACTTYPE_DELIDCMDBUSERCONTACTTYPE(/*String StrIDCMDBUSERCONTACTTYPE*/Param);
    }
    else {
        Res = Persistence.SMCI.Methods.CMDBUSERCONTACTTYPE_DELCMDBUSERCONTACTTYPE(Param) ;
    }
    return (Res);
}

Persistence.SMCI.Methods.CMDBUSERCONTACTTYPE_DELIDCMDBUSERCONTACTTYPE = function (/*String StrIDCMDBUSERCONTACTTYPE*/IDCMDBUSERCONTACTTYPE) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDCMDBUSERCONTACTTYPE == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBUSERCONTACTTYPE.FieldName, " = " + UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.NAME_TABLE + "." + UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBUSERCONTACTTYPE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDCMDBUSERCONTACTTYPE = " IN (" + StrIDCMDBUSERCONTACTTYPE + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBUSERCONTACTTYPE.FieldName, StrIDCMDBUSERCONTACTTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBUSERCONTACTTYPE.FieldName, IDCMDBUSERCONTACTTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   CMDBUSERCONTACTTYPE_DEL_1   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"CMDBUSERCONTACTTYPE_DEL_1", @"CMDBUSERCONTACTTYPE_DEL_1 description.");
        UnSQL.SQL.Add(@" DELETE CMDBUSERCONTACTTYPE  ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  CONTACTDEFINE=@[CONTACTDEFINE] AND  ");
        UnSQL.SQL.Add(@"  IDCMDBCONTACTTYPE=@[IDCMDBCONTACTTYPE] AND  ");
        UnSQL.SQL.Add(@"  IDCMDBUSER=@[IDCMDBUSER] AND  ");
        UnSQL.SQL.Add(@"  IDCMDBUSERCONTACTTYPE=@[IDCMDBUSERCONTACTTYPE] AND  ");
        UnSQL.SQL.Add(@"  IDSYSTEMSTATUS=@[IDSYSTEMSTATUS] ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("DataLink", "CMDBUSERCONTACTTYPE_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_CMDBUSERCONTACTTYPE.js Persistence.SMCI.Methods.CMDBUSERCONTACTTYPE_DEL", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.SMCI.Methods.CMDBUSERCONTACTTYPE_DELCMDBUSERCONTACTTYPE = function (CMDBUSERCONTACTTYPE) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBUSERCONTACTTYPE.FieldName, CMDBUSERCONTACTTYPE.IDCMDBUSERCONTACTTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.CONTACTDEFINE.FieldName, CMDBUSERCONTACTTYPE.CONTACTDEFINE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBCONTACTTYPE.FieldName, CMDBUSERCONTACTTYPE.IDCMDBCONTACTTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBUSER.FieldName, CMDBUSERCONTACTTYPE.IDCMDBUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDSYSTEMSTATUS.FieldName, CMDBUSERCONTACTTYPE.IDSYSTEMSTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   CMDBUSERCONTACTTYPE_DEL_2   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"CMDBUSERCONTACTTYPE_DEL_2", @"CMDBUSERCONTACTTYPE_DEL_2 description.");
        UnSQL.SQL.Add(@" DELETE CMDBUSERCONTACTTYPE  ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  CONTACTDEFINE=@[CONTACTDEFINE] AND  ");
        UnSQL.SQL.Add(@"  IDCMDBCONTACTTYPE=@[IDCMDBCONTACTTYPE] AND  ");
        UnSQL.SQL.Add(@"  IDCMDBUSER=@[IDCMDBUSER] AND  ");
        UnSQL.SQL.Add(@"  IDCMDBUSERCONTACTTYPE=@[IDCMDBUSERCONTACTTYPE] AND  ");
        UnSQL.SQL.Add(@"  IDSYSTEMSTATUS=@[IDSYSTEMSTATUS] ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("DataLink", "CMDBUSERCONTACTTYPE_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_CMDBUSERCONTACTTYPE.js Persistence.SMCI.Methods.CMDBUSERCONTACTTYPE_DEL", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
//**********************   METHODS   ********************************************************************************************
Persistence.SMCI.Methods.CMDBUSERCONTACTTYPE_ListSetID = function (CMDBUSERCONTACTTYPEList, IDCMDBUSERCONTACTTYPE) {
    for (i = 0; i < CMDBUSERCONTACTTYPEList.length; i++) {
        if (IDCMDBUSERCONTACTTYPE == CMDBUSERCONTACTTYPEList[i].IDCMDBUSERCONTACTTYPER)
            return (CMDBUSERCONTACTTYPEList[i]);
    }
    var UnCMDBUSERCONTACTTYPE = new Persistence.SMCI.Properties.TCMDBUSERCONTACTTYPE();
    UnCMDBUSERCONTACTTYPE.IDCMDBUSERCONTACTTYPE = IDCMDBUSERCONTACTTYPE;
    return (UnCMDBUSERCONTACTTYPE);
}
Persistence.SMCI.Methods.CMDBUSERCONTACTTYPE_ListAdd = function (CMDBUSERCONTACTTYPEList, CMDBUSERCONTACTTYPE) {
    var i = Persistence.SMCI.Methods.CMDBUSERCONTACTTYPE_ListGetIndex(CMDBUSERCONTACTTYPEList, CMDBUSERCONTACTTYPE);
    if (i == -1) CMDBUSERCONTACTTYPEList.push(CMDBUSERCONTACTTYPE);
    else {
        CMDBUSERCONTACTTYPEList[i].CONTACTDEFINE = CMDBUSERCONTACTTYPE.CONTACTDEFINE;
        CMDBUSERCONTACTTYPEList[i].IDCMDBCONTACTTYPE = CMDBUSERCONTACTTYPE.IDCMDBCONTACTTYPE;
        CMDBUSERCONTACTTYPEList[i].IDCMDBUSER = CMDBUSERCONTACTTYPE.IDCMDBUSER;
        CMDBUSERCONTACTTYPEList[i].IDCMDBUSERCONTACTTYPE = CMDBUSERCONTACTTYPE.IDCMDBUSERCONTACTTYPE;
        CMDBUSERCONTACTTYPEList[i].IDSYSTEMSTATUS = CMDBUSERCONTACTTYPE.IDSYSTEMSTATUS;
    }
}

//CJRC_26072018
Persistence.SMCI.Methods.CMDBUSERCONTACTTYPE_ListGetIndex = function (CMDBUSERCONTACTTYPEList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.SMCI.Methods.CMDBUSERCONTACTTYPE_ListGetIndexIDCMDBUSERCONTACTTYPE(CMDBUSERCONTACTTYPEList, Param);

    }
    else {
        Res = Persistence.SMCI.Methods.CMDBUSERCONTACTTYPE_ListGetIndexCMDBUSERCONTACTTYPE(CMDBUSERCONTACTTYPEList, Param);
    }
    return (Res);
}

Persistence.SMCI.Methods.CMDBUSERCONTACTTYPE_ListGetIndexCMDBUSERCONTACTTYPE = function (CMDBUSERCONTACTTYPEList, CMDBUSERCONTACTTYPE) {
    for (i = 0; i < CMDBUSERCONTACTTYPEList.length; i++) {
        if (CMDBUSERCONTACTTYPE.IDCMDBUSERCONTACTTYPE == CMDBUSERCONTACTTYPEList[i].IDCMDBUSERCONTACTTYPE)
            return (i);
    }
    return (-1);
}
Persistence.SMCI.Methods.CMDBUSERCONTACTTYPE_ListGetIndexIDCMDBUSERCONTACTTYPE = function (CMDBUSERCONTACTTYPEList, IDCMDBUSERCONTACTTYPE) {
    for (i = 0; i < CMDBUSERCONTACTTYPEList.length; i++) {
        if (IDCMDBUSERCONTACTTYPE == CMDBUSERCONTACTTYPEList[i].IDCMDBUSERCONTACTTYPE)
            return (i);
    }
    return (-1);
}
//String Function 
Persistence.SMCI.Methods.CMDBUSERCONTACTTYPEListtoStr = function (CMDBUSERCONTACTTYPEList) {
    var Res = Persistence.SMCI.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(CMDBUSERCONTACTTYPEList.length, Longitud, Texto);
        for (Counter = 0; Counter < CMDBUSERCONTACTTYPEList.length; Counter++) {
            var UnCMDBUSERCONTACTTYPE = CMDBUSERCONTACTTYPEList[Counter];
            UnCMDBUSERCONTACTTYPE.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.SMCI.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.SMCI.Methods.StrtoCMDBUSERCONTACTTYPE = function (ProtocoloStr, CMDBUSERCONTACTTYPEList) {
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        CMDBUSERCONTACTTYPEList.length = 0;
        if (Persistence.SMCI.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnCMDBUSERCONTACTTYPE = new Persistence.Demo.Properties.TCMDBUSERCONTACTTYPE(); //Mode new row
                UnCMDBUSERCONTACTTYPE.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.CMDBUSERCONTACTTYPE_ListAdd(CMDBUSERCONTACTTYPEList, UnCMDBUSERCONTACTTYPE);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}
//Socket Function 
Persistence.SMCI.Methods.CMDBUSERCONTACTTYPEListToByte = function (CMDBUSERCONTACTTYPEList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, CMDBUSERCONTACTTYPEList.length - idx);
    for (i = idx; i < CMDBUSERCONTACTTYPEList.length; i++) {
        CMDBUSERCONTACTTYPEList[i].ToByte(MemStream);
    }
}
Persistence.SMCI.Methods.ByteToCMDBUSERCONTACTTYPEList = function (CMDBUSERCONTACTTYPEList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnCMDBUSERCONTACTTYPE = new Persistence.SMCI.Properties.TCMDBUSERCONTACTTYPE();
        CMDBUSERCONTACTTYPEList[i].ToByte(MemStream);
    }
}

