﻿//**********************   PROPERTIES   *****************************************************************************************
Persistence.SMCI.Properties.TSDGROUPSERVICEUSERCI = function () {
    this.IDSDTYPEUSER = 0;
    this.TYPEUSERNAME = "";
    this.IDMDSERVICETYPE = 0;
    this.SERVICETYPENAME = "";
    this.IDCMDBCI = 0;
    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, parseInt(this.IDSDTYPEUSER));
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.TYPEUSERNAME);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, parseInt(this.IDMDSERVICETYPE));
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.SERVICETYPENAME);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDCMDBCI);
    }
    this.ByteTo = function (MemStream) {
        this.IDSDTYPEUSER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.TYPEUSERNAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDMDSERVICETYPE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.SERVICETYPENAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDCMDBCI = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    }
}
//**********************   METHODS server  ********************************************************************************************
Persistence.SMCI.Methods.SDGROUPSERVICEUSERCI_Fill = function (SDGROUPSERVICEUSERCIList, StrIDCMDBCI) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDCMDBCI == "") {
        Param.AddUnknown(UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCI.FieldName, " = " + UsrCfg.InternoAtisNames.MDGROUPUSER.NAME_TABLE + "." + UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCI.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDCMDBCI = " IN (" + StrIDCMDBCI + ")";
        Param.AddUnknown(UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCI.FieldName, StrIDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    try {
        var DS_SDGROUPSERVICEUSERCI = SysCfg.DB.SQL.Methods.OpenDataSet("DataLink", "SDGROUPSERVICEUSER_IDCIPersistence", Param.ToBytes());
        ResErr = DS_SDGROUPSERVICEUSERCI.ResErr;
        if (ResErr.NotError) {
            if (DS_SDGROUPSERVICEUSERCI.DataSet.RecordCount > 0) {
                DS_SDGROUPSERVICEUSERCI.DataSet.First();
                while (!(DS_SDGROUPSERVICEUSERCI.DataSet.Eof)) {
                    var IDCMDBCI = DS_SDGROUPSERVICEUSERCI.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCI.FieldName).asInt32();
                    var IDSDTYPEUSER = DS_SDGROUPSERVICEUSERCI.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDGROUPSERVICEUSER.IDSDTYPEUSER.FieldName).asInt32();
                    var IDMDSERVICETYPE = DS_SDGROUPSERVICEUSERCI.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDGROUPSERVICEUSER.IDMDSERVICETYPE.FieldName).asInt32();
                    //Lento Persistence.SMCI.TSDGROUPSERVICEUSERCI SDGROUPSERVICEUSERCI = Persistence.SMCI.Methods.SDGROUPSERVICEUSERCI_ListSetID(SDGROUPSERVICEUSERCIList, IDCMDBCI, IDSDTYPEUSER, IDMDSERVICETYPE); 
                    var SDGROUPSERVICEUSERCI = new Persistence.SMCI.Properties.TSDGROUPSERVICEUSERCI();
                    SDGROUPSERVICEUSERCI.IDCMDBCI = IDCMDBCI;
                    SDGROUPSERVICEUSERCI.IDSDTYPEUSER = IDSDTYPEUSER;
                    SDGROUPSERVICEUSERCI.TYPEUSERNAME = DS_SDGROUPSERVICEUSERCI.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDTYPEUSER.TYPEUSERNAME.FieldName).asString();
                    SDGROUPSERVICEUSERCI.IDMDSERVICETYPE = IDMDSERVICETYPE;
                    SDGROUPSERVICEUSERCI.SERVICETYPENAME = DS_SDGROUPSERVICEUSERCI.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICETYPE.SERVICETYPENAME.FieldName).asString();
                    //lento Persistence.SMCI.Methods.SDGROUPSERVICEUSERCI_ListAdd(SDGROUPSERVICEUSERCIList, SDGROUPSERVICEUSERCI);
                    //Other
                    SDGROUPSERVICEUSERCIList.push(SDGROUPSERVICEUSERCI);
                    DS_SDGROUPSERVICEUSERCI.DataSet.Next();
                }
            }
            else {
                DS_SDGROUPSERVICEUSERCI.ResErr.NotError = false;
                DS_SDGROUPSERVICEUSERCI.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_SDGROUPSERVICEUSERCI.js Persistence.SMCI.Methods.SDGROUPSERVICEUSERCI_Fill", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.SMCI.Methods.SDGROUPSERVICEUSERCI_GETID = function (SDGROUPSERVICEUSERCI) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCI.FieldName, SDGROUPSERVICEUSERCI.IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var DS_SDGROUPSERVICEUSERCI = SysCfg.DB.SQL.Methods.OpenDataSet("DataLink", "SDGROUPSERVICEUSERCI_GETID", Param.ToBytes());
        ResErr = DS_SDGROUPSERVICEUSERCI.ResErr;
        if (ResErr.NotError) {
            if (DS_SDGROUPSERVICEUSERCI.DataSet.RecordCount > 0) {
                DS_SDGROUPSERVICEUSERCI.DataSet.First();
                SDGROUPSERVICEUSERCI.IDCMDBCI = DS_SDGROUPSERVICEUSERCI.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCI.FieldName).asInt32();
                //Other
                SDGROUPSERVICEUSERCI.IDCMDBCI = DS_SDGROUPSERVICEUSERCI.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCI.FieldName).asInt32();
                SDGROUPSERVICEUSERCI.IDSDTYPEUSER = DS_SDGROUPSERVICEUSERCI.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDGROUPSERVICEUSER.IDSDTYPEUSER.FieldName).asInt32();
                SDGROUPSERVICEUSERCI.TYPEUSERNAME = DS_SDGROUPSERVICEUSERCI.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDTYPEUSER.TYPEUSERNAME.FieldName).asString();
                SDGROUPSERVICEUSERCI.IDMDSERVICETYPE = DS_SDGROUPSERVICEUSERCI.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDGROUPSERVICEUSER.IDMDSERVICETYPE.FieldName).asInt32();
                SDGROUPSERVICEUSERCI.SERVICETYPENAME = DS_SDGROUPSERVICEUSERCI.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICETYPE.SERVICETYPENAME.FieldName).asString();
            }
            else {
                DS_SDGROUPSERVICEUSERCI.ResErr.NotError = false;
                DS_SDGROUPSERVICEUSERCI.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_SDGROUPSERVICEUSERCI.js Persistence.SMCI.Methods.SDGROUPSERVICEUSERCI_GETID", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
//**********************   METHODS   ********************************************************************************************
Persistence.SMCI.Methods.SDGROUPSERVICEUSERCI_ListSetID = function (SDGROUPSERVICEUSERCIList,IDCMDBCI,IDSDTYPEUSER,IDMDSERVICETYPE) {
    for (i = 0; i < SDGROUPSERVICEUSERCIList.length; i++) {
        if ((IDCMDBCI == SDGROUPSERVICEUSERCIList[i].IDCMDBCI) && (IDSDTYPEUSER == SDGROUPSERVICEUSERCIList[i].IDSDTYPEUSER) && (IDMDSERVICETYPE == SDGROUPSERVICEUSERCIList[i].IDMDSERVICETYPE))
            return (SDGROUPSERVICEUSERCIList[i]);
    }
    var UnSDGROUPSERVICEUSERCI = new Persistence.SMCI.Properties.TSDGROUPSERVICEUSERCI();
    UnSDGROUPSERVICEUSERCI.IDCMDBCI = IDCMDBCI;
    UnSDGROUPSERVICEUSERCI.IDSDTYPEUSER = IDSDTYPEUSER;
    UnSDGROUPSERVICEUSERCI.IDMDSERVICETYPE = IDMDSERVICETYPE;
    return (UnSDGROUPSERVICEUSERCI);
}
Persistence.SMCI.Methods.SDGROUPSERVICEUSERCI_ListAdd = function (SDGROUPSERVICEUSERCIList, SDGROUPSERVICEUSERCI) {
    var i = Persistence.SMCI.Methods.SDGROUPSERVICEUSERCI_ListGetIndex(SDGROUPSERVICEUSERCIList, SDGROUPSERVICEUSERCI);
    if (i == -1) SDGROUPSERVICEUSERCIList.push(SDGROUPSERVICEUSERCI);
    else {
        SDGROUPSERVICEUSERCIList[i].IDSDTYPEUSER = SDGROUPSERVICEUSERCI.IDSDTYPEUSER;
        SDGROUPSERVICEUSERCIList[i].TYPEUSERNAME = SDGROUPSERVICEUSERCI.TYPEUSERNAME;
        SDGROUPSERVICEUSERCIList[i].IDMDSERVICETYPE = SDGROUPSERVICEUSERCI.IDMDSERVICETYPE;
        SDGROUPSERVICEUSERCIList[i].SERVICETYPENAME = SDGROUPSERVICEUSERCI.SERVICETYPENAME;
        SDGROUPSERVICEUSERCIList[i].IDCMDBCI = SDGROUPSERVICEUSERCI.IDCMDBCI;
    }
}

//CJRC_26072018
Persistence.SMCI.Methods.SDGROUPSERVICEUSERCI_ListGetIndex = function (SDGROUPSERVICEUSERCIList, Param1, Param2, Param3) {
    var Res = -1
    if (typeof (Param1) == 'number') {
        Res = Persistence.SMCI.Methods.SDGROUPSERVICEUSERCI_ListGetIndexIDCMDBCI(SDGROUPSERVICEUSERCIList, Param1, Param2, Param3) ;

    }
    else {
        Res = Persistence.SMCI.Methods.SDGROUPSERVICEUSERCI_ListGetIndexSDGROUPSERVICEUSERCI(SDGROUPSERVICEUSERCIList, Param1);
    }
    return (Res);
}

Persistence.SMCI.Methods.SDGROUPSERVICEUSERCI_ListGetIndexSDGROUPSERVICEUSERCI = function (SDGROUPSERVICEUSERCIList, SDGROUPSERVICEUSERCI) {
    for (i = 0; i < SDGROUPSERVICEUSERCIList.length; i++) {
        if ((SDGROUPSERVICEUSERCI.IDCMDBCI == SDGROUPSERVICEUSERCIList[i].IDCMDBCI) && (SDGROUPSERVICEUSERCI.IDSDTYPEUSER == SDGROUPSERVICEUSERCIList[i].IDSDTYPEUSER) && (SDGROUPSERVICEUSERCI.IDMDSERVICETYPE == SDGROUPSERVICEUSERCIList[i].IDMDSERVICETYPE))
        return (i);
    }
    return (-1);
}

Persistence.SMCI.Methods.SDGROUPSERVICEUSERCI_ListGetIndexIDCMDBCI = function (SDGROUPSERVICEUSERCIList, IDCMDBCI, IDSDTYPEUSER, IDMDSERVICETYPE) {
    for (i = 0; i < SDGROUPSERVICEUSERCIList.length; i++) {
        if ((IDCMDBCI == SDGROUPSERVICEUSERCIList[i].IDCMDBCI) && (IDSDTYPEUSER == SDGROUPSERVICEUSERCIList[i].IDSDTYPEUSER) && (IDMDSERVICETYPE == SDGROUPSERVICEUSERCIList[i].IDMDSERVICETYPE))
        return (i);
    }
    return (-1);
}
//Socket Function 
Persistence.SMCI.Methods.SDGROUPSERVICEUSERCIListToByte = function (SDGROUPSERVICEUSERCIList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, SDGROUPSERVICEUSERCIList.length - idx);
    for (i = idx; i < SDGROUPSERVICEUSERCIList.length; i++) {
        SDGROUPSERVICEUSERCIList[i].ToByte(MemStream);
    }
}
Persistence.SMCI.Methods.ByteToSDGROUPSERVICEUSERCIList = function (SDGROUPSERVICEUSERCIList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnSDGROUPSERVICEUSERCI = new Persistence.SMCI.Properties.TSDGROUPSERVICEUSERCI();
        SDGROUPSERVICEUSERCIList[i].ToByte(MemStream);
    }
}

