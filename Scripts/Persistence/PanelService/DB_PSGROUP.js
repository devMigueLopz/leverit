﻿//**********************   PROPERTIES   *****************************************************************************************
Persistence.PanelService.Properties.TPSGROUP = function () {
    this.IDPSGROUP = 0;
    this.GROUP_NAME = "";
    this.GROUP_BACKGROUND = "";
    this.GROUP_BORDER = "";
    this.GROUP_DESCRIPTION = "";
    this.GROUP_PATH = "";
    //VIRTUALES
    this.PSMAINList = new Array();
    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDPSGROUP);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.GROUP_NAME);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.GROUP_BACKGROUND);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.GROUP_BORDER);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.GROUP_DESCRIPTION);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.GROUP_PATH);

    }
    this.ByteTo = function (MemStream) {
        this.IDPSGROUP = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.GROUP_NAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.GROUP_BACKGROUND = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.GROUP_BORDER = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.GROUP_DESCRIPTION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.GROUP_PATH = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);

    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDPSGROUP, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.GROUP_NAME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.GROUP_BACKGROUND, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.GROUP_BORDER, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.GROUP_DESCRIPTION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.GROUP_PATH, Longitud, Texto);

    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDPSGROUP = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.GROUP_NAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.GROUP_BACKGROUND = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.GROUP_BORDER = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.GROUP_DESCRIPTION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.GROUP_PATH = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);

    }
}
//**********************   METHODS server  ********************************************************************************************
Persistence.PanelService.Methods.PSGROUP_Fill = function (PSGROUPList, StrIDPSGROUP/*noin IDPSGROUP*/) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    PSGROUPList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDPSGROUP == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.PSGROUP.IDPSGROUP.FieldName, " = " + UsrCfg.InternoAtisNames.PSGROUP.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PSGROUP.IDPSGROUP.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDPSGROUP = " IN (" + StrIDPSGROUP + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.PSGROUP.IDPSGROUP.FieldName, StrIDPSGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //noin Param.AddInt32(UsrCfg.InternoDemoNames.PSGROUP.IDPSGROUP.FieldName, IDPSGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {
        /*
        //********   PSGROUP_GET   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PSGROUP_GET", @"PSGROUP_GET description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   IDPSGROUP, "); 
         UnSQL.SQL.Add(@"   GROUP_NAME, "); 
         UnSQL.SQL.Add(@"   GROUP_BACKGROUND, "); 
         UnSQL.SQL.Add(@"   GROUP_BORDER, "); 
         UnSQL.SQL.Add(@"   GROUP_DESCRIPTION, "); 
         UnSQL.SQL.Add(@"   GROUP_PATH "); 
         UnSQL.SQL.Add(@"   FROM PSGROUP "); 
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_PSGROUP = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "PSGROUP_GET", Param.ToBytes());
        ResErr = DS_PSGROUP.ResErr;
        if (ResErr.NotError) {
            if (DS_PSGROUP.DataSet.RecordCount > 0) {
                DS_PSGROUP.DataSet.First();
                while (!(DS_PSGROUP.DataSet.Eof)) {
                    var PSGROUP = new Persistence.PanelService.Properties.TPSGROUP();
                    PSGROUP.IDPSGROUP = DS_PSGROUP.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSGROUP.IDPSGROUP.FieldName).asInt32();
                    PSGROUP.GROUP_NAME = DS_PSGROUP.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSGROUP.GROUP_NAME.FieldName).asString();
                    PSGROUP.GROUP_BACKGROUND = DS_PSGROUP.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSGROUP.GROUP_BACKGROUND.FieldName).asString();
                    PSGROUP.GROUP_BORDER = DS_PSGROUP.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSGROUP.GROUP_BORDER.FieldName).asString();
                    PSGROUP.GROUP_DESCRIPTION = DS_PSGROUP.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSGROUP.GROUP_DESCRIPTION.FieldName).asText();
                    PSGROUP.GROUP_PATH = DS_PSGROUP.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSGROUP.GROUP_PATH.FieldName).asString();

                    //Other
                    PSGROUPList.push(PSGROUP);
                    DS_PSGROUP.DataSet.Next();
                }
            }
            else {
                DS_PSGROUP.ResErr.NotError = false;
                DS_PSGROUP.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    catch (e){
        SysCfg.Log.Methods.WriteLog("DB_PSGROUP.js Persistence.PanelService.Methods.PSGROUP_Fill", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.PanelService.Methods.PSGROUP_GETID = function (PSGROUP) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.PSGROUP.IDPSGROUP.FieldName, PSGROUP.IDPSGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.PSGROUP.GROUP_NAME.FieldName, PSGROUP.GROUP_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.PSGROUP.GROUP_BACKGROUND.FieldName, PSGROUP.GROUP_BACKGROUND, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.PSGROUP.GROUP_BORDER.FieldName, PSGROUP.GROUP_BORDER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddText(UsrCfg.InternoAtisNames.PSGROUP.GROUP_DESCRIPTION.FieldName, PSGROUP.GROUP_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.PSGROUP.GROUP_PATH.FieldName, PSGROUP.GROUP_PATH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

        /*
        //********   PSGROUP_GETID   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PSGROUP_GETID", @"PSGROUP_GETID description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   IDPSGROUP "); 
         UnSQL.SQL.Add(@"   FROM PSGROUP "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   GROUP_NAME=@[GROUP_NAME] And "); 
         UnSQL.SQL.Add(@"   GROUP_BACKGROUND=@[GROUP_BACKGROUND] And "); 
         UnSQL.SQL.Add(@"   GROUP_BORDER=@[GROUP_BORDER] And "); 
         UnSQL.SQL.Add(@"   GROUP_PATH=@[GROUP_PATH] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_PSGROUP = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "PSGROUP_GETID", Param.ToBytes());
        ResErr = DS_PSGROUP.ResErr;
        if (ResErr.NotError) {
            if (DS_PSGROUP.DataSet.RecordCount > 0) {
                DS_PSGROUP.DataSet.First();
                PSGROUP.IDPSGROUP = DS_PSGROUP.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSGROUP.IDPSGROUP.FieldName).asInt32();
            }
            else {
                DS_PSGROUP.ResErr.NotError = false;
                DS_PSGROUP.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    catch (e){
        SysCfg.Log.Methods.WriteLog("DB_PSGROUP.js Persistence.PanelService.Methods.PSGROUP_GETID", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.PanelService.Methods.PSGROUP_ADD = function (PSGROUP) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.PSGROUP.IDPSGROUP.FieldName, PSGROUP.IDPSGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PSGROUP.GROUP_NAME.FieldName, PSGROUP.GROUP_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PSGROUP.GROUP_BACKGROUND.FieldName, PSGROUP.GROUP_BACKGROUND, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PSGROUP.GROUP_BORDER.FieldName, PSGROUP.GROUP_BORDER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.PSGROUP.GROUP_DESCRIPTION.FieldName, PSGROUP.GROUP_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PSGROUP.GROUP_PATH.FieldName, PSGROUP.GROUP_PATH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        /* 
        //********   PSGROUP_ADD   ************************* 
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PSGROUP_ADD", @"PSGROUP_ADD description."); 
        UnSQL.SQL.Add(@"   INSERT INTO PSGROUP( "); 
         UnSQL.SQL.Add(@"   GROUP_NAME, "); 
         UnSQL.SQL.Add(@"   GROUP_BACKGROUND, "); 
         UnSQL.SQL.Add(@"   GROUP_BORDER, "); 
         UnSQL.SQL.Add(@"   GROUP_DESCRIPTION, "); 
         UnSQL.SQL.Add(@"   GROUP_PATH "); 
         UnSQL.SQL.Add(@"   ) Values ( ");  
        UnSQL.SQL.Add(@"   @[GROUP_NAME], "); 
         UnSQL.SQL.Add(@"   @[GROUP_BACKGROUND], "); 
         UnSQL.SQL.Add(@"   @[GROUP_BORDER], "); 
         UnSQL.SQL.Add(@"   @[GROUP_DESCRIPTION], "); 
         UnSQL.SQL.Add(@"   @[GROUP_PATH] "); 
         UnSQL.SQL.Add(@"   )  "); 
        UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PSGROUP_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.PanelService.Methods.PSGROUP_GETID(PSGROUP);
        }
    }
    catch (e){
        SysCfg.Log.Methods.WriteLog("DB_PSGROUP.js Persistence.PanelService.Methods.PSGROUP_ADD", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.PanelService.Methods.PSGROUP_UPD = function (PSGROUP) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.PSGROUP.IDPSGROUP.FieldName, PSGROUP.IDPSGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PSGROUP.GROUP_NAME.FieldName, PSGROUP.GROUP_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PSGROUP.GROUP_BACKGROUND.FieldName, PSGROUP.GROUP_BACKGROUND, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PSGROUP.GROUP_BORDER.FieldName, PSGROUP.GROUP_BORDER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.PSGROUP.GROUP_DESCRIPTION.FieldName, PSGROUP.GROUP_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PSGROUP.GROUP_PATH.FieldName, PSGROUP.GROUP_PATH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);


        /*   
        //********   PSGROUP_UPD   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PSGROUP_UPD", @"PSGROUP_UPD description.");  
        UnSQL.SQL.Add(@"  UPDATE PSGROUP Set "); 
         UnSQL.SQL.Add(@"   GROUP_NAME=@[GROUP_NAME], "); 
         UnSQL.SQL.Add(@"   GROUP_BACKGROUND=@[GROUP_BACKGROUND], "); 
         UnSQL.SQL.Add(@"   GROUP_BORDER=@[GROUP_BORDER], "); 
         UnSQL.SQL.Add(@"   GROUP_DESCRIPTION=@[GROUP_DESCRIPTION], "); 
         UnSQL.SQL.Add(@"   GROUP_PATH=@[GROUP_PATH] "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDPSGROUP=@[IDPSGROUP] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PSGROUP_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    catch (e){
        SysCfg.Log.Methods.WriteLog("DB_PSGROUP.js Persistence.PanelService.Methods.PSGROUP_UPD", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.PanelService.Methods.PSGROUP_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.PanelService.Methods.PSGROUP_DELIDPSGROUP(/*String StrIDPSGROUP*/Param);
    }
    else {
        Res = Persistence.PanelService.Methods.PSGROUP_DELPSGROUP(Param/*PSGROUPList*/);
    }
    return (Res);
}

Persistence.PanelService.Methods.PSGROUP_DELIDPSGROUP = function (/*String StrIDPSGROUP*/IDPSGROUP) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDPSGROUP == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.PSGROUP.IDPSGROUP.FieldName, " = " + UsrCfg.InternoAtisNames.PSGROUP.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PSGROUP.IDPSGROUP.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDPSGROUP = " IN (" + StrIDPSGROUP + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.PSGROUP.IDPSGROUP.FieldName, StrIDPSGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.PSGROUP.IDPSGROUP.FieldName, IDPSGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   PSGROUP_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PSGROUP_DEL", @"PSGROUP_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE PSGROUP "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDPSGROUP=@[IDPSGROUP]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PSGROUP_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e){
        SysCfg.Log.Methods.WriteLog("DB_PSGROUP.js Persistence.PanelService.Methods.PSGROUP_DEL(IDPSGROUP)", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.PanelService.Methods.PSGROUP_DELPSGROUP = function (PSGROUP/*PSGROUPList*/) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        /*
        StrIDPSGROUP = "";
        StrIDOTRO = "";
        for (var i = 0; i < PSGROUPList.length; i++) {
        StrIDPSGROUP += PSGROUPList[i].IDPSGROUP + ",";
        StrIDOTRO += PSGROUPList[i].IDOTRO + ",";
        }
        StrIDPSGROUP = StrIDPSGROUP.substring(0, StrIDPSGROUP.length - 1);
        StrIDOTRO = StrIDOTRO.substring(0, StrIDOTRO.length - 1);
        
        
        if (StrIDPSGROUP == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.PSGROUP.IDPSGROUP.FieldName, " = " + UsrCfg.InternoAtisNames.PSGROUP.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PSGROUP.IDPSGROUP.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDPSGROUP = " IN (" + StrIDPSGROUP + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.PSGROUP.IDPSGROUP.FieldName, StrIDPSGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/


        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.PSGROUP.IDPSGROUP.FieldName, PSGROUP.IDPSGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*    
        //********   PSGROUP_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PSGROUP_DEL", @"PSGROUP_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE PSGROUP "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDPSGROUP=@[IDPSGROUP]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PSGROUP_DEL", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e){
        SysCfg.Log.Methods.WriteLog("DB_PSGROUP.js Persistence.PanelService.Methods.PSGROUP_DEL(PSGROUP)", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
//**********************   METHODS   ********************************************************************************************
Persistence.PanelService.Methods.PSGROUP_ListSetID = function (PSGROUPList, IDPSGROUP) {
    for (i = 0; i < PSGROUPList.length; i++) {
        if (IDPSGROUP == PSGROUPList[i].IDPSGROUP)
            return (PSGROUPList[i]);
    }
    var UnPSGROUP = new Persistence.Properties.TPSGROUP;
    UnPSGROUP.IDPSGROUP = IDPSGROUP;
    return (UnPSGROUP);
}
Persistence.PanelService.Methods.PSGROUP_ListAdd = function (PSGROUPList, PSGROUP) {
    var i = Persistence.PanelService.Methods.PSGROUP_ListGetIndex(PSGROUPList, PSGROUP);
    if (i == -1) PSGROUPList.push(PSGROUP);
    else {
        PSGROUPList[i].IDPSGROUP = PSGROUP.IDPSGROUP;
        PSGROUPList[i].GROUP_NAME = PSGROUP.GROUP_NAME;
        PSGROUPList[i].GROUP_BACKGROUND = PSGROUP.GROUP_BACKGROUND;
        PSGROUPList[i].GROUP_BORDER = PSGROUP.GROUP_BORDER;
        PSGROUPList[i].GROUP_DESCRIPTION = PSGROUP.GROUP_DESCRIPTION;
        PSGROUPList[i].GROUP_PATH = PSGROUP.GROUP_PATH;

    }
}
//CJRC_26072018
Persistence.PanelService.Methods.PSGROUP_ListGetIndex = function (PSGROUPList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.PanelService.Methods.PSGROUP_ListGetIndexIDPSGROUP(PSGROUPList, Param);

    }
    else {
        Res = Persistence.PanelService.Methods.PSGROUP_ListGetIndexPSGROUP(PSGROUPList, Param);
    }
    return (Res);
}

Persistence.PanelService.Methods.PSGROUP_ListGetIndexPSGROUP = function (PSGROUPList, PSGROUP) {
    for (i = 0; i < PSGROUPList.length; i++) {
        if (PSGROUP.IDPSGROUP == PSGROUPList[i].IDPSGROUP)
            return (i);
    }
    return (-1);
}
Persistence.PanelService.Methods.PSGROUP_ListGetIndexIDPSGROUP = function (PSGROUPList, IDPSGROUP) {
    for (i = 0; i < PSGROUPList.length; i++) {
        if (IDPSGROUP == PSGROUPList[i].IDPSGROUP)
            return (i);
    }
    return (-1);
}
//STRING FUNCTION 
Persistence.PanelService.Methods.PSGROUPListtoStr = function (PSGROUPList) {
    var Res = Persistence.Properties._Version + "(1)0";
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(PSGROUPList.length, Longitud, Texto);
        for (Counter = 0; Counter < PSGROUPList.length; Counter++) {
            var UnPSGROUP = PSGROUPList[Counter];
            UnPSGROUP.ToStr(Longitud, Texto);
        }

        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
}
Persistence.PanelService.Methods.StrtoPSGROUP = function (ProtocoloStr, PSGROUPList) {

    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        PSGROUPList.length = 0;
        if (Persistence.PanelService.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")") - ProtocoloStr.indexOf("(") - 1);
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.Length - ProtocoloStr.indexOf(")") - 1);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnPSGROUP = new Persistence.Properties.TPSGROUP(); //Mode new row
                UnPSGROUP.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.PanelService.PSGROUP_ListAdd(PSGROUPList, UnPSGROUP);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}
//SOCKET FUNCTION 
Persistence.PanelService.Methods.PSGROUPListToByte = function (PSGROUPList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, PSGROUPList.Count - idx);
    for (i = idx; i < PSGROUPList.Count; i++) {
        PSGROUPList[i].ToByte(MemStream);
    }
}
Persistence.PanelService.Methods.ByteToPSGROUPList = function (PSGROUPList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnPSGROUP = new Persistence.Properties.TPSGROUP();
        UnPSGROUP.ByteTo(MemStream);
        Methods.PSGROUP_ListAdd(PSGROUPList, UnPSGROUP);
    }
}
