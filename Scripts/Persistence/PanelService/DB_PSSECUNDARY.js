﻿//**********************   PROPERTIES   *****************************************************************************************
Persistence.PanelService.Properties.TPSSECUNDARY = function () {
	this.IDPSSECUNDARY = 0;
	this.IDPSMAIN = 0;
	this.SECUNDARY_POSITION = 0;
	this.SECUNDARY_ICON = "";
	this.SECUNDARY_TITLE = "";
	this.SECUNDARY_TEXTCOLOR = "";
	this.SECUNDARY_DESCRIPTION = "";
	this.SECUNDARY_PHRASE = "";

	//VISRTUALES
	this.PSMAIN = new Persistence.PanelService.Properties.TPSMAIN();

	//Socket IO Properties
	this.ToByte = function (MemStream) {
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDPSSECUNDARY);
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDPSMAIN);
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.SECUNDARY_POSITION);
		SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.SECUNDARY_ICON);
		SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.SECUNDARY_TITLE);
		SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.SECUNDARY_TEXTCOLOR);
		SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.SECUNDARY_DESCRIPTION);
		SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.SECUNDARY_PHRASE);

	}
	this.ByteTo = function (MemStream) {
		this.IDPSSECUNDARY = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
		this.IDPSMAIN = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
		this.SECUNDARY_POSITION = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
		this.SECUNDARY_ICON = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
		this.SECUNDARY_TITLE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
		this.SECUNDARY_TEXTCOLOR = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
		this.SECUNDARY_DESCRIPTION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
		this.SECUNDARY_PHRASE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);

	}
	//Str IO Properties
	this.ToStr = function (Longitud, Texto) {
		SysCfg.Str.Protocol.WriteInt(this.IDPSSECUNDARY, Longitud, Texto);
		SysCfg.Str.Protocol.WriteInt(this.IDPSMAIN, Longitud, Texto);
		SysCfg.Str.Protocol.WriteInt(this.SECUNDARY_POSITION, Longitud, Texto);
		SysCfg.Str.Protocol.WriteStr(this.SECUNDARY_ICON, Longitud, Texto);
		SysCfg.Str.Protocol.WriteStr(this.SECUNDARY_TITLE, Longitud, Texto);
		SysCfg.Str.Protocol.WriteStr(this.SECUNDARY_TEXTCOLOR, Longitud, Texto);
		SysCfg.Str.Protocol.WriteStr(this.SECUNDARY_DESCRIPTION, Longitud, Texto);
		SysCfg.Str.Protocol.WriteStr(this.SECUNDARY_PHRASE, Longitud, Texto);

	}
	this.StrTo = function (Pos, Index, LongitudArray, Texto) {
		this.IDPSSECUNDARY = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
		this.IDPSMAIN = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
		this.SECUNDARY_POSITION = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
		this.SECUNDARY_ICON = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
		this.SECUNDARY_TITLE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
		this.SECUNDARY_TEXTCOLOR = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
		this.SECUNDARY_DESCRIPTION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
		this.SECUNDARY_PHRASE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);

	}
}

//**********************   METHODS server  ********************************************************************************************
Persistence.PanelService.Methods.PSSECUNDARY_Fill = function (PSSECUNDARYList, StrIDPSSECUNDARY/*noin IDPSSECUNDARY*/) {
	var ResErr = new SysCfg.Error.Properties.TResErr();
	PSSECUNDARYList.length = 0;
	var Param = new SysCfg.Stream.Properties.TParam();
	Param.Inicialize();
	if (StrIDPSSECUNDARY == "") {
		Param.AddUnknown(UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSSECUNDARY.FieldName, " = " + UsrCfg.InternoAtisNames.PSSECUNDARY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSSECUNDARY.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
	}
	else {
		StrIDPSSECUNDARY = " IN (" + StrIDPSSECUNDARY + ")";
		Param.AddUnknown(UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSSECUNDARY.FieldName, StrIDPSSECUNDARY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
	}
	//noin Param.AddInt32(UsrCfg.InternoDemoNames.PSSECUNDARY.IDPSSECUNDARY.FieldName, IDPSSECUNDARY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
	try {
		/*
		//********   PSSECUNDARY_GET   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PSSECUNDARY_GET", @"PSSECUNDARY_GET description.");  
		UnSQL.SQL.Add(@"   SELECT "); 
		 UnSQL.SQL.Add(@"   IDPSSECUNDARY, "); 
		 UnSQL.SQL.Add(@"   IDPSMAIN, "); 
		 UnSQL.SQL.Add(@"   SECUNDARY_POSITION, "); 
		 UnSQL.SQL.Add(@"   SECUNDARY_ICON, "); 
		 UnSQL.SQL.Add(@"   SECUNDARY_TITLE, "); 
		 UnSQL.SQL.Add(@"   SECUNDARY_TEXTCOLOR, "); 
		 UnSQL.SQL.Add(@"   SECUNDARY_DESCRIPTION, "); 
		 UnSQL.SQL.Add(@"   SECUNDARY_PHRASE "); 
		 UnSQL.SQL.Add(@"   FROM PSSECUNDARY "); 
		 UnConfigSQL.SQL.Add(UnSQL); 
		 
		
		*/
		var DS_PSSECUNDARY = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "PSSECUNDARY_GET", Param.ToBytes());
		ResErr = DS_PSSECUNDARY.ResErr;
		if (ResErr.NotError) {
			if (DS_PSSECUNDARY.DataSet.RecordCount > 0) {
				DS_PSSECUNDARY.DataSet.First();
				while (!(DS_PSSECUNDARY.DataSet.Eof)) {
					var PSSECUNDARY = new Persistence.PanelService.Properties.TPSSECUNDARY();
					PSSECUNDARY.IDPSSECUNDARY = DS_PSSECUNDARY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSSECUNDARY.FieldName).asInt32();
					PSSECUNDARY.IDPSMAIN = DS_PSSECUNDARY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSMAIN.FieldName).asInt32();
					PSSECUNDARY.SECUNDARY_POSITION = DS_PSSECUNDARY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_POSITION.FieldName).asInt32();
					PSSECUNDARY.SECUNDARY_ICON = DS_PSSECUNDARY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_ICON.FieldName).asString();
					PSSECUNDARY.SECUNDARY_TITLE = DS_PSSECUNDARY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_TITLE.FieldName).asString();
					PSSECUNDARY.SECUNDARY_TEXTCOLOR = DS_PSSECUNDARY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_TEXTCOLOR.FieldName).asString();
					PSSECUNDARY.SECUNDARY_DESCRIPTION = DS_PSSECUNDARY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_DESCRIPTION.FieldName).asText();
					PSSECUNDARY.SECUNDARY_PHRASE = DS_PSSECUNDARY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_PHRASE.FieldName).asText();

					//Other
					PSSECUNDARYList.push(PSSECUNDARY);
					DS_PSSECUNDARY.DataSet.Next();
				}
			}
			else {
				DS_PSSECUNDARY.ResErr.NotError = false;
				DS_PSSECUNDARY.ResErr.Mesaje = "RECORDCOUNT = 0";
			}
		}
	}
	catch (e){
		SysCfg.Log.Methods.WriteLog("DB_PSMAIN.js Persistence.PanelService.Methods.PSSECUNDARY_Fill", e);
	}
	finally {
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.PanelService.Methods.PSSECUNDARY_GETID = function (PSSECUNDARY) {
	var ResErr = new SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try {
		Param.Inicialize();
		//Other
		Param.AddInt32(UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSSECUNDARY.FieldName, PSSECUNDARY.IDPSSECUNDARY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddInt32(UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSMAIN.FieldName, PSSECUNDARY.IDPSMAIN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddInt32(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_POSITION.FieldName, PSSECUNDARY.SECUNDARY_POSITION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddString(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_ICON.FieldName, PSSECUNDARY.SECUNDARY_ICON, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddString(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_TITLE.FieldName, PSSECUNDARY.SECUNDARY_TITLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddString(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_TEXTCOLOR.FieldName, PSSECUNDARY.SECUNDARY_TEXTCOLOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddText(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_DESCRIPTION.FieldName, PSSECUNDARY.SECUNDARY_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddText(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_PHRASE.FieldName, PSSECUNDARY.SECUNDARY_PHRASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

		/*
		//********   PSSECUNDARY_GETID   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PSSECUNDARY_GETID", @"PSSECUNDARY_GETID description.");  
		UnSQL.SQL.Add(@"   SELECT "); 
		 UnSQL.SQL.Add(@"   IDPSSECUNDARY "); 
		 UnSQL.SQL.Add(@"   FROM PSSECUNDARY "); 
		 UnSQL.SQL.Add(@"   Where "); 
		 UnSQL.SQL.Add(@"   IDPSMAIN=@[IDPSMAIN] And "); 
		 UnSQL.SQL.Add(@"   SECUNDARY_POSITION=@[SECUNDARY_POSITION] And "); 
		 UnSQL.SQL.Add(@"   SECUNDARY_ICON=@[SECUNDARY_ICON] And "); 
		 UnSQL.SQL.Add(@"   SECUNDARY_TITLE=@[SECUNDARY_TITLE] And "); 
		 UnSQL.SQL.Add(@"   SECUNDARY_TEXTCOLOR=@[SECUNDARY_TEXTCOLOR] ");
		 UnConfigSQL.SQL.Add(UnSQL); 
		 
		
		*/
		var DS_PSSECUNDARY = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "PSSECUNDARY_GETID", Param.ToBytes());
		ResErr = DS_PSSECUNDARY.ResErr;
		if (ResErr.NotError) {
			if (DS_PSSECUNDARY.DataSet.RecordCount > 0) {
				DS_PSSECUNDARY.DataSet.First();
				PSSECUNDARY.IDPSSECUNDARY = DS_PSSECUNDARY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSSECUNDARY.FieldName).asInt32();
			}
			else {
				DS_PSSECUNDARY.ResErr.NotError = false;
				DS_PSSECUNDARY.ResErr.Mesaje = "RECORDCOUNT = 0";
			}
		}
	}
	catch (e){
		SysCfg.Log.Methods.WriteLog("DB_PSMAIN.js Persistence.PanelService.Methods.PSSECUNDARY_GETID", e);
	}
	finally {
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.PanelService.Methods.PSSECUNDARY_ADD = function (PSSECUNDARY) {
	var ResErr = new SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try {
		Param.Inicialize();
		//Other
		Param.AddInt32(UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSSECUNDARY.FieldName, PSSECUNDARY.IDPSSECUNDARY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSMAIN.FieldName, PSSECUNDARY.IDPSMAIN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_POSITION.FieldName, PSSECUNDARY.SECUNDARY_POSITION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_ICON.FieldName, PSSECUNDARY.SECUNDARY_ICON, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_TITLE.FieldName, PSSECUNDARY.SECUNDARY_TITLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_TEXTCOLOR.FieldName, PSSECUNDARY.SECUNDARY_TEXTCOLOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddText(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_DESCRIPTION.FieldName, PSSECUNDARY.SECUNDARY_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddText(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_PHRASE.FieldName, PSSECUNDARY.SECUNDARY_PHRASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

		/* 
		//********   PSSECUNDARY_ADD   ************************* 
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PSSECUNDARY_ADD", @"PSSECUNDARY_ADD description."); 
		UnSQL.SQL.Add(@"   INSERT INTO PSSECUNDARY( "); 
		 UnSQL.SQL.Add(@"   IDPSMAIN, "); 
		 UnSQL.SQL.Add(@"   SECUNDARY_POSITION, "); 
		 UnSQL.SQL.Add(@"   SECUNDARY_ICON, "); 
		 UnSQL.SQL.Add(@"   SECUNDARY_TITLE, "); 
		 UnSQL.SQL.Add(@"   SECUNDARY_TEXTCOLOR, "); 
		 UnSQL.SQL.Add(@"   SECUNDARY_DESCRIPTION, "); 
		 UnSQL.SQL.Add(@"   SECUNDARY_PHRASE "); 
		 UnSQL.SQL.Add(@"   ) Values ( ");  
		UnSQL.SQL.Add(@"   @[IDPSMAIN], "); 
		 UnSQL.SQL.Add(@"   @[SECUNDARY_POSITION], "); 
		 UnSQL.SQL.Add(@"   @[SECUNDARY_ICON], "); 
		 UnSQL.SQL.Add(@"   @[SECUNDARY_TITLE], "); 
		 UnSQL.SQL.Add(@"   @[SECUNDARY_TEXTCOLOR], "); 
		 UnSQL.SQL.Add(@"   @[SECUNDARY_DESCRIPTION], "); 
		 UnSQL.SQL.Add(@"   @[SECUNDARY_PHRASE] "); 
		 UnSQL.SQL.Add(@"   )  "); 
		UnConfigSQL.SQL.Add(UnSQL); 
		 
		
		*/
		this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PSSECUNDARY_ADD", Param.ToBytes());
		ResErr = Exec.ResErr;
		if (ResErr.NotError) {
			Persistence.PanelService.Methods.PSSECUNDARY_GETID(PSSECUNDARY);
		}
	}
	catch (e){
		SysCfg.Log.Methods.WriteLog("DB_PSMAIN.js Persistence.PanelService.Methods.PSSECUNDARY_ADD", e);
	}
	finally {
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.PanelService.Methods.PSSECUNDARY_UPD = function (PSSECUNDARY) {
	var ResErr = SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try {
		Param.Inicialize();
		Param.AddInt32(UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSSECUNDARY.FieldName, PSSECUNDARY.IDPSSECUNDARY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSMAIN.FieldName, PSSECUNDARY.IDPSMAIN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_POSITION.FieldName, PSSECUNDARY.SECUNDARY_POSITION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_ICON.FieldName, PSSECUNDARY.SECUNDARY_ICON, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_TITLE.FieldName, PSSECUNDARY.SECUNDARY_TITLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_TEXTCOLOR.FieldName, PSSECUNDARY.SECUNDARY_TEXTCOLOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddText(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_DESCRIPTION.FieldName, PSSECUNDARY.SECUNDARY_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddText(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_PHRASE.FieldName, PSSECUNDARY.SECUNDARY_PHRASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);


		/*   
		//********   PSSECUNDARY_UPD   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PSSECUNDARY_UPD", @"PSSECUNDARY_UPD description.");  
		UnSQL.SQL.Add(@"  UPDATE PSSECUNDARY Set "); 
		 UnSQL.SQL.Add(@"   IDPSMAIN=@[IDPSMAIN], "); 
		 UnSQL.SQL.Add(@"   SECUNDARY_POSITION=@[SECUNDARY_POSITION], "); 
		 UnSQL.SQL.Add(@"   SECUNDARY_ICON=@[SECUNDARY_ICON], "); 
		 UnSQL.SQL.Add(@"   SECUNDARY_TITLE=@[SECUNDARY_TITLE], "); 
		 UnSQL.SQL.Add(@"   SECUNDARY_TEXTCOLOR=@[SECUNDARY_TEXTCOLOR], "); 
		 UnSQL.SQL.Add(@"   SECUNDARY_DESCRIPTION=@[SECUNDARY_DESCRIPTION], "); 
		 UnSQL.SQL.Add(@"   SECUNDARY_PHRASE=@[SECUNDARY_PHRASE] "); 
		 UnSQL.SQL.Add(@"   Where "); 
		 UnSQL.SQL.Add(@"   IDPSSECUNDARY=@[IDPSSECUNDARY] ");
		 UnConfigSQL.SQL.Add(UnSQL); 
		 
		
		*/
		var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PSSECUNDARY_UPD", Param.ToBytes());
		ResErr = Exec.ResErr;
		if (ResErr.NotError) {
			//GET(); 
		}
	}
	catch (e){
		SysCfg.Log.Methods.WriteLog("DB_PSMAIN.js Persistence.PanelService.Methods.PSSECUNDARY_UPD", e);
	}
	finally {
		Param.Destroy();
	}
	return (ResErr);
}

//CJRC_27072018
Persistence.PanelService.Methods.PSSECUNDARY_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.PanelService.Methods.PSSECUNDARY_DELIDPSSECUNDARY(/*String StrIDPSSECUNDARY*/Param);
    }
    else {
        Res = Persistence.PanelService.Methods.PSSECUNDARY_DELPSSECUNDARY(Param/*PSSECUNDARYList*/);
    }
    return (Res);
}

Persistence.PanelService.Methods.PSSECUNDARY_DELIDPSSECUNDARY = function (/*String StrIDPSSECUNDARY*/IDPSSECUNDARY) {
	var ResErr = SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try {
		Param.Inicialize();
		/*if (StrIDPSSECUNDARY == "")
		{
		Param.AddUnknown(UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSSECUNDARY.FieldName, " = " + UsrCfg.InternoAtisNames.PSSECUNDARY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSSECUNDARY.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
		}
		else
		{
		StrIDPSSECUNDARY = " IN (" + StrIDPSSECUNDARY + ")";
		Param.AddUnknown(UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSSECUNDARY.FieldName, StrIDPSSECUNDARY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		}*/
		Param.AddInt32(UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSSECUNDARY.FieldName, IDPSSECUNDARY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		/*
		//********   PSSECUNDARY_DEL   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PSSECUNDARY_DEL", @"PSSECUNDARY_DEL description.");  
		UnSQL.SQL.Add(@"   DELETE PSSECUNDARY "); 
		 UnSQL.SQL.Add(@"   Where "); 
		 UnSQL.SQL.Add(@"   IDPSSECUNDARY=@[IDPSSECUNDARY]   ");
		 UnConfigSQL.SQL.Add(UnSQL); 
		 
		
		*/
		var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PSSECUNDARY_DEL_1", Param.ToBytes());
		ResErr = Exec.ResErr;
	}
	catch (e){
		SysCfg.Log.Methods.WriteLog("DB_PSMAIN.js Persistence.PanelService.Methods.PSSECUNDARY_DEL(IDPSSECUNDARY)", e);
	}
	finally {
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.PanelService.Methods.PSSECUNDARY_DELPSSECUNDARY = function (PSSECUNDARY/*PSSECUNDARYList*/) {
	var ResErr = SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try {
		/*
		StrIDPSSECUNDARY = "";
		StrIDOTRO = "";
		for (var i = 0; i < PSSECUNDARYList.length; i++) {
		StrIDPSSECUNDARY += PSSECUNDARYList[i].IDPSSECUNDARY + ",";
		StrIDOTRO += PSSECUNDARYList[i].IDOTRO + ",";
		}
		StrIDPSSECUNDARY = StrIDPSSECUNDARY.substring(0, StrIDPSSECUNDARY.length - 1);
		StrIDOTRO = StrIDOTRO.substring(0, StrIDOTRO.length - 1);
		
		
		if (StrIDPSSECUNDARY == "")
		{
		Param.AddUnknown(UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSSECUNDARY.FieldName, " = " + UsrCfg.InternoAtisNames.PSSECUNDARY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSSECUNDARY.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
		}
		else
		{
		StrIDPSSECUNDARY = " IN (" + StrIDPSSECUNDARY + ")";
		Param.AddUnknown(UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSSECUNDARY.FieldName, StrIDPSSECUNDARY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		}*/


		Param.Inicialize();
		Param.AddInt32(UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSSECUNDARY.FieldName, PSSECUNDARY.IDPSSECUNDARY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		/*    
		//********   PSSECUNDARY_DEL   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PSSECUNDARY_DEL", @"PSSECUNDARY_DEL description.");  
		UnSQL.SQL.Add(@"   DELETE PSSECUNDARY "); 
		 UnSQL.SQL.Add(@"   Where "); 
		 UnSQL.SQL.Add(@"   IDPSSECUNDARY=@[IDPSSECUNDARY]   ");
		 UnConfigSQL.SQL.Add(UnSQL); 
		 
		
		*/
		var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PSSECUNDARY_DEL", Param.ToBytes());
		ResErr = Exec.ResErr;
	}
	catch (e){
		SysCfg.Log.Methods.WriteLog("DB_PSMAIN.js Persistence.PanelService.Methods.PSSECUNDARY_DEL(PSSECUNDARY)", e);
	}
	finally {
		Param.Destroy();
	}
	return (ResErr);
}

//**********************   METHODS   ********************************************************************************************
Persistence.PanelService.Methods.PSSECUNDARY_ListSetID = function (PSSECUNDARYList, IDPSSECUNDARY) {
	for (i = 0; i < PSSECUNDARYList.length; i++) {
		if (IDPSSECUNDARY == PSSECUNDARYList[i].IDPSSECUNDARY)
			return (PSSECUNDARYList[i]);
	}
	var UnPSSECUNDARY = new Persistence.Properties.TPSSECUNDARY;
	UnPSSECUNDARY.IDPSSECUNDARY = IDPSSECUNDARY;
	return (UnPSSECUNDARY);
}
Persistence.PanelService.Methods.PSSECUNDARY_ListAdd = function (PSSECUNDARYList, PSSECUNDARY) {
	var i = Persistence.PanelService.Methods.PSSECUNDARY_ListGetIndex(PSSECUNDARYList, PSSECUNDARY);
	if (i == -1) PSSECUNDARYList.push(PSSECUNDARY);
	else {
		PSSECUNDARYList[i].IDPSSECUNDARY = PSSECUNDARY.IDPSSECUNDARY;
		PSSECUNDARYList[i].IDPSMAIN = PSSECUNDARY.IDPSMAIN;
		PSSECUNDARYList[i].SECUNDARY_POSITION = PSSECUNDARY.SECUNDARY_POSITION;
		PSSECUNDARYList[i].SECUNDARY_ICON = PSSECUNDARY.SECUNDARY_ICON;
		PSSECUNDARYList[i].SECUNDARY_TITLE = PSSECUNDARY.SECUNDARY_TITLE;
		PSSECUNDARYList[i].SECUNDARY_TEXTCOLOR = PSSECUNDARY.SECUNDARY_TEXTCOLOR;
		PSSECUNDARYList[i].SECUNDARY_DESCRIPTION = PSSECUNDARY.SECUNDARY_DESCRIPTION;
		PSSECUNDARYList[i].SECUNDARY_PHRASE = PSSECUNDARY.SECUNDARY_PHRASE;

	}
}
//CJRC_26072018
Persistence.PanelService.Methods.PSSECUNDARY_ListGetIndex = function (PSSECUNDARYList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.PanelService.Methods.PSSECUNDARY_ListGetIndexIDPSSECUNDARY(PSSECUNDARYList, Param);

    }
    else {
        Res = Persistence.PanelService.Methods.PSSECUNDARY_ListGetIndexPSSECUNDARY(PSSECUNDARYList, Param);
    }
    return (Res);
}

Persistence.PanelService.Methods.PSSECUNDARY_ListGetIndexPSSECUNDARY = function (PSSECUNDARYList, PSSECUNDARY) {
	for (i = 0; i < PSSECUNDARYList.length; i++) {
		if (PSSECUNDARY.IDPSSECUNDARY == PSSECUNDARYList[i].IDPSSECUNDARY)
			return (i);
	}
	return (-1);
}
Persistence.PanelService.Methods.PSSECUNDARY_ListGetIndexIDPSSECUNDARY = function (PSSECUNDARYList, IDPSSECUNDARY) {
	for (i = 0; i < PSSECUNDARYList.length; i++) {
		if (IDPSSECUNDARY == PSSECUNDARYList[i].IDPSSECUNDARY)
			return (i);
	}
	return (-1);
}

//String Function 
Persistence.PanelService.Methods.PSSECUNDARYListtoStr = function (PSSECUNDARYList) {
	var Res = Persistence.Properties._Version + "(1)0";
	var Longitud = new SysCfg.ref("");
	var Texto = new SysCfg.ref("");
	try {
		SysCfg.Str.Protocol.WriteInt(PSSECUNDARYList.length, Longitud, Texto);
		for (Counter = 0; Counter < PSSECUNDARYList.length; Counter++) {
			var UnPSSECUNDARY = PSSECUNDARYList[Counter];
			UnPSSECUNDARY.ToStr(Longitud, Texto);
		}

		Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
		Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
	}
	catch (e) {
		var ResErr = new SysCfg.Error.Properties.TResErr();
		ResErr.NotError = false;
		ResErr.Mesaje = "LIFESTATUStoStr:" + e;
	}
	return Res;
}
Persistence.PanelService.Methods.StrtoPSSECUNDARY = function (ProtocoloStr, PSSECUNDARYList) {

	var Res = false;
	var Longitud, Texto;
	var Index = new SysCfg.ref(0);
	var Pos = new SysCfg.ref(0);
	try {
		PSSECUNDARYList.length = 0;
		if (Persistence.PanelService.Properties._Version == ProtocoloStr.substring(0, 3)) {
			Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")") - ProtocoloStr.indexOf("(") - 1);
			Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.Length - ProtocoloStr.indexOf(")") - 1);
			LongitudArray = Longitud.split(",");
			LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
			for (i = 0; i < LSCount; i++) {
				UnPSSECUNDARY = new Persistence.Properties.TPSSECUNDARY(); //Mode new row
				UnPSSECUNDARY.StrTo(Pos, Index, LongitudArray, Texto);
				Persistence.PanelService.PSSECUNDARY_ListAdd(PSSECUNDARYList, UnPSSECUNDARY);
			}
		}
	}
	catch (e) {
		var ResErr = SysCfg.Error.Properties.TResErr;
		ResErr.NotError = false;
		ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
	}
	return (Res);
}


//Socket Function 
Persistence.PanelService.Methods.PSSECUNDARYListToByte = function (PSSECUNDARYList, MemStream, idx) {
	SysCfg.Stream.Methods.WriteStreamInt32(MemStream, PSSECUNDARYList.Count - idx);
	for (i = idx; i < PSSECUNDARYList.length; i++) {
		PSSECUNDARYList[i].ToByte(MemStream);
	}
}
Persistence.PanelService.Methods.ByteToPSSECUNDARYList = function (PSSECUNDARYList, MemStream) {
	Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
	for (i = 0; i < Count; i++) {
		UnPSSECUNDARY = new Persistence.Properties.TPSSECUNDARY();
		UnPSSECUNDARY.ByteTo(MemStream);
		Methods.PSSECUNDARY_ListAdd(PSSECUNDARYList, UnPSSECUNDARY);
	}
}
////**********************   PROPERTIES   *****************************************************************************************
//Persistence.PanelService.Properties.TPSSECUNDARY = function () {
//    this.IDPSSECUNDARY = 0;
//    this.IDPSMAIN = 0;
//    this.SECUNDARY_POSITION = 0;
//    this.SECUNDARY_ICON = "";
//    this.SECUNDARY_TITLE = "";
//    this.SECUNDARY_DESCRIPTION = "";
//    this.SECUNDARY_PHRASE = "";

//    //VISRTUALES
//    this.PSMAIN = new Persistence.PanelService.Properties.TPSMAIN();

//    //Socket IO Properties
//    this.ToByte = function (MemStream) {
//        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDPSSECUNDARY);
//        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDPSMAIN);
//        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.SECUNDARY_POSITION);
//        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.SECUNDARY_ICON);
//        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.SECUNDARY_TITLE);
//        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.SECUNDARY_DESCRIPTION);
//        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.SECUNDARY_PHRASE);

//    }
//    this.ByteTo = function (MemStream) {
//        this.IDPSSECUNDARY = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
//        this.IDPSMAIN = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
//        this.SECUNDARY_POSITION = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
//        this.SECUNDARY_ICON = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
//        this.SECUNDARY_TITLE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
//        this.SECUNDARY_DESCRIPTION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
//        this.SECUNDARY_PHRASE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);

//    }
//    //Str IO Properties
//    this.ToStr = function (Longitud, Texto) {
//        SysCfg.Str.Protocol.WriteInt(this.IDPSSECUNDARY, Longitud, Texto);
//        SysCfg.Str.Protocol.WriteInt(this.IDPSMAIN, Longitud, Texto);
//        SysCfg.Str.Protocol.WriteInt(this.SECUNDARY_POSITION, Longitud, Texto);
//        SysCfg.Str.Protocol.WriteStr(this.SECUNDARY_ICON, Longitud, Texto);
//        SysCfg.Str.Protocol.WriteStr(this.SECUNDARY_TITLE, Longitud, Texto);
//        SysCfg.Str.Protocol.WriteStr(this.SECUNDARY_DESCRIPTION, Longitud, Texto);
//        SysCfg.Str.Protocol.WriteStr(this.SECUNDARY_PHRASE, Longitud, Texto);

//    }
//    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
//        this.IDPSSECUNDARY = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
//        this.IDPSMAIN = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
//        this.SECUNDARY_POSITION = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
//        this.SECUNDARY_ICON = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
//        this.SECUNDARY_TITLE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
//        this.SECUNDARY_DESCRIPTION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
//        this.SECUNDARY_PHRASE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);

//    }
//}

////**********************   METHODS server  ********************************************************************************************
//Persistence.PanelService.Methods.PSSECUNDARY_Fill = function (PSSECUNDARYList, StrIDPSSECUNDARY/*noin IDPSSECUNDARY*/) {
//    var ResErr = new SysCfg.Error.Properties.TResErr();
//    PSSECUNDARYList.length = 0;
//    var Param = new SysCfg.Stream.Properties.TParam();
//    Param.Inicialize();
//    if (StrIDPSSECUNDARY == "") {
//        Param.AddUnknown(UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSSECUNDARY.FieldName, " = " + UsrCfg.InternoAtisNames.PSSECUNDARY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSSECUNDARY.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
//    }
//    else {
//        StrIDPSSECUNDARY = " IN (" + StrIDPSSECUNDARY + ")";
//        Param.AddUnknown(UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSSECUNDARY.FieldName, StrIDPSSECUNDARY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
//    }
//    //noin Param.AddInt32(UsrCfg.InternoDemoNames.PSSECUNDARY.IDPSSECUNDARY.FieldName, IDPSSECUNDARY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
//    try {
//        /*
//        //********   PSSECUNDARY_GET   *************************  
//        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PSSECUNDARY_GET", @"PSSECUNDARY_GET description.");  
//        UnSQL.SQL.Add(@"   SELECT "); 
//         UnSQL.SQL.Add(@"   IDPSSECUNDARY, "); 
//         UnSQL.SQL.Add(@"   IDPSMAIN, "); 
//         UnSQL.SQL.Add(@"   SECUNDARY_POSITION, "); 
//         UnSQL.SQL.Add(@"   SECUNDARY_ICON, "); 
//         UnSQL.SQL.Add(@"   SECUNDARY_TITLE, "); 
//         UnSQL.SQL.Add(@"   SECUNDARY_DESCRIPTION, "); 
//         UnSQL.SQL.Add(@"   SECUNDARY_PHRASE "); 
//         UnSQL.SQL.Add(@"   FROM PSSECUNDARY "); 
//         UnConfigSQL.SQL.Add(UnSQL); 
		 
		
//        */
//        var DS_PSSECUNDARY = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "PSSECUNDARY_GET", Param.ToBytes());
//        ResErr = DS_PSSECUNDARY.ResErr;
//        if (ResErr.NotError) {
//            if (DS_PSSECUNDARY.DataSet.RecordCount > 0) {
//                DS_PSSECUNDARY.DataSet.First();
//                while (!(DS_PSSECUNDARY.DataSet.Eof)) {
//                    var PSSECUNDARY = new Persistence.PanelService.Properties.TPSSECUNDARY();
//                    PSSECUNDARY.IDPSSECUNDARY = DS_PSSECUNDARY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSSECUNDARY.FieldName).asInt32();
//                    PSSECUNDARY.IDPSMAIN = DS_PSSECUNDARY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSMAIN.FieldName).asInt32();
//                    PSSECUNDARY.SECUNDARY_POSITION = DS_PSSECUNDARY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_POSITION.FieldName).asInt32();
//                    PSSECUNDARY.SECUNDARY_ICON = DS_PSSECUNDARY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_ICON.FieldName).asString();
//                    PSSECUNDARY.SECUNDARY_TITLE = DS_PSSECUNDARY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_TITLE.FieldName).asString();
//                    PSSECUNDARY.SECUNDARY_DESCRIPTION = DS_PSSECUNDARY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_DESCRIPTION.FieldName).asString();
//                    PSSECUNDARY.SECUNDARY_PHRASE = DS_PSSECUNDARY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_PHRASE.FieldName).asString();

//                    //Other
//                    PSSECUNDARYList.push(PSSECUNDARY);
//                    DS_PSSECUNDARY.DataSet.Next();
//                }
//            }
//            else {
//                DS_PSSECUNDARY.ResErr.NotError = false;
//                DS_PSSECUNDARY.ResErr.Mesaje = "RECORDCOUNT = 0";
//            }
//        }
//    }
//    finally {
//        Param.Destroy();
//    }
//    return (ResErr);
//}
//Persistence.PanelService.Methods.PSSECUNDARY_GETID = function (PSSECUNDARY) {
//    var ResErr = new SysCfg.Error.Properties.TResErr();
//    var Param = new SysCfg.Stream.Properties.TParam();
//    try {
//        Param.Inicialize();
//        //Other
//        Param.AddInt32(UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSSECUNDARY.FieldName, PSSECUNDARY.IDPSSECUNDARY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
//        Param.AddInt32(UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSMAIN.FieldName, PSSECUNDARY.IDPSMAIN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
//        Param.AddInt32(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_POSITION.FieldName, PSSECUNDARY.SECUNDARY_POSITION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
//        Param.AddString(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_ICON.FieldName, PSSECUNDARY.SECUNDARY_ICON, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
//        Param.AddString(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_TITLE.FieldName, PSSECUNDARY.SECUNDARY_TITLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
//        Param.AddString(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_DESCRIPTION.FieldName, PSSECUNDARY.SECUNDARY_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
//        Param.AddString(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_PHRASE.FieldName, PSSECUNDARY.SECUNDARY_PHRASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

//        /*
//        //********   PSSECUNDARY_GETID   *************************  
//        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PSSECUNDARY_GETID", @"PSSECUNDARY_GETID description.");  
//        UnSQL.SQL.Add(@"   SELECT "); 
//         UnSQL.SQL.Add(@"   IDPSSECUNDARY "); 
//         UnSQL.SQL.Add(@"   FROM PSSECUNDARY "); 
//         UnSQL.SQL.Add(@"   Where "); 
//         UnSQL.SQL.Add(@"   IDPSMAIN=@[IDPSMAIN] And "); 
//         UnSQL.SQL.Add(@"   SECUNDARY_POSITION=@[SECUNDARY_POSITION] And "); 
//         UnSQL.SQL.Add(@"   SECUNDARY_ICON=@[SECUNDARY_ICON] And "); 
//         UnSQL.SQL.Add(@"   SECUNDARY_TITLE=@[SECUNDARY_TITLE] ");
//         UnConfigSQL.SQL.Add(UnSQL); 
		 
		
//        */
//        var DS_PSSECUNDARY = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "PSSECUNDARY_GETID", Param.ToBytes());
//        ResErr = DS_PSSECUNDARY.ResErr;
//        if (ResErr.NotError) {
//            if (DS_PSSECUNDARY.DataSet.RecordCount > 0) {
//                DS_PSSECUNDARY.DataSet.First();
//                PSSECUNDARY.IDPSSECUNDARY = DS_PSSECUNDARY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSSECUNDARY.FieldName).asInt32();
//            }
//            else {
//                DS_PSSECUNDARY.ResErr.NotError = false;
//                DS_PSSECUNDARY.ResErr.Mesaje = "RECORDCOUNT = 0";
//            }
//        }
//    }
//    finally {
//        Param.Destroy();
//    }
//    return (ResErr);
//}
//Persistence.PanelService.Methods.PSSECUNDARY_ADD = function (PSSECUNDARY) {
//    var ResErr = new SysCfg.Error.Properties.TResErr();
//    var Param = new SysCfg.Stream.Properties.TParam();
//    try {
//        Param.Inicialize();
//        //Other
//        Param.AddInt32(UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSSECUNDARY.FieldName, PSSECUNDARY.IDPSSECUNDARY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
//        Param.AddInt32(UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSMAIN.FieldName, PSSECUNDARY.IDPSMAIN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
//        Param.AddInt32(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_POSITION.FieldName, PSSECUNDARY.SECUNDARY_POSITION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
//        Param.AddString(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_ICON.FieldName, PSSECUNDARY.SECUNDARY_ICON, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
//        Param.AddString(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_TITLE.FieldName, PSSECUNDARY.SECUNDARY_TITLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
//        Param.AddString(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_DESCRIPTION.FieldName, PSSECUNDARY.SECUNDARY_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
//        Param.AddString(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_PHRASE.FieldName, PSSECUNDARY.SECUNDARY_PHRASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

//        /* 
//        //********   PSSECUNDARY_ADD   ************************* 
//        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PSSECUNDARY_ADD", @"PSSECUNDARY_ADD description."); 
//        UnSQL.SQL.Add(@"   INSERT INTO PSSECUNDARY( "); 
//         UnSQL.SQL.Add(@"   IDPSMAIN, "); 
//         UnSQL.SQL.Add(@"   SECUNDARY_POSITION, "); 
//         UnSQL.SQL.Add(@"   SECUNDARY_ICON, "); 
//         UnSQL.SQL.Add(@"   SECUNDARY_TITLE, "); 
//         UnSQL.SQL.Add(@"   SECUNDARY_DESCRIPTION, "); 
//         UnSQL.SQL.Add(@"   SECUNDARY_PHRASE "); 
//         UnSQL.SQL.Add(@"   ) Values ( ");  
//        UnSQL.SQL.Add(@"   @[IDPSMAIN], "); 
//         UnSQL.SQL.Add(@"   @[SECUNDARY_POSITION], "); 
//         UnSQL.SQL.Add(@"   @[SECUNDARY_ICON], "); 
//         UnSQL.SQL.Add(@"   @[SECUNDARY_TITLE], "); 
//         UnSQL.SQL.Add(@"   @[SECUNDARY_DESCRIPTION], "); 
//         UnSQL.SQL.Add(@"   @[SECUNDARY_PHRASE] "); 
//         UnSQL.SQL.Add(@"   )  "); 
//        UnConfigSQL.SQL.Add(UnSQL); 
		 
		
//        */
//        this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PSSECUNDARY_ADD", Param.ToBytes());
//        ResErr = Exec.ResErr;
//        if (ResErr.NotError) {
//            Persistence.PanelService.Methods.PSSECUNDARY_GETID(PSSECUNDARY);
//        }
//    }
//    finally {
//        Param.Destroy();
//    }
//    return (ResErr);
//}
//Persistence.PanelService.Methods.PSSECUNDARY_UPD = function (PSSECUNDARY) {
//    var ResErr = SysCfg.Error.Properties.TResErr();
//    var Param = new SysCfg.Stream.Properties.TParam();
//    try {
//        Param.Inicialize();
//        Param.AddInt32(UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSSECUNDARY.FieldName, PSSECUNDARY.IDPSSECUNDARY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
//        Param.AddInt32(UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSMAIN.FieldName, PSSECUNDARY.IDPSMAIN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
//        Param.AddInt32(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_POSITION.FieldName, PSSECUNDARY.SECUNDARY_POSITION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
//        Param.AddString(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_ICON.FieldName, PSSECUNDARY.SECUNDARY_ICON, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
//        Param.AddString(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_TITLE.FieldName, PSSECUNDARY.SECUNDARY_TITLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
//        Param.AddString(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_DESCRIPTION.FieldName, PSSECUNDARY.SECUNDARY_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
//        Param.AddString(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_PHRASE.FieldName, PSSECUNDARY.SECUNDARY_PHRASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);


//        /*   
//        //********   PSSECUNDARY_UPD   *************************  
//        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PSSECUNDARY_UPD", @"PSSECUNDARY_UPD description.");  
//        UnSQL.SQL.Add(@"  UPDATE PSSECUNDARY Set "); 
//         UnSQL.SQL.Add(@"   IDPSMAIN=@[IDPSMAIN], "); 
//         UnSQL.SQL.Add(@"   SECUNDARY_POSITION=@[SECUNDARY_POSITION], "); 
//         UnSQL.SQL.Add(@"   SECUNDARY_ICON=@[SECUNDARY_ICON], "); 
//         UnSQL.SQL.Add(@"   SECUNDARY_TITLE=@[SECUNDARY_TITLE], "); 
//         UnSQL.SQL.Add(@"   SECUNDARY_DESCRIPTION=@[SECUNDARY_DESCRIPTION], "); 
//         UnSQL.SQL.Add(@"   SECUNDARY_PHRASE=@[SECUNDARY_PHRASE] "); 
//         UnSQL.SQL.Add(@"   Where "); 
//         UnSQL.SQL.Add(@"   IDPSSECUNDARY=@[IDPSSECUNDARY] ");
//         UnConfigSQL.SQL.Add(UnSQL); 
		 
		
//        */
//        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PSSECUNDARY_UPD", Param.ToBytes());
//        ResErr = Exec.ResErr;
//        if (ResErr.NotError) {
//            //GET(); 
//        }
//    }
//    finally {
//        Param.Destroy();
//    }
//    return (ResErr);
//}
//Persistence.PanelService.Methods.PSSECUNDARY_DEL = function (/*String StrIDPSSECUNDARY*/IDPSSECUNDARY) {
//    var ResErr = SysCfg.Error.Properties.TResErr();
//    var Param = new SysCfg.Stream.Properties.TParam();
//    try {
//        Param.Inicialize();
//        /*if (StrIDPSSECUNDARY == "")
//        {
//        Param.AddUnknown(UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSSECUNDARY.FieldName, " = " + UsrCfg.InternoAtisNames.PSSECUNDARY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSSECUNDARY.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
//        }
//        else
//        {
//        StrIDPSSECUNDARY = " IN (" + StrIDPSSECUNDARY + ")";
//        Param.AddUnknown(UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSSECUNDARY.FieldName, StrIDPSSECUNDARY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
//        }*/
//        Param.AddInt32(UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSSECUNDARY.FieldName, IDPSSECUNDARY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
//        /*
//        //********   PSSECUNDARY_DEL   *************************  
//        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PSSECUNDARY_DEL", @"PSSECUNDARY_DEL description.");  
//        UnSQL.SQL.Add(@"   DELETE PSSECUNDARY "); 
//         UnSQL.SQL.Add(@"   Where "); 
//         UnSQL.SQL.Add(@"   IDPSSECUNDARY=@[IDPSSECUNDARY]   ");
//         UnConfigSQL.SQL.Add(UnSQL); 
		 
		
//        */
//        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PSSECUNDARY_DEL_1", Param.ToBytes());
//        ResErr = Exec.ResErr;
//    }
//    finally {
//        Param.Destroy();
//    }
//    return (ResErr);
//}
//Persistence.PanelService.Methods.PSSECUNDARY_DEL = function (PSSECUNDARY/*PSSECUNDARYList*/) {
//    var ResErr = SysCfg.Error.Properties.TResErr();
//    var Param = new SysCfg.Stream.Properties.TParam();
//    try {
//        /*
//        StrIDPSSECUNDARY = "";
//        StrIDOTRO = "";
//        for (var i = 0; i < PSSECUNDARYList.length; i++) {
//        StrIDPSSECUNDARY += PSSECUNDARYList[i].IDPSSECUNDARY + ",";
//        StrIDOTRO += PSSECUNDARYList[i].IDOTRO + ",";
//        }
//        StrIDPSSECUNDARY = StrIDPSSECUNDARY.substring(0, StrIDPSSECUNDARY.length - 1);
//        StrIDOTRO = StrIDOTRO.substring(0, StrIDOTRO.length - 1);
		
		
//        if (StrIDPSSECUNDARY == "")
//        {
//        Param.AddUnknown(UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSSECUNDARY.FieldName, " = " + UsrCfg.InternoAtisNames.PSSECUNDARY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSSECUNDARY.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
//        }
//        else
//        {
//        StrIDPSSECUNDARY = " IN (" + StrIDPSSECUNDARY + ")";
//        Param.AddUnknown(UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSSECUNDARY.FieldName, StrIDPSSECUNDARY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
//        }*/


//        Param.Inicialize();
//        Param.AddInt32(UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSSECUNDARY.FieldName, PSSECUNDARY.IDPSSECUNDARY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
//        /*    
//        //********   PSSECUNDARY_DEL   *************************  
//        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PSSECUNDARY_DEL", @"PSSECUNDARY_DEL description.");  
//        UnSQL.SQL.Add(@"   DELETE PSSECUNDARY "); 
//         UnSQL.SQL.Add(@"   Where "); 
//         UnSQL.SQL.Add(@"   IDPSSECUNDARY=@[IDPSSECUNDARY]   ");
//         UnConfigSQL.SQL.Add(UnSQL); 
		 
		
//        */
//        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PSSECUNDARY_DEL", Param.ToBytes());
//        ResErr = Exec.ResErr;
//    }
//    finally {
//        Param.Destroy();
//    }
//    return (ResErr);
//}

////**********************   METHODS   ********************************************************************************************
//Persistence.PanelService.Methods.PSSECUNDARY_ListSetID = function (PSSECUNDARYList, IDPSSECUNDARY) {
//    for (i = 0; i < PSSECUNDARYList.length; i++) {
//        if (IDPSSECUNDARY == PSSECUNDARYList[i].IDPSSECUNDARY)
//            return (PSSECUNDARYList[i]);
//    }
//    var UnPSSECUNDARY = new Persistence.Properties.TPSSECUNDARY;
//    UnPSSECUNDARY.IDPSSECUNDARY = IDPSSECUNDARY;
//    return (UnPSSECUNDARY);
//}
//Persistence.PanelService.Methods.PSSECUNDARY_ListAdd = function (PSSECUNDARYList, PSSECUNDARY) {
//    var i = Persistence.PanelService.Methods.PSSECUNDARY_ListGetIndex(PSSECUNDARYList, PSSECUNDARY);
//    if (i == -1) PSSECUNDARYList.push(PSSECUNDARY);
//    else {
//        PSSECUNDARYList[i].IDPSSECUNDARY = PSSECUNDARY.IDPSSECUNDARY;
//        PSSECUNDARYList[i].IDPSMAIN = PSSECUNDARY.IDPSMAIN;
//        PSSECUNDARYList[i].SECUNDARY_POSITION = PSSECUNDARY.SECUNDARY_POSITION;
//        PSSECUNDARYList[i].SECUNDARY_ICON = PSSECUNDARY.SECUNDARY_ICON;
//        PSSECUNDARYList[i].SECUNDARY_TITLE = PSSECUNDARY.SECUNDARY_TITLE;
//        PSSECUNDARYList[i].SECUNDARY_DESCRIPTION = PSSECUNDARY.SECUNDARY_DESCRIPTION;
//        PSSECUNDARYList[i].SECUNDARY_PHRASE = PSSECUNDARY.SECUNDARY_PHRASE;

//    }
//}
//CJRC_26072018
//Persistence.PanelService.Methods.PSSECUNDARY_ListGetIndex = function (PSSECUNDARYList, Param) {
//    var Res = -1
//    if (typeof (Param) == 'number') {
//        Res = Persistence.PanelService.Methods.PSSECUNDARY_ListGetIndexIDPSSECUNDARY(PSSECUNDARYList, Param);

//    }
//    else {
//        Res = Persistence.PanelService.Methods.PSSECUNDARY_ListGetIndexPSSECUNDARY(PSSECUNDARYList, Param);
//    }
//    return (Res);
//}

//Persistence.PanelService.Methods.PSSECUNDARY_ListGetIndexPSSECUNDARY = function (PSSECUNDARYList, PSSECUNDARY) {
//    for (i = 0; i < PSSECUNDARYList.length; i++) {
//        if (PSSECUNDARY.IDPSSECUNDARY == PSSECUNDARYList[i].IDPSSECUNDARY)
//            return (i);
//    }
//    return (-1);
//}
//Persistence.PanelService.Methods.PSSECUNDARY_ListGetIndexIDPSSECUNDARY = function (PSSECUNDARYList, IDPSSECUNDARY) {
//    for (i = 0; i < PSSECUNDARYList.length; i++) {
//        if (IDPSSECUNDARY == PSSECUNDARYList[i].IDPSSECUNDARY)
//            return (i);
//    }
//    return (-1);
//}

////String Function 
//Persistence.PanelService.Methods.PSSECUNDARYListtoStr = function (PSSECUNDARYList) {
//    var Res = Persistence.PanelService.Properties._Version + "(1)0";
//    var Longitud = new SysCfg.ref("");
//    var Texto = new SysCfg.ref("");
//    try {
//        SysCfg.Str.Protocol.WriteInt(PSSECUNDARYList.length, Longitud, Texto);
//        for (Counter = 0; Counter < PSSECUNDARYList.length; Counter++) {
//            var UnPSSECUNDARY = PSSECUNDARYList[Counter];
//            UnPSSECUNDARY.ToStr(Longitud, Texto);
//        }

//        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
//        Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
//    }
//    catch (e) {
//        var ResErr = new SysCfg.Error.Properties.TResErr();
//        ResErr.NotError = false;
//        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
//    }
//    return Res;
//}
//Persistence.PanelService.Methods.StrtoPSSECUNDARY = function (ProtocoloStr, PSSECUNDARYList) {

//    var Res = false;
//    var Longitud, Texto;
//    var Index = new SysCfg.ref(0);
//    var Pos = new SysCfg.ref(0);
//    try {
//        PSSECUNDARYList.length = 0;
//        if (Persistence.PanelService.Properties._Version == ProtocoloStr.substring(0, 3)) {
//            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")") - ProtocoloStr.indexOf("(") - 1);
//            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.Length - ProtocoloStr.indexOf(")") - 1);
//            LongitudArray = Longitud.split(",");
//            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
//            for (i = 0; i < LSCount; i++) {
//                UnPSSECUNDARY = new Persistence.Properties.TPSSECUNDARY(); //Mode new row
//                UnPSSECUNDARY.StrTo(Pos, Index, LongitudArray, Texto);
//                Persistence.PanelService.PSSECUNDARY_ListAdd(PSSECUNDARYList, UnPSSECUNDARY);
//            }
//        }
//    }
//    catch (e) {
//        var ResErr = SysCfg.Error.Properties.TResErr;
//        ResErr.NotError = false;
//        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
//    }
//    return (Res);
//}


////Socket Function 
//Persistence.PanelService.Methods.PSSECUNDARYListToByte = function (PSSECUNDARYList, MemStream, idx) {
//    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, PSSECUNDARYList.Count - idx);
//    for (i = idx; i < PSSECUNDARYList.length; i++) {
//        PSSECUNDARYList[i].ToByte(MemStream);
//    }
//}
//Persistence.PanelService.Methods.ByteToPSSECUNDARYList = function (PSSECUNDARYList, MemStream) {
//    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
//    for (i = 0; i < Count; i++) {
//        UnPSSECUNDARY = new Persistence.Properties.TPSSECUNDARY();
//        UnPSSECUNDARY.ByteTo(MemStream);
//        Methods.PSSECUNDARY_ListAdd(PSSECUNDARYList, UnPSSECUNDARY);
//    }
//}
