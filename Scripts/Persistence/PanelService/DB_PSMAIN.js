﻿//**********************   PROPERTIES   *****************************************************************************************
Persistence.PanelService.Properties.TPSMAIN = function () {
    this.IDPSMAIN = 0;
    this.IDPSGROUP = 0;
    this.MAIN_POSITION = 0;
    this.MAIN_ICON = "";
    this.MAIN_TITLE = "";
    this.MAIN_TITLECOLOR = "";
    this.MAIN_BACKGROUNDACTIVE = "";
    this.MAIN_TITLEACTIVECOLOR = "";
    this.MAIN_BORDERACTIVE = "";
    this.MAIN_BACKGROUNDHOVER = "";
    this.MAIN_TITLEHOVERCOLOR = "";
    this.MAIN_DESCRIPTION = "";

    //VISRTUALES
    this.PSGROUP = new Persistence.PanelService.Properties.TPSGROUP();
    this.PSSECUNDARYList = new Array();

    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDPSMAIN);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDPSGROUP);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.MAIN_POSITION);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.MAIN_ICON);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.MAIN_TITLE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.MAIN_TITLECOLOR);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.MAIN_BACKGROUNDACTIVE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.MAIN_TITLEACTIVECOLOR);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.MAIN_BORDERACTIVE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.MAIN_BACKGROUNDHOVER);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.MAIN_TITLEHOVERCOLOR);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.MAIN_DESCRIPTION);

    }
    this.ByteTo = function (MemStream) {
        this.IDPSMAIN = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDPSGROUP = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.MAIN_POSITION = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.MAIN_ICON = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.MAIN_TITLE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.MAIN_TITLECOLOR = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.MAIN_BACKGROUNDACTIVE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.MAIN_TITLEACTIVECOLOR = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.MAIN_BORDERACTIVE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.MAIN_BACKGROUNDHOVER = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.MAIN_TITLEHOVERCOLOR = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.MAIN_DESCRIPTION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);

    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDPSMAIN, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDPSGROUP, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.MAIN_POSITION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.MAIN_ICON, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.MAIN_TITLE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.MAIN_TITLECOLOR, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.MAIN_BACKGROUNDACTIVE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.MAIN_TITLEACTIVECOLOR, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.MAIN_BORDERACTIVE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.MAIN_BACKGROUNDHOVER, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.MAIN_TITLEHOVERCOLOR, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.MAIN_DESCRIPTION, Longitud, Texto);

    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDPSMAIN = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDPSGROUP = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.MAIN_POSITION = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.MAIN_ICON = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.MAIN_TITLE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.MAIN_TITLECOLOR = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.MAIN_BACKGROUNDACTIVE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.MAIN_TITLEACTIVECOLOR = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.MAIN_BORDERACTIVE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.MAIN_BACKGROUNDHOVER = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.MAIN_TITLEHOVERCOLOR = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.MAIN_DESCRIPTION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);

    }
}

//**********************   METHODS server  ********************************************************************************************
Persistence.PanelService.Methods.PSMAIN_Fill = function (PSMAINList, StrIDPSMAIN/*noin IDPSMAIN*/) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    PSMAINList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDPSMAIN == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.PSMAIN.IDPSMAIN.FieldName, " = " + UsrCfg.InternoAtisNames.PSMAIN.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PSMAIN.IDPSMAIN.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDPSMAIN = " IN (" + StrIDPSMAIN + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.PSMAIN.IDPSMAIN.FieldName, StrIDPSMAIN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //noin Param.AddInt32(UsrCfg.InternoDemoNames.PSMAIN.IDPSMAIN.FieldName, IDPSMAIN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {
        /*
        //********   PSMAIN_GET   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PSMAIN_GET", @"PSMAIN_GET description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   IDPSMAIN, "); 
         UnSQL.SQL.Add(@"   IDPSGROUP, "); 
         UnSQL.SQL.Add(@"   MAIN_POSITION, "); 
         UnSQL.SQL.Add(@"   MAIN_ICON, "); 
         UnSQL.SQL.Add(@"   MAIN_TITLE, "); 
         UnSQL.SQL.Add(@"   MAIN_TITLECOLOR, "); 
         UnSQL.SQL.Add(@"   MAIN_BACKGROUNDACTIVE, "); 
         UnSQL.SQL.Add(@"   MAIN_TITLEACTIVECOLOR, "); 
         UnSQL.SQL.Add(@"   MAIN_BORDERACTIVE, "); 
         UnSQL.SQL.Add(@"   MAIN_BACKGROUNDHOVER, "); 
         UnSQL.SQL.Add(@"   MAIN_TITLEHOVERCOLOR, "); 
         UnSQL.SQL.Add(@"   MAIN_DESCRIPTION "); 
         UnSQL.SQL.Add(@"   FROM PSMAIN "); 
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_PSMAIN = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "PSMAIN_GET", Param.ToBytes());
        ResErr = DS_PSMAIN.ResErr;
        if (ResErr.NotError) {
            if (DS_PSMAIN.DataSet.RecordCount > 0) {
                DS_PSMAIN.DataSet.First();
                while (!(DS_PSMAIN.DataSet.Eof)) {
                    var PSMAIN = new Persistence.PanelService.Properties.TPSMAIN();
                    PSMAIN.IDPSMAIN = DS_PSMAIN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSMAIN.IDPSMAIN.FieldName).asInt32();
                    PSMAIN.IDPSGROUP = DS_PSMAIN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSMAIN.IDPSGROUP.FieldName).asInt32();
                    PSMAIN.MAIN_POSITION = DS_PSMAIN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSMAIN.MAIN_POSITION.FieldName).asInt32();
                    PSMAIN.MAIN_ICON = DS_PSMAIN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSMAIN.MAIN_ICON.FieldName).asString();
                    PSMAIN.MAIN_TITLE = DS_PSMAIN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSMAIN.MAIN_TITLE.FieldName).asString();
                    PSMAIN.MAIN_TITLECOLOR = DS_PSMAIN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSMAIN.MAIN_TITLECOLOR.FieldName).asString();
                    PSMAIN.MAIN_BACKGROUNDACTIVE = DS_PSMAIN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSMAIN.MAIN_BACKGROUNDACTIVE.FieldName).asString();
                    PSMAIN.MAIN_TITLEACTIVECOLOR = DS_PSMAIN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSMAIN.MAIN_TITLEACTIVECOLOR.FieldName).asString();
                    PSMAIN.MAIN_BORDERACTIVE = DS_PSMAIN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSMAIN.MAIN_BORDERACTIVE.FieldName).asString();
                    PSMAIN.MAIN_BACKGROUNDHOVER = DS_PSMAIN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSMAIN.MAIN_BACKGROUNDHOVER.FieldName).asString();
                    PSMAIN.MAIN_TITLEHOVERCOLOR = DS_PSMAIN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSMAIN.MAIN_TITLEHOVERCOLOR.FieldName).asString();
                    PSMAIN.MAIN_DESCRIPTION = DS_PSMAIN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSMAIN.MAIN_DESCRIPTION.FieldName).asText();

                    //Other
                    PSMAINList.push(PSMAIN);
                    DS_PSMAIN.DataSet.Next();
                }
            }
            else {
                DS_PSMAIN.ResErr.NotError = false;
                DS_PSMAIN.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    catch (e){
        SysCfg.Log.Methods.WriteLog("DB_PSMAIN.js Persistence.PanelService.Methods.PSMAIN_Fill", e);
    }

    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.PanelService.Methods.PSMAIN_GETID = function (PSMAIN) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.PSMAIN.IDPSMAIN.FieldName, PSMAIN.IDPSMAIN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.PSMAIN.IDPSGROUP.FieldName, PSMAIN.IDPSGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.PSMAIN.MAIN_POSITION.FieldName, PSMAIN.MAIN_POSITION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.PSMAIN.MAIN_ICON.FieldName, PSMAIN.MAIN_ICON, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.PSMAIN.MAIN_TITLE.FieldName, PSMAIN.MAIN_TITLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.PSMAIN.MAIN_TITLECOLOR.FieldName, PSMAIN.MAIN_TITLECOLOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.PSMAIN.MAIN_BACKGROUNDACTIVE.FieldName, PSMAIN.MAIN_BACKGROUNDACTIVE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.PSMAIN.MAIN_TITLEACTIVECOLOR.FieldName, PSMAIN.MAIN_TITLEACTIVECOLOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.PSMAIN.MAIN_BORDERACTIVE.FieldName, PSMAIN.MAIN_BORDERACTIVE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.PSMAIN.MAIN_BACKGROUNDHOVER.FieldName, PSMAIN.MAIN_BACKGROUNDHOVER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.PSMAIN.MAIN_TITLEHOVERCOLOR.FieldName, PSMAIN.MAIN_TITLEHOVERCOLOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddText(UsrCfg.InternoAtisNames.PSMAIN.MAIN_DESCRIPTION.FieldName, PSMAIN.MAIN_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

        /*
        //********   PSMAIN_GETID   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PSMAIN_GETID", @"PSMAIN_GETID description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   IDPSMAIN "); 
         UnSQL.SQL.Add(@"   FROM PSMAIN "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDPSGROUP=@[IDPSGROUP] And "); 
         UnSQL.SQL.Add(@"   MAIN_POSITION=@[MAIN_POSITION] And "); 
         UnSQL.SQL.Add(@"   MAIN_ICON=@[MAIN_ICON] And "); 
         UnSQL.SQL.Add(@"   MAIN_TITLE=@[MAIN_TITLE] And "); 
         UnSQL.SQL.Add(@"   MAIN_TITLECOLOR=@[MAIN_TITLECOLOR] And "); 
         UnSQL.SQL.Add(@"   MAIN_BACKGROUNDACTIVE=@[MAIN_BACKGROUNDACTIVE] And "); 
         UnSQL.SQL.Add(@"   MAIN_TITLEACTIVECOLOR=@[MAIN_TITLEACTIVECOLOR] And "); 
         UnSQL.SQL.Add(@"   MAIN_BORDERACTIVE=@[MAIN_BORDERACTIVE] And "); 
         UnSQL.SQL.Add(@"   MAIN_BACKGROUNDHOVER=@[MAIN_BACKGROUNDHOVER] And "); 
         UnSQL.SQL.Add(@"   MAIN_TITLEHOVERCOLOR=@[MAIN_TITLEHOVERCOLOR] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_PSMAIN = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "PSMAIN_GETID", Param.ToBytes());
        ResErr = DS_PSMAIN.ResErr;
        if (ResErr.NotError) {
            if (DS_PSMAIN.DataSet.RecordCount > 0) {
                DS_PSMAIN.DataSet.First();
                PSMAIN.IDPSMAIN = DS_PSMAIN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSMAIN.IDPSMAIN.FieldName).asInt32();
            }
            else {
                DS_PSMAIN.ResErr.NotError = false;
                DS_PSMAIN.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    catch (e){
        SysCfg.Log.Methods.WriteLog("DB_PSMAIN.js Persistence.PanelService.Methods.PSMAIN_GETID", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.PanelService.Methods.PSMAIN_ADD = function (PSMAIN) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.PSMAIN.IDPSMAIN.FieldName, PSMAIN.IDPSMAIN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.PSMAIN.IDPSGROUP.FieldName, PSMAIN.IDPSGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.PSMAIN.MAIN_POSITION.FieldName, PSMAIN.MAIN_POSITION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PSMAIN.MAIN_ICON.FieldName, PSMAIN.MAIN_ICON, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PSMAIN.MAIN_TITLE.FieldName, PSMAIN.MAIN_TITLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PSMAIN.MAIN_TITLECOLOR.FieldName, PSMAIN.MAIN_TITLECOLOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PSMAIN.MAIN_BACKGROUNDACTIVE.FieldName, PSMAIN.MAIN_BACKGROUNDACTIVE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PSMAIN.MAIN_TITLEACTIVECOLOR.FieldName, PSMAIN.MAIN_TITLEACTIVECOLOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PSMAIN.MAIN_BORDERACTIVE.FieldName, PSMAIN.MAIN_BORDERACTIVE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PSMAIN.MAIN_BACKGROUNDHOVER.FieldName, PSMAIN.MAIN_BACKGROUNDHOVER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PSMAIN.MAIN_TITLEHOVERCOLOR.FieldName, PSMAIN.MAIN_TITLEHOVERCOLOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.PSMAIN.MAIN_DESCRIPTION.FieldName, PSMAIN.MAIN_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        /* 
        //********   PSMAIN_ADD   ************************* 
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PSMAIN_ADD", @"PSMAIN_ADD description."); 
        UnSQL.SQL.Add(@"   INSERT INTO PSMAIN( "); 
         UnSQL.SQL.Add(@"   IDPSGROUP, "); 
         UnSQL.SQL.Add(@"   MAIN_POSITION, "); 
         UnSQL.SQL.Add(@"   MAIN_ICON, "); 
         UnSQL.SQL.Add(@"   MAIN_TITLE, "); 
         UnSQL.SQL.Add(@"   MAIN_TITLECOLOR, "); 
         UnSQL.SQL.Add(@"   MAIN_BACKGROUNDACTIVE, "); 
         UnSQL.SQL.Add(@"   MAIN_TITLEACTIVECOLOR, "); 
         UnSQL.SQL.Add(@"   MAIN_BORDERACTIVE, "); 
         UnSQL.SQL.Add(@"   MAIN_BACKGROUNDHOVER, "); 
         UnSQL.SQL.Add(@"   MAIN_TITLEHOVERCOLOR, "); 
         UnSQL.SQL.Add(@"   MAIN_DESCRIPTION "); 
         UnSQL.SQL.Add(@"   ) Values ( ");  
        UnSQL.SQL.Add(@"   @[IDPSGROUP], "); 
         UnSQL.SQL.Add(@"   @[MAIN_POSITION], "); 
         UnSQL.SQL.Add(@"   @[MAIN_ICON], "); 
         UnSQL.SQL.Add(@"   @[MAIN_TITLE], "); 
         UnSQL.SQL.Add(@"   @[MAIN_TITLECOLOR], "); 
         UnSQL.SQL.Add(@"   @[MAIN_BACKGROUNDACTIVE], "); 
         UnSQL.SQL.Add(@"   @[MAIN_TITLEACTIVECOLOR], "); 
         UnSQL.SQL.Add(@"   @[MAIN_BORDERACTIVE], "); 
         UnSQL.SQL.Add(@"   @[MAIN_BACKGROUNDHOVER], "); 
         UnSQL.SQL.Add(@"   @[MAIN_TITLEHOVERCOLOR], "); 
         UnSQL.SQL.Add(@"   @[MAIN_DESCRIPTION] "); 
         UnSQL.SQL.Add(@"   )  "); 
        UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PSMAIN_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.PanelService.Methods.PSMAIN_GETID(PSMAIN);
        }
    }
    catch (e){
        SysCfg.Log.Methods.WriteLog("DB_PSMAIN.js Persistence.PanelService.Methods.PSMAIN_ADD", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.PanelService.Methods.PSMAIN_UPD = function (PSMAIN) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.PSMAIN.IDPSMAIN.FieldName, PSMAIN.IDPSMAIN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.PSMAIN.IDPSGROUP.FieldName, PSMAIN.IDPSGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.PSMAIN.MAIN_POSITION.FieldName, PSMAIN.MAIN_POSITION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PSMAIN.MAIN_ICON.FieldName, PSMAIN.MAIN_ICON, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PSMAIN.MAIN_TITLE.FieldName, PSMAIN.MAIN_TITLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PSMAIN.MAIN_TITLECOLOR.FieldName, PSMAIN.MAIN_TITLECOLOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PSMAIN.MAIN_BACKGROUNDACTIVE.FieldName, PSMAIN.MAIN_BACKGROUNDACTIVE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PSMAIN.MAIN_TITLEACTIVECOLOR.FieldName, PSMAIN.MAIN_TITLEACTIVECOLOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PSMAIN.MAIN_BORDERACTIVE.FieldName, PSMAIN.MAIN_BORDERACTIVE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PSMAIN.MAIN_BACKGROUNDHOVER.FieldName, PSMAIN.MAIN_BACKGROUNDHOVER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PSMAIN.MAIN_TITLEHOVERCOLOR.FieldName, PSMAIN.MAIN_TITLEHOVERCOLOR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.PSMAIN.MAIN_DESCRIPTION.FieldName, PSMAIN.MAIN_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);


        /*   
        //********   PSMAIN_UPD   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PSMAIN_UPD", @"PSMAIN_UPD description.");  
        UnSQL.SQL.Add(@"  UPDATE PSMAIN Set "); 
         UnSQL.SQL.Add(@"   IDPSGROUP=@[IDPSGROUP], "); 
         UnSQL.SQL.Add(@"   MAIN_POSITION=@[MAIN_POSITION], "); 
         UnSQL.SQL.Add(@"   MAIN_ICON=@[MAIN_ICON], "); 
         UnSQL.SQL.Add(@"   MAIN_TITLE=@[MAIN_TITLE], "); 
         UnSQL.SQL.Add(@"   MAIN_TITLECOLOR=@[MAIN_TITLECOLOR], "); 
         UnSQL.SQL.Add(@"   MAIN_BACKGROUNDACTIVE=@[MAIN_BACKGROUNDACTIVE], "); 
         UnSQL.SQL.Add(@"   MAIN_TITLEACTIVECOLOR=@[MAIN_TITLEACTIVECOLOR], "); 
         UnSQL.SQL.Add(@"   MAIN_BORDERACTIVE=@[MAIN_BORDERACTIVE], "); 
         UnSQL.SQL.Add(@"   MAIN_BACKGROUNDHOVER=@[MAIN_BACKGROUNDHOVER], "); 
         UnSQL.SQL.Add(@"   MAIN_TITLEHOVERCOLOR=@[MAIN_TITLEHOVERCOLOR], "); 
         UnSQL.SQL.Add(@"   MAIN_DESCRIPTION=@[MAIN_DESCRIPTION] "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDPSMAIN=@[IDPSMAIN] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PSMAIN_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    catch (e){
        SysCfg.Log.Methods.WriteLog("DB_PSMAIN.js Persistence.PanelService.Methods.PSMAIN_UPD", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.PanelService.Methods.PSMAIN_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.PanelService.Methods.PSMAIN_DELIDPSMAIN(/*String StrIDPSMAIN*/Param);
    }
    else {
        Res = Persistence.PanelService.Methods.PSMAIN_DELPSMAIN(Param/*PSMAINList*/);
    }
    return (Res);
}

Persistence.PanelService.Methods.PSMAIN_DELIDPSMAIN = function (/*String StrIDPSMAIN*/IDPSMAIN) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDPSMAIN == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.PSMAIN.IDPSMAIN.FieldName, " = " + UsrCfg.InternoAtisNames.PSMAIN.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PSMAIN.IDPSMAIN.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDPSMAIN = " IN (" + StrIDPSMAIN + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.PSMAIN.IDPSMAIN.FieldName, StrIDPSMAIN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.PSMAIN.IDPSMAIN.FieldName, IDPSMAIN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   PSMAIN_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PSMAIN_DEL", @"PSMAIN_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE PSMAIN "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDPSMAIN=@[IDPSMAIN]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PSMAIN_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e){
        SysCfg.Log.Methods.WriteLog("DB_PSMAIN.js Persistence.PanelService.Methods.PSMAIN_DEL(IDPSMAIN)", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.PanelService.Methods.PSMAIN_DELPSMAIN = function (PSMAIN/*PSMAINList*/) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        /*
        StrIDPSMAIN = "";
        StrIDOTRO = "";
        for (var i = 0; i < PSMAINList.length; i++) {
        StrIDPSMAIN += PSMAINList[i].IDPSMAIN + ",";
        StrIDOTRO += PSMAINList[i].IDOTRO + ",";
        }
        StrIDPSMAIN = StrIDPSMAIN.substring(0, StrIDPSMAIN.length - 1);
        StrIDOTRO = StrIDOTRO.substring(0, StrIDOTRO.length - 1);
        
        
        if (StrIDPSMAIN == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.PSMAIN.IDPSMAIN.FieldName, " = " + UsrCfg.InternoAtisNames.PSMAIN.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PSMAIN.IDPSMAIN.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDPSMAIN = " IN (" + StrIDPSMAIN + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.PSMAIN.IDPSMAIN.FieldName, StrIDPSMAIN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/


        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.PSMAIN.IDPSMAIN.FieldName, PSMAIN.IDPSMAIN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*    
        //********   PSMAIN_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PSMAIN_DEL", @"PSMAIN_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE PSMAIN "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDPSMAIN=@[IDPSMAIN]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PSMAIN_DEL", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e){
        SysCfg.Log.Methods.WriteLog("DB_PSMAIN.js Persistence.PanelService.Methods.PSMAIN_DEL(PSMAIN)", e);
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//**********************   METHODS   ********************************************************************************************
Persistence.PanelService.Methods.PSMAIN_ListSetID = function (PSMAINList, IDPSMAIN) {
    for (i = 0; i < PSMAINList.length; i++) {
        if (IDPSMAIN == PSMAINList[i].IDPSMAIN)
            return (PSMAINList[i]);
    }
    var UnPSMAIN = new Persistence.Properties.TPSMAIN;
    UnPSMAIN.IDPSMAIN = IDPSMAIN;
    return (UnPSMAIN);
}
Persistence.PanelService.Methods.PSMAIN_ListAdd = function (PSMAINList, PSMAIN) {
    var i = Persistence.PanelService.Methods.PSMAIN_ListGetIndex(PSMAINList, PSMAIN);
    if (i == -1) PSMAINList.push(PSMAIN);
    else {
        PSMAINList[i].IDPSMAIN = PSMAIN.IDPSMAIN;
        PSMAINList[i].IDPSGROUP = PSMAIN.IDPSGROUP;
        PSMAINList[i].MAIN_POSITION = PSMAIN.MAIN_POSITION;
        PSMAINList[i].MAIN_ICON = PSMAIN.MAIN_ICON;
        PSMAINList[i].MAIN_TITLE = PSMAIN.MAIN_TITLE;
        PSMAINList[i].MAIN_TITLECOLOR = PSMAIN.MAIN_TITLECOLOR;
        PSMAINList[i].MAIN_BACKGROUNDACTIVE = PSMAIN.MAIN_BACKGROUNDACTIVE;
        PSMAINList[i].MAIN_TITLEACTIVECOLOR = PSMAIN.MAIN_TITLEACTIVECOLOR;
        PSMAINList[i].MAIN_BORDERACTIVE = PSMAIN.MAIN_BORDERACTIVE;
        PSMAINList[i].MAIN_BACKGROUNDHOVER = PSMAIN.MAIN_BACKGROUNDHOVER;
        PSMAINList[i].MAIN_TITLEHOVERCOLOR = PSMAIN.MAIN_TITLEHOVERCOLOR;
        PSMAINList[i].MAIN_DESCRIPTION = PSMAIN.MAIN_DESCRIPTION;

    }
}
//CJRC_26072018
Persistence.PanelService.Methods.PSMAIN_ListGetIndex = function (PSMAINList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.PanelService.Methods.PSMAIN_ListGetIndexIDPSMAIN(PSMAINList, Param);

    }
    else {
        Res = Persistence.PanelService.Methods.PSMAIN_ListGetIndexPSMAIN(PSMAINList, Param);
    }
    return (Res);
}

Persistence.PanelService.Methods.PSMAIN_ListGetIndexPSMAIN = function (PSMAINList, PSMAIN) {
    for (i = 0; i < PSMAINList.length; i++) {
        if (PSMAIN.IDPSMAIN == PSMAINList[i].IDPSMAIN)
            return (i);
    }
    return (-1);
}
Persistence.PanelService.Methods.PSMAIN_ListGetIndexIDPSMAIN = function (PSMAINList, IDPSMAIN) {
    for (i = 0; i < PSMAINList.length; i++) {
        if (IDPSMAIN == PSMAINList[i].IDPSMAIN)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.PanelService.Methods.PSMAINListtoStr = function (PSMAINList) {
    var Res = Persistence.Properties._Version + "(1)0";
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(PSMAINList.length, Longitud, Texto);
        for (Counter = 0; Counter < PSMAINList.length; Counter++) {
            var UnPSMAIN = PSMAINList[Counter];
            UnPSMAIN.ToStr(Longitud, Texto);
        }

        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
}
Persistence.PanelService.Methods.StrtoPSMAIN = function (ProtocoloStr, PSMAINList) {

    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        PSMAINList.length = 0;
        if (Persistence.PanelService.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")") - ProtocoloStr.indexOf("(") - 1);
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.Length - ProtocoloStr.indexOf(")") - 1);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnPSMAIN = new Persistence.Properties.TPSMAIN(); //Mode new row
                UnPSMAIN.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.PanelService.PSMAIN_ListAdd(PSMAINList, UnPSMAIN);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}


//Socket Function 
Persistence.PanelService.Methods.PSMAINListToByte = function (PSMAINList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, PSMAINList.Count - idx);
    for (i = idx; i < PSMAINList.length; i++) {
        PSMAINList[i].ToByte(MemStream);
    }
}
Persistence.PanelService.Methods.ByteToPSMAINList = function (PSMAINList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnPSMAIN = new Persistence.Properties.TPSMAIN();
        UnPSMAIN.ByteTo(MemStream);
        Methods.PSMAIN_ListAdd(PSMAINList, UnPSMAIN);
    }
}

////**********************   PROPERTIES   *****************************************************************************************
//Persistence.PanelService.Properties.TPSMAIN = function () {
//    this.IDPSMAIN = 0;
//    this.IDPSGROUP = 0;
//    this.MAIN_POSITION = 0;
//    this.MAIN_ICON = "";
//    this.MAIN_TITLE = "";
//    this.MAIN_DESCRIPTION = "";
//    //VISRTUALES
//    this.PSGROUP = new Persistence.PanelService.Properties.TPSGROUP();
//    this.PSSECUNDARYList = new Array();

//    //Socket IO Properties
//    this.ToByte = function (MemStream) {
//        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDPSMAIN);
//        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDPSGROUP);
//        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.MAIN_POSITION);
//        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.MAIN_ICON);
//        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.MAIN_TITLE);
//        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.MAIN_DESCRIPTION);

//    }
//    this.ByteTo = function (MemStream) {
//        this.IDPSMAIN = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
//        this.IDPSGROUP = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
//        this.MAIN_POSITION = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
//        this.MAIN_ICON = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
//        this.MAIN_TITLE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
//        this.MAIN_DESCRIPTION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);

//    }
//    //Str IO Properties
//    this.ToStr = function (Longitud, Texto) {
//        SysCfg.Str.Protocol.WriteInt(this.IDPSMAIN, Longitud, Texto);
//        SysCfg.Str.Protocol.WriteInt(this.IDPSGROUP, Longitud, Texto);
//        SysCfg.Str.Protocol.WriteInt(this.MAIN_POSITION, Longitud, Texto);
//        SysCfg.Str.Protocol.WriteStr(this.MAIN_ICON, Longitud, Texto);
//        SysCfg.Str.Protocol.WriteStr(this.MAIN_TITLE, Longitud, Texto);
//        SysCfg.Str.Protocol.WriteStr(this.MAIN_DESCRIPTION, Longitud, Texto);

//    }
//    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
//        this.IDPSMAIN = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
//        this.IDPSGROUP = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
//        this.MAIN_POSITION = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
//        this.MAIN_ICON = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
//        this.MAIN_TITLE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
//        this.MAIN_DESCRIPTION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);

//    }
//}

////**********************   METHODS server  ********************************************************************************************
//Persistence.PanelService.Methods.PSMAIN_Fill = function (PSMAINList, StrIDPSMAIN/*noin IDPSMAIN*/) {
//    var ResErr = new SysCfg.Error.Properties.TResErr();
//    PSMAINList.length = 0;
//    var Param = new SysCfg.Stream.Properties.TParam();
//    Param.Inicialize();
//    if (StrIDPSMAIN == "") {
//        Param.AddUnknown(UsrCfg.InternoAtisNames.PSMAIN.IDPSMAIN.FieldName, " = " + UsrCfg.InternoAtisNames.PSMAIN.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PSMAIN.IDPSMAIN.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
//    }
//    else {
//        StrIDPSMAIN = " IN (" + StrIDPSMAIN + ")";
//        Param.AddUnknown(UsrCfg.InternoAtisNames.PSMAIN.IDPSMAIN.FieldName, StrIDPSMAIN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
//    }
//    //noin Param.AddInt32(UsrCfg.InternoDemoNames.PSMAIN.IDPSMAIN.FieldName, IDPSMAIN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
//    try {
//        /*
//        //********   PSMAIN_GET   *************************  
//        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PSMAIN_GET", @"PSMAIN_GET description.");  
//        UnSQL.SQL.Add(@"   SELECT "); 
//         UnSQL.SQL.Add(@"   IDPSMAIN, "); 
//         UnSQL.SQL.Add(@"   IDPSGROUP, "); 
//         UnSQL.SQL.Add(@"   MAIN_POSITION, "); 
//         UnSQL.SQL.Add(@"   MAIN_ICON, "); 
//         UnSQL.SQL.Add(@"   MAIN_TITLE, "); 
//         UnSQL.SQL.Add(@"   MAIN_DESCRIPTION "); 
//         UnSQL.SQL.Add(@"   FROM PSMAIN "); 
//         UnConfigSQL.SQL.Add(UnSQL); 
         
        
//        */
//        var DS_PSMAIN = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "PSMAIN_GET", Param.ToBytes());
//        ResErr = DS_PSMAIN.ResErr;
//        if (ResErr.NotError) {
//            if (DS_PSMAIN.DataSet.RecordCount > 0) {
//                DS_PSMAIN.DataSet.First();
//                while (!(DS_PSMAIN.DataSet.Eof)) {
//                    var PSMAIN = new Persistence.PanelService.Properties.TPSMAIN();
//                    PSMAIN.IDPSMAIN = DS_PSMAIN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSMAIN.IDPSMAIN.FieldName).asInt32();
//                    PSMAIN.IDPSGROUP = DS_PSMAIN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSMAIN.IDPSGROUP.FieldName).asInt32();
//                    PSMAIN.MAIN_POSITION = DS_PSMAIN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSMAIN.MAIN_POSITION.FieldName).asInt32();
//                    PSMAIN.MAIN_ICON = DS_PSMAIN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSMAIN.MAIN_ICON.FieldName).asString();
//                    PSMAIN.MAIN_TITLE = DS_PSMAIN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSMAIN.MAIN_TITLE.FieldName).asString();
//                    PSMAIN.MAIN_DESCRIPTION = DS_PSMAIN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSMAIN.MAIN_DESCRIPTION.FieldName).asString();

//                    //Other
//                    PSMAINList.push(PSMAIN);
//                    DS_PSMAIN.DataSet.Next();
//                }
//            }
//            else {
//                DS_PSMAIN.ResErr.NotError = false;
//                DS_PSMAIN.ResErr.Mesaje = "RECORDCOUNT = 0";
//            }
//        }
//    }
//    finally {
//        Param.Destroy();
//    }
//    return (ResErr);
//}
//Persistence.PanelService.Methods.PSMAIN_GETID = function (PSMAIN) {
//    var ResErr = new SysCfg.Error.Properties.TResErr();
//    var Param = new SysCfg.Stream.Properties.TParam();
//    try {
//        Param.Inicialize();
//        //Other
//        Param.AddInt32(UsrCfg.InternoAtisNames.PSMAIN.IDPSMAIN.FieldName, PSMAIN.IDPSMAIN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
//        Param.AddInt32(UsrCfg.InternoAtisNames.PSMAIN.IDPSGROUP.FieldName, PSMAIN.IDPSGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
//        Param.AddInt32(UsrCfg.InternoAtisNames.PSMAIN.MAIN_POSITION.FieldName, PSMAIN.MAIN_POSITION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
//        Param.AddString(UsrCfg.InternoAtisNames.PSMAIN.MAIN_ICON.FieldName, PSMAIN.MAIN_ICON, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
//        Param.AddString(UsrCfg.InternoAtisNames.PSMAIN.MAIN_TITLE.FieldName, PSMAIN.MAIN_TITLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
//        Param.AddString(UsrCfg.InternoAtisNames.PSMAIN.MAIN_DESCRIPTION.FieldName, PSMAIN.MAIN_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

//        /*
//        //********   PSMAIN_GETID   *************************  
//        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PSMAIN_GETID", @"PSMAIN_GETID description.");  
//        UnSQL.SQL.Add(@"   SELECT "); 
//         UnSQL.SQL.Add(@"   IDPSMAIN "); 
//         UnSQL.SQL.Add(@"   FROM PSMAIN "); 
//         UnSQL.SQL.Add(@"   Where "); 
//         UnSQL.SQL.Add(@"   IDPSGROUP=@[IDPSGROUP] And "); 
//         UnSQL.SQL.Add(@"   MAIN_POSITION=@[MAIN_POSITION] And "); 
//         UnSQL.SQL.Add(@"   MAIN_ICON=@[MAIN_ICON] And "); 
//         UnSQL.SQL.Add(@"   MAIN_TITLE=@[MAIN_TITLE] ");
//         UnConfigSQL.SQL.Add(UnSQL); 
         
        
//        */
//        var DS_PSMAIN = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "PSMAIN_GETID", Param.ToBytes());
//        ResErr = DS_PSMAIN.ResErr;
//        if (ResErr.NotError) {
//            if (DS_PSMAIN.DataSet.RecordCount > 0) {
//                DS_PSMAIN.DataSet.First();
//                PSMAIN.IDPSMAIN = DS_PSMAIN.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSMAIN.IDPSMAIN.FieldName).asInt32();
//            }
//            else {
//                DS_PSMAIN.ResErr.NotError = false;
//                DS_PSMAIN.ResErr.Mesaje = "RECORDCOUNT = 0";
//            }
//        }
//    }
//    finally {
//        Param.Destroy();
//    }
//    return (ResErr);
//}
//Persistence.PanelService.Methods.PSMAIN_ADD = function (PSMAIN) {
//    var ResErr = new SysCfg.Error.Properties.TResErr();
//    var Param = new SysCfg.Stream.Properties.TParam();
//    try {
//        Param.Inicialize();
//        //Other
//        Param.AddInt32(UsrCfg.InternoAtisNames.PSMAIN.IDPSMAIN.FieldName, PSMAIN.IDPSMAIN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
//        Param.AddInt32(UsrCfg.InternoAtisNames.PSMAIN.IDPSGROUP.FieldName, PSMAIN.IDPSGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
//        Param.AddInt32(UsrCfg.InternoAtisNames.PSMAIN.MAIN_POSITION.FieldName, PSMAIN.MAIN_POSITION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
//        Param.AddString(UsrCfg.InternoAtisNames.PSMAIN.MAIN_ICON.FieldName, PSMAIN.MAIN_ICON, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
//        Param.AddString(UsrCfg.InternoAtisNames.PSMAIN.MAIN_TITLE.FieldName, PSMAIN.MAIN_TITLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
//        Param.AddString(UsrCfg.InternoAtisNames.PSMAIN.MAIN_DESCRIPTION.FieldName, PSMAIN.MAIN_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

//        /* 
//        //********   PSMAIN_ADD   ************************* 
//        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PSMAIN_ADD", @"PSMAIN_ADD description."); 
//        UnSQL.SQL.Add(@"   INSERT INTO PSMAIN( "); 
//         UnSQL.SQL.Add(@"   IDPSGROUP, "); 
//         UnSQL.SQL.Add(@"   MAIN_POSITION, "); 
//         UnSQL.SQL.Add(@"   MAIN_ICON, "); 
//         UnSQL.SQL.Add(@"   MAIN_TITLE, "); 
//         UnSQL.SQL.Add(@"   MAIN_DESCRIPTION "); 
//         UnSQL.SQL.Add(@"   ) Values ( ");  
//        UnSQL.SQL.Add(@"   @[IDPSGROUP], "); 
//         UnSQL.SQL.Add(@"   @[MAIN_POSITION], "); 
//         UnSQL.SQL.Add(@"   @[MAIN_ICON], "); 
//         UnSQL.SQL.Add(@"   @[MAIN_TITLE], "); 
//         UnSQL.SQL.Add(@"   @[MAIN_DESCRIPTION] "); 
//         UnSQL.SQL.Add(@"   )  "); 
//        UnConfigSQL.SQL.Add(UnSQL); 
         
        
//        */
//        this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PSMAIN_ADD", Param.ToBytes());
//        ResErr = Exec.ResErr;
//        if (ResErr.NotError) {
//            Persistence.PanelService.Methods.PSMAIN_GETID(PSMAIN);
//        }
//    }
//    finally {
//        Param.Destroy();
//    }
//    return (ResErr);
//}
//Persistence.PanelService.Methods.PSMAIN_UPD = function (PSMAIN) {
//    var ResErr = SysCfg.Error.Properties.TResErr();
//    var Param = new SysCfg.Stream.Properties.TParam();
//    try {
//        Param.Inicialize();
//        Param.AddInt32(UsrCfg.InternoAtisNames.PSMAIN.IDPSMAIN.FieldName, PSMAIN.IDPSMAIN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
//        Param.AddInt32(UsrCfg.InternoAtisNames.PSMAIN.IDPSGROUP.FieldName, PSMAIN.IDPSGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
//        Param.AddInt32(UsrCfg.InternoAtisNames.PSMAIN.MAIN_POSITION.FieldName, PSMAIN.MAIN_POSITION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
//        Param.AddString(UsrCfg.InternoAtisNames.PSMAIN.MAIN_ICON.FieldName, PSMAIN.MAIN_ICON, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
//        Param.AddString(UsrCfg.InternoAtisNames.PSMAIN.MAIN_TITLE.FieldName, PSMAIN.MAIN_TITLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
//        Param.AddString(UsrCfg.InternoAtisNames.PSMAIN.MAIN_DESCRIPTION.FieldName, PSMAIN.MAIN_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);


//        /*   
//        //********   PSMAIN_UPD   *************************  
//        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PSMAIN_UPD", @"PSMAIN_UPD description.");  
//        UnSQL.SQL.Add(@"  UPDATE PSMAIN Set "); 
//         UnSQL.SQL.Add(@"   IDPSGROUP=@[IDPSGROUP], "); 
//         UnSQL.SQL.Add(@"   MAIN_POSITION=@[MAIN_POSITION], "); 
//         UnSQL.SQL.Add(@"   MAIN_ICON=@[MAIN_ICON], "); 
//         UnSQL.SQL.Add(@"   MAIN_TITLE=@[MAIN_TITLE], "); 
//         UnSQL.SQL.Add(@"   MAIN_DESCRIPTION=@[MAIN_DESCRIPTION] "); 
//         UnSQL.SQL.Add(@"   Where "); 
//         UnSQL.SQL.Add(@"   IDPSMAIN=@[IDPSMAIN] ");
//         UnConfigSQL.SQL.Add(UnSQL); 
         
        
//        */
//        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PSMAIN_UPD", Param.ToBytes());
//        ResErr = Exec.ResErr;
//        if (ResErr.NotError) {
//            //GET(); 
//        }
//    }
//    finally {
//        Param.Destroy();
//    }
//    return (ResErr);
//}

////CJRC_27072018
//Persistence.PanelService.Methods.PSMAIN_DEL = function (Param) {
//    var Res = -1
//    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
//        Res = Persistence.PanelService.Methods.PSMAIN_DELIDPSMAIN(/*String StrIDPSMAIN*/Param);
//    }
//    else {
//        Res = Persistence.PanelService.Methods.PSMAIN_DELPSMAIN(Param/*PSMAINList*/);
//    }
//    return (Res);
//}
//
//Persistence.PanelService.Methods.PSMAIN_DELIDPSMAIN = function (/*String StrIDPSMAIN*/IDPSMAIN) {
//    var ResErr = SysCfg.Error.Properties.TResErr();
//    var Param = new SysCfg.Stream.Properties.TParam();
//    try {
//        Param.Inicialize();
//        /*if (StrIDPSMAIN == "")
//        {
//        Param.AddUnknown(UsrCfg.InternoAtisNames.PSMAIN.IDPSMAIN.FieldName, " = " + UsrCfg.InternoAtisNames.PSMAIN.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PSMAIN.IDPSMAIN.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
//        }
//        else
//        {
//        StrIDPSMAIN = " IN (" + StrIDPSMAIN + ")";
//        Param.AddUnknown(UsrCfg.InternoAtisNames.PSMAIN.IDPSMAIN.FieldName, StrIDPSMAIN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
//        }*/
//        Param.AddInt32(UsrCfg.InternoAtisNames.PSMAIN.IDPSMAIN.FieldName, IDPSMAIN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
//        /*
//        //********   PSMAIN_DEL   *************************  
//        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PSMAIN_DEL", @"PSMAIN_DEL description.");  
//        UnSQL.SQL.Add(@"   DELETE PSMAIN "); 
//         UnSQL.SQL.Add(@"   Where "); 
//         UnSQL.SQL.Add(@"   IDPSMAIN=@[IDPSMAIN]   ");
//         UnConfigSQL.SQL.Add(UnSQL); 
         
        
//        */
//        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PSMAIN_DEL_1", Param.ToBytes());
//        ResErr = Exec.ResErr;
//    }
//    finally {
//        Param.Destroy();
//    }
//    return (ResErr);
//}
//Persistence.PanelService.Methods.PSMAIN_DELPSMAIN = function (PSMAIN/*PSMAINList*/) {
//    var ResErr = SysCfg.Error.Properties.TResErr();
//    var Param = new SysCfg.Stream.Properties.TParam();
//    try {
//        /*
//        StrIDPSMAIN = "";
//        StrIDOTRO = "";
//        for (var i = 0; i < PSMAINList.length; i++) {
//        StrIDPSMAIN += PSMAINList[i].IDPSMAIN + ",";
//        StrIDOTRO += PSMAINList[i].IDOTRO + ",";
//        }
//        StrIDPSMAIN = StrIDPSMAIN.substring(0, StrIDPSMAIN.length - 1);
//        StrIDOTRO = StrIDOTRO.substring(0, StrIDOTRO.length - 1);
        
        
//        if (StrIDPSMAIN == "")
//        {
//        Param.AddUnknown(UsrCfg.InternoAtisNames.PSMAIN.IDPSMAIN.FieldName, " = " + UsrCfg.InternoAtisNames.PSMAIN.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PSMAIN.IDPSMAIN.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
//        }
//        else
//        {
//        StrIDPSMAIN = " IN (" + StrIDPSMAIN + ")";
//        Param.AddUnknown(UsrCfg.InternoAtisNames.PSMAIN.IDPSMAIN.FieldName, StrIDPSMAIN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
//        }*/


//        Param.Inicialize();
//        Param.AddInt32(UsrCfg.InternoAtisNames.PSMAIN.IDPSMAIN.FieldName, PSMAIN.IDPSMAIN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
//        /*    
//        //********   PSMAIN_DEL   *************************  
//        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PSMAIN_DEL", @"PSMAIN_DEL description.");  
//        UnSQL.SQL.Add(@"   DELETE PSMAIN "); 
//         UnSQL.SQL.Add(@"   Where "); 
//         UnSQL.SQL.Add(@"   IDPSMAIN=@[IDPSMAIN]   ");
//         UnConfigSQL.SQL.Add(UnSQL); 
         
        
//        */
//        // var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PSMAIN_DEL_2", Param.ToBytes());
//        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PSMAIN_DEL", Param.ToBytes());
//        ResErr = Exec.ResErr;
//    }
//    finally {
//        Param.Destroy();
//    }
//    return (ResErr);
//}

////**********************   METHODS   ********************************************************************************************
//Persistence.PanelService.Methods.PSMAIN_ListSetID = function (PSMAINList, IDPSMAIN) {
//    for (i = 0; i < PSMAINList.length; i++) {
//        if (IDPSMAIN == PSMAINList[i].IDPSMAIN)
//            return (PSMAINList[i]);
//    }
//    var UnPSMAIN = new Persistence.Properties.TPSMAIN;
//    UnPSMAIN.IDPSMAIN = IDPSMAIN;
//    return (UnPSMAIN);
//}
//Persistence.PanelService.Methods.PSMAIN_ListAdd = function (PSMAINList, PSMAIN) {
//    var i = Persistence.PanelService.Methods.PSMAIN_ListGetIndex(PSMAINList, PSMAIN);
//    if (i == -1) PSMAINList.push(PSMAIN);
//    else {
//        PSMAINList[i].IDPSMAIN = PSMAIN.IDPSMAIN;
//        PSMAINList[i].IDPSGROUP = PSMAIN.IDPSGROUP;
//        PSMAINList[i].MAIN_POSITION = PSMAIN.MAIN_POSITION;
//        PSMAINList[i].MAIN_ICON = PSMAIN.MAIN_ICON;
//        PSMAINList[i].MAIN_TITLE = PSMAIN.MAIN_TITLE;
//        PSMAINList[i].MAIN_DESCRIPTION = PSMAIN.MAIN_DESCRIPTION;

//    }
//}
//
////CJRC_26072018
//Persistence.PanelService.Methods.PSMAIN_ListGetIndex = function (PSMAINList, Param) {
//    var Res = -1
//    if (typeof (Param) == 'number') {
//        Res = Persistence.PanelService.Methods.PSMAIN_ListGetIndexIDPSMAIN(PSMAINList, Param);

//    }
//    else {
//        Res = Persistence.PanelService.Methods.PSMAIN_ListGetIndexPSMAIN(PSMAINList, Param);
//    }
//    return (Res);
//}

//Persistence.PanelService.Methods.PSMAIN_ListGetIndexPSMAIN = function (PSMAINList, PSMAIN) {
//    for (i = 0; i < PSMAINList.length; i++) {
//        if (PSMAIN.IDPSMAIN == PSMAINList[i].IDPSMAIN)
//            return (i);
//    }
//    return (-1);
//}
//Persistence.PanelService.Methods.PSMAIN_ListGetIndexIDPSMAIN = function (PSMAINList, IDPSMAIN) {
//    for (i = 0; i < PSMAINList.length; i++) {
//        if (IDPSMAIN == PSMAINList[i].IDPSMAIN)
//            return (i);
//    }
//    return (-1);
//}

////String Function 
//Persistence.PanelService.Methods.PSMAINListtoStr = function (PSMAINList) {
//    var Res = Persistence.PanelService.Properties._Version + "(1)0";
//    var Longitud = new SysCfg.ref("");
//    var Texto = new SysCfg.ref("");
//    try {
//        SysCfg.Str.Protocol.WriteInt(PSMAINList.length, Longitud, Texto);
//        for (Counter = 0; Counter < PSMAINList.length; Counter++) {
//            var UnPSMAIN = PSMAINList[Counter];
//            UnPSMAIN.ToStr(Longitud, Texto);
//        }

//        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
//        Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
//    }
//    catch (e) {
//        var ResErr = new SysCfg.Error.Properties.TResErr();
//        ResErr.NotError = false;
//        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
//    }
//    return Res;
//}
//Persistence.PanelService.Methods.StrtoPSMAIN = function (ProtocoloStr, PSMAINList) {

//    var Res = false;
//    var Longitud, Texto;
//    var Index = new SysCfg.ref(0);
//    var Pos = new SysCfg.ref(0);
//    try {
//        PSMAINList.length = 0;
//        if (Persistence.PanelService.Properties._Version == ProtocoloStr.substring(0, 3)) {
//            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")") - ProtocoloStr.indexOf("(") - 1);
//            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.Length - ProtocoloStr.indexOf(")") - 1);
//            LongitudArray = Longitud.split(",");
//            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
//            for (i = 0; i < LSCount; i++) {
//                UnPSMAIN = new Persistence.Properties.TPSMAIN(); //Mode new row
//                UnPSMAIN.StrTo(Pos, Index, LongitudArray, Texto);
//                Persistence.PanelService.PSMAIN_ListAdd(PSMAINList, UnPSMAIN);
//            }
//        }
//    }
//    catch (e) {
//        var ResErr = SysCfg.Error.Properties.TResErr;
//        ResErr.NotError = false;
//        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
//    }
//    return (Res);
//}


////Socket Function 
//Persistence.PanelService.Methods.PSMAINListToByte = function (PSMAINList, MemStream, idx) {
//    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, PSMAINList.Count - idx);
//    for (i = idx; i < PSMAINList.length; i++) {
//        PSMAINList[i].ToByte(MemStream);
//    }
//}
//Persistence.PanelService.Methods.ByteToPSMAINList = function (PSMAINList, MemStream) {
//    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
//    for (i = 0; i < Count; i++) {
//        UnPSMAIN = new Persistence.Properties.TPSMAIN();
//        UnPSMAIN.ByteTo(MemStream);
//        Methods.PSMAIN_ListAdd(PSMAINList, UnPSMAIN);
//    }
//}
