﻿//Persistence.Methods

//UserFunctions
//List Function 
Persistence.Methods.TPersistenceProfiler = function () {
    this.MemStrm_Request = new SysCfg.Stream.TMemoryStream();
    this.Byte_Request = new Array();//Byte[]
    this.MemStrm_Response = new SysCfg.Stream.TMemoryStream();
    this.Byte_Response = new Array();//Byte[]
    this.PersistenceSource = Comunic.Properties.TPersistenceSource._None;
    this.PersistenceType = Comunic.Properties.TPersistenceType._None;
    this.PersistenceCmd = Comunic.Properties.TPersistenceCmd._None;
    this.Completed = null;
    this.OnCompleted = function(e){
         var handler = Completed;
         if (handler != null)
         {
             handler(this, e);
         }
    }
    this.TPersistenceProfiler = function () {
        MemStrm_Request.Position = 0;
    }
    this.GetPersistence = function () {
        Byte_Request = new Array[MemStrm_Request.Length()];
        Byte_Request = MemStrm_Request.ToArray();
        var GetPersistence = new Comunic.Properties.TGetPersistence();
        GetPersistence = Comunic.Ashx.Client.Merhods.GetPersistence(PersistenceSource, PersistenceType, PersistenceCmd, Byte_Request);
        OnCompletedGetPersistence(GetPersistence, null);
    }
    this.OnCompletedGetPersistence = function (sender, e) {
        GetPersistence = sender;               //Comunic.Properties.TGetPersistence
        if (GetPersistence != null) {
            if (GetPersistence.ResErr.NotError) {

                try {
                    Byte_Response = GetPersistence.Byte_Response;
                    MemStrm_Response.Write(GetPersistence.Byte_Response, 0, GetPersistence.Byte_Response.length);
                    MemStrm_Response.Position = 0;
                    /*
                    Persistence.Catalog.Methods.ByteToSDTYPEUSERList(CatalogProfiler.SDTYPEUSERList, MemStream);
                    Persistence.Catalog.Methods.ByteToMDSERVICETYPEList(CatalogProfiler.MDSERVICETYPEList, MemStream);
                    Persistence.Catalog.Methods.ByteToMDPRIORITYList(CatalogProfiler.MDPRIORITYList,  MemStream);
                    Persistence.Catalog.Methods.ByteToMDHIERPERList(CatalogProfiler.MDHIERPERList, MemStream);
                    Persistence.Catalog.Methods.ByteToMDFUNCPERList(CatalogProfiler.MDFUNCPERList, MemStream);*/
                    this.OnCompleted(null);
                }
                finally {

                }
            }
        }
    }
    this.Destroy = function () {
        MemStrm_Request.Close();
        MemStrm_Request.Dispose();
        MemStrm_Response.Close();
        MemStrm_Response.Dispose();
    }

}

Persistence.Methods.UserFunctions_ListSetID = function (UserFunctionsList, IDCMDBCI) {
    for (var i = 0; i < UserFunctionsList.length; i++) {

        if (IDCMDBCI == UserFunctionsList[i].IDCMDBCI)
            return (UserFunctionsList[i]);

    }
    return (new TUserFunctions(IDCMDBCI));
}

Persistence.Methods.UserFunctions_ListAdd = function (UserFunctionsList, UserFunctions) {
    var i = UserFunctions_ListGetIndex(UserFunctionsList, UserFunctions);
    if (i == -1)
        UserFunctionsList.Add(UserFunctions);
    else {
        UserFunctionsList[i].User.ROLENAME = UserFunctions.User.ROLENAME;
        UserFunctionsList[i].User.IDATROLE = UserFunctions.User.IDATROLE;
        UserFunctionsList[i].User.IDCMDBUSER = UserFunctions.User.IDCMDBUSER;
        UserFunctionsList[i].User.PASSWORD = UserFunctions.User.PASSWORD;
        UserFunctionsList[i].User.LASTPWDCHANGE = UserFunctions.User.LASTPWDCHANGE;
        UserFunctionsList[i].User.IDMDCALENDARYDATE = UserFunctions.User.IDMDCALENDARYDATE;
        UserFunctionsList[i].User.CI_GENERICNAME = UserFunctions.User.CI_GENERICNAME;
    }
}

//CJRC_26072018
Persistence.Methods.UserFunctions_ListGetIndex = function (UserFunctionsList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.Methods.UserFunctions_ListGetIndexIDCMDBCI(UserFunctionsList, Param);

    }
    else {
        Res = Persistence.Methods.UserFunctions_ListGetIndexUserFunctions(UserFunctionsList, Param);
    }
    return (Res);
}

Persistence.Methods.UserFunctions_ListGetIndexUserFunctions = function (UserFunctionsList, UserFunctions) {
    for (var i = 0; i < UserFunctionsList.length; i++) {
        if (UserFunctions.IDCMDBCI == UserFunctionsList[i].IDCMDBCI)
            return (i);
    }
    return (-1);
}

Persistence.Methods.UserFunctions_ListGetIndexIDCMDBCI = function (UserFunctionsList, IDCMDBCI) {
    for (var i = 0; i < UserFunctionsList.length; i++) {
        if (IDCMDBCI == UserFunctionsList[i].IDCMDBCI)
            return (i);
    }
    return (-1);
}


Persistence.Methods.GenereSQLHW = function (Param) {
    Param.AddInt32(UsrCfg.InternoAtisNames.ATROLE.NAME_TABLE + "." + UsrCfg.InternoAtisNames.ATROLE.IDATROLE.FieldName, UsrCfg.Properties.UserATRole.ATROLE.IDATROLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCI.NAME_TABLE + "." + UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCI.FieldName, UsrCfg.Properties.UserATRole.User.IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
}

Persistence.Methods.IsNotInGenereSQLHW = function (Str) {
    var Res = true;
    if (SysCfg.Str.Methods.isEqual(Str, "@[" + UsrCfg.InternoAtisNames.ATROLE.NAME_TABLE + "." + UsrCfg.InternoAtisNames.ATROLE.IDATROLE.FieldName + "]")) {
        Res = false;
    }
    if (SysCfg.Str.Methods.isEqual(Str, "@[" + UsrCfg.InternoCMDBNames.CMDBCI.NAME_TABLE + "." + UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCI.FieldName + "]")) {
        Res = false;
    }
    return Res;
}


Persistence.Methods.ListRemoveGenereSQLHW = function (StringListField) {
    var Res = true;
    var Counter = 0;
    while (Counter < StringListField.Count) {
        var Str = StringListField[Counter];

        if (SysCfg.Str.Methods.isEqual(Str, "@[" + UsrCfg.InternoAtisNames.ATROLE.NAME_TABLE + "." + UsrCfg.InternoAtisNames.ATROLE.IDATROLE.FieldName + "]")) {
            StringListField.RemoveAt(Counter);

        }
        else if (SysCfg.Str.Methods.isEqual(Str, "@[" + UsrCfg.InternoCMDBNames.CMDBCI.NAME_TABLE + "." + UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCI.FieldName + "]")) {
            StringListField.RemoveAt(Counter);
        }
        else {
            Counter++;
        }

    }
}




