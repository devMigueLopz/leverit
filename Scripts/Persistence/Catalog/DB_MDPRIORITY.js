﻿//**********************   PROPERTIES   *****************************************************************************************
Persistence.Catalog.Properties.TMDPRIORITY = function () {
    this.IDMDPRIORITY =0;
    this.PRIORITYNAME ="";
    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDPRIORITY);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.PRIORITYNAME);
    }
    this.ByteTo = function (MemStream) {
        this.IDMDPRIORITY = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.PRIORITYNAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDMDPRIORITY, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.PRIORITYNAME, Longitud, Texto);
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDMDPRIORITY = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.PRIORITYNAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
    }
}
//**********************   METHODS server  ********************************************************************************************
Persistence.Catalog.Methods.MDPRIORITY_Fill = function (MDPRIORITYList) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    MDPRIORITYList.length = 0;
    try {
        var DS_MDPRIORITY = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDPRIORITY_GET", new Array());
        ResErr = DS_MDPRIORITY.ResErr;
        if (ResErr.NotError) {
            if (DS_MDPRIORITY.DataSet.RecordCount > 0) {
                DS_MDPRIORITY.DataSet.First();
                while (!(DS_MDPRIORITY.DataSet.Eof)) {
                    var MDPRIORITY = new Persistence.Catalog.Properties.TMDPRIORITY();
                    MDPRIORITY.IDMDPRIORITY = DS_MDPRIORITY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName).asInt32();
                    MDPRIORITY.PRIORITYNAME = DS_MDPRIORITY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDPRIORITY.PRIORITYNAME.FieldName).asString();
                    //Other
                    MDPRIORITYList.push(MDPRIORITY);
                    DS_MDPRIORITY.DataSet.Next();
                }
            }
            else {
                DS_MDPRIORITY.ResErr.NotError = false;
                DS_MDPRIORITY.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_MDPRIORITY.js Persistence.Catalog.Methods.MDPRIORITY_Fill", e);
    }
    return (ResErr);
}
Persistence.Catalog.Methods.MDPRIORITY_GETID = function (MDPRIORITY) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName, MDPRIORITY.IDMDPRIORITY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.MDPRIORITY.PRIORITYNAME.FieldName, MDPRIORITY.PRIORITYNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   MDPRIORITY_GETID   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDPRIORITY_GETID", @"MDPRIORITY_GETID description.");
        UnSQL.SQL.Add(@" SELECT  ");
        UnSQL.SQL.Add(@"  MDPRIORITY.IDMDPRIORITY, ");
        UnSQL.SQL.Add(@"  MDPRIORITY.PRIORITYNAME ");
        UnSQL.SQL.Add(@" FROM  MDPRIORITY ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  MDPRIORITY.IDMDPRIORITY=@[IDMDPRIORITY] and  ");
        UnSQL.SQL.Add(@"  MDPRIORITY.PRIORITYNAME=@[PRIORITYNAME] ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var DS_MDPRIORITY = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDPRIORITY_GETID", Param.ToBytes());
        ResErr = DS_MDPRIORITY.ResErr;
        if (ResErr.NotError) {
            if (DS_MDPRIORITY.DataSet.RecordCount > 0) {
                DS_MDPRIORITY.DataSet.First();
                MDPRIORITY.IDMDPRIORITY = DS_MDPRIORITY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName).asInt32();
                //Other
                MDPRIORITY.PRIORITYNAME = DS_MDPRIORITY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDPRIORITY.PRIORITYNAME.FieldName).asString();
            }
            else {
                DS_MDPRIORITY.ResErr.NotError = false;
                DS_MDPRIORITY.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_MDPRIORITY.js Persistence.Catalog.Methods.MDPRIORITY_GETID", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Catalog.Methods.MDPRIORITY_ADD = function (MDPRIORITY) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName, MDPRIORITY.IDMDPRIORITY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.MDPRIORITY.PRIORITYNAME.FieldName, MDPRIORITY.PRIORITYNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   MDPRIORITY_ADD   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDPRIORITY_ADD", @"MDPRIORITY_ADD description.");
        UnSQL.SQL.Add(@" INSERT INTO MDPRIORITY( ");
        UnSQL.SQL.Add(@"  IDMDPRIORITY,  ");
        UnSQL.SQL.Add(@"  PRIORITYNAME ");
        UnSQL.SQL.Add(@" )  ");
        UnSQL.SQL.Add(@" VALUES ( ");
        UnSQL.SQL.Add(@"  @[IDMDPRIORITY], ");
        UnSQL.SQL.Add(@"  @[PRIORITYNAME] ");
        UnSQL.SQL.Add(@" ) ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDPRIORITY_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.Catalog.Methods.MDPRIORITY_GETID(MDPRIORITY);
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_MDPRIORITY.js Persistence.Catalog.Methods.MDPRIORITY_ADD", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Catalog.Methods.MDPRIORITY_UPD = function (MDPRIORITY) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName, MDPRIORITY.IDMDPRIORITY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.MDPRIORITY.PRIORITYNAME.FieldName, MDPRIORITY.PRIORITYNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   MDPRIORITY_UPD   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDPRIORITY_UPD", @"MDPRIORITY_UPD description.");
        UnSQL.SQL.Add(@" UPDATE MDPRIORITY SET ");
        UnSQL.SQL.Add(@"  IDMDPRIORITY=@[IDMDPRIORITY],  ");
        UnSQL.SQL.Add(@"  PRIORITYNAME=@[PRIORITYNAME] ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  IDMDPRIORITY=@[IDMDPRIORITY] AND  ");
        UnSQL.SQL.Add(@"  PRIORITYNAME=@[PRIORITYNAME] ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDPRIORITY_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_MDPRIORITY.js Persistence.Catalog.Methods.MDPRIORITY_UPD", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.Catalog.Methods.MDPRIORITY_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.Catalog.Methods.MDPRIORITY_DELIDMDPRIORITY(/*String StrIDMDPRIORITY*/Param);
    }
    else {
        Res = Persistence.Catalog.Methods.MDPRIORITY_DELMDPRIORITY(Param);
    }
    return (Res);
}

Persistence.Catalog.Methods.MDPRIORITY_DELIDMDPRIORITY = function (/*String StrIDMDPRIORITY*/IDMDPRIORITY) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDMDPRIORITY == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName, " = " + UsrCfg.InternoAtisNames.MDPRIORITY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDMDPRIORITY = " IN (" + StrIDMDPRIORITY + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName, StrIDMDPRIORITY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName, IDMDPRIORITY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   MDPRIORITY_DEL_1   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDPRIORITY_DEL_1", @"MDPRIORITY_DEL_1 description.");
        UnSQL.SQL.Add(@" DELETE MDPRIORITY  ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  IDMDPRIORITY=@[IDMDPRIORITY] AND  ");
        UnSQL.SQL.Add(@"  PRIORITYNAME=@[PRIORITYNAME] ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDPRIORITY_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_MDPRIORITY.js Persistence.Catalog.Methods.MDPRIORITY_DEL", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Catalog.Methods.MDPRIORITY_DELMDPRIORITY = function (MDPRIORITY) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName, MDPRIORITY.IDMDPRIORITY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.MDPRIORITY.PRIORITYNAME.FieldName, MDPRIORITY.PRIORITYNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   MDPRIORITY_DEL_2   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDPRIORITY_DEL_2", @"MDPRIORITY_DEL_2 description.");
        UnSQL.SQL.Add(@" DELETE MDPRIORITY  ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  IDMDPRIORITY=@[IDMDPRIORITY] AND  ");
        UnSQL.SQL.Add(@"  PRIORITYNAME=@[PRIORITYNAME] ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDPRIORITY_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_MDPRIORITY.js Persistence.Catalog.Methods.MDPRIORITY_DEL", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
//**********************   METHODS   ********************************************************************************************
Persistence.Catalog.Methods.MDPRIORITY_ListSetID = function (MDPRIORITYList, IDMDPRIORITY) {
    for (i = 0; i < MDPRIORITYList.length; i++) {
        if (IDMDPRIORITY == MDPRIORITYList[i].IDMDPRIORITYR)
            return (MDPRIORITYList[i]);
    }
    var UnMDPRIORITY = new Persistence.Catalog.Properties.TMDPRIORITY();
    UnMDPRIORITY.IDMDPRIORITY = IDMDPRIORITY;
    return (UnMDPRIORITY);
}
Persistence.Catalog.Methods.MDPRIORITY_ListAdd = function (MDPRIORITYList, MDPRIORITY) {
    var i = Persistence.Catalog.Methods.MDPRIORITY_ListGetIndex(MDPRIORITYList, MDPRIORITY);
    if (i == -1) MDPRIORITYList.push(MDPRIORITY);
    else {
        MDPRIORITYList[i].IDMDPRIORITY = MDPRIORITY.IDMDPRIORITY;

    }
}
//CJRC_26072018
Persistence.Catalog.Methods.MDPRIORITY_ListGetIndex = function (MDPRIORITYList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.Catalog.Methods.MDPRIORITY_ListGetIndexIDMDPRIORITY(MDPRIORITYList, Param);

    }
    else {
        Res = Persistence.Catalog.Methods.MDPRIORITY_ListGetIndexPRIORITYNAME(MDPRIORITYList, Param);
    }
    return (Res);
}

Persistence.Catalog.Methods.MDPRIORITY_ListGetIndexPRIORITYNAME = function (MDPRIORITYList, PRIORITYNAME) {
    for (i = 0; i < MDPRIORITYList.length; i++) {
        if (PRIORITYNAME == MDPRIORITYList[i].IDMDPRIORITY)
            return (i);
    }
    return (-1);
}
Persistence.Catalog.Methods.MDPRIORITY_ListGetIndexIDMDPRIORITY = function (MDPRIORITYList, IDMDPRIORITY) {
    for (i = 0; i < MDPRIORITYList.length; i++) {
        if (IDMDPRIORITY == MDPRIORITYList[i].IDMDPRIORITY)
            return (i);
    }
    return (-1);
}
//String Function 
Persistence.Catalog.Methods.MDPRIORITYListtoStr = function (MDPRIORITYList) {
    var Res = Persistence.Catalog.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(MDPRIORITYList.length, Longitud, Texto);
        for (Counter = 0; Counter < MDPRIORITYList.length; Counter++) {
            var UnMDPRIORITY = MDPRIORITYList[Counter];
            UnMDPRIORITY.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Catalog.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
    
}
Persistence.Catalog.Methods.StrtoMDPRIORITY = function (ProtocoloStr, MDPRIORITYList) {
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        MDPRIORITYList.length = 0;
        if (Persistence.Catalog.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnMDPRIORITY = new Persistence.Demo.Properties.TMDPRIORITY(); //Mode new row
                UnMDPRIORITY.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.MDPRIORITY_ListAdd(MDPRIORITYList, UnMDPRIORITY);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
    
}
//Socket Function 
Persistence.Catalog.Methods.MDPRIORITYListToByte = function (MDPRIORITYList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, MDPRIORITYList.length - idx);
    for (i = idx; i < MDPRIORITYList.length; i++) {
        MDPRIORITYList[i].ToByte(MemStream);
    }
}
Persistence.Catalog.Methods.ByteToMDPRIORITYList = function (MDPRIORITYList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnMDPRIORITY = new Persistence.Catalog.Properties.TMDPRIORITY();
        UnMDPRIORITY.ByteTo(MemStream);
        Persistence.Catalog.Methods.MDPRIORITY_ListAdd(MDPRIORITYList, UnMDPRIORITY);
    }
}

