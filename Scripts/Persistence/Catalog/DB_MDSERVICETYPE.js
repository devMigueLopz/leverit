﻿//**********************   PROPERTIES   *****************************************************************************************
Persistence.Catalog.Properties.TMDSERVICETYPE = function () {
    this.IDMDSERVICETYPE =0;
    this.SERVICETYPEDESCRIPTION ="";
    this.SERVICETYPENAME ="";
    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDSERVICETYPE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.SERVICETYPEDESCRIPTION);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.SERVICETYPENAME);
    }
    this.ByteTo = function (MemStream) {
        this.IDMDSERVICETYPE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.SERVICETYPEDESCRIPTION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.SERVICETYPENAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDMDSERVICETYPE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.SERVICETYPEDESCRIPTION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.SERVICETYPENAME, Longitud, Texto);
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDMDSERVICETYPE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.SERVICETYPEDESCRIPTION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.SERVICETYPENAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
    }
}
//**********************   METHODS server  ********************************************************************************************
Persistence.Catalog.Methods.MDSERVICETYPE_Fill = function (MDSERVICETYPEList) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    MDSERVICETYPEList.length = 0;
    try {
        var DS_MDSERVICETYPE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDSERVICETYPE_GET", new Array());
        if (ResErr.NotError) {
            if (DS_MDSERVICETYPE.DataSet.RecordCount > 0) {
                DS_MDSERVICETYPE.DataSet.First();
                while (!(DS_MDSERVICETYPE.DataSet.Eof)) {
                    var MDSERVICETYPE = new Persistence.Catalog.Properties.TMDSERVICETYPE();
                    //Other
                    MDSERVICETYPE.IDMDSERVICETYPE = DS_MDSERVICETYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICETYPE.IDMDSERVICETYPE.FieldName).asInt32();
                    MDSERVICETYPE.SERVICETYPEDESCRIPTION = DS_MDSERVICETYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICETYPE.SERVICETYPEDESCRIPTION.FieldName).asString();
                    MDSERVICETYPE.SERVICETYPENAME = DS_MDSERVICETYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICETYPE.SERVICETYPENAME.FieldName).asString();
                    MDSERVICETYPEList.push(MDSERVICETYPE);
                    DS_MDSERVICETYPE.DataSet.Next();
                }
            }
            else {
                DS_MDSERVICETYPE.ResErr.NotError = false;
                DS_MDSERVICETYPE.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_MDSERVICETYPE.js Persistence.Catalog.Methods.MDSERVICETYPE_Fill", e);
    }
    return (ResErr);
}
Persistence.Catalog.Methods.MDSERVICETYPE_GETID = function (MDSERVICETYPE) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICETYPE.IDMDSERVICETYPE.FieldName, MDSERVICETYPE.IDMDSERVICETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICETYPE.SERVICETYPEDESCRIPTION.FieldName, MDSERVICETYPE.SERVICETYPEDESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICETYPE.SERVICETYPENAME.FieldName, MDSERVICETYPE.SERVICETYPENAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   MDSERVICETYPE_GETID   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDSERVICETYPE_GETID", @"MDSERVICETYPE_GETID description.");
        UnSQL.SQL.Add(@" SELECT  ");
        UnSQL.SQL.Add(@"  MDSERVICETYPE.IDMDSERVICETYPE, ");
        UnSQL.SQL.Add(@"  MDSERVICETYPE.SERVICETYPEDESCRIPTION, ");
        UnSQL.SQL.Add(@"  MDSERVICETYPE.SERVICETYPENAME ");
        UnSQL.SQL.Add(@" FROM  MDSERVICETYPE ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  MDSERVICETYPE.IDMDSERVICETYPE=@[IDMDSERVICETYPE] and  ");
        UnSQL.SQL.Add(@"  MDSERVICETYPE.SERVICETYPEDESCRIPTION=@[SERVICETYPEDESCRIPTION] and  ");
        UnSQL.SQL.Add(@"  MDSERVICETYPE.SERVICETYPENAME=@[SERVICETYPENAME] ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var DS_MDSERVICETYPE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDSERVICETYPE_GETID", Param.ToBytes());
        ResErr = DS_MDSERVICETYPE.ResErr;
        if (ResErr.NotError) {
            if (DS_MDSERVICETYPE.DataSet.RecordCount > 0) {
                DS_MDSERVICETYPE.DataSet.First();
                //Other
                MDSERVICETYPE.IDMDSERVICETYPE = DS_MDSERVICETYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICETYPE.IDMDSERVICETYPE.FieldName).asInt32();
                MDSERVICETYPE.SERVICETYPEDESCRIPTION = DS_MDSERVICETYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICETYPE.SERVICETYPEDESCRIPTION.FieldName).asString();
                MDSERVICETYPE.SERVICETYPENAME = DS_MDSERVICETYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICETYPE.SERVICETYPENAME.FieldName).asString();
            }
            else {
                DS_MDSERVICETYPE.ResErr.NotError = false;
                DS_MDSERVICETYPE.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_MDSERVICETYPE.js Persistence.Catalog.Methods.MDSERVICETYPE_GETID", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Catalog.Methods.MDSERVICETYPE_ADD = function (MDSERVICETYPE) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICETYPE.IDMDSERVICETYPE.FieldName, MDSERVICETYPE.IDMDSERVICETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICETYPE.SERVICETYPEDESCRIPTION.FieldName, MDSERVICETYPE.SERVICETYPEDESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICETYPE.SERVICETYPENAME.FieldName, MDSERVICETYPE.SERVICETYPENAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   MDSERVICETYPE_ADD   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDSERVICETYPE_ADD", @"MDSERVICETYPE_ADD description.");
        UnSQL.SQL.Add(@" INSERT INTO MDST_EFKE_LOG( ");
        UnSQL.SQL.Add(@" )  ");
        UnSQL.SQL.Add(@" VALUES ( ");
        UnSQL.SQL.Add(@" ) ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDSERVICETYPE_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.Catalog.Methods.MDSERVICETYPE_GETID(MDSERVICETYPE);
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_MDSERVICETYPE.js Persistence.Catalog.Methods.MDSERVICETYPE_ADD", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Catalog.Methods.MDSERVICETYPE_UPD = function (MDSERVICETYPE) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICETYPE.IDMDSERVICETYPE.FieldName, MDSERVICETYPE.IDMDSERVICETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICETYPE.SERVICETYPEDESCRIPTION.FieldName, MDSERVICETYPE.SERVICETYPEDESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICETYPE.SERVICETYPENAME.FieldName, MDSERVICETYPE.SERVICETYPENAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   MDSERVICETYPE_UPD   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDSERVICETYPE_UPD", @"MDSERVICETYPE_UPD description.");
        UnSQL.SQL.Add(@" UPDATE MDST_EFKE_LOG SET ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDSERVICETYPE_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_MDSERVICETYPE.js Persistence.Catalog.Methods.MDSERVICETYPE_UPD", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.Catalog.Methods.MDSERVICETYPE_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.Catalog.Methods.MDSERVICETYPE_DELIDMDSERVICETYPE(/*String StrIDMDSERVICETYPE*/Param);
    }
    else {
        Res = Persistence.Catalog.Methods.MDSERVICETYPE_DELMDSERVICETYPE(Param);
    }
    return (Res);
}

Persistence.Catalog.Methods.MDSERVICETYPE_DELIDMDSERVICETYPE = function (/*String StrIDMDSERVICETYPE*/IDMDSERVICETYPE) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDMDSERVICETYPE == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDSERVICETYPE.IDMDSERVICETYPE.FieldName, " = " + UsrCfg.InternoAtisNames.MDSERVICETYPE.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDSERVICETYPE.IDMDSERVICETYPE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDMDSERVICETYPE = " IN (" + StrIDMDSERVICETYPE + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDSERVICETYPE.IDMDSERVICETYPE.FieldName, StrIDMDSERVICETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICETYPE.IDMDSERVICETYPE.FieldName, IDMDSERVICETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   MDSERVICETYPE_DEL_1   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDSERVICETYPE_DEL_1", @"MDSERVICETYPE_DEL_1 description.");
        UnSQL.SQL.Add(@" DELETE MDST_EFKE_LOG  ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDSERVICETYPE_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_MDSERVICETYPE.js Persistence.Catalog.Methods.MDSERVICETYPE_DEL", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Catalog.Methods.MDSERVICETYPE_DELMDSERVICETYPE = function (MDSERVICETYPE) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICETYPE.IDMDSERVICETYPE.FieldName, MDSERVICETYPE.IDMDSERVICETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICETYPE.SERVICETYPEDESCRIPTION.FieldName, MDSERVICETYPE.SERVICETYPEDESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDSERVICETYPE.SERVICETYPENAME.FieldName, MDSERVICETYPE.SERVICETYPENAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   MDSERVICETYPE_DEL_2   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDSERVICETYPE_DEL_2", @"MDSERVICETYPE_DEL_2 description.");
        UnSQL.SQL.Add(@" DELETE MDST_EFKE_LOG  ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDSERVICETYPE_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_MDSERVICETYPE.js Persistence.Catalog.Methods.MDSERVICETYPE_DEL", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
//**********************   METHODS   ********************************************************************************************
Persistence.Catalog.Methods.MDSERVICETYPE_ListSetID = function (MDSERVICETYPEList, IDMDSERVICETYPE) {
    for (i = 0; i < MDSERVICETYPEList.length; i++) {
        if (IDMDSERVICETYPE == MDSERVICETYPEList[i].IDMDSERVICETYPER)
        return (MDSERVICETYPEList[i]);
    }
    var UnMDSERVICETYPE = new Persistence.Catalog.Properties.TMDSERVICETYPE();
    UnMDSERVICETYPE.IDMDSERVICETYPE = IDMDSERVICETYPE;
    return (UnMDSERVICETYPE);
}
Persistence.Catalog.Methods.MDSERVICETYPE_ListAdd = function (MDSERVICETYPEList, MDSERVICETYPE) {
    var i = Persistence.Catalog.Methods.MDSERVICETYPE_ListGetIndex(MDSERVICETYPEList, MDSERVICETYPE);
    if (i == -1) MDSERVICETYPEList.push(MDSERVICETYPE);
    else {
        MDSERVICETYPEList[i].IDMDSERVICETYPE = MDSERVICETYPE.IDMDSERVICETYPE;
        MDSERVICETYPEList[i].SERVICETYPEDESCRIPTION = MDSERVICETYPE.SERVICETYPEDESCRIPTION;
        MDSERVICETYPEList[i].SERVICETYPENAME = MDSERVICETYPE.SERVICETYPENAME;

    }
}
//CJRC_26072018
Persistence.Catalog.Methods.MDSERVICETYPE_ListGetIndex = function (MDSERVICETYPEList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.Catalog.Methods.MDSERVICETYPE_ListGetIndexIDMDSERVICETYPE(MDSERVICETYPEList, Param);

    }
    else {
        Res = Persistence.Catalog.Methods.MDSERVICETYPE_ListGetIndexMDSERVICETYPE(MDSERVICETYPEList, Param);
    }
    return (Res);
}

Persistence.Catalog.Methods.MDSERVICETYPE_ListGetIndexMDSERVICETYPE = function (MDSERVICETYPEList, MDSERVICETYPE) {
    for (i = 0; i < MDSERVICETYPEList.length; i++) {
        if (MDSERVICETYPE.IDMDSERVICETYPE == MDSERVICETYPEList[i].IDMDSERVICETYPE)
        return (i);
    }
    return (-1);
}
Persistence.Catalog.Methods.MDSERVICETYPE_ListGetIndexIDMDSERVICETYPE = function (MDSERVICETYPEList, IDMDSERVICETYPE) {
    for (i = 0; i < MDSERVICETYPEList.length; i++) {
        if (IDMDSERVICETYPE == MDSERVICETYPEList[i].IDMDSERVICETYPE)
        return (i);
    }
    return (-1);
}
//String Function 
Persistence.Catalog.Methods.MDSERVICETYPEListtoStr = function (MDSERVICETYPEList) {
    var Res = Persistence.Catalog.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(MDSERVICETYPEList.length, Longitud, Texto);
        for (Counter = 0; Counter < MDSERVICETYPEList.length; Counter++) {
            var UnMDSERVICETYPE = MDSERVICETYPEList[Counter];
            UnMDSERVICETYPE.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Catalog.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
   
}
Persistence.Catalog.Methods.StrtoMDSERVICETYPE = function (ProtocoloStr, MDSERVICETYPEList) {
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        MDSERVICETYPEList.length = 0;
        if (Persistence.Catalog.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnMDSERVICETYPE = new Persistence.Demo.Properties.TMDSERVICETYPE(); //Mode new row
                UnMDSERVICETYPE.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.MDSERVICETYPE_ListAdd(MDSERVICETYPEList, UnMDSERVICETYPE);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
    
}
//Socket Function 
Persistence.Catalog.Methods.MDSERVICETYPEListToByte = function (MDSERVICETYPEList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, MDSERVICETYPEList.length - idx);
    for (i = idx; i < MDSERVICETYPEList.length; i++) {
        MDSERVICETYPEList[i].ToByte(MemStream);
    }
}
Persistence.Catalog.Methods.ByteToMDSERVICETYPEList = function (MDSERVICETYPEList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnMDSERVICETYPE = new Persistence.Catalog.Properties.TMDSERVICETYPE();
        UnMDSERVICETYPE.ByteTo(MemStream);
        Persistence.Catalog.Methods.MDSERVICETYPE_ListAdd(MDSERVICETYPEList, UnMDSERVICETYPE);
    }
}

