﻿//**********************   PROPERTIES   *****************************************************************************************
Persistence.Catalog.Properties.TMDPRIORITYMATRIX = function () {
    this.IDMDIMPACT = 0;
    this.IDMDPRIORITY = 0;
    this.IDMDURGENCY = 0;
    this.IMPACTNAME = "";
    this.PRIORITYNAME = "";
    this.URGENCYNAME = "";
    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDIMPACT);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDPRIORITY);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDURGENCY);
    }
    this.ByteTo = function (MemStream) {
        this.IDMDIMPACT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDPRIORITY = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDURGENCY = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDMDIMPACT, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDPRIORITY, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDURGENCY, Longitud, Texto);
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDMDIMPACT = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDPRIORITY = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDURGENCY = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
    }
}
//**********************   METHODS server  ********************************************************************************************
Persistence.Catalog.Methods.MDPRIORITYMATRIX_Fill = function (MDPRIORITYMATRIXList) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    MDPRIORITYMATRIXList.length = 0;
    try {
        var DS_MDPRIORITYMATRIX = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDPRIORITYMATRIX_GET", new Array());
        ResErr = DS_MDPRIORITYMATRIX.ResErr;
        if (ResErr.NotError) {
            if (DS_MDPRIORITYMATRIX.DataSet.RecordCount > 0) {
                DS_MDPRIORITYMATRIX.DataSet.First();
                while (!(DS_MDPRIORITYMATRIX.DataSet.Eof)) {
                    var MDPRIORITYMATRIX = new Persistence.Catalog.Properties.TMDPRIORITYMATRIX();
                    //Other
                    MDPRIORITYMATRIX.IDMDIMPACT = DS_MDPRIORITYMATRIX.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDIMPACT.FieldName).asInt32();
                    MDPRIORITYMATRIX.IMPACTNAME = DS_MDPRIORITYMATRIX.DataSet.RecordSet.FieldName("IMPACTNAME").asString();
                    MDPRIORITYMATRIX.IDMDPRIORITY = DS_MDPRIORITYMATRIX.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDPRIORITY.FieldName).asInt32();
                    MDPRIORITYMATRIX.PRIORITYNAME = DS_MDPRIORITYMATRIX.DataSet.RecordSet.FieldName("PRIORITYNAME").asString();
                    MDPRIORITYMATRIX.IDMDURGENCY = DS_MDPRIORITYMATRIX.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDURGENCY.FieldName).asInt32();
                    MDPRIORITYMATRIX.URGENCYNAME = DS_MDPRIORITYMATRIX.DataSet.RecordSet.FieldName("URGENCYNAME").asString();
                    //Other
                    MDPRIORITYMATRIXList.push(MDPRIORITYMATRIX);
                    DS_MDPRIORITYMATRIX.DataSet.Next();
                }
            }
            else {
                DS_MDPRIORITYMATRIX.ResErr.NotError = false;
                DS_MDPRIORITYMATRIX.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_MDPRIORITYMATRIX.js Persistence.Catalog.Methods.MDPRIORITYMATRIX_Fill", e);
    }
    return (ResErr);
}
Persistence.Catalog.Methods.MDPRIORITYMATRIX_ADD = function (MDPRIORITYMATRIX) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDIMPACT.FieldName, MDPRIORITYMATRIX.IDMDIMPACT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDPRIORITY.FieldName, MDPRIORITYMATRIX.IDMDPRIORITY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDURGENCY.FieldName, MDPRIORITYMATRIX.IDMDURGENCY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDPRIORITYMATRIX_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_MDPRIORITYMATRIX.js Persistence.Catalog.Methods.MDPRIORITYMATRIX_ADD", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Catalog.Methods.MDPRIORITYMATRIX_UPD = function (MDPRIORITYMATRIX) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDIMPACT.FieldName, MDPRIORITYMATRIX.IDMDIMPACT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDPRIORITY.FieldName, MDPRIORITYMATRIX.IDMDPRIORITY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDURGENCY.FieldName, MDPRIORITYMATRIX.IDMDURGENCY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   MDPRIORITYMATRIX_UPD   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDPRIORITYMATRIX_UPD", @"MDPRIORITYMATRIX_UPD description.");
        UnSQL.SQL.Add(@" UPDATE MDPRIORITYMATRIX SET ");
        UnSQL.SQL.Add(@"  IDMDIMPACT=@[IDMDIMPACT],  ");
        UnSQL.SQL.Add(@"  IDMDPRIORITY=@[IDMDPRIORITY],  ");
        UnSQL.SQL.Add(@"  IDMDURGENCY=@[IDMDURGENCY] ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  IDMDIMPACT=@[IDMDIMPACT] AND  ");
        UnSQL.SQL.Add(@"  IDMDPRIORITY=@[IDMDPRIORITY] AND  ");
        UnSQL.SQL.Add(@"  IDMDURGENCY=@[IDMDURGENCY] ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDPRIORITYMATRIX_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_MDPRIORITYMATRIX.js Persistence.Catalog.Methods.MDPRIORITYMATRIX_UPD", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.Catalog.Methods.MDPRIORITYMATRIX_DEL = function (Param1, Param2) {
    var Res = -1
    if ((typeof (Param1) == 'number') || (typeof (Param1) == 'string')) {
        Res = Persistence.Catalog.Methods.MDPRIORITYMATRIX_DELIDMDIMPACT(Param1, Param2);
    }
    else {
        Res = Persistence.Catalog.Methods.MDPRIORITYMATRIX_DELMDPRIORITYMATRIX(Param1);
    }
    return (Res);
}

Persistence.Catalog.Methods.MDPRIORITYMATRIX_DELIDMDIMPACT = function (IDMDIMPACT, IDMDURGENCY) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDIMPACT.FieldName, IDMDIMPACT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDURGENCY.FieldName, IDMDURGENCY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   MDPRIORITYMATRIX_DEL_1   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDPRIORITYMATRIX_DEL_1", @"MDPRIORITYMATRIX_DEL_1 description.");
        UnSQL.SQL.Add(@" DELETE MDPRIORITYMATRIX  ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  IDMDIMPACT=@[IDMDIMPACT] AND  ");
        UnSQL.SQL.Add(@"  IDMDPRIORITY=@[IDMDPRIORITY] AND  ");
        UnSQL.SQL.Add(@"  IDMDURGENCY=@[IDMDURGENCY] ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDPRIORITYMATRIX_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_MDPRIORITYMATRIX.js Persistence.Catalog.Methods.MDPRIORITYMATRIX_DEL", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Catalog.Methods.MDPRIORITYMATRIX_DELMDPRIORITYMATRIX = function (MDPRIORITYMATRIX) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDIMPACT.FieldName, MDPRIORITYMATRIX.IDMDIMPACT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDURGENCY.FieldName, MDPRIORITYMATRIX.IDMDURGENCY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   MDPRIORITYMATRIX_DEL_2   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDPRIORITYMATRIX_DEL_2", @"MDPRIORITYMATRIX_DEL_2 description.");
        UnSQL.SQL.Add(@" DELETE MDPRIORITYMATRIX  ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  IDMDIMPACT=@[IDMDIMPACT] AND  ");
        UnSQL.SQL.Add(@"  IDMDPRIORITY=@[IDMDPRIORITY] AND  ");
        UnSQL.SQL.Add(@"  IDMDURGENCY=@[IDMDURGENCY] ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDPRIORITYMATRIX_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_MDPRIORITYMATRIX.js Persistence.Catalog.Methods.MDPRIORITYMATRIX_DEL", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
//**********************   METHODS   ********************************************************************************************
Persistence.Catalog.Methods.MDPRIORITYMATRIX_ListSetID = function (MDPRIORITYMATRIXList, IDMDIMPACT, IDMDURGENCY) {
    for (i = 0; i < MDPRIORITYMATRIXList.length; i++) {
        if ((IDMDIMPACT == MDPRIORITYMATRIXList[i].IDMDIMPACT) && (IDMDURGENCY == MDPRIORITYMATRIXList[i].IDMDURGENCY))
            return (MDPRIORITYMATRIXList[i]);
    }
    var UnMDPRIORITYMATRIX = new Persistence.Catalog.Properties.TMDPRIORITYMATRIX();
    UnMDPRIORITYMATRIX.IDMDIMPACT = IDMDIMPACT;
    UnMDPRIORITYMATRIX.IDMDURGENCY = IDMDURGENCY;
    return (UnMDPRIORITYMATRIX);
}
Persistence.Catalog.Methods.MDPRIORITYMATRIX_ListAdd = function (MDPRIORITYMATRIXList, MDPRIORITYMATRIX) {
    var i = Persistence.Catalog.Methods.MDPRIORITYMATRIX_ListGetIndex(MDPRIORITYMATRIXList, MDPRIORITYMATRIX);
    if (i == -1) MDPRIORITYMATRIXList.push(MDPRIORITYMATRIX);
    else {
        MDPRIORITYMATRIXList[i].IDMDIMPACT = MDPRIORITYMATRIX.IDMDIMPACT;
        MDPRIORITYMATRIXList[i].IDMDPRIORITY = MDPRIORITYMATRIX.IDMDPRIORITY;
        MDPRIORITYMATRIXList[i].IDMDURGENCY = MDPRIORITYMATRIX.IDMDURGENCY;
    }
}

Persistence.Catalog.Methods.MDPRIORITYMATRIX_ListGetIndex = function (MDPRIORITYMATRIXList, Param1, Param2) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.Catalog.Methods.MDPRIORITYMATRIX_ListGetIndexIDMDURGENCY(MDPRIORITYMATRIXList, Param1,Param2);

    }
    else {
        Res = Persistence.Catalog.Methods.MDPRIORITYMATRIX_ListGetIndexMDPRIORITYMATRIX(MDPRIORITYMATRIXList, Param1);
    }
    return (Res);
}

Persistence.Catalog.Methods.MDPRIORITYMATRIX_ListGetIndexMDPRIORITYMATRIX = function (MDPRIORITYMATRIXList, MDPRIORITYMATRIX) {
    for (i = 0; i < MDPRIORITYMATRIXList.length; i++) {
        if ((MDPRIORITYMATRIX.IDMDURGENCY == MDPRIORITYMATRIXList[i].IDMDURGENCY) && (MDPRIORITYMATRIX.IDMDIMPACT == MDPRIORITYMATRIXList[i].IDMDIMPACT))
            return (i);
    }
    return (-1);
}
Persistence.Catalog.Methods.MDPRIORITYMATRIX_ListGetIndexIDMDURGENCY = function (MDPRIORITYMATRIXList, IDMDIMPACT, IDMDURGENCY) {
    for (i = 0; i < MDPRIORITYMATRIXList.length; i++) {
        if ((IDMDIMPACT == MDPRIORITYMATRIXList[i].IDMDIMPACT) && (IDMDURGENCY == MDPRIORITYMATRIXList[i].IDMDURGENCY))
            return (i);
    }
    return (-1);
}
//String Function 
Persistence.Catalog.Methods.MDPRIORITYMATRIXListtoStr = function (MDPRIORITYMATRIXList) {
    var Res = Persistence.Catalog.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(MDPRIORITYMATRIXList.length, Longitud, Texto);
        for (Counter = 0; Counter < MDPRIORITYMATRIXList.length; Counter++) {
            var UnMDPRIORITYMATRIX = MDPRIORITYMATRIXList[Counter];
            UnMDPRIORITYMATRIX.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Catalog.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
/////**************************************
    var Res = Persistence.Catalog.Properties._Version + "(1)0";
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(MDPRIORITYMATRIXList.length, Longitud, Texto);
        for (Counter = 0; Counter < MDPRIORITYMATRIXList.length; Counter++) {
            var UnMDPRIORITYMATRIX = MDPRIORITYMATRIXList[Counter];
            UnMDPRIORITYMATRIX.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Catalog.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
        SysCfg.Log.Methods.WriteLog("DB_MDPRIORITYMATRIX.js Persistence.Catalog.Methods.MDPRIORITYMATRIXListtoStr", e);
    }
    return (Res);
}
Persistence.Catalog.Methods.StrtoMDPRIORITYMATRIX = function (ProtocoloStr, MDPRIORITYMATRIXList) {
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        MDPRIORITYMATRIXList.length = 0;
        if (Persistence.Catalog.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnMDPRIORITYMATRIX = new Persistence.Demo.Properties.TMDPRIORITYMATRIX(); //Mode new row
                UnMDPRIORITYMATRIX.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.MDPRIORITYMATRIX_ListAdd(MDPRIORITYMATRIXList, UnMDPRIORITYMATRIX);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
    
}
//Socket Function 
Persistence.Catalog.Methods.MDPRIORITYMATRIXListToByte = function (MDPRIORITYMATRIXList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, MDPRIORITYMATRIXList.length - idx);
    for (i = idx; i < MDPRIORITYMATRIXList.length; i++) {
        MDPRIORITYMATRIXList[i].ToByte(MemStream);
    }
}
Persistence.Catalog.Methods.ByteToMDPRIORITYMATRIXList = function (MDPRIORITYMATRIXList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnMDPRIORITYMATRIX = new Persistence.Catalog.Properties.TMDPRIORITYMATRIX();
        UnMDPRIORITYMATRIX.ByteTo(MemStream);
        Persistence.Catalog.Methods.MDPRIORITYMATRIX_ListAdd(MDPRIORITYMATRIXList, UnMDPRIORITYMATRIX);
    }
}

