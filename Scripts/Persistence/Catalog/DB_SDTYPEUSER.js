﻿//**********************   PROPERTIES   *****************************************************************************************
Persistence.Catalog.Properties.TSDTYPEUSER = function () {
    this.IDSDTYPEUSER = 0;
    this.TYPEUSERDESCRIPTION = "";
    this.TYPEUSERNAME = "";
    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDTYPEUSER);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.TYPEUSERDESCRIPTION);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.TYPEUSERNAME);
    }
    this.ByteTo = function (MemStream) {
        this.IDSDTYPEUSER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.TYPEUSERDESCRIPTION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.TYPEUSERNAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDSDTYPEUSER, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.TYPEUSERDESCRIPTION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.TYPEUSERNAME, Longitud, Texto);
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDSDTYPEUSER = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.TYPEUSERDESCRIPTION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.TYPEUSERNAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
    }
}
//**********************   METHODS server  ********************************************************************************************
Persistence.Catalog.Methods.SDTYPEUSER_Fill = function (SDTYPEUSERList) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    SDTYPEUSERList.length = 0;
    try {
        var DS_SDTYPEUSER = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "SDTYPEUSER_GET", new Array());
        ResErr = DS_SDTYPEUSER.ResErr;
        if (ResErr.NotError) {
            if (DS_SDTYPEUSER.DataSet.RecordCount > 0) {
                DS_SDTYPEUSER.DataSet.First();
                while (!(DS_SDTYPEUSER.DataSet.Eof)) {
                    var SDTYPEUSER = new Persistence.Catalog.Properties.TSDTYPEUSER();
                    SDTYPEUSER.IDSDTYPEUSER = DS_SDTYPEUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDTYPEUSER.IDSDTYPEUSER.FieldName).asInt32();
                    SDTYPEUSER.TYPEUSERDESCRIPTION = DS_SDTYPEUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDTYPEUSER.TYPEUSERDESCRIPTION.FieldName).asString();
                    SDTYPEUSER.TYPEUSERNAME = DS_SDTYPEUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDTYPEUSER.TYPEUSERNAME.FieldName).asString();

                    //Other
                    SDTYPEUSERList.push(SDTYPEUSER);
                    DS_SDTYPEUSER.DataSet.Next();
                }
            }
            else {
                DS_SDTYPEUSER.ResErr.NotError = false;
                DS_SDTYPEUSER.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_SDTYPEUSER.js Persistence.Catalog.Methods.SDTYPEUSER_Fill", e);
    }
    return (ResErr);
}
Persistence.Catalog.Methods.SDTYPEUSER_GETID = function (SDTYPEUSER) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.SDTYPEUSER.IDSDTYPEUSER.FieldName, SDTYPEUSER.IDSDTYPEUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.SDTYPEUSER.TYPEUSERDESCRIPTION.FieldName, SDTYPEUSER.TYPEUSERDESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.SDTYPEUSER.TYPEUSERNAME.FieldName, SDTYPEUSER.TYPEUSERNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   SDTYPEUSER_GETID   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"SDTYPEUSER_GETID", @"SDTYPEUSER_GETID description.");
        UnSQL.SQL.Add(@" SELECT  ");
        UnSQL.SQL.Add(@"  SDTYPEUSER.IDSDTYPEUSER, ");
        UnSQL.SQL.Add(@"  SDTYPEUSER.TYPEUSERDESCRIPTION, ");
        UnSQL.SQL.Add(@"  SDTYPEUSER.TYPEUSERNAME ");
        UnSQL.SQL.Add(@" FROM  SDTYPEUSER ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  SDTYPEUSER.IDSDTYPEUSER=@[IDSDTYPEUSER] and  ");
        UnSQL.SQL.Add(@"  SDTYPEUSER.TYPEUSERDESCRIPTION=@[TYPEUSERDESCRIPTION] and  ");
        UnSQL.SQL.Add(@"  SDTYPEUSER.TYPEUSERNAME=@[TYPEUSERNAME] ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var DS_SDTYPEUSER = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "SDTYPEUSER_GETID", Param.ToBytes());
        ResErr = DS_SDTYPEUSER.ResErr;
        if (ResErr.NotError) {
            if (DS_SDTYPEUSER.DataSet.RecordCount > 0) {
                DS_SDTYPEUSER.DataSet.First();
                SDTYPEUSER.IDSDTYPEUSER = DS_SDTYPEUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDTYPEUSER.IDSDTYPEUSER.FieldName).asInt32();
                //Other
                SDTYPEUSER.TYPEUSERDESCRIPTION = DS_SDTYPEUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDTYPEUSER.TYPEUSERDESCRIPTION.FieldName).asString();
                SDTYPEUSER.TYPEUSERNAME = DS_SDTYPEUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDTYPEUSER.TYPEUSERNAME.FieldName).asString();
            }
            else {
                DS_SDTYPEUSER.ResErr.NotError = false;
                DS_SDTYPEUSER.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_SDTYPEUSER.js Persistence.Catalog.Methods.SDTYPEUSER_GETID", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Catalog.Methods.SDTYPEUSER_ADD = function (SDTYPEUSER) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.SDTYPEUSER.IDSDTYPEUSER.FieldName, SDTYPEUSER.IDSDTYPEUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.SDTYPEUSER.TYPEUSERDESCRIPTION.FieldName, SDTYPEUSER.TYPEUSERDESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.SDTYPEUSER.TYPEUSERNAME.FieldName, SDTYPEUSER.TYPEUSERNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   SDTYPEUSER_ADD   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"SDTYPEUSER_ADD", @"SDTYPEUSER_ADD description.");
        UnSQL.SQL.Add(@" INSERT INTO SDTYPEUSER( ");
        UnSQL.SQL.Add(@"  IDSDTYPEUSER,  ");
        UnSQL.SQL.Add(@"  TYPEUSERDESCRIPTION,  ");
        UnSQL.SQL.Add(@"  TYPEUSERNAME ");
        UnSQL.SQL.Add(@" )  ");
        UnSQL.SQL.Add(@" VALUES ( ");
        UnSQL.SQL.Add(@"  @[IDSDTYPEUSER], ");
        UnSQL.SQL.Add(@"  @[TYPEUSERDESCRIPTION], ");
        UnSQL.SQL.Add(@"  @[TYPEUSERNAME] ");
        UnSQL.SQL.Add(@" ) ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "SDTYPEUSER_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.Catalog.Methods.SDTYPEUSER_GETID(SDTYPEUSER);
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_SDTYPEUSER.js Persistence.Catalog.Methods.SDTYPEUSER_ADD", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Catalog.Methods.SDTYPEUSER_UPD = function (SDTYPEUSER) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.SDTYPEUSER.IDSDTYPEUSER.FieldName, SDTYPEUSER.IDSDTYPEUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.SDTYPEUSER.TYPEUSERDESCRIPTION.FieldName, SDTYPEUSER.TYPEUSERDESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.SDTYPEUSER.TYPEUSERNAME.FieldName, SDTYPEUSER.TYPEUSERNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   SDTYPEUSER_UPD   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"SDTYPEUSER_UPD", @"SDTYPEUSER_UPD description.");
        UnSQL.SQL.Add(@" UPDATE SDTYPEUSER SET ");
        UnSQL.SQL.Add(@"  IDSDTYPEUSER=@[IDSDTYPEUSER],  ");
        UnSQL.SQL.Add(@"  TYPEUSERDESCRIPTION=@[TYPEUSERDESCRIPTION],  ");
        UnSQL.SQL.Add(@"  TYPEUSERNAME=@[TYPEUSERNAME] ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  IDSDTYPEUSER=@[IDSDTYPEUSER] AND  ");
        UnSQL.SQL.Add(@"  TYPEUSERDESCRIPTION=@[TYPEUSERDESCRIPTION] AND  ");
        UnSQL.SQL.Add(@"  TYPEUSERNAME=@[TYPEUSERNAME] ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "SDTYPEUSER_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_SDTYPEUSER.js Persistence.Catalog.Methods.SDTYPEUSER_UPD", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.Catalog.Methods.SDTYPEUSER_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.Catalog.Methods.SDTYPEUSER_DELIDSDTYPEUSER(/*String StrIDSDTYPEUSER*/Param);
    }
    else {
        Res = Persistence.Catalog.Methods.SDTYPEUSER_DELSDTYPEUSER(Param);
    }
    return (Res);
}

Persistence.Catalog.Methods.SDTYPEUSER_DELIDSDTYPEUSER = function (/*String StrIDSDTYPEUSER*/IDSDTYPEUSER) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDSDTYPEUSER == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.SDTYPEUSER.IDSDTYPEUSER.FieldName, " = " + UsrCfg.InternoAtisNames.SDTYPEUSER.NAME_TABLE + "." + UsrCfg.InternoAtisNames.SDTYPEUSER.IDSDTYPEUSER.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDSDTYPEUSER = " IN (" + StrIDSDTYPEUSER + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.SDTYPEUSER.IDSDTYPEUSER.FieldName, StrIDSDTYPEUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.SDTYPEUSER.IDSDTYPEUSER.FieldName, IDSDTYPEUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   SDTYPEUSER_DEL_1   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"SDTYPEUSER_DEL_1", @"SDTYPEUSER_DEL_1 description.");
        UnSQL.SQL.Add(@" DELETE SDTYPEUSER  ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  IDSDTYPEUSER=@[IDSDTYPEUSER] AND  ");
        UnSQL.SQL.Add(@"  TYPEUSERDESCRIPTION=@[TYPEUSERDESCRIPTION] AND  ");
        UnSQL.SQL.Add(@"  TYPEUSERNAME=@[TYPEUSERNAME] ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "SDTYPEUSER_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_SDTYPEUSER.js Persistence.Catalog.Methods.SDTYPEUSER_DEL", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Catalog.Methods.SDTYPEUSER_DELSDTYPEUSER = function (SDTYPEUSER) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.SDTYPEUSER.IDSDTYPEUSER.FieldName, SDTYPEUSER.IDSDTYPEUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.SDTYPEUSER.TYPEUSERDESCRIPTION.FieldName, SDTYPEUSER.TYPEUSERDESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.SDTYPEUSER.TYPEUSERNAME.FieldName, SDTYPEUSER.TYPEUSERNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   SDTYPEUSER_DEL_2   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"SDTYPEUSER_DEL_2", @"SDTYPEUSER_DEL_2 description.");
        UnSQL.SQL.Add(@" DELETE SDTYPEUSER  ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  IDSDTYPEUSER=@[IDSDTYPEUSER] AND  ");
        UnSQL.SQL.Add(@"  TYPEUSERDESCRIPTION=@[TYPEUSERDESCRIPTION] AND  ");
        UnSQL.SQL.Add(@"  TYPEUSERNAME=@[TYPEUSERNAME] ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "SDTYPEUSER_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_SDTYPEUSER.js Persistence.Catalog.Methods.SDTYPEUSER_DEL", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
//**********************   METHODS   ********************************************************************************************
Persistence.Catalog.Methods.SDTYPEUSER_ListSetID = function (SDTYPEUSERList, IDSDTYPEUSER) {
    for (i = 0; i < SDTYPEUSERList.length; i++) {
        if (IDSDTYPEUSER == SDTYPEUSERList[i].IDSDTYPEUSER)
            return (SDTYPEUSERList[i]);
    }
    var UnSDTYPEUSER = new Persistence.Catalog.Properties.TSDTYPEUSER();
    UnSDTYPEUSER.IDSDTYPEUSER = IDSDTYPEUSER
    return (UnSDTYPEUSER);
}
Persistence.Catalog.Methods.SDTYPEUSER_ListAdd = function (SDTYPEUSERList, SDTYPEUSER) {
    var i = Persistence.Catalog.Methods.SDTYPEUSER_ListGetIndex(SDTYPEUSERList, SDTYPEUSER);
    if (i == -1) SDTYPEUSERList.push(SDTYPEUSER);
    else {
        SDTYPEUSERList[i].IDSDTYPEUSER = SDTYPEUSER.IDSDTYPEUSER;
        SDTYPEUSERList[i].TYPEUSERDESCRIPTION = SDTYPEUSER.TYPEUSERDESCRIPTION;
        SDTYPEUSERList[i].TYPEUSERNAME = SDTYPEUSER.TYPEUSERNAME;
    }
}
//CJRC_26072018
Persistence.Catalog.Methods.SDTYPEUSER_ListGetIndex = function (SDTYPEUSERList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.Catalog.Methods.SDTYPEUSER_ListGetIndexIDSDTYPEUSER(SDTYPEUSERList, Param);

    }
    else {
        Res = Persistence.Catalog.Methods.SDTYPEUSER_ListGetIndexSDTYPEUSER(SDTYPEUSERList, Param);
    }
    return (Res);
}

Persistence.Catalog.Methods.SDTYPEUSER_ListGetIndexSDTYPEUSER = function (SDTYPEUSERList, SDTYPEUSER) {
    for (i = 0; i < SDTYPEUSERList.length; i++) {
        if (SDTYPEUSER.IDSDTYPEUSER == SDTYPEUSERList[i].IDSDTYPEUSER)
            return (i);
    }
    return (-1);
}
Persistence.Catalog.Methods.SDTYPEUSER_ListGetIndexIDSDTYPEUSER = function (SDTYPEUSERList, IDSDTYPEUSER) {
    for (i = 0; i < SDTYPEUSERList.length; i++) {
        if (IDSDTYPEUSER == SDTYPEUSERList[i].IDSDTYPEUSER)
            return (i);
    }
    return (-1);
}
//String Function 
Persistence.Catalog.Methods.SDTYPEUSERListtoStr = function (SDTYPEUSERList) {
    var Res = Persistence.Catalog.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(SDTYPEUSERList.length, Longitud, Texto);
        for (Counter = 0; Counter < SDTYPEUSERList.length; Counter++) {
            var UnSDTYPEUSER = SDTYPEUSERList[Counter];
            UnSDTYPEUSER.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Catalog.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;   
}
Persistence.Catalog.Methods.StrtoSDTYPEUSER = function (ProtocoloStr, SDTYPEUSERList) {
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        SDTYPEUSERList.length = 0;
        if (Persistence.Catalog.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnSDTYPEUSER = new Persistence.Demo.Properties.TSDTYPEUSER(); //Mode new row
                UnSDTYPEUSER.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.SDTYPEUSER_ListAdd(SDTYPEUSERList, UnSDTYPEUSER);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
    
}
//Socket Function 
Persistence.Catalog.Methods.SDTYPEUSERListToByte = function (SDTYPEUSERList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, SDTYPEUSERList.length - idx);
    for (i = idx; i < SDTYPEUSERList.length; i++) {
        SDTYPEUSERList[i].ToByte(MemStream);
    }
}
Persistence.Catalog.Methods.ByteToSDTYPEUSERList = function (SDTYPEUSERList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnSDTYPEUSER = new Persistence.Catalog.Properties.TSDTYPEUSER();
        UnSDTYPEUSER.ByteTo(MemStream);
        Persistence.Catalog.Methods.SDTYPEUSER_ListAdd(SDTYPEUSERList, UnSDTYPEUSER);
    }
}

