﻿//**********************   PROPERTIES   *****************************************************************************************
Persistence.Catalog.Properties.TMDHIERPER = function () {
    this.COMMENTSH = "";
    this.HIERLAVEL = 0;
    this.IDMDGROUP = 0;
    this.IDMDHIERESC = 0;
    this.IDMDHIERPER = 0;
    this.PERCH = 0;
    this.PERMISSIONH = 0;
    this.TOTAL = 0;

    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.COMMENTSH);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.HIERLAVEL);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDGROUP);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDHIERESC);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDHIERPER);
        SysCfg.Stream.Methods.WriteStreamDouble(MemStream, this.PERCH);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.PERMISSIONH);
        SysCfg.Stream.Methods.WriteStreamDouble(MemStream, this.TOTAL);
    }
    this.ByteTo = function (MemStream) {
        this.COMMENTSH = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.HIERLAVEL = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDGROUP = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDHIERESC = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDHIERPER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.PERCH = SysCfg.Stream.Methods.ReadStreamDouble(MemStream);
        this.PERMISSIONH = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.TOTAL = SysCfg.Stream.Methods.ReadStreamDouble(MemStream);
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteStr(this.COMMENTSH, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.HIERLAVEL, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDGROUP, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDHIERESC, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDHIERPER, Longitud, Texto);
        SysCfg.Str.Protocol.WriteDouble(this.PERCH, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.PERMISSIONH, Longitud, Texto);
        SysCfg.Str.Protocol.WriteDouble(this.TOTAL, Longitud, Texto);
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.COMMENTSH = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.HIERLAVEL = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDGROUP = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDHIERESC = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDHIERPER = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.PERCH = SysCfg.Str.Protocol.ReadDouble(Pos, Index, LongitudArray, Texto);
        this.PERMISSIONH = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.TOTAL = SysCfg.Str.Protocol.ReadDouble(Pos, Index, LongitudArray, Texto);
    }
}
//**********************   METHODS server  ********************************************************************************************
Persistence.Catalog.Methods.MDHIERPER_Fill = function (MDHIERPERList) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    MDHIERPERList.length = 0;
    try {
        var DS_MDHIERPER = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDHIERPER_GET", new Array());
        ResErr = DS_MDHIERPER.ResErr;
        if (ResErr.NotError) {
            if (DS_MDHIERPER.DataSet.RecordCount > 0) {
                DS_MDHIERPER.DataSet.First();
                while (!(DS_MDHIERPER.DataSet.Eof)) {
                    var MDHIERPER = new Persistence.Catalog.Properties.TMDHIERPER();
                    MDHIERPER.IDMDHIERPER = DS_MDHIERPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERPER.FieldName).asInt32();
                    //Other
                    MDHIERPER.HIERLAVEL = DS_MDHIERPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.HIERLAVEL.FieldName).asInt32();
                    MDHIERPER.IDMDGROUP = DS_MDHIERPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.IDMDGROUP.FieldName).asInt32();
                    MDHIERPER.IDMDHIERESC = DS_MDHIERPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERESC.FieldName).asInt32();
                    MDHIERPER.PERCH = DS_MDHIERPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.PERCH.FieldName).asDouble();
                    MDHIERPER.PERMISSIONH = DS_MDHIERPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.PERMISSIONH.FieldName).asInt32();
                    MDHIERPER.TOTAL = DS_MDHIERPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.TOTAL.FieldName).asDouble();
                    //Other
                    MDHIERPERList.push(MDHIERPER);
                    DS_MDHIERPER.DataSet.Next();
                }
            }
            else {
                DS_MDHIERPER.ResErr.NotError = false;
                DS_MDHIERPER.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_MDHIERPER.js Persistence.Catalog.Methods.MDHIERPER_Fill", e);
    }
    return (ResErr);
}
Persistence.Catalog.Methods.MDHIERPER_GETID = function (MDHIERPER) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERPER.FieldName, MDHIERPER.IDMDHIERPER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDHIERPER.COMMENTSH.FieldName, MDHIERPER.COMMENTSH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.HIERLAVEL.FieldName, MDHIERPER.HIERLAVEL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.IDMDGROUP.FieldName, MDHIERPER.IDMDGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERESC.FieldName, MDHIERPER.IDMDHIERESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDouble(UsrCfg.InternoAtisNames.MDHIERPER.PERCH.FieldName, MDHIERPER.PERCH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.PERMISSIONH.FieldName, MDHIERPER.PERMISSIONH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDouble(UsrCfg.InternoAtisNames.MDHIERPER.TOTAL.FieldName, MDHIERPER.TOTAL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   MDHIERPER_GETID   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDHIERPER_GETID", @"MDHIERPER_GETID description.");
        UnSQL.SQL.Add(@" SELECT  ");
        UnSQL.SQL.Add(@"  MDHIERPER.COMMENTSH, ");
        UnSQL.SQL.Add(@"  MDHIERPER.HIERLAVEL, ");
        UnSQL.SQL.Add(@"  MDHIERPER.IDMDGROUP, ");
        UnSQL.SQL.Add(@"  MDHIERPER.IDMDHIERESC, ");
        UnSQL.SQL.Add(@"  MDHIERPER.IDMDHIERPER, ");
        UnSQL.SQL.Add(@"  MDHIERPER.PERCH, ");
        UnSQL.SQL.Add(@"  MDHIERPER.PERMISSIONH, ");
        UnSQL.SQL.Add(@"  MDHIERPER.TOTAL ");
        UnSQL.SQL.Add(@" FROM  MDHIERPER ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  MDHIERPER.COMMENTSH=@[COMMENTSH] and  ");
        UnSQL.SQL.Add(@"  MDHIERPER.HIERLAVEL=@[HIERLAVEL] and  ");
        UnSQL.SQL.Add(@"  MDHIERPER.IDMDGROUP=@[IDMDGROUP] and  ");
        UnSQL.SQL.Add(@"  MDHIERPER.IDMDHIERESC=@[IDMDHIERESC] and  ");
        UnSQL.SQL.Add(@"  MDHIERPER.IDMDHIERPER=@[IDMDHIERPER] and  ");
        UnSQL.SQL.Add(@"  MDHIERPER.PERCH=@[PERCH] and  ");
        UnSQL.SQL.Add(@"  MDHIERPER.PERMISSIONH=@[PERMISSIONH] and  ");
        UnSQL.SQL.Add(@"  MDHIERPER.TOTAL=@[TOTAL] ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var DS_MDHIERPER = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDHIERPER_GETID", Param.ToBytes());
        ResErr = DS_MDHIERPER.ResErr;
        if (ResErr.NotError) {
            if (DS_MDHIERPER.DataSet.RecordCount > 0) {
                DS_MDHIERPER.DataSet.First();
                MDHIERPER.IDMDHIERPER = DS_MDHIERPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERPER.FieldName).asInt32();
                //Other
                MDHIERPER.HIERLAVEL = DS_MDHIERPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.HIERLAVEL.FieldName).asInt32();
                MDHIERPER.IDMDGROUP = DS_MDHIERPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.IDMDGROUP.FieldName).asInt32();
                MDHIERPER.IDMDHIERESC = DS_MDHIERPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERESC.FieldName).asInt32();
                MDHIERPER.PERCH = DS_MDHIERPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.PERCH.FieldName).asDouble();
                MDHIERPER.PERMISSIONH = DS_MDHIERPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.PERMISSIONH.FieldName).asInt32();
                MDHIERPER.TOTAL = DS_MDHIERPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDHIERPER.TOTAL.FieldName).asDouble();
            }
            else {
                DS_MDHIERPER.ResErr.NotError = false;
                DS_MDHIERPER.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_MDHIERPER.js Persistence.Catalog.Methods.MDHIERPER_GETID", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Catalog.Methods.MDHIERPER_ADD = function (MDHIERPER) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERPER.FieldName, MDHIERPER.IDMDHIERPER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDHIERPER.COMMENTSH.FieldName, MDHIERPER.COMMENTSH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.HIERLAVEL.FieldName, MDHIERPER.HIERLAVEL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.IDMDGROUP.FieldName, MDHIERPER.IDMDGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERESC.FieldName, MDHIERPER.IDMDHIERESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDouble(UsrCfg.InternoAtisNames.MDHIERPER.PERCH.FieldName, MDHIERPER.PERCH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.PERMISSIONH.FieldName, MDHIERPER.PERMISSIONH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDouble(UsrCfg.InternoAtisNames.MDHIERPER.TOTAL.FieldName, MDHIERPER.TOTAL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   MDHIERPER_ADD   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDHIERPER_ADD", @"MDHIERPER_ADD description.");
        UnSQL.SQL.Add(@" INSERT INTO MDHIERPER( ");
        UnSQL.SQL.Add(@"  COMMENTSH,  ");
        UnSQL.SQL.Add(@"  HIERLAVEL,  ");
        UnSQL.SQL.Add(@"  IDMDGROUP,  ");
        UnSQL.SQL.Add(@"  IDMDHIERESC,  ");
        UnSQL.SQL.Add(@"  IDMDHIERPER,  ");
        UnSQL.SQL.Add(@"  PERCH,  ");
        UnSQL.SQL.Add(@"  PERMISSIONH,  ");
        UnSQL.SQL.Add(@"  TOTAL ");
        UnSQL.SQL.Add(@" )  ");
        UnSQL.SQL.Add(@" VALUES ( ");
        UnSQL.SQL.Add(@"  @[COMMENTSH], ");
        UnSQL.SQL.Add(@"  @[HIERLAVEL], ");
        UnSQL.SQL.Add(@"  @[IDMDGROUP], ");
        UnSQL.SQL.Add(@"  @[IDMDHIERESC], ");
        UnSQL.SQL.Add(@"  @[IDMDHIERPER], ");
        UnSQL.SQL.Add(@"  @[PERCH], ");
        UnSQL.SQL.Add(@"  @[PERMISSIONH], ");
        UnSQL.SQL.Add(@"  @[TOTAL] ");
        UnSQL.SQL.Add(@" ) ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDHIERPER_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.Catalog.Methods.MDHIERPER_GETID(MDHIERPER);
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_MDHIERPER.js Persistence.Catalog.Methods.MDHIERPER_ADD", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Catalog.Methods.MDHIERPER_UPD = function (MDHIERPER) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERPER.FieldName, MDHIERPER.IDMDHIERPER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDHIERPER.COMMENTSH.FieldName, MDHIERPER.COMMENTSH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.HIERLAVEL.FieldName, MDHIERPER.HIERLAVEL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.IDMDGROUP.FieldName, MDHIERPER.IDMDGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERESC.FieldName, MDHIERPER.IDMDHIERESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDouble(UsrCfg.InternoAtisNames.MDHIERPER.PERCH.FieldName, MDHIERPER.PERCH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.PERMISSIONH.FieldName, MDHIERPER.PERMISSIONH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDouble(UsrCfg.InternoAtisNames.MDHIERPER.TOTAL.FieldName, MDHIERPER.TOTAL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   MDHIERPER_UPD   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDHIERPER_UPD", @"MDHIERPER_UPD description.");
        UnSQL.SQL.Add(@" UPDATE MDHIERPER SET ");
        UnSQL.SQL.Add(@"  COMMENTSH=@[COMMENTSH],  ");
        UnSQL.SQL.Add(@"  HIERLAVEL=@[HIERLAVEL],  ");
        UnSQL.SQL.Add(@"  IDMDGROUP=@[IDMDGROUP],  ");
        UnSQL.SQL.Add(@"  IDMDHIERESC=@[IDMDHIERESC],  ");
        UnSQL.SQL.Add(@"  IDMDHIERPER=@[IDMDHIERPER],  ");
        UnSQL.SQL.Add(@"  PERCH=@[PERCH],  ");
        UnSQL.SQL.Add(@"  PERMISSIONH=@[PERMISSIONH],  ");
        UnSQL.SQL.Add(@"  TOTAL=@[TOTAL] ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  COMMENTSH=@[COMMENTSH] AND  ");
        UnSQL.SQL.Add(@"  HIERLAVEL=@[HIERLAVEL] AND  ");
        UnSQL.SQL.Add(@"  IDMDGROUP=@[IDMDGROUP] AND  ");
        UnSQL.SQL.Add(@"  IDMDHIERESC=@[IDMDHIERESC] AND  ");
        UnSQL.SQL.Add(@"  IDMDHIERPER=@[IDMDHIERPER] AND  ");
        UnSQL.SQL.Add(@"  PERCH=@[PERCH] AND  ");
        UnSQL.SQL.Add(@"  PERMISSIONH=@[PERMISSIONH] AND  ");
        UnSQL.SQL.Add(@"  TOTAL=@[TOTAL] ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDHIERPER_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_MDHIERPER.js Persistence.Catalog.Methods.MDHIERPER_UPD", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.Catalog.Methods.MDHIERPER_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.Catalog.Methods.MDHIERPER_DELIDMDHIERPER(/*String StrIDMDHIERPER*/Param);
    }
    else {
        Res = Persistence.Catalog.Methods.MDHIERPER_DELMDHIERPER(Param);
    }
    return (Res);
}

Persistence.Catalog.Methods.MDHIERPER_DELIDMDHIERPER = function (/*String StrIDMDHIERPER*/IDMDHIERPER) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDMDHIERPER == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERPER.FieldName, " = " + UsrCfg.InternoAtisNames.MDHIERPER.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERPER.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDMDHIERPER = " IN (" + StrIDMDHIERPER + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERPER.FieldName, StrIDMDHIERPER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERPER.FieldName, IDMDHIERPER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   MDHIERPER_DEL_1   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDHIERPER_DEL_1", @"MDHIERPER_DEL_1 description.");
        UnSQL.SQL.Add(@" DELETE MDHIERPER  ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  COMMENTSH=@[COMMENTSH] AND  ");
        UnSQL.SQL.Add(@"  HIERLAVEL=@[HIERLAVEL] AND  ");
        UnSQL.SQL.Add(@"  IDMDGROUP=@[IDMDGROUP] AND  ");
        UnSQL.SQL.Add(@"  IDMDHIERESC=@[IDMDHIERESC] AND  ");
        UnSQL.SQL.Add(@"  IDMDHIERPER=@[IDMDHIERPER] AND  ");
        UnSQL.SQL.Add(@"  PERCH=@[PERCH] AND  ");
        UnSQL.SQL.Add(@"  PERMISSIONH=@[PERMISSIONH] AND  ");
        UnSQL.SQL.Add(@"  TOTAL=@[TOTAL] ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDHIERPER_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_MDHIERPER.js Persistence.Catalog.Methods.MDHIERPER_DEL", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Catalog.Methods.MDHIERPER_DELMDHIERPER = function (MDHIERPER) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERPER.FieldName, MDHIERPER.IDMDHIERPER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDHIERPER.COMMENTSH.FieldName, MDHIERPER.COMMENTSH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.HIERLAVEL.FieldName, MDHIERPER.HIERLAVEL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.IDMDGROUP.FieldName, MDHIERPER.IDMDGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERESC.FieldName, MDHIERPER.IDMDHIERESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDouble(UsrCfg.InternoAtisNames.MDHIERPER.PERCH.FieldName, MDHIERPER.PERCH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDHIERPER.PERMISSIONH.FieldName, MDHIERPER.PERMISSIONH, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDouble(UsrCfg.InternoAtisNames.MDHIERPER.TOTAL.FieldName, MDHIERPER.TOTAL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   MDHIERPER_DEL_2   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDHIERPER_DEL_2", @"MDHIERPER_DEL_2 description.");
        UnSQL.SQL.Add(@" DELETE MDHIERPER  ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  COMMENTSH=@[COMMENTSH] AND  ");
        UnSQL.SQL.Add(@"  HIERLAVEL=@[HIERLAVEL] AND  ");
        UnSQL.SQL.Add(@"  IDMDGROUP=@[IDMDGROUP] AND  ");
        UnSQL.SQL.Add(@"  IDMDHIERESC=@[IDMDHIERESC] AND  ");
        UnSQL.SQL.Add(@"  IDMDHIERPER=@[IDMDHIERPER] AND  ");
        UnSQL.SQL.Add(@"  PERCH=@[PERCH] AND  ");
        UnSQL.SQL.Add(@"  PERMISSIONH=@[PERMISSIONH] AND  ");
        UnSQL.SQL.Add(@"  TOTAL=@[TOTAL] ");
    UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDHIERPER_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_MDHIERPER.js Persistence.Catalog.Methods.MDHIERPER_DEL", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
//**********************   METHODS   ********************************************************************************************

//CJRC_27072018_2
Persistence.Catalog.Methods.MDHIERPER_ListSetID = function (MDHIERPERList) {
    var Res = new Persistence.Catalog.Properties.TMDHIERPER();
    if (arguments.length == 2) {
        Res = Persistence.Catalog.Methods.MDHIERPER_ListSetID1(MDHIERPERList, arguments[1]);
    }
    if (arguments.length == 3) {
        Res = Persistence.Catalog.Methods.MDHIERPER_ListSetID2(MDHIERPERList, arguments[1], arguments[2]);
    }
    return Res;
}

Persistence.Catalog.Methods.MDHIERPER_ListSetID1 = function (MDHIERPERList, IDMDHIERPER) {
    for (i = 0; i < MDHIERPERList.length; i++) {
        if (IDMDHIERPER == MDHIERPERList[i].IDMDHIERPER)
            return (MDHIERPERList[i]);
    }
    var UnMDHIERPER = new Persistence.Catalog.Properties.TMDHIERPER();
    UnMDHIERPER.IDMDHIERPER = IDMDHIERPER;
    return (UnMDHIERPER);
}
Persistence.Catalog.Methods.MDHIERPER_ListSetID2 = function (MDHIERPERList, IDMDHIERESC, HIERLAVEL) {
    for (i = 0; i < MDHIERPERList.length; i++) {
        if ((IDMDHIERESC == MDHIERPERList[i].IDMDHIERESC) && (HIERLAVEL == MDHIERPERList[i].HIERLAVEL))
        return (MDHIERPERList[i]);
    }
    var UnMDHIERPER = new Persistence.Catalog.Properties.TMDHIERPER();
    UnMDHIERPER.IDMDHIERESC = IDMDHIERESC;
    UnMDHIERPER.HIERLAVEL = HIERLAVEL;
    return (UnMDHIERPER);
}
Persistence.Catalog.Methods.MDHIERPER_ListAdd = function (MDHIERPERList, MDHIERPER) {
    var i = Persistence.Catalog.Methods.MDHIERPER_ListGetIndex(MDHIERPERList, MDHIERPER);
    if (i == -1) MDHIERPERList.push(MDHIERPER);
    else {
        MDHIERPERList[i].COMMENTSH = MDHIERPER.COMMENTSH;
        MDHIERPERList[i].HIERLAVEL = MDHIERPER.HIERLAVEL;
        MDHIERPERList[i].IDMDGROUP = MDHIERPER.IDMDGROUP;
        MDHIERPERList[i].IDMDHIERESC = MDHIERPER.IDMDHIERESC;
        MDHIERPERList[i].IDMDHIERPER = MDHIERPER.IDMDHIERPER;
        MDHIERPERList[i].PERCH = MDHIERPER.PERCH;
        MDHIERPERList[i].PERMISSIONH = MDHIERPER.PERMISSIONH;
        MDHIERPERList[i].TOTAL = MDHIERPER.TOTAL;

    }
}

Persistence.Catalog.Methods.MDHIERPER_ListGetIndex = function (MDHIERPERList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.Catalog.Methods.MDHIERPER_ListGetIndexIDMDHIERPER(MDHIERPERList, Param);
    }
    else {
        Res = Persistence.Catalog.Methods.MDHIERPER_ListGetIndexMDHIERPER(MDHIERPERList, Param);
    }
    return (Res);
}

Persistence.Catalog.Methods.MDHIERPER_ListGetIndexMDHIERPER = function (MDHIERPERList, MDHIERPER) {
    for (i = 0; i < MDHIERPERList.length; i++) {
        if (MDHIERPER.IDMDHIERPER == MDHIERPERList[i].IDMDHIERPER)
        return (i);
    }
    return (-1);
}
Persistence.Catalog.Methods.MDHIERPER_ListGetIndexIDMDHIERPER = function (MDHIERPERList, IDMDHIERPER) {
    for (i = 0; i < MDHIERPERList.length; i++) {
        if (IDMDHIERPER == MDHIERPERList[i].IDMDHIERPER)
        return (i);
    }
    return (-1);
}
//String Function 
Persistence.Catalog.Methods.MDHIERPERListtoStr = function (MDHIERPERList) {
    var Res = Persistence.Catalog.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(MDHIERPERList.length, Longitud, Texto);
        for (Counter = 0; Counter < MDHIERPERList.length; Counter++) {
            var UnMDHIERPER = MDHIERPERList[Counter];
            UnMDHIERPER.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Catalog.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
}
Persistence.Catalog.Methods.StrtoMDHIERPER = function (ProtocoloStr, MDHIERPERList) {
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        MDHIERPERList.length = 0;
        if (Persistence.Catalog.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnMDHIERPER = new Persistence.Demo.Properties.TMDHIERPER(); //Mode new row
                UnMDHIERPER.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.MDHIERPER_ListAdd(MDHIERPERList, UnMDHIERPER);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
    
}
//Socket Function 
Persistence.Catalog.Methods.MDHIERPERListToByte = function (MDHIERPERList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, MDHIERPERList.length - idx);
    for (i = idx; i < MDHIERPERList.length; i++) {
        MDHIERPERList[i].ToByte(MemStream);
    }
}
Persistence.Catalog.Methods.ByteToMDHIERPERList = function (MDHIERPERList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnMDHIERPER = new Persistence.Catalog.Properties.TMDHIERPER();
        UnMDHIERPER.ByteTo(MemStream);
        Persistence.Catalog.Methods.MDHIERPER_ListAdd(MDHIERPERList, UnMDHIERPER);
    }
}

