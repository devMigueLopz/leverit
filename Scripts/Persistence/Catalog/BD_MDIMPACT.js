﻿//**********************   PROPERTIES   *****************************************************************************************
Persistence.Catalog.Properties.TMDIMPACT = function () {
    this.IDMDIMPACT = 0;
    this.IMPACTNAME = "";
    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDIMPACT);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.IMPACTNAME);
    }
    this.ByteTo = function (MemStream) {
        this.IDMDIMPACT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IMPACTNAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDMDIMPACT, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.IMPACTNAME, Longitud, Texto);
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDMDIMPACT = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IMPACTNAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
    }
}
//**********************   METHODS server  ********************************************************************************************
Persistence.Catalog.Methods.MDIMPACT_Fill = function (MDIMPACTList) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    MDIMPACTList.length = 0;
    try {
        var DS_MDIMPACT = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDIMPACT_GET", new Array());
        ResErr = DS_MDIMPACT.ResErr;
        if (ResErr.NotError) {
            if (DS_MDIMPACT.DataSet.RecordCount > 0) {
                DS_MDIMPACT.DataSet.First();
                while (!(DS_MDIMPACT.DataSet.Eof)) {
                    var MDIMPACT = new Persistence.Catalog.Properties.TMDIMPACT();
                    MDIMPACT.IDMDIMPACT = DS_MDIMPACT.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.FieldName).asInt32();
                    //Other
                    MDIMPACT.IMPACTNAME = DS_MDIMPACT.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDIMPACT.IMPACTNAME.FieldName).asString();
                    //Other
                    MDIMPACTList.push(MDIMPACT);
                    DS_MDIMPACT.DataSet.Next();
                }
            }
            else {
                DS_MDIMPACT.ResErr.NotError = false;
                DS_MDIMPACT.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_MDIMPACT.js Persistence.Catalog.Methods.MDIMPACT_Fill", e);
    }
    return (ResErr);
}
Persistence.Catalog.Methods.MDIMPACT_GETID = function (MDIMPACT) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.FieldName, MDIMPACT.IDMDIMPACT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.MDIMPACT.IMPACTNAME.FieldName, MDIMPACT.IMPACTNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   MDIMPACT_GETID   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDIMPACT_GETID", @"MDIMPACT_GETID description.");
        UnSQL.SQL.Add(@" SELECT  ");
        UnSQL.SQL.Add(@"  MDIMPACT.IDMDIMPACT, ");
        UnSQL.SQL.Add(@"  MDIMPACT.IMPACTNAME ");
        UnSQL.SQL.Add(@" FROM  MDIMPACT ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  MDIMPACT.IDMDIMPACT=@[IDMDIMPACT] and  ");
        UnSQL.SQL.Add(@"  MDIMPACT.IMPACTNAME=@[IMPACTNAME] ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var DS_MDIMPACT = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDIMPACT_GETID", Param.ToBytes());
        ResErr = DS_MDIMPACT.ResErr;
        if (ResErr.NotError) {
            if (DS_MDIMPACT.DataSet.RecordCount > 0) {
                DS_MDIMPACT.DataSet.First();
                MDIMPACT.IDMDIMPACT = DS_MDIMPACT.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.FieldName).asInt32();
                //Other
                MDIMPACT.IMPACTNAME = DS_MDIMPACT.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDIMPACT.IMPACTNAME.FieldName).asString();
            }
            else {
                DS_MDIMPACT.ResErr.NotError = false;
                DS_MDIMPACT.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_MDIMPACT.js Persistence.Catalog.Methods.MDIMPACT_GETID", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Catalog.Methods.MDIMPACT_ADD = function (MDIMPACT) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.FieldName, MDIMPACT.IDMDIMPACT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.MDIMPACT.IMPACTNAME.FieldName, MDIMPACT.IMPACTNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   MDIMPACT_ADD   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDIMPACT_ADD", @"MDIMPACT_ADD description.");
        UnSQL.SQL.Add(@" INSERT INTO MDIMPACT( ");
        UnSQL.SQL.Add(@"  IDMDIMPACT,  ");
        UnSQL.SQL.Add(@"  IMPACTNAME ");
        UnSQL.SQL.Add(@" )  ");
        UnSQL.SQL.Add(@" VALUES ( ");
        UnSQL.SQL.Add(@"  @[IDMDIMPACT], ");
        UnSQL.SQL.Add(@"  @[IMPACTNAME] ");
        UnSQL.SQL.Add(@" ) ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDIMPACT_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.Catalog.Methods.MDIMPACT_GETID(MDIMPACT);
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_MDIMPACT.js Persistence.Catalog.Methods.MDIMPACT_ADD", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Catalog.Methods.MDIMPACT_UPD = function (MDIMPACT) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.FieldName, MDIMPACT.IDMDIMPACT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.MDIMPACT.IMPACTNAME.FieldName, MDIMPACT.IMPACTNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   MDIMPACT_UPD   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDIMPACT_UPD", @"MDIMPACT_UPD description.");
        UnSQL.SQL.Add(@" UPDATE MDIMPACT SET ");
        UnSQL.SQL.Add(@"  IDMDIMPACT=@[IDMDIMPACT],  ");
        UnSQL.SQL.Add(@"  IMPACTNAME=@[IMPACTNAME] ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  IDMDIMPACT=@[IDMDIMPACT] AND  ");
        UnSQL.SQL.Add(@"  IMPACTNAME=@[IMPACTNAME] ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDIMPACT_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_MDIMPACT.js Persistence.Catalog.Methods.MDIMPACT_UPD", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.Catalog.Methods.MDIMPACT_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number')|| (typeof (Param) == 'string')) {
        Res = Persistence.Catalog.Methods.MDIMPACT_DELIDMDIMPACT(/*String StrIDMDIMPACT*/Param);
    }
    else {
        Res = Persistence.Catalog.Methods.MDIMPACT_DELMDIMPACT(Param);
    }
    return (Res);
}

Persistence.Catalog.Methods.MDIMPACT_DELIDMDIMPACT = function (/*String StrIDMDIMPACT*/IDMDIMPACT) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDMDIMPACT == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.FieldName, " = " + UsrCfg.InternoAtisNames.MDIMPACT.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDMDIMPACT = " IN (" + StrIDMDIMPACT + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.FieldName, StrIDMDIMPACT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.FieldName, IDMDIMPACT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   MDIMPACT_DEL_1   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDIMPACT_DEL_1", @"MDIMPACT_DEL_1 description.");
        UnSQL.SQL.Add(@" DELETE MDIMPACT  ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  IDMDIMPACT=@[IDMDIMPACT] AND  ");
        UnSQL.SQL.Add(@"  IMPACTNAME=@[IMPACTNAME] ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDIMPACT_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_MDIMPACT.js Persistence.Catalog.Methods.MDIMPACT_DEL", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Catalog.Methods.MDIMPACT_DELMDIMPACT = function (MDIMPACT) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.FieldName, MDIMPACT.IDMDIMPACT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.MDIMPACT.IMPACTNAME.FieldName, MDIMPACT.IMPACTNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDIMPACT_DEL_2", @"MDIMPACT_DEL_2 description.");
        UnSQL.SQL.Add(@" DELETE MDIMPACT  ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  IDMDIMPACT=@[IDMDIMPACT] AND  ");
        UnSQL.SQL.Add(@"  IMPACTNAME=@[IMPACTNAME] ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDIMPACT_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_MDIMPACT.js Persistence.Catalog.Methods.MDIMPACT_DEL", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
//**********************   METHODS   ********************************************************************************************
Persistence.Catalog.Methods.MDIMPACT_ListSetID = function (MDIMPACTList, IDMDIMPACT) {
    for (i = 0; i < MDIMPACTList.length; i++) {
        if (IDMDIMPACT == MDIMPACTList[i].IDMDIMPACTR)
        return (MDIMPACTList[i]);
    }
    var UnMDIMPACT = new Persistence.Catalog.Properties.TMDIMPACT();
    UnMDIMPACT.IDMDIMPACT = IDMDIMPACT;
    return (UnMDIMPACT);
}


Persistence.Catalog.Methods.MDIMPACT_ListAdd = function (MDIMPACTList, MDIMPACT) {
    var i = Persistence.Catalog.Methods.MDIMPACT_ListGetIndex(MDIMPACTList, MDIMPACT);
    if (i == -1) MDIMPACTList.push(MDIMPACT);
    else {
        MDIMPACTList[i].IDMDIMPACT = MDIMPACT.IDMDIMPACT;
        MDIMPACTList[i].IMPACTNAME = MDIMPACT.IMPACTNAME;
    }
}

Persistence.Catalog.Methods.MDIMPACT_ListGetIndex = function (MDIMPACTList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.Catalog.Methods.MDIMPACT_ListGetIndexIDMDIMPACT(MDIMPACTList, Param);

    }
    else {
        Res = Persistence.Catalog.Methods.MDIMPACT_ListGetIndexMDIMPACT(MDIMPACTList, Param);
    }
    return (Res);
}

Persistence.Catalog.Methods.MDIMPACT_ListGetIndexMDIMPACT = function (MDIMPACTList, MDIMPACT) {
    for (i = 0; i < MDIMPACTList.length; i++) {
        if (MDIMPACT.IDMDIMPACT == MDIMPACTList[i].IDMDIMPACT)
        return (i);
    }
    return (-1);
}
Persistence.Catalog.Methods.MDIMPACT_ListGetIndexIDMDIMPACT = function (MDIMPACTList, IDMDIMPACT) {
    for (i = 0; i < MDIMPACTList.length; i++) {
        if (IDMDIMPACT == MDIMPACTList[i].IDMDIMPACT)
        return (i);
    }
    return (-1);
}
//String Function 
Persistence.Catalog.Methods.MDIMPACTListtoStr = function (MDIMPACTList) {
    var Res = Persistence.Catalog.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(MDIMPACTList.length, Longitud, Texto);
        for (Counter = 0; Counter < MDIMPACTList.length; Counter++) {
            var UnMDIMPACT = MDIMPACTList[Counter];
            UnMDIMPACT.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Catalog.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
/////**************************************
    var Res = Persistence.Catalog.Properties._Version + "(1)0";
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(MDIMPACTList.length, Longitud, Texto);
        for (Counter = 0; Counter < MDIMPACTList.length; Counter++) {
            var UnMDIMPACT = MDIMPACTList[Counter];
            UnMDIMPACT.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Catalog.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
        SysCfg.Log.Methods.WriteLog("DB_MDIMPACT.js Persistence.Catalog.Methods.MDIMPACTListtoStr", e);
    }
    return (Res);
}
Persistence.Catalog.Methods.StrtoMDIMPACT = function (ProtocoloStr, MDIMPACTList) {
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        MDIMPACTList.length = 0;
        if (Persistence.Catalog.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnMDIMPACT = new Persistence.Demo.Properties.TMDIMPACT(); //Mode new row
                UnMDIMPACT.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.MDIMPACT_ListAdd(MDIMPACTList, UnMDIMPACT);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
    
}
//Socket Function 
Persistence.Catalog.Methods.MDIMPACTListToByte = function (MDIMPACTList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, MDIMPACTList.length - idx);
    for (i = idx; i < MDIMPACTList.length; i++) {
        MDIMPACTList[i].ToByte(MemStream);
    }
}
Persistence.Catalog.Methods.ByteToMDIMPACTList = function (MDIMPACTList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (var i = 0; i < Count; i++) {
        var UnMDIMPACT = new Persistence.Catalog.Properties.TMDIMPACT();
        UnMDIMPACT.ByteTo(MemStream);
        Persistence.Catalog.Methods.MDIMPACT_ListAdd(MDIMPACTList, UnMDIMPACT);
    }
}

