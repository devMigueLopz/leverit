﻿//**********************   PROPERTIES   *****************************************************************************************
Persistence.Catalog.Properties.TMDFUNCPER = function () {
    this.COMMENTSF = "";
    this.FUNLAVEL = 0;
    this.IDMDFUNCESC = 0;
    this.IDMDFUNCPER = 0;
    this.IDMDGROUP = 0;
    this.PERCF = 0;
    this.TOTAL = 0;
    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.COMMENTSF);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.FUNLAVEL);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDFUNCESC);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDFUNCPER);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDGROUP);
        SysCfg.Stream.Methods.WriteStreamDouble(MemStream, this.PERCF);
        SysCfg.Stream.Methods.WriteStreamDouble(MemStream, this.TOTAL);
    }
    this.ByteTo = function (MemStream) {
        this.COMMENTSF = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.FUNLAVEL = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDFUNCESC = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDFUNCPER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDGROUP = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.PERCF = SysCfg.Stream.Methods.ReadStreamDouble(MemStream);
        this.TOTAL = SysCfg.Stream.Methods.ReadStreamDouble(MemStream);
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteStr(this.COMMENTSF, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.FUNLAVEL, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDFUNCESC, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDFUNCPER, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDMDGROUP, Longitud, Texto);
        SysCfg.Str.Protocol.WriteDouble(this.PERCF, Longitud, Texto);
        SysCfg.Str.Protocol.WriteDouble(this.TOTAL, Longitud, Texto);
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.COMMENTSF = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.FUNLAVEL = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDFUNCESC = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDFUNCPER = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDMDGROUP = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.PERCF = SysCfg.Str.Protocol.ReadDouble(Pos, Index, LongitudArray, Texto);
        this.TOTAL = SysCfg.Str.Protocol.ReadDouble(Pos, Index, LongitudArray, Texto);
    }
}
//**********************   METHODS server  ********************************************************************************************
Persistence.Catalog.Methods.MDFUNCPER_Fill = function (MDFUNCPERList) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    MDFUNCPERList.length = 0;
    try {
        var DS_MDFUNCPER = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDFUNCPER_GET", new Array());
        ResErr = DS_MDFUNCPER.ResErr;
        if (ResErr.NotError) {
            if (DS_MDFUNCPER.DataSet.RecordCount > 0) {
                DS_MDFUNCPER.DataSet.First();
                while (!(DS_MDFUNCPER.DataSet.Eof)) {
                    var MDFUNCPER = new Persistence.Catalog.Properties.TMDFUNCPER();
                    MDFUNCPER.IDMDFUNCPER = DS_MDFUNCPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCPER.FieldName).asInt32();
                    //Other
                    MDFUNCPER.FUNLAVEL = DS_MDFUNCPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDFUNCPER.FUNLAVEL.FieldName).asInt32();
                    MDFUNCPER.IDMDFUNCESC = DS_MDFUNCPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCESC.FieldName).asInt32();
                    MDFUNCPER.IDMDGROUP = DS_MDFUNCPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDGROUP.FieldName).asInt32();
                    MDFUNCPER.PERCF = DS_MDFUNCPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDFUNCPER.PERCF.FieldName).asDouble();
                    MDFUNCPER.TOTAL = DS_MDFUNCPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDFUNCPER.TOTAL.FieldName).asDouble();
                    //Other
                    MDFUNCPERList.push(MDFUNCPER);
                    DS_MDFUNCPER.DataSet.Next();
                }
            }
            else {
                DS_MDFUNCPER.ResErr.NotError = false;
                DS_MDFUNCPER.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_MDFUNCPER.js Persistence.Catalog.Methods.MDFUNCPER_Fill", e);
    }
    return (ResErr);
}
Persistence.Catalog.Methods.MDFUNCPER_GETID = function (MDFUNCPER) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCPER.FieldName, MDFUNCPER.IDMDFUNCPER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDFUNCPER.COMMENTSF.FieldName, MDFUNCPER.COMMENTSF, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCPER.FUNLAVEL.FieldName, MDFUNCPER.FUNLAVEL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCESC.FieldName, MDFUNCPER.IDMDFUNCESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDGROUP.FieldName, MDFUNCPER.IDMDGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDouble(UsrCfg.InternoAtisNames.MDFUNCPER.PERCF.FieldName, MDFUNCPER.PERCF, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDouble(UsrCfg.InternoAtisNames.MDFUNCPER.TOTAL.FieldName, MDFUNCPER.TOTAL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   MDFUNCPER_GETID   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDFUNCPER_GETID", @"MDFUNCPER_GETID description.");
        UnSQL.SQL.Add(@" SELECT  ");
        UnSQL.SQL.Add(@"  MDFUNCPER.COMMENTSF, ");
        UnSQL.SQL.Add(@"  MDFUNCPER.FUNLAVEL, ");
        UnSQL.SQL.Add(@"  MDFUNCPER.IDMDFUNCESC, ");
        UnSQL.SQL.Add(@"  MDFUNCPER.IDMDFUNCPER, ");
        UnSQL.SQL.Add(@"  MDFUNCPER.IDMDGROUP, ");
        UnSQL.SQL.Add(@"  MDFUNCPER.PERCF, ");
        UnSQL.SQL.Add(@"  MDFUNCPER.TOTAL ");
        UnSQL.SQL.Add(@" FROM  MDFUNCPER ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  MDFUNCPER.COMMENTSF=@[COMMENTSF] and  ");
        UnSQL.SQL.Add(@"  MDFUNCPER.FUNLAVEL=@[FUNLAVEL] and  ");
        UnSQL.SQL.Add(@"  MDFUNCPER.IDMDFUNCESC=@[IDMDFUNCESC] and  ");
        UnSQL.SQL.Add(@"  MDFUNCPER.IDMDFUNCPER=@[IDMDFUNCPER] and  ");
        UnSQL.SQL.Add(@"  MDFUNCPER.IDMDGROUP=@[IDMDGROUP] and  ");
        UnSQL.SQL.Add(@"  MDFUNCPER.PERCF=@[PERCF] and  ");
        UnSQL.SQL.Add(@"  MDFUNCPER.TOTAL=@[TOTAL] ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var DS_MDFUNCPER = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDFUNCPER_GETID", Param.ToBytes());
        ResErr = DS_MDFUNCPER.ResErr;
        if (ResErr.NotError) {
            if (DS_MDFUNCPER.DataSet.RecordCount > 0) {
                DS_MDFUNCPER.DataSet.First();
                MDFUNCPER.IDMDFUNCPER = DS_MDFUNCPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCPER.FieldName).asInt32();
                //Other
                MDFUNCPER.FUNLAVEL = DS_MDFUNCPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDFUNCPER.FUNLAVEL.FieldName).asInt32();
                MDFUNCPER.IDMDFUNCESC = DS_MDFUNCPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCESC.FieldName).asInt32();
                MDFUNCPER.IDMDGROUP = DS_MDFUNCPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDGROUP.FieldName).asInt32();
                MDFUNCPER.PERCF = DS_MDFUNCPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDFUNCPER.PERCF.FieldName).asDouble();
                MDFUNCPER.TOTAL = DS_MDFUNCPER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDFUNCPER.TOTAL.FieldName).asDouble();
            }
            else {
                DS_MDFUNCPER.ResErr.NotError = false;
                DS_MDFUNCPER.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_MDFUNCPER.js Persistence.Catalog.Methods.MDFUNCPER_GETID", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Catalog.Methods.MDFUNCPER_ADD = function (MDFUNCPER) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCPER.FieldName, MDFUNCPER.IDMDFUNCPER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDFUNCPER.COMMENTSF.FieldName, MDFUNCPER.COMMENTSF, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCPER.FUNLAVEL.FieldName, MDFUNCPER.FUNLAVEL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCESC.FieldName, MDFUNCPER.IDMDFUNCESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDGROUP.FieldName, MDFUNCPER.IDMDGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDouble(UsrCfg.InternoAtisNames.MDFUNCPER.PERCF.FieldName, MDFUNCPER.PERCF, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDouble(UsrCfg.InternoAtisNames.MDFUNCPER.TOTAL.FieldName, MDFUNCPER.TOTAL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   MDFUNCPER_ADD   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDFUNCPER_ADD", @"MDFUNCPER_ADD description.");
        UnSQL.SQL.Add(@" INSERT INTO MDFUNCPER( ");
        UnSQL.SQL.Add(@"  COMMENTSF,  ");
        UnSQL.SQL.Add(@"  FUNLAVEL,  ");
        UnSQL.SQL.Add(@"  IDMDFUNCESC,  ");
        UnSQL.SQL.Add(@"  IDMDFUNCPER,  ");
        UnSQL.SQL.Add(@"  IDMDGROUP,  ");
        UnSQL.SQL.Add(@"  PERCF,  ");
        UnSQL.SQL.Add(@"  TOTAL ");
        UnSQL.SQL.Add(@" )  ");
        UnSQL.SQL.Add(@" VALUES ( ");
        UnSQL.SQL.Add(@"  @[COMMENTSF], ");
        UnSQL.SQL.Add(@"  @[FUNLAVEL], ");
        UnSQL.SQL.Add(@"  @[IDMDFUNCESC], ");
        UnSQL.SQL.Add(@"  @[IDMDFUNCPER], ");
        UnSQL.SQL.Add(@"  @[IDMDGROUP], ");
        UnSQL.SQL.Add(@"  @[PERCF], ");
        UnSQL.SQL.Add(@"  @[TOTAL] ");
        UnSQL.SQL.Add(@" ) ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDFUNCPER_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.Catalog.Methods.MDFUNCPER_GETID(MDFUNCPER);
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_MDFUNCPER.js Persistence.Catalog.Methods.MDFUNCPER_ADD", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Catalog.Methods.MDFUNCPER_UPD = function (MDFUNCPER) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCPER.FieldName, MDFUNCPER.IDMDFUNCPER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDFUNCPER.COMMENTSF.FieldName, MDFUNCPER.COMMENTSF, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCPER.FUNLAVEL.FieldName, MDFUNCPER.FUNLAVEL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCESC.FieldName, MDFUNCPER.IDMDFUNCESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDGROUP.FieldName, MDFUNCPER.IDMDGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDouble(UsrCfg.InternoAtisNames.MDFUNCPER.PERCF.FieldName, MDFUNCPER.PERCF, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDouble(UsrCfg.InternoAtisNames.MDFUNCPER.TOTAL.FieldName, MDFUNCPER.TOTAL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   MDFUNCPER_UPD   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDFUNCPER_UPD", @"MDFUNCPER_UPD description.");
        UnSQL.SQL.Add(@" UPDATE MDFUNCPER SET ");
        UnSQL.SQL.Add(@"  COMMENTSF=@[COMMENTSF],  ");
        UnSQL.SQL.Add(@"  FUNLAVEL=@[FUNLAVEL],  ");
        UnSQL.SQL.Add(@"  IDMDFUNCESC=@[IDMDFUNCESC],  ");
        UnSQL.SQL.Add(@"  IDMDFUNCPER=@[IDMDFUNCPER],  ");
        UnSQL.SQL.Add(@"  IDMDGROUP=@[IDMDGROUP],  ");
        UnSQL.SQL.Add(@"  PERCF=@[PERCF],  ");
        UnSQL.SQL.Add(@"  TOTAL=@[TOTAL] ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  COMMENTSF=@[COMMENTSF] AND  ");
        UnSQL.SQL.Add(@"  FUNLAVEL=@[FUNLAVEL] AND  ");
        UnSQL.SQL.Add(@"  IDMDFUNCESC=@[IDMDFUNCESC] AND  ");
        UnSQL.SQL.Add(@"  IDMDFUNCPER=@[IDMDFUNCPER] AND  ");
        UnSQL.SQL.Add(@"  IDMDGROUP=@[IDMDGROUP] AND  ");
        UnSQL.SQL.Add(@"  PERCF=@[PERCF] AND  ");
        UnSQL.SQL.Add(@"  TOTAL=@[TOTAL] ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDFUNCPER_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_MDFUNCPER.js Persistence.Catalog.Methods.MDFUNCPER_UPD", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}

//CJRC_27072018
Persistence.Catalog.Methods.MDFUNCPER_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.Catalog.Methods.MDFUNCPER_DELIDMDFUNCPER(/*String StrIDMDFUNCPER*/Param);
    }
    else {
        Res = Persistence.Catalog.Methods.MDFUNCPER_DELMDFUNCPER(Param);
    }
    return (Res);
}

Persistence.Catalog.Methods.MDFUNCPER_DELIDMDFUNCPER = function (/*String StrIDMDFUNCPER*/IDMDFUNCPER) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDMDFUNCPER == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCPER.FieldName, " = " + UsrCfg.InternoAtisNames.MDFUNCPER.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCPER.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDMDFUNCPER = " IN (" + StrIDMDFUNCPER + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCPER.FieldName, StrIDMDFUNCPER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCPER.FieldName, IDMDFUNCPER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   MDFUNCPER_DEL_1   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDFUNCPER_DEL_1", @"MDFUNCPER_DEL_1 description.");
        UnSQL.SQL.Add(@" DELETE MDFUNCPER  ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  COMMENTSF=@[COMMENTSF] AND  ");
        UnSQL.SQL.Add(@"  FUNLAVEL=@[FUNLAVEL] AND  ");
        UnSQL.SQL.Add(@"  IDMDFUNCESC=@[IDMDFUNCESC] AND  ");
        UnSQL.SQL.Add(@"  IDMDFUNCPER=@[IDMDFUNCPER] AND  ");
        UnSQL.SQL.Add(@"  IDMDGROUP=@[IDMDGROUP] AND  ");
        UnSQL.SQL.Add(@"  PERCF=@[PERCF] AND  ");
        UnSQL.SQL.Add(@"  TOTAL=@[TOTAL] ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDFUNCPER_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_MDFUNCPER.js Persistence.Catalog.Methods.MDFUNCPER_DEL", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Catalog.Methods.MDFUNCPER_DELMDFUNCPER = function (MDFUNCPER) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCPER.FieldName, MDFUNCPER.IDMDFUNCPER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddText(UsrCfg.InternoAtisNames.MDFUNCPER.COMMENTSF.FieldName, MDFUNCPER.COMMENTSF, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCPER.FUNLAVEL.FieldName, MDFUNCPER.FUNLAVEL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCESC.FieldName, MDFUNCPER.IDMDFUNCESC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDGROUP.FieldName, MDFUNCPER.IDMDGROUP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDouble(UsrCfg.InternoAtisNames.MDFUNCPER.PERCF.FieldName, MDFUNCPER.PERCF, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDouble(UsrCfg.InternoAtisNames.MDFUNCPER.TOTAL.FieldName, MDFUNCPER.TOTAL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   MDFUNCPER_DEL_2   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDFUNCPER_DEL_2", @"MDFUNCPER_DEL_2 description.");
        UnSQL.SQL.Add(@" DELETE MDFUNCPER  ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  COMMENTSF=@[COMMENTSF] AND  ");
        UnSQL.SQL.Add(@"  FUNLAVEL=@[FUNLAVEL] AND  ");
        UnSQL.SQL.Add(@"  IDMDFUNCESC=@[IDMDFUNCESC] AND  ");
        UnSQL.SQL.Add(@"  IDMDFUNCPER=@[IDMDFUNCPER] AND  ");
        UnSQL.SQL.Add(@"  IDMDGROUP=@[IDMDGROUP] AND  ");
        UnSQL.SQL.Add(@"  PERCF=@[PERCF] AND  ");
        UnSQL.SQL.Add(@"  TOTAL=@[TOTAL] ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDFUNCPER_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_MDFUNCPER.js Persistence.Catalog.Methods.MDFUNCPER_DEL", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}


Persistence.Catalog.Methods.MDFUNCPER_ListSetID = function (MDFUNCPERList) {
    var Res = new Persistence.Catalog.Properties.TMDFUNCPER();
    if (arguments.length == 2) {
        Res = Persistence.Catalog.Methods.MDFUNCPER_ListSetID1(MDFUNCPERList, arguments[1]);
    }
    if (arguments.length == 3) {
        Res = Persistence.Catalog.Methods.MDFUNCPER_ListSetID2(MDFUNCPERList, arguments[1], arguments[2]);
    }
    return Res;
}


/*
//CJRC_27072018_2
Persistence.XXX.YYY.ZZZ_ListSetID = function (ZZZList) {
    var Res = new Persistence.XXX.YYY.TZZZ();
    if (arguments.length == 2) {
        Res = Persistence.XXX.YYY.ZZZ_ListSetID1(ZZZList, arguments[1]);
    }
    if (arguments.length == 3) {
        Res = Persistence.XXX.YYY.ZZZ_ListSetID2(ZZZList, arguments[1], arguments[2]);
    }
    return Res;
}
*/

//**********************   METHODS   ********************************************************************************************
Persistence.Catalog.Methods.MDFUNCPER_ListSetID1 = function (MDFUNCPERList, IDMDFUNCPER) {
    for (i = 0; i < MDFUNCPERList.length; i++) {
        if (IDMDFUNCPER == MDFUNCPERList[i].IDMDFUNCPERR)
            return (MDFUNCPERList[i]);
    }
    var UnMDFUNCPER = new Persistence.Catalog.Properties.TMDFUNCPER();
    UnMDFUNCPER.IDMDFUNCPER = IDMDFUNCPER;
    return (UnMDFUNCPER);
}
Persistence.Catalog.Methods.MDFUNCPER_ListSetID2 = function (MDFUNCPERList, IDMDFUNCESC, FUNLAVEL) {
    for (i = 0; i < MDFUNCPERList.length; i++) {
        if ((IDMDFUNCESC == MDFUNCPERList[i].IDMDFUNCESC) && (FUNLAVEL == MDFUNCPERList[i].FUNLAVEL))
            return (MDFUNCPERList[i]);
    }
    var UnMDFUNCPER = new Persistence.Catalog.Properties.TMDFUNCPER();
    UnMDFUNCPER.IDMDFUNCESC = IDMDFUNCESC;
    UnMDFUNCPER.FUNLAVEL = FUNLAVEL;
    return (UnMDFUNCPER);
}
Persistence.Catalog.Methods.MDFUNCPER_ListAdd = function (MDFUNCPERList, MDFUNCPER) {
    var i = Persistence.Catalog.Methods.MDFUNCPER_ListGetIndex(MDFUNCPERList, MDFUNCPER);
    if (i == -1) MDFUNCPERList.push(MDFUNCPER);
    else {
        MDFUNCPERList[i].COMMENTSF = MDFUNCPER.COMMENTSF;
        MDFUNCPERList[i].FUNLAVEL = MDFUNCPER.FUNLAVEL;
        MDFUNCPERList[i].IDMDFUNCESC = MDFUNCPER.IDMDFUNCESC;
        MDFUNCPERList[i].IDMDFUNCPER = MDFUNCPER.IDMDFUNCPER;
        MDFUNCPERList[i].IDMDGROUP = MDFUNCPER.IDMDGROUP;
        MDFUNCPERList[i].PERCF = MDFUNCPER.PERCF;
        MDFUNCPERList[i].TOTAL = MDFUNCPER.TOTAL;

    }
}

//CJRC_26072018
Persistence.Catalog.Methods.MDFUNCPER_ListGetIndex = function (MDFUNCPERList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.Catalog.Methods.MDFUNCPER_ListGetIndexIDMDFUNCPER(MDFUNCPERList, Param);
    }
    else {
        Res = Persistence.Catalog.Methods.MDFUNCPER_ListGetIndexMDFUNCPER(MDFUNCPERList, Param);
    }
    return (Res);
}

Persistence.Catalog.Methods.MDFUNCPER_ListGetIndexMDFUNCPER = function (MDFUNCPERList, MDFUNCPER) {
    for (i = 0; i < MDFUNCPERList.length; i++) {
        if (MDFUNCPER.IDMDFUNCPER == MDFUNCPERList[i].IDMDFUNCPER)
            return (i);
    }
    return (-1);
}
Persistence.Catalog.Methods.MDFUNCPER_ListGetIndexIDMDFUNCPER = function (MDFUNCPERList, IDMDFUNCPER) {
    for (i = 0; i < MDFUNCPERList.length; i++) {
        if (IDMDFUNCPER == MDFUNCPERList[i].IDMDFUNCPER)
            return (i);
    }
    return (-1);
}
//String Function 
Persistence.Catalog.Methods.MDFUNCPERListtoStr = function (MDFUNCPERList) {
    var Res = Persistence.Catalog.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(MDFUNCPERList.length, Longitud, Texto);
        for (Counter = 0; Counter < MDFUNCPERList.length; Counter++) {
            var UnMDFUNCPER = MDFUNCPERList[Counter];
            UnMDFUNCPER.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Catalog.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
}
Persistence.Catalog.Methods.StrtoMDFUNCPER = function (ProtocoloStr, MDFUNCPERList) {
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        MDFUNCPERList.length = 0;
        if (Persistence.Catalog.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnMDFUNCPER = new Persistence.Demo.Properties.TMDFUNCPER(); //Mode new row
                UnMDFUNCPER.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.MDFUNCPER_ListAdd(MDFUNCPERList, UnMDFUNCPER);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
    
}
//Socket Function 
Persistence.Catalog.Methods.MDFUNCPERListToByte = function (MDFUNCPERList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, MDFUNCPERList.length - idx);
    for (i = idx; i < MDFUNCPERList.length; i++) {
        MDFUNCPERList[i].ToByte(MemStream);
    }
}
Persistence.Catalog.Methods.ByteToMDFUNCPERList = function (MDFUNCPERList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnMDFUNCPER = new Persistence.Catalog.Properties.TMDFUNCPER();
        UnMDFUNCPER.ByteTo(MemStream);
        Persistence.Catalog.Methods.MDFUNCPER_ListAdd(MDFUNCPERList, UnMDFUNCPER);
    }
}

