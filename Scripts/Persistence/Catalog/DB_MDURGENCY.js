﻿//**********************   PROPERTIES   *****************************************************************************************
Persistence.Catalog.Properties.TMDURGENCY = function () {

    this.IDMDURGENCY = 0;
    this.URGENCYNAME = "";
    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDURGENCY);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.URGENCYNAME);
    }
    this.ByteTo = function (MemStream) {
        this.IDMDURGENCY = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.URGENCYNAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDMDURGENCY, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.URGENCYNAME, Longitud, Texto);
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDMDURGENCY = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.URGENCYNAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
    }

}
//**********************   METHODS SERVER  ********************************************************************************************
Persistence.Catalog.Methods.MDURGENCY_Fill = function (MDURGENCYList) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    MDURGENCYList.length = 0;
    try {
        var DS_MDURGENCY = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDURGENCY_GET", new Array());
        ResErr = DS_MDURGENCY.ResErr;
        if (ResErr.NotError) {
            if (DS_MDURGENCY.DataSet.RecordCount > 0) {
                DS_MDURGENCY.DataSet.First();
                while (!(DS_MDURGENCY.DataSet.Eof)) {
                    var MDURGENCY = new Persistence.Catalog.Properties.TMDURGENCY();
                    MDURGENCY.IDMDURGENCY = DS_MDURGENCY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDURGENCY.IDMDURGENCY.FieldName).asInt32();
                    MDURGENCY.URGENCYNAME = DS_MDURGENCY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDURGENCY.URGENCYNAME.FieldName).asString();
                    MDURGENCYList.push(MDURGENCY);
                    DS_MDURGENCY.DataSet.Next();
                }
            }
            else {
                DS_MDURGENCY.ResErr.NotError = false;
                DS_MDURGENCY.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_MDURGENCY.js Persistence.Catalog.Methods.MDURGENCY_Fill", e);
    }
    return (ResErr);
}

//public static bool MDURGENCY_FillTEMP (List<Persistence.Catalog.Properties.TMDURGENCY> MDURGENCYList) //jaimes llenado temporal / manual
//{
//    Persistence.Catalog.Properties.TMDURGENCY MDURGENCY = new Persistence.Catalog.Properties.TMDURGENCY();
//    MDURGENCY.IDMDURGENCY = 0;
//    MDURGENCY.URGENCYNAME = "High";
//    MDURGENCYList.push(MDURGENCY);

//    MDURGENCY = new Persistence.Catalog.Properties.TMDURGENCY();
//    MDURGENCY.IDMDURGENCY = 1;
//    MDURGENCY.URGENCYNAME = "Medium";
//    MDURGENCYList.push(MDURGENCY);

//    MDURGENCY = new Persistence.Catalog.Properties.TMDURGENCY();
//    MDURGENCY.IDMDURGENCY = 2;
//    MDURGENCY.URGENCYNAME = "Low";
//    MDURGENCYList.push(MDURGENCY);
//    return true;
//}
Persistence.Catalog.Methods.MDURGENCY_GETID = function (MDURGENCY) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDURGENCY.IDMDURGENCY.FieldName, MDURGENCY.IDMDURGENCY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.MDURGENCY.URGENCYNAME.FieldName, MDURGENCY.URGENCYNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   MDURGENCY_GETID   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDURGENCY_GETID", @"MDURGENCY_GETID description.");
        UnSQL.SQL.Add(@" SELECT  ");
        UnSQL.SQL.Add(@"  MDURGENCY.IDMDURGENCY, ");
        UnSQL.SQL.Add(@"  MDURGENCY.URGENCYNAME ");
        UnSQL.SQL.Add(@" FROM  MDURGENCY ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  MDURGENCY.IDMDURGENCY=@[IDMDURGENCY] and  ");
        UnSQL.SQL.Add(@"  MDURGENCY.URGENCYNAME=@[URGENCYNAME] ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var DS_MDURGENCY = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDURGENCY_GETID", Param.ToBytes());
        ResErr = DS_MDURGENCY.ResErr;
        if (ResErr.NotError) {
            if (DS_MDURGENCY.DataSet.RecordCount > 0) {
                DS_MDURGENCY.DataSet.First();
                MDURGENCY.IDMDURGENCY = DS_MDURGENCY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDURGENCY.IDMDURGENCY.FieldName).asInt32();
                //Other
                MDURGENCY.URGENCYNAME = DS_MDURGENCY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDURGENCY.URGENCYNAME.FieldName).asString();
            }
            else {
                DS_MDURGENCY.ResErr.NotError = false;
                DS_MDURGENCY.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_MDURGENCY.js Persistence.Catalog.Methods.MDURGENCY_GETID", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Catalog.Methods.MDURGENCY_ADD = function (MDURGENCY) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDURGENCY.IDMDURGENCY.FieldName, MDURGENCY.IDMDURGENCY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.MDURGENCY.URGENCYNAME.FieldName, MDURGENCY.URGENCYNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   MDURGENCY_ADD   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDURGENCY_ADD", @"MDURGENCY_ADD description.");
        UnSQL.SQL.Add(@" INSERT INTO MDURGENCY( ");
        UnSQL.SQL.Add(@"  IDMDURGENCY,  ");
        UnSQL.SQL.Add(@"  URGENCYNAME ");
        UnSQL.SQL.Add(@" )  ");
        UnSQL.SQL.Add(@" VALUES ( ");
        UnSQL.SQL.Add(@"  @[IDMDURGENCY], ");
        UnSQL.SQL.Add(@"  @[URGENCYNAME] ");
        UnSQL.SQL.Add(@" ) ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDURGENCY_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.Catalog.Methods.MDURGENCY_GETID(MDURGENCY);
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_MDURGENCY.js Persistence.Catalog.Methods.MDURGENCY_ADD", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Catalog.Methods.MDURGENCY_UPD = function (MDURGENCY) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDURGENCY.IDMDURGENCY.FieldName, MDURGENCY.IDMDURGENCY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.MDURGENCY.URGENCYNAME.FieldName, MDURGENCY.URGENCYNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   MDURGENCY_UPD   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDURGENCY_UPD", @"MDURGENCY_UPD description.");
        UnSQL.SQL.Add(@" UPDATE MDURGENCY SET ");
        UnSQL.SQL.Add(@"  IDMDURGENCY=@[IDMDURGENCY],  ");
        UnSQL.SQL.Add(@"  URGENCYNAME=@[URGENCYNAME] ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  IDMDURGENCY=@[IDMDURGENCY] AND  ");
        UnSQL.SQL.Add(@"  URGENCYNAME=@[URGENCYNAME] ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDURGENCY_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_MDURGENCY.js Persistence.Catalog.Methods.MDURGENCY_UPD", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
//CJRC_27072018
Persistence.Catalog.Methods.MDURGENCY_DEL = function (Param) {
    var Res = -1
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.Catalog.Methods.MDURGENCY_DELIDMDURGENCY(/*String StrIDMDURGENCY*/ Param);
    }
    else {
        Res = Persistence.Catalog.Methods.MDURGENCY_DELMDURGENCY(Param);
    }
    return (Res);
}

Persistence.Catalog.Methods.MDURGENCY_DELIDMDURGENCY = function (/*String StrIDMDURGENCY*/ IDMDURGENCY) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDMDURGENCY == "")
        {
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDURGENCY.IDMDURGENCY.FieldName, " = " + UsrCfg.InternoAtisNames.MDURGENCY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDURGENCY.IDMDURGENCY.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
            StrIDMDURGENCY = " IN (" + StrIDMDURGENCY + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDURGENCY.IDMDURGENCY.FieldName, StrIDMDURGENCY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.MDURGENCY.IDMDURGENCY.FieldName, IDMDURGENCY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   MDURGENCY_DEL_1   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDURGENCY_DEL_1", @"MDURGENCY_DEL_1 description.");
        UnSQL.SQL.Add(@" DELETE MDURGENCY  ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  IDMDURGENCY=@[IDMDURGENCY] AND  ");
        UnSQL.SQL.Add(@"  URGENCYNAME=@[URGENCYNAME] ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDURGENCY_DEL_1", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_MDURGENCY.js Persistence.Catalog.Methods.MDURGENCY_DEL", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.Catalog.Methods.MDURGENCY_DELMDURGENCY = function (MDURGENCY) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDURGENCY.IDMDURGENCY.FieldName, MDURGENCY.IDMDURGENCY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Other
        Param.AddString(UsrCfg.InternoAtisNames.MDURGENCY.URGENCYNAME.FieldName, MDURGENCY.URGENCYNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   MDURGENCY_DEL_2   *************************
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"MDURGENCY_DEL_2", @"MDURGENCY_DEL_2 description.");
        UnSQL.SQL.Add(@" DELETE MDURGENCY  ");
        UnSQL.SQL.Add(@" WHERE   ");
        UnSQL.SQL.Add(@"  IDMDURGENCY=@[IDMDURGENCY] AND  ");
        UnSQL.SQL.Add(@"  URGENCYNAME=@[URGENCYNAME] ");
        UnConfigSQL.SQL.Add(UnSQL);
        //************************************************************
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDURGENCY_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("DB_MDURGENCY.js Persistence.Catalog.Methods.MDURGENCY_DEL", e);
    } finally {
        Param.Destroy();
    }
    return (ResErr);
}
//**********************   METHODS   ********************************************************************************************
Persistence.Catalog.Methods.MDURGENCY_ListSetID = function (MDURGENCYList, IDMDURGENCY) {
    for (i = 0; i < MDURGENCYList.length; i++) {

        if (IDMDURGENCY == MDURGENCYList[i].IDMDURGENCY)
            return (MDURGENCYList[i]);

    }
    var UnMDURGENCYE = new Persistence.Catalog.Properties.TMDURGENCYE;
    UnMDURGENCYE.IDMDURGENCY = IDMDURGENCY;
    return (UnMDURGENCYE);
}
Persistence.Catalog.Methods.MDURGENCY_ListAdd = function (MDURGENCYList, MDURGENCY) {
    var i = Persistence.Catalog.Methods.MDURGENCY_ListGetIndex(MDURGENCYList, MDURGENCY);
    if (i == -1) MDURGENCYList.push(MDURGENCY);
    else {
        MDURGENCYList[i].IDMDURGENCY = MDURGENCY.IDMDURGENCY;
        MDURGENCYList[i].URGENCYNAME = MDURGENCY.URGENCYNAME;
    }
}
//CJRC_26072018
Persistence.Catalog.Methods.MDURGENCY_ListGetIndex = function (MDURGENCYList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.Catalog.Methods.MDURGENCY_ListGetIndexURGENCYNAME(MDURGENCYList, Param);

    }
    else {
        Res = Persistence.Catalog.Methods.MDURGENCY_ListGetIndexMDURGENCY(MDURGENCYList, Param);
    }
    return (Res);
}

Persistence.Catalog.Methods.MDURGENCY_ListGetIndexMDURGENCY = function (MDURGENCYList, MDURGENCY) {
    for (i = 0; i < MDURGENCYList.length; i++) {
        if (MDURGENCY.IDMDURGENCY == MDURGENCYList[i].IDMDURGENCY)
            return (i);
    }
    return (-1);
}
Persistence.Catalog.Methods.MDURGENCY_ListGetIndexURGENCYNAME = function (MDURGENCYList, URGENCYNAME) {
    for (i = 0; i < MDURGENCYList.length; i++) {
        if (URGENCYNAME == MDURGENCYList[i].URGENCYNAME)
            return (i);
    }
    return (-1);
}
//STRING FUNCTION 
Persistence.Catalog.Methods.MDURGENCYListtoStr = function (MDURGENCYList) {
    var Res = Persistence.Catalog.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(MDURGENCYList.length, Longitud, Texto);
        for (Counter = 0; Counter < MDURGENCYList.length; Counter++) {
            var UnMDURGENCY = MDURGENCYList[Counter];
            UnMDURGENCY.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Catalog.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;

}
Persistence.Catalog.Methods.StrtoMDURGENCY = function (ProtocoloStr, MDURGENCYList) {
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        MDURGENCYList.length = 0;
        if (Persistence.Catalog.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnMDURGENCY = new Persistence.Demo.Properties.TMDURGENCY(); //Mode new row
                UnMDURGENCY.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.Demo.Methods.MDURGENCY_ListAdd(MDURGENCYList, UnMDURGENCY);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);

}
//SOCKET FUNCTION 
Persistence.Catalog.Methods.MDURGENCYListToByte = function (MDURGENCYList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, MDURGENCYList.length - idx);
    for (i = idx; i < MDURGENCYList.length; i++) {
        MDURGENCYList[i].ToByte(MemStream);
    }
}
Persistence.Catalog.Methods.ByteToMDURGENCYList = function (MDURGENCYList, MemStream) {
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        var UnMDURGENCY = new Persistence.Catalog.Properties.TMDURGENCY();
        UnMDURGENCY.ByteTo(MemStream);
        Persistence.Catalog.Methods.MDURGENCY_ListAdd(MDURGENCYList, UnMDURGENCY);
    }
}


