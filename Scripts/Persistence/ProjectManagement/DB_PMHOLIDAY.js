//**********************   PROPERTIES   *****************************************************************************************
Persistence.ProjectManagement.Properties.TPMHOLIDAY = function () {
    this.IDPMHOLIDAY = 0;
    this.IDPMMAINTASK = 0;
    this.HOLIDAY_DATE = new Date(1970, 0, 1, 0, 0, 0);


    // Virtuales
    this.PMMAINTASK = new Persistence.ProjectManagement.Properties.TPMMAINTASK();

    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDPMHOLIDAY);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDPMMAINTASK);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.HOLIDAY_DATE);

    }
    this.ByteTo = function (MemStream) {
        this.IDPMHOLIDAY = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDPMMAINTASK = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.HOLIDAY_DATE = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);

    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDPMHOLIDAY, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.IDPMMAINTASK, Longitud, Texto);
        SysCfg.Str.Protocol.WriteDateTime(this.HOLIDAY_DATE, Longitud, Texto);

    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDPMHOLIDAY = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.IDPMMAINTASK = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.HOLIDAY_DATE = SysCfg.Str.Protocol.ReadDateTime(Pos, Index, LongitudArray, Texto);

    }
}

//**********************   METHODS server  ********************************************************************************************
Persistence.ProjectManagement.Methods.PMHOLIDAY_Fill = function (PMHOLIDAYList, StrIDPMHOLIDAY/*noin IDPMHOLIDAY*/) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    PMHOLIDAYList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDPMHOLIDAY == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.PMHOLIDAY.IDPMHOLIDAY.FieldName, " = " + UsrCfg.InternoAtisNames.PMHOLIDAY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PMHOLIDAY.IDPMHOLIDAY.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDPMHOLIDAY = " IN (" + StrIDPMHOLIDAY + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.PMHOLIDAY.IDPMHOLIDAY.FieldName, StrIDPMHOLIDAY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //noin Param.AddInt32(UsrCfg.InternoDemoNames.PMHOLIDAY.IDPMHOLIDAY.FieldName, IDPMHOLIDAY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {
        /*
        //********   PMHOLIDAY_GET   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PMHOLIDAY_GET", @"PMHOLIDAY_GET description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   IDPMHOLIDAY, "); 
         UnSQL.SQL.Add(@"   IDPMMAINTASK, "); 
         UnSQL.SQL.Add(@"   HOLIDAY_DATE "); 
         UnSQL.SQL.Add(@"   FROM PMHOLIDAY "); 
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_PMHOLIDAY = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "PMHOLIDAY_GET", Param.ToBytes());
        ResErr = DS_PMHOLIDAY.ResErr;
        if (ResErr.NotError) {
            if (DS_PMHOLIDAY.DataSet.RecordCount > 0) {
                DS_PMHOLIDAY.DataSet.First();
                while (!(DS_PMHOLIDAY.DataSet.Eof)) {
                    var PMHOLIDAY = new Persistence.ProjectManagement.Properties.TPMHOLIDAY();
                    PMHOLIDAY.IDPMHOLIDAY = DS_PMHOLIDAY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMHOLIDAY.IDPMHOLIDAY.FieldName).asInt32();
                    PMHOLIDAY.IDPMMAINTASK = DS_PMHOLIDAY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMHOLIDAY.IDPMMAINTASK.FieldName).asInt32();
                    PMHOLIDAY.HOLIDAY_DATE = DS_PMHOLIDAY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMHOLIDAY.HOLIDAY_DATE.FieldName).asDateTime();

                    //Other
                    PMHOLIDAYList.push(PMHOLIDAY);
                    DS_PMHOLIDAY.DataSet.Next();
                }
            }
            else {
                DS_PMHOLIDAY.ResErr.NotError = false;
                DS_PMHOLIDAY.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.ProjectManagement.Methods.PMHOLIDAY_GETID = function (PMHOLIDAY) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.PMHOLIDAY.IDPMHOLIDAY.FieldName, PMHOLIDAY.IDPMHOLIDAY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.PMHOLIDAY.IDPMMAINTASK.FieldName, PMHOLIDAY.IDPMMAINTASK, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddDateTime(UsrCfg.InternoAtisNames.PMHOLIDAY.HOLIDAY_DATE.FieldName, PMHOLIDAY.HOLIDAY_DATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

        /*
        //********   PMHOLIDAY_GETID   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PMHOLIDAY_GETID", @"PMHOLIDAY_GETID description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   IDPMHOLIDAY "); 
         UnSQL.SQL.Add(@"   FROM PMHOLIDAY "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDPMMAINTASK=@[IDPMMAINTASK] And "); 
         UnSQL.SQL.Add(@"   HOLIDAY_DATE=@[HOLIDAY_DATE] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_PMHOLIDAY = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "PMHOLIDAY_GETID", Param.ToBytes());
        ResErr = DS_PMHOLIDAY.ResErr;
        if (ResErr.NotError) {
            if (DS_PMHOLIDAY.DataSet.RecordCount > 0) {
                DS_PMHOLIDAY.DataSet.First();
                PMHOLIDAY.IDPMHOLIDAY = DS_PMHOLIDAY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMHOLIDAY.IDPMHOLIDAY.FieldName).asInt32();
            }
            else {
                DS_PMHOLIDAY.ResErr.NotError = false;
                DS_PMHOLIDAY.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.ProjectManagement.Methods.PMHOLIDAY_ADD = function (PMHOLIDAY) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.PMHOLIDAY.IDPMHOLIDAY.FieldName, PMHOLIDAY.IDPMHOLIDAY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.PMHOLIDAY.IDPMMAINTASK.FieldName, PMHOLIDAY.IDPMMAINTASK, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoAtisNames.PMHOLIDAY.HOLIDAY_DATE.FieldName, PMHOLIDAY.HOLIDAY_DATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        /* 
        //********   PMHOLIDAY_ADD   ************************* 
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PMHOLIDAY_ADD", @"PMHOLIDAY_ADD description."); 
        UnSQL.SQL.Add(@"   INSERT INTO PMHOLIDAY( "); 
         UnSQL.SQL.Add(@"   IDPMMAINTASK, "); 
         UnSQL.SQL.Add(@"   HOLIDAY_DATE "); 
         UnSQL.SQL.Add(@"   ) Values ( ");  
        UnSQL.SQL.Add(@"   @[IDPMMAINTASK], "); 
         UnSQL.SQL.Add(@"   @[HOLIDAY_DATE] "); 
         UnSQL.SQL.Add(@"   )  "); 
        UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PMHOLIDAY_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.ProjectManagement.Methods.PMHOLIDAY_GETID(PMHOLIDAY);
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.ProjectManagement.Methods.PMHOLIDAY_UPD = function (PMHOLIDAY) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.PMHOLIDAY.IDPMHOLIDAY.FieldName, PMHOLIDAY.IDPMHOLIDAY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.PMHOLIDAY.IDPMMAINTASK.FieldName, PMHOLIDAY.IDPMMAINTASK, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoAtisNames.PMHOLIDAY.HOLIDAY_DATE.FieldName, PMHOLIDAY.HOLIDAY_DATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);


        /*   
        //********   PMHOLIDAY_UPD   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PMHOLIDAY_UPD", @"PMHOLIDAY_UPD description.");  
        UnSQL.SQL.Add(@"  UPDATE PMHOLIDAY Set "); 
         UnSQL.SQL.Add(@"   IDPMMAINTASK=@[IDPMMAINTASK], "); 
         UnSQL.SQL.Add(@"   HOLIDAY_DATE=@[HOLIDAY_DATE] "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDPMHOLIDAY=@[IDPMHOLIDAY] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PMHOLIDAY_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.ProjectManagement.Methods.PMHOLIDAY_DEL = function (/*String StrIDPMHOLIDAY*/Param) {
    var Res = -1;
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.ProjectManagement.Methods.PMHOLIDAY_DELIDPMHOLIDAY(/*String StrIDPMHOLIDAY*/Param);
    }
    else {
        Res = Persistence.ProjectManagement.Methods.PMHOLIDAY_DELPMHOLIDAY(Param);
    }
    return (Res);
}
Persistence.ProjectManagement.Methods.PMHOLIDAY_DELIDPMHOLIDAY = function (/*String StrIDPMHOLIDAY*/IDPMHOLIDAY) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDPMHOLIDAY == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.PMHOLIDAY.IDPMHOLIDAY.FieldName, " = " + UsrCfg.InternoAtisNames.PMHOLIDAY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PMHOLIDAY.IDPMHOLIDAY.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDPMHOLIDAY = " IN (" + StrIDPMHOLIDAY + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.PMHOLIDAY.IDPMHOLIDAY.FieldName, StrIDPMHOLIDAY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.PMHOLIDAY.IDPMHOLIDAY.FieldName, IDPMHOLIDAY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   PMHOLIDAY_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PMHOLIDAY_DEL", @"PMHOLIDAY_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE PMHOLIDAY "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDPMHOLIDAY=@[IDPMHOLIDAY]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PMHOLIDAY_DEL", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.ProjectManagement.Methods.PMHOLIDAY_DELPMHOLIDAY = function (PMHOLIDAY/*PMHOLIDAYList*/) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        /*
        StrIDPMHOLIDAY = "";
        StrIDOTRO = "";
        for (var i = 0; i < PMHOLIDAYList.length; i++) {
        StrIDPMHOLIDAY += PMHOLIDAYList[i].IDPMHOLIDAY + ",";
        StrIDOTRO += PMHOLIDAYList[i].IDOTRO + ",";
        }
        StrIDPMHOLIDAY = StrIDPMHOLIDAY.substring(0, StrIDPMHOLIDAY.length - 1);
        StrIDOTRO = StrIDOTRO.substring(0, StrIDOTRO.length - 1);
        
        
        if (StrIDPMHOLIDAY == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.PMHOLIDAY.IDPMHOLIDAY.FieldName, " = " + UsrCfg.InternoAtisNames.PMHOLIDAY.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PMHOLIDAY.IDPMHOLIDAY.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDPMHOLIDAY = " IN (" + StrIDPMHOLIDAY + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.PMHOLIDAY.IDPMHOLIDAY.FieldName, StrIDPMHOLIDAY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/


        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.PMHOLIDAY.IDPMHOLIDAY.FieldName, PMHOLIDAY.IDPMHOLIDAY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*    
        //********   PMHOLIDAY_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PMHOLIDAY_DEL", @"PMHOLIDAY_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE PMHOLIDAY "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDPMHOLIDAY=@[IDPMHOLIDAY]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PMHOLIDAY_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//**********************   METHODS   ********************************************************************************************
Persistence.ProjectManagement.Methods.PMHOLIDAY_ListSetID = function (PMHOLIDAYList, IDPMHOLIDAY) {
    for (i = 0; i < PMHOLIDAYList.length; i++) {
        if (IDPMHOLIDAY == PMHOLIDAYList[i].IDPMHOLIDAY)
            return (PMHOLIDAYList[i]);
    }
    var UnPMHOLIDAY = new Persistence.Properties.TPMHOLIDAY;
    UnPMHOLIDAY.IDPMHOLIDAY = IDPMHOLIDAY;
    return (UnPMHOLIDAY);
}
Persistence.ProjectManagement.Methods.PMHOLIDAY_ListAdd = function (PMHOLIDAYList, PMHOLIDAY) {
    var i = Persistence.ProjectManagement.Methods.PMHOLIDAY_ListGetIndex(PMHOLIDAYList, PMHOLIDAY);
    if (i == -1) PMHOLIDAYList.push(PMHOLIDAY);
    else {
        PMHOLIDAYList[i].IDPMHOLIDAY = PMHOLIDAY.IDPMHOLIDAY;
        PMHOLIDAYList[i].IDPMMAINTASK = PMHOLIDAY.IDPMMAINTASK;
        PMHOLIDAYList[i].HOLIDAY_DATE = PMHOLIDAY.HOLIDAY_DATE;

    }
}
Persistence.ProjectManagement.Methods.PMHOLIDAY_ListGetIndex = function (PMHOLIDAYList, Param) {
    var Res = -1;
    if (typeof (Param) == 'number') {
        Res = Persistence.ProjectManagement.Methods.PMHOLIDAY_ListGetIndexIDPMHOLIDAY(PMHOLIDAYList, Param);
    }
    else {
        Res = Persistence.ProjectManagement.Methods.PMHOLIDAY_ListGetIndexPMHOLIDAY(PMHOLIDAYList, Param);
    }
    return (Res);
}
Persistence.ProjectManagement.Methods.PMHOLIDAY_ListGetIndexPMHOLIDAY = function (PMHOLIDAYList, PMHOLIDAY) {
    for (i = 0; i < PMHOLIDAYList.length; i++) {
        if (PMHOLIDAY.IDPMHOLIDAY == PMHOLIDAYList[i].IDPMHOLIDAY)
            return (i);
    }
    return (-1);
}
Persistence.ProjectManagement.Methods.PMHOLIDAY_ListGetIndexIDPMHOLIDAY = function (PMHOLIDAYList, IDPMHOLIDAY) {
    for (i = 0; i < PMHOLIDAYList.length; i++) {
        if (IDPMHOLIDAY == PMHOLIDAYList[i].IDPMHOLIDAY)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.ProjectManagement.Methods.PMHOLIDAYListtoStr = function (PMHOLIDAYList) {
    var Res = Persistence.Properties._Version + "(1)0";
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(PMHOLIDAYList.length, Longitud, Texto);
        for (Counter = 0; Counter < PMHOLIDAYList.length; Counter++) {
            var UnPMHOLIDAY = PMHOLIDAYList[Counter];
            UnPMHOLIDAY.ToStr(Longitud, Texto);
        }

        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
}
Persistence.ProjectManagement.Methods.StrtoPMHOLIDAY = function (ProtocoloStr, PMHOLIDAYList) {

    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        PMHOLIDAYList.length = 0;
        if (Persistence.ProjectManagement.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.Length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnPMHOLIDAY = new Persistence.Properties.TPMHOLIDAY(); //Mode new row
                UnPMHOLIDAY.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.ProjectManagement.PMHOLIDAY_ListAdd(PMHOLIDAYList, UnPMHOLIDAY);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}


//Socket Function 
Persistence.ProjectManagement.Methods.PMHOLIDAYListToByte = function (PMHOLIDAYList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, PMHOLIDAYList.Count - idx);
    for (i = idx; i < PMHOLIDAYList.Count; i++) {
        PMHOLIDAYList[i].ToByte(MemStream);
    }
}
Persistence.ProjectManagement.Methods.ByteToPMHOLIDAYList = function (PMHOLIDAYList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnPMHOLIDAY = new Persistence.Properties.TPMHOLIDAY();
        UnPMHOLIDAY.ByteTo(MemStream);
        Methods.PMHOLIDAY_ListAdd(PMHOLIDAYList, UnPMHOLIDAY);
    }
}
