//**********************   PROPERTIES   *****************************************************************************************
Persistence.ProjectManagement.Properties.TPMTYPERELATION = function() {
	this.IDPMTYPERELATION = 0;
	this.TYPERELATION_NAME = "";
	this.TYPERELATION_STATE = false;

	// Virtuales
	this.PMRELATIONList = new Array();

	//Socket IO Properties
	this.ToByte = function(MemStream){
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDPMTYPERELATION); 
		SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.TYPERELATION_NAME); 
		SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.TYPERELATION_STATE); 

	}
	this.ByteTo = function(MemStream)
	{
		this.IDPMTYPERELATION = SysCfg.Stream.Methods.ReadStreamInt32(MemStream); 
		this.TYPERELATION_NAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream); 
		this.TYPERELATION_STATE = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream); 

	}
	//Str IO Properties
	this.ToStr = function(Longitud,Texto)
	{
		SysCfg.Str.Protocol.WriteInt(this.IDPMTYPERELATION, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteStr(this.TYPERELATION_NAME, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteBool(this.TYPERELATION_STATE, Longitud, Texto); 

	}
	this.StrTo = function(Pos,Index,LongitudArray,Texto)
	{
		this.IDPMTYPERELATION = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto); 
		this.TYPERELATION_NAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto); 
		this.TYPERELATION_STATE = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto); 

	}
}

//**********************   METHODS server  ********************************************************************************************
Persistence.ProjectManagement.Methods.PMTYPERELATION_Fill = function(PMTYPERELATIONList,StrIDPMTYPERELATION/*noin IDPMTYPERELATION*/)
{
	var ResErr = new SysCfg.Error.Properties.TResErr();
	PMTYPERELATIONList.length = 0;
	var Param = new SysCfg.Stream.Properties.TParam();
	Param.Inicialize();
	if (StrIDPMTYPERELATION == "")
	{
		Param.AddUnknown(UsrCfg.InternoAtisNames.PMTYPERELATION.IDPMTYPERELATION.FieldName, " = " + UsrCfg.InternoAtisNames.PMTYPERELATION.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PMTYPERELATION.IDPMTYPERELATION.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
	}
	else
	{
		StrIDPMTYPERELATION = " IN (" + StrIDPMTYPERELATION + ")";
		Param.AddUnknown(UsrCfg.InternoAtisNames.PMTYPERELATION.IDPMTYPERELATION.FieldName, StrIDPMTYPERELATION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
	}
	//noin Param.AddInt32(UsrCfg.InternoDemoNames.PMTYPERELATION.IDPMTYPERELATION.FieldName, IDPMTYPERELATION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
	try
	{
		/*
		//********   PMTYPERELATION_GET   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PMTYPERELATION_GET", @"PMTYPERELATION_GET description.");  
		UnSQL.SQL.Add(@"   SELECT "); 
		 UnSQL.SQL.Add(@"   IDPMTYPERELATION, "); 
		 UnSQL.SQL.Add(@"   TYPERELATION_NAME, "); 
		 UnSQL.SQL.Add(@"   TYPERELATION_STATE "); 
		 UnSQL.SQL.Add(@"   FROM PMTYPERELATION "); 
		 UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		var DS_PMTYPERELATION = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "PMTYPERELATION_GET", Param.ToBytes());
		ResErr = DS_PMTYPERELATION.ResErr;
		if (ResErr.NotError)
		{
			if (DS_PMTYPERELATION.DataSet.RecordCount > 0)
			{
				DS_PMTYPERELATION.DataSet.First();
				while (!(DS_PMTYPERELATION.DataSet.Eof))
				{
					var PMTYPERELATION = new Persistence.ProjectManagement.Properties.TPMTYPERELATION();
					PMTYPERELATION.IDPMTYPERELATION = DS_PMTYPERELATION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMTYPERELATION.IDPMTYPERELATION.FieldName).asInt32();
					PMTYPERELATION.TYPERELATION_NAME = DS_PMTYPERELATION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMTYPERELATION.TYPERELATION_NAME.FieldName).asString();
					PMTYPERELATION.TYPERELATION_STATE = DS_PMTYPERELATION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMTYPERELATION.TYPERELATION_STATE.FieldName).asBoolean();
					 
					//Other
					PMTYPERELATIONList.push(PMTYPERELATION);
					DS_PMTYPERELATION.DataSet.Next();
				}
			}
			else
			{
				DS_PMTYPERELATION.ResErr.NotError = false;
				DS_PMTYPERELATION.ResErr.Mesaje = "RECORDCOUNT = 0";
			}
		}
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.ProjectManagement.Methods.PMTYPERELATION_GETID = function(PMTYPERELATION)
{
	var ResErr = new SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try
	{
		Param.Inicialize();
		//Other
		Param.AddInt32(UsrCfg.InternoAtisNames.PMTYPERELATION.IDPMTYPERELATION.FieldName, PMTYPERELATION.IDPMTYPERELATION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddString(UsrCfg.InternoAtisNames.PMTYPERELATION.TYPERELATION_NAME.FieldName, PMTYPERELATION.TYPERELATION_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddBoolean(UsrCfg.InternoAtisNames.PMTYPERELATION.TYPERELATION_STATE.FieldName, PMTYPERELATION.TYPERELATION_STATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

		/*
		//********   PMTYPERELATION_GETID   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PMTYPERELATION_GETID", @"PMTYPERELATION_GETID description.");  
		UnSQL.SQL.Add(@"   SELECT "); 
		 UnSQL.SQL.Add(@"   IDPMTYPERELATION "); 
		 UnSQL.SQL.Add(@"   FROM PMTYPERELATION "); 
		 UnSQL.SQL.Add(@"   Where "); 
		 UnSQL.SQL.Add(@"   TYPERELATION_NAME=@[TYPERELATION_NAME] And "); 
		 UnSQL.SQL.Add(@"   TYPERELATION_STATE=@[TYPERELATION_STATE] ");
		 UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		var DS_PMTYPERELATION = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "PMTYPERELATION_GETID", Param.ToBytes());
		ResErr = DS_PMTYPERELATION.ResErr;
		if (ResErr.NotError)
		{
			if (DS_PMTYPERELATION.DataSet.RecordCount > 0)
			{
			    DS_PMTYPERELATION.DataSet.First();
			    PMTYPERELATION.IDPMTYPERELATION = DS_PMTYPERELATION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMTYPERELATION.IDPMTYPERELATION.FieldName).asInt32();
			}
			else
			{
			    DS_PMTYPERELATION.ResErr.NotError = false;
			    DS_PMTYPERELATION.ResErr.Mesaje = "RECORDCOUNT = 0";
			}
		}
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.ProjectManagement.Methods.PMTYPERELATION_ADD = function(PMTYPERELATION)
{
	var ResErr = new SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try
	{
		Param.Inicialize();
		//Other
		Param.AddInt32(UsrCfg.InternoAtisNames.PMTYPERELATION.IDPMTYPERELATION.FieldName, PMTYPERELATION.IDPMTYPERELATION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.PMTYPERELATION.TYPERELATION_NAME.FieldName, PMTYPERELATION.TYPERELATION_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddBoolean(UsrCfg.InternoAtisNames.PMTYPERELATION.TYPERELATION_STATE.FieldName, PMTYPERELATION.TYPERELATION_STATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		 
		/* 
		//********   PMTYPERELATION_ADD   ************************* 
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PMTYPERELATION_ADD", @"PMTYPERELATION_ADD description."); 
		UnSQL.SQL.Add(@"   INSERT INTO PMTYPERELATION( "); 
		 UnSQL.SQL.Add(@"   TYPERELATION_NAME, "); 
		 UnSQL.SQL.Add(@"   TYPERELATION_STATE "); 
		 UnSQL.SQL.Add(@"   ) Values ( ");  
		UnSQL.SQL.Add(@"   @[TYPERELATION_NAME], "); 
		 UnSQL.SQL.Add(@"   @[TYPERELATION_STATE] "); 
		 UnSQL.SQL.Add(@"   )  "); 
		UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PMTYPERELATION_ADD", Param.ToBytes());
		ResErr = Exec.ResErr;
		if (ResErr.NotError)
		{
			Persistence.ProjectManagement.Methods.PMTYPERELATION_GETID(PMTYPERELATION);
		}
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.ProjectManagement.Methods.PMTYPERELATION_UPD = function(PMTYPERELATION)
{
	var ResErr = SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try
	{
		Param.Inicialize();
		Param.AddInt32(UsrCfg.InternoAtisNames.PMTYPERELATION.IDPMTYPERELATION.FieldName, PMTYPERELATION.IDPMTYPERELATION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.PMTYPERELATION.TYPERELATION_NAME.FieldName, PMTYPERELATION.TYPERELATION_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddBoolean(UsrCfg.InternoAtisNames.PMTYPERELATION.TYPERELATION_STATE.FieldName, PMTYPERELATION.TYPERELATION_STATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		 

		/*   
		//********   PMTYPERELATION_UPD   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PMTYPERELATION_UPD", @"PMTYPERELATION_UPD description.");  
		UnSQL.SQL.Add(@"  UPDATE PMTYPERELATION Set "); 
		 UnSQL.SQL.Add(@"   TYPERELATION_NAME=@[TYPERELATION_NAME], "); 
		 UnSQL.SQL.Add(@"   TYPERELATION_STATE=@[TYPERELATION_STATE] "); 
		 UnSQL.SQL.Add(@"   Where "); 
		 UnSQL.SQL.Add(@"   IDPMTYPERELATION=@[IDPMTYPERELATION] ");
		 UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PMTYPERELATION_UPD", Param.ToBytes());
		ResErr = Exec.ResErr;
		if (ResErr.NotError)
		{
		//GET(); 
		}
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.ProjectManagement.Methods.PMTYPERELATION_DEL = function(/*String StrIDPMTYPERELATION*/IDPMTYPERELATION)
{
	var ResErr = SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try
	{
		Param.Inicialize();
		/*if (StrIDPMTYPERELATION == "")
		{
		Param.AddUnknown(UsrCfg.InternoAtisNames.PMTYPERELATION.IDPMTYPERELATION.FieldName, " = " + UsrCfg.InternoAtisNames.PMTYPERELATION.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PMTYPERELATION.IDPMTYPERELATION.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
		}
		else
		{
		StrIDPMTYPERELATION = " IN (" + StrIDPMTYPERELATION + ")";
		Param.AddUnknown(UsrCfg.InternoAtisNames.PMTYPERELATION.IDPMTYPERELATION.FieldName, StrIDPMTYPERELATION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		}*/
		Param.AddInt32(UsrCfg.InternoAtisNames.PMTYPERELATION.IDPMTYPERELATION.FieldName, IDPMTYPERELATION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		/*
		//********   PMTYPERELATION_DEL   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PMTYPERELATION_DEL", @"PMTYPERELATION_DEL description.");  
		UnSQL.SQL.Add(@"   DELETE PMTYPERELATION "); 
		 UnSQL.SQL.Add(@"   Where "); 
		 UnSQL.SQL.Add(@"   IDPMTYPERELATION=@[IDPMTYPERELATION]   ");
		 UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PMTYPERELATION_DEL", Param.ToBytes());
		ResErr = Exec.ResErr;
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}


//**********************   METHODS   ********************************************************************************************
Persistence.ProjectManagement.Methods.PMTYPERELATION_ListSetID = function(PMTYPERELATIONList,IDPMTYPERELATION)
{
	for (i = 0; i < PMTYPERELATIONList.length; i++)
	{
		if (IDPMTYPERELATION == PMTYPERELATIONList[i].IDPMTYPERELATION)
			return (PMTYPERELATIONList[i]);
	}
	var UnPMTYPERELATION = new Persistence.Properties.TPMTYPERELATION;
	UnPMTYPERELATION.IDPMTYPERELATION = IDPMTYPERELATION; 
	return (UnPMTYPERELATION);
}
Persistence.ProjectManagement.Methods.PMTYPERELATION_ListAdd = function(PMTYPERELATIONList,PMTYPERELATION)
{
	var i = Persistence.ProjectManagement.Methods.PMTYPERELATION_ListGetIndex(PMTYPERELATIONList, PMTYPERELATION);
	if (i == -1) PMTYPERELATIONList.push(PMTYPERELATION);
	else
	{
		PMTYPERELATIONList[i].IDPMTYPERELATION = PMTYPERELATION.IDPMTYPERELATION;
		PMTYPERELATIONList[i].TYPERELATION_NAME = PMTYPERELATION.TYPERELATION_NAME;
		PMTYPERELATIONList[i].TYPERELATION_STATE = PMTYPERELATION.TYPERELATION_STATE;

	}
}
Persistence.ProjectManagement.Methods.PMTYPERELATION_ListGetIndex = function(PMTYPERELATIONList, PMTYPERELATION)
{
	for (i = 0; i < PMTYPERELATIONList.length; i++)
	{
		if (PMTYPERELATION.IDPMTYPERELATION == PMTYPERELATIONList[i].IDPMTYPERELATION)
			return (i);
	}
	return (-1);
}
Persistence.ProjectManagement.Methods.PMTYPERELATION_ListGetIndex = function(PMTYPERELATIONList, IDPMTYPERELATION)
{
	for (i = 0; i < PMTYPERELATIONList.length; i++)
	{
		if (IDPMTYPERELATION == PMTYPERELATIONList[i].IDPMTYPERELATION)
			return (i);
	}
	return (-1);
	}

//String Function 
Persistence.ProjectManagement.Methods.PMTYPERELATIONListtoStr = function(PMTYPERELATIONList)
{
	var Res = Persistence.Properties._Version + "(1)0";
	var Longitud = new SysCfg.ref("");
	var Texto = new SysCfg.ref("");
	try
	{
		SysCfg.Str.Protocol.WriteInt(PMTYPERELATIONList.length,Longitud,Texto);
		for (Counter = 0; Counter < PMTYPERELATIONList.length; Counter++)
		{
			var UnPMTYPERELATION = PMTYPERELATIONList[Counter];
			UnPMTYPERELATION.ToStr(Longitud,Texto);
		}

		Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
		Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
	}
	catch (e)
	{
		var ResErr = new SysCfg.Error.Properties.TResErr();
		ResErr.NotError = false;
		ResErr.Mesaje = "LIFESTATUStoStr:" + e;
	}
	return Res;
}
Persistence.ProjectManagement.Methods.StrtoPMTYPERELATION = function(ProtocoloStr, PMTYPERELATIONList)
{

	var Res = false;
	var Longitud, Texto;
	var Index =  new SysCfg.ref(0);
	var Pos =  new SysCfg.ref(0);
	try
	{
		PMTYPERELATIONList.length = 0;   
		if (Persistence.ProjectManagement.Properties._Version == ProtocoloStr.substring(0, 3))
		{
			Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")") - ProtocoloStr.indexOf("(") - 1);
			Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.Length - ProtocoloStr.indexOf(")") - 1);
			LongitudArray = Longitud.split(",");
			LSCount = SysCfg.Str.Protocol.ReadInt(Pos,Index,LongitudArray,Texto);
			for (i = 0; i < LSCount; i++)
			{
			    UnPMTYPERELATION = new Persistence.Properties.TPMTYPERELATION(); //Mode new row
			    UnPMTYPERELATION.StrTo(Pos,Index, LongitudArray, Texto);
			    Persistence.ProjectManagement.PMTYPERELATION_ListAdd(PMTYPERELATIONList, UnPMTYPERELATION);
			}
		}
	}
	catch (e)
	{
		var ResErr = SysCfg.Error.Properties.TResErr;
		ResErr.NotError = false;
		ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
	}
	return (Res);
}


//Socket Function 
Persistence.ProjectManagement.Methods.PMTYPERELATIONListToByte = function(PMTYPERELATIONList, MemStream, idx)
{
	SysCfg.Stream.Methods.WriteStreamInt32(MemStream, PMTYPERELATIONList.Count-idx);
	for (i = idx; i < PMTYPERELATIONList.Count; i++)
	{
		PMTYPERELATIONList[i].ToByte(MemStream);
	}
}
Persistence.ProjectManagement.Methods.ByteToPMTYPERELATIONList = function (PMTYPERELATIONList, MemStream)
{
	Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
	for (i = 0; i < Count; i++)
	{
		UnPMTYPERELATION = new Persistence.Properties.TPMTYPERELATION();
		UnPMTYPERELATION.ByteTo(MemStream);
		Methods.PMTYPERELATION_ListAdd(PMTYPERELATIONList,UnPMTYPERELATION);
	}
}
