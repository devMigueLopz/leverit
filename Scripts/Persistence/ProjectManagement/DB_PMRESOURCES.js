//**********************   PROPERTIES   *****************************************************************************************
Persistence.ProjectManagement.Properties.TPMRESOURCES = function() {
	this.IDPMRESOURCES = 0;
	this.IDPMMAINTASK = 0;
	this.RESOURCES_NAME = "";
	this.RESOURCES_ALLOCATIONOWNER = "";
	this.RESOURCES_UNITS = 0;
	this.RESOURCES_COST = "";

	// Virtuales
	this.PMMAINTASK = new Persistence.ProjectManagement.Properties.TPMMAINTASK();
	this.PMRESOURCESALLOCATIONList = new Array();
	


	//Socket IO Properties
	this.ToByte = function(MemStream){
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDPMRESOURCES); 
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDPMMAINTASK); 
		SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.RESOURCES_NAME); 
		SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.RESOURCES_ALLOCATIONOWNER); 
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.RESOURCES_UNITS); 
		SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.RESOURCES_COST); 

	}
	this.ByteTo = function(MemStream)
	{
		this.IDPMRESOURCES = SysCfg.Stream.Methods.ReadStreamInt32(MemStream); 
		this.IDPMMAINTASK = SysCfg.Stream.Methods.ReadStreamInt32(MemStream); 
		this.RESOURCES_NAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream); 
		this.RESOURCES_ALLOCATIONOWNER = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream); 
		this.RESOURCES_UNITS = SysCfg.Stream.Methods.ReadStreamInt32(MemStream); 
		this.RESOURCES_COST = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream); 

	}
	//Str IO Properties
	this.ToStr = function(Longitud,Texto)
	{
		SysCfg.Str.Protocol.WriteInt(this.IDPMRESOURCES, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteInt(this.IDPMMAINTASK, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteStr(this.RESOURCES_NAME, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteStr(this.RESOURCES_ALLOCATIONOWNER, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteInt(this.RESOURCES_UNITS, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteStr(this.RESOURCES_COST, Longitud, Texto); 

	}
	this.StrTo = function(Pos,Index,LongitudArray,Texto)
	{
		this.IDPMRESOURCES = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto); 
		this.IDPMMAINTASK = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto); 
		this.RESOURCES_NAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto); 
		this.RESOURCES_ALLOCATIONOWNER = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto); 
		this.RESOURCES_UNITS = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto); 
		this.RESOURCES_COST = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto); 

	}
}

//**********************   METHODS server  ********************************************************************************************
Persistence.ProjectManagement.Methods.PMRESOURCES_Fill = function(PMRESOURCESList,StrIDPMRESOURCES/*noin IDPMRESOURCES*/)
{
	var ResErr = new SysCfg.Error.Properties.TResErr();
	PMRESOURCESList.length = 0;
	var Param = new SysCfg.Stream.Properties.TParam();
	Param.Inicialize();
	if (StrIDPMRESOURCES == "")
	{
		Param.AddUnknown(UsrCfg.InternoAtisNames.PMRESOURCES.IDPMRESOURCES.FieldName, " = " + UsrCfg.InternoAtisNames.PMRESOURCES.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PMRESOURCES.IDPMRESOURCES.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
	}
	else
	{
		StrIDPMRESOURCES = " IN (" + StrIDPMRESOURCES + ")";
		Param.AddUnknown(UsrCfg.InternoAtisNames.PMRESOURCES.IDPMRESOURCES.FieldName, StrIDPMRESOURCES, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
	}
	//noin Param.AddInt32(UsrCfg.InternoDemoNames.PMRESOURCES.IDPMRESOURCES.FieldName, IDPMRESOURCES, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
	try
	{
		/*
		//********   PMRESOURCES_GET   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PMRESOURCES_GET", @"PMRESOURCES_GET description.");  
		UnSQL.SQL.Add(@"   SELECT "); 
		 UnSQL.SQL.Add(@"   IDPMRESOURCES, "); 
		 UnSQL.SQL.Add(@"   IDPMMAINTASK, "); 
		 UnSQL.SQL.Add(@"   RESOURCES_NAME, "); 
		 UnSQL.SQL.Add(@"   RESOURCES_ALLOCATIONOWNER, "); 
		 UnSQL.SQL.Add(@"   RESOURCES_UNITS, "); 
		 UnSQL.SQL.Add(@"   RESOURCES_COST "); 
		 UnSQL.SQL.Add(@"   FROM PMRESOURCES "); 
		 UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		var DS_PMRESOURCES = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "PMRESOURCES_GET", Param.ToBytes());
		ResErr = DS_PMRESOURCES.ResErr;
		if (ResErr.NotError)
		{
			if (DS_PMRESOURCES.DataSet.RecordCount > 0)
			{
				DS_PMRESOURCES.DataSet.First();
				while (!(DS_PMRESOURCES.DataSet.Eof))
				{
					var PMRESOURCES = new Persistence.ProjectManagement.Properties.TPMRESOURCES();
					PMRESOURCES.IDPMRESOURCES = DS_PMRESOURCES.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMRESOURCES.IDPMRESOURCES.FieldName).asInt32();
					PMRESOURCES.IDPMMAINTASK = DS_PMRESOURCES.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMRESOURCES.IDPMMAINTASK.FieldName).asInt32();
					PMRESOURCES.RESOURCES_NAME = DS_PMRESOURCES.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMRESOURCES.RESOURCES_NAME.FieldName).asString();
					PMRESOURCES.RESOURCES_ALLOCATIONOWNER = DS_PMRESOURCES.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMRESOURCES.RESOURCES_ALLOCATIONOWNER.FieldName).asString();
					PMRESOURCES.RESOURCES_UNITS = DS_PMRESOURCES.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMRESOURCES.RESOURCES_UNITS.FieldName).asInt32();
					PMRESOURCES.RESOURCES_COST = DS_PMRESOURCES.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMRESOURCES.RESOURCES_COST.FieldName).asString();
					 
					//Other
					PMRESOURCESList.push(PMRESOURCES);
					DS_PMRESOURCES.DataSet.Next();
				}
			}
			else
			{
				DS_PMRESOURCES.ResErr.NotError = false;
				DS_PMRESOURCES.ResErr.Mesaje = "RECORDCOUNT = 0";
			}
		}
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.ProjectManagement.Methods.PMRESOURCES_GETID = function(PMRESOURCES)
{
	var ResErr = new SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try
	{
		Param.Inicialize();
		//Other
		Param.AddInt32(UsrCfg.InternoAtisNames.PMRESOURCES.IDPMRESOURCES.FieldName, PMRESOURCES.IDPMRESOURCES, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddInt32(UsrCfg.InternoAtisNames.PMRESOURCES.IDPMMAINTASK.FieldName, PMRESOURCES.IDPMMAINTASK, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddString(UsrCfg.InternoAtisNames.PMRESOURCES.RESOURCES_NAME.FieldName, PMRESOURCES.RESOURCES_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddString(UsrCfg.InternoAtisNames.PMRESOURCES.RESOURCES_ALLOCATIONOWNER.FieldName, PMRESOURCES.RESOURCES_ALLOCATIONOWNER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddInt32(UsrCfg.InternoAtisNames.PMRESOURCES.RESOURCES_UNITS.FieldName, PMRESOURCES.RESOURCES_UNITS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddString(UsrCfg.InternoAtisNames.PMRESOURCES.RESOURCES_COST.FieldName, PMRESOURCES.RESOURCES_COST, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

		/*
		//********   PMRESOURCES_GETID   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PMRESOURCES_GETID", @"PMRESOURCES_GETID description.");  
		UnSQL.SQL.Add(@"   SELECT "); 
		 UnSQL.SQL.Add(@"   IDPMRESOURCES "); 
		 UnSQL.SQL.Add(@"   FROM PMRESOURCES "); 
		 UnSQL.SQL.Add(@"   Where "); 
		 UnSQL.SQL.Add(@"   IDPMMAINTASK=@[IDPMMAINTASK] And "); 
		 UnSQL.SQL.Add(@"   RESOURCES_NAME=@[RESOURCES_NAME] And "); 
		 UnSQL.SQL.Add(@"   RESOURCES_ALLOCATIONOWNER=@[RESOURCES_ALLOCATIONOWNER] And "); 
		 UnSQL.SQL.Add(@"   RESOURCES_UNITS=@[RESOURCES_UNITS] And "); 
		 UnSQL.SQL.Add(@"   RESOURCES_COST=@[RESOURCES_COST] ");
		 UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		var DS_PMRESOURCES = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "PMRESOURCES_GETID", Param.ToBytes());
		ResErr = DS_PMRESOURCES.ResErr;
		if (ResErr.NotError)
		{
			if (DS_PMRESOURCES.DataSet.RecordCount > 0)
			{
			    DS_PMRESOURCES.DataSet.First();
			    PMRESOURCES.IDPMRESOURCES = DS_PMRESOURCES.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMRESOURCES.IDPMRESOURCES.FieldName).asInt32();
			}
			else
			{
			    DS_PMRESOURCES.ResErr.NotError = false;
			    DS_PMRESOURCES.ResErr.Mesaje = "RECORDCOUNT = 0";
			}
		}
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.ProjectManagement.Methods.PMRESOURCES_ADD = function(PMRESOURCES)
{
	var ResErr = new SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try
	{
		Param.Inicialize();
		//Other
		Param.AddInt32(UsrCfg.InternoAtisNames.PMRESOURCES.IDPMRESOURCES.FieldName, PMRESOURCES.IDPMRESOURCES, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.PMRESOURCES.IDPMMAINTASK.FieldName, PMRESOURCES.IDPMMAINTASK, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.PMRESOURCES.RESOURCES_NAME.FieldName, PMRESOURCES.RESOURCES_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.PMRESOURCES.RESOURCES_ALLOCATIONOWNER.FieldName, PMRESOURCES.RESOURCES_ALLOCATIONOWNER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.PMRESOURCES.RESOURCES_UNITS.FieldName, PMRESOURCES.RESOURCES_UNITS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.PMRESOURCES.RESOURCES_COST.FieldName, PMRESOURCES.RESOURCES_COST, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		 
		/* 
		//********   PMRESOURCES_ADD   ************************* 
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PMRESOURCES_ADD", @"PMRESOURCES_ADD description."); 
		UnSQL.SQL.Add(@"   INSERT INTO PMRESOURCES( "); 
		 UnSQL.SQL.Add(@"   IDPMMAINTASK, "); 
		 UnSQL.SQL.Add(@"   RESOURCES_NAME, "); 
		 UnSQL.SQL.Add(@"   RESOURCES_ALLOCATIONOWNER, "); 
		 UnSQL.SQL.Add(@"   RESOURCES_UNITS, "); 
		 UnSQL.SQL.Add(@"   RESOURCES_COST "); 
		 UnSQL.SQL.Add(@"   ) Values ( ");  
		UnSQL.SQL.Add(@"   @[IDPMMAINTASK], "); 
		 UnSQL.SQL.Add(@"   @[RESOURCES_NAME], "); 
		 UnSQL.SQL.Add(@"   @[RESOURCES_ALLOCATIONOWNER], "); 
		 UnSQL.SQL.Add(@"   @[RESOURCES_UNITS], "); 
		 UnSQL.SQL.Add(@"   @[RESOURCES_COST] "); 
		 UnSQL.SQL.Add(@"   )  "); 
		UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PMRESOURCES_ADD", Param.ToBytes());
		ResErr = Exec.ResErr;
		if (ResErr.NotError)
		{
			Persistence.ProjectManagement.Methods.PMRESOURCES_GETID(PMRESOURCES);
		}
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.ProjectManagement.Methods.PMRESOURCES_UPD = function(PMRESOURCES)
{
	var ResErr = SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try
	{
		Param.Inicialize();
		Param.AddInt32(UsrCfg.InternoAtisNames.PMRESOURCES.IDPMRESOURCES.FieldName, PMRESOURCES.IDPMRESOURCES, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.PMRESOURCES.IDPMMAINTASK.FieldName, PMRESOURCES.IDPMMAINTASK, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.PMRESOURCES.RESOURCES_NAME.FieldName, PMRESOURCES.RESOURCES_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.PMRESOURCES.RESOURCES_ALLOCATIONOWNER.FieldName, PMRESOURCES.RESOURCES_ALLOCATIONOWNER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.PMRESOURCES.RESOURCES_UNITS.FieldName, PMRESOURCES.RESOURCES_UNITS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.PMRESOURCES.RESOURCES_COST.FieldName, PMRESOURCES.RESOURCES_COST, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		 

		/*   
		//********   PMRESOURCES_UPD   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PMRESOURCES_UPD", @"PMRESOURCES_UPD description.");  
		UnSQL.SQL.Add(@"  UPDATE PMRESOURCES Set "); 
		 UnSQL.SQL.Add(@"   IDPMMAINTASK=@[IDPMMAINTASK], "); 
		 UnSQL.SQL.Add(@"   RESOURCES_NAME=@[RESOURCES_NAME], "); 
		 UnSQL.SQL.Add(@"   RESOURCES_ALLOCATIONOWNER=@[RESOURCES_ALLOCATIONOWNER], "); 
		 UnSQL.SQL.Add(@"   RESOURCES_UNITS=@[RESOURCES_UNITS], "); 
		 UnSQL.SQL.Add(@"   RESOURCES_COST=@[RESOURCES_COST] "); 
		 UnSQL.SQL.Add(@"   Where "); 
		 UnSQL.SQL.Add(@"   IDPMRESOURCES=@[IDPMRESOURCES] ");
		 UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PMRESOURCES_UPD", Param.ToBytes());
		ResErr = Exec.ResErr;
		if (ResErr.NotError)
		{
		//GET(); 
		}
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.ProjectManagement.Methods.PMRESOURCES_DEL = function(/*String StrIDPMRESOURCES*/IDPMRESOURCES)
{
	var ResErr = SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try
	{
		Param.Inicialize();
		/*if (StrIDPMRESOURCES == "")
		{
		Param.AddUnknown(UsrCfg.InternoAtisNames.PMRESOURCES.IDPMRESOURCES.FieldName, " = " + UsrCfg.InternoAtisNames.PMRESOURCES.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PMRESOURCES.IDPMRESOURCES.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
		}
		else
		{
		StrIDPMRESOURCES = " IN (" + StrIDPMRESOURCES + ")";
		Param.AddUnknown(UsrCfg.InternoAtisNames.PMRESOURCES.IDPMRESOURCES.FieldName, StrIDPMRESOURCES, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		}*/
		Param.AddInt32(UsrCfg.InternoAtisNames.PMRESOURCES.IDPMRESOURCES.FieldName, IDPMRESOURCES, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		/*
		//********   PMRESOURCES_DEL   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PMRESOURCES_DEL", @"PMRESOURCES_DEL description.");  
		UnSQL.SQL.Add(@"   DELETE PMRESOURCES "); 
		 UnSQL.SQL.Add(@"   Where "); 
		 UnSQL.SQL.Add(@"   IDPMRESOURCES=@[IDPMRESOURCES]   ");
		 UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PMRESOURCES_DEL", Param.ToBytes());
		ResErr = Exec.ResErr;
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}


//**********************   METHODS   ********************************************************************************************
Persistence.ProjectManagement.Methods.PMRESOURCES_ListSetID = function(PMRESOURCESList,IDPMRESOURCES)
{
	for (i = 0; i < PMRESOURCESList.length; i++)
	{
		if (IDPMRESOURCES == PMRESOURCESList[i].IDPMRESOURCES)
			return (PMRESOURCESList[i]);
	}
	var UnPMRESOURCES = new Persistence.Properties.TPMRESOURCES;
	UnPMRESOURCES.IDPMRESOURCES = IDPMRESOURCES; 
	return (UnPMRESOURCES);
}
Persistence.ProjectManagement.Methods.PMRESOURCES_ListAdd = function(PMRESOURCESList,PMRESOURCES)
{
	var i = Persistence.ProjectManagement.Methods.PMRESOURCES_ListGetIndex(PMRESOURCESList, PMRESOURCES);
	if (i == -1) PMRESOURCESList.push(PMRESOURCES);
	else
	{
		PMRESOURCESList[i].IDPMRESOURCES = PMRESOURCES.IDPMRESOURCES;
		PMRESOURCESList[i].IDPMMAINTASK = PMRESOURCES.IDPMMAINTASK;
		PMRESOURCESList[i].RESOURCES_NAME = PMRESOURCES.RESOURCES_NAME;
		PMRESOURCESList[i].RESOURCES_ALLOCATIONOWNER = PMRESOURCES.RESOURCES_ALLOCATIONOWNER;
		PMRESOURCESList[i].RESOURCES_UNITS = PMRESOURCES.RESOURCES_UNITS;
		PMRESOURCESList[i].RESOURCES_COST = PMRESOURCES.RESOURCES_COST;

	}
}
Persistence.ProjectManagement.Methods.PMRESOURCES_ListGetIndex = function(PMRESOURCESList, PMRESOURCES)
{
	for (i = 0; i < PMRESOURCESList.length; i++)
	{
		if (PMRESOURCES.IDPMRESOURCES == PMRESOURCESList[i].IDPMRESOURCES)
			return (i);
	}
	return (-1);
}
Persistence.ProjectManagement.Methods.PMRESOURCES_ListGetIndex = function(PMRESOURCESList, IDPMRESOURCES)
{
	for (i = 0; i < PMRESOURCESList.length; i++)
	{
		if (IDPMRESOURCES == PMRESOURCESList[i].IDPMRESOURCES)
			return (i);
	}
	return (-1);
}

//String Function 
Persistence.ProjectManagement.Methods.PMRESOURCESListtoStr = function(PMRESOURCESList)
{
	var Res = Persistence.Properties._Version + "(1)0";
	var Longitud = new SysCfg.ref("");
	var Texto = new SysCfg.ref("");
	try
	{
		SysCfg.Str.Protocol.WriteInt(PMRESOURCESList.length,Longitud,Texto);
		for (Counter = 0; Counter < PMRESOURCESList.length; Counter++)
		{
			var UnPMRESOURCES = PMRESOURCESList[Counter];
			UnPMRESOURCES.ToStr(Longitud,Texto);
		}

		Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
		Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
	}
	catch (e)
	{
		var ResErr = new SysCfg.Error.Properties.TResErr();
		ResErr.NotError = false;
		ResErr.Mesaje = "LIFESTATUStoStr:" + e;
	}
	return Res;
}
Persistence.ProjectManagement.Methods.StrtoPMRESOURCES = function(ProtocoloStr, PMRESOURCESList)
{

	var Res = false;
	var Longitud, Texto;
	var Index =  new SysCfg.ref(0);
	var Pos =  new SysCfg.ref(0);
	try
	{
		PMRESOURCESList.length = 0;   
		if (Persistence.ProjectManagement.Properties._Version == ProtocoloStr.substring(0, 3))
		{
			Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")") - ProtocoloStr.indexOf("(") - 1);
			Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.Length - ProtocoloStr.indexOf(")") - 1);
			LongitudArray = Longitud.split(",");
			LSCount = SysCfg.Str.Protocol.ReadInt(Pos,Index,LongitudArray,Texto);
			for (i = 0; i < LSCount; i++)
			{
			    UnPMRESOURCES = new Persistence.Properties.TPMRESOURCES(); //Mode new row
			    UnPMRESOURCES.StrTo(Pos,Index, LongitudArray, Texto);
			    Persistence.ProjectManagement.PMRESOURCES_ListAdd(PMRESOURCESList, UnPMRESOURCES);
			}
		}
	}
	catch (e)
	{
		var ResErr = SysCfg.Error.Properties.TResErr;
		ResErr.NotError = false;
		ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
	}
	return (Res);
}


//Socket Function 
Persistence.ProjectManagement.Methods.PMRESOURCESListToByte = function(PMRESOURCESList, MemStream, idx)
{
	SysCfg.Stream.Methods.WriteStreamInt32(MemStream, PMRESOURCESList.Count-idx);
	for (i = idx; i < PMRESOURCESList.Count; i++)
	{
		PMRESOURCESList[i].ToByte(MemStream);
	}
}
Persistence.ProjectManagement.Methods.ByteToPMRESOURCESList = function (PMRESOURCESList, MemStream)
{
	Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
	for (i = 0; i < Count; i++)
	{
		UnPMRESOURCES = new Persistence.Properties.TPMRESOURCES();
		UnPMRESOURCES.ByteTo(MemStream);
		Methods.PMRESOURCES_ListAdd(PMRESOURCESList,UnPMRESOURCES);
	}
}
