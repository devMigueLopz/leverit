//**********************   PROPERTIES   *****************************************************************************************
Persistence.ProjectManagement.Properties.TPMDETAILTASK = function() {
	this.IDPMDETAILTASK = 0;
	this.IDPMMAINTASK = 0;
	this.DETAILTASK_NAME = "";
	this.DETAILTASK_DURATION = "";
	this.DETAILTASK_STARTDATE = new Date(1970, 0, 1, 0, 0, 0);
	this.DETAILTASK_ENDDATE = new Date(1970, 0, 1, 0, 0, 0);
	this.DETAILTASK_INDEX = 0;
	this.DETAILTASK_PERCENTCOMPLETE = 0;
	this.DETAILTASK_PRIORITY = 0;
	this.DETAILTASK_ESTIMATED = false;
	this.DETAILTASK_NOTE = "";
	this.IDPMDETAILTASK_PARENT = 0;
	this.DETAILTASK_STARTDATETEMP = new Date(1970, 0, 1, 0, 0, 0);
	this.DETAILTASK_ENDDATETEMP = new Date(1970, 0, 1, 0, 0, 0);
	this.DETAILTASK_DURATIONTEMP = "";

	// Virtuales
	this.PMMAINTASK = new Persistence.ProjectManagement.Properties.TPMMAINTASK();
	this.PMDETAILTASK_PARENT = null;
	this.PMDETAILTASK_CHILDList = new Array();
	this.PMRELATIONList = new Array();
	this.PMRELATION_SUCCESSORSList = new Array();
	this.PMRESOURCESALLOCATIONList = new Array();


	//Socket IO Properties
	this.ToByte = function(MemStream){
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDPMDETAILTASK); 
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDPMMAINTASK); 
		SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.DETAILTASK_NAME); 
		SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.DETAILTASK_DURATION); 
		SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.DETAILTASK_STARTDATE); 
		SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.DETAILTASK_ENDDATE); 
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.DETAILTASK_INDEX); 
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.DETAILTASK_PERCENTCOMPLETE); 
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.DETAILTASK_PRIORITY); 
		SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.DETAILTASK_ESTIMATED); 
		SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.DETAILTASK_NOTE); 
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDPMDETAILTASK_PARENT); 
		SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.DETAILTASK_STARTDATETEMP); 
		SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.DETAILTASK_ENDDATETEMP); 
		SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.DETAILTASK_DURATIONTEMP); 

	}
	this.ByteTo = function(MemStream)
	{
		this.IDPMDETAILTASK = SysCfg.Stream.Methods.ReadStreamInt32(MemStream); 
		this.IDPMMAINTASK = SysCfg.Stream.Methods.ReadStreamInt32(MemStream); 
		this.DETAILTASK_NAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream); 
		this.DETAILTASK_DURATION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream); 
		this.DETAILTASK_STARTDATE = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream); 
		this.DETAILTASK_ENDDATE = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream); 
		this.DETAILTASK_INDEX = SysCfg.Stream.Methods.ReadStreamInt32(MemStream); 
		this.DETAILTASK_PERCENTCOMPLETE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream); 
		this.DETAILTASK_PRIORITY = SysCfg.Stream.Methods.ReadStreamInt32(MemStream); 
		this.DETAILTASK_ESTIMATED = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream); 
		this.DETAILTASK_NOTE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream); 
		this.IDPMDETAILTASK_PARENT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream); 
		this.DETAILTASK_STARTDATETEMP = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream); 
		this.DETAILTASK_ENDDATETEMP = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream); 
		this.DETAILTASK_DURATIONTEMP = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream); 

	}
	//Str IO Properties
	this.ToStr = function(Longitud,Texto)
	{
		SysCfg.Str.Protocol.WriteInt(this.IDPMDETAILTASK, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteInt(this.IDPMMAINTASK, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteStr(this.DETAILTASK_NAME, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteStr(this.DETAILTASK_DURATION, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteDateTime(this.DETAILTASK_STARTDATE, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteDateTime(this.DETAILTASK_ENDDATE, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteInt(this.DETAILTASK_INDEX, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteInt(this.DETAILTASK_PERCENTCOMPLETE, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteInt(this.DETAILTASK_PRIORITY, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteBool(this.DETAILTASK_ESTIMATED, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteStr(this.DETAILTASK_NOTE, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteInt(this.IDPMDETAILTASK_PARENT, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteDateTime(this.DETAILTASK_STARTDATETEMP, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteDateTime(this.DETAILTASK_ENDDATETEMP, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteStr(this.DETAILTASK_DURATIONTEMP, Longitud, Texto); 

	}
	this.StrTo = function(Pos,Index,LongitudArray,Texto)
	{
		this.IDPMDETAILTASK = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto); 
		this.IDPMMAINTASK = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto); 
		this.DETAILTASK_NAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto); 
		this.DETAILTASK_DURATION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto); 
		this.DETAILTASK_STARTDATE = SysCfg.Str.Protocol.ReadDateTime(Pos, Index, LongitudArray, Texto); 
		this.DETAILTASK_ENDDATE = SysCfg.Str.Protocol.ReadDateTime(Pos, Index, LongitudArray, Texto); 
		this.DETAILTASK_INDEX = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto); 
		this.DETAILTASK_PERCENTCOMPLETE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto); 
		this.DETAILTASK_PRIORITY = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto); 
		this.DETAILTASK_ESTIMATED = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto); 
		this.DETAILTASK_NOTE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto); 
		this.IDPMDETAILTASK_PARENT = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto); 
		this.DETAILTASK_STARTDATETEMP = SysCfg.Str.Protocol.ReadDateTime(Pos, Index, LongitudArray, Texto); 
		this.DETAILTASK_ENDDATETEMP = SysCfg.Str.Protocol.ReadDateTime(Pos, Index, LongitudArray, Texto); 
		this.DETAILTASK_DURATIONTEMP = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto); 

	}
}

//**********************   METHODS server  ********************************************************************************************
Persistence.ProjectManagement.Methods.PMDETAILTASK_Fill = function(PMDETAILTASKList,StrIDPMDETAILTASK/*noin IDPMDETAILTASK*/)
{
	var ResErr = new SysCfg.Error.Properties.TResErr();
	PMDETAILTASKList.length = 0;
	var Param = new SysCfg.Stream.Properties.TParam();
	Param.Inicialize();
	if (StrIDPMDETAILTASK == "")
	{
		Param.AddUnknown(UsrCfg.InternoAtisNames.PMDETAILTASK.IDPMDETAILTASK.FieldName, " = " + UsrCfg.InternoAtisNames.PMDETAILTASK.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PMDETAILTASK.IDPMDETAILTASK.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
	}
	else
	{
		StrIDPMDETAILTASK = " IN (" + StrIDPMDETAILTASK + ")";
		Param.AddUnknown(UsrCfg.InternoAtisNames.PMDETAILTASK.IDPMDETAILTASK.FieldName, StrIDPMDETAILTASK, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
	}
	//noin Param.AddInt32(UsrCfg.InternoDemoNames.PMDETAILTASK.IDPMDETAILTASK.FieldName, IDPMDETAILTASK, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
	try
	{
		/*
		//********   PMDETAILTASK_GET   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PMDETAILTASK_GET", @"PMDETAILTASK_GET description.");  
		UnSQL.SQL.Add(@"   SELECT "); 
		 UnSQL.SQL.Add(@"   IDPMDETAILTASK, "); 
		 UnSQL.SQL.Add(@"   IDPMMAINTASK, "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_NAME, "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_DURATION, "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_STARTDATE, "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_ENDDATE, "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_INDEX, "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_PERCENTCOMPLETE, "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_PRIORITY, "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_ESTIMATED, "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_NOTE, "); 
		 UnSQL.SQL.Add(@"   IDPMDETAILTASK_PARENT, "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_STARTDATETEMP, "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_ENDDATETEMP, "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_DURATIONTEMP "); 
		 UnSQL.SQL.Add(@"   FROM PMDETAILTASK "); 
		 UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		var DS_PMDETAILTASK = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "PMDETAILTASK_GET", Param.ToBytes());
		ResErr = DS_PMDETAILTASK.ResErr;
		if (ResErr.NotError)
		{
			if (DS_PMDETAILTASK.DataSet.RecordCount > 0)
			{
				DS_PMDETAILTASK.DataSet.First();
				while (!(DS_PMDETAILTASK.DataSet.Eof))
				{
					var PMDETAILTASK = new Persistence.ProjectManagement.Properties.TPMDETAILTASK();
					PMDETAILTASK.IDPMDETAILTASK = DS_PMDETAILTASK.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMDETAILTASK.IDPMDETAILTASK.FieldName).asInt32();
					PMDETAILTASK.IDPMMAINTASK = DS_PMDETAILTASK.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMDETAILTASK.IDPMMAINTASK.FieldName).asInt32();
					PMDETAILTASK.DETAILTASK_NAME = DS_PMDETAILTASK.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_NAME.FieldName).asString();
					PMDETAILTASK.DETAILTASK_DURATION = DS_PMDETAILTASK.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_DURATION.FieldName).asString();
					PMDETAILTASK.DETAILTASK_STARTDATE = DS_PMDETAILTASK.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_STARTDATE.FieldName).asDateTime();
					PMDETAILTASK.DETAILTASK_ENDDATE = DS_PMDETAILTASK.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_ENDDATE.FieldName).asDateTime();
					PMDETAILTASK.DETAILTASK_INDEX = DS_PMDETAILTASK.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_INDEX.FieldName).asInt32();
					PMDETAILTASK.DETAILTASK_PERCENTCOMPLETE = DS_PMDETAILTASK.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_PERCENTCOMPLETE.FieldName).asInt32();
					PMDETAILTASK.DETAILTASK_PRIORITY = DS_PMDETAILTASK.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_PRIORITY.FieldName).asInt32();
					PMDETAILTASK.DETAILTASK_ESTIMATED = DS_PMDETAILTASK.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_ESTIMATED.FieldName).asBoolean();
					PMDETAILTASK.DETAILTASK_NOTE = DS_PMDETAILTASK.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_NOTE.FieldName).asText();
					PMDETAILTASK.IDPMDETAILTASK_PARENT = DS_PMDETAILTASK.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMDETAILTASK.IDPMDETAILTASK_PARENT.FieldName).asInt32();
					PMDETAILTASK.DETAILTASK_STARTDATETEMP = DS_PMDETAILTASK.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_STARTDATETEMP.FieldName).asDateTime();
					PMDETAILTASK.DETAILTASK_ENDDATETEMP = DS_PMDETAILTASK.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_ENDDATETEMP.FieldName).asDateTime();
					PMDETAILTASK.DETAILTASK_DURATIONTEMP = DS_PMDETAILTASK.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_DURATIONTEMP.FieldName).asString();
					 
					//Other
					PMDETAILTASKList.push(PMDETAILTASK);
					DS_PMDETAILTASK.DataSet.Next();
				}
			}
			else
			{
				DS_PMDETAILTASK.ResErr.NotError = false;
				DS_PMDETAILTASK.ResErr.Mesaje = "RECORDCOUNT = 0";
			}
		}
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.ProjectManagement.Methods.PMDETAILTASK_GETID = function(PMDETAILTASK)
{
	var ResErr = new SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try
	{
		Param.Inicialize();
		//Other
		Param.AddInt32(UsrCfg.InternoAtisNames.PMDETAILTASK.IDPMDETAILTASK.FieldName, PMDETAILTASK.IDPMDETAILTASK, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddInt32(UsrCfg.InternoAtisNames.PMDETAILTASK.IDPMMAINTASK.FieldName, PMDETAILTASK.IDPMMAINTASK, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddString(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_NAME.FieldName, PMDETAILTASK.DETAILTASK_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddString(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_DURATION.FieldName, PMDETAILTASK.DETAILTASK_DURATION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddDateTime(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_STARTDATE.FieldName, PMDETAILTASK.DETAILTASK_STARTDATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddDateTime(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_ENDDATE.FieldName, PMDETAILTASK.DETAILTASK_ENDDATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddInt32(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_INDEX.FieldName, PMDETAILTASK.DETAILTASK_INDEX, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddInt32(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_PERCENTCOMPLETE.FieldName, PMDETAILTASK.DETAILTASK_PERCENTCOMPLETE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddInt32(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_PRIORITY.FieldName, PMDETAILTASK.DETAILTASK_PRIORITY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddBoolean(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_ESTIMATED.FieldName, PMDETAILTASK.DETAILTASK_ESTIMATED, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddText(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_NOTE.FieldName, PMDETAILTASK.DETAILTASK_NOTE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddInt32(UsrCfg.InternoAtisNames.PMDETAILTASK.IDPMDETAILTASK_PARENT.FieldName, PMDETAILTASK.IDPMDETAILTASK_PARENT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddDateTime(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_STARTDATETEMP.FieldName, PMDETAILTASK.DETAILTASK_STARTDATETEMP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddDateTime(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_ENDDATETEMP.FieldName, PMDETAILTASK.DETAILTASK_ENDDATETEMP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddString(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_DURATIONTEMP.FieldName, PMDETAILTASK.DETAILTASK_DURATIONTEMP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

		/*
		//********   PMDETAILTASK_GETID   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PMDETAILTASK_GETID", @"PMDETAILTASK_GETID description.");  
		UnSQL.SQL.Add(@"   SELECT "); 
		 UnSQL.SQL.Add(@"   IDPMDETAILTASK "); 
		 UnSQL.SQL.Add(@"   FROM PMDETAILTASK "); 
		 UnSQL.SQL.Add(@"   Where "); 
		 UnSQL.SQL.Add(@"   IDPMMAINTASK=@[IDPMMAINTASK] And "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_NAME=@[DETAILTASK_NAME] And "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_DURATION=@[DETAILTASK_DURATION] And "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_STARTDATE=@[DETAILTASK_STARTDATE] And "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_ENDDATE=@[DETAILTASK_ENDDATE] And "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_INDEX=@[DETAILTASK_INDEX] And "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_PERCENTCOMPLETE=@[DETAILTASK_PERCENTCOMPLETE] And "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_PRIORITY=@[DETAILTASK_PRIORITY] And "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_ESTIMATED=@[DETAILTASK_ESTIMATED] And "); 
		 UnSQL.SQL.Add(@"   IDPMDETAILTASK_PARENT=@[IDPMDETAILTASK_PARENT] And "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_STARTDATETEMP=@[DETAILTASK_STARTDATETEMP] And "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_ENDDATETEMP=@[DETAILTASK_ENDDATETEMP] And "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_DURATIONTEMP=@[DETAILTASK_DURATIONTEMP] ");
		 UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		var DS_PMDETAILTASK = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "PMDETAILTASK_GETID", Param.ToBytes());
		ResErr = DS_PMDETAILTASK.ResErr;
		if (ResErr.NotError)
		{
			if (DS_PMDETAILTASK.DataSet.RecordCount > 0)
			{
			    DS_PMDETAILTASK.DataSet.First();
			    PMDETAILTASK.IDPMDETAILTASK = DS_PMDETAILTASK.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMDETAILTASK.IDPMDETAILTASK.FieldName).asInt32();
			}
			else
			{
			    DS_PMDETAILTASK.ResErr.NotError = false;
			    DS_PMDETAILTASK.ResErr.Mesaje = "RECORDCOUNT = 0";
			}
		}
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.ProjectManagement.Methods.PMDETAILTASK_ADD = function(PMDETAILTASK)
{
	var ResErr = new SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try
	{
		Param.Inicialize();
		//Other
		Param.AddInt32(UsrCfg.InternoAtisNames.PMDETAILTASK.IDPMDETAILTASK.FieldName, PMDETAILTASK.IDPMDETAILTASK, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.PMDETAILTASK.IDPMMAINTASK.FieldName, PMDETAILTASK.IDPMMAINTASK, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_NAME.FieldName, PMDETAILTASK.DETAILTASK_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_DURATION.FieldName, PMDETAILTASK.DETAILTASK_DURATION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddDateTime(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_STARTDATE.FieldName, PMDETAILTASK.DETAILTASK_STARTDATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddDateTime(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_ENDDATE.FieldName, PMDETAILTASK.DETAILTASK_ENDDATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_INDEX.FieldName, PMDETAILTASK.DETAILTASK_INDEX, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_PERCENTCOMPLETE.FieldName, PMDETAILTASK.DETAILTASK_PERCENTCOMPLETE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_PRIORITY.FieldName, PMDETAILTASK.DETAILTASK_PRIORITY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddBoolean(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_ESTIMATED.FieldName, PMDETAILTASK.DETAILTASK_ESTIMATED, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddText(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_NOTE.FieldName, PMDETAILTASK.DETAILTASK_NOTE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.PMDETAILTASK.IDPMDETAILTASK_PARENT.FieldName, PMDETAILTASK.IDPMDETAILTASK_PARENT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddDateTime(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_STARTDATETEMP.FieldName, PMDETAILTASK.DETAILTASK_STARTDATETEMP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddDateTime(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_ENDDATETEMP.FieldName, PMDETAILTASK.DETAILTASK_ENDDATETEMP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_DURATIONTEMP.FieldName, PMDETAILTASK.DETAILTASK_DURATIONTEMP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		 
		/* 
		//********   PMDETAILTASK_ADD   ************************* 
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PMDETAILTASK_ADD", @"PMDETAILTASK_ADD description."); 
		UnSQL.SQL.Add(@"   INSERT INTO PMDETAILTASK( "); 
		 UnSQL.SQL.Add(@"   IDPMMAINTASK, "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_NAME, "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_DURATION, "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_STARTDATE, "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_ENDDATE, "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_INDEX, "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_PERCENTCOMPLETE, "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_PRIORITY, "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_ESTIMATED, "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_NOTE, "); 
		 UnSQL.SQL.Add(@"   IDPMDETAILTASK_PARENT, "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_STARTDATETEMP, "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_ENDDATETEMP, "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_DURATIONTEMP "); 
		 UnSQL.SQL.Add(@"   ) Values ( ");  
		UnSQL.SQL.Add(@"   @[IDPMMAINTASK], "); 
		 UnSQL.SQL.Add(@"   @[DETAILTASK_NAME], "); 
		 UnSQL.SQL.Add(@"   @[DETAILTASK_DURATION], "); 
		 UnSQL.SQL.Add(@"   @[DETAILTASK_STARTDATE], "); 
		 UnSQL.SQL.Add(@"   @[DETAILTASK_ENDDATE], "); 
		 UnSQL.SQL.Add(@"   @[DETAILTASK_INDEX], "); 
		 UnSQL.SQL.Add(@"   @[DETAILTASK_PERCENTCOMPLETE], "); 
		 UnSQL.SQL.Add(@"   @[DETAILTASK_PRIORITY], "); 
		 UnSQL.SQL.Add(@"   @[DETAILTASK_ESTIMATED], "); 
		 UnSQL.SQL.Add(@"   @[DETAILTASK_NOTE], "); 
		 UnSQL.SQL.Add(@"   @[IDPMDETAILTASK_PARENT], "); 
		 UnSQL.SQL.Add(@"   @[DETAILTASK_STARTDATETEMP], "); 
		 UnSQL.SQL.Add(@"   @[DETAILTASK_ENDDATETEMP], "); 
		 UnSQL.SQL.Add(@"   @[DETAILTASK_DURATIONTEMP] "); 
		 UnSQL.SQL.Add(@"   )  "); 
		UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PMDETAILTASK_ADD", Param.ToBytes());
		ResErr = Exec.ResErr;
		if (ResErr.NotError)
		{
			Persistence.ProjectManagement.Methods.PMDETAILTASK_GETID(PMDETAILTASK);
		}
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.ProjectManagement.Methods.PMDETAILTASK_UPD = function(PMDETAILTASK)
{
	var ResErr = SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try
	{
		Param.Inicialize();
		Param.AddInt32(UsrCfg.InternoAtisNames.PMDETAILTASK.IDPMDETAILTASK.FieldName, PMDETAILTASK.IDPMDETAILTASK, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.PMDETAILTASK.IDPMMAINTASK.FieldName, PMDETAILTASK.IDPMMAINTASK, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_NAME.FieldName, PMDETAILTASK.DETAILTASK_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_DURATION.FieldName, PMDETAILTASK.DETAILTASK_DURATION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddDateTime(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_STARTDATE.FieldName, PMDETAILTASK.DETAILTASK_STARTDATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddDateTime(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_ENDDATE.FieldName, PMDETAILTASK.DETAILTASK_ENDDATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_INDEX.FieldName, PMDETAILTASK.DETAILTASK_INDEX, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_PERCENTCOMPLETE.FieldName, PMDETAILTASK.DETAILTASK_PERCENTCOMPLETE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_PRIORITY.FieldName, PMDETAILTASK.DETAILTASK_PRIORITY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddBoolean(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_ESTIMATED.FieldName, PMDETAILTASK.DETAILTASK_ESTIMATED, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddText(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_NOTE.FieldName, PMDETAILTASK.DETAILTASK_NOTE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.PMDETAILTASK.IDPMDETAILTASK_PARENT.FieldName, PMDETAILTASK.IDPMDETAILTASK_PARENT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddDateTime(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_STARTDATETEMP.FieldName, PMDETAILTASK.DETAILTASK_STARTDATETEMP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddDateTime(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_ENDDATETEMP.FieldName, PMDETAILTASK.DETAILTASK_ENDDATETEMP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddString(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_DURATIONTEMP.FieldName, PMDETAILTASK.DETAILTASK_DURATIONTEMP, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		 

		/*   
		//********   PMDETAILTASK_UPD   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PMDETAILTASK_UPD", @"PMDETAILTASK_UPD description.");  
		UnSQL.SQL.Add(@"  UPDATE PMDETAILTASK Set "); 
		 UnSQL.SQL.Add(@"   IDPMMAINTASK=@[IDPMMAINTASK], "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_NAME=@[DETAILTASK_NAME], "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_DURATION=@[DETAILTASK_DURATION], "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_STARTDATE=@[DETAILTASK_STARTDATE], "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_ENDDATE=@[DETAILTASK_ENDDATE], "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_INDEX=@[DETAILTASK_INDEX], "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_PERCENTCOMPLETE=@[DETAILTASK_PERCENTCOMPLETE], "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_PRIORITY=@[DETAILTASK_PRIORITY], "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_ESTIMATED=@[DETAILTASK_ESTIMATED], "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_NOTE=@[DETAILTASK_NOTE], "); 
		 UnSQL.SQL.Add(@"   IDPMDETAILTASK_PARENT=@[IDPMDETAILTASK_PARENT], "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_STARTDATETEMP=@[DETAILTASK_STARTDATETEMP], "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_ENDDATETEMP=@[DETAILTASK_ENDDATETEMP], "); 
		 UnSQL.SQL.Add(@"   DETAILTASK_DURATIONTEMP=@[DETAILTASK_DURATIONTEMP] "); 
		 UnSQL.SQL.Add(@"   Where "); 
		 UnSQL.SQL.Add(@"   IDPMDETAILTASK=@[IDPMDETAILTASK] ");
		 UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PMDETAILTASK_UPD", Param.ToBytes());
		ResErr = Exec.ResErr;
		if (ResErr.NotError)
		{
		//GET(); 
		}
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.ProjectManagement.Methods.PMDETAILTASK_DEL = function(/*String StrIDPMDETAILTASK*/IDPMDETAILTASK)
{
	var ResErr = SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try
	{
		Param.Inicialize();
		/*if (StrIDPMDETAILTASK == "")
		{
		Param.AddUnknown(UsrCfg.InternoAtisNames.PMDETAILTASK.IDPMDETAILTASK.FieldName, " = " + UsrCfg.InternoAtisNames.PMDETAILTASK.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PMDETAILTASK.IDPMDETAILTASK.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
		}
		else
		{
		StrIDPMDETAILTASK = " IN (" + StrIDPMDETAILTASK + ")";
		Param.AddUnknown(UsrCfg.InternoAtisNames.PMDETAILTASK.IDPMDETAILTASK.FieldName, StrIDPMDETAILTASK, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		}*/
		Param.AddInt32(UsrCfg.InternoAtisNames.PMDETAILTASK.IDPMDETAILTASK.FieldName, IDPMDETAILTASK, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		/*
		//********   PMDETAILTASK_DEL   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PMDETAILTASK_DEL", @"PMDETAILTASK_DEL description.");  
		UnSQL.SQL.Add(@"   DELETE PMDETAILTASK "); 
		 UnSQL.SQL.Add(@"   Where "); 
		 UnSQL.SQL.Add(@"   IDPMDETAILTASK=@[IDPMDETAILTASK]   ");
		 UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PMDETAILTASK_DEL", Param.ToBytes());
		ResErr = Exec.ResErr;
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}


//**********************   METHODS   ********************************************************************************************
Persistence.ProjectManagement.Methods.PMDETAILTASK_ListSetID = function(PMDETAILTASKList,IDPMDETAILTASK)
{
	for (i = 0; i < PMDETAILTASKList.length; i++)
	{
		if (IDPMDETAILTASK == PMDETAILTASKList[i].IDPMDETAILTASK)
			return (PMDETAILTASKList[i]);
	}
	var UnPMDETAILTASK = new Persistence.Properties.TPMDETAILTASK;
	UnPMDETAILTASK.IDPMDETAILTASK = IDPMDETAILTASK; 
	return (UnPMDETAILTASK);
}
Persistence.ProjectManagement.Methods.PMDETAILTASK_ListAdd = function(PMDETAILTASKList,PMDETAILTASK)
{
	var i = Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex(PMDETAILTASKList, PMDETAILTASK);
	if (i == -1) PMDETAILTASKList.push(PMDETAILTASK);
	else
	{
		PMDETAILTASKList[i].IDPMDETAILTASK = PMDETAILTASK.IDPMDETAILTASK;
		PMDETAILTASKList[i].IDPMMAINTASK = PMDETAILTASK.IDPMMAINTASK;
		PMDETAILTASKList[i].DETAILTASK_NAME = PMDETAILTASK.DETAILTASK_NAME;
		PMDETAILTASKList[i].DETAILTASK_DURATION = PMDETAILTASK.DETAILTASK_DURATION;
		PMDETAILTASKList[i].DETAILTASK_STARTDATE = PMDETAILTASK.DETAILTASK_STARTDATE;
		PMDETAILTASKList[i].DETAILTASK_ENDDATE = PMDETAILTASK.DETAILTASK_ENDDATE;
		PMDETAILTASKList[i].DETAILTASK_INDEX = PMDETAILTASK.DETAILTASK_INDEX;
		PMDETAILTASKList[i].DETAILTASK_PERCENTCOMPLETE = PMDETAILTASK.DETAILTASK_PERCENTCOMPLETE;
		PMDETAILTASKList[i].DETAILTASK_PRIORITY = PMDETAILTASK.DETAILTASK_PRIORITY;
		PMDETAILTASKList[i].DETAILTASK_ESTIMATED = PMDETAILTASK.DETAILTASK_ESTIMATED;
		PMDETAILTASKList[i].DETAILTASK_NOTE = PMDETAILTASK.DETAILTASK_NOTE;
		PMDETAILTASKList[i].IDPMDETAILTASK_PARENT = PMDETAILTASK.IDPMDETAILTASK_PARENT;
		PMDETAILTASKList[i].DETAILTASK_STARTDATETEMP = PMDETAILTASK.DETAILTASK_STARTDATETEMP;
		PMDETAILTASKList[i].DETAILTASK_ENDDATETEMP = PMDETAILTASK.DETAILTASK_ENDDATETEMP;
		PMDETAILTASKList[i].DETAILTASK_DURATIONTEMP = PMDETAILTASK.DETAILTASK_DURATIONTEMP;

	}
}
Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex = function(PMDETAILTASKList, PMDETAILTASK)
{
	for (i = 0; i < PMDETAILTASKList.length; i++)
	{
		if (PMDETAILTASK.IDPMDETAILTASK == PMDETAILTASKList[i].IDPMDETAILTASK)
			return (i);
	}
	return (-1);
}
Persistence.ProjectManagement.Methods.PMDETAILTASK_ListGetIndex = function(PMDETAILTASKList, IDPMDETAILTASK)
{
	for (i = 0; i < PMDETAILTASKList.length; i++)
	{
		if (IDPMDETAILTASK == PMDETAILTASKList[i].IDPMDETAILTASK)
			return (i);
	}
	return (-1);
}

//String Function 
Persistence.ProjectManagement.Methods.PMDETAILTASKListtoStr = function(PMDETAILTASKList)
{
	var Res = Persistence.Properties._Version + "(1)0";
	var Longitud = new SysCfg.ref("");
	var Texto = new SysCfg.ref("");
	try
	{
		SysCfg.Str.Protocol.WriteInt(PMDETAILTASKList.length,Longitud,Texto);
		for (Counter = 0; Counter < PMDETAILTASKList.length; Counter++)
		{
			var UnPMDETAILTASK = PMDETAILTASKList[Counter];
			UnPMDETAILTASK.ToStr(Longitud,Texto);
		}

		Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
		Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
	}
	catch (e)
	{
		var ResErr = new SysCfg.Error.Properties.TResErr();
		ResErr.NotError = false;
		ResErr.Mesaje = "LIFESTATUStoStr:" + e;
	}
	return Res;
}
Persistence.ProjectManagement.Methods.StrtoPMDETAILTASK = function(ProtocoloStr, PMDETAILTASKList)
{

	var Res = false;
	var Longitud, Texto;
	var Index =  new SysCfg.ref(0);
	var Pos =  new SysCfg.ref(0);
	try
	{
		PMDETAILTASKList.length = 0;   
		if (Persistence.ProjectManagement.Properties._Version == ProtocoloStr.substring(0, 3))
		{
			Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")") - ProtocoloStr.indexOf("(") - 1);
			Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.Length - ProtocoloStr.indexOf(")") - 1);
			LongitudArray = Longitud.split(",");
			LSCount = SysCfg.Str.Protocol.ReadInt(Pos,Index,LongitudArray,Texto);
			for (i = 0; i < LSCount; i++)
			{
			    UnPMDETAILTASK = new Persistence.Properties.TPMDETAILTASK(); //Mode new row
			    UnPMDETAILTASK.StrTo(Pos,Index, LongitudArray, Texto);
			    Persistence.ProjectManagement.PMDETAILTASK_ListAdd(PMDETAILTASKList, UnPMDETAILTASK);
			}
		}
	}
	catch (e)
	{
		var ResErr = SysCfg.Error.Properties.TResErr;
		ResErr.NotError = false;
		ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
	}
	return (Res);
}


//Socket Function 
Persistence.ProjectManagement.Methods.PMDETAILTASKListToByte = function(PMDETAILTASKList, MemStream, idx)
{
	SysCfg.Stream.Methods.WriteStreamInt32(MemStream, PMDETAILTASKList.Count-idx);
	for (i = idx; i < PMDETAILTASKList.Count; i++)
	{
		PMDETAILTASKList[i].ToByte(MemStream);
	}
}
Persistence.ProjectManagement.Methods.ByteToPMDETAILTASKList = function (PMDETAILTASKList, MemStream)
{
	Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
	for (i = 0; i < Count; i++)
	{
		UnPMDETAILTASK = new Persistence.Properties.TPMDETAILTASK();
		UnPMDETAILTASK.ByteTo(MemStream);
		Methods.PMDETAILTASK_ListAdd(PMDETAILTASKList,UnPMDETAILTASK);
	}
}
