//**********************   PROPERTIES   *****************************************************************************************
Persistence.ProjectManagement.Properties.TPMRESOURCESALLOCATION = function() {
	this.IDPMRESOURCESALLOCATION = 0;
	this.IDPMDETAILTASK = 0;
	this.IDPMRESOURCES = 0;
	this.RESOURCESALLOCATION_UNITS = 0;
	this.RESOURCESALLOCATION_DESCRIPTION = "";

	// Virtuales
	this.PMDETAILTASK = new Persistence.ProjectManagement.Properties.TPMDETAILTASK();
	this.PMRESOURCES = new Persistence.ProjectManagement.Properties.TPMRESOURCES();

	//Socket IO Properties
	this.ToByte = function(MemStream){
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDPMRESOURCESALLOCATION); 
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDPMDETAILTASK); 
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDPMRESOURCES); 
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.RESOURCESALLOCATION_UNITS); 
		SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.RESOURCESALLOCATION_DESCRIPTION); 

	}
	this.ByteTo = function(MemStream)
	{
		this.IDPMRESOURCESALLOCATION = SysCfg.Stream.Methods.ReadStreamInt32(MemStream); 
		this.IDPMDETAILTASK = SysCfg.Stream.Methods.ReadStreamInt32(MemStream); 
		this.IDPMRESOURCES = SysCfg.Stream.Methods.ReadStreamInt32(MemStream); 
		this.RESOURCESALLOCATION_UNITS = SysCfg.Stream.Methods.ReadStreamInt32(MemStream); 
		this.RESOURCESALLOCATION_DESCRIPTION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream); 

	}
	//Str IO Properties
	this.ToStr = function(Longitud,Texto)
	{
		SysCfg.Str.Protocol.WriteInt(this.IDPMRESOURCESALLOCATION, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteInt(this.IDPMDETAILTASK, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteInt(this.IDPMRESOURCES, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteInt(this.RESOURCESALLOCATION_UNITS, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteStr(this.RESOURCESALLOCATION_DESCRIPTION, Longitud, Texto); 

	}
	this.StrTo = function(Pos,Index,LongitudArray,Texto)
	{
		this.IDPMRESOURCESALLOCATION = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto); 
		this.IDPMDETAILTASK = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto); 
		this.IDPMRESOURCES = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto); 
		this.RESOURCESALLOCATION_UNITS = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto); 
		this.RESOURCESALLOCATION_DESCRIPTION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto); 

	}
}

//**********************   METHODS server  ********************************************************************************************
Persistence.ProjectManagement.Methods.PMRESOURCESALLOCATION_Fill = function(PMRESOURCESALLOCATIONList,StrIDPMRESOURCESALLOCATION/*noin IDPMRESOURCESALLOCATION*/)
{
	var ResErr = new SysCfg.Error.Properties.TResErr();
	PMRESOURCESALLOCATIONList.length = 0;
	var Param = new SysCfg.Stream.Properties.TParam();
	Param.Inicialize();
	if (StrIDPMRESOURCESALLOCATION == "")
	{
		Param.AddUnknown(UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.IDPMRESOURCESALLOCATION.FieldName, " = " + UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.IDPMRESOURCESALLOCATION.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
	}
	else
	{
		StrIDPMRESOURCESALLOCATION = " IN (" + StrIDPMRESOURCESALLOCATION + ")";
		Param.AddUnknown(UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.IDPMRESOURCESALLOCATION.FieldName, StrIDPMRESOURCESALLOCATION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
	}
	//noin Param.AddInt32(UsrCfg.InternoDemoNames.PMRESOURCESALLOCATION.IDPMRESOURCESALLOCATION.FieldName, IDPMRESOURCESALLOCATION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
	try
	{
		/*
		//********   PMRESOURCESALLOCATION_GET   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PMRESOURCESALLOCATION_GET", @"PMRESOURCESALLOCATION_GET description.");  
		UnSQL.SQL.Add(@"   SELECT "); 
		 UnSQL.SQL.Add(@"   IDPMRESOURCESALLOCATION, "); 
		 UnSQL.SQL.Add(@"   IDPMDETAILTASK, "); 
		 UnSQL.SQL.Add(@"   IDPMRESOURCES, "); 
		 UnSQL.SQL.Add(@"   RESOURCESALLOCATION_UNITS, "); 
		 UnSQL.SQL.Add(@"   RESOURCESALLOCATION_DESCRIPTION "); 
		 UnSQL.SQL.Add(@"   FROM PMRESOURCESALLOCATION "); 
		 UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		var DS_PMRESOURCESALLOCATION = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "PMRESOURCESALLOCATION_GET", Param.ToBytes());
		ResErr = DS_PMRESOURCESALLOCATION.ResErr;
		if (ResErr.NotError)
		{
			if (DS_PMRESOURCESALLOCATION.DataSet.RecordCount > 0)
			{
				DS_PMRESOURCESALLOCATION.DataSet.First();
				while (!(DS_PMRESOURCESALLOCATION.DataSet.Eof))
				{
					var PMRESOURCESALLOCATION = new Persistence.ProjectManagement.Properties.TPMRESOURCESALLOCATION();
					PMRESOURCESALLOCATION.IDPMRESOURCESALLOCATION = DS_PMRESOURCESALLOCATION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.IDPMRESOURCESALLOCATION.FieldName).asInt32();
					PMRESOURCESALLOCATION.IDPMDETAILTASK = DS_PMRESOURCESALLOCATION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.IDPMDETAILTASK.FieldName).asInt32();
					PMRESOURCESALLOCATION.IDPMRESOURCES = DS_PMRESOURCESALLOCATION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.IDPMRESOURCES.FieldName).asInt32();
					PMRESOURCESALLOCATION.RESOURCESALLOCATION_UNITS = DS_PMRESOURCESALLOCATION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.RESOURCESALLOCATION_UNITS.FieldName).asInt32();
					PMRESOURCESALLOCATION.RESOURCESALLOCATION_DESCRIPTION = DS_PMRESOURCESALLOCATION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.RESOURCESALLOCATION_DESCRIPTION.FieldName).asText();
					 
					//Other
					PMRESOURCESALLOCATIONList.push(PMRESOURCESALLOCATION);
					DS_PMRESOURCESALLOCATION.DataSet.Next();
				}
			}
			else
			{
				DS_PMRESOURCESALLOCATION.ResErr.NotError = false;
				DS_PMRESOURCESALLOCATION.ResErr.Mesaje = "RECORDCOUNT = 0";
			}
		}
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.ProjectManagement.Methods.PMRESOURCESALLOCATION_GETID = function(PMRESOURCESALLOCATION)
{
	var ResErr = new SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try
	{
		Param.Inicialize();
		//Other
		Param.AddInt32(UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.IDPMRESOURCESALLOCATION.FieldName, PMRESOURCESALLOCATION.IDPMRESOURCESALLOCATION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddInt32(UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.IDPMDETAILTASK.FieldName, PMRESOURCESALLOCATION.IDPMDETAILTASK, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddInt32(UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.IDPMRESOURCES.FieldName, PMRESOURCESALLOCATION.IDPMRESOURCES, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddInt32(UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.RESOURCESALLOCATION_UNITS.FieldName, PMRESOURCESALLOCATION.RESOURCESALLOCATION_UNITS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddText(UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.RESOURCESALLOCATION_DESCRIPTION.FieldName, PMRESOURCESALLOCATION.RESOURCESALLOCATION_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

		/*
		//********   PMRESOURCESALLOCATION_GETID   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PMRESOURCESALLOCATION_GETID", @"PMRESOURCESALLOCATION_GETID description.");  
		UnSQL.SQL.Add(@"   SELECT "); 
		 UnSQL.SQL.Add(@"   IDPMRESOURCESALLOCATION "); 
		 UnSQL.SQL.Add(@"   FROM PMRESOURCESALLOCATION "); 
		 UnSQL.SQL.Add(@"   Where "); 
		 UnSQL.SQL.Add(@"   IDPMDETAILTASK=@[IDPMDETAILTASK] And "); 
		 UnSQL.SQL.Add(@"   IDPMRESOURCES=@[IDPMRESOURCES] And "); 
		 UnSQL.SQL.Add(@"   RESOURCESALLOCATION_UNITS=@[RESOURCESALLOCATION_UNITS] ");
		 UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		var DS_PMRESOURCESALLOCATION = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "PMRESOURCESALLOCATION_GETID", Param.ToBytes());
		ResErr = DS_PMRESOURCESALLOCATION.ResErr;
		if (ResErr.NotError)
		{
			if (DS_PMRESOURCESALLOCATION.DataSet.RecordCount > 0)
			{
			    DS_PMRESOURCESALLOCATION.DataSet.First();
			    PMRESOURCESALLOCATION.IDPMRESOURCESALLOCATION = DS_PMRESOURCESALLOCATION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.IDPMRESOURCESALLOCATION.FieldName).asInt32();
			}
			else
			{
			    DS_PMRESOURCESALLOCATION.ResErr.NotError = false;
			    DS_PMRESOURCESALLOCATION.ResErr.Mesaje = "RECORDCOUNT = 0";
			}
		}
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.ProjectManagement.Methods.PMRESOURCESALLOCATION_ADD = function(PMRESOURCESALLOCATION)
{
	var ResErr = new SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try
	{
		Param.Inicialize();
		//Other
		Param.AddInt32(UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.IDPMRESOURCESALLOCATION.FieldName, PMRESOURCESALLOCATION.IDPMRESOURCESALLOCATION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.IDPMDETAILTASK.FieldName, PMRESOURCESALLOCATION.IDPMDETAILTASK, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.IDPMRESOURCES.FieldName, PMRESOURCESALLOCATION.IDPMRESOURCES, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.RESOURCESALLOCATION_UNITS.FieldName, PMRESOURCESALLOCATION.RESOURCESALLOCATION_UNITS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddText(UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.RESOURCESALLOCATION_DESCRIPTION.FieldName, PMRESOURCESALLOCATION.RESOURCESALLOCATION_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		 
		/* 
		//********   PMRESOURCESALLOCATION_ADD   ************************* 
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PMRESOURCESALLOCATION_ADD", @"PMRESOURCESALLOCATION_ADD description."); 
		UnSQL.SQL.Add(@"   INSERT INTO PMRESOURCESALLOCATION( "); 
		 UnSQL.SQL.Add(@"   IDPMDETAILTASK, "); 
		 UnSQL.SQL.Add(@"   IDPMRESOURCES, "); 
		 UnSQL.SQL.Add(@"   RESOURCESALLOCATION_UNITS, "); 
		 UnSQL.SQL.Add(@"   RESOURCESALLOCATION_DESCRIPTION "); 
		 UnSQL.SQL.Add(@"   ) Values ( ");  
		UnSQL.SQL.Add(@"   @[IDPMDETAILTASK], "); 
		 UnSQL.SQL.Add(@"   @[IDPMRESOURCES], "); 
		 UnSQL.SQL.Add(@"   @[RESOURCESALLOCATION_UNITS], "); 
		 UnSQL.SQL.Add(@"   @[RESOURCESALLOCATION_DESCRIPTION] "); 
		 UnSQL.SQL.Add(@"   )  "); 
		UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PMRESOURCESALLOCATION_ADD", Param.ToBytes());
		ResErr = Exec.ResErr;
		if (ResErr.NotError)
		{
			Persistence.ProjectManagement.Methods.PMRESOURCESALLOCATION_GETID(PMRESOURCESALLOCATION);
		}
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.ProjectManagement.Methods.PMRESOURCESALLOCATION_UPD = function(PMRESOURCESALLOCATION)
{
	var ResErr = SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try
	{
		Param.Inicialize();
		Param.AddInt32(UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.IDPMRESOURCESALLOCATION.FieldName, PMRESOURCESALLOCATION.IDPMRESOURCESALLOCATION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.IDPMDETAILTASK.FieldName, PMRESOURCESALLOCATION.IDPMDETAILTASK, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.IDPMRESOURCES.FieldName, PMRESOURCESALLOCATION.IDPMRESOURCES, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.RESOURCESALLOCATION_UNITS.FieldName, PMRESOURCESALLOCATION.RESOURCESALLOCATION_UNITS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddText(UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.RESOURCESALLOCATION_DESCRIPTION.FieldName, PMRESOURCESALLOCATION.RESOURCESALLOCATION_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		 

		/*   
		//********   PMRESOURCESALLOCATION_UPD   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PMRESOURCESALLOCATION_UPD", @"PMRESOURCESALLOCATION_UPD description.");  
		UnSQL.SQL.Add(@"  UPDATE PMRESOURCESALLOCATION Set "); 
		 UnSQL.SQL.Add(@"   IDPMDETAILTASK=@[IDPMDETAILTASK], "); 
		 UnSQL.SQL.Add(@"   IDPMRESOURCES=@[IDPMRESOURCES], "); 
		 UnSQL.SQL.Add(@"   RESOURCESALLOCATION_UNITS=@[RESOURCESALLOCATION_UNITS], "); 
		 UnSQL.SQL.Add(@"   RESOURCESALLOCATION_DESCRIPTION=@[RESOURCESALLOCATION_DESCRIPTION] "); 
		 UnSQL.SQL.Add(@"   Where "); 
		 UnSQL.SQL.Add(@"   IDPMRESOURCESALLOCATION=@[IDPMRESOURCESALLOCATION] ");
		 UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PMRESOURCESALLOCATION_UPD", Param.ToBytes());
		ResErr = Exec.ResErr;
		if (ResErr.NotError)
		{
		//GET(); 
		}
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.ProjectManagement.Methods.PMRESOURCESALLOCATION_DEL = function(/*String StrIDPMRESOURCESALLOCATION*/IDPMRESOURCESALLOCATION)
{
	var ResErr = SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try
	{
		Param.Inicialize();
		/*if (StrIDPMRESOURCESALLOCATION == "")
		{
		Param.AddUnknown(UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.IDPMRESOURCESALLOCATION.FieldName, " = " + UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.IDPMRESOURCESALLOCATION.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
		}
		else
		{
		StrIDPMRESOURCESALLOCATION = " IN (" + StrIDPMRESOURCESALLOCATION + ")";
		Param.AddUnknown(UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.IDPMRESOURCESALLOCATION.FieldName, StrIDPMRESOURCESALLOCATION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		}*/
		Param.AddInt32(UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.IDPMRESOURCESALLOCATION.FieldName, IDPMRESOURCESALLOCATION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		/*
		//********   PMRESOURCESALLOCATION_DEL   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PMRESOURCESALLOCATION_DEL", @"PMRESOURCESALLOCATION_DEL description.");  
		UnSQL.SQL.Add(@"   DELETE PMRESOURCESALLOCATION "); 
		 UnSQL.SQL.Add(@"   Where "); 
		 UnSQL.SQL.Add(@"   IDPMRESOURCESALLOCATION=@[IDPMRESOURCESALLOCATION]   ");
		 UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PMRESOURCESALLOCATION_DEL", Param.ToBytes());
		ResErr = Exec.ResErr;
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}


//**********************   METHODS   ********************************************************************************************
Persistence.ProjectManagement.Methods.PMRESOURCESALLOCATION_ListSetID = function(PMRESOURCESALLOCATIONList,IDPMRESOURCESALLOCATION)
{
	for (i = 0; i < PMRESOURCESALLOCATIONList.length; i++)
	{
		if (IDPMRESOURCESALLOCATION == PMRESOURCESALLOCATIONList[i].IDPMRESOURCESALLOCATION)
			return (PMRESOURCESALLOCATIONList[i]);
	}
	var UnPMRESOURCESALLOCATION = new Persistence.Properties.TPMRESOURCESALLOCATION;
	UnPMRESOURCESALLOCATION.IDPMRESOURCESALLOCATION = IDPMRESOURCESALLOCATION; 
	return (UnPMRESOURCESALLOCATION);
}
Persistence.ProjectManagement.Methods.PMRESOURCESALLOCATION_ListAdd = function(PMRESOURCESALLOCATIONList,PMRESOURCESALLOCATION)
{
	var i = Persistence.ProjectManagement.Methods.PMRESOURCESALLOCATION_ListGetIndex(PMRESOURCESALLOCATIONList, PMRESOURCESALLOCATION);
	if (i == -1) PMRESOURCESALLOCATIONList.push(PMRESOURCESALLOCATION);
	else
	{
		PMRESOURCESALLOCATIONList[i].IDPMRESOURCESALLOCATION = PMRESOURCESALLOCATION.IDPMRESOURCESALLOCATION;
		PMRESOURCESALLOCATIONList[i].IDPMDETAILTASK = PMRESOURCESALLOCATION.IDPMDETAILTASK;
		PMRESOURCESALLOCATIONList[i].IDPMRESOURCES = PMRESOURCESALLOCATION.IDPMRESOURCES;
		PMRESOURCESALLOCATIONList[i].RESOURCESALLOCATION_UNITS = PMRESOURCESALLOCATION.RESOURCESALLOCATION_UNITS;
		PMRESOURCESALLOCATIONList[i].RESOURCESALLOCATION_DESCRIPTION = PMRESOURCESALLOCATION.RESOURCESALLOCATION_DESCRIPTION;

	}
}
Persistence.ProjectManagement.Methods.PMRESOURCESALLOCATION_ListGetIndex = function(PMRESOURCESALLOCATIONList, PMRESOURCESALLOCATION)
{
	for (i = 0; i < PMRESOURCESALLOCATIONList.length; i++)
	{
		if (PMRESOURCESALLOCATION.IDPMRESOURCESALLOCATION == PMRESOURCESALLOCATIONList[i].IDPMRESOURCESALLOCATION)
			return (i);
	}
	return (-1);
}
Persistence.ProjectManagement.Methods.PMRESOURCESALLOCATION_ListGetIndex = function(PMRESOURCESALLOCATIONList, IDPMRESOURCESALLOCATION)
{
	for (i = 0; i < PMRESOURCESALLOCATIONList.length; i++)
	{
		if (IDPMRESOURCESALLOCATION == PMRESOURCESALLOCATIONList[i].IDPMRESOURCESALLOCATION)
			return (i);
	}
	return (-1);
}

//String Function 
Persistence.ProjectManagement.Methods.PMRESOURCESALLOCATIONListtoStr = function(PMRESOURCESALLOCATIONList)
{
	var Res = Persistence.Properties._Version + "(1)0";
	var Longitud = new SysCfg.ref("");
	var Texto = new SysCfg.ref("");
	try
	{
		SysCfg.Str.Protocol.WriteInt(PMRESOURCESALLOCATIONList.length,Longitud,Texto);
		for (Counter = 0; Counter < PMRESOURCESALLOCATIONList.length; Counter++)
		{
			var UnPMRESOURCESALLOCATION = PMRESOURCESALLOCATIONList[Counter];
			UnPMRESOURCESALLOCATION.ToStr(Longitud,Texto);
		}

		Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
		Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
	}
	catch (e)
	{
		var ResErr = new SysCfg.Error.Properties.TResErr();
		ResErr.NotError = false;
		ResErr.Mesaje = "LIFESTATUStoStr:" + e;
	}
	return Res;
}
Persistence.ProjectManagement.Methods.StrtoPMRESOURCESALLOCATION = function(ProtocoloStr, PMRESOURCESALLOCATIONList)
{
	var Res = false;
	var Longitud, Texto;
	var Index =  new SysCfg.ref(0);
	var Pos =  new SysCfg.ref(0);
	try
	{
		PMRESOURCESALLOCATIONList.length = 0;   
		if (Persistence.ProjectManagement.Properties._Version == ProtocoloStr.substring(0, 3))
		{
			Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")") - ProtocoloStr.indexOf("(") - 1);
			Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.Length - ProtocoloStr.indexOf(")") - 1);
			LongitudArray = Longitud.split(",");
			LSCount = SysCfg.Str.Protocol.ReadInt(Pos,Index,LongitudArray,Texto);
			for (i = 0; i < LSCount; i++)
			{
			    UnPMRESOURCESALLOCATION = new Persistence.Properties.TPMRESOURCESALLOCATION(); //Mode new row
			    UnPMRESOURCESALLOCATION.StrTo(Pos,Index, LongitudArray, Texto);
			    Persistence.ProjectManagement.PMRESOURCESALLOCATION_ListAdd(PMRESOURCESALLOCATIONList, UnPMRESOURCESALLOCATION);
			}
		}
	}
	catch (e)
	{
		var ResErr = SysCfg.Error.Properties.TResErr;
		ResErr.NotError = false;
		ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
	}
	return (Res);
}


//Socket Function 
Persistence.ProjectManagement.Methods.PMRESOURCESALLOCATIONListToByte = function(PMRESOURCESALLOCATIONList, MemStream, idx)
{
	SysCfg.Stream.Methods.WriteStreamInt32(MemStream, PMRESOURCESALLOCATIONList.Count-idx);
	for (i = idx; i < PMRESOURCESALLOCATIONList.Count; i++)
	{
		PMRESOURCESALLOCATIONList[i].ToByte(MemStream);
	}
}
Persistence.ProjectManagement.Methods.ByteToPMRESOURCESALLOCATIONList = function (PMRESOURCESALLOCATIONList, MemStream)
{
	Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
	for (i = 0; i < Count; i++)
	{
		UnPMRESOURCESALLOCATION = new Persistence.Properties.TPMRESOURCESALLOCATION();
		UnPMRESOURCESALLOCATION.ByteTo(MemStream);
		Methods.PMRESOURCESALLOCATION_ListAdd(PMRESOURCESALLOCATIONList,UnPMRESOURCESALLOCATION);
	}
}
