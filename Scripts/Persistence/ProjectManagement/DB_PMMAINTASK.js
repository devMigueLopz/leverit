//**********************   PROPERTIES   *****************************************************************************************
Persistence.ProjectManagement.Properties.TPMMAINTASK = function () {
    this.IDPMMAINTASK = 0;
    this.MAINTASK_NAME = "";
    this.MAINTASK_OWNER = "";
    this.MAINTASK_DURATION = "";
    this.MAINTASK_STARTDATE = new Date(1970, 0, 1, 0, 0, 0);
    this.MAINTASK_ENDDATE = new Date(1970, 0, 1, 0, 0, 0);
    this.MAINTASK_STATE = false;
    this.MAINTASK_WEEKENDHOLIDAY = false;
    this.MAINTASK_SUNDAYHOLIDAY = false;
    this.MAINTASK_SCALEUNITS = "";
    this.MAINTASK_SCALECOUNTER = 0;

    // Virtuales
    this.PMDETAILTASKList = new Array();
    this.PMDETAILTASKPARENTList = new Array();
    this.PMRESOURCESList = new Array();
    this.PMHOLIDAYList = new Array();

    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDPMMAINTASK);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.MAINTASK_NAME);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.MAINTASK_OWNER);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.MAINTASK_DURATION);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.MAINTASK_STARTDATE);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.MAINTASK_ENDDATE);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.MAINTASK_STATE);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.MAINTASK_WEEKENDHOLIDAY);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.MAINTASK_SUNDAYHOLIDAY);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.MAINTASK_SCALEUNITS);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.MAINTASK_SCALECOUNTER);

    }
    this.ByteTo = function (MemStream) {
        this.IDPMMAINTASK = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.MAINTASK_NAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.MAINTASK_OWNER = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.MAINTASK_DURATION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.MAINTASK_STARTDATE = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.MAINTASK_ENDDATE = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.MAINTASK_STATE = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.MAINTASK_WEEKENDHOLIDAY = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.MAINTASK_SUNDAYHOLIDAY = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.MAINTASK_SCALEUNITS = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.MAINTASK_SCALECOUNTER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);

    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDPMMAINTASK, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.MAINTASK_NAME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.MAINTASK_OWNER, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.MAINTASK_DURATION, Longitud, Texto);
        SysCfg.Str.Protocol.WriteDateTime(this.MAINTASK_STARTDATE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteDateTime(this.MAINTASK_ENDDATE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteBool(this.MAINTASK_STATE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteBool(this.MAINTASK_WEEKENDHOLIDAY, Longitud, Texto);
        SysCfg.Str.Protocol.WriteBool(this.MAINTASK_SUNDAYHOLIDAY, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.MAINTASK_SCALEUNITS, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.MAINTASK_SCALECOUNTER, Longitud, Texto);

    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDPMMAINTASK = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.MAINTASK_NAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.MAINTASK_OWNER = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.MAINTASK_DURATION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.MAINTASK_STARTDATE = SysCfg.Str.Protocol.ReadDateTime(Pos, Index, LongitudArray, Texto);
        this.MAINTASK_ENDDATE = SysCfg.Str.Protocol.ReadDateTime(Pos, Index, LongitudArray, Texto);
        this.MAINTASK_STATE = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);
        this.MAINTASK_WEEKENDHOLIDAY = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);
        this.MAINTASK_SUNDAYHOLIDAY = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);
        this.MAINTASK_SCALEUNITS = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.MAINTASK_SCALECOUNTER = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);

    }
}

//**********************   METHODS server  ********************************************************************************************
Persistence.ProjectManagement.Methods.PMMAINTASK_Fill = function (PMMAINTASKList, StrIDPMMAINTASK/*noin IDPMMAINTASK*/) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    PMMAINTASKList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    if (StrIDPMMAINTASK == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.PMMAINTASK.IDPMMAINTASK.FieldName, " = " + UsrCfg.InternoAtisNames.PMMAINTASK.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PMMAINTASK.IDPMMAINTASK.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDPMMAINTASK = " IN (" + StrIDPMMAINTASK + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.PMMAINTASK.IDPMMAINTASK.FieldName, StrIDPMMAINTASK, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //noin Param.AddInt32(UsrCfg.InternoDemoNames.PMMAINTASK.IDPMMAINTASK.FieldName, IDPMMAINTASK, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try {
        /*
        //********   PMMAINTASK_GET   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PMMAINTASK_GET", @"PMMAINTASK_GET description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   IDPMMAINTASK, "); 
         UnSQL.SQL.Add(@"   MAINTASK_NAME, "); 
         UnSQL.SQL.Add(@"   MAINTASK_OWNER, "); 
         UnSQL.SQL.Add(@"   MAINTASK_DURATION, "); 
         UnSQL.SQL.Add(@"   MAINTASK_STARTDATE, "); 
         UnSQL.SQL.Add(@"   MAINTASK_ENDDATE, "); 
         UnSQL.SQL.Add(@"   MAINTASK_STATE, "); 
         UnSQL.SQL.Add(@"   MAINTASK_WEEKENDHOLIDAY, "); 
         UnSQL.SQL.Add(@"   MAINTASK_SUNDAYHOLIDAY, "); 
         UnSQL.SQL.Add(@"   MAINTASK_SCALEUNITS, "); 
         UnSQL.SQL.Add(@"   MAINTASK_SCALECOUNTER "); 
         UnSQL.SQL.Add(@"   FROM PMMAINTASK "); 
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_PMMAINTASK = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "PMMAINTASK_GET", Param.ToBytes());
        ResErr = DS_PMMAINTASK.ResErr;
        if (ResErr.NotError) {
            if (DS_PMMAINTASK.DataSet.RecordCount > 0) {
                DS_PMMAINTASK.DataSet.First();
                while (!(DS_PMMAINTASK.DataSet.Eof)) {
                    var PMMAINTASK = new Persistence.ProjectManagement.Properties.TPMMAINTASK();
                    PMMAINTASK.IDPMMAINTASK = DS_PMMAINTASK.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMMAINTASK.IDPMMAINTASK.FieldName).asInt32();
                    PMMAINTASK.MAINTASK_NAME = DS_PMMAINTASK.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_NAME.FieldName).asString();
                    PMMAINTASK.MAINTASK_OWNER = DS_PMMAINTASK.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_OWNER.FieldName).asString();
                    PMMAINTASK.MAINTASK_DURATION = DS_PMMAINTASK.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_DURATION.FieldName).asString();
                    PMMAINTASK.MAINTASK_STARTDATE = DS_PMMAINTASK.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_STARTDATE.FieldName).asDateTime();
                    PMMAINTASK.MAINTASK_ENDDATE = DS_PMMAINTASK.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_ENDDATE.FieldName).asDateTime();
                    PMMAINTASK.MAINTASK_STATE = DS_PMMAINTASK.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_STATE.FieldName).asBoolean();
                    PMMAINTASK.MAINTASK_WEEKENDHOLIDAY = DS_PMMAINTASK.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_WEEKENDHOLIDAY.FieldName).asBoolean();
                    PMMAINTASK.MAINTASK_SUNDAYHOLIDAY = DS_PMMAINTASK.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_SUNDAYHOLIDAY.FieldName).asBoolean();
                    PMMAINTASK.MAINTASK_SCALEUNITS = DS_PMMAINTASK.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_SCALEUNITS.FieldName).asString();
                    PMMAINTASK.MAINTASK_SCALECOUNTER = DS_PMMAINTASK.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_SCALECOUNTER.FieldName).asInt32();

                    //Other
                    PMMAINTASKList.push(PMMAINTASK);
                    DS_PMMAINTASK.DataSet.Next();
                }
            }
            else {
                DS_PMMAINTASK.ResErr.NotError = false;
                DS_PMMAINTASK.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.ProjectManagement.Methods.PMMAINTASK_GETID = function (PMMAINTASK) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.PMMAINTASK.IDPMMAINTASK.FieldName, PMMAINTASK.IDPMMAINTASK, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_NAME.FieldName, PMMAINTASK.MAINTASK_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_OWNER.FieldName, PMMAINTASK.MAINTASK_OWNER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_DURATION.FieldName, PMMAINTASK.MAINTASK_DURATION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddDateTime(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_STARTDATE.FieldName, PMMAINTASK.MAINTASK_STARTDATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddDateTime(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_ENDDATE.FieldName, PMMAINTASK.MAINTASK_ENDDATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddBoolean(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_STATE.FieldName, PMMAINTASK.MAINTASK_STATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddBoolean(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_WEEKENDHOLIDAY.FieldName, PMMAINTASK.MAINTASK_WEEKENDHOLIDAY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddBoolean(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_SUNDAYHOLIDAY.FieldName, PMMAINTASK.MAINTASK_SUNDAYHOLIDAY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_SCALEUNITS.FieldName, PMMAINTASK.MAINTASK_SCALEUNITS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_SCALECOUNTER.FieldName, PMMAINTASK.MAINTASK_SCALECOUNTER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

        /*
        //********   PMMAINTASK_GETID   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PMMAINTASK_GETID", @"PMMAINTASK_GETID description.");  
        UnSQL.SQL.Add(@"   SELECT "); 
         UnSQL.SQL.Add(@"   IDPMMAINTASK "); 
         UnSQL.SQL.Add(@"   FROM PMMAINTASK "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   MAINTASK_NAME=@[MAINTASK_NAME] And "); 
         UnSQL.SQL.Add(@"   MAINTASK_OWNER=@[MAINTASK_OWNER] And "); 
         UnSQL.SQL.Add(@"   MAINTASK_DURATION=@[MAINTASK_DURATION] And "); 
         UnSQL.SQL.Add(@"   MAINTASK_STARTDATE=@[MAINTASK_STARTDATE] And "); 
         UnSQL.SQL.Add(@"   MAINTASK_ENDDATE=@[MAINTASK_ENDDATE] And "); 
         UnSQL.SQL.Add(@"   MAINTASK_STATE=@[MAINTASK_STATE] And "); 
         UnSQL.SQL.Add(@"   MAINTASK_WEEKENDHOLIDAY=@[MAINTASK_WEEKENDHOLIDAY] And "); 
         UnSQL.SQL.Add(@"   MAINTASK_SUNDAYHOLIDAY=@[MAINTASK_SUNDAYHOLIDAY] And "); 
         UnSQL.SQL.Add(@"   MAINTASK_SCALEUNITS=@[MAINTASK_SCALEUNITS] And "); 
         UnSQL.SQL.Add(@"   MAINTASK_SCALECOUNTER=@[MAINTASK_SCALECOUNTER] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var DS_PMMAINTASK = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "PMMAINTASK_GETID", Param.ToBytes());
        ResErr = DS_PMMAINTASK.ResErr;
        if (ResErr.NotError) {
            if (DS_PMMAINTASK.DataSet.RecordCount > 0) {
                DS_PMMAINTASK.DataSet.First();
                PMMAINTASK.IDPMMAINTASK = DS_PMMAINTASK.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMMAINTASK.IDPMMAINTASK.FieldName).asInt32();
            }
            else {
                DS_PMMAINTASK.ResErr.NotError = false;
                DS_PMMAINTASK.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.ProjectManagement.Methods.PMMAINTASK_ADD = function (PMMAINTASK) {
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //Other
        Param.AddInt32(UsrCfg.InternoAtisNames.PMMAINTASK.IDPMMAINTASK.FieldName, PMMAINTASK.IDPMMAINTASK, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_NAME.FieldName, PMMAINTASK.MAINTASK_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_OWNER.FieldName, PMMAINTASK.MAINTASK_OWNER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_DURATION.FieldName, PMMAINTASK.MAINTASK_DURATION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_STARTDATE.FieldName, PMMAINTASK.MAINTASK_STARTDATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_ENDDATE.FieldName, PMMAINTASK.MAINTASK_ENDDATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_STATE.FieldName, PMMAINTASK.MAINTASK_STATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_WEEKENDHOLIDAY.FieldName, PMMAINTASK.MAINTASK_WEEKENDHOLIDAY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_SUNDAYHOLIDAY.FieldName, PMMAINTASK.MAINTASK_SUNDAYHOLIDAY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_SCALEUNITS.FieldName, PMMAINTASK.MAINTASK_SCALEUNITS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_SCALECOUNTER.FieldName, PMMAINTASK.MAINTASK_SCALECOUNTER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        /* 
        //********   PMMAINTASK_ADD   ************************* 
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PMMAINTASK_ADD", @"PMMAINTASK_ADD description."); 
        UnSQL.SQL.Add(@"   INSERT INTO PMMAINTASK( "); 
         UnSQL.SQL.Add(@"   MAINTASK_NAME, "); 
         UnSQL.SQL.Add(@"   MAINTASK_OWNER, "); 
         UnSQL.SQL.Add(@"   MAINTASK_DURATION, "); 
         UnSQL.SQL.Add(@"   MAINTASK_STARTDATE, "); 
         UnSQL.SQL.Add(@"   MAINTASK_ENDDATE, "); 
         UnSQL.SQL.Add(@"   MAINTASK_STATE, "); 
         UnSQL.SQL.Add(@"   MAINTASK_WEEKENDHOLIDAY, "); 
         UnSQL.SQL.Add(@"   MAINTASK_SUNDAYHOLIDAY, "); 
         UnSQL.SQL.Add(@"   MAINTASK_SCALEUNITS, "); 
         UnSQL.SQL.Add(@"   MAINTASK_SCALECOUNTER "); 
         UnSQL.SQL.Add(@"   ) Values ( ");  
        UnSQL.SQL.Add(@"   @[MAINTASK_NAME], "); 
         UnSQL.SQL.Add(@"   @[MAINTASK_OWNER], "); 
         UnSQL.SQL.Add(@"   @[MAINTASK_DURATION], "); 
         UnSQL.SQL.Add(@"   @[MAINTASK_STARTDATE], "); 
         UnSQL.SQL.Add(@"   @[MAINTASK_ENDDATE], "); 
         UnSQL.SQL.Add(@"   @[MAINTASK_STATE], "); 
         UnSQL.SQL.Add(@"   @[MAINTASK_WEEKENDHOLIDAY], "); 
         UnSQL.SQL.Add(@"   @[MAINTASK_SUNDAYHOLIDAY], "); 
         UnSQL.SQL.Add(@"   @[MAINTASK_SCALEUNITS], "); 
         UnSQL.SQL.Add(@"   @[MAINTASK_SCALECOUNTER] "); 
         UnSQL.SQL.Add(@"   )  "); 
        UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PMMAINTASK_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            Persistence.ProjectManagement.Methods.PMMAINTASK_GETID(PMMAINTASK);
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.ProjectManagement.Methods.PMMAINTASK_UPD = function (PMMAINTASK) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.PMMAINTASK.IDPMMAINTASK.FieldName, PMMAINTASK.IDPMMAINTASK, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_NAME.FieldName, PMMAINTASK.MAINTASK_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_OWNER.FieldName, PMMAINTASK.MAINTASK_OWNER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_DURATION.FieldName, PMMAINTASK.MAINTASK_DURATION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_STARTDATE.FieldName, PMMAINTASK.MAINTASK_STARTDATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_ENDDATE.FieldName, PMMAINTASK.MAINTASK_ENDDATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_STATE.FieldName, PMMAINTASK.MAINTASK_STATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_WEEKENDHOLIDAY.FieldName, PMMAINTASK.MAINTASK_WEEKENDHOLIDAY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_SUNDAYHOLIDAY.FieldName, PMMAINTASK.MAINTASK_SUNDAYHOLIDAY, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_SCALEUNITS.FieldName, PMMAINTASK.MAINTASK_SCALEUNITS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_SCALECOUNTER.FieldName, PMMAINTASK.MAINTASK_SCALECOUNTER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);


        /*   
        //********   PMMAINTASK_UPD   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PMMAINTASK_UPD", @"PMMAINTASK_UPD description.");  
        UnSQL.SQL.Add(@"  UPDATE PMMAINTASK Set "); 
         UnSQL.SQL.Add(@"   MAINTASK_NAME=@[MAINTASK_NAME], "); 
         UnSQL.SQL.Add(@"   MAINTASK_OWNER=@[MAINTASK_OWNER], "); 
         UnSQL.SQL.Add(@"   MAINTASK_DURATION=@[MAINTASK_DURATION], "); 
         UnSQL.SQL.Add(@"   MAINTASK_STARTDATE=@[MAINTASK_STARTDATE], "); 
         UnSQL.SQL.Add(@"   MAINTASK_ENDDATE=@[MAINTASK_ENDDATE], "); 
         UnSQL.SQL.Add(@"   MAINTASK_STATE=@[MAINTASK_STATE], "); 
         UnSQL.SQL.Add(@"   MAINTASK_WEEKENDHOLIDAY=@[MAINTASK_WEEKENDHOLIDAY], "); 
         UnSQL.SQL.Add(@"   MAINTASK_SUNDAYHOLIDAY=@[MAINTASK_SUNDAYHOLIDAY], "); 
         UnSQL.SQL.Add(@"   MAINTASK_SCALEUNITS=@[MAINTASK_SCALEUNITS], "); 
         UnSQL.SQL.Add(@"   MAINTASK_SCALECOUNTER=@[MAINTASK_SCALECOUNTER] "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDPMMAINTASK=@[IDPMMAINTASK] ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PMMAINTASK_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.ProjectManagement.Methods.PMMAINTASK_DEL = function (/*String StrIDPMMAINTASK*/Param) {
    var Res = -1;
    if ((typeof (Param) == 'number') || (typeof (Param) == 'string')) {
        Res = Persistence.ProjectManagement.Methods.PMMAINTASK_DELIDPMMAINTASK(/*String StrIDPMMAINTASK*/Param);
    }
    else {
        Res = Persistence.ProjectManagement.Methods.PMMAINTASK_DELPMMAINTASK(Param);
    }
    return (Res);
}
Persistence.ProjectManagement.Methods.PMMAINTASK_DELIDPMMAINTASK = function (/*String StrIDPMMAINTASK*/IDPMMAINTASK) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        /*if (StrIDPMMAINTASK == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.PMMAINTASK.IDPMMAINTASK.FieldName, " = " + UsrCfg.InternoAtisNames.PMMAINTASK.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PMMAINTASK.IDPMMAINTASK.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDPMMAINTASK = " IN (" + StrIDPMMAINTASK + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.PMMAINTASK.IDPMMAINTASK.FieldName, StrIDPMMAINTASK, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/
        Param.AddInt32(UsrCfg.InternoAtisNames.PMMAINTASK.IDPMMAINTASK.FieldName, IDPMMAINTASK, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*
        //********   PMMAINTASK_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PMMAINTASK_DEL", @"PMMAINTASK_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE PMMAINTASK "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDPMMAINTASK=@[IDPMMAINTASK]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PMMAINTASK_DEL", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}
Persistence.ProjectManagement.Methods.PMMAINTASK_DELPMMAINTASK = function (PMMAINTASK/*PMMAINTASKList*/) {
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        /*
        StrIDPMMAINTASK = "";
        StrIDOTRO = "";
        for (var i = 0; i < PMMAINTASKList.length; i++) {
        StrIDPMMAINTASK += PMMAINTASKList[i].IDPMMAINTASK + ",";
        StrIDOTRO += PMMAINTASKList[i].IDOTRO + ",";
        }
        StrIDPMMAINTASK = StrIDPMMAINTASK.substring(0, StrIDPMMAINTASK.length - 1);
        StrIDOTRO = StrIDOTRO.substring(0, StrIDOTRO.length - 1);
        
        
        if (StrIDPMMAINTASK == "")
        {
        Param.AddUnknown(UsrCfg.InternoAtisNames.PMMAINTASK.IDPMMAINTASK.FieldName, " = " + UsrCfg.InternoAtisNames.PMMAINTASK.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PMMAINTASK.IDPMMAINTASK.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
        }
        else
        {
        StrIDPMMAINTASK = " IN (" + StrIDPMMAINTASK + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.PMMAINTASK.IDPMMAINTASK.FieldName, StrIDPMMAINTASK, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }*/


        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.PMMAINTASK.IDPMMAINTASK.FieldName, PMMAINTASK.IDPMMAINTASK, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        /*    
        //********   PMMAINTASK_DEL   *************************  
        UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PMMAINTASK_DEL", @"PMMAINTASK_DEL description.");  
        UnSQL.SQL.Add(@"   DELETE PMMAINTASK "); 
         UnSQL.SQL.Add(@"   Where "); 
         UnSQL.SQL.Add(@"   IDPMMAINTASK=@[IDPMMAINTASK]   ");
         UnConfigSQL.SQL.Add(UnSQL); 
         
        
        */
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PMMAINTASK_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
    return (ResErr);
}

//**********************   METHODS   ********************************************************************************************
Persistence.ProjectManagement.Methods.PMMAINTASK_ListSetID = function (PMMAINTASKList, IDPMMAINTASK) {
    for (i = 0; i < PMMAINTASKList.length; i++) {
        if (IDPMMAINTASK == PMMAINTASKList[i].IDPMMAINTASK)
            return (PMMAINTASKList[i]);
    }
    var UnPMMAINTASK = new Persistence.Properties.TPMMAINTASK;
    UnPMMAINTASK.IDPMMAINTASK = IDPMMAINTASK;
    return (UnPMMAINTASK);
}
Persistence.ProjectManagement.Methods.PMMAINTASK_ListAdd = function (PMMAINTASKList, PMMAINTASK) {
    var i = Persistence.ProjectManagement.Methods.PMMAINTASK_ListGetIndex(PMMAINTASKList, PMMAINTASK);
    if (i == -1) PMMAINTASKList.push(PMMAINTASK);
    else {
        PMMAINTASKList[i].IDPMMAINTASK = PMMAINTASK.IDPMMAINTASK;
        PMMAINTASKList[i].MAINTASK_NAME = PMMAINTASK.MAINTASK_NAME;
        PMMAINTASKList[i].MAINTASK_OWNER = PMMAINTASK.MAINTASK_OWNER;
        PMMAINTASKList[i].MAINTASK_DURATION = PMMAINTASK.MAINTASK_DURATION;
        PMMAINTASKList[i].MAINTASK_STARTDATE = PMMAINTASK.MAINTASK_STARTDATE;
        PMMAINTASKList[i].MAINTASK_ENDDATE = PMMAINTASK.MAINTASK_ENDDATE;
        PMMAINTASKList[i].MAINTASK_STATE = PMMAINTASK.MAINTASK_STATE;
        PMMAINTASKList[i].MAINTASK_WEEKENDHOLIDAY = PMMAINTASK.MAINTASK_WEEKENDHOLIDAY;
        PMMAINTASKList[i].MAINTASK_SUNDAYHOLIDAY = PMMAINTASK.MAINTASK_SUNDAYHOLIDAY;
        PMMAINTASKList[i].MAINTASK_SCALEUNITS = PMMAINTASK.MAINTASK_SCALEUNITS;
        PMMAINTASKList[i].MAINTASK_SCALECOUNTER = PMMAINTASK.MAINTASK_SCALECOUNTER;

    }
}
Persistence.ProjectManagement.Methods.PMMAINTASK_ListGetIndex = function (PMMAINTASKList, Param) {
    var Res = -1;
    if (typeof (Param) == 'number') {
        Res = Persistence.ProjectManagement.Methods.PMMAINTASK_ListGetIndexIDPMMAINTASK(PMMAINTASKList, Param);
    }
    else {
        Res = Persistence.ProjectManagement.Methods.PMMAINTASK_ListGetIndexPMMAINTASK(PMMAINTASKList, Param);
    }
    return (Res);
}
Persistence.ProjectManagement.Methods.PMMAINTASK_ListGetIndexPMMAINTASK = function (PMMAINTASKList, PMMAINTASK) {
    for (i = 0; i < PMMAINTASKList.length; i++) {
        if (PMMAINTASK.IDPMMAINTASK == PMMAINTASKList[i].IDPMMAINTASK)
            return (i);
    }
    return (-1);
}
Persistence.ProjectManagement.Methods.PMMAINTASK_ListGetIndexIDPMMAINTASK = function (PMMAINTASKList, IDPMMAINTASK) {
    for (i = 0; i < PMMAINTASKList.length; i++) {
        if (IDPMMAINTASK == PMMAINTASKList[i].IDPMMAINTASK)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.ProjectManagement.Methods.PMMAINTASKListtoStr = function (PMMAINTASKList) {
    var Res = Persistence.Properties._Version + "(1)0";
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(PMMAINTASKList.length, Longitud, Texto);
        for (Counter = 0; Counter < PMMAINTASKList.length; Counter++) {
            var UnPMMAINTASK = PMMAINTASKList[Counter];
            UnPMMAINTASK.ToStr(Longitud, Texto);
        }

        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
}
Persistence.ProjectManagement.Methods.StrtoPMMAINTASK = function (ProtocoloStr, PMMAINTASKList) {

    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        PMMAINTASKList.length = 0;
        if (Persistence.ProjectManagement.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.Length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnPMMAINTASK = new Persistence.Properties.TPMMAINTASK(); //Mode new row
                UnPMMAINTASK.StrTo(Pos, Index, LongitudArray, Texto);
                Persistence.ProjectManagement.PMMAINTASK_ListAdd(PMMAINTASKList, UnPMMAINTASK);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}


//Socket Function 
Persistence.ProjectManagement.Methods.PMMAINTASKListToByte = function (PMMAINTASKList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, PMMAINTASKList.Count - idx);
    for (i = idx; i < PMMAINTASKList.Count; i++) {
        PMMAINTASKList[i].ToByte(MemStream);
    }
}
Persistence.ProjectManagement.Methods.ByteToPMMAINTASKList = function (PMMAINTASKList, MemStream) {
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++) {
        UnPMMAINTASK = new Persistence.Properties.TPMMAINTASK();
        UnPMMAINTASK.ByteTo(MemStream);
        Methods.PMMAINTASK_ListAdd(PMMAINTASKList, UnPMMAINTASK);
    }
}
