//**********************   PROPERTIES   *****************************************************************************************
Persistence.ProjectManagement.Properties.TPMRELATION = function() {
	this.IDPMRELATION = 0;
	this.IDPMDETAILTASK_PREDECESSORS = 0;
	this.IDPMDETAILTASK = 0;
	this.IDPMTYPERELATION = 0;

	// Virtuales
	this.PMDETAILTASK_PREDECESSORS = new Persistence.ProjectManagement.Properties.TPMDETAILTASK();
	this.PMDETAILTASK = new Persistence.ProjectManagement.Properties.TPMDETAILTASK();
	this.PMTYPERELATION = new Persistence.ProjectManagement.Properties.TPMTYPERELATION();

	//Socket IO Properties
	this.ToByte = function(MemStream){
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDPMRELATION); 
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDPMDETAILTASK_PREDECESSORS); 
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDPMDETAILTASK); 
		SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDPMTYPERELATION); 

	}
	this.ByteTo = function(MemStream)
	{
		this.IDPMRELATION = SysCfg.Stream.Methods.ReadStreamInt32(MemStream); 
		this.IDPMDETAILTASK_PREDECESSORS = SysCfg.Stream.Methods.ReadStreamInt32(MemStream); 
		this.IDPMDETAILTASK = SysCfg.Stream.Methods.ReadStreamInt32(MemStream); 
		this.IDPMTYPERELATION = SysCfg.Stream.Methods.ReadStreamInt32(MemStream); 

	}
	//Str IO Properties
	this.ToStr = function(Longitud,Texto)
	{
		SysCfg.Str.Protocol.WriteInt(this.IDPMRELATION, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteInt(this.IDPMDETAILTASK_PREDECESSORS, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteInt(this.IDPMDETAILTASK, Longitud, Texto); 
		SysCfg.Str.Protocol.WriteInt(this.IDPMTYPERELATION, Longitud, Texto); 

	}
	this.StrTo = function(Pos,Index,LongitudArray,Texto)
	{
		this.IDPMRELATION = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto); 
		this.IDPMDETAILTASK_PREDECESSORS = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto); 
		this.IDPMDETAILTASK = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto); 
		this.IDPMTYPERELATION = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto); 

	}
}

//**********************   METHODS server  ********************************************************************************************
Persistence.ProjectManagement.Methods.PMRELATION_Fill = function(PMRELATIONList,StrIDPMRELATION/*noin IDPMRELATION*/)
{
	var ResErr = new SysCfg.Error.Properties.TResErr();
	PMRELATIONList.length = 0;
	var Param = new SysCfg.Stream.Properties.TParam();
	Param.Inicialize();
	if (StrIDPMRELATION == "")
	{
		Param.AddUnknown(UsrCfg.InternoAtisNames.PMRELATION.IDPMRELATION.FieldName, " = " + UsrCfg.InternoAtisNames.PMRELATION.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PMRELATION.IDPMRELATION.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
	}
	else
	{
		StrIDPMRELATION = " IN (" + StrIDPMRELATION + ")";
		Param.AddUnknown(UsrCfg.InternoAtisNames.PMRELATION.IDPMRELATION.FieldName, StrIDPMRELATION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
	}
	//noin Param.AddInt32(UsrCfg.InternoDemoNames.PMRELATION.IDPMRELATION.FieldName, IDPMRELATION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
	try
	{
		/*
		//********   PMRELATION_GET   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PMRELATION_GET", @"PMRELATION_GET description.");  
		UnSQL.SQL.Add(@"   SELECT "); 
		 UnSQL.SQL.Add(@"   IDPMRELATION, "); 
		 UnSQL.SQL.Add(@"   IDPMDETAILTASK_PREDECESSORS, "); 
		 UnSQL.SQL.Add(@"   IDPMDETAILTASK, "); 
		 UnSQL.SQL.Add(@"   IDPMTYPERELATION "); 
		 UnSQL.SQL.Add(@"   FROM PMRELATION "); 
		 UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		var DS_PMRELATION = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "PMRELATION_GET", Param.ToBytes());
		ResErr = DS_PMRELATION.ResErr;
		if (ResErr.NotError)
		{
			if (DS_PMRELATION.DataSet.RecordCount > 0)
			{
				DS_PMRELATION.DataSet.First();
				while (!(DS_PMRELATION.DataSet.Eof))
				{
					var PMRELATION = new Persistence.ProjectManagement.Properties.TPMRELATION();
					PMRELATION.IDPMRELATION = DS_PMRELATION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMRELATION.IDPMRELATION.FieldName).asInt32();
					PMRELATION.IDPMDETAILTASK_PREDECESSORS = DS_PMRELATION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMRELATION.IDPMDETAILTASK_PREDECESSORS.FieldName).asInt32();
					PMRELATION.IDPMDETAILTASK = DS_PMRELATION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMRELATION.IDPMDETAILTASK.FieldName).asInt32();
					PMRELATION.IDPMTYPERELATION = DS_PMRELATION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMRELATION.IDPMTYPERELATION.FieldName).asInt32();
					 
					//Other
					PMRELATIONList.push(PMRELATION);
					DS_PMRELATION.DataSet.Next();
				}
			}
			else
			{
				DS_PMRELATION.ResErr.NotError = false;
				DS_PMRELATION.ResErr.Mesaje = "RECORDCOUNT = 0";
			}
		}
	}
	finally
	{
	Param.Destroy();
	}
	return (ResErr);
}
Persistence.ProjectManagement.Methods.PMRELATION_GETID = function(PMRELATION)
{
	var ResErr = new SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try
	{
		Param.Inicialize();
		//Other
		Param.AddInt32(UsrCfg.InternoAtisNames.PMRELATION.IDPMRELATION.FieldName, PMRELATION.IDPMRELATION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddInt32(UsrCfg.InternoAtisNames.PMRELATION.IDPMDETAILTASK_PREDECESSORS.FieldName, PMRELATION.IDPMDETAILTASK_PREDECESSORS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddInt32(UsrCfg.InternoAtisNames.PMRELATION.IDPMDETAILTASK.FieldName, PMRELATION.IDPMDETAILTASK, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
		Param.AddInt32(UsrCfg.InternoAtisNames.PMRELATION.IDPMTYPERELATION.FieldName, PMRELATION.IDPMTYPERELATION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);

		/*
		//********   PMRELATION_GETID   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PMRELATION_GETID", @"PMRELATION_GETID description.");  
		UnSQL.SQL.Add(@"   SELECT "); 
		 UnSQL.SQL.Add(@"   IDPMRELATION "); 
		 UnSQL.SQL.Add(@"   FROM PMRELATION "); 
		 UnSQL.SQL.Add(@"   Where "); 
		 UnSQL.SQL.Add(@"   IDPMDETAILTASK_PREDECESSORS=@[IDPMDETAILTASK_PREDECESSORS] And "); 
		 UnSQL.SQL.Add(@"   IDPMDETAILTASK=@[IDPMDETAILTASK] And "); 
		 UnSQL.SQL.Add(@"   IDPMTYPERELATION=@[IDPMTYPERELATION] ");
		 UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		var DS_PMRELATION = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "PMRELATION_GETID", Param.ToBytes());
		ResErr = DS_PMRELATION.ResErr;
		if (ResErr.NotError)
		{
			if (DS_PMRELATION.DataSet.RecordCount > 0)
			{
			    DS_PMRELATION.DataSet.First();
			    PMRELATION.IDPMRELATION = DS_PMRELATION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PMRELATION.IDPMRELATION.FieldName).asInt32();
			}
			else
			{
			    DS_PMRELATION.ResErr.NotError = false;
			    DS_PMRELATION.ResErr.Mesaje = "RECORDCOUNT = 0";
			}
		}
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.ProjectManagement.Methods.PMRELATION_ADD = function(PMRELATION)
{
	var ResErr = new SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try
	{
		Param.Inicialize();
		//Other
		Param.AddInt32(UsrCfg.InternoAtisNames.PMRELATION.IDPMRELATION.FieldName, PMRELATION.IDPMRELATION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.PMRELATION.IDPMDETAILTASK_PREDECESSORS.FieldName, PMRELATION.IDPMDETAILTASK_PREDECESSORS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.PMRELATION.IDPMDETAILTASK.FieldName, PMRELATION.IDPMDETAILTASK, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.PMRELATION.IDPMTYPERELATION.FieldName, PMRELATION.IDPMTYPERELATION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		 
		/* 
		//********   PMRELATION_ADD   ************************* 
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PMRELATION_ADD", @"PMRELATION_ADD description."); 
		UnSQL.SQL.Add(@"   INSERT INTO PMRELATION( "); 
		 UnSQL.SQL.Add(@"   IDPMDETAILTASK_PREDECESSORS, "); 
		 UnSQL.SQL.Add(@"   IDPMDETAILTASK, "); 
		 UnSQL.SQL.Add(@"   IDPMTYPERELATION "); 
		 UnSQL.SQL.Add(@"   ) Values ( ");  
		UnSQL.SQL.Add(@"   @[IDPMDETAILTASK_PREDECESSORS], "); 
		 UnSQL.SQL.Add(@"   @[IDPMDETAILTASK], "); 
		 UnSQL.SQL.Add(@"   @[IDPMTYPERELATION] "); 
		 UnSQL.SQL.Add(@"   )  "); 
		UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		this.Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PMRELATION_ADD", Param.ToBytes());
		ResErr = Exec.ResErr;
		if (ResErr.NotError)
		{
			Persistence.ProjectManagement.Methods.PMRELATION_GETID(PMRELATION);
		}
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.ProjectManagement.Methods.PMRELATION_UPD = function(PMRELATION)
{
	var ResErr = SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try
	{
		Param.Inicialize();
		Param.AddInt32(UsrCfg.InternoAtisNames.PMRELATION.IDPMRELATION.FieldName, PMRELATION.IDPMRELATION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.PMRELATION.IDPMDETAILTASK_PREDECESSORS.FieldName, PMRELATION.IDPMDETAILTASK_PREDECESSORS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.PMRELATION.IDPMDETAILTASK.FieldName, PMRELATION.IDPMDETAILTASK, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		Param.AddInt32(UsrCfg.InternoAtisNames.PMRELATION.IDPMTYPERELATION.FieldName, PMRELATION.IDPMTYPERELATION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		 

		/*   
		//********   PMRELATION_UPD   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PMRELATION_UPD", @"PMRELATION_UPD description.");  
		UnSQL.SQL.Add(@"  UPDATE PMRELATION Set "); 
		 UnSQL.SQL.Add(@"   IDPMRELATION=@[IDPMRELATION], "); 
		 UnSQL.SQL.Add(@"   IDPMDETAILTASK_PREDECESSORS=@[IDPMDETAILTASK_PREDECESSORS], "); 
		 UnSQL.SQL.Add(@"   IDPMDETAILTASK=@[IDPMDETAILTASK], "); 
		 UnSQL.SQL.Add(@"   IDPMTYPERELATION=@[IDPMTYPERELATION] "); 
		 UnSQL.SQL.Add(@"   Where "); 
		 UnSQL.SQL.Add(@"   IDPMRELATION=@[IDPMRELATION] ");
		 UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PMRELATION_UPD", Param.ToBytes());
		ResErr = Exec.ResErr;
		if (ResErr.NotError)
		{
		//GET(); 
		}
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}
Persistence.ProjectManagement.Methods.PMRELATION_DEL = function(/*String StrIDPMRELATION*/IDPMRELATION)
{
	var ResErr = SysCfg.Error.Properties.TResErr();
	var Param = new SysCfg.Stream.Properties.TParam();
	try
	{
		Param.Inicialize();
		/*if (StrIDPMRELATION == "")
		{
		Param.AddUnknown(UsrCfg.InternoAtisNames.PMRELATION.IDPMRELATION.FieldName, " = " + UsrCfg.InternoAtisNames.PMRELATION.NAME_TABLE + "." + UsrCfg.InternoAtisNames.PMRELATION.IDPMRELATION.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
		}
		else
		{
		StrIDPMRELATION = " IN (" + StrIDPMRELATION + ")";
		Param.AddUnknown(UsrCfg.InternoAtisNames.PMRELATION.IDPMRELATION.FieldName, StrIDPMRELATION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		}*/
		Param.AddInt32(UsrCfg.InternoAtisNames.PMRELATION.IDPMRELATION.FieldName, IDPMRELATION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
		/*
		//********   PMRELATION_DEL   *************************  
		UnSQL = SysCfg.ConfigSQL.Methods.CreateTSQL(@"PMRELATION_DEL", @"PMRELATION_DEL description.");  
		UnSQL.SQL.Add(@"   DELETE PMRELATION "); 
		 UnSQL.SQL.Add(@"   Where "); 
		 UnSQL.SQL.Add(@"   IDPMRELATION=@[IDPMRELATION]   ");
		 UnConfigSQL.SQL.Add(UnSQL); 
		 

		*/
		var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "PMRELATION_DEL", Param.ToBytes());
		ResErr = Exec.ResErr;
	}
	finally
	{
		Param.Destroy();
	}
	return (ResErr);
}


//**********************   METHODS   ********************************************************************************************
Persistence.ProjectManagement.Methods.PMRELATION_ListSetID = function(PMRELATIONList,IDPMRELATION)
{
	for (i = 0; i < PMRELATIONList.length; i++)
	{
		if (IDPMRELATION == PMRELATIONList[i].IDPMRELATION)
			return (PMRELATIONList[i]);
	}
	var UnPMRELATION = new Persistence.Properties.TPMRELATION;
	UnPMRELATION.IDPMRELATION = IDPMRELATION; 
	return (UnPMRELATION);
}
Persistence.ProjectManagement.Methods.PMRELATION_ListAdd = function(PMRELATIONList,PMRELATION)
{
	var i = Persistence.ProjectManagement.Methods.PMRELATION_ListGetIndex(PMRELATIONList, PMRELATION);
	if (i == -1) PMRELATIONList.push(PMRELATION);
	else
	{
		PMRELATIONList[i].IDPMRELATION = PMRELATION.IDPMRELATION;
		PMRELATIONList[i].IDPMDETAILTASK_PREDECESSORS = PMRELATION.IDPMDETAILTASK_PREDECESSORS;
		PMRELATIONList[i].IDPMDETAILTASK = PMRELATION.IDPMDETAILTASK;
		PMRELATIONList[i].IDPMTYPERELATION = PMRELATION.IDPMTYPERELATION;

	}
}
Persistence.ProjectManagement.Methods.PMRELATION_ListGetIndex = function(PMRELATIONList, PMRELATION)
{
	for (i = 0; i < PMRELATIONList.length; i++)
	{
		if (PMRELATION.IDPMRELATION == PMRELATIONList[i].IDPMRELATION)
			return (i);
	}
	return (-1);
}
Persistence.ProjectManagement.Methods.PMRELATION_ListGetIndex = function(PMRELATIONList, IDPMRELATION)
{
	for (i = 0; i < PMRELATIONList.length; i++)
	{
		if (IDPMRELATION == PMRELATIONList[i].IDPMRELATION)
			return (i);
	}
	return (-1);
}

//String Function 
Persistence.ProjectManagement.Methods.PMRELATIONListtoStr = function(PMRELATIONList)
{
	var Res = Persistence.Properties._Version + "(1)0";
	var Longitud = new SysCfg.ref("");
	var Texto = new SysCfg.ref("");
	try
	{
		SysCfg.Str.Protocol.WriteInt(PMRELATIONList.length,Longitud,Texto);
		for (Counter = 0; Counter < PMRELATIONList.length; Counter++)
		{
			var UnPMRELATION = PMRELATIONList[Counter];
			UnPMRELATION.ToStr(Longitud,Texto);
		}

		Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
		Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
	}
	catch (e)
	{
		var ResErr = new SysCfg.Error.Properties.TResErr();
		ResErr.NotError = false;
		ResErr.Mesaje = "LIFESTATUStoStr:" + e;
	}
	return Res;
}
Persistence.ProjectManagement.Methods.StrtoPMRELATION = function(ProtocoloStr, PMRELATIONList)
{

	var Res = false;
	var Longitud, Texto;
	var Index =  new SysCfg.ref(0);
	var Pos =  new SysCfg.ref(0);
	try
	{
		PMRELATIONList.length = 0;   
		if (Persistence.ProjectManagement.Properties._Version == ProtocoloStr.substring(0, 3))
		{
			Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")") - ProtocoloStr.indexOf("(") - 1);
			Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.Length - ProtocoloStr.indexOf(")") - 1);
			LongitudArray = Longitud.split(",");
			LSCount = SysCfg.Str.Protocol.ReadInt(Pos,Index,LongitudArray,Texto);
			for (i = 0; i < LSCount; i++)
			{
			    UnPMRELATION = new Persistence.Properties.TPMRELATION(); //Mode new row
			    UnPMRELATION.StrTo(Pos,Index, LongitudArray, Texto);
			    Persistence.ProjectManagement.PMRELATION_ListAdd(PMRELATIONList, UnPMRELATION);
			}
		}
	}
	catch (e)
	{
		var ResErr = SysCfg.Error.Properties.TResErr;
		ResErr.NotError = false;
		ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
	}
	return (Res);
}


//Socket Function 
Persistence.ProjectManagement.Methods.PMRELATIONListToByte = function(PMRELATIONList, MemStream, idx)
{
	SysCfg.Stream.Methods.WriteStreamInt32(MemStream, PMRELATIONList.Count-idx);
	for (i = idx; i < PMRELATIONList.Count; i++)
	{
		PMRELATIONList[i].ToByte(MemStream);
	}
}
Persistence.ProjectManagement.Methods.ByteToPMRELATIONList = function (PMRELATIONList, MemStream)
{
	Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
	for (i = 0; i < Count; i++)
	{
		UnPMRELATION = new Persistence.Properties.TPMRELATION();
		UnPMRELATION.ByteTo(MemStream);
		Methods.PMRELATION_ListAdd(PMRELATIONList,UnPMRELATION);
	}
}
