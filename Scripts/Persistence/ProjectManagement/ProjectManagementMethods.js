Persistence.ProjectManagement.Methods.StartReation = function (PersistenceProfiler, inIDPMMAINTASK) {

	var PMMAINTASKListOrder = SysCfg.CopyList(PersistenceProfiler.PMMAINTASKList).sort(function (a, b) { return a.IDPMMAINTASK - b.IDPMMAINTASK; });
	var PMDETAILTASKListOrder = SysCfg.CopyList(PersistenceProfiler.PMDETAILTASKList).sort(function (a, b) { return a.IDPMMAINTASK - b.IDPMMAINTASK || a.DETAILTASK_INDEX - b.DETAILTASK_INDEX; });
	var PMPMRESOURCESListOrder = SysCfg.CopyList(PersistenceProfiler.PMRESOURCESList).sort(function (a, b) { return a.IDPMMAINTASK - b.IDPMMAINTASK; });
	var PMHOLIDAYListOrder = SysCfg.CopyList(PersistenceProfiler.PMHOLIDAYList).sort(function (a, b) { return a.IDPMMAINTASK - b.IDPMMAINTASK; });
	var IDPMMAINTASK = inIDPMMAINTASK;
	var idxPMDETAILTASK = 0;
	var idxPMRESOURCES = 0;
	var idxPMHOLIDAY = 0;
	for (var i = 0; i < PMMAINTASKListOrder.length; i++) {
		if ((IDPMMAINTASK == -1) || (IDPMMAINTASK == PMMAINTASKListOrder[i].IDPMMAINTASK)) {
			for (var x = idxPMDETAILTASK; x < PMDETAILTASKListOrder.length; x++) {
				idxPMDETAILTASK = x;
				if (PMDETAILTASKListOrder[x].IDPMMAINTASK == PMMAINTASKListOrder[i].IDPMMAINTASK) { 
					PMMAINTASKListOrder[i].PMDETAILTASKList.push(PMDETAILTASKListOrder[x]);
					PMDETAILTASKListOrder[x].PMMAINTASK = PMMAINTASKListOrder[i]; 
					if (PMDETAILTASKListOrder[x].IDPMDETAILTASK_PARENT == 0) {
						PMMAINTASKListOrder[i].PMDETAILTASKPARENTList.push(PMDETAILTASKListOrder[x]); //agrego padres;
					}
				}
				if (PMDETAILTASKListOrder[x].IDPMMAINTASK > PMMAINTASKListOrder[i].IDPMMAINTASK) {
					break;
				}
			}
			for(var x = idxPMRESOURCES; x < PMPMRESOURCESListOrder.length; x++){
				idxPMRESOURCES = x;
				if (PMPMRESOURCESListOrder[x].IDPMMAINTASK == PMMAINTASKListOrder[i].IDPMMAINTASK) {
					PMMAINTASKListOrder[i].PMRESOURCESList.push(PMPMRESOURCESListOrder[x]);
					PMPMRESOURCESListOrder[x].PMMAINTASK = PMMAINTASKListOrder[i];
				}
				if (PMPMRESOURCESListOrder[x].IDPMMAINTASK > PMMAINTASKListOrder[i].IDPMMAINTASK) {
					break;
				}
			}
			for(var x = idxPMHOLIDAY; x < PMHOLIDAYListOrder.length; x++){
				idxPMHOLIDAY = x;
				if (PMHOLIDAYListOrder[x].IDPMMAINTASK == PMMAINTASKListOrder[i].IDPMMAINTASK) {
					PMMAINTASKListOrder[i].PMHOLIDAYList.push(PMHOLIDAYListOrder[x]);
					PMHOLIDAYListOrder[x].PMMAINTASK = PMMAINTASKListOrder[i];
				}
				if (PMHOLIDAYListOrder[x].IDPMMAINTASK > PMMAINTASKListOrder[i].IDPMMAINTASK) {
					break;
				}
			}
		}
	}

	var PMDETAILTASKListOrder = SysCfg.CopyList(PersistenceProfiler.PMDETAILTASKList).sort(function (a, b) { return a.IDPMDETAILTASK - b.IDPMDETAILTASK; });
	var PMDETAILTASKPARENTListOrder = SysCfg.CopyList(PMDETAILTASKListOrder).sort(function (a, b) { return a.IDPMDETAILTASK_PARENT - b.IDPMDETAILTASK_PARENT || a.DETAILTASK_INDEX - b.DETAILTASK_INDEX;});
	var idxPMDETAILTASKPARENT = 0;
	var PMRELATIONListOrder = SysCfg.CopyList(PersistenceProfiler.PMRELATIONList).sort(function (a, b) { return a.IDPMDETAILTASK - b.IDPMDETAILTASK; });
	var idxPMRELATION = 0;

	var PMRELATIONSUCCESSORSListOrder = SysCfg.CopyList(PersistenceProfiler.PMRELATIONList).sort(function (a, b) { return a.IDPMDETAILTASK_PREDECESSORS - b.IDPMDETAILTASK_PREDECESSORS; });
	var idxPMRELATIONSUCCESSORS = 0;
	
	var PMRESOURCESALLOCATIONListOrder = SysCfg.CopyList(PersistenceProfiler.PMRESOURCESALLOCATIONList).sort(function (a, b) { return a.IDPMDETAILTASK - b.IDPMDETAILTASK; });
	var idxPMRESOURCESALLOCATION = 0;
	for (var i = 0; i < PMDETAILTASKListOrder.length; i++) {
		for (var x = idxPMDETAILTASKPARENT; x < PMDETAILTASKPARENTListOrder.length; x++) {
			idxPMDETAILTASKPARENT = x;
			if (PMDETAILTASKPARENTListOrder[x].IDPMDETAILTASK_PARENT == PMDETAILTASKListOrder[i].IDPMDETAILTASK) {
				PMDETAILTASKListOrder[i].PMDETAILTASK_CHILDList.push(PMDETAILTASKPARENTListOrder[x]);
				PMDETAILTASKPARENTListOrder[x].PMDETAILTASK_PARENT = PMDETAILTASKListOrder[i];
			}
			if (PMDETAILTASKPARENTListOrder[x].IDPMDETAILTASK_PARENT > PMDETAILTASKListOrder[i].IDPMDETAILTASK) {
				break;
			}
		}
		for (var x = idxPMRELATION; x < PMRELATIONListOrder.length; x++) {
			idxPMRELATION = x;
			if(PMRELATIONListOrder[x].IDPMDETAILTASK == PMDETAILTASKListOrder[i].IDPMDETAILTASK){
				PMDETAILTASKListOrder[i].PMRELATIONList.push(PMRELATIONListOrder[x]);
				PMRELATIONListOrder[x].PMDETAILTASK = PMDETAILTASKListOrder[i];
			}
			if(PMRELATIONListOrder[x].IDPMDETAILTASK > PMDETAILTASKListOrder[i].IDPMDETAILTASK){
				break;
			}
		}
		for(var x = idxPMRELATIONSUCCESSORS; x < PMRELATIONSUCCESSORSListOrder.length; x++){
			idxPMRELATIONSUCCESSORS = x;
			if(PMRELATIONSUCCESSORSListOrder[x].IDPMDETAILTASK_PREDECESSORS == PMDETAILTASKListOrder[i].IDPMDETAILTASK){
				PMDETAILTASKListOrder[i].PMRELATION_SUCCESSORSList.push(PMRELATIONSUCCESSORSListOrder[x]);
				PMRELATIONSUCCESSORSListOrder[x].PMDETAILTASK_PREDECESSORS = PMDETAILTASKListOrder[i];
			}
			if(PMRELATIONSUCCESSORSListOrder[x].IDPMDETAILTASK_PREDECESSORS > PMDETAILTASKListOrder[i].IDPMDETAILTASK){
				break;
			}
		}
		for(var x = idxPMRESOURCESALLOCATION; x < PMRESOURCESALLOCATIONListOrder.length; x++){
			idxPMRESOURCESALLOCATION = x;
			if(PMRESOURCESALLOCATIONListOrder[x].IDPMDETAILTASK == PMDETAILTASKListOrder[i].IDPMDETAILTASK){
				PMDETAILTASKListOrder[i].PMRESOURCESALLOCATIONList.push(PMRESOURCESALLOCATIONListOrder[x]);
				PMRESOURCESALLOCATIONListOrder[x].PMDETAILTASK = PMDETAILTASKListOrder[i];
			}
			if(PMRESOURCESALLOCATIONListOrder[x].IDPMDETAILTASK > PMDETAILTASKListOrder[i].IDPMDETAILTASK){
				break;
			}
		}
	}

	var PMRESOURCESListOrder = SysCfg.CopyList(PersistenceProfiler.PMRESOURCESList).sort(function (a, b) { return a.IDPMRESOURCES - b.IDPMRESOURCES; });
	var PMRESOURCESALLOCATIONListOrder = SysCfg.CopyList(PersistenceProfiler.PMRESOURCESALLOCATIONList).sort(function (a, b) { return a.IDPMRESOURCES - b.IDPMRESOURCES; });
	idxPMRESOURCESALLOCATION = 0;
	for (var i = 0; i < PMRESOURCESListOrder.length; i++) {
		for (var x = idxPMRESOURCESALLOCATION; x < PMRESOURCESALLOCATIONListOrder.length; x++) {
			idxPMRESOURCESALLOCATION = x;
			if(PMRESOURCESALLOCATIONListOrder[x].IDPMRESOURCES == PMRESOURCESListOrder[i].IDPMRESOURCES){
				PMRESOURCESListOrder[i].PMRESOURCESALLOCATIONList.push(PMRESOURCESALLOCATIONListOrder[x]);
				PMRESOURCESALLOCATIONListOrder[x].PMRESOURCES = PMRESOURCESListOrder[i];
			}
			if(PMRESOURCESALLOCATIONListOrder[x].IDPMRESOURCES > PMRESOURCESListOrder[i].IDPMRESOURCES){
				break;
			}
		}
	}

	var PMTYPERELATIONListOrder = SysCfg.CopyList(PersistenceProfiler.PMTYPERELATIONList).sort(function (a, b) { return a.IDPMTYPERELATION - b.IDPMTYPERELATION; });
	var PMRELATIONListOrder = SysCfg.CopyList(PersistenceProfiler.PMRELATIONList).sort(function (a, b) { return a.IDPMTYPERELATION - b.IDPMTYPERELATION});
	idxPMRELATION = 0;
	for(var i = 0; i < PMTYPERELATIONListOrder.length; i++){
		for(var x = idxPMRELATION; x < PMRELATIONListOrder.length; x++){
			idxPMRELATION = x;
			if(PMRELATIONListOrder[x].IDPMTYPERELATION == PMTYPERELATIONListOrder[i].IDPMTYPERELATION){
				PMTYPERELATIONListOrder[i].PMRELATIONList.push(PMRELATIONListOrder[x]);
				PMRELATIONListOrder[x].PMTYPERELATION = PMTYPERELATIONListOrder[i];
			}
			if(PMRELATIONListOrder[x].IDPMTYPERELATION > PMTYPERELATIONListOrder[i].IDPMTYPERELATION){
				break;
			}
		}
	}
}