﻿ItHelpCenter.TLoguinConfig = function (inObject, incallback, inVersion) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.ObjectHtml = inObject;
    this.Callback = incallback;
    this.version = inVersion;
    //this.idLenguaje = 1;
    if (SysCfg.App.Properties.Config_Language.LanguageITF.value == 0) {
        this.idLenguaje = 2;
    }
    else {//if (SysCfg.App.Properties.Config_Language.LanguageITF.value == 1)
        this.idLenguaje = 1;
    }


    



    var _this = this.TParent();
    this.Mythis = "LoguinConfig";
    //*******************************************************
    arrayleng = [["Subtitle", "Server Host.", "Servidor Huesped."],
                ["Label", "Server Host.", "Servidor Huesped."],
                ["btnAccept", "Save", "Guardar"],
                ["btnCancel", "Cancel", "Cancelar"]];

    //var _DBLocalLeng = new SysCfg.ini.DBLocalLeng('LocalLeng', 1);
    //_DBLocalLeng.Open(function (e) {
    //    _DBLocalLeng.Read(_this.idLenguaje, function (e) {
    //        _this.idLenguaje = e.target.result.IDleng;
    //        _this.Load();
    //    }, function (e) {
    //        alert(" Can´t Read LocalIndexDB. " + _this.Mythis);
    //    });
    //}, function (e) {
    //    alert(" Can´t Load LocalIndexDB. " + _this.Mythis);
    //});

    var _DBLocalLeng = new SysCfg.ini.LSLocalLeng();
    _this.idLenguaje = _DBLocalLeng.Read(_this.idLenguaje,false);
    _this.Load();
}

ItHelpCenter.TLoguinConfig.prototype.Load = function () {
    var _this = this.TParent();
    var ObjHtml = _this.ObjectHtml;
    ObjHtml.innerHTML = "";

    //ESTRUCTURA
    var Container = new TVclStackPanel(ObjHtml, "ContainerConfig", 1, [[12], [12], [12], [12], [12]]);
    var ContainerCont = Container.Column[0].This;

    //div1 BANDERA
    var div1 = new TVclStackPanel(ContainerCont, "Config_div1", 1, [[12], [12], [12], [12], [12]]);
    var div1_Cont = div1.Column[0].This;

    //div2 LOGO / TITLE
    var div2 = new TVclStackPanel(ContainerCont, "Config_div2", 1, [[12], [12], [12], [12], [12]]);
    var div2_Cont = div2.Column[0].This;

    var div2_1 = new TVclStackPanel(ContainerCont, "Config_div2_1", 2, [[12, 12], [5, 7], [5, 7], [5, 7], [4, 8]]);
    var div2_1_1Cont = div2_1.Column[0].This;//LOGO
    var div2_1_2Cont = div2_1.Column[1].This; //TITLE Contenedor

    var div2_1_2_1 = new TVclStackPanel(div2_1_2Cont, "Config_div2_1_1", 1, [[12], [12], [12], [12], [12]]);
    var div2_1_2_1_Cont = div2_1_2_1.Column[0].This; //TITLE

    var div2_1_2_2 = new TVclStackPanel(div2_1_2Cont, "Config_div2_1_2", 1, [[12], [12], [12], [12], [12]]);
    var div2_1_2_2_Cont = div2_1_2_2.Column[0].This; //SUBTITLE

    //div4 text
    var div3 = new TVclStackPanel(ContainerCont, "Config_div3", 2, [[12, 12], [5, 7], [5, 7], [5, 7], [4, 8]]);
    var div3_1Cont = div3.Column[0].This; //Label
    var div3_2Cont = div3.Column[1].This; //Text

    //div4 Btns
    var div4 = new TVclStackPanel(ContainerCont, "Config_div4", 2, [[12, 12], [6, 6], [6, 6], [6, 6], [6, 6]]);
    var div4_1Cont = div4.Column[0].This; //Label
    var div4_2Cont = div4.Column[1].This; //Text

    //ELEMENTOS
    var bandera = new TVclImagen(div1_Cont, "ConfigFlag");
    if (_this.idLenguaje == 1) {
        bandera.Src = "image/24/Spain-flag.png"
    } else {
        bandera.Src = "image/24/United-states-flag.png";
        
    }
    bandera.Cursor = "Pointer";
    bandera.onClick = function Change_Lenguage() {
        if (_this.idLenguaje == 1) {
            _this.idLenguaje = 2;
            bandera.Src = "image/24/United-states-flag.png"
        } else {
            _this.idLenguaje = 1;
            bandera.Src = "image/24/Spain-flag.png"
            
        }
        _this.SaveLocalLanguage();

        Subtitle.innerText = arrayleng[0][_this.idLenguaje];
        LabServer.innerText = arrayleng[1][_this.idLenguaje];
        btnAccept.text = arrayleng[2][_this.idLenguaje];
        btnCancel.text = arrayleng[3][_this.idLenguaje];
    }

    var Logo = new TVclImagen(div2_1_1Cont, "ConfigLogo");
    Logo.Src = "image/High/IT-Help-Center-Final file.png";

    var Title = new Vcllabel(div2_1_2_1_Cont, "ConfigTitle", TVCLType.BS, TlabelType.H0, "IT Help Center");
    var Subtitle = new Vcllabel(div2_1_2_2_Cont, "ChangeSubtitle", TVCLType.BS, TlabelType.H0, arrayleng[0][_this.idLenguaje]);

    var LabServer = new Vcllabel(div3_1Cont, "ConfigServer", TVCLType.BS, TlabelType.H0, arrayleng[1][_this.idLenguaje]);
    var TextServer = new TVclTextBox(div3_2Cont, "ConfigServer");
    $(TextServer.This).css("width", "100%");
    $(TextServer.This).css('border', "1px solid #000000");


    var btnAccept = new TVclButton(div4_1Cont, "ConfigAccept");
    btnAccept.Cursor = "pointer";
    btnAccept.Text = arrayleng[2][_this.idLenguaje];
    btnAccept.VCLType = TVCLType.BS;
    btnAccept.onClick = function myfunction() {
        var endpoint_Address = TextServer.value;
        var datalogin = {
            'endpoint_Address': endpoint_Address
        };
        $.ajax({
            type: "POST",
            url: 'Service/wcfLoguinConfig.svc/Saveendpoint_Address',
            data: JSON.stringify(datalogin),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response) {
                    _this.Callback(ItHelpCenter.PageManager.TGoToPageType.Index);
                }
                else {
                    alert('Error');
                }
            },
            error: function (response) {
                SysCfg.Log.Methods.WriteLog("LoginConfig.js ItHelpCenter.TLoguinConfig.prototype.Load Service/wcfLoguinConfig.svc/Saveendpoint_Address " + 'Login');
            }
        });
    }

    var btnCancel = new TVclButton(div4_2Cont, "ConfigCancel");
    btnCancel.Cursor = "pointer";
    btnCancel.Text = arrayleng[3][_this.idLenguaje];
    btnCancel.VCLType = TVCLType.BS;
    btnCancel.onClick = function myfunction() {
        _this.Callback(ItHelpCenter.PageManager.TGoToPageType.Index);
    }
}

ItHelpCenter.TLoguinConfig.prototype.SaveLocalLanguage = function () {
    var _this = this.TParent();
    //var _DBLocalLeng = new SysCfg.ini.DBLocalLeng('LocalLeng', 1);
    //_DBLocalLeng.Open(function (e) {
    //    _DBLocalLeng.Write(_this.idLenguaje, function (e) {
    //    }, function (e) {
    //        alert("Can´t write LocalIndexDB. " + _this.Mythis); 
    //    });
    //}, function (e) {
    //    alert("Can´t load LocalIndexDB. " + _this.Mythis);
    //});

    var _DBLocalLeng = new SysCfg.ini.LSLocalLeng();
    _this.idLenguaje = _DBLocalLeng.Write(_this.idLenguaje);
}