﻿

var SysCfg = {
    App: {
        Methods: {},
        Properties: {}
    },
    DateTimeMethods: {},
    Methods: {},
    Stream: {
        Methods: {},
        Properties: {}
    },
    Str: {
        Methods: {},
        Protocol: {}
    },
    FilesIO: {
        Methods: {}
    },
    DB: {
        SQL: {
            Methods: {},
            Properties: {}
        },
        Properties: {
        }
    },
    MemTable: {
        Properties: {}
    },
    Error: {
        Methods: {},
        Properties: {}
    },
    Controls: {
    },
    Properties: {
        Device: {},
        TDevice: {
            Destok: path = "/",
            Tablet: path = "/Devices/Tablet/",
            Mobile: path = "/Devices/Mobile/"
        }
    },
    Interno: {
        Properties: {
            Date_SysLog_mdb: "[DATE_SYSLOG]DATETIME",
            Date_SysLog_mdf: "[DATE_SYSLOG]DATETIME",
            Date_SysLog_orc: '""DATE_SYSLOG""DATE',
            Event_SysLog_mdb: "[IDEVENT_SYSLOG]INTEGER",
            Event_SysLog_mdf: "[IDEVENT_SYSLOG]INTEGER",
            Event_SysLog_orc: '""IDEVENT_SYSLOG""NUMBER',
            FieldName_Date: "DATE_SYSLOG",
            FieldName_Event_SysLog: "IDEVENT_SYSLOG",
            FieldName_IDCMDBCI_SysLog: "IDCMDBCI_SYSLOG",
            FieldName_Source_SysLog: "IDSOURCE_SYSLOG",
            IDCMDBCI_SysLog_mdb: "[IDCMDBCI_SYSLOG]INTEGER",
            IDCMDBCI_SysLog_mdf: "[IDCMDBCI_SYSLOG]INTEGER",
            IDCMDBCI_SysLog_orc: '"""IDCMDBCI_SYSLOG""NUMBER',
            PrefixSysLog: "_SYSLOG",
            Source_SysLog_mdb: "[IDSOURCE_SYSLOG]INTEGER",
            Source_SysLog_mdf: "[IDSOURCE_SYSLOG]INTEGER",
            Source_SysLog_orc: '""IDSOURCE_SYSLOG""NUMBER'
        }
    },
    License: {
        Properties: {
        },
        Client: {
            Methods: {
            }

        }
    },
    ini: {
    },
    Device: {
    },
    Log: {
        Methods: {},
        Properties: {}
    },
    IDI: {
        Client: {},
        Properties: {}
    },
    Domain: {}
};

//Interface
//Interface.Methods
//Interface.Properties
var Interface = {
    Methods: {},
    Properties: {}
}


var Comunic = {
    Properties: {},
    AjaxJson: {
        Client: {
            Methods: {},
            Properties: {}
        }
    },
    Ashx: {
        Client: {
            Methods: {},
            Properties: {}
        }
    }
}

var UsrCfg = {
    SD: {
        Properties: {},
        Service: {},
        TYPEUSER_RWX: {
            Methods: {},
            Properties: {}
        },
        SDCaseTimerCount: {
            Methods: {},
            Properties: {}
        }
    },
    CMDB: {
        Properties: {}
    },
    Properties: {},
    Methods: {},
    EF: {},
    Version: {},
    Library: {},
    SC: {
        Properties: {},
        Methods: {}
    },
    Traslate: {}
}

var ST = {
    Shared: {}
}



//Persistence.PBITemplate.Properties.TPBITEMPLATE 
var Persistence = {
    Demo: {
        Methods: {},
        Properties: {}
    },
    PanelService: {
        Methods: {},
        Properties: {}

    },
    PBITemplate: {
        Methods: {},
        Properties: {}
    },
    Forums: {
        Methods: {},
        Properties: {}
    },
    GP: {
        Methods: {},
        Properties: {}
    },
    SD: {
        Methods: {},
        Properties: {}
    },

    MD: {//se eliminara por que no esta completa 
        Methods: {},
        Properties: {}
    },
    Votes: {
        Methods: {},
        Properties: {}
    },
    ProjectManagement:{
        Methods:{},
        Properties:{}
    },
	
    Catalog: {
        Methods: {},
        Properties: {}
    },
    Chat: {
        Methods: {},
        Properties: {}
    },
    Model: {
        Methods: {},
        Properties: {}
    },

    Notify: {
        Methods: {},
        Properties: {}
    },
    RemoteHelp: {
        Methods: {},
        Properties: {}
    },
    SMCI: {
        Methods: {},
        Properties: {},
        Methods_Cmd: {},
        Methods_DataBase: {}
    },
    Properties: {},
    Methods: {},
    Profiler: {}
}
var Source = {
    Font: {},
    Menu: {},
    Page: {
        Example: {},
        Methods: {},
        Properties: {}
    },
    Shared: {},
    SequentialColor: {},
    Style: {
        Methods: {},
        Properties: {},
        Profiler: {}
    },
    Sequencial: {
        Properties: {},
        Methods: {}
    }
}

var ItHelpCenter = {
    Componet: {
        SeekSearch: {
            Methods: {},
            Properties: {}
        }, NavTabControls: {
            Methods: {},
            Properties: {}
        }, TimeLine: {
            Methods: {},
            Properties: {}
        }, Captcha: {
            Methods: {},
            Properties: {}
        }, ImageControls: {
            Methods: {},
            Properties: {}
        }, MapsControls: {
            Methods: {},
            Properties: {}
        }
    },
    Demo: {
        //lo eseña a usar el componete 
        //GraphicsPBI 
        //Panelservices        
    },
    RemoteHelp: {
    },
    ScriptManager: {
    },
    SD: {
        AConsole: {},
        AdvSearch: {
            TAdvSearchManager: {
            }
        },
        Methods: {
        },
        frAdvMsgEdit: {},
        CaseUser: {
            Atention: {
            },
            NewCase: {
            },
            Shared: {
            }
        },
        CaseManager: {
            Atention: {
            },
            NewCase: {
            },
            Shared: {
            }
        },
        Shared: {
            Atention: {
                CaseAtentionDetail: {},
                ChildUserPermissions: {}
            },
            Attached: {
            },
            Case_CMDBCI: {
                TfrSDCMDBCI: {}
            },
            Category: {
                Mode: {}
            },
            Contact: {
            },
            SLASelect: {
            },
            RelatedCase: {
            }
        },
        Atention: {//no esta bien oragnizado pero respeta atis 
            CaseAtentionCheckStd: {//no esta bien oragnizado pero respeta atis 
            }
        },

    },
    MD: {
        Configuration: {
            Priority: {
            },
            Urgency: {
            },
            Impact: {
            },
            PriorityMatrix: {
            }
        }
    },
    CIEditor: {
    },
    CMDB: {
        DiagramMap: {},
        CIEditor: {}
    },

    //lo utiliza con un objetivo esoecifico //nodo en main
    //TGraphicsPBIManager  //editor de configuracion 
    //TPanelservicesManager //editor de configuracion 
    TPanelServicesManager: {
        PanelServicesMain: {},
        TVclDropdownPanelServices: {},
        PanelServicesManager: {},
        PanelServicesBasic: {},
        PanelServicesMapping: {}
    },
    GraphicsPBIManager: {
        TGraphicsPBIManagerBasic: {}
    },
    VotesManager: {
    },
    PageManager: {
    },
    App: {
    },
    ComponentMindFusionManagerDiagramingProvider: {},
    TForumsManager: {}

}

var Componet = {
    Controls: {
        Vertical: 0,
        Horizontal: 1
    },
    TGraphicsPBI: {
        TVclFunnel: {},
        TVclTable: {},
        TVclPie: {},
        TVclTreeMap: {},
        TVclBars: {},
        TVclTemplate: {},
        TVclCard: {},
        TVclLogo: {}
    },
    TPalleteColor: {},
    TGraphicsElements: {
        TVclGraphicElementFunnel: {},
        TVclGraphicElementTable: {},
        TVclPieElements: {},
        TVclTreeMapElements: {},
        TVclBarsElements: {},
        TVclTemplateElement: {},
        TVclCardElements: {}
    },
    TVotesElements: {},
    TElements: {
        TPanelServicesElementTab: {},
        TPanelServicesElementTabContent: {}
    },
    GridCF: {
        Methods: {},
        Properties: {}
    },
    AdvSearch: {},
    Properties: {},
    Scheduler: {},
    ListView: {},
    TreeView: {},
    ListBox: {}
}





SysCfg.Methods.getRaiz = function () {
    var xURL = window.location.href;
    var xRes = xURL.split("/");
    var cant = xRes.length;
    var i = 0;
    var tempRaiz = "";
    while (i < cant) {
        tempRaiz += xRes[i] + "/";
        direc = tempRaiz + "Index.aspx";
        try {
            if (SysCfg.Methods.fileHeadExists(direc)) {
                i = cant; //para el ciclo
            } else {
                i = i + 1; //cotinua el ciclo
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("namespace.js SysCfg.Methods.getRaiz", e);
        }
    }
    return tempRaiz;
}
SysCfg.Methods.getVclRaiz = function () {
    var VlcxURL = window.location.href;
    var VlcxRes = VlcxURL.split("/");
    var VlcxRaiz = "";

    var Vlccant = VlcxRes.length;
    var Vlci = 0;
    while (Vlci < Vlccant) {
        VlcxRaiz += VlcxRes[Vlci] + "/";
        Vlcdirec = VlcxRaiz + "Index.aspx";
        if (SysCfg.Methods.fileExists(Vlcdirec)) {
            Vlci = Vlccant; //para el ciclo
        } else {
            Vlci = Vlci + 1; //cotinua el ciclo
        }
    }
    return VlcxRaiz;
}
SysCfg.Methods.include = function (archivo) {
    var isIE = /*@cc_on!@*/false || !!document.documentMode;
    if (!SysCfg.Methods.ScriptExists(SysCfg.App.Properties.xRaiz + archivo)) {
         

        document.write('<script charset="utf-8" type="text/javascript" src="' + SysCfg.App.Properties.xRaiz + archivo + "?" + UsrCfg.Version._UsrCfgAppVersion + '"></script>');

    }
    else
        alert("File no exist: " + archivo + "\\n please add file.");
}
SysCfg.Methods.ScriptExists = function (archivo) {
    var scripts = document.getElementsByTagName("script")
    for (var i = 0; i < scripts.length; ++i) {
        if (scripts[i].getAttribute('src') == archivo)
            return true;
    }
    return false;
}
SysCfg.Methods.includeStyle = function (archivo) {
    var isIE = /*@cc_on!@*/false || !!document.documentMode;
    if (!SysCfg.Methods.StyletExists(SysCfg.App.Properties.xRaiz + archivo)) {
        document.write('<link rel="stylesheet" media="all" type="text/css" href="' + archivo + '"></link>');

    }
    else
        alert("File no exist: " + archivo + "\\n please add file.");
}
SysCfg.Methods.StyletExists = function (archivo) {
    var scriptsStyle = document.getElementsByTagName("link")
    for (var i = 0; i < scriptsStyle.length; ++i) {
        if (scriptsStyle[i].getAttribute('href') == archivo)
            return true;
    }
    return false;
}
SysCfg.Methods.fileHeadExists = function (direc) {
    var respuesta = false;
    $.ajax({
        url: direc,
        async: false,
        type: 'HEAD',
        error: function () {
            respuesta = false;
        },
        success: function () {
            respuesta = true;
        }
    });
    return respuesta;
}

SysCfg.App.Properties.xRaiz = SysCfg.Methods.getRaiz();


SysCfg.App.IniDB = null;
SysCfg.Methods.getRandomInt = function(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}

UsrCfg.Version._UsrCfgAppVersion = SysCfg.Methods.getRandomInt(1, 10000).toString();
//randomNo=Math.random()
SysCfg.Methods.include("Scripts/UsrCfgScripts/Version.js");
SysCfg.Methods.include("Scripts/Componet/ComponetLibrary.js");
SysCfg.Methods.include("Scripts/SysCfgScripts/SysLibrary.js");
SysCfg.Methods.include("Scripts/UsrCfgScripts/UsrLibrary.js");


