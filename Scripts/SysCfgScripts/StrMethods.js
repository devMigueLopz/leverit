﻿SysCfg.Str.isEqual = function (str1, str2) {
    return (str1.toUpperCase() == str2.toUpperCase());
}

SysCfg.Str.BooleanToTxt = function (Value) {
    if (Value) return ("True");
    else return ("False");
}


SysCfg.Str.Protocol.WriteStr = function (ValueStr, Longitud, Texto)
{
    Longitud.Value += ValueStr.length.toString() + ",";
    Texto.Value += ValueStr.toString();
}
SysCfg.Str.Protocol.WriteInt = function (ValueInt32, Longitud, Texto)
{
    Longitud.Value += ValueInt32.toString().length.toString() + ",";
    Texto.Value += ValueInt32.toString();
}
SysCfg.Str.Protocol.WriteBool = function (ValueBoolean, Longitud, Texto)
{    
    var ValueInt32 = 0;
    if (ValueBoolean) ValueInt32 = 1;                         
    Longitud.Value += ValueInt32.toString().length.toString() + ",";
    Texto.Value += ValueInt32.toString();
}


SysCfg.Str.Protocol.DateTimeToStrProtocol = function (inValueDateTime2)
{
    //https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date
    var ValueDateTime2 = inValueDateTime2;
    var dy = ValueDateTime2.getDate().toString();
    if (dy.length == 1) dy = "0" + dy;
    var mn = (ValueDateTime2.getMonth() + 1).toString();
    if (mn.length == 1) mn = "0" + mn;
    var yy = ValueDateTime2.getFullYear().toString();
    if (yy.length == 1) yy = "000" + yy;
    if (yy.length == 2) yy = "00" + yy;
    if (yy.length == 3) yy = "0" + yy;
    var hh = ValueDateTime2.getHours().toString();
    if (hh.length == 1) hh = "0" + hh;
    var mm = ValueDateTime2.getMinutes().toString();
    if (mm.length == 1) mm = "0" + mm;
    var ss = ValueDateTime2.getSeconds().toString();
    if (ss.length == 1) ss = "0" + ss;
    var DateTimeStr = dy + mn + yy + hh + mm + ss;
    return (DateTimeStr);
}

SysCfg.Str.Protocol.WriteDateTime = function (inValueDateTime2, Longitud, Texto)
{
    var DateTimeStr = DateTimeToStrProtocol(inValueDateTime2);
    Longitud.Value += DateTimeStr.length.toString() + ",";
    Texto.Value += DateTimeStr.toString();
}

SysCfg.Str.Protocol.WriteDouble = function (ValueDouble, Longitud, Texto)
{
    Longitud.Value += ValueDouble.toString().length.toString() + ",";
    Texto.Value += ValueDouble.toString();
}


SysCfg.Str.Protocol.ReadStr = function (Pos, Index, LongitudArray, Texto)
{
    var Res = "";
    var Longitud = parseInt(LongitudArray[Index.Value]);
    Res = Texto.substring(Pos.Value, Pos.Value + Longitud);
    Index.Value++;
    Pos.Value += Longitud;
    return Res;
}
SysCfg.Str.Protocol.ReadInt = function (Pos, Index, LongitudArray, Texto) {
    var Res = 0;
    var Longitud = parseInt(LongitudArray[Index.Value]);
    Res = parseInt(Texto.substring(Pos.Value, Pos.Value + Longitud));
    Index.Value++;
    Pos.Value += Longitud;
    return Res;
}
SysCfg.Str.Protocol.ReadBool = function (Pos, Index, LongitudArray, Texto) {
    var Res = false;
    var Longitud = parseInt(LongitudArray[Index.Value]);
    Res = (parseInt(Texto.substring(Pos.Value, Pos.Value + Longitud)) == 1);
    Index.Value++;
    Pos.Value += Longitud;
    return Res;
}

SysCfg.Str.Protocol.StrProtocolToDateTime = function (DateTimeStr) {
    var Res = new Date(1970, 0, 1, 0, 0, 0);
    try {
        var dy = parseInt(DateTimeStr.substring(0, 2));
        var mn = parseInt(DateTimeStr.substring(2, 4)) - 1;
        var yy = parseInt(DateTimeStr.substring(4, 8));
        var hh = parseInt(DateTimeStr.substring(8, 10));
        var mm = parseInt(DateTimeStr.substring(10, 12));
        var ss = parseInt(DateTimeStr.substring(12, 14));
        Res = new Date(yy, mn, dy, hh, mm, ss, 0);
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("StrMethods.js SysCfg.Str.Protocol.StrProtocolToDateTime", e);

    }
    return Res;
}

SysCfg.Str.Protocol.ReadDateTime = function (Pos,Index,LongitudArray,Texto)
{   
    try {
        var Longitud = parseInt(LongitudArray[Index.Value]);        
        var ResDateTime =  SysCfg.Str.Protocol.StrProtocolToDateTime(Texto.substring(Pos.Value, Pos.Value + Longitud));
        Index.Value++;
        Pos.Value += Longitud;        
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("StrMethods.js SysCfg.Str.Protocol.ReadDateTime", e);
    }
    return ResDateTime;
}

SysCfg.Str.Protocol.ReadDouble = function (Pos, Index, LongitudArray, Texto) {
    var Res = 0;
    var Longitud = parseInt(LongitudArray[Index.Value]);
    Res = parseFloat(Texto.substring(Pos.Value, Pos.Value + Longitud));
    Index.Value++;
    Pos.Value += Longitud;
    return Res;
}

SysCfg.Str.Methods.GetStrLastChar = function (Cadena, inChar) {
    var StrOut = "";
    var idx = 0;
    if (Cadena != undefined) {
        while (Cadena.length != idx) {
            if (inChar == Cadena[idx]) {
                StrOut = "";
            }
            else {
                StrOut += Cadena[idx];
            }
            idx++;
        }
    }
    return (StrOut);
}
SysCfg.Str.Methods.IsNullOrEmpity = function (DinamycValue) {
    if (DinamycValue === null || DinamycValue === "" || DinamycValue === undefined) {
        return (true);
    }
    else {
        return (false);
    }
}
SysCfg.Str.Methods.ExtensionDate = {
    convert: function (d) {
        // Converts the date in d to a date-object. The input can be:
        //   a date object: returned without modification
        //  an array      : Interpreted as [year,month,day]. NOTE: month is 0-11.
        //   a number     : Interpreted as number of milliseconds
        //                  since 1 Jan 1970 (a timestamp) 
        //   a string     : Any format supported by the javascript engine, like
        //                  "YYYY/MM/DD", "MM/DD/YYYY", "Jan 31 2009" etc.
        //  an object     : Interpreted as an object with year, month and date
        //                  attributes.  **NOTE** month is 0-11.
        return (
            d.constructor === Date ? d :
                d.constructor === Array ? new Date(d[0], d[1], d[2]) :
                    d.constructor === Number ? new Date(d) :
                        d.constructor === String ? new Date(d) :
                            typeof d === "object" ? new Date(d.year, d.month, d.date) :
                                NaN
        );
    },
    compare: function (a, b) {
        // Compare two dates (could be of any type supported by the convert
        // function above) and returns:
        //  -1 : if a < b
        //   0 : if a = b
        //   1 : if a > b
        // NaN : if a or b is an illegal date
        // NOTE: The code inside isFinite does an assignment (=).
        return (
            isFinite(a = this.convert(a).valueOf()) &&
                isFinite(b = this.convert(b).valueOf()) ?
                (a > b) - (a < b) :
                NaN
        );
    },
    inRange: function (d, start, end) {
        // Checks if date in d is between dates in start and end.
        // Returns a boolean or NaN:
        //    true  : if d is between start and end (inclusive)
        //    false : if d is before start or after end
        //    NaN   : if one or more of the dates is illegal.
        // NOTE: The code inside isFinite does an assignment (=).
        return (
            isFinite(d = this.convert(d).valueOf()) &&
                isFinite(start = this.convert(start).valueOf()) &&
                isFinite(end = this.convert(end).valueOf()) ?
                start <= d && d <= end :
                NaN
        );
    }
}