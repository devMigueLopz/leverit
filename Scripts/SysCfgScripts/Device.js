﻿//https://blog.endeos.com/como-detectar-un-dispositivo-movil-con-jquery/

SysCfg.Device.IsMovile1 = function () {
    var isMobile = {
        Android: function () {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function () {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function () {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function () {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function () {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function () {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };
    //if (isMobile.BlackBerry()) alert('BlackBerry');
    //if (isMobile.iOS()) alert('iOS');
    //if (isMobile.Android()) alert('Android');
    //if (isMobile.Windows()) alert('Windows');
    return (isMobile.any())
}

SysCfg.Device.IsMovile2 = function () {
    return (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) 
}