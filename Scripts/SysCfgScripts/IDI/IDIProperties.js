﻿//SysCfg.IDI.Properties

SysCfg.IDI.Properties.TIDIRow = function()
{
    this.IDSUB = "";
    this.LangText = "";
}
SysCfg.IDI.Properties.TIDITable = function()
{            
    this.SPANICH_IDIByte = new Array();
    this.ENGLISH_IDIByte = new Array();
    this.PORTUGUESE_IDIByte = new Array();
    this.SPANICH_IDIDataSet = new SysCfg.MemTable.TDataSet();
    this.ENGLISH_IDIDataSet = new SysCfg.MemTable.TDataSet();
    this.PORTUGUESE_IDIDataSet = new SysCfg.MemTable.TDataSet();
    this.ID = "";
    this.IDIRowList = new Array();//TIDIRow
    this.SPANICH_IDIRowList = new Array();//TIDIRow
    this.ENGLISH_IDIRowList = new Array();//TIDIRow
    this.PORTUGUESE_IDIRowList = new Array();//TIDIRow

}
SysCfg.IDI.Properties.TIDIApplication = function()
{
    this.Application = SysCfg.App.TApplication.GetEnum(undefined);            
    this.IDITableList = new Array();//TIDITable
}

SysCfg.IDI.Properties.IDIApplicationList = new Array();//TIDIApplication  
SysCfg.IDI.Properties.EnableTraslate = false;
        

SysCfg.IDI.Properties.IDIApplication_ListAdd = function(IDIApplicationList,IDIApplication)
{
    var i = SysCfg.IDI.Properties.IDIApplication_ListGetIndex(IDIApplicationList, IDIApplication.Application);
    if (i == -1)
    {
        IDIApplicationList.push(IDIApplication);
    }
    else IDIApplication = IDIApplicationList[i];
}

SysCfg.IDI.Properties.IDIApplication_ListGetIndex = function(IDIApplicationList,Application)
{
    for (var i = 0; i < IDIApplicationList.length; i++)
    {
        if (Application == IDIApplicationList[i].Application)
            return (i);
    }
    return (-1);
}

SysCfg.IDI.Properties.IDITable_ListAdd = function (IDITableList, IDITable, isServer)
{
    var i = SysCfg.IDI.Properties.IDITable_ListGetIndex(IDITableList, IDITable.ID);
    if (i == -1)
    {
        if (isServer)
        {
            SysCfg.IDI.Properties.DataSetCreate(IDITable.SPANICH_IDIDataSet);
            SysCfg.IDI.Properties.DataSetCreate(IDITable.ENGLISH_IDIDataSet);
            SysCfg.IDI.Properties.DataSetCreate(IDITable.PORTUGUESE_IDIDataSet);                                      
        }
        IDITableList.push(IDITable);
    }
    else IDITable = IDITableList[i];
}

SysCfg.IDI.Properties.IDITable_ListGetIndex = function(IDITableList,ID)
{
    for (var i = 0; i < IDITableList.length; i++)
    {
        if (ID == IDITableList[i].ID)
            return (i);

    }
    return (-1);
}


SysCfg.IDI.Properties.IDIRow_ListAdd = function(IDIRowList,IDIRow)
{
    var i = SysCfg.IDI.Properties.IDIRow_ListGetIndex(IDIRowList, IDIRow.IDSUB);
    if (i == -1) IDIRowList.push(IDIRow);
    else IDIRow = IDIRowList[i];
}


SysCfg.IDI.Properties.IDIRow_ListGetIndex = function(IDIRowList,IDSUB)
{
    for (var i = 0; i < IDIRowList.length; i++)
    {
        if (IDSUB == IDIRowList[i].IDSUB)
            return (i);

    }
    return (-1);
}

SysCfg.IDI.Properties.DataSetCreate = function (DataSet) {
    DataSet = new SysCfg.MemTable.TDataSet();
    DataSet.Inicialize(false);
    DataSet.AddFields("IDSUB", SysCfg.DB.Properties.TDataType.String, 10);
    DataSet.AddFields("LangText", SysCfg.DB.Properties.TDataType.Text, 255);
}

SysCfg.IDI.Properties.AddRow = function (DataSet, IDSUB, LangText) {
    DataSet.Append();
    DataSet.Fields(0, IDSUB);
    DataSet.Fields(1, LangText);
    DataSet.Post();
}

