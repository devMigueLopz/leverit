﻿//SysCfg.IDI.Client


SysCfg.IDI.Client.GetLangText = function(Application,ID,IDSUB,LangText,Language)
{
    if (SysCfg.IDI.Properties.EnableTraslate)
    {

        var idx1 = SysCfg.IDI.Properties.IDIApplication_ListGetIndex(SysCfg.IDI.Properties.IDIApplicationList, Application);
        if (idx1 != -1)
        {
            var idx2 = SysCfg.IDI.Properties.IDITable_ListGetIndex(SysCfg.IDI.Properties.IDIApplicationList[idx1].IDITableList, ID);
            if (idx2 != -1)
            {
                var idx3 = SysCfg.IDI.Properties.IDIRow_ListGetIndex(SysCfg.IDI.Properties.IDIApplicationList[idx1].IDITableList[idx2].IDIRowList, IDSUB);
                if (idx3 != -1)
                {
                    LangText = SysCfg.IDI.Properties.IDIApplicationList[idx1].IDITableList[idx2].IDIRowList[idx3].LangText;
                }
                else
                {
                    var IDIRow = new SysCfg.IDI.Properties.TIDIRow();
                    IDIRow.IDSUB = IDSUB;
                    IDIRow.LangText = LangText;
                    SysCfg.IDI.Properties.IDIRow_ListAdd(SysCfg.IDI.Properties.IDIApplicationList[idx1].IDITableList[idx2].IDIRowList,  IDIRow);
                    Comunic.AjaxJson.Client.Methods.GetIDI(Application, ID, IDSUB, LangText, Comunic.Properties.TIDIType._SETLangText, Language);//SysCfg.App.Properties.Config_Language.LanguageITF
                    

                    return LangText;
                }

            }
            else
            {
                var IDITable = new SysCfg.IDI.Properties.TIDITable();
                IDITable.ID = ID;
                SysCfg.IDI.Properties.IDITable_ListAdd(SysCfg.IDI.Properties.IDIApplicationList[idx1].IDITableList, IDITable, false);
                BytetoDataSet = new SysCfg.MemTable.Properties.TBytetoDataSet();
                SysCfg.Error.Properties.ResErrfill(BytetoDataSet.ResErr);
                IDIByte = Comunic.AjaxJson.Client.Methods.GetIDI(Application, ID, IDSUB, LangText, Comunic.Properties.TIDIType._GETIDITable, Language).IDIByte;// SysCfg.App.Properties.Config_Language.LanguageITF
                BytetoDataSet = SysCfg.MemTable.Properties.BytetoDataSet(IDIByte);
                if (BytetoDataSet.ResErr.NotError)
                {
                    var DS = BytetoDataSet.DataSet;
                    DS.First();
                    while (!(DS.Eof))
                    {
                        var IDIRow = new SysCfg.IDI.Properties.TIDIRow();
                        IDIRow.IDSUB = DS.RecordSet.FieldName("IDSUB").asString();
                        IDIRow.LangText = DS.RecordSet.FieldName("LangText").asString();
                        SysCfg.IDI.Properties.IDIRow_ListAdd(IDITable.IDIRowList,  IDIRow);
                        DS.Next();
                    }
                }
                return SysCfg.IDI.Client.GetLangText(Application, ID, IDSUB, LangText, Language);
            }

        }
        else
        {
            var IDIApplication = new SysCfg.IDI.Properties.TIDIApplication();
            IDIApplication.Application = Application;
            SysCfg.IDI.Properties.IDIApplication_ListAdd(SysCfg.IDI.Properties.IDIApplicationList, IDIApplication);
            return SysCfg.IDI.Client.GetLangText(Application, ID, IDSUB, LangText, Language);

        }
    }
    return LangText;
}


