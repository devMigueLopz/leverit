﻿

SysCfg.App.TUser = function () {    
    this.ResErr = new SysCfg.Error.Properties.TResErr();
    this.ROLENAME = "";
    this.IDATROLE = 0;
    this.IDCMDBUSER = 0;
    this.PASSWORD = "";
    this.LASTPWDCHANGE = new Date(1970, 1, 1, 0, 0, 0);
    this.IDMDCALENDARYDATE = 0;
    this.IDCMDBCI = 0;
    this.CI_GENERICNAME = "";
    this.EXPIRED_PASSWORD = false;
    this.EXPIRED_DAYS = 0;
    this.OLD_GENERICNAME = new Array(9);
    this.CMDBUSERToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.ROLENAME);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDATROLE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDCMDBUSER);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.PASSWORD);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.LASTPWDCHANGE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDCALENDARYDATE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDCMDBCI);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CI_GENERICNAME);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.EXPIRED_PASSWORD);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.EXPIRED_DAYS);
    }
    this.ByteToCMDBUSER = function (MemStream) {
        this.ROLENAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDATROLE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDCMDBUSER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.PASSWORD = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.LASTPWDCHANGE = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.IDMDCALENDARYDATE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDCMDBCI = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);        
        this.CI_GENERICNAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.EXPIRED_PASSWORD = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.EXPIRED_DAYS = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    }
}

SysCfg.App.TPrefix = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    Unknown: { value: 0, name: "Unknown" },
    SetupBox: { value: 1, name: "SetupBox" },
    DataLink: { value: 2, name: "DataLink" },
    Atis: { value: 3, name: "Atis" },
    CMDB: { value: 4, name: "CMDB" },
    DRobot: { value: 5, name: "DRobot" },
    ServiceManager: { value: 6, name: "Common" },
    }

SysCfg.App.TApplication = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    Unknown: { value: 0, name: "Unknown" },
    SetupBox: { value: 1, name: "SetupBox" },
    DataLink: { value: 2, name: "DataLink" },
    Atis: { value: 3, name: "Atis" },
    CMDB: { value: 4, name: "CMDB" },
    DRobot: { value: 5, name: "DRobot" },
    ServiceManager: { value: 6, name: "ServiceManager" },
    SetupIDI: { value: 7, name: "SetupIDI" },
    Common: { value: 8, name: "Common" },
    ITHelpCenter: { value: 9, name: "ITHelpCenter" },
};



//**************  SysCfg.App.Properties (LOAD) ***********************
SysCfg.App.Properties.Application = SysCfg.App.TApplication.Unknown;
SysCfg.App.Properties._SysCfgAppVersion = "3.2.0.0";        
SysCfg.App.Properties._DirCfg = "SrvCfg";
SysCfg.App.Properties._SrvFlb = "SrvFlb";
SysCfg.App.Properties.NoDeploy = true;    
SysCfg.App.Properties.TConfig_Root = function () {
    this.AppPath = "";
    this.AppPathSoftware = "";
    this.AppPathCfg = "";
    this.AppPathFiles = "";
    this.SDNotify = "";
    this.RutaemailIncoming = "";
    this.RutaemailReply = "";
    this.RutaemailSDNotify = "";
    this.RutaemailCertificate = "";
    this.Rutaemail = "";
    this.SDAttachPath = "";
    this.SDPhotoPath = "";
    this.CIDefinePath = "";

    this.AtentionLayoutPath = "";
    this.DashBoardLayoutPath = "";
    this.ReportTemplate = "";
    this.ReportTemporal = "";
    this.IO_BDTemplates = "";
}
SysCfg.App.Properties.Config_Root = new SysCfg.App.Properties.TConfig_Root();

SysCfg.App.Properties.TLanguage = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },    
    SPANICH: { value: 0, name: "SPANICH" },    
    ENGLISH: { value: 1, name: "ENGLISH" },    
    PORTUGUESE: { value: 2, name: "PORTUGUESE" }
}

SysCfg.App.Properties.TConfig_Language = function () {    
    this.Language = SysCfg.App.Properties.TLanguage.ENGLISH;
    this.LanguageITF = SysCfg.App.Properties.TLanguage.ENGLISH;
    this.Config_LanguageToByte= function(MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamInt16(MemStream, this.Language.value);
        SysCfg.Stream.Methods.WriteStreamInt16(MemStream, this.LanguageITF.value);
    }
    this.ByteToConfig_Language = function(MemStream)
    {
        this.Language = SysCfg.App.Properties.TLanguage.GetEnum(SysCfg.Stream.Methods.ReadStreamInt16(MemStream));
        this.LanguageITF = SysCfg.App.Properties.TLanguage.GetEnum(SysCfg.Stream.Methods.ReadStreamInt16(MemStream));
    }
}
SysCfg.App.Properties.Config_Language = new SysCfg.App.Properties.TConfig_Language();


SysCfg.App.Properties.TConfig_RemoteHelp = function () {
    this.CR_ChatInternet = false;
    this.Config_RemoteHelpToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.CR_ChatInternet);
    }
    this.ByteToConfig_RemoteHelp = function (MemStream) {
        this.CR_ChatInternet = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
    }
}
SysCfg.App.Properties.Config_RemoteHelp = new SysCfg.App.Properties.TConfig_RemoteHelp();


SysCfg.App.TDevice = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    Unknown: { value: 0, name: "None" },
    Desktop: { value: 1, name: "Desktop" },
    Movile: { value: 2, name: "Movile" },
    Tablet: { value: 3, name: "Tablet" },
}
SysCfg.App.Properties.Device = SysCfg.App.TDevice.GetEnum(undefined);



SysCfg.App.Properties.DataDomain = new SysCfg.Domain.TDataDomain();