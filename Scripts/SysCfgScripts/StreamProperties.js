﻿

SysCfg.Stream.Properties.TParam = function () {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.FieldCount;
    this.RecordCount;
    this.MemStreamFields;
    this.MemStreamRecord;
    this.MemStream;
    //_this.Inicialize();
}
SysCfg.Stream.Properties.TParam.prototype.Inicialize = function () {
    var _this = this.TParent();
    _this.MemStreamFields = new SysCfg.Stream.TMemoryStream()
    _this.MemStreamRecord = new SysCfg.Stream.TMemoryStream()
    _this.MemStream = new SysCfg.Stream.TMemoryStream()
    _this.FieldCount = 0;
    _this.RecordCount = 1; 
}
SysCfg.Stream.Properties.TParam.prototype.AddString = function (FieldName, TString, Motor, Extra, Style) {
    var _this = this.TParent();
    _this.AddMemStmFields(FieldName, SysCfg.DB.Properties.TDataType.String, 255, Motor, Extra, Style);
    SysCfg.Stream.Methods.WriteStreamStrInt16(_this.MemStreamRecord, TString);
}
SysCfg.Stream.Properties.TParam.prototype.AddText = function (FieldName, TString, Motor, Extra, Style) {
    var _this = this.TParent();
    _this.AddMemStmFields(FieldName, SysCfg.DB.Properties.TDataType.Text, 4000, Motor, Extra, Style);
    SysCfg.Stream.Methods.WriteStreamStrInt16(_this.MemStreamRecord, TString);
}
SysCfg.Stream.Properties.TParam.prototype.AddInt32 = function (FieldName, TInt32, Motor, Extra, Style) {
    var _this = this.TParent();
    _this.AddMemStmFields(FieldName, SysCfg.DB.Properties.TDataType.Int32, 4, Motor, Extra, Style);
    SysCfg.Stream.Methods.WriteStreamInt32(_this.MemStreamRecord, TInt32);
}
SysCfg.Stream.Properties.TParam.prototype.AddDouble = function (FieldName, TDouble, Motor, Extra, Style) {
    var _this = this.TParent();
    _this.AddMemStmFields(FieldName, SysCfg.DB.Properties.TDataType.Double, 8, Motor, Extra, Style);
    SysCfg.Stream.Methods.WriteStreamDouble(_this.MemStreamRecord, TDouble);
}
SysCfg.Stream.Properties.TParam.prototype.AddDecimal = function (FieldName, TDecimal, Motor, Extra, Style) {
    var _this = this.TParent();
    _this.AddMemStmFields(FieldName, SysCfg.DB.Properties.TDataType.Decimal, 8, Motor, Extra, Style);
    SysCfg.Stream.Methods.WriteStreamDecimal(_this.MemStreamRecord, TDecimal);
}
SysCfg.Stream.Properties.TParam.prototype.AddBoolean = function (FieldName, TBoolean, Motor, Extra, Style) {
    var _this = this.TParent();
    _this.AddMemStmFields(FieldName, SysCfg.DB.Properties.TDataType.Boolean, 1, Motor, Extra, Style);
    SysCfg.Stream.Methods.WriteStreamBoolean(_this.MemStreamRecord, TBoolean);
}
SysCfg.Stream.Properties.TParam.prototype.AddDateTime = function (FieldName, TDateTime, Motor, Extra, Style) {
    var _this = this.TParent();
    _this.AddMemStmFields(FieldName, SysCfg.DB.Properties.TDataType.DateTime, 255, Motor, Extra, Style);
    SysCfg.Stream.Methods.WriteStreamDateTime(_this.MemStreamRecord, TDateTime);
}
SysCfg.Stream.Properties.TParam.prototype.AddUnknown = function (FieldName, TString, Motor, Extra, Style) {
    var _this = this.TParent();
    _this.AddMemStmFields(FieldName, SysCfg.DB.Properties.TDataType.Unknown, 4000, Motor, Extra, Style);
    SysCfg.Stream.Methods.WriteStreamStrInt16(_this.MemStreamRecord, TString);
}

SysCfg.Stream.Properties.TParam.prototype.Destroy = function ()
{
    var _this = this.TParent();
    if (_this.MemStreamFields != null)
    {
        _this.MemStreamFields.Close();
        _this.MemStreamFields.Dispose();
    }
    if (_this.MemStreamRecord != null)
    {
        _this.MemStreamRecord.Close();
        _this.MemStreamRecord.Dispose();
    }
    if (_this.MemStream != null)
    {
        _this.MemStream.Close();
        _this.MemStream.Dispose();
    }
}
SysCfg.Stream.Properties.TParam.prototype.AddMemStmFields = function (FieldName, DataType,FieldSize,Motor,Extra,Style)
{
    var _this = this.TParent();
    _this.FieldCount ++;
    SysCfg.Stream.Methods.WriteStreamStrInt16(_this.MemStreamFields, FieldName);
    SysCfg.Stream.Methods.WriteStreamInt16(_this.MemStreamFields, DataType.value);
    SysCfg.Stream.Methods.WriteStreamInt16(_this.MemStreamFields, FieldSize);
    SysCfg.Stream.Methods.WriteStreamByte(_this.MemStreamFields, Motor.value);//*
    SysCfg.Stream.Methods.WriteStreamByte(_this.MemStreamFields, Extra.value);//*
    SysCfg.Stream.Methods.WriteStreamByte(_this.MemStreamFields, Style.value);//*
}

SysCfg.Stream.Properties.TParam.prototype.ToBytes = function ()
{
    var _this = this.TParent();
    if (this.MemStream.Length() > 0) this.MemStream = new SysCfg.Stream.TMemoryStream();
    var Postemp1 = _this.MemStreamFields.Position;
    var Postemp2 = _this.MemStreamRecord.Position;
    SysCfg.Stream.Methods.WriteStreamInt16(_this.MemStream, _this.FieldCount);
    SysCfg.Stream.Methods.WriteStreamInt32(_this.MemStream, _this.RecordCount);
    _this.MemStreamFields.Position = 0;
    //byteFields = new Array(_this.MemStreamFields.Length);
    var byteFields = _this.MemStreamFields.ToArray();
    _this.MemStream.Write(byteFields, 0, byteFields.length);
    //byteRecord = new Array(_this.MemStreamRecord.Length);
    var byteRecord = _this.MemStreamRecord.ToArray();
    _this.MemStream.Write(byteRecord, 0, byteRecord.length);
    _this.MemStreamFields.Position = Postemp1;
    _this.MemStreamRecord.Position = Postemp2;
    return (_this.MemStream.ToArray());
}
   
  


