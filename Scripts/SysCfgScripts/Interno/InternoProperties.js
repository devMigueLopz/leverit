﻿


SysCfg.Interno.TBuildType = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    Complte: { value: 0, name: "Complte"},
    WithoutSQL: { value: 1, name: "WithoutSQL"}
}

SysCfg.Interno.TTypeAttribute = { 
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    NOP: { value: 0, name: "NOP"},
    SLA: { value: 1, name: "SLA"},
    LOG: { value: 2, name: "LOG"},
    VAR: { value: 3, name: "VAR"},
    NEW: { value: 4, name: "NEW"},
    ADN: { value: 5, name: "ADN"},
    INS: { value: 6, name: "INS"},
    EXT: { value: 7, name: "EXT"},
}


SysCfg.Interno.TTypeRelation = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _UNIQUE: { value: 1, name: "UNIQUE"},
    _FIELD: { value: 2, name: "FIELD"},
    _REFERENCE: { value: 3, name: "REFERENCE"},
}

SysCfg.Interno.TLineInterno= function () {
    this.isSuccessfull = false;
    this.isEmpty = false;
    this.isAutoInc = false;
    this.isSLA = false;
    this.isLOG = false;
    this.isVAR = false;
    this.isNEW = false;
    this.isADN = false;
    this.isINS = false;
    this.isEXT = false;
    this.LineInst = "";
    this.DataType =  SysCfg.DB.Properties.TDataType.Unknown;
    this.PartA_Atribute="";
    this.PartA_Relation="";
    this.SysCfg.Interno.TTypeRelation.PartA_TypeRelation = TTypeRelation._FIELD;    
    //lista
    this.PartA_TypeAttributeList = new Array();       
    this.PartB_TableName="";
    this.PartB_isRelation=false;       
    this.PartB_RelationFildName="";
    this.PartB_RelationTableName="";
    this.PartC_FieldName="";
    this.PartC_ExParamA="";
    this.PartC_ExParamB="";
    this.PartC_ExParamC="";
    this.PartC_IsConstraint = false;
    this.PartC_ConstraintName = "";
    this.PartC_ConstraintPartSQL = "";
    this.PartC_PartSQL = "";
    this.PartC_CompSQL = "";  
    this.PartC_Bytes=0;

    this.ToByte = function (MemStream) {

        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.isSuccessfull);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.isEmpty);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.isSLA);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.isLOG);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.isVAR);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.isNEW);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.isADN);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.isINS);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.isEXT);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.LineInst);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.DataType.value);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.PartA_Atribute);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.PartA_Relation);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.PartA_TypeRelation.value);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.PartA_TypeAttributeList.length);

    for (var i = 0; i < PartA_TypeAttributeList.Count() ; i++) SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.PartA_TypeAttributeList[i].value);
    SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.PartB_TableName);
    SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.PartB_isRelation);
    SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.PartB_RelationFildName);
    SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.PartB_RelationTableName);
    SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.PartC_FieldName);
    SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.PartC_ExParamA);
    SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.PartC_ExParamB);
    SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.PartC_ExParamC);
    SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.PartC_IsConstraint);
    SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.PartC_ConstraintName);
    SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.PartC_ConstraintPartSQL);
    SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.PartC_PartSQL);
    SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.PartC_CompSQL);
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.PartC_Bytes);
    }
    this.ByteTo = function (MemStream) {
        this.isSuccessfull = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.isEmpty = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.isSLA = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.isLOG = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.isVAR = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.isNEW = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.isADN = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.isINS = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.isEXT = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.LineInst = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.DataType = DB.Properties.TDataType.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        this.PartA_Atribute = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.PartA_Relation = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.PartA_TypeRelation = SysCfg.Interno.TTypeRelation.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);            
for (var i = 0; i < Count; i++) this.PartA_TypeAttributeList.push(SysCfg.Interno.TTypeAttribute.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream)));
this.PartB_TableName = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
this.PartB_isRelation = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
this.PartB_RelationFildName = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
this.PartB_RelationTableName = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
this.PartC_FieldName = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
this.PartC_ExParamA = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
this.PartC_ExParamB = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
this.PartC_ExParamC = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
this.PartC_IsConstraint = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
this.PartC_ConstraintName = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
this.PartC_ConstraintPartSQL = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
this.PartC_PartSQL = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
this.PartC_CompSQL = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
this.PartC_Bytes = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
}
}




