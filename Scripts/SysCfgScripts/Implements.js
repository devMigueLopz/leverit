﻿//https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Object/getOwnPropertyDescriptor
Object.defineProperty(Object.prototype, "Implements", {
    value: function () {
        var i = arguments.length;
        while (i--) {
            var Padre = arguments[i];
            for (var property in Padre) {
                //Object.defineProperty(this.prototype, property, Object.getOwnPropertyDescriptor(Padre, property));
                if (!!this.prototype && !this.prototype.hasOwnProperty(property)) {
                    //this.prototype[property] = Padre[property];
                    if(Object.getOwnPropertyDescriptor(Padre, property) != undefined)
                    {
                        if (Object.getOwnPropertyDescriptor(Padre, property).value != undefined)
                            this.prototype[property] = Object.getOwnPropertyDescriptor(Padre, property).value;
                        else {
                            //Object.defineProperty(this.prototype, property, { get: Object.getOwnPropertyDescriptor(Padre, property).get, set: Object.getOwnPropertyDescriptor(Padre, property).set, enumerable: true });
                            Object.defineProperty(this.prototype, property, Object.getOwnPropertyDescriptor(Padre, property));
                            //this.prototype[property] = Padre[property];
                            //this.prototype[property]["get"] = Object.getOwnPropertyDescriptor(Padre, property)["get"];
                            //this.prototype[property]["set"] = Object.getOwnPropertyDescriptor(Padre, property)["set"];
                        }
                    }
                    else
                        this.prototype[property] = Padre[property];
                }
            }
        }
        return this;
    },
    enumerable: false
});