﻿
//Agregar intancia del servidor 

SysCfg.Log.Methods.WriteLog = function () {
    if (SysCfg.Log.Properties.EnableLog) {
        var Outmessage = "";
        var UnDate = SysCfg.DB.Properties.SVRNOW();
        try {                        
            if (arguments.length == 1) {
                var message = arguments[0];
                Outmessage = SysCfg.DateTimeMethods.ToDateTimefull(UnDate) + "  " + message;
            }
            else if (arguments.length == 2) {
                var message = arguments[0];
                var ex = arguments[1];
                if (ex == undefined) {
                    Outmessage = SysCfg.DateTimeMethods.ToDateTimefull(UnDate) + "  " + message;
                    //Instance.WriteLineLog(message);
                }
                else {
                    Outmessage = SysCfg.DateTimeMethods.ToDateTimefull(UnDate) + "  " + message + " Exception:" + ex;
                    //Instance.WriteLineLog(message);
                }
            }
            else {
                var message = arguments[0];
                Outmessage = SysCfg.DateTimeMethods.ToDateTimefull(UnDate) + "  " + message;
            }
        }
        catch (e) {
            var Outmessage = "Log Error";
        }
        console.log(Outmessage);
    }
}


//SysCfg.Log.Methods.WriteLog