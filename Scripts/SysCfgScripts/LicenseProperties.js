﻿      
      SysCfg.License.Properties.TContLic = function()
      {
          this.SerialValido = false;
          this.mensaje = "";
          this.mensaje2 = "";
          this.num_pcs = 0;
          this.num_edit = 0;
          this.num_Consultas = 0;
          this.num_adms = 0;
          this.num_visor = 0;
          //Funciones
          this._6ExportaHTML = false;
          this._7ExportaEXCEL = false;
          this._8OpBuscar = false;
          this._9OpAutoAjustar = false;
          this._10GraficadorAutomatico = false;
          this._11ManipulaTablas = false;                    // (Drag an Drop Filters)
          this._12InstalableSobreMSSQL = false;              //  Base  Datos
          this._13InstalableSobreAcces = false;              // (ya incluida)
          this._14EditorSQL = false;
          this._15AlarmaCambios = false;
          this._16ConfiguraAlarmaCambios = false;
          this._17AccesoExternoaBaseDatos = false;
          //Informes
          this._18TablaConsultasSQL = false;
          this._19TablaAdminisEjecutiva = false;             // (Generales)
          this._20TablaInforma = false;                      // (Todos)
          //Certificados
          this._21EdiPlantillas = false;
          //AdminisGeneral
          this._22GeneraAdminisContraYSeguirdad = false;
          //OpcionesAdministrativas
          this._23RestriHabiAccesoAdminis = false;
          this._24RestriHabiAdminisControlRemoto = false;
          this._25RestriHabiAccesoControlRemoto = false;
          this._26RestriHabiEdiLogSesionRemot = false;
          this._27RestriHabiGeneraReportesSQL = false;
          this._28RestriHabiAccesoTraductSoftware = false;
          //AdminisControlRemoto
          this._29DefiniGruposUsuarios = false;
          this._30RestriAccesoDeAdminisAUsrRemotos = false;
          //EditorTablas
          this._31EditorTablaConsultas = false;                      //(SQL)
          this._32EditorTablaEjectuvia = false;                      //(Generales)
          this._33editorreportes = false;
          this._34editoreditable = false;
          //AyudaRemota
          this._35AyudaRemota = false;
          this._36LogAyudaRemota = false;
          //SistemaMensajeria
          this._37EnvioMensajes = false;
          this._38ConversaporChat = false;
          //SistemaControlRemoto
          this._39Noporsinpedirpermiso = false;
          this._40OpSoloMonitoreo = false;
          this._41BloqueoRatonTeclado = false;
          //OperacionesRemotas
          this._42PetiInventariosenLinea = false;
          this._43ActualizaAgente = false;
          this._44CambiarNombreID = false;
          this._45BusquedaArchivosPeligrosos = false;
          //EnviosComandos
          this._46EnviarArchivosProgramasOrnes = false;
          this._47EjecutarComandosRemotamente = false;
          this._48Traductor = false;
          this._59HelpDesk = false;

          this._60Relojes = false;
          this._61Slas = false;
          this._62Adjuntos = false;
          this._63Videos = false;
          this._64Subtemas = false;
          this._70TipoVersion = false;
          this._71NumLicWeb = 0;
          this._72DeshabilitaControlRemoto = false;
          this._73AdmonProblemas = false;
      }

      SysCfg.License.Properties.ContLic = new SysCfg.License.Properties.TContLic();

      SysCfg.License.Properties.GetContLic = function()
      {
         return (SysCfg.License.Properties.ContLic);
      }

      SysCfg.License.Properties.SerialValido = function(ShowMsg)
      {
          if ((ShowMsg) && (SysCfg.License.Properties.ContLic.SerialValido == false)) SysCfg.App.Methods.ShowMessage("Invalid User Name and Password.");
         return (SysCfg.License.Properties.ContLic.SerialValido);
         
      }

      SysCfg.License.Properties.Create = function()
      {
         SysCfg.License.Properties.ContLic.SerialValido = false;
         SysCfg.License.Properties.ContLic.mensaje = "";
         SysCfg.License.Properties.ContLic.num_pcs = 0;
         SysCfg.License.Properties.ContLic.num_edit = 0;
         SysCfg.License.Properties.ContLic.num_Consultas = 0;
         SysCfg.License.Properties.ContLic.num_adms = 0;
         SysCfg.License.Properties.ContLic.num_visor = 0;
         //Funciones
         SysCfg.License.Properties.ContLic._6ExportaHTML = false;
         SysCfg.License.Properties.ContLic._7ExportaEXCEL = false;
         SysCfg.License.Properties.ContLic._8OpBuscar = false;
         SysCfg.License.Properties.ContLic._9OpAutoAjustar = false;
         SysCfg.License.Properties.ContLic._10GraficadorAutomatico = false;
         SysCfg.License.Properties.ContLic._11ManipulaTablas = false;                    // (Drag an Drop Filters)
         SysCfg.License.Properties.ContLic._12InstalableSobreMSSQL = false;              //  Base  Datos
         SysCfg.License.Properties.ContLic._13InstalableSobreAcces = false;              // (ya incluida)
         SysCfg.License.Properties.ContLic._14EditorSQL = false;
         SysCfg.License.Properties.ContLic._15AlarmaCambios = false;
         SysCfg.License.Properties.ContLic._16ConfiguraAlarmaCambios = false;
         SysCfg.License.Properties.ContLic._17AccesoExternoaBaseDatos = false;
         //Informes
         SysCfg.License.Properties.ContLic._18TablaConsultasSQL = false;
         SysCfg.License.Properties.ContLic._19TablaAdminisEjecutiva = false;             // (Generales)
         SysCfg.License.Properties.ContLic._20TablaInforma = false;                      // (Todos)
         //Certificados
         SysCfg.License.Properties.ContLic._21EdiPlantillas = false;
         //AdminisGeneral
         SysCfg.License.Properties.ContLic._22GeneraAdminisContraYSeguirdad = false;
         //OpcionesAdministrativas
         SysCfg.License.Properties.ContLic._23RestriHabiAccesoAdminis = false;
         SysCfg.License.Properties.ContLic._24RestriHabiAdminisControlRemoto = false;
         SysCfg.License.Properties.ContLic._25RestriHabiAccesoControlRemoto = false;
         SysCfg.License.Properties.ContLic._26RestriHabiEdiLogSesionRemot = false;
         SysCfg.License.Properties.ContLic._27RestriHabiGeneraReportesSQL = false;
         SysCfg.License.Properties.ContLic._28RestriHabiAccesoTraductSoftware = false;
         //AdminisControlRemoto
         SysCfg.License.Properties.ContLic._29DefiniGruposUsuarios = false;
         SysCfg.License.Properties.ContLic._30RestriAccesoDeAdminisAUsrRemotos = false;
         //EditorTablas
         SysCfg.License.Properties.ContLic._31EditorTablaConsultas = false;                      //(SQL)
         SysCfg.License.Properties.ContLic._32EditorTablaEjectuvia = false;                      //(Generales)
         SysCfg.License.Properties.ContLic._33editorreportes = false;
         SysCfg.License.Properties.ContLic._34editoreditable = false;
         //AyudaRemota
         SysCfg.License.Properties.ContLic._35AyudaRemota = false;
         SysCfg.License.Properties.ContLic._36LogAyudaRemota = false;
         //SistemaMensajeria
         SysCfg.License.Properties.ContLic._37EnvioMensajes = false;
         SysCfg.License.Properties.ContLic._38ConversaporChat = false;
         //SistemaControlRemoto
         SysCfg.License.Properties.ContLic._39Noporsinpedirpermiso = false;
         SysCfg.License.Properties.ContLic._40OpSoloMonitoreo = false;
         SysCfg.License.Properties.ContLic._41BloqueoRatonTeclado = false;
         //OperacionesRemotas
         SysCfg.License.Properties.ContLic._42PetiInventariosenLinea = false;
         SysCfg.License.Properties.ContLic._43ActualizaAgente = false;
         SysCfg.License.Properties.ContLic._44CambiarNombreID = false;
         SysCfg.License.Properties.ContLic._45BusquedaArchivosPeligrosos = false;
         //EnviosComandos
         SysCfg.License.Properties.ContLic._46EnviarArchivosProgramasOrnes = false;
         SysCfg.License.Properties.ContLic._47EjecutarComandosRemotamente = false;
         SysCfg.License.Properties.ContLic._48Traductor = false;
         SysCfg.License.Properties.ContLic._59HelpDesk = false;

         SysCfg.License.Properties.ContLic._60Relojes = false;
         SysCfg.License.Properties.ContLic._61Slas = false;
         SysCfg.License.Properties.ContLic._62Adjuntos = false;
         SysCfg.License.Properties.ContLic._63Videos = false;
         SysCfg.License.Properties.ContLic._64Subtemas = false;
         SysCfg.License.Properties.ContLic._70TipoVersion = false;
         SysCfg.License.Properties.ContLic._71NumLicWeb = 0;
         SysCfg.License.Properties.ContLic._72DeshabilitaControlRemoto = false;
         SysCfg.License.Properties.ContLic._73AdmonProblemas = false;
      }

      SysCfg.License.Properties.ContLicToByte = function(MemStream,ContLicSurce)
      {
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce.SerialValido);
         SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, ContLicSurce.mensaje);
         SysCfg.Stream.Methods.WriteStreamInt32(MemStream, ContLicSurce.num_pcs);
         SysCfg.Stream.Methods.WriteStreamInt32(MemStream, ContLicSurce.num_edit);
         SysCfg.Stream.Methods.WriteStreamInt32(MemStream, ContLicSurce.num_Consultas);
         SysCfg.Stream.Methods.WriteStreamInt32(MemStream, ContLicSurce.num_adms);
         SysCfg.Stream.Methods.WriteStreamInt32(MemStream, ContLicSurce.num_visor);
         //Funciones
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._6ExportaHTML);
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._7ExportaEXCEL);
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._8OpBuscar);
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._9OpAutoAjustar);
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._10GraficadorAutomatico);
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._11ManipulaTablas);                    // (Drag an Drop Filters)
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._12InstalableSobreMSSQL);              //  Base  Datos
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._13InstalableSobreAcces);              // (ya incluida)
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._14EditorSQL);
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._15AlarmaCambios);
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._16ConfiguraAlarmaCambios);
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._17AccesoExternoaBaseDatos);
         //Informes
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._18TablaConsultasSQL);
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._19TablaAdminisEjecutiva);             // (Generales)
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._20TablaInforma);                      // (Todos)
         //Certificados
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._21EdiPlantillas);
         //AdminisGeneral
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._22GeneraAdminisContraYSeguirdad);
         //OpcionesAdministrativas
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._23RestriHabiAccesoAdminis);
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._24RestriHabiAdminisControlRemoto);
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._25RestriHabiAccesoControlRemoto);
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._26RestriHabiEdiLogSesionRemot);
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._27RestriHabiGeneraReportesSQL);
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._28RestriHabiAccesoTraductSoftware);
         //AdminisControlRemoto
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._29DefiniGruposUsuarios);
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._30RestriAccesoDeAdminisAUsrRemotos);
         //EditorTablas
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._31EditorTablaConsultas);                      //(SQL)
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._32EditorTablaEjectuvia);                      //(Generales)
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._33editorreportes);
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._34editoreditable);
         //AyudaRemota
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._35AyudaRemota);
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._36LogAyudaRemota);
         //SistemaMensajeria
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._37EnvioMensajes);
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._38ConversaporChat);
         //SistemaControlRemoto
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._39Noporsinpedirpermiso);
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._40OpSoloMonitoreo);
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._41BloqueoRatonTeclado);
         //OperacionesRemotas
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._42PetiInventariosenLinea);
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._43ActualizaAgente);
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._44CambiarNombreID);
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._45BusquedaArchivosPeligrosos);
         //EnviosComandos
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._46EnviarArchivosProgramasOrnes);
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._47EjecutarComandosRemotamente);
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._48Traductor);
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._59HelpDesk);

         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._60Relojes);
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._61Slas);
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._62Adjuntos);
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._63Videos);
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._64Subtemas);
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._70TipoVersion);
         SysCfg.Stream.Methods.WriteStreamInt32(MemStream, ContLicSurce._71NumLicWeb);         
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._72DeshabilitaControlRemoto);
         SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ContLicSurce._73AdmonProblemas);
      }

      SysCfg.License.Properties.ByteToContLic = function(MemStream)
      {
         var ContLicSurce = new TContLic();
         ContLicSurce.SerialValido = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         ContLicSurce.mensaje = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
         ContLicSurce.num_pcs = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
         ContLicSurce.num_edit = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
         ContLicSurce.num_Consultas = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
         ContLicSurce.num_adms = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
         ContLicSurce.num_visor = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
         //Funciones
         ContLicSurce._6ExportaHTML = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         ContLicSurce._7ExportaEXCEL = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         ContLicSurce._8OpBuscar = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         ContLicSurce._9OpAutoAjustar = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         ContLicSurce._10GraficadorAutomatico = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         ContLicSurce._11ManipulaTablas = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);                    // (Drag an Drop Filters)
         ContLicSurce._12InstalableSobreMSSQL = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);              //  Base  Datos
         ContLicSurce._13InstalableSobreAcces = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);              // (ya incluida)
         ContLicSurce._14EditorSQL = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         ContLicSurce._15AlarmaCambios = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         ContLicSurce._16ConfiguraAlarmaCambios = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         ContLicSurce._17AccesoExternoaBaseDatos = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         //Informes
         ContLicSurce._18TablaConsultasSQL = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         ContLicSurce._19TablaAdminisEjecutiva = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);             // (Generales)
         ContLicSurce._20TablaInforma = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);                      // (Todos)
         //Certificados
         ContLicSurce._21EdiPlantillas = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         //AdminisGeneral
         ContLicSurce._22GeneraAdminisContraYSeguirdad = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         //OpcionesAdministrativas
         ContLicSurce._23RestriHabiAccesoAdminis = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         ContLicSurce._24RestriHabiAdminisControlRemoto = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         ContLicSurce._25RestriHabiAccesoControlRemoto = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         ContLicSurce._26RestriHabiEdiLogSesionRemot = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         ContLicSurce._27RestriHabiGeneraReportesSQL = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         ContLicSurce._28RestriHabiAccesoTraductSoftware = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         //AdminisControlRemoto
         ContLicSurce._29DefiniGruposUsuarios = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         ContLicSurce._30RestriAccesoDeAdminisAUsrRemotos = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         //EditorTablas
         ContLicSurce._31EditorTablaConsultas = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);                      //(SQL)
         ContLicSurce._32EditorTablaEjectuvia = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);                      //(Generales)
         ContLicSurce._33editorreportes = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         ContLicSurce._34editoreditable = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         //AyudaRemota
         ContLicSurce._35AyudaRemota = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         ContLicSurce._36LogAyudaRemota = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         //SistemaMensajeria
         ContLicSurce._37EnvioMensajes = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         ContLicSurce._38ConversaporChat = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         //SistemaControlRemoto
         ContLicSurce._39Noporsinpedirpermiso = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         ContLicSurce._40OpSoloMonitoreo = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         ContLicSurce._41BloqueoRatonTeclado = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         //OperacionesRemotas
         ContLicSurce._42PetiInventariosenLinea = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         ContLicSurce._43ActualizaAgente = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         ContLicSurce._44CambiarNombreID = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         ContLicSurce._45BusquedaArchivosPeligrosos = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         //EnviosComandos
         ContLicSurce._46EnviarArchivosProgramasOrnes = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         ContLicSurce._47EjecutarComandosRemotamente = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         ContLicSurce._48Traductor = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         ContLicSurce._59HelpDesk = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);

         ContLicSurce._60Relojes = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         ContLicSurce._61Slas = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         ContLicSurce._62Adjuntos = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         ContLicSurce._63Videos = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         ContLicSurce._64Subtemas = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         ContLicSurce._70TipoVersion = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         ContLicSurce._71NumLicWeb = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
         ContLicSurce._72DeshabilitaControlRemoto = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         ContLicSurce._73AdmonProblemas = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
         return (ContLicSurce);
      }  
