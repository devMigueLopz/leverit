﻿SysCfg.DateTimeMethods.ConvertMinutesToHours = function (minutes) {
    var resp = "";

    var hour = parseInt((minutes / 60)) < 0 ? parseInt((minutes / 60)) * -1 : parseInt((minutes / 60));
    var min = (minutes % 60) < 0 ? (minutes % 60) * -1 : (minutes % 60);
    resp = (SysCfg.DateTimeMethods.Pad(hour, 2) + ":" + SysCfg.DateTimeMethods.Pad(min, 2));

    return resp;
}

SysCfg.DateTimeMethods.ConvertTimeStampToHours = function (timestamp) {
    var resp = "";
    var timestamp = parseInt(timestamp) < 0 ? parseInt(timestamp) * -1 : parseInt(timestamp);

    var ms = timestamp % 1000;
    timestamp = (timestamp - ms) / 1000;
    var sec = timestamp % 60;
    timestamp = (timestamp - sec) / 60;
    var min = timestamp % 60;
    var hour = (timestamp - min) / 60;

    resp = (SysCfg.DateTimeMethods.Pad(hour, 2) + ":" + SysCfg.DateTimeMethods.Pad(min, 2) + ":" + SysCfg.DateTimeMethods.Pad(sec, 2));

    return resp;
}


SysCfg.DateTimeMethods.DayOfYear = function (d) {   // d is a Date object
    var yn = d.getFullYear();
    var mn = d.getMonth();
    var dn = d.getDate();
    var d1 = new Date(yn, 0, 1, 12, 0, 0); // noon on Jan. 1
    var d2 = new Date(yn, mn, dn, 12, 0, 0); // noon on input date
    var ddiff = Math.round((d2 - d1) / 864e5);
    return ddiff + 1;
}

SysCfg.DateTimeMethods.ToDateString = function (d) {
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    var date = SysCfg.DateTimeMethods.Pad(day, 2) + "/" + SysCfg.DateTimeMethods.Pad(month, 2) + "/" + year;
    return date;
}

SysCfg.DateTimeMethods.ToDateTimeString = function (d) {
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    var hour = d.getHours();
    var min = d.getMinutes();
    var AMPM = hour >= 12 ? "PM" : "AM";
    var date = SysCfg.DateTimeMethods.Pad(day, 2) + "/" + SysCfg.DateTimeMethods.Pad(month, 2) + "/" + year + " " + SysCfg.DateTimeMethods.Pad(hour, 2) + ":" + SysCfg.DateTimeMethods.Pad(min, 2) + " " + AMPM;
    return date;
}

SysCfg.DateTimeMethods.ToDateTimefull = function (d) {
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    var hour = d.getHours();
    var min = d.getMinutes();
    var sec = d.getSeconds()
    var date = SysCfg.DateTimeMethods.Pad(day, 2) + "/" + SysCfg.DateTimeMethods.Pad(month, 2) + "/" + year + " " + SysCfg.DateTimeMethods.Pad(hour, 2) + ":" + SysCfg.DateTimeMethods.Pad(min, 2) + ":" + SysCfg.DateTimeMethods.Pad(sec, 2);
    return date;
}


SysCfg.DateTimeMethods.ToTimeString = function (d) {
    var hour = d.getHours();
    var min = d.getMinutes();
    var AMPM = hour >= 12 ? "PM" : "AM";
    var date = SysCfg.DateTimeMethods.Pad(hour, 2) + ":" + SysCfg.DateTimeMethods.Pad(min, 2) + " " + AMPM;
    return date;
}


SysCfg.DateTimeMethods.Pad = function (str, max) {
    str = str.toString();
    return str.length < max ? pad("0" + str, max) : str;
}

function formatJSONDate(jsonDate) {//crc
    var dateString = jsonDate.substr(6);
    var currentTime = new Date(parseInt(dateString));
    var month = currentTime.getMonth() + 1;
    var day = currentTime.getDate();
    var year = currentTime.getFullYear();
    var hour = currentTime.getHours();
    var min = currentTime.getMinutes();
    var date = pad(day, 2) + "/" + pad(month, 2) + "/" + year;
    return date;
}

function formatJSONDateTime(jsonDate) {//crc
    var dateString = jsonDate.substr(6);
    var currentTime = new Date(parseInt(dateString));
    var month = currentTime.getMonth() + 1;
    var day = currentTime.getDate();
    var year = currentTime.getFullYear();
    var hour = currentTime.getHours();
    var min = currentTime.getMinutes();
    var date = pad(day, 2) + "/" + pad(month, 2) + "/" + year + " " + pad(hour, 2) + ":" + pad(min, 2);
    return date;
}


function pad(str, max) {
    str = str.toString();
    return str.length < max ? pad("0" + str, max) : str;
}


SysCfg.DateTimeMethods.formatJSONDateTimetoDate = function (jsonDate) {//crc
    var dateString = jsonDate.substr(6);
    var currentTime = new Date(parseInt(dateString));
    return currentTime;
}
SysCfg.DateTimeMethods.ExtensionDate = {
    convert: function (d) {
        // Converts the date in d to a date-object. The input can be:
        //   a date object: returned without modification
        //  an array      : Interpreted as [year,month,day]. NOTE: month is 0-11.
        //   a number     : Interpreted as number of milliseconds
        //                  since 1 Jan 1970 (a timestamp) 
        //   a string     : Any format supported by the javascript engine, like
        //                  "YYYY/MM/DD", "MM/DD/YYYY", "Jan 31 2009" etc.
        //  an object     : Interpreted as an object with year, month and date
        //                  attributes.  **NOTE** month is 0-11.
        return (
            d.constructor === Date ? d :
                d.constructor === Array ? new Date(d[0], d[1], d[2]) :
                    d.constructor === Number ? new Date(d) :
                        d.constructor === String ? new Date(d) :
                            typeof d === "object" ? new Date(d.year, d.month, d.date) :
                                NaN
        );
    },
    compare: function (a, b) {
        // Compare two dates (could be of any type supported by the convert
        // function above) and returns:
        //  -1 : if a < b
        //   0 : if a = b
        //   1 : if a > b
        // NaN : if a or b is an illegal date
        // NOTE: The code inside isFinite does an assignment (=).
        return (
            isFinite(a = this.convert(a).valueOf()) &&
                isFinite(b = this.convert(b).valueOf()) ?
                (a > b) - (a < b) :
                NaN
        );
    },
    inRange: function (d, start, end) {
        // Checks if date in d is between dates in start and end.
        // Returns a boolean or NaN:
        //    true  : if d is between start and end (inclusive)
        //    false : if d is before start or after end
        //    NaN   : if one or more of the dates is illegal.
        // NOTE: The code inside isFinite does an assignment (=).
        return (
            isFinite(d = this.convert(d).valueOf()) &&
                isFinite(start = this.convert(start).valueOf()) &&
                isFinite(end = this.convert(end).valueOf()) ?
                start <= d && d <= end :
                NaN
        );
    },
    convertDateTime: function (d) {
        var date = d.constructor === Date ? d :
            d.constructor === Array ? new Date(d[0], d[1], d[2]) :
                d.constructor === Number ? new Date(d) :
                    d.constructor === String ? new Date(d) :
                        typeof d === "object" ? new Date(d.year, d.month, d.date) :
                            NaN
        if (!isNaN(date)) {
            var hours = date.getHours();
            var minutes = date.getMinutes();
            var ampm = hours >= 12 ? 'pm' : 'am';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0' + minutes : minutes;
            var strTime = hours + ':' + minutes + ' ' + ampm;
            return (date.toDateString() + " " + strTime);
        }
        else {
            return (date);
        }
    }
}





////c#
//DateTime VDateTime1 = new DateTime(2017, 11, 1, 3, 2, 1);
//Double NDouble = Convert.ToDouble(
//VDateTime1.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds
//);
//DateTime VDateTime2 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(NDouble).ToLocalTime();
//if (VDateTime1 == VDateTime2)
//{
//    SysCfg.App.Methods.ShowMessage("equal");
//}

////Js
//VDateTime1 = new Date(1970, 0, 1, 0, 0, 0);
//NDouble = VDateTime1.getTime();  
//VDateTime2 =  new Date(NDouble);  >>123132432000.0000
//if (VDateTime1 == VDateTime2)
//{
//    SysCfg.App.Methods.ShowMessage("equal");
//}
//123132432000.0000
//DateTime VDateTime2 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds((Double)123132432000.0000).ToLocalTime();