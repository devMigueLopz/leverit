﻿SysCfg.App.Methods._DirCfg = "SrvCfg";




SysCfg.App.Methods.ShowMessage = function (ShowMessage) {
    alert(ShowMessage);
}


SysCfg.App.Methods.TCfg_WindowsTask = function () {
    this.NumSesion = 0;
    this.User = new SysCfg.App.TUser();
    this.LogEnable = false;
    this.ApplicationPathLog = "";
    this.ApplicationPathCfg;
    this.ApplicationPathIni;
    this.EncripPasswordHawk;
    this.ContinueAtention = false;
    
}
SysCfg.App.Methods.Cfg_WindowsTask = new SysCfg.App.Methods.TCfg_WindowsTask();

SysCfg.App.Methods.Create = function () {
    SysCfg.App.Methods.Cfg_WindowsTask.NumSesion = 0;
    SysCfg.App.Methods.Cfg_WindowsTask.LogEnable = false;
    SysCfg.App.Methods.Cfg_WindowsTask.ApplicationPathLog = "";
    SysCfg.App.Methods.Cfg_WindowsTask.ApplicationPathCfg = "";
    SysCfg.App.Methods.Cfg_WindowsTask.ApplicationPathIni = "";
    SysCfg.App.Methods.Cfg_WindowsTask.EncripPasswordHawk = false;
    SysCfg.App.Methods.Cfg_WindowsTask.User.IDCMDBUSER = 0;
    SysCfg.App.Methods.Cfg_WindowsTask.User.IDCMDBCI = 0;
    SysCfg.App.Methods.Cfg_WindowsTask.User.IDATROLE = 0;
    SysCfg.App.Methods.Cfg_WindowsTask.User.CI_GENERICNAME = "None";
    SysCfg.App.Methods.Cfg_WindowsTask.ContinueAtention = false;
}

SysCfg.App.Methods.LoadbyIniDataSet = function (inCallback) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.CallBackResult = inCallback;
    this.Conter = 0;
    
   /* var IniDataSet = new SysCfg.IniMemTable.TIniDataSet();
    IniDataSet.Inicialize(SysCfg.App.Methods.Cfg_WindowsTask.ApplicationPathIni, false);
    Cfg_WindowsTask.User.CI_GENERICNAME = IniDataSet.ReadString("Cfg_WindowsTask", "CI_GENERICNAME", Cfg_WindowsTask.User.CI_GENERICNAME);
    for (var i = 0; i < Cfg_WindowsTask.User.OLD_GENERICNAME.Length; i++) {
        Cfg_WindowsTask.User.OLD_GENERICNAME[i] = IniDataSet.ReadString("Cfg_WindowsTask", "CI_GENERICNAME_" + i, "");
    }
    IniDataSet.Free();*/

    var IniDataSet = new SysCfg.ini.DBHelper('iniIndexDB105', 1);
    IniDataSet.OnOpenCompleted = function (sender, Args) {
        sender.ReadString("Cfg_WindowsTask", "CI_GENERICNAME", SysCfg.App.Methods.Cfg_WindowsTask.User.CI_GENERICNAME);
    };
    IniDataSet.OnWriteCompleted = function (sender, Args) {
    };
    IniDataSet.OnReadCompleted = function (sender, Args) {
        if (sender.Data != null) {
            SysCfg.App.Methods.Cfg_WindowsTask.User.CI_GENERICNAME = sender.Data.Values;
        }
        for (var i = 0; i < SysCfg.App.Methods.Cfg_WindowsTask.User.OLD_GENERICNAME.length; i++) {
            _this.Conter++;
            sender.ReadString("Cfg_WindowsTask", "CI_GENERICNAME_" + i, "");
            sender.OnReadCompleted = function (sender, Args) {
                _this.Conter--;
                if (sender.Data != null) {
                    SysCfg.App.Methods.Cfg_WindowsTask.User.OLD_GENERICNAME[sender.Data.Idx] = sender.Data.Values;                    
                }
                if (_this.Conter == 0) {
                    _this.CallBackResult();
                }
            }


        }
    }
}

SysCfg.App.Methods.LoadbyIniIndexDb = function (sender, inCallback) {
    //WriteString
    //WriteInt
    //WriteBoolean

    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.CallBackResult = inCallback;
    this.ParentSender = sender;
    this.Conter = 0;
    //***************************
    _this.Conter = 3;    
    var IniDataSet1 = new SysCfg.ini.DBHelper('iniIndexDB105', 1);
    IniDataSet1.OnOpenCompleted = function (sender, Args) {sender.ReadInt("App", "Device", SysCfg.App.Properties.Device.value);};
    IniDataSet1.OnWriteCompleted = function (sender, Args) {};
    IniDataSet1.OnReadCompleted = function (sender, Args) {
        _this.Conter--;
        if (sender.Data != null) {SysCfg.App.Properties.Device = SysCfg.App.TDevice.GetEnum(sender.Data.Values);}
        if (_this.Conter == 0) {
            _this.CallBackResult(_this.ParentSender);
        }
    }
    //***************************
    
    var IniDataSet2 = new SysCfg.ini.DBHelper('iniIndexDB105', 1);
    IniDataSet2.OnOpenCompleted = function (sender, Args) { sender.ReadInt("Componet_GridCF", "FontSize", Componet.GridCF.Properties.FontSize); };
    IniDataSet2.OnWriteCompleted = function (sender, Args) { };
    IniDataSet2.OnReadCompleted = function (sender, Args) {
        _this.Conter--;
        if (sender.Data != null) { Componet.GridCF.Properties.FontSize = sender.Data.Values;}
        if (_this.Conter == 0) {
            _this.CallBackResult(_this.ParentSender);
        }
    }

    //***************************
    var IniDataSet3 = new SysCfg.ini.DBHelper('iniIndexDB105', 1);
    IniDataSet3.OnOpenCompleted = function (sender, Args) {
        sender.ReadBoolean("Cfg_WindowsTask", "ContinueAtention", SysCfg.App.Methods.Cfg_WindowsTask.ContinueAtention);
    };
    IniDataSet3.OnWriteCompleted = function (sender, Args) { };
    IniDataSet3.OnReadCompleted = function (sender, Args) {
        _this.Conter--;
        if (sender.Data != null) { SysCfg.App.Methods.Cfg_WindowsTask.ContinueAtention = sender.Data.Values; }
        if (_this.Conter == 0) {
            _this.CallBackResult(_this.ParentSender);
        }
    }
}

SysCfg.App.Methods.SavebyIniIndexDb = function () {
    
    var IniDataSet1 = new SysCfg.ini.DBHelper('iniIndexDB105', 1);
    IniDataSet1.OnOpenCompleted = function (sender, Args) {
        sender.WriteInt("App", "Device", SysCfg.App.Properties.Device.value);
    };
    IniDataSet1.OnWriteCompleted = function (sender, Args) {
    };
    IniDataSet1.OnReadCompleted = function (sender, Args) {        
    }
    //**************************
    var IniDataSet2 = new SysCfg.ini.DBHelper('iniIndexDB105', 1);
    IniDataSet2.OnOpenCompleted = function (sender, Args) {
        sender.WriteInt("Componet_GridCF", "FontSize", Componet.GridCF.Properties.FontSize);
    };
    IniDataSet2.OnWriteCompleted = function (sender, Args) {
    };
    IniDataSet2.OnReadCompleted = function (sender, Args) {
    }
    //**************************
    SysCfg.App.Methods.Cfg_WindowsTask.ContinueAtention
    var IniDataSet3 = new SysCfg.ini.DBHelper('iniIndexDB105', 1);
    IniDataSet3.OnOpenCompleted = function (sender, Args) {
        sender.WriteBoolean("Cfg_WindowsTask", "ContinueAtention", SysCfg.App.Methods.Cfg_WindowsTask.ContinueAtention);
    };
    IniDataSet3.OnWriteCompleted = function (sender, Args) {
    };
    IniDataSet3.OnReadCompleted = function (sender, Args) {
    }
}





SysCfg.App.Methods.SavebyIniDataSet = function () {
    /*
    var IniDataSet = new SysCfg.IniMemTable.TIniDataSet();
    IniDataSet.Inicialize(SysCfg.App.Methods.Cfg_WindowsTask.ApplicationPathIni, false);
    IniDataSet.WriteString("Cfg_WindowsTask", "CI_GENERICNAME", Cfg_WindowsTask.User.CI_GENERICNAME);
    var noexiste = true;
    for (var i = 0; i < Cfg_WindowsTask.User.OLD_GENERICNAME.Length; i++) {
        Cfg_WindowsTask.User.OLD_GENERICNAME[i] = IniDataSet.ReadString("Cfg_WindowsTask", "CI_GENERICNAME_" + i, "");
        if (SysCfg.Str.isEqual(Cfg_WindowsTask.User.OLD_GENERICNAME[i], Cfg_WindowsTask.User.CI_GENERICNAME)) { noexiste = false; }
    }
    if (noexiste) {
        for (var i = Cfg_WindowsTask.User.OLD_GENERICNAME.Length - 1; i >= 0; i--) {
            if (i == 0) { Cfg_WindowsTask.User.OLD_GENERICNAME[i] = Cfg_WindowsTask.User.CI_GENERICNAME; }
            else
            {
                Cfg_WindowsTask.User.OLD_GENERICNAME[i] = Cfg_WindowsTask.User.OLD_GENERICNAME[i - 1];
            }
        }
    }
    for (var i = 0; i < Cfg_WindowsTask.User.OLD_GENERICNAME.Length; i++) {
        IniDataSet.WriteString("Cfg_WindowsTask", "CI_GENERICNAME_" + i.ToString(), Cfg_WindowsTask.User.OLD_GENERICNAME[i]);
    }
    IniDataSet.WriteString("Cfg_WindowsTask", "CI_GENERICNAME", Cfg_WindowsTask.User.CI_GENERICNAME);
    IniDataSet.Free();
    */
}


SysCfg.App.Methods.LoadApplicationPathLog = function () {
    //************************* log *******************************
    SysCfg.App.Methods.Cfg_WindowsTask.LogEnable = true;
    /*
    Cfg_WindowsTask.ApplicationPathLog = SysCfg.App.Properties.Config_Root.AppPath + "\\" + SysCfg.Log.Properties._DirLog + "\\" + SysCfg.App.Properties.Application.value + "_" + Cfg_WindowsTask.NumSesion.ToString() + "." + SysCfg.Log.Properties._ExtLog;
    SysCfg.FilesIO.Methods.CreateDirectory(SysCfg.App.Properties.Config_Root.AppPath + "\\" + SysCfg.Log.Properties._DirLog);
    Cfg_WindowsTask.ApplicationPathCfg = SysCfg.App.Properties.Config_Root.AppPath + "\\" + _DirCfg;
    SysCfg.FilesIO.Methods.CreateDirectory(Cfg_WindowsTask.ApplicationPathCfg);
    SysCfg.Log.Methods.EliminaLog(SysCfg.App.Properties.Config_Root.AppPath + "\\SrvLog\\", SysCfg.App.Properties.Application.value + "_*.log", 10);
    */
}

SysCfg.App.Methods.LoadApplicationPathIni = function () {
    ////************************* Ini *********************************
    //SysCfg.FilesIO.Methods.CreateDirectory(SysCfg.App.Properties.Config_Root.AppPath);
    //Cfg_WindowsTask.ApplicationPathIni = SysCfg.App.Properties.Config_Root.AppPath + "\\" + SysCfg.App.Properties.Application.name + ".ini";
}

SysCfg.App.Methods.LoadNumSesion = function (Root) {
    
    //Comunic.AjaxJson.Client.Methods.GetSesion(Root).Num
    var GetSesion = Comunic.AjaxJson.Client.Methods.GetSesion(Root);
    return (GetSesion.Num);
}

SysCfg.App.Methods.InicDLPSSL_Client = function () {
    Comunic.Sockets.Client.Methods.GetSSLCode();
}

SysCfg.App.Methods.CloseApp = function () {
    /*
    if (Application.Current.IsRunningOutOfBrowser && Application.Current.HasElevatedPermissions) {
        Application.Current.Resources.Clear();
        Application.Current.MainWindow.Close();
    }
    else {
        Application.Current.Resources.Clear();
        HtmlPage.Window.Invoke("CloseWindow");
    }
    */

}


