﻿//SysCfg.Methods

SysCfg.Methods.include("Scripts/SysCfgScripts/Implements.js");

SysCfg.Methods.include("Scripts/SysCfgScripts/Device.js");
SysCfg.Methods.include("Scripts/SysCfgScripts/ErrorProperties.js");
SysCfg.Methods.include("Scripts/SysCfgScripts/ErrorMethods.js");
SysCfg.Methods.include("Scripts/SysCfgScripts/EnumProvider.js");
SysCfg.Methods.include("Scripts/SysCfgScripts/StartFunction.js");


SysCfg.Methods.include("Scripts/SysCfgScripts/DomainProperties.js");
SysCfg.Methods.include("Scripts/SysCfgScripts/DomainJavaScript.js");
SysCfg.Methods.include("Scripts/SysCfgScripts/AppProperties.js");
SysCfg.Methods.include("Scripts/SysCfgScripts/JavaScriptClientMethods.js");



SysCfg.Methods.include("Scripts/SysCfgScripts/DateTimeMethods.js");
SysCfg.Methods.include("Scripts/SysCfgScripts/MemTable.js");
SysCfg.Methods.include("Scripts/SysCfgScripts/StrMethods.js");

SysCfg.Methods.include("Scripts/SysCfgScripts/DB/BDProperties.js");
SysCfg.Methods.include("Scripts/SysCfgScripts/DB/DBSQLCientMethods.js");
SysCfg.Methods.include("Scripts/SysCfgScripts/DB/DBSQLCientProperties.js");
SysCfg.Methods.include("Scripts/AjaxJson/AjaxJsonClient.js");
SysCfg.Methods.include("Scripts/AjaxJson/AjaxJsonClientMerhods.js");
SysCfg.Methods.include("Scripts/Ashx/AshxClient.js");
SysCfg.Methods.include("Scripts/Ashx/AshxClientMerhods.js");


SysCfg.Methods.include("Scripts/Comunic/ComunicProperties.js");

SysCfg.Methods.include("Scripts/SysCfgScripts/StreamProperties.js");
SysCfg.Methods.include("Scripts/SysCfgScripts/binary-parser/binary-parser.js");
SysCfg.Methods.include("Scripts/SysCfgScripts/StreamMethods.js");
SysCfg.Methods.include("Scripts/SysCfgScripts/StreamProperties.js");

SysCfg.Methods.include("Scripts/SysCfgScripts/LicenseMethods.js");
SysCfg.Methods.include("Scripts/SysCfgScripts/LicenseProperties.js");
SysCfg.Methods.include("Scripts/SysCfgScripts/iniIndexDB.js");

SysCfg.Methods.include("Scripts/SysCfgScripts/LogJavaScriptMethods.js");
SysCfg.Methods.include("Scripts/SysCfgScripts/LogProperties.js");

SysCfg.Methods.include("Scripts/SysCfgScripts/IDI/IDIClient.js");
SysCfg.Methods.include("Scripts/SysCfgScripts/IDI/IDIProperties.js");
