﻿//Bibliografia 
//https://developer.mozilla.org/es/docs/Web/JavaScript/Introducción_a_JavaScript_orientado_a_objetos
//http://lemoncode.net/lemoncode-blog/2018/1/29/javascript-asincrono
//https://www.todojs.com/controlar-la-ejecucion-asincrona/




SysCfg.ini.DBHelper = function (inDBName, inVersion/*, inCount, inTimeOut*/) {
    this.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
    this.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction;
    this.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange;

    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    /*
    this.Count = inCount;
    this.TimeOut = inCount;
    */
    this.DBName = inDBName;
    this.Version = inVersion;
    this.DB = null;
    this.Data = null;
    this.Value = null;

    this.Result = null;

    this.IsOpening = false;
    this.IsFinally = false;

    this.IsError = false;
    this.ErrorText = "";

    this.OnOpenCompleted = null;
    this.OnWriteCompleted = null;
    this.OnReadCompleted = null;

    this.State = SysCfg.ini.State.None;

    this.OpenCompleted = function (e) {
        if (this.OnOpenCompleted != null)
            this.OnOpenCompleted(this, e);
    }

    this.WriteCompleted = function (e) {
        if (this.OnWriteCompleted != null)
            this.OnWriteCompleted(this, e);
    }

    this.ReadCompleted = function (e) {
        if (this.OnReadCompleted != null)
            this.OnReadCompleted(this, e);
    }

    this.Open();
    //
}

SysCfg.ini.State = { None: 0, Open: 1, Read: 2, Write: 3 };

SysCfg.ini.DBHelper.prototype.Open = function () {
    var _this = this.TParent();

    _this.State = SysCfg.ini.State.Open;

    var openRequest = _this.indexedDB.open(this.DBName, this.Version);
    openRequest.onupgradeneeded = function (e) {
        _this.DB = e.target.result;
        if (!_this.DB.objectStoreNames.contains("ini")) {
            var objectStore = _this.DB.createObjectStore("ini", { keyPath: "Idx", autoIncrement: true });
            objectStore.createIndex('Root', 'Root', { unique: false });
            objectStore.createIndex('Key', 'Key', { unique: false });
            objectStore.createIndex('Values', 'Values', { unique: false });
        }
    };
    openRequest.onsuccess = function (e) {
        _this.Result = e;
        _this.IsError = false;
        _this.ErrorText = "Open Success";
        _this.OpenCompleted(e);
    };
    openRequest.onerror = function (e) {
        _this.Result = e;
        _this.IsError = true;
        _this.ErrorText = "Error Opening DataBase";
        _this.OpenCompleted(e);
    };
};

SysCfg.ini.DBHelper.prototype.WriteString = function (Root, Key, Value) {
    var _this = this.TParent();
    Value = Value.toString();
    _this.Value = Value;

    _this.State = SysCfg.ini.State.Write;
    var openRequest = _this.indexedDB.open(this.DBName, this.Version);
    openRequest.onupgradeneeded = function (e) {
        _this.DB = e.target.result;
        if (!_this.DB.objectStoreNames.contains("ini")) {
            //console.log("I need to make the Traslate objectstore");
            var objectStore = _this.DB.createObjectStore("ini", { keyPath: "Idx", autoIncrement: true });
            objectStore.createIndex('Root', 'Root', { unique: false });
            objectStore.createIndex('Key', 'Key', { unique: false });
            objectStore.createIndex('Values', 'Values', { unique: false });
        }
    }
    openRequest.onsuccess = function (e) {
        var thisDb = e.target.result;
        var transaction = thisDb.transaction(["ini"], "readwrite");
        var objectStore = transaction.objectStore("ini");
        var openRequestCursor = objectStore.openCursor();
        openRequestCursor.onsuccess = function (e) {
            var res = false;
            var data;
            var cursor = e.target.result;
            if (cursor) {

                if (cursor.value.Root == Root && cursor.value.Key == Key) {
                    res = true;
                    data = cursor.value;
                    data.Values = Value;
                }
                //else
                //    cursor.continue();
            }
            var add;
            if (res)
                add = objectStore.put(data);
            else
                add = objectStore.put({ Root: Root, Key: Key, Values: Value });

            add.onsuccess = function (e) {
                _this.Result = e;
                _this.IsError = false;
                _this.ErrorText = "Save Success";
                _this.WriteCompleted(e);
            };
            add.onerror = function (e) {
                _this.Result = e;
                _this.IsError = true;
                _this.ErrorText = "Error Saving";
                _this.WriteCompleted(e);
            };
        };
        openRequestCursor.onerror = function (e) {
            _this.Result = e;
            _this.IsError = true;
            _this.ErrorText = "Error Opening DataBase";
            _this.WriteCompleted(e);
        };
    }
    openRequest.onerror = function (e) {
        _this.Result = e;
        _this.IsError = true;
        _this.ErrorText = "Error Opening DataBase";
        _this.WriteCompleted(e);
    }
}

SysCfg.ini.DBHelper.prototype.WriteInt = function (Root, Key, Value) {
    var _this = this.TParent();
    Value = parseInt(Value);
    _this.Value = Value;

    _this.State = SysCfg.ini.State.Write;
    var openRequest = _this.indexedDB.open(this.DBName, this.Version);
    openRequest.onupgradeneeded = function (e) {
        _this.DB = e.target.result;
        if (!_this.DB.objectStoreNames.contains("ini")) {
            //console.log("I need to make the Traslate objectstore");
            var objectStore = _this.DB.createObjectStore("ini", { keyPath: "Idx", autoIncrement: true });
            objectStore.createIndex('Root', 'Root', { unique: false });
            objectStore.createIndex('Key', 'Key', { unique: false });
            objectStore.createIndex('Values', 'Values', { unique: false });
        }
    }
    openRequest.onsuccess = function (e) {
        var thisDb = e.target.result;
        var transaction = thisDb.transaction(["ini"], "readwrite");
        var objectStore = transaction.objectStore("ini");
        var openRequestCursor = objectStore.openCursor();
        openRequestCursor.onsuccess = function (e) {
            var res = false;
            var data;
            var cursor = e.target.result;
            if (cursor) {

                if (cursor.value.Root == Root && cursor.value.Key == Key) {
                    res = true;
                    data = cursor.value;
                    data.Values = Value;
                }
                //else
                //    cursor.continue();
            }
            var add;
            if (res)
                add = objectStore.put(data);
            else
                add = objectStore.put({ Root: Root, Key: Key, Values: Value });

            add.onsuccess = function (e) {
                _this.Result = e;
                _this.IsError = false;
                _this.ErrorText = "Save Success";
                _this.WriteCompleted(e);
            };
            add.onerror = function (e) {
                _this.Result = e;
                _this.IsError = true;
                _this.ErrorText = "Error Saving";
                _this.WriteCompleted(e);
            };
        };
        openRequestCursor.onerror = function (e) {
            _this.Result = e;
            _this.IsError = true;
            _this.ErrorText = "Error Opening DataBase";
            _this.WriteCompleted(e);
        };
    }
    openRequest.onerror = function (e) {
        _this.Result = e;
        _this.IsError = true;
        _this.ErrorText = "Error Opening DataBase";
        _this.WriteCompleted(e);
    }
}

SysCfg.ini.DBHelper.prototype.WriteBoolean = function (Root, Key, Value) {
    var _this = this.TParent();
    _this.Value = false;
    if (typeof Value == 'string') {
        if (Value === 'true')
            _this.Value = true;
        else if (Value === '0')
            _this.Value = false;
        else if (Value === '1')
            _this.Value = true;
        else
            _this.Value = false;
    }
    else if (typeof Value == 'number')
        _this.Value = (Value > 0) ? true : false;
    else if (typeof Value == 'boolean')
        _this.Value = Value;

    Value = (_this.Value ? "1" : "0");

    _this.State = SysCfg.ini.State.Write;
    var openRequest = _this.indexedDB.open(this.DBName, this.Version);
    openRequest.onupgradeneeded = function (e) {
        _this.DB = e.target.result;
        if (!_this.DB.objectStoreNames.contains("ini")) {
            //console.log("I need to make the Traslate objectstore");
            var objectStore = _this.DB.createObjectStore("ini", { keyPath: "Idx", autoIncrement: true });
            objectStore.createIndex('Root', 'Root', { unique: false });
            objectStore.createIndex('Key', 'Key', { unique: false });
            objectStore.createIndex('Values', 'Values', { unique: false });
        }
    }
    openRequest.onsuccess = function (e) {
        var thisDb = e.target.result;
        var transaction = thisDb.transaction(["ini"], "readwrite");
        var objectStore = transaction.objectStore("ini");
        var openRequestCursor = objectStore.openCursor();
        openRequestCursor.onsuccess = function (e) {
            var res = false;
            var data;
            var cursor = e.target.result;
            if (cursor) {

                if (cursor.value.Root == Root && cursor.value.Key == Key) {
                    res = true;
                    data = cursor.value;
                    data.Values = Value;
                }
                //else
                //    cursor.continue();
            }
            var add;
            if (res)
                add = objectStore.put(data);
            else
                add = objectStore.put({ Root: Root, Key: Key, Values: Value });

            add.onsuccess = function (e) {
                _this.Result = e;
                _this.IsError = false;
                _this.ErrorText = "Save Success";
                _this.WriteCompleted(e);
            };
            add.onerror = function (e) {
                _this.Result = e;
                _this.IsError = true;
                _this.ErrorText = "Error Saving";
                _this.WriteCompleted(e);
            };
        };
        openRequestCursor.onerror = function (e) {
            _this.Result = e;
            _this.IsError = true;
            _this.ErrorText = "Error Opening DataBase";
            _this.WriteCompleted(e);
        };
    }
    openRequest.onerror = function (e) {
        _this.Result = e;
        _this.IsError = true;
        _this.ErrorText = "Error Opening DataBase";
        _this.WriteCompleted(e);
    }
}

SysCfg.ini.DBHelper.prototype.ReadString = function (Root, Key, Value) {
    var _this = this.TParent();
    Value = Value.toString();
    _this.Value = Value;

    _this.State = SysCfg.ini.State.Read;
    var openRequest = _this.indexedDB.open(this.DBName, this.Version);
    openRequest.onupgradeneeded = function (e) {
        _this.DB = e.target.result;
        if (!_this.DB.objectStoreNames.contains("ini")) {
            var objectStore = _this.DB.createObjectStore("ini", { keyPath: "Idx", autoIncrement: true });
            objectStore.createIndex('Root', 'Root', { unique: false });
            objectStore.createIndex('Key', 'Key', { unique: false });
            objectStore.createIndex('Values', 'Values', { unique: false });
        }
    };
    openRequest.onsuccess = function (e) {
        _this.DB = e.target.result;
        var transaction = _this.DB.transaction(["ini"], "readwrite");
        var objectStore = transaction.objectStore("ini");

        var openRequestCursor = objectStore.openCursor();
        openRequestCursor.onsuccess = function (e) {
            var res = false;
            var data = null;

            var cursor = e.target.result;
            if (cursor) {

                if (cursor.value.Root == Root && cursor.value.Key == Key) {
                    //como ya existe cargamos resultados
                    res = true;
                    data = cursor.value;
                    _this.Data = cursor.value;
                    _this.Value = _this.Data.Values.toString();
                    //data.value = Value;                    
                }
                //else
                //    cursor.continue();
            }
            if (res)

                var add;
            if (res)
                objectStore.get(data.Idx).onsuccess = function (e) {
                    //se ejecuta el evento de salida para el formulario ya que se consulto correctamente los datos
                    _this.Result = e;
                    _this.IsError = false;
                    _this.ErrorText = "Read Success";
                    _this.ReadCompleted(e);
                };
            else {
                add = objectStore.put({ Root: Root, Key: Key, Values: Value });
                add.onsuccess = function (e) {
                    var openRequestCursor2 = objectStore.openCursor();
                    openRequestCursor2.onsuccess = function (e) {
                        var res2 = false;
                        var data2 = null;

                        var cursor2 = e.target.result;
                        if (cursor2) {

                            if (cursor2.value.Root == Root && cursor2.value.Key == Key) {
                                res2 = true;
                                data2 = cursor2.value;
                                _this.Data = cursor2.value;
                                _this.Value = _this.Data.Values.toString();
                            }
                        }
                        //if (res2) {
                        //    objectStore.get(data2.Idx).onsuccess = function (e) {
                        //        _this.Result = e;
                        //        _this.IsError = false;
                        //        _this.ErrorText = "Read Success";
                        //    };
                        //}
                        _this.Result = e;
                        _this.IsError = false;
                        _this.ErrorText = "Read Success";
                        _this.ReadCompleted(e);
                    }
                    openRequestCursor2.onerror = function (e) {
                        _this.Result = e;
                        _this.IsError = true;
                        _this.ErrorText = "Error finding value";
                        _this.ReadCompleted(e);
                    }
                };
                add.onerror = function (e) {
                    _this.Result = e;
                    _this.IsError = true;
                    _this.ErrorText = "Error saving";
                    _this.ReadCompleted(e);
                };
            }

        }
        openRequestCursor.onerror = function (e) {
            _this.Result = e;
            _this.IsError = true;
            _this.ErrorText = "Error finding value";
            _this.ReadCompleted(e);
        }


    };
    openRequest.onerror = function (e) {
        _this.Result = e;
        _this.IsError = true;
        _this.ErrorText = "Error Opening DataBase";
        _this.ReadCompleted(e);

    };
}

SysCfg.ini.DBHelper.prototype.ReadInt = function (Root, Key, Value) {
    var _this = this.TParent();
    Value = parseInt(Value);
    _this.Value = Value;

    _this.State = SysCfg.ini.State.Read;
    var openRequest = _this.indexedDB.open(this.DBName, this.Version);
    openRequest.onupgradeneeded = function (e) {
        _this.DB = e.target.result;
        if (!_this.DB.objectStoreNames.contains("ini")) {
            var objectStore = _this.DB.createObjectStore("ini", { keyPath: "Idx", autoIncrement: true });
            objectStore.createIndex('Root', 'Root', { unique: false });
            objectStore.createIndex('Key', 'Key', { unique: false });
            objectStore.createIndex('Values', 'Values', { unique: false });
        }
    };
    openRequest.onsuccess = function (e) {
        _this.DB = e.target.result;
        var transaction = _this.DB.transaction(["ini"], "readwrite");
        var objectStore = transaction.objectStore("ini");

        var openRequestCursor = objectStore.openCursor();
        openRequestCursor.onsuccess = function (e) {
            var res = false;
            var data = null;

            var cursor = e.target.result;
            if (cursor) {

                if (cursor.value.Root == Root && cursor.value.Key == Key) {
                    //como ya existe cargamos resultados
                    res = true;
                    data = cursor.value;
                    _this.Data = cursor.value;
                    _this.Value = parseInt(_this.Data.Values);
                }
                //else
                //    cursor.continue();
            }
            if (res)
                var add;
            if (res)
                objectStore.get(data.Idx).onsuccess = function (e) {
                    //se ejecuta el evento de salida para el formulario ya que se consulto correctamente los datos
                    _this.Result = e;
                    _this.IsError = false;
                    _this.ErrorText = "Read Success";
                    _this.ReadCompleted(e);
                };
            else {
                add = objectStore.put({ Root: Root, Key: Key, Values: Value });
                add.onsuccess = function (e) {
                    var openRequestCursor2 = objectStore.openCursor();
                    openRequestCursor2.onsuccess = function (e) {
                        var res2 = false;
                        var data2 = null;

                        var cursor2 = e.target.result;
                        if (cursor2) {

                            if (cursor2.value.Root == Root && cursor2.value.Key == Key) {
                                res2 = true;
                                data2 = cursor2.value;
                                _this.Data = cursor2.value;
                                _this.Value = parseInt(_this.Data.Values);
                            }
                        }
                        //if (res2) {
                        //    objectStore.get(data2.Idx).onsuccess = function (e) {
                        //        _this.Result = e;
                        //        _this.IsError = false;
                        //        _this.ErrorText = "Read Success";
                        //    };
                        //}
                        _this.Result = e;
                        _this.IsError = false;
                        _this.ErrorText = "Read Success";
                        _this.ReadCompleted(e);
                    }
                    openRequestCursor2.onerror = function (e) {
                        _this.Result = e;
                        _this.IsError = true;
                        _this.ErrorText = "Error finding value";
                        _this.ReadCompleted(e);
                    }
                };
                add.onerror = function (e) {
                    _this.Result = e;
                    _this.IsError = true;
                    _this.ErrorText = "Error saving";
                    _this.ReadCompleted(e);
                };
            }

        }
        openRequestCursor.onerror = function (e) {
            _this.Result = e;
            _this.IsError = true;
            _this.ErrorText = "Error finding value";
            _this.ReadCompleted(e);
        }


    };
    openRequest.onerror = function (e) {
        _this.Result = e;
        _this.IsError = true;
        _this.ErrorText = "Error Opening DataBase";
        _this.ReadCompleted(e);

    };
}

SysCfg.ini.DBHelper.prototype.ReadBoolean = function (Root, Key, Value) {
    
    var _this = this.TParent();
    _this.Value = false;

    if (typeof Value == 'string') {
        if (Value === 'true')
            _this.Value = true;
        else if (Value === '0')
            _this.Value = false;
        else if (Value === '1')
            _this.Value = true;
        else
            _this.Value = false;
    }
    else if (typeof Value == 'number')
        _this.Value = (Value > 0) ? true : false;
    else if (typeof Value == 'boolean')
        _this.Value = (Value ? "1" : "0");


    _this.State = SysCfg.ini.State.Read;
    var openRequest = _this.indexedDB.open(this.DBName, this.Version);
    openRequest.onupgradeneeded = function (e) {
        _this.DB = e.target.result;
        if (!_this.DB.objectStoreNames.contains("ini")) {
            var objectStore = _this.DB.createObjectStore("ini", { keyPath: "Idx", autoIncrement: true });
            objectStore.createIndex('Root', 'Root', { unique: false });
            objectStore.createIndex('Key', 'Key', { unique: false });
            objectStore.createIndex('Values', 'Values', { unique: false });
        }
    };
    openRequest.onsuccess = function (e) {
        _this.DB = e.target.result;
        var transaction = _this.DB.transaction(["ini"], "readwrite");
        var objectStore = transaction.objectStore("ini");

        var openRequestCursor = objectStore.openCursor();
        openRequestCursor.onsuccess = function (e) {
            var res = false;
            var data = null;

            var cursor = e.target.result;
            if (cursor) {

                if (cursor.value.Root == Root && cursor.value.Key == Key) {
                    //como ya existe cargamos resultados
                    res = true;
                    data = cursor.value;
                    _this.Data = cursor.value;
                    _this.Value = _this.Data.Values;
                }
                //else
                //    cursor.continue();
            }
            if (res)
                var add;
            if (res) {
                objectStore.get(data.Idx).onsuccess = function (e) {
                    //se ejecuta el evento de salida para el formulario ya que se consulto correctamente los datos
                    _this.Result = e;
                    _this.IsError = false;
                    _this.ErrorText = "Read Success";
                    _this.ReadCompleted(e);
                };
            }
            else {
                add = objectStore.put({ Root: Root, Key: Key, Values: Value });
                add.onsuccess = function (e) {
                    var openRequestCursor2 = objectStore.openCursor();
                    openRequestCursor2.onsuccess = function (e) {
                        var res2 = false;
                        var data2 = null;

                        var cursor2 = e.target.result;
                        if (cursor2) {

                            if (cursor2.value.Root == Root && cursor2.value.Key == Key) {
                                res2 = true;
                                data2 = cursor2.value;
                                _this.Data = cursor2.value;
                                _this.Value = _this.Data.Values;
                            }
                        }
                        //if (res2) {
                        //    objectStore.get(data2.Idx).onsuccess = function (e) {
                        //        _this.Result = e;
                        //        _this.IsError = false;
                        //        _this.ErrorText = "Read Success";
                        //    };
                        //}
                        _this.Result = e;
                        _this.IsError = false;
                        _this.ErrorText = "Read Success";
                        _this.ReadCompleted(e);
                    }
                    openRequestCursor2.onerror = function (e) {
                        _this.Result = e;
                        _this.IsError = true;
                        _this.ErrorText = "Error finding value";
                        _this.ReadCompleted(e);
                    }
                };
                add.onerror = function (e) {
                    _this.Result = e;
                    _this.IsError = true;
                    _this.ErrorText = "Error saving";
                    _this.ReadCompleted(e);
                };
            }
        }
        openRequestCursor.onerror = function (e) {
            _this.Result = e;
            _this.IsError = true;
            _this.ErrorText = "Error finding value";
            _this.ReadCompleted(e);
        }


    };
    openRequest.onerror = function (e) {
        _this.Result = e;
        _this.IsError = true;
        _this.ErrorText = "Error Opening DataBase";
        _this.ReadCompleted(e);

    };
}

//************ Example 
/*
var _DBHeper = new DBHelper('DBCarlos', 1);
_DBHeper.Open(function (e) {
    _DBHeper.Write("SysCfg", "CompVer", 999, function (e) {
        _DBHeper.Read("SysCfg", "CompVer", 999, function (e) {
            alert(e);
        }, function (e) {
            alert(e);
        });
    }, function (e) {
        alert("no se escribio");
    });
}, function (e) {
    alert(e.target.result);
});
*/

//id traslate local

SysCfg.ini.DBLocalLeng = function (inDBName, inVersion) {
    this.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
    this.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction;
    this.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange;

    this.TParent = function () {
        return this;
    }.bind(this);


    this.DBName = inDBName;
    this.Version = inVersion;
    this.DB = null;
}

SysCfg.ini.DBLocalLeng.prototype.Open = function (onSuccessCallback, onErrorCallBack) {
    var _this = this.TParent();

    var openRequest = _this.indexedDB.open(this.DBName, this.Version);
    openRequest.onupgradeneeded = function (e) {
        var thisDb = e.target.result;
        if (!thisDb.objectStoreNames.contains("ini")) {
            //console.log("I need to make the Traslate objectstore");
            var objectStore = thisDb.createObjectStore("ini", { keyPath: "Idx", autoIncrement: true });
            objectStore.createIndex('IDleng', 'IDleng', { unique: false });
        }
    };
    openRequest.onsuccess = onSuccessCallback;
    openRequest.onerror = onErrorCallBack;
};

///////////////////////////////////////////////////////////////////////////////////
SysCfg.ini.DBLocalLeng.prototype.Write = function (idleng, onSuccessCallback, onErrorCallBack) {
    var _this = this.TParent();

    var openRequest = _this.indexedDB.open(this.DBName, this.Version);
    openRequest.onupgradeneeded = function (e) {
        var thisDb = e.target.result;
        if (!thisDb.objectStoreNames.contains("ini")) {
            var objectStore = thisDb.createObjectStore("ini", { keyPath: "Idx", autoIncrement: true });
            objectStore.createIndex('IDleng', 'IDleng', { unique: false });
        }
    };
    openRequest.onsuccess = function (e) {
        var thisDb = e.target.result;
        var transaction = thisDb.transaction(["ini"], "readwrite");
        var objectStore = transaction.objectStore("ini");
        objectStore.openCursor().onsuccess = function (e) {
            var cursor = e.target.result;
            if (cursor) {
                var updatedata = cursor.value;
                updatedata.IDleng = idleng;
                var result = cursor.update(updatedata);
                result.onsuccess = onSuccessCallback;
            }
        };
        openRequest.onerror = onErrorCallBack;
    }
}

SysCfg.ini.DBLocalLeng.prototype.Read = function (idleng, onSuccessCallback, onErrorCallBack) {
    var _this = this.TParent();

    var openRequest = _this.indexedDB.open(this.DBName, this.Version);
    openRequest.onupgradeneeded = function (e) {
        // Create Traslate
        var thisDb = e.target.result;
        if (!thisDb.objectStoreNames.contains("ini")) {
            //console.log("I need to make the Traslate objectstore");
            var objectStore = thisDb.createObjectStore("ini", { keyPath: "Idx", autoIncrement: true });
            objectStore.createIndex('IDleng', 'IDleng', { unique: false });
        }
    };
    openRequest.onsuccess = function (e) {
        var thisDb = e.target.result;
        var transaction = thisDb.transaction(["ini"], "readwrite");
        var objectStore = transaction.objectStore("ini");
        objectStore.openCursor().onsuccess = function (e) {
            var res = false;
            var data = null;

            var cursor = e.target.result;
            if (cursor) {
                //como ya existe cargamos resultados
                res = true;
                data = cursor.value;
            }
            var add;
            if (res)
                objectStore.get(data.Idx).onsuccess = onSuccessCallback;
            else {
                add = objectStore.put({ IDleng: idleng });
                add.onsuccess = function (e) {
                    objectStore.openCursor().onsuccess = function (e) {
                        var res2 = false;
                        var data2 = null;
                        var cursor2 = e.target.result;
                        if (cursor2) {
                            //como ya existe cargamos resultados
                            res2 = true;
                            data2 = cursor2.value;
                        }
                        if (res2)
                            objectStore.get(data2.Idx).onsuccess = onSuccessCallback;
                    }
                }
                add.onerror = onErrorCallBack;
            }

        }

    };
    openRequest.onerror = onErrorCallBack;
}