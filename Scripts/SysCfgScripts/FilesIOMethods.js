﻿
SysCfg.FilesIO.Methods.file_Exists = function (file) {
    var respuesta = false;
    try {
        if (fs.accessSync(file)) {
            respuesta = true;
            //existe
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("FilesIOMethods.js SysCfg.FilesIO.Methods.file_Exists", e);
        // no existe
    }
    return respuesta;
}

