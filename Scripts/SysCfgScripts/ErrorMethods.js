﻿SysCfg.Error.Methods.CopyResErr = function(ResErrDest,ResErrSurce)
{
     ResErrDest.NotError = ResErrSurce.NotError;
     ResErrDest.Mesaje = ResErrSurce.Mesaje;
     ResErrDest.Exception = ResErrSurce.Exception;
     ResErrDest.Date = ResErrSurce.Date;
}

SysCfg.Error.Methods.ResErrToByte = function(MemStream,ResErrSurce)
{         
    SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, ResErrSurce.NotError);
    SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream,ResErrSurce.Mesaje);
    SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream,ResErrSurce.Exception);
    SysCfg.Stream.Methods.WriteStreamDateTime(MemStream,ResErrSurce.Date);         
}

SysCfg.Error.Methods.ByteToResErr = function(MemStream)
{
    var ResErrSurce = new SysCfg.Error.Properties.TResErr();
    ResErrSurce.NotError = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
    ResErrSurce.Mesaje = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    ResErrSurce.Exception = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    ResErrSurce.Date = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
    return (ResErrSurce);
}  
