﻿/*Bibliografia
integer https://stackoverflow.com/questions/8482309/converting-javascript-integer-to-byte-array-and-back
data reader http://jsfromhell.com/classes/binary-parser
*/    


SysCfg.Stream.TMemoryStream = function () {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this._Position = -1;
    this.Stream = new Array();
    Object.defineProperty(this, 'Position', {
        get: function () {
            return this._Position;
        },
        set: function (Value) {
            this._Position = Value;
            if (_this.Position > _this.Stream.length - 1) {
                _this.Position = -1;
            }
            else if ((_this.Position == -1) && (this.Stream.length > 0)) {
                _this.Position = 0;
            }

        }
    });
    //this.Close = function () {
    //}
    //this.Dispose = function () {
    //}
    //function ToArray() {
    ////this.ToArray = function () {
    //    var Parent = TParent();
    //    var UnArray = new Array(Parent.Stream.length);
    //    for (var i = 0; i < UnArray.length; i++) {
    //        UnArray[i] = Parent.Stream[i];
    //    }
    //    return this.Stream;
    //}
}

SysCfg.Stream.TMemoryStream.prototype.Length = function () {
    var _this = this.TParent();
    return _this.Stream.length;
}

SysCfg.Stream.TMemoryStream.prototype.Close = function () {
    var _this = this.TParent();
}

SysCfg.Stream.TMemoryStream.prototype.Dispose = function () {
    var _this = this.TParent();
}

SysCfg.Stream.TMemoryStream.prototype.ToArray = function () {
    var _this = this.TParent();
    var UnArray = new Array(_this.Stream.length);
    for (var i = 0; i < UnArray.length; i++) {
        UnArray[i] = _this.Stream[i];
    }
    return (UnArray);
}


SysCfg.Stream.TMemoryStream.prototype.Write = function (buffer, offset, count) {
    var _this = this.TParent();
    //Corta el sobrante apartir de position 
    if (_this.Position > _this.Stream.length - 1)
        _this.Records.splice(_this.Position);
    for (var i = offset; i < offset + count; i++) {
        _this.Stream.push(buffer[i]);
        _this.Position++;
    }
}

SysCfg.Stream.Methods.ReadStreamInt16 = function (MemStream) {
    //var TInt32 = new Uint8Array(2);
    //for (var i =0; i < TInt32.length; i++) {
    //    TInt32[i] = MemStream.Stream[MemStream.Position];
    //    MemStream.Position++;
    //}
    //var value = 0;
    //for ( var i = TInt32 .length - 1; i >= 0; i--) {
    //    value = (value * 256) + TInt32[i];
    //}
    //return value;
    var TInt16 = new Uint8Array(2);
    for (var i = 0; i < TInt16.length; i++) {
        TInt16[TInt16.length - i - 1] = MemStream.Stream[MemStream.Position];
        MemStream.Position++;
    }
    var value = 0;
    BP = new BinaryParser(true, true);
    value = BP.toInt16(String.fromCharCode.apply(String, TInt16));
    return value;

}

SysCfg.Stream.Methods.WriteStreamInt16 = function (MemStream, NInt16) {
    //if (NInt16 == undefined) NInt16 = 0;
    //var BInt32 = new Uint8Array(2);
    //for (var index = 0; index < BInt32.length; index++) {
    //    var byte = NInt16 & 0xff;
    //    BInt32[index] = byte;
    //    NInt16 = (NInt16 - byte) / 256;
    //}
    //MemStream.Write(BInt32, 0, 2);
    if (NInt16 == undefined) NInt16 = 0;
    var BInt16 = new Uint8Array(2);
    BP = new BinaryParser(true, true);
    var BBPInt16 = BP.fromInt16(NInt16);
    for (var i = 0; i < BBPInt16.length; i++) {
        BInt16[BBPInt16.length - i - 1] = BBPInt16.charCodeAt(i);
    }
    MemStream.Write(BInt16, 0, BInt16.length);
}

SysCfg.Stream.Methods.ReadStreamInt32 = function (MemStream) {
    var TInt32 = new Uint8Array(4);
    for (var i = 0; i < TInt32.length; i++) {
        TInt32[TInt32.length - i - 1] = MemStream.Stream[MemStream.Position];
        MemStream.Position++;
    }
    var value = 0;
    BP = new BinaryParser(true, true);
    value = BP.toInt32(String.fromCharCode.apply(String, TInt32));
    return value;
}

SysCfg.Stream.Methods.WriteStreamInt32 = function (MemStream, NInt32) {
    if (NInt32 == undefined) NInt32 = 0;
    var BInt32 = new Uint8Array(4);
    BP = new BinaryParser(true, true);
    var BBPInt32 = BP.fromInt32(NInt32);
    for (var i = 0; i < BBPInt32.length; i++) {
        BInt32[BBPInt32.length - i - 1] = BBPInt32.charCodeAt(i);
    }
    MemStream.Write(BInt32, 0, BInt32.length);
}

SysCfg.Stream.Methods.ReadStreamInt64 = function (MemStream) {
    //var TInt64 = new Uint8Array(8);
    //for (var i = 0; i < TInt64.length; i++) {
    //    TInt64[i] = MemStream.Stream[MemStream.Position];
    //    MemStream.Position++;
    //}
    //var value = 0;
    //for (var i = TInt64.length - 1; i >= 0; i--) {
    //    value = (value * 256) + TInt64[i];
    //}
    //return value;
    var TInt64 = new Uint8Array(8);
    for (var i = 0; i < TInt64.length; i++) {
        TInt64[TInt64.length - i - 1] = MemStream.Stream[MemStream.Position];
        MemStream.Position++;
    }
    var value = 0;
    BP = new BinaryParser(true, true);
    value = BP.toInt64(String.fromCharCode.apply(String, TInt64));
    return value;

}

SysCfg.Stream.Methods.WriteStreamInt64 = function (MemStream, NInt64) {
    //if (NInt64 == undefined) NInt64 = 0;
    //var BInt64 = new Uint8Array(4);
    //for (var index = 0; index < BInt64.length; index++) {
    //    var byte = NInt64 & 0xff;
    //    BInt64[index] = byte;
    //    NInt64 = (NInt64 - byte) / 256;
    //}
    //MemStream.Write(BInt64, 0, 8);
    if (NInt64 == undefined) NInt64 = 0;
    var BInt64 = new Uint8Array(8);
    BP = new BinaryParser(true, true);
    var BBPInt64 = BP.fromInt64(NInt64);
    for (var i = 0; i < BBPInt64.length; i++) {
        BInt64[BBPInt64.length - i - 1] = BBPInt64.charCodeAt(i);
    }
    MemStream.Write(BInt64, 0, BInt64.length);
}

SysCfg.Stream.Methods.ReadStreamStrInt16 = function (MemStream) {
    var Smallint = SysCfg.Stream.Methods.ReadStreamInt16(MemStream);
    var StrShortint = new Uint8Array(Smallint);
    for (var i = 0; i < StrShortint.length; i++) {
        StrShortint[i] = MemStream.Stream[MemStream.Position];
        MemStream.Position++;
    }
    var value = "";
    value = String.fromCharCode.apply(String, StrShortint);
    return value;
}

SysCfg.Stream.Methods.WriteStreamStrInt16 = function (MemStream, StrInt16) {
    if (StrInt16 == undefined) StrInt16 = "";
    SysCfg.Stream.Methods.WriteStreamInt16(MemStream, StrInt16.length);
    var BStrInt16 = new Uint8Array(StrInt16.length)
    for (var i = 0; i < BStrInt16.length; i++) {
        BStrInt16[i] = StrInt16.charCodeAt(i);
    }
    MemStream.Write(BStrInt16, 0, BStrInt16.length);
}


SysCfg.Stream.Methods.ReadStreamByteInt32 = function (MemStream) {
    var UnInt32 = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    var StrShortint = new Array(UnInt32);
    MemStream.Read(StrShortint, 0, UnInt32);
    return (StrShortint);
}


SysCfg.Stream.Methods.WriteStreamByteInt32 = function (MemStream, ByteInt32) {
    if (ByteInt32 == undefined) ByteInt32 = new Array();
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, ByteInt32.length);
    MemStream.Write(ByteInt32, 0, ByteInt32.length);
}





SysCfg.Stream.Methods.ReadStreamDouble = function (MemStream) {
    //var BDoble = new Uint8Array(8);
    //for (var i = 0; i < BDoble.length; i++) {
    //    BDoble[BDoble.length-i-1] = MemStream.Stream[MemStream.Position];
    //    MemStream.Position++;
    //};
    //var value = 0;
    //value = fromIEEE754(BDoble, 11, 52);
    ////value = fromIEEE754(BDoble, 8, 23);
    //return value;
    var TDouble = new Uint8Array(8);
    for (var i = 0; i < TDouble.length; i++) {
        TDouble[TDouble.length - i - 1] = MemStream.Stream[MemStream.Position];
        MemStream.Position++;
    }
    var value = 0;
    BP = new BinaryParser(true, true);
    value = BP.toDouble(String.fromCharCode.apply(String, TDouble));
    return value;

}

SysCfg.Stream.Methods.WriteStreamDouble = function (MemStream, NDouble) {
    //if (NDouble == undefined) NDouble = 0;
    //var BDouble = new Uint8Array(8);
    //bytes = toIEEE754(NDouble, 11, 52);
    //for (var i =0; i <  BDouble.length; i++) {
    //    BDouble[BDouble.length - i-1 ] = bytes[i];
    //}
    //MemStream.Write(BDouble, 0, BDouble.length);
    if (NDouble == undefined) NDouble = 0;
    var BDouble = new Uint8Array(8);
    BP = new BinaryParser(true, true);
    var BBPDouble = BP.fromDouble(NDouble);
    for (var i = 0; i < BBPDouble.length; i++) {
        BDouble[BBPDouble.length - i - 1] = BBPDouble.charCodeAt(i);
    }
    MemStream.Write(BDouble, 0, BDouble.length);
}

SysCfg.Stream.Methods.ReadStreamDecimal = function (MemStream) {
    var TDecimal = new Uint8Array(16);
    for (var i = 0; i < TDecimal.length; i++) {
        TDecimal[TDecimal.length - i - 1] = MemStream.Stream[MemStream.Position];
        MemStream.Position++;
    }
    var value = 0;
    BP = new BinaryParser(true, true);
    value = BP.toUInt128(String.fromCharCode.apply(String, TDecimal));
    return value;

}

SysCfg.Stream.Methods.WriteStreamDecimal = function (MemStream, NDecimal) {
    if (NDecimal == undefined) NDecimal = 0;
    var BDecimal = new Uint8Array(16);
    BP = new BinaryParser(true, true);
    var BBPDecimal = BP.fromUInt128(NDecimal);
    for (var i = 0; i < BBPDecimal.length; i++) {
        BDecimal[BBPDecimal.length - i - 1] = BBPDecimal.charCodeAt(i);
    }
    MemStream.Write(BDecimal, 0, BDecimal.length);
}

SysCfg.Stream.Methods.ReadStreamBoolean = function (MemStream) {

    var TBoolean = new Uint8Array(1);
    for (var i = 0; i < TBoolean.length; i++) {
        TBoolean[i] = MemStream.Stream[MemStream.Position];
        MemStream.Position++;
    }
    var value2 = 0;
    for (var i = TBoolean.length - 1; i >= 0; i--) {
        value2 = (value2 * 256) + TBoolean[i];
    }
    return !(value2 == 0);


    //Hay un problema con el write parece que read esta bien
    //var TBoolean = new Uint8Array(1);
    //for (var i = 0; i < TBoolean.length; i++) {
    //    TBoolean[TBoolean.length - i - 1] = MemStream.Stream[MemStream.Position];
    //    MemStream.Position++;
    //}
    //var value = 0;
    //BP = new BinaryParser(true, true);
    //value = BP.toUInt8(String.fromCharCode.apply(String, TBoolean));
    //return (value==1);
}

SysCfg.Stream.Methods.WriteStreamBoolean = function (MemStream, inVBoolean) {
    if (inVBoolean == undefined) VBoolean = false;
    if (inVBoolean) VBoolean = 1;
    else VBoolean = 0;

    var BBoolean = new Uint8Array(1);
    for (var index = 0; index < BBoolean.length; index++) {
        var byte = VBoolean & 0xff;
        BBoolean[index] = byte;
        VBoolean = (VBoolean - byte) / 256;
    }
    MemStream.Write(BBoolean, 0, BBoolean.length);

    //Hay un problema con el write parece que read esta bien
    //if (VBoolean == undefined) VBoolean = 0;
    //if (VBoolean) VBoolean = 1;
    //else VBoolean = 0;
    //var BBoolean = new Uint8Array(1);
    //BP = new BinaryParser(true, true);
    //var BBPBoolean = BP.fromUInt8(VBoolean);
    //for (var i = 0; i < BBPBoolean.length; i++) {
    //    BBoolean[BBPBoolean.length - i - 1] = BBPBoolean.charCodeAt(i);
    //}
    //MemStream.Write(BBoolean, 0, BBoolean.length);
}

SysCfg.Stream.Methods.WriteStreamByte = function (MemStream, inVByte) {
    var BByte = new Uint8Array(1);
    var VByte = inVByte;
    for (var index = 0; index < BByte.length; index++) {
        var byte = VByte & 0xff;
        BByte[index] = byte;
        VByte = (VByte - byte) / 256;
    }
    MemStream.Write(BByte, 0, BByte.length);
}


SysCfg.Stream.Methods.ReadStreamByte = function (MemStream)
{
    var BByte = new Uint8Array(1);
    for (var i = 0; i < TBoolean.length; i++) {
        BByte[i] = MemStream.Stream[MemStream.Position];
        MemStream.Position++;
    }
    var value2 = 0;
    for (var i = BByte.length - 1; i >= 0; i--) {
        value2 = (value2 * 256) + BByte[i];
    }
    return (value2);
}
 


SysCfg.Stream.Methods.ReadStreamDateTime = function (MemStream) {
    var TDateTime = new Uint8Array(8);
    for (var i = 0; i < TDateTime.length; i++) {
        TDateTime[TDateTime.length - i - 1] = MemStream.Stream[MemStream.Position];
        MemStream.Position++;
    }
    var value = 0;
    BP = new BinaryParser(true, true);
    value = BP.toDouble(String.fromCharCode.apply(String, TDateTime));
    return (new Date(value));
}

SysCfg.Stream.Methods.WriteStreamDateTime = function (MemStream, NDateTime) {
    https://javascript.info/date

    if (NDateTime == undefined) NDateTime = new Date(1970, 0, 1, 0, 0, 0);
    var BDateTime = new Uint8Array(8);
    BP = new BinaryParser(true, true);
    var BBPDateTime = BP.fromDouble(NDateTime.getTime());
    for (var i = 0; i < BBPDateTime.length; i++) {
        BDateTime[BBPDateTime.length - i - 1] = BBPDateTime.charCodeAt(i);
    }
    MemStream.Write(BDateTime, 0, BDateTime.length);
}
