﻿SysCfg.DB.SQL.Methods.GetSQLPrefixIDBuild = function (Prefix, ID, SQLPARAM) {
    UnGetSQLPrefixIDBuild = new SysCfg.DB.SQL.Properties.TGetSQLPrefixIDBuild();
    SysCfg.Error.Properties.ResErrfill(UnGetSQLPrefixIDBuild.ResErr);
    try {
        GetSQLPrefixIDBuild = Comunic.AjaxJson.Client.Methods.GetSQLPrefixIDBuild(Prefix, ID, SQLPARAM);
        UnGetSQLPrefixIDBuild.ResErr = GetSQLPrefixIDBuild.ResErr;
        if (GetSQLPrefixIDBuild.ResErr.NotError) {
            UnGetSQLPrefixIDBuild.StrSQL = GetSQLPrefixIDBuild.StrSQL;
        }
    }
    catch (Excep) {
        UnGetSQLPrefixIDBuild.ResErr.Mesaje = Excep;
    }
    return (UnGetSQLPrefixIDBuild);
}

SysCfg.DB.SQL.Methods.Open = function () {  
    Open = new SysCfg.DB.SQL.Properties.TOpen();
    SysCfg.Error.Properties.ResErrfill(Open.ResErr);
    if (arguments.length == 3) {
        Prefix = arguments[0];
        ID = arguments[1];
        SQLPARAM = arguments[2];
        GetSQLOpen = Comunic.AjaxJson.Client.Methods.GetSQLOpen(Prefix, ID, SQLPARAM);
    }
    else if (arguments.length == 2) {
        StrSQL = arguments[0];
        SQLPARAM = arguments[1];
        GetSQLOpen = Comunic.AjaxJson.Client.Methods.GetStrSQLOpen(StrSQL, SQLPARAM);
    }
    else {
        Open.ResErr.Mesaje = "Param error";
    }

    if (GetSQLOpen != undefined) {        
        try {
            Open.ResErr = GetSQLOpen.ResErr;
            if (GetSQLOpen.ResErr.NotError) {
                BytetoDataSet = SysCfg.MemTable.Properties.BytetoDataSet(GetSQLOpen.SQLOpen, false);
                if (GetSQLOpen.ResErr.NotError) {
                    //****** Data table en lugar del datatable y observable colection ******
                    Open.DataSet = BytetoDataSet.DataSet;
                    Open.DataSet.EnableControls = false;
                    Open.DataSet.First();
                    Open.ResErr = GetSQLOpen.ResErr;
                    //****** Datos extras ******
                    Open.FieldCount = BytetoDataSet.FieldCount;
                    Open.FieldName = BytetoDataSet.FieldName;
                    Open.DataType = BytetoDataSet.DataType;
                    Open.FieldSize = BytetoDataSet.FieldSize;
                }
                else {
                    Open.ResErr = BytetoDataSet.ResErr;
                }
            }
        }
        catch (Excep) {
            Open.ResErr.Mesaje = Excep;
        }
    }
    return (Open);
}

SysCfg.DB.SQL.Methods.OpenDataSet = function ()
{
    var OpenDataSet = new SysCfg.DB.SQL.Properties.TOpenDataSet();
    SysCfg.Error.Properties.ResErrfill(OpenDataSet.ResErr);
    if (arguments.length == 3) {
        Prefix = arguments[0];
        ID = arguments[1];
        SQLPARAM = arguments[2];                     
        GetSQLOpen = Comunic.AjaxJson.Client.Methods.GetSQLOpen(Prefix, ID, SQLPARAM);
    }
    else if (arguments.length == 2) {
        StrSQL = arguments[0];
        SQLPARAM = arguments[1];        
        GetSQLOpen = Comunic.AjaxJson.Client.Methods.GetStrSQLOpen(StrSQL, SQLPARAM);
    }
    else {
        Open.ResErr.Mesaje = "Param error";
    }
    if (GetSQLOpen != undefined) {
        
        try {
            OpenDataSet.ResErr = GetSQLOpen.ResErr;
            if (GetSQLOpen.ResErr.NotError) {
                try {
                    BytetoDataSet = SysCfg.MemTable.Properties.BytetoDataSet(GetSQLOpen.SQLOpen, false);
                    if (BytetoDataSet.ResErr.NotError) {
                        OpenDataSet.DataSet = BytetoDataSet.DataSet;
                        OpenDataSet.DataSet.EnableControls = false;
                        OpenDataSet.DataSet.First();
                        OpenDataSet.ResErr = GetSQLOpen.ResErr;
                    }
                }
                catch (Excep) {
                    OpenDataSet.ResErr.Mesaje = Excep;
                }
            }
        }
        catch (Excep) {
            OpenDataSet.ResErr.Mesaje = Excep;
        }
    }
    return (OpenDataSet); 
}

SysCfg.DB.SQL.Methods.Exec = function ()
{
    Exec = new SysCfg.DB.SQL.Properties.TExec();
    SysCfg.Error.Properties.ResErrfill(Exec.ResErr);
    if (arguments.length == 3) {
        Prefix = arguments[0];
        ID = arguments[1];
        SQLPARAM = arguments[2];                     
        GetSQLExec = Comunic.AjaxJson.Client.Methods.GetSQLExec(Prefix, ID, SQLPARAM);
    }
    else if (arguments.length == 2) {
        StrSQL = arguments[0];
        SQLPARAM = arguments[1];        
        GetSQLExec = Comunic.AjaxJson.Client.Methods.GetStrSQLExec(StrSQL, SQLPARAM);
    }
    else {
        Open.ResErr.Mesaje = "Param error";
    }
    if (GetSQLExec != undefined) {
        Exec.ResErr = GetSQLExec.ResErr;
    }
    return (Exec);
}

