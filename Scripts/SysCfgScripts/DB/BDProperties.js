﻿SysCfg.DB.TField = function () {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.FieldName = "";
    this.TableName = "";
    this.DataType = SysCfg.DB.Properties.TDataType.Unknow;
    this._Header = "";
    Object.defineProperty(this, 'Header', {
        get: function () {
            if (this._Header == "") {
                this._Header = SysCfg.IDI.Client.GetLangText(SysCfg.App.TApplication.Common, "db." + this.TableName, this.FieldName, this.FieldName, SysCfg.App.Properties.Config_Language.LanguageITF);
            }
            if (this._Header == "") this._Header = "-" + this.FieldName;
            //return "xCmBdx" + this._Header + "x";

            return this._Header;
        },
        set: function (inValue) {
            this._Header = inValue;
        }
    });
    this.Size = 0;
}

SysCfg.DB.TSQLExec = function () {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.ResErr = new SysCfg.Error.Properties.TResErr();
}
   
SysCfg.DB.TSQLOpentoByte = function () {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.ResErr = new SysCfg.Error.Properties.TResErr();
    this.TblByte;
}
 
SysCfg.DB.TParam = function () {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.ParamName;
    this.DataType = SysCfg.DB.Properties.TDataType.Unknow;
    this.Size;
    this.Pos;
    this.Value;
}

SysCfg.DB.TSQL = function () {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.ResErr = new SysCfg.Error.Properties.TResErr();
    this.StrSql;
    this.StrLog;       
}

SysCfg.DB.TSQLParam = function () {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.ResErr = new SysCfg.Error.Properties.TResErr();
    this.StrSql;
    this.Param = new Array();
}

SysCfg.DB.Properties.Field_list = new Array();

SysCfg.DB.Properties.TBDDAccess = {
    GetEnum: function (Param) {return (SysCfg.GetEnum(Param, this));},
    _None: { value: -1, name: "None" },
    _ADO: { value: 1, name: "ADO" }    
}

SysCfg.DB.Properties.TBddModeType = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _Mdb: { value: 0, name: "mdb"},
    _Mdf: { value: 1, name: "mdf"},
    _Orc: { value: 2, name: "orc"}
}

SysCfg.DB.Properties.TMotor = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    None: { value: 0, name: "None"},
    OLE: { value: 1, name: "OLE"}
}

SysCfg.DB.Properties.TExtra = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    None: { value: 0, name: "None"},
    Equal: { value: 1, name: "Equal"},
    Field: { value: 2, name: "Field"}
}

SysCfg.DB.Properties.TStyle = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    Normal: { value: 0, name: "Normal"},
    Where: { value: 1, name: "Where"}           
}

SysCfg.DB.Properties.TKeyType = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    EXT: { value: 0, name: "EXT" },
    ADN: { value: 1, name: "ADN" },
    VAR: { value: 2, name: "VAR" },
    NEW: { value: 3, name: "NEW" },
    INS: { value: 4, name: "INS" },
    LOG: { value: 5, name: "LOG" }
}



SysCfg.DB.Properties.TDataType = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    Unknown: { value: 0, name: "Unknown" },
    String: { value: 1, name: "String" },
    Int32: { value: 2, name: "Int32" },
    Boolean: { value: 3, name: "Boolean" },
    AutoInc: { value: 4, name: "AutoInc" },
    Text: { value: 5, name: "Text" },
    Double: { value: 6, name: "Double" },
    DateTime: { value: 7, name: "DateTime" },
    Object: { value: 8, name: "Object" },
    Decimal: { value: 9, name: "Decimal" },
}

SysCfg.DB.Properties._IdetMdbString1 = "TEXT(";
SysCfg.DB.Properties._IdetMdbInt321 = "INT";
SysCfg.DB.Properties._IdetMdbBoolean1 = "BIT";
SysCfg.DB.Properties._IdetMdbAutoInc1 = "COUNTER";
SysCfg.DB.Properties._IdetMdbText1 = "LONGTEXT";
SysCfg.DB.Properties._IdetMdbDouble1 = "DOUBLE";        
SysCfg.DB.Properties._IdetMdbDateTime1 = "DATETIME";

SysCfg.DB.Properties._IdetMdfString1 = "VARCHAR(";
SysCfg.DB.Properties._IdetMdfInt321 = "INT";
SysCfg.DB.Properties._IdetMdfBoolean1 = "BIT";
SysCfg.DB.Properties._IdetMdfAutoInc1 = "IDENTITY";
SysCfg.DB.Properties._IdetMdfText1 = "TEXT";
SysCfg.DB.Properties._IdetMdfDouble1 = "FLOAT";
SysCfg.DB.Properties._IdetMdfDateTime1 = "DATETIME";

SysCfg.DB.Properties._IdetOrcString1 = "VARCHAR2(";
SysCfg.DB.Properties._IdetOrcInt321 = "NUMBER";
SysCfg.DB.Properties._IdetOrcBoolean1 = "NUMBER";
SysCfg.DB.Properties._IdetOrcBoolean2 = ")CHECK(";
SysCfg.DB.Properties._IdetOrcAutoInc1 = "NUMBER(";
SysCfg.DB.Properties._IdetOrcAutoInc2 = "COUNTER";

SysCfg.DB.Properties._IdetOrcText1 = "CLOB";
SysCfg.DB.Properties._IdetOrcDouble1 = "NUMBER(";
SysCfg.DB.Properties._IdetOrcDateTime1 = "DATE";



SysCfg.DB.Properties.TCfg_Bdd = function () {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    
    this.NumDateTimeServer=0;
    this.NUmDateTimeNow=0;
    this.FechaServidor = 0;
    this.BddAccess=SysCfg.DB.Properties.TBDDAccess._None;
    this.BDDInvalida = "false";
    this.UDLlink = "";
    this.ExtDataBase ="";//
    this.BddMode = SysCfg.DB.Properties.TBddModeType._Mdf;
    
    this.FormatoTime = "hh.nn.ss am/pm";
    this.FormatoDate = "mm/dd/yyyy";
    this.FormatoFecha = "#hh.nn.ss am/pm mm/dd/yyyy#";
    this.FormatoTimeStr = "hh.nn.ss am/pm";
    this.FormatoDateStr = "mm/dd/yyyy";
    this.FormatoFechaStr = "#hh.nn.ss am/pm mm/dd/yyyy#";

}
SysCfg.DB.Properties.Cfg_Bdd = new SysCfg.DB.Properties.TCfg_Bdd();

      
SysCfg.DB.Properties.toType = function (inDataType) {
    //https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Operadores/typeof
    var MyType = typeof('undefined');
    switch (inDataType)
    {        
        case SysCfg.DB.Properties.TDataType.String: MyType= typeof('string'); break;
        case SysCfg.DB.Properties.TDataType.Text: MyType = typeof('string'); break;
        case SysCfg.DB.Properties.TDataType.Int32: MyType = typeof('number'); break;
        case SysCfg.DB.Properties.TDataType.Double: MyType = typeof('number'); break;
        case SysCfg.DB.Properties.TDataType.Decimal: MyType = typeof('number'); break;
        case SysCfg.DB.Properties.TDataType.Boolean: MyType = typeof('boolean'); break;
        case SysCfg.DB.Properties.TDataType.DateTime: MyType = typeof('object'); break;
        case SysCfg.DB.Properties.TDataType.Object: MyType = typeof('object'); break;
              
    }
    return (MyType);
}

SysCfg.DB.Properties.toDataType = function (MyType)
{
    MyDataType = SysCfg.DB.Properties.TDataType.Unknown;
    if (MyType==typeof('string'))
    {
        MyDataType=SysCfg.DB.Properties.TDataType.String;
    }
    else if (MyType == typeof('number'))
    {
        //https://developer.mozilla.org/es/docs/Web/JavaScript/Guide/Funciones
        //var floatValue = parseFloat(toFloat);
        //if (isNaN(floatValue)) {
        //    notFloat();
        //} else {
        //    isFloat();
        //}
        if (isNaN(MyType)) {
            MyDataType = SysCfg.DB.Properties.TDataType.Int32;
        } else {
            MyDataType = SysCfg.DB.Properties.TDataType.Double;
        }


        
    }
    else if (MyType == typeof('boolean'))
    {
        MyDataType = SysCfg.DB.Properties.TDataType.Boolean;
    }
    else if (MyType == typeof('object'))
    {
        MyDataType = SysCfg.DB.Properties.TDataType.Object;
    }
    return (MyDataType);
}


SysCfg.DB.Properties.SVRNOW = function ()
{
    //https://www.w3schools.com/jsref/jsref_obj_date.asp
    UnDatetime = new Date(Date.now() + SysCfg.DB.Properties.Cfg_Bdd.FechaServidor);
    return (new Date(UnDatetime));
}


SysCfg.DB.Properties.Cfg_BddToByte = function (MemStream,Cfg_BddSurce)
{

    SysCfg.Stream.Methods.WriteStreamDouble(MemStream, Convert.ToDouble(SysCfg.DB.Properties.SVRNOW().ToOADate()));
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, Cfg_Bdd.BddAccess);
    SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, Cfg_Bdd.BDDInvalida);
    SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, Cfg_Bdd.UDLlink);
    SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, Cfg_Bdd.ExtDataBase);
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, Cfg_Bdd.BddMode);
    SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, Cfg_Bdd.FormatoTime);
    SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, Cfg_Bdd.FormatoDate);
    SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, Cfg_Bdd.FormatoFecha);
    SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, Cfg_Bdd.FormatoTimeStr);
    SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, Cfg_Bdd.FormatoDateStr);
    SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, Cfg_Bdd.FormatoFechaStr);
}

SysCfg.DB.Properties.ByteToCfg_Bdd = function (MemStream)
{
    Cfg_BddSurce = new SysCfg.DB.Properties.TCfg_Bdd();
    Cfg_BddSurce.NumDateTimeServer = SysCfg.Stream.Methods.ReadStreamDouble(MemStream);
    Cfg_BddSurce.NUmDateTimeNow = Date.now();
    SysCfg.DB.Properties.Cfg_Bdd.FechaServidor = Cfg_BddSurce.NumDateTimeServer - Cfg_BddSurce.NUmDateTimeNow;
    Cfg_BddSurce.BddAccess = SysCfg.DB.Properties.TBDDAccess.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
    Cfg_BddSurce.BDDInvalida = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
    Cfg_BddSurce.UDLlink = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    Cfg_BddSurce.ExtDataBase = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    Cfg_BddSurce.BddMode = SysCfg.DB.Properties.TBddModeType.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
    Cfg_BddSurce.FormatoTime = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    Cfg_BddSurce.FormatoDate = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    Cfg_BddSurce.FormatoFecha = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    Cfg_BddSurce.FormatoTimeStr = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    Cfg_BddSurce.FormatoDateStr = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    Cfg_BddSurce.FormatoFechaStr = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);

    return (Cfg_BddSurce);
}



