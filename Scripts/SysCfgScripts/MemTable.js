﻿
SysCfg.MemTable.Properties.TBytetoDataSet = function () {
    this.ResErr = new SysCfg.Error.Properties.TResErr();
    this.DataSet;
}

SysCfg.MemTable.Properties.BytetoDataSet = function (TblByte, EnableControls) {
    var BytetoDataSet = new SysCfg.MemTable.Properties.TBytetoDataSet();
    SysCfg.Error.Properties.ResErrfill(BytetoDataSet.ResErr);
    try {
        BytetoDataSet.DataSet = new SysCfg.MemTable.TDataSet();

        BytetoDataSet.DataSet.Inicialize();
        BytetoDataSet.DataSet.EnableControls = false;
        var ContX = 0;
        if (TblByte.length > 0) {
            var MemStream = new SysCfg.Stream.TMemoryStream();
            try {
                MemStream.Write(TblByte, 0, TblByte.length);
                MemStream.Position = 0;
                var FieldCount = SysCfg.Stream.Methods.ReadStreamInt16(MemStream);
                var RecordCount = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
                ContX = 0;
                FieldName = new Array(FieldCount);
                DataType = new Array(FieldCount);
                FieldSize = new Array(FieldCount);
                BytetoDataSet.DataSet.Inicialize();
                for (ContX = 0; ContX <= FieldCount - 1; ContX++) {
                    FieldName[ContX] = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
                    var value = SysCfg.Stream.Methods.ReadStreamInt16(MemStream);
                    DataType[ContX] = SysCfg.DB.Properties.TDataType.GetEnum(value);
                    FieldSize[ContX] = SysCfg.Stream.Methods.ReadStreamInt16(MemStream);
                    BytetoDataSet.DataSet.AddFields(FieldName[ContX], DataType[ContX], FieldSize[ContX]);
                }
                for (var ContY = 0; ContY <= RecordCount - 1; ContY++) {
                    BytetoDataSet.DataSet.Append();
                    for (ContX = 0; ContX <= FieldCount - 1; ContX++) {
                        switch (DataType[ContX]) {
                            case SysCfg.DB.Properties.TDataType.String: BytetoDataSet.DataSet.FieldsIndex(ContX, SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream)); break;
                            case SysCfg.DB.Properties.TDataType.Text: BytetoDataSet.DataSet.FieldsIndex(ContX, SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream)); break;
                            case SysCfg.DB.Properties.TDataType.Int32: BytetoDataSet.DataSet.FieldsIndex(ContX, SysCfg.Stream.Methods.ReadStreamInt32(MemStream)); break;
                            case SysCfg.DB.Properties.TDataType.Double: BytetoDataSet.DataSet.FieldsIndex(ContX, SysCfg.Stream.Methods.ReadStreamDouble(MemStream)); break;
                            case SysCfg.DB.Properties.TDataType.Decimal: BytetoDataSet.DataSet.FieldsIndex(ContX, SysCfg.Stream.Methods.ReadStreamDecimal(MemStream)); break;
                            case SysCfg.DB.Properties.TDataType.Boolean: BytetoDataSet.DataSet.FieldsIndex(ContX, SysCfg.Stream.Methods.ReadStreamBoolean(MemStream)); break;
                            case SysCfg.DB.Properties.TDataType.DateTime: BytetoDataSet.DataSet.FieldsIndex(ContX, SysCfg.Stream.Methods.ReadStreamDateTime(MemStream)); break;
                                //case SysCfg.DB.Properties.TDataType.Object : BytetoDataSet.DataSet.FieldsIndex(ContX, ReadStreamObject(ref MemStream)); break;                                                           
                            default:
                                BytetoDataSet.DataSet.FieldsIndex(ContX, "error:" + SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream));
                                break;
                        }
                    }
                    BytetoDataSet.DataSet.Post();
                }
            }
            finally {
                MemStream.Close();
                MemStream.Dispose();
                BytetoDataSet.DataSet.First();
                BytetoDataSet.DataSet.EnableControls = true;
            }
        }
        BytetoDataSet.ResErr.NotError = true;
        BytetoDataSet.ResErr.Mesaje = SysCfg.Error.Properties._ResOK;
    }
    catch (Excep) {
        BytetoDataSet.ResErr.Mesaje = Excep;
    }
    return (BytetoDataSet);
}

SysCfg.MemTable.Properties.TStatus = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    None: { value: 0, name: "None" },
    Update: { value: 1, name: "Update" },
    Apped: { value: 2, name: "Apped" },
    Delete: { value: 3, name: "Delete" }
}

SysCfg.MemTable.Properties.TField = function () {
    this.TParent = function () {
        return this;
    }.bind(this);
    this.FieldDef = null;


    var _this = this.TParent();
    this.Value = null;

    this.asString = function () {
        if (this.Value == null) return ("");
        else return (this.Value);
    }
    this.asText = function () {
        if (this.Value == null) return ("");
        else return (this.Value);
    }
    this.asInt32 = function () {
        if (this.Value == null) return (0);
        else return (parseInt(this.Value));
    }
    this.asDouble = function () {
        if (this.Value == null) return (0.00);
        else return (parseFloat(this.Value));
    }
    this.asDecimal = function () {
        if (this.Value == null) return (0.00);
        else return (parseFloat(this.Value));
    }
    this.asBoolean = function () {
        if (this.Value == null) return (false);
        else return (this.Value);
    }
    this.asDateTime = function () {
        if (this.Value == null) return (Date.now());
        else return (this.Value);
    }
    this.asObject = function () {
        if (this.Value == null) return (null);
        else return (this.MValue);
    }

}

SysCfg.MemTable.Properties.TFieldDef = function () {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.FieldName = "";
    this.DataType;
    this.Size;
}

SysCfg.MemTable.Properties.TRecord = function () {

    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.Fields = new Array();
    this.FieldDefs = new Array();
    this.FieldName = function (inFieldName) {
        var Field = null;
        for (var ContX = 0; ContX <= this.FieldDefs.length - 1; ContX++) {
            if (SysCfg.Str.isEqual(this.FieldDefs[ContX].FieldName, inFieldName)) {
                Field = this.Fields[ContX];
                return (Field);
            }
        }
        return (Field);
    }
}

SysCfg.MemTable.TDataSet = function () {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.FieldCount;
    this.RecordCount;
    this.FieldDefs = new Array();
    this.Records = new Array();
    this.RecordSet;
    this.Index;
    Object.defineProperty(this, 'Index', {
        get: function () {
            return this._Index;
        },
        set: function (Value) {
            this._Index = Value;
            _this.ChangeIndex(this._Index);
        }
    });
    this.Bof;
    this.Eof;
    this.ShowMessageError;
    this._EnableControls;
    Object.defineProperty(this, 'EnableControls', {
        get: function () {
            return this._EnableControls;
        },
        set: function (Value) {
            this._EnableControls = Value;
            _this.Refresch(_this.EnableControls);
            _this.ChangeIndex(this._Index);
            _this.RefreschRecordSet(_this.RecordSet);
        }
    });
    this.OnRefreschRecordSet = null;//evento
    this.OnRefresch = null;//evento

    this.OnBeforeChange = null;//evento
    this.OnAfterChange = null;//evento    
    this.OnAddColumn = null;//evento    
    this.OnChangeIndex = null;//evento    

    this.Status;
    this.Inicialize;

    _this.Inicialize();
}

SysCfg.MemTable.TDataSet.prototype.Inicialize = function (inEnableControls) {
    var _this = this.TParent();
    _this.Status = SysCfg.MemTable.Properties.TStatus.None;
    _this.ShowMessageError = false;
    _this.RecordSet = null;
    _this.FieldCount = 0;
    _this.RecordCount = 0;
    _this.FieldDefs = new Array();
    _this.Records = new Array();
    _this.Bof = true;
    _this.Eof = true;
    _this._Index = 0;
    _this.EnableControls = inEnableControls;
    _this.RefreschRecordSet(null);
}

SysCfg.MemTable.TDataSet.prototype.RefreschRecordSet = function (EventArgs) {
    var _this = this.TParent();
    if (_this.EnableControls) {
        if (_this.OnRefreschRecordSet != null) {
            _this.OnRefreschRecordSet(_this, EventArgs);
        }
    }
}

SysCfg.MemTable.TDataSet.prototype.Refresch = function (EventArgs) {
    var _this = this.TParent();
    if (_this.EnableControls) {
        if (_this.OnRefresch != null) {
            _this.OnRefresch(_this, EventArgs);
        }
    }
}

SysCfg.MemTable.TDataSet.prototype.BeforeChange = function (EventArgs) {
    var _this = this.TParent();
    if (_this.EnableControls) {
        if (_this.OnBeforeChange != null) {
            _this.OnBeforeChange(_this, EventArgs);
        }
    }
}

SysCfg.MemTable.TDataSet.prototype.AfterChange = function (EventArgs) {
    var _this = this.TParent();
    if (_this.EnableControls) {
        if (_this.OnAfterChange != null) {
            _this.OnAfterChange(_this, EventArgs);
        }
    }
}

SysCfg.MemTable.TDataSet.prototype.ChangeIndex = function (EventArgs) {
    var _this = this.TParent();
    if (_this.EnableControls) {
        if (_this.OnChangeIndex != null) {
            _this.OnChangeIndex(_this, EventArgs);
        }
    }
}

SysCfg.MemTable.TDataSet.prototype.AddColumn = function (EventArgs) {
    var _this = this.TParent();
    if (_this.EnableControls) {
        if (_this.OnAddColumn != null) {
            _this.OnAddColumn(_this, EventArgs);
        }
    }
}

SysCfg.MemTable.TDataSet.prototype.AddFields = function (FieldName, DataType, FieldSize) {
    var _this = this.TParent();
    for (var i = 0; i < _this.FieldCount; i++) {
        if (SysCfg.Str.isEqual(_this.FieldDefs[i].FieldName, FieldName)) {

            _this.Error("Then field name is duplicate:" + FieldName)
        }
    }
    _this.FieldCount++;
    var FieldDef = new SysCfg.MemTable.Properties.TFieldDef();
    FieldDef.FieldName = FieldName;
    FieldDef.DataType = DataType;
    FieldDef.Size = FieldSize;
    _this.FieldDefs.push(FieldDef);

    if (_this.Records.length > 0) {
        for (var e = 0; e < _this.RecordCount; e++) {
            var Field = new SysCfg.MemTable.Properties.TField();
            Field.FieldDef = FieldDef;
            switch (DataType) {
                case SysCfg.DB.Properties.TDataType.String: Field.Value = ""; break;
                case SysCfg.DB.Properties.TDataType.Text: Field.Value = ""; break;
                case SysCfg.DB.Properties.TDataType.Int32: Field.Value = 0; break;
                case SysCfg.DB.Properties.TDataType.Double: Field.Value = 0.00; break;
                case SysCfg.DB.Properties.TDataType.Decimal: Field.Value = 0.00; break;
                case SysCfg.DB.Properties.TDataType.Boolean: Field.Value = false; break;
                case SysCfg.DB.Properties.TDataType.DateTime: Field.Value = Date.now(); break;
                case SysCfg.DB.Properties.TDataType.Object: Field.Value = null; break;
            }
            _this.Records[e].Fields.push(Field);
        }
    }
    _this.AddColumn(FieldDef);

}

SysCfg.MemTable.TDataSet.prototype.FieldsIndex = function (IndexField, Value) {
    var _this = this.TParent();
    var Field = _this.RecordSet.Fields[IndexField];
    Field.Value = Value;
}

SysCfg.MemTable.TDataSet.prototype.FieldsName = function (FieldName, Value) {
    var _this = this.TParent();
    var Field;
    for (var ContX = 0; ContX <= _this.FieldCount - 1; ContX++) {
        if (SysCfg.Str.isEqual(_this.FieldDefs[ContX].FieldName, FieldName)) {
            Field = _this.RecordSet.Fields[ContX];
            switch (_this.FieldDefs[ContX].DataType) {
                case SysCfg.DB.Properties.TDataType.String: Field.Value = Value; break;
                case SysCfg.DB.Properties.TDataType.Text: Field.Value = Value; break;
                case SysCfg.DB.Properties.TDataType.Int32: Field.Value = Value; break;
                case SysCfg.DB.Properties.TDataType.Double: Field.Value = Value; break;
                case SysCfg.DB.Properties.TDataType.Decimal: Field.Value = Value; break;
                case SysCfg.DB.Properties.TDataType.Boolean: Field.Value = Value; break;
                case SysCfg.DB.Properties.TDataType.DateTime: Field.Value = Value; break;
                 case SysCfg.DB.Properties.TDataType.Object: Field.Value = Value; break;
            }
        }
    }
}

SysCfg.MemTable.TDataSet.prototype.Error = function (message) {
    var _this = this.TParent();
    if (_this.ShowMessageError) {
        alert(message);
    };
    //SysCfg.Log.Methods.WriteLog(message, ex);     
}

//***********************************************************************

SysCfg.MemTable.TDataSet.prototype.isUpdate = function () {
    var _this = this.TParent();
    return (((_this.Index >= 0) && (_this.Index <= _this.RecordCount - 1)) && (_this.Status == SysCfg.MemTable.Properties.TStatus.None));
}

SysCfg.MemTable.TDataSet.prototype.Update = function () {
    var _this = this.TParent();
    _this.Status = SysCfg.MemTable.Properties.TStatus.Update;


    //******************************************
    _this.RecordSet = new SysCfg.MemTable.Properties.TRecord();
    _this.RecordSet.Fields = new Array();
    for (var ContX = 0; ContX <= _this.FieldCount - 1; ContX++) {
        var Field = new SysCfg.MemTable.Properties.TField();
        Field.Value = _this.Records[_this.Index].Fields[ContX].Value;
        Field.FieldDef = _this.FieldDefs[ContX];
        _this.RecordSet.Fields.push(Field);
    }

    _this.RecordSet.FieldDefs = _this.FieldDefs;
    //*******************************************

    _this.RefreschRecordSet(_this.RecordSet);//*null
}

SysCfg.MemTable.TDataSet.prototype.isCancel = function () {
    var _this = this.TParent();
    return (!(_this.Status == SysCfg.MemTable.Properties.TStatus.None));
}

SysCfg.MemTable.TDataSet.prototype.Cancel = function () {
    var _this = this.TParent();
    if (_this.Status == SysCfg.MemTable.Properties.TStatus.Apped) {
        _this.RecordSet = _this.Records[_this.Index];
    }
    else if (_this.Status == SysCfg.MemTable.Properties.TStatus.Update) {
        _this.RecordSet = _this.Records[_this.Index];
    }
    else if (_this.Status == SysCfg.MemTable.Properties.TStatus.Delete) {
        _this.RecordSet = _this.Records[_this.Index];
    }

    _this.Status = SysCfg.MemTable.Properties.TStatus.None;
    _this.RefreschRecordSet(_this.RecordSet);//*null

}

SysCfg.MemTable.TDataSet.prototype.isPost = function () {
    var _this = this.TParent();
    return (!(_this.Status == SysCfg.MemTable.Properties.TStatus.None));
}

SysCfg.MemTable.TDataSet.prototype.Post = function () {
    var _this = this.TParent();
    _this.BeforeChange(null);
    if (_this.Status != SysCfg.MemTable.Properties.TStatus.Cancel) {
        _this.AfterChange(null);
    }
    if (_this.Status == SysCfg.MemTable.Properties.TStatus.Apped) {
        _this.Bof = false;
        _this.Eof = false;
        if (_this.RecordCount - 1 == _this.Index) {
            _this.Records.push(_this.RecordSet);
            _this.Index++;
            _this.RecordSet = _this.Records[_this.Index];
        }
        else {
            if (_this.Index == -1) {
                _this.Index++;
                _this.Records.splice(_this.Index, 0, _this.RecordSet);
                _this.RecordSet = _this.Records[_this.Index];
            }
            else {
                _this.Records.splice(_this.Index, 0, _this.RecordSet);
                _this.RecordSet = _this.Records[_this.Index];
            }
        }
        _this.RecordCount++;
        _this.Status = SysCfg.MemTable.Properties.TStatus.None;
        _this.RefreschRecordSet(_this.RecordSet);//*null     
    }
    else if (this.Status == SysCfg.MemTable.Properties.TStatus.Update) {
        _this.Bof = false;
        _this.Eof = false;
        _this.RefreschRecordSet(null);
        _this.Records[_this.Index] = _this.RecordSet;


        var Field;
        for (var ContX = 0; ContX <= _this.FieldCount - 1; ContX++) {
            _this.Records[_this.Index].Fields[ContX].Value = _this.RecordSet.Fields[ContX].Value;
        }


        _this.Status = SysCfg.MemTable.Properties.TStatus.None;
        _this.RefreschRecordSet(_this.RecordSet);//*null
    }
    //RefreschRecordSet(this, null);     
}

SysCfg.MemTable.TDataSet.prototype.isAppend = function () {
    var _this = this.TParent();
    return (_this.Status == SysCfg.MemTable.Properties.TStatus.None);
}

SysCfg.MemTable.TDataSet.prototype.Append = function () {
    var _this = this.TParent();
    _this.Status = SysCfg.MemTable.Properties.TStatus.Apped;
    _this.RecordSet = new SysCfg.MemTable.Properties.TRecord();
    _this.RecordSet.Fields = new Array();
    for (var ContX = 0; ContX <= _this.FieldCount - 1; ContX++) {
        var Field = new SysCfg.MemTable.Properties.TField();
        Field.FieldDef = _this.FieldDefs[ContX];
        switch (_this.FieldDefs[ContX].DataType) {
            case SysCfg.DB.Properties.TDataType.String: Field.Value = ""; break;
            case SysCfg.DB.Properties.TDataType.Text: Field.Value = ""; break;
            case SysCfg.DB.Properties.TDataType.Int32: Field.Value = 0; break;
            case SysCfg.DB.Properties.TDataType.Double: Field.Value = 0.00; break;
            case SysCfg.DB.Properties.TDataType.Decimal: Field.Value = 0.00; break;
            case SysCfg.DB.Properties.TDataType.Boolean: Field.Value = false; break;
            case SysCfg.DB.Properties.TDataType.DateTime: Field.Value = Date.now(); break;
            case SysCfg.DB.Properties.TDataType.Object: Field.Value = null; break;
        }
        _this.RecordSet.Fields.push(Field);
    }
    _this.RecordSet.FieldDefs = _this.FieldDefs;
    _this.RefreschRecordSet(_this.RecordSet);//*null
    return (_this.RecordSet);
}

SysCfg.MemTable.TDataSet.prototype.isDelete = function () {
    var _this = this.TParent();
    return (((_this.Index >= 0) && (_this.Index <= _this.RecordCount - 1)) && (_this.Status == SysCfg.MemTable.Properties.TStatus.None));
}

SysCfg.MemTable.TDataSet.prototype.Delete = function () {
    var _this = this.TParent();
    _this.Status = SysCfg.MemTable.Properties.TStatus.Delete;
    _this.BeforeChange(null);

    if (_this.Status != SysCfg.MemTable.Properties.TStatus.Cancel) {
        _this.AfterChange(null);
    }
    if (_this.Status == SysCfg.MemTable.Properties.TStatus.Delete) {
        _this.Records.splice(_this.Index, 1);
        _this.RecordCount--;
        _this.Index--;
        if (_this.RecordCount == 0) {
            _this.RecordSet = null;
            _this.Index = -1;
            _this.Bof = true;
            _this.Eof = true;
        }
        else {
            if (_this.Index < 0) {
                _this.Bof = false;
                _this.Eof = false;
                _this.Index = 0;
            }
            _this.RecordSet = _this.Records[_this.Index];
        }
        _this.Status = SysCfg.MemTable.Properties.TStatus.None;
        _this.RefreschRecordSet(_this.RecordSet);//*null
    }
}

//************************************************************************

SysCfg.MemTable.TDataSet.prototype.isFirst = function () {
    var _this = this.TParent();
    return ((_this.Index > 0) && (_this.Status == SysCfg.MemTable.Properties.TStatus.None));
}

SysCfg.MemTable.TDataSet.prototype.First = function () {
    var _this = this.TParent();
    if (_this.RecordCount == 0) {
        _this.Bof = true;
        _this.Eof = true;
        _this.Index = -1;
        _this.RecordSet = null;
    }
    else {
        _this.Index = 0;
        _this.Bof = false;
        _this.Eof = false;
        _this.RecordSet = _this.Records[_this.Index];
    }
    _this.RefreschRecordSet(_this.RecordSet);
}

SysCfg.MemTable.TDataSet.prototype.isLast = function () {
    var _this = this.TParent();
    return ((_this.Index < _this.RecordCount - 1) && (_this.Status == SysCfg.MemTable.Properties.TStatus.None));
}

SysCfg.MemTable.TDataSet.prototype.Last = function () {
    var _this = this.TParent();
    if (_this.RecordCount == 0) {
        _this.Bof = true;
        _this.Eof = true;
        _this.Index = _this.RecordCount;
        _this.RecordSet = null;
    }
    else {
        _this.Index = _this.RecordCount - 1;
        _this.Bof = false;
        _this.Eof = false;
        _this.RecordSet = _this.Records[_this.Index];
    }
    _this.RefreschRecordSet(_this.RecordSet);//*null
}

SysCfg.MemTable.TDataSet.prototype.isNext = function () {
    var _this = this.TParent();
    return ((_this.Index < _this.RecordCount - 1) && (_this.Status == SysCfg.MemTable.Properties.TStatus.None));
}

SysCfg.MemTable.TDataSet.prototype.Next = function () {
    var _this = this.TParent();
    if (_this.Index >= _this.RecordCount - 1) {
        _this.Bof = false;
        _this.Eof = true;
        _this.Index = _this.RecordCount;
        _this.RecordSet = null;
    }
    else {
        _this.Index++;
        _this.RecordSet = _this.Records[_this.Index];
    }
    _this.RefreschRecordSet(_this.RecordSet);//*null
}



//21/07/2018
//SysCfg.MemTable.TDataSet.prototype.SetIndex = function (inIndex) {
//    var _this = this.TParent();
//    if (inIndex != _this.Index) {
//        if (inIndex < 0) inIndex = -1;
//        if (inIndex > _this.RecordCount - 1) inIndex = _this.RecordCount - 1;


//        if ((inIndex <= 0) && (inIndex >= _this.RecordCount - 1)) {
//            _this.Bof = (inIndex = 0);
//            _this.Eof = (InIndex = _this.RecordCount - 1);
//            _this.Index = _this.RecordCount;
//            _this.RecordSet = null;
//        }
//        else {
//            _this.Index = inIndex;
//            _this.RecordSet = _this.Records[_this.Index];
//        }
//        _this.RefreschRecordSet(_this.RecordSet);//*null
//    }
//}

SysCfg.MemTable.TDataSet.prototype.SetIndex = function (inIndex) {
    var _this = this.TParent();
    if (inIndex != _this.Index) {
        if (inIndex < 0) inIndex = -1;
        if (inIndex > _this.RecordCount - 1) inIndex = _this.RecordCount;


        _this.Bof = (inIndex <= 0);
        _this.Eof = (inIndex >= _this.RecordCount - 1);

        if ((inIndex < 0) || (inIndex > _this.RecordCount - 1)) {
            _this.Index = inIndex;//_this.RecordCount;//fuera del indice puede ser tambien -1
            _this.RecordSet = null;
        }
        else {
            _this.Index = inIndex;
            _this.RecordSet = _this.Records[_this.Index];
        }
        _this.RefreschRecordSet(_this.RecordSet);//*null
    }
    return (inIndex);
}


SysCfg.MemTable.TDataSet.prototype.isPrior = function () {
    var _this = this.TParent();
    return ((_this.Index > 0) && (_this.Status == SysCfg.MemTable.Properties.TStatus.None));
}

SysCfg.MemTable.TDataSet.prototype.Prior = function () {
    var _this = this.TParent();
    if (_this.Index <= 0) {
        _this.Bof = true;
        _this.Eof = false;
        _this.Index = -1;
        _this.RecordSet = null;
    }
    else {
        _this.Index--;
        _this.RecordSet = _this.Records[_this.Index];
    }
    _this.RefreschRecordSet(_this.RecordSet);
}

/*
public void ()
{
    
}*/




