﻿SysCfg.Domain.TDataDomain = function () {
    this.ResErr = new SysCfg.Error.Properties.TResErr();
    this.DomainLogin = false;
    this.DomainUser = "";
    this.DomainName = "";
    this.DomainControllerName = "";
    this.AccountDomainSid = "";
    this.DomainControllerNameIP = "127.0.0.1";
    this.DomainAuitorization = false;
    this.DomainPW = "";
    this.isValidDomain = false;
    this.DomainPath = "LDAP://{1}/{2}";
}
