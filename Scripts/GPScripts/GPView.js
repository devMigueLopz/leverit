﻿//Version 1 / 2018/03/14 Felipe y anterior
//Version 2 / 2018/03/14 Jaimes

ItHelpCenter.TUfrGPView = function (inObject, incallback, inGPQUERY)
{
    this.TParent = function ()
    {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.BarControls = null;
    this.GridCFToolbar = null;
    this.GridCFView = null;
    this.Open = new SysCfg.MemTable.TDataSet();
    this.Param = new SysCfg.Stream.Properties.TParam();

    var stackPanel = new TVclStackPanel(inObject, "1", 1, [[12], [12], [12], [12], [12]]);

    if (!Source.Menu.IsMobil)
    {
        stackPanel.Row.This.style.marginLeft = "5px";
        stackPanel.Row.This.style.marginBottom = "5px";
        stackPanel.Column[0].This.style.padding = "15px";
        stackPanel.Column[0].This.style.border = "2px solid rgba(0, 0, 0, 0.08)";
        stackPanel.Column[0].This.style.boxShadow = " 0 0px 8px 0 rgba(0, 0, 0, 0.06), 0 1px 0px 0 rgba(0, 0, 0, 0.02);";
        stackPanel.Column[0].This.style.marginTop = "15px";
        stackPanel.Column[0].This.style.backgroundColor = "#FAFAFA";
    }
        this.stackPanelBody = new TVclStackPanel(stackPanel.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);

        this.ObjectHtml = this.stackPanelBody.Column[0].This;

        this.CallbackModalResult = incallback;

        this.GPQUERY = inGPQUERY;

        ////TRADUCCION          
        this.Mythis = "TUfrGPView";
        UsrCfg.Traslate.GetLangText(_this.Mythis, "RHOutputMsgSuccessful", "The remote-control request has been sent");
        UsrCfg.Traslate.GetLangText(_this.Mythis, " toolRemoteHelp.Text ", "Help");
        
    }


ItHelpCenter.TUfrGPView.prototype.OpenQuery = function ()
{
    var _this = this;
    _this.VTCfgFactory = null;
    if (_this.GPQUERY.STRSQL != "")
    {
        if (_this.Param.FieldCount > 0)
        {
            Persistence.Methods.GenereSQLHW(_this.Param);
            _this.Open = SysCfg.DB.SQL.Methods.OpenDataSet(_this.GPQUERY.STRSQL, _this.Param.ToBytes());
        }
        else
        {

            var Param = new SysCfg.Stream.Properties.TParam();
            try
            {
                Param.Inicialize();
                Persistence.Methods.GenereSQLHW(Param);
                _this.Open = SysCfg.DB.SQL.Methods.OpenDataSet(_this.GPQUERY.STRSQL, Param.ToBytes());
            }
            finally
            {
                Param.Destroy();
            }

            
        }
    }
    else
    {
        if (_this.Param.FieldCount > 0)
        {
            Persistence.Methods.GenereSQLHW(_this.Param);
            _this.Open = SysCfg.DB.SQL.Methods.OpenDataSet(_this.GPQUERY.OPEN_PREFIX, _this.GPQUERY.OPEN_ID, _this.Param.ToBytes());
        }
        else
        {
            var Param = new SysCfg.Stream.Properties.TParam();
            try {
                Param.Inicialize();
                Persistence.Methods.GenereSQLHW(Param);
                _this.Open = SysCfg.DB.SQL.Methods.OpenDataSet(_this.GPQUERY.OPEN_PREFIX, _this.GPQUERY.OPEN_ID, Param.ToBytes());                
            }
            finally {
                Param.Destroy();
            }



            
        }
    }

    if (_this.Open.ResErr.NotError)
    {
        if (_this.GPQUERY.GPVIEWList != null)
        {
            if (_this.GPQUERY.GPVIEWList.length > 0)
            {
                var ContTabControl = new TVclStackPanel(_this.ObjectHtml, "1", 1, [[12], [12], [12], [12], [12]]);

                var TabControl1 = new TTabControl(ContTabControl.Column[0].This, "", ""); //CREA UN TAB CONTROL 

                var active = true;
                for (var i = 0; i < _this.GPQUERY.GPVIEWList.length; i++)
                {
                    var TabPag1 = new TTabPage(); //CREA UN TAB PAGE PARA AGREGARSE AL TABCONTROL 
                    TabPag1.Name = _this.GPQUERY.GPVIEWList[i].GPVIEW_NAME; //ASIGNA O EXTRAE EL NAME DEL TAB PAGE
                    TabPag1.Caption = _this.GPQUERY.GPVIEWList[i].GPVIEW_NAME; //ASIGNA O EXTRAE EL TEXTO QUE VA A MOSTRAR EN EL TAB
                    TabPag1.Active = active; //ASIGNA O EXTRAE SI EL TABPAGE ES EL ACTIVO (SE ESTA MOSTRANDO)
                    TabPag1.Tag = _this.GPQUERY.GPVIEWList[i];
                    TabControl1.AddTabPages(TabPag1); //ADICIONA UN TABPAGE AL TABCONTROL
                    TabPag1.Page.style.padding = "0px";

                    active = false;
                }

                TabControl1.OnChangeTab = function (sender, EventArgs)
                {
                    _this.EventArgsTAG = EventArgs.Tag;
                    _this.LoadQueryView(EventArgs.Tag);
                }
            }
        }
        _this.EventArgsTAG = null;
        if (TabControl1 != null) {
            if (TabControl1.TabPages.length > 0) {
                TabControl1.TabPages[0].Active = true;
                _this.EventArgsTAG = _this.GPQUERY.GPVIEWList[0];
                //TabControl1.OnChangeTab(null, TabControl1.TabPages[0]);
            }
        }

        if (_this.detectIE()) {
            _this.LoadDefaultQuery();
            //Para poner animacion de espera
            setTimeout(function () {
                _this.GridCFView.loadDataSetColumns(_this.Open.DataSet);
                _this.GridCFView.StyleGrid = _this.VTCfgFactory;
                if (_this.EventArgsTAG==null) {
                    _this.GridCFView.loadDataSetRecords();
                } else {
                    _this.LoadQueryView(_this.EventArgsTAG);
                }
                
            }, 0);
        } else {
            _this.LoadDefaultQuery();
            _this.GridCFView.loaderShow(true);
            setTimeout(function () {
                _this.GridCFView.loadDataSetColumns(_this.Open.DataSet);
                _this.GridCFView.StyleGrid = _this.VTCfgFactory;
                if (_this.EventArgsTAG == null) {
                    _this.GridCFView.loadDataSetRecords();
                } else {
                    _this.LoadQueryView(_this.EventArgsTAG);
                }
                _this.GridCFView.loaderShow(false);
            }, 0);
        }
    };
}

ItHelpCenter.TUfrGPView.prototype.detectIE = function () {

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        return true;
    }
    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        return true;
    }

    return false;

}
ItHelpCenter.TUfrGPView.prototype.OpenDataSet = function (StrSQL)
{
    var _this = this.TParent();
    Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        OpenDataSet = SysCfg.DB.SQL.Methods.OpenDataSet(StrSQL, Param.ToBytes());
        if (OpenDataSet.ResErr.NotError)
        {
            if (OpenDataSet.DataSet.RecordCount > 0)
            {
            }
            else
            {
                OpenDataSet.ResErr.NotError = true;
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (OpenDataSet);
}

ItHelpCenter.TUfrGPView.prototype.AddStyleFile = function (nombre, onSuccess, onError)
{
    var style = document.createElement("link");
    style.rel = "stylesheet";
    style.type = "text/css";
    style.href = nombre;
    var s = document.head.appendChild(style);
    s.onload = onSuccess;
    s.onerror = onError;
}

ItHelpCenter.TUfrGPView.prototype.LoadDefaultQuery = function ()
{
    var _this = this.TParent();
    var ConGridControl = new TVclStackPanel(_this.ObjectHtml, "1", 2, [[12, 0], [12, 0], [12, 0], [12, 0], [12, 0]]);

    _this.GridCFView = new Componet.GridCF.TGridCFView(ConGridControl.Column[0].This, "", TlabelType.H0);

    _this.GridCFView.OnChangeIndex = function (GridCFView, Record)
    {
        _this.GridSelectedRowChanged(Record);
    }

    _this.GridCFView.ButtonImg_Refresh_Visible = true;
    _this.GridCFView.OnGridRefresch = function (GridCFView, Toolbar)
    {
        _this.ObjectHtml.innerHTML = "";
        _this.OpenQuery();
    }
    var toolRemoteHelpSection = new ItHelpCenter.Componet.NavTabControls.TNavPanelBar(_this.GridCFView.GridCFToolbar.TabPagView);
    toolRemoteHelpSection.Title = "";
    var toolRemoteHelp = new ItHelpCenter.Componet.NavTabControls.TNavBarStackPanel();
    toolRemoteHelp.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, " toolRemoteHelp.Text ");
    toolRemoteHelp.TextColor = "#757575";
    toolRemoteHelpSection.addItemBar(toolRemoteHelp);
    HelpRemote = new ItHelpCenter.TUfrRemoteHelp(toolRemoteHelp.Panel,
        function () {
            //toolRemoteHelp.Visible = false;
            toolRemoteHelpSection.Visible = false;
            if (HelpRemote.hasPermissions) {
                _this.GridCFView.VclGridCF.ColumnCheckVisible = false; //true o false para visualizar la columna de cheks 
                for (i = 0; i < _this.GPQUERY.GPEDIT_COLUMN_List.length; i++) {
                    if (_this.GPQUERY.GPEDIT_COLUMN_List[i].EDIT_COLUMNNAME.toUpperCase() == "IDCPU") {
                        toolRemoteHelpSection.Visible = true;
                        _this.GridCFView.GridCFToolbar.ButtonImg_RowCheckToolsBar.ChekedClickDown = true;
                        _this.GridCFView.VclGridCF.ColumnCheckVisible = true; //true o false para visualizar la columna de cheks 
                        HelpRemote.GridCFView = _this.GridCFView;
                        break;
                    }
                }
            }
        }, document.body);
    
    HelpRemote.EnabledModal = true;


    _this.GridCFView.OnBeforeChange = function (DataSet, RecordOld, RecordNew, Status, isCancel)
    {
        var message = "";
        if (Status == SysCfg.MemTable.Properties.TStatus.Apped)
        {
            message = message + "Esta seguro de grabar el registro?";
        }
        else if (Status == SysCfg.MemTable.Properties.TStatus.Update)
        {
            message = message + "Esta seguro de grabar el registro?";
        }
        else if (Status == SysCfg.MemTable.Properties.TStatus.Delete)
        {
            message = message + "Esta seguro de eliminar el registro?";
        }
        isCancel.Value = !confirm(message);

        if (!isCancel.Value)
        {
            if (Status == SysCfg.MemTable.Properties.TStatus.Apped)
            {
                isCancel.Value = !_this.AddRecord(RecordNew);
            }
            else if (Status == SysCfg.MemTable.Properties.TStatus.Update)
            {
                isCancel.Value = !_this.UpdateRecord(RecordOld, RecordNew);
            }
            else if (Status == SysCfg.MemTable.Properties.TStatus.Delete)
            {
                isCancel.Value = !_this.DeleteRecord(RecordNew);
            }
        }
    }
    _this.GridCFView.OnAfterChange = function (GridCFView, RecordOld, RecordNew)
    {
        var message = "";
        if (GridCFView.DataSet.Status == SysCfg.MemTable.Properties.TStatus.Apped)
        {
            message = "El registro fue grabado con exito.";
        }
        else if (GridCFView.DataSet.Status == SysCfg.MemTable.Properties.TStatus.Update)
        {
            message = "El registro fue grabado con exito.";
        }
        else if (GridCFView.DataSet.Status == SysCfg.MemTable.Properties.TStatus.Delete)
        {
            message = "El registro fue eliminado con exito.";
        }
        alert(message);
    }

    ///////////////////////////
    //CONFIGURACON DE EDITABLES
    ///////////////////////////
    var Columns = new Array();
    var ShowInsert = false;
    var ShowUpdate = false;
    var ShowDelete = false;
    var VTCfgFactory = new Componet.GridCF.TGridCFView.TCfg();


    for (y = 0; y < _this.GPQUERY.GPEDIT_COLUMN_List.length; y++) {
        var item;
       
        var objGPEDIT_COLUMN = _this.GPQUERY.GPEDIT_COLUMN_List[y];
        if (objGPEDIT_COLUMN.COLUMNSTYLE == 6) //FileSrv
        {
            var item = {
                Name: _this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME,
                Style: Componet.Properties.TExtrafields_ColumnStyle.FileSRV,
                FileType: objGPEDIT_COLUMN.FILETYPE,
            }
            Columns.push(item);
        }
        if (objGPEDIT_COLUMN.COLUMNSTYLE == 1) //Memo
        {
            var item = {
                Name: _this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME,
                Style: Componet.Properties.TExtrafields_ColumnStyle.Text,
                
            }
            Columns.push(item);
        }
        if (objGPEDIT_COLUMN.COLUMNSTYLE == 4) //Progress
        {
            var item = {
                Name: _this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME,
                Style: Componet.Properties.TExtrafields_ColumnStyle.ProgressBar,
                
            }
            Columns.push(item);
        }
    }


    if (this.GPQUERY.GPEDIT_TABLE_List != null)
    {
        for (i = 0; i < this.GPQUERY.GPEDIT_TABLE_List.length; i++)
        {
            var objGPEDIT_TABLE = this.GPQUERY.GPEDIT_TABLE_List[i];
            for (j = 0; j < objGPEDIT_TABLE.GPEDIT_COLUMNEVENT_List.length; j++)
            {
                var objGPEDIT_COLUMNEVENT = objGPEDIT_TABLE.GPEDIT_COLUMNEVENT_List[j];
                for (y = 0; y < _this.GPQUERY.GPEDIT_COLUMN_List.length; y++)
                {
                    var objGPEDIT_COLUMN = _this.GPQUERY.GPEDIT_COLUMN_List[y];
                    if (objGPEDIT_COLUMN.IDGPEDIT_COLUMN == objGPEDIT_COLUMNEVENT.IDGPEDIT_COLUMN)
                    {
                        if (objGPEDIT_COLUMNEVENT.EVINSERT == "S")
                            ShowInsert = true;
                        if (objGPEDIT_COLUMNEVENT.EVUPDATESET == "S" || objGPEDIT_COLUMNEVENT.EVUPDATEWHERE == "S")
                            ShowUpdate = true;
                        if (objGPEDIT_COLUMNEVENT.EVDELETE == "S")
                            ShowDelete = true;

                        var item;
                        if (objGPEDIT_COLUMN.COLUMNSTYLE == 1) //Memo
                        {
                            var item = {
                                Name: _this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME,
                                InsertActiveEditor: (objGPEDIT_COLUMNEVENT.EVINSERT == "S"),
                                UpdateActiveEditor: (objGPEDIT_COLUMNEVENT.EVUPDATESET == "S"),
                                Style: Componet.Properties.TExtrafields_ColumnStyle.Text,
                               
                            }
                        }
                          else if (objGPEDIT_COLUMN.COLUMNSTYLE == 4) //Progress
                        {
                            var item = {
                                Name: _this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME,
                                InsertActiveEditor: (objGPEDIT_COLUMNEVENT.EVINSERT == "S"),
                                UpdateActiveEditor: (objGPEDIT_COLUMNEVENT.EVUPDATESET == "S"),
                                Style: Componet.Properties.TExtrafields_ColumnStyle.ProgressBar,

                            }
                        }
                        else if (objGPEDIT_COLUMN.COLUMNSTYLE == 5) //LookUp
                        {
                            var OpenLookup = new SysCfg.MemTable.TDataSet();

                            var Param = new SysCfg.Stream.Properties.TParam();
                            try
                            {
                                Param.Inicialize();
                                Persistence.Methods.GenereSQLHW(Param);
                                OpenLookup = SysCfg.DB.SQL.Methods.OpenDataSet(objGPEDIT_COLUMN.LOOKUPSQL, Param.ToBytes());
                                if (OpenLookup.ResErr.NotError) {
                                    item = {
                                        Name: _this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME,
                                        InsertActiveEditor: (objGPEDIT_COLUMNEVENT.EVINSERT == "S"),
                                        UpdateActiveEditor: (objGPEDIT_COLUMNEVENT.EVUPDATESET == "S"),
                                        Style: Componet.Properties.TExtrafields_ColumnStyle.LookUp,
                                        DataEventControl: {
                                            "OpenLookup": OpenLookup,
                                            "objGPEDIT_COLUMN": objGPEDIT_COLUMN,
                                        },
                                        OnEventControl: function (inTVclComboBox2, inDataEventControl) {
                                            var OpenLookup_Interno = inDataEventControl["OpenLookup"];
                                            var objGPEDIT_COLUMN_Interno = inDataEventControl["objGPEDIT_COLUMN"];

                                            for (var f = 0; f < OpenLookup_Interno.DataSet.Records.length; f++) {
                                                var valueMember = "";
                                                var displayMember = "";
                                                var displayMemberColumns = objGPEDIT_COLUMN_Interno.LOOKUPDISPLAYCOLUMNLIST.split(',');
                                                for (var g = 0; g < OpenLookup_Interno.DataSet.Records[f].Fields.length; g++) {
                                                    if (OpenLookup_Interno.DataSet.Records[f].Fields[g].FieldDef.FieldName ==
                                                        objGPEDIT_COLUMN_Interno.LOOKUPDISPLAYCOLUMN) {
                                                        valueMember = OpenLookup_Interno.DataSet.Records[f].Fields[g].Value;
                                                    }
                                                    for (var h = 0; h < displayMemberColumns.length; h++) {
                                                        if (OpenLookup_Interno.DataSet.Records[f].Fields[g].FieldDef.FieldName ==
                                                            objGPEDIT_COLUMN_Interno.LOOKUPDISPLAYCOLUMNLIST.split(',')[h]) {
                                                            displayMember += OpenLookup_Interno.DataSet.Records[f].Fields[g].Value + "   ";
                                                        }
                                                    }
                                                }
                                                var VclComboBoxItem1 = new TVclComboBoxItem();
                                                VclComboBoxItem1.Value = valueMember;
                                                VclComboBoxItem1.Text = displayMember;
                                                inTVclComboBox2.AddItem(VclComboBoxItem1);
                                            }
                                        },
                                    }
                                }

                            }
                            finally
                            {
                                Param.Destroy();
                            }                            
                        }
                        else if (objGPEDIT_COLUMN.COLUMNSTYLE == 6) //FileSrv
                        {
                            var item = {
                                Name: _this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME,
                                InsertActiveEditor: (objGPEDIT_COLUMNEVENT.EVINSERT == "S"),
                                UpdateActiveEditor: (objGPEDIT_COLUMNEVENT.EVUPDATESET == "S"),
                                Style: Componet.Properties.TExtrafields_ColumnStyle.FileSRV,
                                FileType: objGPEDIT_COLUMN.FILETYPE,
                            }
                        }
                      
                        else
                        {
                            var item = {
                                Name: _this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME,
                                InsertActiveEditor: (objGPEDIT_COLUMNEVENT.EVINSERT == "S"),
                                UpdateActiveEditor: (objGPEDIT_COLUMNEVENT.EVUPDATESET == "S"),
                            }
                        }
                        Columns.push(item);
                        break;
                    }
                }
            }
        }

        VTCfgFactory.ToolBar.ButtonImg_DeleteBar.Active = ShowDelete;
        VTCfgFactory.ToolBar.ButtonImg_AddBar.Active = ShowInsert;
        VTCfgFactory.ToolBar.ButtonImg_EditBar.Active = ShowUpdate;
        VTCfgFactory.ToolBar.ButtonImg_EditToolsBar.Visible = (ShowDelete || ShowInsert || ShowUpdate);
        if (ShowDelete || ShowInsert || ShowUpdate)
        {
            VTCfgFactory.ToolBar.ButtonImg_EditToolsBar.ChekedClickDown = true;
        }

        //Aqui asigno coleccion de columnas con estilo aplicado.
        VTCfgFactory.Columns = Columns;

    }

    //_this.GridCFView.VclGridCF.DataSource = _this.Open.DataSet;
    _this.VTCfgFactory=VTCfgFactory;
    //_this.GridCFView.LoadTCfg(VTCfgFactory);

    HelpRemote.OnRemoteClick = function (_remotehelp, object, Type)
    {
   
        if (Type == "Button")
        {
            if (object == "OK")
            {
                var FilasSeleccionadas = _this.GridCFView.VclGridCF.GetSelectedRows();
                HelpRemote.RHREQUESTOPTIONS.CPUList = new Array();
                for (var i = 0; i < FilasSeleccionadas.length; i++)
                {
                    HelpRemote.RHREQUESTOPTIONS.CPUList.push(FilasSeleccionadas[i].GetCellValue("IDCPU"));
                }

                //Llamamos a la web service
                //debugger;
                if (HelpRemote.RHREQUESTOPTIONS.CPUList.length > 0)
                {

                    var dataArray = {
                        "RHREQUESTOPTIONS": HelpRemote.RHREQUESTOPTIONS,
                    };

                    $.ajax({
                        type: "POST",
                        url: SysCfg.App.Properties.xRaiz + 'Service/RemoteHelpService/RemoteHelp.svc/RHREQUEST',
                        data: JSON.stringify(dataArray),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: false,
                        success: function (response)
                        {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "RHOutputMsgSuccessful"));
                        },
                        error: function (response)
                        {
                           //debugger;
                            alert('RemoteHelp RHREQUEST');
                        }
                    });
                }
                else
                {
                    alert('Please select a IDCPU');
                }

            }
        }
    }
}

ItHelpCenter.TUfrGPView.prototype.LoadQueryView = function (inGPVIEW)
{
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();


    ///////////////////////////
    //APLICO FILTROS///////////
    ///////////////////////////
    var CUSTOMFILTERList = new Array();
    Persistence.GP.Methods.StrtoCUSTOMFILTER(inGPVIEW.GPVIEW_FILTER, CUSTOMFILTERList);
    var ListCustomFilter = new Array();
    for (var i = 0; i < CUSTOMFILTERList.length; i++)
    {
        var item1 = {
            COLUMNAME: CUSTOMFILTERList[i].COLUMNNAME,
            COLUMNTYPE: SysCfg.DB.Properties.TDataType.String,
            OPERATOR: CUSTOMFILTERList[i].OPERATOR,
            VALUE: CUSTOMFILTERList[i].VALUE,
            ANDOR: CUSTOMFILTERList[i].ANDOR
        }
        ListCustomFilter.push(item1);
    }
    _this.GridCFView.ListCustomFilter = ListCustomFilter;
    ///////////////////////////


    ///////////////////////////
    //CONFIGURACON DE EDITABLES
    ///////////////////////////
    var Columns = new Array();
    var Groups = new Array();
    var Orders = new Array();
    var OrderColumns = new Array();

    var VistaOrdenada = inGPVIEW.GPCOLUMNVIEWList.sort(function (a, b) { return a.POSITION - b.POSITION });
    for (var w = 0; w < VistaOrdenada.length; w++)
    {
        var objGPCOLUMNVIEW = VistaOrdenada[w];

        if (objGPCOLUMNVIEW.ISGROUP == "S")
            Groups.push(objGPCOLUMNVIEW.FIELD_NAME);
        if (objGPCOLUMNVIEW.SORT == "S")
        {
            var order = { Type: 0, ColumnName: objGPCOLUMNVIEW.FIELD_NAME }
            Orders.push(order);
        }
        var item =
        {
            Name: objGPCOLUMNVIEW.FIELD_NAME,
            CustomDrawCell:
            {
                data: objGPCOLUMNVIEW,
                function: function (Cell, DataType, value, dataObject)
                {
                    if (dataObject != null)
                    {
                        Cell.Color = "#" + dataObject.COLOR.substring(3, 9);
                        if (dataObject.GPVALUECOLORList.length != null)
                        {
                            for (var i = 0; i < dataObject.GPVALUECOLORList.length; i++)
                            {
                                var condition = dataObject.GPVALUECOLORList[i].CONDITION;

                                if (condition.indexOf(">=") != -1)
                                {
                                    var conditionValue = condition.replace(">=", "");
                                    if (DataType.name == "Int32")
                                    {
                                        if (parseInt(value) >= parseInt(conditionValue.trim()))
                                        {
                                            Cell.Color = "#" + dataObject.GPVALUECOLORList[i].COLOR.substring(3, 9);
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        if (value >= conditionValue.trim())
                                        {
                                            Cell.Color = "#" + dataObject.GPVALUECOLORList[i].COLOR.substring(3, 9);
                                            break;
                                        }
                                    }
                                }
                                else if (condition.indexOf("<=") != -1)
                                {
                                    var conditionValue = condition.replace("<=", "");
                                    if (DataType.name == "Int32")
                                    {
                                        if (parseInt(value) <= parseInt(conditionValue.trim()))
                                        {
                                            Cell.Color = "#" + dataObject.GPVALUECOLORList[i].COLOR.substring(3, 9);
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        if (value <= conditionValue.trim())
                                        {
                                            Cell.Color = "#" + dataObject.GPVALUECOLORList[i].COLOR.substring(3, 9);
                                            break;
                                        }
                                    }
                                }
                                else if (condition.indexOf(">") != -1)
                                {
                                    var conditionValue = condition.replace(">", "");
                                    if (DataType.name == "Int32")
                                    {
                                        if (parseInt(value) > parseInt(conditionValue.trim()))
                                        {
                                            Cell.Color = "#" + dataObject.GPVALUECOLORList[i].COLOR.substring(3, 9);
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        if (value > conditionValue.trim())
                                        {
                                            Cell.Color = "#" + dataObject.GPVALUECOLORList[i].COLOR.substring(3, 9);
                                            break;
                                        }
                                    }
                                }
                                else if (condition.indexOf("<") != -1)
                                {
                                    var conditionValue = condition.replace("<", "");
                                    if (DataType.name == "Int32")
                                    {
                                        if (parseInt(value) < parseInt(conditionValue.trim()))
                                        {
                                            Cell.Color = "#" + dataObject.GPVALUECOLORList[i].COLOR.substring(3, 9);
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        if (value < conditionValue.trim())
                                        {
                                            Cell.Color = "#" + dataObject.GPVALUECOLORList[i].COLOR.substring(3, 9);
                                            break;
                                        }
                                    }
                                }
                                else if (condition.indexOf("=") != -1)
                                {
                                    var conditionValue = condition.replace("=", "");
                                    if (value == conditionValue.trim())
                                    {
                                        Cell.Color = "#" + dataObject.GPVALUECOLORList[i].COLOR.substring(3, 9);
                                        break;
                                    }
                                }
                                else
                                {
                                    if (value == condition)
                                    {
                                        Cell.Color = "#" + dataObject.GPVALUECOLORList[i].COLOR.substring(3, 9);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                },
            },
            Caption: objGPCOLUMNVIEW.HEADER,
            Show: (objGPCOLUMNVIEW.ISVISIBLE == "S"),
            ShowVertical: (objGPCOLUMNVIEW.ISVISIBLEDETAIL == "S"),
        }
        Columns.push(item);
        OrderColumns.push(objGPCOLUMNVIEW.FIELD_NAME);
    }

    //Aqui Mauricio
    var Order = null;
    if (Orders.length > 0)
        Order = Orders[0];
    ///////////////

    var tmpEnableChecks = _this.GridCFView.GridCFToolbar.ButtonImg_RowCheckToolsBar.Cheked;
    var VTCfgFactory = new Componet.GridCF.TGridCFView.TCfg();
    VTCfgFactory.TCfgConfig = _this.VTCfgFactory;
    VTCfgFactory.ToolBar.ButtonImg_RowCheckToolsBar.ChekedClickDown = tmpEnableChecks;
    VTCfgFactory.ToolBar.ButtonImg_RowEditToolsBar.ChekedClickDown = false;
    VTCfgFactory.ToolBar.ButtonImg_ColumnGroupBar.ChekedClickDown = true;
    VTCfgFactory.ToolBar.ButtonImg_ColumnGroupBar.ChekedClickDown = false;
    VTCfgFactory.ToolBar.ButtonImg_DatailsViewBar.ChekedClickDown = true;
    VTCfgFactory.OptionButtonCkeck.SetSelectedRows = true;
    VTCfgFactory.OrderColumns = OrderColumns;
    VTCfgFactory.ListAgrupation = Groups;
    VTCfgFactory.Order = Order;
    VTCfgFactory.Columns = Columns;
    _this.GridCFView.LoadTCfg(VTCfgFactory);
    _this.GridCFView.loadDataSetRecords();

}



ItHelpCenter.TUfrGPView.prototype.AddRecord = function (RecordNew)
{
    var resp = false;
    for (i = 0; i < this.GPQUERY.GPEDIT_TABLE_List.length; i++)
    {
        var Param = new SysCfg.Stream.Properties.TParam();
        Param.Inicialize();

        var fields = "";
        var values = "";

        var objGPEDIT_TABLE = this.GPQUERY.GPEDIT_TABLE_List[i];

        for (j = 0; j < objGPEDIT_TABLE.GPEDIT_COLUMNEVENT_List.length; j++)
        {
            var objGPEDIT_COLUMNEVENT = objGPEDIT_TABLE.GPEDIT_COLUMNEVENT_List[j];

            if (objGPEDIT_COLUMNEVENT.EVINSERT == "S")
            {
                for (y = 0; y < this.GPQUERY.GPEDIT_COLUMN_List.length; y++)
                {
                    if (this.GPQUERY.GPEDIT_COLUMN_List[y].IDGPEDIT_COLUMN == objGPEDIT_COLUMNEVENT.IDGPEDIT_COLUMN)
                    {
                        var val = RecordNew.FieldName(this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME) //Obtengo el valor de la celda
                        switch (this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNTYPE)
                        {
                            case "String":
                                Param.AddString(this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME, val.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                                break;
                            case "Text":
                                Param.AddString(this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME, val.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                                break;
                            case "Int32":
                                Param.AddInt32(this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME, val.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                                break;
                            case "DateTime":
                                Param.AddDateTime(this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME, new Date(val.Value), SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                                break;
                            case "Boolean":
                                Param.AddBoolean(this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME, val.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
                                break;
                            case "Decimal":
                                Param.AddDouble(this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME, val.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                                break;
                            case "Double":
                                Param.AddDouble(this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME, val.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                                break;
                            default:
                                Param.AddString(this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME, val.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                                break;
                        }
                        fields += "," + this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME;
                        values += ",@[" + this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME + "]";
                        break;
                    }
                }
            }
        }
        fields = fields.substring(1, fields.length);
        values = values.substring(1, values.length);
        query = "INSERT INTO " + objGPEDIT_TABLE.EDIT_TABLENAME + " (" + fields + ") VALUES (" + values + ")";
        OpenExecute = SysCfg.DB.SQL.Methods.Exec(query, Param.ToBytes());

        if (!OpenExecute.ResErr.NotError)
        {
            resp = false;
            alert(OpenExecute.ResErr.Mesaje);
        }
        else
        {
            resp = true;
            for (y = 0; y < this.GPQUERY.GPEDIT_COLUMN_List.length; y++)
            {
                if (this.GPQUERY.GPEDIT_COLUMN_List[y].MYCONSTRAINT == 2)
                {
                    var ParamIdentity = new SysCfg.Stream.Properties.TParam();
                    ParamIdentity.Inicialize();
                    query = "SELECT MAX(" + this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME + ") AS VALOR FROM " + objGPEDIT_TABLE.EDIT_TABLENAME;
                    var result = SysCfg.DB.SQL.Methods.OpenDataSet(query, Param.ToBytes());
                    RecordNew.FieldName(this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME).Value = result.DataSet.Records[0].Fields[0].Value;
                }
            }
        }
    }
    return resp;
}

ItHelpCenter.TUfrGPView.prototype.UpdateRecord = function (RecordOld, RecordNew)
{
    var resp = false;
    for (i = 0; i < this.GPQUERY.GPEDIT_TABLE_List.length; i++)
    {
        var Param = new SysCfg.Stream.Properties.TParam();
        Param.Inicialize();

        var objGPEDIT_TABLE = this.GPQUERY.GPEDIT_TABLE_List[i];

        //INICIO BLOQUE: OBTENER WHERE
        var where = "";
        for (j = 0; j < objGPEDIT_TABLE.GPEDIT_COLUMNEVENT_List.length; j++)
        {
            var objGPEDIT_COLUMNEVENT = objGPEDIT_TABLE.GPEDIT_COLUMNEVENT_List[j];
            if (objGPEDIT_COLUMNEVENT.EVUPDATEWHERE == "S")
            {
                for (y = 0; y < this.GPQUERY.GPEDIT_COLUMN_List.length; y++)
                {
                    if (this.GPQUERY.GPEDIT_COLUMN_List[y].IDGPEDIT_COLUMN == objGPEDIT_COLUMNEVENT.IDGPEDIT_COLUMN)
                    {
                        var val = RecordOld.FieldName(this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME) //Obtengo el valor anterior de la celda
                        switch (this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNTYPE)
                        {
                            case "String":
                                Param.AddString(this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME + "_OLD", val.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                                break;
                            case "Text":
                                Param.AddString(this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME + "_OLD", val.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                                break;
                            case "Int32":
                                Param.AddInt32(this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME + "_OLD", val.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                                break;
                            case "DateTime":
                                Param.AddDateTime(this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME + "_OLD", new Date(val.Value), SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                                break;
                            case "Boolean":
                                Param.AddBoolean(this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME + "_OLD", val.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
                                break;
                            case "Decimal":
                                Param.AddDouble(this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME + "_OLD", val.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                                break;
                            case "Double":
                                Param.AddDouble(this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME + "_OLD", val.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                                break;
                            default:
                                Param.AddString(this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME + "_OLD", val.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                                break;
                        }
                    }
                    where += this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME + "= @[" + this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME + "_OLD] AND ";
                    break;
                }
            }
        }
        where = where.substring(0, where.length - 4);
        ////FIN BLOQUE: OBTENER WHERE


        ////INICIO BLOQUE: VERIFICA SI EXISTE REGISTRO
        query = "SELECT * FROM " + objGPEDIT_TABLE.EDIT_TABLENAME + " WHERE " + where;
        var openSelect = SysCfg.DB.SQL.Methods.OpenDataSet(query, Param.ToBytes());
        if (openSelect.ResErr.NotError)
        {
            if (openSelect.DataSet.RecordCount > 0) //SI EL REGISTRO EXISTE
            {
                //UPDATE SET
                var set = "";
                for (j = 0; j < objGPEDIT_TABLE.GPEDIT_COLUMNEVENT_List.length; j++)
                {
                    var objGPEDIT_COLUMNEVENT = objGPEDIT_TABLE.GPEDIT_COLUMNEVENT_List[j];
                    if (objGPEDIT_COLUMNEVENT.EVUPDATESET == "S")
                    {
                        for (y = 0; y < this.GPQUERY.GPEDIT_COLUMN_List.length; y++)
                        {
                            if (this.GPQUERY.GPEDIT_COLUMN_List[y].IDGPEDIT_COLUMN == objGPEDIT_COLUMNEVENT.IDGPEDIT_COLUMN)
                            {
                                var val = RecordNew.FieldName(this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME) //Obtengo el valor nuevo de la celda
                                switch (this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNTYPE)
                                {
                                    case "String":
                                        Param.AddString(this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME + "_NEW", val.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                                        break;
                                    case "Text":
                                        Param.AddString(this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME + "_NEW", val.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                                        break;
                                    case "Int32":
                                        Param.AddInt32(this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME + "_NEW", val.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                                        break;
                                    case "DateTime":
                                        Param.AddDateTime(this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME + "_NEW", new Date(val.Value), SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                                        break;
                                    case "Boolean":
                                        Param.AddBoolean(this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME + "_NEW", val.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
                                        break;
                                    case "Decimal":
                                        Param.AddDouble(this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME + "_NEW", val.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                                        break;
                                    case "Double":
                                        Param.AddDouble(this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME + "_NEW", val.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                                        break;
                                    default:
                                        Param.AddString(this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME + "_NEW", val.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                                        break;
                                }
                                set += "," + this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME + "= @[" + this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME + "_NEW] ";
                                break;
                            }
                        }
                    }
                }
                set = set.substring(1, set.length);
                query = "UPDATE " + objGPEDIT_TABLE.EDIT_TABLENAME + " SET " + set + " WHERE " + where;
                OpenExecute = SysCfg.DB.SQL.Methods.Exec(query, Param.ToBytes());
                if (!OpenExecute.ResErr.NotError)
                {
                    resp = false;
                    alert(OpenExecute.ResErr.Mesaje);
                }
                else
                {
                    resp = true;
                }
            }
            else
            {
                //INSERT
                var fields = "";
                var values = "";

                for (j = 0; j < objGPEDIT_TABLE.GPEDIT_COLUMNEVENT_List.length; j++)
                {
                    var objGPEDIT_COLUMNEVENT = objGPEDIT_TABLE.GPEDIT_COLUMNEVENT_List[j];

                    if (objGPEDIT_COLUMNEVENT.EVUPDATEWHERE == "S" || objGPEDIT_COLUMNEVENT.EVUPDATESET == "S")
                    {
                        for (y = 0; y < this.GPQUERY.GPEDIT_COLUMN_List.length; y++)
                        {
                            if (this.GPQUERY.GPEDIT_COLUMN_List[y].IDGPEDIT_COLUMN == objGPEDIT_COLUMNEVENT.IDGPEDIT_COLUMN)
                            {
                                var val = RecordNew.FieldName(this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME) //Obtengo el valor anterior de la celda
                                switch (this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNTYPE)
                                {
                                    case "String":
                                        Param.AddString(this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME, val.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                                        break;
                                    case "Text":
                                        Param.AddString(this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME, val.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                                        break;
                                    case "Int32":
                                        Param.AddInt32(this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME, val.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                                        break;
                                    case "DateTime":
                                        Param.AddDateTime(this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME, new Date(val.Value), SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                                        break;
                                    case "Boolean":
                                        Param.AddBoolean(this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME, val.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
                                        break;
                                    case "Decimal":
                                        Param.AddDouble(this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME, val.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                                        break;
                                    case "Double":
                                        Param.AddDouble(this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME, val.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                                        break;
                                    default:
                                        Param.AddString(this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME, val.Value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                                        break;
                                }
                                fields += "," + this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME;
                                values += ",@[" + this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME + "]";
                                break;
                            }
                        }
                    }
                }
                fields = fields.substring(1, fields.length);
                values = values.Remove(1, values.length);
                query = "INSERT INTO " + objGPEDIT_TABLE.EDIT_TABLENAME + " (" + fields + ") VALUES (" + values + ")";
                OpenExecute = SysCfg.DB.SQL.Methods.Exec(query, Param.ToBytes());
                if (!OpenExecute.ResErr.NotError)
                {
                    resp = false;
                    alert(OpenExecute.ResErr.Mesaje);
                }
                else
                {
                    resp = true;
                }
            }
        }
        else
        {
            //ERROR AL VERIFICAR SI EXISTE REGISTRO
            alert(openSelect.ResErr.Mesaje);
        }
    }
    return resp;
}

ItHelpCenter.TUfrGPView.prototype.DeleteRecord = function (RecordOld)
{
    var query = "";
    var resp = false;
    for (i = 0; i < this.GPQUERY.GPEDIT_TABLE_List.length; i++)
    {
        var Param = new SysCfg.Stream.Properties.TParam();
        Param.Inicialize();
        var objGPEDIT_TABLE = this.GPQUERY.GPEDIT_TABLE_List[i];
        query = "DELETE FROM " + objGPEDIT_TABLE.EDIT_TABLENAME + " WHERE ";
        var countWhere = 1;

        for (j = 0; j < objGPEDIT_TABLE.GPEDIT_COLUMNEVENT_List.length; j++)
        {
            var objGPEDIT_COLUMNEVENT = objGPEDIT_TABLE.GPEDIT_COLUMNEVENT_List[j];
            if (objGPEDIT_COLUMNEVENT.EVDELETE == "S")
            {
                for (y = 0; y < this.GPQUERY.GPEDIT_COLUMN_List.length; y++)
                {
                    if (this.GPQUERY.GPEDIT_COLUMN_List[y].IDGPEDIT_COLUMN == objGPEDIT_COLUMNEVENT.IDGPEDIT_COLUMN)
                    {
                        deleteQuery = true;
                        var val = RecordOld.FieldName(this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME) //Obtengo el valor de la celda
                        if (countWhere == 1)
                        {

                            query += this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME + " = '" + val.Value + "'";
                            countWhere++;
                        }
                        else
                        {
                            query += " AND " + this.GPQUERY.GPEDIT_COLUMN_List[y].EDIT_COLUMNNAME + " = '" + val.Value + "'";
                        }
                    }
                    break;
                }
            }
        }
        if (deleteQuery)
        {
            var OpenExecute = SysCfg.DB.SQL.Methods.Exec(query, Param.ToBytes());
            if (!OpenExecute.ResErr.NotError)
            {
                resp = false;
                alert(OpenExecute.ResErr.Mesaje);
            }
            else
            {
                resp = true;
            }
        }
    }
    return resp;
}


ItHelpCenter.TUfrGPView.prototype.OnGridSelectedRowChanged = null;
ItHelpCenter.TUfrGPView.prototype.GridSelectedRowChanged = function (SelectedRow)
{
    var _this = this.TParent();    
    if (_this.OnGridSelectedRowChanged != null)
    {
        _this.OnGridSelectedRowChanged(SelectedRow);
    }    
}

