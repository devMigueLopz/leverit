﻿
ItHelpCenter.PageManager.TGoToPageType =
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    Reload: { value: 0, name: "Reload" }, // Reload
    Index: { value: 1, name: "Index" },  //llamaria Index.js (LoginConfig , Login)
    LoginConfig: { value: 2, name: "LoginConfig" }, // LoginConfig.js (Index)
    Login: { value: 3, name: "Login" }, // Login.js (changepassword, BeforeMain)
    ChangePassword: { value: 4, name: "ChangePassword" }, //changepassword.js (BeforeMain)
    BeforeMain: { value: 5, name: "BeforeMain" }, // (SMConsoleManagerSwitch o DynamicSTEFAtentionCase)(Main)
    Main: { value: 6, name: "Main" }, // Main.js
    CloseSession: { value: 7, name: "CloseSession" } // Reload
}


ItHelpCenter.PageManager.GoToPageType = function (GoToPageType) {
    var ElementDiv = document.body.firstElementChild;
    ElementDiv.innerHTML = "";
    switch (GoToPageType) {
        case ItHelpCenter.PageManager.TGoToPageType.CloseSession:
            ItHelpCenter.App.Application_Exit(undefined, undefined)
            {
                location.href = SysCfg.App.Properties.xRaiz + 'CloseSession.aspx';
            }
            break;
        case ItHelpCenter.PageManager.TGoToPageType.Reload:
            ItHelpCenter.App.Application_Exit(undefined, undefined)
            {
                location.href = SysCfg.App.Properties.xRaiz + 'Index.aspx';

            }
            break;
        case ItHelpCenter.PageManager.TGoToPageType.Index:

            var ElementDiv = document.body.firstElementChild;
            index = new ItHelpCenter.TIndex(
            ElementDiv,
            function (OutGoToPageType) {
                ItHelpCenter.PageManager.GoToPageType(OutGoToPageType);
            });            
            break;
        case ItHelpCenter.PageManager.TGoToPageType.LoginConfig:
            var ElementDiv = document.body.firstElementChild;
            Config = new ItHelpCenter.TLoguinConfig(
            ElementDiv,
            function (OutGoToPageType) {
                ItHelpCenter.PageManager.GoToPageType(OutGoToPageType);
            }, UsrCfg.Version._UsrCfgAppVersion);
            break;
        case ItHelpCenter.PageManager.TGoToPageType.Login:
            Login = new ItHelpCenter.TLogin(
            ElementDiv,
            function (OutGoToPageType) {
                ItHelpCenter.PageManager.GoToPageType(OutGoToPageType);
            }, UsrCfg.Version._UsrCfgAppVersion);       
            break;
        case ItHelpCenter.PageManager.TGoToPageType.ChangePassword:
            ChangePsw = new ItHelpCenter.TChangePsw(
                   ElementDiv,
                   function (OutGoToPageType) {
                       ItHelpCenter.PageManager.GoToPageType(OutGoToPageType);
                   }, UsrCfg.Version._UsrCfgAppVersion);
            break;
        case ItHelpCenter.PageManager.TGoToPageType.BeforeMain:
            try { //frAtentionCasesPending                                                   
                var idcase = -1;

        


               // var GetPegeTraslate = new Source.Page.GetPegeTraslate(ElementDiv, 1);


               //var SMConsoleManagerSwitch = new ItHelpCenter.SD.CaseManager.Atention.TSMConsoleManagerSwitch(//MODO Forza caso 3 en owner             
               //ElementDiv,
               //function (_this) {
               //    ItHelpCenter.PageManager.GoToPageType(ItHelpCenter.PageManager.TGoToPageType.Main);
               //},
               //-1,0,1  //MODO Forza caso 3 en owner   
               //);
              

                
                var SMConsoleUserSwitch = new ItHelpCenter.SD.CaseUser.Atention.TSMConsoleUserSwitch(//MODO Normal 
                ElementDiv,
                function (_this) {  
                    ItHelpCenter.PageManager.GoToPageType(ItHelpCenter.PageManager.TGoToPageType.Main);
                },                                              
                idcase  //MODO Normal 
                );
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("PageManager.js ItHelpCenter.PageManager.GoToPageType", e);
                ItHelpCenter.PageManager.GoToPageType(ItHelpCenter.PageManager.TGoToPageType.Reload);
                //location.reload();
            }
            break;

        case ItHelpCenter.PageManager.TGoToPageType.Main:
            var ElementDiv = new TVclBodyMain(document.body.firstElementChild, "idmain");
            //////////////////////////////////////////////////////////////MAIN////////////////////////////////////////////////////
            UsrCfg.Properties.Main = new TMain(
                ElementDiv,
                function (Outthis) {
                    alert("User " + Outthis.CI_GENERICNAME + " Session Close");
                }
            );
            //$(window).resize(function () {
            //    Main.ContainerResolution();
            //})

            break;


        default:
            break;
    }

}
