// var TUfrPanelServicesControl = function (inObjectHtml, Id, inCallback){
ItHelpCenter.TPanelServicesManager.PanelServicesMain = function (inObjectHtml, Id, inCallback) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this._ObjectHtml = inObjectHtml;
    this._Id = Id;

    this.Mythis = "PanelServicesMain";

    UsrCfg.Traslate.GetLangText(_this.Mythis, "VclComboBoxItem0.Text", "BASIC");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "VclComboBoxItem1.Text", "MAPPING");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1", "Ya existen registros en la Base de Datos");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btn2.Text", "Add Data");
    _this.Load();

}

ItHelpCenter.TPanelServicesManager.PanelServicesMain.prototype.Load = function () {
	var _this = this.TParent();
	try{
		var AllContainer = new TVclStackPanel(_this._ObjectHtml, '', 2, [[12, 12], [12, 12], [12, 12], [12, 12], [12, 12]]);
		var Container_1 = AllContainer.Column[0].This;
		var Container_2 = AllContainer.Column[1].This;
		Container_1.style.marginTop = '10px';
		Container_1.style.textAlign = 'center';

		var containerBtn = new TVclStackPanel(Container_1, '', 2, [[8, 4], [8, 4], [8, 4], [8, 4], [8, 4]]);

		var btn2 = new TVclInputbutton(containerBtn.Column[1].This, "");
		btn2.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btn2.Text");
		btn2.VCLType = TVCLType.BS;
		btn2.onClick = function () {
			var PanelServiceProfiler = new Persistence.PanelService.TPanelServiceProfiler();
			PanelServiceProfiler.Fill();
			if (PanelServiceProfiler.PSGROUPList.length === 0) {
				PanelServiceProfiler.CreateAll();
			} else {
			    alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1"));
			}
		}
		btn2.This.style.paddingRight = "5%";

		var VclComboBoxDIV = new TVclComboBox2(containerBtn.Column[0].This, '');
		VclComboBoxDIV.Width = "250px";

		VclComboBoxDIV.This.style.height = "30px";
		VclComboBoxDIV.This.style.textAlign = 'center';

		var VclComboBoxItem0 = new TVclComboBoxItem();
		VclComboBoxItem0.Value = 0;
		VclComboBoxItem0.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "VclComboBoxItem0.Text"); //"BASIC";		
		VclComboBoxItem0.Tag = "";
		VclComboBoxDIV.AddItem(VclComboBoxItem0);

		var VclComboBoxItem1 = new TVclComboBoxItem();
		VclComboBoxItem1.Value = 1;
		VclComboBoxItem1.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "VclComboBoxItem1.Text");//"MAPPING";
		VclComboBoxItem1.Tag = "";
		VclComboBoxDIV.AddItem(VclComboBoxItem1);

		VclComboBoxDIV.selectedIndex = 0;

		var PSEditor_1 = new ItHelpCenter.TPanelServicesManager.PanelServicesBasic(Container_2, '', '');

		VclComboBoxDIV.onChange = function () {
			if (VclComboBoxDIV.selectedIndex == 0) {
				Container_2.innerHTML = "";
				PSEditor_1.Load();
			}
			else if (VclComboBoxDIV.selectedIndex == 1) {
				Container_2.innerHTML = "";
				var PSEditor_2 = new ItHelpCenter.TPanelServicesManager.PanelServicesMapping(Container_2, '');
			}
		}
		VclComboBoxDIV.This.parentNode.style.textAlign = "right";
		VclComboBoxDIV.This.parentNode.style.paddingRight = "7%";
	}
	catch (e) {
        SysCfg.Log.Methods.WriteLog("PanelServicesMain.js ItHelpCenter.TPanelServicesManager.PanelServicesMain", e);
    }
}