﻿ItHelpCenter.TControlPanelServices = function (inObjectHtml, Id, inCallBack, IDGroup) {
	this.TParent = function () {
		return this;
	}.bind(this);

	var _this = this.TParent();
	this._Id = Id;
	this._ObjectHtml = inObjectHtml;
	this._IDGroup = IDGroup;
	this._Key = null;
	this.ObjControl = null;
	this.Show = true;

	 this.Mythis = "ControlPanelServices";
	 UsrCfg.Traslate.GetLangText(_this.Mythis, "ALERTGROUP", "The user who tries to search does not exist in our registry.");
	 UsrCfg.Traslate.GetLangText(_this.Mythis, "ALERTMAIN", "The user you are looking for does not contain records to display.");
	_this.Load();
}

ItHelpCenter.TControlPanelServices.prototype.Load = function () {
	var _this = this.TParent();
	try{
		var PSProfiler = new Persistence.PanelService.TPanelServiceProfiler();
		PSProfiler.Fill();

		var PSGROUPList = PSProfiler.PSGROUPList;
		// var PSMAINList = PSProfiler.PSMAINList;
		// var PSSECUNDARYList = PSProfiler.PSSECUNDARYList;
		var Container = new TVclStackPanel(this._ObjectHtml, 'PSControl', 1, [[12], [12], [12], [12], [12]]);
		//Container.Column[0].This.style.padding = '0 0 0 0px';
		var PService = new Componet.TPanelServices(Container.Column[0].This, _this._Id, "");

		for (var i = 0; i < PSGROUPList.length; i++) {
			if (PSGROUPList[i]['IDPSGROUP'] == this._IDGroup) this._Key = i;
		}
		if (this._Key != null) {
			var MAINList = PSGROUPList[this._Key]['PSMAINList'];

			if (MAINList.length != 0) {
				var SECUNDARYList = null;
				for (var i = 0; i < MAINList.length; i++) {
					var ElementTab = new Componet.TElements.TPanelServicesElementTab();
					ElementTab.Id = MAINList[i]['IDPSMAIN'];
					ElementTab.Position = MAINList[i]['MAIN_POSITION'];
					ElementTab.Image = MAINList[i]['MAIN_ICON'];
					ElementTab.Title = MAINList[i]['MAIN_TITLE'];
					ElementTab.TitleColor = MAINList[i]['MAIN_TITLECOLOR'];
					ElementTab.BackgroundActive = MAINList[i]['MAIN_BACKGROUNDACTIVE'];
					ElementTab.TitleActiveColor = MAINList[i]['MAIN_TITLEACTIVECOLOR'];
					ElementTab.Border = MAINList[i]['MAIN_BORDERACTIVE'];
					ElementTab.BackgroundHover = MAINList[i]['MAIN_BACKGROUNDHOVER'];
					ElementTab.TitleHoverColor = MAINList[i]['MAIN_TITLEHOVERCOLOR'];
					ElementTab.Description = MAINList[i]['MAIN_DESCRIPTION'];
					PService.TPanelServicesElementsAdd(ElementTab);

					SECUNDARYList = MAINList[i]['PSSECUNDARYList'];
					for (var j = 0; j < SECUNDARYList.length; j++) {
						var ElementContent = new Componet.TElements.TPanelServicesElementTabContent();
						ElementContent.ContentId = SECUNDARYList[j]['IDPSSECUNDARY'];
						ElementContent.ContentPosition = SECUNDARYList[j]['SECUNDARY_POSITION'];
						ElementContent.ContentImage = SECUNDARYList[j]['SECUNDARY_ICON'];
						ElementContent.ContentTitle = SECUNDARYList[j]['SECUNDARY_TITLE'];
						ElementContent.ContentTextColor = SECUNDARYList[j]['SECUNDARY_TEXTCOLOR'];
						ElementContent.ContentDescription = SECUNDARYList[j]['SECUNDARY_DESCRIPTION'];
						ElementContent.ContentPhrase = SECUNDARYList[j]['SECUNDARY_PHRASE'];
						PService.Elements[i].TPanelServiceElementsContentAdd(ElementContent);
					}
				}
				PService.BackgroundContainer = PSGROUPList[this._Key]['GROUP_BACKGROUND'];
				PService.Orientation = PService.OrientationType.Horizontal;
				// PService.Orientation = PService.OrientationType.Vertical;
				PService.Load();

				_this.ObjControl = PService;

				// PService.Elements[0].ElementsContent[0].This
				// var texto = new Uspan(PService.Elements[0].ElementsContent[0].This,'','Texto de Prueba dentro del DropDownPanel');
			}
			else {
			    if (!UsrCfg.Version.IsProduction) {
			        alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ALERTMAIN"));
			    }
			    _this.Show = false;
			}
		}
		else {
		    if (!UsrCfg.Version.IsProduction) {
		        alert(UsrCfg.Traslate.GetLangText(_this.mythis, "ALERTGROUP"));
		    }
			_this.Show = false;
		}
		Container.Row.This.classList.remove('row');
		Container.Row.This.style.width = '100%';
		if (Source.Menu.IsMobil) {
			//Container.Column[0].This.style.padding = '0 0 0 0px';
		}
	}
	catch (e){
		SysCfg.Log.Methods.WriteLog("ControlPanelServices.js ItHelpCenter.TControlPanelServices.prototype.Load", e);
	}
}
