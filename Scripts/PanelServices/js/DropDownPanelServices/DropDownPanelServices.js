﻿ItHelpCenter.TPanelServicesManager.TVclDropDownPanelService = function (ObjectHtml, Id) {
    var TParent = function () {
        return this;
    }.bind(this);
    this.This = null;
    this.RowHeader = null;
    this.RowBodyForm = null;
    this.RowBodyPreview = null;
    this.RowBody = null;
    this.RowBodyContent = null;
    this.Image = null;
    this._SrcUp = "image/24/Navigate-up.png";
    this._SrcDown = "image/24/Navigate-down.png";
    this._IsOpen = false;
    this._BorderAllColor = null;
    this._functionOpen = function () {

    };
    this._functionClose = function () {

    };
    var arrCont = new Array(1);
    arrCont[0] = new Array(1);
    arrCont[0][0] = new Array(2);
    arrCont[0][0][0] = new Array(2);
    arrCont[0][0][1] = new Array(1);
    arrCont[0][0][1][0] = new Array(3);
    arrCont[0][0][1][0][0] = new Array(1);
    arrCont[0][0][1][0][1] = new Array(1);
    arrCont[0][0][1][0][2] = new Array(1);
    try {
        this.This = VclDivitions(ObjectHtml, "Acodion" + Id, arrCont);
        BootStrapCreateRow(this.This[0].This);
        BootStrapCreateColumn(this.This[0].Child[0].This, [12, 12, 12, 12, 12]);
        BootStrapCreateRows(this.This[0].Child[0].Child);
        BootStrapCreateColumns(this.This[0].Child[0].Child[0].Child, [[10, 2], [10, 2], [11, 1], [11, 1], [11, 1]]);
        BootStrapCreateColumn(this.This[0].Child[0].Child[1].Child[0].This, [12, 12, 12, 12, 12]);
        BootStrapCreateRows(this.This[0].Child[0].Child[1].Child[0].Child);
        BootStrapCreateColumn(this.This[0].Child[0].Child[1].Child[0].Child[0].Child[0].This, [12, 12, 12, 12, 12]);
        BootStrapCreateColumn(this.This[0].Child[0].Child[1].Child[0].Child[1].Child[0].This, [12, 12, 12, 12, 12]);
        BootStrapCreateColumn(this.This[0].Child[0].Child[1].Child[0].Child[2].Child[0].This, [12, 12, 12, 12, 12]);

        this.RowHeader = new TVclDiv(this.This[0].Child[0].Child[0].Child[0].This);
        this.RowBody = new TVclDiv(this.This[0].Child[0].Child[1].Child[0].This);
        this.RowBodyForm = new TVclDiv(this.This[0].Child[0].Child[1].Child[0].Child[0].Child[0].This);
        this.RowBodyPreview = new TVclDiv(this.This[0].Child[0].Child[1].Child[0].Child[1].Child[0].This);
        this.RowBodyContent = new TVclDiv(this.This[0].Child[0].Child[1].Child[0].Child[2].Child[0].This);

        this.RowHeader.This.style.padding = "8px";
        this.RowBody.This.style.padding = "8px";


        $(this.RowBody.This).toggle();
        this.This[0].This.style.borderTop = "3px solid #F39C12";
        this.This[0].This.style.borderBottom = "1px solid #E6E6E6";
        this.This[0].This.style.borderLeft = "1px solid #E6E6E6";
        this.This[0].This.style.borderRight = "1px solid #E6E6E6";
        this.Image = new TVclImagen(this.This[0].Child[0].Child[0].Child[1].This, "");
        this.Image.This.style.float = "right";
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("DropDownPanelServices.js ItHelpCenter.TPanelServicesManager.TVclDropDownPanelService", e);
    }

    this.Image.onClick = function () {
        try {
            var _this = TParent();
            $(_this.RowBody.This).toggle("slow", function () {
                try {

                    if (_this._IsOpen) {
                        _this.Image.Src = _this._SrcDown;
                        _this.RowBody.This.style.borderTop = "0px solid";
                        _this.RowBody.This.style.borderTopColor = "";
                        _this.Image.Title = "Open";
                        _this._IsOpen = false;
                        _this._functionClose();

                    }
                    else {
                        _this.RowBody.This.style.borderTop = "1px solid";
                        _this.RowBody.This.style.borderTopColor = _this.This[0].This.style.borderBottomColor;
                        _this.Image.Src = _this._SrcUp;
                        _this.Image.Title = "Close";
                        _this._IsOpen = true;
                        _this._functionOpen();
                    }
                } catch (e) {
                    SysCfg.Log.Methods.WriteLog("DropDownPanelServices.js ItHelpCenter.TPanelServicesManager.TVclDropDownPanelService this.Image.onClick $(_this.RowBody.This).toggle('slow', function ()", e);
                }
            });
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("DropDownPanelServices.js ItHelpCenter.TPanelServicesManager.TVclDropDownPanelService this.Image.onClick ", e);
        }
    }
    this.Image.Src = this._SrcDown;
    this.Image.Title = "Open";
    this.Image.Cursor = "pointer";
    this.Image.This.style.marginTop = "8px";

    Object.defineProperty(this, 'BackgroundColor', {
        get: function () {
            return this.This[0].This.style.backgroundColor;
        },
        set: function (_Value) {
            this.This[0].This.style.backgroundColor = _Value;
        }
    });

    Object.defineProperty(this, 'BoderTopColor', {
        get: function () {
            return this.This[0].This.style.borderTopColor;
        },
        set: function (_Value) {
            this.This[0].This.style.borderTopColor = _Value;
        }
    });

    Object.defineProperty(this, 'BoderAllColor', {
        get: function () {
            return this.This[0].This.style.borderBottomColor;
        },
        set: function (_Value) {
            this.This[0].This.style.borderBottomColor = _Value
            this.This[0].This.style.borderLeftColor = _Value
            this.This[0].This.style.borderRightColor = _Value
        }
    });

    Object.defineProperty(this, 'SrcUp', {
        get: function () {
            return this._SrcUp;
        },
        set: function (_Value) {
            this._SrcUp = _Value;
        }
    });

    Object.defineProperty(this, 'SrcDown', {
        get: function () {
            return this._SrcDown;
        },
        set: function (_Value) {
            this._SrcDown = _Value;
        }
    });

    Object.defineProperty(this, 'FunctionOpen', {
        get: function () {
            return this._functionOpen;
        },
        set: function (_Value) {
            this._functionOpen = _Value;
        }
    });

    Object.defineProperty(this, 'FunctionClose', {
        get: function () {
            return this._functionClose;
        },
        set: function (_Value) {
            this._functionClose = _Value;
        }
    });

    Object.defineProperty(this, 'MarginTop', {
        get: function () {
            return this.This[0].This.style.marginTop;
        },
        set: function (_Value) {
            this.This[0].This.style.marginTop = _Value;
        }
    });

    Object.defineProperty(this, 'MarginBottom', {
        get: function () {
            return this.This[0].This.style.marginBottom;
        },
        set: function (_Value) {
            this.This[0].This.style.marginBottom = _Value;
        }
    });

    Object.defineProperty(this, 'MarginLeft', {
        get: function () {
            return this.This[0].This.style.marginLeft;
        },
        set: function (_Value) {
            this.This[0].This.style.marginLeft = _Value;
        }
    });

    Object.defineProperty(this, 'MarginRight', {
        get: function () {
            return this.This[0].This.style.marginRight;
        },
        set: function (_Value) {
            this.This[0].This.style.marginRight = _Value;
        }
    });

    Object.defineProperty(this, 'Visible', {
        get: function () {
            if (this.This[0].This.style.display == "none")
                return false;
            return true;
        },
        set: function (_Value) {
            if (_Value)
                this.This[0].This.style.display = "";
            else
                this.This[0].This.style.display = "none";
        }
    });

    this.AddToHeader = function (ObjectHtml) {
        try {
            $(this.RowHeader.This).append(ObjectHtml);

        } catch (e) {
            SysCfg.Log.Methods.WriteLog("DropDownPanelServices.js ItHelpCenter.TPanelServicesManager.TVclDropDownPanelService this.AddToHeader()", e);
        }
    }
    this.AddToBody = function (ObjectHtml) {
        try {
            $(this.RowBody.This).append(ObjectHtml);

        } catch (e) {
            SysCfg.Log.Methods.WriteLog("DropDownPanelServices.js ItHelpCenter.TPanelServicesManager.TVclDropDownPanelService this.AddToBody()", e);
        }
    }
    this.AddToForm = function (ObjectHtml) {
        try {
            $(this.RowBodyForm.This).append(ObjectHtml);

        } catch (e) {
            SysCfg.Log.Methods.WriteLog("DropDownPanelServices.js ItHelpCenter.TPanelServicesManager.TVclDropDownPanelService this.AddToForm()", e);
        }
    }
    this.AddToPreview = function (ObjectHtml) {
        try {
            $(this.RowBodyPreview.This).append(ObjectHtml);

        } catch (e) {
            SysCfg.Log.Methods.WriteLog("DropDownPanelServices.js ItHelpCenter.TPanelServicesManager.TVclDropDownPanelService this.AddToPreview()", e);
        }
    }
    this.AddToContent = function (ObjectHtml) {
        try {
            $(this.RowBodyContent.This).append(ObjectHtml);

        } catch (e) {
            SysCfg.Log.Methods.WriteLog("DropDownPanelServices.js ItHelpCenter.TPanelServicesManager.TVclDropDownPanelService this.AddToContent()", e);
        }
    }
    this.CleanHeader = function () {
        try {
            $(this.RowHeader.This).html("");

        } catch (e) {
            SysCfg.Log.Methods.WriteLog("DropDownPanelServices.js ItHelpCenter.TPanelServicesManager.TVclDropDownPanelService this.CleanHeader()", e);
        }
    }

    this.CleanBody = function () {
        try {
            $(this.RowBody.This).html("");

        } catch (e) {
            SysCfg.Log.Methods.WriteLog("DropDownPanelServices.js ItHelpCenter.TPanelServicesManager.TVclDropDownPanelService this.CleanBody()", e);
        }
    }
    this.CleanForm = function () {
        try {
            $(this.RowBodyForm.This).html("");

        } catch (e) {
            SysCfg.Log.Methods.WriteLog("DropDownPanelServices.js ItHelpCenter.TPanelServicesManager.TVclDropDownPanelService this.CleanForm()", e);
        }
    }

    this.CleanPreview = function () {
        try {
            $(this.RowBodyPreview.This).html("");

        } catch (e) {
            SysCfg.Log.Methods.WriteLog("DropDownPanelServices.js ItHelpCenter.TPanelServicesManager.TVclDropDownPanelService this.CleanPreview()", e);
        }
    }

    this.CleanContent = function () {
        try {
            $(this.RowBodyContent.This).html("");

        } catch (e) {
            SysCfg.Log.Methods.WriteLog("DropDownPanelServices.js ItHelpCenter.TPanelServicesManager.TVclDropDownPanelService this.CleanContent()", e);
        }
    }
}

