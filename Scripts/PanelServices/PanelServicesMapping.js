ItHelpCenter.TPanelServicesManager.PanelServicesMapping = function (inObject, incallback) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.ObjectHtml = inObject;
    this.CallbackModalResult = incallback;
    this.Mythis = "PanelServicesMapping";
    UsrCfg.Traslate.GetLangText(_this.Mythis, "ID", "Id");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "TITLE", "Title");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "DESCRIPTION", "Description");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "POSITION", "Position");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "ICON", "Icon");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "PHRASE", "Phrase");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "FATHER", "Parent");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "SEARCH", "Search");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "ADD", "Add");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "UPDATE", "Update");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "DELETE", "Delete");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "TITLEGROUP", "Users");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "TITLEMAIN", "Modules");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "TITLESECUNDARY", "Sub Modules");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "SAVE", "Save");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "PREVIEW", "Delete");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "CLEAR", "Clear");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "SYNCHRONIZE", "Synchronize");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "GROUP", "Group");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "MAIN", "Main");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "SECONDARY", "Secondary");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "CORRECT", "Registration saved correctly");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECT", "An error occurred while saving the record");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "CORRECTUPDATE", "The registration was updated correctly");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECTUPDATE", "An error occurred while updating a record");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "ADDITEM", "Add new Item");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "DELETEITEM", "The record (s) were deleted correctly");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "FIELDOBLIGATORY", "You must enter a title to complete the registration");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "DELETEGROUP", "Do you want to delete the element Group and everything it contains?");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "DELETEMAIN", "Do you want to delete the element Main and everything it contains?");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "DELETESECUNDARY", "Do you want to delete the element Secondary?");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECTPOSITION", "You must add a correct number in the position field");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BACKGROUNDCOLOR", "Background Color");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BORDERCOLOR", "Border Color");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "TITLECOLOR", "Title Color");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BACKGROUNDACTIVECOLOR", "Background Color Active");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "TITLEACTIVECOLOR", "Title Color Active ");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BORDERCOLORACTIVE", "Border Color Active ");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "TITLEHOVERCOLOR", "Title Color Hover");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BACKGROUNDHOVERCOLOR01", "Background Color Hover");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "TEXTCOLOR", "Text Color");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "GROUPBACKGROUND", "Enter the background color correctly");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "GROUPBORDER", "Enter the border color correctly");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "MAINTITLECOLOR", "Enter the title color correctly");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "MAINBACKGROUNDACTIVE", "Enter the background active color correctly");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "MAINTITLEACTIVECOLOR", "Enter the title active color correctly");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "MAINBORDERACTIVE", "Enter the border active color correctly");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "MAINBACKGROUNDHOVER", "Enter the background hover color correctly");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "MAINTITLEHOVERCOLOR", "Enter the title hover color correctly");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "SECUNDARYTEXTCOLOR", "Enter the text color correctly");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "PATH", "Path");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "GROUPPATH", "Enter the path color correctly");
    _this.LoadScript(SysCfg.App.Properties.xRaiz + "Componet/Color/Plugin/spectrum.js", function () {
        _this.AddStyleFile(SysCfg.App.Properties.xRaiz + "Componet/Color/Plugin/spectrum.css", function () {
            _this.AddStyleFile(SysCfg.App.Properties.xRaiz + "Css/PanelService/style/css/PanelServicesManager.css", function () {
                _this.Load();
            }, function (e) {
                SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping - _DBTranslate.SetTranslate - ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.AddStyleFile (" + SysCfg.App.Properties.xRaiz + "Css/PanelService/style/css/PanelServicesManager.css)", e);
            });
        }, function (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping - _DBTranslate.SetTranslate - ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.AddStyleFile (" + SysCfg.App.Properties.xRaiz + "Componet/Color/Plugin/spectrum.css)", e);
        });
    }, function (e) {
        SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping - _DBTranslate.SetTranslate - ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.LoadScript (" + SysCfg.App.Properties.xRaiz + "Componet/Color/Plugin/spectrum.js)", e);
    });

    
}
ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Load = function () {
    var _this = this.TParent();
    var Translate = _this.DBTranslate
    //Page//
    try {
        var Page = new _this.Page(_this.ObjectHtml);
        function Preview() {
            try {
                Page.RemovePreviewGroup();
                var Preview = new _this.Preview(Translate);
                if (Preview.CreatePreview(parseInt(ComboGroup.Combo.Value))) {
                    return Page.AddPreviewGroup(Preview.ContentPreview);
                }
                return false;
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Load Preview()", e);
            }
        }
        Page.CreatePage(_this.ObjectHtml);
        // End Page//
        // Data //
        var Data = new _this.Data();
        // End Data //
        // Group //
        var ComboData = Data.DataComboInitGroup();
        var ComboGroup = new _this.ComboBox(Translate);
        ComboGroup.CrateCombo("GROUP");
        ComboGroup.AllAddData(ComboData);
        Page.AddComboGroup(ComboGroup.ContentBoxCombo);
        var IconsGroup = new _this.Icons(Translate);
        IconsGroup.Buttons.Clear.This.style.display = "none";
        Page.AddIconsGroup(IconsGroup.ContentBoxImage);
        //FUNCTIONS GROUP //
        IconsGroup.Buttons.Add.onClick = function () {
            try {
                var ModalGroup = new _this.Modal(Translate);
                var FormGroup = new _this.FormGroup(Translate);
                FormGroup.Controls.Save.onClick = function () {
                    try {
                        var positionCombo = ComboGroup.Combo.Value;
                        var PSGROUP = new Persistence.PanelService.Properties.TPSGROUP();
                        PSGROUP.GROUP_NAME = FormGroup.Controls.Title.Text;
                        PSGROUP.GROUP_DESCRIPTION = FormGroup.Controls.Description.Text;
                        PSGROUP.GROUP_BACKGROUND = FormGroup.Controls.Background.Color;
                        PSGROUP.GROUP_BORDER = FormGroup.Controls.Border.Color;
                        PSGROUP.GROUP_PATH = FormGroup.Controls.Path.Text;
                        if (PSGROUP.GROUP_NAME.trim() === "") {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "FIELDOBLIGATORY"));
                            return;
                        }
                        if (PSGROUP.GROUP_BACKGROUND.trim() === "") {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "GROUPBACKGROUND"));
                            return;
                        }
                        if (PSGROUP.GROUP_BORDER.trim() === "") {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "GROUPBORDER"));
                            return;
                        }
                        if (PSGROUP.GROUP_PATH.trim() === "") {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "GROUPPATH"));
                            return;
                        }
                        if (Data.DataAddGroup(PSGROUP)) {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "CORRECT"));
                            var initVal = ComboGroup.AddData({ Id: PSGROUP.IDPSGROUP, Name: PSGROUP.GROUP_NAME });
                            if (initVal) {
                                var ComboDataMain = Data.DataComboInitMain(parseInt(ComboGroup.Combo.Value));
                                ComboMain.AllDeleteDataItems();
                                ComboMain.AllAddData(ComboDataMain);
                                var ComboDataSecundary = Data.DataComboInitSecundary(parseInt(ComboMain.Combo.Value));
                                ComboSecundary.AllDeleteDataItems();
                                ComboSecundary.AllAddData(ComboDataSecundary);
                                AddVisibleIconsGroup();
                            }
                            else {
                                ComboGroup.Combo.Value = positionCombo;
                            }
                            FormGroup.ClearData();
                        }
                        else {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECT"));
                        }
                    } catch (e) {
                        SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Load IconsGroup.Buttons.Add.onClick FormGroup.Controls.Save.onClick", e);
                    }
                }
                ModalGroup.AddTitle(UsrCfg.Traslate.GetLangText(_this.Mythis, "GROUP"));
                ModalGroup.AddBoddy(FormGroup.ContentControls);
                ModalGroup.Modal.ShowModal();
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Load IconsGroup.Buttons.Add.onClick", e);
            }
        }
        IconsGroup.Buttons.Update.onClick = function () {
            try {
                var ModalGroup = new _this.Modal(Translate);
                var FormGroup = new _this.FormGroup(Translate);
                FormGroup.AddData(Data.DataGetGroup(parseInt(ComboGroup.Combo.Value)));
                FormGroup.Controls.Save.onClick = function () {
                    try {
                        var PSGROUP = new Persistence.PanelService.Properties.TPSGROUP();
                        PSGROUP.IDPSGROUP = parseInt(ComboGroup.Combo.Value);
                        PSGROUP.GROUP_NAME = FormGroup.Controls.Title.Text;
                        PSGROUP.GROUP_DESCRIPTION = FormGroup.Controls.Description.Text;
                        PSGROUP.GROUP_BACKGROUND = FormGroup.Controls.Background.Color;
                        PSGROUP.GROUP_BORDER = FormGroup.Controls.Border.Color;
                        PSGROUP.GROUP_PATH = FormGroup.Controls.Path.Text;
                        if (PSGROUP.GROUP_NAME.trim() === "") {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "FIELDOBLIGATORY"));
                            return;
                        }
                        if (PSGROUP.GROUP_BACKGROUND.trim() === "") {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "GROUPBACKGROUND"));
                            return;
                        }
                        if (PSGROUP.GROUP_BORDER.trim() === "") {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "GROUPBORDER"));
                            return;
                        }
                        if (PSGROUP.GROUP_PATH.trim() === "") {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "GROUPPATH"));
                            return;
                        }
                        if (Data.DataUpdateGroup(PSGROUP)) {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "CORRECTUPDATE"));
                            ComboGroup.Combo.Text = PSGROUP.GROUP_NAME;
                            if (Page.VisiblePreview) {
                                Preview();
                            }
                        }
                        else {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECTUPDATE"));
                        }
                    } catch (e) {
                        SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Load IconsGroup.Buttons.Update.onClick FormGroup.Controls.Save.onClick", e);
                    }
                }
                ModalGroup.AddTitle(UsrCfg.Traslate.GetLangText(_this.Mythis, "GROUP"));
                ModalGroup.AddBoddy(FormGroup.ContentControls);
                ModalGroup.Modal.ShowModal();
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Load IconsGroup.Update.Add.onClick", e);
            }
        }
        IconsGroup.Buttons.Preview.onClick = function () {
            try {
                Preview();
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Load IconsGroup.Buttons.Preview.onClick", e);
            }
        }
        IconsGroup.Buttons.Delete.onClick = function () {
            try {
                var confirmDelete = confirm(UsrCfg.Traslate.GetLangText(_this.Mythis, "DELETEGROUP"));
                if (confirmDelete) {
                    var PSGROUP = new Persistence.PanelService.Properties.TPSGROUP();
                    PSGROUP.IDPSGROUP = parseInt(ComboGroup.Combo.Value);
                    if (Data.DeleteAllGroup(PSGROUP)) {
                        var Init = ComboGroup.DeleteDataItem(parseInt(ComboGroup.Combo.Value));
                        if (!Init) {
                            ComboGroup.Combo.selectedIndex = 0;
                        }
                        else {
                            AddVisibleIconsGroup();
                        }
                        var ComboDataMain = Data.DataComboInitMain(parseInt(ComboGroup.Combo.Value));
                        ComboMain.AllDeleteDataItems();
                        ComboMain.AllAddData(ComboDataMain);
                        AddVisibleIconsMain();
                        var ComboDataSecundary = Data.DataComboInitSecundary(parseInt(ComboMain.Combo.Value));
                        ComboSecundary.AllDeleteDataItems();
                        ComboSecundary.AllAddData(ComboDataSecundary);
                        AddVisibleIconsSecundary();
                        if (Page.VisiblePreview) {
                            Preview();
                        }
                        alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "DELETEITEM"));
                    }
                    else {
                        alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NOTDELETE"));
                    }
                }
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Load IconsGroup.Buttons.Delete.onClick", e);
            }
        }
        // End Group //
        // Main //
        var ComboData = Data.DataComboInitMain(parseInt(ComboGroup.Combo.Value));
        var ComboMain = new _this.ComboBox(Translate);
        ComboMain.CrateCombo("MAIN");
        ComboMain.AllAddData(ComboData);
        Page.AddComboMain(ComboMain.ContentBoxCombo);
        var IconsMain = new _this.Icons(Translate);
        IconsMain.Buttons.Clear.This.style.display = "none";
        IconsMain.Buttons.Preview.This.style.display = "none";
        IconsMain.Buttons.Add.This.style.display = "none";
        Page.AddIconsMain(IconsMain.ContentBoxImage);
        //FUNCTIONS MAIN //
        IconsMain.Buttons.Add.onClick = function () {
            try {
                var positionCombo = ComboMain.Combo.Value;
                var ModalMain = new _this.Modal(Translate);
                var FormMain = new _this.FormMain(Translate);
                FormMain.RelationGroup(ComboGroup.Combo.Text);
                FormMain.Controls.Save.onClick = function () {
                    try {
                        if (FormMain.Controls.Title.Text.trim() === "") {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "FIELDOBLIGATORY"));
                            return;
                        }
                        if (FormMain.Controls.TitleColor.Color.trim() === "") {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "MAINTITLECOLOR"));
                            return;
                        }
                        if (FormMain.Controls.BackgroundActive.Color.trim() === "") {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "MAINBACKGROUNDACTIVE"));
                            return;
                        }
                        if (FormMain.Controls.TitleActiveColor.Color.trim() === "") {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "MAINTITLEACTIVECOLOR"));
                            return;
                        }
                        if (FormMain.Controls.BorderActive.Color.trim() === "") {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "MAINBORDERACTIVE"));
                            return;
                        }
                        if (FormMain.Controls.BackgroundHover.Color.trim() === "") {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "MAINBACKGROUNDHOVER"));
                            return;
                        }
                        if (FormMain.Controls.TitleHoverColor.Color.trim() === "") {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "MAINTITLEHOVERCOLOR"));
                            return;
                        }
                        if (isNaN(FormMain.Controls.Position.Text)) {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECTPOSITION"));
                            return
                        }
                        var PSMAIN = new Persistence.PanelService.Properties.TPSMAIN();
                        PSMAIN.MAIN_TITLE = FormMain.Controls.Title.Text;
                        PSMAIN.MAIN_DESCRIPTION = FormMain.Controls.Description.Text;
                        PSMAIN.MAIN_POSITION = parseInt(FormMain.Controls.Position.Text);
                        PSMAIN.MAIN_ICON = FormMain.Controls.Icon.Text;
                        PSMAIN.MAIN_TITLECOLOR = FormMain.Controls.TitleColor.Color.trim();
                        PSMAIN.MAIN_BACKGROUNDACTIVE = FormMain.Controls.BackgroundActive.Color.trim();
                        PSMAIN.MAIN_TITLEACTIVECOLOR = FormMain.Controls.TitleActiveColor.Color.trim();
                        PSMAIN.MAIN_BORDERACTIVE = FormMain.Controls.BorderActive.Color.trim();
                        PSMAIN.MAIN_BACKGROUNDHOVER = FormMain.Controls.BackgroundHover.Color.trim();
                        PSMAIN.MAIN_TITLEHOVERCOLOR = FormMain.Controls.TitleHoverColor.Color.trim();
                        PSMAIN.IDPSGROUP = parseInt(ComboGroup.Combo.Value);
                        if (Data.DataAddMain(PSMAIN)) {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "CORRECT"));
                            var initVal = ComboMain.AddData({ Id: PSMAIN.IDPSMAIN, Name: PSMAIN.MAIN_TITLE });
                            if (initVal) {
                                var ComboDataSecundary = Data.DataComboInitSecundary(parseInt(ComboMain.Combo.Value));
                                ComboSecundary.AllDeleteDataItems();
                                ComboSecundary.AllAddData(ComboDataSecundary);
                                AddVisibleIconsMain();
                            }
                            else {
                                ComboMain.Combo.Value = positionCombo;
                            }
                            FormMain.ClearData();
                            if (Page.VisiblePreview) {
                                Preview();
                            }
                        }
                        else {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECT"));
                        }
                    } catch (e) {
                        SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Load IconsMain.Buttons.Add.onClick FormMain.Controls.Save.onClick ", e);
                    }
                }
                ModalMain.AddTitle(UsrCfg.Traslate.GetLangText(_this.Mythis, "MAIN"));
                ModalMain.AddBoddy(FormMain.ContentControls);
                ModalMain.Modal.ShowModal();
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Load IconsMain.Buttons.Add.onClick", e);
            }
        }
        IconsMain.Buttons.Update.onClick = function () {
            try {
                var ModalMain = new _this.Modal(Translate);
                var FormMain = new _this.FormMain(Translate);
                FormMain.RelationGroup(ComboGroup.Combo.Text);
                FormMain.AddData(Data.DataGetMain(parseInt(ComboMain.Combo.Value)));
                FormMain.Controls.Save.onClick = function () {
                    try {
                        if (FormMain.Controls.Title.Text.trim() === "") {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "FIELDOBLIGATORY"));
                            return;
                        }
                        if (FormMain.Controls.TitleColor.Color.trim() === "") {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "MAINTITLECOLOR"));
                            return;
                        }
                        if (FormMain.Controls.BackgroundActive.Color.trim() === "") {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "MAINBACKGROUNDACTIVE"));
                            return;
                        }
                        if (FormMain.Controls.TitleActiveColor.Color.trim() === "") {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "MAINTITLEACTIVECOLOR"));
                            return;
                        }
                        if (FormMain.Controls.BorderActive.Color.trim() === "") {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "MAINBORDERACTIVE"));
                            return;
                        }
                        if (FormMain.Controls.BackgroundHover.Color.trim() === "") {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "MAINBACKGROUNDHOVER"));
                            return;
                        }
                        if (FormMain.Controls.TitleHoverColor.Color.trim() === "") {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "MAINTITLEHOVERCOLOR"));
                            return;
                        }
                        if (isNaN(FormMain.Controls.Position.Text)) {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECTPOSITION"));
                            return
                        }
                        var PSMAIN = new Persistence.PanelService.Properties.TPSMAIN();
                        PSMAIN.IDPSMAIN = parseInt(ComboMain.Combo.Value);
                        PSMAIN.MAIN_TITLE = FormMain.Controls.Title.Text;
                        PSMAIN.MAIN_DESCRIPTION = FormMain.Controls.Description.Text;
                        PSMAIN.MAIN_POSITION = parseInt(FormMain.Controls.Position.Text);
                        PSMAIN.MAIN_ICON = FormMain.Controls.Icon.Text;
                        PSMAIN.MAIN_TITLECOLOR = FormMain.Controls.TitleColor.Color.trim();
                        PSMAIN.MAIN_BACKGROUNDACTIVE = FormMain.Controls.BackgroundActive.Color.trim();
                        PSMAIN.MAIN_TITLEACTIVECOLOR = FormMain.Controls.TitleActiveColor.Color.trim();
                        PSMAIN.MAIN_BORDERACTIVE = FormMain.Controls.BorderActive.Color.trim();
                        PSMAIN.MAIN_BACKGROUNDHOVER = FormMain.Controls.BackgroundHover.Color.trim();
                        PSMAIN.MAIN_TITLEHOVERCOLOR = FormMain.Controls.TitleHoverColor.Color.trim();
                        PSMAIN.IDPSGROUP = parseInt(ComboGroup.Combo.Value);
                        if (Data.DataUpdateMain(PSMAIN)) {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "CORRECTUPDATE"));
                            ComboMain.Combo.Text = PSMAIN.MAIN_TITLE;
                            if (Page.VisiblePreview) {
                                Preview();
                            }
                        }
                        else {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECTUPDATE"));
                        }
                    } catch (e) {
                        SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Load IconsMain.Buttons.Add.onClick FormMain.Controls.Save.onClick ", e);
                    }
                }
                ModalMain.AddTitle(UsrCfg.Traslate.GetLangText(_this.Mythis, "MAIN"));
                ModalMain.AddBoddy(FormMain.ContentControls);
                ModalMain.Modal.ShowModal();
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Load IconsMain.Buttons.Update.onClick", e);
            }

        }
        IconsMain.Buttons.Delete.onClick = function () {
            try {
                var confirmDelete = confirm(UsrCfg.Traslate.GetLangText(_this.Mythis, "DELETEMAIN"));
                if (confirmDelete) {
                    var PSMAIN = new Persistence.PanelService.Properties.TPSMAIN();
                    PSMAIN.IDPSMAIN = parseInt(ComboMain.Combo.Value);
                    if (Data.DeleteAllMain(PSMAIN)) {
                        var Init = ComboMain.DeleteDataItem(parseInt(ComboMain.Combo.Value));
                        if (!Init) {
                            ComboMain.Combo.selectedIndex = 0;
                        }
                        else {
                            AddVisibleIconsMain();
                        }
                        var ComboDataSecundary = Data.DataComboInitSecundary(parseInt(ComboMain.Combo.Value));
                        ComboSecundary.AllDeleteDataItems();
                        ComboSecundary.AllAddData(ComboDataSecundary);
                        AddVisibleIconsSecundary()
                        if (Page.VisiblePreview) {
                            Preview();
                        }
                        alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "DELETEITEM"));
                    }
                    else {
                        alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NOTDELETE"));
                    }
                }
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Load IconsMain.Buttons.Delete.onClick", e);
            }
        }
        // End Main //
        // Secundary //
        var ComboData = Data.DataComboInitSecundary(parseInt(ComboMain.Combo.Value));
        var ComboSecundary = new _this.ComboBox(Translate);
        ComboSecundary.CrateCombo("SECONDARY");
        ComboSecundary.AllAddData(ComboData);
        Page.AddComboSecundary(ComboSecundary.ContentBoxCombo);
        var IconsSecundary = new _this.Icons(Translate);
        IconsSecundary.Buttons.Clear.This.style.display = "none";
        IconsSecundary.Buttons.Preview.This.style.display = "none";
        IconsSecundary.Buttons.Add.This.style.display = "none";
        Page.AddIconsSecundary(IconsSecundary.ContentBoxImage);
        //FUNCTIONS MAIN //
        IconsSecundary.Buttons.Add.onClick = function () {
            try {
                var positionSecundary = ComboSecundary.Combo.Value;
                var ModalSecundary = new _this.Modal(Translate);
                var FormSecundary = new _this.FormSecundary(Translate);
                FormSecundary.RelationMain(ComboMain.Combo.Text);
                FormSecundary.Controls.Save.onClick = function () {
                    try {
                        if (FormSecundary.Controls.Title.Text.trim() === "") {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "FIELDOBLIGATORY"));
                            return;
                        }
                        if (FormSecundary.Controls.TextColor.Color.trim() === "") {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "SECUNDARYTEXTCOLOR"));
                            return;
                        }
                        if (isNaN(FormSecundary.Controls.Position.Text.trim())) {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECTPOSITION"));
                            return
                        }
                        var PSSECUNDARY = new Persistence.PanelService.Properties.TPSSECUNDARY();
                        PSSECUNDARY.SECUNDARY_TITLE = FormSecundary.Controls.Title.Text;
                        PSSECUNDARY.SECUNDARY_DESCRIPTION = FormSecundary.Controls.Description.Text;
                        PSSECUNDARY.SECUNDARY_POSITION = parseInt(FormSecundary.Controls.Position.Text);
                        PSSECUNDARY.SECUNDARY_ICON = FormSecundary.Controls.Icon.Text;
                        PSSECUNDARY.SECUNDARY_TEXTCOLOR = FormSecundary.Controls.TextColor.Color.trim();
                        PSSECUNDARY.IDPSMAIN = parseInt(ComboMain.Combo.Value);
                        PSSECUNDARY.SECUNDARY_PHRASE = FormSecundary.Controls.Phrase.Text;
                        if (Data.DataAddSecundary(PSSECUNDARY)) {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "CORRECT"));
                            var initVal = ComboSecundary.AddData({ Id: PSSECUNDARY.IDPSSECUNDARY, Name: PSSECUNDARY.SECUNDARY_TITLE });
                            if (initVal) {
                                AddVisibleIconsSecundary();
                            }
                            else {
                                ComboSecundary.Combo.Value = positionSecundary;
                            }
                            FormSecundary.ClearData();
                            if (Page.VisiblePreview) {
                                Preview();
                            }

                        }
                        else {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECT"));
                        }
                    } catch (e) {
                        SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Load IconsSecundary.Buttons.Add.onClick FormSecundary.Controls.Save.onClick", e);
                    }
                }
                ModalSecundary.AddTitle(UsrCfg.Traslate.GetLangText(_this.Mythis, "SECONDARY"));
                ModalSecundary.AddBoddy(FormSecundary.ContentControls);
                ModalSecundary.Modal.ShowModal();
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Load IconsSecundary.Buttons.Add.onClick", e);
            }
        }
        IconsSecundary.Buttons.Update.onClick = function () {
            try {
                var ModalSecundary = new _this.Modal(Translate);
                var FormSecundary = new _this.FormSecundary(Translate);
                FormSecundary.RelationMain(ComboMain.Combo.Text);
                FormSecundary.AddData(Data.DataGetSecundary(parseInt(ComboSecundary.Combo.Value)));
                FormSecundary.Controls.Save.onClick = function () {
                    try {
                        if (FormSecundary.Controls.Title.Text.trim() === "") {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "FIELDOBLIGATORY"));
                            return;
                        }
                        if (FormSecundary.Controls.TextColor.Color.trim() === "") {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "SECUNDARYTEXTCOLOR"));
                            return;
                        }
                        if (isNaN(FormSecundary.Controls.Position.Text.trim())) {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECTPOSITION"));
                            return
                        }
                        var PSSECUNDARY = new Persistence.PanelService.Properties.TPSSECUNDARY();
                        PSSECUNDARY.IDPSSECUNDARY = parseInt(ComboSecundary.Combo.Value);
                        PSSECUNDARY.SECUNDARY_TITLE = FormSecundary.Controls.Title.Text;
                        PSSECUNDARY.SECUNDARY_DESCRIPTION = FormSecundary.Controls.Description.Text;
                        PSSECUNDARY.SECUNDARY_POSITION = parseInt(FormSecundary.Controls.Position.Text);
                        PSSECUNDARY.SECUNDARY_ICON = FormSecundary.Controls.Icon.Text;
                        PSSECUNDARY.SECUNDARY_TEXTCOLOR = FormSecundary.Controls.TextColor.Color.trim();
                        PSSECUNDARY.IDPSMAIN = parseInt(ComboMain.Combo.Value);
                        PSSECUNDARY.SECUNDARY_PHRASE = FormSecundary.Controls.Phrase.Text;
                        if (Data.DataUpdateSecundary(PSSECUNDARY)) {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "CORRECTUPDATE"));
                            ComboSecundary.Combo.Text = PSSECUNDARY.SECUNDARY_TITLE;
                            if (Page.VisiblePreview) {
                                Preview();
                            }
                        }
                        else {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECTUPDATE"));
                        }
                    } catch (e) {
                        SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Load IconsSecundary.Buttons.Update.onClick FormSecundary.Controls.Save.onClick", e);
                    }
                }
                ModalSecundary.AddTitle(UsrCfg.Traslate.GetLangText(_this.Mythis, "SECUNDARY"));
                ModalSecundary.AddBoddy(FormSecundary.ContentControls);
                ModalSecundary.Modal.ShowModal();
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Load IconsSecundary.Buttons.Update.onClick", e);
            }

        }
        IconsSecundary.Buttons.Delete.onClick = function () {
            try {
                var confirmDelete = confirm(UsrCfg.Traslate.GetLangText(_this.Mythis, "DELETESECUNDARY"));
                if (confirmDelete) {
                    var PSSECUNDARY = new Persistence.PanelService.Properties.TPSSECUNDARY();
                    PSSECUNDARY.IDPSSECUNDARY = parseInt(ComboSecundary.Combo.Value);
                    if (Data.DeleteSecundary(PSSECUNDARY)) {
                        var Init = ComboSecundary.DeleteDataItem(parseInt(ComboSecundary.Combo.Value));
                        if (!Init) {
                            ComboSecundary.Combo.selectedIndex = 0;
                        }
                        else {
                            AddVisibleIconsSecundary();
                        }
                        if (Page.VisiblePreview) {
                            Preview();
                        }
                        alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "DELETEITEM"));
                    }
                    else {
                        alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NOTDELETE"));
                    }
                }
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Load IconsSecundary.Buttons.Delete.onClick", e);
            }
        }
        // End Seduncary
        ComboGroup.Combo.onChange = function () {
            try {
                var ComboDataMain = Data.DataComboInitMain(parseInt(ComboGroup.Combo.Value));
                ComboMain.AllDeleteDataItems();
                ComboMain.AllAddData(ComboDataMain);
                AddVisibleIconsMain();
                var ComboDataSecundary = Data.DataComboInitSecundary(parseInt(ComboMain.Combo.Value));
                ComboSecundary.AllDeleteDataItems();
                ComboSecundary.AllAddData(ComboDataSecundary);
                AddVisibleIconsSecundary();
                if (Page.VisiblePreview) {
                    Preview();
                }
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Load ComboGroup.Combo.onChange ()", e);
            }
        }
        ComboMain.Combo.onChange = function () {
            try {
                var ComboDataSecundary = Data.DataComboInitSecundary(parseInt(ComboMain.Combo.Value));
                ComboSecundary.AllDeleteDataItems();
                ComboSecundary.AllAddData(ComboDataSecundary);
                AddVisibleIconsSecundary()
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Load ComboMain.Combo.onChange()", e);
            }
        }
        AddVisibleIconsGroup();
        AddVisibleIconsMain();
        AddVisibleIconsSecundary();
        function AddVisibleIconsSecundary() {
            try {
                if (ComboSecundary.InitElementCombo) {
                    IconsSecundary.Buttons.Update.This.style.display = "none";
                    IconsSecundary.Buttons.Delete.This.style.display = "none";
                    if (ComboMain.InitElementCombo) {
                        IconsSecundary.Buttons.Add.This.style.display = "none";
                    }
                }
                else {
                    IconsSecundary.Buttons.Update.This.style.display = "block";
                    IconsSecundary.Buttons.Delete.This.style.display = "block";
                    IconsSecundary.Buttons.Add.This.style.display = "block";

                }
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Load AddVisibleIconsSecundary()", e);
            }
        }
        function AddVisibleIconsGroup() {
            try {
                if (ComboGroup.InitElementCombo) {
                    IconsGroup.Buttons.Preview.This.style.display = "none";
                    IconsGroup.Buttons.Update.This.style.display = "none";
                    IconsGroup.Buttons.Delete.This.style.display = "none";
                }
                else {
                    IconsGroup.Buttons.Preview.This.style.display = "block";
                    IconsGroup.Buttons.Update.This.style.display = "block";
                    IconsGroup.Buttons.Delete.This.style.display = "block";
                    IconsMain.Buttons.Add.This.style.display = "block";
                }
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Load AddVisibleIconsGroup()", e);
            }
        }
        function AddVisibleIconsMain() {
            try {
                if (ComboMain.InitElementCombo) {
                    IconsMain.Buttons.Update.This.style.display = "none";
                    IconsMain.Buttons.Delete.This.style.display = "none";
                    if (ComboGroup.InitElementCombo) {
                        IconsMain.Buttons.Add.This.style.display = "none";
                    }
                }
                else {
                    IconsMain.Buttons.Update.This.style.display = "block";
                    IconsMain.Buttons.Delete.This.style.display = "block";
                    IconsMain.Buttons.Add.This.style.display = "block";
                    IconsSecundary.Buttons.Add.This.style.display = "block";

                }
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Load AddVisibleIconsMain()", e);
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Load", e);
    }
}
ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Page = function () {
    var This = this;
    this._BoxPage = null;
    this._VisiblePreview = false;
    Object.defineProperty(this, 'BoxPage', {
        get: function () {
            if (this._BoxPage === null || this._BoxPage === undefined) {
                if (this.CreatePage()) {
                    return this._BoxPage;
                }
            }
            else {
                return this._BoxPage;
            }
        }
    });
    this._Controls = { Group: { ComboGroup: null, IconsGroup: null, PreviewGroup: null }, Main: { ComboMain: null, IconsMain: null }, Secundary: { ComboSecundary: null, IconsSecundary: null } };
    Object.defineProperty(this, 'Controls', {
        get: function () {
            if (this._Controls === null || this._Controls === undefined) {
                if (this.CreatePage()) {
                    return this._Controls;
                }
            }
            else {
                return this._Controls;
            }
        }
    })
    Object.defineProperty(this, 'VisiblePreview', {
        get: function () {
            return This._VisiblePreview
        },
        set: function (_Value) {
            This._VisiblePreview = _Value;

        }
    })
    this.CreatePage = function (objHTML) {
        try {
            var BoxPage = new TVclStackPanel(objHTML, "", 1, [[12], [12], [12], [12], [12]]);
            This._BoxPage = BoxPage;
            var ContentPage = new TVclStackPanel(BoxPage.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
            ContentPage.Row.This.style.margin = "20px 0 0 0";
            var ContentGroup = new TVclStackPanel(ContentPage.Column[0].This, "", 1, [[10], [10], [10], [10], [10]]);
            ContentGroup.Column[0].This.classList.add("FormGroup");
            var ContentGroupBody = new TVclStackPanel(ContentGroup.Column[0].This, "", 2, [[9, 3], [9, 3], [9, 3], [8, 4], [8, 4]]);
            var ContentGroupCombo = new TVclStackPanel(ContentGroupBody.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
            This._Controls.Group.ComboGroup = ContentGroupCombo.Column[0].This;
            var ContentGroupIcons = new TVclStackPanel(ContentGroupBody.Column[1].This, "", 1, [[12], [12], [12], [12], [12]]);
            This._Controls.Group.IconsGroup = ContentGroupIcons.Column[0].This;
            var ContentPreview = new TVclStackPanel(ContentGroup.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
            This._Controls.Group.PreviewGroup = ContentPreview.Column[0].This;

            var ContentMain = new TVclStackPanel(ContentPage.Column[0].This, "", 1, [[10], [10], [10], [10], [10]]);
            ContentMain.Column[0].This.classList.add("FormMain");
            var ContentMainBody = new TVclStackPanel(ContentMain.Column[0].This, "", 2, [[9, 3], [9, 3], [9, 3], [8, 3], [8, 4]]);
            var ContentMainCombo = new TVclStackPanel(ContentMainBody.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
            This._Controls.Main.ComboMain = ContentMainCombo.Column[0].This;
            var ContentMainIcons = new TVclStackPanel(ContentMainBody.Column[1].This, "", 1, [[12], [12], [12], [12], [12]]);
            This._Controls.Main.IconsMain = ContentMainIcons.Column[0].This;

            var ContentSecundary = new TVclStackPanel(ContentPage.Column[0].This, "", 1, [[10], [10], [10], [10], [10]]);
            ContentSecundary.Column[0].This.classList.add("FormSecundary");
            var ContentSecundaryBody = new TVclStackPanel(ContentSecundary.Column[0].This, "", 2, [[9, 3], [9, 3], [9, 3], [8, 4], [8, 4]]);
            var ContentSecundaryCombo = new TVclStackPanel(ContentSecundaryBody.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
            This._Controls.Secundary.ComboSecundary = ContentSecundaryCombo.Column[0].This;
            var ContentSecundaryIcons = new TVclStackPanel(ContentSecundaryBody.Column[1].This, "", 1, [[12], [12], [12], [12], [12]]);
            This._Controls.Secundary.IconsSecundary = ContentSecundaryIcons.Column[0].This;
            //ISMOVIL
            if (Source.Menu.IsMobil) {
                $(ContentGroup.Column[0].This).removeClass('col-xs-10 col-sm-10 col-md-10 col-lg-10 col-xl-10');
                $(ContentGroup.Column[0].This).addClass('col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12');
                ContentGroup.Column[0].This.style.cssText += " margin-left:0px !important; margin-right:0px !important; padding-left:0px !important; padding-right:0px !important";
                $(ContentMain.Column[0].This).removeClass('col-xs-10 col-sm-10 col-md-10 col-lg-10 col-xl-10');
                $(ContentMain.Column[0].This).addClass('col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12');
                ContentMain.Column[0].This.style.cssText += " margin-left:0px !important; margin-right:0px !important";
                $(ContentSecundary.Column[0].This).removeClass('col-xs-10 col-sm-10 col-md-10 col-lg-10 col-xl-10');
                $(ContentSecundary.Column[0].This).addClass('col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12');
                ContentSecundary.Column[0].This.style.cssText += " margin-left:0px !important; margin-right:0px !important";
            }
            return true;
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Page CreatePage()", e);
        }
    };
    this.AddComboGroup = function (ComboGroup) {
        try {
            if (This._Controls.Group.ComboGroup !== null || This._Controls.Group.ComboGroup !== undefined) {
                This._Controls.Group.ComboGroup.appendChild(ComboGroup);
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Page AddComboGroup()", e);
        }
    }
    this.AddIconsGroup = function (IconsGroup) {
        try {
            if (This._Controls.Group.IconsGroup !== null || This._Controls.Group.IconsGroup !== undefined) {
                This._Controls.Group.IconsGroup.appendChild(IconsGroup);
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Page AddIconsGroup()", e);
        }
    }
    this.AddPreviewGroup = function (PreviewGroup) {
        try {
            if (PreviewGroup !== null || PreviewGroup !== undefined) {
                var Preview = null;
                Preview = This._Controls.Group.PreviewGroup.appendChild(PreviewGroup);
                var listElement = document.getElementsByClassName("firstMapping");
                for (var i = 0; i < listElement.length; i++) {
                    listElement[i].style.display = "none";
                }
                if (Preview !== null || Preview !== undefine) {
                    This._VisiblePreview = true;
                    return true;
                }
                else {
                    This._VisiblePreview = false;
                    return false;
                }
            }
            else {
                This._VisiblePreview = false;
                return false;
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Page AddPreviewGroup()", e);
        }
    };
    this.AddComboMain = function (ComboMain) {
        try {
            if (This._Controls.Main.ComboMain !== null || This._Controls.Main.ComboMain !== undefined) {
                This._Controls.Main.ComboMain.appendChild(ComboMain);
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Page AddComboMain()", e);
        }
    }
    this.AddIconsMain = function (IconsMain) {
        try {
            if (This._Controls.Main.IconsMain !== null || This._Controls.Main.IconsMain !== undefined) {
                This._Controls.Main.IconsMain.appendChild(IconsMain);
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Page AddIconsMain()", e);
        }
    }
    this.AddComboSecundary = function (ComboSecundary) {
        try {
            if (This._Controls.Secundary.ComboSecundary !== null || This._Controls.Secundary.ComboSecundary !== undefined) {
                This._Controls.Secundary.ComboSecundary.appendChild(ComboSecundary);
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Page AddComboSecundary()", e);
        }
    }
    this.AddIconsSecundary = function (IconsSecundary) {
        try {
            if (This._Controls.Secundary.IconsSecundary !== null || This._Controls.Secundary.IconsSecundary !== undefined) {
                This._Controls.Secundary.IconsSecundary.appendChild(IconsSecundary);
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Page AddIconsSecundary()", e);
        }
    }
    this.RemovePage = function () {
        try {
            if (This._BoxPage !== null || This._BoxPage !== undefined) {
                This._BoxPage.remove();
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Page RemovePage()", e);
        }
    };
    this.RemovePreviewGroup = function () {
        try {
            if (This._Controls.Group.PreviewGroup !== null || This._Controls.Group.PreviewGroup !== undefined) {
                This._Controls.Group.PreviewGroup.innerHTML = '';
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Page RemovePreviewGroup()", e);
        }
    };
    (function () {
        This.CreatePage();
    })();
}
ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.FormGroup = function (Translate) {
    var This = this;
    this._Mythis = "PanelServicesMapping";
    this._Translate = Translate;
    this._ContentControls = null;
    this._Controls = { Title: null, Description: null, Background: null, Border: null, Path: null, Save: null };
    Object.defineProperty(this, 'ContentControls', {
        get: function () {
            if (This._ContentControls === null || This._ContentControls === undefined) {
                if (This.CreateGroup()) {
                    return This._ContentControls;
                };
            }
            else {
                return This._ContentControls;
            }
        },
        set: function (_Value) {
            this.This._ContentControls = _Value;
        }
    });
    Object.defineProperty(this, 'Controls', {
        get: function () {
            if (This._Controls === null || This._Controls === undefined) {
                if (This.CreateGroup()) {
                    return This._Controls;
                };
            }
            else {
                return This._Controls;
            }
        },
    });
    this.AddData = function (objGroup) {
        try {
            if (This._Controls.Title !== null) {
                This._Controls.Title.Text = objGroup.GROUP_NAME;
            }
            if (This._Controls.Description !== null) {
                This._Controls.Description.Text = objGroup.GROUP_DESCRIPTION;
            }
            if (This._Controls.Background !== null) {
                This._Controls.Background.Color = objGroup.GROUP_BACKGROUND;
            }
            if (This._Controls.Border !== null) {
                This._Controls.Border.Color = objGroup.GROUP_BORDER;
            }
            if (This._Controls.Path !== null) {
                This._Controls.Path.Text = objGroup.GROUP_PATH;
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.FormGroup AddData()", e);
        }
    }
    this.ClearData = function () {
        try {
            if (This._Controls.Title !== null) {
                This._Controls.Title.Text = "";
            }
            if (This._Controls.Description !== null) {
                This._Controls.Description.Text = "";
            }
            if (This._Controls.Background !== null) {
                This._Controls.Background.Color = "white";
            }
            if (This._Controls.Border !== null) {
                This._Controls.Border.Color = "white";
            }
            if (This._Controls.Path !== null) {
                This._Controls.Path.Text = "";
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.FormGroup ClearData()", e);
        }
    }
    this.CreateGroup = function (objGroup, action) {
        try {
            // CONTENT //
            var ContentInitBox1 = new TVclStackPanel("", "", 1, [[12], [12], [12], [12], [12]]);
            This._ContentControls = ContentInitBox1.Row.This;
            var ContentBox1 = new TVclStackPanel(ContentInitBox1.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
            // END CONTENT //
            // TITLE //
            var Box1_Formas1_Title = new TVclStackPanel(ContentBox1.Column[0].This, "", 2, [[12, 12], [3, 9], [3, 9], [3, 9], [3, 9]]);
            Box1_Formas1_Title.Row.This.classList.add("DownSpace");
            var Box1_Formas1_lbl_Title = new TVcllabel(Box1_Formas1_Title.Column[0].This, "", TlabelType.H0);
            Box1_Formas1_lbl_Title.Text = UsrCfg.Traslate.GetLangText(This._Mythis, "TITLE");
            Box1_Formas1_lbl_Title.VCLType = TVCLType.BS;
            var Box1_Formas1_txt_Title = new TVclTextBox(Box1_Formas1_Title.Column[1].This, "");
            Box1_Formas1_txt_Title.This.disabled = false;
            Box1_Formas1_txt_Title.VCLType = TVCLType.BS;
            Box1_Formas1_txt_Title.Text = (action === "new") ? "" : objGroup.GROUP_NAME;
            This._Controls.Title = Box1_Formas1_txt_Title;
            // END TITLE //
            // BACKGROUND //
            var Box1_Formas1_Background = new TVclStackPanel(ContentBox1.Column[0].This, "", 2, [[12, 12], [3, 9], [3, 9], [3, 9], [3, 9]]);
            Box1_Formas1_Background.Row.This.classList.add("DownSpace");
            var Box1_Formas1_lbl_Background = new TVcllabel(Box1_Formas1_Background.Column[0].This, "", TlabelType.H0);
            Box1_Formas1_lbl_Background.Text = UsrCfg.Traslate.GetLangText(This._Mythis, "BACKGROUNDCOLOR");
            Box1_Formas1_lbl_Background.VCLType = TVCLType.BS;
            This._Controls.Background = new Componet.TPalleteColor(Box1_Formas1_Background.Column[1].This, "GroupColor00");
            This._Controls.Background.CreatePallete();
            (action === "new") ? This._Controls.Background.Color = "white" : This._Controls.Background.Color = objGroup.GROUP_BACKGROUND;
            // END BACKGROUND //
            // BORDER //
            var Box1_Formas1_Border = new TVclStackPanel(ContentBox1.Column[0].This, "", 2, [[12, 12], [3, 9], [3, 9], [3, 9], [3, 9]]);
            Box1_Formas1_Border.Row.This.classList.add("DownSpace");
            var Box1_Formas1_lbl_Border = new TVcllabel(Box1_Formas1_Border.Column[0].This, "", TlabelType.H0);
            Box1_Formas1_lbl_Border.Text = UsrCfg.Traslate.GetLangText(This._Mythis, "BORDERCOLOR");
            Box1_Formas1_lbl_Border.VCLType = TVCLType.BS;
            This._Controls.Border = new Componet.TPalleteColor(Box1_Formas1_Border.Column[1].This, "GroupColor01");
            This._Controls.Border.CreatePallete();
            (action === "new") ? This._Controls.Border.Color = "white" : This._Controls.Border.Color = objGroup.GROUP_BORDER;
            // END BORDER //
            // DESCRIPTION //
            var Box1_Formas1_Description = new TVclStackPanel(ContentBox1.Column[0].This, "", 2, [[12, 12], [3, 9], [3, 9], [3, 9], [3, 9]]);
            Box1_Formas1_Description.Row.This.classList.add("DownSpace");
            var Box1_Formas1_lbl_Description = new TVcllabel(Box1_Formas1_Description.Column[0].This, "", TlabelType.H0);
            Box1_Formas1_lbl_Description.Text = UsrCfg.Traslate.GetLangText(This._Mythis, "DESCRIPTION");
            Box1_Formas1_lbl_Description.VCLType = TVCLType.BS;
            var Box1_Formas1_txt_Description = new TVclMemo(Box1_Formas1_Description.Column[1].This, "");
            Box1_Formas1_txt_Description.VCLType = TVCLType.BS;
            Box1_Formas1_txt_Description.This.removeAttribute("cols");
            Box1_Formas1_txt_Description.This.removeAttribute("rows");
            Box1_Formas1_txt_Description.This.classList.add("form-control");
            Box1_Formas1_txt_Description.This.disabled = false;
            Box1_Formas1_txt_Description.Text = (action === "new") ? "" : objGroup.GROUP_DESCRIPTION;
            This._Controls.Description = Box1_Formas1_txt_Description;
            // END DESCRIPTION //
            // PATH //
            var Box1_Formas1_Path = new TVclStackPanel(ContentBox1.Column[0].This, "", 2, [[12, 12], [3, 9], [3, 9], [3, 9], [3, 9]]);
            Box1_Formas1_Path.Row.This.classList.add("DownSpace");
            var Box1_Formas1_lbl_Path = new TVcllabel(Box1_Formas1_Path.Column[0].This, "", TlabelType.H0);
            Box1_Formas1_lbl_Path.Text = UsrCfg.Traslate.GetLangText(This._Mythis, "PATH");
            Box1_Formas1_lbl_Path.VCLType = TVCLType.BS;
            var Box1_Formas1_txt_Path = new TVclTextBox(Box1_Formas1_Path.Column[1].This, "");
            Box1_Formas1_txt_Path.This.disabled = false;
            Box1_Formas1_txt_Path.VCLType = TVCLType.BS;
            Box1_Formas1_txt_Path.Text = (action === "new") ? "" : objGroup.GROUP_PATH;
            This._Controls.Path = Box1_Formas1_txt_Path;
            // END PATH //
            // BUTTONS //
            var Box1_Formas1_Btn = new TVclStackPanel(ContentBox1.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
            Box1_Formas1_Btn.Row.This.classList.add("DownSpace");
            var ButtonAdd = new TVclInputbutton(Box1_Formas1_Btn.Column[0].This, "");
            ButtonAdd.Text = UsrCfg.Traslate.GetLangText(This._Mythis, "SAVE");
            ButtonAdd.This.style.float = "right";
            ButtonAdd.VCLType = TVCLType.BS;
            This._Controls.Save = ButtonAdd;
            //END BUTTONS//
            return true;
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.FormGroup CreateGroup()", e);
        }
    };
    (function () {
        This.CreateGroup({ GROUP_NAME: "", GROUP_DESCRIPTION: "", GROUP_BACKGROUND: "", GROUP_BORDER: "" }, "new");
    })();
}
ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.FormMain = function (Translate) {
    var This = this;
    this._Mythis = "PanelServicesMapping";
    this._Translate = Translate;
    this._ContentControls = null;
    this._Controls = { Title: null, Description: null, Position: null, Icon: null, Group: null, TitleColor: null, BackgroundActive: null, TitleActiveColor: null, BorderActive: null, BackgroundHover: null, TitleHoverColor: null, Save: null };
    Object.defineProperty(this, 'ContentControls', {
        get: function () {
            if (This._ContentControls === null || This._ContentControls === undefined) {
                if (This.CreateMain()) {
                    return This._ContentControls;
                };
            }
            else {
                return This._ContentControls;
            }
        }
    });
    Object.defineProperty(this, 'Controls', {
        get: function () {
            if (This._Controls === null || This._Controls === undefined) {
                if (this.CreateMain()) {
                    return This._Controls;
                };
            }
            else {
                return This._Controls;
            }
        }
    });
    this.RelationGroup = function (Text) {
        try {
            if (This._Controls !== null || This._Controls !== undefined) {
                This._Controls.Group.Text = Text;
                This._Controls.Group.This.disabled = true;
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.FormMain RelationGroup()", e);
        }
    }
    this.AddData = function (objMain) {
        try {
            if (This._Controls.Title !== null) {
                This._Controls.Title.Text = objMain.MAIN_TITLE;
            }
            if (This._Controls.Description !== null) {
                This._Controls.Description.Text = objMain.MAIN_DESCRIPTION;
            }
            if (This._Controls.Position !== null) {
                This._Controls.Position.Text = objMain.MAIN_POSITION;
            }
            if (This._Controls.Icon !== null) {
                This._Controls.Icon.Text = objMain.MAIN_ICON;
            }
            if (This._Controls.TitleColor !== null) {
                This._Controls.TitleColor.Color = objMain.MAIN_TITLECOLOR;
            }
            if (This._Controls.BackgroundActive !== null) {
                This._Controls.BackgroundActive.Color = objMain.MAIN_BACKGROUNDACTIVE;
            }
            if (This._Controls.TitleActiveColor !== null) {
                This._Controls.TitleActiveColor.Color = objMain.MAIN_TITLEACTIVECOLOR;
            }
            if (This._Controls.BorderActive !== null) {
                This._Controls.BorderActive.Color = objMain.MAIN_BORDERACTIVE;
            }
            if (This._Controls.BackgroundHover !== null) {
                This._Controls.BackgroundHover.Color = objMain.MAIN_BACKGROUNDHOVER;
            }
            if (This._Controls.TitleHoverColor !== null) {
                This._Controls.TitleHoverColor.Color = objMain.MAIN_TITLEHOVERCOLOR;
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.FormMain AddData()", e);
        }
    }
    this.ClearData = function () {
        try {
            if (This._Controls.Title !== null) {
                This._Controls.Title.Text = "";
            }
            if (This._Controls.Description !== null) {
                This._Controls.Description.Text = "";
            }
            if (This._Controls.Position !== null) {
                This._Controls.Position.Text = "";
            }
            if (This._Controls.Icon !== null) {
                This._Controls.Icon.Text = "";
            }
            if (This._Controls.TitleColor !== null) {
                This._Controls.TitleColor.Color = "white";
            }
            if (This._Controls.BackgroundActive !== null) {
                This._Controls.BackgroundActive.Color = "white";
            }
            if (This._Controls.TitleActiveColor !== null) {
                This._Controls.TitleActiveColor.Color = "white";
            }
            if (This._Controls.BorderActive !== null) {
                This._Controls.BorderActive.Color = "white";
            }
            if (This._Controls.BackgroundHover !== null) {
                This._Controls.BackgroundHover.Color = "white";
            }
            if (This._Controls.TitleHoverColor !== null) {
                This._Controls.TitleHoverColor.Color = "white";
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.FormMain ClearData()", e);
        }
    }
    this.CreateMain = function (objMain, action) {
        try {
            // CONTENT //
            var ContentInitBox2 = new TVclStackPanel("", "", 1, [[12], [12], [12], [12], [12]]);
            This._ContentControls = ContentInitBox2.Row.This;
            var ContentBox2 = new TVclStackPanel(ContentInitBox2.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
            // END CONTENT //
            // TITLE //
            var Box1_Formas2_Title = new TVclStackPanel(ContentBox2.Column[0].This, "", 2, [[12, 12], [3, 9], [3, 9], [3, 9], [3, 9]]);
            Box1_Formas2_Title.Row.This.classList.add("DownSpaceMain");
            var Box1_Formas2_lbl_Title = new TVcllabel(Box1_Formas2_Title.Column[0].This, "", TlabelType.H0);
            Box1_Formas2_lbl_Title.Text = UsrCfg.Traslate.GetLangText(This._Mythis, "TITLE");
            Box1_Formas2_lbl_Title.VCLType = TVCLType.BS;
            var Box1_Formas2_txt_Title = new TVclTextBox(Box1_Formas2_Title.Column[1].This, "");
            Box1_Formas2_txt_Title.VCLType = TVCLType.BS;
            Box1_Formas2_txt_Title.Text = (action === "new") ? "" : objMain.MAIN_TITLE;
            This._Controls.Title = Box1_Formas2_txt_Title;
            // END TITLE //
            // TITLECOLOR //
            var Box1_Formas2_TitleColor = new TVclStackPanel(ContentBox2.Column[0].This, "", 2, [[12, 12], [3, 9], [3, 9], [3, 9], [3, 9]]);
            Box1_Formas2_TitleColor.Row.This.classList.add("DownSpaceMain");
            var Box1_Formas2_lbl_TitleColor = new TVcllabel(Box1_Formas2_TitleColor.Column[0].This, "", TlabelType.H0);
            Box1_Formas2_lbl_TitleColor.Text = UsrCfg.Traslate.GetLangText(This._Mythis, "TITLECOLOR");
            Box1_Formas2_lbl_TitleColor.VCLType = TVCLType.BS;
            This._Controls.TitleColor = new Componet.TPalleteColor(Box1_Formas2_TitleColor.Column[1].This, "MainColor00");
            This._Controls.TitleColor.CreatePallete();
            (action === "new") ? This._Controls.TitleColor.Color = "white" : This._Controls.TitleColor.Color = objMain.MAIN_TITLECOLOR;
            // END TITLECOLOR //
            // BACKGROUNDACTIVE//
            var Box1_Formas2_BackgroundActive = new TVclStackPanel(ContentBox2.Column[0].This, "", 2, [[12, 12], [3, 9], [3, 9], [3, 9], [3, 9]]);
            Box1_Formas2_BackgroundActive.Row.This.classList.add("DownSpaceMain");
            var Box1_Formas2_lbl_BackgroundActive = new TVcllabel(Box1_Formas2_BackgroundActive.Column[0].This, "", TlabelType.H0);
            Box1_Formas2_lbl_BackgroundActive.Text = UsrCfg.Traslate.GetLangText(This._Mythis, "BACKGROUNDACTIVECOLOR");
            Box1_Formas2_lbl_BackgroundActive.VCLType = TVCLType.BS;
            This._Controls.BackgroundActive = new Componet.TPalleteColor(Box1_Formas2_BackgroundActive.Column[1].This, "MainColor01");
            This._Controls.BackgroundActive.Transparency = false;
            This._Controls.BackgroundActive.CreatePallete();
            (action === "new") ? This._Controls.BackgroundActive.Color = "white" : This._Controls.BackgroundActive.Color = objMain.MAIN_BACKGROUNDACTIVE;
            // END BACKGROUNDACTIVE //
            // TITLEACTIVECOLOR//
            var Box1_Formas2_TitleActiveColor = new TVclStackPanel(ContentBox2.Column[0].This, "", 2, [[12, 12], [3, 9], [3, 9], [3, 9], [3, 9]]);
            Box1_Formas2_TitleActiveColor.Row.This.classList.add("DownSpaceMain");
            var Box1_Formas2_lbl_TitleActiveColor = new TVcllabel(Box1_Formas2_TitleActiveColor.Column[0].This, "", TlabelType.H0);
            Box1_Formas2_lbl_TitleActiveColor.Text = UsrCfg.Traslate.GetLangText(This._Mythis, "TITLEACTIVECOLOR");
            Box1_Formas2_lbl_TitleActiveColor.VCLType = TVCLType.BS;
            This._Controls.TitleActiveColor = new Componet.TPalleteColor(Box1_Formas2_TitleActiveColor.Column[1].This, "MainColor02");
            This._Controls.TitleActiveColor.CreatePallete();
            (action === "new") ? This._Controls.TitleActiveColor.Color = "white" : This._Controls.TitleActiveColor.Color = objMain.MAIN_TITLEACTIVECOLOR;
            // END TITLEACTIVECOLOR //
            // BORDERACTIVE//
            var Box1_Formas2_BorderActive = new TVclStackPanel(ContentBox2.Column[0].This, "", 2, [[12, 12], [3, 9], [3, 9], [3, 9], [3, 9]]);
            Box1_Formas2_BorderActive.Row.This.classList.add("DownSpaceMain");
            var Box1_Formas2_lbl_BorderActive = new TVcllabel(Box1_Formas2_BorderActive.Column[0].This, "", TlabelType.H0);
            Box1_Formas2_lbl_BorderActive.Text = UsrCfg.Traslate.GetLangText(This._Mythis, "BORDERCOLORACTIVE");
            Box1_Formas2_lbl_BorderActive.VCLType = TVCLType.BS;
            This._Controls.BorderActive = new Componet.TPalleteColor(Box1_Formas2_BorderActive.Column[1].This, "MainColor03");
            This._Controls.BorderActive.CreatePallete();
            (action === "new") ? This._Controls.BorderActive.Color = "white" : This._Controls.BorderActive.Color = objMain.MAIN_BORDERACTIVE;
            // END BORDERACTIVE //
            // BACKGROUNDHOVER//
            var Box1_Formas2_BackgroundHover = new TVclStackPanel(ContentBox2.Column[0].This, "", 2, [[12, 12], [3, 9], [3, 9], [3, 9], [3, 9]]);
            Box1_Formas2_BackgroundHover.Row.This.classList.add("DownSpaceMain");
            var Box1_Formas2_lbl_BackgroundHover = new TVcllabel(Box1_Formas2_BackgroundHover.Column[0].This, "", TlabelType.H0);
            Box1_Formas2_lbl_BackgroundHover.Text = UsrCfg.Traslate.GetLangText(This._Mythis, "BACKGROUNDHOVERCOLOR01");
            Box1_Formas2_lbl_BackgroundHover.VCLType = TVCLType.BS;
            This._Controls.BackgroundHover = new Componet.TPalleteColor(Box1_Formas2_BackgroundHover.Column[1].This, "MainColor04");
            This._Controls.BackgroundHover.CreatePallete();
            (action === "new") ? This._Controls.BackgroundHover.Color = "white" : This._Controls.BackgroundHover.Color = objMain.MAIN_BACKGROUNDHOVER;
            // END BACKGROUNDHOVER //
            // TITLEHOVERCOLOR//
            var Box1_Formas2_TitleHoverColor = new TVclStackPanel(ContentBox2.Column[0].This, "", 2, [[12, 12], [3, 9], [3, 9], [3, 9], [3, 9]]);
            Box1_Formas2_TitleHoverColor.Row.This.classList.add("DownSpaceMain");
            var Box1_Formas2_lbl_TitleHoverColor = new TVcllabel(Box1_Formas2_TitleHoverColor.Column[0].This, "", TlabelType.H0);
            Box1_Formas2_lbl_TitleHoverColor.Text = UsrCfg.Traslate.GetLangText(This._Mythis, "TITLEHOVERCOLOR");
            Box1_Formas2_lbl_TitleHoverColor.VCLType = TVCLType.BS;
            This._Controls.TitleHoverColor = new Componet.TPalleteColor(Box1_Formas2_TitleHoverColor.Column[1].This, "MainColor05");
            This._Controls.TitleHoverColor.CreatePallete();
            (action === "new") ? This._Controls.TitleHoverColor.Color = "white" : This._Controls.TitleHoverColor.Color = objMain.MAIN_TITLEHOVERCOLOR;
            // END TITLEHOVERCOLOR //
            // DESCRIPTION //
            var Box1_Formas2_Description = new TVclStackPanel(ContentBox2.Column[0].This, "", 2, [[12, 12], [3, 9], [3, 9], [3, 9], [3, 9]]);
            Box1_Formas2_Description.Row.This.classList.add("DownSpaceMain");
            var Box1_Formas2_lbl_Description = new TVcllabel(Box1_Formas2_Description.Column[0].This, "", TlabelType.H0);
            Box1_Formas2_lbl_Description.Text = UsrCfg.Traslate.GetLangText(This._Mythis, "DESCRIPTION");
            Box1_Formas2_lbl_Description.VCLType = TVCLType.BS;
            var Box1_Formas2_txt_Description = new TVclMemo(Box1_Formas2_Description.Column[1].This, "");
            Box1_Formas2_txt_Description.VCLType = TVCLType.BS;
            Box1_Formas2_txt_Description.This.removeAttribute("cols");
            Box1_Formas2_txt_Description.This.removeAttribute("rows");
            Box1_Formas2_txt_Description.This.classList.add("form-control");
            Box1_Formas2_txt_Description.Text = (action === "new") ? "" : objMain.MAIN_DESCRIPTION;
            This._Controls.Description = Box1_Formas2_txt_Description;
            // END DESCRIPTION //
            // POSITION //
            var Box1_Formas2_Position = new TVclStackPanel(ContentBox2.Column[0].This, "", 2, [[12, 12], [3, 9], [3, 9], [3, 9], [3, 9]]);
            Box1_Formas2_Position.Row.This.classList.add("DownSpaceMain");
            var Box1_Formas2_lbl_Position = new TVcllabel(Box1_Formas2_Position.Column[0].This, "", TlabelType.H0);
            Box1_Formas2_lbl_Position.Text = UsrCfg.Traslate.GetLangText(This._Mythis, "POSITION");
            Box1_Formas2_lbl_Position.VCLType = TVCLType.BS;
            var Box1_Formas2_txt_Position = new TVclTextBox(Box1_Formas2_Position.Column[1].This, "");
            Box1_Formas2_txt_Position.VCLType = TVCLType.BS;
            Box1_Formas2_txt_Position.This.removeAttribute("type")
            Box1_Formas2_txt_Position.This.setAttribute("type", "number");
            Box1_Formas2_txt_Position.This.setAttribute("min", "0");
            Box1_Formas2_txt_Position.Text = (action === "new") ? "0" : objMain.MAIN_POSITION;
            This._Controls.Position = Box1_Formas2_txt_Position;
            // END POSITION //
            // ICON //
            var Box1_Formas2_Icon = new TVclStackPanel(ContentBox2.Column[0].This, "", 2, [[12, 12], [3, 9], [3, 9], [3, 9], [3, 9]]);
            Box1_Formas2_Icon.Row.This.classList.add("DownSpaceMain");
            var Box1_Formas2_lbl_Icon = new TVcllabel(Box1_Formas2_Icon.Column[0].This, "", TlabelType.H0);
            Box1_Formas2_lbl_Icon.Text = UsrCfg.Traslate.GetLangText(This._Mythis, "ICON");
            Box1_Formas2_lbl_Icon.VCLType = TVCLType.BS;
            var Box1_Formas2_txt_Icon = new TVclTextBox(Box1_Formas2_Icon.Column[1].This, "");
            Box1_Formas2_txt_Icon.VCLType = TVCLType.BS;
            Box1_Formas2_txt_Icon.Text = (action === "new") ? "" : objMain.MAIN_ICON;
            This._Controls.Icon = Box1_Formas2_txt_Icon;
            //END ICON //
            // PARENT //
            var Box1_Formas2_Padre = new TVclStackPanel(ContentBox2.Column[0].This, "", 2, [[12, 12], [3, 9], [3, 9], [3, 9], [3, 9]]);
            Box1_Formas2_Padre.Row.This.classList.add("DownSpaceMain");
            var Box1_Formas2_lbl_Padre = new TVcllabel(Box1_Formas2_Padre.Column[0].This, "", TlabelType.H0);
            Box1_Formas2_lbl_Padre.Text = UsrCfg.Traslate.GetLangText(This._Mythis, "GROUP");
            Box1_Formas2_lbl_Padre.VCLType = TVCLType.BS;
            var Box1_Formas2_txt_Padre = new TVclTextBox(Box1_Formas2_Padre.Column[1].This, "");
            Box1_Formas2_txt_Padre.VCLType = TVCLType.BS;
            Box1_Formas2_txt_Padre.Text = (action === "new") ? "" : objMain.MAIN_GROUP;
            This._Controls.Group = Box1_Formas2_txt_Padre;
            // END PARENT //
            // BUTTONS //
            var Box1_formas2_Btn = new TVclStackPanel(ContentBox2.Column[0].This, "", 3, [[12, 12, 12], [12, 12, 12], [12, 12, 12], [12, 12, 12], [12, 12, 12]]);
            Box1_formas2_Btn.Row.This.classList.add("DownSpaceMain");
            var ButtonAdd = new TVclInputbutton(Box1_formas2_Btn.Column[0].This, "");
            ButtonAdd.Text = UsrCfg.Traslate.GetLangText(This._Mythis, "SAVE");
            ButtonAdd.VCLType = TVCLType.BS;
            ButtonAdd.This.style.float = "right";
            This._Controls.Save = ButtonAdd;
            // END BUTTONS //
            return true;
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.FormMain CreateMain()", e);
        }
    };
    (function () {
        This.CreateMain(
            {
                MAIN_TITLE: "",
                MAIN_DESCRIPTION: "",
                MAIN_POSITION: "",
                MAIN_ICON: "",
                MAIN_GROUP: "",
                MAIN_TITLECOLOR: "",
                MAIN_BACKGROUNDACTIVE: "",
                MAIN_TITLEACTIVECOLOR: "",
                MAIN_BORDERACTIVE: "",
                MAIN_BACKGROUNDHOVER: "",
                MAIN_TITLEHOVERCOLOR: ""
            }, "new");
    })();

}
ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.FormSecundary = function (Translate) {
    var This = this;
    this._Mythis = "PanelServicesMapping";
    this._Translate = Translate;
    this._ContentControls = null;
    Object.defineProperty(this, 'Controls', {
        get: function () {
            if (This._Controls === null || This._Controls === undefined) {
                if (This.Secundary()) {
                    return This._Controls;
                };
            }
            else {
                return This._Controls;
            }
        }
    });
    Object.defineProperty(this, 'ContentControls', {
        get: function () {
            if (This._ContentControls === null || This._ContentControls === undefined) {
                if (This.CreateSecundary()) {
                    return This._ContentControls;
                };
            }
            else {
                return This._ContentControls;
            }
        }
    });
    this._Controls = { Title: null, Description: null, Position: null, Icon: null, Main: null, Phrase: null, TextColor: null, Save: null };
    this.RelationMain = function (Text) {
        try {
            if (This._Controls !== null || This._Controls !== undefined) {
                This._Controls.Main.Text = Text;
                This._Controls.Main.This.disabled = true;
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.FormSecundary RelationMain()", e);
        }
    }
    this.AddData = function (objSecundary) {
        try {
            if (This._Controls.Title !== null) {
                This._Controls.Title.Text = objSecundary.SECUNDARY_TITLE;
            }
            if (This._Controls.Description !== null) {
                This._Controls.Description.Text = objSecundary.SECUNDARY_DESCRIPTION;
            }
            if (This._Controls.Position !== null) {
                This._Controls.Position.Text = objSecundary.SECUNDARY_POSITION;
            }
            if (This._Controls.Icon !== null) {
                This._Controls.Icon.Text = objSecundary.SECUNDARY_ICON;
            }
            if (This._Controls.Phrase !== null) {
                This._Controls.Phrase.Text = objSecundary.SECUNDARY_PHRASE;
            }
            if (This._Controls.TextColor !== null) {
                This._Controls.TextColor.Color = objSecundary.SECUNDARY_TEXTCOLOR;
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.FormSecundary AddData()", e);
        }
    }
    this.ClearData = function () {
        try {
            if (This._Controls.Title !== null) {
                This._Controls.Title.Text = "";
            }
            if (This._Controls.Description !== null) {
                This._Controls.Description.Text = "";
            }
            if (This._Controls.Position !== null) {
                This._Controls.Position.Text = "";
            }
            if (This._Controls.Icon !== null) {
                This._Controls.Icon.Text = "";
            }
            if (This._Controls.Phrase !== null) {
                This._Controls.Phrase.Text = "";
            }
            if (This._Controls.TextColor !== null) {
                This._Controls.TextColor.Color = "white";
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.FormSecundary ClearData()", e);
        }
    }
    this.CreateSecundary = function (objSecundary, action) {
        try {
            // CONTENT //
            var ContentInitBox3 = new TVclStackPanel("", "", 1, [[12], [12], [12], [12], [12]]);
            This._ContentControls = ContentInitBox3.Row.This;
            var ContentBox3 = new TVclStackPanel(ContentInitBox3.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
            // END CONTENT //
            // TITLE //
            var Box1_Formas3_Title = new TVclStackPanel(ContentBox3.Column[0].This, "", 2, [[12, 12], [3, 9], [3, 9], [3, 9], [3, 9]]);
            Box1_Formas3_Title.Row.This.classList.add("DownSpaceSecundary");
            var Box1_Formas3_lbl_Title = new TVcllabel(Box1_Formas3_Title.Column[0].This, "", TlabelType.H0);
            Box1_Formas3_lbl_Title.Text = UsrCfg.Traslate.GetLangText(This._Mythis, "TITLE");
            Box1_Formas3_lbl_Title.VCLType = TVCLType.BS;
            var Box1_Formas3_txt_Title = new TVclTextBox(Box1_Formas3_Title.Column[1].This, "");
            Box1_Formas3_txt_Title.VCLType = TVCLType.BS;
            Box1_Formas3_txt_Title.Text = (action === "new") ? "" : objSecundary.SECUNDARY_TITLE;
            This._Controls.Title = Box1_Formas3_txt_Title;
            // END TITLE //
            // DESCRIPTION //
            var Box1_Formas3_Description = new TVclStackPanel(ContentBox3.Column[0].This, "", 2, [[12, 12], [3, 9], [3, 9], [3, 9], [3, 9]]);
            Box1_Formas3_Description.Row.This.classList.add("DownSpaceSecundary");
            var Box1_Formas3_lbl_Description = new TVcllabel(Box1_Formas3_Description.Column[0].This, "", TlabelType.H0);
            Box1_Formas3_lbl_Description.Text = UsrCfg.Traslate.GetLangText(This._Mythis, "DESCRIPTION");
            Box1_Formas3_lbl_Description.VCLType = TVCLType.BS;
            var Box1_Formas3_txt_Description = new TVclTextBox(Box1_Formas3_Description.Column[1].This, "");
            Box1_Formas3_txt_Description.VCLType = TVCLType.BS;
            Box1_Formas3_txt_Description.Text = (action === "new") ? "" : objSecundary.SECUNDARY_DESCRIPTION;
            This._Controls.Description = Box1_Formas3_txt_Description;
            // END DESCRIPTION //
            // TEXTCOLOR//
            var Box1_Formas3_TextColor = new TVclStackPanel(ContentBox3.Column[0].This, "", 2, [[12, 12], [3, 9], [3, 9], [3, 9], [3, 9]]);
            Box1_Formas3_TextColor.Row.This.classList.add("DownSpaceSecundary");
            var Box1_Formas3_lbl_TextColor = new TVcllabel(Box1_Formas3_TextColor.Column[0].This, "", TlabelType.H0);
            Box1_Formas3_lbl_TextColor.Text = UsrCfg.Traslate.GetLangText(This._Mythis, "TEXTCOLOR");
            Box1_Formas3_lbl_TextColor.VCLType = TVCLType.BS;
            This._Controls.TextColor = new Componet.TPalleteColor(Box1_Formas3_TextColor.Column[1].This, "SecundaryColor00");
            This._Controls.TextColor.CreatePallete();
            (action === "new") ? This._Controls.TextColor.Color = "white" : This._Controls.TextColor.Color = objSecundary.SECUNDARY_TEXTCOLOR;
            // END TEXTCOLOR //
            // POSITION //
            var Box1_Formas3_Position = new TVclStackPanel(ContentBox3.Column[0].This, "", 2, [[12, 12], [3, 9], [3, 9], [3, 9], [3, 9]]);
            Box1_Formas3_Position.Row.This.classList.add("DownSpaceSecundary");
            var Box1_Formas3_lbl_Position = new TVcllabel(Box1_Formas3_Position.Column[0].This, "", TlabelType.H0);
            Box1_Formas3_lbl_Position.Text = UsrCfg.Traslate.GetLangText(This._Mythis, "POSITION");
            Box1_Formas3_lbl_Position.VCLType = TVCLType.BS;
            var Box1_Formas3_txt_Position = new TVclTextBox(Box1_Formas3_Position.Column[1].This, "");
            Box1_Formas3_txt_Position.VCLType = TVCLType.BS;
            Box1_Formas3_txt_Position.This.removeAttribute("type")
            Box1_Formas3_txt_Position.This.setAttribute("type", "number");
            Box1_Formas3_txt_Position.This.setAttribute("min", "0");
            Box1_Formas3_txt_Position.Text = (action === "new") ? "0" : objSecundary.SECUNDARY_POSITION;
            This._Controls.Position = Box1_Formas3_txt_Position;
            // END POSITION //
            // ICON //
            var Box1_Formas3_Icon = new TVclStackPanel(ContentBox3.Column[0].This, "", 2, [[12, 12], [3, 9], [3, 9], [3, 9], [3, 9]]);
            Box1_Formas3_Icon.Row.This.classList.add("DownSpaceSecundary");
            var Box1_Formas3_lbl_Icon = new TVcllabel(Box1_Formas3_Icon.Column[0].This, "", TlabelType.H0);
            Box1_Formas3_lbl_Icon.Text = UsrCfg.Traslate.GetLangText(This._Mythis, "ICON");
            Box1_Formas3_lbl_Icon.VCLType = TVCLType.BS;
            var Box1_Formas3_txt_Icon = new TVclTextBox(Box1_Formas3_Icon.Column[1].This, "");
            Box1_Formas3_txt_Icon.VCLType = TVCLType.BS;
            Box1_Formas3_txt_Icon.Text = (action === "new") ? "" : objSecundary.SECUNDARY_ICON;
            This._Controls.Icon = Box1_Formas3_txt_Icon;
            // END ICON //
            // PHRASE //
            var Box1_Formas3_Frase = new TVclStackPanel(ContentBox3.Column[0].This, "", 2, [[12, 12], [3, 9], [3, 9], [3, 9], [3, 9]]);
            Box1_Formas3_Frase.Row.This.classList.add("DownSpaceSecundary");
            var Box1_Formas3_lbl_Frase = new TVcllabel(Box1_Formas3_Frase.Column[0].This, "", TlabelType.H0);
            Box1_Formas3_lbl_Frase.Text = UsrCfg.Traslate.GetLangText(This._Mythis, "PHRASE");
            Box1_Formas3_lbl_Frase.VCLType = TVCLType.BS;
            var Box1_Formas3_txt_Frase = new TVclMemo(Box1_Formas3_Frase.Column[1].This, "");
            Box1_Formas3_txt_Frase.VCLType = TVCLType.BS;
            Box1_Formas3_txt_Frase.This.removeAttribute("cols");
            Box1_Formas3_txt_Frase.This.removeAttribute("rows");
            Box1_Formas3_txt_Frase.This.classList.add("form-control");
            Box1_Formas3_txt_Frase.Text = (action === "new") ? "" : Secundary.SECUNDARY_PHRASE;
            This._Controls.Phrase = Box1_Formas3_txt_Frase;
            // END PHRASE //
            // PARENT //
            var Box1_Formas3_Padre = new TVclStackPanel(ContentBox3.Column[0].This, "", 2, [[12, 12], [3, 9], [3, 9], [3, 9], [3, 9]]);
            Box1_Formas3_Padre.Row.This.classList.add("DownSpaceSecundary");
            var Box1_Formas3_lbl_Padre = new TVcllabel(Box1_Formas3_Padre.Column[0].This, "", TlabelType.H0);
            Box1_Formas3_lbl_Padre.Text = UsrCfg.Traslate.GetLangText(This._Mythis, "MAIN");
            Box1_Formas3_lbl_Padre.VCLType = TVCLType.BS;
            var Box1_Formas3_txt_Padre = new TVclTextBox(Box1_Formas3_Padre.Column[1].This, "");
            Box1_Formas3_txt_Padre.VCLType = TVCLType.BS;
            Box1_Formas3_txt_Padre.Text = (action === "new") ? "" : Secundary.SECUNDARY_MAIN;
            This._Controls.Main = Box1_Formas3_txt_Padre;
            // END PARENT //
            // BUTTOMS //
            var Box1_formas3_Btn = new TVclStackPanel(ContentBox3.Column[0].This, "", 3, [[12, 12, 12], [12, 12, 12], [12, 12, 12], [12, 12, 12], [12, 12, 12]]);
            Box1_formas3_Btn.Row.This.classList.add("DownSpaceSecundary");
            var ButtonAdd = new TVclInputbutton(Box1_formas3_Btn.Column[0].This, "");
            ButtonAdd.Text = UsrCfg.Traslate.GetLangText(This._Mythis, "SAVE");
            ButtonAdd.VCLType = TVCLType.BS;
            ButtonAdd.This.style.float = "right";
            This._Controls.Save = ButtonAdd;
            //// END BUTTONS //
            return true;
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.FormSecundary CreateSecundary()", e);
        }
    };
    (function () {
        This.CreateSecundary({ SECUNDARY_TITLE: "", SECUNDARY_DESCRIPTION: "", SECUNDARY_POSITION: "", SECUNDARY_ICON: "", SECUNDARY_PHRASE: "", SECUNDARY_MAIN: "", SECUNDARY_TEXTCOLOR: "" }, "new");
    })();
}
ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Modal = function (Translate) {
    var This = this;
    this._Mythis = "PanelServicesMapping";
    this._Translate = Translate;
    this._Modal = null;
    Object.defineProperty(this, 'Modal', {
        get: function () {
            if (This._Modal === null || This._Modal === undefined) {
                if (this.CrateModal()) {
                    return This._Modal;
                };
            }
            else {
                return This._Modal;
            }
        }
    });
    this.CrateModal = function () {
        try {
            var Modal = new TVclModal(document.body, null, "")
            This._Modal = Modal;
            return true;
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Modal CrateModal()", e);
        }
    }
    this.AddTitle = function (Text) {
        try {
            if (This._Modal !== null || This._Modal !== undefined) {
                This._Modal.AddHeaderContent(Text);
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Modal AddTitle()", e);
        }
    }
    this.AddBoddy = function (nodeHTML) {
        try {
            if (This._Modal !== null || This._Modal !== undefined) {
                This._Modal.AddContent(nodeHTML);
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Modal AddBoddy()", e);
        }
    }
    this.AddFooterContent = function (nodeHTML) {
        try {
            if (This._Modal !== null || This._Modal !== undefined) {
                This._Modal.AddFooterContent(nodeHTML);
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Modal AddFooterContent()", e);
        }
    };
    (function () {
        This.CrateModal();
    })();
}
ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.ComboBox = function (Translate) {
    var This = this;
    this._Mythis = "PanelServicesMapping";
    this._Translate = Translate;
    this._ContentBoxCombo = null;
    this._Combo = null;
    this._ComboItems = new Array();
    this._InitElementCombo = null;
    Object.defineProperty(this, 'ContentBoxCombo', {
        get: function () {
            if (This._ContentBoxCombo === null || This._ContentBoxCombo === undefined) {
                if (this.CrateCombo()) {
                    return This._ContentBoxCombo;
                };
            }
            else {
                return This._ContentBoxCombo;
            }
        },
    });
    Object.defineProperty(this, 'ComboItems', {
        get: function () {
            return This._ComboItems;
        },
    });
    Object.defineProperty(this, 'InitElementCombo', {
        get: function () {
            return This._InitElementCombo;
        },
        set: function (_Value) {
            This._InitElementCombo = _Value;
        }
    });
    Object.defineProperty(this, 'Combo', {
        get: function () {
            if (This._Combo === null || This._Combo === undefined) {
                if (This.CrateCombo()) {
                    return This._Combo;
                };
            }
            else {
                return This._Combo;
            }
        },
    });
    this.AllAddData = function (objData) {
        try {
            if (objData === null || objData === undefined || objData.length <= 0) {
                var ComboItem = new TVclComboBoxItem();
                ComboItem.Value = 0;
                ComboItem.Text = UsrCfg.Traslate.GetLangText(This._Mythis, "ADDITEM");
                ComboItem.Tag = UsrCfg.Traslate.GetLangText(This._Mythis, "ADDITEM");
                This._Combo.AddItem(ComboItem);
                This._ComboItems.push(ComboItem);
                //This._Combo.selectedIndex = 0;
                This._Combo.Value = 0;
                This._Combo.This.disabled = true;
                This._InitElementCombo = true;
            }
            else {
                for (var i = 0; i < objData.length; i++) {
                    var ComboItem = new TVclComboBoxItem();
                    ComboItem.Value = objData[i].Id;
                    ComboItem.Text = objData[i].Name;
                    ComboItem.Tag = "Test" + objData[i].Id;
                    This._Combo.AddItem(ComboItem);
                    This._ComboItems.push(ComboItem);
                    This._Combo.This.disabled = false;
                    //This._Combo.selectedIndex = 0;
                    This._Combo.Value = objData[i].Id;
                    This._InitElementCombo = false;
                }
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.ComboBox AllAddData()", e);
        }
    }
    this.AddData = function (objData) {
        try {
            if (This._Combo.This.disabled) {
                This._Combo.This.disabled = false;
                This._Combo.This.innerHTML = '';
                This._ComboItems = new Array();
                var ComboItem = new TVclComboBoxItem();
                ComboItem.Value = objData.Id;
                ComboItem.Text = objData.Name;
                ComboItem.Tag = "Test";
                This._Combo.AddItem(ComboItem);
                This._ComboItems.push(ComboItem);
                This._Combo.Value = objData.Id;
                This._InitElementCombo = false;
                return true;
            }
            var ComboItem = new TVclComboBoxItem();
            ComboItem.Value = objData.Id;
            ComboItem.Text = objData.Name;
            ComboItem.Tag = "Test";
            This._Combo.AddItem(ComboItem);
            This._ComboItems.push(ComboItem);
            return false;
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.ComboBox AddData()", e);
        }

    }
    this.EditData = function (objData) {
        try {
            if (objData.Value > 0) {
                for (var i = 0; This._ComboItems < length; i++) {
                    if (This._ComboItems[i].Value === objData.Value) {
                        This._ComboItems[i].Text === objData.Text
                    }
                }
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.ComboBox EditData()", e);
        }
    }
    this.DeleteDataItem = function (ID) {
        try {
            var options = This._Combo.This.options;
            for (var i = 0; i < options.length; i++) {
                if (parseInt(options[i].value) === ID) {
                    for (var x = 0; x < This._ComboItems.length; x++) {
                        if (parseInt(This._ComboItems[x].Value) === ID) {
                            This._ComboItems.splice(x, 1);
                            break;
                        }
                    }
                    This._Combo.This.removeChild(options[i]);
                    var options = This._Combo.This.options.length;
                    if (options === 0) {
                        This.AllAddData(null);
                        return true;
                    }
                    return false;
                }
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.ComboBox DeleteDataItem()", e);
        }
    }
    this.AllDeleteDataItems = function () {
        try {
            This._Combo.This.innerHTML = '';
            This._ComboItems = new Array();
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.ComboBox AllDeleteDataItems()", e);
        }
    }
    this.CrateCombo = function (Key) {
        try {
            var BoxCombo = new TVclStackPanel("", "", 2, [[12, 12], [3, 9], [3, 9], [3, 9], [3, 9]]);
            BoxCombo.Row.This.style.margin = "0 0 10px 0";
            This._ContentBoxCombo = BoxCombo.Row.This;
            var BoxTitle = new TVcllabel(BoxCombo.Column[0].This, "", TlabelType.H0);
            BoxTitle.Text = UsrCfg.Traslate.GetLangText(This._Mythis, Key);
            BoxTitle.VCLType = TVCLType.BS;
            var Combo = new TVclComboBox2(BoxCombo.Column[1].This, "");
            Combo.VCLType = TVCLType.BS;
            This._Combo = Combo;
            return true;
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.ComboBox CrateCombo()", e);
        }
    }
    this.DeleteCombo = function () {
        try {
            if (This._ContentBoxCombo !== null || This._ContentBoxCombo !== undefined) {
                This._ContentBoxCombo.remove();
                This._ContentBoxCombo = null;
                This._Combo = null;
                This._ComboItems = new Array();
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.ComboBox DeleteCombo()", e);
        }
    }
}
ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Icons = function (Translate) {
    var This = this;
    this._Mythis = "PanelServicesMapping";
    this._Translate = Translate;
    this._ContentBoxImage = null;
    this._Buttons = { Delete: null, Clear: null, Synchronize: null, Preview: null, Update: null, Add: null }
    Object.defineProperty(this, 'ContentBoxImage', {
        get: function () {
            if (This._ContentBoxImage === null || This._ContentBoxImage === undefined) {
                if (this.CrateIcons()) {
                    return This._ContentBoxImage;
                };
            }
            else {
                return This._ContentBoxImage;
            }

        },
    });
    Object.defineProperty(this, 'Buttons', {
        get: function () {
            if (This._Buttons === null || This._Buttons === undefined) {
                if (this.CrateIcons()) {
                    return This._Buttons;
                };
            }
            else {
                return This._Buttons;
            }
        },
    });
    this.DeleteCombo = function () {
        try {
            if (This._ContentBoxImage !== null || This._ContentBoxImage !== undefined) {
                This._ContentBoxImage.remove();
            }
            This._ContentBoxImage = null;
            This._Buttons = { Delete: null, Clear: null, Synchronize: null, Preview: null, Update: null, Add: null }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Icons DeleteCombo()", e);
        }
    }
    this.CrateIcons = function () {
        try {
            // CONTENT ICONS //
            var ContentBoxImg = new TVclStackPanel("", "", 1, [[12], [12], [12], [12], [12]]);
            This._ContentBoxImage = ContentBoxImg.Row.This;
            // END CONTENT ICONS //
            // ADD //
            var ImageAdd = new TVclImagen(ContentBoxImg.Column[0].This, "");
            This._Buttons.Add = ImageAdd;
            ImageAdd.This.style.float = "left";
            ImageAdd.This.style.marginLeft = "5px";
            ImageAdd.Title = UsrCfg.Traslate.GetLangText(This._Mythis, "ADD");
            ImageAdd.Src = "image/16/add.png";
            ImageAdd.Cursor = "pointer";
            // END ADD //
            // UPDATE //
            var ImageUpdate = new TVclImagen(ContentBoxImg.Column[0].This, "");
            This._Buttons.Update = ImageUpdate;
            ImageUpdate.This.style.marginLeft = "5px";
            ImageUpdate.This.style.float = "left";
            ImageUpdate.Title = UsrCfg.Traslate.GetLangText(This._Mythis, "UPDATE");
            ImageUpdate.Src = "image/16/edit.png";
            ImageUpdate.Cursor = "pointer";
            // END UPDATE //
            // PREVIEW //
            var ImagePreview = new TVclImagen(ContentBoxImg.Column[0].This, "");
            This._Buttons.Preview = ImagePreview;
            ImagePreview.This.style.marginLeft = "5px";
            ImagePreview.This.style.float = "left";
            ImagePreview.Title = UsrCfg.Traslate.GetLangText(This._Mythis, "PREVIEW");
            ImagePreview.Src = "image/16/Computer.png";
            ImagePreview.Cursor = "pointer";
            // END PREVIEW //
            // CLEAR //
            var ImageClear = new TVclImagen(ContentBoxImg.Column[0].This, "");
            This._Buttons.Clear = ImageClear;
            ImageClear.This.style.marginLeft = "5px";
            ImageClear.This.style.float = "left";
            ImageClear.Title = UsrCfg.Traslate.GetLangText(This._Mythis, "CLEAR");
            ImageClear.Src = "image/16/Place-an-order.png";
            ImageClear.Cursor = "pointer";
            ImageClear.onClick = function () {
            }
            // END CLEAR //
            // DELETE //
            var ImageDelete = new TVclImagen(ContentBoxImg.Column[0].This, "");
            This._Buttons.Delete = ImageDelete;
            ImageDelete.This.style.float = "left";
            ImageDelete.This.style.marginLeft = "5px";
            ImageDelete.Title = UsrCfg.Traslate.GetLangText(This._Mythis, "DELETE");
            ImageDelete.Src = "image/16/delete.png";
            ImageDelete.Cursor = "pointer";
            // END DELETE //
            return true;
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Icons CrateIcons()", e);
        }
    };
    (function () {
        This.CrateIcons();
    })();
}
ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Preview = function (Translate) {
    var This = this;
    this._Mythis = "PanelServicesMapping";
    this._Translate = Translate;
    this._ContentPreview = null;
    this._Preview = null;
    Object.defineProperty(this, 'ContentPreview', {
        get: function () {
            if (This._ContentPreview === null || This._ContentPreview === undefined) {
                if (This.CreatePreview()) {
                    return This._ContentPreview;
                };
            }
            else {
                return This._ContentPreview;
            }
        }
    });
    Object.defineProperty(this, 'Preview', {
        get: function () {
            if (This._Preview === null || This._Preview === undefined) {
                if (This.CreatePreview()) {
                    return This._Preview;
                };
            }
            else {
                return This._Preview;
            }
        }
    });
    this.CreatePreview = function (IDGroup) {
        try {
            var Container_Preview = new TVclStackPanel("", 'ContainerBTN_', 1, [[11], [12], [12], [12], [12]]);
            This._ContentPreview = Container_Preview.Row.This;
            var Container_Preview_Column = Container_Preview.Column[0].This;
            var PSManager = new ItHelpCenter.TControlPanelServices(Container_Preview_Column, 'PanelMapping_', '', IDGroup);
            This._Preview = Container_Preview.Row.This;
            return true;
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Preview CreatePreview()", e);
        }
    }

}
ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Data = function () {
    var This = this;
    this._PanelServiceProfiler = new Persistence.PanelService.TPanelServiceProfiler();
    this.DataComboInitGroup = function () {
        try {
            var ListComboGroup = new Array();
            var DataGroup = This._PanelServiceProfiler.getPSGROUP();
            if (DataGroup.length <= 0) {
                return null;
            }
            for (var i = 0; i < DataGroup.length; i++) {
                ListComboGroup.push({ Id: DataGroup[i].IDPSGROUP, Name: DataGroup[i].GROUP_NAME })
            }
            return ListComboGroup;
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Data DataComboInitGroup()", e);
        }
    }
    this.DataComboInitMain = function (ID) {
        try {
            if (ID === 0) {
                return null;
            }
            else {
                var ListComboMain = new Array();
                var DataMain = This._PanelServiceProfiler.getPSMAIN();
                for (var i = 0; i < DataMain.length; i++) {
                    if (DataMain[i].IDPSGROUP === ID) {
                        ListComboMain.push({ Id: DataMain[i].IDPSMAIN, Name: DataMain[i].MAIN_TITLE })
                    }
                }
                if (ListComboMain.length <= 0) {
                    return null;
                }
                else {
                    return ListComboMain;
                }
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Data DataComboInitMain()", e);
        }
    }
    this.DataComboInitSecundary = function (ID) {
        try {
            if (ID === 0) {
                return null;
            }
            else {
                var ListComboSecundary = new Array();
                var DataSecundary = This._PanelServiceProfiler.getPSSECUNDARY();
                for (var i = 0; i < DataSecundary.length; i++) {
                    if (DataSecundary[i].IDPSMAIN === ID) {
                        ListComboSecundary.push({ Id: DataSecundary[i].IDPSSECUNDARY, Name: DataSecundary[i].SECUNDARY_TITLE })
                    }
                }
                if (ListComboSecundary.length <= 0) {
                    return null;
                }
                else {
                    return ListComboSecundary;
                }
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Data DataComboInitSecundary()", e);
        }
    }
    this.DataAddGroup = function (PSGROUP) {
        try {
            var success = This._PanelServiceProfiler.CreateGROUP_GetID(PSGROUP);
            if (success) {
                return true;
            }
            else {
                false;
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Data DataAddGroup()", e);
        }
    }
    this.DataUpdateGroup = function (PSGROUP) {
        try {
            var success = This._PanelServiceProfiler.UpdateGROUP(PSGROUP);
            if (success) {
                return true;
            }
            else {
                false;
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Data DataUpdateGroup()", e);
        }
    }
    this.DataAddMain = function (PSMAIN) {
        try {
            var success = This._PanelServiceProfiler.CreateMAIN_GetID(PSMAIN);
            if (success) {
                return true;
            }
            else {
                false;
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Data DataAddMain()", e);
        }
    }
    this.DataUpdateMain = function (PSMAIN) {
        try {
            var success = This._PanelServiceProfiler.UpdateMAIN(PSMAIN);
            if (success) {
                return true;
            }
            else {
                false;
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Data DataUpdateMain()", e);
        }
    }
    this.DataAddSecundary = function (PSSECUNDARY) {
        try {
            var success = This._PanelServiceProfiler.CreateSecundary_GetID(PSSECUNDARY);
            if (success) {
                return true;
            }
            else {
                false;
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Data DataAddSecundary()", e);
        }
    }
    this.DataUpdateSecundary = function (PSSECUNDARY) {
        try {
            var success = This._PanelServiceProfiler.UpdateSECUNDARY(PSSECUNDARY);
            if (success) {
                return true;
            }
            else {
                false;
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Data DataUpdateSecundary()", e);
        }
    }
    this.DataGetGroup = function (IDGroup) {
        try {
            var objGroup = This._PanelServiceProfiler.getPSGROUP();
            for (var i = 0; i < objGroup.length; i++) {
                if (objGroup[i].IDPSGROUP === IDGroup) {
                    return objGroup[i];
                }
            }
            return new Persistence.PanelService.Properties.TPSGROUP();
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Data DataGetGroup()", e);
        }
    }
    this.DataGetMain = function (IDMain) {
        try {
            var objMain = This._PanelServiceProfiler.getPSMAIN();
            for (var i = 0; i < objMain.length; i++) {
                if (objMain[i].IDPSMAIN === IDMain) {
                    return objMain[i];
                }
            }
            return new Persistence.PanelService.Properties.TPSMAIN();
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Data DataGetMain()", e);
        }
    }
    this.DataGetSecundary = function (IDSecundary) {
        try {
            var objSecundary = This._PanelServiceProfiler.getPSSECUNDARY();
            for (var i = 0; i < objSecundary.length; i++) {
                if (objSecundary[i].IDPSSECUNDARY === IDSecundary) {
                    return objSecundary[i];
                }
            }
            return new Persistence.PanelService.Properties.TPSSECUNDARY();
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Data DataGetSecundary()", e);
        }
    }
    this.DeleteAllGroup = function (PSGROUP) {
        try {
            var success = This._PanelServiceProfiler.DeleteAllGROUP(PSGROUP);
            if (success) {
                return true;
            }
            else {
                false;
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Data DeleteAllGroup()", e);
        }
    }
    this.DeleteAllMain = function (PSMAIN) {
        try {
            var success = This._PanelServiceProfiler.DeleteAllMain(PSMAIN);
            if (success) {
                return true;
            }
            else {
                false;
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Data DeleteAllMain()", e);
        }
    }
    this.DeleteSecundary = function (PSSECUNDARY) {
        try {
            var success = This._PanelServiceProfiler.DeleteSECUNDARY(PSSECUNDARY);
            if (success) {
                return true;
            }
            else {
                false;
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.Data DeleteSecundary()", e);
        }
    }
}
ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.AddStyleFile = function (nombre, onSuccess, onError) {
    try {
        var style = document.createElement("link");
        style.rel = "stylesheet";
        style.type = "text/css";
        style.href = nombre;
        var s = document.head.appendChild(style);
        s.onload = onSuccess;
        s.onerror = onError;
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.AddStyleFile", e);
    }
}
ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.LoadScript = function (nombre, onSuccess, onError) {
    try {
        var s = document.createElement("script");
        s.onload = onSuccess;
        s.onerror = onError;
        s.src = nombre;
        document.querySelector("head").appendChild(s);
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("PanelServicesMapping.js ItHelpCenter.TPanelServicesManager.PanelServicesMapping.prototype.LoadScript", e);
    }
}
