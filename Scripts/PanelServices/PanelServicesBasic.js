ItHelpCenter.TPanelServicesManager.PanelServicesBasic = function (inObjectHtml, id, inCallBack) {
    this.TParent = function () {
        return this;
    }.bind(this);

    var _this = this.TParent();
    this._ObjectHtml = inObjectHtml;
    this._Id = id;
    this.ElementContentGroup = null;
    this.ElementContentMain = null;
    this.ElementContentSecundary = null;
    this.ElementModal = null;
    this.ElementRowGroup = new Array();
    this.ElementRowMain = new Array();
    this.ElementRowSecundary = new Array();

    this.Mythis = "PanelServicesBasic";

    UsrCfg.Traslate.GetLangText(_this.Mythis, "USER", "User");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "MAIN", "Main");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "SECUNDARY", "Secondary");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "ID", "Id");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "TITLE", "Title");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "PATH", "Path");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "DESCRIPTION", "Description");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "POSITION", "Position");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "IMAGE", "Icon");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "PHRASE", "Phrase");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "PARENT", "Parent");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "SAVE", "Save");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "UPDATE", "Update");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "CANCEL", "Cancel");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BACKGROUND", "Background");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BORDER", "Border");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "TITLECOLOR", "Title Color");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BACKGROUNDACTIVE", "Background Active");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "TITLECOLORACTIVE", "Title Color Active");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BORDERCOLORACTIVE", "Border Color Active");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "BACKGROUNDHOVER", "Background Hover");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "TITLECOLORHOVER", "Title Color Hover");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "TEXTCOLOR", "Text Color");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "CONFIRMDELETE", "Do you want to delete the selected record?");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "NOCONFIRMDELETE", "No record was deleted");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "NOSELECT", "No record selected");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "NOTITLE", "Cannot register a user without a name");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "NONUMBER", "You must add a correct number in the position field");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "CORRECTSAVE", "Registration saved correctly");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECTSAVE", "An error occurred while saving the record");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "CORRECTUPDATE", "The registration was updated correctly");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECTUPDATE", "An error occurred while updating a record");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "CORRECTDELETE", "The record (s) were deleted correctly");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECTDELETE", "The record (s) could not be deleted");

    _this.LoadScript(SysCfg.App.Properties.xRaiz + "Componet/Color/Plugin/spectrum.js", function () {
        _this.AddStyleFile(SysCfg.App.Properties.xRaiz + "Componet/Color/Plugin/spectrum.css", function () {
            _this.AddStyleFile(SysCfg.App.Properties.xRaiz + "Css/PanelService/PSBasic.css", function () {
                _this.Load();
            }, function (e) {
                SysCfg.Log.Methods.WriteLog("PanelServicesBasic.js ItHelpCenter.TPanelServicesManager.PanelServicesBasic _this.AddStyleFile(" + _this.AddStyleFile + "Css/PanelService/PSBasic.css)", e);
            });
        }, function (e) {
            SysCfg.Log.Methods.WriteLog("PanelServicesBasic.js ItHelpCenter.TPanelServicesManager.PanelServicesBasic _this.AddStyleFile(" + _this.AddStyleFile + "Componet/Color/Plugin/spectrum.css)", e);
        });
    }, function (e) {
        SysCfg.Log.Methods.WriteLog("PanelServicesBasic.js ItHelpCenter.TPanelServicesManager.PanelServicesBasic _this.LoadScript(" + _this.LoadScript + "Componet/Color/Plugin/spectrum.js)", e);
    });
}

ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.Load = function () {
	var _this = this.TParent();
	try {
		var PSProfiler = new Persistence.PanelService.TPanelServiceProfiler();
		PSProfiler.Fill();
		_this.PSGROUPList = PSProfiler.PSGROUPList;
		_this.PSMAINList = PSProfiler.PSMAINList;
		_this.PSSECUNDARYList = PSProfiler.PSSECUNDARYList;
		_this.Content(_this._ObjectHtml, '',UsrCfg.Traslate.GetLangText(_this.Mythis, 'USER'), '', _this.PSGROUPList);
	} catch (e) {
		SysCfg.Log.Methods.WriteLog("PanelServicesBasic.js ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.Load", e);
	}
}

ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.Content = function (objectHtml, id, title, name, psList) {
	var _this = this.TParent();
	var ObjectHtml = objectHtml;
	var Title = title;
	var Name = name;
	var PSList = psList;
	try {
		if (_this.ElementContentMain != null && Title ==UsrCfg.Traslate.GetLangText(_this.Mythis, 'MAIN')) {
			$(_this.ElementContentMain.form).remove();
			if (_this.ElementContentSecundary != null) {
				$(_this.ElementContentSecundary.form).remove();
			}
		}
		else if (_this.ElementContentSecundary != null && Title ==UsrCfg.Traslate.GetLangText(_this.Mythis, 'SECUNDARY')) {
			$(_this.ElementContentSecundary.form).remove();
		}

		var Container = new TVclStackPanel(ObjectHtml, id + '_1_' + Title, 1, [[12], [12], [12], [12], [12]]);
		var ConLabelControl = new TVclStackPanel(Container.Column[0].This, '', 1, [[12], [12], [12], [12], [12]]);
		ConLabelControl.Row.This.classList.add('TitleBasic');
		var lblTitle = new TVcllabel(ConLabelControl.Column[0].This, '', TlabelType.H0);
		(Name != '') ? lblTitle.Text = Title + ': ' + Name : lblTitle.Text = Title ;
		lblTitle.VCLType = TVCLType.BS;

		var FormContainer = new TVclStackPanel(Container.Column[0].This, '', 2, [[12, 12], [12, 12], [5,7], [5, 7], [4, 8]]);
		var TitleGrid = new TGrid(FormContainer.Column[0].This, '', '');
		var Col0 = new TColumn();
		Col0.Name = ""; 
		Col0.Caption = "Title";
		Col0.Index = 0; 
		Col0.EnabledEditor = false;
		Col0.DataType = SysCfg.DB.Properties.TDataType.String;
		Col0.ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.None;
		Col0.This.classList.add('col-xs-12');
		TitleGrid.AddColumn(Col0);
		if (Title == UsrCfg.Traslate.GetLangText(_this.Mythis, 'USER')) {
			Container.Row.This.classList.add('ContainerGroup');
			_this.ElementContentGroup = { table: TitleGrid, formText: FormContainer.Column[1].This };
			_this.AddRowGroup();
			TitleGrid.This.classList.add('TableGroup');
			TitleGrid.This.classList.add('table-fixed');
			_this.CreateFormGroup(FormContainer.Column[1].This);
			if(Source.Menu.IsMobil){
				Container.Row.This.style.marginLeft = '0px';
				Container.Row.This.style.marginRight = '0px';
				Container.Row.This.style.paddingLeft = '0px';
			}
		}
		else if (Title == UsrCfg.Traslate.GetLangText(_this.Mythis, 'MAIN')) {
			Container.Row.This.classList.add('ContainerMain');
			TitleGrid.ResponsiveCont.classList.remove('table-responsive');
			_this.ElementContentMain = { form: Container.Row.This, table: TitleGrid, title: lblTitle, formText: FormContainer.Column[1].This };
			_this.AddRowMain(PSList);
			TitleGrid.This.classList.add('TableMain');
			TitleGrid.This.classList.add('table-fixed');
			_this.CreateFormMain(FormContainer.Column[1].This);
			if(Source.Menu.IsMobil){
				Container.Row.This.style.marginLeft = '0px';
				Container.Row.This.style.marginRight = '0px';
				Container.Row.This.style.paddingLeft = '0px';
			}
		}
		else if (Title == UsrCfg.Traslate.GetLangText(_this.Mythis, 'SECUNDARY')) {
			Container.Row.This.classList.add('ContainerSecundary');
			_this.ElementContentSecundary = { form: Container.Row.This, table: TitleGrid, title: lblTitle, formText: FormContainer.Column[1].This };
			_this.AddRowSecundary(PSList)
			TitleGrid.This.classList.add('TableSecundary');
			TitleGrid.This.classList.add('table-fixed');
			_this.CreateFormSecundary(FormContainer.Column[1].This);
			if(Source.Menu.IsMobil){
				Container.Row.This.style.marginLeft = '0px';
				Container.Row.This.style.marginRight = '0px';
				Container.Row.This.style.paddingLeft = '0px';
			}
		}
	} catch (e) {
		SysCfg.Log.Methods.WriteLog("PanelServicesBasic.js ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.Content", e);
	}
}

ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.CreateFormGroup = function (objectHtml) {
	var _this = this.TParent();
	_this.ElementFormGroup = null;
	var ObjectHtml = objectHtml;
	try {
		var ContImages = new TVclStackPanel(ObjectHtml, "", 1, [[12], [12], [12], [12], [12]]);
		ContImages.Row.This.classList.add("DownSpace");
		//Delete
		var ImageDelete = new TVclImagen(ContImages.Column[0].This, "");
		ImageDelete.This.style.float = "right";
		ImageDelete.This.style.marginLeft = "5px";
		ImageDelete.Title = "Delete";
		ImageDelete.Src = "image/16/delete.png";
		ImageDelete.Cursor = "pointer";
		ImageDelete.onClick = function () {
			if (txtId.Text != '') {
				var confirmed = confirm(UsrCfg.Traslate.GetLangText(_this.Mythis, 'CONFIRMDELETE'));
				if (confirmed) {
					_this.DeleteGroup(parseInt(txtId.Text));
				}
				else {
					alert(UsrCfg.Traslate.GetLangText(_this.Mythis, 'NOCONFIRMDELETE'));
				}
			}
			else {
				alert(UsrCfg.Traslate.GetLangText(_this.Mythis, 'NOSELECT'));
			}
		}
		//Update
		var ImageUpdate = new TVclImagen(ContImages.Column[0].This, "");
		ImageUpdate.This.style.marginLeft = "5px";
		ImageUpdate.This.style.float = "right";
		ImageUpdate.Title = "Edit";
		ImageUpdate.Src = "image/16/edit.png";
		ImageUpdate.Cursor = "pointer";
		ImageUpdate.onClick = function () {
			if (txtId.Text != '') {
				txtName.This.disabled = false;
				txtPath.This.disabled = false;
				bkg.Disabled = false;
				bdr.Disabled = false;
				memoDescription.This.disabled = false;
				btnAdd.This.style.display = "none";
				btnUpdate.This.style.display = "block";
				btnCancel.This.style.visibility = 'visible';
			}
			else {
				txtName.This.disabled = true;
				txtPath.This.disabled = true;
				bkg.Disabled = true;
				bdr.Disabled = true;
				memoDescription.This.disabled = true;
				alert(UsrCfg.Traslate.GetLangText(_this.Mythis, 'NOSELECT'));
				btnAdd.This.style.display = "none";
				btnUpdate.This.style.display = "none";
				btnCancel.This.style.visibility = 'hidden';
			}
		}
		//Add
		var ImageAdd = new TVclImagen(ContImages.Column[0].This, "");
		ImageAdd.This.style.marginLeft = "5px";
		ImageAdd.This.style.float = "right";
		ImageAdd.Title = "Add";
		ImageAdd.Src = "image/16/add.png";
		ImageAdd.Cursor = "pointer";
		ImageAdd.onClick = function () {
			txtId.Text = '';
			txtName.Text = '';
			txtPath.Text = '';
			bkg.Color = 'white';
			bdr.Color = 'white';
			memoDescription.Text = '';
			txtName.This.disabled = false;
			txtPath.This.disabled = false;
			bkg.Disabled = false;
			bdr.Disabled = false;
			memoDescription.This.disabled = false;
			btnUpdate.This.style.display = 'none';
			btnAdd.This.style.display = "block";
			btnCancel.This.style.visibility = 'visible';
			if (_this.ElementContentMain != null) {
				$(_this.ElementContentMain.form).remove();
				if (_this.ElementContentSecundary != null) {
					$(_this.ElementContentSecundary.form).remove();
				}
			}
		}
	
		//Preview
		var ImagePreview = new TVclImagen(ContImages.Column[0].This, "");
		ImagePreview.This.style.float = "right";
		ImagePreview.Title = "Preview";
		ImagePreview.Src = "image/16/Computer.png";
		ImagePreview.Cursor = "pointer";
		ImagePreview.onClick = function () {
			_this.ShowPreview(parseInt(_this.ElementFormGroup.id.Text), _this.ElementFormGroup.name.Text);
		}

		var ContTextId = new TVclStackPanel(ObjectHtml, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
		ContTextId.Row.This.classList.add('DownSpace');
		var lblId = new TVcllabel(ContTextId.Column[0].This, "", TlabelType.H0);
		lblId.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ID");
		lblId.VCLType = TVCLType.BS;
		var txtId = new TVclTextBox(ContTextId.Column[1].This, "txtIDGROUP");
		txtId.Text = "";
		txtId.This.disabled = true;
		txtId.VCLType = TVCLType.BS;

		var ContTextName = new TVclStackPanel(ObjectHtml, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
		ContTextName.Row.This.classList.add('DownSpace');
		var lblName = new TVcllabel(ContTextName.Column[0].This, "", TlabelType.H0);
		lblName.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TITLE");
		lblName.VCLType = TVCLType.BS;
		var txtName = new TVclTextBox(ContTextName.Column[1].This, "txtNAMEGROUP");
		txtName.Text = "";
		txtName.This.disabled = true;
		txtName.VCLType = TVCLType.BS;

		var ContTextPath = new TVclStackPanel(ObjectHtml, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
		ContTextPath.Row.This.classList.add('DownSpace');
		var lblPath = new TVcllabel(ContTextPath.Column[0].This, "", TlabelType.H0);
		lblPath.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "PATH");
		lblPath.VCLType = TVCLType.BS;
		var txtPath = new TVclTextBox(ContTextPath.Column[1].This, "txtPATHGROUP");
		txtPath.Text = "";
		txtPath.This.disabled = true;
		txtPath.VCLType = TVCLType.BS;

		var ContTextBackground = new TVclStackPanel(ObjectHtml, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
		ContTextBackground.Row.This.classList.add("DownSpace");
		var lblBackground = new TVcllabel(ContTextBackground.Column[0].This, "", TlabelType.H0);
		lblBackground.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, 'BACKGROUND');
		lblBackground.VCLType = TVCLType.BS;
		var bkg = new Componet.TPalleteColor(ContTextBackground.Column[1].This, "GroupColor00");
		bkg.Color = "white";
		bkg.Disabled = true;
		bkg.CreatePallete();

		var ContTextBorder = new TVclStackPanel(ObjectHtml, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
		ContTextBorder.Row.This.classList.add("DownSpace");
		var lblBorder = new TVcllabel(ContTextBorder.Column[0].This, "", TlabelType.H0);
		lblBorder.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "BORDER");
		lblBorder.VCLType = TVCLType.BS;
		var bdr = new Componet.TPalleteColor(ContTextBorder.Column[1].This, "GroupColor01");
		bdr.Color = "white";
		bdr.Disabled = true;
		bdr.CreatePallete();

		var ContDescription = new TVclStackPanel(ObjectHtml, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
		ContDescription.Row.This.classList.add('DownSpace');
		var lblDescription = new TVcllabel(ContDescription.Column[0].This, "", TlabelType.H0);
		lblDescription.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "DESCRIPTION");
		lblDescription.VCLType = TVCLType.BS;
		var memoDescription = new TVclMemo(ContDescription.Column[1].This, "");
		memoDescription.VCLType = TVCLType.BS;
		memoDescription.This.removeAttribute("cols");
		memoDescription.This.removeAttribute("rows");
		memoDescription.This.classList.add("form-control");
		memoDescription.This.disabled = true;
		memoDescription.Text = '';

		var ContButton = new TVclStackPanel(ObjectHtml, "", 2, [[12, 12], [12, 12], [12, 12], [12, 12], [12, 12]]);
		ContButton.Row.This.classList.add("DownSpace");

		var btnCancel = new TVclInputbutton(ContButton.Column[0].This, "");
		btnCancel.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "CANCEL");
		btnCancel.This.style.visibility = 'hidden'
		btnCancel.VCLType = TVCLType.BS;
		btnCancel.onClick = function () {
			txtName.This.disabled = true;
			txtPath.This.disabled = true;
			bkg.Disabled = true;
			bdr.Disabled = true;
			memoDescription.This.disabled = true
			btnAdd.This.style.display = 'none';
			btnUpdate.This.style.display = 'none';
			btnCancel.This.style.visibility = 'hidden'
		}
		btnCancel.This.style.float = "right";
		btnCancel.This.style.marginLeft = "10px";

		var btnAdd = new TVclInputbutton(ContButton.Column[0].This, "");
		btnAdd.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "SAVE");
		btnAdd.This.style.display = "none";
		btnAdd.VCLType = TVCLType.BS;
		btnAdd.onClick = function () {
			if (txtName.Text.trim() != ''){
				_this.SaveGroup(txtName.Text.trim(), txtPath.Text.trim(), bkg.Color, bdr.Color, memoDescription.Text.trim());
				btnAdd.This.style.display = 'none';
				btnCancel.This.style.visibility = 'hidden'
			}
			else {
				alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NOTITLE"));
			}
		}
		btnAdd.This.style.float = "right";

		var btnUpdate = new TVclInputbutton(ContButton.Column[0].This, "");
		btnUpdate.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "UPDATE");
		btnUpdate.This.style.display = "none";
		btnUpdate.VCLType = TVCLType.BS;
		btnUpdate.onClick = function () {
			if (txtName.Text.trim() != '') {
				_this.UpdateGroup(parseInt(txtId.Text), txtName.Text.trim(), txtPath.Text.trim(), bkg.Color, bdr.Color, memoDescription.Text.trim());
				btnUpdate.This.style.display = 'none';
				btnCancel.This.style.visibility = 'hidden'
			}
			else {
				alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NOTITLE"));
			}
		}
		btnUpdate.This.style.float = "right";
		/*Bloquear los botones update y delete si no hay registros en la tabla*/
		if (_this.ElementContentGroup.table.ResponsiveCont.style.display == 'none') {
			ImageUpdate.This.style.display = 'none';
			ImageDelete.This.style.display = 'none';
		}

		_this.ElementFormGroup = { id: txtId, name: txtName, path: txtPath, background: bkg, border: bdr, description: memoDescription, save: btnAdd, update: btnUpdate, cancel: btnCancel, iupdate: ImageUpdate, idelete: ImageDelete };
	} catch (e) {
		SysCfg.Log.Methods.WriteLog("PanelServicesBasic.js ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.CreateFormGroup", e);
	}
}

ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.ShowPreview = function(idGroup, name){
	var _this = this.TParent();
	try {
		if (_this.ElementModal != null) { $(_this.ElementModal.This.This).html(''); }
		var IDGroup = idGroup;
		var Name = name;
		var Modal2 = new TVclModal(document.body, null, "");
		Modal2.AddHeaderContent(Name);
		var Container = new TVclStackPanel(Modal2.Body.This, 'PSBasic_', 1, [[12], [12], [12], [12], [12]]);
		var PanelservicesManager = new ItHelpCenter.TControlPanelServices(Container.Column[0].This, "PanelBasic_", '', IDGroup);
		if (PanelservicesManager.Show) {
			Modal2.ShowModal();
			_this.ElementModal = Modal2;
		}
	} catch (e) {
		SysCfg.Log.Methods.WriteLog("PanelServicesBasic.js ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.ShowPreview", e);
	}  
}

ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.FillGroup = function (idGroup) {
	var _this = this.TParent();
	var IDGroup = idGroup;
	try {
		for (var i = 0; i < _this.PSGROUPList.length; i++) {
			if (_this.PSGROUPList[i].IDPSGROUP == IDGroup) {
				_this.ElementFormGroup.id.Text = _this.PSGROUPList[i].IDPSGROUP;
				_this.ElementFormGroup.name.Text = _this.PSGROUPList[i].GROUP_NAME;
				_this.ElementFormGroup.path.Text = _this.PSGROUPList[i].GROUP_PATH;
				_this.ElementFormGroup.background.Color = _this.PSGROUPList[i].GROUP_BACKGROUND;
				_this.ElementFormGroup.border.Color =_this.PSGROUPList[i].GROUP_BORDER;
				_this.ElementFormGroup.description.Text = _this.PSGROUPList[i].GROUP_DESCRIPTION;

				_this.Content(_this._ObjectHtml, '', UsrCfg.Traslate.GetLangText(_this.Mythis, 'MAIN'), _this.PSGROUPList[i].GROUP_NAME, _this.PSGROUPList[i].PSMAINList);
			}
		}
	} catch (e) {
		SysCfg.Log.Methods.WriteLog("PanelServicesBasic.js ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.FillGroup", e);
	}  
}

ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.CreateFormMain = function (objectHtml) {
	var _this = this.TParent();
	_this.ElementFormMain = null;
	var ObjectHtml = objectHtml;
	var ContImages = new TVclStackPanel(ObjectHtml, "", 1, [[12], [12], [12], [12], [12]]);
	ContImages.Row.This.classList.add("DownSpace");
	//Delete
	var ImageDelete = new TVclImagen(ContImages.Column[0].This, "");
	ImageDelete.This.style.float = "right";
	ImageDelete.This.style.marginLeft = "5px";
	ImageDelete.Title = "Delete";
	ImageDelete.Src = "image/16/delete.png";
	ImageDelete.Cursor = "pointer";
	ImageDelete.onClick = function () {
		if (txtId.Text != '') {
			var confirmed = confirm(UsrCfg.Traslate.GetLangText(_this.Mythis, "CONFIRMDELETE"));
			if (confirmed) {
				_this.DeleteMain(parseInt(txtId.Text));
			}
			else {
				alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NOCONFIRMDELETE"));
			}
		}
		else {
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NOSELECT"));
		}
	}
	//Update
	var ImageUpdate = new TVclImagen(ContImages.Column[0].This, "");
	ImageUpdate.This.style.marginLeft = "5px";
	ImageUpdate.This.style.float = "right";
	ImageUpdate.Title = "Edit";
	ImageUpdate.Src = "image/16/edit.png";
	ImageUpdate.Cursor = "pointer";
	ImageUpdate.onClick = function () {
		if (txtId.Text != '') {
			txtTitle.This.disabled = false;
			titleColor.Disabled = false;
			bkgActive.Disabled = false;
			colorActive.Disabled = false;
			borderActive.Disabled = false;
			bkgHover.Disabled = false;
			colorHover.Disabled = false;
			memoDescription.This.disabled = false;
			txtPosition.This.disabled = false;
			txtIcon.This.disabled = false;
			cbGroup.This.disabled = false;
			btnAdd.This.style.display = "none";
			btnUpdate.This.style.display = "block";
			btnCancel.This.style.visibility = 'visible';
		}
		else {
			txtTitle.This.disabled = true;
			titleColor.Disabled = true;
			bkgActive.Disabled = true;
			colorActive.Disabled = true;
			borderActive.Disabled = true;
			bkgHover.Disabled = true;
			colorHover.Disabled = true;
			memoDescription.This.disabled = true;
			txtPosition.This.disabled = true;
			txtIcon.This.disabled = true;
			cbGroup.This.disabled = true;
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NOSELECT"));
			btnAdd.This.style.display = "none";
			btnUpdate.This.style.display = "none";
			btnCancel.This.style.visibility = 'hidden';
		}
	}
	//Add
	var ImageAdd = new TVclImagen(ContImages.Column[0].This, "");
	ImageAdd.This.style.float = "right";
	ImageAdd.Title = "Add";
	ImageAdd.Src = "image/16/add.png";
	ImageAdd.Cursor = "pointer";
	ImageAdd.onClick = function () {
		txtId.Text = '';
		titleColor.Color = 'white';
		bkgActive.Color = 'white';
		colorActive.Color = 'white';
		borderActive.Color = 'white';
		bkgHover.Color = 'white';
		colorHover.Color = 'white';
		txtTitle.Text = '';
		memoDescription.Text = '';
		txtPosition.Text = '0';
		txtIcon.Text = '';
		txtTitle.This.disabled = false;
		titleColor.Disabled = false;
		bkgActive.Disabled = false;
		colorActive.Disabled = false;
		borderActive.Disabled = false;
		bkgHover.Disabled = false;
		colorHover.Disabled = false;
		memoDescription.This.disabled = false;
		txtPosition.This.disabled = false;
		txtIcon.This.disabled = false;
		cbGroup.This.disabled = false;
		btnUpdate.This.style.display = 'none';
		btnAdd.This.style.display = "block";
		btnCancel.This.style.visibility = 'visible';
		if (_this.ElementContentSecundary != null) {
			$(_this.ElementContentSecundary.form).remove();
		}
	}

	var ContTextId = new TVclStackPanel(ObjectHtml, "1", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	// ContTextId.Row.This.classList.add("DownSpace");
	ContTextId.Row.This.style.marginBottom = '5px';
	var lblId = new TVcllabel(ContTextId.Column[0].This, "", TlabelType.H0);
	lblId.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ID");
	lblId.VCLType = TVCLType.BS;
	var txtId = new TVclTextBox(ContTextId.Column[1].This, "txtIDMAIN");
	txtId.Text = "";
	txtId.This.disabled = true;
	txtId.VCLType = TVCLType.BS;

	var ContTextTitle = new TVclStackPanel(ObjectHtml, "1", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	// ContTextTitle.Row.This.classList.add("DownSpace");
	ContTextTitle.Row.This.style.marginBottom = '5px';
	var lblTitle = new TVcllabel(ContTextTitle.Column[0].This, "", TlabelType.H0);
	lblTitle.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TITLE");
	lblTitle.VCLType = TVCLType.BS;
	var txtTitle = new TVclTextBox(ContTextTitle.Column[1].This, "txtTITLEMAIN");
	txtTitle.Text = "";
	txtTitle.This.disabled = true;
	txtTitle.VCLType = TVCLType.BS;

	var ContTextTitleColor = new TVclStackPanel(ObjectHtml, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	// ContTextTitleColor.Row.This.classList.add("DownSpace");
	ContTextTitleColor.Row.This.style.marginBottom = '5px';
	var lblTitleColor = new TVcllabel(ContTextTitleColor.Column[0].This, "", TlabelType.H0);
	lblTitleColor.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TITLECOLOR");
	lblTitleColor.VCLType = TVCLType.BS;
	var titleColor = new Componet.TPalleteColor(ContTextTitleColor.Column[1].This, "GroupColor02");
	titleColor.Color = "white";
	titleColor.Disabled = true;
	titleColor.CreatePallete();

	var ContTextBackgroundActive = new TVclStackPanel(ObjectHtml, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	// ContTextBackgroundActive.Row.This.classList.add("DownSpace");
	ContTextBackgroundActive.Row.This.style.marginBottom = '5px';
	var lblBackgroundActive = new TVcllabel(ContTextBackgroundActive.Column[0].This, "", TlabelType.H0);
	lblBackgroundActive.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "BACKGROUNDACTIVE");
	lblBackgroundActive.VCLType = TVCLType.BS;
	var bkgActive = new Componet.TPalleteColor(ContTextBackgroundActive.Column[1].This, "GroupColor03");
	bkgActive.Color = "white";
	bkgActive.Transparency = false;
	bkgActive.Disabled = true;
	bkgActive.CreatePallete();

	var ContTextColorActive= new TVclStackPanel(ObjectHtml, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	// ContTextColorActive.Row.This.classList.add("DownSpace");
	ContTextColorActive.Row.This.style.marginBottom = '5px';
	var lblColorActive = new TVcllabel(ContTextColorActive.Column[0].This, "", TlabelType.H0);
	lblColorActive.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TITLECOLORACTIVE");
	lblColorActive.VCLType = TVCLType.BS;
	var colorActive = new Componet.TPalleteColor(ContTextColorActive.Column[1].This, "GroupColor04");
	colorActive.Color = "white";
	colorActive.Disabled = true;
	colorActive.CreatePallete();

	var ContTextBorderActive= new TVclStackPanel(ObjectHtml, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	// ContTextBorderActive.Row.This.classList.add("DownSpace");
	ContTextBorderActive.Row.This.style.marginBottom = '5px';
	var lblBorderActive = new TVcllabel(ContTextBorderActive.Column[0].This, "", TlabelType.H0);
	lblBorderActive.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "BORDERCOLORACTIVE");
	lblBorderActive.VCLType = TVCLType.BS;
	var borderActive = new Componet.TPalleteColor(ContTextBorderActive.Column[1].This, "GroupColor05");
	borderActive.Color = "white";
	borderActive.Disabled = true;
	borderActive.CreatePallete();

	var ContTextBackgroundHover= new TVclStackPanel(ObjectHtml, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	// ContTextBackgroundHover.Row.This.classList.add("DownSpace");
	ContTextBackgroundHover.Row.This.style.marginBottom = '5px';
	var lblBackgroundHover = new TVcllabel(ContTextBackgroundHover.Column[0].This, "", TlabelType.H0);
	lblBackgroundHover.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "BACKGROUNDHOVER");
	lblBackgroundHover.VCLType = TVCLType.BS;
	var bkgHover = new Componet.TPalleteColor(ContTextBackgroundHover.Column[1].This, "GroupColor06");
	bkgHover.Color = "white";
	bkgHover.Disabled = true;
	bkgHover.CreatePallete();

	var ContTextColorHover= new TVclStackPanel(ObjectHtml, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	// ContTextColorHover.Row.This.classList.add("DownSpace");
	ContTextColorHover.Row.This.style.marginBottom = '5px';
	var lblColorHover = new TVcllabel(ContTextColorHover.Column[0].This, "", TlabelType.H0);
	lblColorHover.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TITLECOLORHOVER");
	lblColorHover.VCLType = TVCLType.BS;
	var colorHover = new Componet.TPalleteColor(ContTextColorHover.Column[1].This, "GroupColor07");
	colorHover.Color = "white";
	colorHover.Disabled = true;
	colorHover.CreatePallete();

	var ContDescription = new TVclStackPanel(ObjectHtml, "1", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	// ContDescription.Row.This.classList.add("DownSpace");
	ContDescription.Row.This.style.marginBottom = '5px';
	var lblDescription = new TVcllabel(ContDescription.Column[0].This, "", TlabelType.H0);
	lblDescription.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "DESCRIPTION");
	lblDescription.VCLType = TVCLType.BS;
	var memoDescription = new TVclMemo(ContDescription.Column[1].This, "");
	memoDescription.VCLType = TVCLType.BS;
	memoDescription.This.removeAttribute("cols");
	memoDescription.This.removeAttribute("rows");
	memoDescription.This.classList.add("form-control");
	memoDescription.This.disabled = true;
	memoDescription.Text = '';

	var ContPosition = new TVclStackPanel(ObjectHtml, "1", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	// ContPosition.Row.This.classList.add("DownSpace");
	ContPosition.Row.This.style.marginBottom = '5px';
	var lblPosition = new TVcllabel(ContPosition.Column[0].This, "", TlabelType.H0);
	lblPosition.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "POSITION");
	lblPosition.VCLType = TVCLType.BS;
	var txtPosition = new TVclTextBox(ContPosition.Column[1].This, "txtPOSIMAIN");
	txtPosition.Text = "";
	txtPosition.This.disabled = true;
	txtPosition.VCLType = TVCLType.BS;
	txtPosition.This.setAttribute("type", "number");
	txtPosition.This.setAttribute("min", "0");

	var ContIcon = new TVclStackPanel(ObjectHtml, "1", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	// ContIcon.Row.This.classList.add("DownSpace");
	ContIcon.Row.This.style.marginBottom = '5px';
	var lblIcon = new TVcllabel(ContIcon.Column[0].This, "", TlabelType.H0);
	lblIcon.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "IMAGE");
	lblIcon.VCLType = TVCLType.BS;
	var txtIcon = new TVclTextBox(ContIcon.Column[1].This, "txtICONMAIN");
	txtIcon.Text = "";
	txtIcon.This.disabled = true;
	txtIcon.VCLType = TVCLType.BS;

	/* Cambiar por un LIST */
	var ContGroup = new TVclStackPanel(ObjectHtml, "1", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	// ContGroup.Row.This.classList.add("DownSpace");
	ContGroup.Row.This.style.marginBottom = '5px';
	var lblGroup = new TVcllabel(ContGroup.Column[0].This, "", TlabelType.H0);
	lblGroup.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "PARENT");
	lblGroup.VCLType = TVCLType.BS;
	var cbGroup = new TVclComboBox2(ContGroup.Column[1].This, "");
	cbGroup.VCLType = TVCLType.BS;
	var aux = null;
	for (var i = 0; i < _this.PSGROUPList.length; i++) {
		var ComboItem = new TVclComboBoxItem();
		ComboItem.Value = _this.PSGROUPList[i].IDPSGROUP;
		ComboItem.Text = _this.PSGROUPList[i].GROUP_NAME;
		ComboItem.Tag = "Test";
		cbGroup.AddItem(ComboItem);
		if (_this.PSGROUPList[i].IDPSGROUP == parseInt(_this.ElementFormGroup.id.Text)) {
			aux = i;
		}
	}
	cbGroup.selectedIndex = aux;
	//Box1_Formas2_txt_Padre.Value = Main.IDPSGROUP;
	cbGroup.This.classList.add("DropDownListGroup");
	cbGroup.This.disabled = true;

	var ContButton = new TVclStackPanel(ObjectHtml, "", 2, [[12, 12], [12, 12], [12, 12], [12, 12], [12, 12]]);
	ContButton.Row.This.classList.add("DownSpace");

	var btnCancel = new TVclInputbutton(ContButton.Column[0].This, "");
	btnCancel.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "CANCEL");
	btnCancel.This.style.visibility = "hidden";
	btnCancel.VCLType = TVCLType.BS;
	btnCancel.onClick = function () {
		txtId.This.disabled = true;
		titleColor.Disabled = true;
		bkgActive.Disabled = true;
		colorActive.Disabled = true;
		borderActive.Disabled = true;
		bkgHover.Disabled = true;
		colorHover.Disabled = true;
		txtTitle.This.disabled = true;
		memoDescription.This.disabled = true;
		txtPosition.This.disabled = true;
		txtIcon.This.disabled = true;
		cbGroup.This.disabled = true;
		btnAdd.This.style.display = 'none';
		btnUpdate.This.style.display = 'none';
		btnCancel.This.style.visibility = 'hidden';
	}
	btnCancel.This.style.float = "right";
	btnCancel.This.style.marginLeft = "10px";

	var btnAdd = new TVclInputbutton(ContButton.Column[0].This, "");
	btnAdd.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "SAVE");
	btnAdd.This.style.display = "none";
	btnAdd.VCLType = TVCLType.BS;
	btnAdd.onClick = function () {
		if (txtTitle.Text.trim() != '') {
			if (isNaN(txtPosition.Text.trim())) {
				alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NONUMBER"));
			}
			else {
				_this.SaveMain(txtTitle.Text.trim(), titleColor.Color.trim(), bkgActive.Color.trim(), colorActive.Color.trim(), borderActive.Color.trim(), bkgHover.Color.trim(), colorHover.Color.trim(), memoDescription.Text.trim(), parseInt(txtPosition.Text), txtIcon.Text.trim(), parseInt(cbGroup.Value));
				btnAdd.This.style.display = 'none';
				btnCancel.This.style.visibility = 'hidden';
			}
		}
		else {
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NOTITLE"));
		}
	}
	btnAdd.This.style.float = "right";

	var btnUpdate = new TVclInputbutton(ContButton.Column[0].This, "");
	btnUpdate.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "UPDATE");
	btnUpdate.This.style.display = "none";
	btnUpdate.VCLType = TVCLType.BS;
	btnUpdate.onClick = function () {
		if (txtTitle.Text.trim() != '') {

			if(isNaN(txtPosition.Text.trim())){
				alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NONUMBER"));
			}
			else{
				_this.UpdateMain(parseInt(txtId.Text), txtTitle.Text.trim(), titleColor.Color.trim(), bkgActive.Color.trim(), colorActive.Color.trim(), borderActive.Color.trim(), bkgHover.Color.trim(), colorHover.Color.trim(), memoDescription.Text.trim(), parseInt(txtPosition.Text), txtIcon.Text.trim(), parseInt(cbGroup.Value));
				btnUpdate.This.style.display = 'none';
				btnCancel.This.style.visibility = 'hidden';
			}
		}
		else {
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NOTITLE"));
		}
	}
	btnUpdate.This.style.float = "right";
	/*Bloquear los botones update y delete si no hay registros en la tabla*/
	if (_this.ElementContentMain.table.ResponsiveCont.style.display == 'none') {
		ImageUpdate.This.style.display = 'none';
		ImageDelete.This.style.display = 'none';
	}
	_this.ElementFormMain = { 
		id: txtId,
		title: txtTitle,
		titleColor: titleColor,
		bkgActive: bkgActive,
		colorActive: colorActive,
		borderActive: borderActive,
		bkgHover: bkgHover,
		colorHover: colorHover,
		description: memoDescription,
		position: txtPosition,
		icon: txtIcon,
		parent: cbGroup, 
		save: btnAdd, 
		update: btnUpdate, 
		cancel: btnCancel, 
		iupdate: ImageUpdate, 
		idelete: ImageDelete
	};
}

ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.FillMain = function (idMain) {
	var _this = this.TParent();
	var IDMain = idMain;
	//ID obtenido para copiar los datos en las casillas del formulario, en PSGROUP esta el id y el name falta descripcion;
	try {
		for (var i = 0; i < _this.PSMAINList.length; i++) {
			if (_this.PSMAINList[i].IDPSMAIN == IDMain) {
				_this.ElementFormMain.id.Text = _this.PSMAINList[i].IDPSMAIN;
				_this.ElementFormMain.title.Text = _this.PSMAINList[i].MAIN_TITLE;
				_this.ElementFormMain.titleColor.Color = _this.PSMAINList[i].MAIN_TITLECOLOR;
				_this.ElementFormMain.bkgActive.Color = _this.PSMAINList[i].MAIN_BACKGROUNDACTIVE;
				_this.ElementFormMain.colorActive.Color =_this.PSMAINList[i].MAIN_TITLEACTIVECOLOR;
				_this.ElementFormMain.borderActive.Color = _this.PSMAINList[i].MAIN_BORDERACTIVE;
				_this.ElementFormMain.bkgHover.Color = _this.PSMAINList[i].MAIN_BACKGROUNDHOVER;
				_this.ElementFormMain.colorHover.Color = _this.PSMAINList[i].MAIN_TITLEHOVERCOLOR;
				_this.ElementFormMain.description.Text = _this.PSMAINList[i].MAIN_DESCRIPTION;
				_this.ElementFormMain.position.Text = _this.PSMAINList[i].MAIN_POSITION;
				_this.ElementFormMain.icon.Text = _this.PSMAINList[i].MAIN_ICON;
				for (var j = 0; j < _this.ElementFormMain.parent.Options.length; j++) {
					if (parseInt(_this.ElementFormMain.parent.Options[j].Value) == _this.PSMAINList[i].IDPSGROUP) {
						_this.ElementFormMain.parent.selectedIndex = j;
					}
				}
				_this.Content(_this._ObjectHtml, '', UsrCfg.Traslate.GetLangText(_this.Mythis, 'SECUNDARY'), _this.PSMAINList[i].MAIN_TITLE, _this.PSMAINList[i].PSSECUNDARYList);
			}
		}
	} catch (e) {
		SysCfg.Log.Methods.WriteLog("PanelServicesBasic.js ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.FillMain", e);
	} 
}

ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.CreateFormSecundary = function (objectHtml) {
	var _this = this.TParent();
	_this.ElementFormSecundary = null;
	var ObjectHtml = objectHtml;
	//ICONS
	var ContImages = new TVclStackPanel(ObjectHtml, "", 1, [[12], [12], [12], [12], [12]]);
	ContImages.Row.This.classList.add("DownSpace");
	//Delete
	var ImageDelete = new TVclImagen(ContImages.Column[0].This, "");
	ImageDelete.This.style.float = "right";
	ImageDelete.This.style.marginLeft = "5px";
	ImageDelete.Title = "Delete";
	ImageDelete.Src = "image/16/delete.png";
	ImageDelete.Cursor = "pointer";
	ImageDelete.onClick = function () {
		if (txtId.Text != '') {
			var confirmed = confirm(UsrCfg.Traslate.GetLangText(_this.Mythis, "CONFIRMDELETE"));
			if (confirmed) {
				_this.DeleteSecundary(parseInt(txtId.Text));
			}
			else {
				alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NOCONFIRMDELETE"));
			}
		}
		else {
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NOSELECT"));
		}
	}
	//Update
	var ImageUpdate = new TVclImagen(ContImages.Column[0].This, "");
	ImageUpdate.This.style.marginLeft = "5px";
	ImageUpdate.This.style.float = "right";
	ImageUpdate.Title = "Edit";
	ImageUpdate.Src = "image/16/edit.png";
	ImageUpdate.Cursor = "pointer";
	ImageUpdate.onClick = function () {
		if (txtId.Text != '') {
			txtTitle.This.disabled = false;
			color.Disabled = false;
			memoDescription.This.disabled = false;
			txtPosition.This.disabled = false;
			txtIcon.This.disabled = false;
			txtPhrase.This.disabled = false;
			cbMain.This.disabled = false;
			btnAdd.This.style.display = "none";
			btnUpdate.This.style.display = "block";
			btnCancel.This.style.visibility = 'visible';
		}   
		else {
			txtTitle.This.disabled = true;
			color.Disabled = false;
			memoDescription.This.disabled = true;
			txtPosition.This.disabled = true;
			txtIcon.This.disabled = true;
			txtPhrase.This.disabled = true;
			cbMain.This.disabled = true;
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NOSELECT"))
			btnAdd.This.style.display = "none";
			btnUpdate.This.style.display = "none";
			btnCancel.This.style.visibility = 'hidden';
		}
	}
	//Add
	var ImageAdd = new TVclImagen(ContImages.Column[0].This, "");
	ImageAdd.This.style.float = "right";
	ImageAdd.Title = "Add";
	ImageAdd.Src = "image/16/add.png";
	ImageAdd.Cursor = "pointer";
	ImageAdd.onClick = function () {
		txtId.Text = '';
		txtTitle.Text = '';
		color.Color = "white";
		memoDescription.Text = '';
		txtPosition.Text = '0';
		txtIcon.Text = '';
		txtPhrase.Text = '';
		txtTitle.This.disabled = false;
		color.Disabled = false;
		memoDescription.This.disabled = false;
		txtPosition.This.disabled = false;
		txtIcon.This.disabled = false;
		txtPhrase.This.disabled = false;
		cbMain.This.disabled = false;
		btnUpdate.This.style.display = 'none';
		btnAdd.This.style.display = "block";
		btnCancel.This.style.visibility = 'visible';
	}

	var ContTextId = new TVclStackPanel(ObjectHtml, "1", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	// ContTextId.Row.This.classList.add("DownSpace");
	ContTextId.Row.This.style.marginBottom = '5px';
	var lblId = new TVcllabel(ContTextId.Column[0].This, "", TlabelType.H0);
	lblId.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ID");
	lblId.VCLType = TVCLType.BS;
	var txtId = new TVclTextBox(ContTextId.Column[1].This, "txtIDSECUNDARY");
	txtId.Text = "";
	txtId.This.disabled = true;
	txtId.VCLType = TVCLType.BS;

	var ContTextTitle = new TVclStackPanel(ObjectHtml, "1", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	// ContTextTitle.Row.This.classList.add("DownSpace");
	ContTextTitle.Row.This.style.marginBottom = '5px';
	var lblTitle = new TVcllabel(ContTextTitle.Column[0].This, "", TlabelType.H0);
	lblTitle.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TITLE");
	lblTitle.VCLType = TVCLType.BS;
	var txtTitle = new TVclTextBox(ContTextTitle.Column[1].This, "txtTITLESECUNDARY");
	txtTitle.Text = "";
	txtTitle.This.disabled = true;
	txtTitle.VCLType = TVCLType.BS;

	var ContTextColor= new TVclStackPanel(ObjectHtml, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	// ContTextColor.Row.This.classList.add("DownSpace");
	ContTextColor.Row.This.style.marginBottom = '5px';
	var lblColor = new TVcllabel(ContTextColor.Column[0].This, "", TlabelType.H0);
	lblColor.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TEXTCOLOR");
	lblColor.VCLType = TVCLType.BS;
	var color = new Componet.TPalleteColor(ContTextColor.Column[1].This, "GroupColor08");
	color.Color = "white";
	color.Disabled = true;
	color.CreatePallete();

	var ContDescription = new TVclStackPanel(ObjectHtml, "1", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	// ContDescription.Row.This.classList.add("DownSpace");
	ContDescription.Row.This.style.marginBottom = '5px';
	var lblDescription = new TVcllabel(ContDescription.Column[0].This, "", TlabelType.H0);
	lblDescription.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "DESCRIPTION");
	lblDescription.VCLType = TVCLType.BS;
	var memoDescription = new TVclMemo(ContDescription.Column[1].This, "");
	memoDescription.VCLType = TVCLType.BS;
	memoDescription.This.removeAttribute("cols");
	memoDescription.This.removeAttribute("rows");
	memoDescription.This.classList.add("form-control");
	memoDescription.This.disabled = true;
	memoDescription.Text = '';
	memoDescription.This.disabled = true;

	var ContPosition = new TVclStackPanel(ObjectHtml, "1", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	// ContPosition.Row.This.classList.add("DownSpace");
	ContPosition.Row.This.style.marginBottom = '5px';
	var lblPosition = new TVcllabel(ContPosition.Column[0].This, "", TlabelType.H0);
	lblPosition.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "POSITION");
	lblPosition.VCLType = TVCLType.BS;
	var txtPosition = new TVclTextBox(ContPosition.Column[1].This, "txtPOSISECUNDARY");
	txtPosition.Text = "";
	txtPosition.This.disabled = true;
	txtPosition.VCLType = TVCLType.BS;
	txtPosition.This.setAttribute("type", "number");
	txtPosition.This.setAttribute("min", "0");

	var ContIcon = new TVclStackPanel(ObjectHtml, "1", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	// ContIcon.Row.This.classList.add("DownSpace");
	ContIcon.Row.This.style.marginBottom = '5px';
	var lblIcon = new TVcllabel(ContIcon.Column[0].This, "", TlabelType.H0);
	lblIcon.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "IMAGE");
	lblIcon.VCLType = TVCLType.BS;
	var txtIcon = new TVclTextBox(ContIcon.Column[1].This, "txtICONSECUNDARY");
	txtIcon.Text = "";
	txtIcon.This.disabled = true;
	txtIcon.VCLType = TVCLType.BS;

	var ContPhrase = new TVclStackPanel(ObjectHtml, "1", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	// ContPhrase.Row.This.classList.add("DownSpace");
	ContPhrase.Row.This.style.marginBottom = '5px';
	var lblPhrase = new TVcllabel(ContPhrase.Column[0].This, "", TlabelType.H0);
	lblPhrase.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "PHRASE");
	lblPhrase.VCLType = TVCLType.BS;
	var txtPhrase = new TVclTextBox(ContPhrase.Column[1].This, "txtPHRASESECUNDARY");
	txtPhrase.Text = "";
	txtPhrase.This.disabled = true;
	txtPhrase.VCLType = TVCLType.BS;

	/* Cambiar por un LIST */
	var ContMain = new TVclStackPanel(ObjectHtml, "1", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	// ContMain.Row.This.classList.add("DownSpace");
	ContMain.Row.This.style.marginBottom = '5px';
	var lblMain = new TVcllabel(ContMain.Column[0].This, "", TlabelType.H0);
	lblMain.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "PARENT");
	lblMain.VCLType = TVCLType.BS;
	var cbMain = new TVclComboBox2(ContMain.Column[1].This, "");
	cbMain.VCLType = TVCLType.BS;
	var aux = null;

	for(var i = 0 ; i < _this.PSGROUPList.length; i++){
		if(_this.PSGROUPList[i].IDPSGROUP == parseInt(_this.ElementFormGroup.id.Text)){
			for(var j = 0; j < _this.PSGROUPList[i].PSMAINList.length; j++){
				var ComboItem = new TVclComboBoxItem();
				ComboItem.Value = _this.PSGROUPList[i].PSMAINList[j].IDPSMAIN;
				ComboItem.Text = _this.PSGROUPList[i].PSMAINList[j].MAIN_TITLE;
				ComboItem.Tag = "Test";
				cbMain.AddItem(ComboItem);
				if (_this.PSGROUPList[i].PSMAINList[j].IDPSMAIN == parseInt(_this.ElementFormMain.id.Text)) {
					aux = j;
				}
			}
		}
	}
	cbMain.selectedIndex = aux;
	cbMain.This.classList.add("DropDownListGroup");
	cbMain.This.disabled = true;

	var ContButton = new TVclStackPanel(ObjectHtml, "", 2, [[12, 12], [12, 12], [12, 12], [12, 12], [12, 12]]);
	ContButton.Row.This.classList.add("DownSpace");

	var btnCancel = new TVclInputbutton(ContButton.Column[0].This, "");
	btnCancel.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "CANCEL");
	btnCancel.This.style.visibility = "hidden";
	btnCancel.VCLType = TVCLType.BS;
	btnCancel.onClick = function () {
		txtId.This.disabled = true;
		txtTitle.This.disabled = true;
		color.Disabled = true;
		memoDescription.This.disabled = true;
		txtPosition.This.disabled = true;
		txtIcon.This.disabled = true;
		txtPhrase.This.disabled = true;
		cbMain.This.disabled = true;
		btnAdd.This.style.display = 'none';
		btnUpdate.This.style.display = 'none';
		btnCancel.This.style.visibility = 'hidden';
	}
	btnCancel.This.style.float = "right";
	btnCancel.This.style.marginLeft = "10px";

	var btnAdd = new TVclInputbutton(ContButton.Column[0].This, "");
	btnAdd.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "SAVE");
	btnAdd.This.style.display = "none";
	btnAdd.VCLType = TVCLType.BS;
	btnAdd.onClick = function () {
		if (txtTitle.Text.trim() != '') {
			if (isNaN(txtPosition.Text.trim())) {
				alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NONUMBER"));
			}
			else{
				_this.SaveSecundary(txtTitle.Text.trim(), color.Color.trim(), memoDescription.Text.trim(), parseInt(txtPosition.Text), txtIcon.Text.trim(), txtPhrase.Text, parseInt(cbMain.Value));
				btnAdd.This.style.display = 'none';
				btnCancel.This.style.visibility = 'hidden';
			}
		}
		else {
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NOTITLE"));
		}
	}
	btnAdd.This.style.float = "right";

	var btnUpdate = new TVclInputbutton(ContButton.Column[0].This, "");
	btnUpdate.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "UPDATE");
	btnUpdate.This.style.display = "none";
	btnUpdate.VCLType = TVCLType.BS;
	btnUpdate.onClick = function () {
		if (txtTitle.Text.trim() != '') {
			if (isNaN(txtPosition.Text.trim())) {
				alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NONUMBER"));
			}
			else{
				_this.UpdateSecundary(parseInt(txtId.Text), txtTitle.Text.trim(), color.Color.trim(), memoDescription.Text.trim(), parseInt(txtPosition.Text), txtIcon.Text.trim(), txtPhrase.Text, parseInt(cbMain.Value));
				btnUpdate.This.style.display = 'none';
				btnCancel.This.style.visibility = 'hidden';
			}
		}
		else {
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NOTITLE"));
		}
	}
	btnUpdate.This.style.float = "right";
	/*Bloquear los botones update y delete si no hay registros en la tabla*/
	if (_this.ElementContentSecundary.table.ResponsiveCont.style.display == 'none') {
		ImageUpdate.This.style.display = 'none';
		ImageDelete.This.style.display = 'none';
	}
	_this.ElementFormSecundary = { id: txtId, title: txtTitle, color: color, description: memoDescription, position: txtPosition, icon: txtIcon, phrase: txtPhrase, parent: cbMain, save: btnAdd, update: btnUpdate, cancel: btnCancel, iupdate: ImageUpdate, idelete: ImageDelete };
}

ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.FillSecundary = function (idSecundary) {
	var _this = this.TParent();
	var IDSecundary = idSecundary;
	try {
		for (var i = 0; i < _this.PSSECUNDARYList.length; i++) {
			if (_this.PSSECUNDARYList[i].IDPSSECUNDARY == IDSecundary) {
				_this.ElementFormSecundary.id.Text = _this.PSSECUNDARYList[i].IDPSSECUNDARY;
				_this.ElementFormSecundary.title.Text = _this.PSSECUNDARYList[i].SECUNDARY_TITLE;
				_this.ElementFormSecundary.color.Color = _this.PSSECUNDARYList[i].SECUNDARY_TEXTCOLOR;
				_this.ElementFormSecundary.description.Text = _this.PSSECUNDARYList[i].SECUNDARY_DESCRIPTION;
				_this.ElementFormSecundary.position.Text = _this.PSSECUNDARYList[i].SECUNDARY_POSITION;
				_this.ElementFormSecundary.icon.Text = _this.PSSECUNDARYList[i].SECUNDARY_ICON;
				_this.ElementFormSecundary.phrase.Text = _this.PSSECUNDARYList[i].SECUNDARY_PHRASE;
				//_this.ElementFormSecundary.idMain.Text = _this.PSSECUNDARYList[i].IDPSMAIN;
				for (var j = 0; j < _this.ElementFormSecundary.parent.Options.length; j++) {
					if (parseInt(_this.ElementFormSecundary.parent.Options[j].Value) == _this.PSSECUNDARYList[i].IDPSMAIN) {
						_this.ElementFormSecundary.parent.selectedIndex = j;
					}
				}
			}
		}
	} catch (e) {
		SysCfg.Log.Methods.WriteLog("PanelServicesBasic.js ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.FillSecundary", e);
	} 
}

ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.SaveGroup = function (nameGroup, pathGroup, backgroundGroup, borderGroup, descriptionGroup) {
	var _this = this.TParent();
	try {
		var PSGROUP = new Persistence.PanelService.Properties.TPSGROUP();
		PSGROUP.GROUP_NAME = nameGroup;
		PSGROUP.GROUP_PATH = pathGroup;
		PSGROUP.GROUP_DESCRIPTION = descriptionGroup;
		PSGROUP.GROUP_BACKGROUND = backgroundGroup;
		PSGROUP.GROUP_BORDER = borderGroup;
		var success = Persistence.PanelService.Methods.PSGROUP_ADD(PSGROUP);
		if (success) {
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "CORRECTSAVE"));
			_this.ElementFormGroup.id.Text = '';
			_this.ElementFormGroup.name.Text = '';
			_this.ElementFormGroup.path.Text = '';
			_this.ElementFormGroup.background.Color = 'white';
			_this.ElementFormGroup.border.Color = 'white';
			_this.ElementFormGroup.description.Text = '';
			_this.ElementFormGroup.id.This.disabled = true;
			_this.ElementFormGroup.name.This.disabled = true;
			_this.ElementFormGroup.path.This.disabled = true;
			_this.ElementFormGroup.background.Disabled = true;
			_this.ElementFormGroup.border.Disabled = true;
			_this.ElementFormGroup.description.This.disabled = true;
			/* Obtenci�n de nuevos Recursos */
			var PSProfiler = new Persistence.PanelService.TPanelServiceProfiler();
			PSProfiler.Fill();
			_this.PSGROUPList = PSProfiler.PSGROUPList;
			_this.PSMAINList = PSProfiler.PSMAINList;
			_this.PSSECUNDARYList = PSProfiler.PSSECUNDARYList;
			/*Eliminaci�n de Celdas de la tabla*/
			for (var i = 0; i < _this.ElementContentGroup.table._Rows.length; i++) {
				$(_this.ElementContentGroup.table._Rows[i].This).remove();
			}
			/*Llenado de Celdas de la tabla*/
			_this.AddRowGroup();
		}
		else {
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECTSAVE"))
		}
	} catch (e) {
		SysCfg.Log.Methods.WriteLog("PanelServicesBasic.js ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.SaveGroup", e);
	} 
}

ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.UpdateGroup = function (idGroup, nameGroup, pathGroup, backgroundGroup, borderGroup, descriptionGroup) {
	var _this = this.TParent();
	try {
		var PSGROUP = new Persistence.PanelService.Properties.TPSGROUP();
		PSGROUP.IDPSGROUP = idGroup;
		PSGROUP.GROUP_NAME = nameGroup;
		PSGROUP.GROUP_PATH = pathGroup;
		PSGROUP.GROUP_BACKGROUND = backgroundGroup;
		PSGROUP.GROUP_BORDER = borderGroup;
		PSGROUP.GROUP_DESCRIPTION = descriptionGroup;
		var success = Persistence.PanelService.Methods.PSGROUP_UPD(PSGROUP);
		if (success) {
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "CORRECTUPDATE"));
			_this.ElementFormGroup.id.This.disabled = true;
			_this.ElementFormGroup.name.This.disabled = true;
			_this.ElementFormGroup.path.This.disabled = true;
			_this.ElementFormGroup.background.Disabled = true;
			_this.ElementFormGroup.border.Disabled = true;
			_this.ElementFormGroup.description.This.disabled = true;
			_this.ElementContentMain.title.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, 'MAIN') + ': ' + nameGroup;
			/* Obtenci�n de nuevos Recursos */
			var PSProfiler = new Persistence.PanelService.TPanelServiceProfiler();
			PSProfiler.Fill();
			_this.PSGROUPList = PSProfiler.PSGROUPList;
			_this.PSMAINList = PSProfiler.PSMAINList;
			_this.PSSECUNDARYList = PSProfiler.PSSECUNDARYList;
			/*Eliminaci�n de Celdas de la tabla*/
			for (var i = 0; i < _this.ElementContentGroup.table._Rows.length; i++) {
				$(_this.ElementContentGroup.table._Rows[i].This).remove();
			}
			/*Llenado de Celdas de la tabla*/
			_this.AddRowGroup();
			//Llenar dropdownlist-combobox PSMAIN
			for (var i = 0; i < _this.ElementFormMain.parent.Options.length; i++) {
				if (parseInt(_this.ElementFormMain.parent.Options[i].This.value) == PSGROUP.IDPSGROUP) {
					_this.ElementFormMain.parent.Options[i].Text = PSGROUP.GROUP_NAME;
				}
			}
		}
		else{
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECTUPDATE"));
		}
	} catch (e) {
		SysCfg.Log.Methods.WriteLog("PanelServicesBasic.js ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.UpdateGroup", e);
	}
}

ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.DeleteGroup = function (idGroup) {
	var _this = this.TParent();
	try {
		for (var i = 0; i < _this.PSGROUPList.length; i++) {
			if (_this.PSGROUPList[i].IDPSGROUP == idGroup) {
				if (_this.PSGROUPList[i].PSMAINList.length != 0) {
					for (var j = 0; j < _this.PSGROUPList[i].PSMAINList.length; j++) {
						if (_this.PSGROUPList[i].PSMAINList[j].PSSECUNDARYList.length != 0) {
							for (var k = 0; k < _this.PSGROUPList[i].PSMAINList[j].PSSECUNDARYList.length; k++) {
								var idSecundary = _this.PSGROUPList[i].PSMAINList[j].PSSECUNDARYList[k].IDPSSECUNDARY;
								var PSSECUNDARY = new Persistence.PanelService.Properties.TPSSECUNDARY();
								PSSECUNDARY.IDPSSECUNDARY = idSecundary;
								var success = Persistence.PanelService.Methods.PSSECUNDARY_DEL(PSSECUNDARY);
								if (success) {
									//alert('eliminado');
								}
								else {
									alert('No record could be deleted in Secundary ' + k);
								}
							}
						}
						//else {
						//}
						var idMain = _this.PSGROUPList[i].PSMAINList[j].IDPSMAIN;
						var PSMAIN = new Persistence.PanelService.Properties.TPSMAIN();
						PSMAIN.IDPSMAIN = idMain;
						var success = Persistence.PanelService.Methods.PSMAIN_DEL(PSMAIN);
						if (success) {
						}
						else {
							alert('No record could be deleted in Main ' + j);
						}
					}
				}
				//else {
				//}
				var PSGROUP = new Persistence.PanelService.Properties.TPSGROUP();
				PSGROUP.IDPSGROUP = idGroup;
				var success = Persistence.PanelService.Methods.PSGROUP_DEL(PSGROUP);
				if (success) {
					alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "CORRECTDELETE"));
					$(_this.ElementContentMain.form).remove(); //remover contenedor de Main
					if (_this.ElementContentSecundary != null) {
						$(_this.ElementContentSecundary.form).remove();
					}
					_this.ElementFormGroup.id.Text = '';
					_this.ElementFormGroup.name.Text = '';
					_this.ElementFormGroup.path.Text = '';
					_this.ElementFormGroup.background.Color = 'white';
					_this.ElementFormGroup.border.Color = 'white';
					_this.ElementFormGroup.description.Text = '';
					/* Obtenci�n de nuevos Recursos */
					var PSProfiler = new Persistence.PanelService.TPanelServiceProfiler();
					PSProfiler.Fill();
					_this.PSGROUPList = PSProfiler.PSGROUPList;
					_this.PSMAINList = PSProfiler.PSMAINList;
					_this.PSSECUNDARYList = PSProfiler.PSSECUNDARYList;
					/*Eliminaci�n de Celdas de la tabla*/
					for (var i = 0; i < _this.ElementContentGroup.table._Rows.length; i++) {
						$(_this.ElementContentGroup.table._Rows[i].This).remove();
					}
					/*Llenado de Celdas de la tabla*/
					_this.AddRowGroup();
				}
				else {
					alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECTDELETE"));
				}
			}
		}
	} catch (e) {
		SysCfg.Log.Methods.WriteLog("PanelServicesBasic.js ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.DeleteGroup", e);
	}
}

ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.AddRowGroup = function () {
	var _this = this.TParent();
	try {
		if (_this.PSGROUPList.length != 0) {
			for (var i = 0; i < _this.PSGROUPList.length; i++) {
				var NewRow = new TRow();
				var Cell0 = new TCell();
				Cell0.Value = _this.PSGROUPList[i].GROUP_NAME;
				Cell0.IndexColumn = 0;
				Cell0.IndexRow = i;
				NewRow.AddCell(Cell0);
				Cell0.This.setAttribute('data-id', _this.PSGROUPList[i].IDPSGROUP);
				Cell0.This.classList.add('col-xs-12'); //Eliminar en caso no funcione el fixed
				_this.ElementRowGroup[i] = { row: Cell0.This };
				_this.ElementContentGroup.table.AddRow(NewRow);
				_this.ElementRowGroup[i].row.onclick = function () {
					_this.ElementFormGroup.id.This.disabled = true;
					_this.ElementFormGroup.name.This.disabled = true;
					_this.ElementFormGroup.path.This.disabled = true;
					_this.ElementFormGroup.background.Disabled = true;
					_this.ElementFormGroup.border.Disabled = true;
					_this.ElementFormGroup.description.This.disabled = true;
					_this.ElementFormGroup.save.This.style.display = 'none';
					_this.ElementFormGroup.update.This.style.display = 'none';
					_this.ElementFormGroup.cancel.This.style.visibility = 'hidden';
					_this.FillGroup(parseInt(this.dataset.id));
				}
				// $(Cell0.This).css('max-width', '')
			}
			if (_this.ElementFormGroup != null) {
				_this.ElementFormGroup.iupdate.This.style.display = 'block';
				_this.ElementFormGroup.idelete.This.style.display = 'block';
			}
			_this.ElementContentGroup.table.ResponsiveCont.style.display = 'block';
			_this.ElementContentGroup.table.EnabledResizeColumn = false;
			_this.ElementContentGroup.formText.classList.remove('col-md-offset-2');
		}
		else {
			if (_this.ElementFormGroup != null) {
				_this.ElementFormGroup.iupdate.This.style.display = 'none';
				_this.ElementFormGroup.idelete.This.style.display = 'none';
			}
			_this.ElementContentGroup.table.ResponsiveCont.style.display = 'none';

			_this.ElementContentGroup.table.EnabledResizeColumn = false;

			_this.ElementContentGroup.formText.classList.add('col-md-offset-2');
		}
	} catch (e) {
		SysCfg.Log.Methods.WriteLog("PanelServicesBasic.js ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.AddRowGroup", e);
	}
}

ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.SaveMain = function (titleMain, titleColor, bkgActive, colorActive, borderActive, bkgHover, colorHover, descriptionMain, positionMain, iconMain, idGroup) {
	var _this = this.TParent();
	try {
		var PSMAIN = new Persistence.PanelService.Properties.TPSMAIN();
		PSMAIN.MAIN_TITLE = titleMain;
		PSMAIN.MAIN_TITLECOLOR = titleColor;
		PSMAIN.MAIN_BACKGROUNDACTIVE = bkgActive;
		PSMAIN.MAIN_TITLEACTIVECOLOR = colorActive;
		PSMAIN.MAIN_BORDERACTIVE = borderActive;
		PSMAIN.MAIN_BACKGROUNDHOVER = bkgHover;
		PSMAIN.MAIN_TITLEHOVERCOLOR = colorHover;
		PSMAIN.MAIN_DESCRIPTION = descriptionMain;
		PSMAIN.MAIN_POSITION = positionMain;
		PSMAIN.MAIN_ICON = iconMain;
		PSMAIN.IDPSGROUP = idGroup;
		var success = Persistence.PanelService.Methods.PSMAIN_ADD(PSMAIN);
		if (success) {
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "CORRECTSAVE"));
		
			/* Obtenci�n de nuevos Recursos */
			var PSProfiler = new Persistence.PanelService.TPanelServiceProfiler();
			PSProfiler.Fill();
			_this.PSGROUPList = PSProfiler.PSGROUPList;
			_this.PSMAINList = PSProfiler.PSMAINList;
			_this.PSSECUNDARYList = PSProfiler.PSSECUNDARYList;
			/*Limpiar Cajas*/
			_this.ElementFormMain.id.Text = '';
			_this.ElementFormMain.titleColor.Color = 'white';
			_this.ElementFormMain.bkgActive.Color = 'white';
			_this.ElementFormMain.colorActive.Color = 'white';
			_this.ElementFormMain.borderActive.Color = 'white';
			_this.ElementFormMain.bkgHover.Color = 'white';
			_this.ElementFormMain.colorHover.Color = 'white';
			_this.ElementFormMain.title.Text = '';
			_this.ElementFormMain.description.Text = '';
			_this.ElementFormMain.position.Text = '0';
			_this.ElementFormMain.icon.Text = '';
			_this.ElementFormMain.id.This.disabled = true;
			_this.ElementFormMain.title.This.disabled = true;
			_this.ElementFormMain.titleColor.Disabled = true;
			_this.ElementFormMain.bkgActive.Disabled = true;
			_this.ElementFormMain.colorActive.Disabled = true;
			_this.ElementFormMain.borderActive.Disabled = true;
			_this.ElementFormMain.bkgHover.Disabled = true;
			_this.ElementFormMain.colorHover.Disabled = true;
			_this.ElementFormMain.description.This.disabled = true;
			_this.ElementFormMain.position.This.disabled = true;
			_this.ElementFormMain.icon.This.disabled = true;
			_this.ElementFormMain.parent.This.disabled = true;
			if (PSMAIN.IDPSGROUP == parseInt(_this.ElementFormGroup.id.Text)) {
				/*Eliminaci�n de Celdas de la tabla*/
				for (var i = 0; i < _this.ElementContentMain.table._Rows.length; i++) {
					$(_this.ElementContentMain.table._Rows[i].This).remove();
				}
				/*Llenado de Celdas de la tabla*/
				for (var i = 0; i < _this.PSGROUPList.length; i++) {
					if (_this.PSGROUPList[i].IDPSGROUP == PSMAIN.IDPSGROUP) {
						_this.AddRowMain(_this.PSGROUPList[i].PSMAINList);
					}
				}
			}
			else {
				//_this.ElementFormMain.id.Text = PSMAIN.IDPSMAIN;
				_this.ElementFormMain.id.This.disabled = true;
				_this.ElementFormMain.title.This.disabled = true;
				_this.ElementFormMain.titleColor.Disabled = true;
				_this.ElementFormMain.bkgActive.Disabled = true;
				_this.ElementFormMain.colorActive.Disabled = true;
				_this.ElementFormMain.borderActive.Disabled = true;
				_this.ElementFormMain.bkgHover.Disabled = true;
				_this.ElementFormMain.colorHover.Disabled = true;
				_this.ElementFormMain.description.This.disabled = true;
				_this.ElementFormMain.position.This.disabled = true;
				_this.ElementFormMain.icon.This.disabled = true;
				_this.ElementFormMain.parent.This.disabled = true;
				_this.ElementFormMain.save.This.style.display = 'none';
				_this.ElementFormMain.update.This.style.display = 'none';
				_this.ElementFormMain.cancel.This.style.visibility = 'hidden';
				/*Regresar el combobox al parent de PSMAIN*/
				for (var j = 0; j < _this.ElementFormMain.parent.Options.length; j++) {
					if (parseInt(_this.ElementFormMain.parent.Options[j].Value) == parseInt(_this.ElementFormGroup.id.Text)) {
						_this.ElementFormMain.parent.selectedIndex = j;
					}
				}
			}
		}
		else {
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECTSAVE"));
		}
	} catch (e) {
		SysCfg.Log.Methods.WriteLog("PanelServicesBasic.js ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.SaveMain", e);
	}
}

ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.UpdateMain = function (idMain, titleMain, titleColor, bkgActive, colorActive, borderActive, bkgHover, colorHover, descriptionMain, positionMain, iconMain, idGroup) {
	var _this = this.TParent();
	try {
		var PSMAIN = new Persistence.PanelService.Properties.TPSMAIN();
		PSMAIN.IDPSMAIN = idMain;
		PSMAIN.MAIN_TITLE = titleMain;
		PSMAIN.MAIN_TITLECOLOR = titleColor;
		PSMAIN.MAIN_BACKGROUNDACTIVE = bkgActive;
		PSMAIN.MAIN_TITLEACTIVECOLOR = colorActive;
		PSMAIN.MAIN_BORDERACTIVE = borderActive;
		PSMAIN.MAIN_BACKGROUNDHOVER = bkgHover;
		PSMAIN.MAIN_TITLEHOVERCOLOR = colorHover;
		PSMAIN.MAIN_DESCRIPTION = descriptionMain;
		PSMAIN.MAIN_POSITION = positionMain;
		PSMAIN.MAIN_ICON = iconMain;
		PSMAIN.IDPSGROUP = idGroup;
		var success = Persistence.PanelService.Methods.PSMAIN_UPD(PSMAIN);
		if (success) {
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "CORRECTUPDATE"));
			_this.ElementFormMain.id.This.disabled = true;
			_this.ElementFormMain.title.This.disabled = true;
			_this.ElementFormMain.titleColor.Disabled = true;
			_this.ElementFormMain.bkgActive.Disabled = true;
			_this.ElementFormMain.colorActive.Disabled = true;
			_this.ElementFormMain.borderActive.Disabled = true;
			_this.ElementFormMain.bkgHover.Disabled = true;
			_this.ElementFormMain.colorHover.Disabled = true;
			_this.ElementFormMain.description.This.disabled = true;
			_this.ElementFormMain.position.This.disabled = true;
			_this.ElementFormMain.icon.This.disabled = true;
			_this.ElementFormMain.parent.This.disabled = true;
			/* Obtenci�n de nuevos Recursos */
			var PSProfiler = new Persistence.PanelService.TPanelServiceProfiler();
			PSProfiler.Fill();
			_this.PSGROUPList = PSProfiler.PSGROUPList;
			_this.PSMAINList = PSProfiler.PSMAINList;
			_this.PSSECUNDARYList = PSProfiler.PSSECUNDARYList;
			/*Eliminaci�n de Celdas de la tabla*/
			for (var i = 0; i < _this.ElementContentMain.table._Rows.length; i++) {
				$(_this.ElementContentMain.table._Rows[i].This).remove();
			}
			/*Llenado de Celdas de la tabla*/
			for (var i = 0; i < _this.PSGROUPList.length; i++) {
				if (_this.PSGROUPList[i].IDPSGROUP == parseInt(_this.ElementFormGroup.id.Text)) {
					_this.AddRowMain(_this.PSGROUPList[i].PSMAINList);
				}
			}
			/*Regresar el combobox al parent de PSMAIN*/
			for (var j = 0; j < _this.ElementFormMain.parent.Options.length; j++) {
				if (parseInt(_this.ElementFormMain.parent.Options[j].Value) == parseInt(_this.ElementFormGroup.id.Text)) {
					_this.ElementFormMain.parent.selectedIndex = j;
				}
			}
			_this.ElementContentSecundary.title.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, 'SECUNDARY') + ': ' + titleMain;
			//Llenar dropdownlist-combobox PSSECUNDARY
			for (var i = 0; i < _this.ElementFormSecundary.parent.Options.length; i++) {
				if (parseInt(_this.ElementFormSecundary.parent.Options[i].This.value) == PSMAIN.IDPSMAIN) {
					_this.ElementFormSecundary.parent.Options[i].Text = PSMAIN.MAIN_TITLE ;
				}
			}
			if (PSMAIN.IDPSGROUP != parseInt(_this.ElementFormGroup.id.Text)) {
				_this.ElementFormMain.id.Text = '';
				_this.ElementFormMain.title.Text = '';
				_this.ElementFormMain.titleColor.Color = 'white';
				_this.ElementFormMain.bkgActive.Color = 'white';
				_this.ElementFormMain.colorActive.Color = 'white';
				_this.ElementFormMain.borderActive.Color = 'white';
				_this.ElementFormMain.bkgHover.Color = 'white';
				_this.ElementFormMain.colorHover.Color = 'white';
				_this.ElementFormMain.description.Text = '';
				_this.ElementFormMain.position.Text = '0';
				_this.ElementFormMain.icon.Text = '';
				$(_this.ElementContentSecundary.form).remove();
			}
		}
		else {
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECTUPDATE"));
		}
	} catch (e) {
		SysCfg.Log.Methods.WriteLog("PanelServicesBasic.js ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.UpdateMain", e);
	}
}

ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.DeleteMain = function (idMain) {
	var _this = this.TParent();
	try {
		for (var i = 0; i < _this.PSMAINList.length; i++) {
			if (_this.PSMAINList[i].IDPSMAIN == idMain) {
				if (_this.PSMAINList[i].PSSECUNDARYList.length != 0) {
					for (var j = 0; j < _this.PSMAINList[i].PSSECUNDARYList.length; j++) {
						var idSecundary = _this.PSMAINList[i].PSSECUNDARYList[j].IDPSSECUNDARY;
						var PSSECUNDARY = new Persistence.PanelService.Properties.TPSSECUNDARY();
						PSSECUNDARY.IDPSSECUNDARY = idSecundary;
						var success = Persistence.PanelService.Methods.PSSECUNDARY_DEL(PSSECUNDARY);
						if (success) {
							//alert('eliminado');
						}
						else {
							alert('No record could be deleted in Secundary ' + k);
						}
					}
				}
				var PSMAIN = new Persistence.PanelService.Properties.TPSMAIN();
				PSMAIN.IDPSMAIN = idMain;
				var success = Persistence.PanelService.Methods.PSMAIN_DEL(PSMAIN);
				if (success) {
					alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "CORRECTDELETE"));
					/*Limpiar Cajas*/
					_this.ElementFormMain.id.Text = '';
					_this.ElementFormMain.title.Text = '';
					_this.ElementFormMain.titleColor.Color = 'white';
					_this.ElementFormMain.bkgActive.Color = 'white';
					_this.ElementFormMain.colorActive.Color = 'white';
					_this.ElementFormMain.borderActive.Color = 'white';
					_this.ElementFormMain.bkgHover.Color = 'white';
					_this.ElementFormMain.colorHover.Color = 'white';
					_this.ElementFormMain.description.Text = '';
					_this.ElementFormMain.position.Text = '0';
					_this.ElementFormMain.icon.Text = '';
					_this.ElementFormMain.id.This.disabled = true;
					_this.ElementFormMain.title.This.disabled = true;
					_this.ElementFormMain.titleColor.Disabled = true;
					_this.ElementFormMain.bkgActive.Disabled = true;
					_this.ElementFormMain.colorActive.Disabled = true;
					_this.ElementFormMain.borderActive.Disabled = true;
					_this.ElementFormMain.bkgHover.Disabled = true;
					_this.ElementFormMain.colorHover.Disabled = true;
					_this.ElementFormMain.description.This.disabled = true;
					_this.ElementFormMain.position.This.disabled = true;
					_this.ElementFormMain.icon.This.disabled = true;
					_this.ElementFormMain.parent.This.disabled = true;
					$(_this.ElementContentSecundary.form).remove();
					/* Obtenci�n de nuevos Recursos */
					var PSProfiler = new Persistence.PanelService.TPanelServiceProfiler();
					PSProfiler.Fill();
					_this.PSGROUPList = PSProfiler.PSGROUPList;
					_this.PSMAINList = PSProfiler.PSMAINList;
					_this.PSSECUNDARYList = PSProfiler.PSSECUNDARYList;
					/*Eliminaci�n de Celdas de la tabla*/
					for (var i = 0; i < _this.ElementContentMain.table._Rows.length; i++) {
						$(_this.ElementContentMain.table._Rows[i].This).remove();
					}
					/*Llenado de Celdas de la tabla*/
					for (var i = 0; i < _this.PSGROUPList.length; i++) {
						if (_this.PSGROUPList[i].IDPSGROUP == parseInt(_this.ElementFormGroup.id.Text)) {
							_this.AddRowMain(_this.PSGROUPList[i].PSMAINList);
						}
					}
				}
				else {
					alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECTDELETE"));
				}
			}
		}
	} catch (e) {
		SysCfg.Log.Methods.WriteLog("PanelServicesBasic.js ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.DeleteMain", e);
	}
}

ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.AddRowMain = function (psList) {
	var _this = this.TParent();
	var PSList = psList;
	try {
		if (PSList.length != 0) {
			for (var j = 0; j < PSList.length; j++) {
				var NewRow = new TRow();
				var Cell0 = new TCell();
				Cell0.Value = PSList[j].MAIN_TITLE;
				Cell0.IndexColumn = 0;
				Cell0.IndexRow = j;
				NewRow.AddCell(Cell0);
				Cell0.This.setAttribute('data-id', PSList[j].IDPSMAIN);
				Cell0.This.classList.add('col-xs-12');
				_this.ElementRowMain[j] = { row: Cell0.This };
				_this.ElementContentMain.table.AddRow(NewRow);
				_this.ElementRowMain[j].row.onclick = function () {
					_this.ElementFormMain.id.This.disabled = true;
					_this.ElementFormMain.title.This.disabled = true;
					_this.ElementFormMain.titleColor.Disabled = true;;
					_this.ElementFormMain.bkgActive.Disabled = true;;
					_this.ElementFormMain.colorActive.Disabled = true;
					_this.ElementFormMain.borderActive.Disabled = true;
					_this.ElementFormMain.bkgHover.Disabled = true;
					_this.ElementFormMain.colorHover.Disabled = true;
					_this.ElementFormMain.description.This.disabled = true;
					_this.ElementFormMain.position.This.disabled = true;
					_this.ElementFormMain.icon.This.disabled = true;
					_this.ElementFormMain.parent.This.disabled = true;
					_this.ElementFormMain.save.This.style.display = 'none';
					_this.ElementFormMain.update.This.style.display = 'none';
					_this.ElementFormMain.cancel.This.style.visibility = 'hidden';
					_this.FillMain(parseInt(this.dataset.id));
				}
				// $(Cell0.This).css('max-width', '')
			}
			if (_this.ElementFormMain != null) {
				_this.ElementFormMain.iupdate.This.style.display = 'block';
				_this.ElementFormMain.idelete.This.style.display = 'block';
			}
			_this.ElementContentMain.table.ResponsiveCont.style.display = 'block';
			_this.ElementContentMain.table.EnabledResizeColumn = false;
			_this.ElementContentMain.formText.classList.remove('col-md-offset-2');
		}
		else {
			if (_this.ElementFormMain != null) {
				_this.ElementFormMain.iupdate.This.style.display = 'none';
				_this.ElementFormMain.idelete.This.style.display = 'none';
			}
			_this.ElementContentMain.table.ResponsiveCont.style.display = 'none';
			_this.ElementContentMain.table.EnabledResizeColumn = false;
			_this.ElementContentMain.formText.classList.add('col-md-offset-2');
		}
	} catch (e) {
		SysCfg.Log.Methods.WriteLog("PanelServicesBasic.js ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.AddRowMain", e);
	}
}

ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.SaveSecundary = function (titleSec, color, descriptionSec, positionSec, iconSec, phraseSec, idMain) {
	var _this = this.TParent();
	try {
		var PSSECUNDARY = new Persistence.PanelService.Properties.TPSSECUNDARY();
		PSSECUNDARY.SECUNDARY_TITLE = titleSec;
		PSSECUNDARY.SECUNDARY_TEXTCOLOR = color;
		PSSECUNDARY.SECUNDARY_DESCRIPTION = descriptionSec;
		PSSECUNDARY.SECUNDARY_POSITION = positionSec;
		PSSECUNDARY.SECUNDARY_ICON = iconSec;
		PSSECUNDARY.SECUNDARY_PHRASE = phraseSec;
		PSSECUNDARY.IDPSMAIN = idMain;
		var success = Persistence.PanelService.Methods.PSSECUNDARY_ADD(PSSECUNDARY);
		if (success) {
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "CORRECTSAVE"));
			/* Obtenci�n de nuevos Recursos */
			var PSProfiler = new Persistence.PanelService.TPanelServiceProfiler();
			PSProfiler.Fill();
			_this.PSGROUPList = PSProfiler.PSGROUPList;
			_this.PSMAINList = PSProfiler.PSMAINList;
			_this.PSSECUNDARYList = PSProfiler.PSSECUNDARYList;
			/*Limpiar Cajas*/
			_this.ElementFormSecundary.id.Text = '';
			_this.ElementFormSecundary.title.Text = '';
			_this.ElementFormSecundary.color.Color =  'white';
			_this.ElementFormSecundary.description.Text = '';
			_this.ElementFormSecundary.position.Text = '0';
			_this.ElementFormSecundary.icon.Text = '';
			_this.ElementFormSecundary.phrase.Text = '';
			_this.ElementFormSecundary.id.This.disabled = true;
			_this.ElementFormSecundary.title.This.disabled = true;
			_this.ElementFormSecundary.color.Disabled = true;
			_this.ElementFormSecundary.description.This.disabled = true;
			_this.ElementFormSecundary.position.This.disabled = true;
			_this.ElementFormSecundary.icon.This.disabled = true;
			_this.ElementFormSecundary.phrase.This.disabled = true;
			_this.ElementFormSecundary.parent.This.disabled = true;
			if (PSSECUNDARY.IDPSMAIN == parseInt(_this.ElementFormMain.id.Text)) {
				/*Eliminaci�n de Celdas de la tabla*/
				for (var i = 0; i < _this.ElementContentSecundary.table._Rows.length; i++) {
					$(_this.ElementContentSecundary.table._Rows[i].This).remove();
				}
				/*Llenado de Celdas de la tabla*/
				for (var i = 0; i < _this.PSMAINList.length; i++) {
					if (_this.PSMAINList[i].IDPSMAIN == PSSECUNDARY.IDPSMAIN) {
						_this.AddRowSecundary(_this.PSMAINList[i].PSSECUNDARYList);
					}
				}
			}
			else {
				//_this.ElementFormMain.id.Text = PSMAIN.IDPSMAIN;
				_this.ElementFormSecundary.id.This.disabled = true;
				_this.ElementFormSecundary.title.This.disabled = true;
				_this.ElementFormSecundary.color.Disabled = true;
				_this.ElementFormSecundary.description.This.disabled = true;
				_this.ElementFormSecundary.position.This.disabled = true;
				_this.ElementFormSecundary.icon.This.disabled = true;
				_this.ElementFormSecundary.phrase.This.disabled = true;
				_this.ElementFormSecundary.parent.This.disabled = true;
				_this.ElementFormSecundary.save.This.style.display = 'none';
				_this.ElementFormSecundary.update.This.style.display = 'none';
				_this.ElementFormSecundary.cancel.This.style.visibility = 'hidden';
				/*Regresar el combobox al parent de PSMAIN*/
				for (var j = 0; j < _this.ElementFormSecundary.parent.Options.length; j++) {
					if (parseInt(_this.ElementFormSecundary.parent.Options[j].Value) == parseInt(_this.ElementFormMain.id.Text)) {
						_this.ElementFormSecundary.parent.selectedIndex = j;
					}
				}
			}
		}
		else {
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECTSAVE"));
		}
	} catch (e) {
		SysCfg.Log.Methods.WriteLog("PanelServicesBasic.js ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.SaveSecundary", e);
	}
}

ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.UpdateSecundary = function (idSec, titleSec, color, descriptionSec, positionSec, iconSec, phraseSec, idMain) {
	var _this = this.TParent();
	try {
		var PSSECUNDARY = new Persistence.PanelService.Properties.TPSSECUNDARY();
		PSSECUNDARY.IDPSSECUNDARY = idSec;
		PSSECUNDARY.SECUNDARY_TITLE = titleSec;
		PSSECUNDARY.SECUNDARY_TEXTCOLOR = color;
		PSSECUNDARY.SECUNDARY_DESCRIPTION = descriptionSec;
		PSSECUNDARY.SECUNDARY_POSITION = positionSec;
		PSSECUNDARY.SECUNDARY_ICON = iconSec;
		PSSECUNDARY.SECUNDARY_PHRASE = phraseSec;
		PSSECUNDARY.IDPSMAIN = idMain;
		var success = Persistence.PanelService.Methods.PSSECUNDARY_UPD(PSSECUNDARY);
		if (success) {
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "CORRECTUPDATE"));
			_this.ElementFormSecundary.id.This.disabled = true;
			_this.ElementFormSecundary.title.This.disabled = true;
			_this.ElementFormSecundary.color.Disabled = true;
			_this.ElementFormSecundary.description.This.disabled = true;
			_this.ElementFormSecundary.position.This.disabled = true;
			_this.ElementFormSecundary.icon.This.disabled = true;
			_this.ElementFormSecundary.phrase.This.disabled = true;
			_this.ElementFormSecundary.parent.This.disabled = true;
			/* Obtenci�n de nuevos Recursos */
			var PSProfiler = new Persistence.PanelService.TPanelServiceProfiler();
			PSProfiler.Fill();
			_this.PSGROUPList = PSProfiler.PSGROUPList;
			_this.PSMAINList = PSProfiler.PSMAINList;
			_this.PSSECUNDARYList = PSProfiler.PSSECUNDARYList;
			/*Eliminaci�n de Celdas de la tabla*/
			for (var i = 0; i < _this.ElementContentSecundary.table._Rows.length; i++) {
				$(_this.ElementContentSecundary.table._Rows[i].This).remove();
			}
			/*Llenado de Celdas de la tabla*/
			for (var i = 0; i < _this.PSMAINList.length; i++) {
				if (_this.PSMAINList[i].IDPSMAIN == parseInt(_this.ElementFormMain.id.Text)) {
					_this.AddRowSecundary(_this.PSMAINList[i].PSSECUNDARYList);
				}
			}

			for (var j = 0; j < _this.ElementFormSecundary.parent.Options.length; j++) {
				if (parseInt(_this.ElementFormSecundary.parent.Options[j].Value) == parseInt(_this.ElementFormMain.id.Text)) {
					_this.ElementFormSecundary.parent.selectedIndex = j;
				}
			}

			if (PSSECUNDARY.IDPSMAIN != parseInt(_this.ElementFormMain.id.Text)) {
				_this.ElementFormSecundary.id.Text = '';
				_this.ElementFormSecundary.title.Text = '';
				_this.ElementFormSecundary.color.Color = 'white';
				_this.ElementFormSecundary.description.Text = '';
				_this.ElementFormSecundary.position.Text = '0';
				_this.ElementFormSecundary.icon.Text = '';
				_this.ElementFormSecundary.phrase.Text = '';
			}
		}
		else {
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECTUPDATE"));
		}
	} catch (e) {
		SysCfg.Log.Methods.WriteLog("PanelServicesBasic.js ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.UpdateSecundary", e);
	}
}

ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.DeleteSecundary = function (idSec) {
	var _this = this.TParent();
	try {
		var PSSECUNDARY = new Persistence.PanelService.Properties.TPSSECUNDARY();
		PSSECUNDARY.IDPSSECUNDARY = idSec;
		var success = Persistence.PanelService.Methods.PSSECUNDARY_DEL(PSSECUNDARY);
		if (success) {
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "CORRECTDELETE"));
			/*Limpiar Cajas*/
			_this.ElementFormSecundary.id.Text = '';
			_this.ElementFormSecundary.title.Text = '';
			_this.ElementFormSecundary.color.Color = 'white';
			_this.ElementFormSecundary.description.Text = '';
			_this.ElementFormSecundary.position.Text = '0';
			_this.ElementFormSecundary.icon.Text = '';
			_this.ElementFormSecundary.phrase.Text = '';
			_this.ElementFormSecundary.id.This.disabled = true;
			_this.ElementFormSecundary.color.Disabled = true;
			_this.ElementFormSecundary.title.This.disabled = true;
			_this.ElementFormSecundary.description.This.disabled = true;
			_this.ElementFormSecundary.position.This.disabled = true;
			_this.ElementFormSecundary.icon.This.disabled = true;
			_this.ElementFormSecundary.phrase.This.disabled = true;
			_this.ElementFormSecundary.parent.This.disabled = true;
			/* Obtenci�n de nuevos Recursos */
			var PSProfiler = new Persistence.PanelService.TPanelServiceProfiler();
			PSProfiler.Fill();
			_this.PSGROUPList = PSProfiler.PSGROUPList;
			_this.PSMAINList = PSProfiler.PSMAINList;
			_this.PSSECUNDARYList = PSProfiler.PSSECUNDARYList;
			/*Eliminaci�n de Celdas de la tabla*/
			for (var i = 0; i < _this.ElementContentSecundary.table._Rows.length; i++) {
				$(_this.ElementContentSecundary.table._Rows[i].This).remove();
			}
			/*Llenado de Celdas de la tabla*/
			for (var i = 0; i < _this.PSMAINList.length; i++) {
				if (_this.PSMAINList[i].IDPSMAIN == parseInt(_this.ElementFormMain.id.Text)) {
					_this.AddRowSecundary(_this.PSMAINList[i].PSSECUNDARYList);
				}
			}
		}
		else {
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECTDELETE"));
		}
	} catch (e) {
		SysCfg.Log.Methods.WriteLog("PanelServicesBasic.js ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.DeleteSecundary", e);
	}
}

ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.AddRowSecundary = function (psList) {
	var _this = this.TParent();
	var PSList = psList;
	try {
		if (PSList.length != 0) {
			for (var i = 0; i < PSList.length; i++) {
				var NewRow = new TRow();
				var Cell0 = new TCell();
				Cell0.Value = PSList[i].SECUNDARY_TITLE;
				Cell0.IndexColumn = 0;
				Cell0.IndexRow = i;
				NewRow.AddCell(Cell0);
				Cell0.This.setAttribute('data-id', PSList[i].IDPSSECUNDARY);
				Cell0.This.classList.add('col-xs-12');
				_this.ElementRowSecundary[i] = { row: Cell0.This };
				_this.ElementContentSecundary.table.AddRow(NewRow);
				_this.ElementRowSecundary[i].row.onclick = function () {
					_this.ElementFormSecundary.id.This.disabled = true;
					_this.ElementFormSecundary.title.This.disabled = true;
					_this.ElementFormSecundary.color.Disabled = true;;
					_this.ElementFormSecundary.description.This.disabled = true;
					_this.ElementFormSecundary.position.This.disabled = true;
					_this.ElementFormSecundary.icon.This.disabled = true;
					_this.ElementFormSecundary.phrase.This.disabled = true;
					_this.ElementFormSecundary.parent.This.disabled = true;
					_this.ElementFormSecundary.save.This.style.display = 'none';
					_this.ElementFormSecundary.update.This.style.display = 'none';
					_this.ElementFormSecundary.cancel.This.style.visibility = 'hidden';
					_this.FillSecundary(parseInt(this.dataset.id));
				}
			}
			if (_this.ElementFormSecundary != null) {
				_this.ElementFormSecundary.iupdate.This.style.display = 'block';
				_this.ElementFormSecundary.idelete.This.style.display = 'block';
			}
			_this.ElementContentSecundary.table.ResponsiveCont.style.display = 'block';
			_this.ElementContentSecundary.table.EnabledResizeColumn = false;
			_this.ElementContentSecundary.formText.classList.remove('col-md-offset-2');
		}
		else {
			if (_this.ElementFormSecundary != null) {
				_this.ElementFormSecundary.iupdate.This.style.display = 'none';
				_this.ElementFormSecundary.idelete.This.style.display = 'none';
			}
			_this.ElementContentSecundary.table.ResponsiveCont.style.display = 'none';
			_this.ElementContentSecundary.table.EnabledResizeColumn = false;
			_this.ElementContentSecundary.formText.classList.add('col-md-offset-2');
		}
	} catch (e) {
		SysCfg.Log.Methods.WriteLog("PanelServicesBasic.js ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.AddRowSecundary", e);
	}
}

ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.AddStyleFile = function (nombre, onSuccess, onError) {
	var style = document.createElement("link");
	style.rel = "stylesheet";
	style.type = "text/css";
	style.href = nombre;
	var s = document.head.appendChild(style);
	s.onload = onSuccess;
	s.onerror = onError;
}

ItHelpCenter.TPanelServicesManager.PanelServicesBasic.prototype.LoadScript = function (nombre, onSuccess, onError) {
	var s = document.createElement("script");
	s.onload = onSuccess;
	s.onerror = onError;
	s.src = nombre;
	document.querySelector("head").appendChild(s);
}

