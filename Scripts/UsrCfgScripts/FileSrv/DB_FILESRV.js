﻿//**********************   PROPERTIES   *****************************************************************************************
Persistence.GP.Properties.TFILESRV = function ()
{
    this.FILENAME = "";
    this.IDFILE = "";
    this.ATTACHNETDATE = "";
    this.DESCRIPTION = "";

    //Socket IO Properties
    this.ToByte = function (MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.FILENAME);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.IDFILE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.ATTACHNETDATE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.DESCRIPTION);

    }
    this.ByteTo = function (MemStream)
    {
        this.FILENAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDFILE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.ATTACHNETDATE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.DESCRIPTION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);

    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto)
    {
        SysCfg.Str.Protocol.WriteStr(this.FILENAME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.IDFILE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.ATTACHNETDATE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteStr(this.DESCRIPTION, Longitud, Texto);
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto)
    {
        this.FILENAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.IDFILE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.ATTACHNETDATE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.DESCRIPTION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);

    }
}

//**********************   METHODS   ********************************************************************************************
Persistence.GP.Methods.FILESRV_ListSetID = function (FILESRVList, IDFILESRV)
{
    for (i = 0; i < FILESRVList.length; i++)
    {
        if (IDFILESRV == FILESRVList[i].IDFILESRV)
            return (FILESRVList[i]);
    }
    var UnFILESRV = new Persistence.GP.Properties.TFILESRV;
    UnFILESRV.IDFILESRV = IDFILESRV;
    return (UnFILESRV);
}
Persistence.GP.Methods.FILESRV_ListAdd = function (FILESRVList, FILESRV)
{
    var i = Persistence.GP.Methods.FILESRV_ListGetIndex(FILESRVList, FILESRV);
    if (i == -1) FILESRVList.push(FILESRV);
    else
    {
        FILESRVList[i].FILENAME = FILESRV.FILENAME;
        FILESRVList[i].IDFILE = FILESRV.IDFILE;
        FILESRVList[i].ATTACHNETDATE = FILESRV.ATTACHNETDATE;
        FILESRVList[i].DESCRIPTION = FILESRV.DESCRIPTION;

    }
}

//CJRC_26072018
Persistence.GP.Methods.FILESRV_ListGetIndex = function (FILESRVList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = Persistence.GP.Methods.FILESRV_ListGetIndexIDFILESRV(FILESRVList, Param);

    }
    else {
        Res = Persistence.GP.Methods.FILESRV_ListGetIndexFILESRV(FILESRVList, Param);
    }
    return (Res);
}

Persistence.GP.Methods.FILESRV_ListGetIndexFILESRV = function (FILESRVList, FILESRV)
{
    for (i = 0; i < FILESRVList.length; i++)
    {
        if (FILESRV.IDFILESRV == FILESRVList[i].IDFILESRV)
            return (i);
    }
    return (-1);
}
Persistence.GP.Methods.FILESRV_ListGetIndexIDFILESRV = function (FILESRVList, IDFILESRV)
{
    for (i = 0; i < FILESRVList.length; i++)
    {
        if (IDFILESRV == FILESRVList[i].IDFILESRV)
            return (i);
    }
    return (-1);
}

//String Function 
Persistence.GP.Methods.FILESRVListtoStr = function (FILESRVList)
{
    var Res = Persistence.GP.Properties._Version + "(1)0";
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try
    {
        SysCfg.Str.Protocol.WriteInt(FILESRVList.length, Longitud, Texto);
        for (Counter = 0; Counter < FILESRVList.length; Counter++)
        {
            var UnFILESRV = FILESRVList[Counter];
            UnFILESRV.ToStr(Longitud, Texto);
        }

        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.GP.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e)
    {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
}
Persistence.GP.Methods.StrtoFILESRV = function (ProtocoloStr, FILESRVList)
{
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try
    {
        FILESRVList.length = 0;
        if (Persistence.GP.Properties._Version == ProtocoloStr.substring(0, 3))
        {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++)
            {
                UnFILESRV = new Persistence.GP.Properties.TFILESRV();
                UnFILESRV.StrTo(Pos, Index, LongitudArray, Texto);
                FILESRVList.push(UnFILESRV);
            }
        }
    }
    catch (e)
    {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}


//Socket Function 
Persistence.GP.Methods.FILESRVListToByte = function (FILESRVList, MemStream, idx)
{
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, FILESRVList.Count - idx);
    for (i = idx; i < FILESRVList.Count; i++)
    {
        FILESRVList[i].ToByte(MemStream);
    }
}
Persistence.GP.Methods.ByteToFILESRVList = function (FILESRVList, MemStream)
{
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++)
    {
        UnFILESRV = new Persistence.GP.Properties.TFILESRV();
        UnFILESRV.ByteTo(MemStream);
        Methods.FILESRV_ListAdd(FILESRVList, UnFILESRV);
    }
}
