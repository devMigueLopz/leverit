UsrCfg.InternoCommonNames = { 
    _Prefix : "Common", 
    TESTTABLE : { 
        NAME_TABLE : "TESTTABLE", 
        IDTESTTABLE : new SysCfg.DB.TField(), 
        FIELD_DATETIME : new SysCfg.DB.TField() 
    } 
} 
UsrCfg.InternoCommonNames.TESTTABLE.NAME_TABLE = "TESTTABLE"; 
UsrCfg.InternoCommonNames.TESTTABLE.IDTESTTABLE.FieldName = "IDTESTTABLE"; 
UsrCfg.InternoCommonNames.TESTTABLE.IDTESTTABLE.TableName = "TESTTABLE"; 
UsrCfg.InternoCommonNames.TESTTABLE.IDTESTTABLE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoCommonNames.TESTTABLE.IDTESTTABLE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoCommonNames.TESTTABLE.IDTESTTABLE);
UsrCfg.InternoCommonNames.TESTTABLE.NAME_TABLE = "TESTTABLE"; 
UsrCfg.InternoCommonNames.TESTTABLE.FIELD_DATETIME.FieldName = "FIELD_DATETIME"; 
UsrCfg.InternoCommonNames.TESTTABLE.FIELD_DATETIME.TableName = "TESTTABLE"; 
UsrCfg.InternoCommonNames.TESTTABLE.FIELD_DATETIME.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoCommonNames.TESTTABLE.FIELD_DATETIME.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoCommonNames.TESTTABLE.FIELD_DATETIME);
