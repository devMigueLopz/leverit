﻿//UsrCfg.SC.Methods

UsrCfg.SC.Methods.GET_SDGROUPSERVICEUSER = function(IDCMDBCI,SDGROUPSERVICEUSERCI_LIST)
{
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCI.FieldName, IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //Param.AddUnknown(UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCI.FieldName, "MDGROUPUSER."+UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCI.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //*********** SDGROUPSERVICEUSER_GET ***************************** 
        var DS_SDGROUPSERVICEUSER = SysCfg.DB.SQL.Methods.OpenDataSet("DataLink", "SDGROUPSERVICEUSER_IDCI", Param.ToBytes());                
        if (DS_SDGROUPSERVICEUSER.ResErr.NotError)
        {
            if (DS_SDGROUPSERVICEUSER.DataSet.RecordCount > 0)
            {
                DS_SDGROUPSERVICEUSER.DataSet.First();
                while (!(DS_SDGROUPSERVICEUSER.DataSet.Eof)) 
                {
                    var SDGROUPSERVICEUSERCI = new UsrCfg.SC.TSDGROUPSERVICEUSERCI();
                    SDGROUPSERVICEUSERCI.IDSDTYPEUSER = DS_SDGROUPSERVICEUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDGROUPSERVICEUSER.IDSDTYPEUSER.FieldName).asInt32();
                    SDGROUPSERVICEUSERCI.TYPEUSERNAME = DS_SDGROUPSERVICEUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDTYPEUSER.TYPEUSERNAME.FieldName).asString();
                    SDGROUPSERVICEUSERCI.IDMDSERVICETYPE = DS_SDGROUPSERVICEUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDGROUPSERVICEUSER.IDMDSERVICETYPE.FieldName).asInt32();
                    SDGROUPSERVICEUSERCI.SERVICETYPENAME = DS_SDGROUPSERVICEUSER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICETYPE.SERVICETYPENAME.FieldName).asString();
                    SDGROUPSERVICEUSERCI.COUNTITEM = DS_SDGROUPSERVICEUSER.DataSet.RecordSet.FieldName("COUNTITEM").asInt32();
                    SDGROUPSERVICEUSERCI_LIST.push(SDGROUPSERVICEUSERCI);
                    DS_SDGROUPSERVICEUSER.DataSet.Next(); 
                } 
            }
            else
            {
                DS_SDGROUPSERVICEUSER.ResErr.NotError = false;
                DS_SDGROUPSERVICEUSER.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
}

UsrCfg.SC.Methods.GetRole = function(idRole)
{
    UsrCfg.SC.Properties.ATROLE_LIST = UsrCfg.SC.Methods.GetRoleList();
    for (var i = 0; i < UsrCfg.SC.Properties.ATROLE_LIST.length; i++) 
    {
        var UnATRole = UsrCfg.SC.Properties.ATROLE_LIST[i];
        if (UnATRole.IDATROLE == idRole)
        {
            return UnATRole;
        }
    }
    return null;
}

UsrCfg.SC.Methods.GetRoleList = function()
{
    var ATROLE_LIST = new Array();//TATROLE
    try
    {
        var DS_ATROLE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "ATROLE_SELECTALL", new Array());
        if (DS_ATROLE.ResErr.NotError)
        {
            if (DS_ATROLE.DataSet.RecordCount > 0)
            {
                DS_ATROLE.DataSet.First();
                while (!(DS_ATROLE.DataSet.Eof))
                {
                    var ATROLE = new UsrCfg.SC.TATROLE();
                    ATROLE.IDATROLE = DS_ATROLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ATROLE.IDATROLE.FieldName).asInt32();
                    ATROLE.ROLENAME = DS_ATROLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ATROLE.ROLENAME.FieldName).asString();
                    ATROLE.IDPSGROUP = DS_ATROLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ATROLE.IDPSGROUP.FieldName).asInt32();
                    ATROLE.IDPBITEMPLATE = DS_ATROLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ATROLE.IDPBITEMPLATE.FieldName).asInt32();
                    ATROLE.SETHOME = DS_ATROLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ATROLE.SETHOME.FieldName).asInt32();
                    ATROLE.REMOTEHELPCONFIG = DS_ATROLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ATROLE.REMOTEHELPCONFIG.FieldName).asString();
                    ATROLE.RHCONFIG = new Persistence.RemoteHelp.RHCONFIG(ATROLE.REMOTEHELPCONFIG);
                    ATROLE.PBITEMPLATEMULTIPLE = DS_ATROLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ATROLE.PBITEMPLATEMULTIPLE.FieldName).asString();
                    ATROLE.PSGROUPMULTIPLE = DS_ATROLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ATROLE.PSGROUPMULTIPLE.FieldName).asString();

                    //CARGA LISTA DE MODULOS
                    ATROLE.ATROLEMODULE_LIST = UsrCfg.SC.Methods.GetRoleModules(ATROLE.IDATROLE);
                    //CARGA LISTA DE QUERIES
                    ATROLE.ATROLEGPQUERY_LIST = UsrCfg.SC.Methods.GetRoleQueries(ATROLE.IDATROLE);
                    //CARGA LISTA DE PBI TEMPLATES
                    ATROLE.ATROLEPBITEMPLATE_LIST = UsrCfg.SC.Methods.GetRolePBITemplates(ATROLE.IDATROLE);
                    //CARGA LISTA DE PANEL SERVICS
                    ATROLE.ATROLEPSGROUP_LIST = UsrCfg.SC.Methods.GetRolePanelServices(ATROLE.IDATROLE);

                    //CARGA LISTA DE PS GROUPS
                    ATROLE.PSGROUP_ALLLIST = UsrCfg.SC.Methods.GetPSGROUPS();

                    
                    //CARGA LISTA DE DASHBOARD
                    //#if SILVERLIGHT
                    //   ATROLE.ATROLEDASHBOARD_LIST = UsrCfg.SC.Methods.GetRoleDashBoard(ATROLE.IDATROLE);
                    //#endif
                    ATROLE_LIST.push(ATROLE);

                    DS_ATROLE.DataSet.Next();
                }
            }
            else
            {
                DS_ATROLE.ResErr.NotError = false;
                DS_ATROLE.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
    }
    return ATROLE_LIST;
}


UsrCfg.SC.Methods.GetRoleModules = function(idRole)
{
    var ATROLEMODULE_LIST = new Array(); //TATROLEMODULE
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.ATROLEMODULE.IDATROLE.FieldName, idRole, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var DS_ATROLEMODULE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "ATROLEMODULE_SELECTBYROLE", Param.ToBytes());
        if (DS_ATROLEMODULE.ResErr.NotError)
        {
            if (DS_ATROLEMODULE.DataSet.RecordCount > 0)
            {
                DS_ATROLEMODULE.DataSet.First();
                while (!(DS_ATROLEMODULE.DataSet.Eof)) 
                {
                    var ATROLEMODULE = new UsrCfg.SC.TATROLEMODULE();
                            
                    ATROLEMODULE.IDATROLEMODULE = DS_ATROLEMODULE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ATROLEMODULE.IDATROLEMODULE.FieldName).asInt32();
                    ATROLEMODULE.IDATROLE = DS_ATROLEMODULE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ATROLEMODULE.IDATROLE.FieldName).asInt32();
                    ATROLEMODULE.IDATMODULE = DS_ATROLEMODULE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ATROLEMODULE.IDATMODULE.FieldName).asInt32();
                    ATROLEMODULE.REQUEST = DS_ATROLEMODULE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ATROLEMODULE.REQUEST.FieldName).asString();
                    ATROLEMODULE_LIST.push(ATROLEMODULE);
                    DS_ATROLEMODULE.DataSet.Next();
                } 
            }
            else
            {
                DS_ATROLEMODULE.ResErr.NotError = false;
                DS_ATROLEMODULE.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return ATROLEMODULE_LIST;
}


UsrCfg.SC.Methods.GetRoleQueries = function(idRole)
{
    var ATROLEGPQUERY_LIST = new Array();//TATROLEGPQUERY
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.ATROLEGPQUERY.IDATROLE.FieldName, idRole, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var DS_ATROLEGPQUERY = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "ATROLEGPQUERY_SELECTBYROLE", Param.ToBytes());
        if (DS_ATROLEGPQUERY.ResErr.NotError)
        {
            if (DS_ATROLEGPQUERY.DataSet.RecordCount > 0)
            {
                DS_ATROLEGPQUERY.DataSet.First();
                while (!(DS_ATROLEGPQUERY.DataSet.Eof)) 
                {
                    var ATROLEGPQUERY = new UsrCfg.SC.TATROLEGPQUERY();
                    ATROLEGPQUERY.IDGPQUERYTYPE =  DS_ATROLEGPQUERY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.GPQUERY.IDGPQUERYTYPE.FieldName).asInt32();
                    ATROLEGPQUERY.IDATROLE = DS_ATROLEGPQUERY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ATROLEGPQUERY.IDATROLE.FieldName).asInt32();
                    ATROLEGPQUERY.IDGPQUERY = DS_ATROLEGPQUERY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ATROLEGPQUERY.IDGPQUERY.FieldName).asInt32();
                    ATROLEGPQUERY.GPQUERY = Persistence.GP.Methods.GPQUERY_GETBYID(ATROLEGPQUERY.IDGPQUERY);
                    ATROLEGPQUERY_LIST.push(ATROLEGPQUERY);
                    DS_ATROLEGPQUERY.DataSet.Next();
                } 
            }
            else
            {
                DS_ATROLEGPQUERY.ResErr.NotError = false;
                DS_ATROLEGPQUERY.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return ATROLEGPQUERY_LIST;
}


UsrCfg.SC.Methods.GetRolePBITemplates = function (idRole)
{
    var ATROLEPBITEMPLATE_LIST = new Array();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.ATROLEPBITEMPLATE.IDATROLE.FieldName, idRole, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var openSelect = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "ATROLEPBITEMPLATE_SELECTBYROLE", Param.ToBytes());
        if (openSelect.ResErr.NotError)
        {
            if (openSelect.DataSet.RecordCount > 0) 
            {
                openSelect.DataSet.First();
                while (!(openSelect.DataSet.Eof))
                {
                    var ATROLEPBITEMPLATE = new UsrCfg.SC.TATROLEPBITEMPLATE();

                    ATROLEPBITEMPLATE.IDATROLEPBITEMPLATE = openSelect.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ATROLEPBITEMPLATE.IDATROLEPBITEMPLATE.FieldName).asInt32();
                    ATROLEPBITEMPLATE.IDATROLE = openSelect.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ATROLEPBITEMPLATE.IDATROLE.FieldName).asInt32();
                    ATROLEPBITEMPLATE.IDPBITEMPLATE = openSelect.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ATROLEPBITEMPLATE.IDPBITEMPLATE.FieldName).asInt32();

                    ATROLEPBITEMPLATE.TEMPLATE_NAME = openSelect.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_NAME.FieldName).asString();
                    ATROLEPBITEMPLATE.TEMPLATE_DESCRIPTION = openSelect.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_DESCRIPTION.FieldName).asString();
                    ATROLEPBITEMPLATE.TEMPLATE_PATH = openSelect.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_PATH.FieldName).asString();

                    ATROLEPBITEMPLATE_LIST.push(ATROLEPBITEMPLATE);

                    openSelect.DataSet.Next();
                }
            }
            else {
                openSelect.ResErr.NotError = false;
                openSelect.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
    return ATROLEPBITEMPLATE_LIST;
}


UsrCfg.SC.Methods.GetRolePanelServices = function (idRole)
{
    var ATROLEPSGROUP_LIST = new Array();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.ATROLEPSGROUP.IDATROLE.FieldName, idRole, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var openSelect = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "ATROLEPSGROUP_SELECTBYROLE", Param.ToBytes());
        if (openSelect.ResErr.NotError)
        {
            if (openSelect.DataSet.RecordCount > 0)
            {
                openSelect.DataSet.First();
                while (!(openSelect.DataSet.Eof))
                {
                    var ATROLEPSGROUP = new UsrCfg.SC.TATROLEPSGROUP();

                    ATROLEPSGROUP.IDATROLEPSGROUP = openSelect.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ATROLEPSGROUP.IDATROLEPSGROUP.FieldName).asInt32();
                    ATROLEPSGROUP.IDATROLE = openSelect.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ATROLEPSGROUP.IDATROLE.FieldName).asInt32();
                    ATROLEPSGROUP.IDPSGROUP = openSelect.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ATROLEPSGROUP.IDPSGROUP.FieldName).asInt32();

                    ATROLEPSGROUP.GROUP_NAME = openSelect.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSGROUP.GROUP_NAME.FieldName).asString();
                    ATROLEPSGROUP.GROUP_DESCRIPTION = openSelect.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSGROUP.GROUP_DESCRIPTION.FieldName).asString();
                    ATROLEPSGROUP.GROUP_PATH = openSelect.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSGROUP.GROUP_PATH.FieldName).asString();

                    ATROLEPSGROUP_LIST.push(ATROLEPSGROUP);

                    openSelect.DataSet.Next();
                }
            }
            else
            {
                openSelect.ResErr.NotError = false;
                openSelect.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return ATROLEPSGROUP_LIST;
}


UsrCfg.SC.Methods.GetPSGROUPS = function ()
{
    var PSGROUP_ALLLIST = new Array();
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        var openSelect = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "PSGROUP_GET", Param.ToBytes());
        if (openSelect.ResErr.NotError)
        {
            if (openSelect.DataSet.RecordCount > 0)
            {
                openSelect.DataSet.First();
                while (!(openSelect.DataSet.Eof))
                {
                    var PSGROUP = new UsrCfg.SC.TPSGROUP();
                    PSGROUP.IDPSGROUP = openSelect.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSGROUP.IDPSGROUP.FieldName).asInt32();
                    PSGROUP.GROUP_NAME = openSelect.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.PSGROUP.GROUP_NAME.FieldName).asString();

                    PSGROUP_ALLLIST.push(PSGROUP);

                    openSelect.DataSet.Next();
                }
            }
            else
            {
                openSelect.ResErr.NotError = false;
                openSelect.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return PSGROUP_ALLLIST;
}

//#if SILVERLIGHT

//UsrCfg.SC.Methods.GetRoleDashBoard = function( idRole)
//{
//    var ATROLEDASHBOARD_LIST = new List<TATROLEDASHBOARD>();
//    var Param = new SysCfg.Stream.Properties.TParam();
//    try
//    {
//        Param.Inicialize();
//        Param.AddInt32(UsrCfg.InternoAtisNames.ATROLEDASHBOARD.IDATROLE.FieldName, idRole, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
//        var DS_ATROLEDASHBOARD = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "ATROLEDASHBOARD_SELECTBYROLE", Param.ToBytes());
//        if (DS_ATROLEDASHBOARD.ResErr.NotError)
//        {
//            if (DS_ATROLEDASHBOARD.DataSet.RecordCount > 0)
//            {
//                DS_ATROLEDASHBOARD.DataSet.First();
//                while (!(DS_ATROLEDASHBOARD.DataSet.Eof))
//                {
//                    var ATROLEDASHBOARD = new UsrCfg.SC.TATROLEDASHBOARD();
//                    ATROLEDASHBOARD.IDATROLEDASHBOARD = DS_ATROLEDASHBOARD.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ATROLEDASHBOARD.IDATROLEDASHBOARD.FieldName).asInt32();
//                    ATROLEDASHBOARD.IDATROLE = DS_ATROLEDASHBOARD.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ATROLEDASHBOARD.IDATROLE.FieldName).asInt32();
//                    ATROLEDASHBOARD.IDATROLEGPQUERYGROUPLAYOUT = DS_ATROLEDASHBOARD.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.ATROLEDASHBOARD.IDATROLEGPQUERYGROUPLAYOUT.FieldName).asInt32();
//                    ATROLEDASHBOARD.ATROLEGPQUERYGROUPLAYOUT = ATROLEGPQUERYGROUPLAYOUT_METHODS.GetById(ATROLEDASHBOARD.IDATROLEGPQUERYGROUPLAYOUT);
//                    ATROLEDASHBOARD_LIST.push(ATROLEDASHBOARD);

//                    DS_ATROLEDASHBOARD.DataSet.Next();
//                }
//            }
//            else
//            {
//                DS_ATROLEDASHBOARD.ResErr.NotError = false;
//                DS_ATROLEDASHBOARD.ResErr.Mesaje = "Record Count = 0";
//            }
//        }
//    }
//    finally
//    {
//        Param.Destroy();
//    }
//    return ATROLEDASHBOARD_LIST;
//}
//#endif

