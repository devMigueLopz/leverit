﻿//UsrCfg.SC

UsrCfg.SC.TUserATRole.prototype.HasMenuPermission = function (module) {
    var _this = this.TParent();
    var resp = false;

    for (var i = 0; i < UsrCfg.Properties.UserATRole.ATROLE.ATROLEMODULE_LIST.length; i++) {
        var menu = UsrCfg.Properties.UserATRole.ATROLE.ATROLEMODULE_LIST[i];
        if (menu.IDATMODULE == Convert.ToInt32(module)) {
            resp = true;
            break;
        }
    }
    return resp;
}

UsrCfg.SC.TATROLEGPQUERY = function () {
    this.IDGPQUERYTYPE = 0;
    this.IDATROLEGPQUERY = 0;
    this.IDATROLE = 0;
    this.IDGPQUERY = 0;
    this.GPQUERY = new Persistence.GP.Properties.TGPQUERY();
}


UsrCfg.SC.TATROLEDASHBOARD = function () {
    this.IDATROLEDASHBOARD = 0;
    this.IDATROLE = 0;
    this.IDATROLEGPQUERYGROUPLAYOUT = 0;
    this.ATROLEGPQUERYGROUPLAYOUT = ItHelpCenter.SC.ATROLEGPQUERYGROUPLAYOUT;
}


