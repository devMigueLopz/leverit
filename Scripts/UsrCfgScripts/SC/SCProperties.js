﻿//UsrCfg.SC.Properties

UsrCfg.SC.Properties.TModules =
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    //PermisoMenu
    Atis_Define: { value: 1, name: "Atis_Define" },
    Atis_Add_User: { value: 2, name: "Atis_Add_User" },
    Atis_Advanced_Mapping: { value: 3, name: "Atis_Advanced_Mapping" },
    Atis_Managers_Informed: { value: 4, name: "Atis_Managers_Informed" },
    Atis_Handler: { value: 5, name: "Atis_Handler" },
    Atis_Other_jobs: { value: 6, name: "Atis_Other_jobs" },
    Atis_Basic: { value: 7, name: "Atis_Basic" },
    Atis_Custom: { value: 8, name: "Atis_Custom" },
    Atis_SLA: { value: 9, name: "Atis_SLA" },
    Atis_Category: { value: 10, name: "Atis_Category" },
    Atis_Timetable: { value: 11, name: "Atis_Timetable" },
    Atis_Calendar: { value: 12, name: "Atis_Calendar" },
    Atis_Edit_all_user: { value: 13, name: "Atis_Edit_all_user" },
    Atis_Security_Manager: { value: 14, name: "Atis_Security_Manager" },
    Atis_Main_menu: { value: 15, name: "Atis_Main_menu" },
    Atis_Custom_console: { value: 16, name: "Atis_Custom_console" },
    Atis_Extra_queries: { value: 17, name: "Atis_Extra_queries" },
    Atis_Edit_all_queries: { value: 18, name: "Atis_Edit_all_queries" },
    Atis_Interface: { value: 19, name: "Atis_Interface" },
    CMDB_ExtraTable: { value: 20, name: "CMDB_ExtraTable" },
    CMDB_ExtraTableService: { value: 21, name: "CMDB_ExtraTableService" },
    Atis_NotifyEmail: { value: 22, name: " Atis_NotifyEmail" },
    Atis_CIView: { value: 23, name: "Atis_CIView" },
    Atis_SQL: { value: 24, name: "Atis_SQL" },
    Config_Verification: { value: 25, name: "Config_Verification" },
    Atis_Urgency: { value: 26, name: "Atis_Urgency" },
    Atis_Priority: { value: 27, name: "Atis_Priority" },
    Atis_Impact: { value: 28, name: "Atis_Impact" },
    Atis_PriorityMatrix: { value: 29, name: "Atis_PriorityMatrix" },
    Atis_DefaultConfig: { value: 30, name: "Atis_DefaultConfig" },
    Atis_RemoteControl: { value: 31, name: "Atis_RemoteControl" },
    Atis_RemotePolicy: { value: 32, name: "Atis_RemotePolicy" },
    Atis_GraphicsPBI: { value: 33, name: "Atis_GraphicsPBI" },
    Atis_PanelService: { value: 34, name: "Atis_PanelService" },
}

UsrCfg.SC.Properties.TSYSTEMSTATUS =
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _Prep: { value: 0, name: "Prep" },
    _Live: { value: 1, name: "Live" },
    _Retired: { value: 2, name: "Retired" },
}
UsrCfg.SC.Properties.TATMODULE =
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _GPQUERY: { value: 0, name: "GPQUERY" },
    _MDSCHEDULER: { value: 1, name: "MDSCHEDULER" },
    _ATCONFIG: { value: 2, name: "ATCONFIG" },
}

UsrCfg.SC.Properties.ATROLE_LIST = new Array(); //UsrCfg.SC.TATROLE
UsrCfg.SC.Properties.CMDBUSER_LIST = new Array();//UsrCfg.SC.TUserATRole



UsrCfg.SC.TSDGROUPSERVICEUSERCI = function () {
    this.IDSDTYPEUSER = 0;
    this.TYPEUSERNAME = "";
    this.IDMDSERVICETYPE = 0;
    this.SERVICETYPENAME = "";
    this.COUNTITEM = 0;
}

UsrCfg.SC.TATROLE = function () {
    this.IDATROLE = 0;
    this.ROLENAME = "";
    this.IDPSGROUP = 0;
    this.IDPBITEMPLATE = 0;
    this.SETHOME = 0;
    this.PBITEMPLATEMULTIPLE = "";
    this.PSGROUPMULTIPLE = "";
    this.REMOTEHELPCONFIG = 0;
    this.RHCONFIG = new Persistence.RemoteHelp.RHCONFIG();
    this.ATROLEMODULE_LIST = new Array();//TATROLEMODULE
    this.ATROLEGPQUERY_LIST = new Array();//TATROLEGPQUERY
    this.ATROLEPBITEMPLATE_LIST = new Array();
    this.ATROLEPSGROUP_LIST = new Array();
    this.PSGROUP_ALLLIST = new Array();
    //#if SILVERLIGHT
    //    this.ATROLEDASHBOARD_LIST = new Array();//TATROLEDASHBOARD
    //#endif
}

UsrCfg.SC.TUserATRole = function () {
    this.TParent = function () {
        return this;
    }.bind(this);

    this.User = new SysCfg.App.TUser;
    this.isAtention = false;
    this.ATROLE = new UsrCfg.SC.TATROLE;
    this.SDGROUPSERVICEUSERCI_LIST = new Array()//TSDGROUPSERVICEUSERCI
    //this.MDSERVICETYPEList = new Array();//Persistence.SMCI.Properties.TMDSERVICETYPE
    //this.SDTYPEUSERList = new Array();//Persistence.SMCI.Properties.TSDTYPEUSER


    this.isServiceDesk = function () {
        var resp = false;
        for (var i = 0; i < this.SDGROUPSERVICEUSERCI_LIST.length; i++) {
            var SDGROUPSERVICEUSERCI = this.SDGROUPSERVICEUSERCI_LIST[i]
            if ((SDGROUPSERVICEUSERCI.IDSDTYPEUSER == UsrCfg.SD.Properties.TSDTypeUser._Owner.value) &&
                (SDGROUPSERVICEUSERCI.IDMDSERVICETYPE == UsrCfg.SD.Properties.TMDSERVICETYPE._Incident.value) ||
                (SDGROUPSERVICEUSERCI.IDMDSERVICETYPE == UsrCfg.SD.Properties.TMDSERVICETYPE._Problem.value)) {
                resp = true;
                break;
            }
        }
        return resp;
    }

    this.isUserIncident = function () {
        var resp = false;
        for (var i = 0; i < this.SDGROUPSERVICEUSERCI_LIST.length; i++) {
            var SDGROUPSERVICEUSERCI = this.SDGROUPSERVICEUSERCI_LIST[i];
            if ((SDGROUPSERVICEUSERCI.IDSDTYPEUSER == UsrCfg.SD.Properties.TSDTypeUser._User.value) &&
                (SDGROUPSERVICEUSERCI.IDMDSERVICETYPE == UsrCfg.SD.Properties.TMDSERVICETYPE._Incident.value)) {
                resp = true;
                break;
            }
        }
        return resp;
    }

    this.isProblemManager = function () {
        var resp = false;
        for (var i = 0; i < this.SDGROUPSERVICEUSERCI_LIST.length; i++) {
            var SDGROUPSERVICEUSERCI = this.SDGROUPSERVICEUSERCI_LIST[i];
            if ((SDGROUPSERVICEUSERCI.IDSDTYPEUSER == UsrCfg.SD.Properties.TSDTypeUser._Owner.value) &&
                (SDGROUPSERVICEUSERCI.IDMDSERVICETYPE == UsrCfg.SD.Properties.TMDSERVICETYPE._knownerrors.value) ||
                (SDGROUPSERVICEUSERCI.IDMDSERVICETYPE == UsrCfg.SD.Properties.TMDSERVICETYPE._Workaround.value)) {
                resp = true;
                break;
            }
        }
        return resp;
    }

    this.isManager = function ()//verificado
    {
        var resp = false;
        for (var i = 0; i < this.SDGROUPSERVICEUSERCI_LIST.length; i++) {
            var SDGROUPSERVICEUSERCI = this.SDGROUPSERVICEUSERCI_LIST[i];
            if (!(
                (SDGROUPSERVICEUSERCI.IDSDTYPEUSER == UsrCfg.SD.Properties.TSDTypeUser._System.value) ||
                (SDGROUPSERVICEUSERCI.IDSDTYPEUSER == UsrCfg.SD.Properties.TSDTypeUser._User.value)
            )) {
                resp = true;
                break;
            }
        }
        return resp;
    }


    this.isSDTYPEUSER = function (IDSDTYPEUSER) {
        var resp = false;
        for (var i = 0; i < this.SDGROUPSERVICEUSERCI_LIST.length; i++) {
            var SDGROUPSERVICEUSERCI = this.SDGROUPSERVICEUSERCI_LIST[i];
            if (SDGROUPSERVICEUSERCI.IDSDTYPEUSER == IDSDTYPEUSER.value) {
                resp = true;
                break;
            }
        }
        return resp;
    }

}

UsrCfg.SC.TATROLEMODULE = function () {
    this.IDATROLEMODULE = 0;
    this.IDATROLE = 0;
    this.IDATMODULE = 0;
    this.ATMODULE = new UsrCfg.SC.Properties.TATMODULE.GetEnum(undefined);
    this.REQUEST = "";
}

UsrCfg.SC.TATROLEPBITEMPLATE = function () {
    this.IDATROLEPBITEMPLATE = 0;
    this.IDATROLE = 0;
    this.IDPBITEMPLATE = 0;
    //Virtual
    this.TEMPLATE_NAME = "";
    this.TEMPLATE_DESCRIPTION = "";
    this.TEMPLATE_PATH = "";

}

UsrCfg.SC.TATROLEPSGROUP = function ()
{
    this.IDATROLEPSGROUP = 0;
    this.IDATROLE = 0;
    this.IDPSGROUP = 0;
    //Virtual
    this.GROUP_NAME = "";
    this.GROUP_DESCRIPTION = "";
    this.GROUP_PATH = "";
}

UsrCfg.SC.TPBITEMPLATE = function ()
{
    this.IDPBITEMPLATE = 0;
    this.TEMPLATE_NAME = "";
    this.TEMPLATE_DESCRIPTION = "";
    this.TEMPLATE_PATH = "";

}

UsrCfg.SC.TPSGROUP = function ()
{
    this.IDPSGROUP = 0;
    this.GROUP_NAME = "";
}


