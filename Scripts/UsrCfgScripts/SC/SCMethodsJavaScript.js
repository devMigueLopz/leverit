﻿UsrCfg.SC.Methods.InitializeUser = function(inUser)
{
    UsrCfg.Properties.UserATRole.User = inUser;                                         
    UsrCfg.Properties.UserATRole.ATROLE = UsrCfg.SC.Methods.GetRole(UsrCfg.Properties.UserATRole.User.IDATROLE);
    UsrCfg.Properties.UserATRole.SDGROUPSERVICEUSERCI_LIST = new Array();//Clear();

    Persistence.Profiler.Initialize(UsrCfg.Properties.UserATRole);
    Persistence.Profiler.UserFunctions.User = UsrCfg.Properties.UserATRole.User;
    UsrCfg.SC.Methods.GET_SDGROUPSERVICEUSER(UsrCfg.Properties.UserATRole.User.IDCMDBCI, UsrCfg.Properties.UserATRole.SDGROUPSERVICEUSERCI_LIST);
 }
