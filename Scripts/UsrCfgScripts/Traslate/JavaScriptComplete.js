﻿//UsrCfg.Traslate



UsrCfg.Traslate.GetLangText = function () {
    this.TParent = function () {
        return this;
    }.bind(this);

    var _this = this.TParent();
    var LangText = "";
    if (arguments.length == 3) {
        Obj = arguments[0];
        IDSUB = arguments[1];
        LangTextDefault = arguments[2];
        LangText = UsrCfg.Traslate.GetLangTextDefine(Obj, IDSUB, LangTextDefault);
    }
    else if (arguments.length == 2) {
        Obj = arguments[0];
        IDSUB = arguments[1];
        LangText = UsrCfg.Traslate.GetLangTextUse(Obj, IDSUB);
    }
    else {
        LangText = "Param error";
    }
    return LangText;
    
}

UsrCfg.Traslate.GetLangTextUse = function (Obj, IDSUB) {
    var ID = SysCfg.Str.Methods.GetStrLastChar(Obj, '.');
    var Res = UsrCfg.Traslate.SysCfg_GetLangText(ID, IDSUB, "");
    return Res;

}
UsrCfg.Traslate.GetLangTextDefine = function (Obj, IDSUB, LangTextDefault) {
    var ID = SysCfg.Str.Methods.GetStrLastChar(Obj, '.');
    var Res = UsrCfg.Traslate.SysCfg_GetLangText(ID, IDSUB, LangTextDefault);
    //if (Res!=LangTextDefault)   MessageBox.Show("Actualiza Upgrade Text  ID:" + ID + " IDSUB:" + IDSUB + "  LangTextDefault:" + LangTextDefault + "-->" + Res);            
    return Res;
}


UsrCfg.Traslate.SysCfg_GetLangText = function (ID, IDSUB, LangTextDefault) {
    var Res = SysCfg.IDI.Client.GetLangText(SysCfg.App.Properties.Application, ID, IDSUB, LangTextDefault, SysCfg.App.Properties.Config_Language.LanguageITF);//Persistence.Profiler.UserFunctions.CMDBUSERSETTINGS.IDLANGUAGE
    //return "xAtx" + Res + "xxx";
    return Res;
}


UsrCfg.Traslate.GetLangTextdbExtra = function (IDSUB, LangTextDefault) {
    var Res = SysCfg.IDI.Client.GetLangText(SysCfg.App.TApplication.Common, "db.EXTRA", IDSUB, LangTextDefault, SysCfg.App.Properties.Config_Language.LanguageITF);//Persistence.Profiler.UserFunctions.CMDBUSERSETTINGS.IDLANGUAGE
    //return "xCmExx" + Res + "xxx";
    return Res;
}


UsrCfg.Traslate.GetLangTextdb = function (IDSUB, LangTextDefault) {
    var Res = LangTextDefault;
    var isdbfield = false;
    for (var i = 0; i < SysCfg.DB.Properties.Field_list.length; i++) {
        if (SysCfg.Str.isEqual(SysCfg.DB.Properties.Field_list[i].FieldName,LangTextDefault)) {
            Res = SysCfg.DB.Properties.Field_list[i].Header;
            isdbfield = true;
            break;
        }
    }
    if (!isdbfield) {
        Res = UsrCfg.Traslate.GetLangTextdbExtra(IDSUB, LangTextDefault);
    }
    return Res;

}

