UsrCfg.InternoDataLinkNames = { 
    _Prefix : "DataLink", 
    SMCHATASSIGNED : { 
        NAME_TABLE : "SMCHATASSIGNED", 
        IDSMCHATASSIGNED : new SysCfg.DB.TField(), 
        ASSIGNED_ENABLE : new SysCfg.DB.TField(), 
        ASSIGNED_TITLE : new SysCfg.DB.TField(), 
        ASSIGNED_DATE : new SysCfg.DB.TField(), 
        IDSMCHATASSIGNEDTYPE : new SysCfg.DB.TField() 
    }, 
    SMCHATASSIGNEDMEMBER : { 
        NAME_TABLE : "SMCHATASSIGNEDMEMBER", 
        IDSMCHATASSIGNEDMEMBER : new SysCfg.DB.TField(), 
        IDSMCHATASSIGNED : new SysCfg.DB.TField(), 
        IDCMDBCI_MEMBER : new SysCfg.DB.TField(), 
        DATETIME_MEMBER : new SysCfg.DB.TField(), 
        ENABLE_MEMBER : new SysCfg.DB.TField(), 
        CREATE_MEMBER : new SysCfg.DB.TField() 
    }, 
    SMCHAT : { 
        NAME_TABLE : "SMCHAT", 
        IDSMCHAT : new SysCfg.DB.TField(), 
        IDSMCHATASSIGNED : new SysCfg.DB.TField(), 
        IDCMDBCI_SEND : new SysCfg.DB.TField(), 
        EVENTDATE : new SysCfg.DB.TField(), 
        CHATMESSAGES : new SysCfg.DB.TField() 
    }, 
    SMCHATASSIGNEDTYPE : { 
        NAME_TABLE : "SMCHATASSIGNEDTYPE", 
        IDSMCHATASSIGNEDTYPE : new SysCfg.DB.TField(), 
        ASSIGNEDTYPE : new SysCfg.DB.TField() 
    } 
} 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNED.NAME_TABLE = "SMCHATASSIGNED"; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNED.IDSMCHATASSIGNED.FieldName = "IDSMCHATASSIGNED"; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNED.IDSMCHATASSIGNED.TableName = "SMCHATASSIGNED"; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNED.IDSMCHATASSIGNED.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNED.IDSMCHATASSIGNED.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoDataLinkNames.SMCHATASSIGNED.IDSMCHATASSIGNED);
UsrCfg.InternoDataLinkNames.SMCHATASSIGNED.NAME_TABLE = "SMCHATASSIGNED"; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNED.ASSIGNED_ENABLE.FieldName = "ASSIGNED_ENABLE"; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNED.ASSIGNED_ENABLE.TableName = "SMCHATASSIGNED"; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNED.ASSIGNED_ENABLE.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNED.ASSIGNED_ENABLE.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoDataLinkNames.SMCHATASSIGNED.ASSIGNED_ENABLE);
UsrCfg.InternoDataLinkNames.SMCHATASSIGNED.NAME_TABLE = "SMCHATASSIGNED"; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNED.ASSIGNED_TITLE.FieldName = "ASSIGNED_TITLE"; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNED.ASSIGNED_TITLE.TableName = "SMCHATASSIGNED"; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNED.ASSIGNED_TITLE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNED.ASSIGNED_TITLE.Size = 200; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoDataLinkNames.SMCHATASSIGNED.ASSIGNED_TITLE);
UsrCfg.InternoDataLinkNames.SMCHATASSIGNED.NAME_TABLE = "SMCHATASSIGNED"; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNED.ASSIGNED_DATE.FieldName = "ASSIGNED_DATE"; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNED.ASSIGNED_DATE.TableName = "SMCHATASSIGNED"; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNED.ASSIGNED_DATE.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNED.ASSIGNED_DATE.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoDataLinkNames.SMCHATASSIGNED.ASSIGNED_DATE);
UsrCfg.InternoDataLinkNames.SMCHATASSIGNED.NAME_TABLE = "SMCHATASSIGNED"; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNED.IDSMCHATASSIGNEDTYPE.FieldName = "IDSMCHATASSIGNEDTYPE"; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNED.IDSMCHATASSIGNEDTYPE.TableName = "SMCHATASSIGNED"; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNED.IDSMCHATASSIGNEDTYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNED.IDSMCHATASSIGNEDTYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoDataLinkNames.SMCHATASSIGNED.IDSMCHATASSIGNEDTYPE);
UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDMEMBER.NAME_TABLE = "SMCHATASSIGNEDMEMBER"; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDMEMBER.IDSMCHATASSIGNEDMEMBER.FieldName = "IDSMCHATASSIGNEDMEMBER"; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDMEMBER.IDSMCHATASSIGNEDMEMBER.TableName = "SMCHATASSIGNEDMEMBER"; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDMEMBER.IDSMCHATASSIGNEDMEMBER.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDMEMBER.IDSMCHATASSIGNEDMEMBER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDMEMBER.IDSMCHATASSIGNEDMEMBER);
UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDMEMBER.NAME_TABLE = "SMCHATASSIGNEDMEMBER"; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDMEMBER.IDSMCHATASSIGNED.FieldName = "IDSMCHATASSIGNED"; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDMEMBER.IDSMCHATASSIGNED.TableName = "SMCHATASSIGNEDMEMBER"; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDMEMBER.IDSMCHATASSIGNED.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDMEMBER.IDSMCHATASSIGNED.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDMEMBER.IDSMCHATASSIGNED);
UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDMEMBER.NAME_TABLE = "SMCHATASSIGNEDMEMBER"; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDMEMBER.IDCMDBCI_MEMBER.FieldName = "IDCMDBCI_MEMBER"; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDMEMBER.IDCMDBCI_MEMBER.TableName = "SMCHATASSIGNEDMEMBER"; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDMEMBER.IDCMDBCI_MEMBER.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDMEMBER.IDCMDBCI_MEMBER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDMEMBER.IDCMDBCI_MEMBER);
UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDMEMBER.NAME_TABLE = "SMCHATASSIGNEDMEMBER"; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDMEMBER.DATETIME_MEMBER.FieldName = "DATETIME_MEMBER"; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDMEMBER.DATETIME_MEMBER.TableName = "SMCHATASSIGNEDMEMBER"; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDMEMBER.DATETIME_MEMBER.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDMEMBER.DATETIME_MEMBER.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDMEMBER.DATETIME_MEMBER);
UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDMEMBER.NAME_TABLE = "SMCHATASSIGNEDMEMBER"; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDMEMBER.ENABLE_MEMBER.FieldName = "ENABLE_MEMBER"; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDMEMBER.ENABLE_MEMBER.TableName = "SMCHATASSIGNEDMEMBER"; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDMEMBER.ENABLE_MEMBER.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDMEMBER.ENABLE_MEMBER.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDMEMBER.ENABLE_MEMBER);
UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDMEMBER.NAME_TABLE = "SMCHATASSIGNEDMEMBER"; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDMEMBER.CREATE_MEMBER.FieldName = "CREATE_MEMBER"; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDMEMBER.CREATE_MEMBER.TableName = "SMCHATASSIGNEDMEMBER"; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDMEMBER.CREATE_MEMBER.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDMEMBER.CREATE_MEMBER.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDMEMBER.CREATE_MEMBER);
UsrCfg.InternoDataLinkNames.SMCHAT.NAME_TABLE = "SMCHAT"; 
UsrCfg.InternoDataLinkNames.SMCHAT.IDSMCHAT.FieldName = "IDSMCHAT"; 
UsrCfg.InternoDataLinkNames.SMCHAT.IDSMCHAT.TableName = "SMCHAT"; 
UsrCfg.InternoDataLinkNames.SMCHAT.IDSMCHAT.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoDataLinkNames.SMCHAT.IDSMCHAT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoDataLinkNames.SMCHAT.IDSMCHAT);
UsrCfg.InternoDataLinkNames.SMCHAT.NAME_TABLE = "SMCHAT"; 
UsrCfg.InternoDataLinkNames.SMCHAT.IDSMCHATASSIGNED.FieldName = "IDSMCHATASSIGNED"; 
UsrCfg.InternoDataLinkNames.SMCHAT.IDSMCHATASSIGNED.TableName = "SMCHAT"; 
UsrCfg.InternoDataLinkNames.SMCHAT.IDSMCHATASSIGNED.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoDataLinkNames.SMCHAT.IDSMCHATASSIGNED.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoDataLinkNames.SMCHAT.IDSMCHATASSIGNED);
UsrCfg.InternoDataLinkNames.SMCHAT.NAME_TABLE = "SMCHAT"; 
UsrCfg.InternoDataLinkNames.SMCHAT.IDCMDBCI_SEND.FieldName = "IDCMDBCI_SEND"; 
UsrCfg.InternoDataLinkNames.SMCHAT.IDCMDBCI_SEND.TableName = "SMCHAT"; 
UsrCfg.InternoDataLinkNames.SMCHAT.IDCMDBCI_SEND.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoDataLinkNames.SMCHAT.IDCMDBCI_SEND.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoDataLinkNames.SMCHAT.IDCMDBCI_SEND);
UsrCfg.InternoDataLinkNames.SMCHAT.NAME_TABLE = "SMCHAT"; 
UsrCfg.InternoDataLinkNames.SMCHAT.EVENTDATE.FieldName = "EVENTDATE"; 
UsrCfg.InternoDataLinkNames.SMCHAT.EVENTDATE.TableName = "SMCHAT"; 
UsrCfg.InternoDataLinkNames.SMCHAT.EVENTDATE.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoDataLinkNames.SMCHAT.EVENTDATE.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoDataLinkNames.SMCHAT.EVENTDATE);
UsrCfg.InternoDataLinkNames.SMCHAT.NAME_TABLE = "SMCHAT"; 
UsrCfg.InternoDataLinkNames.SMCHAT.CHATMESSAGES.FieldName = "CHATMESSAGES"; 
UsrCfg.InternoDataLinkNames.SMCHAT.CHATMESSAGES.TableName = "SMCHAT"; 
UsrCfg.InternoDataLinkNames.SMCHAT.CHATMESSAGES.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoDataLinkNames.SMCHAT.CHATMESSAGES.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoDataLinkNames.SMCHAT.CHATMESSAGES);
UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDTYPE.NAME_TABLE = "SMCHATASSIGNEDTYPE"; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDTYPE.IDSMCHATASSIGNEDTYPE.FieldName = "IDSMCHATASSIGNEDTYPE"; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDTYPE.IDSMCHATASSIGNEDTYPE.TableName = "SMCHATASSIGNEDTYPE"; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDTYPE.IDSMCHATASSIGNEDTYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDTYPE.IDSMCHATASSIGNEDTYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDTYPE.IDSMCHATASSIGNEDTYPE);
UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDTYPE.NAME_TABLE = "SMCHATASSIGNEDTYPE"; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDTYPE.ASSIGNEDTYPE.FieldName = "ASSIGNEDTYPE"; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDTYPE.ASSIGNEDTYPE.TableName = "SMCHATASSIGNEDTYPE"; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDTYPE.ASSIGNEDTYPE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDTYPE.ASSIGNEDTYPE.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoDataLinkNames.SMCHATASSIGNEDTYPE.ASSIGNEDTYPE);
