﻿UsrCfg.Library.loadfrist = function(inCallBack)
{
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.CallBackResult = inCallBack;
    SysCfg.App.Properties.Application = UsrCfg.Properties._Application;
    

    SysCfg.App.Methods.Create();
    SysCfg.App.Properties.Config_Root.AppPath = SysCfg.App.Properties.xRaiz;
    SysCfg.App.Properties.Device = SysCfg.App.TDevice.Desktop;
    if (SysCfg.Device.IsMovile1()) SysCfg.App.Properties.Device = SysCfg.App.TDevice.Movile;
    //SysCfg.App.Methods.LoadApplicationPathIni();
    SysCfg.App.Methods.LoadbyIniIndexDb(_this, function (sender) {
        sender.CallBackResult();
    });

}

UsrCfg.Library.loadSecond = function()
{
    //SysCfg.App.Methods.InicDLPSSL_Client();  
    //SysCfg.License.Client.Methods.loadLicense();
    //if (SysCfg.License.Properties.SerialValido(true))
    //{
        //*************************************************************          
        SysCfg.App.Methods.Cfg_WindowsTask.NumSesion = SysCfg.App.Methods.LoadNumSesion(UsrCfg.Properties._Application.name + "_Client");
        SysCfg.App.Methods.LoadApplicationPathLog();
        SysCfg.Log.Properties.EnableLog = true;
        SysCfg.Log.Methods.WriteLog("********** " + SysCfg.App.Properties.Application.name + "Java Script Client " + UsrCfg.Version._UsrCfgAppVersion + " **********", null);
        SysCfg.Log.Methods.WriteLog(SysCfg.App.Properties.Application.name + "Library SysCfg Version:" + SysCfg.App.Properties._SysCfgAppVersion, null);
        //SysCfg.Log.Methods.WriteLog("Cfg_Socket.IPNumber=" + Comunic.Sockets.Properties.IPNumber, null);
        //SysCfg.Log.Methods.WriteLog("Cfg_Socket.HostName=" + Comunic.Sockets.Properties.HostName, null);
        //SysCfg.Log.Methods.WriteLog("Cfg_Socket.ServerIPNumber=" + Comunic.Sockets.Properties.ServerIPNumber, null);
        //SysCfg.Log.Methods.WriteLog("Cfg_Socket.ServerHostName=" + Comunic.Sockets.Properties.ServerHostName, null);
        SysCfg.App.Properties.Config_Language.Language = SysCfg.App.Properties.TLanguage.SPANICH;
        SysCfg.App.Properties.Config_Language.LanguageITF = SysCfg.App.Properties.TLanguage.SPANICH;
        SysCfg.IDI.Properties.EnableTraslate = true;
    
    

        UsrCfg.Methods.load();
   //}

}