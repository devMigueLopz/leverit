﻿

SysCfg.Methods.include("Scripts/UsrCfgScripts/InternoAtisNames.js");
SysCfg.Methods.include("Scripts/UsrCfgScripts/InternoCMDBNames.js");
SysCfg.Methods.include("Scripts/UsrCfgScripts/InternoDemoNames.js");
SysCfg.Methods.include("Scripts/UsrCfgScripts/InternoDataLinkNames.js");

//SysCfg.Methods.include("Scripts/UsrCfgScripts/InternoCommonNames.js");
//SysCfg.Methods.include("Scripts/UsrCfgScripts/InternoSetupBoxNames.js");
//SysCfg.Methods.include("Scripts/UsrCfgScripts/InternoDRobotNames.js");

SysCfg.Methods.include("Scripts/UsrCfgScripts/Traslate/JavaScriptComplete.js");
SysCfg.Methods.include("Scripts/UsrCfgScripts/AppJavaScriptClientMethods.js");
SysCfg.Methods.include("Scripts/Sources/Page/PageProperties.js");


//********************* persistence begin *************************************
//****** Se cambian los md
SysCfg.Methods.include("Scripts/Persistence/Properties.js");

SysCfg.Methods.include("Scripts/Componet/SeekSearch/SeekSerchProperties.js");
SysCfg.Methods.include("Scripts/Componet/SeekSearch/SeekSearchMethods.js");
SysCfg.Methods.include("Scripts/Componet/SeekSearch/SeekSerchCategory.js");
SysCfg.Methods.include("Scripts/Componet/SeekSearch/SeekSearchProfiler.js");

SysCfg.Methods.include("Scripts/Interface/InterfaceMethods.js");
SysCfg.Methods.include("Scripts/Interface/InterfaceProfiler.js");
SysCfg.Methods.include("Scripts/Interface/InterfaceProperties.js");



SysCfg.Methods.include("Scripts/Persistence/Catalog/BD_MDIMPACT.js");


SysCfg.Methods.include("Scripts/Persistence/Catalog/BD_MDIMPACT.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Catalog/BD_MDPRIORITYMATRIX.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Catalog/CatalogMethods.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Catalog/CatalogProfiler.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Catalog/CatalogProperties.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Catalog/DB_MDFUNCPER.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Catalog/DB_MDHIERPER.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Catalog/DB_MDPRIORITY.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Catalog/DB_MDSERVICETYPE.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Catalog/DB_MDURGENCY.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Catalog/DB_SDTYPEUSER.js");//TreduceDB

SysCfg.Methods.include("Scripts/Persistence/Chat/BOMensaje.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Chat/ChatMethods.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Chat/ChatProfiler.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Chat/ChatProperties.js");//TreduceDB

SysCfg.Methods.include("Scripts/Persistence/Model/DB_MDCASESTEXTRAFIELDS.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Model/DB_MDCASESTEXTRATABLE.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Model/DB_MDCATEGORY.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Model/DB_MDCATEGORYDETAIL.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Model/DB_MDCATEGORYDETAIL_TYPEUSER.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Model/DB_MDCONFIG.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Model/DB_MDFUNCESC.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Model/DB_MDFUNCPER.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Model/DB_MDGROUP.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Model/DB_MDGROUPUSER.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Model/DB_MDHIERESC.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Model/DB_MDHIERPER.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Model/DB_MDIMPACT.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Model/DB_MDINTERFACE.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Model/DB_MDINTERFACETYPE.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Model/DB_MDLIFESTATUS.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Model/DB_MDLIFESTATUSPERMISSION.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Model/DB_MDMATRIXMODELS.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Model/DB_MDMODELTYPED.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Model/DB_MDPRIORITY.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Model/DB_MDPRIORITYMATRIX.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Model/DB_MDSERVICEEXTRAFIELDS.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Model/DB_MDSERVICEEXTRATABLE.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Model/DB_MDSERVICETYPE.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Model/DB_MDSLA.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Model/MDCATEGORYNODE.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Model/ModelMethods.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Model/ModelProfiler.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Model/ModelProperties.js");//TreduceDB

SysCfg.Methods.include("Scripts/Persistence/Notify/NotifyMethods.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Notify/NotifyProfiler.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Notify/NotifyProperties.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/Notify/NotifyMethodsClient.js");

SysCfg.Methods.include("Scripts/Persistence/RemoteHelp/DB_AGENTE.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/RemoteHelp/DB_CPU.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/RemoteHelp/DB_ESTACION_RED.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/RemoteHelp/DB_EXTRADATA.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/RemoteHelp/DB_IP.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/RemoteHelp/DB_IP_SERVERS.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/RemoteHelp/DB_RHGROUPCR.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/RemoteHelp/DB_RHGROUPCRATROLEPERMISSION.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/RemoteHelp/DB_RHGROUPCRCPU.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/RemoteHelp/DB_RHGROUPCRPERMISSION.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/RemoteHelp/DB_RHGROUPPL.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/RemoteHelp/DB_RHGROUPPL_CFGBACKUP.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/RemoteHelp/DB_RHGROUPPL_CLI_CONFIG.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/RemoteHelp/DB_RHGROUPPL_POLICY.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/RemoteHelp/DB_RHGROUPPLATROLE.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/RemoteHelp/DB_RHGROUPPLCPU.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/RemoteHelp/DB_RHREQUEST.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/RemoteHelp/DB_RHREQUEST_CPUCR.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/RemoteHelp/DB_RHREQUEST_CPUPL.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/RemoteHelp/DB_USERS.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/RemoteHelp/DB_VPRO.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/RemoteHelp/DB_WINDOWS.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/RemoteHelp/RemoteHelpMethods.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/RemoteHelp/RemoteHelpMethodsServer.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/RemoteHelp/RemoteHelpProfiler.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/RemoteHelp/RemoteHelpMethods.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/RemoteHelp/RemoteHelpProperties.js");
SysCfg.Methods.include("Scripts/Persistence/RemoteHelp/RHCONFIG.js");

SysCfg.Methods.include("Scripts/Persistence/SMCI/BD_CMDBUSER.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/SMCI/BD_CMDBUSERADDRESS.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/SMCI/BD_CMDBUSERCONTACTTYPE.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/SMCI/BD_CMDBUSERSETTINGS.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/SMCI/BD_SDGROUPSERVICEUSERCI.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/SMCI/SMCIMethods.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/SMCI/SMCIProfiler.js");//TreduceDB
SysCfg.Methods.include("Scripts/Persistence/SMCI/SMCIProperties.js");//TreduceDB

SysCfg.Methods.include("Scripts/Persistence/MD/DB_MDPRIORITY.js");
SysCfg.Methods.include("Scripts/Persistence/MD/DB_MDIMPACT.js");
SysCfg.Methods.include("Scripts/Persistence/MD/DB_MDURGENCY.js");
SysCfg.Methods.include("Scripts/Persistence/MD/DB_MDPRIORITYMATRIX.js");
SysCfg.Methods.include("Scripts/Persistence/MD/MDMethods.js");
SysCfg.Methods.include("Scripts/Persistence/MD/MDProfiler.js");



SysCfg.Methods.include("Scripts/Persistence/PersistenceProfilerClientJavaScript.js");
SysCfg.Methods.include("Scripts/Persistence/Methods.js");
//********************* persistence end *************************************




SysCfg.Methods.include("Scripts/UsrCfgScripts/SC/SCProperties.js");
SysCfg.Methods.include("Scripts/UsrCfgScripts/SC/SCPropertiesJavaScript.js");
SysCfg.Methods.include("Scripts/UsrCfgScripts/SC/SCMethods.js");                                                 
SysCfg.Methods.include("Scripts/UsrCfgScripts/SC/SCMethodsJavaScript.js");

SysCfg.Methods.include("Scripts/UsrCfgScripts/FileSrv/DB_FILESRV.js");


SysCfg.Methods.include("Scripts/Persistence/GP/GPMethods.js");
SysCfg.Methods.include("Scripts/Persistence/GP/GPProfiler.js");




SysCfg.Methods.include("Scripts/UsrCfgScripts/Properties.js");
SysCfg.Methods.include("Scripts/UsrCfgScripts/Methods.js");



SysCfg.Methods.include("Scripts/PageManager.js");
SysCfg.Methods.include("Scripts/App.js");
SysCfg.Methods.include("Scripts/index.js");
SysCfg.Methods.include("Scripts/LoginConfig.js");
SysCfg.Methods.include("Scripts/main.js");
SysCfg.Methods.include("Scripts/ChangePassword.js");
SysCfg.Methods.include("Scripts/Login.js");







//SysCfg.Methods.include("Scripts/ST/Shared/STProperties.js");
//SysCfg.Methods.include("Scripts/ST/Shared/STMethods.js");
//SysCfg.Methods.include("Scripts/ST/Shared/STProfiler.js");
//SysCfg.Methods.include("Scripts/ST/STProfilerClientAsp.js");

SysCfg.Methods.include("Scripts/SD/Shared/SDCaseTimerCount.js");
SysCfg.Methods.include("Scripts/SD/Shared/SDProperties.js");
SysCfg.Methods.include("Scripts/SD/Shared/SDTYPEUSER_RWX.js");
//SysCfg.Methods.include("Scripts/SD/Atention/LifeStatus/SDLifeStatus.js");
SysCfg.Methods.include("Scripts/SD/Client/SDClientProperties.js");
SysCfg.Methods.include("Scripts/SD/Client/SDClientMethods.js");
SysCfg.Methods.include("Scripts/SD/Client/SDClientService.js");
SysCfg.Methods.include("Scripts/SD/Shared/Atention/CheckStd/CaseAtentionCheckStd.js");
SysCfg.Methods.include("Scripts/SD/Shared/Atention/CheckStd/frSDCaseAtentionCheckStd.js");

SysCfg.Methods.include("Scripts/SD/Shared/Atention/CaseExplorer/SDCaseExplorer.js");
SysCfg.Methods.include("Scripts/SD/Shared/Atention/CaseExplorer/SDClientCaseExplorer.js");




/*CMDB */
SysCfg.Methods.include("Scripts/EF/ExtraFieldProperties.js");

SysCfg.Methods.include("Scripts/CMDB/Shared/CIProperties.js");
SysCfg.Methods.include("Scripts/CMDB/Shared/CIProfiler.js");
SysCfg.Methods.include("Scripts/CMDB/Shared/CIMethods.js");


SysCfg.Methods.include("Scripts/Persistence/Demo/DB_DMTABLA.js");
SysCfg.Methods.include("Scripts/Persistence/Demo/DEMOProfiler.js");
SysCfg.Methods.include("Scripts/PanelServices/PanelservicesManager.js");

/*PANEL SERVICE*/
SysCfg.Methods.include("Scripts/Persistence/PanelService/DB_PSGROUP.js");
SysCfg.Methods.include("Scripts/Persistence/PanelService/DB_PSMAIN.js");
SysCfg.Methods.include("Scripts/Persistence/PanelService/DB_PSSECUNDARY.js");
SysCfg.Methods.include("Scripts/Persistence/PanelService/PanelServiceProfiler.js");
SysCfg.Methods.include("Scripts/Persistence/PanelService/PanelServiceMethods.js");

/*PBITEMPLATE AND PBI GRAPHIC PERSISTENCE*/
SysCfg.Methods.include("Scripts/Persistence/PBITemplate/DB_PBIGRAPHICTYPE.js");
SysCfg.Methods.include("Scripts/Persistence/PBITemplate/DB_PBITEMPLATE.js");
SysCfg.Methods.include("Scripts/Persistence/PBITemplate/DB_PBITEMPLATECELLGRAPHIC_COLUMN.js");
SysCfg.Methods.include("Scripts/Persistence/PBITemplate/DB_PBITEMPLATECELLGRAPHIC.js");
SysCfg.Methods.include("Scripts/Persistence/PBITemplate/DB_PBITEMPLATECELLS.js");
SysCfg.Methods.include("Scripts/Persistence/PBITemplate/DB_PBITEMPLATECELLSDESIGN.js");
SysCfg.Methods.include("Scripts/Persistence/PBITemplate/PBITemplateProfiler.js");
SysCfg.Methods.include("Scripts/Persistence/PBITemplate/PBITemplateMethods.js");
/****/

/*PBIGRAPHICS AND PBITEMPLATE*/
SysCfg.Methods.include("Scripts/GraphicsPBI/GraphicsPBIBasic.js");
SysCfg.Methods.include("Scripts/GraphicsPBI/GraphicsPBIManagerBasic.js");
/**/

SysCfg.Methods.include("Scripts/PanelServices/PanelServicesBasic.js");
SysCfg.Methods.include("Scripts/PanelServices/PanelServicesMain.js");
SysCfg.Methods.include("Scripts/PanelServices/PanelServicesMapping.js");
SysCfg.Methods.include("Scripts/PanelServices/js/DropDownPanelServices/DropDownPanelServices.js");
SysCfg.Methods.include("Scripts/PanelServices/js/ControlPanelServices/ControlPanelServices.js");

/*Persistence Votes*/
SysCfg.Methods.include("Scripts/Persistence/Votes/DB_VOTE.js");
SysCfg.Methods.include("Scripts/Persistence/Votes/DB_VOTEOPTION.js");
SysCfg.Methods.include("Scripts/Persistence/Votes/DB_VOTEDATA.js");
SysCfg.Methods.include("Scripts/Persistence/Votes/DB_VOTEDATAUSER.js");
SysCfg.Methods.include("Scripts/Persistence/Votes/VotesProfiler.js");
SysCfg.Methods.include("Scripts/Persistence/Votes/VotesMethods.js");
/*Votes Editor*/
SysCfg.Methods.include("Scripts/Votes/VotesBasic.js");
SysCfg.Methods.include("Scripts/Votes/VotesManagerBasic.js");


/* INI GP QUERY */
SysCfg.Methods.include("Scripts/Persistence/GP/DB_CUSTOMFILTER.js");
SysCfg.Methods.include("Scripts/Persistence/GP/DB_GPCHART.js");
SysCfg.Methods.include("Scripts/Persistence/GP/DB_GPCOLUMNEVENT.js");
SysCfg.Methods.include("Scripts/Persistence/GP/DB_GPCOLUMNVIEW.js");
SysCfg.Methods.include("Scripts/Persistence/GP/DB_GPEDIT_COLUMN.js");
SysCfg.Methods.include("Scripts/Persistence/GP/DB_GPEDIT_TABLE.js");
SysCfg.Methods.include("Scripts/Persistence/GP/DB_GPQUERY.js");
SysCfg.Methods.include("Scripts/Persistence/GP/DB_GPQUERYTYPE.js");
SysCfg.Methods.include("Scripts/Persistence/GP/DB_GPREPORTSQL.js");
SysCfg.Methods.include("Scripts/Persistence/GP/DB_GPREPORTTEMPLATE.js");
SysCfg.Methods.include("Scripts/Persistence/GP/DB_GPVALUECOLOR.js");
SysCfg.Methods.include("Scripts/Persistence/GP/DB_GPVIEW.js");


SysCfg.Methods.include("Scripts/GPScripts/GPView.js");
SysCfg.Methods.include("Scripts/SD/CaseManager/Console/SMConsoleManager.js");
SysCfg.Methods.include("Scripts/SD/CaseManager/Console/SMConsoleManagerDesigner.js");
SysCfg.Methods.include("SD/Shared/Contact/UfrCMDBContact.js");


/* FIN GP QUERY */




SysCfg.Methods.include("Scripts/Persistence/SD/CaseManager/Shared/SMKEWA/DB_SMKEWA.js");
SysCfg.Methods.include("Scripts/SD/CaseManager/Shared/SMKEWA/frSMKEWAConteiner.js");




SysCfg.Methods.include("Componet/UCode/VCL/linq/linq.js");
/**/
SysCfg.Methods.include("Scripts/MD/MDImpact.js");
SysCfg.Methods.include("Scripts/MD/MDPriority.js");
SysCfg.Methods.include("Scripts/MD/MDPriorityMatrix.js");
SysCfg.Methods.include("Scripts/MD/MDUrgency.js");

SysCfg.Methods.include("Scripts/ProcessSetup/Model/frMDBase.js");
SysCfg.Methods.include("Scripts/ProcessSetup/Model/frMDBaseDesigner.js");

//--Category.Basic--
SysCfg.Methods.include("SD/Shared/Category/Basic/CategoryBasic.js");
//--CaseUser.Atention--
SysCfg.Methods.include("ST/Shared/DynamicSTEFProperties.js");
SysCfg.Methods.include("ST/Shared/DynamicSTEFMethods.js");


SysCfg.Methods.include("SD/CaseUser/Atention/SMConsoleUserSwitch.js");
SysCfg.Methods.include("SD/CaseUser/Atention/frSDCaseAtention.js");
SysCfg.Methods.include("SD/CaseUser/Atention/frSDCaseAtentionDesigner.js");
SysCfg.Methods.include("SD/CaseUser/Atention/frSDOperationStatus.js");
SysCfg.Methods.include("SD/CaseUser/Atention/frSDCaseAtentionMethods.js");
SysCfg.Methods.include("SD/Shared/Atention/frSDCaseAtentionChangeSLA.js");
SysCfg.Methods.include("SD/Shared/Atention/frSDCaseAtentionChangeSLADesigner.js");

SysCfg.Methods.include("SD/Shared/RelatedCase/frSDRelatedCase.js");
SysCfg.Methods.include("SD/Shared/RelatedCase/frSDRelatedCaseDesigner.js");
SysCfg.Methods.include("SD/Shared/RelatedCase/frSDRelatedCaseEditor.js");
SysCfg.Methods.include("SD/Shared/RelatedCase/frSDRelatedCaseEditorDesigner.js");

SysCfg.Methods.include("SD/CaseUser/Atention/DynamicSTEFAtentionCase.js");


SysCfg.Methods.include("SD/CaseManager/Atention/SMConsoleManagerSwitch.js");
SysCfg.Methods.include("SD/CaseManager/Atention/frSDCaseAtention.js");
SysCfg.Methods.include("SD/CaseManager/Atention/frSDCaseAtentionDesigner.js");
SysCfg.Methods.include("SD/CaseManager/Atention/frSDOperationStatus.js");
SysCfg.Methods.include("SD/CaseManager/Atention/frSDCaseAtentionMethods.js");
SysCfg.Methods.include("SD/CaseManager/Atention/DynamicSTEFAtentionCase.js");
SysCfg.Methods.include("Scripts/SD/Shared/Console/ChildUserPermissions.js");//error en jaimes SysCfg.Methods.include("Scripts/SD/Shared/Atention/ChildUserPermissions/ChildUserPermissions.js");
SysCfg.Methods.include("Scripts/SD/Shared/Atention/frSDCaseAtentionDetail.js");//error en jaimes  SysCfg.Methods.include("Scripts/SD/Shared/Atention/CaseAtentionDetail/frSDCaseAtentionDetail.js");
SysCfg.Methods.include("SD/Shared/Attached/UfrSDAttached.js");
SysCfg.Methods.include("SD/Shared/Attached/UfrSDAttachedDesigner.js");
SysCfg.Methods.include("SD/Shared/Attached/frPermissions.js");
SysCfg.Methods.include("SD/Shared/Attached/frPermissionsDesigner.js");


SysCfg.Methods.include("SD/Shared/Atention/frSDOperationESC.js");
SysCfg.Methods.include("SD/Shared/Atention/frSDOperationESCDesigner.js");
SysCfg.Methods.include("SD/Shared/Contact/UfrCMDBContactDesigner.js");


//--Demo.AllControls--
SysCfg.Methods.include("Demo/VCL/AllControls/frBasic.js");
SysCfg.Methods.include("Demo/VCL/AllControls/AllControls.js");
//--CaseUser.Atention--
SysCfg.Methods.include("SD/CaseUser/NewCase/UfrSDCaseNew.js");
SysCfg.Methods.include("SD/CaseUser/NewCase/UfrSDCaseNewDesigner.js");

//--CaseUser.NewCase--
SysCfg.Methods.include("SD/CaseUser/NewCase/DynamicSTEFNewCase.js");
 //--Casemanager.Atention--
SysCfg.Methods.include("SD/Casemanager/NewCase/UfrSDCaseNew.js");
SysCfg.Methods.include("Scripts/SD/CaseManager/NewCase/frSDCaseNew_Template.js");
SysCfg.Methods.include("Scripts/SD/CaseManager/NewCase/frSDCaseNew_TemplateDesigner.js");

//--Casemanager.NewCase--
SysCfg.Methods.include("SD/Casemanager/NewCase/DynamicSTEFNewCase.js");
SysCfg.Methods.include("Scripts/SD/CaseManager/NewCase/frSDCaseNew.js");
SysCfg.Methods.include("Scripts/SD/CaseManager/NewCase/frSDCaseSet.js");
//--ChatNotify--
SysCfg.Methods.include("Chat/ChatRooms.js");
SysCfg.Methods.include("PersistenceNotify/Notify.js");
SysCfg.Methods.include("PersistenceNotify/NotifyDesigner.js");
//--Tool.SQL--
SysCfg.Methods.include("Tools/SQL/frUSQL.js");

//--CMDB.CIEditor--
SysCfg.Methods.include("CMDB/CIEditor/frCIEditor3.js");


SysCfg.Methods.include("Scripts/RemoteHelp/RemoteHelpOptionsl.js");
SysCfg.Methods.include("Scripts/RemoteHelpScripts/RemoteHelp.js");

//--didijoca SeekSerach--
 SysCfg.Methods.include("Componet/AdvSearch/GridCF/frAdvSearchGridCF.js");
 SysCfg.Methods.include("SD/Shared/Category/Basic_SeekSearch/Basic_SeekSearch.js");


 SysCfg.Methods.include("SD/Shared/Category/Basic/CategoryBasic.js");

 SysCfg.Methods.include("Scripts/Sources/Menu/MenuProfiler.js");

 SysCfg.Methods.include("Scripts/Sources/Page/PageMethods.js");
 SysCfg.Methods.include("Scripts/Sources/Page/PageProperties.js");

 SysCfg.Methods.include("Scripts/Sources/Page/Example/PageOne.js");
 SysCfg.Methods.include("Scripts/Sources/Page/Example/PageOneDesigner.js");

 SysCfg.Methods.include("Scripts/Sources/Page/Example/PageModal.js");
 SysCfg.Methods.include("Scripts/Sources/Page/Example/PageModalDesigner.js");

 SysCfg.Methods.include("Scripts/Sources/Page/Example/PageMenu.js");
 SysCfg.Methods.include("Scripts/Sources/Page/Example/PageMenuDesigner.js");

 // style
 SysCfg.Methods.include("Scripts/Sources/Style/StyleProperties.js");
 SysCfg.Methods.include("Scripts/Sources/Style/StyleMethods.js");
 SysCfg.Methods.include("Scripts/Sources/Style/StyleProfiler.js");
//

 SysCfg.Methods.include("Scripts/Sources/SquencialColor/SequencialMethods.js");
 SysCfg.Methods.include("Scripts/Sources/SquencialColor/SequencialProperties.js");


//ProcessSetup -- Graphic Model
                             
 SysCfg.Methods.include("Scripts/ProcessSetup/Plugins/MindFusion.Common.js");
 SysCfg.Methods.include("Scripts/ProcessSetup/Plugins/MindFusion.Diagramming.js");

 SysCfg.Methods.include("Scripts/ProcessSetup/Graphic/Diagram.js");
 SysCfg.Methods.include("Scripts/ProcessSetup/Graphic/Node.js");
 SysCfg.Methods.include("Scripts/ProcessSetup/Graphic/Layout.js");
 SysCfg.Methods.include("Scripts/ProcessSetup/Model/GraphicModel.js");
 SysCfg.Methods.include("Scripts/ProcessSetup/Model/GraphicModelDesigner.js");
 SysCfg.Methods.include("Scripts/ProcessSetup/Model/UcGrapyModel.js");
 SysCfg.Methods.include("Scripts/ProcessSetup/Model/UcGrapyModelDesigner.js");
 SysCfg.Methods.include("Scripts/ProcessSetup/Model/UcGraphicStatusModel.js");
 SysCfg.Methods.include("Scripts/ProcessSetup/Model/UcGraphicStatusDesigner.js");
// END ProcessSetup -- Graphic Model


 //Graphic CI 
 SysCfg.Methods.include("Componet/MindFusion/Diagraming/DiagramMap.js");
 SysCfg.Methods.include("Componet/MindFusion/Diagraming/Link.js");
 SysCfg.Methods.include("Componet/MindFusion/Diagraming/Node.js");

 SysCfg.Methods.include("CMDB/DiagramMap/DiagramMapProvider.js");

 SysCfg.Methods.include("CMDB/CIEditor/frCIRelationGraphics.js");
 SysCfg.Methods.include("CMDB/CIEditor/frCIRelationGraphicsDesigner.js");
 //END Graphic CI 


/*Persistence ProjectManagement*/
SysCfg.Methods.include("Scripts/Persistence/ProjectManagement/DB_PMMAINTASK.js");
SysCfg.Methods.include("Scripts/Persistence/ProjectManagement/DB_PMHOLIDAY.js");
SysCfg.Methods.include("Scripts/Persistence/ProjectManagement/DB_PMDETAILTASK.js");
SysCfg.Methods.include("Scripts/Persistence/ProjectManagement/DB_PMRESOURCES.js");
SysCfg.Methods.include("Scripts/Persistence/ProjectManagement/DB_PMRESOURCESALLOCATION.js");
SysCfg.Methods.include("Scripts/Persistence/ProjectManagement/DB_PMTYPERELATION.js");
SysCfg.Methods.include("Scripts/Persistence/ProjectManagement/DB_PMRELATION.js");
SysCfg.Methods.include("Scripts/Persistence/ProjectManagement/ProjectManagementProfiler.js");
SysCfg.Methods.include("Scripts/Persistence/ProjectManagement/ProjectManagementMethods.js");
/**/
/*FORUMS*/
SysCfg.Methods.include("Scripts/Forums/DataTables-1.10.19/media/js/jquery.dataTables.min.js");
SysCfg.Methods.include("Scripts/Forums/DataTables-1.10.19/media/js/dataTables.bootstrap.min.js");
SysCfg.Methods.include("Scripts/Forums/DataTables-1.10.19/extensions/Responsive/js/dataTables.responsive.min.js");
SysCfg.Methods.include("Scripts/Forums/DataTables-1.10.19/extensions/Responsive/js/responsive.bootstrap.min.js");

SysCfg.Methods.includeStyle("Scripts/Forums/DataTables-1.10.19/media/css/dataTables.bootstrap.min.css");
SysCfg.Methods.includeStyle("Scripts/Forums/DataTables-1.10.19/extensions/Responsive/css/responsive.bootstrap.min.css");

SysCfg.Methods.include("Scripts/Persistence/Forums/DB_FORUMS.js");
SysCfg.Methods.include("Scripts/Persistence/Forums/DB_FORUMSCATEGORY.js");
SysCfg.Methods.include("Scripts/Persistence/Forums/DB_QUERYS.js");
SysCfg.Methods.include("Scripts/Persistence/Forums/DB_TOPICS.js");
SysCfg.Methods.include("Scripts/Persistence/Forums/DB_TOPICSRESPONSE.js");
SysCfg.Methods.include("Scripts/Persistence/Forums/DB_TOPICSRESPONSEABUSE.js");
SysCfg.Methods.include("Scripts/Persistence/Forums/DB_TOPICSRESPONSEVOTES.js");
SysCfg.Methods.include("Scripts/Persistence/Forums/DB_FORUMSSEARCH.js");

SysCfg.Methods.include("Scripts/Persistence/Forums/ForumsProfiler.js");
SysCfg.Methods.include("Scripts/Persistence/Forums/ForumsMethods.js");

SysCfg.Methods.include("Scripts/Forums/ForumsBasic.js");
SysCfg.Methods.include("Scripts/Forums/ForumsTranslate.js");
SysCfg.Methods.include("Scripts/Forums/ForumsDesigner.js");

SysCfg.Methods.include("Scripts/Forums/ForumsManager.js");
SysCfg.Methods.include("Scripts/Forums/ForumsManagerDesigner.js");
/****/
