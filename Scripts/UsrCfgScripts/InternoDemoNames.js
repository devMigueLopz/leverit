UsrCfg.InternoDemoNames = { 
    _Prefix : "Demo", 
    DMTABLA : { 
        NAME_TABLE : "DMTABLA", 
        IDDMTABLA : new SysCfg.DB.TField(), 
        FDTEXTO : new SysCfg.DB.TField(), 
        FDCADENA : new SysCfg.DB.TField(), 
        FDBINARIO : new SysCfg.DB.TField(), 
        FDENTRO : new SysCfg.DB.TField(), 
        FDFLOTANTE : new SysCfg.DB.TField(), 
        FDDATETIME : new SysCfg.DB.TField() 
    }, 
    DMTABLAREF : { 
        NAME_TABLE : "DMTABLAREF", 
        IDDMTABLAREF : new SysCfg.DB.TField(), 
        IDDMTABLA : new SysCfg.DB.TField(), 
        FDCADENA : new SysCfg.DB.TField() 
    }, 
    DMTABLASIMPLE : { 
        NAME_TABLE : "DMTABLASIMPLE", 
        IDDMTABLASIMPLE : new SysCfg.DB.TField(), 
        FDTEXTO : new SysCfg.DB.TField() 
    } 
} 
UsrCfg.InternoDemoNames.DMTABLA.NAME_TABLE = "DMTABLA"; 
UsrCfg.InternoDemoNames.DMTABLA.IDDMTABLA.FieldName = "IDDMTABLA"; 
UsrCfg.InternoDemoNames.DMTABLA.IDDMTABLA.TableName = "DMTABLA"; 
UsrCfg.InternoDemoNames.DMTABLA.IDDMTABLA.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoDemoNames.DMTABLA.IDDMTABLA.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoDemoNames.DMTABLA.IDDMTABLA);
UsrCfg.InternoDemoNames.DMTABLA.NAME_TABLE = "DMTABLA"; 
UsrCfg.InternoDemoNames.DMTABLA.FDTEXTO.FieldName = "FDTEXTO"; 
UsrCfg.InternoDemoNames.DMTABLA.FDTEXTO.TableName = "DMTABLA"; 
UsrCfg.InternoDemoNames.DMTABLA.FDTEXTO.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoDemoNames.DMTABLA.FDTEXTO.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoDemoNames.DMTABLA.FDTEXTO);
UsrCfg.InternoDemoNames.DMTABLA.NAME_TABLE = "DMTABLA"; 
UsrCfg.InternoDemoNames.DMTABLA.FDCADENA.FieldName = "FDCADENA"; 
UsrCfg.InternoDemoNames.DMTABLA.FDCADENA.TableName = "DMTABLA"; 
UsrCfg.InternoDemoNames.DMTABLA.FDCADENA.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoDemoNames.DMTABLA.FDCADENA.Size = 255; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoDemoNames.DMTABLA.FDCADENA);
UsrCfg.InternoDemoNames.DMTABLA.NAME_TABLE = "DMTABLA"; 
UsrCfg.InternoDemoNames.DMTABLA.FDBINARIO.FieldName = "FDBINARIO"; 
UsrCfg.InternoDemoNames.DMTABLA.FDBINARIO.TableName = "DMTABLA"; 
UsrCfg.InternoDemoNames.DMTABLA.FDBINARIO.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoDemoNames.DMTABLA.FDBINARIO.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoDemoNames.DMTABLA.FDBINARIO);
UsrCfg.InternoDemoNames.DMTABLA.NAME_TABLE = "DMTABLA"; 
UsrCfg.InternoDemoNames.DMTABLA.FDENTRO.FieldName = "FDENTRO"; 
UsrCfg.InternoDemoNames.DMTABLA.FDENTRO.TableName = "DMTABLA"; 
UsrCfg.InternoDemoNames.DMTABLA.FDENTRO.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoDemoNames.DMTABLA.FDENTRO.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoDemoNames.DMTABLA.FDENTRO);
UsrCfg.InternoDemoNames.DMTABLA.NAME_TABLE = "DMTABLA"; 
UsrCfg.InternoDemoNames.DMTABLA.FDFLOTANTE.FieldName = "FDFLOTANTE"; 
UsrCfg.InternoDemoNames.DMTABLA.FDFLOTANTE.TableName = "DMTABLA"; 
UsrCfg.InternoDemoNames.DMTABLA.FDFLOTANTE.DataType = SysCfg.DB.Properties.TDataType.Double; 
UsrCfg.InternoDemoNames.DMTABLA.FDFLOTANTE.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoDemoNames.DMTABLA.FDFLOTANTE);
UsrCfg.InternoDemoNames.DMTABLA.NAME_TABLE = "DMTABLA"; 
UsrCfg.InternoDemoNames.DMTABLA.FDDATETIME.FieldName = "FDDATETIME"; 
UsrCfg.InternoDemoNames.DMTABLA.FDDATETIME.TableName = "DMTABLA"; 
UsrCfg.InternoDemoNames.DMTABLA.FDDATETIME.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoDemoNames.DMTABLA.FDDATETIME.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoDemoNames.DMTABLA.FDDATETIME);
UsrCfg.InternoDemoNames.DMTABLAREF.NAME_TABLE = "DMTABLAREF"; 
UsrCfg.InternoDemoNames.DMTABLAREF.IDDMTABLAREF.FieldName = "IDDMTABLAREF"; 
UsrCfg.InternoDemoNames.DMTABLAREF.IDDMTABLAREF.TableName = "DMTABLAREF"; 
UsrCfg.InternoDemoNames.DMTABLAREF.IDDMTABLAREF.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoDemoNames.DMTABLAREF.IDDMTABLAREF.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoDemoNames.DMTABLAREF.IDDMTABLAREF);
UsrCfg.InternoDemoNames.DMTABLAREF.NAME_TABLE = "DMTABLAREF"; 
UsrCfg.InternoDemoNames.DMTABLAREF.IDDMTABLA.FieldName = "IDDMTABLA"; 
UsrCfg.InternoDemoNames.DMTABLAREF.IDDMTABLA.TableName = "DMTABLAREF"; 
UsrCfg.InternoDemoNames.DMTABLAREF.IDDMTABLA.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoDemoNames.DMTABLAREF.IDDMTABLA.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoDemoNames.DMTABLAREF.IDDMTABLA);
UsrCfg.InternoDemoNames.DMTABLAREF.NAME_TABLE = "DMTABLAREF"; 
UsrCfg.InternoDemoNames.DMTABLAREF.FDCADENA.FieldName = "FDCADENA"; 
UsrCfg.InternoDemoNames.DMTABLAREF.FDCADENA.TableName = "DMTABLAREF"; 
UsrCfg.InternoDemoNames.DMTABLAREF.FDCADENA.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoDemoNames.DMTABLAREF.FDCADENA.Size = 255; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoDemoNames.DMTABLAREF.FDCADENA);
UsrCfg.InternoDemoNames.DMTABLASIMPLE.NAME_TABLE = "DMTABLASIMPLE"; 
UsrCfg.InternoDemoNames.DMTABLASIMPLE.IDDMTABLASIMPLE.FieldName = "IDDMTABLASIMPLE"; 
UsrCfg.InternoDemoNames.DMTABLASIMPLE.IDDMTABLASIMPLE.TableName = "DMTABLASIMPLE"; 
UsrCfg.InternoDemoNames.DMTABLASIMPLE.IDDMTABLASIMPLE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoDemoNames.DMTABLASIMPLE.IDDMTABLASIMPLE.Size = 250; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoDemoNames.DMTABLASIMPLE.IDDMTABLASIMPLE);
UsrCfg.InternoDemoNames.DMTABLASIMPLE.NAME_TABLE = "DMTABLASIMPLE"; 
UsrCfg.InternoDemoNames.DMTABLASIMPLE.FDTEXTO.FieldName = "FDTEXTO"; 
UsrCfg.InternoDemoNames.DMTABLASIMPLE.FDTEXTO.TableName = "DMTABLASIMPLE"; 
UsrCfg.InternoDemoNames.DMTABLASIMPLE.FDTEXTO.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoDemoNames.DMTABLASIMPLE.FDTEXTO.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoDemoNames.DMTABLASIMPLE.FDTEXTO);
