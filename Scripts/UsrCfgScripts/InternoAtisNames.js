UsrCfg.InternoAtisNames = { 
    _Prefix : "Atis", 
    SYSTEMSTATUS : { 
        NAME_TABLE : "SYSTEMSTATUS", 
        IDSYSTEMSTATUS : new SysCfg.DB.TField(), 
        SYSTEMSTATUSNAME : new SysCfg.DB.TField() 
    }, 
    SDCASETYPE : { 
        NAME_TABLE : "SDCASETYPE", 
        IDSDCASETYPE : new SysCfg.DB.TField(), 
        TYPENAME : new SysCfg.DB.TField() 
    }, 
    SDRESPONSETYPE : { 
        NAME_TABLE : "SDRESPONSETYPE", 
        IDSDRESPONSETYPE : new SysCfg.DB.TField(), 
        RESPONSETYPENAME : new SysCfg.DB.TField() 
    }, 
    SDCASESOURCETYPE : { 
        NAME_TABLE : "SDCASESOURCETYPE", 
        IDSDCASESOURCETYPE : new SysCfg.DB.TField(), 
        SOURCETYPENAME : new SysCfg.DB.TField() 
    }, 
    SDCASE : { 
        NAME_TABLE : "SDCASE", 
        IDSDCASE : new SysCfg.DB.TField(), 
        IDSDCASE_PARENT : new SysCfg.DB.TField(), 
        IDSDCASESTATUS : new SysCfg.DB.TField(), 
        IDSDRESPONSETYPE : new SysCfg.DB.TField(), 
        IDSDCASESOURCETYPE : new SysCfg.DB.TField(), 
        CASE_DATESTART : new SysCfg.DB.TField(), 
        CASE_DATECREATE : new SysCfg.DB.TField(), 
        CASE_DATERESOLVED : new SysCfg.DB.TField(), 
        CASE_DATECLOSED : new SysCfg.DB.TField(), 
        CASE_DATEPROGRESS : new SysCfg.DB.TField(), 
        IDOWNER : new SysCfg.DB.TField(), 
        IDUSER : new SysCfg.DB.TField(), 
        IDHANDLER : new SysCfg.DB.TField(), 
        IDMANAGERSINFORMED : new SysCfg.DB.TField(), 
        IDCMDBCONTACTTYPE_USER : new SysCfg.DB.TField(), 
        IDMDCATEGORYDETAIL_INITIAL : new SysCfg.DB.TField(), 
        IDMDCATEGORYDETAIL_FINAL : new SysCfg.DB.TField(), 
        CASE_ISMAYOR : new SysCfg.DB.TField(), 
        CASE_DESCRIPTION : new SysCfg.DB.TField(), 
        CASE_TITLE : new SysCfg.DB.TField(), 
        CASE_RETURN_STR : new SysCfg.DB.TField(), 
        CASE_RETURN_COST : new SysCfg.DB.TField(), 
        CASE_FINALSUMM : new SysCfg.DB.TField(), 
        CASE_COUNTTIME : new SysCfg.DB.TField(), 
        CASE_COUNTTIMEPAUSE : new SysCfg.DB.TField(), 
        CASE_COUNTTIMERESOLVED : new SysCfg.DB.TField(), 
        CASE_DATELASTCUT : new SysCfg.DB.TField() 
    }, 
    SDCASEEF : { 
        NAME_TABLE : "SDCASEEF", 
        IDSDCASEEF : new SysCfg.DB.TField(), 
        CASEEF_DATE : new SysCfg.DB.TField(), 
        IDCMDBCI : new SysCfg.DB.TField(), 
        IDSDCASE : new SysCfg.DB.TField() 
    }, 
    SDCASETEMPLATE : { 
        NAME_TABLE : "SDCASETEMPLATE", 
        IDSDCASETEMPLATE : new SysCfg.DB.TField(), 
        TEMPLATE_DATECREATE : new SysCfg.DB.TField(), 
        TEMPLATE_IDSDCASETYPE : new SysCfg.DB.TField(), 
        TEMPLATE_ENABLE : new SysCfg.DB.TField(), 
        TEMPLATE_IDCMDBCI : new SysCfg.DB.TField(), 
        TEMPLATE_ISPUBLIC : new SysCfg.DB.TField(), 
        TEMPLATE_NAME : new SysCfg.DB.TField(), 
        TEMPLATE_DESCRIPTION : new SysCfg.DB.TField(), 
        TEMPLATE_CODECFGSTR : new SysCfg.DB.TField() 
    }, 
    SDCASEMTASGSTATUS : { 
        NAME_TABLE : "SDCASEMTASGSTATUS", 
        IDSDCASEMTASGSTATUS : new SysCfg.DB.TField(), 
        ASGSTATUSNAME : new SysCfg.DB.TField() 
    }, 
    SDCASEMT : { 
        NAME_TABLE : "SDCASEMT", 
        IDSDCASEMT : new SysCfg.DB.TField(), 
        IDSDCASE : new SysCfg.DB.TField(), 
        IDSDCASEMTSTATUS : new SysCfg.DB.TField(), 
        CASEMT_DATECREATE : new SysCfg.DB.TField(), 
        CASEMT_DATEASSIGNED : new SysCfg.DB.TField(), 
        CASEMT_DATEPROGRESS : new SysCfg.DB.TField(), 
        CASEMT_DATECANCELLED : new SysCfg.DB.TField(), 
        IDSDWHOTOCASECREATE : new SysCfg.DB.TField(), 
        IDSDWHOTOCASECANCELLED : new SysCfg.DB.TField(), 
        IDMDCATEGORYDETAIL : new SysCfg.DB.TField(), 
        IDSLA : new SysCfg.DB.TField(), 
        MT_COMMENTSM : new SysCfg.DB.TField(), 
        MT_GUIDETEXT : new SysCfg.DB.TField(), 
        MT_IDMDFUNCESC : new SysCfg.DB.TField(), 
        MT_IDMDHIERESC : new SysCfg.DB.TField(), 
        MT_IDMDMODELTYPED : new SysCfg.DB.TField(), 
        MT_IDMDSERVICETYPE : new SysCfg.DB.TField(), 
        MT_MAXTIME : new SysCfg.DB.TField(), 
        MT_NORMALTIME : new SysCfg.DB.TField(), 
        IDSDSCALETYPE_FUNC : new SysCfg.DB.TField(), 
        IDSDSCALETYPE_HIER : new SysCfg.DB.TField(), 
        MT_OPEARTIONM : new SysCfg.DB.TField(), 
        MT_POSSIBLERETURNS : new SysCfg.DB.TField(), 
        MT_TITLEM : new SysCfg.DB.TField(), 
        MT_STEPVALIDATE : new SysCfg.DB.TField(), 
        MT_TIMEENABLE : new SysCfg.DB.TField(), 
        MT_STEEPTYPEUSERENABLE : new SysCfg.DB.TField(), 
        MT_IDCMDBCIDEFINE : new SysCfg.DB.TField(), 
        IDMDIMPACT : new SysCfg.DB.TField(), 
        IDMDPRIORITY : new SysCfg.DB.TField(), 
        IDMDURGENCY : new SysCfg.DB.TField(), 
        CASEMT_LIFESTATUS : new SysCfg.DB.TField(), 
        CASEMT_TIMERCOUNT : new SysCfg.DB.TField(), 
        CASEMT_SET_LS_NAMESTEP : new SysCfg.DB.TField(), 
        CASEMT_SET_LS_STATUSN : new SysCfg.DB.TField(), 
        CASEMT_SET_LS_NEXTSTEP : new SysCfg.DB.TField(), 
        CASEMT_SET_LS_COMMENTSST : new SysCfg.DB.TField(), 
        CASEMT_COUNTTIME : new SysCfg.DB.TField(), 
        CASEMT_COUNTTIMEPAUSE : new SysCfg.DB.TField(), 
        CASEMT_COUNTTIMERESOLVED : new SysCfg.DB.TField(), 
        CASEMT_DATELASTCUT : new SysCfg.DB.TField(), 
        CASEMT_SET_FUNLAVEL : new SysCfg.DB.TField(), 
        CASEMT_SET_HIERLAVEL : new SysCfg.DB.TField(), 
        IDSDCASEMTASGSTATUS_FUN : new SysCfg.DB.TField(), 
        IDSDCASEMTASGSTATUS_HIER : new SysCfg.DB.TField() 
    }, 
    SDCASEMT_LSSTATUS : { 
        NAME_TABLE : "SDCASEMT_LSSTATUS", 
        IDSDCASEMT_LSSTATUS : new SysCfg.DB.TField(), 
        CASEMT_LSSTATUSNAME : new SysCfg.DB.TField() 
    }, 
    SDCASEMT_LSTYPE : { 
        NAME_TABLE : "SDCASEMT_LSTYPE", 
        IDSDCASEMT_LSTYPE : new SysCfg.DB.TField(), 
        CASEMT_LSTYPENAME : new SysCfg.DB.TField() 
    }, 
    SDCASEMT_LS : { 
        NAME_TABLE : "SDCASEMT_LS", 
        IDSDCASEMT_LS : new SysCfg.DB.TField(), 
        IDSDCASEMT : new SysCfg.DB.TField(), 
        IDSDCASE : new SysCfg.DB.TField(), 
        IDSDCASEMT_LSSTATUS : new SysCfg.DB.TField(), 
        IDSDCASEMT_LSTYPE : new SysCfg.DB.TField(), 
        CASEMT_LSNAMESTEP : new SysCfg.DB.TField(), 
        CASEMT_LSSTATUSN : new SysCfg.DB.TField(), 
        CASEMT_LSNEXTSTEP : new SysCfg.DB.TField(), 
        CASEMT_LSCOMMENTSST : new SysCfg.DB.TField(), 
        CASEMT_LSCOUNTTIME : new SysCfg.DB.TField(), 
        CASEMT_LSCOUNTTIMEPAUSE : new SysCfg.DB.TField(), 
        CASEMT_LSCOUNTTIMERESOLVED : new SysCfg.DB.TField(), 
        CASEMT_LSDATELASTCUT : new SysCfg.DB.TField() 
    }, 
    SDCASEMT_CITYPE : { 
        NAME_TABLE : "SDCASEMT_CITYPE", 
        IDSDCASEMT_CITYPE : new SysCfg.DB.TField(), 
        SDCASEMT_CITYPENAME : new SysCfg.DB.TField() 
    }, 
    SDCASEMT_CI : { 
        NAME_TABLE : "SDCASEMT_CI", 
        IDSDCASEMT_CI : new SysCfg.DB.TField(), 
        IDSDCASEMT_CITYPE : new SysCfg.DB.TField(), 
        IDSDCASEMT : new SysCfg.DB.TField(), 
        IDCMDBCI : new SysCfg.DB.TField() 
    }, 
    MDCATEGORYDETAIL_CMDBCI : { 
        NAME_TABLE : "MDCATEGORYDETAIL_CMDBCI", 
        IDMDCATEGORYDETAIL_CMDBCI : new SysCfg.DB.TField(), 
        IDMDCATEGORYDETAIL : new SysCfg.DB.TField(), 
        IDCMDBCI : new SysCfg.DB.TField(), 
        IDSYSTEMSTATUS : new SysCfg.DB.TField() 
    }, 
    MDCATEGORYDETAIL_SDCASE : { 
        NAME_TABLE : "MDCATEGORYDETAIL_SDCASE", 
        IDMDCATEGORYDETAIL_SDCASE : new SysCfg.DB.TField(), 
        IDMDCATEGORYDETAIL : new SysCfg.DB.TField(), 
        IDSDCASE : new SysCfg.DB.TField(), 
        IDCMDBCI : new SysCfg.DB.TField(), 
        IDSMWAREVIEW : new SysCfg.DB.TField() 
    }, 
    SMWAREVIEW : { 
        NAME_TABLE : "SMWAREVIEW", 
        IDSMWAREVIEW : new SysCfg.DB.TField(), 
        SMWAREVIEW_NAME : new SysCfg.DB.TField() 
    }, 
    SDWHOTOCASE : { 
        NAME_TABLE : "SDWHOTOCASE", 
        IDSDWHOTOCASE : new SysCfg.DB.TField(), 
        IDSDWHOTOCASESTATUS : new SysCfg.DB.TField(), 
        IDSDCASE : new SysCfg.DB.TField(), 
        IDSDCASEMT : new SysCfg.DB.TField(), 
        IDCMDBCI : new SysCfg.DB.TField(), 
        IDSDTYPEUSER : new SysCfg.DB.TField(), 
        IDSDCASEPERMISSION : new SysCfg.DB.TField(), 
        DATEPERMISSIONENABLE : new SysCfg.DB.TField(), 
        DATEPERMISSIONDISABLE : new SysCfg.DB.TField(), 
        CASEMT_SET_LAVEL : new SysCfg.DB.TField(), 
        COUNTTIME : new SysCfg.DB.TField(), 
        COUNTTIMEPAUSE : new SysCfg.DB.TField(), 
        COUNTTIMERESOLVED : new SysCfg.DB.TField(), 
        DATELASTCUT : new SysCfg.DB.TField(), 
        DATELASTREAD : new SysCfg.DB.TField(), 
        IDSDWHOTOCASETYPE : new SysCfg.DB.TField() 
    }, 
    SDWHOTOCASESTATUS : { 
        NAME_TABLE : "SDWHOTOCASESTATUS", 
        IDSDWHOTOCASESTATUS : new SysCfg.DB.TField(), 
        WHOTOCASENAME : new SysCfg.DB.TField() 
    }, 
    SDWHOTOCASETYPE : { 
        NAME_TABLE : "SDWHOTOCASETYPE", 
        IDSDWHOTOCASETYPE : new SysCfg.DB.TField(), 
        SDWHOTOCASETYPENAME : new SysCfg.DB.TField() 
    }, 
    SDCASESTATUS : { 
        NAME_TABLE : "SDCASESTATUS", 
        IDSDCASESTATUS : new SysCfg.DB.TField(), 
        CASESTATUSNAME : new SysCfg.DB.TField() 
    }, 
    SDCASEMTSTATUS : { 
        NAME_TABLE : "SDCASEMTSTATUS", 
        IDSDCASEMTSTATUS : new SysCfg.DB.TField(), 
        CASEMTSTATUSNAME : new SysCfg.DB.TField() 
    }, 
    SDSCALETYPE_FUNC : { 
        NAME_TABLE : "SDSCALETYPE_FUNC", 
        IDSDSCALETYPE_FUNC : new SysCfg.DB.TField(), 
        SDSCALETYPE_FUNCNAME : new SysCfg.DB.TField() 
    }, 
    SDSCALETYPE_HIER : { 
        NAME_TABLE : "SDSCALETYPE_HIER", 
        IDSDSCALETYPE_HIER : new SysCfg.DB.TField(), 
        SDSCALETYPE_HIERNAME : new SysCfg.DB.TField() 
    }, 
    SDTYPEUSER : { 
        NAME_TABLE : "SDTYPEUSER", 
        IDSDTYPEUSER : new SysCfg.DB.TField(), 
        TYPEUSERNAME : new SysCfg.DB.TField(), 
        TYPEUSERDESCRIPTION : new SysCfg.DB.TField() 
    }, 
    SDOPERATION_POST : { 
        NAME_TABLE : "SDOPERATION_POST", 
        IDSDOPERATION_POST : new SysCfg.DB.TField(), 
        IDSDWHOTOCASE : new SysCfg.DB.TField(), 
        POST_MESSAGE : new SysCfg.DB.TField(), 
        MESSAGE_DATE : new SysCfg.DB.TField(), 
        POST_IDSDTYPEUSER : new SysCfg.DB.TField() 
    }, 
    SDOPERATION_FUNCESC : { 
        NAME_TABLE : "SDOPERATION_FUNCESC", 
        IDSDOPERATION_FUNCESC : new SysCfg.DB.TField(), 
        IDSDWHOTOCASE : new SysCfg.DB.TField(), 
        FUNCESC_MESSAGE : new SysCfg.DB.TField(), 
        IDSDSCALETYPE_FUNC : new SysCfg.DB.TField(), 
        FUNCESC_DATE : new SysCfg.DB.TField(), 
        IDSDOPERATION_ESCTYPE_FUNC : new SysCfg.DB.TField(), 
        FUNCESC_LAVEL : new SysCfg.DB.TField(), 
        FUNCESC_IDCMDBCI : new SysCfg.DB.TField(), 
        FUNCESC_APPLY : new SysCfg.DB.TField() 
    }, 
    SDOPERATION_HIERESC : { 
        NAME_TABLE : "SDOPERATION_HIERESC", 
        IDSDOPERATION_HIERESC : new SysCfg.DB.TField(), 
        IDSDWHOTOCASE : new SysCfg.DB.TField(), 
        HIERESC_MESSAGE : new SysCfg.DB.TField(), 
        IDSDSCALETYPE_HIER : new SysCfg.DB.TField(), 
        HIERESC_DATE : new SysCfg.DB.TField(), 
        IDSDOPERATION_ESCTYPE_HIER : new SysCfg.DB.TField(), 
        HIERESC_LAVEL : new SysCfg.DB.TField(), 
        HIERESC_IDCMDBCI : new SysCfg.DB.TField(), 
        HIERESC_APPLY : new SysCfg.DB.TField() 
    }, 
    SDOPERATION_ESCTYPE : { 
        NAME_TABLE : "SDOPERATION_ESCTYPE", 
        IDSDOPERATION_ESCTYPE : new SysCfg.DB.TField(), 
        ESCTYPEENAME : new SysCfg.DB.TField() 
    }, 
    SDOPERATION_PARENT : { 
        NAME_TABLE : "SDOPERATION_PARENT", 
        IDSDOPERATION_PARENT : new SysCfg.DB.TField(), 
        IDSDCASE_PARENT : new SysCfg.DB.TField(), 
        IDSDCASE : new SysCfg.DB.TField(), 
        IDSDWHOTOCASE : new SysCfg.DB.TField(), 
        PARENT_MESSAGE : new SysCfg.DB.TField(), 
        PARENT_DATE : new SysCfg.DB.TField(), 
        IDSDOPERATION_PARENTTYPE : new SysCfg.DB.TField() 
    }, 
    SDOPERATION_PARENTTYPE : { 
        NAME_TABLE : "SDOPERATION_PARENTTYPE", 
        IDSDOPERATION_PARENTTYPE : new SysCfg.DB.TField(), 
        PARENTTYPENAME : new SysCfg.DB.TField() 
    }, 
    SDOPERATION_CASESTATUS : { 
        NAME_TABLE : "SDOPERATION_CASESTATUS", 
        IDSDOPERATION_CASESTATUS : new SysCfg.DB.TField(), 
        IDSDWHOTOCASE : new SysCfg.DB.TField(), 
        CASESTATUS_MESSAGE : new SysCfg.DB.TField(), 
        CASESTATUS_DATE : new SysCfg.DB.TField(), 
        IDSDCASESTATUS : new SysCfg.DB.TField() 
    }, 
    SDOPERATION_CASESETMODEL : { 
        NAME_TABLE : "SDOPERATION_CASESETMODEL", 
        IDSDOPERATION_CASESETMODEL : new SysCfg.DB.TField(), 
        IDSDWHOTOCASE : new SysCfg.DB.TField(), 
        CASESETMODEL_MESSAGE : new SysCfg.DB.TField(), 
        CASESETMODEL_DATE : new SysCfg.DB.TField(), 
        IDSDCASESETMODELTYPE : new SysCfg.DB.TField(), 
        MT_IDMDMODELTYPED : new SysCfg.DB.TField(), 
        IDSDCASE : new SysCfg.DB.TField(), 
        IDSDCASEMT : new SysCfg.DB.TField(), 
        IDMDCATEGORYDETAIL : new SysCfg.DB.TField(), 
        IDMDMODELTYPED : new SysCfg.DB.TField(), 
        IDMDPRIORITY : new SysCfg.DB.TField(), 
        IDSLA : new SysCfg.DB.TField(), 
        IDMDIMPACT : new SysCfg.DB.TField(), 
        IDMDURGENCY : new SysCfg.DB.TField(), 
        CASE_ISMAYOR : new SysCfg.DB.TField(), 
        CASE_DESCRIPTION : new SysCfg.DB.TField(), 
        CASE_TITLE : new SysCfg.DB.TField() 
    }, 
    SDCASESETMODELTYPE : { 
        NAME_TABLE : "SDCASESETMODELTYPE", 
        IDSDCASESETMODELTYPE : new SysCfg.DB.TField(), 
        SETMODELTYPENAME : new SysCfg.DB.TField() 
    }, 
    SDOPERATION_CASEACTIVITIES : { 
        NAME_TABLE : "SDOPERATION_CASEACTIVITIES", 
        IDSDOPERATION_CASEACTIVITIES : new SysCfg.DB.TField(), 
        IDSDWHOTOCASE : new SysCfg.DB.TField(), 
        CASEACTIVITIES_MESSAGE : new SysCfg.DB.TField(), 
        CASEACTIVITIES_DATE : new SysCfg.DB.TField(), 
        IDSDRUNNIGSTATUS : new SysCfg.DB.TField() 
    }, 
    SDOPERATION_ATTENTION : { 
        NAME_TABLE : "SDOPERATION_ATTENTION", 
        IDSDOPERATION_ATTENTION : new SysCfg.DB.TField(), 
        IDSDWHOTOCASE : new SysCfg.DB.TField(), 
        CASEMT_SET_LS_NAMESTEP : new SysCfg.DB.TField(), 
        CASEMT_SET_LS_STATUSN : new SysCfg.DB.TField(), 
        ATTENTION_MESSAGE : new SysCfg.DB.TField(), 
        ATTENTION_DATE : new SysCfg.DB.TField(), 
        IDSDOPERATION_ATTENTIONTYPE : new SysCfg.DB.TField(), 
        LS_IDSDCASESTATUS : new SysCfg.DB.TField() 
    }, 
    SDOPERATION_ATTENTIONTYPE : { 
        NAME_TABLE : "SDOPERATION_ATTENTIONTYPE", 
        IDSDOPERATION_ATTENTIONTYPE : new SysCfg.DB.TField(), 
        OPERATIONTYPENAME : new SysCfg.DB.TField() 
    }, 
    SDCASE_CMDB_ASSET : { 
        NAME_TABLE : "SDCASE_CMDB_ASSET", 
        IDSDCASE_CMDB_ASSET : new SysCfg.DB.TField(), 
        IDSDCASE : new SysCfg.DB.TField(), 
        IDCMDBCI_AFFECTED : new SysCfg.DB.TField(), 
        DESCRIPTION : new SysCfg.DB.TField(), 
        CMDB_ASSETDATE : new SysCfg.DB.TField(), 
        IDCMDBCI : new SysCfg.DB.TField(), 
        IDSDTYPEUSER : new SysCfg.DB.TField(), 
        IDSYSTEMSTATUS : new SysCfg.DB.TField() 
    }, 
    SDCASE_RELATION : { 
        NAME_TABLE : "SDCASE_RELATION", 
        IDSDCASE_RELATION : new SysCfg.DB.TField(), 
        IDSDCASE_THIS : new SysCfg.DB.TField(), 
        IDSDCASE_OF : new SysCfg.DB.TField(), 
        RELATIONS_TITLE : new SysCfg.DB.TField(), 
        RELATIONS_DESCRIPTION : new SysCfg.DB.TField(), 
        IDSDCASE_RELATIONTYPE : new SysCfg.DB.TField(), 
        IDCMDBCI : new SysCfg.DB.TField(), 
        IDSDTYPEUSER : new SysCfg.DB.TField(), 
        IDSYSTEMSTATUS : new SysCfg.DB.TField() 
    }, 
    SDCASE_RELATIONTYPE : { 
        NAME_TABLE : "SDCASE_RELATIONTYPE", 
        IDSDCASE_RELATIONTYPE : new SysCfg.DB.TField(), 
        RELATIONSTYPENAME : new SysCfg.DB.TField() 
    }, 
    SDCASE_ATTACHMENT : { 
        NAME_TABLE : "SDCASE_ATTACHMENT", 
        IDSDCASE_ATTACHMENT : new SysCfg.DB.TField(), 
        IDSDCASE : new SysCfg.DB.TField(), 
        IDCMDBCI : new SysCfg.DB.TField(), 
        IDSDTYPEUSER : new SysCfg.DB.TField(), 
        IDSYSTEMSTATUS : new SysCfg.DB.TField(), 
        ATTACH : new SysCfg.DB.TField(), 
        PERMISSIONS : new SysCfg.DB.TField() 
    }, 
    SDCASEACTIVITIES : { 
        NAME_TABLE : "SDCASEACTIVITIES", 
        IDSDCASEACTIVITIES : new SysCfg.DB.TField(), 
        IDSDCASEMT_ATVPARENT : new SysCfg.DB.TField(), 
        IDSDCASE_ATVPARENT : new SysCfg.DB.TField(), 
        IDSDCASE : new SysCfg.DB.TField(), 
        LS_NAMESTEP : new SysCfg.DB.TField(), 
        LS_STATUSN : new SysCfg.DB.TField(), 
        MT_RETURNS : new SysCfg.DB.TField(), 
        IDMDMODELTYPED : new SysCfg.DB.TField(), 
        IDSDRUNNIGSTATUS : new SysCfg.DB.TField(), 
        GUIDET : new SysCfg.DB.TField(), 
        COMMENTSL : new SysCfg.DB.TField(), 
        IDSDRUNNIGSOURCEMODEL : new SysCfg.DB.TField(), 
        ACTIVITIES_DATECREATE : new SysCfg.DB.TField(), 
        ACTIVITIES_DATERUNNING : new SysCfg.DB.TField(), 
        ACTIVITIES_DATECLOSED : new SysCfg.DB.TField(), 
        ACTIVITIES_DATECANCELLED : new SysCfg.DB.TField() 
    }, 
    SDRUNNIGSOURCEMODEL : { 
        NAME_TABLE : "SDRUNNIGSOURCEMODEL", 
        IDSDRUNNIGSOURCEMODEL : new SysCfg.DB.TField(), 
        SOURCEMODELNAME : new SysCfg.DB.TField() 
    }, 
    SDRUNNIGSTATUS : { 
        NAME_TABLE : "SDRUNNIGSTATUS", 
        IDSDRUNNIGSTATUS : new SysCfg.DB.TField(), 
        ACTIVITIESNAME : new SysCfg.DB.TField() 
    }, 
    SDCASEPERMISSION : { 
        NAME_TABLE : "SDCASEPERMISSION", 
        IDSDCASEPERMISSION : new SysCfg.DB.TField(), 
        PERMISSIONNAME : new SysCfg.DB.TField(), 
        PERMISSIONDESCRIPTION : new SysCfg.DB.TField() 
    }, 
    CMDBCONTACTTYPE : { 
        NAME_TABLE : "CMDBCONTACTTYPE", 
        IDCMDBCONTACTTYPE : new SysCfg.DB.TField(), 
        TYPENAME : new SysCfg.DB.TField() 
    }, 
    CMDBUSERCONTACTTYPE : { 
        NAME_TABLE : "CMDBUSERCONTACTTYPE", 
        IDCMDBUSERCONTACTTYPE : new SysCfg.DB.TField(), 
        IDCMDBUSER : new SysCfg.DB.TField(), 
        IDCMDBCONTACTTYPE : new SysCfg.DB.TField(), 
        IDSYSTEMSTATUS : new SysCfg.DB.TField(), 
        CONTACTDEFINE : new SysCfg.DB.TField() 
    }, 
    CMDBUSERADDRESS : { 
        NAME_TABLE : "CMDBUSERADDRESS", 
        IDCMDBUSERADDRESS : new SysCfg.DB.TField(), 
        IDCMDBUSER : new SysCfg.DB.TField(), 
        ADDRESS1 : new SysCfg.DB.TField(), 
        ADDRESS2 : new SysCfg.DB.TField(), 
        ADDRESS3 : new SysCfg.DB.TField(), 
        ADDRESS4 : new SysCfg.DB.TField(), 
        ADDRESS5 : new SysCfg.DB.TField(), 
        ADDRESS6 : new SysCfg.DB.TField(), 
        IDSYSTEMSTATUS : new SysCfg.DB.TField() 
    }, 
    CMDBCOUNTRY : { 
        NAME_TABLE : "CMDBCOUNTRY", 
        IDCMDBCOUNTRY : new SysCfg.DB.TField(), 
        COUNTRY_NAME : new SysCfg.DB.TField(), 
        ZONES : new SysCfg.DB.TField(), 
        DST : new SysCfg.DB.TField(), 
        DST_START_DATE : new SysCfg.DB.TField(), 
        DST_END_DATE : new SysCfg.DB.TField(), 
        ALPHA2 : new SysCfg.DB.TField(), 
        ALPHA3 : new SysCfg.DB.TField(), 
        CC_TLD : new SysCfg.DB.TField() 
    }, 
    MDSERVICETYPE : { 
        NAME_TABLE : "MDSERVICETYPE", 
        IDMDSERVICETYPE : new SysCfg.DB.TField(), 
        SERVICETYPENAME : new SysCfg.DB.TField(), 
        SERVICETYPEDESCRIPTION : new SysCfg.DB.TField() 
    }, 
    MDSERVICEEXTRATABLE : { 
        NAME_TABLE : "MDSERVICEEXTRATABLE", 
        IDMDSERVICEEXTRATABLE : new SysCfg.DB.TField(), 
        IDMDSERVICETYPE : new SysCfg.DB.TField(), 
        EXTRATABLE_NAMETABLE : new SysCfg.DB.TField(), 
        EXTRATABLE_IDSOURCE : new SysCfg.DB.TField(), 
        EXTRATABLE_LOG : new SysCfg.DB.TField(), 
        EXTRATABLE_DESCRIPTION : new SysCfg.DB.TField(), 
        EXTRATABLE_NAME : new SysCfg.DB.TField(), 
        EXTRATABLE_ENABLED : new SysCfg.DB.TField(), 
        EXTRATABLE_ORDER : new SysCfg.DB.TField() 
    }, 
    MDSERVICEEXTRAFIELDS : { 
        NAME_TABLE : "MDSERVICEEXTRAFIELDS", 
        IDMDSERVICEEXTRAFIELDS : new SysCfg.DB.TField(), 
        IDMDSERVICETYPE : new SysCfg.DB.TField(), 
        IDCMDBKEYTYPE : new SysCfg.DB.TField(), 
        DBKEYTYPE_NAME : new SysCfg.DB.TField(), 
        IDCMDBDBDATATYPES : new SysCfg.DB.TField(), 
        DBDATATYPES_SIZE : new SysCfg.DB.TField(), 
        EXTRAFIELDS_NAME : new SysCfg.DB.TField(), 
        EXTRAFIELDS_DESCRIPTION : new SysCfg.DB.TField(), 
        IDMDSERVICEEXTRATABLE : new SysCfg.DB.TField(), 
        EXTRAFIELDS_COLUMNSTYLE : new SysCfg.DB.TField(), 
        EXTRAFIELDS_LOOKUPSQL : new SysCfg.DB.TField(), 
        EXTRAFIELDS_LOOKUPID : new SysCfg.DB.TField(), 
        EXTRAFIELDS_LOOKUPDISPLAY : new SysCfg.DB.TField(), 
        EXTRAFIELDS_ORDER : new SysCfg.DB.TField() 
    }, 
    MDGROUP : { 
        NAME_TABLE : "MDGROUP", 
        IDMDGROUP : new SysCfg.DB.TField(), 
        IDSYSTEMSTATUS : new SysCfg.DB.TField(), 
        NAMEGROUP : new SysCfg.DB.TField(), 
        COMMENTSG : new SysCfg.DB.TField() 
    }, 
    MDGROUPUSER : { 
        NAME_TABLE : "MDGROUPUSER", 
        IDMDGROUPUSER : new SysCfg.DB.TField(), 
        IDCMDBCI : new SysCfg.DB.TField(), 
        IDMDGROUP : new SysCfg.DB.TField(), 
        IDSYSTEMSTATUS : new SysCfg.DB.TField() 
    }, 
    MDFUNCESC : { 
        NAME_TABLE : "MDFUNCESC", 
        IDMDFUNCESC : new SysCfg.DB.TField(), 
        TITLEFUNCESC : new SysCfg.DB.TField(), 
        COMMENTSFE : new SysCfg.DB.TField() 
    }, 
    MDFUNCPER : { 
        NAME_TABLE : "MDFUNCPER", 
        IDMDFUNCPER : new SysCfg.DB.TField(), 
        IDMDFUNCESC : new SysCfg.DB.TField(), 
        PERCF : new SysCfg.DB.TField(), 
        TOTAL : new SysCfg.DB.TField(), 
        IDMDGROUP : new SysCfg.DB.TField(), 
        COMMENTSF : new SysCfg.DB.TField(), 
        FUNLAVEL : new SysCfg.DB.TField() 
    }, 
    MDHIERESC : { 
        NAME_TABLE : "MDHIERESC", 
        IDMDHIERESC : new SysCfg.DB.TField(), 
        TITLEHIERESC : new SysCfg.DB.TField(), 
        COMMENTSJE : new SysCfg.DB.TField() 
    }, 
    MDHIERPER : { 
        NAME_TABLE : "MDHIERPER", 
        IDMDHIERPER : new SysCfg.DB.TField(), 
        IDMDHIERESC : new SysCfg.DB.TField(), 
        PERCH : new SysCfg.DB.TField(), 
        TOTAL : new SysCfg.DB.TField(), 
        IDMDGROUP : new SysCfg.DB.TField(), 
        PERMISSIONH : new SysCfg.DB.TField(), 
        COMMENTSH : new SysCfg.DB.TField(), 
        HIERLAVEL : new SysCfg.DB.TField() 
    }, 
    MDLIFESTATUS : { 
        NAME_TABLE : "MDLIFESTATUS", 
        IDMDLIFESTATUS : new SysCfg.DB.TField(), 
        IDMDMODELTYPED : new SysCfg.DB.TField(), 
        STATUSN : new SysCfg.DB.TField(), 
        NAMESTEP : new SysCfg.DB.TField(), 
        NEXTSTEP : new SysCfg.DB.TField(), 
        ENDSTEP : new SysCfg.DB.TField(), 
        COMMENTSST : new SysCfg.DB.TField(), 
        CAUTIONSTEP : new SysCfg.DB.TField(), 
        WARNINGSTEP : new SysCfg.DB.TField(), 
        LS_IDSDCASESTATUS : new SysCfg.DB.TField() 
    }, 
    MDLIFESTATUSPERMISSION : { 
        NAME_TABLE : "MDLIFESTATUSPERMISSION", 
        IDMDLIFESTATUSPERMISSION : new SysCfg.DB.TField(), 
        LIFESTATUSPERMISSION_NAME : new SysCfg.DB.TField() 
    }, 
    MDINTERFACE : { 
        NAME_TABLE : "MDINTERFACE", 
        IDMDINTERFACE : new SysCfg.DB.TField(), 
        INTERFACE_NAME : new SysCfg.DB.TField(), 
        IDMDINTERFACETYPE : new SysCfg.DB.TField(), 
        CUSTOMCODE : new SysCfg.DB.TField() 
    }, 
    MDLIFESTATUSTYPEUSER : { 
        NAME_TABLE : "MDLIFESTATUSTYPEUSER", 
        IDMDLIFESTATUSTYPEUSER : new SysCfg.DB.TField(), 
        IDMDLIFESTATUS : new SysCfg.DB.TField(), 
        IDSDTYPEUSER : new SysCfg.DB.TField(), 
        IDMDLIFESTATUSPERMISSION : new SysCfg.DB.TField(), 
        IDMDINTERFACE : new SysCfg.DB.TField(), 
        IDMDLIFESTATUSBEHAVIOR : new SysCfg.DB.TField(), 
        LSEND_COMMENTSENABLE : new SysCfg.DB.TField(), 
        LSBEGIN_COMMENTSENABLE : new SysCfg.DB.TField(), 
        LSCHANGE_COMMENTS : new SysCfg.DB.TField(), 
        LSEND_COMMENTSLABEL : new SysCfg.DB.TField(), 
        LSBEGIN_COMMENTSLABEL : new SysCfg.DB.TField(), 
        LSHERESTEP_CONFIG : new SysCfg.DB.TField(), 
        LSNEXTSTEP_CONFIG : new SysCfg.DB.TField(), 
        ENABLETIME : new SysCfg.DB.TField(), 
        INCLUDEALLGROUP : new SysCfg.DB.TField(), 
        IDSDNOTIFYTEMPLATE_CONSOLE : new SysCfg.DB.TField(), 
        IDSDNOTIFYTEMPLATE_MAIL : new SysCfg.DB.TField() 
    }, 
    MDLIFESTATUSBEHAVIOR : { 
        NAME_TABLE : "MDLIFESTATUSBEHAVIOR", 
        IDMDLIFESTATUSBEHAVIOR : new SysCfg.DB.TField(), 
        LIFESTATUSBEHAVIOR_NAME : new SysCfg.DB.TField() 
    }, 
    MDINTERFACETYPE : { 
        NAME_TABLE : "MDINTERFACETYPE", 
        IDMDINTERFACETYPE : new SysCfg.DB.TField(), 
        INTERFACETYPE_NAME : new SysCfg.DB.TField() 
    }, 
    MDLIFESTATUSSTEXTRATABLE : { 
        NAME_TABLE : "MDLIFESTATUSSTEXTRATABLE", 
        IDMDLIFESTATUSSTEXTRATABLE : new SysCfg.DB.TField(), 
        IDMDLIFESTATUSTYPEUSER : new SysCfg.DB.TField(), 
        IDMDLIFESTATUS : new SysCfg.DB.TField(), 
        IDMDSERVICETYPE : new SysCfg.DB.TField(), 
        IDMDINTERFACE : new SysCfg.DB.TField(), 
        IDMDSERVICEEXTRATABLE : new SysCfg.DB.TField(), 
        LSCHANGETABLE_COMMENTS : new SysCfg.DB.TField() 
    }, 
    MDLIFESTATUSSTEXTRAFIELDS : { 
        NAME_TABLE : "MDLIFESTATUSSTEXTRAFIELDS", 
        IDMDLIFESTATUSSTEXTRAFIELDS : new SysCfg.DB.TField(), 
        IDMDLIFESTATUSSTEXTRATABLE : new SysCfg.DB.TField(), 
        IDMDSERVICEEXTRAFIELDS : new SysCfg.DB.TField(), 
        IDMDLIFESTATUSPERMISSION : new SysCfg.DB.TField(), 
        MANDATORY : new SysCfg.DB.TField() 
    }, 
    MDLIFESTATUSCIEXTRATABLE : { 
        NAME_TABLE : "MDLIFESTATUSCIEXTRATABLE", 
        IDMDLIFESTATUSCIEXTRATABLE : new SysCfg.DB.TField(), 
        IDMDLIFESTATUSTYPEUSER : new SysCfg.DB.TField(), 
        IDMDLIFESTATUS : new SysCfg.DB.TField(), 
        IDCMDBCIDEFINE : new SysCfg.DB.TField(), 
        IDMDINTERFACE : new SysCfg.DB.TField(), 
        IDCMDBCIDEFINEEXTRATABLE : new SysCfg.DB.TField() 
    }, 
    MDLIFESTATUSCIEXTRAFIELDS : { 
        NAME_TABLE : "MDLIFESTATUSCIEXTRAFIELDS", 
        IDMDLIFESTATUSCIEXTRAFIELDS : new SysCfg.DB.TField(), 
        IDMDLIFESTATUSCIEXTRATABLE : new SysCfg.DB.TField(), 
        IDCMDBCIDEFINEEXTRAFIELDS : new SysCfg.DB.TField(), 
        IDMDLIFESTATUSPERMISSION : new SysCfg.DB.TField(), 
        MANDATORY : new SysCfg.DB.TField() 
    }, 
    MDCASESTEXTRATABLE : { 
        NAME_TABLE : "MDCASESTEXTRATABLE", 
        IDMDCASESTEXTRATABLE : new SysCfg.DB.TField(), 
        IDMDMODELTYPED : new SysCfg.DB.TField(), 
        IDMDSERVICETYPE : new SysCfg.DB.TField(), 
        IDMDINTERFACE : new SysCfg.DB.TField(), 
        IDMDSERVICEEXTRATABLE : new SysCfg.DB.TField(), 
        LSCHANGETABLE_COMMENTS : new SysCfg.DB.TField() 
    }, 
    MDCASESTEXTRAFIELDS : { 
        NAME_TABLE : "MDCASESTEXTRAFIELDS", 
        IDMDCASESTEXTRAFIELDS : new SysCfg.DB.TField(), 
        IDMDCASESTEXTRATABLE : new SysCfg.DB.TField(), 
        IDMDSERVICEEXTRAFIELDS : new SysCfg.DB.TField(), 
        IDMDLIFESTATUSPERMISSION : new SysCfg.DB.TField(), 
        MANDATORY : new SysCfg.DB.TField() 
    }, 
    MDMODELTYPED : { 
        NAME_TABLE : "MDMODELTYPED", 
        IDMDMODELTYPED : new SysCfg.DB.TField(), 
        TITLEM : new SysCfg.DB.TField(), 
        COMMENTSM : new SysCfg.DB.TField(), 
        IDMDSERVICETYPE : new SysCfg.DB.TField(), 
        IDMDMODELTYPED1 : new SysCfg.DB.TField(), 
        OPEARTIONM : new SysCfg.DB.TField(), 
        IDMDHIERESC : new SysCfg.DB.TField(), 
        IDMDFUNCESC : new SysCfg.DB.TField(), 
        NORMALTIME : new SysCfg.DB.TField(), 
        MAXTIME : new SysCfg.DB.TField(), 
        GUIDETEXT : new SysCfg.DB.TField(), 
        POSSIBLERETURNS : new SysCfg.DB.TField(), 
        STEPVALIDATE : new SysCfg.DB.TField(), 
        TIMEENABLE : new SysCfg.DB.TField(), 
        STEEPTYPEUSERENABLE : new SysCfg.DB.TField(), 
        IDCMDBCIDEFINE : new SysCfg.DB.TField() 
    }, 
    MDMATRIXMODELS : { 
        NAME_TABLE : "MDMATRIXMODELS", 
        IDMDMATRIXMODELS : new SysCfg.DB.TField(), 
        IDMDMODELTYPED1 : new SysCfg.DB.TField(), 
        POSINSTEP : new SysCfg.DB.TField(), 
        IDMDMODELTYPED2 : new SysCfg.DB.TField(), 
        GUIDET : new SysCfg.DB.TField(), 
        IDMDLIFESTATUS : new SysCfg.DB.TField(), 
        COMMENTSL : new SysCfg.DB.TField(), 
        MODELVALIDATE : new SysCfg.DB.TField() 
    }, 
    MDCATEGORY : { 
        NAME_TABLE : "MDCATEGORY", 
        IDMDCATEGORY : new SysCfg.DB.TField(), 
        CATEGORY1 : new SysCfg.DB.TField(), 
        CATEGORY2 : new SysCfg.DB.TField(), 
        CATEGORY3 : new SysCfg.DB.TField(), 
        CATEGORY4 : new SysCfg.DB.TField(), 
        CATEGORY5 : new SysCfg.DB.TField(), 
        CATEGORY6 : new SysCfg.DB.TField(), 
        CATEGORY7 : new SysCfg.DB.TField(), 
        CATEGORY8 : new SysCfg.DB.TField() 
    }, 
    MDCATEGORYDETAIL : { 
        NAME_TABLE : "MDCATEGORYDETAIL", 
        IDMDCATEGORYDETAIL : new SysCfg.DB.TField(), 
        IDMDCATEGORY : new SysCfg.DB.TField(), 
        CATEGORYNAME : new SysCfg.DB.TField(), 
        CATEGORYDESCRIPTION : new SysCfg.DB.TField(), 
        CATEGORYSTATUS : new SysCfg.DB.TField(), 
        CUSTOMCODE : new SysCfg.DB.TField() 
    }, 
    MDCATEGORYDETAIL_TYPEUSER : { 
        NAME_TABLE : "MDCATEGORYDETAIL_TYPEUSER", 
        IDMDCATEGORYDETAIL_TYPEUSER : new SysCfg.DB.TField(), 
        IDSDTYPEUSER : new SysCfg.DB.TField(), 
        IDMDCATEGORYDETAIL : new SysCfg.DB.TField() 
    }, 
    MDIMPACT : { 
        NAME_TABLE : "MDIMPACT", 
        IDMDIMPACT : new SysCfg.DB.TField(), 
        IMPACTNAME : new SysCfg.DB.TField() 
    }, 
    MDPRIORITY : { 
        NAME_TABLE : "MDPRIORITY", 
        IDMDPRIORITY : new SysCfg.DB.TField(), 
        PRIORITYNAME : new SysCfg.DB.TField() 
    }, 
    MDURGENCY : { 
        NAME_TABLE : "MDURGENCY", 
        IDMDURGENCY : new SysCfg.DB.TField(), 
        URGENCYNAME : new SysCfg.DB.TField() 
    }, 
    MDPRIORITYMATRIX : { 
        NAME_TABLE : "MDPRIORITYMATRIX", 
        IDMDIMPACT : new SysCfg.DB.TField(), 
        IDMDURGENCY : new SysCfg.DB.TField(), 
        IDMDPRIORITY : new SysCfg.DB.TField() 
    }, 
    MDSLA : { 
        NAME_TABLE : "MDSLA", 
        IDMDSLA : new SysCfg.DB.TField(), 
        IDMDMODELTYPED : new SysCfg.DB.TField(), 
        IDCALEDAYDATE : new SysCfg.DB.TField(), 
        IDMDIMPACT : new SysCfg.DB.TField(), 
        SLANAME : new SysCfg.DB.TField(), 
        SLADESCRIPTION : new SysCfg.DB.TField(), 
        SLAPARENT : new SysCfg.DB.TField(), 
        SLACONDITION : new SysCfg.DB.TField(), 
        SLA_MAXTIME : new SysCfg.DB.TField(), 
        SLA_NORMALTIME : new SysCfg.DB.TField(), 
        IDSDSCALETYPE_FUNC : new SysCfg.DB.TField(), 
        IDSDSCALETYPE_HIER : new SysCfg.DB.TField(), 
        SLAORDER : new SysCfg.DB.TField(), 
        SLASTATUS : new SysCfg.DB.TField() 
    }, 
    MDCONFIG : { 
        NAME_TABLE : "MDCONFIG", 
        IDMDCONFIG : new SysCfg.DB.TField(), 
        IDMDCATEGORYDETAIL_DEFAULT : new SysCfg.DB.TField(), 
        IDMDURGENCY_DEFAULT : new SysCfg.DB.TField() 
    }, 
    CPU : { 
        NAME_TABLE : "CPU", 
        IDCPU : new SysCfg.DB.TField(), 
        MARCA_DE_CPU : new SysCfg.DB.TField(), 
        MODELO_DE_CPU : new SysCfg.DB.TField(), 
        SERIAL_DE_CPU : new SysCfg.DB.TField(), 
        MEMORIA_TOTAL_SIST : new SysCfg.DB.TField(), 
        MEMORIA_TOTAL_CPU : new SysCfg.DB.TField(), 
        MEMORIA_DISPONIBLE : new SysCfg.DB.TField(), 
        MEMORIA_VIRTUAL : new SysCfg.DB.TField(), 
        MEMORIA_VIRTUAL_DISPONIBLE : new SysCfg.DB.TField(), 
        TIENE_CD : new SysCfg.DB.TField(), 
        FABRICANTE_DEL_PROCESADOR : new SysCfg.DB.TField(), 
        NOMBRE_DEL_PROCESADOR : new SysCfg.DB.TField(), 
        TIPO_DEL_PROCESADOR : new SysCfg.DB.TField(), 
        VELOCIDAD_PROCESADOR : new SysCfg.DB.TField(), 
        CRONO_PROCESADOR : new SysCfg.DB.TField(), 
        PROCESADOR : new SysCfg.DB.TField(), 
        NUMERO_DE_PROCESADORES : new SysCfg.DB.TField(), 
        NOMBRE_DE_BIOS : new SysCfg.DB.TField(), 
        VERSION_DE_BIOS : new SysCfg.DB.TField(), 
        FECHA_DE_BIOS : new SysCfg.DB.TField(), 
        BRANDID : new SysCfg.DB.TField(), 
        HYPER : new SysCfg.DB.TField() 
    }, 
    ESTACION_RED : { 
        NAME_TABLE : "ESTACION_RED", 
        IDCPU : new SysCfg.DB.TField(), 
        NOMBRE_ESTACION : new SysCfg.DB.TField(), 
        DESCRIPCION : new SysCfg.DB.TField(), 
        NOMBRE_USUARIO : new SysCfg.DB.TField(), 
        GRUPO_DE_TRABAJO : new SysCfg.DB.TField(), 
        DOMINIO : new SysCfg.DB.TField(), 
        SERVIDOR : new SysCfg.DB.TField(), 
        REGISTRADO_POR : new SysCfg.DB.TField(), 
        ORGANIZACION : new SysCfg.DB.TField(), 
        HOST_NAME : new SysCfg.DB.TField() 
    }, 
    WINDOWS : { 
        NAME_TABLE : "WINDOWS", 
        IDCPU : new SysCfg.DB.TField(), 
        WINDOWS : new SysCfg.DB.TField(), 
        TIPO_SO : new SysCfg.DB.TField(), 
        KERNEL : new SysCfg.DB.TField(), 
        SUITE : new SysCfg.DB.TField(), 
        EXTRA_DATA : new SysCfg.DB.TField(), 
        VERSION_WINDOWS : new SysCfg.DB.TField(), 
        SERIAL_WIN : new SysCfg.DB.TField(), 
        WINSOCK : new SysCfg.DB.TField(), 
        IDIOM : new SysCfg.DB.TField(), 
        INSTALL_DATE : new SysCfg.DB.TField() 
    }, 
    VPRO : { 
        NAME_TABLE : "VPRO", 
        IDVPRO : new SysCfg.DB.TField(), 
        IDCPU : new SysCfg.DB.TField(), 
        VPRONAME : new SysCfg.DB.TField(), 
        VPROIP : new SysCfg.DB.TField(), 
        VPROCOMMENT : new SysCfg.DB.TField() 
    }, 
    AGENTE : { 
        NAME_TABLE : "AGENTE", 
        IDCPU : new SysCfg.DB.TField(), 
        VERSION : new SysCfg.DB.TField(), 
        VERSION_HEAR : new SysCfg.DB.TField(), 
        ULTIMA_GENERACION : new SysCfg.DB.TField(), 
        ULTIMA_RECEPCION : new SysCfg.DB.TField(), 
        NINVENT : new SysCfg.DB.TField(), 
        CLIENTTYPE : new SysCfg.DB.TField(), 
        DIRS_DATE : new SysCfg.DB.TField(), 
        AG_STATUS : new SysCfg.DB.TField(), 
        COMPANY : new SysCfg.DB.TField(), 
        SSLSERVER : new SysCfg.DB.TField(), 
        SLICENSE : new SysCfg.DB.TField(), 
        ID_COMPANY_L : new SysCfg.DB.TField(), 
        ALL_DATE : new SysCfg.DB.TField(), 
        A_CONNECTED : new SysCfg.DB.TField() 
    }, 
    USERS : { 
        NAME_TABLE : "USERS", 
        IDCPU : new SysCfg.DB.TField(), 
        NOMBREE : new SysCfg.DB.TField(), 
        APELLIDOE : new SysCfg.DB.TField(), 
        DIRECCION : new SysCfg.DB.TField(), 
        TELEFONO : new SysCfg.DB.TField(), 
        UBICACION : new SysCfg.DB.TField(), 
        MAIL : new SysCfg.DB.TField(), 
        CEMPLEADO : new SysCfg.DB.TField() 
    }, 
    IP : { 
        NAME_TABLE : "IP", 
        IDCPU : new SysCfg.DB.TField(), 
        ADAPTADOR_IP : new SysCfg.DB.TField(), 
        DIRECCION_IP : new SysCfg.DB.TField(), 
        NETCARDTYPE_IP : new SysCfg.DB.TField(), 
        MASK_IP : new SysCfg.DB.TField(), 
        GATEWAY_IP : new SysCfg.DB.TField(), 
        MAC_IP : new SysCfg.DB.TField(), 
        DHCP_ON : new SysCfg.DB.TField(), 
        DHCP_IP : new SysCfg.DB.TField(), 
        DNS1 : new SysCfg.DB.TField(), 
        DNS2 : new SysCfg.DB.TField() 
    }, 
    EXTRADATA : { 
        NAME_TABLE : "EXTRADATA", 
        IDCPU : new SysCfg.DB.TField(), 
        NOMBREE : new SysCfg.DB.TField(), 
        APELLIDOE : new SysCfg.DB.TField(), 
        DIRECCION : new SysCfg.DB.TField(), 
        TELEFONO : new SysCfg.DB.TField(), 
        UBICACION : new SysCfg.DB.TField(), 
        MAIL : new SysCfg.DB.TField(), 
        CEMPLEADO : new SysCfg.DB.TField(), 
        AYUNOMBRE : new SysCfg.DB.TField(), 
        AYUTELEFONO : new SysCfg.DB.TField(), 
        AYUPROBLEMA : new SysCfg.DB.TField(), 
        USERS : new SysCfg.DB.TField(), 
        EXTRA_DATE : new SysCfg.DB.TField() 
    }, 
    IP_SERVERS : { 
        NAME_TABLE : "IP_SERVERS", 
        COMPANY_NAME : new SysCfg.DB.TField(), 
        IP_MIRROR : new SysCfg.DB.TField(), 
        IP_SECURE_SERVER : new SysCfg.DB.TField(), 
        IP_CONTROLR : new SysCfg.DB.TField() 
    }, 
    RHGROUPCR : { 
        NAME_TABLE : "RHGROUPCR", 
        IDRHGROUPCR : new SysCfg.DB.TField(), 
        RHGROUPCR_NAME : new SysCfg.DB.TField(), 
        RHGROUPCR_DESCRIPTION : new SysCfg.DB.TField() 
    }, 
    RHGROUPCRCPU : { 
        NAME_TABLE : "RHGROUPCRCPU", 
        IDRHGROUPCRCPU : new SysCfg.DB.TField(), 
        IDCPU : new SysCfg.DB.TField(), 
        IDRHGROUPCR : new SysCfg.DB.TField() 
    }, 
    RHGROUPCRPERMISSION : { 
        NAME_TABLE : "RHGROUPCRPERMISSION", 
        IDRHGROUPCRPERMISSION : new SysCfg.DB.TField(), 
        PERMISSION : new SysCfg.DB.TField() 
    }, 
    RHGROUPCRATROLEPERMISSION : { 
        NAME_TABLE : "RHGROUPCRATROLEPERMISSION", 
        IDRHGROUPCRATROLEPERMISSION : new SysCfg.DB.TField(), 
        IDATROLE : new SysCfg.DB.TField(), 
        IDRHGROUPCR : new SysCfg.DB.TField(), 
        IDRHGROUPCRPERMISSION : new SysCfg.DB.TField() 
    }, 
    RHGROUPPL_POLICY : { 
        NAME_TABLE : "RHGROUPPL_POLICY", 
        IDRHGROUPPL_POLICY : new SysCfg.DB.TField(), 
        POLICY_NAME : new SysCfg.DB.TField() 
    }, 
    RHGROUPPL_CLI_CONFIG : { 
        NAME_TABLE : "RHGROUPPL_CLI_CONFIG", 
        IDRHGROUPPL_CLI_CONFIG : new SysCfg.DB.TField(), 
        CLI_CONFIG_NAME : new SysCfg.DB.TField() 
    }, 
    RHGROUPPL_CFGBACKUP : { 
        NAME_TABLE : "RHGROUPPL_CFGBACKUP", 
        IDRHGROUPPL_CFGBACKUP : new SysCfg.DB.TField(), 
        CFGBACKUP_NAME : new SysCfg.DB.TField() 
    }, 
    RHGROUPPL : { 
        NAME_TABLE : "RHGROUPPL", 
        IDRHGROUPPL : new SysCfg.DB.TField(), 
        GROUPPL_NAME : new SysCfg.DB.TField(), 
        GROUPPL_DESCRIPTION : new SysCfg.DB.TField(), 
        IDRHGROUPPL_POLICY : new SysCfg.DB.TField(), 
        IDRHGROUPPL_CLI_CONFIG : new SysCfg.DB.TField(), 
        IDRHGROUPPL_CFGBACKUP : new SysCfg.DB.TField() 
    }, 
    RHGROUPPLCPU : { 
        NAME_TABLE : "RHGROUPPLCPU", 
        IDRHGROUPPLCPU : new SysCfg.DB.TField(), 
        IDCPU : new SysCfg.DB.TField(), 
        IDRHGROUPPL : new SysCfg.DB.TField() 
    }, 
    RHGROUPPLATROLE : { 
        NAME_TABLE : "RHGROUPPLATROLE", 
        IDRHGROUPPLATROLE : new SysCfg.DB.TField(), 
        IDATROLE : new SysCfg.DB.TField(), 
        IDRHGROUPPL : new SysCfg.DB.TField() 
    }, 
    RHREQUEST : { 
        NAME_TABLE : "RHREQUEST", 
        IDRHREQUEST : new SysCfg.DB.TField(), 
        IDATROLE : new SysCfg.DB.TField(), 
        IDCMDBCI : new SysCfg.DB.TField(), 
        IDCMDBUSER : new SysCfg.DB.TField(), 
        EVENTDATE : new SysCfg.DB.TField(), 
        REMOTEHELPFUNTION : new SysCfg.DB.TField(), 
        AUX : new SysCfg.DB.TField(), 
        CONNECTBYHOST : new SysCfg.DB.TField(), 
        INTERNETCONNECTION : new SysCfg.DB.TField(), 
        JUSTSEE : new SysCfg.DB.TField(), 
        DONOTASKFORPERMISSION : new SysCfg.DB.TField(), 
        LOCKKEYBOARD : new SysCfg.DB.TField(), 
        LOWBANDWIDTH : new SysCfg.DB.TField(), 
        RECORDSESSION : new SysCfg.DB.TField(), 
        REQUEST_STATUS : new SysCfg.DB.TField(), 
        IDRHMODULE_EXECUTE : new SysCfg.DB.TField(), 
        IDRHMODULE_CREATE : new SysCfg.DB.TField(), 
        IDRHTYPEEXECUTE : new SysCfg.DB.TField() 
    }, 
    RHREQUEST_CPUPL : { 
        NAME_TABLE : "RHREQUEST_CPUPL", 
        IDRHREQUEST_CPUPL : new SysCfg.DB.TField(), 
        IDCPU : new SysCfg.DB.TField(), 
        IDRHREQUEST : new SysCfg.DB.TField(), 
        HOSTLIST : new SysCfg.DB.TField(), 
        HOSTVALID : new SysCfg.DB.TField(), 
        REQUEST_CPUPL_STATUS : new SysCfg.DB.TField(), 
        IDRHGROUPPL : new SysCfg.DB.TField(), 
        PERMISSION : new SysCfg.DB.TField() 
    }, 
    RHREQUEST_CPUCR : { 
        NAME_TABLE : "RHREQUEST_CPUCR", 
        IDRHREQUEST_CPUCR : new SysCfg.DB.TField(), 
        IDCPU : new SysCfg.DB.TField(), 
        IDRHREQUEST : new SysCfg.DB.TField(), 
        HOSTLIST : new SysCfg.DB.TField(), 
        HOSTVALID : new SysCfg.DB.TField(), 
        REQUEST_CPUCR_STATUS : new SysCfg.DB.TField(), 
        IDRHGROUPCR : new SysCfg.DB.TField(), 
        IDRHGROUPCRPERMISSION : new SysCfg.DB.TField() 
    }, 
    MDCALENDARYGROUPTYPE : { 
        NAME_TABLE : "MDCALENDARYGROUPTYPE", 
        IDMDCALENDARYGROUPTYPE : new SysCfg.DB.TField(), 
        GROUPTYPEPNAME : new SysCfg.DB.TField() 
    }, 
    MDCALENDARYDATERULE : { 
        NAME_TABLE : "MDCALENDARYDATERULE", 
        IDMDCALENDARYDATERULE : new SysCfg.DB.TField(), 
        RULENAME : new SysCfg.DB.TField(), 
        RULEDESCRIPTION : new SysCfg.DB.TField() 
    }, 
    MDCALENDARYTIME : { 
        NAME_TABLE : "MDCALENDARYTIME", 
        IDMDCALENDARYTIME : new SysCfg.DB.TField(), 
        TIMESTATUS : new SysCfg.DB.TField(), 
        TIMENAME : new SysCfg.DB.TField() 
    }, 
    MDCALENDARYTIMEDETAIL : { 
        NAME_TABLE : "MDCALENDARYTIMEDETAIL", 
        IDMDCALENDARYTIMEDETAIL : new SysCfg.DB.TField(), 
        IDMDCALENDARYTIME : new SysCfg.DB.TField(), 
        TIMESTART : new SysCfg.DB.TField(), 
        TIMEEND : new SysCfg.DB.TField(), 
        TIMEDESCRIPTION : new SysCfg.DB.TField() 
    }, 
    MDCALENDARYDATE : { 
        NAME_TABLE : "MDCALENDARYDATE", 
        IDMDCALENDARYDATE : new SysCfg.DB.TField(), 
        DATENAME : new SysCfg.DB.TField(), 
        DATESTATUS : new SysCfg.DB.TField(), 
        DATEORDER : new SysCfg.DB.TField() 
    }, 
    MDCALENDARYDATEDETAIL : { 
        NAME_TABLE : "MDCALENDARYDATEDETAIL", 
        IDMDCALENDARYDATEDETAIL : new SysCfg.DB.TField(), 
        IDMDCALENDARYDATE : new SysCfg.DB.TField(), 
        IDMDCALENDARYTIME : new SysCfg.DB.TField(), 
        IDMDCALENDARYGROUP : new SysCfg.DB.TField(), 
        IDMDCALENDARYDATERULE : new SysCfg.DB.TField(), 
        DATEDESCRIPTION : new SysCfg.DB.TField(), 
        DATESTART : new SysCfg.DB.TField(), 
        DATEEND : new SysCfg.DB.TField(), 
        DATEINCLUDE : new SysCfg.DB.TField(), 
        DATEORDER : new SysCfg.DB.TField() 
    }, 
    MDCALENDARYGROUP : { 
        NAME_TABLE : "MDCALENDARYGROUP", 
        IDMDCALENDARYGROUP : new SysCfg.DB.TField(), 
        MDCALENDARYGROUPTYPE : new SysCfg.DB.TField(), 
        IDMDCALENDARYDATE : new SysCfg.DB.TField(), 
        IDMDCALENDARYTIME : new SysCfg.DB.TField(), 
        GROUPNAME : new SysCfg.DB.TField(), 
        GROUPSTATUS : new SysCfg.DB.TField() 
    }, 
    GPQUERYTYPE : { 
        NAME_TABLE : "GPQUERYTYPE", 
        IDGPQUERYTYPE : new SysCfg.DB.TField(), 
        QUERYTYPENAME : new SysCfg.DB.TField() 
    }, 
    GPQUERY : { 
        NAME_TABLE : "GPQUERY", 
        IDGPQUERY : new SysCfg.DB.TField(), 
        IDGPQUERYTYPE : new SysCfg.DB.TField(), 
        GPQUERY_NAME : new SysCfg.DB.TField(), 
        GPQUERY_DESCRIPTION : new SysCfg.DB.TField(), 
        STRSQL : new SysCfg.DB.TField(), 
        PATH : new SysCfg.DB.TField(), 
        OPEN_TYPENAME : new SysCfg.DB.TField(), 
        OPEN_PREFIX : new SysCfg.DB.TField(), 
        OPEN_ID : new SysCfg.DB.TField(), 
        IDATUSER : new SysCfg.DB.TField(), 
        START : new SysCfg.DB.TField() 
    }, 
    GPVIEW : { 
        NAME_TABLE : "GPVIEW", 
        IDGPVIEW : new SysCfg.DB.TField(), 
        IDGPQUERY : new SysCfg.DB.TField(), 
        GPVIEW_NAME : new SysCfg.DB.TField(), 
        GPVIEW_DESCRIPTION : new SysCfg.DB.TField(), 
        GPVIEW_ISACTIVE : new SysCfg.DB.TField(), 
        GPVIEW_ISPUBLIC : new SysCfg.DB.TField(), 
        IDCMDBUSER : new SysCfg.DB.TField(), 
        GPVIEW_FILTER : new SysCfg.DB.TField() 
    }, 
    GPCOLUMNVIEW : { 
        NAME_TABLE : "GPCOLUMNVIEW", 
        IDGPCOLUMNVIEW : new SysCfg.DB.TField(), 
        IDGPVIEW : new SysCfg.DB.TField(), 
        TABLE_NAME : new SysCfg.DB.TField(), 
        FIELD_NAME : new SysCfg.DB.TField(), 
        HEADER : new SysCfg.DB.TField(), 
        POSITION : new SysCfg.DB.TField(), 
        SIZE : new SysCfg.DB.TField(), 
        ISVISIBLE : new SysCfg.DB.TField(), 
        ISVISIBLEDETAIL : new SysCfg.DB.TField(), 
        COLOR : new SysCfg.DB.TField(), 
        ISGROUP : new SysCfg.DB.TField(), 
        GROUP_POSITION : new SysCfg.DB.TField(), 
        SORT : new SysCfg.DB.TField() 
    }, 
    GPVALUECOLOR : { 
        NAME_TABLE : "GPVALUECOLOR", 
        IDGPVALUECOLOR : new SysCfg.DB.TField(), 
        IDGPCOLUMNVIEW : new SysCfg.DB.TField(), 
        NAME : new SysCfg.DB.TField(), 
        CONDITION : new SysCfg.DB.TField(), 
        COLOR : new SysCfg.DB.TField() 
    }, 
    GPEDIT_COLUMN : { 
        NAME_TABLE : "GPEDIT_COLUMN", 
        IDGPEDIT_COLUMN : new SysCfg.DB.TField(), 
        IDGPQUERY : new SysCfg.DB.TField(), 
        EDIT_COLUMNNAME : new SysCfg.DB.TField(), 
        EDIT_COLUMNTYPE : new SysCfg.DB.TField(), 
        MYCONSTRAINT : new SysCfg.DB.TField(), 
        AUTOINCTABLENAME : new SysCfg.DB.TField(), 
        COLUMNSTYLE : new SysCfg.DB.TField(), 
        LOOKUPSQL : new SysCfg.DB.TField(), 
        LOOKUPDISPLAYCOLUMN : new SysCfg.DB.TField(), 
        LOOKUPDISPLAYCOLUMNLIST : new SysCfg.DB.TField(), 
        ACTIVE : new SysCfg.DB.TField(), 
        FILETYPE : new SysCfg.DB.TField() 
    }, 
    GPEDIT_TABLE : { 
        NAME_TABLE : "GPEDIT_TABLE", 
        IDGPEDIT_TABLE : new SysCfg.DB.TField(), 
        IDGPQUERY : new SysCfg.DB.TField(), 
        EDIT_TABLENAME : new SysCfg.DB.TField(), 
        EDIT_TABLEORDER : new SysCfg.DB.TField() 
    }, 
    GPEDIT_COLUMNEVENT : { 
        NAME_TABLE : "GPEDIT_COLUMNEVENT", 
        IDGPEDIT_COLUMNEVENT : new SysCfg.DB.TField(), 
        IDGPEDIT_COLUMN : new SysCfg.DB.TField(), 
        IDGPEDIT_TABLE : new SysCfg.DB.TField(), 
        EVINSERT : new SysCfg.DB.TField(), 
        EVDELETE : new SysCfg.DB.TField(), 
        EVUPDATESET : new SysCfg.DB.TField(), 
        EVUPDATEWHERE : new SysCfg.DB.TField() 
    }, 
    GPCHART : { 
        NAME_TABLE : "GPCHART", 
        IDGPCHART : new SysCfg.DB.TField(), 
        IDGPQUERY : new SysCfg.DB.TField(), 
        NAME : new SysCfg.DB.TField(), 
        DESCRIPTION : new SysCfg.DB.TField(), 
        STRSQL : new SysCfg.DB.TField(), 
        XFIELDNAME : new SysCfg.DB.TField(), 
        YFIELDNAME : new SysCfg.DB.TField(), 
        SERIESGROUP : new SysCfg.DB.TField(), 
        ISGAUGE : new SysCfg.DB.TField(), 
        ISCHART : new SysCfg.DB.TField(), 
        MINVALUE : new SysCfg.DB.TField(), 
        MAXVALUE : new SysCfg.DB.TField(), 
        GAUGERANGE : new SysCfg.DB.TField(), 
        GAUGETYPE : new SysCfg.DB.TField(), 
        CONFIGURATION : new SysCfg.DB.TField() 
    }, 
    GPREPORTSQL : { 
        NAME_TABLE : "GPREPORTSQL", 
        IDGPREPORTSQL : new SysCfg.DB.TField(), 
        IDGPQUERY : new SysCfg.DB.TField(), 
        NAME : new SysCfg.DB.TField(), 
        DESCRIPTION : new SysCfg.DB.TField(), 
        TABLENAME : new SysCfg.DB.TField(), 
        STRSQL : new SysCfg.DB.TField() 
    }, 
    GPREPORTTEMPLATE : { 
        NAME_TABLE : "GPREPORTTEMPLATE", 
        IDGPREPORTTEMPLATE : new SysCfg.DB.TField(), 
        IDGPQUERY : new SysCfg.DB.TField(), 
        NAME : new SysCfg.DB.TField(), 
        DESCRIPTION : new SysCfg.DB.TField(), 
        CODE : new SysCfg.DB.TField(), 
        FILENAME : new SysCfg.DB.TField(), 
        MULTIPLEFILE : new SysCfg.DB.TField() 
    }, 
    ATROLE : { 
        NAME_TABLE : "ATROLE", 
        IDATROLE : new SysCfg.DB.TField(), 
        ROLENAME : new SysCfg.DB.TField(), 
        REMOTEHELPCONFIG : new SysCfg.DB.TField(), 
        IDPSGROUP : new SysCfg.DB.TField(), 
        IDPBITEMPLATE : new SysCfg.DB.TField(), 
        SETHOME : new SysCfg.DB.TField(), 
        PBITEMPLATEMULTIPLE : new SysCfg.DB.TField(), 
        PSGROUPMULTIPLE : new SysCfg.DB.TField() 
    }, 
    ATMODULE : { 
        NAME_TABLE : "ATMODULE", 
        IDATMODULE : new SysCfg.DB.TField(), 
        MODULENAME : new SysCfg.DB.TField(), 
        STATUS : new SysCfg.DB.TField() 
    }, 
    ATROLEMODULE : { 
        NAME_TABLE : "ATROLEMODULE", 
        IDATROLEMODULE : new SysCfg.DB.TField(), 
        IDATROLE : new SysCfg.DB.TField(), 
        IDATMODULE : new SysCfg.DB.TField(), 
        REQUEST : new SysCfg.DB.TField() 
    }, 
    CMDBUSER : { 
        NAME_TABLE : "CMDBUSER", 
        IDCMDBUSER : new SysCfg.DB.TField(), 
        IDCMDBCI : new SysCfg.DB.TField(), 
        IDATROLE : new SysCfg.DB.TField(), 
        PASSWORD : new SysCfg.DB.TField(), 
        PASSWORD01 : new SysCfg.DB.TField(), 
        PASSWORD02 : new SysCfg.DB.TField(), 
        PASSWORD03 : new SysCfg.DB.TField(), 
        PASSWORD04 : new SysCfg.DB.TField(), 
        PASSWORD05 : new SysCfg.DB.TField(), 
        PASSWORD06 : new SysCfg.DB.TField(), 
        PASSWORD07 : new SysCfg.DB.TField(), 
        PASSWORD08 : new SysCfg.DB.TField(), 
        PASSWORD09 : new SysCfg.DB.TField(), 
        PASSWORD10 : new SysCfg.DB.TField(), 
        LASTPWDCHANGE : new SysCfg.DB.TField(), 
        IDMDCALENDARYDATE : new SysCfg.DB.TField() 
    }, 
    CMDBUSERSETTINGS : { 
        NAME_TABLE : "CMDBUSERSETTINGS", 
        IDCMDBUSERSETTINGS : new SysCfg.DB.TField(), 
        IDCMDBUSER : new SysCfg.DB.TField(), 
        IDLANGUAGE : new SysCfg.DB.TField(), 
        IDSKINATIS : new SysCfg.DB.TField(), 
        IDSKINITHELPCENTER : new SysCfg.DB.TField(), 
        ATENLAYOUT_IDFILE_ATIS : new SysCfg.DB.TField(), 
        CONSOLEDETAIL_COLUMN_ATIS : new SysCfg.DB.TField() 
    }, 
    ATROLEGPQUERY : { 
        NAME_TABLE : "ATROLEGPQUERY", 
        IDATROLEGPQUERY : new SysCfg.DB.TField(), 
        IDATROLE : new SysCfg.DB.TField(), 
        IDGPQUERY : new SysCfg.DB.TField() 
    }, 
    ATROLEGPQUERYGROUPLAYOUT : { 
        NAME_TABLE : "ATROLEGPQUERYGROUPLAYOUT", 
        IDATROLEGPQUERYGROUPLAYOUT : new SysCfg.DB.TField(), 
        STYLE : new SysCfg.DB.TField(), 
        LAYOUT_CUSTOMCODE_ATIS : new SysCfg.DB.TField(), 
        NAME : new SysCfg.DB.TField(), 
        DESCRIPTION : new SysCfg.DB.TField(), 
        PATH : new SysCfg.DB.TField() 
    }, 
    ATROLEGPQUERYGROUPLAYOUTDET : { 
        NAME_TABLE : "ATROLEGPQUERYGROUPLAYOUTDET", 
        IDATROLEGPQUERYGROUPLAYOUTDET : new SysCfg.DB.TField(), 
        IDATROLEGPQUERYGROUPLAYOUT : new SysCfg.DB.TField(), 
        IDGPQUERY : new SysCfg.DB.TField(), 
        AUTOREFRESHTIME : new SysCfg.DB.TField() 
    }, 
    ATROLEDASHBOARD : { 
        NAME_TABLE : "ATROLEDASHBOARD", 
        IDATROLEDASHBOARD : new SysCfg.DB.TField(), 
        IDATROLE : new SysCfg.DB.TField(), 
        IDATROLEGPQUERYGROUPLAYOUT : new SysCfg.DB.TField() 
    }, 
    CMDBUSER_GROUPLAYOUT : { 
        NAME_TABLE : "CMDBUSER_GROUPLAYOUT", 
        IDCMDBUSER_GROUPLAYOUT : new SysCfg.DB.TField(), 
        IDATROLEGPQUERYGROUPLAYOUT : new SysCfg.DB.TField(), 
        IDCMDBUSER : new SysCfg.DB.TField(), 
        ID_FILE : new SysCfg.DB.TField() 
    }, 
    ATROLEPBITEMPLATE : { 
        NAME_TABLE : "ATROLEPBITEMPLATE", 
        IDATROLEPBITEMPLATE : new SysCfg.DB.TField(), 
        IDATROLE : new SysCfg.DB.TField(), 
        IDPBITEMPLATE : new SysCfg.DB.TField() 
    }, 
    ATROLEPSGROUP : { 
        NAME_TABLE : "ATROLEPSGROUP", 
        IDATROLEPSGROUP : new SysCfg.DB.TField(), 
        IDATROLE : new SysCfg.DB.TField(), 
        IDPSGROUP : new SysCfg.DB.TField() 
    }, 
    SDNOTIFYTYPEEVENT : { 
        NAME_TABLE : "SDNOTIFYTYPEEVENT", 
        IDSDNOTIFYTYPEEVENT : new SysCfg.DB.TField(), 
        TYPEEVENTNAME : new SysCfg.DB.TField() 
    }, 
    SDNOTIFYTEMPLATE : { 
        NAME_TABLE : "SDNOTIFYTEMPLATE", 
        IDSDNOTIFYTEMPLATE : new SysCfg.DB.TField(), 
        TEMPLATE_NAME : new SysCfg.DB.TField(), 
        DESCRIPTION : new SysCfg.DB.TField(), 
        STATUS : new SysCfg.DB.TField(), 
        ISEMAILSQL : new SysCfg.DB.TField(), 
        EMAILSQL : new SysCfg.DB.TField(), 
        EMAILTEMPLATE : new SysCfg.DB.TField() 
    }, 
    SDNOTIFYTEMPLATE_TYPEUSER : { 
        NAME_TABLE : "SDNOTIFYTEMPLATE_TYPEUSER", 
        IDSDNOTIFYTEMPLATE_TYPEUSER : new SysCfg.DB.TField(), 
        IDSDNOTIFYTYPEEVENT : new SysCfg.DB.TField(), 
        IDSDNOTIFYTEMPLATE : new SysCfg.DB.TField(), 
        IDSDTYPEUSER : new SysCfg.DB.TField(), 
        SENDCONSOLE : new SysCfg.DB.TField(), 
        SENDEMAIL : new SysCfg.DB.TField(), 
        IDMDSERVICETYPE : new SysCfg.DB.TField() 
    }, 
    SDNOTIFY : { 
        NAME_TABLE : "SDNOTIFY", 
        IDSDNOTIFY : new SysCfg.DB.TField(), 
        IDSDCASE : new SysCfg.DB.TField(), 
        EVENTDATE : new SysCfg.DB.TField(), 
        IDSDNOTIFYTEMPLATE : new SysCfg.DB.TField(), 
        CHANGE_DESCRIPTION : new SysCfg.DB.TField(), 
        CHANGE_EMAILSQL : new SysCfg.DB.TField(), 
        ISSENDCONSOLE : new SysCfg.DB.TField(), 
        ISSENDEMAIL : new SysCfg.DB.TField(), 
        IDSDTYPEUSER : new SysCfg.DB.TField(), 
        IDCMDBCI : new SysCfg.DB.TField() 
    }, 
    SDGROUPSERVICEUSER : { 
        NAME_TABLE : "SDGROUPSERVICEUSER", 
        IDSDGROUPSERVICEUSER : new SysCfg.DB.TField(), 
        IDMDGROUP : new SysCfg.DB.TField(), 
        IDMDSERVICETYPE : new SysCfg.DB.TField(), 
        IDSDTYPEUSER : new SysCfg.DB.TField() 
    }, 
    SMINTERFACE : { 
        NAME_TABLE : "SMINTERFACE", 
        IDSMINTERFACE : new SysCfg.DB.TField(), 
        INTERFACENAME : new SysCfg.DB.TField() 
    }, 
    SMINTERFACECUSTOM : { 
        NAME_TABLE : "SMINTERFACECUSTOM", 
        IDSMINTERFACECUSTOM : new SysCfg.DB.TField(), 
        IDSMINTERFACE : new SysCfg.DB.TField(), 
        INTERFACECUSTOMNAME : new SysCfg.DB.TField(), 
        CUSTOMCODE : new SysCfg.DB.TField() 
    }, 
    PSGROUP : { 
        NAME_TABLE : "PSGROUP", 
        IDPSGROUP : new SysCfg.DB.TField(), 
        GROUP_NAME : new SysCfg.DB.TField(), 
        GROUP_BACKGROUND : new SysCfg.DB.TField(), 
        GROUP_BORDER : new SysCfg.DB.TField(), 
        GROUP_DESCRIPTION : new SysCfg.DB.TField(), 
        GROUP_PATH : new SysCfg.DB.TField() 
    }, 
    PSMAIN : { 
        NAME_TABLE : "PSMAIN", 
        IDPSMAIN : new SysCfg.DB.TField(), 
        IDPSGROUP : new SysCfg.DB.TField(), 
        MAIN_POSITION : new SysCfg.DB.TField(), 
        MAIN_ICON : new SysCfg.DB.TField(), 
        MAIN_TITLE : new SysCfg.DB.TField(), 
        MAIN_TITLECOLOR : new SysCfg.DB.TField(), 
        MAIN_BACKGROUNDACTIVE : new SysCfg.DB.TField(), 
        MAIN_TITLEACTIVECOLOR : new SysCfg.DB.TField(), 
        MAIN_BORDERACTIVE : new SysCfg.DB.TField(), 
        MAIN_BACKGROUNDHOVER : new SysCfg.DB.TField(), 
        MAIN_TITLEHOVERCOLOR : new SysCfg.DB.TField(), 
        MAIN_DESCRIPTION : new SysCfg.DB.TField() 
    }, 
    PSSECUNDARY : { 
        NAME_TABLE : "PSSECUNDARY", 
        IDPSSECUNDARY : new SysCfg.DB.TField(), 
        IDPSMAIN : new SysCfg.DB.TField(), 
        SECUNDARY_POSITION : new SysCfg.DB.TField(), 
        SECUNDARY_ICON : new SysCfg.DB.TField(), 
        SECUNDARY_TITLE : new SysCfg.DB.TField(), 
        SECUNDARY_TEXTCOLOR : new SysCfg.DB.TField(), 
        SECUNDARY_DESCRIPTION : new SysCfg.DB.TField(), 
        SECUNDARY_PHRASE : new SysCfg.DB.TField() 
    }, 
    VOTE : { 
        NAME_TABLE : "VOTE", 
        IDVOTE : new SysCfg.DB.TField(), 
        VOTE_NAME : new SysCfg.DB.TField(), 
        VOTE_GROUP : new SysCfg.DB.TField(), 
        VOTE_QUESTION : new SysCfg.DB.TField(), 
        VOTE_COMMENTREQUIRED : new SysCfg.DB.TField(), 
        VOTE_PROGRESSCOLOR : new SysCfg.DB.TField(), 
        VOTE_PROGRESSTITLE : new SysCfg.DB.TField(), 
        VOTE_TOTALPROGRESSTITLE : new SysCfg.DB.TField(), 
        VOTE_INITGRAPHIC : new SysCfg.DB.TField(), 
        VOTE_SHOWCOMMENT : new SysCfg.DB.TField(), 
        VOTE_SHOWCOMMENTGROUP : new SysCfg.DB.TField() 
    }, 
    VOTEOPTION : { 
        NAME_TABLE : "VOTEOPTION", 
        IDVOTEOPTION : new SysCfg.DB.TField(), 
        IDVOTE : new SysCfg.DB.TField(), 
        VOTEOPTION_NAME : new SysCfg.DB.TField(), 
        VOTEOPTION_DESCRIPTION : new SysCfg.DB.TField() 
    }, 
    VOTEDATA : { 
        NAME_TABLE : "VOTEDATA", 
        IDVOTEDATA : new SysCfg.DB.TField(), 
        IDVOTE : new SysCfg.DB.TField(), 
        VOTEDATA_STARTDATE : new SysCfg.DB.TField(), 
        VOTEDATA_FINALDATE : new SysCfg.DB.TField(), 
        VOTEDATA_STATUS : new SysCfg.DB.TField() 
    }, 
    VOTEDATAUSER : { 
        NAME_TABLE : "VOTEDATAUSER", 
        IDVOTEDATAUSER : new SysCfg.DB.TField(), 
        IDVOTEDATA : new SysCfg.DB.TField(), 
        IDCMDBCI : new SysCfg.DB.TField(), 
        IDVOTEOPTION : new SysCfg.DB.TField(), 
        VOTEDATAUSER_NAME : new SysCfg.DB.TField(), 
        VOTEDATAUSER_COMMENTARY : new SysCfg.DB.TField() 
    }, 
    PMMAINTASK : { 
        NAME_TABLE : "PMMAINTASK", 
        IDPMMAINTASK : new SysCfg.DB.TField(), 
        MAINTASK_NAME : new SysCfg.DB.TField(), 
        MAINTASK_OWNER : new SysCfg.DB.TField(), 
        MAINTASK_DURATION : new SysCfg.DB.TField(), 
        MAINTASK_STARTDATE : new SysCfg.DB.TField(), 
        MAINTASK_ENDDATE : new SysCfg.DB.TField(), 
        MAINTASK_STATE : new SysCfg.DB.TField(), 
        MAINTASK_WEEKENDHOLIDAY : new SysCfg.DB.TField(), 
        MAINTASK_SUNDAYHOLIDAY : new SysCfg.DB.TField(), 
        MAINTASK_SCALEUNITS : new SysCfg.DB.TField(), 
        MAINTASK_SCALECOUNTER : new SysCfg.DB.TField() 
    }, 
    PMHOLIDAY : { 
        NAME_TABLE : "PMHOLIDAY", 
        IDPMHOLIDAY : new SysCfg.DB.TField(), 
        IDPMMAINTASK : new SysCfg.DB.TField(), 
        HOLIDAY_DATE : new SysCfg.DB.TField() 
    }, 
    PMDETAILTASK : { 
        NAME_TABLE : "PMDETAILTASK", 
        IDPMDETAILTASK : new SysCfg.DB.TField(), 
        IDPMMAINTASK : new SysCfg.DB.TField(), 
        DETAILTASK_NAME : new SysCfg.DB.TField(), 
        DETAILTASK_DURATION : new SysCfg.DB.TField(), 
        DETAILTASK_STARTDATE : new SysCfg.DB.TField(), 
        DETAILTASK_ENDDATE : new SysCfg.DB.TField(), 
        DETAILTASK_INDEX : new SysCfg.DB.TField(), 
        DETAILTASK_PERCENTCOMPLETE : new SysCfg.DB.TField(), 
        DETAILTASK_PRIORITY : new SysCfg.DB.TField(), 
        DETAILTASK_ESTIMATED : new SysCfg.DB.TField(), 
        DETAILTASK_NOTE : new SysCfg.DB.TField(), 
        IDPMDETAILTASK_PARENT : new SysCfg.DB.TField(), 
        DETAILTASK_STARTDATETEMP : new SysCfg.DB.TField(), 
        DETAILTASK_ENDDATETEMP : new SysCfg.DB.TField(), 
        DETAILTASK_DURATIONTEMP : new SysCfg.DB.TField() 
    }, 
    PMRESOURCES : { 
        NAME_TABLE : "PMRESOURCES", 
        IDPMRESOURCES : new SysCfg.DB.TField(), 
        IDPMMAINTASK : new SysCfg.DB.TField(), 
        RESOURCES_NAME : new SysCfg.DB.TField(), 
        RESOURCES_ALLOCATIONOWNER : new SysCfg.DB.TField(), 
        RESOURCES_UNITS : new SysCfg.DB.TField(), 
        RESOURCES_COST : new SysCfg.DB.TField() 
    }, 
    PMRESOURCESALLOCATION : { 
        NAME_TABLE : "PMRESOURCESALLOCATION", 
        IDPMRESOURCESALLOCATION : new SysCfg.DB.TField(), 
        IDPMDETAILTASK : new SysCfg.DB.TField(), 
        IDPMRESOURCES : new SysCfg.DB.TField(), 
        RESOURCESALLOCATION_UNITS : new SysCfg.DB.TField(), 
        RESOURCESALLOCATION_DESCRIPTION : new SysCfg.DB.TField() 
    }, 
    PMTYPERELATION : { 
        NAME_TABLE : "PMTYPERELATION", 
        IDPMTYPERELATION : new SysCfg.DB.TField(), 
        TYPERELATION_NAME : new SysCfg.DB.TField(), 
        TYPERELATION_STATE : new SysCfg.DB.TField() 
    }, 
    PMRELATION : { 
        NAME_TABLE : "PMRELATION", 
        IDPMRELATION : new SysCfg.DB.TField(), 
        IDPMDETAILTASK_PREDECESSORS : new SysCfg.DB.TField(), 
        IDPMDETAILTASK : new SysCfg.DB.TField(), 
        IDPMTYPERELATION : new SysCfg.DB.TField() 
    }, 
    PBITEMPLATE : { 
        NAME_TABLE : "PBITEMPLATE", 
        IDPBITEMPLATE : new SysCfg.DB.TField(), 
        TEMPLATE_NAME : new SysCfg.DB.TField(), 
        TEMPLATE_DESCRIPTION : new SysCfg.DB.TField(), 
        TEMPLATE_DATASQL : new SysCfg.DB.TField(), 
        TEMPLATE_REFRESHTIMEENABLE : new SysCfg.DB.TField(), 
        TEMPLATE_REFRESHTIME : new SysCfg.DB.TField(), 
        TEMPLATE_PATH : new SysCfg.DB.TField() 
    }, 
    PBITEMPLATECELLS : { 
        NAME_TABLE : "PBITEMPLATECELLS", 
        IDPBITEMPLATECELLS : new SysCfg.DB.TField(), 
        IDPBITEMPLATE : new SysCfg.DB.TField(), 
        IDPBITEMPLATECELLS_PARENT : new SysCfg.DB.TField(), 
        CELLS_POSITION : new SysCfg.DB.TField(), 
        CELLS_END : new SysCfg.DB.TField(), 
        CELLS_NAME : new SysCfg.DB.TField() 
    }, 
    PBITEMPLATECELLSDESIGN : { 
        NAME_TABLE : "PBITEMPLATECELLSDESIGN", 
        IDPBITEMPLATECELLSDESIGN : new SysCfg.DB.TField(), 
        IDPBITEMPLATECELLS : new SysCfg.DB.TField(), 
        DESIGN_XS : new SysCfg.DB.TField(), 
        DESIGN_S : new SysCfg.DB.TField(), 
        DESIGN_M : new SysCfg.DB.TField(), 
        DESIGN_L : new SysCfg.DB.TField(), 
        DESIGN_XL : new SysCfg.DB.TField(), 
        DESIGN_ROW : new SysCfg.DB.TField(), 
        DESIGN_COLUMN : new SysCfg.DB.TField() 
    }, 
    PBIGRAPHICTYPE : { 
        NAME_TABLE : "PBIGRAPHICTYPE", 
        IDPBIGRAPHICTYPE : new SysCfg.DB.TField(), 
        GRAPHIC_NAME : new SysCfg.DB.TField(), 
        GRAPHIC_DESCRIPTION : new SysCfg.DB.TField() 
    }, 
    PBITEMPLATECELLGRAPHIC : { 
        NAME_TABLE : "PBITEMPLATECELLGRAPHIC", 
        IDPBITEMPLATECELLGRAPHIC : new SysCfg.DB.TField(), 
        IDPBITEMPLATECELLS : new SysCfg.DB.TField(), 
        IDPBIGRAPHICTYPE : new SysCfg.DB.TField(), 
        CELLGRAPHIC_VISIBLE : new SysCfg.DB.TField(), 
        CELLGRAPHIC_TITLE : new SysCfg.DB.TField(), 
        CELLGRAPHIC_VISIBLETITLE : new SysCfg.DB.TField(), 
        CELLGRAPHIC_COLORTITLE : new SysCfg.DB.TField(), 
        CELLGRAPHIC_SIZETITLE : new SysCfg.DB.TField(), 
        CELLGRAPHIC_ALIGNTITLE : new SysCfg.DB.TField() 
    }, 
    PBITEMPLATECELLGRAPHIC_COLUMN : { 
        NAME_TABLE : "PBITEMPLATECELLGRAPHIC_COLUMN", 
        IDPBITEMPLATECELLGRAPHIC_COLUMN : new SysCfg.DB.TField(), 
        IDPBITEMPLATECELLGRAPHIC : new SysCfg.DB.TField(), 
        COLUMN_NAME : new SysCfg.DB.TField() 
    }, 
    FORUMS : { 
        NAME_TABLE : "FORUMS", 
        IDFORUMS : new SysCfg.DB.TField(), 
        IDCMDBCI : new SysCfg.DB.TField(), 
        NAMEFORUMS : new SysCfg.DB.TField(), 
        DESCRIPTIONFORUMS : new SysCfg.DB.TField() 
    }, 
    FORUMSCATEGORY : { 
        NAME_TABLE : "FORUMSCATEGORY", 
        IDFORUMSCATEGORY : new SysCfg.DB.TField(), 
        IDMDCATEGORYDETAIL : new SysCfg.DB.TField(), 
        IDFORUMS : new SysCfg.DB.TField(), 
        DESCRIPTIONFORUMSCATEGORY : new SysCfg.DB.TField(), 
        TOPICSFORUMSCATEGORY_COUNT : new SysCfg.DB.TField(), 
        RESPONSEFORUMSCATEGORY_COUNT : new SysCfg.DB.TField(), 
        MEMBERSFORUMSCATEGORY_COUNT : new SysCfg.DB.TField(), 
        IMAGEFORUMSCATEGORY : new SysCfg.DB.TField(), 
        STATUSFORUMSCATEGORY : new SysCfg.DB.TField() 
    }, 
    TOPICS : { 
        NAME_TABLE : "TOPICS", 
        IDTOPICS : new SysCfg.DB.TField(), 
        IDFORUMSCATEGORY : new SysCfg.DB.TField(), 
        IDCMDBCI : new SysCfg.DB.TField(), 
        NAMETOPICS : new SysCfg.DB.TField(), 
        DESCRIPTIONTOPICS : new SysCfg.DB.TField(), 
        RESPONSETOPICS_COUNT : new SysCfg.DB.TField(), 
        VIEWSTOPICS_COUNT : new SysCfg.DB.TField(), 
        IMAGETOPICS : new SysCfg.DB.TField(), 
        STATUSTOPICS : new SysCfg.DB.TField(), 
        CREATEDDATETOPICS : new SysCfg.DB.TField(), 
        KEYWORDSTOPICS : new SysCfg.DB.TField() 
    }, 
    TOPICSRESPONSE : { 
        NAME_TABLE : "TOPICSRESPONSE", 
        IDTOPICSRESPONSE : new SysCfg.DB.TField(), 
        IDTOPICS : new SysCfg.DB.TField(), 
        IDCMDBCI : new SysCfg.DB.TField(), 
        IDTOPICSRESPONSE_PARENT : new SysCfg.DB.TField(), 
        TITLETOPICSRESPONSE : new SysCfg.DB.TField(), 
        SUBJECTTOPICSRESPONSE : new SysCfg.DB.TField(), 
        STATUSTOPICSRESPOSE : new SysCfg.DB.TField(), 
        CREATEDDATETOPICSRESPONSE : new SysCfg.DB.TField(), 
        LIKETOPICSRESPONSE_COUNT : new SysCfg.DB.TField(), 
        NOTLIKETOPICSRESPONSE_COUNT : new SysCfg.DB.TField(), 
        ABUSETOPICSRESPONSE_COUNT : new SysCfg.DB.TField() 
    }, 
    TOPICSRESPONSEVOTES : { 
        NAME_TABLE : "TOPICSRESPONSEVOTES", 
        IDTOPICSRESPONSEVOTES : new SysCfg.DB.TField(), 
        IDTOPICSRESPONSE : new SysCfg.DB.TField(), 
        IDCMDBCI : new SysCfg.DB.TField(), 
        LIKETOPICSRESPONSE : new SysCfg.DB.TField(), 
        NOTLIKETOPICSRESPONSE : new SysCfg.DB.TField() 
    }, 
    TOPICSRESPONSEABUSE : { 
        NAME_TABLE : "TOPICSRESPONSEABUSE", 
        IDTOPICSRESPONSEABUSE : new SysCfg.DB.TField(), 
        IDTOPICSRESPONSE : new SysCfg.DB.TField(), 
        IDCMDBCI : new SysCfg.DB.TField(), 
        LIKETOPICSRESPONSEABUSE : new SysCfg.DB.TField(), 
        NOTLIKETOPICSRESPONSEABUSE : new SysCfg.DB.TField() 
    }, 
    FORUMSSEARCH : { 
        NAME_TABLE : "FORUMSSEARCH", 
        IDFORUMSSEARCH : new SysCfg.DB.TField(), 
        IDFORUMS : new SysCfg.DB.TField(), 
        NAMEFORUMSSEARCH : new SysCfg.DB.TField(), 
        IMAGEFORUMSSEARCH : new SysCfg.DB.TField(), 
        QUERYFORUMSSEARCH : new SysCfg.DB.TField() 
    } 
} 
UsrCfg.InternoAtisNames.SYSTEMSTATUS.NAME_TABLE = "SYSTEMSTATUS"; 
UsrCfg.InternoAtisNames.SYSTEMSTATUS.IDSYSTEMSTATUS.FieldName = "IDSYSTEMSTATUS"; 
UsrCfg.InternoAtisNames.SYSTEMSTATUS.IDSYSTEMSTATUS.TableName = "SYSTEMSTATUS"; 
UsrCfg.InternoAtisNames.SYSTEMSTATUS.IDSYSTEMSTATUS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SYSTEMSTATUS.IDSYSTEMSTATUS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SYSTEMSTATUS.IDSYSTEMSTATUS);
UsrCfg.InternoAtisNames.SYSTEMSTATUS.NAME_TABLE = "SYSTEMSTATUS"; 
UsrCfg.InternoAtisNames.SYSTEMSTATUS.SYSTEMSTATUSNAME.FieldName = "SYSTEMSTATUSNAME"; 
UsrCfg.InternoAtisNames.SYSTEMSTATUS.SYSTEMSTATUSNAME.TableName = "SYSTEMSTATUS"; 
UsrCfg.InternoAtisNames.SYSTEMSTATUS.SYSTEMSTATUSNAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.SYSTEMSTATUS.SYSTEMSTATUSNAME.Size = 10; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SYSTEMSTATUS.SYSTEMSTATUSNAME);
UsrCfg.InternoAtisNames.SDCASETYPE.NAME_TABLE = "SDCASETYPE"; 
UsrCfg.InternoAtisNames.SDCASETYPE.IDSDCASETYPE.FieldName = "IDSDCASETYPE"; 
UsrCfg.InternoAtisNames.SDCASETYPE.IDSDCASETYPE.TableName = "SDCASETYPE"; 
UsrCfg.InternoAtisNames.SDCASETYPE.IDSDCASETYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASETYPE.IDSDCASETYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASETYPE.IDSDCASETYPE);
UsrCfg.InternoAtisNames.SDCASETYPE.NAME_TABLE = "SDCASETYPE"; 
UsrCfg.InternoAtisNames.SDCASETYPE.TYPENAME.FieldName = "TYPENAME"; 
UsrCfg.InternoAtisNames.SDCASETYPE.TYPENAME.TableName = "SDCASETYPE"; 
UsrCfg.InternoAtisNames.SDCASETYPE.TYPENAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.SDCASETYPE.TYPENAME.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASETYPE.TYPENAME);
UsrCfg.InternoAtisNames.SDRESPONSETYPE.NAME_TABLE = "SDRESPONSETYPE"; 
UsrCfg.InternoAtisNames.SDRESPONSETYPE.IDSDRESPONSETYPE.FieldName = "IDSDRESPONSETYPE"; 
UsrCfg.InternoAtisNames.SDRESPONSETYPE.IDSDRESPONSETYPE.TableName = "SDRESPONSETYPE"; 
UsrCfg.InternoAtisNames.SDRESPONSETYPE.IDSDRESPONSETYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDRESPONSETYPE.IDSDRESPONSETYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDRESPONSETYPE.IDSDRESPONSETYPE);
UsrCfg.InternoAtisNames.SDRESPONSETYPE.NAME_TABLE = "SDRESPONSETYPE"; 
UsrCfg.InternoAtisNames.SDRESPONSETYPE.RESPONSETYPENAME.FieldName = "RESPONSETYPENAME"; 
UsrCfg.InternoAtisNames.SDRESPONSETYPE.RESPONSETYPENAME.TableName = "SDRESPONSETYPE"; 
UsrCfg.InternoAtisNames.SDRESPONSETYPE.RESPONSETYPENAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.SDRESPONSETYPE.RESPONSETYPENAME.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDRESPONSETYPE.RESPONSETYPENAME);
UsrCfg.InternoAtisNames.SDCASESOURCETYPE.NAME_TABLE = "SDCASESOURCETYPE"; 
UsrCfg.InternoAtisNames.SDCASESOURCETYPE.IDSDCASESOURCETYPE.FieldName = "IDSDCASESOURCETYPE"; 
UsrCfg.InternoAtisNames.SDCASESOURCETYPE.IDSDCASESOURCETYPE.TableName = "SDCASESOURCETYPE"; 
UsrCfg.InternoAtisNames.SDCASESOURCETYPE.IDSDCASESOURCETYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASESOURCETYPE.IDSDCASESOURCETYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASESOURCETYPE.IDSDCASESOURCETYPE);
UsrCfg.InternoAtisNames.SDCASESOURCETYPE.NAME_TABLE = "SDCASESOURCETYPE"; 
UsrCfg.InternoAtisNames.SDCASESOURCETYPE.SOURCETYPENAME.FieldName = "SOURCETYPENAME"; 
UsrCfg.InternoAtisNames.SDCASESOURCETYPE.SOURCETYPENAME.TableName = "SDCASESOURCETYPE"; 
UsrCfg.InternoAtisNames.SDCASESOURCETYPE.SOURCETYPENAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.SDCASESOURCETYPE.SOURCETYPENAME.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASESOURCETYPE.SOURCETYPENAME);
UsrCfg.InternoAtisNames.SDCASE.NAME_TABLE = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.IDSDCASE.FieldName = "IDSDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.IDSDCASE.TableName = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.IDSDCASE.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.SDCASE.IDSDCASE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE.IDSDCASE);
UsrCfg.InternoAtisNames.SDCASE.NAME_TABLE = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.IDSDCASE_PARENT.FieldName = "IDSDCASE_PARENT"; 
UsrCfg.InternoAtisNames.SDCASE.IDSDCASE_PARENT.TableName = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.IDSDCASE_PARENT.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASE.IDSDCASE_PARENT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE.IDSDCASE_PARENT);
UsrCfg.InternoAtisNames.SDCASE.NAME_TABLE = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.IDSDCASESTATUS.FieldName = "IDSDCASESTATUS"; 
UsrCfg.InternoAtisNames.SDCASE.IDSDCASESTATUS.TableName = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.IDSDCASESTATUS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASE.IDSDCASESTATUS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE.IDSDCASESTATUS);
UsrCfg.InternoAtisNames.SDCASE.NAME_TABLE = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.IDSDRESPONSETYPE.FieldName = "IDSDRESPONSETYPE"; 
UsrCfg.InternoAtisNames.SDCASE.IDSDRESPONSETYPE.TableName = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.IDSDRESPONSETYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASE.IDSDRESPONSETYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE.IDSDRESPONSETYPE);
UsrCfg.InternoAtisNames.SDCASE.NAME_TABLE = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.IDSDCASESOURCETYPE.FieldName = "IDSDCASESOURCETYPE"; 
UsrCfg.InternoAtisNames.SDCASE.IDSDCASESOURCETYPE.TableName = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.IDSDCASESOURCETYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASE.IDSDCASESOURCETYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE.IDSDCASESOURCETYPE);
UsrCfg.InternoAtisNames.SDCASE.NAME_TABLE = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_DATESTART.FieldName = "CASE_DATESTART"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_DATESTART.TableName = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_DATESTART.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.SDCASE.CASE_DATESTART.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE.CASE_DATESTART);
UsrCfg.InternoAtisNames.SDCASE.NAME_TABLE = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_DATECREATE.FieldName = "CASE_DATECREATE"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_DATECREATE.TableName = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_DATECREATE.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.SDCASE.CASE_DATECREATE.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE.CASE_DATECREATE);
UsrCfg.InternoAtisNames.SDCASE.NAME_TABLE = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_DATERESOLVED.FieldName = "CASE_DATERESOLVED"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_DATERESOLVED.TableName = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_DATERESOLVED.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.SDCASE.CASE_DATERESOLVED.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE.CASE_DATERESOLVED);
UsrCfg.InternoAtisNames.SDCASE.NAME_TABLE = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_DATECLOSED.FieldName = "CASE_DATECLOSED"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_DATECLOSED.TableName = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_DATECLOSED.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.SDCASE.CASE_DATECLOSED.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE.CASE_DATECLOSED);
UsrCfg.InternoAtisNames.SDCASE.NAME_TABLE = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_DATEPROGRESS.FieldName = "CASE_DATEPROGRESS"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_DATEPROGRESS.TableName = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_DATEPROGRESS.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.SDCASE.CASE_DATEPROGRESS.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE.CASE_DATEPROGRESS);
UsrCfg.InternoAtisNames.SDCASE.NAME_TABLE = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.IDOWNER.FieldName = "IDOWNER"; 
UsrCfg.InternoAtisNames.SDCASE.IDOWNER.TableName = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.IDOWNER.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASE.IDOWNER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE.IDOWNER);
UsrCfg.InternoAtisNames.SDCASE.NAME_TABLE = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.IDUSER.FieldName = "IDUSER"; 
UsrCfg.InternoAtisNames.SDCASE.IDUSER.TableName = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.IDUSER.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASE.IDUSER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE.IDUSER);
UsrCfg.InternoAtisNames.SDCASE.NAME_TABLE = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.IDHANDLER.FieldName = "IDHANDLER"; 
UsrCfg.InternoAtisNames.SDCASE.IDHANDLER.TableName = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.IDHANDLER.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASE.IDHANDLER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE.IDHANDLER);
UsrCfg.InternoAtisNames.SDCASE.NAME_TABLE = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.IDMANAGERSINFORMED.FieldName = "IDMANAGERSINFORMED"; 
UsrCfg.InternoAtisNames.SDCASE.IDMANAGERSINFORMED.TableName = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.IDMANAGERSINFORMED.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASE.IDMANAGERSINFORMED.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE.IDMANAGERSINFORMED);
UsrCfg.InternoAtisNames.SDCASE.NAME_TABLE = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.IDCMDBCONTACTTYPE_USER.FieldName = "IDCMDBCONTACTTYPE_USER"; 
UsrCfg.InternoAtisNames.SDCASE.IDCMDBCONTACTTYPE_USER.TableName = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.IDCMDBCONTACTTYPE_USER.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASE.IDCMDBCONTACTTYPE_USER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE.IDCMDBCONTACTTYPE_USER);
UsrCfg.InternoAtisNames.SDCASE.NAME_TABLE = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.IDMDCATEGORYDETAIL_INITIAL.FieldName = "IDMDCATEGORYDETAIL_INITIAL"; 
UsrCfg.InternoAtisNames.SDCASE.IDMDCATEGORYDETAIL_INITIAL.TableName = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.IDMDCATEGORYDETAIL_INITIAL.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASE.IDMDCATEGORYDETAIL_INITIAL.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE.IDMDCATEGORYDETAIL_INITIAL);
UsrCfg.InternoAtisNames.SDCASE.NAME_TABLE = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.IDMDCATEGORYDETAIL_FINAL.FieldName = "IDMDCATEGORYDETAIL_FINAL"; 
UsrCfg.InternoAtisNames.SDCASE.IDMDCATEGORYDETAIL_FINAL.TableName = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.IDMDCATEGORYDETAIL_FINAL.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASE.IDMDCATEGORYDETAIL_FINAL.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE.IDMDCATEGORYDETAIL_FINAL);
UsrCfg.InternoAtisNames.SDCASE.NAME_TABLE = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_ISMAYOR.FieldName = "CASE_ISMAYOR"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_ISMAYOR.TableName = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_ISMAYOR.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.SDCASE.CASE_ISMAYOR.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE.CASE_ISMAYOR);
UsrCfg.InternoAtisNames.SDCASE.NAME_TABLE = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_DESCRIPTION.FieldName = "CASE_DESCRIPTION"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_DESCRIPTION.TableName = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_DESCRIPTION.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.SDCASE.CASE_DESCRIPTION.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE.CASE_DESCRIPTION);
UsrCfg.InternoAtisNames.SDCASE.NAME_TABLE = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_TITLE.FieldName = "CASE_TITLE"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_TITLE.TableName = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_TITLE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.SDCASE.CASE_TITLE.Size = 255; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE.CASE_TITLE);
UsrCfg.InternoAtisNames.SDCASE.NAME_TABLE = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_RETURN_STR.FieldName = "CASE_RETURN_STR"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_RETURN_STR.TableName = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_RETURN_STR.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.SDCASE.CASE_RETURN_STR.Size = 255; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE.CASE_RETURN_STR);
UsrCfg.InternoAtisNames.SDCASE.NAME_TABLE = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_RETURN_COST.FieldName = "CASE_RETURN_COST"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_RETURN_COST.TableName = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_RETURN_COST.DataType = SysCfg.DB.Properties.TDataType.Double; 
UsrCfg.InternoAtisNames.SDCASE.CASE_RETURN_COST.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE.CASE_RETURN_COST);
UsrCfg.InternoAtisNames.SDCASE.NAME_TABLE = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_FINALSUMM.FieldName = "CASE_FINALSUMM"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_FINALSUMM.TableName = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_FINALSUMM.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.SDCASE.CASE_FINALSUMM.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE.CASE_FINALSUMM);
UsrCfg.InternoAtisNames.SDCASE.NAME_TABLE = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_COUNTTIME.FieldName = "CASE_COUNTTIME"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_COUNTTIME.TableName = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_COUNTTIME.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASE.CASE_COUNTTIME.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE.CASE_COUNTTIME);
UsrCfg.InternoAtisNames.SDCASE.NAME_TABLE = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_COUNTTIMEPAUSE.FieldName = "CASE_COUNTTIMEPAUSE"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_COUNTTIMEPAUSE.TableName = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_COUNTTIMEPAUSE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASE.CASE_COUNTTIMEPAUSE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE.CASE_COUNTTIMEPAUSE);
UsrCfg.InternoAtisNames.SDCASE.NAME_TABLE = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_COUNTTIMERESOLVED.FieldName = "CASE_COUNTTIMERESOLVED"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_COUNTTIMERESOLVED.TableName = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_COUNTTIMERESOLVED.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASE.CASE_COUNTTIMERESOLVED.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE.CASE_COUNTTIMERESOLVED);
UsrCfg.InternoAtisNames.SDCASE.NAME_TABLE = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_DATELASTCUT.FieldName = "CASE_DATELASTCUT"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_DATELASTCUT.TableName = "SDCASE"; 
UsrCfg.InternoAtisNames.SDCASE.CASE_DATELASTCUT.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.SDCASE.CASE_DATELASTCUT.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE.CASE_DATELASTCUT);
UsrCfg.InternoAtisNames.SDCASEEF.NAME_TABLE = "SDCASEEF"; 
UsrCfg.InternoAtisNames.SDCASEEF.IDSDCASEEF.FieldName = "IDSDCASEEF"; 
UsrCfg.InternoAtisNames.SDCASEEF.IDSDCASEEF.TableName = "SDCASEEF"; 
UsrCfg.InternoAtisNames.SDCASEEF.IDSDCASEEF.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.SDCASEEF.IDSDCASEEF.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEEF.IDSDCASEEF);
UsrCfg.InternoAtisNames.SDCASEEF.NAME_TABLE = "SDCASEEF"; 
UsrCfg.InternoAtisNames.SDCASEEF.CASEEF_DATE.FieldName = "CASEEF_DATE"; 
UsrCfg.InternoAtisNames.SDCASEEF.CASEEF_DATE.TableName = "SDCASEEF"; 
UsrCfg.InternoAtisNames.SDCASEEF.CASEEF_DATE.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.SDCASEEF.CASEEF_DATE.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEEF.CASEEF_DATE);
UsrCfg.InternoAtisNames.SDCASEEF.NAME_TABLE = "SDCASEEF"; 
UsrCfg.InternoAtisNames.SDCASEEF.IDCMDBCI.FieldName = "IDCMDBCI"; 
UsrCfg.InternoAtisNames.SDCASEEF.IDCMDBCI.TableName = "SDCASEEF"; 
UsrCfg.InternoAtisNames.SDCASEEF.IDCMDBCI.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEEF.IDCMDBCI.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEEF.IDCMDBCI);
UsrCfg.InternoAtisNames.SDCASEEF.NAME_TABLE = "SDCASEEF"; 
UsrCfg.InternoAtisNames.SDCASEEF.IDSDCASE.FieldName = "IDSDCASE"; 
UsrCfg.InternoAtisNames.SDCASEEF.IDSDCASE.TableName = "SDCASEEF"; 
UsrCfg.InternoAtisNames.SDCASEEF.IDSDCASE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEEF.IDSDCASE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEEF.IDSDCASE);
UsrCfg.InternoAtisNames.SDCASETEMPLATE.NAME_TABLE = "SDCASETEMPLATE"; 
UsrCfg.InternoAtisNames.SDCASETEMPLATE.IDSDCASETEMPLATE.FieldName = "IDSDCASETEMPLATE"; 
UsrCfg.InternoAtisNames.SDCASETEMPLATE.IDSDCASETEMPLATE.TableName = "SDCASETEMPLATE"; 
UsrCfg.InternoAtisNames.SDCASETEMPLATE.IDSDCASETEMPLATE.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.SDCASETEMPLATE.IDSDCASETEMPLATE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASETEMPLATE.IDSDCASETEMPLATE);
UsrCfg.InternoAtisNames.SDCASETEMPLATE.NAME_TABLE = "SDCASETEMPLATE"; 
UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_DATECREATE.FieldName = "TEMPLATE_DATECREATE"; 
UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_DATECREATE.TableName = "SDCASETEMPLATE"; 
UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_DATECREATE.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_DATECREATE.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_DATECREATE);
UsrCfg.InternoAtisNames.SDCASETEMPLATE.NAME_TABLE = "SDCASETEMPLATE"; 
UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_IDSDCASETYPE.FieldName = "TEMPLATE_IDSDCASETYPE"; 
UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_IDSDCASETYPE.TableName = "SDCASETEMPLATE"; 
UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_IDSDCASETYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_IDSDCASETYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_IDSDCASETYPE);
UsrCfg.InternoAtisNames.SDCASETEMPLATE.NAME_TABLE = "SDCASETEMPLATE"; 
UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_ENABLE.FieldName = "TEMPLATE_ENABLE"; 
UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_ENABLE.TableName = "SDCASETEMPLATE"; 
UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_ENABLE.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_ENABLE.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_ENABLE);
UsrCfg.InternoAtisNames.SDCASETEMPLATE.NAME_TABLE = "SDCASETEMPLATE"; 
UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_IDCMDBCI.FieldName = "TEMPLATE_IDCMDBCI"; 
UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_IDCMDBCI.TableName = "SDCASETEMPLATE"; 
UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_IDCMDBCI.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_IDCMDBCI.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_IDCMDBCI);
UsrCfg.InternoAtisNames.SDCASETEMPLATE.NAME_TABLE = "SDCASETEMPLATE"; 
UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_ISPUBLIC.FieldName = "TEMPLATE_ISPUBLIC"; 
UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_ISPUBLIC.TableName = "SDCASETEMPLATE"; 
UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_ISPUBLIC.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_ISPUBLIC.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_ISPUBLIC);
UsrCfg.InternoAtisNames.SDCASETEMPLATE.NAME_TABLE = "SDCASETEMPLATE"; 
UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_NAME.FieldName = "TEMPLATE_NAME"; 
UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_NAME.TableName = "SDCASETEMPLATE"; 
UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_NAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_NAME.Size = 255; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_NAME);
UsrCfg.InternoAtisNames.SDCASETEMPLATE.NAME_TABLE = "SDCASETEMPLATE"; 
UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_DESCRIPTION.FieldName = "TEMPLATE_DESCRIPTION"; 
UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_DESCRIPTION.TableName = "SDCASETEMPLATE"; 
UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_DESCRIPTION.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_DESCRIPTION.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_DESCRIPTION);
UsrCfg.InternoAtisNames.SDCASETEMPLATE.NAME_TABLE = "SDCASETEMPLATE"; 
UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_CODECFGSTR.FieldName = "TEMPLATE_CODECFGSTR"; 
UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_CODECFGSTR.TableName = "SDCASETEMPLATE"; 
UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_CODECFGSTR.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_CODECFGSTR.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_CODECFGSTR);
UsrCfg.InternoAtisNames.SDCASEMTASGSTATUS.NAME_TABLE = "SDCASEMTASGSTATUS"; 
UsrCfg.InternoAtisNames.SDCASEMTASGSTATUS.IDSDCASEMTASGSTATUS.FieldName = "IDSDCASEMTASGSTATUS"; 
UsrCfg.InternoAtisNames.SDCASEMTASGSTATUS.IDSDCASEMTASGSTATUS.TableName = "SDCASEMTASGSTATUS"; 
UsrCfg.InternoAtisNames.SDCASEMTASGSTATUS.IDSDCASEMTASGSTATUS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMTASGSTATUS.IDSDCASEMTASGSTATUS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMTASGSTATUS.IDSDCASEMTASGSTATUS);
UsrCfg.InternoAtisNames.SDCASEMTASGSTATUS.NAME_TABLE = "SDCASEMTASGSTATUS"; 
UsrCfg.InternoAtisNames.SDCASEMTASGSTATUS.ASGSTATUSNAME.FieldName = "ASGSTATUSNAME"; 
UsrCfg.InternoAtisNames.SDCASEMTASGSTATUS.ASGSTATUSNAME.TableName = "SDCASEMTASGSTATUS"; 
UsrCfg.InternoAtisNames.SDCASEMTASGSTATUS.ASGSTATUSNAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.SDCASEMTASGSTATUS.ASGSTATUSNAME.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMTASGSTATUS.ASGSTATUSNAME);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASEMT.FieldName = "IDSDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASEMT.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASEMT.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASEMT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASEMT);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASE.FieldName = "IDSDCASE"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASE.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASE);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASEMTSTATUS.FieldName = "IDSDCASEMTSTATUS"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASEMTSTATUS.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASEMTSTATUS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASEMTSTATUS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASEMTSTATUS);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_DATECREATE.FieldName = "CASEMT_DATECREATE"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_DATECREATE.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_DATECREATE.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_DATECREATE.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_DATECREATE);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_DATEASSIGNED.FieldName = "CASEMT_DATEASSIGNED"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_DATEASSIGNED.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_DATEASSIGNED.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_DATEASSIGNED.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_DATEASSIGNED);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_DATEPROGRESS.FieldName = "CASEMT_DATEPROGRESS"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_DATEPROGRESS.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_DATEPROGRESS.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_DATEPROGRESS.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_DATEPROGRESS);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_DATECANCELLED.FieldName = "CASEMT_DATECANCELLED"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_DATECANCELLED.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_DATECANCELLED.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_DATECANCELLED.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_DATECANCELLED);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDSDWHOTOCASECREATE.FieldName = "IDSDWHOTOCASECREATE"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDSDWHOTOCASECREATE.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDSDWHOTOCASECREATE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMT.IDSDWHOTOCASECREATE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.IDSDWHOTOCASECREATE);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDSDWHOTOCASECANCELLED.FieldName = "IDSDWHOTOCASECANCELLED"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDSDWHOTOCASECANCELLED.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDSDWHOTOCASECANCELLED.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMT.IDSDWHOTOCASECANCELLED.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.IDSDWHOTOCASECANCELLED);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDMDCATEGORYDETAIL.FieldName = "IDMDCATEGORYDETAIL"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDMDCATEGORYDETAIL.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDMDCATEGORYDETAIL.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMT.IDMDCATEGORYDETAIL.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.IDMDCATEGORYDETAIL);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDSLA.FieldName = "IDSLA"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDSLA.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDSLA.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMT.IDSLA.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.IDSLA);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_COMMENTSM.FieldName = "MT_COMMENTSM"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_COMMENTSM.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_COMMENTSM.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_COMMENTSM.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.MT_COMMENTSM);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_GUIDETEXT.FieldName = "MT_GUIDETEXT"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_GUIDETEXT.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_GUIDETEXT.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_GUIDETEXT.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.MT_GUIDETEXT);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_IDMDFUNCESC.FieldName = "MT_IDMDFUNCESC"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_IDMDFUNCESC.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_IDMDFUNCESC.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_IDMDFUNCESC.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.MT_IDMDFUNCESC);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_IDMDHIERESC.FieldName = "MT_IDMDHIERESC"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_IDMDHIERESC.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_IDMDHIERESC.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_IDMDHIERESC.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.MT_IDMDHIERESC);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_IDMDMODELTYPED.FieldName = "MT_IDMDMODELTYPED"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_IDMDMODELTYPED.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_IDMDMODELTYPED.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_IDMDMODELTYPED.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.MT_IDMDMODELTYPED);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_IDMDSERVICETYPE.FieldName = "MT_IDMDSERVICETYPE"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_IDMDSERVICETYPE.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_IDMDSERVICETYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_IDMDSERVICETYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.MT_IDMDSERVICETYPE);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_MAXTIME.FieldName = "MT_MAXTIME"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_MAXTIME.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_MAXTIME.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_MAXTIME.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.MT_MAXTIME);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_NORMALTIME.FieldName = "MT_NORMALTIME"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_NORMALTIME.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_NORMALTIME.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_NORMALTIME.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.MT_NORMALTIME);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDSDSCALETYPE_FUNC.FieldName = "IDSDSCALETYPE_FUNC"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDSDSCALETYPE_FUNC.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDSDSCALETYPE_FUNC.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMT.IDSDSCALETYPE_FUNC.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.IDSDSCALETYPE_FUNC);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDSDSCALETYPE_HIER.FieldName = "IDSDSCALETYPE_HIER"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDSDSCALETYPE_HIER.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDSDSCALETYPE_HIER.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMT.IDSDSCALETYPE_HIER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.IDSDSCALETYPE_HIER);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_OPEARTIONM.FieldName = "MT_OPEARTIONM"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_OPEARTIONM.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_OPEARTIONM.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_OPEARTIONM.Size = 255; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.MT_OPEARTIONM);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_POSSIBLERETURNS.FieldName = "MT_POSSIBLERETURNS"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_POSSIBLERETURNS.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_POSSIBLERETURNS.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_POSSIBLERETURNS.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.MT_POSSIBLERETURNS);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_TITLEM.FieldName = "MT_TITLEM"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_TITLEM.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_TITLEM.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_TITLEM.Size = 255; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.MT_TITLEM);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_STEPVALIDATE.FieldName = "MT_STEPVALIDATE"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_STEPVALIDATE.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_STEPVALIDATE.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_STEPVALIDATE.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.MT_STEPVALIDATE);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_TIMEENABLE.FieldName = "MT_TIMEENABLE"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_TIMEENABLE.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_TIMEENABLE.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_TIMEENABLE.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.MT_TIMEENABLE);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_STEEPTYPEUSERENABLE.FieldName = "MT_STEEPTYPEUSERENABLE"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_STEEPTYPEUSERENABLE.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_STEEPTYPEUSERENABLE.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_STEEPTYPEUSERENABLE.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.MT_STEEPTYPEUSERENABLE);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_IDCMDBCIDEFINE.FieldName = "MT_IDCMDBCIDEFINE"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_IDCMDBCIDEFINE.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_IDCMDBCIDEFINE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMT.MT_IDCMDBCIDEFINE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.MT_IDCMDBCIDEFINE);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDMDIMPACT.FieldName = "IDMDIMPACT"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDMDIMPACT.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDMDIMPACT.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMT.IDMDIMPACT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.IDMDIMPACT);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDMDPRIORITY.FieldName = "IDMDPRIORITY"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDMDPRIORITY.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDMDPRIORITY.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMT.IDMDPRIORITY.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.IDMDPRIORITY);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDMDURGENCY.FieldName = "IDMDURGENCY"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDMDURGENCY.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDMDURGENCY.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMT.IDMDURGENCY.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.IDMDURGENCY);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_LIFESTATUS.FieldName = "CASEMT_LIFESTATUS"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_LIFESTATUS.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_LIFESTATUS.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_LIFESTATUS.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_LIFESTATUS);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_TIMERCOUNT.FieldName = "CASEMT_TIMERCOUNT"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_TIMERCOUNT.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_TIMERCOUNT.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_TIMERCOUNT.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_TIMERCOUNT);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_LS_NAMESTEP.FieldName = "CASEMT_SET_LS_NAMESTEP"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_LS_NAMESTEP.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_LS_NAMESTEP.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_LS_NAMESTEP.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_LS_NAMESTEP);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_LS_STATUSN.FieldName = "CASEMT_SET_LS_STATUSN"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_LS_STATUSN.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_LS_STATUSN.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_LS_STATUSN.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_LS_STATUSN);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_LS_NEXTSTEP.FieldName = "CASEMT_SET_LS_NEXTSTEP"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_LS_NEXTSTEP.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_LS_NEXTSTEP.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_LS_NEXTSTEP.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_LS_NEXTSTEP);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_LS_COMMENTSST.FieldName = "CASEMT_SET_LS_COMMENTSST"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_LS_COMMENTSST.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_LS_COMMENTSST.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_LS_COMMENTSST.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_LS_COMMENTSST);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_COUNTTIME.FieldName = "CASEMT_COUNTTIME"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_COUNTTIME.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_COUNTTIME.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_COUNTTIME.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_COUNTTIME);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_COUNTTIMEPAUSE.FieldName = "CASEMT_COUNTTIMEPAUSE"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_COUNTTIMEPAUSE.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_COUNTTIMEPAUSE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_COUNTTIMEPAUSE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_COUNTTIMEPAUSE);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_COUNTTIMERESOLVED.FieldName = "CASEMT_COUNTTIMERESOLVED"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_COUNTTIMERESOLVED.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_COUNTTIMERESOLVED.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_COUNTTIMERESOLVED.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_COUNTTIMERESOLVED);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_DATELASTCUT.FieldName = "CASEMT_DATELASTCUT"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_DATELASTCUT.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_DATELASTCUT.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_DATELASTCUT.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_DATELASTCUT);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_FUNLAVEL.FieldName = "CASEMT_SET_FUNLAVEL"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_FUNLAVEL.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_FUNLAVEL.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_FUNLAVEL.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_FUNLAVEL);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_HIERLAVEL.FieldName = "CASEMT_SET_HIERLAVEL"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_HIERLAVEL.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_HIERLAVEL.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_HIERLAVEL.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_HIERLAVEL);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASEMTASGSTATUS_FUN.FieldName = "IDSDCASEMTASGSTATUS_FUN"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASEMTASGSTATUS_FUN.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASEMTASGSTATUS_FUN.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASEMTASGSTATUS_FUN.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASEMTASGSTATUS_FUN);
UsrCfg.InternoAtisNames.SDCASEMT.NAME_TABLE = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASEMTASGSTATUS_HIER.FieldName = "IDSDCASEMTASGSTATUS_HIER"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASEMTASGSTATUS_HIER.TableName = "SDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASEMTASGSTATUS_HIER.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASEMTASGSTATUS_HIER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASEMTASGSTATUS_HIER);
UsrCfg.InternoAtisNames.SDCASEMT_LSSTATUS.NAME_TABLE = "SDCASEMT_LSSTATUS"; 
UsrCfg.InternoAtisNames.SDCASEMT_LSSTATUS.IDSDCASEMT_LSSTATUS.FieldName = "IDSDCASEMT_LSSTATUS"; 
UsrCfg.InternoAtisNames.SDCASEMT_LSSTATUS.IDSDCASEMT_LSSTATUS.TableName = "SDCASEMT_LSSTATUS"; 
UsrCfg.InternoAtisNames.SDCASEMT_LSSTATUS.IDSDCASEMT_LSSTATUS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMT_LSSTATUS.IDSDCASEMT_LSSTATUS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT_LSSTATUS.IDSDCASEMT_LSSTATUS);
UsrCfg.InternoAtisNames.SDCASEMT_LSSTATUS.NAME_TABLE = "SDCASEMT_LSSTATUS"; 
UsrCfg.InternoAtisNames.SDCASEMT_LSSTATUS.CASEMT_LSSTATUSNAME.FieldName = "CASEMT_LSSTATUSNAME"; 
UsrCfg.InternoAtisNames.SDCASEMT_LSSTATUS.CASEMT_LSSTATUSNAME.TableName = "SDCASEMT_LSSTATUS"; 
UsrCfg.InternoAtisNames.SDCASEMT_LSSTATUS.CASEMT_LSSTATUSNAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.SDCASEMT_LSSTATUS.CASEMT_LSSTATUSNAME.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT_LSSTATUS.CASEMT_LSSTATUSNAME);
UsrCfg.InternoAtisNames.SDCASEMT_LSTYPE.NAME_TABLE = "SDCASEMT_LSTYPE"; 
UsrCfg.InternoAtisNames.SDCASEMT_LSTYPE.IDSDCASEMT_LSTYPE.FieldName = "IDSDCASEMT_LSTYPE"; 
UsrCfg.InternoAtisNames.SDCASEMT_LSTYPE.IDSDCASEMT_LSTYPE.TableName = "SDCASEMT_LSTYPE"; 
UsrCfg.InternoAtisNames.SDCASEMT_LSTYPE.IDSDCASEMT_LSTYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMT_LSTYPE.IDSDCASEMT_LSTYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT_LSTYPE.IDSDCASEMT_LSTYPE);
UsrCfg.InternoAtisNames.SDCASEMT_LSTYPE.NAME_TABLE = "SDCASEMT_LSTYPE"; 
UsrCfg.InternoAtisNames.SDCASEMT_LSTYPE.CASEMT_LSTYPENAME.FieldName = "CASEMT_LSTYPENAME"; 
UsrCfg.InternoAtisNames.SDCASEMT_LSTYPE.CASEMT_LSTYPENAME.TableName = "SDCASEMT_LSTYPE"; 
UsrCfg.InternoAtisNames.SDCASEMT_LSTYPE.CASEMT_LSTYPENAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.SDCASEMT_LSTYPE.CASEMT_LSTYPENAME.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT_LSTYPE.CASEMT_LSTYPENAME);
UsrCfg.InternoAtisNames.SDCASEMT_LS.NAME_TABLE = "SDCASEMT_LS"; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.IDSDCASEMT_LS.FieldName = "IDSDCASEMT_LS"; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.IDSDCASEMT_LS.TableName = "SDCASEMT_LS"; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.IDSDCASEMT_LS.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.IDSDCASEMT_LS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT_LS.IDSDCASEMT_LS);
UsrCfg.InternoAtisNames.SDCASEMT_LS.NAME_TABLE = "SDCASEMT_LS"; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.IDSDCASEMT.FieldName = "IDSDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.IDSDCASEMT.TableName = "SDCASEMT_LS"; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.IDSDCASEMT.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.IDSDCASEMT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT_LS.IDSDCASEMT);
UsrCfg.InternoAtisNames.SDCASEMT_LS.NAME_TABLE = "SDCASEMT_LS"; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.IDSDCASE.FieldName = "IDSDCASE"; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.IDSDCASE.TableName = "SDCASEMT_LS"; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.IDSDCASE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.IDSDCASE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT_LS.IDSDCASE);
UsrCfg.InternoAtisNames.SDCASEMT_LS.NAME_TABLE = "SDCASEMT_LS"; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.IDSDCASEMT_LSSTATUS.FieldName = "IDSDCASEMT_LSSTATUS"; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.IDSDCASEMT_LSSTATUS.TableName = "SDCASEMT_LS"; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.IDSDCASEMT_LSSTATUS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.IDSDCASEMT_LSSTATUS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT_LS.IDSDCASEMT_LSSTATUS);
UsrCfg.InternoAtisNames.SDCASEMT_LS.NAME_TABLE = "SDCASEMT_LS"; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.IDSDCASEMT_LSTYPE.FieldName = "IDSDCASEMT_LSTYPE"; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.IDSDCASEMT_LSTYPE.TableName = "SDCASEMT_LS"; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.IDSDCASEMT_LSTYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.IDSDCASEMT_LSTYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT_LS.IDSDCASEMT_LSTYPE);
UsrCfg.InternoAtisNames.SDCASEMT_LS.NAME_TABLE = "SDCASEMT_LS"; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.CASEMT_LSNAMESTEP.FieldName = "CASEMT_LSNAMESTEP"; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.CASEMT_LSNAMESTEP.TableName = "SDCASEMT_LS"; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.CASEMT_LSNAMESTEP.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.CASEMT_LSNAMESTEP.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT_LS.CASEMT_LSNAMESTEP);
UsrCfg.InternoAtisNames.SDCASEMT_LS.NAME_TABLE = "SDCASEMT_LS"; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.CASEMT_LSSTATUSN.FieldName = "CASEMT_LSSTATUSN"; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.CASEMT_LSSTATUSN.TableName = "SDCASEMT_LS"; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.CASEMT_LSSTATUSN.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.CASEMT_LSSTATUSN.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT_LS.CASEMT_LSSTATUSN);
UsrCfg.InternoAtisNames.SDCASEMT_LS.NAME_TABLE = "SDCASEMT_LS"; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.CASEMT_LSNEXTSTEP.FieldName = "CASEMT_LSNEXTSTEP"; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.CASEMT_LSNEXTSTEP.TableName = "SDCASEMT_LS"; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.CASEMT_LSNEXTSTEP.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.CASEMT_LSNEXTSTEP.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT_LS.CASEMT_LSNEXTSTEP);
UsrCfg.InternoAtisNames.SDCASEMT_LS.NAME_TABLE = "SDCASEMT_LS"; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.CASEMT_LSCOMMENTSST.FieldName = "CASEMT_LSCOMMENTSST"; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.CASEMT_LSCOMMENTSST.TableName = "SDCASEMT_LS"; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.CASEMT_LSCOMMENTSST.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.CASEMT_LSCOMMENTSST.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT_LS.CASEMT_LSCOMMENTSST);
UsrCfg.InternoAtisNames.SDCASEMT_LS.NAME_TABLE = "SDCASEMT_LS"; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.CASEMT_LSCOUNTTIME.FieldName = "CASEMT_LSCOUNTTIME"; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.CASEMT_LSCOUNTTIME.TableName = "SDCASEMT_LS"; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.CASEMT_LSCOUNTTIME.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.CASEMT_LSCOUNTTIME.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT_LS.CASEMT_LSCOUNTTIME);
UsrCfg.InternoAtisNames.SDCASEMT_LS.NAME_TABLE = "SDCASEMT_LS"; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.CASEMT_LSCOUNTTIMEPAUSE.FieldName = "CASEMT_LSCOUNTTIMEPAUSE"; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.CASEMT_LSCOUNTTIMEPAUSE.TableName = "SDCASEMT_LS"; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.CASEMT_LSCOUNTTIMEPAUSE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.CASEMT_LSCOUNTTIMEPAUSE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT_LS.CASEMT_LSCOUNTTIMEPAUSE);
UsrCfg.InternoAtisNames.SDCASEMT_LS.NAME_TABLE = "SDCASEMT_LS"; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.CASEMT_LSCOUNTTIMERESOLVED.FieldName = "CASEMT_LSCOUNTTIMERESOLVED"; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.CASEMT_LSCOUNTTIMERESOLVED.TableName = "SDCASEMT_LS"; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.CASEMT_LSCOUNTTIMERESOLVED.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.CASEMT_LSCOUNTTIMERESOLVED.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT_LS.CASEMT_LSCOUNTTIMERESOLVED);
UsrCfg.InternoAtisNames.SDCASEMT_LS.NAME_TABLE = "SDCASEMT_LS"; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.CASEMT_LSDATELASTCUT.FieldName = "CASEMT_LSDATELASTCUT"; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.CASEMT_LSDATELASTCUT.TableName = "SDCASEMT_LS"; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.CASEMT_LSDATELASTCUT.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.SDCASEMT_LS.CASEMT_LSDATELASTCUT.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT_LS.CASEMT_LSDATELASTCUT);
UsrCfg.InternoAtisNames.SDCASEMT_CITYPE.NAME_TABLE = "SDCASEMT_CITYPE"; 
UsrCfg.InternoAtisNames.SDCASEMT_CITYPE.IDSDCASEMT_CITYPE.FieldName = "IDSDCASEMT_CITYPE"; 
UsrCfg.InternoAtisNames.SDCASEMT_CITYPE.IDSDCASEMT_CITYPE.TableName = "SDCASEMT_CITYPE"; 
UsrCfg.InternoAtisNames.SDCASEMT_CITYPE.IDSDCASEMT_CITYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMT_CITYPE.IDSDCASEMT_CITYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT_CITYPE.IDSDCASEMT_CITYPE);
UsrCfg.InternoAtisNames.SDCASEMT_CITYPE.NAME_TABLE = "SDCASEMT_CITYPE"; 
UsrCfg.InternoAtisNames.SDCASEMT_CITYPE.SDCASEMT_CITYPENAME.FieldName = "SDCASEMT_CITYPENAME"; 
UsrCfg.InternoAtisNames.SDCASEMT_CITYPE.SDCASEMT_CITYPENAME.TableName = "SDCASEMT_CITYPE"; 
UsrCfg.InternoAtisNames.SDCASEMT_CITYPE.SDCASEMT_CITYPENAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.SDCASEMT_CITYPE.SDCASEMT_CITYPENAME.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT_CITYPE.SDCASEMT_CITYPENAME);
UsrCfg.InternoAtisNames.SDCASEMT_CI.NAME_TABLE = "SDCASEMT_CI"; 
UsrCfg.InternoAtisNames.SDCASEMT_CI.IDSDCASEMT_CI.FieldName = "IDSDCASEMT_CI"; 
UsrCfg.InternoAtisNames.SDCASEMT_CI.IDSDCASEMT_CI.TableName = "SDCASEMT_CI"; 
UsrCfg.InternoAtisNames.SDCASEMT_CI.IDSDCASEMT_CI.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.SDCASEMT_CI.IDSDCASEMT_CI.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT_CI.IDSDCASEMT_CI);
UsrCfg.InternoAtisNames.SDCASEMT_CI.NAME_TABLE = "SDCASEMT_CI"; 
UsrCfg.InternoAtisNames.SDCASEMT_CI.IDSDCASEMT_CITYPE.FieldName = "IDSDCASEMT_CITYPE"; 
UsrCfg.InternoAtisNames.SDCASEMT_CI.IDSDCASEMT_CITYPE.TableName = "SDCASEMT_CI"; 
UsrCfg.InternoAtisNames.SDCASEMT_CI.IDSDCASEMT_CITYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMT_CI.IDSDCASEMT_CITYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT_CI.IDSDCASEMT_CITYPE);
UsrCfg.InternoAtisNames.SDCASEMT_CI.NAME_TABLE = "SDCASEMT_CI"; 
UsrCfg.InternoAtisNames.SDCASEMT_CI.IDSDCASEMT.FieldName = "IDSDCASEMT"; 
UsrCfg.InternoAtisNames.SDCASEMT_CI.IDSDCASEMT.TableName = "SDCASEMT_CI"; 
UsrCfg.InternoAtisNames.SDCASEMT_CI.IDSDCASEMT.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMT_CI.IDSDCASEMT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT_CI.IDSDCASEMT);
UsrCfg.InternoAtisNames.SDCASEMT_CI.NAME_TABLE = "SDCASEMT_CI"; 
UsrCfg.InternoAtisNames.SDCASEMT_CI.IDCMDBCI.FieldName = "IDCMDBCI"; 
UsrCfg.InternoAtisNames.SDCASEMT_CI.IDCMDBCI.TableName = "SDCASEMT_CI"; 
UsrCfg.InternoAtisNames.SDCASEMT_CI.IDCMDBCI.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMT_CI.IDCMDBCI.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMT_CI.IDCMDBCI);
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_CMDBCI.NAME_TABLE = "MDCATEGORYDETAIL_CMDBCI"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_CMDBCI.IDMDCATEGORYDETAIL_CMDBCI.FieldName = "IDMDCATEGORYDETAIL_CMDBCI"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_CMDBCI.IDMDCATEGORYDETAIL_CMDBCI.TableName = "MDCATEGORYDETAIL_CMDBCI"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_CMDBCI.IDMDCATEGORYDETAIL_CMDBCI.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_CMDBCI.IDMDCATEGORYDETAIL_CMDBCI.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_CMDBCI.IDMDCATEGORYDETAIL_CMDBCI);
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_CMDBCI.NAME_TABLE = "MDCATEGORYDETAIL_CMDBCI"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_CMDBCI.IDMDCATEGORYDETAIL.FieldName = "IDMDCATEGORYDETAIL"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_CMDBCI.IDMDCATEGORYDETAIL.TableName = "MDCATEGORYDETAIL_CMDBCI"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_CMDBCI.IDMDCATEGORYDETAIL.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_CMDBCI.IDMDCATEGORYDETAIL.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_CMDBCI.IDMDCATEGORYDETAIL);
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_CMDBCI.NAME_TABLE = "MDCATEGORYDETAIL_CMDBCI"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_CMDBCI.IDCMDBCI.FieldName = "IDCMDBCI"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_CMDBCI.IDCMDBCI.TableName = "MDCATEGORYDETAIL_CMDBCI"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_CMDBCI.IDCMDBCI.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_CMDBCI.IDCMDBCI.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_CMDBCI.IDCMDBCI);
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_CMDBCI.NAME_TABLE = "MDCATEGORYDETAIL_CMDBCI"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_CMDBCI.IDSYSTEMSTATUS.FieldName = "IDSYSTEMSTATUS"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_CMDBCI.IDSYSTEMSTATUS.TableName = "MDCATEGORYDETAIL_CMDBCI"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_CMDBCI.IDSYSTEMSTATUS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_CMDBCI.IDSYSTEMSTATUS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_CMDBCI.IDSYSTEMSTATUS);
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_SDCASE.NAME_TABLE = "MDCATEGORYDETAIL_SDCASE"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_SDCASE.IDMDCATEGORYDETAIL_SDCASE.FieldName = "IDMDCATEGORYDETAIL_SDCASE"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_SDCASE.IDMDCATEGORYDETAIL_SDCASE.TableName = "MDCATEGORYDETAIL_SDCASE"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_SDCASE.IDMDCATEGORYDETAIL_SDCASE.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_SDCASE.IDMDCATEGORYDETAIL_SDCASE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_SDCASE.IDMDCATEGORYDETAIL_SDCASE);
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_SDCASE.NAME_TABLE = "MDCATEGORYDETAIL_SDCASE"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_SDCASE.IDMDCATEGORYDETAIL.FieldName = "IDMDCATEGORYDETAIL"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_SDCASE.IDMDCATEGORYDETAIL.TableName = "MDCATEGORYDETAIL_SDCASE"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_SDCASE.IDMDCATEGORYDETAIL.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_SDCASE.IDMDCATEGORYDETAIL.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_SDCASE.IDMDCATEGORYDETAIL);
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_SDCASE.NAME_TABLE = "MDCATEGORYDETAIL_SDCASE"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_SDCASE.IDSDCASE.FieldName = "IDSDCASE"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_SDCASE.IDSDCASE.TableName = "MDCATEGORYDETAIL_SDCASE"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_SDCASE.IDSDCASE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_SDCASE.IDSDCASE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_SDCASE.IDSDCASE);
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_SDCASE.NAME_TABLE = "MDCATEGORYDETAIL_SDCASE"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_SDCASE.IDCMDBCI.FieldName = "IDCMDBCI"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_SDCASE.IDCMDBCI.TableName = "MDCATEGORYDETAIL_SDCASE"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_SDCASE.IDCMDBCI.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_SDCASE.IDCMDBCI.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_SDCASE.IDCMDBCI);
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_SDCASE.NAME_TABLE = "MDCATEGORYDETAIL_SDCASE"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_SDCASE.IDSMWAREVIEW.FieldName = "IDSMWAREVIEW"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_SDCASE.IDSMWAREVIEW.TableName = "MDCATEGORYDETAIL_SDCASE"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_SDCASE.IDSMWAREVIEW.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_SDCASE.IDSMWAREVIEW.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_SDCASE.IDSMWAREVIEW);
UsrCfg.InternoAtisNames.SMWAREVIEW.NAME_TABLE = "SMWAREVIEW"; 
UsrCfg.InternoAtisNames.SMWAREVIEW.IDSMWAREVIEW.FieldName = "IDSMWAREVIEW"; 
UsrCfg.InternoAtisNames.SMWAREVIEW.IDSMWAREVIEW.TableName = "SMWAREVIEW"; 
UsrCfg.InternoAtisNames.SMWAREVIEW.IDSMWAREVIEW.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SMWAREVIEW.IDSMWAREVIEW.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SMWAREVIEW.IDSMWAREVIEW);
UsrCfg.InternoAtisNames.SMWAREVIEW.NAME_TABLE = "SMWAREVIEW"; 
UsrCfg.InternoAtisNames.SMWAREVIEW.SMWAREVIEW_NAME.FieldName = "SMWAREVIEW_NAME"; 
UsrCfg.InternoAtisNames.SMWAREVIEW.SMWAREVIEW_NAME.TableName = "SMWAREVIEW"; 
UsrCfg.InternoAtisNames.SMWAREVIEW.SMWAREVIEW_NAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.SMWAREVIEW.SMWAREVIEW_NAME.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SMWAREVIEW.SMWAREVIEW_NAME);
UsrCfg.InternoAtisNames.SDWHOTOCASE.NAME_TABLE = "SDWHOTOCASE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDWHOTOCASE.FieldName = "IDSDWHOTOCASE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDWHOTOCASE.TableName = "SDWHOTOCASE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDWHOTOCASE.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDWHOTOCASE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDWHOTOCASE);
UsrCfg.InternoAtisNames.SDWHOTOCASE.NAME_TABLE = "SDWHOTOCASE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDWHOTOCASESTATUS.FieldName = "IDSDWHOTOCASESTATUS"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDWHOTOCASESTATUS.TableName = "SDWHOTOCASE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDWHOTOCASESTATUS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDWHOTOCASESTATUS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDWHOTOCASESTATUS);
UsrCfg.InternoAtisNames.SDWHOTOCASE.NAME_TABLE = "SDWHOTOCASE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDCASE.FieldName = "IDSDCASE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDCASE.TableName = "SDWHOTOCASE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDCASE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDCASE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDCASE);
UsrCfg.InternoAtisNames.SDWHOTOCASE.NAME_TABLE = "SDWHOTOCASE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDCASEMT.FieldName = "IDSDCASEMT"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDCASEMT.TableName = "SDWHOTOCASE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDCASEMT.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDCASEMT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDCASEMT);
UsrCfg.InternoAtisNames.SDWHOTOCASE.NAME_TABLE = "SDWHOTOCASE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.IDCMDBCI.FieldName = "IDCMDBCI"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.IDCMDBCI.TableName = "SDWHOTOCASE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.IDCMDBCI.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.IDCMDBCI.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDWHOTOCASE.IDCMDBCI);
UsrCfg.InternoAtisNames.SDWHOTOCASE.NAME_TABLE = "SDWHOTOCASE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDTYPEUSER.FieldName = "IDSDTYPEUSER"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDTYPEUSER.TableName = "SDWHOTOCASE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDTYPEUSER.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDTYPEUSER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDTYPEUSER);
UsrCfg.InternoAtisNames.SDWHOTOCASE.NAME_TABLE = "SDWHOTOCASE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDCASEPERMISSION.FieldName = "IDSDCASEPERMISSION"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDCASEPERMISSION.TableName = "SDWHOTOCASE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDCASEPERMISSION.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDCASEPERMISSION.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDCASEPERMISSION);
UsrCfg.InternoAtisNames.SDWHOTOCASE.NAME_TABLE = "SDWHOTOCASE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.DATEPERMISSIONENABLE.FieldName = "DATEPERMISSIONENABLE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.DATEPERMISSIONENABLE.TableName = "SDWHOTOCASE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.DATEPERMISSIONENABLE.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.DATEPERMISSIONENABLE.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDWHOTOCASE.DATEPERMISSIONENABLE);
UsrCfg.InternoAtisNames.SDWHOTOCASE.NAME_TABLE = "SDWHOTOCASE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.DATEPERMISSIONDISABLE.FieldName = "DATEPERMISSIONDISABLE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.DATEPERMISSIONDISABLE.TableName = "SDWHOTOCASE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.DATEPERMISSIONDISABLE.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.DATEPERMISSIONDISABLE.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDWHOTOCASE.DATEPERMISSIONDISABLE);
UsrCfg.InternoAtisNames.SDWHOTOCASE.NAME_TABLE = "SDWHOTOCASE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.CASEMT_SET_LAVEL.FieldName = "CASEMT_SET_LAVEL"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.CASEMT_SET_LAVEL.TableName = "SDWHOTOCASE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.CASEMT_SET_LAVEL.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.CASEMT_SET_LAVEL.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDWHOTOCASE.CASEMT_SET_LAVEL);
UsrCfg.InternoAtisNames.SDWHOTOCASE.NAME_TABLE = "SDWHOTOCASE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.COUNTTIME.FieldName = "COUNTTIME"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.COUNTTIME.TableName = "SDWHOTOCASE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.COUNTTIME.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.COUNTTIME.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDWHOTOCASE.COUNTTIME);
UsrCfg.InternoAtisNames.SDWHOTOCASE.NAME_TABLE = "SDWHOTOCASE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.COUNTTIMEPAUSE.FieldName = "COUNTTIMEPAUSE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.COUNTTIMEPAUSE.TableName = "SDWHOTOCASE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.COUNTTIMEPAUSE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.COUNTTIMEPAUSE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDWHOTOCASE.COUNTTIMEPAUSE);
UsrCfg.InternoAtisNames.SDWHOTOCASE.NAME_TABLE = "SDWHOTOCASE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.COUNTTIMERESOLVED.FieldName = "COUNTTIMERESOLVED"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.COUNTTIMERESOLVED.TableName = "SDWHOTOCASE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.COUNTTIMERESOLVED.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.COUNTTIMERESOLVED.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDWHOTOCASE.COUNTTIMERESOLVED);
UsrCfg.InternoAtisNames.SDWHOTOCASE.NAME_TABLE = "SDWHOTOCASE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.DATELASTCUT.FieldName = "DATELASTCUT"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.DATELASTCUT.TableName = "SDWHOTOCASE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.DATELASTCUT.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.DATELASTCUT.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDWHOTOCASE.DATELASTCUT);
UsrCfg.InternoAtisNames.SDWHOTOCASE.NAME_TABLE = "SDWHOTOCASE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.DATELASTREAD.FieldName = "DATELASTREAD"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.DATELASTREAD.TableName = "SDWHOTOCASE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.DATELASTREAD.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.DATELASTREAD.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDWHOTOCASE.DATELASTREAD);
UsrCfg.InternoAtisNames.SDWHOTOCASE.NAME_TABLE = "SDWHOTOCASE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDWHOTOCASETYPE.FieldName = "IDSDWHOTOCASETYPE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDWHOTOCASETYPE.TableName = "SDWHOTOCASE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDWHOTOCASETYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDWHOTOCASETYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDWHOTOCASETYPE);
UsrCfg.InternoAtisNames.SDWHOTOCASESTATUS.NAME_TABLE = "SDWHOTOCASESTATUS"; 
UsrCfg.InternoAtisNames.SDWHOTOCASESTATUS.IDSDWHOTOCASESTATUS.FieldName = "IDSDWHOTOCASESTATUS"; 
UsrCfg.InternoAtisNames.SDWHOTOCASESTATUS.IDSDWHOTOCASESTATUS.TableName = "SDWHOTOCASESTATUS"; 
UsrCfg.InternoAtisNames.SDWHOTOCASESTATUS.IDSDWHOTOCASESTATUS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDWHOTOCASESTATUS.IDSDWHOTOCASESTATUS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDWHOTOCASESTATUS.IDSDWHOTOCASESTATUS);
UsrCfg.InternoAtisNames.SDWHOTOCASESTATUS.NAME_TABLE = "SDWHOTOCASESTATUS"; 
UsrCfg.InternoAtisNames.SDWHOTOCASESTATUS.WHOTOCASENAME.FieldName = "WHOTOCASENAME"; 
UsrCfg.InternoAtisNames.SDWHOTOCASESTATUS.WHOTOCASENAME.TableName = "SDWHOTOCASESTATUS"; 
UsrCfg.InternoAtisNames.SDWHOTOCASESTATUS.WHOTOCASENAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.SDWHOTOCASESTATUS.WHOTOCASENAME.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDWHOTOCASESTATUS.WHOTOCASENAME);
UsrCfg.InternoAtisNames.SDWHOTOCASETYPE.NAME_TABLE = "SDWHOTOCASETYPE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASETYPE.IDSDWHOTOCASETYPE.FieldName = "IDSDWHOTOCASETYPE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASETYPE.IDSDWHOTOCASETYPE.TableName = "SDWHOTOCASETYPE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASETYPE.IDSDWHOTOCASETYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDWHOTOCASETYPE.IDSDWHOTOCASETYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDWHOTOCASETYPE.IDSDWHOTOCASETYPE);
UsrCfg.InternoAtisNames.SDWHOTOCASETYPE.NAME_TABLE = "SDWHOTOCASETYPE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASETYPE.SDWHOTOCASETYPENAME.FieldName = "SDWHOTOCASETYPENAME"; 
UsrCfg.InternoAtisNames.SDWHOTOCASETYPE.SDWHOTOCASETYPENAME.TableName = "SDWHOTOCASETYPE"; 
UsrCfg.InternoAtisNames.SDWHOTOCASETYPE.SDWHOTOCASETYPENAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.SDWHOTOCASETYPE.SDWHOTOCASETYPENAME.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDWHOTOCASETYPE.SDWHOTOCASETYPENAME);
UsrCfg.InternoAtisNames.SDCASESTATUS.NAME_TABLE = "SDCASESTATUS"; 
UsrCfg.InternoAtisNames.SDCASESTATUS.IDSDCASESTATUS.FieldName = "IDSDCASESTATUS"; 
UsrCfg.InternoAtisNames.SDCASESTATUS.IDSDCASESTATUS.TableName = "SDCASESTATUS"; 
UsrCfg.InternoAtisNames.SDCASESTATUS.IDSDCASESTATUS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASESTATUS.IDSDCASESTATUS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASESTATUS.IDSDCASESTATUS);
UsrCfg.InternoAtisNames.SDCASESTATUS.NAME_TABLE = "SDCASESTATUS"; 
UsrCfg.InternoAtisNames.SDCASESTATUS.CASESTATUSNAME.FieldName = "CASESTATUSNAME"; 
UsrCfg.InternoAtisNames.SDCASESTATUS.CASESTATUSNAME.TableName = "SDCASESTATUS"; 
UsrCfg.InternoAtisNames.SDCASESTATUS.CASESTATUSNAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.SDCASESTATUS.CASESTATUSNAME.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASESTATUS.CASESTATUSNAME);
UsrCfg.InternoAtisNames.SDCASEMTSTATUS.NAME_TABLE = "SDCASEMTSTATUS"; 
UsrCfg.InternoAtisNames.SDCASEMTSTATUS.IDSDCASEMTSTATUS.FieldName = "IDSDCASEMTSTATUS"; 
UsrCfg.InternoAtisNames.SDCASEMTSTATUS.IDSDCASEMTSTATUS.TableName = "SDCASEMTSTATUS"; 
UsrCfg.InternoAtisNames.SDCASEMTSTATUS.IDSDCASEMTSTATUS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEMTSTATUS.IDSDCASEMTSTATUS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMTSTATUS.IDSDCASEMTSTATUS);
UsrCfg.InternoAtisNames.SDCASEMTSTATUS.NAME_TABLE = "SDCASEMTSTATUS"; 
UsrCfg.InternoAtisNames.SDCASEMTSTATUS.CASEMTSTATUSNAME.FieldName = "CASEMTSTATUSNAME"; 
UsrCfg.InternoAtisNames.SDCASEMTSTATUS.CASEMTSTATUSNAME.TableName = "SDCASEMTSTATUS"; 
UsrCfg.InternoAtisNames.SDCASEMTSTATUS.CASEMTSTATUSNAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.SDCASEMTSTATUS.CASEMTSTATUSNAME.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEMTSTATUS.CASEMTSTATUSNAME);
UsrCfg.InternoAtisNames.SDSCALETYPE_FUNC.NAME_TABLE = "SDSCALETYPE_FUNC"; 
UsrCfg.InternoAtisNames.SDSCALETYPE_FUNC.IDSDSCALETYPE_FUNC.FieldName = "IDSDSCALETYPE_FUNC"; 
UsrCfg.InternoAtisNames.SDSCALETYPE_FUNC.IDSDSCALETYPE_FUNC.TableName = "SDSCALETYPE_FUNC"; 
UsrCfg.InternoAtisNames.SDSCALETYPE_FUNC.IDSDSCALETYPE_FUNC.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDSCALETYPE_FUNC.IDSDSCALETYPE_FUNC.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDSCALETYPE_FUNC.IDSDSCALETYPE_FUNC);
UsrCfg.InternoAtisNames.SDSCALETYPE_FUNC.NAME_TABLE = "SDSCALETYPE_FUNC"; 
UsrCfg.InternoAtisNames.SDSCALETYPE_FUNC.SDSCALETYPE_FUNCNAME.FieldName = "SDSCALETYPE_FUNCNAME"; 
UsrCfg.InternoAtisNames.SDSCALETYPE_FUNC.SDSCALETYPE_FUNCNAME.TableName = "SDSCALETYPE_FUNC"; 
UsrCfg.InternoAtisNames.SDSCALETYPE_FUNC.SDSCALETYPE_FUNCNAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.SDSCALETYPE_FUNC.SDSCALETYPE_FUNCNAME.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDSCALETYPE_FUNC.SDSCALETYPE_FUNCNAME);
UsrCfg.InternoAtisNames.SDSCALETYPE_HIER.NAME_TABLE = "SDSCALETYPE_HIER"; 
UsrCfg.InternoAtisNames.SDSCALETYPE_HIER.IDSDSCALETYPE_HIER.FieldName = "IDSDSCALETYPE_HIER"; 
UsrCfg.InternoAtisNames.SDSCALETYPE_HIER.IDSDSCALETYPE_HIER.TableName = "SDSCALETYPE_HIER"; 
UsrCfg.InternoAtisNames.SDSCALETYPE_HIER.IDSDSCALETYPE_HIER.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDSCALETYPE_HIER.IDSDSCALETYPE_HIER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDSCALETYPE_HIER.IDSDSCALETYPE_HIER);
UsrCfg.InternoAtisNames.SDSCALETYPE_HIER.NAME_TABLE = "SDSCALETYPE_HIER"; 
UsrCfg.InternoAtisNames.SDSCALETYPE_HIER.SDSCALETYPE_HIERNAME.FieldName = "SDSCALETYPE_HIERNAME"; 
UsrCfg.InternoAtisNames.SDSCALETYPE_HIER.SDSCALETYPE_HIERNAME.TableName = "SDSCALETYPE_HIER"; 
UsrCfg.InternoAtisNames.SDSCALETYPE_HIER.SDSCALETYPE_HIERNAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.SDSCALETYPE_HIER.SDSCALETYPE_HIERNAME.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDSCALETYPE_HIER.SDSCALETYPE_HIERNAME);
UsrCfg.InternoAtisNames.SDTYPEUSER.NAME_TABLE = "SDTYPEUSER"; 
UsrCfg.InternoAtisNames.SDTYPEUSER.IDSDTYPEUSER.FieldName = "IDSDTYPEUSER"; 
UsrCfg.InternoAtisNames.SDTYPEUSER.IDSDTYPEUSER.TableName = "SDTYPEUSER"; 
UsrCfg.InternoAtisNames.SDTYPEUSER.IDSDTYPEUSER.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDTYPEUSER.IDSDTYPEUSER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDTYPEUSER.IDSDTYPEUSER);
UsrCfg.InternoAtisNames.SDTYPEUSER.NAME_TABLE = "SDTYPEUSER"; 
UsrCfg.InternoAtisNames.SDTYPEUSER.TYPEUSERNAME.FieldName = "TYPEUSERNAME"; 
UsrCfg.InternoAtisNames.SDTYPEUSER.TYPEUSERNAME.TableName = "SDTYPEUSER"; 
UsrCfg.InternoAtisNames.SDTYPEUSER.TYPEUSERNAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.SDTYPEUSER.TYPEUSERNAME.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDTYPEUSER.TYPEUSERNAME);
UsrCfg.InternoAtisNames.SDTYPEUSER.NAME_TABLE = "SDTYPEUSER"; 
UsrCfg.InternoAtisNames.SDTYPEUSER.TYPEUSERDESCRIPTION.FieldName = "TYPEUSERDESCRIPTION"; 
UsrCfg.InternoAtisNames.SDTYPEUSER.TYPEUSERDESCRIPTION.TableName = "SDTYPEUSER"; 
UsrCfg.InternoAtisNames.SDTYPEUSER.TYPEUSERDESCRIPTION.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.SDTYPEUSER.TYPEUSERDESCRIPTION.Size = 255; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDTYPEUSER.TYPEUSERDESCRIPTION);
UsrCfg.InternoAtisNames.SDOPERATION_POST.NAME_TABLE = "SDOPERATION_POST"; 
UsrCfg.InternoAtisNames.SDOPERATION_POST.IDSDOPERATION_POST.FieldName = "IDSDOPERATION_POST"; 
UsrCfg.InternoAtisNames.SDOPERATION_POST.IDSDOPERATION_POST.TableName = "SDOPERATION_POST"; 
UsrCfg.InternoAtisNames.SDOPERATION_POST.IDSDOPERATION_POST.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.SDOPERATION_POST.IDSDOPERATION_POST.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_POST.IDSDOPERATION_POST);
UsrCfg.InternoAtisNames.SDOPERATION_POST.NAME_TABLE = "SDOPERATION_POST"; 
UsrCfg.InternoAtisNames.SDOPERATION_POST.IDSDWHOTOCASE.FieldName = "IDSDWHOTOCASE"; 
UsrCfg.InternoAtisNames.SDOPERATION_POST.IDSDWHOTOCASE.TableName = "SDOPERATION_POST"; 
UsrCfg.InternoAtisNames.SDOPERATION_POST.IDSDWHOTOCASE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDOPERATION_POST.IDSDWHOTOCASE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_POST.IDSDWHOTOCASE);
UsrCfg.InternoAtisNames.SDOPERATION_POST.NAME_TABLE = "SDOPERATION_POST"; 
UsrCfg.InternoAtisNames.SDOPERATION_POST.POST_MESSAGE.FieldName = "POST_MESSAGE"; 
UsrCfg.InternoAtisNames.SDOPERATION_POST.POST_MESSAGE.TableName = "SDOPERATION_POST"; 
UsrCfg.InternoAtisNames.SDOPERATION_POST.POST_MESSAGE.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.SDOPERATION_POST.POST_MESSAGE.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_POST.POST_MESSAGE);
UsrCfg.InternoAtisNames.SDOPERATION_POST.NAME_TABLE = "SDOPERATION_POST"; 
UsrCfg.InternoAtisNames.SDOPERATION_POST.MESSAGE_DATE.FieldName = "MESSAGE_DATE"; 
UsrCfg.InternoAtisNames.SDOPERATION_POST.MESSAGE_DATE.TableName = "SDOPERATION_POST"; 
UsrCfg.InternoAtisNames.SDOPERATION_POST.MESSAGE_DATE.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.SDOPERATION_POST.MESSAGE_DATE.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_POST.MESSAGE_DATE);
UsrCfg.InternoAtisNames.SDOPERATION_POST.NAME_TABLE = "SDOPERATION_POST"; 
UsrCfg.InternoAtisNames.SDOPERATION_POST.POST_IDSDTYPEUSER.FieldName = "POST_IDSDTYPEUSER"; 
UsrCfg.InternoAtisNames.SDOPERATION_POST.POST_IDSDTYPEUSER.TableName = "SDOPERATION_POST"; 
UsrCfg.InternoAtisNames.SDOPERATION_POST.POST_IDSDTYPEUSER.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDOPERATION_POST.POST_IDSDTYPEUSER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_POST.POST_IDSDTYPEUSER);
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.NAME_TABLE = "SDOPERATION_FUNCESC"; 
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.IDSDOPERATION_FUNCESC.FieldName = "IDSDOPERATION_FUNCESC"; 
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.IDSDOPERATION_FUNCESC.TableName = "SDOPERATION_FUNCESC"; 
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.IDSDOPERATION_FUNCESC.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.IDSDOPERATION_FUNCESC.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.IDSDOPERATION_FUNCESC);
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.NAME_TABLE = "SDOPERATION_FUNCESC"; 
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.IDSDWHOTOCASE.FieldName = "IDSDWHOTOCASE"; 
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.IDSDWHOTOCASE.TableName = "SDOPERATION_FUNCESC"; 
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.IDSDWHOTOCASE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.IDSDWHOTOCASE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.IDSDWHOTOCASE);
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.NAME_TABLE = "SDOPERATION_FUNCESC"; 
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.FUNCESC_MESSAGE.FieldName = "FUNCESC_MESSAGE"; 
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.FUNCESC_MESSAGE.TableName = "SDOPERATION_FUNCESC"; 
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.FUNCESC_MESSAGE.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.FUNCESC_MESSAGE.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.FUNCESC_MESSAGE);
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.NAME_TABLE = "SDOPERATION_FUNCESC"; 
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.IDSDSCALETYPE_FUNC.FieldName = "IDSDSCALETYPE_FUNC"; 
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.IDSDSCALETYPE_FUNC.TableName = "SDOPERATION_FUNCESC"; 
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.IDSDSCALETYPE_FUNC.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.IDSDSCALETYPE_FUNC.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.IDSDSCALETYPE_FUNC);
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.NAME_TABLE = "SDOPERATION_FUNCESC"; 
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.FUNCESC_DATE.FieldName = "FUNCESC_DATE"; 
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.FUNCESC_DATE.TableName = "SDOPERATION_FUNCESC"; 
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.FUNCESC_DATE.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.FUNCESC_DATE.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.FUNCESC_DATE);
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.NAME_TABLE = "SDOPERATION_FUNCESC"; 
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.IDSDOPERATION_ESCTYPE_FUNC.FieldName = "IDSDOPERATION_ESCTYPE_FUNC"; 
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.IDSDOPERATION_ESCTYPE_FUNC.TableName = "SDOPERATION_FUNCESC"; 
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.IDSDOPERATION_ESCTYPE_FUNC.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.IDSDOPERATION_ESCTYPE_FUNC.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.IDSDOPERATION_ESCTYPE_FUNC);
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.NAME_TABLE = "SDOPERATION_FUNCESC"; 
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.FUNCESC_LAVEL.FieldName = "FUNCESC_LAVEL"; 
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.FUNCESC_LAVEL.TableName = "SDOPERATION_FUNCESC"; 
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.FUNCESC_LAVEL.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.FUNCESC_LAVEL.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.FUNCESC_LAVEL);
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.NAME_TABLE = "SDOPERATION_FUNCESC"; 
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.FUNCESC_IDCMDBCI.FieldName = "FUNCESC_IDCMDBCI"; 
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.FUNCESC_IDCMDBCI.TableName = "SDOPERATION_FUNCESC"; 
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.FUNCESC_IDCMDBCI.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.FUNCESC_IDCMDBCI.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.FUNCESC_IDCMDBCI);
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.NAME_TABLE = "SDOPERATION_FUNCESC"; 
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.FUNCESC_APPLY.FieldName = "FUNCESC_APPLY"; 
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.FUNCESC_APPLY.TableName = "SDOPERATION_FUNCESC"; 
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.FUNCESC_APPLY.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.FUNCESC_APPLY.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.FUNCESC_APPLY);
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.NAME_TABLE = "SDOPERATION_HIERESC"; 
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.IDSDOPERATION_HIERESC.FieldName = "IDSDOPERATION_HIERESC"; 
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.IDSDOPERATION_HIERESC.TableName = "SDOPERATION_HIERESC"; 
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.IDSDOPERATION_HIERESC.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.IDSDOPERATION_HIERESC.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.IDSDOPERATION_HIERESC);
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.NAME_TABLE = "SDOPERATION_HIERESC"; 
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.IDSDWHOTOCASE.FieldName = "IDSDWHOTOCASE"; 
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.IDSDWHOTOCASE.TableName = "SDOPERATION_HIERESC"; 
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.IDSDWHOTOCASE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.IDSDWHOTOCASE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.IDSDWHOTOCASE);
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.NAME_TABLE = "SDOPERATION_HIERESC"; 
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.HIERESC_MESSAGE.FieldName = "HIERESC_MESSAGE"; 
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.HIERESC_MESSAGE.TableName = "SDOPERATION_HIERESC"; 
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.HIERESC_MESSAGE.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.HIERESC_MESSAGE.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.HIERESC_MESSAGE);
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.NAME_TABLE = "SDOPERATION_HIERESC"; 
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.IDSDSCALETYPE_HIER.FieldName = "IDSDSCALETYPE_HIER"; 
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.IDSDSCALETYPE_HIER.TableName = "SDOPERATION_HIERESC"; 
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.IDSDSCALETYPE_HIER.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.IDSDSCALETYPE_HIER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.IDSDSCALETYPE_HIER);
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.NAME_TABLE = "SDOPERATION_HIERESC"; 
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.HIERESC_DATE.FieldName = "HIERESC_DATE"; 
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.HIERESC_DATE.TableName = "SDOPERATION_HIERESC"; 
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.HIERESC_DATE.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.HIERESC_DATE.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.HIERESC_DATE);
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.NAME_TABLE = "SDOPERATION_HIERESC"; 
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.IDSDOPERATION_ESCTYPE_HIER.FieldName = "IDSDOPERATION_ESCTYPE_HIER"; 
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.IDSDOPERATION_ESCTYPE_HIER.TableName = "SDOPERATION_HIERESC"; 
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.IDSDOPERATION_ESCTYPE_HIER.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.IDSDOPERATION_ESCTYPE_HIER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.IDSDOPERATION_ESCTYPE_HIER);
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.NAME_TABLE = "SDOPERATION_HIERESC"; 
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.HIERESC_LAVEL.FieldName = "HIERESC_LAVEL"; 
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.HIERESC_LAVEL.TableName = "SDOPERATION_HIERESC"; 
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.HIERESC_LAVEL.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.HIERESC_LAVEL.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.HIERESC_LAVEL);
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.NAME_TABLE = "SDOPERATION_HIERESC"; 
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.HIERESC_IDCMDBCI.FieldName = "HIERESC_IDCMDBCI"; 
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.HIERESC_IDCMDBCI.TableName = "SDOPERATION_HIERESC"; 
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.HIERESC_IDCMDBCI.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.HIERESC_IDCMDBCI.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.HIERESC_IDCMDBCI);
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.NAME_TABLE = "SDOPERATION_HIERESC"; 
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.HIERESC_APPLY.FieldName = "HIERESC_APPLY"; 
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.HIERESC_APPLY.TableName = "SDOPERATION_HIERESC"; 
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.HIERESC_APPLY.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.HIERESC_APPLY.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.HIERESC_APPLY);
UsrCfg.InternoAtisNames.SDOPERATION_ESCTYPE.NAME_TABLE = "SDOPERATION_ESCTYPE"; 
UsrCfg.InternoAtisNames.SDOPERATION_ESCTYPE.IDSDOPERATION_ESCTYPE.FieldName = "IDSDOPERATION_ESCTYPE"; 
UsrCfg.InternoAtisNames.SDOPERATION_ESCTYPE.IDSDOPERATION_ESCTYPE.TableName = "SDOPERATION_ESCTYPE"; 
UsrCfg.InternoAtisNames.SDOPERATION_ESCTYPE.IDSDOPERATION_ESCTYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDOPERATION_ESCTYPE.IDSDOPERATION_ESCTYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_ESCTYPE.IDSDOPERATION_ESCTYPE);
UsrCfg.InternoAtisNames.SDOPERATION_ESCTYPE.NAME_TABLE = "SDOPERATION_ESCTYPE"; 
UsrCfg.InternoAtisNames.SDOPERATION_ESCTYPE.ESCTYPEENAME.FieldName = "ESCTYPEENAME"; 
UsrCfg.InternoAtisNames.SDOPERATION_ESCTYPE.ESCTYPEENAME.TableName = "SDOPERATION_ESCTYPE"; 
UsrCfg.InternoAtisNames.SDOPERATION_ESCTYPE.ESCTYPEENAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.SDOPERATION_ESCTYPE.ESCTYPEENAME.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_ESCTYPE.ESCTYPEENAME);
UsrCfg.InternoAtisNames.SDOPERATION_PARENT.NAME_TABLE = "SDOPERATION_PARENT"; 
UsrCfg.InternoAtisNames.SDOPERATION_PARENT.IDSDOPERATION_PARENT.FieldName = "IDSDOPERATION_PARENT"; 
UsrCfg.InternoAtisNames.SDOPERATION_PARENT.IDSDOPERATION_PARENT.TableName = "SDOPERATION_PARENT"; 
UsrCfg.InternoAtisNames.SDOPERATION_PARENT.IDSDOPERATION_PARENT.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.SDOPERATION_PARENT.IDSDOPERATION_PARENT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_PARENT.IDSDOPERATION_PARENT);
UsrCfg.InternoAtisNames.SDOPERATION_PARENT.NAME_TABLE = "SDOPERATION_PARENT"; 
UsrCfg.InternoAtisNames.SDOPERATION_PARENT.IDSDCASE_PARENT.FieldName = "IDSDCASE_PARENT"; 
UsrCfg.InternoAtisNames.SDOPERATION_PARENT.IDSDCASE_PARENT.TableName = "SDOPERATION_PARENT"; 
UsrCfg.InternoAtisNames.SDOPERATION_PARENT.IDSDCASE_PARENT.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDOPERATION_PARENT.IDSDCASE_PARENT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_PARENT.IDSDCASE_PARENT);
UsrCfg.InternoAtisNames.SDOPERATION_PARENT.NAME_TABLE = "SDOPERATION_PARENT"; 
UsrCfg.InternoAtisNames.SDOPERATION_PARENT.IDSDCASE.FieldName = "IDSDCASE"; 
UsrCfg.InternoAtisNames.SDOPERATION_PARENT.IDSDCASE.TableName = "SDOPERATION_PARENT"; 
UsrCfg.InternoAtisNames.SDOPERATION_PARENT.IDSDCASE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDOPERATION_PARENT.IDSDCASE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_PARENT.IDSDCASE);
UsrCfg.InternoAtisNames.SDOPERATION_PARENT.NAME_TABLE = "SDOPERATION_PARENT"; 
UsrCfg.InternoAtisNames.SDOPERATION_PARENT.IDSDWHOTOCASE.FieldName = "IDSDWHOTOCASE"; 
UsrCfg.InternoAtisNames.SDOPERATION_PARENT.IDSDWHOTOCASE.TableName = "SDOPERATION_PARENT"; 
UsrCfg.InternoAtisNames.SDOPERATION_PARENT.IDSDWHOTOCASE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDOPERATION_PARENT.IDSDWHOTOCASE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_PARENT.IDSDWHOTOCASE);
UsrCfg.InternoAtisNames.SDOPERATION_PARENT.NAME_TABLE = "SDOPERATION_PARENT"; 
UsrCfg.InternoAtisNames.SDOPERATION_PARENT.PARENT_MESSAGE.FieldName = "PARENT_MESSAGE"; 
UsrCfg.InternoAtisNames.SDOPERATION_PARENT.PARENT_MESSAGE.TableName = "SDOPERATION_PARENT"; 
UsrCfg.InternoAtisNames.SDOPERATION_PARENT.PARENT_MESSAGE.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.SDOPERATION_PARENT.PARENT_MESSAGE.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_PARENT.PARENT_MESSAGE);
UsrCfg.InternoAtisNames.SDOPERATION_PARENT.NAME_TABLE = "SDOPERATION_PARENT"; 
UsrCfg.InternoAtisNames.SDOPERATION_PARENT.PARENT_DATE.FieldName = "PARENT_DATE"; 
UsrCfg.InternoAtisNames.SDOPERATION_PARENT.PARENT_DATE.TableName = "SDOPERATION_PARENT"; 
UsrCfg.InternoAtisNames.SDOPERATION_PARENT.PARENT_DATE.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.SDOPERATION_PARENT.PARENT_DATE.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_PARENT.PARENT_DATE);
UsrCfg.InternoAtisNames.SDOPERATION_PARENT.NAME_TABLE = "SDOPERATION_PARENT"; 
UsrCfg.InternoAtisNames.SDOPERATION_PARENT.IDSDOPERATION_PARENTTYPE.FieldName = "IDSDOPERATION_PARENTTYPE"; 
UsrCfg.InternoAtisNames.SDOPERATION_PARENT.IDSDOPERATION_PARENTTYPE.TableName = "SDOPERATION_PARENT"; 
UsrCfg.InternoAtisNames.SDOPERATION_PARENT.IDSDOPERATION_PARENTTYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDOPERATION_PARENT.IDSDOPERATION_PARENTTYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_PARENT.IDSDOPERATION_PARENTTYPE);
UsrCfg.InternoAtisNames.SDOPERATION_PARENTTYPE.NAME_TABLE = "SDOPERATION_PARENTTYPE"; 
UsrCfg.InternoAtisNames.SDOPERATION_PARENTTYPE.IDSDOPERATION_PARENTTYPE.FieldName = "IDSDOPERATION_PARENTTYPE"; 
UsrCfg.InternoAtisNames.SDOPERATION_PARENTTYPE.IDSDOPERATION_PARENTTYPE.TableName = "SDOPERATION_PARENTTYPE"; 
UsrCfg.InternoAtisNames.SDOPERATION_PARENTTYPE.IDSDOPERATION_PARENTTYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDOPERATION_PARENTTYPE.IDSDOPERATION_PARENTTYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_PARENTTYPE.IDSDOPERATION_PARENTTYPE);
UsrCfg.InternoAtisNames.SDOPERATION_PARENTTYPE.NAME_TABLE = "SDOPERATION_PARENTTYPE"; 
UsrCfg.InternoAtisNames.SDOPERATION_PARENTTYPE.PARENTTYPENAME.FieldName = "PARENTTYPENAME"; 
UsrCfg.InternoAtisNames.SDOPERATION_PARENTTYPE.PARENTTYPENAME.TableName = "SDOPERATION_PARENTTYPE"; 
UsrCfg.InternoAtisNames.SDOPERATION_PARENTTYPE.PARENTTYPENAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.SDOPERATION_PARENTTYPE.PARENTTYPENAME.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_PARENTTYPE.PARENTTYPENAME);
UsrCfg.InternoAtisNames.SDOPERATION_CASESTATUS.NAME_TABLE = "SDOPERATION_CASESTATUS"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESTATUS.IDSDOPERATION_CASESTATUS.FieldName = "IDSDOPERATION_CASESTATUS"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESTATUS.IDSDOPERATION_CASESTATUS.TableName = "SDOPERATION_CASESTATUS"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESTATUS.IDSDOPERATION_CASESTATUS.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESTATUS.IDSDOPERATION_CASESTATUS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_CASESTATUS.IDSDOPERATION_CASESTATUS);
UsrCfg.InternoAtisNames.SDOPERATION_CASESTATUS.NAME_TABLE = "SDOPERATION_CASESTATUS"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESTATUS.IDSDWHOTOCASE.FieldName = "IDSDWHOTOCASE"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESTATUS.IDSDWHOTOCASE.TableName = "SDOPERATION_CASESTATUS"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESTATUS.IDSDWHOTOCASE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESTATUS.IDSDWHOTOCASE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_CASESTATUS.IDSDWHOTOCASE);
UsrCfg.InternoAtisNames.SDOPERATION_CASESTATUS.NAME_TABLE = "SDOPERATION_CASESTATUS"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESTATUS.CASESTATUS_MESSAGE.FieldName = "CASESTATUS_MESSAGE"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESTATUS.CASESTATUS_MESSAGE.TableName = "SDOPERATION_CASESTATUS"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESTATUS.CASESTATUS_MESSAGE.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESTATUS.CASESTATUS_MESSAGE.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_CASESTATUS.CASESTATUS_MESSAGE);
UsrCfg.InternoAtisNames.SDOPERATION_CASESTATUS.NAME_TABLE = "SDOPERATION_CASESTATUS"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESTATUS.CASESTATUS_DATE.FieldName = "CASESTATUS_DATE"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESTATUS.CASESTATUS_DATE.TableName = "SDOPERATION_CASESTATUS"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESTATUS.CASESTATUS_DATE.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESTATUS.CASESTATUS_DATE.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_CASESTATUS.CASESTATUS_DATE);
UsrCfg.InternoAtisNames.SDOPERATION_CASESTATUS.NAME_TABLE = "SDOPERATION_CASESTATUS"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESTATUS.IDSDCASESTATUS.FieldName = "IDSDCASESTATUS"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESTATUS.IDSDCASESTATUS.TableName = "SDOPERATION_CASESTATUS"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESTATUS.IDSDCASESTATUS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESTATUS.IDSDCASESTATUS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_CASESTATUS.IDSDCASESTATUS);
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.NAME_TABLE = "SDOPERATION_CASESETMODEL"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDSDOPERATION_CASESETMODEL.FieldName = "IDSDOPERATION_CASESETMODEL"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDSDOPERATION_CASESETMODEL.TableName = "SDOPERATION_CASESETMODEL"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDSDOPERATION_CASESETMODEL.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDSDOPERATION_CASESETMODEL.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDSDOPERATION_CASESETMODEL);
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.NAME_TABLE = "SDOPERATION_CASESETMODEL"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDSDWHOTOCASE.FieldName = "IDSDWHOTOCASE"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDSDWHOTOCASE.TableName = "SDOPERATION_CASESETMODEL"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDSDWHOTOCASE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDSDWHOTOCASE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDSDWHOTOCASE);
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.NAME_TABLE = "SDOPERATION_CASESETMODEL"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.CASESETMODEL_MESSAGE.FieldName = "CASESETMODEL_MESSAGE"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.CASESETMODEL_MESSAGE.TableName = "SDOPERATION_CASESETMODEL"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.CASESETMODEL_MESSAGE.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.CASESETMODEL_MESSAGE.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.CASESETMODEL_MESSAGE);
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.NAME_TABLE = "SDOPERATION_CASESETMODEL"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.CASESETMODEL_DATE.FieldName = "CASESETMODEL_DATE"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.CASESETMODEL_DATE.TableName = "SDOPERATION_CASESETMODEL"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.CASESETMODEL_DATE.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.CASESETMODEL_DATE.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.CASESETMODEL_DATE);
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.NAME_TABLE = "SDOPERATION_CASESETMODEL"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDSDCASESETMODELTYPE.FieldName = "IDSDCASESETMODELTYPE"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDSDCASESETMODELTYPE.TableName = "SDOPERATION_CASESETMODEL"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDSDCASESETMODELTYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDSDCASESETMODELTYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDSDCASESETMODELTYPE);
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.NAME_TABLE = "SDOPERATION_CASESETMODEL"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.MT_IDMDMODELTYPED.FieldName = "MT_IDMDMODELTYPED"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.MT_IDMDMODELTYPED.TableName = "SDOPERATION_CASESETMODEL"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.MT_IDMDMODELTYPED.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.MT_IDMDMODELTYPED.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.MT_IDMDMODELTYPED);
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.NAME_TABLE = "SDOPERATION_CASESETMODEL"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDSDCASE.FieldName = "IDSDCASE"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDSDCASE.TableName = "SDOPERATION_CASESETMODEL"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDSDCASE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDSDCASE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDSDCASE);
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.NAME_TABLE = "SDOPERATION_CASESETMODEL"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDSDCASEMT.FieldName = "IDSDCASEMT"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDSDCASEMT.TableName = "SDOPERATION_CASESETMODEL"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDSDCASEMT.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDSDCASEMT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDSDCASEMT);
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.NAME_TABLE = "SDOPERATION_CASESETMODEL"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDMDCATEGORYDETAIL.FieldName = "IDMDCATEGORYDETAIL"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDMDCATEGORYDETAIL.TableName = "SDOPERATION_CASESETMODEL"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDMDCATEGORYDETAIL.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDMDCATEGORYDETAIL.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDMDCATEGORYDETAIL);
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.NAME_TABLE = "SDOPERATION_CASESETMODEL"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDMDMODELTYPED.FieldName = "IDMDMODELTYPED"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDMDMODELTYPED.TableName = "SDOPERATION_CASESETMODEL"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDMDMODELTYPED.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDMDMODELTYPED.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDMDMODELTYPED);
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.NAME_TABLE = "SDOPERATION_CASESETMODEL"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDMDPRIORITY.FieldName = "IDMDPRIORITY"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDMDPRIORITY.TableName = "SDOPERATION_CASESETMODEL"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDMDPRIORITY.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDMDPRIORITY.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDMDPRIORITY);
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.NAME_TABLE = "SDOPERATION_CASESETMODEL"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDSLA.FieldName = "IDSLA"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDSLA.TableName = "SDOPERATION_CASESETMODEL"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDSLA.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDSLA.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDSLA);
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.NAME_TABLE = "SDOPERATION_CASESETMODEL"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDMDIMPACT.FieldName = "IDMDIMPACT"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDMDIMPACT.TableName = "SDOPERATION_CASESETMODEL"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDMDIMPACT.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDMDIMPACT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDMDIMPACT);
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.NAME_TABLE = "SDOPERATION_CASESETMODEL"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDMDURGENCY.FieldName = "IDMDURGENCY"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDMDURGENCY.TableName = "SDOPERATION_CASESETMODEL"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDMDURGENCY.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDMDURGENCY.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.IDMDURGENCY);
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.NAME_TABLE = "SDOPERATION_CASESETMODEL"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.CASE_ISMAYOR.FieldName = "CASE_ISMAYOR"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.CASE_ISMAYOR.TableName = "SDOPERATION_CASESETMODEL"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.CASE_ISMAYOR.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.CASE_ISMAYOR.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.CASE_ISMAYOR);
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.NAME_TABLE = "SDOPERATION_CASESETMODEL"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.CASE_DESCRIPTION.FieldName = "CASE_DESCRIPTION"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.CASE_DESCRIPTION.TableName = "SDOPERATION_CASESETMODEL"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.CASE_DESCRIPTION.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.CASE_DESCRIPTION.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.CASE_DESCRIPTION);
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.NAME_TABLE = "SDOPERATION_CASESETMODEL"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.CASE_TITLE.FieldName = "CASE_TITLE"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.CASE_TITLE.TableName = "SDOPERATION_CASESETMODEL"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.CASE_TITLE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.CASE_TITLE.Size = 255; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_CASESETMODEL.CASE_TITLE);
UsrCfg.InternoAtisNames.SDCASESETMODELTYPE.NAME_TABLE = "SDCASESETMODELTYPE"; 
UsrCfg.InternoAtisNames.SDCASESETMODELTYPE.IDSDCASESETMODELTYPE.FieldName = "IDSDCASESETMODELTYPE"; 
UsrCfg.InternoAtisNames.SDCASESETMODELTYPE.IDSDCASESETMODELTYPE.TableName = "SDCASESETMODELTYPE"; 
UsrCfg.InternoAtisNames.SDCASESETMODELTYPE.IDSDCASESETMODELTYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASESETMODELTYPE.IDSDCASESETMODELTYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASESETMODELTYPE.IDSDCASESETMODELTYPE);
UsrCfg.InternoAtisNames.SDCASESETMODELTYPE.NAME_TABLE = "SDCASESETMODELTYPE"; 
UsrCfg.InternoAtisNames.SDCASESETMODELTYPE.SETMODELTYPENAME.FieldName = "SETMODELTYPENAME"; 
UsrCfg.InternoAtisNames.SDCASESETMODELTYPE.SETMODELTYPENAME.TableName = "SDCASESETMODELTYPE"; 
UsrCfg.InternoAtisNames.SDCASESETMODELTYPE.SETMODELTYPENAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.SDCASESETMODELTYPE.SETMODELTYPENAME.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASESETMODELTYPE.SETMODELTYPENAME);
UsrCfg.InternoAtisNames.SDOPERATION_CASEACTIVITIES.NAME_TABLE = "SDOPERATION_CASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASEACTIVITIES.IDSDOPERATION_CASEACTIVITIES.FieldName = "IDSDOPERATION_CASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASEACTIVITIES.IDSDOPERATION_CASEACTIVITIES.TableName = "SDOPERATION_CASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASEACTIVITIES.IDSDOPERATION_CASEACTIVITIES.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.SDOPERATION_CASEACTIVITIES.IDSDOPERATION_CASEACTIVITIES.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_CASEACTIVITIES.IDSDOPERATION_CASEACTIVITIES);
UsrCfg.InternoAtisNames.SDOPERATION_CASEACTIVITIES.NAME_TABLE = "SDOPERATION_CASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASEACTIVITIES.IDSDWHOTOCASE.FieldName = "IDSDWHOTOCASE"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASEACTIVITIES.IDSDWHOTOCASE.TableName = "SDOPERATION_CASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASEACTIVITIES.IDSDWHOTOCASE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDOPERATION_CASEACTIVITIES.IDSDWHOTOCASE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_CASEACTIVITIES.IDSDWHOTOCASE);
UsrCfg.InternoAtisNames.SDOPERATION_CASEACTIVITIES.NAME_TABLE = "SDOPERATION_CASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASEACTIVITIES.CASEACTIVITIES_MESSAGE.FieldName = "CASEACTIVITIES_MESSAGE"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASEACTIVITIES.CASEACTIVITIES_MESSAGE.TableName = "SDOPERATION_CASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASEACTIVITIES.CASEACTIVITIES_MESSAGE.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.SDOPERATION_CASEACTIVITIES.CASEACTIVITIES_MESSAGE.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_CASEACTIVITIES.CASEACTIVITIES_MESSAGE);
UsrCfg.InternoAtisNames.SDOPERATION_CASEACTIVITIES.NAME_TABLE = "SDOPERATION_CASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASEACTIVITIES.CASEACTIVITIES_DATE.FieldName = "CASEACTIVITIES_DATE"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASEACTIVITIES.CASEACTIVITIES_DATE.TableName = "SDOPERATION_CASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASEACTIVITIES.CASEACTIVITIES_DATE.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.SDOPERATION_CASEACTIVITIES.CASEACTIVITIES_DATE.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_CASEACTIVITIES.CASEACTIVITIES_DATE);
UsrCfg.InternoAtisNames.SDOPERATION_CASEACTIVITIES.NAME_TABLE = "SDOPERATION_CASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASEACTIVITIES.IDSDRUNNIGSTATUS.FieldName = "IDSDRUNNIGSTATUS"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASEACTIVITIES.IDSDRUNNIGSTATUS.TableName = "SDOPERATION_CASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDOPERATION_CASEACTIVITIES.IDSDRUNNIGSTATUS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDOPERATION_CASEACTIVITIES.IDSDRUNNIGSTATUS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_CASEACTIVITIES.IDSDRUNNIGSTATUS);
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.NAME_TABLE = "SDOPERATION_ATTENTION"; 
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.IDSDOPERATION_ATTENTION.FieldName = "IDSDOPERATION_ATTENTION"; 
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.IDSDOPERATION_ATTENTION.TableName = "SDOPERATION_ATTENTION"; 
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.IDSDOPERATION_ATTENTION.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.IDSDOPERATION_ATTENTION.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.IDSDOPERATION_ATTENTION);
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.NAME_TABLE = "SDOPERATION_ATTENTION"; 
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.IDSDWHOTOCASE.FieldName = "IDSDWHOTOCASE"; 
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.IDSDWHOTOCASE.TableName = "SDOPERATION_ATTENTION"; 
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.IDSDWHOTOCASE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.IDSDWHOTOCASE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.IDSDWHOTOCASE);
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.NAME_TABLE = "SDOPERATION_ATTENTION"; 
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.CASEMT_SET_LS_NAMESTEP.FieldName = "CASEMT_SET_LS_NAMESTEP"; 
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.CASEMT_SET_LS_NAMESTEP.TableName = "SDOPERATION_ATTENTION"; 
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.CASEMT_SET_LS_NAMESTEP.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.CASEMT_SET_LS_NAMESTEP.Size = 255; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.CASEMT_SET_LS_NAMESTEP);
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.NAME_TABLE = "SDOPERATION_ATTENTION"; 
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.CASEMT_SET_LS_STATUSN.FieldName = "CASEMT_SET_LS_STATUSN"; 
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.CASEMT_SET_LS_STATUSN.TableName = "SDOPERATION_ATTENTION"; 
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.CASEMT_SET_LS_STATUSN.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.CASEMT_SET_LS_STATUSN.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.CASEMT_SET_LS_STATUSN);
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.NAME_TABLE = "SDOPERATION_ATTENTION"; 
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.ATTENTION_MESSAGE.FieldName = "ATTENTION_MESSAGE"; 
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.ATTENTION_MESSAGE.TableName = "SDOPERATION_ATTENTION"; 
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.ATTENTION_MESSAGE.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.ATTENTION_MESSAGE.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.ATTENTION_MESSAGE);
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.NAME_TABLE = "SDOPERATION_ATTENTION"; 
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.ATTENTION_DATE.FieldName = "ATTENTION_DATE"; 
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.ATTENTION_DATE.TableName = "SDOPERATION_ATTENTION"; 
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.ATTENTION_DATE.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.ATTENTION_DATE.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.ATTENTION_DATE);
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.NAME_TABLE = "SDOPERATION_ATTENTION"; 
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.IDSDOPERATION_ATTENTIONTYPE.FieldName = "IDSDOPERATION_ATTENTIONTYPE"; 
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.IDSDOPERATION_ATTENTIONTYPE.TableName = "SDOPERATION_ATTENTION"; 
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.IDSDOPERATION_ATTENTIONTYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.IDSDOPERATION_ATTENTIONTYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.IDSDOPERATION_ATTENTIONTYPE);
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.NAME_TABLE = "SDOPERATION_ATTENTION"; 
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.LS_IDSDCASESTATUS.FieldName = "LS_IDSDCASESTATUS"; 
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.LS_IDSDCASESTATUS.TableName = "SDOPERATION_ATTENTION"; 
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.LS_IDSDCASESTATUS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.LS_IDSDCASESTATUS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.LS_IDSDCASESTATUS);
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTIONTYPE.NAME_TABLE = "SDOPERATION_ATTENTIONTYPE"; 
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTIONTYPE.IDSDOPERATION_ATTENTIONTYPE.FieldName = "IDSDOPERATION_ATTENTIONTYPE"; 
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTIONTYPE.IDSDOPERATION_ATTENTIONTYPE.TableName = "SDOPERATION_ATTENTIONTYPE"; 
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTIONTYPE.IDSDOPERATION_ATTENTIONTYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTIONTYPE.IDSDOPERATION_ATTENTIONTYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_ATTENTIONTYPE.IDSDOPERATION_ATTENTIONTYPE);
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTIONTYPE.NAME_TABLE = "SDOPERATION_ATTENTIONTYPE"; 
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTIONTYPE.OPERATIONTYPENAME.FieldName = "OPERATIONTYPENAME"; 
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTIONTYPE.OPERATIONTYPENAME.TableName = "SDOPERATION_ATTENTIONTYPE"; 
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTIONTYPE.OPERATIONTYPENAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.SDOPERATION_ATTENTIONTYPE.OPERATIONTYPENAME.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDOPERATION_ATTENTIONTYPE.OPERATIONTYPENAME);
UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.NAME_TABLE = "SDCASE_CMDB_ASSET"; 
UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.IDSDCASE_CMDB_ASSET.FieldName = "IDSDCASE_CMDB_ASSET"; 
UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.IDSDCASE_CMDB_ASSET.TableName = "SDCASE_CMDB_ASSET"; 
UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.IDSDCASE_CMDB_ASSET.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.IDSDCASE_CMDB_ASSET.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.IDSDCASE_CMDB_ASSET);
UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.NAME_TABLE = "SDCASE_CMDB_ASSET"; 
UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.IDSDCASE.FieldName = "IDSDCASE"; 
UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.IDSDCASE.TableName = "SDCASE_CMDB_ASSET"; 
UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.IDSDCASE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.IDSDCASE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.IDSDCASE);
UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.NAME_TABLE = "SDCASE_CMDB_ASSET"; 
UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.IDCMDBCI_AFFECTED.FieldName = "IDCMDBCI_AFFECTED"; 
UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.IDCMDBCI_AFFECTED.TableName = "SDCASE_CMDB_ASSET"; 
UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.IDCMDBCI_AFFECTED.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.IDCMDBCI_AFFECTED.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.IDCMDBCI_AFFECTED);
UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.NAME_TABLE = "SDCASE_CMDB_ASSET"; 
UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.DESCRIPTION.FieldName = "DESCRIPTION"; 
UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.DESCRIPTION.TableName = "SDCASE_CMDB_ASSET"; 
UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.DESCRIPTION.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.DESCRIPTION.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.DESCRIPTION);
UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.NAME_TABLE = "SDCASE_CMDB_ASSET"; 
UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.CMDB_ASSETDATE.FieldName = "CMDB_ASSETDATE"; 
UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.CMDB_ASSETDATE.TableName = "SDCASE_CMDB_ASSET"; 
UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.CMDB_ASSETDATE.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.CMDB_ASSETDATE.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.CMDB_ASSETDATE);
UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.NAME_TABLE = "SDCASE_CMDB_ASSET"; 
UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.IDCMDBCI.FieldName = "IDCMDBCI"; 
UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.IDCMDBCI.TableName = "SDCASE_CMDB_ASSET"; 
UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.IDCMDBCI.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.IDCMDBCI.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.IDCMDBCI);
UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.NAME_TABLE = "SDCASE_CMDB_ASSET"; 
UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.IDSDTYPEUSER.FieldName = "IDSDTYPEUSER"; 
UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.IDSDTYPEUSER.TableName = "SDCASE_CMDB_ASSET"; 
UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.IDSDTYPEUSER.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.IDSDTYPEUSER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.IDSDTYPEUSER);
UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.NAME_TABLE = "SDCASE_CMDB_ASSET"; 
UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.IDSYSTEMSTATUS.FieldName = "IDSYSTEMSTATUS"; 
UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.IDSYSTEMSTATUS.TableName = "SDCASE_CMDB_ASSET"; 
UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.IDSYSTEMSTATUS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.IDSYSTEMSTATUS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.IDSYSTEMSTATUS);
UsrCfg.InternoAtisNames.SDCASE_RELATION.NAME_TABLE = "SDCASE_RELATION"; 
UsrCfg.InternoAtisNames.SDCASE_RELATION.IDSDCASE_RELATION.FieldName = "IDSDCASE_RELATION"; 
UsrCfg.InternoAtisNames.SDCASE_RELATION.IDSDCASE_RELATION.TableName = "SDCASE_RELATION"; 
UsrCfg.InternoAtisNames.SDCASE_RELATION.IDSDCASE_RELATION.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.SDCASE_RELATION.IDSDCASE_RELATION.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE_RELATION.IDSDCASE_RELATION);
UsrCfg.InternoAtisNames.SDCASE_RELATION.NAME_TABLE = "SDCASE_RELATION"; 
UsrCfg.InternoAtisNames.SDCASE_RELATION.IDSDCASE_THIS.FieldName = "IDSDCASE_THIS"; 
UsrCfg.InternoAtisNames.SDCASE_RELATION.IDSDCASE_THIS.TableName = "SDCASE_RELATION"; 
UsrCfg.InternoAtisNames.SDCASE_RELATION.IDSDCASE_THIS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASE_RELATION.IDSDCASE_THIS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE_RELATION.IDSDCASE_THIS);
UsrCfg.InternoAtisNames.SDCASE_RELATION.NAME_TABLE = "SDCASE_RELATION"; 
UsrCfg.InternoAtisNames.SDCASE_RELATION.IDSDCASE_OF.FieldName = "IDSDCASE_OF"; 
UsrCfg.InternoAtisNames.SDCASE_RELATION.IDSDCASE_OF.TableName = "SDCASE_RELATION"; 
UsrCfg.InternoAtisNames.SDCASE_RELATION.IDSDCASE_OF.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASE_RELATION.IDSDCASE_OF.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE_RELATION.IDSDCASE_OF);
UsrCfg.InternoAtisNames.SDCASE_RELATION.NAME_TABLE = "SDCASE_RELATION"; 
UsrCfg.InternoAtisNames.SDCASE_RELATION.RELATIONS_TITLE.FieldName = "RELATIONS_TITLE"; 
UsrCfg.InternoAtisNames.SDCASE_RELATION.RELATIONS_TITLE.TableName = "SDCASE_RELATION"; 
UsrCfg.InternoAtisNames.SDCASE_RELATION.RELATIONS_TITLE.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.SDCASE_RELATION.RELATIONS_TITLE.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE_RELATION.RELATIONS_TITLE);
UsrCfg.InternoAtisNames.SDCASE_RELATION.NAME_TABLE = "SDCASE_RELATION"; 
UsrCfg.InternoAtisNames.SDCASE_RELATION.RELATIONS_DESCRIPTION.FieldName = "RELATIONS_DESCRIPTION"; 
UsrCfg.InternoAtisNames.SDCASE_RELATION.RELATIONS_DESCRIPTION.TableName = "SDCASE_RELATION"; 
UsrCfg.InternoAtisNames.SDCASE_RELATION.RELATIONS_DESCRIPTION.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.SDCASE_RELATION.RELATIONS_DESCRIPTION.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE_RELATION.RELATIONS_DESCRIPTION);
UsrCfg.InternoAtisNames.SDCASE_RELATION.NAME_TABLE = "SDCASE_RELATION"; 
UsrCfg.InternoAtisNames.SDCASE_RELATION.IDSDCASE_RELATIONTYPE.FieldName = "IDSDCASE_RELATIONTYPE"; 
UsrCfg.InternoAtisNames.SDCASE_RELATION.IDSDCASE_RELATIONTYPE.TableName = "SDCASE_RELATION"; 
UsrCfg.InternoAtisNames.SDCASE_RELATION.IDSDCASE_RELATIONTYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASE_RELATION.IDSDCASE_RELATIONTYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE_RELATION.IDSDCASE_RELATIONTYPE);
UsrCfg.InternoAtisNames.SDCASE_RELATION.NAME_TABLE = "SDCASE_RELATION"; 
UsrCfg.InternoAtisNames.SDCASE_RELATION.IDCMDBCI.FieldName = "IDCMDBCI"; 
UsrCfg.InternoAtisNames.SDCASE_RELATION.IDCMDBCI.TableName = "SDCASE_RELATION"; 
UsrCfg.InternoAtisNames.SDCASE_RELATION.IDCMDBCI.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASE_RELATION.IDCMDBCI.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE_RELATION.IDCMDBCI);
UsrCfg.InternoAtisNames.SDCASE_RELATION.NAME_TABLE = "SDCASE_RELATION"; 
UsrCfg.InternoAtisNames.SDCASE_RELATION.IDSDTYPEUSER.FieldName = "IDSDTYPEUSER"; 
UsrCfg.InternoAtisNames.SDCASE_RELATION.IDSDTYPEUSER.TableName = "SDCASE_RELATION"; 
UsrCfg.InternoAtisNames.SDCASE_RELATION.IDSDTYPEUSER.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASE_RELATION.IDSDTYPEUSER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE_RELATION.IDSDTYPEUSER);
UsrCfg.InternoAtisNames.SDCASE_RELATION.NAME_TABLE = "SDCASE_RELATION"; 
UsrCfg.InternoAtisNames.SDCASE_RELATION.IDSYSTEMSTATUS.FieldName = "IDSYSTEMSTATUS"; 
UsrCfg.InternoAtisNames.SDCASE_RELATION.IDSYSTEMSTATUS.TableName = "SDCASE_RELATION"; 
UsrCfg.InternoAtisNames.SDCASE_RELATION.IDSYSTEMSTATUS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASE_RELATION.IDSYSTEMSTATUS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE_RELATION.IDSYSTEMSTATUS);
UsrCfg.InternoAtisNames.SDCASE_RELATIONTYPE.NAME_TABLE = "SDCASE_RELATIONTYPE"; 
UsrCfg.InternoAtisNames.SDCASE_RELATIONTYPE.IDSDCASE_RELATIONTYPE.FieldName = "IDSDCASE_RELATIONTYPE"; 
UsrCfg.InternoAtisNames.SDCASE_RELATIONTYPE.IDSDCASE_RELATIONTYPE.TableName = "SDCASE_RELATIONTYPE"; 
UsrCfg.InternoAtisNames.SDCASE_RELATIONTYPE.IDSDCASE_RELATIONTYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASE_RELATIONTYPE.IDSDCASE_RELATIONTYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE_RELATIONTYPE.IDSDCASE_RELATIONTYPE);
UsrCfg.InternoAtisNames.SDCASE_RELATIONTYPE.NAME_TABLE = "SDCASE_RELATIONTYPE"; 
UsrCfg.InternoAtisNames.SDCASE_RELATIONTYPE.RELATIONSTYPENAME.FieldName = "RELATIONSTYPENAME"; 
UsrCfg.InternoAtisNames.SDCASE_RELATIONTYPE.RELATIONSTYPENAME.TableName = "SDCASE_RELATIONTYPE"; 
UsrCfg.InternoAtisNames.SDCASE_RELATIONTYPE.RELATIONSTYPENAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.SDCASE_RELATIONTYPE.RELATIONSTYPENAME.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE_RELATIONTYPE.RELATIONSTYPENAME);
UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.NAME_TABLE = "SDCASE_ATTACHMENT"; 
UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.IDSDCASE_ATTACHMENT.FieldName = "IDSDCASE_ATTACHMENT"; 
UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.IDSDCASE_ATTACHMENT.TableName = "SDCASE_ATTACHMENT"; 
UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.IDSDCASE_ATTACHMENT.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.IDSDCASE_ATTACHMENT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.IDSDCASE_ATTACHMENT);
UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.NAME_TABLE = "SDCASE_ATTACHMENT"; 
UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.IDSDCASE.FieldName = "IDSDCASE"; 
UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.IDSDCASE.TableName = "SDCASE_ATTACHMENT"; 
UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.IDSDCASE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.IDSDCASE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.IDSDCASE);
UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.NAME_TABLE = "SDCASE_ATTACHMENT"; 
UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.IDCMDBCI.FieldName = "IDCMDBCI"; 
UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.IDCMDBCI.TableName = "SDCASE_ATTACHMENT"; 
UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.IDCMDBCI.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.IDCMDBCI.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.IDCMDBCI);
UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.NAME_TABLE = "SDCASE_ATTACHMENT"; 
UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.IDSDTYPEUSER.FieldName = "IDSDTYPEUSER"; 
UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.IDSDTYPEUSER.TableName = "SDCASE_ATTACHMENT"; 
UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.IDSDTYPEUSER.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.IDSDTYPEUSER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.IDSDTYPEUSER);
UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.NAME_TABLE = "SDCASE_ATTACHMENT"; 
UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.IDSYSTEMSTATUS.FieldName = "IDSYSTEMSTATUS"; 
UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.IDSYSTEMSTATUS.TableName = "SDCASE_ATTACHMENT"; 
UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.IDSYSTEMSTATUS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.IDSYSTEMSTATUS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.IDSYSTEMSTATUS);
UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.NAME_TABLE = "SDCASE_ATTACHMENT"; 
UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.ATTACH.FieldName = "ATTACH"; 
UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.ATTACH.TableName = "SDCASE_ATTACHMENT"; 
UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.ATTACH.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.ATTACH.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.ATTACH);
UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.NAME_TABLE = "SDCASE_ATTACHMENT"; 
UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.PERMISSIONS.FieldName = "PERMISSIONS"; 
UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.PERMISSIONS.TableName = "SDCASE_ATTACHMENT"; 
UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.PERMISSIONS.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.PERMISSIONS.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASE_ATTACHMENT.PERMISSIONS);
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.NAME_TABLE = "SDCASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDSDCASEACTIVITIES.FieldName = "IDSDCASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDSDCASEACTIVITIES.TableName = "SDCASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDSDCASEACTIVITIES.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDSDCASEACTIVITIES.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDSDCASEACTIVITIES);
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.NAME_TABLE = "SDCASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDSDCASEMT_ATVPARENT.FieldName = "IDSDCASEMT_ATVPARENT"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDSDCASEMT_ATVPARENT.TableName = "SDCASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDSDCASEMT_ATVPARENT.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDSDCASEMT_ATVPARENT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDSDCASEMT_ATVPARENT);
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.NAME_TABLE = "SDCASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDSDCASE_ATVPARENT.FieldName = "IDSDCASE_ATVPARENT"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDSDCASE_ATVPARENT.TableName = "SDCASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDSDCASE_ATVPARENT.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDSDCASE_ATVPARENT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDSDCASE_ATVPARENT);
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.NAME_TABLE = "SDCASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDSDCASE.FieldName = "IDSDCASE"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDSDCASE.TableName = "SDCASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDSDCASE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDSDCASE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDSDCASE);
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.NAME_TABLE = "SDCASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.LS_NAMESTEP.FieldName = "LS_NAMESTEP"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.LS_NAMESTEP.TableName = "SDCASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.LS_NAMESTEP.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.LS_NAMESTEP.Size = 255; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEACTIVITIES.LS_NAMESTEP);
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.NAME_TABLE = "SDCASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.LS_STATUSN.FieldName = "LS_STATUSN"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.LS_STATUSN.TableName = "SDCASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.LS_STATUSN.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.LS_STATUSN.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEACTIVITIES.LS_STATUSN);
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.NAME_TABLE = "SDCASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.MT_RETURNS.FieldName = "MT_RETURNS"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.MT_RETURNS.TableName = "SDCASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.MT_RETURNS.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.MT_RETURNS.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEACTIVITIES.MT_RETURNS);
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.NAME_TABLE = "SDCASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDMDMODELTYPED.FieldName = "IDMDMODELTYPED"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDMDMODELTYPED.TableName = "SDCASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDMDMODELTYPED.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDMDMODELTYPED.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDMDMODELTYPED);
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.NAME_TABLE = "SDCASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDSDRUNNIGSTATUS.FieldName = "IDSDRUNNIGSTATUS"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDSDRUNNIGSTATUS.TableName = "SDCASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDSDRUNNIGSTATUS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDSDRUNNIGSTATUS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDSDRUNNIGSTATUS);
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.NAME_TABLE = "SDCASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.GUIDET.FieldName = "GUIDET"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.GUIDET.TableName = "SDCASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.GUIDET.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.GUIDET.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEACTIVITIES.GUIDET);
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.NAME_TABLE = "SDCASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.COMMENTSL.FieldName = "COMMENTSL"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.COMMENTSL.TableName = "SDCASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.COMMENTSL.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.COMMENTSL.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEACTIVITIES.COMMENTSL);
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.NAME_TABLE = "SDCASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDSDRUNNIGSOURCEMODEL.FieldName = "IDSDRUNNIGSOURCEMODEL"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDSDRUNNIGSOURCEMODEL.TableName = "SDCASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDSDRUNNIGSOURCEMODEL.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDSDRUNNIGSOURCEMODEL.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDSDRUNNIGSOURCEMODEL);
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.NAME_TABLE = "SDCASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.ACTIVITIES_DATECREATE.FieldName = "ACTIVITIES_DATECREATE"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.ACTIVITIES_DATECREATE.TableName = "SDCASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.ACTIVITIES_DATECREATE.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.ACTIVITIES_DATECREATE.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEACTIVITIES.ACTIVITIES_DATECREATE);
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.NAME_TABLE = "SDCASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.ACTIVITIES_DATERUNNING.FieldName = "ACTIVITIES_DATERUNNING"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.ACTIVITIES_DATERUNNING.TableName = "SDCASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.ACTIVITIES_DATERUNNING.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.ACTIVITIES_DATERUNNING.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEACTIVITIES.ACTIVITIES_DATERUNNING);
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.NAME_TABLE = "SDCASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.ACTIVITIES_DATECLOSED.FieldName = "ACTIVITIES_DATECLOSED"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.ACTIVITIES_DATECLOSED.TableName = "SDCASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.ACTIVITIES_DATECLOSED.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.ACTIVITIES_DATECLOSED.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEACTIVITIES.ACTIVITIES_DATECLOSED);
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.NAME_TABLE = "SDCASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.ACTIVITIES_DATECANCELLED.FieldName = "ACTIVITIES_DATECANCELLED"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.ACTIVITIES_DATECANCELLED.TableName = "SDCASEACTIVITIES"; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.ACTIVITIES_DATECANCELLED.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.SDCASEACTIVITIES.ACTIVITIES_DATECANCELLED.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEACTIVITIES.ACTIVITIES_DATECANCELLED);
UsrCfg.InternoAtisNames.SDRUNNIGSOURCEMODEL.NAME_TABLE = "SDRUNNIGSOURCEMODEL"; 
UsrCfg.InternoAtisNames.SDRUNNIGSOURCEMODEL.IDSDRUNNIGSOURCEMODEL.FieldName = "IDSDRUNNIGSOURCEMODEL"; 
UsrCfg.InternoAtisNames.SDRUNNIGSOURCEMODEL.IDSDRUNNIGSOURCEMODEL.TableName = "SDRUNNIGSOURCEMODEL"; 
UsrCfg.InternoAtisNames.SDRUNNIGSOURCEMODEL.IDSDRUNNIGSOURCEMODEL.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDRUNNIGSOURCEMODEL.IDSDRUNNIGSOURCEMODEL.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDRUNNIGSOURCEMODEL.IDSDRUNNIGSOURCEMODEL);
UsrCfg.InternoAtisNames.SDRUNNIGSOURCEMODEL.NAME_TABLE = "SDRUNNIGSOURCEMODEL"; 
UsrCfg.InternoAtisNames.SDRUNNIGSOURCEMODEL.SOURCEMODELNAME.FieldName = "SOURCEMODELNAME"; 
UsrCfg.InternoAtisNames.SDRUNNIGSOURCEMODEL.SOURCEMODELNAME.TableName = "SDRUNNIGSOURCEMODEL"; 
UsrCfg.InternoAtisNames.SDRUNNIGSOURCEMODEL.SOURCEMODELNAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.SDRUNNIGSOURCEMODEL.SOURCEMODELNAME.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDRUNNIGSOURCEMODEL.SOURCEMODELNAME);
UsrCfg.InternoAtisNames.SDRUNNIGSTATUS.NAME_TABLE = "SDRUNNIGSTATUS"; 
UsrCfg.InternoAtisNames.SDRUNNIGSTATUS.IDSDRUNNIGSTATUS.FieldName = "IDSDRUNNIGSTATUS"; 
UsrCfg.InternoAtisNames.SDRUNNIGSTATUS.IDSDRUNNIGSTATUS.TableName = "SDRUNNIGSTATUS"; 
UsrCfg.InternoAtisNames.SDRUNNIGSTATUS.IDSDRUNNIGSTATUS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDRUNNIGSTATUS.IDSDRUNNIGSTATUS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDRUNNIGSTATUS.IDSDRUNNIGSTATUS);
UsrCfg.InternoAtisNames.SDRUNNIGSTATUS.NAME_TABLE = "SDRUNNIGSTATUS"; 
UsrCfg.InternoAtisNames.SDRUNNIGSTATUS.ACTIVITIESNAME.FieldName = "ACTIVITIESNAME"; 
UsrCfg.InternoAtisNames.SDRUNNIGSTATUS.ACTIVITIESNAME.TableName = "SDRUNNIGSTATUS"; 
UsrCfg.InternoAtisNames.SDRUNNIGSTATUS.ACTIVITIESNAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.SDRUNNIGSTATUS.ACTIVITIESNAME.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDRUNNIGSTATUS.ACTIVITIESNAME);
UsrCfg.InternoAtisNames.SDCASEPERMISSION.NAME_TABLE = "SDCASEPERMISSION"; 
UsrCfg.InternoAtisNames.SDCASEPERMISSION.IDSDCASEPERMISSION.FieldName = "IDSDCASEPERMISSION"; 
UsrCfg.InternoAtisNames.SDCASEPERMISSION.IDSDCASEPERMISSION.TableName = "SDCASEPERMISSION"; 
UsrCfg.InternoAtisNames.SDCASEPERMISSION.IDSDCASEPERMISSION.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDCASEPERMISSION.IDSDCASEPERMISSION.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEPERMISSION.IDSDCASEPERMISSION);
UsrCfg.InternoAtisNames.SDCASEPERMISSION.NAME_TABLE = "SDCASEPERMISSION"; 
UsrCfg.InternoAtisNames.SDCASEPERMISSION.PERMISSIONNAME.FieldName = "PERMISSIONNAME"; 
UsrCfg.InternoAtisNames.SDCASEPERMISSION.PERMISSIONNAME.TableName = "SDCASEPERMISSION"; 
UsrCfg.InternoAtisNames.SDCASEPERMISSION.PERMISSIONNAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.SDCASEPERMISSION.PERMISSIONNAME.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEPERMISSION.PERMISSIONNAME);
UsrCfg.InternoAtisNames.SDCASEPERMISSION.NAME_TABLE = "SDCASEPERMISSION"; 
UsrCfg.InternoAtisNames.SDCASEPERMISSION.PERMISSIONDESCRIPTION.FieldName = "PERMISSIONDESCRIPTION"; 
UsrCfg.InternoAtisNames.SDCASEPERMISSION.PERMISSIONDESCRIPTION.TableName = "SDCASEPERMISSION"; 
UsrCfg.InternoAtisNames.SDCASEPERMISSION.PERMISSIONDESCRIPTION.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.SDCASEPERMISSION.PERMISSIONDESCRIPTION.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDCASEPERMISSION.PERMISSIONDESCRIPTION);
UsrCfg.InternoAtisNames.CMDBCONTACTTYPE.NAME_TABLE = "CMDBCONTACTTYPE"; 
UsrCfg.InternoAtisNames.CMDBCONTACTTYPE.IDCMDBCONTACTTYPE.FieldName = "IDCMDBCONTACTTYPE"; 
UsrCfg.InternoAtisNames.CMDBCONTACTTYPE.IDCMDBCONTACTTYPE.TableName = "CMDBCONTACTTYPE"; 
UsrCfg.InternoAtisNames.CMDBCONTACTTYPE.IDCMDBCONTACTTYPE.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.CMDBCONTACTTYPE.IDCMDBCONTACTTYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBCONTACTTYPE.IDCMDBCONTACTTYPE);
UsrCfg.InternoAtisNames.CMDBCONTACTTYPE.NAME_TABLE = "CMDBCONTACTTYPE"; 
UsrCfg.InternoAtisNames.CMDBCONTACTTYPE.TYPENAME.FieldName = "TYPENAME"; 
UsrCfg.InternoAtisNames.CMDBCONTACTTYPE.TYPENAME.TableName = "CMDBCONTACTTYPE"; 
UsrCfg.InternoAtisNames.CMDBCONTACTTYPE.TYPENAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.CMDBCONTACTTYPE.TYPENAME.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBCONTACTTYPE.TYPENAME);
UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.NAME_TABLE = "CMDBUSERCONTACTTYPE"; 
UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBUSERCONTACTTYPE.FieldName = "IDCMDBUSERCONTACTTYPE"; 
UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBUSERCONTACTTYPE.TableName = "CMDBUSERCONTACTTYPE"; 
UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBUSERCONTACTTYPE.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBUSERCONTACTTYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBUSERCONTACTTYPE);
UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.NAME_TABLE = "CMDBUSERCONTACTTYPE"; 
UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBUSER.FieldName = "IDCMDBUSER"; 
UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBUSER.TableName = "CMDBUSERCONTACTTYPE"; 
UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBUSER.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBUSER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBUSER);
UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.NAME_TABLE = "CMDBUSERCONTACTTYPE"; 
UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBCONTACTTYPE.FieldName = "IDCMDBCONTACTTYPE"; 
UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBCONTACTTYPE.TableName = "CMDBUSERCONTACTTYPE"; 
UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBCONTACTTYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBCONTACTTYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBCONTACTTYPE);
UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.NAME_TABLE = "CMDBUSERCONTACTTYPE"; 
UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDSYSTEMSTATUS.FieldName = "IDSYSTEMSTATUS"; 
UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDSYSTEMSTATUS.TableName = "CMDBUSERCONTACTTYPE"; 
UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDSYSTEMSTATUS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDSYSTEMSTATUS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDSYSTEMSTATUS);
UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.NAME_TABLE = "CMDBUSERCONTACTTYPE"; 
UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.CONTACTDEFINE.FieldName = "CONTACTDEFINE"; 
UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.CONTACTDEFINE.TableName = "CMDBUSERCONTACTTYPE"; 
UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.CONTACTDEFINE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.CONTACTDEFINE.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.CONTACTDEFINE);
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.NAME_TABLE = "CMDBUSERADDRESS"; 
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.IDCMDBUSERADDRESS.FieldName = "IDCMDBUSERADDRESS"; 
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.IDCMDBUSERADDRESS.TableName = "CMDBUSERADDRESS"; 
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.IDCMDBUSERADDRESS.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.IDCMDBUSERADDRESS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.IDCMDBUSERADDRESS);
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.NAME_TABLE = "CMDBUSERADDRESS"; 
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.IDCMDBUSER.FieldName = "IDCMDBUSER"; 
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.IDCMDBUSER.TableName = "CMDBUSERADDRESS"; 
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.IDCMDBUSER.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.IDCMDBUSER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.IDCMDBUSER);
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.NAME_TABLE = "CMDBUSERADDRESS"; 
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS1.FieldName = "ADDRESS1"; 
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS1.TableName = "CMDBUSERADDRESS"; 
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS1.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS1.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS1);
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.NAME_TABLE = "CMDBUSERADDRESS"; 
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS2.FieldName = "ADDRESS2"; 
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS2.TableName = "CMDBUSERADDRESS"; 
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS2.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS2.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS2);
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.NAME_TABLE = "CMDBUSERADDRESS"; 
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS3.FieldName = "ADDRESS3"; 
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS3.TableName = "CMDBUSERADDRESS"; 
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS3.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS3.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS3);
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.NAME_TABLE = "CMDBUSERADDRESS"; 
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS4.FieldName = "ADDRESS4"; 
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS4.TableName = "CMDBUSERADDRESS"; 
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS4.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS4.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS4);
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.NAME_TABLE = "CMDBUSERADDRESS"; 
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS5.FieldName = "ADDRESS5"; 
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS5.TableName = "CMDBUSERADDRESS"; 
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS5.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS5.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS5);
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.NAME_TABLE = "CMDBUSERADDRESS"; 
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS6.FieldName = "ADDRESS6"; 
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS6.TableName = "CMDBUSERADDRESS"; 
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS6.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS6.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.ADDRESS6);
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.NAME_TABLE = "CMDBUSERADDRESS"; 
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.IDSYSTEMSTATUS.FieldName = "IDSYSTEMSTATUS"; 
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.IDSYSTEMSTATUS.TableName = "CMDBUSERADDRESS"; 
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.IDSYSTEMSTATUS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.CMDBUSERADDRESS.IDSYSTEMSTATUS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBUSERADDRESS.IDSYSTEMSTATUS);
UsrCfg.InternoAtisNames.CMDBCOUNTRY.NAME_TABLE = "CMDBCOUNTRY"; 
UsrCfg.InternoAtisNames.CMDBCOUNTRY.IDCMDBCOUNTRY.FieldName = "IDCMDBCOUNTRY"; 
UsrCfg.InternoAtisNames.CMDBCOUNTRY.IDCMDBCOUNTRY.TableName = "CMDBCOUNTRY"; 
UsrCfg.InternoAtisNames.CMDBCOUNTRY.IDCMDBCOUNTRY.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.CMDBCOUNTRY.IDCMDBCOUNTRY.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBCOUNTRY.IDCMDBCOUNTRY);
UsrCfg.InternoAtisNames.CMDBCOUNTRY.NAME_TABLE = "CMDBCOUNTRY"; 
UsrCfg.InternoAtisNames.CMDBCOUNTRY.COUNTRY_NAME.FieldName = "COUNTRY_NAME"; 
UsrCfg.InternoAtisNames.CMDBCOUNTRY.COUNTRY_NAME.TableName = "CMDBCOUNTRY"; 
UsrCfg.InternoAtisNames.CMDBCOUNTRY.COUNTRY_NAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.CMDBCOUNTRY.COUNTRY_NAME.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBCOUNTRY.COUNTRY_NAME);
UsrCfg.InternoAtisNames.CMDBCOUNTRY.NAME_TABLE = "CMDBCOUNTRY"; 
UsrCfg.InternoAtisNames.CMDBCOUNTRY.ZONES.FieldName = "ZONES"; 
UsrCfg.InternoAtisNames.CMDBCOUNTRY.ZONES.TableName = "CMDBCOUNTRY"; 
UsrCfg.InternoAtisNames.CMDBCOUNTRY.ZONES.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.CMDBCOUNTRY.ZONES.Size = 10; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBCOUNTRY.ZONES);
UsrCfg.InternoAtisNames.CMDBCOUNTRY.NAME_TABLE = "CMDBCOUNTRY"; 
UsrCfg.InternoAtisNames.CMDBCOUNTRY.DST.FieldName = "DST"; 
UsrCfg.InternoAtisNames.CMDBCOUNTRY.DST.TableName = "CMDBCOUNTRY"; 
UsrCfg.InternoAtisNames.CMDBCOUNTRY.DST.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.CMDBCOUNTRY.DST.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBCOUNTRY.DST);
UsrCfg.InternoAtisNames.CMDBCOUNTRY.NAME_TABLE = "CMDBCOUNTRY"; 
UsrCfg.InternoAtisNames.CMDBCOUNTRY.DST_START_DATE.FieldName = "DST_START_DATE"; 
UsrCfg.InternoAtisNames.CMDBCOUNTRY.DST_START_DATE.TableName = "CMDBCOUNTRY"; 
UsrCfg.InternoAtisNames.CMDBCOUNTRY.DST_START_DATE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.CMDBCOUNTRY.DST_START_DATE.Size = 15; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBCOUNTRY.DST_START_DATE);
UsrCfg.InternoAtisNames.CMDBCOUNTRY.NAME_TABLE = "CMDBCOUNTRY"; 
UsrCfg.InternoAtisNames.CMDBCOUNTRY.DST_END_DATE.FieldName = "DST_END_DATE"; 
UsrCfg.InternoAtisNames.CMDBCOUNTRY.DST_END_DATE.TableName = "CMDBCOUNTRY"; 
UsrCfg.InternoAtisNames.CMDBCOUNTRY.DST_END_DATE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.CMDBCOUNTRY.DST_END_DATE.Size = 15; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBCOUNTRY.DST_END_DATE);
UsrCfg.InternoAtisNames.CMDBCOUNTRY.NAME_TABLE = "CMDBCOUNTRY"; 
UsrCfg.InternoAtisNames.CMDBCOUNTRY.ALPHA2.FieldName = "ALPHA2"; 
UsrCfg.InternoAtisNames.CMDBCOUNTRY.ALPHA2.TableName = "CMDBCOUNTRY"; 
UsrCfg.InternoAtisNames.CMDBCOUNTRY.ALPHA2.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.CMDBCOUNTRY.ALPHA2.Size = 2; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBCOUNTRY.ALPHA2);
UsrCfg.InternoAtisNames.CMDBCOUNTRY.NAME_TABLE = "CMDBCOUNTRY"; 
UsrCfg.InternoAtisNames.CMDBCOUNTRY.ALPHA3.FieldName = "ALPHA3"; 
UsrCfg.InternoAtisNames.CMDBCOUNTRY.ALPHA3.TableName = "CMDBCOUNTRY"; 
UsrCfg.InternoAtisNames.CMDBCOUNTRY.ALPHA3.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.CMDBCOUNTRY.ALPHA3.Size = 3; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBCOUNTRY.ALPHA3);
UsrCfg.InternoAtisNames.CMDBCOUNTRY.NAME_TABLE = "CMDBCOUNTRY"; 
UsrCfg.InternoAtisNames.CMDBCOUNTRY.CC_TLD.FieldName = "CC_TLD"; 
UsrCfg.InternoAtisNames.CMDBCOUNTRY.CC_TLD.TableName = "CMDBCOUNTRY"; 
UsrCfg.InternoAtisNames.CMDBCOUNTRY.CC_TLD.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.CMDBCOUNTRY.CC_TLD.Size = 5; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBCOUNTRY.CC_TLD);
UsrCfg.InternoAtisNames.MDSERVICETYPE.NAME_TABLE = "MDSERVICETYPE"; 
UsrCfg.InternoAtisNames.MDSERVICETYPE.IDMDSERVICETYPE.FieldName = "IDMDSERVICETYPE"; 
UsrCfg.InternoAtisNames.MDSERVICETYPE.IDMDSERVICETYPE.TableName = "MDSERVICETYPE"; 
UsrCfg.InternoAtisNames.MDSERVICETYPE.IDMDSERVICETYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDSERVICETYPE.IDMDSERVICETYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDSERVICETYPE.IDMDSERVICETYPE);
UsrCfg.InternoAtisNames.MDSERVICETYPE.NAME_TABLE = "MDSERVICETYPE"; 
UsrCfg.InternoAtisNames.MDSERVICETYPE.SERVICETYPENAME.FieldName = "SERVICETYPENAME"; 
UsrCfg.InternoAtisNames.MDSERVICETYPE.SERVICETYPENAME.TableName = "MDSERVICETYPE"; 
UsrCfg.InternoAtisNames.MDSERVICETYPE.SERVICETYPENAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.MDSERVICETYPE.SERVICETYPENAME.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDSERVICETYPE.SERVICETYPENAME);
UsrCfg.InternoAtisNames.MDSERVICETYPE.NAME_TABLE = "MDSERVICETYPE"; 
UsrCfg.InternoAtisNames.MDSERVICETYPE.SERVICETYPEDESCRIPTION.FieldName = "SERVICETYPEDESCRIPTION"; 
UsrCfg.InternoAtisNames.MDSERVICETYPE.SERVICETYPEDESCRIPTION.TableName = "MDSERVICETYPE"; 
UsrCfg.InternoAtisNames.MDSERVICETYPE.SERVICETYPEDESCRIPTION.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.MDSERVICETYPE.SERVICETYPEDESCRIPTION.Size = 255; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDSERVICETYPE.SERVICETYPEDESCRIPTION);
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.NAME_TABLE = "MDSERVICEEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE.FieldName = "IDMDSERVICEEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE.TableName = "MDSERVICEEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE);
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.NAME_TABLE = "MDSERVICEEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.IDMDSERVICETYPE.FieldName = "IDMDSERVICETYPE"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.IDMDSERVICETYPE.TableName = "MDSERVICEEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.IDMDSERVICETYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.IDMDSERVICETYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.IDMDSERVICETYPE);
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.NAME_TABLE = "MDSERVICEEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_NAMETABLE.FieldName = "EXTRATABLE_NAMETABLE"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_NAMETABLE.TableName = "MDSERVICEEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_NAMETABLE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_NAMETABLE.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_NAMETABLE);
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.NAME_TABLE = "MDSERVICEEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_IDSOURCE.FieldName = "EXTRATABLE_IDSOURCE"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_IDSOURCE.TableName = "MDSERVICEEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_IDSOURCE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_IDSOURCE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_IDSOURCE);
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.NAME_TABLE = "MDSERVICEEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_LOG.FieldName = "EXTRATABLE_LOG"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_LOG.TableName = "MDSERVICEEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_LOG.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_LOG.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_LOG);
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.NAME_TABLE = "MDSERVICEEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_DESCRIPTION.FieldName = "EXTRATABLE_DESCRIPTION"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_DESCRIPTION.TableName = "MDSERVICEEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_DESCRIPTION.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_DESCRIPTION.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_DESCRIPTION);
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.NAME_TABLE = "MDSERVICEEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_NAME.FieldName = "EXTRATABLE_NAME"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_NAME.TableName = "MDSERVICEEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_NAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_NAME.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_NAME);
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.NAME_TABLE = "MDSERVICEEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_ENABLED.FieldName = "EXTRATABLE_ENABLED"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_ENABLED.TableName = "MDSERVICEEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_ENABLED.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_ENABLED.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_ENABLED);
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.NAME_TABLE = "MDSERVICEEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_ORDER.FieldName = "EXTRATABLE_ORDER"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_ORDER.TableName = "MDSERVICEEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_ORDER.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_ORDER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_ORDER);
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.NAME_TABLE = "MDSERVICEEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS.FieldName = "IDMDSERVICEEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS.TableName = "MDSERVICEEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS);
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.NAME_TABLE = "MDSERVICEEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICETYPE.FieldName = "IDMDSERVICETYPE"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICETYPE.TableName = "MDSERVICEEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICETYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICETYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICETYPE);
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.NAME_TABLE = "MDSERVICEEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDCMDBKEYTYPE.FieldName = "IDCMDBKEYTYPE"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDCMDBKEYTYPE.TableName = "MDSERVICEEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDCMDBKEYTYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDCMDBKEYTYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDCMDBKEYTYPE);
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.NAME_TABLE = "MDSERVICEEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.DBKEYTYPE_NAME.FieldName = "DBKEYTYPE_NAME"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.DBKEYTYPE_NAME.TableName = "MDSERVICEEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.DBKEYTYPE_NAME.DataType = SysCfg.DB.Properties.TDataType.Unknown; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.DBKEYTYPE_NAME.Size = 0; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.DBKEYTYPE_NAME);
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.NAME_TABLE = "MDSERVICEEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES.FieldName = "IDCMDBDBDATATYPES"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES.TableName = "MDSERVICEEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES);
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.NAME_TABLE = "MDSERVICEEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.DBDATATYPES_SIZE.FieldName = "DBDATATYPES_SIZE"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.DBDATATYPES_SIZE.TableName = "MDSERVICEEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.DBDATATYPES_SIZE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.DBDATATYPES_SIZE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.DBDATATYPES_SIZE);
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.NAME_TABLE = "MDSERVICEEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME.FieldName = "EXTRAFIELDS_NAME"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME.TableName = "MDSERVICEEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME);
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.NAME_TABLE = "MDSERVICEEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_DESCRIPTION.FieldName = "EXTRAFIELDS_DESCRIPTION"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_DESCRIPTION.TableName = "MDSERVICEEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_DESCRIPTION.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_DESCRIPTION.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_DESCRIPTION);
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.NAME_TABLE = "MDSERVICEEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRATABLE.FieldName = "IDMDSERVICEEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRATABLE.TableName = "MDSERVICEEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRATABLE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRATABLE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRATABLE);
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.NAME_TABLE = "MDSERVICEEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE.FieldName = "EXTRAFIELDS_COLUMNSTYLE"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE.TableName = "MDSERVICEEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE);
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.NAME_TABLE = "MDSERVICEEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPSQL.FieldName = "EXTRAFIELDS_LOOKUPSQL"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPSQL.TableName = "MDSERVICEEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPSQL.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPSQL.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPSQL);
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.NAME_TABLE = "MDSERVICEEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPID.FieldName = "EXTRAFIELDS_LOOKUPID"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPID.TableName = "MDSERVICEEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPID.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPID.Size = 150; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPID);
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.NAME_TABLE = "MDSERVICEEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPDISPLAY.FieldName = "EXTRAFIELDS_LOOKUPDISPLAY"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPDISPLAY.TableName = "MDSERVICEEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPDISPLAY.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPDISPLAY.Size = 150; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPDISPLAY);
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.NAME_TABLE = "MDSERVICEEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_ORDER.FieldName = "EXTRAFIELDS_ORDER"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_ORDER.TableName = "MDSERVICEEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_ORDER.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_ORDER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_ORDER);
UsrCfg.InternoAtisNames.MDGROUP.NAME_TABLE = "MDGROUP"; 
UsrCfg.InternoAtisNames.MDGROUP.IDMDGROUP.FieldName = "IDMDGROUP"; 
UsrCfg.InternoAtisNames.MDGROUP.IDMDGROUP.TableName = "MDGROUP"; 
UsrCfg.InternoAtisNames.MDGROUP.IDMDGROUP.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.MDGROUP.IDMDGROUP.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDGROUP.IDMDGROUP);
UsrCfg.InternoAtisNames.MDGROUP.NAME_TABLE = "MDGROUP"; 
UsrCfg.InternoAtisNames.MDGROUP.IDSYSTEMSTATUS.FieldName = "IDSYSTEMSTATUS"; 
UsrCfg.InternoAtisNames.MDGROUP.IDSYSTEMSTATUS.TableName = "MDGROUP"; 
UsrCfg.InternoAtisNames.MDGROUP.IDSYSTEMSTATUS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDGROUP.IDSYSTEMSTATUS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDGROUP.IDSYSTEMSTATUS);
UsrCfg.InternoAtisNames.MDGROUP.NAME_TABLE = "MDGROUP"; 
UsrCfg.InternoAtisNames.MDGROUP.NAMEGROUP.FieldName = "NAMEGROUP"; 
UsrCfg.InternoAtisNames.MDGROUP.NAMEGROUP.TableName = "MDGROUP"; 
UsrCfg.InternoAtisNames.MDGROUP.NAMEGROUP.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.MDGROUP.NAMEGROUP.Size = 255; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDGROUP.NAMEGROUP);
UsrCfg.InternoAtisNames.MDGROUP.NAME_TABLE = "MDGROUP"; 
UsrCfg.InternoAtisNames.MDGROUP.COMMENTSG.FieldName = "COMMENTSG"; 
UsrCfg.InternoAtisNames.MDGROUP.COMMENTSG.TableName = "MDGROUP"; 
UsrCfg.InternoAtisNames.MDGROUP.COMMENTSG.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.MDGROUP.COMMENTSG.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDGROUP.COMMENTSG);
UsrCfg.InternoAtisNames.MDGROUPUSER.NAME_TABLE = "MDGROUPUSER"; 
UsrCfg.InternoAtisNames.MDGROUPUSER.IDMDGROUPUSER.FieldName = "IDMDGROUPUSER"; 
UsrCfg.InternoAtisNames.MDGROUPUSER.IDMDGROUPUSER.TableName = "MDGROUPUSER"; 
UsrCfg.InternoAtisNames.MDGROUPUSER.IDMDGROUPUSER.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.MDGROUPUSER.IDMDGROUPUSER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDGROUPUSER.IDMDGROUPUSER);
UsrCfg.InternoAtisNames.MDGROUPUSER.NAME_TABLE = "MDGROUPUSER"; 
UsrCfg.InternoAtisNames.MDGROUPUSER.IDCMDBCI.FieldName = "IDCMDBCI"; 
UsrCfg.InternoAtisNames.MDGROUPUSER.IDCMDBCI.TableName = "MDGROUPUSER"; 
UsrCfg.InternoAtisNames.MDGROUPUSER.IDCMDBCI.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDGROUPUSER.IDCMDBCI.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDGROUPUSER.IDCMDBCI);
UsrCfg.InternoAtisNames.MDGROUPUSER.NAME_TABLE = "MDGROUPUSER"; 
UsrCfg.InternoAtisNames.MDGROUPUSER.IDMDGROUP.FieldName = "IDMDGROUP"; 
UsrCfg.InternoAtisNames.MDGROUPUSER.IDMDGROUP.TableName = "MDGROUPUSER"; 
UsrCfg.InternoAtisNames.MDGROUPUSER.IDMDGROUP.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDGROUPUSER.IDMDGROUP.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDGROUPUSER.IDMDGROUP);
UsrCfg.InternoAtisNames.MDGROUPUSER.NAME_TABLE = "MDGROUPUSER"; 
UsrCfg.InternoAtisNames.MDGROUPUSER.IDSYSTEMSTATUS.FieldName = "IDSYSTEMSTATUS"; 
UsrCfg.InternoAtisNames.MDGROUPUSER.IDSYSTEMSTATUS.TableName = "MDGROUPUSER"; 
UsrCfg.InternoAtisNames.MDGROUPUSER.IDSYSTEMSTATUS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDGROUPUSER.IDSYSTEMSTATUS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDGROUPUSER.IDSYSTEMSTATUS);
UsrCfg.InternoAtisNames.MDFUNCESC.NAME_TABLE = "MDFUNCESC"; 
UsrCfg.InternoAtisNames.MDFUNCESC.IDMDFUNCESC.FieldName = "IDMDFUNCESC"; 
UsrCfg.InternoAtisNames.MDFUNCESC.IDMDFUNCESC.TableName = "MDFUNCESC"; 
UsrCfg.InternoAtisNames.MDFUNCESC.IDMDFUNCESC.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.MDFUNCESC.IDMDFUNCESC.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDFUNCESC.IDMDFUNCESC);
UsrCfg.InternoAtisNames.MDFUNCESC.NAME_TABLE = "MDFUNCESC"; 
UsrCfg.InternoAtisNames.MDFUNCESC.TITLEFUNCESC.FieldName = "TITLEFUNCESC"; 
UsrCfg.InternoAtisNames.MDFUNCESC.TITLEFUNCESC.TableName = "MDFUNCESC"; 
UsrCfg.InternoAtisNames.MDFUNCESC.TITLEFUNCESC.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.MDFUNCESC.TITLEFUNCESC.Size = 255; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDFUNCESC.TITLEFUNCESC);
UsrCfg.InternoAtisNames.MDFUNCESC.NAME_TABLE = "MDFUNCESC"; 
UsrCfg.InternoAtisNames.MDFUNCESC.COMMENTSFE.FieldName = "COMMENTSFE"; 
UsrCfg.InternoAtisNames.MDFUNCESC.COMMENTSFE.TableName = "MDFUNCESC"; 
UsrCfg.InternoAtisNames.MDFUNCESC.COMMENTSFE.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.MDFUNCESC.COMMENTSFE.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDFUNCESC.COMMENTSFE);
UsrCfg.InternoAtisNames.MDFUNCPER.NAME_TABLE = "MDFUNCPER"; 
UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCPER.FieldName = "IDMDFUNCPER"; 
UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCPER.TableName = "MDFUNCPER"; 
UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCPER.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCPER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCPER);
UsrCfg.InternoAtisNames.MDFUNCPER.NAME_TABLE = "MDFUNCPER"; 
UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCESC.FieldName = "IDMDFUNCESC"; 
UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCESC.TableName = "MDFUNCPER"; 
UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCESC.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCESC.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDFUNCESC);
UsrCfg.InternoAtisNames.MDFUNCPER.NAME_TABLE = "MDFUNCPER"; 
UsrCfg.InternoAtisNames.MDFUNCPER.PERCF.FieldName = "PERCF"; 
UsrCfg.InternoAtisNames.MDFUNCPER.PERCF.TableName = "MDFUNCPER"; 
UsrCfg.InternoAtisNames.MDFUNCPER.PERCF.DataType = SysCfg.DB.Properties.TDataType.Double; 
UsrCfg.InternoAtisNames.MDFUNCPER.PERCF.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDFUNCPER.PERCF);
UsrCfg.InternoAtisNames.MDFUNCPER.NAME_TABLE = "MDFUNCPER"; 
UsrCfg.InternoAtisNames.MDFUNCPER.TOTAL.FieldName = "TOTAL"; 
UsrCfg.InternoAtisNames.MDFUNCPER.TOTAL.TableName = "MDFUNCPER"; 
UsrCfg.InternoAtisNames.MDFUNCPER.TOTAL.DataType = SysCfg.DB.Properties.TDataType.Double; 
UsrCfg.InternoAtisNames.MDFUNCPER.TOTAL.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDFUNCPER.TOTAL);
UsrCfg.InternoAtisNames.MDFUNCPER.NAME_TABLE = "MDFUNCPER"; 
UsrCfg.InternoAtisNames.MDFUNCPER.IDMDGROUP.FieldName = "IDMDGROUP"; 
UsrCfg.InternoAtisNames.MDFUNCPER.IDMDGROUP.TableName = "MDFUNCPER"; 
UsrCfg.InternoAtisNames.MDFUNCPER.IDMDGROUP.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDFUNCPER.IDMDGROUP.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDFUNCPER.IDMDGROUP);
UsrCfg.InternoAtisNames.MDFUNCPER.NAME_TABLE = "MDFUNCPER"; 
UsrCfg.InternoAtisNames.MDFUNCPER.COMMENTSF.FieldName = "COMMENTSF"; 
UsrCfg.InternoAtisNames.MDFUNCPER.COMMENTSF.TableName = "MDFUNCPER"; 
UsrCfg.InternoAtisNames.MDFUNCPER.COMMENTSF.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.MDFUNCPER.COMMENTSF.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDFUNCPER.COMMENTSF);
UsrCfg.InternoAtisNames.MDFUNCPER.NAME_TABLE = "MDFUNCPER"; 
UsrCfg.InternoAtisNames.MDFUNCPER.FUNLAVEL.FieldName = "FUNLAVEL"; 
UsrCfg.InternoAtisNames.MDFUNCPER.FUNLAVEL.TableName = "MDFUNCPER"; 
UsrCfg.InternoAtisNames.MDFUNCPER.FUNLAVEL.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDFUNCPER.FUNLAVEL.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDFUNCPER.FUNLAVEL);
UsrCfg.InternoAtisNames.MDHIERESC.NAME_TABLE = "MDHIERESC"; 
UsrCfg.InternoAtisNames.MDHIERESC.IDMDHIERESC.FieldName = "IDMDHIERESC"; 
UsrCfg.InternoAtisNames.MDHIERESC.IDMDHIERESC.TableName = "MDHIERESC"; 
UsrCfg.InternoAtisNames.MDHIERESC.IDMDHIERESC.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.MDHIERESC.IDMDHIERESC.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDHIERESC.IDMDHIERESC);
UsrCfg.InternoAtisNames.MDHIERESC.NAME_TABLE = "MDHIERESC"; 
UsrCfg.InternoAtisNames.MDHIERESC.TITLEHIERESC.FieldName = "TITLEHIERESC"; 
UsrCfg.InternoAtisNames.MDHIERESC.TITLEHIERESC.TableName = "MDHIERESC"; 
UsrCfg.InternoAtisNames.MDHIERESC.TITLEHIERESC.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.MDHIERESC.TITLEHIERESC.Size = 255; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDHIERESC.TITLEHIERESC);
UsrCfg.InternoAtisNames.MDHIERESC.NAME_TABLE = "MDHIERESC"; 
UsrCfg.InternoAtisNames.MDHIERESC.COMMENTSJE.FieldName = "COMMENTSJE"; 
UsrCfg.InternoAtisNames.MDHIERESC.COMMENTSJE.TableName = "MDHIERESC"; 
UsrCfg.InternoAtisNames.MDHIERESC.COMMENTSJE.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.MDHIERESC.COMMENTSJE.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDHIERESC.COMMENTSJE);
UsrCfg.InternoAtisNames.MDHIERPER.NAME_TABLE = "MDHIERPER"; 
UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERPER.FieldName = "IDMDHIERPER"; 
UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERPER.TableName = "MDHIERPER"; 
UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERPER.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERPER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERPER);
UsrCfg.InternoAtisNames.MDHIERPER.NAME_TABLE = "MDHIERPER"; 
UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERESC.FieldName = "IDMDHIERESC"; 
UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERESC.TableName = "MDHIERPER"; 
UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERESC.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERESC.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDHIERPER.IDMDHIERESC);
UsrCfg.InternoAtisNames.MDHIERPER.NAME_TABLE = "MDHIERPER"; 
UsrCfg.InternoAtisNames.MDHIERPER.PERCH.FieldName = "PERCH"; 
UsrCfg.InternoAtisNames.MDHIERPER.PERCH.TableName = "MDHIERPER"; 
UsrCfg.InternoAtisNames.MDHIERPER.PERCH.DataType = SysCfg.DB.Properties.TDataType.Double; 
UsrCfg.InternoAtisNames.MDHIERPER.PERCH.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDHIERPER.PERCH);
UsrCfg.InternoAtisNames.MDHIERPER.NAME_TABLE = "MDHIERPER"; 
UsrCfg.InternoAtisNames.MDHIERPER.TOTAL.FieldName = "TOTAL"; 
UsrCfg.InternoAtisNames.MDHIERPER.TOTAL.TableName = "MDHIERPER"; 
UsrCfg.InternoAtisNames.MDHIERPER.TOTAL.DataType = SysCfg.DB.Properties.TDataType.Double; 
UsrCfg.InternoAtisNames.MDHIERPER.TOTAL.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDHIERPER.TOTAL);
UsrCfg.InternoAtisNames.MDHIERPER.NAME_TABLE = "MDHIERPER"; 
UsrCfg.InternoAtisNames.MDHIERPER.IDMDGROUP.FieldName = "IDMDGROUP"; 
UsrCfg.InternoAtisNames.MDHIERPER.IDMDGROUP.TableName = "MDHIERPER"; 
UsrCfg.InternoAtisNames.MDHIERPER.IDMDGROUP.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDHIERPER.IDMDGROUP.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDHIERPER.IDMDGROUP);
UsrCfg.InternoAtisNames.MDHIERPER.NAME_TABLE = "MDHIERPER"; 
UsrCfg.InternoAtisNames.MDHIERPER.PERMISSIONH.FieldName = "PERMISSIONH"; 
UsrCfg.InternoAtisNames.MDHIERPER.PERMISSIONH.TableName = "MDHIERPER"; 
UsrCfg.InternoAtisNames.MDHIERPER.PERMISSIONH.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDHIERPER.PERMISSIONH.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDHIERPER.PERMISSIONH);
UsrCfg.InternoAtisNames.MDHIERPER.NAME_TABLE = "MDHIERPER"; 
UsrCfg.InternoAtisNames.MDHIERPER.COMMENTSH.FieldName = "COMMENTSH"; 
UsrCfg.InternoAtisNames.MDHIERPER.COMMENTSH.TableName = "MDHIERPER"; 
UsrCfg.InternoAtisNames.MDHIERPER.COMMENTSH.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.MDHIERPER.COMMENTSH.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDHIERPER.COMMENTSH);
UsrCfg.InternoAtisNames.MDHIERPER.NAME_TABLE = "MDHIERPER"; 
UsrCfg.InternoAtisNames.MDHIERPER.HIERLAVEL.FieldName = "HIERLAVEL"; 
UsrCfg.InternoAtisNames.MDHIERPER.HIERLAVEL.TableName = "MDHIERPER"; 
UsrCfg.InternoAtisNames.MDHIERPER.HIERLAVEL.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDHIERPER.HIERLAVEL.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDHIERPER.HIERLAVEL);
UsrCfg.InternoAtisNames.MDLIFESTATUS.NAME_TABLE = "MDLIFESTATUS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUS.IDMDLIFESTATUS.FieldName = "IDMDLIFESTATUS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUS.IDMDLIFESTATUS.TableName = "MDLIFESTATUS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUS.IDMDLIFESTATUS.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.MDLIFESTATUS.IDMDLIFESTATUS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUS.IDMDLIFESTATUS);
UsrCfg.InternoAtisNames.MDLIFESTATUS.NAME_TABLE = "MDLIFESTATUS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUS.IDMDMODELTYPED.FieldName = "IDMDMODELTYPED"; 
UsrCfg.InternoAtisNames.MDLIFESTATUS.IDMDMODELTYPED.TableName = "MDLIFESTATUS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUS.IDMDMODELTYPED.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDLIFESTATUS.IDMDMODELTYPED.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUS.IDMDMODELTYPED);
UsrCfg.InternoAtisNames.MDLIFESTATUS.NAME_TABLE = "MDLIFESTATUS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUS.STATUSN.FieldName = "STATUSN"; 
UsrCfg.InternoAtisNames.MDLIFESTATUS.STATUSN.TableName = "MDLIFESTATUS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUS.STATUSN.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDLIFESTATUS.STATUSN.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUS.STATUSN);
UsrCfg.InternoAtisNames.MDLIFESTATUS.NAME_TABLE = "MDLIFESTATUS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUS.NAMESTEP.FieldName = "NAMESTEP"; 
UsrCfg.InternoAtisNames.MDLIFESTATUS.NAMESTEP.TableName = "MDLIFESTATUS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUS.NAMESTEP.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.MDLIFESTATUS.NAMESTEP.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUS.NAMESTEP);
UsrCfg.InternoAtisNames.MDLIFESTATUS.NAME_TABLE = "MDLIFESTATUS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUS.NEXTSTEP.FieldName = "NEXTSTEP"; 
UsrCfg.InternoAtisNames.MDLIFESTATUS.NEXTSTEP.TableName = "MDLIFESTATUS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUS.NEXTSTEP.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.MDLIFESTATUS.NEXTSTEP.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUS.NEXTSTEP);
UsrCfg.InternoAtisNames.MDLIFESTATUS.NAME_TABLE = "MDLIFESTATUS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUS.ENDSTEP.FieldName = "ENDSTEP"; 
UsrCfg.InternoAtisNames.MDLIFESTATUS.ENDSTEP.TableName = "MDLIFESTATUS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUS.ENDSTEP.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.MDLIFESTATUS.ENDSTEP.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUS.ENDSTEP);
UsrCfg.InternoAtisNames.MDLIFESTATUS.NAME_TABLE = "MDLIFESTATUS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUS.COMMENTSST.FieldName = "COMMENTSST"; 
UsrCfg.InternoAtisNames.MDLIFESTATUS.COMMENTSST.TableName = "MDLIFESTATUS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUS.COMMENTSST.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.MDLIFESTATUS.COMMENTSST.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUS.COMMENTSST);
UsrCfg.InternoAtisNames.MDLIFESTATUS.NAME_TABLE = "MDLIFESTATUS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUS.CAUTIONSTEP.FieldName = "CAUTIONSTEP"; 
UsrCfg.InternoAtisNames.MDLIFESTATUS.CAUTIONSTEP.TableName = "MDLIFESTATUS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUS.CAUTIONSTEP.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.MDLIFESTATUS.CAUTIONSTEP.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUS.CAUTIONSTEP);
UsrCfg.InternoAtisNames.MDLIFESTATUS.NAME_TABLE = "MDLIFESTATUS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUS.WARNINGSTEP.FieldName = "WARNINGSTEP"; 
UsrCfg.InternoAtisNames.MDLIFESTATUS.WARNINGSTEP.TableName = "MDLIFESTATUS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUS.WARNINGSTEP.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.MDLIFESTATUS.WARNINGSTEP.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUS.WARNINGSTEP);
UsrCfg.InternoAtisNames.MDLIFESTATUS.NAME_TABLE = "MDLIFESTATUS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUS.LS_IDSDCASESTATUS.FieldName = "LS_IDSDCASESTATUS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUS.LS_IDSDCASESTATUS.TableName = "MDLIFESTATUS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUS.LS_IDSDCASESTATUS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDLIFESTATUS.LS_IDSDCASESTATUS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUS.LS_IDSDCASESTATUS);
UsrCfg.InternoAtisNames.MDLIFESTATUSPERMISSION.NAME_TABLE = "MDLIFESTATUSPERMISSION"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSPERMISSION.IDMDLIFESTATUSPERMISSION.FieldName = "IDMDLIFESTATUSPERMISSION"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSPERMISSION.IDMDLIFESTATUSPERMISSION.TableName = "MDLIFESTATUSPERMISSION"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSPERMISSION.IDMDLIFESTATUSPERMISSION.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDLIFESTATUSPERMISSION.IDMDLIFESTATUSPERMISSION.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSPERMISSION.IDMDLIFESTATUSPERMISSION);
UsrCfg.InternoAtisNames.MDLIFESTATUSPERMISSION.NAME_TABLE = "MDLIFESTATUSPERMISSION"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSPERMISSION.LIFESTATUSPERMISSION_NAME.FieldName = "LIFESTATUSPERMISSION_NAME"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSPERMISSION.LIFESTATUSPERMISSION_NAME.TableName = "MDLIFESTATUSPERMISSION"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSPERMISSION.LIFESTATUSPERMISSION_NAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.MDLIFESTATUSPERMISSION.LIFESTATUSPERMISSION_NAME.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSPERMISSION.LIFESTATUSPERMISSION_NAME);
UsrCfg.InternoAtisNames.MDINTERFACE.NAME_TABLE = "MDINTERFACE"; 
UsrCfg.InternoAtisNames.MDINTERFACE.IDMDINTERFACE.FieldName = "IDMDINTERFACE"; 
UsrCfg.InternoAtisNames.MDINTERFACE.IDMDINTERFACE.TableName = "MDINTERFACE"; 
UsrCfg.InternoAtisNames.MDINTERFACE.IDMDINTERFACE.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.MDINTERFACE.IDMDINTERFACE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDINTERFACE.IDMDINTERFACE);
UsrCfg.InternoAtisNames.MDINTERFACE.NAME_TABLE = "MDINTERFACE"; 
UsrCfg.InternoAtisNames.MDINTERFACE.INTERFACE_NAME.FieldName = "INTERFACE_NAME"; 
UsrCfg.InternoAtisNames.MDINTERFACE.INTERFACE_NAME.TableName = "MDINTERFACE"; 
UsrCfg.InternoAtisNames.MDINTERFACE.INTERFACE_NAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.MDINTERFACE.INTERFACE_NAME.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDINTERFACE.INTERFACE_NAME);
UsrCfg.InternoAtisNames.MDINTERFACE.NAME_TABLE = "MDINTERFACE"; 
UsrCfg.InternoAtisNames.MDINTERFACE.IDMDINTERFACETYPE.FieldName = "IDMDINTERFACETYPE"; 
UsrCfg.InternoAtisNames.MDINTERFACE.IDMDINTERFACETYPE.TableName = "MDINTERFACE"; 
UsrCfg.InternoAtisNames.MDINTERFACE.IDMDINTERFACETYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDINTERFACE.IDMDINTERFACETYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDINTERFACE.IDMDINTERFACETYPE);
UsrCfg.InternoAtisNames.MDINTERFACE.NAME_TABLE = "MDINTERFACE"; 
UsrCfg.InternoAtisNames.MDINTERFACE.CUSTOMCODE.FieldName = "CUSTOMCODE"; 
UsrCfg.InternoAtisNames.MDINTERFACE.CUSTOMCODE.TableName = "MDINTERFACE"; 
UsrCfg.InternoAtisNames.MDINTERFACE.CUSTOMCODE.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.MDINTERFACE.CUSTOMCODE.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDINTERFACE.CUSTOMCODE);
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.NAME_TABLE = "MDLIFESTATUSTYPEUSER"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.IDMDLIFESTATUSTYPEUSER.FieldName = "IDMDLIFESTATUSTYPEUSER"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.IDMDLIFESTATUSTYPEUSER.TableName = "MDLIFESTATUSTYPEUSER"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.IDMDLIFESTATUSTYPEUSER.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.IDMDLIFESTATUSTYPEUSER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.IDMDLIFESTATUSTYPEUSER);
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.NAME_TABLE = "MDLIFESTATUSTYPEUSER"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.IDMDLIFESTATUS.FieldName = "IDMDLIFESTATUS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.IDMDLIFESTATUS.TableName = "MDLIFESTATUSTYPEUSER"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.IDMDLIFESTATUS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.IDMDLIFESTATUS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.IDMDLIFESTATUS);
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.NAME_TABLE = "MDLIFESTATUSTYPEUSER"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.IDSDTYPEUSER.FieldName = "IDSDTYPEUSER"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.IDSDTYPEUSER.TableName = "MDLIFESTATUSTYPEUSER"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.IDSDTYPEUSER.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.IDSDTYPEUSER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.IDSDTYPEUSER);
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.NAME_TABLE = "MDLIFESTATUSTYPEUSER"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.IDMDLIFESTATUSPERMISSION.FieldName = "IDMDLIFESTATUSPERMISSION"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.IDMDLIFESTATUSPERMISSION.TableName = "MDLIFESTATUSTYPEUSER"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.IDMDLIFESTATUSPERMISSION.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.IDMDLIFESTATUSPERMISSION.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.IDMDLIFESTATUSPERMISSION);
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.NAME_TABLE = "MDLIFESTATUSTYPEUSER"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.IDMDINTERFACE.FieldName = "IDMDINTERFACE"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.IDMDINTERFACE.TableName = "MDLIFESTATUSTYPEUSER"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.IDMDINTERFACE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.IDMDINTERFACE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.IDMDINTERFACE);
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.NAME_TABLE = "MDLIFESTATUSTYPEUSER"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.IDMDLIFESTATUSBEHAVIOR.FieldName = "IDMDLIFESTATUSBEHAVIOR"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.IDMDLIFESTATUSBEHAVIOR.TableName = "MDLIFESTATUSTYPEUSER"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.IDMDLIFESTATUSBEHAVIOR.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.IDMDLIFESTATUSBEHAVIOR.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.IDMDLIFESTATUSBEHAVIOR);
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.NAME_TABLE = "MDLIFESTATUSTYPEUSER"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.LSEND_COMMENTSENABLE.FieldName = "LSEND_COMMENTSENABLE"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.LSEND_COMMENTSENABLE.TableName = "MDLIFESTATUSTYPEUSER"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.LSEND_COMMENTSENABLE.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.LSEND_COMMENTSENABLE.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.LSEND_COMMENTSENABLE);
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.NAME_TABLE = "MDLIFESTATUSTYPEUSER"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.LSBEGIN_COMMENTSENABLE.FieldName = "LSBEGIN_COMMENTSENABLE"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.LSBEGIN_COMMENTSENABLE.TableName = "MDLIFESTATUSTYPEUSER"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.LSBEGIN_COMMENTSENABLE.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.LSBEGIN_COMMENTSENABLE.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.LSBEGIN_COMMENTSENABLE);
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.NAME_TABLE = "MDLIFESTATUSTYPEUSER"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.LSCHANGE_COMMENTS.FieldName = "LSCHANGE_COMMENTS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.LSCHANGE_COMMENTS.TableName = "MDLIFESTATUSTYPEUSER"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.LSCHANGE_COMMENTS.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.LSCHANGE_COMMENTS.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.LSCHANGE_COMMENTS);
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.NAME_TABLE = "MDLIFESTATUSTYPEUSER"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.LSEND_COMMENTSLABEL.FieldName = "LSEND_COMMENTSLABEL"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.LSEND_COMMENTSLABEL.TableName = "MDLIFESTATUSTYPEUSER"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.LSEND_COMMENTSLABEL.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.LSEND_COMMENTSLABEL.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.LSEND_COMMENTSLABEL);
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.NAME_TABLE = "MDLIFESTATUSTYPEUSER"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.LSBEGIN_COMMENTSLABEL.FieldName = "LSBEGIN_COMMENTSLABEL"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.LSBEGIN_COMMENTSLABEL.TableName = "MDLIFESTATUSTYPEUSER"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.LSBEGIN_COMMENTSLABEL.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.LSBEGIN_COMMENTSLABEL.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.LSBEGIN_COMMENTSLABEL);
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.NAME_TABLE = "MDLIFESTATUSTYPEUSER"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.LSHERESTEP_CONFIG.FieldName = "LSHERESTEP_CONFIG"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.LSHERESTEP_CONFIG.TableName = "MDLIFESTATUSTYPEUSER"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.LSHERESTEP_CONFIG.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.LSHERESTEP_CONFIG.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.LSHERESTEP_CONFIG);
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.NAME_TABLE = "MDLIFESTATUSTYPEUSER"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.LSNEXTSTEP_CONFIG.FieldName = "LSNEXTSTEP_CONFIG"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.LSNEXTSTEP_CONFIG.TableName = "MDLIFESTATUSTYPEUSER"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.LSNEXTSTEP_CONFIG.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.LSNEXTSTEP_CONFIG.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.LSNEXTSTEP_CONFIG);
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.NAME_TABLE = "MDLIFESTATUSTYPEUSER"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.ENABLETIME.FieldName = "ENABLETIME"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.ENABLETIME.TableName = "MDLIFESTATUSTYPEUSER"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.ENABLETIME.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.ENABLETIME.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.ENABLETIME);
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.NAME_TABLE = "MDLIFESTATUSTYPEUSER"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.INCLUDEALLGROUP.FieldName = "INCLUDEALLGROUP"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.INCLUDEALLGROUP.TableName = "MDLIFESTATUSTYPEUSER"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.INCLUDEALLGROUP.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.INCLUDEALLGROUP.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.INCLUDEALLGROUP);
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.NAME_TABLE = "MDLIFESTATUSTYPEUSER"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.IDSDNOTIFYTEMPLATE_CONSOLE.FieldName = "IDSDNOTIFYTEMPLATE_CONSOLE"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.IDSDNOTIFYTEMPLATE_CONSOLE.TableName = "MDLIFESTATUSTYPEUSER"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.IDSDNOTIFYTEMPLATE_CONSOLE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.IDSDNOTIFYTEMPLATE_CONSOLE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.IDSDNOTIFYTEMPLATE_CONSOLE);
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.NAME_TABLE = "MDLIFESTATUSTYPEUSER"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.IDSDNOTIFYTEMPLATE_MAIL.FieldName = "IDSDNOTIFYTEMPLATE_MAIL"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.IDSDNOTIFYTEMPLATE_MAIL.TableName = "MDLIFESTATUSTYPEUSER"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.IDSDNOTIFYTEMPLATE_MAIL.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.IDSDNOTIFYTEMPLATE_MAIL.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSTYPEUSER.IDSDNOTIFYTEMPLATE_MAIL);
UsrCfg.InternoAtisNames.MDLIFESTATUSBEHAVIOR.NAME_TABLE = "MDLIFESTATUSBEHAVIOR"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSBEHAVIOR.IDMDLIFESTATUSBEHAVIOR.FieldName = "IDMDLIFESTATUSBEHAVIOR"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSBEHAVIOR.IDMDLIFESTATUSBEHAVIOR.TableName = "MDLIFESTATUSBEHAVIOR"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSBEHAVIOR.IDMDLIFESTATUSBEHAVIOR.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDLIFESTATUSBEHAVIOR.IDMDLIFESTATUSBEHAVIOR.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSBEHAVIOR.IDMDLIFESTATUSBEHAVIOR);
UsrCfg.InternoAtisNames.MDLIFESTATUSBEHAVIOR.NAME_TABLE = "MDLIFESTATUSBEHAVIOR"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSBEHAVIOR.LIFESTATUSBEHAVIOR_NAME.FieldName = "LIFESTATUSBEHAVIOR_NAME"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSBEHAVIOR.LIFESTATUSBEHAVIOR_NAME.TableName = "MDLIFESTATUSBEHAVIOR"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSBEHAVIOR.LIFESTATUSBEHAVIOR_NAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.MDLIFESTATUSBEHAVIOR.LIFESTATUSBEHAVIOR_NAME.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSBEHAVIOR.LIFESTATUSBEHAVIOR_NAME);
UsrCfg.InternoAtisNames.MDINTERFACETYPE.NAME_TABLE = "MDINTERFACETYPE"; 
UsrCfg.InternoAtisNames.MDINTERFACETYPE.IDMDINTERFACETYPE.FieldName = "IDMDINTERFACETYPE"; 
UsrCfg.InternoAtisNames.MDINTERFACETYPE.IDMDINTERFACETYPE.TableName = "MDINTERFACETYPE"; 
UsrCfg.InternoAtisNames.MDINTERFACETYPE.IDMDINTERFACETYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDINTERFACETYPE.IDMDINTERFACETYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDINTERFACETYPE.IDMDINTERFACETYPE);
UsrCfg.InternoAtisNames.MDINTERFACETYPE.NAME_TABLE = "MDINTERFACETYPE"; 
UsrCfg.InternoAtisNames.MDINTERFACETYPE.INTERFACETYPE_NAME.FieldName = "INTERFACETYPE_NAME"; 
UsrCfg.InternoAtisNames.MDINTERFACETYPE.INTERFACETYPE_NAME.TableName = "MDINTERFACETYPE"; 
UsrCfg.InternoAtisNames.MDINTERFACETYPE.INTERFACETYPE_NAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.MDINTERFACETYPE.INTERFACETYPE_NAME.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDINTERFACETYPE.INTERFACETYPE_NAME);
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.NAME_TABLE = "MDLIFESTATUSSTEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.IDMDLIFESTATUSSTEXTRATABLE.FieldName = "IDMDLIFESTATUSSTEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.IDMDLIFESTATUSSTEXTRATABLE.TableName = "MDLIFESTATUSSTEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.IDMDLIFESTATUSSTEXTRATABLE.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.IDMDLIFESTATUSSTEXTRATABLE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.IDMDLIFESTATUSSTEXTRATABLE);
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.NAME_TABLE = "MDLIFESTATUSSTEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.IDMDLIFESTATUSTYPEUSER.FieldName = "IDMDLIFESTATUSTYPEUSER"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.IDMDLIFESTATUSTYPEUSER.TableName = "MDLIFESTATUSSTEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.IDMDLIFESTATUSTYPEUSER.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.IDMDLIFESTATUSTYPEUSER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.IDMDLIFESTATUSTYPEUSER);
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.NAME_TABLE = "MDLIFESTATUSSTEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.IDMDLIFESTATUS.FieldName = "IDMDLIFESTATUS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.IDMDLIFESTATUS.TableName = "MDLIFESTATUSSTEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.IDMDLIFESTATUS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.IDMDLIFESTATUS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.IDMDLIFESTATUS);
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.NAME_TABLE = "MDLIFESTATUSSTEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.IDMDSERVICETYPE.FieldName = "IDMDSERVICETYPE"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.IDMDSERVICETYPE.TableName = "MDLIFESTATUSSTEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.IDMDSERVICETYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.IDMDSERVICETYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.IDMDSERVICETYPE);
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.NAME_TABLE = "MDLIFESTATUSSTEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.IDMDINTERFACE.FieldName = "IDMDINTERFACE"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.IDMDINTERFACE.TableName = "MDLIFESTATUSSTEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.IDMDINTERFACE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.IDMDINTERFACE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.IDMDINTERFACE);
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.NAME_TABLE = "MDLIFESTATUSSTEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.IDMDSERVICEEXTRATABLE.FieldName = "IDMDSERVICEEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.IDMDSERVICEEXTRATABLE.TableName = "MDLIFESTATUSSTEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.IDMDSERVICEEXTRATABLE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.IDMDSERVICEEXTRATABLE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.IDMDSERVICEEXTRATABLE);
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.NAME_TABLE = "MDLIFESTATUSSTEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.LSCHANGETABLE_COMMENTS.FieldName = "LSCHANGETABLE_COMMENTS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.LSCHANGETABLE_COMMENTS.TableName = "MDLIFESTATUSSTEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.LSCHANGETABLE_COMMENTS.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.LSCHANGETABLE_COMMENTS.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRATABLE.LSCHANGETABLE_COMMENTS);
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRAFIELDS.NAME_TABLE = "MDLIFESTATUSSTEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRAFIELDS.IDMDLIFESTATUSSTEXTRAFIELDS.FieldName = "IDMDLIFESTATUSSTEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRAFIELDS.IDMDLIFESTATUSSTEXTRAFIELDS.TableName = "MDLIFESTATUSSTEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRAFIELDS.IDMDLIFESTATUSSTEXTRAFIELDS.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRAFIELDS.IDMDLIFESTATUSSTEXTRAFIELDS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRAFIELDS.IDMDLIFESTATUSSTEXTRAFIELDS);
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRAFIELDS.NAME_TABLE = "MDLIFESTATUSSTEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRAFIELDS.IDMDLIFESTATUSSTEXTRATABLE.FieldName = "IDMDLIFESTATUSSTEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRAFIELDS.IDMDLIFESTATUSSTEXTRATABLE.TableName = "MDLIFESTATUSSTEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRAFIELDS.IDMDLIFESTATUSSTEXTRATABLE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRAFIELDS.IDMDLIFESTATUSSTEXTRATABLE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRAFIELDS.IDMDLIFESTATUSSTEXTRATABLE);
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRAFIELDS.NAME_TABLE = "MDLIFESTATUSSTEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS.FieldName = "IDMDSERVICEEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS.TableName = "MDLIFESTATUSSTEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS);
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRAFIELDS.NAME_TABLE = "MDLIFESTATUSSTEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRAFIELDS.IDMDLIFESTATUSPERMISSION.FieldName = "IDMDLIFESTATUSPERMISSION"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRAFIELDS.IDMDLIFESTATUSPERMISSION.TableName = "MDLIFESTATUSSTEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRAFIELDS.IDMDLIFESTATUSPERMISSION.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRAFIELDS.IDMDLIFESTATUSPERMISSION.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRAFIELDS.IDMDLIFESTATUSPERMISSION);
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRAFIELDS.NAME_TABLE = "MDLIFESTATUSSTEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRAFIELDS.MANDATORY.FieldName = "MANDATORY"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRAFIELDS.MANDATORY.TableName = "MDLIFESTATUSSTEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRAFIELDS.MANDATORY.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRAFIELDS.MANDATORY.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSSTEXTRAFIELDS.MANDATORY);
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRATABLE.NAME_TABLE = "MDLIFESTATUSCIEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRATABLE.IDMDLIFESTATUSCIEXTRATABLE.FieldName = "IDMDLIFESTATUSCIEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRATABLE.IDMDLIFESTATUSCIEXTRATABLE.TableName = "MDLIFESTATUSCIEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRATABLE.IDMDLIFESTATUSCIEXTRATABLE.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRATABLE.IDMDLIFESTATUSCIEXTRATABLE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRATABLE.IDMDLIFESTATUSCIEXTRATABLE);
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRATABLE.NAME_TABLE = "MDLIFESTATUSCIEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRATABLE.IDMDLIFESTATUSTYPEUSER.FieldName = "IDMDLIFESTATUSTYPEUSER"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRATABLE.IDMDLIFESTATUSTYPEUSER.TableName = "MDLIFESTATUSCIEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRATABLE.IDMDLIFESTATUSTYPEUSER.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRATABLE.IDMDLIFESTATUSTYPEUSER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRATABLE.IDMDLIFESTATUSTYPEUSER);
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRATABLE.NAME_TABLE = "MDLIFESTATUSCIEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRATABLE.IDMDLIFESTATUS.FieldName = "IDMDLIFESTATUS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRATABLE.IDMDLIFESTATUS.TableName = "MDLIFESTATUSCIEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRATABLE.IDMDLIFESTATUS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRATABLE.IDMDLIFESTATUS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRATABLE.IDMDLIFESTATUS);
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRATABLE.NAME_TABLE = "MDLIFESTATUSCIEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRATABLE.IDCMDBCIDEFINE.FieldName = "IDCMDBCIDEFINE"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRATABLE.IDCMDBCIDEFINE.TableName = "MDLIFESTATUSCIEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRATABLE.IDCMDBCIDEFINE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRATABLE.IDCMDBCIDEFINE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRATABLE.IDCMDBCIDEFINE);
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRATABLE.NAME_TABLE = "MDLIFESTATUSCIEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRATABLE.IDMDINTERFACE.FieldName = "IDMDINTERFACE"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRATABLE.IDMDINTERFACE.TableName = "MDLIFESTATUSCIEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRATABLE.IDMDINTERFACE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRATABLE.IDMDINTERFACE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRATABLE.IDMDINTERFACE);
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRATABLE.NAME_TABLE = "MDLIFESTATUSCIEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE.FieldName = "IDCMDBCIDEFINEEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE.TableName = "MDLIFESTATUSCIEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE);
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRAFIELDS.NAME_TABLE = "MDLIFESTATUSCIEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRAFIELDS.IDMDLIFESTATUSCIEXTRAFIELDS.FieldName = "IDMDLIFESTATUSCIEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRAFIELDS.IDMDLIFESTATUSCIEXTRAFIELDS.TableName = "MDLIFESTATUSCIEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRAFIELDS.IDMDLIFESTATUSCIEXTRAFIELDS.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRAFIELDS.IDMDLIFESTATUSCIEXTRAFIELDS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRAFIELDS.IDMDLIFESTATUSCIEXTRAFIELDS);
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRAFIELDS.NAME_TABLE = "MDLIFESTATUSCIEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRAFIELDS.IDMDLIFESTATUSCIEXTRATABLE.FieldName = "IDMDLIFESTATUSCIEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRAFIELDS.IDMDLIFESTATUSCIEXTRATABLE.TableName = "MDLIFESTATUSCIEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRAFIELDS.IDMDLIFESTATUSCIEXTRATABLE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRAFIELDS.IDMDLIFESTATUSCIEXTRATABLE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRAFIELDS.IDMDLIFESTATUSCIEXTRATABLE);
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRAFIELDS.NAME_TABLE = "MDLIFESTATUSCIEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS.FieldName = "IDCMDBCIDEFINEEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS.TableName = "MDLIFESTATUSCIEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS);
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRAFIELDS.NAME_TABLE = "MDLIFESTATUSCIEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRAFIELDS.IDMDLIFESTATUSPERMISSION.FieldName = "IDMDLIFESTATUSPERMISSION"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRAFIELDS.IDMDLIFESTATUSPERMISSION.TableName = "MDLIFESTATUSCIEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRAFIELDS.IDMDLIFESTATUSPERMISSION.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRAFIELDS.IDMDLIFESTATUSPERMISSION.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRAFIELDS.IDMDLIFESTATUSPERMISSION);
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRAFIELDS.NAME_TABLE = "MDLIFESTATUSCIEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRAFIELDS.MANDATORY.FieldName = "MANDATORY"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRAFIELDS.MANDATORY.TableName = "MDLIFESTATUSCIEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRAFIELDS.MANDATORY.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRAFIELDS.MANDATORY.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDLIFESTATUSCIEXTRAFIELDS.MANDATORY);
UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.NAME_TABLE = "MDCASESTEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDCASESTEXTRATABLE.FieldName = "IDMDCASESTEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDCASESTEXTRATABLE.TableName = "MDCASESTEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDCASESTEXTRATABLE.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDCASESTEXTRATABLE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDCASESTEXTRATABLE);
UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.NAME_TABLE = "MDCASESTEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDMODELTYPED.FieldName = "IDMDMODELTYPED"; 
UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDMODELTYPED.TableName = "MDCASESTEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDMODELTYPED.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDMODELTYPED.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDMODELTYPED);
UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.NAME_TABLE = "MDCASESTEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDSERVICETYPE.FieldName = "IDMDSERVICETYPE"; 
UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDSERVICETYPE.TableName = "MDCASESTEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDSERVICETYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDSERVICETYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDSERVICETYPE);
UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.NAME_TABLE = "MDCASESTEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDINTERFACE.FieldName = "IDMDINTERFACE"; 
UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDINTERFACE.TableName = "MDCASESTEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDINTERFACE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDINTERFACE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDINTERFACE);
UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.NAME_TABLE = "MDCASESTEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDSERVICEEXTRATABLE.FieldName = "IDMDSERVICEEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDSERVICEEXTRATABLE.TableName = "MDCASESTEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDSERVICEEXTRATABLE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDSERVICEEXTRATABLE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.IDMDSERVICEEXTRATABLE);
UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.NAME_TABLE = "MDCASESTEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.LSCHANGETABLE_COMMENTS.FieldName = "LSCHANGETABLE_COMMENTS"; 
UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.LSCHANGETABLE_COMMENTS.TableName = "MDCASESTEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.LSCHANGETABLE_COMMENTS.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.LSCHANGETABLE_COMMENTS.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCASESTEXTRATABLE.LSCHANGETABLE_COMMENTS);
UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.NAME_TABLE = "MDCASESTEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.IDMDCASESTEXTRAFIELDS.FieldName = "IDMDCASESTEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.IDMDCASESTEXTRAFIELDS.TableName = "MDCASESTEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.IDMDCASESTEXTRAFIELDS.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.IDMDCASESTEXTRAFIELDS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.IDMDCASESTEXTRAFIELDS);
UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.NAME_TABLE = "MDCASESTEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.IDMDCASESTEXTRATABLE.FieldName = "IDMDCASESTEXTRATABLE"; 
UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.IDMDCASESTEXTRATABLE.TableName = "MDCASESTEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.IDMDCASESTEXTRATABLE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.IDMDCASESTEXTRATABLE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.IDMDCASESTEXTRATABLE);
UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.NAME_TABLE = "MDCASESTEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS.FieldName = "IDMDSERVICEEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS.TableName = "MDCASESTEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS);
UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.NAME_TABLE = "MDCASESTEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.IDMDLIFESTATUSPERMISSION.FieldName = "IDMDLIFESTATUSPERMISSION"; 
UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.IDMDLIFESTATUSPERMISSION.TableName = "MDCASESTEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.IDMDLIFESTATUSPERMISSION.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.IDMDLIFESTATUSPERMISSION.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.IDMDLIFESTATUSPERMISSION);
UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.NAME_TABLE = "MDCASESTEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.MANDATORY.FieldName = "MANDATORY"; 
UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.MANDATORY.TableName = "MDCASESTEXTRAFIELDS"; 
UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.MANDATORY.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.MANDATORY.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCASESTEXTRAFIELDS.MANDATORY);
UsrCfg.InternoAtisNames.MDMODELTYPED.NAME_TABLE = "MDMODELTYPED"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDMODELTYPED.FieldName = "IDMDMODELTYPED"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDMODELTYPED.TableName = "MDMODELTYPED"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDMODELTYPED.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDMODELTYPED.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDMODELTYPED);
UsrCfg.InternoAtisNames.MDMODELTYPED.NAME_TABLE = "MDMODELTYPED"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.TITLEM.FieldName = "TITLEM"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.TITLEM.TableName = "MDMODELTYPED"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.TITLEM.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.MDMODELTYPED.TITLEM.Size = 255; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDMODELTYPED.TITLEM);
UsrCfg.InternoAtisNames.MDMODELTYPED.NAME_TABLE = "MDMODELTYPED"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.COMMENTSM.FieldName = "COMMENTSM"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.COMMENTSM.TableName = "MDMODELTYPED"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.COMMENTSM.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.MDMODELTYPED.COMMENTSM.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDMODELTYPED.COMMENTSM);
UsrCfg.InternoAtisNames.MDMODELTYPED.NAME_TABLE = "MDMODELTYPED"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDSERVICETYPE.FieldName = "IDMDSERVICETYPE"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDSERVICETYPE.TableName = "MDMODELTYPED"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDSERVICETYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDSERVICETYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDSERVICETYPE);
UsrCfg.InternoAtisNames.MDMODELTYPED.NAME_TABLE = "MDMODELTYPED"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDMODELTYPED1.FieldName = "IDMDMODELTYPED1"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDMODELTYPED1.TableName = "MDMODELTYPED"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDMODELTYPED1.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDMODELTYPED1.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDMODELTYPED1);
UsrCfg.InternoAtisNames.MDMODELTYPED.NAME_TABLE = "MDMODELTYPED"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.OPEARTIONM.FieldName = "OPEARTIONM"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.OPEARTIONM.TableName = "MDMODELTYPED"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.OPEARTIONM.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.MDMODELTYPED.OPEARTIONM.Size = 255; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDMODELTYPED.OPEARTIONM);
UsrCfg.InternoAtisNames.MDMODELTYPED.NAME_TABLE = "MDMODELTYPED"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDHIERESC.FieldName = "IDMDHIERESC"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDHIERESC.TableName = "MDMODELTYPED"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDHIERESC.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDHIERESC.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDHIERESC);
UsrCfg.InternoAtisNames.MDMODELTYPED.NAME_TABLE = "MDMODELTYPED"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDFUNCESC.FieldName = "IDMDFUNCESC"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDFUNCESC.TableName = "MDMODELTYPED"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDFUNCESC.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDFUNCESC.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDFUNCESC);
UsrCfg.InternoAtisNames.MDMODELTYPED.NAME_TABLE = "MDMODELTYPED"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.NORMALTIME.FieldName = "NORMALTIME"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.NORMALTIME.TableName = "MDMODELTYPED"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.NORMALTIME.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDMODELTYPED.NORMALTIME.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDMODELTYPED.NORMALTIME);
UsrCfg.InternoAtisNames.MDMODELTYPED.NAME_TABLE = "MDMODELTYPED"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.MAXTIME.FieldName = "MAXTIME"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.MAXTIME.TableName = "MDMODELTYPED"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.MAXTIME.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDMODELTYPED.MAXTIME.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDMODELTYPED.MAXTIME);
UsrCfg.InternoAtisNames.MDMODELTYPED.NAME_TABLE = "MDMODELTYPED"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.GUIDETEXT.FieldName = "GUIDETEXT"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.GUIDETEXT.TableName = "MDMODELTYPED"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.GUIDETEXT.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.MDMODELTYPED.GUIDETEXT.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDMODELTYPED.GUIDETEXT);
UsrCfg.InternoAtisNames.MDMODELTYPED.NAME_TABLE = "MDMODELTYPED"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.POSSIBLERETURNS.FieldName = "POSSIBLERETURNS"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.POSSIBLERETURNS.TableName = "MDMODELTYPED"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.POSSIBLERETURNS.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.MDMODELTYPED.POSSIBLERETURNS.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDMODELTYPED.POSSIBLERETURNS);
UsrCfg.InternoAtisNames.MDMODELTYPED.NAME_TABLE = "MDMODELTYPED"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.STEPVALIDATE.FieldName = "STEPVALIDATE"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.STEPVALIDATE.TableName = "MDMODELTYPED"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.STEPVALIDATE.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.MDMODELTYPED.STEPVALIDATE.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDMODELTYPED.STEPVALIDATE);
UsrCfg.InternoAtisNames.MDMODELTYPED.NAME_TABLE = "MDMODELTYPED"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.TIMEENABLE.FieldName = "TIMEENABLE"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.TIMEENABLE.TableName = "MDMODELTYPED"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.TIMEENABLE.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.MDMODELTYPED.TIMEENABLE.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDMODELTYPED.TIMEENABLE);
UsrCfg.InternoAtisNames.MDMODELTYPED.NAME_TABLE = "MDMODELTYPED"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.STEEPTYPEUSERENABLE.FieldName = "STEEPTYPEUSERENABLE"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.STEEPTYPEUSERENABLE.TableName = "MDMODELTYPED"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.STEEPTYPEUSERENABLE.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.MDMODELTYPED.STEEPTYPEUSERENABLE.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDMODELTYPED.STEEPTYPEUSERENABLE);
UsrCfg.InternoAtisNames.MDMODELTYPED.NAME_TABLE = "MDMODELTYPED"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.IDCMDBCIDEFINE.FieldName = "IDCMDBCIDEFINE"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.IDCMDBCIDEFINE.TableName = "MDMODELTYPED"; 
UsrCfg.InternoAtisNames.MDMODELTYPED.IDCMDBCIDEFINE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDMODELTYPED.IDCMDBCIDEFINE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDMODELTYPED.IDCMDBCIDEFINE);
UsrCfg.InternoAtisNames.MDMATRIXMODELS.NAME_TABLE = "MDMATRIXMODELS"; 
UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDMATRIXMODELS.FieldName = "IDMDMATRIXMODELS"; 
UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDMATRIXMODELS.TableName = "MDMATRIXMODELS"; 
UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDMATRIXMODELS.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDMATRIXMODELS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDMATRIXMODELS);
UsrCfg.InternoAtisNames.MDMATRIXMODELS.NAME_TABLE = "MDMATRIXMODELS"; 
UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDMODELTYPED1.FieldName = "IDMDMODELTYPED1"; 
UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDMODELTYPED1.TableName = "MDMATRIXMODELS"; 
UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDMODELTYPED1.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDMODELTYPED1.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDMODELTYPED1);
UsrCfg.InternoAtisNames.MDMATRIXMODELS.NAME_TABLE = "MDMATRIXMODELS"; 
UsrCfg.InternoAtisNames.MDMATRIXMODELS.POSINSTEP.FieldName = "POSINSTEP"; 
UsrCfg.InternoAtisNames.MDMATRIXMODELS.POSINSTEP.TableName = "MDMATRIXMODELS"; 
UsrCfg.InternoAtisNames.MDMATRIXMODELS.POSINSTEP.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDMATRIXMODELS.POSINSTEP.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDMATRIXMODELS.POSINSTEP);
UsrCfg.InternoAtisNames.MDMATRIXMODELS.NAME_TABLE = "MDMATRIXMODELS"; 
UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDMODELTYPED2.FieldName = "IDMDMODELTYPED2"; 
UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDMODELTYPED2.TableName = "MDMATRIXMODELS"; 
UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDMODELTYPED2.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDMODELTYPED2.Size = 255; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDMODELTYPED2);
UsrCfg.InternoAtisNames.MDMATRIXMODELS.NAME_TABLE = "MDMATRIXMODELS"; 
UsrCfg.InternoAtisNames.MDMATRIXMODELS.GUIDET.FieldName = "GUIDET"; 
UsrCfg.InternoAtisNames.MDMATRIXMODELS.GUIDET.TableName = "MDMATRIXMODELS"; 
UsrCfg.InternoAtisNames.MDMATRIXMODELS.GUIDET.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.MDMATRIXMODELS.GUIDET.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDMATRIXMODELS.GUIDET);
UsrCfg.InternoAtisNames.MDMATRIXMODELS.NAME_TABLE = "MDMATRIXMODELS"; 
UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDLIFESTATUS.FieldName = "IDMDLIFESTATUS"; 
UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDLIFESTATUS.TableName = "MDMATRIXMODELS"; 
UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDLIFESTATUS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDLIFESTATUS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDMATRIXMODELS.IDMDLIFESTATUS);
UsrCfg.InternoAtisNames.MDMATRIXMODELS.NAME_TABLE = "MDMATRIXMODELS"; 
UsrCfg.InternoAtisNames.MDMATRIXMODELS.COMMENTSL.FieldName = "COMMENTSL"; 
UsrCfg.InternoAtisNames.MDMATRIXMODELS.COMMENTSL.TableName = "MDMATRIXMODELS"; 
UsrCfg.InternoAtisNames.MDMATRIXMODELS.COMMENTSL.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.MDMATRIXMODELS.COMMENTSL.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDMATRIXMODELS.COMMENTSL);
UsrCfg.InternoAtisNames.MDMATRIXMODELS.NAME_TABLE = "MDMATRIXMODELS"; 
UsrCfg.InternoAtisNames.MDMATRIXMODELS.MODELVALIDATE.FieldName = "MODELVALIDATE"; 
UsrCfg.InternoAtisNames.MDMATRIXMODELS.MODELVALIDATE.TableName = "MDMATRIXMODELS"; 
UsrCfg.InternoAtisNames.MDMATRIXMODELS.MODELVALIDATE.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.MDMATRIXMODELS.MODELVALIDATE.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDMATRIXMODELS.MODELVALIDATE);
UsrCfg.InternoAtisNames.MDCATEGORY.NAME_TABLE = "MDCATEGORY"; 
UsrCfg.InternoAtisNames.MDCATEGORY.IDMDCATEGORY.FieldName = "IDMDCATEGORY"; 
UsrCfg.InternoAtisNames.MDCATEGORY.IDMDCATEGORY.TableName = "MDCATEGORY"; 
UsrCfg.InternoAtisNames.MDCATEGORY.IDMDCATEGORY.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.MDCATEGORY.IDMDCATEGORY.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCATEGORY.IDMDCATEGORY);
UsrCfg.InternoAtisNames.MDCATEGORY.NAME_TABLE = "MDCATEGORY"; 
UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY1.FieldName = "CATEGORY1"; 
UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY1.TableName = "MDCATEGORY"; 
UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY1.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY1.Size = 255; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY1);
UsrCfg.InternoAtisNames.MDCATEGORY.NAME_TABLE = "MDCATEGORY"; 
UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY2.FieldName = "CATEGORY2"; 
UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY2.TableName = "MDCATEGORY"; 
UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY2.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY2.Size = 255; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY2);
UsrCfg.InternoAtisNames.MDCATEGORY.NAME_TABLE = "MDCATEGORY"; 
UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY3.FieldName = "CATEGORY3"; 
UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY3.TableName = "MDCATEGORY"; 
UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY3.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY3.Size = 255; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY3);
UsrCfg.InternoAtisNames.MDCATEGORY.NAME_TABLE = "MDCATEGORY"; 
UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY4.FieldName = "CATEGORY4"; 
UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY4.TableName = "MDCATEGORY"; 
UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY4.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY4.Size = 255; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY4);
UsrCfg.InternoAtisNames.MDCATEGORY.NAME_TABLE = "MDCATEGORY"; 
UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY5.FieldName = "CATEGORY5"; 
UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY5.TableName = "MDCATEGORY"; 
UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY5.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY5.Size = 255; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY5);
UsrCfg.InternoAtisNames.MDCATEGORY.NAME_TABLE = "MDCATEGORY"; 
UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY6.FieldName = "CATEGORY6"; 
UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY6.TableName = "MDCATEGORY"; 
UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY6.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY6.Size = 255; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY6);
UsrCfg.InternoAtisNames.MDCATEGORY.NAME_TABLE = "MDCATEGORY"; 
UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY7.FieldName = "CATEGORY7"; 
UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY7.TableName = "MDCATEGORY"; 
UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY7.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY7.Size = 255; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY7);
UsrCfg.InternoAtisNames.MDCATEGORY.NAME_TABLE = "MDCATEGORY"; 
UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY8.FieldName = "CATEGORY8"; 
UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY8.TableName = "MDCATEGORY"; 
UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY8.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY8.Size = 255; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCATEGORY.CATEGORY8);
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.NAME_TABLE = "MDCATEGORYDETAIL"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.IDMDCATEGORYDETAIL.FieldName = "IDMDCATEGORYDETAIL"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.IDMDCATEGORYDETAIL.TableName = "MDCATEGORYDETAIL"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.IDMDCATEGORYDETAIL.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.IDMDCATEGORYDETAIL.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.IDMDCATEGORYDETAIL);
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.NAME_TABLE = "MDCATEGORYDETAIL"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.IDMDCATEGORY.FieldName = "IDMDCATEGORY"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.IDMDCATEGORY.TableName = "MDCATEGORYDETAIL"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.IDMDCATEGORY.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.IDMDCATEGORY.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.IDMDCATEGORY);
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.NAME_TABLE = "MDCATEGORYDETAIL"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CATEGORYNAME.FieldName = "CATEGORYNAME"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CATEGORYNAME.TableName = "MDCATEGORYDETAIL"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CATEGORYNAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CATEGORYNAME.Size = 255; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CATEGORYNAME);
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.NAME_TABLE = "MDCATEGORYDETAIL"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CATEGORYDESCRIPTION.FieldName = "CATEGORYDESCRIPTION"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CATEGORYDESCRIPTION.TableName = "MDCATEGORYDETAIL"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CATEGORYDESCRIPTION.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CATEGORYDESCRIPTION.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CATEGORYDESCRIPTION);
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.NAME_TABLE = "MDCATEGORYDETAIL"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CATEGORYSTATUS.FieldName = "CATEGORYSTATUS"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CATEGORYSTATUS.TableName = "MDCATEGORYDETAIL"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CATEGORYSTATUS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CATEGORYSTATUS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CATEGORYSTATUS);
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.NAME_TABLE = "MDCATEGORYDETAIL"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CUSTOMCODE.FieldName = "CUSTOMCODE"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CUSTOMCODE.TableName = "MDCATEGORYDETAIL"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CUSTOMCODE.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CUSTOMCODE.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CUSTOMCODE);
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.NAME_TABLE = "MDCATEGORYDETAIL_TYPEUSER"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.IDMDCATEGORYDETAIL_TYPEUSER.FieldName = "IDMDCATEGORYDETAIL_TYPEUSER"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.IDMDCATEGORYDETAIL_TYPEUSER.TableName = "MDCATEGORYDETAIL_TYPEUSER"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.IDMDCATEGORYDETAIL_TYPEUSER.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.IDMDCATEGORYDETAIL_TYPEUSER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.IDMDCATEGORYDETAIL_TYPEUSER);
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.NAME_TABLE = "MDCATEGORYDETAIL_TYPEUSER"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.IDSDTYPEUSER.FieldName = "IDSDTYPEUSER"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.IDSDTYPEUSER.TableName = "MDCATEGORYDETAIL_TYPEUSER"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.IDSDTYPEUSER.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.IDSDTYPEUSER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.IDSDTYPEUSER);
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.NAME_TABLE = "MDCATEGORYDETAIL_TYPEUSER"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.IDMDCATEGORYDETAIL.FieldName = "IDMDCATEGORYDETAIL"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.IDMDCATEGORYDETAIL.TableName = "MDCATEGORYDETAIL_TYPEUSER"; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.IDMDCATEGORYDETAIL.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.IDMDCATEGORYDETAIL.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL_TYPEUSER.IDMDCATEGORYDETAIL);
UsrCfg.InternoAtisNames.MDIMPACT.NAME_TABLE = "MDIMPACT"; 
UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.FieldName = "IDMDIMPACT"; 
UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.TableName = "MDIMPACT"; 
UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT);
UsrCfg.InternoAtisNames.MDIMPACT.NAME_TABLE = "MDIMPACT"; 
UsrCfg.InternoAtisNames.MDIMPACT.IMPACTNAME.FieldName = "IMPACTNAME"; 
UsrCfg.InternoAtisNames.MDIMPACT.IMPACTNAME.TableName = "MDIMPACT"; 
UsrCfg.InternoAtisNames.MDIMPACT.IMPACTNAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.MDIMPACT.IMPACTNAME.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDIMPACT.IMPACTNAME);
UsrCfg.InternoAtisNames.MDPRIORITY.NAME_TABLE = "MDPRIORITY"; 
UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName = "IDMDPRIORITY"; 
UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.TableName = "MDPRIORITY"; 
UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY);
UsrCfg.InternoAtisNames.MDPRIORITY.NAME_TABLE = "MDPRIORITY"; 
UsrCfg.InternoAtisNames.MDPRIORITY.PRIORITYNAME.FieldName = "PRIORITYNAME"; 
UsrCfg.InternoAtisNames.MDPRIORITY.PRIORITYNAME.TableName = "MDPRIORITY"; 
UsrCfg.InternoAtisNames.MDPRIORITY.PRIORITYNAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.MDPRIORITY.PRIORITYNAME.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDPRIORITY.PRIORITYNAME);
UsrCfg.InternoAtisNames.MDURGENCY.NAME_TABLE = "MDURGENCY"; 
UsrCfg.InternoAtisNames.MDURGENCY.IDMDURGENCY.FieldName = "IDMDURGENCY"; 
UsrCfg.InternoAtisNames.MDURGENCY.IDMDURGENCY.TableName = "MDURGENCY"; 
UsrCfg.InternoAtisNames.MDURGENCY.IDMDURGENCY.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDURGENCY.IDMDURGENCY.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDURGENCY.IDMDURGENCY);
UsrCfg.InternoAtisNames.MDURGENCY.NAME_TABLE = "MDURGENCY"; 
UsrCfg.InternoAtisNames.MDURGENCY.URGENCYNAME.FieldName = "URGENCYNAME"; 
UsrCfg.InternoAtisNames.MDURGENCY.URGENCYNAME.TableName = "MDURGENCY"; 
UsrCfg.InternoAtisNames.MDURGENCY.URGENCYNAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.MDURGENCY.URGENCYNAME.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDURGENCY.URGENCYNAME);
UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.NAME_TABLE = "MDPRIORITYMATRIX"; 
UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDIMPACT.FieldName = "IDMDIMPACT"; 
UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDIMPACT.TableName = "MDPRIORITYMATRIX"; 
UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDIMPACT.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDIMPACT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDIMPACT);
UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.NAME_TABLE = "MDPRIORITYMATRIX"; 
UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDURGENCY.FieldName = "IDMDURGENCY"; 
UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDURGENCY.TableName = "MDPRIORITYMATRIX"; 
UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDURGENCY.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDURGENCY.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDURGENCY);
UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.NAME_TABLE = "MDPRIORITYMATRIX"; 
UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDPRIORITY.FieldName = "IDMDPRIORITY"; 
UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDPRIORITY.TableName = "MDPRIORITYMATRIX"; 
UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDPRIORITY.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDPRIORITY.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDPRIORITY);
UsrCfg.InternoAtisNames.MDSLA.NAME_TABLE = "MDSLA"; 
UsrCfg.InternoAtisNames.MDSLA.IDMDSLA.FieldName = "IDMDSLA"; 
UsrCfg.InternoAtisNames.MDSLA.IDMDSLA.TableName = "MDSLA"; 
UsrCfg.InternoAtisNames.MDSLA.IDMDSLA.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.MDSLA.IDMDSLA.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDSLA.IDMDSLA);
UsrCfg.InternoAtisNames.MDSLA.NAME_TABLE = "MDSLA"; 
UsrCfg.InternoAtisNames.MDSLA.IDMDMODELTYPED.FieldName = "IDMDMODELTYPED"; 
UsrCfg.InternoAtisNames.MDSLA.IDMDMODELTYPED.TableName = "MDSLA"; 
UsrCfg.InternoAtisNames.MDSLA.IDMDMODELTYPED.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDSLA.IDMDMODELTYPED.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDSLA.IDMDMODELTYPED);
UsrCfg.InternoAtisNames.MDSLA.NAME_TABLE = "MDSLA"; 
UsrCfg.InternoAtisNames.MDSLA.IDCALEDAYDATE.FieldName = "IDCALEDAYDATE"; 
UsrCfg.InternoAtisNames.MDSLA.IDCALEDAYDATE.TableName = "MDSLA"; 
UsrCfg.InternoAtisNames.MDSLA.IDCALEDAYDATE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDSLA.IDCALEDAYDATE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDSLA.IDCALEDAYDATE);
UsrCfg.InternoAtisNames.MDSLA.NAME_TABLE = "MDSLA"; 
UsrCfg.InternoAtisNames.MDSLA.IDMDIMPACT.FieldName = "IDMDIMPACT"; 
UsrCfg.InternoAtisNames.MDSLA.IDMDIMPACT.TableName = "MDSLA"; 
UsrCfg.InternoAtisNames.MDSLA.IDMDIMPACT.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDSLA.IDMDIMPACT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDSLA.IDMDIMPACT);
UsrCfg.InternoAtisNames.MDSLA.NAME_TABLE = "MDSLA"; 
UsrCfg.InternoAtisNames.MDSLA.SLANAME.FieldName = "SLANAME"; 
UsrCfg.InternoAtisNames.MDSLA.SLANAME.TableName = "MDSLA"; 
UsrCfg.InternoAtisNames.MDSLA.SLANAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.MDSLA.SLANAME.Size = 255; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDSLA.SLANAME);
UsrCfg.InternoAtisNames.MDSLA.NAME_TABLE = "MDSLA"; 
UsrCfg.InternoAtisNames.MDSLA.SLADESCRIPTION.FieldName = "SLADESCRIPTION"; 
UsrCfg.InternoAtisNames.MDSLA.SLADESCRIPTION.TableName = "MDSLA"; 
UsrCfg.InternoAtisNames.MDSLA.SLADESCRIPTION.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.MDSLA.SLADESCRIPTION.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDSLA.SLADESCRIPTION);
UsrCfg.InternoAtisNames.MDSLA.NAME_TABLE = "MDSLA"; 
UsrCfg.InternoAtisNames.MDSLA.SLAPARENT.FieldName = "SLAPARENT"; 
UsrCfg.InternoAtisNames.MDSLA.SLAPARENT.TableName = "MDSLA"; 
UsrCfg.InternoAtisNames.MDSLA.SLAPARENT.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDSLA.SLAPARENT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDSLA.SLAPARENT);
UsrCfg.InternoAtisNames.MDSLA.NAME_TABLE = "MDSLA"; 
UsrCfg.InternoAtisNames.MDSLA.SLACONDITION.FieldName = "SLACONDITION"; 
UsrCfg.InternoAtisNames.MDSLA.SLACONDITION.TableName = "MDSLA"; 
UsrCfg.InternoAtisNames.MDSLA.SLACONDITION.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.MDSLA.SLACONDITION.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDSLA.SLACONDITION);
UsrCfg.InternoAtisNames.MDSLA.NAME_TABLE = "MDSLA"; 
UsrCfg.InternoAtisNames.MDSLA.SLA_MAXTIME.FieldName = "SLA_MAXTIME"; 
UsrCfg.InternoAtisNames.MDSLA.SLA_MAXTIME.TableName = "MDSLA"; 
UsrCfg.InternoAtisNames.MDSLA.SLA_MAXTIME.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDSLA.SLA_MAXTIME.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDSLA.SLA_MAXTIME);
UsrCfg.InternoAtisNames.MDSLA.NAME_TABLE = "MDSLA"; 
UsrCfg.InternoAtisNames.MDSLA.SLA_NORMALTIME.FieldName = "SLA_NORMALTIME"; 
UsrCfg.InternoAtisNames.MDSLA.SLA_NORMALTIME.TableName = "MDSLA"; 
UsrCfg.InternoAtisNames.MDSLA.SLA_NORMALTIME.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDSLA.SLA_NORMALTIME.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDSLA.SLA_NORMALTIME);
UsrCfg.InternoAtisNames.MDSLA.NAME_TABLE = "MDSLA"; 
UsrCfg.InternoAtisNames.MDSLA.IDSDSCALETYPE_FUNC.FieldName = "IDSDSCALETYPE_FUNC"; 
UsrCfg.InternoAtisNames.MDSLA.IDSDSCALETYPE_FUNC.TableName = "MDSLA"; 
UsrCfg.InternoAtisNames.MDSLA.IDSDSCALETYPE_FUNC.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDSLA.IDSDSCALETYPE_FUNC.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDSLA.IDSDSCALETYPE_FUNC);
UsrCfg.InternoAtisNames.MDSLA.NAME_TABLE = "MDSLA"; 
UsrCfg.InternoAtisNames.MDSLA.IDSDSCALETYPE_HIER.FieldName = "IDSDSCALETYPE_HIER"; 
UsrCfg.InternoAtisNames.MDSLA.IDSDSCALETYPE_HIER.TableName = "MDSLA"; 
UsrCfg.InternoAtisNames.MDSLA.IDSDSCALETYPE_HIER.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDSLA.IDSDSCALETYPE_HIER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDSLA.IDSDSCALETYPE_HIER);
UsrCfg.InternoAtisNames.MDSLA.NAME_TABLE = "MDSLA"; 
UsrCfg.InternoAtisNames.MDSLA.SLAORDER.FieldName = "SLAORDER"; 
UsrCfg.InternoAtisNames.MDSLA.SLAORDER.TableName = "MDSLA"; 
UsrCfg.InternoAtisNames.MDSLA.SLAORDER.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDSLA.SLAORDER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDSLA.SLAORDER);
UsrCfg.InternoAtisNames.MDSLA.NAME_TABLE = "MDSLA"; 
UsrCfg.InternoAtisNames.MDSLA.SLASTATUS.FieldName = "SLASTATUS"; 
UsrCfg.InternoAtisNames.MDSLA.SLASTATUS.TableName = "MDSLA"; 
UsrCfg.InternoAtisNames.MDSLA.SLASTATUS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDSLA.SLASTATUS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDSLA.SLASTATUS);
UsrCfg.InternoAtisNames.MDCONFIG.NAME_TABLE = "MDCONFIG"; 
UsrCfg.InternoAtisNames.MDCONFIG.IDMDCONFIG.FieldName = "IDMDCONFIG"; 
UsrCfg.InternoAtisNames.MDCONFIG.IDMDCONFIG.TableName = "MDCONFIG"; 
UsrCfg.InternoAtisNames.MDCONFIG.IDMDCONFIG.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDCONFIG.IDMDCONFIG.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCONFIG.IDMDCONFIG);
UsrCfg.InternoAtisNames.MDCONFIG.NAME_TABLE = "MDCONFIG"; 
UsrCfg.InternoAtisNames.MDCONFIG.IDMDCATEGORYDETAIL_DEFAULT.FieldName = "IDMDCATEGORYDETAIL_DEFAULT"; 
UsrCfg.InternoAtisNames.MDCONFIG.IDMDCATEGORYDETAIL_DEFAULT.TableName = "MDCONFIG"; 
UsrCfg.InternoAtisNames.MDCONFIG.IDMDCATEGORYDETAIL_DEFAULT.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDCONFIG.IDMDCATEGORYDETAIL_DEFAULT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCONFIG.IDMDCATEGORYDETAIL_DEFAULT);
UsrCfg.InternoAtisNames.MDCONFIG.NAME_TABLE = "MDCONFIG"; 
UsrCfg.InternoAtisNames.MDCONFIG.IDMDURGENCY_DEFAULT.FieldName = "IDMDURGENCY_DEFAULT"; 
UsrCfg.InternoAtisNames.MDCONFIG.IDMDURGENCY_DEFAULT.TableName = "MDCONFIG"; 
UsrCfg.InternoAtisNames.MDCONFIG.IDMDURGENCY_DEFAULT.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDCONFIG.IDMDURGENCY_DEFAULT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCONFIG.IDMDURGENCY_DEFAULT);
UsrCfg.InternoAtisNames.CPU.NAME_TABLE = "CPU"; 
UsrCfg.InternoAtisNames.CPU.IDCPU.FieldName = "IDCPU"; 
UsrCfg.InternoAtisNames.CPU.IDCPU.TableName = "CPU"; 
UsrCfg.InternoAtisNames.CPU.IDCPU.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.CPU.IDCPU.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CPU.IDCPU);
UsrCfg.InternoAtisNames.CPU.NAME_TABLE = "CPU"; 
UsrCfg.InternoAtisNames.CPU.MARCA_DE_CPU.FieldName = "MARCA_DE_CPU"; 
UsrCfg.InternoAtisNames.CPU.MARCA_DE_CPU.TableName = "CPU"; 
UsrCfg.InternoAtisNames.CPU.MARCA_DE_CPU.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.CPU.MARCA_DE_CPU.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CPU.MARCA_DE_CPU);
UsrCfg.InternoAtisNames.CPU.NAME_TABLE = "CPU"; 
UsrCfg.InternoAtisNames.CPU.MODELO_DE_CPU.FieldName = "MODELO_DE_CPU"; 
UsrCfg.InternoAtisNames.CPU.MODELO_DE_CPU.TableName = "CPU"; 
UsrCfg.InternoAtisNames.CPU.MODELO_DE_CPU.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.CPU.MODELO_DE_CPU.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CPU.MODELO_DE_CPU);
UsrCfg.InternoAtisNames.CPU.NAME_TABLE = "CPU"; 
UsrCfg.InternoAtisNames.CPU.SERIAL_DE_CPU.FieldName = "SERIAL_DE_CPU"; 
UsrCfg.InternoAtisNames.CPU.SERIAL_DE_CPU.TableName = "CPU"; 
UsrCfg.InternoAtisNames.CPU.SERIAL_DE_CPU.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.CPU.SERIAL_DE_CPU.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CPU.SERIAL_DE_CPU);
UsrCfg.InternoAtisNames.CPU.NAME_TABLE = "CPU"; 
UsrCfg.InternoAtisNames.CPU.MEMORIA_TOTAL_SIST.FieldName = "MEMORIA_TOTAL_SIST"; 
UsrCfg.InternoAtisNames.CPU.MEMORIA_TOTAL_SIST.TableName = "CPU"; 
UsrCfg.InternoAtisNames.CPU.MEMORIA_TOTAL_SIST.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.CPU.MEMORIA_TOTAL_SIST.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CPU.MEMORIA_TOTAL_SIST);
UsrCfg.InternoAtisNames.CPU.NAME_TABLE = "CPU"; 
UsrCfg.InternoAtisNames.CPU.MEMORIA_TOTAL_CPU.FieldName = "MEMORIA_TOTAL_CPU"; 
UsrCfg.InternoAtisNames.CPU.MEMORIA_TOTAL_CPU.TableName = "CPU"; 
UsrCfg.InternoAtisNames.CPU.MEMORIA_TOTAL_CPU.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.CPU.MEMORIA_TOTAL_CPU.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CPU.MEMORIA_TOTAL_CPU);
UsrCfg.InternoAtisNames.CPU.NAME_TABLE = "CPU"; 
UsrCfg.InternoAtisNames.CPU.MEMORIA_DISPONIBLE.FieldName = "MEMORIA_DISPONIBLE"; 
UsrCfg.InternoAtisNames.CPU.MEMORIA_DISPONIBLE.TableName = "CPU"; 
UsrCfg.InternoAtisNames.CPU.MEMORIA_DISPONIBLE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.CPU.MEMORIA_DISPONIBLE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CPU.MEMORIA_DISPONIBLE);
UsrCfg.InternoAtisNames.CPU.NAME_TABLE = "CPU"; 
UsrCfg.InternoAtisNames.CPU.MEMORIA_VIRTUAL.FieldName = "MEMORIA_VIRTUAL"; 
UsrCfg.InternoAtisNames.CPU.MEMORIA_VIRTUAL.TableName = "CPU"; 
UsrCfg.InternoAtisNames.CPU.MEMORIA_VIRTUAL.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.CPU.MEMORIA_VIRTUAL.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CPU.MEMORIA_VIRTUAL);
UsrCfg.InternoAtisNames.CPU.NAME_TABLE = "CPU"; 
UsrCfg.InternoAtisNames.CPU.MEMORIA_VIRTUAL_DISPONIBLE.FieldName = "MEMORIA_VIRTUAL_DISPONIBLE"; 
UsrCfg.InternoAtisNames.CPU.MEMORIA_VIRTUAL_DISPONIBLE.TableName = "CPU"; 
UsrCfg.InternoAtisNames.CPU.MEMORIA_VIRTUAL_DISPONIBLE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.CPU.MEMORIA_VIRTUAL_DISPONIBLE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CPU.MEMORIA_VIRTUAL_DISPONIBLE);
UsrCfg.InternoAtisNames.CPU.NAME_TABLE = "CPU"; 
UsrCfg.InternoAtisNames.CPU.TIENE_CD.FieldName = "TIENE_CD"; 
UsrCfg.InternoAtisNames.CPU.TIENE_CD.TableName = "CPU"; 
UsrCfg.InternoAtisNames.CPU.TIENE_CD.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.CPU.TIENE_CD.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CPU.TIENE_CD);
UsrCfg.InternoAtisNames.CPU.NAME_TABLE = "CPU"; 
UsrCfg.InternoAtisNames.CPU.FABRICANTE_DEL_PROCESADOR.FieldName = "FABRICANTE_DEL_PROCESADOR"; 
UsrCfg.InternoAtisNames.CPU.FABRICANTE_DEL_PROCESADOR.TableName = "CPU"; 
UsrCfg.InternoAtisNames.CPU.FABRICANTE_DEL_PROCESADOR.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.CPU.FABRICANTE_DEL_PROCESADOR.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CPU.FABRICANTE_DEL_PROCESADOR);
UsrCfg.InternoAtisNames.CPU.NAME_TABLE = "CPU"; 
UsrCfg.InternoAtisNames.CPU.NOMBRE_DEL_PROCESADOR.FieldName = "NOMBRE_DEL_PROCESADOR"; 
UsrCfg.InternoAtisNames.CPU.NOMBRE_DEL_PROCESADOR.TableName = "CPU"; 
UsrCfg.InternoAtisNames.CPU.NOMBRE_DEL_PROCESADOR.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.CPU.NOMBRE_DEL_PROCESADOR.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CPU.NOMBRE_DEL_PROCESADOR);
UsrCfg.InternoAtisNames.CPU.NAME_TABLE = "CPU"; 
UsrCfg.InternoAtisNames.CPU.TIPO_DEL_PROCESADOR.FieldName = "TIPO_DEL_PROCESADOR"; 
UsrCfg.InternoAtisNames.CPU.TIPO_DEL_PROCESADOR.TableName = "CPU"; 
UsrCfg.InternoAtisNames.CPU.TIPO_DEL_PROCESADOR.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.CPU.TIPO_DEL_PROCESADOR.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CPU.TIPO_DEL_PROCESADOR);
UsrCfg.InternoAtisNames.CPU.NAME_TABLE = "CPU"; 
UsrCfg.InternoAtisNames.CPU.VELOCIDAD_PROCESADOR.FieldName = "VELOCIDAD_PROCESADOR"; 
UsrCfg.InternoAtisNames.CPU.VELOCIDAD_PROCESADOR.TableName = "CPU"; 
UsrCfg.InternoAtisNames.CPU.VELOCIDAD_PROCESADOR.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.CPU.VELOCIDAD_PROCESADOR.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CPU.VELOCIDAD_PROCESADOR);
UsrCfg.InternoAtisNames.CPU.NAME_TABLE = "CPU"; 
UsrCfg.InternoAtisNames.CPU.CRONO_PROCESADOR.FieldName = "CRONO_PROCESADOR"; 
UsrCfg.InternoAtisNames.CPU.CRONO_PROCESADOR.TableName = "CPU"; 
UsrCfg.InternoAtisNames.CPU.CRONO_PROCESADOR.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.CPU.CRONO_PROCESADOR.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CPU.CRONO_PROCESADOR);
UsrCfg.InternoAtisNames.CPU.NAME_TABLE = "CPU"; 
UsrCfg.InternoAtisNames.CPU.PROCESADOR.FieldName = "PROCESADOR"; 
UsrCfg.InternoAtisNames.CPU.PROCESADOR.TableName = "CPU"; 
UsrCfg.InternoAtisNames.CPU.PROCESADOR.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.CPU.PROCESADOR.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CPU.PROCESADOR);
UsrCfg.InternoAtisNames.CPU.NAME_TABLE = "CPU"; 
UsrCfg.InternoAtisNames.CPU.NUMERO_DE_PROCESADORES.FieldName = "NUMERO_DE_PROCESADORES"; 
UsrCfg.InternoAtisNames.CPU.NUMERO_DE_PROCESADORES.TableName = "CPU"; 
UsrCfg.InternoAtisNames.CPU.NUMERO_DE_PROCESADORES.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.CPU.NUMERO_DE_PROCESADORES.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CPU.NUMERO_DE_PROCESADORES);
UsrCfg.InternoAtisNames.CPU.NAME_TABLE = "CPU"; 
UsrCfg.InternoAtisNames.CPU.NOMBRE_DE_BIOS.FieldName = "NOMBRE_DE_BIOS"; 
UsrCfg.InternoAtisNames.CPU.NOMBRE_DE_BIOS.TableName = "CPU"; 
UsrCfg.InternoAtisNames.CPU.NOMBRE_DE_BIOS.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.CPU.NOMBRE_DE_BIOS.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CPU.NOMBRE_DE_BIOS);
UsrCfg.InternoAtisNames.CPU.NAME_TABLE = "CPU"; 
UsrCfg.InternoAtisNames.CPU.VERSION_DE_BIOS.FieldName = "VERSION_DE_BIOS"; 
UsrCfg.InternoAtisNames.CPU.VERSION_DE_BIOS.TableName = "CPU"; 
UsrCfg.InternoAtisNames.CPU.VERSION_DE_BIOS.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.CPU.VERSION_DE_BIOS.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CPU.VERSION_DE_BIOS);
UsrCfg.InternoAtisNames.CPU.NAME_TABLE = "CPU"; 
UsrCfg.InternoAtisNames.CPU.FECHA_DE_BIOS.FieldName = "FECHA_DE_BIOS"; 
UsrCfg.InternoAtisNames.CPU.FECHA_DE_BIOS.TableName = "CPU"; 
UsrCfg.InternoAtisNames.CPU.FECHA_DE_BIOS.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.CPU.FECHA_DE_BIOS.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CPU.FECHA_DE_BIOS);
UsrCfg.InternoAtisNames.CPU.NAME_TABLE = "CPU"; 
UsrCfg.InternoAtisNames.CPU.BRANDID.FieldName = "BRANDID"; 
UsrCfg.InternoAtisNames.CPU.BRANDID.TableName = "CPU"; 
UsrCfg.InternoAtisNames.CPU.BRANDID.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.CPU.BRANDID.Size = 80; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CPU.BRANDID);
UsrCfg.InternoAtisNames.CPU.NAME_TABLE = "CPU"; 
UsrCfg.InternoAtisNames.CPU.HYPER.FieldName = "HYPER"; 
UsrCfg.InternoAtisNames.CPU.HYPER.TableName = "CPU"; 
UsrCfg.InternoAtisNames.CPU.HYPER.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.CPU.HYPER.Size = 20; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CPU.HYPER);
UsrCfg.InternoAtisNames.ESTACION_RED.NAME_TABLE = "ESTACION_RED"; 
UsrCfg.InternoAtisNames.ESTACION_RED.IDCPU.FieldName = "IDCPU"; 
UsrCfg.InternoAtisNames.ESTACION_RED.IDCPU.TableName = "ESTACION_RED"; 
UsrCfg.InternoAtisNames.ESTACION_RED.IDCPU.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.ESTACION_RED.IDCPU.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ESTACION_RED.IDCPU);
UsrCfg.InternoAtisNames.ESTACION_RED.NAME_TABLE = "ESTACION_RED"; 
UsrCfg.InternoAtisNames.ESTACION_RED.NOMBRE_ESTACION.FieldName = "NOMBRE_ESTACION"; 
UsrCfg.InternoAtisNames.ESTACION_RED.NOMBRE_ESTACION.TableName = "ESTACION_RED"; 
UsrCfg.InternoAtisNames.ESTACION_RED.NOMBRE_ESTACION.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.ESTACION_RED.NOMBRE_ESTACION.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ESTACION_RED.NOMBRE_ESTACION);
UsrCfg.InternoAtisNames.ESTACION_RED.NAME_TABLE = "ESTACION_RED"; 
UsrCfg.InternoAtisNames.ESTACION_RED.DESCRIPCION.FieldName = "DESCRIPCION"; 
UsrCfg.InternoAtisNames.ESTACION_RED.DESCRIPCION.TableName = "ESTACION_RED"; 
UsrCfg.InternoAtisNames.ESTACION_RED.DESCRIPCION.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.ESTACION_RED.DESCRIPCION.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ESTACION_RED.DESCRIPCION);
UsrCfg.InternoAtisNames.ESTACION_RED.NAME_TABLE = "ESTACION_RED"; 
UsrCfg.InternoAtisNames.ESTACION_RED.NOMBRE_USUARIO.FieldName = "NOMBRE_USUARIO"; 
UsrCfg.InternoAtisNames.ESTACION_RED.NOMBRE_USUARIO.TableName = "ESTACION_RED"; 
UsrCfg.InternoAtisNames.ESTACION_RED.NOMBRE_USUARIO.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.ESTACION_RED.NOMBRE_USUARIO.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ESTACION_RED.NOMBRE_USUARIO);
UsrCfg.InternoAtisNames.ESTACION_RED.NAME_TABLE = "ESTACION_RED"; 
UsrCfg.InternoAtisNames.ESTACION_RED.GRUPO_DE_TRABAJO.FieldName = "GRUPO_DE_TRABAJO"; 
UsrCfg.InternoAtisNames.ESTACION_RED.GRUPO_DE_TRABAJO.TableName = "ESTACION_RED"; 
UsrCfg.InternoAtisNames.ESTACION_RED.GRUPO_DE_TRABAJO.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.ESTACION_RED.GRUPO_DE_TRABAJO.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ESTACION_RED.GRUPO_DE_TRABAJO);
UsrCfg.InternoAtisNames.ESTACION_RED.NAME_TABLE = "ESTACION_RED"; 
UsrCfg.InternoAtisNames.ESTACION_RED.DOMINIO.FieldName = "DOMINIO"; 
UsrCfg.InternoAtisNames.ESTACION_RED.DOMINIO.TableName = "ESTACION_RED"; 
UsrCfg.InternoAtisNames.ESTACION_RED.DOMINIO.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.ESTACION_RED.DOMINIO.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ESTACION_RED.DOMINIO);
UsrCfg.InternoAtisNames.ESTACION_RED.NAME_TABLE = "ESTACION_RED"; 
UsrCfg.InternoAtisNames.ESTACION_RED.SERVIDOR.FieldName = "SERVIDOR"; 
UsrCfg.InternoAtisNames.ESTACION_RED.SERVIDOR.TableName = "ESTACION_RED"; 
UsrCfg.InternoAtisNames.ESTACION_RED.SERVIDOR.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.ESTACION_RED.SERVIDOR.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ESTACION_RED.SERVIDOR);
UsrCfg.InternoAtisNames.ESTACION_RED.NAME_TABLE = "ESTACION_RED"; 
UsrCfg.InternoAtisNames.ESTACION_RED.REGISTRADO_POR.FieldName = "REGISTRADO_POR"; 
UsrCfg.InternoAtisNames.ESTACION_RED.REGISTRADO_POR.TableName = "ESTACION_RED"; 
UsrCfg.InternoAtisNames.ESTACION_RED.REGISTRADO_POR.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.ESTACION_RED.REGISTRADO_POR.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ESTACION_RED.REGISTRADO_POR);
UsrCfg.InternoAtisNames.ESTACION_RED.NAME_TABLE = "ESTACION_RED"; 
UsrCfg.InternoAtisNames.ESTACION_RED.ORGANIZACION.FieldName = "ORGANIZACION"; 
UsrCfg.InternoAtisNames.ESTACION_RED.ORGANIZACION.TableName = "ESTACION_RED"; 
UsrCfg.InternoAtisNames.ESTACION_RED.ORGANIZACION.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.ESTACION_RED.ORGANIZACION.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ESTACION_RED.ORGANIZACION);
UsrCfg.InternoAtisNames.ESTACION_RED.NAME_TABLE = "ESTACION_RED"; 
UsrCfg.InternoAtisNames.ESTACION_RED.HOST_NAME.FieldName = "HOST_NAME"; 
UsrCfg.InternoAtisNames.ESTACION_RED.HOST_NAME.TableName = "ESTACION_RED"; 
UsrCfg.InternoAtisNames.ESTACION_RED.HOST_NAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.ESTACION_RED.HOST_NAME.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ESTACION_RED.HOST_NAME);
UsrCfg.InternoAtisNames.WINDOWS.NAME_TABLE = "WINDOWS"; 
UsrCfg.InternoAtisNames.WINDOWS.IDCPU.FieldName = "IDCPU"; 
UsrCfg.InternoAtisNames.WINDOWS.IDCPU.TableName = "WINDOWS"; 
UsrCfg.InternoAtisNames.WINDOWS.IDCPU.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.WINDOWS.IDCPU.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.WINDOWS.IDCPU);
UsrCfg.InternoAtisNames.WINDOWS.NAME_TABLE = "WINDOWS"; 
UsrCfg.InternoAtisNames.WINDOWS.WINDOWS.FieldName = "WINDOWS"; 
UsrCfg.InternoAtisNames.WINDOWS.WINDOWS.TableName = "WINDOWS"; 
UsrCfg.InternoAtisNames.WINDOWS.WINDOWS.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.WINDOWS.WINDOWS.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.WINDOWS.WINDOWS);
UsrCfg.InternoAtisNames.WINDOWS.NAME_TABLE = "WINDOWS"; 
UsrCfg.InternoAtisNames.WINDOWS.TIPO_SO.FieldName = "TIPO_SO"; 
UsrCfg.InternoAtisNames.WINDOWS.TIPO_SO.TableName = "WINDOWS"; 
UsrCfg.InternoAtisNames.WINDOWS.TIPO_SO.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.WINDOWS.TIPO_SO.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.WINDOWS.TIPO_SO);
UsrCfg.InternoAtisNames.WINDOWS.NAME_TABLE = "WINDOWS"; 
UsrCfg.InternoAtisNames.WINDOWS.KERNEL.FieldName = "KERNEL"; 
UsrCfg.InternoAtisNames.WINDOWS.KERNEL.TableName = "WINDOWS"; 
UsrCfg.InternoAtisNames.WINDOWS.KERNEL.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.WINDOWS.KERNEL.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.WINDOWS.KERNEL);
UsrCfg.InternoAtisNames.WINDOWS.NAME_TABLE = "WINDOWS"; 
UsrCfg.InternoAtisNames.WINDOWS.SUITE.FieldName = "SUITE"; 
UsrCfg.InternoAtisNames.WINDOWS.SUITE.TableName = "WINDOWS"; 
UsrCfg.InternoAtisNames.WINDOWS.SUITE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.WINDOWS.SUITE.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.WINDOWS.SUITE);
UsrCfg.InternoAtisNames.WINDOWS.NAME_TABLE = "WINDOWS"; 
UsrCfg.InternoAtisNames.WINDOWS.EXTRA_DATA.FieldName = "EXTRA_DATA"; 
UsrCfg.InternoAtisNames.WINDOWS.EXTRA_DATA.TableName = "WINDOWS"; 
UsrCfg.InternoAtisNames.WINDOWS.EXTRA_DATA.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.WINDOWS.EXTRA_DATA.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.WINDOWS.EXTRA_DATA);
UsrCfg.InternoAtisNames.WINDOWS.NAME_TABLE = "WINDOWS"; 
UsrCfg.InternoAtisNames.WINDOWS.VERSION_WINDOWS.FieldName = "VERSION_WINDOWS"; 
UsrCfg.InternoAtisNames.WINDOWS.VERSION_WINDOWS.TableName = "WINDOWS"; 
UsrCfg.InternoAtisNames.WINDOWS.VERSION_WINDOWS.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.WINDOWS.VERSION_WINDOWS.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.WINDOWS.VERSION_WINDOWS);
UsrCfg.InternoAtisNames.WINDOWS.NAME_TABLE = "WINDOWS"; 
UsrCfg.InternoAtisNames.WINDOWS.SERIAL_WIN.FieldName = "SERIAL_WIN"; 
UsrCfg.InternoAtisNames.WINDOWS.SERIAL_WIN.TableName = "WINDOWS"; 
UsrCfg.InternoAtisNames.WINDOWS.SERIAL_WIN.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.WINDOWS.SERIAL_WIN.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.WINDOWS.SERIAL_WIN);
UsrCfg.InternoAtisNames.WINDOWS.NAME_TABLE = "WINDOWS"; 
UsrCfg.InternoAtisNames.WINDOWS.WINSOCK.FieldName = "WINSOCK"; 
UsrCfg.InternoAtisNames.WINDOWS.WINSOCK.TableName = "WINDOWS"; 
UsrCfg.InternoAtisNames.WINDOWS.WINSOCK.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.WINDOWS.WINSOCK.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.WINDOWS.WINSOCK);
UsrCfg.InternoAtisNames.WINDOWS.NAME_TABLE = "WINDOWS"; 
UsrCfg.InternoAtisNames.WINDOWS.IDIOM.FieldName = "IDIOM"; 
UsrCfg.InternoAtisNames.WINDOWS.IDIOM.TableName = "WINDOWS"; 
UsrCfg.InternoAtisNames.WINDOWS.IDIOM.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.WINDOWS.IDIOM.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.WINDOWS.IDIOM);
UsrCfg.InternoAtisNames.WINDOWS.NAME_TABLE = "WINDOWS"; 
UsrCfg.InternoAtisNames.WINDOWS.INSTALL_DATE.FieldName = "INSTALL_DATE"; 
UsrCfg.InternoAtisNames.WINDOWS.INSTALL_DATE.TableName = "WINDOWS"; 
UsrCfg.InternoAtisNames.WINDOWS.INSTALL_DATE.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.WINDOWS.INSTALL_DATE.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.WINDOWS.INSTALL_DATE);
UsrCfg.InternoAtisNames.VPRO.NAME_TABLE = "VPRO"; 
UsrCfg.InternoAtisNames.VPRO.IDVPRO.FieldName = "IDVPRO"; 
UsrCfg.InternoAtisNames.VPRO.IDVPRO.TableName = "VPRO"; 
UsrCfg.InternoAtisNames.VPRO.IDVPRO.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.VPRO.IDVPRO.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.VPRO.IDVPRO);
UsrCfg.InternoAtisNames.VPRO.NAME_TABLE = "VPRO"; 
UsrCfg.InternoAtisNames.VPRO.IDCPU.FieldName = "IDCPU"; 
UsrCfg.InternoAtisNames.VPRO.IDCPU.TableName = "VPRO"; 
UsrCfg.InternoAtisNames.VPRO.IDCPU.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.VPRO.IDCPU.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.VPRO.IDCPU);
UsrCfg.InternoAtisNames.VPRO.NAME_TABLE = "VPRO"; 
UsrCfg.InternoAtisNames.VPRO.VPRONAME.FieldName = "VPRONAME"; 
UsrCfg.InternoAtisNames.VPRO.VPRONAME.TableName = "VPRO"; 
UsrCfg.InternoAtisNames.VPRO.VPRONAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.VPRO.VPRONAME.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.VPRO.VPRONAME);
UsrCfg.InternoAtisNames.VPRO.NAME_TABLE = "VPRO"; 
UsrCfg.InternoAtisNames.VPRO.VPROIP.FieldName = "VPROIP"; 
UsrCfg.InternoAtisNames.VPRO.VPROIP.TableName = "VPRO"; 
UsrCfg.InternoAtisNames.VPRO.VPROIP.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.VPRO.VPROIP.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.VPRO.VPROIP);
UsrCfg.InternoAtisNames.VPRO.NAME_TABLE = "VPRO"; 
UsrCfg.InternoAtisNames.VPRO.VPROCOMMENT.FieldName = "VPROCOMMENT"; 
UsrCfg.InternoAtisNames.VPRO.VPROCOMMENT.TableName = "VPRO"; 
UsrCfg.InternoAtisNames.VPRO.VPROCOMMENT.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.VPRO.VPROCOMMENT.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.VPRO.VPROCOMMENT);
UsrCfg.InternoAtisNames.AGENTE.NAME_TABLE = "AGENTE"; 
UsrCfg.InternoAtisNames.AGENTE.IDCPU.FieldName = "IDCPU"; 
UsrCfg.InternoAtisNames.AGENTE.IDCPU.TableName = "AGENTE"; 
UsrCfg.InternoAtisNames.AGENTE.IDCPU.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.AGENTE.IDCPU.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.AGENTE.IDCPU);
UsrCfg.InternoAtisNames.AGENTE.NAME_TABLE = "AGENTE"; 
UsrCfg.InternoAtisNames.AGENTE.VERSION.FieldName = "VERSION"; 
UsrCfg.InternoAtisNames.AGENTE.VERSION.TableName = "AGENTE"; 
UsrCfg.InternoAtisNames.AGENTE.VERSION.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.AGENTE.VERSION.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.AGENTE.VERSION);
UsrCfg.InternoAtisNames.AGENTE.NAME_TABLE = "AGENTE"; 
UsrCfg.InternoAtisNames.AGENTE.VERSION_HEAR.FieldName = "VERSION_HEAR"; 
UsrCfg.InternoAtisNames.AGENTE.VERSION_HEAR.TableName = "AGENTE"; 
UsrCfg.InternoAtisNames.AGENTE.VERSION_HEAR.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.AGENTE.VERSION_HEAR.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.AGENTE.VERSION_HEAR);
UsrCfg.InternoAtisNames.AGENTE.NAME_TABLE = "AGENTE"; 
UsrCfg.InternoAtisNames.AGENTE.ULTIMA_GENERACION.FieldName = "ULTIMA_GENERACION"; 
UsrCfg.InternoAtisNames.AGENTE.ULTIMA_GENERACION.TableName = "AGENTE"; 
UsrCfg.InternoAtisNames.AGENTE.ULTIMA_GENERACION.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.AGENTE.ULTIMA_GENERACION.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.AGENTE.ULTIMA_GENERACION);
UsrCfg.InternoAtisNames.AGENTE.NAME_TABLE = "AGENTE"; 
UsrCfg.InternoAtisNames.AGENTE.ULTIMA_RECEPCION.FieldName = "ULTIMA_RECEPCION"; 
UsrCfg.InternoAtisNames.AGENTE.ULTIMA_RECEPCION.TableName = "AGENTE"; 
UsrCfg.InternoAtisNames.AGENTE.ULTIMA_RECEPCION.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.AGENTE.ULTIMA_RECEPCION.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.AGENTE.ULTIMA_RECEPCION);
UsrCfg.InternoAtisNames.AGENTE.NAME_TABLE = "AGENTE"; 
UsrCfg.InternoAtisNames.AGENTE.NINVENT.FieldName = "NINVENT"; 
UsrCfg.InternoAtisNames.AGENTE.NINVENT.TableName = "AGENTE"; 
UsrCfg.InternoAtisNames.AGENTE.NINVENT.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.AGENTE.NINVENT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.AGENTE.NINVENT);
UsrCfg.InternoAtisNames.AGENTE.NAME_TABLE = "AGENTE"; 
UsrCfg.InternoAtisNames.AGENTE.CLIENTTYPE.FieldName = "CLIENTTYPE"; 
UsrCfg.InternoAtisNames.AGENTE.CLIENTTYPE.TableName = "AGENTE"; 
UsrCfg.InternoAtisNames.AGENTE.CLIENTTYPE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.AGENTE.CLIENTTYPE.Size = 10; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.AGENTE.CLIENTTYPE);
UsrCfg.InternoAtisNames.AGENTE.NAME_TABLE = "AGENTE"; 
UsrCfg.InternoAtisNames.AGENTE.DIRS_DATE.FieldName = "DIRS_DATE"; 
UsrCfg.InternoAtisNames.AGENTE.DIRS_DATE.TableName = "AGENTE"; 
UsrCfg.InternoAtisNames.AGENTE.DIRS_DATE.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.AGENTE.DIRS_DATE.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.AGENTE.DIRS_DATE);
UsrCfg.InternoAtisNames.AGENTE.NAME_TABLE = "AGENTE"; 
UsrCfg.InternoAtisNames.AGENTE.AG_STATUS.FieldName = "AG_STATUS"; 
UsrCfg.InternoAtisNames.AGENTE.AG_STATUS.TableName = "AGENTE"; 
UsrCfg.InternoAtisNames.AGENTE.AG_STATUS.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.AGENTE.AG_STATUS.Size = 10; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.AGENTE.AG_STATUS);
UsrCfg.InternoAtisNames.AGENTE.NAME_TABLE = "AGENTE"; 
UsrCfg.InternoAtisNames.AGENTE.COMPANY.FieldName = "COMPANY"; 
UsrCfg.InternoAtisNames.AGENTE.COMPANY.TableName = "AGENTE"; 
UsrCfg.InternoAtisNames.AGENTE.COMPANY.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.AGENTE.COMPANY.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.AGENTE.COMPANY);
UsrCfg.InternoAtisNames.AGENTE.NAME_TABLE = "AGENTE"; 
UsrCfg.InternoAtisNames.AGENTE.SSLSERVER.FieldName = "SSLSERVER"; 
UsrCfg.InternoAtisNames.AGENTE.SSLSERVER.TableName = "AGENTE"; 
UsrCfg.InternoAtisNames.AGENTE.SSLSERVER.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.AGENTE.SSLSERVER.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.AGENTE.SSLSERVER);
UsrCfg.InternoAtisNames.AGENTE.NAME_TABLE = "AGENTE"; 
UsrCfg.InternoAtisNames.AGENTE.SLICENSE.FieldName = "SLICENSE"; 
UsrCfg.InternoAtisNames.AGENTE.SLICENSE.TableName = "AGENTE"; 
UsrCfg.InternoAtisNames.AGENTE.SLICENSE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.AGENTE.SLICENSE.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.AGENTE.SLICENSE);
UsrCfg.InternoAtisNames.AGENTE.NAME_TABLE = "AGENTE"; 
UsrCfg.InternoAtisNames.AGENTE.ID_COMPANY_L.FieldName = "ID_COMPANY_L"; 
UsrCfg.InternoAtisNames.AGENTE.ID_COMPANY_L.TableName = "AGENTE"; 
UsrCfg.InternoAtisNames.AGENTE.ID_COMPANY_L.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.AGENTE.ID_COMPANY_L.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.AGENTE.ID_COMPANY_L);
UsrCfg.InternoAtisNames.AGENTE.NAME_TABLE = "AGENTE"; 
UsrCfg.InternoAtisNames.AGENTE.ALL_DATE.FieldName = "ALL_DATE"; 
UsrCfg.InternoAtisNames.AGENTE.ALL_DATE.TableName = "AGENTE"; 
UsrCfg.InternoAtisNames.AGENTE.ALL_DATE.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.AGENTE.ALL_DATE.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.AGENTE.ALL_DATE);
UsrCfg.InternoAtisNames.AGENTE.NAME_TABLE = "AGENTE"; 
UsrCfg.InternoAtisNames.AGENTE.A_CONNECTED.FieldName = "A_CONNECTED"; 
UsrCfg.InternoAtisNames.AGENTE.A_CONNECTED.TableName = "AGENTE"; 
UsrCfg.InternoAtisNames.AGENTE.A_CONNECTED.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.AGENTE.A_CONNECTED.Size = 10; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.AGENTE.A_CONNECTED);
UsrCfg.InternoAtisNames.USERS.NAME_TABLE = "USERS"; 
UsrCfg.InternoAtisNames.USERS.IDCPU.FieldName = "IDCPU"; 
UsrCfg.InternoAtisNames.USERS.IDCPU.TableName = "USERS"; 
UsrCfg.InternoAtisNames.USERS.IDCPU.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.USERS.IDCPU.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.USERS.IDCPU);
UsrCfg.InternoAtisNames.USERS.NAME_TABLE = "USERS"; 
UsrCfg.InternoAtisNames.USERS.NOMBREE.FieldName = "NOMBREE"; 
UsrCfg.InternoAtisNames.USERS.NOMBREE.TableName = "USERS"; 
UsrCfg.InternoAtisNames.USERS.NOMBREE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.USERS.NOMBREE.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.USERS.NOMBREE);
UsrCfg.InternoAtisNames.USERS.NAME_TABLE = "USERS"; 
UsrCfg.InternoAtisNames.USERS.APELLIDOE.FieldName = "APELLIDOE"; 
UsrCfg.InternoAtisNames.USERS.APELLIDOE.TableName = "USERS"; 
UsrCfg.InternoAtisNames.USERS.APELLIDOE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.USERS.APELLIDOE.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.USERS.APELLIDOE);
UsrCfg.InternoAtisNames.USERS.NAME_TABLE = "USERS"; 
UsrCfg.InternoAtisNames.USERS.DIRECCION.FieldName = "DIRECCION"; 
UsrCfg.InternoAtisNames.USERS.DIRECCION.TableName = "USERS"; 
UsrCfg.InternoAtisNames.USERS.DIRECCION.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.USERS.DIRECCION.Size = 80; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.USERS.DIRECCION);
UsrCfg.InternoAtisNames.USERS.NAME_TABLE = "USERS"; 
UsrCfg.InternoAtisNames.USERS.TELEFONO.FieldName = "TELEFONO"; 
UsrCfg.InternoAtisNames.USERS.TELEFONO.TableName = "USERS"; 
UsrCfg.InternoAtisNames.USERS.TELEFONO.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.USERS.TELEFONO.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.USERS.TELEFONO);
UsrCfg.InternoAtisNames.USERS.NAME_TABLE = "USERS"; 
UsrCfg.InternoAtisNames.USERS.UBICACION.FieldName = "UBICACION"; 
UsrCfg.InternoAtisNames.USERS.UBICACION.TableName = "USERS"; 
UsrCfg.InternoAtisNames.USERS.UBICACION.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.USERS.UBICACION.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.USERS.UBICACION);
UsrCfg.InternoAtisNames.USERS.NAME_TABLE = "USERS"; 
UsrCfg.InternoAtisNames.USERS.MAIL.FieldName = "MAIL"; 
UsrCfg.InternoAtisNames.USERS.MAIL.TableName = "USERS"; 
UsrCfg.InternoAtisNames.USERS.MAIL.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.USERS.MAIL.Size = 80; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.USERS.MAIL);
UsrCfg.InternoAtisNames.USERS.NAME_TABLE = "USERS"; 
UsrCfg.InternoAtisNames.USERS.CEMPLEADO.FieldName = "CEMPLEADO"; 
UsrCfg.InternoAtisNames.USERS.CEMPLEADO.TableName = "USERS"; 
UsrCfg.InternoAtisNames.USERS.CEMPLEADO.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.USERS.CEMPLEADO.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.USERS.CEMPLEADO);
UsrCfg.InternoAtisNames.IP.NAME_TABLE = "IP"; 
UsrCfg.InternoAtisNames.IP.IDCPU.FieldName = "IDCPU"; 
UsrCfg.InternoAtisNames.IP.IDCPU.TableName = "IP"; 
UsrCfg.InternoAtisNames.IP.IDCPU.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.IP.IDCPU.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.IP.IDCPU);
UsrCfg.InternoAtisNames.IP.NAME_TABLE = "IP"; 
UsrCfg.InternoAtisNames.IP.ADAPTADOR_IP.FieldName = "ADAPTADOR_IP"; 
UsrCfg.InternoAtisNames.IP.ADAPTADOR_IP.TableName = "IP"; 
UsrCfg.InternoAtisNames.IP.ADAPTADOR_IP.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.IP.ADAPTADOR_IP.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.IP.ADAPTADOR_IP);
UsrCfg.InternoAtisNames.IP.NAME_TABLE = "IP"; 
UsrCfg.InternoAtisNames.IP.DIRECCION_IP.FieldName = "DIRECCION_IP"; 
UsrCfg.InternoAtisNames.IP.DIRECCION_IP.TableName = "IP"; 
UsrCfg.InternoAtisNames.IP.DIRECCION_IP.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.IP.DIRECCION_IP.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.IP.DIRECCION_IP);
UsrCfg.InternoAtisNames.IP.NAME_TABLE = "IP"; 
UsrCfg.InternoAtisNames.IP.NETCARDTYPE_IP.FieldName = "NETCARDTYPE_IP"; 
UsrCfg.InternoAtisNames.IP.NETCARDTYPE_IP.TableName = "IP"; 
UsrCfg.InternoAtisNames.IP.NETCARDTYPE_IP.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.IP.NETCARDTYPE_IP.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.IP.NETCARDTYPE_IP);
UsrCfg.InternoAtisNames.IP.NAME_TABLE = "IP"; 
UsrCfg.InternoAtisNames.IP.MASK_IP.FieldName = "MASK_IP"; 
UsrCfg.InternoAtisNames.IP.MASK_IP.TableName = "IP"; 
UsrCfg.InternoAtisNames.IP.MASK_IP.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.IP.MASK_IP.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.IP.MASK_IP);
UsrCfg.InternoAtisNames.IP.NAME_TABLE = "IP"; 
UsrCfg.InternoAtisNames.IP.GATEWAY_IP.FieldName = "GATEWAY_IP"; 
UsrCfg.InternoAtisNames.IP.GATEWAY_IP.TableName = "IP"; 
UsrCfg.InternoAtisNames.IP.GATEWAY_IP.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.IP.GATEWAY_IP.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.IP.GATEWAY_IP);
UsrCfg.InternoAtisNames.IP.NAME_TABLE = "IP"; 
UsrCfg.InternoAtisNames.IP.MAC_IP.FieldName = "MAC_IP"; 
UsrCfg.InternoAtisNames.IP.MAC_IP.TableName = "IP"; 
UsrCfg.InternoAtisNames.IP.MAC_IP.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.IP.MAC_IP.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.IP.MAC_IP);
UsrCfg.InternoAtisNames.IP.NAME_TABLE = "IP"; 
UsrCfg.InternoAtisNames.IP.DHCP_ON.FieldName = "DHCP_ON"; 
UsrCfg.InternoAtisNames.IP.DHCP_ON.TableName = "IP"; 
UsrCfg.InternoAtisNames.IP.DHCP_ON.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.IP.DHCP_ON.Size = 10; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.IP.DHCP_ON);
UsrCfg.InternoAtisNames.IP.NAME_TABLE = "IP"; 
UsrCfg.InternoAtisNames.IP.DHCP_IP.FieldName = "DHCP_IP"; 
UsrCfg.InternoAtisNames.IP.DHCP_IP.TableName = "IP"; 
UsrCfg.InternoAtisNames.IP.DHCP_IP.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.IP.DHCP_IP.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.IP.DHCP_IP);
UsrCfg.InternoAtisNames.IP.NAME_TABLE = "IP"; 
UsrCfg.InternoAtisNames.IP.DNS1.FieldName = "DNS1"; 
UsrCfg.InternoAtisNames.IP.DNS1.TableName = "IP"; 
UsrCfg.InternoAtisNames.IP.DNS1.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.IP.DNS1.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.IP.DNS1);
UsrCfg.InternoAtisNames.IP.NAME_TABLE = "IP"; 
UsrCfg.InternoAtisNames.IP.DNS2.FieldName = "DNS2"; 
UsrCfg.InternoAtisNames.IP.DNS2.TableName = "IP"; 
UsrCfg.InternoAtisNames.IP.DNS2.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.IP.DNS2.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.IP.DNS2);
UsrCfg.InternoAtisNames.EXTRADATA.NAME_TABLE = "EXTRADATA"; 
UsrCfg.InternoAtisNames.EXTRADATA.IDCPU.FieldName = "IDCPU"; 
UsrCfg.InternoAtisNames.EXTRADATA.IDCPU.TableName = "EXTRADATA"; 
UsrCfg.InternoAtisNames.EXTRADATA.IDCPU.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.EXTRADATA.IDCPU.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.EXTRADATA.IDCPU);
UsrCfg.InternoAtisNames.EXTRADATA.NAME_TABLE = "EXTRADATA"; 
UsrCfg.InternoAtisNames.EXTRADATA.NOMBREE.FieldName = "NOMBREE"; 
UsrCfg.InternoAtisNames.EXTRADATA.NOMBREE.TableName = "EXTRADATA"; 
UsrCfg.InternoAtisNames.EXTRADATA.NOMBREE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.EXTRADATA.NOMBREE.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.EXTRADATA.NOMBREE);
UsrCfg.InternoAtisNames.EXTRADATA.NAME_TABLE = "EXTRADATA"; 
UsrCfg.InternoAtisNames.EXTRADATA.APELLIDOE.FieldName = "APELLIDOE"; 
UsrCfg.InternoAtisNames.EXTRADATA.APELLIDOE.TableName = "EXTRADATA"; 
UsrCfg.InternoAtisNames.EXTRADATA.APELLIDOE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.EXTRADATA.APELLIDOE.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.EXTRADATA.APELLIDOE);
UsrCfg.InternoAtisNames.EXTRADATA.NAME_TABLE = "EXTRADATA"; 
UsrCfg.InternoAtisNames.EXTRADATA.DIRECCION.FieldName = "DIRECCION"; 
UsrCfg.InternoAtisNames.EXTRADATA.DIRECCION.TableName = "EXTRADATA"; 
UsrCfg.InternoAtisNames.EXTRADATA.DIRECCION.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.EXTRADATA.DIRECCION.Size = 80; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.EXTRADATA.DIRECCION);
UsrCfg.InternoAtisNames.EXTRADATA.NAME_TABLE = "EXTRADATA"; 
UsrCfg.InternoAtisNames.EXTRADATA.TELEFONO.FieldName = "TELEFONO"; 
UsrCfg.InternoAtisNames.EXTRADATA.TELEFONO.TableName = "EXTRADATA"; 
UsrCfg.InternoAtisNames.EXTRADATA.TELEFONO.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.EXTRADATA.TELEFONO.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.EXTRADATA.TELEFONO);
UsrCfg.InternoAtisNames.EXTRADATA.NAME_TABLE = "EXTRADATA"; 
UsrCfg.InternoAtisNames.EXTRADATA.UBICACION.FieldName = "UBICACION"; 
UsrCfg.InternoAtisNames.EXTRADATA.UBICACION.TableName = "EXTRADATA"; 
UsrCfg.InternoAtisNames.EXTRADATA.UBICACION.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.EXTRADATA.UBICACION.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.EXTRADATA.UBICACION);
UsrCfg.InternoAtisNames.EXTRADATA.NAME_TABLE = "EXTRADATA"; 
UsrCfg.InternoAtisNames.EXTRADATA.MAIL.FieldName = "MAIL"; 
UsrCfg.InternoAtisNames.EXTRADATA.MAIL.TableName = "EXTRADATA"; 
UsrCfg.InternoAtisNames.EXTRADATA.MAIL.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.EXTRADATA.MAIL.Size = 80; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.EXTRADATA.MAIL);
UsrCfg.InternoAtisNames.EXTRADATA.NAME_TABLE = "EXTRADATA"; 
UsrCfg.InternoAtisNames.EXTRADATA.CEMPLEADO.FieldName = "CEMPLEADO"; 
UsrCfg.InternoAtisNames.EXTRADATA.CEMPLEADO.TableName = "EXTRADATA"; 
UsrCfg.InternoAtisNames.EXTRADATA.CEMPLEADO.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.EXTRADATA.CEMPLEADO.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.EXTRADATA.CEMPLEADO);
UsrCfg.InternoAtisNames.EXTRADATA.NAME_TABLE = "EXTRADATA"; 
UsrCfg.InternoAtisNames.EXTRADATA.AYUNOMBRE.FieldName = "AYUNOMBRE"; 
UsrCfg.InternoAtisNames.EXTRADATA.AYUNOMBRE.TableName = "EXTRADATA"; 
UsrCfg.InternoAtisNames.EXTRADATA.AYUNOMBRE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.EXTRADATA.AYUNOMBRE.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.EXTRADATA.AYUNOMBRE);
UsrCfg.InternoAtisNames.EXTRADATA.NAME_TABLE = "EXTRADATA"; 
UsrCfg.InternoAtisNames.EXTRADATA.AYUTELEFONO.FieldName = "AYUTELEFONO"; 
UsrCfg.InternoAtisNames.EXTRADATA.AYUTELEFONO.TableName = "EXTRADATA"; 
UsrCfg.InternoAtisNames.EXTRADATA.AYUTELEFONO.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.EXTRADATA.AYUTELEFONO.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.EXTRADATA.AYUTELEFONO);
UsrCfg.InternoAtisNames.EXTRADATA.NAME_TABLE = "EXTRADATA"; 
UsrCfg.InternoAtisNames.EXTRADATA.AYUPROBLEMA.FieldName = "AYUPROBLEMA"; 
UsrCfg.InternoAtisNames.EXTRADATA.AYUPROBLEMA.TableName = "EXTRADATA"; 
UsrCfg.InternoAtisNames.EXTRADATA.AYUPROBLEMA.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.EXTRADATA.AYUPROBLEMA.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.EXTRADATA.AYUPROBLEMA);
UsrCfg.InternoAtisNames.EXTRADATA.NAME_TABLE = "EXTRADATA"; 
UsrCfg.InternoAtisNames.EXTRADATA.USERS.FieldName = "USERS"; 
UsrCfg.InternoAtisNames.EXTRADATA.USERS.TableName = "EXTRADATA"; 
UsrCfg.InternoAtisNames.EXTRADATA.USERS.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.EXTRADATA.USERS.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.EXTRADATA.USERS);
UsrCfg.InternoAtisNames.EXTRADATA.NAME_TABLE = "EXTRADATA"; 
UsrCfg.InternoAtisNames.EXTRADATA.EXTRA_DATE.FieldName = "EXTRA_DATE"; 
UsrCfg.InternoAtisNames.EXTRADATA.EXTRA_DATE.TableName = "EXTRADATA"; 
UsrCfg.InternoAtisNames.EXTRADATA.EXTRA_DATE.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.EXTRADATA.EXTRA_DATE.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.EXTRADATA.EXTRA_DATE);
UsrCfg.InternoAtisNames.IP_SERVERS.NAME_TABLE = "IP_SERVERS"; 
UsrCfg.InternoAtisNames.IP_SERVERS.COMPANY_NAME.FieldName = "COMPANY_NAME"; 
UsrCfg.InternoAtisNames.IP_SERVERS.COMPANY_NAME.TableName = "IP_SERVERS"; 
UsrCfg.InternoAtisNames.IP_SERVERS.COMPANY_NAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.IP_SERVERS.COMPANY_NAME.Size = 150; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.IP_SERVERS.COMPANY_NAME);
UsrCfg.InternoAtisNames.IP_SERVERS.NAME_TABLE = "IP_SERVERS"; 
UsrCfg.InternoAtisNames.IP_SERVERS.IP_MIRROR.FieldName = "IP_MIRROR"; 
UsrCfg.InternoAtisNames.IP_SERVERS.IP_MIRROR.TableName = "IP_SERVERS"; 
UsrCfg.InternoAtisNames.IP_SERVERS.IP_MIRROR.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.IP_SERVERS.IP_MIRROR.Size = 150; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.IP_SERVERS.IP_MIRROR);
UsrCfg.InternoAtisNames.IP_SERVERS.NAME_TABLE = "IP_SERVERS"; 
UsrCfg.InternoAtisNames.IP_SERVERS.IP_SECURE_SERVER.FieldName = "IP_SECURE_SERVER"; 
UsrCfg.InternoAtisNames.IP_SERVERS.IP_SECURE_SERVER.TableName = "IP_SERVERS"; 
UsrCfg.InternoAtisNames.IP_SERVERS.IP_SECURE_SERVER.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.IP_SERVERS.IP_SECURE_SERVER.Size = 150; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.IP_SERVERS.IP_SECURE_SERVER);
UsrCfg.InternoAtisNames.IP_SERVERS.NAME_TABLE = "IP_SERVERS"; 
UsrCfg.InternoAtisNames.IP_SERVERS.IP_CONTROLR.FieldName = "IP_CONTROLR"; 
UsrCfg.InternoAtisNames.IP_SERVERS.IP_CONTROLR.TableName = "IP_SERVERS"; 
UsrCfg.InternoAtisNames.IP_SERVERS.IP_CONTROLR.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.IP_SERVERS.IP_CONTROLR.Size = 150; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.IP_SERVERS.IP_CONTROLR);
UsrCfg.InternoAtisNames.RHGROUPCR.NAME_TABLE = "RHGROUPCR"; 
UsrCfg.InternoAtisNames.RHGROUPCR.IDRHGROUPCR.FieldName = "IDRHGROUPCR"; 
UsrCfg.InternoAtisNames.RHGROUPCR.IDRHGROUPCR.TableName = "RHGROUPCR"; 
UsrCfg.InternoAtisNames.RHGROUPCR.IDRHGROUPCR.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.RHGROUPCR.IDRHGROUPCR.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHGROUPCR.IDRHGROUPCR);
UsrCfg.InternoAtisNames.RHGROUPCR.NAME_TABLE = "RHGROUPCR"; 
UsrCfg.InternoAtisNames.RHGROUPCR.RHGROUPCR_NAME.FieldName = "RHGROUPCR_NAME"; 
UsrCfg.InternoAtisNames.RHGROUPCR.RHGROUPCR_NAME.TableName = "RHGROUPCR"; 
UsrCfg.InternoAtisNames.RHGROUPCR.RHGROUPCR_NAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.RHGROUPCR.RHGROUPCR_NAME.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHGROUPCR.RHGROUPCR_NAME);
UsrCfg.InternoAtisNames.RHGROUPCR.NAME_TABLE = "RHGROUPCR"; 
UsrCfg.InternoAtisNames.RHGROUPCR.RHGROUPCR_DESCRIPTION.FieldName = "RHGROUPCR_DESCRIPTION"; 
UsrCfg.InternoAtisNames.RHGROUPCR.RHGROUPCR_DESCRIPTION.TableName = "RHGROUPCR"; 
UsrCfg.InternoAtisNames.RHGROUPCR.RHGROUPCR_DESCRIPTION.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.RHGROUPCR.RHGROUPCR_DESCRIPTION.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHGROUPCR.RHGROUPCR_DESCRIPTION);
UsrCfg.InternoAtisNames.RHGROUPCRCPU.NAME_TABLE = "RHGROUPCRCPU"; 
UsrCfg.InternoAtisNames.RHGROUPCRCPU.IDRHGROUPCRCPU.FieldName = "IDRHGROUPCRCPU"; 
UsrCfg.InternoAtisNames.RHGROUPCRCPU.IDRHGROUPCRCPU.TableName = "RHGROUPCRCPU"; 
UsrCfg.InternoAtisNames.RHGROUPCRCPU.IDRHGROUPCRCPU.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.RHGROUPCRCPU.IDRHGROUPCRCPU.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHGROUPCRCPU.IDRHGROUPCRCPU);
UsrCfg.InternoAtisNames.RHGROUPCRCPU.NAME_TABLE = "RHGROUPCRCPU"; 
UsrCfg.InternoAtisNames.RHGROUPCRCPU.IDCPU.FieldName = "IDCPU"; 
UsrCfg.InternoAtisNames.RHGROUPCRCPU.IDCPU.TableName = "RHGROUPCRCPU"; 
UsrCfg.InternoAtisNames.RHGROUPCRCPU.IDCPU.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.RHGROUPCRCPU.IDCPU.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHGROUPCRCPU.IDCPU);
UsrCfg.InternoAtisNames.RHGROUPCRCPU.NAME_TABLE = "RHGROUPCRCPU"; 
UsrCfg.InternoAtisNames.RHGROUPCRCPU.IDRHGROUPCR.FieldName = "IDRHGROUPCR"; 
UsrCfg.InternoAtisNames.RHGROUPCRCPU.IDRHGROUPCR.TableName = "RHGROUPCRCPU"; 
UsrCfg.InternoAtisNames.RHGROUPCRCPU.IDRHGROUPCR.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.RHGROUPCRCPU.IDRHGROUPCR.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHGROUPCRCPU.IDRHGROUPCR);
UsrCfg.InternoAtisNames.RHGROUPCRPERMISSION.NAME_TABLE = "RHGROUPCRPERMISSION"; 
UsrCfg.InternoAtisNames.RHGROUPCRPERMISSION.IDRHGROUPCRPERMISSION.FieldName = "IDRHGROUPCRPERMISSION"; 
UsrCfg.InternoAtisNames.RHGROUPCRPERMISSION.IDRHGROUPCRPERMISSION.TableName = "RHGROUPCRPERMISSION"; 
UsrCfg.InternoAtisNames.RHGROUPCRPERMISSION.IDRHGROUPCRPERMISSION.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.RHGROUPCRPERMISSION.IDRHGROUPCRPERMISSION.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHGROUPCRPERMISSION.IDRHGROUPCRPERMISSION);
UsrCfg.InternoAtisNames.RHGROUPCRPERMISSION.NAME_TABLE = "RHGROUPCRPERMISSION"; 
UsrCfg.InternoAtisNames.RHGROUPCRPERMISSION.PERMISSION.FieldName = "PERMISSION"; 
UsrCfg.InternoAtisNames.RHGROUPCRPERMISSION.PERMISSION.TableName = "RHGROUPCRPERMISSION"; 
UsrCfg.InternoAtisNames.RHGROUPCRPERMISSION.PERMISSION.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.RHGROUPCRPERMISSION.PERMISSION.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHGROUPCRPERMISSION.PERMISSION);
UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.NAME_TABLE = "RHGROUPCRATROLEPERMISSION"; 
UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDRHGROUPCRATROLEPERMISSION.FieldName = "IDRHGROUPCRATROLEPERMISSION"; 
UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDRHGROUPCRATROLEPERMISSION.TableName = "RHGROUPCRATROLEPERMISSION"; 
UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDRHGROUPCRATROLEPERMISSION.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDRHGROUPCRATROLEPERMISSION.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDRHGROUPCRATROLEPERMISSION);
UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.NAME_TABLE = "RHGROUPCRATROLEPERMISSION"; 
UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDATROLE.FieldName = "IDATROLE"; 
UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDATROLE.TableName = "RHGROUPCRATROLEPERMISSION"; 
UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDATROLE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDATROLE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDATROLE);
UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.NAME_TABLE = "RHGROUPCRATROLEPERMISSION"; 
UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDRHGROUPCR.FieldName = "IDRHGROUPCR"; 
UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDRHGROUPCR.TableName = "RHGROUPCRATROLEPERMISSION"; 
UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDRHGROUPCR.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDRHGROUPCR.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDRHGROUPCR);
UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.NAME_TABLE = "RHGROUPCRATROLEPERMISSION"; 
UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDRHGROUPCRPERMISSION.FieldName = "IDRHGROUPCRPERMISSION"; 
UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDRHGROUPCRPERMISSION.TableName = "RHGROUPCRATROLEPERMISSION"; 
UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDRHGROUPCRPERMISSION.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDRHGROUPCRPERMISSION.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHGROUPCRATROLEPERMISSION.IDRHGROUPCRPERMISSION);
UsrCfg.InternoAtisNames.RHGROUPPL_POLICY.NAME_TABLE = "RHGROUPPL_POLICY"; 
UsrCfg.InternoAtisNames.RHGROUPPL_POLICY.IDRHGROUPPL_POLICY.FieldName = "IDRHGROUPPL_POLICY"; 
UsrCfg.InternoAtisNames.RHGROUPPL_POLICY.IDRHGROUPPL_POLICY.TableName = "RHGROUPPL_POLICY"; 
UsrCfg.InternoAtisNames.RHGROUPPL_POLICY.IDRHGROUPPL_POLICY.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.RHGROUPPL_POLICY.IDRHGROUPPL_POLICY.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHGROUPPL_POLICY.IDRHGROUPPL_POLICY);
UsrCfg.InternoAtisNames.RHGROUPPL_POLICY.NAME_TABLE = "RHGROUPPL_POLICY"; 
UsrCfg.InternoAtisNames.RHGROUPPL_POLICY.POLICY_NAME.FieldName = "POLICY_NAME"; 
UsrCfg.InternoAtisNames.RHGROUPPL_POLICY.POLICY_NAME.TableName = "RHGROUPPL_POLICY"; 
UsrCfg.InternoAtisNames.RHGROUPPL_POLICY.POLICY_NAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.RHGROUPPL_POLICY.POLICY_NAME.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHGROUPPL_POLICY.POLICY_NAME);
UsrCfg.InternoAtisNames.RHGROUPPL_CLI_CONFIG.NAME_TABLE = "RHGROUPPL_CLI_CONFIG"; 
UsrCfg.InternoAtisNames.RHGROUPPL_CLI_CONFIG.IDRHGROUPPL_CLI_CONFIG.FieldName = "IDRHGROUPPL_CLI_CONFIG"; 
UsrCfg.InternoAtisNames.RHGROUPPL_CLI_CONFIG.IDRHGROUPPL_CLI_CONFIG.TableName = "RHGROUPPL_CLI_CONFIG"; 
UsrCfg.InternoAtisNames.RHGROUPPL_CLI_CONFIG.IDRHGROUPPL_CLI_CONFIG.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.RHGROUPPL_CLI_CONFIG.IDRHGROUPPL_CLI_CONFIG.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHGROUPPL_CLI_CONFIG.IDRHGROUPPL_CLI_CONFIG);
UsrCfg.InternoAtisNames.RHGROUPPL_CLI_CONFIG.NAME_TABLE = "RHGROUPPL_CLI_CONFIG"; 
UsrCfg.InternoAtisNames.RHGROUPPL_CLI_CONFIG.CLI_CONFIG_NAME.FieldName = "CLI_CONFIG_NAME"; 
UsrCfg.InternoAtisNames.RHGROUPPL_CLI_CONFIG.CLI_CONFIG_NAME.TableName = "RHGROUPPL_CLI_CONFIG"; 
UsrCfg.InternoAtisNames.RHGROUPPL_CLI_CONFIG.CLI_CONFIG_NAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.RHGROUPPL_CLI_CONFIG.CLI_CONFIG_NAME.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHGROUPPL_CLI_CONFIG.CLI_CONFIG_NAME);
UsrCfg.InternoAtisNames.RHGROUPPL_CFGBACKUP.NAME_TABLE = "RHGROUPPL_CFGBACKUP"; 
UsrCfg.InternoAtisNames.RHGROUPPL_CFGBACKUP.IDRHGROUPPL_CFGBACKUP.FieldName = "IDRHGROUPPL_CFGBACKUP"; 
UsrCfg.InternoAtisNames.RHGROUPPL_CFGBACKUP.IDRHGROUPPL_CFGBACKUP.TableName = "RHGROUPPL_CFGBACKUP"; 
UsrCfg.InternoAtisNames.RHGROUPPL_CFGBACKUP.IDRHGROUPPL_CFGBACKUP.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.RHGROUPPL_CFGBACKUP.IDRHGROUPPL_CFGBACKUP.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHGROUPPL_CFGBACKUP.IDRHGROUPPL_CFGBACKUP);
UsrCfg.InternoAtisNames.RHGROUPPL_CFGBACKUP.NAME_TABLE = "RHGROUPPL_CFGBACKUP"; 
UsrCfg.InternoAtisNames.RHGROUPPL_CFGBACKUP.CFGBACKUP_NAME.FieldName = "CFGBACKUP_NAME"; 
UsrCfg.InternoAtisNames.RHGROUPPL_CFGBACKUP.CFGBACKUP_NAME.TableName = "RHGROUPPL_CFGBACKUP"; 
UsrCfg.InternoAtisNames.RHGROUPPL_CFGBACKUP.CFGBACKUP_NAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.RHGROUPPL_CFGBACKUP.CFGBACKUP_NAME.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHGROUPPL_CFGBACKUP.CFGBACKUP_NAME);
UsrCfg.InternoAtisNames.RHGROUPPL.NAME_TABLE = "RHGROUPPL"; 
UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL.FieldName = "IDRHGROUPPL"; 
UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL.TableName = "RHGROUPPL"; 
UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL);
UsrCfg.InternoAtisNames.RHGROUPPL.NAME_TABLE = "RHGROUPPL"; 
UsrCfg.InternoAtisNames.RHGROUPPL.GROUPPL_NAME.FieldName = "GROUPPL_NAME"; 
UsrCfg.InternoAtisNames.RHGROUPPL.GROUPPL_NAME.TableName = "RHGROUPPL"; 
UsrCfg.InternoAtisNames.RHGROUPPL.GROUPPL_NAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.RHGROUPPL.GROUPPL_NAME.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHGROUPPL.GROUPPL_NAME);
UsrCfg.InternoAtisNames.RHGROUPPL.NAME_TABLE = "RHGROUPPL"; 
UsrCfg.InternoAtisNames.RHGROUPPL.GROUPPL_DESCRIPTION.FieldName = "GROUPPL_DESCRIPTION"; 
UsrCfg.InternoAtisNames.RHGROUPPL.GROUPPL_DESCRIPTION.TableName = "RHGROUPPL"; 
UsrCfg.InternoAtisNames.RHGROUPPL.GROUPPL_DESCRIPTION.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.RHGROUPPL.GROUPPL_DESCRIPTION.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHGROUPPL.GROUPPL_DESCRIPTION);
UsrCfg.InternoAtisNames.RHGROUPPL.NAME_TABLE = "RHGROUPPL"; 
UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL_POLICY.FieldName = "IDRHGROUPPL_POLICY"; 
UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL_POLICY.TableName = "RHGROUPPL"; 
UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL_POLICY.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL_POLICY.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL_POLICY);
UsrCfg.InternoAtisNames.RHGROUPPL.NAME_TABLE = "RHGROUPPL"; 
UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL_CLI_CONFIG.FieldName = "IDRHGROUPPL_CLI_CONFIG"; 
UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL_CLI_CONFIG.TableName = "RHGROUPPL"; 
UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL_CLI_CONFIG.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL_CLI_CONFIG.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL_CLI_CONFIG);
UsrCfg.InternoAtisNames.RHGROUPPL.NAME_TABLE = "RHGROUPPL"; 
UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL_CFGBACKUP.FieldName = "IDRHGROUPPL_CFGBACKUP"; 
UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL_CFGBACKUP.TableName = "RHGROUPPL"; 
UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL_CFGBACKUP.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL_CFGBACKUP.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHGROUPPL.IDRHGROUPPL_CFGBACKUP);
UsrCfg.InternoAtisNames.RHGROUPPLCPU.NAME_TABLE = "RHGROUPPLCPU"; 
UsrCfg.InternoAtisNames.RHGROUPPLCPU.IDRHGROUPPLCPU.FieldName = "IDRHGROUPPLCPU"; 
UsrCfg.InternoAtisNames.RHGROUPPLCPU.IDRHGROUPPLCPU.TableName = "RHGROUPPLCPU"; 
UsrCfg.InternoAtisNames.RHGROUPPLCPU.IDRHGROUPPLCPU.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.RHGROUPPLCPU.IDRHGROUPPLCPU.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHGROUPPLCPU.IDRHGROUPPLCPU);
UsrCfg.InternoAtisNames.RHGROUPPLCPU.NAME_TABLE = "RHGROUPPLCPU"; 
UsrCfg.InternoAtisNames.RHGROUPPLCPU.IDCPU.FieldName = "IDCPU"; 
UsrCfg.InternoAtisNames.RHGROUPPLCPU.IDCPU.TableName = "RHGROUPPLCPU"; 
UsrCfg.InternoAtisNames.RHGROUPPLCPU.IDCPU.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.RHGROUPPLCPU.IDCPU.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHGROUPPLCPU.IDCPU);
UsrCfg.InternoAtisNames.RHGROUPPLCPU.NAME_TABLE = "RHGROUPPLCPU"; 
UsrCfg.InternoAtisNames.RHGROUPPLCPU.IDRHGROUPPL.FieldName = "IDRHGROUPPL"; 
UsrCfg.InternoAtisNames.RHGROUPPLCPU.IDRHGROUPPL.TableName = "RHGROUPPLCPU"; 
UsrCfg.InternoAtisNames.RHGROUPPLCPU.IDRHGROUPPL.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.RHGROUPPLCPU.IDRHGROUPPL.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHGROUPPLCPU.IDRHGROUPPL);
UsrCfg.InternoAtisNames.RHGROUPPLATROLE.NAME_TABLE = "RHGROUPPLATROLE"; 
UsrCfg.InternoAtisNames.RHGROUPPLATROLE.IDRHGROUPPLATROLE.FieldName = "IDRHGROUPPLATROLE"; 
UsrCfg.InternoAtisNames.RHGROUPPLATROLE.IDRHGROUPPLATROLE.TableName = "RHGROUPPLATROLE"; 
UsrCfg.InternoAtisNames.RHGROUPPLATROLE.IDRHGROUPPLATROLE.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.RHGROUPPLATROLE.IDRHGROUPPLATROLE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHGROUPPLATROLE.IDRHGROUPPLATROLE);
UsrCfg.InternoAtisNames.RHGROUPPLATROLE.NAME_TABLE = "RHGROUPPLATROLE"; 
UsrCfg.InternoAtisNames.RHGROUPPLATROLE.IDATROLE.FieldName = "IDATROLE"; 
UsrCfg.InternoAtisNames.RHGROUPPLATROLE.IDATROLE.TableName = "RHGROUPPLATROLE"; 
UsrCfg.InternoAtisNames.RHGROUPPLATROLE.IDATROLE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.RHGROUPPLATROLE.IDATROLE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHGROUPPLATROLE.IDATROLE);
UsrCfg.InternoAtisNames.RHGROUPPLATROLE.NAME_TABLE = "RHGROUPPLATROLE"; 
UsrCfg.InternoAtisNames.RHGROUPPLATROLE.IDRHGROUPPL.FieldName = "IDRHGROUPPL"; 
UsrCfg.InternoAtisNames.RHGROUPPLATROLE.IDRHGROUPPL.TableName = "RHGROUPPLATROLE"; 
UsrCfg.InternoAtisNames.RHGROUPPLATROLE.IDRHGROUPPL.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.RHGROUPPLATROLE.IDRHGROUPPL.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHGROUPPLATROLE.IDRHGROUPPL);
UsrCfg.InternoAtisNames.RHREQUEST.NAME_TABLE = "RHREQUEST"; 
UsrCfg.InternoAtisNames.RHREQUEST.IDRHREQUEST.FieldName = "IDRHREQUEST"; 
UsrCfg.InternoAtisNames.RHREQUEST.IDRHREQUEST.TableName = "RHREQUEST"; 
UsrCfg.InternoAtisNames.RHREQUEST.IDRHREQUEST.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.RHREQUEST.IDRHREQUEST.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHREQUEST.IDRHREQUEST);
UsrCfg.InternoAtisNames.RHREQUEST.NAME_TABLE = "RHREQUEST"; 
UsrCfg.InternoAtisNames.RHREQUEST.IDATROLE.FieldName = "IDATROLE"; 
UsrCfg.InternoAtisNames.RHREQUEST.IDATROLE.TableName = "RHREQUEST"; 
UsrCfg.InternoAtisNames.RHREQUEST.IDATROLE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.RHREQUEST.IDATROLE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHREQUEST.IDATROLE);
UsrCfg.InternoAtisNames.RHREQUEST.NAME_TABLE = "RHREQUEST"; 
UsrCfg.InternoAtisNames.RHREQUEST.IDCMDBCI.FieldName = "IDCMDBCI"; 
UsrCfg.InternoAtisNames.RHREQUEST.IDCMDBCI.TableName = "RHREQUEST"; 
UsrCfg.InternoAtisNames.RHREQUEST.IDCMDBCI.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.RHREQUEST.IDCMDBCI.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHREQUEST.IDCMDBCI);
UsrCfg.InternoAtisNames.RHREQUEST.NAME_TABLE = "RHREQUEST"; 
UsrCfg.InternoAtisNames.RHREQUEST.IDCMDBUSER.FieldName = "IDCMDBUSER"; 
UsrCfg.InternoAtisNames.RHREQUEST.IDCMDBUSER.TableName = "RHREQUEST"; 
UsrCfg.InternoAtisNames.RHREQUEST.IDCMDBUSER.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.RHREQUEST.IDCMDBUSER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHREQUEST.IDCMDBUSER);
UsrCfg.InternoAtisNames.RHREQUEST.NAME_TABLE = "RHREQUEST"; 
UsrCfg.InternoAtisNames.RHREQUEST.EVENTDATE.FieldName = "EVENTDATE"; 
UsrCfg.InternoAtisNames.RHREQUEST.EVENTDATE.TableName = "RHREQUEST"; 
UsrCfg.InternoAtisNames.RHREQUEST.EVENTDATE.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.RHREQUEST.EVENTDATE.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHREQUEST.EVENTDATE);
UsrCfg.InternoAtisNames.RHREQUEST.NAME_TABLE = "RHREQUEST"; 
UsrCfg.InternoAtisNames.RHREQUEST.REMOTEHELPFUNTION.FieldName = "REMOTEHELPFUNTION"; 
UsrCfg.InternoAtisNames.RHREQUEST.REMOTEHELPFUNTION.TableName = "RHREQUEST"; 
UsrCfg.InternoAtisNames.RHREQUEST.REMOTEHELPFUNTION.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.RHREQUEST.REMOTEHELPFUNTION.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHREQUEST.REMOTEHELPFUNTION);
UsrCfg.InternoAtisNames.RHREQUEST.NAME_TABLE = "RHREQUEST"; 
UsrCfg.InternoAtisNames.RHREQUEST.AUX.FieldName = "AUX"; 
UsrCfg.InternoAtisNames.RHREQUEST.AUX.TableName = "RHREQUEST"; 
UsrCfg.InternoAtisNames.RHREQUEST.AUX.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.RHREQUEST.AUX.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHREQUEST.AUX);
UsrCfg.InternoAtisNames.RHREQUEST.NAME_TABLE = "RHREQUEST"; 
UsrCfg.InternoAtisNames.RHREQUEST.CONNECTBYHOST.FieldName = "CONNECTBYHOST"; 
UsrCfg.InternoAtisNames.RHREQUEST.CONNECTBYHOST.TableName = "RHREQUEST"; 
UsrCfg.InternoAtisNames.RHREQUEST.CONNECTBYHOST.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.RHREQUEST.CONNECTBYHOST.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHREQUEST.CONNECTBYHOST);
UsrCfg.InternoAtisNames.RHREQUEST.NAME_TABLE = "RHREQUEST"; 
UsrCfg.InternoAtisNames.RHREQUEST.INTERNETCONNECTION.FieldName = "INTERNETCONNECTION"; 
UsrCfg.InternoAtisNames.RHREQUEST.INTERNETCONNECTION.TableName = "RHREQUEST"; 
UsrCfg.InternoAtisNames.RHREQUEST.INTERNETCONNECTION.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.RHREQUEST.INTERNETCONNECTION.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHREQUEST.INTERNETCONNECTION);
UsrCfg.InternoAtisNames.RHREQUEST.NAME_TABLE = "RHREQUEST"; 
UsrCfg.InternoAtisNames.RHREQUEST.JUSTSEE.FieldName = "JUSTSEE"; 
UsrCfg.InternoAtisNames.RHREQUEST.JUSTSEE.TableName = "RHREQUEST"; 
UsrCfg.InternoAtisNames.RHREQUEST.JUSTSEE.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.RHREQUEST.JUSTSEE.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHREQUEST.JUSTSEE);
UsrCfg.InternoAtisNames.RHREQUEST.NAME_TABLE = "RHREQUEST"; 
UsrCfg.InternoAtisNames.RHREQUEST.DONOTASKFORPERMISSION.FieldName = "DONOTASKFORPERMISSION"; 
UsrCfg.InternoAtisNames.RHREQUEST.DONOTASKFORPERMISSION.TableName = "RHREQUEST"; 
UsrCfg.InternoAtisNames.RHREQUEST.DONOTASKFORPERMISSION.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.RHREQUEST.DONOTASKFORPERMISSION.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHREQUEST.DONOTASKFORPERMISSION);
UsrCfg.InternoAtisNames.RHREQUEST.NAME_TABLE = "RHREQUEST"; 
UsrCfg.InternoAtisNames.RHREQUEST.LOCKKEYBOARD.FieldName = "LOCKKEYBOARD"; 
UsrCfg.InternoAtisNames.RHREQUEST.LOCKKEYBOARD.TableName = "RHREQUEST"; 
UsrCfg.InternoAtisNames.RHREQUEST.LOCKKEYBOARD.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.RHREQUEST.LOCKKEYBOARD.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHREQUEST.LOCKKEYBOARD);
UsrCfg.InternoAtisNames.RHREQUEST.NAME_TABLE = "RHREQUEST"; 
UsrCfg.InternoAtisNames.RHREQUEST.LOWBANDWIDTH.FieldName = "LOWBANDWIDTH"; 
UsrCfg.InternoAtisNames.RHREQUEST.LOWBANDWIDTH.TableName = "RHREQUEST"; 
UsrCfg.InternoAtisNames.RHREQUEST.LOWBANDWIDTH.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.RHREQUEST.LOWBANDWIDTH.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHREQUEST.LOWBANDWIDTH);
UsrCfg.InternoAtisNames.RHREQUEST.NAME_TABLE = "RHREQUEST"; 
UsrCfg.InternoAtisNames.RHREQUEST.RECORDSESSION.FieldName = "RECORDSESSION"; 
UsrCfg.InternoAtisNames.RHREQUEST.RECORDSESSION.TableName = "RHREQUEST"; 
UsrCfg.InternoAtisNames.RHREQUEST.RECORDSESSION.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.RHREQUEST.RECORDSESSION.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHREQUEST.RECORDSESSION);
UsrCfg.InternoAtisNames.RHREQUEST.NAME_TABLE = "RHREQUEST"; 
UsrCfg.InternoAtisNames.RHREQUEST.REQUEST_STATUS.FieldName = "REQUEST_STATUS"; 
UsrCfg.InternoAtisNames.RHREQUEST.REQUEST_STATUS.TableName = "RHREQUEST"; 
UsrCfg.InternoAtisNames.RHREQUEST.REQUEST_STATUS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.RHREQUEST.REQUEST_STATUS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHREQUEST.REQUEST_STATUS);
UsrCfg.InternoAtisNames.RHREQUEST.NAME_TABLE = "RHREQUEST"; 
UsrCfg.InternoAtisNames.RHREQUEST.IDRHMODULE_EXECUTE.FieldName = "IDRHMODULE_EXECUTE"; 
UsrCfg.InternoAtisNames.RHREQUEST.IDRHMODULE_EXECUTE.TableName = "RHREQUEST"; 
UsrCfg.InternoAtisNames.RHREQUEST.IDRHMODULE_EXECUTE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.RHREQUEST.IDRHMODULE_EXECUTE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHREQUEST.IDRHMODULE_EXECUTE);
UsrCfg.InternoAtisNames.RHREQUEST.NAME_TABLE = "RHREQUEST"; 
UsrCfg.InternoAtisNames.RHREQUEST.IDRHMODULE_CREATE.FieldName = "IDRHMODULE_CREATE"; 
UsrCfg.InternoAtisNames.RHREQUEST.IDRHMODULE_CREATE.TableName = "RHREQUEST"; 
UsrCfg.InternoAtisNames.RHREQUEST.IDRHMODULE_CREATE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.RHREQUEST.IDRHMODULE_CREATE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHREQUEST.IDRHMODULE_CREATE);
UsrCfg.InternoAtisNames.RHREQUEST.NAME_TABLE = "RHREQUEST"; 
UsrCfg.InternoAtisNames.RHREQUEST.IDRHTYPEEXECUTE.FieldName = "IDRHTYPEEXECUTE"; 
UsrCfg.InternoAtisNames.RHREQUEST.IDRHTYPEEXECUTE.TableName = "RHREQUEST"; 
UsrCfg.InternoAtisNames.RHREQUEST.IDRHTYPEEXECUTE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.RHREQUEST.IDRHTYPEEXECUTE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHREQUEST.IDRHTYPEEXECUTE);
UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.NAME_TABLE = "RHREQUEST_CPUPL"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.IDRHREQUEST_CPUPL.FieldName = "IDRHREQUEST_CPUPL"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.IDRHREQUEST_CPUPL.TableName = "RHREQUEST_CPUPL"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.IDRHREQUEST_CPUPL.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.IDRHREQUEST_CPUPL.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.IDRHREQUEST_CPUPL);
UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.NAME_TABLE = "RHREQUEST_CPUPL"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.IDCPU.FieldName = "IDCPU"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.IDCPU.TableName = "RHREQUEST_CPUPL"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.IDCPU.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.IDCPU.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.IDCPU);
UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.NAME_TABLE = "RHREQUEST_CPUPL"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.IDRHREQUEST.FieldName = "IDRHREQUEST"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.IDRHREQUEST.TableName = "RHREQUEST_CPUPL"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.IDRHREQUEST.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.IDRHREQUEST.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.IDRHREQUEST);
UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.NAME_TABLE = "RHREQUEST_CPUPL"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.HOSTLIST.FieldName = "HOSTLIST"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.HOSTLIST.TableName = "RHREQUEST_CPUPL"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.HOSTLIST.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.HOSTLIST.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.HOSTLIST);
UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.NAME_TABLE = "RHREQUEST_CPUPL"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.HOSTVALID.FieldName = "HOSTVALID"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.HOSTVALID.TableName = "RHREQUEST_CPUPL"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.HOSTVALID.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.HOSTVALID.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.HOSTVALID);
UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.NAME_TABLE = "RHREQUEST_CPUPL"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.REQUEST_CPUPL_STATUS.FieldName = "REQUEST_CPUPL_STATUS"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.REQUEST_CPUPL_STATUS.TableName = "RHREQUEST_CPUPL"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.REQUEST_CPUPL_STATUS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.REQUEST_CPUPL_STATUS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.REQUEST_CPUPL_STATUS);
UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.NAME_TABLE = "RHREQUEST_CPUPL"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.IDRHGROUPPL.FieldName = "IDRHGROUPPL"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.IDRHGROUPPL.TableName = "RHREQUEST_CPUPL"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.IDRHGROUPPL.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.IDRHGROUPPL.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.IDRHGROUPPL);
UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.NAME_TABLE = "RHREQUEST_CPUPL"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.PERMISSION.FieldName = "PERMISSION"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.PERMISSION.TableName = "RHREQUEST_CPUPL"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.PERMISSION.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.PERMISSION.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHREQUEST_CPUPL.PERMISSION);
UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.NAME_TABLE = "RHREQUEST_CPUCR"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDRHREQUEST_CPUCR.FieldName = "IDRHREQUEST_CPUCR"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDRHREQUEST_CPUCR.TableName = "RHREQUEST_CPUCR"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDRHREQUEST_CPUCR.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDRHREQUEST_CPUCR.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDRHREQUEST_CPUCR);
UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.NAME_TABLE = "RHREQUEST_CPUCR"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDCPU.FieldName = "IDCPU"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDCPU.TableName = "RHREQUEST_CPUCR"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDCPU.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDCPU.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDCPU);
UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.NAME_TABLE = "RHREQUEST_CPUCR"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDRHREQUEST.FieldName = "IDRHREQUEST"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDRHREQUEST.TableName = "RHREQUEST_CPUCR"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDRHREQUEST.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDRHREQUEST.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDRHREQUEST);
UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.NAME_TABLE = "RHREQUEST_CPUCR"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.HOSTLIST.FieldName = "HOSTLIST"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.HOSTLIST.TableName = "RHREQUEST_CPUCR"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.HOSTLIST.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.HOSTLIST.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.HOSTLIST);
UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.NAME_TABLE = "RHREQUEST_CPUCR"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.HOSTVALID.FieldName = "HOSTVALID"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.HOSTVALID.TableName = "RHREQUEST_CPUCR"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.HOSTVALID.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.HOSTVALID.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.HOSTVALID);
UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.NAME_TABLE = "RHREQUEST_CPUCR"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.REQUEST_CPUCR_STATUS.FieldName = "REQUEST_CPUCR_STATUS"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.REQUEST_CPUCR_STATUS.TableName = "RHREQUEST_CPUCR"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.REQUEST_CPUCR_STATUS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.REQUEST_CPUCR_STATUS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.REQUEST_CPUCR_STATUS);
UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.NAME_TABLE = "RHREQUEST_CPUCR"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDRHGROUPCR.FieldName = "IDRHGROUPCR"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDRHGROUPCR.TableName = "RHREQUEST_CPUCR"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDRHGROUPCR.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDRHGROUPCR.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDRHGROUPCR);
UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.NAME_TABLE = "RHREQUEST_CPUCR"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDRHGROUPCRPERMISSION.FieldName = "IDRHGROUPCRPERMISSION"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDRHGROUPCRPERMISSION.TableName = "RHREQUEST_CPUCR"; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDRHGROUPCRPERMISSION.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDRHGROUPCRPERMISSION.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.RHREQUEST_CPUCR.IDRHGROUPCRPERMISSION);
UsrCfg.InternoAtisNames.MDCALENDARYGROUPTYPE.NAME_TABLE = "MDCALENDARYGROUPTYPE"; 
UsrCfg.InternoAtisNames.MDCALENDARYGROUPTYPE.IDMDCALENDARYGROUPTYPE.FieldName = "IDMDCALENDARYGROUPTYPE"; 
UsrCfg.InternoAtisNames.MDCALENDARYGROUPTYPE.IDMDCALENDARYGROUPTYPE.TableName = "MDCALENDARYGROUPTYPE"; 
UsrCfg.InternoAtisNames.MDCALENDARYGROUPTYPE.IDMDCALENDARYGROUPTYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDCALENDARYGROUPTYPE.IDMDCALENDARYGROUPTYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCALENDARYGROUPTYPE.IDMDCALENDARYGROUPTYPE);
UsrCfg.InternoAtisNames.MDCALENDARYGROUPTYPE.NAME_TABLE = "MDCALENDARYGROUPTYPE"; 
UsrCfg.InternoAtisNames.MDCALENDARYGROUPTYPE.GROUPTYPEPNAME.FieldName = "GROUPTYPEPNAME"; 
UsrCfg.InternoAtisNames.MDCALENDARYGROUPTYPE.GROUPTYPEPNAME.TableName = "MDCALENDARYGROUPTYPE"; 
UsrCfg.InternoAtisNames.MDCALENDARYGROUPTYPE.GROUPTYPEPNAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.MDCALENDARYGROUPTYPE.GROUPTYPEPNAME.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCALENDARYGROUPTYPE.GROUPTYPEPNAME);
UsrCfg.InternoAtisNames.MDCALENDARYDATERULE.NAME_TABLE = "MDCALENDARYDATERULE"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATERULE.IDMDCALENDARYDATERULE.FieldName = "IDMDCALENDARYDATERULE"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATERULE.IDMDCALENDARYDATERULE.TableName = "MDCALENDARYDATERULE"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATERULE.IDMDCALENDARYDATERULE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDCALENDARYDATERULE.IDMDCALENDARYDATERULE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCALENDARYDATERULE.IDMDCALENDARYDATERULE);
UsrCfg.InternoAtisNames.MDCALENDARYDATERULE.NAME_TABLE = "MDCALENDARYDATERULE"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATERULE.RULENAME.FieldName = "RULENAME"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATERULE.RULENAME.TableName = "MDCALENDARYDATERULE"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATERULE.RULENAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.MDCALENDARYDATERULE.RULENAME.Size = 255; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCALENDARYDATERULE.RULENAME);
UsrCfg.InternoAtisNames.MDCALENDARYDATERULE.NAME_TABLE = "MDCALENDARYDATERULE"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATERULE.RULEDESCRIPTION.FieldName = "RULEDESCRIPTION"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATERULE.RULEDESCRIPTION.TableName = "MDCALENDARYDATERULE"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATERULE.RULEDESCRIPTION.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.MDCALENDARYDATERULE.RULEDESCRIPTION.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCALENDARYDATERULE.RULEDESCRIPTION);
UsrCfg.InternoAtisNames.MDCALENDARYTIME.NAME_TABLE = "MDCALENDARYTIME"; 
UsrCfg.InternoAtisNames.MDCALENDARYTIME.IDMDCALENDARYTIME.FieldName = "IDMDCALENDARYTIME"; 
UsrCfg.InternoAtisNames.MDCALENDARYTIME.IDMDCALENDARYTIME.TableName = "MDCALENDARYTIME"; 
UsrCfg.InternoAtisNames.MDCALENDARYTIME.IDMDCALENDARYTIME.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.MDCALENDARYTIME.IDMDCALENDARYTIME.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCALENDARYTIME.IDMDCALENDARYTIME);
UsrCfg.InternoAtisNames.MDCALENDARYTIME.NAME_TABLE = "MDCALENDARYTIME"; 
UsrCfg.InternoAtisNames.MDCALENDARYTIME.TIMESTATUS.FieldName = "TIMESTATUS"; 
UsrCfg.InternoAtisNames.MDCALENDARYTIME.TIMESTATUS.TableName = "MDCALENDARYTIME"; 
UsrCfg.InternoAtisNames.MDCALENDARYTIME.TIMESTATUS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDCALENDARYTIME.TIMESTATUS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCALENDARYTIME.TIMESTATUS);
UsrCfg.InternoAtisNames.MDCALENDARYTIME.NAME_TABLE = "MDCALENDARYTIME"; 
UsrCfg.InternoAtisNames.MDCALENDARYTIME.TIMENAME.FieldName = "TIMENAME"; 
UsrCfg.InternoAtisNames.MDCALENDARYTIME.TIMENAME.TableName = "MDCALENDARYTIME"; 
UsrCfg.InternoAtisNames.MDCALENDARYTIME.TIMENAME.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.MDCALENDARYTIME.TIMENAME.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCALENDARYTIME.TIMENAME);
UsrCfg.InternoAtisNames.MDCALENDARYTIMEDETAIL.NAME_TABLE = "MDCALENDARYTIMEDETAIL"; 
UsrCfg.InternoAtisNames.MDCALENDARYTIMEDETAIL.IDMDCALENDARYTIMEDETAIL.FieldName = "IDMDCALENDARYTIMEDETAIL"; 
UsrCfg.InternoAtisNames.MDCALENDARYTIMEDETAIL.IDMDCALENDARYTIMEDETAIL.TableName = "MDCALENDARYTIMEDETAIL"; 
UsrCfg.InternoAtisNames.MDCALENDARYTIMEDETAIL.IDMDCALENDARYTIMEDETAIL.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.MDCALENDARYTIMEDETAIL.IDMDCALENDARYTIMEDETAIL.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCALENDARYTIMEDETAIL.IDMDCALENDARYTIMEDETAIL);
UsrCfg.InternoAtisNames.MDCALENDARYTIMEDETAIL.NAME_TABLE = "MDCALENDARYTIMEDETAIL"; 
UsrCfg.InternoAtisNames.MDCALENDARYTIMEDETAIL.IDMDCALENDARYTIME.FieldName = "IDMDCALENDARYTIME"; 
UsrCfg.InternoAtisNames.MDCALENDARYTIMEDETAIL.IDMDCALENDARYTIME.TableName = "MDCALENDARYTIMEDETAIL"; 
UsrCfg.InternoAtisNames.MDCALENDARYTIMEDETAIL.IDMDCALENDARYTIME.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDCALENDARYTIMEDETAIL.IDMDCALENDARYTIME.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCALENDARYTIMEDETAIL.IDMDCALENDARYTIME);
UsrCfg.InternoAtisNames.MDCALENDARYTIMEDETAIL.NAME_TABLE = "MDCALENDARYTIMEDETAIL"; 
UsrCfg.InternoAtisNames.MDCALENDARYTIMEDETAIL.TIMESTART.FieldName = "TIMESTART"; 
UsrCfg.InternoAtisNames.MDCALENDARYTIMEDETAIL.TIMESTART.TableName = "MDCALENDARYTIMEDETAIL"; 
UsrCfg.InternoAtisNames.MDCALENDARYTIMEDETAIL.TIMESTART.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.MDCALENDARYTIMEDETAIL.TIMESTART.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCALENDARYTIMEDETAIL.TIMESTART);
UsrCfg.InternoAtisNames.MDCALENDARYTIMEDETAIL.NAME_TABLE = "MDCALENDARYTIMEDETAIL"; 
UsrCfg.InternoAtisNames.MDCALENDARYTIMEDETAIL.TIMEEND.FieldName = "TIMEEND"; 
UsrCfg.InternoAtisNames.MDCALENDARYTIMEDETAIL.TIMEEND.TableName = "MDCALENDARYTIMEDETAIL"; 
UsrCfg.InternoAtisNames.MDCALENDARYTIMEDETAIL.TIMEEND.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.MDCALENDARYTIMEDETAIL.TIMEEND.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCALENDARYTIMEDETAIL.TIMEEND);
UsrCfg.InternoAtisNames.MDCALENDARYTIMEDETAIL.NAME_TABLE = "MDCALENDARYTIMEDETAIL"; 
UsrCfg.InternoAtisNames.MDCALENDARYTIMEDETAIL.TIMEDESCRIPTION.FieldName = "TIMEDESCRIPTION"; 
UsrCfg.InternoAtisNames.MDCALENDARYTIMEDETAIL.TIMEDESCRIPTION.TableName = "MDCALENDARYTIMEDETAIL"; 
UsrCfg.InternoAtisNames.MDCALENDARYTIMEDETAIL.TIMEDESCRIPTION.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.MDCALENDARYTIMEDETAIL.TIMEDESCRIPTION.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCALENDARYTIMEDETAIL.TIMEDESCRIPTION);
UsrCfg.InternoAtisNames.MDCALENDARYDATE.NAME_TABLE = "MDCALENDARYDATE"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATE.IDMDCALENDARYDATE.FieldName = "IDMDCALENDARYDATE"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATE.IDMDCALENDARYDATE.TableName = "MDCALENDARYDATE"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATE.IDMDCALENDARYDATE.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.MDCALENDARYDATE.IDMDCALENDARYDATE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCALENDARYDATE.IDMDCALENDARYDATE);
UsrCfg.InternoAtisNames.MDCALENDARYDATE.NAME_TABLE = "MDCALENDARYDATE"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATE.DATENAME.FieldName = "DATENAME"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATE.DATENAME.TableName = "MDCALENDARYDATE"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATE.DATENAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.MDCALENDARYDATE.DATENAME.Size = 255; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCALENDARYDATE.DATENAME);
UsrCfg.InternoAtisNames.MDCALENDARYDATE.NAME_TABLE = "MDCALENDARYDATE"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATE.DATESTATUS.FieldName = "DATESTATUS"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATE.DATESTATUS.TableName = "MDCALENDARYDATE"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATE.DATESTATUS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDCALENDARYDATE.DATESTATUS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCALENDARYDATE.DATESTATUS);
UsrCfg.InternoAtisNames.MDCALENDARYDATE.NAME_TABLE = "MDCALENDARYDATE"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATE.DATEORDER.FieldName = "DATEORDER"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATE.DATEORDER.TableName = "MDCALENDARYDATE"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATE.DATEORDER.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDCALENDARYDATE.DATEORDER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCALENDARYDATE.DATEORDER);
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.NAME_TABLE = "MDCALENDARYDATEDETAIL"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.IDMDCALENDARYDATEDETAIL.FieldName = "IDMDCALENDARYDATEDETAIL"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.IDMDCALENDARYDATEDETAIL.TableName = "MDCALENDARYDATEDETAIL"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.IDMDCALENDARYDATEDETAIL.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.IDMDCALENDARYDATEDETAIL.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.IDMDCALENDARYDATEDETAIL);
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.NAME_TABLE = "MDCALENDARYDATEDETAIL"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.IDMDCALENDARYDATE.FieldName = "IDMDCALENDARYDATE"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.IDMDCALENDARYDATE.TableName = "MDCALENDARYDATEDETAIL"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.IDMDCALENDARYDATE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.IDMDCALENDARYDATE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.IDMDCALENDARYDATE);
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.NAME_TABLE = "MDCALENDARYDATEDETAIL"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.IDMDCALENDARYTIME.FieldName = "IDMDCALENDARYTIME"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.IDMDCALENDARYTIME.TableName = "MDCALENDARYDATEDETAIL"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.IDMDCALENDARYTIME.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.IDMDCALENDARYTIME.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.IDMDCALENDARYTIME);
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.NAME_TABLE = "MDCALENDARYDATEDETAIL"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.IDMDCALENDARYGROUP.FieldName = "IDMDCALENDARYGROUP"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.IDMDCALENDARYGROUP.TableName = "MDCALENDARYDATEDETAIL"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.IDMDCALENDARYGROUP.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.IDMDCALENDARYGROUP.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.IDMDCALENDARYGROUP);
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.NAME_TABLE = "MDCALENDARYDATEDETAIL"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.IDMDCALENDARYDATERULE.FieldName = "IDMDCALENDARYDATERULE"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.IDMDCALENDARYDATERULE.TableName = "MDCALENDARYDATEDETAIL"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.IDMDCALENDARYDATERULE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.IDMDCALENDARYDATERULE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.IDMDCALENDARYDATERULE);
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.NAME_TABLE = "MDCALENDARYDATEDETAIL"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.DATEDESCRIPTION.FieldName = "DATEDESCRIPTION"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.DATEDESCRIPTION.TableName = "MDCALENDARYDATEDETAIL"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.DATEDESCRIPTION.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.DATEDESCRIPTION.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.DATEDESCRIPTION);
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.NAME_TABLE = "MDCALENDARYDATEDETAIL"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.DATESTART.FieldName = "DATESTART"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.DATESTART.TableName = "MDCALENDARYDATEDETAIL"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.DATESTART.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.DATESTART.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.DATESTART);
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.NAME_TABLE = "MDCALENDARYDATEDETAIL"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.DATEEND.FieldName = "DATEEND"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.DATEEND.TableName = "MDCALENDARYDATEDETAIL"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.DATEEND.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.DATEEND.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.DATEEND);
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.NAME_TABLE = "MDCALENDARYDATEDETAIL"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.DATEINCLUDE.FieldName = "DATEINCLUDE"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.DATEINCLUDE.TableName = "MDCALENDARYDATEDETAIL"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.DATEINCLUDE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.DATEINCLUDE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.DATEINCLUDE);
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.NAME_TABLE = "MDCALENDARYDATEDETAIL"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.DATEORDER.FieldName = "DATEORDER"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.DATEORDER.TableName = "MDCALENDARYDATEDETAIL"; 
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.DATEORDER.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.DATEORDER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCALENDARYDATEDETAIL.DATEORDER);
UsrCfg.InternoAtisNames.MDCALENDARYGROUP.NAME_TABLE = "MDCALENDARYGROUP"; 
UsrCfg.InternoAtisNames.MDCALENDARYGROUP.IDMDCALENDARYGROUP.FieldName = "IDMDCALENDARYGROUP"; 
UsrCfg.InternoAtisNames.MDCALENDARYGROUP.IDMDCALENDARYGROUP.TableName = "MDCALENDARYGROUP"; 
UsrCfg.InternoAtisNames.MDCALENDARYGROUP.IDMDCALENDARYGROUP.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.MDCALENDARYGROUP.IDMDCALENDARYGROUP.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCALENDARYGROUP.IDMDCALENDARYGROUP);
UsrCfg.InternoAtisNames.MDCALENDARYGROUP.NAME_TABLE = "MDCALENDARYGROUP"; 
UsrCfg.InternoAtisNames.MDCALENDARYGROUP.MDCALENDARYGROUPTYPE.FieldName = "MDCALENDARYGROUPTYPE"; 
UsrCfg.InternoAtisNames.MDCALENDARYGROUP.MDCALENDARYGROUPTYPE.TableName = "MDCALENDARYGROUP"; 
UsrCfg.InternoAtisNames.MDCALENDARYGROUP.MDCALENDARYGROUPTYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDCALENDARYGROUP.MDCALENDARYGROUPTYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCALENDARYGROUP.MDCALENDARYGROUPTYPE);
UsrCfg.InternoAtisNames.MDCALENDARYGROUP.NAME_TABLE = "MDCALENDARYGROUP"; 
UsrCfg.InternoAtisNames.MDCALENDARYGROUP.IDMDCALENDARYDATE.FieldName = "IDMDCALENDARYDATE"; 
UsrCfg.InternoAtisNames.MDCALENDARYGROUP.IDMDCALENDARYDATE.TableName = "MDCALENDARYGROUP"; 
UsrCfg.InternoAtisNames.MDCALENDARYGROUP.IDMDCALENDARYDATE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDCALENDARYGROUP.IDMDCALENDARYDATE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCALENDARYGROUP.IDMDCALENDARYDATE);
UsrCfg.InternoAtisNames.MDCALENDARYGROUP.NAME_TABLE = "MDCALENDARYGROUP"; 
UsrCfg.InternoAtisNames.MDCALENDARYGROUP.IDMDCALENDARYTIME.FieldName = "IDMDCALENDARYTIME"; 
UsrCfg.InternoAtisNames.MDCALENDARYGROUP.IDMDCALENDARYTIME.TableName = "MDCALENDARYGROUP"; 
UsrCfg.InternoAtisNames.MDCALENDARYGROUP.IDMDCALENDARYTIME.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDCALENDARYGROUP.IDMDCALENDARYTIME.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCALENDARYGROUP.IDMDCALENDARYTIME);
UsrCfg.InternoAtisNames.MDCALENDARYGROUP.NAME_TABLE = "MDCALENDARYGROUP"; 
UsrCfg.InternoAtisNames.MDCALENDARYGROUP.GROUPNAME.FieldName = "GROUPNAME"; 
UsrCfg.InternoAtisNames.MDCALENDARYGROUP.GROUPNAME.TableName = "MDCALENDARYGROUP"; 
UsrCfg.InternoAtisNames.MDCALENDARYGROUP.GROUPNAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.MDCALENDARYGROUP.GROUPNAME.Size = 255; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCALENDARYGROUP.GROUPNAME);
UsrCfg.InternoAtisNames.MDCALENDARYGROUP.NAME_TABLE = "MDCALENDARYGROUP"; 
UsrCfg.InternoAtisNames.MDCALENDARYGROUP.GROUPSTATUS.FieldName = "GROUPSTATUS"; 
UsrCfg.InternoAtisNames.MDCALENDARYGROUP.GROUPSTATUS.TableName = "MDCALENDARYGROUP"; 
UsrCfg.InternoAtisNames.MDCALENDARYGROUP.GROUPSTATUS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.MDCALENDARYGROUP.GROUPSTATUS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.MDCALENDARYGROUP.GROUPSTATUS);
UsrCfg.InternoAtisNames.GPQUERYTYPE.NAME_TABLE = "GPQUERYTYPE"; 
UsrCfg.InternoAtisNames.GPQUERYTYPE.IDGPQUERYTYPE.FieldName = "IDGPQUERYTYPE"; 
UsrCfg.InternoAtisNames.GPQUERYTYPE.IDGPQUERYTYPE.TableName = "GPQUERYTYPE"; 
UsrCfg.InternoAtisNames.GPQUERYTYPE.IDGPQUERYTYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.GPQUERYTYPE.IDGPQUERYTYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPQUERYTYPE.IDGPQUERYTYPE);
UsrCfg.InternoAtisNames.GPQUERYTYPE.NAME_TABLE = "GPQUERYTYPE"; 
UsrCfg.InternoAtisNames.GPQUERYTYPE.QUERYTYPENAME.FieldName = "QUERYTYPENAME"; 
UsrCfg.InternoAtisNames.GPQUERYTYPE.QUERYTYPENAME.TableName = "GPQUERYTYPE"; 
UsrCfg.InternoAtisNames.GPQUERYTYPE.QUERYTYPENAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPQUERYTYPE.QUERYTYPENAME.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPQUERYTYPE.QUERYTYPENAME);
UsrCfg.InternoAtisNames.GPQUERY.NAME_TABLE = "GPQUERY"; 
UsrCfg.InternoAtisNames.GPQUERY.IDGPQUERY.FieldName = "IDGPQUERY"; 
UsrCfg.InternoAtisNames.GPQUERY.IDGPQUERY.TableName = "GPQUERY"; 
UsrCfg.InternoAtisNames.GPQUERY.IDGPQUERY.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.GPQUERY.IDGPQUERY.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPQUERY.IDGPQUERY);
UsrCfg.InternoAtisNames.GPQUERY.NAME_TABLE = "GPQUERY"; 
UsrCfg.InternoAtisNames.GPQUERY.IDGPQUERYTYPE.FieldName = "IDGPQUERYTYPE"; 
UsrCfg.InternoAtisNames.GPQUERY.IDGPQUERYTYPE.TableName = "GPQUERY"; 
UsrCfg.InternoAtisNames.GPQUERY.IDGPQUERYTYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.GPQUERY.IDGPQUERYTYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPQUERY.IDGPQUERYTYPE);
UsrCfg.InternoAtisNames.GPQUERY.NAME_TABLE = "GPQUERY"; 
UsrCfg.InternoAtisNames.GPQUERY.GPQUERY_NAME.FieldName = "GPQUERY_NAME"; 
UsrCfg.InternoAtisNames.GPQUERY.GPQUERY_NAME.TableName = "GPQUERY"; 
UsrCfg.InternoAtisNames.GPQUERY.GPQUERY_NAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPQUERY.GPQUERY_NAME.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPQUERY.GPQUERY_NAME);
UsrCfg.InternoAtisNames.GPQUERY.NAME_TABLE = "GPQUERY"; 
UsrCfg.InternoAtisNames.GPQUERY.GPQUERY_DESCRIPTION.FieldName = "GPQUERY_DESCRIPTION"; 
UsrCfg.InternoAtisNames.GPQUERY.GPQUERY_DESCRIPTION.TableName = "GPQUERY"; 
UsrCfg.InternoAtisNames.GPQUERY.GPQUERY_DESCRIPTION.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.GPQUERY.GPQUERY_DESCRIPTION.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPQUERY.GPQUERY_DESCRIPTION);
UsrCfg.InternoAtisNames.GPQUERY.NAME_TABLE = "GPQUERY"; 
UsrCfg.InternoAtisNames.GPQUERY.STRSQL.FieldName = "STRSQL"; 
UsrCfg.InternoAtisNames.GPQUERY.STRSQL.TableName = "GPQUERY"; 
UsrCfg.InternoAtisNames.GPQUERY.STRSQL.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.GPQUERY.STRSQL.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPQUERY.STRSQL);
UsrCfg.InternoAtisNames.GPQUERY.NAME_TABLE = "GPQUERY"; 
UsrCfg.InternoAtisNames.GPQUERY.PATH.FieldName = "PATH"; 
UsrCfg.InternoAtisNames.GPQUERY.PATH.TableName = "GPQUERY"; 
UsrCfg.InternoAtisNames.GPQUERY.PATH.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.GPQUERY.PATH.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPQUERY.PATH);
UsrCfg.InternoAtisNames.GPQUERY.NAME_TABLE = "GPQUERY"; 
UsrCfg.InternoAtisNames.GPQUERY.OPEN_TYPENAME.FieldName = "OPEN_TYPENAME"; 
UsrCfg.InternoAtisNames.GPQUERY.OPEN_TYPENAME.TableName = "GPQUERY"; 
UsrCfg.InternoAtisNames.GPQUERY.OPEN_TYPENAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPQUERY.OPEN_TYPENAME.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPQUERY.OPEN_TYPENAME);
UsrCfg.InternoAtisNames.GPQUERY.NAME_TABLE = "GPQUERY"; 
UsrCfg.InternoAtisNames.GPQUERY.OPEN_PREFIX.FieldName = "OPEN_PREFIX"; 
UsrCfg.InternoAtisNames.GPQUERY.OPEN_PREFIX.TableName = "GPQUERY"; 
UsrCfg.InternoAtisNames.GPQUERY.OPEN_PREFIX.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPQUERY.OPEN_PREFIX.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPQUERY.OPEN_PREFIX);
UsrCfg.InternoAtisNames.GPQUERY.NAME_TABLE = "GPQUERY"; 
UsrCfg.InternoAtisNames.GPQUERY.OPEN_ID.FieldName = "OPEN_ID"; 
UsrCfg.InternoAtisNames.GPQUERY.OPEN_ID.TableName = "GPQUERY"; 
UsrCfg.InternoAtisNames.GPQUERY.OPEN_ID.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPQUERY.OPEN_ID.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPQUERY.OPEN_ID);
UsrCfg.InternoAtisNames.GPQUERY.NAME_TABLE = "GPQUERY"; 
UsrCfg.InternoAtisNames.GPQUERY.IDATUSER.FieldName = "IDATUSER"; 
UsrCfg.InternoAtisNames.GPQUERY.IDATUSER.TableName = "GPQUERY"; 
UsrCfg.InternoAtisNames.GPQUERY.IDATUSER.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.GPQUERY.IDATUSER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPQUERY.IDATUSER);
UsrCfg.InternoAtisNames.GPQUERY.NAME_TABLE = "GPQUERY"; 
UsrCfg.InternoAtisNames.GPQUERY.START.FieldName = "START"; 
UsrCfg.InternoAtisNames.GPQUERY.START.TableName = "GPQUERY"; 
UsrCfg.InternoAtisNames.GPQUERY.START.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.GPQUERY.START.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPQUERY.START);
UsrCfg.InternoAtisNames.GPVIEW.NAME_TABLE = "GPVIEW"; 
UsrCfg.InternoAtisNames.GPVIEW.IDGPVIEW.FieldName = "IDGPVIEW"; 
UsrCfg.InternoAtisNames.GPVIEW.IDGPVIEW.TableName = "GPVIEW"; 
UsrCfg.InternoAtisNames.GPVIEW.IDGPVIEW.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.GPVIEW.IDGPVIEW.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPVIEW.IDGPVIEW);
UsrCfg.InternoAtisNames.GPVIEW.NAME_TABLE = "GPVIEW"; 
UsrCfg.InternoAtisNames.GPVIEW.IDGPQUERY.FieldName = "IDGPQUERY"; 
UsrCfg.InternoAtisNames.GPVIEW.IDGPQUERY.TableName = "GPVIEW"; 
UsrCfg.InternoAtisNames.GPVIEW.IDGPQUERY.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.GPVIEW.IDGPQUERY.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPVIEW.IDGPQUERY);
UsrCfg.InternoAtisNames.GPVIEW.NAME_TABLE = "GPVIEW"; 
UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_NAME.FieldName = "GPVIEW_NAME"; 
UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_NAME.TableName = "GPVIEW"; 
UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_NAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_NAME.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_NAME);
UsrCfg.InternoAtisNames.GPVIEW.NAME_TABLE = "GPVIEW"; 
UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_DESCRIPTION.FieldName = "GPVIEW_DESCRIPTION"; 
UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_DESCRIPTION.TableName = "GPVIEW"; 
UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_DESCRIPTION.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_DESCRIPTION.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_DESCRIPTION);
UsrCfg.InternoAtisNames.GPVIEW.NAME_TABLE = "GPVIEW"; 
UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_ISACTIVE.FieldName = "GPVIEW_ISACTIVE"; 
UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_ISACTIVE.TableName = "GPVIEW"; 
UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_ISACTIVE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_ISACTIVE.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_ISACTIVE);
UsrCfg.InternoAtisNames.GPVIEW.NAME_TABLE = "GPVIEW"; 
UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_ISPUBLIC.FieldName = "GPVIEW_ISPUBLIC"; 
UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_ISPUBLIC.TableName = "GPVIEW"; 
UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_ISPUBLIC.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_ISPUBLIC.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_ISPUBLIC);
UsrCfg.InternoAtisNames.GPVIEW.NAME_TABLE = "GPVIEW"; 
UsrCfg.InternoAtisNames.GPVIEW.IDCMDBUSER.FieldName = "IDCMDBUSER"; 
UsrCfg.InternoAtisNames.GPVIEW.IDCMDBUSER.TableName = "GPVIEW"; 
UsrCfg.InternoAtisNames.GPVIEW.IDCMDBUSER.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.GPVIEW.IDCMDBUSER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPVIEW.IDCMDBUSER);
UsrCfg.InternoAtisNames.GPVIEW.NAME_TABLE = "GPVIEW"; 
UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_FILTER.FieldName = "GPVIEW_FILTER"; 
UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_FILTER.TableName = "GPVIEW"; 
UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_FILTER.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_FILTER.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPVIEW.GPVIEW_FILTER);
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.NAME_TABLE = "GPCOLUMNVIEW"; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.IDGPCOLUMNVIEW.FieldName = "IDGPCOLUMNVIEW"; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.IDGPCOLUMNVIEW.TableName = "GPCOLUMNVIEW"; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.IDGPCOLUMNVIEW.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.IDGPCOLUMNVIEW.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.IDGPCOLUMNVIEW);
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.NAME_TABLE = "GPCOLUMNVIEW"; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.IDGPVIEW.FieldName = "IDGPVIEW"; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.IDGPVIEW.TableName = "GPCOLUMNVIEW"; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.IDGPVIEW.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.IDGPVIEW.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.IDGPVIEW);
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.NAME_TABLE = "GPCOLUMNVIEW"; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.TABLE_NAME.FieldName = "TABLE_NAME"; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.TABLE_NAME.TableName = "GPCOLUMNVIEW"; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.TABLE_NAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.TABLE_NAME.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.TABLE_NAME);
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.NAME_TABLE = "GPCOLUMNVIEW"; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.FIELD_NAME.FieldName = "FIELD_NAME"; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.FIELD_NAME.TableName = "GPCOLUMNVIEW"; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.FIELD_NAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.FIELD_NAME.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.FIELD_NAME);
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.NAME_TABLE = "GPCOLUMNVIEW"; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.HEADER.FieldName = "HEADER"; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.HEADER.TableName = "GPCOLUMNVIEW"; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.HEADER.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.HEADER.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.HEADER);
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.NAME_TABLE = "GPCOLUMNVIEW"; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.POSITION.FieldName = "POSITION"; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.POSITION.TableName = "GPCOLUMNVIEW"; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.POSITION.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.POSITION.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.POSITION);
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.NAME_TABLE = "GPCOLUMNVIEW"; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.SIZE.FieldName = "SIZE"; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.SIZE.TableName = "GPCOLUMNVIEW"; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.SIZE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.SIZE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.SIZE);
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.NAME_TABLE = "GPCOLUMNVIEW"; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.ISVISIBLE.FieldName = "ISVISIBLE"; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.ISVISIBLE.TableName = "GPCOLUMNVIEW"; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.ISVISIBLE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.ISVISIBLE.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.ISVISIBLE);
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.NAME_TABLE = "GPCOLUMNVIEW"; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.ISVISIBLEDETAIL.FieldName = "ISVISIBLEDETAIL"; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.ISVISIBLEDETAIL.TableName = "GPCOLUMNVIEW"; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.ISVISIBLEDETAIL.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.ISVISIBLEDETAIL.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.ISVISIBLEDETAIL);
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.NAME_TABLE = "GPCOLUMNVIEW"; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.COLOR.FieldName = "COLOR"; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.COLOR.TableName = "GPCOLUMNVIEW"; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.COLOR.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.COLOR.Size = 10; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.COLOR);
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.NAME_TABLE = "GPCOLUMNVIEW"; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.ISGROUP.FieldName = "ISGROUP"; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.ISGROUP.TableName = "GPCOLUMNVIEW"; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.ISGROUP.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.ISGROUP.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.ISGROUP);
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.NAME_TABLE = "GPCOLUMNVIEW"; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.GROUP_POSITION.FieldName = "GROUP_POSITION"; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.GROUP_POSITION.TableName = "GPCOLUMNVIEW"; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.GROUP_POSITION.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.GROUP_POSITION.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.GROUP_POSITION);
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.NAME_TABLE = "GPCOLUMNVIEW"; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.SORT.FieldName = "SORT"; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.SORT.TableName = "GPCOLUMNVIEW"; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.SORT.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPCOLUMNVIEW.SORT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPCOLUMNVIEW.SORT);
UsrCfg.InternoAtisNames.GPVALUECOLOR.NAME_TABLE = "GPVALUECOLOR"; 
UsrCfg.InternoAtisNames.GPVALUECOLOR.IDGPVALUECOLOR.FieldName = "IDGPVALUECOLOR"; 
UsrCfg.InternoAtisNames.GPVALUECOLOR.IDGPVALUECOLOR.TableName = "GPVALUECOLOR"; 
UsrCfg.InternoAtisNames.GPVALUECOLOR.IDGPVALUECOLOR.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.GPVALUECOLOR.IDGPVALUECOLOR.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPVALUECOLOR.IDGPVALUECOLOR);
UsrCfg.InternoAtisNames.GPVALUECOLOR.NAME_TABLE = "GPVALUECOLOR"; 
UsrCfg.InternoAtisNames.GPVALUECOLOR.IDGPCOLUMNVIEW.FieldName = "IDGPCOLUMNVIEW"; 
UsrCfg.InternoAtisNames.GPVALUECOLOR.IDGPCOLUMNVIEW.TableName = "GPVALUECOLOR"; 
UsrCfg.InternoAtisNames.GPVALUECOLOR.IDGPCOLUMNVIEW.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.GPVALUECOLOR.IDGPCOLUMNVIEW.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPVALUECOLOR.IDGPCOLUMNVIEW);
UsrCfg.InternoAtisNames.GPVALUECOLOR.NAME_TABLE = "GPVALUECOLOR"; 
UsrCfg.InternoAtisNames.GPVALUECOLOR.NAME.FieldName = "NAME"; 
UsrCfg.InternoAtisNames.GPVALUECOLOR.NAME.TableName = "GPVALUECOLOR"; 
UsrCfg.InternoAtisNames.GPVALUECOLOR.NAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPVALUECOLOR.NAME.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPVALUECOLOR.NAME);
UsrCfg.InternoAtisNames.GPVALUECOLOR.NAME_TABLE = "GPVALUECOLOR"; 
UsrCfg.InternoAtisNames.GPVALUECOLOR.CONDITION.FieldName = "CONDITION"; 
UsrCfg.InternoAtisNames.GPVALUECOLOR.CONDITION.TableName = "GPVALUECOLOR"; 
UsrCfg.InternoAtisNames.GPVALUECOLOR.CONDITION.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPVALUECOLOR.CONDITION.Size = 250; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPVALUECOLOR.CONDITION);
UsrCfg.InternoAtisNames.GPVALUECOLOR.NAME_TABLE = "GPVALUECOLOR"; 
UsrCfg.InternoAtisNames.GPVALUECOLOR.COLOR.FieldName = "COLOR"; 
UsrCfg.InternoAtisNames.GPVALUECOLOR.COLOR.TableName = "GPVALUECOLOR"; 
UsrCfg.InternoAtisNames.GPVALUECOLOR.COLOR.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPVALUECOLOR.COLOR.Size = 10; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPVALUECOLOR.COLOR);
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.NAME_TABLE = "GPEDIT_COLUMN"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.IDGPEDIT_COLUMN.FieldName = "IDGPEDIT_COLUMN"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.IDGPEDIT_COLUMN.TableName = "GPEDIT_COLUMN"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.IDGPEDIT_COLUMN.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.IDGPEDIT_COLUMN.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.IDGPEDIT_COLUMN);
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.NAME_TABLE = "GPEDIT_COLUMN"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.IDGPQUERY.FieldName = "IDGPQUERY"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.IDGPQUERY.TableName = "GPEDIT_COLUMN"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.IDGPQUERY.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.IDGPQUERY.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.IDGPQUERY);
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.NAME_TABLE = "GPEDIT_COLUMN"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.EDIT_COLUMNNAME.FieldName = "EDIT_COLUMNNAME"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.EDIT_COLUMNNAME.TableName = "GPEDIT_COLUMN"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.EDIT_COLUMNNAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.EDIT_COLUMNNAME.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.EDIT_COLUMNNAME);
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.NAME_TABLE = "GPEDIT_COLUMN"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.EDIT_COLUMNTYPE.FieldName = "EDIT_COLUMNTYPE"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.EDIT_COLUMNTYPE.TableName = "GPEDIT_COLUMN"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.EDIT_COLUMNTYPE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.EDIT_COLUMNTYPE.Size = 20; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.EDIT_COLUMNTYPE);
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.NAME_TABLE = "GPEDIT_COLUMN"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.MYCONSTRAINT.FieldName = "MYCONSTRAINT"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.MYCONSTRAINT.TableName = "GPEDIT_COLUMN"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.MYCONSTRAINT.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.MYCONSTRAINT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.MYCONSTRAINT);
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.NAME_TABLE = "GPEDIT_COLUMN"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.AUTOINCTABLENAME.FieldName = "AUTOINCTABLENAME"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.AUTOINCTABLENAME.TableName = "GPEDIT_COLUMN"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.AUTOINCTABLENAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.AUTOINCTABLENAME.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.AUTOINCTABLENAME);
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.NAME_TABLE = "GPEDIT_COLUMN"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.COLUMNSTYLE.FieldName = "COLUMNSTYLE"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.COLUMNSTYLE.TableName = "GPEDIT_COLUMN"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.COLUMNSTYLE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.COLUMNSTYLE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.COLUMNSTYLE);
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.NAME_TABLE = "GPEDIT_COLUMN"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.LOOKUPSQL.FieldName = "LOOKUPSQL"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.LOOKUPSQL.TableName = "GPEDIT_COLUMN"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.LOOKUPSQL.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.LOOKUPSQL.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.LOOKUPSQL);
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.NAME_TABLE = "GPEDIT_COLUMN"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.LOOKUPDISPLAYCOLUMN.FieldName = "LOOKUPDISPLAYCOLUMN"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.LOOKUPDISPLAYCOLUMN.TableName = "GPEDIT_COLUMN"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.LOOKUPDISPLAYCOLUMN.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.LOOKUPDISPLAYCOLUMN.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.LOOKUPDISPLAYCOLUMN);
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.NAME_TABLE = "GPEDIT_COLUMN"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.LOOKUPDISPLAYCOLUMNLIST.FieldName = "LOOKUPDISPLAYCOLUMNLIST"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.LOOKUPDISPLAYCOLUMNLIST.TableName = "GPEDIT_COLUMN"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.LOOKUPDISPLAYCOLUMNLIST.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.LOOKUPDISPLAYCOLUMNLIST.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.LOOKUPDISPLAYCOLUMNLIST);
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.NAME_TABLE = "GPEDIT_COLUMN"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.ACTIVE.FieldName = "ACTIVE"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.ACTIVE.TableName = "GPEDIT_COLUMN"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.ACTIVE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.ACTIVE.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.ACTIVE);
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.NAME_TABLE = "GPEDIT_COLUMN"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.FILETYPE.FieldName = "FILETYPE"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.FILETYPE.TableName = "GPEDIT_COLUMN"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.FILETYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMN.FILETYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPEDIT_COLUMN.FILETYPE);
UsrCfg.InternoAtisNames.GPEDIT_TABLE.NAME_TABLE = "GPEDIT_TABLE"; 
UsrCfg.InternoAtisNames.GPEDIT_TABLE.IDGPEDIT_TABLE.FieldName = "IDGPEDIT_TABLE"; 
UsrCfg.InternoAtisNames.GPEDIT_TABLE.IDGPEDIT_TABLE.TableName = "GPEDIT_TABLE"; 
UsrCfg.InternoAtisNames.GPEDIT_TABLE.IDGPEDIT_TABLE.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.GPEDIT_TABLE.IDGPEDIT_TABLE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPEDIT_TABLE.IDGPEDIT_TABLE);
UsrCfg.InternoAtisNames.GPEDIT_TABLE.NAME_TABLE = "GPEDIT_TABLE"; 
UsrCfg.InternoAtisNames.GPEDIT_TABLE.IDGPQUERY.FieldName = "IDGPQUERY"; 
UsrCfg.InternoAtisNames.GPEDIT_TABLE.IDGPQUERY.TableName = "GPEDIT_TABLE"; 
UsrCfg.InternoAtisNames.GPEDIT_TABLE.IDGPQUERY.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.GPEDIT_TABLE.IDGPQUERY.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPEDIT_TABLE.IDGPQUERY);
UsrCfg.InternoAtisNames.GPEDIT_TABLE.NAME_TABLE = "GPEDIT_TABLE"; 
UsrCfg.InternoAtisNames.GPEDIT_TABLE.EDIT_TABLENAME.FieldName = "EDIT_TABLENAME"; 
UsrCfg.InternoAtisNames.GPEDIT_TABLE.EDIT_TABLENAME.TableName = "GPEDIT_TABLE"; 
UsrCfg.InternoAtisNames.GPEDIT_TABLE.EDIT_TABLENAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPEDIT_TABLE.EDIT_TABLENAME.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPEDIT_TABLE.EDIT_TABLENAME);
UsrCfg.InternoAtisNames.GPEDIT_TABLE.NAME_TABLE = "GPEDIT_TABLE"; 
UsrCfg.InternoAtisNames.GPEDIT_TABLE.EDIT_TABLEORDER.FieldName = "EDIT_TABLEORDER"; 
UsrCfg.InternoAtisNames.GPEDIT_TABLE.EDIT_TABLEORDER.TableName = "GPEDIT_TABLE"; 
UsrCfg.InternoAtisNames.GPEDIT_TABLE.EDIT_TABLEORDER.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.GPEDIT_TABLE.EDIT_TABLEORDER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPEDIT_TABLE.EDIT_TABLEORDER);
UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.NAME_TABLE = "GPEDIT_COLUMNEVENT"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMNEVENT.FieldName = "IDGPEDIT_COLUMNEVENT"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMNEVENT.TableName = "GPEDIT_COLUMNEVENT"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMNEVENT.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMNEVENT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMNEVENT);
UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.NAME_TABLE = "GPEDIT_COLUMNEVENT"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMN.FieldName = "IDGPEDIT_COLUMN"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMN.TableName = "GPEDIT_COLUMNEVENT"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMN.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMN.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_COLUMN);
UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.NAME_TABLE = "GPEDIT_COLUMNEVENT"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_TABLE.FieldName = "IDGPEDIT_TABLE"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_TABLE.TableName = "GPEDIT_COLUMNEVENT"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_TABLE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_TABLE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.IDGPEDIT_TABLE);
UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.NAME_TABLE = "GPEDIT_COLUMNEVENT"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.EVINSERT.FieldName = "EVINSERT"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.EVINSERT.TableName = "GPEDIT_COLUMNEVENT"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.EVINSERT.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.EVINSERT.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.EVINSERT);
UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.NAME_TABLE = "GPEDIT_COLUMNEVENT"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.EVDELETE.FieldName = "EVDELETE"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.EVDELETE.TableName = "GPEDIT_COLUMNEVENT"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.EVDELETE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.EVDELETE.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.EVDELETE);
UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.NAME_TABLE = "GPEDIT_COLUMNEVENT"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.EVUPDATESET.FieldName = "EVUPDATESET"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.EVUPDATESET.TableName = "GPEDIT_COLUMNEVENT"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.EVUPDATESET.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.EVUPDATESET.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.EVUPDATESET);
UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.NAME_TABLE = "GPEDIT_COLUMNEVENT"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.EVUPDATEWHERE.FieldName = "EVUPDATEWHERE"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.EVUPDATEWHERE.TableName = "GPEDIT_COLUMNEVENT"; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.EVUPDATEWHERE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.EVUPDATEWHERE.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPEDIT_COLUMNEVENT.EVUPDATEWHERE);
UsrCfg.InternoAtisNames.GPCHART.NAME_TABLE = "GPCHART"; 
UsrCfg.InternoAtisNames.GPCHART.IDGPCHART.FieldName = "IDGPCHART"; 
UsrCfg.InternoAtisNames.GPCHART.IDGPCHART.TableName = "GPCHART"; 
UsrCfg.InternoAtisNames.GPCHART.IDGPCHART.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.GPCHART.IDGPCHART.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPCHART.IDGPCHART);
UsrCfg.InternoAtisNames.GPCHART.NAME_TABLE = "GPCHART"; 
UsrCfg.InternoAtisNames.GPCHART.IDGPQUERY.FieldName = "IDGPQUERY"; 
UsrCfg.InternoAtisNames.GPCHART.IDGPQUERY.TableName = "GPCHART"; 
UsrCfg.InternoAtisNames.GPCHART.IDGPQUERY.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.GPCHART.IDGPQUERY.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPCHART.IDGPQUERY);
UsrCfg.InternoAtisNames.GPCHART.NAME_TABLE = "GPCHART"; 
UsrCfg.InternoAtisNames.GPCHART.NAME.FieldName = "NAME"; 
UsrCfg.InternoAtisNames.GPCHART.NAME.TableName = "GPCHART"; 
UsrCfg.InternoAtisNames.GPCHART.NAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPCHART.NAME.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPCHART.NAME);
UsrCfg.InternoAtisNames.GPCHART.NAME_TABLE = "GPCHART"; 
UsrCfg.InternoAtisNames.GPCHART.DESCRIPTION.FieldName = "DESCRIPTION"; 
UsrCfg.InternoAtisNames.GPCHART.DESCRIPTION.TableName = "GPCHART"; 
UsrCfg.InternoAtisNames.GPCHART.DESCRIPTION.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.GPCHART.DESCRIPTION.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPCHART.DESCRIPTION);
UsrCfg.InternoAtisNames.GPCHART.NAME_TABLE = "GPCHART"; 
UsrCfg.InternoAtisNames.GPCHART.STRSQL.FieldName = "STRSQL"; 
UsrCfg.InternoAtisNames.GPCHART.STRSQL.TableName = "GPCHART"; 
UsrCfg.InternoAtisNames.GPCHART.STRSQL.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.GPCHART.STRSQL.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPCHART.STRSQL);
UsrCfg.InternoAtisNames.GPCHART.NAME_TABLE = "GPCHART"; 
UsrCfg.InternoAtisNames.GPCHART.XFIELDNAME.FieldName = "XFIELDNAME"; 
UsrCfg.InternoAtisNames.GPCHART.XFIELDNAME.TableName = "GPCHART"; 
UsrCfg.InternoAtisNames.GPCHART.XFIELDNAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPCHART.XFIELDNAME.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPCHART.XFIELDNAME);
UsrCfg.InternoAtisNames.GPCHART.NAME_TABLE = "GPCHART"; 
UsrCfg.InternoAtisNames.GPCHART.YFIELDNAME.FieldName = "YFIELDNAME"; 
UsrCfg.InternoAtisNames.GPCHART.YFIELDNAME.TableName = "GPCHART"; 
UsrCfg.InternoAtisNames.GPCHART.YFIELDNAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPCHART.YFIELDNAME.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPCHART.YFIELDNAME);
UsrCfg.InternoAtisNames.GPCHART.NAME_TABLE = "GPCHART"; 
UsrCfg.InternoAtisNames.GPCHART.SERIESGROUP.FieldName = "SERIESGROUP"; 
UsrCfg.InternoAtisNames.GPCHART.SERIESGROUP.TableName = "GPCHART"; 
UsrCfg.InternoAtisNames.GPCHART.SERIESGROUP.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPCHART.SERIESGROUP.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPCHART.SERIESGROUP);
UsrCfg.InternoAtisNames.GPCHART.NAME_TABLE = "GPCHART"; 
UsrCfg.InternoAtisNames.GPCHART.ISGAUGE.FieldName = "ISGAUGE"; 
UsrCfg.InternoAtisNames.GPCHART.ISGAUGE.TableName = "GPCHART"; 
UsrCfg.InternoAtisNames.GPCHART.ISGAUGE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPCHART.ISGAUGE.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPCHART.ISGAUGE);
UsrCfg.InternoAtisNames.GPCHART.NAME_TABLE = "GPCHART"; 
UsrCfg.InternoAtisNames.GPCHART.ISCHART.FieldName = "ISCHART"; 
UsrCfg.InternoAtisNames.GPCHART.ISCHART.TableName = "GPCHART"; 
UsrCfg.InternoAtisNames.GPCHART.ISCHART.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPCHART.ISCHART.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPCHART.ISCHART);
UsrCfg.InternoAtisNames.GPCHART.NAME_TABLE = "GPCHART"; 
UsrCfg.InternoAtisNames.GPCHART.MINVALUE.FieldName = "MINVALUE"; 
UsrCfg.InternoAtisNames.GPCHART.MINVALUE.TableName = "GPCHART"; 
UsrCfg.InternoAtisNames.GPCHART.MINVALUE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPCHART.MINVALUE.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPCHART.MINVALUE);
UsrCfg.InternoAtisNames.GPCHART.NAME_TABLE = "GPCHART"; 
UsrCfg.InternoAtisNames.GPCHART.MAXVALUE.FieldName = "MAXVALUE"; 
UsrCfg.InternoAtisNames.GPCHART.MAXVALUE.TableName = "GPCHART"; 
UsrCfg.InternoAtisNames.GPCHART.MAXVALUE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPCHART.MAXVALUE.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPCHART.MAXVALUE);
UsrCfg.InternoAtisNames.GPCHART.NAME_TABLE = "GPCHART"; 
UsrCfg.InternoAtisNames.GPCHART.GAUGERANGE.FieldName = "GAUGERANGE"; 
UsrCfg.InternoAtisNames.GPCHART.GAUGERANGE.TableName = "GPCHART"; 
UsrCfg.InternoAtisNames.GPCHART.GAUGERANGE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPCHART.GAUGERANGE.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPCHART.GAUGERANGE);
UsrCfg.InternoAtisNames.GPCHART.NAME_TABLE = "GPCHART"; 
UsrCfg.InternoAtisNames.GPCHART.GAUGETYPE.FieldName = "GAUGETYPE"; 
UsrCfg.InternoAtisNames.GPCHART.GAUGETYPE.TableName = "GPCHART"; 
UsrCfg.InternoAtisNames.GPCHART.GAUGETYPE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPCHART.GAUGETYPE.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPCHART.GAUGETYPE);
UsrCfg.InternoAtisNames.GPCHART.NAME_TABLE = "GPCHART"; 
UsrCfg.InternoAtisNames.GPCHART.CONFIGURATION.FieldName = "CONFIGURATION"; 
UsrCfg.InternoAtisNames.GPCHART.CONFIGURATION.TableName = "GPCHART"; 
UsrCfg.InternoAtisNames.GPCHART.CONFIGURATION.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.GPCHART.CONFIGURATION.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPCHART.CONFIGURATION);
UsrCfg.InternoAtisNames.GPREPORTSQL.NAME_TABLE = "GPREPORTSQL"; 
UsrCfg.InternoAtisNames.GPREPORTSQL.IDGPREPORTSQL.FieldName = "IDGPREPORTSQL"; 
UsrCfg.InternoAtisNames.GPREPORTSQL.IDGPREPORTSQL.TableName = "GPREPORTSQL"; 
UsrCfg.InternoAtisNames.GPREPORTSQL.IDGPREPORTSQL.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.GPREPORTSQL.IDGPREPORTSQL.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPREPORTSQL.IDGPREPORTSQL);
UsrCfg.InternoAtisNames.GPREPORTSQL.NAME_TABLE = "GPREPORTSQL"; 
UsrCfg.InternoAtisNames.GPREPORTSQL.IDGPQUERY.FieldName = "IDGPQUERY"; 
UsrCfg.InternoAtisNames.GPREPORTSQL.IDGPQUERY.TableName = "GPREPORTSQL"; 
UsrCfg.InternoAtisNames.GPREPORTSQL.IDGPQUERY.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.GPREPORTSQL.IDGPQUERY.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPREPORTSQL.IDGPQUERY);
UsrCfg.InternoAtisNames.GPREPORTSQL.NAME_TABLE = "GPREPORTSQL"; 
UsrCfg.InternoAtisNames.GPREPORTSQL.NAME.FieldName = "NAME"; 
UsrCfg.InternoAtisNames.GPREPORTSQL.NAME.TableName = "GPREPORTSQL"; 
UsrCfg.InternoAtisNames.GPREPORTSQL.NAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPREPORTSQL.NAME.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPREPORTSQL.NAME);
UsrCfg.InternoAtisNames.GPREPORTSQL.NAME_TABLE = "GPREPORTSQL"; 
UsrCfg.InternoAtisNames.GPREPORTSQL.DESCRIPTION.FieldName = "DESCRIPTION"; 
UsrCfg.InternoAtisNames.GPREPORTSQL.DESCRIPTION.TableName = "GPREPORTSQL"; 
UsrCfg.InternoAtisNames.GPREPORTSQL.DESCRIPTION.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.GPREPORTSQL.DESCRIPTION.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPREPORTSQL.DESCRIPTION);
UsrCfg.InternoAtisNames.GPREPORTSQL.NAME_TABLE = "GPREPORTSQL"; 
UsrCfg.InternoAtisNames.GPREPORTSQL.TABLENAME.FieldName = "TABLENAME"; 
UsrCfg.InternoAtisNames.GPREPORTSQL.TABLENAME.TableName = "GPREPORTSQL"; 
UsrCfg.InternoAtisNames.GPREPORTSQL.TABLENAME.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.GPREPORTSQL.TABLENAME.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPREPORTSQL.TABLENAME);
UsrCfg.InternoAtisNames.GPREPORTSQL.NAME_TABLE = "GPREPORTSQL"; 
UsrCfg.InternoAtisNames.GPREPORTSQL.STRSQL.FieldName = "STRSQL"; 
UsrCfg.InternoAtisNames.GPREPORTSQL.STRSQL.TableName = "GPREPORTSQL"; 
UsrCfg.InternoAtisNames.GPREPORTSQL.STRSQL.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.GPREPORTSQL.STRSQL.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPREPORTSQL.STRSQL);
UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.NAME_TABLE = "GPREPORTTEMPLATE"; 
UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.IDGPREPORTTEMPLATE.FieldName = "IDGPREPORTTEMPLATE"; 
UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.IDGPREPORTTEMPLATE.TableName = "GPREPORTTEMPLATE"; 
UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.IDGPREPORTTEMPLATE.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.IDGPREPORTTEMPLATE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.IDGPREPORTTEMPLATE);
UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.NAME_TABLE = "GPREPORTTEMPLATE"; 
UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.IDGPQUERY.FieldName = "IDGPQUERY"; 
UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.IDGPQUERY.TableName = "GPREPORTTEMPLATE"; 
UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.IDGPQUERY.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.IDGPQUERY.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.IDGPQUERY);
UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.NAME_TABLE = "GPREPORTTEMPLATE"; 
UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.NAME.FieldName = "NAME"; 
UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.NAME.TableName = "GPREPORTTEMPLATE"; 
UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.NAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.NAME.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.NAME);
UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.NAME_TABLE = "GPREPORTTEMPLATE"; 
UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.DESCRIPTION.FieldName = "DESCRIPTION"; 
UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.DESCRIPTION.TableName = "GPREPORTTEMPLATE"; 
UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.DESCRIPTION.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.DESCRIPTION.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.DESCRIPTION);
UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.NAME_TABLE = "GPREPORTTEMPLATE"; 
UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.CODE.FieldName = "CODE"; 
UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.CODE.TableName = "GPREPORTTEMPLATE"; 
UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.CODE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.CODE.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.CODE);
UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.NAME_TABLE = "GPREPORTTEMPLATE"; 
UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.FILENAME.FieldName = "FILENAME"; 
UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.FILENAME.TableName = "GPREPORTTEMPLATE"; 
UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.FILENAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.FILENAME.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.FILENAME);
UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.NAME_TABLE = "GPREPORTTEMPLATE"; 
UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.MULTIPLEFILE.FieldName = "MULTIPLEFILE"; 
UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.MULTIPLEFILE.TableName = "GPREPORTTEMPLATE"; 
UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.MULTIPLEFILE.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.MULTIPLEFILE.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.GPREPORTTEMPLATE.MULTIPLEFILE);
UsrCfg.InternoAtisNames.ATROLE.NAME_TABLE = "ATROLE"; 
UsrCfg.InternoAtisNames.ATROLE.IDATROLE.FieldName = "IDATROLE"; 
UsrCfg.InternoAtisNames.ATROLE.IDATROLE.TableName = "ATROLE"; 
UsrCfg.InternoAtisNames.ATROLE.IDATROLE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.ATROLE.IDATROLE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ATROLE.IDATROLE);
UsrCfg.InternoAtisNames.ATROLE.NAME_TABLE = "ATROLE"; 
UsrCfg.InternoAtisNames.ATROLE.ROLENAME.FieldName = "ROLENAME"; 
UsrCfg.InternoAtisNames.ATROLE.ROLENAME.TableName = "ATROLE"; 
UsrCfg.InternoAtisNames.ATROLE.ROLENAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.ATROLE.ROLENAME.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ATROLE.ROLENAME);
UsrCfg.InternoAtisNames.ATROLE.NAME_TABLE = "ATROLE"; 
UsrCfg.InternoAtisNames.ATROLE.REMOTEHELPCONFIG.FieldName = "REMOTEHELPCONFIG"; 
UsrCfg.InternoAtisNames.ATROLE.REMOTEHELPCONFIG.TableName = "ATROLE"; 
UsrCfg.InternoAtisNames.ATROLE.REMOTEHELPCONFIG.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.ATROLE.REMOTEHELPCONFIG.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ATROLE.REMOTEHELPCONFIG);
UsrCfg.InternoAtisNames.ATROLE.NAME_TABLE = "ATROLE"; 
UsrCfg.InternoAtisNames.ATROLE.IDPSGROUP.FieldName = "IDPSGROUP"; 
UsrCfg.InternoAtisNames.ATROLE.IDPSGROUP.TableName = "ATROLE"; 
UsrCfg.InternoAtisNames.ATROLE.IDPSGROUP.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.ATROLE.IDPSGROUP.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ATROLE.IDPSGROUP);
UsrCfg.InternoAtisNames.ATROLE.NAME_TABLE = "ATROLE"; 
UsrCfg.InternoAtisNames.ATROLE.IDPBITEMPLATE.FieldName = "IDPBITEMPLATE"; 
UsrCfg.InternoAtisNames.ATROLE.IDPBITEMPLATE.TableName = "ATROLE"; 
UsrCfg.InternoAtisNames.ATROLE.IDPBITEMPLATE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.ATROLE.IDPBITEMPLATE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ATROLE.IDPBITEMPLATE);
UsrCfg.InternoAtisNames.ATROLE.NAME_TABLE = "ATROLE"; 
UsrCfg.InternoAtisNames.ATROLE.SETHOME.FieldName = "SETHOME"; 
UsrCfg.InternoAtisNames.ATROLE.SETHOME.TableName = "ATROLE"; 
UsrCfg.InternoAtisNames.ATROLE.SETHOME.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.ATROLE.SETHOME.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ATROLE.SETHOME);
UsrCfg.InternoAtisNames.ATROLE.NAME_TABLE = "ATROLE"; 
UsrCfg.InternoAtisNames.ATROLE.PBITEMPLATEMULTIPLE.FieldName = "PBITEMPLATEMULTIPLE"; 
UsrCfg.InternoAtisNames.ATROLE.PBITEMPLATEMULTIPLE.TableName = "ATROLE"; 
UsrCfg.InternoAtisNames.ATROLE.PBITEMPLATEMULTIPLE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.ATROLE.PBITEMPLATEMULTIPLE.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ATROLE.PBITEMPLATEMULTIPLE);
UsrCfg.InternoAtisNames.ATROLE.NAME_TABLE = "ATROLE"; 
UsrCfg.InternoAtisNames.ATROLE.PSGROUPMULTIPLE.FieldName = "PSGROUPMULTIPLE"; 
UsrCfg.InternoAtisNames.ATROLE.PSGROUPMULTIPLE.TableName = "ATROLE"; 
UsrCfg.InternoAtisNames.ATROLE.PSGROUPMULTIPLE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.ATROLE.PSGROUPMULTIPLE.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ATROLE.PSGROUPMULTIPLE);
UsrCfg.InternoAtisNames.ATMODULE.NAME_TABLE = "ATMODULE"; 
UsrCfg.InternoAtisNames.ATMODULE.IDATMODULE.FieldName = "IDATMODULE"; 
UsrCfg.InternoAtisNames.ATMODULE.IDATMODULE.TableName = "ATMODULE"; 
UsrCfg.InternoAtisNames.ATMODULE.IDATMODULE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.ATMODULE.IDATMODULE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ATMODULE.IDATMODULE);
UsrCfg.InternoAtisNames.ATMODULE.NAME_TABLE = "ATMODULE"; 
UsrCfg.InternoAtisNames.ATMODULE.MODULENAME.FieldName = "MODULENAME"; 
UsrCfg.InternoAtisNames.ATMODULE.MODULENAME.TableName = "ATMODULE"; 
UsrCfg.InternoAtisNames.ATMODULE.MODULENAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.ATMODULE.MODULENAME.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ATMODULE.MODULENAME);
UsrCfg.InternoAtisNames.ATMODULE.NAME_TABLE = "ATMODULE"; 
UsrCfg.InternoAtisNames.ATMODULE.STATUS.FieldName = "STATUS"; 
UsrCfg.InternoAtisNames.ATMODULE.STATUS.TableName = "ATMODULE"; 
UsrCfg.InternoAtisNames.ATMODULE.STATUS.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.ATMODULE.STATUS.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ATMODULE.STATUS);
UsrCfg.InternoAtisNames.ATROLEMODULE.NAME_TABLE = "ATROLEMODULE"; 
UsrCfg.InternoAtisNames.ATROLEMODULE.IDATROLEMODULE.FieldName = "IDATROLEMODULE"; 
UsrCfg.InternoAtisNames.ATROLEMODULE.IDATROLEMODULE.TableName = "ATROLEMODULE"; 
UsrCfg.InternoAtisNames.ATROLEMODULE.IDATROLEMODULE.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.ATROLEMODULE.IDATROLEMODULE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ATROLEMODULE.IDATROLEMODULE);
UsrCfg.InternoAtisNames.ATROLEMODULE.NAME_TABLE = "ATROLEMODULE"; 
UsrCfg.InternoAtisNames.ATROLEMODULE.IDATROLE.FieldName = "IDATROLE"; 
UsrCfg.InternoAtisNames.ATROLEMODULE.IDATROLE.TableName = "ATROLEMODULE"; 
UsrCfg.InternoAtisNames.ATROLEMODULE.IDATROLE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.ATROLEMODULE.IDATROLE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ATROLEMODULE.IDATROLE);
UsrCfg.InternoAtisNames.ATROLEMODULE.NAME_TABLE = "ATROLEMODULE"; 
UsrCfg.InternoAtisNames.ATROLEMODULE.IDATMODULE.FieldName = "IDATMODULE"; 
UsrCfg.InternoAtisNames.ATROLEMODULE.IDATMODULE.TableName = "ATROLEMODULE"; 
UsrCfg.InternoAtisNames.ATROLEMODULE.IDATMODULE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.ATROLEMODULE.IDATMODULE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ATROLEMODULE.IDATMODULE);
UsrCfg.InternoAtisNames.ATROLEMODULE.NAME_TABLE = "ATROLEMODULE"; 
UsrCfg.InternoAtisNames.ATROLEMODULE.REQUEST.FieldName = "REQUEST"; 
UsrCfg.InternoAtisNames.ATROLEMODULE.REQUEST.TableName = "ATROLEMODULE"; 
UsrCfg.InternoAtisNames.ATROLEMODULE.REQUEST.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.ATROLEMODULE.REQUEST.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ATROLEMODULE.REQUEST);
UsrCfg.InternoAtisNames.CMDBUSER.NAME_TABLE = "CMDBUSER"; 
UsrCfg.InternoAtisNames.CMDBUSER.IDCMDBUSER.FieldName = "IDCMDBUSER"; 
UsrCfg.InternoAtisNames.CMDBUSER.IDCMDBUSER.TableName = "CMDBUSER"; 
UsrCfg.InternoAtisNames.CMDBUSER.IDCMDBUSER.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.CMDBUSER.IDCMDBUSER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBUSER.IDCMDBUSER);
UsrCfg.InternoAtisNames.CMDBUSER.NAME_TABLE = "CMDBUSER"; 
UsrCfg.InternoAtisNames.CMDBUSER.IDCMDBCI.FieldName = "IDCMDBCI"; 
UsrCfg.InternoAtisNames.CMDBUSER.IDCMDBCI.TableName = "CMDBUSER"; 
UsrCfg.InternoAtisNames.CMDBUSER.IDCMDBCI.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.CMDBUSER.IDCMDBCI.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBUSER.IDCMDBCI);
UsrCfg.InternoAtisNames.CMDBUSER.NAME_TABLE = "CMDBUSER"; 
UsrCfg.InternoAtisNames.CMDBUSER.IDATROLE.FieldName = "IDATROLE"; 
UsrCfg.InternoAtisNames.CMDBUSER.IDATROLE.TableName = "CMDBUSER"; 
UsrCfg.InternoAtisNames.CMDBUSER.IDATROLE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.CMDBUSER.IDATROLE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBUSER.IDATROLE);
UsrCfg.InternoAtisNames.CMDBUSER.NAME_TABLE = "CMDBUSER"; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD.FieldName = "PASSWORD"; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD.TableName = "CMDBUSER"; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD);
UsrCfg.InternoAtisNames.CMDBUSER.NAME_TABLE = "CMDBUSER"; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD01.FieldName = "PASSWORD01"; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD01.TableName = "CMDBUSER"; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD01.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD01.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD01);
UsrCfg.InternoAtisNames.CMDBUSER.NAME_TABLE = "CMDBUSER"; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD02.FieldName = "PASSWORD02"; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD02.TableName = "CMDBUSER"; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD02.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD02.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD02);
UsrCfg.InternoAtisNames.CMDBUSER.NAME_TABLE = "CMDBUSER"; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD03.FieldName = "PASSWORD03"; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD03.TableName = "CMDBUSER"; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD03.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD03.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD03);
UsrCfg.InternoAtisNames.CMDBUSER.NAME_TABLE = "CMDBUSER"; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD04.FieldName = "PASSWORD04"; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD04.TableName = "CMDBUSER"; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD04.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD04.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD04);
UsrCfg.InternoAtisNames.CMDBUSER.NAME_TABLE = "CMDBUSER"; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD05.FieldName = "PASSWORD05"; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD05.TableName = "CMDBUSER"; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD05.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD05.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD05);
UsrCfg.InternoAtisNames.CMDBUSER.NAME_TABLE = "CMDBUSER"; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD06.FieldName = "PASSWORD06"; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD06.TableName = "CMDBUSER"; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD06.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD06.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD06);
UsrCfg.InternoAtisNames.CMDBUSER.NAME_TABLE = "CMDBUSER"; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD07.FieldName = "PASSWORD07"; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD07.TableName = "CMDBUSER"; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD07.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD07.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD07);
UsrCfg.InternoAtisNames.CMDBUSER.NAME_TABLE = "CMDBUSER"; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD08.FieldName = "PASSWORD08"; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD08.TableName = "CMDBUSER"; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD08.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD08.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD08);
UsrCfg.InternoAtisNames.CMDBUSER.NAME_TABLE = "CMDBUSER"; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD09.FieldName = "PASSWORD09"; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD09.TableName = "CMDBUSER"; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD09.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD09.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD09);
UsrCfg.InternoAtisNames.CMDBUSER.NAME_TABLE = "CMDBUSER"; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD10.FieldName = "PASSWORD10"; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD10.TableName = "CMDBUSER"; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD10.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD10.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBUSER.PASSWORD10);
UsrCfg.InternoAtisNames.CMDBUSER.NAME_TABLE = "CMDBUSER"; 
UsrCfg.InternoAtisNames.CMDBUSER.LASTPWDCHANGE.FieldName = "LASTPWDCHANGE"; 
UsrCfg.InternoAtisNames.CMDBUSER.LASTPWDCHANGE.TableName = "CMDBUSER"; 
UsrCfg.InternoAtisNames.CMDBUSER.LASTPWDCHANGE.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.CMDBUSER.LASTPWDCHANGE.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBUSER.LASTPWDCHANGE);
UsrCfg.InternoAtisNames.CMDBUSER.NAME_TABLE = "CMDBUSER"; 
UsrCfg.InternoAtisNames.CMDBUSER.IDMDCALENDARYDATE.FieldName = "IDMDCALENDARYDATE"; 
UsrCfg.InternoAtisNames.CMDBUSER.IDMDCALENDARYDATE.TableName = "CMDBUSER"; 
UsrCfg.InternoAtisNames.CMDBUSER.IDMDCALENDARYDATE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.CMDBUSER.IDMDCALENDARYDATE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBUSER.IDMDCALENDARYDATE);
UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.NAME_TABLE = "CMDBUSERSETTINGS"; 
UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDCMDBUSERSETTINGS.FieldName = "IDCMDBUSERSETTINGS"; 
UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDCMDBUSERSETTINGS.TableName = "CMDBUSERSETTINGS"; 
UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDCMDBUSERSETTINGS.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDCMDBUSERSETTINGS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDCMDBUSERSETTINGS);
UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.NAME_TABLE = "CMDBUSERSETTINGS"; 
UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDCMDBUSER.FieldName = "IDCMDBUSER"; 
UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDCMDBUSER.TableName = "CMDBUSERSETTINGS"; 
UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDCMDBUSER.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDCMDBUSER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDCMDBUSER);
UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.NAME_TABLE = "CMDBUSERSETTINGS"; 
UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDLANGUAGE.FieldName = "IDLANGUAGE"; 
UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDLANGUAGE.TableName = "CMDBUSERSETTINGS"; 
UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDLANGUAGE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDLANGUAGE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDLANGUAGE);
UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.NAME_TABLE = "CMDBUSERSETTINGS"; 
UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDSKINATIS.FieldName = "IDSKINATIS"; 
UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDSKINATIS.TableName = "CMDBUSERSETTINGS"; 
UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDSKINATIS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDSKINATIS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDSKINATIS);
UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.NAME_TABLE = "CMDBUSERSETTINGS"; 
UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDSKINITHELPCENTER.FieldName = "IDSKINITHELPCENTER"; 
UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDSKINITHELPCENTER.TableName = "CMDBUSERSETTINGS"; 
UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDSKINITHELPCENTER.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDSKINITHELPCENTER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.IDSKINITHELPCENTER);
UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.NAME_TABLE = "CMDBUSERSETTINGS"; 
UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.ATENLAYOUT_IDFILE_ATIS.FieldName = "ATENLAYOUT_IDFILE_ATIS"; 
UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.ATENLAYOUT_IDFILE_ATIS.TableName = "CMDBUSERSETTINGS"; 
UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.ATENLAYOUT_IDFILE_ATIS.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.ATENLAYOUT_IDFILE_ATIS.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.ATENLAYOUT_IDFILE_ATIS);
UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.NAME_TABLE = "CMDBUSERSETTINGS"; 
UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.CONSOLEDETAIL_COLUMN_ATIS.FieldName = "CONSOLEDETAIL_COLUMN_ATIS"; 
UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.CONSOLEDETAIL_COLUMN_ATIS.TableName = "CMDBUSERSETTINGS"; 
UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.CONSOLEDETAIL_COLUMN_ATIS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.CONSOLEDETAIL_COLUMN_ATIS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBUSERSETTINGS.CONSOLEDETAIL_COLUMN_ATIS);
UsrCfg.InternoAtisNames.ATROLEGPQUERY.NAME_TABLE = "ATROLEGPQUERY"; 
UsrCfg.InternoAtisNames.ATROLEGPQUERY.IDATROLEGPQUERY.FieldName = "IDATROLEGPQUERY"; 
UsrCfg.InternoAtisNames.ATROLEGPQUERY.IDATROLEGPQUERY.TableName = "ATROLEGPQUERY"; 
UsrCfg.InternoAtisNames.ATROLEGPQUERY.IDATROLEGPQUERY.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.ATROLEGPQUERY.IDATROLEGPQUERY.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ATROLEGPQUERY.IDATROLEGPQUERY);
UsrCfg.InternoAtisNames.ATROLEGPQUERY.NAME_TABLE = "ATROLEGPQUERY"; 
UsrCfg.InternoAtisNames.ATROLEGPQUERY.IDATROLE.FieldName = "IDATROLE"; 
UsrCfg.InternoAtisNames.ATROLEGPQUERY.IDATROLE.TableName = "ATROLEGPQUERY"; 
UsrCfg.InternoAtisNames.ATROLEGPQUERY.IDATROLE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.ATROLEGPQUERY.IDATROLE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ATROLEGPQUERY.IDATROLE);
UsrCfg.InternoAtisNames.ATROLEGPQUERY.NAME_TABLE = "ATROLEGPQUERY"; 
UsrCfg.InternoAtisNames.ATROLEGPQUERY.IDGPQUERY.FieldName = "IDGPQUERY"; 
UsrCfg.InternoAtisNames.ATROLEGPQUERY.IDGPQUERY.TableName = "ATROLEGPQUERY"; 
UsrCfg.InternoAtisNames.ATROLEGPQUERY.IDGPQUERY.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.ATROLEGPQUERY.IDGPQUERY.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ATROLEGPQUERY.IDGPQUERY);
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUT.NAME_TABLE = "ATROLEGPQUERYGROUPLAYOUT"; 
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUT.IDATROLEGPQUERYGROUPLAYOUT.FieldName = "IDATROLEGPQUERYGROUPLAYOUT"; 
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUT.IDATROLEGPQUERYGROUPLAYOUT.TableName = "ATROLEGPQUERYGROUPLAYOUT"; 
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUT.IDATROLEGPQUERYGROUPLAYOUT.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUT.IDATROLEGPQUERYGROUPLAYOUT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUT.IDATROLEGPQUERYGROUPLAYOUT);
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUT.NAME_TABLE = "ATROLEGPQUERYGROUPLAYOUT"; 
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUT.STYLE.FieldName = "STYLE"; 
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUT.STYLE.TableName = "ATROLEGPQUERYGROUPLAYOUT"; 
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUT.STYLE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUT.STYLE.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUT.STYLE);
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUT.NAME_TABLE = "ATROLEGPQUERYGROUPLAYOUT"; 
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUT.LAYOUT_CUSTOMCODE_ATIS.FieldName = "LAYOUT_CUSTOMCODE_ATIS"; 
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUT.LAYOUT_CUSTOMCODE_ATIS.TableName = "ATROLEGPQUERYGROUPLAYOUT"; 
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUT.LAYOUT_CUSTOMCODE_ATIS.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUT.LAYOUT_CUSTOMCODE_ATIS.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUT.LAYOUT_CUSTOMCODE_ATIS);
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUT.NAME_TABLE = "ATROLEGPQUERYGROUPLAYOUT"; 
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUT.NAME.FieldName = "NAME"; 
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUT.NAME.TableName = "ATROLEGPQUERYGROUPLAYOUT"; 
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUT.NAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUT.NAME.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUT.NAME);
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUT.NAME_TABLE = "ATROLEGPQUERYGROUPLAYOUT"; 
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUT.DESCRIPTION.FieldName = "DESCRIPTION"; 
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUT.DESCRIPTION.TableName = "ATROLEGPQUERYGROUPLAYOUT"; 
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUT.DESCRIPTION.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUT.DESCRIPTION.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUT.DESCRIPTION);
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUT.NAME_TABLE = "ATROLEGPQUERYGROUPLAYOUT"; 
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUT.PATH.FieldName = "PATH"; 
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUT.PATH.TableName = "ATROLEGPQUERYGROUPLAYOUT"; 
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUT.PATH.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUT.PATH.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUT.PATH);
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUTDET.NAME_TABLE = "ATROLEGPQUERYGROUPLAYOUTDET"; 
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUTDET.IDATROLEGPQUERYGROUPLAYOUTDET.FieldName = "IDATROLEGPQUERYGROUPLAYOUTDET"; 
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUTDET.IDATROLEGPQUERYGROUPLAYOUTDET.TableName = "ATROLEGPQUERYGROUPLAYOUTDET"; 
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUTDET.IDATROLEGPQUERYGROUPLAYOUTDET.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUTDET.IDATROLEGPQUERYGROUPLAYOUTDET.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUTDET.IDATROLEGPQUERYGROUPLAYOUTDET);
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUTDET.NAME_TABLE = "ATROLEGPQUERYGROUPLAYOUTDET"; 
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUTDET.IDATROLEGPQUERYGROUPLAYOUT.FieldName = "IDATROLEGPQUERYGROUPLAYOUT"; 
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUTDET.IDATROLEGPQUERYGROUPLAYOUT.TableName = "ATROLEGPQUERYGROUPLAYOUTDET"; 
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUTDET.IDATROLEGPQUERYGROUPLAYOUT.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUTDET.IDATROLEGPQUERYGROUPLAYOUT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUTDET.IDATROLEGPQUERYGROUPLAYOUT);
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUTDET.NAME_TABLE = "ATROLEGPQUERYGROUPLAYOUTDET"; 
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUTDET.IDGPQUERY.FieldName = "IDGPQUERY"; 
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUTDET.IDGPQUERY.TableName = "ATROLEGPQUERYGROUPLAYOUTDET"; 
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUTDET.IDGPQUERY.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUTDET.IDGPQUERY.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUTDET.IDGPQUERY);
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUTDET.NAME_TABLE = "ATROLEGPQUERYGROUPLAYOUTDET"; 
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUTDET.AUTOREFRESHTIME.FieldName = "AUTOREFRESHTIME"; 
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUTDET.AUTOREFRESHTIME.TableName = "ATROLEGPQUERYGROUPLAYOUTDET"; 
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUTDET.AUTOREFRESHTIME.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUTDET.AUTOREFRESHTIME.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ATROLEGPQUERYGROUPLAYOUTDET.AUTOREFRESHTIME);
UsrCfg.InternoAtisNames.ATROLEDASHBOARD.NAME_TABLE = "ATROLEDASHBOARD"; 
UsrCfg.InternoAtisNames.ATROLEDASHBOARD.IDATROLEDASHBOARD.FieldName = "IDATROLEDASHBOARD"; 
UsrCfg.InternoAtisNames.ATROLEDASHBOARD.IDATROLEDASHBOARD.TableName = "ATROLEDASHBOARD"; 
UsrCfg.InternoAtisNames.ATROLEDASHBOARD.IDATROLEDASHBOARD.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.ATROLEDASHBOARD.IDATROLEDASHBOARD.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ATROLEDASHBOARD.IDATROLEDASHBOARD);
UsrCfg.InternoAtisNames.ATROLEDASHBOARD.NAME_TABLE = "ATROLEDASHBOARD"; 
UsrCfg.InternoAtisNames.ATROLEDASHBOARD.IDATROLE.FieldName = "IDATROLE"; 
UsrCfg.InternoAtisNames.ATROLEDASHBOARD.IDATROLE.TableName = "ATROLEDASHBOARD"; 
UsrCfg.InternoAtisNames.ATROLEDASHBOARD.IDATROLE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.ATROLEDASHBOARD.IDATROLE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ATROLEDASHBOARD.IDATROLE);
UsrCfg.InternoAtisNames.ATROLEDASHBOARD.NAME_TABLE = "ATROLEDASHBOARD"; 
UsrCfg.InternoAtisNames.ATROLEDASHBOARD.IDATROLEGPQUERYGROUPLAYOUT.FieldName = "IDATROLEGPQUERYGROUPLAYOUT"; 
UsrCfg.InternoAtisNames.ATROLEDASHBOARD.IDATROLEGPQUERYGROUPLAYOUT.TableName = "ATROLEDASHBOARD"; 
UsrCfg.InternoAtisNames.ATROLEDASHBOARD.IDATROLEGPQUERYGROUPLAYOUT.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.ATROLEDASHBOARD.IDATROLEGPQUERYGROUPLAYOUT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ATROLEDASHBOARD.IDATROLEGPQUERYGROUPLAYOUT);
UsrCfg.InternoAtisNames.CMDBUSER_GROUPLAYOUT.NAME_TABLE = "CMDBUSER_GROUPLAYOUT"; 
UsrCfg.InternoAtisNames.CMDBUSER_GROUPLAYOUT.IDCMDBUSER_GROUPLAYOUT.FieldName = "IDCMDBUSER_GROUPLAYOUT"; 
UsrCfg.InternoAtisNames.CMDBUSER_GROUPLAYOUT.IDCMDBUSER_GROUPLAYOUT.TableName = "CMDBUSER_GROUPLAYOUT"; 
UsrCfg.InternoAtisNames.CMDBUSER_GROUPLAYOUT.IDCMDBUSER_GROUPLAYOUT.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.CMDBUSER_GROUPLAYOUT.IDCMDBUSER_GROUPLAYOUT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBUSER_GROUPLAYOUT.IDCMDBUSER_GROUPLAYOUT);
UsrCfg.InternoAtisNames.CMDBUSER_GROUPLAYOUT.NAME_TABLE = "CMDBUSER_GROUPLAYOUT"; 
UsrCfg.InternoAtisNames.CMDBUSER_GROUPLAYOUT.IDATROLEGPQUERYGROUPLAYOUT.FieldName = "IDATROLEGPQUERYGROUPLAYOUT"; 
UsrCfg.InternoAtisNames.CMDBUSER_GROUPLAYOUT.IDATROLEGPQUERYGROUPLAYOUT.TableName = "CMDBUSER_GROUPLAYOUT"; 
UsrCfg.InternoAtisNames.CMDBUSER_GROUPLAYOUT.IDATROLEGPQUERYGROUPLAYOUT.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.CMDBUSER_GROUPLAYOUT.IDATROLEGPQUERYGROUPLAYOUT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBUSER_GROUPLAYOUT.IDATROLEGPQUERYGROUPLAYOUT);
UsrCfg.InternoAtisNames.CMDBUSER_GROUPLAYOUT.NAME_TABLE = "CMDBUSER_GROUPLAYOUT"; 
UsrCfg.InternoAtisNames.CMDBUSER_GROUPLAYOUT.IDCMDBUSER.FieldName = "IDCMDBUSER"; 
UsrCfg.InternoAtisNames.CMDBUSER_GROUPLAYOUT.IDCMDBUSER.TableName = "CMDBUSER_GROUPLAYOUT"; 
UsrCfg.InternoAtisNames.CMDBUSER_GROUPLAYOUT.IDCMDBUSER.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.CMDBUSER_GROUPLAYOUT.IDCMDBUSER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBUSER_GROUPLAYOUT.IDCMDBUSER);
UsrCfg.InternoAtisNames.CMDBUSER_GROUPLAYOUT.NAME_TABLE = "CMDBUSER_GROUPLAYOUT"; 
UsrCfg.InternoAtisNames.CMDBUSER_GROUPLAYOUT.ID_FILE.FieldName = "ID_FILE"; 
UsrCfg.InternoAtisNames.CMDBUSER_GROUPLAYOUT.ID_FILE.TableName = "CMDBUSER_GROUPLAYOUT"; 
UsrCfg.InternoAtisNames.CMDBUSER_GROUPLAYOUT.ID_FILE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.CMDBUSER_GROUPLAYOUT.ID_FILE.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.CMDBUSER_GROUPLAYOUT.ID_FILE);
UsrCfg.InternoAtisNames.ATROLEPBITEMPLATE.NAME_TABLE = "ATROLEPBITEMPLATE"; 
UsrCfg.InternoAtisNames.ATROLEPBITEMPLATE.IDATROLEPBITEMPLATE.FieldName = "IDATROLEPBITEMPLATE"; 
UsrCfg.InternoAtisNames.ATROLEPBITEMPLATE.IDATROLEPBITEMPLATE.TableName = "ATROLEPBITEMPLATE"; 
UsrCfg.InternoAtisNames.ATROLEPBITEMPLATE.IDATROLEPBITEMPLATE.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.ATROLEPBITEMPLATE.IDATROLEPBITEMPLATE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ATROLEPBITEMPLATE.IDATROLEPBITEMPLATE);
UsrCfg.InternoAtisNames.ATROLEPBITEMPLATE.NAME_TABLE = "ATROLEPBITEMPLATE"; 
UsrCfg.InternoAtisNames.ATROLEPBITEMPLATE.IDATROLE.FieldName = "IDATROLE"; 
UsrCfg.InternoAtisNames.ATROLEPBITEMPLATE.IDATROLE.TableName = "ATROLEPBITEMPLATE"; 
UsrCfg.InternoAtisNames.ATROLEPBITEMPLATE.IDATROLE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.ATROLEPBITEMPLATE.IDATROLE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ATROLEPBITEMPLATE.IDATROLE);
UsrCfg.InternoAtisNames.ATROLEPBITEMPLATE.NAME_TABLE = "ATROLEPBITEMPLATE"; 
UsrCfg.InternoAtisNames.ATROLEPBITEMPLATE.IDPBITEMPLATE.FieldName = "IDPBITEMPLATE"; 
UsrCfg.InternoAtisNames.ATROLEPBITEMPLATE.IDPBITEMPLATE.TableName = "ATROLEPBITEMPLATE"; 
UsrCfg.InternoAtisNames.ATROLEPBITEMPLATE.IDPBITEMPLATE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.ATROLEPBITEMPLATE.IDPBITEMPLATE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ATROLEPBITEMPLATE.IDPBITEMPLATE);
UsrCfg.InternoAtisNames.ATROLEPSGROUP.NAME_TABLE = "ATROLEPSGROUP"; 
UsrCfg.InternoAtisNames.ATROLEPSGROUP.IDATROLEPSGROUP.FieldName = "IDATROLEPSGROUP"; 
UsrCfg.InternoAtisNames.ATROLEPSGROUP.IDATROLEPSGROUP.TableName = "ATROLEPSGROUP"; 
UsrCfg.InternoAtisNames.ATROLEPSGROUP.IDATROLEPSGROUP.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.ATROLEPSGROUP.IDATROLEPSGROUP.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ATROLEPSGROUP.IDATROLEPSGROUP);
UsrCfg.InternoAtisNames.ATROLEPSGROUP.NAME_TABLE = "ATROLEPSGROUP"; 
UsrCfg.InternoAtisNames.ATROLEPSGROUP.IDATROLE.FieldName = "IDATROLE"; 
UsrCfg.InternoAtisNames.ATROLEPSGROUP.IDATROLE.TableName = "ATROLEPSGROUP"; 
UsrCfg.InternoAtisNames.ATROLEPSGROUP.IDATROLE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.ATROLEPSGROUP.IDATROLE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ATROLEPSGROUP.IDATROLE);
UsrCfg.InternoAtisNames.ATROLEPSGROUP.NAME_TABLE = "ATROLEPSGROUP"; 
UsrCfg.InternoAtisNames.ATROLEPSGROUP.IDPSGROUP.FieldName = "IDPSGROUP"; 
UsrCfg.InternoAtisNames.ATROLEPSGROUP.IDPSGROUP.TableName = "ATROLEPSGROUP"; 
UsrCfg.InternoAtisNames.ATROLEPSGROUP.IDPSGROUP.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.ATROLEPSGROUP.IDPSGROUP.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.ATROLEPSGROUP.IDPSGROUP);
UsrCfg.InternoAtisNames.SDNOTIFYTYPEEVENT.NAME_TABLE = "SDNOTIFYTYPEEVENT"; 
UsrCfg.InternoAtisNames.SDNOTIFYTYPEEVENT.IDSDNOTIFYTYPEEVENT.FieldName = "IDSDNOTIFYTYPEEVENT"; 
UsrCfg.InternoAtisNames.SDNOTIFYTYPEEVENT.IDSDNOTIFYTYPEEVENT.TableName = "SDNOTIFYTYPEEVENT"; 
UsrCfg.InternoAtisNames.SDNOTIFYTYPEEVENT.IDSDNOTIFYTYPEEVENT.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDNOTIFYTYPEEVENT.IDSDNOTIFYTYPEEVENT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDNOTIFYTYPEEVENT.IDSDNOTIFYTYPEEVENT);
UsrCfg.InternoAtisNames.SDNOTIFYTYPEEVENT.NAME_TABLE = "SDNOTIFYTYPEEVENT"; 
UsrCfg.InternoAtisNames.SDNOTIFYTYPEEVENT.TYPEEVENTNAME.FieldName = "TYPEEVENTNAME"; 
UsrCfg.InternoAtisNames.SDNOTIFYTYPEEVENT.TYPEEVENTNAME.TableName = "SDNOTIFYTYPEEVENT"; 
UsrCfg.InternoAtisNames.SDNOTIFYTYPEEVENT.TYPEEVENTNAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.SDNOTIFYTYPEEVENT.TYPEEVENTNAME.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDNOTIFYTYPEEVENT.TYPEEVENTNAME);
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.NAME_TABLE = "SDNOTIFYTEMPLATE"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.IDSDNOTIFYTEMPLATE.FieldName = "IDSDNOTIFYTEMPLATE"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.IDSDNOTIFYTEMPLATE.TableName = "SDNOTIFYTEMPLATE"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.IDSDNOTIFYTEMPLATE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.IDSDNOTIFYTEMPLATE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.IDSDNOTIFYTEMPLATE);
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.NAME_TABLE = "SDNOTIFYTEMPLATE"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.TEMPLATE_NAME.FieldName = "TEMPLATE_NAME"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.TEMPLATE_NAME.TableName = "SDNOTIFYTEMPLATE"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.TEMPLATE_NAME.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.TEMPLATE_NAME.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.TEMPLATE_NAME);
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.NAME_TABLE = "SDNOTIFYTEMPLATE"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.DESCRIPTION.FieldName = "DESCRIPTION"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.DESCRIPTION.TableName = "SDNOTIFYTEMPLATE"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.DESCRIPTION.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.DESCRIPTION.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.DESCRIPTION);
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.NAME_TABLE = "SDNOTIFYTEMPLATE"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.STATUS.FieldName = "STATUS"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.STATUS.TableName = "SDNOTIFYTEMPLATE"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.STATUS.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.STATUS.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.STATUS);
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.NAME_TABLE = "SDNOTIFYTEMPLATE"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.ISEMAILSQL.FieldName = "ISEMAILSQL"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.ISEMAILSQL.TableName = "SDNOTIFYTEMPLATE"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.ISEMAILSQL.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.ISEMAILSQL.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.ISEMAILSQL);
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.NAME_TABLE = "SDNOTIFYTEMPLATE"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.EMAILSQL.FieldName = "EMAILSQL"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.EMAILSQL.TableName = "SDNOTIFYTEMPLATE"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.EMAILSQL.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.EMAILSQL.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.EMAILSQL);
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.NAME_TABLE = "SDNOTIFYTEMPLATE"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.EMAILTEMPLATE.FieldName = "EMAILTEMPLATE"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.EMAILTEMPLATE.TableName = "SDNOTIFYTEMPLATE"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.EMAILTEMPLATE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.EMAILTEMPLATE.Size = 200; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE.EMAILTEMPLATE);
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.NAME_TABLE = "SDNOTIFYTEMPLATE_TYPEUSER"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.IDSDNOTIFYTEMPLATE_TYPEUSER.FieldName = "IDSDNOTIFYTEMPLATE_TYPEUSER"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.IDSDNOTIFYTEMPLATE_TYPEUSER.TableName = "SDNOTIFYTEMPLATE_TYPEUSER"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.IDSDNOTIFYTEMPLATE_TYPEUSER.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.IDSDNOTIFYTEMPLATE_TYPEUSER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.IDSDNOTIFYTEMPLATE_TYPEUSER);
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.NAME_TABLE = "SDNOTIFYTEMPLATE_TYPEUSER"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.IDSDNOTIFYTYPEEVENT.FieldName = "IDSDNOTIFYTYPEEVENT"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.IDSDNOTIFYTYPEEVENT.TableName = "SDNOTIFYTEMPLATE_TYPEUSER"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.IDSDNOTIFYTYPEEVENT.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.IDSDNOTIFYTYPEEVENT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.IDSDNOTIFYTYPEEVENT);
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.NAME_TABLE = "SDNOTIFYTEMPLATE_TYPEUSER"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.IDSDNOTIFYTEMPLATE.FieldName = "IDSDNOTIFYTEMPLATE"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.IDSDNOTIFYTEMPLATE.TableName = "SDNOTIFYTEMPLATE_TYPEUSER"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.IDSDNOTIFYTEMPLATE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.IDSDNOTIFYTEMPLATE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.IDSDNOTIFYTEMPLATE);
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.NAME_TABLE = "SDNOTIFYTEMPLATE_TYPEUSER"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.IDSDTYPEUSER.FieldName = "IDSDTYPEUSER"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.IDSDTYPEUSER.TableName = "SDNOTIFYTEMPLATE_TYPEUSER"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.IDSDTYPEUSER.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.IDSDTYPEUSER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.IDSDTYPEUSER);
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.NAME_TABLE = "SDNOTIFYTEMPLATE_TYPEUSER"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.SENDCONSOLE.FieldName = "SENDCONSOLE"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.SENDCONSOLE.TableName = "SDNOTIFYTEMPLATE_TYPEUSER"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.SENDCONSOLE.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.SENDCONSOLE.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.SENDCONSOLE);
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.NAME_TABLE = "SDNOTIFYTEMPLATE_TYPEUSER"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.SENDEMAIL.FieldName = "SENDEMAIL"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.SENDEMAIL.TableName = "SDNOTIFYTEMPLATE_TYPEUSER"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.SENDEMAIL.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.SENDEMAIL.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.SENDEMAIL);
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.NAME_TABLE = "SDNOTIFYTEMPLATE_TYPEUSER"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.IDMDSERVICETYPE.FieldName = "IDMDSERVICETYPE"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.IDMDSERVICETYPE.TableName = "SDNOTIFYTEMPLATE_TYPEUSER"; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.IDMDSERVICETYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.IDMDSERVICETYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDNOTIFYTEMPLATE_TYPEUSER.IDMDSERVICETYPE);
UsrCfg.InternoAtisNames.SDNOTIFY.NAME_TABLE = "SDNOTIFY"; 
UsrCfg.InternoAtisNames.SDNOTIFY.IDSDNOTIFY.FieldName = "IDSDNOTIFY"; 
UsrCfg.InternoAtisNames.SDNOTIFY.IDSDNOTIFY.TableName = "SDNOTIFY"; 
UsrCfg.InternoAtisNames.SDNOTIFY.IDSDNOTIFY.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.SDNOTIFY.IDSDNOTIFY.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDNOTIFY.IDSDNOTIFY);
UsrCfg.InternoAtisNames.SDNOTIFY.NAME_TABLE = "SDNOTIFY"; 
UsrCfg.InternoAtisNames.SDNOTIFY.IDSDCASE.FieldName = "IDSDCASE"; 
UsrCfg.InternoAtisNames.SDNOTIFY.IDSDCASE.TableName = "SDNOTIFY"; 
UsrCfg.InternoAtisNames.SDNOTIFY.IDSDCASE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDNOTIFY.IDSDCASE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDNOTIFY.IDSDCASE);
UsrCfg.InternoAtisNames.SDNOTIFY.NAME_TABLE = "SDNOTIFY"; 
UsrCfg.InternoAtisNames.SDNOTIFY.EVENTDATE.FieldName = "EVENTDATE"; 
UsrCfg.InternoAtisNames.SDNOTIFY.EVENTDATE.TableName = "SDNOTIFY"; 
UsrCfg.InternoAtisNames.SDNOTIFY.EVENTDATE.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.SDNOTIFY.EVENTDATE.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDNOTIFY.EVENTDATE);
UsrCfg.InternoAtisNames.SDNOTIFY.NAME_TABLE = "SDNOTIFY"; 
UsrCfg.InternoAtisNames.SDNOTIFY.IDSDNOTIFYTEMPLATE.FieldName = "IDSDNOTIFYTEMPLATE"; 
UsrCfg.InternoAtisNames.SDNOTIFY.IDSDNOTIFYTEMPLATE.TableName = "SDNOTIFY"; 
UsrCfg.InternoAtisNames.SDNOTIFY.IDSDNOTIFYTEMPLATE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDNOTIFY.IDSDNOTIFYTEMPLATE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDNOTIFY.IDSDNOTIFYTEMPLATE);
UsrCfg.InternoAtisNames.SDNOTIFY.NAME_TABLE = "SDNOTIFY"; 
UsrCfg.InternoAtisNames.SDNOTIFY.CHANGE_DESCRIPTION.FieldName = "CHANGE_DESCRIPTION"; 
UsrCfg.InternoAtisNames.SDNOTIFY.CHANGE_DESCRIPTION.TableName = "SDNOTIFY"; 
UsrCfg.InternoAtisNames.SDNOTIFY.CHANGE_DESCRIPTION.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.SDNOTIFY.CHANGE_DESCRIPTION.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDNOTIFY.CHANGE_DESCRIPTION);
UsrCfg.InternoAtisNames.SDNOTIFY.NAME_TABLE = "SDNOTIFY"; 
UsrCfg.InternoAtisNames.SDNOTIFY.CHANGE_EMAILSQL.FieldName = "CHANGE_EMAILSQL"; 
UsrCfg.InternoAtisNames.SDNOTIFY.CHANGE_EMAILSQL.TableName = "SDNOTIFY"; 
UsrCfg.InternoAtisNames.SDNOTIFY.CHANGE_EMAILSQL.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.SDNOTIFY.CHANGE_EMAILSQL.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDNOTIFY.CHANGE_EMAILSQL);
UsrCfg.InternoAtisNames.SDNOTIFY.NAME_TABLE = "SDNOTIFY"; 
UsrCfg.InternoAtisNames.SDNOTIFY.ISSENDCONSOLE.FieldName = "ISSENDCONSOLE"; 
UsrCfg.InternoAtisNames.SDNOTIFY.ISSENDCONSOLE.TableName = "SDNOTIFY"; 
UsrCfg.InternoAtisNames.SDNOTIFY.ISSENDCONSOLE.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.SDNOTIFY.ISSENDCONSOLE.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDNOTIFY.ISSENDCONSOLE);
UsrCfg.InternoAtisNames.SDNOTIFY.NAME_TABLE = "SDNOTIFY"; 
UsrCfg.InternoAtisNames.SDNOTIFY.ISSENDEMAIL.FieldName = "ISSENDEMAIL"; 
UsrCfg.InternoAtisNames.SDNOTIFY.ISSENDEMAIL.TableName = "SDNOTIFY"; 
UsrCfg.InternoAtisNames.SDNOTIFY.ISSENDEMAIL.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.SDNOTIFY.ISSENDEMAIL.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDNOTIFY.ISSENDEMAIL);
UsrCfg.InternoAtisNames.SDNOTIFY.NAME_TABLE = "SDNOTIFY"; 
UsrCfg.InternoAtisNames.SDNOTIFY.IDSDTYPEUSER.FieldName = "IDSDTYPEUSER"; 
UsrCfg.InternoAtisNames.SDNOTIFY.IDSDTYPEUSER.TableName = "SDNOTIFY"; 
UsrCfg.InternoAtisNames.SDNOTIFY.IDSDTYPEUSER.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDNOTIFY.IDSDTYPEUSER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDNOTIFY.IDSDTYPEUSER);
UsrCfg.InternoAtisNames.SDNOTIFY.NAME_TABLE = "SDNOTIFY"; 
UsrCfg.InternoAtisNames.SDNOTIFY.IDCMDBCI.FieldName = "IDCMDBCI"; 
UsrCfg.InternoAtisNames.SDNOTIFY.IDCMDBCI.TableName = "SDNOTIFY"; 
UsrCfg.InternoAtisNames.SDNOTIFY.IDCMDBCI.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDNOTIFY.IDCMDBCI.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDNOTIFY.IDCMDBCI);
UsrCfg.InternoAtisNames.SDGROUPSERVICEUSER.NAME_TABLE = "SDGROUPSERVICEUSER"; 
UsrCfg.InternoAtisNames.SDGROUPSERVICEUSER.IDSDGROUPSERVICEUSER.FieldName = "IDSDGROUPSERVICEUSER"; 
UsrCfg.InternoAtisNames.SDGROUPSERVICEUSER.IDSDGROUPSERVICEUSER.TableName = "SDGROUPSERVICEUSER"; 
UsrCfg.InternoAtisNames.SDGROUPSERVICEUSER.IDSDGROUPSERVICEUSER.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.SDGROUPSERVICEUSER.IDSDGROUPSERVICEUSER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDGROUPSERVICEUSER.IDSDGROUPSERVICEUSER);
UsrCfg.InternoAtisNames.SDGROUPSERVICEUSER.NAME_TABLE = "SDGROUPSERVICEUSER"; 
UsrCfg.InternoAtisNames.SDGROUPSERVICEUSER.IDMDGROUP.FieldName = "IDMDGROUP"; 
UsrCfg.InternoAtisNames.SDGROUPSERVICEUSER.IDMDGROUP.TableName = "SDGROUPSERVICEUSER"; 
UsrCfg.InternoAtisNames.SDGROUPSERVICEUSER.IDMDGROUP.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDGROUPSERVICEUSER.IDMDGROUP.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDGROUPSERVICEUSER.IDMDGROUP);
UsrCfg.InternoAtisNames.SDGROUPSERVICEUSER.NAME_TABLE = "SDGROUPSERVICEUSER"; 
UsrCfg.InternoAtisNames.SDGROUPSERVICEUSER.IDMDSERVICETYPE.FieldName = "IDMDSERVICETYPE"; 
UsrCfg.InternoAtisNames.SDGROUPSERVICEUSER.IDMDSERVICETYPE.TableName = "SDGROUPSERVICEUSER"; 
UsrCfg.InternoAtisNames.SDGROUPSERVICEUSER.IDMDSERVICETYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDGROUPSERVICEUSER.IDMDSERVICETYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDGROUPSERVICEUSER.IDMDSERVICETYPE);
UsrCfg.InternoAtisNames.SDGROUPSERVICEUSER.NAME_TABLE = "SDGROUPSERVICEUSER"; 
UsrCfg.InternoAtisNames.SDGROUPSERVICEUSER.IDSDTYPEUSER.FieldName = "IDSDTYPEUSER"; 
UsrCfg.InternoAtisNames.SDGROUPSERVICEUSER.IDSDTYPEUSER.TableName = "SDGROUPSERVICEUSER"; 
UsrCfg.InternoAtisNames.SDGROUPSERVICEUSER.IDSDTYPEUSER.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SDGROUPSERVICEUSER.IDSDTYPEUSER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SDGROUPSERVICEUSER.IDSDTYPEUSER);
UsrCfg.InternoAtisNames.SMINTERFACE.NAME_TABLE = "SMINTERFACE"; 
UsrCfg.InternoAtisNames.SMINTERFACE.IDSMINTERFACE.FieldName = "IDSMINTERFACE"; 
UsrCfg.InternoAtisNames.SMINTERFACE.IDSMINTERFACE.TableName = "SMINTERFACE"; 
UsrCfg.InternoAtisNames.SMINTERFACE.IDSMINTERFACE.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.SMINTERFACE.IDSMINTERFACE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SMINTERFACE.IDSMINTERFACE);
UsrCfg.InternoAtisNames.SMINTERFACE.NAME_TABLE = "SMINTERFACE"; 
UsrCfg.InternoAtisNames.SMINTERFACE.INTERFACENAME.FieldName = "INTERFACENAME"; 
UsrCfg.InternoAtisNames.SMINTERFACE.INTERFACENAME.TableName = "SMINTERFACE"; 
UsrCfg.InternoAtisNames.SMINTERFACE.INTERFACENAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.SMINTERFACE.INTERFACENAME.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SMINTERFACE.INTERFACENAME);
UsrCfg.InternoAtisNames.SMINTERFACECUSTOM.NAME_TABLE = "SMINTERFACECUSTOM"; 
UsrCfg.InternoAtisNames.SMINTERFACECUSTOM.IDSMINTERFACECUSTOM.FieldName = "IDSMINTERFACECUSTOM"; 
UsrCfg.InternoAtisNames.SMINTERFACECUSTOM.IDSMINTERFACECUSTOM.TableName = "SMINTERFACECUSTOM"; 
UsrCfg.InternoAtisNames.SMINTERFACECUSTOM.IDSMINTERFACECUSTOM.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.SMINTERFACECUSTOM.IDSMINTERFACECUSTOM.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SMINTERFACECUSTOM.IDSMINTERFACECUSTOM);
UsrCfg.InternoAtisNames.SMINTERFACECUSTOM.NAME_TABLE = "SMINTERFACECUSTOM"; 
UsrCfg.InternoAtisNames.SMINTERFACECUSTOM.IDSMINTERFACE.FieldName = "IDSMINTERFACE"; 
UsrCfg.InternoAtisNames.SMINTERFACECUSTOM.IDSMINTERFACE.TableName = "SMINTERFACECUSTOM"; 
UsrCfg.InternoAtisNames.SMINTERFACECUSTOM.IDSMINTERFACE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.SMINTERFACECUSTOM.IDSMINTERFACE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SMINTERFACECUSTOM.IDSMINTERFACE);
UsrCfg.InternoAtisNames.SMINTERFACECUSTOM.NAME_TABLE = "SMINTERFACECUSTOM"; 
UsrCfg.InternoAtisNames.SMINTERFACECUSTOM.INTERFACECUSTOMNAME.FieldName = "INTERFACECUSTOMNAME"; 
UsrCfg.InternoAtisNames.SMINTERFACECUSTOM.INTERFACECUSTOMNAME.TableName = "SMINTERFACECUSTOM"; 
UsrCfg.InternoAtisNames.SMINTERFACECUSTOM.INTERFACECUSTOMNAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.SMINTERFACECUSTOM.INTERFACECUSTOMNAME.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SMINTERFACECUSTOM.INTERFACECUSTOMNAME);
UsrCfg.InternoAtisNames.SMINTERFACECUSTOM.NAME_TABLE = "SMINTERFACECUSTOM"; 
UsrCfg.InternoAtisNames.SMINTERFACECUSTOM.CUSTOMCODE.FieldName = "CUSTOMCODE"; 
UsrCfg.InternoAtisNames.SMINTERFACECUSTOM.CUSTOMCODE.TableName = "SMINTERFACECUSTOM"; 
UsrCfg.InternoAtisNames.SMINTERFACECUSTOM.CUSTOMCODE.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.SMINTERFACECUSTOM.CUSTOMCODE.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.SMINTERFACECUSTOM.CUSTOMCODE);
UsrCfg.InternoAtisNames.PSGROUP.NAME_TABLE = "PSGROUP"; 
UsrCfg.InternoAtisNames.PSGROUP.IDPSGROUP.FieldName = "IDPSGROUP"; 
UsrCfg.InternoAtisNames.PSGROUP.IDPSGROUP.TableName = "PSGROUP"; 
UsrCfg.InternoAtisNames.PSGROUP.IDPSGROUP.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.PSGROUP.IDPSGROUP.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PSGROUP.IDPSGROUP);
UsrCfg.InternoAtisNames.PSGROUP.NAME_TABLE = "PSGROUP"; 
UsrCfg.InternoAtisNames.PSGROUP.GROUP_NAME.FieldName = "GROUP_NAME"; 
UsrCfg.InternoAtisNames.PSGROUP.GROUP_NAME.TableName = "PSGROUP"; 
UsrCfg.InternoAtisNames.PSGROUP.GROUP_NAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.PSGROUP.GROUP_NAME.Size = 250; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PSGROUP.GROUP_NAME);
UsrCfg.InternoAtisNames.PSGROUP.NAME_TABLE = "PSGROUP"; 
UsrCfg.InternoAtisNames.PSGROUP.GROUP_BACKGROUND.FieldName = "GROUP_BACKGROUND"; 
UsrCfg.InternoAtisNames.PSGROUP.GROUP_BACKGROUND.TableName = "PSGROUP"; 
UsrCfg.InternoAtisNames.PSGROUP.GROUP_BACKGROUND.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.PSGROUP.GROUP_BACKGROUND.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PSGROUP.GROUP_BACKGROUND);
UsrCfg.InternoAtisNames.PSGROUP.NAME_TABLE = "PSGROUP"; 
UsrCfg.InternoAtisNames.PSGROUP.GROUP_BORDER.FieldName = "GROUP_BORDER"; 
UsrCfg.InternoAtisNames.PSGROUP.GROUP_BORDER.TableName = "PSGROUP"; 
UsrCfg.InternoAtisNames.PSGROUP.GROUP_BORDER.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.PSGROUP.GROUP_BORDER.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PSGROUP.GROUP_BORDER);
UsrCfg.InternoAtisNames.PSGROUP.NAME_TABLE = "PSGROUP"; 
UsrCfg.InternoAtisNames.PSGROUP.GROUP_DESCRIPTION.FieldName = "GROUP_DESCRIPTION"; 
UsrCfg.InternoAtisNames.PSGROUP.GROUP_DESCRIPTION.TableName = "PSGROUP"; 
UsrCfg.InternoAtisNames.PSGROUP.GROUP_DESCRIPTION.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.PSGROUP.GROUP_DESCRIPTION.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PSGROUP.GROUP_DESCRIPTION);
UsrCfg.InternoAtisNames.PSGROUP.NAME_TABLE = "PSGROUP"; 
UsrCfg.InternoAtisNames.PSGROUP.GROUP_PATH.FieldName = "GROUP_PATH"; 
UsrCfg.InternoAtisNames.PSGROUP.GROUP_PATH.TableName = "PSGROUP"; 
UsrCfg.InternoAtisNames.PSGROUP.GROUP_PATH.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.PSGROUP.GROUP_PATH.Size = 500; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PSGROUP.GROUP_PATH);
UsrCfg.InternoAtisNames.PSMAIN.NAME_TABLE = "PSMAIN"; 
UsrCfg.InternoAtisNames.PSMAIN.IDPSMAIN.FieldName = "IDPSMAIN"; 
UsrCfg.InternoAtisNames.PSMAIN.IDPSMAIN.TableName = "PSMAIN"; 
UsrCfg.InternoAtisNames.PSMAIN.IDPSMAIN.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.PSMAIN.IDPSMAIN.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PSMAIN.IDPSMAIN);
UsrCfg.InternoAtisNames.PSMAIN.NAME_TABLE = "PSMAIN"; 
UsrCfg.InternoAtisNames.PSMAIN.IDPSGROUP.FieldName = "IDPSGROUP"; 
UsrCfg.InternoAtisNames.PSMAIN.IDPSGROUP.TableName = "PSMAIN"; 
UsrCfg.InternoAtisNames.PSMAIN.IDPSGROUP.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.PSMAIN.IDPSGROUP.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PSMAIN.IDPSGROUP);
UsrCfg.InternoAtisNames.PSMAIN.NAME_TABLE = "PSMAIN"; 
UsrCfg.InternoAtisNames.PSMAIN.MAIN_POSITION.FieldName = "MAIN_POSITION"; 
UsrCfg.InternoAtisNames.PSMAIN.MAIN_POSITION.TableName = "PSMAIN"; 
UsrCfg.InternoAtisNames.PSMAIN.MAIN_POSITION.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.PSMAIN.MAIN_POSITION.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PSMAIN.MAIN_POSITION);
UsrCfg.InternoAtisNames.PSMAIN.NAME_TABLE = "PSMAIN"; 
UsrCfg.InternoAtisNames.PSMAIN.MAIN_ICON.FieldName = "MAIN_ICON"; 
UsrCfg.InternoAtisNames.PSMAIN.MAIN_ICON.TableName = "PSMAIN"; 
UsrCfg.InternoAtisNames.PSMAIN.MAIN_ICON.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.PSMAIN.MAIN_ICON.Size = 255; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PSMAIN.MAIN_ICON);
UsrCfg.InternoAtisNames.PSMAIN.NAME_TABLE = "PSMAIN"; 
UsrCfg.InternoAtisNames.PSMAIN.MAIN_TITLE.FieldName = "MAIN_TITLE"; 
UsrCfg.InternoAtisNames.PSMAIN.MAIN_TITLE.TableName = "PSMAIN"; 
UsrCfg.InternoAtisNames.PSMAIN.MAIN_TITLE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.PSMAIN.MAIN_TITLE.Size = 255; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PSMAIN.MAIN_TITLE);
UsrCfg.InternoAtisNames.PSMAIN.NAME_TABLE = "PSMAIN"; 
UsrCfg.InternoAtisNames.PSMAIN.MAIN_TITLECOLOR.FieldName = "MAIN_TITLECOLOR"; 
UsrCfg.InternoAtisNames.PSMAIN.MAIN_TITLECOLOR.TableName = "PSMAIN"; 
UsrCfg.InternoAtisNames.PSMAIN.MAIN_TITLECOLOR.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.PSMAIN.MAIN_TITLECOLOR.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PSMAIN.MAIN_TITLECOLOR);
UsrCfg.InternoAtisNames.PSMAIN.NAME_TABLE = "PSMAIN"; 
UsrCfg.InternoAtisNames.PSMAIN.MAIN_BACKGROUNDACTIVE.FieldName = "MAIN_BACKGROUNDACTIVE"; 
UsrCfg.InternoAtisNames.PSMAIN.MAIN_BACKGROUNDACTIVE.TableName = "PSMAIN"; 
UsrCfg.InternoAtisNames.PSMAIN.MAIN_BACKGROUNDACTIVE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.PSMAIN.MAIN_BACKGROUNDACTIVE.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PSMAIN.MAIN_BACKGROUNDACTIVE);
UsrCfg.InternoAtisNames.PSMAIN.NAME_TABLE = "PSMAIN"; 
UsrCfg.InternoAtisNames.PSMAIN.MAIN_TITLEACTIVECOLOR.FieldName = "MAIN_TITLEACTIVECOLOR"; 
UsrCfg.InternoAtisNames.PSMAIN.MAIN_TITLEACTIVECOLOR.TableName = "PSMAIN"; 
UsrCfg.InternoAtisNames.PSMAIN.MAIN_TITLEACTIVECOLOR.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.PSMAIN.MAIN_TITLEACTIVECOLOR.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PSMAIN.MAIN_TITLEACTIVECOLOR);
UsrCfg.InternoAtisNames.PSMAIN.NAME_TABLE = "PSMAIN"; 
UsrCfg.InternoAtisNames.PSMAIN.MAIN_BORDERACTIVE.FieldName = "MAIN_BORDERACTIVE"; 
UsrCfg.InternoAtisNames.PSMAIN.MAIN_BORDERACTIVE.TableName = "PSMAIN"; 
UsrCfg.InternoAtisNames.PSMAIN.MAIN_BORDERACTIVE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.PSMAIN.MAIN_BORDERACTIVE.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PSMAIN.MAIN_BORDERACTIVE);
UsrCfg.InternoAtisNames.PSMAIN.NAME_TABLE = "PSMAIN"; 
UsrCfg.InternoAtisNames.PSMAIN.MAIN_BACKGROUNDHOVER.FieldName = "MAIN_BACKGROUNDHOVER"; 
UsrCfg.InternoAtisNames.PSMAIN.MAIN_BACKGROUNDHOVER.TableName = "PSMAIN"; 
UsrCfg.InternoAtisNames.PSMAIN.MAIN_BACKGROUNDHOVER.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.PSMAIN.MAIN_BACKGROUNDHOVER.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PSMAIN.MAIN_BACKGROUNDHOVER);
UsrCfg.InternoAtisNames.PSMAIN.NAME_TABLE = "PSMAIN"; 
UsrCfg.InternoAtisNames.PSMAIN.MAIN_TITLEHOVERCOLOR.FieldName = "MAIN_TITLEHOVERCOLOR"; 
UsrCfg.InternoAtisNames.PSMAIN.MAIN_TITLEHOVERCOLOR.TableName = "PSMAIN"; 
UsrCfg.InternoAtisNames.PSMAIN.MAIN_TITLEHOVERCOLOR.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.PSMAIN.MAIN_TITLEHOVERCOLOR.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PSMAIN.MAIN_TITLEHOVERCOLOR);
UsrCfg.InternoAtisNames.PSMAIN.NAME_TABLE = "PSMAIN"; 
UsrCfg.InternoAtisNames.PSMAIN.MAIN_DESCRIPTION.FieldName = "MAIN_DESCRIPTION"; 
UsrCfg.InternoAtisNames.PSMAIN.MAIN_DESCRIPTION.TableName = "PSMAIN"; 
UsrCfg.InternoAtisNames.PSMAIN.MAIN_DESCRIPTION.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.PSMAIN.MAIN_DESCRIPTION.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PSMAIN.MAIN_DESCRIPTION);
UsrCfg.InternoAtisNames.PSSECUNDARY.NAME_TABLE = "PSSECUNDARY"; 
UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSSECUNDARY.FieldName = "IDPSSECUNDARY"; 
UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSSECUNDARY.TableName = "PSSECUNDARY"; 
UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSSECUNDARY.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSSECUNDARY.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSSECUNDARY);
UsrCfg.InternoAtisNames.PSSECUNDARY.NAME_TABLE = "PSSECUNDARY"; 
UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSMAIN.FieldName = "IDPSMAIN"; 
UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSMAIN.TableName = "PSSECUNDARY"; 
UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSMAIN.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSMAIN.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PSSECUNDARY.IDPSMAIN);
UsrCfg.InternoAtisNames.PSSECUNDARY.NAME_TABLE = "PSSECUNDARY"; 
UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_POSITION.FieldName = "SECUNDARY_POSITION"; 
UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_POSITION.TableName = "PSSECUNDARY"; 
UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_POSITION.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_POSITION.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_POSITION);
UsrCfg.InternoAtisNames.PSSECUNDARY.NAME_TABLE = "PSSECUNDARY"; 
UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_ICON.FieldName = "SECUNDARY_ICON"; 
UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_ICON.TableName = "PSSECUNDARY"; 
UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_ICON.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_ICON.Size = 255; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_ICON);
UsrCfg.InternoAtisNames.PSSECUNDARY.NAME_TABLE = "PSSECUNDARY"; 
UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_TITLE.FieldName = "SECUNDARY_TITLE"; 
UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_TITLE.TableName = "PSSECUNDARY"; 
UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_TITLE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_TITLE.Size = 255; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_TITLE);
UsrCfg.InternoAtisNames.PSSECUNDARY.NAME_TABLE = "PSSECUNDARY"; 
UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_TEXTCOLOR.FieldName = "SECUNDARY_TEXTCOLOR"; 
UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_TEXTCOLOR.TableName = "PSSECUNDARY"; 
UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_TEXTCOLOR.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_TEXTCOLOR.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_TEXTCOLOR);
UsrCfg.InternoAtisNames.PSSECUNDARY.NAME_TABLE = "PSSECUNDARY"; 
UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_DESCRIPTION.FieldName = "SECUNDARY_DESCRIPTION"; 
UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_DESCRIPTION.TableName = "PSSECUNDARY"; 
UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_DESCRIPTION.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_DESCRIPTION.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_DESCRIPTION);
UsrCfg.InternoAtisNames.PSSECUNDARY.NAME_TABLE = "PSSECUNDARY"; 
UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_PHRASE.FieldName = "SECUNDARY_PHRASE"; 
UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_PHRASE.TableName = "PSSECUNDARY"; 
UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_PHRASE.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_PHRASE.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PSSECUNDARY.SECUNDARY_PHRASE);
UsrCfg.InternoAtisNames.VOTE.NAME_TABLE = "VOTE"; 
UsrCfg.InternoAtisNames.VOTE.IDVOTE.FieldName = "IDVOTE"; 
UsrCfg.InternoAtisNames.VOTE.IDVOTE.TableName = "VOTE"; 
UsrCfg.InternoAtisNames.VOTE.IDVOTE.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.VOTE.IDVOTE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.VOTE.IDVOTE);
UsrCfg.InternoAtisNames.VOTE.NAME_TABLE = "VOTE"; 
UsrCfg.InternoAtisNames.VOTE.VOTE_NAME.FieldName = "VOTE_NAME"; 
UsrCfg.InternoAtisNames.VOTE.VOTE_NAME.TableName = "VOTE"; 
UsrCfg.InternoAtisNames.VOTE.VOTE_NAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.VOTE.VOTE_NAME.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.VOTE.VOTE_NAME);
UsrCfg.InternoAtisNames.VOTE.NAME_TABLE = "VOTE"; 
UsrCfg.InternoAtisNames.VOTE.VOTE_GROUP.FieldName = "VOTE_GROUP"; 
UsrCfg.InternoAtisNames.VOTE.VOTE_GROUP.TableName = "VOTE"; 
UsrCfg.InternoAtisNames.VOTE.VOTE_GROUP.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.VOTE.VOTE_GROUP.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.VOTE.VOTE_GROUP);
UsrCfg.InternoAtisNames.VOTE.NAME_TABLE = "VOTE"; 
UsrCfg.InternoAtisNames.VOTE.VOTE_QUESTION.FieldName = "VOTE_QUESTION"; 
UsrCfg.InternoAtisNames.VOTE.VOTE_QUESTION.TableName = "VOTE"; 
UsrCfg.InternoAtisNames.VOTE.VOTE_QUESTION.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.VOTE.VOTE_QUESTION.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.VOTE.VOTE_QUESTION);
UsrCfg.InternoAtisNames.VOTE.NAME_TABLE = "VOTE"; 
UsrCfg.InternoAtisNames.VOTE.VOTE_COMMENTREQUIRED.FieldName = "VOTE_COMMENTREQUIRED"; 
UsrCfg.InternoAtisNames.VOTE.VOTE_COMMENTREQUIRED.TableName = "VOTE"; 
UsrCfg.InternoAtisNames.VOTE.VOTE_COMMENTREQUIRED.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.VOTE.VOTE_COMMENTREQUIRED.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.VOTE.VOTE_COMMENTREQUIRED);
UsrCfg.InternoAtisNames.VOTE.NAME_TABLE = "VOTE"; 
UsrCfg.InternoAtisNames.VOTE.VOTE_PROGRESSCOLOR.FieldName = "VOTE_PROGRESSCOLOR"; 
UsrCfg.InternoAtisNames.VOTE.VOTE_PROGRESSCOLOR.TableName = "VOTE"; 
UsrCfg.InternoAtisNames.VOTE.VOTE_PROGRESSCOLOR.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.VOTE.VOTE_PROGRESSCOLOR.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.VOTE.VOTE_PROGRESSCOLOR);
UsrCfg.InternoAtisNames.VOTE.NAME_TABLE = "VOTE"; 
UsrCfg.InternoAtisNames.VOTE.VOTE_PROGRESSTITLE.FieldName = "VOTE_PROGRESSTITLE"; 
UsrCfg.InternoAtisNames.VOTE.VOTE_PROGRESSTITLE.TableName = "VOTE"; 
UsrCfg.InternoAtisNames.VOTE.VOTE_PROGRESSTITLE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.VOTE.VOTE_PROGRESSTITLE.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.VOTE.VOTE_PROGRESSTITLE);
UsrCfg.InternoAtisNames.VOTE.NAME_TABLE = "VOTE"; 
UsrCfg.InternoAtisNames.VOTE.VOTE_TOTALPROGRESSTITLE.FieldName = "VOTE_TOTALPROGRESSTITLE"; 
UsrCfg.InternoAtisNames.VOTE.VOTE_TOTALPROGRESSTITLE.TableName = "VOTE"; 
UsrCfg.InternoAtisNames.VOTE.VOTE_TOTALPROGRESSTITLE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.VOTE.VOTE_TOTALPROGRESSTITLE.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.VOTE.VOTE_TOTALPROGRESSTITLE);
UsrCfg.InternoAtisNames.VOTE.NAME_TABLE = "VOTE"; 
UsrCfg.InternoAtisNames.VOTE.VOTE_INITGRAPHIC.FieldName = "VOTE_INITGRAPHIC"; 
UsrCfg.InternoAtisNames.VOTE.VOTE_INITGRAPHIC.TableName = "VOTE"; 
UsrCfg.InternoAtisNames.VOTE.VOTE_INITGRAPHIC.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.VOTE.VOTE_INITGRAPHIC.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.VOTE.VOTE_INITGRAPHIC);
UsrCfg.InternoAtisNames.VOTE.NAME_TABLE = "VOTE"; 
UsrCfg.InternoAtisNames.VOTE.VOTE_SHOWCOMMENT.FieldName = "VOTE_SHOWCOMMENT"; 
UsrCfg.InternoAtisNames.VOTE.VOTE_SHOWCOMMENT.TableName = "VOTE"; 
UsrCfg.InternoAtisNames.VOTE.VOTE_SHOWCOMMENT.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.VOTE.VOTE_SHOWCOMMENT.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.VOTE.VOTE_SHOWCOMMENT);
UsrCfg.InternoAtisNames.VOTE.NAME_TABLE = "VOTE"; 
UsrCfg.InternoAtisNames.VOTE.VOTE_SHOWCOMMENTGROUP.FieldName = "VOTE_SHOWCOMMENTGROUP"; 
UsrCfg.InternoAtisNames.VOTE.VOTE_SHOWCOMMENTGROUP.TableName = "VOTE"; 
UsrCfg.InternoAtisNames.VOTE.VOTE_SHOWCOMMENTGROUP.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.VOTE.VOTE_SHOWCOMMENTGROUP.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.VOTE.VOTE_SHOWCOMMENTGROUP);
UsrCfg.InternoAtisNames.VOTEOPTION.NAME_TABLE = "VOTEOPTION"; 
UsrCfg.InternoAtisNames.VOTEOPTION.IDVOTEOPTION.FieldName = "IDVOTEOPTION"; 
UsrCfg.InternoAtisNames.VOTEOPTION.IDVOTEOPTION.TableName = "VOTEOPTION"; 
UsrCfg.InternoAtisNames.VOTEOPTION.IDVOTEOPTION.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.VOTEOPTION.IDVOTEOPTION.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.VOTEOPTION.IDVOTEOPTION);
UsrCfg.InternoAtisNames.VOTEOPTION.NAME_TABLE = "VOTEOPTION"; 
UsrCfg.InternoAtisNames.VOTEOPTION.IDVOTE.FieldName = "IDVOTE"; 
UsrCfg.InternoAtisNames.VOTEOPTION.IDVOTE.TableName = "VOTEOPTION"; 
UsrCfg.InternoAtisNames.VOTEOPTION.IDVOTE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.VOTEOPTION.IDVOTE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.VOTEOPTION.IDVOTE);
UsrCfg.InternoAtisNames.VOTEOPTION.NAME_TABLE = "VOTEOPTION"; 
UsrCfg.InternoAtisNames.VOTEOPTION.VOTEOPTION_NAME.FieldName = "VOTEOPTION_NAME"; 
UsrCfg.InternoAtisNames.VOTEOPTION.VOTEOPTION_NAME.TableName = "VOTEOPTION"; 
UsrCfg.InternoAtisNames.VOTEOPTION.VOTEOPTION_NAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.VOTEOPTION.VOTEOPTION_NAME.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.VOTEOPTION.VOTEOPTION_NAME);
UsrCfg.InternoAtisNames.VOTEOPTION.NAME_TABLE = "VOTEOPTION"; 
UsrCfg.InternoAtisNames.VOTEOPTION.VOTEOPTION_DESCRIPTION.FieldName = "VOTEOPTION_DESCRIPTION"; 
UsrCfg.InternoAtisNames.VOTEOPTION.VOTEOPTION_DESCRIPTION.TableName = "VOTEOPTION"; 
UsrCfg.InternoAtisNames.VOTEOPTION.VOTEOPTION_DESCRIPTION.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.VOTEOPTION.VOTEOPTION_DESCRIPTION.Size = 500; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.VOTEOPTION.VOTEOPTION_DESCRIPTION);
UsrCfg.InternoAtisNames.VOTEDATA.NAME_TABLE = "VOTEDATA"; 
UsrCfg.InternoAtisNames.VOTEDATA.IDVOTEDATA.FieldName = "IDVOTEDATA"; 
UsrCfg.InternoAtisNames.VOTEDATA.IDVOTEDATA.TableName = "VOTEDATA"; 
UsrCfg.InternoAtisNames.VOTEDATA.IDVOTEDATA.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.VOTEDATA.IDVOTEDATA.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.VOTEDATA.IDVOTEDATA);
UsrCfg.InternoAtisNames.VOTEDATA.NAME_TABLE = "VOTEDATA"; 
UsrCfg.InternoAtisNames.VOTEDATA.IDVOTE.FieldName = "IDVOTE"; 
UsrCfg.InternoAtisNames.VOTEDATA.IDVOTE.TableName = "VOTEDATA"; 
UsrCfg.InternoAtisNames.VOTEDATA.IDVOTE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.VOTEDATA.IDVOTE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.VOTEDATA.IDVOTE);
UsrCfg.InternoAtisNames.VOTEDATA.NAME_TABLE = "VOTEDATA"; 
UsrCfg.InternoAtisNames.VOTEDATA.VOTEDATA_STARTDATE.FieldName = "VOTEDATA_STARTDATE"; 
UsrCfg.InternoAtisNames.VOTEDATA.VOTEDATA_STARTDATE.TableName = "VOTEDATA"; 
UsrCfg.InternoAtisNames.VOTEDATA.VOTEDATA_STARTDATE.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.VOTEDATA.VOTEDATA_STARTDATE.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.VOTEDATA.VOTEDATA_STARTDATE);
UsrCfg.InternoAtisNames.VOTEDATA.NAME_TABLE = "VOTEDATA"; 
UsrCfg.InternoAtisNames.VOTEDATA.VOTEDATA_FINALDATE.FieldName = "VOTEDATA_FINALDATE"; 
UsrCfg.InternoAtisNames.VOTEDATA.VOTEDATA_FINALDATE.TableName = "VOTEDATA"; 
UsrCfg.InternoAtisNames.VOTEDATA.VOTEDATA_FINALDATE.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.VOTEDATA.VOTEDATA_FINALDATE.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.VOTEDATA.VOTEDATA_FINALDATE);
UsrCfg.InternoAtisNames.VOTEDATA.NAME_TABLE = "VOTEDATA"; 
UsrCfg.InternoAtisNames.VOTEDATA.VOTEDATA_STATUS.FieldName = "VOTEDATA_STATUS"; 
UsrCfg.InternoAtisNames.VOTEDATA.VOTEDATA_STATUS.TableName = "VOTEDATA"; 
UsrCfg.InternoAtisNames.VOTEDATA.VOTEDATA_STATUS.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.VOTEDATA.VOTEDATA_STATUS.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.VOTEDATA.VOTEDATA_STATUS);
UsrCfg.InternoAtisNames.VOTEDATAUSER.NAME_TABLE = "VOTEDATAUSER"; 
UsrCfg.InternoAtisNames.VOTEDATAUSER.IDVOTEDATAUSER.FieldName = "IDVOTEDATAUSER"; 
UsrCfg.InternoAtisNames.VOTEDATAUSER.IDVOTEDATAUSER.TableName = "VOTEDATAUSER"; 
UsrCfg.InternoAtisNames.VOTEDATAUSER.IDVOTEDATAUSER.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.VOTEDATAUSER.IDVOTEDATAUSER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.VOTEDATAUSER.IDVOTEDATAUSER);
UsrCfg.InternoAtisNames.VOTEDATAUSER.NAME_TABLE = "VOTEDATAUSER"; 
UsrCfg.InternoAtisNames.VOTEDATAUSER.IDVOTEDATA.FieldName = "IDVOTEDATA"; 
UsrCfg.InternoAtisNames.VOTEDATAUSER.IDVOTEDATA.TableName = "VOTEDATAUSER"; 
UsrCfg.InternoAtisNames.VOTEDATAUSER.IDVOTEDATA.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.VOTEDATAUSER.IDVOTEDATA.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.VOTEDATAUSER.IDVOTEDATA);
UsrCfg.InternoAtisNames.VOTEDATAUSER.NAME_TABLE = "VOTEDATAUSER"; 
UsrCfg.InternoAtisNames.VOTEDATAUSER.IDCMDBCI.FieldName = "IDCMDBCI"; 
UsrCfg.InternoAtisNames.VOTEDATAUSER.IDCMDBCI.TableName = "VOTEDATAUSER"; 
UsrCfg.InternoAtisNames.VOTEDATAUSER.IDCMDBCI.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.VOTEDATAUSER.IDCMDBCI.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.VOTEDATAUSER.IDCMDBCI);
UsrCfg.InternoAtisNames.VOTEDATAUSER.NAME_TABLE = "VOTEDATAUSER"; 
UsrCfg.InternoAtisNames.VOTEDATAUSER.IDVOTEOPTION.FieldName = "IDVOTEOPTION"; 
UsrCfg.InternoAtisNames.VOTEDATAUSER.IDVOTEOPTION.TableName = "VOTEDATAUSER"; 
UsrCfg.InternoAtisNames.VOTEDATAUSER.IDVOTEOPTION.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.VOTEDATAUSER.IDVOTEOPTION.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.VOTEDATAUSER.IDVOTEOPTION);
UsrCfg.InternoAtisNames.VOTEDATAUSER.NAME_TABLE = "VOTEDATAUSER"; 
UsrCfg.InternoAtisNames.VOTEDATAUSER.VOTEDATAUSER_NAME.FieldName = "VOTEDATAUSER_NAME"; 
UsrCfg.InternoAtisNames.VOTEDATAUSER.VOTEDATAUSER_NAME.TableName = "VOTEDATAUSER"; 
UsrCfg.InternoAtisNames.VOTEDATAUSER.VOTEDATAUSER_NAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.VOTEDATAUSER.VOTEDATAUSER_NAME.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.VOTEDATAUSER.VOTEDATAUSER_NAME);
UsrCfg.InternoAtisNames.VOTEDATAUSER.NAME_TABLE = "VOTEDATAUSER"; 
UsrCfg.InternoAtisNames.VOTEDATAUSER.VOTEDATAUSER_COMMENTARY.FieldName = "VOTEDATAUSER_COMMENTARY"; 
UsrCfg.InternoAtisNames.VOTEDATAUSER.VOTEDATAUSER_COMMENTARY.TableName = "VOTEDATAUSER"; 
UsrCfg.InternoAtisNames.VOTEDATAUSER.VOTEDATAUSER_COMMENTARY.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.VOTEDATAUSER.VOTEDATAUSER_COMMENTARY.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.VOTEDATAUSER.VOTEDATAUSER_COMMENTARY);
UsrCfg.InternoAtisNames.PMMAINTASK.NAME_TABLE = "PMMAINTASK"; 
UsrCfg.InternoAtisNames.PMMAINTASK.IDPMMAINTASK.FieldName = "IDPMMAINTASK"; 
UsrCfg.InternoAtisNames.PMMAINTASK.IDPMMAINTASK.TableName = "PMMAINTASK"; 
UsrCfg.InternoAtisNames.PMMAINTASK.IDPMMAINTASK.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.PMMAINTASK.IDPMMAINTASK.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMMAINTASK.IDPMMAINTASK);
UsrCfg.InternoAtisNames.PMMAINTASK.NAME_TABLE = "PMMAINTASK"; 
UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_NAME.FieldName = "MAINTASK_NAME"; 
UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_NAME.TableName = "PMMAINTASK"; 
UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_NAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_NAME.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_NAME);
UsrCfg.InternoAtisNames.PMMAINTASK.NAME_TABLE = "PMMAINTASK"; 
UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_OWNER.FieldName = "MAINTASK_OWNER"; 
UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_OWNER.TableName = "PMMAINTASK"; 
UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_OWNER.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_OWNER.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_OWNER);
UsrCfg.InternoAtisNames.PMMAINTASK.NAME_TABLE = "PMMAINTASK"; 
UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_DURATION.FieldName = "MAINTASK_DURATION"; 
UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_DURATION.TableName = "PMMAINTASK"; 
UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_DURATION.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_DURATION.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_DURATION);
UsrCfg.InternoAtisNames.PMMAINTASK.NAME_TABLE = "PMMAINTASK"; 
UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_STARTDATE.FieldName = "MAINTASK_STARTDATE"; 
UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_STARTDATE.TableName = "PMMAINTASK"; 
UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_STARTDATE.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_STARTDATE.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_STARTDATE);
UsrCfg.InternoAtisNames.PMMAINTASK.NAME_TABLE = "PMMAINTASK"; 
UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_ENDDATE.FieldName = "MAINTASK_ENDDATE"; 
UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_ENDDATE.TableName = "PMMAINTASK"; 
UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_ENDDATE.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_ENDDATE.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_ENDDATE);
UsrCfg.InternoAtisNames.PMMAINTASK.NAME_TABLE = "PMMAINTASK"; 
UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_STATE.FieldName = "MAINTASK_STATE"; 
UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_STATE.TableName = "PMMAINTASK"; 
UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_STATE.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_STATE.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_STATE);
UsrCfg.InternoAtisNames.PMMAINTASK.NAME_TABLE = "PMMAINTASK"; 
UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_WEEKENDHOLIDAY.FieldName = "MAINTASK_WEEKENDHOLIDAY"; 
UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_WEEKENDHOLIDAY.TableName = "PMMAINTASK"; 
UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_WEEKENDHOLIDAY.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_WEEKENDHOLIDAY.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_WEEKENDHOLIDAY);
UsrCfg.InternoAtisNames.PMMAINTASK.NAME_TABLE = "PMMAINTASK"; 
UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_SUNDAYHOLIDAY.FieldName = "MAINTASK_SUNDAYHOLIDAY"; 
UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_SUNDAYHOLIDAY.TableName = "PMMAINTASK"; 
UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_SUNDAYHOLIDAY.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_SUNDAYHOLIDAY.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_SUNDAYHOLIDAY);
UsrCfg.InternoAtisNames.PMMAINTASK.NAME_TABLE = "PMMAINTASK"; 
UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_SCALEUNITS.FieldName = "MAINTASK_SCALEUNITS"; 
UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_SCALEUNITS.TableName = "PMMAINTASK"; 
UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_SCALEUNITS.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_SCALEUNITS.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_SCALEUNITS);
UsrCfg.InternoAtisNames.PMMAINTASK.NAME_TABLE = "PMMAINTASK"; 
UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_SCALECOUNTER.FieldName = "MAINTASK_SCALECOUNTER"; 
UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_SCALECOUNTER.TableName = "PMMAINTASK"; 
UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_SCALECOUNTER.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_SCALECOUNTER.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMMAINTASK.MAINTASK_SCALECOUNTER);
UsrCfg.InternoAtisNames.PMHOLIDAY.NAME_TABLE = "PMHOLIDAY"; 
UsrCfg.InternoAtisNames.PMHOLIDAY.IDPMHOLIDAY.FieldName = "IDPMHOLIDAY"; 
UsrCfg.InternoAtisNames.PMHOLIDAY.IDPMHOLIDAY.TableName = "PMHOLIDAY"; 
UsrCfg.InternoAtisNames.PMHOLIDAY.IDPMHOLIDAY.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.PMHOLIDAY.IDPMHOLIDAY.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMHOLIDAY.IDPMHOLIDAY);
UsrCfg.InternoAtisNames.PMHOLIDAY.NAME_TABLE = "PMHOLIDAY"; 
UsrCfg.InternoAtisNames.PMHOLIDAY.IDPMMAINTASK.FieldName = "IDPMMAINTASK"; 
UsrCfg.InternoAtisNames.PMHOLIDAY.IDPMMAINTASK.TableName = "PMHOLIDAY"; 
UsrCfg.InternoAtisNames.PMHOLIDAY.IDPMMAINTASK.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.PMHOLIDAY.IDPMMAINTASK.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMHOLIDAY.IDPMMAINTASK);
UsrCfg.InternoAtisNames.PMHOLIDAY.NAME_TABLE = "PMHOLIDAY"; 
UsrCfg.InternoAtisNames.PMHOLIDAY.HOLIDAY_DATE.FieldName = "HOLIDAY_DATE"; 
UsrCfg.InternoAtisNames.PMHOLIDAY.HOLIDAY_DATE.TableName = "PMHOLIDAY"; 
UsrCfg.InternoAtisNames.PMHOLIDAY.HOLIDAY_DATE.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.PMHOLIDAY.HOLIDAY_DATE.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMHOLIDAY.HOLIDAY_DATE);
UsrCfg.InternoAtisNames.PMDETAILTASK.NAME_TABLE = "PMDETAILTASK"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.IDPMDETAILTASK.FieldName = "IDPMDETAILTASK"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.IDPMDETAILTASK.TableName = "PMDETAILTASK"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.IDPMDETAILTASK.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.PMDETAILTASK.IDPMDETAILTASK.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMDETAILTASK.IDPMDETAILTASK);
UsrCfg.InternoAtisNames.PMDETAILTASK.NAME_TABLE = "PMDETAILTASK"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.IDPMMAINTASK.FieldName = "IDPMMAINTASK"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.IDPMMAINTASK.TableName = "PMDETAILTASK"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.IDPMMAINTASK.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.PMDETAILTASK.IDPMMAINTASK.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMDETAILTASK.IDPMMAINTASK);
UsrCfg.InternoAtisNames.PMDETAILTASK.NAME_TABLE = "PMDETAILTASK"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_NAME.FieldName = "DETAILTASK_NAME"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_NAME.TableName = "PMDETAILTASK"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_NAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_NAME.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_NAME);
UsrCfg.InternoAtisNames.PMDETAILTASK.NAME_TABLE = "PMDETAILTASK"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_DURATION.FieldName = "DETAILTASK_DURATION"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_DURATION.TableName = "PMDETAILTASK"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_DURATION.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_DURATION.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_DURATION);
UsrCfg.InternoAtisNames.PMDETAILTASK.NAME_TABLE = "PMDETAILTASK"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_STARTDATE.FieldName = "DETAILTASK_STARTDATE"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_STARTDATE.TableName = "PMDETAILTASK"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_STARTDATE.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_STARTDATE.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_STARTDATE);
UsrCfg.InternoAtisNames.PMDETAILTASK.NAME_TABLE = "PMDETAILTASK"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_ENDDATE.FieldName = "DETAILTASK_ENDDATE"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_ENDDATE.TableName = "PMDETAILTASK"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_ENDDATE.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_ENDDATE.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_ENDDATE);
UsrCfg.InternoAtisNames.PMDETAILTASK.NAME_TABLE = "PMDETAILTASK"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_INDEX.FieldName = "DETAILTASK_INDEX"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_INDEX.TableName = "PMDETAILTASK"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_INDEX.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_INDEX.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_INDEX);
UsrCfg.InternoAtisNames.PMDETAILTASK.NAME_TABLE = "PMDETAILTASK"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_PERCENTCOMPLETE.FieldName = "DETAILTASK_PERCENTCOMPLETE"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_PERCENTCOMPLETE.TableName = "PMDETAILTASK"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_PERCENTCOMPLETE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_PERCENTCOMPLETE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_PERCENTCOMPLETE);
UsrCfg.InternoAtisNames.PMDETAILTASK.NAME_TABLE = "PMDETAILTASK"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_PRIORITY.FieldName = "DETAILTASK_PRIORITY"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_PRIORITY.TableName = "PMDETAILTASK"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_PRIORITY.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_PRIORITY.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_PRIORITY);
UsrCfg.InternoAtisNames.PMDETAILTASK.NAME_TABLE = "PMDETAILTASK"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_ESTIMATED.FieldName = "DETAILTASK_ESTIMATED"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_ESTIMATED.TableName = "PMDETAILTASK"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_ESTIMATED.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_ESTIMATED.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_ESTIMATED);
UsrCfg.InternoAtisNames.PMDETAILTASK.NAME_TABLE = "PMDETAILTASK"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_NOTE.FieldName = "DETAILTASK_NOTE"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_NOTE.TableName = "PMDETAILTASK"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_NOTE.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_NOTE.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_NOTE);
UsrCfg.InternoAtisNames.PMDETAILTASK.NAME_TABLE = "PMDETAILTASK"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.IDPMDETAILTASK_PARENT.FieldName = "IDPMDETAILTASK_PARENT"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.IDPMDETAILTASK_PARENT.TableName = "PMDETAILTASK"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.IDPMDETAILTASK_PARENT.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.PMDETAILTASK.IDPMDETAILTASK_PARENT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMDETAILTASK.IDPMDETAILTASK_PARENT);
UsrCfg.InternoAtisNames.PMDETAILTASK.NAME_TABLE = "PMDETAILTASK"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_STARTDATETEMP.FieldName = "DETAILTASK_STARTDATETEMP"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_STARTDATETEMP.TableName = "PMDETAILTASK"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_STARTDATETEMP.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_STARTDATETEMP.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_STARTDATETEMP);
UsrCfg.InternoAtisNames.PMDETAILTASK.NAME_TABLE = "PMDETAILTASK"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_ENDDATETEMP.FieldName = "DETAILTASK_ENDDATETEMP"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_ENDDATETEMP.TableName = "PMDETAILTASK"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_ENDDATETEMP.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_ENDDATETEMP.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_ENDDATETEMP);
UsrCfg.InternoAtisNames.PMDETAILTASK.NAME_TABLE = "PMDETAILTASK"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_DURATIONTEMP.FieldName = "DETAILTASK_DURATIONTEMP"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_DURATIONTEMP.TableName = "PMDETAILTASK"; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_DURATIONTEMP.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_DURATIONTEMP.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMDETAILTASK.DETAILTASK_DURATIONTEMP);
UsrCfg.InternoAtisNames.PMRESOURCES.NAME_TABLE = "PMRESOURCES"; 
UsrCfg.InternoAtisNames.PMRESOURCES.IDPMRESOURCES.FieldName = "IDPMRESOURCES"; 
UsrCfg.InternoAtisNames.PMRESOURCES.IDPMRESOURCES.TableName = "PMRESOURCES"; 
UsrCfg.InternoAtisNames.PMRESOURCES.IDPMRESOURCES.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.PMRESOURCES.IDPMRESOURCES.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMRESOURCES.IDPMRESOURCES);
UsrCfg.InternoAtisNames.PMRESOURCES.NAME_TABLE = "PMRESOURCES"; 
UsrCfg.InternoAtisNames.PMRESOURCES.IDPMMAINTASK.FieldName = "IDPMMAINTASK"; 
UsrCfg.InternoAtisNames.PMRESOURCES.IDPMMAINTASK.TableName = "PMRESOURCES"; 
UsrCfg.InternoAtisNames.PMRESOURCES.IDPMMAINTASK.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.PMRESOURCES.IDPMMAINTASK.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMRESOURCES.IDPMMAINTASK);
UsrCfg.InternoAtisNames.PMRESOURCES.NAME_TABLE = "PMRESOURCES"; 
UsrCfg.InternoAtisNames.PMRESOURCES.RESOURCES_NAME.FieldName = "RESOURCES_NAME"; 
UsrCfg.InternoAtisNames.PMRESOURCES.RESOURCES_NAME.TableName = "PMRESOURCES"; 
UsrCfg.InternoAtisNames.PMRESOURCES.RESOURCES_NAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.PMRESOURCES.RESOURCES_NAME.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMRESOURCES.RESOURCES_NAME);
UsrCfg.InternoAtisNames.PMRESOURCES.NAME_TABLE = "PMRESOURCES"; 
UsrCfg.InternoAtisNames.PMRESOURCES.RESOURCES_ALLOCATIONOWNER.FieldName = "RESOURCES_ALLOCATIONOWNER"; 
UsrCfg.InternoAtisNames.PMRESOURCES.RESOURCES_ALLOCATIONOWNER.TableName = "PMRESOURCES"; 
UsrCfg.InternoAtisNames.PMRESOURCES.RESOURCES_ALLOCATIONOWNER.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.PMRESOURCES.RESOURCES_ALLOCATIONOWNER.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMRESOURCES.RESOURCES_ALLOCATIONOWNER);
UsrCfg.InternoAtisNames.PMRESOURCES.NAME_TABLE = "PMRESOURCES"; 
UsrCfg.InternoAtisNames.PMRESOURCES.RESOURCES_UNITS.FieldName = "RESOURCES_UNITS"; 
UsrCfg.InternoAtisNames.PMRESOURCES.RESOURCES_UNITS.TableName = "PMRESOURCES"; 
UsrCfg.InternoAtisNames.PMRESOURCES.RESOURCES_UNITS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.PMRESOURCES.RESOURCES_UNITS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMRESOURCES.RESOURCES_UNITS);
UsrCfg.InternoAtisNames.PMRESOURCES.NAME_TABLE = "PMRESOURCES"; 
UsrCfg.InternoAtisNames.PMRESOURCES.RESOURCES_COST.FieldName = "RESOURCES_COST"; 
UsrCfg.InternoAtisNames.PMRESOURCES.RESOURCES_COST.TableName = "PMRESOURCES"; 
UsrCfg.InternoAtisNames.PMRESOURCES.RESOURCES_COST.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.PMRESOURCES.RESOURCES_COST.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMRESOURCES.RESOURCES_COST);
UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.NAME_TABLE = "PMRESOURCESALLOCATION"; 
UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.IDPMRESOURCESALLOCATION.FieldName = "IDPMRESOURCESALLOCATION"; 
UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.IDPMRESOURCESALLOCATION.TableName = "PMRESOURCESALLOCATION"; 
UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.IDPMRESOURCESALLOCATION.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.IDPMRESOURCESALLOCATION.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.IDPMRESOURCESALLOCATION);
UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.NAME_TABLE = "PMRESOURCESALLOCATION"; 
UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.IDPMDETAILTASK.FieldName = "IDPMDETAILTASK"; 
UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.IDPMDETAILTASK.TableName = "PMRESOURCESALLOCATION"; 
UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.IDPMDETAILTASK.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.IDPMDETAILTASK.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.IDPMDETAILTASK);
UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.NAME_TABLE = "PMRESOURCESALLOCATION"; 
UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.IDPMRESOURCES.FieldName = "IDPMRESOURCES"; 
UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.IDPMRESOURCES.TableName = "PMRESOURCESALLOCATION"; 
UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.IDPMRESOURCES.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.IDPMRESOURCES.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.IDPMRESOURCES);
UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.NAME_TABLE = "PMRESOURCESALLOCATION"; 
UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.RESOURCESALLOCATION_UNITS.FieldName = "RESOURCESALLOCATION_UNITS"; 
UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.RESOURCESALLOCATION_UNITS.TableName = "PMRESOURCESALLOCATION"; 
UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.RESOURCESALLOCATION_UNITS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.RESOURCESALLOCATION_UNITS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.RESOURCESALLOCATION_UNITS);
UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.NAME_TABLE = "PMRESOURCESALLOCATION"; 
UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.RESOURCESALLOCATION_DESCRIPTION.FieldName = "RESOURCESALLOCATION_DESCRIPTION"; 
UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.RESOURCESALLOCATION_DESCRIPTION.TableName = "PMRESOURCESALLOCATION"; 
UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.RESOURCESALLOCATION_DESCRIPTION.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.RESOURCESALLOCATION_DESCRIPTION.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMRESOURCESALLOCATION.RESOURCESALLOCATION_DESCRIPTION);
UsrCfg.InternoAtisNames.PMTYPERELATION.NAME_TABLE = "PMTYPERELATION"; 
UsrCfg.InternoAtisNames.PMTYPERELATION.IDPMTYPERELATION.FieldName = "IDPMTYPERELATION"; 
UsrCfg.InternoAtisNames.PMTYPERELATION.IDPMTYPERELATION.TableName = "PMTYPERELATION"; 
UsrCfg.InternoAtisNames.PMTYPERELATION.IDPMTYPERELATION.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.PMTYPERELATION.IDPMTYPERELATION.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMTYPERELATION.IDPMTYPERELATION);
UsrCfg.InternoAtisNames.PMTYPERELATION.NAME_TABLE = "PMTYPERELATION"; 
UsrCfg.InternoAtisNames.PMTYPERELATION.TYPERELATION_NAME.FieldName = "TYPERELATION_NAME"; 
UsrCfg.InternoAtisNames.PMTYPERELATION.TYPERELATION_NAME.TableName = "PMTYPERELATION"; 
UsrCfg.InternoAtisNames.PMTYPERELATION.TYPERELATION_NAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.PMTYPERELATION.TYPERELATION_NAME.Size = 100; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMTYPERELATION.TYPERELATION_NAME);
UsrCfg.InternoAtisNames.PMTYPERELATION.NAME_TABLE = "PMTYPERELATION"; 
UsrCfg.InternoAtisNames.PMTYPERELATION.TYPERELATION_STATE.FieldName = "TYPERELATION_STATE"; 
UsrCfg.InternoAtisNames.PMTYPERELATION.TYPERELATION_STATE.TableName = "PMTYPERELATION"; 
UsrCfg.InternoAtisNames.PMTYPERELATION.TYPERELATION_STATE.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.PMTYPERELATION.TYPERELATION_STATE.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMTYPERELATION.TYPERELATION_STATE);
UsrCfg.InternoAtisNames.PMRELATION.NAME_TABLE = "PMRELATION"; 
UsrCfg.InternoAtisNames.PMRELATION.IDPMRELATION.FieldName = "IDPMRELATION"; 
UsrCfg.InternoAtisNames.PMRELATION.IDPMRELATION.TableName = "PMRELATION"; 
UsrCfg.InternoAtisNames.PMRELATION.IDPMRELATION.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.PMRELATION.IDPMRELATION.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMRELATION.IDPMRELATION);
UsrCfg.InternoAtisNames.PMRELATION.NAME_TABLE = "PMRELATION"; 
UsrCfg.InternoAtisNames.PMRELATION.IDPMDETAILTASK_PREDECESSORS.FieldName = "IDPMDETAILTASK_PREDECESSORS"; 
UsrCfg.InternoAtisNames.PMRELATION.IDPMDETAILTASK_PREDECESSORS.TableName = "PMRELATION"; 
UsrCfg.InternoAtisNames.PMRELATION.IDPMDETAILTASK_PREDECESSORS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.PMRELATION.IDPMDETAILTASK_PREDECESSORS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMRELATION.IDPMDETAILTASK_PREDECESSORS);
UsrCfg.InternoAtisNames.PMRELATION.NAME_TABLE = "PMRELATION"; 
UsrCfg.InternoAtisNames.PMRELATION.IDPMDETAILTASK.FieldName = "IDPMDETAILTASK"; 
UsrCfg.InternoAtisNames.PMRELATION.IDPMDETAILTASK.TableName = "PMRELATION"; 
UsrCfg.InternoAtisNames.PMRELATION.IDPMDETAILTASK.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.PMRELATION.IDPMDETAILTASK.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMRELATION.IDPMDETAILTASK);
UsrCfg.InternoAtisNames.PMRELATION.NAME_TABLE = "PMRELATION"; 
UsrCfg.InternoAtisNames.PMRELATION.IDPMTYPERELATION.FieldName = "IDPMTYPERELATION"; 
UsrCfg.InternoAtisNames.PMRELATION.IDPMTYPERELATION.TableName = "PMRELATION"; 
UsrCfg.InternoAtisNames.PMRELATION.IDPMTYPERELATION.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.PMRELATION.IDPMTYPERELATION.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PMRELATION.IDPMTYPERELATION);
UsrCfg.InternoAtisNames.PBITEMPLATE.NAME_TABLE = "PBITEMPLATE"; 
UsrCfg.InternoAtisNames.PBITEMPLATE.IDPBITEMPLATE.FieldName = "IDPBITEMPLATE"; 
UsrCfg.InternoAtisNames.PBITEMPLATE.IDPBITEMPLATE.TableName = "PBITEMPLATE"; 
UsrCfg.InternoAtisNames.PBITEMPLATE.IDPBITEMPLATE.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.PBITEMPLATE.IDPBITEMPLATE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PBITEMPLATE.IDPBITEMPLATE);
UsrCfg.InternoAtisNames.PBITEMPLATE.NAME_TABLE = "PBITEMPLATE"; 
UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_NAME.FieldName = "TEMPLATE_NAME"; 
UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_NAME.TableName = "PBITEMPLATE"; 
UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_NAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_NAME.Size = 250; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_NAME);
UsrCfg.InternoAtisNames.PBITEMPLATE.NAME_TABLE = "PBITEMPLATE"; 
UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_DESCRIPTION.FieldName = "TEMPLATE_DESCRIPTION"; 
UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_DESCRIPTION.TableName = "PBITEMPLATE"; 
UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_DESCRIPTION.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_DESCRIPTION.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_DESCRIPTION);
UsrCfg.InternoAtisNames.PBITEMPLATE.NAME_TABLE = "PBITEMPLATE"; 
UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_DATASQL.FieldName = "TEMPLATE_DATASQL"; 
UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_DATASQL.TableName = "PBITEMPLATE"; 
UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_DATASQL.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_DATASQL.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_DATASQL);
UsrCfg.InternoAtisNames.PBITEMPLATE.NAME_TABLE = "PBITEMPLATE"; 
UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_REFRESHTIMEENABLE.FieldName = "TEMPLATE_REFRESHTIMEENABLE"; 
UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_REFRESHTIMEENABLE.TableName = "PBITEMPLATE"; 
UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_REFRESHTIMEENABLE.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_REFRESHTIMEENABLE.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_REFRESHTIMEENABLE);
UsrCfg.InternoAtisNames.PBITEMPLATE.NAME_TABLE = "PBITEMPLATE"; 
UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_REFRESHTIME.FieldName = "TEMPLATE_REFRESHTIME"; 
UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_REFRESHTIME.TableName = "PBITEMPLATE"; 
UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_REFRESHTIME.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_REFRESHTIME.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_REFRESHTIME);
UsrCfg.InternoAtisNames.PBITEMPLATE.NAME_TABLE = "PBITEMPLATE"; 
UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_PATH.FieldName = "TEMPLATE_PATH"; 
UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_PATH.TableName = "PBITEMPLATE"; 
UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_PATH.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_PATH.Size = 500; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PBITEMPLATE.TEMPLATE_PATH);
UsrCfg.InternoAtisNames.PBITEMPLATECELLS.NAME_TABLE = "PBITEMPLATECELLS"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLS.IDPBITEMPLATECELLS.FieldName = "IDPBITEMPLATECELLS"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLS.IDPBITEMPLATECELLS.TableName = "PBITEMPLATECELLS"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLS.IDPBITEMPLATECELLS.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLS.IDPBITEMPLATECELLS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PBITEMPLATECELLS.IDPBITEMPLATECELLS);
UsrCfg.InternoAtisNames.PBITEMPLATECELLS.NAME_TABLE = "PBITEMPLATECELLS"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLS.IDPBITEMPLATE.FieldName = "IDPBITEMPLATE"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLS.IDPBITEMPLATE.TableName = "PBITEMPLATECELLS"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLS.IDPBITEMPLATE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLS.IDPBITEMPLATE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PBITEMPLATECELLS.IDPBITEMPLATE);
UsrCfg.InternoAtisNames.PBITEMPLATECELLS.NAME_TABLE = "PBITEMPLATECELLS"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLS.IDPBITEMPLATECELLS_PARENT.FieldName = "IDPBITEMPLATECELLS_PARENT"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLS.IDPBITEMPLATECELLS_PARENT.TableName = "PBITEMPLATECELLS"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLS.IDPBITEMPLATECELLS_PARENT.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLS.IDPBITEMPLATECELLS_PARENT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PBITEMPLATECELLS.IDPBITEMPLATECELLS_PARENT);
UsrCfg.InternoAtisNames.PBITEMPLATECELLS.NAME_TABLE = "PBITEMPLATECELLS"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLS.CELLS_POSITION.FieldName = "CELLS_POSITION"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLS.CELLS_POSITION.TableName = "PBITEMPLATECELLS"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLS.CELLS_POSITION.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLS.CELLS_POSITION.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PBITEMPLATECELLS.CELLS_POSITION);
UsrCfg.InternoAtisNames.PBITEMPLATECELLS.NAME_TABLE = "PBITEMPLATECELLS"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLS.CELLS_END.FieldName = "CELLS_END"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLS.CELLS_END.TableName = "PBITEMPLATECELLS"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLS.CELLS_END.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLS.CELLS_END.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PBITEMPLATECELLS.CELLS_END);
UsrCfg.InternoAtisNames.PBITEMPLATECELLS.NAME_TABLE = "PBITEMPLATECELLS"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLS.CELLS_NAME.FieldName = "CELLS_NAME"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLS.CELLS_NAME.TableName = "PBITEMPLATECELLS"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLS.CELLS_NAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLS.CELLS_NAME.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PBITEMPLATECELLS.CELLS_NAME);
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.NAME_TABLE = "PBITEMPLATECELLSDESIGN"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLSDESIGN.FieldName = "IDPBITEMPLATECELLSDESIGN"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLSDESIGN.TableName = "PBITEMPLATECELLSDESIGN"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLSDESIGN.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLSDESIGN.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLSDESIGN);
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.NAME_TABLE = "PBITEMPLATECELLSDESIGN"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLS.FieldName = "IDPBITEMPLATECELLS"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLS.TableName = "PBITEMPLATECELLSDESIGN"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLS);
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.NAME_TABLE = "PBITEMPLATECELLSDESIGN"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_XS.FieldName = "DESIGN_XS"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_XS.TableName = "PBITEMPLATECELLSDESIGN"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_XS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_XS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_XS);
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.NAME_TABLE = "PBITEMPLATECELLSDESIGN"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_S.FieldName = "DESIGN_S"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_S.TableName = "PBITEMPLATECELLSDESIGN"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_S.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_S.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_S);
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.NAME_TABLE = "PBITEMPLATECELLSDESIGN"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_M.FieldName = "DESIGN_M"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_M.TableName = "PBITEMPLATECELLSDESIGN"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_M.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_M.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_M);
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.NAME_TABLE = "PBITEMPLATECELLSDESIGN"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_L.FieldName = "DESIGN_L"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_L.TableName = "PBITEMPLATECELLSDESIGN"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_L.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_L.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_L);
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.NAME_TABLE = "PBITEMPLATECELLSDESIGN"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_XL.FieldName = "DESIGN_XL"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_XL.TableName = "PBITEMPLATECELLSDESIGN"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_XL.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_XL.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_XL);
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.NAME_TABLE = "PBITEMPLATECELLSDESIGN"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_ROW.FieldName = "DESIGN_ROW"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_ROW.TableName = "PBITEMPLATECELLSDESIGN"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_ROW.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_ROW.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_ROW);
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.NAME_TABLE = "PBITEMPLATECELLSDESIGN"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_COLUMN.FieldName = "DESIGN_COLUMN"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_COLUMN.TableName = "PBITEMPLATECELLSDESIGN"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_COLUMN.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_COLUMN.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PBITEMPLATECELLSDESIGN.DESIGN_COLUMN);
UsrCfg.InternoAtisNames.PBIGRAPHICTYPE.NAME_TABLE = "PBIGRAPHICTYPE"; 
UsrCfg.InternoAtisNames.PBIGRAPHICTYPE.IDPBIGRAPHICTYPE.FieldName = "IDPBIGRAPHICTYPE"; 
UsrCfg.InternoAtisNames.PBIGRAPHICTYPE.IDPBIGRAPHICTYPE.TableName = "PBIGRAPHICTYPE"; 
UsrCfg.InternoAtisNames.PBIGRAPHICTYPE.IDPBIGRAPHICTYPE.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.PBIGRAPHICTYPE.IDPBIGRAPHICTYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PBIGRAPHICTYPE.IDPBIGRAPHICTYPE);
UsrCfg.InternoAtisNames.PBIGRAPHICTYPE.NAME_TABLE = "PBIGRAPHICTYPE"; 
UsrCfg.InternoAtisNames.PBIGRAPHICTYPE.GRAPHIC_NAME.FieldName = "GRAPHIC_NAME"; 
UsrCfg.InternoAtisNames.PBIGRAPHICTYPE.GRAPHIC_NAME.TableName = "PBIGRAPHICTYPE"; 
UsrCfg.InternoAtisNames.PBIGRAPHICTYPE.GRAPHIC_NAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.PBIGRAPHICTYPE.GRAPHIC_NAME.Size = 250; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PBIGRAPHICTYPE.GRAPHIC_NAME);
UsrCfg.InternoAtisNames.PBIGRAPHICTYPE.NAME_TABLE = "PBIGRAPHICTYPE"; 
UsrCfg.InternoAtisNames.PBIGRAPHICTYPE.GRAPHIC_DESCRIPTION.FieldName = "GRAPHIC_DESCRIPTION"; 
UsrCfg.InternoAtisNames.PBIGRAPHICTYPE.GRAPHIC_DESCRIPTION.TableName = "PBIGRAPHICTYPE"; 
UsrCfg.InternoAtisNames.PBIGRAPHICTYPE.GRAPHIC_DESCRIPTION.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.PBIGRAPHICTYPE.GRAPHIC_DESCRIPTION.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PBIGRAPHICTYPE.GRAPHIC_DESCRIPTION);
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.NAME_TABLE = "PBITEMPLATECELLGRAPHIC"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLGRAPHIC.FieldName = "IDPBITEMPLATECELLGRAPHIC"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLGRAPHIC.TableName = "PBITEMPLATECELLGRAPHIC"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLGRAPHIC.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLGRAPHIC.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLGRAPHIC);
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.NAME_TABLE = "PBITEMPLATECELLGRAPHIC"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLS.FieldName = "IDPBITEMPLATECELLS"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLS.TableName = "PBITEMPLATECELLGRAPHIC"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLS);
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.NAME_TABLE = "PBITEMPLATECELLGRAPHIC"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.IDPBIGRAPHICTYPE.FieldName = "IDPBIGRAPHICTYPE"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.IDPBIGRAPHICTYPE.TableName = "PBITEMPLATECELLGRAPHIC"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.IDPBIGRAPHICTYPE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.IDPBIGRAPHICTYPE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.IDPBIGRAPHICTYPE);
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.NAME_TABLE = "PBITEMPLATECELLGRAPHIC"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_VISIBLE.FieldName = "CELLGRAPHIC_VISIBLE"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_VISIBLE.TableName = "PBITEMPLATECELLGRAPHIC"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_VISIBLE.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_VISIBLE.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_VISIBLE);
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.NAME_TABLE = "PBITEMPLATECELLGRAPHIC"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_TITLE.FieldName = "CELLGRAPHIC_TITLE"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_TITLE.TableName = "PBITEMPLATECELLGRAPHIC"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_TITLE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_TITLE.Size = 255; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_TITLE);
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.NAME_TABLE = "PBITEMPLATECELLGRAPHIC"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_VISIBLETITLE.FieldName = "CELLGRAPHIC_VISIBLETITLE"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_VISIBLETITLE.TableName = "PBITEMPLATECELLGRAPHIC"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_VISIBLETITLE.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_VISIBLETITLE.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_VISIBLETITLE);
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.NAME_TABLE = "PBITEMPLATECELLGRAPHIC"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_COLORTITLE.FieldName = "CELLGRAPHIC_COLORTITLE"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_COLORTITLE.TableName = "PBITEMPLATECELLGRAPHIC"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_COLORTITLE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_COLORTITLE.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_COLORTITLE);
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.NAME_TABLE = "PBITEMPLATECELLGRAPHIC"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_SIZETITLE.FieldName = "CELLGRAPHIC_SIZETITLE"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_SIZETITLE.TableName = "PBITEMPLATECELLGRAPHIC"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_SIZETITLE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_SIZETITLE.Size = 10; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_SIZETITLE);
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.NAME_TABLE = "PBITEMPLATECELLGRAPHIC"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_ALIGNTITLE.FieldName = "CELLGRAPHIC_ALIGNTITLE"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_ALIGNTITLE.TableName = "PBITEMPLATECELLGRAPHIC"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_ALIGNTITLE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_ALIGNTITLE.Size = 50; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_ALIGNTITLE);
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC_COLUMN.NAME_TABLE = "PBITEMPLATECELLGRAPHIC_COLUMN"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC_COLUMN.FieldName = "IDPBITEMPLATECELLGRAPHIC_COLUMN"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC_COLUMN.TableName = "PBITEMPLATECELLGRAPHIC_COLUMN"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC_COLUMN.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC_COLUMN.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC_COLUMN);
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC_COLUMN.NAME_TABLE = "PBITEMPLATECELLGRAPHIC_COLUMN"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC.FieldName = "IDPBITEMPLATECELLGRAPHIC"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC.TableName = "PBITEMPLATECELLGRAPHIC_COLUMN"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC);
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC_COLUMN.NAME_TABLE = "PBITEMPLATECELLGRAPHIC_COLUMN"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC_COLUMN.COLUMN_NAME.FieldName = "COLUMN_NAME"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC_COLUMN.COLUMN_NAME.TableName = "PBITEMPLATECELLGRAPHIC_COLUMN"; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC_COLUMN.COLUMN_NAME.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC_COLUMN.COLUMN_NAME.Size = 32; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.PBITEMPLATECELLGRAPHIC_COLUMN.COLUMN_NAME);
UsrCfg.InternoAtisNames.FORUMS.NAME_TABLE = "FORUMS"; 
UsrCfg.InternoAtisNames.FORUMS.IDFORUMS.FieldName = "IDFORUMS"; 
UsrCfg.InternoAtisNames.FORUMS.IDFORUMS.TableName = "FORUMS"; 
UsrCfg.InternoAtisNames.FORUMS.IDFORUMS.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.FORUMS.IDFORUMS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.FORUMS.IDFORUMS);
UsrCfg.InternoAtisNames.FORUMS.NAME_TABLE = "FORUMS"; 
UsrCfg.InternoAtisNames.FORUMS.IDCMDBCI.FieldName = "IDCMDBCI"; 
UsrCfg.InternoAtisNames.FORUMS.IDCMDBCI.TableName = "FORUMS"; 
UsrCfg.InternoAtisNames.FORUMS.IDCMDBCI.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.FORUMS.IDCMDBCI.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.FORUMS.IDCMDBCI);
UsrCfg.InternoAtisNames.FORUMS.NAME_TABLE = "FORUMS"; 
UsrCfg.InternoAtisNames.FORUMS.NAMEFORUMS.FieldName = "NAMEFORUMS"; 
UsrCfg.InternoAtisNames.FORUMS.NAMEFORUMS.TableName = "FORUMS"; 
UsrCfg.InternoAtisNames.FORUMS.NAMEFORUMS.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.FORUMS.NAMEFORUMS.Size = 250; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.FORUMS.NAMEFORUMS);
UsrCfg.InternoAtisNames.FORUMS.NAME_TABLE = "FORUMS"; 
UsrCfg.InternoAtisNames.FORUMS.DESCRIPTIONFORUMS.FieldName = "DESCRIPTIONFORUMS"; 
UsrCfg.InternoAtisNames.FORUMS.DESCRIPTIONFORUMS.TableName = "FORUMS"; 
UsrCfg.InternoAtisNames.FORUMS.DESCRIPTIONFORUMS.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.FORUMS.DESCRIPTIONFORUMS.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.FORUMS.DESCRIPTIONFORUMS);
UsrCfg.InternoAtisNames.FORUMSCATEGORY.NAME_TABLE = "FORUMSCATEGORY"; 
UsrCfg.InternoAtisNames.FORUMSCATEGORY.IDFORUMSCATEGORY.FieldName = "IDFORUMSCATEGORY"; 
UsrCfg.InternoAtisNames.FORUMSCATEGORY.IDFORUMSCATEGORY.TableName = "FORUMSCATEGORY"; 
UsrCfg.InternoAtisNames.FORUMSCATEGORY.IDFORUMSCATEGORY.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.FORUMSCATEGORY.IDFORUMSCATEGORY.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.FORUMSCATEGORY.IDFORUMSCATEGORY);
UsrCfg.InternoAtisNames.FORUMSCATEGORY.NAME_TABLE = "FORUMSCATEGORY"; 
UsrCfg.InternoAtisNames.FORUMSCATEGORY.IDMDCATEGORYDETAIL.FieldName = "IDMDCATEGORYDETAIL"; 
UsrCfg.InternoAtisNames.FORUMSCATEGORY.IDMDCATEGORYDETAIL.TableName = "FORUMSCATEGORY"; 
UsrCfg.InternoAtisNames.FORUMSCATEGORY.IDMDCATEGORYDETAIL.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.FORUMSCATEGORY.IDMDCATEGORYDETAIL.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.FORUMSCATEGORY.IDMDCATEGORYDETAIL);
UsrCfg.InternoAtisNames.FORUMSCATEGORY.NAME_TABLE = "FORUMSCATEGORY"; 
UsrCfg.InternoAtisNames.FORUMSCATEGORY.IDFORUMS.FieldName = "IDFORUMS"; 
UsrCfg.InternoAtisNames.FORUMSCATEGORY.IDFORUMS.TableName = "FORUMSCATEGORY"; 
UsrCfg.InternoAtisNames.FORUMSCATEGORY.IDFORUMS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.FORUMSCATEGORY.IDFORUMS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.FORUMSCATEGORY.IDFORUMS);
UsrCfg.InternoAtisNames.FORUMSCATEGORY.NAME_TABLE = "FORUMSCATEGORY"; 
UsrCfg.InternoAtisNames.FORUMSCATEGORY.DESCRIPTIONFORUMSCATEGORY.FieldName = "DESCRIPTIONFORUMSCATEGORY"; 
UsrCfg.InternoAtisNames.FORUMSCATEGORY.DESCRIPTIONFORUMSCATEGORY.TableName = "FORUMSCATEGORY"; 
UsrCfg.InternoAtisNames.FORUMSCATEGORY.DESCRIPTIONFORUMSCATEGORY.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.FORUMSCATEGORY.DESCRIPTIONFORUMSCATEGORY.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.FORUMSCATEGORY.DESCRIPTIONFORUMSCATEGORY);
UsrCfg.InternoAtisNames.FORUMSCATEGORY.NAME_TABLE = "FORUMSCATEGORY"; 
UsrCfg.InternoAtisNames.FORUMSCATEGORY.TOPICSFORUMSCATEGORY_COUNT.FieldName = "TOPICSFORUMSCATEGORY_COUNT"; 
UsrCfg.InternoAtisNames.FORUMSCATEGORY.TOPICSFORUMSCATEGORY_COUNT.TableName = "FORUMSCATEGORY"; 
UsrCfg.InternoAtisNames.FORUMSCATEGORY.TOPICSFORUMSCATEGORY_COUNT.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.FORUMSCATEGORY.TOPICSFORUMSCATEGORY_COUNT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.FORUMSCATEGORY.TOPICSFORUMSCATEGORY_COUNT);
UsrCfg.InternoAtisNames.FORUMSCATEGORY.NAME_TABLE = "FORUMSCATEGORY"; 
UsrCfg.InternoAtisNames.FORUMSCATEGORY.RESPONSEFORUMSCATEGORY_COUNT.FieldName = "RESPONSEFORUMSCATEGORY_COUNT"; 
UsrCfg.InternoAtisNames.FORUMSCATEGORY.RESPONSEFORUMSCATEGORY_COUNT.TableName = "FORUMSCATEGORY"; 
UsrCfg.InternoAtisNames.FORUMSCATEGORY.RESPONSEFORUMSCATEGORY_COUNT.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.FORUMSCATEGORY.RESPONSEFORUMSCATEGORY_COUNT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.FORUMSCATEGORY.RESPONSEFORUMSCATEGORY_COUNT);
UsrCfg.InternoAtisNames.FORUMSCATEGORY.NAME_TABLE = "FORUMSCATEGORY"; 
UsrCfg.InternoAtisNames.FORUMSCATEGORY.MEMBERSFORUMSCATEGORY_COUNT.FieldName = "MEMBERSFORUMSCATEGORY_COUNT"; 
UsrCfg.InternoAtisNames.FORUMSCATEGORY.MEMBERSFORUMSCATEGORY_COUNT.TableName = "FORUMSCATEGORY"; 
UsrCfg.InternoAtisNames.FORUMSCATEGORY.MEMBERSFORUMSCATEGORY_COUNT.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.FORUMSCATEGORY.MEMBERSFORUMSCATEGORY_COUNT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.FORUMSCATEGORY.MEMBERSFORUMSCATEGORY_COUNT);
UsrCfg.InternoAtisNames.FORUMSCATEGORY.NAME_TABLE = "FORUMSCATEGORY"; 
UsrCfg.InternoAtisNames.FORUMSCATEGORY.IMAGEFORUMSCATEGORY.FieldName = "IMAGEFORUMSCATEGORY"; 
UsrCfg.InternoAtisNames.FORUMSCATEGORY.IMAGEFORUMSCATEGORY.TableName = "FORUMSCATEGORY"; 
UsrCfg.InternoAtisNames.FORUMSCATEGORY.IMAGEFORUMSCATEGORY.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.FORUMSCATEGORY.IMAGEFORUMSCATEGORY.Size = 255; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.FORUMSCATEGORY.IMAGEFORUMSCATEGORY);
UsrCfg.InternoAtisNames.FORUMSCATEGORY.NAME_TABLE = "FORUMSCATEGORY"; 
UsrCfg.InternoAtisNames.FORUMSCATEGORY.STATUSFORUMSCATEGORY.FieldName = "STATUSFORUMSCATEGORY"; 
UsrCfg.InternoAtisNames.FORUMSCATEGORY.STATUSFORUMSCATEGORY.TableName = "FORUMSCATEGORY"; 
UsrCfg.InternoAtisNames.FORUMSCATEGORY.STATUSFORUMSCATEGORY.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.FORUMSCATEGORY.STATUSFORUMSCATEGORY.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.FORUMSCATEGORY.STATUSFORUMSCATEGORY);
UsrCfg.InternoAtisNames.TOPICS.NAME_TABLE = "TOPICS"; 
UsrCfg.InternoAtisNames.TOPICS.IDTOPICS.FieldName = "IDTOPICS"; 
UsrCfg.InternoAtisNames.TOPICS.IDTOPICS.TableName = "TOPICS"; 
UsrCfg.InternoAtisNames.TOPICS.IDTOPICS.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.TOPICS.IDTOPICS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.TOPICS.IDTOPICS);
UsrCfg.InternoAtisNames.TOPICS.NAME_TABLE = "TOPICS"; 
UsrCfg.InternoAtisNames.TOPICS.IDFORUMSCATEGORY.FieldName = "IDFORUMSCATEGORY"; 
UsrCfg.InternoAtisNames.TOPICS.IDFORUMSCATEGORY.TableName = "TOPICS"; 
UsrCfg.InternoAtisNames.TOPICS.IDFORUMSCATEGORY.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.TOPICS.IDFORUMSCATEGORY.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.TOPICS.IDFORUMSCATEGORY);
UsrCfg.InternoAtisNames.TOPICS.NAME_TABLE = "TOPICS"; 
UsrCfg.InternoAtisNames.TOPICS.IDCMDBCI.FieldName = "IDCMDBCI"; 
UsrCfg.InternoAtisNames.TOPICS.IDCMDBCI.TableName = "TOPICS"; 
UsrCfg.InternoAtisNames.TOPICS.IDCMDBCI.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.TOPICS.IDCMDBCI.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.TOPICS.IDCMDBCI);
UsrCfg.InternoAtisNames.TOPICS.NAME_TABLE = "TOPICS"; 
UsrCfg.InternoAtisNames.TOPICS.NAMETOPICS.FieldName = "NAMETOPICS"; 
UsrCfg.InternoAtisNames.TOPICS.NAMETOPICS.TableName = "TOPICS"; 
UsrCfg.InternoAtisNames.TOPICS.NAMETOPICS.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.TOPICS.NAMETOPICS.Size = 150; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.TOPICS.NAMETOPICS);
UsrCfg.InternoAtisNames.TOPICS.NAME_TABLE = "TOPICS"; 
UsrCfg.InternoAtisNames.TOPICS.DESCRIPTIONTOPICS.FieldName = "DESCRIPTIONTOPICS"; 
UsrCfg.InternoAtisNames.TOPICS.DESCRIPTIONTOPICS.TableName = "TOPICS"; 
UsrCfg.InternoAtisNames.TOPICS.DESCRIPTIONTOPICS.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.TOPICS.DESCRIPTIONTOPICS.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.TOPICS.DESCRIPTIONTOPICS);
UsrCfg.InternoAtisNames.TOPICS.NAME_TABLE = "TOPICS"; 
UsrCfg.InternoAtisNames.TOPICS.RESPONSETOPICS_COUNT.FieldName = "RESPONSETOPICS_COUNT"; 
UsrCfg.InternoAtisNames.TOPICS.RESPONSETOPICS_COUNT.TableName = "TOPICS"; 
UsrCfg.InternoAtisNames.TOPICS.RESPONSETOPICS_COUNT.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.TOPICS.RESPONSETOPICS_COUNT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.TOPICS.RESPONSETOPICS_COUNT);
UsrCfg.InternoAtisNames.TOPICS.NAME_TABLE = "TOPICS"; 
UsrCfg.InternoAtisNames.TOPICS.VIEWSTOPICS_COUNT.FieldName = "VIEWSTOPICS_COUNT"; 
UsrCfg.InternoAtisNames.TOPICS.VIEWSTOPICS_COUNT.TableName = "TOPICS"; 
UsrCfg.InternoAtisNames.TOPICS.VIEWSTOPICS_COUNT.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.TOPICS.VIEWSTOPICS_COUNT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.TOPICS.VIEWSTOPICS_COUNT);
UsrCfg.InternoAtisNames.TOPICS.NAME_TABLE = "TOPICS"; 
UsrCfg.InternoAtisNames.TOPICS.IMAGETOPICS.FieldName = "IMAGETOPICS"; 
UsrCfg.InternoAtisNames.TOPICS.IMAGETOPICS.TableName = "TOPICS"; 
UsrCfg.InternoAtisNames.TOPICS.IMAGETOPICS.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.TOPICS.IMAGETOPICS.Size = 250; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.TOPICS.IMAGETOPICS);
UsrCfg.InternoAtisNames.TOPICS.NAME_TABLE = "TOPICS"; 
UsrCfg.InternoAtisNames.TOPICS.STATUSTOPICS.FieldName = "STATUSTOPICS"; 
UsrCfg.InternoAtisNames.TOPICS.STATUSTOPICS.TableName = "TOPICS"; 
UsrCfg.InternoAtisNames.TOPICS.STATUSTOPICS.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.TOPICS.STATUSTOPICS.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.TOPICS.STATUSTOPICS);
UsrCfg.InternoAtisNames.TOPICS.NAME_TABLE = "TOPICS"; 
UsrCfg.InternoAtisNames.TOPICS.CREATEDDATETOPICS.FieldName = "CREATEDDATETOPICS"; 
UsrCfg.InternoAtisNames.TOPICS.CREATEDDATETOPICS.TableName = "TOPICS"; 
UsrCfg.InternoAtisNames.TOPICS.CREATEDDATETOPICS.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.TOPICS.CREATEDDATETOPICS.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.TOPICS.CREATEDDATETOPICS);
UsrCfg.InternoAtisNames.TOPICS.NAME_TABLE = "TOPICS"; 
UsrCfg.InternoAtisNames.TOPICS.KEYWORDSTOPICS.FieldName = "KEYWORDSTOPICS"; 
UsrCfg.InternoAtisNames.TOPICS.KEYWORDSTOPICS.TableName = "TOPICS"; 
UsrCfg.InternoAtisNames.TOPICS.KEYWORDSTOPICS.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.TOPICS.KEYWORDSTOPICS.Size = 500; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.TOPICS.KEYWORDSTOPICS);
UsrCfg.InternoAtisNames.TOPICSRESPONSE.NAME_TABLE = "TOPICSRESPONSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICSRESPONSE.FieldName = "IDTOPICSRESPONSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICSRESPONSE.TableName = "TOPICSRESPONSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICSRESPONSE.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICSRESPONSE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICSRESPONSE);
UsrCfg.InternoAtisNames.TOPICSRESPONSE.NAME_TABLE = "TOPICSRESPONSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICS.FieldName = "IDTOPICS"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICS.TableName = "TOPICSRESPONSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICS);
UsrCfg.InternoAtisNames.TOPICSRESPONSE.NAME_TABLE = "TOPICSRESPONSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDCMDBCI.FieldName = "IDCMDBCI"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDCMDBCI.TableName = "TOPICSRESPONSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDCMDBCI.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDCMDBCI.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDCMDBCI);
UsrCfg.InternoAtisNames.TOPICSRESPONSE.NAME_TABLE = "TOPICSRESPONSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICSRESPONSE_PARENT.FieldName = "IDTOPICSRESPONSE_PARENT"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICSRESPONSE_PARENT.TableName = "TOPICSRESPONSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICSRESPONSE_PARENT.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICSRESPONSE_PARENT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.TOPICSRESPONSE.IDTOPICSRESPONSE_PARENT);
UsrCfg.InternoAtisNames.TOPICSRESPONSE.NAME_TABLE = "TOPICSRESPONSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.TITLETOPICSRESPONSE.FieldName = "TITLETOPICSRESPONSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.TITLETOPICSRESPONSE.TableName = "TOPICSRESPONSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.TITLETOPICSRESPONSE.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.TITLETOPICSRESPONSE.Size = 150; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.TOPICSRESPONSE.TITLETOPICSRESPONSE);
UsrCfg.InternoAtisNames.TOPICSRESPONSE.NAME_TABLE = "TOPICSRESPONSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.SUBJECTTOPICSRESPONSE.FieldName = "SUBJECTTOPICSRESPONSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.SUBJECTTOPICSRESPONSE.TableName = "TOPICSRESPONSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.SUBJECTTOPICSRESPONSE.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.SUBJECTTOPICSRESPONSE.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.TOPICSRESPONSE.SUBJECTTOPICSRESPONSE);
UsrCfg.InternoAtisNames.TOPICSRESPONSE.NAME_TABLE = "TOPICSRESPONSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.STATUSTOPICSRESPOSE.FieldName = "STATUSTOPICSRESPOSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.STATUSTOPICSRESPOSE.TableName = "TOPICSRESPONSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.STATUSTOPICSRESPOSE.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.STATUSTOPICSRESPOSE.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.TOPICSRESPONSE.STATUSTOPICSRESPOSE);
UsrCfg.InternoAtisNames.TOPICSRESPONSE.NAME_TABLE = "TOPICSRESPONSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.CREATEDDATETOPICSRESPONSE.FieldName = "CREATEDDATETOPICSRESPONSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.CREATEDDATETOPICSRESPONSE.TableName = "TOPICSRESPONSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.CREATEDDATETOPICSRESPONSE.DataType = SysCfg.DB.Properties.TDataType.DateTime; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.CREATEDDATETOPICSRESPONSE.Size = 8; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.TOPICSRESPONSE.CREATEDDATETOPICSRESPONSE);
UsrCfg.InternoAtisNames.TOPICSRESPONSE.NAME_TABLE = "TOPICSRESPONSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.LIKETOPICSRESPONSE_COUNT.FieldName = "LIKETOPICSRESPONSE_COUNT"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.LIKETOPICSRESPONSE_COUNT.TableName = "TOPICSRESPONSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.LIKETOPICSRESPONSE_COUNT.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.LIKETOPICSRESPONSE_COUNT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.TOPICSRESPONSE.LIKETOPICSRESPONSE_COUNT);
UsrCfg.InternoAtisNames.TOPICSRESPONSE.NAME_TABLE = "TOPICSRESPONSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.NOTLIKETOPICSRESPONSE_COUNT.FieldName = "NOTLIKETOPICSRESPONSE_COUNT"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.NOTLIKETOPICSRESPONSE_COUNT.TableName = "TOPICSRESPONSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.NOTLIKETOPICSRESPONSE_COUNT.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.NOTLIKETOPICSRESPONSE_COUNT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.TOPICSRESPONSE.NOTLIKETOPICSRESPONSE_COUNT);
UsrCfg.InternoAtisNames.TOPICSRESPONSE.NAME_TABLE = "TOPICSRESPONSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.ABUSETOPICSRESPONSE_COUNT.FieldName = "ABUSETOPICSRESPONSE_COUNT"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.ABUSETOPICSRESPONSE_COUNT.TableName = "TOPICSRESPONSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.ABUSETOPICSRESPONSE_COUNT.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.TOPICSRESPONSE.ABUSETOPICSRESPONSE_COUNT.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.TOPICSRESPONSE.ABUSETOPICSRESPONSE_COUNT);
UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.NAME_TABLE = "TOPICSRESPONSEVOTES"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDTOPICSRESPONSEVOTES.FieldName = "IDTOPICSRESPONSEVOTES"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDTOPICSRESPONSEVOTES.TableName = "TOPICSRESPONSEVOTES"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDTOPICSRESPONSEVOTES.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDTOPICSRESPONSEVOTES.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDTOPICSRESPONSEVOTES);
UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.NAME_TABLE = "TOPICSRESPONSEVOTES"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDTOPICSRESPONSE.FieldName = "IDTOPICSRESPONSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDTOPICSRESPONSE.TableName = "TOPICSRESPONSEVOTES"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDTOPICSRESPONSE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDTOPICSRESPONSE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDTOPICSRESPONSE);
UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.NAME_TABLE = "TOPICSRESPONSEVOTES"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDCMDBCI.FieldName = "IDCMDBCI"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDCMDBCI.TableName = "TOPICSRESPONSEVOTES"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDCMDBCI.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDCMDBCI.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.IDCMDBCI);
UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.NAME_TABLE = "TOPICSRESPONSEVOTES"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.LIKETOPICSRESPONSE.FieldName = "LIKETOPICSRESPONSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.LIKETOPICSRESPONSE.TableName = "TOPICSRESPONSEVOTES"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.LIKETOPICSRESPONSE.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.LIKETOPICSRESPONSE.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.LIKETOPICSRESPONSE);
UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.NAME_TABLE = "TOPICSRESPONSEVOTES"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.NOTLIKETOPICSRESPONSE.FieldName = "NOTLIKETOPICSRESPONSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.NOTLIKETOPICSRESPONSE.TableName = "TOPICSRESPONSEVOTES"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.NOTLIKETOPICSRESPONSE.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.NOTLIKETOPICSRESPONSE.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.TOPICSRESPONSEVOTES.NOTLIKETOPICSRESPONSE);
UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.NAME_TABLE = "TOPICSRESPONSEABUSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDTOPICSRESPONSEABUSE.FieldName = "IDTOPICSRESPONSEABUSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDTOPICSRESPONSEABUSE.TableName = "TOPICSRESPONSEABUSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDTOPICSRESPONSEABUSE.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDTOPICSRESPONSEABUSE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDTOPICSRESPONSEABUSE);
UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.NAME_TABLE = "TOPICSRESPONSEABUSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDTOPICSRESPONSE.FieldName = "IDTOPICSRESPONSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDTOPICSRESPONSE.TableName = "TOPICSRESPONSEABUSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDTOPICSRESPONSE.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDTOPICSRESPONSE.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDTOPICSRESPONSE);
UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.NAME_TABLE = "TOPICSRESPONSEABUSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDCMDBCI.FieldName = "IDCMDBCI"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDCMDBCI.TableName = "TOPICSRESPONSEABUSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDCMDBCI.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDCMDBCI.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.IDCMDBCI);
UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.NAME_TABLE = "TOPICSRESPONSEABUSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.LIKETOPICSRESPONSEABUSE.FieldName = "LIKETOPICSRESPONSEABUSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.LIKETOPICSRESPONSEABUSE.TableName = "TOPICSRESPONSEABUSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.LIKETOPICSRESPONSEABUSE.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.LIKETOPICSRESPONSEABUSE.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.LIKETOPICSRESPONSEABUSE);
UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.NAME_TABLE = "TOPICSRESPONSEABUSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.NOTLIKETOPICSRESPONSEABUSE.FieldName = "NOTLIKETOPICSRESPONSEABUSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.NOTLIKETOPICSRESPONSEABUSE.TableName = "TOPICSRESPONSEABUSE"; 
UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.NOTLIKETOPICSRESPONSEABUSE.DataType = SysCfg.DB.Properties.TDataType.Boolean; 
UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.NOTLIKETOPICSRESPONSEABUSE.Size = 1; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.TOPICSRESPONSEABUSE.NOTLIKETOPICSRESPONSEABUSE);
UsrCfg.InternoAtisNames.FORUMSSEARCH.NAME_TABLE = "FORUMSSEARCH"; 
UsrCfg.InternoAtisNames.FORUMSSEARCH.IDFORUMSSEARCH.FieldName = "IDFORUMSSEARCH"; 
UsrCfg.InternoAtisNames.FORUMSSEARCH.IDFORUMSSEARCH.TableName = "FORUMSSEARCH"; 
UsrCfg.InternoAtisNames.FORUMSSEARCH.IDFORUMSSEARCH.DataType = SysCfg.DB.Properties.TDataType.AutoInc; 
UsrCfg.InternoAtisNames.FORUMSSEARCH.IDFORUMSSEARCH.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.FORUMSSEARCH.IDFORUMSSEARCH);
UsrCfg.InternoAtisNames.FORUMSSEARCH.NAME_TABLE = "FORUMSSEARCH"; 
UsrCfg.InternoAtisNames.FORUMSSEARCH.IDFORUMS.FieldName = "IDFORUMS"; 
UsrCfg.InternoAtisNames.FORUMSSEARCH.IDFORUMS.TableName = "FORUMSSEARCH"; 
UsrCfg.InternoAtisNames.FORUMSSEARCH.IDFORUMS.DataType = SysCfg.DB.Properties.TDataType.Int32; 
UsrCfg.InternoAtisNames.FORUMSSEARCH.IDFORUMS.Size = 4; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.FORUMSSEARCH.IDFORUMS);
UsrCfg.InternoAtisNames.FORUMSSEARCH.NAME_TABLE = "FORUMSSEARCH"; 
UsrCfg.InternoAtisNames.FORUMSSEARCH.NAMEFORUMSSEARCH.FieldName = "NAMEFORUMSSEARCH"; 
UsrCfg.InternoAtisNames.FORUMSSEARCH.NAMEFORUMSSEARCH.TableName = "FORUMSSEARCH"; 
UsrCfg.InternoAtisNames.FORUMSSEARCH.NAMEFORUMSSEARCH.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.FORUMSSEARCH.NAMEFORUMSSEARCH.Size = 255; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.FORUMSSEARCH.NAMEFORUMSSEARCH);
UsrCfg.InternoAtisNames.FORUMSSEARCH.NAME_TABLE = "FORUMSSEARCH"; 
UsrCfg.InternoAtisNames.FORUMSSEARCH.IMAGEFORUMSSEARCH.FieldName = "IMAGEFORUMSSEARCH"; 
UsrCfg.InternoAtisNames.FORUMSSEARCH.IMAGEFORUMSSEARCH.TableName = "FORUMSSEARCH"; 
UsrCfg.InternoAtisNames.FORUMSSEARCH.IMAGEFORUMSSEARCH.DataType = SysCfg.DB.Properties.TDataType.String; 
UsrCfg.InternoAtisNames.FORUMSSEARCH.IMAGEFORUMSSEARCH.Size = 255; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.FORUMSSEARCH.IMAGEFORUMSSEARCH);
UsrCfg.InternoAtisNames.FORUMSSEARCH.NAME_TABLE = "FORUMSSEARCH"; 
UsrCfg.InternoAtisNames.FORUMSSEARCH.QUERYFORUMSSEARCH.FieldName = "QUERYFORUMSSEARCH"; 
UsrCfg.InternoAtisNames.FORUMSSEARCH.QUERYFORUMSSEARCH.TableName = "FORUMSSEARCH"; 
UsrCfg.InternoAtisNames.FORUMSSEARCH.QUERYFORUMSSEARCH.DataType = SysCfg.DB.Properties.TDataType.Text; 
UsrCfg.InternoAtisNames.FORUMSSEARCH.QUERYFORUMSSEARCH.Size = 4000; 
SysCfg.DB.Properties.Field_list.push(UsrCfg.InternoAtisNames.FORUMSSEARCH.QUERYFORUMSSEARCH);
