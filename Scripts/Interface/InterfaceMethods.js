﻿//

Interface.Methods.GetInterfaceComponents = function (MDINTERFACETYPE) {
    this.INTERFACEROW = new Array();//Interface.Properties.TINTERFACEROW
    switch (MDINTERFACETYPE) {
        case Interface.Properties.TMDINTERFACETYPE.Attention_Reports:
            INTERFACEROW.Add(new Properties.TINTERFACEROW(Interface.Properties.TPROPERTYNAME._ATNREP_Information_Case, Interface.Properties.TPROPERTYCLASS._Visible, Interface.Properties.TPROPERTYTYPE._Boolean));
            INTERFACEROW.Add(new Properties.TINTERFACEROW(Interface.Properties.TPROPERTYNAME._ATNREP_Information_Parent, Interface.Properties.TPROPERTYCLASS._Visible, Interface.Properties.TPROPERTYTYPE._Boolean));
            INTERFACEROW.Add(new Properties.TINTERFACEROW(Interface.Properties.TPROPERTYNAME._ATNREP_Information_Category, Interface.Properties.TPROPERTYCLASS._Visible, Interface.Properties.TPROPERTYTYPE._Boolean));
            INTERFACEROW.Add(new Properties.TINTERFACEROW(Interface.Properties.TPROPERTYNAME._ATNREP_Information_WorkAround, Interface.Properties.TPROPERTYCLASS._Visible, Interface.Properties.TPROPERTYTYPE._Boolean));
            INTERFACEROW.Add(new Properties.TINTERFACEROW(Interface.Properties.TPROPERTYNAME._ATNREP_Information_User, Interface.Properties.TPROPERTYCLASS._Visible, Interface.Properties.TPROPERTYTYPE._Boolean));

            INTERFACEROW.Add(new Properties.TINTERFACEROW(Interface.Properties.TPROPERTYNAME._ATNREP_StatusScale_Functional, Interface.Properties.TPROPERTYCLASS._Visible, Interface.Properties.TPROPERTYTYPE._Boolean));
            INTERFACEROW.Add(new Properties.TINTERFACEROW(Interface.Properties.TPROPERTYNAME._ATNREP_StatusScale_Hierarchical, Interface.Properties.TPROPERTYCLASS._Visible, Interface.Properties.TPROPERTYTYPE._Boolean));
            INTERFACEROW.Add(new Properties.TINTERFACEROW(Interface.Properties.TPROPERTYNAME._ATNREP_StatusScale_Status, Interface.Properties.TPROPERTYCLASS._Visible, Interface.Properties.TPROPERTYTYPE._Boolean));

            INTERFACEROW.Add(new Properties.TINTERFACEROW(Interface.Properties.TPROPERTYNAME._ATNREP_Link_CI, Interface.Properties.TPROPERTYCLASS._Visible, Interface.Properties.TPROPERTYTYPE._Boolean));
            INTERFACEROW.Add(new Properties.TINTERFACEROW(Interface.Properties.TPROPERTYNAME._ATNREP_Link_AttachFile, Interface.Properties.TPROPERTYCLASS._Visible, Interface.Properties.TPROPERTYTYPE._Boolean));
            INTERFACEROW.Add(new Properties.TINTERFACEROW(Interface.Properties.TPROPERTYNAME._ATNREP_Link_RelatedCases, Interface.Properties.TPROPERTYCLASS._Visible, Interface.Properties.TPROPERTYTYPE._Boolean));

            INTERFACEROW.Add(new Properties.TINTERFACEROW(Interface.Properties.TPROPERTYNAME._ATNREP_AdminEqualCases_View, Interface.Properties.TPROPERTYCLASS._Visible, Interface.Properties.TPROPERTYTYPE._Boolean));
            INTERFACEROW.Add(new Properties.TINTERFACEROW(Interface.Properties.TPROPERTYNAME._ATNREP_AdminEqualCases_Add, Interface.Properties.TPROPERTYCLASS._Visible, Interface.Properties.TPROPERTYTYPE._Boolean));
            INTERFACEROW.Add(new Properties.TINTERFACEROW(Interface.Properties.TPROPERTYNAME._ATNREP_AdminEqualCases_Delete, Interface.Properties.TPROPERTYCLASS._Visible, Interface.Properties.TPROPERTYTYPE._Boolean));

            //INTERFACEROW.Add(new Properties.TINTERFACEROW(Interface.Properties.TPROPERTYNAME._ATNREP_Layout_Save, Interface.Properties.TPROPERTYCLASS._Visible, Interface.Properties.TPROPERTYTYPE._Boolean));
            //INTERFACEROW.Add(new Properties.TINTERFACEROW(Interface.Properties.TPROPERTYNAME._ATNREP_Layout_Restore, Interface.Properties.TPROPERTYCLASS._Visible, Interface.Properties.TPROPERTYTYPE._Boolean));

            INTERFACEROW.Add(new Properties.TINTERFACEROW(Interface.Properties.TPROPERTYNAME._ATNREP_Panel_DescriptionCase, Interface.Properties.TPROPERTYCLASS._Visible, Interface.Properties.TPROPERTYTYPE._Boolean));
            INTERFACEROW.Add(new Properties.TINTERFACEROW(Interface.Properties.TPROPERTYNAME._ATNREP_Panel_StepsDiagram, Interface.Properties.TPROPERTYCLASS._Visible, Interface.Properties.TPROPERTYTYPE._Boolean));
            INTERFACEROW.Add(new Properties.TINTERFACEROW(Interface.Properties.TPROPERTYNAME._ATNREP_Panel_GeneralCaseGuide, Interface.Properties.TPROPERTYCLASS._Visible, Interface.Properties.TPROPERTYTYPE._Boolean));
            INTERFACEROW.Add(new Properties.TINTERFACEROW(Interface.Properties.TPROPERTYNAME._ATNREP_Panel_Atention, Interface.Properties.TPROPERTYCLASS._Visible, Interface.Properties.TPROPERTYTYPE._Boolean));
            INTERFACEROW.Add(new Properties.TINTERFACEROW(Interface.Properties.TPROPERTYNAME._ATNREP_Panel_Messages, Interface.Properties.TPROPERTYCLASS._Visible, Interface.Properties.TPROPERTYTYPE._Boolean));
            INTERFACEROW.Add(new Properties.TINTERFACEROW(Interface.Properties.TPROPERTYNAME._ATNREP_Panel_MatrixOfActivities, Interface.Properties.TPROPERTYCLASS._Visible, Interface.Properties.TPROPERTYTYPE._Boolean));
            INTERFACEROW.Add(new Properties.TINTERFACEROW(Interface.Properties.TPROPERTYNAME._ATNREP_Panel_LifeStatusTab, Interface.Properties.TPROPERTYCLASS._Visible, Interface.Properties.TPROPERTYTYPE._Boolean));
            INTERFACEROW.Add(new Properties.TINTERFACEROW(Interface.Properties.TPROPERTYNAME._ATNREP_Panel_Status, Interface.Properties.TPROPERTYCLASS._Visible, Interface.Properties.TPROPERTYTYPE._Boolean));
            INTERFACEROW.Add(new Properties.TINTERFACEROW(Interface.Properties.TPROPERTYNAME._ATNREP_Panel_StepEditor, Interface.Properties.TPROPERTYCLASS._Visible, Interface.Properties.TPROPERTYTYPE._Boolean));

            break;
        case Interface.Properties.TMDINTERFACETYPE.CI_Editor:
            //INTERFACEROW.Add(new Properties.TINTERFACEROW(Interface.Properties.TPROPERTYNAME._ATNREP_Information_Case, Interface.Properties.TPROPERTYCLASS._Enable, Interface.Properties.TPROPERTYTYPE._Boolean) );
            //INTERFACEROW.Add(new Properties.TINTERFACEROW(Interface.Properties.TPROPERTYNAME._ATNREP_Information_Case, Interface.Properties.TPROPERTYCLASS._Text, Interface.Properties.TPROPERTYTYPE._String) );
            //INTERFACEROW.Add(new Properties.TINTERFACEROW(Interface.Properties.TPROPERTYNAME._None, Interface.Properties.TPROPERTYCLASS._Visible, Interface.Properties.TPROPERTYTYPE._Boolean) );
            break;
        case Interface.Properties.TMDINTERFACETYPE.ST_ExtraField:
            //INTERFACEROW.Add(new Properties.TINTERFACEROW(Interface.Properties.TPROPERTYNAME._None, Interface.Properties.TPROPERTYCLASS._Visible, Interface.Properties.TPROPERTYTYPE._Boolean) );
            break;
        default:
            break;
    }
    return INTERFACEROW;
}

Interface.Methods.ApplyFormbyCode = function (Obj, elemento, CUSTOMCODE)//IDMDINTERFACETYPE
{
    //se debe crear un profiler local y  agregar los valores en la inicializacion o refresco ver ejemplo atencion casos
    ///this.InterfaceProfiler = new Interface.TInterfaceProfiler(CUSTOMCODE);
    ////StrtoCUSTOMCODE(CUSTOMCODE, InterfaceProfiler.INTERFACEROWList);
    //aqui habia una funcion que llenaba el customcode los valores de la forma dinamica         
}

Interface.Methods.MDINTERFACE_Fill = function (MDINTERFACEList, StrIDMDINTERFACE)
{
    MDINTERFACEList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();

    if (StrIDMDINTERFACE == "") {
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDINTERFACE.IDMDINTERFACE.FieldName, " = " + UsrCfg.InternoAtisNames.MDINTERFACE.NAME_TABLE + "." + UsrCfg.InternoAtisNames.MDINTERFACE.IDMDINTERFACE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Where);
    }
    else {
        StrIDMDINTERFACE = " IN (" + StrIDMDINTERFACE + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDINTERFACE.IDMDINTERFACE.FieldName, StrIDMDINTERFACE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    try {
        //*********** MDINTERFACE_GET ***************************** 
        var DS_MDINTERFACE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDINTERFACE_GET", Param.ToBytes());

        var ResErr = DS_MDINTERFACE.ResErr;
        if (ResErr.NotError) {
            if (DS_MDINTERFACE.DataSet.RecordCount > 0) {
                DS_MDINTERFACE.DataSet.First();
                while (!(DS_MDINTERFACE.DataSet.Eof)) {
                    var MDINTERFACE = new Properties.TMDINTERFACE();
                    MDINTERFACE.CUSTOMCODE = DS_MDINTERFACE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDINTERFACE.CUSTOMCODE.FieldName).asString();
                    MDINTERFACE.IDMDINTERFACE = DS_MDINTERFACE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDINTERFACE.IDMDINTERFACE.FieldName).asInt32();
                    MDINTERFACE.IDMDINTERFACETYPE = Interface.Properties.TMDINTERFACETYPE.GetEnum(DS_MDINTERFACE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDINTERFACE.IDMDINTERFACETYPE.FieldName).asInt32());
                    MDINTERFACE.INTERFACE_NAME = DS_MDINTERFACE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDINTERFACE.INTERFACE_NAME.FieldName).asString();
                    MDINTERFACEList.push(MDINTERFACE);
                    DS_MDINTERFACE.DataSet.Next();
                }
            }
            else {
                DS_MDINTERFACE.ResErr.NotError = false;
                DS_MDINTERFACE.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
}

Interface.Methods.MDINTERFACE_GETID = function (MDINTERFACE) {
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDINTERFACE.IDMDINTERFACETYPE.FieldName, MDINTERFACE.IDMDINTERFACETYPE.value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDINTERFACE.INTERFACE_NAME.FieldName, MDINTERFACE.INTERFACE_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        //*********** GET_GETID ***************************** 
        var DS_MDINTERFACE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDINTERFACE_GETID", Param.ToBytes());
        var ResErr = DS_MDINTERFACE.ResErr;
        if (ResErr.NotError) {
            if (DS_MDINTERFACE.DataSet.RecordCount > 0) {
                DS_MDINTERFACE.DataSet.First();
                MDINTERFACE.IDMDINTERFACE = DS_MDINTERFACE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDINTERFACE.IDMDINTERFACE.FieldName).asInt32();
            }
            else {
                DS_MDINTERFACE.ResErr.NotError = false;
                DS_MDINTERFACE.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
}

Interface.Methods.MDINTERFACE_ADD = function (MDINTERFACE) {
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddText(UsrCfg.InternoAtisNames.MDINTERFACE.CUSTOMCODE.FieldName, MDINTERFACE.CUSTOMCODE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDINTERFACE.IDMDINTERFACETYPE.FieldName, MDINTERFACE.IDMDINTERFACETYPE.value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDINTERFACE.INTERFACE_NAME.FieldName, MDINTERFACE.INTERFACE_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //*********** MDINTERFACE_ADD ***************************** 
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDINTERFACE_ADD", Param.ToBytes());
        var ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            MDINTERFACE_GETID(MDINTERFACE);
        }
    }
    finally {
        Param.Destroy();
    }
}

Interface.Methods.MDINTERFACE_UPD = function (MDINTERFACE) {
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddText(UsrCfg.InternoAtisNames.MDINTERFACE.CUSTOMCODE.FieldName, MDINTERFACE.CUSTOMCODE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDINTERFACE.IDMDINTERFACE.FieldName, MDINTERFACE.IDMDINTERFACE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.MDINTERFACE.IDMDINTERFACETYPE.FieldName, MDINTERFACE.IDMDINTERFACETYPE.value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.MDINTERFACE.INTERFACE_NAME.FieldName, MDINTERFACE.INTERFACE_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //*********** MDINTERFACE_UPD ***************************** 
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDINTERFACE_UPD", Param.ToBytes());
        var ResErr = Exec.ResErr;
        if (ResErr.NotError) {
            //GET(); 
        }
    }
    finally {
        Param.Destroy();
    }
}


Interface.Methods.MDINTERFACE_DEL = function (MDINTERFACE) {
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDINTERFACE.IDMDINTERFACE.FieldName, MDINTERFACE.IDMDINTERFACE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //*********** MDINTERFACE_DEL ***************************** 
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "MDINTERFACE_DEL", Param.ToBytes());
        var ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
}

//#region INTERFACEROW
Interface.Methods.INTERFACEROW_ListSetID = function (INTERFACEROWList, PROPERTYNAME, PROPERTYCLASS) {
    for (var i = 0; i < INTERFACEROWList.length; i++) {

        if ((PROPERTYNAME == INTERFACEROWList[i].PROPERTYNAME) && (PROPERTYCLASS == INTERFACEROWList[i].PROPERTYCLASS))
            return (INTERFACEROWList[i]);

    }
    return (new Interface.Properties.TINTERFACEROW(PROPERTYNAME, Interface.Properties.TPROPERTYTYPE._None, PROPERTYCLASS));
}
Interface.Methods.INTERFACEROW_ListAdd = function (INTERFACEROWList, INTERFACEROW) {
    var i = INTERFACEROW_ListGetIndex(INTERFACEROWList, INTERFACEROW);
    if (i == -1) INTERFACEROWList.push(INTERFACEROW);
    else INTERFACEROWList[i] = INTERFACEROW;
}
Interface.Methods.INTERFACEROW_ListGetIndex = function (INTERFACEROWList, INTERFACEROW) {
    for (var i = 0; i < INTERFACEROWList.length; i++) {
        if ((INTERFACEROW.PROPERTYNAME == INTERFACEROWList[i].PROPERTYNAME) && (INTERFACEROW.PROPERTYCLASS == INTERFACEROWList[i].PROPERTYCLASS))
            return (i);
    }
    return (-1);
}
Interface.Methods.INTERFACEROW_ListGetIndex = function (INTERFACEROWList, PROPERTYNAME, PROPERTYCLASS) {
    for (var i = 0; i < INTERFACEROWList.length; i++) {
        if ((PROPERTYNAME == INTERFACEROWList[i].PROPERTYNAME) && (PROPERTYCLASS == INTERFACEROWList[i].PROPERTYCLASS))
            return (i);
    }
    return (-1);
}
//#endregion

Interface.Methods.CUSTOMCODEtoStr = function (INTERFACEROWList) {
    var Res = Interface.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(INTERFACEROWList.length, Longitud, Texto);
        for (Counter = 0; Counter < INTERFACEROWList.length; Counter++) {
            var UnINTERFACEROW = INTERFACEROWList[Counter];
            UnINTERFACEROW.ToStr(Longitud, Texto);
            //SysCfg.Str.Protocol.WriteInt(UnINTERFACEROW.PROPERTYNAME.value, Longitud, Texto);//id                    
            //SysCfg.Str.Protocol.WriteInt(UnINTERFACEROW.PROPERTYTYPE.value, Longitud, Texto);//id
            //SysCfg.Str.Protocol.WriteStr(UnINTERFACEROW.PROPERTYVALUE, Longitud, Texto);
            //SysCfg.Str.Protocol.WriteInt(UnINTERFACEROW.PROPERTYCLASS.value, Longitud, Texto); //Preguntar
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Interface.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
}

Interface.Methods.StrtoCUSTOMCODE = function (ProtocoloStr, INTERFACEROWList) {
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        INTERFACEROWList.length = 0;
        if (Interface.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnINTERFACEROW = new Interface.Properties.TINTERFACEROW(); //Mode new row
                UnINTERFACEROW.StrTo(Pos, Index, LongitudArray, Texto);
                //UnINTERFACEROW.PROPERTYNAME = Interface.Properties.TPROPERTYNAME.GetEnum(SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto));
                //UnINTERFACEROW.PROPERTYTYPE = Interface.Properties.TPROPERTYTYPE.GetEnum(SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto));
                //UnINTERFACEROW.PROPERTYVALUE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
                //UnINTERFACEROW.PROPERTYCLASS = Interface.Properties.TPROPERTYCLASS.GetEnum(SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto));
                Interface.Methods.INTERFACEROW_ListAdd(INTERFACEROWList, UnINTERFACEROW);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}
