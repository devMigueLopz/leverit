﻿//Interface


Interface.TInterfaceProfiler = function () {
    this.ResErr = SysCfg.Error.Properties.TResErr();
    this.evaluar = false;

    this.TInterfaceProfiler = function (inCUSTOMCODE) {
        Interface.Methods.StrtoCUSTOMCODE(inCUSTOMCODE, INTERFACEROWList);
    }


    this.INTERFACEROWList = new Array();//Interface.Properties.TINTERFACEROW


    this.GetInterfaceVisible = function (PROPERTYNAME, PROPERTYCLASS, Value) {
        var idx1 = Interface.Methods.INTERFACEROW_ListGetIndex(this.INTERFACEROWList, PROPERTYNAME, PROPERTYCLASS);
        if (idx1 != -1) {
            if ((INTERFACEROWList[idx1].PROPERTYVALUE == "1") &&(Value == true)) {
                return (true);
            }
            else {
                return (false);
            }
        }
        return Value;
    }

    this.GetInterfaceEnable = function (PROPERTYNAME, PROPERTYCLASS, Value) {
        var idx1 = Interface.Methods.INTERFACEROW_ListGetIndex(this.INTERFACEROWList, PROPERTYNAME, PROPERTYCLASS);
        if (idx1 != -1) {
            if ((INTERFACEROWList[idx1].PROPERTYVALUE == "1")&&(Value=true)) {
                return (true);
            }
            else {
                return (false);
            }
        }
        return Value;
    }


    this.GetInterfaceText = function (PROPERTYNAME, PROPERTYCLASS, Value) {
        var idx1 = Interface.Methods.INTERFACEROW_ListGetIndex(this.INTERFACEROWList, PROPERTYNAME, PROPERTYCLASS);
        if (idx1 != -1) {
            return INTERFACEROWList[idx1].PROPERTYVALUE;
        }
        return Value;
    }
}


