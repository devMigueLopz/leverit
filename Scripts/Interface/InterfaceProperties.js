﻿//Interface.Properties
Interface.Properties.UseElementNamePrefix = false;
Interface.Properties._Version = "002";

Interface.Properties.TINTERFACEROW = function (inPROPERTYNAME, inPROPERTYCLASS, inPROPERTYTYPE) {
    this.PROPERTYNAME = Interface.Properties.TPROPERTYNAME._None;
    this.PROPERTYCLASS = Interface.Properties.TPROPERTYCLASS._None;
    this.PROPERTYTYPE = Interface.Properties.TPROPERTYTYPE.GetEnum(undefined);
    if (arguments.length == 3) {
        this.PROPERTYNAME = arguments[0];
        this.PROPERTYCLASS = arguments[1];
        this.PROPERTYTYPE =  arguments[2];
    }
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.PROPERTYNAME.value, Longitud, Texto);//id                    
        SysCfg.Str.Protocol.WriteInt(this.PROPERTYTYPE.value, Longitud, Texto);//id
        SysCfg.Str.Protocol.WriteStr(this.PROPERTYVALUE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.PROPERTYCLASS.value, Longitud, Texto); //Preguntar
    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.PROPERTYNAME = Interface.Properties.TPROPERTYNAME.GetEnum(SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto));
        this.PROPERTYTYPE = Interface.Properties.TPROPERTYTYPE.GetEnum(SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto));
        this.PROPERTYVALUE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        this.PROPERTYCLASS = Interface.Properties.TPROPERTYCLASS.GetEnum(SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto));
    }

}

Persistence.TPERSISTENCE_MODE = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _Local: { value: 0, name: "Local" },
    _Server_DataBase: { value: 1, name: "Server_DataBase" },
    _Server_Memory: { value: 2, name: "Server_Memory" }
}

Interface.Properties.TMDINTERFACETYPE = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    None: { value: 0, name: "None" },
    Attention_Reports: { value: 1, name: "Attention Reports" },//ATNREP
    CI_Editor: { value: 2, name: "CI Editor" },//CIEDIT
    ST_ExtraField: { value: 3, name: "ST Extra Field" }//STEXFD
}

Interface.Properties.TPROPERTYTYPE = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _None: { value: 0, name: "None" },
    _Boolean: { value: 1, name: "Boolean" },
    _String: { value: 2, name: "String" }
}

Interface.Properties.TPROPERTYCLASS = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _None: { value: 0, name: "None" },
    _Visible: { value: 1, name: "Visible" },
    _Enable: { value: 2, name: "Enable" },
    _Text: { value: 3, name: "Text" }
}

Interface.Properties.TPROPERTYNAME = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _None: { value: 0, name: "None" },
    _ATNREP_Information_Case: { value: 1, name: "Information Case" },
    _ATNREP_Information_Parent: { value: 2, name: "Information Parent" },
    _ATNREP_Information_Category: { value: 3, name: "Information Category" },
    _ATNREP_Information_WorkAround: { value: 4, name: "Information WorkAround" },
    _ATNREP_Information_User: { value: 5, name: "Information User" },
    _ATNREP_StatusScale_Functional: { value: 6, name: "StatusScale Functional" },
    _ATNREP_StatusScale_Hierarchical: { value: 7, name: "StatusScale Hierarchical" },
    _ATNREP_StatusScale_Status: { value: 8, name: "StatusScale Status" },
    _ATNREP_Link_CI: { value: 9, name: "Link CI" },
    _ATNREP_Link_AttachFile: { value: 10, name: "Link AttachFile" },
    _ATNREP_Link_RelatedCases: { value: 11, name: "Link RelatedCases" },
    _ATNREP_AdminEqualCases_View: { value: 12, name: "AdminEqualCases View" },
    _ATNREP_AdminEqualCases_Add: { value: 13, name: "AdminEqualCases Add" },
    _ATNREP_AdminEqualCases_Delete: { value: 14, name: "AdminEqualCases Delete" },
    _ATNREP_Layout_Save: { value: 15, name: "Layout Save" },
    _ATNREP_Layout_Restore: { value: 16, name: "Layout Restore" },
    _ATNREP_Panel_DescriptionCase: { value: 17, name: "Panel DescriptionCase" },
    _ATNREP_Panel_StepsDiagram: { value: 18, name: "Panel StepsDiagram" },
    _ATNREP_Panel_GeneralCaseGuide: { value: 19, name: "Panel GeneralCaseGuide" },
    _ATNREP_Panel_Atention: { value: 20, name: "Panel Atention" },
    _ATNREP_Panel_Messages: { value: 21, name: "Panel Messages" },
    _ATNREP_Panel_MatrixOfActivities: { value: 22, name: "Panel MatrixOfActivities" },
    _ATNREP_Panel_LifeStatusTab: { value: 23, name: "Panel LifeStatusTab" },
    _ATNREP_Panel_Status: { value: 24, name: "Panel Status" },
    _ATNREP_Panel_StepEditor: { value: 25, name: "Panel StepEditor" }
}

Interface.Properties.TMDINTERFACE = function () {
    this.CUSTOMCODE = Interface.Properties._Version + "001(1)0";;
    this.IDMDINTERFACE = Interface.Properties.TMDINTERFACETYPE.GetEnum(undefined);
    this.IDMDINTERFACETYPE = Interface.Properties.TMDINTERFACETYPE.GetEnum(undefined);
    this.INTERFACE_NAME = "";
    this.INTERFACEROWList = new Array();//Interface.Properties.TINTERFACEROW
    this.IsSave = false;
    this._TITLE = "";
    Object.defineProperty(this, 'TITLE', {
        get: function () {
            return this.INTERFACE_NAME + "[" + IDMDINTERFACETYPE.GetName() + "]";
        },
        set: function (_Value) {
            this._TITLE = _Value;
        }
    });
}