﻿// ItHelpCenter.SD.Methods



ItHelpCenter.SD.Methods.SetGetSDFunction = function(GetSDFunction,inSDObject)
{
           
    return Comunic.AjaxJson.Client.Methods.GetSDFunction(GetSDFunction, inSDObject);
    //#if SILVERLIGHT este es el camino para sokcet migracion node js 
    //    return Comunic.Sockets.Client.Methods.GetSDFunction(ref GetSDFunction, inSDObject);
    //#endif
}


ItHelpCenter.SD.Methods.SetSDConsoleAtentionStart = function(SetSDConsoleAtentionStart)
{
    SetSDConsoleAtentionStart.SDConsoleAtentionStart.ResultSDConsoleAtentionStart = UsrCfg.SD.Properties.TResultSDConsoleAtentionStart._IncomeError;
    try
    {
        var GetSDFunction = new UsrCfg.SD.TGetSDFunction();
        GetSDFunction.SDFunction = UsrCfg.SD.Properties.TSDFunction._SetSDConsoleAtentionStart;
        GetSDFunction = ItHelpCenter.SD.Methods.SetGetSDFunction(GetSDFunction, SetSDConsoleAtentionStart);
    }
    catch (Excep)
    {
        SetSDConsoleAtentionStart.ResErr.NotError = false;
        SetSDConsoleAtentionStart.ResErr.Mesaje = Excep;
    }
    return (SetSDConsoleAtentionStart);
}


ItHelpCenter.SD.Methods.SetSDConsoleInfo = function(SetSDConsoleInfo)
{
    SetSDConsoleInfo.SDConsoleAtentionStart.ResultSDConsoleAtentionStart = UsrCfg.SD.Properties.TResultSDConsoleAtentionStart._IncomeError;
    try
    {
        var GetSDFunction = new UsrCfg.SD.TGetSDFunction();
        GetSDFunction.SDFunction = UsrCfg.SD.Properties.TSDFunction._SetSDConsoleInfo;
        GetSDFunction = ItHelpCenter.SD.Methods.SetGetSDFunction(GetSDFunction, SetSDConsoleInfo);
    }
    catch (Excep)
    {
        SetSDConsoleInfo.ResErr.NotError = false;
        SetSDConsoleInfo.ResErr.Mesaje = Excep;
    }
    return (SetSDConsoleInfo);
}



ItHelpCenter.SD.Methods.SetSDOperationCase = function(SetSDOperationCase)
{
    SetSDOperationCase.SDCASEVERYFY.SDCASEVERYFYAction = UsrCfg.SD.Properties.TSDCASEVERYFYAction._ErrorOut;
    try
    {
        var GetSDFunction = new UsrCfg.SD.TGetSDFunction();
        GetSDFunction.SDFunction = UsrCfg.SD.Properties.TSDFunction._SetSDOperationCase;
        GetSDFunction = ItHelpCenter.SD.Methods.SetGetSDFunction(GetSDFunction, SetSDOperationCase);
    }
    catch (Excep)
    {
        SetSDOperationCase.ResErr.NotError = false;
        SetSDOperationCase.ResErr.Mesaje = Excep;
    }
    return (SetSDOperationCase);
}

ItHelpCenter.SD.Methods.SetSDCaseChangeStatus = function(SetSDCaseChangeStatus)
{             
    SetSDCaseChangeStatus.ResultSDCaseChangeStatus = UsrCfg.SD.Properties.TResultSDCaseChangeStatus._CSError;
    try
    {                
        var GetSDFunction = new UsrCfg.SD.TGetSDFunction();
        GetSDFunction.SDFunction = UsrCfg.SD.Properties.TSDFunction._SetSDCaseChangeStatus;
        GetSDFunction = ItHelpCenter.SD.Methods.SetGetSDFunction(GetSDFunction, SetSDCaseChangeStatus);
    }
    catch (Excep)
    {
        SetSDCaseChangeStatus.ResErr.NotError = false;
        SetSDCaseChangeStatus.ResErr.Mesaje = Excep;
    }
    return (SetSDCaseChangeStatus);
}

ItHelpCenter.SD.Methods.SDCaseUserNew = function(SDCaseUserNew)
{            
    try
    {
        var Now = SysCfg.DB.Properties.SVRNOW();
        SDCaseUserNew.SDCASE.IDMANAGERSINFORMED = 0;
        SDCaseUserNew.SDCASE.IDHANDLER = 0;
        SDCaseUserNew.SDCASE.IDOWNER = 0;
        SDCaseUserNew.SDCASE.IDSDCASESOURCETYPE = UsrCfg.SD.Properties.TIDSDCASESOURCETYPE._PERSON;
        SDCaseUserNew.SDCASE.CASE_DATESTART = Now;
        SDCaseUserNew.SDCASE.CASE_DATERESOLVED = Now;
        SDCaseUserNew.SDCASE.CASE_DATECLOSED = Now;
        SDCaseUserNew.SDCASE.CASE_DATECREATE = Now;
        SDCaseUserNew.SDCASE.CASE_DATEPROGRESS = Now;
        SDCaseUserNew.SDCASE.CASE_DATELASTCUT = Now;
        SDCaseUserNew.SDCASE.CASE_COUNTTIME = 0;
        SDCaseUserNew.SDCASE.CASE_COUNTTIMEPAUSE = 0;
        SDCaseUserNew.SDCASE.CASE_COUNTTIMERESOLVED = 0;
        SDCaseUserNew.SDCASE.IDSDRESPONSETYPE = UsrCfg.SD.Properties.TSDRESPONSETYPE._SERVICEDESK;
        SDCaseUserNew.SDCASE.IDMDCATEGORYDETAIL_FINAL= SDCaseUserNew.SDCASEMT.IDMDCATEGORYDETAIL;
        SDCaseUserNew.SDCASE.IDMDCATEGORYDETAIL_INITIAL = SDCaseUserNew.SDCASEMT.IDMDCATEGORYDETAIL;
 
        SDCaseUserNew.SDCASE.IDUSER = UsrCfg.Properties.UserATRole.User.IDCMDBCI;
        SDCaseUserNew.SDCASEEF.IDCMDBCI = UsrCfg.Properties.UserATRole.User.IDCMDBCI;

        //SDCaseUserNew.SDCASE.CASE_ISMAYOR = false;
        //SDCaseUserNew.SDCASE.CASE_DESCRIPTION = "Nuevo Problema en impresora samsung";
        //SDCaseUserNew.SDCASE.CASE_TITLE = "Problema en impresora";
        SDCaseUserNew.SDCASE.IDSDCASE = 0;
        //SDCaseUserNew.SDCASE.IDCMDBCONTACTTYPE_USER = 0; 
        SDCaseUserNew.SDCASE.IDSDCASESTATUS = UsrCfg.SD.Properties.TSDCaseStatus._Start;

        //SDCaseUserNew.SDCASEMT.MT_IDMDMODELTYPED = 0;
        //SDCaseUserNew.SDCASEMT.IDMDCATEGORYDETAIL = 12;
        //SDCaseUserNew.SDCASEMT.IDSLA = 0;
        //SDCaseUserNew.SDCASEMT.IDMDIMPACT = 0;
        SDCaseUserNew.SDCASEMT.IDMDPRIORITY = 0;
        //SDCaseUserNew.SDCASEMT.IDMDURGENCY = 2;
        SDCaseUserNew.SDCASEMT.MT_IDMDSERVICETYPE = UsrCfg.SD.Properties.TMDSERVICETYPE._Incident.value;
        SysCfg.Error.Properties.ResErrfill(SDCaseUserNew.ResErr);
        var GetSDFunction = new UsrCfg.SD.TGetSDFunction();
        GetSDFunction.SDFunction = UsrCfg.SD.Properties.TSDFunction._GetNewUsrSDCASE;
        GetSDFunction = ItHelpCenter.SD.Methods.SetGetSDFunction(GetSDFunction, SDCaseUserNew);
    }
    catch (Excep)
    {
        SDCaseUserNew.ResErr.NotError = false;
        SDCaseUserNew.ResErr.Mesaje = Excep;
    }
    return (SDCaseUserNew);
}



ItHelpCenter.SD.Methods.GetNewIDSDCASE = function(inIDSDRESPONSETYPE)
{
    var SDCaseNew = new UsrCfg.SD.TSDCaseNew();
    var  SDCaseEFNew = new UsrCfg.SD.TSDCaseNew();
    SDCaseNew.SDCASE.IDSDCASE = -1;
    try
    {                
        var Now = SysCfg.DB.Properties.SVRNOW();
        SDCaseNew.SDCASE.IDMANAGERSINFORMED = 0;
        SDCaseNew.SDCASE.IDHANDLER = 0;
        SDCaseNew.SDCASE.IDOWNER =  SysCfg.App.Methods.Cfg_WindowsTask.User.IDCMDBCI;    
        SDCaseNew.SDCASE.IDSDCASESOURCETYPE =  UsrCfg.SD.Properties.TIDSDCASESOURCETYPE._PERSON;                
        SDCaseNew.SDCASE.CASE_DATESTART = Now;
        SDCaseNew.SDCASE.CASE_DATERESOLVED = Now;
        SDCaseNew.SDCASE.CASE_DATECLOSED = Now;
        SDCaseNew.SDCASE.CASE_DATECREATE = Now;
        SDCaseNew.SDCASE.CASE_DATEPROGRESS = Now;
        SDCaseNew.SDCASE.CASE_DATELASTCUT = Now;
        SDCaseNew.SDCASE.CASE_COUNTTIME = 0;
        SDCaseNew.SDCASE.CASE_COUNTTIMEPAUSE = 0;
        SDCaseNew.SDCASE.CASE_COUNTTIMERESOLVED = 0;
        SDCaseNew.SDCASE.IDSDRESPONSETYPE = inIDSDRESPONSETYPE;
        SDCaseNew.SDCASEEF.IDCMDBCI = SysCfg.App.Methods.Cfg_WindowsTask.User.IDCMDBCI;
        SDCaseNew.SDCASEEF.CASEEF_DATE = Now;
        SDCaseNew.SDCASEEF.IDSDCASE = 0;
        var GetSDFunction = new UsrCfg.SD.TGetSDFunction();
        GetSDFunction.SDFunction = UsrCfg.SD.Properties.TSDFunction._GetNewIDSDCASE;
        GetSDFunction = ItHelpCenter.SD.Methods.SetGetSDFunction(GetSDFunction, SDCaseNew);
    }
    catch (Excep)
    {
        SDCaseNew.ResErr.NotError = false;
        SDCaseNew.ResErr.Mesaje = Excep; 
    }
    return (SDCaseNew);
}

ItHelpCenter.SD.Methods.GetNewIDSDCASEEF= function()
{
    var SDCaseEFNew = new UsrCfg.SD.TSDCaseEFNew();
    SDCaseEFNew.SDCASEEF.IDSDCASEEF = 0;
    try
    {
        var Now = SysCfg.DB.Properties.SVRNOW();
        SDCaseEFNew.SDCASEEF.IDCMDBCI = SysCfg.App.Methods.Cfg_WindowsTask.User.IDCMDBCI;                
        SDCaseEFNew.SDCASEEF.CASEEF_DATE = Now;
        SDCaseEFNew.SDCASEEF.IDSDCASE = 0;
        var GetSDFunction = new UsrCfg.SD.TGetSDFunction();
        GetSDFunction.SDFunction = UsrCfg.SD.Properties.TSDFunction._GetNewIDSDCASEEF;
        GetSDFunction = ItHelpCenter.SD.Methods.SetGetSDFunction(GetSDFunction, SDCaseEFNew);
    }
    catch (Excep)
    {
        SDCaseEFNew.ResErr.NotError = false;
        SDCaseEFNew.ResErr.Mesaje = Excep;
    }
    return (SDCaseEFNew);
}



ItHelpCenter.SD.Methods.GetSLAbyCATEGORY = function(IDSDCASETYPE,IDMDCATEGORYDETAIL,IDSDRESPONSETYPE,IDCMDBCI,CASE_ISMAYOR,IDMDURGENCY)
{            
    var GetByteMDValueBySLA = new UsrCfg.SD.TGetByteMDValueBySLA();
    GetByteMDValueBySLA.IDSDCASETYPE = IDSDCASETYPE;
    GetByteMDValueBySLA.IDMDCATEGORYDETAIL = IDMDCATEGORYDETAIL;
    GetByteMDValueBySLA.IDSDRESPONSETYPE = IDSDRESPONSETYPE;
    GetByteMDValueBySLA.IDCMDBCI = IDCMDBCI;
    GetByteMDValueBySLA.CASE_ISMAYOR = CASE_ISMAYOR;
    GetByteMDValueBySLA.IDMDURGENCY = IDMDURGENCY;
    return (GetMDValueBySLA(GetByteMDValueBySLA));
}

ItHelpCenter.SD.Methods.GetSLAbyMODELTYPED = function(IDSDCASETYPE,IDMDMODELTYPED)
{
    var GetByteMDValueBySLA = new UsrCfg.SD.TGetByteMDValueBySLA();
    GetByteMDValueBySLA.IDSDCASETYPE = IDSDCASETYPE;
    GetByteMDValueBySLA.IDMDMODELTYPED = IDMDMODELTYPED;
    return(GetMDValueBySLA(GetByteMDValueBySLA));
}

ItHelpCenter.SD.Methods.GetMDValueBySLA = function (GetByteMDValueBySLA) {
    var GetMDValueBySLA = new Atis.SD.TGetMDValueBySLA();
    try {

        var GetSDFunction = new UsrCfg.SD.TGetSDFunction();
        GetSDFunction.SDFunction = UsrCfg.SD.Properties.TSDFunction._GetMDValueBySLA;
        GetSDFunction = ItHelpCenter.SD.Methods.SetGetSDFunction(GetSDFunction, GetByteMDValueBySLA);
        if (GetByteMDValueBySLA.ResErr.NotError) {
            var BytetoDataSet = new SysCfg.MemTable.Properties.TBytetoDataSet();
            SysCfg.Error.Properties.ResErrfill(BytetoDataSet.ResErr);
            try {
                BytetoDataSet = SysCfg.MemTable.Properties.BytetoDataSet(GetByteMDValueBySLA.SQLOpen);
                if (BytetoDataSet.ResErr.NotError) {
                    GetMDValueBySLA.DataGridSet = BytetoDataSet.DataSet;
                    GetMDValueBySLA.DataGridSet.EnableControls = false;
                    GetMDValueBySLA.DataGridSet.First();
                    GetMDValueBySLA.ResErr = BytetoDataSet.ResErr;
                }

            }
            catch (Excep) {
                GetMDValueBySLA.ResErr.Mesaje = Excep;
            }

        }
        else {
            GetMDValueBySLA.ResErr = GetByteMDValueBySLA.ResErr;
        }

    }
    catch (Excep) {
        GetMDValueBySLA.ResErr.NotError = false;
        GetMDValueBySLA.ResErr.Mesaje = Excep;
    }
    return (GetMDValueBySLA);
}



ItHelpCenter.SD.Methods.TGetMDValueBySLA  = function()
{
    this.IDMDCATEGORYDETAIL= 0;
    this.DataGridSet = new SysCfg.MemTable.TDataSet();
    this.ResErr = new SysCfg.Error.Properties.TResErr();
}   


