﻿

//UsrCfg.SD.Service

UsrCfg.SD.Service.WriteResponseSDFunction= function(GetSDFunction,MemStrm_Request)
{
    switch (GetSDFunction.SDFunction)
    {
        case UsrCfg.SD.Properties.TSDFunction._None:
            break;
        case UsrCfg.SD.Properties.TSDFunction._GetNewIDSDCASE:                   
            var SDCaseNew = GetSDFunction.SDObject;//UsrCfg.SD.TSDCaseNew
            SDCaseNew.SDCASE.ToByte(MemStrm_Request);
            SDCaseNew.SDCASEEF.ToByte(MemStrm_Request);
            //SysCfg.Stream.Methods.WriteStreamInt32(ref MemStrm_Request, (Int32)SDCaseNew.IDOWNER);
            //SysCfg.Stream.Methods.WriteStreamDateTime(ref MemStrm_Request, SDCaseNew.DATESTART);
            break;
        case UsrCfg.SD.Properties.TSDFunction._GetMDValueBySLA:
            var GetByteMDValueBySLA = GetSDFunction.SDObject;//UsrCfg.SD.TGetByteMDValueBySLA
            SysCfg.Stream.Methods.WriteStreamInt32(MemStrm_Request, GetByteMDValueBySLA.IDSDCASETYPE.value);
            SysCfg.Stream.Methods.WriteStreamInt32(MemStrm_Request, GetByteMDValueBySLA.IDMDCATEGORYDETAIL);
            SysCfg.Stream.Methods.WriteStreamInt32(MemStrm_Request, GetByteMDValueBySLA.IDMDMODELTYPED);
            SysCfg.Stream.Methods.WriteStreamInt32(MemStrm_Request, GetByteMDValueBySLA.IDSDRESPONSETYPE.value);
            SysCfg.Stream.Methods.WriteStreamInt32(MemStrm_Request, GetByteMDValueBySLA.IDCMDBCI);
            SysCfg.Stream.Methods.WriteStreamBoolean(MemStrm_Request, GetByteMDValueBySLA.CASE_ISMAYOR);
            SysCfg.Stream.Methods.WriteStreamInt32(MemStrm_Request, GetByteMDValueBySLA.IDMDURGENCY);


            break;
        case UsrCfg.SD.Properties.TSDFunction._SetSDCaseChangeStatus:
            var SetSDCaseChangeStatus = GetSDFunction.SDObject; //UsrCfg.SD.TSetSDCaseChangeStatus                  
            SetSDCaseChangeStatus.SDCASE.ToByte(MemStrm_Request);
            SetSDCaseChangeStatus.SDCASEMT.ToByte(MemStrm_Request);                   
            SysCfg.Stream.Methods.WriteStreamInt32(MemStrm_Request, SetSDCaseChangeStatus.IDCMDBCI);
            SysCfg.Stream.Methods.WriteStreamInt32(MemStrm_Request, SetSDCaseChangeStatus.IDSDTYPEUSER.value);                    
            SysCfg.Stream.Methods.WriteStreamInt32(MemStrm_Request, SetSDCaseChangeStatus.IDSDCASESTATUS_NEW.value);
            break;
        case UsrCfg.SD.Properties.TSDFunction._GetNewUsrSDCASE:
            var SDCaseUserNew = GetSDFunction.SDObject;//UsrCfg.SD.TSDCaseUserNew
            SDCaseUserNew.SDCASE.ToByte(MemStrm_Request);
            SDCaseUserNew.SDCASEMT.ToByte(MemStrm_Request);
            SDCaseUserNew.SDCASEEF.ToByte(MemStrm_Request);
            break;
        case UsrCfg.SD.Properties.TSDFunction._SetSDConsoleAtentionStart:
            var SetSDConsoleAtentionStart = GetSDFunction.SDObject;               //UsrCfg.SD.TSetSDConsoleAtentionStart
            SysCfg.Stream.Methods.WriteStreamInt32(MemStrm_Request, SetSDConsoleAtentionStart.IDCMDBCI);
            SysCfg.Stream.Methods.WriteStreamInt32(MemStrm_Request, SetSDConsoleAtentionStart.IDSDTYPEUSER.value);  
            SysCfg.Stream.Methods.WriteStreamInt32(MemStrm_Request, SetSDConsoleAtentionStart.IDSDWHOTOCASETYPE.value);                    
            SysCfg.Stream.Methods.WriteStreamInt32(MemStrm_Request, SetSDConsoleAtentionStart.IDSDCASE);
            SysCfg.Stream.Methods.WriteStreamInt32(MemStrm_Request, SetSDConsoleAtentionStart.IDSDCASEMT);    
            break;
        case UsrCfg.SD.Properties.TSDFunction._SetSDConsoleInfo:
            var SetSDConsoleInfo = GetSDFunction.SDObject;//UsrCfg.SD.TSetSDConsoleInfo
            SysCfg.Stream.Methods.WriteStreamInt32(MemStrm_Request, SetSDConsoleInfo.IDCMDBCI);
            SysCfg.Stream.Methods.WriteStreamInt32(MemStrm_Request, SetSDConsoleInfo.IDSDTYPEUSER.value); 
            SysCfg.Stream.Methods.WriteStreamInt32(MemStrm_Request, SetSDConsoleInfo.IDSDCASE);
            break;

        case UsrCfg.SD.Properties.TSDFunction._SetSDOperationCase:
            var SetSDOperationCase = GetSDFunction.SDObject;//UsrCfg.SD.TSetSDOperationCase 
            SysCfg.Stream.Methods.WriteStreamInt32(MemStrm_Request, SetSDOperationCase.SDOPERATIONTYPES.value);
            switch (SetSDOperationCase.SDOPERATIONTYPES)
            {
                case UsrCfg.SD.Properties.TSDOPERATIONTYPES._ATTENTION:
                    SetSDOperationCase.SDOPERATION_ATTENTIONOld.ToByte(MemStrm_Request);
                    SetSDOperationCase.SDOPERATION_ATTENTION.ToByte(MemStrm_Request);
                    break;
                case UsrCfg.SD.Properties.TSDOPERATIONTYPES._POST:
                    SetSDOperationCase.SDOPERATION_POST.ToByte(MemStrm_Request);
                    break;
                case UsrCfg.SD.Properties.TSDOPERATIONTYPES._FUNCESC:
                    SetSDOperationCase.SDOPERATION_FUNCESC.ToByte(MemStrm_Request);
                    break;
                case UsrCfg.SD.Properties.TSDOPERATIONTYPES._HIERESC:
                    SetSDOperationCase.SDOPERATION_HIERESC.ToByte(MemStrm_Request);
                    break;
                case UsrCfg.SD.Properties.TSDOPERATIONTYPES._PARENT:
                    SetSDOperationCase.SDOPERATION_PARENT.ToByte(MemStrm_Request);
                    break;
                case UsrCfg.SD.Properties.TSDOPERATIONTYPES._CASESTATUS:
                    SetSDOperationCase.SDOPERATION_CASESTATUS.ToByte(MemStrm_Request);
                    break;
                case UsrCfg.SD.Properties.TSDOPERATIONTYPES._CASEACTIVITIES:
                    SetSDOperationCase.SDOPERATION_CASEACTIVITIES.ToByte(MemStrm_Request);
                    SetSDOperationCase.SDCASEACTIVITIES.ToByte(MemStrm_Request);
                    break;
                case UsrCfg.SD.Properties.TSDOPERATIONTYPES._CASEDESCRIPTION:
                    break;
                case UsrCfg.SD.Properties.TSDOPERATIONTYPES._CASETITLE:
                    break;

                default:
                    break;
            }
                    
            SetSDOperationCase.SDCASEVERYFY.ToByte(MemStrm_Request);
            break;
        case UsrCfg.SD.Properties.TSDFunction._GetNewIDSDCASEEF:

            var SDCaseEFNew = GetSDFunction.SDObject; //UsrCfg.SD.TSDCaseEFNew
            SDCaseEFNew.SDCASEEF.ToByte(MemStrm_Request);
            break;
        default:
            break;
    }
}



UsrCfg.SD.Service.ReadResponseSDFunction= function(GetSDFunction,MemStrm_Response)
{
    switch (GetSDFunction.SDFunction)
    {
        case UsrCfg.SD.Properties.TSDFunction._None:
            break;
        case UsrCfg.SD.Properties.TSDFunction._GetNewIDSDCASE:
            var SDCaseNew = GetSDFunction.SDObject;//UsrCfg.SD.TSDCaseNew
            SDCaseNew.ResErr = GetSDFunction.ResErr;
            if (SDCaseNew.ResErr.NotError)
            {
                SDCaseNew.SDCASE.ByteTo(MemStrm_Response);
                SDCaseNew.SDCASEEF.ByteTo(MemStrm_Response);
            }
            break;
        case UsrCfg.SD.Properties.TSDFunction._GetMDValueBySLA:

            var GetMDValueBySLA = GetSDFunction.SDObject;//UsrCfg.SD.TGetByteMDValueBySLA
            GetMDValueBySLA.ResErr = GetSDFunction.ResErr;
            try
            {
                if (GetMDValueBySLA.ResErr.NotError)
                {
                    //GetMDValueBySLA.SDCASEMT.ByteTo(ref MemStrm_Response);
                    GetMDValueBySLA.SQLOpen = new Array(MemStrm_Response.Length - MemStrm_Response.Position);
                    MemStrm_Response.Read(GetMDValueBySLA.SQLOpen, 0, GetMDValueBySLA.SQLOpen.Length);
                }
            }
            catch (Excep)
            {
                GetMDValueBySLA.ResErr.Mesaje = Excep;
            }
            break;
        case UsrCfg.SD.Properties.TSDFunction._SetSDCaseChangeStatus:
            var SetSDCaseChangeStatus = GetSDFunction.SDObject;//UsrCfg.SD.TSetSDCaseChangeStatus
            SetSDCaseChangeStatus.ResErr = GetSDFunction.ResErr;
            if (SetSDCaseChangeStatus.ResErr.NotError)
            {                                                                       
                SetSDCaseChangeStatus.SDCASE.ByteTo(MemStrm_Response);
                SetSDCaseChangeStatus.SDCASEMT.ByteTo(MemStrm_Response);
                SetSDCaseChangeStatus.ResultSDCaseChangeStatus = UsrCfg.SD.Properties.TResultSDCaseChangeStatus.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStrm_Response));
            }
            break;
        case UsrCfg.SD.Properties.TSDFunction._GetNewUsrSDCASE:
            SDCaseUserNew = GetSDFunction.SDObject;//UsrCfg.SD.TSDCaseUserNew
            SDCaseUserNew.ResErr = GetSDFunction.ResErr;
            if (SDCaseUserNew.ResErr.NotError)
            {
                SDCaseUserNew.SDCASE.ByteTo(MemStrm_Response);
                SDCaseUserNew.SDCASEMT.ByteTo(MemStrm_Response);
                SDCaseUserNew.SDCASEEF.ByteTo(MemStrm_Response);
            }
            break;
        case UsrCfg.SD.Properties.TSDFunction._SetSDConsoleAtentionStart:
            SetSDConsoleAtentionStart = GetSDFunction.SDObject;//UsrCfg.SD.TSetSDConsoleAtentionStart
            SetSDConsoleAtentionStart.ResErr = GetSDFunction.ResErr;
            if (SetSDConsoleAtentionStart.ResErr.NotError)
            {
                SetSDConsoleAtentionStart.SDConsoleAtentionStart.SDCONSOLEATENTION.ByteToSDConsoleAtention(MemStrm_Response);
                SetSDConsoleAtentionStart.SDConsoleAtentionStart.SDWHOTOCASE.ByteTo(MemStrm_Response);
                //SetSDConsoleAtentionStart.SDConsoleAtentionStart.SDCASEACTIVITIES.ByteTo(MemStrm_Response);
                SetSDConsoleAtentionStart.SDConsoleAtentionStart.ResultSDConsoleAtentionStart = UsrCfg.SD.Properties.TResultSDConsoleAtentionStart.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStrm_Response));
            }
            break;
        case UsrCfg.SD.Properties.TSDFunction._SetSDConsoleInfo:
            SetSDConsoleInfo = GetSDFunction.SDObject;//UsrCfg.SD.TSetSDConsoleInfo
            SetSDConsoleInfo.ResErr = GetSDFunction.ResErr;
            if (SetSDConsoleInfo.ResErr.NotError)
            {
                SetSDConsoleInfo.SDConsoleAtentionStart.SDCONSOLEATENTION.ByteToSDConsoleAtention(MemStrm_Response);
                SetSDConsoleInfo.SDConsoleAtentionStart.SDWHOTOCASE.ByteTo(MemStrm_Response);
                //SetSDConsoleAtentionStart.SDConsoleAtentionStart.SDCASEACTIVITIES.ByteTo(MemStrm_Response);
                SetSDConsoleInfo.SDConsoleAtentionStart.ResultSDConsoleAtentionStart = UsrCfg.SD.Properties.TResultSDConsoleAtentionStart.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStrm_Response));
            }
            break;

        case UsrCfg.SD.Properties.TSDFunction._SetSDOperationCase:
            var SetSDOperationCase = GetSDFunction.SDObject;//UsrCfg.SD.TSetSDOperationCase
            SetSDOperationCase.ResErr = GetSDFunction.ResErr;
            if (SetSDOperationCase.ResErr.NotError)
            {
                switch (SetSDOperationCase.SDOPERATIONTYPES)
                {
                    case UsrCfg.SD.Properties.TSDOPERATIONTYPES._ATTENTION:
                        SetSDOperationCase.SDOPERATION_ATTENTIONOld.ByteTo(MemStrm_Response);
                        SetSDOperationCase.SDOPERATION_ATTENTION.ByteTo(MemStrm_Response);    
                        break;
                    case UsrCfg.SD.Properties.TSDOPERATIONTYPES._POST:
                        SetSDOperationCase.SDOPERATION_POST.ByteTo(MemStrm_Response);    
                        break;
                    case UsrCfg.SD.Properties.TSDOPERATIONTYPES._FUNCESC:
                        SetSDOperationCase.SDOPERATION_FUNCESC.ByteTo(MemStrm_Response);    
                        break;
                    case UsrCfg.SD.Properties.TSDOPERATIONTYPES._HIERESC:
                        SetSDOperationCase.SDOPERATION_HIERESC.ByteTo(MemStrm_Response);    
                        break;
                    case UsrCfg.SD.Properties.TSDOPERATIONTYPES._PARENT:
                        SetSDOperationCase.SDOPERATION_PARENT.ByteTo(MemStrm_Response);    
                        break;
                    case UsrCfg.SD.Properties.TSDOPERATIONTYPES._CASESTATUS:
                        SetSDOperationCase.SDOPERATION_CASESTATUS.ByteTo(MemStrm_Response);    
                        break;
                    case UsrCfg.SD.Properties.TSDOPERATIONTYPES._CASEACTIVITIES:
                        SetSDOperationCase.SDConsoleAtentionStart.SDCONSOLEATENTION.ByteToSDConsoleAtention(MemStrm_Response);
                        SetSDOperationCase.SDConsoleAtentionStart.SDWHOTOCASE.ByteTo(MemStrm_Response);                                
                        SetSDOperationCase.SDConsoleAtentionStart.ResultSDConsoleAtentionStart = UsrCfg.SD.Properties.TResultSDConsoleAtentionStart.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStrm_Response));                                
                        break;
                    case UsrCfg.SD.Properties.TSDOPERATIONTYPES._CASEDESCRIPTION:
                        break;
                    case UsrCfg.SD.Properties.TSDOPERATIONTYPES._CASETITLE:
                        break;
                    default:
                        break;
                }
                SetSDOperationCase.SDCASEVERYFY.ByteTo(MemStrm_Response);
            }
            break;
        case UsrCfg.SD.Properties.TSDFunction._GetNewIDSDCASEEF:
            var SDCaseEFNew = GetSDFunction.SDObject;//UsrCfg.SD.TSDCaseEFNew
            SDCaseEFNew.ResErr = GetSDFunction.ResErr;
            if (SDCaseEFNew.ResErr.NotError)
            {
                SDCaseEFNew.SDCASEEF.ByteTo(MemStrm_Response);
            }
            break;
        default:
            break;
    }
}

