﻿//UsrCfg.SD


UsrCfg.SD.TGetSDFunction = function()
{
    this.ResErr = new SysCfg.Error.Properties.TResErr();
    this.SDObject = undefined;
    this.SDFunction = UsrCfg.SD.Properties.TSDFunction.GetEnum(undefined);
    /*#if SILVERLIGHT
    public event EventHandler Completed;
    public virtual void OnCompleted(EventArgs e)
    {
        EventHandler handler = Completed;
        if (handler != null)
        {
            System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() => {handler(this, e);});
        }
    }
    #endif*/
}

UsrCfg.SD.TGetByteMDValueBySLA = function()
{
    this.IDSDCASETYPE= UsrCfg.SD.Properties.TIDSDCASETYPE.GetEnum(undefined)
    this.IDMDMODELTYPED=0;
    this.IDMDCATEGORYDETAIL=0;        
    this.IDSDRESPONSETYPE = UsrCfg.SD.Properties.TSDRESPONSETYPE.GetEnum(undefined);
    this.IDCMDBCI=0;
    this.CASE_ISMAYOR=false;
    this.IDMDURGENCY=0;
    this.SQLOpen = new Array();
    this.ResErr = new SysCfg.Error.Properties.TResErr();
}


UsrCfg.SD.TSDCaseNew = function()
{
    this.SDCASE = new UsrCfg.SD.Properties.TSDCASE();
    this.SDCASEEF = new UsrCfg.SD.Properties.TSDCASEEF();
    this.ResErr = new SysCfg.Error.Properties.TResErr();
}
UsrCfg.SD.TSDCaseEFNew
{
    this.SDCASEEF = new UsrCfg.SD.Properties.TSDCASEEF();
    this.ResErr = new SysCfg.Error.Properties.TResErr();
}
      
                
UsrCfg.SD.TSDCaseUserNew = function()
{
    this.SDCASE = new UsrCfg.SD.Properties.TSDCASE();
    this.SDCASEMT = new UsrCfg.SD.Properties.TSDCASEMT();
    this.SDCASEEF = new UsrCfg.SD.Properties.TSDCASEEF();
    this.ResErr = new SysCfg.Error.Properties.TResErr();
}


UsrCfg.SD.TSDNOTIFY_ADD = function()
{
    this.SDNOTIFY = new UsrCfg.SD.Properties.TSDNOTIFY();
    this.ResErr = new SysCfg.Error.Properties.TResErr();
}


UsrCfg.SD.TSetSDCaseChangeStatus = function()
{       
    this.IDCMDBCI=0;
    this.IDSDTYPEUSER = UsrCfg.SD.Properties.TSDTypeUser.GetEnum(undefined);        
    this.IDSDCASESTATUS_NEW =UsrCfg.SD.Properties.TSDCaseStatus.GetEnum(undefined);        ;       
    this.SDCASE = new UsrCfg.SD.Properties.TSDCASE();
    this.SDCASEMT = new UsrCfg.SD.Properties.TSDCASEMT();
    this.ResultSDCaseChangeStatus = UsrCfg.SD.Properties.TResultSDCaseChangeStatus.GetEnum(undefined);
    this.ResErr = new SysCfg.Error.Properties.TResErr();
}

UsrCfg.SD.TSetSDConsoleAtentionStart = function()
{
    this.IDCMDBCI=0;
    this.IDSDTYPEUSER = 0;
    this.IDSDWHOTOCASETYPE = UsrCfg.SD.Properties.TIDSDWHOTOCASETYPE.GetEnum(undefined);
    this.IDSDCASE = 0;
    this.IDSDCASEMT = 0;       
    this.ResErr = new SysCfg.Error.Properties.TResErr();
    this.SDConsoleAtentionStart = new UsrCfg.SD.TSDConsoleAtentionStart();
}

UsrCfg.SD.TSetSDConsoleInfo = function()
{
    this.IDCMDBCI=0; 
    this.IDSDTYPEUSER = 0;
    this.IDSDCASE = 0;
    this.ResErr = new SysCfg.Error.Properties.TResErr();       
    this.SDConsoleAtentionStart = new UsrCfg.SD.TSDConsoleAtentionStart();
}


UsrCfg.SD.TSDConsoleAtentionStart = function()
{
    this.SDCONSOLEATENTION = new UsrCfg.SD.Properties.TSDCONSOLEATENTION();
    this.SDWHOTOCASE = new UsrCfg.SD.Properties.TSDWHOTOCASE();
    this.SDCASEMT_LS_COUNT = 0;
    this.ResultSDConsoleAtentionStart = UsrCfg.SD.Properties.TResultSDConsoleAtentionStart.GetEnum(undefined);

}

UsrCfg.SD.TSetSDOperationCase = function()
{
    this.SDOPERATION_ATTENTION = new UsrCfg.SD.Properties.TSDOPERATION_ATTENTION();
    this.SDOPERATION_ATTENTIONOld = new UsrCfg.SD.Properties.TSDOPERATION_ATTENTION();
    this.SDOPERATION_POST = new UsrCfg.SD.Properties.TSDOPERATION_POST();        
    this.SDOPERATION_FUNCESC = new UsrCfg.SD.Properties.TSDOPERATION_FUNCESC();
    this.SDOPERATION_HIERESC = new UsrCfg.SD.Properties.TSDOPERATION_HIERESC();
    this.SDOPERATION_PARENT = new UsrCfg.SD.Properties.TSDOPERATION_PARENT();        
    //*** Status ******
    this.SDOPERATION_CASESTATUS = new UsrCfg.SD.Properties.TSDOPERATION_CASESTATUS();     
    this.SDCASEMT = new UsrCfg.SD.Properties.TSDCASEMT();
    //*** ACTIVITIES ***
    this.SDOPERATION_CASEACTIVITIES = new UsrCfg.SD.Properties.TSDOPERATION_CASEACTIVITIES();
    this.SDCASEACTIVITIES = new UsrCfg.SD.Properties.TSDCASEACTIVITIES();
    this.SDCONSOLEATENTION = new UsrCfg.SD.Properties.TSDCONSOLEATENTION();
    this.SDWHOTOCASE = new UsrCfg.SD.Properties.TSDWHOTOCASE();        
    this.SDConsoleAtentionStart = new UsrCfg.SD.TSDConsoleAtentionStart();
    //******************
    this.SDCASEVERYFY = new UsrCfg.SD.Properties.TSDCASEVERYFY();       
    this.SDOPERATIONTYPES = UsrCfg.SD.Properties.TSDOPERATIONTYPES.GetEnum(undefined);
    this.ResErr = new SysCfg.Error.Properties.TResErr();
}


 
     