﻿ItHelpCenter.SD.Shared.Atention.ChildUserPermissions.TfrChildUserPermissions = function (ElementDiv, CallbackModalResult, inDataSet) {

    this.TParent = function () {
        return this;
    }.bind(this);

    var _this = this.TParent();
    this.ObjectHtml = ElementDiv;
    this.CallbackModalResult = CallbackModalResult;
    this.DBTranslate = null;
    this.Mythis = "ChildUserPermissions";
    this.ID = this.Mythis + (Math.floor(Math.random() * 1000));
    this.DataSet = inDataSet;
 
    //UsrCfg.Traslate.GetLangText(_this.Mythis, "TabInformation", "Information");
    //UsrCfg.Traslate.GetLangText(_this.Mythis, "TabPermissions", "Permissions");

     _this.InitializeComponent();
}

ItHelpCenter.SD.Shared.Atention.ChildUserPermissions.TfrChildUserPermissions.prototype.InitializeComponent = function () {
    var _this = this.TParent();
    _this.GridDataSet(_this);
}

ItHelpCenter.SD.Shared.Atention.ChildUserPermissions.TfrChildUserPermissions.prototype.GridDataSet = function (sender) {
    var _this = sender;
    var _cont = _this.ObjectHtml;

    _cont.innerHTML = "";

    //arreglo para ocultar columnas
    try {
        var hidden = new Array(21);
        hidden[0] = true;
        hidden[1] = false;
        hidden[2] = false;
        hidden[3] = false;
        hidden[4] = false;
        hidden[5] = true;
        hidden[6] = false;
        hidden[7] = false;
        hidden[8] = true;
        hidden[9] = true;
        hidden[10] = false;
        hidden[11] = false;
        hidden[12] = false;
        hidden[13] = false;
        hidden[14] = true;
        hidden[15] = false;
        hidden[16] = false;
        hidden[17] = false;
        hidden[18] = true;
        hidden[19] = true;
        hidden[20] = true;
        hidden[21] = true;
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ChildUserPermissions.js ItHelpCenter.SD.Shared.Atention.ChildUserPermissions.TfrChildUserPermissions.prototype.GridDataSet", e);
        //debugger;
    }

    var DataSet = _this.DataSet;


    DataSet.AddFields(UsrCfg.InternoAtisNames.SDTYPEUSER.TYPEUSERNAME.FieldName, SysCfg.DB.Properties.TDataType.String, 10);
    DataSet.First();
    while (!(DataSet.Eof)) {
        DataSet.Update();//update
        var IDSDTYPEUSER = DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDTYPEUSER.FieldName).asString();
        var TYPEUSERNAME = Persistence.Catalog.Methods.SDTYPEUSER_ListSetID(Persistence.Profiler.CatalogProfiler.SDTYPEUSERList, IDSDTYPEUSER).TYPEUSERNAME;
        DataSet.FieldsName(UsrCfg.InternoAtisNames.SDTYPEUSER.TYPEUSERNAME.FieldName, TYPEUSERNAME);
        DataSet.Post();
        DataSet.Next();
    }




    //GRID     
    var Grilla = new TGrid(_cont, "", ""); //CREA UN GRID CONTRO
    //aki solo utilizar el datasource jaimesssssssssssssssssssssssssssssssssssssssssssssssssss!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    var tempI = 0;
    for (var i = 0; i < DataSet.FieldCount; i++) {
        if (hidden[i]) {
            var Col0 = new TColumn(); //CREA UNA COLUMNA PARA AGREGAR AL GRID CONTROL
            Col0.Name = DataSet.FieldDefs[i].FieldName;
            Col0.Caption = DataSet.FieldDefs[i].FieldName;
            Col0.DataType = DataSet.FieldDefs[i].DataType;
            Col0.Index = tempI;
            Col0.EnabledEditor = false; //ASIGNA O EXTRAE UN VALO TRUE O FALSE QUE INDICA SI LA COLUMNA MOSTRARA UN EDITOR DE DATOS             
            Grilla.AddColumn(Col0); //AGREGA UNA COLUMNA AL GRIDCONTROL 
            tempI++;
        }
    }

    tempI = 0;

    for (var i = 0; i < DataSet.RecordCount; i++) {//rows
        var NewRow = new TRow(); //CREA UN ROW PARA AGREGAR AL GRIDCONTROL  
        for (var j = 0; j < DataSet.FieldCount; j++) {//columns
            if (hidden[j]) {
                var Cell = new TCell();
                Cell.Value = DataSet.Records[i].Fields[j].Value;
                Cell.IndexColumn = tempI;
                Cell.IndexRow = i;
                NewRow.AddCell(Cell);
                tempI++;
            }

        }

        Grilla.AddRow(NewRow);
    }


    Grilla.OnRowClick = function (sender, EventArgs) {
        _this.RowSelected = EventArgs;
    }
}