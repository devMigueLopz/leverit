﻿
UsrCfg.SD.Properties.TIDSDCASEMTASGSTATUS = 
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _NOAPPLY: { value: 0, name: "_NOAPPLY" },
    _ASSIGNED: { value: 1, name: "_ASSIGNED" },
    _NOASSIGNED: { value: 2, name: "_NOASSIGNED" },
    _ASSIGNEDEND: { value: 3, name: "_ASSIGNEDEND" }
}       

UsrCfg.SD.Properties.TIDSDOPERATION_ESCTYPE =
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _ASSIGNEDNEXTUSER: { value: 0, name: "_ASSIGNEDNEXTUSER" },
    _ASSIGNEDNEXTLAVEL: { value: 1, name: "_ASSIGNEDNEXTLAVEL" },
    _ASSIGNEDUSER: { value: 2, name: "_ASSIGNEDUSER" },
    _NOSCALE: { value: 3, name: "_NOSCALE" }
}

UsrCfg.SD.Properties.TIDSDWHOTOCASETYPE = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _INTERNAL: { value: 0, name: "INTERNAL" },
    _EXTERNAL: { value: 1, name: "EXTERNAL" }
}

UsrCfg.SD.Properties.TIDSDOPERATION_ATTENTIONTYPE =
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _Back: { value: 0, name: "_Back" },
    _Message: { value: 1, name: "_Message" },
    _Next: { value: 2, name: "_Next" },
    _Result: { value: 3, name: "_Result" },
    _Summary: { value: 4, name: "_Summary" }   
}

UsrCfg.SD.Properties.TIDSDSCALETYPE_FUNC =
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _ScalingDisabled: { value: 0, name: "Scaling Disabled" },
    _ScalingWithoutCalendar : { value: 1, name: "Scaling Without Calendar"  },
    _ScalingWithCalendar: { value: 2, name: "Scaling With Calendar" }
}
UsrCfg.SD.Properties.TIDSDSCALETYPE_HIER =
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _ScalingDisabled: { value: 0, name: "Scaling Disabled" },
    _ScalingWithoutCalendar: { value: 1, name: "Scaling Without Calendar" },
    _ScalingWithCalendar: { value: 2, name: "Scaling With Calendar" }
}
      
UsrCfg.SD.Properties.TSDRUNNIGSOURCEMODEL =
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _INMODEL: { value: 0, name: "_INMODEL" },
    _OUTMODEL: { value: 1, name: "OUT MODEL" }
}

UsrCfg.SD.Properties.TIDSDOPERATION_PARENTTYPE =
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _ADDRELATION: { value: 0, name: "_ADDRELATION" },
    _DELRELATION: { value: 1, name: "_DELRELATION" }
}

UsrCfg.SD.Properties.TIDSDCASESOURCETYPE =
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _UNKNOWN: { value: 0, name: "UNKNOW" },
    _PERSON: { value: 1, name: "PERSON" },
    _EVENT: { value: 2, name: "EVENT" }
}

UsrCfg.SD.Properties.TIDSDRUNNIGSTATUS =
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _MODEL: { value: 0, name: "_MODEL" },
    _CREATE: { value: 1, name: "_CREATE" },
    _RUNNING: { value: 2, name: "_RUNNING" },
    _CLOSED: { value: 3, name: "_CLOSED" },
    _CANCELLED: { value: 4, name: "_CANCELLED" }
}

UsrCfg.SD.Properties.TIDSDCASETYPE =
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _NORMAL: { value: 0, name: "_NORMAL" },
    _ACTIVITY_MODEL: { value: 1, name: "_ACTIVITY_MODEL" },
    _ACTIVITY_NEW: { value: 2, name: "_ACTIVITY_NEW" }
}

UsrCfg.SD.Properties.TSDCASEVERYFYAction =
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _Continuous: { value: 0, name: "_Continuous" },
    _ErrorOut: { value: 1, name: "_ErrorOut" },
    _ErrorRefresh: { value: 2, name: "_ErrorRefresh" }
}

UsrCfg.SD.Properties.TSDOPERATIONTYPES =
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _ATTENTION: { value: 0, name: "_ATTENTION" },
    _CASEDESCRIPTION: { value: 1, name: "_CASEDESCRIPTION" },
    _CASETITLE: { value: 2, name: "_CASETITLE" },
    _POST: { value: 3, name: "_POST" },
    _FUNCESC: { value: 5, name: "_FUNCESC" },
    _HIERESC: { value: 6, name: "_HIERESC" },
    _PARENT: { value: 7, name: "_PARENT" },        
    _CASESTATUS: { value: 8, name: "_CASESTATUS" },
    _CASEACTIVITIES: { value: 9, name: "_CASEACTIVITIES" },
    _UPDCaseMT: { value: 10, name: "_UPDCaseMT" }
}

UsrCfg.SD.Properties.TResultSDConsoleAtentionStart =
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _IncomeError: { value: 0, name: "_IncomeError" },
    _IncomeDenied: { value: 1, name: "_IncomeDenied" },
    _IncomeAccepted: { value: 2, name: "_IncomeAccepted" },
    _IncomeCreate: { value: 3, name: "_IncomeCreate" },
    _IncomeStatusExpire: { value: 4, name: "_IncomeStatusExpire" }
}
        
UsrCfg.SD.Properties.TCmd_SDWHOTOCASE =
{            
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _CmdGetInsert: { value: 0, name: "_CmdGetInsert" },
    _CmdGetbyUser: { value: 1, name: "_CmdGetbyUser" },
    _CmdGetbyID: { value: 2, name: "_CmdGetbyID" }
}

UsrCfg.SD.Properties.TSDFunction =
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _None: { value: 0, name: "_None" },
    _GetNewIDSDCASE: { value: 1, name: "_GetNewIDSDCASE" },
    _GetMDValueBySLA: { value: 2, name: "_GetMDValueBySLA" },
    _SetSDCaseChangeStatus: { value: 3, name: "_SetSDCaseChangeStatus" },
    _SetSDConsoleAtentionStart: { value: 4, name: "_SetSDConsoleAtentionStart" },
    _SetSDOperationCase: { value: 5, name: "_SetSDOperationCase" },
    _SetSDConsoleInfo: { value: 6, name: "_SetSDConsoleInfo" },
    _GetNewUsrSDCASE: { value: 7, name: "_GetNewUsrSDCASE" },
    _GetNewIDSDCASEEF: { value: 8, name: "_GetNewIDSDCASEEF" }
}

UsrCfg.SD.Properties.TResultSDCaseChangeStatus =
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _CSError: { value: 0, name: "_CSError" },
    _CSSuccessfullyCreate: { value: 1, name: "_CSSuccessfullyCreate" },
    _CSSuccessfullyClose: { value: 2, name: "_CSSuccessfullyClose" }
}

UsrCfg.SD.Properties.TIDSDCASESETMODELTYPE =
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _None: { value: 0, name: "None" },
    _InsertMT: { value: 1, name: "InsertMT" },
    _UpdateMT: { value: 2, name: "UpdateMT" }
}

UsrCfg.SD.Properties.TSDCaseStatus =
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _Start: { value: 0, name: "Start" },
    _InsertMT: { value: 1, name: "InsertMT" },
    _Created: { value: 2, name: "Created" },
    _InProgress: { value: 3, name: "InProgress" },
    _Paused: { value: 4, name: "Paused" },
    _Closed: { value: 5, name: "Closed" },
    _Cancelled: { value: 6, name: "Cancelled" },
    _Resolved: { value: 7, name: "Resolved" }
}

UsrCfg.SD.Properties.TSDCASEMTSTATUS =
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _Created: { value: 0, name: "_Created" },
    _Assigned: { value: 1, name: "_Assigned" },
    _InProgress: { value: 2, name: "_InProgress" },
    _Cancelled: { value: 3, name: "_Cancelled" }
}

UsrCfg.SD.Properties.TMDLIFESTATUSPERMISSION =
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _Disable: { value: 0, name: "_Disable" },
    _Read: { value: 1, name: "_Read" },
    _Write: { value: 2, name: "_Write" }
}

UsrCfg.SD.Properties.TMDLIFESTATUSBEHAVIOR =
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _None: { value: 0, name: "None" },
    _CSATSurvery: { value: 1, name: "CSATSurvery" }
}

UsrCfg.SD.Properties.TSDCASEMT_LSSTATUS =
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _InProgress: { value: 0, name: "_InProgress" },
    _Closed: { value: 1, name: "_Closed" }
}

UsrCfg.SD.Properties.TSDCASEMT_LSTYPE =
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _Normal: { value: 0, name: "_Normal" }
}


UsrCfg.SD.Properties.TSDTypeUser = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _System: { value: 0, name: "System" },
    _Owner: { value: 1, name: "Owner" },
    _Handler: { value: 2, name:  "Handler" },
    _ManagersInformed: { value: 3, name: "ManagersInformed" },
    _User: { value: 4, name:"User" },
    _Unknown5: { value: 5, name:"_Unknown5" },
    _Unknown6: { value: 6, name: "_Unknown6" },
    _Unknown7: { value: 7, name: "_Unknown7" },
    _Unknown8: { value: 8, name: "_Unknown8" },
    _Unknown9: { value: 9, name:  "_Unknown9" },
    _Unknown10: { value: 10, name: "_Unknown10" },
    _Unknown11: { value: 11, name: "_Unknown11" },
    _Unknown12: { value: 12, name: "_Unknown12" },
    _Unknown13: { value: 13, name: "_Unknown13" },
    _Unknown14: { value: 14, name: "_Unknown14" },
    _Unknown15: { value: 15, name: "_Unknown15" },
    _Unknown16: { value: 16, name: "_Unknown16" },
    _Unknown17: { value: 17, name: "_Unknown17" },
    _Unknown18: { value: 18, name: "_Unknown18" },
    _Unknown19: { value: 19, name: "_Unknown19" },
    _Unknown20: { value: 20, name: "_Unknown20" },
    _Unknown21: { value: 21, name: "_Unknown21" },
    _Unknown22: { value: 22, name: "_Unknown22" },
    _Unknown23: { value: 23, name: "_Unknown23" },
    _Unknown24: { value: 24, name: "_Unknown24" },
    _Unknown25: { value: 25, name: "_Unknown25" },
    _Unknown26: { value: 26, name: "_Unknown26" },
    _Unknown27: { value: 27, name: "_Unknown27" },
    _Unknown28: { value: 28, name: "_Unknown28" },
    _Unknown29: { value: 29, name: "_Unknown29" }
}

UsrCfg.SD.Properties.TMDSERVICEEXTRATABLE_SOURCETYPE =
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    IDSDCASEMT: { value: 1, name: "IDSDCASEMT" },
    IDSDCASE: { value: 2, name: "IDSDCASE" },
    IDSDCASEEF: { value: 3, name: "IDSDCASEEF" }
}

UsrCfg.SD.Properties.TMDSERVICETYPE =
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _Incident: { value: 1, name: "_Incident" },
    _Problem: { value: 2, name: "_Problem" },
    _Workaround: { value: 3, name: "_Workaround" },
    _knownerrors: { value: 4, name: "_knownerrors" },
    _Activity: { value: 5, name: "_Activity" },
    _Requestfulfillment: { value: 6, name: "_Requestfulfillment" },
    _Request_for_Change: { value: 7, name: "_Request_for_Change" }
}

UsrCfg.SD.Properties.TSDRESPONSETYPE =
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _ALL: { value: 0, name: "All" },
    _SERVICEDESK: { value: 1, name: "SERVICEDESK" },
    _PROBLEMMANAGER: { value: 2, name: "PROBLEMMANAGER" },
}

UsrCfg.SD.Properties.TSDWHOTOCASESTATUS =
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _Active: { value: 0, name: "_Active" },
    _Closed: { value: 1, name: "_Closed" },
    _Cancelled: { value: 2, name: "_Cancelled" }
}

UsrCfg.SD.Properties.TSDCASEPERMISSION =
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _VIEW: { value: 0, name: "View" },
    _ADDACTION: { value: 1, name: "Add action" },
    _TRANSFER: { value: 2, name: "Transfer" }
}


UsrCfg.SD.Properties.TSDWHOTOCASE = function()
{
    this.COUNTTIME = 0;
    this.COUNTTIMEPAUSE = 0;
    this.COUNTTIMERESOLVED = 0;
    this.DATELASTCUT = new Date(1970, 0, 1, 0, 0, 0);
    this.DATELASTREAD = new Date(1970, 0, 1, 0, 0, 0);
    this.DATEPERMISSIONDISABLE = new Date(1970, 0, 1, 0, 0, 0);
    this.DATEPERMISSIONENABLE = new Date(1970, 0, 1, 0, 0, 0);
    this.IDCMDBCI = 0;
    this.IDSDCASE = 0;
    this.IDSDCASEMT = 0;
    this.IDSDCASEPERMISSION = UsrCfg.SD.Properties.TSDCASEPERMISSION.GetEnum(undefined);
    this.IDSDTYPEUSER = 0;
    this.IDSDWHOTOCASE = 0;
    this.CASEMT_SET_LAVEL = 0;
    this.IDSDWHOTOCASESTATUS = UsrCfg.SD.Properties.TSDWHOTOCASESTATUS.GetEnum(undefined);
    this.IDSDWHOTOCASETYPE = UsrCfg.SD.Properties.TIDSDWHOTOCASETYPE.GetEnum(undefined);
    this.SerialWCF = function (SDWHOTOCASE) {
        this.COUNTTIME = SDWHOTOCASE.COUNTTIME;
        this.COUNTTIMEPAUSE = SDWHOTOCASE.COUNTTIMEPAUSE;
        this.COUNTTIMERESOLVED = SDWHOTOCASE.COUNTTIMERESOLVED;
        this.DATELASTCUT = SysCfg.DateTimeMethods.formatJSONDateTimetoDate(SDWHOTOCASE.DATELASTCUT);
   
        this.DATELASTREAD = SysCfg.DateTimeMethods.formatJSONDateTimetoDate(SDWHOTOCASE.DATELASTREAD);
        
        this.DATEPERMISSIONDISABLE = SysCfg.DateTimeMethods.formatJSONDateTimetoDate(SDWHOTOCASE.DATEPERMISSIONDISABLE);
        this.DATEPERMISSIONENABLE = SysCfg.DateTimeMethods.formatJSONDateTimetoDate(SDWHOTOCASE.DATEPERMISSIONENABLE);
        this.IDCMDBCI = SDWHOTOCASE.IDCMDBCI;
        this.IDSDCASE = SDWHOTOCASE.IDSDCASE;
        this.IDSDCASEMT = SDWHOTOCASE.IDSDCASEMT;
        this.IDSDCASEPERMISSION = UsrCfg.SD.Properties.TSDCASEPERMISSION.GetEnum(SDWHOTOCASE.IDSDCASEPERMISSION);
        this.IDSDTYPEUSER = SDWHOTOCASE.IDSDTYPEUSER;
        this.IDSDWHOTOCASE = SDWHOTOCASE.IDSDWHOTOCASE;
        this.IDSDWHOTOCASESTATUS =  UsrCfg.SD.Properties.TSDWHOTOCASESTATUS.GetEnum(SDWHOTOCASE.IDSDWHOTOCASESTATUS);
        this.IDSDWHOTOCASETYPE = UsrCfg.SD.Properties.TIDSDWHOTOCASETYPE.GetEnum(SDWHOTOCASE.IDSDWHOTOCASETYPE);
    }


    this.ToByte = function(MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.COUNTTIME);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.COUNTTIMEPAUSE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.COUNTTIMERESOLVED);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.DATELASTCUT);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.DATELASTREAD);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.DATEPERMISSIONDISABLE);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.DATEPERMISSIONENABLE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDCMDBCI);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASEMT);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASEPERMISSION.value);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDTYPEUSER.value);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDWHOTOCASE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDWHOTOCASESTATUS.value);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDWHOTOCASETYPE.value);
                
    }
    this.ByteTo = function(MemStream)
    {
        this.COUNTTIME = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.COUNTTIMEPAUSE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.COUNTTIMERESOLVED = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.DATELASTCUT = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.DATELASTREAD = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.DATEPERMISSIONDISABLE = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.DATEPERMISSIONENABLE = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.IDCMDBCI = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDCASE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDCASEMT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDCASEPERMISSION = UsrCfg.SD.Properties.TSDCASEPERMISSION.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        this.IDSDTYPEUSER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDWHOTOCASE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDWHOTOCASESTATUS = UsrCfg.SD.Properties.TSDWHOTOCASESTATUS.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        this.IDSDWHOTOCASETYPE = UsrCfg.SD.Properties.TIDSDWHOTOCASETYPE.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));                
    }
}


UsrCfg.SD.Properties.TSDCASEMT_LS = function()
{
    this.IDSDCASEMT = 0;
    this.IDSDCASE = 0;
    this.IDSDCASEMT_LS = 0;
    this.IDSDCASEMT_LSSTATUS = TSDCASEMT_LSSTATUS.GetEnum(undefined);
    this.IDSDCASEMT_LSTYPE = TSDCASEMT_LSTYPE.GetEnum(undefined);
    this.CASEMT_LSCOMMENTSST = "";
    this.CASEMT_LSCOUNTTIME = 0;
    this.CASEMT_LSCOUNTTIMEPAUSE = 0;
    this.CASEMT_LSCOUNTTIMERESOLVED = 0;
    this.CASEMT_LSDATELASTCUT = new Date(1970, 0, 1, 0, 0, 0);
    this.CASEMT_LSNAMESTEP = "";
    this.CASEMT_LSNEXTSTEP = "";
    this.CASEMT_LSSTATUSN = 0;

    this.ToByte = function(MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASEMT);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASEMT_LS);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASEMT_LSSTATUS.value);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASEMT_LSTYPE).value;
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CASEMT_LSCOMMENTSST);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.CASEMT_LSCOUNTTIME);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.CASEMT_LSCOUNTTIMEPAUSE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.CASEMT_LSCOUNTTIMERESOLVED);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.CASEMT_LSDATELASTCUT);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CASEMT_LSNAMESTEP);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CASEMT_LSNEXTSTEP);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.CASEMT_LSSTATUSN);
    }
    this.ByteTo = function(MemStream)
    {
        IDSDCASEMT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        IDSDCASEMT_LS = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        IDSDCASEMT_LSSTATUS = UsrCfg.SD.Properties.TSDCASEMT_LSSTATUS.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        IDSDCASEMT_LSTYPE =  UsrCfg.SD.Properties.TSDCASEMT_LSTYPE.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        CASEMT_LSCOMMENTSST = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        CASEMT_LSCOUNTTIME = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        CASEMT_LSCOUNTTIMEPAUSE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        CASEMT_LSCOUNTTIMERESOLVED = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        CASEMT_LSDATELASTCUT = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        CASEMT_LSNAMESTEP = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        CASEMT_LSNEXTSTEP = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        CASEMT_LSSTATUSN = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    } 
}

UsrCfg.SD.Properties.TSDCASE = function()
{
    this.IDHANDLER = 0;
    this.IDOWNER = 0;
    this.IDMANAGERSINFORMED = 0;
    this.IDSDCASE = 0;
    this.IDSDCASE_PARENT = 0;
    this.IDSDCASESTATUS = UsrCfg.SD.Properties.TSDCaseStatus.GetEnum(undefined);
    this.IDSDRESPONSETYPE = UsrCfg.SD.Properties.TSDRESPONSETYPE.GetEnum(undefined);
    this.IDUSER = 0;
    this.CASE_DATECREATE = new Date(1970, 0, 1, 0, 0, 0);
    this.CASE_DATERESOLVED = new Date(1970, 0, 1, 0, 0, 0);
    this.CASE_DATECLOSED = new Date(1970, 0, 1, 0, 0, 0);
    this.CASE_DATELASTCUT = new Date(1970, 0, 1, 0, 0, 0);
    this.IDSDCASESOURCETYPE = UsrCfg.SD.Properties.TIDSDCASESOURCETYPE._UNKNOWN;
    this.IDCMDBCONTACTTYPE_USER = 0;
    this.IDMDCATEGORYDETAIL_INITIAL = 0;
    this.IDMDCATEGORYDETAIL_FINAL = 0;
    this.CASE_DATESTART = new Date(1970, 0, 1, 0, 0, 0);
    this.CASE_DATEPROGRESS = new Date(1970, 0, 1, 0, 0, 0);
    this.CASE_DESCRIPTION = "";
    this.CASE_FINALSUMM = "";
    this.CASE_ISMAYOR = false;
    this.CASE_COUNTTIME = 0;
    this.CASE_COUNTTIMEPAUSE = 0;
    this.CASE_COUNTTIMERESOLVED = 0;
    this.CASE_RETURN_COST = 0;
    this.CASE_RETURN_STR = "";
    this.CASE_TITLE = "";
         
    this.ToByte = function(MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDHANDLER);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDOWNER);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMANAGERSINFORMED);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASE_PARENT);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASESTATUS.value);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDRESPONSETYPE.value);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDUSER);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.CASE_DATECREATE);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.CASE_DATERESOLVED);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.CASE_DATECLOSED);                
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.CASE_DATELASTCUT);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASESOURCETYPE.value);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDCMDBCONTACTTYPE_USER);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDCATEGORYDETAIL_INITIAL);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDCATEGORYDETAIL_FINAL);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.CASE_DATESTART);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.CASE_DATEPROGRESS);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CASE_DESCRIPTION);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CASE_FINALSUMM);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.CASE_ISMAYOR);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.CASE_COUNTTIME);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.CASE_COUNTTIMEPAUSE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.CASE_COUNTTIMERESOLVED);
        SysCfg.Stream.Methods.WriteStreamDouble(MemStream, this.CASE_RETURN_COST);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CASE_RETURN_STR);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CASE_TITLE);                               
    }
    this.ByteTo = function(MemStream)
    {
        this.IDHANDLER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDOWNER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMANAGERSINFORMED = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);                
        this.IDSDCASE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDCASE_PARENT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDCASESTATUS = UsrCfg.SD.Properties.TSDCaseStatus.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        this.IDSDRESPONSETYPE = UsrCfg.SD.Properties.TSDRESPONSETYPE.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        this.IDUSER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.CASE_DATECREATE = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.CASE_DATERESOLVED = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.CASE_DATECLOSED = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.CASE_DATELASTCUT = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.IDSDCASESOURCETYPE = UsrCfg.SD.Properties.TIDSDCASESOURCETYPE.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        this.IDCMDBCONTACTTYPE_USER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDCATEGORYDETAIL_INITIAL = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDCATEGORYDETAIL_FINAL = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.CASE_DATESTART = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.CASE_DATEPROGRESS = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.CASE_DESCRIPTION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CASE_FINALSUMM = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CASE_ISMAYOR = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.CASE_COUNTTIME = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.CASE_COUNTTIMEPAUSE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.CASE_COUNTTIMERESOLVED = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);  
        this.CASE_RETURN_COST = SysCfg.Stream.Methods.ReadStreamDouble(MemStream);
        this.CASE_RETURN_STR = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CASE_TITLE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);                      
    }
}



UsrCfg.SD.Properties.TSDCASEEF = function()
{
    
    this.CASEEF_DATE = new Date(1970, 0, 1, 0, 0, 0);
    this.IDCMDBCI = 0;
    this.IDSDCASE = 0;
    this.IDSDCASEEF = 0;
    //Socket IO Properties
    this.ToByte = function(MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.CASEEF_DATE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDCMDBCI);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASEEF);
    }
    this.ByteTo = function(MemStream)
    {
        this.CASEEF_DATE = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.IDCMDBCI = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDCASE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDCASEEF = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    }
}



UsrCfg.SD.Properties.TSDCASEMT = function()
{

    this.IDMDCATEGORYDETAIL = 0;
    this.IDMDIMPACT = 0;
    this.IDMDPRIORITY = 0;
    this.IDMDURGENCY = 0;
    this.IDSDCASE = 0;
    this.IDSDCASEMT = 0;
    this. IDSDCASEMTSTATUS = UsrCfg.SD.Properties.TSDCASEMTSTATUS.GetEnum(undefined); 
    this.IDSDWHOTOCASECANCELLED = 0;
    this.IDSDWHOTOCASECREATE = 0;
    this.IDSLA = 0;
    this.MT_COMMENTSM = "";
    this.CASEMT_DATELASTCUT = new Date(1970, 0, 1, 0, 0, 0);
    this.MT_GUIDETEXT = "";
    this.MT_IDMDFUNCESC = 0;
    this.MT_IDMDHIERESC = 0;
    this.MT_IDMDMODELTYPED = 0;
    this.CASEMT_LIFESTATUS = "";
    this.CASEMT_TIMERCOUNT = "";
    this.CASEMT_SET_LS_NAMESTEP = "";
    this.CASEMT_SET_LS_STATUSN = 0;
    this.CASEMT_SET_LS_NEXTSTEP = "";
    this.CASEMT_SET_LS_COMMENTSST = "";
    this.CASEMT_SET_HIERLAVEL = 0;
    this.CASEMT_SET_FUNLAVEL = 0;
    this.IDSDCASEMTASGSTATUS_HIER= UsrCfg.SD.Properties.TIDSDCASEMTASGSTATUS.GetEnum(undefined);
    this.IDSDCASEMTASGSTATUS_FUN=UsrCfg.SD.Properties.TIDSDCASEMTASGSTATUS.GetEnum(undefined);
    this.MT_IDMDSERVICETYPE = 0;
    this.MT_MAXTIME = 0;
    this.MT_NORMALTIME = 0;
    this.IDSDSCALETYPE_FUNC = UsrCfg.SD.Properties.TIDSDSCALETYPE_FUNC.GetEnum(undefined);
    this.IDSDSCALETYPE_HIER = UsrCfg.SD.Properties.TIDSDSCALETYPE_HIER.GetEnum(undefined);
    this.MT_OPEARTIONM = "";
    this.MT_POSSIBLERETURNS = "";
    this.CASEMT_COUNTTIME = 0;
    this.CASEMT_COUNTTIMEPAUSE = 0;
    this.CASEMT_COUNTTIMERESOLVED = 0;
    this.MT_TITLEM = "";
    this.CASEMT_DATEASSIGNED = new Date(1970, 0, 1, 0, 0, 0);
    this.CASEMT_DATECANCELLED = new Date(1970, 0, 1, 0, 0, 0);
    this.CASEMT_DATECREATE = new Date(1970, 0, 1, 0, 0, 0);
    this.CASEMT_DATEPROGRESS = new Date(1970, 0, 1, 0, 0, 0);
    this.MT_IDCMDBCIDEFINE = 0;
    this.MT_STEEPTYPEUSERENABLE = false;
    this.MT_STEPVALIDATE = false;
    this.MT_TIMEENABLE = false;
    this.ToByte = function(MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDCATEGORYDETAIL);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDIMPACT);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDPRIORITY);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDURGENCY);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASEMT);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASEMTSTATUS.value);                
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDWHOTOCASECANCELLED);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDWHOTOCASECREATE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSLA);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.MT_COMMENTSM);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.CASEMT_DATELASTCUT);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.MT_GUIDETEXT);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.MT_IDMDFUNCESC);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.MT_IDMDHIERESC);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.MT_IDMDMODELTYPED);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CASEMT_LIFESTATUS);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CASEMT_TIMERCOUNT);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CASEMT_SET_LS_NAMESTEP);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.CASEMT_SET_LS_STATUSN);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CASEMT_SET_LS_NEXTSTEP);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CASEMT_SET_LS_COMMENTSST);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.CASEMT_SET_HIERLAVEL);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.CASEMT_SET_FUNLAVEL);   
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASEMTASGSTATUS_HIER.value);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASEMTASGSTATUS_FUN.value);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.MT_IDMDSERVICETYPE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.MT_MAXTIME);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.MT_NORMALTIME);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDSCALETYPE_FUNC.value);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDSCALETYPE_HIER.value);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.MT_OPEARTIONM);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.MT_POSSIBLERETURNS);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.CASEMT_COUNTTIME);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.CASEMT_COUNTTIMEPAUSE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.CASEMT_COUNTTIMERESOLVED);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.MT_TITLEM);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.CASEMT_DATEASSIGNED);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.CASEMT_DATECANCELLED);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.CASEMT_DATECREATE);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.CASEMT_DATEPROGRESS);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.MT_IDCMDBCIDEFINE);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.MT_STEEPTYPEUSERENABLE);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.MT_STEPVALIDATE);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.MT_TIMEENABLE); 
    }
    this.ByteTo = function(MemStream)
    {
        this.IDMDCATEGORYDETAIL = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDIMPACT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDPRIORITY = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDURGENCY = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDCASE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDCASEMT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDCASEMTSTATUS = UsrCfg.SD.Properties.TSDCASEMTSTATUS.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));                
        this.IDSDWHOTOCASECANCELLED = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDWHOTOCASECREATE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSLA = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.MT_COMMENTSM = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CASEMT_DATELASTCUT = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.MT_GUIDETEXT = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.MT_IDMDFUNCESC = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.MT_IDMDHIERESC = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.MT_IDMDMODELTYPED = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.CASEMT_LIFESTATUS = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CASEMT_TIMERCOUNT = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CASEMT_SET_LS_NAMESTEP = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CASEMT_SET_LS_STATUSN = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.CASEMT_SET_LS_NEXTSTEP = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CASEMT_SET_LS_COMMENTSST = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CASEMT_SET_HIERLAVEL = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.CASEMT_SET_FUNLAVEL = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDCASEMTASGSTATUS_HIER = UsrCfg.SD.Properties.TIDSDCASEMTASGSTATUS.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        this.IDSDCASEMTASGSTATUS_FUN = UsrCfg.SD.Properties.TIDSDCASEMTASGSTATUS.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        this.MT_IDMDSERVICETYPE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.MT_MAXTIME = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.MT_NORMALTIME = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDSCALETYPE_FUNC = UsrCfg.SD.Properties.TIDSDSCALETYPE_FUNC.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        this.IDSDSCALETYPE_HIER = UsrCfg.SD.Properties.TIDSDSCALETYPE_HIER.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        this.MT_OPEARTIONM = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.MT_POSSIBLERETURNS = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CASEMT_COUNTTIME = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.CASEMT_COUNTTIMEPAUSE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.CASEMT_COUNTTIMERESOLVED = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.MT_TITLEM = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CASEMT_DATEASSIGNED = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.CASEMT_DATECANCELLED = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.CASEMT_DATECREATE = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.CASEMT_DATEPROGRESS = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.MT_IDCMDBCIDEFINE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.MT_STEEPTYPEUSERENABLE = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.MT_STEPVALIDATE = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.MT_TIMEENABLE = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream); 
    }
}


UsrCfg.SD.Properties.TASGNATIONFUNC = function()
{
    this.IDMDFUNCESC = 0;
    this.FUNLAVEL = 0;
    this.IDCMDBCI = 0;
    this.PERCF = 0;
    this.TOTAL = 0;
    this.ToByte = function(MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDFUNCESC);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.FUNLAVEL);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDCMDBCI);
        SysCfg.Stream.Methods.WriteStreamDouble(MemStream, this.PERCF);
        SysCfg.Stream.Methods.WriteStreamDouble(MemStream, this.TOTAL);
    }
    this.ByteTo = function(MemStream)
    {
        this.IDMDFUNCESC = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.FUNLAVEL = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDCMDBCI = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.PERCF = SysCfg.Stream.Methods.ReadStreamDouble(MemStream);
        this.TOTAL = SysCfg.Stream.Methods.ReadStreamDouble(MemStream);
    }
}

UsrCfg.SD.Properties.TASGNATIONHIER = function()
{
    this.IDMDHIERESC = 0;
    this.HIERLAVEL = 0;
    this.IDCMDBCI = 0;
    this.PERMISSIONH = UsrCfg.SD.Properties.TSDCASEPERMISSION.GetEnum(undefined);
    this.PERCH = 0;
    this.TOTAL = 0;
    this.ToByte = function(MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDHIERESC);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.HIERLAVEL);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDCMDBCI);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.PERMISSIONH.value);
        SysCfg.Stream.Methods.WriteStreamDouble(MemStream, this.PERCH);
        SysCfg.Stream.Methods.WriteStreamDouble(MemStream, this.TOTAL);
    }
    this.ByteTo = function(MemStream)
    {
        this.IDMDHIERESC = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.HIERLAVEL = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDCMDBCI = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.PERMISSIONH = UsrCfg.SD.Properties.TSDCASEPERMISSION.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        this.PERCH = SysCfg.Stream.Methods.ReadStreamDouble(MemStream);
        this.TOTAL = SysCfg.Stream.Methods.ReadStreamDouble(MemStream);
    }
}
//js interface type c# :
//https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Object/assign mdn js add functions join
//https://medium.com/@foobarjs/c-interfaces-in-javascript-1ca518933b54 mdn js inteface c#

UsrCfg.SD.Properties.TSDCASECOMPLTE/*:TSDCASE*/ = function ()
{
    //begin interface
    this.IDHANDLER = 0;
    this.IDOWNER = 0;
    this.IDMANAGERSINFORMED = 0;
    this.IDSDCASE = 0;
    this.IDSDCASE_PARENT = 0;
    this.IDSDCASESTATUS = UsrCfg.SD.Properties.TSDCaseStatus.GetEnum(undefined);
    this.IDSDRESPONSETYPE = UsrCfg.SD.Properties.TSDRESPONSETYPE.GetEnum(undefined);
    this.IDUSER = 0;
    this.CASE_DATECREATE = new Date(1970, 0, 1, 0, 0, 0);
    this.CASE_DATERESOLVED = new Date(1970, 0, 1, 0, 0, 0);
    this.CASE_DATECLOSED = new Date(1970, 0, 1, 0, 0, 0);
    this.CASE_DATELASTCUT = new Date(1970, 0, 1, 0, 0, 0);
    this.IDSDCASESOURCETYPE = UsrCfg.SD.Properties.TIDSDCASESOURCETYPE._UNKNOWN;
    this.IDCMDBCONTACTTYPE_USER = 0;
    this.IDMDCATEGORYDETAIL_INITIAL = 0;
    this.IDMDCATEGORYDETAIL_FINAL = 0;
    this.CASE_DATESTART = new Date(1970, 0, 1, 0, 0, 0);
    this.CASE_DATEPROGRESS = new Date(1970, 0, 1, 0, 0, 0);
    this.CASE_DESCRIPTION = "";
    this.CASE_FINALSUMM = "";
    this.CASE_ISMAYOR = false;
    this.CASE_COUNTTIME = 0;
    this.CASE_COUNTTIMEPAUSE = 0;
    this.CASE_COUNTTIMERESOLVED = 0;
    this.CASE_RETURN_COST = 0;
    this.CASE_RETURN_STR = "";
    this.CASE_TITLE = "";

    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDHANDLER);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDOWNER);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMANAGERSINFORMED);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASE_PARENT);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASESTATUS.value);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDRESPONSETYPE.value);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDUSER);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.CASE_DATECREATE);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.CASE_DATERESOLVED);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.CASE_DATECLOSED);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.CASE_DATELASTCUT);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASESOURCETYPE.value);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDCMDBCONTACTTYPE_USER);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDCATEGORYDETAIL_INITIAL);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDCATEGORYDETAIL_FINAL);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.CASE_DATESTART);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.CASE_DATEPROGRESS);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CASE_DESCRIPTION);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CASE_FINALSUMM);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.CASE_ISMAYOR);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.CASE_COUNTTIME);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.CASE_COUNTTIMEPAUSE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.CASE_COUNTTIMERESOLVED);
        SysCfg.Stream.Methods.WriteStreamDouble(MemStream, this.CASE_RETURN_COST);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CASE_RETURN_STR);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CASE_TITLE);
    }
    this.ByteTo = function (MemStream) {
        this.IDHANDLER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDOWNER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMANAGERSINFORMED = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDCASE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDCASE_PARENT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDCASESTATUS = UsrCfg.SD.Properties.TSDCaseStatus.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        this.IDSDRESPONSETYPE = UsrCfg.SD.Properties.TSDRESPONSETYPE.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        this.IDUSER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.CASE_DATECREATE = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.CASE_DATERESOLVED = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.CASE_DATECLOSED = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.CASE_DATELASTCUT = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.IDSDCASESOURCETYPE = UsrCfg.SD.Properties.TIDSDCASESOURCETYPE.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        this.IDCMDBCONTACTTYPE_USER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDCATEGORYDETAIL_INITIAL = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDCATEGORYDETAIL_FINAL = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.CASE_DATESTART = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.CASE_DATEPROGRESS = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.CASE_DESCRIPTION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CASE_FINALSUMM = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CASE_ISMAYOR = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.CASE_COUNTTIME = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.CASE_COUNTTIMEPAUSE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.CASE_COUNTTIMERESOLVED = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.CASE_RETURN_COST = SysCfg.Stream.Methods.ReadStreamDouble(MemStream);
        this.CASE_RETURN_STR = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CASE_TITLE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    }


    //end interface


    this.CASESTATUSNAME = "";
    this.SOURCETYPENAME = "";
    this.RESPONSETYPENAME = "";
    this.HANDLERNAME = "";
    this.OWNERNAME = "";
    this.MANAGERSINFORMEDNAME = "";
    this.USERNAME = "";
    this.IDSDCASEEF = 0;
    this.SDCASECOMPLTEToByte = function(MemStream)
    {
        this.ToByte(MemStream);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CASESTATUSNAME);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.SOURCETYPENAME);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.HANDLERNAME);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.OWNERNAME);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.MANAGERSINFORMEDNAME);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.USERNAME);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASEEF);
    }
    this.ByteToSDCASECOMPLTE = function(MemStream)
    {
        this.ByteTo(MemStream);
        this.CASESTATUSNAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.SOURCETYPENAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.HANDLERNAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.OWNERNAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.MANAGERSINFORMEDNAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.USERNAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDSDCASEEF = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    }
}
        
UsrCfg.SD.Properties.TSDWHOTOCASECOMPLTE = function()//: TSDWHOTOCASE      
{
    this.CI_GENERICNAME = "";
    this.LIST_SDOPERATION_POST = new Array();//List<TSDOPERATION_POST>
    this.LIST_SDOPERATION_FUNCESC = new Array();//List<TSDOPERATION_FUNCESC> 
    this.LIST_SDOPERATION_HIERESC = new Array();//List<TSDOPERATION_HIERESC> 
    this.LIST_SDOPERATION_PARENT = new Array();//List<TSDOPERATION_PARENT> 
    this.LIST_SDOPERATION_CASESTATUS = new Array();//List<TSDOPERATION_CASESTATUS> 
    this.LIST_SDOPERATION_CASEACTIVITIES = new Array();//List<TSDOPERATION_CASEACTIVITIES> 
    this.LIST_SDOPERATION_ATTENTION =  new Array();//List<TSDOPERATION_ATTENTION> 
    this.SDCASEACTIVITIESCOMPLTEToByte = function(MemStream)
    {
        this.ToByte(MemStream);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CI_GENERICNAME);
    }
    this.ByteToSDCASEACTIVITIESCOMPLTE = function(MemStream)
    {
        this.ByteTo(MemStream);
        this.CI_GENERICNAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream); 

    }
}


UsrCfg.SD.Properties.TSDCASEMT_LSCOMPLTE/*: TSDCASEMT_LS*/ = function ()
{
    // begin interface
    this.IDSDCASEMT = 0;
    this.IDSDCASE = 0;
    this.IDSDCASEMT_LS = 0;
    this.IDSDCASEMT_LSSTATUS = TSDCASEMT_LSSTATUS.GetEnum(undefined);
    this.IDSDCASEMT_LSTYPE = TSDCASEMT_LSTYPE.GetEnum(undefined);
    this.CASEMT_LSCOMMENTSST = "";
    this.CASEMT_LSCOUNTTIME = 0;
    this.CASEMT_LSCOUNTTIMEPAUSE = 0;
    this.CASEMT_LSCOUNTTIMERESOLVED = 0;
    this.CASEMT_LSDATELASTCUT = new Date(1970, 0, 1, 0, 0, 0);
    this.CASEMT_LSNAMESTEP = "";
    this.CASEMT_LSNEXTSTEP = "";
    this.CASEMT_LSSTATUSN = 0;

    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASEMT);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASEMT_LS);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASEMT_LSSTATUS.value);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASEMT_LSTYPE).value;
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CASEMT_LSCOMMENTSST);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.CASEMT_LSCOUNTTIME);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.CASEMT_LSCOUNTTIMEPAUSE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.CASEMT_LSCOUNTTIMERESOLVED);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.CASEMT_LSDATELASTCUT);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CASEMT_LSNAMESTEP);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CASEMT_LSNEXTSTEP);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.CASEMT_LSSTATUSN);
    }
    this.ByteTo = function (MemStream) {
        this.IDSDCASEMT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDCASEMT_LS = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDCASEMT_LSSTATUS = UsrCfg.SD.Properties.TSDCASEMT_LSSTATUS.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        this.IDSDCASEMT_LSTYPE = UsrCfg.SD.Properties.TSDCASEMT_LSTYPE.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        this.CASEMT_LSCOMMENTSST = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CASEMT_LSCOUNTTIME = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.CASEMT_LSCOUNTTIMEPAUSE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.CASEMT_LSCOUNTTIMERESOLVED = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.CASEMT_LSDATELASTCUT = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.CASEMT_LSNAMESTEP = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CASEMT_LSNEXTSTEP = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CASEMT_LSSTATUSN = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    }
    // end interface 

    this.CASEMT_LSSTATUSNAME = "";
    this.CASEMT_LSTYPENAME = "";
    this.SDCASEMT_LSCOMPLTEToByte = function(MemStream)
    {
        this.ToByte(MemStream);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CASEMT_LSSTATUSNAME);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CASEMT_LSTYPENAME); 
    }
    this.ByteToSDCASEMT_LSCOMPLTE = function(MemStream)
    {
        this.ByteTo(MemStream);
        this.CASEMT_LSSTATUSNAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CASEMT_LSTYPENAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream); 
    }

}


UsrCfg.SD.Properties.TSDCASEACTIVITIESCOMPLTE/*: TSDCASEACTIVITIES */ = function ()
{
    //begin interface
    this.COMMENTSL = "";
    this.GUIDET = "";
    this.IDMDMODELTYPED = 0;
    this.IDSDRUNNIGSOURCEMODEL = UsrCfg.SD.Properties.TSDRUNNIGSOURCEMODEL.GetEnum(undefined);
    this.IDSDRUNNIGSTATUS = UsrCfg.SD.Properties.TIDSDRUNNIGSTATUS.GetEnum(undefined);
    this.IDSDCASEACTIVITIES = 0;
    this.IDSDCASEMT_ATVPARENT = 0;
    this.IDSDCASE_ATVPARENT = 0;
    this.IDSDCASEMT;//virtual = 0;
    this.IDSDCASE = 0;
    this.LS_NAMESTEP = "";
    this.LS_STATUSN = 0;
    this.MT_RETURNS = "";
    this.ACTIVITIES_DATECANCELLED = new Date(1970, 0, 1, 0, 0, 0);
    this.ACTIVITIES_DATECLOSED = new Date(1970, 0, 1, 0, 0, 0);
    this.ACTIVITIES_DATECREATE = new Date(1970, 0, 1, 0, 0, 0);
    this.ACTIVITIES_DATERUNNING = new Date(1970, 0, 1, 0, 0, 0);
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.COMMENTSL);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.GUIDET);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDMODELTYPED);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDRUNNIGSOURCEMODEL.value);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDRUNNIGSTATUS.value);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASEACTIVITIES);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASEMT_ATVPARENT);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASE_ATVPARENT);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASEMT);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.LS_NAMESTEP);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.LS_STATUSN);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.MT_RETURNS);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.ACTIVITIES_DATECANCELLED);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.ACTIVITIES_DATECLOSED);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.ACTIVITIES_DATECREATE);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.ACTIVITIES_DATERUNNING);
    }
    this.ByteTo = function (MemStream) {
        this.COMMENTSL = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.GUIDET = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDMDMODELTYPED = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDRUNNIGSOURCEMODEL = UsrCfg.SD.Properties.TSDRUNNIGSOURCEMODEL.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        this.IDSDRUNNIGSTATUS = UsrCfg.SD.Properties.TIDSDRUNNIGSTATUS.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        this.IDSDCASEACTIVITIES = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDCASEMT_ATVPARENT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDCASE_ATVPARENT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDCASEMT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDCASE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.LS_NAMESTEP = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.LS_STATUSN = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.MT_RETURNS = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.ACTIVITIES_DATECANCELLED = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.ACTIVITIES_DATECLOSED = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.ACTIVITIES_DATECREATE = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.ACTIVITIES_DATERUNNING = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
    }
    //end interface


    this.TITLEM = "";
    this.ACTIVITIESNAME = "";
    this.SOURCEMODELNAME = ""; 
    this.SDCASEACTIVITIESCOMPLTEToByte = function(MemStream)
    {
        this.ToByte(MemStream);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.TITLEM);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.ACTIVITIESNAME);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.SOURCEMODELNAME); 


    }
    this.ByteToSDCASEACTIVITIESCOMPLTE = function(MemStream)
    {
        this.ByteTo(MemStream);
        TITLEM = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        ACTIVITIESNAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        SOURCEMODELNAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream); 

    }

}


UsrCfg.SD.Properties.TSDCASEMTCOMPLTE/*TSDCASEMT*/ = function ()
{
    //begin interface
    this.IDMDCATEGORYDETAIL = 0;
    this.IDMDIMPACT = 0;
    this.IDMDPRIORITY = 0;
    this.IDMDURGENCY = 0;
    this.IDSDCASE = 0;
    this.IDSDCASEMT = 0;
    this.IDSDCASEMTSTATUS = UsrCfg.SD.Properties.TSDCASEMTSTATUS.GetEnum(undefined);
    this.IDSDWHOTOCASECANCELLED = 0;
    this.IDSDWHOTOCASECREATE = 0;
    this.IDSLA = 0;
    this.MT_COMMENTSM = "";
    this.CASEMT_DATELASTCUT = new Date(1970, 0, 1, 0, 0, 0);
    this.MT_GUIDETEXT = "";
    this.MT_IDMDFUNCESC = 0;
    this.MT_IDMDHIERESC = 0;
    this.MT_IDMDMODELTYPED = 0;
    this.CASEMT_LIFESTATUS = "";
    this.CASEMT_TIMERCOUNT = "";
    this.CASEMT_SET_LS_NAMESTEP = "";
    this.CASEMT_SET_LS_STATUSN = 0;
    this.CASEMT_SET_LS_NEXTSTEP = "";
    this.CASEMT_SET_LS_COMMENTSST = "";
    this.CASEMT_SET_HIERLAVEL = 0;
    this.CASEMT_SET_FUNLAVEL = 0;
    this.IDSDCASEMTASGSTATUS_HIER = UsrCfg.SD.Properties.TIDSDCASEMTASGSTATUS.GetEnum(undefined);
    this.IDSDCASEMTASGSTATUS_FUN = UsrCfg.SD.Properties.TIDSDCASEMTASGSTATUS.GetEnum(undefined);
    this.MT_IDMDSERVICETYPE = 0;
    this.MT_MAXTIME = 0;
    this.MT_NORMALTIME = 0;
    this.IDSDSCALETYPE_FUNC = UsrCfg.SD.Properties.TIDSDSCALETYPE_FUNC.GetEnum(undefined);
    this.IDSDSCALETYPE_HIER = UsrCfg.SD.Properties.TIDSDSCALETYPE_HIER.GetEnum(undefined);
    this.MT_OPEARTIONM = "";
    this.MT_POSSIBLERETURNS = "";
    this.CASEMT_COUNTTIME = 0;
    this.CASEMT_COUNTTIMEPAUSE = 0;
    this.CASEMT_COUNTTIMERESOLVED = 0;
    this.MT_TITLEM = "";
    this.CASEMT_DATEASSIGNED = new Date(1970, 0, 1, 0, 0, 0);
    this.CASEMT_DATECANCELLED = new Date(1970, 0, 1, 0, 0, 0);
    this.CASEMT_DATECREATE = new Date(1970, 0, 1, 0, 0, 0);
    this.CASEMT_DATEPROGRESS = new Date(1970, 0, 1, 0, 0, 0);
    this.MT_IDCMDBCIDEFINE = 0;
    this.MT_STEEPTYPEUSERENABLE = false;
    this.MT_STEPVALIDATE = false;
    this.MT_TIMEENABLE = false;
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDCATEGORYDETAIL);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDIMPACT);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDPRIORITY);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDURGENCY);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASEMT);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASEMTSTATUS.value);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDWHOTOCASECANCELLED);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDWHOTOCASECREATE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSLA);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.MT_COMMENTSM);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.CASEMT_DATELASTCUT);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.MT_GUIDETEXT);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.MT_IDMDFUNCESC);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.MT_IDMDHIERESC);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.MT_IDMDMODELTYPED);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CASEMT_LIFESTATUS);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CASEMT_TIMERCOUNT);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CASEMT_SET_LS_NAMESTEP);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.CASEMT_SET_LS_STATUSN);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CASEMT_SET_LS_NEXTSTEP);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CASEMT_SET_LS_COMMENTSST);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.CASEMT_SET_HIERLAVEL);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.CASEMT_SET_FUNLAVEL);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASEMTASGSTATUS_HIER.value);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASEMTASGSTATUS_FUN.value);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.MT_IDMDSERVICETYPE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.MT_MAXTIME);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.MT_NORMALTIME);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDSCALETYPE_FUNC.value);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDSCALETYPE_HIER.value);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.MT_OPEARTIONM);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.MT_POSSIBLERETURNS);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.CASEMT_COUNTTIME);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.CASEMT_COUNTTIMEPAUSE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.CASEMT_COUNTTIMERESOLVED);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.MT_TITLEM);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.CASEMT_DATEASSIGNED);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.CASEMT_DATECANCELLED);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.CASEMT_DATECREATE);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.CASEMT_DATEPROGRESS);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.MT_IDCMDBCIDEFINE);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.MT_STEEPTYPEUSERENABLE);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.MT_STEPVALIDATE);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.MT_TIMEENABLE);
    }
    this.ByteTo = function (MemStream) {
        this.IDMDCATEGORYDETAIL = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDIMPACT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDPRIORITY = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDURGENCY = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDCASE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDCASEMT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDCASEMTSTATUS = UsrCfg.SD.Properties.TSDCASEMTSTATUS.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        this.IDSDWHOTOCASECANCELLED = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDWHOTOCASECREATE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSLA = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.MT_COMMENTSM = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CASEMT_DATELASTCUT = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.MT_GUIDETEXT = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.MT_IDMDFUNCESC = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.MT_IDMDHIERESC = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.MT_IDMDMODELTYPED = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.CASEMT_LIFESTATUS = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CASEMT_TIMERCOUNT = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CASEMT_SET_LS_NAMESTEP = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CASEMT_SET_LS_STATUSN = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.CASEMT_SET_LS_NEXTSTEP = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CASEMT_SET_LS_COMMENTSST = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CASEMT_SET_HIERLAVEL = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.CASEMT_SET_FUNLAVEL = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDCASEMTASGSTATUS_HIER = UsrCfg.SD.Properties.TIDSDCASEMTASGSTATUS.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        this.IDSDCASEMTASGSTATUS_FUN = UsrCfg.SD.Properties.TIDSDCASEMTASGSTATUS.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        this.MT_IDMDSERVICETYPE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.MT_MAXTIME = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.MT_NORMALTIME = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDSCALETYPE_FUNC = UsrCfg.SD.Properties.TIDSDSCALETYPE_FUNC.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        this.IDSDSCALETYPE_HIER = UsrCfg.SD.Properties.TIDSDSCALETYPE_HIER.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        this.MT_OPEARTIONM = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.MT_POSSIBLERETURNS = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CASEMT_COUNTTIME = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.CASEMT_COUNTTIMEPAUSE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.CASEMT_COUNTTIMERESOLVED = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.MT_TITLEM = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CASEMT_DATEASSIGNED = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.CASEMT_DATECANCELLED = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.CASEMT_DATECREATE = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.CASEMT_DATEPROGRESS = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.MT_IDCMDBCIDEFINE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.MT_STEEPTYPEUSERENABLE = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.MT_STEPVALIDATE = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.MT_TIMEENABLE = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
    }
    //end interface 
    this.CATEGORY = "";
    this.CATEGORYNAME = "";
    this.PRIORITYNAME = "";
    this.CASEMTSTATUSNAME = "";
    this.SLANAME = "";
    this.MT_SERVICETYPENAME = "";
    this.SDCASEMTCOMPLTEToByte = function(MemStream)
    {
        this.ToByte(MemStream);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CATEGORY);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CATEGORYNAME);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.PRIORITYNAME);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CASEMTSTATUSNAME);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.SLANAME);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.MT_SERVICETYPENAME);                
    }
    this.ByteToSDCASEMTCOMPLTE = function(MemStream)
    {
        this.ByteTo(MemStream);
        this.CATEGORY = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CATEGORYNAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.PRIORITYNAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CASEMTSTATUSNAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.SLANAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.MT_SERVICETYPENAME = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    }
}


UsrCfg.SD.Properties.TSDCONSOLEATENTION = function()
{
    this.SDCASECOMPLTE = new UsrCfg.SD.Properties.TSDCASECOMPLTE();
    this.SDCASEMTCOMPLTE = new UsrCfg.SD.Properties.TSDCASEMTCOMPLTE();
    this.SDCASEACTIVITIESCOMPLTE = new UsrCfg.SD.Properties.TSDCASEACTIVITIESCOMPLTE();
    this.SDCASEMT_LS_COUNT = 0;
    this.SDCASEMT_LSCOMPLTEList = new Array();//List<UsrCfg.SD.Properties.TSDCASEMT_LSCOMPLTE>
    this.SDConsoleAtentionToByte = function(MemStream)
    {
        this.SDCASECOMPLTE.SDCASECOMPLTEToByte(MemStream);
        this.SDCASEMTCOMPLTE.SDCASEMTCOMPLTEToByte(MemStream);
        this.SDCASEACTIVITIESCOMPLTE.SDCASEACTIVITIESCOMPLTEToByte(MemStream);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.SDCASEMT_LSCOMPLTEList.length);
        for (var i = 0; i < this.SDCASEMT_LSCOMPLTEList.length; i++)
        {
            this.SDCASEMT_LSCOMPLTEList[i].SDCASEMT_LSCOMPLTEToByte(MemStream);
        }  
    }
    this.ByteToSDConsoleAtention = function(MemStream)
    {
        this.SDCASECOMPLTE.ByteToSDCASECOMPLTE(MemStream);
        this.SDCASEMTCOMPLTE.ByteToSDCASEMTCOMPLTE(MemStream);
        this.SDCASEACTIVITIESCOMPLTE.ByteToSDCASEACTIVITIESCOMPLTE(MemStream);
        var SDCASEMT_LS_COUNT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        for (var i = 0; i < SDCASEMT_LS_COUNT; i++)
        {
            var UNSDCASEMT_LSCOMPLTE = new UsrCfg.SD.Properties.TSDCASEMT_LSCOMPLTE();
            UNSDCASEMT_LSCOMPLTE.ByteToSDCASEMT_LSCOMPLTE(MemStream);
            this.SDCASEMT_LSCOMPLTEList.push(UNSDCASEMT_LSCOMPLTE);
        }
    }
}


UsrCfg.SD.Properties.TSDCASEVERYFY = function()
{
    this.IDSDCASEMT = 0;
    this.CASEMT_SET_LS_NAMESTEP = "";
    this.CASEMT_SET_LS_STATUSN = 0;
    this.IDHANDLER = 0;
    this.IDMANAGERSINFORMED = 0;
    this.IDOWNER = 0;
    this.IDSDCASE = 0;
    this.IDSDCASE_PARENT = 0;
    this.IDSDCASEMTSTATUS = UsrCfg.SD.Properties.TSDCASEMTSTATUS.GetEnum(undefined);
    this.IDSDCASESTATUS = UsrCfg.SD.Properties.TSDCaseStatus.GetEnum(undefined);
    //NO BDD
    this.SDCASEVERYFYAction = UsrCfg.SD.Properties.TSDCASEVERYFYAction.GetEnum(undefined);
    this.Reason = "";
    this.SDWHOTOCASE = new UsrCfg.SD.Properties.TSDWHOTOCASE();                        
    this.ToByte = function(MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASEMT);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CASEMT_SET_LS_NAMESTEP);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.CASEMT_SET_LS_STATUSN);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDHANDLER);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMANAGERSINFORMED);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDOWNER);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASE_PARENT);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASESTATUS.value);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASEMTSTATUS.value);
        //NO BDD
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.SDCASEVERYFYAction.value);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.Reason);
        this.SDWHOTOCASE.ToByte(MemStream);

    }
    this.ByteTo = function(MemStream)
    {
        this.IDSDCASEMT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.CASEMT_SET_LS_NAMESTEP = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CASEMT_SET_LS_STATUSN = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDHANDLER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMANAGERSINFORMED = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDOWNER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDCASE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDCASE_PARENT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDCASESTATUS = UsrCfg.SD.Properties.TSDCaseStatus.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        this.IDSDCASEMTSTATUS = UsrCfg.SD.Properties.TSDCASEMTSTATUS.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        this.SDCASEVERYFYAction = UsrCfg.SD.Properties.TSDCASEVERYFYAction.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        this.Reason = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.SDWHOTOCASE.ByteTo(MemStream);
    }
}

UsrCfg.SD.Properties.TSDOPERATION_POST = function()
{            
    this.IDSDOPERATION_POST = 0;
    this.IDSDWHOTOCASE = 0;
    this.MESSAGE_DATE = new Date(1970, 0, 1, 0, 0, 0);
    this.POST_IDSDTYPEUSER = 0;           
    this.POST_MESSAGE = "";      
    this.ToByte = function(MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDOPERATION_POST);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDWHOTOCASE);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.MESSAGE_DATE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.POST_IDSDTYPEUSER);                
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.POST_MESSAGE);
    }
    this.ByteTo = function(MemStream)
    {
        this.IDSDOPERATION_POST = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDWHOTOCASE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.MESSAGE_DATE = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.POST_IDSDTYPEUSER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);               
        this.POST_MESSAGE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    }
} 

UsrCfg.SD.Properties.TSDOPERATION_ATTENTION = function()
{
    this.ATTENTION_DATE = new Date(1970, 0, 1, 0, 0, 0);
    this.ATTENTION_MESSAGE = "";
    this.IDSDOPERATION_ATTENTION = 0;
    this.IDSDOPERATION_ATTENTIONTYPE = UsrCfg.SD.Properties.TIDSDOPERATION_ATTENTIONTYPE.GetEnum(undefined);
    this.IDSDWHOTOCASE            = 0; 
    this.CASEMT_SET_LS_NAMESTEP = "";
    this.CASEMT_SET_LS_STATUSN    = 0;         
    this.CASEMT_SET_LS_NEXTSTEP = "";
    this.CASEMT_SET_LS_COMMENTSST = "";            
    this.CASE_RETURN_COST = 0;
    this.CASE_RETURN_STR = "";
    this.LS_IDSDCASESTATUS = UsrCfg.SD.Properties.TSDCaseStatus.GetEnum(undefined);
    this.ToByte = function(MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.ATTENTION_DATE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.ATTENTION_MESSAGE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDOPERATION_ATTENTION);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDOPERATION_ATTENTIONTYPE.value);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDWHOTOCASE);                
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CASEMT_SET_LS_NAMESTEP);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.CASEMT_SET_LS_STATUSN);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CASEMT_SET_LS_NEXTSTEP);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CASEMT_SET_LS_COMMENTSST);
        SysCfg.Stream.Methods.WriteStreamDouble(MemStream, this.CASE_RETURN_COST);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CASE_RETURN_STR);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.LS_IDSDCASESTATUS.value);

    }
    this.ByteTo = function(MemStream)
    {
        this.ATTENTION_DATE = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.ATTENTION_MESSAGE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDSDOPERATION_ATTENTION = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDOPERATION_ATTENTIONTYPE = UsrCfg.SD.Properties.TIDSDOPERATION_ATTENTIONTYPE.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        this.IDSDWHOTOCASE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);                
        this.CASEMT_SET_LS_NAMESTEP = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CASEMT_SET_LS_STATUSN = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.CASEMT_SET_LS_NEXTSTEP = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CASEMT_SET_LS_COMMENTSST = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CASE_RETURN_COST = SysCfg.Stream.Methods.ReadStreamDouble(MemStream);
        this.CASE_RETURN_STR = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.LS_IDSDCASESTATUS = UsrCfg.SD.Properties.TSDCaseStatus.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
    }           
}

UsrCfg.SD.Properties.TSDOPERATION_FUNCESC = function()
{
    this.FUNCESC_DATE = new Date(1970, 0, 1, 0, 0, 0);
    this.FUNCESC_MESSAGE = "";
    this.IDSDSCALETYPE_FUNC = UsrCfg.SD.Properties.TIDSDSCALETYPE_FUNC.GetEnum(undefined);
    this.IDSDOPERATION_FUNCESC = 0;
    this.FUNCESC_LAVEL = 0;
    this.FUNCESC_IDCMDBCI = 0;
    this.IDSDOPERATION_ESCTYPE_FUNC = UsrCfg.SD.Properties.TIDSDOPERATION_ESCTYPE.GetEnum(undefined);
    this.IDSDWHOTOCASE = 0;
    this.FUNCESC_APPLY = false;
    this.ToByte = function(MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.FUNCESC_DATE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.FUNCESC_MESSAGE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDSCALETYPE_FUNC.value);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDOPERATION_FUNCESC);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDOPERATION_ESCTYPE_FUNC.value);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.FUNCESC_LAVEL);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.FUNCESC_IDCMDBCI);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDWHOTOCASE);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.FUNCESC_APPLY);
    }
    this.ByteTo = function(MemStream)
    {
        this.FUNCESC_DATE = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.FUNCESC_MESSAGE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDSDSCALETYPE_FUNC = UsrCfg.SD.Properties.TIDSDSCALETYPE_FUNC.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        this.IDSDOPERATION_FUNCESC = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDOPERATION_ESCTYPE_FUNC = UsrCfg.SD.Properties.TIDSDOPERATION_ESCTYPE.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        this.FUNCESC_LAVEL = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.FUNCESC_IDCMDBCI = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDWHOTOCASE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.FUNCESC_APPLY = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
    }
}

UsrCfg.SD.Properties.TSDOPERATION_HIERESC = function()
{
    this.HIERESC_DATE = new Date(1970, 0, 1, 0, 0, 0);
    this.HIERESC_MESSAGE = "";
    this.IDSDSCALETYPE_HIER = UsrCfg.SD.Properties.TIDSDSCALETYPE_HIER.GetEnum(undefined);
    this.IDSDOPERATION_HIERESC = 0;
    this.HIERESC_LAVEL = 0;
    this.HIERESC_IDCMDBCI = 0;
    this.IDSDOPERATION_ESCTYPE_HIER = UsrCfg.SD.Properties.TIDSDOPERATION_ESCTYPE.GetEnum(undefined);
    this.IDSDWHOTOCASE = 0;
    this.HIERESC_APPLY = false;
    this.ToByte = function(MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.HIERESC_DATE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.HIERESC_MESSAGE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDSCALETYPE_HIER.value);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDOPERATION_HIERESC);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDOPERATION_ESCTYPE_HIER.value);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.HIERESC_LAVEL);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.HIERESC_IDCMDBCI);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDWHOTOCASE);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.HIERESC_APPLY);
    }
    this.ByteTo = function(MemStream)
    {
        this.HIERESC_DATE = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.HIERESC_MESSAGE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDSDSCALETYPE_HIER = UsrCfg.SD.Properties.TIDSDSCALETYPE_HIER.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        this.IDSDOPERATION_HIERESC = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDOPERATION_ESCTYPE_HIER = UsrCfg.SD.Properties.TIDSDOPERATION_ESCTYPE.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        this.HIERESC_LAVEL = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.HIERESC_IDCMDBCI = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDWHOTOCASE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.HIERESC_APPLY = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
    }
}

UsrCfg.SD.Properties.TSDOPERATION_PARENT = function()
{
    this.PARENT_DATE = new Date(1970, 0, 1, 0, 0, 0);
    this.PARENT_MESSAGE = "";
    this.IDSDOPERATION_PARENT = 0;
    this.IDSDOPERATION_PARENTTYPE = UsrCfg.SD.Properties.TIDSDOPERATION_PARENTTYPE.GetEnum(undefined);
    this.IDSDCASE = 0;
    this.IDSDCASE_PARENT = 0;
    this.IDSDWHOTOCASE = 0;
    this.ToByte = function(MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.PARENT_DATE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.PARENT_MESSAGE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDOPERATION_PARENT);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDOPERATION_PARENTTYPE.value);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASE_PARENT);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDWHOTOCASE);
                
    }
    this.ByteTo = function(MemStream)
    {
        this.PARENT_DATE = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.PARENT_MESSAGE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDSDOPERATION_PARENT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDOPERATION_PARENTTYPE = UsrCfg.SD.Properties.TIDSDOPERATION_PARENTTYPE.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        this.IDSDCASE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDCASE_PARENT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDWHOTOCASE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    }
}

UsrCfg.SD.Properties.TSDOPERATION_CASESTATUS = function()
{
    this.CASESTATUS_DATE = new Date(1970, 0, 1, 0, 0, 0);
    this.CASESTATUS_MESSAGE = "";
    this.IDSDOPERATION_CASESTATUS = 0;
    this.IDSDCASESTATUS=UsrCfg.SD.Properties.TSDCaseStatus.GetEnum(undefined);           
    this.IDSDWHOTOCASE = 0;
    this.CASE_RETURN_COST = 0;
    this.CASE_RETURN_STR = "";
    this.IDSDCASESETMODELTYPE = UsrCfg.SD.Properties.TIDSDCASESETMODELTYPE._None;            
    this.MT_IDMDMODELTYPED = 0;
    this.IDSDCASE = 0;
    this.IDSDCASEMT    = 0;
    this.IDMDCATEGORYDETAIL = 0;
    this.IDMDMODELTYPED = 0;
    this.IDMDPRIORITY = 0;
    this.IDSLA = 0;
    this.IDMDIMPACT = 0;
    this.IDMDURGENCY        = 0;
    this.CASE_ISMAYOR  = false;
    this.CASE_DESCRIPTION = "";
    this.CASE_TITLE = "";
    this.ToByte = function(MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.CASESTATUS_DATE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CASESTATUS_MESSAGE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDOPERATION_CASESTATUS);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASESTATUS.value);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDWHOTOCASE);
        SysCfg.Stream.Methods.WriteStreamDouble(MemStream, this.CASE_RETURN_COST);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CASE_RETURN_STR);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASESETMODELTYPE.value);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.MT_IDMDMODELTYPED);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASEMT);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDCATEGORYDETAIL);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDMODELTYPED);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDPRIORITY);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSLA);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDIMPACT);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDURGENCY);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.CASE_ISMAYOR);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CASE_DESCRIPTION);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CASE_TITLE);
    }
    this.ByteTo = function(MemStream)
    {
        this.CASESTATUS_DATE = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.CASESTATUS_MESSAGE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDSDOPERATION_CASESTATUS = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDCASESTATUS = UsrCfg.SD.Properties.TSDCaseStatus.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        this.IDSDWHOTOCASE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.CASE_RETURN_COST = SysCfg.Stream.Methods.ReadStreamDouble(MemStream);
        this.CASE_RETURN_STR = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDSDCASESETMODELTYPE =  UsrCfg.SD.Properties.TIDSDCASESETMODELTYPE.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        this.MT_IDMDMODELTYPED = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDCASE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDCASEMT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDCATEGORYDETAIL = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDMODELTYPED = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDPRIORITY = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSLA = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDIMPACT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDMDURGENCY = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.CASE_ISMAYOR = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.CASE_DESCRIPTION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CASE_TITLE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
    }
}

UsrCfg.SD.Properties.TSDOPERATION_CASEACTIVITIES = function()
{

    this.CASEACTIVITIES_DATE = new Date(1970, 0, 1, 0, 0, 0);
    this.CASEACTIVITIES_MESSAGE = "";
    this.IDSDOPERATION_CASEACTIVITIES = 0;
    this.IDSDRUNNIGSTATUS = UsrCfg.SD.Properties.TIDSDRUNNIGSTATUS.GetEnum(undefined);
    this.IDSDWHOTOCASE = 0;
    this.ToByte = function(MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.CASEACTIVITIES_DATE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CASEACTIVITIES_MESSAGE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDOPERATION_CASEACTIVITIES);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDRUNNIGSTATUS.value);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDWHOTOCASE);
    }
    this.ByteTo = function(MemStream)
    {
        this.CASEACTIVITIES_DATE = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.CASEACTIVITIES_MESSAGE = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDSDOPERATION_CASEACTIVITIES = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDRUNNIGSTATUS = UsrCfg.SD.Properties.TIDSDRUNNIGSTATUS.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        this.IDSDWHOTOCASE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    }
}


UsrCfg.SD.Properties.TSDCASEACTIVITIES = function()
{
    this.COMMENTSL = "";
    this.GUIDET = "";
    this.IDMDMODELTYPED = 0;
    this.IDSDRUNNIGSOURCEMODEL = UsrCfg.SD.Properties.TSDRUNNIGSOURCEMODEL.GetEnum(undefined);
    this.IDSDRUNNIGSTATUS = UsrCfg.SD.Properties.TIDSDRUNNIGSTATUS.GetEnum(undefined);
    this.IDSDCASEACTIVITIES = 0;
    this.IDSDCASEMT_ATVPARENT = 0;
    this.IDSDCASE_ATVPARENT = 0;
    this.IDSDCASEMT;//virtual = 0;
    this.IDSDCASE = 0;
    this.LS_NAMESTEP = "";
    this.LS_STATUSN = 0;
    this.MT_RETURNS = "";
    this.ACTIVITIES_DATECANCELLED = new Date(1970, 0, 1, 0, 0, 0);
    this.ACTIVITIES_DATECLOSED = new Date(1970, 0, 1, 0, 0, 0);
    this.ACTIVITIES_DATECREATE = new Date(1970, 0, 1, 0, 0, 0);
    this.ACTIVITIES_DATERUNNING  = new Date(1970, 0, 1, 0, 0, 0);
    this.ToByte = function(MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.COMMENTSL);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.GUIDET);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDMDMODELTYPED);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDRUNNIGSOURCEMODEL.value);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDRUNNIGSTATUS.value);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASEACTIVITIES);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASEMT_ATVPARENT);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASE_ATVPARENT);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASEMT);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASE);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.LS_NAMESTEP);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.LS_STATUSN);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.MT_RETURNS);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.ACTIVITIES_DATECANCELLED);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.ACTIVITIES_DATECLOSED);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.ACTIVITIES_DATECREATE);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.ACTIVITIES_DATERUNNING); 
    }
    this.ByteTo = function(MemStream)
    {
        this.COMMENTSL = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.GUIDET = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDMDMODELTYPED = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDRUNNIGSOURCEMODEL = UsrCfg.SD.Properties.TSDRUNNIGSOURCEMODEL.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        this.IDSDRUNNIGSTATUS = UsrCfg.SD.Properties.TIDSDRUNNIGSTATUS.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        this.IDSDCASEACTIVITIES = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDCASEMT_ATVPARENT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDCASE_ATVPARENT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDCASEMT = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDCASE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.LS_NAMESTEP = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.LS_STATUSN = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.MT_RETURNS = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.ACTIVITIES_DATECANCELLED = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.ACTIVITIES_DATECLOSED = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.ACTIVITIES_DATECREATE = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.ACTIVITIES_DATERUNNING = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream); 
    }
}

UsrCfg.SD.Properties.TSDNOTIFY = function()//_XYZ
{
    this.ResErr = new SysCfg.Error.Properties.TResErr();
    this.ParamStr = "";
    this.ParamSql = new Array();
    this.CHANGE_DESCRIPTION = "";
    this.CHANGE_EMAILSQL = "";
    this.IDCMDBCI = 0;
    this.IDSDTYPEUSER = UsrCfg.SD.Properties.TSDTypeUser.GetEnum(undefined);
    this.ISSENDCONSOLE = false;
    this.ISSENDEMAIL  = false;
    this.EVENTDATE = new Date(1970, 0, 1, 0, 0, 0);
    this.IDSDCASE = 0;
    this.IDSDNOTIFY = 0;
    this.IDSDNOTIFYTEMPLATE = 0;

    this.SDNOTIFYToByte = function(MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.ParamStr);
        SysCfg.Stream.Methods.WriteStreamByteInt32(MemStream, this.ParamSql);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CHANGE_DESCRIPTION);
        SysCfg.Stream.Methods.WriteStreamStrInt16(MemStream, this.CHANGE_EMAILSQL);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDCMDBCI); 
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDTYPEUSER.value);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.ISSENDCONSOLE);
        SysCfg.Stream.Methods.WriteStreamBoolean(MemStream, this.ISSENDEMAIL); 
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.EVENTDATE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDCASE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDNOTIFY);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDNOTIFYTEMPLATE);
    }
    this.ByteToSDNOTIFY = function(MemStream)
    {
        this.ParamStr = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.ParamSql = SysCfg.Stream.Methods.ReadStreamByteInt32(MemStream);
        this.CHANGE_DESCRIPTION = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.CHANGE_EMAILSQL = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
        this.IDCMDBCI = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDTYPEUSER = UsrCfg.SD.Properties.TSDTypeUser.GetEnum(SysCfg.Stream.Methods.ReadStreamInt32(MemStream));
        this.ISSENDCONSOLE = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
        this.ISSENDEMAIL = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream); 
        this.EVENTDATE = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.IDSDCASE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDNOTIFY = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.IDSDNOTIFYTEMPLATE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    }

} 


