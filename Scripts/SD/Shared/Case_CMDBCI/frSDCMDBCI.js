﻿ItHelpCenter.SD.Shared.Case_CMDBCI.TfrSDCMDBCI = function (inMenuObject, inObject, inMe, incallback) {
    
    this.TParent = function () {
        return this;
    }.bind(this);

    var _this = this.TParent();
    this.ObjectHtml = inObject;

    this.CallbackModalResult = null;
    this.CallbackLoad = incallback;
    this.DBTranslate = null; 
    this.Mythis = "TfrSDCMDBCI";
    this.ID = this.Mythis + (Math.floor(Math.random() * 1000));
    this.MenuObject = inMenuObject;
    this.THIS = inMe;

    this.CMDBUSER_NAME = "";
    this.IDSDCASE_CMDB_ASSET = 0;
    this.IDCMDBCI_AFFECTED = 0;
  
    this.IDSDCASE = 0;
    this.inDESCRIPCION = null;
    
    this.IDUSER = 0;
    this.IDSDTYPEUSER = 0;
    this.RowSelected = null;

    //TRADUCCION   
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lbl001", "Edit");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lbl002", "Delete");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lbl003", "Add");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lbl004", "View");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1", "Edit");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2", "Description");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3", "Are you sure you want to delete the record from the database?");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines4", "Select a record");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines5", "CI Affected");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines6", "Advance Search");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnOK", "Accept");

    _this.InitializeComponent();
} 

ItHelpCenter.SD.Shared.Case_CMDBCI.TfrSDCMDBCI.prototype.InitializeComponent = function () {
    var _this = this.TParent();
    _this.ObjectHtml.innerHTML = "";
    $(_this.ObjectHtml).css("padding-top", "0px");

    if (SysCfg.App.Properties.Device.value != SysCfg.App.TDevice.Desktop.value) {
        $(_this.ObjectHtml).css("padding","0px");
    }


    //ESTRUCTURA DIVS
    var CDivChild = new TVclStackPanel(_this.ObjectHtml, "DivChild" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
    _this.DivChild = CDivChild.Column[0].This;

    _this.DivCont = new TVclStackPanel(_this.ObjectHtml, "TfrSDCMDBCIContainer" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
    _this.DivContainer = _this.DivCont.Column[0].This;
    $(_this.DivCont.Row.This).addClass("TfrSDCMDBCIContainer");

    var DivMain = new TVclStackPanel(_this.DivContainer, "DivMain" + _this.ID, 2, [[12, 12], [7, 5], [7, 5], [8, 4], [9, 3]]);
    var Div1C = DivMain.Column[0].This;
    var Div2C = DivMain.Column[1].This;  

    var DivBotonera = Div2C; //_this.MenuObject.GetDivData(Div2C, Div1C);

    var Div1Cont = new TVclStackPanel(Div1C, "Div1Cont" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
    var Div1 = Div1Cont.Column[0].This;  
    $(Div1).css("min-height","250px");
    _this.divGrid = Div1;
    if (SysCfg.App.Properties.Device.value == SysCfg.App.TDevice.Desktop.value) {
        $(_this.divGrid).addClass("StackPanelContainer");
        $(Div1Cont.Row.This).css("padding", "10px");
    } else {
        $(Div1Cont.Row.This).css("padding", "0px");
    }

    var Div2Cont = new TVclStackPanel(DivBotonera, "Div2Cont" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
    var Div2 = Div2Cont.Column[0].This;
    $(Div2Cont.Row.This).css("padding", "10px");
    if (SysCfg.App.Properties.Device.value == SysCfg.App.TDevice.Desktop.value) {        
        $(Div2).addClass("StackPanelContainer");
        $(Div2).css("padding-bottom", "10px");
    } else {
        $(Div2).css("padding-bottom", "0px");
    }

    var Div2_1 = new TVclStackPanel(Div2, "Div2_1" + _this.ID, 2, [[8, 4], [8, 4], [8, 4], [8, 4], [9, 3]]);
    var Div2_1C1 = Div2_1.Column[0].This;
    var Div2_1C2 = Div2_1.Column[1].This;

    var Div2_2 = new TVclStackPanel(Div2, "Div2_2" + _this.ID, 2, [[8, 4], [8, 4], [8, 4], [8, 4], [9, 3]]);
    var Div2_2C1 = Div2_2.Column[0].This;
    var Div2_2C2 = Div2_2.Column[1].This;

    var Div2_3 = new TVclStackPanel(Div2, "Div2_3" + _this.ID, 2, [[8, 4], [8, 4], [8, 4], [8, 4], [9, 3]]);
    var Div2_3C1 = Div2_3.Column[0].This;
    var Div2_3C2 = Div2_3.Column[1].This;

    var Div2_4 = new TVclStackPanel(Div2, "Div2_4" + _this.ID, 2, [[8, 4], [8, 4], [8, 4], [8, 4], [9, 3]]);
    var Div2_4C1 = Div2_4.Column[0].This;
    var Div2_4C2 = Div2_4.Column[1].This;

    var Div3Cont = new TVclStackPanel(_this.DivContainer, "Div3Cont" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
    var Div3C = Div3Cont.Column[0].This;


    //ELEMENTS
    var lbl001 = new Vcllabel(Div2_1C1, "lbl001" + _this.ID, TVCLType.BS, TlabelType.H0, "");
    lbl001.innerText = UsrCfg.Traslate.GetLangText(_this.Mythis, "lbl001");

    _this.btnEditarCI = new TVclButton(Div2_1C2, "btnEditarCI" + _this.ID);
    _this.btnEditarCI.VCLType = TVCLType.BS;
    _this.btnEditarCI.Src = "image/24/Item-configuration1.png";
    _this.btnEditarCI.This.style.minWidth = "100%";
    _this.btnEditarCI.This.classList.add("btn-xs");
    _this.btnEditarCI.onClick = function myfunction() {
        _this.btnEditarCI_Click(_this, _this.btnEditarCI);
    }

    var lbl002 = new Vcllabel(Div2_2C1, "lbl002" + _this.ID, TVCLType.BS, TlabelType.H0, "");
    lbl002.innerText = UsrCfg.Traslate.GetLangText(_this.Mythis, "lbl002");

    _this.btnEliminarCI = new TVclButton(Div2_2C2, "btn_Template" + _this.ID);
    _this.btnEliminarCI.VCLType = TVCLType.BS;
    _this.btnEliminarCI.Src = "image/24/Package-delete.png";
    _this.btnEliminarCI.This.style.minWidth = "100%";
    _this.btnEliminarCI.This.classList.add("btn-xs");
    _this.btnEliminarCI.onClick = function myfunction() {
        _this.btnEliminarCI_Click(_this, _this.btnEliminarCI);
    }

    var lbl003 = new Vcllabel(Div2_3C1, "lbl003" + _this.ID, TVCLType.BS, TlabelType.H0, "");
    lbl003.innerText = UsrCfg.Traslate.GetLangText(_this.Mythis, "lbl003");

    _this.btnAddCI = new TVclButton(Div2_3C2, "btnAddCI" + _this.ID);
    _this.btnAddCI.VCLType = TVCLType.BS;
    _this.btnAddCI.Src = "image/24/Package-add.png";
    _this.btnAddCI.This.style.minWidth = "100%";
    _this.btnAddCI.This.classList.add("btn-xs");
    _this.btnAddCI.onClick = function myfunction() {
        _this.btnAddCI_Click(_this, _this.btnAddCI);
    }

    if (!UsrCfg.Version.IsProduction) {
        var lbl004 = new Vcllabel(Div2_4C1, "lbl004" + _this.ID, TVCLType.BS, TlabelType.H0, "");
        lbl004.innerText = UsrCfg.Traslate.GetLangText(_this.Mythis, "lbl004");

        _this.btnFunction = new TVclButton(Div2_4C2, "btn_Template" + _this.ID);
        _this.btnFunction.VCLType = TVCLType.BS;
        _this.btnFunction.Src = "image/24/search-good.png";
        _this.btnFunction.This.style.minWidth = "100%";
        _this.btnFunction.This.classList.add("btn-xs");
        _this.btnFunction.onClick = function myfunction() {
            _this.btnFunction_Click(_this, _this.btnFunction);
        }
    }  

    _this.btnOk = new TVclButton(Div3C, "btnOk" + _this.ID);
    _this.btnOk.VCLType = TVCLType.BS;
    _this.btnOk.Src = "image/24/accept24.png"
    _this.btnOk.Cursor = "pointer";
    _this.btnOk.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnOK");
    _this.btnOk.onClick = function () {
        _this.btnOk_Click(_this, _this.btnOk);
    }
    $(_this.btnOk.This).css("float", "right");
    $(_this.btnOk.This).css("margin", "10px");
    $(_this.btnOk.This).css("cursor", "pointer");

    _this.CallbackLoad(_this);
      
}

ItHelpCenter.SD.Shared.Case_CMDBCI.TfrSDCMDBCI.prototype.Initialize = function (inIDSDCASE, inIDUSER, inIDSDTYPEUSER, inCALLBACK) {
    
    var _this = this.TParent();

    _this.CallbackModalResult = inCALLBACK;

    _this.IDSDCASE = inIDSDCASE;
    _this.IDUSER = inIDUSER;
    _this.IDSDTYPEUSER = inIDSDTYPEUSER;

    _this.CargarDatosGridCIAfectado();
}

ItHelpCenter.SD.Shared.Case_CMDBCI.TfrSDCMDBCI.prototype.CargarDatosGridCIAfectado = function () {
    var _this = this.TParent();
 
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.IDSDCASE.FieldName, _this.IDSDCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    var SQLBuild = SysCfg.DB.SQL.Methods.GetSQLPrefixIDBuild("Atis", "SDTCA_002", new Array());
    _this.OpenDataSet = SysCfg.DB.SQL.Methods.OpenDataSet(SQLBuild.StrSQL, Param.ToBytes());
   
    ResErr = _this.OpenDataSet.ResErr;
    if (ResErr.NotError) {
        if (_this.OpenDataSet.DataSet.FieldDefs.length > 0) {
            _this.divGrid.innerHTML = "";
            var Grilla = new TGrid(_this.divGrid, "", ""); //CREA UN GRID CONTRO
            for (var i = 0; i < _this.OpenDataSet.DataSet.FieldCount; i++) {
                var Col0 = new TColumn(); //CREA UNA COLUMNA PARA AGREGAR AL GRID CONTROL
                Col0.Name = _this.OpenDataSet.DataSet.FieldDefs[i].FieldName;
                Col0.Caption = _this.OpenDataSet.DataSet.FieldDefs[i].FieldName;
                Col0.Index = i;
                Col0.EnabledEditor = false; //ASIGNA O EXTRAE UN VALO TRUE O FALSE QUE INDICA SI LA COLUMNA MOSTRARA UN EDITOR DE DATOS             
                Grilla.AddColumn(Col0); //AGREGA UNA COLUMNA AL GRIDCONTROL 
            }

            for (var i = 0; i < _this.OpenDataSet.DataSet.RecordCount; i++) {//rows
                var NewRow = new TRow(); //CREA UN ROW PARA AGREGAR AL GRIDCONTROL  
                for (var j = 0; j < _this.OpenDataSet.DataSet.FieldCount; j++) {//columns
                    var Cell = new TCell();
                    Cell.Value = _this.OpenDataSet.DataSet.Records[i].Fields[j].Value;
                    Cell.IndexColumn = j;
                    Cell.IndexRow = i;
                    NewRow.AddCell(Cell);
                }
                Grilla.AddRow(NewRow);
            }


            Grilla.OnRowClick = function (sender, EventArgs) {
                _this.RowSelected = EventArgs;
            }
        }
    }
}

ItHelpCenter.SD.Shared.Case_CMDBCI.TfrSDCMDBCI.prototype.btnEditarCI_Click = function (sender, e) {
    _this = sender;
    if (_this.RowSelected != null) {
        $(_this.DivCont.Row.This).css("display", "none");
        $(_this.DivChild).css("display", "");
        _this.DivChild.innerHTML = "";
        _this.THIS.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2"));
        _this.TfrAdvMsgEdit = new ItHelpCenter.SD.frAdvMsgEdit(_this.DivChild, function (sender) {
             
            var inTITLE = _this.RowSelected.Cells[0].Value;
            var inDESCRIPCION = _this.RowSelected.Cells[7].Value;

            sender.Inicialize(inTITLE, inDESCRIPCION, function (outRest, outText, sender) {
                    var _this = sender;
                    $(_this.DivCont.Row.This).css("display", "");
                    _this.DivChild.innerHTML = "";
                    $(_this.DivChild).css("display", "none");
                    _this.THIS.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines5"));
                    if (outRest) {
                        _this.TfrAdvMsgEdit_CierraVentanaDescripcion(outText);
                    }
                });      
        }, _this);
    } else {
        alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines4"));
    }
}

ItHelpCenter.SD.Shared.Case_CMDBCI.TfrSDCMDBCI.prototype.btnEliminarCI_Click = function (sender, e) {
    _this = sender;

    var DialogResult = confirm(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3"));
    if (DialogResult == true) {         
        if (_this.RowSelected  != null)
        {
            var Param = new SysCfg.Stream.Properties.TParam();
            try
            {
                var IDSDCASE_CMDB_ASSET = _this.RowSelected.Cells[4].Value; 
                Param.Inicialize();
                Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.IDSDCASE_CMDB_ASSET.FieldName, IDSDCASE_CMDB_ASSET, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                var Open = SysCfg.DB.SQL.Methods.Open("Atis", "SDTCA_004", Param.ToBytes());
                if (Open.ResErr.NotError)
                {
                    _this.CargarDatosGridCIAfectado();
                }
                else
                {
                   //debugger;
                }
            }
            finally
            {
                Param.Destroy();
            }
        }
        else
        {
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines4"));
        }
    }
}

ItHelpCenter.SD.Shared.Case_CMDBCI.TfrSDCMDBCI.prototype.btnAddCI_Click = function (sender, e) {
    _this = sender;  
  
    $(_this.DivCont.Row.This).css("display", "none");
    $(_this.DivChild).css("display", "");
    _this.DivChild.innerHTML = "";
    _this.THIS.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines6"));

    try {
        var query = SysCfg.DB.SQL.Methods.GetSQLPrefixIDBuild("Atis", "SDCMDBCI_SEARCHCI", new Array()).StrSQL;
        var objAdvSearchManager = new ItHelpCenter.SD.AdvSearch.TAdvSearchManager(_this.DivChild, function (sender2) {
            var _this2 = sender2;
            _this2.Inicialize("COD_01", query, function (outRest, sender3) {
                var _this3 = sender3;
                $(_this3.DivCont.Row.This).css("display", "");
                _this3.DivChild.innerHTML = "";
                $(_this3.DivChild).css("display", "none");

                _this3.THIS.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines5"));
                _this3.TAdvSearchManagerRow = outRest;
                if (_this3.TAdvSearchManagerRow != null) {
                    _this3.objAdvSearchManager_GuardarDatosBDSDCASE_CMDB_ASSET();
                } 
            });
        }, _this);
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("frSDCMDBCI.js ItHelpCenter.SD.Shared.Case_CMDBCI.TfrSDCMDBCI.prototype.btnAddCI_Click", e);
       
    }
 
}

ItHelpCenter.SD.Shared.Case_CMDBCI.TfrSDCMDBCI.prototype.btnFunction_Click = function (sender, e) {
    _this = sender;

    if (_this.RowSelected != null) {
        var IDCI = _this.RowSelected.Cells[6].Value;
        ////PENDIENTE CI CARLOS////////////////////////////////////////

        $(_this.DivCont.Row.This).css("display", "none");
        $(_this.DivChild).css("display", "");
        _this.DivChild.innerHTML = "";

        var CIEditor = new ItHelpCenter.CIEditor.TCIEditor(_this.DivChild, "1");
        CIEditor.ContCIEditor.Row.This.style.marginLeft = "10px";
        CIEditor.OnCancel = function (sender) {
            $(_this.DivCont.Row.This).css("display", "");
            $(_this.DivChild).css("display", "none");
        }
        CIEditor.setCI(IDCI);
    }
    else {
        alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines4"));
    }


}

ItHelpCenter.SD.Shared.Case_CMDBCI.TfrSDCMDBCI.prototype.btnOk_Click = function (sender, e) {
    _this = sender;
    _this.THIS.CloseModal();
}

ItHelpCenter.SD.Shared.Case_CMDBCI.TfrSDCMDBCI.prototype.objAdvSearchManager_GuardarDatosBDSDCASE_CMDB_ASSET = function () {
    var _this = this.TParent();   

   var Temp_IDCMDBCI_AFFECTED = _this.TAdvSearchManagerRow.Cells[0].Value; 
   var  DESCRIPTION = _this.TAdvSearchManagerRow.Cells[5].Value;

    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.IDSDCASE.FieldName, _this.IDSDCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.IDCMDBCI_AFFECTED.FieldName, Temp_IDCMDBCI_AFFECTED, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    Param.AddString(UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.DESCRIPTION.FieldName, DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    Param.AddDateTime(UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.CMDB_ASSETDATE.FieldName, SysCfg.DB.Properties.SVRNOW(), SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.IDCMDBCI.FieldName, _this.IDUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.IDSDTYPEUSER.FieldName, _this.IDSDTYPEUSER.value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.IDSYSTEMSTATUS.FieldName, 1, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "SDTCA_001", Param.ToBytes());
    if (!Exec.ResErr.NotError)
    {
        ShowMessage.Message.ErrorMessage(Exec.ResErr.Mesaje);
    }

    _this.CargarDatosGridCIAfectado();
    Param.Destroy();

}

ItHelpCenter.SD.Shared.Case_CMDBCI.TfrSDCMDBCI.prototype.TfrAdvMsgEdit_CierraVentanaDescripcion = function (inTxtDescripcion) {
    var _this = this.TParent();

    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();

    var IDSDCASE_CMDB_ASSET = _this.RowSelected.Cells[4].Value;
    Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.IDSDCASE_CMDB_ASSET.FieldName, IDSDCASE_CMDB_ASSET, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    Param.AddText(UsrCfg.InternoAtisNames.SDCASE_CMDB_ASSET.DESCRIPTION.FieldName, inTxtDescripcion, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    var Open = SysCfg.DB.SQL.Methods.Open("Atis", "SDTCA_003", Param.ToBytes());
    if (Open.ResErr.NotError)
    {
        _this.CargarDatosGridCIAfectado();
    }
    else
    {
        ShowMessage.Message.ErrorMessage(Open.ResErr.Mesaje);
    }
    Param.Destroy();
}
