﻿
UsrCfg.SD.TYPEUSER_RWX.Properties._Version = "001";
UsrCfg.SD.TYPEUSER_RWX.Properties.TSDTYPEUSER_RWX = function () {
    this.IDSDTYPEUSER = 0;
    this.R_READ = 0;
    this.W_READ = 0;

    //Socket IO Properties
    this.ToByte = function (MemStream) {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDSDTYPEUSER);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.R_READ);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.W_READ);

    }
    this.ByteTo = function (MemStream) {
        this.IDSDTYPEUSER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.R_READ = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.W_READ = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);

    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto) {
        SysCfg.Str.Protocol.WriteInt(this.IDSDTYPEUSER, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.R_READ, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.W_READ, Longitud, Texto);

    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto) {
        this.IDSDTYPEUSER = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.R_READ = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.W_READ = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);

    }
}

// SDTYPEUSER_RWX
//DataSet Function 
//List Function 
UsrCfg.SD.TYPEUSER_RWX.Methods.SDTYPEUSER_RWX_ListSetID = function (SDTYPEUSER_RWXList, IDSDTYPEUSER) {
    for (var i = 0; i < SDTYPEUSER_RWXList.length; i++) {

        if (IDSDTYPEUSER == SDTYPEUSER_RWXList[i].IDSDTYPEUSER)
            return (SDTYPEUSER_RWXList[i]);

    }

    var SDTYPEUSER_RWX = new UsrCfg.SD.TYPEUSER_RWX.Properties.TSDTYPEUSER_RWX();
    SDTYPEUSER_RWX.IDSDTYPEUSER = IDSDTYPEUSER;
    return (SDTYPEUSER_RWX);
}

UsrCfg.SD.TYPEUSER_RWX.Methods.SDTYPEUSER_RWX_ListAdd = function (SDTYPEUSER_RWXList, SDTYPEUSER_RWX) {
    var i = UsrCfg.SD.TYPEUSER_RWX.Methods.SDTYPEUSER_RWX_ListGetIndex(SDTYPEUSER_RWXList, SDTYPEUSER_RWX.IDSDTYPEUSER);
    if (i == -1) SDTYPEUSER_RWXList.push(SDTYPEUSER_RWX);
    else {
        SDTYPEUSER_RWXList[i].IDSDTYPEUSER = SDTYPEUSER_RWX.IDSDTYPEUSER;
        SDTYPEUSER_RWXList[i].R_READ = SDTYPEUSER_RWX.R_READ;
        SDTYPEUSER_RWXList[i].W_READ = SDTYPEUSER_RWX.W_READ;

    }
}
UsrCfg.SD.TYPEUSER_RWX.Methods.SDTYPEUSER_RWX_ListGetIndex = function (SDTYPEUSER_RWXList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = UsrCfg.SD.TYPEUSER_RWX.Methods.SDTYPEUSER_RWX_ListGetIndexIDSDTYPEUSER(SDTYPEUSER_RWXList, Param);
    }
    else {
        Res = UsrCfg.SD.TYPEUSER_RWX.Methods.SDTYPEUSER_RWX_ListGetIndexSDTYPEUSER_RWX(SDTYPEUSER_RWXList, Param);
    }
    return (Res);
}

//estructura
UsrCfg.SD.TYPEUSER_RWX.Methods.SDTYPEUSER_RWX_ListGetIndexSDTYPEUSER_RWX = function (SDTYPEUSER_RWXList, SDTYPEUSER_RWX) {
    for (var i = 0; i < SDTYPEUSER_RWXList.length; i++) {
        if (SDTYPEUSER_RWX.IDSDTYPEUSER == SDTYPEUSER_RWXList[i].IDSDTYPEUSER)
            return (i);
    }
    return (-1);
}

//number
UsrCfg.SD.TYPEUSER_RWX.Methods.SDTYPEUSER_RWX_ListGetIndexIDSDTYPEUSER = function (SDTYPEUSER_RWXList, IDSDTYPEUSER) {
    for (var i = 0; i < SDTYPEUSER_RWXList.length; i++) {
        if (IDSDTYPEUSER == SDTYPEUSER_RWXList[i].IDSDTYPEUSER)
            return (i);
    }
    return (-1);
}

//String Function 
UsrCfg.SD.TYPEUSER_RWX.Methods.SDTYPEUSER_RWXListtoStr = function (SDTYPEUSER_RWXList) {

    var Res = UsrCfg.SD.TYPEUSER_RWX.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(SDTYPEUSER_RWXList.length, Longitud, Texto);
        for (Counter = 0; Counter < SDTYPEUSER_RWXList.length; Counter++) {
            var UnSDTYPEUSER_RWX = SDTYPEUSER_RWXList[Counter];
            UnSDTYPEUSER_RWX.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = UsrCfg.SD.TYPEUSER_RWX.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
}

UsrCfg.SD.TYPEUSER_RWX.Methods.StrtoSDTYPEUSER_RWX = function (ProtocoloStr, SDTYPEUSER_RWXList) {

    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        SDTYPEUSER_RWXList.length = 0;
        if (UsrCfg.SD.TYPEUSER_RWX.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++) {
                UnSDTYPEUSER_RWX = new UsrCfg.SD.TYPEUSER_RWX.Properties.TSDTYPEUSER_RWX(); //Mode new row
                UnSDTYPEUSER_RWX.StrTo(Pos, Index, LongitudArray, Texto);
                UsrCfg.SD.TYPEUSER_RWX.Methods.SDTYPEUSER_RWX_ListAdd(SDTYPEUSER_RWXList, UnSDTYPEUSER_RWX);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);

}

UsrCfg.SD.TYPEUSER_RWX.Methods.GetSDTYPEUSER_RWXListDefult =  function(IDTypeUser,SDTYPEUSER_RWXList,IDSDTYPEUSERList){
    
    
    var TSDTYPEUSER_RWX = new UsrCfg.SD.TYPEUSER_RWX.Properties.TSDTYPEUSER_RWX();
    TSDTYPEUSER_RWX.IDSDTYPEUSER = UsrCfg.SD.Properties.TSDTypeUser._Owner.value;
    TSDTYPEUSER_RWX.R_READ = 1;
    TSDTYPEUSER_RWX.W_READ = 1;
    var Exist = UsrCfg.SD.TYPEUSER_RWX.Methods.SDTYPEUSER_RWX_ListGetIndex(SDTYPEUSER_RWXList, TSDTYPEUSER_RWX);
    if (Exist == -1) SDTYPEUSER_RWXList.push(TSDTYPEUSER_RWX);
    
    var TSDTYPEUSER_RWX = new UsrCfg.SD.TYPEUSER_RWX.Properties.TSDTYPEUSER_RWX();
    TSDTYPEUSER_RWX.IDSDTYPEUSER = UsrCfg.SD.Properties.TSDTypeUser._Handler.value
    TSDTYPEUSER_RWX.R_READ = 1;
    TSDTYPEUSER_RWX.W_READ = 1;
    var Exist = UsrCfg.SD.TYPEUSER_RWX.Methods.SDTYPEUSER_RWX_ListGetIndex(SDTYPEUSER_RWXList, TSDTYPEUSER_RWX);
    if (Exist == -1) SDTYPEUSER_RWXList.push(TSDTYPEUSER_RWX);

    var TSDTYPEUSER_RWX = new UsrCfg.SD.TYPEUSER_RWX.Properties.TSDTYPEUSER_RWX();
    TSDTYPEUSER_RWX.IDSDTYPEUSER = UsrCfg.SD.Properties.TSDTypeUser._ManagersInformed.value;
    TSDTYPEUSER_RWX.R_READ = 1;
    TSDTYPEUSER_RWX.W_READ = 1;
    var Exist = UsrCfg.SD.TYPEUSER_RWX.Methods.SDTYPEUSER_RWX_ListGetIndex(SDTYPEUSER_RWXList, TSDTYPEUSER_RWX);
    if (Exist == -1) SDTYPEUSER_RWXList.push(TSDTYPEUSER_RWX);

    
    if (IDTypeUser != 4)
    {
        var TSDTYPEUSER_RWX = new UsrCfg.SD.TYPEUSER_RWX.Properties.TSDTYPEUSER_RWX();
        TSDTYPEUSER_RWX.IDSDTYPEUSER = UsrCfg.SD.Properties.TSDTypeUser._User.value;
        TSDTYPEUSER_RWX.R_READ = 0;
        TSDTYPEUSER_RWX.W_READ = 0;
        var Exist = UsrCfg.SD.TYPEUSER_RWX.Methods.SDTYPEUSER_RWX_ListGetIndex(SDTYPEUSER_RWXList, TSDTYPEUSER_RWX);
        if (Exist == -1) SDTYPEUSER_RWXList.push(TSDTYPEUSER_RWX);

    }
    else
    {
        var TSDTYPEUSER_RWX = new UsrCfg.SD.TYPEUSER_RWX.Properties.TSDTYPEUSER_RWX();
        TSDTYPEUSER_RWX.IDSDTYPEUSER = UsrCfg.SD.Properties.TSDTypeUser._User.value;
        TSDTYPEUSER_RWX.R_READ = 1;
        TSDTYPEUSER_RWX.W_READ = 1;
        var Exist = UsrCfg.SD.TYPEUSER_RWX.Methods.SDTYPEUSER_RWX_ListGetIndex(SDTYPEUSER_RWXList, TSDTYPEUSER_RWX);
        if (Exist == -1) SDTYPEUSER_RWXList.push(TSDTYPEUSER_RWX);

    }

    for (var i = 0; i < IDSDTYPEUSERList.length; i++) {
        var TSDTYPEUSER_RWX = new UsrCfg.SD.TYPEUSER_RWX.Properties.TSDTYPEUSER_RWX();
        TSDTYPEUSER_RWX.IDSDTYPEUSER = IDSDTYPEUSERList[i];
        TSDTYPEUSER_RWX.R_READ = 1;
        TSDTYPEUSER_RWX.W_READ = 1;       

        var Exist = UsrCfg.SD.TYPEUSER_RWX.Methods.SDTYPEUSER_RWX_ListGetIndex(SDTYPEUSER_RWXList, TSDTYPEUSER_RWX.IDSDTYPEUSER);
        if (Exist == -1) SDTYPEUSER_RWXList.push(TSDTYPEUSER_RWX);
    }
    return (SDTYPEUSER_RWXList)
}


//Socket Function 
UsrCfg.SD.TYPEUSER_RWX.Methods.SDTYPEUSER_RWXListToByte = function (SDTYPEUSER_RWXList, MemStream, idx) {
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, SDTYPEUSER_RWXList.length - idx);
    for (var i = idx; i < SDTYPEUSER_RWXList.length; i++) {
        SDTYPEUSER_RWXList[i].ToByte(MemStream);
    }
}
UsrCfg.SD.TYPEUSER_RWX.Methods.ByteToSDTYPEUSER_RWXList = function (SDTYPEUSER_RWXList, MemStream) {
    var Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (var i = 0; i < Count; i++) {
        var UnSDTYPEUSER_RWX = new UsrCfg.SD.TYPEUSER_RWX.Properties.TSDTYPEUSER_RWX();
        UnSDTYPEUSER_RWX.ByteTo(MemStream);
        Methods.SDTYPEUSER_RWX_ListAdd(SDTYPEUSER_RWXList, UnSDTYPEUSER_RWX);
    }
}


