﻿ItHelpCenter.SD.Shared.SLASelect.TfrSDSLASelect = function (inObject, incallback) {
    
    this.TParent = function () {
        return this;
    }.bind(this);

    var _this = this.TParent();
    this.ObjectHtml = inObject;
    this.CallbackModalResult = incallback;
    this.DBTranslate = null; 
    this.Mythis = "TfrSDSLASelect";
    this.ID = this.Mythis + (Math.floor(Math.random() * 1000));

    this.IDSDCASETYPE = UsrCfg.SD.Properties.TIDSDCASETYPE.GetEnum(undefined);
    this.IDSDRESPONSETYPE = UsrCfg.SD.Properties.TSDRESPONSETYPE.GetEnum(undefined);


    this.IDMDMODELTYPED = 0;//**
    this.IDSLA = 0;//***
    this.IDMDCATEGORYDETAIL = 0;//***
    this.Category = null;
    this.PathCategory = null;
    this.CASEMT_LIFESTATUS = "";         
    this.MT_COMMENTSM = "";
    this.MT_GUIDETEXT = "";
    this.MT_IDMDFUNCESC = 0;
    this.MT_IDMDHIERESC = 0;
    this.MT_IDMDMODELTYPED = 0;
    this.MT_IDMDMODELTYPED1 = 0;
    this.MT_IDMDSERVICETYPE = 0;
    this.MT_MAXTIME = 0;
    this.MT_NORMALTIME = 0;
    this.IDSDSCALETYPE_FUNC = UsrCfg.SD.Properties.TIDSDSCALETYPE_FUNC.GetEnum(undefined);
    this.IDSDSCALETYPE_HIER = UsrCfg.SD.Properties.TIDSDSCALETYPE_HIER.GetEnum(undefined);
    this.MT_OPEARTIONM = "";
    this.MT_POSSIBLERETURNS = "";
    this.MT_TITLEM = "";
    this.IDMDIMPACT = 0;//**
    this.IDMDPRIORITY = 0;//**
    this.IDMDURGENCY = 0;//**
    this.IDSDCASEEF = 0;
    this.IDSDCASE = 0;

    this.IDSDCASETYPE = 0;
    this.IDSDRESPONSETYPE = 0;
     

    this.IDCMDBUSER = 0;
    this.UserName = 0;
    this.EnableControls = true;

    //TRADUCCION     
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lb_Urgency", "Urgency:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lb_isMayor", "Is Mayor:");                   
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lb_SelectCategory", "Select category:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lb_Category", "Category:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lb_Details", "Details:");                   
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lb_Impact", "Impact:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lb_WAround", "Work-Around:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lb_WAroundfound", "Work-Arounds found: ");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lb_Priority", "Priority:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lb_Title", "Title:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lb_Description", "Description:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lb_subtitel1", "Description");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lb_userSelect", "Work-Around")

    _this.LoadComponent();
}

ItHelpCenter.SD.Shared.SLASelect.TfrSDSLASelect.prototype.LoadComponent = function () {
    var _this = this.TParent(); 
    var PathCSS = ItHelpCenter.ScriptManager.GetStyle("SD/CaseManager/NewCase/frSDCaseNew.css");
    ItHelpCenter.ScriptManager.AddStyleFile(PathCSS, "TfrSDSLASelectCSS", function () {

        _this.InitializeComponent();
 
    }, function (e) {
    });

}

ItHelpCenter.SD.Shared.SLASelect.TfrSDSLASelect.prototype.InitializeComponent = function () {
    var _this = this.TParent();
    _this.ObjectHtml.innerHTML = "";
     

    //ESTRUCTURA DIVS
    var Container = new TVclStackPanel(_this.ObjectHtml, "Container_" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
    divCont = Container.Column[0].This;
    if (SysCfg.App.Properties.Device.value == SysCfg.App.TDevice.Desktop.value) {
        $(divCont).addClass("TfrSDSLASelectContainer");
    } else {
        $(_this.ObjectHtml).css("padding-top", "0px");
        $(Container.Row.This).css("padding", "0px");
        $(divCont).css("padding", "0px");
    } 
      
    var Div1 = new TVclStackPanel(divCont, "Div1_" + _this.ID, 2, [[4, 8], [5, 7], [5, 7], [5, 7], [5, 7]]);
    var Div1C1 = Div1.Column[0].This;
    var Div1C2 = Div1.Column[1].This;
    _this.DivComboUrgency = Div1C2;

    var Div2 = new TVclStackPanel(divCont, "Div2_" + _this.ID, 2, [[4,8], [5, 7], [5, 7], [5, 7], [5, 7]]);
    var Div2C1 = Div2.Column[0].This;
    var Div2C2 = Div2.Column[1].This;

    var Div3 = new TVclStackPanel(divCont, "Div3_" + _this.ID, 2, [[4, 8], [5, 7], [5, 7], [5, 7], [5, 7]]);
    var Div3C1 = Div3.Column[0].This;
    var Div3C2 = Div3.Column[1].This;

    var Div4 = new TVclStackPanel(divCont, "Div4_" + _this.ID, 2, [[4, 8], [5, 7], [5, 7], [5, 7], [5, 7]]);
    var Div4C1 = Div4.Column[0].This;
    var Div4C2 = Div4.Column[1].This;

    var Div5 = new TVclStackPanel(divCont, "Div5_" + _this.ID, 2, [[4, 8], [5, 7], [5, 7], [5, 7], [5, 7]]);
    var Div5C1 = Div5.Column[0].This;
    var Div5C2 = Div5.Column[1].This;

    _this.ZonaWorkArrown = new TVclStackPanel(divCont, "Div6_" + _this.ID, 2, [[4, 8], [5, 7], [5, 7], [5, 7], [5, 7]]);
    _this.Div6C1 = _this.ZonaWorkArrown.Column[0].This;
    _this.Div6C2 = _this.ZonaWorkArrown.Column[1].This;

    var Div7 = new TVclStackPanel(divCont, "Div7_" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
    _this.divGrid = Div7.Column[0].This;
   
    var Div8 = new TVclStackPanel(divCont, "Div8_" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
    var Div8C = Div8.Column[0].This;//subtitle  

    var Div9 = new TVclStackPanel(divCont, "Div9_" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
    var Div9C = Div9.Column[0].This;//container detail
    $(Div9C).css("border", "1px solid grey");
    $(Div9.Row.This).css("padding-top", "0px");
    $(Div9C).css("padding-bottom", "10px");

    ////
    var Div9_1 = new TVclStackPanel(Div9C, "Div9_1_" + _this.ID, 2, [[4,8], [5, 7], [5, 7], [5, 7], [5, 7]]);
    var Div9_1C1 = Div9_1.Column[0].This;
    var Div9_1C2 = Div9_1.Column[1].This;
                     
    var Div9_2 = new TVclStackPanel(Div9C, "Div9_2_" + _this.ID, 2, [[4, 8], [5, 7], [5, 7], [5, 7], [5, 7]]);
    var Div9_2C1 = Div9_2.Column[0].This;
    var Div9_2C2 = Div9_2.Column[1].This;
    _this.DivComboPriority = Div9_2C2;

    var Div9_3 = new TVclStackPanel(Div9C, "Div9_3_" + _this.ID, 2, [[4, 8], [5, 7], [5, 7], [5, 7], [5, 7]]);
    var Div9_3C1 = Div9_3.Column[0].This;
    var Div9_3C2 = Div9_3.Column[1].This;
                      
    var Div9_4 = new TVclStackPanel(Div9C, "Div9_4_" + _this.ID, 2, [[4, 8], [5, 7], [5, 7], [5, 7], [5, 7]]);
    var Div9_4C1 = Div9_4.Column[0].This;
    var Div9_4C2 = Div9_4.Column[1].This;   
    
    $(_this.ZonaWorkArrown.Row.This).css("display", "none");    
    $(_this.divGrid).css("display", "none");

    //ELEMENTS
    var lb_Urgency = new Vcllabel(Div1C1, "lb_Urgency_" + _this.ID, TVCLType.BS, TlabelType.H0, "");
    lb_Urgency.innerText = UsrCfg.Traslate.GetLangText(_this.Mythis, "lb_Urgency");


    var lb_isMayor = new Vcllabel(Div2C1, "lb_isMayor_" + _this.ID, TVCLType.BS, TlabelType.H0, "");
    lb_isMayor.innerText = UsrCfg.Traslate.GetLangText(_this.Mythis, "lb_isMayor");

    _this.CheckisMayor = new TVclInputcheckbox(Div2C2, "CheckisMayor_" + _this.ID);
    _this.CheckisMayor.VCLType = TVCLType.BS;
    _this.CheckisMayor.onClick = function myfunction() {
        _this.CheckisMayor_Click(_this, _this.CheckisMayor);
    }


    _this.lb_SelectCategory = new Vcllabel(Div3C1, "lb_SelectCategory_" + _this.ID, TVCLType.BS, TlabelType.H0, "");
    _this.lb_SelectCategory.innerText = UsrCfg.Traslate.GetLangText(_this.Mythis, "lb_SelectCategory");

    _this.btnDetalle = new TVclImagen(Div3C2, "imgSearchCategory_" + _this.ID);
    _this.btnDetalle.Src = "image/24/Search.png"
    _this.btnDetalle.Cursor = "pointer";
    _this.btnDetalle.onClick = function () {
        _this.btnDetalle_Click(_this, _this.btnDetalle);        
    }

    _this.lb_Category = new Vcllabel(Div4C1, "lb_Category_" + _this.ID, TVCLType.BS, TlabelType.H0, "");
    _this.lb_Category.innerText = UsrCfg.Traslate.GetLangText(_this.Mythis, "lb_Category");

    _this.txtCategoria = new Vcllabel(Div4C2, "txtCategoria" + _this.ID, TVCLType.BS, TlabelType.H0, "");

    _this.lb_Details = new Vcllabel(Div5C1, "lb_Details_" + _this.ID, TVCLType.BS, TlabelType.H0, "");
    _this.lb_Details.innerText = UsrCfg.Traslate.GetLangText(_this.Mythis, "lb_Details");

    _this.TextDetails = new Vcllabel(Div5C2, "lb_DetailsText_" + _this.ID, TVCLType.BS, TlabelType.H0, "");
    _this.TextDetails.innerText = "";

    ////////////////  
    var lb_WAround = new Vcllabel(_this.Div6C1, "lb_WAround_" + _this.ID, TVCLType.BS, TlabelType.H0, "");
    lb_WAround.innerText = UsrCfg.Traslate.GetLangText(_this.Mythis, "lb_WAround");

    _this.btnWorkArrown = new TVclImagen(_this.Div6C2, "imgWAround_" + _this.ID);
    _this.btnWorkArrown.Src = "image/24/Product-documentation.png"
    _this.btnWorkArrown.Cursor = "pointer";
    _this.btnWorkArrown.onClick = function () {
        _this.btnWorkArrown_Click(_this, _this.btnWorkArrown); 
    }

   _this.lb_ErroresText = new Vcllabel(_this.Div6C2, "lb_ErroresText_" + _this.ID, TVCLType.BS, TlabelType.H0, "");    
   $(_this.lb_ErroresText).css("margin-left", "7px");

    //Grid  
    $(_this.divGrid).css("min-height", "70px");
    $(_this.divGrid).css("border", "1px solid grey");
    $(_this.divGrid).css("border-radius;", "0px 20px 0px 0px");

    ////subtitle
    var lb_subtitel1 = new Vcllabel(Div8C, "lb_subtitel1_" + _this.ID, TVCLType.BS, TlabelType.H0, "");
    lb_subtitel1.innerText = UsrCfg.Traslate.GetLangText(_this.Mythis, "lb_subtitel1");
    $(Div8C).css("width", "auto");
    $(Div8C).css("background", "rgba(236, 240, 245, 1)");
    $(Div8C).css("margin-bottom", "0px");
    $(Div8C).css("border-radius", "0px 12p 0px 0px");
    $(Div8C).css("border-top", "1px solid grey");
    $(Div8C).css("border-right", "1px solid grey");
    $(Div8C).css("border-left", "1px solid grey");

    var lb_Impact = new Vcllabel(Div9_1C1, "lb_Impact_" + _this.ID, TVCLType.BS, TlabelType.H0, "");
    lb_Impact.innerText = UsrCfg.Traslate.GetLangText(_this.Mythis, "lb_Impact");

    _this.DivComboImpact = Div9_1C2;
    

    var lb_Priority = new Vcllabel(Div9_2C1, "lb_Priority_" + _this.ID, TVCLType.BS, TlabelType.H0, "");
    lb_Priority.innerText = UsrCfg.Traslate.GetLangText(_this.Mythis, "lb_Priority");

    
    var lb_Title = new Vcllabel(Div9_3C1, "lb_Title_" + _this.ID, TVCLType.BS, TlabelType.H0, "");
    lb_Title.innerText = UsrCfg.Traslate.GetLangText(_this.Mythis, "lb_Title");
    
    
    _this.CheckTitle = new TVclInputcheckbox(Div9_3C1, "CheckisMayor_" + _this.ID);
    _this.CheckTitle.VCLType = TVCLType.BS;
    _this.CheckTitle.onClick = function myfunction() {
        _this.CheckTitle_Checked(_this, _this.CheckTitle);             
    }
    $(_this.CheckTitle.This).css("float", "right");    
     
    _this.txtTitulo = new TVclTextBox(Div9_3C2, "TxtSubject_" + _this.ID);
    $(_this.txtTitulo.This).css('width', "100%");
    $(_this.txtTitulo.This).addClass("TextFormat");    

    var lb_Description = new Vcllabel(Div9_4C1, "lb_Priority_" + _this.ID, TVCLType.BS, TlabelType.H0, "");
    lb_Description.innerText = UsrCfg.Traslate.GetLangText(_this.Mythis, "lb_Description");
     
    _this.CheckDescription = new TVclInputcheckbox(Div9_4C1, "CheckDescription_" + _this.ID);
    _this.CheckDescription.VCLType = TVCLType.BS;
    _this.CheckDescription.onClick = function myfunction() {
        _this.CheckDescription_Checked(_this, _this.CheckDescription);
    }
    $(_this.CheckDescription.This).css("float", "right");
       

    _this.txtDescripcion = new TVclMemo(Div9_4C2, "AreaTextDescription_" + _this.ID)
    $(_this.txtDescripcion.This).css('width', "100%");
    $(_this.txtDescripcion.This).css('height', "70px");
    $(_this.txtDescripcion.This).addClass("TextFormat");
    
      
    _this.Initialize();
}

ItHelpCenter.SD.Shared.SLASelect.TfrSDSLASelect.prototype.Initialize = function () {
    var _this = this.TParent();
 
    _this.LoadComboUrgency();
    _this.LoadComboImpact();
    _this.LoadComboPriority();

    _this.CmbUrgency.SelectedIndex = 1;

    switch (_this.IDSDCASETYPE) {
        case UsrCfg.SD.Properties.TIDSDCASETYPE._ACTIVITY_NEW:
        case UsrCfg.SD.Properties.TIDSDCASETYPE._NORMAL:
            _this.txtCategoria.Visible = true;
            _this.lb_Category.Visible = true;
            _this.TextDetails.Visible = true;
            _this.lb_Details.Visible = true;
            _this.TextDetails.Visible = true;
            _this.lb_SelectCategory.innerText = UsrCfg.Traslate.GetLangText(_this.Mythis, "lb_SelectCategory");
            _this.CargarDetalleWorkArrown();
            _this.CargarDatosGridDetalle();
            break;
        case UsrCfg.SD.Properties.TIDSDCASETYPE._ACTIVITY_MODEL:
            _this.txtCategoria.Visible = false;
            _this.lb_Category.Visible = false;
            _this.TextDetails.Visible = false;
            _this.lb_Details.Visible = false;
            _this.TextDetails.Visible = false;
            _this.CargarDetalleWorkArrown();
            _this.CargarDatosGridDetalle();
            break;
        default:
            break;
    }
    _this.CallbackModalResult(true); //termino el load
}

ItHelpCenter.SD.Shared.SLASelect.TfrSDSLASelect.prototype.ReInitialize = function (inIDMDCATEGORYDETAIL) {
    var _this = this.TParent();

    _this.IDMDCATEGORYDETAIL = inIDMDCATEGORYDETAIL;
    switch (_this.IDSDCASETYPE) {
        case UsrCfg.SD.Properties.TIDSDCASETYPE._ACTIVITY_NEW:
        case UsrCfg.SD.Properties.TIDSDCASETYPE._NORMAL:
            _this.txtCategoria.Visible = true;
            _this.lb_Category.Visible = true;
            _this.TextDetails.Visible = true;
            _this.lb_Details.Visible = true;
            _this.TextDetails.Visible = true;
            _this.lb_SelectCategory.innerText = UsrCfg.Traslate.GetLangText(_this.Mythis, "lb_SelectCategory");
            _this.LoadCategory();
            _this.CargarDetalleWorkArrown();
            _this.CargarDatosGridDetalle();
            break;
        case UsrCfg.SD.Properties.TIDSDCASETYPE._ACTIVITY_MODEL:
            _this.txtCategoria.Visible = false;
            _this.lb_Category.Visible = false;
            _this.TextDetails.Visible = false;
            _this.lb_Details.Visible = false;
            _this.TextDetails.Visible = false;
            _this.LoadCategory();
            _this.CargarDetalleWorkArrown();
            _this.CargarDatosGridDetalle();
            break;
        default:
            break;
    }
}

ItHelpCenter.SD.Shared.SLASelect.TfrSDSLASelect.prototype.btnWorkArrown_Click = function (sender, e) {
    var Modal = new TVclModal(document.body, null, "IDModal_0");
    if (SysCfg.App.Properties.Device.value == SysCfg.App.TDevice.Desktop.value) {
        Modal.Width = "60%";
        Modal.Body.This.style.minHeight = "300px";
    } else {
        Modal.Width = "100%";
    }    
    Modal.Top = 0;
    Modal.AddHeaderContent("Work Around");
    Modal.Body.This.id = "Modal_WorkAround_" + _this.ID;

    Modal.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.Mythis, "lb_userSelect"));
    Modal.Body.This.id = "Modal_SearchDataSet_" + _this.ID;
    var frSMKEWAConteiner = new ItHelpCenter.frSMKEWAConteiner(Modal.Body.This, _this.IDSDCASE, _this.IDMDCATEGORYDETAIL, function (outRest) {

    });
    Modal.ShowModal();
    Modal.FunctionClose = function () {

    }
}

ItHelpCenter.SD.Shared.SLASelect.TfrSDSLASelect.prototype.btnDetalle_Click = function (sender, e) {
    _this = sender;

    try {
        var Modal = new TVclModal(document.body, null, "IDModal_" + _this.ID);
        if (SysCfg.App.Properties.Device.value == SysCfg.App.TDevice.Desktop.value) {
            Modal.Width = "70%";
            Modal.Body.This.style.minHeight = "300px";
        } else {
            Modal.Width = "100%";
        }       
        Modal.Top = 0;
        Modal.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.Mythis, "lb_SelectCategory"));
        Modal.Body.This.id = "Modal_SearchCategory_" + _this.ID;

        //Parameters: (6) = 1.HTML Container, 2.Function Back, 3.Modo busqueda, 4.Ver boton cambio de modo (true - False), 5.Ver mensaje resultado, 6.Frase predeterminada    
        var _Category = new ItHelpCenter.SD.Shared.Category.TCategoryManger(
           Modal.Body.This,
            function (OutRes) {
                if (OutRes.ID > 0) {
                    _this.IDMDCATEGORYDETAIL = OutRes.ID;
                    _this.Category = OutRes.Detail;
                    _this.PathCategory = OutRes.Path;

                    //FUNCIONES                     
                    _this.CargarDetalleWorkArrown();
                    _this.CargarDatosGridDetalle();
                        
                    _this.CheckTitle_Checked(_this, null);
                    _this.CheckDescription_Checked(_this, null);
                } else {
                    $(_this.ZonaWorkArrown.Row.This).css("display", "none");
                    $(Div7C).css("display", "none");
                    _this.IDMDCATEGORYDETAIL = 0;
                    _this.Category = "";
                    _this.PathCategory = "";
                    _this.IDSLA = 0;
                }

                _this.txtCategoria.innerText = _this.PathCategory;
                _this.TextDetails.innerText = _this.Category;
                Modal.CloseModal();
                _this.CallbackModalResult(false, _this);
            },
            ItHelpCenter.SD.Shared.Category.Mode.SeekSearch,
            true, false, "");

        Modal.ShowModal();
        Modal.FunctionClose = function () {

        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("frSDSLASelect.js ItHelpCenter.SD.Shared.SLASelect.TfrSDSLASelect.prototype.btnDetalle_Click", e);
        //debugger;
    }
}


ItHelpCenter.SD.Shared.SLASelect.TfrSDSLASelect.prototype.CargarDetalleWorkArrown = function () {
    var _this = this.TParent();

    try {
        direc = SysCfg.App.Properties.xRaiz + "SD/CaseUser/NewCase/SDCaseNew.svc/CargarDetalleWorkArrown";
        dataArray = {
            IDMDCATEGORYDETAIL: _this.IDMDCATEGORYDETAIL
        };

        $.ajax({
            async: false,
            type: "POST",
            url: direc,
            data: JSON.stringify(dataArray),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $(_this.ZonaWorkArrown.Row.This).css("display", "block");
                _this.lb_ErroresText.innerText = UsrCfg.Traslate.GetLangText(_this.Mythis, "lb_WAroundfound") + response.d;
            },
            error: function (error) {
                //enviar a la web
                alert('error: ' + error.txtDescripcion + " \n Location: Cargar Detalle Work Arrown");
            }
        });
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("frSDSLASelect.js ItHelpCenter.SD.Shared.SLASelect.TfrSDSLASelect.prototype.CargarDetalleWorkArrown", e);
    }
}

ItHelpCenter.SD.Shared.SLASelect.TfrSDSLASelect.prototype.LoadCategory = function () {
    var _this = this.TParent();

    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.IDMDCATEGORYDETAIL.FieldName, _this.IDMDCATEGORYDETAIL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var DS = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "SELECT_SLA_AUTO", Param.ToBytes());
        if (DS.ResErr.NotError) {
            if (DS.DataSet.RecordCount > 0) {
                DS.DataSet.First();
                if (!(DS.DataSet.Eof)) {
                    _this.txtCategoria.innerText = DS.DataSet.RecordSet.FieldName("CATEGORY").asString();
                    _this.TextDetails.innerText = DS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CATEGORYNAME.FieldName).asString();
                }
            }
            else {
                DS.ResErr.NotError = false;
                DS.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
}

ItHelpCenter.SD.Shared.SLASelect.TfrSDSLASelect.prototype.CargarDatosGridDetalle = function () {
    var _this = this.TParent();  
    try {
        direc = SysCfg.App.Properties.xRaiz + "SD/CaseUser/NewCase/SDCaseNew.svc/GetSLAbyCATEGORYList";
        dataArray = {
            IDSDCASEEF: _this.IDSDCASEEF,
            IDMDCATEGORYDETAIL: _this.IDMDCATEGORYDETAIL,
            IDSDRESPONSETYPE: _this.IDSDRESPONSETYPE.value,
            CASE_ISMAYOR: _this.CheckisMayor.Checked,
            IDMDURGENCY: _this.IDMDURGENCY
        };
        
        _this.divGrid.innerHTML = "";
        $.ajax({
            async: false,
            type: "POST",
            url: direc,
            data: JSON.stringify(dataArray),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {                
                try {
                    _this.IDSLA = response.d[0].IDSLA;
                    _this.MT_IDMDMODELTYPED = response.d[0].MT_IDMDMODELTYPED;

                    _this.ComboImpact.Value = response.d[0].IDMDIMPACT;
                    _this.IDPRIORITY = response.d[0].IDMDIMPACT;
                    _this.LoadGrid(response.d[0].DataTableJs);
                    $(_this.divGrid).css("display", "block");
                } catch (e) {
                    SysCfg.Log.Methods.WriteLog("frSDSLASelect.js ItHelpCenter.SD.Shared.SLASelect.TfrSDSLASelect.prototype.CargarDatosGridDetalle", e);
                }               

            },
            error: function (error) {
                //enviar a la web
                alert('error: ' + error.txtDescripcion + " \n Location: Cargar Detalle Work Arrown");
            }
        });
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("frSDSLASelect.js ItHelpCenter.SD.Shared.SLASelect.TfrSDSLASelect.prototype.CargarDatosGridDetalle", e);

    }
}

ItHelpCenter.SD.Shared.SLASelect.TfrSDSLASelect.prototype.LoadComboUrgency = function () {
    var _this = this.TParent();

    var MDURGENCYList = Persistence.Profiler.CatalogProfiler.MDURGENCYList;

    _this.CmbUrgency = new TVclComboBox2(_this.DivComboUrgency, "CmbUrgency_" + _this.ID);
    for (var i = 0; i < MDURGENCYList.length; i++) {
        var VclComboBoxItem = new TVclComboBoxItem();
        VclComboBoxItem.Value = MDURGENCYList[i].IDMDURGENCY;
        VclComboBoxItem.Text = MDURGENCYList[i].URGENCYNAME;       

        _this.CmbUrgency.AddItem(VclComboBoxItem);
    }
    _this.CmbUrgency.onChange = function () {
        _this.IDMDURGENCY = parseInt(_this.CmbUrgency.Value);
        _this.CargarDatosGridDetalle();
      
    }
    $(_this.CmbUrgency.This).css('width', "100%");
    $(_this.CmbUrgency.This).addClass("TextFormat");

    _this.CmbUrgency.selectedIndex = 0;
    _this.IDMDURGENCY = MDURGENCYList[0].IDMDURGENCY;  
}

ItHelpCenter.SD.Shared.SLASelect.TfrSDSLASelect.prototype.SetDisplayUrgencia = function(IDUrgencia)
{
    var _this = this.TParent();

    for (var i = 0; i < _this.CmbUrgency.Options.length; i++) {
        if (i == 0) {
            _this.CmbUrgency.selectedIndex = i;
            _this.IDMDURGENCY = _this.CmbUrgency.Options[i].Value;
        }
        if (_this.CmbUrgency.Options[i].Value == IDUrgencia) {
            _this.CmbUrgency.selectedIndex = i;
            _this.IDMDURGENCY = IDUrgencia;
        }
    }
}

ItHelpCenter.SD.Shared.SLASelect.TfrSDSLASelect.prototype.LoadComboImpact = function () {
    var _this = this.TParent();

    var MDIMPACTList = Persistence.Profiler.CatalogProfiler.MDIMPACTList;

    _this.ComboImpact = new TVclComboBox2(_this.DivComboImpact, "ComboImpact_" + _this.ID);
    for (var i = 0; i < MDIMPACTList.length; i++) {
        var VclComboBoxItem = new TVclComboBoxItem();
        VclComboBoxItem.Value = MDIMPACTList[i].IDMDIMPACT;
        VclComboBoxItem.Text = MDIMPACTList[i].IMPACTNAME;

        _this.ComboImpact.AddItem(VclComboBoxItem);
    }
    _this.ComboImpact.onChange = function () {
        _this.IDIMPACT = _this.ComboImpact.Value;
    }
    $(_this.ComboImpact.This).css('width', "100%");
    $(_this.ComboImpact.This).addClass("TextFormat");

    _this.ComboImpact.selectedIndex = 0;
    _this.IDIMPACT = MDIMPACTList[0].IDMDIMPACT;     
} 

ItHelpCenter.SD.Shared.SLASelect.TfrSDSLASelect.prototype.SetDisplayImpacto = function (IDImpacto) {
    var _this = this.TParent(); 

    for (var i = 0; i < _this.ComboImpact.Options.length; i++) {
        if (i == 0) {
            _this.ComboImpact.selectedIndex = i;
            _this.IDIMPACT = _this.ComboImpact.Options[i].Value;
        }
        if (_this.ComboImpact.Options[i].Value == IDImpacto) {
            _this.ComboImpact.selectedIndex = i;
            _this.IDIMPACT = IDImpacto;
        }
    }
}

ItHelpCenter.SD.Shared.SLASelect.TfrSDSLASelect.prototype.LoadComboPriority = function () {
    var _this = this.TParent();

    var MDPRIORITYList = Persistence.Profiler.CatalogProfiler.MDPRIORITYList;

    _this.CmbPriority = new TVclComboBox2(_this.DivComboPriority, "CmbPriority_" + _this.ID);
    for (var i = 0; i < MDPRIORITYList.length; i++) {
        var VclComboBoxItem = new TVclComboBoxItem();
        VclComboBoxItem.Value = MDPRIORITYList[i].IDMDPRIORITY;
        VclComboBoxItem.Text = MDPRIORITYList[i].PRIORITYNAME;

        _this.CmbPriority.AddItem(VclComboBoxItem);
    }
    _this.CmbPriority.onChange = function () {
        _this.IDPRIORITY = _this.CmbPriority.Value;
    }
    $(_this.CmbPriority.This).css('width', "100%");
    $(_this.CmbPriority.This).addClass("TextFormat");

    _this.CmbPriority.selectedIndex = 0;
    _this.IDPRIORITY = MDPRIORITYList[0].IDMDPRIORITY; 
}
 

ItHelpCenter.SD.Shared.SLASelect.TfrSDSLASelect.prototype.SetDisplayPriority = function (IDPriority) {
    var _this = this.TParent();
     
    for (var i = 0; i < _this.CmbPriority.Options.length; i++) {
        if (i == 0) {
            _this.CmbPriority.selectedIndex = i;
            _this.IDPRIORITY = _this.CmbPriority.Options[i].Value;
        }
        if (_this.CmbPriority.Options[i].Value == IDPriority) {
            _this.CmbPriority.selectedIndex = i;
            _this.IDPRIORITY = IDPriority;
        }
    }
}

ItHelpCenter.SD.Shared.SLASelect.TfrSDSLASelect.prototype.CheckTitle_Checked = function (sender, e) {
    var _this = sender;
    if (_this.CheckTitle.Checked) {
        _this.txtTitulo.Text = Persistence.Model.Methods.GETSDCASE_TITLE(_this.IDCMDBUSER, _this.IDMDCATEGORYDETAIL, _this.IDSDCASE);
    }
}

ItHelpCenter.SD.Shared.SLASelect.TfrSDSLASelect.prototype.CheckDescription_Checked = function (sender, e) {
    var _this = sender;
    if (_this.CheckDescription.Checked) {
        _this.txtDescripcion.Text = Persistence.Model.Methods.GETSDCASE_DESCRIPTION(_this.IDCMDBUSER, _this.IDMDCATEGORYDETAIL, _this.IDSDCASE);
    }
}

ItHelpCenter.SD.Shared.SLASelect.TfrSDSLASelect.prototype.CheckisMayor_Click = function (sender, e) {
    var _this = sender;
    _this.CargarDatosGridDetalle();
}

ItHelpCenter.SD.Shared.SLASelect.TfrSDSLASelect.prototype.LoadGrid = function (OpenDataSet) {
    var _this = this.TParent();
   
    var Grilla = new TGrid(_this.divGrid, "", ""); //CREA UN GRID CONTRO
    for (var i = 0; i < OpenDataSet.Columnas.length; i++) {
        var Col0 = new TColumn(); //CREA UNA COLUMNA PARA AGREGAR AL GRID CONTROL
        Col0.Name = OpenDataSet.Columnas[i].NombreColumna;
        Col0.Caption = OpenDataSet.Columnas[i].NombreColumna;
        Col0.Index = i;
        Col0.EnabledEditor = false; //ASIGNA O EXTRAE UN VALO TRUE O FALSE QUE INDICA SI LA COLUMNA MOSTRARA UN EDITOR DE DATOS             
        Grilla.AddColumn(Col0); //AGREGA UNA COLUMNA AL GRIDCONTROL 
    }

    for (var i = 0; i < OpenDataSet.Filas.length; i++) {//rows
        var index = i;
        var NewRow = new TRow(); //CREA UN ROW PARA AGREGAR AL GRIDCONTROL  
        for (var j = 0; j < OpenDataSet.Columnas.length; j++) {//columns
            var Cell = new TCell();
            //debugger;
            try {
                Cell.Value = OpenDataSet.Filas[i].Celdas[j].Valor;
                Cell.IndexColumn = j;
                Cell.IndexRow = i;
                NewRow.AddCell(Cell);
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("frSDSLASelect.js ItHelpCenter.SD.Shared.SLASelect.TfrSDSLASelect.prototype.LoadGrid", e);
                //debugger;
            }
          
            if (i == 0) {
                _this.RowSelected = NewRow;
                _this.GridCategoria_SelectionChanged(OpenDataSet, index);
            }
        }
        NewRow.onclick = function () { // ASIGNA UN EVENTO CUANDO SE HACE CLICK EN EL ROW DEL GRIDCONTROL                
            Grilla_rowclick(NewRow);
        }
        Grilla.AddRow(NewRow);

        
    }

    var Grilla_rowclick = function (inRow) {
        //alert(inRow.Cells[1].Value);
        _this.RowSelected = inRow;

        _this.GridCategoria_SelectionChanged(OpenDataSet, index);
    }
}

ItHelpCenter.SD.Shared.SLASelect.TfrSDSLASelect.prototype.GridCategoria_SelectionChanged = function (sender, index)
{
    var _this = this.TParent();
 
        //cmbImpacto.EditValue = Convert.ToInt32(currentRow["IDMDIMPACT"].ToString());
    if (_this.EnableControls == true) {
        _this.IDMDIMPACT = parseInt(sender.Filas[index].Celdas[12].Valor);
        _this.SetDisplayImpacto(_this.IDMDIMPACT);
        _this.EnableControls = true;

        _this.IDSLA = parseInt(sender.Filas[index].Celdas[0].Valor); 
        _this.MT_COMMENTSM = sender.Filas[index].Celdas[6].Valor; 
        _this.MT_GUIDETEXT = sender.Filas[index].Celdas[7].Valor;
        _this.MT_IDMDFUNCESC = parseInt(sender.Filas[index].Celdas[14].Valor);
        _this.MT_IDMDHIERESC = parseInt(sender.Filas[index].Celdas[15].Valor);
        _this.MT_IDMDMODELTYPED =  parseInt(sender.Filas[index].Celdas[16].Valor);
        _this.MT_IDMDMODELTYPED1 = parseInt(sender.Filas[index].Celdas[17].Valor);
        _this.MT_MAXTIME = parseInt(sender[4]);
        _this.MT_NORMALTIME = parseInt(sender[5]);
        _this.IDSDSCALETYPE_FUNC = parseInt(sender.Filas[index].Celdas[19].Valor);
        _this.IDSDSCALETYPE_HIER = parseInt(sender.Filas[index].Celdas[20].Valor);
        _this.MT_OPEARTIONM = sender.Filas[index].Celdas[8].Valor;
        _this.MT_POSSIBLERETURNS = sender.Filas[index].Celdas[9].Valor;
       
        _this.CASEMT_LIFESTATUS = sender.Filas[index].Celdas[21].Valor;
      
    }
}

