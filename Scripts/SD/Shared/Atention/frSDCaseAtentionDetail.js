﻿
ItHelpCenter.SD.Shared.Atention.CaseAtentionDetail.TfrSDCaseAtentionDetail = function (ElementDiv, CallbackModalResult, inSDCONSOLEATENTION) {

    this.TParent = function () {
        return this;
    }.bind(this);

    var _this = this.TParent();
    this.ObjectHtml = ElementDiv;
    this.CallbackModalResult = CallbackModalResult;
    this.DBTranslate = null;
    this.Mythis = "TfrSDCaseAtentionDetail";
    this.ID = this.Mythis + (Math.floor(Math.random() * 1000));
    this.DataSet = null;
    
    this.IDSDCASE = inSDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE;
    this.IDCMDBCI = inSDCONSOLEATENTION.SDCASECOMPLTE.IDOWNER;
    this.IDMDMODELTYPED = inSDCONSOLEATENTION.SDCASEACTIVITIESCOMPLTE.IDMDMODELTYPED;
  
    UsrCfg.Traslate.GetLangText(_this.Mythis, "TabInformation", "Information");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "TabPermissions", "Permissions");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "TabAtention", "Attention");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "TabGraphic", "Graphic");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "GridHeader1", "Value");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "GridHeader2", "Descript");

    _this.InitializeComponent();
}

ItHelpCenter.SD.Shared.Atention.CaseAtentionDetail.TfrSDCaseAtentionDetail.prototype.InitializeComponent = function () {

    var _this = this.TParent();

    var Container = new TVclStackPanel(_this.ObjectHtml, "Container_" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
    _this.divCont = Container.Column[0].This;
   
    var DivAccept = new TVclStackPanel(_this.ObjectHtml, "Container_" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
    DivAcceptC = DivAccept.Column[0].This;

    //Aceptar        
    if (SysCfg.App.Properties.Device.value == SysCfg.App.TDevice.Desktop.value) {
        $(DivAcceptC).css("text-align", "right");
    } else {
        $(DivAcceptC).css("text-align", "center");
        $(DivAcceptC).css("width", "60%");
    }

    _this.BtnClose = new TVclButton(DivAcceptC, "BtnClose");
    _this.BtnClose.Text = "Aceptar";
    _this.BtnClose.VCLType = TVCLType.BS;
    _this.BtnClose.Cursor = "pointer";
    _this.BtnClose.onClick = function () {
        _this.BtnClose_Click(_this, _this.BtnClose);
    }
   
    

    //TABCONTROL
    _this.TabControls = new TTabControl(_this.divCont, "", "");

    _this.TabInformation = new TTabPage();
    _this.TabInformation.Name = "Information";
    _this.TabInformation.Caption = UsrCfg.Traslate.GetLangText(_this.Mythis, "TabInformation");
    _this.TabControls.AddTabPages(_this.TabInformation);

    _this.TabPermissions = new TTabPage();
    _this.TabPermissions.Name = "Permissions";
    _this.TabPermissions.Caption = UsrCfg.Traslate.GetLangText(_this.Mythis, "TabPermissions");
    _this.TabControls.AddTabPages(_this.TabPermissions);

    // _this.TabAtention = new TTabPage();
    // _this.TabAtention.Name = "Attention";
    // _this.TabAtention.Caption = UsrCfg.Traslate.GetLangText(_this.Mythis, "TabAtention");
    // _this.TabControls.AddTabPages(_this.TabAtention);

    //if (!UsrCfg.Version.IsProduction) {
        _this.TabGraphic = new TTabPage();
        _this.TabGraphic.Name = "Graphic";
        _this.TabGraphic.Caption = UsrCfg.Traslate.GetLangText(_this.Mythis, "TabGraphic");
        _this.TabControls.AddTabPages(_this.TabGraphic);
    //}
    

    _this.Initialize();

    _this.TabInformation.Active = true;

}

ItHelpCenter.SD.Shared.Atention.CaseAtentionDetail.TfrSDCaseAtentionDetail.prototype.Initialize = function () {
    var _this = this.TParent();
    try {

        var Param = new SysCfg.Stream.Properties.TParam();
        try
        {
            Param.Inicialize();
            Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE.IDSDCASE.FieldName, _this.IDSDCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCI.FieldName, _this.IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);         
            var Open = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "GETSMCONSOLEMANAGER_WHO2", Param.ToBytes());
            ResErr = Open.ResErr;
            if (ResErr.NotError)
            {
                _this.DataSet = Open.DataSet;
                _this.Load_Information(_this, _this.TabInformation.Page);
                _this.Load_Permissions(_this, _this.TabPermissions.Page);                
                _this.Load_Graphic(_this, _this.TabGraphic.Page);                    

            }
            else
            {
                alert('error: ' + error.txtDescripcion + " \n Location: SD/Shared/ChildUserPermissions , Load Permissions");
            }
        }
        finally
        {
            Param.Destroy();
        }

      
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("frSDCaseAtentionDetail.js ItHelpCenter.SD.Shared.Atention.CaseAtentionDetail.TfrSDCaseAtentionDetail.prototype.Initialize", e);

    }
}
//{$R *.dfm}
ItHelpCenter.SD.Shared.Atention.CaseAtentionDetail.TfrSDCaseAtentionDetail.prototype.Load_Information = function (sender, e) {
    var _this = sender;
    var _cont = e;

    try {
        direc = SysCfg.App.Properties.xRaiz + "SD/CaseUser/NewCase/SDCaseNew.svc/CargarDetalleCaseAtention";
        dataArray = {
            IDSDCASE: _this.IDSDCASE
        };

        $.ajax({
            async: false,
            type: "POST",
            url: direc,
            data: JSON.stringify(dataArray),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                _this.GridDataSet(_this, _this.TabInformation.Page, response.d)
            },
            error: function (error) {
                //enviar a la web
                alert('error: ' + error.txtDescripcion + " \n Location: SD/Shared/FrSDCaseAtentionDetail , Load Information");
            }
        });
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("frSDCaseAtentionDetail.js ItHelpCenter.SD.Shared.Atention.CaseAtentionDetail.TfrSDCaseAtentionDetail.prototype.Load_Information", e);

    }
}

ItHelpCenter.SD.Shared.Atention.CaseAtentionDetail.TfrSDCaseAtentionDetail.prototype.Load_Permissions = function (sender, e) {
    var _this = sender;
    var _cont = e;

    var UserPermissions = new ItHelpCenter.SD.Shared.Atention.ChildUserPermissions.TfrChildUserPermissions(_cont, function (thisOut) {

    }, _this.DataSet);
}

ItHelpCenter.SD.Shared.Atention.CaseAtentionDetail.TfrSDCaseAtentionDetail.prototype.Load_Atention = function (sender, e) {
    var _this = sender;
    var _cont = e;

}

ItHelpCenter.SD.Shared.Atention.CaseAtentionDetail.TfrSDCaseAtentionDetail.prototype.Load_Graphic = function (sender, e) {
    var _this = sender;
    var _cont = e;
    //_this.TabGraphic.Page contenedor
    //GRAFICO
    //TABCONTROL
    _this.TabControl1 = new TTabControl(_cont, "", "");

    var ListaModelos = new Array(); // ObservableCollection<ConsolePermissionModel>();
    var existmodel = false;

    if (_this.DataSet.RecordCount > 0) {
        for (var j = 0; j < _this.DataSet.Records.length; j++) {
            existmodel = false;
            for (var i = 0; i < ListaModelos.length; i++) {
                var cp = ListaModelos[i];

                
                if (cp.IDMDMODELTYPED == parseInt(_this.DataSet.Records[j].Fields[2].Value)) {// parseInt(_this.DataSet.Records[j].Fields[2].Value)DS_DMTABLA.DataSet.RecordSet.FieldName(UsrCfg.InternoDemoNames.DMTABLA.FDENTRO.FieldName).asInt32()
                    existmodel = true;
                    //var cp = new ItHelpCenter.SD.AConsole.ConsolePermissionModel();
                    cp.ConsolePermissionClassList.push(_this.DataSet.Records[j]);
                    break;
                }
            }

            if (!existmodel) {
                var cp1 = new ItHelpCenter.SD.AConsole.ConsolePermissionModel();
                cp1.IDMDMODELTYPED = parseInt(_this.DataSet.Records[j].Fields[2].Value);
                cp1.TITLEM = _this.DataSet.Records[j].Fields[4].Value.toString();
                cp1.ConsolePermissionClassList.push(_this.DataSet.Records[j]);
                ListaModelos.push(cp1);
            }
        }

        for (var i = 0; i < ListaModelos.length; i++) {
            var cp = ListaModelos[i];

            var ti = new TTabPage();
            ti.Name = cp.TITLEM;
            ti.Caption = cp.TITLEM;
            ti.Tag = cp;

            //var ListaSteps = new Array();

            //var NombreStep = "";
            //var IdStep = 0;
            //var existestep = false;

            //for (var j = 0; j < cp.ConsolePermissionClassList.length; j++) {
            //    var dyn = cp.ConsolePermissionClassList[j];

            //    var MDLIFESTATUSProfiler = new UsrCfg.SD.TMDLIFESTATUSProfiler();

            //    MDLIFESTATUSProfiler.StrtoLIFESTATUS(dyn.Fields[3].Value.toString());
            //    var ls = MDLIFESTATUSProfiler.MDLIFESTATUSList[MDLIFESTATUSProfiler.MDLIFESTATUSList.Count - 1];

            //    IdStep = ls.IDMDLIFESTATUS;
            //    NombreStep = ls.NAMESTEP;

            //    existestep = false;
            //    for (var k = 0; k < cp.ConsolePermissionClassList.length; k++) {
            //        var subcp = cp.ConsolePermissionClassList[k];
            //        if (subcp.IDMDLIFESTATUS == IdStep) {
            //            existestep = true;
            //            subcp.ConsolePermissionClassList.push(dyn);
            //            break;
            //        }
            //    }

            //    if (!existestep) {
            //        var cp1 = new ItHelpCenter.SD.AConsole.ConsolePermissionStep();
            //        cp1.IDMDLIFESTATUS = IdStep;
            //        cp1.NAMESTEP = NombreStep;
            //        cp1.ConsolePermissionClassList.push(dyn);
            //        ListaSteps.push(cp1);
            //    }
            //}

            //var tc = new new TTabControl(ti.Page, "", "");
            //for (var i = 0; i < ListaSteps.length; i++) {
            //    var step = ListaSteps[i];

            //    var subti = new TTabPage();
            //    subti.Name = step.NAMESTEP;
            //    subti.Caption = step.NAMESTEP;
            //    subti.Tag = step;



            //    tc.AddTabPages(subti);
            //}
            //var texto = JSON.stringify(cp);
            var grafyModel = new ItHelpCenter.MD.TUcGrapyModel(document.getElementsByTagName("BODY")[0], "GraphicModel2", false,null);
            grafyModel.Inicialize(cp.IDMDMODELTYPED);
            grafyModel.ConfigUser(cp, false);
            grafyModel.CreateGraphic();
            $(ti.Page).append(grafyModel.container.Row.This);

            _this.TabControl1.AddTabPages(ti);

            if (_this.TabControl1.TabPages.length > 0)
                _this.TabControl1.TabPages[0].Active = true;
        }
    }
    else {
        Open.ResErr.NotError = false;
        Open.ResErr.Mesaje = "RECORDCOUNT = 0";
    }
}


ItHelpCenter.SD.Shared.Atention.CaseAtentionDetail.TfrSDCaseAtentionDetail.prototype.GridDataSet = function (sender, e, DataSet) {
    var _this = sender;
    var _cont = e;

    //GRID
    try {
        _cont.innerHTML = "";
        var Grilla = new TGrid(_cont, "", ""); //CREA UN GRID CONTRO

        var Col0 = new TColumn(); //CREA UNA COLUMNA PARA AGREGAR AL GRID CONTROL
        Col0.Name = "Value";
        Col0.Caption = UsrCfg.Traslate.GetLangText(_this.Mythis, "GridHeader1");
        Col0.Index = 0;
        Col0.DataType = SysCfg.DB.Properties.TDataType.String;
        Col0.EnabledEditor = false;
        Grilla.AddColumn(Col0);

        var Col1 = new TColumn(); //CREA UNA COLUMNA PARA AGREGAR AL GRID CONTROL
        Col1.Name = "Description";
        Col1.Caption = UsrCfg.Traslate.GetLangText(_this.Mythis, "GridHeader2");
        Col1.Index = 1;
        Col1.DataType = SysCfg.DB.Properties.TDataType.String;
        Col1.EnabledEditor = false;
        Grilla.AddColumn(Col1);

        for (var i = 0; i < DataSet.length; i++) {//rows
            var NewRow = new TRow(); //CREA UN ROW PARA AGREGAR AL GRIDCONTROL  

            var Cell = new TCell();
            Cell.Value = DataSet[i].Value;
            Cell.IndexColumn = 0;
            Cell.IndexRow = i;
            NewRow.AddCell(Cell);

            var Cell = new TCell();
            Cell.Value = DataSet[i].Description;
            Cell.IndexColumn = 1;
            Cell.IndexRow = i;
            NewRow.AddCell(Cell);

            Grilla.AddRow(NewRow);
        }


        Grilla.OnRowClick = function (sender, EventArgs) {
            //_alert(EventArgs.Cell[0]);
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("frSDCaseAtentionDetail.js ItHelpCenter.SD.Shared.Atention.CaseAtentionDetail.TfrSDCaseAtentionDetail.prototype.GridDataSet", e);
        //debugger;
    }

}

ItHelpCenter.SD.Shared.Atention.CaseAtentionDetail.TfrSDCaseAtentionDetail.prototype.BtnClose_Click = function (sender, e) {
    var _this = sender;
    _this.CallbackModalResult();
}



ItHelpCenter.SD.AConsole.ConsolePermissionModel = function () {
    this.IDMDMODELTYPED = 0;
    this.TITLEM = "";
    this.ConsolePermissionClassList = new Array()
}


ItHelpCenter.SD.AConsole.ConsolePermissionStep = function () {

    this.IDMDLIFESTATUS = 0;
    this.NAMESTEP = "";
    this.ConsolePermissionClassList = new Array()

}


