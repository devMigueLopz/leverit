﻿//UsrCfg.SD

UsrCfg.SD.Properties.TCaseType =
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _None: { value: 0, name: "_None" },
    _Childrens: { value: 1, name: "_Childrens" },
    _Parents: { value: 2, name: "_Parents" },
    _Activities: { value: 3, name: "_Activities" }
}

//Variables


UsrCfg.SD.TSDCaseExplorer.prototype.inRefresch = function () {
    var _this = this.TParent();
    if (EnableControls) if (Refresch != null) _this.Refresch(this, null);
}


UsrCfg.SD.TSDCaseExplorer.prototype.GETSETCASEClient = function (inIDSDCASE, Recursivo) {
    var _this = this.TParent();
    _this.GETSETCASE(inIDSDCASE, Recursivo);
    _this.inRefresch();
    //updatate event;
}

UsrCfg.SD.TSDCaseExplorer.prototype.isEnableACTIVITIES_Client = function (inSDCASE, StrPend, StrOut) {
    var _this = this.TParent();
    var ResBool = false;
    StrPend.Value = "";
    try {
        for (var i = 0; i < LISTCASE.length; i++) {
            if (LISTCASE[i].SDCASECOMPLTE.IDSDCASE == inSDCASE) {
                if (StrOut == "") StrOut = "(TBLW." + UsrCfg.InternoAtisNames.SDCASE.IDSDCASE.FieldName + "<>" + LISTCASE[i].SDCASECOMPLTE.IDSDCASE + ")";
                else StrOut += "and (TBLW." + UsrCfg.InternoAtisNames.SDCASE.IDSDCASE.FieldName + "<>" + LISTCASE[i].SDCASECOMPLTE.IDSDCASE + ")";

                for (var x = 0; x < LISTCASE[i].LIST_CASE_ACTIVITIES.length; x++) {
                    if ((LISTCASE[i].LIST_CASE_ACTIVITIES[x].SDCASEACTIVITIESCOMPLTE.IDSDRUNNIGSTATUS == UsrCfg.SD.Properties.TIDSDRUNNIGSTATUS._RUNNING) ||
                        (LISTCASE[i].LIST_CASE_ACTIVITIES[x].SDCASEACTIVITIESCOMPLTE.IDSDRUNNIGSTATUS == UsrCfg.SD.Properties.TIDSDRUNNIGSTATUS._CREATE)) {
                        if (StrPend.Value == "") StrPend.Value = LISTCASE[i].LIST_CASE_ACTIVITIES[x].SDCASECOMPLTE.IDSDCASE.toString();
                        else StrPend.Value += ", " + LISTCASE[i].LIST_CASE_ACTIVITIES[x].SDCASECOMPLTE.IDSDCASE.toString();

                        ResBool = true;
                    }
                }
            }
        }
    }
    catch (Exception) {
        ResBool = true;//para indicar que no se sabe exacatmante;
        ResErr.NotError = false;
        ResErr.Mesaje = "isEnableACTIVITIES error";
        throw Exception;
    }
    return (ResBool);
}

