﻿//UsrCfg.SD




UsrCfg.SD.Properties.TCASE = function () {
    this.SDCASECOMPLTE = new UsrCfg.SD.Properties.TSDCASECOMPLTE();
    this.SDCASEMTCOMPLTE = new UsrCfg.SD.Properties.TSDCASEMTCOMPLTE();
    this.SDCASEACTIVITIESCOMPLTE = new UsrCfg.SD.Properties.TSDCASEACTIVITIESCOMPLTE();
    this.LIST_SDWHOTOCASECOMPLTE = new Array();//UsrCfg.SD.Properties.TSDWHOTOCASECOMPLTE
    //begin RELACIONES
    this.isLoad = false;
    this.isVisible = false;
    this.LIST_CASE_ACTIVITIES = new Array();//UsrCfg.SD.Properties.TCASE
    this.CASEParent = undefined;//new UsrCfg.SD.Properties.TCASE();
    this.LIST_CASE_CHILDREN = new Array();//UsrCfg.SD.Properties.TCASE
    this.isLoadWho = false;
    this.LIST_SDOPERATION_POST = new Array();//UsrCfg.SD.Properties.TSDOPERATION_POST
    this.LIST_SDOPERATION_FUNCESC = new Array();//UsrCfg.SD.Properties.TSDOPERATION_FUNCESC
    this.LIST_SDOPERATION_HIERESC = new Array();//UsrCfg.SD.Properties.TSDOPERATION_HIERESC
    this.LIST_SDOPERATION_PARENT = new Array();//UsrCfg.SD.Properties.TSDOPERATION_PARENT
    this.LIST_SDOPERATION_CASESTATUS = new Array();//UsrCfg.SD.Properties.TSDOPERATION_CASESTATUS
    this.LIST_SDOPERATION_CASEACTIVITIES = new Array();//UsrCfg.SD.Properties.TSDOPERATION_CASEACTIVITIES
    this.LIST_SDOPERATION_ATTENTION = new Array();//UsrCfg.SD.Properties.TSDOPERATION_ATTENTION
    //end  RELACIONES
}





UsrCfg.SD.TSDCaseExplorer = function () {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.SETCASE = new UsrCfg.SD.Properties.TCASE();
    this.ResErr = SysCfg.Error.Properties.TResErr();
    this.LISTCASE = new Array();//TCASE



    this.GETCASEWho = function (CASE) {
        if (!CASE.isLoadWho) {
            var StrIDSDWHOTOCASE = "";
            for (var i = 0; i < CASE.LIST_SDWHOTOCASECOMPLTE.length; i++) {
                if (StrIDSDWHOTOCASE == "") StrIDSDWHOTOCASE = CASE.LIST_SDWHOTOCASECOMPLTE[i].IDSDWHOTOCASE.toString();
                else StrIDSDWHOTOCASE += "," + CASE.LIST_SDWHOTOCASECOMPLTE[i].IDSDWHOTOCASE.toString();
            }
            _this.SDOPERATION_POST_ListFill(CASE, StrIDSDWHOTOCASE);
            _this.SDOPERATION_FUNCESC_ListFill(CASE, StrIDSDWHOTOCASE);
            _this.SDOPERATION_HIERESC_ListFill(CASE, StrIDSDWHOTOCASE);
            _this.SDOPERATION_PARENT_ListFill(CASE, StrIDSDWHOTOCASE);
            _this.SDOPERATION_CASESTATUS_ListFill(CASE, StrIDSDWHOTOCASE);
            //_this.SDOPERATION_CASEACTIVITIES_ListFill(CASE, StrIDSDWHOTOCASE);
            _this.SDOPERATION_ATTENTION_ListFill(CASE, StrIDSDWHOTOCASE);
        }
        CASE.isLoadWho = true;
    }
    //***********
    this.SDOPERATION_POST_ListFill = function (CASE, StrIDSDWHOTOCASE)
    {
        var Param = new SysCfg.Stream.Properties.TParam();
        try {
            Param.Inicialize();
            if (StrIDSDWHOTOCASE == "") {
                var IDSDWHOTOCASE = 0;
                Param.AddInt32(UsrCfg.InternoAtisNames.SDOPERATION_POST.IDSDWHOTOCASE.FieldName, IDSDWHOTOCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Normal);
            }
            else {
                StrIDSDWHOTOCASE = " IN (" + StrIDSDWHOTOCASE + ")";
                Param.AddUnknown(UsrCfg.InternoAtisNames.SDOPERATION_POST.IDSDWHOTOCASE.FieldName, StrIDSDWHOTOCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            }
            //*********** GET_GETID ***************************** 
            var DS_SDOPERATION_POST = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "LIST_SDOPERATION_POST", Param.ToBytes());
            ResErr = DS_SDOPERATION_POST.ResErr;
            if (ResErr.NotError) {
                if (DS_SDOPERATION_POST.DataSet.RecordCount > 0) {
                    DS_SDOPERATION_POST.DataSet.First();
                    while (!(DS_SDOPERATION_POST.DataSet.Eof)) {
                        var SDOPERATION_POST = new UsrCfg.SD.Properties.TSDOPERATION_POST();
                        SDOPERATION_POST.IDSDOPERATION_POST = DS_SDOPERATION_POST.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_POST.IDSDOPERATION_POST.FieldName).asInt32();
                        SDOPERATION_POST.IDSDWHOTOCASE = DS_SDOPERATION_POST.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_POST.IDSDWHOTOCASE.FieldName).asInt32();
                        SDOPERATION_POST.MESSAGE_DATE = DS_SDOPERATION_POST.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_POST.MESSAGE_DATE.FieldName).asDateTime();
                        SDOPERATION_POST.POST_IDSDTYPEUSER = DS_SDOPERATION_POST.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_POST.POST_IDSDTYPEUSER.FieldName).asInt32();
                        SDOPERATION_POST.POST_MESSAGE = DS_SDOPERATION_POST.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_POST.POST_MESSAGE.FieldName).asString();

                        //agrega a la lista principal
                        _this.SDOPERATION_POST_ListAdd(CASE.LIST_SDOPERATION_POST, SDOPERATION_POST);
                        //se agrega a la lista especifica del permiso                         
                        var Index = SDWHOTOCASECOMPLTE_ListGetIndexWho(CASE.LIST_SDWHOTOCASECOMPLTE, SDOPERATION_POST.IDSDWHOTOCASE);
                        _this.SDOPERATION_POST_ListAdd(CASE.LIST_SDWHOTOCASECOMPLTE[Index].LIST_SDOPERATION_POST, SDOPERATION_POST);

                        DS_SDOPERATION_POST.DataSet.Next();
                    }
                }
                else {
                    DS_SDOPERATION_POST.ResErr.NotError = false;
                    DS_SDOPERATION_POST.ResErr.Mesaje = "Record Count = 0";
                }
            }
        }
        finally {
            Param.Destroy();
        }
    }
    this.SDOPERATION_POST_ListAdd = function (SDOPERATION_POSTList, SDOPERATION_POST) {
        var i = _this.SDOPERATION_POST_ListGetIndex(SDOPERATION_POSTList, SDOPERATION_POST);
        if (i == -1) SDOPERATION_POSTList.push(SDOPERATION_POST);
        else SDOPERATION_POSTList[i] = SDOPERATION_POST;
    }
    this.SDOPERATION_POST_ListGetIndex = function (SDOPERATION_POSTList, SDOPERATION_POST) {

        for (var i = 0; i < SDOPERATION_POSTList.length; i++) {
            if (SDOPERATION_POST.IDSDOPERATION_POST == SDOPERATION_POSTList[i].IDSDOPERATION_POST)
                return (i);

        }
        return (-1);
    }
    //***********
    this.SDOPERATION_FUNCESC_ListFill = function (CASE, StrIDSDWHOTOCASE) {
        var Param = new SysCfg.Stream.Properties.TParam();
        try {
            Param.Inicialize();
            if (StrIDSDWHOTOCASE == "") {
                var IDSDWHOTOCASE = 0;
                Param.AddInt32(UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.IDSDWHOTOCASE.FieldName, IDSDWHOTOCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Normal);
            }
            else {
                StrIDSDWHOTOCASE = " IN (" + StrIDSDWHOTOCASE + ")";
                Param.AddUnknown(UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.IDSDWHOTOCASE.FieldName, StrIDSDWHOTOCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            }
            //*********** GET_GETID ***************************** 
            var DS_SDOPERATION_FUNCESC = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "LIST_SDOPERATION_FUNCESC", Param.ToBytes());
            ResErr = DS_SDOPERATION_FUNCESC.ResErr;
            if (ResErr.NotError) {
                if (DS_SDOPERATION_FUNCESC.DataSet.RecordCount > 0) {
                    DS_SDOPERATION_FUNCESC.DataSet.First();
                    while (!(DS_SDOPERATION_FUNCESC.DataSet.Eof)) {
                        var SDOPERATION_FUNCESC = new UsrCfg.SD.Properties.TSDOPERATION_FUNCESC();
                        SDOPERATION_FUNCESC.FUNCESC_APPLY = DS_SDOPERATION_FUNCESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.FUNCESC_APPLY.FieldName).asBoolean();
                        SDOPERATION_FUNCESC.FUNCESC_DATE = DS_SDOPERATION_FUNCESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.FUNCESC_DATE.FieldName).asDateTime();
                        SDOPERATION_FUNCESC.FUNCESC_IDCMDBCI = DS_SDOPERATION_FUNCESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.FUNCESC_IDCMDBCI.FieldName).asInt32();
                        SDOPERATION_FUNCESC.FUNCESC_LAVEL = DS_SDOPERATION_FUNCESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.FUNCESC_LAVEL.FieldName).asInt32();
                        SDOPERATION_FUNCESC.FUNCESC_MESSAGE = DS_SDOPERATION_FUNCESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.FUNCESC_MESSAGE.FieldName).asText();
                        SDOPERATION_FUNCESC.IDSDSCALETYPE_FUNC = UsrCfg.SD.Properties.TIDSDSCALETYPE_FUNC.GetEnum(DS_SDOPERATION_FUNCESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.IDSDSCALETYPE_FUNC.FieldName).asInt32());
                        //SDOPERATION_FUNCESC.IDSDOPERATION_ESCTYPE_FUNC = DS_SDOPERATION_FUNCESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.IDSDOPERATION_ESCTYPE_FUNC.FieldName).asInt32();
                        SDOPERATION_FUNCESC.IDSDOPERATION_FUNCESC = DS_SDOPERATION_FUNCESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.IDSDOPERATION_FUNCESC.FieldName).asInt32();
                        SDOPERATION_FUNCESC.IDSDWHOTOCASE = DS_SDOPERATION_FUNCESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_FUNCESC.IDSDWHOTOCASE.FieldName).asInt32();
                        //agrega a la lista principal
                        _this.SDOPERATION_FUNCESC_ListAdd(CASE.LIST_SDOPERATION_FUNCESC, SDOPERATION_FUNCESC);
                        //se agrega a la lista especifica del permiso                                                   
                        var Index = SDWHOTOCASECOMPLTE_ListGetIndexWho(CASE.LIST_SDWHOTOCASECOMPLTE, SDOPERATION_FUNCESC.IDSDWHOTOCASE);
                        _this.SDOPERATION_FUNCESC_ListAdd(CASE.LIST_SDWHOTOCASECOMPLTE[Index].LIST_SDOPERATION_FUNCESC, SDOPERATION_FUNCESC);

                        DS_SDOPERATION_FUNCESC.DataSet.Next();
                    }
                }
                else {
                    DS_SDOPERATION_FUNCESC.ResErr.NotError = false;
                    DS_SDOPERATION_FUNCESC.ResErr.Mesaje = "Record Count = 0";
                }
            }
        }
        finally {
            Param.Destroy();
        }
    }
    this.SDOPERATION_FUNCESC_ListAdd = function (SDOPERATION_FUNCESCList, SDOPERATION_FUNCESC) {
        var i = _this.SDOPERATION_FUNCESC_ListGetIndex(SDOPERATION_FUNCESCList, SDOPERATION_FUNCESC);
        if (i == -1) SDOPERATION_FUNCESCList.push(SDOPERATION_FUNCESC);
        else SDOPERATION_FUNCESCList[i] = SDOPERATION_FUNCESC;
    }
    this.SDOPERATION_FUNCESC_ListGetIndex = function (SDOPERATION_FUNCESCList, SDOPERATION_FUNCESC) {

        for (var i = 0; i < SDOPERATION_FUNCESCList.length; i++) {
            if (SDOPERATION_FUNCESC.IDSDOPERATION_FUNCESC == SDOPERATION_FUNCESCList[i].IDSDOPERATION_FUNCESC)
                return (i);

        }
        return (-1);
    }
    //***********
    this.SDOPERATION_HIERESC_ListFill = function (CASE, StrIDSDWHOTOCASE) {
        var Param = new SysCfg.Stream.Properties.TParam();
        try {
            Param.Inicialize();
            if (StrIDSDWHOTOCASE == "") {
                var IDSDWHOTOCASE = 0;
                Param.AddInt32(UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.IDSDWHOTOCASE.FieldName, IDSDWHOTOCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Normal);
            }
            else {
                StrIDSDWHOTOCASE = " IN (" + StrIDSDWHOTOCASE + ")";
                Param.AddUnknown(UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.IDSDWHOTOCASE.FieldName, StrIDSDWHOTOCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            }
            //*********** GET_GETID ***************************** 
            var DS_SDOPERATION_HIERESC = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "LIST_SDOPERATION_HIERESC", Param.ToBytes());
            ResErr = DS_SDOPERATION_HIERESC.ResErr;
            if (ResErr.NotError) {
                if (DS_SDOPERATION_HIERESC.DataSet.RecordCount > 0) {
                    DS_SDOPERATION_HIERESC.DataSet.First();
                    while (!(DS_SDOPERATION_HIERESC.DataSet.Eof)) {
                        SDOPERATION_HIERESC = new UsrCfg.SD.Properties.Properties.TSDOPERATION_HIERESC();
                        SDOPERATION_HIERESC.HIERESC_APPLY = DS_SDOPERATION_HIERESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.HIERESC_APPLY.FieldName).asBoolean();
                        SDOPERATION_HIERESC.HIERESC_DATE = DS_SDOPERATION_HIERESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.HIERESC_DATE.FieldName).asDateTime();
                        SDOPERATION_HIERESC.HIERESC_IDCMDBCI = DS_SDOPERATION_HIERESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.HIERESC_IDCMDBCI.FieldName).asInt32();
                        SDOPERATION_HIERESC.HIERESC_LAVEL = DS_SDOPERATION_HIERESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.HIERESC_LAVEL.FieldName).asInt32();
                        SDOPERATION_HIERESC.HIERESC_MESSAGE = DS_SDOPERATION_HIERESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.HIERESC_MESSAGE.FieldName).asText();
                        SDOPERATION_HIERESC.IDSDSCALETYPE_HIER = UsrCfg.SD.Properties.TIDSDSCALETYPE_HIER.GetEnum(DS_SDOPERATION_HIERESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.IDSDSCALETYPE_HIER.FieldName).asInt32());
                        //SDOPERATION_HIERESC.IDSDOPERATION_ESCTYPE_HIER = DS_SDOPERATION_HIERESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.IDSDOPERATION_ESCTYPE_HIER.FieldName).asInt32();
                        SDOPERATION_HIERESC.IDSDOPERATION_HIERESC = DS_SDOPERATION_HIERESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.IDSDOPERATION_HIERESC.FieldName).asInt32();
                        SDOPERATION_HIERESC.IDSDWHOTOCASE = DS_SDOPERATION_HIERESC.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_HIERESC.IDSDWHOTOCASE.FieldName).asInt32();
                        //agrega a la lista principal
                        _this.SDOPERATION_HIERESC_ListAdd(CASE.LIST_SDOPERATION_HIERESC, SDOPERATION_HIERESC);
                        //se agrega a la lista especifica del permiso                                                   
                        var Index = SDWHOTOCASECOMPLTE_ListGetIndexWho(CASE.LIST_SDWHOTOCASECOMPLTE, SDOPERATION_HIERESC.IDSDWHOTOCASE);
                        _this.SDOPERATION_HIERESC_ListAdd(CASE.LIST_SDWHOTOCASECOMPLTE[Index].LIST_SDOPERATION_HIERESC, SDOPERATION_HIERESC);

                        DS_SDOPERATION_HIERESC.DataSet.Next();
                    }
                }
                else {
                    DS_SDOPERATION_HIERESC.ResErr.NotError = false;
                    DS_SDOPERATION_HIERESC.ResErr.Mesaje = "Record Count = 0";
                }
            }
        }
        finally {
            Param.Destroy();
        }
    }
    this.SDOPERATION_HIERESC_ListAdd = function (SDOPERATION_HIERESCList, SDOPERATION_HIERESC) {
        var i = _this.SDOPERATION_HIERESC_ListGetIndex(SDOPERATION_HIERESCList, SDOPERATION_HIERESC);
        if (i == -1) SDOPERATION_HIERESCList.push(SDOPERATION_HIERESC);
        else SDOPERATION_HIERESCList[i] = SDOPERATION_HIERESC;
    }
    this.SDOPERATION_HIERESC_ListGetIndex = function (SDOPERATION_HIERESCList, SDOPERATION_HIERESC) {

        for (var i = 0; i < SDOPERATION_HIERESCList.length; i++) {
            if (SDOPERATION_HIERESC.IDSDOPERATION_HIERESC == SDOPERATION_HIERESCList[i].IDSDOPERATION_HIERESC)
                return (i);

        }
        return (-1);
    }
    //***********
    this.SDOPERATION_PARENT_ListFill = function (CASE, StrIDSDWHOTOCASE) {
        var Param = new SysCfg.Stream.Properties.TParam();
        try {
            Param.Inicialize();
            if (StrIDSDWHOTOCASE == "") {
                var IDSDWHOTOCASE = 0;
                Param.AddInt32(UsrCfg.InternoAtisNames.SDOPERATION_PARENT.IDSDWHOTOCASE.FieldName, IDSDWHOTOCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Normal);
            }
            else {
                StrIDSDWHOTOCASE = " IN (" + StrIDSDWHOTOCASE + ")";
                Param.AddUnknown(UsrCfg.InternoAtisNames.SDOPERATION_PARENT.IDSDWHOTOCASE.FieldName, StrIDSDWHOTOCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            }
            //*********** GET_GETID ***************************** 
            var DS_SDOPERATION_PARENT = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "LIST_SDOPERATION_PARENT", Param.ToBytes());
            ResErr = DS_SDOPERATION_PARENT.ResErr;
            if (ResErr.NotError) {
                if (DS_SDOPERATION_PARENT.DataSet.RecordCount > 0) {
                    DS_SDOPERATION_PARENT.DataSet.First();
                    while (!(DS_SDOPERATION_PARENT.DataSet.Eof)) {
                        var SDOPERATION_PARENT = new UsrCfg.SD.Properties.TSDOPERATION_PARENT();
                        SDOPERATION_PARENT.IDSDOPERATION_PARENT = DS_SDOPERATION_PARENT.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_PARENT.IDSDOPERATION_PARENT.FieldName).asInt32();
                        //SDOPERATION_PARENT.IDSDOPERATION_PARENTTYPE = DS_SDOPERATION_PARENT.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_PARENT.IDSDOPERATION_PARENTTYPE.FieldName).asInt32();
                        SDOPERATION_PARENT.IDSDCASE = DS_SDOPERATION_PARENT.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_PARENT.IDSDCASE.FieldName).asInt32();
                        SDOPERATION_PARENT.IDSDCASE_PARENT = DS_SDOPERATION_PARENT.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_PARENT.IDSDCASE_PARENT.FieldName).asInt32();
                        SDOPERATION_PARENT.IDSDWHOTOCASE = DS_SDOPERATION_PARENT.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_PARENT.IDSDWHOTOCASE.FieldName).asInt32();
                        SDOPERATION_PARENT.PARENT_DATE = DS_SDOPERATION_PARENT.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_PARENT.PARENT_DATE.FieldName).asDateTime();
                        SDOPERATION_PARENT.PARENT_MESSAGE = DS_SDOPERATION_PARENT.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_PARENT.PARENT_MESSAGE.FieldName).asText();
                        //agrega a la lista principal
                        _this.SDOPERATION_PARENT_ListAdd(CASE.LIST_SDOPERATION_PARENT, SDOPERATION_PARENT);
                        //se agrega a la lista especifica del permiso                                               
                        var Index = SDWHOTOCASECOMPLTE_ListGetIndexWho(CASE.LIST_SDWHOTOCASECOMPLTE, SDOPERATION_PARENT.IDSDWHOTOCASE);
                        _this.SDOPERATION_PARENT_ListAdd(CASE.LIST_SDWHOTOCASECOMPLTE[Index].LIST_SDOPERATION_PARENT, SDOPERATION_PARENT);

                        DS_SDOPERATION_PARENT.DataSet.Next();
                    }
                }
                else {
                    DS_SDOPERATION_PARENT.ResErr.NotError = false;
                    DS_SDOPERATION_PARENT.ResErr.Mesaje = "Record Count = 0";
                }
            }
        }
        finally {
            Param.Destroy();
        }
    }
    this.SDOPERATION_PARENT_ListAdd = function (SDOPERATION_PARENTList, SDOPERATION_PARENT) {
        var i = _this.SDOPERATION_PARENT_ListGetIndex(SDOPERATION_PARENTList, SDOPERATION_PARENT);
        if (i == -1) SDOPERATION_PARENTList.push(SDOPERATION_PARENT);
        else SDOPERATION_PARENTList[i] = SDOPERATION_PARENT;
    }
    this.SDOPERATION_PARENT_ListGetIndex = function(SDOPERATION_PARENTList, SDOPERATION_PARENT)
    {

        for (var i = 0; i < SDOPERATION_PARENTList.length; i++) {
            if (SDOPERATION_PARENT.IDSDOPERATION_PARENT == SDOPERATION_PARENTList[i].IDSDOPERATION_PARENT)
                return (i);

        }
        return (-1);
    }
    //***********
    this.SDOPERATION_CASESTATUS_ListFill = function (CASE, StrIDSDWHOTOCASE) {
        var Param = new SysCfg.Stream.Properties.TParam();
        try {
            Param.Inicialize();
            if (StrIDSDWHOTOCASE == "") {
                var IDSDWHOTOCASE = 0;
                Param.AddInt32(UsrCfg.InternoAtisNames.SDOPERATION_CASESTATUS.IDSDWHOTOCASE.FieldName, IDSDWHOTOCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Normal);
            }
            else {
                StrIDSDWHOTOCASE = " IN (" + StrIDSDWHOTOCASE + ")";
                Param.AddUnknown(UsrCfg.InternoAtisNames.SDOPERATION_CASESTATUS.IDSDWHOTOCASE.FieldName, StrIDSDWHOTOCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            }
            //*********** GET_GETID ***************************** 
            var DS_SDOPERATION_CASESTATUS = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "LIST_SDOPERATION_CASESTATUS", Param.ToBytes());
            ResErr = DS_SDOPERATION_CASESTATUS.ResErr;
            if (ResErr.NotError) {
                if (DS_SDOPERATION_CASESTATUS.DataSet.RecordCount > 0) {
                    DS_SDOPERATION_CASESTATUS.DataSet.First();
                    while (!(DS_SDOPERATION_CASESTATUS.DataSet.Eof)) {
                        var SDOPERATION_CASESTATUS = new UsrCfg.SD.Properties.TSDOPERATION_CASESTATUS();
                        SDOPERATION_CASESTATUS.IDSDOPERATION_CASESTATUS = DS_SDOPERATION_CASESTATUS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_CASESTATUS.IDSDOPERATION_CASESTATUS.FieldName).asInt32();
                        //SDOPERATION_CASESTATUS.IDSDCASESTATUS = DS_SDOPERATION_CASESTATUS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_CASESTATUS.IDSDCASESTATUS.FieldName).asInt32();
                        SDOPERATION_CASESTATUS.IDSDWHOTOCASE = DS_SDOPERATION_CASESTATUS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_CASESTATUS.IDSDWHOTOCASE.FieldName).asInt32();
                        SDOPERATION_CASESTATUS.CASESTATUS_DATE = DS_SDOPERATION_CASESTATUS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_CASESTATUS.CASESTATUS_DATE.FieldName).asDateTime();
                        SDOPERATION_CASESTATUS.CASESTATUS_MESSAGE = DS_SDOPERATION_CASESTATUS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_CASESTATUS.CASESTATUS_MESSAGE.FieldName).asText();
                        //agrega a la lista principal
                        _this.SDOPERATION_CASESTATUS_ListAdd(CASE.LIST_SDOPERATION_CASESTATUS, SDOPERATION_CASESTATUS);
                        //se agrega a la lista especifica del permiso                                                
                        var Index = SDWHOTOCASECOMPLTE_ListGetIndexWho(CASE.LIST_SDWHOTOCASECOMPLTE, SDOPERATION_CASESTATUS.IDSDWHOTOCASE);
                        _this.SDOPERATION_CASESTATUS_ListAdd(CASE.LIST_SDWHOTOCASECOMPLTE[Index].LIST_SDOPERATION_CASESTATUS, SDOPERATION_CASESTATUS);

                        DS_SDOPERATION_CASESTATUS.DataSet.Next();
                    }
                }
                else {
                    DS_SDOPERATION_CASESTATUS.ResErr.NotError = false;
                    DS_SDOPERATION_CASESTATUS.ResErr.Mesaje = "Record Count = 0";
                }
            }
        }
        finally {
            Param.Destroy();
        }
    }
    this.SDOPERATION_CASESTATUS_ListAdd = function (SDOPERATION_CASESTATUSList, SDOPERATION_CASESTATUS) {
        var i = _this.SDOPERATION_CASESTATUS_ListGetIndex(SDOPERATION_CASESTATUSList, SDOPERATION_CASESTATUS);
        if (i == -1) SDOPERATION_CASESTATUSList.push(SDOPERATION_CASESTATUS);
        else SDOPERATION_CASESTATUSList[i] = SDOPERATION_CASESTATUS;
    }
    this.SDOPERATION_CASESTATUS_ListGetIndex = function (SDOPERATION_CASESTATUSList, SDOPERATION_CASESTATUS) {
        for (var i = 0; i < SDOPERATION_CASESTATUSList.length; i++) {
            if (SDOPERATION_CASESTATUS.IDSDOPERATION_CASESTATUS == SDOPERATION_CASESTATUSList[i].IDSDOPERATION_CASESTATUS)
                return (i);
        }
        return (-1);
    }
    //***********
    this.SDOPERATION_CASEACTIVITIES_ListFill = function (CASE, StrIDSDWHOTOCASE) {
        var Param = new SysCfg.Stream.Properties.TParam();
        try {
            Param.Inicialize();
            if (StrIDSDWHOTOCASE == "") {
                var IDSDWHOTOCASE = 0;
                Param.AddInt32(UsrCfg.InternoAtisNames.SDOPERATION_CASEACTIVITIES.IDSDWHOTOCASE.FieldName, IDSDWHOTOCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Normal);
            }
            else {
                StrIDSDWHOTOCASE = " IN (" + StrIDSDWHOTOCASE + ")";
                Param.AddUnknown(UsrCfg.InternoAtisNames.SDOPERATION_CASEACTIVITIES.IDSDWHOTOCASE.FieldName, StrIDSDWHOTOCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            }
            //*********** GET_GETID ***************************** 
            var DS_SDOPERATION_CASEACTIVITIES = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "LIST_SDOPERATION_CASEACTIVITIES", Param.ToBytes());
            ResErr = DS_SDOPERATION_CASEACTIVITIES.ResErr;
            if (ResErr.NotError) {
                if (DS_SDOPERATION_CASEACTIVITIES.DataSet.RecordCount > 0) {
                    DS_SDOPERATION_CASEACTIVITIES.DataSet.First();
                    while (!(DS_SDOPERATION_CASEACTIVITIES.DataSet.Eof)) {
                        var SDOPERATION_CASEACTIVITIES = new UsrCfg.SD.Properties.TSDOPERATION_CASEACTIVITIES();
                        SDOPERATION_CASEACTIVITIES.IDSDOPERATION_CASEACTIVITIES = DS_SDOPERATION_CASEACTIVITIES.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_CASEACTIVITIES.IDSDOPERATION_CASEACTIVITIES.FieldName).asInt32();
                        //SDOPERATION_CASEACTIVITIES.IDSDRUNNIGSTATUS = DS_SDOPERATION_CASEACTIVITIES.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_CASEACTIVITIES.IDSDRUNNIGSTATUS.FieldName).asInt32();
                        SDOPERATION_CASEACTIVITIES.IDSDWHOTOCASE = DS_SDOPERATION_CASEACTIVITIES.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_CASEACTIVITIES.IDSDWHOTOCASE.FieldName).asInt32();
                        SDOPERATION_CASEACTIVITIES.CASEACTIVITIES_DATE = DS_SDOPERATION_CASEACTIVITIES.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_CASEACTIVITIES.CASEACTIVITIES_DATE.FieldName).asDateTime();
                        SDOPERATION_CASEACTIVITIES.CASEACTIVITIES_MESSAGE = DS_SDOPERATION_CASEACTIVITIES.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_CASEACTIVITIES.CASEACTIVITIES_MESSAGE.FieldName).asText();
                        //agrega a la lista principal
                        _this.SDOPERATION_CASEACTIVITIES_ListAdd(CASE.LIST_SDOPERATION_CASEACTIVITIES, SDOPERATION_CASEACTIVITIES);
                        //se agrega a la lista especifica del permiso                         
                        var Index = SDWHOTOCASECOMPLTE_ListGetIndexWho(CASE.LIST_SDWHOTOCASECOMPLTE, SDOPERATION_CASEACTIVITIES.IDSDWHOTOCASE);
                        _this.SDOPERATION_CASEACTIVITIES_ListAdd(CASE.LIST_SDWHOTOCASECOMPLTE[Index].LIST_SDOPERATION_CASEACTIVITIES, SDOPERATION_CASEACTIVITIES);

                        DS_SDOPERATION_CASEACTIVITIES.DataSet.Next();
                    }
                }
                else {
                    DS_SDOPERATION_CASEACTIVITIES.ResErr.NotError = false;
                    DS_SDOPERATION_CASEACTIVITIES.ResErr.Mesaje = "Record Count = 0";
                }
            }
        }
        finally {
            Param.Destroy();
        }
    }
    this.SDOPERATION_CASEACTIVITIES_ListAdd = function (SDOPERATION_CASEACTIVITIESList, SDOPERATION_CASEACTIVITIES) {
        var i = _this.SDOPERATION_CASEACTIVITIES_ListGetIndex(SDOPERATION_CASEACTIVITIESList, SDOPERATION_CASEACTIVITIES);
        if (i == -1) SDOPERATION_CASEACTIVITIESList.push(SDOPERATION_CASEACTIVITIES);
        else SDOPERATION_CASEACTIVITIESList[i] = SDOPERATION_CASEACTIVITIES;
    }
    this.SDOPERATION_CASEACTIVITIES_ListGetIndex = function (SDOPERATION_CASEACTIVITIESList, SDOPERATION_CASEACTIVITIES) {

        for (var i = 0; i < SDOPERATION_CASEACTIVITIESList.length; i++) {
            if (SDOPERATION_CASEACTIVITIES.IDSDOPERATION_CASEACTIVITIES == SDOPERATION_CASEACTIVITIESList[i].IDSDOPERATION_CASEACTIVITIES)
                return (i);

        }
        return (-1);
    }
    //***********
    this.SDOPERATION_ATTENTION_ListFill = function(CASE, StrIDSDWHOTOCASE)
    {
        var Param = new SysCfg.Stream.Properties.TParam();
        try {
            Param.Inicialize();
            if (StrIDSDWHOTOCASE == "") {
                var IDSDWHOTOCASE = 0;
                Param.AddInt32(UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.IDSDWHOTOCASE.FieldName, IDSDWHOTOCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Normal);
            }
            else {
                StrIDSDWHOTOCASE = " IN (" + StrIDSDWHOTOCASE + ")";
                Param.AddUnknown(UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.IDSDWHOTOCASE.FieldName, StrIDSDWHOTOCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            }
            //*********** GET_GETID ***************************** 
            var DS_SDOPERATION_ATTENTION = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "LIST_SDOPERATION_ATTENTION", Param.ToBytes());
            ResErr = DS_SDOPERATION_ATTENTION.ResErr;
            if (ResErr.NotError) {
                if (DS_SDOPERATION_ATTENTION.DataSet.RecordCount > 0) {
                    DS_SDOPERATION_ATTENTION.DataSet.First();
                    while (!(DS_SDOPERATION_ATTENTION.DataSet.Eof)) {
                        var SDOPERATION_ATTENTION = new UsrCfg.SD.Properties.TSDOPERATION_ATTENTION();

                        SDOPERATION_ATTENTION.ATTENTION_DATE = DS_SDOPERATION_ATTENTION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.ATTENTION_DATE.FieldName).asDateTime();
                        SDOPERATION_ATTENTION.ATTENTION_MESSAGE = DS_SDOPERATION_ATTENTION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.ATTENTION_MESSAGE.FieldName).asText();
                        SDOPERATION_ATTENTION.IDSDOPERATION_ATTENTION = DS_SDOPERATION_ATTENTION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.IDSDOPERATION_ATTENTION.FieldName).asInt32();
                        //SDOPERATION_ATTENTION.IDSDOPERATION_ATTENTIONTYPE = DS_SDOPERATION_ATTENTION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.IDSDOPERATION_ATTENTIONTYPE.FieldName).asInt32();
                        SDOPERATION_ATTENTION.IDSDWHOTOCASE = DS_SDOPERATION_ATTENTION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.IDSDWHOTOCASE.FieldName).asInt32();
                        SDOPERATION_ATTENTION.CASEMT_SET_LS_NAMESTEP = DS_SDOPERATION_ATTENTION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.CASEMT_SET_LS_NAMESTEP.FieldName).asString();
                        SDOPERATION_ATTENTION.CASEMT_SET_LS_STATUSN = DS_SDOPERATION_ATTENTION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.CASEMT_SET_LS_STATUSN.FieldName).asInt32();
                        SDOPERATION_ATTENTION.LS_IDSDCASESTATUS = UsrCfg.SD.Properties.TSDCaseStatus.GetEnum(DS_SDOPERATION_ATTENTION.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDOPERATION_ATTENTION.LS_IDSDCASESTATUS.FieldName).asInt32());
                        //agrega a la lista principal
                        _this.SDOPERATION_ATTENTION_ListAdd(CASE.LIST_SDOPERATION_ATTENTION, SDOPERATION_ATTENTION);
                        //se agrega a la lista especifica del permiso                                                     
                        var Index = SDWHOTOCASECOMPLTE_ListGetIndexWho(CASE.LIST_SDWHOTOCASECOMPLTE, SDOPERATION_ATTENTION.IDSDWHOTOCASE);
                        _this.SDOPERATION_ATTENTION_ListAdd(CASE.LIST_SDWHOTOCASECOMPLTE[Index].LIST_SDOPERATION_ATTENTION, SDOPERATION_ATTENTION);

                        DS_SDOPERATION_ATTENTION.DataSet.Next();
                    }
                }
                else {
                    DS_SDOPERATION_ATTENTION.ResErr.NotError = false;
                    DS_SDOPERATION_ATTENTION.ResErr.Mesaje = "Record Count = 0";
                }
            }
        }
        finally {
            Param.Destroy();
        }
    }
    this.SDWHOTOCASECOMPLTE_ListGetIndexWho = function (SDWHOTOCASECOMPLTEList, IDSDWHOTOCASE) {

        for (var i = 0; i < SDWHOTOCASECOMPLTEList.length; i++) {
            if (SDWHOTOCASECOMPLTEList[i].IDSDWHOTOCASE == IDSDWHOTOCASE)
                return (i);

        }
        return (-1);
    }
    this.SDOPERATION_ATTENTION_ListAdd = function (SDOPERATION_ATTENTIONList, SDOPERATION_ATTENTION) {
        var i = _this.SDOPERATION_ATTENTION_ListGetIndex(SDOPERATION_ATTENTIONList, SDOPERATION_ATTENTION);
        if (i == -1) SDOPERATION_ATTENTIONList.push(SDOPERATION_ATTENTION);
        else SDOPERATION_ATTENTIONList[i] = SDOPERATION_ATTENTION;
    }
    this.SDOPERATION_ATTENTION_ListGetIndex = function (SDOPERATION_ATTENTIONList, SDOPERATION_ATTENTION) {

        for (var i = 0; i < SDOPERATION_ATTENTIONList.length; i++) {
            if (SDOPERATION_ATTENTION.IDSDOPERATION_ATTENTION == SDOPERATION_ATTENTIONList[i].IDSDOPERATION_ATTENTION)
                return (i);

        }
        return (-1);
    }

    //***********

    this.GETSETCASE = function (inIDSDCASE, Recursivo) {        
        var SETCASE = _this.GETCASEFROMLIST(inIDSDCASE, Recursivo);
        if (!Recursivo) if (!SETCASE.isLoad) _this.GETRELATIONSCASE(SETCASE, Recursivo);
    }
    this.GETRELATIONSCASE = function (inCASE, Recursivo) {
        //********* CASEParent *****************************************
        if (inCASE.SDCASECOMPLTE.IDSDCASE_PARENT > 0) {
            inCASE.CASEParent = _this.GETCASEFROMLIST(inCASE.SDCASECOMPLTE.IDSDCASE_PARENT, Recursivo);
        }


        var MDLIFESTATUSProfiler = new UsrCfg.SD.TMDLIFESTATUSProfiler();
        MDLIFESTATUSProfiler.StrtoLIFESTATUS(inCASE.SDCASEMTCOMPLTE.CASEMT_LIFESTATUS);

        var SDCASETIMERCOUNTProfiler = new UsrCfg.SD.SDCaseTimerCount.TSDCASETIMERCOUNTProfiler();
        SDCASETIMERCOUNTProfiler.StrtoTIMERCOUNT(inCASE.SDCASEMTCOMPLTE.CASEMT_TIMERCOUNT);


        //********* LIST_CASE_ACTIVITIES *****************************************
        var Param = new SysCfg.Stream.Properties.TParam();
        try {
            Param.Inicialize();
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDLIFESTATUS.NAME_TABLE, MDLIFESTATUSProfiler.LIFESTATUStoSQL(inCASE.SDCASEMTCOMPLTE.MT_IDMDMODELTYPED), SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            Param.AddInt32(UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDSDCASEMT_ATVPARENT.FieldName, inCASE.SDCASEMTCOMPLTE.IDSDCASEMT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            Param.AddInt32(UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDSDCASE_ATVPARENT.FieldName, inCASE.SDCASECOMPLTE.IDSDCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            var DS_SDCASEACTIVITIES = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MATRIXOFACTIVITIES", Param.ToBytes());
            ResErr = DS_SDCASEACTIVITIES.ResErr;
            if (ResErr.NotError) {
                if (DS_SDCASEACTIVITIES.DataSet.RecordCount > 0) {
                    DS_SDCASEACTIVITIES.DataSet.First();
                    while (!(DS_SDCASEACTIVITIES.DataSet.Eof)) {
                        if (UsrCfg.SD.Properties.TIDSDRUNNIGSTATUS.GetEnum(DS_SDCASEACTIVITIES.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDSDRUNNIGSTATUS.FieldName).asInt32()) !=
                             UsrCfg.SD.Properties.TIDSDRUNNIGSTATUS._MODEL) {
                            var UnCASE_ACTIVITIES = _this.GETCASEFROMLIST(DS_SDCASEACTIVITIES.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDSDCASE.FieldName).asInt32(), Recursivo);
                            inCASE.LIST_CASE_ACTIVITIES.push(UnCASE_ACTIVITIES);
                        }
                        DS_SDCASEACTIVITIES.DataSet.Next();
                    }
                }
            }
        }
        finally {
            Param.Destroy();
        }

        //********* LIST_CASE_CHILDREN *****************************************
        try {
            Param = new SysCfg.Stream.Properties.TParam();
            Param.Inicialize();
            Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE.IDSDCASE_PARENT.FieldName, inCASE.SDCASECOMPLTE.IDSDCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            //*********** SDCASE_GET ***************************** 
            var DS_SDCASE_PARENT = SysCfg.DB.SQL.Methods.OpenDataSet("DataLink", "SDCASE_GET_PARENT", Param.ToBytes());
            ResErr = DS_SDCASE_PARENT.ResErr;
            if (ResErr.NotError) {
                if (DS_SDCASE_PARENT.DataSet.RecordCount > 0) {
                    DS_SDCASE_PARENT.DataSet.First();
                    while (!(DS_SDCASE_PARENT.DataSet.Eof)) {
                        var UnCASE_CHILDREN = _this.GETCASEFROMLIST(DS_SDCASE_PARENT.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASE.IDSDCASE.FieldName).asInt32(), Recursivo);
                        inCASE.LIST_CASE_CHILDREN.push(UnCASE_CHILDREN);
                        DS_SDCASE_PARENT.DataSet.Next();
                    }
                }
            }
        }
        finally {
            Param.Destroy();
        }
        inCASE.isLoad = true;
    }
    this.GETCASEFROMLIST = function (IDSDCASE, Recursivo) {
        for (var i = 0; i < _this.LISTCASE.length; i++) {
            if (_this.LISTCASE[i].SDCASECOMPLTE.IDSDCASE == IDSDCASE) {
                return _this.LISTCASE[i];
            }
        }
        UnCASE = new UsrCfg.SD.Properties.TCASE();
        UnCASE.SDCASECOMPLTE.IDSDCASE = IDSDCASE;
        _this.GETCASE(UnCASE);
        if (Recursivo) _this.GETRELATIONSCASE(UnCASE, Recursivo);
        return UnCASE;
    }
    this.GETCASE = function (inCASE) {
        var Param = new SysCfg.Stream.Properties.TParam();
        try {
            Param.Inicialize();
            Param.AddUnknown(UsrCfg.InternoAtisNames.SDCASE.IDHANDLER.FieldName, UsrCfg.InternoAtisNames.SDCASE.IDHANDLER.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            Param.AddUnknown(UsrCfg.InternoAtisNames.SDCASE.IDMANAGERSINFORMED.FieldName, UsrCfg.InternoAtisNames.SDCASE.IDMANAGERSINFORMED.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            Param.AddUnknown(UsrCfg.InternoAtisNames.SDCASE.IDOWNER.FieldName, UsrCfg.InternoAtisNames.SDCASE.IDOWNER.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE.IDSDCASE.FieldName, inCASE.SDCASECOMPLTE.IDSDCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            var StrIDSDCASESTATUS = " ";
            //String StrIDSDCASESTATUS = "(" + UsrCfg.InternoAtisNames.SDCASESTATUS.NAME_TABLE + "." + UsrCfg.InternoAtisNames.SDCASESTATUS.IDSDCASESTATUS.FieldName + "<>5) and (" + UsrCfg.InternoAtisNames.SDCASESTATUS.NAME_TABLE + "." + UsrCfg.InternoAtisNames.SDCASESTATUS.IDSDCASESTATUS.FieldName + "<>6) and ";
            Param.AddUnknown(UsrCfg.InternoAtisNames.SDCASE.IDSDCASESTATUS.FieldName, StrIDSDCASESTATUS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            var DS_SDCASECONSOLE = SysCfg.DB.SQL.Methods.OpenDataSet("DataLink", "GETSDCASECONSOLE", Param.ToBytes());
            ResErr = DS_SDCASECONSOLE.ResErr;
            if ((DS_SDCASECONSOLE.ResErr.NotError) && (DS_SDCASECONSOLE.DataSet.RecordCount > 0)) {
                DS_SDCASECONSOLE.DataSet.First();
                inCASE.SDCASECOMPLTE.IDSDCASE = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASE.IDSDCASE.FieldName).asInt32();
                inCASE.SDCASECOMPLTE.IDSDCASE_PARENT = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASE.IDSDCASE_PARENT.FieldName).asInt32();
                inCASE.SDCASECOMPLTE.IDSDCASESTATUS = UsrCfg.SD.Properties.TSDCaseStatus.GetEnum(DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASE.IDSDCASESTATUS.FieldName).asInt32());
                inCASE.SDCASECOMPLTE.CASESTATUSNAME = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName("CASESTATUSNAME").asString();
                inCASE.SDCASECOMPLTE.CASE_ISMAYOR = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASE.CASE_ISMAYOR.FieldName).asBoolean();
                inCASE.SDCASECOMPLTE.CASE_TITLE = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASE.CASE_TITLE.FieldName).asString();
                inCASE.SDCASECOMPLTE.CASE_COUNTTIME = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASE.CASE_COUNTTIME.FieldName).asInt32();
                inCASE.SDCASECOMPLTE.CASE_COUNTTIMEPAUSE = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASE.CASE_COUNTTIMEPAUSE.FieldName).asInt32();
                inCASE.SDCASECOMPLTE.CASE_COUNTTIMERESOLVED = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASE.CASE_COUNTTIMERESOLVED.FieldName).asInt32();
                inCASE.SDCASECOMPLTE.CASE_DATERESOLVED = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASE.CASE_DATERESOLVED.FieldName).asDateTime();
                inCASE.SDCASECOMPLTE.CASE_DATECLOSED = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASE.CASE_DATECLOSED.FieldName).asDateTime();
                inCASE.SDCASECOMPLTE.CASE_DATELASTCUT = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASE.CASE_DATELASTCUT.FieldName).asDateTime();
                inCASE.SDCASECOMPLTE.IDSDCASESOURCETYPE =UsrCfg.SD.Properties.TIDSDCASESOURCETYPE.GetEnum(DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASE.IDSDCASESOURCETYPE.FieldName).asInt32());
                inCASE.SDCASECOMPLTE.IDCMDBCONTACTTYPE_USER = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASE.IDCMDBCONTACTTYPE_USER.FieldName).asInt32();
                inCASE.SDCASECOMPLTE.IDMDCATEGORYDETAIL_INITIAL = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASE.IDMDCATEGORYDETAIL_INITIAL.FieldName).asInt32();
                inCASE.SDCASECOMPLTE.IDMDCATEGORYDETAIL_FINAL = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASE.IDMDCATEGORYDETAIL_FINAL.FieldName).asInt32();
                inCASE.SDCASECOMPLTE.CASE_DATESTART = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASE.CASE_DATESTART.FieldName).asDateTime();
                inCASE.SDCASECOMPLTE.CASE_DESCRIPTION = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASE.CASE_DESCRIPTION.FieldName).asString();
                inCASE.SDCASECOMPLTE.CASE_FINALSUMM = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASE.CASE_FINALSUMM.FieldName).asString();
                inCASE.SDCASECOMPLTE.CASE_RETURN_COST = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASE.CASE_RETURN_COST.FieldName).asDouble();
                inCASE.SDCASECOMPLTE.CASE_RETURN_STR = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASE.CASE_RETURN_STR.FieldName).asString();
                inCASE.SDCASECOMPLTE.IDHANDLER = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASE.IDHANDLER.FieldName).asInt32();
                inCASE.SDCASECOMPLTE.HANDLERNAME = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName("HANDLERNAME").asString();
                inCASE.SDCASECOMPLTE.IDOWNER = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASE.IDOWNER.FieldName).asInt32();
                inCASE.SDCASECOMPLTE.OWNERNAME = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName("OWNERNAME").asString();
                inCASE.SDCASECOMPLTE.IDMANAGERSINFORMED = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASE.IDMANAGERSINFORMED.FieldName).asInt32();
                inCASE.SDCASECOMPLTE.MANAGERSINFORMEDNAME = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName("MANAGERSINFORMEDNAME").asString();
                inCASE.SDCASECOMPLTE.IDUSER = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASE.IDUSER.FieldName).asInt32();
                inCASE.SDCASECOMPLTE.USERNAME = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName("USERNAME").asString();
                inCASE.SDCASECOMPLTE.IDSDCASEEF = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEEF.IDSDCASEEF.FieldName).asInt32();


                inCASE.SDCASEMTCOMPLTE.IDSDCASE = inCASE.SDCASECOMPLTE.IDSDCASE;
                inCASE.SDCASEMTCOMPLTE.IDMDCATEGORYDETAIL = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.IDMDCATEGORYDETAIL.FieldName).asInt32();
                inCASE.SDCASEMTCOMPLTE.CATEGORY = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName("CATEGORY").asString();
                inCASE.SDCASEMTCOMPLTE.CATEGORYNAME = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName("CATEGORYNAME").asString();
                inCASE.SDCASEMTCOMPLTE.IDMDPRIORITY = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.IDMDPRIORITY.FieldName).asInt32();
                inCASE.SDCASEMTCOMPLTE.PRIORITYNAME = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName("PRIORITYNAME").asString();
                inCASE.SDCASEMTCOMPLTE.IDSDCASEMT = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASEMT.FieldName).asInt32();
                inCASE.SDCASEMTCOMPLTE.IDSDCASEMTSTATUS = UsrCfg.SD.Properties.TSDCASEMTSTATUS.GetEnum(DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASEMTSTATUS.FieldName).asInt32());
                inCASE.SDCASEMTCOMPLTE.CASEMTSTATUSNAME = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName("CASEMTSTATUSNAME").asString();
                inCASE.SDCASEMTCOMPLTE.CASEMT_DATECREATE = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_DATECREATE.FieldName).asDateTime();
                inCASE.SDCASEMTCOMPLTE.CASEMT_SET_FUNLAVEL = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_FUNLAVEL.FieldName).asInt32();
                inCASE.SDCASEMTCOMPLTE.CASEMT_SET_HIERLAVEL = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_HIERLAVEL.FieldName).asInt32();
                inCASE.SDCASEMTCOMPLTE.CASEMT_SET_LS_NAMESTEP = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_LS_NAMESTEP.FieldName).asString();
                inCASE.SDCASEMTCOMPLTE.CASEMT_SET_LS_STATUSN = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_LS_STATUSN.FieldName).asInt32();
                inCASE.SDCASEMTCOMPLTE.CASEMT_SET_LS_NEXTSTEP = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_LS_NEXTSTEP.FieldName).asString();
                inCASE.SDCASEMTCOMPLTE.CASEMT_SET_LS_COMMENTSST = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_LS_COMMENTSST.FieldName).asString();
                inCASE.SDCASEMTCOMPLTE.IDSLA = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.IDSLA.FieldName).asInt32();
                inCASE.SDCASEMTCOMPLTE.SLANAME = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName("SLANAME").asString();
                inCASE.SDCASEMTCOMPLTE.MT_SERVICETYPENAME = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName("MT_SERVICETYPENAME").asString();
                inCASE.SDCASEMTCOMPLTE.IDMDIMPACT = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.IDMDIMPACT.FieldName).asInt32();
                inCASE.SDCASEMTCOMPLTE.IDMDURGENCY = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.IDMDURGENCY.FieldName).asInt32();
                inCASE.SDCASEMTCOMPLTE.IDSDWHOTOCASECANCELLED = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.IDSDWHOTOCASECANCELLED.FieldName).asInt32();
                inCASE.SDCASEMTCOMPLTE.MT_COMMENTSM = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.MT_COMMENTSM.FieldName).asString();
                inCASE.SDCASEMTCOMPLTE.MT_GUIDETEXT = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.MT_GUIDETEXT.FieldName).asString();
                inCASE.SDCASEMTCOMPLTE.MT_IDMDFUNCESC = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.MT_IDMDFUNCESC.FieldName).asInt32();
                inCASE.SDCASEMTCOMPLTE.MT_IDMDHIERESC = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.MT_IDMDHIERESC.FieldName).asInt32();
                inCASE.SDCASEMTCOMPLTE.MT_IDMDMODELTYPED = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.MT_IDMDMODELTYPED.FieldName).asInt32();

                inCASE.SDCASEMTCOMPLTE.MT_IDMDSERVICETYPE = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.MT_IDMDSERVICETYPE.FieldName).asInt32();
                inCASE.SDCASEMTCOMPLTE.MT_MAXTIME = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.MT_MAXTIME.FieldName).asInt32();
                inCASE.SDCASEMTCOMPLTE.MT_NORMALTIME = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.MT_NORMALTIME.FieldName).asInt32();
                inCASE.SDCASEMTCOMPLTE.IDSDSCALETYPE_FUNC = UsrCfg.SD.Properties.TIDSDSCALETYPE_FUNC.GetEnum(DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.IDSDSCALETYPE_FUNC.FieldName).asInt32());
                inCASE.SDCASEMTCOMPLTE.IDSDSCALETYPE_HIER = UsrCfg.SD.Properties.TIDSDSCALETYPE_HIER.GetEnum(DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.IDSDSCALETYPE_HIER.FieldName).asInt32());
                inCASE.SDCASEMTCOMPLTE.MT_OPEARTIONM = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.MT_OPEARTIONM.FieldName).asString();
                inCASE.SDCASEMTCOMPLTE.MT_POSSIBLERETURNS = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.MT_POSSIBLERETURNS.FieldName).asString();
                inCASE.SDCASEMTCOMPLTE.MT_TITLEM = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.MT_TITLEM.FieldName).asString();
                inCASE.SDCASEMTCOMPLTE.CASEMT_COUNTTIME = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_COUNTTIME.FieldName).asInt32();
                inCASE.SDCASEMTCOMPLTE.CASEMT_COUNTTIMEPAUSE = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_COUNTTIMEPAUSE.FieldName).asInt32();
                inCASE.SDCASEMTCOMPLTE.CASEMT_COUNTTIMERESOLVED = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_COUNTTIMERESOLVED.FieldName).asInt32();
                inCASE.SDCASEMTCOMPLTE.CASEMT_DATEASSIGNED = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_DATEASSIGNED.FieldName).asDateTime();
                inCASE.SDCASEMTCOMPLTE.CASEMT_DATECANCELLED = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_DATECANCELLED.FieldName).asDateTime();
                inCASE.SDCASEMTCOMPLTE.CASEMT_DATELASTCUT = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_DATELASTCUT.FieldName).asDateTime();
                inCASE.SDCASEMTCOMPLTE.CASEMT_LIFESTATUS = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_LIFESTATUS.FieldName).asString();
                inCASE.SDCASEMTCOMPLTE.CASEMT_TIMERCOUNT = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_TIMERCOUNT.FieldName).asString();
                inCASE.SDCASEACTIVITIESCOMPLTE.ACTIVITIES_DATECANCELLED = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEACTIVITIES.ACTIVITIES_DATECANCELLED.FieldName).asDateTime();
                inCASE.SDCASEACTIVITIESCOMPLTE.ACTIVITIES_DATECLOSED = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEACTIVITIES.ACTIVITIES_DATECLOSED.FieldName).asDateTime();
                inCASE.SDCASEACTIVITIESCOMPLTE.ACTIVITIES_DATECREATE = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEACTIVITIES.ACTIVITIES_DATECREATE.FieldName).asDateTime();
                inCASE.SDCASEACTIVITIESCOMPLTE.ACTIVITIES_DATERUNNING = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEACTIVITIES.ACTIVITIES_DATERUNNING.FieldName).asDateTime();
                inCASE.SDCASEACTIVITIESCOMPLTE.COMMENTSL = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEACTIVITIES.COMMENTSL.FieldName).asString();
                inCASE.SDCASEACTIVITIESCOMPLTE.GUIDET = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEACTIVITIES.GUIDET.FieldName).asString();
                inCASE.SDCASEACTIVITIESCOMPLTE.IDMDMODELTYPED = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDMDMODELTYPED.FieldName).asInt32();
                inCASE.SDCASEACTIVITIESCOMPLTE.IDSDRUNNIGSOURCEMODEL = UsrCfg.SD.Properties.TSDRUNNIGSOURCEMODEL.GetEnum(DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDSDRUNNIGSOURCEMODEL.FieldName).asInt32());
                inCASE.SDCASEACTIVITIESCOMPLTE.IDSDRUNNIGSTATUS = UsrCfg.SD.Properties.TIDSDRUNNIGSTATUS.GetEnum(DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDSDRUNNIGSTATUS.FieldName).asInt32());
                inCASE.SDCASEACTIVITIESCOMPLTE.IDSDCASE = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDSDCASE.FieldName).asInt32();
                inCASE.SDCASEACTIVITIESCOMPLTE.IDSDCASEACTIVITIES = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDSDCASEACTIVITIES.FieldName).asInt32();
                inCASE.SDCASEACTIVITIESCOMPLTE.IDSDCASEMT_ATVPARENT = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDSDCASEMT_ATVPARENT.FieldName).asInt32();
                inCASE.SDCASEACTIVITIESCOMPLTE.IDSDCASE_ATVPARENT = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDSDCASE_ATVPARENT.FieldName).asInt32();
                inCASE.SDCASEACTIVITIESCOMPLTE.LS_NAMESTEP = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEACTIVITIES.LS_NAMESTEP.FieldName).asString();
                inCASE.SDCASEACTIVITIESCOMPLTE.LS_STATUSN = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEACTIVITIES.LS_STATUSN.FieldName).asInt32();
                inCASE.SDCASEACTIVITIESCOMPLTE.MT_RETURNS = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEACTIVITIES.MT_RETURNS.FieldName).asString();
                inCASE.SDCASEACTIVITIESCOMPLTE.TITLEM = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDMODELTYPED.TITLEM.FieldName).asString();
                inCASE.SDCASEACTIVITIESCOMPLTE.ACTIVITIESNAME = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDRUNNIGSTATUS.ACTIVITIESNAME.FieldName).asString();
                inCASE.SDCASEACTIVITIESCOMPLTE.SOURCEMODELNAME = DS_SDCASECONSOLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDRUNNIGSOURCEMODEL.SOURCEMODELNAME.FieldName).asString();
                Param = new SysCfg.Stream.Properties.TParam();
                try {
                    Param.Inicialize();
                    Param.AddInt32(UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDCASE.FieldName, inCASE.SDCASECOMPLTE.IDSDCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                    //Param.AddInt32(UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDCASEMT.FieldName, IDSDCASEMT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal); 
                    //*********** SDWHOTOCASE_GET ***************************** 
                    var DS_SDWHOTOCASE = SysCfg.DB.SQL.Methods.OpenDataSet("DataLink", "SDWHOTOCASE_GETComplete", Param.ToBytes());
                    ResErr = DS_SDWHOTOCASE.ResErr;
                    if (ResErr.NotError) {
                        if (DS_SDWHOTOCASE.DataSet.RecordCount > 0) {
                            DS_SDWHOTOCASE.DataSet.First();
                            while (!(DS_SDWHOTOCASE.DataSet.Eof)) {
                                var UnSDWHOTOCASECOMPLTE = new UsrCfg.SD.Properties.TSDWHOTOCASECOMPLTE();
                                UnSDWHOTOCASECOMPLTE.COUNTTIME = DS_SDWHOTOCASE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDWHOTOCASE.COUNTTIME.FieldName).asInt32();
                                UnSDWHOTOCASECOMPLTE.COUNTTIMEPAUSE = DS_SDWHOTOCASE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDWHOTOCASE.COUNTTIMEPAUSE.FieldName).asInt32();
                                UnSDWHOTOCASECOMPLTE.COUNTTIMERESOLVED = DS_SDWHOTOCASE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDWHOTOCASE.COUNTTIMERESOLVED.FieldName).asInt32();
                                UnSDWHOTOCASECOMPLTE.DATELASTCUT = DS_SDWHOTOCASE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDWHOTOCASE.DATELASTCUT.FieldName).asDateTime();
                                UnSDWHOTOCASECOMPLTE.DATELASTREAD = DS_SDWHOTOCASE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDWHOTOCASE.DATELASTREAD.FieldName).asDateTime();
                                UnSDWHOTOCASECOMPLTE.DATEPERMISSIONDISABLE = DS_SDWHOTOCASE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDWHOTOCASE.DATEPERMISSIONDISABLE.FieldName).asDateTime();
                                UnSDWHOTOCASECOMPLTE.DATEPERMISSIONENABLE = DS_SDWHOTOCASE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDWHOTOCASE.DATEPERMISSIONENABLE.FieldName).asDateTime();
                                UnSDWHOTOCASECOMPLTE.IDCMDBCI = DS_SDWHOTOCASE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDWHOTOCASE.IDCMDBCI.FieldName).asInt32();
                                UnSDWHOTOCASECOMPLTE.IDSDCASE = DS_SDWHOTOCASE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDCASE.FieldName).asInt32();
                                UnSDWHOTOCASECOMPLTE.IDSDCASEMT = DS_SDWHOTOCASE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDCASEMT.FieldName).asInt32();
                                UnSDWHOTOCASECOMPLTE.IDSDCASEPERMISSION = UsrCfg.SD.Properties.TSDCASEPERMISSION.GetEnum(DS_SDWHOTOCASE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDCASEPERMISSION.FieldName).asInt32());
                                UnSDWHOTOCASECOMPLTE.IDSDTYPEUSER = DS_SDWHOTOCASE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDTYPEUSER.FieldName).asInt32();
                                UnSDWHOTOCASECOMPLTE.IDSDWHOTOCASE = DS_SDWHOTOCASE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDWHOTOCASE.FieldName).asInt32();
                                UnSDWHOTOCASECOMPLTE.IDSDWHOTOCASESTATUS = UsrCfg.SD.Properties.TSDWHOTOCASESTATUS.GetEnum(DS_SDWHOTOCASE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDWHOTOCASESTATUS.FieldName).asInt32());
                                UnSDWHOTOCASECOMPLTE.IDSDWHOTOCASETYPE = UsrCfg.SD.Properties.TIDSDWHOTOCASETYPE.GETCASE(DS_SDWHOTOCASE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDWHOTOCASETYPE.FieldName).asInt32());
                                UnSDWHOTOCASECOMPLTE.CASEMT_SET_LAVEL = DS_SDWHOTOCASE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDWHOTOCASE.CASEMT_SET_LAVEL.FieldName).asInt32();
                                UnSDWHOTOCASECOMPLTE.CI_GENERICNAME = DS_SDWHOTOCASE.DataSet.RecordSet.FieldName("CI_GENERICNAME").asString();
                                inCASE.LIST_SDWHOTOCASECOMPLTE.push(UnSDWHOTOCASECOMPLTE);
                                DS_SDWHOTOCASE.DataSet.Next();

                            }
                        }
                    }
                }
                finally {
                    Param.Destroy();
                }
                _this.LISTCASE.push(inCASE);
            }
        }
        finally {
            Param.Destroy();
        }
    }

    this.isEnableACTIVITIES = function () {
        var _this = this.TParent();
        var ResBool = false;
        if (arguments.length == 3) {
            inSDCASE = arguments[0];
            StrPend = arguments[1];//
            StrOut = arguments[2];
            ResBool = _this.isEnableACTIVITIES_Client(inSDCASE, StrPend, StrOut);
        }
        else if (arguments.length == 2) {
            inSDCASE = arguments[0];
            Res = _this.isEnableACTIVITIES_Normal(inSDCASE);
        }
        else {
            Open.ResErr.Mesaje = "Param error";
        }
        return (ResBool);
    }

    this.isEnableACTIVITIES_Normal = function (inSDCASE) {

        var ResBool = false;
        try {
            for (var i = 0; i < _this.LISTCASE.length; i++) {
                if (_this.LISTCASE[i].SDCASECOMPLTE.IDSDCASE == inSDCASE) {
                    for (var x = 0; x < _this.LISTCASE[i].LIST_CASE_ACTIVITIES.length; x++) {
                        if ((_this.LISTCASE[i].LIST_CASE_ACTIVITIES[x].SDCASEACTIVITIESCOMPLTE.IDSDRUNNIGSTATUS == UsrCfg.SD.Properties.TIDSDRUNNIGSTATUS._RUNNING) ||
                            (_this.LISTCASE[i].LIST_CASE_ACTIVITIES[x].SDCASEACTIVITIESCOMPLTE.IDSDRUNNIGSTATUS == UsrCfg.SD.Properties.TIDSDRUNNIGSTATUS._CREATE)) {
                            // _this.LISTCASE[i].LIST_CASE_ACTIVITIES[x].SDCASECOMPLTE.IDSDCASE.toString();                                               
                            ResBool = true;
                        }
                    }
                }
            }
        }
        catch (Exception) {
            ResBool = true;//para indicar que no se sabe exacatmante;
            ResErr.NotError = false;
            ResErr.Mesaje = "isEnableACTIVITIES error";
            throw Exception;
        }
        return (ResBool);
    }


    //begin SDClientCaseExplorer
    this.Refresch;//evento
    this._EnableControls = true;
    Object.defineProperty(this, 'EnableControls', {
        get: function () {
            return this._EnableControls;
        },
        set: function (_Value) {
            this._EnableControls = value;
            if (_EnableControls == true) {
                this.inRefresch();
            }
        }
    });

    //end SDClientCaseExplorer
}

