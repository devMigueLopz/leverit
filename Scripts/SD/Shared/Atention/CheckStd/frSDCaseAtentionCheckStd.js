﻿//Atis.SD
//interface

ItHelpCenter.SD.TfrSDCaseAtentionCheckStd = function (inObject, inId) {
    this.TParent = function () {
        return this;
    }.bind(this);    

    var _this = this.TParent();

    this.ObjectHtml = inObject;
    this.DialogResult = false;
    this.CierraVentana = null;
    this.OnInicializeComponent = null;
    this.Mythis = "TfrSDCaseAtentionCheckStd";
    this.Id = inId;
    //UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1", "Hi");//Declaración
    _this.InitializeComponent();

    
    
    
}

ItHelpCenter.SD.TfrSDCaseAtentionCheckStd.prototype.InitializeComponent = function () {

    var _this = this.TParent();

    _this.Modal = new TVclModal(document.body, null, "");
    _this.Modal.Width = "19%";

    var spPrincipal = new TVclStackPanel(_this.Modal.Body.This, "1", 1, [[12], [12], [12], [12], [12]]);
    _this.This = spPrincipal.Row.This;

    var spLabel = new TVclStackPanel(spPrincipal.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
    _this.lblDescripcion = new TVcllabel(spLabel.Column[0].This, "", TlabelType.H4);

    var spTextArea = new TVclStackPanel(spPrincipal.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
    _this.txtDescripcion = new TVclMemo(spTextArea.Column[0].This, "");
    _this.txtDescripcion.Text = "";
    _this.txtDescripcion.VCLType = TVCLType.BS;
    _this.txtDescripcion.Rows = 5;
    _this.txtDescripcion.Cols = 50;

    var spBotones = new TVclStackPanel(spPrincipal.Column[0].This, "1", 3, [[4, 4, 4], [4, 4, 4], [4, 4, 4], [4, 4, 4], [4, 4, 4]]);
    _this.BtnRefresh = new TVclButton(spBotones.Column[0].This, "");
    _this.BtnRefresh.Src = "image/16/Refresh.png";
    _this.BtnRefresh.Text = "Refresh Case";
    _this.BtnRefresh.This.classList.add("btn-xs");
    _this.BtnRefresh.VCLType = TVCLType.BS;

    _this.BtnQuit = new TVclButton(spBotones.Column[1].This, "");
    _this.BtnQuit.Src = "image/16/Logout.png";
    _this.BtnQuit.Text = "Quit Case";
    _this.BtnQuit.This.classList.add("btn-xs");
    _this.BtnQuit.VCLType = TVCLType.BS;

    _this.BtnCancel = new TVclButton(spBotones.Column[2].This, "");
    _this.BtnCancel.Src = "image/16/Cancel.png";
    _this.BtnCancel.Text = "Cancel";
    _this.BtnCancel.This.classList.add("btn-xs");
    _this.BtnCancel.VCLType = TVCLType.BS;

    _this.BtnRefresh.onClick = function () {
        _this.BtnRefresh_onClick(_this, _this.BtnRefresh);
    }
    _this.BtnQuit.onClick = function () {
        _this.BtnQuit_onClick(_this, _this.BtnQuit);
    }
    _this.BtnCancel.onClick = function () {
        _this.BtnCancel_onClick(_this, _this.BtnCancel);
    }

    if (_this.OnInicializeComponent != null)
        _this.OnInicializeComponent();
}



//{$R *.dfm}

ItHelpCenter.SD.TfrSDCaseAtentionCheckStd.prototype.Initialize = function (Descripcion, Refresh) {
    var _this = this.TParent();
    _this.txtDescripcion.Text = Descripcion;
    if (Refresh)
        _this.BtnRefresh.Visible = true;
    else
        _this.BtnRefresh.Visible = false;
}

ItHelpCenter.SD.TfrSDCaseAtentionCheckStd.prototype.BtnRefresh_onClick = function (sender, e) {
    var _this = sender;
    if (_this.CierraVentana != null)
        _this.CierraVentana(ItHelpCenter.SD.Atention.CaseAtentionCheckStd.TCheckStdResult.Refresh, null);

    _this.Modal.CloseModal();
}

ItHelpCenter.SD.TfrSDCaseAtentionCheckStd.prototype.BtnQuit_onClick = function (sender, e) {
    var _this = sender;
    if (_this.CierraVentana != null)
        _this.CierraVentana(ItHelpCenter.SD.Atention.CaseAtentionCheckStd.TCheckStdResult.Quit, null);
    _this.Modal.CloseModal();
}

ItHelpCenter.SD.TfrSDCaseAtentionCheckStd.prototype.BtnCancel_onClick = function (sender, e) {
    var _this = sender;
    if (_this.CierraVentana != null)
        _this.CierraVentana(ItHelpCenter.SD.Atention.CaseAtentionCheckStd.TCheckStdResult.Cancel, null);
    _this.Modal.CloseModal();
}





/*Anexo
, incallbackModalResult    this.CallbackModalResult = incallbackModalResult;//CallbackModalResult(OutStr);
*/