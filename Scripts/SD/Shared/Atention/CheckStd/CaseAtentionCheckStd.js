﻿//ItHelpCenter.SD.Atention.CaseAtentionCheckStd

ItHelpCenter.SD.Atention.CaseAtentionCheckStd.TCheckStd = function()
{
    this.IDSDWHOTOCASE = 0;
    this.IDSDWHOTOCASESTATUS = UsrCfg.SD.Properties.TSDWHOTOCASESTATUS.GetEnum(undefined);
    this.WHOTOCASENAME = "";
    this.SDWHOTOCASETYPENAME = "";
    this.IDSDCASE_PARENT = 0;
    this.IDSDCASESTATUS = UsrCfg.SD.Properties.TSDCaseStatus.GetEnum(undefined);
    this.CASESTATUSNAME = "";
    this.IDSDCASEMTSTATUS = UsrCfg.SD.Properties.TSDCASEMTSTATUS.GetEnum(undefined);
    this.CASEMT_SET_LS_STATUSN = 0;
    this.CASEMTSTATUSNAME = "";
    this.LFS = 0;
    this.ATV = 0;
}

ItHelpCenter.SD.Atention.CaseAtentionCheckStd.SDWHOTOCASE_Fill = function(IDSDWHOTOCASE, CASEMT_LSSTATUSN)
{
    var CheckStd = new ItHelpCenter.SD.Atention.CaseAtentionCheckStd.TCheckStd();
    CheckStd.IDSDWHOTOCASE = -1;
    var ResErr = SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    Param.AddInt32(UsrCfg.InternoAtisNames.SDCASEMT_LS.CASEMT_LSSTATUSN.FieldName, CASEMT_LSSTATUSN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    Param.AddInt32(UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDWHOTOCASE.FieldName, IDSDWHOTOCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try
    {
        var  DS_CheckStd = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "CHECK_STATUS_ALL", Param.ToBytes());
        ResErr = DS_CheckStd.ResErr;
        if (ResErr.NotError)
        {
            if (DS_CheckStd.DataSet.RecordCount > 0)
            {
                DS_CheckStd.DataSet.First();
                while (!(DS_CheckStd.DataSet.Eof))
                {
                    CheckStd.IDSDWHOTOCASE = DS_CheckStd.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDWHOTOCASE.FieldName).asInt32();
                    //Other
                    CheckStd.IDSDWHOTOCASESTATUS = UsrCfg.SD.Properties.TSDWHOTOCASESTATUS.GetEnum(DS_CheckStd.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDWHOTOCASESTATUS.FieldName).asInt32());
                    CheckStd.WHOTOCASENAME = DS_CheckStd.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDWHOTOCASESTATUS.WHOTOCASENAME.FieldName).asString();
                    CheckStd.SDWHOTOCASETYPENAME = DS_CheckStd.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDWHOTOCASETYPE.SDWHOTOCASETYPENAME.FieldName).asString();
                    CheckStd.IDSDCASE_PARENT = DS_CheckStd.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASE.IDSDCASE_PARENT.FieldName).asInt32();
                    CheckStd.IDSDCASESTATUS = UsrCfg.SD.Properties.TSDCaseStatus.GetEnum(DS_CheckStd.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASE.IDSDCASESTATUS.FieldName).asInt32());
                    CheckStd.CASESTATUSNAME = DS_CheckStd.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASESTATUS.CASESTATUSNAME.FieldName).asString();
                    CheckStd.IDSDCASEMTSTATUS = UsrCfg.SD.Properties.TSDCASEMTSTATUS.GetEnum(DS_CheckStd.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASEMTSTATUS.FieldName).asInt32());
                    CheckStd.CASEMT_SET_LS_STATUSN = DS_CheckStd.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_LS_STATUSN.FieldName).asInt32();
                    CheckStd.CASEMTSTATUSNAME = DS_CheckStd.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMTSTATUS.CASEMTSTATUSNAME.FieldName).asString();
                    CheckStd.LFS = DS_CheckStd.DataSet.RecordSet.FieldName("LFS").asInt32();
                    CheckStd.ATV = DS_CheckStd.DataSet.RecordSet.FieldName("ATV").asInt32();
                    DS_CheckStd.DataSet.Next();
                }
            }
            else
            {
                DS_CheckStd.ResErr.NotError = false;
                DS_CheckStd.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (CheckStd);
}
        
ItHelpCenter.SD.Atention.CaseAtentionCheckStd.TCheckStdResult =
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    Cancel: { value: 1, name: "Cancel" },
    Refresh: { value: 2, name: "Refresh" },
    Quit: { value: 3, name: "Quit" },
}
        
ItHelpCenter.SD.Atention.CaseAtentionCheckStd.TOrigen =
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    General: { value: 1, name: "General" },
    Actividad: { value: 2, name: "Actividad" },
    Escalemiento: { value: 3, name: "Escalemiento" },
    MsgAtencion: { value: 4, name: "MsgAtencion" },
    Step: { value: 5, name: "Step" },
    Status: { value: 6, name: "Status" },
    Relation_DEL: { value: 7, name: "Relation_DEL" },
    Relation_ADD: { value: 8, name: "Relation_ADD" },
    Relation_VIEW: { value: 9, name: "Relation_VIEW" },
    Relations_Ligth: { value: 10, name: "Relations_Ligth" },
}

        
ItHelpCenter.SD.Atention.CaseAtentionCheckStd.CheckStdRes = function(Origen,IDSDWHOTOCASE,STATUSN)
{
   
    var CheckStdRes = new ItHelpCenter.SD.Atention.CaseAtentionCheckStd.TCheckStdRes();
    var NuevaLinea = " \n ";
    var CheckStd = ItHelpCenter.SD.Atention.CaseAtentionCheckStd.SDWHOTOCASE_Fill(IDSDWHOTOCASE, STATUSN);

    if (CheckStd.IDSDCASEMTSTATUS == UsrCfg.SD.Properties.TSDCASEMTSTATUS._Cancelled)
    {
        CheckStdRes.MsgInfo += NuevaLinea + "The model of care has changed.";
        CheckStdRes.Refersh = false;
        CheckStdRes.Res = true;
    }

    if ((CheckStd.IDSDCASESTATUS == UsrCfg.SD.Properties.TSDCaseStatus._Cancelled) || (CheckStd.IDSDCASESTATUS == UsrCfg.SD.Properties.TSDCaseStatus._Closed))
    {
        CheckStdRes.MsgInfo += NuevaLinea + "The case has been cancelled or terminated.";
        CheckStdRes.Refersh = false;
        CheckStdRes.Res = true;
    }

    if (CheckStd.IDSDWHOTOCASESTATUS != UsrCfg.SD.Properties.TSDWHOTOCASESTATUS._Active)
    {
        CheckStdRes.MsgInfo += NuevaLinea + "Your permission to issued.";
        CheckStdRes.Refersh = false;
        CheckStdRes.Res = true;
    }

    if ((CheckStd.IDSDCASE_PARENT > 0)&&(Origen != TOrigen.Relation_VIEW))
    {
        CheckStdRes.MsgInfo += NuevaLinea + "The case is linked to a parent.";
        CheckStdRes.Refersh = false;
        CheckStdRes.Res = true;
    }

    //if (CheckStd.LFS == 0)
    if (CheckStd.CASEMT_SET_LS_STATUSN != STATUSN)
    {
        CheckStdRes.MsgInfo += NuevaLinea + "The step current already not be is active.";
        CheckStdRes.Res = true;
    }

    if ((CheckStd.ATV > 0) && 
        (Origen != TOrigen.Actividad) && 
        (Origen != TOrigen.MsgAtencion) && 
        (Origen != TOrigen.Relations_Ligth) && 
        (Origen != TOrigen.Escalemiento) && 
        (Origen != TOrigen.Relation_VIEW) && 
        (Origen != TOrigen.Relation_DEL)
        )
    {
        CheckStdRes.MsgInfo += NuevaLinea + "There are " + CheckStd.ATV.toString() + " activities in progress.";
        CheckStdRes.Res = true;
    }
    return (CheckStdRes);
}


ItHelpCenter.SD.Atention.CaseAtentionCheckStd.TCheckStdRes = function()
{
    this.Res = false;   
    this.Refersh = true;    
    this.MsgInfo = "Reasons why I could not continue:";
}
     

