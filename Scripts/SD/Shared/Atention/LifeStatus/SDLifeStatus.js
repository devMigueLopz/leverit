﻿
//UsrCfg.SD
/*

[DataContract]
UsrCfg.SD.TMDLIFESTATUSProfiler = function()
{
    this._Version = "002";
    this.ResErr = new SysCfg.Error.Properties.TResErr();
    this.CASEMT_LIFESTATUS = "";

    this.LsObjectToSQL = function(Field,FieldName)
    {
        return(SysCfg.DB.Format.objectToParam(Field, "", Field.GetType(), SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal) +" "+FieldName+",");
    }

    this.LIFESTATUSROWToSQL = function(UnMDLIFESTATUS)
    {
        if (UnMDLIFESTATUS == null)
            UnMDLIFESTATUS = new TMDLIFESTATUS();
        var UnLS = "Select ";
        UnLS += LsObjectToSQL(UnMDLIFESTATUS.STATUSN, InternoAtisNames.MDLIFESTATUS.STATUSN.FieldName);
        UnLS += LsObjectToSQL(UnMDLIFESTATUS.NAMESTEP, InternoAtisNames.MDLIFESTATUS.NAMESTEP.FieldName);
        UnLS += LsObjectToSQL(UnMDLIFESTATUS.NEXTSTEP, InternoAtisNames.MDLIFESTATUS.NEXTSTEP.FieldName);
        UnLS += LsObjectToSQL(UnMDLIFESTATUS.ENDSTEP, InternoAtisNames.MDLIFESTATUS.ENDSTEP.FieldName);
        UnLS += LsObjectToSQL(UnMDLIFESTATUS.COMMENTSST, InternoAtisNames.MDLIFESTATUS.COMMENTSST.FieldName);
        UnLS += LsObjectToSQL(UnMDLIFESTATUS.WARNINGSTEP, InternoAtisNames.MDLIFESTATUS.WARNINGSTEP.FieldName);
        UnLS += LsObjectToSQL(UnMDLIFESTATUS.LS_IDSDCASESTATUS.value, InternoAtisNames.MDLIFESTATUS.LS_IDSDCASESTATUS.FieldName);
        UnLS += LsObjectToSQL(UnMDLIFESTATUS.CAUTIONSTEP, InternoAtisNames.MDLIFESTATUS.CAUTIONSTEP.FieldName);            
        return (UnLS);
    }
    this.MDMATRIXMODELSROWToSQL = function(UnMDMATRIXMODELS)
    {
        var UnMXM = "";            
        if (UnMDMATRIXMODELS==null)  
            UnMDMATRIXMODELS= new TMDMATRIXMODELS();
        UnMXM += LsObjectToSQL(UnMDMATRIXMODELS.COMMENTSL, InternoAtisNames.MDMATRIXMODELS.COMMENTSL.FieldName);
        UnMXM += LsObjectToSQL(UnMDMATRIXMODELS.GUIDET, InternoAtisNames.MDMATRIXMODELS.GUIDET.FieldName);
        UnMXM += LsObjectToSQL(UnMDMATRIXMODELS.IDMDMODELTYPED2, InternoAtisNames.MDMATRIXMODELS.IDMDMODELTYPED2.FieldName);
        UnMXM += LsObjectToSQL(UnMDMATRIXMODELS.MODELVALIDATE, InternoAtisNames.MDMATRIXMODELS.MODELVALIDATE.FieldName);
        UnMXM += LsObjectToSQL(UnMDMATRIXMODELS.POSINSTEP, InternoAtisNames.MDMATRIXMODELS.POSINSTEP.FieldName);
        UnMXM = UnMXM.Substring(0, UnMXM.Length - 1);// para cortar la ultimo coma
        return (UnMXM);
    }
    this.LIFESTATUStoSQL = function(IDMDMODELTYPED1)
    {
        var Res ="";
        try
        {
            for (var CounterLS = 0; CounterLS < MDLIFESTATUSList.length; CounterLS++)
            {
                    
                var UnMDLIFESTATUS;
                UnMDLIFESTATUS = MDLIFESTATUSList[CounterLS];
                var UnLS = LIFESTATUSROWToSQL(UnMDLIFESTATUS);

                for (var CounterMXM = 0; CounterMXM < UnMDLIFESTATUS.MDMATRIXMODELSList.length; CounterMXM++)
                {
                        
                    var UnMDMATRIXMODELS = UnMDLIFESTATUS.MDMATRIXMODELSList[CounterMXM];
                    var UnMXM = MDMATRIXMODELSROWToSQL(UnMDMATRIXMODELS);
                    Res += UnLS + UnMXM + " UNION ";
                }                    
            }

            if (Res.Length > 0)
            {
                if (Res.IndexOf(" UNION ") != -1) Res = Res.Substring(0, Res.Length - 7);// para cortar la ultimo Union
                Res = "SELECT " + LsObjectToSQL(IDMDMODELTYPED1, InternoAtisNames.MDMATRIXMODELS.IDMDMODELTYPED1.FieldName) + "TBL_LS.* FROM (" + Res + ") TBL_LS";
            }
            else
            {
                Res = "SELECT " + LsObjectToSQL(IDMDMODELTYPED1, InternoAtisNames.MDMATRIXMODELS.IDMDMODELTYPED1.FieldName) + "TBL_LS.* FROM (" + LIFESTATUSROWToSQL(null) + MDMATRIXMODELSROWToSQL(null) + ") TBL_LS";
            }
           
        }
        catch (e)
        {
            ResErr.NotError = false;
            ResErr.Mesaje = "LIFESTATUStoSQL:" + e;
        }
        return Res;
    }

    this.LIFESTATUStoStr = function()
    {
        var Res = _Version + "002(1,1,1,1)0000";
        var Longitud = "";
        var Texto = "";
        try
        {

            SysCfg.Str.Protocol.WriteInt(MDLIFESTATUSList.Count, Longitud, Texto);
            SysCfg.Str.Protocol.WriteInt(MDINTERFACEList.Count, Longitud, Texto);   
            SysCfg.Str.Protocol.WriteInt(MDLIFESTATUSTYPEUSERList.Count, Longitud, Texto);                             
            SysCfg.Str.Protocol.WriteInt(MDLIFESTATUSCIEXTRATABLEList.Count, Longitud, Texto);
            SysCfg.Str.Protocol.WriteInt(MDLIFESTATUSCIEXTRAFIELDSList.Count, Longitud, Texto);
            SysCfg.Str.Protocol.WriteInt(MDLIFESTATUSSTEXTRATABLEList.Count, Longitud, Texto);
            SysCfg.Str.Protocol.WriteInt(MDLIFESTATUSSTEXTRAFIELDSList.Count, Longitud, Texto);
            SysCfg.Str.Protocol.WriteInt(MDCASESTEXTRATABLEList.Count, Longitud, Texto);
            SysCfg.Str.Protocol.WriteInt(MDCASESTEXTRAFIELDSList.Count, Longitud, Texto);               
            SysCfg.Str.Protocol.WriteInt(MDMATRIXMODELSList.Count, Longitud, Texto);
            for (var Counter = 0; Counter < MDLIFESTATUSList.length; Counter++)
            {
                var UnMDLIFESTATUS;
                UnMDLIFESTATUS = MDLIFESTATUSList[Counter];
                SysCfg.Str.Protocol.WriteInt(UnMDLIFESTATUS.STATUSN, Longitud, Texto);
                SysCfg.Str.Protocol.WriteStr(UnMDLIFESTATUS.NAMESTEP, Longitud, Texto);
                SysCfg.Str.Protocol.WriteStr(UnMDLIFESTATUS.NEXTSTEP, Longitud, Texto);
                SysCfg.Str.Protocol.WriteStr(UnMDLIFESTATUS.ENDSTEP, Longitud, Texto);
                SysCfg.Str.Protocol.WriteStr(UnMDLIFESTATUS.COMMENTSST, Longitud, Texto);
                SysCfg.Str.Protocol.WriteStr(UnMDLIFESTATUS.WARNINGSTEP, Longitud, Texto);
                SysCfg.Str.Protocol.WriteInt(UnMDLIFESTATUS.LS_IDSDCASESTATUS.value, Longitud, Texto);
                SysCfg.Str.Protocol.WriteStr(UnMDLIFESTATUS.CAUTIONSTEP, Longitud, Texto);                    
            }
                
            for (var Counter = 0; Counter < MDINTERFACEList.length; Counter++)
            {
                var UnMDINTERFACE = MDINTERFACEList[Counter];
                SysCfg.Str.Protocol.WriteInt(UnMDINTERFACE.IDMDINTERFACE, Longitud, Texto);
                SysCfg.Str.Protocol.WriteStr(UnMDINTERFACE.CUSTOMCODE, Longitud, Texto);
            }
                
            for (var Counter = 0; Counter < MDLIFESTATUSTYPEUSERList.length; Counter++)
            {
                var UnMDLIFESTATUSTYPEUSER = MDLIFESTATUSTYPEUSERList[Counter];
                SysCfg.Str.Protocol.WriteInt(UnMDLIFESTATUSTYPEUSER.IDMDLIFESTATUSTYPEUSER, Longitud, Texto);
                SysCfg.Str.Protocol.WriteInt(UnMDLIFESTATUSTYPEUSER.STATUSN, Longitud, Texto);
                SysCfg.Str.Protocol.WriteInt(UnMDLIFESTATUSTYPEUSER.IDSDTYPEUSER.value, Longitud, Texto);
                SysCfg.Str.Protocol.WriteInt(UnMDLIFESTATUSTYPEUSER.IDMDLIFESTATUSPERMISSION.value, Longitud, Texto);
                SysCfg.Str.Protocol.WriteInt(UnMDLIFESTATUSTYPEUSER.MDINTERFACE.IDMDINTERFACE, Longitud, Texto);
                SysCfg.Str.Protocol.WriteBool(UnMDLIFESTATUSTYPEUSER.ENABLETIME, Longitud, Texto);                    
                SysCfg.Str.Protocol.WriteBool(UnMDLIFESTATUSTYPEUSER.INCLUDEALLGROUP, Longitud, Texto);                    
                SysCfg.Str.Protocol.WriteInt(UnMDLIFESTATUSTYPEUSER.IDMDLIFESTATUSBEHAVIOR.value, Longitud, Texto);
                SysCfg.Str.Protocol.WriteBool(UnMDLIFESTATUSTYPEUSER.LSEND_COMMENTSENABLE, Longitud, Texto);
                SysCfg.Str.Protocol.WriteBool(UnMDLIFESTATUSTYPEUSER.LSBEGIN_COMMENTSENABLE, Longitud, Texto);
                SysCfg.Str.Protocol.WriteStr(UnMDLIFESTATUSTYPEUSER.LSCHANGE_COMMENTS, Longitud, Texto);
                SysCfg.Str.Protocol.WriteStr(UnMDLIFESTATUSTYPEUSER.LSEND_COMMENTSLABEL, Longitud, Texto);
                SysCfg.Str.Protocol.WriteStr(UnMDLIFESTATUSTYPEUSER.LSBEGIN_COMMENTSLABEL, Longitud, Texto);
                SysCfg.Str.Protocol.WriteStr(UnMDLIFESTATUSTYPEUSER.LSHERESTEP_CONFIG, Longitud, Texto);
                SysCfg.Str.Protocol.WriteStr(UnMDLIFESTATUSTYPEUSER.LSNEXTSTEP_CONFIG, Longitud, Texto);                    
            }
            for (var Counter = 0; Counter < MDLIFESTATUSCIEXTRATABLEList.length; Counter++)
            {
                var UnMDLIFESTATUSCIEXTRATABLE = MDLIFESTATUSCIEXTRATABLEList[Counter];
                SysCfg.Str.Protocol.WriteInt(UnMDLIFESTATUSCIEXTRATABLE.IDMDLIFESTATUSCIEXTRATABLE, Longitud, Texto);   
                SysCfg.Str.Protocol.WriteInt(UnMDLIFESTATUSCIEXTRATABLE.STATUSN, Longitud, Texto);
                SysCfg.Str.Protocol.WriteInt(UnMDLIFESTATUSCIEXTRATABLE.IDCMDBCIDEFINE, Longitud, Texto);
                SysCfg.Str.Protocol.WriteInt(UnMDLIFESTATUSCIEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE, Longitud, Texto);                                     
                SysCfg.Str.Protocol.WriteInt(UnMDLIFESTATUSCIEXTRATABLE.IDMDLIFESTATUSTYPEUSER, Longitud, Texto);
                SysCfg.Str.Protocol.WriteInt(UnMDLIFESTATUSCIEXTRATABLE.MDINTERFACE.IDMDINTERFACE, Longitud, Texto);
                SysCfg.Str.Protocol.WriteStr(UnMDLIFESTATUSCIEXTRATABLE.CMDBCIDEFINE.CIDEFINE_NAME, Longitud, Texto);
                    
                SysCfg.Str.Protocol.WriteStr(UnMDLIFESTATUSCIEXTRATABLE.CMDBCIDEFINEEXTRATABLE.EXTRATABLE_NAMETABLE, Longitud, Texto);
                SysCfg.Str.Protocol.WriteInt(UnMDLIFESTATUSCIEXTRATABLE.CMDBCIDEFINEEXTRATABLE.EXTRATABLE_IDSOURCE.value, Longitud, Texto);
                SysCfg.Str.Protocol.WriteBool(UnMDLIFESTATUSCIEXTRATABLE.CMDBCIDEFINEEXTRATABLE.EXTRATABLE_LOG, Longitud, Texto);
                SysCfg.Str.Protocol.WriteStr(UnMDLIFESTATUSCIEXTRATABLE.CMDBCIDEFINEEXTRATABLE.EXTRATABLE_NAME, Longitud, Texto);
                SysCfg.Str.Protocol.WriteStr(UnMDLIFESTATUSCIEXTRATABLE.CMDBCIDEFINEEXTRATABLE.EXTRATABLE_DESCRIPTION, Longitud, Texto);

            }
            for (var Counter = 0; Counter < MDLIFESTATUSCIEXTRAFIELDSList.length; Counter++)
            {
                var UnMDLIFESTATUSCIEXTRAFIELDS = MDLIFESTATUSCIEXTRAFIELDSList[Counter];
                SysCfg.Str.Protocol.WriteInt(UnMDLIFESTATUSCIEXTRAFIELDS.IDMDLIFESTATUSCIEXTRAFIELDS, Longitud, Texto);
                SysCfg.Str.Protocol.WriteInt(UnMDLIFESTATUSCIEXTRAFIELDS.IDMDLIFESTATUSCIEXTRATABLE, Longitud, Texto);
                SysCfg.Str.Protocol.WriteInt(UnMDLIFESTATUSCIEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS, Longitud, Texto);
                SysCfg.Str.Protocol.WriteInt(UnMDLIFESTATUSCIEXTRAFIELDS.IDMDLIFESTATUSPERMISSION.value, Longitud, Texto);                    
                SysCfg.Str.Protocol.WriteBool(UnMDLIFESTATUSCIEXTRAFIELDS.MANDATORY, Longitud, Texto);     
            }

            for (var Counter = 0; Counter < MDLIFESTATUSSTEXTRATABLEList.length; Counter++)
            {
                var UnMDLIFESTATUSSTEXTRATABLE = MDLIFESTATUSSTEXTRATABLEList[Counter];
                SysCfg.Str.Protocol.WriteInt(UnMDLIFESTATUSSTEXTRATABLE.IDMDLIFESTATUSSTEXTRATABLE, Longitud, Texto);                                        
                SysCfg.Str.Protocol.WriteInt(UnMDLIFESTATUSSTEXTRATABLE.STATUSN, Longitud, Texto);
                SysCfg.Str.Protocol.WriteInt(UnMDLIFESTATUSSTEXTRATABLE.IDMDSERVICETYPE.value, Longitud, Texto);
                SysCfg.Str.Protocol.WriteInt(UnMDLIFESTATUSSTEXTRATABLE.IDMDSERVICEEXTRATABLE, Longitud, Texto);                    
                SysCfg.Str.Protocol.WriteInt(UnMDLIFESTATUSSTEXTRATABLE.IDMDLIFESTATUSTYPEUSER, Longitud, Texto);
                SysCfg.Str.Protocol.WriteInt(UnMDLIFESTATUSSTEXTRATABLE.MDINTERFACE.IDMDINTERFACE, Longitud, Texto);
                SysCfg.Str.Protocol.WriteStr(UnMDLIFESTATUSSTEXTRATABLE.LSCHANGETABLE_COMMENTS, Longitud, Texto);

                SysCfg.Str.Protocol.WriteStr(UnMDLIFESTATUSSTEXTRATABLE.MDSERVICEEXTRATABLE.EXTRATABLE_NAMETABLE, Longitud, Texto);
                SysCfg.Str.Protocol.WriteInt(UnMDLIFESTATUSSTEXTRATABLE.MDSERVICEEXTRATABLE.EXTRATABLE_IDSOURCE.value, Longitud, Texto);
                SysCfg.Str.Protocol.WriteBool(UnMDLIFESTATUSSTEXTRATABLE.MDSERVICEEXTRATABLE.EXTRATABLE_LOG, Longitud, Texto);

                SysCfg.Str.Protocol.WriteStr(UnMDLIFESTATUSSTEXTRATABLE.MDSERVICEEXTRATABLE.EXTRATABLE_NAME, Longitud, Texto);
                SysCfg.Str.Protocol.WriteStr(UnMDLIFESTATUSSTEXTRATABLE.MDSERVICEEXTRATABLE.EXTRATABLE_DESCRIPTION, Longitud, Texto);
            }                
                
            for (var Counter = 0; Counter < MDLIFESTATUSSTEXTRAFIELDSList.length; Counter++)
            {
                var UnMDLIFESTATUSSTEXTRAFIELDS = MDLIFESTATUSSTEXTRAFIELDSList[Counter];
                SysCfg.Str.Protocol.WriteInt(UnMDLIFESTATUSSTEXTRAFIELDS.IDMDLIFESTATUSSTEXTRAFIELDS, Longitud, Texto);
                SysCfg.Str.Protocol.WriteInt(UnMDLIFESTATUSSTEXTRAFIELDS.IDMDLIFESTATUSSTEXTRATABLE, Longitud, Texto);
                SysCfg.Str.Protocol.WriteInt(UnMDLIFESTATUSSTEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS, Longitud, Texto);
                SysCfg.Str.Protocol.WriteInt(UnMDLIFESTATUSSTEXTRAFIELDS.IDMDLIFESTATUSPERMISSION.value, Longitud, Texto);
                SysCfg.Str.Protocol.WriteBool(UnMDLIFESTATUSSTEXTRAFIELDS.MANDATORY, Longitud, Texto);
            }

            for (var Counter = 0; Counter < MDCASESTEXTRATABLEList.length; Counter++)
            {
                var UnMDCASESTEXTRATABLE = MDCASESTEXTRATABLEList[Counter];
                    
                SysCfg.Str.Protocol.WriteInt(UnMDCASESTEXTRATABLE.IDMDCASESTEXTRATABLE, Longitud, Texto);
                SysCfg.Str.Protocol.WriteInt(UnMDCASESTEXTRATABLE.IDMDMODELTYPED, Longitud, Texto);
                SysCfg.Str.Protocol.WriteInt(UnMDCASESTEXTRATABLE.IDMDSERVICETYPE.value, Longitud, Texto);
                SysCfg.Str.Protocol.WriteInt(UnMDCASESTEXTRATABLE.IDMDSERVICEEXTRATABLE, Longitud, Texto);
                    
                SysCfg.Str.Protocol.WriteInt(UnMDCASESTEXTRATABLE.MDINTERFACE.IDMDINTERFACE, Longitud, Texto);
                SysCfg.Str.Protocol.WriteStr(UnMDCASESTEXTRATABLE.LSCHANGETABLE_COMMENTS, Longitud, Texto);

                SysCfg.Str.Protocol.WriteStr(UnMDCASESTEXTRATABLE.MDSERVICEEXTRATABLE.EXTRATABLE_NAMETABLE, Longitud, Texto);
                SysCfg.Str.Protocol.WriteInt(UnMDCASESTEXTRATABLE.MDSERVICEEXTRATABLE.EXTRATABLE_IDSOURCE.value, Longitud, Texto);
                SysCfg.Str.Protocol.WriteBool(UnMDCASESTEXTRATABLE.MDSERVICEEXTRATABLE.EXTRATABLE_LOG, Longitud, Texto);

                SysCfg.Str.Protocol.WriteStr(UnMDCASESTEXTRATABLE.MDSERVICEEXTRATABLE.EXTRATABLE_NAME, Longitud, Texto);
                SysCfg.Str.Protocol.WriteStr(UnMDCASESTEXTRATABLE.MDSERVICEEXTRATABLE.EXTRATABLE_DESCRIPTION, Longitud, Texto);
            }

            for (var Counter = 0; Counter < MDCASESTEXTRAFIELDSList.length; Counter++)
            {
                UnMDCASESTEXTRAFIELDS = MDCASESTEXTRAFIELDSList[Counter];
                SysCfg.Str.Protocol.WriteInt(UnMDCASESTEXTRAFIELDS.IDMDCASESTEXTRAFIELDS, Longitud, Texto);
                SysCfg.Str.Protocol.WriteInt(UnMDCASESTEXTRAFIELDS.IDMDCASESTEXTRATABLE, Longitud, Texto);
                SysCfg.Str.Protocol.WriteInt(UnMDCASESTEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS, Longitud, Texto);
                SysCfg.Str.Protocol.WriteInt(UnMDCASESTEXTRAFIELDS.IDMDLIFESTATUSPERMISSION.value, Longitud, Texto);
                SysCfg.Str.Protocol.WriteBool(UnMDCASESTEXTRAFIELDS.MANDATORY, Longitud, Texto);
            }



            for (var Counter = 0; Counter < MDMATRIXMODELSList.length; Counter++)
            {
                var UnMDMATRIXMODELS = MDMATRIXMODELSList[Counter];
                SysCfg.Str.Protocol.WriteInt(UnMDMATRIXMODELS.STATUSN, Longitud, Texto);
                SysCfg.Str.Protocol.WriteStr(UnMDMATRIXMODELS.COMMENTSL, Longitud, Texto);
                SysCfg.Str.Protocol.WriteStr(UnMDMATRIXMODELS.GUIDET, Longitud, Texto);
                SysCfg.Str.Protocol.WriteInt(UnMDMATRIXMODELS.IDMDMODELTYPED2, Longitud, Texto);
                SysCfg.Str.Protocol.WriteBool(UnMDMATRIXMODELS.MODELVALIDATE, Longitud, Texto);
                SysCfg.Str.Protocol.WriteInt(UnMDMATRIXMODELS.POSINSTEP, Longitud, Texto);
            }

            Longitud = Longitud.Substring(0, Longitud.Length - 1);// para cortar la ultima coma 
            Res= _Version + "(" + Longitud + ")" + Texto;
        }
        catch (e)
        {
            ResErr.NotError = false;
            ResErr.Mesaje = "LIFESTATUStoStr:" + e;                
        }
        CASEMT_LIFESTATUS = Res;
        return Res;
    }
    this.toStrLIFESTATUS = function()
    {
        try
        {

                
        }
        catch (e)
        {
            ResErr.NotError = false;
            ResErr.Mesaje = "LIFESTATUStoStr:" + e;                                
        }
    }
    this.StrtoLIFESTATUS = function(ProtocoloStr)
    {
        CASEMT_LIFESTATUS = ProtocoloStr;
        var Res = false;
        var Longitud = new SysCfg.ref("");
        var Texto = new SysCfg.ref("");            
        var PosicionInicial = 0;            
        var Index = new SysCfg.ref(0);
        var Pos = new SysCfg.ref(0);
        try
        {
            Clear();
            if (_Version == ProtocoloStr.Substring(0, 3))
            {

                Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
                Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
                var LongitudArray = Longitud.split(",");


                var LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
                var IFCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
                var TUCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
                var ETCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
                var EFCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
                var STETCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
                var STEFCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
                var CSTETCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
                var CSTEFCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
                var MMCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);

                    
                for (var i = 0; i < LSCount; i++)
                {
                    var UnMDLIFESTATUS = new TMDLIFESTATUS();
                    UnMDLIFESTATUS.STATUSN = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
                    UnMDLIFESTATUS.NAMESTEP = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
                    UnMDLIFESTATUS.NEXTSTEP = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
                    UnMDLIFESTATUS.ENDSTEP = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
                    UnMDLIFESTATUS.COMMENTSST = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
                    UnMDLIFESTATUS.WARNINGSTEP = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
                    UnMDLIFESTATUS.LS_IDSDCASESTATUS = UsrCfg.SD.Properties.TSDCaseStatus.GetEnum(SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto));
                    UnMDLIFESTATUS.CAUTIONSTEP = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
                    MDLIFESTATUS_ListAdd(MDLIFESTATUSList, UnMDLIFESTATUS);                        
                }

                for (var i = 0; i < IFCount; i++)
                {
                    var UnMDINTERFACE = MDINTERFACE_ListSetID(MDINTERFACEList, SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto));
                    UnMDINTERFACE.CUSTOMCODE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
                    MDINTERFACE_ListAdd(MDINTERFACEList, UnMDINTERFACE);
                }


                for (var i = 0; i < TUCount; i++)
                {
                    var UnMDLIFESTATUSTYPEUSER = MDLIFESTATUSTYPEUSER_ListSetID(MDLIFESTATUSTYPEUSERList, SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto));                        
                    UnMDLIFESTATUSTYPEUSER.STATUSN = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
                    UnMDLIFESTATUSTYPEUSER.IDSDTYPEUSER = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
                    UnMDLIFESTATUSTYPEUSER.IDMDLIFESTATUSPERMISSION = Properties.TMDLIFESTATUSPERMISSION.GetEnum(SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto));                        
                    UnMDLIFESTATUSTYPEUSER.MDINTERFACE = MDINTERFACE_ListSetID(MDINTERFACEList, SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto));
                    UnMDLIFESTATUSTYPEUSER.ENABLETIME = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);
                    UnMDLIFESTATUSTYPEUSER.INCLUDEALLGROUP = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);
                    UnMDLIFESTATUSTYPEUSER.IDMDLIFESTATUSBEHAVIOR = Properties.TMDLIFESTATUSBEHAVIOR.GetEnum(SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto));
                    UnMDLIFESTATUSTYPEUSER.LSEND_COMMENTSENABLE = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);
                    UnMDLIFESTATUSTYPEUSER.LSBEGIN_COMMENTSENABLE = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);
                    UnMDLIFESTATUSTYPEUSER.LSCHANGE_COMMENTS = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
                    UnMDLIFESTATUSTYPEUSER.LSEND_COMMENTSLABEL = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
                    UnMDLIFESTATUSTYPEUSER.LSBEGIN_COMMENTSLABEL = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
                    UnMDLIFESTATUSTYPEUSER.LSHERESTEP_CONFIG = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
                    UnMDLIFESTATUSTYPEUSER.LSNEXTSTEP_CONFIG = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
                    MDLIFESTATUSTYPEUSER_ListAdd(MDLIFESTATUSTYPEUSERList, UnMDLIFESTATUSTYPEUSER);
                }
                for (var i = 0; i < ETCount; i++)
                {
                    var UnMDLIFESTATUSCIEXTRATABLE = MDLIFESTATUSCIEXTRATABLE_ListSetID(MDLIFESTATUSCIEXTRATABLEList,SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto));                        
                    UnMDLIFESTATUSCIEXTRATABLE.STATUSN = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
                    UnMDLIFESTATUSCIEXTRATABLE.IDCMDBCIDEFINE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
                    UnMDLIFESTATUSCIEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);                        
                    UnMDLIFESTATUSCIEXTRATABLE.IDMDLIFESTATUSTYPEUSER = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
                    UnMDLIFESTATUSCIEXTRATABLE.MDLIFESTATUSTYPEUSER = MDLIFESTATUSTYPEUSER_ListSetID(MDLIFESTATUSTYPEUSERList, UnMDLIFESTATUSCIEXTRATABLE.IDMDLIFESTATUSTYPEUSER);                        
                    UnMDLIFESTATUSCIEXTRATABLE.MDINTERFACE = MDINTERFACE_ListSetID(MDINTERFACEList, SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto));
                    //Indexacion                         
                    UnMDLIFESTATUSCIEXTRATABLE.CMDBCIDEFINE = CMDBCIDEFINE_ListSetID(CMDBCIDEFINEList, UnMDLIFESTATUSCIEXTRATABLE.IDCMDBCIDEFINE);
                    UnMDLIFESTATUSCIEXTRATABLE.CMDBCIDEFINE.CIDEFINE_NAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
                    CMDBCIDEFINE_ListAdd(CMDBCIDEFINEList, UnMDLIFESTATUSCIEXTRATABLE.CMDBCIDEFINE);
                        
                    //****
                    UnMDLIFESTATUSCIEXTRATABLE.CMDBCIDEFINEEXTRATABLE = CMDBCIDEFINEEXTRATABLE_ListSetID(CMDBCIDEFINEEXTRATABLEList, UnMDLIFESTATUSCIEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE);
                    UnMDLIFESTATUSCIEXTRATABLE.CMDBCIDEFINEEXTRATABLE.EXTRATABLE_NAMETABLE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
                    UnMDLIFESTATUSCIEXTRATABLE.CMDBCIDEFINEEXTRATABLE.EXTRATABLE_IDSOURCE = CMDB.Properties.TCMDBCIDEFINEEXTRATABLE_SOURCETYPE.GetEnum(SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto));
                    UnMDLIFESTATUSCIEXTRATABLE.CMDBCIDEFINEEXTRATABLE.EXTRATABLE_LOG = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);

                    UnMDLIFESTATUSCIEXTRATABLE.CMDBCIDEFINEEXTRATABLE.EXTRATABLE_NAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
                    UnMDLIFESTATUSCIEXTRATABLE.CMDBCIDEFINEEXTRATABLE.EXTRATABLE_DESCRIPTION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
                    CMDBCIDEFINEEXTRATABLE_ListAdd(CMDBCIDEFINEEXTRATABLEList, UnMDLIFESTATUSCIEXTRATABLE.CMDBCIDEFINEEXTRATABLE);


                    //SysCfg.Str.Protocol.WriteStr(UnMDLIFESTATUSCIEXTRATABLE.CMDBCIDEFINEEXTRATABLE.EXTRATABLE_NAMETABLE, Longitud, Texto);
                    //   SysCfg.Str.Protocol.WriteInt((Int32)UnMDLIFESTATUSCIEXTRATABLE.CMDBCIDEFINEEXTRATABLE.EXTRATABLE_IDSOURCE, Longitud, Texto);
                    //   SysCfg.Str.Protocol.WriteBool(UnMDLIFESTATUSCIEXTRATABLE.CMDBCIDEFINEEXTRATABLE.EXTRATABLE_LOG, Longitud, Texto);

                    //   SysCfg.Str.Protocol.WriteStr(UnMDLIFESTATUSCIEXTRATABLE.CMDBCIDEFINEEXTRATABLE.EXTRATABLE_NAME, Longitud, Texto);
                    //   SysCfg.Str.Protocol.WriteStr(UnMDLIFESTATUSCIEXTRATABLE.CMDBCIDEFINEEXTRATABLE.EXTRATABLE_DESCRIPTION, Longitud, Texto);
                    //****


                    MDLIFESTATUSCIEXTRATABLE_ListAdd(MDLIFESTATUSCIEXTRATABLEList, UnMDLIFESTATUSCIEXTRATABLE);
                }
                for (var i = 0; i < EFCount; i++)
                {
                    var UnMDLIFESTATUSCIEXTRAFIELDS = MDLIFESTATUSCIEXTRAFIELDS_ListSetID(MDLIFESTATUSCIEXTRAFIELDSList, SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto));                        
                    UnMDLIFESTATUSCIEXTRAFIELDS.IDMDLIFESTATUSCIEXTRATABLE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
                    UnMDLIFESTATUSCIEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
                    UnMDLIFESTATUSCIEXTRAFIELDS.IDMDLIFESTATUSPERMISSION = Properties.TMDLIFESTATUSPERMISSION.GetEnum(SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto));
                    UnMDLIFESTATUSCIEXTRAFIELDS.MANDATORY = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);
                    MDLIFESTATUSCIEXTRAFIELDS_ListAdd(MDLIFESTATUSCIEXTRAFIELDSList, UnMDLIFESTATUSCIEXTRAFIELDS);

                    var UnMDLIFESTATUSCIEXTRATABLE = MDLIFESTATUSCIEXTRATABLE_ListSetID(MDLIFESTATUSCIEXTRATABLEList, UnMDLIFESTATUSCIEXTRAFIELDS.IDMDLIFESTATUSCIEXTRATABLE);
                    UnMDLIFESTATUSCIEXTRAFIELDS.MDLIFESTATUSCIEXTRATABLE = UnMDLIFESTATUSCIEXTRATABLE;
                    MDLIFESTATUSCIEXTRAFIELDS_ListAdd(UnMDLIFESTATUSCIEXTRATABLE.MDLIFESTATUSCIEXTRAFIELDSList, UnMDLIFESTATUSCIEXTRAFIELDS);                        
                        

                }




                //*****************************
                for (var i = 0; i < STETCount; i++)
                {
                    var UnMDLIFESTATUSSTEXTRATABLE = MDLIFESTATUSSTEXTRATABLE_ListSetID(MDLIFESTATUSSTEXTRATABLEList, SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto));                        
                    UnMDLIFESTATUSSTEXTRATABLE.STATUSN = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
                    UnMDLIFESTATUSSTEXTRATABLE.IDMDSERVICETYPE = SD.Properties.TMDSERVICETYPE.GetEnum(SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto));
                    UnMDLIFESTATUSSTEXTRATABLE.IDMDSERVICEEXTRATABLE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);                        
                    UnMDLIFESTATUSSTEXTRATABLE.IDMDLIFESTATUSTYPEUSER = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
                    UnMDLIFESTATUSSTEXTRATABLE.MDLIFESTATUSTYPEUSER = MDLIFESTATUSTYPEUSER_ListSetID(MDLIFESTATUSTYPEUSERList, UnMDLIFESTATUSSTEXTRATABLE.IDMDLIFESTATUSTYPEUSER);                        
                    UnMDLIFESTATUSSTEXTRATABLE.MDINTERFACE = MDINTERFACE_ListSetID(MDINTERFACEList, SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto));
                    UnMDLIFESTATUSSTEXTRATABLE.LSCHANGETABLE_COMMENTS = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
                    //Indexacion                         
                    UnMDLIFESTATUSSTEXTRATABLE.MDSERVICEEXTRATABLE = MDSERVICEEXTRATABLE_ListSetID(MDSERVICEEXTRATABLEList, UnMDLIFESTATUSSTEXTRATABLE.IDMDSERVICEEXTRATABLE);                        
                    UnMDLIFESTATUSSTEXTRATABLE.MDSERVICEEXTRATABLE.EXTRATABLE_NAMETABLE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
                    UnMDLIFESTATUSSTEXTRATABLE.MDSERVICEEXTRATABLE.EXTRATABLE_IDSOURCE = (Properties.TMDSERVICEEXTRATABLE_SOURCETYPE) SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
                    UnMDLIFESTATUSSTEXTRATABLE.MDSERVICEEXTRATABLE.EXTRATABLE_LOG = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);

                    UnMDLIFESTATUSSTEXTRATABLE.MDSERVICEEXTRATABLE.EXTRATABLE_NAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
                    UnMDLIFESTATUSSTEXTRATABLE.MDSERVICEEXTRATABLE.EXTRATABLE_DESCRIPTION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
                        
                    MDSERVICEEXTRATABLE_ListAdd(MDSERVICEEXTRATABLEList, UnMDLIFESTATUSSTEXTRATABLE.MDSERVICEEXTRATABLE);
                    MDLIFESTATUSSTEXTRATABLE_ListAdd(MDLIFESTATUSSTEXTRATABLEList, UnMDLIFESTATUSSTEXTRATABLE);
                        
                }

                  
                for (var i = 0; i < STEFCount; i++)
                {
                    var UnMDLIFESTATUSSTEXTRAFIELDS = MDLIFESTATUSSTEXTRAFIELDS_ListSetID(MDLIFESTATUSSTEXTRAFIELDSList, SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto));                        
                    UnMDLIFESTATUSSTEXTRAFIELDS.IDMDLIFESTATUSSTEXTRATABLE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
                    UnMDLIFESTATUSSTEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
                    UnMDLIFESTATUSSTEXTRAFIELDS.IDMDLIFESTATUSPERMISSION = Properties.TMDLIFESTATUSPERMISSION.GetEnum(SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto));
                    UnMDLIFESTATUSSTEXTRAFIELDS.MANDATORY = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);
                    MDLIFESTATUSSTEXTRAFIELDS_ListAdd(MDLIFESTATUSSTEXTRAFIELDSList, UnMDLIFESTATUSSTEXTRAFIELDS);
                        
                    //indexacion
                    var UnMDLIFESTATUSSTEXTRATABLE = MDLIFESTATUSSTEXTRATABLE_ListSetID(MDLIFESTATUSSTEXTRATABLEList, UnMDLIFESTATUSSTEXTRAFIELDS.IDMDLIFESTATUSSTEXTRATABLE);
                    UnMDLIFESTATUSSTEXTRAFIELDS.MDLIFESTATUSSTEXTRATABLE = UnMDLIFESTATUSSTEXTRATABLE;
                    MDLIFESTATUSSTEXTRAFIELDS_ListAdd(UnMDLIFESTATUSSTEXTRATABLE.MDLIFESTATUSSTEXTRAFIELDSList, UnMDLIFESTATUSSTEXTRAFIELDS);
                }






                //*****************************
                for (var i = 0; i < CSTETCount; i++)
                {
                    var UnMDCASESTEXTRATABLE = MDCASESTEXTRATABLE_ListSetID(MDCASESTEXTRATABLEList, SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto));

                    UnMDCASESTEXTRATABLE.IDMDMODELTYPED = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
                    UnMDCASESTEXTRATABLE.IDMDSERVICETYPE = SD.Properties.TMDSERVICETYPE.GetEnum(SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto));
                    UnMDCASESTEXTRATABLE.IDMDSERVICEEXTRATABLE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);

                    UnMDCASESTEXTRATABLE.MDINTERFACE = MDINTERFACE_ListSetID(MDINTERFACEList, SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto));
                    UnMDCASESTEXTRATABLE.LSCHANGETABLE_COMMENTS = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
                    //Indexacion                         
                    UnMDCASESTEXTRATABLE.MDSERVICEEXTRATABLE = MDSERVICEEXTRATABLE_ListSetID(MDSERVICEEXTRATABLEList, UnMDCASESTEXTRATABLE.IDMDSERVICEEXTRATABLE);
                    UnMDCASESTEXTRATABLE.MDSERVICEEXTRATABLE.EXTRATABLE_NAMETABLE = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
                    UnMDCASESTEXTRATABLE.MDSERVICEEXTRATABLE.EXTRATABLE_IDSOURCE = (Properties.TMDSERVICEEXTRATABLE_SOURCETYPE)SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
                    UnMDCASESTEXTRATABLE.MDSERVICEEXTRATABLE.EXTRATABLE_LOG = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);

                    UnMDCASESTEXTRATABLE.MDSERVICEEXTRATABLE.EXTRATABLE_NAME = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
                    UnMDCASESTEXTRATABLE.MDSERVICEEXTRATABLE.EXTRATABLE_DESCRIPTION = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);

                    MDSERVICEEXTRATABLE_ListAdd(MDSERVICEEXTRATABLEList, UnMDCASESTEXTRATABLE.MDSERVICEEXTRATABLE);
                    MDCASESTEXTRATABLE_ListAdd(MDCASESTEXTRATABLEList, UnMDCASESTEXTRATABLE);

                }


                for (var i = 0; i < CSTEFCount; i++)
                {
                    var UnMDCASESTEXTRAFIELDS = MDCASESTEXTRAFIELDS_ListSetID(MDCASESTEXTRAFIELDSList, SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto));
                    UnMDCASESTEXTRAFIELDS.IDMDCASESTEXTRATABLE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
                    UnMDCASESTEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
                    UnMDCASESTEXTRAFIELDS.IDMDLIFESTATUSPERMISSION = Properties.TMDLIFESTATUSPERMISSION.GetEnum(SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto));
                    UnMDCASESTEXTRAFIELDS.MANDATORY = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);
                    MDCASESTEXTRAFIELDS_ListAdd(MDCASESTEXTRAFIELDSList, UnMDCASESTEXTRAFIELDS);

                    //indexacion
                    var UnMDCASESTEXTRATABLE = MDCASESTEXTRATABLE_ListSetID(MDCASESTEXTRATABLEList, UnMDCASESTEXTRAFIELDS.IDMDCASESTEXTRATABLE);
                    UnMDCASESTEXTRAFIELDS.MDCASESTEXTRATABLE = UnMDCASESTEXTRATABLE;
                    MDCASESTEXTRAFIELDS_ListAdd(UnMDCASESTEXTRATABLE.MDCASESTEXTRAFIELDSList, UnMDCASESTEXTRAFIELDS);
                }




                for (var i = 0; i < MMCount; i++)
                {
                    var UnMDMATRIXMODELS = new TMDMATRIXMODELS();
                    UnMDMATRIXMODELS.STATUSN = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
                    UnMDMATRIXMODELS.COMMENTSL= SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
                    UnMDMATRIXMODELS.GUIDET= SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
                    UnMDMATRIXMODELS.IDMDMODELTYPED2 = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
                    UnMDMATRIXMODELS.MODELVALIDATE = SysCfg.Str.Protocol.ReadBool(Pos, Index, LongitudArray, Texto);
                    UnMDMATRIXMODELS.POSINSTEP = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
                    MDMATRIXMODELS_ListAdd(MDMATRIXMODELSList, UnMDMATRIXMODELS);
                }
              
                 
                ReationLS();
               
              
            }
        }
        catch (e)
        {
            ResErr.NotError = false;
            ResErr.Mesaje = "LIFESTATUStoStr:" + e;         
        }
        return (Res);
    }

this.Clear()
{
    CASEMT_LIFESTATUS = "";
    MDLIFESTATUSTYPEUSERList.Clear();
    MDLIFESTATUSList.Clear();
    MDMATRIXMODELSList.Clear();
    CMDBCIDEFINEList.Clear();
    CMDBCIDEFINEEXTRAFIELDSList.Clear();
    CMDBCIDEFINEEXTRATABLEList.Clear();
    MDSERVICEEXTRAFIELDSList.Clear();
    MDSERVICEEXTRATABLEList.Clear();
}

[DataContract]//rev
public class TNEXTSTEP_CONFIG
{
    [DataMember]
    public Int32 STATUSN { get; set; }
    [DataMember]
    public String NAMESTEP { get; set; }
    [DataMember]            
    public Boolean Enable { get; set; }
    [DataMember]
    public String Caption { get; set; }
}

[DataContract]//rev
public class THERESTEP_CONFIG
{
    [DataMember]
    public Boolean ShowLogin { get; set; }
    [DataMember]
    public Boolean Hide      { get; set; }
}





[DataContract]//rev
public class TMDLIFESTATUS
{
    [DataMember]
    public Int32 IDMDLIFESTATUS = 0;
    [DataMember]
    public Int32 STATUSN = 0;
    [DataMember]
    public String NAMESTEP = "";
    [DataMember]
    public String NEXTSTEP = "";
    [DataMember]
    public String ENDSTEP = "";
    [DataMember]
    public String COMMENTSST = "";
    [DataMember]
    public String CAUTIONSTEP = "";
    [DataMember]
    public String WARNINGSTEP = "";
    [DataMember]
    public UsrCfg.SD.Properties.TSDCaseStatus LS_IDSDCASESTATUS = Properties.TSDCaseStatus._InProgress;
    //[DataMember]//Recurrente
    public List<TNEXTLIFESTATUS> NEXTLIFESTATUSList = new List<TNEXTLIFESTATUS>();
    //[DataMember]//Recurrente
    public List<TMDLIFESTATUS> ENDLIFESTATUSList = new List<TMDLIFESTATUS>();
    [DataMember]
    public List<TMDLIFESTATUSTYPEUSER> MDLIFESTATUSTYPEUSERList = new List<TMDLIFESTATUSTYPEUSER>();
    [DataMember]
    public List<TMDMATRIXMODELS> MDMATRIXMODELSList = new List<TMDMATRIXMODELS>();
    [DataMember]
    public List<TCMDBCIDEFINE> CMDBCIDEFINEList = new List<TCMDBCIDEFINE>();
    [DataMember]
    public List<TMDLIFESTATUSCIEXTRAFIELDS> MDLIFESTATUSCIEXTRAFIELDSList = new List<TMDLIFESTATUSCIEXTRAFIELDS>();
    [DataMember]
    public List<TMDLIFESTATUSSTEXTRAFIELDS> MDLIFESTATUSSTEXTRAFIELDSList = new List<TMDLIFESTATUSSTEXTRAFIELDS>();
    [DataMember]
    public List<TMDLIFESTATUSCIEXTRATABLE> MDLIFESTATUSCIEXTRATABLEList = new List<TMDLIFESTATUSCIEXTRATABLE>();
    [DataMember]
    public List<TMDLIFESTATUSSTEXTRATABLE> MDLIFESTATUSSTEXTRATABLEList = new List<TMDLIFESTATUSSTEXTRATABLE>();
}


//[DataMember]//Recurrente
public class TNEXTLIFESTATUS
{
    //[DataMember]//Recurrente
    public List<TMDLIFESTATUS> NEXTSTEPList = new List<TMDLIFESTATUS>();
}


[DataContract]
public class TMDLIFESTATUSTYPEUSER//rev
{
    [DataMember]
    public Int32 IDMDLIFESTATUS;
    //[DataMember]//Recurrente
    public TMDLIFESTATUS MDLIFESTATUS;
    [DataMember]
    public Int32 IDMDLIFESTATUSTYPEUSER;
    [DataMember]
    public Int32 STATUSN;
    //[DataMember]
    //public Int32 IDMDINTERFACE;//Slash
    [DataMember]
    public TMDINTERFACE MDINTERFACE;
    [DataMember]
    public Boolean ENABLETIME;
    [DataMember]
    public Boolean INCLUDEALLGROUP;
    [DataMember]
    public UsrCfg.SD.Properties.TMDLIFESTATUSBEHAVIOR IDMDLIFESTATUSBEHAVIOR;
    [DataMember]
    public Boolean LSEND_COMMENTSENABLE;
    [DataMember]
    public Boolean LSBEGIN_COMMENTSENABLE;
    [DataMember]
    public String LSCHANGE_COMMENTS;
    [DataMember]
    public String LSEND_COMMENTSLABEL;
    [DataMember]
    public String LSBEGIN_COMMENTSLABEL;
    [DataMember]
    public String LSHERESTEP_CONFIG;
    [DataMember]
    public THERESTEP_CONFIG HERESTEP_CONFIG = new THERESTEP_CONFIG();
    [DataMember]
    public String LSNEXTSTEP_CONFIG;
    [DataMember]
    public List<TNEXTSTEP_CONFIG> NEXTSTEP_CONFIGList = new List<TNEXTSTEP_CONFIG>();
    [DataMember]
    public UsrCfg.SD.Properties.TMDLIFESTATUSPERMISSION IDMDLIFESTATUSPERMISSION;
    [DataMember]
    public Int32 IDSDTYPEUSER;
}

[DataContract]
public class TMDLIFESTATUSCIEXTRAFIELDS//rev
{
    [DataMember]
    public Int32 IDMDLIFESTATUSCIEXTRAFIELDS;
    [DataMember]
    public Int32 IDCMDBCIDEFINEEXTRAFIELDS;
    [DataMember]
    public UsrCfg.SD.Properties.TMDLIFESTATUSPERMISSION IDMDLIFESTATUSPERMISSION;
    [DataMember]
    public TCMDBCIDEFINEEXTRAFIELDS CMDBCIDEFINEEXTRAFIELDS;
    [DataMember]
    public Boolean MANDATORY;
    [DataMember]
    public Int32 IDMDLIFESTATUSCIEXTRATABLE;
    //[DataMember]//Recurrente
    public TMDLIFESTATUSCIEXTRATABLE MDLIFESTATUSCIEXTRATABLE;            
}


[DataContract]//(IsReference = true)
public class TMDLIFESTATUSCIEXTRATABLE//rev
{
    [DataMember]
    public Int32 IDMDLIFESTATUS;
    //[DataMember]//Recurrente
    public TMDLIFESTATUS MDLIFESTATUS;
    [DataMember]
    public Int32 STATUSN;
    [DataMember]
    public Int32 IDMDLIFESTATUSCIEXTRATABLE;
    [DataMember]
    public Int32 IDCMDBCIDEFINE;
    [DataMember]
    public Int32 IDCMDBCIDEFINEEXTRATABLE;
    [DataMember]
    public Int32 IDMDLIFESTATUSTYPEUSER;
    [DataMember]
    public TCMDBCIDEFINE CMDBCIDEFINE;
    [DataMember]
    public TCMDBCIDEFINEEXTRATABLE CMDBCIDEFINEEXTRATABLE;
    //[DataMember]
    //public Int32 IDMDINTERFACE;//Slash
    [DataMember]
    public TMDINTERFACE MDINTERFACE;
    [DataMember]
    public TMDLIFESTATUSTYPEUSER MDLIFESTATUSTYPEUSER;
    [DataMember]
    public List<TMDLIFESTATUSCIEXTRAFIELDS> MDLIFESTATUSCIEXTRAFIELDSList = new List<TMDLIFESTATUSCIEXTRAFIELDS>();
}

//*********************************************************************************************************************************
[DataContract]//(IsReference = true)
public class TMDLIFESTATUSSTEXTRAFIELDS
{
    [DataMember]
    public Int32 IDMDLIFESTATUSSTEXTRAFIELDS;
    [DataMember]
    public Int32 IDMDSERVICEEXTRAFIELDS;
    [DataMember]
    public UsrCfg.SD.Properties.TMDLIFESTATUSPERMISSION IDMDLIFESTATUSPERMISSION;
    [DataMember]
    public TMDSERVICEEXTRAFIELDS MDSERVICEEXTRAFIELDS;
    [DataMember]
    public Int32 IDMDLIFESTATUSSTEXTRATABLE;
    [DataMember]
    public Boolean MANDATORY;
    //[DataMember]//Recurrente
    public TMDLIFESTATUSSTEXTRATABLE MDLIFESTATUSSTEXTRATABLE;
}

[DataContract]
public class TMDCASESTEXTRAFIELDS
{
    [DataMember]
    public Int32 IDMDCASESTEXTRAFIELDS;
    [DataMember]
    public Int32 IDMDSERVICEEXTRAFIELDS;
    [DataMember]
    public UsrCfg.SD.Properties.TMDLIFESTATUSPERMISSION IDMDLIFESTATUSPERMISSION;
    [DataMember]
    public TMDSERVICEEXTRAFIELDS MDSERVICEEXTRAFIELDS;
    [DataMember]
    public Int32 IDMDCASESTEXTRATABLE;
    [DataMember]
    public Boolean MANDATORY;
    //[DataMember]//Recurrente
    public TMDCASESTEXTRATABLE MDCASESTEXTRATABLE;
}



[DataContract]
public class TMDLIFESTATUSSTEXTRATABLE//rev
{
    [DataMember]
    public Int32 IDMDLIFESTATUS;
    //[DataMember]//Recurrente
    public TMDLIFESTATUS MDLIFESTATUS;
    [DataMember]
    public Int32 STATUSN;
    [DataMember]
    public Int32 IDMDLIFESTATUSSTEXTRATABLE;
    [DataMember]
    public SD.Properties.TMDSERVICETYPE IDMDSERVICETYPE;
    [DataMember]
    public Int32 IDMDSERVICEEXTRATABLE;
    [DataMember]
    public Int32 IDMDLIFESTATUSTYPEUSER;
    //[DataMember]
    //public Int32 IDMDINTERFACE;//Slash
    [DataMember]
    public TMDINTERFACE MDINTERFACE;
    [DataMember]
    public String LSCHANGETABLE_COMMENTS;
    [DataMember]
    public TMDSERVICEEXTRATABLE MDSERVICEEXTRATABLE;
    [DataMember]
    public TMDLIFESTATUSTYPEUSER MDLIFESTATUSTYPEUSER;
    [DataMember]
    public List<TMDLIFESTATUSSTEXTRAFIELDS> MDLIFESTATUSSTEXTRAFIELDSList = new List<TMDLIFESTATUSSTEXTRAFIELDS>();
}


[DataContract]
public class TMDCASESTEXTRATABLE//rev
{
    [DataMember]
    public Int32 IDMDMODELTYPED;
    [DataMember]
    public Int32 IDMDCASESTEXTRATABLE;
    [DataMember]
    public SD.Properties.TMDSERVICETYPE IDMDSERVICETYPE;
    [DataMember]
    public Int32 IDMDSERVICEEXTRATABLE;
    //[DataMember]
    //public Int32 IDMDINTERFACE;//Slash
    [DataMember]
    public TMDINTERFACE MDINTERFACE;
    [DataMember]
    public String LSCHANGETABLE_COMMENTS;
    [DataMember]
    public TMDSERVICEEXTRATABLE MDSERVICEEXTRATABLE;
    //[DataMember]
    public List<TMDCASESTEXTRAFIELDS> MDCASESTEXTRAFIELDSList = new List<TMDCASESTEXTRAFIELDS>();
}




[DataContract]
public class TMDMATRIXMODELS//rev
{
    [DataMember]
    public Int32 IDMDLIFESTATUS = 0;
    //[DataMember]//Recurrente
    public TMDLIFESTATUS MDLIFESTATUS;
    [DataMember]
    public Int32 STATUSN = 0;
    [DataMember]
    public String COMMENTSL = "";
    [DataMember]
    public String GUIDET = "";
    [DataMember]
    public Int32 IDMDMATRIXMODELS = 0;
    [DataMember]
    public Int32 IDMDMODELTYPED1 = 0;
    [DataMember]
    public Int32 IDMDMODELTYPED2 = 0;
    [DataMember]
    public Boolean MODELVALIDATE = false;
    [DataMember]
    public Int32 POSINSTEP = 0;
}

[DataMember]
public List<TMDLIFESTATUS> MDLIFESTATUSList = new List<TMDLIFESTATUS>();
[DataMember]
public List<TMDLIFESTATUSTYPEUSER> MDLIFESTATUSTYPEUSERList = new List<TMDLIFESTATUSTYPEUSER>();
[DataMember]
public List<TMDLIFESTATUSCIEXTRAFIELDS> MDLIFESTATUSCIEXTRAFIELDSList = new List<TMDLIFESTATUSCIEXTRAFIELDS>();
[DataMember]
public List<TMDLIFESTATUSSTEXTRAFIELDS> MDLIFESTATUSSTEXTRAFIELDSList = new List<TMDLIFESTATUSSTEXTRAFIELDS>();
[DataMember]
public List<TMDCASESTEXTRAFIELDS> MDCASESTEXTRAFIELDSList = new List<TMDCASESTEXTRAFIELDS>();
[DataMember]
public List<TMDLIFESTATUSCIEXTRATABLE> MDLIFESTATUSCIEXTRATABLEList = new List<TMDLIFESTATUSCIEXTRATABLE>();
[DataMember]
public List<TMDLIFESTATUSSTEXTRATABLE> MDLIFESTATUSSTEXTRATABLEList = new List<TMDLIFESTATUSSTEXTRATABLE>();
[DataMember]
public List<TMDCASESTEXTRATABLE> MDCASESTEXTRATABLEList = new List<TMDCASESTEXTRATABLE>();
[DataMember]
public List<TMDINTERFACE> MDINTERFACEList = new List<TMDINTERFACE>();
[DataMember]
public List<TMDMATRIXMODELS> MDMATRIXMODELSList = new List<TMDMATRIXMODELS>();
[DataMember]
public List<TCMDBCIDEFINE> CMDBCIDEFINEList = new List<TCMDBCIDEFINE>();
[DataMember]
public List<TCMDBCIDEFINEEXTRAFIELDS> CMDBCIDEFINEEXTRAFIELDSList = new List<TCMDBCIDEFINEEXTRAFIELDS>();
[DataMember]
public List<TCMDBCIDEFINEEXTRATABLE> CMDBCIDEFINEEXTRATABLEList = new List<TCMDBCIDEFINEEXTRATABLE>();
[DataMember]
public List<TMDSERVICEEXTRAFIELDS> MDSERVICEEXTRAFIELDSList = new List<TMDSERVICEEXTRAFIELDS>();
[DataMember]
public List<TMDSERVICEEXTRATABLE> MDSERVICEEXTRATABLEList = new List<TMDSERVICEEXTRATABLE>();

[DataContract]
public class TCMDBCIDEFINE//rev
{
    [DataMember]
    public Int32 IDCMDBCIDEFINE;
    [DataMember]
    public String CIDEFINE_NAME;//Slash
    [DataMember]
    public List<TCMDBCIDEFINEEXTRATABLE> CMDBCIDEFINEEXTRATABLElist = new List<TCMDBCIDEFINEEXTRATABLE>();
}

[DataContract]
public class TCMDBCIDEFINEEXTRATABLE//rev
{
    [DataMember]
    public Int32 IDCMDBCIDEFINEEXTRATABLE;
    //[DataMember]//Recurrente
    public TCMDBCIDEFINE CMDBCIDEFINE;
    [DataMember]
    public string EXTRATABLE_NAMETABLE;//Slash            
    [DataMember]
    public UsrCfg.CMDB.Properties.TCMDBCIDEFINEEXTRATABLE_SOURCETYPE EXTRATABLE_IDSOURCE;//Slash
    [DataMember]
    public Boolean EXTRATABLE_LOG;
    [DataMember]
    public string EXTRATABLE_NAME;
    [DataMember]
    public string EXTRATABLE_DESCRIPTION;
    [DataMember]
    public List<TCMDBCIDEFINEEXTRAFIELDS> CMDBCIDEFINEEXTRAFIELDSList = new List<TCMDBCIDEFINEEXTRAFIELDS>();
}
[DataContract]
public class TCMDBCIDEFINEEXTRAFIELDS//rev
{
    //[DataMember]//Recurrente
    public TCMDBCIDEFINEEXTRATABLE CMDBCIDEFINEEXTRATABLE;
    [DataMember]
    public Int32 IDCMDBCIDEFINEEXTRAFIELDS;
    //[DataMember]//Recurrente
    public TCMDBCIDEFINE CMDBCIDEFINE;
}
[DataContract]
public class TMDSERVICEEXTRATABLE//rev
{
    [DataMember]
    public Int32 IDMDSERVICEEXTRATABLE;
    [DataMember]
    public string EXTRATABLE_NAMETABLE;//Slash            
    [DataMember]
    public UsrCfg.SD.Properties.TMDSERVICEEXTRATABLE_SOURCETYPE EXTRATABLE_IDSOURCE;//Slash
    [DataMember]
    public Boolean EXTRATABLE_LOG;
    [DataMember]
    public string EXTRATABLE_NAME;
    [DataMember]
    public string EXTRATABLE_DESCRIPTION;
    [DataMember]
    public UsrCfg.SD.Properties.TMDSERVICETYPE MDSERVICETYPE;
    [DataMember]
    public List<TMDSERVICEEXTRAFIELDS> MDSERVICEEXTRAFIELDSList = new List<TMDSERVICEEXTRAFIELDS>();
}
[DataContract]
public class TMDSERVICEEXTRAFIELDS//rev
{
    //[DataMember]//Recurrente
    public TMDSERVICEEXTRATABLE MDSERVICEEXTRATABLE;
    [DataMember]
    public Int32 IDMDSERVICEEXTRAFIELDS;
    [DataMember]
    public UsrCfg.SD.Properties.TMDSERVICETYPE MDSERVICETYPE;
}
[DataContract]
public class TMDINTERFACE//rev
{
    [DataMember]
    public Int32 IDMDINTERFACE;
    [DataMember]
    public String CUSTOMCODE;            
}

    
//public static THERESTEP_CONFIG GetHERESTEP_CONFIGbyCode(String Code)
//{
//    THERESTEP_CONFIG HERESTEP_CONFIG = new THERESTEP_CONFIG { ShowLogin = false, Hide = false };
//    try
//    {
//        String[] Arr2 = Code.Split(Convert.ToChar(","));
//        if (Arr2.Length > 0 && Arr2 != null)
//        {
//            String[] Arr3 = Arr2[0].Split(Convert.ToChar("="));
//            if (Arr3.Length > 0 && Arr3 != null)
//            {
//                HERESTEP_CONFIG.ShowLogin = Arr3[1] != null ? Convert.ToBoolean(Convert.ToInt32(Arr3[1])) : false;
//            }
    
//            String[] Arr4 = Arr2[1].Split(Convert.ToChar("="));
//            if (Arr4.Length > 0 && Arr4 != null)
//            {
//                HERESTEP_CONFIG.ShowLogin = Arr3[1] != null ? Convert.ToBoolean(Convert.ToInt32(Arr3[1])) : false;
//            }
//        }
//    }
//    catch (Exception)
//    {
    
    
//    }
//    return HERESTEP_CONFIG;
//}
    
    
    
            
//public static List<TNEXTSTEP_CONFIG> GetNEXTSTEP_CONFIGbyCode(String Code)
//{
                
//    List<TNEXTSTEP_CONFIG> lista = new List<TNEXTSTEP_CONFIG>();
//    try
//    {
//        String[] Arr1 = Code.Split(Convert.ToChar(";"));
//    if (Arr1.Length > 0 && Arr1 != null)
//    {
//        for (var i = 0; i < Arr1.Length; i++)
//        {
//            TNEXTSTEP_CONFIG NEXTSTEP_CONFIG = new TNEXTSTEP_CONFIG();
//            NEXTSTEP_CONFIG.STATUSN = Arr1[i] != null ? Convert.ToInt32(Arr1[i].Substring(0,1)) : 0;
    
//            String[] Arr2 = Arr1[i].Split(Convert.ToChar(","));
//            if (Arr2.Length > 0 && Arr2 != null)
//            {
//                String[] Arr3 = Arr2[0].Split(Convert.ToChar("="));
//                if(Arr3.Length > 0 && Arr3 != null)
//                {
//                    NEXTSTEP_CONFIG.Enable = Arr3[1] != null ? Convert.ToBoolean(Convert.ToInt32(Arr3[1])) : false;
//                }
    
//                String[] Arr4 = Arr2[1].Split(Convert.ToChar("="));
//                if (Arr4.Length > 0 && Arr4 != null)
//                {
//                    NEXTSTEP_CONFIG.Caption = Arr4[1] != null ? Convert.ToString(Arr4[1].Substring(0, Arr4[1].Length-1)) : string.Empty;
//                }
//            }
//            lista.Add(NEXTSTEP_CONFIG);
//        }
                    
//    }
//    }
//    catch (Exception)
//    {
                    
                   
//    }
//    return lista;
//}
      
            

public static List<TNEXTSTEP_CONFIG> GetNEXTSTEP_CONFIGbyCode(String Code)
{
    //4[Enable=1,Caption=Yes];5[Enable=1,Caption=No]
    //4[Enable=1,Caption=Yes];5[Caption=No,Enable=1]
    //4[Enable=1,Caption=Yes];5[Enable=1]

List<TNEXTSTEP_CONFIG> lista = new List<TNEXTSTEP_CONFIG>();
try
{
    if (Code.Length > 0)
    {
        String[] Arr1 = Code.Split(Convert.ToChar(";"));

        if (Arr1.Length > 0 && Arr1 != null)
        {

            for (var i = 0; i < Arr1.Length; i++)
            {

                try
                {
                    TNEXTSTEP_CONFIG oTNEXTSTEP_CONFIG = new TNEXTSTEP_CONFIG() { STATUSN = 0, NAMESTEP = string.Empty, Enable = true, Caption = string.Empty };
                    oTNEXTSTEP_CONFIG.STATUSN = Arr1[i] != null ? Convert.ToInt32(Arr1[i].Substring(0, 1)) : 0;

                    int Ini = Arr1[i].IndexOf("[") + 1;
                    int Fin = Arr1[i].IndexOf("]", Ini);
                    Arr1[i] = Arr1[i].Substring(Ini, Fin - Ini);

                    String[] Arr2 = Arr1[i].Split(Convert.ToChar(","));
                    if (Arr2.Length > 0 && Arr2 != null)
                    {
                        for (var j = 0; j < Arr2.Length; j++)
                        {
                            try
                            {
                                String[] Arr3 = Arr2[j].Split(Convert.ToChar("="));
                                if (Arr3.Length > 0 && Arr3 != null)
                                {
                                    BindingFlags flags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.Static;

                                    foreach (PropertyInfo item in typeof(TNEXTSTEP_CONFIG).GetProperties(flags))
                                    {
                                        if (item.Name.ToUpper() == Arr3[0].ToUpper())
                                    {
                                        object value = Arr3[1];

                                        if ((item.PropertyType == typeof(Boolean) || item.PropertyType == typeof(bool)) && (Arr3[1] == "1" || Arr3[1] == "0"))
                                            value = (bool)(Convert.ToInt32(Arr3[1]) == 1);
                                        item.SetValue(oTNEXTSTEP_CONFIG, value, null);
                                        break;
                                    }
                                }
                            }


                        }
                            catch (Exception e)
                    {

                                SysCfg.Log.Methods.WriteLog("SDLifeStatus.js ", e);
                    }
                }
            }
            lista.Add(oTNEXTSTEP_CONFIG);

        }
                catch (Exception e)
    {
                    SysCfg.Log.Methods.WriteLog("SDLifeStatus.js", e);

    }





}
}
}
}
catch (Exception e)
{
SysCfg.Log.Methods.WriteLog("", e);
               
}
            
return lista;
}

public static THERESTEP_CONFIG GetHERESTEP_CONFIGbyCode(String Code)
{
//ShowLogin=1,Hide=0
//Hide=0,ShowLogin=1
THERESTEP_CONFIG HERESTEP_CONFIG = new THERESTEP_CONFIG { ShowLogin = false, Hide = false };
try
{
    if (Code.Length > 0)
    {
        String[] Arr2 = Code.Split(Convert.ToChar(","));
        if (Arr2.Length > 0 && Arr2 != null)
        {
            for (var j = 0; j < Arr2.Length; j++)
            {
                try
                {
                    String[] Arr3 = Arr2[j].Split(Convert.ToChar("="));
                    if (Arr3.Length > 0 && Arr3 != null)
                    {
                        BindingFlags flags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.Static;
                        foreach (PropertyInfo item in typeof(THERESTEP_CONFIG).GetProperties(flags))
                        {
                            if (item.Name.ToUpper() == Arr3[0].ToUpper())
                        {
                            object value = Arr3[1];

                            if ((item.PropertyType == typeof(Boolean) || item.PropertyType == typeof(bool)) && (Arr3[1] == "1" || Arr3[1] == "0"))
                                value = (bool)(Convert.ToInt32(Arr3[1])==1);
                                             
                            item.SetValue(HERESTEP_CONFIG, value, null);
                            break;
                        }
                    }
                }

            }
            catch (Exception e)
        {
                SysCfg.Log.Methods.WriteLog("SDLifeStatus.js", e);
                     
        }

    }
}
}
}
catch (Exception e)
{
SysCfg.Log.Methods.WriteLog("", e);

}
return HERESTEP_CONFIG;
}








public void ReationLS()
{






    for (var i = 0; i < MDLIFESTATUSList.length; i++)
    {
        if (i != -1)
        {
            try
            {
                //********* NEXTSTEP ***********
                //ejemplo de protocolo
                //"(2-3),4,"
                //"(se ejecutan 2 y 3 en paralelo),se pasa al 4,se sale por estar vacio"                        
                //considera un blanco como step exepto cuando es multiple
                String NEXTSTEPstr = MDLIFESTATUSList[i].NEXTSTEP.Replace(" ", "");
                foreach (string Token in NEXTSTEPstr.Split(','))
            {
                    TNEXTLIFESTATUS NEXTLIFESTATUS = new TNEXTLIFESTATUS();
                if ((Token.IndexOf("(") != -1) && (Token.IndexOf(")") != -1))
                {
                    foreach (string TokenSub in SysCfg.Str.Methods.CopyExtract(Token, @"(", @")").Split('-'))
                    {
                        if (TokenSub != "") MDLIFESTATUS_ListAdd(NEXTLIFESTATUS.NEXTSTEPList, MDLIFESTATUS_ListSetID(MDLIFESTATUSList, int.Parse(TokenSub)));
                }
            }
        else if ((Token.IndexOf("(") == -1) || (Token.IndexOf(")") == -1))
            {
                if (Token != "") MDLIFESTATUS_ListAdd(NEXTLIFESTATUS.NEXTSTEPList, MDLIFESTATUS_ListSetID(MDLIFESTATUSList, int.Parse(Token)));

            }                            
            MDLIFESTATUSList[i].NEXTLIFESTATUSList.Add(NEXTLIFESTATUS);                            
        }



        //expluye los blancos siempre 
    
        //String NEXTSTEPstr = MDLIFESTATUSList[i].NEXTSTEP.Replace(" ", "");
        //foreach (string Token in NEXTSTEPstr.Split(','))
        //{
        //    if (Token != "")
        //    {
        //        TNEXTLIFESTATUS NEXTLIFESTATUS = new TNEXTLIFESTATUS();
        //        if ((Token.IndexOf("(") != -1) && (Token.IndexOf(")") != -1) && (Token.IndexOf(",") == -1))
        //        {
        //            foreach (string TokenSub in SysCfg.Str.Methods.CopyExtract(Token, @"(", @")").Split('-'))
        //            {
        //                 if (TokenSub != "") 
        //                 MDLIFESTATUS_ListAdd(NEXTLIFESTATUS.NEXTSTEPList, MDLIFESTATUS_ListSetID(MDLIFESTATUSList, int.Parse(TokenSub)));
        //            }
        //        }
        //        else if ((Token.IndexOf("(") == -1) || (Token.IndexOf(")") == -1) || (Token.IndexOf("-") == -1))
        //        {
        //            MDLIFESTATUS_ListAdd(NEXTLIFESTATUS.NEXTSTEPList, MDLIFESTATUS_ListSetID(MDLIFESTATUSList, int.Parse(Token)));

        //        }
        //        if (NEXTLIFESTATUS.NEXTSTEPList.Count > 0)  MDLIFESTATUSList[i].NEXTLIFESTATUSList.Add(NEXTLIFESTATUS);
        //    }
        //}
        //********* ENDSTEP ***********
        String ENDSTEPstr = MDLIFESTATUSList[i].ENDSTEP.Replace(" ", "");
        foreach (string Token in MDLIFESTATUSList[i].ENDSTEP.Split(','))
        {
            if (Token != "")
        {
            MDLIFESTATUS_ListAdd(MDLIFESTATUSList[i].ENDLIFESTATUSList, MDLIFESTATUS_ListSetID(MDLIFESTATUSList, int.Parse(Token)));
        }
    }
    //********* MDLIFESTATUSTYPEUSER ***********
    foreach (TMDLIFESTATUSTYPEUSER MDLIFESTATUSTYPEUSER in MDLIFESTATUSTYPEUSERList)
    {
        if ((MDLIFESTATUSTYPEUSER.STATUSN == MDLIFESTATUSList[i].STATUSN))
    {




        MDLIFESTATUSTYPEUSER.MDLIFESTATUS = MDLIFESTATUSList[i];

        MDLIFESTATUSTYPEUSER.HERESTEP_CONFIG = GetHERESTEP_CONFIGbyCode(MDLIFESTATUSTYPEUSER.LSHERESTEP_CONFIG);//********** aqui 
                                

        for (var LSLCounter = 0; LSLCounter < MDLIFESTATUSList[i].NEXTLIFESTATUSList.length; LSLCounter++)
        {
            for (var NLCounter = 0; NLCounter < MDLIFESTATUSList[i].NEXTLIFESTATUSList[LSLCounter].NEXTSTEPList.length; NLCounter++)
            {


                TNEXTSTEP_CONFIG NEXTSTEP_CONFIG = new TNEXTSTEP_CONFIG { STATUSN = MDLIFESTATUSList[i].NEXTLIFESTATUSList[LSLCounter].NEXTSTEPList[NLCounter].STATUSN, NAMESTEP = MDLIFESTATUSList[i].NEXTLIFESTATUSList[LSLCounter].NEXTSTEPList[NLCounter].NAMESTEP, Enable = true, Caption = MDLIFESTATUSList[i].NEXTLIFESTATUSList[LSLCounter].NEXTSTEPList[NLCounter].NAMESTEP };//********** aqui 
                List<TNEXTSTEP_CONFIG> NEXTSTEP_CONFIGList = GetNEXTSTEP_CONFIGbyCode(MDLIFESTATUSTYPEUSER.LSNEXTSTEP_CONFIG); //********** aqui 
                                        


                for (var NST = 0; NST < NEXTSTEP_CONFIGList.length; NST++)
                {
                    if (NEXTSTEP_CONFIGList[NST].STATUSN == MDLIFESTATUSList[i].NEXTLIFESTATUSList[LSLCounter].NEXTSTEPList[NLCounter].STATUSN)
                    {
                        NEXTSTEP_CONFIG.Caption = NEXTSTEP_CONFIGList[NST].Caption;
                        NEXTSTEP_CONFIG.Enable = NEXTSTEP_CONFIGList[NST].Enable;

                    }
                }
                MDLIFESTATUSTYPEUSER.NEXTSTEP_CONFIGList.Add(NEXTSTEP_CONFIG);


            }
        }



        //**********************************

        MDLIFESTATUSTYPEUSER_ListAdd(MDLIFESTATUSList[i].MDLIFESTATUSTYPEUSERList, MDLIFESTATUSTYPEUSER);
    }
}
//********* MDMATRIXMODELS ***********
foreach (TMDMATRIXMODELS MDMATRIXMODELS in MDMATRIXMODELSList)
{
if ((MDMATRIXMODELS.STATUSN == MDLIFESTATUSList[i].STATUSN))
{
    MDMATRIXMODELS.MDLIFESTATUS = MDLIFESTATUSList[i];
    MDMATRIXMODELS_ListAdd(MDLIFESTATUSList[i].MDMATRIXMODELSList, MDMATRIXMODELS);
}
}

//********* MDLIFESTATUSCIEXTRAFIELDS ***********
foreach (TMDLIFESTATUSCIEXTRAFIELDS MDLIFESTATUSCIEXTRAFIELDS in MDLIFESTATUSCIEXTRAFIELDSList)
{
if (MDLIFESTATUSCIEXTRAFIELDS.MDLIFESTATUSCIEXTRATABLE.STATUSN == MDLIFESTATUSList[i].STATUSN)
{
    MDLIFESTATUSCIEXTRAFIELDS.MDLIFESTATUSCIEXTRATABLE.MDLIFESTATUS = MDLIFESTATUSList[i];
    MDLIFESTATUSCIEXTRAFIELDS_ListAdd(MDLIFESTATUSList[i].MDLIFESTATUSCIEXTRAFIELDSList, MDLIFESTATUSCIEXTRAFIELDS);
    MDLIFESTATUSCIEXTRATABLE_ListAdd(MDLIFESTATUSList[i].MDLIFESTATUSCIEXTRATABLEList, MDLIFESTATUSCIEXTRAFIELDS.MDLIFESTATUSCIEXTRATABLE);
}
//*** CMDBCIDEFINEEXTRATABLE*** 
MDLIFESTATUSCIEXTRAFIELDS.MDLIFESTATUSCIEXTRATABLE.CMDBCIDEFINEEXTRATABLE = CMDBCIDEFINEEXTRATABLE_ListSetID(CMDBCIDEFINEEXTRATABLEList, MDLIFESTATUSCIEXTRAFIELDS.MDLIFESTATUSCIEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE);
CMDBCIDEFINE_ListAdd(MDLIFESTATUSList[i].CMDBCIDEFINEList, MDLIFESTATUSCIEXTRAFIELDS.MDLIFESTATUSCIEXTRATABLE.CMDBCIDEFINE);

CMDBCIDEFINEEXTRATABLE_ListAdd(MDLIFESTATUSCIEXTRAFIELDS.MDLIFESTATUSCIEXTRATABLE.CMDBCIDEFINE.CMDBCIDEFINEEXTRATABLElist, MDLIFESTATUSCIEXTRAFIELDS.MDLIFESTATUSCIEXTRATABLE.CMDBCIDEFINEEXTRATABLE);
CMDBCIDEFINEEXTRATABLE_ListAdd(CMDBCIDEFINEEXTRATABLEList, MDLIFESTATUSCIEXTRAFIELDS.MDLIFESTATUSCIEXTRATABLE.CMDBCIDEFINEEXTRATABLE);
MDLIFESTATUSCIEXTRAFIELDS.MDLIFESTATUSCIEXTRATABLE.CMDBCIDEFINEEXTRATABLE.CMDBCIDEFINE = MDLIFESTATUSCIEXTRAFIELDS.MDLIFESTATUSCIEXTRATABLE.CMDBCIDEFINE;

//***CMDBCIDEFINEEXTRAFIELDS***
MDLIFESTATUSCIEXTRAFIELDS.CMDBCIDEFINEEXTRAFIELDS = CMDBCIDEFINEEXTRAFIELDS_ListSetID(CMDBCIDEFINEEXTRAFIELDSList, MDLIFESTATUSCIEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS);
CMDBCIDEFINEEXTRAFIELDS_ListAdd(MDLIFESTATUSCIEXTRAFIELDS.MDLIFESTATUSCIEXTRATABLE.CMDBCIDEFINEEXTRATABLE.CMDBCIDEFINEEXTRAFIELDSList, MDLIFESTATUSCIEXTRAFIELDS.CMDBCIDEFINEEXTRAFIELDS);
CMDBCIDEFINEEXTRAFIELDS_ListAdd(CMDBCIDEFINEEXTRAFIELDSList, MDLIFESTATUSCIEXTRAFIELDS.CMDBCIDEFINEEXTRAFIELDS);
MDLIFESTATUSCIEXTRAFIELDS.CMDBCIDEFINEEXTRAFIELDS.CMDBCIDEFINE = MDLIFESTATUSCIEXTRAFIELDS.MDLIFESTATUSCIEXTRATABLE.CMDBCIDEFINE;
MDLIFESTATUSCIEXTRAFIELDS.CMDBCIDEFINEEXTRAFIELDS.CMDBCIDEFINEEXTRATABLE = MDLIFESTATUSCIEXTRAFIELDS.MDLIFESTATUSCIEXTRATABLE.CMDBCIDEFINEEXTRATABLE;
}
//********* MDLIFESTATUSSTEXTRAFIELDS ***********
foreach (TMDLIFESTATUSSTEXTRAFIELDS MDLIFESTATUSSTEXTRAFIELDS in MDLIFESTATUSSTEXTRAFIELDSList)
{
if (MDLIFESTATUSSTEXTRAFIELDS.MDLIFESTATUSSTEXTRATABLE.STATUSN == MDLIFESTATUSList[i].STATUSN)
{
    MDLIFESTATUSSTEXTRAFIELDS.MDLIFESTATUSSTEXTRATABLE.MDLIFESTATUS = MDLIFESTATUSList[i];
    MDLIFESTATUSSTEXTRAFIELDS_ListAdd(MDLIFESTATUSList[i].MDLIFESTATUSSTEXTRAFIELDSList, MDLIFESTATUSSTEXTRAFIELDS);
    MDLIFESTATUSSTEXTRATABLE_ListAdd(MDLIFESTATUSList[i].MDLIFESTATUSSTEXTRATABLEList, MDLIFESTATUSSTEXTRAFIELDS.MDLIFESTATUSSTEXTRATABLE);

}
//***MDSERVICEEXTRATABLE***
//MDLIFESTATUSSTEXTRAFIELDS.MDLIFESTATUSSTEXTRATABLE.MDSERVICEEXTRATABLE = MDSERVICEEXTRATABLE_ListSetID(MDSERVICEEXTRATABLEList, MDLIFESTATUSSTEXTRAFIELDS.MDLIFESTATUSSTEXTRATABLE.IDMDSERVICEEXTRATABLE);
MDLIFESTATUSSTEXTRAFIELDS.MDLIFESTATUSSTEXTRATABLE.MDSERVICEEXTRATABLE.MDSERVICETYPE = MDLIFESTATUSSTEXTRAFIELDS.MDLIFESTATUSSTEXTRATABLE.IDMDSERVICETYPE;
//***CMDBCIDEFINEEXTRAFIELDS***
MDLIFESTATUSSTEXTRAFIELDS.MDSERVICEEXTRAFIELDS = MDSERVICEEXTRAFIELDS_ListSetID(MDSERVICEEXTRAFIELDSList, MDLIFESTATUSSTEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS);
MDSERVICEEXTRAFIELDS_ListAdd(MDLIFESTATUSSTEXTRAFIELDS.MDLIFESTATUSSTEXTRATABLE.MDSERVICEEXTRATABLE.MDSERVICEEXTRAFIELDSList, MDLIFESTATUSSTEXTRAFIELDS.MDSERVICEEXTRAFIELDS);
MDSERVICEEXTRAFIELDS_ListAdd(MDSERVICEEXTRAFIELDSList, MDLIFESTATUSSTEXTRAFIELDS.MDSERVICEEXTRAFIELDS);
MDLIFESTATUSSTEXTRAFIELDS.MDSERVICEEXTRAFIELDS.MDSERVICETYPE = MDLIFESTATUSSTEXTRAFIELDS.MDLIFESTATUSSTEXTRATABLE.IDMDSERVICETYPE;
MDLIFESTATUSSTEXTRAFIELDS.MDSERVICEEXTRAFIELDS.MDSERVICEEXTRATABLE = MDLIFESTATUSSTEXTRAFIELDS.MDLIFESTATUSSTEXTRATABLE.MDSERVICEEXTRATABLE;
}

//********* MDCASESTEXTRAFIELDS ***********
foreach (TMDCASESTEXTRAFIELDS MDCASESTEXTRAFIELDS in MDCASESTEXTRAFIELDSList)
{
MDCASESTEXTRAFIELDS.MDSERVICEEXTRAFIELDS = MDSERVICEEXTRAFIELDS_ListSetID(MDSERVICEEXTRAFIELDSList, MDCASESTEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS);
MDSERVICEEXTRAFIELDS_ListAdd(MDCASESTEXTRAFIELDS.MDCASESTEXTRATABLE.MDSERVICEEXTRATABLE.MDSERVICEEXTRAFIELDSList, MDCASESTEXTRAFIELDS.MDSERVICEEXTRAFIELDS);
MDSERVICEEXTRAFIELDS_ListAdd(MDSERVICEEXTRAFIELDSList, MDCASESTEXTRAFIELDS.MDSERVICEEXTRAFIELDS);
MDCASESTEXTRAFIELDS.MDSERVICEEXTRAFIELDS.MDSERVICEEXTRATABLE = MDCASESTEXTRAFIELDS.MDCASESTEXTRATABLE.MDSERVICEEXTRATABLE;
}


}





catch (Exception e)
{
ResErr.NotError = false;
ResErr.Mesaje = "ReationLS:" + e.Message;
}
}

}
}

#region MDLIFESTATUS
public TMDLIFESTATUS MDLIFESTATUS_ListSetID(List<TMDLIFESTATUS> MDLIFESTATUSList, Int32 STATUSN)
{
for (var i = 0; i < MDLIFESTATUSList.length; i++)
{
    if (STATUSN == MDLIFESTATUSList[i].STATUSN)
        return (MDLIFESTATUSList[i]);

}
return (new TMDLIFESTATUS { STATUSN = STATUSN });
}
public TMDLIFESTATUS MDLIFESTATUS_ListSetID(Int32 STATUSN)
{
for (var i = 0; i < MDLIFESTATUSList.length; i++)
{
    if (STATUSN == MDLIFESTATUSList[i].STATUSN)
        return (MDLIFESTATUSList[i]);

}
return (new TMDLIFESTATUS { STATUSN = STATUSN });
}

public void MDLIFESTATUS_ListAdd(List<TMDLIFESTATUS> MDLIFESTATUSList, TMDLIFESTATUS MDLIFESTATUS)
{
Int32 i = MDLIFESTATUS_ListGetIndex(MDLIFESTATUSList, MDLIFESTATUS);
if (i == -1) MDLIFESTATUSList.Add(MDLIFESTATUS);
else MDLIFESTATUSList[i] = MDLIFESTATUS;
}
public Int32 MDLIFESTATUS_ListGetIndex(List<TMDLIFESTATUS> MDLIFESTATUSList, TMDLIFESTATUS MDLIFESTATUS)
{
for (var i = 0; i < MDLIFESTATUSList.length; i++)
{
    if (MDLIFESTATUS.STATUSN == MDLIFESTATUSList[i].STATUSN)
        return (i);
}
return (-1);
}
public TMDLIFESTATUS MDLIFESTATUS_ListGetIndex(Int32 Index)
{

if (MDLIFESTATUSList.Count > 0)
{
    return MDLIFESTATUSList[Index];
}
else
{
return (new TMDLIFESTATUS());
}
}
#endregion
#region MDLIFESTATUSTYPEUSER


//public TMDLIFESTATUSTYPEUSER MDLIFESTATUSTYPEUSER_ListSetID(List<TMDLIFESTATUSTYPEUSER> MDLIFESTATUSTYPEUSERList, UsrCfg.SD.Properties.TSDTypeUser IDSDTYPEUSER, Int32 STATUSN)
//{
//    for (var i = 0; i < MDLIFESTATUSTYPEUSERList.length; i++)
//    {
//        if ((IDSDTYPEUSER == MDLIFESTATUSTYPEUSERList[i].IDSDTYPEUSER) && (STATUSN == MDLIFESTATUSTYPEUSERList[i].STATUSN))
//            return (MDLIFESTATUSTYPEUSERList[i]);

//    }
//    return (new TMDLIFESTATUSTYPEUSER { STATUSN = STATUSN, IDSDTYPEUSER = IDSDTYPEUSER });
//}
    





public TMDLIFESTATUSTYPEUSER MDLIFESTATUSTYPEUSER_ListSetID(List<TMDLIFESTATUSTYPEUSER> MDLIFESTATUSTYPEUSERList, Int32 IDMDLIFESTATUSTYPEUSER)
{
for (var i = 0; i < MDLIFESTATUSTYPEUSERList.length; i++)
{
    if ((IDMDLIFESTATUSTYPEUSER == MDLIFESTATUSTYPEUSERList[i].IDMDLIFESTATUSTYPEUSER))
        return (MDLIFESTATUSTYPEUSERList[i]);

}
return (new TMDLIFESTATUSTYPEUSER { IDMDLIFESTATUSTYPEUSER = IDMDLIFESTATUSTYPEUSER});
}
public void MDLIFESTATUSTYPEUSER_ListAdd(List<TMDLIFESTATUSTYPEUSER> MDLIFESTATUSTYPEUSERList, TMDLIFESTATUSTYPEUSER MDLIFESTATUSTYPEUSER)
{
Int32 i = MDLIFESTATUSTYPEUSER_ListGetIndex(MDLIFESTATUSTYPEUSERList, MDLIFESTATUSTYPEUSER);
if (i == -1) MDLIFESTATUSTYPEUSERList.Add(MDLIFESTATUSTYPEUSER);
else MDLIFESTATUSTYPEUSERList[i] = MDLIFESTATUSTYPEUSER;
}
public Int32 MDLIFESTATUSTYPEUSER_ListGetIndex(List<TMDLIFESTATUSTYPEUSER> MDLIFESTATUSTYPEUSERList, TMDLIFESTATUSTYPEUSER MDLIFESTATUSTYPEUSER)         
{
for (var i = 0; i < MDLIFESTATUSTYPEUSERList.length; i++)
{
    if (MDLIFESTATUSTYPEUSER.IDMDLIFESTATUSTYPEUSER == MDLIFESTATUSTYPEUSERList[i].IDMDLIFESTATUSTYPEUSER)
        return (i);
}
return (-1);
}

#endregion
#region MDMATRIXMODELS
public TMDMATRIXMODELS MDMATRIXMODELS_ListSetID(List<TMDMATRIXMODELS> MDMATRIXMODELSList, Int32 IDMDMODELTYPED2, Int32 STATUSN)
{
for (var i = 0; i < MDMATRIXMODELSList.length; i++)
{
    if ((IDMDMODELTYPED2 == MDMATRIXMODELSList[i].IDMDMODELTYPED2) && (STATUSN == MDMATRIXMODELSList[i].STATUSN))
        return (MDMATRIXMODELSList[i]);

}
return (new TMDMATRIXMODELS { STATUSN = STATUSN, IDMDMODELTYPED2 = IDMDMODELTYPED2 });
}
public void MDMATRIXMODELS_ListAdd(List<TMDMATRIXMODELS> MDMATRIXMODELSList, TMDMATRIXMODELS MDMATRIXMODELS)
{
Int32 i = MDMATRIXMODELS_ListGetIndex(MDMATRIXMODELSList, MDMATRIXMODELS);
if (i == -1) 
    MDMATRIXMODELSList.Add(MDMATRIXMODELS);
else MDMATRIXMODELSList[i] = MDMATRIXMODELS;
}
public Int32 MDMATRIXMODELS_ListGetIndex(List<TMDMATRIXMODELS> MDMATRIXMODELSList, TMDMATRIXMODELS MDMATRIXMODELS)
{
for (var i = 0; i < MDMATRIXMODELSList.length; i++)
{
    if ((MDMATRIXMODELS.STATUSN == MDMATRIXMODELSList[i].STATUSN) && (MDMATRIXMODELS.IDMDMODELTYPED2 == MDMATRIXMODELSList[i].IDMDMODELTYPED2))
        return (i);
}
return (-1);
}
#endregion
#region CMDBCIDEFINE
public TCMDBCIDEFINE CMDBCIDEFINE_ListSetID(List<TCMDBCIDEFINE> CMDBCIDEFINEList, Int32 IDCMDBCIDEFINE)
{
for (var i = 0; i < CMDBCIDEFINEList.length; i++)
{
    if (IDCMDBCIDEFINE == CMDBCIDEFINEList[i].IDCMDBCIDEFINE)
        return (CMDBCIDEFINEList[i]);

}
return (new TCMDBCIDEFINE { IDCMDBCIDEFINE = IDCMDBCIDEFINE });
}
public void CMDBCIDEFINE_ListAdd(List<TCMDBCIDEFINE> CMDBCIDEFINEList, TCMDBCIDEFINE CMDBCIDEFINE)
{
Int32 i = CMDBCIDEFINE_ListGetIndex(CMDBCIDEFINEList, CMDBCIDEFINE);
if (i == -1) CMDBCIDEFINEList.Add(CMDBCIDEFINE);
else CMDBCIDEFINEList[i] = CMDBCIDEFINE;
}
public Int32 CMDBCIDEFINE_ListGetIndex(List<TCMDBCIDEFINE> CMDBCIDEFINEList, TCMDBCIDEFINE CMDBCIDEFINE)
{
for (var i = 0; i < CMDBCIDEFINEList.length; i++)
{
    if (CMDBCIDEFINE.IDCMDBCIDEFINE == CMDBCIDEFINEList[i].IDCMDBCIDEFINE)
        return (i);
}
return (-1);
}
#endregion

#region MDINTERFACE
public TMDINTERFACE MDINTERFACE_ListSetID(List<TMDINTERFACE> MDINTERFACEList, Int32 IDMDINTERFACE)
{
for (var i = 0; i < MDINTERFACEList.length; i++)
{
    if (IDMDINTERFACE == MDINTERFACEList[i].IDMDINTERFACE)
        return (MDINTERFACEList[i]);

}
return (new TMDINTERFACE { IDMDINTERFACE = IDMDINTERFACE });
}
public void MDINTERFACE_ListAdd(List<TMDINTERFACE> MDINTERFACEList, TMDINTERFACE MDINTERFACE)
{
Int32 i = MDINTERFACE_ListGetIndex(MDINTERFACEList, MDINTERFACE);
if (i == -1)
    MDINTERFACEList.Add(MDINTERFACE);
else MDINTERFACEList[i] = MDINTERFACE;
}
public Int32 MDINTERFACE_ListGetIndex(List<TMDINTERFACE> MDINTERFACEList, TMDINTERFACE MDINTERFACE)
{
for (var i = 0; i < MDINTERFACEList.length; i++)
{
    if (MDINTERFACE.IDMDINTERFACE == MDINTERFACEList[i].IDMDINTERFACE)
        return (i);
}
return (-1);
}
#endregion



#region MDLIFESTATUSCIEXTRAFIELDS
public TMDLIFESTATUSCIEXTRAFIELDS MDLIFESTATUSCIEXTRAFIELDS_ListSetID(List<TMDLIFESTATUSCIEXTRAFIELDS> MDLIFESTATUSCIEXTRAFIELDSList, Int32 IDMDLIFESTATUSCIEXTRAFIELDS)
{
for (var i = 0; i < MDLIFESTATUSCIEXTRAFIELDSList.length; i++)
{
    if (IDMDLIFESTATUSCIEXTRAFIELDS == MDLIFESTATUSCIEXTRAFIELDSList[i].IDMDLIFESTATUSCIEXTRAFIELDS)
        return (MDLIFESTATUSCIEXTRAFIELDSList[i]);

}
return (new TMDLIFESTATUSCIEXTRAFIELDS { IDMDLIFESTATUSCIEXTRAFIELDS = IDMDLIFESTATUSCIEXTRAFIELDS });
}
public void MDLIFESTATUSCIEXTRAFIELDS_ListAdd(List<TMDLIFESTATUSCIEXTRAFIELDS> MDLIFESTATUSCIEXTRAFIELDSList, TMDLIFESTATUSCIEXTRAFIELDS MDLIFESTATUSCIEXTRAFIELDS)
{
Int32 i = MDLIFESTATUSCIEXTRAFIELDS_ListGetIndex(MDLIFESTATUSCIEXTRAFIELDSList, MDLIFESTATUSCIEXTRAFIELDS);
if (i == -1) MDLIFESTATUSCIEXTRAFIELDSList.Add(MDLIFESTATUSCIEXTRAFIELDS);
else MDLIFESTATUSCIEXTRAFIELDSList[i] = MDLIFESTATUSCIEXTRAFIELDS;
}
public Int32 MDLIFESTATUSCIEXTRAFIELDS_ListGetIndex(List<TMDLIFESTATUSCIEXTRAFIELDS> MDLIFESTATUSCIEXTRAFIELDSList, TMDLIFESTATUSCIEXTRAFIELDS MDLIFESTATUSCIEXTRAFIELDS)
{
for (var i = 0; i < MDLIFESTATUSCIEXTRAFIELDSList.length; i++)
{
    if (MDLIFESTATUSCIEXTRAFIELDS.IDMDLIFESTATUSCIEXTRAFIELDS == MDLIFESTATUSCIEXTRAFIELDSList[i].IDMDLIFESTATUSCIEXTRAFIELDS)
        return (i);
}
return (-1);
}
#endregion
#region MDLIFESTATUSCIEXTRATABLE
public TMDLIFESTATUSCIEXTRATABLE MDLIFESTATUSCIEXTRATABLE_ListSetID(List<TMDLIFESTATUSCIEXTRATABLE> MDLIFESTATUSCIEXTRATABLEList, Int32 IDMDLIFESTATUSCIEXTRATABLE)
{
for (var i = 0; i < MDLIFESTATUSCIEXTRATABLEList.length; i++)
{
    if (IDMDLIFESTATUSCIEXTRATABLE == MDLIFESTATUSCIEXTRATABLEList[i].IDMDLIFESTATUSCIEXTRATABLE)
        return (MDLIFESTATUSCIEXTRATABLEList[i]);

}
return (new TMDLIFESTATUSCIEXTRATABLE { IDMDLIFESTATUSCIEXTRATABLE = IDMDLIFESTATUSCIEXTRATABLE });
}
public void MDLIFESTATUSCIEXTRATABLE_ListAdd(List<TMDLIFESTATUSCIEXTRATABLE> MDLIFESTATUSCIEXTRATABLEList, TMDLIFESTATUSCIEXTRATABLE MDLIFESTATUSCIEXTRATABLE)
{
Int32 i = MDLIFESTATUSCIEXTRATABLE_ListGetIndex(MDLIFESTATUSCIEXTRATABLEList, MDLIFESTATUSCIEXTRATABLE);
if (i == -1) MDLIFESTATUSCIEXTRATABLEList.Add(MDLIFESTATUSCIEXTRATABLE);
else MDLIFESTATUSCIEXTRATABLEList[i] = MDLIFESTATUSCIEXTRATABLE;
}
public Int32 MDLIFESTATUSCIEXTRATABLE_ListGetIndex(List<TMDLIFESTATUSCIEXTRATABLE> MDLIFESTATUSCIEXTRATABLEList, TMDLIFESTATUSCIEXTRATABLE MDLIFESTATUSCIEXTRATABLE)
{
for (var i = 0; i < MDLIFESTATUSCIEXTRATABLEList.length; i++)
{
    if (MDLIFESTATUSCIEXTRATABLE.IDMDLIFESTATUSCIEXTRATABLE == MDLIFESTATUSCIEXTRATABLEList[i].IDMDLIFESTATUSCIEXTRATABLE)
        return (i);
}
return (-1);
}
#endregion

#region CMDBCIDEFINEEXTRATABLE
public TCMDBCIDEFINEEXTRATABLE CMDBCIDEFINEEXTRATABLE_ListSetID(List<TCMDBCIDEFINEEXTRATABLE> CMDBCIDEFINEEXTRATABLEList, Int32 IDCMDBCIDEFINEEXTRATABLE)
{
for (var i = 0; i < CMDBCIDEFINEEXTRATABLEList.length; i++)
{
    if (IDCMDBCIDEFINEEXTRATABLE == CMDBCIDEFINEEXTRATABLEList[i].IDCMDBCIDEFINEEXTRATABLE)
        return (CMDBCIDEFINEEXTRATABLEList[i]);

}
return (new TCMDBCIDEFINEEXTRATABLE { IDCMDBCIDEFINEEXTRATABLE = IDCMDBCIDEFINEEXTRATABLE });
}
public void CMDBCIDEFINEEXTRATABLE_ListAdd(List<TCMDBCIDEFINEEXTRATABLE> CMDBCIDEFINEEXTRATABLEList, TCMDBCIDEFINEEXTRATABLE CMDBCIDEFINEEXTRATABLE)
{
Int32 i = CMDBCIDEFINEEXTRATABLE_ListGetIndex(CMDBCIDEFINEEXTRATABLEList, CMDBCIDEFINEEXTRATABLE);
if (i == -1) CMDBCIDEFINEEXTRATABLEList.Add(CMDBCIDEFINEEXTRATABLE);
else CMDBCIDEFINEEXTRATABLEList[i] = CMDBCIDEFINEEXTRATABLE;
}
public Int32 CMDBCIDEFINEEXTRATABLE_ListGetIndex(List<TCMDBCIDEFINEEXTRATABLE> CMDBCIDEFINEEXTRATABLEList, TCMDBCIDEFINEEXTRATABLE CMDBCIDEFINEEXTRATABLE)
{
for (var i = 0; i < CMDBCIDEFINEEXTRATABLEList.length; i++)
{
    if (CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE == CMDBCIDEFINEEXTRATABLEList[i].IDCMDBCIDEFINEEXTRATABLE)
        return (i);
}
return (-1);
}
#endregion
#region CMDBCIDEFINEEXTRAFIELDS
public TCMDBCIDEFINEEXTRAFIELDS CMDBCIDEFINEEXTRAFIELDS_ListSetID(List<TCMDBCIDEFINEEXTRAFIELDS> CMDBCIDEFINEEXTRAFIELDSList, Int32 IDCMDBCIDEFINEEXTRAFIELDS)
{
for (var i = 0; i < CMDBCIDEFINEEXTRAFIELDSList.length; i++)
{
    if (IDCMDBCIDEFINEEXTRAFIELDS == CMDBCIDEFINEEXTRAFIELDSList[i].IDCMDBCIDEFINEEXTRAFIELDS)
        return (CMDBCIDEFINEEXTRAFIELDSList[i]);

}
return (new TCMDBCIDEFINEEXTRAFIELDS { IDCMDBCIDEFINEEXTRAFIELDS = IDCMDBCIDEFINEEXTRAFIELDS });
}
public void CMDBCIDEFINEEXTRAFIELDS_ListAdd(List<TCMDBCIDEFINEEXTRAFIELDS> CMDBCIDEFINEEXTRAFIELDSList, TCMDBCIDEFINEEXTRAFIELDS CMDBCIDEFINEEXTRAFIELDS)
{
Int32 i = CMDBCIDEFINEEXTRAFIELDS_ListGetIndex(CMDBCIDEFINEEXTRAFIELDSList, CMDBCIDEFINEEXTRAFIELDS);
if (i == -1) CMDBCIDEFINEEXTRAFIELDSList.Add(CMDBCIDEFINEEXTRAFIELDS);
else CMDBCIDEFINEEXTRAFIELDSList[i] = CMDBCIDEFINEEXTRAFIELDS;
}
public Int32 CMDBCIDEFINEEXTRAFIELDS_ListGetIndex(List<TCMDBCIDEFINEEXTRAFIELDS> CMDBCIDEFINEEXTRAFIELDSList, TCMDBCIDEFINEEXTRAFIELDS CMDBCIDEFINEEXTRAFIELDS)
{
for (var i = 0; i < CMDBCIDEFINEEXTRAFIELDSList.length; i++)
{
    if (CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS == CMDBCIDEFINEEXTRAFIELDSList[i].IDCMDBCIDEFINEEXTRAFIELDS)
        return (i);
}
return (-1);
}
#endregion

#region MDLIFESTATUSSTEXTRAFIELDS
public TMDLIFESTATUSSTEXTRAFIELDS MDLIFESTATUSSTEXTRAFIELDS_ListSetID(List<TMDLIFESTATUSSTEXTRAFIELDS> MDLIFESTATUSSTEXTRAFIELDSList, Int32 IDMDLIFESTATUSSTEXTRAFIELDS)
{
for (var i = 0; i < MDLIFESTATUSSTEXTRAFIELDSList.length; i++)
{
    if (IDMDLIFESTATUSSTEXTRAFIELDS == MDLIFESTATUSSTEXTRAFIELDSList[i].IDMDLIFESTATUSSTEXTRAFIELDS)
        return (MDLIFESTATUSSTEXTRAFIELDSList[i]);

}
return (new TMDLIFESTATUSSTEXTRAFIELDS { IDMDLIFESTATUSSTEXTRAFIELDS = IDMDLIFESTATUSSTEXTRAFIELDS });
}
public void MDLIFESTATUSSTEXTRAFIELDS_ListAdd(List<TMDLIFESTATUSSTEXTRAFIELDS> MDLIFESTATUSSTEXTRAFIELDSList, TMDLIFESTATUSSTEXTRAFIELDS MDLIFESTATUSSTEXTRAFIELDS)
{
Int32 i = MDLIFESTATUSSTEXTRAFIELDS_ListGetIndex(MDLIFESTATUSSTEXTRAFIELDSList, MDLIFESTATUSSTEXTRAFIELDS);
if (i == -1) MDLIFESTATUSSTEXTRAFIELDSList.Add(MDLIFESTATUSSTEXTRAFIELDS);
else MDLIFESTATUSSTEXTRAFIELDSList[i] = MDLIFESTATUSSTEXTRAFIELDS;
}
public Int32 MDLIFESTATUSSTEXTRAFIELDS_ListGetIndex(List<TMDLIFESTATUSSTEXTRAFIELDS> MDLIFESTATUSSTEXTRAFIELDSList, TMDLIFESTATUSSTEXTRAFIELDS MDLIFESTATUSSTEXTRAFIELDS)
{
for (var i = 0; i < MDLIFESTATUSSTEXTRAFIELDSList.length; i++)
{
    if (MDLIFESTATUSSTEXTRAFIELDS.IDMDLIFESTATUSSTEXTRAFIELDS == MDLIFESTATUSSTEXTRAFIELDSList[i].IDMDLIFESTATUSSTEXTRAFIELDS)
        return (i);
}
return (-1);
}
#endregion
#region MDLIFESTATUSSTEXTRATABLE
public TMDLIFESTATUSSTEXTRATABLE MDLIFESTATUSSTEXTRATABLE_ListSetID(List<TMDLIFESTATUSSTEXTRATABLE> MDLIFESTATUSSTEXTRATABLEList, Int32 IDMDLIFESTATUSSTEXTRATABLE)
{
for (var i = 0; i < MDLIFESTATUSSTEXTRATABLEList.length; i++)
{
    if (IDMDLIFESTATUSSTEXTRATABLE == MDLIFESTATUSSTEXTRATABLEList[i].IDMDLIFESTATUSSTEXTRATABLE)
        return (MDLIFESTATUSSTEXTRATABLEList[i]);

}
return (new TMDLIFESTATUSSTEXTRATABLE { IDMDLIFESTATUSSTEXTRATABLE = IDMDLIFESTATUSSTEXTRATABLE });
}
public void MDLIFESTATUSSTEXTRATABLE_ListAdd(List<TMDLIFESTATUSSTEXTRATABLE> MDLIFESTATUSSTEXTRATABLEList, TMDLIFESTATUSSTEXTRATABLE MDLIFESTATUSSTEXTRATABLE)
{
Int32 i = MDLIFESTATUSSTEXTRATABLE_ListGetIndex(MDLIFESTATUSSTEXTRATABLEList, MDLIFESTATUSSTEXTRATABLE);
if (i == -1) MDLIFESTATUSSTEXTRATABLEList.Add(MDLIFESTATUSSTEXTRATABLE);
else MDLIFESTATUSSTEXTRATABLEList[i] = MDLIFESTATUSSTEXTRATABLE;
}
public Int32 MDLIFESTATUSSTEXTRATABLE_ListGetIndex(List<TMDLIFESTATUSSTEXTRATABLE> MDLIFESTATUSSTEXTRATABLEList, TMDLIFESTATUSSTEXTRATABLE MDLIFESTATUSSTEXTRATABLE)
{
for (var i = 0; i < MDLIFESTATUSSTEXTRATABLEList.length; i++)
{
    if (MDLIFESTATUSSTEXTRATABLE.IDMDLIFESTATUSSTEXTRATABLE == MDLIFESTATUSSTEXTRATABLEList[i].IDMDLIFESTATUSSTEXTRATABLE)
        return (i);
}
return (-1);
}
#endregion

#region MDCASESTEXTRAFIELDS
public TMDCASESTEXTRAFIELDS MDCASESTEXTRAFIELDS_ListSetID(List<TMDCASESTEXTRAFIELDS> MDCASESTEXTRAFIELDSList, Int32 IDMDCASESTEXTRAFIELDS)
{
for (var i = 0; i < MDCASESTEXTRAFIELDSList.length; i++)
{
    if (IDMDCASESTEXTRAFIELDS == MDCASESTEXTRAFIELDSList[i].IDMDCASESTEXTRAFIELDS)
        return (MDCASESTEXTRAFIELDSList[i]);

}
return (new TMDCASESTEXTRAFIELDS { IDMDCASESTEXTRAFIELDS = IDMDCASESTEXTRAFIELDS });
}
public void MDCASESTEXTRAFIELDS_ListAdd(List<TMDCASESTEXTRAFIELDS> MDCASESTEXTRAFIELDSList, TMDCASESTEXTRAFIELDS MDCASESTEXTRAFIELDS)
{
Int32 i = MDCASESTEXTRAFIELDS_ListGetIndex(MDCASESTEXTRAFIELDSList, MDCASESTEXTRAFIELDS);
if (i == -1) MDCASESTEXTRAFIELDSList.Add(MDCASESTEXTRAFIELDS);
else MDCASESTEXTRAFIELDSList[i] = MDCASESTEXTRAFIELDS;
}
public Int32 MDCASESTEXTRAFIELDS_ListGetIndex(List<TMDCASESTEXTRAFIELDS> MDCASESTEXTRAFIELDSList, TMDCASESTEXTRAFIELDS MDCASESTEXTRAFIELDS)
{
for (var i = 0; i < MDCASESTEXTRAFIELDSList.length; i++)
{
    if (MDCASESTEXTRAFIELDS.IDMDCASESTEXTRAFIELDS == MDCASESTEXTRAFIELDSList[i].IDMDCASESTEXTRAFIELDS)
        return (i);
}
return (-1);
}
#endregion
#region MDCASESTEXTRATABLE
public TMDCASESTEXTRATABLE MDCASESTEXTRATABLE_ListSetID(List<TMDCASESTEXTRATABLE> MDCASESTEXTRATABLEList, Int32 IDMDCASESTEXTRATABLE)
{
for (var i = 0; i < MDCASESTEXTRATABLEList.length; i++)
{
    if (IDMDCASESTEXTRATABLE == MDCASESTEXTRATABLEList[i].IDMDCASESTEXTRATABLE)
        return (MDCASESTEXTRATABLEList[i]);

}
return (new TMDCASESTEXTRATABLE { IDMDCASESTEXTRATABLE = IDMDCASESTEXTRATABLE });
}
public void MDCASESTEXTRATABLE_ListAdd(List<TMDCASESTEXTRATABLE> MDCASESTEXTRATABLEList, TMDCASESTEXTRATABLE MDCASESTEXTRATABLE)
{
Int32 i = MDCASESTEXTRATABLE_ListGetIndex(MDCASESTEXTRATABLEList, MDCASESTEXTRATABLE);
if (i == -1) MDCASESTEXTRATABLEList.Add(MDCASESTEXTRATABLE);
else MDCASESTEXTRATABLEList[i] = MDCASESTEXTRATABLE;
}
public Int32 MDCASESTEXTRATABLE_ListGetIndex(List<TMDCASESTEXTRATABLE> MDCASESTEXTRATABLEList, TMDCASESTEXTRATABLE MDCASESTEXTRATABLE)
{
for (var i = 0; i < MDCASESTEXTRATABLEList.length; i++)
{
    if (MDCASESTEXTRATABLE.IDMDCASESTEXTRATABLE == MDCASESTEXTRATABLEList[i].IDMDCASESTEXTRATABLE)
        return (i);
}
return (-1);
}
#endregion


#region MDSERVICEEXTRATABLE
public TMDSERVICEEXTRATABLE MDSERVICEEXTRATABLE_ListSetID(List<TMDSERVICEEXTRATABLE> MDSERVICEEXTRATABLEList, Int32 IDMDSERVICEEXTRATABLE)
{
for (var i = 0; i < MDSERVICEEXTRATABLEList.length; i++)
{
    if (IDMDSERVICEEXTRATABLE == MDSERVICEEXTRATABLEList[i].IDMDSERVICEEXTRATABLE)
        return (MDSERVICEEXTRATABLEList[i]);

}
return (new TMDSERVICEEXTRATABLE { IDMDSERVICEEXTRATABLE = IDMDSERVICEEXTRATABLE });
}
public void MDSERVICEEXTRATABLE_ListAdd(List<TMDSERVICEEXTRATABLE> MDSERVICEEXTRATABLEList, TMDSERVICEEXTRATABLE MDSERVICEEXTRATABLE)
{
Int32 i = MDSERVICEEXTRATABLE_ListGetIndex(MDSERVICEEXTRATABLEList, MDSERVICEEXTRATABLE);
if (i == -1) MDSERVICEEXTRATABLEList.Add(MDSERVICEEXTRATABLE);
else MDSERVICEEXTRATABLEList[i] = MDSERVICEEXTRATABLE;
}
public Int32 MDSERVICEEXTRATABLE_ListGetIndex(List<TMDSERVICEEXTRATABLE> MDSERVICEEXTRATABLEList, TMDSERVICEEXTRATABLE MDSERVICEEXTRATABLE)
{
for (var i = 0; i < MDSERVICEEXTRATABLEList.length; i++)
{
    if (MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE == MDSERVICEEXTRATABLEList[i].IDMDSERVICEEXTRATABLE)
        return (i);
}
return (-1);
}
#endregion
#region MDSERVICEEXTRAFIELDS
public TMDSERVICEEXTRAFIELDS MDSERVICEEXTRAFIELDS_ListSetID(List<TMDSERVICEEXTRAFIELDS> MDSERVICEEXTRAFIELDSList, Int32 IDMDSERVICEEXTRAFIELDS)
{
for (var i = 0; i < MDSERVICEEXTRAFIELDSList.length; i++)
{
    if (IDMDSERVICEEXTRAFIELDS == MDSERVICEEXTRAFIELDSList[i].IDMDSERVICEEXTRAFIELDS)
        return (MDSERVICEEXTRAFIELDSList[i]);

}
return (new TMDSERVICEEXTRAFIELDS { IDMDSERVICEEXTRAFIELDS = IDMDSERVICEEXTRAFIELDS });
}
public void MDSERVICEEXTRAFIELDS_ListAdd(List<TMDSERVICEEXTRAFIELDS> MDSERVICEEXTRAFIELDSList, TMDSERVICEEXTRAFIELDS MDSERVICEEXTRAFIELDS)
{
Int32 i = MDSERVICEEXTRAFIELDS_ListGetIndex(MDSERVICEEXTRAFIELDSList, MDSERVICEEXTRAFIELDS);
if (i == -1) MDSERVICEEXTRAFIELDSList.Add(MDSERVICEEXTRAFIELDS);
else MDSERVICEEXTRAFIELDSList[i] = MDSERVICEEXTRAFIELDS;
}
public Int32 MDSERVICEEXTRAFIELDS_ListGetIndex(List<TMDSERVICEEXTRAFIELDS> MDSERVICEEXTRAFIELDSList, TMDSERVICEEXTRAFIELDS MDSERVICEEXTRAFIELDS)
{
for (var i = 0; i < MDSERVICEEXTRAFIELDSList.length; i++)
{
    if (MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS == MDSERVICEEXTRAFIELDSList[i].IDMDSERVICEEXTRAFIELDS)
        return (i);
}
return (-1);
}
#endregion

}

*/