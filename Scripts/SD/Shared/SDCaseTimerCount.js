﻿UsrCfg.SD.SDCaseTimerCount.Properties._Version = '001';

UsrCfg.SD.SDCaseTimerCount.TSDCASETIMERCOUNTProfiler = function ()
{
    this.SDCASEMT_TIMER = new UsrCfg.SD.SDCaseTimerCount.Properties.TSDCASEMT_TIMER();
    this.MDFUNCPER_TIMERList = new Array();
    this.MDHIERPER_TIMERList = new Array();

    //String Function 
    this.toStrTIMERCOUNT = function ()
    {
        var Res = UsrCfg.SD.SDCaseTimerCount.Properties._Version + "(1)0";
        var Longitud = new SysCfg.ref("");
        var Texto = new SysCfg.ref("");
        try
        {
            this.SDCASEMT_TIMER.ToStr(Longitud, Texto);
            UsrCfg.SD.SDCaseTimerCount.Methods.MDFUNCPER_TIMERListtoStr(this.MDFUNCPER_TIMERList, Longitud, Texto);
            UsrCfg.SD.SDCaseTimerCount.Methods.MDHIERPER_TIMERListtoStr(this.MDHIERPER_TIMERList, Longitud, Texto);
            Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
            Res = UsrCfg.SD.SDCaseTimerCount.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
        }
        catch (e)
        {
            var ResErr = new SysCfg.Error.Properties.TResErr();
            ResErr.NotError = false;
            ResErr.Mesaje = "LIFESTATUStoStr:" + e;
        }
        return Res;
    }

    this.StrtoTIMERCOUNT = function (ProtocoloStr)
    {
        var Res = false;
        var Longitud, Texto;
        var Index = new SysCfg.ref(0);
        var Pos = new SysCfg.ref(0);
        try
        {
            if (UsrCfg.SD.SDCaseTimerCount.Properties._Version == ProtocoloStr.substring(0, 3))
            {
                Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
                Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
                LongitudArray = Longitud.split(",");
                this.SDCASEMT_TIMER.StrTo(Pos, Index, LongitudArray, Texto);
                UsrCfg.SD.SDCaseTimerCount.Methods.StrtoMDFUNCPER_TIMER(this.MDFUNCPER_TIMERList, Pos, Index, LongitudArray, Texto);
                UsrCfg.SD.SDCaseTimerCount.Methods.StrtoMDHIERPER_TIMER(this.MDHIERPER_TIMERList, Pos, Index, LongitudArray, Texto);
            }
            res = true;
        }
        catch (e)
        {
            var ResErr = SysCfg.Error.Properties.TResErr;
            ResErr.NotError = false;
            ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
        }
        return (Res);
    }

    //Socket Function 
    this.ToByte = function(SDCASEMT_TIMERList, MemStream, idx)
    {
        SDCASEMT_TIMER.ToByte(MemStream);
        UsrCfg.SD.SDCaseTimerCount.Methods.MDFUNCPER_TIMERListToByte(MDFUNCPER_TIMERList, MemStream, 0);
        UsrCfg.SD.SDCaseTimerCount.Methods.MDHIERPER_TIMERListToByte(MDHIERPER_TIMERList, MemStream, 0);
    }
    this.ByteTo = function(SDCASEMT_TIMERList, MemStream)
    {
        SDCASEMT_TIMER.ByteTo(MemStream);
        UsrCfg.SD.SDCaseTimerCount.Methods.ByteToMDFUNCPER_TIMERList(MDFUNCPER_TIMERList, MemStream);
        UsrCfg.SD.SDCaseTimerCount.Methods.ByteToMDHIERPER_TIMERList(MDHIERPER_TIMERList, MemStream);
    }
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

UsrCfg.SD.SDCaseTimerCount.Properties.TSDCASEMT_TIMER = function ()
{
    this.IDCALEDAYDATE = 0;
    this.CASE_DATELASTCUT = new Date(1970, 0, 1, 0, 0, 0);
    this.MDFUNCPER = 0;
    this.NORMALTIME = 0;
    this.MT_NORMALTIME_DATE = new Date(1970, 0, 1, 0, 0, 0);
    this.MT_MAXTIME = 0;
    this.MT_MAXTIME_DATE = new Date(1970, 0, 1, 0, 0, 0);

    //Socket IO Properties
    this.ToByte = function (MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.IDCALEDAYDATE);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.CASE_DATELASTCUT);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.MDFUNCPER);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.NORMALTIME);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.MT_NORMALTIME_DATE);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.MT_MAXTIME);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.MT_MAXTIME_DATE);

    }
    this.ByteTo = function (MemStream)
    {
        this.IDCALEDAYDATE = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.CASE_DATELASTCUT = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.MDFUNCPER = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.NORMALTIME = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.MT_NORMALTIME_DATE = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.MT_MAXTIME = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.MT_MAXTIME_DATE = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);

    }

    this.ToStr = function (Longitud, Texto)
    {
        SysCfg.Str.Protocol.WriteInt(this.IDCALEDAYDATE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteDateTime(this.CASE_DATELASTCUT, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.MDFUNCPER, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.NORMALTIME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteDateTime(this.MT_NORMALTIME_DATE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.MT_MAXTIME, Longitud, Texto);
        SysCfg.Str.Protocol.WriteDateTime(this.MT_MAXTIME_DATE, Longitud, Texto);

    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto)
    {
        this.IDCALEDAYDATE = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.CASE_DATELASTCUT = SysCfg.Str.Protocol.ReadDateTime(Pos, Index, LongitudArray, Texto);
        this.MDFUNCPER = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.NORMALTIME = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.MT_NORMALTIME_DATE = SysCfg.Str.Protocol.ReadDateTime(Pos, Index, LongitudArray, Texto);
        this.MT_MAXTIME = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.MT_MAXTIME_DATE = SysCfg.Str.Protocol.ReadDateTime(Pos, Index, LongitudArray, Texto);

    }
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

UsrCfg.SD.SDCaseTimerCount.Properties.TMDFUNCPER_TIMER = function ()
{
    this.FUNLAVEL = 0;
    this.XXXX = 0;
    this.FUNLAVEL_DATE = new Date(1970, 0, 1, 0, 0, 0);
    this.PERCF = 0;
    this.TOTALF = 0;

    //Socket IO Properties
    this.ToByte = function (MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.FUNLAVEL);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.XXXX);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.FUNLAVEL_DATE);
        SysCfg.Stream.Methods.WriteStreamDouble(MemStream, this.PERCF);
        SysCfg.Stream.Methods.WriteStreamDouble(MemStream, this.TOTALF);

    }
    this.ByteTo = function (MemStream)
    {
        this.FUNLAVEL = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.XXXX = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.FUNLAVEL_DATE = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.PERCF = SysCfg.Stream.Methods.ReadStreamDouble(MemStream);
        this.TOTALF = SysCfg.Stream.Methods.ReadStreamDouble(MemStream);
    }

    //Str IO Properties
    this.ToStr = function (Longitud, Texto)
    {
        SysCfg.Str.Protocol.WriteInt(this.FUNLAVEL, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.XXXX, Longitud, Texto);
        SysCfg.Str.Protocol.WriteDateTime(this.FUNLAVEL_DATE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteDouble(this.PERCF, Longitud, Texto);
        SysCfg.Str.Protocol.WriteDouble(this.TOTALF, Longitud, Texto);

    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto)
    {
        this.FUNLAVEL = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.XXXX = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.FUNLAVEL_DATE = SysCfg.Str.Protocol.ReadDateTime(Pos, Index, LongitudArray, Texto);
        this.PERCF = SysCfg.Str.Protocol.ReadDouble(Pos, Index, LongitudArray, Texto);
        this.TOTALF = SysCfg.Str.Protocol.ReadDouble(Pos, Index, LongitudArray, Texto);

    }
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

UsrCfg.SD.SDCaseTimerCount.Properties.TMDHIERPER_TIMER = function ()
{
    this.HIERLAVEL = 0;
    this.YYYY = 0;
    this.HIERLAVEL_DATE = new Date(1970, 0, 1, 0, 0, 0);
    this.PERCH = 0;
    this.TOTALF = 0;

    //Socket IO Properties
    this.ToByte = function (MemStream)
    {
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.HIERLAVEL);
        SysCfg.Stream.Methods.WriteStreamInt32(MemStream, this.YYYY);
        SysCfg.Stream.Methods.WriteStreamDateTime(MemStream, this.HIERLAVEL_DATE);
        SysCfg.Stream.Methods.WriteStreamDouble(MemStream, this.PERCH);
        SysCfg.Stream.Methods.WriteStreamDouble(MemStream, this.TOTALF);

    }
    this.ByteTo = function (MemStream)
    {
        this.HIERLAVEL = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.YYYY = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
        this.HIERLAVEL_DATE = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
        this.PERCH = SysCfg.Stream.Methods.ReadStreamDouble(MemStream);
        this.TOTALF = SysCfg.Stream.Methods.ReadStreamDouble(MemStream);

    }
    //Str IO Properties
    this.ToStr = function (Longitud, Texto)
    {
        SysCfg.Str.Protocol.WriteInt(this.HIERLAVEL, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(this.YYYY, Longitud, Texto);
        SysCfg.Str.Protocol.WriteDateTime(this.HIERLAVEL_DATE, Longitud, Texto);
        SysCfg.Str.Protocol.WriteDouble(this.PERCH, Longitud, Texto);
        SysCfg.Str.Protocol.WriteDouble(this.TOTALF, Longitud, Texto);

    }
    this.StrTo = function (Pos, Index, LongitudArray, Texto)
    {
        this.HIERLAVEL = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.YYYY = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
        this.HIERLAVEL_DATE = SysCfg.Str.Protocol.ReadDateTime(Pos, Index, LongitudArray, Texto);
        this.PERCH = SysCfg.Str.Protocol.ReadDouble(Pos, Index, LongitudArray, Texto);
        this.TOTALF = SysCfg.Str.Protocol.ReadDouble(Pos, Index, LongitudArray, Texto);

    }
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

UsrCfg.SD.SDCaseTimerCount.Methods.MDFUNCPER_TIMER_ListSetID = function (MDFUNCPER_TIMERList, FUNLAVEL)
{
    for (i = 0; i < MDFUNCPER_TIMERList.length; i++)
    {
        if (FUNLAVEL == MDFUNCPER_TIMERList[i].FUNLAVEL)
            return (MDFUNCPER_TIMERList[i]);
    }
    var UnMDFUNCPER_TIMER = new Persistence.Properties.TMDFUNCPER_TIMER;
    UnMDFUNCPER_TIMER.FUNLAVEL = FUNLAVEL;
    return (UnMDFUNCPER_TIMER);
}

UsrCfg.SD.SDCaseTimerCount.Methods.MDFUNCPER_TIMER_ListAdd = function (MDFUNCPER_TIMERList, MDFUNCPER_TIMER)
{
    var i = UsrCfg.SD.SDCaseTimerCount.Methods.MDFUNCPER_TIMER_ListGetIndex(MDFUNCPER_TIMERList, MDFUNCPER_TIMER);
    if (i == -1) MDFUNCPER_TIMERList.push(MDFUNCPER_TIMER);
    else
    {
        MDFUNCPER_TIMERList[i].FUNLAVEL = MDFUNCPER_TIMER.FUNLAVEL;
        MDFUNCPER_TIMERList[i].XXXX = MDFUNCPER_TIMER.XXXX;
        MDFUNCPER_TIMERList[i].FUNLAVEL_DATE = MDFUNCPER_TIMER.FUNLAVEL_DATE;
        MDFUNCPER_TIMERList[i].PERCF = MDFUNCPER_TIMER.PERCF;
        MDFUNCPER_TIMERList[i].TOTALF = MDFUNCPER_TIMER.TOTALF;

    }
}

UsrCfg.SD.SDCaseTimerCount.Methods.MDFUNCPER_TIMER_ListGetIndex = function (MDFUNCPER_TIMERList, MDFUNCPER_TIMER)
{
    for (i = 0; i < MDFUNCPER_TIMERList.length; i++)
    {
        if (MDFUNCPER_TIMER.FUNLAVEL == MDFUNCPER_TIMERList[i].FUNLAVEL)
            return (i);
    }
    return (-1);
}

UsrCfg.SD.SDCaseTimerCount.Methods.MDFUNCPER_TIMER_ListGetIndex = function (MDFUNCPER_TIMERList, FUNLAVEL)
{
    for (i = 0; i < MDFUNCPER_TIMERList.length; i++)
    {
        if (FUNLAVEL == MDFUNCPER_TIMERList[i].FUNLAVEL)
            return (i);
    }
    return (-1);
}

UsrCfg.SD.SDCaseTimerCount.Methods.MDFUNCPER_TIMERListtoStr = function (MDFUNCPER_TIMERList)
{
    var Res = Persistence.Properties._Version + "(1)0";
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try
    {
        SysCfg.Str.Protocol.WriteInt(MDFUNCPER_TIMERList.length, Longitud, Texto);
        for (Counter = 0; Counter < MDFUNCPER_TIMERList.length; Counter++)
        {
            var UnMDFUNCPER_TIMER = MDFUNCPER_TIMERList[Counter];
            UnMDFUNCPER_TIMER.ToStr(Longitud, Texto);
        }

        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e)
    {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
}

UsrCfg.SD.SDCaseTimerCount.Methods.StrtoMDFUNCPER_TIMER = function (ProtocoloStr, MDFUNCPER_TIMERList)
{

    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try
    {
        MDFUNCPER_TIMERList.length = 0;
        if (UsrCfg.SD.SDCaseTimerCount.Properties._Version == ProtocoloStr.substring(0, 3))
        {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.Length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++)
            {
                UnMDFUNCPER_TIMER = new Persistence.Properties.TMDFUNCPER_TIMER(); //Mode new row
                UnMDFUNCPER_TIMER.StrTo(Pos, Index, LongitudArray, Texto);
                UsrCfg.SD.SDCaseTimerCount.MDFUNCPER_TIMER_ListAdd(MDFUNCPER_TIMERList, UnMDFUNCPER_TIMER);
            }
        }
    }
    catch (e)
    {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}

//Socket Function 
UsrCfg.SD.SDCaseTimerCount.Methods.MDFUNCPER_TIMERListToByte = function (MDFUNCPER_TIMERList, MemStream, idx)
{
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, MDFUNCPER_TIMERList.Count - idx);
    for (i = idx; i < MDFUNCPER_TIMERList.Count; i++)
    {
        MDFUNCPER_TIMERList[i].ToByte(MemStream);
    }
}
UsrCfg.SD.SDCaseTimerCount.Methods.ByteToMDFUNCPER_TIMERList = function (MDFUNCPER_TIMERList, MemStream)
{
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++)
    {
        UnMDFUNCPER_TIMER = new Persistence.Properties.TMDFUNCPER_TIMER();
        UnMDFUNCPER_TIMER.ByteTo(MemStream);
        Methods.MDFUNCPER_TIMER_ListAdd(MDFUNCPER_TIMERList, UnMDFUNCPER_TIMER);
    }
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

//**********************   METHODS   ********************************************************************************************
UsrCfg.SD.SDCaseTimerCount.Methods.MDHIERPER_TIMER_ListSetID = function (MDHIERPER_TIMERList, HIERLAVEL)
{
    for (i = 0; i < MDHIERPER_TIMERList.length; i++)
    {
        if (HIERLAVEL == MDHIERPER_TIMERList[i].HIERLAVEL)
            return (MDHIERPER_TIMERList[i]);
    }
    var UnMDHIERPER_TIMER = new Persistence.Properties.TMDHIERPER_TIMER;
    UnMDHIERPER_TIMER.HIERLAVEL = HIERLAVEL;
    return (UnMDHIERPER_TIMER);
}
UsrCfg.SD.SDCaseTimerCount.Methods.MDHIERPER_TIMER_ListAdd = function (MDHIERPER_TIMERList, MDHIERPER_TIMER)
{
    var i = UsrCfg.SD.SDCaseTimerCount.Methods.MDHIERPER_TIMER_ListGetIndex(MDHIERPER_TIMERList, MDHIERPER_TIMER);
    if (i == -1) MDHIERPER_TIMERList.push(MDHIERPER_TIMER);
    else
    {
        MDHIERPER_TIMERList[i].HIERLAVEL = MDHIERPER_TIMER.HIERLAVEL;
        MDHIERPER_TIMERList[i].YYYY = MDHIERPER_TIMER.YYYY;
        MDHIERPER_TIMERList[i].HIERLAVEL_DATE = MDHIERPER_TIMER.HIERLAVEL_DATE;
        MDHIERPER_TIMERList[i].PERCH = MDHIERPER_TIMER.PERCH;
        MDHIERPER_TIMERList[i].TOTALF = MDHIERPER_TIMER.TOTALF;

    }
}

UsrCfg.SD.SDCaseTimerCount.Methods.MDHIERPER_TIMER_ListGetIndex = function (MDHIERPER_TIMERList, MDHIERPER_TIMER)
{
    for (i = 0; i < MDHIERPER_TIMERList.length; i++)
    {
        if (MDHIERPER_TIMER.HIERLAVEL == MDHIERPER_TIMERList[i].HIERLAVEL)
            return (i);
    }
    return (-1);

}

UsrCfg.SD.SDCaseTimerCount.Methods.MDHIERPER_TIMER_ListGetIndex = function (MDHIERPER_TIMERList, HIERLAVEL)
{
    for (i = 0; i < MDHIERPER_TIMERList.length; i++)
    {
        if (HIERLAVEL == MDHIERPER_TIMERList[i].HIERLAVEL)
            return (i);
    }
    return (-1);

}


//String Function 
UsrCfg.SD.SDCaseTimerCount.Methods.MDHIERPER_TIMERListtoStr = function (MDHIERPER_TIMERList)
{
    var Res = Persistence.Properties._Version + "(1)0";
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try
    {
        SysCfg.Str.Protocol.WriteInt(MDHIERPER_TIMERList.length, Longitud, Texto);
        for (Counter = 0; Counter < MDHIERPER_TIMERList.length; Counter++)
        {
            var UnMDHIERPER_TIMER = MDHIERPER_TIMERList[Counter];
            UnMDHIERPER_TIMER.ToStr(Longitud, Texto);
        }

        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e)
    {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
}
UsrCfg.SD.SDCaseTimerCount.Methods.StrtoMDHIERPER_TIMER = function (ProtocoloStr, MDHIERPER_TIMERList)
{

    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try
    {
        MDHIERPER_TIMERList.length = 0;
        if (UsrCfg.SD.SDCaseTimerCount.Properties._Version == ProtocoloStr.substring(0, 3))
        {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.Length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (i = 0; i < LSCount; i++)
            {
                UnMDHIERPER_TIMER = new Persistence.Properties.TMDHIERPER_TIMER(); //Mode new row
                UnMDHIERPER_TIMER.StrTo(Pos, Index, LongitudArray, Texto);
                UsrCfg.SD.SDCaseTimerCount.MDHIERPER_TIMER_ListAdd(MDHIERPER_TIMERList, UnMDHIERPER_TIMER);
            }
        }
    }
    catch (e)
    {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);
}


//Socket Function 
UsrCfg.SD.SDCaseTimerCount.Methods.MDHIERPER_TIMERListToByte = function (MDHIERPER_TIMERList, MemStream, idx)
{
    SysCfg.Stream.Methods.WriteStreamInt32(MemStream, MDHIERPER_TIMERList.Count - idx);
    for (i = idx; i < MDHIERPER_TIMERList.Count; i++)
    {
        MDHIERPER_TIMERList[i].ToByte(MemStream);
    }
}
UsrCfg.SD.SDCaseTimerCount.Methods.ByteToMDHIERPER_TIMERList = function (MDHIERPER_TIMERList, MemStream)
{
    Count = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
    for (i = 0; i < Count; i++)
    {
        UnMDHIERPER_TIMER = new Persistence.Properties.TMDHIERPER_TIMER();
        UnMDHIERPER_TIMER.ByteTo(MemStream);
        Methods.MDHIERPER_TIMER_ListAdd(MDHIERPER_TIMERList, UnMDHIERPER_TIMER);
    }
}

