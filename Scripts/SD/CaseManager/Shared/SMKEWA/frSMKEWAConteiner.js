﻿//Version 1 / 2018/04/30 Mauricio

ItHelpCenter.frSMKEWAConteiner = function (inObject, inIDSDCASE, inIDMDCATEGORYDETAIL, incallback)
{
    this.TParent = function ()
    {
        return this;
    }.bind(this);
    var _this = this.TParent();

    this.IDSDCASE = inIDSDCASE;
    this.IDMDCATEGORYDETAIL = inIDMDCATEGORYDETAIL;
    this.ObjectHtml = inObject;

    this.KE_CIList = new Array();
    this.WA_CIList = new Array();

    this.SMKEWAList = new Array();
    this.SDCASEList = new Array();

    this.LabelSelect = null;
    this.cmbType = null;
    this.spContainer = null;

    this.CallbackModalResult = incallback;

    //TRADUCCION          
    this.Mythis = "TUfrSMKEWAConteiner";   
    UsrCfg.Traslate.GetLangText(_this.Mythis, "LabelSelect.Text", "Select:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "VclComboBoxItem1.Text", "Mapping");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "VclComboBoxItem2.Text", "Normal");

    var PathCSS = ItHelpCenter.ScriptManager.GetStyle("SD/CaseManager/Console/SMKEWAConteiner.css");
    ItHelpCenter.ScriptManager.AddStyleFile(PathCSS, "SMKEWAConteinerCSS", function ()
    {
        _this.Load();//Funcion principal del archivo actual           
    }, function (e)
    {
    });

}

ItHelpCenter.frSMKEWAConteiner.prototype.AddStyleFile = function (nombre, onSuccess, onError)
{
    var style = document.createElement("link");
    style.rel = "stylesheet";
    style.type = "text/css";
    style.href = nombre;
    var s = document.head.appendChild(style);
    s.onload = onSuccess;
    s.onerror = onError;
}


ItHelpCenter.frSMKEWAConteiner.prototype.Load = function ()
{
    var _this = this.TParent();

    var VclStackPanelPrincipal = new TVclStackPanel(_this.ObjectHtml, "2", 1, [[12], [12], [12], [12], [12]]);

    /////////////////////////////////////////

    var spSelect = new TVclStackPanel(VclStackPanelPrincipal.Column[0].This, "2", 2, [[3, 9], [3, 9], [3, 9], [3, 9], [3, 9]]);

    _this.LabelSelect = new TVcllabel(spSelect.Column[0].This, "LabelSelect", TlabelType.H0);
    _this.LabelSelect.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "LabelSelect.Text");
    _this.cmbType = new TVclComboBox2(spSelect.Column[1].This, "");
    var VclComboBoxItem1 = new TVclComboBoxItem();
    VclComboBoxItem1.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "VclComboBoxItem1.Text");
    _this.cmbType.AddItem(VclComboBoxItem1);

    var VclComboBoxItem2 = new TVclComboBoxItem();
    VclComboBoxItem2.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "VclComboBoxItem2.Text");
    _this.cmbType.AddItem(VclComboBoxItem2);

    _this.cmbType.selectedIndex = 0;

    if (UsrCfg.Version.IsProduction)
    {
        $(spSelect.Row.This).css("display", "none"); //invisible
    }

    /////////////////////////////////////////

    //var spRaya1 = new TVclStackPanel(VclStackPanelPrincipal.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
    //spRaya1.Column[0].This.innerHTML = "<hr>";

    /////////////////////////////////////////

    _this.spContainer = new TVclStackPanel(VclStackPanelPrincipal.Column[0].This, "2", 1, [[12], [12], [12], [12], [12]]);

    /////////////////////////////////////////

    _this.LoadMapping();
}


ItHelpCenter.frSMKEWAConteiner.prototype.LoadMapping = function () {
    var _this = this.TParent();

    this.SMKEWAList = new Array();
    this.SDCASEList = new Array();
    this.KE_CIList = new Array();

    _this.spContainer.Column[0].This.innerHTML = "";

    Persistence.SD.Methods.SMKEWA_GET(_this.SMKEWAList, _this.IDMDCATEGORYDETAIL);
    Persistence.SD.Methods.SDCASE_GET(_this.SDCASEList, _this.IDSDCASE);
    Persistence.SD.Methods.INDEXAR_KE_WA(_this.SMKEWAList, _this.SDCASEList, _this.KE_CIList, _this.WA_CIList);

    if (_this.KE_CIList.length > 0)
    {
        for (var i = 0; i < _this.KE_CIList.length; i++)
        {
            var spSubContenedor = new TVclStackPanel(_this.spContainer.Column[0].This, "2", 1, [[12], [12], [12], [12], [12]]);
            spSubContenedor.Row.This.style.margin = "0px 0px";
            spSubContenedor.Row.This.style.marginBottom = "5px";
            spSubContenedor.Column[0].This.style.paddingLeft = "10px ";
            spSubContenedor.Column[0].This.style.paddingTop = "10px ";
            spSubContenedor.Column[0].This.style.border = "2px solid rgba(0, 0, 0, 0.08)";
            spSubContenedor.Column[0].This.style.boxShadow = " 0 0px 8px 0 rgba(0, 0, 0, 0.06), 0 1px 0px 0 rgba(0, 0, 0, 0.02);";
            spSubContenedor.Column[0].This.style.marginTop = "15px";
            spSubContenedor.Column[0].This.style.backgroundColor = "#FAFAFA";

            var spBloque = new TVclStackPanel(spSubContenedor.Column[0].This, "2", 2, [[3, 9], [3, 9], [3, 9], [3, 9], [3, 9]]);


            var txtGenericName = new TVclTextBox(spBloque.Column[0].This, "txtGenericName");
            txtGenericName.VCLType = TVCLType.BS;
            txtGenericName.Enabled = false;
            txtGenericName.Text = _this.KE_CIList[i].KE_CI_GENERICNAME;
            txtGenericName.Tag = _this.KE_CIList[i];

            ///////////////////////////

            var spArriba = new TVclStackPanel(spBloque.Column[1].This, "", 1, [[12], [12], [12], [12], [12]]);
            var txtKE_CI_Dexcription = new TVclTextBox(spArriba.Column[0].This, "txtKE_CI_Dexcription");
            txtKE_CI_Dexcription.VCLType = TVCLType.BS;
            txtKE_CI_Dexcription.Enabled = false;
            txtKE_CI_Dexcription.Text = _this.KE_CIList[i].KE_CI_DESCRIPTION;

            ///////////////////////////

            var spCentro = new TVclStackPanel(spBloque.Column[1].This, "", 1, [[12], [12], [12], [12], [12]]);
            var List_KE_KNOWNERRORSINFORMATION = _this.KE_CIList[i].KE_KNOWNERRORSINFORMATION.split('#');
            var mmKE_KNOWNERRORSINFORMATION = new TVclTextBox(spCentro.Column[0].This, "mmKE_KNOWNERRORSINFORMATION");
            mmKE_KNOWNERRORSINFORMATION.VCLType = TVCLType.BS;
            mmKE_KNOWNERRORSINFORMATION.Enabled = false;
            for (var a = 0; a < List_KE_KNOWNERRORSINFORMATION.length; a++)
            {
                mmKE_KNOWNERRORSINFORMATION.Text = mmKE_KNOWNERRORSINFORMATION.Text + List_KE_KNOWNERRORSINFORMATION[a] + "\n";
            }

            ///////////////////////////

            var spAbajo = new TVclStackPanel(spBloque.Column[1].This, "", 1, [[12], [12], [12], [12], [12]]);
            for (var j = 0; j < _this.KE_CIList[i].WA_CIList.length; j++)
            {
                var spLinea1 = new TVclStackPanel(spAbajo.Column[0].This, "2", 2, [[1, 11], [1, 11], [1, 11], [1, 11], [1, 11]]);
                spLinea1.Row.This.style.marginTop = "5px";

                var ckbOther = new TVclInputcheckbox(spLinea1.Column[0].This, "ckbOther");
                ckbOther.Checked = _this.KE_CIList[i].WA_CIList[j].ISCHECKED;
                ckbOther.Tag = _this.KE_CIList[i].WA_CIList[j];
                ckbOther.OnCheckedChanged = function(sender)
                {
                    if (sender.Checked)
                    {
                        Persistence.SD.Methods.SMKEWA_ADD(_this.IDMDCATEGORYDETAIL, _this.IDSDCASE, sender.Tag.WA_IDCMDBCI);
                    }
                    else
                    {
                        Persistence.SD.Methods.SMKEWA_DEL(sender.Tag.IDMDCATEGORYDETAIL_SDCASE);
                    }
                    _this.LoadMapping();
                }

                var txtWA_CI_GENERICNAME_WA_CI_DESCRIPTION = new TVclTextBox(spLinea1.Column[1].This, "txtWA_CI_GENERICNAME_WA_CI_DESCRIPTION");
                txtWA_CI_GENERICNAME_WA_CI_DESCRIPTION.VCLType = TVCLType.BS;
                txtWA_CI_GENERICNAME_WA_CI_DESCRIPTION.Enabled = false;
                txtWA_CI_GENERICNAME_WA_CI_DESCRIPTION.Text = _this.KE_CIList[i].WA_CIList[j].WA_CI_GENERICNAME + " " + _this.KE_CIList[i].WA_CIList[j].WA_CI_DESCRIPTION;

                /////////////////////////////////
                /////////////////////////////////

                var spLinea2 = new TVclStackPanel(spAbajo.Column[0].This, "2", 1, [[12], [12], [12], [12], [12]]);

                var List_WA_WORKAROUNDINFORMATION = _this.KE_CIList[i].WA_CIList[j].WA_WORKAROUNDINFORMATION.split('#');
                for (var a = 0; a < List_WA_WORKAROUNDINFORMATION.length; a++)
                {
                    _this.LabelSelect = new TVcllabel(spAbajo.Column[0].This, "lbl", TlabelType.H0);
                    _this.LabelSelect.Text = "- " + List_WA_WORKAROUNDINFORMATION[a];
                }

                /////////////////////////////////
                /////////////////////////////////

                var spStar = new TVclStackPanel(spAbajo.Column[0].This, "2", 1, [[12], [12], [12], [12], [12]]);
                spStar.Row.This.style.marginTop = "5px";
                var UriSource = "";
                for (var r = 0; r < 5; r++)
                {
                    if (r < _this.KE_CIList[i].WA_CIList[j].IDSMWAREVIEW)
                        UriSource = "image/16/star-full.png";
                    else
                        UriSource = "image/16/star-empty.png";

                    var imgStar = new TVclImagen(spStar.Column[0].This, "idimg");
                    imgStar.Tag = _this.KE_CIList[i].WA_CIList[j];
                    imgStar.Tag1 = r;
                    imgStar.Src = UriSource;
                    imgStar.OnClick = function (sender)
                    {
                        Persistence.SD.Methods.SDCASE_UPD(sender.Tag.IDMDCATEGORYDETAIL_SDCASE, sender.Tag1 + 1);
                        _this.LoadMapping();
                    }
                }

                if (_this.KE_CIList[i].WA_CIList[j].ISCHECKED)
                    $(spStar.Row.This).css("display", "block"); //visible
                else
                    $(spStar.Row.This).css("display", "none"); //invisible
            }
        }
    }
}


ItHelpCenter.frSMKEWAConteiner.prototype.LoadNormal = function ()
{
}