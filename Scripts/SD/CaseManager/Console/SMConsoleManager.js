﻿//Version 1 / 2018/03/28 Mauricio
ItHelpCenter.TSDWHOTOCASE_SET = function () {
    this.IDSDCASE = 0;
    this.IDSDCASEMT = 0;
    this.IDSDWHOTOCASETYPE = UsrCfg.SD.Properties.TIDSDWHOTOCASETYPE.GetEnum(undefined);
    this.IDSDTYPEUSER = 0;
    this.TYPEUSERNAME = "";
    this.IDSDWHOTOCASE = 0;
}


ItHelpCenter.TUfrSMConsoleManager = function (inMenuObject, inObject, inGetUsrCFg_Properties, incallback) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();

    this.MenuObject = inMenuObject;
    this.GPView = null;
    this.IDSDCASE = null;
    this.DS_SDWHOTOCASE = null;
    this.GetUsrCFg_Properties = inGetUsrCFg_Properties;

    this.ObjectHtml = inObject;

    this.cmbResponseType = null;
    this.btnCreateNewCAT = null;
    this.bloqueCreateComolexCase = null;
    this.hlbSeleccionaTipoUsuario = null;
    this.spBotones = null;
    this.bloqueUsuarios = null;
    this.objGPQuery = null;

    this.SelectedRow = null;

    this.CallbackModalResult = incallback;

    this.MenuObject.OnBeforerBackPage = function (sender, e) {
        //_this.Load();
    }
    //TRADUCCION          
    this.Mythis = "TUfrSMConsoleManager";
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1", "Refresh");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2", "Case Detail");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3", "Create Complex Case");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines4", "Import");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines5", "Include related");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines6", "Create New CAT");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines7", "Resolve");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Modal.AddHeaderContent", "Attention Detail");

    var PathCSS = ItHelpCenter.ScriptManager.GetStyle("SD/CaseManager/Console/SMConsoleManager.css");
    ItHelpCenter.ScriptManager.AddStyleFile(PathCSS, "SMConsoleManagerCSS", function () {
        _this.Load();//Funcion principal del archivo actual           
    }, function (e) {
    })
}

ItHelpCenter.TUfrSMConsoleManager.prototype.AddStyleFile = function (nombre, onSuccess, onError) {
    var style = document.createElement("link");
    style.rel = "stylesheet";
    style.type = "text/css";
    style.href = nombre;
    var s = document.head.appendChild(style);
    s.onload = onSuccess;
    s.onerror = onError;
}


ItHelpCenter.TUfrSMConsoleManager.prototype.Load = function () {
    var _this = this.TParent();

    _this.MenuObject.ObjectData.innerHTML = "";
    _this.MenuObject._ObjectDataMain.innerHTML = "";

    _this.objGPQuery = null;
    for (var i = 0; i < UsrCfg.Properties.UserATRole.ATROLE.ATROLEGPQUERY_LIST.length; i++) {
        var GPQUERY = UsrCfg.Properties.UserATRole.ATROLE.ATROLEGPQUERY_LIST[i].GPQUERY;
        if (GPQUERY.OPEN_TYPENAME == "CONSOLEMANAGER") {
            Persistence.GP.Methods.GPQUERY_COMPLETE(GPQUERY);
            _this.objGPQuery = GPQUERY;
            break;
        }
    }
    if (_this.objGPQuery == null) {
        _this.objGPQuery = new Persistence.GP.Properties.TGPQUERY();
        _this.objGPQuery.IDGPQUERY = 0;
        _this.objGPQuery.OPEN_ID = "GETSMCONSOLEMANAGER";
        _this.objGPQuery.OPEN_PREFIX = "Atis";
        _this.objGPQuery.OPEN_TYPENAME = "CONSOLEMANAGER";

        var defaultview = new Persistence.GP.Properties.TGPVIEW();
        defaultview.GPVIEW_NAME = "Console";
        defaultview.GPVIEW_ISACTIVE = "S";
        defaultview.GPVIEW_ISPUBLIC = "S";

        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", UsrCfg.InternoAtisNames.SDCASE.IDSDCASE.FieldName, 0, UsrCfg.InternoAtisNames.SDCASE.IDSDCASE.Header, "N", "S", "S", 1, 18, "NONE", true, true));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", UsrCfg.InternoAtisNames.SDCASE.IDSDCASE_PARENT.FieldName, 0, UsrCfg.InternoAtisNames.SDCASE.IDSDCASE_PARENT.Header, "N", "N", "S", 2, 20, "NONE", false, true));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", UsrCfg.InternoAtisNames.SDCASE.IDSDCASESTATUS.FieldName, 0, UsrCfg.InternoAtisNames.SDCASE.IDSDCASESTATUS.Header, "N", "N", "N", 3, 20, "NONE", false, false));

        var col = new Persistence.GP.Properties.TGPCOLUMNVIEW();
        col.COLOR = "#FFFFFFFF";
        col.FIELD_NAME = UsrCfg.InternoAtisNames.SDCASESTATUS.CASESTATUSNAME.FieldName;
        col.GROUP_POSITION = 0;
        col.HEADER = UsrCfg.InternoAtisNames.SDCASESTATUS.CASESTATUSNAME.Header;
        col.ISGROUP = "N";
        col.ISVISIBLE = "S";
        col.ISVISIBLEDETAIL = "N";
        col.POSITION = 4;
        col.SIZE = 28;
        col.SORT = "NONE";
        col.VISIBLE = true;
        col.VISIBLEDETAIL = false;

        var GPVALUECOLOR_Col_01 = new Persistence.GP.Properties.TGPVALUECOLOR();
        GPVALUECOLOR_Col_01.COLOR = "#FFFF6347"; GPVALUECOLOR_Col_01.CONDITION = "Start"; GPVALUECOLOR_Col_01.NAME = "Start";
        col.GPVALUECOLORList.push(GPVALUECOLOR_Col_01);

        var GPVALUECOLOR_Col_02 = new Persistence.GP.Properties.TGPVALUECOLOR();
        GPVALUECOLOR_Col_02.COLOR = "#FF90EE90"; GPVALUECOLOR_Col_02.CONDITION = "InProgress"; GPVALUECOLOR_Col_02.NAME = "InProgress";
        col.GPVALUECOLORList.push(GPVALUECOLOR_Col_02);

        var GPVALUECOLOR_Col_03 = new Persistence.GP.Properties.TGPVALUECOLOR();
        GPVALUECOLOR_Col_03.COLOR = "#FFFFFFE0"; GPVALUECOLOR_Col_03.CONDITION = "Paused"; GPVALUECOLOR_Col_03.NAME = "Paused";
        col.GPVALUECOLORList.push(GPVALUECOLOR_Col_03);

        var GPVALUECOLOR_Col_04 = new Persistence.GP.Properties.TGPVALUECOLOR();
        GPVALUECOLOR_Col_04.COLOR = "#FFFFFF00"; GPVALUECOLOR_Col_04.CONDITION = "Resolved"; GPVALUECOLOR_Col_04.NAME = "Resolved";
        col.GPVALUECOLORList.push(GPVALUECOLOR_Col_04);

        defaultview.GPCOLUMNVIEWList.push(col);

        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", UsrCfg.InternoAtisNames.MDSERVICETYPE.SERVICETYPENAME.FieldName, 0, UsrCfg.InternoAtisNames.MDSERVICETYPE.SERVICETYPENAME.Header, "N", "S", "S", 6, 20, "NONE", true, true));

        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", UsrCfg.InternoAtisNames.SDRESPONSETYPE.IDSDRESPONSETYPE.FieldName, 0, UsrCfg.InternoAtisNames.SDRESPONSETYPE.IDSDRESPONSETYPE.Header, "N", "N", "N", 7, 20, "NONE", false, false));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", UsrCfg.InternoAtisNames.SDRESPONSETYPE.RESPONSETYPENAME.FieldName, 0, UsrCfg.InternoAtisNames.SDRESPONSETYPE.RESPONSETYPENAME.Header, "N", "N", "S", 8, 20, "NONE", false, true));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", UsrCfg.InternoAtisNames.SDCASE.IDSDCASESOURCETYPE.FieldName, 0, UsrCfg.InternoAtisNames.SDCASE.IDSDCASESOURCETYPE.Header, "N", "N", "S", 9, 20, "NONE", false, true));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", UsrCfg.InternoAtisNames.SDCASE.CASE_DATESTART.FieldName, 0, UsrCfg.InternoAtisNames.SDCASE.CASE_DATESTART.Header, "N", "N", "S", 10, 20, "NONE", false, true));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", UsrCfg.InternoAtisNames.SDCASE.CASE_DATERESOLVED.FieldName, 0, UsrCfg.InternoAtisNames.SDCASE.CASE_DATERESOLVED.Header, "N", "N", "S", 11, 20, "NONE", false, true));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", UsrCfg.InternoAtisNames.SDCASE.CASE_DESCRIPTION.FieldName, 0, UsrCfg.InternoAtisNames.SDCASE.CASE_DESCRIPTION.Header, "N", "N", "S", 12, 20, "NONE", false, true));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", UsrCfg.InternoAtisNames.SDCASE.CASE_ISMAYOR.FieldName, 0, UsrCfg.InternoAtisNames.SDCASE.CASE_ISMAYOR.Header, "N", "S", "N", 13, 15, "NONE", true, false));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", UsrCfg.InternoAtisNames.SDCASE.CASE_TITLE.FieldName, 0, UsrCfg.InternoAtisNames.SDCASE.CASE_TITLE.Header, "N", "N", "S", 14, 20, "NONE", false, true));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", UsrCfg.InternoAtisNames.SDCASE.IDHANDLER.FieldName, 0, UsrCfg.InternoAtisNames.SDCASE.IDHANDLER.Header, "N", "N", "N", 15, 20, "NONE", false, false));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", "HANDLERNAME", 0, UsrCfg.Traslate.GetLangTextdbExtra("HANDLERNAME", "HANDLERNAME"), "N", "N", "S", 16, 20, "NONE", false, true));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", UsrCfg.InternoAtisNames.SDCASE.IDOWNER.FieldName, 0, UsrCfg.InternoAtisNames.SDCASE.IDOWNER.Header, "N", "N", "N", 17, 20, "NONE", false, false));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", "OWNERNAME", 0, UsrCfg.Traslate.GetLangTextdbExtra("OWNERNAME", "OWNERNAME"), "N", "N", "S", 18, 20, "NONE", false, true));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", UsrCfg.InternoAtisNames.SDCASE.IDMANAGERSINFORMED.FieldName, 0, UsrCfg.InternoAtisNames.SDCASE.IDMANAGERSINFORMED.Header, "N", "N", "N", 19, 20, "NONE", false, false));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", "MANAGERSINFORMEDNAME", 0, UsrCfg.Traslate.GetLangTextdbExtra("MANAGERSINFORMEDNAME", "MANAGERSINFORMEDNAME"), "N", "N", "S", 20, 20, "NONE", false, true));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", UsrCfg.InternoAtisNames.SDCASE.IDUSER.FieldName, 0, UsrCfg.InternoAtisNames.SDCASE.IDUSER.Header, "N", "N", "N", 21, 20, "NONE", false, false));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", "USERNAME", 0, UsrCfg.Traslate.GetLangTextdbExtra("USERNAME", "USERNAME"), "N", "S", "N", 22, 20, "NONE", true, false));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", UsrCfg.InternoAtisNames.SDCASE.CASE_COUNTTIME.FieldName, 0,  /*"Case"+ (Char)13 + (Char)10 +"prueba"*/UsrCfg.InternoAtisNames.SDCASE.CASE_COUNTTIME.Header, "N", "S", "N", 23, 20, "NONE", true, false));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", UsrCfg.InternoAtisNames.SDCASE.CASE_COUNTTIMEPAUSE.FieldName, 0, UsrCfg.InternoAtisNames.SDCASE.CASE_COUNTTIMEPAUSE.Header, "N", "S", "N", 24, 20, "NONE", true, false));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", UsrCfg.InternoAtisNames.SDCASE.CASE_COUNTTIMERESOLVED.FieldName, 0, UsrCfg.InternoAtisNames.SDCASE.CASE_COUNTTIMERESOLVED.Header, "N", "S", "N", 25, 20, "NONE", true, false));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", UsrCfg.InternoAtisNames.SDCASEMT.IDMDCATEGORYDETAIL.FieldName, 0, UsrCfg.InternoAtisNames.SDCASEMT.IDMDCATEGORYDETAIL.Header, "N", "N", "N", 26, 20, "NONE", false, false));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", "CATEGORY", 0, UsrCfg.Traslate.GetLangTextdbExtra("CATEGORY", "CATEGORY"), "N", "N", "S", 22, 27, "NONE", false, true));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CATEGORYNAME.FieldName, 0, UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CATEGORYNAME.Header, "N", "N", "S", 28, 20, "NONE", false, true));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", UsrCfg.InternoAtisNames.SDCASEMT.IDMDPRIORITY.FieldName, 0, UsrCfg.InternoAtisNames.SDCASEMT.IDMDPRIORITY.Header, "N", "N", "N", 29, 20, "NONE", false, false));

        var col1 = new Persistence.GP.Properties.TGPCOLUMNVIEW();
        col1.COLOR = "#FFFFFFFF";
        col1.FIELD_NAME = UsrCfg.InternoAtisNames.MDPRIORITY.PRIORITYNAME.FieldName;
        col1.GROUP_POSITION = 0;
        col1.HEADER = UsrCfg.InternoAtisNames.MDPRIORITY.PRIORITYNAME.Header;
        col1.ISGROUP = "N";
        col1.ISVISIBLE = "S";
        col1.ISVISIBLEDETAIL = "N";
        col1.POSITION = 30;
        col1.SIZE = 20;
        col1.SORT = "NONE";
        col1.VISIBLE = true;
        col1.VISIBLEDETAIL = false;


        var GPVALUECOLOR_Col1_01 = new Persistence.GP.Properties.TGPVALUECOLOR();
        GPVALUECOLOR_Col1_01.COLOR = "#FFFF0000"; GPVALUECOLOR_Col1_01.CONDITION = "High"; GPVALUECOLOR_Col1_01.NAME = "Alta";
        col1.GPVALUECOLORList.push(GPVALUECOLOR_Col1_01);

        var GPVALUECOLOR_Col1_02 = new Persistence.GP.Properties.TGPVALUECOLOR();
        GPVALUECOLOR_Col1_02.COLOR = "#FFFFFF00"; GPVALUECOLOR_Col1_02.CONDITION = "low"; GPVALUECOLOR_Col1_02.NAME = "Baja";
        col1.GPVALUECOLORList.push(GPVALUECOLOR_Col1_02);

        var GPVALUECOLOR_Col1_03 = new Persistence.GP.Properties.TGPVALUECOLOR();
        GPVALUECOLOR_Col1_03.COLOR = "#FFFFA500"; GPVALUECOLOR_Col1_03.CONDITION = "Medium"; GPVALUECOLOR_Col1_03.NAME = "Media";
        col1.GPVALUECOLORList.push(GPVALUECOLOR_Col1_03);

        defaultview.GPCOLUMNVIEWList.push(col1);

        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASEMT.FieldName, 0, UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASEMT.Header, "N", "N", "S", 31, 20, "NONE", false, true));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASEMTSTATUS.FieldName, 0, UsrCfg.InternoAtisNames.SDCASEMT.IDSDCASEMTSTATUS.Header, "N", "N", "N", 32, 20, "NONE", false, false));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", UsrCfg.InternoAtisNames.SDCASEMTSTATUS.CASEMTSTATUSNAME.FieldName, 0, UsrCfg.InternoAtisNames.SDCASEMTSTATUS.CASEMTSTATUSNAME.Header, "N", "N", "S", 33, 20, "NONE", false, true));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_DATECREATE.FieldName, 0, UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_DATECREATE.Header, "N", "N", "N", 34, 20, "NONE", false, false));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_FUNLAVEL.FieldName, 0, UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_FUNLAVEL.Header, "N", "N", "S", 35, 20, "NONE", false, true));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_HIERLAVEL.FieldName, 0, UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_HIERLAVEL.Header, "N", "N", "S", 36, 20, "NONE", false, true));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_LS_NAMESTEP.FieldName, 0, UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_LS_NAMESTEP.Header, "N", "N", "N", 37, 20, "NONE", false, false));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_LS_STATUSN.FieldName, 0, UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_LS_STATUSN.Header, "N", "N", "N", 38, 20, "NONE", false, false));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_LS_NEXTSTEP.FieldName, 0, UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_LS_NEXTSTEP.Header, "N", "N", "N", 39, 20, "NONE", false, false));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_LS_COMMENTSST.FieldName, 0, UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_SET_LS_COMMENTSST.Header, "N", "N", "N", 40, 20, "NONE", false, false));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", UsrCfg.InternoAtisNames.SDCASEMT.IDSLA.FieldName, 0, UsrCfg.InternoAtisNames.SDCASEMT.IDSLA.Header, "N", "N", "N", 41, 20, "NONE", false, false));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", UsrCfg.InternoAtisNames.MDSLA.SLANAME.FieldName, 0, UsrCfg.InternoAtisNames.MDSLA.SLANAME.Header, "N", "N", "S", 42, 20, "NONE", false, true));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_COUNTTIME.FieldName, 0, UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_COUNTTIME.Header, "N", "N", "S", 43, 20, "NONE", false, true));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_COUNTTIMEPAUSE.FieldName, 0, UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_COUNTTIMEPAUSE.Header, "N", "N", "S", 44, 20, "NONE", false, true));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_COUNTTIMERESOLVED.FieldName, 0, UsrCfg.InternoAtisNames.SDCASEMT.CASEMT_COUNTTIMERESOLVED.Header, "N", "N", "S", 45, 20, "NONE", false, true));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", UsrCfg.InternoAtisNames.MDMODELTYPED.TITLEM.FieldName, 0, UsrCfg.InternoAtisNames.MDMODELTYPED.TITLEM.Header, "N", "N", "S", 46, 20, "NONE", false, true));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", UsrCfg.InternoAtisNames.SDRUNNIGSTATUS.ACTIVITIESNAME.FieldName, 0, UsrCfg.InternoAtisNames.SDRUNNIGSTATUS.ACTIVITIESNAME.Header, "N", "N", "S", 47, 20, "NONE", false, true));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", UsrCfg.InternoAtisNames.SDRUNNIGSOURCEMODEL.SOURCEMODELNAME.FieldName, 0, UsrCfg.InternoAtisNames.SDRUNNIGSOURCEMODEL.SOURCEMODELNAME.Header, "N", "S", "N", 48, 20, "NONE", true, false));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", UsrCfg.InternoAtisNames.SDCASE.IDCMDBCONTACTTYPE_USER.FieldName, 0, UsrCfg.InternoAtisNames.SDCASE.IDCMDBCONTACTTYPE_USER.Header, "N", "N", "S", 49, 20, "NONE", false, true));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", "USERCONTACTTYPENAME", 0, UsrCfg.Traslate.GetLangTextdbExtra("USERCONTACTTYPENAME", "USERCONTACTTYPENAME"), "N", "N", "S", 50, 20, "NONE", false, true));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", "USERCONTACTDEFINE", 0, UsrCfg.Traslate.GetLangTextdbExtra("USERCONTACTDEFINE", "USERCONTACTDEFINE"), "N", "N", "S", 51, 20, "NONE", false, true));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", "USERFIRSTNAME", 0, UsrCfg.Traslate.GetLangTextdbExtra("USERFIRSTNAME", "USERFIRSTNAME"), "N", "N", "S", 49, 20, "NONE", false, true));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", "USERLASTNAME", 0, UsrCfg.Traslate.GetLangTextdbExtra("USERLASTNAME", "USERLASTNAME"), "N", "N", "S", 50, 20, "NONE", false, true));
        defaultview.GPCOLUMNVIEWList.push(new Persistence.GP.Properties.TGPCOLUMNVIEW("#FFFFFFFF", "USERMIDDLENAME", 0, UsrCfg.Traslate.GetLangTextdbExtra("USERMIDDLENAME", "USERMIDDLENAME"), "N", "N", "S", 51, 20, "NONE", false, true));

        _this.objGPQuery.GPVIEWList.push(defaultview);
    }

    _this.SDWHOTOCASE_GET();

    _this.InitializeComponent();

    UsrCfg.WaitMe.CloseWaitme();
}

ItHelpCenter.TUfrSMConsoleManager.prototype.BtnRefresh_OnClick = function (sender, EventArgs) {
    var _this = sender;
}


ItHelpCenter.TUfrSMConsoleManager.prototype.btnCaseDetail_OnClick = function (sender, EventArgs) {
    var _this = sender;
    var inIDSDCASE = parseInt(_this.bloqueUsuarios.Options[_this.bloqueUsuarios.selectedIndex].Tag.IDSDCASE);

    var parameters =
    {
        IDSDCASE: inIDSDCASE,
        IDCMDBCI: SysCfg.App.Methods.Cfg_WindowsTask.User.IDCMDBCI
    };
    parameters = JSON.stringify(parameters);
    $.ajax({
        type: "POST",
        url: SysCfg.App.Properties.xRaiz + 'Service/SD/Atention.svc/SDConsoleInfo',
        data: parameters,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (response) {
            //***************************************************************************************************
            var Atention = response;

            if (Atention.ResErr.NotError == true) {
                if (Atention.ResultSDConsoleAtentionStart == UsrCfg.SD.Properties.TResultSDConsoleAtentionStart._IncomeAccepted.value) {

                    var Modal = new TVclModal(document.body, null, "IDModalAtentionDetail");
                    Modal.Width = "85%";
                    Modal.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.Mythis, "Modal.AddHeaderContent"));
                    Modal.Body.This.id = "Modal_AtentionDetail";
                    var TfrSDCaseAtentionDetail = new ItHelpCenter.SD.Shared.Atention.CaseAtentionDetail.TfrSDCaseAtentionDetail(Modal.Body.This, function (thisOut) {
                        _this.Load();
                    }, Atention.SDCONSOLEATENTION);
                    Modal.ShowModal();
                    Modal.FunctionClose = function () {
                    }
                }
                else if (Atention.ResultSDConsoleAtentionStart == UsrCfg.SD.Properties.TResultSDConsoleAtentionStart._IncomeCreate.value) {
                }
            }
            else {
            }

        },
        error: function (response) {
            SysCfg.Log.Methods.WriteLog("SMConsoleManager.js ItHelpCenter.TUfrSMConsoleManager.prototype.Load " + "Error no llamo al Servicio [SDConsoleAtentionStart]");
        }
    });
}


ItHelpCenter.TUfrSMConsoleManager.prototype.btnNewComplexCase_OnClick = function (sender, EventArgs) {
    var _this = sender;
    //var MenuObjetNewCase = UsrCfg.Properties.MenuProfiler.NewPage(_this.MenuObject);
    var form = new ItHelpCenter.SD.frSDCaseSet(document.body.firstChild, "", _this.MenuObject);
    form.Modal.Width = "90%"
    form.Modal.ShowModal();
}



ItHelpCenter.TUfrSMConsoleManager.prototype.btnCreateNewCAT_OnClick = function (sender, EventArgs) {
    var _this = sender;
    try {
        var IDSDCASETYPE = UsrCfg.SD.Properties.TIDSDCASETYPE._NORMAL;
        var IDSDRESPONSETYPE = UsrCfg.SD.Properties.TSDRESPONSETYPE._SERVICEDESK;
        var MenuObjetNewCase = UsrCfg.Properties.MenuProfiler.NewPage(_this.MenuObject);

        _this.MenuObject.OnAfterBackPage = function (sender, e) {
            _this.Load();
        };

        var NewCase = new ItHelpCenter.SD.CaseManager.NewCase.frSDCMCaseNew(MenuObjetNewCase, MenuObjetNewCase.ObjectDataMain/*_this.ObjectHtml*/, function () {
            NewCase.Initialize(IDSDCASETYPE, IDSDRESPONSETYPE, "", "", function (thisOut, ResultSDCaseChangeStatus, IDSDCASE, IDSDCASEMT, IDSDTYPEUSER) {
                if (thisOut.chkShowConsole.Checked) {
                    if (ResultSDCaseChangeStatus == UsrCfg.SD.Properties.TResultSDCaseChangeStatus._CSSuccessfullyCreate) {
                        MenuObjetNewCase.ObjectData.innerHTML = "";
                        MenuObjetNewCase._ObjectDataMain.innerHTML = "";
                        MenuObjetNewCase.OnBeforeBackPage = null;

                        var SMConsoleManagerSwitch = new ItHelpCenter.SD.CaseManager.Atention.TSMConsoleManagerSwitch(MenuObjetNewCase, //MODO Forza caso 3 en owner             
                        MenuObjetNewCase.ObjectDataMain,
                        function (outthis) {
                            _this.MenuObjetNewCase.ObjectData.innerHTML = "";
                            _this.MenuObjetNewCase._ObjectDataMain.innerHTML = "";
                            _this.MenuObjetNewCase.OnBeforeBackPage = null;
                            _this.Load();
                        },
                        IDSDCASE, IDSDCASEMT, IDSDTYPEUSER.value  //MODO Forza caso 3 en owner   
                        );
                    }
                    else {
                        thisOut.MenuObject.BackPageOnlyParent(_this);
                    }
                }
                else {
                    thisOut.MenuObject.BackPageOnlyParent(_this);
                }
            });
        });
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("SMConsoleManager.js ItHelpCenter.TUfrSMConsoleManager.prototype.Load", e);
    }
}


ItHelpCenter.TUfrSMConsoleManager.prototype.btnResolve_OnClick = function (sender, EventArgs) {
    var _this = sender;
    try {

        if (_this.SelectedRow != null) {
            UsrCfg.WaitMe.ShowWaitme();
            var MenuObjetResolve = UsrCfg.Properties.MenuProfiler.NewPage(_this.MenuObject);

            _this.MenuObject.OnAfterBackPage = function (sender, e) {
                _this.Load();
            };

            var IDSDCASE = _this.bloqueUsuarios.Options[_this.bloqueUsuarios.selectedIndex].Tag.IDSDCASE;
            var IDSDCASEMT = _this.bloqueUsuarios.Options[_this.bloqueUsuarios.selectedIndex].Tag.IDSDCASEMT;
            var IDSDTYPEUSER = _this.bloqueUsuarios.Options[_this.bloqueUsuarios.selectedIndex].Tag.IDSDTYPEUSER;
            var SMConsoleManagerSwitch = new ItHelpCenter.SD.CaseManager.Atention.TSMConsoleManagerSwitch(MenuObjetResolve, //MODO Forza caso 3 en owner             
            MenuObjetResolve.ObjectDataMain,
            function (outthis) {
                MenuObjetResolve.ObjectDataMain.innerHTML = "";
                _this.Load();
            },
            IDSDCASE, IDSDCASEMT, IDSDTYPEUSER  //MODO Forza caso 3 en owner   
            );
        }
    } catch (e) {
        //UsrCfg.WaitMe.CloseWaitme();
    }
}


ItHelpCenter.TUfrSMConsoleManager.prototype.SDWHOTOCASE_GET = function () {
    var _this = this.TParent();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCI.FieldName, _this.GetUsrCFg_Properties.IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        _this.DS_SDWHOTOCASE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "GETSMCONSOLEMANAGER_WHO1", Param.ToBytes());
    }
    finally {
        Param.Destroy();
    }
}


ItHelpCenter.TUfrSMConsoleManager.prototype.UpdateButtonZone = function () {
    var _this = this.TParent();
    if (_this.cmbResponseType.Options.length > 0) {
        $(_this.bloqueCreateComolexCase.Row.This).css("display", "block"); //visible
    }
    else if (_this.cmbResponseType.Options.length == 0) {
        $(_this.bloqueCreateComolexCase.Row.This).css("display", "none"); //invisible
    }

    $(_this.spBotones.Row.This).css("display", "none"); //invisible

    _this.bloqueUsuarios.ClearOptions();
    _this.bloqueUsuarios.Visible = false;

    _this.hlbSeleccionaTipoUsuario.Visible = false;

    if (_this.DS_SDWHOTOCASE.ResErr.NotError) {
        if (_this.DS_SDWHOTOCASE.DataSet.RecordCount > 0) {
            _this.DS_SDWHOTOCASE.DataSet.First();
            while (!(_this.DS_SDWHOTOCASE.DataSet.Eof)) {
                var SDWHOTOCASE_SET = new ItHelpCenter.TSDWHOTOCASE_SET();
                SDWHOTOCASE_SET.IDSDCASE = _this.DS_SDWHOTOCASE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDCASE.FieldName).asInt32();
                SDWHOTOCASE_SET.IDSDCASEMT = _this.DS_SDWHOTOCASE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDCASEMT.FieldName).asInt32();
                SDWHOTOCASE_SET.IDSDWHOTOCASETYPE = UsrCfg.SD.Properties.TIDSDWHOTOCASETYPE.GetEnum(_this.DS_SDWHOTOCASE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDWHOTOCASETYPE.FieldName).Value);
                SDWHOTOCASE_SET.IDSDTYPEUSER = _this.DS_SDWHOTOCASE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDTYPEUSER.FieldName).asInt32();
                SDWHOTOCASE_SET.TYPEUSERNAME = _this.DS_SDWHOTOCASE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDTYPEUSER.TYPEUSERNAME.FieldName).asString();
                SDWHOTOCASE_SET.IDSDWHOTOCASE = _this.DS_SDWHOTOCASE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDWHOTOCASE.IDSDWHOTOCASE.FieldName).asInt32();


                if (_this.IDSDCASE == SDWHOTOCASE_SET.IDSDCASE) {
                    var VclInputRadioButtonItem1 = new TVclInputRadioButtonItem();
                    VclInputRadioButtonItem1.Text = SDWHOTOCASE_SET.TYPEUSERNAME;
                    VclInputRadioButtonItem1.Tag = SDWHOTOCASE_SET;
                    _this.bloqueUsuarios.AddItem(VclInputRadioButtonItem1);

                    _this.btnCreateNewCAT.Tag = SDWHOTOCASE_SET;
                }
                _this.DS_SDWHOTOCASE.DataSet.Next();
            }
        }
        else {
            var VclInputRadioButtonItem1 = new TVclInputRadioButtonItem();
            //VclInputRadioButtonItem1.Text = Persistence.Catalog.Methods.SDTYPEUSER_ListSetID(Persistence.Profiler.CatalogProfiler.SDTYPEUSERList, (Int32)UsrCfg.SD.Properties.TSDTypeUser._Owner).TYPEUSERNAME;
            //VclInputRadioButtonItem1.Tag = Persistence.Catalog.Methods.SDTYPEUSER_ListSetID(Persistence.Profiler.CatalogProfiler.SDTYPEUSERList, (Int32)UsrCfg.SD.Properties.TSDTypeUser._Owner).IDSDTYPEUSER; 
            _this.bloqueUsuarios.AddItem(VclInputRadioButtonItem1);
            _this.bloqueUsuarios.selectedIndex = 0;
        }

        if (_this.bloqueUsuarios.Options.length > 0) {
            $(_this.spBotones.Row.This).css("display", "block"); //visible
            if (_this.bloqueUsuarios.Options.length > 1) {
                _this.hlbSeleccionaTipoUsuario.Visible = true;
            }
            _this.bloqueUsuarios.selectedIndex = 0;
        }

    }

}

