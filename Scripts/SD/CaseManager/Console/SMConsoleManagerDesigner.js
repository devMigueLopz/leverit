﻿ItHelpCenter.TUfrSMConsoleManager.prototype.InitializeComponent = function ()
{
    var _this = this.TParent();

    var VclStackPanelPrincipal = new TVclStackPanel(_this.ObjectHtml, "2", 2, [[9, 3], [9, 3], [9, 3], [9, 3], [9, 3]]);
    _this.GPView = new ItHelpCenter.TUfrGPView(VclStackPanelPrincipal.Column[0].This, function () { }, _this.objGPQuery);
    _this.GPView.Param.Inicialize();
    _this.GPView.Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCI.FieldName, _this.GetUsrCFg_Properties.IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    _this.GPView.Param.AddUnknown(UsrCfg.InternoAtisNames.SDCASE.IDSDCASE.FieldName, UsrCfg.InternoAtisNames.SDCASE.NAME_TABLE + "." + UsrCfg.InternoAtisNames.SDCASE.IDSDCASE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

    var DivBotonera = _this.MenuObject.GetDivData(VclStackPanelPrincipal.Column[1], VclStackPanelPrincipal.Column[0]);

    var stackPanelBotonera = new TVclStackPanel(DivBotonera/*VclStackPanelPrincipal.Column[1].This*/, "1", 1, [[12], [12], [12], [12], [12]]);

    if (Source.Menu.IsMobil)
    { stackPanelBotonera.Row.This.classList.add("menuContenedorMobil"); }
    else
    {
        stackPanelBotonera.Row.This.style.margin = "0px 0px";
        stackPanelBotonera.Column[0].This.style.paddingLeft = "10px ";
        stackPanelBotonera.Column[0].This.style.paddingTop = "10px ";
        stackPanelBotonera.Column[0].This.style.border = "2px solid rgba(0, 0, 0, 0.08)";
        stackPanelBotonera.Column[0].This.style.boxShadow = " 0 0px 8px 0 rgba(0, 0, 0, 0.06), 0 1px 0px 0 rgba(0, 0, 0, 0.02);";
        stackPanelBotonera.Column[0].This.style.marginTop = "15px";
        stackPanelBotonera.Column[0].This.style.backgroundColor = "#FAFAFA";
    }

    this.stackPanelBodyBotonera = new TVclStackPanel(stackPanelBotonera.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);


    var spBotonera = new TVclStackPanel(this.stackPanelBodyBotonera.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
    spBotonera.Row.This.style.margin = "4px";

    ////////////////////////////////////

    var spRefresh = new TVclStackPanel(spBotonera.Column[0].This, "1", 2, [[8, 4], [8, 4], [8, 4], [8, 4], [8, 4]]);
    $(spRefresh.Column[0].This).addClass("LabelBoton");

    _this.LabelRefresh = new TVcllabel(spRefresh.Column[0].This, "LabelRefresh", TlabelType.H0);
    _this.LabelRefresh.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1");
    var btnRefresh = new TVclButton(spRefresh.Column[1].This, "btnRefresh");
    btnRefresh.Cursor = "pointer";
    btnRefresh.Src = "image/24/Refresh.png";
    btnRefresh.VCLType = TVCLType.BS;
    btnRefresh.This.style.minWidth = "100%";
    btnRefresh.This.classList.add("btn-xs");
    btnRefresh.onClick = function myfunction()
    {
        _this.BtnRefresh_OnClick(_this, btnRefresh);
    }


    ////////////////////////////////////

    var spCaseDetail = new TVclStackPanel(spBotonera.Column[0].This, "1", 2, [[8, 4], [8, 4], [8, 4], [8, 4], [8, 4]]);
    spCaseDetail.Row.This.style.marginTop = "3px";
    $(spCaseDetail.Column[0].This).addClass("LabelBoton");

    _this.LabelCaseDetail = new TVcllabel(spCaseDetail.Column[0].This, "LabelCaseDetail", TlabelType.H0);
    _this.LabelCaseDetail.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2");
    

    var btnCaseDetail = new TVclButton(spCaseDetail.Column[1].This, "btnCaseDetail");
    btnCaseDetail.Cursor = "pointer";
    btnCaseDetail.Src = "image/24/Checklist.png";
    btnCaseDetail.VCLType = TVCLType.BS;
    btnCaseDetail.This.style.minWidth = "100%";
    btnCaseDetail.This.classList.add("btn-xs");
    btnCaseDetail.onClick = function myfunction()
    {
        _this.btnCaseDetail_OnClick(_this, btnCaseDetail);
    }

    ////////////////////////////////////

    var spRaya1 = new TVclStackPanel(spBotonera.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
    spRaya1.Column[0].This.innerHTML = "<hr>";

    ////////////////////////////////////

    if (_this.GetUsrCFg_Properties.UserATRole.isManager)
    {
        if (_this.GetUsrCFg_Properties.UserATRole.isServiceDesk)
        {
            var spNewComplexCase = new TVclStackPanel(spBotonera.Column[0].This, "1", 2, [[8, 4], [8, 4], [8, 4], [8, 4], [8, 4]]);

            _this.LabelNewComplexCase = new TVcllabel(spNewComplexCase.Column[0].This, "LabelNewComplexCase", TlabelType.H0);
            _this.LabelNewComplexCase.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3");

            var btnNewComplexCase = new TVclButton(spNewComplexCase.Column[1].This, "btnNewComplexCase");
            btnNewComplexCase.Cursor = "pointer";
            btnNewComplexCase.Src = "image/24/Surveys.png";
            btnNewComplexCase.VCLType = TVCLType.BS;
            btnNewComplexCase.This.style.minWidth = "100%";
            btnNewComplexCase.This.classList.add("btn-xs");
            btnNewComplexCase.onClick = function myfunction()
            {
                _this.btnNewComplexCase_OnClick(_this, btnNewComplexCase);
            }

            ////////////////////////////////////

            var spRaya2 = new TVclStackPanel(spBotonera.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
            spRaya2.Column[0].This.innerHTML = "<hr>";

            ////////////////////////////////////

        }
    }

    ///////////////////////////////////////
    ///////////////////////////////////////

    var spCombos = new TVclStackPanel(spBotonera.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
    _this.cmbResponseType = new TVclComboBox2(spCombos.Column[0].This, "");
    $(spCombos.Row.This).css("display", "none"); //invisible
    if (_this.GetUsrCFg_Properties.UserATRole.isServiceDesk)
    {
        var VclComboBoxItem1 = new TVclComboBoxItem();
        VclComboBoxItem1.Text = "SERVICEDESK";
        VclComboBoxItem1.Tag = 1;
        _this.cmbResponseType.AddItem(VclComboBoxItem1);
        //cmbResponseType.Items.Add(UsrCfg.SD.Properties.TSDRESPONSETYPE._SERVICEDESK);
    }
    if (_this.GetUsrCFg_Properties.UserATRole.isProblemManager)
    {
        var VclComboBoxItem2 = new TVclComboBoxItem();
        VclComboBoxItem2.Text = "PROBLEMMANAGER";
        VclComboBoxItem2.Tag = 2;
        _this.cmbResponseType.AddItem(VclComboBoxItem2);
        //cmbResponseType.Items.Add(UsrCfg.SD.Properties.TSDRESPONSETYPE._PROBLEMMANAGER);
    }

    if (_this.cmbResponseType.Options.length == 1)
    {
        _this.cmbResponseType.selectedIndex = 0;
    }
    else
        _this.cmbResponseType.selectedIndex = -1;

    ///////////////////////////////////////
    ///////////////////////////////////////

    _this.bloqueCreateComolexCase = new TVclStackPanel(spBotonera.Column[0].This, "bloqueCreateComolexCase", 1, [[12], [12], [12], [12], [12]]);

    var spImport = new TVclStackPanel(_this.bloqueCreateComolexCase.Column[0].This, "spImport", 1, [[12], [12], [12], [12], [12]]);
    var chkImport = new TVclInputcheckbox(spImport.Column[0].This, "chkImport");
    chkImport.Checked = false;
    chkImport.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines4");
    chkImport.VCLType = TVCLType.BS;

    ///////////////////////////////////////
    ///////////////////////////////////////

    var spIncludeRelated = new TVclStackPanel(_this.bloqueCreateComolexCase.Column[0].This, "spIncludeRelated", 1, [[12], [12], [12], [12], [12]]);
    var chkIncludeRelated = new TVclInputcheckbox(spIncludeRelated.Column[0].This, "");
    chkIncludeRelated.Checked = false;
    chkIncludeRelated.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines5");
    chkIncludeRelated.VCLType = TVCLType.BS;

    ///////////////////////////////////////
    ///////////////////////////////////////

    var spCreateNewCAT = new TVclStackPanel(_this.bloqueCreateComolexCase.Column[0].This, "spCreateNewCAT", 2, [[8, 4], [8, 4], [8, 4], [8, 4], [8, 4]]);
    spCreateNewCAT.Column[0].This.style = "display: flex;justify-content: center;align-content: center;flex-direction: column;";
    $(spCreateNewCAT.Column[0].This).addClass("LabelBoton");

    _this.LabelNewComplexCase = new TVcllabel(spCreateNewCAT.Column[0].This, "LabelNewComplexCase", TlabelType.H0);
    _this.LabelNewComplexCase.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines6");

    _this.btnCreateNewCAT = new TVclButton(spCreateNewCAT.Column[1].This, "btnCreateNewCAT");
    _this.btnCreateNewCAT.Cursor = "pointer";
    _this.btnCreateNewCAT.Src = "image/24/case-add.png";
    _this.btnCreateNewCAT.VCLType = TVCLType.BS;
    _this.btnCreateNewCAT.This.style.minWidth = "100%";
    _this.btnCreateNewCAT.This.classList.add("btn-xs");
    _this.btnCreateNewCAT.onClick = function myfunction()
    {
        _this.btnCreateNewCAT_OnClick(_this, _this.btnCreateNewCAT);
    }

    ////////////////////////////////////

    var spRaya3 = new TVclStackPanel(spBotonera.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
    spRaya3.Column[0].This.innerHTML = "<hr>";

    ////////////////////////////////////

    _this.spBotones = new TVclStackPanel(spBotonera.Column[0].This, "spBotones", 2, [[10, 2], [10, 2], [10, 2], [10, 2], [10, 2]]);

    var btnResolve = new TVclButton(_this.spBotones.Column[0].This, "btnResolve");
    btnResolve.Cursor = "pointer";
    btnResolve.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines7");
    btnResolve.Src = "image/24/customer-service.png";
    btnResolve.VCLType = TVCLType.BS;
    btnResolve.This.style.minWidth = "100%";
    btnResolve.This.classList.add("btn-xs");
    btnResolve.onClick = function myfunction()
    {
        _this.btnResolve_OnClick(_this, btnResolve);
    }

    _this.hlbSeleccionaTipoUsuario = new TVclImagen(_this.spBotones.Column[1].This, "hlbSeleccionaTipoUsuario");
    _this.hlbSeleccionaTipoUsuario.Src = "image/24/users.png";
    _this.hlbSeleccionaTipoUsuario.Visible = false;
    _this.hlbSeleccionaTipoUsuario.onClick = function ()
    {
        if (_this.bloqueUsuarios.Visible == true)
        {
            _this.bloqueUsuarios.Visible = false;
        }
        else
        {
            _this.bloqueUsuarios.Visible = true;
        }
    }

    _this.bloqueUsuarios = new TVclInputRadioButton(_this.spBotones.Column[0].This, "bloqueUsuarios"); //CREA UN LOOKUPOPTION
    _this.bloqueUsuarios.Visible = false;
    $(_this.spBotones.Column[1].This).css("text-align", "center");
    $(_this.spBotones.Column[1].This).css("padding", "10px 0px 0px 0px");

    _this.GPView.OnGridSelectedRowChanged = function (SelectedRow)
    {

        _this.SelectedRow = SelectedRow;
        for (var i = 0; i < _this.SelectedRow.Fields.length; i++)
        {
            if (_this.SelectedRow.Fields[i].FieldDef.FieldName.toUpperCase() == "IDSDCASE")
            {
                _this.IDSDCASE = _this.SelectedRow.Fields[i].Value;
                break;
            }
        }
        _this.UpdateButtonZone();
    }

    if (UsrCfg.Version.IsProduction)
    {
        $(spRefresh.Row.This).css("display", "none"); //invisible
        $(spImport.Row.This).css("display", "none"); //invisible
        $(spIncludeRelated.Row.This).css("display", "none"); //invisible
    }

    _this.GPView.OpenQuery();
}

