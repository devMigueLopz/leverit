﻿ItHelpCenter.SD.frSDCaseSet = function (inObjectHtml, Id, inMenuObjet) {
    this.TParent = function () {
        return this;
    }.bind(this);

    var _this = this.TParent();

    this.IDSDCASETYPE; //public UsrCfg.SD.Properties.TIDSDCASETYPE IDSDCASETYPE;
    this.IDSDCASESTATUSLast; //public UsrCfg.SD.Properties.TSDCaseStatus IDSDCASESTATUSLast;
    this.IDSDRESPONSETYPE; //public UsrCfg.SD.Properties.TSDRESPONSETYPE IDSDRESPONSETYPE;
    this.IDSDCASE = 0; //public Int32 IDSDCASE;
    this.IDMDCATEGORYDETAIL = 0; //public Int32 IDMDCATEGORYDETAIL;
    this.IDMDMODELTYPED = 0;  //public Int32 IDMDMODELTYPED;
    this.NewCase; //public event EventHandler NewCase;
 
  
    this.MenuObjet = inMenuObjet;

    this.ObjectHtml = inObjectHtml;
    this.OnInicializeComponent = null;
    this.NewCase = null;
    this.Mythis = "frSDCaseSet";
    UsrCfg.Traslate.GetLangText(this.Mythis, "HeaderModal", "Case Set");
    UsrCfg.Traslate.GetLangText(this.Mythis, "TabPag1", "Case search");
    UsrCfg.Traslate.GetLangText(this.Mythis, "TabPag2", "CI search");
    UsrCfg.Traslate.GetLangText(this.Mythis, "lbl01", "Permissions");
    UsrCfg.Traslate.GetLangText(this.Mythis, "lbl04", "Create New CAT");
    UsrCfg.Traslate.GetLangText(this.Mythis, "chkImportar", "Import");
    UsrCfg.Traslate.GetLangText(this.Mythis, "chkRelated", "Include related.");
    UsrCfg.Traslate.GetLangText(this.Mythis, "Modal.AddHeaderContent","Case Detail")

    
    this.TSDCASEEXPORT = function () {       
        this.IDSDCASE = 0;
        this.IDSDRUNNIGSOURCEMODEL = UsrCfg.SD.Properties.TSDRUNNIGSOURCEMODEL; //ItHelpCenter.SD.Properties.TSDRUNNIGSOURCEMODEL
        this.IDSDRESPONSETYPE = UsrCfg.SD.Properties.TSDRESPONSETYPE; //ItHelpCenter.SD.Properties.TSDRESPONSETYPE 
        this.IDSDCASEACTIVITIES = 0;
        this.IDSDCASETYPE = UsrCfg.SD.Properties.TIDSDCASETYPE; //ItHelpCenter.SD.Properties.TIDSDCASETYPE
        this.CASE_DESCRIPTION = "";
        this.IDMDCATEGORYDETAIL = 0;
        this.IDSLA = 0;
        this.IDMDURGENCY = 0;
        this.IDMDIMPACT = 0;
        this.IDMDPRIORITY = 0;
        this.CASE_ISMAYOR = false;
        this.CASE_TITLE = "";
        this.CATEGORY = "";
        this.CATEGORYNAME = ""; 
    }

    this.TSDCASE_RELATION= function ()
    {
        this.IDCMDBCI = 0;
        this.IDSDCASE = 0; 
        //this.IDSDCASE_OF = 0; 
        this.IDSDCASE_RELATION = 0;
        this.IDSDCASE_RELATIONTYPE = 0;
        //this.IDSDCASE_THIS = 0;
        this.IDSDTYPEUSER = 0;
        this.IDSYSTEMSTATUS = 1;
        this.RELATIONS_DESCRIPTION = "";
        this.RELATIONS_TITLE = "";
        this._SDCASE_OF_TITLE = "";
        this._SDCASE_OF_CATEGORY = "";
        this._SDCASE_OF_CATEGORYNAME = "";
    }

    this.InicializeComponent();
 
}

ItHelpCenter.SD.frSDCaseSet.prototype.InicializeComponent = function () {
    var _this = this.TParent();

    _this.Modal = new TVclModal(document.body, null, "");
    _this.Modal.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.Mythis, "HeaderModal"));

    var spPrincipal = new TVclStackPanel(_this.Modal.Body.This, "1", 2, [[12, 12], [12, 12], [9, 3], [9, 3], [9, 3]]);

    spPrincipal.Row.This.style.marginRight = "0px";
    spPrincipal.Row.This.style.marginLeft = "0px";


    var spIzquierda = new TVclStackPanel(spPrincipal.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
    spIzquierda.Row.This.style.marginRight = "3px";

    spIzquierda.Column[0].This.classList.add("StackPanelContainer");

    _this.TabControl1 = new TTabControl(spIzquierda.Column[0].This, "", "");

    var TabPag1 = new TTabPage();
    TabPag1.Name = "TabItem1";
    TabPag1.Caption = UsrCfg.Traslate.GetLangText(_this.Mythis, "TabPag1");
    TabPag1.Active = true;
    _this.TabControl1.AddTabPages(TabPag1);

    var TabPag2 = new TTabPage();
    TabPag2.Name = "TabItem2";
    TabPag2.Caption = UsrCfg.Traslate.GetLangText(_this.Mythis, "TabPag2");
    _this.TabControl1.AddTabPages(TabPag2);

    var spTab1 = new TVclStackPanel(TabPag1.Page, "1", 1, [[12], [12], [12], [12], [12]]);

    _this.TSearchControl = new ItHelpCenter.UcSearchControl(spTab1.Column[0].This, "", _this);
    _this.TSearchControl.SQLStr = SysCfg.DB.SQL.Methods.GetSQLPrefixIDBuild("Atis", "GETSDCASESET", new Array(0)).StrSQL
    _this.TSearchControl.Inicialize();

    var spTab2 = new TVclStackPanel(TabPag2.Page, "1", 1, [[12], [12], [12], [12], [12]]);
    _this.ciManager = new ItHelpCenter.CIEditor.TfrCiEditor(spTab2.Column[0].This, "");


    var spDerecha = new TVclStackPanel(spPrincipal.Column[1].This, "1", 1, [[12], [12], [12], [12], [12]]);
    spDerecha.Column[0].This.classList.add("StackPanelContainer");

    var spPanelDerecha1 = new TVclStackPanel(spDerecha.Column[0].This, "1", 2, [[12, 12], [12, 12], [12, 12], [8, 4], [8, 4]]);    

    this.lbl01 = new TVcllabel(spPanelDerecha1.Column[0].This, "", TlabelType.H0);
    this.lbl01.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lbl01");
    this.lbl01.VCLType = TVCLType.BS;

    this.BtnViewCaseDetail = new TVclButton(spPanelDerecha1.Column[1].This, "");
    this.BtnViewCaseDetail.Src = "image/24/Checklist.png";
    this.BtnViewCaseDetail.VCLType = TVCLType.BS;
    this.BtnViewCaseDetail.onClick = function () {
        _this.BtnViewCaseDetail_Onclick(_this, _this.BtnViewCaseDetail);
    }

    var spPanelDerecha2 = new TVclStackPanel(spDerecha.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);

  
    this.cmbResponseType = new TVclComboBox2(spPanelDerecha2.Column[0].This, '' + ID);
    this.cmbResponseType.Visible = false;
    this.cmbResponseType.VCLType = TVCLType.BS;


    var spPanelDerecha3 = new TVclStackPanel(spDerecha.Column[0].This, "1", 2, [[12, 12], [12, 12], [12, 12], [8, 4], [8, 4]]);
    this.lbl04 = new TVcllabel(spPanelDerecha3.Column[0].This, "", TlabelType.H0);
    this.lbl04.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lbl04");
    this.lbl04.VCLType = TVCLType.BS;

    this.BtnNewCase = new TVclButton(spPanelDerecha3.Column[1].This, "");
    this.BtnNewCase.Src = "image/24/case-add.png";
    this.BtnNewCase.VCLType = TVCLType.BS;
    this.BtnNewCase.onClick = function () {
        _this.BtnNewCase_Click(_this, _this.BtnViewCaseDetail);
    }
    
    var spPanelDerecha4 = new TVclStackPanel(spDerecha.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
    this.chkImportar = new TVclInputcheckbox(spPanelDerecha4.Column[0].This, "");
    this.chkImportar.Checked = false;
    this.chkImportar.VCLType = TVCLType.BS;
    this.chkImportar.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "chkImportar");
    this.chkImportar.Visible = (!UsrCfg.Version.IsProduction);

    var spPanelDerecha5 = new TVclStackPanel(spDerecha.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
    this.chkRelated = new TVclInputcheckbox(spPanelDerecha5.Column[0].This, "");
    this.chkRelated.Checked = false;
    this.chkRelated.VCLType = TVCLType.BS;
    this.chkRelated.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "chkRelated");
    this.chkRelated.Visible = (!UsrCfg.Version.IsProduction);

    _this.Initialize();
   
}

ItHelpCenter.SD.frSDCaseSet.prototype.Initialize = function ()
{
    var _this = this.TParent();

    //TSearchControl.Initialize(GridNative, SysCfg.DB.SQL.Methods.GetSQLPrefixIDBuild("Atis", "GETSDCASESET", new Byte[0]).StrSQL);
    //TSearchControl.SearchClick = TUcSearchControl_SearchClick;
      
    if (UsrCfg.Properties.UserATRole.isServiceDesk()) {
        //_this.cmbResponseType.Items.Add(UsrCfg.SD.Properties.TSDRESPONSETYPE._SERVICEDESK);
        var VclComboBoxItem = new TVclComboBoxItem();
        VclComboBoxItem.Text = UsrCfg.SD.Properties.TSDRESPONSETYPE._SERVICEDESK.name;
        VclComboBoxItem.Value = UsrCfg.SD.Properties.TSDRESPONSETYPE._SERVICEDESK.value;
        _this.cmbResponseType.AddItem(VclComboBoxItem);
    }
    if (UsrCfg.Properties.UserATRole.isProblemManager()) {
        //_this.cmbResponseType.Items.Add(UsrCfg.SD.Properties.TSDRESPONSETYPE._PROBLEMMANAGER);
        var VclComboBoxItem = new TVclComboBoxItem();
        VclComboBoxItem.Text = UsrCfg.SD.Properties.TSDRESPONSETYPE._PROBLEMMANAGER.name;
        VclComboBoxItem.Value = UsrCfg.SD.Properties.TSDRESPONSETYPE._PROBLEMMANAGER.value;
        _this.cmbResponseType.AddItem(VclComboBoxItem);
    }
    if (_this.cmbResponseType.Options.length == 1) {
        _this.cmbResponseType.selectedIndex = 0;
    }
    else if (_this.cmbResponseType.Items.Count == 0) {
       // _this.cmbResponseType.Visibility = false;
       // _this.BtnNewCase.Visibility.Visibility = false;
    }

    if (_this.OnInicializeComponent != null) {
        _this.OnInicializeComponent();
    }
}

ItHelpCenter.SD.frSDCaseSet.prototype.TUcSearchControl_SearchClick = function (sender, e)
{
    var _this = this.TParent();
} 

ItHelpCenter.SD.frSDCaseSet.prototype.SDCASE_GETID = function (IDSDCASE)
{
    var _this = this.TParent();
    var SDCASEEXPORT = new _this.TSDCASEEXPORT();
    var ResErr;
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE.IDSDCASE.FieldName, IDSDCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var DS_SDCASE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "SDCASEEXPORT_GETID", Param.ToBytes());
        ResErr = DS_SDCASE.ResErr;
        if (ResErr.NotError)
        {
            if (DS_SDCASE.DataSet.RecordCount > 0)
            {
                DS_SDCASE.DataSet.First();
                SDCASEEXPORT.IDSDCASE = DS_SDCASE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASE.IDSDCASE.FieldName).Value;
                //Other
                SDCASEEXPORT.IDSDRUNNIGSOURCEMODEL = DS_SDCASE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDRUNNIGSOURCEMODEL.IDSDRUNNIGSOURCEMODEL.FieldName).Value;
                SDCASEEXPORT.IDSDRESPONSETYPE = DS_SDCASE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASE.IDSDRESPONSETYPE.FieldName).Value;
                SDCASEEXPORT.IDSDCASEACTIVITIES = DS_SDCASE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEACTIVITIES.IDSDCASEACTIVITIES.FieldName).Value;
                SDCASEEXPORT.CASE_TITLE = DS_SDCASE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASE.CASE_TITLE.FieldName).Value;
                SDCASEEXPORT.CATEGORY = DS_SDCASE.DataSet.RecordSet.FieldName("CATEGORY").Value;  
                SDCASEEXPORT.CATEGORYNAME = DS_SDCASE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CATEGORYNAME.FieldName).Value;
                SDCASEEXPORT.CASE_DESCRIPTION = DS_SDCASE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASE.CASE_DESCRIPTION.FieldName).Value;
                SDCASEEXPORT.IDMDCATEGORYDETAIL = DS_SDCASE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.IDMDCATEGORYDETAIL.FieldName).Value;
                SDCASEEXPORT.IDSLA = DS_SDCASE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.IDSLA.FieldName).Value;
                SDCASEEXPORT.IDMDURGENCY = DS_SDCASE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.IDMDURGENCY.FieldName).Value;
                SDCASEEXPORT.IDMDIMPACT = DS_SDCASE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.IDMDIMPACT.FieldName).Value;
                SDCASEEXPORT.IDMDPRIORITY = DS_SDCASE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASEMT.IDMDPRIORITY.FieldName).Value;
                SDCASEEXPORT.CASE_ISMAYOR = DS_SDCASE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASE.CASE_ISMAYOR.FieldName).Value;
                SDCASEEXPORT.IDSDCASETYPE = UsrCfg.SD.Properties.TIDSDCASETYPE._NORMAL;
                if (SDCASEEXPORT.IDSDCASEACTIVITIES > 0)
                {
                    if (SDCASEEXPORT.IDSDRUNNIGSOURCEMODEL == UsrCfg.SD.Properties.TSDRUNNIGSOURCEMODEL._OUTMODEL)
                    {
                        SDCASEEXPORT.IDSDCASETYPE = UsrCfg.SD.Properties.TIDSDCASETYPE._ACTIVITY_NEW;
                    }
                    else if (SDCASEEXPORT.IDSDRUNNIGSOURCEMODEL == UsrCfg.SD.Properties.TSDRUNNIGSOURCEMODEL._INMODEL)
                    {
                        SDCASEEXPORT.IDSDCASETYPE = UsrCfg.SD.Properties.TIDSDCASETYPE._ACTIVITY_MODEL;
                    }
                }
                else
                {
                    SDCASEEXPORT.IDSDCASETYPE = UsrCfg.SD.Properties.TIDSDCASETYPE._NORMAL;

                }
            }
            else
            {
                DS_SDCASE.ResErr.NotError = false;
                DS_SDCASE.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (SDCASEEXPORT);
}

ItHelpCenter.SD.frSDCaseSet.prototype.BtnViewCaseDetail_Click = function (sender, e)
{
    var _this = this.TParent();
    //SysCfg.DataGridManager.Record current = (SysCfg.DataGridManager.Record)GridNative.SelectedItem;
    //if (current != null)
    //{
    //    UsrCfg.SD.TSetSDConsoleInfo SetSDConsoleInfo = new UsrCfg.SD.TSetSDConsoleInfo();

                
    //    SetSDConsoleInfo.IDSDCASE = int.Parse(current[UsrCfg.InternoAtisNames.SDCASE.IDSDCASE.FieldName].toString());
    //    SetSDConsoleInfo.IDCMDBCI = SysCfg.App.Methods.Cfg_WindowsTask.User.IDCMDBCI;
    //    SetSDConsoleInfo.IDSDTYPEUSER = (Int32)UsrCfg.SD.Properties.TSDTypeUser._Owner;
    //    SetSDConsoleInfo = Atis.SD.Methods.SetSDConsoleInfo(ref SetSDConsoleInfo);
    //    if (SetSDConsoleInfo.ResErr.NotError == true)
    //    {
    //        if (SetSDConsoleInfo.SDConsoleAtentionStart.ResultSDConsoleAtentionStart == UsrCfg.SD.Properties.TResultSDConsoleAtentionStart._IncomeAccepted)
    //        {
    //            frSDCaseAtentionDetail TfrSDCaseAtentionDetail = new frSDCaseAtentionDetail();
    //            TfrSDCaseAtentionDetail.Initialize(SetSDConsoleInfo.SDConsoleAtentionStart.SDCONSOLEATENTION);
    //            TfrSDCaseAtentionDetail.Show();
    //        }
    //    }
    //}
}
ItHelpCenter.SD.frSDCaseSet.prototype.TableDx_SelectionChanged_1 = function (sender, e)
{
    var _this = this.TParent();
    //if (TableDx.FocusedRow != null)
    //{
    //    chkImportar.Visibility = System.Windows.Visibility.Visible;
    //    chkRelated.Visibility = System.Windows.Visibility.Visible;
    //}
    //else
    //{
    //    chkImportar.Visibility = System.Windows.Visibility.Collapsed;
    //    chkRelated.Visibility = System.Windows.Visibility.Collapsed;
    //}
}
ItHelpCenter.SD.frSDCaseSet.prototype.GridNative_SelectionChanged_1 = function (sender, e)
{
    var _this = this.TParent();
    //if (GridNative.SelectedItem != null)
    //{
    //    chkImportar.Visibility = System.Windows.Visibility.Visible;
    //    chkRelated.Visibility = System.Windows.Visibility.Visible;

    //}
    //else
    //{
    //    chkImportar.Visibility = System.Windows.Visibility.Collapsed;
    //    chkRelated.Visibility = System.Windows.Visibility.Collapsed;
    //}
}

ItHelpCenter.SD.frSDCaseSet.prototype.BtnNewCase_Click = function (sender, e)
{
    var _this = this.TParent();

    try {
        if (_this.cmbResponseType.selectedIndex != -1) {
            if (_this.chkImportar.Visible == true) {
                if (_this.IDSDCASE > 0) {
                    if ((_this.chkImportar.Checked == true) || (_this.chkRelated.Checked == true)) {
                        var SDCASEEXPORT = _this.SDCASE_GETID(_this.IDSDCASE);

                        var SDSLASelect =
                            {
                                txtDescripcion: SDCASEEXPORT.CASE_DESCRIPTION,
                                txtTitulo: SDCASEEXPORT.CASE_TITLE,
                                EnableControls: false,
                                IDMDURGENCY: SDCASEEXPORT.IDMDURGENCY,
                                IDMDIMPACT: SDCASEEXPORT.IDMDIMPACT,
                                IDMDPRIORITY: SDCASEEXPORT.IDMDPRIORITY,
                                isMayor: SDCASEEXPORT.CASE_ISMAYOR,
                                IDMDCATEGORYDETAIL: SDCASEEXPORT.IDMDCATEGORYDETAIL,
                                IDSLA: SDCASEEXPORT.IDSLA
                            };
                    }

                    var SDSLASelect =
                        {
                            CASE_DESCRIPTION: "",
                            CASE_TITLE: "",
                            IDMDURGENCY: 0,
                            IDMDIMPACT: 0,
                            IDMDPRIORITY: 0,
                            CASE_ISMAYOR: false,
                            IDMDCATEGORYDETAIL: 0,
                            IDSLA: 0,
                            IDSDCASE: 0
                        };

                    if (_this.chkImportar.Checked == true) {
                        var SDSLASelect =
                        {
                            CASE_DESCRIPTION: SDCASEEXPORT.CASE_DESCRIPTION,
                            CASE_TITLE: SDCASEEXPORT.CASE_TITLE,
                            IDMDURGENCY: SDCASEEXPORT.IDMDURGENCY,
                            IDMDIMPACT: SDCASEEXPORT.IDMDIMPACT,
                            IDMDPRIORITY: SDCASEEXPORT.IDMDPRIORITY,
                            CASE_ISMAYOR: SDCASEEXPORT.CASE_ISMAYOR,
                            IDMDCATEGORYDETAIL: SDCASEEXPORT.IDMDCATEGORYDETAIL,
                            IDSLA: SDCASEEXPORT.IDSLA,
                            IDSDCASE: SDCASEEXPORT.IDSDCASE
                        };
                    }

                    var SDCASE_RELATION = new _this.TSDCASE_RELATION();
                    if (_this.chkRelated.Checked == true) {
                        SDCASE_RELATION.IDSDCASE = SDCASEEXPORT.IDSDCASE;
                        SDCASE_RELATION._SDCASE_OF_TITLE = SDCASEEXPORT.CASE_TITLE;
                        SDCASE_RELATION._SDCASE_OF_CATEGORY = SDCASEEXPORT.CATEGORY;
                        SDCASE_RELATION._SDCASE_OF_CATEGORYNAME = SDCASEEXPORT.CATEGORYNAME;
                    }

                    var MenuObjetNewCase = UsrCfg.Properties.MenuProfiler.NewPage(_this.MenuObjet);
                    var NewCase = new ItHelpCenter.SD.CaseManager.NewCase.frSDCMCaseNew(MenuObjetNewCase, MenuObjetNewCase.ObjectDataMain, function () {
                        NewCase.InitializeComplexCase(UsrCfg.SD.Properties.TIDSDCASETYPE._NORMAL, _this.cmbResponseType.Value, SDCASEEXPORT.IDMDCATEGORYDETAIL, 0, SDSLASelect, SDCASE_RELATION,
                            function (thisOut, ResultSDCaseChangeStatus, IDSDCASE, IDSDCASEMT, IDSDTYPEUSER) {
                                if (thisOut.chkShowConsole.Checked) {
                                    if (ResultSDCaseChangeStatus == UsrCfg.SD.Properties.TResultSDCaseChangeStatus._CSSuccessfullyCreate) {
                                        _this.MenuObjetNewCase.ObjectData.innerHTML = "";
                                        _this.MenuObjetNewCase._ObjectDataMain.innerHTML = "";
                                        _this.MenuObjetNewCase.OnBeforeBackPage = null;

                                        var SMConsoleManagerSwitch = new ItHelpCenter.SD.CaseManager.Atention.TSMConsoleManagerSwitch(_this.MenuObjetNewCase, //MODO Forza caso 3 en owner             
                                        _this.MenuObjetNewCase.ObjectDataMain,
                                        function (outthis) {
                                            _this.MenuObjetNewCase.ObjectData.innerHTML = "";
                                            _this.MenuObjetNewCase._ObjectDataMain.innerHTML = "";
                                            _this.MenuObjetNewCase.OnBeforeBackPage = null;
                                        },
                                        IDSDCASE, IDSDCASEMT, IDSDTYPEUSER.value
                                        );
                                    }
                                    else {
                                        thisOut.MenuObject.BackPageOnlyParent(_this);
                                    }
                                }
                                else {
                                    thisOut.MenuObject.BackPageOnlyParent(_this);
                                }
                            });
                    });
                    _this.Modal.CloseModal();
                } 
            } 
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.frSDCaseSet.prototype.BtnNewCase_Click ", e);
    } 
} 

ItHelpCenter.SD.frSDCaseSet.prototype.BtnViewCaseDetail_Onclick = function (sender, e) {
    var _this = sender;

    if (_this.TSearchControl.ctrGrid.FocusedRowHandle != null) {
        var inIDSDCASE = parseInt(_this.TSearchControl.ctrGrid.FocusedRowHandle.GetCellValue("IDSDCASE"));

        var parameters =
        {
            IDSDCASE: inIDSDCASE,
            IDCMDBCI: SysCfg.App.Methods.Cfg_WindowsTask.User.IDCMDBCI
        };
        parameters = JSON.stringify(parameters);
        $.ajax({
            type: "POST",
            url: SysCfg.App.Properties.xRaiz + 'Service/SD/Atention.svc/SDConsoleInfo',
            data: parameters,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (response) {
                //***************************************************************************************************
                var Atention = response;

                if (Atention.ResErr.NotError == true) {
                    if (Atention.ResultSDConsoleAtentionStart == UsrCfg.SD.Properties.TResultSDConsoleAtentionStart._IncomeAccepted.value) {

                        var Modal = new TVclModal(document.body, null, "IDModalAtentionDetail");
                        Modal.Width = "85%";
                        Modal.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.Mythis, "Modal.AddHeaderContent"));//Emply
                        Modal.Body.This.id = "Modal_AtentionDetail";
                        var TfrSDCaseAtentionDetail = new ItHelpCenter.SD.Shared.Atention.CaseAtentionDetail.TfrSDCaseAtentionDetail(Modal.Body.This, function (thisOut) {
                            Modal.CloseModal();
                        }, Atention.SDCONSOLEATENTION);
                        Modal.ShowModal();
                        Modal.FunctionClose = function () {
                        }
                    }
                    else if (Atention.ResultSDConsoleAtentionStart == UsrCfg.SD.Properties.TResultSDConsoleAtentionStart._IncomeCreate.value) {
                    }
                }
                else {
                }

            },
            error: function (response) {
                SysCfg.Log.Methods.WriteLog("frSDCaseSet.js ItHelpCenter.SD.frSDCaseSet.prototype.BtnViewCaseDetail_Onclick Service/SD/Atention.svc/SDConsoleInfo " + "Error no llamo al Servicio [SDConsoleAtentionStart]");
            }
        });

    }
    else {
        alert("select case");
    }
 
}

 
ItHelpCenter.UcSearchControl = function (inObjectHTML, ID, sender) {
    var _thisCaseSet = sender;
    this.ObjectHtml = inObjectHTML;
    this.Orientation = Componet.Controls.Horizontal;
    this.CIGridSearch = null;
    this.OnSelectCI = null;

    var _this = this;
    this.EnableColumnAll = true;
    this.spPrincipal = new TVclStackPanel(inObjectHTML, ID, 1, [[12], [12], [12], [12], [12]]);

    this.spBuscador = null;
    this.ComboBoxCol = null;
    this.txtTextBox = null;
    this.BtnSearch = null;
    this.chkModo = null;
    this.SQLStr = "";

    this.Mythis = "UcSearchControl";
    UsrCfg.Traslate.GetLangText(this.Mythis, "chkModo", "All");
    UsrCfg.Traslate.GetLangText(this.Mythis, "LblColumna", "Columns");
    UsrCfg.Traslate.GetLangText(this.Mythis, "BtnSearch", "Search");

    this.Inicialize = function () {
        if (this.Orientation == Componet.Controls.Horizontal) {

            this.spBuscador = new TVclStackPanel(this.ObjectHtml, ID, 4, [[2, 4, 4, 2], [2, 4, 4, 2], [2, 4, 4, 2], [2, 4, 4, 2], [2, 4, 4, 2]]);
            this.spBuscador.Row.This.style.margin = "10px";

            var spCheck = new TVclStackPanel(this.spBuscador.Column[0].This, ID, 1, [[12], [12], [12], [12], [12]]);
            this.chkModo = new TVclInputcheckbox(spCheck.Column[0].This, "");
            this.chkModo.Checked = false;
            this.chkModo.VCLType = TVCLType.BS;
            this.chkModo.Text = UsrCfg.Traslate.GetLangText(this.Mythis, "chkModo");
            this.chkModo.OnCheckedChanged = _this.chkModo_OnCheckedChanged;

            var spColumns = new TVclStackPanel(this.spBuscador.Column[1].This, ID, 2, [[5, 7], [5, 7], [5, 7], [5, 7], [5, 7]]);

            this.LblColumna = new TVcllabel(spColumns.Column[0].This, "", TlabelType.H0);
            this.LblColumna.Text = UsrCfg.Traslate.GetLangText(this.Mythis, "LblColumna");
            this.LblColumna.VCLType = TVCLType.BS;

            this.CmbComlumna = new TVclComboBox2(spColumns.Column[1].This, 'cbColumnas_' + ID);
            this.CmbComlumna.VCLType = TVCLType.BS;

            if (this.EnableColumnAll) {
                var cbItemAll = new TVclComboBoxItem();
                cbItemAll.Value = "(ALL)";
                cbItemAll.Text = "(ALL)";
                cbItemAll.Tag = "(ALL)";
                this.CmbComlumna.AddItem(cbItemAll);
            }

            var spTextBox = new TVclStackPanel(this.spBuscador.Column[2].This, ID, 1, [[12], [12], [12], [12], [12]]);
            this.TxtSearch = new TVclTextBox(spTextBox.Column[0].This, "txtTextBox");
            this.TxtSearch.Text = ""
            this.TxtSearch.VCLType = TVCLType.BS;

            var spBotones = new TVclStackPanel(this.spBuscador.Column[3].This, ID, 1, [[12], [12], [12], [12], [12]]);
            this.BtnSearch = new TVclInputbutton(spBotones.Column[0].This, "");
            this.BtnSearch.Text = UsrCfg.Traslate.GetLangText(this.Mythis, "BtnSearch");
            this.BtnSearch.VCLType = TVCLType.BS;
            this.BtnSearch.onClick = function () {
                _this.BtnSearch_Onclick(_this, _this.BtnSearch);
            }

            var DivGrid = new TVclStackPanel(this.ObjectHtml, "DivGrid_" + ID, 1, [[12], [12], [12], [12], [12]]);
            DivGrid.Row.This.style.margin = "10px";
            this.ctrGrid = new TGrid(DivGrid.Column[0].This, "IdGridSearch", function () { });  // ItHelpCenter.CIEditor.TGrid(DivGrid.Column[0].This, "IdGridSearch")

            this.FillColCombo();
        }
    }

    this.FillColCombo = function () {
        var SQLStrTemp = "Select top 0 * From (" + this.SQLStr + ")" + " as TblSearch ";

        try {
            var DS = SysCfg.DB.SQL.Methods.Open(SQLStrTemp, new Array(0));
            if (DS.ResErr.NotError) {
                for (var i = 0; i < DS.DataSet.FieldDefs.length; i++) {
                    var cbItemAll = new TVclComboBoxItem();
                    cbItemAll.Value = DS.DataSet.FieldDefs[i].FieldName;
                    cbItemAll.Text = DS.DataSet.FieldDefs[i].FieldName;
                    cbItemAll.Tag = DS.DataSet.FieldDefs[i];
                    this.CmbComlumna.AddItem(cbItemAll);
                }
                this.CmbComlumna.selectedIndex = 0;
            }
        }
        finally {
        }
    }

    this.chkModo_OnCheckedChanged = function (sender) {
        if (sender.Checked == true) {
            _this.LblColumna.Visible = false;
            _this.CmbComlumna.Visible = false;
            _this.TxtSearch.Visible = false;
            _this.BtnSearch.Visible = false;
            _this.CargarGrilla(_this.CmbComlumna.Options[_this.CmbComlumna.selectedIndex].Text, _this.TxtSearch.Text, true);
        }
        else {
            _this.LblColumna.Visible = true;
            _this.CmbComlumna.Visible = true;
            _this.TxtSearch.Visible = true;
            _this.BtnSearch.Visible = true;
            _this.CmbComlumna.selectedIndex = 0;
            _this.TxtSearch.Text = "";
            //ActualizaColumnasNombre();
        }
    }
    this.BtnSearch_Onclick = function (sender, e) {
        var _this = sender;
        _this.CargarGrilla(_this.CmbComlumna.Options[_this.CmbComlumna.selectedIndex].Text, _this.TxtSearch.Text, false);
    }

    this.CargarGrilla = function (Paramcolumna, ParamValue, showAll) {

        var Param = "";

        if (!showAll) {
            if (Paramcolumna == "(ALL)") {
                Param = " WHERE " + _this.CmbComlumna.Options[1].Text + " LIKE '" + ParamValue + "%'";

                for (var i = 2; i < _this.CmbComlumna.Options.length; i++) {
                    Param += " OR " + _this.CmbComlumna.Options[i].Text + " LIKE '" + ParamValue + "%'";
                }
            }
            else {
                Param = " WHERE " + Paramcolumna + " LIKE '%" + ParamValue + "%'";
            }
        }

        StrOut = "SELECT * FROM (" + _this.SQLStr + " ) as TblSearch " + Param;


        var Open = SysCfg.DB.SQL.Methods.Open(StrOut, new Array(0));

        if (Open.ResErr.NotError) {
            _this.ctrGrid.DataSource = Open.DataSet;
        }

        this.ctrGrid.OnRowClick = function (sender, evenArgs) {          
            //var _IDCMDBCI = sender.FocusedRowHandle.GetCellValue("IDCMDBCI");
            _thisCaseSet.IDSDCASE = parseInt(evenArgs.Cells[0].Value);
            //if (_this.OnSelectCI != null)
            //    _this.OnSelectCI(_this, _IDCMDBCI);
        }
    }

}





