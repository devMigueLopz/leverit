﻿ItHelpCenter.SD.CaseManager.NewCase.frSDCaseNew_Template = function (inMenuObject, inObject, infrSDCaseNew, incallback) {
    this.TParent = function () {
        return this;
    }.bind(this);

    this.isMenu = (inMenuObject != null);
    var _this = this.TParent();
    this.ObjectHtml = inObject;
    this.MenuObject = inMenuObject;

    this.CallbackLoad = incallback
    this.CallbackModalResult = null;
    this.DBTranslate = null;
    this.Parent = infrSDCaseNew;
    this.Mythis = "frSDCaseNew_Template";
    this.ID = this.Mythis + (Math.floor(Math.random() * 1000));
   

    this.SDCASETEMPLATEList = new Array();  
    this.SDCASETEMPLATE_SET = null;
    this.LastSatus = null;  
    this.EnableControls = true;
    this.GridTemplate_SelectedItem = null;


    ////TRADUCCION   
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lblTitle.Text", "Title");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lblDescription.Text", "Description");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "cbxPublic.Text", "Public");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "cbxEnable.Text", "Enable");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "chkPublic.Text", "Other user");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "txtStatus.Text", "Status: ---");
    
    this.TTEMPLATE_CODECFG = function()
    {
        this._Version = "001";            
        this.CASE_DESCRIPTION="";
        this.CASE_TITLE="";
        this.IDMDMODELTYPED=-1;
        this.IDSDCASETYPE = UsrCfg.SD.Properties.TIDSDCASETYPE._NORMAL;
        this.IDMDCATEGORYDETAIL=-1;
        this.IDSLA=-1;
        this.IDMDIMPACT=-1;
        this.IDMDPRIORITY=-1;
        this.IDMDURGENCY=-1;
        this.CASE_ISMAYOR=false;
        //Str IO Properties

        var TEMPLATEThis = this;

        this.ToStr = function(Longitud, Texto)
        {                
            SysCfg.Str.Protocol.WriteStr(TEMPLATEThis.CASE_DESCRIPTION, Longitud, Texto);
            SysCfg.Str.Protocol.WriteStr(TEMPLATEThis.CASE_TITLE, Longitud, Texto);
            SysCfg.Str.Protocol.WriteInt(TEMPLATEThis.IDMDMODELTYPED, Longitud, Texto);
            SysCfg.Str.Protocol.WriteInt(TEMPLATEThis.IDSDCASETYPE.value, Longitud, Texto);
            SysCfg.Str.Protocol.WriteInt(TEMPLATEThis.IDMDCATEGORYDETAIL, Longitud, Texto);
            SysCfg.Str.Protocol.WriteInt(TEMPLATEThis.IDSLA, Longitud, Texto);
            SysCfg.Str.Protocol.WriteInt(TEMPLATEThis.IDMDIMPACT, Longitud, Texto);
            SysCfg.Str.Protocol.WriteInt(TEMPLATEThis.IDMDPRIORITY, Longitud, Texto);
            SysCfg.Str.Protocol.WriteInt(TEMPLATEThis.IDMDURGENCY, Longitud, Texto);
            SysCfg.Str.Protocol.WriteBool(TEMPLATEThis.CASE_ISMAYOR, Longitud, Texto);
        }

        this.StrTo = function(Pos,Index, LongitudArray, Texto)
        {               
            this.CASE_DESCRIPTION = SysCfg.Str.Protocol.ReadStr(Pos,Index, LongitudArray, Texto);
            this.CASE_TITLE = SysCfg.Str.Protocol.ReadStr(Pos,Index, LongitudArray, Texto);
            this.IDMDMODELTYPED = SysCfg.Str.Protocol.ReadInt(Pos,Index, LongitudArray, Texto);
            this.IDSDCASETYPE =   SysCfg.Str.Protocol.ReadInt(Pos,Index, LongitudArray, Texto);
            this.IDMDCATEGORYDETAIL = SysCfg.Str.Protocol.ReadInt(Pos,Index, LongitudArray, Texto);
            this.IDSLA = SysCfg.Str.Protocol.ReadInt(Pos,Index, LongitudArray, Texto);
            this.IDMDIMPACT = SysCfg.Str.Protocol.ReadInt(Pos,Index, LongitudArray, Texto);
            this.IDMDPRIORITY = SysCfg.Str.Protocol.ReadInt(Pos,Index, LongitudArray, Texto);
            this.IDMDURGENCY = SysCfg.Str.Protocol.ReadInt(Pos,Index, LongitudArray, Texto);
            this.CASE_ISMAYOR = SysCfg.Str.Protocol.ReadBool(Pos,Index, LongitudArray, Texto);
        }
    }

    this.TSDCASETEMPLATE = function()
    {
        this.CI_GENERICNAME = "";
        this.IDSDCASETEMPLATE = 0;
        this.TEMPLATE_CODECFG = new _this.TTEMPLATE_CODECFG(); 
        this.TEMPLATE_CODECFGSTR  = "";
        this.TEMPLATE_DATECREATE = "";
        this.TEMPLATE_DESCRIPTION  = "";
        this.TEMPLATE_ENABLE  = false;
        this.TEMPLATE_IDCMDBCI = 0;
        this.TEMPLATE_IDSDCASETYPE = UsrCfg.SD.Properties.TIDSDCASETYPE;
        this.TEMPLATE_ISPUBLIC= false;
        this.TEMPLATE_NAME = "";
    }
     
    //this.TSatus  =
    //{
    //    _Refresh: 0,
    //    _NoItems: 1,
    //    _Set: 2,
    //    _Import: 3,            
    //    _Acept: 4,
    //    _Update: 5,
    //    _Delete: 7,
    //    _Cancel: 6
    //}

    this.TSatus = {
        _Refresh: { value: 0, name: "_Refresh" },
        _NoItems: { value: 1, name: "_NoItems" },
        _Set: { value: 2, name: "_Set" },
        _Import: { value: 3, name: "_Import" },
        _Acept: { value: 4, name: "_Acept" },
        _Update: { value: 5, name: "_Update" },
        _Delete: { value: 7, name: "_Delete" },
        _Cancel: { value: 6, name: "_Cancel" },
    };

    _this.InitializeComponent();
}
 
ItHelpCenter.SD.CaseManager.NewCase.frSDCaseNew_Template.prototype.Initialize = function () {
    var _this = this.TParent();

    _this.SDCASETEMPLATEList = new Array(); // _this.TSDCASETEMPLATE();
    _this.LastSatus = _this.TSatus._NoItems;

    _this.EnableControls = true;

    _this.Set(_this.TSatus._Refresh);
}


ItHelpCenter.SD.CaseManager.NewCase.frSDCaseNew_Template.prototype.SDCASETEMPLATE_PUT_ITFZ = function(SDCASETEMPLATE)
{
    var _this = this.TParent();

    _this.EnableControls = false;
    _this.txtTitle.Enabled = false;
    _this.txtDescription.Enabled = false;
    _this.cbxPublic.Enabled = false;
    _this.cbxEnable.Enabled = false;

    try
    {

        if (SDCASETEMPLATE == null)
        {
            //_this.lblTitle.Visible = false;
            //_this.txtTitle.Visible = false;
            //_this.lblDescription.Visible = false;
            //_this.txtDescription.Visible = false;
            //_this.cbxPublic.Visible = false;
            //_this.cbxEnable.Visible = false;

            _this.txtTitle.Text = "";
            _this.txtDescription.Text = "";
            _this.cbxPublic.Checked = false;
            _this.cbxEnable.Checked = false;

            _this.lblTitle.Enabled = false;
            _this.txtTitle.Enabled = false;
            _this.lblDescription.Enabled = false;
            _this.txtDescription.Enabled = false;
            _this.cbxPublic.Enabled = false;
            _this.cbxEnable.Enabled = false;
          

        } else {
            //_this.lblTitle.Visible = true;
            //_this.txtTitle.Visible = true;
            //_this.lblDescription.Visible = true;
            //_this.txtDescription.Visible = true;
            //_this.cbxPublic.Visible = true;
            //_this.cbxEnable.Visible = true;

             
            _this.txtTitle.Text = SDCASETEMPLATE.TEMPLATE_NAME;
            _this.txtDescription.Text = SDCASETEMPLATE.TEMPLATE_DESCRIPTION;
            _this.cbxPublic.Checked = SDCASETEMPLATE.TEMPLATE_ISPUBLIC;
            _this.cbxEnable.Checked = SDCASETEMPLATE.TEMPLATE_ENABLE;

            if (_this.SDCASETEMPLATE_SET.TEMPLATE_IDCMDBCI == SysCfg.App.Methods.Cfg_WindowsTask.User.IDCMDBCI) {
                _this.EnableControls = true;
                _this.txtTitle.Enabled = true;
                _this.txtDescription.Enabled = true;
                _this.cbxPublic.Enabled = true;
                _this.cbxEnable.Enabled = true;
            }
        }
        
    }
    finally
    {
        _this.EnableControls = true;
    }
}

ItHelpCenter.SD.CaseManager.NewCase.frSDCaseNew_Template.prototype.SDCASETEMPLATE_GET_ITFZ = function (SDCASETEMPLATE)
{
    var _this = this.TParent();

    if (SDCASETEMPLATE != null)
    {
        SDCASETEMPLATE.TEMPLATE_NAME = _this.txtTitle.Text;
        SDCASETEMPLATE.TEMPLATE_DESCRIPTION = _this.txtDescription.Text;
        SDCASETEMPLATE.TEMPLATE_ISPUBLIC = _this.cbxPublic.Checked;
        SDCASETEMPLATE.TEMPLATE_ENABLE = _this.cbxEnable.Checked;
    }
}

ItHelpCenter.SD.CaseManager.NewCase.frSDCaseNew_Template.prototype.TEMPLATE_CODECFG_GET_ITFZ = function (TEMPLATE_CODECFG)
{
    var _this = this.TParent();

    if (TEMPLATE_CODECFG != null)
    {
        TEMPLATE_CODECFG.CASE_DESCRIPTION = _this.Parent.TfrSDSLASelect.txtDescripcion.Text;
        TEMPLATE_CODECFG.CASE_TITLE = _this.Parent.TfrSDSLASelect.txtTitulo.Text;
        TEMPLATE_CODECFG.IDMDMODELTYPED = _this.Parent.TfrSDSLASelect.MT_IDMDMODELTYPED;
        TEMPLATE_CODECFG.IDSDCASETYPE = _this.Parent.IDSDCASETYPE;
        TEMPLATE_CODECFG.IDMDCATEGORYDETAIL = _this.Parent.TfrSDSLASelect.IDMDCATEGORYDETAIL;
        TEMPLATE_CODECFG.IDSLA = _this.Parent.TfrSDSLASelect.IDSLA;
        TEMPLATE_CODECFG.IDMDIMPACT = parseInt(_this.Parent.TfrSDSLASelect.IDMDIMPACT);
        TEMPLATE_CODECFG.IDMDPRIORITY = parseInt(_this.Parent.TfrSDSLASelect.IDMDPRIORITY);
        TEMPLATE_CODECFG.IDMDURGENCY = parseInt(_this.Parent.TfrSDSLASelect.IDMDURGENCY);
        TEMPLATE_CODECFG.CASE_ISMAYOR = _this.Parent.TfrSDSLASelect.CheckisMayor.Checked;
    } 
}

ItHelpCenter.SD.CaseManager.NewCase.frSDCaseNew_Template.prototype.Set = function (Satus)
{
    var _this = this.TParent();

    //_this.btnRefresh.Visible =false;
    //_this.btnExportCase.Visible = false;
    //_this.btnImportCase.Visible = false;
    //_this.btnOk.Visible = false;
    //_this.btnDelete.Visible = false;
    //_this.btnCancel.Visible = false;
 
    _this.btnRefresh.Visible = false;
    $(_this.divbtnExportCase).css("display","none");
    $(_this.divbtnImportCase).css("display","none");
    $(_this.divbtnOk).css("display","none");
    $(_this.divbtnDelete).css("display","none");
    $(_this.divbtnCancel).css("display", "none");

    //_this.btnRefresh.Enabled = false;
    //_this.btnExportCase.Enabled = false;
    //_this.btnImportCase.Enabled = false;
    //_this.btnOk.Enabled = false;
    //_this.btnDelete.Enabled = false;
    //_this.btnCancel.Enabled = false;

    switch (Satus.value)
    {
        case _this.TSatus._Refresh.value:
            _this.SDCASETEMPLATE_Fill(_this.SDCASETEMPLATEList, SysCfg.App.Methods.Cfg_WindowsTask.User.IDCMDBCI, _this.chkPublic.Checked, _this.Parent.IDSDCASETYPE);          
            //GridTemplate.ItemsSource = _this.SDCASETEMPLATEList;
            if (_this.SDCASETEMPLATEList.length == 0)
            {
                _this.DivGrid.innerHTML = "";
                _this.Set(_this.TSatus._NoItems);
                return;
            }
            else
            {
                //GridTemplate.SelectedIndex =0;
                _this.Set(_this.TSatus._Set);
                return;
            }
            break;
        case _this.TSatus._NoItems.value:
                    
            _this.SDCASETEMPLATE_SET = null;
            _this.SDCASETEMPLATE_PUT_ITFZ(_this.SDCASETEMPLATE_SET);
            //_this.btnImportCase.Visible = true;
            $(_this.divbtnImportCase).css("display", "");
            //_this.btnImportCase.Enabled = true;
            break;
        case _this.TSatus._Set.value:
            if (_this.GridTemplate_SelectedItem != null)
            {
                var SDCASETEMPLATE = _this.GridTemplate_SelectedItem;
                _this.SDCASETEMPLATE_SET = _this.SDCASETEMPLATE_ListSetID(_this.SDCASETEMPLATEList, SDCASETEMPLATE.IDSDCASETEMPLATE);

            }
            _this.SDCASETEMPLATE_PUT_ITFZ(_this.SDCASETEMPLATE_SET);
            _this.btnRefresh.Visible = true;
            //_this.ImportCase.Visible = true;
            //_this.ExportCase.Visible = true;
            $(_this.divbtnImportCase).css("display", "");
            $(_this.divbtnExportCase).css("display", "");

            //_this.btnRefresh.Enabled = true;
            //_this.btnImportCase.Enabled = true;
            //_this.btnExportCase.Enabled = true;
            if (_this.SDCASETEMPLATE_SET.TEMPLATE_IDCMDBCI == SysCfg.App.Methods.Cfg_WindowsTask.User.IDCMDBCI)
            {
                //_this.btnDelete.Visible = true;
                //_this.btnDelete.Enabled = true;
                $(_this.divbtnDelete).css("display", "");
            }
                    
            break;
        case _this.TSatus._Import.value:


            //_this.btnCancel.Visible = true;
            //_this.btnOk.Visible = true;

            $(_this.divbtnCancel).css("display", "");
            $(_this.divbtnOk).css("display", "");

            //_this.btnCancel.Enabled = true;
            //_this.btnOk.Enabled = true;

            _this.SDCASETEMPLATE_SET = new _this.TSDCASETEMPLATE();
            _this.TEMPLATE_CODECFG_GET_ITFZ(_this.SDCASETEMPLATE_SET.TEMPLATE_CODECFG);
            _this.SDCASETEMPLATE_SET.TEMPLATE_CODECFGSTR = _this.TEMPLATE_CODECFGtoStr(_this.SDCASETEMPLATE_SET.TEMPLATE_CODECFG);
            _this.SDCASETEMPLATE_SET.TEMPLATE_DATECREATE = SysCfg.DB.Properties.SVRNOW();
            _this.SDCASETEMPLATE_SET.CI_GENERICNAME = SysCfg.App.Methods.Cfg_WindowsTask.User.CI_GENERICNAME;
            _this.SDCASETEMPLATE_SET.TEMPLATE_IDCMDBCI = SysCfg.App.Methods.Cfg_WindowsTask.User.IDCMDBCI;
            _this.SDCASETEMPLATE_SET.TEMPLATE_NAME = "";
            _this.SDCASETEMPLATE_SET.TEMPLATE_DESCRIPTION = "";
            _this.SDCASETEMPLATE_SET.TEMPLATE_ISPUBLIC = false;
            _this.SDCASETEMPLATE_SET.TEMPLATE_ENABLE = true;
            _this.SDCASETEMPLATE_SET.TEMPLATE_IDSDCASETYPE = _this.SDCASETEMPLATE_SET.TEMPLATE_CODECFG.IDSDCASETYPE;
            _this.SDCASETEMPLATE_PUT_ITFZ(_this.SDCASETEMPLATE_SET);
            break;
        case _this.TSatus._Acept.value:
            switch (_this.LastSatus.value)
            {
                case _this.TSatus._Import.value:
                    _this.SDCASETEMPLATE_GET_ITFZ(_this.SDCASETEMPLATE_SET);
                    _this.SDCASETEMPLATE_ADD(_this.SDCASETEMPLATE_SET);
                   // _this.SDCASETEMPLATEList.push(_this.SDCASETEMPLATE_SET);
                    //GridTemplate.ItemsSource = _this.SDCASETEMPLATEList;
                    //GridTemplate.SelectedIndex = _this.SDCASETEMPLATEList.length - 1;

                    _this.SDCASETEMPLATEList = new Array();
                    _this.SDCASETEMPLATE_Fill(_this.SDCASETEMPLATEList, SysCfg.App.Methods.Cfg_WindowsTask.User.IDCMDBCI, _this.chkPublic.Checked, _this.Parent.IDSDCASETYPE);
                    _this.Set(_this.TSatus._Set);
                    return;
                    break;
                case _this.TSatus._Update.value:
                    _this.SDCASETEMPLATE_GET_ITFZ(_this.SDCASETEMPLATE_SET);
                    _this.SDCASETEMPLATE_UPD(_this.SDCASETEMPLATE_SET);
                    //Int32 Idx = GridTemplate.SelectedIndex;
                    //GridTemplate.ItemsSource = null;
                    //GridTemplate.ItemsSource = _this.SDCASETEMPLATEList;
                    //GridTemplate.SelectedIndex = Idx;
                    //GridTemplate.SelectedIndex = SDCASETEMPLATE_ListGetIndex(_this.SDCASETEMPLATEList, _this.SDCASETEMPLATE_SET);

                    _this.SDCASETEMPLATEList = new Array();
                    _this.SDCASETEMPLATE_Fill(_this.SDCASETEMPLATEList, SysCfg.App.Methods.Cfg_WindowsTask.User.IDCMDBCI, _this.chkPublic.Checked, _this.Parent.IDSDCASETYPE);

                    _this.Set(_this.TSatus._Set);
                    //refresca grid 
                    return;
                    break;
            }
            break;
        case _this.TSatus._Update.value:
            //_this.divbtnCancel.Visible = true;
            //_this.divbtnOk.Visible = true;

            //_this.btnCancel.Enabled = true;
            //_this.btnOk.Enabled = true;

            $(_this.divbtnCancel).css("display", "");
            $(_this.divbtnOk).css("display", "");

            break;
        case _this.TSatus._Delete.value:
            _this.SDCASETEMPLATE_DEL(_this.SDCASETEMPLATE_SET);
           // _this.SDCASETEMPLATEList.RemoveAt(SDCASETEMPLATE_ListGetIndex(_this.SDCASETEMPLATEList, _this.SDCASETEMPLATE_SET));
            _this.Set(_this.TSatus._Refresh);
            return;
            break;
        case _this.TSatus._Cancel.value:
            switch (_this.LastSatus.value)
            {
                case _this.TSatus._Import.value:
                    _this.SDCASETEMPLATE_SET = null;
                    _this.Set(_this.TSatus._Refresh);
                    return;
                    break;
                case _this.TSatus._Update.value:
                    _this.Set(_this.TSatus._Set);
                    return;
                    break;
            }
            break;
    }
    
    _this.LastSatus = Satus;
    _this.txtStatus.Text = _this.LastSatus.name;
}

 
//Funciones 
ItHelpCenter.SD.CaseManager.NewCase.frSDCaseNew_Template.prototype.SDCASETEMPLATE_Fill = function (SDCASETEMPLATEList, TEMPLATE_IDCMDBCI, TEMPLATE_ISPUBLIC, TEMPLATE_IDSDCASETYPE)
{
    var _this = this.TParent();

    var ResErr;
    SDCASETEMPLATEList.length = 0;
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    Param.AddInt32(UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_IDSDCASETYPE.FieldName, TEMPLATE_IDSDCASETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    Param.AddInt32(UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_IDCMDBCI.FieldName, TEMPLATE_IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

    var StrExtSQL="";
    if (TEMPLATE_ISPUBLIC)
    {
        StrExtSQL =") or (SDCASETEMPLATE.TEMPLATE_ISPUBLIC<>0";
                
    }            
    Param.AddUnknown(UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_ISPUBLIC.FieldName, StrExtSQL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try
    {
        var DS_SDCASETEMPLATE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "SDCASETEMPLATE_GET", Param.ToBytes());
        ResErr = DS_SDCASETEMPLATE.ResErr;
        if (ResErr.NotError)
        {
            if (DS_SDCASETEMPLATE.DataSet.RecordCount > 0)
            {
                
                DS_SDCASETEMPLATE.DataSet.First();
                while (!(DS_SDCASETEMPLATE.DataSet.Eof))
                {
                    var SDCASETEMPLATE = new _this.TSDCASETEMPLATE(); 

                    SDCASETEMPLATE.CI_GENERICNAME = DS_SDCASETEMPLATE.DataSet.RecordSet.FieldName("CI_GENERICNAME").asString();
                    SDCASETEMPLATE.IDSDCASETEMPLATE = DS_SDCASETEMPLATE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASETEMPLATE.IDSDCASETEMPLATE.FieldName).asInt32();
                    SDCASETEMPLATE.TEMPLATE_CODECFGSTR = DS_SDCASETEMPLATE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_CODECFGSTR.FieldName).asString();
                    _this.StrtoTEMPLATE_CODECFG(SDCASETEMPLATE.TEMPLATE_CODECFGSTR, SDCASETEMPLATE.TEMPLATE_CODECFG);
                    //public static String TEMPLATE_CODECFGtoStr(TTEMPLATE_CODECFG TEMPLATE_CODECFG)                           
                    SDCASETEMPLATE.TEMPLATE_DATECREATE = DS_SDCASETEMPLATE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_DATECREATE.FieldName).asDateTime();
                    SDCASETEMPLATE.TEMPLATE_ENABLE = DS_SDCASETEMPLATE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_ENABLE.FieldName).asBoolean();
                    SDCASETEMPLATE.TEMPLATE_IDCMDBCI = DS_SDCASETEMPLATE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_IDCMDBCI.FieldName).asInt32();
                    SDCASETEMPLATE.TEMPLATE_IDSDCASETYPE = DS_SDCASETEMPLATE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_IDSDCASETYPE.FieldName).asInt32();
                    SDCASETEMPLATE.TEMPLATE_ISPUBLIC = DS_SDCASETEMPLATE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_ISPUBLIC.FieldName).asBoolean();
                    SDCASETEMPLATE.TEMPLATE_NAME = DS_SDCASETEMPLATE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_NAME.FieldName).asString();
                    SDCASETEMPLATE.TEMPLATE_DESCRIPTION = DS_SDCASETEMPLATE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_DESCRIPTION.FieldName).asString();
                    SDCASETEMPLATEList.push(SDCASETEMPLATE);

                    DS_SDCASETEMPLATE.DataSet.Next();
                }

                _this.GridTemplate_Fill(DS_SDCASETEMPLATE.DataSet);
            }
            else
            {
                DS_SDCASETEMPLATE.ResErr.NotError = false;
                DS_SDCASETEMPLATE.ResErr.Mesaje = "ecord Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }

    return  (ResErr);
}

ItHelpCenter.SD.CaseManager.NewCase.frSDCaseNew_Template.prototype.SDCASETEMPLATE_GETID = function(SDCASETEMPLATE)
{
    var _this = this.TParent();

    var ResErr;
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddDateTime(UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_DATECREATE.FieldName, SDCASETEMPLATE.TEMPLATE_DATECREATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_ENABLE.FieldName, SDCASETEMPLATE.TEMPLATE_ENABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddInt32(UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_IDCMDBCI.FieldName, SDCASETEMPLATE.TEMPLATE_IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_IDSDCASETYPE.FieldName,SDCASETEMPLATE.TEMPLATE_IDSDCASETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_ISPUBLIC.FieldName, SDCASETEMPLATE.TEMPLATE_ISPUBLIC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Where);
        Param.AddString(UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_NAME.FieldName, SDCASETEMPLATE.TEMPLATE_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        
        var DS_SDCASETEMPLATE = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "SDCASETEMPLATE_GETID", Param.ToBytes());
        ResErr = DS_SDCASETEMPLATE.ResErr;
        if (ResErr.NotError)
        {
            if (DS_SDCASETEMPLATE.DataSet.RecordCount > 0)
            {
                DS_SDCASETEMPLATE.DataSet.First();
                SDCASETEMPLATE.IDSDCASETEMPLATE = DS_SDCASETEMPLATE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASETEMPLATE.IDSDCASETEMPLATE.FieldName).asInt32();
            }
            else
            {
                DS_SDCASETEMPLATE.ResErr.NotError = false;
                DS_SDCASETEMPLATE.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}

ItHelpCenter.SD.CaseManager.NewCase.frSDCaseNew_Template.prototype.SDCASETEMPLATE_ADD = function(SDCASETEMPLATE)
{
    var _this = this.TParent();

    var ResErr;
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddText(UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_CODECFGSTR.FieldName, SDCASETEMPLATE.TEMPLATE_CODECFGSTR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_DATECREATE.FieldName, SDCASETEMPLATE.TEMPLATE_DATECREATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_DESCRIPTION.FieldName, SDCASETEMPLATE.TEMPLATE_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_ENABLE.FieldName, SDCASETEMPLATE.TEMPLATE_ENABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_IDCMDBCI.FieldName, SDCASETEMPLATE.TEMPLATE_IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_IDSDCASETYPE.FieldName, SDCASETEMPLATE.TEMPLATE_IDSDCASETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_ISPUBLIC.FieldName, SDCASETEMPLATE.TEMPLATE_ISPUBLIC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_NAME.FieldName, SDCASETEMPLATE.TEMPLATE_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "SDCASETEMPLATE_ADD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            _this.SDCASETEMPLATE_GETID(SDCASETEMPLATE);
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}

ItHelpCenter.SD.CaseManager.NewCase.frSDCaseNew_Template.prototype.SDCASETEMPLATE_UPD = function(SDCASETEMPLATE)
{
    var _this = this.TParent();

    var ResErr;
    var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.SDCASETEMPLATE.IDSDCASETEMPLATE.FieldName, SDCASETEMPLATE.IDSDCASETEMPLATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_CODECFGSTR.FieldName, SDCASETEMPLATE.TEMPLATE_CODECFGSTR, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_DATECREATE.FieldName, SDCASETEMPLATE.TEMPLATE_DATECREATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_DESCRIPTION.FieldName, SDCASETEMPLATE.TEMPLATE_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_ENABLE.FieldName, SDCASETEMPLATE.TEMPLATE_ENABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_IDCMDBCI.FieldName, SDCASETEMPLATE.TEMPLATE_IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_IDSDCASETYPE.FieldName, SDCASETEMPLATE.TEMPLATE_IDSDCASETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddBoolean(UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_ISPUBLIC.FieldName, SDCASETEMPLATE.TEMPLATE_ISPUBLIC, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.SDCASETEMPLATE.TEMPLATE_NAME.FieldName, SDCASETEMPLATE.TEMPLATE_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "SDCASETEMPLATE_UPD", Param.ToBytes());
        ResErr = Exec.ResErr;
        if (ResErr.NotError)
        {
            //GET(); 
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}

ItHelpCenter.SD.CaseManager.NewCase.frSDCaseNew_Template.prototype.SDCASETEMPLATE_DEL = function(SDCASETEMPLATE)
{
  var _this = this.TParent();        
  var ResErr;
  var Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.SDCASETEMPLATE.IDSDCASETEMPLATE.FieldName, SDCASETEMPLATE.IDSDCASETEMPLATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Exec = SysCfg.DB.SQL.Methods.Exec("Atis", "SDCASETEMPLATE_DEL_2", Param.ToBytes());
        ResErr = Exec.ResErr;
    }
    finally
    {
        Param.Destroy();
    }
    return (ResErr);
}

//String Function 
ItHelpCenter.SD.CaseManager.NewCase.frSDCaseNew_Template.prototype.TEMPLATE_CODECFGtoStr = function(TEMPLATE_CODECFG)
{
    var _this = this.TParent();             
    //var Res = "001(1)0";
    //var Longitud = "";
    //var Texto = "";

    var Res = "001(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");

    try
    {

        TEMPLATE_CODECFG.ToStr(Longitud, Texto);   
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);

        Res = "001(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e)
    {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "TEMPLATE_CODECFGtoStr:" + e.Message;
    }
    return Res;
}

ItHelpCenter.SD.CaseManager.NewCase.frSDCaseNew_Template.prototype.StrtoTEMPLATE_CODECFG=function(ProtocoloStr, TEMPLATE_CODECFG)
{
    var _this = this.TParent();
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try
    { 
        if ("001" == ProtocoloStr.substring(0, 3))
        {
            //Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")") - ProtocoloStr.indexOf("(") - 1);
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));

            //Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length - ProtocoloStr.indexOf(")") - 1);
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            

            LongitudArray = Longitud.split(",");
            TEMPLATE_CODECFG.StrTo(Pos, Index, LongitudArray, Texto);  
        }
    }
     catch (e)
    {
         var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "StrtoTEMPLATE_CODECFG:" + e.Message;
    }
    return (Res);
}

//List Function 
ItHelpCenter.SD.CaseManager.NewCase.frSDCaseNew_Template.prototype.SDCASETEMPLATE_ListSetID=function(SDCASETEMPLATEList, IDSDCASETEMPLATE)
{
    var _this = this.TParent();

    for (var i = 0; i < _this.SDCASETEMPLATEList.length; i++)
    {
        if (IDSDCASETEMPLATE == _this.SDCASETEMPLATEList[i].IDSDCASETEMPLATE)
            return (_this.SDCASETEMPLATEList[i]);
    }

    var TSDCASETEMPLATE = new _this.TSDCASETEMPLATE();
    TSDCASETEMPLATE.IDSDCASETEMPLATE = IDSDCASETEMPLATE;
    return (TSDCASETEMPLATE); 
}

ItHelpCenter.SD.CaseManager.NewCase.frSDCaseNew_Template.prototype.SDCASETEMPLATE_ListAdd=function(SDCASETEMPLATEList, SDCASETEMPLATE)
{
    var _this = this.TParent();

    var i = SDCASETEMPLATE_ListGetIndex(SDCASETEMPLATEList, SDCASETEMPLATE);
    if (i == -1) SDCASETEMPLATEList.Add(SDCASETEMPLATE);
    else
    {
        SDCASETEMPLATEList[i].IDSDCASETEMPLATE = SDCASETEMPLATE.IDSDCASETEMPLATE;
        SDCASETEMPLATEList[i].TEMPLATE_CODECFGSTR = SDCASETEMPLATE.TEMPLATE_CODECFGSTR;
        SDCASETEMPLATEList[i].TEMPLATE_DATECREATE = SDCASETEMPLATE.TEMPLATE_DATECREATE;
        SDCASETEMPLATEList[i].TEMPLATE_DESCRIPTION = SDCASETEMPLATE.TEMPLATE_DESCRIPTION;
        SDCASETEMPLATEList[i].TEMPLATE_ENABLE = SDCASETEMPLATE.TEMPLATE_ENABLE;
        SDCASETEMPLATEList[i].TEMPLATE_IDCMDBCI = SDCASETEMPLATE.TEMPLATE_IDCMDBCI;
        SDCASETEMPLATEList[i].TEMPLATE_IDSDCASETYPE = SDCASETEMPLATE.TEMPLATE_IDSDCASETYPE;
        SDCASETEMPLATEList[i].TEMPLATE_ISPUBLIC = SDCASETEMPLATE.TEMPLATE_ISPUBLIC;
        SDCASETEMPLATEList[i].TEMPLATE_NAME = SDCASETEMPLATE.TEMPLATE_NAME;
    }
}

ItHelpCenter.SD.CaseManager.NewCase.frSDCaseNew_Template.prototype.SDCASETEMPLATE_ListGetIndex=function(SDCASETEMPLATEList, SDCASETEMPLATE)
{
    var _this = this.TParent();

    for (var i = 0; i < SDCASETEMPLATEList.length; i++)
    {
        if (SDCASETEMPLATE.IDSDCASETEMPLATE == SDCASETEMPLATEList[i].IDSDCASETEMPLATE)
            return (i);
    }
    return (-1);
}

ItHelpCenter.SD.CaseManager.NewCase.frSDCaseNew_Template.prototype.SDCASETEMPLATE_ListGetIndex = function(SDCASETEMPLATEList, IDSDCASETEMPLATE)
{
     var _this = this.TParent();

     for (var i = 0; i < SDCASETEMPLATEList.length; i++)
    {
        if (IDSDCASETEMPLATE == SDCASETEMPLATEList[i].IDSDCASETEMPLATE)
            return (i);
    }
    return (-1);
}


//Botones
ItHelpCenter.SD.CaseManager.NewCase.frSDCaseNew_Template.prototype.btnExportCase_Click = function (sender, e)
{
    var _this = sender;

    _this.Parent.TfrSDSLASelect.txtDescripcion.Text = _this.SDCASETEMPLATE_SET.TEMPLATE_CODECFG.CASE_DESCRIPTION;
    _this.Parent.TfrSDSLASelect.txtTitulo.Text = _this.SDCASETEMPLATE_SET.TEMPLATE_CODECFG.CASE_TITLE;
   // _this.Parent.TfrSDSLASelect.EnableControls = false;
    _this.Parent.TfrSDSLASelect.SetDisplayUrgencia(_this.SDCASETEMPLATE_SET.TEMPLATE_CODECFG.IDMDURGENCY);
    _this.Parent.TfrSDSLASelect.CheckisMayor.Checked = _this.SDCASETEMPLATE_SET.TEMPLATE_CODECFG.CASE_ISMAYOR;
    if (_this.SDCASETEMPLATE_SET.TEMPLATE_CODECFG.IDMDCATEGORYDETAIL > 0)
    {

        _this.Parent.TfrSDSLASelect.ReInitialize(_this.SDCASETEMPLATE_SET.TEMPLATE_CODECFG.IDMDCATEGORYDETAIL);
        //if (_this.Parent.TfrSDSLASelect.DataGridManager.DataGridSet.RecordCount > 0)
        //{
        //    for (var i = 0; i < _this.Parent.TfrSDSLASelect.DataGridManager.DataGridSet.RecordCount; i++)
        //    {
        //        if (_this.SDCASETEMPLATE_SET.TEMPLATE_CODECFG.IDSLA == parseInt(_this.Parent.TfrSDSLASelect.DataGridManager.DataGridSet.Records[i]["IDMDSLA"].toString()))
        //        {
        //            _this.Parent.TfrSDSLASelect.GridCategoria.SelectedIndex = i;
        //        }
        //    }
        //}

    }
            
    _this.Parent.TfrSDSLASelect.SetDisplayImpacto(_this.SDCASETEMPLATE_SET.TEMPLATE_CODECFG.IDMDIMPACT);
    _this.Parent.TfrSDSLASelect.SetDisplayPriority(_this.SDCASETEMPLATE_SET.TEMPLATE_CODECFG.IDMDPRIORITY);
    //_this.Parent.TfrSDSLASelect.EnableControls = true;
}

ItHelpCenter.SD.CaseManager.NewCase.frSDCaseNew_Template.prototype.btnImportCase_Click = function (sender, e)
{
    var _this = sender;
    _this.Set(_this.TSatus._Import);
}

ItHelpCenter.SD.CaseManager.NewCase.frSDCaseNew_Template.prototype.btnOk_Click = function (sender, e)
{
    var _this = sender;
    _this.Set(_this.TSatus._Acept);
}

ItHelpCenter.SD.CaseManager.NewCase.frSDCaseNew_Template.prototype.btnDelete_Click = function (sender, e)
{
    var _this = sender;
    _this.Set(_this.TSatus._Delete);
}

ItHelpCenter.SD.CaseManager.NewCase.frSDCaseNew_Template.prototype.btnCancel_Click = function (sender, e)
{
    var _this = sender;
    _this.Set(_this.TSatus._Cancel);
}

ItHelpCenter.SD.CaseManager.NewCase.frSDCaseNew_Template.prototype.GridTemplate_Fill = function (OpenDataSet)
{
    var _this = this.TParent();
 
    _this.DivGrid.innerHTML = "";
    var GridTemplate = new TGrid(_this.DivGrid, "", "");  
    GridTemplate.AutoTranslate = true;
    GridTemplate.DataSource = OpenDataSet;
    GridTemplate.Columns[2].Visible = false;
    GridTemplate.Columns[3].Visible = false;
    GridTemplate.Columns[6].Visible = false;
    GridTemplate.Columns[7].Visible = false;

    _this.GridTemplate_SelectedItem = _this.SDCASETEMPLATEList[0];

    GridTemplate.OnRowClick = function (sender, EventArgs) {
        
        _this.GridTemplate_SelectedItem = _this.SDCASETEMPLATEList[EventArgs.Index];
 
        if (_this.TSatus._Set == _this.LastSatus)
        {
            _this.Set(_this.TSatus._Set);
        }      
    }
}

ItHelpCenter.SD.CaseManager.NewCase.frSDCaseNew_Template.prototype.btnRefresh_Click = function (sender, e)
{           
    var _this = sender;
    _this.Set(_this.TSatus._Refresh);
}


ItHelpCenter.SD.CaseManager.NewCase.frSDCaseNew_Template.prototype.chkPublic_Click = function (sender, e)
{
    var _this = sender;

    if ((_this.TSatus._Set == _this.LastSatus) && (_this.EnableControls))
    {
        _this.Set(_this.TSatus._Update);
    }
}

ItHelpCenter.SD.CaseManager.NewCase.frSDCaseNew_Template.prototype.txtTitle_KeyDown = function (sender, e)
{
    var _this = sender;
    if ((_this.TSatus._Set == _this.LastSatus) && (_this.EnableControls))
    {
        _this.Set(_this.TSatus._Update);
    }

}
