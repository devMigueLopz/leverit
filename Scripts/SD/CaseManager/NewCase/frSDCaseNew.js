﻿
ItHelpCenter.SD.CaseManager.NewCase.frSDCMCaseNew = function (inMenuObject, inObject, incallback) {

    this.TParent = function () {
        return this;
    }.bind(this);
    this.isMenu = (inMenuObject != null);
    var _this = this.TParent();
    this.ObjectHtml = inObject;
    this.MenuObject = inMenuObject;
 
    this.CallbackLoad = incallback
    this.CallbackModalResult = null;
    this.DBTranslate = null; 
    this.Mythis = "frSDCMCaseNew";
    this.ID = this.Mythis + (Math.floor(Math.random() * 1000));
    this.TfrSDSLASelect = null;
                                                          
    this.IDSDCASETYPE = UsrCfg.SD.Properties.TIDSDCASETYPE.GetEnum(undefined);                              
    this.IDSDCASESTATUSLast = UsrCfg.SD.Properties.TSDCaseStatus.GetEnum(undefined);
    this.IDSDRESPONSETYPE = UsrCfg.SD.Properties.TSDRESPONSETYPE.GetEnum(undefined);

    this.IDSDCASE = 0;
    this.IDSDCASEEF = 0;
    this.IDMDCATEGORYDETAIL = 0;
    this.IDMDMODELTYPED = 0;
    this.IDCMDBUSER = 0;
    this.CI_GENERICNAME = "";
    this.IDSDTYPEUSER = UsrCfg.SD.Properties.TSDTypeUser._Owner;

    
    //***************************
    this.IDCONTACTTYPE = 0;  

    //TRADUCCION   
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lb_userSelect", "Select user:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lb_NotificationMetod", "Notification Method:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Z2_label1", "Template");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Z2_label2", "CI Affected");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Z2_label3", "Link File");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Z2_label4", "Link Case");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Z2_label5", "Cancel Case");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Z2_label6", "Save Draft and Exit");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Z2_label7", "Add Case");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Z2_label8", "Keep Resolving");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "MsgCancel", "You sure you want to cancel?");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines6", "The case:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines5", "has been cancelled");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines4", "Select an SLA");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines7", "Select a User");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines8", "Fill the description.");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines9", "Fill the title field.");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines11", "The case ID has not been loaded");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines12", "Select a category");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines13", "Select a contact method");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "labContactCmbMsg", "Add more notification methods");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "msgAddCaseTrue1", "The Case is created");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "msgAddCaseTrue2", "Correct");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "msgAddCaseFalse", "A failure to create the case is generated");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "labTitle", "Case number:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1", "Do you want to save changes before get out?");
       
    _this.LoadComponent();
} 

ItHelpCenter.SD.CaseManager.NewCase.frSDCMCaseNew.prototype.LoadComponent = function () {
    var _this = this.TParent(); 
    var PathCSS = ItHelpCenter.ScriptManager.GetStyle("SD/CaseManager/NewCase/frSDCaseNew.css");
    ItHelpCenter.ScriptManager.AddStyleFile(PathCSS, "frSDCMCaseNewCSS", function () {
        _this.importarScript(SysCfg.App.Properties.xRaiz + "Componet/AdvSearch/GridContol/frAdvSearchDataSet.js", function () {               
            _this.importarScript(SysCfg.App.Properties.xRaiz + "Scripts/SD/Shared/SLASelect/frSDSLASelect.js", function () {    
                _this.importarScript(SysCfg.App.Properties.xRaiz + "SD/Shared/Category/Basic/CategoryBasic.js", function () {                          
                    _this.InitializeComponent();
                }, function (e) {
                    //debugger;
                });
            }, function (e) {
                //debugger;
            });
        }, function (e) {
            //debugger;
        });        
    }, function (e) {
        //debugger;
    });    
}

ItHelpCenter.SD.CaseManager.NewCase.frSDCMCaseNew.prototype.InitializeComponent = function () {
    var _this = this.TParent();
    _this.ObjectHtml.innerHTML = "";


    this.MenuObject.OnBeforeBackPage = function (sender, e) {
        var DialogResult = confirm(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1"));
        if (DialogResult == true) {
            _this.UpdateCaseNoAsc();
        }
    };


    try {
        //ESTRUCTURA DIVS
        var Container = new TVclStackPanel(_this.ObjectHtml, "Container_" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
        var divCont = Container.Column[0].This;
        $(divCont).addClass("frSDCMCaseNewContainer");
 
        var DivsMain = new TVclStackPanel(divCont, "DivsMain" + _this.ID, 2, [[12, 12], [9, 3], [9, 3], [9, 3], [9, 3]]);
        var Div1C = DivsMain.Column[0].This;
        var Div2C = DivsMain.Column[1].This;

        var DivBotonera = _this.MenuObject.GetDivData(DivsMain.Column[1], DivsMain.Column[0]);
       
        var Div1Cont = new TVclStackPanel(Div1C, "Div1Cont" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
        var Div1 = Div1Cont.Column[0].This;       

        var Div2Cont = new TVclStackPanel(DivBotonera, "Div2Cont" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
        var Div2 = Div2Cont.Column[0].This;
        
        if (Source.Menu.IsMobil)
        { DivBotonera.classList.add("menuContenedorMobil"); }
        else
        {
            $(Div1Cont.Row.This).css("padding", "10px");
            $(Div1).addClass("StackPanelContainer");
            $(Div2Cont.Row.This).css("padding", "10px");
            $(Div2).addClass("StackPanelContainer");
        }

        var DivTtitle = new TVclStackPanel(Div1, "DivTtitle" + _this.ID, 1, [[12], [12], [12], [12], [12]]);        
        var DivT = DivTtitle.Column[0].This;
        $(DivT).css("border-bottom", "1px solid rgba(0, 0, 0, 0.08)");
        $(DivT).css("padding", "0px");
        $(DivTtitle.Row.This).css("padding-left", "0px");

        var Div1_1 = new TVclStackPanel(Div1, "Div1_1" + _this.ID, 3, [[4, 1, 7], [5, 2, 5], [5, 2, 5], [5, 2, 5], [5, 2, 5]]);
        var Div1_1C1 = Div1_1.Column[0].This;
        var Div1_1C2 = Div1_1.Column[1].This;
        var Div1_1C3 = Div1_1.Column[2].This;
        $(Div1_1.Row.This).css("padding", "0px 5px");
        $(Div1_1C1).css("padding", "0px");

        _this.Div1_2 = new TVclStackPanel(Div1, "Div1_2" + _this.ID, 3, [[4, 1, 7], [5, 2, 5], [5, 2, 5], [5, 2, 5], [5, 2, 5]]);
        var Div1_2C1 = _this.Div1_2.Column[0].This;
        var Div1_2C2 = _this.Div1_2.Column[1].This;
        var Div1_2C3 = _this.Div1_2.Column[2].This;
        $(_this.Div1_2.Row.This).css("display", "none");        
        $(Div1_2C1).css("padding", "0px");
        $(Div1_2C1).css("max-height", "40px");
        $(_this.Div1_2.Row.This).css("padding", "0px 5px");

        var DivsSLASelect = new TVclStackPanel(Div1, "DivsSLASelect_" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
        _this.DivsSLASelectC = DivsSLASelect.Column[0].This;
        $(DivsSLASelect.Row.This).css("padding-Top", "0px");
        $(DivsSLASelect.Row.This).css("padding-left", "5px");
        $(DivsSLASelect.Row.This).css("padding-right", "15px");
        $(DivsSLASelect.Row.This).css("padding-bottom", "0px"); 

        var Div2_1 = new TVclStackPanel(Div2, "Div2_1" + _this.ID, 2, [[8, 4], [8, 4], [8, 4], [8, 4], [9, 3]]);
        var Div2_1C1 = Div2_1.Column[0].This;
        var Div2_1C2 = Div2_1.Column[1].This;

        var Div2_2 = new TVclStackPanel(Div2, "Div2_2" + _this.ID, 2, [[8, 4], [8, 4], [8, 4], [8, 4], [9, 3]]);
        var Div2_2C1 = Div2_2.Column[0].This;
        var Div2_2C2 = Div2_2.Column[1].This;

        var Div2_3 = new TVclStackPanel(Div2, "Div2_3" + _this.ID, 2, [[8, 4], [8, 4], [8, 4], [8, 4], [9, 3]]);
        var Div2_3C1 = Div2_3.Column[0].This;
        var Div2_3C2 = Div2_3.Column[1].This;

        var Div2_4 = new TVclStackPanel(Div2, "Div2_4" + _this.ID, 2, [[8, 4], [8, 4], [8, 4], [8, 4], [9, 3]]);
        var Div2_4C1 = Div2_4.Column[0].This;
        var Div2_4C2 = Div2_4.Column[1].This;

        var Div2_5 = new TVclStackPanel(Div2, "Div2_5" + _this.ID, 2, [[8, 4], [8, 4], [8, 4], [8, 4], [9, 3]]);
        var Div2_5C1 = Div2_5.Column[0].This;
        var Div2_5C2 = Div2_5.Column[1].This;

        var Div2_6 = new TVclStackPanel(Div2, "Div2_6" + _this.ID, 2, [[8, 4], [8, 4], [8, 4], [8, 4], [9, 3]]);
        var Div2_6C1 = Div2_6.Column[0].This;
        var Div2_6C2 = Div2_6.Column[1].This;

        var Div2_7 = new TVclStackPanel(Div2, "Div2_7" + _this.ID, 2, [[8, 4], [8, 4], [8, 4], [8, 4], [9, 3]]);
        var Div2_7C1 = Div2_7.Column[0].This;
        var Div2_7C2 = Div2_7.Column[1].This;

        var Div2_8 = new TVclStackPanel(Div2, "Div2_8" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
        var Div2_8C1 = Div2_8.Column[0].This; 
       
        //ELEMENTOS 
        $(DivT).css("margin-left", "5px");
        _this.Title = new Vcllabel(DivT, "Title" + _this.ID, TVCLType.BS, TlabelType.H0, "");

        //**Select 
        var lblSelecionarUsuario = new Vcllabel(Div1_1C1, "lb_userSelect_" + _this.ID, TVCLType.BS, TlabelType.H0, "");
        lblSelecionarUsuario.innerText = UsrCfg.Traslate.GetLangText(_this.Mythis, "lb_userSelect");

        _this.btnSeleccionarUsuario = new TVclImagen(Div1_1C2, "imgSearch" + _this.ID);
        _this.btnSeleccionarUsuario.Src = "image/24/Search.png"
        _this.btnSeleccionarUsuario.Cursor = "pointer";
        _this.btnSeleccionarUsuario.onClick = function () {
            _this.btnSeleccionarUsuario_Click(_this, _this.btnSeleccionarUsuario);
        }

        _this.txtSeleccionarUsuario = new Vcllabel(Div1_1C3, "lb_userSelected" + _this.ID, TVCLType.BS, TlabelType.H0, "");
        _this.txtSeleccionarUsuario.innerText = "";

        var lblMetodoNotificaion = new Vcllabel(Div1_2C1, "lb_NotificationMetod_" + _this.ID, TVCLType.BS, TlabelType.H0, "");
        lblMetodoNotificaion.innerText = UsrCfg.Traslate.GetLangText(_this.Mythis, "lb_NotificationMetod");

        _this.labContactCmbMsg = new TVcllabel(Div1_2C1, "labContactCmbMsg" + _this.ID, TlabelType.H0);
        _this.labContactCmbMsg.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "labContactCmbMsg");
        _this.labContactCmbMsg.VCLType = TVCLType.BS;
        _this.labContactCmbMsg.This.style.color = "#B40404";
        $(_this.labContactCmbMsg.This).css("margin-left", "15px");
        $(_this.labContactCmbMsg.This).css("font-weight", "normal");
        $(_this.labContactCmbMsg.This).css("font-style", "oblique");

        _this.btnMetodoNotification = new TVclImagen(Div1_2C2, "imgNotificationMetod" + _this.ID);
        _this.btnMetodoNotification.Src = "image/24/Addressbook.png"
        _this.btnMetodoNotification.Cursor = "pointer";
        _this.btnMetodoNotification.onClick = function () {
            _this.btnMetodoNotification_Click(_this, _this.btnMetodoNotification);
        }

        _this.Div_cmbMetodoNotification = Div1_2C3;

        //zona Botonera
        
        var Z2_label1 = new Vcllabel(Div2_1C1, "Z2_label1_" + _this.ID, TVCLType.BS, TlabelType.H0, "");
        Z2_label1.innerText = UsrCfg.Traslate.GetLangText(_this.Mythis, "Z2_label1");
        Div2_1C1.style = "display: flex;justify-content: center;align-content: center;flex-direction: column;";
        $(Div2_1C1).addClass("LabelBoton");

        _this.btnFunction = new TVclButton(Div2_1C2, "btn_Template" + _this.ID);
        _this.btnFunction.VCLType = TVCLType.BS;
        _this.btnFunction.Src = "image/24/Import-export.png";
        _this.btnFunction.This.style.minWidth = "100%";
        _this.btnFunction.This.classList.add("btn-xs");
        _this.btnFunction.onClick = function myfunction() {
            _this.btnFunction_Click(_this, _this.btnFunction);
        }
        if (UsrCfg.Version.IsProduction) {
            $(Div2_1C1).css("display", "none");
            $(Div2_1C2).css("display", "none");
        }
        

        var Z2_label2 = new Vcllabel(Div2_2C1, "Z2_label2_" + _this.ID, TVCLType.BS, TlabelType.H0, "");
        Z2_label2.innerText = UsrCfg.Traslate.GetLangText(_this.Mythis, "Z2_label2");
        Div2_2C1.style = "display: flex;justify-content: center;align-content: center;flex-direction: column;";
        $(Div2_2C1).addClass("LabelBoton");

        _this.BtnCIAfectado = new TVclButton(Div2_2C2, "btn_CIAffected" + _this.ID);
        _this.BtnCIAfectado.VCLType = TVCLType.BS;
        _this.BtnCIAfectado.Src = "image/24/Inventory-maintenance.png";
        _this.BtnCIAfectado.This.style.minWidth = "100%";
        _this.BtnCIAfectado.This.classList.add("btn-xs");
        _this.BtnCIAfectado.onClick = function myfunction() {
            _this.BtnCIAfectado_Click(_this, _this.BtnCIAfectado);
        }
        

        var Z2_label3 = new Vcllabel(Div2_3C1, "Z2_label3_" + _this.ID, TVCLType.BS, TlabelType.H0, "");
        Z2_label3.innerText = UsrCfg.Traslate.GetLangText(_this.Mythis, "Z2_label3");
        Div2_3C1.style = "display: flex;justify-content: center;align-content: center;flex-direction: column;";
        $(Div2_3C1).addClass("LabelBoton");

        _this.btnLinkFile = new TVclButton(Div2_3C2, "btn_LinkFile" + _this.ID);
        _this.btnLinkFile.VCLType = TVCLType.BS;
        _this.btnLinkFile.Src = "image/24/Attachment.png";
        _this.btnLinkFile.This.style.minWidth = "100%";
        _this.btnLinkFile.This.classList.add("btn-xs");
        _this.btnLinkFile.onClick = function myfunction() {
            _this.btnLinkFile_Click(_this, _this.btnLinkFile);
        }

        if (!UsrCfg.Version.IsProduction) {
            var Z2_label4 = new Vcllabel(Div2_4C1, "Z2_label4_" + _this.ID, TVCLType.BS, TlabelType.H0, "");
            Z2_label4.innerText = UsrCfg.Traslate.GetLangText(_this.Mythis, "Z2_label4");
            Div2_4C1.style = "display: flex;justify-content: center;align-content: center;flex-direction: column;";
            $(Div2_4C1).addClass("LabelBoton");

            _this.BtnRelatedCase = new TVclButton(Div2_4C2, "btn_LinkCase" + _this.ID);
            _this.BtnRelatedCase.VCLType = TVCLType.BS;
            _this.BtnRelatedCase.Src = "image/24/Link.png";
            _this.BtnRelatedCase.This.style.minWidth = "100%";
            _this.BtnRelatedCase.This.classList.add("btn-xs");
            _this.BtnRelatedCase.onClick = function myfunction() {
                _this.BtnRelatedCase_Click(_this, _this.BtnRelatedCase);
            }
        }


        var Z2_label5 = new Vcllabel(Div2_5C1, "Z2_label5_" + _this.ID, TVCLType.BS, TlabelType.H0, "");
        Z2_label5.innerText = UsrCfg.Traslate.GetLangText(_this.Mythis, "Z2_label5");
        Div2_5C1.style = "display: flex;justify-content: center;align-content: center;flex-direction: column;";
        $(Div2_5C1).addClass("LabelBoton");

        _this.btnCancel = new TVclButton(Div2_5C2, "btn_CancelCase" + _this.ID);
        _this.btnCancel.VCLType = TVCLType.BS;
        _this.btnCancel.Src = "image/24/ticket-remove.png";
        _this.btnCancel.This.style.minWidth = "100%";
        _this.btnCancel.This.classList.add("btn-xs");
        _this.btnCancel.onClick = function myfunction() {
            _this.btnCancel_Click(_this, _this.btnCancel);
        }


        var Z2_label6 = new Vcllabel(Div2_6C1, "Z2_label6_" + _this.ID, TVCLType.BS, TlabelType.H0, "");
        Z2_label6.innerText = UsrCfg.Traslate.GetLangText(_this.Mythis, "Z2_label6");
        Div2_6C1.style = "display: flex;justify-content: center;align-content: center;flex-direction: column;";
        $(Div2_6C1).addClass("LabelBoton");

        _this.btnQuit = new TVclButton(Div2_6C2, "btn_SaveDraft" + _this.ID);
        _this.btnQuit.VCLType = TVCLType.BS;
        _this.btnQuit.Src = "image/24/Export.png";
        _this.btnQuit.This.style.minWidth = "100%";
        _this.btnQuit.This.classList.add("btn-xs");
        _this.btnQuit.onClick = function myfunction() {
            _this.btnQuit_Click(_this, _this.btnQuit);
        }

        var Z2_label7 = new Vcllabel(Div2_7C1, "Z2_label7_" + _this.ID, TVCLType.BS, TlabelType.H0, "");
        Z2_label7.innerText = UsrCfg.Traslate.GetLangText(_this.Mythis, "Z2_label7");
        Div2_7C1.style = "display: flex;justify-content: center;align-content: center;flex-direction: column;";
        $(Div2_7C1).addClass("LabelBoton");

        _this.btnAdd = new TVclButton(Div2_7C2, "btn_AddCase" + _this.ID);
        _this.btnAdd.VCLType = TVCLType.BS;
        _this.btnAdd.Src = "image/24/case-add.png";
        _this.btnAdd.This.style.minWidth = "100%";
        _this.btnAdd.This.classList.add("btn-xs");
        _this.btnAdd.onClick = function myfunction() {
            _this.btnAdd_Click(_this, _this.btnAdd);
        }

       
        _this.chkShowConsole = new TVclInputcheckbox(Div2_8C1, "Check_Z2_8_" + _this.ID);
        _this.chkShowConsole.VCLType = TVCLType.BS;
        _this.chkShowConsole.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Z2_label8");
        _this.chkShowConsole.onClick = function myfunction() {
            SysCfg.App.Methods.Cfg_WindowsTask.ContinueAtention = _this.chkShowConsole.Checked;
            SysCfg.App.Methods.SavebyIniIndexDb();
        }
        _this.chkShowConsole.Checked = SysCfg.App.Methods.Cfg_WindowsTask.ContinueAtention;
        _this.LoadSDSLASelect();
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("frSDCaseNew.js ItHelpCenter.SD.CaseManager.NewCase.frSDCMCaseNew.prototype.InitializeComponent", e);
       //debugger;
    }
     
    
    $(Div2).css("padding-left", "40px");
    $(Div2).css("padding-right", "40px");
    $(Div2_1.Row.This).css("padding-top", "5px");
    $(Div2_2.Row.This).css("padding-top", "5px");
    $(Div2_3.Row.This).css("padding-top", "5px");
    $(Div2_4.Row.This).css("padding-top", "5px");
    $(Div2_5.Row.This).css("padding-top", "5px");
    $(Div2_6.Row.This).css("padding-top", "5px");
    $(Div2_7.Row.This).css("padding-top", "5px");
    $(Div2_8.Row.This).css("padding-top", "5px");

    $(Div2_1.Row.This).css("border-top", "1px solid #c4c4c4");
    $(Div2_7.Row.This).css("margin-top", "5px");
    $(Div2_7.Row.This).css("padding-top", "10px");
    $(Div2_7.Row.This).css("border-top", "1px solid #c4c4c4");
    $(Div2_8C1).css("border-bottom", "1px solid #c4c4c4"); 
    
}

ItHelpCenter.SD.CaseManager.NewCase.frSDCMCaseNew.prototype.LoadSDSLASelect = function () {
    var _this = this.TParent(); 
    _this.TfrSDSLASelect = new ItHelpCenter.SD.Shared.SLASelect.TfrSDSLASelect(_this.DivsSLASelectC, function (_load, this_out) {
        if (_load) { //proceso al terminar el Initialize de frSDSLASelect
            _this.CallbackLoad();
        } else { //proceso cuando se optiene el ID de la categoria
            _this.IDMDCATEGORYDETAIL = this_out.IDMDCATEGORYDETAIL;
        }
    });
}


ItHelpCenter.SD.CaseManager.NewCase.frSDCMCaseNew.prototype.Initialize = function (inIDSDCASETYPE, inIDSDRESPONSETYPE, inIDMDCATEGORYDETAIL, inIDMDMODELTYPED, inCallback) {
    var _this = this.TParent();
    _this.CallbackModalResult = inCallback;

    _this.IDSDCASETYPE = inIDSDCASETYPE;
    _this.IDSDRESPONSETYPE = inIDSDRESPONSETYPE;

    switch (inIDSDCASETYPE) {
        case UsrCfg.SD.Properties.TIDSDCASETYPE._ACTIVITY_NEW:
        case UsrCfg.SD.Properties.TIDSDCASETYPE._NORMAL:
            _this.IDMDCATEGORYDETAIL = 0;
            _this.IDMDMODELTYPED = 0;
            break;
        case UsrCfg.SD.Properties.TIDSDCASETYPE._ACTIVITY_MODEL:
            _this.IDMDCATEGORYDETAIL = inIDMDCATEGORYDETAIL;
            _this.IDMDMODELTYPED = inIDMDMODELTYPED;
            break;
        default:
            break;
    }
    _this.TfrSDSLASelect.IDSDCASETYPE = inIDSDCASETYPE;
    _this.TfrSDSLASelect.IDMDCATEGORYDETAIL = _this.IDMDCATEGORYDETAIL;
    _this.TfrSDSLASelect.IDMDMODELTYPED = _this.IDMDMODELTYPED;
    _this.TfrSDSLASelect.IDSDRESPONSETYPE = inIDSDRESPONSETYPE;
    _this.TfrSDSLASelect.IDSDCASE = _this.IDSDCASE;
    _this.TfrSDSLASelect.IDSDCASEEF = _this.IDSDCASEEF;
    _this.CreateCase();

    //if ((UsrCfg.SD.Properties.TIDSDCASETYPE._ACTIVITY_MODEL == IDSDCASETYPE) || (IDSDRESPONSETYPE == UsrCfg.SD.Properties.TSDRESPONSETYPE._PROBLEMMANAGER)) {
    //    ZonaUsuario.Visibility = System.Windows.Visibility.Collapsed;
    //    ZonaNotificaion.Visibility = System.Windows.Visibility.Collapsed;
    //}
    //if (IDSDRESPONSETYPE == UsrCfg.SD.Properties.TSDRESPONSETYPE._PROBLEMMANAGER) {
    //    BtnCIAfectado.Visibility = System.Windows.Visibility.Collapsed;
    //}
}

ItHelpCenter.SD.CaseManager.NewCase.frSDCMCaseNew.prototype.InitializeComplexCase = function
    (inIDSDCASETYPE, inIDSDRESPONSETYPE, inIDMDCATEGORYDETAIL, inIDMDMODELTYPED, inSDSLASelect, inSDCASE_RELATION, inCallback) {
    var _this = this.TParent();
    try {
        _this.CallbackModalResult = inCallback;

        _this.IDSDCASETYPE = inIDSDCASETYPE;
        _this.IDSDRESPONSETYPE = inIDSDRESPONSETYPE;
        _this.IDMDCATEGORYDETAIL = inIDMDCATEGORYDETAIL;
        _this.IDMDMODELTYPED = inIDMDMODELTYPED;
 
        _this.TfrSDSLASelect.IDSDCASETYPE = inIDSDCASETYPE;
        _this.TfrSDSLASelect.IDMDCATEGORYDETAIL = inSDSLASelect.IDMDCATEGORYDETAIL;
        _this.TfrSDSLASelect.IDMDMODELTYPED = inSDSLASelect.IDMDMODELTYPED;
        _this.TfrSDSLASelect.IDSDRESPONSETYPE = inIDSDRESPONSETYPE;
        _this.TfrSDSLASelect.IDSDCASE = inSDSLASelect.IDSDCASE;
        _this.TfrSDSLASelect.IDSDCASEEF = _this.IDSDCASEEF;
        _this.TfrSDSLASelect.CheckisMayor.Checked = inSDSLASelect.CASE_ISMAYOR
        _this.TfrSDSLASelect.txtDescripcion.Text = inSDSLASelect.CASE_DESCRIPTION
        _this.TfrSDSLASelect.txtTitulo.Text = inSDSLASelect.CASE_TITLE
        _this.TfrSDSLASelect.IDIMPACT = inSDSLASelect.IDMDIMPACT;
        _this.TfrSDSLASelect.IDPRIORITY = inSDSLASelect.IDMDPRIORITY;
        _this.TfrSDSLASelect.IDURGENCY = inSDSLASelect.IDMDURGENCY;
        _this.TfrSDSLASelect.IDSLA = inSDSLASelect.IDSLA
        _this.TfrSDSLASelect.MT_IDMDMODELTYPED = 0;

        _this.TfrSDSLASelect.CargarDetalleWorkArrown();
        _this.TfrSDSLASelect.CargarDatosGridDetalle();

        try {
            var Param = new SysCfg.Stream.Properties.TParam();
            Param.Inicialize();
            Param.AddInt32(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.IDMDCATEGORYDETAIL.FieldName, _this.IDMDCATEGORYDETAIL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            var DS = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "SELECT_SLA_AUTO", Param.ToBytes());
            if (DS.ResErr.NotError) {
                if (DS.DataSet.RecordCount > 0) {
                    DS.DataSet.First();
                    if (!(DS.DataSet.Eof)) {
                        _this.TfrSDSLASelect.txtCategoria.innerText = DS.DataSet.RecordSet.FieldName("CATEGORY").asString();
                        _this.TfrSDSLASelect.TextDetails.innerText = DS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDCATEGORYDETAIL.CATEGORYNAME.FieldName).asString();
                    }
                }
                else {
                    DS.ResErr.NotError = false;
                    DS.ResErr.Mesaje = "RECORDCOUNT = 0";
                }
            }

        }
        finally {
            Param.Destroy();
        }

        _this.CreateCase();

        if (inSDCASE_RELATION.IDSDCASE > 0) {
            _this.BtnRelatedCase_Click(_this, null);
            _this.frSDRelatedCase.ADD_Relation(inSDCASE_RELATION);
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ItHelpCenter.SD.CaseManager.NewCase.frSDCMCaseNew.prototype.InitializeComplexCase ", e);
    }      
}

ItHelpCenter.SD.CaseManager.NewCase.frSDCMCaseNew.prototype.ReInitialize = function (SDConsoleAtentionStart, inCallback) {//cargar caso existente
    var _this = this.TParent();
    _this.CallbackModalResult = inCallback;

    if (SDConsoleAtentionStart.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE > 0) {
        //cargamos datos          
        //_this.LoadUserByID(SDConsoleAtentionStart.IDUSER);
        _this.IDCMDBUSER = SDConsoleAtentionStart.SDCONSOLEATENTION.SDCASECOMPLTE.IDUSER;
        _this.CI_GENERICNAME = SDConsoleAtentionStart.SDCONSOLEATENTION.SDCASECOMPLTE.USERNAME;
        _this.USERFULLNAME = _this.GETSDCASE_USER(SDConsoleAtentionStart.SDCONSOLEATENTION.SDCASECOMPLTE.IDUSER);
        _this.txtSeleccionarUsuario.innerText = "(" + _this.CI_GENERICNAME + ") " + _this.USERFULLNAME;
        if (_this.IDCMDBUSER > 0) {
            $(_this.Div1_2.Row.This).css("display", "block");
            try {
                _this.load_cmbMetodoNotification();
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("frSDCaseNew.js ItHelpCenter.SD.CaseManager.NewCase.frSDCMCaseNew.prototype.ReInitialize", e);
               //debugger;
            }            
        } else {
            $(_this.Div1_2.Row.This).css("display", "none");
        }

        _this.IDSDCASE = SDConsoleAtentionStart.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE;
        _this.IDSDCASEEF = SDConsoleAtentionStart.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASEEF;
        _this.IDMDCATEGORYDETAIL = SDConsoleAtentionStart.SDCONSOLEATENTION.SDCASECOMPLTE.IDMDCATEGORYDETAIL_INITIAL;

        _this.TfrSDSLASelect.IDSDCASE = _this.IDSDCASE;
        _this.TfrSDSLASelect.IDSDCASEEF = _this.IDSDCASEEF;
        _this.TfrSDSLASelect.IDMDCATEGORYDETAIL = _this.IDMDCATEGORYDETAIL;

        _this.Title.innerText = UsrCfg.Traslate.GetLangText(_this.Mythis, "labTitle") + " " + _this.IDSDCASE;

        _this.TfrSDSLASelect.LoadCategory();

        _this.IDCONTACTTYPE = SDConsoleAtentionStart.SDCONSOLEATENTION.SDCASECOMPLTE.IDCMDBCONTACTTYPE_USER

        _this.IDSDRESPONSETYPE = UsrCfg.SD.Properties.TSDRESPONSETYPE.GetEnum(SDConsoleAtentionStart.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDRESPONSETYPE);


        _this.IDSDCASESTATUSLast = UsrCfg.SD.Properties.TSDCaseStatus.GetEnum(SDConsoleAtentionStart.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASESTATUS);

        _this.IDSDCASETYPE = UsrCfg.SD.Properties.TIDSDCASETYPE._NORMAL;

        //**************
        if (SDConsoleAtentionStart.SDCONSOLEATENTION.SDCASEACTIVITIESCOMPLTE.IDSDCASEACTIVITIES > 0) {
            if (SDConsoleAtentionStart.SDCONSOLEATENTION.SDCASEACTIVITIESCOMPLTE.IDSDRUNNIGSOURCEMODEL == UsrCfg.SD.Properties.TSDRUNNIGSOURCEMODEL._OUTMODEL) {
                _this.IDSDCASETYPE = UsrCfg.SD.Properties.TIDSDCASETYPE._ACTIVITY_NEW;
            }
            else if (SDConsoleAtentionStart.SDCONSOLEATENTION.SDCASEACTIVITIESCOMPLTE.IDSDRUNNIGSOURCEMODEL == UsrCfg.SD.Properties.TSDRUNNIGSOURCEMODEL._INMODEL) {
                _this.IDSDCASETYPE = UsrCfg.SD.Properties.TIDSDCASETYPE._ACTIVITY_MODEL;
            }
        }
        else {
            _this.IDSDCASETYPE = UsrCfg.SD.Properties.TIDSDCASETYPE._NORMAL;
        }

        switch (_this.IDSDCASETYPE) {
            case UsrCfg.SD.Properties.TIDSDCASETYPE._ACTIVITY_NEW:
            case UsrCfg.SD.Properties.TIDSDCASETYPE._NORMAL:

                if (SDConsoleAtentionStart.SDCONSOLEATENTION.SDCASECOMPLTE.IDMDCATEGORYDETAIL_INITIAL > 0) {
                    _this.IDMDCATEGORYDETAIL = SDConsoleAtentionStart.SDCONSOLEATENTION.SDCASECOMPLTE.IDMDCATEGORYDETAIL_INITIAL;
                }
                else {
                    _this.IDMDCATEGORYDETAIL = 0;
                    _this.IDMDMODELTYPED = 0;
                }
                break;
            case UsrCfg.SD.Properties.TIDSDCASETYPE._ACTIVITY_MODEL:
                _this.IDMDCATEGORYDETAIL = SDConsoleAtentionStart.SDCONSOLEATENTION.SDCASEMTCOMPLTE.IDMDCATEGORYDETAIL;
                _this.IDMDMODELTYPED = SDConsoleAtentionStart.SDCONSOLEATENTION.SDCASEACTIVITIESCOMPLTE.IDMDMODELTYPED;
                break;
            default:
                break;
        }
        //************************

        _this.TfrSDSLASelect.IDSDCASETYPE = _this.IDSDCASETYPE;
        _this.TfrSDSLASelect.IDSDRESPONSETYPE = _this.IDSDRESPONSETYPE;
        _this.TfrSDSLASelect.IDMDCATEGORYDETAIL = _this.IDMDCATEGORYDETAIL;
        _this.TfrSDSLASelect.IDCMDBUSER = _this.IDCMDBUSER;        

        _this.TfrSDSLASelect.CheckisMayor.Checked = SDConsoleAtentionStart.SDCONSOLEATENTION.SDCASECOMPLTE.CASE_ISMAYOR;
        _this.TfrSDSLASelect.txtDescripcion.Text = SDConsoleAtentionStart.SDCONSOLEATENTION.SDCASECOMPLTE.CASE_DESCRIPTION;
        _this.TfrSDSLASelect.txtTitulo.Text = SDConsoleAtentionStart.SDCONSOLEATENTION.SDCASECOMPLTE.CASE_TITLE;


        _this.TfrSDSLASelect.IDIMPACT = 0;
        _this.TfrSDSLASelect.IDPRIORITY = 0;
        _this.TfrSDSLASelect.IDURGENCY = 0;
        _this.TfrSDSLASelect.IDSLA = SDConsoleAtentionStart.SDCONSOLEATENTION.SDCASEMTCOMPLTE.IDSLA;
        _this.TfrSDSLASelect.MT_IDMDMODELTYPED = SDConsoleAtentionStart.SDCONSOLEATENTION.SDCASEMTCOMPLTE.MT_IDMDMODELTYPED;

        _this.TfrSDSLASelect.CargarDetalleWorkArrown();
        _this.TfrSDSLASelect.CargarDatosGridDetalle();

    } else {
        _this.CallbackModalResult();
    }

}

ItHelpCenter.SD.CaseManager.NewCase.frSDCMCaseNew.prototype.GETSDCASE_USER = function(inIDUSER)
{
    
    var  ResErr = new SysCfg.Error.Properties.TResErr;
    var Res = "";
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE.IDUSER.FieldName, inIDUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    try
    {
        var DS_USER = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "GETAUTOCASE_USER", Param.ToBytes());
        ResErr = DS_USER.ResErr;
        if (ResErr.NotError)
        {
            if (DS_USER.DataSet.RecordCount > 0)
            {
                DS_USER.DataSet.First();
                if (!(DS_USER.DataSet.Eof))
                {
                    Res = DS_USER.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.SDCASE.IDUSER.FieldName).asString();
                }
            }
        }
    }
    finally
    {
        Param.Destroy();
    }
    return (Res);
}

ItHelpCenter.SD.CaseManager.NewCase.frSDCMCaseNew.prototype.CreateCase = function () {
    var _this = this.TParent();
   
    var dataArray = {
        "inIDSDRESPONSETYPE": _this.IDSDRESPONSETYPE.value
    }
    var DireccURL = SysCfg.App.Properties.xRaiz + 'SD/Shared/Contact/CMBDContact.svc/GetNewIDSDCASE';
    $.ajax({
        type: "POST",
        url: DireccURL,
        async: false,
        data: JSON.stringify(dataArray),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {            
            if (response.d.ResErr.NotError == true) {
                if (response.d.SDCASE.IDSDCASE > 0) {
                    try {
                        _this.IDSDCASE = response.d.SDCASE.IDSDCASE;
                        _this.IDSDCASEEF = response.d.SDCASEEF.IDSDCASEEF;
                        _this.TfrSDSLASelect.IDSDCASE = response.d.SDCASE.IDSDCASE;
                        _this.TfrSDSLASelect.IDSDCASEEF = response.d.SDCASEEF.IDSDCASEEF;
                        _this.IDSDCASESTATUSLast = UsrCfg.SD.Properties.TSDCaseStatus.GetEnum(response.d.SDCASE.IDSDCASESTATUS);
                        _this.Title.innerText = UsrCfg.Traslate.GetLangText(_this.Mythis, "labTitle") + " " + _this.IDSDCASE;
                    } catch (e) {
                        SysCfg.Log.Methods.WriteLog("frSDCaseNew.js ItHelpCenter.SD.CaseManager.NewCase.frSDCMCaseNew.prototype.CreateCase", e);
                    }                  
                }
            }

        },
        error: function (error) {
            alert('error: ' + error.txtDescripcion + " \n Location: Create a new Case Manager / Get IDS");
        }
    });
}

ItHelpCenter.SD.CaseManager.NewCase.frSDCMCaseNew.prototype.load_cmbMetodoNotification = function () {
    var _this = this.TParent();
 
    _this.Div_cmbMetodoNotification.innerHTML = "";
    _this.cmbMetodoNotification = new TVclComboBox2(_this.Div_cmbMetodoNotification, "CmbMethod_" + _this.ID);
    $(_this.cmbMetodoNotification.This).css('width', "100%");
    $(_this.cmbMetodoNotification.This).addClass("TextFormat");
    _this.cmbMetodoNotification.onChange = function () {
        _this.IDCONTACTTYPE = _this.cmbMetodoNotification.Value;
    }

    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.CMDBUSERCONTACTTYPE.IDCMDBUSER.FieldName, _this.IDCMDBUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var OpenDataSetContactType = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "LOAD_CMDBCONTACTTYPE_STYLE", Param.ToBytes());
        if (OpenDataSetContactType.DataSet.RecordCount > 0) {
            _this.labContactCmbMsg.Visible = (OpenDataSetContactType.DataSet.RecordCount == 0);

            //llenar combo
            var index = -1;
            for (var i = 0; i < OpenDataSetContactType.DataSet.RecordCount; i++) {
                var VclComboBoxItem = new TVclComboBoxItem();
                VclComboBoxItem.Value = OpenDataSetContactType.DataSet.Records[i].Fields[0].Value;
                VclComboBoxItem.Text = OpenDataSetContactType.DataSet.Records[i].Fields[1].Value + " ( " + OpenDataSetContactType.DataSet.Records[i].Fields[2].Value + " ) ";

                _this.cmbMetodoNotification.AddItem(VclComboBoxItem);

                if (OpenDataSetContactType.DataSet.Records[i].Fields[2].Value.trim() != "" && index == -1) {
                    index = i;
                }
            }

            if (index != -1) {
                _this.cmbMetodoNotification.selectedIndex = index;
                _this.IDCONTACTTYPE = OpenDataSetContactType.DataSet.Records[index].Fields[0].Value;
            }
        }
    } finally {
        Param.Destroy();
    }

}

ItHelpCenter.SD.CaseManager.NewCase.frSDCMCaseNew.prototype.UpdateCaseNoAsc = function () {
    var _this = this.TParent();

    var Exec = new SysCfg.DB.SQL.Properties.TExec();
    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();   
    try {       
        var CkIsMayor = _this.TfrSDSLASelect.CheckisMayor.Checked ? 1 : 0;
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE.IDUSER.FieldName, _this.IDCMDBUSER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE.IDSDCASE.FieldName, _this.IDSDCASE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE.CASE_ISMAYOR.FieldName, CkIsMayor, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoAtisNames.SDCASE.CASE_DESCRIPTION.FieldName, _this.TfrSDSLASelect.txtDescripcion.Text, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoAtisNames.SDCASE.CASE_TITLE.FieldName, _this.TfrSDSLASelect.txtTitulo.Text, SysCfg.DB.Properties.TMotor.OLE, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE.IDCMDBCONTACTTYPE_USER.FieldName, _this.IDCONTACTTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE.IDMDCATEGORYDETAIL_INITIAL.FieldName, _this.TfrSDSLASelect.IDMDCATEGORYDETAIL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoAtisNames.SDCASE.IDMDCATEGORYDETAIL_FINAL.FieldName, _this.TfrSDSLASelect.IDMDCATEGORYDETAIL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
       
        Exec =  SysCfg.DB.SQL.Methods.Exec("Atis","SDCASE_001", Param.ToBytes());
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("frSDCaseNew.js ItHelpCenter.SD.CaseManager.NewCase.frSDCMCaseNew.prototype.UpdateCaseNoAsc", e);
        //debugger;
    }
}

ItHelpCenter.SD.CaseManager.NewCase.frSDCMCaseNew.prototype.btnSeleccionarUsuario_Click = function (sender, e) {
    _this = sender;

    var Modal = new TVclModal(document.body, null, "IDModal");
    if (SysCfg.App.Properties.Device.value == SysCfg.App.TDevice.Desktop.value) {
        Modal.Width = "70%";
    } else {
        Modal.Width = "100%";
    }    
    Modal.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.Mythis, "lb_userSelect"));
    Modal.Body.This.id = "Modal_SearchDataSet_" + _this.ID;
    var SearchDataSet = new ItHelpCenter.SD.CaseManager.NewCase.frAdvSearchDataSet(Modal.Body.This, function (outRest) {

        _this.IDCMDBUSER = parseInt(outRest.Cells[0].Value);
        _this.CI_GENERICNAME = outRest.Cells[1].Value;
        _this.USERFULLNAME = outRest.Cells[2].Value + " " + outRest.Cells[3].Value + " " + outRest.Cells[4].Value;
        _this.txtSeleccionarUsuario.innerText = "(" + _this.CI_GENERICNAME + ") " + _this.USERFULLNAME;

        _this.TfrSDSLASelect.IDCMDBUSER = _this.IDCMDBUSER;

        var _thisChild = _this.TfrSDSLASelect.TParent();
        _this.TfrSDSLASelect.CheckTitle_Checked(_thisChild, null);
        _this.TfrSDSLASelect.CheckDescription_Checked(_thisChild, null);

        Modal.CloseModal();
        if (_this.IDCMDBUSER > 0) {
            $(_this.Div1_2.Row.This).css("display", "block");
            _this.load_cmbMetodoNotification();
        } else {
            $(_this.Div1_2.Row.This).css("display", "none");
        }
    });
    Modal.ShowModal();
    Modal.FunctionClose = function () {
    }


}

ItHelpCenter.SD.CaseManager.NewCase.frSDCMCaseNew.prototype.btnMetodoNotification_Click = function (sender, e) {
    var _this = sender;

    var Modal = new TVclModal(document.body, null, "");
    if (SysCfg.App.Properties.Device.value == SysCfg.App.TDevice.Desktop.value) {
        Modal.Width = "70%";
    } else {
        Modal.Width = "100%";
    }
    Modal.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.Mythis, "lb_NotificationMetod"));
    Modal.Body.This.id = "Modal_Contact_" + _this.ID;
    Modal.Tag = _this;

    var contact = new ItHelpCenter.SD.Shared.Contact.UfrCMDBContact(Modal.Body.This, _this.ID, function (sender) {
        Modal.CloseModal();
        _this.load_cmbMetodoNotification();
    }, _this.IDCMDBUSER, _this);

    Modal.ShowModal();
    Modal.FunctionClose = function () {
        _this.load_cmbMetodoNotification();
    }
}

ItHelpCenter.SD.CaseManager.NewCase.frSDCMCaseNew.prototype.btnFunction_Click = function (sender, e) {
    _this = sender;

    var Modal = new TVclModal(document.body, null, "");
    if (SysCfg.App.Properties.Device.value == SysCfg.App.TDevice.Desktop.value) {
        Modal.Width = "70%";
    } else {
        Modal.Width = "100%";
    }

    //Modal.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.Mythis, "Z2_label2"));
    //Modal.Body.This.id = "ModalTfrSDCMDBCI" + _this.ID;
    
    _this.frSDCaseNew_Template = new ItHelpCenter.SD.CaseManager.NewCase.frSDCaseNew_Template(_this.MenuObject, Modal.Body.This, _this, function (sender2) {
        debugger;        
    });    

    Modal.ShowModal();
    Modal.FunctionClose = function () {        
    }
}

ItHelpCenter.SD.CaseManager.NewCase.frSDCMCaseNew.prototype.BtnCIAfectado_Click = function (sender, e) {
    _this = sender;

    var Modal = new TVclModal(document.body, null, "");
    if (SysCfg.App.Properties.Device.value == SysCfg.App.TDevice.Desktop.value) {
        Modal.Width = "70%";
    } else {
        Modal.Width = "100%";
    }

    Modal.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.Mythis, "Z2_label2"));
    Modal.Body.This.id = "ModalTfrSDCMDBCI" + _this.ID;
    
    _this.TfrSDCMDBCI = new ItHelpCenter.SD.Shared.Case_CMDBCI.TfrSDCMDBCI(_this.MenuObject, Modal.Body.This, Modal, function (sender2) {
        var _this2 = sender2;
        _this2.Initialize(_this.IDSDCASE, _this.IDCMDBUSER, _this.IDSDTYPEUSER, function () {
            //debugger;
        });
    });
    

    Modal.ShowModal();
    Modal.FunctionClose = function () {        
    }
}

ItHelpCenter.SD.CaseManager.NewCase.frSDCMCaseNew.prototype.btnLinkFile_Click = function (sender, e) {
    _this = sender;

    try {
        var Modal = new TVclModal(document.body, null, "ModalLinkFile");
        if (SysCfg.App.Properties.Device.value == SysCfg.App.TDevice.Desktop.value) {
            Modal.Width = "70%";
        } else {
            Modal.Width = "100%";
        }

        Modal.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.Mythis, "Z2_label3"));
        Modal.Body.This.id = "ModalLinkFile" + _this.ID;
        _this.Attach = new ItHelpCenter.SD.Shared.Attached.UfrSDAttached(Modal.Body.This, "ModalLink" + _this.ID,
              function () { },
              _this.IDSDCASE,
              _this.IDCMDBUSER,
              _this.IDSDTYPEUSER.value,
              -1,
              false,
              new Array()
          );
        Modal.ShowModal();
        Modal.FunctionClose = function () {
           
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("frSDCaseNew.js ItHelpCenter.SD.CaseManager.NewCase.frSDCMCaseNew.prototype.btnLinkFile_Click", e);
        //debugger;
    }
}

ItHelpCenter.SD.CaseManager.NewCase.frSDCMCaseNew.prototype.BtnRelatedCase_Click = function (sender, e) {
    _this = sender;
    _this.frSDRelatedCase = null;

    try {
        _this.ModalRelatedCase = new TVclModal(document.body, null, "ModalLinkFile");
        if (SysCfg.App.Properties.Device.value == SysCfg.App.TDevice.Desktop.value) {
            _this.ModalRelatedCase.Width = "70%";
        } else {
            _this.ModalRelatedCase.Width = "100%";
        }

        _this.ModalRelatedCase.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.Mythis, "Z2_label4"));
        _this.ModalRelatedCase.Body.This.id = "BtnRelatedCase" + _this.ID;
        _this.frSDRelatedCase = new ItHelpCenter.SD.Shared.RelatedCase.frSDRelatedCase(null, _this.ModalRelatedCase.Body.This, "ModalLink" + _this.ID,
              function (myThis) {
                  _this.ModalRelatedCase.CloseModal();
              },
              _this.IDSDCASE, SysCfg.App.Methods.Cfg_WindowsTask.User.IDCMDBCI,
              UsrCfg.SD.Properties.TSDTypeUser._Owner.value, _this
          );
        _this.ModalRelatedCase.ShowModal();
        _this.ModalRelatedCase.FunctionClose = function () {

        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("frSDCaseNew.js ItHelpCenter.SD.CaseManager.NewCase.frSDCMCaseNew.prototype.BtnRelatedCase_Click", e);
    }

}

ItHelpCenter.SD.CaseManager.NewCase.frSDCMCaseNew.prototype.btnCancel_Click = function (sender, e) {
    _this = sender;

    var DialogResult = confirm(UsrCfg.Traslate.GetLangText(_this.Mythis, "MsgCancel"));
    if (DialogResult == true) {
        try {
            direc = SysCfg.App.Properties.xRaiz + "SD/CaseUser/NewCase/SDCaseNew.svc/btnCancel_Click";
            dataArray = {
                IDCMDBUSER: _this.IDCMDBUSER,
                chkEsMayor: _this.TfrSDSLASelect.CheckisMayor.Checked,
                txtDescripcion: _this.TfrSDSLASelect.txtDescripcion.Text,
                txtTitulo: _this.TfrSDSLASelect.txtTitulo.Text,
                IDSDCASE: _this.IDSDCASE,
                IDSDCASESTATUSLast: _this.IDSDCASESTATUSLast.value
            };

            $.ajax({
                async: false,
                type: "POST",
                url: direc,
                data: JSON.stringify(dataArray),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.d == 1) {
                        alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines6") + _this.IDSDCASE + " " + UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines5"));
                    }
                    else if (response.d == 2) {
                        //alert(UsrCfg.SD.Properties.ResultSDCaseChangeStatusIntToStr(SetSDCaseChangeStatus.ResultSDCaseChangeStatus));
                    }
                    else {
                        //alert(UsrCfg.SD.Properties.ResultSDCaseChangeStatusIntToStr(SetSDCaseChangeStatus.ResultSDCaseChangeStatus));
                    }
                },
                error: function (error) {
                    //enviar a la web
                    alert('error: ' + error.txtDescripcion + " \n Location: Category Seek Search, SearchID / newCategoryDetail");
                }
            });
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("frSDCaseNew.js ItHelpCenter.SD.CaseManager.NewCase.frSDCMCaseNew.prototype.btnCancel_Click", e);

        }
        //_this.CallbackModalResult();
        _this.MenuObject.BackPageOnlyParent();
    }
}

ItHelpCenter.SD.CaseManager.NewCase.frSDCMCaseNew.prototype.btnQuit_Click = function (sender, e) {
    _this = sender;

    _this.UpdateCaseNoAsc();
    //_this.CallbackModalResult();
    _this.MenuObject.BackPageOnlyParent();
}

ItHelpCenter.SD.CaseManager.NewCase.frSDCMCaseNew.prototype.btnAdd_Click = function (sender, e) {
    _this = sender;
    _this.UpdateCaseNoAsc();
    var ok = true;
    if (_this.TfrSDSLASelect.txtDescripcion.Text == "") {
        alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines8"));
        ok = false;
    }

    if ((_this.TfrSDSLASelect.txtTitulo.Text == "") && (ok==true)) {
        alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines9"));
        ok = false;
    }

    if ((_this.IDCMDBUSER < 1) && (ok == true)) {
        alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines7"));
        ok = false;
    }

    if ((_this.TfrSDSLASelect.IDSLA < 0) && (ok==true)) {  //1
        alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines4"));
        ok = false;
    }

    if ((_this.IDSDCASE < 1) && (ok==true)) {   
        alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines11"));
        ok = false;
    }

    if ((_this.IDMDCATEGORYDETAIL < 1) && (ok==true)) {
        alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines12"));
        ok = false;
    }

    if ((_this.IDCONTACTTYPE < 1) && (ok==true)) {
        alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines13"));
        ok = false;
    }

    if (ok) {
        try {
            direc = SysCfg.App.Properties.xRaiz + "SD/CaseUser/NewCase/SDCaseNew.svc/btnAdd_Click";
            dataArray = {
                IDCMDBUSER: _this.IDCMDBUSER,
                chkEsMayor: _this.TfrSDSLASelect.CheckisMayor.Checked,
                txtDescripcion: _this.TfrSDSLASelect.txtDescripcion.Text,
                txtTitulo: _this.TfrSDSLASelect.txtTitulo.Text,
                IDSDCASE: _this.IDSDCASE,
                IDSDCASESTATUSLast: _this.IDSDCASESTATUSLast.value,
                IDContactType: _this.IDCONTACTTYPE,
                IDMDCATEGORYDETAIL: _this.IDMDCATEGORYDETAIL,
                IDImpact: _this.TfrSDSLASelect.IDIMPACT,
                IDPriority: _this.TfrSDSLASelect.IDPRIORITY,
                IDUrgencia: _this.TfrSDSLASelect.IDURGENCY,
                IDSLA: _this.TfrSDSLASelect.IDSLA,
                MT_IDMDMODELTYPED: _this.TfrSDSLASelect.MT_IDMDMODELTYPED
            };

            $.ajax({
                async: false,
                type: "POST",
                url: direc,
                data: JSON.stringify(dataArray),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var SetSDCaseChangeStatus = response.d;
                    if (SetSDCaseChangeStatus.ResErr.NotError == true) {
                        if (SetSDCaseChangeStatus.ResultSDCaseChangeStatus == UsrCfg.SD.Properties.TResultSDCaseChangeStatus._CSSuccessfullyCreate.value) {
                            //if (_this.chkShowConsole.Checked) {
                            //    _this.OpenTicket(_this, UsrCfg.SD.Properties.TSDTypeUser._Owner, SetSDCaseChangeStatus.SDCASE.IDSDCASE, SetSDCaseChangeStatus.SDCASEMT.IDSDCASEMT);
                            //} else {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "msgAddCaseTrue1") + " " + _this.IDSDCASE + " " + UsrCfg.Traslate.GetLangText(_this.Mythis, "msgAddCaseTrue2"));
                            //}
                        }
                        else {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "msgAddCaseFalse") + " " + _this.IDSDCASE);
                        }
                    }
                    else {
                        alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "msgAddCaseFalse") + " " + _this.IDSDCASE);
                    }

                    _this.CallbackModalResult(_this,
                        UsrCfg.SD.Properties.TResultSDCaseChangeStatus.GetEnum(SetSDCaseChangeStatus.ResultSDCaseChangeStatus),
                        SetSDCaseChangeStatus.IDSDCASE,
                        SetSDCaseChangeStatus.IDSDCASEMT,
                        UsrCfg.SD.Properties.TSDTypeUser.GetEnum(SetSDCaseChangeStatus.IDSDTYPEUSER)
                     );


                    //_this.MenuObject.BackPageOnlyParent(_this);
                    
                    /*if (_this.isMenu) {
                        
                    }
                    else {
                        _this.CallbackModalResult(_this);
                    }*/

                },
                error: function (error) {
                    //enviar a la web
                    alert('error: ' + error.txtDescripcion + " \n Location: Category Seek Search, SearchID / newCategoryDetail");
                }
            });
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("frSDCaseNew.js ItHelpCenter.SD.CaseManager.NewCase.frSDCMCaseNew.prototype.btnAdd_Click", e);

        }
    }
}

ItHelpCenter.SD.CaseManager.NewCase.frSDCMCaseNew.prototype.OpenTicket = function (sender,inSDTypeUser,inIDSDCASE,inIDSDCASEMT) {
    _this = sender;
    
    SetSDConsoleAtentionStart = new UsrCfg.SD.TSetSDConsoleAtentionStart();
    SetSDConsoleAtentionStart.IDSDCASEMT = inIDSDCASEMT;
    SetSDConsoleAtentionStart.IDSDCASE = inIDSDCASE;
    SetSDConsoleAtentionStart.IDCMDBCI = SysCfg.App.Methods.Cfg_WindowsTask.User.IDCMDBCI;
    SetSDConsoleAtentionStart.IDSDTYPEUSER = inSDTypeUser;
    SetSDConsoleAtentionStart.IDSDWHOTOCASETYPE = UsrCfg.SD.Properties.TIDSDWHOTOCASETYPE._INTERNAL;
    SetSDConsoleAtentionStart = Atis.SD.Methods.SetSDConsoleAtentionStart(SetSDConsoleAtentionStart);
    if (SetSDConsoleAtentionStart.ResErr.NotError == true)
    {
        if (SetSDConsoleAtentionStart.SDConsoleAtentionStart.ResultSDConsoleAtentionStart == UsrCfg.SD.Properties.TResultSDConsoleAtentionStart._IncomeAccepted)
        {
            var NoExiste = true;
            objWindow = new Atis.TaskManager.TaskWindow;
          
            //for (var i = 0; i < UsrCfg.Properties.TaskWindowManager.taskWindows.Count; i++)
            //{
            //    objWindow = (Atis.TaskManager.TaskWindow)UsrCfg.Properties.TaskWindowManager.taskWindows[i];
            //    if (objWindow.Form is Atis.SD.frSDCaseAtention)
            //    {
            //        if ((((Atis.SD.frSDCaseAtention)objWindow.Form).SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE == SetSDConsoleAtentionStart.SDConsoleAtentionStart.SDCONSOLEATENTION.SDCASECOMPLTE.IDSDCASE) &&
            //        (((Atis.SD.frSDCaseAtention)objWindow.Form).SDWHOTOCASE.IDSDTYPEUSER == SetSDConsoleAtentionStart.SDConsoleAtentionStart.SDWHOTOCASE.IDSDTYPEUSER) &&
            //          (((Atis.SD.frSDCaseAtention)objWindow.Form).SDWHOTOCASE.IDSDCASEMT == SetSDConsoleAtentionStart.SDConsoleAtentionStart.SDWHOTOCASE.IDSDCASEMT)
            //        )
            //        {
            //            //NoExiste = false;//DifMAJS
            //            ((IFormManager)((Atis.SD.frSDCaseAtention)objWindow.Form))._frDestroy();
            //            break;
            //        }
            //    }
            //}
            //if (NoExiste)
            //{
            //    object form = null;
            //    double width = 0;
            //    double height = 0;
            //    form = new Atis.SD.frSDCaseAtention();

            //    ((Atis.SD.frSDCaseAtention)form).Initialize(ref SetSDConsoleAtentionStart.SDConsoleAtentionStart);
            //    ((Atis.SD.frSDCaseAtention)form).Background = new SolidColorBrush(Colors.White);
            //    width = ((Atis.SD.frSDCaseAtention)form).Width;
            //    height = ((Atis.SD.frSDCaseAtention)form).Height;
            //    objWindow = new TaskManager.TaskWindow();
            //    objWindow.Form = form;
            //    objWindow.WindowType = UsrCfg.Properties.TaskWindowManager.GetWindowType("frSDConsoleProblemManager");//DifMAJS
            //    objWindow.Status = TaskManager.TaskWindow.TWindowStatus.Minimized;
            //    objWindow.WindowColorType = TaskManager.TaskWindow.TWindowColorType.Alert;
            //    UsrCfg.Properties.TaskWindowManager.AddTaskWindow(objWindow);
            //    ((IFormManager)((Atis.SD.frSDCaseAtention)form))._frRefresh();
            //}
        }
    }
    else
    {
        ShowMessage.Message.ErrorMessage(SetSDConsoleAtentionStart.ResErr.Mesaje);
    }
}

ItHelpCenter.SD.CaseManager.NewCase.frSDCMCaseNew.prototype.importarScript = function (nombre, onSuccess, onError) {
    var s = document.createElement("script");
    s.onload = onSuccess;
    s.onerror = onError;
    s.src = nombre;
    document.querySelector("head").appendChild(s);
}
 




