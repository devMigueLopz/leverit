﻿ItHelpCenter.SD.CaseManager.NewCase.frSDCaseNew_Template.prototype.InitializeComponent = function () {
    var _this = this.TParent();

    //DIVS
    var div1 = new TVclStackPanel(_this.ObjectHtml, "div1", 5, [[2, 2, 2, 2, 2], [2, 2, 2, 2, 2], [2, 2, 2, 2, 2], [2, 2, 2, 2, 2], [2, 2, 2, 2, 2]]);
    _this.divbtnExportCase = div1.Column[0].This;
    _this.divbtnImportCase = div1.Column[1].This;
    _this.divbtnOk = div1.Column[2].This;
    _this.divbtnDelete = div1.Column[3].This;
    _this.divbtnCancel = div1.Column[4].This;
     

    var div2 = new TVclStackPanel(_this.ObjectHtml, "div2", 1, [[12], [12], [12], [12], [12]]);
    var div2Cont1 = div2.Column[0].This;

    var div3 = new TVclStackPanel(_this.ObjectHtml, "div3", 1, [[12], [12], [12], [12], [12]]);
    var div3Cont1 = div3.Column[0].This;

    var div4 = new TVclStackPanel(_this.ObjectHtml, "div4", 1, [[12], [12], [12], [12], [12]]);
    var div4Cont1 = div4.Column[0].This;

    var div5 = new TVclStackPanel(_this.ObjectHtml, "div5", 1, [[12], [12], [12], [12], [12]]);
    var div5Cont1 = div5.Column[0].This;

    var div6 = new TVclStackPanel(_this.ObjectHtml, "div6", 1, [[12], [12], [12], [12], [12]]);
    var div6Cont1 = div6.Column[0].This;

    var div7 = new TVclStackPanel(_this.ObjectHtml, "div7", 1, [[12], [12], [12], [12], [12]]);
    var div7Cont1 = div7.Column[0].This;

    var div8 = new TVclStackPanel(_this.ObjectHtml, "div8", 3, [[2, 5, 5], [2, 5, 5], [2, 5, 5], [2, 5, 5], [2, 5, 5]]);
    var div8Cont1 = div8.Column[0].This;
    var div8Cont2 = div8.Column[1].This;
    var div8Cont3 = div8.Column[2].This;

    var div9 = new TVclStackPanel(_this.ObjectHtml, "div9", 1, [[12], [12], [12], [12], [12]]);
    var div9Cont1 = div9.Column[0].This;
    _this.DivGrid = div9Cont1;

    //ELEMENTS
    _this.btnExportCase = new TVclButton(_this.divbtnExportCase, "btnExportCase");
    _this.btnExportCase.VCLType = TVCLType.BS;
    _this.btnExportCase.Src = "image/24/export.png";   
    _this.btnExportCase.Text = "Exportar";
    _this.btnExportCase.This.classList.add("btn-xs");
    _this.btnExportCase.onClick = function myfunction() {
        _this.btnExportCase_Click(_this, _this.btnExportCase);
    }
    $(_this.btnExportCase.This).css("width", "100%");

    _this.btnImportCase = new TVclButton(_this.divbtnImportCase, "btnImportCase");
    _this.btnImportCase.VCLType = TVCLType.BS;
    _this.btnImportCase.Src = "image/24/import.png";
    _this.btnImportCase.Text = "import";
    _this.btnImportCase.This.classList.add("btn-xs");
    _this.btnImportCase.onClick = function myfunction() {
        _this.btnImportCase_Click(_this, _this.btnImportCase);
    }
    $(_this.btnImportCase.This).css("width", "100%");

    _this.btnOk = new TVclButton(_this.divbtnOk, "btnOk");
    _this.btnOk.VCLType = TVCLType.BS;
    _this.btnOk.Src = "image/24/success.png";
    _this.btnOk.Text = "OK";
    _this.btnOk.This.classList.add("btn-xs");
    _this.btnOk.onClick = function myfunction() {
        _this.btnOk_Click(_this, _this.btnOk);
    }
    $(_this.btnOk.This).css("width", "100%");

    _this.btnDelete = new TVclButton(_this.divbtnDelete, "btnDelete");
    _this.btnDelete.VCLType = TVCLType.BS;
    _this.btnDelete.Src = "image/24/Trash-can.png";
    _this.btnDelete.Text = "Delete";
    _this.btnDelete.This.classList.add("btn-xs");
    _this.btnDelete.onClick = function myfunction() {
        _this.btnDelete_Click(_this, _this.btnDelete);
    }
    $(_this.btnDelete.This).css("width", "100%");
     
    _this.btnCancel = new TVclButton(_this.divbtnCancel, "btnCancel");
    _this.btnCancel.VCLType = TVCLType.BS;
    _this.btnCancel.Src = "image/24/delete.png";
    _this.btnCancel.Text = "Cancel";
    _this.btnCancel.This.classList.add("btn-xs");
    _this.btnCancel.onClick = function myfunction() {
        _this.btnCancel_Click(_this, _this.btnCancel);
    }
    $(_this.btnCancel.This).css("width", "100%");

 
    this.lblTitle = new TVcllabel(div2Cont1, "lblTitle", TlabelType.H0);
    this.lblTitle.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblTitle.Text");
    this.lblTitle.VCLType = TVCLType.BS;

    _this.txtTitle = new TVclTextBox(div3Cont1, "txtTitle");
    $(_this.txtTitle.This).css('width', "100%"); 
    $(_this.txtTitle.This).addClass("TextFormat");
    _this.txtTitle.This.onkeypress = function validar(e) {
        _this.txtTitle_KeyDown(_this, _this.txtTitle);
    }

    this.lblDescription = new TVcllabel(div4Cont1, "lblDescription", TlabelType.H0);
    this.lblDescription.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblDescription.Text");
    this.lblDescription.VCLType = TVCLType.BS;

    _this.txtDescription = new TVclMemo(div5Cont1, "txtDescription");
    $(_this.txtDescription.This).css('width', "100%");
    $(_this.txtDescription.This).css('height', "60px");
    $(_this.txtDescription.This).addClass("TextFormat");
    _this.txtDescription.This.onkeypress = function validar(e) {
        _this.txtTitle_KeyDown(_this, _this.txtDescription);
    }

    _this.cbxPublic = new TVclInputcheckbox(div6Cont1, "cbxPublic");
    _this.cbxPublic.VCLType = TVCLType.BS;
    _this.cbxPublic.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "cbxPublic.Text");
    _this.cbxPublic.onClick = function myfunction() {
        //_this.cbxPublic_Click(_this, _this.cbxPublic);
    }
     
    _this.cbxEnable = new TVclInputcheckbox(div7Cont1, "cbxEnable");
    _this.cbxEnable.VCLType = TVCLType.BS;
    _this.cbxEnable.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "cbxEnable.Text");
    _this.cbxEnable.onClick = function myfunction() {
        //_this.cbxEnable_Click(_this, _this.cbxEnable);
    }

    _this.btnRefresh = new TVclImagen(div8Cont1, "refresh");
    _this.btnRefresh.Src = "image/24/refresh.png"
    _this.btnRefresh.Cursor = "pointer";
    _this.btnRefresh.onClick = function () {
        _this.btnRefresh_Click(_this, _this.btnRefresh);
    }

    _this.chkPublic = new TVclInputcheckbox(div8Cont2, "chkPublic");
    _this.chkPublic.VCLType = TVCLType.BS;
    _this.chkPublic.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "chkPublic.Text");
    _this.chkPublic.onClick = function myfunction() {
        _this.chkPublic_Click(_this, _this.chkPublic_Click);
    }

    this.txtStatus = new TVcllabel(div8Cont3, "txtStatus", TlabelType.H0);
    this.txtStatus.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "txtStatus.Text");
    this.txtStatus.VCLType = TVCLType.BS;
     
    _this.Initialize();
}