﻿
UsrCfg.EF.Properties = {
    DATE_SYSLOG: SysCfg.DB.Properties.SVRNOW(),
    IDCMDBCI_SYSLOG: SysCfg.App.Methods.Cfg_WindowsTask.User.IDCMDBCI,
    TSOURCE_SYSLOG: {
        UNKNOWN: 1,
        IDATROLE: 2,
        IDTYPEUSER: 3
    },
    TEVENT_SYSLOG: {
        UNKNOWN: 1,
        INSERT: 2,
        UPDATE: 3,
        DELETE: 4
    }
}
