﻿
ItHelpCenter.TIndex = function (inObject, incallback) {

    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.ObjectHtml = inObject;
    this.Callback = incallback;
    this.version =UsrCfg.Version._UsrCfgAppVersion;
    this.idLenguaje = 1;

    var _this = this.TParent();
    this.Mythis = "index";
    //*******************************************************
    arrayleng = [["IndexInfo", "Server connecting", "Conectando al Servidor"]];
    //var _DBLocalLeng = new SysCfg.ini.DBLocalLeng('LocalLeng', 1);
    //_DBLocalLeng.Open(function (e) {
    //    _DBLocalLeng.Read(_this.idLenguaje, function (e) {            
    //        _this.idLenguaje = e.target.result.IDleng;
    //        _this.Load();
    //    }, function (e) {
    //        alert(" Can´t Read LocalIndexDB. "+ _this.Mythis);
    //    });
    //}, function (e) {
    //    alert(" Can´t Load LocalIndexDB. " + _this.Mythis);
    //});

    var _DBLocalLeng = new SysCfg.ini.LSLocalLeng();
    _this.idLenguaje = _DBLocalLeng.Read(_this.idLenguaje,false);
    _this.Load();
}

//ItHelpCenter.TPageManager = {
//    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
//    _Mdb: { value: 0, name: "" },
//    _AtentionCasesPending: { value: 1, name: "SD/CaseUser/Atention/AtentionCasesPending.js" },
//    _Orc: { value: 2, name: "orc" }
//}


ItHelpCenter.TIndex.prototype.Load = function () {
    var _this = this.TParent();
    var ObjHtml = _this.ObjectHtml;
    ObjHtml.innerHTML = "";

    //ESTRUCTURA DE ESTILOS==============================================================================================
    //Divs-------------------------------------------------------------------------------------------
    var Container = document.createElement("div");
    Container.id = "IndexContainer";
    ObjHtml.appendChild(Container);

    //div1 BANDERA
    var div1 = document.createElement("div");
    div1.id = "IndexDiv1";
    Container.appendChild(div1);

    //div2 LOGO
    var div2 = document.createElement("div");
    div2.id = "IndexDiv2";
    Container.appendChild(div2);

    //div3 Label DATALINK
    var div3 = document.createElement("div");
    div3.id = "IndexDiv3";
    Container.appendChild(div3);

    //div4 Label INFO
    var div4 = document.createElement("div");
    div4.id = "IndexDiv4";
    Container.appendChild(div4);

    //div5 Label ICONS
    var div5 = document.createElement("div");
    div5.id = "IndexDiv5";
    Container.appendChild(div5);

    //ELEMENTOS------------------------------------------------------------------------------------
    //--IMG Banderita
    var bandera = document.createElement("img");
    if (_this.idLenguaje == 1) {
        bandera.setAttribute("src", "image/24/United-states-flag.png");
    } else {
        bandera.setAttribute("src", "image/24/Spain-flag.png");
    }
    bandera.id = "indexFlag";  
    bandera.style.cursor = "Pointer";
    bandera.addEventListener('click', function (event) {
        if (_this.idLenguaje == 1) {
            _this.idLenguaje = 2;
            bandera.setAttribute("src", "image/24/Spain-flag.png");
        } else {
            _this.idLenguaje = 1;
            bandera.setAttribute("src", "image/24/United-states-flag.png");
        }
        _this.SaveLocalLanguage();
        Info.innerText = arrayleng[0][_this.idLenguaje];
    });
    div1.appendChild(bandera);

    //--IMG Logo
    var Logo = document.createElement("img");
    Logo.setAttribute("src", "image/High/IT-Help-Center-Final file.png");
    Logo.id = "IndexLogo";
    div2.appendChild(Logo);

    //--Label Title    
    var Title = document.createElement("Label");
    Title.innerText = "DATA LINK";
    Title.id = "IndexTitle";
    div3.appendChild(Title);

    //--Label Info   
    var Info = document.createElement("Label");
    Info.innerText = arrayleng[0][_this.idLenguaje];
    Info.id = "IndexInfo";
    div4.appendChild(Info);

    //IMG Icon 1
    var Icon1 = document.createElement("img");
    Icon1.id = "Indexicon1";
    Icon1.setAttribute("src", "image/48/connection.png");
    Icon1.style.cursor = "Pointer"; 
    Icon1.addEventListener('click', function (event) {
        _this.Callback(ItHelpCenter.PageManager.TGoToPageType.Reload);
    });
    div5.appendChild(Icon1);

    //IMG Icon 2
    var Icon2 = document.createElement("img");
    Icon2.id = "Indexicon2";
    Icon2.setAttribute("src", "image/48/setting.png");
    Icon2.style.cursor = "Pointer";
    Icon2.addEventListener('click', function (event) {
        _this.Callback(ItHelpCenter.PageManager.TGoToPageType.LoginConfig);
    });       
    div5.appendChild(Icon2);
    

   
    var GetPing = Comunic.AjaxJson.Client.Methods.GetPing();
    if (GetPing.ResErr.NotError) {
        if (GetPing.Ping) {
            SysCfg.App.Properties.DataDomain.isValidDomain = GetPing.isValidDomain;
            if (SysCfg.App.Properties.DataDomain.isValidDomain) {
                var GetDataDomain = Comunic.AjaxJson.Client.Methods.GetDataDomain();
                SysCfg.App.Properties.DataDomain.DomainUser = GetDataDomain.DomainUser;

            }



            SysCfg.License.Client.Methods.loadLicense();
            if (SysCfg.License.Properties.SerialValido(true)) {
                _this.Callback(ItHelpCenter.PageManager.TGoToPageType.Login);
            }
        }
        else {
            alert("Index:" + GetPing.ResErr.Mesaje);
        }
    }
    else {
        alert("Index:" + GetPing.ResErr.Mesaje);
    }

}

ItHelpCenter.TIndex.prototype.SaveLocalLanguage = function () {
    var _this = this.TParent();
    //var _DBLocalLeng = new SysCfg.ini.DBLocalLeng('LocalLeng', 1);
    //_DBLocalLeng.Open(function (e) {
    //    _DBLocalLeng.Write(_this.idLenguaje, function (e) {
    //    }, function (e) {
    //        alert("Can´t write LocalIndexDB. " + _this.Mythis);
    //    });
    //}, function (e) {
    //    alert("Can´t load LocalIndexDB. " + _this.Mythis);
    //});
    var _DBLocalLeng = new SysCfg.ini.LSLocalLeng();
    _this.idLenguaje = _DBLocalLeng.Write(_this.idLenguaje);
}

