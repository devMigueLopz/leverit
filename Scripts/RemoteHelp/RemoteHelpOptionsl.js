﻿
ItHelpCenter.RemoteHelp.TRHREQUESTOPTIONS = function () {

    this.ConnectbyHost = false;
    this.InternetConnection = false;
    this.RemoteHelpFuntion = Persistence.RemoteHelp.Properties.TRemoteHelpFuntion._None;
    this.JustSee = false;
    this.LockKeyboard = false;
    this.DonotAskforPermission = false;
    this.LowBandwidth = false;
    this.RecordSession = false;
    this.IDRHMODULE_EXECUTE = 0;
    this.IDRHTYPEEXECUTE = 0;
    this.CPUList = new Array();
}

