﻿ItHelpCenter.MD.Configuration.PriorityMatrix.TfrPriorityMatrix = function (inObjectHtml, _this) {

    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    if (SysCfg.App.Properties.Device == SysCfg.App.TDevice.Desktop) {
        var stackPanel = new TVclStackPanel(inObjectHtml, "1", 1, [[12], [12], [12], [12], [12]]);
        stackPanel.Row.This.style.margin = "0px 14px";
        stackPanel.Column[0].This.style.padding = "20px 20px";
        stackPanel.Column[0].This.style.border = "1px solid rgba(0, 0, 0, 0.08)";
        stackPanel.Column[0].This.style.boxShadow = " 0 0px 8px 0 rgba(0, 0, 0, 0.06), 0 1px 0px 0 rgba(0, 0, 0, 0.02);";
        stackPanel.Column[0].This.style.marginTop = "15px";
        stackPanel.Column[0].This.style.backgroundColor = "#FAFAFA";
        this.stackPanelBody = new TVclStackPanel(stackPanel.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);

        this.ObjectHtml = this.stackPanelBody.Column[0].This;
    } else {
        this.ObjectHtml = new Udiv(null, "");
        this.ObjectHtml.style.backgroundColor = "#FAFAFA";
        inObjectHtml.appendChild(this.ObjectHtml);
    }


    this.GridCFView = null;
    this.MDPRIORITYMATRIXList = new Array();

    this.MDURGENCYList = new Array();
    this.MDPRIORITYList = new Array();
    this.MDIMPACTList = new Array();

    this.DS_URGENCY;
    this.DS_PRIORITY;
    this.DS_IMPACT;



    
    this.Mythis = "TfrPriorityMatrix";    
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1", "You want to add the record");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2", "You want to update the record");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3", "You want to delete the record");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines4", "You are sure to change the record");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines5", "The record was added");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines6", "The record was updated");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines7", "Delete Record");
    this.InitializeComponent();
    this.OpenQuery();
    
}
ItHelpCenter.MD.Configuration.PriorityMatrix.TfrPriorityMatrix.prototype.InitializeComponent = function () {
    var _this = this.TParent();
    _this.GridCFView = new Componet.GridCF.TGridCFView(_this.ObjectHtml, function (OutRes) { }, null);
    _this.GridCFView.OnBeforeChange = function (DataSet, RecordOld, RecordNew, Status, isCancel) {//RecorOLD,RecordNew,Status,Cancel
        //
        var message = "";
        if (Status == SysCfg.MemTable.Properties.TStatus.Apped) {
            message = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1");
        }
        else if (Status == SysCfg.MemTable.Properties.TStatus.Update) {
            message = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2");
        }
        else if (Status == SysCfg.MemTable.Properties.TStatus.Delete) {
            message = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3");
        }
        isCancel.Value = !confirm(message + " \n " + UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines4"));
        //
        if (!isCancel.Value) {
            if (Status == SysCfg.MemTable.Properties.TStatus.Apped) {
                isCancel.Value = !_this.AddRecord(RecordNew);
            }
            else if (Status == SysCfg.MemTable.Properties.TStatus.Update) {
                isCancel.Value = !_this.UpdateRecord(RecordOld, RecordNew);
            }
            else if (Status == SysCfg.MemTable.Properties.TStatus.Delete) {
                isCancel.Value = !_this.DeleteRecord(RecordNew);
            }
        }
    }
    _this.GridCFView.OnAfterChange = function (GridCFView, RecordOld, RecordNew) {

        //
        var message = "";

        if (GridCFView.DataSet.Status == SysCfg.MemTable.Properties.TStatus.Apped) {
            message = message + UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines5");
        }
        else if (GridCFView.DataSet.Status == SysCfg.MemTable.Properties.TStatus.Update) {
            message = message + UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines6");
        }
        else if (GridCFView.DataSet.Status == SysCfg.MemTable.Properties.TStatus.Delete) {
            message = message + UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines7");
        }
        //
        alert(message);

    }

    

    _this.DS_URGENCY = Persistence.MD.Methods.MDURGENCY_Fill(_this.MDURGENCYList, "");
    _this.DS_PRIORITY = Persistence.MD.Methods.MDPRIORITY_Fill(_this.MDPRIORITYList, "");
    _this.DS_IMPACT = Persistence.MD.Methods.MDIMPACT_Fill(_this.MDIMPACTList, "");


    var col1 = new TVclColumnCF();
    col1.Name = UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName;
    col1.Caption = UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName;
    col1.DataType = SysCfg.DB.Properties.TDataType.String;//Int32
    col1.ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.LookUp;
    col1.onEventControl = function (inTVclComboBox2) {

        try {
            _this.ResErr = _this.DS_PRIORITY.ResErr;
            if (_this.ResErr.NotError) {
                if (_this.DS_PRIORITY.DataSet.RecordCount > 0) {
                    _this.DS_PRIORITY.DataSet.First();
                    while (!(_this.DS_PRIORITY.DataSet.Eof)) {

                        var VclComboBoxItem1 = new TVclComboBoxItem();
                        VclComboBoxItem1.Value = _this.DS_PRIORITY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName).asInt32();
                        VclComboBoxItem1.Text = _this.DS_PRIORITY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDPRIORITY.PRIORITYNAME.FieldName).asString();
                        VclComboBoxItem1.Tag = _this.DS_PRIORITY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName).asInt32();
                        inTVclComboBox2.AddItem(VclComboBoxItem1);
                        _this.DS_PRIORITY.DataSet.Next();
                    }
                }
                else {
                    _this.DS_PRIORITY.ResErr.NotError = false;
                    _this.DS_PRIORITY.ResErr.Mesaje = "RECORDCOUNT = 0";
                }
            }
        }
        finally {
        }
    }
    var col2 = new TVclColumnCF();
    col2.Name = UsrCfg.InternoAtisNames.MDURGENCY.IDMDURGENCY.FieldName;
    col2.Caption = UsrCfg.InternoAtisNames.MDURGENCY.IDMDURGENCY.FieldName;
    col2.DataType = SysCfg.DB.Properties.TDataType.String;//Int32
    col2.ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.LookUp;
    col2.onEventControl = function (inTVclComboBox2) {

        try {
            _this.ResErr = _this.DS_URGENCY.ResErr;
            if (_this.ResErr.NotError) {
                if (_this.DS_URGENCY.DataSet.RecordCount > 0) {
                    _this.DS_URGENCY.DataSet.First();
                    while (!(_this.DS_URGENCY.DataSet.Eof)) {

                        var VclComboBoxItem1 = new TVclComboBoxItem();
                        VclComboBoxItem1.Value = _this.DS_URGENCY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDURGENCY.IDMDURGENCY.FieldName).asInt32();
                        VclComboBoxItem1.Text = _this.DS_URGENCY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDURGENCY.URGENCYNAME.FieldName).asString();
                        VclComboBoxItem1.Tag = _this.DS_URGENCY.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDURGENCY.IDMDURGENCY.FieldName).asInt32();
                        inTVclComboBox2.AddItem(VclComboBoxItem1);
                        _this.DS_URGENCY.DataSet.Next();
                    }
                }
                else {
                    _this.DS_URGENCY.ResErr.NotError = false;
                    _this.DS_URGENCY.ResErr.Mesaje = "RECORDCOUNT = 0";
                }
            }
        }
        finally {
        }
    }

    var col3 = new TVclColumnCF();
    col3.Name = UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.FieldName;
    col3.Caption = UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.FieldName;
    col3.DataType = SysCfg.DB.Properties.TDataType.String;//Int32
    col3.ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.LookUp;
    col3.onEventControl = function (inTVclComboBox2) {

        try {
            _this.ResErr = _this.DS_IMPACT.ResErr;
            if (_this.ResErr.NotError) {
                if (_this.DS_IMPACT.DataSet.RecordCount > 0) {
                    _this.DS_IMPACT.DataSet.First();
                    while (!(_this.DS_IMPACT.DataSet.Eof)) {

                        var VclComboBoxItem1 = new TVclComboBoxItem();
                        VclComboBoxItem1.Value = _this.DS_IMPACT.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.FieldName).asInt32();
                        VclComboBoxItem1.Text = _this.DS_IMPACT.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDIMPACT.IMPACTNAME.FieldName).asString();
                        VclComboBoxItem1.Tag = _this.DS_IMPACT.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDIMPACT.IDMDIMPACT.FieldName).asInt32();
                        inTVclComboBox2.AddItem(VclComboBoxItem1);
                        _this.DS_IMPACT.DataSet.Next();
                    }
                }
                else {
                    _this.DS_IMPACT.ResErr.NotError = false;
                    _this.DS_IMPACT.ResErr.Mesaje = "RECORDCOUNT = 0";
                }
            }
        }
        finally {
        }
    }
    _this.GridCFView.VclGridCF.AutoGenerateColumns = false;
    _this.GridCFView.AddColumnCF(col1);
    _this.GridCFView.AddColumnCF(col2);
    _this.GridCFView.AddColumnCF(col3);

}
ItHelpCenter.MD.Configuration.PriorityMatrix.TfrPriorityMatrix.prototype.OpenQuery = function () {
    var _this = this.TParent();

    var Open = Persistence.MD.Methods.MDPRIORITYMATRIX_Fill(_this.MDPRIORITYMATRIXList, "");

    if (Open.ResErr.NotError) {
        
        _this.GridCFView.VclGridCF.AutoGenerateColumns = true;
        _this.GridCFView.VclGridCF.DataSource = Open.DataSet;
        
        var VTCfgFactoryConfig = new Componet.GridCF.TGridCFView.TCfg();
        VTCfgFactoryConfig.ToolBar.ButtonImg_DeleteBar.Active = true;
        VTCfgFactoryConfig.ToolBar.ButtonImg_EditBar.Active = true;
        VTCfgFactoryConfig.ToolBar.ButtonImg_AddBar.Active = true;
        VTCfgFactoryConfig.ColumnsOptionsDef.InsertActiveEditor = true;
        VTCfgFactoryConfig.ColumnsOptionsDef.UpdateActiveEditor = true;
        VTCfgFactoryConfig.ToolBar.ButtonImg_EditToolsBar.Visible = true;
        _this.GridCFView.GridCFToolbar.NavTabControls.BackgroundHeader = "#2B569A";
        VTCfgFactoryConfig.ToolBar.ButtonImg_EditToolsBar.ChekedClickDown = true;
        _this.GridCFView.LoadTCfg(VTCfgFactoryConfig);
        _this.GridCFView.AutoTranslete = true;
    };
}
ItHelpCenter.MD.Configuration.PriorityMatrix.TfrPriorityMatrix.prototype.AddStyleFile = function (nombre, onSuccess, onError) {
    var style = document.createElement("link");
    style.rel = "stylesheet";
    style.type = "text/css";
    style.href = nombre;
    var s = document.head.appendChild(style);
    s.onload = onSuccess;
    s.onerror = onError;
}
ItHelpCenter.MD.Configuration.PriorityMatrix.TfrPriorityMatrix.prototype.AddRecord = function (RecordNew) {

    var MDPRIORITYMATRIX = new Persistence.MD.Properties.TMDPRIORITYMATRIX();
    MDPRIORITYMATRIX.IDMDIMPACT = RecordNew.FieldName(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDIMPACT.FieldName).asInt32();
    MDPRIORITYMATRIX.IDMDPRIORITY = RecordNew.FieldName(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDPRIORITY.FieldName).asInt32();
    MDPRIORITYMATRIX.IDMDURGENCY = RecordNew.FieldName(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDURGENCY.FieldName).asInt32();
    ResErr = Persistence.MD.Methods.MDPRIORITYMATRIX_ADD(MDPRIORITYMATRIX);
    return ResErr.NotError;
}
ItHelpCenter.MD.Configuration.PriorityMatrix.TfrPriorityMatrix.prototype.UpdateRecord = function (RecordOld, RecordNew) {
    var MDPRIORITYMATRIX = new Persistence.MD.Properties.TMDPRIORITYMATRIX();
    MDPRIORITYMATRIX.IDMDIMPACT = RecordNew.FieldName(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDIMPACT.FieldName).asInt32();
    MDPRIORITYMATRIX.IDMDPRIORITY = RecordNew.FieldName(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDPRIORITY.FieldName).asInt32();
    MDPRIORITYMATRIX.IDMDURGENCY = RecordNew.FieldName(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDURGENCY.FieldName).asInt32();
    ResErr = Persistence.MD.Methods.MDPRIORITYMATRIX_UPD(MDPRIORITYMATRIX);

    return ResErr.NotError;
}
ItHelpCenter.MD.Configuration.PriorityMatrix.TfrPriorityMatrix.prototype.DeleteRecord = function (RecordOld) {
    var MDPRIORITYMATRIX = new Persistence.MD.Properties.TMDPRIORITYMATRIX();
    MDPRIORITYMATRIX.IDMDIMPACT = RecordOld.FieldName(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDIMPACT.FieldName).asInt32();
    MDPRIORITYMATRIX.IDMDPRIORITY = RecordOld.FieldName(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDPRIORITY.FieldName).asInt32();
    MDPRIORITYMATRIX.IDMDURGENCY = RecordOld.FieldName(UsrCfg.InternoAtisNames.MDPRIORITYMATRIX.IDMDURGENCY.FieldName).asInt32();
    ResErr = Persistence.MD.Methods.MDPRIORITYMATRIX_DEL(MDPRIORITYMATRIX);

    return ResErr.NotError;
}

