﻿ItHelpCenter.MD.Configuration.Priority.TfrMDPriority = function (inObjectHtml, _this) {

    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
   
    //var stackPanelHeader = new TVclStackPanel(stackPanel.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
    //stackPanelHeader.Row.This.style.minHeight = "39px";
    //var label = new TVcllabel(stackPanelHeader.Column[0].This, "", TlabelType.H0);
    //label.This.style.fontSize = "24px;"
    //label.This.style.color = "#999"
    //label.Text = "MD PRIORITY";
    
    if (SysCfg.App.Properties.Device == SysCfg.App.TDevice.Desktop) {
        var stackPanel = new TVclStackPanel(inObjectHtml, "1", 1, [[12], [12], [12], [12], [12]]);
        stackPanel.Row.This.style.margin = "0px 14px";
        stackPanel.Column[0].This.style.padding = "20px 20px";
        stackPanel.Column[0].This.style.border = "1px solid rgba(0, 0, 0, 0.08)";
        stackPanel.Column[0].This.style.boxShadow = " 0 0px 8px 0 rgba(0, 0, 0, 0.06), 0 1px 0px 0 rgba(0, 0, 0, 0.02);";
        stackPanel.Column[0].This.style.marginTop = "15px";
        stackPanel.Column[0].This.style.backgroundColor = "#FAFAFA";
        this.stackPanelBody = new TVclStackPanel(stackPanel.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);

        this.ObjectHtml = this.stackPanelBody.Column[0].This;
    } else {
        this.ObjectHtml = new Udiv(null, "");
        this.ObjectHtml.style.backgroundColor = "#FAFAFA";
        inObjectHtml.appendChild(this.ObjectHtml);
    }
   
    
    this.BarControls = null;
    this.GridCFToolbar = null;
    this.GridCFView = null;
    this.MDPRIORITYList = new Array();

    this.Mythis = "TfrMDPriority";
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1", "You want to add the record");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2", "You want to update the record");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3", "You want to delete the record");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines4", "You are sure to change the record");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines5", "The record was added");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines6", "The record was updated");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines7", "Delete Record");
    this.InitializeComponent();
    this.OpenQuery();
}

ItHelpCenter.MD.Configuration.Priority.TfrMDPriority.prototype.InitializeComponent = function () {
    var _this = this.TParent();

    _this.GridCFView = new Componet.GridCF.TGridCFView(_this.ObjectHtml, function (OutRes) { }, null);
    _this.GridCFView.OnBeforeChange = function (DataSet, RecordOld, RecordNew, Status, isCancel) {//RecorOLD,RecordNew,Status,Cancel

        var message = "";
        if (Status == SysCfg.MemTable.Properties.TStatus.Apped) {
            message = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1");
        }
        else if (Status == SysCfg.MemTable.Properties.TStatus.Update) {
            message = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2");
        }
        else if (Status == SysCfg.MemTable.Properties.TStatus.Delete) {
            message = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3");
        }
        isCancel.Value = !confirm(message + " \n " + UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines4"));

        if (!isCancel.Value) {

            if (Status == SysCfg.MemTable.Properties.TStatus.Apped) {
                isCancel.Value = !_this.AddRecord(RecordNew);
            }
            else if (Status == SysCfg.MemTable.Properties.TStatus.Update) {
                isCancel.Value = !_this.UpdateRecord(RecordOld, RecordNew);
            }
            else if (Status == SysCfg.MemTable.Properties.TStatus.Delete) {
                isCancel.Value = !_this.DeleteRecord(RecordNew);
            }
        }
    }
    _this.GridCFView.OnAfterChange = function (GridCFView, RecordOld, RecordNew) {


        var message = "";

        if (GridCFView.DataSet.Status == SysCfg.MemTable.Properties.TStatus.Apped) {
            message = message + UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines5");
        }
        else if (GridCFView.DataSet.Status == SysCfg.MemTable.Properties.TStatus.Update) {
            message = message + UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines6");
        }
        else if (GridCFView.DataSet.Status == SysCfg.MemTable.Properties.TStatus.Delete) {
            message = message + UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines7");
        }
        alert(message);

    }

}

ItHelpCenter.MD.Configuration.Priority.TfrMDPriority.prototype.OpenQuery = function () {
    var _this = this.TParent();
    var Open = Persistence.MD.Methods.MDPRIORITY_Fill(_this.MDPRIORITYList, "");

    if (Open.ResErr.NotError) {
        
        
        _this.GridCFView.VclGridCF.DataSource = Open.DataSet;
       
        var VTCfgFactoryConfig = new Componet.GridCF.TGridCFView.TCfg();
        VTCfgFactoryConfig.ToolBar.ButtonImg_DeleteBar.Active = true;
        VTCfgFactoryConfig.ToolBar.ButtonImg_EditBar.Active = true;
        VTCfgFactoryConfig.ToolBar.ButtonImg_AddBar.Active = true;
        VTCfgFactoryConfig.ColumnsOptionsDef.InsertActiveEditor = true;
        VTCfgFactoryConfig.ColumnsOptionsDef.UpdateActiveEditor = true;
        VTCfgFactoryConfig.ToolBar.ButtonImg_EditToolsBar.Visible = true;
        _this.GridCFView.GridCFToolbar.NavTabControls.BackgroundHeader = "#2B569A"; 
        VTCfgFactoryConfig.ToolBar.ButtonImg_EditToolsBar.ChekedClickDown = true;
        _this.GridCFView.LoadTCfg(VTCfgFactoryConfig);
        _this.GridCFView.AutoTranslete = true;

    };
}
ItHelpCenter.MD.Configuration.Priority.TfrMDPriority.prototype.AddStyleFile = function (nombre, onSuccess, onError) {
    var style = document.createElement("link");
    style.rel = "stylesheet";
    style.type = "text/css";
    style.href = nombre;
    var s = document.head.appendChild(style);
    s.onload = onSuccess;
    s.onerror = onError;
}
ItHelpCenter.MD.Configuration.Priority.TfrMDPriority.prototype.AddRecord = function (RecordNew) {

    RecordNew.Fields[1].Value = (RecordNew.Fields[1].Value).toLowerCase();
    var MDPRIORITY = new Persistence.MD.Properties.TMDPRIORITY();
    MDPRIORITY.IDMDPRIORITY = RecordNew.FieldName(UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName).asInt32();
    MDPRIORITY.PRIORITYNAME = RecordNew.FieldName(UsrCfg.InternoAtisNames.MDPRIORITY.PRIORITYNAME.FieldName).asString();
    ResErr = Persistence.MD.Methods.MDPRIORITY_ADD(MDPRIORITY);
    return ResErr.NotError;
}
ItHelpCenter.MD.Configuration.Priority.TfrMDPriority.prototype.UpdateRecord = function (RecordOld, RecordNew) {

    RecordNew.Fields[1].Value = (RecordNew.Fields[1].Value).toLowerCase();
    var MDPRIORITY = new Persistence.MD.Properties.TMDPRIORITY();
    MDPRIORITY.IDMDPRIORITY = RecordNew.FieldName(UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName).asInt32();
    MDPRIORITY.PRIORITYNAME = RecordNew.FieldName(UsrCfg.InternoAtisNames.MDPRIORITY.PRIORITYNAME.FieldName).asString();
    ResErr = Persistence.MD.Methods.MDPRIORITY_UPD(MDPRIORITY);
    return ResErr.NotError;
}
ItHelpCenter.MD.Configuration.Priority.TfrMDPriority.prototype.DeleteRecord = function (RecordOld) {


    ResErr = Persistence.MD.Methods.MDPRIORITY_DEL_BY_ID(RecordOld.FieldName(UsrCfg.InternoAtisNames.MDPRIORITY.IDMDPRIORITY.FieldName).asInt32());
    return ResErr.NotError;
}






