ItHelpCenter.GraphicsPBIManager.TGraphicsPBIManagerBasic = function (inObject, incallback, idTemplate) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();//variable que maneja la reeferencia actual del objeto.
    this.ObjectHtml = inObject;//objeto donde estara el tenplate
    this.CallbackModalResult = incallback; //funcion de retorno;
    this.IdTemplate = idTemplate;// id del template a buscar.
    this.ObjBD = new _this.Data(this); //objeto de acceso a BD
    this.ObjTemplate = new _this.Template(this); //Objeto de acceso a Template
    this.ObjGraphics = new _this.Graphics(this);// objeto de acceso a Grafico
    this.Mythis = "TGraphicsPBIManagerBasic";// el nombre del objeto que tendra nuestars traducciones
    UsrCfg.Traslate.GetLangText(_this.Mythis, "ID", "Id");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "SEARCH_NOT_ID_TEMPLATE", "There is no template that you are looking fo");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "OK", "Ok");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "NO_CELLS", "There are no cells");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "NO_CELLS_PARENT", "There are no parent cells");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "NO_CELLS_PARENT_CHILD", "There is no child cell");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "ERROR_QUERY_SQL", "An error occurred when executing the SQL query. Please check that the check is correct.");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "ERROR_CATCH", "An unexpected error occurred, Please try again or check with the administrator.");
    this.CreateObject = false;
    (function () { 
                _this.LoadScript(SysCfg.App.Properties.xRaiz + "Componet/GraphicsPBI/Scripts/Plugin/Chart/Chart.bundle.min.js", function () {
                    _this.LoadScript(SysCfg.App.Properties.xRaiz + "Componet/GraphicsPBI/Scripts/Plugin/D3/d3.min.js", function () {
                        _this.AddStyleFile(SysCfg.App.Properties.xRaiz + "Componet/GraphicsPBI/Card/css/Card.css", function () {
                            _this.AddStyleFile(SysCfg.App.Properties.xRaiz + "Componet/GraphicsPBI/Bar/css/Bar.css", function () {
                                _this.AddStyleFile(SysCfg.App.Properties.xRaiz + "Componet/GraphicsPBI/Pie/css/Pie.css", function () {
                                    _this.AddStyleFile(SysCfg.App.Properties.xRaiz + "Componet/GraphicsPBI/TreeMap/css/TreeMap.css", function () {
                                        _this.AddStyleFile(SysCfg.App.Properties.xRaiz + "Componet/GraphicsPBI/Table/css/GraphicTable.css", function () {
                                            _this.AddStyleFile(SysCfg.App.Properties.xRaiz + "Componet/GraphicsPBI/Funnel/css/GraphicFunnel.css", function () {
                                                try {
                                                    clearInterval(IntervalGraphicsPBIManager);
                                                    var DataTemplate = _this.ObjBD.getTemplate(_this.IdTemplate)
                                                    if (DataTemplate.Status) {
                                                        if (!DataTemplate.Result.TEMPLATE_REFRESHTIMEENABLE) {
                                                            _this.Load();
                                                            if (typeof _this.CallbackModalResult !== 'undefined' && typeof _this.CallbackModalResult === 'function') {
                                                                typeof _this.CallbackModalResult();
                                                            }
                                                        }
                                                        else {
                                                            (function CallTimer() {
                                                                if (_this.CreateObject) {
                                                                    if (document.getElementById(_this.ObjTemplate.ObjHtmlClear.id) === null) {
                                                                        clearInterval(IntervalGraphicsPBIManager);
                                                                        IntervalGraphicsPBIManager = null;
                                                                        return;
                                                                    }
                                                                    $(_this.ObjectHtml).html("");
                                                                    _this.ObjBD = null;
                                                                    _this.ObjTemplate = null;
                                                                    _this.ObjGraphics = null;
                                                                    _this.CreateObject = false;
                                                                    _this.ObjBD = new _this.Data(_this);
                                                                    _this.ObjTemplate = new _this.Template(_this);
                                                                    _this.ObjGraphics = new _this.Graphics(_this);
                                                                }
                                                                clearInterval(IntervalGraphicsPBIManager);
                                                                _this.Load();
                                                                if (typeof _this.CallbackModalResult !== 'undefined' && typeof _this.CallbackModalResult === 'function') {
                                                                        _this.CallbackModalResult();
                                                                }
                                                                var DataTemplate = _this.ObjBD.getTemplate(_this.IdTemplate)
                                                                if (DataTemplate.Status && _this.CreateObject) {
                                                                    IntervalGraphicsPBIManager = setTimeout(CallTimer, (parseInt(DataTemplate.Result.TEMPLATE_REFRESHTIME)*1000));
                                                                }
                                                            })()
                                                        }
                                                    }
                                                    else {
                                                        clearInterval(IntervalGraphicsPBIManager);
                                                        alert(DataTemplate.Message);
                                                    }
                                                } catch (e) {
                                                    SysCfg.Log.Methods.WriteLog("GraphicsPBIManagerBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIManagerBasic", e);
                                                    clearInterval(IntervalGraphicsPBIManager);
                                                    alert(e);
                                                }
                                            }, function (error) { });
                                        }, function (error) { });
                                    }, function (error) { });
                                }, function (error) { });
                            }, function (error) { });
                        }, function (error) { });
                    }, function (error) { });
                }, function (error) { });
           
    })()
}
ItHelpCenter.GraphicsPBIManager.TGraphicsPBIManagerBasic.prototype.Load = function () {
    try {
        var _this = this.TParent();//variable que manejrar el objeto prinicipal TGraphicsPBIManagerBasic 
        var DataTemplate = _this.ObjBD.getTemplate(_this.IdTemplate);// variable que almacena el resultado de la busqueda de template
        if (DataTemplate.Status) {//verifica el estado de la busqueda
            //si es verdadera el estado  hace referencia al objeto template y a partir de ahí manda a llamar al metodo createTemplate para crear el template entero
            // le pasa como parametro el resultado del datatemplate en este caso el obejto dtemplate de la base de datos t donde lo va fincar.
            var QueryTemplate = _this.ObjBD.getQuery(DataTemplate.Result.TEMPLATE_DATASQL);// Verifico si la query pasada obtiene datos, en caso no obtener mando una alerta.
            if (QueryTemplate.Status) { // si existen datos en la consulta SQL y ningun error entonces el status contiene True y puede seguir con la verificación.
                _this.ObjTemplate.FormatObjectQueryTemplate(QueryTemplate.Result); //fomateamo la query prinicipal para tenerlo como referencia en el objeto template es decir la query del template.
                var CreateTemplate = _this.ObjTemplate.CreateTemplate(DataTemplate.Result, _this.ObjectHtml);
                _this.CreateObject = CreateTemplate.Status;
                if (!CreateTemplate.Status) { // Si Al crear el  objeto Template retorna la propiedad status false, manda un mensaje de alerta
                    alert(CreateTemplate.Message);
                }
            }
            else {
                alert(QueryTemplate.Message);// en caso de error al ejecutar la consulta jquery template
            }
        }
        else {
            alert(DataTemplate.Message); // en caso no ser así alerta con un mensaje.
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicsPBIManagerBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIManagerBasic.prototype.Load", e);
        throw e;
    }
}
ItHelpCenter.GraphicsPBIManager.TGraphicsPBIManagerBasic.prototype.Data = function (_this) {
    var This = this; // almaceno en una variable global el this actual del objeto Data, para accederlo desde cualquier parte del objeto.
    this._This = _this; //almaceno el This del objeto en general.
    this.PanelServiceProfiler = new Persistence.PBITemplate.TPBITemplateProfiler(); //Variable que manejara todo la informacion de la data que vamos a trabajar.
    //getTemplate:Metodo que permite verificar si existe un template o no
    this.getTemplate = function (Id) {
        try {
            //DataTemplate: obtengo el objeto template por id, en caso no ser así retorna el objeto template creado desde 0 con el id pasado.
            var DataTemplate = Persistence.PBITemplate.Methods.PBITEMPLATE_ListSetID(This.PanelServiceProfiler.PBITEMPLATEList, Id);
            //verifica mediante le metodo PBITEMPLATE_ListGetIndex si existe o no el id, si es mayor a -1 es que existe si no no existe
            //para luego retornar el DataTemplate
            if (Persistence.PBITemplate.Methods.PBITEMPLATE_ListGetIndex(This.PanelServiceProfiler.PBITEMPLATEList, Id) > -1) {
                return ({ Message: UsrCfg.Traslate.GetLangText(_this.Mythis, "OK"), Status: true, Result: DataTemplate });
            }
            else {
                return ({ Message: UsrCfg.Traslate.GetLangText(_this.Mythis, "SEARCH_NOT_ID_TEMPLATE"), Status: false, Result: null });
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("GraphicsPBIManagerBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIManagerBasic.prototype.Data", e);
            throw e;
        }
    }
    this.getQuery = function (StrSQL) {
        Param = new SysCfg.Stream.Properties.TParam();
        var Message = null;
        var Status = null;
        try {
            Param.Inicialize();
            OpenDataSet = SysCfg.DB.SQL.Methods.OpenDataSet(StrSQL, Param.ToBytes());
            if (OpenDataSet.ResErr.NotError) {
                if (OpenDataSet.DataSet.RecordCount > 0) {
                    Status = true;
                    Message = UsrCfg.Traslate.GetLangText(_this.Mythis, "OK");
                }
                else {
                    OpenDataSet.ResErr.NotError = true;
                    Status = false;
                    Message = OpenDataSet.ResErr.Mesaje;
                }
            }
            else {
                Status = false;
                Message = UsrCfg.Traslate.GetLangText(_this.Mythis, "ERROR_QUERY_SQL");
            }
        }
        catch (e) {
            Status = false
            Message = UsrCfg.Traslate.GetLangText(_this.Mythis, "ERROR_CATCH");
            SysCfg.Log.Methods.WriteLog("GraphicsPBIManagerBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIManagerBasic.prototype.Data this.getQuery" + Message, e);
        }
        finally {
            Param.Destroy();
        }
        return ({ Result: OpenDataSet.DataSet, Status: Status, Message: Message });
    }
    this.getDataAllSync = function () {
        try {
            This.PanelServiceProfiler.Fill();
            return ({ Message: "Ok", Status: true, Result: null });
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("GraphicsPBIManagerBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIManagerBasic.prototype.Data this.getQuery", e);
            throw e;
        }
    };
    (function () {
        This.getDataAllSync();
    })();
}
ItHelpCenter.GraphicsPBIManager.TGraphicsPBIManagerBasic.prototype.Template = function (_this) {
    var This = this;// almaceno en una variable global el this actual del objeto Data, para accederlo desde cualquier parte del objeto.
    this._This = _this //almaceno el This del objeto en general.
    this.NodeParent = new Array(); //variable que alamacenara todo el arbol de contendio para el template y graficos.
    this.ObjectQueryTemplate = null;
    this.FormatObjectQueryTemplate = function (DataSetQueryTemplate) {
        try {
            var ArrayQueryTemplate = new Array(); // creo un array por que sera una lista de objetos 
            for (var i = 0; i < DataSetQueryTemplate.RecordCount; i++) { //recorro el data set
                var ObjectQueryTemplate = {}; //creo mi objeto como variable principal
                for (var x = 0; x < DataSetQueryTemplate.Records[i].Fields.length; x++) { // recorro los campos de l dataset
                    var Column = DataSetQueryTemplate.Records[i].Fields[x].FieldDef.FieldName; //obtengo columan
                    var Value = DataSetQueryTemplate.Records[i].Fields[x].Value; //obtengo valor
                    ObjectQueryTemplate[Column] = Value; //al objeto añado la propiedad dinamicamente y su valor de uigual manera
                }
                ArrayQueryTemplate.push(ObjectQueryTemplate); // poor ultimo al terminar de recorrer la fila añado a la lista de arreglo
            }
            This.ObjectQueryTemplate = ArrayQueryTemplate; //devuelvo el valor.
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("GraphicsPBIManagerBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIManagerBasic.prototype.Template  this.FormatObjectQueryTemplate", e);
            throw e;
        }
    };
    this.CreateTemplate = function (objTemplate, objDom) {
        try {
            var ListNodes = objTemplate.PBITEMPLATECELLSList;//obtiene la lista de celdas de la BD
            var ListNodesParent = objTemplate.PBITEMPLATECELLSPARENTList;//obtiene la lista de celdas padres BD
            if (ListNodes.length > 0) { //verifica que exista al menos 1 celda, en caso no ser así manda un mensaje de alerta, alertando que no existen celdas.
                if (ListNodesParent.length > 0) {//verifica que exista al menos 1 celda padre, en caso no ser así manda un mensaje de alerta, alertando que no existen celdas padre.
                    //_OrderPosition:Metodo que me permite ordenar por posición las celdas.
                    This._OrderPosition(ListNodesParent); // orderno por posición las celdas padres, para así dibujar por posición las cendras
                    //_CreateBoostrap:Metodo que permite crear las celdas segun su diseño de celda basanadose el boostrap.
                    //los parametros que recibe son las celdas el objeto dom donde va ir fincado y además, true si lo que envia son los padres.
                    This.NodeParent = This._CreateBoostrap(ListNodesParent, objDom, true).Result;
                    //Graphic.CreateGraphics(This.NodeParent.Child);
                    //recorremos la lista de parent para ir buscando sus nodos hijos.
                    //var CountChild = 0; //declaramos esta variable para saber si existe por lo menos un hijo en las celdas padres, en caso no existir ningun hijo retornara un mensaje.
                    for (var i = 0; i < ListNodesParent.length; i++) {//RECOORE LA LISTA DE CELDAS PADRES Y VA PREGUNTANSO SI TIENE HIJOS, SI TIENES HIJOS LAS ORDENA  Y RECORRE LOS HIJOS
                        var ListNodeChild = ListNodesParent[i].PBITEMPLATECELLS_CHILDList;// asignar la lista de hijos del padre
                        if (ListNodeChild.length > 0) {//si la lista de hijos es mayor a 0 es que hay hijoS en el padre
                            //CountChild++; // por lo tanto cuenta hijos
                            This._OrderPosition(ListNodeChild);// los cuales lo ordena por position ya que se van a dibujar de acuerdo a la posición
                            This._EachChild(ListNodeChild, This.NodeParent.Child[i].Child, This.NodeParent.Child[i].Dom, true);// MAando a recorrelos hijos
                            //_EachChild, se le pasa como parametro la lista de hijos, el objeto del padre con la propiedad list child que es la que contendra esos hijos que van a ser creados, 
                            //el padre html para fincar ahí a los hijos y si es inicial, es decir padre directto.
                            //This.NodeParent esta ordenado por ende se dibujo de acuerdo a la posición y se va recorriendo de acuerdo a eso
                            // Va tener This.NodeParent.Child 2 hijos igual de lo que se esta recorriendo
                        }
                        else{
                            var _ArrayTem = new Array(This.NodeParent.Child[i]);
                            This._This.ObjGraphics.CreateGraphics(_ArrayTem);
                        }
                    }
                    return { Message: UsrCfg.Traslate.GetLangText(_this.Mythis, "OK"), Status: true, Result: null }
                    //if (CountChild > 0) {//si existe hijo el estado es true, si no existe hay no hay hijo y alertara al cliente.
                        //return { Message: UsrCfg.Traslate.GetLangText(_this.Mythis, "OK"), Status: true, Result: null };
                    //}
                    //else {
                        //return { Message: UsrCfg.Traslate.GetLangText(_this.Mythis, "NO_CELLS_PARENT_CHILD"), Status: false, Result: null };
                    //}
                }
                else {
                    return { Message: UsrCfg.Traslate.GetLangText(_this.Mythis, "NO_CELLS_PARENT"), Status: false, Result: null };
                }
            }
            else {
                return { Message: UsrCfg.Traslate.GetLangText(_this.Mythis, "NO_CELLS"), Status: false, Result: null };
            }
        } catch (e) {
            //manejador principal de errores
            SysCfg.Log.Methods.WriteLog("GraphicsPBIManagerBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIManagerBasic.prototype.Template this.CreateTemplate", e);
            return { Message: e, Status: false, Result: null };
        }
    }
    this._OrderPosition = function (ListCells) {
        //metodo que permite ordernar de acuerdo a la posicion.
        //para Dibujar las celdas necesita primero ordernarlas segun la posición que se desea dibujar.
        try {
            ListCells.sort(function (a, b) { return a.CELLS_POSITION - b.CELLS_POSITION; });
        } catch (e) {
            // en caso exista un error manda a llamar al error principal para que lo maneje.
            SysCfg.Log.Methods.WriteLog("GraphicsPBIManagerBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIManagerBasic.prototype.Template this._OrderPosition", e);
            throw e;
        }
    }
    this._CreateBoostrap = function (ListCells, objHtml, Init) {
        try {
            //param: listcell: lista de celdas, objhtml:donde va fincar, Init, si es celda padre.
            var objBoostrap = { xs: [], sm: [], md: [], lg: [], xl: [], length: 0, Data: { Row: null, Child: new Array() } };
            //objBoostrap tendra gurdado el diseño del boostrap la cantidad de celdas, la data que es el Row principal y la Lista de celdas creadas
            var ObjDesign = null;
            //ObjDesign. Tendra gurdado todo lo que esta en el objeto design de la BD
            objBoostrap.length = ListCells.length; //objBoostrap guarda la cantidad de celdas
            for (var x = 0; x < ListCells.length; x++) {// recorre las celdas y va agregando  al objeto boostrap la información
                ObjDesign = ListCells[x].PBITEMPLATECELLSDESIGN; //asign al ObjDesign el objeto diseño de la celda actual
                //va agregando los diseños al objBoostrap por celda
                objBoostrap.xs.push(ObjDesign.DESIGN_XS);
                objBoostrap.sm.push(ObjDesign.DESIGN_S);
                objBoostrap.md.push(ObjDesign.DESIGN_M);
                objBoostrap.lg.push(ObjDesign.DESIGN_L);
                objBoostrap.xl.push(ObjDesign.DESIGN_XL);
                //además va agregando Al objeto objBoostrap porpieda chield el valor del objeto celda BD, Dom de la celda, y una lista de Child ya que ese hijo puede convertirse en padre y tener hijos.
                objBoostrap.Data.Child.push({ Data: ListCells[x], Dom: null, ObjectGraphic: null, Child: new Array() });
            }
            if (Init === true) {// luego de ellos valida si es true (por que va crear celdas padres)
                //crea El tenplate inciial pasandole  el objeto donde se va fincar, la cantidad de celdas(bjBoostrap.length), y además los arrays del boostrap de las celdas.
                var RowColumn = new Componet.TGraphicsPBI.TVclTemplate(objHtml, "", objBoostrap.length, [objBoostrap.xs, objBoostrap.sm, objBoostrap.md, objBoostrap.lg, objBoostrap.xl]);
                This.ObjHtmlClear = RowColumn.Row.This;
                //una vez creado RowColumn, este Row se agregara en el objeto objBoostrap la propiedad  data la cual sera el objeto a retornar. En data se agregar en la propiedad Row, que es el row de los childs (celdas)
                objBoostrap.Data.Row = RowColumn.Row.This;
                // OJO ES IMPORTANTE QUE LAS CELDAS ESTEN ORDENAS POR POSICION
                // YA QUE DE LA MANERA QUE SE VA DIBUJANDO, TAMBIEN VOY LEYENDO Y RECORRIENDO EL DOM
                for (var i = 0; i < RowColumn.Column.length; i++) {//luego recorro las columnas del RowColumn
                    //esas columnas son las mismas que las celdas es decir que si yo tengo 2 columnas voy a tener 2 celdas
                    ///las cuales se llenaran de acuerdo a como se dibujaron, por eso es importante ordernar por posicion la celdas
                    objBoostrap.Data.Child[i].Dom = RowColumn.Column[i].ColumnInit.This; //agrego en el dom del child la celda creada
                    This._SetId(objBoostrap.Data.Child[i].Dom, objBoostrap.Data.Child[i].Data.IDPBITEMPLATECELLS);// agrego su id
                    //set id me permite agregar un atributo data-CellId pasandolre  la celda que va tener el CellId y el id (CellId= el id de la celda)
                    //puede que utilice este valor más adelnate
                    //hasta ahora funciona solo para saber como va dibujandose las celdas.
                }
                return { Message: "Ok", Status: true, Result: objBoostrap.Data };
                //OJO RETORNA objBoostrap.Data
                //ES DECIR Result Es el objeto data el cual tendra 
                //Data: { Row: null, Child: new Array({ Data: ListCells[x], Dom: null, Child: new Array() }) }
                //Row: El Row Creado que contendra la celdas, Child la cual es una lista de las celdas es decir columnas, las cuales tendra
                //Data es Decvir objeto BD de la celda, Dom: que es la CElda HTML y nuevamente Child donde tendra una lista de objBoostrap.Data
            }
            else {
                //LO MISMA LOGICA QUE LA PRIMERA PERO ACA UTILIZO TVclStackPanel , LUEGO TIENE LA MISMA LOGICA QUE EL INIT TRUE,
                //A TENER EN CUENTA QUE SIEMPRE ENTRARA A ESTE PARTE SIEMPRE Y CUANDO LAS CELDAS NO SEAN LAS INICIALES.
                var RowColumn = new TVclStackPanel(objHtml, "", objBoostrap.length, [objBoostrap.xs, objBoostrap.sm, objBoostrap.md, objBoostrap.lg, objBoostrap.xl]);
                objBoostrap.Data.Row = RowColumn.Row.This;
                for (var i = 0; i < RowColumn.Column.length; i++) {
                    objBoostrap.Data.Child[i].Dom = RowColumn.Column[i].This;
                    This._SetId(objBoostrap.Data.Child[i].Dom, objBoostrap.Data.Child[i].Data.IDPBITEMPLATECELLS);
                }
                return { Message: "Ok", Status: true, Result: objBoostrap.Data };
                //OJO RETORNA objBoostrap.Data
                //ES DECIR Result Es el objeto data el cual tendra 
                //Data: { Row: null, Child: new Array({ Data: ListCells[x], Dom: null, Child: new Array() }) }
                //Row: El Row Creado que contendra la celdas, Child la cual es una lista de las celdas es decir columnas, las cuales tendra
                //Data es Decvir objeto BD de la celda, Dom: que es la CElda HTML y nuevamente Child donde tendra una lista de objBoostrap.Data
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("GraphicsPBIManagerBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIManagerBasic.prototype.Template  this._CreateBoostrap", e);
            throw e; //en caso de error salta catch
            
        }
    }
    this._SetId = function (objDom, Id) {
        //Recibe el elemento el cual se va crear CellId y además el id a setear.
        try {
            objDom.setAttribute('data-CellId', Id);
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("GraphicsPBIManagerBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIManagerBasic.prototype.Template  this._SetId", e);
            throw e; //en caso de error salta catch
        }
    }
    this._EachChild = function (ListNodesChild, Parent, ParentDom, Init) {
        try {
            var FlagParent = false;
            if (Parent.length <= 0 && !Init) { //dondicion true para que cambie la bandera. // emtra a una llamda recursiva por eso el Init mando en false // siemrpe entrara acá cuando comienze a llamarse recursivamente

                var NodeChild = This._CreateBoostrap(ListNodesChild, ParentDom, false).Result; //envia la lista de nodos a crear / con su parent dom donde se va fincar es decir la celda padre
                This._This.ObjGraphics.CreateGraphics(NodeChild.Child); // crea su grafico  en caso lo tenga. //envia NodeChild, los Child que son las columnas
                Parent.push(NodeChild);//Parent object agrego el objeto hijo creado
                FlagParent = true;//cambio la bandera a true por que ya cree el nodo hijo por completo
            }
            for (var i = 0; i < ListNodesChild.length; i++) {// recorre todos las celdas hijas
                var ListNodesChild_ListNodesChild = ListNodesChild[i].PBITEMPLATECELLS_CHILDList; // verifica que las celdas hijas no sean padres es decir que tengas hijos
                if (ListNodesChild_ListNodesChild.length <= 0) {// si las celdas hijas continenen no contienen hijo entras es decir que sea creara un row y un column
                    if (FlagParent) {//Una bandera para saber que es una llamdas que es cuando un hijo se vuelve padre y tiene hijos-
                        continue;// salta de hijo, ya que cuando es hijo de hijo lo crea en el bloque (Parent.length <= 0 && !Init) 
                        //OJO CUANDO ENTRA RECURSIVAMENTE SIEMPRE VA ENTRA (Parent.length <= 0 && !Init)  POR ENDE YA NO PUEDO CREARLO NUEVAMENTE POR ESO SALGO DEL BUCLE SIEMPRE Y CUADNO SEA HIJO UNICO CUANDO NO ES HIJO UNICO VUELVE A LLAMARSE RECURSIVAMENTE
                    }
                    //OJO ACA CREARA SOLO UN 1 ROW Y 1 COLUMN
                    var _ArrayTem = new Array(ListNodesChild[i]); // CREA UN ARRAY UNICO CON EL HIJO(HIJO CONTENDRA EL DATO CELDA)
                    var NodeChild = This._CreateBoostrap(_ArrayTem, ParentDom, false).Result;//aca crea el boostrap de row y column dentro del padre
                    This._This.ObjGraphics.CreateGraphics(NodeChild.Child);//aca crea el grafico, mandandole el objeto NodeChild.Child que se creo previamente en el boostrap es ddecir le mando la celda donde va estar el grafico
                    Parent.push(NodeChild);// ya ca agreag en el parent del objeto princiapl del template el nodo hijo.

                }
                else {// en caso el noddo hijo se convierte en padre y tenga hijos
                    This._OrderPosition(ListNodesChild_ListNodesChild); // ordena a los hijos
                    This._EachChild(ListNodesChild_ListNodesChild, (Parent.length <= 0) ? Parent : Parent[Parent.length - 1].Child[i].Child, (Parent.length <= 0) ? ParentDom : Parent[Parent.length - 1].Child[i].Dom, false);
                    //vuelve a llamar al mismo metodo de manera recursiva mandandole lo siguiente
                    //lista de los hijos del padre que es hijo.
                    //Parent.length< = 0 Pranet quiere decir que es cuando tiene hijo de hijo por primera vez // es donde contendran los hijos.
                    //Parent[Parent.length - 1].Child[i].Child, SIGNIFICA QUE LOS HIJOS SE CONVITIERON EN PADRE SE LLAMARON RECURSIVAMENTE SE AGREGARON AL PADRE Y ESOS HIJOS TUVIERON HIJOS
                    //LOS CUALES AL ENTRAR A LA BANDERA CREARON UN OBJETO HIJO QUE CONTIENE HIJOS POR ESO MANDO A LLAMR AL ULTIMO ELEMETO PADRE. CON EL CHILD[I] POR QUE SE ESTA RECORRIENDO ORDENAMENTE Y TIENENN LOS MISMOS ELEMETOS
                    //EL CUAL SERA EL ARRAY QUE CONTENDRAN ESOS NUEVOS HIJOS, DE IGUAL MANERA PARA EL DOM.
                    //Parent.length< = 0 ParentDom quiere decir que es cuando tiene hijo de hijo por primera vez // es donde se fincaran los hijos
                    //envio false para que cumpla la condición y entre al agregar hijo de hijo y cambie de bandera // que ya no es Init, esta llamandose recursivamente
                }
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("GraphicsPBIManagerBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIManagerBasic.prototype.Template  this._EachChild ", e);
            throw e;
        }
    }
    this.GetFilterDataColumnTemplate = function (Column, Value) {
        try {
            var NewObjectQueryTemplate = SysCfg.CopyList(This.ObjectQueryTemplate);
            NewObjectQueryTemplate = Enumerable.From(NewObjectQueryTemplate)
                .Where(function (Element) { return Element[Column] == Value })
                .Select(function (Element) { return Element }).ToArray();
            return (NewObjectQueryTemplate);
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("GraphicsPBIManagerBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIManagerBasic.prototype.Template  this.GetFilterDataColumnTemplate ", e);
            throw e;
        }
    };
    this.ClearTemplate = function () {
        //limpia el template
        try {
            $(This.ObjectHtml).html("");
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("GraphicsPBIManagerBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIManagerBasic.prototype.Template  this.ClearTemplate ", e);
            console.alert(e);
        }
    }
}
ItHelpCenter.GraphicsPBIManager.TGraphicsPBIManagerBasic.prototype.Graphics = function (_this) {
    var This = this;// almaceno en una variable global el this actual del objeto Data, para accederlo desde cualquier parte del objeto.
    this._This = _this //almaceno el This del objeto en general.
    this._Type = {
        Logo: { value: 1, name: "Logo" },
        Bar: { value: 2, name: "Bar" },
        TreeMap: { value: 3, name: "TreeMap" },
        Pie: { value: 4, name: "Pie" },
        Table: { value: 5, name: "Table" },
        Funnel: { value: 6, name: "Funnel" },
        Card: { value: 7, name: "Card" },
        PieOne: { value: 1002, name: "PieOne" }
    }; //variable que almacena los enum de los tipo de graficos que existen en la BD
    this._ArrayColors =
        ['#01B8AA', '#FEAB85', '#BE4A47', '#A66999', '#B59525', '#374649', '#F95C38', '#F2C80F', '#8AD4EB', '#A66999',
            '#3599B8', '#AF9393', '#2B6863', '#1C2325', '#047283', '#C19028', '#142D1A', '#9B0F7D', '#FFED28', '#10099B']
    this._TempColors = new Array();
    this.ListGraphics = new Array(); //almacena todos mis graficos creados//sera una lista de graficos todos los graficos de la celda quese hya creados
    this.CreateGraphics = function (ListTemplateCell) { //le paso el objeto template cell
        try {
            for (var i = 0; i < ListTemplateCell.length; i++) { //recorro las celdas
                var CellGraphic = ListTemplateCell[i].Data.PBITEMPLATECELLGRAPHIC; //obtengo la informacion de BD de la celda
                if (CellGraphic !== null) { // si es diferente de nulo es decir que tiene un PBITEMPLATECELLGRAPHIC entra al metodo si no no hace nada es decir no agrega ningun grafico a la celda ya sea por que es padre o por un error
                    var CellGraphicType = CellGraphic.PBIGRAPHICTYPE; //obtiene el tipo de grafico
                    if (CellGraphicType !== null) {// si es diferente de nulo es decir que tiene un PBIGRAPHICTYPE entra al metodo si no no hace nada es decir no agrega ningun grafico a la celda, esto ocurririra por algun error por que todo cellfrafic debe de tener un grafico
                        var CellGraphicName = CellGraphicType.GRAPHIC_NAME;// obtiene el nombre del grafico y los compara con el enum del objeto, si no coinciden es por que existe un error
                        switch (CellGraphicName) { // crea de acuerdo a cada tipo su objeto grafico y agrega ListGraphics y ListTemplateCell el grafico
                            case This._Type.Logo.name:
                                This._CreateLogo(ListTemplateCell[i]);
                                break;
                            case This._Type.Bar.name:
                                This._CreateBar(ListTemplateCell[i]);
                                break;
                            case This._Type.TreeMap.name:
                                This._CreateTreeMap(ListTemplateCell[i]);
                                break;
                            case This._Type.Pie.name:
                                This._CreatePie(ListTemplateCell[i]);
                                break;
                            case This._Type.PieOne.name:
                                This._CreatePie(ListTemplateCell[i]);
                                break;
                            case This._Type.Table.name:
                                This._CreateTable(ListTemplateCell[i]);
                                break;
                            case This._Type.Funnel.name:
                                This._CreateFunnel(ListTemplateCell[i]);
                                break;
                            case This._Type.Card.name:
                                This._CreateCard(ListTemplateCell[i]);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("GraphicsPBIManagerBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIManagerBasic.prototype.Graphics  this.ClearTemplate ", e);
            throw e
        }
    }
    this._CreateLogo = function (TemplateCell) {
        try {
            var CellGraphic = TemplateCell.Data.PBITEMPLATECELLGRAPHIC;
            var CellGraphicType = CellGraphic.PBIGRAPHICTYPE.GRAPHIC_NAME;
            var LogoID = CellGraphic.IDPBITEMPLATECELLGRAPHIC.toString() + CellGraphic.IDPBITEMPLATECELLS.toString() + CellGraphic.IDPBIGRAPHICTYPE.toString() + "_" + CellGraphic.CELLGRAPHIC_TITLE;
            var TitleLogoID = LogoID + "_Title";
            This._CreateTitleCellGraphic(TemplateCell, TitleLogoID);
            var ObjectLogo = new Componet.TGraphicsPBI.TVclLogo(TemplateCell.Dom, LogoID + "_GraphicLogo");
            ObjectLogo.Url = "Scripts/GraphicsPBI/Image/Discovery.png";
            ObjectLogo.CreateGraphics();
            This.ListGraphics.push({ Id: LogoID + "_GraphicLogo", Graphic: ObjectLogo, Data: TemplateCell, DataSetInit: This._This.ObjTemplate.ObjectQueryTemplate, DataSetColumns: null, FilterColumn: null, Type: CellGraphicType });
            TemplateCell.ObjectGraphic = ObjectLogo;
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("GraphicsPBIManagerBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIManagerBasic.prototype.Graphics  this._CreateLogo ", e);
            throw e;
        }
    }
    this._CreateBar = function (TemplateCell) {
        try {
            var CellGraphic = TemplateCell.Data.PBITEMPLATECELLGRAPHIC;
            var CellGraphicType = CellGraphic.PBIGRAPHICTYPE.GRAPHIC_NAME;
            var BarID = CellGraphic.IDPBITEMPLATECELLGRAPHIC.toString() + CellGraphic.IDPBITEMPLATECELLS.toString() + CellGraphic.IDPBIGRAPHICTYPE.toString() + "_" + CellGraphic.CELLGRAPHIC_TITLE + "_Cell";
            var TitleBarID = BarID + "_Title";
            This._CreateTitleCellGraphic(TemplateCell, TitleBarID);
            var ListBarColumn = CellGraphic.PBITEMPLATECELLGRAPHIC_COLUMNList;
            if (ListBarColumn.length <= 0) {
                return;
            }
            else if (ListBarColumn.length === 1) {
                var ObjectBar = new Componet.TGraphicsPBI.TVclBars(TemplateCell.Dom, BarID + "_GraphicBar");
                var GraphicBarID = BarID + "_ObjectGraphicBar_";
                var ArrayElements = This._GetLinqBar(This._This.ObjTemplate.ObjectQueryTemplate, ListBarColumn[0], GraphicBarID, null);
                for (var i = 0; i < ArrayElements.length; i++) {
                    var Element = new Componet.TGraphicsElements.TVclBarsElements();
                    Element.DisplayValue = ArrayElements[i]["Label"];
                    Element.Value = ArrayElements[i]["Value"];
                    Element.Color = ArrayElements[i]["Color"];
                    Element.ColorHighlight.ColorElementFather.name = This._HexToRgba(ArrayElements[i]["Color"], "0.6");
                    Element.ColorHighlight.ColorElementChild.name = This._HexToRgba(ArrayElements[i]["Color"], "1");
                    Element.Text = ArrayElements[i]["Text"];
                    Element.ColorOption = ArrayElements[i]["ColorOption"];
                    Element.BorderOption = ArrayElements[i]["BorderOption"];
                    Element.DisplayOption = ArrayElements[i]["DisplayOption"];
                    Element.ColorText = ArrayElements[i]["ColorText"];
                    Element.BorderColorGraphic = ArrayElements[i]["BorderColorGraphic"];
                    Element.HoverColorGraphic = ArrayElements[i]["HoverColorGraphic"];
                    Element.Id = ArrayElements[i]["Id"];
                    ObjectBar.BarsElementsAdd(Element);
                }
                ObjectBar.BorderWidth = 1;
                ObjectBar.BeginAtZero = true;
                ObjectBar.MinRotation = 40;
                ObjectBar.MaxRotation = 40;
                ObjectBar.HeightPlugin = "380px";
                ObjectBar.CreateGraphics();
                ObjectBar.onClickPlugin = function (chartData, idx) {
                    var GraphicBar = TemplateCell.ObjectGraphic;
                    GraphicBar.Graphic.ResetElementsHighlight();
                    var Element = GraphicBar.Graphic.SearchElement(chartData.labels[idx]);
                    if (Element.FlagClick == true) {
                        This._GraphicReset(GraphicBar);
                        Element.FlagClick = false;
                    }
                    else {
                        GraphicBar.Graphic.ResetFlagClick();
                        GraphicBar.Graphic.ResetOption();
                        var FilterColumn = GraphicBar.FilterColumn.COLUMN_NAME;
                        var GraphicLabel = chartData.labels[idx];
                        var NewArrayElements = This._This.ObjTemplate.GetFilterDataColumnTemplate(FilterColumn, GraphicLabel);
                        This._SetValuesHighlight(NewArrayElements, GraphicBar);
                        Element.FlagClick = true;
                    }
                }
                This.ListGraphics.push({ Id: BarID + "_GraphicBar", Graphic: ObjectBar, Data: TemplateCell, DataSetInit: This._This.ObjTemplate.ObjectQueryTemplate, DataSetColumns: ArrayElements, FilterColumn: ListBarColumn[0], Type: CellGraphicType });
                TemplateCell.ObjectGraphic = { Id: BarID + "_GraphicBar", Graphic: ObjectBar, Data: TemplateCell, DataSetInit: This._This.ObjTemplate.ObjectQueryTemplate, DataSetColumns: ArrayElements, FilterColumn: ListBarColumn[0], Type: CellGraphicType };
            }
            else if (ListBarColumn.length > 1) {
                // PARA CUANDO EXISTA RELACION DE UNO A MUCHOS.
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("GraphicsPBIManagerBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIManagerBasic.prototype.Graphics  this._CreateBar ", e);
            throw e;
        }
    }
    this._GetLinqBar = function (ObjectQueryTemplate, Column, GraphicBarID, NameValue) {
        try {
            if (NameValue === null) {
                var ObjectQueryBar = SysCfg.CopyList(ObjectQueryTemplate);
                ObjectQueryBar = Enumerable.From(ObjectQueryBar).GroupBy(function (Element) {
                    return Element[Column.COLUMN_NAME];
                }).Select(function (Element) {
                    var Color = This._GetRandomColor();
                    return {
                        Label: Element.source[0][Column.COLUMN_NAME],
                        Value: Element.Sum(function (subItem) {
                            return 1 | 0;
                        }),
                        Color: Color,
                        Text: "Cantidad",
                        ColorOption: "white",
                        BorderOption: "#00a98f",
                        DisplayOption: true,
                        ColorText: "black",
                        BorderColorGraphic: Color,
                        HoverColorGraphic: Color,
                        Id: GraphicBarID
                    }
                }).ToArray();
                return (ObjectQueryBar);
            }
            else {

            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("GraphicsPBIManagerBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIManagerBasic.prototype.Graphics  this._GetLinqBar ", e);
            throw e;
        }
    }
    this._CreateTreeMap = function (TemplateCell) {
        try {
            var CellGraphic = TemplateCell.Data.PBITEMPLATECELLGRAPHIC;
            var CellGraphicType = CellGraphic.PBIGRAPHICTYPE.GRAPHIC_NAME;
            var TreeMapID = CellGraphic.IDPBITEMPLATECELLGRAPHIC.toString() + CellGraphic.IDPBITEMPLATECELLS.toString() + CellGraphic.IDPBIGRAPHICTYPE.toString() + "_" + CellGraphic.CELLGRAPHIC_TITLE + "_Cell";
            var TitleTreeMapID = TreeMapID + "_Title";
            This._CreateTitleCellGraphic(TemplateCell, TitleTreeMapID);
            var ListTreeMapColumn = CellGraphic.PBITEMPLATECELLGRAPHIC_COLUMNList;
            if (ListTreeMapColumn.length <= 0) {
                return;
            }
            else if (ListTreeMapColumn.length === 1) {
                var ObjectTreeMap = new Componet.TGraphicsPBI.TVclTreeMap(TemplateCell.Dom, TreeMapID + "_GraphicTreeMap");
                var GraphicTreeMapID = TreeMapID + "_ObjectGraphicTreeMap_";
                var ArrayElements = This._GetLinqTreeMap(This._This.ObjTemplate.ObjectQueryTemplate, ListTreeMapColumn[0], GraphicTreeMapID, null);
                for (var i = 0; i < ArrayElements.length; i++) {
                    var Element = new Componet.TGraphicsElements.TVclTreeMapElements();
                    Element.Id = ArrayElements[i]["Id"];
                    Element.Name = ArrayElements[i]["Label"];
                    Element.Color = ArrayElements[i]["Color"];
                    Element.Value = ArrayElements[i]["Value"];
                    Element.onClick = function (Element) {
                        //debugger;
                        var GraphicTreeMap = TemplateCell.ObjectGraphic;
                        GraphicTreeMap.Graphic.ResetElementsHighlight();
                        if (Element.FlagClick == true) {
                            This._GraphicReset(GraphicTreeMap);
                            Element.FlagClick = false;
                        }
                        else {
                            GraphicTreeMap.Graphic.ResetFlagClick();
                            var FilterColumn = GraphicTreeMap.FilterColumn.COLUMN_NAME;
                            var GraphicLabel = Element.Name; var NewArrayElements = This._This.ObjTemplate.GetFilterDataColumnTemplate(FilterColumn, GraphicLabel);
                            This._SetValuesHighlight(NewArrayElements, GraphicTreeMap);
                            Element.FlagClick = true;
                        }
                    }
                    ObjectTreeMap.TreeMapElementsAdd(Element);
                }
                ObjectTreeMap.HeightPlugin = "400px";
                ObjectTreeMap.CreateGraphics();
                This.ListGraphics.push({ Id: TreeMapID + "_GraphicTreeMap", Graphic: ObjectTreeMap, Data: TemplateCell, DataSetInit: This._This.ObjTemplate.ObjectQueryTemplate, DataSetColumns: ArrayElements, FilterColumn: ListTreeMapColumn[0], Type: CellGraphicType });
                TemplateCell.ObjectGraphic = { Id: TreeMapID + "_GraphicTreeMap", Graphic: ObjectTreeMap, Data: TemplateCell, DataSetInit: This._This.ObjTemplate.ObjectQueryTemplate, DataSetColumns: ArrayElements, FilterColumn: ListTreeMapColumn[0], Type: CellGraphicType };
            }
            else if (ListTreeMapColumn.length > 1) {
                // PARA CUANDO EXISTA RELACION DE UNO A MUCHOS.
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("GraphicsPBIManagerBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIManagerBasic.prototype.Graphics  this._CreateTreeMap ", e);
            throw e;
        }
    }
    this._GetLinqTreeMap = function (ObjectQueryTemplate, Column, GraphicTreeMapID, NameValue) {
        try {
            if (NameValue === null) {
                var ObjectQueryTreeMap = SysCfg.CopyList(ObjectQueryTemplate);
                ObjectQueryTreeMap = Enumerable.From(ObjectQueryTreeMap).GroupBy(function (Element) {
                    return Element[Column.COLUMN_NAME];
                }).Select(function (Element) {
                    return {
                        Label: Element.source[0][Column.COLUMN_NAME],
                        Value: Element.Sum(function (subItem) {
                            return 1 | 0;
                        }),
                        Color: This._GetRandomColor(),
                        Id: GraphicTreeMapID
                    }
                }).ToArray();
                return (ObjectQueryTreeMap);
            }
            else {

            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("GraphicsPBIManagerBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIManagerBasic.prototype.Graphics  this._GetLinqTreeMap ", e);
            throw e;
        }
    }
    this._CreatePie = function (TemplateCell) {
        try {
            var CellGraphic = TemplateCell.Data.PBITEMPLATECELLGRAPHIC;
            var CellGraphicType = CellGraphic.PBIGRAPHICTYPE.GRAPHIC_NAME;
            var PieID = CellGraphic.IDPBITEMPLATECELLGRAPHIC.toString() + CellGraphic.IDPBITEMPLATECELLS.toString() + CellGraphic.IDPBIGRAPHICTYPE.toString() + "_" + CellGraphic.CELLGRAPHIC_TITLE + "_Cell";
            var TitlePieID = PieID + "_Title";
            This._CreateTitleCellGraphic(TemplateCell, TitlePieID);
            var ListPieColumn = CellGraphic.PBITEMPLATECELLGRAPHIC_COLUMNList;
            if (ListPieColumn.length <= 0) {
                return;
            }
            else if (ListPieColumn.length === 1) {
                var ObjectPie = new Componet.TGraphicsPBI.TVclPie(TemplateCell.Dom, PieID + "_Graphic" + CellGraphicType);
                var GraphicPieID = PieID + "_ObjectGraphic" + CellGraphicType + "_";
                var ArrayElements = This._GetLinqPieAndPieOne(This._This.ObjTemplate.ObjectQueryTemplate, ListPieColumn[0], GraphicPieID, null);
                for (var i = 0; i < ArrayElements.length; i++) {
                    var Element = new Componet.TGraphicsElements.TVclPieElements();
                    Element.DisplayValue = ArrayElements[i]["Label"];
                    Element.Value = ArrayElements[i]["Value"];
                    Element.Color = ArrayElements[i]["Color"];
                    Element.ColorHighlight.ColorElementFather.name = This._HexToRgba(ArrayElements[i]["Color"], "0.6");
                    Element.ColorHighlight.ColorElementChild.name = This._HexToRgba(ArrayElements[i]["Color"], "1");
                    Element.Text = ArrayElements[i]["Text"];
                    Element.ColorOption = ArrayElements[i]["ColorOption"];
                    Element.BorderOption = ArrayElements[i]["BorderOption"];
                    Element.DisplayOption = ArrayElements[i]["DisplayOption"];
                    Element.ColorText = ArrayElements[i]["ColorText"];
                    Element.BorderColorGraphic = ArrayElements[i]["BorderColorGraphic"];
                    Element.HoverColorGraphic = ArrayElements[i]["HoverColorGraphic"];
                    Element.Id = ArrayElements[i]["Id"];
                    ObjectPie.PieElementsAdd(Element);
                }
                if (This._Type.PieOne.name === CellGraphicType) {
                    ObjectPie.Option = false;
                    ObjectPie.HeightPlugin = "110px";
                }
                ObjectPie.CreateGraphics();
                ObjectPie.onClickPlugin = function (chartData, idx) {
                    //debugger;
                    var GraphicPie = TemplateCell.ObjectGraphic;
                    var Element = GraphicPie.Graphic.SearchElement(chartData.labels[idx]);
                    if (Element === null) {
                        Element = GraphicPie.Graphic.SearchElement(chartData.labels[idx + 1]);
                    }
                    GraphicPie.Graphic.ResetElementsHighlight();
                    if (Element.FlagClick == true) {
                        This._GraphicReset(GraphicPie);
                        Element.FlagClick = false;
                    }
                    else {
                        GraphicPie.Graphic.ResetFlagClick();
                        GraphicPie.Graphic.ResetOption();
                        var FilterColumn = GraphicPie.FilterColumn.COLUMN_NAME;
                        var GraphicLabel = Element.DisplayValue;
                        var NewArrayElements = This._This.ObjTemplate.GetFilterDataColumnTemplate(FilterColumn, GraphicLabel);
                        This._SetValuesHighlight(NewArrayElements, GraphicPie);
                        Element.FlagClick = true;
                    }
                }
                This.ListGraphics.push({ Id: PieID + "_Graphic" + CellGraphicType, Graphic: ObjectPie, Data: TemplateCell, DataSetInit: This._This.ObjTemplate.ObjectQueryTemplate, DataSetColumns: ArrayElements, FilterColumn: ListPieColumn[0], Type: CellGraphicType });
                TemplateCell.ObjectGraphic = { Id: PieID + "_Graphic" + CellGraphicType, Graphic: ObjectPie, Data: TemplateCell, DataSetInit: This._This.ObjTemplate.ObjectQueryTemplate, DataSetColumns: ArrayElements, FilterColumn: ListPieColumn[0], Type: CellGraphicType };
            }
            else if (ListCardlColumn.length > 1) {
                // PARA CUANDO EXISTA RELACION DE UNO A MUCHOS.
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("GraphicsPBIManagerBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIManagerBasic.prototype.Graphics  this._CreatePie ", e);
            throw e;
        }
    }
    this._GetLinqPieAndPieOne = function (ObjectQueryTemplate, Column, GraphicPieID, NameValue) {
        try {
            if (NameValue === null) {
                var ObjectQueryPieAndPieOne = SysCfg.CopyList(ObjectQueryTemplate);
                ObjectQueryPieAndPieOne = Enumerable.From(ObjectQueryPieAndPieOne).GroupBy(function (Element) {
                    return Element[Column.COLUMN_NAME];
                }).Select(function (Element) {
                    var Color = This._GetRandomColor();
                    return {
                        Label: Element.source[0][Column.COLUMN_NAME],
                        Value: Element.Sum(function (subItem) {
                            return 1 | 0;
                        }),
                        Color: Color,
                        Text: "Cantidad",
                        ColorOption: "white",
                        BorderOption: "#00a98f",
                        DisplayOption: true,
                        ColorText: "black",
                        BorderColorGraphic: Color,
                        HoverColorGraphic: Color,
                        Id: GraphicPieID
                    }
                }).ToArray();
                return (ObjectQueryPieAndPieOne);
            }
            else {

            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("GraphicsPBIManagerBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIManagerBasic.prototype.Graphics  this._GetLinqPieAndPieOne ", e);
            throw e;
        }
    }
    this._CreateTable = function (TemplateCell) {
        try {
            var CellGraphic = TemplateCell.Data.PBITEMPLATECELLGRAPHIC;
            var CellGraphicType = CellGraphic.PBIGRAPHICTYPE.GRAPHIC_NAME;
            var TableID = CellGraphic.IDPBITEMPLATECELLGRAPHIC.toString() + CellGraphic.IDPBITEMPLATECELLS.toString() + CellGraphic.IDPBIGRAPHICTYPE.toString() + "_" + CellGraphic.CELLGRAPHIC_TITLE + "_Cell";
            var TitleTableID = TableID + "_Title";
            This._CreateTitleCellGraphic(TemplateCell, TitleTableID);
            var ListTableColumn = CellGraphic.PBITEMPLATECELLGRAPHIC_COLUMNList;
            if (ListTableColumn.length <= 0) {
                return;
            }
            else if (ListTableColumn.length >= 0) {
                var ObjectTable = new Componet.TGraphicsPBI.TVclTable(TemplateCell.Dom, TableID + "_GraphicTable");
                var GraphicTableID = TableID + "_ObjectGraphicTable_";
                var ArrayElements = This._GetLinqTable(This._This.ObjTemplate.ObjectQueryTemplate, ListTableColumn, GraphicTableID, null);
                for (var i = 0; i < ArrayElements.length; i++) {
                    var Element = new Componet.TGraphicsElements.TVclGraphicElementTable();
                    //Element.Id = ArrayElements[i].id;
                    Element.Array = ArrayElements[i];
                    Element.onClick = function (inElement) {
                        var GraphicTable = TemplateCell.ObjectGraphic;
                        GraphicTable.Graphic.ResetElementsHighlight();
                        if (inElement.FlagClick == true) {
                            This._GraphicReset(GraphicTable);
                            inElement.FlagClick = false;
                        }
                        else {
                            GraphicTable.Graphic.ResetFlagClick();
                            var NewArrayElements = new Array();
                            NewArrayElements.push(inElement.Array);
                            This._SetValuesHighlight(NewArrayElements, GraphicTable);
                            inElement.FlagClick = true;
                        }
                    };
                    ObjectTable.TVclGraphicElementsAdd(Element);
                }
                ObjectTable.CreateGraphic();
                This.ListGraphics.push({ Id: TableID + "_GraphicTable", Graphic: ObjectTable, Data: TemplateCell, DataSetInit: This._This.ObjTemplate.ObjectQueryTemplate, DataSetColumns: ArrayElements, FilterColumn: ListTableColumn, Type: CellGraphicType });
                TemplateCell.ObjectGraphic = { Id: TableID + "_GraphicTable", Graphic: ObjectTable, Data: TemplateCell, DataSetInit: This._This.ObjTemplate.ObjectQueryTemplate, DataSetColumns: ArrayElements, FilterColumn: ListTableColumn, Type: CellGraphicType };
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("GraphicsPBIManagerBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIManagerBasic.prototype.Graphics  this._CreateTable ", e);
            throw e;
        }
    }
    this._GetLinqTable = function (ObjectQueryTemplate, Column, GraphicTableID, NameValue) {
        try {
            if (NameValue === null) {
                var ObjectQueryTable = SysCfg.CopyList(ObjectQueryTemplate);
                ObjectQueryTable = Enumerable.From(ObjectQueryTable).Select(function (Element) {
                    var ObjTemp = {};
                    for (var i = 0; i < Column.length; i++) {
                        var prop = Column[i].COLUMN_NAME;
                        ObjTemp[prop] = Element[prop];
                    }
                    return ObjTemp
                }).ToArray();
                return (ObjectQueryTable);
            }
            else {

            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("GraphicsPBIManagerBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIManagerBasic.prototype.Graphics  this._GetLinqTable ", e);
            throw e;
        }
    }
    this._CreateFunnel = function (TemplateCell) {
        try {
            var CellGraphic = TemplateCell.Data.PBITEMPLATECELLGRAPHIC;
            var CellGraphicType = CellGraphic.PBIGRAPHICTYPE.GRAPHIC_NAME;
            var FunnelID = CellGraphic.IDPBITEMPLATECELLGRAPHIC.toString() + CellGraphic.IDPBITEMPLATECELLS.toString() + CellGraphic.IDPBIGRAPHICTYPE.toString() + "_" + CellGraphic.CELLGRAPHIC_TITLE + "_Cell";
            var TitleFunnelID = FunnelID + "_Title";
            This._CreateTitleCellGraphic(TemplateCell, TitleFunnelID);
            var ListFunnelColumn = CellGraphic.PBITEMPLATECELLGRAPHIC_COLUMNList;
            if (ListFunnelColumn.length <= 0) {
                return;
            }
            else if (ListFunnelColumn.length === 1) {
                var ObjectFunnel = new Componet.TGraphicsPBI.TVclFunnel(TemplateCell.Dom, FunnelID + "_GraphicFunnel");
                var GraphicFunnelID = FunnelID + "_ObjectGraphicFunnel_";
                var ArrayElements = This._GetLinqFunnel(This._This.ObjTemplate.ObjectQueryTemplate, ListFunnelColumn[0], GraphicFunnelID, null);
                for (var i = 0; i < ArrayElements.length; i++) {
                    var Element = new Componet.TGraphicsElements.TVclGraphicElementFunnel();
                    Element.Label = ArrayElements[i]["Label"];
                    Element.Value = ArrayElements[i]["Value"];
                    Element.Color = This._HexToRgb(ArrayElements[i]["Color"]);
                    Element.Id = ArrayElements[i]["Id"];
                    Element.onClick = function (Element) {
                        var GraphicFunnel = TemplateCell.ObjectGraphic;
                        GraphicFunnel.Graphic.ResetElementsHighlight();
                        if (Element.FlagClick == true) {
                            This._GraphicReset(GraphicFunnel);
                            Element.FlagClick = false;
                        }
                        else {
                            GraphicFunnel.Graphic.ResetFlagClick();
                            var FilterColumn = GraphicFunnel.FilterColumn.COLUMN_NAME;
                            var GraphicLabel = Element.Label; var NewArrayElements = This._This.ObjTemplate.GetFilterDataColumnTemplate(FilterColumn, GraphicLabel);
                            This._SetValuesHighlight(NewArrayElements, GraphicFunnel);
                            Element.FlagClick = true;
                        }
                    };
                    ObjectFunnel.TVclGraphicElementsAdd(Element);
                }
                ObjectFunnel.CreateGraphic();
                This.ListGraphics.push({ Id: FunnelID + "_GraphicFunnel", Graphic: ObjectFunnel, Data: TemplateCell, DataSetInit: This._This.ObjTemplate.ObjectQueryTemplate, DataSetColumns: ArrayElements, FilterColumn: ListFunnelColumn[0], Type: CellGraphicType, FlagClick: false });
                TemplateCell.ObjectGraphic = { Id: FunnelID + "_GraphicFunnel", Graphic: ObjectFunnel, Data: TemplateCell, DataSetInit: This._This.ObjTemplate.ObjectQueryTemplate, DataSetColumns: ArrayElements, FilterColumn: ListFunnelColumn[0], Type: CellGraphicType, FlagClick: false };
            }
            else if (ListFunnelColumn.length > 1) {
                // PARA CUANDO EXISTA RELACION DE UNO A MUCHOS.
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("GraphicsPBIManagerBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIManagerBasic.prototype.Graphics  this._CreateTable this._CreateFunnel", e);
            throw e;
        }
    }
    this._GetLinqFunnel = function (ObjectQueryTemplate, Column, GraphicFunnelID, NameValue) {
        try {
            if (NameValue === null) {
                var ObjectQueryFunnel = SysCfg.CopyList(ObjectQueryTemplate);
                ObjectQueryFunnel = Enumerable.From(ObjectQueryFunnel).GroupBy(function (Element) {
                    return Element[Column.COLUMN_NAME];
                }).Select(function (Element) {
                    return {
                        Label: Element.source[0][Column.COLUMN_NAME],
                        Value: Element.Sum(function (subItem) {
                            return 1 | 0;
                        }),
                        Color: This._GetRandomColor(),
                        Id: GraphicFunnelID
                    }
                }).ToArray();
                return (ObjectQueryFunnel);
            }
            else {

            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("GraphicsPBIManagerBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIManagerBasic.prototype.Graphics  this._GetLinqFunnel", e);
            throw e;
        }
    }
    this._CreateCard = function (TemplateCell) {
        try {
            var CellGraphic = TemplateCell.Data.PBITEMPLATECELLGRAPHIC;
            var CellGraphicType = CellGraphic.PBIGRAPHICTYPE.GRAPHIC_NAME;
            var CardlID = CellGraphic.IDPBITEMPLATECELLGRAPHIC.toString() + CellGraphic.IDPBITEMPLATECELLS.toString() + CellGraphic.IDPBIGRAPHICTYPE.toString() + "_" + CellGraphic.CELLGRAPHIC_TITLE + "_Cell";
            var TitleCardID = CardlID + "_Title";
            This._CreateTitleCellGraphic(TemplateCell, TitleCardID);
            var ListCardColumn = CellGraphic.PBITEMPLATECELLGRAPHIC_COLUMNList;
            if (ListCardColumn.length <= 0) {
                return;
            }
            else if (ListCardColumn.length === 1) {
                var ObjectCard = new Componet.TGraphicsPBI.TVclCard(TemplateCell.Dom, CardlID + "_GraphicCard");
                var GraphicCardlID = CardlID + "_ObjectGraphicCard_";
                var ArrayElements = This._GetLinqCard(This._This.ObjTemplate.ObjectQueryTemplate, ListCardColumn[0], GraphicCardlID, null);
                for (var i = 0; i < ArrayElements.length; i++) {
                    var Element = new Componet.TGraphicsElements.TVclCardElements();
                    Element.DisplayValue = ArrayElements[i]["Label"];
                    Element.Value = ArrayElements[i]["Value"];
                    Element.Text = ArrayElements[i]["Text"];
                    Element.Color = ArrayElements[i]["Color"];
                    Element.ColorText = ArrayElements[i]["ColorText"];
                    Element.ColorValue = ArrayElements[i]["ColorValue"];
                    Element.ColorDisplay = ArrayElements[i]["ColorDisplay"];
                    Element.Border = ArrayElements[i]["Border"];
                    Element.Display = ArrayElements[i]["Display"];
                    Element.Id = ArrayElements[i]["Id"];
                    ObjectCard.CardElementsAdd(Element);
                }
                ObjectCard.CreateCards();
                This.ListGraphics.push({ Id: CardlID + "_GraphicCard", Graphic: ObjectCard, Data: TemplateCell, DataSetInit: This._This.ObjTemplate.ObjectQueryTemplate, DataSetColumns: ArrayElements, FilterColumn: ListCardColumn[0], Type: CellGraphicType });
                TemplateCell.ObjectGraphic = { Id: CardlID + "_GraphicCard", Graphic: ObjectCard, Data: TemplateCell, DataSetInit: This._This.ObjTemplate.ObjectQueryTemplate, DataSetColumns: ArrayElements, FilterColumn: ListCardColumn[0], Type: CellGraphicType };
            }
            else if (ListCardColumn.length > 1) {
                // PARA CUANDO EXISTA RELACION DE UNO A MUCHOS.
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("GraphicsPBIManagerBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIManagerBasic.prototype.Graphics  this._CreateCard", e);
            throw e;
        }
    }
    this._GetLinqCard = function (ObjectQueryTemplate, Column, GraphicCardID, NameValue) {
        try {
            if (NameValue === null) {
                var ObjectQueryCard = SysCfg.CopyList(ObjectQueryTemplate);
                ObjectQueryCard = Enumerable.From(ObjectQueryCard).GroupBy(function (Element) {
                    return Element[Column.COLUMN_NAME];
                }).Select(function (Element) {
                    return {
                        Label: 'TOTAL',
                        Value: Element.Sum(function (subItem) {
                            return 1 | 0;
                        }),
                        Text: Element.source[0][Column.COLUMN_NAME],
                        Color: "rgba(185, 229, 243, 0.26)",
                        ColorText: ":#6a737b",
                        ColorValue: "black",
                        ColorDisplay: "#00a98f",
                        Border: "#00a98f",
                        Display: true,
                        Id: GraphicCardID
                    }
                }).ToArray();
                return (ObjectQueryCard);
            }
            else {

            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("GraphicsPBIManagerBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIManagerBasic.prototype.Graphics  this._GetLinqCard", e);
            throw e;
        }
    }
    this._CreateTitleCellGraphic = function (TemplateCell, TitleLogoID) {
        try {
            var DataCell = TemplateCell.Data.PBITEMPLATECELLGRAPHIC;
            if (DataCell.CELLGRAPHIC_VISIBLETITLE) {
                var Title = Uh2(TemplateCell.Dom, TitleLogoID, DataCell.CELLGRAPHIC_TITLE);
                Title.style.textAlign = DataCell.CELLGRAPHIC_ALIGNTITLE;
                Title.style.color = DataCell.CELLGRAPHIC_COLORTITLE;
                Title.style.fontSize = DataCell.CELLGRAPHIC_SIZETITLE;
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("GraphicsPBIManagerBasic.js this._CreateTitleCellGraphic this._CreateCard", e);
            throw e;
        }
    }
    this._GetRandomColor = function () {
        if (This._ArrayColors.length === 0) {
            This._ArrayColors = This._TempColors;
            This._TempColors = new Array();
        }
        var randomValue = Math.floor((Math.random() * This._ArrayColors.length));
        var randomColor = This._ArrayColors[randomValue];
        This._ArrayColors.splice(randomValue, 1);
        This._TempColors.push(randomColor);
        return randomColor;
    }
    this._SetValuesHighlight = function (NewArrayElements, Graphic) {
        try {
            for (var i = 0; i < This.ListGraphics.length; i++) {
                if (Graphic.Id != This.ListGraphics[i].Id) {
                    switch (This.ListGraphics[i].Type) {
                        case This._Type.Logo.name:
                            break;
                        case This._Type.Bar.name:
                            var ArrayElements = This._GetLinqBar(NewArrayElements, This.ListGraphics[i].FilterColumn, "", null);
                            This.ListGraphics[i].Graphic.ResetFlagClick();
                            for (var x = 0; x < This.ListGraphics[i].Graphic.Elements.length; x++) {
                                This.ListGraphics[i].Graphic.Elements[x].ValueHighlight = 0;
                                This.ListGraphics[i].Graphic.Elements[x].ValueText = "0";
                                for (var y = 0; y < ArrayElements.length; y++) {
                                    if (This.ListGraphics[i].Graphic.Elements[x].DisplayValue == ArrayElements[y].Label) {
                                        This.ListGraphics[i].Graphic.Elements[x].ValueHighlight = parseInt(ArrayElements[y].Value);
                                        This.ListGraphics[i].Graphic.Elements[x].DisplayHighlight = true;
                                        This.ListGraphics[i].Graphic.Elements[x].ValueText = ArrayElements[y].Value.toString();
                                    }
                                }
                            }
                            break;
                        case This._Type.TreeMap.name:
                            var ArrayElements = This._GetLinqTreeMap(NewArrayElements, This.ListGraphics[i].FilterColumn, "", null);
                            This.ListGraphics[i].Graphic.ResetFlagClick();
                            for (var x = 0; x < This.ListGraphics[i].Graphic.Elements.length; x++) {
                                This.ListGraphics[i].Graphic.Elements[x].ValueHighlight = 0;
                                //This.ListGraphics[i].Graphic.Elements[x].DisplayHighlight = false;
                                for (var y = 0; y < ArrayElements.length; y++) {
                                    if (This.ListGraphics[i].Graphic.Elements[x].Name == ArrayElements[y].Label) {
                                        This.ListGraphics[i].Graphic.Elements[x].ValueHighlight = parseInt(ArrayElements[y].Value);
                                        This.ListGraphics[i].Graphic.Elements[x].DisplayHighlight = true;
                                    }
                                }
                            }
                            break;
                        case This._Type.Pie.name:
                            var ArrayElements = This._GetLinqPieAndPieOne(NewArrayElements, This.ListGraphics[i].FilterColumn, "", null);
                            This.ListGraphics[i].Graphic.ResetFlagClick();
                            for (var x = 0; x < This.ListGraphics[i].Graphic.Elements.length; x++) {
                                This.ListGraphics[i].Graphic.Elements[x].ValueHighlight = 0;
                                This.ListGraphics[i].Graphic.Elements[x].ValueText = "0";
                                for (var y = 0; y < ArrayElements.length; y++) {
                                    if (This.ListGraphics[i].Graphic.Elements[x].DisplayValue == ArrayElements[y].Label) {
                                        This.ListGraphics[i].Graphic.Elements[x].ValueHighlight = parseInt(ArrayElements[y].Value);
                                        This.ListGraphics[i].Graphic.Elements[x].DisplayHighlight = true;
                                        This.ListGraphics[i].Graphic.Elements[x].ValueText = ArrayElements[y].Value.toString();
                                    }
                                }
                            }
                            break;
                        case This._Type.PieOne.name:
                            var ArrayElements = This._GetLinqPieAndPieOne(NewArrayElements, This.ListGraphics[i].FilterColumn, "", null);
                            This.ListGraphics[i].Graphic.ResetFlagClick();
                            for (var x = 0; x < This.ListGraphics[i].Graphic.Elements.length; x++) {
                                This.ListGraphics[i].Graphic.Elements[x].ValueHighlight = 0;
                                for (var y = 0; y < ArrayElements.length; y++) {
                                    if (This.ListGraphics[i].Graphic.Elements[x].DisplayValue == ArrayElements[y].Label) {
                                        This.ListGraphics[i].Graphic.Elements[x].ValueHighlight = parseInt(ArrayElements[y].Value);
                                        This.ListGraphics[i].Graphic.Elements[x].DisplayHighlight = true;
                                    }
                                }
                            }
                            break;
                        case This._Type.Table.name:
                            var ArrayElements = This._GetLinqTable(NewArrayElements, This.ListGraphics[i].FilterColumn, "", null);
                            This.ListGraphics[i].Graphic.Elements = new Array();
                            for (var x = 0; x < ArrayElements.length; x++) {
                                var Element = new Componet.TGraphicsElements.TVclGraphicElementTable();
                                //Element.Id = "Element4_" + i;
                                Element.Array = ArrayElements[x];
                                This.ListGraphics[i].Graphic.TVclGraphicElementsAdd(Element);
                                This._ExecRefreshGraphicTable(Element, This.ListGraphics[i]);
                            }
                            This.ListGraphics[i].Graphic.RefreshGraphic();
                            break;
                        case This._Type.Funnel.name:
                            var ArrayElements = This._GetLinqFunnel(NewArrayElements, This.ListGraphics[i].FilterColumn, "", null);
                            This.ListGraphics[i].Graphic.ResetFlagClick();
                            for (var x = 0; x < This.ListGraphics[i].Graphic.Elements.length; x++) {
                                This.ListGraphics[i].Graphic.Elements[x].Highlight = 0;
                                for (var y = 0; y < ArrayElements.length; y++) {
                                    if (This.ListGraphics[i].Graphic.Elements[x].Label == ArrayElements[y].Label) {
                                        This.ListGraphics[i].Graphic.Elements[x].Highlight = parseInt(ArrayElements[y].Value);
                                    }
                                }
                            }
                        case This._Type.Card.name:
                            var ArrayElements = This._GetLinqCard(NewArrayElements, This.ListGraphics[i].FilterColumn, "", null);
                            for (var x = 0; x < This.ListGraphics[i].Graphic.Elements.length; x++) {
                                This.ListGraphics[i].Graphic.Elements[x].ValueText = "0";
                                for (var y = 0; y < ArrayElements.length; y++) {
                                    if (This.ListGraphics[i].Graphic.Elements[x].Text == ArrayElements[y].Text) {
                                        This.ListGraphics[i].Graphic.Elements[x].ValueText = ArrayElements[y].Value.toString();
                                    }
                                }
                            }
                            break;
                        default:
                            break;
                    }

                }
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("GraphicsPBIManagerBasic.js this._CreateTitleCellGraphic this._SetValuesHighlight", e);
            throw e;
        };
    }
    this._HexToRgba = function (hex, opacity) {
        var c;
        if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
            c = hex.substring(1).split('');
            if (c.length == 3) {
                c = [c[0], c[0], c[1], c[1], c[2], c[2]];
            }
            c = '0x' + c.join('');
            return 'rgba(' + [(c >> 16) & 255, (c >> 8) & 255, c & 255].join(',') + ',' + opacity + ')';
        }
        throw new Error('Bad Hex');
    }
    this._HexToRgb = function (hex) {
        var c;
        if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
            c = hex.substring(1).split('');
            if (c.length == 3) {
                c = [c[0], c[0], c[1], c[1], c[2], c[2]];
            }
            c = '0x' + c.join('');
            return 'rgb(' + [(c >> 16) & 255, (c >> 8) & 255, c & 255].join(',') + ')';
        }
        throw new Error('Bad Hex');
    }
    this._GraphicReset = function (Graphic) {
        try {
            for (var i = 0; i < This.ListGraphics.length; i++) {
                if (Graphic.Id != This.ListGraphics[i].Id) {
                    switch (This.ListGraphics[i].Type) {
                        case This._Type.Bar.name:
                        case This._Type.Pie.name:
                        case This._Type.PieOne.name:
                        case This._Type.TreeMap.name:
                        case This._Type.Card.name:
                        case This._Type.Funnel.name:
                            if (This.ListGraphics[i].Graphic.ResetElementsHighlight !== null) {
                                This.ListGraphics[i].Graphic.ResetElementsHighlight();
                            }
                            if (This.ListGraphics[i].Graphic.ResetOption !== null) {
                                This.ListGraphics[i].Graphic.ResetOption();
                            }
                            break;
                        case This._Type.Table.name:
                            var ArrayElements = This._GetLinqTable(This._This.ObjTemplate.ObjectQueryTemplate, This.ListGraphics[i].FilterColumn, "", null);
                            This.ListGraphics[i].Graphic.Elements = new Array();
                            for (var x = 0; x < ArrayElements.length; x++) {
                                var Element = new Componet.TGraphicsElements.TVclGraphicElementTable();
                                //Element.Id = "Element4_" + i;
                                Element.Array = ArrayElements[x];
                                This.ListGraphics[i].Graphic.TVclGraphicElementsAdd(Element);
                                This._ExecRefreshGraphicTable(Element, This.ListGraphics[i]);
                            }
                            This.ListGraphics[i].Graphic.RefreshGraphic();
                            break;
                        default:
                            break;
                    }
                }
                else if (Graphic.Type === This._Type.Table.name) {
                    var ArrayElements = This._GetLinqTable(This._This.ObjTemplate.ObjectQueryTemplate, This.ListGraphics[i].FilterColumn, "", null);
                    This.ListGraphics[i].Graphic.Elements = new Array();
                    for (var x = 0; x < ArrayElements.length; x++) {
                        var Element = new Componet.TGraphicsElements.TVclGraphicElementTable();
                        //Element.Id = "Element4_" + i;
                        Element.Array = ArrayElements[x];
                        This.ListGraphics[i].Graphic.TVclGraphicElementsAdd(Element);
                        This._ExecRefreshGraphicTable(Element, This.ListGraphics[i]);
                    }
                    This.ListGraphics[i].Graphic.RefreshGraphic();
                }
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("GraphicsPBIManagerBasic.js this._CreateTitleCellGraphic this._GraphicReset", e);
            throw e;
        }
    }
    this._ExecRefreshGraphicTable = function (Element, Graphic) {
        var Element = Element;
        var Graphic = Graphic;
        Element.onClick = function (inElement) {
            var GraphicTable = Graphic;
            GraphicTable.Graphic.ResetElementsHighlight();
            if (inElement.FlagClick == true) {
                This._GraphicReset(GraphicTable);
                inElement.FlagClick = false;
            }
            else {
                GraphicTable.Graphic.ResetFlagClick();
                var NewArrayElements = new Array();
                NewArrayElements.push(inElement.Array);
                This._SetValuesHighlight(NewArrayElements, GraphicTable);
                inElement.FlagClick = true;
            }
        };
    }
}
ItHelpCenter.GraphicsPBIManager.TGraphicsPBIManagerBasic.prototype.AddStyleFile = function (nombre, onSuccess, onError) {
    var style = document.createElement("link");
    style.rel = "stylesheet";
    style.type = "text/css";
    style.href = nombre;
    var s = document.head.appendChild(style);
    s.onload = onSuccess;
    s.onerror = onError;
}
ItHelpCenter.GraphicsPBIManager.TGraphicsPBIManagerBasic.prototype.LoadScript = function (nombre, onSuccess, onError) {
    var s = document.createElement("script");
    s.onload = onSuccess;
    s.onerror = onError;
    s.src = nombre;
    document.querySelector("head").appendChild(s);
}