ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic = function (inObjectHtml, id, inCallBack){
	this.TParent = function () {
		return this;
	}.bind(this);

	this.isInteger = function (num) {
        return (num ^ 0) === num;
    }

	var _this = this.TParent();
	this._ObjectHtml = inObjectHtml;
	this._Id = id;
	this.CallbackModalResult = inCallBack;
	this.ElementContentTemplate = null;
	this.ElementRowTemplate = new Array();
	this.ElementContentCell = null;
	this.ElementContentCellParent = null;
	this.ElementContentCellChild = new Array();
	this.ElementRowCell = new Array();
	this.ElementFormCellParent = null;
	this.ElementFormCellChild = new Array();
	this.ElementFormDesignParent = null;
	this.ElementFormDesignChild = new Array();
	this.PanelChild = null;
	this.ElementModal = null;
	_this.GraphicsPBI = new Persistence.PBITemplate.TPBITemplateProfiler();
	this.Mythis = "GraphicsPBIBasic";
	
	UsrCfg.Traslate.GetLangText(_this.Mythis, "TEMPLATE", "Template");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "CELL", "Cell");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "CELLCHILD", "Cell Child");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "ID", "Id");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "NAME", "Name");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "ENABLEDREFRESH", "Enable Refresh");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "TIMEREFRESH", "Time Refresh(sec)");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "PATH", "Path");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "DESCRIPTION", "Description");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "DATASQL", "Data SQL");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "POSITION", "Position");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "PARENT", "Template");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "DESIGN_XS", "Extra Small");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "DESIGN_S", "Small");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "DESIGN_M", "Medium");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "DESIGN_L", "Large");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "DESIGN_XL", "Extra Large");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "DESIGN_ROW", "Design Row");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "DESIGN_COLUMN", "Design Column");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "GRAPHICTYPE", "Graphic Type");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "TITLE", "Title");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "COLORTITLE", "Color Title");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "SIZETITLE", "Size Title(px)");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "ALIGNTITLE", "Align Title");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "VISIBLEGRAPHIC", "Visible Graphic");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "VISIBLETITLE", "Visible Title");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "SAVE", "Save");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "UPDATE", "Update");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "CANCEL", "Cancel");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "CONFIRMDELETE", "Do you want to delete the selected record?");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "NOCONFIRMDELETE", "No record was deleted");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "NOSELECT", "No record selected");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "NONAME", "Cannot register a user without a name");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "REPEATEDNAME", "This name already exists");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "CORRECTSAVE", "Registration saved correctly");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECTSAVE", "An error occurred while saving the record");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "CORRECTUPDATE", "The registration was updated correctly");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECTUPDATE", "An error occurred while updating a record");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "CORRECTDELETE" , "The record (s) were deleted correctly");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "NONUMBERTIME" , "The time refresh field is not a number");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "NONUMBERPOSITION" , "The position field is not a number");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "NONUMBERSIZE" , "The size title field is not a number");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "EXCESSEDTIME", "The maximum time in seconds is 86400. Please enter a valid value");
			UsrCfg.Traslate.GetLangText(_this.Mythis, "ERROR_QUERY_SQL", "An error occurred when executing the SQL query. Please check that the check is correct.");
		 

			_this.LoadScript(SysCfg.App.Properties.xRaiz + "Componet/Color/Plugin/spectrum.js", function () {
				_this.AddStyleFile(SysCfg.App.Properties.xRaiz + "Componet/Color/Plugin/spectrum.css", function () {
					_this.AddStyleFile(SysCfg.App.Properties.xRaiz + "Css/GraphicsPBI/GraphicsPBIBasic.css", function () {
						_this.Load();
                    }, function (e) {
                        SysCfg.Log.Methods.WriteLog("GraphicPBIBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic _this.AddStyleFile(" + _this.AddStyleFile + "Css/GraphicsPBI/GraphicsPBIBasic.css)", e);
                    });
                }, function (e) {
                    SysCfg.Log.Methods.WriteLog("GraphicPBIBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic _this.AddStyleFile(" + _this.AddStyleFile + "Componet/Color/Plugin/spectrum.css)", e);
                });	
            }, function (e) {
                SysCfg.Log.Methods.WriteLog("GraphicPBIBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic _this.LoadScript(" + _this.LoadScript + "Componet/Color/Plugin/spectrum.js)", e);
            });	
}

ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.Load = function (){
    var _this = this.TParent();
    try {
        _this.ListLoad();
        var btn2 = new TVclInputbutton(_this._ObjectHtml, "");
        btn2.Text = "Add All Data";
        btn2.VCLType = TVCLType.BS;
        btn2.onClick = function () {
            var Template = new Persistence.PBITemplate.TPBITemplateProfiler();
            Template.ValidationData()
            if (Template.PBITEMPLATEList.length === 0) {
                Template.CreateAllDataTables();
            }
            else {
                //Template.Fill();
                alert('Ya existen registros en la Base de Datos');
            }
        }
        btn2.This.style.marginTop = "10px";
        btn2.This.style.position = "relative";
        btn2.This.style.left = "50%";
        btn2.This.style.transform = "translate(-50%,0%)";
        _this.Content(_this._ObjectHtml, '', UsrCfg.Traslate.GetLangText(_this.Mythis, 'TEMPLATE'), '', _this.PBITEMPLATEList, null);
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicPBIBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.Load", e);
    }
}

ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.Content = function (objectHtml, id, title, name, pbiGraphicList, idControl){
	var _this = this.TParent();
	var ObjectHtml = objectHtml;
	var Id = id;
	var Title = title;
	var Name = name;
	var PBIGraphicList = pbiGraphicList;
    try {
	    if (_this.ElementContentCell != null && Title == UsrCfg.Traslate.GetLangText(_this.Mythis, 'CELL')) {
		    $(_this.ElementContentCellParent.form).remove();
		    for(var  i = 0 ; i < _this.ElementContentCellChild.length ; i++){
			    $(_this.ElementContentCellChild[i].form).remove();
		    }

		    this.ElementContentCell = null;
		    this.ElementContentCellParent = null;
		    this.ElementContentCellChild = new Array();
		    this.ElementRowCell = new Array();
		    this.ElementFormCellParent = null;
		    this.ElementFormCellChild = new Array();
		    this.ElementFormDesignParent = null;
		    this.ElementFormDesignChild = new Array();
		    this.PanelChild = null;
		    _this.ElementFormCellGraphic = null;
	    }
	    if(_this.ElementContentCellChild.length != 0 && Title == UsrCfg.Traslate.GetLangText(_this.Mythis, 'CELLCHILD')){
		    if(_this.ElementContentCellChild.length-1 > _this.PanelChild){
			    for(var i = _this.PanelChild+1; i < _this.ElementContentCellChild.length ; i++){
				    $(_this.ElementContentCellChild[i].form).remove();
			    }
		    }
	    }

	    var Container = new TVclStackPanel(ObjectHtml, id + '_' + Title, 1, [[12], [12], [12], [12], [12]]);
	    var ConLabelControl = new TVclStackPanel(Container.Column[0].This, '', 1, [[12], [12], [12], [12], [12]]);
	    ConLabelControl.Row.This.classList.add('TitleGraphicsBasic');
	    var lblTitle = new TVcllabel(ConLabelControl.Column[0].This, '', TlabelType.H0);
	    (Name != '') ? lblTitle.Text = Title + ': ' + Name : lblTitle.Text = Title;
	    lblTitle.VCLType = TVCLType.BS;

	    var FormContainer = new TVclStackPanel(Container.Column[0].This, '', 2, [[12, 12], [12, 12], [5, 7], [5, 7], [4, 8]]);
	    var TitleGrid = new TGrid(FormContainer.Column[0].This, '', '');
	    var Col0 = new TColumn();
	    Col0.Name = "";
	    Col0.Caption = "Name";
	    Col0.Index = 0;
	    Col0.EnabledEditor = false;
	    Col0.DataType = SysCfg.DB.Properties.TDataType.String;
	    Col0.ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.None;
	    Col0.This.classList.add('col-xs-12');
	    TitleGrid.AddColumn(Col0);

	    if (Title == UsrCfg.Traslate.GetLangText(_this.Mythis, 'TEMPLATE')) {
		    Container.Row.This.classList.add('ContainerTemplate');
		    _this.ElementContentTemplate = { table: TitleGrid, formText: FormContainer.Column[1].This };
		    _this.AddRowTemplate();
		    TitleGrid.This.classList.add('TableTemplate');
		    TitleGrid.This.classList.add('table-fixed');
		    _this.CreateFormTemplate(FormContainer.Column[1].This);
		    if(Source.Menu.IsMobil){
				Container.Row.This.style.marginLeft = '0px';
				Container.Row.This.style.marginRight = '0px';
				Container.Row.This.style.paddingLeft = '0px';
			}
	    }
	    else if (Title == UsrCfg.Traslate.GetLangText(_this.Mythis, 'CELL')) {
		    Container.Row.This.classList.add('ContainerCell');
		    TitleGrid.ResponsiveCont.classList.remove('table-responsive');
		    _this.ElementContentCell = { form: Container.Row.This, table: TitleGrid, title: lblTitle, formText: FormContainer.Column[1].This, idCell: idControl };
		    _this.ElementContentCellParent = _this.ElementContentCell;
		    _this.AddRowCell(PBIGraphicList, false);
		    TitleGrid.This.classList.add('TableCell');
		    TitleGrid.This.classList.add('table-fixed');
		    _this.CreateFormCell(FormContainer.Column[1].This, idControl);
		    if(Source.Menu.IsMobil){
				Container.Row.This.style.marginLeft = '0px';
				Container.Row.This.style.marginRight = '0px';
				Container.Row.This.style.paddingLeft = '0px';
			}
	    }
	    else if (Title == UsrCfg.Traslate.GetLangText(_this.Mythis, 'CELLCHILD')) {
		    Container.Row.This.classList.add('ContainerCell');
		    TitleGrid.ResponsiveCont.classList.remove('table-responsive');
		    _this.ElementContentCell = { form: Container.Row.This, table: TitleGrid, title: lblTitle, formText: FormContainer.Column[1].This, idCell: idControl };
		    _this.ElementContentCellChild.push(_this.ElementContentCell);
		    _this.AddRowCell(PBIGraphicList, false);
		    TitleGrid.This.classList.add('TableCell');
		    TitleGrid.This.classList.add('table-fixed');
		    _this.CreateFormCell(FormContainer.Column[1].This, idControl);
		    if(Source.Menu.IsMobil){
				Container.Row.This.style.marginLeft = '0px';
				Container.Row.This.style.marginRight = '0px';
				Container.Row.This.style.paddingLeft = '0px';
			}
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicPBIBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.Content", e);
    }
}

ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.CreateFormTemplate = function (objectHtml){
	var _this = this.TParent();
	_this.ElementFormTemplate = null;
    var ObjectHtml = objectHtml;
    try {
	    var ContImages = new TVclStackPanel(ObjectHtml, "", 1, [[12], [12], [12], [12], [12]]);
	    ContImages.Row.This.classList.add("DownSpace");
	    //Delete
	    var ImageDelete = new TVclImagen(ContImages.Column[0].This, "");
	    ImageDelete.This.style.float = "right";
	    ImageDelete.This.style.marginLeft = "5px";
	    ImageDelete.Title = "Delete";
	    ImageDelete.Src = "image/16/delete.png";
	    ImageDelete.Cursor = "pointer";
	    ImageDelete.onClick = function () {
		    if (txtId.Text != '') {
			    var confirmed = confirm(UsrCfg.Traslate.GetLangText(_this.Mythis, 'CONFIRMDELETE'));
			    if (confirmed) {
				    _this.DeleteTemplate(parseInt(txtId.Text));
			    }
			    else {
				    alert(UsrCfg.Traslate.GetLangText(_this.Mythis, 'NOCONFIRMDELETE'));
			    }
		    }
		    else {
			    alert(UsrCfg.Traslate.GetLangText(_this.Mythis, 'NOSELECT'));
		    }
	    }
	    //Update
	    var ImageUpdate = new TVclImagen(ContImages.Column[0].This, "");
	    ImageUpdate.This.style.marginLeft = "5px";
	    ImageUpdate.This.style.float = "right";
	    ImageUpdate.Title = "Edit";
	    ImageUpdate.Src = "image/16/edit.png";
	    ImageUpdate.Cursor = "pointer";
	    ImageUpdate.onClick = function () {
		    if (txtId.Text != '') {
			    txtName.This.disabled = false;
			    txtTime.This.disabled = false;
			    txtPath.This.disabled = false;
			    ListBoxEnable.ListBoxItems[0].This.Column[0].This.children[0].removeAttribute('disabled','');
			    memoDescription.This.disabled = false;
			    memoDataSQL.This.disabled = false;
			    btnAdd.This.style.display = "none";
			    btnUpdate.This.style.display = "block";
			    btnCancel.This.style.visibility = 'visible';
		    }
		    else {
			    txtName.This.disabled = true;
			    txtTime.This.disabled = true;
			    txtPath.This.disabled = true;
			    ListBoxEnable.ListBoxItems[0].This.Column[0].This.children[0].setAttribute('disabled','');
			    memoDescription.This.disabled = true;
			    memoDataSQL.This.disabled = true;
			    alert(UsrCfg.Traslate.GetLangText(_this.Mythis, 'NOSELECT'));
			    btnAdd.This.style.display = "none";
			    btnUpdate.This.style.display = "none";
			    btnCancel.This.style.visibility = 'hidden';
		    }
	    }
	    //Add
	    var ImageAdd = new TVclImagen(ContImages.Column[0].This, "");
	    ImageAdd.This.style.marginLeft = "5px";
	    ImageAdd.This.style.float = "right";
	    ImageAdd.Title = "Add";
	    ImageAdd.Src = "image/16/add.png";
	    ImageAdd.Cursor = "pointer";
	    ImageAdd.onClick = function () {
		    txtId.Text = '';
		    txtName.Text = '';
		    txtTime.Text = '0';
		    txtPath.Text = '';
		    ListBoxEnable.ListBoxItems[0].Checked = false;
		    memoDescription.Text = '';
		    memoDataSQL.Text = '';
		    txtName.This.disabled = false;
		    txtTime.This.disabled = false;
		    txtPath.This.disabled = false;
		    ListBoxEnable.ListBoxItems[0].This.Column[0].This.children[0].removeAttribute('disabled','');
		    memoDescription.This.disabled = false;
		    memoDataSQL.This.disabled = false;
		    btnUpdate.This.style.display = 'none';
		    btnAdd.This.style.display = "block";
		    btnCancel.This.style.visibility = 'visible';
		    if (_this.ElementContentCell != null) {
			    $(_this.ElementContentCellParent.form).remove();
			    for(var  i = 0 ; i < _this.ElementContentCellChild.length ; i++){
				    $(_this.ElementContentCellChild[i].form).remove();
			    }
		    }
	    }
	    //Preview
	    var ImagePreview = new TVclImagen(ContImages.Column[0].This, "");
	    ImagePreview.This.style.float = "right";
	    ImagePreview.Title = "Preview";
	    ImagePreview.Src = "image/16/Computer.png";
	    ImagePreview.Cursor = "pointer";
	    ImagePreview.onClick = function () {
		    /*Preview*/
		    _this.ShowPreview(parseInt(_this.ElementFormTemplate.id.Text), _this.ElementFormTemplate.name.Text);
	    }

	    var ContTextId = new TVclStackPanel(ObjectHtml, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	    ContTextId.Row.This.classList.add('DownSpace');
	    var lblId = new TVcllabel(ContTextId.Column[0].This, "", TlabelType.H0);
	    lblId.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ID");
	    lblId.VCLType = TVCLType.BS;
	    var txtId = new TVclTextBox(ContTextId.Column[1].This, "txtIDTEMPLATE");
	    txtId.Text = "";
	    txtId.This.disabled = true;
	    txtId.VCLType = TVCLType.BS;

	    var ContTextName = new TVclStackPanel(ObjectHtml, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	    ContTextName.Row.This.classList.add('DownSpace');
	    var lblName = new TVcllabel(ContTextName.Column[0].This, "", TlabelType.H0);
	    lblName.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "NAME");
	    lblName.VCLType = TVCLType.BS;
	    var txtName = new TVclTextBox(ContTextName.Column[1].This, "txtNAMETEMPLATE");
	    txtName.Text = "";
	    txtName.This.disabled = true;
	    txtName.VCLType = TVCLType.BS;

	    var ContEnabledRefresh = new TVclStackPanel(ObjectHtml, "", 1, [[8], [8], [8], [8], [9]]);
	    ContEnabledRefresh.Row.This.classList.add('DownSpace');
	    ContEnabledRefresh.Row.This.children[0].classList.add('col-lg-offset-4');
	    ContEnabledRefresh.Row.This.children[0].classList.add('col-xl-offset-3');
	    var ListBoxEnable = new TVclListBox(ContEnabledRefresh.Column[0].This, "");
	    ListBoxEnable.EnabledCheckBox = true;
	    var ListBoxItem_Enable = new TVclListBoxItem();
	    ListBoxItem_Enable.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ENABLEDREFRESH");
	    ListBoxItem_Enable.Index = 0;
	    ListBoxEnable.AddListBoxItem(ListBoxItem_Enable);
	    ListBoxEnable.ListBoxItems[0].This.Column[0].This.children[0].setAttribute('disabled','');

	    var ContTimeRefresh = new TVclStackPanel(ObjectHtml, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	    ContTimeRefresh.Row.This.classList.add('DownSpace');
	    var lblTime = new TVcllabel(ContTimeRefresh.Column[0].This, "", TlabelType.H0);
	    lblTime.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TIMEREFRESH");
	    lblTime.VCLType = TVCLType.BS;
	    var txtTime = new TVclTextBox(ContTimeRefresh.Column[1].This, "txtTIMETEMPLATE");
	    txtTime.Text = "0";
	    txtTime.This.disabled = true;
	    txtTime.VCLType = TVCLType.BS;
	    txtTime.This.setAttribute("type", "number");
	    txtTime.This.setAttribute("min", "0");

	    var ContPath = new TVclStackPanel(ObjectHtml, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	    ContPath.Row.This.classList.add('DownSpace');
	    var lblPath = new TVcllabel(ContPath.Column[0].This, "", TlabelType.H0);
	    lblPath.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "PATH");
	    lblPath.VCLType = TVCLType.BS;
	    var txtPath = new TVclTextBox(ContPath.Column[1].This, "txtPATHTEMPLATE");
	    txtPath.Text = "";
	    txtPath.This.disabled = true;
	    txtPath.VCLType = TVCLType.BS;

	    var ContDescription = new TVclStackPanel(ObjectHtml, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	    ContDescription.Row.This.classList.add('DownSpace');
	    var lblDescription = new TVcllabel(ContDescription.Column[0].This, "", TlabelType.H0);
	    lblDescription.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "DESCRIPTION");
	    lblDescription.VCLType = TVCLType.BS;
	    var memoDescription = new TVclMemo(ContDescription.Column[1].This, "txtDESCRIPTIONTEMPLATE");
	    memoDescription.VCLType = TVCLType.BS;
	    memoDescription.This.removeAttribute("cols");
	    memoDescription.This.removeAttribute("rows");
	    memoDescription.This.classList.add("form-control");
	    memoDescription.This.disabled = true;
	    memoDescription.Text = '';

	    var ContDataSQL = new TVclStackPanel(ObjectHtml, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	    ContDataSQL.Row.This.classList.add('DownSpace');
	    var lblDataSQL = new TVcllabel(ContDataSQL.Column[0].This, "", TlabelType.H0);
	    lblDataSQL.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "DATASQL");
	    lblDataSQL.VCLType = TVCLType.BS;
	    var memoDataSQL = new TVclMemo(ContDataSQL.Column[1].This, "txtDATASQLTIONTEMPLATE");
	    memoDataSQL.VCLType = TVCLType.BS;
	    memoDataSQL.This.removeAttribute("cols");
	    memoDataSQL.This.removeAttribute("rows");
	    memoDataSQL.This.classList.add("form-control");
	    memoDataSQL.This.disabled = true;
	    memoDataSQL.Text = '';

	    var ContButton = new TVclStackPanel(ObjectHtml, "", 2, [[12, 12], [12, 12], [12, 12], [12, 12], [12, 12]]);
	    ContButton.Row.This.classList.add("DownSpace");

	    var btnCancel = new TVclInputbutton(ContButton.Column[0].This, "");
	    btnCancel.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "CANCEL");
	    btnCancel.This.style.visibility = 'hidden'
	    btnCancel.VCLType = TVCLType.BS;
	    btnCancel.onClick = function () {
		    txtName.This.disabled = true;
		    txtTime.This.disabled = true;
		    txtPath.This.disabled = true;
		    ListBoxEnable.ListBoxItems[0].Checked = false;
		    ListBoxEnable.ListBoxItems[0].This.Column[0].This.children[0].setAttribute('disabled','');
		    memoDescription.This.disabled = true;
		    memoDataSQL.This.disabled = true;
		    btnAdd.This.style.display = 'none';
		    btnUpdate.This.style.display = 'none';
		    btnCancel.This.style.visibility = 'hidden'
	    }
	    btnCancel.This.style.float = "right";
	    btnCancel.This.style.marginLeft = "10px";

	    var btnAdd = new TVclInputbutton(ContButton.Column[0].This, "");
	    btnAdd.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "SAVE");
	    btnAdd.This.style.display = "none";
	    btnAdd.VCLType = TVCLType.BS;
	    btnAdd.onClick = function () {
		    if (txtName.Text.trim() != ''){
			    /*Evitar nombres repetidos*/
			    var stateName = true;
			    for(var i = 0 ; i < _this.PBITEMPLATEList.length ; i++){
				    if(_this.PBITEMPLATEList[i].TEMPLATE_NAME.toLowerCase() === txtName.Text.trim().toLowerCase()){
					    stateName = false;
				    }
			    }
			    if(stateName){
				    if(_this.isInteger(parseInt(txtTime.Text))){
					    if(parseInt(txtTime.Text) <= 86400){
						    var objectState = _this.getQuery(memoDataSQL.Text.trim());
						    if(objectState.Status){
							    _this.SaveTemplate(txtName.Text.trim(), parseInt(txtTime.Text), ListBoxEnable.ListBoxItems[0].Checked, txtPath.Text.trim(), memoDescription.Text.trim(), memoDataSQL.Text.trim());
							    btnAdd.This.style.display = 'none';
							    btnCancel.This.style.visibility = 'hidden'
						    }else{
							    alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ERROR_QUERY_SQL"));
						    }
					    }else{
						    alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "EXCESSEDTIME"));
					    }
				    }else{
					    alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NONUMBERTIME"));
				    }
			    }else{
				    alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "REPEATEDNAME"));
			    }
		    }
		    else {
			    alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NONAME"));
		    }
	    }
	    btnAdd.This.style.float = "right";

	    var btnUpdate = new TVclInputbutton(ContButton.Column[0].This, "");
	    btnUpdate.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "UPDATE");
	    btnUpdate.This.style.display = "none";
	    btnUpdate.VCLType = TVCLType.BS;
	    btnUpdate.onClick = function () {
		    if (txtName.Text.trim() != '') {
			    /*Evitar nombres repetidos*/
			    var stateName = true;
			    for(var i = 0 ; i < _this.PBITEMPLATEList.length ; i++){
				    if(_this.PBITEMPLATEList[i].IDPBITEMPLATE !== parseInt(txtId.Text)){
					    if(_this.PBITEMPLATEList[i].TEMPLATE_NAME.toLowerCase() === txtName.Text.trim().toLowerCase()){
						    stateName = false;
					    }
				    }
			    }
			    if(stateName){
				    if(_this.isInteger(parseInt(txtTime.Text))){
					    if(parseInt(txtTime.Text) <= 86400){
						    var objectState = _this.getQuery(memoDataSQL.Text.trim());
						    if(objectState.Status){
							    _this.UpdateTemplate(parseInt(txtId.Text), txtName.Text.trim(), parseInt(txtTime.Text), ListBoxEnable.ListBoxItems[0].Checked, txtPath.Text.trim(), memoDescription.Text.trim(), memoDataSQL.Text.trim());
							    btnUpdate.This.style.display = 'none';
							    btnCancel.This.style.visibility = 'hidden'
						    }else{
							    alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "ERROR_QUERY_SQL"));
						    }
					    }else{
						    alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "EXCESSEDTIME"));
					    }
				    }else{
					    alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NONUMBERTIME"));
				    }
			    }else{
				    alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "REPEATEDNAME"));
			    }
		    }
		    else {
			    alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NONAME"));
		    }
	    }
	    btnUpdate.This.style.float = "right";

	    /*Bloquear los botones update y delete si no hay registros en la tabla*/
	    if (_this.ElementContentTemplate.table.ResponsiveCont.style.display == 'none') {
		    ImageUpdate.This.style.display = 'none';
		    ImageDelete.This.style.display = 'none';
		    ImagePreview.This.style.display = 'none';
	    }
	    _this.ElementFormTemplate = { id: txtId, name: txtName, time: txtTime, enabled: ListBoxEnable.ListBoxItems, path: txtPath, description: memoDescription, dataSQL: memoDataSQL, save: btnAdd, update: btnUpdate, cancel: btnCancel, iupdate: ImageUpdate, idelete: ImageDelete, ipreview: ImagePreview };	
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicPBIBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.CreateFormTemplate", e);
    }
}

ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.AddRowTemplate = function (){
    var _this = this.TParent();
    try {
	    if (_this.PBITEMPLATEList.length != 0) {
		    for (var i = 0; i < _this.PBITEMPLATEList.length; i++) {
			    var NewRow = new TRow();
			    var Cell0 = new TCell();
			    Cell0.Value = _this.PBITEMPLATEList[i].TEMPLATE_NAME;
			    Cell0.IndexColumn = 0;
			    Cell0.IndexRow = i;
			    NewRow.AddCell(Cell0);
			    Cell0.This.setAttribute('data-id', _this.PBITEMPLATEList[i].IDPBITEMPLATE);
			    Cell0.This.classList.add('col-xs-12'); //Eliminar en caso no funcione el fixed
			    _this.ElementRowTemplate[i] = { row: Cell0.This };
			    _this.ElementContentTemplate.table.AddRow(NewRow);
			    _this.ElementRowTemplate[i].row.onclick = function () {
				    _this.ElementFormTemplate.id.This.disabled = true;
				    _this.ElementFormTemplate.name.This.disabled = true;
				    _this.ElementFormTemplate.time.This.disabled = true;
				    _this.ElementFormTemplate.enabled[0].This.Column[0].This.children[0].setAttribute('disabled','');
				    _this.ElementFormTemplate.path.This.disabled = true;
				    _this.ElementFormTemplate.description.This.disabled = true;
				    _this.ElementFormTemplate.dataSQL.This.disabled = true;
				    _this.ElementFormTemplate.save.This.style.display = 'none';
				    _this.ElementFormTemplate.update.This.style.display = 'none';
				    _this.ElementFormTemplate.cancel.This.style.visibility = 'hidden';
				    _this.FillTemplate(parseInt(this.dataset.id));
			    }
		    }
		    if (_this.ElementFormTemplate != null) {
		       _this.ElementFormTemplate.iupdate.This.style.display = 'block';
		       _this.ElementFormTemplate.idelete.This.style.display = 'block';
		       _this.ElementFormTemplate.ipreview.This.style.display = 'block';
		    }
		    _this.ElementContentTemplate.table.ResponsiveCont.style.display = 'block';
		    _this.ElementContentTemplate.table.EnabledResizeColumn = false;
		    _this.ElementContentTemplate.formText.classList.remove('col-md-offset-2');
	    }
	    else {
		    if (_this.ElementFormTemplate != null) {
		       _this.ElementFormTemplate.iupdate.This.style.display = 'none';
		       _this.ElementFormTemplate.idelete.This.style.display = 'none';
		       _this.ElementFormTemplate.ipreview.This.style.display = 'none';
		    }
		    _this.ElementContentTemplate.table.ResponsiveCont.style.display = 'none';
		    _this.ElementContentTemplate.table.EnabledResizeColumn = false;
		    _this.ElementContentTemplate.formText.classList.add('col-md-offset-2');
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicPBIBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.AddRowTemplate", e);
    }
}

ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.FillTemplate = function (idTemplate){
	var _this = this.TParent();
    var IDTemplate = idTemplate;
    try {
	    for (var i = 0; i < _this.PBITEMPLATEList.length; i++) {
		    if (_this.PBITEMPLATEList[i].IDPBITEMPLATE == IDTemplate) {
			    _this.ElementFormTemplate.id.Text = _this.PBITEMPLATEList[i].IDPBITEMPLATE;
			    _this.ElementFormTemplate.name.Text = _this.PBITEMPLATEList[i].TEMPLATE_NAME;
			    _this.ElementFormTemplate.time.Text = _this.PBITEMPLATEList[i].TEMPLATE_REFRESHTIME;
			    _this.ElementFormTemplate.enabled[0].Checked = _this.PBITEMPLATEList[i].TEMPLATE_REFRESHTIMEENABLE;
			    _this.ElementFormTemplate.path.Text = _this.PBITEMPLATEList[i].TEMPLATE_PATH;
			    _this.ElementFormTemplate.description.Text = _this.PBITEMPLATEList[i].TEMPLATE_DESCRIPTION;
			    _this.ElementFormTemplate.dataSQL.Text = _this.PBITEMPLATEList[i].TEMPLATE_DATASQL;
			    _this.Content(_this._ObjectHtml, '', UsrCfg.Traslate.GetLangText(_this.Mythis, 'CELL'), _this.PBITEMPLATEList[i].TEMPLATE_NAME, _this.PBITEMPLATEList[i].PBITEMPLATECELLSPARENTList, null);
		    }
	    }   
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicPBIBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.FillTemplate", e);
    }
}

ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.SaveTemplate = function (nameTemplate, time, enabled, path, descriptionTemplate, dataSQLTemplate){
    var _this = this.TParent();
    try {
	    var PBITEMPLATE = new Persistence.PBITemplate.Properties.TPBITEMPLATE();
	    PBITEMPLATE.TEMPLATE_NAME = nameTemplate;
	    PBITEMPLATE.TEMPLATE_REFRESHTIMEENABLE = enabled;
	    PBITEMPLATE.TEMPLATE_REFRESHTIME = time;
	    PBITEMPLATE.TEMPLATE_PATH = path;
	    PBITEMPLATE.TEMPLATE_DESCRIPTION = descriptionTemplate;
	    PBITEMPLATE.TEMPLATE_DATASQL = dataSQLTemplate;
	    var success = Persistence.PBITemplate.Methods.PBITEMPLATE_ADD(PBITEMPLATE);
	
	    if (success) {
		    alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "CORRECTSAVE"));
		    _this.ElementFormTemplate.id.Text = '';
		    _this.ElementFormTemplate.name.Text = '';
		    _this.ElementFormTemplate.time.Text = '';
		    _this.ElementFormTemplate.enabled[0].Checked = false;
		    _this.ElementFormTemplate.path.Text = '';
		    _this.ElementFormTemplate.description.Text = '';
		    _this.ElementFormTemplate.dataSQL.Text = '';
		    _this.ElementFormTemplate.id.This.disabled = true;
		    _this.ElementFormTemplate.name.This.disabled = true;
		    _this.ElementFormTemplate.time.This.disabled = true;
		    _this.ElementFormTemplate.enabled[0].This.Column[0].This.children[0].setAttribute('disabled','');
		    _this.ElementFormTemplate.path.This.disabled = true;
		    _this.ElementFormTemplate.description.This.disabled = true;
		    _this.ElementFormTemplate.dataSQL.This.disabled = true;

		    /* Obtenci�n de nuevos Recursos */
		    _this.ListLoad();
		    /*Eliminaci�n de Celdas de la tabla*/
		    for (var i = 0; i < _this.ElementContentTemplate.table._Rows.length; i++) {
			    $(_this.ElementContentTemplate.table._Rows[i].This).remove();
		    }
		    /*Llenado de Celdas de la tabla*/
		    _this.AddRowTemplate();
	    }
	    else {
		    alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECTSAVE"))
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicPBIBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.SaveTemplate", e);
    }
}

ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.UpdateTemplate = function (idTemplate, nameTemplate, time, enabled, path, descriptionTemplate, dataSQLTemplate){
    var _this = this.TParent();
    try {
	    var PBITEMPLATE = new Persistence.PBITemplate.Properties.TPBITEMPLATE();
	    PBITEMPLATE.IDPBITEMPLATE = idTemplate;
	    PBITEMPLATE.TEMPLATE_NAME = nameTemplate;
	    PBITEMPLATE.TEMPLATE_REFRESHTIMEENABLE = enabled;
	    PBITEMPLATE.TEMPLATE_REFRESHTIME = time;
	    PBITEMPLATE.TEMPLATE_PATH = path;
	    PBITEMPLATE.TEMPLATE_DESCRIPTION = descriptionTemplate;
	    PBITEMPLATE.TEMPLATE_DATASQL = dataSQLTemplate;
	    var success = Persistence.PBITemplate.Methods.PBITEMPLATE_UPD(PBITEMPLATE);
	
	    if (success) {
		    _this.ElementFormTemplate.time.Text = PBITEMPLATE.TEMPLATE_REFRESHTIME;
		    alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "CORRECTUPDATE"));
		    _this.ElementFormTemplate.id.This.disabled = true;
		    _this.ElementFormTemplate.name.This.disabled = true;
		    _this.ElementFormTemplate.time.This.disabled = true;
		    _this.ElementFormTemplate.enabled[0].This.Column[0].This.children[0].setAttribute('disabled','');
		    _this.ElementFormTemplate.path.This.disabled = true;
		    _this.ElementFormTemplate.description.This.disabled = true;
		    _this.ElementFormTemplate.dataSQL.This.disabled = true;
		    _this.ElementContentCellParent.title.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, 'CELL') + ': ' + nameTemplate;

		    /* Obtenci�n de nuevos Recursos */
		    _this.ListLoad();
		    /*Eliminaci�n de Celdas de la tabla*/
		    for (var i = 0; i < _this.ElementContentTemplate.table._Rows.length; i++) {
			    $(_this.ElementContentTemplate.table._Rows[i].This).remove();
		    }
		    /*Llenado de Celdas de la tabla*/
		    _this.AddRowTemplate();
		    //Llenar dropdownlist-combobox PSMAIN
		    for (var i = 0; i < _this.ElementFormCellParent.parent.Options.length; i++) {
			    if (parseInt(_this.ElementFormCellParent.parent.Options[i].This.value) == PBITEMPLATE.IDPBITEMPLATE) {
				    _this.ElementFormCellParent.parent.Options[i].Text = PBITEMPLATE.TEMPLATE_NAME;
			    }
		    }
	    }
	    else{
		    alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECTUPDATE"));
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicPBIBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.UpdateTemplate", e);
    }
}

ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.CreateFormCell = function (objectHtml, idControl){
    var _this = this.TParent();
    try {
	    _this.ElementFormCell = null;
	    _this.ElementFormDesign = null;
	    var ObjectHtml = objectHtml;
	    var ContImages = new TVclStackPanel(ObjectHtml, "", 1, [[12], [12], [12], [12], [12]]);
	    ContImages.Row.This.classList.add("DownSpace");
	    //Delete
	    var ImageDelete = new TVclImagen(ContImages.Column[0].This, "");
	    ImageDelete.This.style.float = "right";
	    ImageDelete.This.style.marginLeft = "5px";
	    ImageDelete.Title = "Delete";
	    ImageDelete.Src = "image/16/delete.png";
	    ImageDelete.Cursor = "pointer";
	    /*data-id: ID del Padre(Solo cells)*/
	    ImageDelete.This.setAttribute('data-id', idControl);
	    /*data-control: manejo de paneles(Solo cells)*/
	    ImageDelete.This.setAttribute('data-control', _this.ElementContentCellChild.length-1);
	    ImageDelete.onClick = function () {
		    if (txtId.Text != '') {
			    var confirmed = confirm(UsrCfg.Traslate.GetLangText(_this.Mythis, "CONFIRMDELETE"));
			    if (confirmed) {
				    if(this.dataset.id == 'null' || this.dataset.id == ''){
					    var state = _this.DeleteCell(parseInt(txtId.Text),parseInt(this.dataset.control));
					    if(state){
						    alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "CORRECTDELETE"));
					    }
				    }else{
					    var idParent = parseInt(this.dataset.id);
					
					    for(var i = 0 ; i < _this.ElementFormCellChild.length ; i++){
						    if(_this.ElementFormCellChild[i].idCell == idParent){
							    _this.DeleteCellChild(parseInt(txtId.Text), i);
						    }
					    }
				    }
			    }
			    else {
				    alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NOCONFIRMDELETE"));
			    }
		    }
		    else {
			    alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NOSELECT"));
		    }
	    }
	    //Update
	    var ImageUpdate = new TVclImagen(ContImages.Column[0].This, "");
	    ImageUpdate.This.style.marginLeft = "5px";
	    ImageUpdate.This.style.float = "right";
	    ImageUpdate.Title = "Edit";
	    ImageUpdate.Src = "image/16/edit.png";
	    ImageUpdate.Cursor = "pointer";
	    /*data-id: ID del Padre(Solo cells)*/
	    ImageUpdate.This.setAttribute('data-id', idControl);
	    /*data-control: manejo de paneles(Solo cells)*/
	    ImageUpdate.This.setAttribute('data-control', _this.ElementContentCellChild.length-1);
	    /*Temporal- para saber el panel parent*/
	    ImageUpdate.This.setAttribute('data-parent', _this.PanelChild);
	    ImageUpdate.onClick = function () {
		    _this.PanelChild = parseInt(this.dataset.control);

		    if (txtId.Text != '') {
			    /*Agregado-INICIO*/
			    /*eliminar Paneles*/
			    if(_this.ElementContentCellChild.length != 0){
				    var control = parseInt(this.dataset.control);
				    if(_this.ElementContentCellChild.length-1 > control){
					    for(var i = control+2; i < _this.ElementContentCellChild.length ; i++){
						    $(_this.ElementContentCellChild[i].form).remove();
					    }
				    }
			    }
			    _this.FillCell(parseInt(txtId.Text));
			    /*Agregado-FIN*/
			
			    txtName.This.disabled = false;
			    txtPosition.This.disabled = false;
			    cbTemplate.This.disabled = false;
			    txtDesign_XS.This.disabled  = false;
			    txtDesign_S.This.disabled  = false;
			    txtDesign_M.This.disabled  = false;
			    txtDesign_L.This.disabled  = false;
			    txtDesign_XL.This.disabled  = false;
			    ListBox_Row.ListBoxItems[0].This.Column[0].This.children[0].removeAttribute('disabled','');
			    ListBox_Column.ListBoxItems[0].This.Column[0].This.children[0].removeAttribute('disabled','');
			    btnAdd.This.style.display = "none";
			    btnUpdate.This.style.display = "block";
			    btnCancel.This.style.visibility = 'visible';
			    if(this.dataset.id == '' || this.dataset.id == 'null'){
				    if(_this.ElementFormCellParent.container.Column[0].This.children.length != 0){
					    _this.ElementFormCellGraphic.listVisible[0].This.Column[0].This.children[0].removeAttribute('disabled');
					    _this.ElementFormCellGraphic.title.This.disabled = false;
					    _this.ElementFormCellGraphic.listVisibleTitle[0].This.Column[0].This.children[0].removeAttribute('disabled');
					    _this.ElementFormCellGraphic.colorTitle.Disabled = false;
					    _this.ElementFormCellGraphic.sizeTitle.This.disabled = false;
					    _this.ElementFormCellGraphic.alignTitle.This.disabled = false;
					    _this.ElementFormCellGraphic.typeGraphic.This.disabled = false;
					    for(var i = 0 ; i < _this.ElementFormCellGraphic.listbox.length ; i++){
						    _this.ElementFormCellGraphic.listbox[i].This.Column[0].This.children[0].removeAttribute('disabled');
					    }
				    }
				
				    else{
					    _this.ListLoad();
					    // aqui se debe verificar si la celda no tiene nodos finales para convertirlo en uno
					    var indexEnd = Persistence.PBITemplate.Methods.PBITEMPLATECELLS_ListGetIndex(_this.PBITEMPLATECELLSList, parseInt(txtId.Text));
					    if(_this.PBITEMPLATECELLSList[indexEnd].PBITEMPLATECELLS_CHILDList.length === 0 && !_this.PBITEMPLATECELLSList[indexEnd].CELLS_END){
						    _this.CreateFormCellGraphic(_this.ElementFormCellParent.container, parseInt(_this.ElementFormTemplate.id.Text));
						    _this.ElementFormCellGraphic.listVisible[0].This.Column[0].This.children[0].removeAttribute('disabled');
						    _this.ElementFormCellGraphic.title.This.disabled = false;
						    _this.ElementFormCellGraphic.listVisibleTitle[0].This.Column[0].This.children[0].removeAttribute('disabled');
						    _this.ElementFormCellGraphic.colorTitle.Disabled = false;
						    _this.ElementFormCellGraphic.sizeTitle.This.disabled = false;
						    _this.ElementFormCellGraphic.alignTitle.This.disabled = false;
						    _this.ElementFormCellGraphic.typeGraphic.This.disabled = false;
						    for(var i = 0 ; i < _this.ElementFormCellGraphic.listbox.length ; i++){
							    _this.ElementFormCellGraphic.listbox[i].This.Column[0].This.children[0].removeAttribute('disabled');
						    }
					    }
				    }
			    }else{
				    var idParent = parseInt(this.dataset.id);
				    if(_this.ElementFormCellChild[parseInt(this.dataset.control)].container.Column[0].This.children.length != 0){
					    _this.ElementFormCellGraphic.listVisible[0].This.Column[0].This.children[0].removeAttribute('disabled');
					    _this.ElementFormCellGraphic.title.This.disabled = false;
					    _this.ElementFormCellGraphic.listVisibleTitle[0].This.Column[0].This.children[0].removeAttribute('disabled');
					    _this.ElementFormCellGraphic.colorTitle.Disabled = false;
					    _this.ElementFormCellGraphic.sizeTitle.This.disabled = false;
					    _this.ElementFormCellGraphic.alignTitle.This.disabled = false;
					    _this.ElementFormCellGraphic.typeGraphic.This.disabled = false;
					    for(var j = 0 ; j < _this.ElementFormCellGraphic.listbox.length ; j++){
						    _this.ElementFormCellGraphic.listbox[j].This.Column[0].This.children[0].removeAttribute('disabled');
					    }
				    }
				    /*Revisar el cancel*/
				    else{
					    _this.ListLoad();
					    // aqui se debe verificar si la celda no tiene nodos finales para convertirlo en uno
					    var indexEnd = Persistence.PBITemplate.Methods.PBITEMPLATECELLS_ListGetIndex(_this.PBITEMPLATECELLSList, parseInt(txtId.Text));
					    if(_this.PBITEMPLATECELLSList[indexEnd].PBITEMPLATECELLS_CHILDList.length === 0 && !_this.PBITEMPLATECELLSList[indexEnd].CELLS_END){
						    _this.CreateFormCellGraphic(_this.ElementFormCellChild[parseInt(this.dataset.control)].container, parseInt(_this.ElementFormTemplate.id.Text));
						    _this.ElementFormCellGraphic.listVisible[0].This.Column[0].This.children[0].removeAttribute('disabled');
						    _this.ElementFormCellGraphic.title.This.disabled = false;
						    _this.ElementFormCellGraphic.listVisibleTitle[0].This.Column[0].This.children[0].removeAttribute('disabled');
						    _this.ElementFormCellGraphic.colorTitle.Disabled = false;
						    _this.ElementFormCellGraphic.sizeTitle.This.disabled = false;
						    _this.ElementFormCellGraphic.alignTitle.This.disabled = false;
						    _this.ElementFormCellGraphic.typeGraphic.This.disabled = false;
						    for(var i = 0 ; i < _this.ElementFormCellGraphic.listbox.length ; i++){
							    _this.ElementFormCellGraphic.listbox[i].This.Column[0].This.children[0].removeAttribute('disabled');
						    }
					    }
				    }
			    }
		    }else {
			    /*Cuando cancelas el guardado de un hijo con el icon image update*/
			    txtName.This.disabled = true;
			    txtPosition.This.disabled = true;
			    cbTemplate.This.disabled = true;
			    txtDesign_XS.This.disabled  = true;
			    txtDesign_S.This.disabled  = true;
			    txtDesign_M.This.disabled  = true;
			    txtDesign_L.This.disabled  = true;
			    txtDesign_XL.This.disabled  = true;
			    alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NOSELECT"));
			    btnAdd.This.style.display = "none";
			    btnUpdate.This.style.display = "none";
			    btnCancel.This.style.visibility = 'hidden';
			    if(parseInt(this.dataset.control) < 0){
				    $(_this.ElementFormCellParent.container.Column[0].This).html('');
			    }else{
				    var idFill = null;
				    var idParent = parseInt(this.dataset.id);
				    var PBITEMPLATECELLS = new Persistence.PBITemplate.Methods.PBITEMPLATECELLS_ListSetID(_this.PBITEMPLATECELLSList, idParent);
				    if(PBITEMPLATECELLS.CELLS_END){
					    var idFill = PBITEMPLATECELLS.IDPBITEMPLATECELLS_PARENT;
					    $(_this.ElementFormCellChild[parseInt(this.dataset.control)].container.Column[0].This).html('');
					    // for(var i = 0 ; i < _this.ElementFormCellChild.length ; i++){
						    if(idFill > 0){
							    if(_this.ElementFormCellChild[parseInt(this.dataset.parent)].idCell == idFill){
								    _this.CreateFormCellGraphic(_this.ElementFormCellChild[parseInt(this.dataset.parent)].container, parseInt(_this.ElementFormTemplate.id.Text));
								    /*Extraer datos*/
								    for(var j = 0 ; j < _this.PBITEMPLATECELLGRAPHICList.length ; j++){
									    if(_this.PBITEMPLATECELLGRAPHICList[j].IDPBITEMPLATECELLS == idParent){
										    _this.FillCellGraphic(idParent);
										    _this.FillColumn(_this.PBITEMPLATECELLGRAPHICList[j].IDPBITEMPLATECELLGRAPHIC);
									    }
								    }
								    if(!_this.ElementFormCellChild[parseInt(this.dataset.parent)].name.This.disabled){
									    _this.ElementFormCellGraphic.listVisible[0].This.Column[0].This.children[0].removeAttribute('disabled');
									    _this.ElementFormCellGraphic.title.This.disabled = false;
									    _this.ElementFormCellGraphic.listVisibleTitle[0].This.Column[0].This.children[0].removeAttribute('disabled');
									    _this.ElementFormCellGraphic.colorTitle.Disabled = false;
									    _this.ElementFormCellGraphic.sizeTitle.This.disabled = false;
									    _this.ElementFormCellGraphic.alignTitle.This.disabled = false;
									    _this.ElementFormCellGraphic.typeGraphic.This.disabled = false;
									    for(var j = 0 ; j < _this.ElementFormCellGraphic.listbox.length ; j++){
										    _this.ElementFormCellGraphic.listbox[j].This.Column[0].This.children[0].removeAttribute('disabled');
									    }
								    }
							    }
						    }
						    else{
							    _this.CreateFormCellGraphic(_this.ElementFormCellParent.container, parseInt(_this.ElementFormTemplate.id.Text));
							    for(var j = 0 ; j < _this.PBITEMPLATECELLGRAPHICList.length ; j++){
								    if(_this.PBITEMPLATECELLGRAPHICList[j].IDPBITEMPLATECELLS == idParent){
									    _this.FillCellGraphic(idParent);
									    _this.FillColumn(_this.PBITEMPLATECELLGRAPHICList[j].IDPBITEMPLATECELLGRAPHIC);
								    }
							    }
							    if(!_this.ElementFormCellParent.name.This.disabled){
								    _this.ElementFormCellGraphic.listVisible[0].This.Column[0].This.children[0].removeAttribute('disabled');
								    _this.ElementFormCellGraphic.title.This.disabled = false;
								    _this.ElementFormCellGraphic.listVisibleTitle[0].This.Column[0].This.children[0].removeAttribute('disabled');
								    _this.ElementFormCellGraphic.colorTitle.Disabled = false;
								    _this.ElementFormCellGraphic.sizeTitle.This.disabled = false;
								    _this.ElementFormCellGraphic.alignTitle.This.disabled = false;
								    _this.ElementFormCellGraphic.typeGraphic.This.disabled = false;
								    for(var j = 0 ; j < _this.ElementFormCellGraphic.listbox.length ; j++){
									    _this.ElementFormCellGraphic.listbox[j].This.Column[0].This.children[0].removeAttribute('disabled');
								    }
							    }
						    }
					    // }
				    }else{
					    $(_this.ElementFormCellChild[parseInt(this.dataset.control)].container.Column[0].This).html('');
				    }
			    }
		    }
	    }
	    //Add
	    var ImageAdd = new TVclImagen(ContImages.Column[0].This, "");
	    ImageAdd.This.style.marginLeft = "5px";
	    ImageAdd.This.style.float = "right";
	    ImageAdd.Title = "Add";
	    ImageAdd.Src = "image/16/add.png";
	    ImageAdd.Cursor = "pointer";
	    /*data-id: ID del Padre(Solo cells)*/
	    ImageAdd.This.setAttribute('data-id', idControl);
	    /*data-control: manejo de paneles(Solo cells)*/
	    ImageAdd.This.setAttribute('data-control', _this.ElementContentCellChild.length-1);
	    ImageAdd.onClick = function () {
		    txtId.Text = '';
		    txtName.Text = '';
		    txtPosition.Text = '0';
		    txtDesign_XS.selectedIndex = 0;
		    txtDesign_S.selectedIndex = 0;
		    txtDesign_M.selectedIndex = 0;
		    txtDesign_L.selectedIndex = 0;
		    txtDesign_XL.selectedIndex = 0;
		    ListBox_Row.ListBoxItems[0].Checked = false;
		    ListBox_Column.ListBoxItems[0].Checked = false;
		    txtName.This.disabled = false;
		    txtPosition.This.disabled = false;
		    cbTemplate.This.disabled = false;
		    txtDesign_XS.This.disabled  = false;
		    txtDesign_S.This.disabled  = false;
		    txtDesign_M.This.disabled  = false;
		    txtDesign_L.This.disabled  = false;
		    txtDesign_XL.This.disabled  = false;
		    ListBox_Row.ListBoxItems[0].This.Column[0].This.children[0].removeAttribute('disabled','');
		    ListBox_Column.ListBoxItems[0].This.Column[0].This.children[0].removeAttribute('disabled','');
		    btnUpdate.This.style.display = 'none';
		    btnAdd.This.style.display = "block";
		    btnCancel.This.style.visibility = 'visible';
		    /*Eliminar paneles*/
		    if(_this.ElementContentCellChild.length != 0){
			    var control = parseInt(this.dataset.control);
			    if(_this.ElementContentCellChild.length-1 > control){
				    for(var i = control+1; i < _this.ElementContentCellChild.length ; i++){
					    $(_this.ElementContentCellChild[i].form).remove();
				    }
			    }
		    }
		    if(this.dataset.id == '' || this.dataset.id == 'null'){
			    // alert(this.dataset.id);
			    _this.CreateFormCellGraphic(_this.ElementFormCellParent.container, parseInt(_this.ElementFormTemplate.id.Text));
			    _this.ElementFormCellGraphic.listVisible[0].This.Column[0].This.children[0].removeAttribute('disabled');
			    _this.ElementFormCellGraphic.title.This.disabled = false;
			    _this.ElementFormCellGraphic.listVisibleTitle[0].This.Column[0].This.children[0].removeAttribute('disabled');
			    _this.ElementFormCellGraphic.colorTitle.Disabled = false;
			    _this.ElementFormCellGraphic.sizeTitle.This.disabled = false;
			    _this.ElementFormCellGraphic.alignTitle.This.disabled = false;
			    _this.ElementFormCellGraphic.typeGraphic.This.disabled = false;
			    for(var i = 0 ; i < _this.ElementFormCellGraphic.listbox.length ; i++){
				    _this.ElementFormCellGraphic.listbox[i].This.Column[0].This.children[0].removeAttribute('disabled');
			    }
		    }
		    else{
			    var formCellGraphic = null;
			    var selectedIndex = 0;
			    var selectedIndexAlign = 0;
			    var idParent = parseInt(this.dataset.id);
			    var idFill = null;
			    var stateCellGraphic = false;
			
			    for(var i = 0 ; i < _this.PBITEMPLATECELLGRAPHICList.length; i++){
				    if(_this.PBITEMPLATECELLGRAPHICList[i].IDPBITEMPLATECELLS == idParent){
					    stateCellGraphic = true;
					    /*Si el padre es un nodo final*/
				    }
			    }
			    if(stateCellGraphic){
				    for(var i = 0 ; i < _this.PBITEMPLATECELLSList.length ; i++){
					    if(_this.PBITEMPLATECELLSList[i].IDPBITEMPLATECELLS == idParent){
						    if(_this.PBITEMPLATECELLSList[i].PBITEMPLATECELLS_PARENT != null){
							    idFill = _this.PBITEMPLATECELLSList[i].PBITEMPLATECELLS_PARENT.IDPBITEMPLATECELLS;
						    }
					    }
				    }
				    /*Recorrer los ElementContentCellChild en busca del contenedor padre*/
				    if(idFill > 0){
					    for (var i = 0; i < _this.ElementFormCellChild.length; i++) {
						    if(_this.ElementFormCellChild[i].idCell == idFill){
							    formCellGraphic = _this.ElementFormCellGraphic;
							    selectedIndex = _this.ElementFormCellGraphic.typeGraphic.selectedIndex;
							    selectedIndexAlign = _this.ElementFormCellGraphic.alignTitle.selectedIndex;
							    $(_this.ElementFormCellChild[i].container.Column[0].This).html('');

							    _this.ElementFormCellChild[i].id.This.disabled = true;
							    _this.ElementFormCellChild[i].name.This.disabled = true;
							    _this.ElementFormCellChild[i].position.This.disabled = true;
							    _this.ElementFormCellChild[i].parent.This.disabled = true;
							    _this.ElementFormDesignChild[i].designXS.This.disabled  = true;
							    _this.ElementFormDesignChild[i].designS.This.disabled  = true;
							    _this.ElementFormDesignChild[i].designM.This.disabled  = true;
							    _this.ElementFormDesignChild[i].designL.This.disabled  = true;
							    _this.ElementFormDesignChild[i].designXL.This.disabled  = true;
							    _this.ElementFormDesignChild[i].designRow[0].This.Column[0].This.children[0].setAttribute('disabled','');
							    _this.ElementFormDesignChild[i].designColumn[0].This.Column[0].This.children[0].setAttribute('disabled','');
							    _this.ElementFormCellChild[i].save.This.style.display = 'none';
							    _this.ElementFormCellChild[i].update.This.style.display = 'none';
							    _this.ElementFormCellChild[i].cancel.This.style.visibility = 'hidden'
						    }
					    }
				    }else{
					    formCellGraphic = _this.ElementFormCellGraphic;
					    selectedIndex = _this.ElementFormCellGraphic.typeGraphic.selectedIndex;
					    selectedIndexAlign = _this.ElementFormCellGraphic.alignTitle.selectedIndex;
					    $(_this.ElementFormCellParent.container.Column[0].This).html('');

					    _this.ElementFormCellParent.id.This.disabled = true;
					    _this.ElementFormCellParent.name.This.disabled = true;
					    _this.ElementFormCellParent.position.This.disabled = true;
					    _this.ElementFormCellParent.parent.This.disabled = true;
					    _this.ElementFormDesignParent.designXS.This.disabled  = true;
					    _this.ElementFormDesignParent.designS.This.disabled  = true;
					    _this.ElementFormDesignParent.designM.This.disabled  = true;
					    _this.ElementFormDesignParent.designL.This.disabled  = true;
					    _this.ElementFormDesignParent.designXL.This.disabled  = true;
					    _this.ElementFormDesignParent.designRow[0].This.Column[0].This.children[0].setAttribute('disabled','');
					    _this.ElementFormDesignParent.designColumn[0].This.Column[0].This.children[0].setAttribute('disabled','');
					    _this.ElementFormCellParent.save.This.style.display = 'none';
					    _this.ElementFormCellParent.update.This.style.display = 'none';
					    _this.ElementFormCellParent.cancel.This.style.visibility = 'hidden';

				    }

				    for (var i = 0; i < _this.ElementFormCellChild.length; i++) {
					    if(_this.ElementFormCellChild[i].idCell == idParent){
						    _this.CreateFormCellGraphic(_this.ElementFormCellChild[i].container, parseInt(_this.ElementFormTemplate.id.Text));
						    // llenar el formulario con la data en formCellGraphic
						    _this.ElementFormCellGraphic.listVisible[0].Checked = formCellGraphic.listVisible[0].Checked;
						    _this.ElementFormCellGraphic.title.Text = formCellGraphic.title.Text;
						    _this.ElementFormCellGraphic.listVisibleTitle[0].Checked = formCellGraphic.listVisibleTitle[0].Checked;
						    _this.ElementFormCellGraphic.colorTitle.Color = formCellGraphic.colorTitle.Color;
						    _this.ElementFormCellGraphic.sizeTitle.Text = formCellGraphic.sizeTitle.Text;
						    _this.ElementFormCellGraphic.alignTitle.selectedIndex = selectedIndexAlign;
						    _this.ElementFormCellGraphic.typeGraphic.selectedIndex = selectedIndex;//formCellGraphic.typeGraphic.selectedIndex;
						    for(var j = 0 ; j < formCellGraphic.listbox.length ; j++){
							    _this.ElementFormCellGraphic.listbox[j].Checked = formCellGraphic.listbox[j].Checked;
						    }

						    /*Habilitar Formulario*/
						    _this.ElementFormCellGraphic.listVisible[0].This.Column[0].This.children[0].removeAttribute('disabled');
						    _this.ElementFormCellGraphic.title.This.disabled = false;
						    _this.ElementFormCellGraphic.listVisibleTitle[0].This.Column[0].This.children[0].removeAttribute('disabled');
						    _this.ElementFormCellGraphic.colorTitle.Disabled = false;
						    _this.ElementFormCellGraphic.sizeTitle.This.disabled = false;
						    _this.ElementFormCellGraphic.alignTitle.This.disabled = false;
						    _this.ElementFormCellGraphic.typeGraphic.This.disabled = false;
						    for(var j = 0 ; j < _this.ElementFormCellGraphic.listbox.length ; j++){
							    _this.ElementFormCellGraphic.listbox[j].This.Column[0].This.children[0].removeAttribute('disabled');
						    }
					    }
				    }
			    }else{
				    /*Si el padre no es un nodo final solo se muestra el container en el panel actual*/
				    for (var i = 0; i < _this.ElementFormCellChild.length; i++) {
					    if(_this.ElementFormCellChild[i].idCell == idParent){
						    _this.CreateFormCellGraphic(_this.ElementFormCellChild[i].container, parseInt(_this.ElementFormTemplate.id.Text));
						    /*Habilitar Formulario*/
						    _this.ElementFormCellGraphic.listVisible[0].This.Column[0].This.children[0].removeAttribute('disabled');
						    _this.ElementFormCellGraphic.title.This.disabled = false;
						    _this.ElementFormCellGraphic.listVisibleTitle[0].This.Column[0].This.children[0].removeAttribute('disabled');
						    _this.ElementFormCellGraphic.colorTitle.Disabled = false;
						    _this.ElementFormCellGraphic.sizeTitle.This.disabled = false;
						    _this.ElementFormCellGraphic.alignTitle.This.disabled = false;
						    _this.ElementFormCellGraphic.typeGraphic.This.disabled = false;
						    for(var j = 0 ; j < _this.ElementFormCellGraphic.listbox.length ; j++){
							    _this.ElementFormCellGraphic.listbox[j].This.Column[0].This.children[0].removeAttribute('disabled');
						    }
					    }
				    }
			    }
		    }
	    }

	    var ContTextId = new TVclStackPanel(ObjectHtml, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	    ContTextId.Row.This.style.marginBottom = '5px';
	    var lblId = new TVcllabel(ContTextId.Column[0].This, "", TlabelType.H0);
	    lblId.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ID");
	    lblId.VCLType = TVCLType.BS;
	    var txtId = new TVclTextBox(ContTextId.Column[1].This, "");
	    txtId.Text = "";
	    txtId.This.disabled = true;
	    txtId.VCLType = TVCLType.BS;

	    var ContTextName = new TVclStackPanel(ObjectHtml, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	    ContTextName.Row.This.style.marginBottom = '5px';
	    var lblName = new TVcllabel(ContTextName.Column[0].This, "", TlabelType.H0);
	    lblName.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "NAME");
	    lblName.VCLType = TVCLType.BS;
	    var txtName = new TVclTextBox(ContTextName.Column[1].This, "");
	    txtName.Text = "";
	    txtName.This.disabled = true;
	    txtName.VCLType = TVCLType.BS;

	    var ContPosition = new TVclStackPanel(ObjectHtml, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	    ContPosition.Row.This.style.marginBottom = '5px';
	    var lblPosition = new TVcllabel(ContPosition.Column[0].This, "", TlabelType.H0);
	    lblPosition.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "POSITION");
	    lblPosition.VCLType = TVCLType.BS;
	    var txtPosition = new TVclTextBox(ContPosition.Column[1].This, "");
	    txtPosition.Text = "";
	    txtPosition.This.disabled = true;
	    txtPosition.VCLType = TVCLType.BS;
	    txtPosition.This.setAttribute("type", "number");
	    txtPosition.This.setAttribute("min", "0");

	    /* Cambiar por un LIST */
	    var ContTemplate = new TVclStackPanel(ObjectHtml, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	    ContTemplate.Row.This.style.marginBottom = '5px';
	    var lblTemplate = new TVcllabel(ContTemplate.Column[0].This, "", TlabelType.H0);
	    lblTemplate.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "PARENT");
	    lblTemplate.VCLType = TVCLType.BS;
	    var cbTemplate = new TVclComboBox2(ContTemplate.Column[1].This, "");
	    cbTemplate.VCLType = TVCLType.BS;
	    var aux = null;
	    for (var i = 0; i < _this.PBITEMPLATEList.length; i++) {
		    var ComboItem = new TVclComboBoxItem();
		    ComboItem.Value = _this.PBITEMPLATEList[i].IDPBITEMPLATE;
		    ComboItem.Text = _this.PBITEMPLATEList[i].TEMPLATE_NAME;
		    ComboItem.Tag = "Test";
		    cbTemplate.AddItem(ComboItem);
		    if (_this.PBITEMPLATEList[i].IDPBITEMPLATE == parseInt(_this.ElementFormTemplate.id.Text)) {
			    aux = i;
		    }
	    }
	    cbTemplate.selectedIndex = aux;
	    cbTemplate.This.classList.add("DropDownListGroup");
	    cbTemplate.This.disabled = true;
	
	    if(idControl != null){	
		    ContTemplate.Row.This.style.display = 'none';
	    }

	    /*CELL DESIGN*/
	    var ContDesign_XS = new TVclStackPanel(ObjectHtml, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	    ContDesign_XS.Row.This.style.marginBottom = '5px';
	    var lblDesign_XS = new TVcllabel(ContDesign_XS.Column[0].This, "", TlabelType.H0);
	    lblDesign_XS.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "DESIGN_XS");
	    lblDesign_XS.VCLType = TVCLType.BS;
	    var txtDesign_XS = new TVclComboBox2(ContDesign_XS.Column[1].This, "");
	    txtDesign_XS.VCLType = TVCLType.BS;

	    var ContDesign_S = new TVclStackPanel(ObjectHtml, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	    ContDesign_S.Row.This.style.marginBottom = '5px';
	    var lblDesign_S = new TVcllabel(ContDesign_S.Column[0].This, "", TlabelType.H0);
	    lblDesign_S.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "DESIGN_S");
	    lblDesign_S.VCLType = TVCLType.BS;
	    var txtDesign_S = new TVclComboBox2(ContDesign_S.Column[1].This, "");
	    txtDesign_S.VCLType = TVCLType.BS;
	
	    var ContDesign_M = new TVclStackPanel(ObjectHtml, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	    ContDesign_M.Row.This.style.marginBottom = '5px';
	    var lblDesign_M = new TVcllabel(ContDesign_M.Column[0].This, "", TlabelType.H0);
	    lblDesign_M.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "DESIGN_M");
	    lblDesign_M.VCLType = TVCLType.BS;
	    var txtDesign_M = new TVclComboBox2(ContDesign_M.Column[1].This, "");
	    txtDesign_M.VCLType = TVCLType.BS;

	    var ContDesign_L = new TVclStackPanel(ObjectHtml, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	    ContDesign_L.Row.This.style.marginBottom = '5px';
	    var lblDesign_L = new TVcllabel(ContDesign_L.Column[0].This, "", TlabelType.H0);
	    lblDesign_L.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "DESIGN_L");
	    lblDesign_L.VCLType = TVCLType.BS;
	    var txtDesign_L = new TVclComboBox2(ContDesign_L.Column[1].This, "");
	    txtDesign_L.VCLType = TVCLType.BS;

	    var ContDesign_XL = new TVclStackPanel(ObjectHtml, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	    ContDesign_XL.Row.This.style.marginBottom = '5px';
	    var lblDesign_XL = new TVcllabel(ContDesign_XL.Column[0].This, "", TlabelType.H0);
	    lblDesign_XL.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "DESIGN_XL");
	    lblDesign_XL.VCLType = TVCLType.BS;
	    var txtDesign_XL = new TVclComboBox2(ContDesign_XL.Column[1].This, "");
	    txtDesign_XL.VCLType = TVCLType.BS;

	    for (var i = 0; i <= 12; i++) {
		    var ComboItem_XS = new TVclComboBoxItem();
		    var ComboItem_S = new TVclComboBoxItem();
		    var ComboItem_M = new TVclComboBoxItem();
		    var ComboItem_L = new TVclComboBoxItem();
		    var ComboItem_XL = new TVclComboBoxItem();
		    ComboItem_XS.Value = i;
		    ComboItem_XS.Text = i;
		    ComboItem_S.Value = i;
		    ComboItem_S.Text = i;
		    ComboItem_M.Value = i;
		    ComboItem_M.Text = i;
		    ComboItem_L.Value = i;
		    ComboItem_L.Text = i;
		    ComboItem_XL.Value = i;
		    ComboItem_XL.Text = i;
		    txtDesign_XS.AddItem(ComboItem_XS);
		    txtDesign_S.AddItem(ComboItem_S);
		    txtDesign_M.AddItem(ComboItem_M);
		    txtDesign_L.AddItem(ComboItem_L);
		    txtDesign_XL.AddItem(ComboItem_XL);
	    }

	    txtDesign_XS.selectedIndex = 0;
	    txtDesign_XS.This.classList.add("DropDownListGroup");
	    txtDesign_XS.This.disabled = true;
	    txtDesign_S.selectedIndex = 0;
	    txtDesign_S.This.classList.add("DropDownListGroup");
	    txtDesign_S.This.disabled = true;
	    txtDesign_M.selectedIndex = 0;
	    txtDesign_M.This.classList.add("DropDownListGroup");
	    txtDesign_M.This.disabled = true;
	    txtDesign_M.selectedIndex = 0;
	    txtDesign_L.This.classList.add("DropDownListGroup");
	    txtDesign_L.This.disabled = true;
	    txtDesign_L.selectedIndex = 0;
	    txtDesign_XL.This.classList.add("DropDownListGroup");
	    txtDesign_XL.This.disabled = true;
	    txtDesign_XL.selectedIndex = 0;


	    var ContDesign_Row = new TVclStackPanel(ObjectHtml, "", 2, [[12, 12], [6, 6], [6, 6], [6, 6], [6, 6]]);
	    ContDesign_Row.Row.This.style.display = 'none';
	    var ListBox_Row = new TVclListBox(ContDesign_Row.Column[0].This, "");
	    ListBox_Row.EnabledCheckBox = true;
	    var ListBoxItem_Row = new TVclListBoxItem();
	    ListBoxItem_Row.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "DESIGN_ROW");
	    ListBoxItem_Row.Index = 0;
	    ListBox_Row.AddListBoxItem(ListBoxItem_Row);
	    ListBox_Row.ListBoxItems[0].This.Column[0].This.children[0].setAttribute('disabled','');

	    var ListBox_Column = new TVclListBox(ContDesign_Row.Column[1].This, "");
	    ListBox_Column.EnabledCheckBox = true;
	    var ListBoxItem_Column = new TVclListBoxItem();
	    ListBoxItem_Column.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "DESIGN_COLUMN");
	    ListBoxItem_Column.Index = 0;
	    ListBox_Column.AddListBoxItem(ListBoxItem_Column);
	    ListBox_Column.ListBoxItems[0].This.Column[0].This.children[0].setAttribute('disabled','');

	    /*Contenedor de los CELGRAPHICS*/
	    var ContCellGraphics = new TVclStackPanel(ObjectHtml, "", 1, [[12], [12], [12], [12], [12]]);
	    ContCellGraphics.Row.This.classList.add("DownSpace");
	    /*-------*/

	    var ContButton = new TVclStackPanel(ObjectHtml, "", 2, [[12, 12], [12, 12], [12, 12], [12, 12], [12, 12]]);
	    ContButton.Row.This.classList.add("DownSpace");

	    var btnCancel = new TVclInputbutton(ContButton.Column[0].This, "");
	    btnCancel.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "CANCEL");
	    btnCancel.This.style.visibility = 'hidden'
	    btnCancel.VCLType = TVCLType.BS;
	    btnCancel.This.setAttribute('data-id', idControl);
	    btnCancel.This.setAttribute('data-control', _this.ElementContentCellChild.length-1); //control

	    /*Temporal- para saber el panel parent*/
	    btnCancel.This.setAttribute('data-parent', _this.PanelChild); //control	

	    btnCancel.onClick = function () {
		    txtName.This.disabled = true;
		    txtPosition.This.disabled = true;
		    cbTemplate.This.disabled = true;
		    txtDesign_XS.This.disabled  = true;
		    txtDesign_S.This.disabled  = true;
		    txtDesign_M.This.disabled  = true;
		    txtDesign_L.This.disabled  = true;
		    txtDesign_XL.This.disabled  = true;
		    ListBox_Row.ListBoxItems[0].This.Column[0].This.children[0].setAttribute('disabled','');
		    ListBox_Column.ListBoxItems[0].This.Column[0].This.children[0].setAttribute('disabled','');
		    btnAdd.This.style.display = 'none';
		    btnUpdate.This.style.display = 'none';
		    btnCancel.This.style.visibility = 'hidden'
		    if(this.dataset.id == 'null' || this.dataset.id == ''){
			    /*Nodo Padre*/
			    if(_this.ElementFormCellParent.id.Text == ''){
				    /*Si la acción es guardar(al guardar un nuevo registroe el campo id se muestra vacio)*/
				    $(_this.ElementFormCellParent.container.Column[0].This).html('');
			    }else{
				    /*En este caso es editar*/
				    var index = Persistence.PBITemplate.Methods.PBITEMPLATECELLS_ListGetIndex(_this.PBITEMPLATECELLSList, parseInt(txtId.Text));
				    if(_this.PBITEMPLATECELLSList[index].CELLS_END){
					    if(_this.ElementFormCellParent.container.Column[0].This.children.length != 0){
						    _this.ElementFormCellGraphic.listVisible[0].This.Column[0].This.children[0].setAttribute('disabled','');
						    _this.ElementFormCellGraphic.title.This.disabled = true;
						    _this.ElementFormCellGraphic.listVisibleTitle[0].This.Column[0].This.children[0].setAttribute('disabled','');
						    _this.ElementFormCellGraphic.colorTitle.Disabled = true;
						    _this.ElementFormCellGraphic.sizeTitle.This.disabled = true;
						    _this.ElementFormCellGraphic.alignTitle.This.disabled = true;
						    _this.ElementFormCellGraphic.typeGraphic.This.disabled = true;
						    for(var i = 0 ; i < _this.ElementFormCellGraphic.listbox.length ; i++){
							    _this.ElementFormCellGraphic.listbox[i].This.Column[0].This.children[0].setAttribute('disabled','');
						    }
					    }
				    }else{
					    $(_this.ElementFormCellParent.container.Column[0].This).html('');
				    }
			    }
		    }else{
			    /*Si no es el Nodo Padre*/
			    var idParent = parseInt(this.dataset.id);

			    var idFill = null;
			    var stateCellGraphic = false;

			    if(_this.ElementFormCellChild[parseInt(this.dataset.control)].id.Text == ''){
				    /*Si la acción es guardar(al guardar un nuevo registro el campo id se muestra vacio)*/
				    for(var i = 0 ; i < _this.PBITEMPLATECELLGRAPHICList.length; i++){
					    if(_this.PBITEMPLATECELLGRAPHICList[i].IDPBITEMPLATECELLS == idParent){
						    stateCellGraphic = true;
						    /*Si el padre es un nodo final*/
					    }
				    }

				    if(stateCellGraphic){
					    for(var i = 0 ; i < _this.PBITEMPLATECELLSList.length ; i++){
						    if(_this.PBITEMPLATECELLSList[i].IDPBITEMPLATECELLS == idParent){
							    if(_this.PBITEMPLATECELLSList[i].PBITEMPLATECELLS_PARENT != null){
								    idFill = _this.PBITEMPLATECELLSList[i].PBITEMPLATECELLS_PARENT.IDPBITEMPLATECELLS;
							    }
						    }
					    }
					    /*Recorrer los ElementContentCellChild en busca del contenedor padre*/
					    if(idFill > 0){						
						    // crear el formulario en el panel padre si el padre es nodo final
						    _this.CreateFormCellGraphic(_this.ElementFormCellChild[this.dataset.parent].container, parseInt(_this.ElementFormTemplate.id.Text));
						    if(!_this.ElementFormCellChild[parseInt(this.dataset.parent)].name.This.disabled){
							    _this.ElementFormCellGraphic.listVisible[0].This.Column[0].This.children[0].removeAttribute('disabled');
							    _this.ElementFormCellGraphic.title.This.disabled = false;
							    _this.ElementFormCellGraphic.listVisibleTitle[0].This.Column[0].This.children[0].removeAttribute('disabled');
							    _this.ElementFormCellGraphic.colorTitle.Disabled = false;
							    _this.ElementFormCellGraphic.sizeTitle.This.disabled = false;
							    _this.ElementFormCellGraphic.alignTitle.This.disabled = false;
							    _this.ElementFormCellGraphic.typeGraphic.This.disabled = false;
							    for(var j = 0 ; j < _this.ElementFormCellGraphic.listbox.length ; j++){
								    _this.ElementFormCellGraphic.listbox[j].This.Column[0].This.children[0].removeAttribute('disabled');
							    }
						    }
						    // llenar formulario
					    }else{
						    _this.CreateFormCellGraphic(_this.ElementFormCellParent.container, parseInt(_this.ElementFormTemplate.id.Text));
						    if(!_this.ElementFormCellParent.name.This.disabled){
							    _this.ElementFormCellGraphic.listVisible[0].This.Column[0].This.children[0].removeAttribute('disabled');
							    _this.ElementFormCellGraphic.title.This.disabled = false;
							    _this.ElementFormCellGraphic.listVisibleTitle[0].This.Column[0].This.children[0].removeAttribute('disabled');
							    _this.ElementFormCellGraphic.colorTitle.Disabled = false;
							    _this.ElementFormCellGraphic.sizeTitle.This.disabled = false;
							    _this.ElementFormCellGraphic.alignTitle.This.disabled = false;
							    _this.ElementFormCellGraphic.typeGraphic.This.disabled = false;
							    for(var j = 0 ; j < _this.ElementFormCellGraphic.listbox.length ; j++){
								    _this.ElementFormCellGraphic.listbox[j].This.Column[0].This.children[0].removeAttribute('disabled');
							    }
						    }
					    }

					    /*Llenar Formularios*/
					    for(var j = 0 ; j < _this.PBITEMPLATECELLGRAPHICList.length ; j++){
						    if(_this.PBITEMPLATECELLGRAPHICList[j].IDPBITEMPLATECELLS == idParent){
							    _this.ElementFormCellGraphic.listVisible[0].Checked = _this.PBITEMPLATECELLGRAPHICList[j].CELLGRAPHIC_VISIBLE;
							    _this.ElementFormCellGraphic.title.Text = _this.PBITEMPLATECELLGRAPHICList[j].CELLGRAPHIC_TITLE;
							    _this.ElementFormCellGraphic.listVisibleTitle[0].Checked = _this.PBITEMPLATECELLGRAPHICList[j].CELLGRAPHIC_VISIBLETITLE;
							    _this.ElementFormCellGraphic.colorTitle.Color = _this.PBITEMPLATECELLGRAPHICList[j].CELLGRAPHIC_COLORTITLE;
							    _this.ElementFormCellGraphic.sizeTitle.Text = parseInt(_this.PBITEMPLATECELLGRAPHICList[j].CELLGRAPHIC_SIZETITLE);
							    for(var k = 0 ; k < _this.ElementFormCellGraphic.alignTitle.Options.length; k++){
								    if(_this.ElementFormCellGraphic.alignTitle.Options[k].Text.toLowerCase() == _this.PBITEMPLATECELLGRAPHICList[j].CELLGRAPHIC_ALIGNTITLE.toLowerCase()){
									    _this.ElementFormCellGraphic.alignTitle.selectedIndex = k;
								    }
							    }

							    for(var k = 0 ; k < _this.PBITEMPLATECELLGRAPHIC_COLUMNList.length ; k++){
								    if(_this.PBITEMPLATECELLGRAPHIC_COLUMNList[k].IDPBITEMPLATECELLGRAPHIC == _this.PBITEMPLATECELLGRAPHICList[j].IDPBITEMPLATECELLGRAPHIC){

									    for(var m = 0 ; m < _this.ElementFormCellGraphic.listbox.length ; m++){
										    if(_this.ElementFormCellGraphic.listbox[m].Text === _this.PBITEMPLATECELLGRAPHIC_COLUMNList[k].COLUMN_NAME){
											    _this.ElementFormCellGraphic.listbox[m].Checked = true;
										    }
									    }
								    }
							    }
							    for (var k = 0; k < _this.ElementFormCellGraphic.typeGraphic.Options.length; k++) {
								    if (parseInt(_this.ElementFormCellGraphic.typeGraphic.Options[k].Value) == _this.PBITEMPLATECELLGRAPHICList[j].IDPBIGRAPHICTYPE) {
									    _this.ElementFormCellGraphic.typeGraphic.selectedIndex = k;
								    }
							    }
						    }
					    }

				    }
				    $(_this.ElementFormCellChild[parseInt(this.dataset.control)].container.Column[0].This).html('');
			    }
			    else{
				    /*En este caso es editar*/
				    var index = Persistence.PBITemplate.Methods.PBITEMPLATECELLS_ListGetIndex(_this.PBITEMPLATECELLSList, parseInt(txtId.Text));
				    if(_this.PBITEMPLATECELLSList[index].CELLS_END){
					    if(_this.ElementFormCellChild[parseInt(this.dataset.control)].container.Column[0].This.children.length != 0){
						    _this.ElementFormCellGraphic.listVisible[0].This.Column[0].This.children[0].setAttribute('disabled','');
						    _this.ElementFormCellGraphic.title.This.disabled = true;
						    _this.ElementFormCellGraphic.listVisibleTitle[0].This.Column[0].This.children[0].setAttribute('disabled','');
						    _this.ElementFormCellGraphic.colorTitle.Disabled = true;
						    _this.ElementFormCellGraphic.sizeTitle.This.disabled = true;
						    _this.ElementFormCellGraphic.alignTitle.This.disabled = true;
						    _this.ElementFormCellGraphic.typeGraphic.This.disabled = true;
						    for(var i = 0 ; i < _this.ElementFormCellGraphic.listbox.length ; i++){
							    _this.ElementFormCellGraphic.listbox[i].This.Column[0].This.children[0].setAttribute('disabled','');
						    }
					    }
				    }else{
					    $(_this.ElementFormCellChild[parseInt(this.dataset.control)].container.Column[0].This).html('');
				    }

			    }
		    }
	    }
	    btnCancel.This.style.float = "right";
	    btnCancel.This.style.marginLeft = "10px";

	    var btnAdd = new TVclInputbutton(ContButton.Column[0].This, "");
	    btnAdd.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "SAVE");
	    btnAdd.This.style.display = "none";
	    btnAdd.VCLType = TVCLType.BS;
	    btnAdd.This.setAttribute('data-id', idControl);
	    btnAdd.This.setAttribute('data-control', _this.ElementContentCellChild.length-1); 
	    btnAdd.onClick = function () {
		    if (txtName.Text.trim() != ''){
			    var indexTemplate = Persistence.PBITemplate.Methods.PBITEMPLATE_ListGetIndex (_this.PBITEMPLATEList, parseInt(cbTemplate.Value));
			    var stateName = true;
			    var PBITEMPLATE = _this.PBITEMPLATEList[indexTemplate];
			    for(var i = 0 ; i < PBITEMPLATE.PBITEMPLATECELLSList.length ; i++){
				    if(PBITEMPLATE.PBITEMPLATECELLSList[i].IDPBITEMPLATECELLS !== parseInt(txtId.Text)){
					    if(PBITEMPLATE.PBITEMPLATECELLSList[i].CELLS_NAME.toLowerCase() === txtName.Text.trim().toLowerCase()){
						    stateName = false;
					    }
				    }
			    }
			    if(stateName){
				    if(_this.isInteger(parseInt(txtPosition.Text))){
					    if(_this.isInteger(parseInt(_this.ElementFormCellGraphic.sizeTitle.Text))){
						    var idCell = 0;
						    _this.PanelChild = parseInt(this.dataset.control);

						    if(_this.PanelChild < 0){
							    idCell = _this.SaveCell(0, parseInt(txtPosition.Text), txtName.Text, parseInt(cbTemplate.Value));
						    }
						    else{
							    idCell = _this.SaveCell(parseInt(this.dataset.id), parseInt(txtPosition.Text), txtName.Text, parseInt(cbTemplate.Value));
						    }
						    _this.SaveDesign(idCell, parseInt(txtDesign_XS.Text), parseInt(txtDesign_S.Text), parseInt(txtDesign_M.Text), parseInt(txtDesign_L.Text), parseInt(txtDesign_XL.Text), ListBox_Row.ListBoxItems[0].Checked, ListBox_Column.ListBoxItems[0].Checked);

						    _this.SaveCellGraphic(idCell, parseInt(this.dataset.id));
						    btnAdd.This.style.display = 'none';
						    btnCancel.This.style.visibility = 'hidden';
					    }else{
						    alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NONUMBERSIZE"));
					    }
				    }else{
					    alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NONUMBERPOSITION"));
				    }
			    }else{
				    alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "REPEATEDNAME"));
			    }
		    }
		    else {
			    alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NONAME"));
		    }
	    }
	    btnAdd.This.style.float = "right";

	    var btnUpdate = new TVclInputbutton(ContButton.Column[0].This, "");
	    btnUpdate.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "UPDATE");
	    btnUpdate.This.style.display = "none";
	    btnUpdate.VCLType = TVCLType.BS;
	    btnUpdate.This.setAttribute('data-id', idControl);
	    btnUpdate.This.setAttribute('data-control', _this.ElementContentCellChild.length-1); //control
	    btnUpdate.onClick = function () {
		    _this.PanelChild = parseInt(this.dataset.control);
		    var stateNumber = true;
		    for(var i = 0 ; i < _this.ElementFormCellChild.length ; i++){
			    if(_this.ElementFormCellChild[i].idCell == parseInt(this.dataset.id)){
				    if(_this.ElementFormCellChild[i].container.Column[0].This.children.length != 0){
					    if(!_this.isInteger(parseInt(_this.ElementFormCellGraphic.sizeTitle.Text))){
						    stateNumber =  false;
					    }
				    }
			    }
		    }
		    if(_this.ElementFormCellParent.container.Column[0].This.children.length != 0){
			    if(!_this.isInteger(parseInt(_this.ElementFormCellGraphic.sizeTitle.Text))){
				    stateNumber =  false;
			    }
		    }
		    if (txtName.Text.trim() != '') {
			    var indexTemplate = Persistence.PBITemplate.Methods.PBITEMPLATE_ListGetIndex (_this.PBITEMPLATEList, parseInt(cbTemplate.Value));
			    var stateName = true;
			    var PBITEMPLATE = _this.PBITEMPLATEList[indexTemplate];
			    for(var i = 0 ; i < PBITEMPLATE.PBITEMPLATECELLSList.length ; i++){
				    if(PBITEMPLATE.PBITEMPLATECELLSList[i].IDPBITEMPLATECELLS !== parseInt(txtId.Text)){
					    if(PBITEMPLATE.PBITEMPLATECELLSList[i].CELLS_NAME.toLowerCase() === txtName.Text.trim().toLowerCase()){
						    stateName = false;
					    }
				    }
			    }
			    if(stateName){
				    if(_this.isInteger(parseInt(txtPosition.Text))){
					    if(stateNumber){
						    var state = _this.UpdateCell(parseInt(txtId.Text), txtName.Text.trim(), parseInt(txtPosition.Text), parseInt(cbTemplate.Value));
						    if(state){
							    var state_1 = _this.UpdateDesign(parseInt(txtId.Text), parseInt(txtDesign_XS.Text), parseInt(txtDesign_S.Text), parseInt(txtDesign_M.Text), parseInt(txtDesign_L.Text), parseInt(txtDesign_XL.Text), ListBox_Row.ListBoxItems[0].Checked, ListBox_Column.ListBoxItems[0].Checked);
							    if(state_1){
								    if(this.dataset.id == 'null' || this.dataset.id == ''){
									    if(_this.ElementFormCellParent.container.Column[0].This.children.length != 0){
										    _this.UpdateCellGraphic(parseInt(txtId.Text));
									    }
								    }else{
									    var idParent = parseInt(this.dataset.id);
									    for(var i = 0 ; i < _this.ElementFormCellChild.length ; i++){
										    if(_this.ElementFormCellChild[i].idCell == idParent){
											    if(_this.ElementFormCellChild[i].container.Column[0].This.children.length != 0){
												    _this.UpdateCellGraphic(parseInt(txtId.Text));
											    }
										    }
									    }
								    }
							    }
							    alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "CORRECTUPDATE"));
						    }
						    else{
							    alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECTUPDATE"));
						    }
						    btnUpdate.This.style.display = 'none';
						    btnCancel.This.style.visibility = 'hidden'
					    }else{
						    alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NONUMBERSIZE"));	
					    }
				    }else{
					    alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NONUMBERPOSITION"));
				    }
			    }else{
				    alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "REPEATEDNAME"));
			    }
		    }
		    else {
			    alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NONAME"));
		    }
	    }
	    btnUpdate.This.style.float = "right";

	    _this.ElementFormCell = { id: txtId, name: txtName, position: txtPosition, parent: cbTemplate, save: btnAdd, update: btnUpdate, cancel: btnCancel, iupdate: ImageUpdate, idelete: ImageDelete, container: ContCellGraphics, idCell: idControl};
	    _this.ElementFormDesign = { designXS: txtDesign_XS, designS: txtDesign_S, designM: txtDesign_M, designL: txtDesign_L, designXL: txtDesign_XL, designRow: ListBox_Row.ListBoxItems, designColumn: ListBox_Column.ListBoxItems};
	    if(_this.ElementContentCellChild.length-1 < 0){
		    _this.ElementFormCellParent = _this.ElementFormCell;
		    _this.ElementFormDesignParent = _this.ElementFormDesign;
	    }
	    else{
		    _this.ElementFormCellChild.push(_this.ElementFormCell);
		    _this.ElementFormDesignChild.push(_this.ElementFormDesign);
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicPBIBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.CreateFormCell", e);
    }
}

ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.ShowPreview = function (idTemplate, name) {
    var _this = this.TParent();
    try {
	    if (_this.ElementModal != null) { $(_this.ElementModal.This.This).html(''); }
	    var IDTemplate = idTemplate;
	    var Name = name;
	    var Modal2 = new TVclModal(document.body, null, "");
	    Modal2.AddHeaderContent(Name);
	    var Container = new TVclStackPanel(Modal2.Body.This, 'PBIBasic_', 1, [[12], [12], [12], [12], [12]]);
	    var PBIGraphics = new ItHelpCenter.GraphicsPBIManager.TGraphicsPBIManagerBasic(Container.Column[0].This, function () {
		    if (this.CreateObject) {
			    Modal2.This.This.style.visibility = "visible";
			    if (this.ObjTemplate.ObjHtmlClear !== undefined) {
				    this.ObjTemplate.ObjHtmlClear.id = "ClearModal";
			    }
		    }
		    else {
			    if (this.ObjTemplate.ObjHtmlClear !== undefined) {
				    this.ObjTemplate.ObjHtmlClear.id = "ClearModal";
			    }
			    Modal2.CloseModal();
			    Modal2.This.This.style.visibility = "visible";
			    _this.ElementModal = null;
		    }
	    }, IDTemplate)
	    Modal2.ShowModal();
	    Modal2.This.This.style.visibility = "hidden";
	    Modal2.FunctionClose = function () {
		    clearInterval(IntervalGraphicsPBIManager)
	    }
	    Modal2.FunctionAfterClose = function () {
		    $(this.This.This).remove();
	    }
        _this.ElementModal = Modal2;
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicPBIBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.ShowPreview", e);
    }
}

ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.AddRowCell = function (pbiGraphicList, stateMod) {
    var _this = this.TParent();
    try {
	    var PBIGraphicList = pbiGraphicList;
	    if (PBIGraphicList.length != 0) {
		    for (var j = 0; j < PBIGraphicList.length; j++) {
			    var NewRow = new TRow();
			    var Cell0 = new TCell();
			    Cell0.Value = PBIGraphicList[j].CELLS_NAME;
			    Cell0.IndexColumn = 0;
			    Cell0.IndexRow = j;
			    NewRow.AddCell(Cell0);

			    Cell0.This.setAttribute('data-id', PBIGraphicList[j].IDPBITEMPLATECELLS);
			    Cell0.This.classList.add('col-xs-12');
			    _this.ElementRowCell[j] = { row: Cell0.This };
			    if(stateMod){
				    Cell0.This.setAttribute('data-control', _this.PanelChild);
				    if(_this.PanelChild < 0 /*||_this.PanelChild == null*/){
					    _this.ElementContentCellParent.table.AddRow(NewRow);
				    }else{
					    _this.ElementContentCellChild[_this.PanelChild].table.AddRow(NewRow);
				    }
			    }else{
				    Cell0.This.setAttribute('data-control', _this.ElementContentCellChild.length-1);
				    _this.ElementContentCell.table.AddRow(NewRow);
			    }
			
			    _this.ElementRowCell[j].row.onclick = function () {
				    _this.PanelChild = parseInt(this.dataset.control);
				    if(_this.PanelChild < 0){
					    _this.ElementFormCellParent.id.This.disabled = true;
					    _this.ElementFormCellParent.name.This.disabled = true;
					    _this.ElementFormCellParent.position.This.disabled = true;
					    _this.ElementFormCellParent.parent.This.disabled = true;

					    _this.ElementFormDesignParent.designXS.This.disabled = true;
					    _this.ElementFormDesignParent.designS.This.disabled = true;
					    _this.ElementFormDesignParent.designM.This.disabled = true;
					    _this.ElementFormDesignParent.designL.This.disabled = true;
					    _this.ElementFormDesignParent.designXL.This.disabled = true;
					    _this.ElementFormDesignParent.designRow[0].This.Column[0].This.children[0].setAttribute('disabled','');
					    _this.ElementFormDesignParent.designColumn[0].This.Column[0].This.children[0].setAttribute('disabled','');

					    _this.ElementFormCellParent.save.This.style.display = 'none';
					    _this.ElementFormCellParent.update.This.style.display = 'none';
					    _this.ElementFormCellParent.cancel.This.style.visibility = 'hidden';

					    $(_this.ElementFormCellParent.container.Column[0].This).html('');
					
				    }
				    else{
					    _this.ElementFormCellChild[_this.PanelChild].id.This.disabled = true;
					    _this.ElementFormCellChild[_this.PanelChild].name.This.disabled = true;
					    _this.ElementFormCellChild[_this.PanelChild].position.This.disabled = true;
					    _this.ElementFormCellChild[_this.PanelChild].parent.This.disabled = true;

					    _this.ElementFormDesignChild[_this.PanelChild].designXS.This.disabled = true;
					    _this.ElementFormDesignChild[_this.PanelChild].designS.This.disabled = true;
					    _this.ElementFormDesignChild[_this.PanelChild].designM.This.disabled = true;
					    _this.ElementFormDesignChild[_this.PanelChild].designL.This.disabled = true;
					    _this.ElementFormDesignChild[_this.PanelChild].designXL.This.disabled = true;
					    _this.ElementFormDesignChild[_this.PanelChild].designRow[0].This.Column[0].This.children[0].setAttribute('disabled','');
					    _this.ElementFormDesignChild[_this.PanelChild].designColumn[0].This.Column[0].This.children[0].setAttribute('disabled','');

					    _this.ElementFormCellChild[_this.PanelChild].save.This.style.display = 'none';
					    _this.ElementFormCellChild[_this.PanelChild].update.This.style.display = 'none';
					    _this.ElementFormCellChild[_this.PanelChild].cancel.This.style.visibility = 'hidden';

					    $(_this.ElementFormCellChild[_this.PanelChild].container.Column[0].This).html('');
				    }
				    _this.FillCell(parseInt(this.dataset.id));
			    }

		    }
		    // if (_this.ElementFormCell != null) {
		    // 	_this.ElementFormCell.iupdate.This.style.display = 'block';
		    // 	_this.ElementFormCell.idelete.This.style.display = 'block';
		    // }
		    // _this.ElementContentCell.table.ResponsiveCont.style.display = 'block';
		    // _this.ElementContentCell.formText.classList.remove('col-md-offset-2');
		    _this.ElementContentCell.table.EnabledResizeColumn = false;

	    }else {
		    // if (_this.ElementFormMain != null) {
		    // 	_this.ElementFormMain.iupdate.This.style.display = 'none';
		    // 	_this.ElementFormMain.idelete.This.style.display = 'none';
		    // }
		    // _this.ElementContentMain.table.ResponsiveCont.style.display = 'none';
		    // _this.ElementContentMain.formText.classList.add('col-md-offset-2');
		    _this.ElementContentCell.table.EnabledResizeColumn = false;
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicPBIBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.AddRowCell", e);
    }
}

ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.FillCell = function (idCell){
	var _this = this.TParent();
	var IDCell = idCell;
    try {
	    var PBITEMPLATECELLS = new Persistence.PBITemplate.Methods.PBITEMPLATECELLS_ListSetID(_this.PBITEMPLATECELLSList, idCell);
	    for (var i = 0; i < _this.PBITEMPLATECELLSList.length; i++) {
		    if (_this.PBITEMPLATECELLSList[i].IDPBITEMPLATECELLS == IDCell) {
			    if(_this.PanelChild < 0){
				    _this.ElementFormCellParent.id.Text = _this.PBITEMPLATECELLSList[i].IDPBITEMPLATECELLS;
				    _this.ElementFormCellParent.name.Text = _this.PBITEMPLATECELLSList[i].CELLS_NAME;
				    _this.ElementFormCellParent.position.Text = _this.PBITEMPLATECELLSList[i].CELLS_POSITION;
				    /*Contenedor de los nodos*/
				    if(_this.PBITEMPLATECELLSList[i].CELLS_END){
					    _this.CreateFormCellGraphic(_this.ElementFormCellParent.container, PBITEMPLATECELLS.IDPBITEMPLATE);
					    var idCellGraphic = _this.FillCellGraphic(IDCell);
					    _this.FillColumn(idCellGraphic);
				    }
			    }
			    else{
				    _this.ElementFormCellChild[_this.PanelChild].id.Text = _this.PBITEMPLATECELLSList[i].IDPBITEMPLATECELLS;
				    _this.ElementFormCellChild[_this.PanelChild].name.Text = _this.PBITEMPLATECELLSList[i].CELLS_NAME;
				    _this.ElementFormCellChild[_this.PanelChild].position.Text = _this.PBITEMPLATECELLSList[i].CELLS_POSITION;
				    /*Contenedor de los nodos*/
				    if(_this.PBITEMPLATECELLSList[i].CELLS_END){
					    _this.CreateFormCellGraphic(_this.ElementFormCellChild[_this.PanelChild].container, PBITEMPLATECELLS.IDPBITEMPLATE);
					    var idCellGraphic = _this.FillCellGraphic(IDCell);
					    _this.FillColumn(idCellGraphic);
				    }
			    }
			    _this.FillDesign(IDCell);

			    _this.Content(_this._ObjectHtml, '', UsrCfg.Traslate.GetLangText(_this.Mythis, 'CELLCHILD'), _this.PBITEMPLATECELLSList[i].CELLS_NAME, _this.PBITEMPLATECELLSList[i].PBITEMPLATECELLS_CHILDList, IDCell);
		    }
		    // else if(_this.PBITEMPLATECELLSDESIGNList[i].IDPBITEMPLATECELLS == IDCell){

		    // }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicPBIBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.FillCell", e);
    }
}

ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.CreateFormCellGraphic = function(container, idTemplate){
    var _this = this.TParent();
    try {
	    _this.ElementFormCellGraphic = null;
	    $(container.Column[0].This).html('');

	    var ContTextGraphic = new TVclStackPanel(container.Column[0].This, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	    ContTextGraphic.Row.This.style.marginBottom = '5px';
	    var lblGraphic = new TVcllabel(ContTextGraphic.Column[0].This, "", TlabelType.H0);
	    lblGraphic.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "GRAPHICTYPE");
	    lblGraphic.VCLType = TVCLType.BS;
	    var cbGraphic = new TVclComboBox2(ContTextGraphic.Column[1].This, "");
	    cbGraphic.VCLType = TVCLType.BS;
	    var aux = null;
	    for (var i = 0; i < _this.PBIGRAPHICTYPEList.length; i++) {
		    var ComboItem = new TVclComboBoxItem();
		    ComboItem.Value = _this.PBIGRAPHICTYPEList[i].IDPBIGRAPHICTYPE;
		    ComboItem.Text = _this.PBIGRAPHICTYPEList[i].GRAPHIC_NAME;
		    ComboItem.Tag = "Test";
		    cbGraphic.AddItem(ComboItem);
	    }
	    cbGraphic.selectedIndex = aux;
	    cbGraphic.This.classList.add("DropDownListGroup");
	    cbGraphic.This.disabled = true;

	    var ContTextTitle = new TVclStackPanel(container.Column[0].This, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	    ContTextTitle.Row.This.style.marginBottom = '5px';
	    var lblTitle = new TVcllabel(ContTextTitle.Column[0].This, "", TlabelType.H0);
	    lblTitle.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TITLE");
	    lblTitle.VCLType = TVCLType.BS;
	    var txtTitle = new TVclTextBox(ContTextTitle.Column[1].This, "");
	    txtTitle.Text = "";
	    txtTitle.This.disabled = true;
	    txtTitle.VCLType = TVCLType.BS;

	    var ContTextColorTitle = new TVclStackPanel(container.Column[0].This, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	    ContTextColorTitle.Row.This.style.marginBottom = '5px';
	    var lblColorTitle = new TVcllabel(ContTextColorTitle.Column[0].This, "", TlabelType.H0);
	    lblColorTitle.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "COLORTITLE");
	    lblColorTitle.VCLType = TVCLType.BS;
	    var colorTitle = new Componet.TPalleteColor(ContTextColorTitle.Column[1].This, "");
	    colorTitle.Color = "white";
	    colorTitle.Disabled = true;
	    colorTitle.CreatePallete();

	    var ContTextSizeTitle = new TVclStackPanel(container.Column[0].This, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	    ContTextSizeTitle.Row.This.style.marginBottom = '5px';
	    var lblSizeTitle = new TVcllabel(ContTextSizeTitle.Column[0].This, "", TlabelType.H0);
	    lblSizeTitle.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "SIZETITLE");
	    lblSizeTitle.VCLType = TVCLType.BS;
	    var txtSizeTitle = new TVclTextBox(ContTextSizeTitle.Column[1].This, "");
	    txtSizeTitle.Text = "";
	    txtSizeTitle.This.disabled = true;
	    txtSizeTitle.VCLType = TVCLType.BS;
	    txtSizeTitle.This.setAttribute("type", "number");
	    txtSizeTitle.This.setAttribute("min", "0");

	    var alignArray = ['Left','Right', 'Center'];
	    var ContTextAlign = new TVclStackPanel(container.Column[0].This, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
	    ContTextAlign.Row.This.style.marginBottom = '5px';
	    var lblAlign = new TVcllabel(ContTextAlign.Column[0].This, "", TlabelType.H0);
	    lblAlign.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ALIGNTITLE");
	    lblAlign.VCLType = TVCLType.BS;
	    var cbAlign = new TVclComboBox2(ContTextAlign.Column[1].This, "");
	    cbAlign.VCLType = TVCLType.BS;
	    for (var i = 0; i < alignArray.length; i++) {
		    var ComboItem = new TVclComboBoxItem();
		    ComboItem.Value = i;
		    ComboItem.Text = alignArray[i];;
		    ComboItem.Tag = "Test";
		    cbAlign.AddItem(ComboItem);
	    }
	    cbAlign.selectedIndex = 0;
	    cbAlign.This.classList.add("DropDownListGroup");
	    cbAlign.This.disabled = true;

	    var ContVisible = new TVclStackPanel(container.Column[0].This, "", 2, [[12, 12], [6, 6], [6, 6], [6, 6], [6, 6]]);
	    ContVisible.Row.This.style.marginBottom = '5px';
	    var ListBoxVisible = new TVclListBox(ContVisible.Column[0].This, "");
	    ListBoxVisible.EnabledCheckBox = true;
	    var ListBoxItem_Visible = new TVclListBoxItem();
	    ListBoxItem_Visible.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "VISIBLEGRAPHIC");
	    ListBoxItem_Visible.Index = 0;
	    ListBoxVisible.AddListBoxItem(ListBoxItem_Visible);
	    ListBoxVisible.ListBoxItems[0].This.Column[0].This.children[0].setAttribute('disabled','');

	    var ListBoxVisibleTitle = new TVclListBox(ContVisible.Column[1].This, "");
	    ListBoxVisibleTitle.EnabledCheckBox = true;
	    var ListBoxItem_VisibleTitle = new TVclListBoxItem();
	    ListBoxItem_VisibleTitle.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "VISIBLETITLE");
	    ListBoxItem_VisibleTitle.Index = 0;
	    ListBoxVisibleTitle.AddListBoxItem(ListBoxItem_VisibleTitle);
	    ListBoxVisibleTitle.ListBoxItems[0].This.Column[0].This.children[0].setAttribute('disabled','');

	    var ContTextColumn = new TVclStackPanel(container.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
	    ContTextColumn.Row.This.style.marginBottom = '5px';

	    var PBITEMPLATE = new Persistence.PBITemplate.Methods.PBITEMPLATE_ListSetID(_this.PBITEMPLATEList, idTemplate);
	    var query = _this.getQuery(PBITEMPLATE.TEMPLATE_DATASQL);
	    var ListBox = new TVclListBox(ContTextColumn.Column[0].This, "");
	    ListBox.EnabledCheckBox = true;
	
	    for(var i = 0 ; i < query.Result.FieldCount ; i++ ){
		    var ListBoxItem = new TVclListBoxItem();
		    ListBoxItem.Text = query.Result.FieldDefs[i].FieldName;
		    ListBoxItem.Index = i;
		    ListBox.AddListBoxItem(ListBoxItem);
		    ListBox.ListBoxItems[i].This.Column[0].This.children[0].setAttribute('disabled','');
	    }

        _this.ElementFormCellGraphic = { typeGraphic: cbGraphic, title: txtTitle, colorTitle: colorTitle, sizeTitle: txtSizeTitle, alignTitle: cbAlign, listVisible: ListBoxVisible.ListBoxItems, listVisibleTitle: ListBoxVisibleTitle.ListBoxItems, listbox: ListBox.ListBoxItems };
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicPBIBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.CreateFormCellGraphic", e);
    }
}

ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.FillCellGraphic = function(idCell){
	var _this = this.TParent();
    var IDCell = idCell;
    try {
	    for (var i = 0; i < _this.PBITEMPLATECELLGRAPHICList.length; i++) {
		    if (_this.PBITEMPLATECELLGRAPHICList[i].IDPBITEMPLATECELLS == IDCell) {

			    _this.ElementFormCellGraphic.listVisible[0].Checked = _this.PBITEMPLATECELLGRAPHICList[i].CELLGRAPHIC_VISIBLE;
			    _this.ElementFormCellGraphic.title.Text = _this.PBITEMPLATECELLGRAPHICList[i].CELLGRAPHIC_TITLE;
			    _this.ElementFormCellGraphic.listVisibleTitle[0].Checked = _this.PBITEMPLATECELLGRAPHICList[i].CELLGRAPHIC_VISIBLETITLE;
			    _this.ElementFormCellGraphic.colorTitle.Color = _this.PBITEMPLATECELLGRAPHICList[i].CELLGRAPHIC_COLORTITLE;
			    _this.ElementFormCellGraphic.sizeTitle.Text = parseInt(_this.PBITEMPLATECELLGRAPHICList[i].CELLGRAPHIC_SIZETITLE);
			    for(var j = 0 ; j < _this.ElementFormCellGraphic.alignTitle.Options.length; j++){
				    if(_this.ElementFormCellGraphic.alignTitle.Options[j].Text.toLowerCase() == _this.PBITEMPLATECELLGRAPHICList[i].CELLGRAPHIC_ALIGNTITLE.toLowerCase()){
					    _this.ElementFormCellGraphic.alignTitle.selectedIndex = j;
				    }
			    }
			    for (var j = 0; j < _this.ElementFormCellGraphic.typeGraphic.Options.length; j++) {
				    if (parseInt(_this.ElementFormCellGraphic.typeGraphic.Options[j].Value) == _this.PBITEMPLATECELLGRAPHICList[i].IDPBIGRAPHICTYPE) {
					    _this.ElementFormCellGraphic.typeGraphic.selectedIndex = j;
				    }
			    }
			    return _this.PBITEMPLATECELLGRAPHICList[i].IDPBITEMPLATECELLGRAPHIC;
		    }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicPBIBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.FillCellGraphic", e);
    }
}

ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.FillDesign = function (idCell){
	var _this = this.TParent();
    var IDCell = idCell;
    try {
	    for (var i = 0; i < _this.PBITEMPLATECELLSDESIGNList.length; i++) {
		    if (_this.PBITEMPLATECELLSDESIGNList[i].IDPBITEMPLATECELLS == IDCell) {
			    if(_this.PanelChild < 0){
				    for(var j = 0 ; j <= 12 ; j++){
					    if(parseInt(_this.ElementFormDesignParent.designXS.Options[j].Text) == _this.PBITEMPLATECELLSDESIGNList[i].DESIGN_XS){
						    _this.ElementFormDesignParent.designXS.selectedIndex = j;
					    }
					    if(parseInt(_this.ElementFormDesignParent.designS.Options[j].Text) == _this.PBITEMPLATECELLSDESIGNList[i].DESIGN_S){
						    _this.ElementFormDesignParent.designS.selectedIndex = j;
					    }
					    if(parseInt(_this.ElementFormDesignParent.designM.Options[j].Text) == _this.PBITEMPLATECELLSDESIGNList[i].DESIGN_M){
						    _this.ElementFormDesignParent.designM.selectedIndex = j;
					    }
					    if(parseInt(_this.ElementFormDesignParent.designL.Options[j].Text) == _this.PBITEMPLATECELLSDESIGNList[i].DESIGN_L){
						    _this.ElementFormDesignParent.designL.selectedIndex = j;
					    }
					    if(parseInt(_this.ElementFormDesignParent.designXL.Options[j].Text) == _this.PBITEMPLATECELLSDESIGNList[i].DESIGN_XL){
						    _this.ElementFormDesignParent.designXL.selectedIndex = j;
					    }
				    }

				    _this.ElementFormDesignParent.designRow[0].Checked = _this.PBITEMPLATECELLSDESIGNList[i].DESIGN_ROW;
				    _this.ElementFormDesignParent.designColumn[0].Checked = _this.PBITEMPLATECELLSDESIGNList[i].DESIGN_COLUMN;
			    }
			    else{
				    for(var j = 0 ; j <= 12 ; j++){
					    if(parseInt(_this.ElementFormDesignChild[_this.PanelChild].designXS.Options[j].Text) == _this.PBITEMPLATECELLSDESIGNList[i].DESIGN_XS){
						    _this.ElementFormDesignChild[_this.PanelChild].designXS.selectedIndex = j;
					    }
					    if(parseInt(_this.ElementFormDesignChild[_this.PanelChild].designS.Options[j].Text) == _this.PBITEMPLATECELLSDESIGNList[i].DESIGN_S){
						    _this.ElementFormDesignChild[_this.PanelChild].designS.selectedIndex = j;
					    }
					    if(parseInt(_this.ElementFormDesignChild[_this.PanelChild].designM.Options[j].Text) == _this.PBITEMPLATECELLSDESIGNList[i].DESIGN_M){
						    _this.ElementFormDesignChild[_this.PanelChild].designM.selectedIndex = j;
					    }
					    if(parseInt(_this.ElementFormDesignChild[_this.PanelChild].designL.Options[j].Text) == _this.PBITEMPLATECELLSDESIGNList[i].DESIGN_L){
						    _this.ElementFormDesignChild[_this.PanelChild].designL.selectedIndex = j;
					    }
					    if(parseInt(_this.ElementFormDesignChild[_this.PanelChild].designXL.Options[j].Text) == _this.PBITEMPLATECELLSDESIGNList[i].DESIGN_XL){
						    _this.ElementFormDesignChild[_this.PanelChild].designXL.selectedIndex = j;
					    }
				    }

				    _this.ElementFormDesignChild[_this.PanelChild].designRow[0].Checked = _this.PBITEMPLATECELLSDESIGNList[i].DESIGN_ROW;
				    _this.ElementFormDesignChild[_this.PanelChild].designColumn[0].Checked = _this.PBITEMPLATECELLSDESIGNList[i].DESIGN_COLUMN;
			    }
		    }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicPBIBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.FillDesign", e);
    }
}

ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.FillColumn = function (idCellGraphic){
    var _this = this.TParent();
    try {
	    for (var i = 0; i < _this.PBITEMPLATECELLGRAPHIC_COLUMNList.length; i++) {
		    if (_this.PBITEMPLATECELLGRAPHIC_COLUMNList[i].IDPBITEMPLATECELLGRAPHIC == idCellGraphic) {
			    for(var j = 0 ; j < _this.ElementFormCellGraphic.listbox.length ; j++){
				    if(_this.ElementFormCellGraphic.listbox[j].Text === _this.PBITEMPLATECELLGRAPHIC_COLUMNList[i].COLUMN_NAME){
					    _this.ElementFormCellGraphic.listbox[j].Checked = true;
				    }
			    }
		    }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicPBIBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.FillColumn", e);
    }
}

ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.SaveCell = function (parentCell, positionCell, nameCell, idTemplate){
    var _this = this.TParent();
    try {
	    var PBITEMPLATECELLS = new Persistence.PBITemplate.Properties.TPBITEMPLATECELLS();
	    PBITEMPLATECELLS.IDPBITEMPLATECELLS_PARENT = parentCell;
	    PBITEMPLATECELLS.CELLS_POSITION = positionCell;
	    PBITEMPLATECELLS.CELLS_NAME = nameCell;
	    PBITEMPLATECELLS.CELLS_END = false;
	    PBITEMPLATECELLS.IDPBITEMPLATE = idTemplate;
	    var success = Persistence.PBITemplate.Methods.PBITEMPLATECELLS_ADD(PBITEMPLATECELLS);
	    if(success){
		    _this.ListLoad();
		    if(_this.PanelChild < 0){
			    _this.ElementFormCellParent.id.Text = '';
			    _this.ElementFormCellParent.name.Text = '';
			    _this.ElementFormCellParent.position.Text = '0';
			    _this.ElementFormCellParent.id.This.disabled = true;
			    _this.ElementFormCellParent.name.This.disabled = true;
			    _this.ElementFormCellParent.position.This.disabled = true;
			    _this.ElementFormCellParent.parent.This.disabled = true;
			    if(PBITEMPLATECELLS.IDPBITEMPLATE == parseInt(_this.ElementFormTemplate.id.Text)){
				    for(var i = 0 ; i < _this.ElementContentCellParent.table._Rows.length; i++){
					    $(_this.ElementContentCellParent.table._Rows[i].This).remove();
				    }
				    for (var i = 0; i < _this.PBITEMPLATEList.length; i++) {
					    if (_this.PBITEMPLATEList[i].IDPBITEMPLATE == PBITEMPLATECELLS.IDPBITEMPLATE) {
						    _this.AddRowCell(_this.PBITEMPLATEList[i].PBITEMPLATECELLSPARENTList, true);
					    }
				    }
			    }
			    return PBITEMPLATECELLS.IDPBITEMPLATECELLS;
		    }
		    else{
			    _this.ElementFormCellChild[_this.PanelChild].id.Text = '';
			    _this.ElementFormCellChild[_this.PanelChild].name.Text = '';
			    _this.ElementFormCellChild[_this.PanelChild].position.Text = '0';
			    _this.ElementFormCellChild[_this.PanelChild].id.This.disabled = true;
			    _this.ElementFormCellChild[_this.PanelChild].name.This.disabled = true;
			    _this.ElementFormCellChild[_this.PanelChild].position.This.disabled = true;
			    _this.ElementFormCellChild[_this.PanelChild].parent.This.disabled = true;

			    for(var i = 0 ; i < _this.ElementContentCellChild[_this.PanelChild].table._Rows.length; i++){
				    $(_this.ElementContentCellChild[_this.PanelChild].table._Rows[i].This).remove();
			    }
			    for (var i = 0; i < _this.PBITEMPLATECELLSList.length; i++) {
				    if (_this.PBITEMPLATECELLSList[i].IDPBITEMPLATECELLS == PBITEMPLATECELLS.IDPBITEMPLATECELLS_PARENT) {
					    _this.AddRowCell(_this.PBITEMPLATECELLSList[i].PBITEMPLATECELLS_CHILDList, true);
				    }
			    }
			    return PBITEMPLATECELLS.IDPBITEMPLATECELLS;
		    }
	    }
	    else{
		    alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECTSAVE"));
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicPBIBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.SaveCell", e);
    }
}

ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.SaveDesign = function(idCell, designXS, designS, designM, designL, designXL, designRow, designColumn){
    var _this = this.TParent();
    try {
	    var PBITEMPLATECELLSDESIGN = new Persistence.PBITemplate.Properties.TPBITEMPLATECELLSDESIGN();
	    PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLS = idCell;
	    PBITEMPLATECELLSDESIGN.DESIGN_XS = designXS;
	    PBITEMPLATECELLSDESIGN.DESIGN_S = designS;
	    PBITEMPLATECELLSDESIGN.DESIGN_M = designM;
	    PBITEMPLATECELLSDESIGN.DESIGN_L = designL;
	    PBITEMPLATECELLSDESIGN.DESIGN_XL = designXL;
	    PBITEMPLATECELLSDESIGN.DESIGN_ROW = designRow;
	    PBITEMPLATECELLSDESIGN.DESIGN_COLUMN = designColumn;
	    var success = Persistence.PBITemplate.Methods.PBITEMPLATECELLSDESIGN_ADD(PBITEMPLATECELLSDESIGN);
	    if(success){
		    _this.ListLoad();
		    if(_this.PanelChild < 0){
			    _this.ElementFormDesignParent.designXS.selectedIndex = 0;
			    _this.ElementFormDesignParent.designS.selectedIndex = 0;
			    _this.ElementFormDesignParent.designM.selectedIndex = 0;
			    _this.ElementFormDesignParent.designL.selectedIndex = 0;
			    _this.ElementFormDesignParent.designXL.selectedIndex = 0;
			    _this.ElementFormDesignParent.designRow[0].Checked = false;
			    _this.ElementFormDesignParent.designColumn[0].Checked = false;
			    _this.ElementFormDesignParent.designXS.This.disabled = true;
			    _this.ElementFormDesignParent.designS.This.disabled = true;
			    _this.ElementFormDesignParent.designM.This.disabled = true;
			    _this.ElementFormDesignParent.designL.This.disabled = true;
			    _this.ElementFormDesignParent.designXL.This.disabled = true;
			    _this.ElementFormDesignParent.designRow[0].This.Column[0].This.children[0].setAttribute('disabled','');
			    _this.ElementFormDesignParent.designColumn[0].This.Column[0].This.children[0].setAttribute('disabled','');
		    }else{
			    _this.ElementFormDesignChild[_this.PanelChild].designXS.selectedIndex = 0;
			    _this.ElementFormDesignChild[_this.PanelChild].designS.selectedIndex = 0;
			    _this.ElementFormDesignChild[_this.PanelChild].designM.selectedIndex = 0;
			    _this.ElementFormDesignChild[_this.PanelChild].designL.selectedIndex = 0;
			    _this.ElementFormDesignChild[_this.PanelChild].designXL.selectedIndex = 0;
			    _this.ElementFormDesignChild[_this.PanelChild].designRow[0].Checked = false;
			    _this.ElementFormDesignChild[_this.PanelChild].designColumn[0].Checked = false;
			    _this.ElementFormDesignChild[_this.PanelChild].designXS.This.disabled = true;
			    _this.ElementFormDesignChild[_this.PanelChild].designS.This.disabled = true;
			    _this.ElementFormDesignChild[_this.PanelChild].designM.This.disabled = true;
			    _this.ElementFormDesignChild[_this.PanelChild].designL.This.disabled = true;
			    _this.ElementFormDesignChild[_this.PanelChild].designXL.This.disabled = true;
			    _this.ElementFormDesignChild[_this.PanelChild].designRow[0].This.Column[0].This.children[0].setAttribute('disabled','');
			    _this.ElementFormDesignChild[_this.PanelChild].designColumn[0].This.Column[0].This.children[0].setAttribute('disabled','');
		    }

	    }else{
		    alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECTSAVE"));
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicPBIBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.SaveDesign", e);
    }
}

ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.SaveCellGraphic = function(idCell, idParent){
	var _this = this.TParent();
	// HACER QUE NO ELIMINE SI NO QUE EDITE LOS CAMPOS YA QUE EL ID SE NECESITA PARA EL COLUMN
    var ind = null;
    try {
	    for(var i = 0 ; i < _this.PBITEMPLATECELLGRAPHICList.length ; i++){
		    if(_this.PBITEMPLATECELLGRAPHICList[i].IDPBITEMPLATECELLS == idParent){
			    ind = i;
		    }
	    }
	    if(ind != null){
		    // editar y no elimnar
		    var idCellGraphic = _this.PBITEMPLATECELLGRAPHICList[ind].IDPBITEMPLATECELLGRAPHIC;
		    var PBITEMPLATECELLGRAPHIC = new Persistence.PBITemplate.Properties.TPBITEMPLATECELLGRAPHIC();
		    PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLGRAPHIC = idCellGraphic;
		    PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLS = idCell;
		    PBITEMPLATECELLGRAPHIC.IDPBIGRAPHICTYPE = parseInt(_this.ElementFormCellGraphic.typeGraphic.Value);
		    PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_VISIBLE = _this.ElementFormCellGraphic.listVisible[0].Checked;
		    PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_TITLE = _this.ElementFormCellGraphic.title.Text;
		    PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_VISIBLETITLE = _this.ElementFormCellGraphic.listVisibleTitle[0].Checked;
		    PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_COLORTITLE = _this.ElementFormCellGraphic.colorTitle.Color;
		    PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_SIZETITLE = _this.ElementFormCellGraphic.sizeTitle.Text + 'px';
		    PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_ALIGNTITLE = _this.ElementFormCellGraphic.alignTitle.Text;
		    var success = Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_UPD(PBITEMPLATECELLGRAPHIC);
		    if(success){
			    /*Actualizar el PBITEMPLATECELLS a cell final y cambiar el anterior*/
			    // before
			    var PBITEMPLATECELLS_PARENT = Persistence.PBITemplate.Methods.PBITEMPLATECELLS_ListSetID(_this.PBITEMPLATECELLSList, idParent);
			    PBITEMPLATECELLS_PARENT.CELLS_END = false;
			    var successParent = Persistence.PBITemplate.Methods.PBITEMPLATECELLS_UPD(PBITEMPLATECELLS_PARENT);
			    // after
			    if(successParent){
				    var PBITEMPLATECELLS = Persistence.PBITemplate.Methods.PBITEMPLATECELLS_ListSetID(_this.PBITEMPLATECELLSList, idCell);
				    PBITEMPLATECELLS.CELLS_END = true;
				    Persistence.PBITemplate.Methods.PBITEMPLATECELLS_UPD(PBITEMPLATECELLS);
			    }
			    _this.ListLoad();
			    var success_1 = _this.SaveCellColumn(idCellGraphic);
			    if(success_1){
				    alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "CORRECTSAVE"));
			    }
			    if(_this.PanelChild < 0){
					    $(_this.ElementFormCellParent.container.Column[0].This).html('');
			    }else{
				    $(_this.ElementFormCellChild[_this.PanelChild].container.Column[0].This).html('');
			    }
		    }
	    }
	    else{
		    var PBITEMPLATECELLGRAPHIC = new Persistence.PBITemplate.Properties.TPBITEMPLATECELLGRAPHIC();
		    PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLS = idCell;
		    PBITEMPLATECELLGRAPHIC.IDPBIGRAPHICTYPE = parseInt(_this.ElementFormCellGraphic.typeGraphic.Value);
		    PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_VISIBLE = _this.ElementFormCellGraphic.listVisible[0].Checked;
		    PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_TITLE = _this.ElementFormCellGraphic.title.Text;
		    PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_VISIBLETITLE = _this.ElementFormCellGraphic.listVisibleTitle[0].Checked;
		    PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_COLORTITLE = _this.ElementFormCellGraphic.colorTitle.Color;
		    PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_SIZETITLE = _this.ElementFormCellGraphic.sizeTitle.Text + 'px';
		    PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_ALIGNTITLE = _this.ElementFormCellGraphic.alignTitle.Text;
		    var success = Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_ADD(PBITEMPLATECELLGRAPHIC);

		    if(success){
			    // Actualizar el PBITEMPLATECELLS a cell final
			    var PBITEMPLATECELLS = Persistence.PBITemplate.Methods.PBITEMPLATECELLS_ListSetID(_this.PBITEMPLATECELLSList, idCell);
			    PBITEMPLATECELLS.CELLS_END = true;
			    Persistence.PBITemplate.Methods.PBITEMPLATECELLS_UPD(PBITEMPLATECELLS);
			    _this.ListLoad();
			    var success_1 = _this.SaveCellColumn(PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLGRAPHIC);
			    if(success_1){
				    alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "CORRECTSAVE"));
			    }
			    if(_this.PanelChild < 0){
					    $(_this.ElementFormCellParent.container.Column[0].This).html('');
			    }else{
				    $(_this.ElementFormCellChild[_this.PanelChild].container.Column[0].This).html('');
			    }
		    }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicPBIBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.SaveCellGraphic", e);
    }
}

ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.SaveCellColumn = function(idCellGraphic){
	var _this = this.TParent();
    var state = true;
    try {
	    for(var i = 0 ; i < _this.PBITEMPLATECELLGRAPHIC_COLUMNList.length ; i++){
		    if(_this.PBITEMPLATECELLGRAPHIC_COLUMNList[i].IDPBITEMPLATECELLGRAPHIC == idCellGraphic){
			    // eliminar todos
			    Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_COLUMN_DEL(_this.PBITEMPLATECELLGRAPHIC_COLUMNList[i].IDPBITEMPLATECELLGRAPHIC_COLUMN);
		    }
	    }
	    for(var i = 0 ; i < _this.ElementFormCellGraphic.listbox.length ; i++){
		    if(_this.ElementFormCellGraphic.listbox[i].Checked){
			    var PBITEMPLATECELLGRAPHIC_COLUMN = new Persistence.PBITemplate.Properties.TPBITEMPLATECELLGRAPHIC_COLUMN();			
			    PBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC = idCellGraphic;
			    PBITEMPLATECELLGRAPHIC_COLUMN.COLUMN_NAME = _this.ElementFormCellGraphic.listbox[i].Text;
			    var success = Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_COLUMN_ADD(PBITEMPLATECELLGRAPHIC_COLUMN); 
			    if(success){
				    _this.ListLoad();
			    }
			    else{
				    state = false;
				    alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECTSAVE"));
			    }
		    }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicPBIBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.SaveCellColumn", e);
    }
	return state;
}

ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.UpdateCell = function(idCell, nameCell, positionCell, idTemplate){
    var _this = this.TParent();
    try {
	    var cells = new Persistence.PBITemplate.Methods.PBITEMPLATECELLS_ListSetID(_this.PBITEMPLATECELLSList, idCell);
	    var PBITEMPLATECELLS = new Persistence.PBITemplate.Properties.TPBITEMPLATECELLS();
	    PBITEMPLATECELLS.IDPBITEMPLATECELLS = idCell;
	    PBITEMPLATECELLS.IDPBITEMPLATECELLS_PARENT = cells.IDPBITEMPLATECELLS_PARENT;
	    PBITEMPLATECELLS.CELLS_POSITION = positionCell;
	    PBITEMPLATECELLS.CELLS_NAME = nameCell;
	    PBITEMPLATECELLS.CELLS_END = cells.CELLS_END;
	    PBITEMPLATECELLS.IDPBITEMPLATE = idTemplate;
	    var success = Persistence.PBITemplate.Methods.PBITEMPLATECELLS_UPD(PBITEMPLATECELLS);
	    if(success){
		    _this.ListLoad();
		    if(_this.PanelChild < 0){
			    _this.ElementFormCellParent.position.Text = PBITEMPLATECELLS.CELLS_POSITION;
			    _this.ElementFormCellParent.id.This.disabled = true;
			    _this.ElementFormCellParent.name.This.disabled = true;
			    _this.ElementFormCellParent.position.This.disabled = true;
			    _this.ElementFormCellParent.parent.This.disabled = true;
			    _this.ElementFormDesignParent.designXS.This.disabled = true;
			    _this.ElementFormDesignParent.designS.This.disabled = true;
			    _this.ElementFormDesignParent.designM.This.disabled = true;
			    _this.ElementFormDesignParent.designL.This.disabled = true;
			    _this.ElementFormDesignParent.designXL.This.disabled = true;
			    _this.ElementFormDesignParent.designRow[0].This.Column[0].This.children[0].setAttribute('disabled','');
			    _this.ElementFormDesignParent.designColumn[0].This.Column[0].This.children[0].setAttribute('disabled','');

			    /*Eliminaci�n y llenado de Celdas de la tabla*/
			    for(var i = 0 ; i < _this.ElementContentCellParent.table._Rows.length; i++){
				    $(_this.ElementContentCellParent.table._Rows[i].This).remove();
			    }
			    var indexTemplate = Persistence.PBITemplate.Methods.PBITEMPLATE_ListGetIndex(_this.PBITEMPLATEList, parseInt(_this.ElementFormTemplate.id.Text));
			    _this.AddRowCell(_this.PBITEMPLATEList[indexTemplate].PBITEMPLATECELLSPARENTList, true);
			    if(PBITEMPLATECELLS.IDPBITEMPLATE != parseInt(_this.ElementFormTemplate.id.Text)){
				    _this.ElementFormCellParent.id.Text = '';
				    _this.ElementFormCellParent.name.Text = '';
				    _this.ElementFormCellParent.position.Text = '0';
				    _this.ElementFormDesignParent.designXS.selectedIndex = 0;
				    _this.ElementFormDesignParent.designS.selectedIndex= 0;
				    _this.ElementFormDesignParent.designM.selectedIndex = 0;
				    _this.ElementFormDesignParent.designL.selectedIndex = 0;
				    _this.ElementFormDesignParent.designXL.selectedIndex= 0;
				    for (var j = 0; j < _this.ElementFormCellParent.parent.Options.length; j++) {
					    if (parseInt(_this.ElementFormCellParent.parent.Options[j].Value) == parseInt(_this.ElementFormTemplate.id.Text)) {
						    _this.ElementFormCellParent.parent.selectedIndex = j;
					    }
				    }
				    $(_this.ElementFormCellParent.container.Column[0].This).html('');
				    for(var  i = 0 ; i < _this.ElementContentCellChild.length ; i++){
					    $(_this.ElementContentCellChild[i].form).remove();
				    }
			    }

		    }else{
			    _this.ElementFormCellChild[_this.PanelChild].position.Text = PBITEMPLATECELLS.CELLS_POSITION;
			    _this.ElementFormCellChild[_this.PanelChild].id.This.disabled = true;
			    _this.ElementFormCellChild[_this.PanelChild].name.This.disabled = true;
			    _this.ElementFormCellChild[_this.PanelChild].position.This.disabled = true;
			    _this.ElementFormCellChild[_this.PanelChild].parent.This.disabled = true;

			    _this.ElementFormDesignChild[_this.PanelChild].designXS.This.disabled = true;
			    _this.ElementFormDesignChild[_this.PanelChild].designS.This.disabled = true;
			    _this.ElementFormDesignChild[_this.PanelChild].designM.This.disabled = true;
			    _this.ElementFormDesignChild[_this.PanelChild].designL.This.disabled = true;
			    _this.ElementFormDesignChild[_this.PanelChild].designXL.This.disabled = true;
			    _this.ElementFormDesignChild[_this.PanelChild].designRow[0].This.Column[0].This.children[0].setAttribute('disabled','');
			    _this.ElementFormDesignChild[_this.PanelChild].designColumn[0].This.Column[0].This.children[0].setAttribute('disabled','');

			    for(var i = 0 ; i < _this.ElementContentCellChild[_this.PanelChild].table._Rows.length; i++){
				    $(_this.ElementContentCellChild[_this.PanelChild].table._Rows[i].This).remove();
			    }
			    for (var i = 0; i < _this.PBITEMPLATECELLSList.length; i++) {
				    if (_this.PBITEMPLATECELLSList[i].IDPBITEMPLATECELLS == PBITEMPLATECELLS.IDPBITEMPLATECELLS_PARENT) {
					    _this.AddRowCell(_this.PBITEMPLATECELLSList[i].PBITEMPLATECELLS_CHILDList, true);
				    }
			    }
		    }
		    for(var i = 0 ; i < _this.ElementContentCellChild.length ; i++){
			    if(_this.ElementContentCellChild[i].idCell == idCell){
				    _this.ElementContentCellChild[i].title.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, 'CELLCHILD') + ': ' + nameCell;
			    }
		    }
	    }
        return success;
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicPBIBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.UpdateCell", e);
    }
}

ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.UpdateDesign = function(idCell, designXS, designS, designM, designL, designXL, designRow, designColumn){
    var _this = this.TParent();
    try {
	    var PBITEMPLATECELLSDESIGN = new Persistence.PBITemplate.Properties.TPBITEMPLATECELLSDESIGN();
	    for(var i = 0 ; i < _this.PBITEMPLATECELLSDESIGNList.length ; i++){
		    if(_this.PBITEMPLATECELLSDESIGNList[i].IDPBITEMPLATECELLS == idCell){
			    PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLSDESIGN = _this.PBITEMPLATECELLSDESIGNList[i].IDPBITEMPLATECELLSDESIGN;	
		    }
	    }
	    PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLS = idCell;
	    PBITEMPLATECELLSDESIGN.DESIGN_XS = designXS;
	    PBITEMPLATECELLSDESIGN.DESIGN_S = designS;
	    PBITEMPLATECELLSDESIGN.DESIGN_M = designM;
	    PBITEMPLATECELLSDESIGN.DESIGN_L = designL;
	    PBITEMPLATECELLSDESIGN.DESIGN_XL = designXL;
	    PBITEMPLATECELLSDESIGN.DESIGN_ROW = designRow;
	    PBITEMPLATECELLSDESIGN.DESIGN_COLUMN = designColumn;

	    var success = Persistence.PBITemplate.Methods.PBITEMPLATECELLSDESIGN_UPD(PBITEMPLATECELLSDESIGN);
	    if(success){
		    _this.ListLoad();
	    }
        return success;
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicPBIBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.UpdateDesign", e);
    }
}

ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.UpdateCellGraphic = function(idCell){
    var _this = this.TParent();
    try {
	    var PBITEMPLATECELLGRAPHIC = new Persistence.PBITemplate.Properties.TPBITEMPLATECELLGRAPHIC();
	    for(var i = 0 ; i < _this.PBITEMPLATECELLGRAPHICList.length ; i++){
		    if(_this.PBITEMPLATECELLGRAPHICList[i].IDPBITEMPLATECELLS == idCell){
			    PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLGRAPHIC = _this.PBITEMPLATECELLGRAPHICList[i].IDPBITEMPLATECELLGRAPHIC;	
		    }
	    }
	    PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLS = idCell;
	    PBITEMPLATECELLGRAPHIC.IDPBIGRAPHICTYPE = parseInt(_this.ElementFormCellGraphic.typeGraphic.Value);
	    PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_VISIBLE = _this.ElementFormCellGraphic.listVisible[0].Checked;
	    PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_TITLE = _this.ElementFormCellGraphic.title.Text;
	    PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_VISIBLETITLE = _this.ElementFormCellGraphic.listVisibleTitle[0].Checked;
	    PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_COLORTITLE = _this.ElementFormCellGraphic.colorTitle.Color;
	    PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_SIZETITLE = _this.ElementFormCellGraphic.sizeTitle.Text + 'px';
	    PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_ALIGNTITLE = _this.ElementFormCellGraphic.alignTitle.Text;
	    if(PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLGRAPHIC > 0){
		    var success = Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_UPD(PBITEMPLATECELLGRAPHIC);
	    }
	    else{
		    var success = Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_ADD(PBITEMPLATECELLGRAPHIC);
	    }
	    if(success){
		    /*actualiza a celda final*/
		    _this.ListLoad();
		    for(var i = 0 ; i < _this.PBITEMPLATECELLGRAPHICList.length ; i++){
			    if(_this.PBITEMPLATECELLGRAPHICList[i].IDPBITEMPLATECELLS == idCell){
				    var PBITEMPLATECELLS = Persistence.PBITemplate.Methods.PBITEMPLATECELLS_ListSetID(_this.PBITEMPLATECELLSList, idCell);
				    PBITEMPLATECELLS.CELLS_END = true;
				    Persistence.PBITemplate.Methods.PBITEMPLATECELLS_UPD(PBITEMPLATECELLS);
			    }
		    }
		
		    _this.ListLoad();
		    _this.UpdateCellColumn(PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLGRAPHIC);
		    _this.ElementFormCellGraphic.sizeTitle.Text = parseInt(PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_SIZETITLE);
		    _this.ElementFormCellGraphic.listVisible[0].This.Column[0].This.children[0].setAttribute('disabled','');
		    _this.ElementFormCellGraphic.title.This.disabled = true;
		    _this.ElementFormCellGraphic.listVisibleTitle[0].This.Column[0].This.children[0].setAttribute('disabled','');
		    _this.ElementFormCellGraphic.colorTitle.Disabled = true;
		    _this.ElementFormCellGraphic.sizeTitle.This.disabled = true;
		    _this.ElementFormCellGraphic.alignTitle.This.disabled = true;
		    _this.ElementFormCellGraphic.typeGraphic.This.disabled = true;
		    for(var i = 0 ; i < _this.ElementFormCellGraphic.listbox.length ; i++){
			    _this.ElementFormCellGraphic.listbox[i].This.Column[0].This.children[0].setAttribute('disabled','');
		    }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicPBIBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.UpdateCellGraphic", e);
    }
}

ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.UpdateCellColumn = function(idCellGraphic){
    var _this = this.TParent();
    try {
	    for(var i = 0 ; i < _this.PBITEMPLATECELLGRAPHIC_COLUMNList.length ; i++){
		    if(_this.PBITEMPLATECELLGRAPHIC_COLUMNList[i].IDPBITEMPLATECELLGRAPHIC == idCellGraphic){
			    // eliminar todos
			    Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_COLUMN_DEL(_this.PBITEMPLATECELLGRAPHIC_COLUMNList[i].IDPBITEMPLATECELLGRAPHIC_COLUMN);
		    }
	    }
	    for(var i = 0 ; i < _this.ElementFormCellGraphic.listbox.length ; i++){
		    if(_this.ElementFormCellGraphic.listbox[i].Checked){
			    var PBITEMPLATECELLGRAPHIC_COLUMN = new Persistence.PBITemplate.Properties.TPBITEMPLATECELLGRAPHIC_COLUMN();			
			    PBITEMPLATECELLGRAPHIC_COLUMN.IDPBITEMPLATECELLGRAPHIC = idCellGraphic;
			    PBITEMPLATECELLGRAPHIC_COLUMN.COLUMN_NAME = _this.ElementFormCellGraphic.listbox[i].Text;
			    var success = Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_COLUMN_ADD(PBITEMPLATECELLGRAPHIC_COLUMN); 
			    if(success){
				    _this.ListLoad();
			    }
		    }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicPBIBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.UpdateCellColumn", e);
    }
}

ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.DeleteTemplate = function (idTemplate){
    var _this = this.TParent();
    try {
	    for (var i = 0; i < _this.PBITEMPLATEList.length; i++) {
		    if (_this.PBITEMPLATEList[i].IDPBITEMPLATE == idTemplate) {
			    if (_this.PBITEMPLATEList[i].PBITEMPLATECELLSList.length != 0) {
				    for (var j = 0; j < _this.PBITEMPLATEList[i].PBITEMPLATECELLSList.length; j++) {
					    if(_this.PBITEMPLATEList[i].PBITEMPLATECELLSList[j].PBITEMPLATECELLGRAPHIC != null){
						    if(_this.PBITEMPLATEList[i].PBITEMPLATECELLSList[j].PBITEMPLATECELLGRAPHIC.PBITEMPLATECELLGRAPHIC_COLUMNList.length != 0){
							    for (var k = 0; k < _this.PBITEMPLATEList[i].PBITEMPLATECELLSList[j].PBITEMPLATECELLGRAPHIC.PBITEMPLATECELLGRAPHIC_COLUMNList.length; k++) {
								    var idCellColumn = _this.PBITEMPLATEList[i].PBITEMPLATECELLSList[j].PBITEMPLATECELLGRAPHIC.PBITEMPLATECELLGRAPHIC_COLUMNList[k].IDPBITEMPLATECELLGRAPHIC_COLUMN;
								    var success = Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_COLUMN_DEL(idCellColumn);
								    if(success){}else{alert('No record could be deleted in CellColumn ' + k)}
							    }
						    }
						    var idCellGraphic = _this.PBITEMPLATEList[i].PBITEMPLATECELLSList[j].PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLGRAPHIC;
						    var success = Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_DEL(idCellGraphic);
						    if(success){}else {alert('No record could be deleted in CellGraphic')}
					    }
					    if(_this.PBITEMPLATEList[i].PBITEMPLATECELLSList[j].PBITEMPLATECELLSDESIGN != null){
						    var idCellDesign = _this.PBITEMPLATEList[i].PBITEMPLATECELLSList[j].PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLSDESIGN;
						    var success = Persistence.PBITemplate.Methods.PBITEMPLATECELLSDESIGN_DEL(idCellDesign);
						    if(success){}else {alert('No record could be deleted in CellGraphic')}
					    }
					    var idCell = _this.PBITEMPLATEList[i].PBITEMPLATECELLSList[j].IDPBITEMPLATECELLS;
					    var success = Persistence.PBITemplate.Methods.PBITEMPLATECELLS_DEL(idCell);
					    if(success){}else{alert('No record could be deleted in CellGraphic '+ j)}
				    }
			    }
			    var success = Persistence.PBITemplate.Methods.PBITEMPLATE_DEL(idTemplate);
			    if(success){
				    alert('ELiminacion Culminada');
				    _this.ListLoad();

				    /*Remover los contenedores de las celdas*/
				    $(_this.ElementContentCellParent.form).remove();
				    for(var  i = 0 ; i < _this.ElementContentCellChild.length ; i++){
					    $(_this.ElementContentCellChild[i].form).remove();
				    }
				    this.ElementContentCell = null;
				    this.ElementContentCellParent = null;
				    this.ElementContentCellChild = new Array();
				    this.ElementRowCell = new Array();
				    this.ElementFormCellParent = null;
				    this.ElementFormCellChild = new Array();
				    this.ElementFormDesignParent = null;
				    this.ElementFormDesignChild = new Array();
				    this.PanelChild = null;
				    _this.ElementFormCellGraphic = null;
				    for (var i = 0; i < _this.ElementContentTemplate.table._Rows.length; i++) {
					    $(_this.ElementContentTemplate.table._Rows[i].This).remove();
				    }
				    /*Llenado de Celdas de la tabla*/
				    _this.AddRowTemplate();

				    _this.ElementFormTemplate.id.Text = '';
				    _this.ElementFormTemplate.name.Text = '';
				    _this.ElementFormTemplate.time.Text = '';
				    _this.ElementFormTemplate.enabled[0].Checked = false;
				    _this.ElementFormTemplate.path.Text = '';
				    _this.ElementFormTemplate.description.Text = '';
				    _this.ElementFormTemplate.dataSQL.Text = '';
				    _this.ElementFormTemplate.id.This.disabled  = true;
				    _this.ElementFormTemplate.name.This.disabled  = true;
				    _this.ElementFormTemplate.time.This.disabled  = true;
				    _this.ElementFormTemplate.enabled[0].This.Column[0].This.children[0].setAttribute('disabled','');
				    _this.ElementFormTemplate.path.This.disabled  = true;
				    _this.ElementFormTemplate.description.This.disabled  = true;
				    _this.ElementFormTemplate.dataSQL.This.disabled  = true;
				    _this.ElementFormTemplate.save.This.style.display = 'none';
				    _this.ElementFormTemplate.update.This.style.display = 'none';
				    _this.ElementFormTemplate.cancel.This.style.visibility = 'hidden'

			    }else{alert('Eliminacion perdida')}
		    }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicPBIBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.DeleteTemplate", e);
    }
}

ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.DeleteCell = function (idCell, control){
    var _this = this.TParent();
    try {
	    _this.cellDelete = new Array();
	    var indice = Persistence.PBITemplate.Methods.PBITEMPLATECELLS_ListGetIndex(_this.PBITEMPLATECELLSList, idCell);
	    var idTemplate = _this.PBITEMPLATECELLSList[indice].IDPBITEMPLATE;
	    _this.cellDelete.push(_this.PBITEMPLATECELLSList[indice]);
	    _this.deleteChilds(_this.cellDelete);
	    var state = true;
	    for(var i = 0 ; i < _this.cellDelete.length ; i++){	
		    if(_this.cellDelete[i].PBITEMPLATECELLGRAPHIC != null){
			    if(_this.cellDelete[i].PBITEMPLATECELLGRAPHIC.PBITEMPLATECELLGRAPHIC_COLUMNList.length != 0){
				    for (var k = 0; k < _this.cellDelete[i].PBITEMPLATECELLGRAPHIC.PBITEMPLATECELLGRAPHIC_COLUMNList.length; k++) {
					    var idCellColumn = _this.cellDelete[i].PBITEMPLATECELLGRAPHIC.PBITEMPLATECELLGRAPHIC_COLUMNList[k].IDPBITEMPLATECELLGRAPHIC_COLUMN;
					    var success = Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_COLUMN_DEL(idCellColumn);
					    if(success){}else{alert('No record could be deleted in CellColumn ' + k)}
				    }
			    }
			    var idCellGraphic = _this.cellDelete[i].PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLGRAPHIC;
			    var success = Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_DEL(idCellGraphic);
			    if(success){}else {alert('No record could be deleted in CellGraphic')}
		    }
		    if(_this.cellDelete[i].PBITEMPLATECELLSDESIGN != null){
			    var idCellDesign = _this.cellDelete[i].PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLSDESIGN;
			    var success = Persistence.PBITemplate.Methods.PBITEMPLATECELLSDESIGN_DEL(idCellDesign);
			    if(success){}else {alert('No record could be deleted in CellGraphic')}
		    }
		    var idCell = _this.cellDelete[i].IDPBITEMPLATECELLS;
		    var success = Persistence.PBITemplate.Methods.PBITEMPLATECELLS_DEL(idCell);
		    if(success){
			    // alert('ELiminacion Culminada');
			    _this.ListLoad();
		    }else{
			    state = false;
			    alert('No record could be deleted in CellGraphic '+ i);
		    }
	    }
	    /*Eliminar contenedores child*/
	    for(var x = 0; x < _this.ElementContentCellChild.length ; x++){
		    $(_this.ElementContentCellChild[x].form).remove();
	    }
	    /*Actualización de tabla*/
	    for(var x = 0 ; x < _this.ElementContentCellParent.table._Rows.length; x++){
		    $(_this.ElementContentCellParent.table._Rows[x].This).remove();
	    }
	    _this.PanelChild = control;
	    for (var x = 0; x < _this.PBITEMPLATEList.length; x++) {
		    if (_this.PBITEMPLATEList[x].IDPBITEMPLATE == idTemplate) {
			    _this.AddRowCell(_this.PBITEMPLATEList[x].PBITEMPLATECELLSPARENTList, true);
		    }
	    }
	    /*Limpiar cajas*/
	    /*------------*/
	
	    _this.ElementFormCellParent.id.Text = '';
	    _this.ElementFormCellParent.name.Text = '';
	    _this.ElementFormCellParent.position.Text = '0';
	    _this.ElementFormCellParent.id.This.disabled = true;
	    _this.ElementFormCellParent.name.This.disabled = true;
	    _this.ElementFormCellParent.position.This.disabled = true;
	    _this.ElementFormCellParent.parent.This.disabled = true;
	    _this.ElementFormDesignParent.designXS.selectedIndex = 0;
	    _this.ElementFormDesignParent.designS.selectedIndex = 0;
	    _this.ElementFormDesignParent.designM.selectedIndex = 0;
	    _this.ElementFormDesignParent.designL.selectedIndex = 0;
	    _this.ElementFormDesignParent.designXL.selectedIndex = 0;
	    _this.ElementFormDesignParent.designRow[0].Checked = false;
	    _this.ElementFormDesignParent.designColumn[0].Checked = false;
	    _this.ElementFormDesignParent.designXS.This.disabled  = true;
	    _this.ElementFormDesignParent.designS.This.disabled  = true;
	    _this.ElementFormDesignParent.designM.This.disabled  = true;
	    _this.ElementFormDesignParent.designL.This.disabled  = true;
	    _this.ElementFormDesignParent.designXL.This.disabled  = true;
	    _this.ElementFormDesignParent.designRow[0].This.Column[0].This.children[0].setAttribute('disabled','');
	    _this.ElementFormDesignParent.designColumn[0].This.Column[0].This.children[0].setAttribute('disabled','');
	    _this.ElementFormCellParent.save.This.style.display = 'none';
	    _this.ElementFormCellParent.update.This.style.display = 'none';
	    _this.ElementFormCellParent.cancel.This.style.visibility = 'hidden';
	    $(_this.ElementFormCellParent.container.Column[0].This).html('');
        return state;
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicPBIBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.DeleteCell", e);
    }
}

ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.DeleteCellChild = function(idCell, index){
	var _this = this.TParent();
    _this.cellDelete = new Array();
    try {
	    var indice = Persistence.PBITemplate.Methods.PBITEMPLATECELLS_ListGetIndex(_this.PBITEMPLATECELLSList, idCell);
	    if(indice != -1){
		    /*ultimo nodo*/
		    var PBICELLGRAPHIC = null;
		    var idParent = _this.PBITEMPLATECELLSList[indice].IDPBITEMPLATECELLS_PARENT;
		    var indexTemp = Persistence.PBITemplate.Methods.PBITEMPLATECELLS_ListGetIndex(_this.PBITEMPLATECELLSList, idParent);
		    _this.PBICELLCHILD = null;
		    if(_this.PBITEMPLATECELLSList[indice].CELLS_END && _this.PBITEMPLATECELLSList[indexTemp].PBITEMPLATECELLS_CHILDList.length == 1){
			    _this.PBICELLCHILD = _this.PBITEMPLATECELLSList[indice];
		    }else{
			    _this.getChild(_this.PBITEMPLATECELLSList[indice]);
		    }
		    if(_this.PBICELLCHILD != null && _this.PBITEMPLATECELLSList[indexTemp].PBITEMPLATECELLS_CHILDList.length == 1){
			    if(_this.PBICELLCHILD.PBITEMPLATECELLGRAPHIC != null){
				    PBICELLGRAPHIC = new Persistence.PBITemplate.Properties.TPBITEMPLATECELLGRAPHIC();
				    PBICELLGRAPHIC.IDPBITEMPLATECELLGRAPHIC = _this.PBICELLCHILD.PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLGRAPHIC;
				    PBICELLGRAPHIC.IDPBITEMPLATECELLS = idParent;
				    PBICELLGRAPHIC.IDPBIGRAPHICTYPE = _this.PBICELLCHILD.PBITEMPLATECELLGRAPHIC.IDPBIGRAPHICTYPE;
				    PBICELLGRAPHIC.CELLGRAPHIC_VISIBLE = _this.PBICELLCHILD.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_VISIBLE;
				    PBICELLGRAPHIC.CELLGRAPHIC_TITLE = _this.PBICELLCHILD.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_TITLE;
				    PBICELLGRAPHIC.CELLGRAPHIC_VISIBLETITLE = _this.PBICELLCHILD.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_VISIBLETITLE;
				    PBICELLGRAPHIC.CELLGRAPHIC_COLORTITLE = _this.PBICELLCHILD.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_COLORTITLE;
				    PBICELLGRAPHIC.CELLGRAPHIC_SIZETITLE =_this.PBICELLCHILD.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_SIZETITLE;
				    PBICELLGRAPHIC.CELLGRAPHIC_ALIGNTITLE = _this.PBICELLCHILD.PBITEMPLATECELLGRAPHIC.CELLGRAPHIC_ALIGNTITLE;
				    var success = Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_UPD(PBICELLGRAPHIC);
				    var PBITEMPLATECELLS = Persistence.PBITemplate.Methods.PBITEMPLATECELLS_ListSetID(_this.PBITEMPLATECELLSList, idParent);
				    PBITEMPLATECELLS.CELLS_END = true;
				    Persistence.PBITemplate.Methods.PBITEMPLATECELLS_UPD(PBITEMPLATECELLS);

				    /*Eliminación*/
				    _this.cellDelete.push(_this.PBITEMPLATECELLSList[indice]);
				    _this.deleteChilds(_this.cellDelete);
				    var state = true;
				    for(var i = 0 ; i < _this.cellDelete.length ; i++){	
					    if(_this.cellDelete[i].PBITEMPLATECELLSDESIGN != null){
						    var idCellDesign = _this.cellDelete[i].PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLSDESIGN;
						    var success = Persistence.PBITemplate.Methods.PBITEMPLATECELLSDESIGN_DEL(idCellDesign);
						    if(success){}else {alert('No record could be deleted in CellGraphic')}
					    }
					    var idCell = _this.cellDelete[i].IDPBITEMPLATECELLS;
					    var success = Persistence.PBITemplate.Methods.PBITEMPLATECELLS_DEL(idCell);
					    if(success){
						    // alert('ELiminacion Culminada');
						    _this.ListLoad();
					    }else{
						    state = false;
						    alert('No record could be deleted in CellGraphic '+ i);
					    }
				    }
				    /*Mostrar datos en el formCell*/
				    var idNodeParent = PBITEMPLATECELLS.IDPBITEMPLATECELLS_PARENT;
				    /*Encontrar el valor del PanelChild*/
				    var statePanel = false;
				    var indexPanel = null;
				    for(var i = 0 ; i < _this.ElementFormCellChild.length ; i++){
					    if(_this.ElementFormCellChild[i].idCell == idNodeParent){
						    statePanel = true;
						    indexPanel = i;
					    }
				    }
				    if(statePanel){
					    _this.PanelChild = _this.ElementFormCellChild[indexPanel].iupdate.This.dataset.control;
				    }else{
					    _this.PanelChild = _this.ElementFormCellParent.iupdate.This.dataset.control;
				    }

				    var indexFill = Persistence.PBITemplate.Methods.PBITEMPLATECELLS_ListGetIndex(_this.PBITEMPLATECELLSList, idParent);
				    if(_this.PanelChild < 0){
					    _this.ElementFormCellParent.id.Text = _this.PBITEMPLATECELLSList[indexFill].IDPBITEMPLATECELLS;
					    _this.ElementFormCellParent.name.Text = _this.PBITEMPLATECELLSList[indexFill].CELLS_NAME;
					    _this.ElementFormCellParent.position.Text = _this.PBITEMPLATECELLSList[indexFill].CELLS_POSITION;
					    /*Contenedor de los nodos*/
					    if(_this.PBITEMPLATECELLSList[indexFill].CELLS_END){
						    _this.CreateFormCellGraphic(_this.ElementFormCellParent.container, PBITEMPLATECELLS.IDPBITEMPLATE);
						    var idCellGraphic = _this.FillCellGraphic(idParent);
						    _this.FillColumn(idCellGraphic);
					    }
				    }
				    else{
					    _this.ElementFormCellChild[_this.PanelChild].id.Text = _this.PBITEMPLATECELLSList[indexFill].IDPBITEMPLATECELLS;
					    _this.ElementFormCellChild[_this.PanelChild].name.Text = _this.PBITEMPLATECELLSList[indexFill].CELLS_NAME;
					    _this.ElementFormCellChild[_this.PanelChild].position.Text = _this.PBITEMPLATECELLSList[indexFill].CELLS_POSITION;
					    /*Contenedor de los nodos*/
					
					    if(_this.PBITEMPLATECELLSList[indexFill].CELLS_END){
						    _this.CreateFormCellGraphic(_this.ElementFormCellChild[_this.PanelChild].container, PBITEMPLATECELLS.IDPBITEMPLATE);
						    var idCellGraphic = _this.FillCellGraphic(idParent);
						    _this.FillColumn(idCellGraphic);
					    }
				    }
				    _this.FillDesign(idParent);
			    }
		    }
		    else{
			    /*Eliminación*/
			    _this.cellDelete.push(_this.PBITEMPLATECELLSList[indice]);
			    _this.deleteChilds(_this.cellDelete);
			    var state = true;
			    for(var i = 0 ; i < _this.cellDelete.length ; i++){	
				    if(_this.cellDelete[i].PBITEMPLATECELLGRAPHIC != null){
					    if(_this.cellDelete[i].PBITEMPLATECELLGRAPHIC.PBITEMPLATECELLGRAPHIC_COLUMNList.length != 0){
						    for (var k = 0; k < _this.cellDelete[i].PBITEMPLATECELLGRAPHIC.PBITEMPLATECELLGRAPHIC_COLUMNList.length; k++) {
							    var idCellColumn = _this.cellDelete[i].PBITEMPLATECELLGRAPHIC.PBITEMPLATECELLGRAPHIC_COLUMNList[k].IDPBITEMPLATECELLGRAPHIC_COLUMN;
							    var success = Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_COLUMN_DEL(idCellColumn);
							    if(success){}else{alert('No record could be deleted in CellColumn ' + k)}
						    }
					    }
					    var idCellGraphic = _this.cellDelete[i].PBITEMPLATECELLGRAPHIC.IDPBITEMPLATECELLGRAPHIC;
					    var success = Persistence.PBITemplate.Methods.PBITEMPLATECELLGRAPHIC_DEL(idCellGraphic);
					    if(success){}else {alert('No record could be deleted in CellGraphic')}
				    }
				    if(_this.cellDelete[i].PBITEMPLATECELLSDESIGN != null){
					    var idCellDesign = _this.cellDelete[i].PBITEMPLATECELLSDESIGN.IDPBITEMPLATECELLSDESIGN;
					    var success = Persistence.PBITemplate.Methods.PBITEMPLATECELLSDESIGN_DEL(idCellDesign);
					    if(success){}else {alert('No record could be deleted in CellGraphic')}
				    }
				    var idCell = _this.cellDelete[i].IDPBITEMPLATECELLS;
				    var success = Persistence.PBITemplate.Methods.PBITEMPLATECELLS_DEL(idCell);
				    if(success){
					    // alert('ELiminacion Culminada');
					    _this.ListLoad();
				    }else{
					    state = false;
					    alert('No record could be deleted in CellGraphic '+ i);
				    }
			    }
		    }
		    /*Eliminacion de Paneles*/
		    _this.PanelChild = index;
		    for(var x = index+1; x < _this.ElementContentCellChild.length ; x++){
			    $(_this.ElementContentCellChild[x].form).remove();
		    }

		    for(var i = 0 ; i < _this.ElementContentCellChild[_this.PanelChild].table._Rows.length; i++){
			    $(_this.ElementContentCellChild[_this.PanelChild].table._Rows[i].This).remove();
		    }

		    var fill = Persistence.PBITemplate.Methods.PBITEMPLATECELLS_ListGetIndex(_this.PBITEMPLATECELLSList, idParent);
		    _this.AddRowCell(_this.PBITEMPLATECELLSList[fill].PBITEMPLATECELLS_CHILDList, true);

		    _this.ElementFormCellChild[_this.PanelChild].id.Text = '';
		    _this.ElementFormCellChild[_this.PanelChild].name.Text = '';
		    _this.ElementFormCellChild[_this.PanelChild].position.Text = '0';
		    _this.ElementFormCellChild[_this.PanelChild].id.This.disabled = true;
		    _this.ElementFormCellChild[_this.PanelChild].name.This.disabled = true;
		    _this.ElementFormCellChild[_this.PanelChild].position.This.disabled = true;
		    _this.ElementFormCellChild[_this.PanelChild].parent.This.disabled = true;
		    _this.ElementFormDesignChild[_this.PanelChild].designXS.selectedIndex = 0;
		    _this.ElementFormDesignChild[_this.PanelChild].designS.selectedIndex = 0;
		    _this.ElementFormDesignChild[_this.PanelChild].designM.selectedIndex = 0;
		    _this.ElementFormDesignChild[_this.PanelChild].designL.selectedIndex = 0;
		    _this.ElementFormDesignChild[_this.PanelChild].designXL.selectedIndex = 0;
		    _this.ElementFormDesignChild[_this.PanelChild].designRow[0].Checked = false;
		    _this.ElementFormDesignChild[_this.PanelChild].designColumn[0].Checked = false;
		    _this.ElementFormDesignChild[_this.PanelChild].designXS.This.disabled  = true;
		    _this.ElementFormDesignChild[_this.PanelChild].designS.This.disabled  = true;
		    _this.ElementFormDesignChild[_this.PanelChild].designM.This.disabled  = true;
		    _this.ElementFormDesignChild[_this.PanelChild].designL.This.disabled  = true;
		    _this.ElementFormDesignChild[_this.PanelChild].designXL.This.disabled  = true;
		    _this.ElementFormDesignChild[_this.PanelChild].designRow[0].This.Column[0].This.children[0].setAttribute('disabled','');
		    _this.ElementFormDesignChild[_this.PanelChild].designColumn[0].This.Column[0].This.children[0].setAttribute('disabled','');
		    _this.ElementFormCellChild[_this.PanelChild].save.This.style.display = 'none';
		    _this.ElementFormCellChild[_this.PanelChild].update.This.style.display = 'none';
		    _this.ElementFormCellChild[_this.PanelChild].cancel.This.style.visibility = 'hidden'
		    $(_this.ElementFormCellChild[_this.PanelChild].container.Column[0].This).html('');
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicPBIBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.DeleteCellChild", e);
    }
}

ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.deleteChilds = function (cellList){
	var _this = this.TParent();
    var temporal = new Array();
    try {
	    for(var i = 0 ; i < cellList.length ; i++){
		    if(cellList[i].PBITEMPLATECELLS_CHILDList.length != 0){
			    for(var j = 0 ; j < cellList[i].PBITEMPLATECELLS_CHILDList.length ; j++){
				    temporal.push(cellList[i].PBITEMPLATECELLS_CHILDList[j]);	
				    _this.cellDelete.push(cellList[i].PBITEMPLATECELLS_CHILDList[j]);
			    }
		    }
	    }
	    if(temporal.length != 0){
		    _this.deleteChilds(temporal);
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicPBIBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.deleteChilds", e);
    }
}

ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.getChild = function (cellList){
	var _this = this.TParent();
    var temporal = null;
    try {
	    if(cellList.PBITEMPLATECELLS_CHILDList.length == 1){
		    if(!cellList.PBITEMPLATECELLS_CHILDList[0].CELLS_END){
			    _this.getChild(cellList.PBITEMPLATECELLS_CHILDList[0]);
		    }
		    else{
			    temporal = cellList.PBITEMPLATECELLS_CHILDList[0];
			    _this.PBICELLCHILD = temporal;
		    }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicPBIBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.getChild", e);
    }
}

ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.ListLoad = function (){
    var _this = this.TParent();
    try {
        _this.GraphicsPBI.Fill();
        _this.PBITEMPLATEList = _this.GraphicsPBI.PBITEMPLATEList;
        _this.PBIGRAPHICTYPEList = _this.GraphicsPBI.PBIGRAPHICTYPEList;
        _this.PBITEMPLATECELLSList = _this.GraphicsPBI.PBITEMPLATECELLSList;
        _this.PBITEMPLATECELLSDESIGNList = _this.GraphicsPBI.PBITEMPLATECELLSDESIGNList;
        _this.PBITEMPLATECELLGRAPHICList = _this.GraphicsPBI.PBITEMPLATECELLGRAPHICList;
        _this.PBITEMPLATECELLGRAPHIC_COLUMNList = _this.GraphicsPBI.PBITEMPLATECELLGRAPHIC_COLUMNList;
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicPBIBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.ListLoad", e);
    } 
}

ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.getQuery = function (StrSQL) {
	Param = new SysCfg.Stream.Properties.TParam();
	var Message = null;
	var Status = null;
	try {
		Param.Inicialize();
		OpenDataSet = SysCfg.DB.SQL.Methods.OpenDataSet(StrSQL, Param.ToBytes());
		if (OpenDataSet.ResErr.NotError) {
			if (OpenDataSet.DataSet.RecordCount > 0) {
				Status = true;
				Message = 'OK';
			}
			else {
				OpenDataSet.ResErr.NotError = true;
				Status = false;
				Message = OpenDataSet.ResErr.Mesaje;
			}
		}
		else {
			Status = false;
			Message = OpenDataSet.ResErr.Mesaje;
		}
	}
    catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicsPBIBasic.js ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.getQuery", e);
		Status = false
		Message = e;
	}
	finally {
		Param.Destroy();
	}
	return ({ Result: OpenDataSet.DataSet, Status: Status, Message: Message });
}

ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.AddStyleFile = function (nombre, onSuccess, onError) {
	var style = document.createElement("link");
	style.rel = "stylesheet";
	style.type = "text/css";
	style.href = nombre;
	var s = document.head.appendChild(style);
	s.onload = onSuccess;
	s.onerror = onError;
}

ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic.prototype.LoadScript = function (nombre, onSuccess, onError) {
	var s = document.createElement("script");
	s.onload = onSuccess;
	s.onerror = onError;
	s.src = nombre;
	document.querySelector("head").appendChild(s);
}