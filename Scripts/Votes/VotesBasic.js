ItHelpCenter.VotesManager.TVotesBasic = function (inObjectHtml, id, inCallBack){
    this.TParent = function () {
        return this;
    }.bind(this);

    this.isInteger = function (num) {
        return (num ^ 0) === num;
    }

    var _this = this.TParent();
    this._ObjectHtml = inObjectHtml;
    this._Id = id;
    this.VotesProfiler = new Persistence.Votes.TVotesProfiler();
    this.ElementRowVote = new Array();
    this.ElementRowVoteOption = new Array();

    this.Mythis = "VotesBasic";
	
    UsrCfg.Traslate.GetLangText(_this.Mythis, "VOTE", "Vote");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "VOTEOPTION", "Vote Option");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "ID", "Id");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "NAME", "Name");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "GROUP", "Votes Group");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "QUESTION", "Question");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "COMMENTREQUIRED", "Comment Required");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "PROGRESSCOLOR", "Progress Color");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "PROGRESSTITLE", "Start Title");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "TOTALPROGRESSTITLE", "End Title");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "INITGRAPHIC", "Init Graphic");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "SHOWCOMMENT", "Show Comment");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "SHOWCOMMENTGROUP", "Show Bubble Comment Group");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "OPTIONNAME", "Option Name");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "DESCRIPTION", "Description");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "PARENT", "Vote");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "SAVE", "Save");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "UPDATE", "Update");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "CANCEL", "Cancel");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "CONFIRMDELETE", "Do you want to delete the selected record?");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "NOCONFIRMDELETE", "No record was deleted");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "NOSELECT", "No record selected");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "NONAME", "Cannot register a user without a name");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "NONUMBER", "The votes group field is not a number");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "NOQUESTION", "The question field is required");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "REPEATEDNAME", "This name already exists");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "CORRECTSAVE", "Registration saved correctly");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECTSAVE", "An error occurred while saving the record");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "CORRECTUPDATE", "The registration was updated correctly");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECTUPDATE", "An error occurred while updating a record");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "CORRECTDELETE", "The record (s) were deleted correctly");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECTDELETE", "The record (s) could not be deleted");
		 
    _this.LoadScript(SysCfg.App.Properties.xRaiz + "Componet/Color/Plugin/spectrum.js", function () {
        _this.AddStyleFile(SysCfg.App.Properties.xRaiz + "Componet/Color/Plugin/spectrum.css", function () {
            _this.AddStyleFile(SysCfg.App.Properties.xRaiz + "Css/Votes/VotesBasic.css", function () {
                _this.Load();
            }, function (e) {
                SysCfg.Log.Methods.WriteLog("VotesBasic.js ItHelpCenter.VotesManager.TVotesBasic _this.AddStyleFile(" + _this.AddStyleFile + "Css/Votes/VotesBasic.css)", e);
            });
        }, function (e) {
            SysCfg.Log.Methods.WriteLog("VotesBasic.js ItHelpCenter.VotesManager.TVotesBasic _this.AddStyleFile(" + _this.AddStyleFile + "Componet/Color/Plugin/spectrum.css)", e);
        });
    }, function (e) {
        SysCfg.Log.Methods.WriteLog("VotesBasic.js ItHelpCenter.VotesManager.TVotesBasic _this.LoadScript(" + _this.LoadScript + "Componet/Color/Plugin/spectrum.js)", e);
    });
	 
}

ItHelpCenter.VotesManager.TVotesBasic.prototype.Load = function () {
	var _this = this.TParent();
	try{
		_this.ListLoad();

		_this.btn2 = new TVclInputbutton(_this._ObjectHtml, "");
		_this.btn2.Text = "Add All Data";
		_this.btn2.VCLType = TVCLType.BS;
		_this.btn2.onClick = function () {
			var Votes = new Persistence.Votes.TVotesProfiler();
			Votes.Fill();
			if (Votes.VOTEList.length === 0) {
				Votes.CreateAll();
			}
			else {
				alert('Records already exist in the Database');
			}
		}
		_this.btn2.This.style.marginTop = "10px";
		_this.btn2.This.style.position = "relative";
		_this.btn2.This.style.left = "50%";
		_this.btn2.This.style.transform = "translate(-50%,0%)";

		// _this.ContPreview = new TVclStackPanel(_this._ObjectHtml, "", 1, [[12], [12], [12], [12], [12]]);
		_this.Content(_this._ObjectHtml, '', UsrCfg.Traslate.GetLangText(_this.Mythis, 'VOTE'), '', _this.VOTEList);
	} 
	catch (e) {
		SysCfg.Log.Methods.WriteLog("VotesBasic.js ItHelpCenter.VotesManager.TVotesBasic.prototype.Load", e);
	}
}

ItHelpCenter.VotesManager.TVotesBasic.prototype.Content = function (objectHtml, id, title, name, voteList) {
	var _this = this.TParent();
	var ObjectHtml = objectHtml;
	var Title = title;
	var Name = name;
	var VOTEOPTIONList = voteList;
	try{
		if (_this.ElementContentVoteOption != null && Title == UsrCfg.Traslate.GetLangText(_this.Mythis, 'VOTEOPTION')) {
			$(_this.ElementContentVoteOption.form).remove();
		}

		var Container = new TVclStackPanel(ObjectHtml, id + '_'  + Title, 1, [[12], [12], [12], [12], [12]]);

		_this.ContPreview = new TVclStackPanel(_this._ObjectHtml, "", 1, [[12], [12], [12], [12], [12]]);

		var ConLabelControl = new TVclStackPanel(Container.Column[0].This, '', 1, [[12], [12], [12], [12], [12]]);
		ConLabelControl.Row.This.classList.add('TitleBasic');
		var lblTitle = new TVcllabel(ConLabelControl.Column[0].This, '', TlabelType.H0);
		(Name != '') ? lblTitle.Text = Title + ': ' + Name : lblTitle.Text = Title ;
		lblTitle.VCLType = TVCLType.BS;

		var FormContainer = new TVclStackPanel(Container.Column[0].This, '', 2, [[12, 12], [12, 12], [5,7], [5, 7], [4, 8]]);
		var TitleGrid = new TGrid(FormContainer.Column[0].This, '', '');
		var Col0 = new TColumn();
		Col0.Name = ""; 
		Col0.Caption = "Name";
		Col0.Index = 0; 
		Col0.EnabledEditor = false;
		Col0.DataType = SysCfg.DB.Properties.TDataType.String;
		Col0.ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.None;
		Col0.This.classList.add('col-xs-12');
		TitleGrid.AddColumn(Col0);
		if (Title == UsrCfg.Traslate.GetLangText(_this.Mythis, 'VOTE')) {
			Container.Row.This.classList.add('ContainerVote');
			_this.ElementContentVote = { form: Container.Row.This, table: TitleGrid, formText: FormContainer.Column[1].This };
			_this.AddRowVote();
			TitleGrid.This.classList.add('TableVote');
			TitleGrid.This.classList.add('table-fixed');
			_this.CreateFormVote(FormContainer.Column[1].This);
			if(Source.Menu.IsMobil){
				Container.Row.This.style.marginLeft = '0px';
				Container.Row.This.style.marginRight = '0px';
				Container.Row.This.style.paddingLeft = '0px';
			}
		}
		else if (Title == UsrCfg.Traslate.GetLangText(_this.Mythis, 'VOTEOPTION')) {
			Container.Row.This.classList.add('ContainerVoteOption');
			TitleGrid.ResponsiveCont.classList.remove('table-responsive');
			_this.ElementContentVoteOption = { form: Container.Row.This, table: TitleGrid, title: lblTitle, formText: FormContainer.Column[1].This };
			_this.AddRowVoteOption(VOTEOPTIONList);
			TitleGrid.This.classList.add('TableVoteOption');
			TitleGrid.This.classList.add('table-fixed');
			_this.CreateFormVoteOption(FormContainer.Column[1].This);
			if(Source.Menu.IsMobil){
				Container.Row.This.style.marginLeft = '0px';
				Container.Row.This.style.marginRight = '0px';
				Container.Row.This.style.paddingLeft = '0px';
			}
		}
	} 
	catch (e) {
		SysCfg.Log.Methods.WriteLog("VotesBasic.js ItHelpCenter.VotesManager.TVotesBasic.prototype.Content", e);
	}
}

ItHelpCenter.VotesManager.TVotesBasic.prototype.CreateFormVote = function (objectHtml) {
	var _this = this.TParent();
	_this.ElementFormVote = null;
	var ObjectHtml = objectHtml;

	try{
		var ContImages = new TVclStackPanel(ObjectHtml, "", 1, [[12], [12], [12], [12], [12]]);
		ContImages.Row.This.classList.add("DownSpace");
		var ImageDelete = new TVclImagen(ContImages.Column[0].This, "");
		ImageDelete.This.style.float = "right";
		ImageDelete.This.style.marginLeft = "5px";
		ImageDelete.Title = "Delete";
		ImageDelete.Src = "image/16/delete.png";
		ImageDelete.Cursor = "pointer";
		ImageDelete.onClick = function () {
			if (txtId.Text != '') {
				var confirmed = confirm(UsrCfg.Traslate.GetLangText(_this.Mythis, 'CONFIRMDELETE'));
				if (confirmed) {
					_this.DeleteVote(parseInt(txtId.Text));
				}
				else {
					alert(UsrCfg.Traslate.GetLangText(_this.Mythis, 'NOCONFIRMDELETE'));
				}
			}
			else {
				alert(UsrCfg.Traslate.GetLangText(_this.Mythis, 'NOSELECT'));
			}
		}
		var ImageUpdate = new TVclImagen(ContImages.Column[0].This, "");
		ImageUpdate.This.style.marginLeft = "5px";
		ImageUpdate.This.style.float = "right";
		ImageUpdate.Title = "Edit";
		ImageUpdate.Src = "image/16/edit.png";
		ImageUpdate.Cursor = "pointer";
		ImageUpdate.onClick = function () {
			if (txtId.Text != '') {
				txtGroup.This.disabled = false;
				txtName.This.disabled = false;
				txtQuestion.This.disabled = false;
				txtProgressTitle.This.disabled = false;
				txtTotalProgressTitle.This.disabled = false;
				ProgressColor.Disabled = false;
				cbInitGraphic.This.disabled = false;
				ListBox.ListBoxItems[0].This.Column[0].This.children[0].removeAttribute('disabled','');
				ListBox.ListBoxItems[1].This.Column[0].This.children[0].removeAttribute('disabled','');
				ListBox.ListBoxItems[2].This.Column[0].This.children[0].removeAttribute('disabled','');

				btnUpdate.This.style.display = 'block';
				btnAdd.This.style.display = "none";
				btnCancel.This.style.visibility = 'visible';
			}
			else{
				txtGroup.This.disabled = true;
				txtName.This.disabled = true;
				txtQuestion.This.disabled = true;
				txtProgressTitle.This.disabled = true;
				txtTotalProgressTitle.This.disabled = true;
				ProgressColor.Disabled = true;
				cbInitGraphic.This.disabled = true;
				ListBox.ListBoxItems[0].This.Column[0].This.children[0].setAttribute('disabled','');
				ListBox.ListBoxItems[1].This.Column[0].This.children[0].setAttribute('disabled','');
				ListBox.ListBoxItems[2].This.Column[0].This.children[0].setAttribute('disabled','');
				
				alert(UsrCfg.Traslate.GetLangText(_this.Mythis, 'NOSELECT'));
				btnUpdate.This.style.display = 'none';
				btnAdd.This.style.display = "none";
				btnCancel.This.style.visibility = 'hidden';
			}
		}
		var ImageAdd = new TVclImagen(ContImages.Column[0].This, "");
		ImageAdd.This.style.marginLeft = "5px";
		ImageAdd.This.style.float = "right";
		ImageAdd.Title = "Add";
		ImageAdd.Src = "image/16/add.png";
		ImageAdd.Cursor = "pointer";
		ImageAdd.onClick = function () {
			txtId.Text = '';
			txtName.Text = '';
			txtGroup.Text = '0';
			txtQuestion.Text = '';
			txtProgressTitle.Text = '';
			txtTotalProgressTitle.Text = '';
			ProgressColor.Color = 'white';
			cbInitGraphic.selectedIndex = 0;
			ListBox.ListBoxItems[0].Checked = false;
			ListBox.ListBoxItems[1].Checked = false;
			ListBox.ListBoxItems[2].Checked = false;
			txtGroup.This.disabled = false;
			txtName.This.disabled = false;
			txtQuestion.This.disabled = false;
			txtProgressTitle.This.disabled = false;
			txtTotalProgressTitle.This.disabled = false;
			ProgressColor.Disabled = false;
			cbInitGraphic.This.disabled = false;
			ListBox.ListBoxItems[0].This.Column[0].This.children[0].removeAttribute('disabled','');
			ListBox.ListBoxItems[1].This.Column[0].This.children[0].removeAttribute('disabled','');
			ListBox.ListBoxItems[2].This.Column[0].This.children[0].removeAttribute('disabled','');
			btnUpdate.This.style.display = 'none';
			btnAdd.This.style.display = "block";
			btnCancel.This.style.visibility = 'visible';

			ListBox.ListBoxItems[1].This.Column[0].This.style.display = 'block';
			ListBox.ListBoxItems[2].This.Column[0].This.style.display = 'block';

		}
		var ImagePreview = new TVclImagen(ContImages.Column[0].This, "");
		ImagePreview.This.style.float = "right";
		ImagePreview.Title = "Preview";
		ImagePreview.Src = "image/16/Computer.png";
		ImagePreview.Cursor = "pointer";
		ImagePreview.onClick = function () {
			if (txtId.Text != '') {
				var indexVote = Persistence.Votes.Methods.VOTE_ListGetIndex(_this.VOTEList, parseInt(_this.ElementFormVote.id.Text));
				if(_this.VOTEList[indexVote].VOTEOPTIONList.length != 0 ){
					_this.ShowPreview(parseInt(_this.ElementFormVote.id.Text), _this.ElementFormVote.name.Text);
				}
				else{
					alert('No existen datos de la opción para mostrar')
				}
			}
			else{
				alert(UsrCfg.Traslate.GetLangText(_this.Mythis, 'NOSELECT'));
			}
		}
		var ContTextId = new TVclStackPanel(ObjectHtml, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
		ContTextId.Row.This.style.marginBottom = '5px';
		var lblId = new TVcllabel(ContTextId.Column[0].This, "", TlabelType.H0);
		lblId.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ID");
		lblId.VCLType = TVCLType.BS;
		var txtId = new TVclTextBox(ContTextId.Column[1].This, "txtIDVOTE");
		txtId.Text = "";
		txtId.This.disabled = true;
		txtId.VCLType = TVCLType.BS;

		var ContTextName = new TVclStackPanel(ObjectHtml, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
		ContTextName.Row.This.style.marginBottom = '5px';
		var lblName = new TVcllabel(ContTextName.Column[0].This, "", TlabelType.H0);
		lblName.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "NAME");
		lblName.VCLType = TVCLType.BS;
		var txtName = new TVclTextBox(ContTextName.Column[1].This, "txtNameVOTE");
		txtName.Text = "";
		txtName.This.disabled = true;
		txtName.VCLType = TVCLType.BS;

		var ContTextGroup = new TVclStackPanel(ObjectHtml, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
		ContTextGroup.Row.This.style.marginBottom = '5px';
		var lblGroup = new TVcllabel(ContTextGroup.Column[0].This, "", TlabelType.H0);
		lblGroup.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "GROUP");
		lblGroup.VCLType = TVCLType.BS;
		var txtGroup = new TVclTextBox(ContTextGroup.Column[1].This, "txtGroupVOTE");
		txtGroup.Text = "0";
		txtGroup.This.disabled = true;
		txtGroup.VCLType = TVCLType.BS;
		txtGroup.This.setAttribute("type", "number");
		txtGroup.This.setAttribute("min", "0");

		var ContTextQuestion = new TVclStackPanel(ObjectHtml, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
		ContTextQuestion.Row.This.style.marginBottom = '5px';
		var lblQuestion = new TVcllabel(ContTextQuestion.Column[0].This, "", TlabelType.H0);
		lblQuestion.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "QUESTION");
		lblQuestion.VCLType = TVCLType.BS;
		var txtQuestion = new TVclTextBox(ContTextQuestion.Column[1].This, "txtQuestionVOTE");
		txtQuestion.Text = "";
		txtQuestion.This.disabled = true;
		txtQuestion.VCLType = TVCLType.BS;

		var ContTextProgressTitle = new TVclStackPanel(ObjectHtml, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
		ContTextProgressTitle.Row.This.style.marginBottom = '5px';
		var lblProgressTitle = new TVcllabel(ContTextProgressTitle.Column[0].This, "", TlabelType.H0);
		lblProgressTitle.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "PROGRESSTITLE");
		lblProgressTitle.VCLType = TVCLType.BS;
		var txtProgressTitle = new TVclTextBox(ContTextProgressTitle.Column[1].This, "txtProgressTitleVOTE");
		txtProgressTitle.Text = "";
		txtProgressTitle.This.disabled = true;
		txtProgressTitle.VCLType = TVCLType.BS;

		var ContTextTotalProgressTitle = new TVclStackPanel(ObjectHtml, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
		ContTextTotalProgressTitle.Row.This.style.marginBottom = '5px';
		var lblTotalProgressTitle = new TVcllabel(ContTextTotalProgressTitle.Column[0].This, "", TlabelType.H0);
		lblTotalProgressTitle.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TOTALPROGRESSTITLE");
		lblTotalProgressTitle.VCLType = TVCLType.BS;
		var txtTotalProgressTitle = new TVclTextBox(ContTextTotalProgressTitle.Column[1].This, "txtTotalProgressTitleVOTE");
		txtTotalProgressTitle.Text = "";
		txtTotalProgressTitle.This.disabled = true;
		txtTotalProgressTitle.VCLType = TVCLType.BS;

		var ContTextProgressColor = new TVclStackPanel(ObjectHtml, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
		ContTextProgressColor.Row.This.style.marginBottom = '5px';
		var lblProgressColor = new TVcllabel(ContTextProgressColor.Column[0].This, "", TlabelType.H0);
		lblProgressColor.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "PROGRESSCOLOR");
		lblProgressColor.VCLType = TVCLType.BS;
		var ProgressColor = new Componet.TPalleteColor(ContTextProgressColor.Column[1].This, "txtProgressColorVOTE");
		ProgressColor.Color = "white";
		ProgressColor.Disabled = true;
		ProgressColor.CreatePallete();

		var ContTextInitGraphic = new TVclStackPanel(ObjectHtml, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
		ContTextInitGraphic.Row.This.style.marginBottom = '5px';
		var lblInitGraphic = new TVcllabel(ContTextInitGraphic.Column[0].This, "", TlabelType.H0);
		lblInitGraphic.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "INITGRAPHIC");
		lblInitGraphic.VCLType = TVCLType.BS;
		var cbInitGraphic = new TVclComboBox2(ContTextInitGraphic.Column[1].This, "txtInitGraphicVOTE");
		cbInitGraphic.VCLType = TVCLType.BS;
		var _arrayGraphic = ['Bar Graphic', 'Pie Graphic', 'TreeMap Graphic'];
		for (var i = 0; i < _arrayGraphic.length; i++) {
			var ComboItem = new TVclComboBoxItem();
			ComboItem.Value = i+1;
			ComboItem.Text = _arrayGraphic[i];
			ComboItem.Tag = "Test";
			cbInitGraphic.AddItem(ComboItem);
		}
		cbInitGraphic.selectedIndex = 0;
		cbInitGraphic.This.classList.add("DropDownListGroup");
		cbInitGraphic.This.disabled = true;

		var ContTextColumn = new TVclStackPanel(ObjectHtml, "", 1, [[8], [8], [8], [8], [8]]);
		ContTextColumn.Row.This.style.marginBottom = '5px';
		ContTextColumn.Row.This.children[0].classList.add('col-lg-offset-4');
		ContTextColumn.Row.This.children[0].classList.add('col-xl-offset-3');
		var ListBox = new TVclListBox(ContTextColumn.Column[0].This, "");
		ListBox.EnabledCheckBox = true;
		var ListBoxItem = new TVclListBoxItem();
		ListBoxItem.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "COMMENTREQUIRED");
		ListBoxItem.Index = 0;
		ListBox.AddListBoxItem(ListBoxItem);
		ListBox.ListBoxItems[0].This.Column[0].This.children[0].setAttribute('disabled','');
		ListBoxItem = new TVclListBoxItem();
		ListBoxItem.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "SHOWCOMMENT");
		ListBoxItem.Index = 1;
		ListBox.AddListBoxItem(ListBoxItem);
		ListBox.ListBoxItems[1].This.Column[0].This.children[0].setAttribute('disabled','');
		ListBoxItem = new TVclListBoxItem();
		ListBoxItem.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "SHOWCOMMENTGROUP");
		ListBoxItem.Index = 2;
		ListBox.AddListBoxItem(ListBoxItem);
		ListBox.ListBoxItems[2].This.Column[0].This.children[0].setAttribute('disabled','');

		ListBox.ListBoxItems[0].OnClick = function(){
			if(ListBox.ListBoxItems[0].Checked){
				ListBox.ListBoxItems[1].Checked = true;
				ListBox.ListBoxItems[2].Checked = true;
				ListBox.ListBoxItems[1].This.Column[0].This.style.display = 'none';
				ListBox.ListBoxItems[2].This.Column[0].This.style.display = 'none';
			}
			else{
				// ListBox.ListBoxItems[1].Checked = false;
				// ListBox.ListBoxItems[2].Checked = false;
				ListBox.ListBoxItems[1].This.Column[0].This.style.display = 'block';
				ListBox.ListBoxItems[2].This.Column[0].This.style.display = 'block';
			}
		}

		var ContButton = new TVclStackPanel(ObjectHtml, "", 2, [[12, 12], [12, 12], [12, 12], [12, 12], [12, 12]]);
		ContButton.Row.This.classList.add("DownSpace");

		var btnCancel = new TVclInputbutton(ContButton.Column[0].This, "");
		btnCancel.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "CANCEL");
		btnCancel.This.style.visibility = 'hidden'
		btnCancel.VCLType = TVCLType.BS;
		btnCancel.onClick = function () {
			txtName.This.disabled = true;
			txtGroup.This.disabled = true;
			txtQuestion.This.disabled = true;
			txtProgressTitle.This.disabled = true;
			txtTotalProgressTitle.This.disabled = true;
			cbInitGraphic.This.disabled = true;
			ProgressColor.Disabled = true;
			ListBox.ListBoxItems[0].This.Column[0].This.children[0].setAttribute('disabled','');
			ListBox.ListBoxItems[1].This.Column[0].This.children[0].setAttribute('disabled','');
			ListBox.ListBoxItems[2].This.Column[0].This.children[0].setAttribute('disabled','');

			// ListBox.ListBoxItems[1].This.Column[0].This.style.display = 'block';
			// ListBox.ListBoxItems[2].This.Column[0].This.style.display = 'block';

			btnAdd.This.style.display = 'none';
			btnUpdate.This.style.display = 'none';
			btnCancel.This.style.visibility = 'hidden';
		}
		btnCancel.This.style.float = "right";
		btnCancel.This.style.marginLeft = "10px";

		var btnAdd = new TVclInputbutton(ContButton.Column[0].This, "");
		btnAdd.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "SAVE");
		btnAdd.This.style.display = "none";
		btnAdd.VCLType = TVCLType.BS;
		btnAdd.onClick = function () {
			if (txtName.Text.trim() != ''){
				if(txtQuestion.Text.trim() != ''){
					var stateName = true;
					for(var i = 0 ; i < _this.VOTEList.length ; i++){
						if(_this.VOTEList[i].VOTE_NAME.toLowerCase() === txtName.Text.trim().toLowerCase()){
							stateName = false;
						}
					}
					if(stateName){
						if(_this.isInteger(parseInt(txtGroup.Text))){
							_this.SaveVote(txtName.Text.trim(), parseInt(txtGroup.Text.trim()), txtQuestion.Text.trim(), txtProgressTitle.Text.trim(), txtTotalProgressTitle.Text.trim(),
											 ProgressColor.Color, parseInt(cbInitGraphic.Value), ListBox.ListBoxItems[0].Checked, ListBox.ListBoxItems[1].Checked, ListBox.ListBoxItems[2].Checked);
							btnAdd.This.style.display = 'none';
							btnCancel.This.style.visibility = 'hidden';
						}
						else{
							alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NONUMBER"));
						}
					}
					else{
						alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "REPEATEDNAME"));
					}
				}
				else{
					alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NOQUESTION"));
				}
			}
			else {
				alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NONAME"));
			}

		}
		btnAdd.This.style.float = "right";

		var btnUpdate = new TVclInputbutton(ContButton.Column[0].This, "");
		btnUpdate.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "UPDATE");
		btnUpdate.This.style.display = "none";
		btnUpdate.VCLType = TVCLType.BS;
		btnUpdate.onClick = function () {
			
			if (txtName.Text.trim() != '') {
				if(txtQuestion.Text.trim() != ''){
					var stateName = true;
					for(var i = 0 ; i < _this.VOTEList.length ; i++){
						if(_this.VOTEList[i].IDVOTE !== parseInt(txtId.Text)){
							if(_this.VOTEList[i].VOTE_NAME.toLowerCase() === txtName.Text.trim().toLowerCase()){
								stateName = false;
							}
						}
					}
					if(stateName){
						if(_this.isInteger(parseInt(txtGroup.Text))){
							_this.UpdateVote(parseInt(txtId.Text.trim()), txtName.Text.trim(), parseInt(txtGroup.Text.trim()), txtQuestion.Text.trim(), txtProgressTitle.Text.trim(), txtTotalProgressTitle.Text.trim(),
											 ProgressColor.Color, parseInt(cbInitGraphic.Value), ListBox.ListBoxItems[0].Checked, ListBox.ListBoxItems[1].Checked, ListBox.ListBoxItems[2].Checked);
							btnUpdate.This.style.display = 'none';
							btnCancel.This.style.visibility = 'hidden'
						}
						else{
							alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NONUMBER"));
						}
					}
					else{
						alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "REPEATEDNAME"));
					}
				}
				else{
					alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NOQUESTION"));
				}
			}
			else {
				alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NONAME"));
			}

		}
		btnUpdate.This.style.float = "right";

		if (_this.ElementContentVote.table.ResponsiveCont.style.display == 'none') {
			ImagePreview.This.style.display = 'none';
			ImageUpdate.This.style.display = 'none';
			ImageDelete.This.style.display = 'none';
		}

		_this.ElementFormVote = {id: txtId,
								name: txtName, 
								group: txtGroup, 
								question: txtQuestion, 
								progress: txtProgressTitle, 
								totalProgress: txtTotalProgressTitle,
								color: ProgressColor,
								init: cbInitGraphic,
								listComment: ListBox.ListBoxItems,
								save: btnAdd,
								update: btnUpdate,
								cancel: btnCancel,
								ipreview: ImagePreview,
								iupdate: ImageUpdate,
								idelete: ImageDelete };
	} 
	catch (e) {
		SysCfg.Log.Methods.WriteLog("VotesBasic.js ItHelpCenter.VotesManager.TVotesBasic.prototype.CreateFormVote", e);
	}
}

ItHelpCenter.VotesManager.TVotesBasic.prototype.ShowPreview = function (id, name){
	var _this = this.TParent();
	try{
		_this.ElementContentVote.form.style.display = 'none';
		_this.ElementContentVoteOption.form.style.display = 'none';
		_this.btn2.This.style.display = 'none';

		_this.ContPreview.Row.This.style.position = 'relative';
		
		_this.ContPreview.Row.This.style.width = '100%';
		_this.ContPreview.Row.This.style.marginTop = '10px';

		var ImageBack = new TVclImagen(_this.ContPreview.Column[0].This, "");
		ImageBack.This.style.marginLeft = "5px";
		ImageBack.Title = "Back";
		ImageBack.Src = "image/24/back.png";
		ImageBack.Cursor = "pointer";
		ImageBack.onClick = function () {
			_this.ElementContentVote.form.style.display = 'block';
			_this.ElementContentVoteOption.form.style.display = 'block';
			_this.btn2.This.style.display = 'block';
			// _this.ContPreview.Row.This.children[0].style.display = 'none';
			$(_this.ContPreview.Column[0].This).html('');
		}
		ImageBack.This.style.marginBottom = '20px';
		ImageBack.This.style.marginTop = '10px';
		var Container = new TVclStackPanel(_this.ContPreview.Column[0].This, 'VotesBasic_', 1, [[12], [12], [12], [12], [12]]);
		if(!Source.Menu.IsMobil){
			_this.ContPreview.Row.This.style.paddingRight = '25px';
			Container.Column[0].This.style.paddingRight = '20%';
			Container.Column[0].This.style.paddingLeft = '15%';
			Container.Column[0].This.style.marginLeft = '7%';
		}
		else{
			_this.ContPreview.Column[0].This.style.paddingRight = '0px';
			Container.Column[0].This.style.paddingRight = '0px';
		}

		// /*var ArrayVotes = [{ idchoosed: '1', idvotes: '1', commentary: 'Good'},
		// 					{ idchoosed: '16', idvotes: '4', commentary: 'Good'}];*/

		var VotesManager = new ItHelpCenter.VotesManager.TVotesManagerBasic(Container.Column[0].This, false, /*ArrayVotes,*/null, 1, 'Demo', id);
	} 
	catch (e) {
		SysCfg.Log.Methods.WriteLog("VotesBasic.js ItHelpCenter.VotesManager.TVotesBasic.prototype.ShowPreview", e);
	}
}

ItHelpCenter.VotesManager.TVotesBasic.prototype.SaveVote = function (name, group, question, progress, totalProgress, color, init, commentRequired, comment, commentGroup){
	var _this = this.TParent();
	try{
		var VOTE = new Persistence.Votes.Properties.TVOTE();
		VOTE.VOTE_NAME = name;
		VOTE.VOTE_GROUP = group;
		VOTE.VOTE_QUESTION = question;
		VOTE.VOTE_COMMENTREQUIRED = commentRequired;
		VOTE.VOTE_PROGRESSCOLOR = color;
		VOTE.VOTE_PROGRESSTITLE = progress;
		VOTE.VOTE_TOTALPROGRESSTITLE = totalProgress;
		VOTE.VOTE_INITGRAPHIC = init;
		VOTE.VOTE_SHOWCOMMENT = comment;
		VOTE.VOTE_SHOWCOMMENTGROUP = commentGroup;

		var success = Persistence.Votes.Methods.VOTE_ADD(VOTE);
		if (success) {
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "CORRECTSAVE"));

			_this.ElementFormVote.id.Text = '';
			_this.ElementFormVote.name.Text = '';
			_this.ElementFormVote.group.Text = '0';
			_this.ElementFormVote.question.Text = '';
			_this.ElementFormVote.progress.Text = '';
			_this.ElementFormVote.totalProgress.Text = '';
			_this.ElementFormVote.init.selectedIndex = 0;
			_this.ElementFormVote.color.Color = 'white';
			_this.ElementFormVote.listComment[0].Checked = false;
			_this.ElementFormVote.listComment[1].Checked = false;
			_this.ElementFormVote.listComment[2].Checked = false;

			_this.ElementFormVote.id.This.disabled = true;
			_this.ElementFormVote.name.This.disabled = true;
			_this.ElementFormVote.group.This.disabled = true;
			_this.ElementFormVote.question.This.disabled = true;
			_this.ElementFormVote.progress.This.disabled = true;
			_this.ElementFormVote.totalProgress.This.disabled = true;
			_this.ElementFormVote.init.This.disabled = true;
			_this.ElementFormVote.color.Disabled = true;
			_this.ElementFormVote.listComment[0].This.Column[0].This.children[0].setAttribute('disabled','');
			_this.ElementFormVote.listComment[1].This.Column[0].This.children[0].setAttribute('disabled','');
			_this.ElementFormVote.listComment[2].This.Column[0].This.children[0].setAttribute('disabled','');

			_this.ListLoad();
			
			/*Eliminaci�n de Celdas de la tabla*/
			for (var i = 0; i < _this.ElementContentVote.table._Rows.length; i++) {
				$(_this.ElementContentVote.table._Rows[i].This).remove();
			}
			/*Llenado de Celdas de la tabla*/
			_this.AddRowVote();
		}
		else {
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECTSAVE"))
		}
	} 
	catch (e) {
		SysCfg.Log.Methods.WriteLog("VotesBasic.js ItHelpCenter.VotesManager.TVotesBasic.prototype.SaveVote", e);
	}
}

ItHelpCenter.VotesManager.TVotesBasic.prototype.UpdateVote = function (id, name, group, question, progress, totalProgress, color, init, commentRequired, comment, commentGroup) {
	var _this = this.TParent();
	try{
		var VOTE = new Persistence.Votes.Properties.TVOTE();
		VOTE.IDVOTE = id;
		VOTE.VOTE_NAME = name;
		VOTE.VOTE_GROUP = group;
		VOTE.VOTE_QUESTION = question;
		VOTE.VOTE_COMMENTREQUIRED = commentRequired;
		VOTE.VOTE_PROGRESSCOLOR = color;
		VOTE.VOTE_PROGRESSTITLE = progress;
		VOTE.VOTE_TOTALPROGRESSTITLE = totalProgress;
		VOTE.VOTE_INITGRAPHIC = init;
		VOTE.VOTE_SHOWCOMMENT = comment;
		VOTE.VOTE_SHOWCOMMENTGROUP = commentGroup;

		var success = Persistence.Votes.Methods.VOTE_UPD(VOTE);

		if(success){
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "CORRECTUPDATE"));
			_this.ElementFormVote.id.This.disabled = true;
			_this.ElementFormVote.name.This.disabled = true;
			_this.ElementFormVote.group.This.disabled = true;
			_this.ElementFormVote.question.This.disabled = true;
			_this.ElementFormVote.progress.This.disabled = true;
			_this.ElementFormVote.totalProgress.This.disabled = true;
			_this.ElementFormVote.init.This.disabled = true;
			_this.ElementFormVote.color.Disabled = true;
			_this.ElementFormVote.listComment[0].This.Column[0].This.children[0].setAttribute('disabled','');
			_this.ElementFormVote.listComment[1].This.Column[0].This.children[0].setAttribute('disabled','');
			_this.ElementFormVote.listComment[2].This.Column[0].This.children[0].setAttribute('disabled','');

			_this.ElementContentVoteOption.title.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, 'VOTEOPTION') + ': ' + name;

			_this.ListLoad();

			/*Eliminaci�n de Celdas de la tabla*/
			for (var i = 0; i < _this.ElementContentVote.table._Rows.length; i++) {
				$(_this.ElementContentVote.table._Rows[i].This).remove();
			}
			/*Llenado de Celdas de la tabla*/
			_this.AddRowVote();
			//Llenar dropdownlist-combobox VOTEOPTION
			for (var i = 0; i < _this.ElementFormVoteOption.parent.Options.length; i++) {
				if (parseInt(_this.ElementFormVoteOption.parent.Options[i].This.value) == VOTE.IDVOTE) {
					_this.ElementFormVoteOption.parent.Options[i].Text = VOTE.VOTE_NAME;
				}
			}
		}
		else{
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECTUPDATE"));
		}
	} 
	catch (e) {
		SysCfg.Log.Methods.WriteLog("VotesBasic.js ItHelpCenter.VotesManager.TVotesBasic.prototype.UpdateVote", e);
	}
}

ItHelpCenter.VotesManager.TVotesBasic.prototype.DeleteVote = function (idVote){
	var _this = this.TParent();
	try{
		var indexVote = Persistence.Votes.Methods.VOTE_ListGetIndex(_this.VOTEList, idVote);		
		if (_this.VOTEList[indexVote].VOTEOPTIONList.length != 0) {
			for (var i = 0; i < _this.VOTEList[indexVote].VOTEOPTIONList.length; i++) {
				var idVoteOption = _this.VOTEList[indexVote].VOTEOPTIONList[i].IDVOTEOPTION;
				for(var j = 0 ; j < _this.VOTEList[indexVote].VOTEOPTIONList[i].VOTEDATAUSERList.length ; j++){
					Persistence.Votes.Methods.VOTEDATAUSER_DEL(_this.VOTEList[indexVote].VOTEOPTIONList[i].VOTEDATAUSERList[j].IDVOTEDATAUSER);
				}
				Persistence.Votes.Methods.VOTEOPTION_DEL(idVoteOption);
			}
		}

		if (_this.VOTEList[indexVote].VOTEDATAList.length != 0) {
			for (var i = 0; i < _this.VOTEList[indexVote].VOTEDATAList.length; i++) {
				var idVoteData = _this.VOTEList[indexVote].VOTEDATAList[i].IDVOTEDATA;
				Persistence.Votes.Methods.VOTEDATA_DEL(idVoteData);
			}
		}

		var success = Persistence.Votes.Methods.VOTE_DEL(idVote);
		if (success) {
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "CORRECTDELETE"));

			$(_this.ElementContentVoteOption.form).remove(); //remover contenedor de VOTEOPTION

			_this.ElementFormVote.id.Text = '';
			_this.ElementFormVote.name.Text = '';
			_this.ElementFormVote.group.Text = '0';
			_this.ElementFormVote.question.Text = '';
			_this.ElementFormVote.progress.Text = '';
			_this.ElementFormVote.totalProgress.Text = '';
			_this.ElementFormVote.init.selectedIndex = 0;
			_this.ElementFormVote.color.Color = 'white';
			_this.ElementFormVote.listComment[0].Checked = false;
			_this.ElementFormVote.listComment[1].Checked = false;
			_this.ElementFormVote.listComment[2].Checked = false;

			_this.ElementFormVote.name.This.disabled = true;
			_this.ElementFormVote.group.This.disabled = true;
			_this.ElementFormVote.question.This.disabled = true;
			_this.ElementFormVote.progress.This.disabled = true;
			_this.ElementFormVote.totalProgress.This.disabled = true;
			_this.ElementFormVote.init.This.disabled = true;
			_this.ElementFormVote.color.Disabled = true;
			_this.ElementFormVote.listComment[0].This.Column[0].This.children[0].setAttribute('disabled','');
			_this.ElementFormVote.listComment[1].This.Column[0].This.children[0].setAttribute('disabled','');
			_this.ElementFormVote.listComment[2].This.Column[0].This.children[0].setAttribute('disabled','');

			_this.ElementFormVote.listComment[1].This.Column[0].This.style.display = 'block';
			_this.ElementFormVote.listComment[2].This.Column[0].This.style.display = 'block';

			_this.ListLoad();
			/*Eliminaci�n de Celdas de la tabla*/
			for (var i = 0; i < _this.ElementContentVote.table._Rows.length; i++) {
				$(_this.ElementContentVote.table._Rows[i].This).remove();
			}
			/*Llenado de Celdas de la tabla*/
			_this.AddRowVote();
		}
		else {
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECTDELETE"));
		}
	} 
	catch (e) {
		SysCfg.Log.Methods.WriteLog("VotesBasic.js ItHelpCenter.VotesManager.TVotesBasic.prototype.DeleteVote", e);
	}
}

ItHelpCenter.VotesManager.TVotesBasic.prototype.AddRowVote = function(){
	var _this = this.TParent();
	try{
		if (_this.VOTEList.length != 0) {
			for (var i = 0; i < _this.VOTEList.length; i++) {
				var NewRow = new TRow();
				var Cell0 = new TCell();
				Cell0.Value = _this.VOTEList[i].VOTE_NAME;
				Cell0.IndexColumn = 0;
				Cell0.IndexRow = i;
				NewRow.AddCell(Cell0);
				Cell0.This.setAttribute('data-id', _this.VOTEList[i].IDVOTE);
				Cell0.This.classList.add('col-xs-12'); //Eliminar en caso no funcione el fixed
				_this.ElementRowVote[i] = { row: Cell0.This };
				_this.ElementContentVote.table.AddRow(NewRow);
				_this.ElementRowVote[i].row.onclick = function () {
					_this.ElementFormVote.id.This.disabled = true;
					_this.ElementFormVote.name.This.disabled = true;
					_this.ElementFormVote.group.This.disabled = true;
					_this.ElementFormVote.question.This.disabled = true;
					_this.ElementFormVote.progress.This.disabled = true;
					_this.ElementFormVote.totalProgress.This.disabled = true;
					_this.ElementFormVote.init.This.disabled = true;
					_this.ElementFormVote.color.Disabled = true;
					_this.ElementFormVote.listComment[0].This.Column[0].This.children[0].setAttribute('disabled','');
					_this.ElementFormVote.listComment[1].This.Column[0].This.children[0].setAttribute('disabled','');
					_this.ElementFormVote.listComment[2].This.Column[0].This.children[0].setAttribute('disabled','');

					_this.ElementFormVote.save.This.style.display = 'none';
					_this.ElementFormVote.update.This.style.display = 'none';
					_this.ElementFormVote.cancel.This.style.visibility = 'hidden';
					_this.FillVote(parseInt(this.dataset.id));
				}
			}
			if (_this.ElementFormVote != null) {
				_this.ElementFormVote.iupdate.This.style.display = 'block';
				_this.ElementFormVote.idelete.This.style.display = 'block';
				_this.ElementFormVote.ipreview.This.style.display = 'block';
			}
			_this.ElementContentVote.table.ResponsiveCont.style.display = 'block';
			_this.ElementContentVote.table.EnabledResizeColumn = false;
			_this.ElementContentVote.formText.classList.remove('col-md-offset-2');
		}
		else {
			if (_this.ElementFormVote != null) {
				_this.ElementFormVote.iupdate.This.style.display = 'none';
				_this.ElementFormVote.idelete.This.style.display = 'none';
				_this.ElementFormVote.ipreview.This.style.display = 'none';
			}
			_this.ElementContentVote.table.ResponsiveCont.style.display = 'none';
			_this.ElementContentVote.table.EnabledResizeColumn = false;
			_this.ElementContentVote.formText.classList.add('col-md-offset-2');
		}
	} 
	catch (e) {
		SysCfg.Log.Methods.WriteLog("VotesBasic.js ItHelpCenter.VotesManager.TVotesBasic.prototype.AddRowVote", e);
	}
}

ItHelpCenter.VotesManager.TVotesBasic.prototype.FillVote = function(idVote){
	var _this = this.TParent();
	try{
		var indexVote = Persistence.Votes.Methods.VOTE_ListGetIndex(_this.VOTEList, idVote);
		_this.ElementFormVote.id.Text = _this.VOTEList[indexVote].IDVOTE;
		_this.ElementFormVote.name.Text = _this.VOTEList[indexVote].VOTE_NAME;
		_this.ElementFormVote.group.Text = _this.VOTEList[indexVote].VOTE_GROUP;
		_this.ElementFormVote.question.Text = _this.VOTEList[indexVote].VOTE_QUESTION;
		_this.ElementFormVote.progress.Text = _this.VOTEList[indexVote].VOTE_PROGRESSTITLE;
		_this.ElementFormVote.totalProgress.Text = _this.VOTEList[indexVote].VOTE_TOTALPROGRESSTITLE;
		_this.ElementFormVote.init.selectedIndex = _this.VOTEList[indexVote].VOTE_INITGRAPHIC - 1;
		_this.ElementFormVote.color.Color = _this.VOTEList[indexVote].VOTE_PROGRESSCOLOR;
		_this.ElementFormVote.listComment[0].Checked = _this.VOTEList[indexVote].VOTE_COMMENTREQUIRED;
		_this.ElementFormVote.listComment[1].Checked = _this.VOTEList[indexVote].VOTE_SHOWCOMMENT;
		_this.ElementFormVote.listComment[2].Checked = _this.VOTEList[indexVote].VOTE_SHOWCOMMENTGROUP;

		if(_this.ElementFormVote.listComment[0].Checked){
			_this.ElementFormVote.listComment[1].This.Column[0].This.style.display = 'none';
			_this.ElementFormVote.listComment[2].This.Column[0].This.style.display = 'none';
		}
		else{
			_this.ElementFormVote.listComment[1].This.Column[0].This.style.display = 'block';
			_this.ElementFormVote.listComment[2].This.Column[0].This.style.display = 'block';
		}

		_this.Content(_this._ObjectHtml, '', UsrCfg.Traslate.GetLangText(_this.Mythis, 'VOTEOPTION'), _this.VOTEList[i].VOTE_NAME, _this.VOTEList[i].VOTEOPTIONList);
	} 
	catch (e) {
		SysCfg.Log.Methods.WriteLog("VotesBasic.js ItHelpCenter.VotesManager.TVotesBasic.prototype.FillVote", e);
	}
}

ItHelpCenter.VotesManager.TVotesBasic.prototype.CreateFormVoteOption = function (objectHtml) {
	var _this = this.TParent();
	_this.ElementFormVoteOption = null;
	var ObjectHtml = objectHtml;
	try{
		var ContImages = new TVclStackPanel(ObjectHtml, "", 1, [[12], [12], [12], [12], [12]]);
		ContImages.Row.This.classList.add("DownSpace");
		var ImageDelete = new TVclImagen(ContImages.Column[0].This, "");
		ImageDelete.This.style.float = "right";
		ImageDelete.This.style.marginLeft = "5px";
		ImageDelete.Title = "Delete";
		ImageDelete.Src = "image/16/delete.png";
		ImageDelete.Cursor = "pointer";
		ImageDelete.onClick = function () {
			if (txtId.Text != '') {
				var confirmed = confirm(UsrCfg.Traslate.GetLangText(_this.Mythis, 'CONFIRMDELETE'));
				if (confirmed) {
					_this.DeleteVoteOption(parseInt(txtId.Text));
				}
				else {
					alert(UsrCfg.Traslate.GetLangText(_this.Mythis, 'NOCONFIRMDELETE'));
				}
			}
			else {
				alert(UsrCfg.Traslate.GetLangText(_this.Mythis, 'NOSELECT'));
			}
		}
		var ImageUpdate = new TVclImagen(ContImages.Column[0].This, "");
		ImageUpdate.This.style.marginLeft = "5px";
		ImageUpdate.This.style.float = "right";
		ImageUpdate.Title = "Edit";
		ImageUpdate.Src = "image/16/edit.png";
		ImageUpdate.Cursor = "pointer";
		ImageUpdate.onClick = function () {
			if (txtId.Text != '') {
				txtName.This.disabled = false;
				memoDescription.This.disabled = false;
				cbVote.This.disabled = false;
				btnAdd.This.style.display = "none";
				btnUpdate.This.style.display = "block";
				btnCancel.This.style.visibility = 'visible';
			}
			else {
				txtName.This.disabled = true;
				memoDescription.This.disabled = true;
				cbVote.This.disabled = true;
				alert(UsrCfg.Traslate.GetLangText(_this.Mythis, 'NOSELECT'));
				btnAdd.This.style.display = "none";
				btnUpdate.This.style.display = "none";
				btnCancel.This.style.visibility = 'hidden';
			}
		}
		var ImageAdd = new TVclImagen(ContImages.Column[0].This, "");
		ImageAdd.This.style.marginLeft = "5px";
		ImageAdd.This.style.float = "right";
		ImageAdd.Title = "Add";
		ImageAdd.Src = "image/16/add.png";
		ImageAdd.Cursor = "pointer";
		ImageAdd.onClick = function () {
			txtId.Text = '';
			txtName.Text = '';
			memoDescription.Text = '';
			txtName.This.disabled = false;
			memoDescription.This.disabled = false;
			cbVote.This.disabled = false;
			btnUpdate.This.style.display = 'none';
			btnAdd.This.style.display = "block";
			btnCancel.This.style.visibility = 'visible';
		}

		var ContTextId = new TVclStackPanel(ObjectHtml, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
		ContTextId.Row.This.style.marginBottom = '5px';
		var lblId = new TVcllabel(ContTextId.Column[0].This, "", TlabelType.H0);
		lblId.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ID");
		lblId.VCLType = TVCLType.BS;
		var txtId = new TVclTextBox(ContTextId.Column[1].This, "txtIDVOTE");
		txtId.Text = "";
		txtId.This.disabled = true;
		txtId.VCLType = TVCLType.BS;

		var ContTextName = new TVclStackPanel(ObjectHtml, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
		ContTextName.Row.This.style.marginBottom = '5px';
		var lblName = new TVcllabel(ContTextName.Column[0].This, "", TlabelType.H0);
		lblName.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "OPTIONNAME");
		lblName.VCLType = TVCLType.BS;
		var txtName = new TVclTextBox(ContTextName.Column[1].This, "txtNameVOTE");
		txtName.Text = "";
		txtName.This.disabled = true;
		txtName.VCLType = TVCLType.BS;

		var ContDescription = new TVclStackPanel(ObjectHtml, "1", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
		ContDescription.Row.This.style.marginBottom = '5px';
		var lblDescription = new TVcllabel(ContDescription.Column[0].This, "", TlabelType.H0);
		lblDescription.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "DESCRIPTION");
		lblDescription.VCLType = TVCLType.BS;
		var memoDescription = new TVclMemo(ContDescription.Column[1].This, "");
		memoDescription.VCLType = TVCLType.BS;
		memoDescription.This.removeAttribute("cols");
		memoDescription.This.removeAttribute("rows");
		memoDescription.This.classList.add("form-control");
		memoDescription.This.disabled = true;
		memoDescription.Text = '';

		/* Cambiar por un LIST */
		var ContVote = new TVclStackPanel(ObjectHtml, "1", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [3, 9]]);
		// ContMain.Row.This.classList.add("DownSpace");
		ContVote.Row.This.style.marginBottom = '5px';
		var lblVote = new TVcllabel(ContVote.Column[0].This, "", TlabelType.H0);
		lblVote.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "PARENT");
		lblVote.VCLType = TVCLType.BS;
		var cbVote = new TVclComboBox2(ContVote.Column[1].This, "");
		cbVote.VCLType = TVCLType.BS;
		
		var aux = null;
		for (var i = 0; i < _this.VOTEList.length; i++) {
			var ComboItem = new TVclComboBoxItem();
			ComboItem.Value = _this.VOTEList[i].IDVOTE;
			ComboItem.Text = _this.VOTEList[i].VOTE_NAME;
			ComboItem.Tag = "Test";
			cbVote.AddItem(ComboItem);
			if (_this.VOTEList[i].IDVOTE == parseInt(_this.ElementFormVote.id.Text)) {
				aux = i;
			}
		}
		cbVote.selectedIndex = aux;
		cbVote.This.classList.add("DropDownListGroup");
		cbVote.This.disabled = true;

		var ContButton = new TVclStackPanel(ObjectHtml, "", 2, [[12, 12], [12, 12], [12, 12], [12, 12], [12, 12]]);
		ContButton.Row.This.classList.add("DownSpace");

		var btnCancel = new TVclInputbutton(ContButton.Column[0].This, "");
		btnCancel.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "CANCEL");
		btnCancel.This.style.visibility = 'hidden'
		btnCancel.VCLType = TVCLType.BS;
		btnCancel.onClick = function () {
			txtName.This.disabled = true;
			memoDescription.This.disabled = true;
			cbVote.This.disabled = true;
			btnAdd.This.style.display = 'none';
			btnUpdate.This.style.display = 'none';
			btnCancel.This.style.visibility = 'hidden';
		}
		btnCancel.This.style.float = "right";
		btnCancel.This.style.marginLeft = "10px";

		var btnAdd = new TVclInputbutton(ContButton.Column[0].This, "");
		btnAdd.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "SAVE");
		btnAdd.This.style.display = "none";
		btnAdd.VCLType = TVCLType.BS;
		btnAdd.onClick = function () {
			if (txtName.Text.trim() != ''){
				var indexVote = Persistence.Votes.Methods.VOTE_ListGetIndex(_this.VOTEList, parseInt(cbVote.Value));
				var stateName = true;
				var VOTE = _this.VOTEList[indexVote];
				for(var i = 0 ; i < VOTE.VOTEOPTIONList.length ; i++){
					if(VOTE.VOTEOPTIONList[i].VOTEOPTION_NAME.toLowerCase() === txtName.Text.trim().toLowerCase()){
						stateName = false;
					}
				}
				if(stateName){
					_this.SaveVoteOption(txtName.Text.trim(), memoDescription.Text.trim(), parseInt(cbVote.Value));
					btnAdd.This.style.display = 'none';
					btnCancel.This.style.visibility = 'hidden';
				}
				else{
					alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "REPEATEDNAME"));
				}
			}
			else {
				alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NONAME"));
			}
		}
		btnAdd.This.style.float = "right";
		var btnUpdate = new TVclInputbutton(ContButton.Column[0].This, "");
		btnUpdate.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "UPDATE");
		btnUpdate.This.style.display = "none";
		btnUpdate.VCLType = TVCLType.BS;
		btnUpdate.onClick = function () {
			if (txtName.Text.trim() != '') {
				var indexVote = Persistence.Votes.Methods.VOTE_ListGetIndex(_this.VOTEList, parseInt(cbVote.Value));
				var stateName = true;
				var VOTE = _this.VOTEList[indexVote];
				for(var i = 0 ; i < VOTE.VOTEOPTIONList.length ; i++){
					if(VOTE.VOTEOPTIONList[i].IDVOTEOPTION !== parseInt(txtId.Text)){
						if(VOTE.VOTEOPTIONList[i].VOTEOPTION_NAME.toLowerCase() === txtName.Text.trim().toLowerCase()){
							stateName = false;
						}
					}
				}
				if(stateName){
					_this.UpdateVoteOption(parseInt(txtId.Text.trim()), txtName.Text.trim(), memoDescription.Text.trim(), parseInt(cbVote.Value));
					btnUpdate.This.style.display = 'none';
					btnCancel.This.style.visibility = 'hidden';
				}
				else{
					alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "REPEATEDNAME"));
				}
			}
			else {
				alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NONAME"));
			}
		}
		btnUpdate.This.style.float = "right";

		if (_this.ElementContentVoteOption.table.ResponsiveCont.style.display == 'none') {
			ImageUpdate.This.style.display = 'none';
			ImageDelete.This.style.display = 'none';
		}
		_this.ElementFormVoteOption = { id: txtId,
										name: txtName, 
										description: memoDescription,
										parent: cbVote,
										save: btnAdd,
										update: btnUpdate,
										cancel: btnCancel,
										iupdate: ImageUpdate,
										idelete : ImageDelete};
	} 
	catch (e) {
		SysCfg.Log.Methods.WriteLog("VotesBasic.js ItHelpCenter.VotesManager.TVotesBasic.prototype.CreateFormVoteOption", e);
	}
}

ItHelpCenter.VotesManager.TVotesBasic.prototype.SaveVoteOption = function (name, description, parent) {
	var _this = this.TParent();
	try{
		var VOTEOPTION = new Persistence.Votes.Properties.TVOTEOPTION();
		VOTEOPTION.VOTEOPTION_NAME = name;
		VOTEOPTION.VOTEOPTION_DESCRIPTION = description;
		VOTEOPTION.IDVOTE = parent;
		var success = Persistence.Votes.Methods.VOTEOPTION_ADD(VOTEOPTION);

		if(success){
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "CORRECTSAVE"));
			_this.ElementFormVoteOption.id.Text = '';
			_this.ElementFormVoteOption.name.Text = '';
			_this.ElementFormVoteOption.description.Text = '';		
			_this.ElementFormVoteOption.name.This.disabled = true;
			_this.ElementFormVoteOption.description.This.disabled = true;
			_this.ElementFormVoteOption.parent.This.disabled = true;
			_this.ListLoad();
			if (VOTEOPTION.IDVOTE == parseInt(_this.ElementFormVote.id.Text)) {
				/*Eliminaci�n de Celdas de la tabla*/
				for (var i = 0; i < _this.ElementContentVoteOption.table._Rows.length; i++) {
					$(_this.ElementContentVoteOption.table._Rows[i].This).remove();
				}
				/*Llenado de Celdas de la tabla*/
				var indexVote = Persistence.Votes.Methods.VOTE_ListGetIndex(_this.VOTEList, parseInt(_this.ElementFormVote.id.Text));
				_this.AddRowVoteOption(_this.VOTEList[indexVote].VOTEOPTIONList);
			}
			else{
				/*Regresar el combobox al parent de VOTEOPTION*/
				for (var j = 0; j < _this.ElementFormVoteOption.parent.Options.length; j++) {
					if (parseInt(_this.ElementFormVoteOption.parent.Options[j].Value) == parseInt(_this.ElementFormVote.id.Text)) {
						_this.ElementFormVoteOption.parent.selectedIndex = j;
					}
				}
			}
		}
		else {
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECTSAVE"));
		}
	} 
	catch (e) {
		SysCfg.Log.Methods.WriteLog("VotesBasic.js ItHelpCenter.VotesManager.TVotesBasic.prototype.SaveVoteOption", e);
	}
}

ItHelpCenter.VotesManager.TVotesBasic.prototype.UpdateVoteOption = function (id, name, description, parent) {
	var _this = this.TParent();
	try{
		var VOTEOPTION = new Persistence.Votes.Properties.TVOTEOPTION();
		VOTEOPTION.IDVOTEOPTION = id;
		VOTEOPTION.VOTEOPTION_NAME = name;
		VOTEOPTION.VOTEOPTION_DESCRIPTION = description;
		VOTEOPTION.IDVOTE = parent;
		var success = Persistence.Votes.Methods.VOTEOPTION_UPD(VOTEOPTION);
		if(success){
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "CORRECTUPDATE"));
			_this.ElementFormVoteOption.name.This.disabled = true;
			_this.ElementFormVoteOption.description.This.disabled = true;
			_this.ElementFormVoteOption.parent.This.disabled = true;
			_this.ListLoad();
			/*Eliminaci�n de Celdas de la tabla*/
			for (var i = 0; i < _this.ElementContentVoteOption.table._Rows.length; i++) {
				$(_this.ElementContentVoteOption.table._Rows[i].This).remove();
			}
			/*Llenado de Celdas de la tabla*/
			var indexVote = Persistence.Votes.Methods.VOTE_ListGetIndex(_this.VOTEList, parseInt(_this.ElementFormVote.id.Text));
			_this.AddRowVoteOption(_this.VOTEList[indexVote].VOTEOPTIONList);
			if(VOTEOPTION.IDVOTE != parseInt(_this.ElementFormVote.id.Text)){
				_this.ElementFormVoteOption.id.Text = '';
				_this.ElementFormVoteOption.name.Text = '';
				_this.ElementFormVoteOption.description.Text = '';		
				/*Regresar el combobox al parent de VOTEOPTION*/
				for (var j = 0; j < _this.ElementFormVoteOption.parent.Options.length; j++) {
					if (parseInt(_this.ElementFormVoteOption.parent.Options[j].Value) == parseInt(_this.ElementFormVote.id.Text)) {
						_this.ElementFormVoteOption.parent.selectedIndex = j;
					}
				}
			}
		}
		else{
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECTUPDATE"));
		}
	} 
	catch (e) {
		SysCfg.Log.Methods.WriteLog("VotesBasic.js ItHelpCenter.VotesManager.TVotesBasic.prototype.UpdateVoteOption", e);
	}
}

ItHelpCenter.VotesManager.TVotesBasic.prototype.DeleteVoteOption = function (idVoteOption){
	var _this = this.TParent();	
	try{
		_this.ListLoad();
		var indexOption = Persistence.Votes.Methods.VOTEOPTION_ListGetIndex(_this.VOTEOPTIONList, idVoteOption);
		var ctrl = 0;
		for(var i = 0 ; i < _this.VOTEOPTIONList[indexOption].VOTEDATAUSERList.length ; i++){
			var successData = Persistence.Votes.Methods.VOTEDATAUSER_DEL(_this.VOTEOPTIONList[indexOption].VOTEDATAUSERList[i].IDVOTEDATAUSER);
			if(successData){
				ctrl++;
			}
		}
		if(_this.VOTEOPTIONList[indexOption].VOTEDATAUSERList.length == ctrl){
			var success = Persistence.Votes.Methods.VOTEOPTION_DEL(idVoteOption);
			if(success){
				alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "CORRECTDELETE"));
				_this.ElementFormVoteOption.id.Text = '';
				_this.ElementFormVoteOption.name.Text = '';
				_this.ElementFormVoteOption.description.Text = '';		
				_this.ElementFormVoteOption.name.This.disabled = true;
				_this.ElementFormVoteOption.description.This.disabled = true;
				_this.ElementFormVoteOption.parent.This.disabled = true;
				_this.ListLoad();
				/*Eliminaci�n de Celdas de la tabla*/
				for (var j = 0; j < _this.ElementContentVoteOption.table._Rows.length; j++) {
					$(_this.ElementContentVoteOption.table._Rows[j].This).remove();
				}
				/*Llenado de Celdas de la tabla*/
				var indexVote = Persistence.Votes.Methods.VOTE_ListGetIndex(_this.VOTEList, parseInt(_this.ElementFormVote.id.Text));
				_this.AddRowVoteOption(_this.VOTEList[indexVote].VOTEOPTIONList);
			}
			else {
				alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECTDELETE"));
			}
		}
		else{
			alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "INCORRECTDELETE"));
		}
	} 
	catch (e) {
		SysCfg.Log.Methods.WriteLog("VotesBasic.js ItHelpCenter.VotesManager.TVotesBasic.prototype.DeleteVoteOption", e);
	}
}

ItHelpCenter.VotesManager.TVotesBasic.prototype.AddRowVoteOption = function(voteList) {
	var _this = this.TParent();
	try{
		if (voteList.length != 0) {
			for (var j = 0; j < voteList.length; j++) {
				var NewRow = new TRow();
				var Cell0 = new TCell();
				Cell0.Value = voteList[j].VOTEOPTION_NAME;
				Cell0.IndexColumn = 0;
				Cell0.IndexRow = j;
				NewRow.AddCell(Cell0);
				Cell0.This.setAttribute('data-id', voteList[j].IDVOTEOPTION);
				Cell0.This.classList.add('col-xs-12');
				_this.ElementRowVoteOption[j] = { row: Cell0.This };
				_this.ElementContentVoteOption.table.AddRow(NewRow);
				_this.ElementRowVoteOption[j].row.onclick = function () {
					_this.ElementFormVoteOption.id.This.disabled = true;
					_this.ElementFormVoteOption.name.This.disabled = true;
					_this.ElementFormVoteOption.description.This.disabled = true;
					_this.ElementFormVoteOption.parent.This.disabled = true;
					_this.ElementFormVoteOption.save.This.style.display = 'none';
					_this.ElementFormVoteOption.update.This.style.display = 'none';
					_this.ElementFormVoteOption.cancel.This.style.visibility = 'hidden';
					_this.FillVoteOption(parseInt(this.dataset.id));
				}
			}
			if (_this.ElementFormVoteOption != null) {
				_this.ElementFormVoteOption.iupdate.This.style.display = 'block';
				_this.ElementFormVoteOption.idelete.This.style.display = 'block';
			}
			_this.ElementContentVoteOption.table.ResponsiveCont.style.display = 'block';
			_this.ElementContentVoteOption.table.EnabledResizeColumn = false;
			_this.ElementContentVoteOption.formText.classList.remove('col-md-offset-2');
		}
		else{
			if (_this.ElementFormVoteOption != null) {
				_this.ElementFormVoteOption.iupdate.This.style.display = 'none';
				_this.ElementFormVoteOption.idelete.This.style.display = 'none';
			}
			_this.ElementContentVoteOption.table.ResponsiveCont.style.display = 'none';
			_this.ElementContentVoteOption.table.EnabledResizeColumn = false;
			_this.ElementContentVoteOption.formText.classList.add('col-md-offset-2');
		}
	} 
	catch (e) {
		SysCfg.Log.Methods.WriteLog("VotesBasic.js ItHelpCenter.VotesManager.TVotesBasic.prototype.AddRowVoteOption", e);
	}
}

ItHelpCenter.VotesManager.TVotesBasic.prototype.FillVoteOption = function(idVote){
	var _this = this.TParent();
	try{
		var indexVoteoption = Persistence.Votes.Methods.VOTEOPTION_ListGetIndex(_this.VOTEOPTIONList, idVote);
		_this.ElementFormVoteOption.id.Text = _this.VOTEOPTIONList[indexVoteoption].IDVOTEOPTION;
		_this.ElementFormVoteOption.name.Text = _this.VOTEOPTIONList[indexVoteoption].VOTEOPTION_NAME;
		_this.ElementFormVoteOption.description.Text = _this.VOTEOPTIONList[indexVoteoption].VOTEOPTION_DESCRIPTION;
		for (var i = 0; i < _this.ElementFormVoteOption.parent.Options.length; i++) {
			if (parseInt(_this.ElementFormVoteOption.parent.Options[i].Value) == _this.VOTEOPTIONList[indexVoteoption].IDVOTE) {
				_this.ElementFormVoteOption.parent.selectedIndex = i;
			}
		}
	} 
	catch (e) {
		SysCfg.Log.Methods.WriteLog("VotesBasic.js ItHelpCenter.VotesManager.TVotesBasic.prototype.FillVoteOption", e);
	}
}

ItHelpCenter.VotesManager.TVotesBasic.prototype.ListLoad = function () {
	var _this = this.TParent();
	try{
		_this.VotesProfiler.Fill();
		_this.VOTEList = _this.VotesProfiler.VOTEList;
		_this.VOTEOPTIONList = _this.VotesProfiler.VOTEOPTIONList;
	} 
	catch (e) {
		SysCfg.Log.Methods.WriteLog("VotesBasic.js ItHelpCenter.VotesManager.TVotesBasic.prototype.ListLoad", e);
	}
}

ItHelpCenter.VotesManager.TVotesBasic.prototype.AddStyleFile = function (nombre, onSuccess, onError) {
	var style = document.createElement("link");
	style.rel = "stylesheet";
	style.type = "text/css";
	style.href = nombre;
	var s = document.head.appendChild(style);
	s.onload = onSuccess;
	s.onerror = onError;
}

ItHelpCenter.VotesManager.TVotesBasic.prototype.LoadScript = function (nombre, onSuccess, onError) {
	var s = document.createElement("script");
	s.onload = onSuccess;
	s.onerror = onError;
	s.src = nombre;
	document.querySelector("head").appendChild(s);
}
