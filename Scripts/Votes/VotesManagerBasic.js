ItHelpCenter.VotesManager.TVotesManagerBasic = function (inObjectHtml, status, groupComments, idUser, nameUser, idVote) {
	this.TParent = function () {
		return this;
	}.bind(this);

	var _this = this.TParent();
	this._ObjectHtml = inObjectHtml;
	this._Key = null;
	this._status = status;
	this._idUser = idUser;
	this._nameUser = nameUser;
	this._idVote = idVote;
	this._Comments = groupComments;
	this.ListItemVote = null;
	this.SaveCommentary = function(){};

	_this.Load();
}

ItHelpCenter.VotesManager.TVotesManagerBasic.prototype.Load = function () {
	var _this = this.TParent();
	try{
		var VotesProfiler = new Persistence.Votes.TVotesProfiler();
		VotesProfiler.Fill();

		var VOTEList = VotesProfiler.VOTEList;

		var Container = new TVclStackPanel(this._ObjectHtml, 'VotesControl', 1, [[12], [12], [12], [12], [12]]);

		_this._Key = Persistence.Votes.Methods.VOTE_ListGetIndex(VOTEList, _this._idVote);

		if (_this._Key != null) {
			if(VOTEList[_this._Key].VOTEOPTIONList.length != 0 ){
				var Votes = new Componet.TVotes(Container.Column[0].This, "", "", VOTEList[_this._Key].VOTE_GROUP);
				if(_this._Comments != null){
					for (var i = 0; i < _this._Comments.length; i++){
						var ElementVotes = new Componet.TVotesElements.TItemVote();
						ElementVotes.IdNumberChoose = _this._Comments[i].IDVOTEOPTION;
						ElementVotes.IDCMDBCI = _this._Comments[i].IDCMDBCI;
						ElementVotes.Name = _this._Comments[i].VOTEDATAUSER_NAME;
						ElementVotes.Commentary = _this._Comments[i].VOTEDATAUSER_COMMENTARY;
						Votes.TVotesItemVoteAdd(ElementVotes);
					}
				}
				for (var i = 0; i < VOTEList[_this._Key].VOTEOPTIONList.length; i++) {
					var ElementChoose = new Componet.TVotesElements.TItemToChoose();
					ElementChoose.Id = VOTEList[_this._Key].VOTEOPTIONList[i].IDVOTEOPTION;
					ElementChoose.Name = VOTEList[_this._Key].VOTEOPTIONList[i].VOTEOPTION_NAME;
					ElementChoose.Description = VOTEList[_this._Key].VOTEOPTIONList[i].VOTEOPTION_DESCRIPTION;
					Votes.TVotesItemToChooseAdd(ElementChoose);

					// for (var j = 0; j < VOTEList[_this._Key].VOTEOPTIONList[i].VOTEDATAUSERList.length; j++){
					// 	var ElementVotes = new Componet.TVotesElements.TItemVote();
					// 	ElementVotes.IdNumberChoose = VOTEList[_this._Key].VOTEOPTIONList[i].VOTEDATAUSERList[j].IDVOTEOPTION;
					// 	ElementVotes.IDCMDBCI = VOTEList[_this._Key].VOTEOPTIONList[i].VOTEDATAUSERList[j].IDCMDBCI;
					// 	ElementVotes.Commentary = VOTEList[_this._Key].VOTEOPTIONList[i].VOTEDATAUSERList[j].VOTEDATAUSER_COMMENTARY;
					// 	Votes.TVotesItemVoteAdd(ElementVotes);
					// }
				}
				Votes.ProgressColor = VOTEList[_this._Key].VOTE_PROGRESSCOLOR;
				Votes.QuestionText = VOTEList[_this._Key].VOTE_QUESTION;
				Votes.CommentRequired = VOTEList[_this._Key].VOTE_COMMENTREQUIRED;
				Votes._ShowComment = VOTEList[_this._Key].VOTE_SHOWCOMMENT;
				Votes._ShowCommentsGroup= VOTEList[_this._Key].VOTE_SHOWCOMMENTGROUP; // caso el comentario no sea necesario se pueden mostrar textbox de comentarios
				Votes.ProgressTitle = VOTEList[_this._Key].VOTE_PROGRESSTITLE;
				Votes.TotalProgressTitle = VOTEList[_this._Key].VOTE_TOTALPROGRESSTITLE;
				Votes.IdUser = _this._idUser;
				Votes.NameUser = _this._nameUser;
				Votes.SetTypeGraphics = Votes.TypeGraphics.GetEnum(VOTEList[_this._Key].VOTE_INITGRAPHIC);   // Bar - Pie - Treemap
				if(!_this._status){
					Votes.View();
				}
				else{
					Votes.ShowStatistic();
				}
				Votes.FunctionVote = function(_value){
					_this.SaveCommentary(_value);
				}
				_this.ListItemVote = Votes._ListItemVote; //arreglo de elementos durante la sesión
				_this.ItemVote = Votes._ItemVoteTemp; //elemento actual al votar
			}
		}
	} 
	catch (e) {
		SysCfg.Log.Methods.WriteLog("VotesManagerBasic.js ItHelpCenter.VotesManager.TVotesManagerBasic.prototype.Load", e);
	}
}