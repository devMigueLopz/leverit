﻿ItHelpCenter.MD.TfrMDBase.prototype.InitializeComponent = function ()
{
    var _this = this.TParent();

    var spPrincipal = new TVclStackPanel(_this.Object, "2", 2, [[9, 3], [9, 3], [9, 3], [9, 3], [9, 3]]);

    _this.GridCFView = new Componet.GridCF.TGridCFView(spPrincipal.Column[0].This, function (OutRes) { }, null);
    _this.GridCFView.OnChangeIndex = function (GridCFView, Record)
    {
        _this.GridSelectedRowChanged(_this, Record);
    }

    //////////////////////////////////////////////

    var DivBotonera = _this.MenuObject.GetDivData(spPrincipal.Column[1], spPrincipal.Column[0]);

    var stackPanelBotonera = new TVclStackPanel(DivBotonera, "1", 1, [[12], [12], [12], [12], [12]]);

    if (Source.Menu.IsMobil)
    {
        stackPanelBotonera.Row.This.classList.add("menuContenedorMobil");
    }
    else
    {
        stackPanelBotonera.Row.This.style.margin = "0px 0px";
        stackPanelBotonera.Column[0].This.style.paddingLeft = "10px ";
        stackPanelBotonera.Column[0].This.style.paddingTop = "10px ";
        stackPanelBotonera.Column[0].This.style.border = "2px solid rgba(0, 0, 0, 0.08)";
        stackPanelBotonera.Column[0].This.style.boxShadow = " 0 0px 8px 0 rgba(0, 0, 0, 0.06), 0 1px 0px 0 rgba(0, 0, 0, 0.02);";
        stackPanelBotonera.Column[0].This.style.marginTop = "15px";
        stackPanelBotonera.Column[0].This.style.backgroundColor = "#FAFAFA";
    }

    this.stackPanelBodyBotonera = new TVclStackPanel(stackPanelBotonera.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);

    var spBotonera = new TVclStackPanel(this.stackPanelBodyBotonera.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
    spBotonera.Row.This.style.margin = "4px";

    ////////////////////////////////////

    if (UsrCfg.Version.IsProduction) {
        var spGraphic = new TVclStackPanel(spBotonera.Column[0].This, "1", 2, [[8, 4], [8, 4], [8, 4], [8, 4], [8, 4]]);
        $(spGraphic.Column[0].This).addClass("LabelBoton");

        _this.LabelGraphic = new TVcllabel(spGraphic.Column[0].This, "LabelGraphic", TlabelType.H0);
        _this.LabelGraphic.Text = "Graphic";
        var btnGraphic = new TVclButton(spGraphic.Column[1].This, "btnGraphic");
        btnGraphic.Cursor = "pointer";
        btnGraphic.Src = "image/24/Binary-tree.png";
        btnGraphic.VCLType = TVCLType.BS;
        btnGraphic.This.style.minWidth = "100%";
        btnGraphic.This.classList.add("btn-xs");
        btnGraphic.onClick = function myfunction() {
            _this.BtnGraphic_OnClick(_this, _this.BtnOk);
        }
    }
}

