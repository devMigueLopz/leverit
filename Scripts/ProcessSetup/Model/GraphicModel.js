ItHelpCenter.MD.TGraphicModel = function (inObjectHtml, inId, inCallback) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.ObjectHtml = inObjectHtml;
    this.CallBackFinish = inCallback;
    this.Name = "TGraphicModel";
    this.id = inId;
    this.IdMdModelTyped = 0;
    this.DiagramGraphic = null;
    this.Mythis = "GraphicModel";
    UsrCfg.Traslate.GetLangText(_this.Mythis, "GRAPHICATTENTIONMODEL", "Graphic of the attention model.");
    this.InitializeComponent();
}.Implements(new Source.Page.Properties.Page());

ItHelpCenter.MD.TGraphicModel.prototype.Inicialize = function (inIdMdModelTyped) {
    try {
        var _this = this.TParent();
        _this.IdMdModelTyped = inIdMdModelTyped;
        _this.CreateDiagram();
        _this.CreateDiagramView();
        _this.LoadData();
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicModel.js ItHelpCenter.MD.TGraphicModel.prototype.Inicialize", e);
    }
}
ItHelpCenter.MD.TGraphicModel.prototype.LoadData = function () {
    var _this = this.TParent();
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoAtisNames.MDMODELTYPED.IDMDMODELTYPED.FieldName, _this.IdMdModelTyped, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var OpenDataGridSet = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "GRAPHY_MODEL", Param.ToBytes());
        if (OpenDataGridSet.ResErr.NotError) {
            if (OpenDataGridSet.DataSet.RecordCount > 0) {
                var list = _this.ConvertList(OpenDataGridSet)
                var listGroup = Enumerable.From(list).GroupBy(function (element) { return element.TitleM; }).ToArray();
                for (var i = 0; i < listGroup.length; i++) {
                    if (i === 0) {//node origin
                        var item = listGroup[i];
                        var data = Enumerable.From(list).First(function (element) { return (element.TitleM == item.Key()); });
                        var TextNode1 = item.Key() + ((data != undefined && data != null) ? " (" + data.ServiceTypeName + ")" : "");
                        var ProperityNode1 = { Id: TextNode1, Text: TextNode1, AnchorPattern: MindFusion.Diagramming.AnchorPattern.topInBottomOut, Background: "#66ffb2", Image: "Home.png", Repeat: true, Origin: true, Parent: null }
                        var Node1 = _this.CreateNodeInDiagram(ProperityNode1);
                    }
                    var listGroup1 = Enumerable.From(list).Where(function (element) { return (element.TitleM == item.Key()); }).GroupBy(function (element) { return element.TypeUserName; }).ToArray();
                    for (var j = 0; j < listGroup1.length; j++) {
                        var item2 = listGroup1[j];
                        var listGroup2 = Enumerable.From(list).Where(function (element) { return (element.TitleM == item.Key() && element.TypeUserName == item2.Key()); }).GroupBy(function (element) { return element.TitleHierESC; }).ToArray();
                        for (var k = 0; k < listGroup2.length; k++) {
                            var item3 = listGroup2[k];
                            if (item3.Key() == "none") {
                                var listGroup3 = Enumerable.From(list).Where(function (element) { return (element.TitleM == item.Key() && element.TypeUserName == item2.Key() && element.TitleHierESC == item3.Key()); }).GroupBy(function (element) { return element.NameGroup; }).ToArray();
                                for (var l = 0; l < listGroup3.length; l++) {
                                    var item4 = listGroup3[l];
                                    var listGroup4 = Enumerable.From(list).Where(function (element) { return (element.TitleM == item.Key() && element.TypeUserName == item2.Key() && element.TitleHierESC == item3.Key() && element.NameGroup == item4.Key()); }).GroupBy(function (element) { return element.Perch; }).ToArray();
                                    for (var m = 0; m < listGroup4.length; m++) {
                                        var item5 = listGroup4[m];
                                        var TextNode2 = item2.Key() + " (" + item4.Key() + ")" + (parseInt(item5.Key()) == -1 ? "" : "(" + item5.Key() + " %)");
                                        console.log(TextNode1 + "==>" + TextNode2);
                                        var ProperityNode2 = { Id: TextNode2, Text: TextNode2, AnchorPattern: MindFusion.Diagramming.AnchorPattern.topInBottomOut, Background: "#66b2ff", Image: "catalog.png", Repeat: true, Origin: false, Parent: Node1 }
                                        var Node2 = _this.CreateNodeInDiagram(ProperityNode2);
                                        var listGroup5 = Enumerable.From(list).Where(function (element) { return (element.TitleM == item.Key() && element.TypeUserName == item2.Key() && element.TitleHierESC == item3.Key() && element.NameGroup == item4.Key() && element.Perch == item5.Key()); }).GroupBy(function (element) { return element.CiGenericName; }).ToArray();
                                        for (var n = 0; n < listGroup5.length; n++) {
                                            var item6 = listGroup5[n];
                                            var TextNode3 = item6.Key();
                                            console.log(TextNode2 + "==>" + TextNode3);
                                            var ProperityNode3 = { Id: TextNode3, Text: TextNode3, AnchorPattern: MindFusion.Diagramming.AnchorPattern.topInBottomOut, Background: "#ffffff", Image: "user-blue.png", Repeat: true, Origin: false, Parent: Node2 }
                                            var Node3 = _this.CreateNodeInDiagram(ProperityNode3);
                                        }
                                    }
                                }
                            }
                            else {
                                var TextNode2 = item2.Key() + " (" + item3.Key() + ")";
                                console.log(TextNode1 + "==>" + TextNode2);
                                var ProperityNode2 = { Id: TextNode2, Text: TextNode2, AnchorPattern: MindFusion.Diagramming.AnchorPattern.topInBottomOut, Background: "#66b2ff", Image: "catalog.png", Repeat: true, Origin: false, Parent: Node1 }
                                var Node2 = _this.CreateNodeInDiagram(ProperityNode2);
                                var listGroup3 = Enumerable.From(list).Where(function (element) { return (element.TitleM == item.Key() && element.TypeUserName == item2.Key() && element.TitleHierESC == item3.Key()); }).GroupBy(function (element) { return element.NameGroup; }).ToArray();
                                for (var l = 0; l < listGroup3.length; l++) {
                                    var item4 = listGroup3[l];

                                    var listGroup4 = Enumerable.From(list).Where(function (element) { return (element.TitleM == item.Key() && element.TypeUserName == item2.Key() && element.TitleHierESC == item3.Key() && element.NameGroup == item4.Key()); }).GroupBy(function (element) { return element.Perch; }).ToArray();
                                    for (var m = 0; m < listGroup4.length; m++) {
                                        var item5 = listGroup4[m];
                                        var TextNode3 = item4.Key() + (parseInt(item5.Key()) == -1 ? "" : " (" + item5.Key() + " %)");
                                        console.log(TextNode2 + "==>" + TextNode3);
                                        var ProperityNode3 = { Id: TextNode3, Text: TextNode3, AnchorPattern: MindFusion.Diagramming.AnchorPattern.topInBottomOut, Background: "#c0c0c0", Image: "Ruler.png", Repeat: true, Origin: false, Parent: Node2 }
                                        var Node3 = _this.CreateNodeInDiagram(ProperityNode3);
                                        var listGroup5 = Enumerable.From(list).Where(function (element) { return (element.TitleM == item.Key() && element.TypeUserName == item2.Key() && element.TitleHierESC == item3.Key() && element.NameGroup == item4.Key() && element.Perch == item5.Key()); }).GroupBy(function (element) { return element.CiGenericName; }).ToArray();
                                        for (var n = 0; n < listGroup5.length; n++) {
                                            var item6 = listGroup5[n];
                                            var TextNode4 = item6.Key();
                                            console.log(TextNode3 + "==>" + TextNode4);
                                            var ProperityNode4 = { Id: TextNode4, Text: TextNode4, AnchorPattern: MindFusion.Diagramming.AnchorPattern.topInBottomOut, Background: "#ffffff", Image: "user-blue.png", Repeat: true, Origin: false, Parent: Node3 }
                                            var Node4 = _this.CreateNodeInDiagram(ProperityNode4);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                _this.Arrange();
            }
            else {
                console.log(OpenDataGridSet.ResErr.Mesaje);
            }
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicModel.js ItHelpCenter.MD.TGraphicModel.prototype.InitializeComponent", e);
    }
    finally {
        Param.Destroy();
    }
}
ItHelpCenter.MD.TGraphicModel.prototype.Arrange = function () {
    var _this = this.TParent();
    var Layout = null;
    try {
        Layout = new ItHelpCenter.MD.TLayout();
        Layout.Id = "Layout";
        Layout.Direction = MindFusion.Graphs.LayoutDirection.LeftToRight;
        Layout.LinkType = MindFusion.Graphs.TreeLayoutLinkType.Cascading;
        Layout.NodeDistance=5;
        Layout.LevelDistance=-30;
        Layout.Create(_this.DiagramGraphic);
        _this.DiagramGraphic.ResizeToFitItems = 5;
        _this.DiagramGraphic.ZoomToRect = _this.DiagramGraphic.Element.bounds;
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicModel.js ItHelpCenter.MD.TGraphicModel.prototype.LoadData", e);
    }
}
ItHelpCenter.MD.TGraphicModel.prototype.BtnZoomUpClick = function (sender, element) {
    try {
        sender.DiagramGraphic.Element.setZoomFactor(Math.max(20, sender.DiagramGraphic.Element.zoomFactor + 10));

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicModel.js ItHelpCenter.MD.TGraphicModel.prototype.BtnZoomUpClick ", e);
    }
}
ItHelpCenter.MD.TGraphicModel.prototype.BtnZoomDownClick = function (sender, element) {
    try {
        sender.DiagramGraphic.Element.setZoomFactor(Math.max(20, sender.DiagramGraphic.Element.zoomFactor - 10));

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicModel.js ItHelpCenter.MD.TGraphicModel.prototype.BtnZoomDownClick ", e);
    }
}
ItHelpCenter.MD.TGraphicModel.prototype.BtnSavePngClick = function (sender, element) {
    try {
        var canvas = document.getElementById(sender.ElementDiagramGraphic.id);
        if (canvas.msToBlob) { //for IE
            var blob = canvas.msToBlob();
            window.navigator.msSaveBlob(blob, 'Diagram.png');
        } else {
            element.href = canvas.toDataURL();
            element.download = "Diagram.png";
        }

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicModel.js ItHelpCenter.MD.TGraphicModel.prototype.BtnSavePngClick", e);
    }
}
ItHelpCenter.MD.TGraphicModel.prototype.ConvertList = function (openDataGridSet) {
    try {
        var list = new Array();
        for (var i = 0; i < openDataGridSet.DataSet.Records.length; i++) {
            var item = openDataGridSet.DataSet.Records[i];
            var GraphicModelData = new ItHelpCenter.MD.TGraphicModelData();
            GraphicModelData.IdMdModelTyped = item.FieldName("IDMDMODELTYPED").asInt32();
            GraphicModelData.TitleM = item.FieldName("TITLEM").asString();
            GraphicModelData.ServiceTypeName = item.FieldName("SERVICETYPENAME").asString();
            GraphicModelData.IdSdTypeUser = item.FieldName("IDSDTYPEUSER").asInt32();
            GraphicModelData.TypeUserName = item.FieldName("TYPEUSERNAME").asString();
            GraphicModelData.TitleHierESC = item.FieldName("TITLEHIERESC").asString();
            GraphicModelData.NameGroup = item.FieldName("NAMEGROUP").asString();
            GraphicModelData.HierLavel = item.FieldName("HIERLAVEL").asInt32();
            GraphicModelData.Perch = item.FieldName("PERCH").asInt32();
            GraphicModelData.CiGenericName = item.FieldName("CI_GENERICNAME").asString();
            list.push(GraphicModelData);
        }
        return list;
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicModel.js ItHelpCenter.MD.TGraphicModel.prototype.ConvertList", e);
    }
}
ItHelpCenter.MD.TGraphicModel.prototype.CreateDiagram = function () {
    var _this = this.TParent();
    try {
        _this.DiagramGraphic = new ItHelpCenter.MD.TDiagram(_this.ElementDiagramGraphic, _this.Name + "_Diagram");
        _this.DiagramGraphic.HeaderArrow = 3;
        _this.DiagramGraphic.TypeDiagram = "RoundRect";
        _this.DiagramGraphic.HandlesStyle = MindFusion.Diagramming.HandlesStyle.Invisible
        _this.DiagramGraphic.Create();
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicModel.js ItHelpCenter.MD.TGraphicModel.prototype.CreateDiagram", e);
    }
}
ItHelpCenter.MD.TGraphicModel.prototype.CreateDiagramView = function () {
    var _this = this.TParent();
    try {
        var overview = MindFusion.AbstractionLayer.createControl(MindFusion.Diagramming.Overview,null, null, null, _this.ElementFractalGraphic);
        overview.setDiagram(_this.DiagramGraphic.Element);
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicModel.js ItHelpCenter.MD.TGraphicModel.prototype.CreateDiagramView", e);
    }
}
ItHelpCenter.MD.TGraphicModel.prototype.CreateNodeInDiagram = function (objProperties) {
    var _this = this.TParent();
    var Node = null;
    try {
        Node = new ItHelpCenter.MD.TDiagramNode(_this.DiagramGraphic);
        Node.AnchorPattern = objProperties.AnchorPattern;
        //Node.TextPadding = new Array(40, 10, 20, 10);
        Node.TextAlign = MindFusion.Diagramming.Alignment.Center;
        Node.ImageAlign = MindFusion.Drawing.ImageAlign.MiddleLeft;
        Node.Id = objProperties.Id;
        Node.Text = objProperties.Text;
        Node.Image = objProperties.Image;
        Node.Background = objProperties.Background;
        Node.Bounds = new Array(0, 0, 70, 10);
        Node.Repeat = objProperties.Repeat;
        Node.Origin = objProperties.Origin;
        Node.Parent = objProperties.Parent;
        Node.FontType = "sans-serif";
        Node.FontSize = 3.6;
        Node.Create();
        if (!Node.Origin) {
            var success = this.DiagramGraphic.SearchNode(Node);
            if (success) {
                _this.DiagramGraphic.UnionNodes(Node.Parent, Node);
                _this.DiagramGraphic.ListNode.push(Node);
            }
        }
        else {
            _this.DiagramGraphic.ListNode.push(Node);
        }
        return Node

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicModel.js ItHelpCenter.MD.TGraphicModel.prototype.CreateNodeInDiagram", e);
    }

}
ItHelpCenter.MD.TGraphicModelData = function () {
    try {
        this.IdMdModelTyped = 0;
        this.TitleM = "";
        this.ServiceTypeName = "";
        this.IdSdTypeUser = 0;
        this.TypeUserName = "";
        this.TitleHierESC = "";
        this.NameGroup = "";
        this.HierLavel = 0;
        this.Perch = 0;
        this.CiGenericName = "";
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicModel.js ItHelpCenter.MD.TGraphicModelData", e);
    }
}