ItHelpCenter.MD.TGraphicModel.prototype.InitializeComponent = function () {
    try {
        var _this = this.TParent();
        var container = new TVclStackPanel(_this.ObjectHtml, "", 1, [[12], [12], [12], [12], [12]]);
        var containerHeader = new TVclStackPanel(container.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
        var containerHeaderTitle = new TVclStackPanel(containerHeader.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
        _this.elementContainerHeaderTitle = new Uh3(containerHeaderTitle.Column[0].This, '', UsrCfg.Traslate.GetLangText(_this.Mythis, "GRAPHICATTENTIONMODEL"));
        var containerBody = new TVclStackPanel(container.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
        var containerBodyButtonBox = new TVclStackPanel(containerBody.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
        containerBodyButtonBox.Column[0].This.style.marginBottom = "10px";
        _this.BtnZoomUp = new TVclImagen(containerBodyButtonBox.Column[0].This, "");
        _this.BtnZoomUp.Src = "img/24/Zoom-in.png";
        _this.BtnZoomUp.Title = "Zoom In";
        _this.BtnZoomUp.Cursor = "pointer";
        _this.BtnZoomUp.This.style.margin = "0 10px 0 0";
        _this.BtnZoomUp.onClick = function () {
            try {
                _this.BtnZoomUpClick(_this, _this.BtnZoomUp);

            } catch (e) {
                SysCfg.Log.Methods.WriteLog("GraphicModelDesigner.js ItHelpCenter.MD.TGraphicModel.prototype.InitializeComponent _this.BtnZoomUp.onClick()", e);
            }
        }
        _this.BtnZoomDown = new TVclImagen(containerBodyButtonBox.Column[0].This, "");
        _this.BtnZoomDown.Src = "img/24/Zoom-out.png";
        _this.BtnZoomDown.Title = "Zoom Out";
        _this.BtnZoomDown.Cursor = "pointer";
        _this.BtnZoomDown.This.style.margin = "0 10px 0 0";
        _this.BtnZoomDown.onClick = function () {
            try {
                _this.BtnZoomDownClick(_this, _this.BtnZoomDown);
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("GraphicModelDesigner.js ItHelpCenter.MD.TGraphicModel.prototype.InitializeComponent _this.BtnZoomDown.onClick()", e);
            }
        }
        _this.BtnSavePng = new Ua(containerBodyButtonBox.Column[0].This, "", "");
        _this.BtnSavePng.style.cursor = "pointer";
        _this.ImageSavePng = new TVclImagen(_this.BtnSavePng, "");
        _this.ImageSavePng.Src = "img/24/PNG.png";
        _this.ImageSavePng.Title = "Save Image";
        $(_this.BtnSavePng).click(function () {
            try {
                _this.BtnSavePngClick(_this, _this.BtnSavePng)

            } catch (e) {
                SysCfg.Log.Methods.WriteLog("GraphicModelDesigner.js ItHelpCenter.MD.TGraphicModel.prototype.InitializeComponent  $(_this.BtnSavePng).click()", e);
            }
        });
        var containerBodyMain = new TVclStackPanel(containerBody.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
        var graphicContainerBodyMain = new TVclStackPanel(containerBodyMain.Column[0].This, "", 2, [[10, 2], [10, 2], [10, 2], [9, 2], [9, 2]]);
        var diagramGraphicContainerBodyMain = new TVclStackPanel(graphicContainerBodyMain.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
        var fractalGraphicContainerBodyMain = new TVclStackPanel(graphicContainerBodyMain.Column[1].This, "", 1, [[12], [12], [12], [12], [12]]);
        _this.ElementDiagramGraphic = UCanvas(diagramGraphicContainerBodyMain.Column[0].This, "ElementDiagramGraphic");
        _this.ElementFractalGraphic = UCanvas(fractalGraphicContainerBodyMain.Column[0].This, "ElementFractalGraphic");


        // _this.ElementFractalGraphic.width = 200;
        //_this.ElementFractalGraphic.height = 200;
        //var containerBodyMain = new TVclStackPanel(containerBody.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
        //var graphicContainerBodyMain = new TVclStackPanel(containerBodyMain.Column[0].This, "", 2, [[9, 3], [9, 3], [9, 3], [9, 3], [9, 3]]);
        //var diagramGraphicContainerBodyMain = new TVclStackPanel(graphicContainerBodyMain.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
        //var fractalGraphicContainerBodyMain = new TVclStackPanel(graphicContainerBodyMain.Column[1].This, "", 1, [[12], [12], [12], [12], [12]]);

        //var boxCanvas = new Udiv(containerBodyMain.Column[0].This, "");
        //boxCanvas.style.position = "absolute";
        //boxCanvas.style.left = "0px";
        //boxCanvas.style.top = "0px";
        //boxCanvas.style.bottom = "0px";
        //boxCanvas.style.width = "200px";
        //boxCanvas.style.borderRight = "1px solid #e2e4e7";
        //boxCanvas.style.overflow = "hidden";
        //boxCanvas.style.verticalAlign = "top";
        //var boxContentCanvas = new Udiv(boxCanvas, "");
        //boxContentCanvas.style.position = "absolute";
        //boxContentCanvas.style.top = "0px";
        //boxContentCanvas.style.bottom = "0px";
        //boxContentCanvas.style.right = "0px";
        //boxContentCanvas.style.width = "200px";
        //boxContentCanvas.style.height = "200px";
        //boxContentCanvas.style.borderBottom = "1px solid #e2e4e7";
        //boxContentCanvas.style.backgroundColor = "#c0c0c0";
        //_this.ElementFractalGraphic = UCanvas(boxContentCanvas, "ElementFractalGraphic");
        //_this.ElementFractalGraphic.width = 200;   
        //_this.ElementFractalGraphic.height = 200;

        //var boxDiagram = new Udiv(containerBodyMain.Column[0].This, "");
        //boxDiagram.style.position = "absolute";
        //boxDiagram.style.left = "201px";
        //boxDiagram.style.top = "0px";
        //boxDiagram.style.right = "0px";
        //boxDiagram.style.bottom = "0px";
        //boxDiagram.style.overflow = "auto";
        //_this.ElementDiagramGraphic = UCanvas(boxDiagram, "ElementDiagramGraphic");
        //_this.ElementDiagramGraphic.width = 2100;
        //_this.ElementDiagramGraphic.height = 2100;

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GraphicModelDesigner.js ItHelpCenter.MD.TGraphicModel.prototype.InitializeComponent", e);
    }
}
