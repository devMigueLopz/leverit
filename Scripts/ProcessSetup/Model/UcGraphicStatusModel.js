ItHelpCenter.MD.TUcGraphicStatusModel = function (inObjectHtml, inId, MDLIFESTATUSList, CASEMT_SET_LS_STATUSN, inCallback) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.Name = "TUcGraphicStatusModel";
    this.ObjectHtml = inObjectHtml;
    this.id = inId;
    this.MDLIFESTATUSList = MDLIFESTATUSList;
    this.CASEMT_SET_LS_STATUSN = SysCfg.Str.Methods.IsNullOrEmpity(CASEMT_SET_LS_STATUSN) ? 0 : CASEMT_SET_LS_STATUSN;
    this.CallBackFinish = inCallback;
    this.DiagramGraphic = null;
    this.Mythis = "GraphicStatusModel";
    UsrCfg.Traslate.GetLangText(_this.Mythis, "GRAPHICATTENTIONMODEL", "Graphic of the attention model.");
    this.InitializeComponent();
    this.Inicialize();
}.Implements(new Source.Page.Properties.Page());

ItHelpCenter.MD.TUcGraphicStatusModel.prototype.Inicialize = function () {
    try {
        var _this = this.TParent();
        _this.CreateDiagram();
        _this.CreateDiagramView();
        _this.LoadData();
        _this.Arrange();
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("UcGraphicStatusModel.js ItHelpCenter.MD.TUcGraphicStatusModel.prototype.Inicialize", e);
    }
}
ItHelpCenter.MD.TUcGraphicStatusModel.prototype.CreateDiagram = function () {
    var _this = this.TParent();
    try {
        _this.DiagramGraphic = new ItHelpCenter.MD.TDiagram(_this.ElementDiagramGraphic, _this.Name + "_Diagram");
        _this.DiagramGraphic.HeaderArrow = 3;
        _this.DiagramGraphic.TypeDiagram = "RoundRect";
        _this.DiagramGraphic.HandlesStyle = MindFusion.Diagramming.HandlesStyle.Invisible
        _this.DiagramGraphic.Create();
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("UcGraphicStatusModel.js ItHelpCenter.MD.TUcGraphicStatusModel.prototype.CreateDiagram", e);
    }
}
ItHelpCenter.MD.TUcGraphicStatusModel.prototype.CreateDiagramView = function () {
    var _this = this.TParent();
    try {
        var overview = MindFusion.AbstractionLayer.createControl(MindFusion.Diagramming.Overview, null, null, null, _this.ElementFractalGraphic);
        overview.setDiagram(_this.DiagramGraphic.Element);
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("UcGraphicStatusModel.js ItHelpCenter.MD.TUcGraphicStatusModel.prototype.CreateDiagramView", e);
    }
}
ItHelpCenter.MD.TUcGraphicStatusModel.prototype.LoadData = function () {
    var _this = this.TParent();
    try {
        for (var i = 0; i < _this.MDLIFESTATUSList.length; i++) {
            var Node = new ItHelpCenter.MD.TDiagramNode(_this.DiagramGraphic);
            Node.Id = _this.MDLIFESTATUSList[i].STATUSN;
            Node.Text = "[" + _this.MDLIFESTATUSList[i].STATUSN + "]" + _this.MDLIFESTATUSList[i].NAMESTEP;
            if (_this.MDLIFESTATUSList[i].STATUSN == _this.CASEMT_SET_LS_STATUSN) {
                Node.Background = "#25E31B";
            }
            else {
                Node.Background = "#ffffff";
            }
            _this.CreateNodeInDiagram(Node)
        }
        for (var x = 0; x < _this.MDLIFESTATUSList.length; x++) {
            var item = _this.MDLIFESTATUSList[x];
            for (var y = 0; y < item.NEXTSTEP.split(",").length; y++) {
                var origin = Enumerable.From(_this.DiagramGraphic.ListNode).First(function (element) { return (element.Id == item.STATUSN); });
                var NumSteepStr = SysCfg.Str.Methods.IsNullOrEmpity(item.NEXTSTEP.split(",")[y]) ? null : item.NEXTSTEP.split(",")[y];
                var target = (NumSteepStr) ? Enumerable.From(_this.DiagramGraphic.ListNode).First(function (element) { return (element.Id == NumSteepStr); }) : null;
                if (origin && target) {
                    _this.DiagramGraphic.UnionNodes(origin, target);
                }
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("UcGraphicStatusModel.js ItHelpCenter.MD.TUcGraphicStatusModel.prototype.LoadData", e);
    }
}
ItHelpCenter.MD.TUcGraphicStatusModel.prototype.Arrange = function () {
    var _this = this.TParent();
    var Layout = null;
    try {
        Layout = new ItHelpCenter.MD.TLayout();
        Layout.Id = "Layout";
        Layout.NodeDistance = 20;
        Layout.StatusCreate(_this.DiagramGraphic);
        //_this.DiagramGraphic.ResizeToFitItems = 40;
        //_this.DiagramGraphic.ZoomToRect = _this.DiagramGraphic.Element.bounds;
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("UcGraphicStatusModel.js ItHelpCenter.MD.TUcGraphicStatusModel.prototype.Arrange", e);
    }
}
ItHelpCenter.MD.TUcGraphicStatusModel.prototype.CreateNodeInDiagram = function (Node) {
    var _this = this.TParent();
    try {
        Node.AnchorPattern = MindFusion.Diagramming.AnchorPattern.topInBottomOut;
        Node.TextAlign = MindFusion.Diagramming.Alignment.Center;
        Node.TextPadding = new Array(4, 0, 4, 0);
        Node.Bounds = new Array(0, 0, 40, 10);
        Node.FontType = "sans-serif";
        Node.FontSize = 3.3;
        Node.Create();
        _this.DiagramGraphic.ListNode.push(Node)
        return Node
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("UcGraphicStatusModel.js ItHelpCenter.MD.TUcGraphicStatusModel.prototype.CreateNodeInDiagram", e);
    }
}
ItHelpCenter.MD.TUcGraphicStatusModel.prototype.BtnZoomUpClick = function (sender, element) {
    try {
        sender.DiagramGraphic.Element.setZoomFactor(Math.max(20, sender.DiagramGraphic.Element.zoomFactor + 10));
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("UcGraphicStatusModel.js ItHelpCenter.MD.TUcGraphicStatusModel.prototype.BtnZoomUpClick", e);
    }
}
ItHelpCenter.MD.TUcGraphicStatusModel.prototype.BtnZoomDownClick = function (sender, element) {
    try {
        sender.DiagramGraphic.Element.setZoomFactor(Math.max(20, sender.DiagramGraphic.Element.zoomFactor - 10));
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("UcGraphicStatusModel.js ItHelpCenter.MD.TUcGraphicStatusModel.prototype.BtnZoomDownClick", e);
    }
}
ItHelpCenter.MD.TUcGraphicStatusModel.prototype.BtnSavePngClick = function (sender, element) {
    try {
        var canvas = document.getElementById(sender.ElementDiagramGraphic.id);
        if (canvas.msToBlob) {
            var blob = canvas.msToBlob();
            window.navigator.msSaveBlob(blob, 'Diagram.png');
        } else {
            element.href = canvas.toDataURL();
            element.download = "Diagram.png";
        }

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("UcGraphicStatusModel.js ItHelpCenter.MD.TUcGraphicStatusModel.prototype.BtnSavePngClick", e);
    }
}