﻿ItHelpCenter.MD.TfrMDBase = function (inMenuObject, inObject, inCallback)
{
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.MenuObject = inMenuObject;
    this.Object = inObject;
    this.CallbackModalResult = inCallback;
    this.IDMDMODELTYPED = null;

    _this.name = "frMDBase";

    this.InitializeComponent();

    //************ <Lines><numerolinea> es para textos de mensages,alertas o componetes run time *********
    UsrCfg.Traslate.GetLangText(this.name, "Lines1", "The component is initialize");
    UsrCfg.Traslate.GetLangText(this.name, "Lines2", "Clicked the Close button");
    UsrCfg.Traslate.GetLangText(this.name, "Lines3", "The OK button was clicked");
    UsrCfg.Traslate.GetLangText(this.name, "Lines4", "Change the box status combo");

    //*********** <Nombredelcomponente>.<Porpiedad> ******************************************************
    //UsrCfg.Traslate.GetLangText(_this.name, "VCLxxx.Text", "Press Button to save");

    //*********** <Variable publicas>     ****************************************************************
    _this.ModalResult = Source.Page.Properties.TModalResult.Create;

}.Implements(new Source.Page.Properties.Page());

//{$R *.dfm}

ItHelpCenter.MD.TfrMDBase.prototype.Initialize = function ()
{
    var _this = this.TParent();

    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    var Open = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "MDB_001", Param.ToBytes());

    if (Open.ResErr.NotError)
    {
        _this.GridCFView.VclGridCF.DataSource = Open.DataSet;
    }
}

ItHelpCenter.MD.TfrMDBase.prototype.BtnGraphic_OnClick = function (sender, EventArgs)
{
    var _this = sender;
    alert(_this.IDMDMODELTYPED);
}

ItHelpCenter.MD.TfrMDBase.prototype.GridSelectedRowChanged = function (sender, EventArgs)
{
    var _this = sender;
    _this.SelectedRow = EventArgs;
    for (var i = 0; i < _this.SelectedRow.Fields.length; i++)
    {
        if (_this.SelectedRow.Fields[i].FieldDef.FieldName.toUpperCase() == "IDMDMODELTYPED")
        {
            _this.IDMDMODELTYPED = _this.SelectedRow.Fields[i].Value;
            break;
        }
    }
}

