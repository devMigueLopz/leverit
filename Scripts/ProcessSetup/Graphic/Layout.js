ItHelpCenter.MD.TLayout = function () {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();

    //Public Property//
    this.Id = null;
    //End Public Property//

    //Private Property//
    this._Element = null;
    this._Direction = null;
    this._LinkType = null;
    this._NodeDistance = null;
    this._LevelDistance = null;
    this._Arrange = null;
    //End Private Property//

    Object.defineProperty(this, 'Element', {
        get: function () {
            return this._Element;
        },
        set: function (_Value) {
            this._Element = _Value;
        }
    });
    Object.defineProperty(this, 'Direction', {
        get: function () {
            return this._Direction;
        },
        set: function (_Value) {
            this._Direction = SysCfg.Str.Methods.IsNullOrEmpity(_Value) ? 0 : _Value;
            if (!SysCfg.Str.Methods.IsNullOrEmpity(this._Element)) {
                this._Element.direction = this._Direction;
            }

        }
    });
    Object.defineProperty(this, 'LinkType', {
        get: function () {
            return this._LinkType;
        },
        set: function (_Value) {
            this._LinkType = SysCfg.Str.Methods.IsNullOrEmpity(_Value) ? 0 : _Value;
            if (!SysCfg.Str.Methods.IsNullOrEmpity(this._Element)) {
                this._Element.linkType = this._LinkType;
            }

        }
    });
    Object.defineProperty(this, 'NodeDistance', {
        get: function () {
            return this._NodeDistance;
        },
        set: function (_Value) {
            this._NodeDistance = SysCfg.Str.Methods.IsNullOrEmpity(_Value) ? 0 : _Value;
            if (!SysCfg.Str.Methods.IsNullOrEmpity(this._Element)) {
                this._Element.nodeDistance = this._NodeDistance;
            }
        }
    });
    Object.defineProperty(this, 'LevelDistance', {
        get: function () {
            return this._LevelDistance;
        },
        set: function (_Value) {
            this._LevelDistance = SysCfg.Str.Methods.IsNullOrEmpity(_Value) ? 0 : _Value;
            if (!SysCfg.Str.Methods.IsNullOrEmpity(this._Element)) {
                this._Element.levelDistance = this._LevelDistance;
            }
        }
    });
    Object.defineProperty(this, 'Arrange', {
        get: function () {
            return this._Arrange;
        },
        set: function (_Value) {
            this._Arrange = SysCfg.Str.Methods.IsNullOrEmpity(_Value) ? null : _Value;
            if (!SysCfg.Str.Methods.IsNullOrEmpity(this._Arrange) && !SysCfg.Str.Methods.IsNullOrEmpity(this._Element)) {
                this._Element.arrange(this._Arrange);
            }
        }
    });
}
ItHelpCenter.MD.TLayout.prototype.Create = function (diagram) {
    var _this = this.TParent();
    try {
        _this.ModuleLayout = MindFusion.Graphs.BorderedTreeLayout;
        _this.Element = SysCfg.Str.Methods.IsNullOrEmpity(_this.ModuleLayout) ? _this.Element : new _this.ModuleLayout();
        _this.Element.direction = _this._Direction;
        _this.Element.linkType = _this._LinkType;
        _this.Element.nodeDistance = _this._NodeDistance;
        _this.Element.levelDistance = _this._LevelDistance;
        if (!SysCfg.Str.Methods.IsNullOrEmpity(diagram.Element)) {
            diagram.Element.arrange(_this.Element);
            diagram.Layout = _this;
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Layout.js ItHelpCenter.MD.TLayout.prototype.Create", e);
    }
}
ItHelpCenter.MD.TLayout.prototype.StatusCreate = function (diagram) {
    var _this = this.TParent();
    try {
        _this.ModuleLayout = MindFusion.Graphs.LayeredLayout;
        _this.Element = SysCfg.Str.Methods.IsNullOrEmpity(_this.ModuleLayout) ? _this.Element : new _this.ModuleLayout();
        _this.Element.direction = _this._Direction;
        _this.Element.linkType = _this._LinkType;
        _this.Element.nodeDistance = _this._NodeDistance;
        _this.Element.levelDistance = _this._LevelDistance;
        if (!SysCfg.Str.Methods.IsNullOrEmpity(diagram.Element)) {
            diagram.Element.arrange(_this.Element);
            diagram.Layout = _this;
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Layout.js ItHelpCenter.MD.TLayout.prototype.StatusCreate", e);
    }
}
