ItHelpCenter.MD.TDiagram = function (inObjectHtml, inId) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();

    //Public Property//
    this.ObjectHtml = inObjectHtml;
    this.Id = inId;
    this.ListNode = new Array();
    this.Layout = null;
    this.ModuleDiagram = null;
    //End Public Property//

    //Private Property//
    this._Element = null;
    this._HeaderArrow = null;
    this._TypeDiagram = null;
    this._ZoomToRect = null;
    this._ResizeToFitItems = null;
    this._Arrange = null;
    this._HandlesStyle = null;
    //End Private Property//

    Object.defineProperty(this, 'Element', {
        get: function () {
            return this._Element;
        },
        set: function (_Value) {
            this._Element = _Value;
        }
    });
    Object.defineProperty(this, 'HeaderArrow', {
        get: function () {
            return this._HeaderArrow;
        },
        set: function (_Value) {
            this._HeaderArrow = SysCfg.Str.Methods.IsNullOrEmpity(_Value) ? 5 : _Value;
            if (!SysCfg.Str.Methods.IsNullOrEmpity(this._Element)) {
                this._Element.setLinkHeadShapeSize(this._HeaderArrow);
            }

        }
    });
    Object.defineProperty(this, 'TypeDiagram', {
        get: function () {
            return this._TypeDiagram;
        },
        set: function (_Value) {
            this._TypeDiagram = SysCfg.Str.Methods.IsNullOrEmpity(_Value) ? "Rectangle" : _Value;
            if (!SysCfg.Str.Methods.IsNullOrEmpity(this._Element)) {
                this._Element.setDefaultShape(this._TypeDiagram);
            }

        }
    });
    Object.defineProperty(this, 'ZoomToRect', {
        get: function () {
            return this._ZoomToRect;
        },
        set: function (_Value) {
            this._ZoomToRect = SysCfg.Str.Methods.IsNullOrEmpity(_Value) ? this._ZoomToRect : _Value;;
            if (!SysCfg.Str.Methods.IsNullOrEmpity(this._Element) && !SysCfg.Str.Methods.IsNullOrEmpity(this._ZoomToRect)) {
                this._Element.zoomToRect(this._ZoomToRect);
            }
        }
    });
    Object.defineProperty(this, 'ResizeToFitItems', {
        get: function () {
            return this._ResizeToFitItems;
        },
        set: function (_Value) {
            this._ResizeToFitItems = SysCfg.Str.Methods.IsNullOrEmpity(_Value) ? this._ResizeToFitItems : _Value;
            if (!SysCfg.Str.Methods.IsNullOrEmpity(this._Element) && !SysCfg.Str.Methods.IsNullOrEmpity(this._ResizeToFitItems)) {
                this._Element.resizeToFitItems(this._ResizeToFitItems);
            }
        }
    });
    Object.defineProperty(this, 'HandlesStyle', {
        get: function () {
            return this._HandlesStyle;
        },
        set: function (_Value) {
            this._HandlesStyle = SysCfg.Str.Methods.IsNullOrEmpity(_Value) ? this._HandlesStyle : _Value;
            if (!SysCfg.Str.Methods.IsNullOrEmpity(this._Element) && !SysCfg.Str.Methods.IsNullOrEmpity(this._HandlesStyle)) {
                //this._Element.setHandlesStyle(this._HandlesStyle);
            }
        }
    });
    Object.defineProperty(this, 'Arrange', {
        get: function () {
            return this._Arrange;
        },
        set: function (_Value) {
            this._Arrange = SysCfg.Str.Methods.IsNullOrEmpity(_Value) ? this._Arrange  : _Value;
            if (!SysCfg.Str.Methods.IsNullOrEmpity(this._Arrange) && !SysCfg.Str.Methods.IsNullOrEmpity(this._Element)) {
                this._Element.arrange(this._Arrange);
            }

        }
    });
}
ItHelpCenter.MD.TDiagram.prototype.Create = function () {
    var _this = this.TParent();
    try {
        _this.ModuleDiagram = MindFusion.Diagramming.Diagram;
        _this.Element = SysCfg.Str.Methods.IsNullOrEmpity(_this.ModuleDiagram) ? _this.Element : MindFusion.AbstractionLayer.createControl(_this.ModuleDiagram, null, null, null, _this.ObjectHtml);
        _this.HeaderArrow = _this._HeaderArrow;
        _this.TypeDiagram = _this._TypeDiagram;
        _this.ZoomToRect = _this._ZoomToRect;
        _this.ResizeToFitItems = _this._ZoomToRect;
        _this.Arrange = _this._Arrange;
        _this.HandlesStyle = _this._HandlesStyle;

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Diagram.js ItHelpCenter.MD.prototype.Create", e);
    }
}
ItHelpCenter.MD.TDiagram.prototype.ClearAll = function () {
    var _this = this.TParent();
    try {
        if (SysCfg.Str.Methods.IsNullOrEmpity(_this._Element)) {
            _this._Element.clearAll();
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Diagram.js ItHelpCenter.MD.prototype.ClearAll", e);
    }
}
ItHelpCenter.MD.TDiagram.prototype.Node = function (bounds) {
    var _this = this.TParent();
    try {
        if (!SysCfg.Str.Methods.IsNullOrEmpity(_this._Element)) {
            return (_this._Element.getFactory().createShapeNode(bounds));
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Diagram.js ItHelpCenter.MD.prototype.Node", e);
    }
}
ItHelpCenter.MD.TDiagram.prototype.SearchNode = function (node) {
    var _this = this.TParent();
    try {
        if (!node.Repeat) {
            for (var i = 0; i < ListNode.length; i++) {
                if (node.Id === ListNode[i].Id) {
                    return false;
                }
            }
            return true
        }
        else {
            return true;
        }

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Diagram.js ItHelpCenter.MD.prototype.SearchNode", e);
        return false;
    }
}
ItHelpCenter.MD.TDiagram.prototype.UnionNodes = function (parent, child) {
    var _this = this.TParent();
    try {
        if (!SysCfg.Str.Methods.IsNullOrEmpity(_this._Element)) {
            _this._Element.getFactory().createDiagramLink(parent.Node, child.Node);
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Diagram.js ItHelpCenter.MD.prototype.UnionNodes", e);
    }
}
