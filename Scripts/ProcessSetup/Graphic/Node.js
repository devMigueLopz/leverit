ItHelpCenter.MD.TDiagramNode = function (diagram) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();

    //Public Property//
    this.Diagram = diagram;
    //End Public Property//

    //Private Property//
    this._Node = null;
    this._Background = null;
    this._Image = null;
    this._ImageAlign = null;
    this._ImagePadding = null;
    this._Text = null;
    this._TextAlign = null;
    this._TextLineAlign = null;
    this._TextPadding = null;
    this._TextColor = null;
    this._AnchorPattern = null;
    this._Transparent = null;
    this._Bounds = null;
    this._Id = null;
    this._Parent = null
    this._Repeat = false;
    this._Origin = false;
    this._FontType = null;
    this._FontSize = null;
    this._FontBold = false;
    this._FontItalic = false;
    this._FontUnderLine = false;
    this._Visible = true;
    this._Tag = null;
    this._AllIncomingLinks = null;

    //End Private Property//

    Object.defineProperty(this, 'Node', {
        get: function () {
            return this._Node;
        },
        set: function (_Value) {
            this._Node = _Value;
        }
    });
    Object.defineProperty(this, 'Tag', {
        get: function () {
            return this._Tag;
        },
        set: function (_Value) {
            this._Tag = _Value;
        }
    });
    Object.defineProperty(this, 'Repeat', {
        get: function () {
            return this._Repeat;
        },
        set: function (_Value) {
            this._Repeat = _Value;
        }
    });
    Object.defineProperty(this, 'Origin', {
        get: function () {
            return this._Origin;
        },
        set: function (_Value) {
            this._Origin = _Value;
        }
    });
    Object.defineProperty(this, 'Parent', {
        get: function () {
            return this._Parent;
        },
        set: function (_Value) {
            this._Parent = _Value;
        }
    });
    Object.defineProperty(this, 'Background', {
        get: function () {
            return this._Background;
        },
        set: function (_Value) {
            this._Background = SysCfg.Str.Methods.IsNullOrEmpity(_Value) ? this._Background : _Value;
            if (!SysCfg.Str.Methods.IsNullOrEmpity(this._Node) && !SysCfg.Str.Methods.IsNullOrEmpity(this._Background)) {
                this._Node.setBrush(this._Background);
            }
        }
    });
    Object.defineProperty(this, 'Image', {
        get: function () {
            return this._Image;
        },
        set: function (_Value) {
            this._Image = SysCfg.Str.Methods.IsNullOrEmpity(_Value) ? this._Image : _Value;
            if (!SysCfg.Str.Methods.IsNullOrEmpity(this._Node) && !SysCfg.Str.Methods.IsNullOrEmpity(this._Image)) {
                //var img = new TVclImagen();
                //img.Src = SysCfg.App.Properties.xRaiz + "img/24/" + this._Image;
                this._Node.setImageLocation(SysCfg.App.Properties.xRaiz + "image/24/" + this._Image);
            }
        }
    });
    Object.defineProperty(this, 'ImageAlign', {
        get: function () {
            return this._ImageAlign;
        },
        set: function (_Value) {
            this._ImageAlign = SysCfg.Str.Methods.IsNullOrEmpity(_Value) ? this._ImageAlign : _Value;
            if (!SysCfg.Str.Methods.IsNullOrEmpity(this._Node) && !SysCfg.Str.Methods.IsNullOrEmpity(this._ImageAlign)) {
                this._Node.setImageAlign(this._ImageAlign);
            }
        }
    });
    Object.defineProperty(this, 'ImagePadding', {
        get: function () {
            return this._ImagePadding;
        },
        set: function (_Value) {
            this._ImagePadding = SysCfg.Str.Methods.IsNullOrEmpity(_Value) ? this._ImagePadding : _Value;
            if ((this._ImagePadding instanceof Array) && this._ImagePadding.length === 4) {
                for (var i = 0; i < this._ImagePadding.length; i++) {
                    this._ImagePadding[i] = SysCfg.Str.Methods.IsNullOrEmpity(this._ImagePadding[i]) ? 0 : this._ImagePadding[i];
                }
                this._ImagePadding = new MindFusion.Drawing.Thickness(this._ImagePadding[0], this._ImagePadding[1], this._ImagePadding[2], this._ImagePadding[3]);
            }
            if (!SysCfg.Str.Methods.IsNullOrEmpity(this._Node) && !SysCfg.Str.Methods.IsNullOrEmpity(this._ImagePadding)) {
                this._Node.setImagePadding(this._ImagePadding);
            }

        }
    });
    Object.defineProperty(this, 'Text', {
        get: function () {
            return this._Text;
        },
        set: function (_Value) {
            this._Text = SysCfg.Str.Methods.IsNullOrEmpity(_Value) ? this._Text : _Value;
            if (!SysCfg.Str.Methods.IsNullOrEmpity(this._Node) && !SysCfg.Str.Methods.IsNullOrEmpity(this._Text)) {
                this._Node.setText(this._Text);
            }
        }
    });
    Object.defineProperty(this, 'TextAlign', {
        get: function () {
            return this._TextAlign;
        },
        set: function (_Value) {
            this._TextAlign = SysCfg.Str.Methods.IsNullOrEmpity(_Value) ? this._TextAlign : _Value;
            if (!SysCfg.Str.Methods.IsNullOrEmpity(this._Node) && !SysCfg.Str.Methods.IsNullOrEmpity(this._TextAlign)) {
                this._Node.setTextAlignment(this._TextAlign);
            }
        }
    });
    Object.defineProperty(this, 'TextPadding', {
        get: function () {
            return this._TextPadding;
        },
        set: function (_Value) {
            this._TextPadding = SysCfg.Str.Methods.IsNullOrEmpity(_Value) ? this._TextPadding  : _Value;
            if ((this._TextPadding instanceof Array) && this._TextPadding.length === 4) {
                for (var i = 0; i < this._TextPadding.length; i++) {
                    this._TextPadding[i] = SysCfg.Str.Methods.IsNullOrEmpity(this._TextPadding[i]) ? 0 : this._TextPadding[i];
                }
                this._TextPadding = new MindFusion.Drawing.Thickness(this._TextPadding[0], this._TextPadding[1], this._TextPadding[2], this._TextPadding[3]);
            }
            if (!SysCfg.Str.Methods.IsNullOrEmpity(this._Node) && !SysCfg.Str.Methods.IsNullOrEmpity(this._TextPadding)) {
                this._Node.setTextPadding(this._TextPadding);
            }
        }
    });
    Object.defineProperty(this, 'TextLineAlign', {
        get: function () {
            return this._TextLineAlign;
        },
        set: function (_Value) {
            this._TextLineAlign = SysCfg.Str.Methods.IsNullOrEmpity(_Value) ? this._TextLineAlign : _Value;
            if (!SysCfg.Str.Methods.IsNullOrEmpity(this._Node) && !SysCfg.Str.Methods.IsNullOrEmpity(this._TextLineAlign)) {
                this._Node.setLineAlignment(this._TextLineAlign);

            }
        }
    });
    Object.defineProperty(this, 'TextColor', {
        get: function () {
            return this._TextColor;
        },
        set: function (_Value) {
            this._TextColor = SysCfg.Str.Methods.IsNullOrEmpity(_Value) ? this._TextColor : _Value;
            if (!SysCfg.Str.Methods.IsNullOrEmpity(this._Node) && !SysCfg.Str.Methods.IsNullOrEmpity(this._TextColor)) {
                this._Node.setTextColor(this._TextColor);
            }
        }
    });
    Object.defineProperty(this, 'AnchorPattern', {
        get: function () {
            return this._AnchorPattern;
        },
        set: function (_Value) {
            this._AnchorPattern = SysCfg.Str.Methods.IsNullOrEmpity(_Value) ? this._AnchorPattern : _Value;
            if (!SysCfg.Str.Methods.IsNullOrEmpity(this._Node) && !SysCfg.Str.Methods.IsNullOrEmpity(this._AnchorPattern)) {
                this._Node.setAnchorPattern(this._AnchorPattern);
            }
        }
    });
    Object.defineProperty(this, 'Transparent', {
        get: function () {
            return this._Transparent;
        },
        set: function (_Value) {
            this._Transparent = SysCfg.Str.Methods.IsNullOrEmpity(_Value) ? this._Transparent : _Value;
            if (!SysCfg.Str.Methods.IsNullOrEmpity(this._Node) && !SysCfg.Str.Methods.IsNullOrEmpity(this._Transparent)) {
                this._Node.setTransparent(this._Transparent);
            }
        }
    });
    Object.defineProperty(this, 'Id', {
        get: function () {
            return this._Id;
        },
        set: function (_Value) {
            this._Id = SysCfg.Str.Methods.IsNullOrEmpity(_Value) ? this._Id : _Value;
            if (!SysCfg.Str.Methods.IsNullOrEmpity(this._Node) && !SysCfg.Str.Methods.IsNullOrEmpity(this._Id)) {
                this._Node.setId(this._Id);
            }
        }
    });
    Object.defineProperty(this, 'Bounds', {
        get: function () {
            return this._Bounds;
        },
        set: function (_Value) {
            this._Bounds = SysCfg.Str.Methods.IsNullOrEmpity(_Value) ? this._Bounds : _Value;
            if ((this._Bounds instanceof Array) && this._Bounds.length === 4) {
                for (var i = 0; i < this._Bounds.length; i++) {
                    this._Bounds[i] = SysCfg.Str.Methods.IsNullOrEmpity(this._Bounds[i]) ? 0 : this._Bounds[i];
                }
                this._Bounds = new MindFusion.Drawing.Rect(this._Bounds[0], this._Bounds[1], this._Bounds[2], this._Bounds[3])
            }
        }
    });
    Object.defineProperty(this, 'FontType', {
        get: function () {
            return this._FontType;
        },
        set: function (_Value) {
            this._FontType = SysCfg.Str.Methods.IsNullOrEmpity(_Value) ? this._FontType : _Value;
            if (!SysCfg.Str.Methods.IsNullOrEmpity(this._Node) && !SysCfg.Str.Methods.IsNullOrEmpity(this._FontType)) {
                if (!SysCfg.Str.Methods.IsNullOrEmpity(this._FontSize)) {
                    this._Node.setFont(new MindFusion.Drawing.Font(this._FontType, this._FontSize, this.FontBold, this.FontItalic, this.FontUnderLine));
                }
            }
        }
    });
    Object.defineProperty(this, 'FontSize', {
        get: function () {
            return this._FontSize;
        },
        set: function (_Value) {
            this._FontSize = SysCfg.Str.Methods.IsNullOrEmpity(_Value) ? this._FontSize : _Value;
            if (!SysCfg.Str.Methods.IsNullOrEmpity(this._Node) && !SysCfg.Str.Methods.IsNullOrEmpity(this._FontSize)) {
                if (!SysCfg.Str.Methods.IsNullOrEmpity(this._FontType)) {
                    this._Node.setFont(new MindFusion.Drawing.Font(this._FontType, this._FontSize, this.FontBold, this.FontItalic, this.FontUnderLine));
                }
            }
        }
    });
    Object.defineProperty(this, 'FontBold', {
        get: function () {
            return this._FontBold;
        },
        set: function (_Value) {
            this._FontBold = SysCfg.Str.Methods.IsNullOrEmpity(_Value) ? this._FontBold : _Value;
            if (!SysCfg.Str.Methods.IsNullOrEmpity(this._Node) && !SysCfg.Str.Methods.IsNullOrEmpity(this._FontBold)) {
                if (!SysCfg.Str.Methods.IsNullOrEmpity(this._FontType) && !SysCfg.Str.Methods.IsNullOrEmpity(this._FontSize)) {
                    this._Node.setFont(new MindFusion.Drawing.Font(this._FontType, this._FontSize, this.FontBold, this.FontItalic, this.FontUnderLine));
                }
            }
        }
    });
    Object.defineProperty(this, 'FontItalic', {
        get: function () {
            return this._FontItalic;
        },
        set: function (_Value) {
            this._FontItalic = SysCfg.Str.Methods.IsNullOrEmpity(_Value) ? this.FontItalic : _Value;
            if (!SysCfg.Str.Methods.IsNullOrEmpity(this._Node) && !SysCfg.Str.Methods.IsNullOrEmpity(this._FontItalic)) {
                if (!SysCfg.Str.Methods.IsNullOrEmpity(this._FontType) && !SysCfg.Str.Methods.IsNullOrEmpity(this._FontSize)) {
                    this._Node.setFont(new MindFusion.Drawing.Font(this._FontType, this._FontSize, this.FontBold, this.FontItalic, this.FontUnderLine));
                }
            }
        }
    });
    Object.defineProperty(this, 'FontUnderLine', {
        get: function () {
            return this._FontUnderLine;
        },
        set: function (_Value) {
            this._FontUnderLine = SysCfg.Str.Methods.IsNullOrEmpity(_Value) ? this.FontUnderLine : _Value;
            if (!SysCfg.Str.Methods.IsNullOrEmpity(this._Node) && !SysCfg.Str.Methods.IsNullOrEmpity(this._FontUnderLine)) {
                if (!SysCfg.Str.Methods.IsNullOrEmpity(this._FontType) && !SysCfg.Str.Methods.IsNullOrEmpity(this._FontSize)) {
                    this._Node.setFont(new MindFusion.Drawing.Font(this._FontType, this._FontSize, this.FontBold, this.FontItalic, this.FontUnderLine));
                }
            }
        }
    });
    Object.defineProperty(this, 'Visible', {
        get: function () {
            return this._Visible;
        },
        set: function (_Value) {
            this._Visible = SysCfg.Str.Methods.IsNullOrEmpity(_Value) ? this._Visible : _Value;
            if (!SysCfg.Str.Methods.IsNullOrEmpity(this._Node) && !SysCfg.Str.Methods.IsNullOrEmpity(this._Visible)) {
                if (!SysCfg.Str.Methods.IsNullOrEmpity(this._Visible)) {
                    this._Node.setVisible(this._Visible);
                }
            }
        }
    });
    Object.defineProperty(this, 'AllIncomingLinks', {
        get: function () {
            var arrayAllIncomingLinks = new Array()
            if (!SysCfg.Str.Methods.IsNullOrEmpity(this._Node)) {
                _this._Node.getAllIncomingLinks(arrayAllIncomingLinks);
            }
            this._AllIncomingLinks = arrayAllIncomingLinks;
            return this._AllIncomingLinks;
        }
    });
}
ItHelpCenter.MD.TDiagramNode.prototype.Create = function () {
    var _this = this.TParent();
    try {
        _this.Node = _this.Diagram.Node(_this._Bounds);
        _this.Background = _this._Background;
        _this.Text = _this._Text;
        _this.TextAlign = _this._TextAlign;
        _this.TextLineAlign = _this._TextLineAlign;
        _this.TextPadding = _this.TextPadding;
        _this.TextColor = _this._TextColor;
        _this.AnchorPattern = _this._AnchorPattern;
        _this.Transparent = _this._Transparent;
        _this.Bounds = _this._Bounds;
        _this.Id = _this._Id;
        _this.Image = _this._Image;
        _this.ImageAlign = _this._ImageAlign;
        _this.ImagePadding = _this._ImagePadding;
        _this.FontType = _this._FontType; 
        _this.FontSize = _this._FontSize;
        _this.FontBold = _this._FontBold;
        _this.FontItalic = _this._FontItalic;
        _this.FontUnderLine = _this._FontUnderLine;
        _this.Visible = _this._Visible;
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Node.js ItHelpCenter.MD.TDiagramNode.prototype.Create", e);
    }
}


