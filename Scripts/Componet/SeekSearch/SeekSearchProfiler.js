﻿//Autor:JJaimes
//ItHelpCenter.Componet.SeekSearch.


ItHelpCenter.Componet.SeekSearch.TSeekSearchProfiler = function () {
    this.VectorList = new Array(); //Componet.SeekSearch.Properties.TVector

    this.Traelista = function (Inphrase, ModeSeek) //Componet.SeekSearch.Properties.TVectorResult
    {

        if (ModeSeek == ItHelpCenter.Componet.SeekSearch.Properties.TModeSeek.Phase) {
            return TraelistaV2(Inphrase);
        }
        if (ModeSeek == ItHelpCenter.Componet.SeekSearch.Properties.TModeSeek.Like) {
            return TraelistaV3(Inphrase);//la version 3 es una busqueda comparada tipo LIKE
        }

        return TraelistaV2(Inphrase);
    }

    //PROCESO DE BUSCAR RESULTADOS Version 1
    this.TraelistaV1 = function (Inphrase) //Componet.SeekSearch.Properties.TVectorResult
    {
        var VectorResultList = new Array(); //Componet.SeekSearch.Properties.TVectorResult


        //declarar el Vector de respuesta
        //List<Componet.SeekSearch.Properties.TVector> UnVectorList = null;
        try {
            //UnVectorList = new List<Componet.SeekSearch.Properties.TVector>();
            if (Inphrase != "") {
                for (var x = 0; x < this.VectorList.length; x++) //recorrer la lista fija del sistema
                {
                    var fraseFija = this.VectorList[x].Phrase; //tomar la frase 
                    var valorFrase = 0; //contabilizar le valor para una frase 

                    for (var y = 0; y < this.VectorList[x].WordList.length; y++) //recorer las palabras de la lsita fija
                    {
                        var WordFija = this.VectorList[x].WordList[y].WordName; //tomar la palabra fija de la lista  
                        Inphrase = Inphrase.Trim();
                        var VectorWordIn = Inphrase.split(' '); //hacer un arreglo de palabras con la frase escrita por el usuario

                        for (var z = 0; z < VectorWordIn.length; z++) //recorer palabras escritas por usuario
                        {
                            var worldUser = VectorWordIn[z]; //tomar la palabra escrita por el usuario en posicion z
                            var VectorletrasUser = worldUser.split('').join(','); //hacer un arreglo de las letras en la palabra escrita
                            var NLetrasCorrect = 0; //cuenta cuantas letras encontradas en la palabra (pArray)
                            var NLetrasWrong = 0; // cuenta cuantas letras errones en la palabra (pArray)
                            var indices = ""; //se llena con los indices de coincidencias para evitar coincidencias duplicadas ejemplo tallla                                            en tala encuentra 3 concidencias en la l, estos indices no se compararan mas de 2 veces si esa                                            letra ya fue una coincidencia 

                            for (var k = 0; k < VectorletrasUser.length; k++) //recorer letras de la palabra del usuario
                            {
                                var letraUser = VectorletrasUser[k].toString(); //tomar la letra en posision[k] de la palabra escrita por el usuario
                                var Vectorletrasfija = WordFija.split('').join(','); //hacer un arreglo de las letras en la palabra fija pArray
                                var encontro = false; //vaiable que verifica si se encontro o no una conicidencia

                                for (var j = 0; j < Vectorletrasfija.length; j++) //recorrer las letras de la palabra fija
                                {
                                    var letraFija = Vectorletrasfija[j].toString(); //tomar la letra en posision[j] de la palabra fija
                                    //validar indices
                                    //if (!(indices.Contains(j.toString())))
                                    if (!(indices.indexOf(j.toString()) != -1)) //validar si j no esta en los indices ya hallados, estos no se compararan
                                    {
                                        letraUser = letraUser.trim();
                                        letraFija = letraFija.trim();
                                        letraUser = letraUser.toUpper();
                                        letraFija = letraFija.toUpper();

                                        if (letraUser == letraFija) //comparar las letras de la palabra del usuario y la palabra fija
                                        {
                                            encontro = true;
                                            indices += j.toString();
                                            j = Vectorletrasfija.length; //romper el ciclo j
                                        }
                                    }
                                } //end for j

                                //comparar letras k contra letras J
                                if (encontro == true) {
                                    NLetrasCorrect++; //
                                }
                                else {
                                    NLetrasWrong++;
                                }

                                //validar sel porcentaje de coincidencia entre la palabra y las letras encontradas
                                var nltr = WordFija.length;
                                var porC = (NLetrasCorrect * 100) / nltr;
                                if (porC > 100) { //evitar que se supere el 100% 
                                    porC = 100;
                                }
                                var porX = (NLetrasWrong * 100) / nltr;
                                if (porX > 100) {
                                    porX = 100;
                                }
                                var por = porC - porX; //quitamos el porcentaje de error

                                //validar si es el ultimo ciclo k y el porcentaje supera el 39% es una posible palabra
                                if ((k == (VectorletrasUser.length - 1)) && por > 39) {
                                    //tomar el valor de la frase (valor de la plabra * porcentaje de coincidencia)
                                    valorFrase += this.VectorList[x].WordList[y].Value * por;
                                }

                                //si es el ultimo ciclo de y & hay palabras
                                if ((y == (this.VectorList[x].WordList.length - 1)) && valorFrase > 0 && (z == (VectorWordIn.length - 1)) && (k == (VectorletrasUser.length - 1))) {
                                    //guadar resultados
                                    var VectorResult = new ItHelpCenter.Componet.SeekSearch.Properties.TVectorResult();
                                    VectorResult.Vector = this.VectorList[x];
                                    VectorResult.ID = this.VectorList[x].ID;
                                    VectorResult.Phrase = this.VectorList[x].Phrase;
                                    VectorResult.Path = this.VectorList[x].Path;
                                    VectorResult.Value = valorFrase;

                                    //Componet.SeekSearch.Properties.TVector VectorFill = new Properties.TVector();
                                    //VectorFill.Phrase = fraseFija;
                                    //VectorFill.Value = valorFrase;
                                    //agregar vector a las lista de respuesta
                                    VectorResultList.push(VectorResult);
                                    //UnVectorList.Add(VectorFill);
                                }
                            } //end for k
                        } //end for z
                    } //end for y
                } //end for x

                //validar existencia de elementos
                if (VectorResultList.length < 1) {
                    //Componet.SeekSearch.Properties.TVectorResult VectorResult = new Properties.TVectorResult();
                    //VectorResult.Vector = null;
                    //VectorResultList.Add(VectorResult);
                }
                return (VectorResultList);

            }
            else {
                //Componet.SeekSearch.Properties.TVectorResult VectorResult = new Properties.TVectorResult();
                //VectorResult.Vector = null;
                //VectorResultList.Add(VectorResult);
                return (VectorResultList);

            }
        }
        catch (ex) {
            //throw;
        }
        return (VectorResultList);
    }

    //PROCESO DE BUSCAR RESULTADOS Version 2 //limite de resultados encontrados
    this.TraelistaV2 = function(Inphrase)//Componet.SeekSearch.Properties.TVectorResult
    {
        var VectorResultList = new Array();//Componet.SeekSearch.Properties.TVectorResult


        //declarar el Vector de respuesta
        //List<Componet.SeekSearch.Properties.TVector> UnVectorList = null;
        try {
            //UnVectorList = new List<Componet.SeekSearch.Properties.TVector>();
            if (Inphrase != "") {
                var resultados = 0;
                for (var x = 0; x < this.VectorList.length; x++) //recorrer la lista fija del sistema
                {
                    var fraseFija = this.VectorList[x].Phrase; //tomar la frase 
                    var valorFrase = 0; //contabilizar le valor para una frase 

                    for (var y = 0; y < this.VectorList[x].WordList.length; y++) //recorer las palabras de la lsita fija
                    {
                        var WordFija = this.VectorList[x].WordList[y].WordName; //tomar la palabra fija de la lista  
                        Inphrase = Inphrase.trim();
                        var VectorWordIn = Inphrase.split(' '); //hacer un arreglo de palabras con la frase escrita por el usuario

                        for (var z = 0; z < VectorWordIn.length; z++) //recorer palabras escritas por usuario
                        {
                            var worldUser = VectorWordIn[z]; //tomar la palabra escrita por el usuario en posicion z
                            var VectorletrasUser = worldUser.split('').join(','); //hacer un arreglo de las letras en la palabra escrita
                            var NLetrasCorrect = 0; //cuenta cuantas letras encontradas en la palabra (pArray)
                            var NLetrasWrong = 0; // cuenta cuantas letras errones en la palabra (pArray)
                            var indices = ""; //se llena con los indices de coincidencias para evitar coincidencias duplicadas ejemplo tallla                                            en tala encuentra 3 concidencias en la l, estos indices no se compararan mas de 2 veces si esa                                            letra ya fue una coincidencia 

                            for (var k = 0; k < VectorletrasUser.length; k++) //recorer letras de la palabra del usuario
                            {
                                var letraUser = VectorletrasUser[k].toString(); //tomar la letra en posision[k] de la palabra escrita por el usuario
                                var Vectorletrasfija = WordFija.split('').join(','); //hacer un arreglo de las letras en la palabra fija pArray
                                var encontro = false; //vaiable que verifica si se encontro o no una conicidencia

                                for (var j = 0; j < Vectorletrasfija.length; j++) //recorrer las letras de la palabra fija
                                {
                                    var letraFija = Vectorletrasfija[j].toString(); //tomar la letra en posision[j] de la palabra fija
                                    //validar indices
                                    //if (!(indices.Contains(j.toString()))) 
                                    if (!(indices.indexOf(j.toString()) != -1)) //validar si j no esta en los indices ya hallados, estos no se compararan
                                    {
                                        letraUser = letraUser.trim();
                                        letraFija = letraFija.trim();
                                        letraUser = letraUser.toUpper();
                                        letraFija = letraFija.toUpper();

                                        if (letraUser == letraFija) //comparar las letras de la palabra del usuario y la palabra fija
                                        {
                                            encontro = true;
                                            indices += j.toString();
                                            j = Vectorletrasfija.length; //romper el ciclo j
                                        }
                                    }
                                } //end for j

                                //comparar letras k contra letras J
                                if (encontro == true) {
                                    NLetrasCorrect++; //
                                }
                                else {
                                    NLetrasWrong++;
                                }

                                //validar sel porcentaje de coincidencia entre la palabra y las letras encontradas
                                var nltr = WordFija.length;
                                var porC = (NLetrasCorrect * 100) / nltr;
                                if (porC > 100) { //evitar que se supere el 100% 
                                    porC = 100;
                                }
                                var porX = (NLetrasWrong * 100) / nltr;
                                if (porX > 100) {
                                    porX = 100;
                                }
                                var por = porC - porX; //quitamos el porcentaje de error

                                //validar si es el ultimo ciclo k y el porcentaje supera el 39% es una posible palabra
                                if ((k == (VectorletrasUser.length - 1)) && por > 39) {
                                    //tomar el valor de la frase (valor de la plabra * porcentaje de coincidencia)
                                    valorFrase += this.VectorList[x].WordList[y].Value * por;
                                }

                                //si es el ultimo ciclo de y & hay palabras
                                if ((y == (this.VectorList[x].WordList.length - 1)) && valorFrase > 0 && (z == (VectorWordIn.length - 1)) && (k == (VectorletrasUser.length - 1))) {
                                    //guadar resultados
                                    var VectorResult = new ItHelpCenter.Componet.SeekSearch.Properties.TVectorResult();
                                    VectorResult.Vector = this.VectorList[x];
                                    VectorResult.ID = this.VectorList[x].ID;
                                    VectorResult.Phrase = this.VectorList[x].Phrase;
                                    VectorResult.Path = this.VectorList[x].Path;
                                    VectorResult.Value = valorFrase;

                                    //Componet.SeekSearch.Properties.TVector VectorFill = new Properties.TVector();
                                    //VectorFill.Phrase = fraseFija;
                                    //VectorFill.Value = valorFrase;
                                    //agregar vector a las lista de respuesta
                                    VectorResultList.push(VectorResult);
                                    //UnVectorList.Add(VectorFill);
                                    resultados = resultados + 1;
                                    if (resultados > 9) {
                                        x = VectorList.length;
                                        y = this.VectorList[x].WordList.length;
                                        z = VectorWordIn.length;
                                        k = VectorletrasUser.length;
                                    }
                                }
                            } //end for k
                        } //end for z
                    } //end for y
                } //end for x

                //validar existencia de elementos
                if (VectorResultList.length < 1) {
                    //Componet.SeekSearch.Properties.TVectorResult VectorResult = new Properties.TVectorResult();
                    //VectorResult.Vector = null;
                    //VectorResultList.Add(VectorResult);
                }
                return (VectorResultList);

            }
            else {
                //Componet.SeekSearch.Properties.TVectorResult VectorResult = new Properties.TVectorResult();
                //VectorResult.Vector = null;
                //VectorResultList.Add(VectorResult);
                return (VectorResultList);

            }
        }
        catch (ex) {
            //throw;
        }
        return (VectorResultList);
    }

    //PROCESO DE BUSCAR RESULTADOS Version 3 //Tipo LIKE
    this.TraelistaV3 = function(Inphrase)//Componet.SeekSearch.Properties.TVectorResult
    {
        var VectorResultList = new Array();//Componet.SeekSearch.Properties.TVectorResult


        //declarar el Vector de respuesta
        //List<Componet.SeekSearch.Properties.TVector> UnVectorList = null;
        try {
            //UnVectorList = new List<Componet.SeekSearch.Properties.TVector>();
            if (Inphrase != "") {
                var resultados = 0;
                for (var x = 0; x < this.VectorList.length; x++) //recorrer la lista fija del sistema
                {
                    var fraseFija = this.VectorList[x].Phrase; //tomar la frase 
                    var valorFrase = 0; //contabilizar le valor para una frase 
                    //Comparacion de la frases
                    //if ((fraseFija.toUpper()).Contains(Inphrase.ToUpper()))
                    if (((fraseFija.toUpper()).indexOf(Inphrase.toUpper()) != -1)) {
                        var VectorResult = new ItHelpCenter.Componet.SeekSearch.Properties.TVectorResult();
                        VectorResult.Vector = this.VectorList[x];
                        VectorResult.ID = this.VectorList[x].ID;
                        VectorResult.Phrase = this.VectorList[x].Phrase;
                        VectorResult.Path = this.VectorList[x].Path;
                        VectorResult.Value = valorFrase;

                        VectorResultList.push(VectorResult);
                        resultados = resultados + 1;
                        if (resultados > 9) {
                            x = VectorList.length;
                        }
                    }
                } //end for x

                //validar existencia de elementos
                if (VectorResultList.length < 1) {
                    //Componet.SeekSearch.Properties.TVectorResult VectorResult = new Properties.TVectorResult();
                    //VectorResult.Vector = null;
                    //VectorResultList.Add(VectorResult);
                }
                return (VectorResultList);

            }
            else {
                //Componet.SeekSearch.Properties.TVectorResult VectorResult = new Properties.TVectorResult();
                //VectorResult.Vector = null;
                //VectorResultList.Add(VectorResult);
                return (VectorResultList);

            }
        }
        catch (ex) {
            //throw;
        }
        return (VectorResultList);
    }


}








/*Anexo
public void InicializaDemo()
        {
            //crear un vector con la informacion fija
            string[,] ArrayPhrases = new string[8, 2];
            ArrayPhrases[0, 0] = @"I have a failing in the security system";
            ArrayPhrases[0, 1] = @"5,10,5,30,5,5,20,20";
            ArrayPhrases[1, 0] = @"There is a problem in the security system";
            ArrayPhrases[1, 1] = @"10,5,5,30,5,5,20,20";
            ArrayPhrases[2, 0] = @"I have a request in security system";
            ArrayPhrases[2, 1] = @"5,10,5,35,5,20,20";
            ArrayPhrases[3, 0] = @"I need a change in the security system";
            ArrayPhrases[3, 1] = @"5,10,5,30,5,5,20,20";
            ArrayPhrases[4, 0] = @"I have a failing in the firewall security system";
            ArrayPhrases[4, 1] = @"3,8,3,25,3,3,20,18,17";
            ArrayPhrases[5, 0] = @"There is a problem in firewall security system";
            ArrayPhrases[5, 1] = @"8,3,3,30,3,20,17,16";
            ArrayPhrases[6, 0] = @"I have a request about the firewall security system";
            ArrayPhrases[6, 1] = @"3,7,2,25,10,3,17,16,17";
            ArrayPhrases[7, 0] = @"I need a change in the firewall security system";
            ArrayPhrases[7, 1] = @"3,7,2,26,3,3,19,18,19";

            Componet.SeekSearch.Properties.TVector Vector = new Properties.TVector();
            for (int j = 0; j < 8; j++)
            {
                Vector = new Properties.TVector();
                //llena vector 
                Vector.ID = j;
                Vector.Phrase = ArrayPhrases[j, 0];

                // crear un vector con las palabras de la pharase
                String[] palabras = ArrayPhrases[j, 0].Split(' ');

                //crear un vector con los valores
                String[] valores = ArrayPhrases[j, 1].Split(',');

                //crear un TWord 
                Componet.SeekSearch.Properties.TWord Word1 = null;
                for (int i = 0; i < palabras.Length; i++)
                {
                    // llenar TWord
                    Word1 = new Properties.TWord();
                    Word1.WordName = palabras[i];
                    Word1.Value = Int32.Parse(valores[i]);
                    Vector.WordList.Add(Word1); //agregar TWord a vector
                }

                //agregar vector a las lista
                VectorList.Add(Vector);
            }
        }



*/