﻿//Autor:JJaimes
//ItHelpCenter.Componet.SeekSearch.Methods.

ItHelpCenter.Componet.SeekSearch.Methods.WordListtoStr = function (WordList) {


    var Res = Persistence.Model.Properties._Version + "(1)0";//js
    var Longitud = new SysCfg.ref("");
    var Texto = new SysCfg.ref("");
    try {
        SysCfg.Str.Protocol.WriteInt(WordList.length, Longitud, Texto);
        for (var Counter = 0; Counter < WordList.length; Counter++) {
            var UnWord = WordList[Counter];
            UnWord.ToStr(Longitud, Texto);
        }
        Longitud.Value = Longitud.Value.substring(0, Longitud.Value.length - 1);
        Res = Persistence.Model.Properties._Version + "(" + Longitud.Value + ")" + Texto.Value;
    }
    catch (e) {
        var ResErr = new SysCfg.Error.Properties.TResErr();
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e;
    }
    return Res;
}


ItHelpCenter.Componet.SeekSearch.Methods.StrtoWord = function (ProtocoloStr, WordList) {
    var Res = false;
    var Longitud, Texto;
    var Index = new SysCfg.ref(0);
    var Pos = new SysCfg.ref(0);
    try {
        WordList.length = 0;
        if (Persistence.Model.Properties._Version == ProtocoloStr.substring(0, 3)) {
            Longitud = ProtocoloStr.substring(ProtocoloStr.indexOf("(") + 1, ProtocoloStr.indexOf(")"));
            Texto = ProtocoloStr.substring(ProtocoloStr.indexOf(")") + 1, ProtocoloStr.length);
            LongitudArray = Longitud.split(",");
            LSCount = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
            for (var i = 0; i < LSCount; i++) {
                var UnWord = new Componet.SeekSearch.Properties.TWord(); //Mode new row
                UnWord.StrTo(Pos, Index, LongitudArray, Texto);
                WordList.push(UnWord);
            }
        }
    }
    catch (e) {
        var ResErr = SysCfg.Error.Properties.TResErr;
        ResErr.NotError = false;
        ResErr.Mesaje = "LIFESTATUStoStr:" + e.Message;
    }
    return (Res);

}


