﻿//ItHelpCenter.Componet.SeekSearch.Properties

ItHelpCenter.Componet.SeekSearch.Properties.TWord = function ()
{
    var WordName =  "";
    var Value =  0;
   this.ToStr = function(Longitud, Texto)
    {
        SysCfg.Str.Protocol.WriteStr(WordName, Longitud, Texto);
        SysCfg.Str.Protocol.WriteInt(Value, Longitud, Texto);
    }
    this.StrTo = function(Pos, Index, LongitudArray, Texto)
    {
        WordName = SysCfg.Str.Protocol.ReadStr(Pos, Index, LongitudArray, Texto);
        Value = SysCfg.Str.Protocol.ReadInt(Pos, Index, LongitudArray, Texto);
    }
}

 
ItHelpCenter.Componet.SeekSearch.Properties.TVector = function()
{
  
    this.ID = 0;  //IDWordDETAIL 
    this.Phrase = "";   //IDWordDETAIL 
    this.Path = "";   //IDWordDETAIL 
    this. MDCATEGORYDETAIL = new Persistence.Model.Properties.TMDCATEGORYDETAIL();
    this.WordList = new Array(); //TWord
}
   
 
ItHelpCenter.Componet.SeekSearch.Properties.TModeSeek = 
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    Phase: { value: 0, name: "Phase" },
    Like: { value: 1, name: "Like" }
}

  

ItHelpCenter.Componet.SeekSearch.Properties.TVectorResult = function()
{ 
    this.Value = 0;
    this.ID  = 0;    
    this.Phrase = "";   
    this.Path = "";
    this.Vector = new ItHelpCenter.Componet.SeekSearch.Properties.TVector();
}

 
