ItHelpCenter.Componet.ImageControls.TVclImageFormat = function (inObject, incallbackModalResult) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.Mythis = "TVclImageFormat";
    //this.Object = inObject;
    this.CallbackModalResult = incallbackModalResult;//CallbackModalResult(OutStr);        
    this._Src = false;//Ruta de imagen
    this._Crop = false;//Recortar imagen
    this._Width = 100;//Ancho de imagen por defecto
    this._Height = 100;//Ancho de imagen por defecto
    this._Image = null;//Imagen
    this._Format = "jpeg";//Formato de salida, solo jpeg o png
    this._Quality = 90;//Calidad de imagen; 
    this.Canvas = null;//Canvas creado;
    this.Name = "image";//Nombre de imagen


    /*Propiedad para establecer o obtener si se va a Recortar la imagen*/
    Object.defineProperty(this, 'Src', {
        get: function () {
            return this._Src;
        },
        set: function (inValue) {
            this._Src = inValue;
        }
    });

    /*Propiedad para establecer o obtener si se va a Recortar la imagen*/
    Object.defineProperty(this, 'Crop', {
        get: function () {
            return this._Crop;
        },
        set: function (inValue) {
            this._Crop = inValue;
        }
    });
    /*Propiedad para establecer o obtener ancho de imagen*/
    Object.defineProperty(this, 'Width', {
        get: function () {
            return this._Width;
        },
        set: function (inValue) {
            this._Width = inValue;
        }
    });
    /*Propiedad para establecer o obtener alto de imagen*/
    Object.defineProperty(this, 'Height', {
        get: function () {
            return this._Height;
        },
        set: function (inValue) {
            this._Height = inValue;
        }
    });

    /*Propiedad para establecer el formato de salida*/
    Object.defineProperty(this, 'Format', {
        get: function () {
            return this._Format;
        },
        set: function (inValue) {
            this._Format = inValue;
        }
    });

    /*Propiedad para establecer la calidad de Imgen de 0 a 100*/
    Object.defineProperty(this, 'Quality', {
        get: function () {
            return this._Quality;
        },
        set: function (inValue) {
            if (inValue >= 0 && inValue <= 100) {
                this._Quality = inValue;
            }
        }
    });


    this.OnSave = null;//Funci�n que se ejecuta al realizar el Save, OnSave(componente,dataurl) 
    this.Download = function () {

        if (this.Canvas != null) {
            //var data = this.Canvas.toDataURL();
            var data = this.Canvas.toDataURL('image/' + _this.Format, _this.Quality / 100);
            //var prev = window.location.href;
            //window.location.href = data.replace("image/png", "image/octet-stream");
            //window.location.href = prev;
            var link = document.createElement("a");
            link.href = data;
            link.download = this.Name;
            link.click();
        }

    }
    this.Save = function (content) {
        var tempHeight = _this._Height;
        var tempWidth = _this._Width;
        var canvas = document.createElement('canvas');
        canvas.width = tempWidth;
        canvas.height = tempHeight;
        var ctx = canvas.getContext('2d');
        var img = new Image();
        img.src = this.Src;
        _this._Image = img;
        img.onload = function () {

            if (!_this.Crop) {
                ctx.drawImage(_this._Image, 0, 0, tempWidth, tempHeight);
            } else {
                ctx.drawImage(_this._Image, 0, 0);
            }
            //$(content).html(canvas);	
            _this.Canvas = canvas;
            if (typeof (content) != "undefined") {
                //content.appendChild(_this._Image.This);
                $(content).html("");
                $(content).html(canvas);
            }
            if (this.OnSave != null) {
                //var dataUrl=canvas.toDataURL();
                var dataUrl = canvas.toDataURL('image/' + _this.Format, _this.Quality / 100);
                this.OnSave(_this, dataUrl);
            }
        }

    }


    /*
	// flip canvas horizontally if desired / voltee el lienzo horizontalmente si lo desea
		if (this.params.flip_horiz) {
			context.translate( params.dest_width, 0 );
			context.scale( -1, 1 );
		}

		user_callback(
				user_canvas ? null : canvas.toDataURL('image/' + params.image_format, params.jpeg_quality / 100 ),
				canvas,
				context
			);
	*/

}


ItHelpCenter.Componet.ImageControls.TVclCamera = function (inObject, incallbackModalResult) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.Mythis = "TVclCamera";
    //this.Object = inObject;
    this.CallbackModalResult = incallbackModalResult;//CallbackModalResult(OutStr);        
    this._Src = false;//Ruta de imagen
    this._Crop = false;//Recortar imagen
    this._Width = 100;//Ancho de imagen por defecto
    this._Height = 100;//Ancho de imagen por defecto
    this._Image = null;//Imagen
    this._Format = "jpeg";//Formato de salida, solo jpeg o png
    this._Quality = 90;//Calidad de imagen; 
    this.Canvas = null;//Canvas creado;
    this.Webcam = null;

    /*Funci�n para importar script*/
    this.importarScript = function (nombre, onSuccess, onError) {
        var s = document.createElement("script");
        s.onload = onSuccess;
        s.onerror = onError;
        s.src = nombre;
        document.querySelector("head").appendChild(s);
    }
    _this.importarScript(SysCfg.App.Properties.xRaiz + "Scripts/Componet/ImageFormat/library/webcam.min.js", function () {
        _this.Webcam = Webcam;
    }, function (e) {
    });

    this.InitilizeCamera = function (inDiv) {

        inDiv.id = "my_camera";

        this.Webcam.set({
            width: 320,
            height: 240,
            dest_width: 64,
            dest_height: 64,
            image_format: 'jpeg',
            jpeg_quality: 90
        });
        this.Webcam.attach('#my_camera');
    }
    this.OnTakeSnapshot = null;
    this.TakeSnapshot = function () {
        _this.Webcam.snap(function (data_uri) {
            if (_this.OnTakeSnapshot != null) {
                _this.OnTakeSnapshot(_this, data_uri);
            }
            _this.Reset();
            //_this.Webcam.reset();
        });
    }

    this.Reset = function () {
        if (this.Webcam != null) {
            this.Webcam.reset();
        }
    }


}
