ItHelpCenter.Componet.Captcha.TStyle =
    {
        GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
        StyleOperation: { value: 1, name: "StyleOperation" },
        StyleLetter: { value: 2, name: "StyleLetter" },
    }

ItHelpCenter.Componet.Captcha.TVclRecaptcha = function (inObject, incallbackModalResult) {

  this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.Mythis = "TVclRecaptcha";
	this.Object = inObject;
    this.CallbackModalResult = incallbackModalResult;//CallbackModalResult(OutStr);        
	this.Sitekey = "6Ld-tXAUAAAAAJ_ejwt6CsuZiJnY7q6UOY3NSBWL";//Key Site

    /*Funci�n para enviar captcha*/
    this.Send = function () {
        var response = grecaptcha.getResponse();//Respuesta
        if (response.length == 0) {
            return false;
        }
        return true;
    }
    /*Funci�n para inicializar componente*/
    this.Initilize = function () {
        this.Content = document.createElement("div");
        var ContentGRecaptcha = document.createElement("div");
        ContentGRecaptcha.classList.add("g-recaptcha");
        var Att_SiteKey = document.createAttribute("data-sitekey");
        Att_SiteKey.value = _this.Sitekey;
        var Att_Callback = document.createAttribute("data-callback");
        Att_Callback.value = _this.OnCallBack;
        ContentGRecaptcha.setAttributeNode(Att_SiteKey);
        inObject.appendChild(ContentGRecaptcha);
        //Antes de cargar el api, se debe crear el contenedor g-recaptcha
        _this.importarScript("https://www.google.com/recaptcha/api.js", function () {
        }, function (e) { });
    }
	/*Funci�n para importar script*/
	this.importarScript = function (nombre, onSuccess, onError) {
			var s = document.createElement("script");
			s.onload = onSuccess;
			s.onerror = onError;
			s.src = nombre;
			document.querySelector("head").appendChild(s);
	}

}

ItHelpCenter.Componet.Captcha.TVclCaptchaNative = function (inObject, incallbackModalResult) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.Mythis = "TVclCaptchaNative";
    this.Object = inObject;
    this.BackgroundColor = "grey";//#FFFFCC
    this.CallbackModalResult = incallbackModalResult;//CallbackModalResult(OutStr);
    this.Code = null;
    this.Style = ItHelpCenter.Componet.Captcha.TStyle.StyleLetter;
    this.Send = function () {
        var response = this.ValidCaptcha();//Respuesta       
        return response;
    }
    this.Initilize = function () {
        this.Content = document.createElement("div");
        inObject.appendChild(this.Content);
        this.Content.style.borderRadius = "4px";
        this.Content.style.width = "300px";
        var VclStackPanelLinea1 = new TVclStackPanel(this.Content, "1", 1, [[12], [12], [12], [12], [12]]);
        var VclStackPanelLinea2 = new TVclStackPanel(this.Content, "1", 1, [[12], [12], [12], [12], [12]]);
        
        this.Content.style.backgroundColor = this.BackgroundColor;
        this.ControlCaptcha = VclStackPanelLinea1.Column[0].This;
        this.Control = new TVclTextBox(VclStackPanelLinea2.Column[0].This, "");
        this.ControlButton = VclStackPanelLinea2.Column[0].This;
        this.Control.This.classList.add("form-control");
        this.Control.This.style.width = "initial";
        
        var ButtonRefresh = new TVclButton(this.ControlButton, "");
        ButtonRefresh.Text = "";
        ButtonRefresh.VCLType = TVCLType.BS;
        ButtonRefresh.onClick = function (e) {
            _this.RefresCode();
        }
        this.Control.This.style.float = "left";
        ButtonRefresh.This.style.float = "left";
        var IconButton = document.createElement("i");
        IconButton.classList.add("icon");
        IconButton.classList.add("ion-android-sync");
        ButtonRefresh.This.appendChild(IconButton);
        this.RefresCode();
    }
    this.RefresCode = function () {
        var cadenaCaptcha = "";
        if (this.Style == ItHelpCenter.Componet.Captcha.TStyle.StyleLetter) {
            var alpha = new Array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                '0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
            var i;
            for (i = 0; i < 6; i++) {
                var a = alpha[Math.floor(Math.random() * alpha.length)];
                var b = alpha[Math.floor(Math.random() * alpha.length)];
                var c = alpha[Math.floor(Math.random() * alpha.length)];
                var d = alpha[Math.floor(Math.random() * alpha.length)];
                var e = alpha[Math.floor(Math.random() * alpha.length)];
                var f = alpha[Math.floor(Math.random() * alpha.length)];
                var g = alpha[Math.floor(Math.random() * alpha.length)];
            }
            cadenaCaptcha = a + ' ' + b + ' ' + ' ' + c + ' ' + d + ' ' + e + ' ' + f + ' ' + g;
            this.Code = a + ' ' + b + ' ' + ' ' + c + ' ' + d + ' ' + e + ' ' + f + ' ' + g;
        } else {
            var numero1 = this.getRandomInt(0, 10);
            var numero2 = this.getRandomInt(0, 10);
            var resultado = numero1 + numero2;
            var texto = numero1 + " + " + numero2;
            if (this.getRandomInt(0, 1) == 0) {
                resultado = numero1 - numero2;
                texto = numero1 + " - " + numero2;
                if (resultado < 0) {
                    resultado = numero1 + numero2;
                    texto = numero1 + " + " + numero2;
                }
            }
            cadenaCaptcha = texto;
            this.Code = resultado;
        }

        var canvas = document.createElement("canvas");
        canvas.width = "300";
        canvas.height = "40";
        var ctx = canvas.getContext("2d");
      
        ctx.lineTo(200, 100);
        ctx.stroke();
        ctx.font = "30px Comic Sans MS";
        ctx.textAlign = "center";
        ctx.fillText(cadenaCaptcha, canvas.width / 2, 30);
        $(this.ControlCaptcha).html("");
        this.ControlCaptcha.appendChild(canvas);
    }
    this.ValidCaptcha = function () {
        if (this.Style == ItHelpCenter.Componet.Captcha.TStyle.StyleLetter) {
            var string2 = this.removeSpaces(this.Control.Text);
            return (this.removeSpaces(this.Code) == string2);
        } else if (this.Style == ItHelpCenter.Componet.Captcha.TStyle.StyleOperation) {
            return (this.removeSpaces(this.Control.Text) == "" + this.Code);
        }
        
    }
    this.removeSpaces = function (string) {
        return string.split(' ').join('');
    }
    this.getRandomInt = function (min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }
}
