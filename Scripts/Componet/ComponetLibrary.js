﻿SysCfg.Methods.include("Componet/UCode/VCL/Shared/SharedProperties.js");

SysCfg.Methods.include("Componet/UCode/UCodeMain.js");
SysCfg.Methods.include("Componet/UCode/Native/Ua/Ua.js");
SysCfg.Methods.include("Componet/UCode/Native/Ubutton/Ubutton.js");
SysCfg.Methods.include("Componet/UCode/Native/Udiv/Udiv.js");
SysCfg.Methods.include("Componet/UCode/Native/Ufooter/Ufooter.js");
SysCfg.Methods.include("Componet/UCode/Native/Uheader/Uheader.js");
SysCfg.Methods.include("Componet/UCode/Native/UHx/UHx.js");
SysCfg.Methods.include("Componet/UCode/Native/Uimg/Uimg.js");
SysCfg.Methods.include("Componet/UCode/Native/Uinput/Uinput.js");
SysCfg.Methods.include("Componet/UCode/Native/Ulabel/Ulabel.js");
SysCfg.Methods.include("Componet/UCode/Native/Uli/Uli.js");
SysCfg.Methods.include("Componet/UCode/Native/Umain/Umain.js");
SysCfg.Methods.include("Componet/UCode/Native/Uoption/Uoption.js");
SysCfg.Methods.include("Componet/UCode/Native/Up/Up.js");
SysCfg.Methods.include("Componet/UCode/Native/Uselect/Uselect.js");
SysCfg.Methods.include("Componet/UCode/Native/Uspan/Uspan.js");
SysCfg.Methods.include("Componet/UCode/Native/Utable/Utable.js");
SysCfg.Methods.include("Componet/UCode/Native/Utbody/Utbody.js");
SysCfg.Methods.include("Componet/UCode/Native/Utd/Utd.js");
SysCfg.Methods.include("Componet/UCode/Native/Utextarea/Utextarea.js");
SysCfg.Methods.include("Componet/UCode/Native/Utfoot/Utfoot.js");
SysCfg.Methods.include("Componet/UCode/Native/Uth/Uth.js");
SysCfg.Methods.include("Componet/UCode/Native/Uthead/Uthead.js");
SysCfg.Methods.include("Componet/UCode/Native/Utr/Utr.js");
SysCfg.Methods.include("Componet/UCode/Native/Uul/Uul.js");
SysCfg.Methods.include("Componet/UCode/Native/Uaside/Uaside.js");
SysCfg.Methods.include("Componet/UCode/Native/Ucenter/Ucenter.js");
SysCfg.Methods.include("Componet/UCode/Native/Unav/Unav.js");

SysCfg.Methods.include("Componet/UCode/Native/Ubutton/BS.js");
SysCfg.Methods.include("Componet/UCode/Native/Udiv/BS.js");
SysCfg.Methods.include("Componet/UCode/Native/Uinput/BS.js");
SysCfg.Methods.include("Componet/UCode/Native/Ulabel/BS.js");
SysCfg.Methods.include("Componet/UCode/Native/Utable/BS.js");
SysCfg.Methods.include("Componet/UCode/Native/Utextarea/BS.js");
SysCfg.Methods.include("Componet/UCode/Native/Uselect/BS.js");
SysCfg.Methods.include("Componet/UCode/Native/Ucanvas/Ucanvas.js");

SysCfg.Methods.include("Componet/UCode/VCL/Body/Body.js");
SysCfg.Methods.include("Scripts/Componet/BodyMain/BodyMain.js");
SysCfg.Methods.include("Componet/UCode/VCL/Button/Button.js");
SysCfg.Methods.include("Componet/UCode/VCL/CheckBox/CheckBox.js");
SysCfg.Methods.include("Componet/UCode/VCL/ComboBox/ComboBox.js");
SysCfg.Methods.include("Componet/UCode/VCL/Divitions/Divitions.js");
SysCfg.Methods.include("Componet/UCode/VCL/DropDownButton/DropDownButton.js");
SysCfg.Methods.include("Componet/UCode/VCL/GridTable/GridTable.js");
SysCfg.Methods.include("Componet/UCode/VCL/List/List.js");
SysCfg.Methods.include("Componet/UCode/VCL/Label/Label.js");
SysCfg.Methods.include("Componet/UCode/VCL/Memo/Memo.js");
SysCfg.Methods.include("Componet/UCode/VCL/TabControl/TabControl.js");
SysCfg.Methods.include("Componet/UCode/VCL/QueryView/QueryView.js");
SysCfg.Methods.include("Componet/UCode/VCL/TextBox/TextBox.js");
SysCfg.Methods.include("Componet/UCode/VCL/Imagen/Imagen.js");
SysCfg.Methods.include("Componet/UCode/VCL/Tree/Tree.js");
SysCfg.Methods.include("Componet/UCode/VCL/DropDownPanel/DropDownPanel.js");
SysCfg.Methods.include("Componet/UCode/VCL/StackPanel/StackPanel.js");
SysCfg.Methods.include("Componet/UCode/VCL/Div/Div.js");
SysCfg.Methods.include("Componet/UCode/VCL/TopBar/TopBar.js");
SysCfg.Methods.include("Componet/UCode/VCL/GridControl/GridControl.js");
SysCfg.Methods.include("Componet/UCode/VCL/RHSelectOutput/RHSelectOutput.js");
SysCfg.Methods.include("Componet/UCode/VCL/Drag/Drag.js");

SysCfg.Methods.include("Componet/UCode/VCL/TabControl/TabControl.js");
SysCfg.Methods.include("Componet/UCode/VCL/Modal/Modal.js");
SysCfg.Methods.include("Componet/UCode/VCL/RadioButton/RadioButton.js");
SysCfg.Methods.include("Componet/UCode/VCL/BarControls/BarControls.js");
SysCfg.Methods.include("Componet/UCode/VCL/NavBar/NavBar.js");
SysCfg.Methods.include("Componet/UCode/VCL/TreeGridControl/TreeGridControl.js");
SysCfg.Methods.include("Componet/UCode/VCL/OpenFile/OpenFile.js");
SysCfg.Methods.include("Componet/UCode/VCL/SaveFile/SaveFile.js");

SysCfg.Methods.include("Componet/UCode/VCL/GridControlCF/GridControlProperties.js");
SysCfg.Methods.include("Componet/UCode/VCL/GridControlCF/GridControlMethodos.js");

SysCfg.Methods.include("Componet/UCode/VCL/GridControlCF/GridControlCF.js");
SysCfg.Methods.include("Componet/UCode/VCL/GridControlCF/GridControlCFView.js");
SysCfg.Methods.include("Componet/UCode/VCL/GridControlCF/GridControlCFToolbar.js");
SysCfg.Methods.include("Scripts/Componet/NavTabControls/NavTabControls.js");
SysCfg.Methods.include("Scripts/Componet/ImageFormat/ImageFormat.js");
SysCfg.Methods.include("Scripts/Componet/Geolocation/Geolocation.js");
SysCfg.Methods.include("Scripts/Componet/Captcha/Recaptcha.js");

SysCfg.Methods.include("Componet/UCode/VCL/Timer/Timer.js");

SysCfg.Methods.include("Componet/UCode/VCL/ListBox/ListBox.js");
SysCfg.Methods.include("Componet/GraphicsPBI/GraphicsPBI.js");
SysCfg.Methods.include("Componet/PanelServices/PanelServices.js");
SysCfg.Methods.include("Componet/UCode/VCL/Modal/Modal2.js");
SysCfg.Methods.include("Componet/UCode/VCL/ExportDoc/word/filesaver.js");
SysCfg.Methods.include("Componet/UCode/VCL/ExportDoc/word/jquery.wordexport.js");
SysCfg.Methods.include("Componet/UCode/VCL/ExportDoc/pdf/jspdf.min.js");
SysCfg.Methods.include("Componet/UCode/VCL/ExportDoc/pdf/jspdf.plugin.autotable.js");
SysCfg.Methods.include("Componet/UCode/VCL/Tooltip/Tooltip.js");
SysCfg.Methods.include("Componet/UCode/VCL/Waitme/Waitme.js");

SysCfg.Methods.include("Componet/GraphicsPBI/GraphicsPBI.js");
SysCfg.Methods.include("Componet/PanelServices/PanelServices.js");

SysCfg.Methods.include("Componet/GraphicsPBI/Bar/Bar.js");
SysCfg.Methods.include("Componet/GraphicsPBI/Card/Card.js");
SysCfg.Methods.include("Componet/GraphicsPBI/Pie/Pie.js");
SysCfg.Methods.include("Componet/GraphicsPBI/TreeMap/TreeMap.js");
SysCfg.Methods.include("Componet/GraphicsPBI/Template/Template.js");
SysCfg.Methods.include("Componet/GraphicsPBI/Funnel/Funnel.js");
SysCfg.Methods.include("Componet/GraphicsPBI/Table/Table.js");
SysCfg.Methods.include("Componet/GraphicsPBI/Logo/Logo.js");

SysCfg.Methods.include("Componet/Color/Color.js");

SysCfg.Methods.include("Componet/Votes/Votes.js");
 
SysCfg.Methods.include("SD/Shared/Category/CategoryManager.js");
SysCfg.Methods.include("Componet/DxExpress/AdvSearch.js");
SysCfg.Methods.include("Componet/Nativos/Msg/AdvMsg/frAdvMsgEdit.js");
SysCfg.Methods.include("Scripts/SD/Shared/Case_CMDBCI/frSDCMDBCI.js");

//Scheduler
SysCfg.Methods.include("Componet/Scheduler/Plugin/dhtmlxscheduler.js");
//SysCfg.Methods.include("Componet/Scheduler/Plugin/ext/dhtmlxscheduler_limit.js");
SysCfg.Methods.include("Componet/Scheduler/Plugin/ext/dhtmlxscheduler_units.js");
SysCfg.Methods.include("Componet/Scheduler/Plugin/ext/dhtmlxscheduler_year_view.js");
SysCfg.Methods.include("Componet/Scheduler/Plugin/ext/dhtmlxscheduler_minical.js");
SysCfg.Methods.include("Componet/Scheduler/Plugin/ext/dhtmlxscheduler_key_nav.js");
SysCfg.Methods.include("Componet/Scheduler/Class/Scheduler.js");
SysCfg.Methods.include("Componet/Scheduler/Class/SchedulerConfig.js");
SysCfg.Methods.include("Componet/Scheduler/Class/SchedulerEvents.js");
SysCfg.Methods.include("Componet/Scheduler/Class/SchedulerInformation.js");
SysCfg.Methods.include("Componet/Scheduler/Class/SchedulerMethods.js");
SysCfg.Methods.include("Componet/Scheduler/Class/SchedulerStyle.js");
SysCfg.Methods.include("Componet/Scheduler/Class/SchedulerTemplate.js");
//Scheduler

SysCfg.Methods.include("Componet/Gantt/Gantt.js");

//
SysCfg.Methods.include("Componet/UCode/Native/Uol/Uol.js");
SysCfg.Methods.include("Componet/UCode/Native/Usection/Usection.js");
SysCfg.Methods.include("Componet/UCode/Native/Utime/Utime.js");
SysCfg.Methods.include("Componet/UCode/Native/Unav/Unav.js");
SysCfg.Methods.include("Componet/UCode/Native/Ui/Ui.js");
SysCfg.Methods.include("Componet/UCode/Native/Usmall/Usmall.js");
SysCfg.Methods.include("Componet/UCode/Native/Uhr/Uhr.js");



