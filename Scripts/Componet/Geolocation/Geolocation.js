ItHelpCenter.Componet.MapsControls.TVclGeolocation = function(inObject, incallbackModalResult){

  this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.Mythis = "TVclGeolocation";
	this.Object = inObject;
    this.CallbackModalResult = incallbackModalResult;//CallbackModalResult(OutStr);        
	this.Content=document.createElement("div");
	this._Zoom=17;
	this._SizeLocationCircle=20;
	this.locationCircle=null;
	this.Map=null;
	/*Propiedad para establecer o obtener Zoom*/
	Object.defineProperty(this, 'Zoom', {
        get: function () {
            return this._Zoom;
        },
        set: function (inValue) {
            this._Zoom = inValue;
        }
    });
	
	/*Propiedad para establecer o obtener SizeLocationCircle*/
	Object.defineProperty(this, 'SizeLocationCircle', {
        get: function () {
            return this._SizeLocationCircle;
        },
        set: function (inValue) {
            this._SizeLocationCircle = inValue;
        }
    });
    this.OnGetLocation = null;
	/*Función para obtener localizacion*/
	this.GetLocation = function () {	
			var positionMap={
			latitud:0,
			longitud:0
			}			
			new ol.Geolocation({
			projection: _this.Map.getView().getProjection(),
			tracking: true,
			trackingOptions: {
			  enableHighAccuracy: true
			}
		  }).on('change', function() {
            
              var position =this.getPosition();
              if (_this.OnGetLocation != null) {
                  
                  _this.OnGetLocation(position);
              }
			positionMap=position;
			_this.SetLocation(position[0],position[1]);
			 return positionMap;
			 
			
		  });
		 
	}
	
	/*Función para establecer localizacion*/
	this.SetLocation=function(Latitud,Longitud){
		_this.Map.getView().setCenter([Latitud,Longitud]);
			_this.locationCircle.setGeometry(
			  new ol.geom.Circle([Latitud,Longitud], _this._SizeLocationCircle)
			);			
	
	}
	
    this.Initilize = function(inCallback) {
     
        if (_this.Map == null) {
            _this.importarScript(SysCfg.App.Properties.xRaiz + "Scripts/Componet/Geolocation/library/ol.js", function() {
                _this.importarScript(SysCfg.App.Properties.xRaiz + "Scripts/Componet/Geolocation/library/polyfill.js", function() {
                    _this.importarStyle(SysCfg.App.Properties.xRaiz + "Scripts/Componet/Geolocation/library/ol.css", function() {

                        _this.Object.appendChild(_this.Content);
                        _this.Content.id = "js-map";
                        _this.locationCircle = new ol.Feature();
                        _this.Map = new ol.Map({
                            view: new ol.View({
                                zoom: _this._Zoom,
                                center: [10030840, 6731350]
                            }),
                            target: 'js-map',
                            layers: [
                                new ol.layer.Tile({
                                    source: new ol.source.OSM()
                                }),
                                new ol.layer.Vector({
                                    source: new ol.source.Vector({
                                        features: [_this.locationCircle]
                                    })
                                })
                            ]
                        });
                        inCallback(_this);

                    }, function(e) { });

                }, function(e) { });
            }, function(e) { });

        } else {
            inCallback(_this);
        }
    

		
	}
	
	
	/*Función para importar script*/
	this.importarScript = function (nombre, onSuccess, onError) {
			var s = document.createElement("script");
			s.onload = onSuccess;
			s.onerror = onError;
			s.src = nombre;
			document.querySelector("head").appendChild(s);
	}
	
    this.importarStyle = function (nombre, onSuccess, onError) {
        var style = document.createElement("link");
        style.rel = "stylesheet";
        style.type = "text/css";
        style.href = nombre;
        var s = document.head.appendChild(style);
        s.onload = onSuccess;
        s.onerror = onError;
    }
		


}

ItHelpCenter.Componet.MapsControls.TVclGeolocationChrome = function(inObject, incallbackModalResult) {

    this.TParent = function() {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.Mythis = "TVclGeolocationChrome";
    this.Object = inObject;
    this.CallbackModalResult = incallbackModalResult;//CallbackModalResult(OutStr);        
    this.Content = document.createElement("div");
    this._Zoom = 13;
    this._SizeLocationCircle = 20;
    this.locationCircle = null;
    this.Map = null;
    this.marker = null;
    /*Propiedad para establecer o obtener Zoom*/
    Object.defineProperty(this, 'Zoom', {
        get: function() {
            return this._Zoom;
        },
        set: function(inValue) {
            this._Zoom = inValue;
        }
    });

    /*Propiedad para establecer o obtener SizeLocationCircle*/
    Object.defineProperty(this, 'SizeLocationCircle', {
        get: function() {
            return this._SizeLocationCircle;
        },
        set: function(inValue) {
            this._SizeLocationCircle = inValue;
        }
    });
    this.OnGetLocation = null;
    /*Función para obtener localizacion*/
    this.GetLocation = function(inCallback) {
        navigator.geolocation.getCurrentPosition(
            function(position) {
                var coords = {
                    lng: position.coords.longitude,
                    lat: position.coords.latitude
                };
                //inCallback(position);               
                if (_this.OnGetLocation != null) {
                    _this.OnGetLocation(coords);
                }

            }, function(error) { console.log(error); });     

    }

    /*Función para establecer localizacion*/
    this.SetLocation = function() {
        this.setMapa(coords);
    }

    this.Initilize = function(inCallback) {
        _this.Object.appendChild(_this.Content);
        _this.Content.id = "js-map";
        _this.Content.style.width = "400px";
        _this.Content.style.height = "400px";
        if (_this.Map == null) {
            _this.importarScript("https://maps.googleapis.com/maps/api/js?key=AIzaSyC4IfiyYPQBMlnLY1VkbxVWXeWpLJDYNQo&callback=initMap", function() {
                _this.Map = navigator;
                var coords = {
                    lng: -79.0249472,
                    lat: -8.120729599999999
                };
                _this.setMapa(coords);  //pasamos las coordenadas al metodo para crear el mapa           
                
            }, function(e) { });

        } else {
            var coords = {
                lng: -79.04177001494139,
                lat: -8.094218076071176
            };
            _this.setMapa(coords);  //pasamos las coordenadas al metodo para crear el mapa           
            inCallback(_this);
        }
    }

    this.setMapa = function(coords) {
        //Se crea una nueva instancia del objeto mapa
        $(_this.Content).html("");
        var map = new google.maps.Map(_this.Content,
            {
                zoom: _this._Zoom,
                center: new google.maps.LatLng(coords.lat, coords.lng),
            });

        //Creamos el marcador en el mapa con sus propiedades
        //para nuestro obetivo tenemos que poner el atributo draggable en true
        //position pondremos las mismas coordenas que obtuvimos en la geolocalización
        _this.marker = new google.maps.Marker({
            map: map,
            draggable: true,
            animation: google.maps.Animation.DROP,
            position: new google.maps.LatLng(coords.lat, coords.lng),

        });
        //agregamos un evento al marcador junto con la funcion callback al igual que el evento dragend que indica 
        //cuando el usuario a soltado el marcador
        _this.marker.addListener('click', _this.toggleBounce);

        _this.marker.addListener('dragend', function(event) {
            //escribimos las coordenadas de la posicion actual del marcador dentro del input #coords
            if (_this.OnDragend != null) {
                _this.OnDragend(this.getPosition());
            }
            
        });
    }

    //callback al hacer clic en el marcador lo que hace es quitar y poner la animacion BOUNCE
    this.toggleBounce = function() {
        if (_this.marker.getAnimation() !== null) {
            _this.marker.setAnimation(null);
        } else {
            _this.marker.setAnimation(google.maps.Animation.BOUNCE);
        }
    }

    /*Función para importar script*/
    this.importarScript = function(nombre, onSuccess, onError) {
        var s = document.createElement("script");
        s.onload = onSuccess;
        s.onerror = onError;
        s.src = nombre;
        document.querySelector("head").appendChild(s);
    }

    this.importarStyle = function(nombre, onSuccess, onError) {
        var style = document.createElement("link");
        style.rel = "stylesheet";
        style.type = "text/css";
        style.href = nombre;
        var s = document.head.appendChild(style);
        s.onload = onSuccess;
        s.onerror = onError;
    }


  
}