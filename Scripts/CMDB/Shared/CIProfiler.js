﻿
UsrCfg.CMDB.TCIProfiler = function () {

    this.BLPrefix = "";
    this.CIList = new Array();
    this.CIDEFINEList = new Array();
    this.CIDEFINEEXTRATABLEList = new Array();
    this.CIDEFINEEXTRAFIELDSList = new Array();
    this.CMDBBRANDList = new Array();
    this.CMDBCIDEFINERELATIONTYPEList = new Array();
    this.CMDBCIRELATIONList = new Array();

    this._EnableControls = true;

    Object.defineProperty(this, 'EnableControls', {
        get: function () {
            return this._EnableControls;
        },
        set: function (_Value) {
            this._EnableControls = _Value;
            if (this._EnableControls == true) {
                this.inRefresch();
            }
        }
    });
    this._Index = -1;
    Object.defineProperty(this, 'Index', {
        get: function () {
            return this._Index;
        },
        set: function (_Value) {
            this._Index = _Value;
            this.inRefresch();
        }
    });

    this.ResErr;

    Object.defineProperty(this, 'SETCI', {
        get: function () {
            if (this._Index != -1)
                return this.CIList[this.Index];
            else
                return null;

        },
        set: function (Value) {
            if (this._Index != -1)
                this.CIList[this.Index] = value;
        }
    });


    this.CMDBBRAND_ListFill(this.CMDBBRANDList);
    this.CMDBCIDEFINERELATIONTYPE_ListFill(this.CMDBCIDEFINERELATIONTYPEList);


    this.Refresch = null //eventhandler;

    this.inRefresch = function () {
        if (this.EnableControls) if (this.Refresch != null) this.Refresch(this, null);
    }

  

    this.GETCI = function (inIDCI) {
        var UnCI = null;
        var AuxIndex = this.CI_ListGetIndex(inIDCI, this.CIList);
        if (AuxIndex == -1) {
            UnCI = new UsrCfg.CMDB.Properties.TCI();
            UnCI.IDCMDBCI = inIDCI;
            this.CI_CIFill(this.CIDEFINEList, this.CMDBBRANDList, UnCI);
            this.CompleteCI(UnCI);
            this.CIList.push(UnCI);
            AuxIndex = this.CIList.length - 1;
        }
        else {
            UnCI = this.CIList[AuxIndex];
            if (!UnCI.isComplete)
                this.CompleteCI(UnCI);
        }
        this.Index = AuxIndex;
    }

    this.GETCI_FRIEND = function (IDCMDBCIDEFINE) {
        var UnCI = new UsrCfg.CMDB.Properties.TCI();
        UnCI.IDCMDBCIDEFINE = IDCMDBCIDEFINE;
        this.CMDBCIRELATION_ListFillComplete(this.CIDEFINEList, UsrCfg.CMDB.Properties.TRelationType._Friend, UnCI, this.CIList, this.CMDBBRANDList, this.CMDBCIRELATIONList, this.CMDBCIDEFINERELATIONTYPEList, -1);
        var AuxIndex = -1;
        if (this.CIList.length > 0) {
            AuxIndex = 0;
            UnCI = this.CIList[AuxIndex];
            if (!UnCI.isComplete) this.CompleteCI(UnCI);
            this.Index = AuxIndex;
        }
    }

    this.CIADD = function (CI_GENERICNAME, CI_DESCRIPTION, CI_SERIALNUMBER, IDCMDBCIDEFINE, IDCMDBBRAND) {
        var CMDBCIDEFINE = this.CIDEFINE_ListSetId(this.CIDEFINEList, IDCMDBCIDEFINE);
        var UnCI = new UsrCfg.CMDB.Properties.TCI();

        UnCI.CI_DATEPLANNED = SysCfg.DB.Properties.SVRNOW();
        UnCI.CI_DATEIN = SysCfg.DB.Properties.SVRNOW();
        UnCI.CI_DATEOUT = SysCfg.DB.Properties.SVRNOW();
        UnCI.CI_GENERICNAME = CI_GENERICNAME;
        UnCI.CI_DESCRIPTION = CI_DESCRIPTION;
        UnCI.CI_PURCHASEDORRENTED = "P";
        UnCI.CI_SERIALNUMBER = CI_SERIALNUMBER;
        UnCI.IDCMDBCI = 0;
        UnCI.IDCMDBCIDEFINE = IDCMDBCIDEFINE;
        UnCI.IDCMDBBRAND = IDCMDBBRAND;
        UnCI.IDCMDBCISTATE = UsrCfg.CMDB.Properties.TCMDBCISTATE.PLANNED;
        UnCI.CMDBCIDEFINE = CMDBCIDEFINE;


        this.CI_ADD(UnCI);
        UnCI.CMDBBRAND = this.CMDBBRAND_ListSetID(this.CMDBBRANDList, UnCI.IDCMDBBRAND);
        UnCI.CMDBCIDEFINE = CMDBCIDEFINE;
        UnCI.CIDEFINE_NAME = CMDBCIDEFINE.CIDEFINE_NAME;
        UnCI.isComplete = true;
        this.CIList.push(UnCI);
        this.GETCI(UnCI.IDCMDBCI);
    }

    this.CIUPD = function (CI_GENERICNAME, CI_DESCRIPTION, CI_SERIALNUMBER, IDCMDBBRAND, CMDBCISTATE) {
        var UnCI = new UsrCfg.CMDB.Properties.TCI

        UnCI.IDCMDBCI = this.SETCI.IDCMDBCI;
        UnCI.CI_GENERICNAME = CI_GENERICNAME;
        UnCI.CI_DESCRIPTION = CI_DESCRIPTION;
        UnCI.CI_PURCHASEDORRENTED = "P";
        UnCI.CI_SERIALNUMBER = CI_SERIALNUMBER;
        UnCI.IDCMDBBRAND = IDCMDBBRAND;
        UnCI.IDCMDBCISTATE = CMDBCISTATE;

        this.CI_UPD(UnCI, this.SETCI, this.CMDBBRANDList);
        this.CIList.push(UnCI);
    }

    //Proceso
    this.CompleteCI = function (UnCI) {
        this.CMDBCIRELATION_ListFillComplete(this.CIDEFINEList, UsrCfg.CMDB.Properties.TRelationType._Children, UnCI, this.CIList, this.CMDBBRANDList, this.CMDBCIRELATIONList, this.CMDBCIDEFINERELATIONTYPEList, -1);
        this.CMDBCIRELATION_ListFillComplete(this.CIDEFINEList, UsrCfg.CMDB.Properties.TRelationType._Parent, UnCI, this.CIList, this.CMDBBRANDList, this.CMDBCIRELATIONList, this.CMDBCIDEFINERELATIONTYPEList, -1);
        this.CMDBCIRELATION_ListFillComplete(this.CIDEFINEList, UsrCfg.CMDB.Properties.TRelationType._Friend, UnCI, this.CIList, this.CMDBBRANDList, this.CMDBCIRELATIONList, this.CMDBCIDEFINERELATIONTYPEList, -1);
        UnCI.isComplete = true;
    }

    this.GETIDCMDBCIDEFINE = function (Parametro) {
        switch (typeof(Parametro)) {
            case "string":
                CIDEFINE_NAME = Parametro;
                this.CIDEFINE_ListFill(this.CIDEFINEList, this.CIDEFINEEXTRATABLEList, this.CIDEFINEEXTRAFIELDSList, Properties.TCMDBCIDEFINELEVEL._LevelNone, "", 0, CIDEFINE_NAME, Properties.TCMDBCIDEFINETYPE._GENERIC); //revisar cr11
                return (this.CIDEFINE_ListSetName(this.CIDEFINEList, CIDEFINE_NAME));
                break;
            case 'number':
                var IDCIDEFINE = Parametro;
                this.CIDEFINE_ListFill(this.CIDEFINEList, this.CIDEFINEEXTRATABLEList, this.CIDEFINEEXTRAFIELDSList, Properties.TCMDBCIDEFINELEVEL._LevelNone, "", 0, "", Properties.TCMDBCIDEFINETYPE._GENERIC); //revisar cr11
                return (this.CIDEFINE_ListSetId(this.CIDEFINEList, IDCIDEFINE));
                break;
            case "object":
                var IDCMDBCIDEFINETYPE = Parametro;
                this.CIDEFINE_ListFill(this.CIDEFINEList, this.CIDEFINEEXTRATABLEList, this.CIDEFINEEXTRAFIELDSList, Properties.TCMDBCIDEFINELEVEL._LevelNone, "", 0, "", IDCMDBCIDEFINETYPE); //revisar cr11
                return (this.CIDEFINE_ListSetIdTYPE(this.CIDEFINEList, IDCMDBCIDEFINETYPE));
                break;
        }
    }


    //this.GETIDCMDBCIDEFINE = function (CIDEFINE_NAME) {
    //    this.CIDEFINE_ListFill(this.CIDEFINEList, this.CIDEFINEEXTRATABLEList, this.CIDEFINEEXTRAFIELDSList, Properties.TCMDBCIDEFINELEVEL._LevelNone, "", 0, CIDEFINE_NAME, Properties.TCMDBCIDEFINETYPE._GENERIC); //revisar cr11
    //    return (this.CIDEFINE_ListSetName(this.CIDEFINEList, CIDEFINE_NAME));
    //}

    //this.GETIDCMDBCIDEFINE = function (IDCIDEFINE) {
    //    this.CIDEFINE_ListFill(this.CIDEFINEList, this.CIDEFINEEXTRATABLEList, this.CIDEFINEEXTRAFIELDSList, Properties.TCMDBCIDEFINELEVEL._LevelNone, "", 0, "", Properties.TCMDBCIDEFINETYPE._GENERIC); //revisar cr11
    //    return (this.CIDEFINE_ListSetId(this.CIDEFINEList, IDCIDEFINE));
    //}

    //this.GETIDCMDBCIDEFINE = function (IDCMDBCIDEFINETYPE) {
    //    this.CIDEFINE_ListFill(this.CIDEFINEList, this.CIDEFINEEXTRATABLEList, this.CIDEFINEEXTRAFIELDSList, Properties.TCMDBCIDEFINELEVEL._LevelNone, "", 0, "", IDCMDBCIDEFINETYPE); //revisar cr11
    //    return (this.CIDEFINE_ListSetIdTYPE(this.CIDEFINEList, IDCMDBCIDEFINETYPE));
    //}


    this.ROWEXTRATABLE_GETID = function (CIDEFINEEXTRATABLE, Param) {
        var ID = -1;
        try {

            var SQL = this.BuildSQL(CIDEFINEEXTRATABLE, this.SETCI, UsrCfg.CMDB.Properties.TSQLResult.Select);
            //*********** CMDBCIDEFINEEXTRATABLE_GET ***************************** 
            var DS_CMDBCIDEFINEEXTRATABLE = SysCfg.DB.SQL.Methods.OpenDataSet(SQL.StrSql, Param.ToBytes());
            if (DS_CMDBCIDEFINEEXTRATABLE.ResErr.NotError) {
                if (DS_CMDBCIDEFINEEXTRATABLE.DataSet.RecordCount > 0) {
                    DS_CMDBCIDEFINEEXTRATABLE.DataSet.First();
                    var PrefixIDField = UsrCfg.CMDB.Properties.PrefixIDField + CIDEFINEEXTRATABLE.EXTRATABLE_NAMETABLE;
                    ID = DS_CMDBCIDEFINEEXTRATABLE.DataSet.RecordSet.FieldName(PrefixIDField).asInt32();
                }
            }
        }
        finally {


        }
        return (ID);
    }


    this.ROWEXTRATABLE_ADD = function (CIDEFINEEXTRATABLE, Param) //revisar cr11
    {
        var ID = -1;
        try {
            var SQL = this.BuildSQL(CIDEFINEEXTRATABLE, this.SETCI, UsrCfg.CMDB.Properties.TSQLResult.Insert);
            //*********** CMDBCIDEFINEEXTRATABLE_ADD ***************************** 
            var DS_CMDBCIDEFINEEXTRATABLE = SysCfg.DB.SQL.Methods.OpenDataSet(SQL.StrSql, Param.ToBytes());
            if (DS_CMDBCIDEFINEEXTRATABLE.ResErr.NotError) {
                if (CIDEFINEEXTRATABLE.EXTRATABLE_LOG) {
                    ID = this.ROWEXTRATABLE_GETID(CIDEFINEEXTRATABLE, Param);
                    Param.AddDateTime(SysCfg.Interno.Properties.FieldName_Date, UsrCfg.EF.Properties.DATE_SYSLOG, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                    Param.AddInt32(SysCfg.Interno.Properties.FieldName_Source_SysLog, UsrCfg.EF.Properties.TSOURCE_SYSLOG.UNKNOWN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                    Param.AddInt32(SysCfg.Interno.Properties.FieldName_Event_SysLog, UsrCfg.EF.Properties.TEVENT_SYSLOG.INSERT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                    Param.AddInt32(SysCfg.Interno.Properties.FieldName_IDCMDBCI_SysLog, UsrCfg.EF.Properties.IDCMDBCI_SYSLOG, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                    var Execlog = SysCfg.DB.SQL.Methods.Exec(SQL.StrLog, Param.ToBytes());
                }
            }

        }
        finally {
            Param.Destroy();
        }
        return (ID);

    }
    this.ROWEXTRATABLE_UPD = function (CIDEFINEEXTRATABLE, Param) {
        try {
            Param.AddDateTime(SysCfg.Interno.Properties.FieldName_Date, UsrCfg.EF.Properties.DATE_SYSLOG, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            Param.AddInt32(SysCfg.Interno.Properties.FieldName_Source_SysLog, UsrCfg.EF.Properties.TSOURCE_SYSLOG.UNKNOWN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            Param.AddInt32(SysCfg.Interno.Properties.FieldName_Event_SysLog, UsrCfg.EF.Properties.TEVENT_SYSLOG.UPDATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            Param.AddInt32(SysCfg.Interno.Properties.FieldName_IDCMDBCI_SysLog, UsrCfg.EF.Properties.IDCMDBCI_SYSLOG, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            var SQL = this.BuildSQL(CIDEFINEEXTRATABLE, this.SETCI, UsrCfg.CMDB.Properties.TSQLResult.Update);
            //*********** CMDBCIDEFINEEXTRATABLE_UPD ***************************** 
            var Exec = SysCfg.DB.SQL.Methods.Exec(SQL.StrSql, Param.ToBytes());
            ResErr = Exec.ResErr;
            if (ResErr.NotError) {
                if (CIDEFINEEXTRATABLE.EXTRATABLE_LOG) {
                    Param.AddDateTime(SysCfg.Interno.Properties.FieldName_Date, UsrCfg.EF.Properties.DATE_SYSLOG, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                    Param.AddInt32(SysCfg.Interno.Properties.FieldName_Source_SysLog, UsrCfg.EF.Properties.TSOURCE_SYSLOG.UNKNOWN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                    Param.AddInt32(SysCfg.Interno.Properties.FieldName_Event_SysLog, UsrCfg.EF.Properties.TEVENT_SYSLOG.INSERT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                    Param.AddInt32(SysCfg.Interno.Properties.FieldName_IDCMDBCI_SysLog, UsrCfg.EF.Properties.IDCMDBCI_SYSLOG, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                    var Execlog = SysCfg.DB.SQL.Methods.Exec(SQL.StrLog, Param.ToBytes());
                }
            }
        }
        finally {
            Param.Destroy();
        }
    }


    this.ROWEXTRATABLE_DEL = function(CIDEFINEEXTRATABLE, Param)
    {
        try {
            var SQL = this.BuildSQL(CIDEFINEEXTRATABLE, this.SETCI, UsrCfg.CMDB.Properties.TSQLResult.Delete);
            //*********** CMDBCIDEFINEEXTRATABLE_DEL ***************************** 
            var Exec = SysCfg.DB.SQL.Methods.Exec(SQL.StrSql, Param.ToBytes());
            ResErr = Exec.ResErr;
            if (ResErr.NotError) {
                if (CIDEFINEEXTRATABLE.EXTRATABLE_LOG) {
                    Param.AddDateTime(SysCfg.Interno.Properties.FieldName_Date, UsrCfg.EF.Properties.DATE_SYSLOG, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                    Param.AddInt32(SysCfg.Interno.Properties.FieldName_Source_SysLog, UsrCfg.EF.Properties.TSOURCE_SYSLOG.UNKNOWN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                    Param.AddInt32(SysCfg.Interno.Properties.FieldName_Event_SysLog, UsrCfg.EF.Properties.TEVENT_SYSLOG.DELETE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                    Param.AddInt32(SysCfg.Interno.Properties.FieldName_IDCMDBCI_SysLog, UsrCfg.EF.Properties.IDCMDBCI_SYSLOG, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                    var Execlog = SysCfg.DB.SQL.Methods.Exec(SQL.StrLog, Param.ToBytes());
                }

            }
        }
        finally {
            Param.Destroy();
        }
    }


    this.CMDBCIRELATION_listDEL = function (IDCMDBCIRELATION) {


        var CMDBCIRELATION = this.CMDBCIRELATION_ListDelID(CMDBCIRELATIONList, IDCMDBCIRELATION);
        this.CMDBCIRELATION_DEL(CMDBCIRELATION);
        this.CMDBCIRELATION_ListDelID(CMDBCIRELATION.CI_CHILD.CIRELATIONListParent, IDCMDBCIRELATION);
        this.CMDBCIRELATION_ListDelID(CMDBCIRELATION.CI_PARENT.CIRELATIONListChild, IDCMDBCIRELATION);
        return (CMDBCIRELATION);
        //Int32 idx = -1;
        //idx = CI_ListGetIndex(CMDBCIRELATION.IDCMDBCI_CHILD,CIList);
        //if (idx > 0) CMDBCIRELATION_ListDelID(CIList[idx].CIRELATIONListParent, IDCMDBCIRELATION);
        //idx = CI_ListGetIndex(CMDBCIRELATION.IDCMDBCI_PARENT, CIList);
        //if (idx > 0) CMDBCIRELATION_ListDelID(CIList[idx].CIRELATIONListChild, IDCMDBCIRELATION);                       
    }

    this.CMDBCIRELATION_listADD = function (CMDBCIRELATION) {

        this.CMDBCIRELATION_ADD(CMDBCIRELATION);
        CMDBCIRELATION.CI_PARENT = this.SETCI;
        CMDBCIRELATION.IDCMDBCI_PARENT = this.SETCI.IDCMDBCI;
        var AuxIndex = this.CI_ListGetIndex(CMDBCIRELATION.IDCMDBCI_CHILD, this.CIList);
        if (AuxIndex == -1) {
            CMDBCIRELATION.CI_CHILD = new UsrCfg.CMDB.Properties.TCI();
            this.CI_CIFill(this.CIDEFINEList, this.CMDBBRANDList, CMDBCIRELATION.CI_CHILD);
            this.CIList.push(CMDBCIRELATION.CI_CHILD);
        }
        else {
            CMDBCIRELATION.CI_CHILD = this.CIList[AuxIndex];
        }
        this.CMDBCIRELATIONList.push(CMDBCIRELATION);
        this.CMDBCIRELATION_ListFillComplete(this.CIDEFINEList, UsrCfg.CMDB.Properties.TRelationType._Children, CMDBCIRELATION.CI_PARENT, this.CIList, this.CMDBBRANDList, this.CMDBCIRELATIONList, this.CMDBCIDEFINERELATIONTYPEList, CMDBCIRELATION.IDCMDBCIRELATION);
        this.CMDBCIRELATION_ListFillComplete(this.CIDEFINEList, UsrCfg.CMDB.Properties.TRelationType._Parent, CMDBCIRELATION.CI_CHILD, this.CIList, this.CMDBBRANDList, this.CMDBCIRELATIONList, this.CMDBCIDEFINERELATIONTYPEList, CMDBCIRELATION.IDCMDBCIRELATION);
        return (CMDBCIRELATION);
    }
}