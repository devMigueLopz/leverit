﻿
UsrCfg.CMDB.TCIProfiler.prototype.CI_CIFill = function (CIDEFINEList, CMDBBRANDList, CI) {
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        //*********** GET_GETID ***************************** 
        Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCI.FieldName, CI.IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddUnknown("BLPREFIX", this.BLPrefix == null ? "" : this.BLPrefix, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var DS_CMDBCI = SysCfg.DB.SQL.Methods.OpenDataSet("CMDB", "CMDBCI_GETID", Param.ToBytes());
        CI.ResErr = DS_CMDBCI.ResErr;
        if (CI.ResErr.NotError) {
            if (DS_CMDBCI.DataSet.RecordCount > 0) {
                DS_CMDBCI.DataSet.First();
                CI.CI_DATEPLANNED = DS_CMDBCI.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCI.CI_DATEPLANNED.FieldName).asDateTime();
                CI.CI_DATEIN = DS_CMDBCI.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCI.CI_DATEIN.FieldName).asDateTime();
                CI.CI_DATEOUT = DS_CMDBCI.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCI.CI_DATEOUT.FieldName).asDateTime();
                CI.CI_GENERICNAME = DS_CMDBCI.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCI.CI_GENERICNAME.FieldName).asString();
                CI.CI_DESCRIPTION = DS_CMDBCI.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCI.CI_DESCRIPTION.FieldName).asString();
                CI.IDCMDBCI = DS_CMDBCI.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCI.FieldName).asInt32();
                CI.IDCMDBCIDEFINE = DS_CMDBCI.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCIDEFINE.FieldName).asInt32();
                CI.CIDEFINE_NAME = DS_CMDBCI.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.CIDEFINE_NAME.FieldName).asString();
                CI.IDCMDBBRAND = DS_CMDBCI.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBBRAND.FieldName).asInt32();
                CI.CMDBBRAND = this.CMDBBRAND_ListSetID(CMDBBRANDList, CI.IDCMDBBRAND);
                CI.IDCMDBCISTATE = UsrCfg.CMDB.Properties.TCMDBCISTATE.GetEnum(DS_CMDBCI.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCISTATE.FieldName).asInt32());
                CI.CI_SERIALNUMBER = DS_CMDBCI.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCI.CI_SERIALNUMBER.FieldName).asString();
                CI.CI_PURCHASEDORRENTED = DS_CMDBCI.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCI.CI_PURCHASEDORRENTED.FieldName).asString();
                CI.Visible = false;

                CI.CMDBCIDEFINE = this.CIDEFINE_ListSetId(CIDEFINEList, CI.IDCMDBCIDEFINE);
            }
            else {
                DS_CMDBCI.ResErr.NotError = false;
                DS_CMDBCI.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
}
UsrCfg.CMDB.TCIProfiler.prototype.CI_ListGetIndex = function (inIDCI, CIList) {
    for (i = 0; i < CIList.length; i++) {
        if (CIList[i].IDCMDBCI == inIDCI) {
            return i;
        }
    }
    return -1;
}
UsrCfg.CMDB.TCIProfiler.prototype.CI_ListSetID = function (CIList, IDCMDBCI) {
    for (i = 0; i < CIList.length; i++) {
        if ((IDCMDBCI == CIList[i].IDCMDBCI))
            return (CIList[i]);
    }
    return null;
}
UsrCfg.CMDB.TCIProfiler.prototype.CI_ADDID = function (CI) {
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddDateTime(UsrCfg.InternoCMDBNames.CMDBCI.CI_DATEPLANNED.FieldName, CI.CI_DATEPLANNED, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoCMDBNames.CMDBCI.CI_DATEIN.FieldName, CI.CI_DATEIN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoCMDBNames.CMDBCI.CI_DATEOUT.FieldName, CI.CI_DATEOUT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoCMDBNames.CMDBCI.CI_GENERICNAME.FieldName, CI.CI_GENERICNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoCMDBNames.CMDBCI.CI_SERIALNUMBER.FieldName, CI.CI_SERIALNUMBER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCIDEFINE.FieldName, CI.IDCMDBCIDEFINE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBBRAND.FieldName, CI.IDCMDBBRAND, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCISTATE.FieldName, CI.IDCMDBCISTATE.value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddUnknown("BLPREFIX", this.BLPrefix == null ? "" : this.BLPrefix, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //*********** CMDBCI_ADD ***************************** 
        var DS_CMDBCI = SysCfg.DB.SQL.Methods.OpenDataSet("CMDB", "CMDBCI_ADDID", Param.ToBytes());
        CI.ResErr = DS_CMDBCI.ResErr;
        if (DS_CMDBCI.ResErr.NotError) {
            if (DS_CMDBCI.DataSet.RecordCount > 0) {
                DS_CMDBCI.DataSet.First();
                CI.ResErr = DS_CMDBCI.ResErr;
                CI.IDCMDBCI = DS_CMDBCI.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCI.FieldName).asInt32();
            }
            else {
                DS_CMDBCI.ResErr.NotError = false;
                DS_CMDBCI.ResErr.Mesaje = "Record Count = 0";
            }
        }

    }
    finally {
        Param.Destroy();
    }
}
UsrCfg.CMDB.TCIProfiler.prototype.CI_ADD = function (CI) {
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddDateTime(UsrCfg.InternoCMDBNames.CMDBCI.CI_DATEPLANNED.FieldName, CI.CI_DATEPLANNED, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoCMDBNames.CMDBCI.CI_DATEIN.FieldName, CI.CI_DATEIN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoCMDBNames.CMDBCI.CI_DATEOUT.FieldName, CI.CI_DATEOUT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoCMDBNames.CMDBCI.CI_GENERICNAME.FieldName, CI.CI_GENERICNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoCMDBNames.CMDBCI.CI_DESCRIPTION.FieldName, CI.CI_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoCMDBNames.CMDBCI.CI_PURCHASEDORRENTED.FieldName, CI.CI_PURCHASEDORRENTED, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoCMDBNames.CMDBCI.CI_SERIALNUMBER.FieldName, CI.CI_SERIALNUMBER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCIDEFINE.FieldName, CI.IDCMDBCIDEFINE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBBRAND.FieldName, CI.IDCMDBBRAND, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCISTATE.FieldName, CI.IDCMDBCISTATE.value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddUnknown("BLPREFIX", this.BLPrefix == null ? "" : this.BLPrefix, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //*********** CMDBCI_ADD ***************************** 
        var Exec = SysCfg.DB.SQL.Methods.Exec("CMDB", "CMDBCI_ADD", Param.ToBytes());
        CI.ResErr = Exec.ResErr;
        if (CI.ResErr.NotError) {
            CI_ADDID(CI);
        }

    }
    finally {
        Param.Destroy();
    }
}
UsrCfg.CMDB.TCIProfiler.prototype.CI_UPD = function (CI_NEW, CI_OLD, CMDBBRANDList) {
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();


        switch ( CI_NEW.IDCMDBCISTATE) {
            case UsrCfg.CMDB.Properties.TCMDBCISTATE.PLANNED:
                Param.AddUnknown(UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCISTATE.FieldName, UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCISTATE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                Param.AddUnknown(UsrCfg.InternoCMDBNames.CMDBCI.CI_DATEIN.FieldName, UsrCfg.InternoCMDBNames.CMDBCI.CI_DATEIN.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                Param.AddUnknown(UsrCfg.InternoCMDBNames.CMDBCI.CI_DATEOUT.FieldName, UsrCfg.InternoCMDBNames.CMDBCI.CI_DATEOUT.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                break;
            case UsrCfg.CMDB.Properties.TCMDBCISTATE.IN:
                if ((CI_OLD.IDCMDBCISTATE == UsrCfg.CMDB.Properties.TCMDBCISTATE.PLANNED) || (CI_OLD.IDCMDBCISTATE == UsrCfg.CMDB.Properties.TCMDBCISTATE.OUT)) {
                    CI_NEW.CI_DATEIN = SysCfg.DB.Properties.SVRNOW();
                    Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCISTATE.FieldName, CI_NEW.IDCMDBCISTATE.value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                    Param.AddDateTime(UsrCfg.InternoCMDBNames.CMDBCI.CI_DATEIN.FieldName, CI_NEW.CI_DATEIN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                    Param.AddUnknown(UsrCfg.InternoCMDBNames.CMDBCI.CI_DATEOUT.FieldName, UsrCfg.InternoCMDBNames.CMDBCI.CI_DATEOUT.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                    CI_OLD.CI_DATEIN = CI_NEW.CI_DATEIN;
                    CI_OLD.IDCMDBCISTATE = CI_NEW.IDCMDBCISTATE;
                }
                else {
                    Param.AddUnknown(UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCISTATE.FieldName, UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCISTATE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                    Param.AddUnknown(UsrCfg.InternoCMDBNames.CMDBCI.CI_DATEIN.FieldName, UsrCfg.InternoCMDBNames.CMDBCI.CI_DATEIN.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                    Param.AddUnknown(UsrCfg.InternoCMDBNames.CMDBCI.CI_DATEOUT.FieldName, UsrCfg.InternoCMDBNames.CMDBCI.CI_DATEOUT.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                }

                break;
            case UsrCfg.CMDB.Properties.TCMDBCISTATE.OUT:
                if ((CI_OLD.IDCMDBCISTATE == UsrCfg.CMDB.Properties.TCMDBCISTATE.IN) || (CI_OLD.IDCMDBCISTATE == UsrCfg.CMDB.Properties.TCMDBCISTATE.PLANNED)) {
                    CI_NEW.CI_DATEOUT = SysCfg.DB.Properties.SVRNOW();
                    Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCISTATE.FieldName, CI_NEW.IDCMDBCISTATE.value, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                    Param.AddDateTime(UsrCfg.InternoCMDBNames.CMDBCI.CI_DATEOUT.FieldName, CI_NEW.CI_DATEOUT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                    Param.AddUnknown(UsrCfg.InternoCMDBNames.CMDBCI.CI_DATEIN.FieldName, UsrCfg.InternoCMDBNames.CMDBCI.CI_DATEIN.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                    CI_OLD.CI_DATEOUT = CI_NEW.CI_DATEOUT;
                    CI_OLD.IDCMDBCISTATE = CI_NEW.IDCMDBCISTATE;

                }
                else {
                    Param.AddUnknown(UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCISTATE.FieldName, UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCISTATE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                    Param.AddUnknown(UsrCfg.InternoCMDBNames.CMDBCI.CI_DATEOUT.FieldName, UsrCfg.InternoCMDBNames.CMDBCI.CI_DATEOUT.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                    Param.AddUnknown(UsrCfg.InternoCMDBNames.CMDBCI.CI_DATEIN.FieldName, UsrCfg.InternoCMDBNames.CMDBCI.CI_DATEIN.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                }



                break;
        }
        CI_OLD.CI_DESCRIPTION = CI_NEW.CI_DESCRIPTION;
        CI_OLD.CI_PURCHASEDORRENTED = CI_NEW.CI_PURCHASEDORRENTED;
        CI_OLD.CI_SERIALNUMBER = CI_NEW.CI_SERIALNUMBER;
        CI_OLD.IDCMDBBRAND = CI_NEW.IDCMDBBRAND;
        CI_OLD.CMDBBRAND = this.CMDBBRAND_ListSetID(CMDBBRANDList, CI_OLD.IDCMDBBRAND);
        CI_OLD.CI_GENERICNAME = CI_NEW.CI_GENERICNAME;
        Param.AddString(UsrCfg.InternoCMDBNames.CMDBCI.CI_GENERICNAME.FieldName, CI_NEW.CI_GENERICNAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoCMDBNames.CMDBCI.CI_DESCRIPTION.FieldName, CI_NEW.CI_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoCMDBNames.CMDBCI.CI_PURCHASEDORRENTED.FieldName, CI_NEW.CI_PURCHASEDORRENTED, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoCMDBNames.CMDBCI.CI_SERIALNUMBER.FieldName, CI_NEW.CI_SERIALNUMBER, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBBRAND.FieldName, CI_NEW.IDCMDBBRAND, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCI.FieldName, CI_OLD.IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddUnknown("BLPREFIX", this.BLPrefix == null ? "" : this.BLPrefix, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //*********** CMDBCI_UPD ***************************** 
        var Exec = SysCfg.DB.SQL.Methods.Exec("CMDB", "CMDBCI_UPD", Param.ToBytes());

        CI_NEW.ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
}

//CMDBCIDEFINE
UsrCfg.CMDB.TCIProfiler.prototype.CIDEFINE_ListSetIdTYPE = function (CIDEFINEList, IDCMDBCIDEFINETYPE) {

    for (i = 0; i < CIDEFINEList.length; i++) {
        if (IDCMDBCIDEFINETYPE == CIDEFINEList[i].IDCMDBCIDEFINETYPE)
            return (CIDEFINEList[i]);

    }
    var CMDBCIDEFINE = new UsrCfg.CMDB.Properties.TCMDBCIDEFINE();
    CMDBCIDEFINE.IDCMDBCIDEFINETYPE = IDCMDBCIDEFINETYPE;
    return (CMDBCIDEFINE);
}
UsrCfg.CMDB.TCIProfiler.prototype.CMDBCIDEFINE_ListDelID = function (CMDBCIDEFINEList, IDCMDBCIDEFINE) {
    var CMDBCIDEFINE = null;
    var Counter = 0;
    while (Counter < CMDBCIDEFINEList.length) {
        if (IDCMDBCIDEFINE == CMDBCIDEFINEList[Counter].IDCMDBCIDEFINE) {
            CMDBCIDEFINE = CMDBCIDEFINEList[Counter];
            CMDBCIDEFINEList.splice(Counter, 1);
        }
        else {
            Counter++;
        }
    }
    return (CMDBCIDEFINE);
}
UsrCfg.CMDB.TCIProfiler.prototype.CIDEFINE_ListSetId = function (CIDEFINEList, IDCIDEFINE) {

    for (i = 0; i < CIDEFINEList.length; i++) {
        if (IDCIDEFINE == CIDEFINEList[i].IDCMDBCIDEFINE)
            return (CIDEFINEList[i]);

    }
    var CMDBCIDEFINE = new UsrCfg.CMDB.Properties.TCMDBCIDEFINE();
    CMDBCIDEFINE.IDCMDBCIDEFINE = IDCIDEFINE;
    return (CMDBCIDEFINE);
}
UsrCfg.CMDB.TCIProfiler.prototype.CIDEFINE_ListSetName = function (CIDEFINEList, CIDEFINE_NAME) {

    for (i = 0; i < CIDEFINEList.length; i++) {
        if (CIDEFINE_NAME == CIDEFINEList[i].CIDEFINE_NAME)
            return (CIDEFINEList[i]);

    }
    var CMDBCIDEFINE = new UsrCfg.CMDB.Properties.TCMDBCIDEFINE();
    CMDBCIDEFINE.CIDEFINE_NAME = CIDEFINE_NAME;
    return (CMDBCIDEFINE);
}
UsrCfg.CMDB.TCIProfiler.prototype.CIDEFINE_ListAdd = function (CIDEFINEList, CIDEFINE) {
    var i = this.CIDEFINE_ListGetIndex(CIDEFINEList, CIDEFINE);
    if (i == -1) CIDEFINEList.push(CIDEFINE);
    else CIDEFINEList[i] = CIDEFINE;
}
UsrCfg.CMDB.TCIProfiler.prototype.CIDEFINE_ListGetIndex = function (CIDEFINEList, CIDEFINE) {

    for (i = 0; i < CIDEFINEList.length; i++) {
        if (CIDEFINE.IDCMDBCIDEFINE == CIDEFINEList[i].IDCMDBCIDEFINE)
            return (i);
    }
    return (-1);
}
UsrCfg.CMDB.TCIProfiler.prototype.CIDEFINE_ListFill = function (CIDEFINEList, CIDEFINEEXTRATABLEList, CIDEFINEEXTRAFIELDSList, IDCMDBCIDEFINELEVEL, IDCMDBCIDEFINE, IDCMDBCIDEFINESUPERCLASS, CIDEFINE_NAME, IDCMDBCIDEFINETYPE) {
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        if ((IDCMDBCIDEFINETYPE == -1) || (IDCMDBCIDEFINETYPE == 0))
            Param.AddUnknown(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.IDCMDBCIDEFINETYPE.FieldName, UsrCfg.InternoCMDBNames.CMDBCIDEFINE.NAME_TABLE + "." + UsrCfg.InternoCMDBNames.CMDBCIDEFINE.IDCMDBCIDEFINETYPE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        else Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.IDCMDBCIDEFINETYPE.FieldName, IDCMDBCIDEFINETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        if (IDCMDBCIDEFINE == "") Param.AddUnknown(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.IDCMDBCIDEFINE.FieldName, "=" + UsrCfg.InternoCMDBNames.CMDBCIDEFINE.IDCMDBCIDEFINE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        else Param.AddUnknown(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.IDCMDBCIDEFINE.FieldName, IDCMDBCIDEFINE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);


        if (IDCMDBCIDEFINELEVEL == UsrCfg.CMDB.Properties.TCMDBCIDEFINELEVEL._LevelNone) Param.AddUnknown(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.IDCMDBCIDEFINELEVEL.FieldName, UsrCfg.InternoCMDBNames.CMDBCIDEFINE.IDCMDBCIDEFINELEVEL.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        else Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.IDCMDBCIDEFINELEVEL.FieldName, IDCMDBCIDEFINELEVEL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        if (IDCMDBCIDEFINESUPERCLASS < 1) Param.AddUnknown(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.IDCMDBCIDEFINESUPERCLASS.FieldName, UsrCfg.InternoCMDBNames.CMDBCIDEFINE.IDCMDBCIDEFINESUPERCLASS.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        else Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.IDCMDBCIDEFINESUPERCLASS.FieldName, IDCMDBCIDEFINESUPERCLASS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        if (CIDEFINE_NAME == "") Param.AddUnknown(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.CIDEFINE_NAME.FieldName, UsrCfg.InternoCMDBNames.CMDBCIDEFINE.CIDEFINE_NAME.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        else Param.AddString(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.CIDEFINE_NAME.FieldName, CIDEFINE_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddUnknown("BLPREFIX", this.BLPrefix == null ? "" : this.BLPrefix, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //*********** CMDBCIDEFINE_GET ***************************** 
        var DS_CMDBCIDEFINE = SysCfg.DB.SQL.Methods.OpenDataSet("CMDB", "CMDBCIDEFINE_GET", Param.ToBytes());


        if (DS_CMDBCIDEFINE.ResErr.NotError) {
            if (DS_CMDBCIDEFINE.DataSet.RecordCount > 0) {
                DS_CMDBCIDEFINE.DataSet.First();
                while (!(DS_CMDBCIDEFINE.DataSet.Eof)) {
                    var UnCIDEFINE = this.CIDEFINE_ListSetId(CIDEFINEList, DS_CMDBCIDEFINE.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.IDCMDBCIDEFINE.FieldName).asInt32());
                    UnCIDEFINE.ResErr = DS_CMDBCIDEFINE.ResErr;
                    UnCIDEFINE.CIDEFINE_INDEX = DS_CMDBCIDEFINE.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.CIDEFINE_INDEX.FieldName).asInt32();
                    UnCIDEFINE.CIDEFINE_DESCRIPTION = DS_CMDBCIDEFINE.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.CIDEFINE_DESCRIPTION.FieldName).asString();
                    UnCIDEFINE.IDCMDBCIDEFINE = DS_CMDBCIDEFINE.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.IDCMDBCIDEFINE.FieldName).asInt32();
                    UnCIDEFINE.IDCMDBCIDEFINELEVEL = DS_CMDBCIDEFINE.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.IDCMDBCIDEFINELEVEL.FieldName).asInt32(); //revisar cr11
                    UnCIDEFINE.IDCMDBCIDEFINESUPERCLASS = DS_CMDBCIDEFINE.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.IDCMDBCIDEFINESUPERCLASS.FieldName).asInt32();
                    UnCIDEFINE.CIDEFINE_NAME = DS_CMDBCIDEFINE.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.CIDEFINE_NAME.FieldName).asString();
                    UnCIDEFINE.CIDEFINE_IDFILE = DS_CMDBCIDEFINE.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.CIDEFINE_IDFILE.FieldName).asString();
                    UnCIDEFINE.CIDEFINE_FILENAME = DS_CMDBCIDEFINE.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.CIDEFINE_FILENAME.FieldName).asString();
                    //UnCIDEFINE.BYTEIMAGE = UsrCfg.SDFile.Methods.GetSDfile(Comunic.Properties.TFileType._CIDefine, Comunic.Properties.TFileOperationType._FileRead, UnCIDEFINE.CIDEFINE_IDFILE, new Array(0)); //revisar cr11
                    var GetSDfile = Comunic.AjaxJson.Client.Methods.GetSDfile(Comunic.Properties.TFileType._CIDefine, Comunic.Properties.TFileOperationType._FileRead, UnCIDEFINE.CIDEFINE_IDFILE, new Array());
                    UnCIDEFINE.BYTEIMAGE = GetSDfile.ByteFile;
                    UnCIDEFINE.IDCMDBCIDEFINETYPE = DS_CMDBCIDEFINE.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.IDCMDBCIDEFINETYPE.FieldName).asInt32();  //revisar cr11
                    this.CIDEFINE_ListAdd(CIDEFINEList, UnCIDEFINE);
                    DS_CMDBCIDEFINE.DataSet.Next();
                }
                this.CIDEFINEEXTRATABLE_ListFill(CIDEFINEList, CIDEFINEEXTRATABLEList, CIDEFINEEXTRAFIELDSList);

            }
            else {
                DS_CMDBCIDEFINE.ResErr.NotError = false;
                DS_CMDBCIDEFINE.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
}
UsrCfg.CMDB.TCIProfiler.prototype.CMDBCIDEFINE_GET = function (CMDBCIDEFINE) {
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddString(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.CIDEFINE_NAME.FieldName, CMDBCIDEFINE.CIDEFINE_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddUnknown("BLPREFIX", this.BLPrefix == null ? "" : this.BLPrefix, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //*********** CMDBCIDEFINE_GET ***************************** 
        var DS_CMDBCIDEFINE = SysCfg.DB.SQL.Methods.OpenDataSet("CMDB", "CMDBCIDEFINE_GETID", Param.ToBytes());
        CMDBCIDEFINE.ResErr = DS_CMDBCIDEFINE.ResErr;
        if (DS_CMDBCIDEFINE.ResErr.NotError) {
            if (DS_CMDBCIDEFINE.DataSet.RecordCount > 0) {
                DS_CMDBCIDEFINE.DataSet.First();
                CMDBCIDEFINE.IDCMDBCIDEFINE = DS_CMDBCIDEFINE.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.IDCMDBCIDEFINE.FieldName).asInt32();
            }
            else {
                DS_CMDBCIDEFINE.ResErr.NotError = false;
                DS_CMDBCIDEFINE.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
}
UsrCfg.CMDB.TCIProfiler.prototype.CMDBCIDEFINE_ADD = function (CMDBCIDEFINE, CMDBCIDEFINEList) {
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddText(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.CIDEFINE_DESCRIPTION.FieldName, CMDBCIDEFINE.CIDEFINE_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.CIDEFINE_FILENAME.FieldName, CMDBCIDEFINE.CIDEFINE_FILENAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.CIDEFINE_IDFILE.FieldName, CMDBCIDEFINE.CIDEFINE_IDFILE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.CIDEFINE_INDEX.FieldName, CMDBCIDEFINE.CIDEFINE_INDEX, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.CIDEFINE_NAME.FieldName, CMDBCIDEFINE.CIDEFINE_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.IDCMDBCIDEFINELEVEL.FieldName, CMDBCIDEFINE.IDCMDBCIDEFINELEVEL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal); //revisar cr11
        Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.IDCMDBCIDEFINESUPERCLASS.FieldName, CMDBCIDEFINE.IDCMDBCIDEFINESUPERCLASS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.IDCMDBCIDEFINETYPE.FieldName, CMDBCIDEFINE.IDCMDBCIDEFINETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal); //revisar cr11
        Param.AddUnknown("BLPREFIX", this.BLPrefix == null ? "" : this.BLPrefix, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //*********** CMDBCIDEFINE_ADD ***************************** 
        var Exec = SysCfg.DB.SQL.Methods.Exec("CMDB", "CMDBCIDEFINE_ADD", Param.ToBytes());
        CMDBCIDEFINE.ResErr = Exec.ResErr;
        if (Exec.ResErr.NotError) {

            this.CMDBCIDEFINE_GET(CMDBCIDEFINE);
            this.CIDEFINE_ListAdd(CMDBCIDEFINEList, CMDBCIDEFINE);
        }
    }
    finally {
        Param.Destroy();
    }
}
UsrCfg.CMDB.TCIProfiler.prototype.CMDBCIDEFINE_UPD = function (CMDBCIDEFINE) {
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddText(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.CIDEFINE_DESCRIPTION.FieldName, CMDBCIDEFINE.CIDEFINE_DESCRIPTION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.CIDEFINE_FILENAME.FieldName, CMDBCIDEFINE.CIDEFINE_FILENAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.CIDEFINE_IDFILE.FieldName, CMDBCIDEFINE.CIDEFINE_IDFILE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.CIDEFINE_INDEX.FieldName, CMDBCIDEFINE.CIDEFINE_INDEX, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.CIDEFINE_NAME.FieldName, CMDBCIDEFINE.CIDEFINE_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.IDCMDBCIDEFINE.FieldName, CMDBCIDEFINE.IDCMDBCIDEFINE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.IDCMDBCIDEFINELEVEL.FieldName, CMDBCIDEFINE.IDCMDBCIDEFINELEVEL, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.IDCMDBCIDEFINESUPERCLASS.FieldName, CMDBCIDEFINE.IDCMDBCIDEFINESUPERCLASS, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.IDCMDBCIDEFINETYPE.FieldName, CMDBCIDEFINE.IDCMDBCIDEFINETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddUnknown("BLPREFIX", this.BLPrefix == null ? "" : this.BLPrefix, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //*********** CMDBCIDEFINE_DEL ***************************** 
        var Exec = SysCfg.DB.SQL.Methods.Exec("CMDB", "CMDBCIDEFINE_UPD", Param.ToBytes());
        CMDBCIDEFINE.ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
}
UsrCfg.CMDB.TCIProfiler.prototype.CMDBCIDEFINE_DEL = function (CMDBCIDEFINE, CMDBCIDEFINEList) {
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.IDCMDBCIDEFINE.FieldName, CMDBCIDEFINE.IDCMDBCIDEFINE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddUnknown("BLPREFIX", this.BLPrefix == null ? "" : this.BLPrefix, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //*********** CMDBCIDEFINE_DEL ***************************** 
        var Exec = SysCfg.DB.SQL.Methods.Exec("CMDB", "CMDBCIDEFINE_DEL", Param.ToBytes());
        CMDBCIDEFINE.ResErr = Exec.ResErr;
        this.CMDBCIDEFINE_ListDelID(CMDBCIDEFINEList, CMDBCIDEFINE.IDCMDBCIDEFINE);
    }
    finally {
        Param.Destroy();
    }
}

//CMDBCIDEFINEEXTRATABLE
UsrCfg.CMDB.TCIProfiler.prototype.CIDEFINEEXTRATABLE_ListSetId = function (CIDEFINEEXTRATABLEList, IDCIDEFINEEXTRATABLE) {
    for (i = 0; i < CIDEFINEEXTRATABLEList.length; i++) {
        if (IDCIDEFINEEXTRATABLE == CIDEFINEEXTRATABLEList[i].IDCMDBCIDEFINEEXTRATABLE)
            return (CIDEFINEEXTRATABLEList[i]);

    }
    var CMDBCIDEFINEEXTRATABLE = new UsrCfg.CMDB.Properties.TCMDBCIDEFINEEXTRATABLE();
    CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE = IDCIDEFINEEXTRATABLE;
    return CMDBCIDEFINEEXTRATABLE;
}
UsrCfg.CMDB.TCIProfiler.prototype.CIDEFINEEXTRATABLE_ListSetName = function (CIDEFINEEXTRATABLEList, EXTRATABLE_NAME, IDCIDEFINE) {
    for (i = 0; i < CIDEFINEEXTRATABLEList.length; i++) {
        if ((IDCIDEFINE == CIDEFINEEXTRATABLEList[i].IDCMDBCIDEFINE) && (EXTRATABLE_NAME == CIDEFINEEXTRATABLEList[i].EXTRATABLE_NAME))
            return (CIDEFINEEXTRATABLEList[i]);
    }
    return null;
}
UsrCfg.CMDB.TCIProfiler.prototype.CIDEFINEEXTRATABLE_ListAdd = function (CIDEFINEEXTRATABLEList, CIDEFINEEXTRATABLE) {
    var i = this.CIDEFINEEXTRATABLE_ListGetIndex(CIDEFINEEXTRATABLEList, CIDEFINEEXTRATABLE);
    if (i == -1) CIDEFINEEXTRATABLEList.push(CIDEFINEEXTRATABLE);
    else CIDEFINEEXTRATABLEList[i] = CIDEFINEEXTRATABLE;
}

UsrCfg.CMDB.TCIProfiler.prototype.CIDEFINEEXTRATABLE_ListGetIndex = function (CIDEFINEEXTRATABLEList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = UsrCfg.CMDB.TCIProfiler.prototype.CIDEFINEEXTRATABLE_ListGetIndexIDCMDBCIDEFINEEXTRATABLE(CIDEFINEEXTRATABLEList, Param);

    }
    else {
        Res = UsrCfg.CMDB.TCIProfiler.prototype.CIDEFINEEXTRATABLE_ListGetIndexCMDBCIDEFINEEXTRATABLE(CIDEFINEEXTRATABLEList, Param);
    }
    return (Res);
}




UsrCfg.CMDB.TCIProfiler.prototype.CIDEFINEEXTRATABLE_ListGetIndexCMDBCIDEFINEEXTRATABLE = function (CIDEFINEEXTRATABLEList, CMDBCIDEFINEEXTRATABLE) {
    for (i = 0; i < CIDEFINEEXTRATABLEList.length; i++) {
        if (CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE == CIDEFINEEXTRATABLEList[i].IDCMDBCIDEFINEEXTRATABLE)
            return (i);
    }
    return (-1);
}
//New01022016
UsrCfg.CMDB.TCIProfiler.prototype.CIDEFINEEXTRATABLE_ListGetIndexIDCMDBCIDEFINEEXTRATABLE = function (CIDEFINEEXTRATABLEList, IDCMDBCIDEFINEEXTRATABLE) {
    for (var i = 0; i < CIDEFINEEXTRATABLEList.length; i++) {
        if (IDCMDBCIDEFINEEXTRATABLE == CIDEFINEEXTRATABLEList[i].IDCMDBCIDEFINEEXTRATABLE)
            return (i);

    }
    return (-1);
}
UsrCfg.CMDB.TCIProfiler.prototype.CIDEFINEEXTRATABLE_ListFill = function (CIDEFINEList, CIDEFINEEXTRATABLEList, CMDBCIDEFINEEXTRAFIELDSList) {
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddUnknown(UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRATABLE.EXTRATABLE_ENABLED.FieldName, UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRATABLE.EXTRATABLE_ENABLED.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddUnknown(UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRATABLE.EXTRATABLE_NAME.FieldName, UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRATABLE.EXTRATABLE_NAME.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddUnknown(UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRATABLE.EXTRATABLE_NAMETABLE.FieldName, UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRATABLE.EXTRATABLE_NAMETABLE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddUnknown(UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE.FieldName, UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var StrIDCMDBCIDEFINE = "";
        for (var i = 0; i < CIDEFINEList.length; i++) {
            if (StrIDCMDBCIDEFINE == "") StrIDCMDBCIDEFINE = CIDEFINEList[i].IDCMDBCIDEFINE;
            else StrIDCMDBCIDEFINE += "," + CIDEFINEList[i].IDCMDBCIDEFINE;
        }
        if (StrIDCMDBCIDEFINE == "") {
            var IDCMDBCIDEFINE = 0;
            Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINE.FieldName, IDCMDBCIDEFINE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Normal);
        }
        else {
            StrIDCMDBCIDEFINE = " IN (" + StrIDCMDBCIDEFINE + ")";
            Param.AddUnknown(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.IDCMDBCIDEFINE.FieldName, StrIDCMDBCIDEFINE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }
        Param.AddUnknown("BLPREFIX", this.BLPrefix == null ? "" : this.BLPrefix, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //*********** CMDBCIDEFINEEXTRATABLE_GET ***************************** 
        var DS_CMDBCIDEFINEEXTRATABLE = SysCfg.DB.SQL.Methods.OpenDataSet("CMDB", "CMDBCIDEFINEEXTRATABLE_GET", Param.ToBytes());
        if (DS_CMDBCIDEFINEEXTRATABLE.ResErr.NotError) {
            if (DS_CMDBCIDEFINEEXTRATABLE.DataSet.RecordCount > 0) {
                DS_CMDBCIDEFINEEXTRATABLE.DataSet.First();

                while (!(DS_CMDBCIDEFINEEXTRATABLE.DataSet.Eof)) {
                    var UnCIDEFINEEXTRATABLE = this.CIDEFINEEXTRATABLE_ListSetId(CIDEFINEEXTRATABLEList, DS_CMDBCIDEFINEEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE.FieldName).asInt32());
                    UnCIDEFINEEXTRATABLE.ResErr = DS_CMDBCIDEFINEEXTRATABLE.ResErr;
                    UnCIDEFINEEXTRATABLE.EXTRATABLE_DESCRIPTION = DS_CMDBCIDEFINEEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRATABLE.EXTRATABLE_DESCRIPTION.FieldName).asString();
                    UnCIDEFINEEXTRATABLE.EXTRATABLE_ENABLED = DS_CMDBCIDEFINEEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRATABLE.EXTRATABLE_ENABLED.FieldName).asBoolean();
                    UnCIDEFINEEXTRATABLE.EXTRATABLE_NAME = DS_CMDBCIDEFINEEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRATABLE.EXTRATABLE_NAME.FieldName).asString();
                    UnCIDEFINEEXTRATABLE.EXTRATABLE_NAMETABLE = DS_CMDBCIDEFINEEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRATABLE.EXTRATABLE_NAMETABLE.FieldName).asString();
                    UnCIDEFINEEXTRATABLE.EXTRATABLE_IDSOURCE = DS_CMDBCIDEFINEEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRATABLE.EXTRATABLE_IDSOURCE.FieldName).asInt32();  //revisar cr11                          
                    UnCIDEFINEEXTRATABLE.EXTRATABLE_LOG = DS_CMDBCIDEFINEEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRATABLE.EXTRATABLE_LOG.FieldName).asBoolean();
                    UnCIDEFINEEXTRATABLE.IDCMDBCIDEFINE = DS_CMDBCIDEFINEEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINE.FieldName).asInt32();
                    UnCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE = DS_CMDBCIDEFINEEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE.FieldName).asInt32();

                    UnCIDEFINEEXTRATABLE.CMDBCIMP_TYPE = DS_CMDBCIDEFINEEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRATABLE.IDCMDBCIIMP_TYPE.FieldName).asInt32(); //revisar cr11
                    UnCIDEFINEEXTRATABLE.USEBASELINE = DS_CMDBCIDEFINEEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRATABLE.USEBASELINE.FieldName).asBoolean();
                    UnCIDEFINEEXTRATABLE.SQL = DS_CMDBCIDEFINEEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRATABLE.SQL.FieldName).asString();

                    for (var i = 0; i < CIDEFINEList.length; i++) {
                        if (CIDEFINEList[i].IDCMDBCIDEFINE == UnCIDEFINEEXTRATABLE.IDCMDBCIDEFINE) {
                            this.CIDEFINEEXTRATABLE_ListAdd(CIDEFINEList[i].CMDBCIDEFINEEXTRATABLElist, UnCIDEFINEEXTRATABLE);
                            break;
                        }
                    }
                    this.CIDEFINEEXTRATABLE_ListAdd(CIDEFINEEXTRATABLEList, UnCIDEFINEEXTRATABLE);
                    DS_CMDBCIDEFINEEXTRATABLE.DataSet.Next();
                }
                this.CIDEFINEEXTRAFIELDS_ListFill(CIDEFINEEXTRATABLEList, CMDBCIDEFINEEXTRAFIELDSList);
            }
            else {
                DS_CMDBCIDEFINEEXTRATABLE.ResErr.NotError = false;
                DS_CMDBCIDEFINEEXTRATABLE.ResErr.Mesaje = "Record Count = 0";
            }
        }

    }
    finally {
        Param.Destroy();
    }
}

//CMDBCIDEFINEEXTRAFIELDS
UsrCfg.CMDB.TCIProfiler.prototype.CIDEFINEEXTRAFIELDS_ListSetID = function (CIDEFINEEXTRAFIELDSList, IDCIDEFINEEXTRAFIELDS) {
    for (var i = 0; i < CIDEFINEEXTRAFIELDSList.length; i++) {
        if (IDCIDEFINEEXTRAFIELDS == CIDEFINEEXTRAFIELDSList[i].IDCMDBCIDEFINEEXTRAFIELDS)
            return (CIDEFINEEXTRAFIELDSList[i]);
    }
    var CMDBCIDEFINEEXTRAFIELDS = new UsrCfg.CMDB.Properties.TCMDBCIDEFINEEXTRAFIELDS();
    CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS = IDCIDEFINEEXTRAFIELDS;
    return CMDBCIDEFINEEXTRAFIELDS;
}




UsrCfg.CMDB.TCIProfiler.prototype.CIDEFINEEXTRAFIELDS_ListAdd = function (CIDEFINEEXTRAFIELDSList, CIDEFINEEXTRAFIELDS) {
    var i = this.CIDEFINEEXTRAFIELDS_ListGetIndex(CIDEFINEEXTRAFIELDSList, CIDEFINEEXTRAFIELDS);
    if (i == -1) CIDEFINEEXTRAFIELDSList.push(CIDEFINEEXTRAFIELDS);
    else CIDEFINEEXTRAFIELDSList[i] = CIDEFINEEXTRAFIELDS;
}


UsrCfg.CMDB.TCIProfiler.prototype.CIDEFINEEXTRAFIELDS_ListGetIndex = function (CIDEFINEEXTRAFIELDSList, Param) {
    var Res = -1
    if (typeof (Param) == 'number') {
        Res = UsrCfg.CMDB.TCIProfiler.prototype.CIDEFINEEXTRAFIELDS_ListGetIndexIDCMDBCIDEFINEEXTRAFIELDS(CIDEFINEEXTRAFIELDSList, Param);

    }
    else {
        Res = UsrCfg.CMDB.TCIProfiler.prototype.CIDEFINEEXTRAFIELDS_ListGetIndexCIDEFINEEXTRAFIELDS(CIDEFINEEXTRAFIELDSList, Param);
    }
    return (Res);
}


UsrCfg.CMDB.TCIProfiler.prototype.CIDEFINEEXTRAFIELDS_ListGetIndexCIDEFINEEXTRAFIELDS = function (CIDEFINEEXTRAFIELDSList, CIDEFINEEXTRAFIELDS) {
    for (var i = 0; i < CIDEFINEEXTRAFIELDSList.length; i++) {
        if (CIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS == CIDEFINEEXTRAFIELDSList[i].IDCMDBCIDEFINEEXTRAFIELDS)
            return (i);
    }
    return (-1);
}
//New01022016
UsrCfg.CMDB.TCIProfiler.prototype.CIDEFINEEXTRAFIELDS_ListGetIndexIDCMDBCIDEFINEEXTRAFIELDS = function (CIDEFINEEXTRAFIELDSList, IDCMDBCIDEFINEEXTRAFIELDS) {
    for (var i = 0; i < CIDEFINEEXTRAFIELDSList.length; i++) {
        if (IDCMDBCIDEFINEEXTRAFIELDS == CIDEFINEEXTRAFIELDSList[i].IDCMDBCIDEFINEEXTRAFIELDS)
            return (i);
    }
    return (-1);
}
UsrCfg.CMDB.TCIProfiler.prototype.CIDEFINEEXTRAFIELDS_ListFill = function (CIDEFINEEXTRATABLEList, CIDEFINEEXTRAFIELDSList) {
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddUnknown(UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_NAME.FieldName, UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_NAME.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddUnknown(UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINE.FieldName, UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddUnknown(UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS.FieldName, UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var StrIDCMDBCIDEFINEEXTRATABLE = "";
        for (var i = 0; i < CIDEFINEEXTRATABLEList.length; i++) {
            if (StrIDCMDBCIDEFINEEXTRATABLE == "") StrIDCMDBCIDEFINEEXTRATABLE = CIDEFINEEXTRATABLEList[i].IDCMDBCIDEFINEEXTRATABLE;
            else StrIDCMDBCIDEFINEEXTRATABLE += "," + CIDEFINEEXTRATABLEList[i].IDCMDBCIDEFINEEXTRATABLE;
        }
        if (StrIDCMDBCIDEFINEEXTRATABLE == "") {
            var IDCMDBCIDEFINEEXTRATABLE = 0;
            Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRATABLE.FieldName, IDCMDBCIDEFINEEXTRATABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Normal);
        }
        else {
            StrIDCMDBCIDEFINEEXTRATABLE = " IN (" + StrIDCMDBCIDEFINEEXTRATABLE + ")";
            Param.AddUnknown(UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRATABLE.FieldName, StrIDCMDBCIDEFINEEXTRATABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }
        Param.AddUnknown("BLPREFIX", this.BLPrefix == null ? "" : this.BLPrefix, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //*********** CMDBCIDEFINEEXTRAFIELDS_GET ***************************** 
        var DS_CMDBCIDEFINEEXTRAFIELDS = SysCfg.DB.SQL.Methods.OpenDataSet("CMDB", "CMDBCIDEFINEEXTRAFIELDS_GET", Param.ToBytes());
        if (DS_CMDBCIDEFINEEXTRAFIELDS.ResErr.NotError) {
            if (DS_CMDBCIDEFINEEXTRAFIELDS.DataSet.RecordCount > 0) {
                DS_CMDBCIDEFINEEXTRAFIELDS.DataSet.First();
                while (!(DS_CMDBCIDEFINEEXTRAFIELDS.DataSet.Eof)) {
                    var UnCIDEFINEEXTRAFIELDS = this.CIDEFINEEXTRAFIELDS_ListSetID(CIDEFINEEXTRAFIELDSList, DS_CMDBCIDEFINEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS.FieldName).asInt32());//new Properties.TCMDBCIDEFINEEXTRAFIELDS();
                    UnCIDEFINEEXTRAFIELDS.ResErr = DS_CMDBCIDEFINEEXTRAFIELDS.ResErr;
                    UnCIDEFINEEXTRAFIELDS.DBDATATYPES_SIZE = DS_CMDBCIDEFINEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRAFIELDS.DBDATATYPES_SIZE.FieldName).asInt32();
                    UnCIDEFINEEXTRAFIELDS.DBKEYTYPE_NAME = DS_CMDBCIDEFINEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRAFIELDS.DBKEYTYPE_NAME.FieldName).asString();
                    UnCIDEFINEEXTRAFIELDS.EXTRAFIELDS_DESCRIPTION = DS_CMDBCIDEFINEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_DESCRIPTION.FieldName).asString();
                    UnCIDEFINEEXTRAFIELDS.EXTRAFIELDS_LOOKUPDISPLAY = DS_CMDBCIDEFINEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_LOOKUPDISPLAY.FieldName).asString();
                    UnCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE = DS_CMDBCIDEFINEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE.FieldName).asInt32();
                    UnCIDEFINEEXTRAFIELDS.EXTRAFIELDS_LOOKUPID = DS_CMDBCIDEFINEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_LOOKUPID.FieldName).asString();
                    UnCIDEFINEEXTRAFIELDS.EXTRAFIELDS_LOOKUPSQL = DS_CMDBCIDEFINEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_LOOKUPSQL.FieldName).asString();
                    UnCIDEFINEEXTRAFIELDS.EXTRAFIELDS_NAME = DS_CMDBCIDEFINEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_NAME.FieldName).asString();
                    UnCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINE = DS_CMDBCIDEFINEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINE.FieldName).asInt32();
                    UnCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS = DS_CMDBCIDEFINEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS.FieldName).asInt32();
                    UnCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRATABLE = DS_CMDBCIDEFINEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRATABLE.FieldName).asInt32();
                    UnCIDEFINEEXTRAFIELDS.IDCMDBDBDATATYPES = DS_CMDBCIDEFINEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRAFIELDS.IDCMDBDBDATATYPES.FieldName).asInt32(); //revisar cr11
                    UnCIDEFINEEXTRAFIELDS.IDCMDBKEYTYPE = DS_CMDBCIDEFINEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINEEXTRAFIELDS.IDCMDBKEYTYPE.FieldName).asInt32(); //revisar cr11

                    for (var i = 0; i < CIDEFINEEXTRATABLEList.length; i++) {
                        if (CIDEFINEEXTRATABLEList[i].IDCMDBCIDEFINEEXTRATABLE == UnCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRATABLE) {
                            if (!CIDEFINEEXTRATABLEList[i].GRIDENABLE) CIDEFINEEXTRATABLEList[i].GRIDENABLE = (UnCIDEFINEEXTRAFIELDS.IDCMDBKEYTYPE == SysCfg.DB.Properties.TKeyType.ADN);
                            this.CIDEFINEEXTRAFIELDS_ListAdd(CIDEFINEEXTRATABLEList[i].CMDBCIDEFINEEXTRAFIELDSList, UnCIDEFINEEXTRAFIELDS);
                            break;
                        }
                    }
                    this.CIDEFINEEXTRAFIELDS_ListAdd(CIDEFINEEXTRAFIELDSList, UnCIDEFINEEXTRAFIELDS);
                    DS_CMDBCIDEFINEEXTRAFIELDS.DataSet.Next();
                }

            }
            else {
                DS_CMDBCIDEFINEEXTRAFIELDS.ResErr.NotError = false;
                DS_CMDBCIDEFINEEXTRAFIELDS.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
}

//CMDBBRAND
UsrCfg.CMDB.TCIProfiler.prototype.CMDBBRAND_ListSetID = function (CMDBBRANDList, IDCMDBBRAND) {
    for (var i = 0; i < CMDBBRANDList.length; i++) {
        if (IDCMDBBRAND == CMDBBRANDList[i].IDCMDBBRAND)
            return (CMDBBRANDList[i]);
    }
    var CMDBBRAND = new UsrCfg.CMDB.Properties.TCMDBBRAND();
    CMDBBRAND.IDCMDBBRAND = IDCMDBBRAND;
    return CMDBBRAND;
}
UsrCfg.CMDB.TCIProfiler.prototype.CMDBBRAND_ListAdd = function (CMDBBRANDList, CMDBBRAND) {
    var i = this.CMDBBRAND_ListGetIndex(CMDBBRANDList, CMDBBRAND);
    if (i == -1) CMDBBRANDList.push(CMDBBRAND);
    else CMDBBRANDList[i] = CMDBBRAND;
}
UsrCfg.CMDB.TCIProfiler.prototype.CMDBBRAND_ListGetIndex = function (CMDBBRANDList, CMDBBRAND) {
    for (var i = 0; i < CMDBBRANDList.length; i++) {
        if (CMDBBRAND.IDCMDBBRAND == CMDBBRANDList[i].IDCMDBBRAND)
            return (i);
    }
    return (-1);
}
UsrCfg.CMDB.TCIProfiler.prototype.CMDBBRAND_ListFill = function (CMDBBRANDList) {
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        var DS_CMDBBRAND = SysCfg.DB.SQL.Methods.OpenDataSet("CMDB", "CMDBBRAND_GET", Param.ToBytes());
        if (DS_CMDBBRAND.ResErr.NotError) {
            if (DS_CMDBBRAND.DataSet.RecordCount > 0) {
                DS_CMDBBRAND.DataSet.First();
                while (!(DS_CMDBBRAND.DataSet.Eof)) {
                    var UnCMDBBRAND = this.CMDBBRAND_ListSetID(CMDBBRANDList, DS_CMDBBRAND.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBBRAND.IDCMDBBRAND.FieldName).asInt32());
                    UnCMDBBRAND.ResErr = DS_CMDBBRAND.ResErr;
                    UnCMDBBRAND.BRAND_DESCRIPTION = DS_CMDBBRAND.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBBRAND.BRAND_DESCRIPTION.FieldName).asString();
                    UnCMDBBRAND.BRAND_INACTIVE = DS_CMDBBRAND.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBBRAND.BRAND_INACTIVE.FieldName).asBoolean();
                    UnCMDBBRAND.BRAND_NAME = DS_CMDBBRAND.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBBRAND.BRAND_NAME.FieldName).asString();
                    UnCMDBBRAND.IDCMDBBRAND = DS_CMDBBRAND.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBBRAND.IDCMDBBRAND.FieldName).asInt32();
                    this.CMDBBRAND_ListAdd(CMDBBRANDList, UnCMDBBRAND);
                    DS_CMDBBRAND.DataSet.Next();
                }

            }
            else {
                DS_CMDBBRAND.ResErr.NotError = false;
                DS_CMDBBRAND.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
}

//CMDBCIDEFINERELATIONTYPE  
UsrCfg.CMDB.TCIProfiler.prototype.CMDBCIDEFINERELATIONTYPE_ListSetID = function (CMDBCIDEFINERELATIONTYPEList, IDCMDBCIDEFINERELATIONTYPE) {
    for (var i = 0; i < CMDBCIDEFINERELATIONTYPEList.length; i++) {
        if (IDCMDBCIDEFINERELATIONTYPE == CMDBCIDEFINERELATIONTYPEList[i].IDCMDBCIDEFINERELATIONTYPE)
            return (CMDBCIDEFINERELATIONTYPEList[i]);

    }
    var CMDBCIDEFINERELATIONTYPE = new UsrCfg.CMDB.Properties.TCMDBCIDEFINERELATIONTYPE();
    CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINERELATIONTYPE = IDCMDBCIDEFINERELATIONTYPE;
    return CMDBCIDEFINERELATIONTYPE;
}
UsrCfg.CMDB.TCIProfiler.prototype.CMDBCIDEFINERELATIONTYPE_ListDelID = function (CMDBCIDEFINERELATIONTYPEList, IDCMDBCIDEFINERELATIONTYPE) {
    var Res = false;
    for (var i = 0; i < CMDBCIDEFINERELATIONTYPEList.length; i++) {
        if (IDCMDBCIDEFINERELATIONTYPE == CMDBCIDEFINERELATIONTYPEList[i].IDCMDBCIDEFINERELATIONTYPE) {
            CMDBCIDEFINERELATIONTYPEList.splice(i, 1);
            Res = true;
            break;
        }
    }
    return (Res);
}

//CMDBCIDEFINERELATIONTYPE
UsrCfg.CMDB.TCIProfiler.prototype.CMDBCIDEFINERELATIONTYPE_ListAdd = function (CMDBCIDEFINERELATIONTYPEList, CMDBCIDEFINERELATIONTYPE) {
    var i = this.CMDBCIDEFINERELATIONTYPE_ListGetIndex(CMDBCIDEFINERELATIONTYPEList, CMDBCIDEFINERELATIONTYPE);
    if (i == -1) CMDBCIDEFINERELATIONTYPEList.push(CMDBCIDEFINERELATIONTYPE);
    else CMDBCIDEFINERELATIONTYPEList[i] = CMDBCIDEFINERELATIONTYPE;
}
UsrCfg.CMDB.TCIProfiler.prototype.CMDBCIDEFINERELATIONTYPE_ListGetIndex = function (CMDBCIDEFINERELATIONTYPEList, CMDBCIDEFINERELATIONTYPE) {
    for (var i = 0; i < CMDBCIDEFINERELATIONTYPEList.length; i++) {
        if (CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINERELATIONTYPE == CMDBCIDEFINERELATIONTYPEList[i].IDCMDBCIDEFINERELATIONTYPE)
            return (i);
    }
    return (-1);
}
UsrCfg.CMDB.TCIProfiler.prototype.CMDBCIDEFINERELATIONTYPE_ListFill = function (CMDBCIDEFINERELATIONTYPEList, STRIDCMDBCIDEFINE_CHILD, STRIDCMDBCIDEFINE_PARENT) {
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddUnknown("IDCMDBCIDEFINE_CHILD", STRIDCMDBCIDEFINE_CHILD, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddUnknown("IDCMDBCIDEFINE_PARENT", STRIDCMDBCIDEFINE_PARENT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddUnknown("BLPREFIX", this.BLPrefix == null ? "" : this.BLPrefix, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        var DS_CMDBCIDEFINERELATIONTYPE = SysCfg.DB.SQL.Methods.OpenDataSet("CMDB", "CMDBCIDEFINERELATIONTYPE_GET2", Param.ToBytes());
        if (DS_CMDBCIDEFINERELATIONTYPE.ResErr.NotError) {
            if (DS_CMDBCIDEFINERELATIONTYPE.DataSet.RecordCount > 0) {
                DS_CMDBCIDEFINERELATIONTYPE.DataSet.First();
                while (!(DS_CMDBCIDEFINERELATIONTYPE.DataSet.Eof)) {
                    var UnCMDBCIDEFINERELATIONTYPE = this.CMDBCIDEFINERELATIONTYPE_ListSetID(CMDBCIDEFINERELATIONTYPEList, DS_CMDBCIDEFINERELATIONTYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINERELATIONTYPE.FieldName).asInt32());//new Properties.TCMDBCIDEFINERELATIONTYPE();
                    UnCMDBCIDEFINERELATIONTYPE.ResErr = DS_CMDBCIDEFINERELATIONTYPE.ResErr;
                    UnCMDBCIDEFINERELATIONTYPE.CIDEFINERELATIONTYPE_NAME = DS_CMDBCIDEFINERELATIONTYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINERELATIONTYPE.CIDEFINERELATIONTYPE_NAME.FieldName).asString();
                    UnCMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINE_CHILD = DS_CMDBCIDEFINERELATIONTYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINE_CHILD.FieldName).asInt32();
                    UnCMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINE_PARENT = DS_CMDBCIDEFINERELATIONTYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINE_PARENT.FieldName).asInt32();
                    UnCMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINERELATIONTYPE = DS_CMDBCIDEFINERELATIONTYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINERELATIONTYPE.FieldName).asInt32();
                    UnCMDBCIDEFINERELATIONTYPE.IDCMDBRELATIONTYPE = DS_CMDBCIDEFINERELATIONTYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINERELATIONTYPE.IDCMDBRELATIONTYPE.FieldName).asInt32(); //revisar cr11
                    this.CMDBCIDEFINERELATIONTYPE_ListAdd(CMDBCIDEFINERELATIONTYPEList, UnCMDBCIDEFINERELATIONTYPE);
                    DS_CMDBCIDEFINERELATIONTYPE.DataSet.Next();
                }

            }
            else {
                DS_CMDBCIDEFINERELATIONTYPE.ResErr.NotError = false;
                DS_CMDBCIDEFINERELATIONTYPE.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
}
UsrCfg.CMDB.TCIProfiler.prototype.CMDBCIDEFINERELATIONTYPE_ListFill = function (CMDBCIDEFINERELATIONTYPEList) {
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddUnknown("BLPREFIX", this.BLPrefix == null ? "" : this.BLPrefix, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        var DS_CMDBCIDEFINERELATIONTYPE = SysCfg.DB.SQL.Methods.OpenDataSet("CMDB", "CMDBCIDEFINERELATIONTYPE_GET", Param.ToBytes());
        if (DS_CMDBCIDEFINERELATIONTYPE.ResErr.NotError) {
            if (DS_CMDBCIDEFINERELATIONTYPE.DataSet.RecordCount > 0) {
                DS_CMDBCIDEFINERELATIONTYPE.DataSet.First();
                while (!(DS_CMDBCIDEFINERELATIONTYPE.DataSet.Eof)) {
                    var UnCMDBCIDEFINERELATIONTYPE = this.CMDBCIDEFINERELATIONTYPE_ListSetID(CMDBCIDEFINERELATIONTYPEList, DS_CMDBCIDEFINERELATIONTYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINERELATIONTYPE.FieldName).asInt32());
                    UnCMDBCIDEFINERELATIONTYPE.ResErr = DS_CMDBCIDEFINERELATIONTYPE.ResErr;
                    UnCMDBCIDEFINERELATIONTYPE.CIDEFINERELATIONTYPE_NAME = DS_CMDBCIDEFINERELATIONTYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINERELATIONTYPE.CIDEFINERELATIONTYPE_NAME.FieldName).asString();
                    UnCMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINE_CHILD = DS_CMDBCIDEFINERELATIONTYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINE_CHILD.FieldName).asInt32();
                    UnCMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINE_PARENT = DS_CMDBCIDEFINERELATIONTYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINE_PARENT.FieldName).asInt32();
                    UnCMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINERELATIONTYPE = DS_CMDBCIDEFINERELATIONTYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINERELATIONTYPE.FieldName).asInt32();
                    UnCMDBCIDEFINERELATIONTYPE.IDCMDBRELATIONTYPE = DS_CMDBCIDEFINERELATIONTYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINERELATIONTYPE.IDCMDBRELATIONTYPE.FieldName).asInt32();
                    this.CMDBCIDEFINERELATIONTYPE_ListAdd(CMDBCIDEFINERELATIONTYPEList, UnCMDBCIDEFINERELATIONTYPE);
                    DS_CMDBCIDEFINERELATIONTYPE.DataSet.Next();
                }

            }
            else {
                DS_CMDBCIDEFINERELATIONTYPE.ResErr.NotError = false;
                DS_CMDBCIDEFINERELATIONTYPE.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
}
UsrCfg.CMDB.TCIProfiler.prototype.CMDBCIDEFINERELATIONTYPE_GET = function (CMDBCIDEFINERELATIONTYPE) {
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINE_CHILD.FieldName, CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINE_CHILD, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINE_PARENT.FieldName, CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINE_PARENT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCIDEFINERELATIONTYPE.IDCMDBRELATIONTYPE.FieldName, CMDBCIDEFINERELATIONTYPE.IDCMDBRELATIONTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddUnknown("BLPREFIX", this.BLPrefix == null ? "" : this.BLPrefix, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        //*********** CMDBCIDEFINERELATIONTYPE_GET ***************************** 
        var DS_CMDBCIDEFINERELATIONTYPE = SysCfg.DB.SQL.Methods.OpenDataSet("CMDB", "CMDBCIDEFINERELATIONTYPE_GETID", Param.ToBytes());
        CMDBCIDEFINERELATIONTYPE.ResErr = DS_CMDBCIDEFINERELATIONTYPE.ResErr;
        if (DS_CMDBCIDEFINERELATIONTYPE.ResErr.NotError) {
            if (DS_CMDBCIDEFINERELATIONTYPE.DataSet.RecordCount > 0) {
                DS_CMDBCIDEFINERELATIONTYPE.DataSet.First();
                CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINERELATIONTYPE = DS_CMDBCIDEFINERELATIONTYPE.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINERELATIONTYPE.FieldName).asInt32();

            }
            else {
                DS_CMDBCIDEFINERELATIONTYPE.ResErr.NotError = false;
                DS_CMDBCIDEFINERELATIONTYPE.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
}
UsrCfg.CMDB.TCIProfiler.prototype.CMDBCIDEFINERELATIONTYPE_ADD = function (CMDBCIDEFINERELATIONTYPE, CMDBCIDEFINERELATIONTYPEList) {
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINE_CHILD.FieldName, CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINE_CHILD, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINE_PARENT.FieldName, CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINE_PARENT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCIDEFINERELATIONTYPE.IDCMDBRELATIONTYPE.FieldName, CMDBCIDEFINERELATIONTYPE.IDCMDBRELATIONTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoCMDBNames.CMDBCIDEFINERELATIONTYPE.CIDEFINERELATIONTYPE_NAME.FieldName, CMDBCIDEFINERELATIONTYPE.CIDEFINERELATIONTYPE_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddUnknown("BLPREFIX", this.BLPrefix == null ? "" : this.BLPrefix, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        //*********** CMDBCIDEFINERELATIONTYPE_ADD ***************************** 
        var Exec = SysCfg.DB.SQL.Methods.Exec("CMDB", "CMDBCIDEFINERELATIONTYPE_ADD", Param.ToBytes());
        CMDBCIDEFINERELATIONTYPE.ResErr = Exec.ResErr;
        if (Exec.ResErr.NotError) {
            this.CMDBCIDEFINERELATIONTYPE_GET(CMDBCIDEFINERELATIONTYPE);
            this.CMDBCIDEFINERELATIONTYPE_ListAdd(CMDBCIDEFINERELATIONTYPEList, CMDBCIDEFINERELATIONTYPE);

        }
    }
    finally {
        Param.Destroy();
    }
}
UsrCfg.CMDB.TCIProfiler.prototype.CMDBCIDEFINERELATIONTYPE_UPD = function (CMDBCIDEFINERELATIONTYPE) {
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINERELATIONTYPE.FieldName, CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINERELATIONTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINE_CHILD.FieldName, CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINE_CHILD, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINE_PARENT.FieldName, CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINE_PARENT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCIDEFINERELATIONTYPE.IDCMDBRELATIONTYPE.FieldName, CMDBCIDEFINERELATIONTYPE.IDCMDBRELATIONTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoCMDBNames.CMDBCIDEFINERELATIONTYPE.CIDEFINERELATIONTYPE_NAME.FieldName, CMDBCIDEFINERELATIONTYPE.CIDEFINERELATIONTYPE_NAME, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddUnknown("BLPREFIX", this.BLPrefix == null ? "" : this.BLPrefix, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //*********** CMDBCIDEFINERELATIONTYPE_DEL ***************************** 
        var Exec = SysCfg.DB.SQL.Methods.Exec("CMDB", "CMDBCIDEFINERELATIONTYPE_UPD", Param.ToBytes());
        CMDBCIDEFINERELATIONTYPE.ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
}
UsrCfg.CMDB.TCIProfiler.prototype.CMDBCIDEFINERELATIONTYPE_DEL = function (CMDBCIDEFINERELATIONTYPE, CMDBCIDEFINERELATIONTYPEList) {
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINERELATIONTYPE.FieldName, CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINERELATIONTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddUnknown("BLPREFIX", this.BLPrefix == null ? "" : this.BLPrefix, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //*********** CMDBCIDEFINERELATIONTYPE_DEL ***************************** 
        var Exec = SysCfg.DB.SQL.Methods.Exec("CMDB", "CMDBCIDEFINERELATIONTYPE_DEL", Param.ToBytes());
        CMDBCIDEFINERELATIONTYPE.ResErr = Exec.ResErr;
        this.CMDBCIDEFINERELATIONTYPE_ListDelID(CMDBCIDEFINERELATIONTYPEList, CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINERELATIONTYPE);
    }
    finally {
        Param.Destroy();
    }
}

//CMDBCIRELATION
UsrCfg.CMDB.TCIProfiler.prototype.CMDBCIRELATION_ListDelID = function (CMDBCIRELATIONList, IDCMDBCIRELATION) {
    var CMDBCIRELATION = null;
    var Counter = 0;
    while (Counter < CMDBCIRELATIONList.length) {

        if (IDCMDBCIRELATION == CMDBCIRELATIONList[Counter].IDCMDBCIRELATION) {
            CMDBCIRELATION = CMDBCIRELATIONList[Counter];
            CMDBCIRELATIONList.splice(Counter, 1);
        }
        else {
            Counter++;
        }
    }
    return (CMDBCIRELATION);
}
UsrCfg.CMDB.TCIProfiler.prototype.CMDBCIRELATION_ListSetID = function (CMDBCIRELATIONList, IDCMDBCIRELATION) {
    for (var i = 0; i < CMDBCIRELATIONList.length; i++) {
        if (IDCMDBCIRELATION == CMDBCIRELATIONList[i].IDCMDBCIRELATION)
            return (CMDBCIRELATIONList[i]);
    }
    var CMDBCIRELATION = new UsrCfg.CMDB.Properties.TCMDBCIRELATION();
    CMDBCIRELATION.IDCMDBCIRELATION = IDCMDBCIRELATION;
    return CMDBCIRELATION;
}
UsrCfg.CMDB.TCIProfiler.prototype.CMDBCIRELATION_ListAdd = function (CMDBCIRELATIONList, CMDBCIRELATION) {
    var i = this.CMDBCIRELATION_ListGetIndex(CMDBCIRELATIONList, CMDBCIRELATION);
    if (i == -1) CMDBCIRELATIONList.push(CMDBCIRELATION);
    else CMDBCIRELATIONList[i] = CMDBCIRELATION;
}
UsrCfg.CMDB.TCIProfiler.prototype.CMDBCIRELATION_ListGetIndex = function (CMDBCIRELATIONList, CMDBCIRELATION) {
    for (var i = 0; i < CMDBCIRELATIONList.length; i++) {
        if (CMDBCIRELATION.IDCMDBCIRELATION == CMDBCIRELATIONList[i].IDCMDBCIRELATION)
            return (i);
    }
    return (-1);
}
UsrCfg.CMDB.TCIProfiler.prototype.CMDBCIRELATION_ListFillComplete = function (CIDEFINEList, RelationType, inCI, CIList, CMDBBRANDList, CMDBCIRELATIONList, CMDBCIDEFINERELATIONTYPEList, IDCMDBCIRELATION) {
    var ResErr;
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        var DS_CIRELATION = null;
        //*********** GET_GETID ***************************** 

        Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCI.FieldName, inCI.IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        switch (RelationType) {
            case UsrCfg.CMDB.Properties.TRelationType._Friend:
                Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCIDEFINE.FieldName, inCI.IDCMDBCIDEFINE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                Param.AddUnknown("BLPREFIX", this.BLPrefix == null ? "" : this.BLPrefix, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                DS_CIRELATION = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "CMDBCI_GET_FRIENDS", Param.ToBytes());
                break;
            case UsrCfg.CMDB.Properties.TRelationType._Children:
                if (IDCMDBCIRELATION < 1) Param.AddUnknown(UsrCfg.InternoCMDBNames.CMDBCIRELATION.IDCMDBCIRELATION.FieldName, UsrCfg.InternoCMDBNames.CMDBCIRELATION.IDCMDBCIRELATION.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                else Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCIRELATION.IDCMDBCIRELATION.FieldName, IDCMDBCIRELATION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                Param.AddUnknown("BLPREFIX", this.BLPrefix == null ? "" : this.BLPrefix, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                DS_CIRELATION = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "CMDBCI_GET_CHILDRENS", Param.ToBytes());

                break;
            case UsrCfg.CMDB.Properties.TRelationType._Parent:
                if (IDCMDBCIRELATION < 1) Param.AddUnknown(UsrCfg.InternoCMDBNames.CMDBCIRELATION.IDCMDBCIRELATION.FieldName, UsrCfg.InternoCMDBNames.CMDBCIRELATION.IDCMDBCIRELATION.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                else Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCIRELATION.IDCMDBCIRELATION.FieldName, IDCMDBCIRELATION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                Param.AddUnknown("BLPREFIX", this.BLPrefix == null ? "" : this.BLPrefix, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                DS_CIRELATION = SysCfg.DB.SQL.Methods.OpenDataSet("Atis", "CMDBCI_GET_PARENT", Param.ToBytes());
                break;
            default:
                break;
        }
        ResErr = DS_CIRELATION.ResErr;
        if (ResErr.NotError) {
            if (DS_CIRELATION.DataSet.RecordCount > 0) {
                DS_CIRELATION.DataSet.First();
                while (!(DS_CIRELATION.DataSet.Eof)) {

                    var IdxCI = this.CI_ListGetIndex(DS_CIRELATION.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCI.FieldName).asInt32(), CIList);
                    if (IdxCI == -1) {
                        var UnCI = new UsrCfg.CMDB.Properties.TCI();


                        UnCI.CI_DATEPLANNED = DS_CIRELATION.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCI.CI_DATEPLANNED.FieldName).asDateTime();
                        UnCI.CI_DATEIN = DS_CIRELATION.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCI.CI_DATEIN.FieldName).asDateTime();
                        UnCI.CI_DATEOUT = DS_CIRELATION.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCI.CI_DATEOUT.FieldName).asDateTime();
                        UnCI.CI_GENERICNAME = DS_CIRELATION.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCI.CI_GENERICNAME.FieldName).asString();
                        UnCI.CI_DESCRIPTION = DS_CIRELATION.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCI.CI_DESCRIPTION.FieldName).asString();
                        UnCI.IDCMDBCI = DS_CIRELATION.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCI.FieldName).asInt32();
                        UnCI.IDCMDBCIDEFINE = DS_CIRELATION.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCIDEFINE.FieldName).asInt32();
                        UnCI.CIDEFINE_NAME = DS_CIRELATION.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINE.CIDEFINE_NAME.FieldName).asString();
                        UnCI.IDCMDBBRAND = DS_CIRELATION.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBBRAND.FieldName).asInt32();
                        UnCI.CMDBBRAND = this.CMDBBRAND_ListSetID(CMDBBRANDList, UnCI.IDCMDBBRAND);
                        UnCI.IDCMDBCISTATE = UsrCfg.CMDB.Properties.TCMDBCISTATE.GetEnum(DS_CIRELATION.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCISTATE.FieldName).asInt32());
                        UnCI.CI_SERIALNUMBER = DS_CIRELATION.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCI.CI_SERIALNUMBER.FieldName).asString();
                        UnCI.CI_PURCHASEDORRENTED = DS_CIRELATION.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCI.CI_PURCHASEDORRENTED.FieldName).asString();
                        UnCI.CMDBCIDEFINE = this.CIDEFINE_ListSetId(CIDEFINEList, UnCI.IDCMDBCIDEFINE);
                        UnCI.Visible = false;
                        UnCI.isComplete = false;
                        CIList.push(UnCI);
                        IdxCI = CIList.length - 1;
                    }



                    if (inCI.IDCMDBCI != CIList[IdxCI].IDCMDBCI) {
                        switch (RelationType) {
                            case UsrCfg.CMDB.Properties.TRelationType._Friend:
                                if (inCI.IDCMDBCI != -1) inCI.ListFriends.push(CIList[IdxCI]);
                                break;
                            case UsrCfg.CMDB.Properties.TRelationType._Children:
                            case UsrCfg.CMDB.Properties.TRelationType._Parent:
                                var CMDBCIRELATION = this.CMDBCIRELATION_ListSetID(CMDBCIRELATIONList, DS_CIRELATION.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIRELATION.IDCMDBCIRELATION.FieldName).asInt32());
                                this.CMDBCIRELATION_ListAdd(CMDBCIRELATIONList, CMDBCIRELATION);
                                CMDBCIRELATION.IDCMDBCI_CHILD = DS_CIRELATION.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIRELATION.IDCMDBCI_CHILD.FieldName).asInt32();
                                CMDBCIRELATION.IDCMDBCI_PARENT = DS_CIRELATION.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIRELATION.IDCMDBCI_PARENT.FieldName).asInt32();
                                CMDBCIRELATION.IDCMDBCIDEFINERELATIONTYPE = DS_CIRELATION.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINERELATIONTYPE.FieldName).asInt32();
                                //if (CMDBCIRELATION.IDCMDBCIDEFINERELATIONTYPE > 0)
                                CMDBCIRELATION.CMDBCIDEFINERELATIONTYPE = this.CMDBCIDEFINERELATIONTYPE_ListSetID(CMDBCIDEFINERELATIONTYPEList, DS_CIRELATION.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINERELATIONTYPE.FieldName).asInt32());
                                CMDBCIRELATION.CMDBCIDEFINERELATIONTYPE.CMDBCIDEFINE_CHILD = this.CIDEFINE_ListSetId(CIDEFINEList, DS_CIRELATION.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINE_CHILD.FieldName).asInt32());
                                CMDBCIRELATION.CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINE_PARENT = DS_CIRELATION.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINE_PARENT.FieldName).asInt32();
                                CMDBCIRELATION.IDCMDBRELATIONTYPE = DS_CIRELATION.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIRELATION.IDCMDBRELATIONTYPE.FieldName).asInt32();


                                if (RelationType == UsrCfg.CMDB.Properties.TRelationType._Children) {
                                    if (inCI.IDCMDBCI != -1) {
                                        inCI.ListChildren.push(CIList[IdxCI]);
                                        CMDBCIRELATION.CI_CHILD = CIList[IdxCI];
                                        CMDBCIRELATION.CI_PARENT = inCI;
                                        inCI.CIRELATIONListChild.push(CMDBCIRELATION);
                                    }

                                }
                                if (RelationType == UsrCfg.CMDB.Properties.TRelationType._Parent) {
                                    if (inCI.IDCMDBCI != -1) {
                                        inCI.ListParent.push(CIList[IdxCI]);
                                        CMDBCIRELATION.CI_CHILD = inCI;
                                        CMDBCIRELATION.CI_PARENT = CIList[IdxCI];
                                        inCI.CIRELATIONListParent.push(CMDBCIRELATION);

                                    }
                                }
                                break;
                        }
                    }
                    DS_CIRELATION.DataSet.Next();
                }
            }
            else {
                DS_CIRELATION.ResErr.NotError = false;
                DS_CIRELATION.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
}
UsrCfg.CMDB.TCIProfiler.prototype.CMDBCIRELATION_GET = function (CMDBCIRELATION) {
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCIRELATION.IDCMDBCI_CHILD.FieldName, CMDBCIRELATION.IDCMDBCI_CHILD, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCIRELATION.IDCMDBCI_PARENT.FieldName, CMDBCIRELATION.IDCMDBCI_PARENT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCIRELATION.IDCMDBRELATIONTYPE.FieldName, CMDBCIRELATION.IDCMDBRELATIONTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddUnknown("BLPREFIX", this.BLPrefix == null ? "" : this.BLPrefix, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //*********** CMDBCIRELATION_GET ***************************** 
        var DS_CMDBCIRELATION = SysCfg.DB.SQL.Methods.OpenDataSet("CMDB", "CMDBCIRELATION_GETID", Param.ToBytes());
        CMDBCIRELATION.ResErr = DS_CMDBCIRELATION.ResErr;
        if (DS_CMDBCIRELATION.ResErr.NotError) {
            if (DS_CMDBCIRELATION.DataSet.RecordCount > 0) {
                DS_CMDBCIRELATION.DataSet.First();
                CMDBCIRELATION.IDCMDBCIRELATION = DS_CMDBCIRELATION.DataSet.RecordSet.FieldName(UsrCfg.InternoCMDBNames.CMDBCIRELATION.IDCMDBCIRELATION.FieldName).asInt32();
            }
            else {
                DS_CMDBCIRELATION.ResErr.NotError = false;
                DS_CMDBCIRELATION.ResErr.Mesaje = "Record Count = 0";
            }
        }
    }
    finally {
        Param.Destroy();
    }
}
UsrCfg.CMDB.TCIProfiler.prototype.CMDBCIRELATION_ADD = function (CMDBCIRELATION) {
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCIRELATION.IDCMDBCI_CHILD.FieldName, CMDBCIRELATION.IDCMDBCI_CHILD, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCIRELATION.IDCMDBCI_PARENT.FieldName, CMDBCIRELATION.IDCMDBCI_PARENT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCIRELATION.IDCMDBRELATIONTYPE.FieldName, CMDBCIRELATION.IDCMDBRELATIONTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddUnknown("BLPREFIX", this.BLPrefix == null ? "" : this.BLPrefix, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //*********** CMDBCIRELATION_ADD ***************************** 
        var Exec = SysCfg.DB.SQL.Methods.Exec("CMDB", "CMDBCIRELATION_ADD", Param.ToBytes());
        CMDBCIRELATION.ResErr = Exec.ResErr;
        if (Exec.ResErr.NotError) {
            this.CMDBCIRELATION_GET(CMDBCIRELATION);
        }
    }
    finally {
        Param.Destroy();
    }
}
UsrCfg.CMDB.TCIProfiler.prototype.CMDBCIRELATION_DEL = function (CMDBCIRELATION) {
    var Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCIRELATION.IDCMDBCIRELATION.FieldName, CMDBCIRELATION.IDCMDBCIRELATION, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddUnknown("BLPREFIX", this.BLPrefix == null ? "" : this.BLPrefix, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        //*********** CMDBCIRELATION_DEL ***************************** 
        var Exec = SysCfg.DB.SQL.Methods.Exec("CMDB", "CMDBCIRELATION_DEL", Param.ToBytes());
        CMDBCIRELATION.ResErr = Exec.ResErr;
    }
    finally {
        Param.Destroy();
    }
}
UsrCfg.CMDB.TCIProfiler.prototype.GETSQL = function (CIDEFINEEXTRATABLEList, CIList, EXTRATABLE_NAME, IDCMDBCI) {
    var CI = this.CI_ListSetID(CIList, IDCMDBCI);
    var CIDEFINEEXTRATABLE = this.CIDEFINEEXTRATABLE_ListSetName(CIDEFINEEXTRATABLEList, EXTRATABLE_NAME, CI.IDCMDBCIDEFINE);
    return "";
}
UsrCfg.CMDB.TCIProfiler.prototype.BuildSQL = function (CIDEFINEEXTRATABLE, CI, SQLResult) {



    var SQL = new SysCfg.DB.TSQL();
    SysCfg.Error.Properties.ResErrfill(SQL.ResErr);
    var PrefixTable = (this.BLPrefix == null ? "" : this.BLPrefix) + UsrCfg.CMDB.Properties.PrefixTable + CIDEFINEEXTRATABLE.EXTRATABLE_NAMETABLE;
    var PrefixTableLog = PrefixTable + SysCfg.Interno.Properties.PrefixSysLog;


    var PrefixIDField = UsrCfg.CMDB.Properties.PrefixIDField + CIDEFINEEXTRATABLE.EXTRATABLE_NAMETABLE;
    var PrefixIDCI = UsrCfg.CMDB.Properties.TCMDBCIDEFINEEXTRATABLE_SOURCETYPE.GetEnum(CIDEFINEEXTRATABLE.EXTRATABLE_IDSOURCE).name;//UsrCfg.CMDB.Properties.PrefixIDCI;            
    var PrefixLOG = CIDEFINEEXTRATABLE.EXTRATABLE_LOG;

    var Aux0 = "";
    var Aux1 = "," + PrefixIDField;
    var Aux2 = " and " + PrefixIDField + "=@[" + PrefixIDField + "]";
    var Aux3 = "";
    var Aux4 = "";
    var Aux5 = PrefixIDCI;
    var Log0 = "," + SysCfg.Interno.Properties.FieldName_Date
                + "," + SysCfg.Interno.Properties.FieldName_Source_SysLog
                + "," + SysCfg.Interno.Properties.FieldName_Event_SysLog
                + "," + SysCfg.Interno.Properties.FieldName_IDCMDBCI_SysLog;
    var Log1 = ",@[" + SysCfg.Interno.Properties.FieldName_Date + "]"
                + ",@[" + SysCfg.Interno.Properties.FieldName_Source_SysLog + "]"
                + ",@[" + SysCfg.Interno.Properties.FieldName_Event_SysLog + "]"
                + ",@[" + SysCfg.Interno.Properties.FieldName_IDCMDBCI_SysLog + "]";
    var Log2 = " , " + SysCfg.Interno.Properties.FieldName_Date + "=@[" + SysCfg.Interno.Properties.FieldName_Date + "]"
               + " , " + SysCfg.Interno.Properties.FieldName_Source_SysLog + "=@[" + SysCfg.Interno.Properties.FieldName_Source_SysLog + "]"
               + " , " + SysCfg.Interno.Properties.FieldName_Event_SysLog + "=@[" + SysCfg.Interno.Properties.FieldName_Event_SysLog + "]"
               + " , " + SysCfg.Interno.Properties.FieldName_IDCMDBCI_SysLog + "=@[" + SysCfg.Interno.Properties.FieldName_IDCMDBCI_SysLog + "]";



    for (var Contfield = 0; Contfield < CIDEFINEEXTRATABLE.CMDBCIDEFINEEXTRAFIELDSList.length; Contfield++) {
        Aux0 += "," + CIDEFINEEXTRATABLE.CMDBCIDEFINEEXTRAFIELDSList[Contfield].EXTRAFIELDS_NAME;
        Aux1 += "," + CIDEFINEEXTRATABLE.CMDBCIDEFINEEXTRAFIELDSList[Contfield].EXTRAFIELDS_NAME;
        if (CIDEFINEEXTRATABLE.CMDBCIDEFINEEXTRAFIELDSList[Contfield].IDCMDBKEYTYPE == SysCfg.DB.Properties.TKeyType.ADN) {
            Aux2 += " and " + CIDEFINEEXTRATABLE.CMDBCIDEFINEEXTRAFIELDSList[Contfield].EXTRAFIELDS_NAME + "=@[" + CIDEFINEEXTRATABLE.CMDBCIDEFINEEXTRAFIELDSList[Contfield].EXTRAFIELDS_NAME + "]";
        }
        Aux3 += ",@[" + CIDEFINEEXTRATABLE.CMDBCIDEFINEEXTRAFIELDSList[Contfield].EXTRAFIELDS_NAME + "]";
        Aux4 += "," + CIDEFINEEXTRATABLE.CMDBCIDEFINEEXTRAFIELDSList[Contfield].EXTRAFIELDS_NAME + "=@[" + CIDEFINEEXTRATABLE.CMDBCIDEFINEEXTRAFIELDSList[Contfield].EXTRAFIELDS_NAME + "]";
    }
    switch (SQLResult) {
        case UsrCfg.CMDB.Properties.TSQLResult.Select:
            SQL.StrSql = "SELECT " + Aux5 + Aux1 + " FROM " + PrefixTable + " WHERE " + Aux5 + "=" + CI.IDCMDBCI;
            SQL.StrLog = "SELECT " + Aux5 + Log0 + Aux1 + " FROM " + PrefixTableLog + " WHERE " + Aux5 + "=" + CI.IDCMDBCI;
            break;
        case UsrCfg.CMDB.Properties.TSQLResult.SelectID:
            SQL.StrSql = "SELECT " + Aux5 + Aux1 + " FROM " + PrefixTable + " WHERE " + Aux5 + "=" + CI.IDCMDBCI + Aux2;
            SQL.StrLog = "SELECT " + Aux5 + Log0 + Aux1 + " FROM " + PrefixTableLog + " WHERE " + Aux5 + "=" + CI.IDCMDBCI + Aux2;
            break;
        case UsrCfg.CMDB.Properties.TSQLResult.Insert:
            SQL.StrSql = "INSERT INTO " + PrefixTable + "(" + Aux5 + Aux0 + ") VALUES (" + CI.IDCMDBCI + Aux3 + ")";
            SQL.StrLog = "INSERT INTO " + PrefixTableLog + "(" + Aux5 + Log0 + Aux0 + ") VALUES (" + CI.IDCMDBCI + Log1 + Aux3 + ")";
            break;
        case UsrCfg.CMDB.Properties.TSQLResult.Update:
            //*1 SQL.StrSql = "UPDATE " + PrefixTable + " SET " + Aux5 + "=" + CI.IDCMDBCI + Aux4 + " WHERE  " + Aux5 + "=" + CI.IDCMDBCI + Aux2;
            SQL.StrSql = "UPDATE " + PrefixTable + " SET " + Aux5 + "=" + CI.IDCMDBCI + Aux4 + " WHERE  " + Aux5 + "=" + CI.IDCMDBCI + " and " + PrefixIDField + "=@[" + PrefixIDField + "]";
            SQL.StrLog = "INSERT INTO " + PrefixTableLog + "(" + Aux5 + Log0 + Aux0 + ") VALUES (" + CI.IDCMDBCI + Log1 + Aux3 + ")";
            //SQL.StrLog = "UPDATE " + PrefixTableLog + " SET " + Aux5 + "=" + CI.IDCMDBCI + Log2 + Aux4 + " WHERE  " + Aux5 + "=" + CI.IDCMDBCI + " and " + PrefixIDField + "=@[" + PrefixIDField + "]";
            break;
        case UsrCfg.CMDB.Properties.TSQLResult.Delete:
            //*1 SQL.StrSql = "DELETE " + PrefixTable + " WHERE  " + Aux5 + "=" + CI.IDCMDBCI + Aux2;
            SQL.StrSql = "DELETE " + PrefixTable + " WHERE  " + Aux5 + "=" + CI.IDCMDBCI + " and " + PrefixIDField + "=@[" + PrefixIDField + "]";
            SQL.StrLog = "INSERT INTO " + PrefixTableLog + "(" + Aux5 + Log0 + Aux0 + ") VALUES (" + CI.IDCMDBCI + Log1 + Aux3 + ")";
            //SQL.StrLog = "DELETE " + PrefixTableLog + " WHERE  " + Aux5 + "=" + CI.IDCMDBCI + " and " + PrefixIDField + "=@[" + PrefixIDField + "]";
            break;
    }
    return (SQL);
}

UsrCfg.CMDB.TCIProfiler.prototype.ROWEXTRATABLE_GET = function(CIDEFINEEXTRATABLE)
{
    var Open = null;
    try
    {
        var SQL = this.BuildSQL(CIDEFINEEXTRATABLE, this.SETCI, UsrCfg.CMDB.Properties.TSQLResult.Select);
        // CMDBCIDEFINEEXTRATABLE_GET ****************** 
        Open = SysCfg.DB.SQL.Methods.Open(SQL.StrSql, new Array(0));
        if (Open.ResErr.NotError)
        {
            return Open;
        }
    }
    finally
    {
    }
    return Open;
}