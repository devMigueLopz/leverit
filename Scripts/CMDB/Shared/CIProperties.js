﻿

//******** Nuevo 
UsrCfg.CMDB.Properties.Prefix = "CMDB";
UsrCfg.CMDB.Properties.PrefixTable = "CMDB_EF";
UsrCfg.CMDB.Properties.PrefixTableBL = "BL";
UsrCfg.CMDB.Properties.PrefixIDField = "IDCMDB_EF";

UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle =
{
    None: 0,
    TextPassword: 1,
    Date: 2,
    Time: 3,
    ProgressBar: 4,
    LookUp: 5,
    LookUpOption: 6
}

UsrCfg.CMDB.Properties.TCMDBRELATIONTYPE = {
    HI: 1,
    MIDDLE: 2,
    LOW: 3
}

UsrCfg.CMDB.Properties.TRelationType = {
    _None: { value: 0, name: "_None" },
    _Friend: { value: 1, name: "_Friend" },
    _Children: { value: 2, name: "_Children" },
    _Parent: { value: 3, name: "_Parent" }
}

//***** quiza no se ocupeun
UsrCfg.CMDB.Properties.TCMDBCIMP_TYPE = {
    MODE_NORMAL: 1,
    MODE_VIRTUAL: 2,
    MODE_INCOMING: 3
}

UsrCfg.CMDB.Properties.TCMDBCIDEFINEEXTRATABLE_SOURCETYPE = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    IDCMDBCI: { value: 1, name: "IDCMDBCI" }
}

UsrCfg.CMDB.Properties.StateFormulario = {
    Insert: 0,
    Update: 1,
    Delete: 2,
    New: 3,
    Cancel: 4
}
UsrCfg.CMDB.Properties.TSQLResult =
{
    Select: 0,
    SelectID: 1,
    Insert: 2,
    Update: 3,
    Delete: 4,
}


UsrCfg.CMDB.Properties.TCMDBCIDEFINERELATIONTYPE = function () { // la definicon de la relacion virtual; es decir estblece un definicion para clasificar la relaciones existentes
    this.ResErr; //public SysCfg.Error.Properties.TResErr ResErr;
    this.CIDEFINERELATIONTYPE_NAME; //public String CIDEFINERELATIONTYPE_NAME;
    this.IDCMDBCIDEFINE_CHILD; //public Int32 IDCMDBCIDEFINE_CHILD;
    this.IDCMDBCIDEFINE_PARENT; //public Int32 IDCMDBCIDEFINE_PARENT;
    this.CMDBCIDEFINE_CHILD; //public TCMDBCIDEFINE CMDBCIDEFINE_CHILD;
    this.CMDBCIDEFINE_PARENT; //public TCMDBCIDEFINE CMDBCIDEFINE_PARENT;
    this.IDCMDBCIDEFINERELATIONTYPE; //public Int32 IDCMDBCIDEFINERELATIONTYPE;
    this.IDCMDBRELATIONTYPE; //public TCMDBRELATIONTYPE IDCMDBRELATIONTYPE;            
}

UsrCfg.CMDB.Properties.TCMDBCIRELATION = function () {
    this.ResErr; //public SysCfg.Error.Properties.TResErr ResErr;
    this.IDCMDBCI_CHILD; //public Int32 IDCMDBCI_CHILD;
    this.IDCMDBCI_PARENT; //public Int32 IDCMDBCI_PARENT;
    this.IDCMDBCIRELATION; //public Int32  IDCMDBCIRELATION;
    this.IDCMDBRELATIONTYPE; //public TCMDBRELATIONTYPE IDCMDBRELATIONTYPE;
    this.CI_PARENT; //public TCI CI_PARENT;
    this.CI_CHILD; //public TCI CI_CHILD;
    this.IDCMDBCIDEFINERELATIONTYPE; //public Int32 IDCMDBCIDEFINERELATIONTYPE;
    this.CMDBCIDEFINERELATIONTYPE; //public TCMDBCIDEFINERELATIONTYPE CMDBCIDEFINERELATIONTYPE;
}

UsrCfg.CMDB.Properties.TCI = function () {
    this.ResErr = null; // new SysCfg.Error.Properties.TResErr ;

    this.isComplete = false; //public Boolean isComplete = false;
    this.CI_DATEPLANNED;  //public DateTime CI_DATEPLANNED;
    this.CI_DATEIN;  //public DateTime CI_DATEIN;
    this.CI_DATEOUT;  //public DateTime CI_DATEOUT;
    this.CI_GENERICNAME;  //public String CI_GENERICNAME;
    this.CI_DESCRIPTION;  //public String CI_DESCRIPTION;
    this.IDCMDBCI;  //public Int32 IDCMDBCI;
    this.IDCMDBBRAND;  //public Int32 IDCMDBBRAND;
    this.CMDBBRAND;  //public TCMDBBRAND CMDBBRAND;
    this.IDCMDBCISTATE;  //public TCMDBCISTATE IDCMDBCISTATE;
    this.CI_SERIALNUMBER;  //public String CI_SERIALNUMBER;
    this.CI_PURCHASEDORRENTED;  //public String CI_PURCHASEDORRENTED;
    this.Visible = false;  //public Boolean Visible = false;

    this.ListFriends = new Array();//public List<TCI> ListFriends = new List<TCI>();
    this.ListParent = new Array();//public List<TCI> ListParent = new List<TCI>();
    this.ListChildren = new Array();//public List<TCI> ListChildren = new List<TCI>();

    this.CIRELATIONListChild = new Array(); //public List<TCMDBCIRELATION> CIRELATIONListChild = new List<TCMDBCIRELATION>();//Los que tu tines relacionados
    this.CIRELATIONListParent = new Array(); //public List<TCMDBCIRELATION> CIRELATIONListParent = new List<TCMDBCIRELATION>();//lo que te ha relacionado                         

    this.IDCMDBCIDEFINE = -1; //public Int32 IDCMDBCIDEFINE = -1;
    this.CIDEFINE_NAME; //public String CIDEFINE_NAME;
    this.CMDBCIDEFINE; //public TCMDBCIDEFINE CMDBCIDEFINE;            
}

UsrCfg.CMDB.Properties.TCMDBCIDEFINEEXTRATABLE = function () {
    this.ResErr; //public SysCfg.Error.Properties.TResErr ResErr;
    this.ExitsLIFESTATUSPERMISSION; //public Boolean ExitsLIFESTATUSPERMISSION = false;//*MASec
    this.IDCMDBCIDEFINEEXTRATABLE; //public Int32 IDCMDBCIDEFINEEXTRATABLE;
    this.EXTRATABLE_DESCRIPTION; //public String EXTRATABLE_DESCRIPTION;
    this.EXTRATABLE_ENABLED; //public Boolean EXTRATABLE_ENABLED;
    this.EXTRATABLE_NAME; //public String EXTRATABLE_NAME;
    this.EXTRATABLE_NAMETABLE; //public String EXTRATABLE_NAMETABLE;
    this.EXTRATABLE_IDSOURCE; //public TCMDBCIDEFINEEXTRATABLE_SOURCETYPE EXTRATABLE_IDSOURCE;
    this.EXTRATABLE_LOG = false; //public Boolean EXTRATABLE_LOG = false;
    this.IDCMDBCIDEFINE; //public Int32 IDCMDBCIDEFINE;
    this.GRIDENABLE = false; //public Boolean GRIDENABLE = false;
    this.CMDBCIDEFINE; //public TCMDBCIDEFINE CMDBCIDEFINE;
    this.CMDBCIMP_TYPE; //public TCMDBCIMP_TYPE CMDBCIMP_TYPE; //CRC
    this.USEBASELINE = false; //public Boolean USEBASELINE = false; //CRC
    this.SQL; //public String SQL; //CRC
    this.READONLY; //public Boolean READONLY = false;//Camb01EF
    this.CMDBCIDEFINEEXTRAFIELDSList = new Array(); //public List<TCMDBCIDEFINEEXTRAFIELDS> CMDBCIDEFINEEXTRAFIELDSList = new List<TCMDBCIDEFINEEXTRAFIELDS>();    
}

UsrCfg.CMDB.Properties.TCMDBCIDEFINE = function () {
    this.ResErr = null; //public SysCfg.Error.Properties.TResErr ResErr;
    this.isComplete = false; //public Boolean isComplete = false;
    this.CIDEFINE_INDEX = 0; //public Int32 CIDEFINE_INDEX;
    this.CIDEFINE_DESCRIPTION = ""; //public String CIDEFINE_DESCRIPTION;
    this.IDCMDBCIDEFINE = 0; //public Int32 IDCMDBCIDEFINE;
    this.IDCMDBCIDEFINELEVEL = 0; //public TCMDBCIDEFINELEVEL IDCMDBCIDEFINELEVEL;
    this.CIDEFINE_NAME = ""; //public String CIDEFINE_NAME { get; set; }
    this.IDCMDBCIDEFINESUPERCLASS = 0; //public Int32 IDCMDBCIDEFINESUPERCLASS;
    this.CIDEFINE_IDFILE = ""; //public String CIDEFINE_IDFILE;
    this.CIDEFINE_FILENAME = ""; //public String CIDEFINE_FILENAME;
    this.BYTEIMAGE = new Array(); //public Byte[] BYTEIMAGE;
    this.IDCMDBCIDEFINETYPE = 0; //public TCMDBCIDEFINETYPE IDCMDBCIDEFINETYPE;
    this.CMDBCIDEFINEEXTRATABLElist = new Array(); //public List<TCMDBCIDEFINEEXTRATABLE> CMDBCIDEFINEEXTRATABLElist = new List<TCMDBCIDEFINEEXTRATABLE>();
}

UsrCfg.CMDB.Properties.TCMDBCIDEFINEEXTRAFIELDS = function () {
    this.IDMDLIFESTATUSPERMISSION; //public UsrCfg.SD.Properties.TMDLIFESTATUSPERMISSION IDMDLIFESTATUSPERMISSION = SD.Properties.TMDLIFESTATUSPERMISSION._Write;//*MASec  viene de sd properties//virtual  //_Disable
    this.MANDATORY = false; //public Boolean MANDATORY = false;   //Camb01EF //virtual
    this.IDCMDBCIDEFINEEXTRAFIELDS; //public Int32 IDCMDBCIDEFINEEXTRAFIELDS;
    this.DBDATATYPES_SIZE; //public Int32 DBDATATYPES_SIZE;
    this.DBKEYTYPE_NAME; //public String DBKEYTYPE_NAME;
    this.EXTRAFIELDS_DESCRIPTION; //public String EXTRAFIELDS_DESCRIPTION;
    this.EXTRAFIELDS_LOOKUPDISPLAY; //public String EXTRAFIELDS_LOOKUPDISPLAY;
    this.EXTRAFIELDS_COLUMNSTYLE; //public Int32 EXTRAFIELDS_COLUMNSTYLE;
    this.EXTRAFIELDS_LOOKUPID; //public String EXTRAFIELDS_LOOKUPID;
    this.EXTRAFIELDS_LOOKUPSQL; //public String EXTRAFIELDS_LOOKUPSQL;
    this.EXTRAFIELDS_NAME; //public String EXTRAFIELDS_NAME;
    this.IDCMDBCIDEFINE; //public Int32 IDCMDBCIDEFINE;
    this.CMDBCIDEFINE; //public TCMDBCIDEFINE CMDBCIDEFINE;
    this.IDCMDBCIDEFINEEXTRATABLE; //public Int32 IDCMDBCIDEFINEEXTRATABLE;
    this.CMDBCIDEFINEEXTRATABLE; //public TCMDBCIDEFINEEXTRATABLE CMDBCIDEFINEEXTRATABLE;
    this.IDCMDBDBDATATYPES; //public SysCfg.DB.Properties.TDataType IDCMDBDBDATATYPES;
    this.IDCMDBKEYTYPE; //public SysCfg.DB.Properties.TKeyType IDCMDBKEYTYPE;
    this.ResErr; //public SysCfg.Error.Properties.TResErr ResErr;
}

UsrCfg.CMDB.Properties.TCMDBCIDEFINETYPE = {
    _GENERIC: 0,
    _PEOPLE: 1,
    _DEVICE: 2,
    _KNOWN_ERRORS_RECORD: 3,
    _WORKAROUND_RECORD: 4,
    _LOCATION: 5
}

UsrCfg.CMDB.Properties.TCMDBBRAND = function () {
    this.ResErr; //public SysCfg.Error.Properties.TResErr ResErr;
    this.BRAND_DESCRIPTION; //public String BRAND_DESCRIPTION;
    this.BRAND_INACTIVE; //public Boolean BRAND_INACTIVE;
    this.BRAND_NAME; //public String BRAND_NAME { get; set; }
    this.IDCMDBBRAND; //public Int32 IDCMDBBRAND { get; set; }
}

UsrCfg.CMDB.Properties.TCMDBCISTATE = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    PLANNED: { value: 1, name: "PLANNED" },
    IN: { value: 2, name: "IN" },
    OUT: { value: 3, name: "OUT" }
}

UsrCfg.CMDB.Properties.TCMDBCIDEFINELEVEL = {
    _LevelNone: { value: -1, name: "None level" },
    _LevelZero: { value: 0, name: "Zero level" },
    _LevelOne: { value: 1, name: "One level" },
    _LevelTwo: { value: 2, name: "Two level" },
    _LevelThree: { value: 3, name: "Three level" },
    _LevelFour: { value: 4, name: "Four level" },
    _LevelFive: { value: 5, name: "Five level" },
    _LevelSix: { value: 6, name: "Six level" },
    _LevelSeven: { value: 7, name: "Seven level" },
    _LevelEight: { value: 8, name: "Eight level" }
}

UsrCfg.CMDB.Properties.TCMDBTRIGGERSTATUS = {
    CREATE: { value: 1, name: "CREATE" },
    ACTIVE: { value: 1, name: "ACTIVE" },
    CANCELED: { value: 1, name: "CANCELED" }
}