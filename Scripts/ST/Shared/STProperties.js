﻿ST.STEditor.Properties

/*


        public const String Prefix = "MDST";
        public const String PrefixTable = "MDST_EF";
        public const String PrefixIDField = "IDMDST_EF";
        //public const String PrefixIDSDCASEMT = "IDSDCASEMT";




public enum TSQLResult
{
    [SysCfg.DisplayText("Select")]
    Select = 0,
    [SysCfg.DisplayText("SelectID")]
    SelectID = 1,
    [SysCfg.DisplayText("Insert")]
    Insert = 2,
    [SysCfg.DisplayText("Update")]
    Update = 3,
    [SysCfg.DisplayText("Delete")]
    Delete = 4,
    }






//MDSERVICEEXTRATABLE
//IDMDSERVICEEXTRATABLE
[DataContract]
public class TMDSERVICEEXTRATABLE
{
    [DataMember]
    public Boolean ExitsLIFESTATUSPERMISSION = false;//*MASec               
    [DataMember]
    public Int32 IDMDSERVICEEXTRATABLE;
    [DataMember]
    public String EXTRATABLE_DESCRIPTION;
    [DataMember]
    public Boolean EXTRATABLE_ENABLED;
    [DataMember]
    public String EXTRATABLE_NAME;
    [DataMember]
    public String EXTRATABLE_NAMETABLE;
    [DataMember]
    public UsrCfg.SD.Properties.TMDSERVICEEXTRATABLE_SOURCETYPE EXTRATABLE_IDSOURCE;
    [DataMember]
    public Boolean EXTRATABLE_LOG;
    [DataMember]
    public Int32 IDMDSERVICETYPE;
    [DataMember]
    public Boolean GRIDENABLE = false;
    [DataMember]
    public Boolean READONLY = false;  //Problema para steeps multiples
    [DataMember]
    public List<TMDSERVICEEXTRAFIELDS> MDSERVICEEXTRAFIELDSList = new List<TMDSERVICEEXTRAFIELDS>();
    [DataMember]
    public SysCfg.Error.Properties.TResErr ResErr;
}


//MDSERVICEEXTRAFIELDS
//IDMDSERVICEEXTRAFIELDS
[DataContract]
public class TMDSERVICEEXTRAFIELDS
{
    [DataMember]
    public UsrCfg.SD.Properties.TMDLIFESTATUSPERMISSION IDMDLIFESTATUSPERMISSION = UsrCfg.SD.Properties.TMDLIFESTATUSPERMISSION._Write;//*MASec  viene de sd properties   //virtual   //Problema para steeps multiples//UsrCfg.SD.Properties.TMDLIFESTATUSPERMISSION._Write
    [DataMember]
    public Boolean MANDATORY = false;    //virtual  //Problema para steeps multiples
    [DataMember]
    public Int32 IDMDSERVICEEXTRAFIELDS;
    [DataMember]
    public Int32 DBDATATYPES_SIZE;
    [DataMember]
    public String DBKEYTYPE_NAME;
    [DataMember]
    public String EXTRAFIELDS_DESCRIPTION;
    [DataMember]
    public String EXTRAFIELDS_LOOKUPDISPLAY;
    [DataMember]
    public Int32 EXTRAFIELDS_COLUMNSTYLE;
    [DataMember]
    public String EXTRAFIELDS_LOOKUPID;
    [DataMember]
    public String EXTRAFIELDS_LOOKUPSQL;
    [DataMember]
    public String EXTRAFIELDS_NAME;
    //IDMDSERVICETYPE
    [DataMember]
    public Int32 IDMDSERVICETYPE;
    [DataMember]
    public Int32 IDCMDBCIDEFINEEXTRATABLE;
    //[DataMember]//Recurrente
    public TMDSERVICEEXTRATABLE MDSERVICEEXTRATABLE;
    [DataMember]
    public SysCfg.DB.Properties.TDataType IDCMDBDBDATATYPES;
    [DataMember]
    public SysCfg.DB.Properties.TKeyType IDCMDBKEYTYPE;
    [DataMember]
    public SysCfg.Error.Properties.TResErr ResErr;
}

*/