﻿//ST.STEditor

/*
public partial class TSTProfiler
{
    public Int32 IDMDSERVICETYPE;
    //public String SERVICETYPEDESCRIPTION;
    public String SERVICETYPENAME { get; set; }
        
        



    public TSTProfiler( )
    {

    }
        
    public Int32 IDSDCASEMT = -1;
      
    public List<STEditor.Properties.TMDSERVICEEXTRATABLE> MDSERVICEEXTRATABLEList = new List<STEditor.Properties.TMDSERVICEEXTRATABLE>();
        
    public List<STEditor.Properties.TMDSERVICEEXTRAFIELDS> MDSERVICEEXTRAFIELDSList = new List<STEditor.Properties.TMDSERVICEEXTRAFIELDS>();
    public SysCfg.Error.Properties.TResErr ResErr;

    public Int32 ROWEXTRATABLE_GETID(STEditor.Properties.TMDSERVICEEXTRATABLE MDSERVICEEXTRATABLE, SysCfg.Stream.Properties.TParam Param)
    {
        Int32 ID = -1;
    try
    {
        SysCfg.DB.TSQL SQL = STEditor.Methods.BuildSQL(MDSERVICEEXTRATABLE, IDSDCASEMT, STEditor.Properties.TSQLResult.Select);
        //*********** CMDBCIDEFINEEXTRATABLE_GET ***************************** 
        var DS_CMDBCIDEFINEEXTRATABLE = SysCfg.DB.SQL.Methods.OpenDataSet(SQL.StrSql, Param.ToBytes());
        if (DS_CMDBCIDEFINEEXTRATABLE.ResErr.NotError)
        {
            if (DS_CMDBCIDEFINEEXTRATABLE.DataSet.RecordCount > 0)
            {
                DS_CMDBCIDEFINEEXTRATABLE.DataSet.First();
                String PrefixIDField = STEditor.Properties.PrefixIDField + MDSERVICEEXTRATABLE.EXTRATABLE_NAMETABLE;
                ID = DS_CMDBCIDEFINEEXTRATABLE.DataSet.RecordSet.FieldName(PrefixIDField).asInt32();
            }
        }
    }
    finally
    {


    }
    return (ID);
}
public Int32 ROWEXTRATABLE_ADD(STEditor.Properties.TMDSERVICEEXTRATABLE MDSERVICEEXTRATABLE, SysCfg.Stream.Properties.TParam Param)
{
    Int32 ID = -1;
try
{
    SysCfg.DB.TSQL SQL = STEditor.Methods.BuildSQL(MDSERVICEEXTRATABLE, IDSDCASEMT, STEditor.Properties.TSQLResult.Insert);
    //*********** CMDBCIDEFINEEXTRATABLE_ADD ***************************** 
    var Exec = SysCfg.DB.SQL.Methods.Exec(SQL.StrSql, Param.ToBytes());
    SysCfg.Log.Methods.WriteLog("STProfiler.ROWEXTRATABLE_ADD: SQL.StrSql=" + Exec.ResErr.Mesaje, null);
                
    ResErr = Exec.ResErr;
    if (ResErr.NotError)
    {
        if (MDSERVICEEXTRATABLE.EXTRATABLE_LOG)
        {
        ID = ROWEXTRATABLE_GETID(MDSERVICEEXTRATABLE, Param);
        Param.AddDateTime(SysCfg.Interno.Properties.FieldName_Date,UsrCfg.EF.Properties.DATE_SYSLOG, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(SysCfg.Interno.Properties.FieldName_Source_SysLog, (Int32)UsrCfg.EF.Properties.TSOURCE_SYSLOG.UNKNOWN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(SysCfg.Interno.Properties.FieldName_Event_SysLog, (Int32)UsrCfg.EF.Properties.TEVENT_SYSLOG.INSERT, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(SysCfg.Interno.Properties.FieldName_IDCMDBCI_SysLog, (Int32)UsrCfg.EF.Properties.IDCMDBCI_SYSLOG, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        var Execlog = SysCfg.DB.SQL.Methods.Exec(SQL.StrLog, Param.ToBytes());
        SysCfg.Log.Methods.WriteLog("STProfiler.ROWEXTRATABLE_ADD: SQL.StrLog=" + Execlog.ResErr.Mesaje, null);
        }
    }

}
finally
{
    Param.Destroy();
}
return (ID);

}
public void ROWEXTRATABLE_UPD(STEditor.Properties.TMDSERVICEEXTRATABLE MDSERVICEEXTRATABLE, SysCfg.Stream.Properties.TParam Param)
{
            try
{
                SysCfg.DB.TSQL SQL = STEditor.Methods.BuildSQL(MDSERVICEEXTRATABLE, IDSDCASEMT, STEditor.Properties.TSQLResult.Update);

//*********** CMDBCIDEFINEEXTRATABLE_UPD ***************************** 
var Exec = SysCfg.DB.SQL.Methods.Exec(SQL.StrSql, Param.ToBytes());
ResErr = Exec.ResErr;
if (ResErr.NotError)
{
    if (MDSERVICEEXTRATABLE.EXTRATABLE_LOG)
    {
    Param.AddDateTime(SysCfg.Interno.Properties.FieldName_Date, UsrCfg.EF.Properties.DATE_SYSLOG, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    Param.AddInt32(SysCfg.Interno.Properties.FieldName_Source_SysLog, (Int32)UsrCfg.EF.Properties.TSOURCE_SYSLOG.UNKNOWN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    Param.AddInt32(SysCfg.Interno.Properties.FieldName_Event_SysLog, (Int32)UsrCfg.EF.Properties.TEVENT_SYSLOG.UPDATE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    Param.AddInt32(SysCfg.Interno.Properties.FieldName_IDCMDBCI_SysLog, (Int32)UsrCfg.EF.Properties.IDCMDBCI_SYSLOG, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    var Execlog = SysCfg.DB.SQL.Methods.Exec(SQL.StrLog, Param.ToBytes());
    }
}
}
            finally
{
    Param.Destroy();
}
}
public void ROWEXTRATABLE_DEL(STEditor.Properties.TMDSERVICEEXTRATABLE MDSERVICEEXTRATABLE, SysCfg.Stream.Properties.TParam Param)
{
            try
{
                SysCfg.DB.TSQL SQL = STEditor.Methods.BuildSQL(MDSERVICEEXTRATABLE, IDSDCASEMT, STEditor.Properties.TSQLResult.Delete);
//*********** CMDBCIDEFINEEXTRATABLE_DEL ***************************** 
var Exec = SysCfg.DB.SQL.Methods.Exec(SQL.StrSql, Param.ToBytes());
ResErr = Exec.ResErr;
if (ResErr.NotError)
{
    if (MDSERVICEEXTRATABLE.EXTRATABLE_LOG)
    {
    Param.AddDateTime(SysCfg.Interno.Properties.FieldName_Date, UsrCfg.EF.Properties.DATE_SYSLOG, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    Param.AddInt32(SysCfg.Interno.Properties.FieldName_Source_SysLog, (Int32)UsrCfg.EF.Properties.TSOURCE_SYSLOG.UNKNOWN, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    Param.AddInt32(SysCfg.Interno.Properties.FieldName_Event_SysLog, (Int32)UsrCfg.EF.Properties.TEVENT_SYSLOG.DELETE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    Param.AddInt32(SysCfg.Interno.Properties.FieldName_IDCMDBCI_SysLog, (Int32)UsrCfg.EF.Properties.IDCMDBCI_SYSLOG, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    var Execlog = SysCfg.DB.SQL.Methods.Exec(SQL.StrLog, Param.ToBytes());
    }

}
}
            finally
{
    Param.Destroy();
}
}





}

*/