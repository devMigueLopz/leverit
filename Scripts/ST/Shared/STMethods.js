﻿//ST.STEditor.Methods


/*
//MDSERVICEEXTRATABLE
public static STEditor.Properties.TMDSERVICEEXTRATABLE MDSERVICEEXTRATABLE_ListSetId(List<STEditor.Properties.TMDSERVICEEXTRATABLE> MDSERVICEEXTRATABLEList, Int32 IDMDSERVICEEXTRATABLE)
{
            for (int i = 0; i < MDSERVICEEXTRATABLEList.length; i++)
{
    if (IDMDSERVICEEXTRATABLE == MDSERVICEEXTRATABLEList[i].IDMDSERVICEEXTRATABLE)
        return (MDSERVICEEXTRATABLEList[i]);

}
return (new STEditor.Properties.TMDSERVICEEXTRATABLE { IDMDSERVICEEXTRATABLE = IDMDSERVICEEXTRATABLE });

}

public static STEditor.Properties.TMDSERVICEEXTRATABLE MDSERVICEEXTRATABLE_ListSetName(List<STEditor.Properties.TMDSERVICEEXTRATABLE> MDSERVICEEXTRATABLEList, String EXTRATABLE_NAME, Int32 IDMDSERVICETYPE)
{
            for (int i = 0; i < MDSERVICEEXTRATABLEList.length; i++)
{
    if ((IDMDSERVICETYPE == MDSERVICEEXTRATABLEList[i].IDMDSERVICETYPE) && (EXTRATABLE_NAME == MDSERVICEEXTRATABLEList[i].EXTRATABLE_NAME))
        return (MDSERVICEEXTRATABLEList[i]);
}
return null;
}
public static void MDSERVICEEXTRATABLE_ListAdd(List<STEditor.Properties.TMDSERVICEEXTRATABLE> MDSERVICEEXTRATABLEList, STEditor.Properties.TMDSERVICEEXTRATABLE MDSERVICEEXTRATABLE)
{
            Int32 i = MDSERVICEEXTRATABLE_ListGetIndex(MDSERVICEEXTRATABLEList, MDSERVICEEXTRATABLE);
if (i == -1) MDSERVICEEXTRATABLEList.push(MDSERVICEEXTRATABLE);
else MDSERVICEEXTRATABLEList[i] = MDSERVICEEXTRATABLE;

}
public static Int32 MDSERVICEEXTRATABLE_ListGetIndex(List<STEditor.Properties.TMDSERVICEEXTRATABLE> MDSERVICEEXTRATABLEList, STEditor.Properties.TMDSERVICEEXTRATABLE MDSERVICEEXTRATABLE)
{
            for (int i = 0; i < MDSERVICEEXTRATABLEList.length; i++)
{
    if (MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE == MDSERVICEEXTRATABLEList[i].IDMDSERVICEEXTRATABLE)
        return (i);

}
return (-1);

}
public static void MDSERVICEEXTRATABLE_ListFill(String StrIDMDSERVICETYPE, List<STEditor.Properties.TMDSERVICEEXTRATABLE> MDSERVICEEXTRATABLEList, List<STEditor.Properties.TMDSERVICEEXTRAFIELDS> MDSERVICEEXTRAFIELDSList)
{
    SysCfg.Stream.Properties.TParam Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_ENABLED.FieldName, UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_ENABLED.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_NAME.FieldName, UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_NAME.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_NAMETABLE.FieldName, UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_NAMETABLE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);                
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.IDMDSERVICETYPE.FieldName, UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.IDMDSERVICETYPE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);       
        if (StrIDMDSERVICETYPE == "")
        {
            Int32 IDCMDBCIDEFINE = 0;

            Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE.FieldName, IDCMDBCIDEFINE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Normal);
        }
        else
        {
            StrIDMDSERVICETYPE = " IN (" + StrIDMDSERVICETYPE + ")";
            Param.AddUnknown(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE.FieldName, StrIDMDSERVICETYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        }
                
        //*********** CMDBCIDEFINEEXTRATABLE_GET ***************************** 
        var DS_MDSERVICEEXTRATABLE = SysCfg.DB.SQL.Methods.OpenDataSet(@"Atis", @"MDSERVICEEXTRATABLE_GET", Param.ToBytes());
        if (DS_MDSERVICEEXTRATABLE.ResErr.NotError)
        {
            if (DS_MDSERVICEEXTRATABLE.DataSet.RecordCount > 0)
            {
                DS_MDSERVICEEXTRATABLE.DataSet.First();

                while (!(DS_MDSERVICEEXTRATABLE.DataSet.Eof))
                {
                    STEditor.Properties.TMDSERVICEEXTRATABLE UnCIDEFINEEXTRATABLE = MDSERVICEEXTRATABLE_ListSetId(MDSERVICEEXTRATABLEList, DS_MDSERVICEEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE.FieldName).asInt32());//new Properties.TCMDBCIDEFINEEXTRATABLE();
                    UnCIDEFINEEXTRATABLE.ResErr = DS_MDSERVICEEXTRATABLE.ResErr;
                    UnCIDEFINEEXTRATABLE.EXTRATABLE_DESCRIPTION = DS_MDSERVICEEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_DESCRIPTION.FieldName).asString();
                    UnCIDEFINEEXTRATABLE.EXTRATABLE_ENABLED = DS_MDSERVICEEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_ENABLED.FieldName).asBoolean();
                    UnCIDEFINEEXTRATABLE.EXTRATABLE_NAME = DS_MDSERVICEEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_NAME.FieldName).asString();
                    UnCIDEFINEEXTRATABLE.EXTRATABLE_NAMETABLE = DS_MDSERVICEEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_NAMETABLE.FieldName).asString();
                    UnCIDEFINEEXTRATABLE.EXTRATABLE_IDSOURCE =(UsrCfg.SD.Properties.TMDSERVICEEXTRATABLE_SOURCETYPE)DS_MDSERVICEEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_IDSOURCE.FieldName).asInt32();
                    UnCIDEFINEEXTRATABLE.EXTRATABLE_LOG = DS_MDSERVICEEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.EXTRATABLE_LOG.FieldName).asBoolean();
                    UnCIDEFINEEXTRATABLE.IDMDSERVICETYPE = DS_MDSERVICEEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.IDMDSERVICETYPE.FieldName).asInt32();
                    UnCIDEFINEEXTRATABLE.IDMDSERVICEEXTRATABLE = DS_MDSERVICEEXTRATABLE.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE.FieldName).asInt32();
                    //MDSERVICEEXTRATABLE_ListAdd(MDSERVICETYPE.MDSERVICEEXTRATABLElist, UnCIDEFINEEXTRATABLE);
                    MDSERVICEEXTRATABLE_ListAdd(MDSERVICEEXTRATABLEList, UnCIDEFINEEXTRATABLE);
                    DS_MDSERVICEEXTRATABLE.DataSet.Next();
                }
                MDSERVICEEXTRAFIELDS_ListFill(MDSERVICEEXTRATABLEList, MDSERVICEEXTRAFIELDSList);
            }
            else
            {
                DS_MDSERVICEEXTRATABLE.ResErr.NotError = false;
                DS_MDSERVICEEXTRATABLE.ResErr.Mesaje = "Record Count = 0";
            }
        }

    }
    finally
    {
        Param.Destroy();
    }
}





//MDSERVICEEXTRAFIELDS
public static STEditor.Properties.TMDSERVICEEXTRAFIELDS MDSERVICEEXTRAFIELDS_ListSetID(List<STEditor.Properties.TMDSERVICEEXTRAFIELDS> MDSERVICEEXTRAFIELDSList, Int32 IDMDSERVICEEXTRAFIELDS)
{
            for (int i = 0; i < MDSERVICEEXTRAFIELDSList.length; i++)
{
    if (IDMDSERVICEEXTRAFIELDS == MDSERVICEEXTRAFIELDSList[i].IDMDSERVICEEXTRAFIELDS)
        return (MDSERVICEEXTRAFIELDSList[i]);

}
return (new STEditor.Properties.TMDSERVICEEXTRAFIELDS { IDMDSERVICEEXTRAFIELDS = IDMDSERVICEEXTRAFIELDS });
}
public static void MDSERVICEEXTRAFIELDS_ListAdd(List<STEditor.Properties.TMDSERVICEEXTRAFIELDS> MDSERVICEEXTRAFIELDSList, STEditor.Properties.TMDSERVICEEXTRAFIELDS MDSERVICEEXTRAFIELDS)
{
            Int32 i = MDSERVICEEXTRAFIELDS_ListGetIndex(MDSERVICEEXTRAFIELDSList, MDSERVICEEXTRAFIELDS);
if (i == -1) MDSERVICEEXTRAFIELDSList.push(MDSERVICEEXTRAFIELDS);
else MDSERVICEEXTRAFIELDSList[i] = MDSERVICEEXTRAFIELDS;
}
public static Int32 MDSERVICEEXTRAFIELDS_ListGetIndex(List<STEditor.Properties.TMDSERVICEEXTRAFIELDS> MDSERVICEEXTRAFIELDSList, Int32 IDMDSERVICEEXTRAFIELDS)
{
            for (int i = 0; i < MDSERVICEEXTRAFIELDSList.length; i++)
{
    if (IDMDSERVICEEXTRAFIELDS == MDSERVICEEXTRAFIELDSList[i].IDMDSERVICEEXTRAFIELDS)
        return (i);

}
return (-1);
}        
        
public static Int32 MDSERVICEEXTRAFIELDS_ListGetIndex(List<STEditor.Properties.TMDSERVICEEXTRAFIELDS> MDSERVICEEXTRAFIELDSList, STEditor.Properties.TMDSERVICEEXTRAFIELDS MDSERVICEEXTRAFIELDS)
{
            for (int i = 0; i < MDSERVICEEXTRAFIELDSList.length; i++)
{
    if (MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS == MDSERVICEEXTRAFIELDSList[i].IDMDSERVICEEXTRAFIELDS)
        return (i);

}
return (-1);
}
public static void MDSERVICEEXTRAFIELDS_ListFill(List<STEditor.Properties.TMDSERVICEEXTRATABLE> MDSERVICEEXTRATABLEList, List<STEditor.Properties.TMDSERVICEEXTRAFIELDS> MDSERVICEEXTRAFIELDSList)
{
    SysCfg.Stream.Properties.TParam Param = new SysCfg.Stream.Properties.TParam();
    try
    {
        Param.Inicialize();
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME.FieldName, UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICETYPE.FieldName, UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICETYPE.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS.FieldName, UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS.FieldName, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        String StrIDCMDBCIDEFINEEXTRATABLE = "";
        for (int i = 0; i < MDSERVICEEXTRATABLEList.length; i++)
        {
            if (StrIDCMDBCIDEFINEEXTRATABLE == "") StrIDCMDBCIDEFINEEXTRATABLE = MDSERVICEEXTRATABLEList[i].IDMDSERVICEEXTRATABLE.toString();
        else StrIDCMDBCIDEFINEEXTRATABLE += "," + MDSERVICEEXTRATABLEList[i].IDMDSERVICEEXTRATABLE.toString();
        }
    if (StrIDCMDBCIDEFINEEXTRATABLE == "")
    {
        Int32 IDCMDBCIDEFINEEXTRATABLE = 0;
        Param.AddInt32(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRATABLE.FieldName, IDCMDBCIDEFINEEXTRATABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.Equal, SysCfg.DB.Properties.TStyle.Normal);
    }
    else
    {
        StrIDCMDBCIDEFINEEXTRATABLE = " IN (" + StrIDCMDBCIDEFINEEXTRATABLE + ")";
        Param.AddUnknown(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRATABLE.FieldName, StrIDCMDBCIDEFINEEXTRATABLE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    }
    //*********** CMDBCIDEFINEEXTRAFIELDS_GET ***************************** 
    var DS_MDSERVICEEXTRAFIELDS = SysCfg.DB.SQL.Methods.OpenDataSet(@"Atis", @"MDSERVICEEXTRAFIELDS_GET", Param.ToBytes());
    if (DS_MDSERVICEEXTRAFIELDS.ResErr.NotError)
    {
        if (DS_MDSERVICEEXTRAFIELDS.DataSet.RecordCount > 0)
        {
            DS_MDSERVICEEXTRAFIELDS.DataSet.First();
            while (!(DS_MDSERVICEEXTRAFIELDS.DataSet.Eof))
            {
                STEditor.Properties.TMDSERVICEEXTRAFIELDS UnCIDEFINEEXTRAFIELDS = MDSERVICEEXTRAFIELDS_ListSetID(MDSERVICEEXTRAFIELDSList, DS_MDSERVICEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS.FieldName).asInt32());//new Properties.TCMDBCIDEFINEEXTRAFIELDS();
                UnCIDEFINEEXTRAFIELDS.ResErr = DS_MDSERVICEEXTRAFIELDS.ResErr;
                UnCIDEFINEEXTRAFIELDS.DBDATATYPES_SIZE = DS_MDSERVICEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.DBDATATYPES_SIZE.FieldName).asInt32();
                UnCIDEFINEEXTRAFIELDS.DBKEYTYPE_NAME = DS_MDSERVICEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.DBKEYTYPE_NAME.FieldName).asString();
                UnCIDEFINEEXTRAFIELDS.EXTRAFIELDS_DESCRIPTION = DS_MDSERVICEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_DESCRIPTION.FieldName).asString();
                UnCIDEFINEEXTRAFIELDS.EXTRAFIELDS_LOOKUPDISPLAY = DS_MDSERVICEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPDISPLAY.FieldName).asString();
                UnCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE = DS_MDSERVICEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE.FieldName).asInt32();
                UnCIDEFINEEXTRAFIELDS.EXTRAFIELDS_LOOKUPID = DS_MDSERVICEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPID.FieldName).asString();
                UnCIDEFINEEXTRAFIELDS.EXTRAFIELDS_LOOKUPSQL = DS_MDSERVICEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_LOOKUPSQL.FieldName).asString();
                UnCIDEFINEEXTRAFIELDS.EXTRAFIELDS_NAME = DS_MDSERVICEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME.FieldName).asString();
                UnCIDEFINEEXTRAFIELDS.IDMDSERVICETYPE = DS_MDSERVICEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICETYPE.FieldName).asInt32();
                UnCIDEFINEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS = DS_MDSERVICEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS.FieldName).asInt32();
                UnCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRATABLE = DS_MDSERVICEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRATABLE.FieldName).asInt32();
                UnCIDEFINEEXTRAFIELDS.IDCMDBDBDATATYPES = (SysCfg.DB.Properties.TDataType)DS_MDSERVICEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES.FieldName).asInt32();
                UnCIDEFINEEXTRAFIELDS.IDCMDBKEYTYPE = (SysCfg.DB.Properties.TKeyType)DS_MDSERVICEEXTRAFIELDS.DataSet.RecordSet.FieldName(UsrCfg.InternoAtisNames.MDSERVICEEXTRAFIELDS.IDCMDBKEYTYPE.FieldName).asInt32();

                for (int i = 0; i < MDSERVICEEXTRATABLEList.length; i++)
                {
                    if (MDSERVICEEXTRATABLEList[i].IDMDSERVICEEXTRATABLE == UnCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRATABLE)
                {
                        if (!MDSERVICEEXTRATABLEList[i].GRIDENABLE) MDSERVICEEXTRATABLEList[i].GRIDENABLE = (UnCIDEFINEEXTRAFIELDS.IDCMDBKEYTYPE == SysCfg.DB.Properties.TKeyType.ADN);
                        MDSERVICEEXTRAFIELDS_ListAdd(MDSERVICEEXTRATABLEList[i].MDSERVICEEXTRAFIELDSList, UnCIDEFINEEXTRAFIELDS);
                        break;
                }
        }
        MDSERVICEEXTRAFIELDS_ListAdd(MDSERVICEEXTRAFIELDSList, UnCIDEFINEEXTRAFIELDS);
        DS_MDSERVICEEXTRAFIELDS.DataSet.Next();
    }

}
else
{
                        DS_MDSERVICEEXTRAFIELDS.ResErr.NotError = false;
DS_MDSERVICEEXTRAFIELDS.ResErr.Mesaje = "Record Count = 0";
}
}
}
            finally
{
    Param.Destroy();
}
}

public static String GETSQL(List<STEditor.Properties.TMDSERVICEEXTRATABLE> MDSERVICEEXTRATABLEList, String EXTRATABLE_NAME, Int32 IDMDSERVICETYPE)
{            
            STEditor.Properties.TMDSERVICEEXTRATABLE CIDEFINEEXTRATABLE = MDSERVICEEXTRATABLE_ListSetName(MDSERVICEEXTRATABLEList, EXTRATABLE_NAME,IDMDSERVICETYPE);
return "";
}
public static SysCfg.DB.TSQL BuildSQL(STEditor.Properties.TMDSERVICEEXTRATABLE MDSERVICEEXTRATABLE, Int32 IDSDCASEMT, STEditor.Properties.TSQLResult SQLResult)
{
            



            SysCfg.DB.TSQL SQL = new SysCfg.DB.TSQL();
SysCfg.Error.Properties.ResErrfill(ref SQL.ResErr);
String PrefixTable = STEditor.Properties.PrefixTable + MDSERVICEEXTRATABLE.EXTRATABLE_NAMETABLE;
String PrefixTableLog = PrefixTable + SysCfg.Interno.Properties.PrefixSysLog;

String PrefixIDField = STEditor.Properties.PrefixIDField + MDSERVICEEXTRATABLE.EXTRATABLE_NAMETABLE;
String PrefixIDSDCASEMT = MDSERVICEEXTRATABLE.EXTRATABLE_IDSOURCE.GetName(); //STEditor.Properties.PrefixIDSDCASEMT;
Boolean PrefixLOG = MDSERVICEEXTRATABLE.EXTRATABLE_LOG; 

String Aux0 = "";
String Aux1 = "," + PrefixIDField;
String Aux2 = " and " + PrefixIDField + "=@[" + PrefixIDField + "]";
String Aux3 = "";
String Aux4 = "";
String Aux5 = PrefixIDSDCASEMT;
String Log0 = "," + SysCfg.Interno.Properties.FieldName_Date
            + "," + SysCfg.Interno.Properties.FieldName_Source_SysLog
            + "," + SysCfg.Interno.Properties.FieldName_Event_SysLog
            + "," + SysCfg.Interno.Properties.FieldName_IDCMDBCI_SysLog;
String Log1 = ",@[" + SysCfg.Interno.Properties.FieldName_Date + "]"
            + ",@[" + SysCfg.Interno.Properties.FieldName_Source_SysLog + "]"
            + ",@[" + SysCfg.Interno.Properties.FieldName_Event_SysLog + "]"
            + ",@[" + SysCfg.Interno.Properties.FieldName_IDCMDBCI_SysLog + "]";
String Log2 =" , " + SysCfg.Interno.Properties.FieldName_Date + "=@[" + SysCfg.Interno.Properties.FieldName_Date + "]"
           + " , " + SysCfg.Interno.Properties.FieldName_Source_SysLog + "=@[" + SysCfg.Interno.Properties.FieldName_Source_SysLog + "]"
           + " , " + SysCfg.Interno.Properties.FieldName_Event_SysLog + "=@[" + SysCfg.Interno.Properties.FieldName_Event_SysLog + "]"
           + " , " + SysCfg.Interno.Properties.FieldName_IDCMDBCI_SysLog + "=@[" + SysCfg.Interno.Properties.FieldName_IDCMDBCI_SysLog + "]";

for (int Contfield = 0; Contfield < MDSERVICEEXTRATABLE.MDSERVICEEXTRAFIELDSList.length; Contfield++)
{
    Aux0 += "," + MDSERVICEEXTRATABLE.MDSERVICEEXTRAFIELDSList[Contfield].EXTRAFIELDS_NAME;
    Aux1 += "," + MDSERVICEEXTRATABLE.MDSERVICEEXTRAFIELDSList[Contfield].EXTRAFIELDS_NAME;
    if (MDSERVICEEXTRATABLE.MDSERVICEEXTRAFIELDSList[Contfield].IDCMDBKEYTYPE == SysCfg.DB.Properties.TKeyType.ADN)
{
    //para poder deseñpeñar old y new valios se le agraga el prefijo *1
    //*1 Aux2 += " and " + CIDEFINEEXTRATABLE.CMDBCIDEFINEEXTRAFIELDSList[Contfield].EXTRAFIELDS_NAME + "=@[" + CIDEFINEEXTRATABLE.CMDBCIDEFINEEXTRAFIELDSList[Contfield].EXTRAFIELDS_NAME + "_OLD]";
        Aux2 += " and " + MDSERVICEEXTRATABLE.MDSERVICEEXTRAFIELDSList[Contfield].EXTRAFIELDS_NAME + "=@[" + MDSERVICEEXTRATABLE.MDSERVICEEXTRAFIELDSList[Contfield].EXTRAFIELDS_NAME + "]";
}
    //*1 Aux3 += ",@[" + CIDEFINEEXTRATABLE.CMDBCIDEFINEEXTRAFIELDSList[Contfield].EXTRAFIELDS_NAME + "_NEW]";
    Aux3 += ",@[" + MDSERVICEEXTRATABLE.MDSERVICEEXTRAFIELDSList[Contfield].EXTRAFIELDS_NAME + "]";
    //*1 Aux4 += "," + CIDEFINEEXTRATABLE.CMDBCIDEFINEEXTRAFIELDSList[Contfield].EXTRAFIELDS_NAME + "=@[" + CIDEFINEEXTRATABLE.CMDBCIDEFINEEXTRAFIELDSList[Contfield].EXTRAFIELDS_NAME + "_NEW]";
    Aux4 += "," + MDSERVICEEXTRATABLE.MDSERVICEEXTRAFIELDSList[Contfield].EXTRAFIELDS_NAME + "=@[" + MDSERVICEEXTRATABLE.MDSERVICEEXTRAFIELDSList[Contfield].EXTRAFIELDS_NAME + "]";
}
switch (SQLResult)
{
    case Properties.TSQLResult.Select:
        SQL.StrSql = "SELECT " + Aux5 + Aux1 + " FROM " + PrefixTable + " WHERE " + Aux5 + "=" + IDSDCASEMT;
        SQL.StrLog = "SELECT " + Aux5 + Log0 + Aux1 + " FROM " + PrefixTableLog + " WHERE " + Aux5 + "=" + IDSDCASEMT;
        break;
    case Properties.TSQLResult.SelectID:
        SQL.StrSql = "SELECT " + Aux5 + Aux1 + " FROM " + PrefixTable + " WHERE " + Aux5 + "=" + IDSDCASEMT + Aux2;
        SQL.StrLog = "SELECT " + Aux5 + Log0 + Aux1 + " FROM " + PrefixTableLog + " WHERE " + Aux5 + "=" + IDSDCASEMT + Aux2;
        break;
    case Properties.TSQLResult.Insert:
        SQL.StrSql = "INSERT INTO " + PrefixTable + "(" + Aux5 + Aux0 + ") VALUES (" + IDSDCASEMT + Aux3 + ")";
        SQL.StrLog = "INSERT INTO " + PrefixTableLog + "(" + Aux5 + Log0 + Aux0 + ") VALUES (" + IDSDCASEMT + Log1 + Aux3 + ")";
        break;
    case Properties.TSQLResult.Update:
    //*1 SQL.StrSql = "UPDATE " + PrefixTable + " SET " + PrefixIDCI + "=" + CI.IDCMDBCI + Aux4 + " WHERE  " + PrefixIDCI + "=" + CI.IDCMDBCI + Aux2;
        SQL.StrSql = "UPDATE " + PrefixTable + " SET " + Aux5 + "=" + IDSDCASEMT + Aux4 + " WHERE  " + Aux5 + "=" + IDSDCASEMT + " and " + PrefixIDField + "=@[" + PrefixIDField + "]";
        SQL.StrLog = "INSERT INTO " + PrefixTableLog + "(" + Aux5 + Log0 + Aux0 + ") VALUES (" + IDSDCASEMT + Log1 + Aux3 + ")";
    //SQL.StrLog = "UPDATE " + PrefixTableLog + " SET " + Aux5 + "=" + IDSDCASEMT + Log2 + Aux4 + " WHERE  " + Aux5 + "=" + IDSDCASEMT + " and " + PrefixIDField + "=@[" + PrefixIDField + "]";
        break;
    case Properties.TSQLResult.Delete:
    //*1 SQL.StrSql = "DELETE " + PrefixTable + " WHERE  " + PrefixIDCI + "=" + CI.IDCMDBCI + Aux2;
        SQL.StrSql = "DELETE " + PrefixTable + " WHERE  " + Aux5 + "=" + IDSDCASEMT + " and " + PrefixIDField + "=@[" + PrefixIDField + "]";
        SQL.StrLog = "INSERT INTO " + PrefixTableLog + "(" + Aux5 + Log0 + Aux0 + ") VALUES (" + IDSDCASEMT + Log1 + Aux3 + ")";
    //SQL.StrLog = "DELETE " + PrefixTableLog + " WHERE  " + Aux5 + "=" + IDSDCASEMT + " and " + PrefixIDField + "=@[" + PrefixIDField + "]";
        break;
}
return (SQL);
}

*/