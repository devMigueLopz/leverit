ItHelpCenter.TForumsManager.ForumsBasic.prototype.InitializeComponent = function () {
    var _this = this.TParent();
    try {
        switch (_this.CurrentPage) {
            case _this.EnumPage.Index:
                _this.ComponentPage();
                _this.ComponentHeader();
                _this.ComponentBody();
                _this.ComponentFooter();
                _this.ComponentConfigurateForum();
                _this.ComponentStyleMain();
                $("#" + (_this.IdTable)).dataTable({ responsive: true });
                _this.ComponentEventMain();
                break;
            case _this.EnumPage.ForumCategory:
                _this.ComponentPage();
                _this.ComponentHeader();
                _this.ComponentBody();
                _this.ComponentFooter();
                _this.ComponentConfigurateForumCategory();
                _this.ComponentStyleMain();
                $("#" + (_this.IdTable)).dataTable({ responsive: true });
                _this.ComponentEventMain();
                break;
            case _this.EnumPage.Topic:
                _this.ComponentPage();
                _this.ComponentHeader();
                _this.ComponentSearch();
                _this.ComponentBody();
                _this.ComponentFooter();
                _this.ComponentConfigurateTopic();
                _this.ComponentStyleMain();
                $("#" + (_this.IdTable)).dataTable({ responsive: true, ordering:false });
                _this.ComponentEventMain();
                break;
            case _this.EnumPage.Response:
                _this.ComponentPage();
                _this.LoadFileExternComponent();
                _this.ComponentHeader();
                _this.ComponentResponse();
                _this.ComponentFooter();
                _this.ComponentConfigurateResponse(false);
                _this.ComponentAditionalConfigurateResponse();
                _this.ComponentStyleMain();
                _this.ComponentEventMain();
                break;
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.InitializeComponent", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentPage = function () {
    var _this = this.TParent();
    try {
        _this.Elements.BoxMain_ForumIndex = new TVclStackPanel(_this.ObjectHtml, ("BoxMain" + "_ForumIndex"), 1, [[12], [12], [12], [12], [12]]);
        _this.Elements.BoxForum_ForumIndex = new TVclStackPanel(_this.Elements.BoxMain_ForumIndex.Column[0].This, ("BoxForum" + "_ForumIndex"), 1, [[12], [12], [12], [12], [12]]);
        
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentPage", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentHeader = function () {
    var _this = this.TParent();
    try {
        _this.Elements.BoxHeader_ForumIndex = new TVclStackPanel(_this.Elements.BoxForum_ForumIndex.Column[0].This, ("BoxHeader" + "_ForumIndex"), 1, [[12], [12], [12], [12], [12]]);
        _this.Elements.BoxNavHeader_ForumIndex = new TVclStackPanel(_this.Elements.BoxHeader_ForumIndex.Column[0].This, ("BoxNavHeader" + "_ForumIndex"), 1, [[12], [12], [12], [12], [12]]);
        _this.Elements.olNav_ForumIndex = new Uol(_this.Elements.BoxNavHeader_ForumIndex.Column[0].This, ("olNav" + "_ForumIndex"));
        $(_this.Elements.olNav_ForumIndex).addClass("breadcrumb");
        _this.Elements.liForumOlNav_ForumIndex = new Uli(_this.Elements.olNav_ForumIndex, ("liForumOlNav" + "_ForumIndex"));

        _this.Elements.textTitleLiForumOlNav_ForumIndex = new Ua(_this.Elements.liForumOlNav_ForumIndex, ("textTitleLiForumOlNav" + "_ForumIndex"), UsrCfg.Traslate.GetLangText(_this.Mythis, "FORUM"));
        $(_this.Elements.textTitleLiForumOlNav_ForumIndex).addClass("textPointer redirectPage");
        $(_this.Elements.textTitleLiForumOlNav_ForumIndex).data("EnumPage", JSON.stringify(_this.EnumPage.Index));
        _this.Elements.iconTitleLiForumOlNav_ForumIndex = new Uspan("", ("iconTitleLiForumOlNav_ForumIndex" + "_ForumIndex"), "");
        $(_this.Elements.iconTitleLiForumOlNav_ForumIndex).addClass("glyphicon glyphicon-home iconSpacing");
        $(_this.Elements.textTitleLiForumOlNav_ForumIndex).prepend(_this.Elements.iconTitleLiForumOlNav_ForumIndex);

        _this.Elements.liCategoryOlNav_ForumIndex = new Uli(_this.Elements.olNav_ForumIndex, ("liCategoryOlNav" + "_ForumIndex"));
        _this.Elements.textTitleLiCategoryOlNav_ForumIndex = new Ua(_this.Elements.liCategoryOlNav_ForumIndex, ("textTitleLiCategoryOlNav" + "_ForumIndex"), UsrCfg.Traslate.GetLangText(_this.Mythis, "FORUMCATEGORY"));
        $(_this.Elements.textTitleLiCategoryOlNav_ForumIndex).addClass("textPointer redirectPage");
        $(_this.Elements.textTitleLiCategoryOlNav_ForumIndex).data("EnumPage", JSON.stringify(_this.EnumPage.ForumCategory));
        _this.Elements.iconTitleLiCategoryOlNav_ForumIndex = new Uspan("", ("iconTitleLiCategoryOlNav_ForumIndex" + "_ForumIndex"), "");
        $(_this.Elements.iconTitleLiCategoryOlNav_ForumIndex).addClass("glyphicon glyphicon-list-alt iconSpacing");
        $(_this.Elements.textTitleLiCategoryOlNav_ForumIndex).prepend(_this.Elements.iconTitleLiCategoryOlNav_ForumIndex);

        _this.Elements.liTopiclNav_ForumIndex = new Uli(_this.Elements.olNav_ForumIndex, ("liTopiclNav" + "_ForumIndex"));
        _this.Elements.textTitleLiTopiclNav_ForumIndex = new Ua(_this.Elements.liTopiclNav_ForumIndex, ("textTitleLiTopiclNav" + "_ForumIndex"), UsrCfg.Traslate.GetLangText(_this.Mythis, "TOPIC"));
        $(_this.Elements.textTitleLiTopiclNav_ForumIndex).addClass("textPointer redirectPage");
        $(_this.Elements.textTitleLiTopiclNav_ForumIndex).data("EnumPage", JSON.stringify(_this.EnumPage.Topic));
        _this.Elements.iconTitleLiTopiclNav_ForumIndex = new Uspan("", ("iconTitleLiTopiclNav_ForumIndex" + "_ForumIndex"), "");
        $(_this.Elements.iconTitleLiTopiclNav_ForumIndex).addClass("glyphicon glyphicon-file iconSpacing");
        $(_this.Elements.textTitleLiTopiclNav_ForumIndex).prepend(_this.Elements.iconTitleLiTopiclNav_ForumIndex);

        _this.Elements.liResponse_ForumIndex = new Uli(_this.Elements.olNav_ForumIndex, ("liResponse" + "_ForumIndex"));
        _this.Elements.textTitleLiResponse_ForumIndex = new Ua(_this.Elements.liResponse_ForumIndex, ("textTitleLiResponse" + "_ForumIndex"), UsrCfg.Traslate.GetLangText(_this.Mythis, "TOPICRESPONSE"))
        $(_this.Elements.textTitleLiResponse_ForumIndex).addClass("textPointer redirectPage");
        $(_this.Elements.textTitleLiResponse_ForumIndex).data("EnumPage", JSON.stringify(_this.EnumPage.Response));
        _this.Elements.iconTitleLiResponse_ForumIndex = new Uspan("", ("iconTitleLiResponse_ForumIndex" + "_ForumIndex"), "");
        $(_this.Elements.iconTitleLiResponse_ForumIndex).addClass("glyphicon glyphicon-user iconSpacing");
        $(_this.Elements.textTitleLiResponse_ForumIndex).prepend(_this.Elements.iconTitleLiResponse_ForumIndex);

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentHeader", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentSearch = function () {
    var _this = this.TParent();
    try {
        _this.Elements.BoxHeaderSearch_ForumIndex = new TVclStackPanel(_this.Elements.BoxForum_ForumIndex.Column[0].This, ("BoxHeaderSearch_ForumIndex"), 1, [[12], [12], [12], [12], [12]]);
        var dataWork = _this.DataWork.ForumsSearch;
        _this.Elements.DynamicElements.BtnSearch = new Array();
        for (var i = 0; i < dataWork.length; i++) {
            var nowDate = new Date().getTime().toString();
            var BtnSearch_ForumIndex = new TVclButton(_this.Elements.BoxHeaderSearch_ForumIndex.Column[0].This, ("BtnSearch_ForumIndex" + i + "_" + nowDate));
            BtnSearch_ForumIndex.VCLType = TVCLType.BS;
            BtnSearch_ForumIndex.Text = dataWork[i].NAMEFORUMSSEARCH;
            BtnSearch_ForumIndex.Src = dataWork[i].IMAGEFORUMSSEARCH;
            BtnSearch_ForumIndex.Tag = dataWork[i];
            _this.Elements.DynamicElements.BtnSearch.push(BtnSearch_ForumIndex);
        }

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentSearch", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentBody = function () {
    var _this = this.TParent();
    try {
        _this.Elements.BoxBody_ForumIndex = new TVclStackPanel(_this.Elements.BoxForum_ForumIndex.Column[0].This, ("BoxBody" + "_ForumIndex"), 1, [[12], [12], [12], [12], [12]]);
        _this.Elements.BoxMainBody_ForumIndex = new TVclStackPanel(_this.Elements.BoxBody_ForumIndex.Column[0].This, ("BoxMainBody" + "_ForumIndex"), 1, [[12], [12], [12], [12], [12]]);
        _this.Elements.panelTableBody_ForumIndex = new Udiv(_this.Elements.BoxMainBody_ForumIndex.Column[0].This, ("panelTableBody" + "_ForumIndex"));
        //TABLE
        $(_this.Elements.panelTableBody_ForumIndex).addClass("panel panel-default");
        _this.Elements.tableBody_ForumIndex = new Utable(_this.Elements.panelTableBody_ForumIndex, _this.IdTable);
        $(_this.Elements.tableBody_ForumIndex).addClass("footable table table-striped table-bordered table-white table-primary table-hover default footable-loaded")
        _this.Elements.tableTheadBody_ForumIndex = new Uthead(_this.Elements.tableBody_ForumIndex, ("tableTheadBody" + "_ForumIndex"));
        _this.Elements.tableTrBody_ForumIndex = new Utr(_this.Elements.tableTheadBody_ForumIndex, ("tableTrBody" + "_ForumIndex"));
        //HEAD TABLE
        _this.Elements.titletableThBody_ForumIndex = new Uth(_this.Elements.tableTrBody_ForumIndex, ("titletableThBody" + "_ForumIndex"));
        _this.Elements.boxTextTitleThBody_ForumIndex = new TVclStackPanel(_this.Elements.titletableThBody_ForumIndex, ("boxTextTitleThBody" + "_ForumIndex"), 1, [[12], [12], [12], [12], [12]]);
        _this.Elements.textTitleThBody_ForumIndex = new Uspan(_this.Elements.boxTextTitleThBody_ForumIndex.Column[0].This, ("textTitleThBody" + "_ForumIndex"), UsrCfg.Traslate.GetLangText(_this.Mythis, "FORUMSLEVERIT"));
        $(_this.Elements.textTitleThBody_ForumIndex).addClass("blockCenter");

        _this.Elements.topicTableThBody_ForumIndex = new Uth(_this.Elements.tableTrBody_ForumIndex, ("topicTableThBody" + "_ForumIndex"));
        _this.Elements.boxTextTopicThBody_ForumIndex = new TVclStackPanel(_this.Elements.topicTableThBody_ForumIndex, ("boxTextTopicThBody" + "_ForumIndex"), 1, [[12], [12], [12], [12], [12]]);
        _this.Elements.textTopicThBody_ForumIndex = new Uspan(_this.Elements.boxTextTopicThBody_ForumIndex.Column[0].This, ("textTopicThBody" + "_ForumIndex"), UsrCfg.Traslate.GetLangText(_this.Mythis, "TOPIC"));
        $(_this.Elements.textTopicThBody_ForumIndex).addClass("blockCenter");

        _this.Elements.responseTableThBody_ForumIndex = new Uth(_this.Elements.tableTrBody_ForumIndex, ("responseTableThBody" + "_ForumIndex"));
        _this.Elements.boxTextResponseThBody_ForumIndex = new TVclStackPanel(_this.Elements.responseTableThBody_ForumIndex, ("boxTextResponseThBody" + "_ForumIndex"), 1, [[12], [12], [12], [12], [12]]);
        _this.Elements.textResponseThBody_ForumIndex = new Uspan(_this.Elements.boxTextResponseThBody_ForumIndex.Column[0].This, ("textResponseThBody" + "_ForumIndex"), UsrCfg.Traslate.GetLangText(_this.Mythis, "TOPICRESPONSE"));
        $(_this.Elements.textResponseThBody_ForumIndex).addClass("blockCenter");

        _this.Elements.lastPostTableThBody_ForumIndex = new Uth(_this.Elements.tableTrBody_ForumIndex, ("lastPostTableThBody" + "_ForumIndex"));
        _this.Elements.boxTextLastPostTableThBody_ForumIndex = new TVclStackPanel(_this.Elements.lastPostTableThBody_ForumIndex, ("boxTextLastPostTableThBody" + "_ForumIndex"), 1, [[12], [12], [12], [12], [12]]);
        _this.Elements.textLastPostTableThBody_ForumIndex = new Uspan(_this.Elements.boxTextLastPostTableThBody_ForumIndex.Column[0].This, ("textLastPostTableThBody" + "_ForumIndex"), UsrCfg.Traslate.GetLangText(_this.Mythis, "LASTPOST"));
        $(_this.Elements.textLastPostTableThBody_ForumIndex).addClass("blockCenter");

        //END HEAD TABLE
        // BODY TABLE
        _this.Elements.tableContent_ForumIndex = new Utbody(_this.Elements.tableBody_ForumIndex, ("tableContent" + "_ForumIndex"));
        var dataWork = _this.DataWork.DataColumn;
        for (var i = 0; i < dataWork.length; i++) {
            var objRow = new Object();
            objRow.tableTrContent_ForumIndex = new Utr(_this.Elements.tableContent_ForumIndex, ("tableTrContent" + "_ForumIndex"+i));
            objRow.sumarytableTdContent_ForumIndex = new Utd(objRow.tableTrContent_ForumIndex, ("sumarytableTdContent" + "_ForumIndex_"+i));
            $(objRow.sumarytableTdContent_ForumIndex).addClass("col-xs-12 col-md-7");
            objRow.boxSumarytableTdContent_ForumIndex = new TVclStackPanel(objRow.sumarytableTdContent_ForumIndex, ("boxSumarytableTdContent" + "_ForumIndex_"+i), 1, [[12], [12], [12], [12], [12]]);
            objRow.boxMediaSumarytableTdContent_ForumIndex = new Udiv(objRow.boxSumarytableTdContent_ForumIndex.Column[0].This, ("boxMediaSumarytableTdContent" + "_ForumIndex_"+i));
            $(objRow.boxMediaSumarytableTdContent_ForumIndex).addClass("media");
            objRow.boxMediaImageSumarytableTdContent_ForumIndex = new Udiv(objRow.boxMediaSumarytableTdContent_ForumIndex, ("boxMediaImageSumarytableTdContent") + "_ForumIndex_"+i);
            $(objRow.boxMediaImageSumarytableTdContent_ForumIndex).addClass("media-left media-middle");
            objRow.imageSumarytableTdContent_ForumIndex = new Uimg(objRow.boxMediaImageSumarytableTdContent_ForumIndex, ("ImageSumarytableTdContent" + "_ForumIndex_" + i), dataWork[i].Column1.Image, "");
            $(objRow.imageSumarytableTdContent_ForumIndex).addClass("media-object mediaWidth")
            objRow.boxMediaBodySumarytableTdContent_ForumIndex = new Udiv(objRow.boxMediaSumarytableTdContent_ForumIndex, ("boxMediaBodySumarytableTdContent" + "_ForumIndex_"+i));
            $(objRow.boxMediaBodySumarytableTdContent_ForumIndex).addClass("media-body");
            objRow.linkTitleMediaBodySumarytableTdContent_ForumIndex = new Ua(objRow.boxMediaBodySumarytableTdContent_ForumIndex, ("linkTitleMediaBodySumarytableTdContent" + "_ForumIndex_"+i), "");
            objRow.textTitleMediaBodySumarytableTdContent_ForumIndex = new Uh4(objRow.linkTitleMediaBodySumarytableTdContent_ForumIndex, ("textTitleMediaBodySumarytableTdContent" + "_ForumIndex_" + i), dataWork[i].Column1.Title);
            $(objRow.textTitleMediaBodySumarytableTdContent_ForumIndex).addClass("txtTitleConnection textPointer redirectPage");
            $(objRow.textTitleMediaBodySumarytableTdContent_ForumIndex).data("Id", dataWork[i].Column1.Id);
            $(objRow.textTitleMediaBodySumarytableTdContent_ForumIndex).data("EnumPage", JSON.stringify(dataWork[i].Column1.RedirectPage));

            $(objRow.textTitleMediaBodySumarytableTdContent_ForumIndex).addClass("media-heading");
            objRow.texContentMediaBodySumarytableTdContent_ForumIndex = new Up(objRow.boxMediaBodySumarytableTdContent_ForumIndex, ("textTitleMediaBodySumarytableTdContent" + "_ForumIndex" + i), dataWork[i].Column1.Description);

            objRow.topictableTdContent_ForumIndex = new Utd(objRow.tableTrContent_ForumIndex, ("topictableTdContent" + "_ForumIndex_" + i));
            $(objRow.topictableTdContent_ForumIndex).addClass("col-xs-12 col-md-1");
            objRow.boxTopicstableTdContent_ForumIndex = new TVclStackPanel(objRow.topictableTdContent_ForumIndex, ("boxTopicstableTdContent" + "_ForumIndex_"+i), 1, [[12], [12], [12], [12], [12]]);
            objRow.textTopicstableTdContent_ForumIndex = new Uspan(objRow.boxTopicstableTdContent_ForumIndex.Column[0].This, ("textTopicstableTdContent" + "_ForumIndex_" + i), dataWork[i].Column2);
            $(objRow.textTopicstableTdContent_ForumIndex).addClass("blockCenter")

            objRow.responsetableTdContent_ForumIndex = new Utd(objRow.tableTrContent_ForumIndex, ("responsetableTdContent" + "_ForumIndex_" + i));
            $(objRow.responsetableTdContent_ForumIndex).addClass("col-xs-12 col-md-1");
            objRow.boxResponsetableTdContent_ForumIndex = new TVclStackPanel(objRow.responsetableTdContent_ForumIndex, ("boxResponsetableTdContent" + "_ForumIndex_"+i), 1, [[12], [12], [12], [12], [12]]);
            objRow.textResponsetableTdContent_ForumIndex = new Uspan(objRow.boxResponsetableTdContent_ForumIndex.Column[0].This, ("textResponsetableTdContent" + "_ForumIndex_" + i), dataWork[i].Column3);
            $(objRow.textResponsetableTdContent_ForumIndex).addClass("blockCenter");

            objRow.lastPostTableTdContent_ForumIndex = new Utd(objRow.tableTrContent_ForumIndex, ("lastPostTableTdContent" + "_ForumIndex_" + i));
            $(objRow.lastPostTableTdContent_ForumIndex).addClass("col-xs-12 col-md-3");
            objRow.boxLastPosttableTdContent_ForumIndex = new TVclStackPanel(objRow.lastPostTableTdContent_ForumIndex, ("boxLastPosttableTdContent" + "_ForumIndex_"+i), 1, [[12], [12], [12], [12], [12]]);
            objRow.boxLastPostReTableTdContent_ForumIndex = new Udiv(objRow.boxLastPosttableTdContent_ForumIndex.Column[0].This, ("boxLastPostReTableTdContent" + "_ForumIndex_"+i));
            objRow.TitleLastPostReTableTdContent_ForumIndex = new Uspan(objRow.boxLastPostReTableTdContent_ForumIndex, ("TitleLastPostReTableTdContent" + "_ForumIndex_"+i), UsrCfg.Traslate.GetLangText(_this.Mythis, "RE") + " : ");
            objRow.ValueLastPostReTableTdContent_ForumIndex = new Uspan(objRow.boxLastPostReTableTdContent_ForumIndex, ("ValueLastPostReTableTdContent" + "_ForumIndex_" + i), dataWork[i].Column4.Title);
            objRow.boxLastPostByTableTdContent_ForumIndex = new Udiv(objRow.boxLastPosttableTdContent_ForumIndex.Column[0].This, ("boxLastPostByTableTdContent" + "_ForumIndex_"+i));
            objRow.TitleLastPostByTableTdContent_ForumIndex = new Uspan(objRow.boxLastPostByTableTdContent_ForumIndex, ("TitleLastPostByTableTdContent" + "_ForumIndex_"+i), UsrCfg.Traslate.GetLangText(_this.Mythis, "BY") + " : ");
            objRow.ValueLastPostByTableTdContent_ForumIndex = new Uspan(objRow.boxLastPostByTableTdContent_ForumIndex, ("ValueLastPostByTableTdContent" + "_ForumIndex_" + i), dataWork[i].Column4.Create);
            objRow.boxLastPostDateTableTdContent_ForumIndex = new Udiv(objRow.boxLastPosttableTdContent_ForumIndex.Column[0].This, ("boxLastPostDateTableTdContent" + "_ForumIndex_"+i));
            objRow.TitleLastPostDateTableTdContent_ForumIndex = new Uspan(objRow.boxLastPostDateTableTdContent_ForumIndex, ("TitleLastPostDateTableTdContent" + "_ForumIndex_"+i), UsrCfg.Traslate.GetLangText(_this.Mythis, "DATE") + " : ");
            objRow.ValueLastPostDateTableTdContent_ForumIndex = new Uspan(objRow.boxLastPostDateTableTdContent_ForumIndex, ("ValueLastPostDateTableTdContent" + "_ForumIndex_" + i), dataWork[i].Column4.CreateDate);
            _this.Elements.DynamicElements.push(objRow);
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentBody ", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentFooter = function () {
    var _this = this.TParent();
    try {
        _this.Elements.BoxFooter_ForumIndex = new TVclStackPanel(_this.Elements.BoxForum_ForumIndex.Column[0].This, ("BoxFooter" + "_ForumIndex"), 1, [[12], [12], [12], [12], [12]]);
        if (!(_this.EnumPage.GetEnum(_this.CurrentPage.value) == _this.EnumPage.Response)) {
            _this.Elements.BoxMainBodyFooter_ForumIndex = new TVclStackPanel(_this.Elements.BoxFooter_ForumIndex.Column[0].This, ("BoxMainBodyFooter" + "_ForumIndex"), 1, [[12], [12], [12], [12], [12]]);
            $(_this.Elements.BoxMainBodyFooter_ForumIndex).addClass("mainBodyFooter")
            _this.Elements.BoxWhoIsOnlineFooter_ForumIndex = new TVclStackPanel(_this.Elements.BoxMainBodyFooter_ForumIndex.Column[0].This, ("BoxWhoIsOnlineFooter" + "_ForumIndex"), 1, [[12], [12], [12], [12], [12]]);
            _this.Elements.titleWhoIsOnlineFooter_ForumIndex = new Uh3(_this.Elements.BoxWhoIsOnlineFooter_ForumIndex.Column[0].This, ("titleWhoIsOnlineFooter" + "_ForumIndex"), UsrCfg.Traslate.GetLangText(_this.Mythis, "WHOISONLINE"));
            $(_this.Elements.titleWhoIsOnlineFooter_ForumIndex).addClass("optionsFooter")
            _this.Elements.boxTextWhoIsOnlineUserOnline_ForumIndex = new Udiv(_this.Elements.BoxWhoIsOnlineFooter_ForumIndex.Column[0].This, ("boxTextWhoIsOnlineUserOnline" + "_ForumIndex"));
            _this.Elements.textWhoIsOnlineUserOnline_ForumIndex = new Uspan(_this.Elements.boxTextWhoIsOnlineUserOnline_ForumIndex, ("textWhoIsOnlineFooter" + "_ForumIndex"), UsrCfg.Traslate.GetLangText(_this.Mythis, "USERONLINE") + " : ");
            _this.Elements.countWhoIsOnlineUserOnline_ForumIndex = new Uspan(_this.Elements.boxTextWhoIsOnlineUserOnline_ForumIndex, ("textWhoIsOnlineFooter" + "_ForumIndex"), "0");
            _this.Elements.boxTextWhoIsOnlineUserRegistered_ForumIndex = new Udiv(_this.Elements.BoxWhoIsOnlineFooter_ForumIndex.Column[0].This, ("boxTextWhoIsOnlineUserRegistered" + "_ForumIndex"));
            _this.Elements.textWhoIsOnlineUserRegistered_ForumIndex = new Uspan(_this.Elements.boxTextWhoIsOnlineUserRegistered_ForumIndex, ("textWhoIsOnlineUserRegistered" + "_ForumIndex"), UsrCfg.Traslate.GetLangText(_this.Mythis, "USERREGISTERED") + " : ");
            _this.Elements.countWhoIsOnlineUserRegistered_ForumIndex = new Uspan(_this.Elements.boxTextWhoIsOnlineUserRegistered_ForumIndex, ("countWhoIsOnlineUserRegistered" + "_ForumIndex"), "0");
            _this.Elements.boxTextWhoIsOnlineUserHidden_ForumIndex = new Udiv(_this.Elements.BoxWhoIsOnlineFooter_ForumIndex.Column[0].This, ("boxTextWhoIsOnlineUserHidden" + "_ForumIndex"));
            _this.Elements.textWhoIsOnlineUserHidden_ForumIndex = new Uspan(_this.Elements.boxTextWhoIsOnlineUserHidden_ForumIndex, ("textWhoIsOnlineUserHidden" + "_ForumIndex"), UsrCfg.Traslate.GetLangText(_this.Mythis, "USERHIDDEN") + " : ");
            _this.Elements.countWhoIsOnlineUserHidden_ForumIndex = new Uspan(_this.Elements.boxTextWhoIsOnlineUserHidden_ForumIndex, ("countWhoIsOnlineUserHidden" + "_ForumIndex"), "0");
            _this.Elements.boxTextWhoIsOnlineUserGuest_ForumIndex = new Udiv(_this.Elements.BoxWhoIsOnlineFooter_ForumIndex.Column[0].This, ("boxTextWhoIsOnlineUserGuest" + "_ForumIndex"));
            _this.Elements.textWhoIsOnlineUserGuest_ForumIndex = new Uspan(_this.Elements.boxTextWhoIsOnlineUserGuest_ForumIndex, ("textWhoIsOnlineUserGuest" + "_ForumIndex"), UsrCfg.Traslate.GetLangText(_this.Mythis, "USERGUEST") + " : ");
            _this.Elements.countWhoIsOnlineUserGuest_ForumIndex = new Uspan(_this.Elements.boxTextWhoIsOnlineUserGuest_ForumIndex, ("countWhoIsOnlineUserGuest" + "_ForumIndex"), "0");
            _this.Elements.BoxBirthdaysFooter_ForumIndex = new TVclStackPanel(_this.Elements.BoxMainBodyFooter_ForumIndex.Column[0].This, ("BoxBirthdaysFooter" + "_ForumIndex"), 1, [[12], [12], [12], [12], [12]]);
            _this.Elements.titleBirthdaysFooter_ForumIndex = new Uh3(_this.Elements.BoxBirthdaysFooter_ForumIndex.Column[0].This, ("titleBirthdaysFooter" + "_ForumIndex"), UsrCfg.Traslate.GetLangText(_this.Mythis, "BIRTHDAYS"));
            $(_this.Elements.titleBirthdaysFooter_ForumIndex).addClass("optionsFooter")
            _this.Elements.boxTextBirthdaysFooter_ForumIndex = new Udiv(_this.Elements.BoxBirthdaysFooter_ForumIndex.Column[0].This, ("boxTextBirthdaysFooter" + "_ForumIndex"));
            _this.Elements.TextBirthdaysFooter_ForumIndex = new Uspan(_this.Elements.boxTextBirthdaysFooter_ForumIndex, ("TextBirthdaysFooter" + "_ForumIndex"), UsrCfg.Traslate.GetLangText(_this.Mythis, "NOBIRTHDAYSTODAY"));
            _this.Elements.BoxStatisticsFooter_ForumIndex = new TVclStackPanel(_this.Elements.BoxMainBodyFooter_ForumIndex.Column[0].This, ("BoxStatisticsFooter" + "_ForumIndex"), 1, [[12], [12], [12], [12], [12]]);
            _this.Elements.titleStatisticsFooter_ForumIndex = new Uh3(_this.Elements.BoxStatisticsFooter_ForumIndex.Column[0].This, ("titleStatisticsFooter" + "_ForumIndex"), UsrCfg.Traslate.GetLangText(_this.Mythis, "STATISTICS"));
            $(_this.Elements.titleStatisticsFooter_ForumIndex).addClass("optionsFooter")
            _this.Elements.boxTextStatisticsTotalTopics_ForumIndex = new Udiv(_this.Elements.BoxStatisticsFooter_ForumIndex.Column[0].This, ("boxTextStatisticsTotalTopics" + "_ForumIndex"));
            _this.Elements.textStatisticsTotalTopics_ForumIndex = new Uspan(_this.Elements.boxTextStatisticsTotalTopics_ForumIndex, ("textStatisticsTotalTopics" + "_ForumIndex"), UsrCfg.Traslate.GetLangText(_this.Mythis, "TOTALTOPICS") + " : ");
            _this.Elements.countStatisticsTotalTopics_ForumIndex = new Uspan(_this.Elements.boxTextStatisticsTotalTopics_ForumIndex, ("countStatisticsTotalTopics" + "_ForumIndex"), _this.DataWork.DataTotalAllForums.totalTopics);
            _this.Elements.boxTextStatisticsTotalResponse_ForumIndex = new Udiv(_this.Elements.BoxStatisticsFooter_ForumIndex.Column[0].This, ("boxTextStatisticsTotalResponse" + "_ForumIndex"));
            _this.Elements.textStatisticsTotalResponse_ForumIndex = new Uspan(_this.Elements.boxTextStatisticsTotalResponse_ForumIndex, ("textStatisticsTotalResponse" + "_ForumIndex"), UsrCfg.Traslate.GetLangText(_this.Mythis, "TOTALRESPONSE") + " : ");
            _this.Elements.countStatisticsTotalResponse_ForumIndex = new Uspan(_this.Elements.boxTextStatisticsTotalResponse_ForumIndex, ("countStatisticsTotalResponse" + "_ForumIndex"), _this.DataWork.DataTotalAllForums.totalResponse);
            _this.Elements.boxTextStatisticsTotalMembers_ForumIndex = new Udiv(_this.Elements.BoxStatisticsFooter_ForumIndex.Column[0].This, ("boxTextStatisticsTotalMembers" + "_ForumIndex"));
            _this.Elements.textStatisticsTotalMembers_ForumIndex = new Uspan(_this.Elements.boxTextStatisticsTotalMembers_ForumIndex, ("textStatisticsTotalMembers" + "_ForumIndex"), UsrCfg.Traslate.GetLangText(_this.Mythis, "TOTALMEMBERS") + " : ");
            _this.Elements.countStatisticsTotalMembers_ForumIndex = new Uspan(_this.Elements.boxTextStatisticsTotalMembers_ForumIndex, ("countStatisticsTotalMembers" + "_ForumIndex"), _this.DataWork.DataTotalAllForums.totalMembers);
        }
        //END BODY FOOTER
        //HEADER FOOTER
        _this.Elements.BoxHeaderFooter_ForumIndex = new TVclStackPanel(_this.Elements.BoxFooter_ForumIndex.Column[0].This, ("BoxHeaderFooter" + "_ForumIndex"), 1, [[12], [12], [12], [12], [12]]);
        _this.Elements.BoxNavHeaderFooter_ForumIndex = new TVclStackPanel(_this.Elements.BoxHeaderFooter_ForumIndex.Column[0].This, ("BoxNavHeaderFooter" + "_ForumIndex"), 1, [[12], [12], [12], [12], [12]]);
        _this.Elements.olNavFooter_ForumIndex = new Uol(_this.Elements.BoxNavHeaderFooter_ForumIndex.Column[0].This, ("olNavFooter" + "_ForumIndex"));
        $(_this.Elements.olNavFooter_ForumIndex).addClass("breadcrumb");
        _this.Elements.liForumOlNavFooter_ForumIndex = new Uli(_this.Elements.olNavFooter_ForumIndex, ("liForumOlNavFooter" + "_ForumIndex"));
        _this.Elements.textTitleLiForumOlNavFooter_ForumIndex = new Ua(_this.Elements.liForumOlNavFooter_ForumIndex, ("textTitleLiForumOlNavFooter" + "_ForumIndex"), UsrCfg.Traslate.GetLangText(_this.Mythis, "FORUM"));
        $(_this.Elements.textTitleLiForumOlNavFooter_ForumIndex).addClass("textPointer redirectPage");
        $(_this.Elements.textTitleLiForumOlNavFooter_ForumIndex).data("EnumPage", JSON.stringify(_this.EnumPage.Index));
        _this.Elements.iconTitleLiForumOlNavFooter_ForumIndex = new Uspan("", ("iconTitleLiForumOlNavFooter_ForumIndex" + "_ForumIndex"), "");
        $(_this.Elements.iconTitleLiForumOlNavFooter_ForumIndex).addClass("glyphicon glyphicon-home iconSpacing");
        $(_this.Elements.textTitleLiForumOlNavFooter_ForumIndex).prepend(_this.Elements.iconTitleLiForumOlNavFooter_ForumIndex);

        _this.Elements.liCategoryOlNavFooter_ForumIndex = new Uli(_this.Elements.olNavFooter_ForumIndex, ("liCategoryOlNavFooter" + "_ForumIndex"));
        _this.Elements.textTitleLiCategoryOlNavFooter_ForumIndex = new Ua(_this.Elements.liCategoryOlNavFooter_ForumIndex, ("textTitleLiCategoryOlNavFooter" + "_ForumIndex"), UsrCfg.Traslate.GetLangText(_this.Mythis, "FORUMCATEGORY"));
        $(_this.Elements.textTitleLiCategoryOlNavFooter_ForumIndex).addClass("textPointer redirectPage");
        $(_this.Elements.textTitleLiCategoryOlNavFooter_ForumIndex).data("EnumPage", JSON.stringify(_this.EnumPage.ForumCategory));
        _this.Elements.iconTitleLiCategoryOlNavFooter_ForumIndex = new Uspan("", ("iconTitleLiCategoryOlNavFooter_ForumIndex" + "_ForumIndex"), "");
        $(_this.Elements.iconTitleLiCategoryOlNavFooter_ForumIndex).addClass("glyphicon glyphicon-list-alt iconSpacing");
        $(_this.Elements.textTitleLiCategoryOlNavFooter_ForumIndex).prepend(_this.Elements.iconTitleLiCategoryOlNavFooter_ForumIndex);

        _this.Elements.liTopiclNavFooter_ForumIndex = new Uli(_this.Elements.olNavFooter_ForumIndex, ("liTopiclNavFooter" + "_ForumIndex"));
        _this.Elements.textTitleLiTopiclNavFooter_ForumIndex = new Ua(_this.Elements.liTopiclNavFooter_ForumIndex, ("textTitleLiTopiclNavFooter" + "_ForumIndex"), UsrCfg.Traslate.GetLangText(_this.Mythis, "TOPIC"));
        $(_this.Elements.textTitleLiTopiclNavFooter_ForumIndex).addClass("textPointer redirectPage");
        $(_this.Elements.textTitleLiTopiclNavFooter_ForumIndex).data("EnumPage", JSON.stringify(_this.EnumPage.Topic));
        _this.Elements.iconTitleLiTopiclNavFooter_ForumIndex = new Uspan("", ("iconTitleLiTopiclNavFooter_ForumIndex" + "_ForumIndex"), "");
        $(_this.Elements.iconTitleLiTopiclNavFooter_ForumIndex).addClass("glyphicon glyphicon-file iconSpacing");
        $(_this.Elements.textTitleLiTopiclNavFooter_ForumIndex).prepend(_this.Elements.iconTitleLiTopiclNavFooter_ForumIndex);

        _this.Elements.liResponseFooter_ForumIndex = new Uli(_this.Elements.olNavFooter_ForumIndex, ("liResponseFooter" + "_ForumIndex"));
        _this.Elements.textTitleLiResponseFooter_ForumIndex = new Ua(_this.Elements.liResponseFooter_ForumIndex, ("textTitleLiResponseFooter" + "_ForumIndex"), UsrCfg.Traslate.GetLangText(_this.Mythis, "TOPICRESPONSE"))
        $(_this.Elements.textTitleLiResponseFooter_ForumIndex).addClass("textPointer redirectPage");
        $(_this.Elements.textTitleLiResponseFooter_ForumIndex).data("EnumPage", JSON.stringify(_this.EnumPage.Response));
        _this.Elements.icontTitleLiResponseFooter_ForumIndex = new Uspan("", ("icontTitleLiResponseFooter_ForumIndex" + "_ForumIndex"), "");
        $(_this.Elements.icontTitleLiResponseFooter_ForumIndex).addClass("glyphicon glyphicon-user iconSpacing");
        $(_this.Elements.textTitleLiResponseFooter_ForumIndex).prepend(_this.Elements.icontTitleLiResponseFooter_ForumIndex);

        //END HEADER FOOTER
        //END FOOTER
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentFooter", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentConfigurateForum = function () {
    var _this = this.TParent();
    try {
        //
        $("#liCategoryOlNav_ForumIndex").css({ "display": "none" });
        $("#liTopiclNav_ForumIndex").css({ "display": "none" });
        $("#liResponse_ForumIndex").css({ "display": "none" });

        $("#liCategoryOlNavFooter_ForumIndex").css({ "display": "none" });
        $("#liTopiclNavFooter_ForumIndex").css({ "display": "none" });
        $("#liResponseFooter_ForumIndex").css({ "display": "none" });
        //
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentConfigurateForum", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentConfigurateForumCategory = function () {
    var _this = this.TParent();
    try {
        //
        $("#liTopiclNav_ForumIndex").css({ "display": "none" });
        $("#liResponse_ForumIndex").css({ "display": "none" });

        $("#liTopiclNavFooter_ForumIndex").css({ "display": "none" });
        $("#liResponseFooter_ForumIndex").css({ "display": "none" });
        //
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentConfigurateForum", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentConfigurateTopic = function () {
    var _this = this.TParent();
    try {
        //STYLE
        $("#liResponse_ForumIndex").css({ "display": "none" });
        $("#liResponseFooter_ForumIndex").css({ "display": "none" });
        $("#textTopicThBody_ForumIndex")[0].innerText = UsrCfg.Traslate.GetLangText(_this.Mythis, "VIEWS");
        _this.Elements.DynamicElements.BtnSearch.forEach(function (element) {
            element.This.style.marginRight = "15px";
            element.This.style.marginBottom = "20px";
        });
        _this.ComponentAdditionalTopic();
        _this.ComponentAddModalTopic();
        _this.ComponentModifyModalTopic(); 
        _this.EventAdditionalTopic();
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentConfigurateTopic", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentAdditionalTopic = function () {
    var _this = this.TParent();
    try {
        //ADD TOPIC
        _this.Elements.BoxHeaderBody_ForumIndex = new TVclStackPanel(_this.Elements.BoxBody_ForumIndex.Column[0].This, "BoxHeaderBody_ForumIndex", 1, [[12], [12], [12], [12], [12]]);

        _this.Elements.btnAddTopicHeaderBody_ForumIndex = new TVclButton(_this.Elements.BoxHeaderBody_ForumIndex.Column[0].This, "btnAddTopicHeaderBody_ForumIndex");
        _this.Elements.btnAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.btnAddTopicHeaderBody_ForumIndex.Src = "image/16/add.png";
        _this.Elements.btnAddTopicHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ADDTOPIC");
        _this.Elements.btnAddTopicHeaderBody_ForumIndex.This.style.marginRight = "15px";

        _this.Elements.btnModifyTopicHeaderBody_ForumIndex = new TVclButton(_this.Elements.BoxHeaderBody_ForumIndex.Column[0].This, "btnAddTopicHeaderBody_ForumIndex");
        _this.Elements.btnModifyTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.btnModifyTopicHeaderBody_ForumIndex.Src = "image/16/Refresh.png";
        _this.Elements.btnModifyTopicHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "MODIFYUPDATETOPIC");

        _this.Elements.boxTextStatisticsTotalViews_ForumIndex = new Udiv(null, ("boxTextStatisticsTotalMembers" + "_ForumIndex"));
        _this.Elements.textStatisticsTotalViews_ForumIndex = new Uspan(_this.Elements.boxTextStatisticsTotalViews_ForumIndex, "textStatisticsTotalViews_ForumIndex", UsrCfg.Traslate.GetLangText(_this.Mythis, "TOTALVIEWS") + " : ");
        _this.Elements.countStatisticsTotalViews_ForumIndex = new Uspan(_this.Elements.boxTextStatisticsTotalViews_ForumIndex, "countStatisticsTotalViews_ForumIndex", _this.DataWork.DataTotalAllForums.totalView);
        $("#boxTextStatisticsTotalTopics_ForumIndex").after(_this.Elements.boxTextStatisticsTotalViews_ForumIndex);

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentAdditionalTopic", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentAddModalTopic = function () {
    var _this = this.TParent();
    try {
        _this.Elements.modalAddTopicHeaderBody_ForumIndex = new TVclModal(_this.Elements.BoxBody_ForumIndex.Column[0].This, null, "modalAddTopicHeaderBody_ForumIndex");
        _this.Elements.modalAddTopicHeaderBody_ForumIndex.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.Mythis, "NEWTOPIC"));
        _this.Elements.modalBoxContentAddTopicHeaderBody_ForumIndex = new TVclStackPanel(_this.Elements.modalAddTopicHeaderBody_ForumIndex.Body.This, "modalBoxContentAddTopicHeaderBody_ForumIndex", 1, [[12], [12], [12], [12], [12]]);
        _this.Elements.modalBoxContentAddTopicHeaderBody_ForumIndex.Row.This.style.marginBottom = "20px";

        _this.Elements.modalBoxBtnActionsContentAddTopicHeaderBody_ForumIndex = new TVclStackPanel(_this.Elements.modalBoxContentAddTopicHeaderBody_ForumIndex.Column[0].This, "modalBoxBtnActionsContentAddTopicHeaderBody_ForumIndex", 1, [[12], [12], [12], [12], [12]]);
        _this.Elements.modalBoxBtnActionsContentAddTopicHeaderBody_ForumIndex.Row.This.style.marginTop = "20px";
        $(_this.Elements.modalBoxBtnActionsContentAddTopicHeaderBody_ForumIndex.Column[0].This).addClass("text-center");

        _this.Elements.modalBtnSaveContentAddTopicHeaderBody_ForumIndex = new TVclButton(_this.Elements.modalBoxBtnActionsContentAddTopicHeaderBody_ForumIndex.Column[0].This, "modalBtnSaveContentAddTopicHeaderBody_ForumIndex");
        _this.Elements.modalBtnSaveContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.modalBtnSaveContentAddTopicHeaderBody_ForumIndex.Src = "image/16/edit.png";
        _this.Elements.modalBtnSaveContentAddTopicHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "SAVETOPIC");

        _this.Elements.modalBoxTitleContentAddTopicHeaderBody_ForumIndex = new TVclStackPanel(_this.Elements.modalBoxContentAddTopicHeaderBody_ForumIndex.Column[0].This, "modalBoxTitleContentAddTopicHeaderBody_ForumIndex", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
        _this.Elements.modalBoxTitleContentAddTopicHeaderBody_ForumIndex.Row.This.style.marginTop = "20px";
        _this.Elements.labelTitleModalContentAddTopicHeaderBody_ForumIndex = new TVcllabel(_this.Elements.modalBoxTitleContentAddTopicHeaderBody_ForumIndex.Column[0].This, "labelTitleModalContentAddTopicHeaderBody_ForumIndex", TlabelType.H0);
        _this.Elements.labelTitleModalContentAddTopicHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TITLETOPIC");
        _this.Elements.labelTitleModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.textTitleModalContentAddTopicHeaderBody_ForumIndex = new TVclTextBox(_this.Elements.modalBoxTitleContentAddTopicHeaderBody_ForumIndex.Column[1].This, "textTitleModalContentAddTopicHeaderBody_ForumIndex");
        _this.Elements.textTitleModalContentAddTopicHeaderBody_ForumIndex.Text = ""
        _this.Elements.textTitleModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;

        _this.Elements.modalBoxDescriptionContentAddTopicHeaderBody_ForumIndex = new TVclStackPanel(_this.Elements.modalBoxContentAddTopicHeaderBody_ForumIndex.Column[0].This, "modalBoxDescriptionContentAddTopicHeaderBody_ForumIndex", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
        _this.Elements.modalBoxDescriptionContentAddTopicHeaderBody_ForumIndex.Row.This.style.marginTop = "20px";
        _this.Elements.labelDescriptionModalContentAddTopicHeaderBody_ForumIndex = new TVcllabel(_this.Elements.modalBoxDescriptionContentAddTopicHeaderBody_ForumIndex.Column[0].This, "labelDescriptionModalContentAddTopicHeaderBody_ForumIndex", TlabelType.H0);
        _this.Elements.labelDescriptionModalContentAddTopicHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "DESCRIPTIONTOPIC");
        _this.Elements.labelDescriptionModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.memoDescriptionModalContentAddTopicHeaderBody_ForumIndex = new TVclMemo(_this.Elements.modalBoxDescriptionContentAddTopicHeaderBody_ForumIndex.Column[1].This, "memoDescriptionModalContentAddTopicHeaderBody_ForumIndex");
        _this.Elements.memoDescriptionModalContentAddTopicHeaderBody_ForumIndex.Text = "";
        _this.Elements.memoDescriptionModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.memoDescriptionModalContentAddTopicHeaderBody_ForumIndex.Rows = 5;
        _this.Elements.memoDescriptionModalContentAddTopicHeaderBody_ForumIndex.Cols = 50;

        _this.Elements.modalBoxImageContentAddTopicHeaderBody_ForumIndex = new TVclStackPanel(_this.Elements.modalBoxContentAddTopicHeaderBody_ForumIndex.Column[0].This, "modalBoxImageContentAddTopicHeaderBody_ForumIndex", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
        _this.Elements.modalBoxImageContentAddTopicHeaderBody_ForumIndex.Row.This.style.marginTop = "20px";
        _this.Elements.labelImageModalContentAddTopicHeaderBody_ForumIndex = new TVcllabel(_this.Elements.modalBoxImageContentAddTopicHeaderBody_ForumIndex.Column[0].This, "labelImageModalContentAddTopicHeaderBody_ForumIndex", TlabelType.H0);
        _this.Elements.labelImageModalContentAddTopicHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TITLEIMAGE");
        _this.Elements.labelImageModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.textImageModalContentAddTopicHeaderBody_ForumIndex = new TVclTextBox(_this.Elements.modalBoxImageContentAddTopicHeaderBody_ForumIndex.Column[1].This, "textImageModalContentAddTopicHeaderBody_ForumIndex");
        _this.Elements.textImageModalContentAddTopicHeaderBody_ForumIndex.Text = ""
        _this.Elements.textImageModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;

        _this.Elements.modalBoxKeyWordsContentAddTopicHeaderBody_ForumIndex = new TVclStackPanel(_this.Elements.modalBoxContentAddTopicHeaderBody_ForumIndex.Column[0].This, "modalBoxKeyWordsContentAddTopicHeaderBody_ForumIndex", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
        _this.Elements.modalBoxKeyWordsContentAddTopicHeaderBody_ForumIndex.Row.This.style.marginTop = "20px";
        _this.Elements.labelKeyWordsModalContentAddTopicHeaderBody_ForumIndex = new TVcllabel(_this.Elements.modalBoxKeyWordsContentAddTopicHeaderBody_ForumIndex.Column[0].This, "labelKeyWordsModalContentAddTopicHeaderBody_ForumIndex", TlabelType.H0);
        _this.Elements.labelKeyWordsModalContentAddTopicHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TITLEKEYWORDS");
        _this.Elements.labelKeyWordsModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.textKeyWordsModalContentAddTopicHeaderBody_ForumIndex = new TVclTextBox(_this.Elements.modalBoxKeyWordsContentAddTopicHeaderBody_ForumIndex.Column[1].This, "textKeyWordsModalContentAddTopicHeaderBody_ForumIndex");
        _this.Elements.textKeyWordsModalContentAddTopicHeaderBody_ForumIndex.Text = ""
        _this.Elements.textKeyWordsModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;


    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentAddModalTopic", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentModifyModalTopic = function () {
    var _this = this.TParent();
    try {
        _this.Elements.modalModifyTopicHeaderBody_ForumIndex = new TVclModal(_this.Elements.BoxBody_ForumIndex.Column[0].This, null, "modalModifyTopicHeaderBody_ForumIndex");
        _this.Elements.modalModifyTopicHeaderBody_ForumIndex.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.Mythis, "MODIFYTOPIC"));
        _this.Elements.modalBoxContentModifyTopicHeaderBody_ForumIndex = new TVclStackPanel(_this.Elements.modalModifyTopicHeaderBody_ForumIndex.Body.This, "modalBoxContentModifyTopicHeaderBody_ForumIndex", 1, [[12], [12], [12], [12], [12]]);
        _this.Elements.modalBoxContentModifyTopicHeaderBody_ForumIndex.Row.This.style.marginBottom = "20px";

        _this.Elements.BoxBtnEditModalBody_ForumIndex = new TVclStackPanel(_this.Elements.modalBoxContentModifyTopicHeaderBody_ForumIndex.Column[0].This, "BoxBtnEditModalBody_ForumIndex", 1, [[12], [12], [12], [12], [12]]);
        _this.Elements.BoxBtnEditModalBody_ForumIndex.Row.This.style.marginTop = "20px";
        $(_this.Elements.BoxBtnEditModalBody_ForumIndex.Column[0].This).addClass("text-center");

        _this.Elements.btnEditUpdateTopicHeaderBody_ForumIndex = new TVclButton(_this.Elements.BoxBtnEditModalBody_ForumIndex.Column[0].This, "btnEditUpdateTopicHeaderBody_ForumIndex");
        _this.Elements.btnEditUpdateTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.btnEditUpdateTopicHeaderBody_ForumIndex.Src = "image/16/edit.png";
        _this.Elements.btnEditUpdateTopicHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "UPDATE");
        _this.Elements.btnEditUpdateTopicHeaderBody_ForumIndex.This.style.marginRight = "15px";

        _this.Elements.btnEditDeleteTopicHeaderBody_ForumIndex = new TVclButton(_this.Elements.BoxBtnEditModalBody_ForumIndex.Column[0].This, "btnEditDeleteTopicHeaderBody_ForumIndex");
        _this.Elements.btnEditDeleteTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.btnEditDeleteTopicHeaderBody_ForumIndex.Src = "image/16/delete.png";
        _this.Elements.btnEditDeleteTopicHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "DELETE");

        _this.Elements.modalBoxEditTopicsContentAddTopicHeaderBody_ForumIndex = new TVclStackPanel(_this.Elements.modalBoxContentModifyTopicHeaderBody_ForumIndex.Column[0].This, "modalBoxEditTopicsContentAddTopicHeaderBody_ForumIndex", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
        _this.Elements.modalBoxEditTopicsContentAddTopicHeaderBody_ForumIndex.Row.This.style.marginTop = "20px";
        _this.Elements.labelEditTopicsModalContentAddTopicHeaderBody_ForumIndex = new TVcllabel(_this.Elements.modalBoxEditTopicsContentAddTopicHeaderBody_ForumIndex.Column[0].This, "labelEditTopicsModalContentAddTopicHeaderBody_ForumIndex", TlabelType.H0);
        _this.Elements.labelEditTopicsModalContentAddTopicHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TOPICS");
        _this.Elements.labelEditTopicsModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.dropDownListEditTopicsModalContentAddTopicHeaderBody_ForumIndex = new TVclComboBox2(_this.Elements.modalBoxEditTopicsContentAddTopicHeaderBody_ForumIndex.Column[1].This, "dropDownListEditTopicsModalContentAddTopicHeaderBody_ForumIndex");
        $(_this.Elements.dropDownListEditTopicsModalContentAddTopicHeaderBody_ForumIndex.This).addClass("form-control");


        _this.Elements.modalBoxEditForumsCategoryContentAddTopicHeaderBody_ForumIndex = new TVclStackPanel(_this.Elements.modalBoxContentModifyTopicHeaderBody_ForumIndex.Column[0].This, "modalBoxEditForumsCategoryContentAddTopicHeaderBody_ForumIndex", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
        _this.Elements.modalBoxEditForumsCategoryContentAddTopicHeaderBody_ForumIndex.Row.This.style.marginTop = "20px";
        _this.Elements.labelEditForumsCategoryModalContentAddTopicHeaderBody_ForumIndex = new TVcllabel(_this.Elements.modalBoxEditForumsCategoryContentAddTopicHeaderBody_ForumIndex.Column[0].This, "labelEditForumsCategoryModalContentAddTopicHeaderBody_ForumIndex", TlabelType.H0);
        _this.Elements.labelEditForumsCategoryModalContentAddTopicHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "FORUMSCATEGORY");
        _this.Elements.labelEditForumsCategoryModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.dropDownListEditForumsCategoryModalContentAddTopicHeaderBody_ForumIndex = new TVclComboBox2(_this.Elements.modalBoxEditForumsCategoryContentAddTopicHeaderBody_ForumIndex.Column[1].This, "dropDownListEditForumsCategoryModalContentAddTopicHeaderBody_ForumIndex");
        $(_this.Elements.dropDownListEditForumsCategoryModalContentAddTopicHeaderBody_ForumIndex.This).addClass("form-control");

        _this.Elements.modalBoxEditUsersContentAddTopicHeaderBody_ForumIndex = new TVclStackPanel(_this.Elements.modalBoxContentModifyTopicHeaderBody_ForumIndex.Column[0].This, "modalBoxEditUsersContentAddTopicHeaderBody_ForumIndex", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
        _this.Elements.modalBoxEditUsersContentAddTopicHeaderBody_ForumIndex.Row.This.style.marginTop = "20px";
        _this.Elements.labelEditUsersModalContentAddTopicHeaderBody_ForumIndex = new TVcllabel(_this.Elements.modalBoxEditUsersContentAddTopicHeaderBody_ForumIndex.Column[0].This, "labelEditUsersModalContentAddTopicHeaderBody_ForumIndex", TlabelType.H0);
        _this.Elements.labelEditUsersModalContentAddTopicHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "USERS");
        _this.Elements.labelEditUsersModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.dropDownListEditUsersModalContentAddTopicHeaderBody_ForumIndex = new TVclComboBox2(_this.Elements.modalBoxEditUsersContentAddTopicHeaderBody_ForumIndex.Column[1].This, "dropDownListEditUsersModalContentAddTopicHeaderBody_ForumIndex");
        $(_this.Elements.dropDownListEditUsersModalContentAddTopicHeaderBody_ForumIndex.This).addClass("form-control");


        _this.Elements.modalBoxEditTitleContentAddTopicHeaderBody_ForumIndex = new TVclStackPanel(_this.Elements.modalBoxContentModifyTopicHeaderBody_ForumIndex.Column[0].This, "modalBoxEditTitleContentAddTopicHeaderBody_ForumIndex", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
        _this.Elements.modalBoxEditTitleContentAddTopicHeaderBody_ForumIndex.Row.This.style.marginTop = "20px";
        _this.Elements.labelEditTitleModalContentAddTopicHeaderBody_ForumIndex = new TVcllabel(_this.Elements.modalBoxEditTitleContentAddTopicHeaderBody_ForumIndex.Column[0].This, "labelEditTitleModalContentAddTopicHeaderBody_ForumIndex", TlabelType.H0);
        _this.Elements.labelEditTitleModalContentAddTopicHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TITLETOPIC");
        _this.Elements.labelEditTitleModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.textEditTitleModalContentAddTopicHeaderBody_ForumIndex = new TVclTextBox(_this.Elements.modalBoxEditTitleContentAddTopicHeaderBody_ForumIndex.Column[1].This, "textEditTitleModalContentAddTopicHeaderBody_ForumIndex");
        _this.Elements.textEditTitleModalContentAddTopicHeaderBody_ForumIndex.Text = ""
        _this.Elements.textEditTitleModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;

        _this.Elements.modalBoxEditDescriptionContentAddTopicHeaderBody_ForumIndex = new TVclStackPanel(_this.Elements.modalBoxContentModifyTopicHeaderBody_ForumIndex.Column[0].This, "modalBoxEditDescriptionContentAddTopicHeaderBody_ForumIndex", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
        _this.Elements.modalBoxEditDescriptionContentAddTopicHeaderBody_ForumIndex.Row.This.style.marginTop = "20px";
        _this.Elements.labelEditDescriptionModalContentAddTopicHeaderBody_ForumIndex = new TVcllabel(_this.Elements.modalBoxEditDescriptionContentAddTopicHeaderBody_ForumIndex.Column[0].This, "labelEditDescriptionModalContentAddTopicHeaderBody_ForumIndex", TlabelType.H0);
        _this.Elements.labelEditDescriptionModalContentAddTopicHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "DESCRIPTIONTOPIC");
        _this.Elements.labelEditDescriptionModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.memoEditDescriptionModalContentAddTopicHeaderBody_ForumIndex = new TVclMemo(_this.Elements.modalBoxEditDescriptionContentAddTopicHeaderBody_ForumIndex.Column[1].This, "memoEditDescriptionModalContentAddTopicHeaderBody_ForumIndex");
        _this.Elements.memoEditDescriptionModalContentAddTopicHeaderBody_ForumIndex.Text = "";
        _this.Elements.memoEditDescriptionModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.memoEditDescriptionModalContentAddTopicHeaderBody_ForumIndex.Rows = 5;
        _this.Elements.memoEditDescriptionModalContentAddTopicHeaderBody_ForumIndex.Cols = 50;

        _this.Elements.modalBoxEditResponseCountContentAddTopicHeaderBody_ForumIndex = new TVclStackPanel(_this.Elements.modalBoxContentModifyTopicHeaderBody_ForumIndex.Column[0].This, "modalBoxEditResponseCountContentAddTopicHeaderBody_ForumIndex", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
        _this.Elements.modalBoxEditResponseCountContentAddTopicHeaderBody_ForumIndex.Row.This.style.marginTop = "20px";
        _this.Elements.labelEditResponseCountModalContentAddTopicHeaderBody_ForumIndex = new TVcllabel(_this.Elements.modalBoxEditResponseCountContentAddTopicHeaderBody_ForumIndex.Column[0].This, "labelEditResponseCountModalContentAddTopicHeaderBody_ForumIndex", TlabelType.H0);
        _this.Elements.labelEditResponseCountModalContentAddTopicHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "RESPONSECOUNT");
        _this.Elements.labelEditResponseCountModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.textEditResponseCountModalContentAddTopicHeaderBody_ForumIndex = new TVclTextBox(_this.Elements.modalBoxEditResponseCountContentAddTopicHeaderBody_ForumIndex.Column[1].This, "textEditResponseCountModalContentAddTopicHeaderBody_ForumIndex");
        _this.Elements.textEditResponseCountModalContentAddTopicHeaderBody_ForumIndex.Text = ""
        _this.Elements.textEditResponseCountModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;

        _this.Elements.modalBoxEditViewContentAddTopicHeaderBody_ForumIndex = new TVclStackPanel(_this.Elements.modalBoxContentModifyTopicHeaderBody_ForumIndex.Column[0].This, "modalBoxEditViewContentAddTopicHeaderBody_ForumIndex", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
        _this.Elements.modalBoxEditViewContentAddTopicHeaderBody_ForumIndex.Row.This.style.marginTop = "20px";
        _this.Elements.labelEditViewModalContentAddTopicHeaderBody_ForumIndex = new TVcllabel(_this.Elements.modalBoxEditViewContentAddTopicHeaderBody_ForumIndex.Column[0].This, "labelEditViewModalContentAddTopicHeaderBody_ForumIndex", TlabelType.H0);
        _this.Elements.labelEditViewModalContentAddTopicHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "VIEWS");
        _this.Elements.labelEditViewModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.textEditViewModalContentAddTopicHeaderBody_ForumIndex = new TVclTextBox(_this.Elements.modalBoxEditViewContentAddTopicHeaderBody_ForumIndex.Column[1].This, "textEditViewModalContentAddTopicHeaderBody_ForumIndex");
        _this.Elements.textEditViewModalContentAddTopicHeaderBody_ForumIndex.Text = ""
        _this.Elements.textEditViewModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;

        _this.Elements.modalBoxEditImageContentAddTopicHeaderBody_ForumIndex = new TVclStackPanel(_this.Elements.modalBoxContentModifyTopicHeaderBody_ForumIndex.Column[0].This, "modalBoxEditImageContentAddTopicHeaderBody_ForumIndex", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
        _this.Elements.modalBoxEditImageContentAddTopicHeaderBody_ForumIndex.Row.This.style.marginTop = "20px";
        _this.Elements.labelEditImageModalContentAddTopicHeaderBody_ForumIndex = new TVcllabel(_this.Elements.modalBoxEditImageContentAddTopicHeaderBody_ForumIndex.Column[0].This, "labelEditImageModalContentAddTopicHeaderBody_ForumIndex", TlabelType.H0);
        _this.Elements.labelEditImageModalContentAddTopicHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TITLEIMAGE");
        _this.Elements.labelEditImageModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.textEditImageModalContentAddTopicHeaderBody_ForumIndex = new TVclTextBox(_this.Elements.modalBoxEditImageContentAddTopicHeaderBody_ForumIndex.Column[1].This, "textEditImageModalContentAddTopicHeaderBody_ForumIndex");
        _this.Elements.textEditImageModalContentAddTopicHeaderBody_ForumIndex.Text = ""
        _this.Elements.textEditImageModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;

        _this.Elements.modalBoxEditCreatedDateContentAddTopicHeaderBody_ForumIndex = new TVclStackPanel(_this.Elements.modalBoxContentModifyTopicHeaderBody_ForumIndex.Column[0].This, "modalBoxEditCreatedDateContentAddTopicHeaderBody_ForumIndex", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
        _this.Elements.modalBoxEditCreatedDateContentAddTopicHeaderBody_ForumIndex.Row.This.style.marginTop = "20px";
        _this.Elements.labelEditCreatedDateModalContentAddTopicHeaderBody_ForumIndex = new TVcllabel(_this.Elements.modalBoxEditCreatedDateContentAddTopicHeaderBody_ForumIndex.Column[0].This, "labelEditCreatedDateModalContentAddTopicHeaderBody_ForumIndex", TlabelType.H0);
        _this.Elements.labelEditCreatedDateModalContentAddTopicHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "CREATEDDATE");
        _this.Elements.labelEditCreatedDateModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.textEditCreatedDateModalContentAddTopicHeaderBody_ForumIndex = new TVclTextBox(_this.Elements.modalBoxEditCreatedDateContentAddTopicHeaderBody_ForumIndex.Column[1].This, "textEditCreatedDateModalContentAddTopicHeaderBody_ForumIndex");
        _this.Elements.textEditCreatedDateModalContentAddTopicHeaderBody_ForumIndex.Text = ""
        _this.Elements.textEditCreatedDateModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;

        _this.Elements.modalBoxEditKeyWordsContentAddTopicHeaderBody_ForumIndex = new TVclStackPanel(_this.Elements.modalBoxContentModifyTopicHeaderBody_ForumIndex.Column[0].This, "modalBoxEditKeyWordsContentAddTopicHeaderBody_ForumIndex", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
        _this.Elements.modalBoxEditKeyWordsContentAddTopicHeaderBody_ForumIndex.Row.This.style.marginTop = "20px";
        _this.Elements.labelEditKeyWordsModalContentAddTopicHeaderBody_ForumIndex = new TVcllabel(_this.Elements.modalBoxEditKeyWordsContentAddTopicHeaderBody_ForumIndex.Column[0].This, "labelEditKeyWordsModalContentAddTopicHeaderBody_ForumIndex", TlabelType.H0);
        _this.Elements.labelEditKeyWordsModalContentAddTopicHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TITLEKEYWORDS");
        _this.Elements.labelEditKeyWordsModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.textEditKeyWordsModalContentAddTopicHeaderBody_ForumIndex = new TVclTextBox(_this.Elements.modalBoxEditKeyWordsContentAddTopicHeaderBody_ForumIndex.Column[1].This, "textEditKeyWordsModalContentAddTopicHeaderBody_ForumIndex");
        _this.Elements.textEditKeyWordsModalContentAddTopicHeaderBody_ForumIndex.Text = ""
        _this.Elements.textEditKeyWordsModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentModifyModalTopic", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.EventAdditionalTopic = function () {
    var _this = this.TParent();
    try {
        _this.Elements.btnAddTopicHeaderBody_ForumIndex.onClick = function () {
            try {
                _this.Elements.textTitleModalContentAddTopicHeaderBody_ForumIndex.Text = "";
                _this.Elements.memoEditDescriptionModalContentAddTopicHeaderBody_ForumIndex.Text = "";
                _this.Elements.textImageModalContentAddTopicHeaderBody_ForumIndex.Text = "";
                _this.Elements.textKeyWordsModalContentAddTopicHeaderBody_ForumIndex.Text = "";
                _this.Elements.modalAddTopicHeaderBody_ForumIndex.ShowModal();
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.EventAdditionalTopic btnAddTopicHeaderBody_ForumIndex.onClick()", e);
            }
        }
        _this.Elements.btnModifyTopicHeaderBody_ForumIndex.onClick = function () {
            try {
                var dataManager = _this.TopicsCreateUserManager(_this.Manager);
                if (dataManager.ListTopic.length > 0) {
                    _this.ClearDataModalModify();
                    _this.LoadDropDownListAndDataModalModify(dataManager);
                    _this.SetDataModalModify(dataManager);
                    _this.ConfigurateDataModalModify();
                    _this.Elements.modalModifyTopicHeaderBody_ForumIndex.ShowModal();
                }
                else {
                    if (_this.Manager)
                        alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NODATATOPIC"));
                    else
                        alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NODATAUSERTOPIC"));
                }
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.EventAdditionalTopic btnModifyTopicHeaderBody_ForumIndex.onClick()", e);
            }
        }
        _this.Elements.modalBtnSaveContentAddTopicHeaderBody_ForumIndex.onClick = function () {
            try {
                if (_this.SaveTopic(_this, this)) {
                    _this.ClearDataModalAddTopic();
                }
                return (true);
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.EventAdditionalTopic modalBtnSaveContentAddTopicHeaderBody_ForumIndex.onClick()", e);
            }
        }
        _this.Elements.DynamicElements.BtnSearch.forEach(function (element) {
            element.onClick = function () {
                try {
                    $(_this.ObjectHtml).html("");
                    _this.Elements = new Object();
                    _this.Elements.DynamicElements = new Array();
                    _this.SearchTopic(_this, element);
                    _this.InitializeComponent();
                } catch (e) {
                    SysCfg.Log.Methods.WriteLog("ForumsDesigner.js.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.EventAdditionalTopic _this.Elements.DynamicElements-BtnSearch.forEach _this.SearchTopic", e);
                }
            }
        });
        _this.Elements.dropDownListEditTopicsModalContentAddTopicHeaderBody_ForumIndex.onChange = function (e) {
            try {
                var topic = _this.Elements.dropDownListEditTopicsModalContentAddTopicHeaderBody_ForumIndex.SearchOption($(e.target).val());
                _this.Elements.dropDownListEditForumsCategoryModalContentAddTopicHeaderBody_ForumIndex.Value = topic.Tag.IDFORUMSCATEGORY;
                _this.Elements.dropDownListEditUsersModalContentAddTopicHeaderBody_ForumIndex.Value = topic.Tag.IDCMDBCI;
                _this.Elements.textEditTitleModalContentAddTopicHeaderBody_ForumIndex.Text = topic.Tag.NAMETOPICS;
                _this.Elements.memoEditDescriptionModalContentAddTopicHeaderBody_ForumIndex.Text = topic.Tag.DESCRIPTIONTOPICS;
                _this.Elements.textEditResponseCountModalContentAddTopicHeaderBody_ForumIndex.Text = topic.Tag.RESPONSETOPICS_COUNT;
                _this.Elements.textEditViewModalContentAddTopicHeaderBody_ForumIndex.Text = topic.Tag.VIEWSTOPICS_COUNT;
                _this.Elements.textEditImageModalContentAddTopicHeaderBody_ForumIndex.Text = topic.Tag.IMAGETOPICS;
                _this.Elements.textEditCreatedDateModalContentAddTopicHeaderBody_ForumIndex.Text = topic.Tag.CREATEDDATETOPICS;
                _this.Elements.textEditKeyWordsModalContentAddTopicHeaderBody_ForumIndex.Text = topic.Tag.KEYWORDSTOPICS;
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.EventAdditionalTopic dropDownListEditTopicsModalContentAddTopicHeaderBody_ForumIndex.onChange", e);
            }
        }
        _this.Elements.btnEditUpdateTopicHeaderBody_ForumIndex.onClick = function () {
            try {
                var success = _this.UpdateTopic();
                var dataManager = _this.TopicsCreateUserManager(_this.Manager);
                if (dataManager.ListTopic.length > 0) {
                    _this.ClearDataModalModify();
                    _this.LoadDropDownListAndDataModalModify(dataManager);
                    _this.SetDataModalModify(dataManager);
                    _this.ConfigurateDataModalModify();
                    _this.Elements.modalModifyTopicHeaderBody_ForumIndex.ShowModal();
                }
                else {
                    _this.Elements.dropDownListEditTopicsModalContentAddTopicHeaderBody_ForumIndex.ClearItems();
                    var VclComboBoxItem1 = new TVclComboBoxItem();
                    VclComboBoxItem1.Value = -1;
                    VclComboBoxItem1.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "NODATA");
                    VclComboBoxItem1.Tag = new Persistence.Forums.Properties.TTOPICS();
                    _this.Elements.dropDownListEditTopicsModalContentAddTopicHeaderBody_ForumIndex.AddItem(VclComboBoxItem1);
                    _this.Elements.dropDownListEditTopicsModalContentAddTopicHeaderBody_ForumIndex.selectedIndex = 0;
                    _this.ConfigurateDataModalModify(true);
                }
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.EventAdditionalTopic btnEditUpdateTopicHeaderBody_ForumIndex.onClick()", e);
            }
        }
        _this.Elements.btnEditDeleteTopicHeaderBody_ForumIndex.onClick = function () {
            try {
                var success = _this.DeleteTopic();
                if (success) {
                    var dataManager = _this.TopicsCreateUserManager(_this.Manager);
                    if (dataManager.ListTopic.length > 0) {
                        _this.ClearDataModalModify();
                        _this.LoadDropDownListAndDataModalModify(dataManager);
                        _this.SetDataModalModify(dataManager);
                        _this.ConfigurateDataModalModify();
                        _this.Elements.modalModifyTopicHeaderBody_ForumIndex.ShowModal();
                    }
                    else {
                        _this.Elements.dropDownListEditTopicsModalContentAddTopicHeaderBody_ForumIndex.ClearItems();
                        var VclComboBoxItem1 = new TVclComboBoxItem();
                        VclComboBoxItem1.Value = -1;
                        VclComboBoxItem1.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "NODATA");
                        VclComboBoxItem1.Tag = new Persistence.Forums.Properties.TTOPICS() ;
                        _this.Elements.dropDownListEditTopicsModalContentAddTopicHeaderBody_ForumIndex.AddItem(VclComboBoxItem1);
                        _this.Elements.dropDownListEditTopicsModalContentAddTopicHeaderBody_ForumIndex.selectedIndex = 0;
                        _this.ConfigurateDataModalModify(true);
                    }
                }
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.EventAdditionalTopic btnEditDeleteTopicHeaderBody_ForumIndex.onClick()", e);
            }
        }
        _this.Elements.modalAddTopicHeaderBody_ForumIndex.FunctionAfterClose = function () {
            try {
                $(_this.Elements.textTitleLiTopiclNav_ForumIndex).trigger("click");
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.modalAddTopicHeaderBody_ForumIndex", e);
            }
        }
        _this.Elements.modalModifyTopicHeaderBody_ForumIndex.FunctionAfterClose = function () {
            try {
                $(_this.Elements.textTitleLiTopiclNav_ForumIndex).trigger("click");
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.FunctionAfterClose", e);
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.EventAdditionalTopic", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.ClearDataModalAddTopic = function () {
    var _this = this.TParent();
    try {
        _this.Elements.textTitleModalContentAddTopicHeaderBody_ForumIndex.Text = "";
        _this.Elements.memoDescriptionModalContentAddTopicHeaderBody_ForumIndex.Text = "";
        _this.Elements.textImageModalContentAddTopicHeaderBody_ForumIndex.Text = "";
        _this.Elements.textKeyWordsModalContentAddTopicHeaderBody_ForumIndex.Text = "";
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.ClearDataModalAddTopic", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.ClearDataModalModify = function () {
    var _this = this.TParent();
    try {
        _this.Elements.dropDownListEditTopicsModalContentAddTopicHeaderBody_ForumIndex.ClearItems();
        _this.Elements.dropDownListEditForumsCategoryModalContentAddTopicHeaderBody_ForumIndex.ClearItems();
        _this.Elements.dropDownListEditUsersModalContentAddTopicHeaderBody_ForumIndex.ClearItems();

        _this.Elements.textEditTitleModalContentAddTopicHeaderBody_ForumIndex.Text = "";
        _this.Elements.memoEditDescriptionModalContentAddTopicHeaderBody_ForumIndex.Text = "";
        _this.Elements.textEditResponseCountModalContentAddTopicHeaderBody_ForumIndex.Text = "";
        _this.Elements.textEditViewModalContentAddTopicHeaderBody_ForumIndex.Text = "";
        _this.Elements.textEditImageModalContentAddTopicHeaderBody_ForumIndex.Text = "";
        _this.Elements.textEditCreatedDateModalContentAddTopicHeaderBody_ForumIndex.Text = "";
        _this.Elements.textEditKeyWordsModalContentAddTopicHeaderBody_ForumIndex.Text = "";

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.ClearDataModalModify", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.LoadDropDownListAndDataModalModify = function (dataManager) {
    var _this = this.TParent();
    var i = 0;
    try {
        for (i = 0; i < dataManager.ListTopic.length; i++) {
            var VclComboBoxItem1 = new TVclComboBoxItem();
            VclComboBoxItem1.Value = dataManager.ListTopic[i].IDTOPICS;
            VclComboBoxItem1.Text = dataManager.ListTopic[i].NAMETOPICS;
            VclComboBoxItem1.Tag = dataManager.ListTopic[i];
            _this.Elements.dropDownListEditTopicsModalContentAddTopicHeaderBody_ForumIndex.AddItem(VclComboBoxItem1);
        }
        for (i = 0; i < dataManager.ListForumCategory.length; i++) {
            var VclComboBoxItem2 = new TVclComboBoxItem();
            VclComboBoxItem2.Value = dataManager.ListForumCategory[i].IDFORUMSCATEGORY;
            VclComboBoxItem2.Text = dataManager.ListForumCategory[i].MDCATEGORYDETAIL.CATEGORYNAME;
            VclComboBoxItem2.Tag = dataManager.ListForumCategory[i];
            _this.Elements.dropDownListEditForumsCategoryModalContentAddTopicHeaderBody_ForumIndex.AddItem(VclComboBoxItem2);

        }
        for (i = 0; i < dataManager.ListUser.length; i++) {
            var VclComboBoxItem3 = new TVclComboBoxItem();
            VclComboBoxItem3.Value = dataManager.ListUser[i].IDCMDBCI;
            VclComboBoxItem3.Text = dataManager.ListUser[i].CI_GENERICNAME;
            VclComboBoxItem3.Tag = dataManager.ListUser[i];
            _this.Elements.dropDownListEditUsersModalContentAddTopicHeaderBody_ForumIndex.AddItem(VclComboBoxItem3);
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.LoadDropDownListAndDataModalModify", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.SetDataModalModify = function (dataManager) {
    var _this = this.TParent();
    try {
        var selectTopic = _this.Elements.dropDownListEditTopicsModalContentAddTopicHeaderBody_ForumIndex;
        selectTopic.selectedIndex = 0;;
        var topic = _this.Elements.dropDownListEditTopicsModalContentAddTopicHeaderBody_ForumIndex.SearchOption(selectTopic.Value);
        for (var i = 0; i < dataManager.ListTopic.length; i++) {
            if (dataManager.ListTopic[i].IDTOPICS == selectTopic.Value) {
                _this.Elements.dropDownListEditForumsCategoryModalContentAddTopicHeaderBody_ForumIndex.Value = topic.Tag.IDFORUMSCATEGORY;
                _this.Elements.dropDownListEditUsersModalContentAddTopicHeaderBody_ForumIndex.Value = topic.Tag.IDCMDBCI;
                _this.Elements.textEditTitleModalContentAddTopicHeaderBody_ForumIndex.Text = topic.Tag.NAMETOPICS;
                _this.Elements.memoEditDescriptionModalContentAddTopicHeaderBody_ForumIndex.Text = topic.Tag.DESCRIPTIONTOPICS;
                _this.Elements.textEditResponseCountModalContentAddTopicHeaderBody_ForumIndex.Text = topic.Tag.RESPONSETOPICS_COUNT;
                _this.Elements.textEditViewModalContentAddTopicHeaderBody_ForumIndex.Text = topic.Tag.VIEWSTOPICS_COUNT;
                _this.Elements.textEditImageModalContentAddTopicHeaderBody_ForumIndex.Text = topic.Tag.IMAGETOPICS;
                _this.Elements.textEditCreatedDateModalContentAddTopicHeaderBody_ForumIndex.Text = topic.Tag.CREATEDDATETOPICS;
                _this.Elements.textEditKeyWordsModalContentAddTopicHeaderBody_ForumIndex.Text = topic.Tag.KEYWORDSTOPICS;
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.SetDataModalModify", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.ConfigurateDataModalModify = function (allHide) {
    var _this = this.TParent();
    try {
        if (_this.Manager) {
            $(_this.Elements.modalBoxEditForumsCategoryContentAddTopicHeaderBody_ForumIndex.Row.This).show();
            $(_this.Elements.modalBoxEditUsersContentAddTopicHeaderBody_ForumIndex.Row.This).show();
            $(_this.Elements.modalBoxEditTitleContentAddTopicHeaderBody_ForumIndex.Row.This).show();
            $(_this.Elements.modalBoxEditDescriptionContentAddTopicHeaderBody_ForumIndex.Row.This).show();
            $(_this.Elements.modalBoxEditResponseCountContentAddTopicHeaderBody_ForumIndex.Row.This).show();
            $(_this.Elements.modalBoxEditViewContentAddTopicHeaderBody_ForumIndex.Row.This).show();
            $(_this.Elements.modalBoxEditImageContentAddTopicHeaderBody_ForumIndex.Row.This).show();
            $(_this.Elements.modalBoxEditCreatedDateContentAddTopicHeaderBody_ForumIndex.Row.This).show();
            $(_this.Elements.modalBoxEditKeyWordsContentAddTopicHeaderBody_ForumIndex.Row.This).show();
        }
        else {
            $(_this.Elements.modalBoxEditForumsCategoryContentAddTopicHeaderBody_ForumIndex.Row.This).hide();
            $(_this.Elements.modalBoxEditUsersContentAddTopicHeaderBody_ForumIndex.Row.This).hide();
            $(_this.Elements.modalBoxEditTitleContentAddTopicHeaderBody_ForumIndex.Row.This).show();
            $(_this.Elements.modalBoxEditDescriptionContentAddTopicHeaderBody_ForumIndex.Row.This).show();
            $(_this.Elements.modalBoxEditResponseCountContentAddTopicHeaderBody_ForumIndex.Row.This).hide();
            $(_this.Elements.modalBoxEditViewContentAddTopicHeaderBody_ForumIndex.Row.This).hide();
            $(_this.Elements.modalBoxEditImageContentAddTopicHeaderBody_ForumIndex.Row.This).show();
            $(_this.Elements.modalBoxEditCreatedDateContentAddTopicHeaderBody_ForumIndex.Row.This).hide();
            $(_this.Elements.modalBoxEditKeyWordsContentAddTopicHeaderBody_ForumIndex.Row.This).show();
        }
        if (allHide) {
            $(_this.Elements.modalBoxEditForumsCategoryContentAddTopicHeaderBody_ForumIndex.Row.This).hide();
            $(_this.Elements.modalBoxEditUsersContentAddTopicHeaderBody_ForumIndex.Row.This).hide();
            $(_this.Elements.modalBoxEditTitleContentAddTopicHeaderBody_ForumIndex.Row.This).hide();
            $(_this.Elements.modalBoxEditDescriptionContentAddTopicHeaderBody_ForumIndex.Row.This).hide();
            $(_this.Elements.modalBoxEditResponseCountContentAddTopicHeaderBody_ForumIndex.Row.This).hide();
            $(_this.Elements.modalBoxEditViewContentAddTopicHeaderBody_ForumIndex.Row.This).hide();
            $(_this.Elements.modalBoxEditImageContentAddTopicHeaderBody_ForumIndex.Row.This).hide();
            $(_this.Elements.modalBoxEditCreatedDateContentAddTopicHeaderBody_ForumIndex.Row.This).hide();
            $(_this.Elements.modalBoxEditKeyWordsContentAddTopicHeaderBody_ForumIndex.Row.This).hide();
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.ConfigurateDataModalModify", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentStyleMain = function () {
    var _this = this.TParent();
    try {
        $(".iconSpacing").css({ "margin-right": "5px" });
        $(".textPointer").css({ "cursor": "pointer" });
        $(".mediaWidth").css({ "width": "60px" });
        $(".blockCenter").css({ "display": "block", "text-align": "center" });
        $(".txtTitleConnection").css({ "font-weight": "bold", "color": "black" });
        $(".mainBodyFooter").css({ "margin-bottom": "10px", "margin-top": "10px" });
        $(".optionsFooter").css({ "border-bottom": "1px solid transparent", "border-bottom-color": "#CCCCCC", "text-transform":"uppercase" });
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentStyleMain", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentEventMain = function () {
    var _this = this.TParent();
    try {
        $(".redirectPage").on("click", function () {
            _this.CurrentPage = _this.EnumPage.GetEnum(JSON.parse($(this).data("EnumPage")).value);
            _this.Elements = new Object();
            _this.Elements.DynamicElements = new Array();
            switch (_this.CurrentPage) {
                case _this.EnumPage.ForumCategory:
                    if (!SysCfg.Str.Methods.IsNullOrEmpity($(this).data("Id")) && $(this).data("Id")> 0) {
                        _this.SessionPage.ForumCategory.IdLoad = $(this).data("Id");
                    }
                    break;
                case _this.EnumPage.Topic:
                    if (!SysCfg.Str.Methods.IsNullOrEmpity($(this).data("Id")) && $(this).data("Id") > 0) {
                        _this.SessionPage.Topic.IdLoad = $(this).data("Id");
                    }
                    break;
                case _this.EnumPage.Response:
                    if (!SysCfg.Str.Methods.IsNullOrEmpity($(this).data("Id")) && $(this).data("Id") > 0) {
                        _this.SessionPage.Response.IdLoad = $(this).data("Id");
                    }
                    break;
            }
            $(_this.ObjectHtml).html("");
            _this.Redirect(_this, this);
            $(this).unbind();
            return (true);
        });
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentEventMain", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.LoadFileExternComponent = function () {
    var _this = this.TParent();
    try {
        if (!document.getElementById("myCssComponentResponse")) {
            var head = document.getElementsByTagName('head')[0];
            var link = document.createElement('link');
            link.id = "myCssComponentResponse";
            link.rel = 'stylesheet';
            link.type = 'text/css';
            link.href = SysCfg.App.Properties.xRaiz + "Css/Forums/ForumsBasic.css";
            link.media = 'all';
            head.appendChild(link);
        }
        if (!document.getElementById("myCssComponentResponseEmoticons")) {
            var head = document.getElementsByTagName('head')[0];
            var link = document.createElement('link');
            link.id = "myCssComponentResponseEmoticons";
            link.rel = 'stylesheet';
            link.type = 'text/css';
            link.href = "https://use.fontawesome.com/releases/v5.2.0/css/all.css";
            link.media = 'all';
            head.appendChild(link);
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.LoadFileExternComponent", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentResponse = function () {
    var _this = this.TParent();
    try {
        _this.Elements.BoxBtnModifyTopicResponseBody_ForumIndex = new TVclStackPanel(_this.Elements.BoxForum_ForumIndex.Column[0].This, "BoxBtnModifyTopicResponseBody_ForumIndex", 1, [[12], [12], [12], [12], [12]]);
        _this.Elements.BoxBtnModifyTopicResponseBody_ForumIndex.Row.This.style.marginTop = "20px";
        _this.Elements.BoxBtnModifyTopicResponseBody_ForumIndex.Row.This.style.marginBottom = "20px";

        $(_this.Elements.BoxBtnModifyTopicResponseBody_ForumIndex.Column[0].This).addClass("text-center");
        _this.Elements.BtnModifyTopicResponseBody_ForumIndex = new TVclButton(_this.Elements.BoxBtnModifyTopicResponseBody_ForumIndex.Column[0].This, "btnAddTopicResponseBody_ForumIndex");
        _this.Elements.BtnModifyTopicResponseBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.BtnModifyTopicResponseBody_ForumIndex.Src = "image/16/Refresh.png";
        _this.Elements.BtnModifyTopicResponseBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "MODIFYUPDATETOPICRESPONSE");
        _this.Elements.BtnModifyTopicResponseBody_ForumIndex.This.style.marginRight = "15px";
        _this.Elements.BtnModifyTopicResponseVotesBody_ForumIndex = new TVclButton(_this.Elements.BoxBtnModifyTopicResponseBody_ForumIndex.Column[0].This, "btnAddTopicResponseBody_ForumIndex");
        _this.Elements.BtnModifyTopicResponseVotesBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.BtnModifyTopicResponseVotesBody_ForumIndex.Src = "image/16/Refresh.png";
        _this.Elements.BtnModifyTopicResponseVotesBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "MODIFYUPDATETOPICSRESPONSEVOTES");
        _this.Elements.BtnModifyTopicResponseVotesBody_ForumIndex.This.style.marginRight = "15px";
        _this.Elements.BtnModifyTopicResponseAbuseBody_ForumIndex = new TVclButton(_this.Elements.BoxBtnModifyTopicResponseBody_ForumIndex.Column[0].This, "btnAddTopicResponseBody_ForumIndex");
        _this.Elements.BtnModifyTopicResponseAbuseBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.BtnModifyTopicResponseAbuseBody_ForumIndex.Src = "image/16/Refresh.png";
        _this.Elements.BtnModifyTopicResponseAbuseBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "MODIFYUPDATETOPICSRESPONSEABUSE");
        for (var i = 0; i < _this.DataWork.Response.length; i++) {
            var objectPost = new Object();
            var nowDate = new Date().getTime().toString()
            objectPost.ContainerResponse_ForumIndex = new Udiv(_this.Elements.BoxForum_ForumIndex.Column[0].This, ("ContainerResponse_ForumIndex_" + i + "_" + nowDate));
            $(objectPost.ContainerResponse_ForumIndex).addClass("container bootstrap snippet");

            objectPost.BoxWidget_ForumIndex = new TVclStackPanel(objectPost.ContainerResponse_ForumIndex, ("BoxWidget_ForumIndex_" + i + "_" + nowDate), 1, [[12], [12], [12], [12], [12]]);
            objectPost.SectionWidget_ForumIndex = new Usection(objectPost.BoxWidget_ForumIndex.Column[0].This, ("SectionWidget_ForumIndex_" + i + "_" + nowDate));
            $(objectPost.SectionWidget_ForumIndex).addClass("widget");

            objectPost.BodyWidget_ForumIndex = new Udiv(objectPost.SectionWidget_ForumIndex, ("BodyWidget_ForumIndex_" + i + "_" + nowDate));
            $(objectPost.BodyWidget_ForumIndex).addClass("widget-body");

            objectPost.BodyWidgetContent_ForumIndex = new Udiv(objectPost.BodyWidget_ForumIndex, ("BodyWidgetContent_ForumIndex_" + i + "_" + nowDate));
            $(objectPost.BodyWidgetContent_ForumIndex).addClass("widget-top-overflow windget-padding-md clearfix bg-warning text-white");

            objectPost.BodyWidgetContentTitle_ForumIndex = new Uh3(objectPost.BodyWidgetContent_ForumIndex, ("BodyWidgetContentTitle_ForumIndex_" + i + "_" + nowDate), _this.DataWork.Response[i].ResponseInitName);
            $(objectPost.BodyWidgetContentTitle_ForumIndex).addClass("mt-lg mb-lg");

            objectPost.BodyWidgetContentKeywords_ForumIndex = new Uul(objectPost.BodyWidgetContent_ForumIndex, ("BodyWidgetContentKeywords_ForumIndex_" + i + "_" + nowDate));
            $(objectPost.BodyWidgetContentKeywords_ForumIndex).addClass("tags text-white pull-right")

            objectPost.BodyWidgetContentListKeywords_ForumIndex = new Uli(objectPost.BodyWidgetContentKeywords_ForumIndex, ("BodyWidgetContentListKeywords_ForumIndex_" + i + "_" + nowDate));

            _this.ComponentResponseKeyWords({ Box: objectPost.BodyWidgetContentListKeywords_ForumIndex, Id: (i + "_" + nowDate), Data: _this.DataWork.KeyWords, ObjectElementWork: objectPost});

            objectPost.BodyWidgetContentPostUser_ForumIndex = new Udiv(objectPost.BodyWidget_ForumIndex, ("BodyWidgetContentPostUser_ForumIndex_" + i + "_" + nowDate));
            $(objectPost.BodyWidgetContentPostUser_ForumIndex).addClass("post-user mt-n-lg");


            objectPost.BodyWidgetContentBoxImagePostUser_ForumIndex = new Uspan(objectPost.BodyWidgetContentPostUser_ForumIndex, ("BodyWidgetContentBoxImagePostUser_ForumIndex_" + i + "_" + nowDate + "_" + nowDate),"");
            $(objectPost.BodyWidgetContentBoxImagePostUser_ForumIndex).addClass("thumb-lg pull-left mr");
            objectPost.BodyWidgetContentImagePostUser_ForumIndex = new Uimg(objectPost.BodyWidgetContentBoxImagePostUser_ForumIndex, ("BodyWidgetContentImagePostUser_ForumIndex_" + i + "_" + nowDate + "_" + nowDate), _this.DataWork.Response[i].ResponseInitCreatedUser.Image, "");
            $(objectPost.BodyWidgetContentImagePostUser_ForumIndex).addClass("img-circle");

            objectPost.BodyWidgetContentBoxUserPost_ForumIndex = new Uh5(objectPost.BodyWidgetContentPostUser_ForumIndex, ("BodyWidgetContentBoxUserPost_ForumIndex_" + i + "_" + nowDate + "_" + nowDate), _this.DataWork.Response[i].ResponseInitCreatedUser.Name);
            $(objectPost.BodyWidgetContentBoxUserPost_ForumIndex).addClass("mt-sm fw-normal text-white")

            objectPost.BodyWidgetContentBoxDatePostUser_ForumIndex = new Up(objectPost.BodyWidgetContentPostUser_ForumIndex, ("BodyWidgetContentBoxDatePostUser_ForumIndex_" + i + "_" + nowDate + "_" + nowDate), "");
            $(objectPost.BodyWidgetContentBoxDatePostUser_ForumIndex).addClass("fs-mini text-muted");
            objectPost.BodyWidgetContentDatePostUser_ForumIndex = new Utime(objectPost.BodyWidgetContentBoxDatePostUser_ForumIndex, ("BodyWidgetContentDatePostUser_ForumIndex_" + i + "_" + nowDate), _this.DataWork.Response[i].ResponseInitCreatedDate);


            objectPost.BodyWidgetDescription_ForumIndex = new Up(objectPost.BodyWidget_ForumIndex, ("BodyWidgetDescription_ForumIndex_" + i + "_" + nowDate + "_" + nowDate), _this.DataWork.Response[i].ResponseInitDescription);
            $(objectPost.BodyWidgetDescription_ForumIndex).addClass("text-light fs-mini m");

            objectPost.FooterWidget_ForumIndex = new Ufooter(objectPost.SectionWidget_ForumIndex, ("FooterWidget_ForumIndex_" + i + "_" + nowDate + "_" + nowDate));
            $(objectPost.FooterWidget_ForumIndex).addClass("bg-body-light removeStyle");
            objectPost.FooterListActionPost_ForumIndex = new Uul(objectPost.FooterWidget_ForumIndex, ("FooterListActionPost_ForumIndex_" + i + "_" + nowDate));
            $(objectPost.FooterListActionPost_ForumIndex).addClass("post-links");
            objectPost.BoxFooterActionPostLike_ForumIndex = new Uli(objectPost.FooterListActionPost_ForumIndex, ("BoxFooterActionPostLike_ForumIndex_" + i + "_" + nowDate));
            objectPost.FooterLinkActionPostLike_ForumIndex = new Ua(objectPost.BoxFooterActionPostLike_ForumIndex, ("FooterLinkActionPostLike_ForumIndex_" + i + "_" + nowDate), "");
            $(objectPost.FooterLinkActionPostLike_ForumIndex).addClass("linkIcon");
            objectPost.FooterLinkActionCountLike_ForumIndex = new Uspan(objectPost.FooterLinkActionPostLike_ForumIndex, ("FooterLinkActionCountLike_ForumIndex_" + i + "_" + nowDate), "0");
            objectPost.FooterLinkActionIconLike_ForumIndex = new Ui(objectPost.FooterLinkActionCountLike_ForumIndex, ("FooterLinkActionIconLike_ForumIndex_" + i + "_" + nowDate));
            $(objectPost.FooterLinkActionIconLike_ForumIndex).addClass("far fa-thumbs-up Emoticon");
            objectPost.BoxFooterActionPostNotLike_ForumIndex = new Uli(objectPost.FooterListActionPost_ForumIndex, ("BoxFooterActionPostNotLike_ForumIndex_" + i + "_" + nowDate));
            objectPost.FooterLinkActionPostNotLike_ForumIndex = new Ua(objectPost.BoxFooterActionPostNotLike_ForumIndex, ("FooterLinkActionPostNotLike_ForumIndex_" + i + "_" + nowDate), "");
            $(objectPost.FooterLinkActionPostNotLike_ForumIndex).addClass("linkIcon");
            objectPost.FooterLinkActionCountNotLike_ForumIndex = new Uspan(objectPost.FooterLinkActionPostNotLike_ForumIndex, ("FooterLinkActionCountNotLike_ForumIndex_" + i + "_" + nowDate), "0");
            objectPost.FooterLinkActionIconNotLike_ForumIndex = new Ui(objectPost.FooterLinkActionCountNotLike_ForumIndex, ("FooterLinkActionIconNotLike_ForumIndex_" + i + "_" + nowDate));
            $(objectPost.FooterLinkActionIconNotLike_ForumIndex).addClass("far fa-thumbs-down Emoticon");
            objectPost.BoxFooterActionPostNotLike_ForumIndex = new Uli(objectPost.FooterListActionPost_ForumIndex, ("BoxFooterActionPostNotLike_ForumIndex_" + i + "_" + nowDate));
            objectPost.FooterLinkActionPostAbuse_ForumIndex = new Ua(objectPost.BoxFooterActionPostNotLike_ForumIndex, ("FooterLinkActionPostAbuse_ForumIndex_" + i + "_" + nowDate), "");
            $(objectPost.FooterLinkActionPostAbuse_ForumIndex).addClass("linkIcon");
            objectPost.FooterLinkActionCountAbuse_ForumIndex = new Uspan(objectPost.FooterLinkActionPostAbuse_ForumIndex, ("FooterLinkActionCountAbuse_ForumIndex_" + i + "_" + nowDate), "0");
            objectPost.FooterLinkActionIconAbuse_ForumIndex = new Ui(objectPost.FooterLinkActionCountAbuse_ForumIndex, ("FooterLinkActionIconAbuse_ForumIndex_" + i + "_" + nowDate));
            $(objectPost.FooterLinkActionIconAbuse_ForumIndex).addClass("fas fa-exclamation-triangle Emoticon");
            objectPost.BoxFooterLinkActionPostComment_ForumIndex = new Uli(objectPost.FooterListActionPost_ForumIndex, ("BoxFooterLinkActionPostComment_ForumIndex_" + i + "_" + nowDate));
            objectPost.FooterLinkActionPostComment_ForumIndex = new Ua(objectPost.BoxFooterLinkActionPostComment_ForumIndex, ("FooterLinkActionPostComment_ForumIndex_" + i + "_" + nowDate), "");
            $(objectPost.FooterLinkActionPostComment_ForumIndex).addClass("linkIcon flagComment");
            objectPost.FooterLinkActionCountComment_ForumIndex = new Uspan(objectPost.FooterLinkActionPostComment_ForumIndex, ("FooterLinkActionCountComment_ForumIndex_" + i + "_" + nowDate), "0");
            objectPost.FooterLinkActionIconComment_ForumIndex = new Ui(objectPost.FooterLinkActionCountComment_ForumIndex, ("FooterLinkActionIconComment_ForumIndex_" + i + "_" + nowDate));
            $(objectPost.FooterLinkActionIconComment_ForumIndex).addClass("far fa-comments Emoticon");

            objectPost.FooterListPostComent_ForumIndex = new Uul(objectPost.FooterWidget_ForumIndex, "FooterListPostComent_ForumIndex");
            $(objectPost.FooterListPostComent_ForumIndex).addClass("post-comments mt mb-0");
            _this.PaintAndEventsIcons(objectPost, _this.DataWork.Response[i]);
            objectPost.CommentUser = _this.ComponentResponseComment({ ObjectElementPost: objectPost, Id: (i + "_" + nowDate), InitComment: true, DataWork: _this.DataWork.Response[i], Grandchild: false });
            objectPost.ListCommentParent = _this.ComponentResponseCommentParent({ Box: objectPost.CommentUser.FooterPostComentInput_ForumIndex, Id: (i + "_" + nowDate), Data: _this.DataWork.Response[i] });
            _this.Elements.DynamicElements.push(objectPost);
        }

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentResponse", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.PaintAndEventsIcons = function (objectPost, dataTopicResponse) {
    var _this = this.TParent();
    var dataSaveEmoticonLike = {
        UserId: SysCfg.Str.Methods.IsNullOrEmpity(UsrCfg.Properties.UserATRole.User.IDCMDBCI) ? 0 : UsrCfg.Properties.UserATRole.User.IDCMDBCI,
        IdTopicResponse: SysCfg.Str.Methods.IsNullOrEmpity(dataTopicResponse.ResponseInitTopicResponseId) ? 0 : dataTopicResponse.ResponseInitTopicResponseId,
        ObjectTopicResponse: dataTopicResponse,
        EmoticonId : dataTopicResponse.ResponseInitVotesId,
        Status : dataTopicResponse.ResponseDataGlobal.ResponseInitPaintLike,
        EnumEmoticon : _this.EnumEmoticons.Like,
        Count: dataTopicResponse.ResponseDataGlobal.ResponseInitCountLike,
        Icon: objectPost.FooterLinkActionIconLike_ForumIndex,
        LinkIcon: objectPost.FooterLinkActionCountLike_ForumIndex
    };
    var dataSaveEmoticonNotLike = {
        UserId: SysCfg.Str.Methods.IsNullOrEmpity(UsrCfg.Properties.UserATRole.User.IDCMDBCI) ? 0 : UsrCfg.Properties.UserATRole.User.IDCMDBCI,
        IdTopicResponse: SysCfg.Str.Methods.IsNullOrEmpity(dataTopicResponse.ResponseInitTopicResponseId) ? 0 : dataTopicResponse.ResponseInitTopicResponseId,
        Count: 0,
        ObjectTopicResponse: dataTopicResponse,
        EmoticonId:dataTopicResponse.ResponseInitVotesId,
        Status:dataTopicResponse.ResponseDataGlobal.ResponseInitPaintNotLike,
        EnumEmoticon:_this.EnumEmoticons.NotLike,
        Count: dataTopicResponse.ResponseDataGlobal.ResponseInitCountNotLike,
        Icon: objectPost.FooterLinkActionIconNotLike_ForumIndex,
        LinkIcon: objectPost.FooterLinkActionCountNotLike_ForumIndex
    };
    var dataSaveEmoticonAbuse = {
        UserId: SysCfg.Str.Methods.IsNullOrEmpity(UsrCfg.Properties.UserATRole.User.IDCMDBCI) ? 0 : UsrCfg.Properties.UserATRole.User.IDCMDBCI,
        IdTopicResponse: SysCfg.Str.Methods.IsNullOrEmpity(dataTopicResponse.ResponseInitTopicResponseId) ? 0 : dataTopicResponse.ResponseInitTopicResponseId,
        Count: 0,
        ObjectTopicResponse: dataTopicResponse,
        EmoticonId : dataTopicResponse.ResponseInitAbuseId,
        Status : dataTopicResponse.ResponseDataGlobal.ResponseInitPaintAbuse,
        EnumEmoticon : _this.EnumEmoticons.Abuse,
        Count: dataTopicResponse.ResponseDataGlobal.ResponseInitCountAbuse,
        Icon: objectPost.FooterLinkActionIconAbuse_ForumIndex,
        LinkIcon: objectPost.FooterLinkActionCountAbuse_ForumIndex
    };
    try {
        var iconComment = objectPost.FooterLinkActionIconComment_ForumIndex;
        $(dataSaveEmoticonLike.LinkIcon).html("").text(dataTopicResponse.ResponseDataGlobal.ResponseInitCountLike).append(dataSaveEmoticonLike.Icon);
        $(dataSaveEmoticonNotLike.LinkIcon).html("").text(dataTopicResponse.ResponseDataGlobal.ResponseInitCountNotLike).append(dataSaveEmoticonNotLike.Icon);
        $(dataSaveEmoticonAbuse.LinkIcon).html("").text(dataTopicResponse.ResponseDataGlobal.ResponseInitCountAbuse).append(dataSaveEmoticonAbuse.Icon);
        $(objectPost.FooterLinkActionCountComment_ForumIndex).html("").text(dataTopicResponse.ResponseDataGlobal.ResponseInitCountComment).append(iconComment);

        if (dataTopicResponse.ResponseDataGlobal.ResponseInitPaintLike) {
            dataSaveEmoticonLike.LinkIcon.style.color = "#3498db";
        }
        if (dataTopicResponse.ResponseDataGlobal.ResponseInitPaintNotLike) {
            dataSaveEmoticonNotLike.LinkIcon.style.color = "#3498db";
        }
        if (dataTopicResponse.ResponseDataGlobal.ResponseInitPaintAbuse) {
            dataSaveEmoticonAbuse.LinkIcon.style.color = "#c0392b";
        }

        $(dataSaveEmoticonLike.LinkIcon).data("dataEmoticon", dataSaveEmoticonLike);
        $(dataSaveEmoticonLike.LinkIcon).unbind()
        $(dataSaveEmoticonLike.LinkIcon).on("click", _this, _this.SaveActionEmoticon);

        $(dataSaveEmoticonNotLike.LinkIcon).data("dataEmoticon", dataSaveEmoticonNotLike);
        $(dataSaveEmoticonNotLike.LinkIcon).unbind()
        $(dataSaveEmoticonNotLike.LinkIcon).on("click", _this, _this.SaveActionEmoticon);

        $(dataSaveEmoticonAbuse.LinkIcon).data("dataEmoticon", dataSaveEmoticonAbuse);
        $(dataSaveEmoticonAbuse.LinkIcon).unbind()
        $(dataSaveEmoticonAbuse.LinkIcon).on("click", _this, _this.SaveActionEmoticon);

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.PaintIcons", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.SaveActionEmoticon = function (event) {
    var dataEmoticon = $(this).data("dataEmoticon");
    var _this = event.data;
    var success = _this.SaveDataActionEmoticon(dataEmoticon);
    try {
        if (success) {
            if (dataEmoticon.Status) {
                if (_this.EnumEmoticons.Like == dataEmoticon.EnumEmoticon || _this.EnumEmoticons.NotLike == dataEmoticon.EnumEmoticon) {
                    dataEmoticon.LinkIcon.style.color = "#3498db";
                    $(dataEmoticon.LinkIcon).html("").text(dataEmoticon.Count).append(dataEmoticon.Icon);
                }
                else {
                    dataEmoticon.LinkIcon.style.color = "#c0392b";
                    $(dataEmoticon.LinkIcon).html("").text(dataEmoticon.Count).append(dataEmoticon.Icon);
                }
            }
            else {
                dataEmoticon.LinkIcon.style.color = "#999";
                $(dataEmoticon.LinkIcon).html("").text(dataEmoticon.Count).append(dataEmoticon.Icon);
            }
            $(dataEmoticon.LinkIcon).data("dataEmoticon", dataEmoticon);
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.SaveActionEmoticon", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentResponseKeyWords = function (objResponseKeyWords) {
    var _this = this.TParent();
    objResponseKeyWords.ObjectElementWork.KeyWords = new Array();
    var separateKeyWords = objResponseKeyWords.Data;
    separateKeyWords = separateKeyWords.split(" ");
    try {
        for (var i = 0; i < separateKeyWords.length; i++) {
            var keyWord = new Ua(objResponseKeyWords.Box, ("textKeywords_ForumIndex_" + objResponseKeyWords.Id + i), separateKeyWords[i])
            if (i<(separateKeyWords.length -1)) {
                $(keyWord).addClass("keyWords");
            }
            objResponseKeyWords.ObjectElementWork.KeyWords.push(keyWord);
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentResponseKeyWords", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentResponseComment = function (objResponseComment) {
    var _this = this.TParent();
    var objInputComment = new Object();
    var objectCommentParent = new _this.ObjectCommentParent();
    objectCommentParent.ElementPost = objResponseComment.ObjectElementPost;
    objectCommentParent.CurrentDataWork = objResponseComment.DataWork;
    objectCommentParent.Grandchild = objResponseComment.Grandchild;
    try {
        if (objResponseComment.InitComment) {
            objectCommentParent.TypeComment = _this.EnumTypeComment.Parent;
            objInputComment.FooterPostComentInput_ForumIndex = new Uli(objResponseComment.ObjectElementPost.FooterListPostComent_ForumIndex, ("FooterPostComentInput_ForumIndex_" + objResponseComment.Id));
        }
        else {
            objectCommentParent.TypeComment = _this.EnumTypeComment.Child;
            objInputComment.FooterPostComentInput_ForumIndex = new Udiv(objResponseComment.ObjectElementPost.FooterBodyPostComent, ("FooterPostComentInput_ForumIndex_" + objResponseComment.Id));
            $(objInputComment.FooterPostComentInput_ForumIndex).addClass("newComent");
        }
        objInputComment.FooterPostComentBoxImageInput_ForumIndex = new Uspan(objInputComment.FooterPostComentInput_ForumIndex, ("FooterPostComentBoxImageInput_ForumIndex_" + objResponseComment.Id),"");
        $(objInputComment.FooterPostComentBoxImageInput_ForumIndex).addClass("thumb-xs avatar pull-left mr-sm");
        objInputComment.FooterPostComentImageInput_ForumIndex = new Uimg(objInputComment.FooterPostComentBoxImageInput_ForumIndex, ("FooterPostComentImageInput_ForumIndex_" + objResponseComment.Id), _this.DataWork.UserCurrentImage, "");
        $(objInputComment.FooterPostComentImageInput_ForumIndex).addClass("img-circle");
        objInputComment.FooterBodyPostComentInput_ForumIndex = new Udiv(objInputComment.FooterPostComentInput_ForumIndex, ("FooterBodyPostComentInput_ForumIndex_" + objResponseComment.Id));
        $(objInputComment.FooterBodyPostComentInput_ForumIndex).addClass("comment-body");
        objInputComment.PostComentInput_ForumIndex = new TVclTextBox(objInputComment.FooterBodyPostComentInput_ForumIndex, ("PostComentInput_ForumIndex_" + objResponseComment.Id));
        objInputComment.PostComentInput_ForumIndex.VCLType = TVCLType.BS;
        objInputComment.PostComentInput_ForumIndex.This.setAttribute("type", "text");
        $(objInputComment.PostComentInput_ForumIndex.This).addClass("form-control input-sm");
        objInputComment.PostComentInput_ForumIndex.This.setAttribute("placeholder", UsrCfg.Traslate.GetLangText(_this.Mythis, "WRITECOMMENT"));
        $(objInputComment.PostComentInput_ForumIndex.This).data("dataNewComment", objectCommentParent);
        $(objInputComment.PostComentInput_ForumIndex.This).on("keypress", _this,_this.SaveComment)

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentResponseComment", e);
    }
    return (objInputComment);
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.SaveComment = function (event) {
    var _this = event.data;
    var key = event.charCode ? event.charCode : event.keyCode ? event.keyCode : 0;
    try {
        if (key == 13 && !SysCfg.Str.Methods.IsNullOrEmpity(this.value)) {
            event.preventDefault();
            var dataNewComment = $(this).data("dataNewComment");
            var result = _this.SaveNewComment(dataNewComment);
            if (!SysCfg.Str.Methods.IsNullOrEmpity(result)) {
                var object = { _This: _this, DataNewComment: dataNewComment, Result: result }
                if (dataNewComment.TypeComment == _this.EnumTypeComment.Parent) {
                    _this.PaintChildParent(object);
                }
                else {
                    _this.PaintChild(object);
                }
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.SaveComment", e);
    }
} 
ItHelpCenter.TForumsManager.ForumsBasic.prototype.PaintChildParent = function (objectData) {
    var _this = objectData._This;
    var nowDate = new Date().getTime().toString();
    var data = { ResponseInitListChildComment: [objectData.Result]}
    try {
        var newComment = _this.ComponentResponseCommentParent({ Box: objectData.DataNewComment.ElementPost.CommentUser.FooterPostComentInput_ForumIndex, Id: ("newComment_" + nowDate), Data: data });
        var elementNewComment = newComment[0];
        var commentTotalFather = $(elementNewComment.FooterPostComent_ForumIndex).closest("ul").siblings("ul").children().children('a').filter(".flagComment");
        if (commentTotalFather.length > 0) {
            var iconSpanComment = $(commentTotalFather).children("span")
            var iconComment = $(commentTotalFather).children("span").children("i");
            $(iconSpanComment).html("").text(objectData.DataNewComment.CurrentDataWork.ResponseDataGlobal.ResponseInitCountComment).append(iconComment);
        }
        $(elementNewComment.CommentUser.FooterPostComentInput_ForumIndex).css({ "display": "none" });
        objectData.DataNewComment.ElementPost.CommentUser.PostComentInput_ForumIndex.Text = "";
        _this.ComponentConfigurateResponse(true);
        objectData.DataNewComment.ElementPost.ListCommentParent.push(elementNewComment);
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.PaintChildParent", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.PaintChild = function (objectData) {
    var _this = objectData._This;
    var nowDate = new Date().getTime().toString();
    var data = { ResponseInitListChildComment: [objectData.Result] }
    try {
        $(objectData.DataNewComment.ElementPost.FooterLinkActionPostComment_ForumIndex).unbind();
        if (objectData.DataNewComment.Grandchild) 
            var newComment = _this.ComponentResponseCommentChild({ ObjectElementPost: objectData.DataNewComment.ElementPost, Id: "newCommentChild", Data: data, DrawChild: true });
        else
            var newComment = _this.ComponentResponseCommentChild({ ObjectElementPost: objectData.DataNewComment.ElementPost, Id: "newCommentChild", Data: data, DrawChild: false });
        var elementNewComment = newComment[0];
        var commentTotalFather = $(elementNewComment.MediaPostChild).siblings("ul").children().children('a').filter(".flagComment");
        if (commentTotalFather.length > 0) {
            var iconSpanComment = $(commentTotalFather).children("span")
            var iconComment = $(commentTotalFather).children("span").children("i");
            $(iconSpanComment).html("").text(objectData.DataNewComment.CurrentDataWork.ResponseDataGlobal.ResponseInitCountComment).append(iconComment);
        }
        if ($(elementNewComment.MediaPostChild).siblings("div").filter(".mediaPost_last").length > 0) {
            $(elementNewComment.MediaPostChild).siblings("div").filter(".mediaPost_last").css({ "display": "block" });
        }
        $(elementNewComment.CommentUser.FooterPostComentInput_ForumIndex).css({ "display": "none" });
        objectData.DataNewComment.ElementPost.CommentUser.PostComentInput_ForumIndex.Text = "";
        _this.ComponentConfigurateResponse(true);
        objectData.DataNewComment.ElementPost.ListCommentChild.push(elementNewComment);

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.PaintChild", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentResponseCommentParent = function (objResponseComment) {
    var _this = this.TParent();
    var _listCommentParent = new Array();
    var listCommentParent = objResponseComment.Data.ResponseInitListChildComment
    try {
        for (var i = 0; i < listCommentParent.length; i++) {
            var commentParent = new Object();
            commentParent.FooterPostComent_ForumIndex = new Uli("", ("FooterPostComent_ForumIndex" + objResponseComment.Id + "_" + i));
            $(commentParent.FooterPostComent_ForumIndex).insertBefore(objResponseComment.Box)
            commentParent.FooterPostComentBoxImage = new Uspan(commentParent.FooterPostComent_ForumIndex, ("FooterPostComentBoxImage" + objResponseComment.Id + "_" + i),"");
            $(commentParent.FooterPostComentBoxImage).addClass("thumb-xs avatar pull-left mr-sm");
            commentParent.FooterPostComentImage = new Uimg(commentParent.FooterPostComentBoxImage, ("FooterPostComentImage" + objResponseComment.Id + "_" + i), listCommentParent[i].ResponseInitCreatedUser.Image, "");
            $(commentParent.FooterPostComentImage).addClass("img-circle");
            commentParent.FooterBodyPostComent = new Udiv(commentParent.FooterPostComent_ForumIndex, ("FooterBodyPostComent" + objResponseComment.Id + "_" + i));
            $(commentParent.FooterBodyPostComent).addClass("comment-body");
            commentParent.FooterPostComentAutor = new Uh6(commentParent.FooterBodyPostComent, ("FooterPostComentAutor" + objResponseComment.Id + "_" + i), listCommentParent[i].ResponseInitCreatedUser.Name);
            $(commentParent.FooterPostComentAutor).addClass("author fw-semi-bold");
            commentParent.FooterPostComentDate = new Usmall(commentParent.FooterPostComentAutor, ("FooterPostComentDate" + objResponseComment.Id + "_" + i), listCommentParent[i].ResponseInitCreatedDate);
            $(commentParent.FooterPostComentDate).addClass("createdDateTopicResponse");
            commentParent.FooterPostComent = new Up(commentParent.FooterBodyPostComent, ("FooterPostComent" + objResponseComment.Id + "_" + i), listCommentParent[i].ResponseInitDescription);
            $(commentParent.FooterPostComent).addClass("descriptionTopicResponse");
            commentParent.FooterListActionPost = new Uul(commentParent.FooterBodyPostComent, ("FooterListActionPost" + objResponseComment.Id + "_" + i));
            $(commentParent.FooterListActionPost).addClass("post-links");

            commentParent.BoxFooterActionPostLike_ForumIndex = new Uli(commentParent.FooterListActionPost, ("BoxFooterActionPostLike_ForumIndex_" + objResponseComment.Id + "_" + i));
            commentParent.FooterLinkActionPostLike_ForumIndex = new Ua(commentParent.BoxFooterActionPostLike_ForumIndex, ("FooterLinkActionPostLike_ForumIndex_" + objResponseComment.Id + "_" + i), "");
            $(commentParent.FooterLinkActionPostLike_ForumIndex).addClass("linkIcon");
            commentParent.FooterLinkActionCountLike_ForumIndex = new Uspan(commentParent.FooterLinkActionPostLike_ForumIndex, ("FooterLinkActionCountLike_ForumIndex_" + objResponseComment.Id + "_" + i),"0");
            commentParent.FooterLinkActionIconLike_ForumIndex = new Ui(commentParent.FooterLinkActionCountLike_ForumIndex, ("FooterLinkActionIconLike_ForumIndex_" + objResponseComment.Id + "_" + i));
            $(commentParent.FooterLinkActionIconLike_ForumIndex).addClass("far fa-thumbs-up Emoticon");

            commentParent.BoxFooterActionPostNotLike_ForumIndex = new Uli(commentParent.FooterListActionPost, ("BoxFooterActionPostNotLike_ForumIndex_" + objResponseComment.Id + "_" + i));
            commentParent.FooterLinkActionPostNotLike_ForumIndex = new Ua(commentParent.BoxFooterActionPostNotLike_ForumIndex, ("FooterLinkActionPostNotLike_ForumIndex_" + objResponseComment.Id + "_" + i), "");
            $(commentParent.FooterLinkActionPostNotLike_ForumIndex).addClass("linkIcon");
            commentParent.FooterLinkActionCountNotLike_ForumIndex = new Uspan(commentParent.FooterLinkActionPostNotLike_ForumIndex, ("FooterLinkActionCountNotLike_ForumIndex_" + objResponseComment.Id + "_" + i), "0");
            commentParent.FooterLinkActionIconNotLike_ForumIndex = new Ui(commentParent.FooterLinkActionCountNotLike_ForumIndex, ("FooterLinkActionIconNotLike_ForumIndex_" + objResponseComment.Id + "_" + i));
            $(commentParent.FooterLinkActionIconNotLike_ForumIndex).addClass("far fa-thumbs-down Emoticon");

            commentParent.BoxFooterLinkActionPostAbuse_ForumIndex = new Uli(commentParent.FooterListActionPost, ("BoxFooterLinkActionPostAbuse_ForumIndex_" + objResponseComment.Id + "_" + i));
            commentParent.FooterLinkActionPostAbuse_ForumIndex = new Ua(commentParent.BoxFooterLinkActionPostAbuse_ForumIndex, ("FooterLinkActionPostAbuse_ForumIndex_" + objResponseComment.Id + "_" + i), "");
            $(commentParent.FooterLinkActionPostAbuse_ForumIndex).addClass("linkIcon");
            commentParent.FooterLinkActionCountAbuse_ForumIndex = new Uspan(commentParent.FooterLinkActionPostAbuse_ForumIndex, ("FooterLinkActionCountAbuse_ForumIndex_" + objResponseComment.Id + "_" + i), "0");
            commentParent.FooterLinkActionIconAbuse_ForumIndex = new Ui(commentParent.FooterLinkActionCountAbuse_ForumIndex, ("FooterLinkActionIconAbuse_ForumIndex_" + objResponseComment.Id + "_" + i));
            $(commentParent.FooterLinkActionIconAbuse_ForumIndex).addClass("fas fa-exclamation-triangle Emoticon");

            commentParent.BoxFooterLinkActionPostComment_ForumIndex = new Uli(commentParent.FooterListActionPost, ("BoxFooterLinkActionPostComment_ForumIndex_" + objResponseComment.Id + "_" + i));
            commentParent.FooterLinkActionPostComment_ForumIndex = new Ua(commentParent.BoxFooterLinkActionPostComment_ForumIndex, ("FooterLinkActionPostComment_ForumIndex_" + objResponseComment.Id + "_" + i), "");
            $(commentParent.FooterLinkActionPostComment_ForumIndex).addClass("linkIcon flagComment");
            commentParent.FooterLinkActionCountComment_ForumIndex = new Uspan(commentParent.FooterLinkActionPostComment_ForumIndex, ("FooterLinkActionCountComment_ForumIndex_" + objResponseComment.Id + "_" + i), "0");
            commentParent.FooterLinkActionIconComment_ForumIndex = new Ui(commentParent.FooterLinkActionCountComment_ForumIndex, ("FooterLinkActionIconComment_ForumIndex_" + objResponseComment.Id + "_" + i));
            $(commentParent.FooterLinkActionIconComment_ForumIndex).addClass("far fa-comments Emoticon");

            commentParent.BoxFooterLinkActionNewComment_ForumIndex = new Uli(commentParent.FooterListActionPost, ("BoxFooterLinkActionNewComment_ForumIndex_" + objResponseComment.Id + "_" + i));
            commentParent.FooterLinkActionNewComment_ForumIndex = new Ua(commentParent.BoxFooterLinkActionNewComment_ForumIndex, ("FooterLinkActionNewComment_ForumIndex_" + objResponseComment.Id + "_" + i), "");
            $(commentParent.FooterLinkActionNewComment_ForumIndex).addClass("linkIcon");
            commentParent.FooterLinkActionNewCountComment_ForumIndex = new Uspan(commentParent.FooterLinkActionNewComment_ForumIndex, ("FooterLinkActionNewCountComment_ForumIndex_" + objResponseComment.Id + "_" + i), "");
            commentParent.FooterLinkActionNewIconComment_ForumIndex = new Ui(commentParent.FooterLinkActionNewCountComment_ForumIndex, ("FooterLinkActionNewIconComment_ForumIndex_" + objResponseComment.Id + "_" + i));
            $(commentParent.FooterLinkActionNewIconComment_ForumIndex).addClass("fab fa-facebook-messenger");

            _this.PaintAndEventsIcons(commentParent, listCommentParent[i]);
            commentParent.CommentUser = _this.ComponentResponseComment({ ObjectElementPost: commentParent, Id: (objResponseComment.Id + "_" + i), InitComment: false, DataWork: listCommentParent[i], Grandchild: false  });
            commentParent.ListCommentChild = _this.ComponentResponseCommentChild({ ObjectElementPost: commentParent, Id: (objResponseComment.Id + "_" + i), Data: listCommentParent[i],DrawChild:false });
            $(commentParent.FooterLinkActionNewComment_ForumIndex).on("click", { Box: commentParent.CommentUser.FooterPostComentInput_ForumIndex, BoxText: commentParent.CommentUser.PostComentInput_ForumIndex }, function (event) {
                $(event.data.Box).toggle("slow");
                event.data.BoxText.Text="";
            });
            _listCommentParent.push(commentParent);
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentResponseCommentParent", e);
    }
    return (_listCommentParent);
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentConfigurateResponse = function (grandChild) {
    var _this = this.TParent();
    try {
        $(".removeStyle").css({ "margin": "0 -20px -15px", "padding": "15px 20px", "min-height": "auto", "width": "auto", "background": "#f6f6f6" });
        $(".createdDateTopicResponse").css({ "padding-left": "5px" });
        $(".descriptionTopicResponse").css({ "color": "#333", "font-weight": "auto" });
        $(".linkIcon").css({ "cursor": "pointer" });
        if (!grandChild) {
            $(".newComent, .commentChildHide").css({ "display": "none" });
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentConfigurateResponse", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentAditionalConfigurateResponse = function () {
    var _this = this.TParent();
    try {
        _this.ComponentAditionalConfigurateTopicResponse();
        _this.EventConfigurateTopicResponse();
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentAditionalConfigurateResponse", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentAditionalConfigurateTopicResponse = function () {
    var _this = this.TParent();
    try {
        _this.ComponentAditionalModalTopicResponse();
        _this.ComponentAditionalModalTopicResponseVotes();
        _this.ComponentAditionalModalTopicResponseAbuse();
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentAditionalConfigurateTopicResponse", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentAditionalModalTopicResponse = function () {
    var _this = this.TParent();
    try {
        _this.Elements.modalModifyTopicResponseHeaderBody_ForumIndex = new TVclModal(_this.Elements.BoxForum_ForumIndex.Column[0].This, null, "modalModifyTopicResponseHeaderBody_ForumIndex");
        _this.Elements.modalModifyTopicResponseHeaderBody_ForumIndex.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.Mythis, "MODIFYTOPICRESPONSE"));
        _this.Elements.modalBoxContentModifyTopicResponseHeaderBody_ForumIndex = new TVclStackPanel(_this.Elements.modalModifyTopicResponseHeaderBody_ForumIndex.Body.This, "modalBoxContentModifyTopicResponseHeaderBody_ForumIndex", 1, [[12], [12], [12], [12], [12]]);
        _this.Elements.modalBoxContentModifyTopicResponseHeaderBody_ForumIndex.Row.This.style.marginBottom = "20px";

        _this.Elements.BoxBtnEditTopicResponseModalBody_ForumIndex = new TVclStackPanel(_this.Elements.modalBoxContentModifyTopicResponseHeaderBody_ForumIndex.Column[0].This, "BoxBtnEditTopicResponseModalBody_ForumIndex", 1, [[12], [12], [12], [12], [12]]);
        _this.Elements.BoxBtnEditTopicResponseModalBody_ForumIndex.Row.This.style.marginTop = "20px";
        $(_this.Elements.BoxBtnEditTopicResponseModalBody_ForumIndex.Column[0].This).addClass("text-center");

        _this.Elements.btnEditUpdateTopicResponseHeaderBody_ForumIndex = new TVclButton(_this.Elements.BoxBtnEditTopicResponseModalBody_ForumIndex.Column[0].This, "btnEditUpdateTopicResponseHeaderBody_ForumIndex");
        _this.Elements.btnEditUpdateTopicResponseHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.btnEditUpdateTopicResponseHeaderBody_ForumIndex.Src = "image/16/edit.png";
        _this.Elements.btnEditUpdateTopicResponseHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "UPDATE");
        _this.Elements.btnEditUpdateTopicResponseHeaderBody_ForumIndex.This.style.marginRight = "15px";

        _this.Elements.btnEditDeleteTopicResponseHeaderBody_ForumIndex = new TVclButton(_this.Elements.BoxBtnEditTopicResponseModalBody_ForumIndex.Column[0].This, "btnEditDeleteTopicResponseHeaderBody_ForumIndex");
        _this.Elements.btnEditDeleteTopicResponseHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.btnEditDeleteTopicResponseHeaderBody_ForumIndex.Src = "image/16/delete.png";
        _this.Elements.btnEditDeleteTopicResponseHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "DELETE");

        _this.Elements.modalBoxEditTopicsResponseContentAddTopicHeaderBody_ForumIndex = new TVclStackPanel(_this.Elements.modalBoxContentModifyTopicResponseHeaderBody_ForumIndex.Column[0].This, "modalBoxEditTopicsResponseContentAddTopicHeaderBody_ForumIndex", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
        _this.Elements.modalBoxEditTopicsResponseContentAddTopicHeaderBody_ForumIndex.Row.This.style.marginTop = "20px";
        _this.Elements.labelEditTopicsResponseModalContentAddTopicHeaderBody_ForumIndex = new TVcllabel(_this.Elements.modalBoxEditTopicsResponseContentAddTopicHeaderBody_ForumIndex.Column[0].This, "labelEditTopicsResponseModalContentAddTopicHeaderBody_ForumIndex", TlabelType.H0);
        _this.Elements.labelEditTopicsResponseModalContentAddTopicHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TOPICSRESPONSE");
        _this.Elements.labelEditTopicsResponseModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.dropDownListEditTopicsResponseModalContentAddTopicHeaderBody_ForumIndex = new TVclComboBox2(_this.Elements.modalBoxEditTopicsResponseContentAddTopicHeaderBody_ForumIndex.Column[1].This, "dropDownListEditTopicsResponseModalContentAddTopicHeaderBody_ForumIndex");
        $(_this.Elements.dropDownListEditTopicsResponseModalContentAddTopicHeaderBody_ForumIndex.This).addClass("form-control");

        _this.Elements.modalBoxEditTopicsContentAddTopicHeaderBody_ForumIndex = new TVclStackPanel(_this.Elements.modalBoxContentModifyTopicResponseHeaderBody_ForumIndex.Column[0].This, "modalBoxEditTopicsContentAddTopicHeaderBody_ForumIndex", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
        _this.Elements.modalBoxEditTopicsContentAddTopicHeaderBody_ForumIndex.Row.This.style.marginTop = "20px";
        _this.Elements.labelEditTopicsModalContentAddTopicHeaderBody_ForumIndex = new TVcllabel(_this.Elements.modalBoxEditTopicsContentAddTopicHeaderBody_ForumIndex.Column[0].This, "labelEditTopicsModalContentAddTopicHeaderBody_ForumIndex", TlabelType.H0);
        _this.Elements.labelEditTopicsModalContentAddTopicHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TOPICS");
        _this.Elements.labelEditTopicsModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.dropDownListEditTopicsModalContentAddTopicHeaderBody_ForumIndex = new TVclComboBox2(_this.Elements.modalBoxEditTopicsContentAddTopicHeaderBody_ForumIndex.Column[1].This, "dropDownListEditTopicsModalContentAddTopicHeaderBody_ForumIndex");
        $(_this.Elements.dropDownListEditTopicsModalContentAddTopicHeaderBody_ForumIndex.This).addClass("form-control");

        _this.Elements.modalBoxEditUsersContentAddTopicHeaderBody_ForumIndex = new TVclStackPanel(_this.Elements.modalBoxContentModifyTopicResponseHeaderBody_ForumIndex.Column[0].This, "modalBoxEditUsersContentAddTopicHeaderBody_ForumIndex", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
        _this.Elements.modalBoxEditUsersContentAddTopicHeaderBody_ForumIndex.Row.This.style.marginTop = "20px";
        _this.Elements.labelEditUsersModalContentAddTopicHeaderBody_ForumIndex = new TVcllabel(_this.Elements.modalBoxEditUsersContentAddTopicHeaderBody_ForumIndex.Column[0].This, "labelEditUsersModalContentAddTopicHeaderBody_ForumIndex", TlabelType.H0);
        _this.Elements.labelEditUsersModalContentAddTopicHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "USERS");
        _this.Elements.labelEditUsersModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.dropDownListEditUsersModalContentAddTopicHeaderBody_ForumIndex = new TVclComboBox2(_this.Elements.modalBoxEditUsersContentAddTopicHeaderBody_ForumIndex.Column[1].This, "dropDownListEditUsersModalContentAddTopicHeaderBody_ForumIndex");
        $(_this.Elements.dropDownListEditUsersModalContentAddTopicHeaderBody_ForumIndex.This).addClass("form-control");

        _this.Elements.modalBoxEditTopicsParentContentAddTopicHeaderBody_ForumIndex = new TVclStackPanel(_this.Elements.modalBoxContentModifyTopicResponseHeaderBody_ForumIndex.Column[0].This, "modalBoxEditTopicsParentContentAddTopicHeaderBody_ForumIndex", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
        _this.Elements.modalBoxEditTopicsParentContentAddTopicHeaderBody_ForumIndex.Row.This.style.marginTop = "20px";
        _this.Elements.labelEditTopicsParentModalContentAddTopicHeaderBody_ForumIndex = new TVcllabel(_this.Elements.modalBoxEditTopicsParentContentAddTopicHeaderBody_ForumIndex.Column[0].This, "labelEditTopicsParentModalContentAddTopicHeaderBody_ForumIndex", TlabelType.H0);
        _this.Elements.labelEditTopicsParentModalContentAddTopicHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TOPICSRESPONSEPARENT");
        _this.Elements.labelEditTopicsParentModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.dropDownListEditTopicsParentModalContentAddTopicHeaderBody_ForumIndex = new TVclComboBox2(_this.Elements.modalBoxEditTopicsParentContentAddTopicHeaderBody_ForumIndex.Column[1].This, "dropDownListEditTopicsParentModalContentAddTopicHeaderBody_ForumIndex");
        $(_this.Elements.dropDownListEditTopicsParentModalContentAddTopicHeaderBody_ForumIndex.This).addClass("form-control");

        _this.Elements.modalBoxEditTitleContentAddTopicHeaderBody_ForumIndex = new TVclStackPanel(_this.Elements.modalBoxContentModifyTopicResponseHeaderBody_ForumIndex.Column[0].This, "modalBoxEditTitleContentAddTopicHeaderBody_ForumIndex", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
        _this.Elements.modalBoxEditTitleContentAddTopicHeaderBody_ForumIndex.Row.This.style.marginTop = "20px";
        _this.Elements.labelEditTitleModalContentAddTopicHeaderBody_ForumIndex = new TVcllabel(_this.Elements.modalBoxEditTitleContentAddTopicHeaderBody_ForumIndex.Column[0].This, "labelEditTitleModalContentAddTopicHeaderBody_ForumIndex", TlabelType.H0);
        _this.Elements.labelEditTitleModalContentAddTopicHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TITLETOPICSRESPONSE");
        _this.Elements.labelEditTitleModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.textEditTitleModalContentAddTopicHeaderBody_ForumIndex = new TVclTextBox(_this.Elements.modalBoxEditTitleContentAddTopicHeaderBody_ForumIndex.Column[1].This, "textEditTitleModalContentAddTopicHeaderBody_ForumIndex");
        _this.Elements.textEditTitleModalContentAddTopicHeaderBody_ForumIndex.Text = ""
        _this.Elements.textEditTitleModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;

        _this.Elements.modalBoxEditSubjectContentAddTopicHeaderBody_ForumIndex = new TVclStackPanel(_this.Elements.modalBoxContentModifyTopicResponseHeaderBody_ForumIndex.Column[0].This, "modalBoxEditSubjectContentAddTopicHeaderBody_ForumIndex", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
        _this.Elements.modalBoxEditSubjectContentAddTopicHeaderBody_ForumIndex.Row.This.style.marginTop = "20px";
        _this.Elements.labelEditSubjectModalContentAddTopicHeaderBody_ForumIndex = new TVcllabel(_this.Elements.modalBoxEditSubjectContentAddTopicHeaderBody_ForumIndex.Column[0].This, "labelEditSubjectModalContentAddTopicHeaderBody_ForumIndex", TlabelType.H0);
        _this.Elements.labelEditSubjectModalContentAddTopicHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "SUBJECTTOPICSRESPONSE");
        _this.Elements.labelEditSubjectModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.memoEditSubjectModalContentAddTopicHeaderBody_ForumIndex = new TVclMemo(_this.Elements.modalBoxEditSubjectContentAddTopicHeaderBody_ForumIndex.Column[1].This, "memoEditSubjectModalContentAddTopicHeaderBody_ForumIndex");
        _this.Elements.memoEditSubjectModalContentAddTopicHeaderBody_ForumIndex.Text = "";
        _this.Elements.memoEditSubjectModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.memoEditSubjectModalContentAddTopicHeaderBody_ForumIndex.Rows = 5;
        _this.Elements.memoEditSubjectModalContentAddTopicHeaderBody_ForumIndex.Cols = 50;

        _this.Elements.modalBoxEditCreatedDateContentAddTopicHeaderBody_ForumIndex = new TVclStackPanel(_this.Elements.modalBoxContentModifyTopicResponseHeaderBody_ForumIndex.Column[0].This, "modalBoxEditCreatedDateContentAddTopicHeaderBody_ForumIndex", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
        _this.Elements.modalBoxEditCreatedDateContentAddTopicHeaderBody_ForumIndex.Row.This.style.marginTop = "20px";
        _this.Elements.labelEditCreatedDateModalContentAddTopicHeaderBody_ForumIndex = new TVcllabel(_this.Elements.modalBoxEditCreatedDateContentAddTopicHeaderBody_ForumIndex.Column[0].This, "labelEditCreatedDateModalContentAddTopicHeaderBody_ForumIndex", TlabelType.H0);
        _this.Elements.labelEditCreatedDateModalContentAddTopicHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "CREATEDDATETOPICSRESPONSE");
        _this.Elements.labelEditCreatedDateModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.textEditCreatedDateModalContentAddTopicHeaderBody_ForumIndex = new TVclTextBox(_this.Elements.modalBoxEditCreatedDateContentAddTopicHeaderBody_ForumIndex.Column[1].This, "textEditCreatedDateModalContentAddTopicHeaderBody_ForumIndex");
        _this.Elements.textEditCreatedDateModalContentAddTopicHeaderBody_ForumIndex.Text = ""
        _this.Elements.textEditCreatedDateModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;

        _this.Elements.modalBoxEditCountLikeContentAddTopicHeaderBody_ForumIndex = new TVclStackPanel(_this.Elements.modalBoxContentModifyTopicResponseHeaderBody_ForumIndex.Column[0].This, "modalBoxEditCountLikeContentAddTopicHeaderBody_ForumIndex", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
        _this.Elements.modalBoxEditCountLikeContentAddTopicHeaderBody_ForumIndex.Row.This.style.marginTop = "20px";
        _this.Elements.labelEditCountLikeModalContentAddTopicHeaderBody_ForumIndex = new TVcllabel(_this.Elements.modalBoxEditCountLikeContentAddTopicHeaderBody_ForumIndex.Column[0].This, "labelEditCountLikeModalContentAddTopicHeaderBody_ForumIndex", TlabelType.H0);
        _this.Elements.labelEditCountLikeModalContentAddTopicHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TOTALTOPICSRESPONSECOUNT");
        _this.Elements.labelEditCountLikeModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.textEditCountLikeModalContentAddTopicHeaderBody_ForumIndex = new TVclTextBox(_this.Elements.modalBoxEditCountLikeContentAddTopicHeaderBody_ForumIndex.Column[1].This, "textEditCountLikeModalContentAddTopicHeaderBody_ForumIndex");
        _this.Elements.textEditCountLikeModalContentAddTopicHeaderBody_ForumIndex.Text = ""
        _this.Elements.textEditCountLikeModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;

        _this.Elements.modalBoxEditCountNotLikeContentAddTopicHeaderBody_ForumIndex = new TVclStackPanel(_this.Elements.modalBoxContentModifyTopicResponseHeaderBody_ForumIndex.Column[0].This, "modalBoxEditCountNotLikeContentAddTopicHeaderBody_ForumIndex", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
        _this.Elements.modalBoxEditCountNotLikeContentAddTopicHeaderBody_ForumIndex.Row.This.style.marginTop = "20px";
        _this.Elements.labelEditCountNotLikeModalContentAddTopicHeaderBody_ForumIndex = new TVcllabel(_this.Elements.modalBoxEditCountNotLikeContentAddTopicHeaderBody_ForumIndex.Column[0].This, "labelEditCountNotLikeModalContentAddTopicHeaderBody_ForumIndex", TlabelType.H0);
        _this.Elements.labelEditCountNotLikeModalContentAddTopicHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TOTALLIKETOPICSRESPONSECOUNT");
        _this.Elements.labelEditCountNotLikeModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.textEditCountNotLikeModalContentAddTopicHeaderBody_ForumIndex = new TVclTextBox(_this.Elements.modalBoxEditCountNotLikeContentAddTopicHeaderBody_ForumIndex.Column[1].This, "textEditCountNotLikeModalContentAddTopicHeaderBody_ForumIndex");
        _this.Elements.textEditCountNotLikeModalContentAddTopicHeaderBody_ForumIndex.Text = ""
        _this.Elements.textEditCountNotLikeModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;

        _this.Elements.modalBoxEditCountAbuseContentAddTopicHeaderBody_ForumIndex = new TVclStackPanel(_this.Elements.modalBoxContentModifyTopicResponseHeaderBody_ForumIndex.Column[0].This, "modalBoxEditCountAbuseContentAddTopicHeaderBody_ForumIndex", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
        _this.Elements.modalBoxEditCountAbuseContentAddTopicHeaderBody_ForumIndex.Row.This.style.marginTop = "20px";
        _this.Elements.labelEditCountAbuseModalContentAddTopicHeaderBody_ForumIndex = new TVcllabel(_this.Elements.modalBoxEditCountAbuseContentAddTopicHeaderBody_ForumIndex.Column[0].This, "labelEditCountAbuseModalContentAddTopicHeaderBody_ForumIndex", TlabelType.H0);
        _this.Elements.labelEditCountAbuseModalContentAddTopicHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ABUSETOPICSRESPONSECOUNT");
        _this.Elements.labelEditCountAbuseModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.textEditCountAbuseModalContentAddTopicHeaderBody_ForumIndex = new TVclTextBox(_this.Elements.modalBoxEditCountAbuseContentAddTopicHeaderBody_ForumIndex.Column[1].This, "textEditCountAbuseModalContentAddTopicHeaderBody_ForumIndex");
        _this.Elements.textEditCountAbuseModalContentAddTopicHeaderBody_ForumIndex.Text = ""
        _this.Elements.textEditCountAbuseModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentAditionalModalTopicResponse", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentAditionalModalTopicResponseVotes = function () {
    var _this = this.TParent();
    try {
        _this.Elements.modalModifyTopicResponseVotesHeaderBody_ForumIndex = new TVclModal(_this.Elements.BoxForum_ForumIndex.Column[0].This, null, "modalModifyTopicResponseVotesHeaderBody_ForumIndex");
        _this.Elements.modalModifyTopicResponseVotesHeaderBody_ForumIndex.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.Mythis, "MODIFYTOPICRESPONSEVOTES"));
        _this.Elements.modalBoxContentModifyTopicResponseVotesHeaderBody_ForumIndex = new TVclStackPanel(_this.Elements.modalModifyTopicResponseVotesHeaderBody_ForumIndex.Body.This, "modalBoxContentModifyTopicResponseVotesHeaderBody_ForumIndex", 1, [[12], [12], [12], [12], [12]]);
        _this.Elements.modalBoxContentModifyTopicResponseVotesHeaderBody_ForumIndex.Row.This.style.marginBottom = "20px";

        _this.Elements.BoxBtnEditResponseVotesModalBody_ForumIndex = new TVclStackPanel(_this.Elements.modalBoxContentModifyTopicResponseVotesHeaderBody_ForumIndex.Column[0].This, "BoxBtnEditResponseVotesModalBody_ForumIndex", 1, [[12], [12], [12], [12], [12]]);
        _this.Elements.BoxBtnEditResponseVotesModalBody_ForumIndex.Row.This.style.marginTop = "20px";
        $(_this.Elements.BoxBtnEditResponseVotesModalBody_ForumIndex.Column[0].This).addClass("text-center");

        _this.Elements.btnEditUpdateTopicResponseVotesHeaderBody_ForumIndex = new TVclButton(_this.Elements.BoxBtnEditResponseVotesModalBody_ForumIndex.Column[0].This, "btnEditUpdateTopicResponseVotesHeaderBody_ForumIndex");
        _this.Elements.btnEditUpdateTopicResponseVotesHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.btnEditUpdateTopicResponseVotesHeaderBody_ForumIndex.Src = "image/16/edit.png";
        _this.Elements.btnEditUpdateTopicResponseVotesHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "UPDATE");
        _this.Elements.btnEditUpdateTopicResponseVotesHeaderBody_ForumIndex.This.style.marginRight = "15px";

        _this.Elements.btnEditDeleteTopicResponseVotesHeaderBody_ForumIndex = new TVclButton(_this.Elements.BoxBtnEditResponseVotesModalBody_ForumIndex.Column[0].This, "btnEditDeleteTopicResponseVotesHeaderBody_ForumIndex");
        _this.Elements.btnEditDeleteTopicResponseVotesHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.btnEditDeleteTopicResponseVotesHeaderBody_ForumIndex.Src = "image/16/delete.png";
        _this.Elements.btnEditDeleteTopicResponseVotesHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "DELETE");

        _this.Elements.modalBoxEditTopicResponseVotesContentAddTopicHeaderBody_ForumIndex = new TVclStackPanel(_this.Elements.modalBoxContentModifyTopicResponseVotesHeaderBody_ForumIndex.Column[0].This, "modalBoxEditTopicResponseVotesContentAddTopicHeaderBody_ForumIndex", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
        _this.Elements.modalBoxEditTopicResponseVotesContentAddTopicHeaderBody_ForumIndex.Row.This.style.marginTop = "20px";
        _this.Elements.labelEditTopicResponseVotesModalContentAddTopicHeaderBody_ForumIndex = new TVcllabel(_this.Elements.modalBoxEditTopicResponseVotesContentAddTopicHeaderBody_ForumIndex.Column[0].This, "labelEditTopicResponseVotesModalContentAddTopicHeaderBody_ForumIndex", TlabelType.H0);
        _this.Elements.labelEditTopicResponseVotesModalContentAddTopicHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TOPICSRESPONSEVOTES");
        _this.Elements.labelEditTopicResponseVotesModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.dropDownListEditTopicResponseVotesModalContentAddTopicHeaderBody_ForumIndex = new TVclComboBox2(_this.Elements.modalBoxEditTopicResponseVotesContentAddTopicHeaderBody_ForumIndex.Column[1].This, "dropDownListEditTopicResponseVotesModalContentAddTopicHeaderBody_ForumIndex");
        $(_this.Elements.dropDownListEditTopicResponseVotesModalContentAddTopicHeaderBody_ForumIndex.This).addClass("form-control");

        _this.Elements.modalBoxEditTopicsResponseVotesContentAddTopicHeaderBody_ForumIndex = new TVclStackPanel(_this.Elements.modalBoxContentModifyTopicResponseVotesHeaderBody_ForumIndex.Column[0].This, "modalBoxEditTopicsResponseVotesContentAddTopicHeaderBody_ForumIndex", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
        _this.Elements.modalBoxEditTopicsResponseVotesContentAddTopicHeaderBody_ForumIndex.Row.This.style.marginTop = "20px";
        _this.Elements.labelEditTopicsResponseVotesModalContentAddTopicHeaderBody_ForumIndex = new TVcllabel(_this.Elements.modalBoxEditTopicsResponseVotesContentAddTopicHeaderBody_ForumIndex.Column[0].This, "labelEditTopicsResponseVotesModalContentAddTopicHeaderBody_ForumIndex", TlabelType.H0);
        _this.Elements.labelEditTopicsResponseVotesModalContentAddTopicHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TOPICSRESPONSE");
        _this.Elements.labelEditTopicsResponseVotesModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.dropDownListEditTopicsResponseVotesModalContentAddTopicHeaderBody_ForumIndex = new TVclComboBox2(_this.Elements.modalBoxEditTopicsResponseVotesContentAddTopicHeaderBody_ForumIndex.Column[1].This, "dropDownListEditTopicsResponseVotesModalContentAddTopicHeaderBody_ForumIndex");
        $(_this.Elements.dropDownListEditTopicsResponseVotesModalContentAddTopicHeaderBody_ForumIndex.This).addClass("form-control");

        _this.Elements.modalBoxEditUsersVotesContentAddTopicHeaderBody_ForumIndex = new TVclStackPanel(_this.Elements.modalBoxContentModifyTopicResponseVotesHeaderBody_ForumIndex.Column[0].This, "modalBoxEditUsersVotesContentAddTopicHeaderBody_ForumIndex", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
        _this.Elements.modalBoxEditUsersVotesContentAddTopicHeaderBody_ForumIndex.Row.This.style.marginTop = "20px";
        _this.Elements.labelEditUsersVotesModalContentAddTopicHeaderBody_ForumIndex = new TVcllabel(_this.Elements.modalBoxEditUsersVotesContentAddTopicHeaderBody_ForumIndex.Column[0].This, "labelEditUsersVotesModalContentAddTopicHeaderBody_ForumIndex", TlabelType.H0);
        _this.Elements.labelEditUsersVotesModalContentAddTopicHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "USERS");
        _this.Elements.labelEditUsersVotesModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.dropDownListEditUsersVotesModalContentAddTopicHeaderBody_ForumIndex = new TVclComboBox2(_this.Elements.modalBoxEditUsersVotesContentAddTopicHeaderBody_ForumIndex.Column[1].This, "dropDownListEditUsersVotesModalContentAddTopicHeaderBody_ForumIndex");
        $(_this.Elements.dropDownListEditUsersVotesModalContentAddTopicHeaderBody_ForumIndex.This).addClass("form-control");

        _this.Elements.modalBoxEditLikeContentAddTopicHeaderBody_ForumIndex = new TVclStackPanel(_this.Elements.modalBoxContentModifyTopicResponseVotesHeaderBody_ForumIndex.Column[0].This, "modalBoxEditLikeContentAddTopicHeaderBody_ForumIndex", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
        _this.Elements.modalBoxEditLikeContentAddTopicHeaderBody_ForumIndex.Row.This.style.marginTop = "20px";
        _this.Elements.labelEditLikeModalContentAddTopicHeaderBody_ForumIndex = new TVcllabel(_this.Elements.modalBoxEditLikeContentAddTopicHeaderBody_ForumIndex.Column[0].This, "labelEditLikeModalContentAddTopicHeaderBody_ForumIndex", TlabelType.H0);
        _this.Elements.labelEditLikeModalContentAddTopicHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "LIKETOPICSRESPONSEVOTES");
        _this.Elements.labelEditLikeModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.textEditLikeModalContentAddTopicHeaderBody_ForumIndex = new TVclInputcheckbox(_this.Elements.modalBoxEditLikeContentAddTopicHeaderBody_ForumIndex.Column[1].This, "textEditLikeModalContentAddTopicHeaderBody_ForumIndex");
        _this.Elements.textEditLikeModalContentAddTopicHeaderBody_ForumIndex.Checked = false;
        _this.Elements.textEditLikeModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;

        _this.Elements.modalBoxEditNotLikeContentAddTopicHeaderBody_ForumIndex = new TVclStackPanel(_this.Elements.modalBoxContentModifyTopicResponseVotesHeaderBody_ForumIndex.Column[0].This, "modalBoxEditNotLikeContentAddTopicHeaderBody_ForumIndex", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
        _this.Elements.modalBoxEditNotLikeContentAddTopicHeaderBody_ForumIndex.Row.This.style.marginTop = "20px";
        _this.Elements.labelEditNotLikeModalContentAddTopicHeaderBody_ForumIndex = new TVcllabel(_this.Elements.modalBoxEditNotLikeContentAddTopicHeaderBody_ForumIndex.Column[0].This, "labelEditNotLikeModalContentAddTopicHeaderBody_ForumIndex", TlabelType.H0);
        _this.Elements.labelEditNotLikeModalContentAddTopicHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "NOTLIKETOPICSRESPONSEVOTES");
        _this.Elements.labelEditNotLikeModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.textEditNotLikeModalContentAddTopicHeaderBody_ForumIndex = new TVclInputcheckbox(_this.Elements.modalBoxEditNotLikeContentAddTopicHeaderBody_ForumIndex.Column[1].This, "textEditNotLikeModalContentAddTopicHeaderBody_ForumIndex");
        _this.Elements.textEditNotLikeModalContentAddTopicHeaderBody_ForumIndex.Checked = false;
        _this.Elements.textEditNotLikeModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentAditionalModalTopicResponseVotes", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentAditionalModalTopicResponseAbuse = function () {
    var _this = this.TParent();
    try {
        _this.Elements.modalModifyTopicResponseAbuseHeaderBody_ForumIndex = new TVclModal(_this.Elements.BoxForum_ForumIndex.Column[0].This, null, "modalModifyTopicResponseAbuseHeaderBody_ForumIndex");
        _this.Elements.modalModifyTopicResponseAbuseHeaderBody_ForumIndex.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.Mythis, "TOPICSRESPONSEABUSE"));
        _this.Elements.modalBoxContentModifyTopicResponseAbuseHeaderBody_ForumIndex = new TVclStackPanel(_this.Elements.modalModifyTopicResponseAbuseHeaderBody_ForumIndex.Body.This, "modalBoxContentModifyTopicResponseAbuseHeaderBody_ForumIndex", 1, [[12], [12], [12], [12], [12]]);
        _this.Elements.modalBoxContentModifyTopicResponseAbuseHeaderBody_ForumIndex.Row.This.style.marginBottom = "20px";

        _this.Elements.BoxBtnEditModalResponseAbuseBody_ForumIndex = new TVclStackPanel(_this.Elements.modalBoxContentModifyTopicResponseAbuseHeaderBody_ForumIndex.Column[0].This, "BoxBtnEditModalResponseAbuseBody_ForumIndex", 1, [[12], [12], [12], [12], [12]]);
        _this.Elements.BoxBtnEditModalResponseAbuseBody_ForumIndex.Row.This.style.marginTop = "20px";
        $(_this.Elements.BoxBtnEditModalResponseAbuseBody_ForumIndex.Column[0].This).addClass("text-center");

        _this.Elements.btnEditUpdateTopicResponseAbuseHeaderBody_ForumIndex = new TVclButton(_this.Elements.BoxBtnEditModalResponseAbuseBody_ForumIndex.Column[0].This, "btnEditUpdateTopicResponseAbuseHeaderBody_ForumIndex");
        _this.Elements.btnEditUpdateTopicResponseAbuseHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.btnEditUpdateTopicResponseAbuseHeaderBody_ForumIndex.Src = "image/16/edit.png";
        _this.Elements.btnEditUpdateTopicResponseAbuseHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "UPDATE");
        _this.Elements.btnEditUpdateTopicResponseAbuseHeaderBody_ForumIndex.This.style.marginRight = "15px";

        _this.Elements.btnEditDeleteTopicResponseAbuseHeaderBody_ForumIndex = new TVclButton(_this.Elements.BoxBtnEditModalResponseAbuseBody_ForumIndex.Column[0].This, "btnEditDeleteTopicResponseAbuseHeaderBody_ForumIndex");
        _this.Elements.btnEditDeleteTopicResponseAbuseHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.btnEditDeleteTopicResponseAbuseHeaderBody_ForumIndex.Src = "image/16/delete.png";
        _this.Elements.btnEditDeleteTopicResponseAbuseHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "DELETE");

        _this.Elements.modalBoxEditTopicResponseAbuseContentAddTopicHeaderBody_ForumIndex = new TVclStackPanel(_this.Elements.modalBoxContentModifyTopicResponseAbuseHeaderBody_ForumIndex.Column[0].This, "modalBoxEditTopicResponseAbuseContentAddTopicHeaderBody_ForumIndex", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
        _this.Elements.modalBoxEditTopicResponseAbuseContentAddTopicHeaderBody_ForumIndex.Row.This.style.marginTop = "20px";
        _this.Elements.labelEditTopicResponseAbuseModalContentAddTopicHeaderBody_ForumIndex = new TVcllabel(_this.Elements.modalBoxEditTopicResponseAbuseContentAddTopicHeaderBody_ForumIndex.Column[0].This, "labelEditTopicResponseAbuseModalContentAddTopicHeaderBody_ForumIndex", TlabelType.H0);
        _this.Elements.labelEditTopicResponseAbuseModalContentAddTopicHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TOPICSRESPONSEVOTES");
        _this.Elements.labelEditTopicResponseAbuseModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.dropDownListEditTopicResponseAbuseModalContentAddTopicHeaderBody_ForumIndex = new TVclComboBox2(_this.Elements.modalBoxEditTopicResponseAbuseContentAddTopicHeaderBody_ForumIndex.Column[1].This, "dropDownListEditTopicResponseAbuseModalContentAddTopicHeaderBody_ForumIndex");
        $(_this.Elements.dropDownListEditTopicResponseAbuseModalContentAddTopicHeaderBody_ForumIndex.This).addClass("form-control");

        _this.Elements.modalBoxEditTopicsResponseAbuseContentAddTopicHeaderBody_ForumIndex = new TVclStackPanel(_this.Elements.modalBoxContentModifyTopicResponseAbuseHeaderBody_ForumIndex.Column[0].This, "modalBoxEditTopicsResponseAbuseContentAddTopicHeaderBody_ForumIndex", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
        _this.Elements.modalBoxEditTopicsResponseAbuseContentAddTopicHeaderBody_ForumIndex.Row.This.style.marginTop = "20px";
        _this.Elements.labelEditTopicsResponseAbuseModalContentAddTopicHeaderBody_ForumIndex = new TVcllabel(_this.Elements.modalBoxEditTopicsResponseAbuseContentAddTopicHeaderBody_ForumIndex.Column[0].This, "labelEditTopicsResponseAbuseModalContentAddTopicHeaderBody_ForumIndex", TlabelType.H0);
        _this.Elements.labelEditTopicsResponseAbuseModalContentAddTopicHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "TOPICSRESPONSE");
        _this.Elements.labelEditTopicsResponseAbuseModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.dropDownListEditTopicsResponseAbuseModalContentAddTopicHeaderBody_ForumIndex = new TVclComboBox2(_this.Elements.modalBoxEditTopicsResponseAbuseContentAddTopicHeaderBody_ForumIndex.Column[1].This, "dropDownListEditTopicsResponseAbuseModalContentAddTopicHeaderBody_ForumIndex");
        $(_this.Elements.dropDownListEditTopicsResponseAbuseModalContentAddTopicHeaderBody_ForumIndex.This).addClass("form-control");

        _this.Elements.modalBoxEditUsersAbuseContentAddTopicHeaderBody_ForumIndex = new TVclStackPanel(_this.Elements.modalBoxContentModifyTopicResponseAbuseHeaderBody_ForumIndex.Column[0].This, "modalBoxEditUsersAbuseContentAddTopicHeaderBody_ForumIndex", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
        _this.Elements.modalBoxEditUsersAbuseContentAddTopicHeaderBody_ForumIndex.Row.This.style.marginTop = "20px";
        _this.Elements.labelEditUsersAbuseModalContentAddTopicHeaderBody_ForumIndex = new TVcllabel(_this.Elements.modalBoxEditUsersAbuseContentAddTopicHeaderBody_ForumIndex.Column[0].This, "labelEditUsersAbuseModalContentAddTopicHeaderBody_ForumIndex", TlabelType.H0);
        _this.Elements.labelEditUsersAbuseModalContentAddTopicHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "USERS");
        _this.Elements.labelEditUsersAbuseModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.dropDownListEditUsersAbuseModalContentAddTopicHeaderBody_ForumIndex = new TVclComboBox2(_this.Elements.modalBoxEditUsersAbuseContentAddTopicHeaderBody_ForumIndex.Column[1].This, "dropDownListEditUsersAbuseModalContentAddTopicHeaderBody_ForumIndex");
        $(_this.Elements.dropDownListEditUsersAbuseModalContentAddTopicHeaderBody_ForumIndex.This).addClass("form-control");

        _this.Elements.modalBoxEditCheckAbuseContentAddTopicHeaderBody_ForumIndex = new TVclStackPanel(_this.Elements.modalBoxContentModifyTopicResponseAbuseHeaderBody_ForumIndex.Column[0].This, "modalBoxEditCheckAbuseContentAddTopicHeaderBody_ForumIndex", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
        _this.Elements.modalBoxEditCheckAbuseContentAddTopicHeaderBody_ForumIndex.Row.This.style.marginTop = "20px";
        _this.Elements.labelEditCheckAbuseModalContentAddTopicHeaderBody_ForumIndex = new TVcllabel(_this.Elements.modalBoxEditCheckAbuseContentAddTopicHeaderBody_ForumIndex.Column[0].This, "labelEditCheckAbuseModalContentAddTopicHeaderBody_ForumIndex", TlabelType.H0);
        _this.Elements.labelEditCheckAbuseModalContentAddTopicHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "LIKETOPICSRESPONSEABUSE");
        _this.Elements.labelEditCheckAbuseModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.textEditCheckAbuseModalContentAddTopicHeaderBody_ForumIndex = new TVclInputcheckbox(_this.Elements.modalBoxEditCheckAbuseContentAddTopicHeaderBody_ForumIndex.Column[1].This, "textEditCheckAbuseModalContentAddTopicHeaderBody_ForumIndex");
        _this.Elements.textEditCheckAbuseModalContentAddTopicHeaderBody_ForumIndex.Checked = false;
        _this.Elements.textEditCheckAbuseModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;

        _this.Elements.modalBoxEditUncheckAbuseContentAddTopicHeaderBody_ForumIndex = new TVclStackPanel(_this.Elements.modalBoxContentModifyTopicResponseAbuseHeaderBody_ForumIndex.Column[0].This, "modalBoxEditUncheckAbuseContentAddTopicHeaderBody_ForumIndex", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
        _this.Elements.modalBoxEditUncheckAbuseContentAddTopicHeaderBody_ForumIndex.Row.This.style.marginTop = "20px";
        _this.Elements.labelEditUncheckAbuseModalContentAddTopicHeaderBody_ForumIndex = new TVcllabel(_this.Elements.modalBoxEditUncheckAbuseContentAddTopicHeaderBody_ForumIndex.Column[0].This, "labelEditUncheckAbuseModalContentAddTopicHeaderBody_ForumIndex", TlabelType.H0);
        _this.Elements.labelEditUncheckAbuseModalContentAddTopicHeaderBody_ForumIndex.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "NOTLIKETOPICSRESPONSEABUSE");
        _this.Elements.labelEditUncheckAbuseModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;
        _this.Elements.textEditUncheckAbuseModalContentAddTopicHeaderBody_ForumIndex = new TVclInputcheckbox(_this.Elements.modalBoxEditUncheckAbuseContentAddTopicHeaderBody_ForumIndex.Column[1].This, "textEditUncheckAbuseModalContentAddTopicHeaderBody_ForumIndex");
        _this.Elements.textEditUncheckAbuseModalContentAddTopicHeaderBody_ForumIndex.Checked = false;
        _this.Elements.textEditUncheckAbuseModalContentAddTopicHeaderBody_ForumIndex.VCLType = TVCLType.BS;

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentAditionalModalTopicResponseAbuse", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.EventConfigurateTopicResponse = function () {
    var _this = this.TParent();
    try {
        _this.Elements.BtnModifyTopicResponseBody_ForumIndex.onClick = function () {
            try {
                var dataManager = _this.TopicsResponseCreateUserManager(_this.Manager);
                if (dataManager.ListTopicResponse.length > 0) {
                    _this.ClearDataModalTopicsResponse();
                    _this.LoadDropDownListAndDataModalTopicResponse(dataManager);
                    _this.SetDataModalTopicResponse(dataManager);
                    _this.ConfigurateDataModalTopicResponse();
                    _this.Elements.modalModifyTopicResponseHeaderBody_ForumIndex.ShowModal();
                }
                else {
                    if (_this.Manager)
                        alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NODATATOPICRESPONSE"));
                    else
                        alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NODATAUSERTOPICRESPONSE"));
                }
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("ForumsDesigner.js EventConfigurateTopicResponse _this.Elements.BtnModifyTopicResponseBody_ForumIndex.onClick)", e);
            }
        }
        _this.Elements.BtnModifyTopicResponseVotesBody_ForumIndex.onClick = function () {
            try {
                var dataManager = _this.TopicsResponseVotesCreateUserManager(_this.Manager);
                if (dataManager.ListTopicResponseVotes.length > 0) {
                    _this.ClearDataModalTopicsResponseVotes();
                    _this.LoadDropDownListAndDataModalTopicResponseVotes(dataManager);
                    _this.SetDataModalTopicResponseVotes(dataManager);
                    _this.ConfigurateDataModalTopicResponseVotes();
                    _this.Elements.modalModifyTopicResponseVotesHeaderBody_ForumIndex.ShowModal();
                }
                else {
                    if (_this.Manager)
                        alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NODATATOPICRESPONSEVOTES"));
                    else
                        alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NODATAUSERTOPICRESPONSEVOTES"));
                }
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("ForumsDesigner.js EventConfigurateTopicResponse _this.Elements.BtnModifyTopicResponseVotesBody_ForumIndex.onClick)", e);
            }
        }
        _this.Elements.BtnModifyTopicResponseAbuseBody_ForumIndex.onClick = function () {
            try {
                var dataManager = _this.TopicsResponseAbuseCreateUserManager(_this.Manager);
                if (dataManager.ListTopicResponseAbuse.length > 0) {
                    _this.ClearDataModalTopicsResponseAbuse();
                    _this.LoadDropDownListAndDataModalTopicResponseAbuse(dataManager);
                    _this.SetDataModalTopicResponseAbuse(dataManager);
                    _this.ConfigurateDataModalTopicResponseAbuse();
                    _this.Elements.modalModifyTopicResponseAbuseHeaderBody_ForumIndex.ShowModal();
                }
                else {
                    if (_this.Manager)
                        alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NODATATOPICRESPONSEABUSE"));
                    else
                        alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "NODATAUSERTOPICRESPONSEABUSE"));
                }
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.EventConfigurateTopicResponse _this.Elements.BtnModifyTopicResponseAbuseBody_ForumIndex.onClick)", e);
            }
        }
        _this.Elements.modalModifyTopicResponseHeaderBody_ForumIndex.FunctionAfterClose = function () {
            try {
                $(_this.Elements.textTitleLiResponse_ForumIndex).trigger("click");
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("ForumsDesigner.js EventConfigurateTopicResponse _this.Elements.modalModifyTopicHeaderBody_ForumIndex.FunctionAfterClose", e);
            }
        }
        _this.Elements.modalModifyTopicResponseVotesHeaderBody_ForumIndex.FunctionAfterClose = function () {
            try {
                $(_this.Elements.textTitleLiResponse_ForumIndex).trigger("click");
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("ForumsDesigner.js EventConfigurateTopicResponse _this.Elements.modalModifyTopicHeaderBody_ForumIndex.FunctionAfterClose", e);
            }
        }
        _this.Elements.modalModifyTopicResponseAbuseHeaderBody_ForumIndex.FunctionAfterClose = function () {
            try {
                $(_this.Elements.textTitleLiResponse_ForumIndex).trigger("click");
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("ForumsDesigner.js EventConfigurateTopicResponse _this.Elements.modalModifyTopicHeaderBody_ForumIndex.FunctionAfterClose", e);
            }
        }
        _this.Elements.dropDownListEditTopicsResponseModalContentAddTopicHeaderBody_ForumIndex.onChange = function (e) {
            try {
                var topicResponse = _this.Elements.dropDownListEditTopicsResponseModalContentAddTopicHeaderBody_ForumIndex.SearchOption($(e.target).val());
                _this.Elements.dropDownListEditTopicsResponseModalContentAddTopicHeaderBody_ForumIndex.Value = topicResponse.Tag.IDTOPICSRESPONSE;
                _this.Elements.dropDownListEditTopicsModalContentAddTopicHeaderBody_ForumIndex.Value = topicResponse.Tag.IDTOPICS;
                _this.Elements.dropDownListEditUsersModalContentAddTopicHeaderBody_ForumIndex.Value = topicResponse.Tag.IDCMDBCI;
                _this.Elements.dropDownListEditTopicsParentModalContentAddTopicHeaderBody_ForumIndex.Value = topicResponse.Tag.IDTOPICSRESPONSE_PARENT;
                _this.Elements.textEditTitleModalContentAddTopicHeaderBody_ForumIndex.Text = topicResponse.Tag.TITLETOPICSRESPONSE;
                _this.Elements.memoEditSubjectModalContentAddTopicHeaderBody_ForumIndex.Text = topicResponse.Tag.SUBJECTTOPICSRESPONSE;
                _this.Elements.textEditCreatedDateModalContentAddTopicHeaderBody_ForumIndex.Text = topicResponse.Tag.CREATEDDATETOPICSRESPONSE;
                _this.Elements.textEditCountLikeModalContentAddTopicHeaderBody_ForumIndex.Text = topicResponse.Tag.LIKETOPICSRESPONSE_COUNT;
                _this.Elements.textEditCountNotLikeModalContentAddTopicHeaderBody_ForumIndex.Text = topicResponse.Tag.NOTLIKETOPICSRESPONSE_COUNT;
                _this.Elements.textEditCountAbuseModalContentAddTopicHeaderBody_ForumIndex.Text = topicResponse.Tag.ABUSETOPICSRESPONSE_COUNT;
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("ForumsDesigner.js EventConfigurateTopicResponse dropDownListEditTopicsResponseModalContentAddTopicHeaderBody_ForumIndex.onChange", e);
            }
        }
        _this.Elements.dropDownListEditTopicResponseVotesModalContentAddTopicHeaderBody_ForumIndex.onChange = function (e) {
            try {
                var topicResponseVotes = _this.Elements.dropDownListEditTopicResponseVotesModalContentAddTopicHeaderBody_ForumIndex.SearchOption($(e.target).val());
                _this.Elements.dropDownListEditTopicResponseVotesModalContentAddTopicHeaderBody_ForumIndex.Value = topicResponseVotes.Tag.IDTOPICSRESPONSEVOTES;
                _this.Elements.dropDownListEditTopicsResponseVotesModalContentAddTopicHeaderBody_ForumIndex.Value = topicResponseVotes.Tag.IDTOPICSRESPONSE;
                _this.Elements.dropDownListEditUsersVotesModalContentAddTopicHeaderBody_ForumIndex.Value = topicResponseVotes.Tag.IDCMDBCI;
                _this.Elements.textEditLikeModalContentAddTopicHeaderBody_ForumIndex.Checked = topicResponseVotes.Tag.LIKETOPICSRESPONSE;
                _this.Elements.textEditNotLikeModalContentAddTopicHeaderBody_ForumIndex.Checked = topicResponseVotes.Tag.NOTLIKETOPICSRESPONSE;
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("ForumsDesigner.js EventConfigurateTopicResponse dropDownListEditTopicResponseVotesModalContentAddTopicHeaderBody_ForumIndex.onChange", e);
            }
        }
        _this.Elements.dropDownListEditTopicResponseAbuseModalContentAddTopicHeaderBody_ForumIndex.onChange = function (e) {
            try {
                var topicResponseAbuse = _this.Elements.dropDownListEditTopicResponseAbuseModalContentAddTopicHeaderBody_ForumIndex.SearchOption($(e.target).val());
                _this.Elements.dropDownListEditTopicResponseAbuseModalContentAddTopicHeaderBody_ForumIndex.Value = topicResponseAbuse.Tag.IDTOPICSRESPONSEABUSE;
                _this.Elements.dropDownListEditTopicsResponseAbuseModalContentAddTopicHeaderBody_ForumIndex.Value = topicResponseAbuse.Tag.IDTOPICSRESPONSE;
                _this.Elements.dropDownListEditUsersAbuseModalContentAddTopicHeaderBody_ForumIndex.Value = topicResponseAbuse.Tag.IDCMDBCI;
                _this.Elements.textEditCheckAbuseModalContentAddTopicHeaderBody_ForumIndex.Checked = topicResponseAbuse.Tag.LIKETOPICSRESPONSEABUSE;
                _this.Elements.textEditUncheckAbuseModalContentAddTopicHeaderBody_ForumIndex.Checked = topicResponseAbuse.Tag.NOTLIKETOPICSRESPONSEABUSE;
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("ForumsDesigner.js EventConfigurateTopicResponse dropDownListEditTopicResponseAbuseModalContentAddTopicHeaderBody_ForumIndex.onChange", e);
            }
        }
        _this.Elements.btnEditUpdateTopicResponseHeaderBody_ForumIndex.onClick = function () {
            try {
                var success = _this.UpdateTopicResponse();
                if (success) {
                    var dataManager = _this.TopicsResponseCreateUserManager(_this.Manager);
                    if (dataManager.ListTopicResponse.length > 0) {
                        _this.ClearDataModalTopicsResponse();
                        _this.LoadDropDownListAndDataModalTopicResponse(dataManager);
                        _this.SetDataModalTopicResponse(dataManager);
                        _this.ConfigurateDataModalTopicResponse();
                    }
                    else {
                        _this.Elements.dropDownListEditTopicsResponseModalContentAddTopicHeaderBody_ForumIndex.ClearItems();
                        var VclComboBoxItem1 = new TVclComboBoxItem();
                        VclComboBoxItem1.Value = -1;
                        VclComboBoxItem1.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "NODATA");
                        VclComboBoxItem1.Tag = new Persistence.Forums.Properties.TTOPICSRESPONSE();
                        _this.Elements.dropDownListEditTopicsResponseModalContentAddTopicHeaderBody_ForumIndex.AddItem(VclComboBoxItem1);
                        _this.Elements.dropDownListEditTopicsResponseModalContentAddTopicHeaderBody_ForumIndex.selectedIndex = 0;
                        _this.ConfigurateDataModalTopicResponse(true);
                    }
                }
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("ForumsDesigner.js EventConfigurateTopicResponse btnEditUpdateTopicResponseHeaderBody_ForumIndex.onClick()", e);
            }
        }
        _this.Elements.btnEditDeleteTopicResponseHeaderBody_ForumIndex.onClick = function () {
            try {
                var success = _this.DeleteTopicResponse();
                if (success) {
                    var dataManager = _this.TopicsResponseCreateUserManager(_this.Manager);
                    if (dataManager.ListTopicResponse.length > 0) {
                        _this.ClearDataModalTopicsResponse();
                        _this.LoadDropDownListAndDataModalTopicResponse(dataManager);
                        _this.SetDataModalTopicResponse(dataManager);
                        _this.ConfigurateDataModalTopicResponse();
                    }
                    else {
                        _this.Elements.dropDownListEditTopicsResponseModalContentAddTopicHeaderBody_ForumIndex.ClearItems();
                        var VclComboBoxItem1 = new TVclComboBoxItem();
                        VclComboBoxItem1.Value = -1;
                        VclComboBoxItem1.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "NODATA");
                        VclComboBoxItem1.Tag = new Persistence.Forums.Properties.TTOPICSRESPONSE();
                        _this.Elements.dropDownListEditTopicsResponseModalContentAddTopicHeaderBody_ForumIndex.AddItem(VclComboBoxItem1);
                        _this.Elements.dropDownListEditTopicsResponseModalContentAddTopicHeaderBody_ForumIndex.selectedIndex = 0;
                        _this.ConfigurateDataModalTopicResponse(true);
                    }
                }
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("ForumsDesigner.js EventConfigurateTopicResponse btnEditDeleteTopicResponseHeaderBody_ForumIndex.onClick()", e);
            }
        }
        _this.Elements.btnEditUpdateTopicResponseVotesHeaderBody_ForumIndex.onClick = function () {
            try {
                var success = _this.UpdateTopicResponseVotes();
                if (success) {
                    var dataManager = _this.TopicsResponseVotesCreateUserManager(_this.Manager);
                    if (dataManager.ListTopicResponseVotes.length > 0) {
                        _this.ClearDataModalTopicsResponseVotes();
                        _this.LoadDropDownListAndDataModalTopicResponseVotes(dataManager);
                        _this.SetDataModalTopicResponseVotes(dataManager);
                        _this.ConfigurateDataModalTopicResponseVotes();
                    }
                    else {
                        _this.Elements.dropDownListEditTopicResponseVotesModalContentAddTopicHeaderBody_ForumIndex.ClearItems();
                        var VclComboBoxItem1 = new TVclComboBoxItem();
                        VclComboBoxItem1.Value = -1;
                        VclComboBoxItem1.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "NODATA");
                        VclComboBoxItem1.Tag = new Persistence.Forums.Properties.TTOPICSRESPONSEVOTES();
                        _this.Elements.dropDownListEditTopicResponseVotesModalContentAddTopicHeaderBody_ForumIndex.AddItem(VclComboBoxItem1);
                        _this.Elements.dropDownListEditTopicResponseVotesModalContentAddTopicHeaderBody_ForumIndex.selectedIndex = 0;
                        _this.ConfigurateDataModalTopicResponseVotes(true);
                    }
                }
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("ForumsDesigner.js EventConfigurateTopicResponse btnEditUpdateTopicResponseVotesHeaderBody_ForumIndex.onClick()", e);
            }
        }
        _this.Elements.btnEditDeleteTopicResponseVotesHeaderBody_ForumIndex.onClick = function () {
            try {
                var success = _this.DeleteTopicResponseVotes();
                if (success) {
                    var dataManager = _this.TopicsResponseVotesCreateUserManager(_this.Manager);
                    if (dataManager.ListTopicResponseVotes.length > 0) {
                        _this.ClearDataModalTopicsResponseVotes();
                        _this.LoadDropDownListAndDataModalTopicResponseVotes(dataManager);
                        _this.SetDataModalTopicResponseVotes(dataManager);
                        _this.ConfigurateDataModalTopicResponseVotes();
                    }
                    else {
                        _this.Elements.dropDownListEditTopicResponseVotesModalContentAddTopicHeaderBody_ForumIndex.ClearItems();
                        var VclComboBoxItem1 = new TVclComboBoxItem();
                        VclComboBoxItem1.Value = -1;
                        VclComboBoxItem1.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "NODATA");
                        VclComboBoxItem1.Tag = new Persistence.Forums.Properties.TTOPICSRESPONSEVOTES();
                        _this.Elements.dropDownListEditTopicResponseVotesModalContentAddTopicHeaderBody_ForumIndex.AddItem(VclComboBoxItem1);
                        _this.Elements.dropDownListEditTopicResponseVotesModalContentAddTopicHeaderBody_ForumIndex.selectedIndex = 0;
                        _this.ConfigurateDataModalTopicResponseVotes(true);
                    }
                }
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("ForumsDesigner.js EventConfigurateTopicResponse btnEditDeleteTopicResponseVotesHeaderBody_ForumIndex.onClick()", e);
            }
        }
        _this.Elements.btnEditUpdateTopicResponseAbuseHeaderBody_ForumIndex.onClick = function () {
            try {
                var success = _this.UpdateTopicResponseAbuse();
                if (success) {
                    var dataManager = _this.TopicsResponseAbuseCreateUserManager(_this.Manager);
                    if (dataManager.ListTopicResponseAbuse.length > 0) {
                        _this.ClearDataModalTopicsResponseAbuse();
                        _this.LoadDropDownListAndDataModalTopicResponseAbuse(dataManager);
                        _this.SetDataModalTopicResponseAbuse(dataManager);
                        _this.ConfigurateDataModalTopicResponseAbuse();
                    }
                    else {
                        _this.Elements.dropDownListEditTopicResponseAbuseModalContentAddTopicHeaderBody_ForumIndex.ClearItems();
                        var VclComboBoxItem1 = new TVclComboBoxItem();
                        VclComboBoxItem1.Value = -1;
                        VclComboBoxItem1.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "NODATA");
                        VclComboBoxItem1.Tag = new Persistence.Forums.Properties.TTOPICSRESPONSEABUSE();
                        _this.Elements.dropDownListEditTopicResponseAbuseModalContentAddTopicHeaderBody_ForumIndex.AddItem(VclComboBoxItem1);
                        _this.Elements.dropDownListEditTopicResponseAbuseModalContentAddTopicHeaderBody_ForumIndex.selectedIndex = 0;
                        _this.ConfigurateDataModalTopicResponseAbuse(true);
                    }
                }
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("ForumsDesigner.js EventConfigurateTopicResponse btnEditUpdateTopicResponseAbuseHeaderBody_ForumIndex.onClick()", e);
            }
        }
        _this.Elements.btnEditDeleteTopicResponseAbuseHeaderBody_ForumIndex.onClick = function () {
            try {
                var success = _this.DeleteTopicResponseAbuse();
                if (success) {
                 var dataManager = _this.TopicsResponseAbuseCreateUserManager(_this.Manager);
                    if (dataManager.ListTopicResponseAbuse.length > 0) {
                        _this.ClearDataModalTopicsResponseAbuse();
                        _this.LoadDropDownListAndDataModalTopicResponseAbuse(dataManager);
                        _this.SetDataModalTopicResponseAbuse(dataManager);
                        _this.ConfigurateDataModalTopicResponseAbuse();
                    }
                    else {
                        _this.Elements.dropDownListEditTopicResponseAbuseModalContentAddTopicHeaderBody_ForumIndex.ClearItems();
                        var VclComboBoxItem1 = new TVclComboBoxItem();
                        VclComboBoxItem1.Value = -1;
                        VclComboBoxItem1.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "NODATA");
                        VclComboBoxItem1.Tag = new Persistence.Forums.Properties.TTOPICSRESPONSEABUSE();
                        _this.Elements.dropDownListEditTopicResponseAbuseModalContentAddTopicHeaderBody_ForumIndex.AddItem(VclComboBoxItem1);
                        _this.Elements.dropDownListEditTopicResponseAbuseModalContentAddTopicHeaderBody_ForumIndex.selectedIndex = 0;
                        _this.ConfigurateDataModalTopicResponseAbuse(true);
                    }
                }
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("ForumsDesigner.js EventConfigurateTopicResponse btnEditDeleteTopicResponseAbuseHeaderBody_ForumIndex.onClick()", e);
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.EventConfigurateTopicResponse", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.ClearDataModalTopicsResponse = function () {
    var _this = this.TParent();
    try {
        _this.Elements.dropDownListEditTopicsResponseModalContentAddTopicHeaderBody_ForumIndex.ClearItems();
        _this.Elements.dropDownListEditTopicsModalContentAddTopicHeaderBody_ForumIndex.ClearItems();
        _this.Elements.dropDownListEditUsersModalContentAddTopicHeaderBody_ForumIndex.ClearItems();
        _this.Elements.dropDownListEditTopicsParentModalContentAddTopicHeaderBody_ForumIndex.ClearItems();
        _this.Elements.textEditTitleModalContentAddTopicHeaderBody_ForumIndex.Text = "";
        _this.Elements.memoEditSubjectModalContentAddTopicHeaderBody_ForumIndex.Text = "";
        _this.Elements.textEditCreatedDateModalContentAddTopicHeaderBody_ForumIndex.Text = "";
        _this.Elements.textEditCountLikeModalContentAddTopicHeaderBody_ForumIndex.Text = "";
        _this.Elements.textEditCountNotLikeModalContentAddTopicHeaderBody_ForumIndex.Text = "";
        _this.Elements.textEditCountAbuseModalContentAddTopicHeaderBody_ForumIndex.Text = "";
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.ClearDataModalTopicsResponse", e);

    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.LoadDropDownListAndDataModalTopicResponse = function (dataManager) {
    var _this = this.TParent();
    try {
        for (i = 0; i < dataManager.ListTopicResponse.length; i++) {
            var VclComboBoxItem1 = new TVclComboBoxItem();
            VclComboBoxItem1.Value = dataManager.ListTopicResponse[i].IDTOPICSRESPONSE;
            VclComboBoxItem1.Text = dataManager.ListTopicResponse[i].SUBJECTTOPICSRESPONSE;
            VclComboBoxItem1.Tag = dataManager.ListTopicResponse[i];
            _this.Elements.dropDownListEditTopicsResponseModalContentAddTopicHeaderBody_ForumIndex.AddItem(VclComboBoxItem1);
        }
        for (i = 0; i < dataManager.ListTopic.length; i++) {
            var VclComboBoxItem2 = new TVclComboBoxItem();
            VclComboBoxItem2.Value = dataManager.ListTopic[i].IDTOPICS;
            VclComboBoxItem2.Text = dataManager.ListTopic[i].NAMETOPICS;
            VclComboBoxItem2.Tag = dataManager.ListTopic[i];
            _this.Elements.dropDownListEditTopicsModalContentAddTopicHeaderBody_ForumIndex.AddItem(VclComboBoxItem2);

        }
        for (i = 0; i < dataManager.ListUser.length; i++) {
            var VclComboBoxItem3 = new TVclComboBoxItem();
            VclComboBoxItem3.Value = dataManager.ListUser[i].IDCMDBCI;
            VclComboBoxItem3.Text = dataManager.ListUser[i].CI_GENERICNAME;
            VclComboBoxItem3.Tag = dataManager.ListUser[i];
            _this.Elements.dropDownListEditUsersModalContentAddTopicHeaderBody_ForumIndex.AddItem(VclComboBoxItem3);
        }
        var VclComboBoxItem0 = new TVclComboBoxItem();
        VclComboBoxItem0.Value = 0;
        VclComboBoxItem0.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "PARENT");
        VclComboBoxItem0.Tag = new Persistence.Forums.Properties.TTOPICSRESPONSE();
        _this.Elements.dropDownListEditTopicsParentModalContentAddTopicHeaderBody_ForumIndex.AddItem(VclComboBoxItem0);
        for (i = 0; i < dataManager.ListTopicResponseParent.length; i++) {
            var VclComboBoxItem4 = new TVclComboBoxItem();
            VclComboBoxItem4.Value = dataManager.ListTopicResponseParent[i].IDTOPICSRESPONSE;
            VclComboBoxItem4.Text = dataManager.ListTopicResponseParent[i].SUBJECTTOPICSRESPONSE;
            VclComboBoxItem4.Tag = dataManager.ListTopicResponseParent[i];
            _this.Elements.dropDownListEditTopicsParentModalContentAddTopicHeaderBody_ForumIndex.AddItem(VclComboBoxItem4);
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.LoadDropDownListAndDataModalTopicResponse", e);

    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.SetDataModalTopicResponse = function (dataManager) {
    var _this = this.TParent();
    try {
        var selectTopicResponse = _this.Elements.dropDownListEditTopicsResponseModalContentAddTopicHeaderBody_ForumIndex;
        selectTopicResponse.selectedIndex = 0;;
        var topicResponse = _this.Elements.dropDownListEditTopicsResponseModalContentAddTopicHeaderBody_ForumIndex.SearchOption(selectTopicResponse.Value);
        for (var i = 0; i < dataManager.ListTopicResponse.length; i++) {
            if (dataManager.ListTopicResponse[i].IDTOPICSRESPONSE == selectTopicResponse.Value) {
                _this.Elements.dropDownListEditTopicsResponseModalContentAddTopicHeaderBody_ForumIndex.Value = topicResponse.Tag.IDTOPICSRESPONSE;
                _this.Elements.dropDownListEditTopicsModalContentAddTopicHeaderBody_ForumIndex.Value = topicResponse.Tag.IDTOPICS;
                _this.Elements.dropDownListEditUsersModalContentAddTopicHeaderBody_ForumIndex.Value = topicResponse.Tag.IDCMDBCI;
                _this.Elements.dropDownListEditTopicsParentModalContentAddTopicHeaderBody_ForumIndex.Value = topicResponse.Tag.IDTOPICSRESPONSE_PARENT;
                _this.Elements.textEditTitleModalContentAddTopicHeaderBody_ForumIndex.Text = topicResponse.Tag.TITLETOPICSRESPONSE;
                _this.Elements.memoEditSubjectModalContentAddTopicHeaderBody_ForumIndex.Text = topicResponse.Tag.SUBJECTTOPICSRESPONSE;
                _this.Elements.textEditCreatedDateModalContentAddTopicHeaderBody_ForumIndex.Text = topicResponse.Tag.CREATEDDATETOPICSRESPONSE;
                _this.Elements.textEditCountLikeModalContentAddTopicHeaderBody_ForumIndex.Text = topicResponse.Tag.LIKETOPICSRESPONSE_COUNT;
                _this.Elements.textEditCountNotLikeModalContentAddTopicHeaderBody_ForumIndex.Text = topicResponse.Tag.NOTLIKETOPICSRESPONSE_COUNT;
                _this.Elements.textEditCountAbuseModalContentAddTopicHeaderBody_ForumIndex.Text = topicResponse.Tag.ABUSETOPICSRESPONSE_COUNT;
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.SetDataModalTopicResponse", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.ConfigurateDataModalTopicResponse = function (allHide) {
    var _this = this.TParent();
    try {
        var _this = this.TParent();
        if (_this.Manager) {
            $(_this.Elements.modalBoxEditTopicsContentAddTopicHeaderBody_ForumIndex.Row.This).show();
            $(_this.Elements.modalBoxEditUsersContentAddTopicHeaderBody_ForumIndex.Row.This).show();
            $(_this.Elements.modalBoxEditTopicsParentContentAddTopicHeaderBody_ForumIndex.Row.This).show();
            $(_this.Elements.modalBoxEditTitleContentAddTopicHeaderBody_ForumIndex.Row.This).show();
            $(_this.Elements.modalBoxEditSubjectContentAddTopicHeaderBody_ForumIndex.Row.This).show();
            $(_this.Elements.modalBoxEditCreatedDateContentAddTopicHeaderBody_ForumIndex.Row.This).show();
            $(_this.Elements.modalBoxEditCountLikeContentAddTopicHeaderBody_ForumIndex.Row.This).show();
            $(_this.Elements.modalBoxEditCountNotLikeContentAddTopicHeaderBody_ForumIndex.Row.This).show();
            $(_this.Elements.modalBoxEditCountAbuseContentAddTopicHeaderBody_ForumIndex.Row.This).show();
        }
        else {
            $(_this.Elements.modalBoxEditTopicsContentAddTopicHeaderBody_ForumIndex.Row.This).hide();
            $(_this.Elements.modalBoxEditUsersContentAddTopicHeaderBody_ForumIndex.Row.This).hide();
            $(_this.Elements.modalBoxEditTopicsParentContentAddTopicHeaderBody_ForumIndex.Row.This).hide();
            $(_this.Elements.modalBoxEditTitleContentAddTopicHeaderBody_ForumIndex.Row.This).show();
            $(_this.Elements.modalBoxEditSubjectContentAddTopicHeaderBody_ForumIndex.Row.This).show();
            $(_this.Elements.modalBoxEditCreatedDateContentAddTopicHeaderBody_ForumIndex.Row.This).show();
            $(_this.Elements.modalBoxEditCountLikeContentAddTopicHeaderBody_ForumIndex.Row.This).hide();
            $(_this.Elements.modalBoxEditCountNotLikeContentAddTopicHeaderBody_ForumIndex.Row.This).hide();
            $(_this.Elements.modalBoxEditCountAbuseContentAddTopicHeaderBody_ForumIndex.Row.This).hide();
        }
        if (allHide) {
            $(_this.Elements.modalBoxEditTopicsContentAddTopicHeaderBody_ForumIndex.Row.This).hide();
            $(_this.Elements.modalBoxEditUsersContentAddTopicHeaderBody_ForumIndex.Row.This).hide();
            $(_this.Elements.modalBoxEditTopicsParentContentAddTopicHeaderBody_ForumIndex.Row.This).hide();
            $(_this.Elements.modalBoxEditTitleContentAddTopicHeaderBody_ForumIndex.Row.This).hide();
            $(_this.Elements.modalBoxEditSubjectContentAddTopicHeaderBody_ForumIndex.Row.This).hide();
            $(_this.Elements.modalBoxEditCreatedDateContentAddTopicHeaderBody_ForumIndex.Row.This).hide();
            $(_this.Elements.modalBoxEditCountLikeContentAddTopicHeaderBody_ForumIndex.Row.This).hide();
            $(_this.Elements.modalBoxEditCountNotLikeContentAddTopicHeaderBody_ForumIndex.Row.This).hide();
            $(_this.Elements.modalBoxEditCountAbuseContentAddTopicHeaderBody_ForumIndex.Row.This).hide();
        }
    } catch (e) {
            SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.ConfigurateDataModalTopicResponse", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.ClearDataModalTopicsResponseVotes = function () {
    var _this = this.TParent();
    try {
        _this.Elements.dropDownListEditTopicResponseVotesModalContentAddTopicHeaderBody_ForumIndex.ClearItems();
        _this.Elements.dropDownListEditTopicsResponseVotesModalContentAddTopicHeaderBody_ForumIndex.ClearItems();
        _this.Elements.dropDownListEditUsersVotesModalContentAddTopicHeaderBody_ForumIndex.ClearItems();
        _this.Elements.textEditLikeModalContentAddTopicHeaderBody_ForumIndex.Checked = false;
        _this.Elements.textEditNotLikeModalContentAddTopicHeaderBody_ForumIndex.Checked = false;

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.EventConfigurateTopicResponseVotes", e);

    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.LoadDropDownListAndDataModalTopicResponseVotes = function (dataManager) {
    var _this = this.TParent();
    try {
        for (i = 0; i < dataManager.ListTopicResponseVotes.length; i++) {
            var VclComboBoxItem1 = new TVclComboBoxItem();
            VclComboBoxItem1.Value = dataManager.ListTopicResponseVotes[i].IDTOPICSRESPONSEVOTES;
            VclComboBoxItem1.Text = dataManager.ListTopicResponseVotes[i].IDTOPICSRESPONSEVOTES;
            VclComboBoxItem1.Tag = dataManager.ListTopicResponseVotes[i];
            _this.Elements.dropDownListEditTopicResponseVotesModalContentAddTopicHeaderBody_ForumIndex.AddItem(VclComboBoxItem1);
        }
        for (i = 0; i < dataManager.ListTopicResponse.length; i++) {
            var VclComboBoxItem2 = new TVclComboBoxItem();
            VclComboBoxItem2.Value = dataManager.ListTopicResponse[i].IDTOPICSRESPONSE;
            VclComboBoxItem2.Text = dataManager.ListTopicResponse[i].SUBJECTTOPICSRESPONSE;
            VclComboBoxItem2.Tag = dataManager.ListTopicResponse[i];
            _this.Elements.dropDownListEditTopicsResponseVotesModalContentAddTopicHeaderBody_ForumIndex.AddItem(VclComboBoxItem2);

        }
        for (i = 0; i < dataManager.ListUser.length; i++) {
            var VclComboBoxItem3 = new TVclComboBoxItem();
            VclComboBoxItem3.Value = dataManager.ListUser[i].IDCMDBCI;
            VclComboBoxItem3.Text = dataManager.ListUser[i].CI_GENERICNAME;
            VclComboBoxItem3.Tag = dataManager.ListUser[i];
            _this.Elements.dropDownListEditUsersVotesModalContentAddTopicHeaderBody_ForumIndex.AddItem(VclComboBoxItem3);
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.EventConfigurateTopicResponseVotes", e);

    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.SetDataModalTopicResponseVotes = function (dataManager) {
    var _this = this.TParent();
    try {
        var selectTopicResponseVotes = _this.Elements.dropDownListEditTopicResponseVotesModalContentAddTopicHeaderBody_ForumIndex;
        selectTopicResponseVotes.selectedIndex = 0;
        var topicResponseVotes = _this.Elements.dropDownListEditTopicResponseVotesModalContentAddTopicHeaderBody_ForumIndex.SearchOption(selectTopicResponseVotes.Value);
        for (var i = 0; i < dataManager.ListTopicResponseVotes.length; i++) {
            if (dataManager.ListTopicResponseVotes[i].IDTOPICSRESPONSEVOTES == selectTopicResponseVotes.Value) {
                _this.Elements.dropDownListEditTopicResponseVotesModalContentAddTopicHeaderBody_ForumIndex.Value = topicResponseVotes.Tag.IDTOPICSRESPONSEVOTES;
                _this.Elements.dropDownListEditTopicsResponseVotesModalContentAddTopicHeaderBody_ForumIndex.Value = topicResponseVotes.Tag.IDTOPICSRESPONSE;
                _this.Elements.dropDownListEditUsersVotesModalContentAddTopicHeaderBody_ForumIndex.Value = topicResponseVotes.Tag.IDCMDBCI;
                _this.Elements.textEditLikeModalContentAddTopicHeaderBody_ForumIndex.Checked = topicResponseVotes.Tag.LIKETOPICSRESPONSE;
                _this.Elements.textEditNotLikeModalContentAddTopicHeaderBody_ForumIndex.Checked = topicResponseVotes.Tag.NOTLIKETOPICSRESPONSE;
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.SetDataModalTopicResponseVotes", e);

    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.ConfigurateDataModalTopicResponseVotes = function (allHide) {
    var _this = this.TParent();
    try {
        if (_this.Manager) {
            $(_this.Elements.modalBoxEditTopicsResponseVotesContentAddTopicHeaderBody_ForumIndex.Row.This).show();
            $(_this.Elements.modalBoxEditUsersVotesContentAddTopicHeaderBody_ForumIndex.Row.This).show();
            $(_this.Elements.modalBoxEditLikeContentAddTopicHeaderBody_ForumIndex.Row.This).show();
            $(_this.Elements.modalBoxEditNotLikeContentAddTopicHeaderBody_ForumIndex.Row.This).show();
        }
        else {
            $(_this.Elements.modalBoxEditTopicsResponseVotesContentAddTopicHeaderBody_ForumIndex.Row.This).hide();
            $(_this.Elements.modalBoxEditUsersVotesContentAddTopicHeaderBody_ForumIndex.Row.This).hide();
            $(_this.Elements.modalBoxEditLikeContentAddTopicHeaderBody_ForumIndex.Row.This).show();
            $(_this.Elements.modalBoxEditNotLikeContentAddTopicHeaderBody_ForumIndex.Row.This).show();
        }
        if (allHide) {
            $(_this.Elements.modalBoxEditTopicsResponseVotesContentAddTopicHeaderBody_ForumIndex.Row.This).hide();
            $(_this.Elements.modalBoxEditUsersVotesContentAddTopicHeaderBody_ForumIndex.Row.This).hide();
            $(_this.Elements.modalBoxEditLikeContentAddTopicHeaderBody_ForumIndex.Row.This).hide();
            $(_this.Elements.modalBoxEditNotLikeContentAddTopicHeaderBody_ForumIndex.Row.This).hide();
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.ConfigurateDataModalTopicResponseVotes", e);

    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.ClearDataModalTopicsResponseAbuse = function () {
    var _this = this.TParent();
    try {
        _this.Elements.dropDownListEditTopicResponseAbuseModalContentAddTopicHeaderBody_ForumIndex.ClearItems();
        _this.Elements.dropDownListEditTopicsResponseAbuseModalContentAddTopicHeaderBody_ForumIndex.ClearItems();
        _this.Elements.dropDownListEditUsersAbuseModalContentAddTopicHeaderBody_ForumIndex.ClearItems();
        _this.Elements.textEditCheckAbuseModalContentAddTopicHeaderBody_ForumIndex.Checked = false;
        _this.Elements.textEditUncheckAbuseModalContentAddTopicHeaderBody_ForumIndex.Checked = false;

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.EventConfigurateTopicResponseAbuse", e);

    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.LoadDropDownListAndDataModalTopicResponseAbuse = function (dataManager) {
    var _this = this.TParent();
    try {
        for (i = 0; i < dataManager.ListTopicResponseAbuse.length; i++) {
            var VclComboBoxItem1 = new TVclComboBoxItem();
            VclComboBoxItem1.Value = dataManager.ListTopicResponseAbuse[i].IDTOPICSRESPONSEABUSE;
            VclComboBoxItem1.Text = dataManager.ListTopicResponseAbuse[i].IDTOPICSRESPONSEABUSE;
            VclComboBoxItem1.Tag = dataManager.ListTopicResponseAbuse[i];
            _this.Elements.dropDownListEditTopicResponseAbuseModalContentAddTopicHeaderBody_ForumIndex.AddItem(VclComboBoxItem1);
        }
        for (i = 0; i < dataManager.ListTopicResponse.length; i++) {
            var VclComboBoxItem2 = new TVclComboBoxItem();
            VclComboBoxItem2.Value = dataManager.ListTopicResponse[i].IDTOPICSRESPONSE;
            VclComboBoxItem2.Text = dataManager.ListTopicResponse[i].SUBJECTTOPICSRESPONSE;
            VclComboBoxItem2.Tag = dataManager.ListTopicResponse[i];
            _this.Elements.dropDownListEditTopicsResponseAbuseModalContentAddTopicHeaderBody_ForumIndex.AddItem(VclComboBoxItem2);

        }
        for (i = 0; i < dataManager.ListUser.length; i++) {
            var VclComboBoxItem3 = new TVclComboBoxItem();
            VclComboBoxItem3.Value = dataManager.ListUser[i].IDCMDBCI;
            VclComboBoxItem3.Text = dataManager.ListUser[i].CI_GENERICNAME;
            VclComboBoxItem3.Tag = dataManager.ListUser[i];
            _this.Elements.dropDownListEditUsersAbuseModalContentAddTopicHeaderBody_ForumIndex.AddItem(VclComboBoxItem3);
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.LoadDropDownListAndDataModalTopicResponseAbuse", e);

    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.SetDataModalTopicResponseAbuse = function (dataManager) {
    var _this = this.TParent();
    try {
        var selectTopicResponseAbuse = _this.Elements.dropDownListEditTopicResponseAbuseModalContentAddTopicHeaderBody_ForumIndex;
        selectTopicResponseAbuse.selectedIndex = 0;
        var topicResponseAbuse = _this.Elements.dropDownListEditTopicResponseAbuseModalContentAddTopicHeaderBody_ForumIndex.SearchOption(selectTopicResponseAbuse.Value);
        for (var i = 0; i < dataManager.ListTopicResponseAbuse.length; i++) {
            if (dataManager.ListTopicResponseAbuse[i].IDTOPICSRESPONSEABUSE == selectTopicResponseAbuse.Value) {
                _this.Elements.dropDownListEditTopicResponseAbuseModalContentAddTopicHeaderBody_ForumIndex.Value = topicResponseAbuse.Tag.IDTOPICSRESPONSEABUSE;
                _this.Elements.dropDownListEditTopicsResponseAbuseModalContentAddTopicHeaderBody_ForumIndex.Value = topicResponseAbuse.Tag.IDTOPICSRESPONSE;
                _this.Elements.dropDownListEditUsersAbuseModalContentAddTopicHeaderBody_ForumIndex.Value = topicResponseAbuse.Tag.IDCMDBCI;
                _this.Elements.textEditCheckAbuseModalContentAddTopicHeaderBody_ForumIndex.Checked = topicResponseAbuse.Tag.LIKETOPICSRESPONSEABUSE;
                _this.Elements.textEditUncheckAbuseModalContentAddTopicHeaderBody_ForumIndex.Checked = topicResponseAbuse.Tag.NOTLIKETOPICSRESPONSEABUSE;
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.SetDataModalTopicResponseAbuse", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.ConfigurateDataModalTopicResponseAbuse = function (allHide) {
    var _this = this.TParent();
    try {
        if (_this.Manager) {
            $(_this.Elements.modalBoxEditTopicsResponseAbuseContentAddTopicHeaderBody_ForumIndex.Row.This).show();
            $(_this.Elements.modalBoxEditUsersAbuseContentAddTopicHeaderBody_ForumIndex.Row.This).show();
            $(_this.Elements.modalBoxEditCheckAbuseContentAddTopicHeaderBody_ForumIndex.Row.This).show();
            $(_this.Elements.modalBoxEditUncheckAbuseContentAddTopicHeaderBody_ForumIndex.Row.This).show(); 
        }
        else {
            $(_this.Elements.modalBoxEditTopicsResponseAbuseContentAddTopicHeaderBody_ForumIndex.Row.This).hide();
            $(_this.Elements.modalBoxEditUsersAbuseContentAddTopicHeaderBody_ForumIndex.Row.This).hide();
            $(_this.Elements.modalBoxEditCheckAbuseContentAddTopicHeaderBody_ForumIndex.Row.This).show();
            $(_this.Elements.modalBoxEditUncheckAbuseContentAddTopicHeaderBody_ForumIndex.Row.This).show(); 
        }
        if (allHide) {
            $(_this.Elements.modalBoxEditTopicsResponseAbuseContentAddTopicHeaderBody_ForumIndex.Row.This).hide();
            $(_this.Elements.modalBoxEditUsersAbuseContentAddTopicHeaderBody_ForumIndex.Row.This).hide();
            $(_this.Elements.modalBoxEditCheckAbuseContentAddTopicHeaderBody_ForumIndex.Row.This).hide();
            $(_this.Elements.modalBoxEditUncheckAbuseContentAddTopicHeaderBody_ForumIndex.Row.This).hide(); 
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.ConfigurateDataModalTopicResponseAbuse", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentResponseCommentChild = function (objResponseComment) {
    var _this = this.TParent();
    var _listCommentChild = new Array();
    var listCommentChild = objResponseComment.Data.ResponseInitListChildComment;
    var nowDate = new Date().getTime().toString();
    var grandchild = objResponseComment.DrawChild;
    try {
        for (var i = 0; i < listCommentChild.length; i++) {
            var commentChild = new Object();
            if (objResponseComment.DrawChild) {
                commentChild.MediaPostChild = new Udiv(objResponseComment.ObjectElementPost.MediaPostBody, ("MediaPostChild_" + objResponseComment.Id + "_" + i + "_" + nowDate));
            }
            else {
                commentChild.MediaPostChild = new Udiv("", ("MediaPostChild_" + objResponseComment.Id + "_" + i + "_" + nowDate));
                $(commentChild.MediaPostChild).insertBefore(objResponseComment.ObjectElementPost.CommentUser.FooterPostComentInput_ForumIndex);
            }
            $(commentChild.MediaPostChild).addClass("media mediaPost_last commentChildHide");
            commentChild.MediaPostBoxInitImage = new Udiv(commentChild.MediaPostChild, ("MediaPostBoxImage_" + objResponseComment.Id + "_" + i + "_" + nowDate));
            $(commentChild.MediaPostBoxInitImage).addClass("media-left");
            //
            if (i == 0) {
                $(objResponseComment.ObjectElementPost.FooterLinkActionPostComment_ForumIndex).on("click", { Box: commentChild.MediaPostChild }, function (event) {
                    event.preventDefault();
                    $(event.data.Box).toggle("slow");
                    if ($(event.data.Box).siblings("div").filter(".mediaPost_last").length > 0) {
                        $(event.data.Box).siblings("div").filter(".mediaPost_last").toggle("slow");
                    }
                });
            }
            //
            //FIX ELEMENT NEW COMMENT
            commentChild.FooterBodyPostComent = commentChild.MediaPostBoxInitImage;

            commentChild.MediaPostBoxImage = new Uspan(commentChild.MediaPostBoxInitImage, ("MediaPostBoxImage_" + objResponseComment.Id + "_" + i + "_" + nowDate), "");
            $(commentChild.MediaPostBoxImage).addClass("thumb-xs avatar pull-left mr-sm");
            commentChild.MediaImage = new Uimg(commentChild.MediaPostBoxImage, ("MediaImage_" + objResponseComment.Id + "_" + i + "_" + nowDate), listCommentChild[i].ResponseInitCreatedUser.Image, "");
            $(commentChild.MediaImage).addClass("img-circle");
            commentChild.MediaPostBody = new Udiv(commentChild.MediaPostBoxInitImage, ("MediaPostBody_" + objResponseComment.Id + "_" + i + "_" + nowDate));
            $(commentChild.MediaPostBody).addClass("media-body");
            
            commentChild.MediaPostComentAutor = new Uh6(commentChild.MediaPostBody, ("MediaPostComentAutor_" + objResponseComment.Id + "_" + i + "_" + nowDate), listCommentChild[i].ResponseInitCreatedUser.Name);
            $(commentChild.MediaPostComentAutor).addClass("author fw-semi-bold");
            commentChild.MediaPostComentDate = new Usmall(commentChild.MediaPostComentAutor, ("MediaPostComentDate_" + objResponseComment.Id + "_" + i + "_" + nowDate), listCommentChild[i].ResponseInitCreatedDate);
            $(commentChild.MediaPostComentDate).addClass("createdDateTopicResponse");
            commentChild.MediaPostComent = new Up(commentChild.MediaPostBody, ("MediaPostComent_" + objResponseComment.Id + "_" + i + "_" + nowDate), listCommentChild[i].ResponseInitDescription);
            $(commentChild.MediaPostComent).addClass("descriptionTopicResponse");
            commentChild.MediaPostLinkAction = new Uul(commentChild.MediaPostBody, ("MediaPostLinkAction_" + objResponseComment.Id + "_" + i + "_" + nowDate));
            $(commentChild.MediaPostLinkAction).addClass("post-links");

            commentChild.BoxFooterActionPostLike_ForumIndex = new Uli(commentChild.MediaPostLinkAction, ("BoxFooterActionPostLike_ForumIndex_" + objResponseComment.Id + "_" + i + "_" + nowDate));
            commentChild.FooterLinkActionPostLike_ForumIndex = new Ua(commentChild.BoxFooterActionPostLike_ForumIndex, ("FooterLinkActionPostLike_ForumIndex_" + objResponseComment.Id + "_" + i + "_" + nowDate), "");
            $(commentChild.FooterLinkActionPostLike_ForumIndex).addClass("linkIcon");
            commentChild.FooterLinkActionCountLike_ForumIndex = new Uspan(commentChild.FooterLinkActionPostLike_ForumIndex, ("FooterLinkActionCountLike_ForumIndex_" + objResponseComment.Id + "_" + i + "_" + nowDate), "0");
            commentChild.FooterLinkActionIconLike_ForumIndex = new Ui(commentChild.FooterLinkActionCountLike_ForumIndex, ("FooterLinkActionIconLike_ForumIndex_" + objResponseComment.Id + "_" + i + "_" + nowDate));
            $(commentChild.FooterLinkActionIconLike_ForumIndex).addClass("far fa-thumbs-up Emoticon");

            commentChild.BoxFooterActionPostNotLike_ForumIndex = new Uli(commentChild.MediaPostLinkAction, ("BoxFooterActionPostNotLike_ForumIndex_" + objResponseComment.Id + "_" + i + "_" + nowDate));
            commentChild.FooterLinkActionPostNotLike_ForumIndex = new Ua(commentChild.BoxFooterActionPostNotLike_ForumIndex, ("FooterLinkActionPostNotLike_ForumIndex_" + objResponseComment.Id + "_" + i + "_" + nowDate), "");
            $(commentChild.FooterLinkActionPostNotLike_ForumIndex).addClass("linkIcon");
            commentChild.FooterLinkActionCountNotLike_ForumIndex = new Uspan(commentChild.FooterLinkActionPostNotLike_ForumIndex, ("FooterLinkActionCountNotLike_ForumIndex_" + objResponseComment.Id + "_" + i + "_" + nowDate), "0");
            commentChild.FooterLinkActionIconNotLike_ForumIndex = new Ui(commentChild.FooterLinkActionCountNotLike_ForumIndex, ("FooterLinkActionIconNotLike_ForumIndex_" + objResponseComment.Id + "_" + i + "_" + nowDate));
            $(commentChild.FooterLinkActionIconNotLike_ForumIndex).addClass("far fa-thumbs-down Emoticon");

            commentChild.BoxFooterLinkActionPostAbuse_ForumIndex = new Uli(commentChild.MediaPostLinkAction, ("BoxFooterLinkActionPostAbuse_ForumIndex_" + objResponseComment.Id + "_" + i + "_" + nowDate));
            commentChild.FooterLinkActionPostAbuse_ForumIndex = new Ua(commentChild.BoxFooterLinkActionPostAbuse_ForumIndex, ("FooterLinkActionPostAbuse_ForumIndex_" + objResponseComment.Id + "_" + i + "_" + nowDate), "");
            $(commentChild.FooterLinkActionPostAbuse_ForumIndex).addClass("linkIcon");
            commentChild.FooterLinkActionCountAbuse_ForumIndex = new Uspan(commentChild.FooterLinkActionPostAbuse_ForumIndex, ("FooterLinkActionCountAbuse_ForumIndex_" + objResponseComment.Id + "_" + i + "_" + nowDate), "0");
            commentChild.FooterLinkActionIconAbuse_ForumIndex = new Ui(commentChild.FooterLinkActionCountAbuse_ForumIndex, ("FooterLinkActionIconAbuse_ForumIndex_" + objResponseComment.Id + "_" + i + "_" + nowDate));
            $(commentChild.FooterLinkActionIconAbuse_ForumIndex).addClass("fas fa-exclamation-triangle Emoticon");

            commentChild.BoxFooterLinkActionPostComment_ForumIndex = new Uli(commentChild.MediaPostLinkAction, ("BoxFooterLinkActionPostComment_ForumIndex_" + objResponseComment.Id + "_" + i + "_" + nowDate));
            commentChild.FooterLinkActionPostComment_ForumIndex = new Ua(commentChild.BoxFooterLinkActionPostComment_ForumIndex, ("FooterLinkActionPostComment_ForumIndex_" + objResponseComment.Id + "_" + i + "_" + nowDate), "");
            $(commentChild.FooterLinkActionPostComment_ForumIndex).addClass("linkIcon flagComment");
            commentChild.FooterLinkActionCountComment_ForumIndex = new Uspan(commentChild.FooterLinkActionPostComment_ForumIndex, ("FooterLinkActionCountComment_ForumIndex_" + objResponseComment.Id + "_" + i + "_" + nowDate), "0");
            commentChild.FooterLinkActionIconComment_ForumIndex = new Ui(commentChild.FooterLinkActionCountComment_ForumIndex, ("FooterLinkActionIconComment_ForumIndex_" + objResponseComment.Id + "_" + i + "_" + nowDate));
            $(commentChild.FooterLinkActionIconComment_ForumIndex).addClass("far fa-comments Emoticon");

            commentChild.BoxFooterLinkActionNewComment_ForumIndex = new Uli(commentChild.MediaPostLinkAction, ("BoxFooterLinkActionNewComment_ForumIndex_" + objResponseComment.Id + "_" + i + "_" + nowDate));
            commentChild.FooterLinkActionNewComment_ForumIndex = new Ua(commentChild.BoxFooterLinkActionNewComment_ForumIndex, ("FooterLinkActionNewComment_ForumIndex_" + objResponseComment.Id + "_" + i + "_" + nowDate), "");
            $(commentChild.FooterLinkActionNewComment_ForumIndex).addClass("linkIcon");
            commentChild.FooterLinkActionNewCountComment_ForumIndex = new Uspan(commentChild.FooterLinkActionNewComment_ForumIndex, ("FooterLinkActionNewCountComment_ForumIndex_" + objResponseComment.Id + "_" + i + "_" + nowDate), "");
            commentChild.FooterLinkActionNewIconComment_ForumIndex = new Ui(commentChild.FooterLinkActionNewCountComment_ForumIndex, ("FooterLinkActionNewIconComment_ForumIndex_" + objResponseComment.Id + "_" + i + "_" + nowDate));
            $(commentChild.FooterLinkActionNewIconComment_ForumIndex).addClass("fab fa-facebook-messenger");

            _this.PaintAndEventsIcons(commentChild, listCommentChild[i]);
            commentChild.CommentUser = _this.ComponentResponseComment({ ObjectElementPost: commentChild, Id: (objResponseComment.Id + "_" + i + "_" + nowDate), InitComment: false, DataWork: listCommentChild[i], Grandchild: true });
            commentChild.ListCommentChild = _this.ComponentResponseCommentChild({ ObjectElementPost: commentChild, Id: (objResponseComment.Id), Data: listCommentChild[i],DrawChild:true });
            $(commentChild.FooterLinkActionNewComment_ForumIndex).on("click", { Box: commentChild.CommentUser.FooterPostComentInput_ForumIndex, BoxText: commentChild.CommentUser.PostComentInput_ForumIndex }, function (event) {
                $(event.data.Box).toggle("slow");
                event.data.BoxText.Text = "";
            });
            _listCommentChild.push(commentChild);
        }
    }
    catch (e){
        SysCfg.Log.Methods.WriteLog("ForumsDesigner.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.ComponentResponseCommentChild", e);
    }
    return (_listCommentChild);
}
