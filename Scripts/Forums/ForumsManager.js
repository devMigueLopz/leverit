ItHelpCenter.TForumsManager.ForumsManager = function  (inMenuObject, inObjectContent, inid, callback) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.MenuObject = inMenuObject;
    this.ObjectContent = inObjectContent;
    _this.id = inid;
    this.Mythis = "ForumsManager";
    UsrCfg.Traslate.GetLangText(this.Mythis, "MANAGERSEARCH", "Manager Search");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ID", "Id");
    UsrCfg.Traslate.GetLangText(this.Mythis, "NAME", "Name");
    UsrCfg.Traslate.GetLangText(this.Mythis, "TEXTNAME", "Text in Name");
    UsrCfg.Traslate.GetLangText(this.Mythis, "QUERY", "Query");
    UsrCfg.Traslate.GetLangText(this.Mythis, "TEXTQUERY", "Text in query");
    UsrCfg.Traslate.GetLangText(this.Mythis, "IMAGE", "Image");
    UsrCfg.Traslate.GetLangText(this.Mythis, "TEXTIMAGE", "Text path in image");
    UsrCfg.Traslate.GetLangText(this.Mythis, "FORUM", "Forum");
    UsrCfg.Traslate.GetLangText(this.Mythis, "TEXTIMAGE", "Text path in image");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ADD", "Add");
    UsrCfg.Traslate.GetLangText(this.Mythis, "UPDATE", "Update");
    UsrCfg.Traslate.GetLangText(this.Mythis, "DELETE", "Delete");
    UsrCfg.Traslate.GetLangText(this.Mythis, "SELECTELEMENT", "Select Element");
    UsrCfg.Traslate.GetLangText(this.Mythis, "LISTFORUMSBD", "List Forums BD");
    UsrCfg.Traslate.GetLangText(this.Mythis, "SUCCESSADD", "The data was saved correctly.");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ERRORADD", "An error occurred while saving the data");
    UsrCfg.Traslate.GetLangText(this.Mythis, "SUCCESSUPDATE", "The data was updated correctly.");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ERRORUPDATE ", "An error occurred when updating the data");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ERRORIDFORUMSSEARCH", "Failed to enter the Forum Search ID. Enter a correct data.");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ERRORFORUMSSEARCHNAME", "Failed to enter the Forum Search Name. Enter a correct data.");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ERRORFORUMSSEARCHIMAGE", "Failed to enter the Forum Search Image. Enter a correct data.");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ERRORFORUMSSEARCHQUERY", "Failed to enter the Forum Search Query. Enter a correct data.");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ERRORIDFORUMS", "Failed to enter the Forum ID. Enter a correct data.");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ELIMINATEDELEMENTCORRECTLY", "Eliminated Element Correctly");
    UsrCfg.Traslate.GetLangText(this.Mythis, "ELEMENTNOTELIMINATED", "Element Not Eliminated");




    //UsrCfg.Traslate.GetLangText(this.Mythis, "REMOVEALLELEMENTS", "Remove All Elements");
    //UsrCfg.Traslate.GetLangText(this.Mythis, "SELECTELEMENT", "Select Element");
    //UsrCfg.Traslate.GetLangText(this.Mythis, "GETSELECTELEMENT", "Get Select Element");
    //UsrCfg.Traslate.GetLangText(this.Mythis, "GETELEMENT", "Get Element");
    //UsrCfg.Traslate.GetLangText(this.Mythis, "ELEMENTADDEDCORRECTLY", "Element Added Correctly");
    //UsrCfg.Traslate.GetLangText(this.Mythis, "EDITELEMENTCORRECTLY", "Edited Element Correctly");
  
    //UsrCfg.Traslate.GetLangText(this.Mythis, "ELIMINATSEDELEMENTCORRECTLY", "Eliminated Elements Correctly");
    //UsrCfg.Traslate.GetLangText(this.Mythis, "ELEMENTCHANGEDCORRECTLY", "Element Changed Correctly");
    //UsrCfg.Traslate.GetLangText(this.Mythis, "ELEMENTNOTADDED", "Element not Added");
    //UsrCfg.Traslate.GetLangText(this.Mythis, "ELEMENTNOTFOUND", "Element not Found");
    //UsrCfg.Traslate.GetLangText(this.Mythis, "ELEMENTNOTEDITED", "Element not edited");
    //UsrCfg.Traslate.GetLangText(this.Mythis, "ELEMENTSNOTELIMINATED", "Elemens Not Eliminated");

    this.InitializeComponent();
    this.LoadComponent();
}.Implements(new Source.Page.Properties.Page());
ItHelpCenter.TForumsManager.ForumsManager.prototype.LoadComponent = function (_this, sender) {
    var _this = this.TParent();
    try {
        var listForum = new Array();
        Persistence.Forums.Methods.FORUMS_Fill(listForum, "");
        listForum.forEach(function (element) {
            var VclComboBoxItem1 = new TVclComboBoxItem();  
            VclComboBoxItem1.Value = element.IDFORUMS;
            VclComboBoxItem1.Text = element.NAMEFORUMS; 
            VclComboBoxItem1.Tag = element; 
            _this.cbCombobox.AddItem(VclComboBoxItem1)
            _this.cbCombobox.Value = -1;
        });
        _this.LoadCompleteSync = function () {
            try {
                listForum.forEach(function (element) {
                    var VclComboBoxItem1 = new TVclComboBoxItem();
                    VclComboBoxItem1.Value = element.IDFORUMS;
                    VclComboBoxItem1.Text = element.NAMEFORUMS;
                    VclComboBoxItem1.Tag = element;
                    _this.cbComboboxFrBd.AddItem(VclComboBoxItem1)
                });
                $(_this.cbComboboxFrBd.This).val($("#" + _this.cbComboboxFrBd.This.id + " option:first").val())
                var listForumsSearch = new Array();
                Persistence.Forums.Methods.FORUMSSEARCH_Fill(listForumsSearch, "");
                listForumsSearch.forEach(function (element) {
                    if (element.IDFORUMS == _this.cbComboboxFrBd.Value) {
                        var data = new _this.listView.Member();
                        data.Id = element.IDFORUMSSEARCH;
                        data.Value = element.NAMEFORUMSSEARCH;
                        data.Description = element.QUERYFORUMSSEARCH;
                        data.Tag = element;
                        _this.listView.AddElement(data);
                    }
                });

            } catch (e) {
                SysCfg.Log.Methods.WriteLog("ForumsManager.js ItHelpCenter.TForumsManager.ForumsManager.prototype.LoadComponent LoadCompleteSync", e);
            }
        }

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsManager.js ItHelpCenter.TForumsManager.ForumsManager.prototype.LoadComponent", e);

    }
}
ItHelpCenter.TForumsManager.ForumsManager.prototype.BtnUpdate_OnClick = function (_this, sender) {
    var result = null;
    try {
        var objForumsSearch = new Persistence.Forums.Properties.TFORUMSSEARCH();
        objForumsSearch.IDFORUMSSEARCH = _this.TxtId.Text;
        objForumsSearch.IDFORUMS = _this.cbCombobox.Value;
        objForumsSearch.NAMEFORUMSSEARCH = _this.TxtValue.Text;
        objForumsSearch.IMAGEFORUMSSEARCH = _this.TxtDescription.Text;
        objForumsSearch.QUERYFORUMSSEARCH = _this.Memo1.Text;
        result = _this.UpdFORUMSSEARCH(objForumsSearch);
        if (!SysCfg.Str.Methods.IsNullOrEmpity(result)) {
            if (result.Response) {
                _this.listView.RemoveAllElement();
                var listForumsSearch = new Array();
                Persistence.Forums.Methods.FORUMSSEARCH_Fill(listForumsSearch, "");
                listForumsSearch.forEach(function (element) {
                    if (element.IDFORUMS == _this.cbComboboxFrBd.Value) {
                        var data = new _this.listView.Member();
                        data.Id = element.IDFORUMSSEARCH;
                        data.Value = element.NAMEFORUMSSEARCH;
                        data.Description = element.QUERYFORUMSSEARCH;
                        data.Tag = element;
                        _this.listView.AddElement(data);
                    }
                });
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, result.Message));
            }
            else if (result.Validation) {
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, result.Message));
            }
            else {
                throw (UsrCfg.Traslate.GetLangText(_this.Mythis, result.Message));
            }
        }
        else {
            throw ("Erro in result");
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsManager.js ItHelpCenter.TForumsManager.ForumsManager.prototype.BtnOk_OnClick", e);
    }
}
ItHelpCenter.TForumsManager.ForumsManager.prototype.BtnOk_OnClick = function (_this, sender) {
    var result = null;
    try {
        var objForumsSearch = new Persistence.Forums.Properties.TFORUMSSEARCH();
        objForumsSearch.IDFORUMSSEARCH = 0;
        objForumsSearch.IDFORUMS = _this.cbCombobox.Value;
        objForumsSearch.NAMEFORUMSSEARCH = _this.TxtValue.Text;
        objForumsSearch.IMAGEFORUMSSEARCH = _this.TxtDescription.Text;
        objForumsSearch.QUERYFORUMSSEARCH = _this.Memo1.Text;
        result = _this.AddFORUMSSEARCH(objForumsSearch);
        if (!SysCfg.Str.Methods.IsNullOrEmpity(result)) {
            if (result.Response) {
                var data = new _this.listView.Member();
                data.Id = objForumsSearch.IDFORUMSSEARCH;
                data.Value = _this.TxtValue.Text;
                data.Description = _this.Memo1.Text;
                data.Tag = objForumsSearch;
                _this.listView.AddElement(data);
                _this.TxtValue.Text = "";
                _this.TxtDescription.Text = "";
                _this.Memo1.Text = "";
                _this.cbCombobox.Value = -1;

                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, result.Message));
            }
            else if (result.Validation) {
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, result.Message));
            }
            else {
                throw (UsrCfg.Traslate.GetLangText(_this.Mythis, result.Message));
            }
        }
        else {
            throw ("Erro in result");
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsManager.js ItHelpCenter.TForumsManager.ForumsManager.prototype.BtnOk_OnClick", e);
    }
}
ItHelpCenter.TForumsManager.ForumsManager.prototype.BtnDelete_OnClick = function (_this, sender) {
    var result = null;
    try {
        result = _this.DeleteFORUMSSEARCH(_this.TxtId.Text);
        if (!SysCfg.Str.Methods.IsNullOrEmpity(result)) {
            if (result.Response) {
                _this.listView.RemoveAllElement();
                var listForumsSearch = new Array();
                Persistence.Forums.Methods.FORUMSSEARCH_Fill(listForumsSearch, "");
                listForumsSearch.forEach(function (element) {
                    if (element.IDFORUMS == _this.cbComboboxFrBd.Value) {
                        var data = new _this.listView.Member();
                        data.Id = element.IDFORUMSSEARCH;
                        data.Value = element.NAMEFORUMSSEARCH;
                        data.Description = element.QUERYFORUMSSEARCH;
                        data.Tag = element;
                        _this.listView.AddElement(data);
                    }
                });
                _this.TxtId.Text = "0";
                _this.TxtValue.Text = "";
                _this.TxtDescription.Text = "";
                _this.Memo1.Text = "";
                _this.cbCombobox.Value = -1;
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, result.Message));
            }
            else if (result.Validation) {
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, result.Message));
            }
            else {
                throw (UsrCfg.Traslate.GetLangText(_this.Mythis, result.Message));
            }
        }
        else {
            throw ("Erro in result");
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsManager.js ItHelpCenter.TForumsManager.ForumsManager.prototype.BtnDelete_OnClick", e);
    }
}
ItHelpCenter.TForumsManager.ForumsManager.prototype.ForumBd_OnChange = function (_this, sender) {
    try {
        _this.listView.RemoveAllElement();
        var listForumsSearch = new Array();
        Persistence.Forums.Methods.FORUMSSEARCH_Fill(listForumsSearch, "");
        listForumsSearch.forEach(function (element) {
            if (element.IDFORUMS == _this.cbComboboxFrBd.Value) {
                var data = new _this.listView.Member();
                data.Id = element.IDFORUMSSEARCH;
                data.Value = element.NAMEFORUMSSEARCH;
                data.Description = element.QUERYFORUMSSEARCH;
                data.Tag = element;
                _this.listView.AddElement(data);
            }
        });
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsManager.js ItHelpCenter.TForumsManager.ForumsManager.prototype.ForumBd_OnChange", e);

    }
}
ItHelpCenter.TForumsManager.ForumsManager.prototype.ForumBd_OnDbClick = function (_this, sender) {
    try {
        _this.TxtId.Text = sender.Tag.IDFORUMSSEARCH;
        _this.TxtValue.Text = sender.Tag.NAMEFORUMSSEARCH;
        _this.TxtDescription.Text = sender.Tag.IMAGEFORUMSSEARCH;
        _this.Memo1.Text = sender.Tag.QUERYFORUMSSEARCH;
        _this.cbCombobox.Value = sender.Tag.IDFORUMS;
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsManager.js ItHelpCenter.TForumsManager.ForumsManager.prototype.ForumBd_OnDbClick", e);
    }
}
ItHelpCenter.TForumsManager.ForumsManager.prototype.AddFORUMSSEARCH = function (objForumsSearch) {
    var _this = this.TParent();
    var objResponse = { Response: false, Validation: null, Result: null, Message: null };
    try {
        if (SysCfg.Str.Methods.IsNullOrEmpity(objForumsSearch.IDFORUMS) || (!isNaN(objForumsSearch.IDFORUMS) && objForumsSearch.IDFORUMS <= 0))
            return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDFORUMS" });
        else if (SysCfg.Str.Methods.IsNullOrEmpity(objForumsSearch.NAMEFORUMSSEARCH))
            return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORFORUMSSEARCHNAME" });
        else if (SysCfg.Str.Methods.IsNullOrEmpity(objForumsSearch.IMAGEFORUMSSEARCH))
            return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORFORUMSSEARCHIMAGE" });
        else if (SysCfg.Str.Methods.IsNullOrEmpity(objForumsSearch.QUERYFORUMSSEARCH))
            return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORFORUMSSEARCHQUERY" });
        var success = Persistence.Forums.Methods.FORUMSSEARCH_ADD(objForumsSearch);
        if (success.NotError) {
            return (objResponse = { Response: true, Validation: false, Result: objForumsSearch, Message: "SUCCESSADD" });
        }
        else {
            return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORADD" });
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsManager.js ItHelpCenter.TForumsManager.ForumsManager.prototype.UpdFORUMSSEARCH this.AddFORUMSSEARCH", e);
        return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORADD" });
    }
}
ItHelpCenter.TForumsManager.ForumsManager.prototype.UpdFORUMSSEARCH = function (objForumsSearch) {
    var _this = this.TParent();
    var objResponse = { Response: false, Validation: null, Result: null, Message: null };
    try {
        if (SysCfg.Str.Methods.IsNullOrEmpity(objForumsSearch.IDFORUMSSEARCH) || (!isNaN(objForumsSearch.IDFORUMSSEARCH) && objForumsSearch.IDFORUMSSEARCH <= 0))
            return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDFORUMSSEARCH" });
        else if (SysCfg.Str.Methods.IsNullOrEmpity(objForumsSearch.IDFORUMS) || (!isNaN(objForumsSearch.IDFORUMS) && objForumsSearch.IDFORUMS <= 0))
            return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDFORUMS" });
        else if (SysCfg.Str.Methods.IsNullOrEmpity(objForumsSearch.NAMEFORUMSSEARCH))
            return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORFORUMSSEARCHNAME" });
        else if (SysCfg.Str.Methods.IsNullOrEmpity(objForumsSearch.IMAGEFORUMSSEARCH))
            return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORFORUMSSEARCHIMAGE" });
        else if (SysCfg.Str.Methods.IsNullOrEmpity(objForumsSearch.QUERYFORUMSSEARCH))
            return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORFORUMSSEARCHQUERY" });
        var success = Persistence.Forums.Methods.FORUMSSEARCH_UPD(objForumsSearch);
        if (success.NotError) {
            return (objResponse = { Response: true, Validation: false, Result: objForumsSearch, Message: "SUCCESSUPDATE" });
        }
        else {
            return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORUPDATE" });
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsManager.js ItHelpCenter.TForumsManager.ForumsManager.prototype.UpdFORUMSSEARCH this.UpdFORUMSSEARCH", e);
        return (objResponse = { Response: false, Validation: false, Result: null, Message: "ERRORUPDATE" });
    }
}
ItHelpCenter.TForumsManager.ForumsManager.prototype.DeleteFORUMSSEARCH = function (idForumsSearch) {
    var _this = this.TParent();
    var objResponse = { Response: false, Validation: null, Result: null, Message: null };
    try {
        if (SysCfg.Str.Methods.IsNullOrEmpity(idForumsSearch) || (!isNaN(idForumsSearch) && idForumsSearch <= 0))
            return (objResponse = { Response: false, Validation: true, Result: null, Message: "ERRORIDFORUMSSEARCH" });
        var success = Persistence.Forums.Methods.FORUMSSEARCH_DELIDFORUMSSEARCH(idForumsSearch);
        if (success.NotError) {
            return (objResponse = { Response: true, Validation: false, Result: null, Message: "ELIMINATEDELEMENTCORRECTLY" });
        }
        else {
            return (objResponse = { Response: false, Validation: false, Result: null, Message: "ELEMENTNOTELIMINATED" });
        }
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsManager.js ItHelpCenter.TForumsManager.ForumsManager.prototype.UpdFORUMSSEARCH this.UpdFORUMSSEARCH", e);
        return (objResponse = { Response: false, Validation: false, Result: null, Message: "ELEMENTNOTELIMINATED" });
    }
}
ItHelpCenter.TForumsManager.ForumsManager.prototype.importarScript = function (nombre, onSuccess, onError) {
    var s = document.createElement("script");
    s.onload = onSuccess;
    s.onerror = onError;
    s.src = nombre;
    document.querySelector("head").appendChild(s);
}
ItHelpCenter.TForumsManager.ForumsManager.prototype.importarStyle = function (nombre, onSuccess, onError) {
    var style = document.createElement("link");
    style.rel = "stylesheet";
    style.type = "text/css";
    style.href = nombre;
    var s = document.head.appendChild(style);
    s.onload = onSuccess;
    s.onerror = onError;
}