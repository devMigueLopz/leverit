ItHelpCenter.TForumsManager.ForumsBasic = function (inObject, id, nameId, isManager, inCallBack) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.IdTable = nameId + "_" + id + "_" + new Date().getTime().toString();
    this.ObjectHtml = inObject;
    this.NameId = nameId;
    this.Id = id;
    this.DataWork = new Object();
    this.InCallback = inCallBack;
    this.Elements = new Object();
    this.Elements.DynamicElements = new Array();
    this.Profiler = new Persistence.Forums.TForumsProfiler();
    this.Manager = isManager;
    this.EnumPage = {
        GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
        Index: { value: 0, name: "Forum" },
        ForumCategory: { value: 1, name: "Forum Category" },
        Topic: { value: 2, name: "Topic" },
        Response: { value: 3, name: "Response" }
    };
    this.EnumStatus = {
        GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
        Disabled: { value: 0, name: "Disabled" },
        Enabled: { value: 1, name: "Enabled" }
    };
    this.EnumTypeComment = {
        GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
        Parent: { value: 0, name: "Parent" },
        Child: { value: 1, name: "Child" }
    };
    this.EnumEmoticons = {
        GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
        Like: { value: 0, name: "Like" },
        NotLike: { value: 1, name: "NotLike" },
        Abuse: { value: 2, name: "Abuse" },
        Comment: { value: 3, name: "Comment" },

    };
    this.SessionPage = {
        Index: { IdLoad: this.Id },
        ForumCategory: { IdLoad: 0 },
        Topic: { IdLoad: 0 },
        Response: { IdLoad: 0 },
    }
    this.CurrentPage = this.EnumPage.Index;
    this.Translate();
    this.Load();
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.Redirect = function (sender, element) {
    var _this = this.TParent();
    try {
        _this.Load();

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsBasic.js ItHelpCenter.TForumsManager.ForumsBasic.Redirect", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.Load = function () {
    var _this = this.TParent();
    try {
        switch (_this.CurrentPage) {
            case _this.EnumPage.Index:
                _this.ConfigurateIndex();
                break;
            case _this.EnumPage.ForumCategory:
                _this.ConfigurateForumCategory();
                break;
            case _this.EnumPage.Topic:
                _this.ConfigurateTopic();
                break;
            case _this.EnumPage.Response:
                _this.ConfigurateResponse();
                break;
        }
    } catch (ex) {
        SysCfg.Log.Methods.WriteLog("ForumsBasic.js ItHelpCenter.TForumsManager.ForumsBasic.Load", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.ConfigurateIndex = function () {
    var _this = this.TParent();
    try {
        _this.Profiler.Fill();
        _this.DataFormatTableForum();
        _this.InitializeComponent();
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsBasic.js ItHelpCenter.TForumsManager.ForumsBasic.ConfigurateIndex", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.ConfigurateForumCategory = function () {
    var _this = this.TParent();
    try {
        _this.Profiler.Fill();
        _this.DataFormatTableForumCategory();
        _this.InitializeComponent();
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsBasic.js ItHelpCenter.TForumsManager.ForumsBasic.ConfigurateForumCategory", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.ConfigurateTopic = function () {
    var _this = this.TParent();
    try {
        _this.Profiler.Fill();
        _this.DataFormatTableTopic();
        _this.InitializeComponent();
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsBasic.js ItHelpCenter.TForumsManager.ForumsBasic.ConfigurateTopic", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.ConfigurateResponse = function () {
    var _this = this.TParent();
    try {
        _this.Profiler.Fill();
        _this.DataFormatTableTopicResponse();
        _this.InitializeComponent();
        _this.Profiler.UpdTOPICSVIEW(_this.SessionPage.Response.IdLoad);
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsBasic.js ItHelpCenter.TForumsManager.ForumsBasic.ConfigurateResponse", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.DataFormatTableForum = function () {
    var _this = this.TParent();
    try {
        var forum = Persistence.Forums.Methods.FORUMS_ListSetID(_this.Profiler.ForumsList, _this.SessionPage.Index.IdLoad);
        var columTable = { Column1: { Title: "", Description: "", RedirectPage: _this.EnumPage.ForumCategory, Image: "", Id: "" }, Column2: 0, Column3: 0, Column4: { Title: UsrCfg.Traslate.GetLangText(_this.Mythis, "NODATA"), Create: UsrCfg.Traslate.GetLangText(_this.Mythis, "NODATA"), CreateDate: UsrCfg.Traslate.GetLangText(_this.Mythis, "NODATA") }}
        var counTotalAllForums = { totalTopics: 0, totalResponse: 0, totalMembers: 0, totalView: 0 };
        if (!SysCfg.Str.Methods.IsNullOrEmpity(forum)) {
            columTable.Column1.Title = forum.NAMEFORUMS;
            columTable.Column1.Description = forum.DESCRIPTIONFORUMS;
            columTable.Column1.Image = 'image/48/connection.png';
            columTable.Column1.Id = forum.IDFORUMS;
            for (var i = 0; i < forum.FORUMSCATEGORYList.length; i++) {
                if (_this.EnumStatus.Enabled == _this.EnumStatus.GetEnum((forum.FORUMSCATEGORYList[i].STATUSFORUMSCATEGORY) ? 1 : 0)) {
                    counTotalAllForums.totalTopics += forum.FORUMSCATEGORYList[i].TOPICSFORUMSCATEGORY_COUNT;
                    counTotalAllForums.totalResponse += forum.FORUMSCATEGORYList[i].RESPONSEFORUMSCATEGORY_COUNT;
                    counTotalAllForums.totalMembers += forum.FORUMSCATEGORYList[i].MEMBERSFORUMSCATEGORY_COUNT;
                }
            }
            columTable.Column2 = counTotalAllForums.totalTopics;
            columTable.Column3 = counTotalAllForums.totalResponse;
            var lastPost = Persistence.Forums.Methods.TOPICSRESPONSE_GETLAST(_this.SessionPage.Index.IdLoad, _this.EnumPage.Index, _this.EnumPage);
            if (!SysCfg.Str.Methods.IsNullOrEmpity(lastPost)) {
            lastPost = Persistence.Forums.Methods.TOPICSRESPONSE_ListSetID(_this.Profiler.TopicsResponseList, lastPost.IDTOPICSRESPONSE)
                if (_this.EnumStatus.Enabled == _this.EnumStatus.GetEnum((lastPost.STATUSTOPICSRESPOSE) ? 1 : 0)) {
                    columTable.Column4.Title = lastPost.TITLETOPICSRESPONSE;
                    columTable.Column4.Create = lastPost.CMDBCI.CI_GENERICNAME;
                    columTable.Column4.CreateDate = SysCfg.DateTimeMethods.ExtensionDate.convertDateTime(lastPost.CREATEDDATETOPICSRESPONSE);
                }
            }
        }
        _this.DataWork.DataColumn = new Array();
        _this.DataWork.DataColumn.push(columTable);
        _this.DataWork.DataTotalAllForums = counTotalAllForums;
    } catch (e) {
        sysCfg.Log.Methods.WriteLog("ForumsBasic.js ItHelpCenter.TForumsManager.ForumsBasic.DataFormatTableForum", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.DataFormatTableForumCategory = function () {
    var _this = this.TParent();
    try {
        var forum = Persistence.Forums.Methods.FORUMS_ListSetID(_this.Profiler.ForumsList, _this.SessionPage.ForumCategory.IdLoad);
        var counTotalAllForums = { totalTopics: 0, totalResponse: 0, totalMembers: 0, totalView: 0 };
        _this.DataWork.DataColumn = new Array();
        if (!SysCfg.Str.Methods.IsNullOrEmpity(forum)) {
            for (var i = 0; i < forum.FORUMSCATEGORYList.length; i++) {
                if (_this.EnumStatus.Enabled == _this.EnumStatus.GetEnum((forum.FORUMSCATEGORYList[i].STATUSFORUMSCATEGORY) ? 1 : 0)) {
                    counTotalAllForums.totalTopics += forum.FORUMSCATEGORYList[i].TOPICSFORUMSCATEGORY_COUNT;
                    counTotalAllForums.totalResponse += forum.FORUMSCATEGORYList[i].RESPONSEFORUMSCATEGORY_COUNT;
                    counTotalAllForums.totalMembers += forum.FORUMSCATEGORYList[i].MEMBERSFORUMSCATEGORY_COUNT;
                    //
                    var columTable = { Column1: { Title: "", Description: "", RedirectPage: _this.EnumPage.Topic, Image: "", Id: "" }, Column2: 0, Column3: 0, Column4: { Title: UsrCfg.Traslate.GetLangText(_this.Mythis, "NODATA"), Create: UsrCfg.Traslate.GetLangText(_this.Mythis, "NODATA"), CreateDate: UsrCfg.Traslate.GetLangText(_this.Mythis, "NODATA") } }
                    columTable.Column1.Title = forum.FORUMSCATEGORYList[i].MDCATEGORYDETAIL.CATEGORYNAME;
                    columTable.Column1.Description = forum.FORUMSCATEGORYList[i].MDCATEGORYDETAIL.CATEGORY + ". " + forum.FORUMSCATEGORYList[i].MDCATEGORYDETAIL.CATEGORYDESCRIPTION;
                    columTable.Column1.Image = forum.FORUMSCATEGORYList[i].IMAGEFORUMSCATEGORY;
                    columTable.Column1.Id = forum.FORUMSCATEGORYList[i].IDFORUMSCATEGORY;
                    columTable.Column2 = forum.FORUMSCATEGORYList[i].TOPICSFORUMSCATEGORY_COUNT;
                    columTable.Column3 = forum.FORUMSCATEGORYList[i].RESPONSEFORUMSCATEGORY_COUNT;
                    var lastPost = Persistence.Forums.Methods.TOPICSRESPONSE_GETLAST(forum.FORUMSCATEGORYList[i].IDFORUMSCATEGORY, _this.EnumPage.ForumCategory, _this.EnumPage);
                    if (!SysCfg.Str.Methods.IsNullOrEmpity(lastPost)) {
                    lastPost = Persistence.Forums.Methods.TOPICSRESPONSE_ListSetID(_this.Profiler.TopicsResponseList, lastPost.IDTOPICSRESPONSE)
                        if (_this.EnumStatus.Enabled == _this.EnumStatus.GetEnum((lastPost.STATUSTOPICSRESPOSE) ? 1 : 0)) {
                            columTable.Column4.Title = lastPost.TITLETOPICSRESPONSE;
                            columTable.Column4.Create = lastPost.CMDBCI.CI_GENERICNAME;
                            columTable.Column4.CreateDate = SysCfg.DateTimeMethods.ExtensionDate.convertDateTime(lastPost.CREATEDDATETOPICSRESPONSE);
                        }
                    }
                    _this.DataWork.DataColumn.push(columTable);
                }

            }
        }
        _this.DataWork.DataTotalAllForums = counTotalAllForums;
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsBasic.js ItHelpCenter.TForumsManager.ForumsBasic.DataFormatTableForumCategory", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.DataFormatTableTopic = function () {
    var _this = this.TParent();
    try {
        var forumCategory = Persistence.Forums.Methods.FORUMSCATEGORY_ListSetID(_this.Profiler.ForumsCategoryList, _this.SessionPage.Topic.IdLoad);
        var counTotalAllForums = { totalTopics: 0, totalResponse: 0, totalMembers: 0, totalView: 0 };
        _this.DataWork.DataColumn = new Array();
        if (!SysCfg.Str.Methods.IsNullOrEmpity(forumCategory)) {
            for (var i = 0; i < forumCategory.TOPICSList.length; i++) {
                if (_this.EnumStatus.Enabled == _this.EnumStatus.GetEnum((forumCategory.TOPICSList[i].STATUSTOPICS) ? 1 : 0)) {
                    counTotalAllForums.totalTopics = forumCategory.TOPICSList[i].FORUMSCATEGORY.TOPICSFORUMSCATEGORY_COUNT;//FIX FOR CATEGORY COUNT EQUALS 
                    counTotalAllForums.totalView += forumCategory.TOPICSList[i].VIEWSTOPICS_COUNT;
                    counTotalAllForums.totalResponse = forumCategory.TOPICSList[i].FORUMSCATEGORY.RESPONSEFORUMSCATEGORY_COUNT;//FIX FOR CATEGORY COUNT EQUALS 
                    counTotalAllForums.totalMembers = forumCategory.TOPICSList[i].FORUMSCATEGORY.MEMBERSFORUMSCATEGORY_COUNT;//FIX FOR CATEGORY COUNT EQUALS 
                    //
                    var columTable = { Column1: { Title: "", Description: "", RedirectPage: _this.EnumPage.Response, Image: "", Id: "" }, Column2: 0, Column3: 0, Column4: { Title: UsrCfg.Traslate.GetLangText(_this.Mythis, "NODATA"), Create: UsrCfg.Traslate.GetLangText(_this.Mythis, "NODATA"), CreateDate: UsrCfg.Traslate.GetLangText(_this.Mythis, "NODATA") } }
                    columTable.Column1.Title = forumCategory.TOPICSList[i].NAMETOPICS;
                    columTable.Column1.Description = UsrCfg.Traslate.GetLangText(_this.Mythis, "BY") + " " + forumCategory.TOPICSList[i].CMDBCI.CI_GENERICNAME + " - " + SysCfg.DateTimeMethods.ExtensionDate.convertDateTime(forumCategory.TOPICSList[i].CREATEDDATETOPICS);
                    columTable.Column1.Image = forumCategory.TOPICSList[i].IMAGETOPICS;
                    columTable.Column1.Id = forumCategory.TOPICSList[i].IDTOPICS;
                    columTable.Column2 = forumCategory.TOPICSList[i].VIEWSTOPICS_COUNT;
                    columTable.Column3 = forumCategory.TOPICSList[i].RESPONSETOPICS_COUNT;
                    var lastPost = Persistence.Forums.Methods.TOPICSRESPONSE_GETLAST(forumCategory.TOPICSList[i].IDTOPICS, _this.EnumPage.Topic, _this.EnumPage);
                    if (!SysCfg.Str.Methods.IsNullOrEmpity(lastPost)) {
                    lastPost = Persistence.Forums.Methods.TOPICSRESPONSE_ListSetID(_this.Profiler.TopicsResponseList, lastPost.IDTOPICSRESPONSE)
                        if (_this.EnumStatus.Enabled == _this.EnumStatus.GetEnum((lastPost.STATUSTOPICSRESPOSE) ? 1 : 0)) {
                            columTable.Column4.Title = lastPost.TITLETOPICSRESPONSE;
                            columTable.Column4.Create = lastPost.CMDBCI.CI_GENERICNAME;
                            columTable.Column4.CreateDate = SysCfg.DateTimeMethods.ExtensionDate.convertDateTime(lastPost.CREATEDDATETOPICSRESPONSE);
                        }
                    }
                    _this.DataWork.DataColumn.push(columTable);
                }

            }
        }
        _this.DataWork.DataTotalAllForums = counTotalAllForums;
        _this.DataWork.ForumsSearch = forumCategory.FORUMS.FORUMSSEARCHList;
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsBasic.js ItHelpCenter.TForumsManager.ForumsBasic.DataFormatTableTopic", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.SaveTopic = function (sender, element) {
    var _this = this.TParent();
    try {
        var newPostTopic = new Persistence.Forums.Properties.TTOPICS();
        var result = null;
        var success = false;
        newPostTopic.IDFORUMSCATEGORY = Persistence.Forums.Methods.FORUMSCATEGORY_ListSetID(_this.Profiler.ForumsCategoryList, _this.SessionPage.Topic.IdLoad).IDFORUMSCATEGORY
        newPostTopic.IDCMDBCI = UsrCfg.Properties.UserATRole.User.IDCMDBCI;
        newPostTopic.NAMETOPICS = _this.Elements.textTitleModalContentAddTopicHeaderBody_ForumIndex.Text;
        newPostTopic.DESCRIPTIONTOPICS = _this.Elements.memoDescriptionModalContentAddTopicHeaderBody_ForumIndex.Text;
        newPostTopic.IMAGETOPICS = _this.Elements.textImageModalContentAddTopicHeaderBody_ForumIndex.Text;
        newPostTopic.STATUSTOPICS = true;
        newPostTopic.CREATEDDATETOPICS = new Date();
        newPostTopic.KEYWORDSTOPICS = _this.Elements.textKeyWordsModalContentAddTopicHeaderBody_ForumIndex.Text;
        result = _this.Profiler.AddTOPICS(newPostTopic);
        if (!SysCfg.Str.Methods.IsNullOrEmpity(result)) {
            if (result.Response && result.Validation === false) {
                if (_this.SaveTopicResponse(newPostTopic)) {
                    //$(_this.ObjectHtml).html("");
                    //_this.Elements.modalAddTopicHeaderBody_ForumIndex.CloseModal();
                    //$(_this.Elements.modalAddTopicHeaderBody_ForumIndex.This.This).remove();
                    //_this.Elements = new Object();
                    //_this.Elements.DynamicElements = new Array();
                    //_this.ConfigurateTopic();
                    //_this.Elements.modalAddTopicHeaderBody_ForumIndex.ShowModal();
                    alert(UsrCfg.Traslate.GetLangText(_this.Mythis, result.Message))
                    success = true;
                    //ADD FILL IN LOGIC
                }
                else {
                    _this.Profiler.TOPICS_DELTOPICS(newPostTopic);
                    throw "Error in Validation" //problem developer or internal db
                }
            }
            else if (result.Response === false && result.Validation)
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, result.Message))
            else
                throw "Error in Save DataBase" //problem developer or internal db
        }
        else
            throw "Error in Save DataBase"//problem developer or internal db
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsBasic.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.SaveTopic", e);
    }
    return (success);
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.SearchTopic = function (sender, element) {
    var _this = sender;
    try {
        var forumCategory = Persistence.Forums.Methods.FORUMSCATEGORY_ListSetID(_this.Profiler.ForumsCategoryList, _this.SessionPage.Topic.IdLoad);
        var listElementTopic = _this.SetTagSearch(forumCategory, element);
        var counTotalAllForums = { totalTopics: 0, totalResponse: 0, totalMembers: 0, totalView: 0 };
        _this.DataWork.DataColumn = new Array();
        if (!SysCfg.Str.Methods.IsNullOrEmpity(forumCategory)) {
            counTotalAllForums.totalTopics = forumCategory.TOPICSFORUMSCATEGORY_COUNT;//FIX FOR CATEGORY COUNT EQUALS 
            counTotalAllForums.totalResponse = forumCategory.RESPONSEFORUMSCATEGORY_COUNT;//FIX FOR CATEGORY COUNT EQUALS 
            counTotalAllForums.totalMembers = forumCategory.MEMBERSFORUMSCATEGORY_COUNT;//FIX FOR CATEGORY COUNT EQUALS
            for (var i = 0; i < listElementTopic.length; i++) {
                if (_this.EnumStatus.Enabled == _this.EnumStatus.GetEnum((listElementTopic[i].STATUSTOPICS) ? _this.EnumStatus.Enabled.value : _this.EnumStatus.Disabled.value)) {
                    counTotalAllForums.totalView += listElementTopic[i].VIEWSTOPICS_COUNT;
                    var columTable = { Column1: { Title: "", Description: "", RedirectPage: _this.EnumPage.Response, Image: "", Id: "" }, Column2: 0, Column3: 0, Column4: { Title: UsrCfg.Traslate.GetLangText(_this.Mythis, "NODATA"), Create: UsrCfg.Traslate.GetLangText(_this.Mythis, "NODATA"), CreateDate: UsrCfg.Traslate.GetLangText(_this.Mythis, "NODATA") } }
                    columTable.Column1.Title = listElementTopic[i].NAMETOPICS;
                    columTable.Column1.Description = UsrCfg.Traslate.GetLangText(_this.Mythis, "BY") + " " + _this.SearchUser(_this.Profiler.CmdbCiList,listElementTopic[i].IDCMDBCI).CI_GENERICNAME + " - " + SysCfg.DateTimeMethods.ExtensionDate.convertDateTime(listElementTopic[i].CREATEDDATETOPICS);
                    columTable.Column1.Image = listElementTopic[i].IMAGETOPICS;
                    columTable.Column1.Id = listElementTopic[i].IDTOPICS;
                    columTable.Column2 = listElementTopic[i].VIEWSTOPICS_COUNT;
                    columTable.Column3 = listElementTopic[i].RESPONSETOPICS_COUNT;
                    var lastPost = Persistence.Forums.Methods.TOPICSRESPONSE_GETLAST(listElementTopic[i].IDTOPICS, _this.EnumPage.Topic, _this.EnumPage);
                    if (!SysCfg.Str.Methods.IsNullOrEmpity(lastPost)) {
                    lastPost = Persistence.Forums.Methods.TOPICSRESPONSE_ListSetID(_this.Profiler.TopicsResponseList, lastPost.IDTOPICSRESPONSE)
                        if (_this.EnumStatus.Enabled == _this.EnumStatus.GetEnum((lastPost.STATUSTOPICSRESPOSE) ? _this.EnumStatus.Enabled.value : _this.EnumStatus.Disabled.value)) {
                            columTable.Column4.Title = lastPost.TITLETOPICSRESPONSE;
                            columTable.Column4.Create = lastPost.CMDBCI.CI_GENERICNAME;
                            columTable.Column4.CreateDate = SysCfg.DateTimeMethods.ExtensionDate.convertDateTime(lastPost.CREATEDDATETOPICSRESPONSE);
                        }
                    }
                    _this.DataWork.DataColumn.push(columTable);
                }
            }
        }
        _this.DataWork.DataTotalAllForums = counTotalAllForums;
        _this.DataWork.ForumsSearch = forumCategory.FORUMS.FORUMSSEARCHList;

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsBasic.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.SearchTopic", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.SetTagSearch = function (forumCategory, element) {
    var _this = this.TParent();
    var arrayResult = new Array();
    try {
        var query = element.Tag.QUERYFORUMSSEARCH.toLowerCase();
        if (query.indexOf("{idforumscategory}") > -1) {
            query = query.replace("{idforumscategory}", (forumCategory.IDFORUMSCATEGORY + ""));
        }
        Persistence.Forums.Methods.TQUERYS_EXECUTE(arrayResult, query);
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsBasic.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.SetTagSearch", e);
    }
    return (arrayResult)
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.SaveTopicResponse = function (objTopic) {
    var _this = this.TParent();
    try {
        var newPostResponseTopic = new Persistence.Forums.Properties.TTOPICSRESPONSE;
        var result = null;
        var success = false;
        newPostResponseTopic.IDTOPICSRESPONSE = 0;
        newPostResponseTopic.IDTOPICS = objTopic.IDTOPICS;
        newPostResponseTopic.IDCMDBCI = objTopic.IDCMDBCI;
        newPostResponseTopic.IDTOPICSRESPONSE_PARENT = 0;
        newPostResponseTopic.TITLETOPICSRESPONSE = objTopic.NAMETOPICS;
        newPostResponseTopic.SUBJECTTOPICSRESPONSE = objTopic.DESCRIPTIONTOPICS;
        newPostResponseTopic.STATUSTOPICSRESPOSE = true;
        newPostResponseTopic.CREATEDDATETOPICSRESPONSE = objTopic.CREATEDDATETOPICS;
        newPostResponseTopic.LIKETOPICSRESPONSE_COUNT = 0;
        newPostResponseTopic.NOTLIKETOPICSRESPONSE_COUNT = 0;
        newPostResponseTopic.ABUSETOPICSRESPONSE_COUNT = 0;
        result = _this.Profiler.AddTOPICSRESPONSE(newPostResponseTopic);
        if (!SysCfg.Str.Methods.IsNullOrEmpity(result)) {
            if (result.Response && result.Validation === false)
                success = true;
            //ADD FILL IN LOGIC
            else if (result.Response === false && result.Validation)
                throw "Error in Validation" //problem developer or internal db
            else
                throw "Error in Save DataBase" //problem developer or internal db
        }
        else
            throw "Error in Save DataBase"//problem developer or internal db
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsBasic.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.SaveTopic", e);
    }
    return (success);
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.DataFormatTableTopicResponse = function () {
    var _this = this.TParent();
    var user = UsrCfg.Properties.UserATRole.User;
    _this.DataWork = new Object();
    _this.DataWork.UserCurrentId = SysCfg.Str.Methods.IsNullOrEmpity(user.IDCMDBCI) ? 0 : user.IDCMDBCI;
    _this.DataWork.UserCurrentName = SysCfg.Str.Methods.IsNullOrEmpity(user.CI_GENERICNAME) ? "" : user.CI_GENERICNAME;
    _this.DataWork.UserCurrentImage = "image/forums/user.png";
    _this.UserCurrentEmoticons(_this.DataWork);
    _this.DataWork.Response = new Array();
    try {
        var topic = Persistence.Forums.Methods.TOPICS_ListSetID(_this.Profiler.TopicsList, _this.SessionPage.Response.IdLoad);
        _this.DataWork.KeyWords = topic.KEYWORDSTOPICS
        if (!SysCfg.Str.Methods.IsNullOrEmpity(topic)) {
            for (var i = 0; i < topic.TOPICSRESPONSEPARENTList.length; i++) {
                if (_this.EnumStatus.Enabled == _this.EnumStatus.GetEnum((topic.TOPICSRESPONSEPARENTList[i].STATUSTOPICSRESPOSE) ? 1 : 0)) {
                    _this.DataWork.Response.push(_this.DataFormatTopicResponse(topic.TOPICSRESPONSEPARENTList[i]));
                }
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsBasic.js ItHelpCenter.TForumsManager.ForumsBasic.DataFormatTableTopic", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.DataFormatTopicResponse = function (objResponse) {
    var _this = this.TParent();
    var response = new _this.ObjectTopicResponse();
    try {
        response.ResponseInitTopicResponseId = objResponse.IDTOPICSRESPONSE;
        response.ResponseInitTopicId = objResponse.IDTOPICS;
        response.ResponseInitParentId = objResponse.IDTOPICSRESPONSE_PARENT;
        response.ResponseInitName = objResponse.TITLETOPICSRESPONSE;
        response.ResponseInitDescription = objResponse.SUBJECTTOPICSRESPONSE;
        response.ResponseInitCreatedDate = SysCfg.DateTimeMethods.ExtensionDate.convertDateTime(objResponse.CREATEDDATETOPICSRESPONSE);
        response.ResponseInitCreatedUser = { Id: objResponse.IDCMDBCI, Name: objResponse.CMDBCI.CI_GENERICNAME, Image: "image/forums/admin.png" };
        response.ResponseDataGlobal.ResponseInitCountLike = objResponse.LIKETOPICSRESPONSE_COUNT;
        response.ResponseDataGlobal.ResponseInitCountNotLike = objResponse.NOTLIKETOPICSRESPONSE_COUNT;
        response.ResponseDataGlobal.ResponseInitCountAbuse = objResponse.ABUSETOPICSRESPONSE_COUNT;
        response.ResponseInitAbuseId = (objResponse.TOPICSRESPONSEABUSEList.length > 0) ? _this.SearchEmoticonAbuseId(objResponse) : 0;
        response.ResponseInitVotesId = (objResponse.TOPICSRESPONSEVOTESList.length > 0) ? _this.SearchEmoticonVotesId(objResponse) : 0;


        response.ResponseDataGlobal.ResponseInitCountComment = (objResponse.IDTOPICSRESPONSE_PARENT == 0) ? _this.CountMessages(objResponse.IDTOPICSRESPONSE) : objResponse.TOPICSRESPONSEChildList.length;
        _this.PaintIconsAndFormatSave(response);
        response.ResponseInitListChildComment = new Array();
        for (var i = 0; i < objResponse.TOPICSRESPONSEChildList.length; i++) {
            response.ResponseInitListChildComment.push(_this.DataFormatTopicResponse(objResponse.TOPICSRESPONSEChildList[i]));
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsBasic.js ItHelpCenter.TForumsManager.ForumsBasic.DataFormatTopicResponse", e);
    }
    return (response);
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.SearchEmoticonVotesId = function (objResponse) {
    var _this = this.TParent();
    var id = 0;
    try {
        for (var i = 0; i < objResponse.TOPICSRESPONSEVOTESList.length; i++) {
            if (objResponse.IDTOPICSRESPONSE == objResponse.TOPICSRESPONSEVOTESList[i].IDTOPICSRESPONSE && objResponse.TOPICSRESPONSEVOTESList[i].IDCMDBCI == UsrCfg.Properties.UserATRole.User.IDCMDBCI) {
                id = objResponse.TOPICSRESPONSEVOTESList[i].IDTOPICSRESPONSEVOTES;
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsBasic.js ItHelpCenter.TForumsManager.ForumsBasic.UserCurrentEmoticons", e);
    }
    return (id);
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.SearchEmoticonAbuseId = function (objResponse) {
    var _this = this.TParent();
    var id = 0;
    try {
        for (var i = 0; i < objResponse.TOPICSRESPONSEABUSEList.length; i++) {
            if (objResponse.IDTOPICSRESPONSE == objResponse.TOPICSRESPONSEABUSEList[i].IDTOPICSRESPONSE && objResponse.TOPICSRESPONSEABUSEList[i].IDCMDBCI == UsrCfg.Properties.UserATRole.User.IDCMDBCI) {
                id = objResponse.TOPICSRESPONSEABUSEList[i].IDTOPICSRESPONSEABUSE;
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsBasic.js ItHelpCenter.TForumsManager.ForumsBasic.SearchEmoticonAbuseId", e);
    }
    return (id);
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.UserCurrentEmoticons = function (dataWork) {
    var _this = this.TParent();
    var listLikeAndNotLike = Persistence.Forums.Methods.TOPICSRESPONSEVOTES_GETLIKEANDNOTLIKE(dataWork.UserCurrentId, _this.SessionPage.Response.IdLoad);
    var listAbuse = Persistence.Forums.Methods.TOPICSRESPONSEABUSE_GETABUSEANDNOTABUSE(dataWork.UserCurrentId, _this.SessionPage.Response.IdLoad);
    dataWork.ListUserCurrentLikeId = new Array();
    dataWork.ListUserCurrentNotLikeId = new Array();
    dataWork.ListUserCurrentAbusedId = new Array();
    try {
        for (var i = 0; i < listLikeAndNotLike.length; i++) {
            if (listLikeAndNotLike[i].LIKETOPICSRESPONSE) {
                dataWork.ListUserCurrentLikeId.push({ TopicResponse: listLikeAndNotLike[i].IDTOPICSRESPONSE, TopicResponseEmoticon: listLikeAndNotLike[i].IDTOPICSRESPONSEVOTES });
            }
            if (listLikeAndNotLike[i].NOTLIKETOPICSRESPONSE) {
                dataWork.ListUserCurrentNotLikeId.push({ TopicResponse: listLikeAndNotLike[i].IDTOPICSRESPONSE, TopicResponseEmoticon: listLikeAndNotLike[i].IDTOPICSRESPONSEVOTES });
            }
        }
        for (var i = 0; i < listAbuse.length; i++) {
            if (listAbuse[i].LIKETOPICSRESPONSEABUSE) {
                dataWork.ListUserCurrentAbusedId.push({ TopicResponse: listAbuse[i].IDTOPICSRESPONSE, TopicResponseEmoticon: listAbuse[i].IDTOPICSRESPONSEABUSE });
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsBasic.js ItHelpCenter.TForumsManager.ForumsBasic.UserCurrentEmoticons", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.CountMessages = function (topicResponseId) {
    var _this = this.TParent();
    try {
        var topic = Persistence.Forums.Methods.TOPICS_ListSetID(_this.Profiler.TopicsList, _this.SessionPage.Response.IdLoad);
        var countComment = 0;
        for (var i = 0; i < topic.TOPICSRESPONSEList.length; i++) {
            if (topic.TOPICSRESPONSEList[i].IDTOPICSRESPONSE_PARENT == topicResponseId) {
                countComment++;
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsBasic.js ItHelpCenter.TForumsManager.ForumsBasic.CountMessages", e);
    }
    return (countComment);
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.PaintIconsAndFormatSave = function (dataResponseTopic) {
    var _this = this.TParent();
    try {
        for (var i = 0; i < _this.DataWork.ListUserCurrentLikeId.length; i++) {
            if (_this.DataWork.ListUserCurrentLikeId[i].TopicResponse == dataResponseTopic.ResponseInitTopicResponseId) {
                dataResponseTopic.ResponseDataGlobal.ResponseInitPaintLike = true;
            }
        }
        for (var i = 0; i < _this.DataWork.ListUserCurrentNotLikeId.length; i++) {
            if (_this.DataWork.ListUserCurrentNotLikeId[i].TopicResponse == dataResponseTopic.ResponseInitTopicResponseId) {
                dataResponseTopic.ResponseDataGlobal.ResponseInitPaintNotLike = true;
            }
        }
        for (var i = 0; i < _this.DataWork.ListUserCurrentAbusedId.length; i++) {
            if (_this.DataWork.ListUserCurrentAbusedId[i].TopicResponse == dataResponseTopic.ResponseInitTopicResponseId) {
                dataResponseTopic.ResponseDataGlobal.ResponseInitPaintAbuse = true;
            }
        }
    } catch (ex) {
        SysCfg.Log.Methods.WriteLog("ForumsBasic.js ItHelpCenter.TForumsManager.ForumsBasic.PaintIconsAndFormatSave", e);
    }

}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.ObjectTopicResponse = function () {
    try {
        this.ResponseInitTopicResponseId = 0;
        this.ResponseInitTopicId = 0;
        this.ResponseInitParentId = 0;
        this.ResponseInitName = "";
        this.ResponseInitDescription = "";
        this.ResponseInitCreatedDate = "";
        this.ResponseInitCreatedUser = { Id: 0, Name: "", Image: "" };
        this.ResponseInitVotesId = 0;
        this.ResponseInitAbuseId = 0;
        this.ResponseInitListChildComment = new Array();
        this.ResponseDataGlobal = {
            ResponseInitCountLike: 0,
            ResponseInitCountNotLike: 0,
            ResponseInitCountAbuse: 0,
            ResponseInitCountComment: 0,
            ResponseInitPaintLike: false,
            ResponseInitPaintNotLike: false,
            ResponseInitPaintAbuse: false,
        };
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsBasic.js ItHelpCenter.TForumsManager.ForumsBasic.ObjectTopicResponse", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.ObjectCommentParent = function () {
    try {
        this.ElementPost = new Object();
        this.CurrentDataWork = new Object();
        this.TypeComment = new Object();
        this.Grandchild = false;
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsBasic.js ItHelpCenter.TForumsManager.ForumsBasic.ObjectCommentParent", e);
    }
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.SaveDataActionEmoticon = function (dataEmoticon) {
    var _this = this.TParent();
    var success = false;
    var result = null;
    var topicResponseVote = new Persistence.Forums.Properties.TTOPICSRESPONSEVOTES();
    topicResponseVote.IDTOPICSRESPONSEVOTES = dataEmoticon.EmoticonId;
    topicResponseVote.IDTOPICSRESPONSE = dataEmoticon.IdTopicResponse;
    topicResponseVote.IDCMDBCI = dataEmoticon.UserId;
    try {
        switch (dataEmoticon.EnumEmoticon) {
            case _this.EnumEmoticons.Like:
                topicResponseVote.LIKETOPICSRESPONSE = !dataEmoticon.Status
                result = _this.Profiler.UpdTopicsResponseLike(topicResponseVote);
                if (!SysCfg.Str.Methods.IsNullOrEmpity(result)) {
                    var topicResponse = Persistence.Forums.Methods.TOPICSRESPONSE_ListSetID(_this.Profiler.TopicsResponseList, topicResponseVote.IDTOPICSRESPONSE);
                    _this.UserCurrentEmoticons(_this.DataWork);
                    dataEmoticon.ObjectTopicResponse = _this.DataFormatTopicResponse(topicResponse);
                    dataEmoticon.Status = dataEmoticon.ObjectTopicResponse.ResponseDataGlobal.ResponseInitPaintLike;
                    dataEmoticon.EmoticonId = dataEmoticon.ObjectTopicResponse.ResponseInitVotesId;
                    dataEmoticon.Count = dataEmoticon.ObjectTopicResponse.ResponseDataGlobal.ResponseInitCountLike;
                    var success = true;
                }
                break
            //ADD FILL IN LOGIC
            case _this.EnumEmoticons.NotLike:
                topicResponseVote.NOTLIKETOPICSRESPONSE = !dataEmoticon.Status
                result = _this.Profiler.UpdTopicsResponseNotLike(topicResponseVote);
                if (!SysCfg.Str.Methods.IsNullOrEmpity(result)) {
                    var topicResponse = Persistence.Forums.Methods.TOPICSRESPONSE_ListSetID(_this.Profiler.TopicsResponseList, topicResponseVote.IDTOPICSRESPONSE);
                    _this.UserCurrentEmoticons(_this.DataWork);
                    dataEmoticon.ObjectTopicResponse = _this.DataFormatTopicResponse(topicResponse);
                    dataEmoticon.Status = dataEmoticon.ObjectTopicResponse.ResponseDataGlobal.ResponseInitPaintNotLike;
                    dataEmoticon.EmoticonId = dataEmoticon.ObjectTopicResponse.ResponseInitVotesId;
                    dataEmoticon.Count = dataEmoticon.ObjectTopicResponse.ResponseDataGlobal.ResponseInitCountNotLike;
                    var success = true;
                }
                break
            //ADD FILL IN LOGIC
            case _this.EnumEmoticons.Abuse:
                var topicResponseAbuse = new Persistence.Forums.Properties.TTOPICSRESPONSEABUSE();
                topicResponseAbuse.IDTOPICSRESPONSEABUSE = dataEmoticon.EmoticonId;
                topicResponseAbuse.IDTOPICSRESPONSE = dataEmoticon.IdTopicResponse;
                topicResponseAbuse.IDCMDBCI = dataEmoticon.UserId;
                topicResponseAbuse.LIKETOPICSRESPONSEABUSE = !dataEmoticon.Status
                result = _this.Profiler.UpdTopicsResponseAbuseLike(topicResponseAbuse);
                if (!SysCfg.Str.Methods.IsNullOrEmpity(result)) {
                    var topicResponse = Persistence.Forums.Methods.TOPICSRESPONSE_ListSetID(_this.Profiler.TopicsResponseList, topicResponseAbuse.IDTOPICSRESPONSE);
                    _this.UserCurrentEmoticons(_this.DataWork);
                    dataEmoticon.ObjectTopicResponse = _this.DataFormatTopicResponse(topicResponse);
                    dataEmoticon.Status = dataEmoticon.ObjectTopicResponse.ResponseDataGlobal.ResponseInitPaintAbuse;
                    dataEmoticon.EmoticonId = dataEmoticon.ObjectTopicResponse.ResponseInitAbuseId;
                    dataEmoticon.Count = dataEmoticon.ObjectTopicResponse.ResponseDataGlobal.ResponseInitCountAbuse;
                    var success = true;
                }
                break;
            //ADD FILL IN LOGIC
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsBasic.js ItHelpCenter.TForumsManager.ForumsBasic.SaveDataActionEmoticon", e);
    }
    return (success);
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.SaveNewComment = function (dataNewComment) {
    var _this = this.TParent();
    var success = null;
    var result = null;
    try {
        var objectTopicResponse = new Persistence.Forums.Properties.TTOPICSRESPONSE();
        objectTopicResponse.IDTOPICSRESPONSE = 0;
        objectTopicResponse.IDTOPICS = _this.SessionPage.Response.IdLoad;
        objectTopicResponse.IDCMDBCI = UsrCfg.Properties.UserATRole.User.IDCMDBCI;
        objectTopicResponse.IDTOPICSRESPONSE_PARENT = dataNewComment.CurrentDataWork.ResponseInitTopicResponseId;
        objectTopicResponse.TITLETOPICSRESPONSE = "Response - " + dataNewComment.CurrentDataWork.ResponseInitName;
        objectTopicResponse.SUBJECTTOPICSRESPONSE = dataNewComment.ElementPost.CommentUser.PostComentInput_ForumIndex.Text;
        objectTopicResponse.STATUSTOPICSRESPOSE = true;
        objectTopicResponse.CREATEDDATETOPICSRESPONSE = new Date();
        objectTopicResponse.LIKETOPICSRESPONSE_COUNT = 0;
        objectTopicResponse.NOTLIKETOPICSRESPONSE_COUNT = 0;
        objectTopicResponse.ABUSETOPICSRESPONSE_COUNT = 0;
        success = _this.Profiler.AddTOPICSRESPONSE(objectTopicResponse);
        if (!SysCfg.Str.Methods.IsNullOrEmpity(success.Result)) {
            _this.UserCurrentEmoticons(_this.DataWork);
            var topicResponse = Persistence.Forums.Methods.TOPICSRESPONSE_ListSetID(_this.Profiler.TopicsResponseList, dataNewComment.CurrentDataWork.ResponseInitTopicResponseId);
            dataNewComment.CurrentDataWork = _this.DataFormatTopicResponse(topicResponse);

            var newComment = Persistence.Forums.Methods.TOPICSRESPONSE_ListSetID(_this.Profiler.TopicsResponseList, objectTopicResponse.IDTOPICSRESPONSE);
            var newCommentData = _this.DataFormatTopicResponse(newComment);
            var result = newCommentData;
        }
        else {
            throw (success.Message)//problem developer or internal db
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsBasic.js ItHelpCenter.TForumsManager.ForumsBasic.SaveNewComment", e);
    }
    return (result);
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.SearchUser = function (CMDBCIList, IDCMDBCI) {
    for (i = 0; i < CMDBCIList.length; i++) {
        if (IDCMDBCI == CMDBCIList[i].IDCMDBCI)
            return (CMDBCIList[i]);
    }
    var UnCMDBCI = {
        CI_GENERICNAME: "",
        FIRSTNAME: "",
        IDCMDBCI: IDCMDBCI,
        IDCMDBCISTATE: 0,
        LASTNAME: "",
        MIDDLENAME: "",
    }
    return (UnCMDBCI);
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.TopicsCreateUserManager = function (isManager) {
    var _this = this.TParent();
    var forumCategory = Persistence.Forums.Methods.FORUMSCATEGORY_ListSetID(_this.Profiler.ForumsCategoryList, _this.SessionPage.Topic.IdLoad);
    var objDataModalTopic = {};
    objDataModalTopic.ListUser = _this.Profiler.CmdbCiList;
    objDataModalTopic.ListForumCategory = _this.Profiler.ForumsCategoryList;
    objDataModalTopic.ListTopic = new Array();
    try {
        if (!SysCfg.Str.Methods.IsNullOrEmpity(forumCategory)) {
            for (var i = 0; i < forumCategory.TOPICSList.length; i++) {
                if (isManager) {
                    objDataModalTopic.ListTopic.push(forumCategory.TOPICSList[i]);
                }
                else {
                    if (forumCategory.TOPICSList[i].CMDBCI.IDCMDBCI == UsrCfg.Properties.UserATRole.User.IDCMDBCI) {
                        objDataModalTopic.ListTopic.push(forumCategory.TOPICSList[i]);
                    }
                }
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsBasic.js ItHelpCenter.TForumsManager.ForumsBasic.TopicsCreateUserManager", e);
    }
    return (objDataModalTopic);
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.TopicsResponseCreateUserManager = function (isManager) {
    var _this = this.TParent();
    var objDataModalTopicResponse = {};
    var listTopic = Persistence.Forums.Methods.TOPICS_ListSetID(_this.Profiler.TopicsList, _this.SessionPage.Response.IdLoad);
    objDataModalTopicResponse.ListTopic = _this.Profiler.TopicsList;
    objDataModalTopicResponse.ListUser = _this.Profiler.CmdbCiList;
    objDataModalTopicResponse.ListTopicResponseParent = _this.Profiler.TopicsResponseList;
    objDataModalTopicResponse.ListTopicResponse = new Array();
    try {
        if (!SysCfg.Str.Methods.IsNullOrEmpity(listTopic)) {
            for (var i = 0; i < listTopic.TOPICSRESPONSEList.length; i++) {
                if (isManager) {
                    objDataModalTopicResponse.ListTopicResponse.push(listTopic.TOPICSRESPONSEList[i]);
                }
                else {
                    if (listTopic.TOPICSRESPONSEList[i].CMDBCI.IDCMDBCI == UsrCfg.Properties.UserATRole.User.IDCMDBCI) {
                        objDataModalTopicResponse.ListTopicResponse.push(listTopic.TOPICSRESPONSEList[i]);
                    }
                }
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsBasic.js ItHelpCenter.TForumsManager.ForumsBasic.TopicsResponseCreateUserManager", e);
    }
    return (objDataModalTopicResponse);
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.TopicsResponseVotesCreateUserManager = function (isManager) {
    var _this = this.TParent();
    var listTopicResponseVotesList = _this.Profiler.TopicsResponseVotesList;
    var objDataModalTopicResponseVotes = {};
    objDataModalTopicResponseVotes.ListTopicResponse = _this.Profiler.TopicsResponseList;
    objDataModalTopicResponseVotes.ListUser = _this.Profiler.CmdbCiList;
    objDataModalTopicResponseVotes.ListTopicResponseVotes = new Array();

    try {
        if (!SysCfg.Str.Methods.IsNullOrEmpity(listTopicResponseVotesList)) {
            for (var i = 0; i < listTopicResponseVotesList.length; i++) {
                if (listTopicResponseVotesList[i].TOPICSRESPONSE.IDTOPICS == _this.SessionPage.Response.IdLoad) {
                    if (isManager) {
                        objDataModalTopicResponseVotes.ListTopicResponseVotes.push(listTopicResponseVotesList[i]);
                    }
                    else {
                        if (listTopicResponseVotesList[i].CMDBCI.IDCMDBCI == UsrCfg.Properties.UserATRole.User.IDCMDBCI) {
                            objDataModalTopicResponseVotes.ListTopicResponseVotes.push(listTopicResponseVotesList[i]);
                        }
                    }
                }
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsBasic.js ItHelpCenter.TForumsManager.ForumsBasic.TopicsResponseVotesCreateUserManager", e);
    }
    return (objDataModalTopicResponseVotes);
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.TopicsResponseAbuseCreateUserManager = function (isManager) {
    var _this = this.TParent();
    var listTopicResponseAbuseList = _this.Profiler.TopicsResponseAbuseList;
    var objDataModalTopicResponseAbuse = {};
    objDataModalTopicResponseAbuse.ListTopicResponse = _this.Profiler.TopicsResponseList;
    objDataModalTopicResponseAbuse.ListUser = _this.Profiler.CmdbCiList;
    objDataModalTopicResponseAbuse.ListTopicResponseAbuse = new Array();

    try {
        if (!SysCfg.Str.Methods.IsNullOrEmpity(listTopicResponseAbuseList)) {
            for (var i = 0; i < listTopicResponseAbuseList.length; i++) {
                if (listTopicResponseAbuseList[i].TOPICSRESPONSE.IDTOPICS == _this.SessionPage.Response.IdLoad) {
                    if (isManager) {
                        objDataModalTopicResponseAbuse.ListTopicResponseAbuse.push(listTopicResponseAbuseList[i]);
                    }
                    else {
                        if (listTopicResponseAbuseList[i].CMDBCI.IDCMDBCI == UsrCfg.Properties.UserATRole.User.IDCMDBCI) {
                            objDataModalTopicResponseAbuse.ListTopicResponseAbuse.push(listTopicResponseAbuseList[i]);
                        }
                    }
                }
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsBasic.js ItHelpCenter.TForumsManager.ForumsBasic.TopicsResponseAbuseCreateUserManager", e);
    }
    return (objDataModalTopicResponseAbuse);
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.UpdateTopic = function () {
    var _this = this.TParent();
    var topic = new Persistence.Forums.Properties.TTOPICS();
    var result = null;
    var success = false;
    try {
        topic.IDTOPICS = _this.Elements.dropDownListEditTopicsModalContentAddTopicHeaderBody_ForumIndex.Value;
        topic.IDFORUMSCATEGORY = _this.Elements.dropDownListEditForumsCategoryModalContentAddTopicHeaderBody_ForumIndex.Value;
        topic.IDCMDBCI = _this.Elements.dropDownListEditUsersModalContentAddTopicHeaderBody_ForumIndex.Value;
        topic.NAMETOPICS = _this.Elements.textEditTitleModalContentAddTopicHeaderBody_ForumIndex.Text;
        topic.DESCRIPTIONTOPICS = _this.Elements.memoEditDescriptionModalContentAddTopicHeaderBody_ForumIndex.Text;
        topic.RESPONSETOPICS_COUNT = _this.Elements.textEditResponseCountModalContentAddTopicHeaderBody_ForumIndex.Text;
        topic.VIEWSTOPICS_COUNT = _this.Elements.textEditViewModalContentAddTopicHeaderBody_ForumIndex.Text;
        topic.IMAGETOPICS = _this.Elements.textEditImageModalContentAddTopicHeaderBody_ForumIndex.Text;
        topic.STATUSTOPICS = true;
        topic.CREATEDDATETOPICS = new Date(_this.Elements.textEditCreatedDateModalContentAddTopicHeaderBody_ForumIndex.Text);
        topic.KEYWORDSTOPICS = _this.Elements.textEditKeyWordsModalContentAddTopicHeaderBody_ForumIndex.Text;
        result = _this.Profiler.UpdTOPICS(topic);
        if (!SysCfg.Str.Methods.IsNullOrEmpity(result)) {
            if (result.Response && result.Validation === false) {
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, result.Message));
                success = true;
            }
            else if (result.Response === false && result.Validation)
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, result.Message))
            else
                throw ("Error in Save DataBase " + result.Message);
        }
        else
            throw ("Error in Save DataBase");
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsBasic.js ItHelpCenter.TForumsManager.ForumsBasic.UpdateTopic", e);
    }
    return (success);
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.DeleteTopic = function () {
    var _this = this.TParent();
    var topic = new Persistence.Forums.Properties.TTOPICS();
    var result = null;
    var success = false;
    try {
        topic.IDTOPICS = _this.Elements.dropDownListEditTopicsModalContentAddTopicHeaderBody_ForumIndex.Value;
        result = _this.Profiler.DeleteTOPICSALL(topic);
        if (!SysCfg.Str.Methods.IsNullOrEmpity(result)) {
            if (result.Response && result.Validation === false) {
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, result.Message))
                success = true;
            }
            else if (result.Response === false && result.Validation)
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, result.Message))
            else
                throw ("Error in Save DataBase " + result.Message);
        }
        else
            throw ("Error in Save DataBase");

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsBasic.js ItHelpCenter.TForumsManager.ForumsBasic.DeleteTopic", e);
    }
    return (success);
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.UpdateTopicResponse = function () {
    var _this = this.TParent();
    var topicResponse = new Persistence.Forums.Properties.TTOPICSRESPONSE();
    var result = null;
    var success = false;
    try {
        topicResponse.IDTOPICSRESPONSE=_this.Elements.dropDownListEditTopicsResponseModalContentAddTopicHeaderBody_ForumIndex.Value;
        topicResponse.IDTOPICS=_this.Elements.dropDownListEditTopicsModalContentAddTopicHeaderBody_ForumIndex.Value;
        topicResponse.IDCMDBCI=_this.Elements.dropDownListEditUsersModalContentAddTopicHeaderBody_ForumIndex.Value;
        topicResponse.IDTOPICSRESPONSE_PARENT = _this.Elements.dropDownListEditTopicsParentModalContentAddTopicHeaderBody_ForumIndex.Value;
        topicResponse.TITLETOPICSRESPONSE = _this.Elements.textEditTitleModalContentAddTopicHeaderBody_ForumIndex.Text;
        topicResponse.SUBJECTTOPICSRESPONSE = _this.Elements.memoEditSubjectModalContentAddTopicHeaderBody_ForumIndex.Text;
        topicResponse.STATUSTOPICSRESPOSE=true;
        topicResponse.CREATEDDATETOPICSRESPONSE=new Date(_this.Elements.textEditCreatedDateModalContentAddTopicHeaderBody_ForumIndex.Text);
        topicResponse.LIKETOPICSRESPONSE_COUNT=_this.Elements.textEditCountLikeModalContentAddTopicHeaderBody_ForumIndex.Text;
        topicResponse.NOTLIKETOPICSRESPONSE_COUNT=_this.Elements.textEditCountNotLikeModalContentAddTopicHeaderBody_ForumIndex.Text;
        topicResponse.ABUSETOPICSRESPONSE_COUNT=_this.Elements.textEditCountAbuseModalContentAddTopicHeaderBody_ForumIndex.Text;
        result = _this.Profiler.UpdTOPICSRESPONSE(topicResponse);
        if (!SysCfg.Str.Methods.IsNullOrEmpity(result)) {
            if (result.Response && result.Validation === false) {
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, result.Message))
                success = true;
            }
            else if (result.Response === false && result.Validation)
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, result.Message))
            else
                throw ("Error in Save DataBase " + result.Message);
        }
        else
            throw ("Error in Save DataBase");
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsBasic.js ItHelpCenter.TForumsManager.ForumsBasic.UpdateTopicResponse", e);
    }
    return (success)
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.DeleteTopicResponse = function () {
    var _this = this.TParent();
    var topicResponse = Persistence.Forums.Methods.TOPICSRESPONSE_ListSetID(_this.Profiler.TopicsResponseList, _this.Elements.dropDownListEditTopicsResponseModalContentAddTopicHeaderBody_ForumIndex.Value);
    var result = null;
    var success = false;
    var topicResponseAllId = new Array();
    var queryTopicResponse = "";
    try {
        topicResponseAllId.push(topicResponse.IDTOPICSRESPONSE);
        _this.TopicResponseAllDependency(topicResponse.TOPICSRESPONSEChildList, topicResponseAllId);
        result = _this.Profiler.DeleteTOPICSRESPONSEALL(_this.CreateQueryResponseDeleteAll(topicResponseAllId), topicResponse);
        if (!SysCfg.Str.Methods.IsNullOrEmpity(result)) {
            if (result.Response && result.Validation === false) {
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, result.Message))
                success = true;
            }
            else if (result.Response === false && result.Validation)
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, result.Message))
            else
                throw ("Error in Delete DataBase " + result.Message);
        }
        else
            throw ("Error in Delete DataBase");
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsBasic.js ItHelpCenter.TForumsManager.ForumsBasic.DeleteTopic", e);
    }
    return (success);
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.CreateQueryResponseDeleteAll = function (topicResponseAllId) {
    var _this = this.TParent();
    var resultQuery = "";
    try {
        if (topicResponseAllId.length>0) {
            var resultTopicResponseVotes = "DELETE FROM TOPICSRESPONSEVOTES WHERE IDTOPICSRESPONSE IN (";
            var resultTopicResponseAbuse = "DELETE FROM TOPICSRESPONSEABUSE WHERE IDTOPICSRESPONSE IN (";
            var resultTopicResponse = "DELETE FROM TOPICSRESPONSE WHERE IDTOPICSRESPONSE IN (";
            for (var i = 0; i < topicResponseAllId.length; i++) {
                if ((i + 1) == topicResponseAllId.length) {
                    resultTopicResponseVotes += topicResponseAllId[i] + ")";
                    resultTopicResponseAbuse += topicResponseAllId[i] + ")";
                    resultTopicResponse += topicResponseAllId[i] + ")";
                }
                else {
                    resultTopicResponseVotes += topicResponseAllId[i] + ",";
                    resultTopicResponseAbuse += topicResponseAllId[i] + ",";
                    resultTopicResponse += topicResponseAllId[i] + ",";
                }
            }
            resultQuery += resultTopicResponseVotes + " " + resultTopicResponseAbuse + " " + resultTopicResponse;
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsBasic.js ItHelpCenter.TForumsManager.ForumsBasic.CreateQueryResponseDeleteAll", e);
    }
    return (resultQuery);
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.TopicResponseAllDependency = function (topicResponseChild, topicResponseAllId) {
    var _this = this.TParent();
    try {
        for (var i = 0; i < topicResponseChild.length; i++) {
            topicResponseAllId.push(topicResponseChild[i].IDTOPICSRESPONSE);
            if (topicResponseChild[i].TOPICSRESPONSEChildList.length > 0) {
                _this.TopicResponseAllDependency(topicResponseChild[i].TOPICSRESPONSEChildList, topicResponseAllId)
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsBasic.js ItHelpCenter.TForumsManager.ForumsBasic.TopicResponseAllDependency", e);
    }
    return (true);
}

ItHelpCenter.TForumsManager.ForumsBasic.prototype.UpdateTopicResponseVotes = function () {
    var _this = this.TParent();
    var topicResponseVotes = new Persistence.Forums.Properties.TTOPICSRESPONSEVOTES();
    var result = null;
    var success = false;
    try {
        topicResponseVotes.IDTOPICSRESPONSEVOTES=_this.Elements.dropDownListEditTopicResponseVotesModalContentAddTopicHeaderBody_ForumIndex.Value;
        topicResponseVotes.IDTOPICSRESPONSE=_this.Elements.dropDownListEditTopicsResponseVotesModalContentAddTopicHeaderBody_ForumIndex.Value;
        topicResponseVotes.IDCMDBCI=_this.Elements.dropDownListEditUsersVotesModalContentAddTopicHeaderBody_ForumIndex.Value;
        topicResponseVotes.LIKETOPICSRESPONSE=_this.Elements.textEditLikeModalContentAddTopicHeaderBody_ForumIndex.Checked;
        topicResponseVotes.NOTLIKETOPICSRESPONSE=_this.Elements.textEditNotLikeModalContentAddTopicHeaderBody_ForumIndex.Checked;
        result = _this.Profiler.UpdTOPICSRESPONSEVOTES(topicResponseVotes);
        if (!SysCfg.Str.Methods.IsNullOrEmpity(result)) {
            if (result.Response && result.Validation === false) {
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, result.Message))
                success = true;
            }
            else if (result.Response === false && result.Validation)
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, result.Message))
            else
                throw ("Error in Save DataBase " + result.Message);
        }
        else
            throw ("Error in Save DataBase");
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsBasic.js ItHelpCenter.TForumsManager.ForumsBasic.UpdateTopicResponseVotes", e);
    }
    return (success)
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.DeleteTopicResponseVotes = function () {
    var _this = this.TParent();
    var topicResponseVotes = new Persistence.Forums.Properties.TTOPICSRESPONSEVOTES();
    var result = null;
    var success = false;
    try {
        topicResponseVotes.IDTOPICSRESPONSEVOTES = _this.Elements.dropDownListEditTopicResponseVotesModalContentAddTopicHeaderBody_ForumIndex.Value;
        result = _this.Profiler.DeleteTOPICSRESPONSEVOTESALL(topicResponseVotes);
        if (!SysCfg.Str.Methods.IsNullOrEmpity(result)) {
            if (result.Response && result.Validation === false) {
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, result.Message))
                success = true;
            }
            else if (result.Response === false && result.Validation)
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, result.Message))
            else
                throw ("Error in Save DataBase " + result.Message);
        }
        else
            throw ("Error in Save DataBase");

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsBasic.js ItHelpCenter.TForumsManager.ForumsBasic.DeleteTopicResponseVotes", e);
    }
    return (success);
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.UpdateTopicResponseAbuse = function () {
    var _this = this.TParent();
    var topicResponseAbuse = new Persistence.Forums.Properties.TTOPICSRESPONSEABUSE();
    var result = null;
    var success = false;
    try {
        topicResponseAbuse.IDTOPICSRESPONSEABUSE = _this.Elements.dropDownListEditTopicResponseAbuseModalContentAddTopicHeaderBody_ForumIndex.Value;
        topicResponseAbuse.IDTOPICSRESPONSE = _this.Elements.dropDownListEditTopicsResponseAbuseModalContentAddTopicHeaderBody_ForumIndex.Value;
        topicResponseAbuse.IDCMDBCI = _this.Elements.dropDownListEditUsersAbuseModalContentAddTopicHeaderBody_ForumIndex.Value;
        topicResponseAbuse.LIKETOPICSRESPONSEABUSE = _this.Elements.textEditCheckAbuseModalContentAddTopicHeaderBody_ForumIndex.Checked;
        topicResponseAbuse.NOTLIKETOPICSRESPONSEABUSE = _this.Elements.textEditUncheckAbuseModalContentAddTopicHeaderBody_ForumIndex.Checked;
        result = _this.Profiler.UpdTOPICSRESPONSEABUSE(topicResponseAbuse);
        if (!SysCfg.Str.Methods.IsNullOrEmpity(result)) {
            if (result.Response && result.Validation === false) {
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, result.Message))
                success = true;
            }
            else if (result.Response === false && result.Validation)
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, result.Message))
            else
                throw ("Error in Save DataBase " + result.Message);
        }
        else
            throw ("Error in Save DataBase");
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsBasic.js ItHelpCenter.TForumsManager.ForumsBasic.UpdateTopicResponseAbuse", e);
    }
    return (success)
}
ItHelpCenter.TForumsManager.ForumsBasic.prototype.DeleteTopicResponseAbuse = function () {
    var _this = this.TParent();
    var topicResponseAbuse = new Persistence.Forums.Properties.TTOPICSRESPONSEABUSE();
    var result = null;
    var success = false;
    try {
        topicResponseAbuse.IDTOPICSRESPONSEABUSE = _this.Elements.dropDownListEditTopicResponseAbuseModalContentAddTopicHeaderBody_ForumIndex.Value;
        result = _this.Profiler.DeleteTOPICSRESPONSEABUSEALL(topicResponseAbuse);
        if (!SysCfg.Str.Methods.IsNullOrEmpity(result)) {
            if (result.Response && result.Validation === false) {
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, result.Message))
                success = true;
            }
            else if (result.Response === false && result.Validation)
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, result.Message))
            else
                throw ("Error in Save DataBase " + result.Message);
        }
        else
            throw ("Error in Save DataBase");

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsBasic.js ItHelpCenter.TForumsManager.ForumsBasic.DeleteTopicResponseAbuse", e);
    }
    return (success);
}