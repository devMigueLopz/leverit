ItHelpCenter.TForumsManager.ForumsManager.prototype.InitializeComponent = function () {
    var _this = this.TParent();
    try {
        _this.spPrincipal = new TVclStackPanel(_this.ObjectContent, "2", 2, [[9, 3], [9, 3], [9, 3], [9, 3], [9, 3]]);
        _this.DivBotonera = _this.MenuObject.GetDivData(_this.spPrincipal.Column[1], _this.spPrincipal.Column[0]);
        _this.stackPanelBotonera = new TVclStackPanel(_this.DivBotonera, "1", 1, [[12], [12], [12], [12], [12]]);


        if (Source.Menu.IsMobil) { _this.stackPanelBotonera.Row.This.classList.add("menuContenedorMobil"); }
        else {
            _this.stackPanelBotonera.Row.This.style.margin = "0px 0px";
            _this.stackPanelBotonera.Column[0].This.style.paddingLeft = "10px ";
            _this.stackPanelBotonera.Column[0].This.style.paddingTop = "10px ";
            _this.stackPanelBotonera.Column[0].This.style.paddingBottom = "10px";
            _this.stackPanelBotonera.Column[0].This.style.border = "2px solid rgba(0, 0, 0, 0.08)";
            _this.stackPanelBotonera.Column[0].This.style.boxShadow = " 0 0px 8px 0 rgba(0, 0, 0, 0.06), 0 1px 0px 0 rgba(0, 0, 0, 0.02);";
            _this.stackPanelBotonera.Column[0].This.style.marginTop = "15px";
            _this.stackPanelBotonera.Column[0].This.style.backgroundColor = "#FAFAFA";
        }
        _this.spBotonera = new TVclStackPanel(_this.stackPanelBotonera.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
        _this.spBotonera.Row.This.style.margin = "4px";

        _this.spBoton3 = new TVclStackPanel(_this.spBotonera.Column[0].This, "1", 2, [[8, 4], [8, 4], [8, 4], [8, 4], [8, 4]]);
        _this.spBoton3.Row.This.style.marginTop = "10px";

        _this.Label3 = new TVcllabel(_this.spBoton3.Column[0].This, "LabelAdd", TlabelType.H0);
        _this.Label3.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ADD");

        _this.BtnOk = new TVclButton(_this.spBoton3.Column[1].This, "");
        _this.BtnOk.VCLType = TVCLType.BS;
        _this.BtnOk.Src = "image/24/success.png";
        _this.BtnOk.Cursor = "pointer";
        _this.BtnOk.This.style.minWidth = "100%";
        _this.BtnOk.This.classList.add("btn-xs");
        _this.BtnOk.onClick = function () {
            try {
                _this.BtnOk_OnClick(_this, _this.BtnOk);
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("ForumsManagerDesigner.js ItHelpCenter.TForumsManager.ForumsManager.prototype.InitializeComponent _this.BtnOk.onClick", e);
            }
        }
        _this.spBoton1 = new TVclStackPanel(_this.spBotonera.Column[0].This, "1", 2, [[8, 4], [8, 4], [8, 4], [8, 4], [8, 4]]);
        _this.spBoton1.Row.This.style.marginTop = "10px";

        _this.Label1 = new TVcllabel(_this.spBoton1.Column[0].This, "LabelUpdate", TlabelType.H0);
        _this.Label1.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "UPDATE");

        _this.btnButton1 = new TVclButton(_this.spBoton1.Column[1].This, "btnButton1");
        _this.btnButton1.Cursor = "pointer";
        _this.btnButton1.Src = "image/24/Refresh.png";
        _this.btnButton1.VCLType = TVCLType.BS;
        _this.btnButton1.This.style.minWidth = "100%";
        _this.btnButton1.This.classList.add("btn-xs");
        _this.btnButton1.onClick = function () {
            try {
                _this.BtnUpdate_OnClick(_this, _this.btnButton1);
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("ForumsManagerDesigner.js ItHelpCenter.TForumsManager.ForumsManager.prototype.InitializeComponent _this.btnButton1.onClick", e);
            }
        }

        _this.spBoton2 = new TVclStackPanel(_this.spBotonera.Column[0].This, "1", 2, [[8, 4], [8, 4], [8, 4], [8, 4], [8, 4]]);
        _this.spBoton2.Row.This.style.marginTop = "10px";

        _this.Label2 = new TVcllabel(_this.spBoton2.Column[0].This, "LabelDelete", TlabelType.H0);
        _this.Label2.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "DELETE");

        _this.BtnCerrar = new TVclButton(_this.spBoton2.Column[1].This, "");
        _this.BtnCerrar.VCLType = TVCLType.BS;
        _this.BtnCerrar.Src = "image/24/delete.png";
        _this.BtnCerrar.Cursor = "pointer";
        _this.BtnCerrar.This.style.minWidth = "100%";
        _this.BtnCerrar.This.classList.add("btn-xs");
        _this.BtnCerrar.onClick = function () {
            try {
                _this.BtnDelete_OnClick(_this, _this.BtnCerrar);
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("ForumsManagerDesigner.js ItHelpCenter.TForumsManager.ForumsManager.prototype.InitializeComponent _this.BtnCerrar.onClick", e);
            }
        }



        _this.spContenido = new TVclStackPanel(_this.spPrincipal.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);

        _this.ContTabControl = new TVclStackPanel(_this.spContenido.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
        _this.ContTabControl.Row.This.style.marginTop = "20px";

        _this.TabControl1 = new TTabControl(_this.ContTabControl.Column[0].This, "", ""); //CREA UN TAB CONTROL 
        _this.TabPag1 = new TTabPage(); //CREA UN TAB PAGE PARA AGREGARSE AL TABCONTROL 
        _this.TabPag1.Name = "TabPag1"; //ASIGNA O EXTRAE EL NAME DEL TAB PAGE
        _this.TabPag1.Caption = UsrCfg.Traslate.GetLangText(_this.Mythis, "MANAGERSEARCH"); //ASIGNA O EXTRAE EL TEXTO QUE VA A MOSTRAR EN EL TAB
        _this.TabPag1.Active = true; //ASIGNA O EXTRAE SI EL TABPAGE ES EL ACTIVO (SE ESTA MOSTRANDO)
        _this.TabControl1.AddTabPages(_this.TabPag1); //ADICIONA UN TABPAGE AL TABCONTROL


        //var lblTabPage1 = new TVcllabel(TabPag1.Page, "", TlabelType.H0); //AGREGA CONTENIDO AL TAB PAGE 
        //lblTabPage1.Text = "Contenido Page 1";
        //lblTabPage1.VCLType = TVCLType.BS;


        _this.ConTextControl0 = new TVclStackPanel(_this.TabPag1.Page, "boxId", 2, [[12, 12], [12, 12], [2, 10], [2, 10], [2, 10]]);
        _this.ConTextControl0.Row.This.style.marginTop = "20px";

        _this.lblLabel0 = new TVcllabel(_this.ConTextControl0.Column[0].This, "", TlabelType.H0);
        _this.lblLabel0.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "ID");
        _this.lblLabel0.VCLType = TVCLType.BS;

        _this.TxtId = new TVclTextBox(_this.ConTextControl0.Column[1].This, "txtId");
        _this.TxtId.Text = "0"
        _this.TxtId.This.type = "number"
        _this.TxtId.This.setAttribute("min", "1");
        _this.TxtId.VCLType = TVCLType.BS;

        //************ INICIO DE TEXTBOX ***********************************************
        _this.ConTextControl1 = new TVclStackPanel(_this.TabPag1.Page, "boxName", 2, [[12, 12], [12, 12], [2, 10], [2, 10], [2, 10]]);
        _this.ConTextControl1.Row.This.style.marginTop = "20px";

        _this.lblLabel1 = new TVcllabel(_this.ConTextControl1.Column[0].This, "", TlabelType.H0);
        _this.lblLabel1.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "NAME");
        _this.lblLabel1.VCLType = TVCLType.BS;

        _this.TxtValue = new TVclTextBox(_this.ConTextControl1.Column[1].This, "txtName");
        _this.TxtValue.This.placeholder = UsrCfg.Traslate.GetLangText(_this.Mythis, "TEXTNAME");
        _this.TxtValue.VCLType = TVCLType.BS;
        //************ FIN DE TEXTBOX ***********************************************
        //************ INICIO DE TEXTBOX ***********************************************
        _this.ConTextControl2 = new TVclStackPanel(_this.TabPag1.Page, "boxPathImage", 2, [[12, 12], [12, 12], [2, 10], [2, 10], [2, 10]]);
        _this.ConTextControl2.Row.This.style.marginTop = "20px";

        _this.lblLabel2 = new TVcllabel(_this.ConTextControl2.Column[0].This, "", TlabelType.H0);
        _this.lblLabel2.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "IMAGE");
        _this.lblLabel2.VCLType = TVCLType.BS;

        _this.TxtDescription = new TVclTextBox(_this.ConTextControl2.Column[1].This, "txtPathImage");
        _this.TxtDescription.This.placeholder = UsrCfg.Traslate.GetLangText(_this.Mythis, "TEXTIMAGE");
        _this.TxtDescription.VCLType = TVCLType.BS;
        //************ FIN DE TEXTBOX ***********************************************
        //************ INICIO DE TEXTBOX ***********************************************
        _this.ConMemoControl = new TVclStackPanel(_this.TabPag1.Page, "boxQuery", 2, [[12, 12], [12, 12], [2, 10], [2, 10], [2, 10]]);
        _this.ConMemoControl.Row.This.style.marginTop = "20px";

        _this.lblMemo = new TVcllabel(_this.ConMemoControl.Column[0].This, "", TlabelType.H0);
        _this.lblMemo.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "QUERY");
        _this.lblMemo.VCLType = TVCLType.BS;

        _this.Memo1 = new TVclMemo(_this.ConMemoControl.Column[1].This, ""); //CREA UN MEMO CONTROL 
        _this.Memo1.VCLType = TVCLType.BS;    // PROPIEDAD PARA DARLE EL ESTILO DE LA LIBRERIA UTILIZADA (BOOTSTRAP, ETC) 
        _this.Memo1.Rows = 5;                 // PROPIEDAD PARA DARLE EL ALTO AL CONTROL 
        _this.Memo1.Cols = 50;                // PROPIEDAD PARA DARLE EL ANCHO AL CONTROL
        _this.Memo1.This.placeholder = UsrCfg.Traslate.GetLangText(_this.Mythis, "TEXTQUERY");

        //************ FIN DE TEXTBOX ***********************************************
        _this.ConComboBoxControl = new TVclStackPanel(_this.TabPag1.Page, "boxForums", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
        _this.ConComboBoxControl.Row.This.style.marginTop = "20px";

        _this.lblComboBox = new TVcllabel(_this.ConComboBoxControl.Column[0].This, "", TlabelType.H0);
        _this.lblComboBox.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "FORUM");
        _this.lblComboBox.VCLType = TVCLType.BS;

        _this.cbCombobox = new TVclComboBox2(_this.ConComboBoxControl.Column[1].This, ""); //CREA UN COMBOBOX CONTROL 

        _this.VclComboBoxItem1 = new TVclComboBoxItem(); // CREA UN COMBOBOX ITEM PARA SER AGREGADO AL COMBO 
        _this.VclComboBoxItem1.Value = -1; //ASIGNA O EXTRAE EL VALUE AL ITEM
        _this.VclComboBoxItem1.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "SELECTELEMENT");
        $(_this.cbCombobox.This).addClass("form-control");
        _this.cbCombobox.AddItem(_this.VclComboBoxItem1); // AGREGA EL ITEM AL COMBOBOX

        _this.cbCombobox.Value = -1;
        ////////////////
        _this.importarStyle(SysCfg.App.Properties.xRaiz + "Componet/ListView/Plugin/skins/dhtmlxlist_dhx_web.css", function () {
            _this.importarScript(SysCfg.App.Properties.xRaiz + "Componet/ListView/Plugin/dhtmlxlist.js", function () {
                _this.importarScript(SysCfg.App.Properties.xRaiz + "Componet/ListView/Class/ListView.js", function () {
                    if (Source.Menu.IsMobil) {
                        _this.ContainerListViewForumsManager = new TVclStackPanel(_this.TabPag1.Page, "ContainerListViewForumsManager" + "_" + _this.id, 1, [[12], [12], [12], [12], [12]]);
                        _this.ContainerChildUhr = new Uhr(_this.ContainerListViewForumsManager.Column[0].This, "ContainerChildUhr" + "_" + _this.id);
                        _this.ConComboBoxControlFrBd = new TVclStackPanel(_this.ContainerListViewForumsManager.Column[0].This, "boxForums", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
                        _this.ConComboBoxControlFrBd.Row.This.style.marginTop = "20px";
                        _this.lblComboBoxFrBd = new TVcllabel(_this.ConComboBoxControlFrBd.Column[0].This, "", TlabelType.H0);
                        _this.lblComboBoxFrBd.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "LISTFORUMSBD");
                        _this.lblComboBoxFrBd.VCLType = TVCLType.BS;
                        _this.cbComboboxFrBd = new TVclComboBox2(_this.ConComboBoxControlFrBd.Column[1].This, "cbComboboxFrBd"); //CREA UN COMBOBOX CONTROL 
                        $(_this.cbComboboxFrBd.This).addClass("form-control");
                        _this.listView = new Componet.ListView.TListView(_this.ContainerListViewForumsManager.Column[0].This, ("ListView1" + "_" + new Date().getTime().toString()), null);
                    }
                    else {
                        _this.ContainerListViewForumsManager = new TVclStackPanel(_this.TabPag1.Page, "ContainerListViewForumsManager" + "_" + _this.id, 1, [[12], [10], [10], [10], [10]]);
                        _this.ContainerChildUhr = new Uhr(_this.ContainerListViewForumsManager.Column[0].This, "ContainerChildUhr" + "_" + _this.id);
                        $(_this.ContainerListViewForumsManager.Column[0].This)[0].className += " center-block";
                        _this.ContainerListViewForumsManager.Column[0].This.style.float = "none";
                        _this.ConComboBoxControlFrBd = new TVclStackPanel(_this.ContainerListViewForumsManager.Column[0].This, "boxForums", 2, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
                        _this.ConComboBoxControlFrBd.Row.This.style.marginTop = "20px";
                        _this.lblComboBoxFrBd = new TVcllabel(_this.ConComboBoxControlFrBd.Column[0].This, "", TlabelType.H0);
                        _this.lblComboBoxFrBd.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "LISTFORUMSBD");
                        _this.lblComboBoxFrBd.VCLType = TVCLType.BS;
                        _this.cbComboboxFrBd = new TVclComboBox2(_this.ConComboBoxControlFrBd.Column[1].This, "cbComboboxFrBd"); //CREA UN COMBOBOX CONTROL 
                        $(_this.cbComboboxFrBd.This).addClass("form-control");
                        _this.listView = new Componet.ListView.TListView(_this.ContainerListViewForumsManager.Column[0].This, ("ListView1" + "_" + new Date().getTime().toString()), null);
                    }
                    _this.ContainerChildUhr.style.borderTop = "1px solid #ddd";
                    var listData = new Array();
                    _this.listView.Template = _this.listView._Template.Template1;
                    _this.listView.Load(listData);

                    _this.cbComboboxFrBd.onChange = function (e) { // ASIGNA EL EVENTO CUANDO SE SELECCIONE UN ITEM
                        try {
                            _this.ForumBd_OnChange(_this, _this.cbComboboxFrBd);
                        } catch (e) {
                            SysCfg.Log.Methods.WriteLog("ForumsManagerDesigner.js ItHelpCenter.TForumsManager.ForumsManager.prototype.InitializeComponent _this.cbComboboxFrBd.onChange", e);
                        }
                    }

                    _this.LoadCompleteSync();



                    _this.listView.OnDbClick = function (objItem) {
                        _this.ForumBd_OnDbClick(_this,objItem);
                    }
                    //listView.OnSelectChange = function (objItem) {
                    //    _this.ChangeElement(objItem);
                    //}
                }, function (e) {
                    SysCfg.Log.Methods.WriteLog("ForumsManagerDesigner.js _this.importarScript(" + SysCfg.App.Properties.xRaiz + "Componet/ListView/Class/ListView.js)", e);
                });
            }, function (e) {
                SysCfg.Log.Methods.WriteLog("ForumsManagerDesigner.js _this.importarScript(" + SysCfg.App.Properties.xRaiz + "Componet/ListView/Plugin/dhtmlxlist.js)", e);
            });
        }, function (e) {
            SysCfg.Log.Methods.WriteLog("ForumsManagerDesigner.js _this.importarStyle(" + SysCfg.App.Properties.xRaiz + "Componet/ListView/Plugin/skins/dhtmlxlist_dhx_web.css)", e);
        });
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsManagerDesigner - InitializeComponent", e);
    }
}