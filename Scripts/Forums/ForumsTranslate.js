ItHelpCenter.TForumsManager.ForumsBasic.prototype.Translate = function () {
    var _this = this.TParent();
    try {
        _this.Mythis = "ForumsBasic";
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORIDFORUMSSEARCH", "Failed to enter the Forum Search ID. Enter a correct data.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORFORUMSSEARCHNAME", "Failed to enter the Forum Search Name. Enter a correct data.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORFORUMSSEARCHIMAGE", "Failed to enter the Forum Search Image. Enter a correct data.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORFORUMSSEARCHQUERY", "Failed to enter the Forum Search Query. Enter a correct data.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "NODATA", "No Data.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORIDFORUMS", "Failed to enter the Forum ID. Enter a correct data.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORIDCMDBCI", "Failed to enter the User ID. Enter a correct data.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORNAMEFORUMS", "Failed to enter the Forum Name. Enter a correct data.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORDESCRIPTIONFORUMS", "Failed to enter the Forum Description. Enter a correct data.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORIDFORUMSCATEGORY", "Failed to enter the Forum Category ID. Enter a correct data.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORIDMDCATEGORYDETAIL", "Failed to enter the Category Detail ID. Enter a correct data.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORDESCRIPTIONFORUMSCATEGORY", "Failed to enter the Forum Category Description. Enter a correct data.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORTOPICSFORUMSCATEGORYCOUNT", "Failed to enter the Forum Category Topics Count. Enter a correct data.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORRESPONSEFORUMSCATEGORYCOUNT", "Failed to enter the Forum Category Response Count. Enter a correct data.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORMEMBERSFORUMSCATEGORYCOUNT", "Failed to enter the Forum Category Members Count. Enter a correct data.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORIMAGEFORUMSCATEGORY", "Failed to enter the Forum Category Image. Enter a correct data.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORSTATUSFORUMSCATEGORY", "Failed to enter the Forum Category Status. Enter a correct data.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORIDTOPICS", "Failed to enter the Topics ID. Enter a correct data.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORNAMETOPICS", "Failed to enter the Topics Name. Enter a correct data.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORDESCRIPTIONTOPICS", "Failed to enter the Topics Description. Enter a correct data.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORRESPONSETOPICSCOUNT", "Failed to enter the Topics Response Count. Enter a correct data.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORVIEWSTOPICSCOUNT", "Failed to enter the Topics Views Count. Enter a correct data.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORIMAGETOPICS", "Failed to enter the Topics Image. Enter a correct data.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORSTATUSTOPICS", "Failed to enter the Topics Status. Enter a correct data.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORCREATEDDATETOPICS", "Failed to enter the Topics Created Date. Enter a correct data.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORIDTOPICSRESPONSE", "Failed to enter the Topics Response ID. Enter a correct data.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORIDTOPICSRESPONSEPARENT", "Failed to enter the Topics Response Parent ID. Enter a correct data.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORTITLETOPICSRESPONSE", "Failed to enter the Topics Response Title. Enter a correct data.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORSUBJECTTOPICSRESPONSE", "Failed to enter the Topics Response Subject. Enter a correct data.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORSTATUSTOPICSRESPOSE", "Failed to enter the Topics Response Status. Enter a correct data.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORCREATEDDATETOPICSRESPONSE", "Failed to enter the Topics Response Created Date. Enter a correct data.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORLIKETOPICSRESPONSECOUNT", "Failed to enter the Topics Response Like Count. Enter a correct data.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORNOTLIKETOPICSRESPONSECOUNT", "Failed to enter the Topics Response Not Like Count. Enter a correct data.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORABUSETOPICSRESPONSECOUNT", "Failed to enter the Topics Response Abuse Count. Enter a correct data.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORIDTOPICSRESPONSEVOTES", "Failed to enter the Topics Response Votes ID. Enter a correct data.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "LIKETOPICSRESPONSE", "Failed to enter the Topics Response Like Vote. Enter a correct data.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORNOTLIKETOPICSRESPONSE", "Failed to enter the Topics Response Not Like Vote. Enter a correct data.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORIDTOPICSRESPONSEABUSE", "Failed to enter the Topics Response Abuse ID. Enter a correct data.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORLIKETOPICSRESPONSEABUSE", "Failed to enter the Topics Response Like Abuse. Enter a correct data.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORNOTLIKETOPICSRESPONSEABUSE", "Failed to enter the Topics Response Not Like Abuse. Enter a correct data.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "SUCCESSADD", "The data was saved correctly.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "SUCCESSDELETE", "The data was deleted correctly.");

        
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORADD", "An error occurred while saving the data");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "SUCCESSUPDATE", "The data was updated correctly.");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORUPDATE ", "An error occurred when updating the data");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ERRORDELETE ", "An error occurred when deleting the data");

        UsrCfg.Traslate.GetLangText(_this.Mythis, "FORUM", "Forum");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "FORUMCATEGORY", "Forum Category");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "TOPIC", "Topic");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "TOPICRESPONSE", "Response");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "WHOISONLINE", "Who is Online");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "BIRTHDAYS", "Birthdays");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "STATISTICS", "Statistics");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "LASTPOST", "Last Post");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "TITLE", "Title");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "USERONLINE", "The total number of user online");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "USERREGISTERED", "The total number of user registered");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "USERHIDDEN", "The total number of user hidden");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "USERGUEST", "The total number of user guest");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "NOBIRTHDAYSTODAY", "No birthdays today");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "TOTALTOPICS", "The total number of topics");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "TOTALRESPONSE", "The total number of response");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "TOTALMEMBERS", "The total number of members");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "TOTALVIEWS", "The total number of views");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "DATE", "Date");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "BY", "By");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "RE", "Re");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "VIEWS", "Views");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "NEWTOPIC", "POST A NEW TOPIC");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "TOPICS", "Topics");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "TOPICSRESPONSE", "Topics Response");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "TOPICSRESPONSEVOTES", "Topics Response Votes");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "TOPICSRESPONSEABUSE", "Topics Response Abuse");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "TOPICSRESPONSEPARENT", "Topics Response Parent");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "TITLETOPICSRESPONSE", "Title Topic Response");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "SUBJECTTOPICSRESPONSE", "Subject Response Topic Response");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "CREATEDDATETOPICSRESPONSE", "Created Date Topic Response");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "TOTALTOPICSRESPONSECOUNT", "Like Count Topic Response");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "TOTALLIKETOPICSRESPONSECOUNT", "Not Like Count Topic Response");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ABUSETOPICSRESPONSECOUNT", "Abuse Count Topic Response");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "LIKETOPICSRESPONSEVOTES", "Like Topic Response Votes");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "NOTLIKETOPICSRESPONSEVOTES", "Not Like Topic Response Votes");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "LIKETOPICSRESPONSEABUSE", "Like Topic Response Abuse");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "NOTLIKETOPICSRESPONSEABUSE", "Not Like Topic Response Abuse");

        UsrCfg.Traslate.GetLangText(_this.Mythis, "TITLETOPIC", "Title topic");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "DESCRIPTIONTOPIC", "Description Topic");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "TITLEIMAGE", "Image topic");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "TITLEKEYWORDS", "Keywords Topic");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "SAVETOPIC", "Save Topic");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "ADDTOPIC", "Add Topic");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "MODIFYUPDATETOPIC", "Modify Topic");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "WRITECOMMENT", "Write your comment...");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "DELETE", "Delete");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "UPDATE", "Update");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "FORUMSCATEGORY", "Forums Category");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "USERS", "Users");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "RESPONSECOUNT", "Response Count");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "CREATEDDATE", "Created Date");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "MODIFYTOPIC", "MODIFY TOPIC");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "MODIFYTOPICRESPONSE", "MODIFY TOPIC RESPONSE");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "MODIFYTOPICRESPONSEVOTES", "MODIFY TOPIC VOTES");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "MODIFYTOPICRESPONSEABUSE", "MODIFY TOPIC ABUSE");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "NODATATOPIC", "There is no data for this Topic");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "NODATAUSERTOPIC", "The user does not have any Topic Created");

        UsrCfg.Traslate.GetLangText(_this.Mythis, "NODATATOPICRESPONSE", "There is no data for this Topic Response");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "NODATAUSERTOPICRESPONSE", "The user does not have any Topic Response Created");

        UsrCfg.Traslate.GetLangText(_this.Mythis, "NODATATOPICRESPONSEVOTES", "There is no data for this Topic Response Votes");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "NODATAUSERTOPICRESPONSEVOTES", "The user does not have any Topic Response Votes Created");

        UsrCfg.Traslate.GetLangText(_this.Mythis, "NODATATOPICRESPONSEABUSE", "There is no data for this Topic Response Abuse");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "NODATAUSERTOPICRESPONSEABUSE", "The user does not have any Topic Response Abuse Created");


        UsrCfg.Traslate.GetLangText(_this.Mythis, "SELECTTOPIC", "Select Topic");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "NODATA", "No Data");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "MODIFYUPDATETOPICRESPONSE", "Modify Topic Response");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "MODIFYUPDATETOPICSRESPONSEVOTES", "Modify Topic Votes");
        UsrCfg.Traslate.GetLangText(_this.Mythis, "MODIFYUPDATETOPICSRESPONSEABUSE", "Modify Topic Abuse");

        UsrCfg.Traslate.GetLangText(_this.Mythis, "PARENT", "Parent");

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ForumsTranslate.js ItHelpCenter.TForumsManager.ForumsBasic.prototype.Translate", e);
    }
}