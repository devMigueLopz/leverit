﻿ItHelpCenter.TChangePsw = function (inObject, incallback, inVersion) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.ObjectHtml = inObject;
    this.Callback = incallback;
    this.version = inVersion;
    if (SysCfg.App.Properties.Config_Language.LanguageITF.value == 0) {
        this.idLenguaje = 2;
    }
    else {//if (SysCfg.App.Properties.Config_Language.LanguageITF.value == 1)
        this.idLenguaje = 1;
    }

    var _this = this.TParent();
    this.Mythis = "ChangePassword";
    //*******************************************************
    arrayleng = [["Info1", "Your password has expired.", "Su contraseña a caducado."],
                ["Info2", "Please enter new password.", "Por favor ingrese una nueva contraseña."],
                ["Subtitle", "User Settings", "Ajustes del usuario"],
                ["btnAccept", "Save", "Guardar"],
                ["lbPsw1", "New Password:", "Nueva Contraseña:"],
                ["lbPsw2", "Confirm password:", "Confirmar Contraseña:"],
                ["msgAcept1", "New password is required.", "El campo Nueva Contraseña es requerido."],
                ["msgAcept2", "Password does not match the confirm password.", "La contraseña no coincide con la contraseña de confirmación."]];

    //var _DBLocalLeng = new SysCfg.ini.DBLocalLeng('LocalLeng', 1);
    //_DBLocalLeng.Open(function (e) {
    //    _DBLocalLeng.Read(_this.idLenguaje, function (e) {
    //        _this.idLenguaje = e.target.result.IDleng;
    //        _this.Load();
    //    }, function (e) {
    //        alert(" Can´t Read LocalIndexDB. "+ _this.Mythis);
    //    });
    //}, function (e) {
    //    alert(" Can´t Load LocalIndexDB. " + _this.Mythis);
    //});

    var _DBLocalLeng = new SysCfg.ini.LSLocalLeng();

    _this.idLenguaje = _DBLocalLeng.Read(_this.idLenguaje);
    _this.Load();
    UsrCfg.WaitMe.CloseWaitme();
}

ItHelpCenter.TChangePsw.prototype.Load = function () {
    var _this = this.TParent();
    var ObjHtml = _this.ObjectHtml;
    ObjHtml.innerHTML = "";

    //ESTRUCTURA
    var Container = new TVclStackPanel(ObjHtml, "ContainerChange", 1, [[12], [12], [12], [12], [12]]);
    var ContainerCont = Container.Column[0].This;

    //div1 BANDERA
    var div1 = new TVclStackPanel(ContainerCont, "Change_div1", 1, [[12], [12], [12], [12], [12]]);
    var div1_Cont = div1.Column[0].This;

    //div2 LOGO / TITLE
    var div2 = new TVclStackPanel(ContainerCont, "Change_div2", 2, [[12, 12], [3, 9], [3, 9], [3, 9], [3, 9]]);
    var div2_1_Cont = div2.Column[0].This;
    var div2_2_Cont = div2.Column[1].This;

    //div3 Subtitle
    var div3 = new TVclStackPanel(ContainerCont, "Change_div3", 1, [[12], [12], [12], [12], [12]]);
    var div3_Cont = div3.Column[0].This;
  
    //div4 Label Info
    var div4 = new TVclStackPanel(ContainerCont, "Change_div4", 1, [[12], [12], [12], [12], [12]]);
    var div4_Cont = div4.Column[0].This;

    var div4_1 = new TVclStackPanel(div4_Cont, "Change_div4_1", 1, [[12], [12], [12], [12], [12]]);
    var div4_1_Cont = div4_1.Column[0].This; //INFO1

    var div4_2 = new TVclStackPanel(div4_Cont, "Change_div4_2", 1, [[12], [12], [12], [12], [12]]);
    var div4_2_Cont = div4_2.Column[0].This; //INFO2

    //div5 Password
    var div5 = new TVclStackPanel(ContainerCont, "Change_div5", 2, [[12, 12], [6, 6], [6, 6], [6, 6], [6, 6]]);
    var div5_1Cont = div5.Column[0].This; //Label
    var div5_2Cont = div5.Column[1].This; //Text

    //div6 Confirm Password
    var div6 = new TVclStackPanel(ContainerCont, "Change_div6", 2, [[12, 12], [6, 6], [6, 6], [6, 6], [6, 6]]);
    var div6_1Cont = div6.Column[0].This; //Label
    var div6_2Cont = div6.Column[1].This; //Text

    //div7 Btn Acept
    var div7 = new TVclStackPanel(ContainerCont, "Change_div7", 1, [[12], [12], [12], [12], [12]]);
    var div7_Cont = div7.Column[0].This;


    //ELEMENTOS
    var bandera = new TVclImagen(div1_Cont, "ChangeFlag");
    if (_this.idLenguaje == 1) {
        bandera.Src = "image/24/Spain-flag.png"
        
    } else {
        bandera.Src = "image/24/United-states-flag.png";
    }
    bandera.Cursor = "Pointer";
    bandera.onClick = function Change_Lenguage() {
        if (_this.idLenguaje == 1) {
            _this.idLenguaje = 2;
            bandera.Src = "image/24/United-states-flag.png"
            
        } else {
            _this.idLenguaje = 1;
            bandera.Src = "image/24/Spain-flag.png"
        }
        _this.SaveLocalLanguage();
      
        Subtitle.innerText = arrayleng[2][_this.idLenguaje];
        Info1.innerText = arrayleng[0][_this.idLenguaje];
        Info2.innerText = arrayleng[1][_this.idLenguaje];
        btnAccept.Text = arrayleng[3][_this.idLenguaje];
        btnAccept.Src = "image/16/Logout.png";
        LabPsw1.innerText = arrayleng[4][_this.idLenguaje];
        LabPsw2.innerText = arrayleng[5][_this.idLenguaje];
    }

    var Logo = new TVclImagen(div2_1_Cont, "ChangeLogo");
    Logo.Src = "image/High/IT-Help-Center-A.png";

    var Title = new TVclImagen(div2_2_Cont, "ChangeTitle");
    Title.Src = "image/High/IT-Help-Center-B.png";

    var Subtitle = new Vcllabel(div3_Cont, "ChangeSubtitle", TVCLType.BS, TlabelType.H0, arrayleng[2][_this.idLenguaje]);

    var Info1 = new Vcllabel(div4_1_Cont, "ChangeInfo1", TVCLType.BS, TlabelType.H0, arrayleng[0][_this.idLenguaje]);
    var Info2 = new Vcllabel(div4_2_Cont, "ChangeInfo2", TVCLType.BS, TlabelType.H0, arrayleng[1][_this.idLenguaje]);

    var LabPsw1 = new Vcllabel(div5_1Cont, "ChangelbPsw1", TVCLType.BS, TlabelType.H0, arrayleng[4][_this.idLenguaje]);
    var TextPsw1 = new TVclTextBox(div5_2Cont, "ChangeTextPsw1");
    TextPsw1.This.type = "password";
    $(TextPsw1.This).css("width", "100%");
    $(TextPsw1.This).css('border', "1px solid #000000");
    TextPsw1.This.onkeypress = function validar(e) {
        tecla = (document.all) ? e.keyCode : e.which;
        if (tecla == 13) TextPsw2.This.focus();
    }

    var LabPsw2 = new Vcllabel(div6_1Cont, "ChangelbPsw2", TVCLType.BS, TlabelType.H0, arrayleng[5][_this.idLenguaje]);
    var TextPsw2 = new TVclTextBox(div6_2Cont, "ChangeTextPsw2");
    TextPsw2.This.type = "password";
    $(TextPsw2.This).css("width", "100%");
    $(TextPsw2.This).css('border', "1px solid #000000");
    TextPsw2.This.onkeypress = function validar(e) {
        tecla = (document.all) ? e.keyCode : e.which;
        if (tecla == 13) btnAccept.onClick();
    }

    var btnAccept = new TVclButton(div7_Cont, "ChangebtnAccept");
    btnAccept.Cursor = "pointer";
    btnAccept.Text = arrayleng[3][_this.idLenguaje];
    btnAccept.Src = "image/16/Logout.png";
    btnAccept.VCLType = TVCLType.BS;
    btnAccept.onClick = function myfunction()
    {
        if (TextPsw1.Text == "") {
            alert(arrayleng[6][_this.idLenguaje]);
            return;
        }
        if (TextPsw1.Text != TextPsw2.Text) {
            alert(arrayleng[7][_this.idLenguaje]);
            try {
                TextPsw2.focus();
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("ChangePassword.js ItHelpCenter.TChangePsw.prototype.Load", e);
            }
            
            return;
        }
        
        if (UsrCfg.Version.IsProduction)
        {
            if (!_this.isValidPW(TextPsw1.Text))
                return;
        }

        var newPassword = TextPsw1.Text;
        var confirmPassword = TextPsw2.Text;
        var datalogin = {
            'newPassword': newPassword,
            'confirmPassword': confirmPassword
        };
        $.ajax({
            type: "POST",
            url: 'Service/wcfLoguinChangePassword.svc/ChangePassword',
            data: JSON.stringify(datalogin),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.Change) {
                    UsrCfg.WaitMe.ShowWaitme();
                    _this.Callback(ItHelpCenter.PageManager.TGoToPageType.BeforeMain);
                }
                else {
                    alert(response.Mensaje);
                }
            },
            error: function (response) {
                SysCfg.Log.Methods.WriteLog("ChangePassword.js ItHelpCenter.TChangePsw.prototype.Load Service/wcfLoguinChangePassword.svc/ChangePassword Login ");
            }
        });
    }
    try {
        TextPsw1.This.focus();
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ChangePassword.js ItHelpCenter.TChangePsw.prototype.Load", e);
    }
    
}

ItHelpCenter.TChangePsw.prototype.SaveLocalLanguage = function () {
    var _this = this.TParent();
    //var _DBLocalLeng = new SysCfg.ini.DBLocalLeng('LocalLeng', 1);
    //_DBLocalLeng.Open(function (e) {
    //    _DBLocalLeng.Write(_this.idLenguaje, function (e) {
    //    }, function (e) {
    //        alert("Can´t write LocalIndexDB. " + _this.Mythis);
    //    });
    //}, function (e) {
    //    alert("Can´t load LocalIndexDB. " + _this.Mythis);
    //});

    var _DBLocalLeng = new SysCfg.ini.LSLocalLeng();
    _this.idLenguaje = _DBLocalLeng.Write(_this.idLenguaje);
}

ItHelpCenter.TChangePsw.prototype.isValidPW = function(myInput)
{
    if (myInput.length <= 7)
    {
        alert("Invalid password. Must have a minimum of 8 characters");
        return false;
    }

    var TEsp = 0;
    var TNum = 0;
    var TUpp = 0;
    var TLwr = 0;
    var StrNoVal = "";

    var specialChars = "\|!#$%&/()=?»«@£§€{}.-;<>_,";
    for (i = 0; i < specialChars.length; i++)
    {
        if (myInput.indexOf(specialChars[i]) > -1)
        {
            TEsp = 1;
            break;
        }
    }

    var lowerCaseLetters = /[a-z]/g;
    if (myInput.match(lowerCaseLetters))
        TLwr = 1;

    var upperCaseLetters = /[A-Z]/g;
    if (myInput.match(upperCaseLetters))
        TUpp = 1;

    var numbers = /[0-9]/g;
    if (myInput.match(numbers))
        TNum = 1;

    if (TEsp == 0) StrNoVal += "Must contain special characters. ";
    if (TNum == 0) StrNoVal += "Must contain numbers. ";
    if (TUpp == 0) StrNoVal += "Must contain a uppercase letter. ";
    if (TLwr == 0) StrNoVal += "Must contain a lowercase letter. ";

    if (StrNoVal != "")
    {
        alert("Invalid password. " + StrNoVal);
        return false;
    }
    else
        return true;
}