﻿ItHelpCenter.TLogin = function (inObject, incallback, inVersion) {

    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.ObjectHtml = inObject;
    this.Callback = incallback;
    
    
    if (SysCfg.App.Properties.Config_Language.LanguageITF.value == 0) {
        this.idLenguaje = 2;
    }
    else {//if (SysCfg.App.Properties.Config_Language.LanguageITF.value == 1)
        this.idLenguaje = 1;
    }

    this.TextUser = null;
    this.TextPassw = null;
    this.version = inVersion;
    this.Mythis = "Login";
    this.User = null;


    //*******************************************************
    this.arrayleng = [
                 ["LogingLabUser", "User name:", "Usuario:"],
                 ["LogingLabPassw", "Password:", "Contrase\361a:"],
                 ["LogingbtnAccept", "Access", "Ingresar"],
                 ["LoginMsgDenied", "User name or password invalid", "Nombre de usuario o Contrase\361a invalidos"],
                 ["LabelVersion", "Version ", "Versión "]
    ];
    //var _DBLocalLeng = new SysCfg.ini.DBLocalLeng('LocalLeng', 1);
    //_DBLocalLeng.Open(function (e) {
    //    _DBLocalLeng.Read(_this.idLenguaje, function (e) {
    //        _this.idLenguaje = e.target.result.IDleng;           
    //        _this.Load();
    //    }, function (e) {
    //        alert(" Can´t Read LocalIndexDB. " + _this.Mythis);
    //    });
    //}, function (e) {
    //    alert(" Can´t Load LocalIndexDB. " + _this.Mythis);
    //});

    var _DBLocalLeng = new SysCfg.ini.LSLocalLeng();
    _this.idLenguaje = _DBLocalLeng.Read(_this.idLenguaje);
    _this.Load();

}

ItHelpCenter.TLogin.prototype.Load = function () {
    var _this = this.TParent();
    var ObjHtml = _this.ObjectHtml;
    ObjHtml.innerHTML = "";

    //ESTRUCTURA 
    var Container = new TVclStackPanel(ObjHtml, "ContainerLoging", 1, [[12], [12], [12], [12], [12]]);
    var ContainerCont = Container.Column[0].This;

    //div0  BANDERA
    var div0 = new TVclStackPanel(ContainerCont, "Loging_div0", 1, [[12], [12], [12], [12], [12]]);
    var div0Cont = div0.Column[0].This;

    //div1 LOGO / TITULO
    var div1 = new TVclStackPanel(ContainerCont, "Loging_div1", 2, [[12, 12], [3, 9], [3, 9], [3, 9], [3, 9]]);
    var div1_1Cont = div1.Column[0].This; // Logo
    var div1_2Cont = div1.Column[1].This; // Titulo

    //USER 
    var div2 = new TVclStackPanel(ContainerCont, "Logigdiv2", 2, [[0, 12], [3, 9], [3, 9], [3, 9], [3, 9]]);
    var div2_Cont = div2.Column[1].This;
    var div2_1 = new TVclStackPanel(div2_Cont, "Loging_div2_1", 2, [[12, 12], [5, 7], [5, 7], [5, 7], [5, 7]]);
    var div2_1_1Cont = div2_1.Column[0].This; //Label
    var div2_1_2Cont = div2_1.Column[1].This; //Text
    $(div2_1_2Cont).css("margin-top", "10px");

    //Password
    var div3 = new TVclStackPanel(ContainerCont, "Loging_div3", 2, [[0, 12], [3, 9], [3, 9], [3, 9], [3, 9]]);
    var div3_Cont = div3.Column[1].This;
    var div3_1 = new TVclStackPanel(div3_Cont, "Loging_div3_1", 2, [[12, 12], [5, 7], [5, 7], [5, 7], [5, 7]]);
    var div3_1_1Cont = div3_1.Column[0].This; //Label
    var div3_1_2Cont = div3_1.Column[1].This; //Text
    $(div3_1_2Cont).css("margin-top", "10px");

    //Boton
    var div4 = new TVclStackPanel(ContainerCont, "Loging_div4", 1, [[12], [12], [12], [12], [12]]);
    var div4Cont = div4.Column[0].This;

    var div5 = new TVclStackPanel(ContainerCont, "Loging_div5", 1, [[12], [12], [12], [12], [12]]);
    var div5Cont = div5.Column[0].This;

    //AGREGAR ELEMENTOS
    var bandera = new TVclImagen(div0Cont, "LogingFlag");
    if (_this.idLenguaje == 1) {
        bandera.Src = "image/24/Spain-flag.png"
    } else {
        bandera.Src = "image/24/United-states-flag.png";

    }
    bandera.Cursor = "Pointer";
    bandera.onClick = function Change_Lenguage() {
        if (_this.idLenguaje == 1) {
            _this.idLenguaje = 2;
            bandera.Src = "image/24/United-states-flag.png"

        } else {
            _this.idLenguaje = 1;
            bandera.Src = "image/24/Spain-flag.png"

        }
        _this.SaveLocalLanguage();
        LabUser.innerText = _this.arrayleng[0][_this.idLenguaje];
        _this.LabPassw.Text = _this.arrayleng[1][_this.idLenguaje];
        _this.btnAccept.Text = _this.arrayleng[2][_this.idLenguaje];
        _this.btnAccept.Src = "image/16/Logout.png";
    }

    var Logo = new TVclImagen(div1_1Cont, "LogingLogo");
    Logo.Src = "";//"image/High/IT-Help-Center-A.png";
    Logo.This.classList.add("LoguinSources_ImageLogin");


    var Title = new TVclImagen(div1_2Cont, "LogingTitle");
    Title.Src = "image/High/IT-Help-Center-B.png";

    var LabUser = new Vcllabel(div2_1_1Cont, "LogingLabUser", TVCLType.BS, TlabelType.H0, _this.arrayleng[0][_this.idLenguaje]);
    LabUser.classList.add("GlobalSources_NormalText");



    _this.TextUser = new TVclTextBox(div2_1_2Cont, "LoginTextUser");
    _this.TextUser.This.setAttribute("list", "usersList");
    _this.TextUser.This.addEventListener("keyup", function (e) {
        tecla = (document.all) ? e.keyCode : e.which;
        if (tecla == 13) {
            _this.TextPassw.This.focus();
        }
    }, false);

    //    tecla = (document.all) ? e.keyCode : e.which;
    //    if (tecla == 13) {
    //        e.preventDefault();
    //        //if (_this.TextUser.This.value.length != 0) {
    //        //_this.TextUser.This.value = _this.TextUser.This.value;
    //        var selectionado = null;
    //        for (var i = 0; i < _this.DataListTemp.options.length; i++) {
    //            if (_this.DataListTemp.options[i].selected)
    //                selectionado = _this.DataListTemp.options[i];
    //        }
    //        //console.log(my_field.value);
    //        // Run my specific process with my_field.value 
    //        //_this.TextUser.This.value = '';
    //        //}
    //      _this.TextPassw.This.focus();
    //    }
    //}


    //_this.LabPassw = new Vcllabel(div3_1_1Cont, "LogingLabPassw", TVCLType.BS, TlabelType.H0, _this.arrayleng[1][_this.idLenguaje]);

    _this.LabPassw = new TVcllabel(div3_1_1Cont, "LogingLabPassw", TlabelType.H0);
    _this.LabPassw.Text = _this.arrayleng[1][_this.idLenguaje];
    _this.LabPassw.VCLType = TVCLType.BS;
    _this.LabPassw.This.classList.add("GlobalSources_NormalText");

    _this.TextPassw = new TVclTextBox(div3_1_2Cont, "LoginTextPassw");
    _this.TextPassw.This.type = "password";
    _this.TextPassw.This.onkeypress = function validar(e) {
        tecla = (document.all) ? e.keyCode : e.which;

        if (tecla == 13) _this.access(_this, _this.TextPassw);
    }

    _this.btnAccept = new TVclButton(div4Cont, "LogingbtnAccept");
    _this.btnAccept.Cursor = "pointer";
    _this.btnAccept.Text = _this.arrayleng[2][_this.idLenguaje];
    _this.btnAccept.Src = "image/16/Logout.png";
    _this.btnAccept.VCLType = TVCLType.BS;
    _this.btnAccept.onClick = function myfunction() {
        if (_this.TextUser.This.value == "") {
            alert('Login.access: User is empty');
            return;
        }
        if (!SysCfg.App.Properties.DataDomain.isValidDomain) {
            if (_this.TextPassw.This.value == "") {
                alert('Login.access: Password is empty');
                return;
            }
        }
        _this.access(_this, _this.btnAccept);
    }

    this.VcllabelVersion = new Vcllabel(div5Cont, "LogingLabPassw", TVCLType.BS, TlabelType.H0, _this.arrayleng[4][_this.idLenguaje] + "0.0.0.0");
    _this.VcllabelVersion.innerText = _this.arrayleng[4][_this.idLenguaje] + UsrCfg.Version._UsrCfgAppVersion;
    _this.VcllabelVersion.classList.add("GlobalSources_NormalText");


    //*************************
    //Carlos
    SysCfg.App.Methods.Cfg_WindowsTask.User.CI_GENERICNAME = "";
    for (var i = 0; i < SysCfg.App.Methods.Cfg_WindowsTask.User.OLD_GENERICNAME.Length; i++) {
        SysCfg.App.Methods.Cfg_WindowsTask.User.OLD_GENERICNAME[i] = "";
    }
    if (!UsrCfg.Version.IsProduction) {

        SysCfg.App.Methods.Cfg_WindowsTask.User.CI_GENERICNAME = "UsrSrvDsk2";
        SysCfg.App.Methods.Cfg_WindowsTask.User.OLD_GENERICNAME[0] = "UsrSrvDsk1";
        SysCfg.App.Methods.Cfg_WindowsTask.User.OLD_GENERICNAME[1] = "UsrPbm1";
        SysCfg.App.Methods.Cfg_WindowsTask.User.OLD_GENERICNAME[2] = "UsrPbm2";
        SysCfg.App.Methods.Cfg_WindowsTask.User.OLD_GENERICNAME[3] = "Usr 001";
        SysCfg.App.Methods.Cfg_WindowsTask.User.OLD_GENERICNAME[4] = "Usr 002";
        SysCfg.App.Methods.Cfg_WindowsTask.User.OLD_GENERICNAME[5] = "ADMINISTRATOR";
    }
    else {
        SysCfg.App.Methods.LoadbyIniDataSet(function () {
        });
    }








    _this.DataListTemp = document.createElement("datalist");
    _this.DataListTemp.id = "usersList"
    _this.DataListTemp.addEventListener("change", function (e) {
        alert("change realizado");
    }, false);
    $(div2_1_2Cont).append(_this.DataListTemp);
    //$UserAtRole
    if (SysCfg.App.Methods.Cfg_WindowsTask.User.CI_GENERICNAME != "") {
        for (var i = 0; i < SysCfg.App.Methods.Cfg_WindowsTask.User.OLD_GENERICNAME.length; i++) {
            if (SysCfg.App.Methods.Cfg_WindowsTask.User.OLD_GENERICNAME[i] != "" && SysCfg.App.Methods.Cfg_WindowsTask.User.OLD_GENERICNAME[i] != undefined) {
                var OptionTemp1 = document.createElement("option");
                OptionTemp1.value = SysCfg.App.Methods.Cfg_WindowsTask.User.OLD_GENERICNAME[i]
                $(_this.DataListTemp).append(OptionTemp1);
                //cbxUsuario.Items.Add(SysCfg.App.Methods.Cfg_WindowsTask.User.OLD_GENERICNAME[i]);
            };
        }
        _this.TextUser.value = SysCfg.App.Methods.Cfg_WindowsTask.User.CI_GENERICNAME;
        _this.TextUser.Text = SysCfg.App.Methods.Cfg_WindowsTask.User.CI_GENERICNAME;
        _this.User = SysCfg.App.Methods.Cfg_WindowsTask.User.CI_GENERICNAME;
        _this.TextPassw.This.focus();
    } else {
        _this.TextUser.This.focus();
    }

    //***************
    //SysCfg.App.Properties.DataDomain.isValidDomain

    //***************
    if (SysCfg.App.Properties.DataDomain.isValidDomain) {
        _this.TextUser.Enabled = false;
        _this.TextPassw.Visible = false;
        _this.LabPassw.Visible = false;
        _this.TextUser.Text = SysCfg.App.Properties.DataDomain.DomainUser;
    }



}

ItHelpCenter.TLogin.prototype.SaveLocalLanguage = function () {
    var _this = this.TParent();
    //var _DBLocalLeng = new SysCfg.ini.DBLocalLeng('LocalLeng', 1);
    //_DBLocalLeng.Open(function (e) {
    //    _DBLocalLeng.Write(_this.idLenguaje, function (e) {
    //    }, function (e) {
    //        alert(" can´t Write LocalIndexDB");
    //    });
    //}, function (e) {
    //    alert(" can´t load LocalIndexDB");
    //});

    var _DBLocalLeng = new SysCfg.ini.LSLocalLeng();
    _this.idLenguaje = _DBLocalLeng.Write(_this.idLenguaje);
}

ItHelpCenter.TLogin.prototype.access = function (sender, e) {
    var _this = sender;
    try {
        UsrCfg.WaitMe.ShowWaitme();

        SysCfg.App.Methods.Cfg_WindowsTask.User.CI_GENERICNAME = _this.TextUser.Text;
        SysCfg.App.Methods.Cfg_WindowsTask.User.PASSWORD = _this.TextPassw.This.value;

        SysCfg.App.Methods.Cfg_WindowsTask.User = Comunic.AjaxJson.Client.Methods.GetUser(SysCfg.App.Methods.Cfg_WindowsTask.User.CI_GENERICNAME, SysCfg.App.Methods.Cfg_WindowsTask.User.PASSWORD, SysCfg.App.Properties.DataDomain.AccountDomainSid);
        if (SysCfg.App.Methods.Cfg_WindowsTask.User.ResErr.NotError) {
            UsrCfg.Library.loadSecond();
            UsrCfg.SC.Methods.InitializeUser(SysCfg.App.Methods.Cfg_WindowsTask.User);
            SysCfg.App.Methods.SavebyIniDataSet();
            if ((SysCfg.App.Methods.Cfg_WindowsTask.User.EXPIRED_PASSWORD) && (!SysCfg.App.Properties.DataDomain.isValidDomain)) {
                _this.Callback(ItHelpCenter.PageManager.TGoToPageType.ChangePassword);
            }
            else {
                _this.Callback(ItHelpCenter.PageManager.TGoToPageType.BeforeMain);
            }
        }
        else {
            UsrCfg.WaitMe.CloseWaitme();
            alert(_this.arrayleng[3][_this.idLenguaje]);
            _this.TextUser.value = _this.User;
            _this.TextPassw.value = "";
            _this.TextPassw.This.focus();
        }
    } catch (e) {
        UsrCfg.WaitMe.CloseWaitme();
    }




    /*
    var datalogin = {
        'username': username,
        'password': password
    };
    try { 
        $.ajax({
            type: "POST",
            async: false,
            url: 'Service/wcfLoguin.svc/Login',
            data: JSON.stringify(datalogin),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {                     
                if (response.Denied) {
                    alert(arrayleng[3][_this.idLenguaje]);
                    _this.TextUser.value = _this.User;
                    _this.TextPassw.value = "";
                    _this.TextPassw.focus();
                }
                else if (response.Expired_password) {
                    _this.Callback(ItHelpCenter.PageManager.GoToPageType.ChangePassword);
                }
                else {                        
                    _this.Callback(ItHelpCenter.PageManager.TGoToPageType.BeforeMain);
                   
                }                    
            },
            error: function (response) {
                alert('Error Service Login' + response);
            }
        });  
    } catch (e) {
        alert('Error Login');
    }
    */
}


