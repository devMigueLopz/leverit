﻿Comunic.Properties.TGetSQLOpen = function()
{
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.ResErr = new SysCfg.Error.Properties.TResErr();
    this.SQLOpen = new Array();
}


Comunic.Properties.TGetCfg_Bdd = function()//ya
{
    this.ResErr = new SysCfg.Error.Properties.TResErr();
    this.Cfg_Bdd = new SysCfg.DB.Properties.TCfg_Bdd();
}

Comunic.Properties.TGetContLic = function()//ya
{
    this.ResErr = new SysCfg.Error.Properties.TResErr();
    this.ContLic = SysCfg.License.Properties.TContLic;
    this.Config_Language = new SysCfg.App.Properties.TConfig_Language();
    this.Config_LanguageITF = new SysCfg.App.Properties.TConfig_Language();
    this.Config_RemoteHelp = new SysCfg.App.Properties.TConfig_RemoteHelp();
}


Comunic.Properties.TGetSQLPrefixIDBuild = function()//ya
{
    this.ResErr = new SysCfg.Error.Properties.TResErr();
    this.StrSQL = "";
}

//********************** GetCrypto *****************************
Comunic.Properties.TGetCrypto = function()//ya
{
    this.ResErr = new SysCfg.Error.Properties.TResErr();
    this.CryptoType = Comunic.Properties.TCryptoType._MD5;
    this.Hash = "";
    this.StrCrypto1 = "";
    this.StrCrypto2 = "";
}

//********************** GetHHH *****************************
Comunic.Properties.TGetHHH = function()//ya
{
    this.ResErr = new SysCfg.Error.Properties.TResErr();
    this.HHHType = Comunic.Properties.THHHType_UUU;
    this.Byte_Request = new Array();
    this.Byte_Response = new Array();
}

//********************** GetCatalog *****************************
Comunic.Properties.TGetCatalog = function()//ya
{
    this.ResErr = new SysCfg.Error.Properties.TResErr();
    this.CatalogType = Comunic.Properties.TCatalogType._All;
    this.Byte_Request=new Array();
    this.Byte_Response=new Array();
}

//********************** GetPersistence *****************************
Comunic.Properties.TGetPersistence = function()//ya
{
    this.ResErr = new SysCfg.Error.Properties.TResErr();          
    this.PersistenceSource = Comunic.Properties.TPersistenceSource._None;
    this.PersistenceType = Comunic.Properties.TPersistenceType._None;
    this.PersistenceCmd = Comunic.Properties.TPersistenceCmd._None;
    this.this.Byte_Request=new Array();
    this.Byte_Response=new Array();
}


//********************** GetInterno *****************************
Comunic.Properties.TGetInterno = function()//ya
{
    this.ResErr = new SysCfg.Error.Properties.TResErr();
    this.InternoType = Comunic.Properties.TInternoType =  _UUU;
    this.StrResultInterno = "";
    this.Byte_Structure=new Array();
    this.Generate = false;
    this.Run = false;
    this.GetStructure = false;
}

//********************** GetNotify *****************************
Comunic.Properties.TGetNotify = function()//ya
{
    this.ResErr = new SysCfg.Error.Properties.TResErr();
    this.NotifyType = Comunic.Properties.TNotifyType._ADDbySteep;
    this.Byte_Request = new Array();
    this.Byte_Response = new Array();
}

//********************** GetSDFunction *****************************
Comunic.Properties.TGetSDFunction = function ()
{
    this.ResErr = new SysCfg.Error.Properties.TResErr();
    this.Byte_Request = new Array();
    this.Byte_Response = new Array();
}

//********************** GetChat *****************************
Comunic.Properties.TGetChat = function()//ya
{
    this.ResErr = new SysCfg.Error.Properties.TResErr();
    this.TChatType =Comunic.Properties.TChatType.ASSIGNED_OWNER;
    this.IDCMDBCI = 0;
    this.MESSAGE = "";
    this.TITLE = "";
    this.IDSMCHATASSIGNED = 0;
    this.IDSMCHAT = 0;
    this.IDSMCHATASSIGNEDMEMBER = 0;
}

//********************** GetRHREQUEST *****************************
Comunic.Properties.TGetRHREQUEST = function()//ya
{
    this.ResErr = new SysCfg.Error.Properties.TResErr();
    this.RHREQUESTType=Comunic.Properties.TRHREQUESTType.CREATE;
    this.Byte_Request=new Array();
    this.Byte_Response=new Array();
}





//********************** GetLiveChange *****************************
Comunic.Properties.TGetLiveChange = function()//ya
{
    this.ResErr = new SysCfg.Error.Properties.TResErr();
    this.LiveChangeType=Comunic.Properties.TLiveChangeType.OnlyLive;
    this.IDCMDBCI;
    this.IDSMCHAT;
    this.IDSMCHATASSIGNED;
    this.IDSDNOTIFY;
    this.IDSMCHATASSIGNEDMEMBER;
    this.ByteLiveChange=new Array();
                   
}

Comunic.Properties.TGetUser = function()//ya
{
    this.ResErr = new SysCfg.Error.Properties.TResErr();
    this.ByteUser=new Array();
}

Comunic.Properties.TGetSDfile = function()//ya
{
    this.ResErr = new SysCfg.Error.Properties.TResErr();          
    this.FileType = Comunic.Properties.TFileType._ServiceDesk;
    this.FileOperationType=Comunic.Properties.TFileOperationType._ReportWrite;
    this.IDCode = "";
    this.ByteFile=new Array();
}

Comunic.Properties.TGetReportfile = function ()//ya
{
    this.ResErr = new SysCfg.Error.Properties.TResErr();
    this.ReportType = Comunic.Properties.TReportType._Word;
    this.ReportOperationType=Comunic.Properties.TReportOperationType._ReportWrite;
    this.IDCode = "";
    this.ByteFile=new Array();
}


Comunic.Properties.TGetSQLOpen = function ()//ya
{
    this.ResErr = new SysCfg.Error.Properties.TResErr();
    this.SQLOpen=new Array();
}

Comunic.Properties.TGetIDI = function ()
{
    this.ResErr = new SysCfg.Error.Properties.TResErr();
    this.IDIByte=new Array();
}

Comunic.Properties.TGetSQLExec = function ()//ya
{
    this.ResErr = new SysCfg.Error.Properties.TResErr();
}

Comunic.Properties.TGetSesion = function ()//ya
{
    this.ResErr = new SysCfg.Error.Properties.TResErr();
    this.Num = 0;//Int64
}

Comunic.Properties.TGetSSLCode = function ()//ya
{
    this.ResErr = new SysCfg.Error.Properties.TResErr();
    this.SSLCode = 0;//Int64      
}

Comunic.Properties.TGetPing = function ()//ya
{
    this.ResErr = new SysCfg.Error.Properties.TResErr();
    this.Ping = false;
    this.isValidDomain = false;
}

Comunic.Properties.TIDIType = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _GETIDITable: { value: 0, name: "_GETIDITable"},
    _SETLangText: { value: 1, name: "_SETLangText"}
}

//*******************************************************     
Comunic.Properties.TFileType = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _ServiceDesk: { value: 0, name: "_ServiceDesk"},
    _Photo: { value: 1, name: "_Photo"},
    _CIDefine: { value: 2, name: "_CIDefine"},
    _AtentionLayout: { value: 3, name: "_AtisAtentionLayout"},
    _DashBoardLayout: { value: 4, name: "_DashBoardLayout"}     
}

//*******************************************************     
Comunic.Properties.TReportType = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _Word: { value: 0, name: "_Word"}
}

Comunic.Properties.TFileOperationType = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _FileWrite: { value: 0, name: "_FileWrite"},
    _FileRead: { value: 1, name: "_FileRead"},
    _FileDelete: { value: 2, name: "_FileDelete"},
    _FileUpdate: { value: 3, name: "_FileUpdate" },
    _MovetoServerITHC: { value: 4, name: "_MovetoServerITHC" },
    _MovetoClientITHC: { value: 5, name: "_MovetoClientITHC" },
    
}

Comunic.Properties.TReportOperationType = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _ReportWrite: { value: 0, name: "_ReportWrite"},
    _ReportRead: { value: 1, name: "_ReportRead"},
    _ReportDelete: { value: 2, name: "_ReportDelete"},
    _ReportUpdate: { value: 3, name: "_ReportUpdate"},
    _ReportGenerate: { value: 4, name: "_ReportGenerate"}
}
      
//********************** GetCrypto *****************************
Comunic.Properties.TCryptoType = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _MD5: { value: 0, name: "_MD5"}
}

//********************** GetHHH *****************************
Comunic.Properties.THHHType = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _UUU: { value: 0, name: "_UUU"}
}

//********************** GetCatalog *****************************
Comunic.Properties.TCatalogType = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _All: { value: 0, name: "_All"}
}



//********************** GetPersistence *****************************
Comunic.Properties.TPersistenceSource = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _None: { value: -1, name: "_None"},
    _DataBase: { value: 0, name: "_DataBase"},
    _Memmory: { value: 1, name: "_Memmory"}
}

Comunic.Properties.TPersistenceType = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _None: { value: -1, name: "_None"},
    _CMDBUSERSETTINGS: { value: 0, name: "_CMDBUSERSETTINGS"}
}

Comunic.Properties.TPersistenceCmd = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _None: { value: -1, name: "_None"},
    _Fill: { value: 0, name: "_Fill"},
    _GETID: { value: 1, name: "_GETID"},
    _ADD: { value: 2, name: "_ADD"},
    _UPD: { value: 3, name: "_UPD"},
    _DEL1: { value: 4, name: "_DEL1"},
    _DEL2: { value: 5, name: "_DEL2"},
    _OTHER1: { value: 6, name: "_OTHER1"},
    _OTHER2: { value: 7, name: "_OTHER2"},
    _OTHER3: { value: 8, name: "_OTHER3"},
    _OTHER4: { value: 9, name: "_OTHER4"},
    _OTHER5: { value: 10, name: "_OTHER5"},
    _OTHER6: { value: 11, name: "_OTHER6"},
    _OTHER7: { value: 12, name: "_OTHER7"},
    _OTHER8: { value: 13, name: "_OTHER8"}
}

//********************** GetInterno *****************************
Comunic.Properties.TInternoType = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _UUU: { value: -1, name: "_UUU"},
    _GlobalInternos: { value: 0, name: "_GlobalInternos"},
    _RunBaseLine: { value: 1, name: "_RunBaseLine"},
    _RunExtraTable: { value: 2, name: "_RunExtraTable"},
    _RunExtraTableService: { value: 3, name: "_RunExtraTableService"}
}    

//********************** GetNotify *****************************
Comunic.Properties.TNotifyType = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    _ADDbySteep: { value: 0, name: "_ADDbySteep"},
    _ADDbyTypeEvent: { value: 1, name: "_ADDbyTypeEvent"},
    _ISSENDCONSOLE: { value: 2, name: "_ISSENDCONSOLE"}
}

//********************** GetChat *****************************
Comunic.Properties.TChatType = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    //resultado
    //OK: { value: 0, name: "OK"},
    //NO: { value: 1, name: "NO"},
    //Peticion 
    ASSIGNED_OWNER: { value: 1, name: "ASSIGNED_OWNER"},
    SENDS_MESSAGE: { value: 2, name: "SENDS_MESSAGE"},
    ASSIGNED_DISABLE: { value: 3, name: "ASSIGNED_DISABLE"},
    MEMBER_DISABLE: { value: 4, name: "MEMBER_DISABLE"}
}

Comunic.Properties.TRHREQUESTType = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    CREATE: { value: 1, name: "CREATE"}
}

//********************** GetLiveChange *****************************
Comunic.Properties.TLiveChangeType = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    OnlyLive: { value: 1, name: "OnlyLive"},
    LiveChange: { value: 2, name: "LiveChange"}
}

