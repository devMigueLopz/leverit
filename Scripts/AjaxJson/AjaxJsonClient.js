﻿//Comunic.AjaxJson.Client.Methods
Comunic.AjaxJson.Client.Properties.TGetComunic = function () {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.ResErr = new SysCfg.Error.Properties.TResErr();
    this.response;
}

Comunic.AjaxJson.Client.Methods.GetComunic = function (infunction, data) {
    var GetComunic = new Comunic.AjaxJson.Client.Properties.TGetComunic;
    SysCfg.Error.Properties.ResErrfill(GetComunic.ResErr);
    //*******************************************
    
    $.ajax({
        type: "POST",
        url: SysCfg.App.Properties.xRaiz + 'Service/ComunicService/ComunicMethods.svc/' + infunction,
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (response) {
            GetComunic.response = response;
            GetComunic.ResErr.NotError = true;
            GetComunic.ResErr.Mesaje = SysCfg.Error.Properties._ResOK;
        },
        error: function (response) {
            SysCfg.Log.Methods.WriteLog("AjaxJsonClient.js Comunic.AjaxJson.Client.Properties.TGetComunic " + infunction);
            GetComunic.ResErr.Mesaje = response.ErrorMessage;
        }
    });
    return (GetComunic);
}

