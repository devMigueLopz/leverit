﻿Comunic.AjaxJson.Client.Methods.CopyResErr = function (ResErr, PropertiesTResErr) {
    ResErr.NotError = PropertiesTResErr.NotError;
    ResErr.Mesaje = PropertiesTResErr.Mesaje;
    ResErr.Exception = PropertiesTResErr.Exception;
    ResErr.Date = PropertiesTResErr.Date;
}

Comunic.AjaxJson.Client.Methods.TestDataType = function () {    
    var ByteArray = new Array(0);
    var data = {
        'BYTEARRAY': ByteArray
    };
    GetComunic = Comunic.AjaxJson.Client.Methods.GetComunic("TestDataType",data);
    if (GetComunic.ResErr.NotError) {
        var MemStream = new SysCfg.Stream.TMemoryStream();
        MemStream.Write(response, 0, response.length);
        MemStream.Position = 0;
        var MemStreamResponse = new SysCfg.Stream.TMemoryStream();
        MemStreamResponse.Position = 0;
        try {
            var StrInt16_1 = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
            SysCfg.Stream.Methods.WriteStreamStrInt16(MemStreamResponse, StrInt16_1);
            var StrInt16_2 = SysCfg.Stream.Methods.ReadStreamStrInt16(MemStream);
            SysCfg.Stream.Methods.WriteStreamStrInt16(MemStreamResponse, StrInt16_2);
            var Int16 = SysCfg.Stream.Methods.ReadStreamInt16(MemStream);
            SysCfg.Stream.Methods.WriteStreamInt16(MemStreamResponse, Int16);
            var Int32 = SysCfg.Stream.Methods.ReadStreamInt32(MemStream);
            SysCfg.Stream.Methods.WriteStreamInt32(MemStreamResponse, Int32);
            var Double = SysCfg.Stream.Methods.ReadStreamDouble(MemStream);
            SysCfg.Stream.Methods.WriteStreamDouble(MemStreamResponse, Double);
            var Decimal = SysCfg.Stream.Methods.ReadStreamDecimal(MemStream);
            SysCfg.Stream.Methods.WriteStreamDecimal(MemStreamResponse, Decimal);
            var Boolean1 = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
            SysCfg.Stream.Methods.WriteStreamBoolean(MemStreamResponse, Boolean1);
            var Boolean2 = SysCfg.Stream.Methods.ReadStreamBoolean(MemStream);
            SysCfg.Stream.Methods.WriteStreamBoolean(MemStreamResponse, Boolean2);
            var DateTime = SysCfg.Stream.Methods.ReadStreamDateTime(MemStream);
            SysCfg.Stream.Methods.WriteStreamDateTime(MemStreamResponse, DateTime);
            for (var i = 0; i < MemStreamResponse.Stream.length; i++) {
                if (MemStreamResponse.Stream[i] != MemStream.Stream[i]) {
                    alert("diferente(" + i + "):[" + MemStreamResponse.Stream[i] + "," + MemStream.Stream[i] + "]");

                }
            }
            alert(
              "R=StrInt16_1=" + StrInt16_1
            + ", StrInt16_2=" + StrInt16_2
            + ", Int16=" + Int16
            + ", Int32=" + Int32
            + ", Double=" + Double
            + ", Decimal=" + Decimal
            + ", Boolean1=" + Boolean1
            + ", Boolean2=" + Boolean2
            + ", DateTime=" + formatJSONDate(DateTime)
            );
        }
        catch (e) {
            SysCfg.Log.Methods.WriteLog("DemoMemTable.js Comunic.AjaxJson.Client.Methods.TestDataType ", e);
            alert(Excep);
        }
    }
    else {
        alert(GetComunic.ResErr.Mesaje);
    }
    return (GetSQLOpen);
}

Comunic.AjaxJson.Client.Methods.GetCfg_Bdd = function ()
{
    GetCfg_Bdd = new Comunic.Properties.TGetCfg_Bdd();  
    var data = {
        //'ValueStr': "",
        //'Valueint': 1
    };
    GetComunic = Comunic.AjaxJson.Client.Methods.GetComunic("GetCfg_Bdd",data);
    if (GetComunic.ResErr.NotError) {
        Comunic.AjaxJson.Client.Methods.CopyResErr(GetCfg_Bdd.ResErr, GetComunic.response.ResErr);

        GetCfg_Bdd.Cfg_Bdd.NumDateTimeServer = GetComunic.response.Cfg_Bdd.NumDateTimeServer;
        GetCfg_Bdd.Cfg_Bdd.NUmDateTimeNow = Date.now();
        GetCfg_Bdd.Cfg_Bdd.FechaServidor = GetCfg_Bdd.Cfg_Bdd.NumDateTimeServer - GetCfg_Bdd.Cfg_Bdd.NUmDateTimeNow;

        GetCfg_Bdd.Cfg_Bdd.BddAccess = SysCfg.DB.Properties.TBDDAccess.GetEnum(GetComunic.response.Cfg_Bdd.BddAccess);
        GetCfg_Bdd.Cfg_Bdd.BDDInvalida = GetComunic.response.Cfg_Bdd.BDDInvalida;
        GetCfg_Bdd.Cfg_Bdd.UDLlink = GetComunic.response.Cfg_Bdd.UDLlink;
        GetCfg_Bdd.Cfg_Bdd.ExtDataBase = GetComunic.response.Cfg_Bdd.ExtDataBase;
        GetCfg_Bdd.Cfg_Bdd.BddMode = SysCfg.DB.Properties.TBddModeType.GetEnum(GetComunic.response.Cfg_Bdd.BddMode);
        GetCfg_Bdd.Cfg_Bdd.FormatoTime = GetComunic.response.Cfg_Bdd.FormatoTime;
        GetCfg_Bdd.Cfg_Bdd.FormatoDate = GetComunic.response.Cfg_Bdd.FormatoDate;
        GetCfg_Bdd.Cfg_Bdd.FormatoFecha = GetComunic.response.Cfg_Bdd.FormatoFecha;
        GetCfg_Bdd.Cfg_Bdd.FormatoTimeStr = GetComunic.response.Cfg_Bdd.FormatoTimeStr;
        GetCfg_Bdd.Cfg_Bdd.FormatoDateStr = GetComunic.response.Cfg_Bdd.FormatoDateStr;
        GetCfg_Bdd.Cfg_Bdd.FormatoFechaStr = GetComunic.response.Cfg_Bdd.FormatoFechaStr;  
        GetCfg_Bdd.ResErr.Mesaje = SysCfg.Error.Properties._ResOK; 
    }
    else {
        GetCfg_Bdd.ResErr.Mesaje = GetComunic.ResErr.Mesaje;

    }
    return (GetCfg_Bdd);
}

Comunic.AjaxJson.Client.Methods.GetContLic = function ()
{
    GetContLic = new Comunic.Properties.TGetContLic();
    var data = {
        //'ValueStr': "",
        //'Valueint': 1
    };
    GetComunic = Comunic.AjaxJson.Client.Methods.GetComunic("GetContLic",data);
    if (GetComunic.ResErr.NotError) {
        Comunic.AjaxJson.Client.Methods.CopyResErr(GetContLic.ResErr, GetComunic.response.ResErr);
        GetContLic.ContLic.SerialValido = GetComunic.response.ContLic.SerialValido;
        GetContLic.ContLic.num_pcs = GetComunic.response.ContLic.num_pcs;
        GetContLic.ContLic.num_Consultas = GetComunic.response.ContLic.num_Consultas;
        GetContLic.ContLic.num_edit = GetComunic.response.ContLic.num_edit;
        GetContLic.ContLic.num_adms = GetComunic.response.ContLic.num_adms;
        GetContLic.ContLic.num_visor = GetComunic.response.ContLic.num_visor;
        SysCfg.License.Properties.ContLic = GetContLic.ContLic; //copia directamente a la variable glogal para seguridad 
        GetContLic.Config_Language.Language = SysCfg.App.Properties.TLanguage.GetEnum(GetComunic.response.Config_Language.Language);
        SysCfg.App.Properties.Config_Language = GetContLic.Config_Language;
        GetContLic.Config_Language.LanguageITF = SysCfg.App.Properties.TLanguage.GetEnum(GetComunic.response.Config_Language.LanguageITF);        
        SysCfg.App.Properties.Config_LanguageITF = GetContLic.Config_Language;
        GetContLic.Config_RemoteHelp.CR_ChatInternet =  GetContLic.CR_ChatInternet;
        SysCfg.App.Properties.Config_RemoteHelp = GetContLic.Config_RemoteHelp;


    }
    else {
        GetContLic.ResErr.Mesaje = GetComunic.ResErr.Mesaje;
    }
    return (GetContLic);
}

Comunic.AjaxJson.Client.Methods.GetSQLPrefixIDBuild = function (Prefix, ID, SQLPARAM) {
    GetSQLPrefixIDBuild = new Comunic.Properties.TGetSQLPrefixIDBuild();
    var data = {
        'Prefix': Prefix,
        'ID': ID,
        'SQLPARAM': SQLPARAM
    };
    GetComunic = Comunic.AjaxJson.Client.Methods.GetComunic("GetSQLPrefixIDBuild", data);
    if (GetComunic.ResErr.NotError) {
        Comunic.AjaxJson.Client.Methods.CopyResErr(GetSQLPrefixIDBuild.ResErr, GetComunic.response.ResErr);
        GetSQLPrefixIDBuild.StrSQL = GetComunic.response.StrSQL;
    }
    else {
        GetSQLPrefixIDBuild.ResErr.Mesaje = GetComunic.ResErr.Mesaje;
    }
    //didijoca seekserach
    return (GetSQLPrefixIDBuild);
}

Comunic.AjaxJson.Client.Methods.GetIDI = function (Application,ID,IDSUB,LangText,IDIType,Language)
{            
    GetIDI = new Comunic.Properties.TGetIDI();
    var data = {
        'Application': Application.value,
        'ID': ID,
        'IDSUB': IDSUB,
        'LangText': LangText,
        'IDIType': IDIType.value,
        'Language': Language.value
    };
    GetComunic = Comunic.AjaxJson.Client.Methods.GetComunic("GetIDI",data);
    if (GetComunic.ResErr.NotError) {
        Comunic.AjaxJson.Client.Methods.CopyResErr(GetIDI.ResErr, GetComunic.response.ResErr);
        GetIDI.IDIByte = GetComunic.response.IDIByte;
    }
    else {
        GetIDI.IDIByte = new Array(0);
        GetIDI.ResErr.Mesaje = GetComunic.ResErr.Mesaje;
    }

    return (GetIDI);
}

Comunic.AjaxJson.Client.Methods.GetSQLOpen = function (Prefix,ID,SQLPARAM)
{
    GetSQLOpen = new Comunic.Properties.TGetSQLOpen();
    var data = {
        'Prefix': Prefix,
        'ID': ID,
        'SQLPARAM': SQLPARAM
    };
    GetComunic = Comunic.AjaxJson.Client.Methods.GetComunic("GetSQLOpen", data);
    if (GetComunic.ResErr.NotError) {
        Comunic.AjaxJson.Client.Methods.CopyResErr(GetSQLOpen.ResErr, GetComunic.response.ResErr);
        GetSQLOpen.SQLOpen = GetComunic.response.SQLOpen;        
    }
    else {
        GetSQLOpen.ResErr.Mesaje = GetComunic.ResErr.Mesaje;
    }

    return (GetSQLOpen);
}

Comunic.AjaxJson.Client.Methods.GetStrSQLOpen = function (StrSQL,SQLPARAM)
{
           
    GetSQLOpen = new Comunic.Properties.TGetSQLOpen();
    var data = {
        'StrSQL': StrSQL,
        'SQLPARAM': SQLPARAM
    };
    GetComunic = Comunic.AjaxJson.Client.Methods.GetComunic("GetStrSQLOpen",data);
    if (GetComunic.ResErr.NotError) {
        Comunic.AjaxJson.Client.Methods.CopyResErr(GetSQLOpen.ResErr, GetComunic.response.ResErr);
        GetSQLOpen.SQLOpen = GetComunic.response.SQLOpen;
    }
    else {
        GetSQLOpen.ResErr.Mesaje = GetComunic.ResErr.Mesaje;
    }
    return (GetSQLOpen);
}

Comunic.AjaxJson.Client.Methods.GetSQLExec = function (Prefix, ID, SQLPARAM)
{
    GetSQLExec = new Comunic.Properties.TGetSQLExec();
    var data = {
        'Prefix': Prefix,
        'ID': ID,
        'SQLPARAM': SQLPARAM
    };
    GetComunic = Comunic.AjaxJson.Client.Methods.GetComunic("GetSQLExec",data);
    if (GetComunic.ResErr.NotError) {
        Comunic.AjaxJson.Client.Methods.CopyResErr(GetSQLExec.ResErr, GetComunic.response.ResErr);        
    }
    else {
        GetSQLExec.ResErr.Mesaje = GetComunic.ResErr.Mesaje;
    }
    return (GetSQLExec);
}

Comunic.AjaxJson.Client.Methods.GetStrSQLExec = function (StrSQL,SQLPARAM)
{           
    GetSQLExec = new Comunic.Properties.TGetSQLExec();
    var data = {
        'StrSQL': StrSQL,
        'SQLPARAM': SQLPARAM
    };
    GetComunic = Comunic.AjaxJson.Client.Methods.GetComunic("GetStrSQLExec",data);
    if (GetComunic.ResErr.NotError) {
        Comunic.AjaxJson.Client.Methods.CopyResErr(GetSQLExec.ResErr, GetComunic.response.ResErr);
    }
    else {
        GetSQLExec.ResErr.Mesaje = GetComunic.ResErr.Mesaje;
    }
    return (GetSQLExec);
}

Comunic.AjaxJson.Client.Methods.GetSesion = function (AppName)
{
                    
    GetSesion = new Comunic.Properties.TGetSesion();
    var data = {
        'AppName': AppName
    };
    GetComunic = Comunic.AjaxJson.Client.Methods.GetComunic("NumSesion",data);
    if (GetComunic.ResErr.NotError) {
        Comunic.AjaxJson.Client.Methods.CopyResErr(GetSesion.ResErr, GetComunic.response.ResErr);
        if (GetSesion.ResErr.NotError)
        {
            GetSesion.Num = GetComunic.response.Num;
        }
    }
    else {
        GetSesion.ResErr.Mesaje = GetComunic.ResErr.Mesaje;
    }
    return (GetSesion);
}

Comunic.AjaxJson.Client.Methods.GetPing = function () {
    GetPing = new Comunic.Properties.TGetPing();
    var data = {
        //'ValueStr': "",
        //'Valueint': 1
    };
    GetComunic = Comunic.AjaxJson.Client.Methods.GetComunic("GetPing", data);
    if (GetComunic.ResErr.NotError) {
        Comunic.AjaxJson.Client.Methods.CopyResErr(GetPing.ResErr, GetComunic.response.ResErr);
        if (GetPing.ResErr.NotError) {
            GetPing.Ping = GetComunic.response.Ping;
            GetPing.isValidDomain = GetComunic.response.isValidDomain;
        }
    }
    else {
        GetPing.ResErr.Mesaje = GetComunic.ResErr.Mesaje;
    }
    return (GetPing);
}


Comunic.AjaxJson.Client.Methods.GetDataDomain = function () {
    GetDataDomain = new SysCfg.Domain.TDataDomain();
    var data = {
        //'ValueStr': "",
        //'Valueint': 1
    };
    GetComunic = Comunic.AjaxJson.Client.Methods.GetComunic("GetDataDomain", data);
    if (GetComunic.ResErr.NotError) {
        Comunic.AjaxJson.Client.Methods.CopyResErr(GetDataDomain.ResErr, GetComunic.ResErr);
        if (GetDataDomain.ResErr.NotError) {
            GetDataDomain.DomainLogin = GetComunic.response.DomainLogin;
            GetDataDomain.DomainUser = GetComunic.response.DomainUser;
            GetDataDomain.DomainName = GetComunic.response.DomainName;
            GetDataDomain.DomainControllerName = GetComunic.response.DomainControllerName;
            GetDataDomain.AccountDomainSid = GetComunic.response.AccountDomainSid;
            GetDataDomain.DomainControllerNameIP = GetComunic.response.DomainControllerNameIP;
            GetDataDomain.DomainAuitorization = GetComunic.response.DomainAuitorization;
            GetDataDomain.DomainPW = GetComunic.response.DomainPW;
            GetDataDomain.isValidDomain = GetComunic.response.isValidDomain;
            GetDataDomain.DomainPath = GetComunic.response.DomainPath;
            

        }
    }
    else {
        GetDataDomain.ResErr.Mesaje = GetComunic.ResErr.Mesaje;
    }
    return (GetDataDomain);
}







Comunic.AjaxJson.Client.Methods.GetSDfile = function (FileType,FileOperationType,IDCode,ByteFile)
{
    GetSDfile = new Comunic.Properties.TGetSDfile();
    var data = {
        'FileType': FileType.value,
        'FileOperationType': FileOperationType.value,
        'IDCode': IDCode,
        'ByteFile': ByteFile
    };
    GetComunic = Comunic.AjaxJson.Client.Methods.GetComunic("GetSDfile",data);
    if (GetComunic.ResErr.NotError) {
        Comunic.AjaxJson.Client.Methods.CopyResErr(GetSDfile.ResErr, GetComunic.response.ResErr);
        if (GetSDfile.ResErr.NotError)
        {
            GetSDfile.IDCode = GetComunic.response.IDCode;
            GetSDfile.ByteFile = GetComunic.response.ByteFile;
        }    
        GetSDfile.FileType =  Comunic.Properties.TFileType.GetEnum(GetComunic.response.FileType);
        GetSDfile.FileOperationType = Comunic.Properties.TFileOperationType.GetEnum(GetComunic.response.FileOperationType);
        GetSDfile.IDCode = GetComunic.response.IDCode;
        GetSDfile.ByteFile = GetComunic.response.ByteFile;
    }
    else {
        GetSDfile.ResErr.Mesaje = GetComunic.ResErr.Mesaje;
    }
    return (GetSDfile);
}

//********************** GetCrypto *****************************
Comunic.AjaxJson.Client.Methods.GetCrypto = function (CryptoType,Hash,StrCrypto1,StrCrypto2)
{
            
    GetCrypto = new Comunic.Properties.TGetCrypto();
    var data = {
        'CryptoType': CryptoType.value,
        'Hash': Hash,
        'StrCrypto1': StrCrypto1,
        'StrCrypto2': StrCrypto2
    };
    GetComunic = Comunic.AjaxJson.Client.Methods.GetComunic("GetCrypto",data);
    if (GetComunic.ResErr.NotError) {
        Comunic.AjaxJson.Client.Methods.CopyResErr(GetCrypto.ResErr, GetComunic.response.ResErr);
        if (GetCrypto.ResErr.NotError)
        {
            GetCrypto.CryptoType = Comunic.Properties.TCryptoType(GetComunic.response.CryptoType);
            GetCrypto.Hash = GetComunic.response.Hash;
            GetCrypto.StrCrypto1 = GetComunic.response.StrCrypto1;
            GetCrypto.StrCrypto2 = GetComunic.response.StrCrypto2;
        }
    }
    else {
        GetCrypto.ResErr.Mesaje = GetComunic.ResErr.Mesaje;
    }

    return (GetCrypto);
    //caso de uso 
    //Comunic.Properties.TGetCrypto GetCrypto = new Comunic.Properties.TGetCrypto();
    //GetCrypto = Comunic.WCF.Client.Merhods.GetCrypto(Comunic.Properties.TCryptoType._Crypto1,"", "", "");            
}

//********************** GetHHH *****************************
Comunic.AjaxJson.Client.Methods.GetHHH = function (HHHType,Byte_Request)
{
    GetHHH = new Comunic.Properties.TGetHHH();
    var data = {
        'HHHType': HHHType.value,
        'Byte_Request': Byte_Request        
    };
    GetComunic = Comunic.AjaxJson.Client.Methods.GetComunic("GetHHH",data);
    if (GetComunic.ResErr.NotError) {
        Comunic.AjaxJson.Client.Methods.CopyResErr(GetHHH.ResErr, GetComunic.response.ResErr);
        if (GetHHH.ResErr.NotError)
        {
            GetHHH.HHHType = Comunic.Properties.THHHType.GetEnum(GetComunic.response.HHHType);
            GetCatalog.Byte_Response = GetComunic.response.Byte_Response;            
        }
    }
    else {
        GetHHH.ResErr.Mesaje = GetComunic.ResErr.Mesaje;
    }
    return (GetHHH);
    //caso de uso 
    //Comunic.Properties.TGetHHH GetHHH = new Comunic.Properties.TGetHHH();
    //GetHHH = Comunic.WCF.Client.Merhods.GetHHH(Comunic.Properties.THHHType._HHH1, "", "");            
}

//********************** GetCatalog *****************************
Comunic.AjaxJson.Client.Methods.GetCatalog = function (CatalogType,Byte_Request)
{
    GetCatalog = new Comunic.Properties.TGetCatalog();
    var data = {
        'CatalogType': CatalogType.value,
        'Byte_Request': Byte_Request
    };
    GetComunic = Comunic.AjaxJson.Client.Methods.GetComunic("GetCatalog",data);
    if (GetComunic.ResErr.NotError) {
        Comunic.AjaxJson.Client.Methods.CopyResErr(GetCatalog.ResErr, GetComunic.response.ResErr);
        if (GetCatalog.ResErr.NotError)
        {
            GetCatalog.CatalogType = Comunic.Properties.TCatalogType.GetEnum(GetComunic.response.CatalogType);
            GetCatalog.Byte_Response = GetComunic.response.Byte_Response;
        }
    }
    else {
        GetCatalog.ResErr.Mesaje = GetComunic.ResErr.Mesaje;
    }
    return (GetCatalog);
    //caso de uso 
    //Comunic.Properties.TGetCatalog GetCatalog = new Comunic.Properties.TGetCatalog();
    //GetCatalog = Comunic.WCF.Client.Merhods.GetCatalog(Comunic.Properties.TCatalogType._Catalog1, "", "");            
}

//********************** GetPersistence *****************************
Comunic.AjaxJson.Client.Methods.GetPersistence = function (PersistenceSource,PersistenceType,PersistenceCmd,Byte_Request)
{
    GetPersistence = new Comunic.Properties.TGetPersistence();
    var data = {
        'PersistenceSource': PersistenceSource.value,
        'PersistenceType': PersistenceType.value,
        'PersistenceCmd': PersistenceCmd.value,
        'Byte_Request': Byte_Request
    };
    GetComunic = Comunic.AjaxJson.Client.Methods.GetComunic("GetPersistence",data);
    if (GetComunic.ResErr.NotError) {
        Comunic.AjaxJson.Client.Methods.CopyResErr(GetPersistence.ResErr, GetComunic.response.ResErr);
        if (GetPersistence.ResErr.NotError)
        {
            GetPersistence.PersistenceSource = Comunic.Properties.TPersistenceSource.GetEnum(GetComunic.response.PersistenceSource);
            GetPersistence.PersistenceType = Comunic.Properties.TPersistenceType.GetEnum(GetComunic.response.PersistenceType);
            GetPersistence.PersistenceCmd = Comunic.Properties.TPersistenceCmd.GetEnum(GetComunic.response.PersistenceCmd);
            GetPersistence.Byte_Response = Comunic.PropertiesTGetPersistence.Byte_Response;
        }
    }
    else {
        GetPersistence.ResErr.Mesaje = GetComunic.ResErr.Mesaje;
    }
    return (GetPersistence);
    //caso de uso 
    //Comunic.Properties.TGetPersistence GetPersistence = new Comunic.Properties.TGetPersistence();
    //GetPersistence = Comunic.WCF.Client.Merhods.GetPersistence(Comunic.Properties.TPersistenceSource._Persistence1,Comunic.Properties.TPersistenceType._Persistence1,Comunic.Properties.TPersistenceCmd._None, "", "");            
}

//********************** GetInterno *****************************
Comunic.AjaxJson.Client.Methods.GetInterno = function (InternoType,Generate,Run,GetStructure)
{
    GetInterno = new Comunic.Properties.TGetInterno();
    var data = {
        'InternoType': InternoType.value,
        'Generate': Generate,
        'Run': Run,
        'GetStructure': GetStructure
    };
    GetComunic = Comunic.AjaxJson.Client.Methods.GetComunic("GetInterno",data);
    if (GetComunic.ResErr.NotError) {
        Comunic.AjaxJson.Client.Methods.CopyResErr(GetInterno.ResErr, GetComunic.response.ResErr);
        if (GetInterno.ResErr.NotError)
        {
            GetInterno.InternoType = Comunic.Properties.TInternoType.GetEnum(GetComunic.response.InternoType);
            GetInterno.StrResultInterno = GetComunic.response.StrResultInterno;
            GetInterno.Byte_Structure = GetComunic.response.Byte_Structure;

        }
    }
    else {
        GetInterno.ResErr.Mesaje = GetComunic.ResErr.Mesaje;
    }
    return (GetInterno);
    //caso de uso 
    //Comunic.Properties.TGetInterno GetInterno = new Comunic.Properties.TGetInterno();
    //GetInterno = Comunic.WCF.Client.Merhods.GetInterno(Comunic.Properties.TInternoType._Interno1, "", "");            
}

//********************** GetNotify *****************************
Comunic.AjaxJson.Client.Methods.GetNotify = function (NotifyType,Byte_Request)
{
    GetNotify = new Comunic.Properties.TGetNotify();
    var data = {
        'NotifyType': NotifyType.value,
        'Byte_Request': Byte_Request
    };
    GetComunic = Comunic.AjaxJson.Client.Methods.GetComunic("GetNotify",data);
    if (GetComunic.ResErr.NotError) {
        Comunic.AjaxJson.Client.Methods.CopyResErr(GetNotify.ResErr, GetComunic.response.ResErr);
        if (GetNotify.ResErr.NotError)
        {
            GetNotify.NotifyType = Comunic.Properties.TNotifyType.GetEnum(GetComunic.response.NotifyType);
            GetNotify.Byte_Response = GetComunic.response.Byte_Response;
        }
    }
    else {
        GetNotify.ResErr.Mesaje = GetComunic.ResErr.Mesaje;
    }
    return (GetNotify);
    //caso de uso 
    //Comunic.Properties.TGetNotify GetNotify = new Comunic.Properties.TGetNotify();
    //GetNotify = Comunic.WCF.Client.Merhods.GetNotify(Comunic.Properties.TNotifyType._Notify1, "", "");            
}

//********************** GetChat *****************************
Comunic.AjaxJson.Client.Methods.GetChat = function (ChatType,IDCMDBCI,MESSAGE,TITLE,IDSMCHATASSIGNED,IDSMCHATASSIGNEDMEMBER,IDSMCHAT)
{
    GetChat = new Comunic.Properties.TGetChat();
    var data = {
        'ChatType': ChatType.value,
        'IDCMDBCI': IDCMDBCI,
        'MESSAGE': MESSAGE,
        'TITLE': TITLE,
        'IDSMCHATASSIGNED': IDSMCHATASSIGNED,
        'IDSMCHATASSIGNEDMEMBER': IDSMCHATASSIGNEDMEMBER,
        'IDSMCHAT': IDSMCHAT
    };
    GetComunic = Comunic.AjaxJson.Client.Methods.GetComunic("GetChat",data);
    if (GetComunic.ResErr.NotError) {
        Comunic.AjaxJson.Client.Methods.CopyResErr(GetChat.ResErr, GetComunic.response.ResErr);
        if (GetChat.ResErr.NotError)
        {

            GetChat.ChatType = Comunic.Properties.TChatType.GetEnum(GetComunic.response.ChatType);
            switch (GetChat.ChatType)
            {
                case Comunic.Properties.TChatType.ASSIGNED_OWNER:
                    GetChat.IDSMCHATASSIGNED = GetComunic.response.IDSMCHATASSIGNED;
                    GetChat.IDSMCHAT = GetComunic.response.IDSMCHAT;
                    break;
                case Comunic.Properties.TChatType.SENDS_MESSAGE:
                    GetChat.IDSMCHAT = GetComunic.response.IDSMCHAT;
                    break;
                default:
                    break;
            }
        }

    }
    else {
        GetChat.ResErr.Mesaje = GetComunic.ResErr.Mesaje;
    }
    return (GetChat);
    //caso de uso 
    //Comunic.Properties.TGetChat GetChat = new Comunic.Properties.TGetChat();
    //GetChat = Comunic.WCF.Client.Merhods.GetChat(Comunic.Properties.TChatType._Chat1, "", "");            
}

//********************** GetLiveChange *****************************
Comunic.AjaxJson.Client.Methods.GetLiveChange = function (LiveChangeType, IDCMDBCI, IDSMCHAT, IDSMCHATASSIGNED, IDSMCHATASSIGNEDMEMBER, IDSDNOTIFY, IDRHREQUEST, IDRHMODULE,IDRHMODULE_ACTIVE, IDRHMODULE_SET, IDRHMODULE_DATE)
{
    GetLiveChange = new Comunic.Properties.TGetLiveChange();
    var data = {
        'LiveChangeType': LiveChangeType.value,
        'IDCMDBCI': IDCMDBCI,
        'IDSMCHAT': IDSMCHAT,
        'IDSMCHATASSIGNED': IDSMCHATASSIGNED,
        'IDSMCHATASSIGNEDMEMBER': IDSMCHATASSIGNEDMEMBER,
        'IDSDNOTIFY': IDSDNOTIFY,
        'IDRHREQUEST': IDRHREQUEST, 
        'IDRHMODULE': IDRHMODULE,   
        'IDRHMODULE_ACTIVE': IDRHMODULE_ACTIVE,
        'IDRHMODULE_SET' :  IDRHMODULE_SET,
        'IDRHMODULE_DATE': IDRHMODULE_DATE
    };
    GetComunic = Comunic.AjaxJson.Client.Methods.GetComunic("GetLiveChange",data);
    if (GetComunic.ResErr.NotError) {
        Comunic.AjaxJson.Client.Methods.CopyResErr(GetLiveChange.ResErr, GetComunic.response.ResErr);
        if (GetLiveChange.ResErr.NotError)
        {
            GetLiveChange.LiveChangeType = Comunic.Properties.TLiveChangeType.GetEnum(GetComunic.responseLiveChangeType);
            GetLiveChange.IDSMCHAT = GetComunic.response.IDSMCHAT;
            GetLiveChange.IDCMDBCI = GetComunic.response.IDCMDBCI;
            GetLiveChange.IDSMCHATASSIGNED = GetComunic.response.IDSMCHATASSIGNED;
            GetLiveChange.IDSMCHATASSIGNEDMEMBER = GetComunic.response.IDSMCHATASSIGNEDMEMBER;
            GetLiveChange.IDCMDBCI = GetComunic.response.IDCMDBCI;
            GetLiveChange.IDSDNOTIFY = GetComunic.response.IDSDNOTIFY;
            GetLiveChange.IDRHREQUEST = GetComunic.response.IDRHREQUEST;
            GetLiveChange.IDRHMODULE = GetComunic.response.IDRHMODULE;
            GetLiveChange.IDRHMODULE_ACTIVE = GetComunic.response.IDRHMODULE_ACTIVE;
            GetLiveChange.IDRHMODULE_SET = GetComunic.response.IDRHMODULE_SET;
            GetLiveChange.IDRHMODULE_DATE = GetComunic.response.IDRHMODULE_DATE;
            GetLiveChange.ByteLiveChange = GetComunic.response.ByteLiveChange;
        }
    }
    else {
        GetLiveChange.ResErr.Mesaje = GetComunic.ResErr.Mesaje;
    }

    return (GetLiveChange);
    //caso de uso 
    //Comunic.Properties.TGetLiveChange GetLiveChange = new Comunic.Properties.TGetLiveChange();
    //GetLiveChange = Comunic.WCF.Client.Merhods.GetLiveChange(Comunic.Properties.TLiveChangeType._LiveChange1, "", "");            
}

//********************** GetUser *****************************
Comunic.AjaxJson.Client.Methods.GetUser = function (CI_GENERICNAME, PASSWORD, AccountDomainSid) {
    var User = new SysCfg.App.TUser();
    var GetUser = new Comunic.Properties.TGetUser();
    var data = {
        'CI_GENERICNAME': CI_GENERICNAME,
        'PASSWORD': PASSWORD,
        'AccountDomainSid': AccountDomainSid
    };
    GetComunic = Comunic.AjaxJson.Client.Methods.GetComunic("GetUser", data);
    if (GetComunic.ResErr.NotError) {
        Comunic.AjaxJson.Client.Methods.CopyResErr(GetUser.ResErr, GetComunic.response.ResErr);
        User.ResErr = GetUser.ResErr;
        if (User.ResErr.NotError) {
            GetUser.ByteUser = GetComunic.response.ByteUser;
            var MemStream = new SysCfg.Stream.TMemoryStream();//Solo Se usa este en caso de que no sea tcp
            MemStream.Write(GetUser.ByteUser, 0, GetUser.ByteUser.length);
            MemStream.Position = 0;
            User.ByteToCMDBUSER(MemStream);
        }
    }
    else {
        GetUser.ResErr.Mesaje = GetComunic.ResErr.Mesaje;
    }

    return (User);
    //caso de uso 
    //Comunic.Properties.TGetUser GetUser = new Comunic.Properties.TGetUser();
    //GetUser = Comunic.WCF.Client.Merhods.GetUser( "", "");            
}

//********************** GetSDFunction *****************************

Comunic.AjaxJson.Client.Methods.GetSDFunction = function (GetSDFunction,inSDObject)
{
    GetSDFunction.SDObject = inSDObject;
    var MemStrm_Response = new SysCfg.Stream.TMemoryStream();
    //Cargar la funcion 
    var MemStrm_Request = new SysCfg.Stream.TMemoryStream();            
    MemStrm_Request.Position = 0;
    SysCfg.Stream.Methods.WriteStreamInt16(MemStrm_Request, GetSDFunction.SDFunction.value);
    UsrCfg.SD.Service.WriteResponseSDFunction(GetSDFunction, MemStrm_Request);//<<<<entrada
    //llenar la copia del web service
    WCFGetSDFunction = new Comunic.Properties.TGetSDFunction();
    
    //WCFGetSDFunction.Byte_Request = new Array[MemStrm_Request.Length()];
    WCFGetSDFunction.Byte_Request = MemStrm_Request.ToArray();
    var data = {
        'Byte_Request': WCFGetSDFunction.Byte_Request
    };
    GetComunic = Comunic.AjaxJson.Client.Methods.GetComunic("GetSDFunction",data);
    if (GetComunic.ResErr.NotError) {
        Comunic.AjaxJson.Client.Methods.CopyResErr(WCFGetSDFunction.ResErr, GetComunic.response.ResErr);
        if (WCFGetSDFunction.ResErr.NotError)
        {
            WCFGetSDFunction.Byte_Response = GetComunic.response.Byte_Response;            
            MemStrm_Response.Write(WCFGetSDFunction.Byte_Response, 0, WCFGetSDFunction.Byte_Response.length);
            MemStrm_Response.Position = 0;
            GetSDFunction.ResErr = SysCfg.Error.Methods.ByteToResErr(MemStrm_Response);
            UsrCfg.SD.Service.ReadResponseSDFunction(GetSDFunction,MemStrm_Response);//>>> salida
        }


    }
    else {
        WCFGetSDFunction.ResErr.Mesaje = GetComunic.ResErr.Mesaje;
    }
    return (GetSDFunction);
}

//var MemStream = new SysCfg.Stream.TMemoryStream();
//MemStream.Write(response, 0, response.length);

//no se usa ya que la que esta en el servidor es para coyotearlo al server datalick
//Comunic.AjaxJson.Client.Methods.GetSDFunction = function (Byte_Request)
//{
//    GetSDFunction = new Comunic.Properties.TGetSDFunction();
//    GetSDFunction.Byte_Request = Byte_Request;
//    var data = {
//        'Byte_Request': Byte_Request
//    };
//    GetComunic = Comunic.AjaxJson.Client.Methods.GetComunic("GetSDFunction",data);
//    if (GetComunic.ResErr.NotError) {
//        Comunic.AjaxJson.Client.Methods.CopyResErr(GetSDFunction.ResErr, GetComunic.response.ResErr);
//        if (GetSDFunction.ResErr.NotError)
//        {
//            GetSDFunction.Byte_Response = Comunic.PropertiesTGetSDFunction.Byte_Response;
//        }
//    }
//    else {
//        GetSDFunction.ResErr.Mesaje = GetComunic.ResErr.Mesaje;
//    }
//    return (GetSDFunction);
//}

