﻿//version 1  jsgogo 16/03/2018  obj.ContentRowBody limpio y pongo idgrupops

//bibliografia
//https://adminlte.io/themes/AdminLTE/index2.html

TMain = function (inObject, incallbackModalResult) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.Object = inObject;
    this.CallbackModalResult = incallbackModalResult;//CallbackModalResult(OutStr);            
    this.asideLeft_Visible = true;
    this.asideRight_Visible = true;
    this.resolucion = 3;

    //******************************************************
    this.Object_header = _this.Object.header;
    this.Object_container = _this.Object.main;
    this.Object_footer = _this.Object.footer;
    this.Object_asideleft = _this.Object.asideleft;
    this.Object_asideright = _this.Object.asideright;
    this.GetUsrCFg_Properties = null;
    this.ChatSupport = null;   

    //$(this.Object_container).addClass("col-lg-12");
    $(this.Object_container).addClass("col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12");


    this.Object.header.style.position = "fixed";
    this.Object.header.style.top = "0px";
    this.Object.header.style.zIndex = "9999999999999";
    this.Object.header.style.marginTop = "0px";
    this.Object.header.style.paddingRight = "25px";

    this.Object.ContenedorGeneral.style.marginTop = "50px";

    window.onresize = function () {
        _this.Object.ContenedorGeneral.style.marginTop = _this.Object.header.clientHeight.toString() + "px";
    }

    this.Object_asideleft.innerHTML = "";
    this.Object_container.innerHTML = "";

    UsrCfg.Properties.MenuProfiler = new Source.Menu.TMenuProfiler(this.Object_asideleft, this.Object_container);
    this.MenuObjetmain = new UsrCfg.Properties.MenuProfiler.NewPage(null);

    UsrCfg.Properties.MenuProfiler.OnSearch = function (sender, e) {
        //alert(e.txtSearch.Text);
        //$(_this.Object_container).html("");
        var DIvTemp = _this.MenuObjetmain.ObjectDataMain
        var _Category = new ItHelpCenter.SD.Shared.Category.TCategoryManger(
                DIvTemp,
                function (OutRes) {
                    //Parametros: ObjectHTML, CallBack_Function, (True o False / Buscar Categoria), Array (idCategory, PhraseCategory, PathCategody) / Si no se va a buscar categoria se debe enviar el array con los datos
                    var DCaseNew = new ItHelpCenter.SD.CaseUser.NewCase.TUfrSDCaseNew(
                      DIvTemp,
                       function (_DIvTemp, IDSDCASE) {
                           $(_DIvTemp).html("");
                           if (IDSDCASE > 0) {
                               var SMConsoleUserSwitch = new ItHelpCenter.SD.CaseUser.Atention.TSMConsoleUserSwitch(
                                 _DIvTemp,
                                 function (_this) {

                                 },
                                 IDSDCASE
                                 );
                           } else {
                               _this.load_Home(_this);
                           }
                         
                       },
                       OutRes.ID, OutRes.Detail, OutRes.Path, OutRes.PhraseIn)
                },
                ItHelpCenter.SD.Shared.Category.Mode.Basic_SeekSearch,
                false, false, e.txtSearch.Text);    
    }

    this.Load();
}

TMain.prototype.Load = function () {
    var _this = this.TParent();
    //*******************************************************
    this.Mythis = "TMain";
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1", "Chat online");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2", "Help online");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3", "© Copyright 2014 Leverit.com, Inc. Use of this site signifies your acceptance of Leverit Terms of Use and Privacy " +
                         "Policy. Leverit, the Leverit logos, and other Leverit marks are trademarks or registered trademarks of Leverit " +
                          "Software, Inc. in the U.S. and/or certain other countries");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Node1", "Home");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Node2", "Service desk");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Node2_1", "Create a new Case");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Node2_2", "Check my Cases");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Node3", "CMDB");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Node3_1", "CI's View");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Node4", "Tools");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Node4_1", "SQL");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "NodeX1", "Demo");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "NodeX1_1", "All Controls");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "NodeX1_2", "Category Basic");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "NodeX1_3", "OneDemo");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "NodeX1_4", "ModalDemo");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "NodeX1_5", "Menu Profiler");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "LabLenguaje", "Language");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "LabEstilos", "Styles");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "LabDevices", "Devices");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Node5", "Remote Help");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Node6", "Process Setup");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Node6_1", "Panel Services");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Node6_2", "Configuration");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Node6_2_1", "Urgency");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Node6_2_2", "Priority");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Node6_2_3", "Impact");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Node6_2_4", "Priority Matrix");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Node6_3", "PBI Graphics");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Node6_4", "Model");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Node6_4_1", "Edit");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Node6_5", "Votes");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Node7", "Case Manager");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Node7_1", "Create Complex Case");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Node7_2", "Console");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "labelMoreOpt", "More Search Options");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "LabelSearchhere", "Search here");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines4", "No media available to run this option see your administrator:");
    
    _this.Mainasideleft();
}

//************ Body ******************
TMain.prototype.Mainheader = function () {
    var _this = this.TParent();

    //creo la barra top
    var cabecera = new TVclTopBar(_this.Object_header, _this.Object_asideleft, _this.Object_asideright, _this.Object_container, _this.Object);

    //creo la imagen de logo
    var Imagen = new TVclImagen(null, "");
    Imagen.Src = "image/High/IT-Help-CenteHoriz_2.png";
    Imagen.This.style.maxHeight = "50px";
    cabecera.Logo = Imagen;
   


    //creo botton de salir
    var OutButton = new TVclImagen(null, "");
    OutButton.Text = "";
    OutButton.Src = "image/imgMono/24/power.png";
    OutButton.Cursor = "pointer";
    OutButton.onClick = function () {
        
        ItHelpCenter.PageManager.GoToPageType(ItHelpCenter.PageManager.TGoToPageType.CloseSession);
    }

    //creo item salir 
    var VclItemTopBar_Out = new TVclItemTopBar();
    VclItemTopBar_Out.Imagen = OutButton;
    VclItemTopBar_Out.Float = "right";


    //lo agrego a la parte derecha de la barra
    cabecera.AddItemRight(VclItemTopBar_Out);

    //creo la etiqueta para el usuario
    this.UserLabel = new TVcllabel(null, "UserLabel1", TlabelType.H0);
    _this.UserLabel.Text = "";
    _this.UserLabel.VCLType = TVCLType.BS;
    _this.UserLabel.This.classList.add("v-p6");
    _this.UserLabel.This.classList.add("v-p5");
    _this.UserLabel.This.classList.add("v-p4");
    _this.UserLabel.This.classList.add("v-p3");
    _this.UserLabel.This.classList.add("h-p2");
    _this.UserLabel.This.classList.add("h-p1");

    //creo la imagen de usuario
    var UserImage = new TVclImagen(null, "UserImg1");
    UserImage.Src = "image/imgMono/24/user.png";
    UserImage.Title = _this.UserLabel.Text;
    UserImage.Cursor = "pointer";

    //creo item salir 
    var VclItemTopBar_user = new TVclItemTopBar();
    VclItemTopBar_user.Imagen = UserImage;
    VclItemTopBar_user.Label = _this.UserLabel;
    VclItemTopBar_user.Float = "right";

    //lo agrego a la parte derecha de la barra
    cabecera.AddItemRight(VclItemTopBar_user);

    // creo chat rooms
    var ChatRooms = null;

    // creo label para mensajes
    this.ManssagesLabel = new TVcllabel(null, "", TlabelType.H0);
    _this.ManssagesLabel.Text = "0";
    _this.ManssagesLabel.VCLType = TVCLType.BS;

    //creo boton para mensajes
    var ManssagesImage = new TVclImagen(null, "");
    ManssagesImage.Text = "";
    ManssagesImage.Src = "image/imgMono/24/chat.png";
    ManssagesImage.Cursor = "pointer";
    ManssagesImage.onClick = function () {
        //$(_this.Object_container).html("");
        try {
            var ChatRooms = new TChatRooms(
            null,
            function (_this) {
                alert("fin Chat");
            },
            _this.GetUsrCFg_Properties.IDCMDBCI
            );

            ChatRooms.Modal.ShowModal();
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("main.js TMain.prototype.Mainheader", e);
            //location.reload();
        };
    }

    //creo item para chats
    var VclItemTopBar_chat = new TVclItemTopBar();
    VclItemTopBar_chat.Imagen = ManssagesImage;
    VclItemTopBar_chat.Label = _this.ManssagesLabel;
    VclItemTopBar_chat.Float = "right";

    //lo agrego a la parte derecha de la barra
    cabecera.AddItemRight(VclItemTopBar_chat);

    //creo notyfy list
    var NotifyList = null;

    //creo label para notify
    this.BellLabel = new TVcllabel(null, "", TlabelType.H0);
    _this.BellLabel.Text = "0";
    _this.BellLabel.VCLType = TVCLType.BS;

    //creo boton para notify
    var BellImage = new TVclImagen(null, "");
    BellImage.Text = "";
    BellImage.Src = "image/imgMono/24/news.png";
    BellImage.Cursor = "pointer";
    BellImage.onClick = function () {
        //$(_this.Object_container).html("");
        //var MenuObjetNotify = UsrCfg.Properties.MenuProfiler.NewPage(_this.MenuObjetmain);
                           
        //MenuObjetNotify.OnBeforeBackPage = function (sender, e) {
        //    alert("before Main");
        //};

  

        //var frSMConsolManager = new ItHelpCenter.TUfrSMConsoleManager(
        // MenuObjetConsole,
        //MenuObjetConsole.ObjectDataMain

        try {
            var NotifyList = new ItHelpCenter.TNotifyList(null);
            NotifyList.Modal.ShowModal();
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("main.js TMain.prototype.Mainheader", e);
            //location.reload();
        };
    }

    //creo item para notify
    var VclItemTopBar_notify = new TVclItemTopBar();
    VclItemTopBar_notify.Imagen = BellImage;
    VclItemTopBar_notify.Label = _this.BellLabel;
    VclItemTopBar_notify.Float = "right";

    //lo agrego a la parte derecha de la barra
    cabecera.AddItemRight(VclItemTopBar_notify);
    var parameters = {
        NumSesion: SysCfg.App.Methods.Cfg_WindowsTask.NumSesion
    };
    
    _this.UserLabel.Text = Persistence.Profiler.UserFunctions.User.CI_GENERICNAME;

    $.ajax({
        type: "POST",
        async: false,
        url: SysCfg.App.Properties.xRaiz + 'Service/main.svc/TimerElapsed',
        data: JSON.stringify(parameters), //'{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            if (response.ResErr.NotError) {
                for (var i = 0; i < response.UrlList.length; i++) {

                    if (response.UrlList[i].indexOf("ttp:") != -1) {
                        window.open(response.UrlList[i], "_blank");
                    }
                    else {
                        alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines4") + response.UrlList[i].length);                        
                    }

                }
                _this.ManssagesLabel.Text = response.ChatCounts;
                _this.BellLabel.Text = response.NotifyCounts;
                //_this.UserLabel.Text = response.User;
            }
            else {
                SysCfg.App.Methods.ShowMessage(response.ResErr.Mesaje);
                ItHelpCenter.PageManager.GoToPageType(ItHelpCenter.PageManager.TGoToPageType.CloseSession);
                
            }
            //setInterval(function () { setTimer() }, 3000);
        },
        error: function (error) {
            ItHelpCenter.PageManager.GoToPageType(ItHelpCenter.PageManager.TGoToPageType.Reload);
        }
    });


    var parameters = {
        NumSesion: SysCfg.App.Methods.Cfg_WindowsTask.NumSesion
    };


    if (UsrCfg.Version.EnableMainTimer) {
        var myVar = setInterval(function () { setTimer() }, 5000);
        function setTimer() {
            $.ajax({
                type: "POST",
                async: false,
                url: SysCfg.App.Properties.xRaiz + 'Service/main.svc/TimerElapsed',
                data: JSON.stringify(parameters), //'{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    if (response.ResErr.NotError) {
                        _this.ManssagesLabel.Text = response.ChatCounts;
                        _this.BellLabel.Text = response.NotifyCounts;

                        for (var i = 0; i < response.UrlList.length; i++) {

                            if (response.UrlList[i].indexOf("ttp:") != -1) {
                                window.open(response.UrlList[i], "_blank");
                            }
                            else {
                                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines4") + response.UrlList[i].length);
                            }

                        }

                        if (NotifyList != null)
                            NotifyList.CreateNotifyList();
                        if (ChatRooms != null)
                            ChatRooms.CreateChatRooms();
                        if (_this.ChatSupport != null) {
                            if (_this.ChatSupport.IsCreateRoom)
                                _this.ChatSupport.LaunchChat();
                        }
                    }
                    else {
                        //SysCfg.App.Methods.ShowMessage(response.ResErr.Mesaje);
                        ItHelpCenter.PageManager.GoToPageType(ItHelpCenter.PageManager.TGoToPageType.CloseSession);
                        //ItHelpCenter.PageManager.GoToPageType(ItHelpCenter.PageManager.TGoToPageType.Login);
                        
                    }

                },
                error: function (error) {
                    ItHelpCenter.PageManager.GoToPageType(ItHelpCenter.PageManager.TGoToPageType.Reload);
                }
            });
        }
    }
   /*
    Persistence.Profiler.Start(function (sender, e) {
        


        var Count=0;
        for (var i = 0; i <  Persistence.Profiler.UserFunctions.ChatProfiler.SMCHATASSIGNEDList.length; i++)
        {
            if (Persistence.Profiler.UserFunctions.ChatProfiler.SMCHATASSIGNEDList[i].ASSIGNED_ENABLE) Count++;                 
        }
        _this.ManssagesLabel.Text = Count.toString();//this.txtNumeroChats.Text

        //chatWindow.UpdateChatList();
        Count = 0;
        for (var i = 0; i < Persistence.Profiler.UserFunctions.NotifyProfiler.SDNOTIFYList.length; i++)
        {
            if (!Persistence.Profiler.UserFunctions.NotifyProfiler.SDNOTIFYList[i].ISSENDCONSOLE) Count++;
        }           
        _this.BellLabel.Text = Count.toString(); //this.txtNumeroNotificaciones1.Text
        //notifyWindow.UpdateNotifyList();

        for (var i = 0; i < Persistence.Profiler.UserFunctions.RHREQUESTList.length; i++)
        {
            if (Persistence.Profiler.UserFunctions.RHREQUESTList[i].REQUEST_STATUS == Persistence.RemoteHelp.Properties.TREQUEST_STATUS._CONFIGURE)
            {
                    
                for (var q = 0; q < Persistence.Profiler.UserFunctions.RHREQUESTList[i].RHREQUEST_CPUCRList.length; q++)
                {
                        
                    var RHREQUESTCR = new Persistence.RemoteHelp.Properties.TRHREQUESTCR();                        
                    Persistence.RemoteHelp.Methods.StrtoRHREQUESTCR(Persistence.Profiler.UserFunctions.RHREQUESTList[i].RHREQUEST_CPUCRList[q].HOSTLIST, RHREQUESTCR);
                    try
                    {
                        if (RHREQUESTCR.GENPATH.Contains("ttp:"))
                        {
                            window.open(RHREQUESTCR.GENPATH + RHREQUESTCR.GENPARAM, "_blank");
                            //Uri MyUry = new Uri(RHREQUESTCR.GENPATH + RHREQUESTCR.GENPARAM);
                            //System.Windows.Browser.HtmlPage.Window.Navigate(MyUry, "blank");

                        }
                        else
                        {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines4") + RHREQUESTCR.GENPATH.Length.toString());
                            //SysCfg.App.Methods.ShowMessage("No media available to run this option see your administrator:" + RHREQUESTCR.GENPATH.Length.toString());
                            //no se encuentran los medios disponibles para ejecutar esta opción consulte a su administrador 
                        }
                    }
                    catch (Exception)
                    {
                        ItHelpCenter.PageManager.GoToPageType(ItHelpCenter.PageManager.TGoToPageType.Reload);
                    }

                    Persistence.Profiler.UserFunctions.RHREQUESTList[i].RHREQUEST_CPUCRList[q].REQUEST_CPUCR_STATUS = Persistence.RemoteHelp.Properties.TREQUEST_CPUCR_STATUS._SUCCESFULLY;
                }
                Persistence.Profiler.UserFunctions.RHREQUESTList[i].REQUEST_STATUS = Persistence.RemoteHelp.Properties.TREQUEST_STATUS._CLOSE;
                Persistence.Profiler.GetRHREQUEST(Comunic.Properties.TRHREQUESTType.UPDATE, Persistence.Profiler.UserFunctions.RHREQUESTList[i]);
            }
        }
        //for (int i = 0; i < Persistence.Profiler.UserFunctions.RHMODULE_ACTIVEList.Count; i++)
        //{
        //    if (Persistence.Profiler.UserFunctions.RHMODULE_ACTIVEList[i].Enable) Count++;
        //}



   
    });
    */
    //alert(Persistence.Catalog.Methods.SDTYPEUSER_ListSetID(Persistence.Profiler.CatalogProfiler.SDTYPEUSERList, 4).TYPEUSERNAME);
}





TMain.prototype.Maincontainer = function () {
    var _this = this.TParent();
    //_this.Object_container
}


TMain.prototype.Mainfooter = function () {
    var _this = this.TParent();
    _this.Object_footer.style.paddinBottom = "50px";

    var div1 = new TVclStackPanel(_this.Object_footer, "Footer_div1" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
    var div1Cont = div1.Column[0].This;
    var div2 = new TVclStackPanel(_this.Object_footer, "Footer_div2" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
    var div2Cont = div2.Column[0].This;
    var div3 = new TVclStackPanel(_this.Object_footer, "Footer_div3" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
    var div3Cont = div3.Column[0].This;
    var div3_1 = new TVclStackPanel(_this.Object_footer, "Footer_div3_1" + _this.ID, 4, [[0, 12, 12, 0], [0, 12, 12, 0], [1, 3, 7, 1], [1, 3, 7, 1], [1, 3, 7, 1]]);
    var div3_1_1Cont = div3_1.Column[1].This; //logo
    var div3_1_2Cont = div3_1.Column[2].This; //info

    div1.Row.MarginTop = "10px";
    div3_1.Row.MarginTop = "10px";
    div3_1.Row.PaddingLeft = "40px";
    div3_1.Row.PaddingRight = "40px";
    div3_1.Row.PaddingBottom = "20px";

    //Title
    var label1 = new TVcllabel(div1Cont, "", TlabelType.H0);
    label1.Text = "IT Help Center ";
    label1.VCLType = TVCLType.BS;
    $(label1.This).css("font-size", "20px");

    var labelV = new TVcllabel(div1Cont, "", TlabelType.H0);
    labelV.Text = _this.GetUsrCFg_Properties._UsrCfgAppVersion;
    labelV.VCLType = TVCLType.BS;
    $(labelV.This).css("font-size", "14px");
    $(labelV.This).css("margin-left", "5px");

    $(div1Cont).css("text-align", "center");

    //Div para crear las lineas del footer
    var ObjectLines = div2Cont;

    //Parte de derechos
    //logo leverit
    var Logo = new TVclImagen(div3_1_1Cont, "");
    Logo.onClick = function () { }
    Logo.Src = "image/High/leverit.png";
    Logo.Title = "";
    $(Logo.This).css("width", "120px");
    $(Logo.This).css("height", "auto");
    $(Logo.This).css("float", "right");
    $(div3_1_1Cont).css("align-items", "center");
    $(div3_1_1Cont).css("display", "flex");
    $(div3_1_1Cont).css("justify-content", "center");


    var labelInfo = new TVcllabel(div3_1_2Cont, "", TlabelType.H0);
    labelInfo.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3");
    labelInfo.VCLType = TVCLType.BS;
    $(div3_1_2Cont).css("text-align", "justify");
    $(labelInfo.This).css("font-weight", "normal");

    _this.AddLinefooter(ObjectLines, UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1"), "image/imgMono/24/chat.png").onClick = function () {
       // $(_this.Object_container).html("");
        try {
            _this.ChatSupport = new TChatSupport(
            null,
            function (_this) {
                alert("fin Chat support");
            },
            _this.GetUsrCFg_Properties.IDCMDBCI
            );

            _this.ChatSupport.Modal.ShowModal();

        } catch (e) {
            SysCfg.Log.Methods.WriteLog("main.js TMain.prototype.Mainheader", e);
            //location.reload();
        };

    }
    _this.AddLinefooter(ObjectLines, UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2"), "image/imgMono/24/information.png").onClick = function () {
        window.open("http://ithelpcenter.lever-it.com");
    }
}

TMain.prototype.AddLinefooter = function (ObjetDiv, Name, image) {



    var _this = this.TParent();
    StackPanelDef = new Array(1);
    StackPanelDef[0] = new Array(2);
    var StackPanel = VclDivitions(ObjetDiv, "", StackPanelDef);
    BootStrapCreateRow(StackPanel[0].This);
    BootStrapCreateColumns(StackPanel[0].Child, [[3, 9], [3, 9], [3, 9], [3, 9], [3, 9]]);

    var Logo = new TVclImagen(StackPanel[0].Child[0].This, "");
    Logo.onClick = function () { }
    Logo.Src = image;
    Logo.Title = "";
    Logo.Cursor = "pointer";
    //$(DivLogo).addClass("LogoMain");
    var label1 = new TVcllabel(StackPanel[0].Child[1].This, "", TlabelType.H0);
    label1.Text = Name;
    label1.VCLType = TVCLType.BS;
    $(label1.This).css("font-weight", "normal");

    //$(StackPanel[0].This).css("border", "1px solid green");
    $(StackPanel[0].Child[0].This).css("width", "auto");
    $(StackPanel[0].Child[1].This).css("width", "auto");

    $(StackPanel[0].This).css("align-items", "center");
    $(StackPanel[0].This).css("display", "flex");
    $(StackPanel[0].This).css("justify-content", "center");

    $(StackPanel[0].This).css("padding-top", "10px");

    return Logo;
}

var NodeClick = function (Node) {
    alert(Node.Tag);
}

TMain.prototype.Mainasideleft = function () {
    var _this = this.TParent();
    //_this.Object_asideleft
    var parameters = {
        SESSION_HASH: _this.SESSION_HASH
    };
    $.ajax({
        type: "POST",
        url: SysCfg.App.Properties.xRaiz + 'Service/main.svc/GetUsrCFg_Properties',
        data: JSON.stringify(parameters),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            
            _this.GetUsrCFg_Properties = response;
            var VclTree = new TVclTree(_this.MenuObjetmain.ObjectData, "");

            $(VclTree.This).addClass("NavUl");
            if (_this.GetUsrCFg_Properties.ResErr.NotError) { 
                VclTree.AddTreeNodeRoot(UsrCfg.Traslate.GetLangText(_this.Mythis, "Node1"), "image/16/Home.png").onClick = function () {
                    _this.load_Home(_this); //actualizacion jaimes 18/05/08
                     
                }
                //NodeDinamicList.Add(new TNodeDinamic { Name = "Node2", Path = Node2, image = @"image/32/Help-desk.png", src = "" });
                if (_this.GetUsrCFg_Properties.UserATRole.isUserIncident) {

                    VclTree.AddTreeNodeRoot(UsrCfg.Traslate.GetLangText(_this.Mythis, "Node2"), "image/16/Help.png");                    
                    VclTree.AddTreeNode(UsrCfg.Traslate.GetLangText(_this.Mythis, "Node2"), UsrCfg.Traslate.GetLangText(_this.Mythis, "Node2_1"), "image/16/Create-ticket.png").onClick = function () {
                        //alert("SD/ACaseConsoleShared/UfrMDCategoryDetail.aspx");
                        //$(_this.Object_container).html("");
                        //_this.Object_container.style.minHeight = _this.Object_asideleft.clientHeight + "px";
                        //Parameters: (6) = 1.HTML Container, 2.Function Back, 3.Modo busqueda, 4.Ver boton cambio de modo (true - False), 5.Ver mensaje resultado, 6.Frase predeterminada    
                        var DIvTemp = _this.MenuObjetmain.ObjectDataMain
                        var _Category = new ItHelpCenter.SD.Shared.Category.TCategoryManger(
                           DIvTemp,
                            function (OutRes) {
                                //Parametros: ObjectHTML, CallBack_Function, (True o False / Buscar Categoria), Array (idCategory, PhraseCategory, PathCategody) / Si no se va a buscar categoria se debe enviar el array con los datos
                                 
                                var DCaseNew = new ItHelpCenter.SD.CaseUser.NewCase.TUfrSDCaseNew(
                                   DIvTemp,
                                   function (_DIvTemp, IDSDCASE) {
                                       $(_DIvTemp).html("");
                                       if (IDSDCASE > 0) {
                                           var SMConsoleUserSwitch = new ItHelpCenter.SD.CaseUser.Atention.TSMConsoleUserSwitch(
                                          _DIvTemp,
                                          function (_this) {

                                          },
                                          IDSDCASE
                                          );
                                       } else {
                                           _this.load_Home(_this);
                                       }                                      
                                      
                                   },
                                   OutRes.ID, OutRes.Detail, OutRes.Path, OutRes.PhraseIn)
                            },
                            ItHelpCenter.SD.Shared.Category.Mode.SeekSearch,
                            true, false, "");

                    };


                    VclTree.AddTreeNode(UsrCfg.Traslate.GetLangText(_this.Mythis, "Node2"), UsrCfg.Traslate.GetLangText(_this.Mythis, "Node2_2"), "image/16/My-tickets.png").onClick = function () {
                        //alert("SD/CaseUser/Atention/frAtentionCases.aspx");                        
                       
                        //$(_this.Object_container).html("");
                        var DIvTemp = _this.MenuObjetmain.ObjectDataMain
                        DIvTemp.innerHTML = "";
                        var idcase = 0;
                        var SMConsoleUserSwitch = new ItHelpCenter.SD.CaseUser.Atention.TSMConsoleUserSwitch(
                        DIvTemp,
                        function (_this) {
                            //window.parent.resizeiFrame(); //Al terminar de cargar todos los elementosllamar el resize para el scroll
                        },//null{del retorno arriba},    
                        idcase
                        );
                    };
                };
                if (_this.GetUsrCFg_Properties.UserATRole.isServiceDesk) {
                    //atencion de reportes.
                };

                if (_this.GetUsrCFg_Properties.UserATRole.isManager) {
                    VclTree.AddTreeNodeRoot(UsrCfg.Traslate.GetLangText(_this.Mythis, "Node7"), "image/imgmono/16/company-building.png");
                    VclTree.AddTreeNode(UsrCfg.Traslate.GetLangText(_this.Mythis, "Node7"), UsrCfg.Traslate.GetLangText(_this.Mythis, "Node7_2"), "image/16/Item-configuration1.png").onClick = function () {
                        //$(_this.Object_container).html("");
                        try {

                            var MenuObjetConsole = UsrCfg.Properties.MenuProfiler.NewPage(_this.MenuObjetmain);
                           
                            //MenuObjetConsole.OnBeforeBackPage = function (sender, e) {
                            //    alert("before Main");
                            //};

                            //MenuObjetConsole.OnAfterBackPage = function (sender, e) {
                            //    alert("afete Main");
                            //};

                            var frSMConsoleManager = new ItHelpCenter.TUfrSMConsoleManager(
                             MenuObjetConsole,
                            MenuObjetConsole.ObjectDataMain, _this.GetUsrCFg_Properties,
                            function () {
                            });
                        }
                        catch (e) {
                            //location.reload();
                        };
                    };
                };


                if (_this.GetUsrCFg_Properties.UserATRole.Atis_CIView) {
                    VclTree.AddTreeNodeRoot(UsrCfg.Traslate.GetLangText(_this.Mythis, "Node3"), "image/imgmono/16/company-building.png");
                    //NodeDinamicList.Add(new TNodeDinamic { Name = "Node3", Path = Node3, image = @"image/24/Place-an-order.png", src = @"" });                    
                    VclTree.AddTreeNode(UsrCfg.Traslate.GetLangText(_this.Mythis, "Node3"), UsrCfg.Traslate.GetLangText(_this.Mythis, "Node3_1"), "image/16/Item-configuration1.png").onClick = function () {
                        //alert("CMDB/CIEditor/frCIEditor.aspx");
                        //$(_this.Object_container).html("");
                        var DIvTemp = _this.MenuObjetmain.ObjectDataMain
                        DIvTemp.innerHTML = "";
                        try {
                            var frCIEditor = new ItHelpCenter.CIEditor.TfrCiEditor(
                            DIvTemp,
                            "1",
                            function (_this) {
                                alert("fin frCIEditor");
                            }
                            );
                        } catch (e) {
                            SysCfg.Log.Methods.WriteLog("main.js TMain.prototype.Mainasideleft", e);
                            //location.reload();
                        };
                    };
                };
                
                if (_this.GetUsrCFg_Properties.UserATRole.Atis_SQL) {
                    //NodeDinamicList.Add(new TNodeDinamic { Name = "Node4", Path = Node4, image = @"image/24/Application.png", src = @"" });
                    VclTree.AddTreeNodeRoot(UsrCfg.Traslate.GetLangText(_this.Mythis, "Node4"), "image/imgmono/16/wrench.png");
                    VclTree.AddTreeNode(UsrCfg.Traslate.GetLangText(_this.Mythis, "Node4"), UsrCfg.Traslate.GetLangText(_this.Mythis, "Node4_1"), "image/16/Search-database.png").onClick = function () {
                        //alert("Tools/SQL/frUSQL.aspx");
                        //$(_this.Object_container).html("");
                        //var DIvTemp = _this.MenuObjetmain.ObjectDataMain;
                        //$(DIvTemp).html("");
                        try {
                            var MenuObjetSQL = UsrCfg.Properties.MenuProfiler.NewPage(_this.MenuObjetmain);

                            var SQL = new TSQL(
                            MenuObjetSQL.ObjectDataMain,
                            function (_this) {
                                alert("fin sql");
                            }
                            );
                        } catch (e) {
                            SysCfg.Log.Methods.WriteLog("main.js TMain.prototype.Mainasideleft", e);
                            //location.reload();
                        };

                    };
                };

                
                if ((!UsrCfg.Version.IsProduction) || (UsrCfg.Version.IsDemo)) {
                    VclTree.AddTreeNodeRoot(UsrCfg.Traslate.GetLangText(_this.Mythis, "NodeX1"), "image/16/Handtool.png");
                    VclTree.AddTreeNode(UsrCfg.Traslate.GetLangText(_this.Mythis, "NodeX1"), UsrCfg.Traslate.GetLangText(_this.Mythis, "NodeX1_1"), "image/16/star-full.png").onClick = function () {
                        //alert("Demo/VCL/AllControls/AllControls.aspx");

                        _this.MenuObjetmain.OnAfterBackPage = function (Param1, Param2) {
                            //alert("despues de atras ya en main(el _this esta en main)");
                        }

                        var MenuObjetMenuProfiler = UsrCfg.Properties.MenuProfiler.NewPage(_this.MenuObjetmain);

                        MenuObjetMenuProfiler.ObjectDataMain.style.backgroundColor = "white";

                        try {
                            var AllControls = new ItHelpCenter.Demo.TAllControls(
                                MenuObjetMenuProfiler,
                            MenuObjetMenuProfiler.ObjectDataMain,
                            function (_this) {
                                alert("fin AllControls");
                            }
                            );
                        } catch (e) {
                            SysCfg.Log.Methods.WriteLog("main.js TMain.prototype.Mainasideleft", e);
                            //location.reload();
                        };
                        //var DIvTemp = _this.MenuObjetmain.ObjectDataMain;
                        //$(DIvTemp).html("");
                        //try {
                        //    var AllControls = new ItHelpCenter.Demo.TAllControls(
                        //    DIvTemp,
                        //    function (_this) {
                        //        alert("fin AllControls");
                        //    }
                        //    );
                        //} catch (e) {
                        //    SysCfg.Log.Methods.WriteLog("main.js TMain.prototype.Mainasideleft", e);
                        //    //location.reload();
                        //};
                    };
                    VclTree.AddTreeNode(UsrCfg.Traslate.GetLangText(_this.Mythis, "NodeX1"), UsrCfg.Traslate.GetLangText(_this.Mythis, "NodeX1_2"), "image/16/star-full.png").onClick = function () {
                        //$(_this.Object_container).html("");
                        var DIvTemp = _this.MenuObjetmain.ObjectDataMain;
                        $(DIvTemp).html("");
                        var IDSDTYPEUSER = 4;
                        try {
                            var CategoryBasic = new ItHelpCenter.SD.Shared.Category.TCategoryBasic(
                            DIvTemp,
                            function (_this) {
                                alert(
                                _this.IdCategory + " | " +
                                _this.IdCategoryDetail + " | " +
                                _this.CategoryName + " | " +
                                _this.CategoryDescription + " | " +
                                _this.Path);
                            },
                            IDSDTYPEUSER
                            );
                        } catch (e) {
                            SysCfg.Log.Methods.WriteLog("main.js TMain.prototype.Mainasideleft", e);
                            //location.reload();
                        };
                        //alert("SD/Shared/Category/Basic/frCategoryBasic.aspx");
                    };

                    VclTree.AddTreeNode(UsrCfg.Traslate.GetLangText(_this.Mythis, "NodeX1"), UsrCfg.Traslate.GetLangText(_this.Mythis, "NodeX1_3"), "image/16/star-full.png").onClick = function () {
                        //$(_this.Object_container).html("");
                        var DIvTemp = _this.MenuObjetmain.ObjectDataMain;
                        $(DIvTemp).html("");
                        var PageOne = new Source.Page.Example.TPageOne(DIvTemp, 1, function (sender) {
                            if (sender.ModalResult == Source.Page.Properties.TModalResult.Create) {
                                alert("se cargo");
                            }
                            else {
                                alert(sender.ModalResult.name);
                            }

                        });
                        PageOne.Initialize();
                    };


                    VclTree.AddTreeNode(UsrCfg.Traslate.GetLangText(_this.Mythis, "NodeX1"), UsrCfg.Traslate.GetLangText(_this.Mythis, "NodeX1_4"), "image/16/star-full.png").onClick = function () {
                        //$(_this.Object_container).html("");

                        var DIvTemp = _this.MenuObjetmain.ObjectDataMain;
                        $(DIvTemp).html("");

                        var PageModal = new Source.Page.Example.TPageModal(DIvTemp, 1);
                        PageModal.Initialize();
                        PageModal.Show();
                        PageModal.OnBeforeDestroy = function (sender) {
                            alert("se destruyo el formulario");
                        }

                        PageModal.OnClosed = function (sender) {
                            alert("se cerro el formulario");
                            PageModal.Destroy();
                        }
                    };


                    VclTree.AddTreeNode(UsrCfg.Traslate.GetLangText(_this.Mythis, "NodeX1"), UsrCfg.Traslate.GetLangText(_this.Mythis, "NodeX1_5"), "image/16/star-full.png").onClick = function () {
                        //$(_this.Object_container).html("");
                        _this.MenuObjetmain.OnAfterBackPage = function () {
                            alert("despues de ir atras a main");
                        }

                        var MenuObjetMenuProfiler = UsrCfg.Properties.MenuProfiler.NewPage(_this.MenuObjetmain);

                        var frSMConsoleManager = new Source.Page.Example.TPageMenu(
                         MenuObjetMenuProfiler,
                        MenuObjetMenuProfiler.ObjectDataMain, "",
                        function () {
                        });


                    };
                };

                //if (_this.GetUsrCFg_Properties.UserATRole.isUserIncident) {
                //    VclTree.AddTreeNodeRoot(UsrCfg.Traslate.GetLangText(_this.Mythis, "Node5"), "image/16/monitor.png").onClick = function () {
                //        try {
                //            $(_this.Object_container).html("");
                //            $(_this.Object_container).css("padding-left", "15px");
                //            var topbar = new TBarControls(_this.Object_container, "IDRemoteHelp");
                //            topbar._ToolBar_Direction = false;
                //            var toolCont1 = new TBarControls.TToolBar(topbar, "1");
                //            HelpRemote = new ItHelpCenter.TUfrRemoteHelp(
                //            toolCont1.ToolBox,
                //           function () {
                //           }, topbar.ControlBody);
                //        } catch (e) {
                //        }
                       
                //    }
                //}


                if (_this.GetUsrCFg_Properties.UserATRole.Atis_PanelService) {
                    VclTree.AddTreeNodeRoot(UsrCfg.Traslate.GetLangText(_this.Mythis, "Node6"), "image/16/Item-configuration1.png");
                    VclTree.AddTreeNode(UsrCfg.Traslate.GetLangText(_this.Mythis, "Node6"), UsrCfg.Traslate.GetLangText(_this.Mythis, "Node6_1"), "image/16/star-full.png").onClick = function () {
                        //$(_this.Object_container).html("");
                        var DIvTemp = _this.MenuObjetmain.ObjectDataMain;
                        $(DIvTemp).html("");
                        try {
                            var PanelServiceManager = new ItHelpCenter.TPanelServicesManager.PanelServicesMain(DIvTemp, "", function (_this) {
                                alert("Callback PanelServiceManager");
                            });
                        } catch (e) {
                            SysCfg.Log.Methods.WriteLog("main.js TMain.prototype.Mainasideleft", e);
                            //location.reload();
                        };
                    };
                }
                if (_this.GetUsrCFg_Properties.UserATRole.Atis_Urgency) {
                    VclTree.AddTreeNode(UsrCfg.Traslate.GetLangText(_this.Mythis, "Node6") + "\\" + UsrCfg.Traslate.GetLangText(_this.Mythis, "Node6_2") + "\\" + UsrCfg.Traslate.GetLangText(_this.Mythis, "Node6_2_1"), UsrCfg.Traslate.GetLangText(_this.Mythis, "Node6_2_1"), "image/16/star-full.png").onClick = function () {
                        //$(_this.Object_container).html("");
                        var DIvTemp = _this.MenuObjetmain.ObjectDataMain;
                        $(DIvTemp).html("");
                        try {
                            var Urgency = new ItHelpCenter.MD.Configuration.Urgency.TfrMDUrgency(DIvTemp, function (_this) {

                            });
                        } catch (e) {
                            SysCfg.Log.Methods.WriteLog("main.js TMain.prototype.Mainasideleft", e);
                            //location.reload();
                        };
                    };
               }
                if (_this.GetUsrCFg_Properties.UserATRole.Atis_Priority) {

                    VclTree.AddTreeNode(UsrCfg.Traslate.GetLangText(_this.Mythis, "Node6") + "\\" + UsrCfg.Traslate.GetLangText(_this.Mythis, "Node6_2") + "\\" + UsrCfg.Traslate.GetLangText(_this.Mythis, "Node6_2_2"), UsrCfg.Traslate.GetLangText(_this.Mythis, "Node6_2_2"), "image/16/star-full.png").onClick = function () {
                        //$(_this.Object_container).html("");
                        var DIvTemp = _this.MenuObjetmain.ObjectDataMain;
                        $(DIvTemp).html("");
                        try {
                            var Priority = new ItHelpCenter.MD.Configuration.Priority.TfrMDPriority(DIvTemp, function (_this) {

                            });

                        } catch (e) {
                            SysCfg.Log.Methods.WriteLog("main.js TMain.prototype.Mainasideleft", e);
                            //location.reload();
                        };
                    };
               }
                if (_this.GetUsrCFg_Properties.UserATRole.Atis_Impact) {

                    VclTree.AddTreeNode(UsrCfg.Traslate.GetLangText(_this.Mythis, "Node6") + "\\" + UsrCfg.Traslate.GetLangText(_this.Mythis, "Node6_2") + "\\" + UsrCfg.Traslate.GetLangText(_this.Mythis, "Node6_2_3"), UsrCfg.Traslate.GetLangText(_this.Mythis, "Node6_2_3"), "image/16/star-full.png").onClick = function () {
                        //$(_this.Object_container).html("");
                        var DIvTemp = _this.MenuObjetmain.ObjectDataMain;
                        $(DIvTemp).html("");
                        try {
                            //impact
                            var Impact = new ItHelpCenter.MD.Configuration.Impact.TfrMDImpact(DIvTemp, function (_this) {

                            });
                        } catch (e) {
                            SysCfg.Log.Methods.WriteLog("main.js TMain.prototype.Mainasideleft", e);
                            //location.reload();
                        };
                    };
               }
                if (_this.GetUsrCFg_Properties.UserATRole.Atis_PriorityMatrix) {

                    VclTree.AddTreeNode(UsrCfg.Traslate.GetLangText(_this.Mythis, "Node6") + "\\" + UsrCfg.Traslate.GetLangText(_this.Mythis, "Node6_2") + "\\" + UsrCfg.Traslate.GetLangText(_this.Mythis, "Node6_2_4"), UsrCfg.Traslate.GetLangText(_this.Mythis, "Node6_2_4"), "image/16/star-full.png").onClick = function () {
                        //$(_this.Object_container).html("");
                        var DIvTemp = _this.MenuObjetmain.ObjectDataMain;
                        $(DIvTemp).html("");
                        try {
                            //priority Matrix
                            var PriorityMatrix = new ItHelpCenter.MD.Configuration.PriorityMatrix.TfrPriorityMatrix(DIvTemp, function (_this) {

                            });
                        } catch (e) {
                            SysCfg.Log.Methods.WriteLog("main.js TMain.prototype.Mainasideleft", e);
                            //location.reload();
                        };
                    };
               }
                if (_this.GetUsrCFg_Properties.UserATRole.Atis_GraphicsPBI) {

                    VclTree.AddTreeNode(UsrCfg.Traslate.GetLangText(_this.Mythis, "Node6"), UsrCfg.Traslate.GetLangText(_this.Mythis, "Node6_3"), "image/16/star-full.png").onClick = function () {
                        
                        //$(_this.Object_container).html("");
                        var DIvTemp = _this.MenuObjetmain.ObjectDataMain;
                        $(DIvTemp).html("");
                        try {
                            //debugger;
                            var GraphicsPBI = new ItHelpCenter.GraphicsPBIManager.TGraphicsPBIBasic(DIvTemp, "", function (_this) {
                                alert("Callback PBIGraphicsManager");
                            });
                        } catch (e) {
                            SysCfg.Log.Methods.WriteLog("main.js TMain.prototype.Mainasideleft", e);
                        };
                    };
                }
		
                if ((!UsrCfg.Version.IsProduction)) {
                    VclTree.AddTreeNode(UsrCfg.Traslate.GetLangText(_this.Mythis, "Node6"), UsrCfg.Traslate.GetLangText(_this.Mythis, "Node6_5"), "image/16/star-full.png").onClick = function () {

                        var DIvTemp = _this.MenuObjetmain.ObjectDataMain;
                        $(DIvTemp).html("");
                        try {
                            //debugger;
                            var Votes = new ItHelpCenter.VotesManager.TVotesBasic(DIvTemp, "", function (_this) {
                                alert("Callback PBIGraphicsManager");
                            });
                        } catch (e) {
                            SysCfg.Log.Methods.WriteLog("main.js TMain.prototype.Mainasideleft", e);
                        };
                    };
                }

                if (_this.GetUsrCFg_Properties.UserATRole.Atis_Basic) {
                    VclTree.AddTreeNode(UsrCfg.Traslate.GetLangText(_this.Mythis, "Node6") + "\\" + UsrCfg.Traslate.GetLangText(_this.Mythis, "Node6_4") + "\\" + UsrCfg.Traslate.GetLangText(_this.Mythis, "Node6_4_1"), UsrCfg.Traslate.GetLangText(_this.Mythis, "Node6_4_1"), "image/16/star-full.png").onClick = function () {
                        var MenuObjetConsole = UsrCfg.Properties.MenuProfiler.NewPage(_this.MenuObjetmain);

                        try {
                            var frMDBase = new ItHelpCenter.MD.TfrMDBase(MenuObjetConsole, MenuObjetConsole.ObjectDataMain, function (_this) {

                            });
                            frMDBase.Initialize();
                        } catch (e) {
                            SysCfg.Log.Methods.WriteLog("main.js TMain.prototype.Mainasideleft", e);
                            //location.reload();
                        };
                    };
                }

                
                //******* Pantalla por defecto *******************************
                //Uimg(_this.Object_container, "imghome", "image/High/Home_HD.png", "");
                _this.load_Home(_this);

                //******* Pantalla por defecto end *******************************

                //////////////////////////////
                //Cambio Mauricio - 03/05/2018
                //CARGA DE MENU DE CONSULTAS EDITABLES - GP QUERIES a demanda
                //Quito llamado a la web service y cargo el menu de editables usando la coleccion del ATROLE
                //////////////////////////////
                var arrayPATHS_Make = new Array(); //areglo para validar si ya se creo el nodo
                var TreeNodeRoot = new Array(); //arreglo para validar si ya se creo el root
                for (var i = 0; i < UsrCfg.Properties.UserATRole.ATROLE.ATROLEGPQUERY_LIST.length; i++) {
                    if (UsrCfg.Properties.UserATRole.ATROLE.ATROLEGPQUERY_LIST[i].GPQUERY.IDGPQUERYTYPE == 1) //Main menu
                    {
                        var GPQUERY = UsrCfg.Properties.UserATRole.ATROLE.ATROLEGPQUERY_LIST[i].GPQUERY;
                        var PATH = GPQUERY.PATH + "." + GPQUERY.GPQUERY_NAME;
                        var arrayPATH = new Array();
                        arrayPATH = PATH.split('.');
                        var PATH = arrayPATH[0];

                        for (var j = 0; j < arrayPATH.length; j++) {
                            if (j == 0) { //NODO ROOT                                   
                                try {
                                    var indexOf = TreeNodeRoot.indexOf(arrayPATH[0]);
                                    if (indexOf == -1) {  //Evitar repetir el root del Nodo
                                        VclTree.AddTreeNodeRoot(arrayPATH[0], "image/ImgMono/16/folder.png");
                                        TreeNodeRoot.push(arrayPATH[0]);
                                    }
                                } catch (e) {
                                    SysCfg.Log.Methods.WriteLog("main.js TMain.prototype.Mainasideleft", e);
                                }
                            }
                            else if (j == (arrayPATH.length - 1)) { //ULTIMO NODO
                                PATH += "\\" + arrayPATH[j];
                                var TreeNode = VclTree.AddTreeNode(PATH, "", "");
                                TreeNode.Img = "image/ImgMono/16/document.png";
                                TreeNode.Tag = GPQUERY;
                                TreeNode.onClick = function (inTreeNode) {
                                    var DivMainTemp = _this.MenuObjetmain.ObjectDataMain;
                                    DivMainTemp.innerHTML = "";

                                    DinamicQuerys = new ItHelpCenter.TUfrGPView(
                                        DivMainTemp,
                                   function () {
                                   }, Persistence.GP.Methods.GPQUERY_COMPLETE(inTreeNode.Tag));
                                    DinamicQuerys.OpenQuery();
                                }
                            }
                            else { //NODOS INTERMEDIOS
                                PATH += "\\" + arrayPATH[j];
                                var indexOf = arrayPATHS_Make.indexOf(PATH);
                                if (indexOf == -1) {  //Evitar repetir el root del Nodo
                                    arrayPATHS_Make.push(PATH);
                                    var TreeNode = VclTree.AddTreeNode(PATH, "", "image/ImgMono/16/folder.png");
                                }
                            }
                        }
                    }
                }



                //////////////////////////////
                //Cambio Mauricio - 03/05/2018
                //CARGA DE MENU DE PBI TEMPLATES 
                //////////////////////////////
                arrayPATHS_Make = new Array(); //areglo para validar si ya se creo el nodo
                TreeNodeRoot = new Array(); //arreglo para validar si ya se creo el root
                for (var i = 0; i < UsrCfg.Properties.UserATRole.ATROLE.ATROLEPBITEMPLATE_LIST.length; i++) {
                    var PBITEMPLATE = UsrCfg.Properties.UserATRole.ATROLE.ATROLEPBITEMPLATE_LIST[i];
                    var PATH = PBITEMPLATE.TEMPLATE_PATH + "." + PBITEMPLATE.TEMPLATE_NAME;
                    var arrayPATH = new Array();
                    arrayPATH = PATH.split('.');
                    var PATH = arrayPATH[0];

                    for (var j = 0; j < arrayPATH.length; j++) {
                        if (j == 0) { //NODO ROOT                                   
                            try {
                                var indexOf = TreeNodeRoot.indexOf(arrayPATH[0]);
                                if (indexOf == -1) {  //Evitar repetir el root del Nodo
                                    VclTree.AddTreeNodeRoot(arrayPATH[0], "image/ImgMono/16/folder.png");
                                    TreeNodeRoot.push(arrayPATH[0]);
                                }
                            } catch (e) {
                                SysCfg.Log.Methods.WriteLog("main.js TMain.prototype.Mainasideleft", e);
                            }
                        }
                        else if (j == (arrayPATH.length - 1)) { //ULTIMO NODO
                            PATH += "\\" + arrayPATH[j];
                            var TreeNode = VclTree.AddTreeNode(PATH, "", "");
                            TreeNode.Img = "image/ImgMono/16/pie-chart.png";
                            TreeNode.Tag = PBITEMPLATE;
                            TreeNode.onClick = function (inTreeNode)
                            {

                                var DivMainTemp = _this.MenuObjetmain.ObjectDataMain;
                                DivMainTemp.innerHTML = "";

                                try {
                                    var PBIGraphics = new ItHelpCenter.GraphicsPBIManager.TGraphicsPBIManagerBasic(DivMainTemp, function (_this) { }, inTreeNode.Tag.IDPBITEMPLATE);
                                } catch (e) {
                                    SysCfg.Log.Methods.WriteLog("main.js TMain.prototype.Mainasideleft", e);
                                }
                            }
                        }
                        else { //NODOS INTERMEDIOS
                            PATH += "\\" + arrayPATH[j];
                            var indexOf = arrayPATHS_Make.indexOf(PATH);
                            if (indexOf == -1) {  //Evitar repetir el root del Nodo
                                arrayPATHS_Make.push(PATH);
                                var TreeNode = VclTree.AddTreeNode(PATH, "", "image/ImgMono/16/folder.png");
                            }
                        }
                    }
                }

                //////////////////////////////
                //Cambio Mauricio - 30/05/2018
                //CARGA DE MENU PANEL SERVICE 
                //////////////////////////////
                arrayPATHS_Make = new Array(); //areglo para validar si ya se creo el nodo
                TreeNodeRoot = new Array(); //arreglo para validar si ya se creo el root
                for (var i = 0; i < UsrCfg.Properties.UserATRole.ATROLE.ATROLEPSGROUP_LIST.length; i++)
                {
                    var PSGROUP = UsrCfg.Properties.UserATRole.ATROLE.ATROLEPSGROUP_LIST[i];
                    var PATH = PSGROUP.GROUP_PATH + "." + PSGROUP.GROUP_NAME;
                    var arrayPATH = new Array();
                    arrayPATH = PATH.split('.');
                    var PATH = arrayPATH[0];

                    for (var j = 0; j < arrayPATH.length; j++)
                    {
                        if (j == 0)
                        { //NODO ROOT                                   
                            try
                            {
                                var indexOf = TreeNodeRoot.indexOf(arrayPATH[0]);
                                if (indexOf == -1)
                                {  //Evitar repetir el root del Nodo
                                    VclTree.AddTreeNodeRoot(arrayPATH[0], "image/ImgMono/16/folder.png");
                                    TreeNodeRoot.push(arrayPATH[0]);
                                }
                            } catch (e)
                            {
                                SysCfg.Log.Methods.WriteLog("main.js TMain.prototype.Mainasideleft", e);
                            }
                        }
                        else if (j == (arrayPATH.length - 1))
                        { //ULTIMO NODO
                            PATH += "\\" + arrayPATH[j];
                            var TreeNode = VclTree.AddTreeNode(PATH, "", "");
                            TreeNode.Img = "image/ImgMono/16/news.png";
                            TreeNode.Tag = PSGROUP;
                            TreeNode.onClick = function (inTreeNode)
                            {
                                var DivMainTemp = _this.MenuObjetmain.ObjectDataMain;
                                DivMainTemp.innerHTML = "";

                                try
                                {
                                    var PanelservicesManager = new ItHelpCenter.TControlPanelServices(DivMainTemp, "", null, inTreeNode.Tag.IDPSGROUP);
                                } catch (e)
                                {
                                    SysCfg.Log.Methods.WriteLog("main.js TMain.prototype.Mainasideleft", e);
                                }
                            }
                        }
                        else
                        { //NODOS INTERMEDIOS
                            PATH += "\\" + arrayPATH[j];
                            var indexOf = arrayPATHS_Make.indexOf(PATH);
                            if (indexOf == -1)
                            {  //Evitar repetir el root del Nodo
                                arrayPATHS_Make.push(PATH);
                                var TreeNode = VclTree.AddTreeNode(PATH, "", "image/ImgMono/16/folder.png");
                            }
                        }
                    }
                }


                _this.Mainheader();
                _this.Maincontainer();
                _this.Mainfooter();
                _this.Mainasideright();
            }
            else {
                _this.CallbackModalResult(_this);
            }

           

        },
        error: function (response) {
            SysCfg.Log.Methods.WriteLog("main.js TMain.prototype.Mainasideleft " + "Error no llamo al servicio");
        }
    });
   
}



TMain.prototype.Mainasideright = function () {
    var _this = this.TParent();
    //_this.Object_asideright
    _this.Object_asideright.innerHTML = "";


    StackPanelDef = new Array(1);
    StackPanelDef[0] = new Array(6);
    StackPanelDef[0][1] = new Array(2); //columnas para botones de lenguaje
    StackPanelDef[0][3] = new Array(3); //columans para botones de estilos
    StackPanelDef[0][5] = new Array(2); //columans para botones de Divices

    var StackPanel = VclDivitions(_this.Object_asideright, "Mainasideright_Con", StackPanelDef);
    BootStrapCreateRows(StackPanel[0].Child);
    BootStrapCreateColumns(StackPanel[0].Child[1].Child, [[12, 12], [6, 6], [6, 6], [6, 6], [6, 6]]);
    BootStrapCreateColumns(StackPanel[0].Child[3].Child, [[12, 12, 12], [6, 6, 6], [6, 6, 6], [6, 6, 6], [6, 6, 6]]);
    BootStrapCreateColumns(StackPanel[0].Child[5].Child, [[12, 12], [6, 6], [6, 6], [6, 6], [6, 6]]);

    //LENGUAJES///////////////////////////////////////////////////////
    //Label1
    var label1 = Vcllabel(StackPanel[0].Child[0].This, "AsiderightLab1" + _this.GetUsrCFg_Properties.IDCMDBCI, TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "LabLenguaje"));
    $(label1.This).css("font-weight", "normal");
    $(StackPanel[0].Child[0].This).css("text-align", "center");

    //botones
    var btnEnglish = new TVclImagen(StackPanel[0].Child[1].Child[0].This, "");
    btnEnglish.Text = "";
    btnEnglish.Src = "image/High/American.png";
    btnEnglish.onClick = function () {
        $(btnEnglish.This).css("border", "1px solid rgb(236,240,245)");
        $(btnSpanish.This).css("border", "none");
        _this._DBTranslate.ClearDB(function (e) {
            _this._DBTranslate.SET_ID(1, function (e) {
                _this.SaveLenguaje(1);
                location.reload();
            }, function (e) {
                SysCfg.Log.Methods.WriteLog("main.js TMain.prototype.Mainasideright Can´t Changue Translate.js");
            });
        }, function (e) {
            SysCfg.Log.Methods.WriteLog("main.js TMain.prototype.Mainasideright Can´t Changue Translate.js");
        });
    }
    $(btnEnglish.This).css("cursor", "pointer");
    $(btnEnglish.This).css("padding", "2px");
    $(StackPanel[0].Child[1].Child[0].This).css("text-align", "center");

    var btnSpanish = new TVclImagen(StackPanel[0].Child[1].Child[1].This, "");
    btnSpanish.Text = "";
    btnSpanish.Src = "image/High/Spain.png";
    btnSpanish.onClick = function () {
        $(btnSpanish.This).css("border", "1px solid rgb(236,240,245)");
        $(btnEnglish.This).css("border", "none");
        _this._DBTranslate.ClearDB(function (e) {
            _this._DBTranslate.SET_ID(0, function (e) {
                _this.SaveLenguaje(0);
                location.reload();
            }, function (e) {
                SysCfg.Log.Methods.WriteLog("main.js TMain.prototype.Mainasideright Can´t Changue Translate.js");
            });
        }, function (e) {
            SysCfg.Log.Methods.WriteLog("main.js TMain.prototype.Mainasideright Can´t Changue Translate.js");
        });
    }
    $(btnSpanish.This).css("cursor", "pointer");
    $(btnSpanish.This).css("padding", "2px");
    $(StackPanel[0].Child[1].Child[1].This).css("text-align", "center");


    if (SysCfg.App.Properties.Config_Language.LanguageITF == SysCfg.App.Properties.TLanguage.SPANICH) {
        $(btnSpanish.This).css("border", "1px solid rgb(236,240,245)");
    }
    if (SysCfg.App.Properties.Config_Language.LanguageITF == SysCfg.App.Properties.TLanguage.ENGLISH) {
        $(btnEnglish.This).css("border", "1px solid rgb(236,240,245)");
    }
    if (SysCfg.App.Properties.Config_Language.LanguageITF == SysCfg.App.Properties.TLanguage.PORTUGUESE) {
        $(btnPortuguese.This).css("border", "1px solid rgb(236,240,245)");
    }
    
    //Persistence.Profiler.UserFunctions.CMDBUSERSETTINGS.IDLANGUAGE

    //Persistence.Profiler.UserFunctions.CMDBUSERSETTINGS.IDLANGUAGE
    //_this._DBTranslate.GET_ID(function (res) {
    //    if (res == 0) {
    //        $(btnSpanish.This).css("border", "1px solid rgb(236,240,245)");
    //    } else {
    //        $(btnEnglish.This).css("border", "1px solid rgb(236,240,245)");
    //    }
    //}, function (e) {
    //});

   

    //DEVICES/////////////////////////////////////////////////////////////////
    var label3 = Vcllabel(StackPanel[0].Child[4].This, "AsiderightLab1" + _this.GetUsrCFg_Properties.IDCMDBCI, TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "LabDevices"));
    $(label3.This).css("font-weight", "normal");
    $(StackPanel[0].Child[4].This).css("text-align", "center");

    var btnDevice1 = new TVclImagen(StackPanel[0].Child[5].Child[0].This, "");
    btnDevice1.Text = "";
    btnDevice1.Src = "image/High/Iphone.png";
    btnDevice1.onClick = function () {
        $(btnDevice1.This).css("border", "1px solid rgb(236,240,245)");
        $(btnDevice2.This).css("border", "none");
        SysCfg.App.Properties.Device = SysCfg.App.TDevice.Movile;
        SysCfg.App.Methods.SavebyIniIndexDb();
    }
    $(btnDevice1.This).css("cursor", "pointer");
    $(btnDevice1.This).css("padding", "2px");
    $(StackPanel[0].Child[5].Child[0].This).css("text-align", "center");

    var btnDevice2 = new TVclImagen(StackPanel[0].Child[5].Child[1].This, "");
    btnDevice2.Text = "";
    btnDevice2.Src = "image/High/Laptop.png";
    btnDevice2.onClick = function () {
        $(btnDevice2.This).css("border", "1px solid rgb(236,240,245)");
        $(btnDevice1.This).css("border", "none");
        SysCfg.App.Properties.Device = SysCfg.App.TDevice.Desktop;
        SysCfg.App.Methods.SavebyIniIndexDb();
    }
    $(btnDevice2.This).css("cursor", "pointer");
    $(btnDevice2.This).css("padding", "2px");
    $(StackPanel[0].Child[5].Child[1].This).css("text-align", "center");

    //precargado
    if (SysCfg.App.Properties.Device == SysCfg.App.TDevice.Movile) {
        $(btnDevice1.This).css("border", "1px solid rgb(236,240,245)");
        $(btnDevice2.This).css("border", "none");
    } else {
        $(btnDevice2.This).css("border", "1px solid rgb(236,240,245)");
        $(btnDevice1.This).css("border", "none");
    }
}


//**************************************
TMain.prototype.load_Home = function (sender) {
    _this = sender;
    //$(_this.Object_container).html("");
    var DivMainTemp = _this.MenuObjetmain.ObjectDataMain;
    DivMainTemp.innerHTML = "";
    try
    {
        var isHomeData = false;
        if (UsrCfg.Properties.UserATRole.ATROLE.SETHOME == 0) {//none

        }
        else if (UsrCfg.Properties.UserATRole.ATROLE.SETHOME == 1) {//Panel service
            var IDPSGroup = UsrCfg.Properties.UserATRole.ATROLE.IDPSGROUP;
            //var IDPSGroup = 1;
            if (IDPSGroup > 0) {
                isHomeData = true;
                //<negro> More Search Options //<azul> Search here
                var showMsgResult = false; //Mostrar el mensaje de resultado de la busqueda

                var PanelservicesManager = new ItHelpCenter.TControlPanelServices(DivMainTemp, "", null, IDPSGroup);
                PanelservicesManager.ObjControl.Click = function (obj) {
                    $(obj.ContentRowBody).html("");
                    //Parameters: (6) = 1.HTML Container, 2.Function Back, 3.Modo busqueda, 4.Ver boton cambio de modo (true - False), 5.Ver mensaje resultado, 6.Frase predeterminada    
                    var _Category = new ItHelpCenter.SD.Shared.Category.TCategoryManger(
                        obj.ContentRowBody,
                        function (OutRes) {
                            //Parametros: ObjectHTML, CallBack_Function, (True o False / Buscar Categoria), Array (idCategory, PhraseCategory, PathCategody) / Si no se va a buscar categoria se debe enviar el array con los datos
                            var DCaseNew = new ItHelpCenter.SD.CaseUser.NewCase.TUfrSDCaseNew(
                               DivMainTemp,
                               function (_DIvTemp, IDSDCASE) {
                                   $(_DIvTemp).html("");
                                   if (IDSDCASE > 0) {
                                       var SMConsoleUserSwitch = new ItHelpCenter.SD.CaseUser.Atention.TSMConsoleUserSwitch(
                                         _DIvTemp,
                                         function (_this) {

                                         },
                                         IDSDCASE
                                         );
                                   } else {//
                                       _this.load_Home(_this); //actualizacion jaimes 18/05/08
                                   }

                               },
                               OutRes.ID, OutRes.Detail, OutRes.Path, OutRes.PhraseIn)
                        },
                        ItHelpCenter.SD.Shared.Category.Mode.Basic_SeekSearch,
                        false, false, obj.PhraseBody);
                }



                //Actualizado jaimes 18/05/08
                $(_this.Object_container).css("padding-top", "10px");
                var MoreOptions = new TVclStackPanel(DivMainTemp, "MoreOptions", 1, [[12], [12], [12], [12], [12]]);
                var MoreOptionsC = MoreOptions.Column[0].This;

                $(MoreOptionsC).css("text-align", "center");
                $(MoreOptionsC).css("padding-top", "50px");
                $(MoreOptionsC).css("font-size", "20px");


                var labelMore = new Vcllabel(MoreOptionsC, "labelMore" + _this.ID, TVCLType.BS, TlabelType.H0, "");
                labelMore.innerText = UsrCfg.Traslate.GetLangText(_this.Mythis, "labelMoreOpt");
                $(labelMore).css("font-weight", "normal");

                var labelSearch = new Vcllabel(MoreOptionsC, "labelSearch" + _this.ID, TVCLType.BS, TlabelType.H0, "");
                labelSearch.innerText = UsrCfg.Traslate.GetLangText(_this.Mythis, "LabelSearchhere");
                labelSearch.style.color = "blue";
                $(labelSearch).css("margin-left", "15px");
                $(labelSearch).css("cursor", "pointer");
                $(labelSearch).css("font-weight", "normal");
                $(labelSearch).css("font-style", "oblique");

                labelSearch.onclick = function () {
                    _this.load_CaseNew(_this);
                }
            }


        }
        else if (UsrCfg.Properties.UserATRole.ATROLE.SETHOME == 2) {//PBI Graphics
            isHomeData = true;
            try {
                var PBIGraphics = new ItHelpCenter.GraphicsPBIManager.TGraphicsPBIManagerBasic(DivMainTemp, function (_this) { }, UsrCfg.Properties.UserATRole.ATROLE.IDPBITEMPLATE);
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("main.js TMain.prototype.load_Home", e);
            }

        }
        if (!isHomeData)
        {
            Uimg(DivMainTemp, "imghome", "image/High/Home_HD.png", "");
        }
    }
    catch(error)
    {
        Uimg(DivMainTemp, "imghome", "image/High/Home_HD.png", "");
    }
}

TMain.prototype.load_CaseNew = function (sender) {
    _this = sender;
    var DIvTemp = _this.MenuObjetmain.ObjectDataMain;
    $(DIvTemp).html("");
    DIvTemp.style.minHeight = _this.Object_asideleft.clientHeight + "px";
    //Parameters: (6) = 1.HTML Container, 2.Function Back, 3.Modo busqueda, 4.Ver boton cambio de modo (true - False), 5.Ver mensaje resultado, 6.Frase predeterminada    
    var _Category = new ItHelpCenter.SD.Shared.Category.TCategoryManger(
       DIvTemp,
        function (OutRes) {
            //Parametros: ObjectHTML, CallBack_Function, (True o False / Buscar Categoria), Array (idCategory, PhraseCategory, PathCategody) / Si no se va a buscar categoria se debe enviar el array con los datos

            var DCaseNew = new ItHelpCenter.SD.CaseUser.NewCase.TUfrSDCaseNew(
               DIvTemp,
               function (_DIvTemp, IDSDCASE) {
                   var DivMainTemp = _this.MenuObjetmain.ObjectDataMain;
                   DivMainTemp.innerHTML = "";
                   if (IDSDCASE > 0) {
                       var SMConsoleUserSwitch = new ItHelpCenter.SD.CaseUser.Atention.TSMConsoleUserSwitch(
                      DivMainTemp,
                      function (_this) {

                      },
                      IDSDCASE
                      );
                   } else {
                       _this.load_Home(_this);
                   }

               },
               OutRes.ID, OutRes.Detail, OutRes.Path, OutRes.PhraseIn)
        },
        ItHelpCenter.SD.Shared.Category.Mode.SeekSearch,
        true, false, "");
}

//************ Methods ******************
TMain.prototype.SaveLenguaje = function (valor) {
    //guardar el codigo de lenguaje
    var dataArray = {
        'IDLANGUAGE': valor
    };

    direc = SysCfg.App.Properties.xRaiz + 'Componet/Traslate/WcfTraslate.svc/SETIDLANGUAGE';
    //enviamos el arreglo a la webService  
    $.ajax({
        type: "POST",
        async: false,
        url: direc,
        data: JSON.stringify(dataArray),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
        },
        error: function (error) {
            alert('error: ' + error.txtDescripcion + " \n Location: Traslate / Send manual List");
        }
    });
}

TMain.prototype.SaveStyle = function (valor) {
    //llamado a la service que guarda los estilos 
    var dataArray = {
        'ID': valor
    };

    $.ajax({
        type: "POST",
        async: false,
        url: 'Service/MainStyle.svc/editStyle',
        data: JSON.stringify(dataArray),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
        },
        error: function (error) {
            alert('error: ' + error.txtDescripcion + " \n Location: Main / Edit Style");
        }
    });
}


TMain.prototype.ContainerSize = function () {
    var _this = this.TParent();
    try {
        $(_this.Object_container).removeClass("main-1");
        $(_this.Object_container).removeClass("main-2");
        $(_this.Object_container).removeClass("main-3");
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("main.js TMain.prototype.Mainasideleft", e);
    }

    ////clases
    //if (_this.asideRight_Visible == true && _this.asideLeft_Visible == true) {
    //    $(_this.Object_container).addClass("main-1");
    //    //_this.Object_asideleft.style.display == "block";
    //    $(_this.Object_asideleft).css("display", "block");
    //    _this.Object_asideright.style.display == "block";
    //} else if (_this.asideRight_Visible == true && _this.asideLeft_Visible == false) {
    //    $(_this.Object_container).addClass("main-2");
    //    //_this.Object_asideleft.style.display == "none";
    //    $(_this.Object_asideleft).css("display", "none");
    //    _this.Object_asideright.style.display == "block";
    //} else if (_this.asideRight_Visible == false && _this.asideLeft_Visible == true) {
    //    $(_this.Object_container).addClass("main-2");
    //    //_this.Object_asideleft.style.display == "block";
    //    $(_this.Object_asideleft).css("display", "block");
    //    _this.Object_asideright.style.display == "none";
    //} else {
    //    $(_this.Object_container).addClass("main-3");
    //    //_this.Object_asideleft.style.display  == "none";
    //    $(_this.Object_asideleft).css("display","none");
    //    _this.Object_asideright.style.display == "none";
    //}
}

TMain.prototype.ContainerResolution = function () {
    var _this = this.TParent();

    //Resolucion    
    //_this.Object_container.style.display = "block";

    //var ancho = $(window).width();
    //if ((ancho <= 700) && ((_this.asideLeft_Visible = true) || (_this.asideRight_Visible = true))) {
    //    //los dos aside invisibles y al 100%  
    //    _this.asideLeft_Visible = false;
    //    _this.asideRight_Visible = false;
    //    $(_this.Object_asideleft).css("width", "100%");
    //    $(_this.Object_asideright).css("width", "100%");
    //    _this.resolucion = 1;
    //    _this.ContainerSize();
    //} else if ((ancho > 700 && ancho <= 900) && (_this.asideRight_Visible = true)) {
    //    //ocultamos aside derecho y mostramos aside izquierdo y regresan al tamaño 24.3%                                     
    //    _this.asideLeft_Visible = true;
    //    _this.asideRight_Visible = false;
    //    _this.Object_container.style.display = "block";
    //    $(_this.Object_asideleft).css("width", "250px");
    //    $(_this.Object_asideright).css("width", "250px");
    //    _this.ContainerSize();
    //    _this.resolucion = 2;
    //} else {
    //    $(_this.Object_asideleft).css("width", "250px");
    //    $(_this.Object_asideright).css("width", "250px");
    //    _this.resolucion = 3;
    //}

}



//Anexo 

//STYLES/////////////////////////////////////////////////////////////////
//Label2
/*
var label2 = Vcllabel(StackPanel[0].Child[2].This, "AsiderightLab2" + _this.GetUsrCFg_Properties.IDCMDBCI, TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "LabEstilos"));
$(label2.This).css("font-weight", "normal");
$(StackPanel[0].Child[2].This).css("text-align", "center");

//botones
var btnStyle1 = new TVclImagen(StackPanel[0].Child[3].Child[0].This, "");
btnStyle1.Text = "";
btnStyle1.Src = "image/High/UiBlue.png";
btnStyle1.onClick = function () {
    $(btnStyle1.This).css("border", "1px solid rgb(236,240,245)");
    $(btnStyle2.This).css("border", "none");
    $(btnStyle3.This).css("border", "none");
    $("#linkestilo").attr("href", SysCfg.App.Properties.xRaiz + "Css/Main_StyleA.css");
    _this.SaveStyle(1);
}
$(btnStyle1.This).css("cursor", "pointer");
$(btnStyle1.This).css("padding", "2px");
$(StackPanel[0].Child[3].Child[0].This).css("text-align", "center");

var btnStyle2 = new TVclImagen(StackPanel[0].Child[3].Child[1].This, "");
btnStyle2.Text = "";
btnStyle2.Src = "image/High/UiGreen.png";
btnStyle2.onClick = function () {
    $(btnStyle2.This).css("border", "1px solid rgb(236,240,245)");
    $(btnStyle1.This).css("border", "none");
    $(btnStyle3.This).css("border", "none");
    $("#linkestilo").attr("href", SysCfg.App.Properties.xRaiz + "Css/Main_StyleB.css");
    _this.SaveStyle(2);
}
$(btnStyle2.This).css("cursor", "pointer");
$(btnStyle2.This).css("padding", "2px");
$(StackPanel[0].Child[3].Child[1].This).css("text-align", "center");

var btnStyle3 = new TVclImagen(StackPanel[0].Child[3].Child[2].This, "");
btnStyle3.Text = "";
btnStyle3.Src = "image/High/UiYellow.png";
btnStyle3.onClick = function () {
    $(btnStyle3.This).css("border", "1px solid rgb(236,240,245)");
    $(btnStyle1.This).css("border", "none");
    $(btnStyle2.This).css("border", "none");
    $("#linkestilo").attr("href", SysCfg.App.Properties.xRaiz + "Css/Main_StyleC.css");
    _this.SaveStyle(3);
}
$(btnStyle3.This).css("cursor", "pointer");
$(btnStyle3.This).css("padding", "2px");

$(btnStyle1.This).css("border", "1px solid rgb(236,240,245)");


//CARGAR DATOS 
//seleccionar opcion de style
try {
    var dataArray = {};
    direc = 'Service/MainStyle.svc/GetStyle';
    $.ajax({
        type: "POST",
        async: false,
        url: direc,
        data: JSON.stringify(dataArray),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            if (response.d == 2) {
                btnStyle2.onClick();
            } else if (response.d == 3) {
                btnStyle3.onClick();
            } else {
                btnStyle1.onClick();
            }
        },
        error: function (error) {
            alert('error: ' + error.txtDescripcion + " \n Location: FrMain / Get Lenguage");
        }
    });
} catch (e) {
    SysCfg.Log.Methods.WriteLog("main.js TMain.prototype.Mainasideleft", e);

}

$(StackPanel[0].Child[3].Child[2].This).css("text-align", "center");

$("#Mainasideright_Con_0").css('width', "calc(100% - 25px)");
$("#Mainasideright_Con_0").css('margin-left', "10px");
$("#Mainasideright_Con_0 .row").css('width', "100%");
$("#Mainasideright_Con_0 .row").css('margin', "auto");
$("#Mainasideright_Con_0 div").css('padding', "0px");
$("#Mainasideright_Con_0 .row").css('padding', "5px 0px 0px 0px");
$("#Mainasideright_Con_0").css('color', "white");

$(StackPanel[0].Child[3].Child[0].This).css("height", "32px");
$(StackPanel[0].Child[3].Child[1].This).css("height", "32px");
$(StackPanel[0].Child[3].Child[2].This).css("height", "32px");

$(StackPanel[0].Child[3].Child[2].This).css("margin-top", "10px");
$(StackPanel[0].Child[2].This).css("margin-top", "20px");
*/