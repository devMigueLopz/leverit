﻿Source.Page.Properties.TModalResult = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },

    None: { value: 0, name: "None" },
    Create: { value: 1, name: "Create" },
    Cancel: { value: 2, name: "Cancel" },    
    Ok: { value: 3, name: "Ok" },
    //Yes: { value: 1, name: "Yes" },
    //No: { value: 1, name: "No" },   

}

/*
   mrNone	0	None. Used as a default value before the user exits.
   mrOk	idOK	The user exited with OK button.
   mrCancel	idCancel	The user exited with the CANCEL button. 
   mrAbort	idAbort	The user exited with the ABORT button.
   mrRetry	idRetry	The user exited with the RETRY button.
   mrIgnore	idIgnore	The user exited with the IGNORE button.
   mrYes	idYes	The user exited with the YES button.
   mrNo	idNo	The user exited with the NO button.
   mrAll	mrNo + 1	The user exited with the ALL button.
   mrNoToAll	mrAll + 1	The user exited with the NO TO ALL button.
   mrYesToAll	mrNoToAll + 1	The user exited with the YES TO ALL button.
   */

//User Control formas
Source.Page.Properties.Page = function () {
    this.name = "Page";
    this.id = null;
    this.OnModalResult = null;
    this._ModalResult = null;//Source.Page.Properties.TModalResult.GetEnum(undefined);
    this.OnClosed = null;
    this.OnBeforeDestroy = null;
    this.PageParent = null;

    Object.defineProperty(this, 'ModalResult', {
        get: function () {
            return this._ModalResult;
        },
        set: function (inValue) {
            this._ModalResult = inValue;
            if (this._ModalResult != Source.Page.Properties.TModalResult.None)
            {
                if (this.OnModalResult != null)
                    this.OnModalResult(this);
            }
        },
        enumerable: true
    });

    //this.ModalResult_set= function () {
    //    return this._ModalResult;
    //}
    //this.ModalResult_get = function (inValue) {
    //    this._ModalResult = inValue;
    //    if (this._ModalResult != Source.Page.Properties.TModalResult.None) {
    //        if (this.OnModalResult != null)
    //            this.OnModalResult(this);
    //    }
    //}

    this.Show = function () {
        this.Modal.ShowModal();
    }

    this.Destroy = function () {
        if (this.OnBeforeDestroy != null)
            this.OnBeforeDestroy(this);

        if (this.ObjectHTML != null) {
            if (this.ObjectHTML.parentNode != undefined)
                this.ObjectHTML.parentNode.removeChild(this.ObjectHTML);
        }
    }

    this.Close = function () {
        if (this.OnClosed != null)
            this.OnClosed(this);
    }
}

//Fomrmas popup
Source.Page.Properties.PageModal = function () {
    this.ObjectHTML = null;
    this.Modal = new TVclModal(document.body, null, "");

}.Implements(new Source.Page.Properties.Page());






