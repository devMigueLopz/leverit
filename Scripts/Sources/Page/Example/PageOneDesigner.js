﻿Source.Page.Example.TPageOne.prototype.InitializeComponent = function () {
    var _this = this.TParent();

    var spPrincipal = new TVclStackPanel(_this.Object, "", 1, [[12], [12], [12], [12], [12]]);
    var spLblStatus = new TVclStackPanel(spPrincipal.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
    _this.LblStatus = new TVcllabel(spLblStatus.Column[0].This, "", TlabelType.H0);
    _this.LblStatus.Text = "Label"
    _this.LblStatus.VCLType = TVCLType.BS;

    var spCmbStatus = new TVclStackPanel(spPrincipal.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
    _this.CmbStatus = new TVclComboBox2(spCmbStatus.Column[0].This, "");
    _this.CmbStatus.This.classList.add("form-control");
    _this.CmbStatus.onChange = function () {
        _this.CmbStatus_SelectedChanged(_this, _this.CmbStatus);
    }
    var spLblDescripcion = new TVclStackPanel(spPrincipal.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);

    _this.LblDescripcion = new TVcllabel(spLblDescripcion.Column[0].This, "", TlabelType.H0);
    _this.LblDescripcion.Text = "Label"
    _this.LblDescripcion.VCLType = TVCLType.BS;

    var spTxtDescripcion = new TVclStackPanel(spPrincipal.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);

    _this.TxtDescripcion = new TVclMemo(spTxtDescripcion.Column[0].This, "");
    _this.TxtDescripcion.Text = "";
    _this.TxtDescripcion.VCLType = TVCLType.BS;
    _this.TxtDescripcion.This.classList.add("form-control");
    _this.TxtDescripcion.Rows = 5;
    _this.TxtDescripcion.Cols = 50;

    _this.GridRetun = new TVclStackPanel(_this.Object, "", 1, [[12], [12], [12], [12], [12]]);

    var spSecundario1 = new TVclStackPanel(_this.GridRetun.Column[0].This, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [4, 8]]);

    _this.Lbl_CASE_RETURN_COST = new TVcllabel(spSecundario1.Column[0].This, "", TlabelType.H0);
    _this.Lbl_CASE_RETURN_COST.Text = "Result: "
    _this.Lbl_CASE_RETURN_COST.VCLType = TVCLType.BS;

    _this.CmbBox_CASE_RETURN_COST = new TVclComboBox2(spSecundario1.Column[1].This, "");
    _this.CmbBox_CASE_RETURN_COST.This.classList.add("form-control");

    var spSecundario2 = new TVclStackPanel(_this.GridRetun.Column[0].This, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [4, 8]]);

    _this.Lbl_CASE_RETURN_STR = new TVcllabel(spSecundario2.Column[0].This, "", TlabelType.H0);
    _this.Lbl_CASE_RETURN_STR.Text = "Cost Return: "
    _this.Lbl_CASE_RETURN_STR.VCLType = TVCLType.BS;

    _this.SpinEdit_CASE_RETURN_STR = new TVclTextBox(spSecundario2.Column[1].This, "");
    _this.SpinEdit_CASE_RETURN_STR.Text = ""
    _this.SpinEdit_CASE_RETURN_STR.VCLType = TVCLType.BS;

    var spSecundario3 = new TVclStackPanel(_this.Object, "", 3, [[8, 2, 2], [8, 2, 2], [8, 2, 2], [8, 2, 2], [8, 2, 2]]);
    spSecundario3.Row.This.style.marginTop = "10px";
    _this.BtnCerrar = new TVclImagen(spSecundario3.Column[1].This, "");
    _this.BtnCerrar.Src = "image/24/delete.png";
    _this.BtnCerrar.Title = "Cerrar";
    _this.BtnCerrar.Cursor = "pointer";
    _this.BtnCerrar.onClick = function () {
        _this.BtnCerrar_OnClick(_this, _this.BtnCerrar);
    }

    _this.BtnOk = new TVclImagen(spSecundario3.Column[2].This, "");
    _this.BtnOk.Src = "image/24/success.png";
    _this.BtnOk.Title = "Aceptar";
    _this.BtnOk.Cursor = "pointer";
    _this.BtnOk.onClick = function () {
        _this.BtnOk_OnClick(_this, _this.BtnOk);
    }

    

}

