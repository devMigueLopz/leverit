﻿Source.Page.Example.TPageOne = function (inObject, inid, inOnModalResult) {
    //*****
    this.TParent = function () {
        return this;
    }.bind(this);

    var _this = this.TParent();
    this.Object = inObject;
    //this.CallbackModalResult = inCallbackModalResult;
    this.OnModalResult = inOnModalResult;
    _this.name = "PageOne";
    _this.id = inid;

    //************ <Lines><numerolinea> es para textos de mensages,alertas o componetes run time *********
    UsrCfg.Traslate.GetLangText(this.name, "Lines1", "The component is initialize");
    UsrCfg.Traslate.GetLangText(this.name, "Lines2", "Clicked the Close button");
    UsrCfg.Traslate.GetLangText(this.name, "Lines3", "The OK button was clicked");
    UsrCfg.Traslate.GetLangText(this.name, "Lines4", "Change the box status combo");

    this.InitializeComponent();

   // if (SysCfg.App.Properties.Device == SysCfg.App.TDevice.Movile)  //Si es mobile o destokp

    //*********** <Nombredelcomponente>.<Porpiedad> ******************************************************
    //UsrCfg.Traslate.GetLangText(_this.name, "VCLxxx.Text", "Press Button to save");

    //*********** <Variable publicas>     ****************************************************************
    _this.ModalResult = Source.Page.Properties.TModalResult.Create;

}.Implements(new Source.Page.Properties.Page());

//{$R *.dfm}

Source.Page.Example.TPageOne.prototype.Initialize = function () {

    var _this = this.TParent();
    alert(_this.Text);

    
}

Source.Page.Example.TPageOne.prototype.BtnCerrar_OnClick = function (sender, EventArgs) {
    var _this = sender;
    //    _this.Modal.CloseModal();
    alert(UsrCfg.Traslate.GetLangText(this.name, "Lines2"));
    _this.ModalResult = Source.Page.Properties.TModalResult.Close;
}

Source.Page.Example.TPageOne.prototype.BtnOk_OnClick = function (sender, EventArgs) {
    var _this = sender;
    alert(UsrCfg.Traslate.GetLangText(this.name, "Lines3"));
    _this.ModalResult = Source.Page.Properties.TModalResult.Ok;
}

Source.Page.Example.TPageOne.prototype.CmbStatus_SelectedChanged = function (sender, EventArgs) {
    var _this = sender;
    alert(UsrCfg.Traslate.GetLangText(this.name, "Lines4"));
}




