﻿Source.Page.Example.TPageMenu.prototype.InitializeComponent = function () {
    var _this = this.TParent();

    _this.MenuObject.OnBeforeBackPage = function () {
        alert("antes de ir atras (forma el _this esta en Menu Profiler)");
    }

    var spPrincipal = new TVclStackPanel(_this.Object, "2", 2, [[9, 3], [9, 3], [9, 3], [9, 3], [9, 3]]);

    var DivBotonera = _this.MenuObject.GetDivData(spPrincipal.Column[1], spPrincipal.Column[0]);

    var stackPanelBotonera = new TVclStackPanel(DivBotonera, "1", 1, [[12], [12], [12], [12], [12]]);

    //_this.stackPanelBodyBotonera = new TVclStackPanel(stackPanelBotonera.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
   
    if (Source.Menu.IsMobil)
    { stackPanelBotonera.Row.This.classList.add("menuContenedorMobil"); }
    else
    {
        stackPanelBotonera.Row.This.style.margin = "0px 0px";
        stackPanelBotonera.Column[0].This.style.paddingLeft = "10px ";
        stackPanelBotonera.Column[0].This.style.paddingTop = "10px ";
        stackPanelBotonera.Column[0].This.style.paddingBottom = "10px";
        stackPanelBotonera.Column[0].This.style.border = "2px solid rgba(0, 0, 0, 0.08)";
        stackPanelBotonera.Column[0].This.style.boxShadow = " 0 0px 8px 0 rgba(0, 0, 0, 0.06), 0 1px 0px 0 rgba(0, 0, 0, 0.02);";
        stackPanelBotonera.Column[0].This.style.marginTop = "15px";
        stackPanelBotonera.Column[0].This.style.backgroundColor = "#FAFAFA";
    }

    var spBotonera = new TVclStackPanel(stackPanelBotonera.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
    spBotonera.Row.This.style.margin = "4px";

    var spBoton1 = new TVclStackPanel(spBotonera.Column[0].This, "1", 2, [[8, 4], [8, 4], [8, 4], [8, 4], [8, 4]]);

    _this.Label1 = new TVcllabel(spBoton1.Column[0].This, "Label1", TlabelType.H0);
    _this.Label1.Text = "Boton 1";

    _this.btnButton1 = new TVclButton(spBoton1.Column[1].This, "btnButton1");
    _this.btnButton1.Cursor = "pointer";
    _this.btnButton1.Src = "image/24/Refresh.png";
    _this.btnButton1.VCLType = TVCLType.BS;
    _this.btnButton1.This.style.minWidth = "100%";
    _this.btnButton1.This.classList.add("btn-xs");
    _this.btnButton1.onClick = function () {
        _this.BtnButton1_onClick(_this, _this.btnButton1);
    }

    var spBoton2 = new TVclStackPanel(spBotonera.Column[0].This, "1", 2, [[8, 4], [8, 4], [8, 4], [8, 4], [8, 4]]);
    spBoton2.Row.This.style.marginTop = "10px";

    _this.Label2 = new TVcllabel(spBoton2.Column[0].This, "LabelCerrar", TlabelType.H0);
    _this.Label2.Text = "Cerrar";

    _this.BtnCerrar = new TVclButton(spBoton2.Column[1].This, "");
    _this.BtnCerrar.VCLType = TVCLType.BS;
    _this.BtnCerrar.Src = "image/24/delete.png";
    _this.BtnCerrar.Cursor = "pointer";
    _this.BtnCerrar.This.style.minWidth = "100%";
    _this.BtnCerrar.This.classList.add("btn-xs");
    _this.BtnCerrar.onClick = function () {
        _this.BtnCerrar_OnClick(_this, _this.BtnCerrar);
    }

    var spBoton3 = new TVclStackPanel(spBotonera.Column[0].This, "1", 2, [[8, 4], [8, 4], [8, 4], [8, 4], [8, 4]]);
    spBoton3.Row.This.style.marginTop = "10px";

    _this.Label3 = new TVcllabel(spBoton3.Column[0].This, "LabelAceptar", TlabelType.H0);
    _this.Label3.Text = "Aceptar";

    _this.BtnOk = new TVclButton(spBoton3.Column[1].This, "");
    _this.BtnOk.VCLType = TVCLType.BS;
    _this.BtnOk.Src = "image/24/success.png";
    _this.BtnOk.Cursor = "pointer";
    _this.BtnOk.This.style.minWidth = "100%";
    _this.BtnOk.This.classList.add("btn-xs");
    _this.BtnOk.onClick = function () {
        _this.BtnOk_OnClick(_this, _this.BtnOk);
    }



    var spContenido = new TVclStackPanel(spPrincipal.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
    spContenido.Row.This.style.margin = "0px 0px";
    spContenido.Column[0].This.style.paddingLeft = "10px ";
    spContenido.Column[0].This.style.paddingTop = "10px ";
    spContenido.Column[0].This.style.border = "2px solid rgba(0, 0, 0, 0.08)";
    spContenido.Column[0].This.style.boxShadow = " 0 0px 8px 0 rgba(0, 0, 0, 0.06), 0 1px 0px 0 rgba(0, 0, 0, 0.02);";
    spContenido.Column[0].This.style.marginTop = "15px";
    spContenido.Column[0].This.style.paddingBottom = "15px";
    spContenido.Column[0].This.style.backgroundColor = "#FAFAFA";
    
    var spLblStatus = new TVclStackPanel(spContenido.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
    _this.LblStatus = new TVcllabel(spLblStatus.Column[0].This, "", TlabelType.H0);
    _this.LblStatus.Text = "Label"
    _this.LblStatus.VCLType = TVCLType.BS;

    var spCmbStatus = new TVclStackPanel(spContenido.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
    _this.CmbStatus = new TVclComboBox2(spCmbStatus.Column[0].This, "");
    _this.CmbStatus.This.classList.add("form-control");
    _this.CmbStatus.onChange = function () {
        _this.CmbStatus_SelectedChanged(_this, _this.CmbStatus);
    }
    var spLblDescripcion = new TVclStackPanel(spContenido.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);

    _this.LblDescripcion = new TVcllabel(spLblDescripcion.Column[0].This, "", TlabelType.H0);
    _this.LblDescripcion.Text = "Label"
    _this.LblDescripcion.VCLType = TVCLType.BS;

    var spTxtDescripcion = new TVclStackPanel(spContenido.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);

    _this.TxtDescripcion = new TVclMemo(spTxtDescripcion.Column[0].This, "");
    _this.TxtDescripcion.Text = "";
    _this.TxtDescripcion.VCLType = TVCLType.BS;
    _this.TxtDescripcion.This.classList.add("form-control");
    _this.TxtDescripcion.Rows = 5;
    _this.TxtDescripcion.Cols = 50;

    _this.GridRetun = new TVclStackPanel(spContenido.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);

    var spSecundario1 = new TVclStackPanel(_this.GridRetun.Column[0].This, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [4, 8]]);

    _this.Lbl_CASE_RETURN_COST = new TVcllabel(spSecundario1.Column[0].This, "", TlabelType.H0);
    _this.Lbl_CASE_RETURN_COST.Text = "Result: "
    _this.Lbl_CASE_RETURN_COST.VCLType = TVCLType.BS;

    _this.CmbBox_CASE_RETURN_COST = new TVclComboBox2(spSecundario1.Column[1].This, "");
    _this.CmbBox_CASE_RETURN_COST.This.classList.add("form-control");

    var spSecundario2 = new TVclStackPanel(_this.GridRetun.Column[0].This, "", 2, [[4, 8], [4, 8], [4, 8], [4, 8], [4, 8]]);

    _this.Lbl_CASE_RETURN_STR = new TVcllabel(spSecundario2.Column[0].This, "", TlabelType.H0);
    _this.Lbl_CASE_RETURN_STR.Text = "Cost Return: "
    _this.Lbl_CASE_RETURN_STR.VCLType = TVCLType.BS;

    _this.SpinEdit_CASE_RETURN_STR = new TVclTextBox(spSecundario2.Column[1].This, "");
    _this.SpinEdit_CASE_RETURN_STR.Text = ""
    _this.SpinEdit_CASE_RETURN_STR.VCLType = TVCLType.BS;

}