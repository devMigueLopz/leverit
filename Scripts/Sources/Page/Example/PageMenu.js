﻿Source.Page.Example.TPageMenu = function (inMenuObject, inObject, inid, inOnModalResult) {
    //*****
    this.TParent = function () {
        return this;
    }.bind(this);

    var _this = this.TParent();
    this.Object = inObject;
    this.MenuObject = inMenuObject;
    //this.CallbackModalResult = inCallbackModalResult;
    this.OnModalResult = inOnModalResult;
    _this.name = "PageOne";
    _this.id = inid;

    //************ <Lines><numerolinea> es para textos de mensages,alertas o componetes run time *********
    UsrCfg.Traslate.GetLangText(this.name, "Lines1", "The component is initialize");
    UsrCfg.Traslate.GetLangText(this.name, "Lines2", "Clicked the Close button");
    UsrCfg.Traslate.GetLangText(this.name, "Lines3", "The OK button was clicked");
    UsrCfg.Traslate.GetLangText(this.name, "Lines4", "Change the box status combo");
    UsrCfg.Traslate.GetLangText(this.name, "Lines5", "Clicked on Button 1 of the menu");

    this.InitializeComponent();



    //*********** <Nombredelcomponente>.<Porpiedad> ******************************************************
    //UsrCfg.Traslate.GetLangText(_this.name, "VCLxxx.Text", "Press Button to save");

    //*********** <Variable publicas>     ****************************************************************
    _this.ModalResult = Source.Page.Properties.TModalResult.Create;

}.Implements(new Source.Page.Properties.Page());

//{$R *.dfm}

Source.Page.Example.TPageMenu.prototype.Initialize = function () {
    var _this = this.TParent();
    alert(_this.Text);


}

Source.Page.Example.TPageMenu.prototype.BtnCerrar_OnClick = function (sender, EventArgs) {
    var _this = sender;
    //    _this.Modal.CloseModal();
    alert(UsrCfg.Traslate.GetLangText(this.name, "Lines2"));
    _this.ModalResult = Source.Page.Properties.TModalResult.Close;
}

Source.Page.Example.TPageMenu.prototype.BtnOk_OnClick = function (sender, EventArgs) {
    var _this = sender;
    alert(UsrCfg.Traslate.GetLangText(this.name, "Lines3"));
    _this.ModalResult = Source.Page.Properties.TModalResult.Ok;
}

Source.Page.Example.TPageMenu.prototype.CmbStatus_SelectedChanged = function (sender, EventArgs) {
    var _this = sender;
    alert(UsrCfg.Traslate.GetLangText(this.name, "Lines4"));
}

Source.Page.Example.TPageMenu.prototype.BtnButton1_onClick = function (sender, EventArgs) {
    var _this = sender;
    alert(UsrCfg.Traslate.GetLangText(this.name, "Lines5"));
    _this.ModalResult = Source.Page.Properties.TModalResult.Ok;
}