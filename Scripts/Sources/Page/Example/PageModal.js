﻿Source.Page.Example.TPageModal = function (inObject, inid) {
    this.TParent = function () {
        return this;
    }.bind(this);

    var _this = this.TParent();
    this.Object = this.Modal.Body.This;
    _this.Modal._inObjectHTMLBack = inObject;

    _this.name = "PageModal";
    _this.id = inid;

    //************ <Lines><numerolinea> es para textos de mensages,alertas o componetes run time *********
    UsrCfg.Traslate.GetLangText(this.name, "Lines1", "The component is initialize");
    UsrCfg.Traslate.GetLangText(this.name, "Lines2", "Clicked the Close button");
    UsrCfg.Traslate.GetLangText(this.name, "Lines3", "The OK button was clicked");
    UsrCfg.Traslate.GetLangText(this.name, "Lines4", "Change the box status combo");

    this.InitializeComponent();


    //*********** <Nombredelcomponente>.<Porpiedad> ******************************************************
    //UsrCfg.Traslate.GetLangText(_this.name, "VCLxxx.Text", "Press Button to save");
    //*********** <Variable publicas>     ****************************************************************
    _this._ModalResult = Source.Page.Properties.TModalResult.Create;

}.Implements(new Source.Page.Properties.PageModal());

//{$R *.dfm}

Source.Page.Example.TPageModal.prototype.Initialize = function () {
    var _this = this.TParent();
    _this.Modal.AddHeaderContent("PageModal");
    _this.Modal.Width = "75%";
}

Source.Page.Example.TPageModal.prototype.BtnCerrar_OnClick = function (sender, EventArgs) {
    var _this = sender;
    _this.Modal.CloseModal();
    _this.Close();
}

Source.Page.Example.TPageModal.prototype.BtnOk_OnClick = function (sender, EventArgs) {
    var _this = sender;
}

Source.Page.Example.TPageModal.prototype.CmbStatus_SelectedChanged = function (sender, EventArgs) {
    var _this = sender;
}


