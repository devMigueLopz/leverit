﻿//Source.Sequencial.Methods

Source.Sequencial.Methods.GetColor = function () {
	if(Source.Sequencial.Properties.ColorList.length-1 > Source.Sequencial.Properties.Index){
		Source.Sequencial.Properties.Index++;
		return Source.Sequencial.Properties.ColorList[Source.Sequencial.Properties.Index];
	}
	else{
		Source.Sequencial.Properties.Index = 0;
		return Source.Sequencial.Properties.ColorList[0];
	}
}
