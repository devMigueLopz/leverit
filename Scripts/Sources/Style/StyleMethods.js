﻿Source.Style.Methods.SetFontStyleInHtml = function (elemntHtml, TypeStyle) {
    var fontStyle = new Source.Style.Properties.TFontStyle();
    var genericElementHtml = null;
    try {
        genericElementHtml = (elemntHtml.hasOwnProperty('This') === true) ? elemntHtml.This : elemntHtml;
        fontStyle = UsrCfg.Properties.ProfilerStyle.SearchListStyle(TypeStyle);
        if (fontStyle!==null) {
            genericElementHtml.style.fontFamily = SysCfg.Str.Methods.IsNullOrEmpity(fontStyle.FontFamily) ? genericElementHtml.style.fontFamily : fontStyle.FontFamily;
            genericElementHtml.style.fontStyle = SysCfg.Str.Methods.IsNullOrEmpity(fontStyle.FontStyle) ? genericElementHtml.style.fontStyle : fontStyle.FontStyle;
            genericElementHtml.style.fontWeight = SysCfg.Str.Methods.IsNullOrEmpity(fontStyle.FontBold) ? genericElementHtml.style.fontWeight : fontStyle.FontBold;
            genericElementHtml.style.fontSize = SysCfg.Str.Methods.IsNullOrEmpity(fontStyle.FontSize) ? genericElementHtml.style.fontSize : fontStyle.FontSize;
            genericElementHtml.style.color = SysCfg.Str.Methods.IsNullOrEmpity(fontStyle.FontColor) ? genericElementHtml.style.color : fontStyle.FontColor;
            genericElementHtml.style.textDecoration = SysCfg.Str.Methods.IsNullOrEmpity(fontStyle.UnderlinedStyle) ? genericElementHtml.style.fontStyle : genericStyleFont.UnderlinedStyle;
            genericElementHtml.style.textDecorationColor = SysCfg.Str.Methods.IsNullOrEmpity(fontStyle.UnderlinedColor) ? genericElementHtml.style.fontStyle : genericStyleFont.UnderlinedColor;
            genericElementHtml.style.textAlign = SysCfg.Str.Methods.IsNullOrEmpity(fontStyle.TextAlign) ? genericElementHtml.style.textAlign : fontStyle.TextAlign;
        }
    } catch (error) {}
}
