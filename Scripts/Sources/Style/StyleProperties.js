﻿Source.Style.Properties.TTypeStyle = {
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    Normal: { value: 0, name: "Normal" },
    Title: { value: 1, name: "TiTle" },
    Title1: { value: 2, name: "Title 1" },
    Title2: { value: 3, name: "Title 2" },
    Subtitle: { value: 4, name: "Subtitle" }
};


Source.Style.Properties.TFontStyle = function () {
    this.TParent = function () {
        return this;
    }.bind(this);
    this._Name = "";
    this._FontFamily = "";
    this._FontStyle = "";
    this._FontBold = "";
    this._FontSize = "";
    this._FontColor = "";
    this._Background = "";
    this._UnderlinedStyle = "";
    this._UnderlinedColor = "";
    this._TextAlign = "";
    Object.defineProperty(this, 'Name', {
        get: function () {
            return this._Name;
        },
        set: function (Value) {
            this._Name = Value;
        }
    });
    Object.defineProperty(this, 'FontFamily', {
        get: function () {
            return this._FontFamily;
        },
        set: function (Value) {
            this._FontFamily = Value;
        }
    });
    Object.defineProperty(this, 'FontStyle', {
        get: function () {
            return this._FontStyle;
        },
        set: function (Value) {
            this._FontStyle = Value;
        }
    });
    Object.defineProperty(this, 'FontBold', {
        get: function () {
            return this._FontBold;
        },
        set: function (Value) {
            this._FontBold = Value;
        }
    });
    Object.defineProperty(this, 'FontSize', {
        get: function () {
            return this._FontSize;
        },
        set: function (Value) {
            this._FontSize = Value;
        }
    });
    Object.defineProperty(this, 'FontColor', {
        get: function () {
            return this._FontColor;
        },
        set: function (Value) {
            this._FontColor = Value;
        }
    });
    Object.defineProperty(this, 'UnderlinedStyle', {
        get: function () {
            return this._UnderlinedStyle;
        },
        set: function (Value) {
            this._UnderlinedStyle = Value;
        }
    });
    Object.defineProperty(this, 'UnderlinedColor', {
        get: function () {
            return this._UnderlinedColor;
        },
        set: function (Value) {
            this._UnderlinedColor = Value;
        }
    });
    Object.defineProperty(this, 'TextAlign', {
        get: function () {
            return this._TextAlign;
        },
        set: function (Value) {
            this._TextAlign = Value;
        }
    });
    Object.defineProperty(this, 'Background', {
        get: function () {
            return this._Background;
        },
        set: function (Value) {
            this._Background = Value;
        }
    });
}
