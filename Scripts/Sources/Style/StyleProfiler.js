Source.Style.Profiler.TProfilerStyle = function () {
    this.TParent = function () {
        return this;
    }.bind(this);
    this.ListStyle = new Array();
}
Source.Style.Profiler.TProfilerStyle.prototype.Initialize = function () {
    var _this = this.TParent();
    try {
        function SetDataObjectFontStyle(objStyle) {
            var objFontStyle = new Source.Style.Properties.TFontStyle();
            objFontStyle.TypeStyle = objStyle.TypeStyle;
            objFontStyle.FontFamily = objStyle.fontFamily;
            objFontStyle.FontStyle = objStyle.fontStyle;
            objFontStyle.FontBold = objStyle.fontStyle;
            objFontStyle.FontSize = objStyle.fontSize;
            objFontStyle.FontColor = objStyle.fontColor;
            objFontStyle.UnderlinedStyle = objStyle.underlinedStyle;
            objFontStyle.UnderlinedColor = objStyle.underlinedColor;
            objFontStyle.TextAlign = objStyle.textAlign;
            _this.ListStyle.push(objFontStyle);
        }
        SetDataObjectFontStyle({ TypeStyle: Source.Style.Properties.TTypeStyle.Normal, fontFamily: "calibri", fontStyle: "normal", fontBold: "normal", fontSize: "11px", fontColor: "black" });
        SetDataObjectFontStyle({ TypeStyle: Source.Style.Properties.TTypeStyle.Title, fontFamily: "calibri light", fontStyle: "normal", fontBold: "normal", fontSize: "28px", fontColor: "black" });
        SetDataObjectFontStyle({ TypeStyle: Source.Style.Properties.TTypeStyle.Title1, fontFamily: "calibri light", fontStyle: "normal", fontBold: "normal", fontSize: "16px", fontColor: "blue" });
        SetDataObjectFontStyle({ TypeStyle: Source.Style.Properties.TTypeStyle.Title2, fontFamily: "calibri light", fontStyle: "normal", fontBold: "normal", fontSize: "13px", fontColor: "blue" });
        SetDataObjectFontStyle({ TypeStyle: Source.Style.Properties.TTypeStyle.Subtitle, fontFamily: "calibri", fontStyle: "normal", fontBold: "normal", fontSize: "11px", fontColor: "silver" });

    } catch (error) {
        _this.ListStyle = null;
    }
}

Source.Style.Profiler.TProfilerStyle.prototype.SearchListStyle = function (TypeStyle) {
    var _this = this.TParent();
    for (var i = 0; i < _this.ListStyle.length; i++) {
        if (_this.ListStyle[i].TypeStyle === TypeStyle) {
            return (_this.ListStyle[i]);
        }
    }
    return (null);
}