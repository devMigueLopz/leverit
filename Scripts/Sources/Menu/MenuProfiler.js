﻿
//Source.Menu.IsMobil = false;

Source.Menu.TMenuStatus =
{
    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
    Ready: { value: 0, name: "Ready" }, 
    Cancel: { value: 1, name: "Cancel" },  
    Continue: { value: 2, name: "Continue" }
}

Source.Menu.TMenuObjet = function (inObjectHtmlMenu, inObjectHtmlMain) {
    var _this = this;
    this.Name = "";
    this.MenuProfiler = null;
    this.ObjectHtmlMenu = inObjectHtmlMenu;
    this.ObjectHtmlMain = inObjectHtmlMain;
    this.OnBeforeBackPage = null;
    this.OnAfterBackPage = null;
    this.ParentObject = null; //si es nulo el primero
    this.ChildObject = null;
    this.MenuStatus = Source.Menu.TMenuStatus.Ready;
    this.IsMobil = false;
    this.Mythis = "TMenuObjet";
    UsrCfg.Traslate.GetLangText(this.Mythis, "txtSearch", "Search...");

    this.spContainer = new TVclStackPanel(this.ObjectHtmlMenu, "1", 1, [[12], [12], [12], [12], [12]]);
    this.DivContrainer = this.spContainer.Row.This;

    this.spPrincipal = new TVclStackPanel(this.spContainer.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
    this.spPrincipal.Row.This.style.backgroundColor = "#222d32";

    this.spObjectData = new TVclStackPanel(this.spContainer.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
    this.ObjectData = this.spObjectData.Column[0].This;

    this.spContainerMain = new TVclStackPanel(this.ObjectHtmlMain, "1", 1, [[12], [12], [12], [12], [12]]);
    this.DivContrainerMain = this.spContainerMain.Row.This;
    this.DivContrainerMain.id = "DivContrainerMain"

    this.spObjectDataMain = new TVclStackPanel(this.spContainerMain.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
    this._ObjectDataMain = this.spObjectDataMain.Column[0].This;


    //this.spObjectDataActive = new TVclStackPanel(this.spContainer.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
    //this.ObjectDataActive = this.spObjectDataActive.Column[0].This;

    this.spBarra = new TVclStackPanel(this.spPrincipal.Column[0].This, "1", 3, [[4, 4, 4], [4, 4, 4], [4, 4, 4], [4, 4, 4], [4, 4, 4]]);

    this.btnBack = new TVclImagen(this.spBarra.Column[0].This, "");
    this.btnBack.Src = "image/SilkyLine/32/Left5.png";
    this.btnBack.Title = "back";
    this.btnBack.Cursor = "pointer";
    this.btnBack.This.style.margin = "10px";
    this.btnBack.onClick = function () {
        _this.BackPage();
    };

    this.btnHome = new TVclImagen(this.spBarra.Column[1].This, "");
    this.btnHome.Src = "image/SilkyLine/32/Home2.png";
    this.btnHome.Title = "Home";
    this.btnHome.Cursor = "pointer";
    this.btnHome.This.style.margin = "10px";
    this.btnHome.onClick = function () {
        _this.Home(_this);
    };

    this.spSearch = new TVclStackPanel(this.spPrincipal.Column[0].This, "1", 2, [[10, 2], [10, 2], [10, 2], [10, 2], [10, 2]]);
    this.spSearch.Row.This.style.margin = "10px 10px 10px 10px";

    this.spSearch.Row.Visible = !UsrCfg.Version.IsProduction;

    this.spSearch.Column[0].This.style.padding = "0px";
    this.txtSearch = new TVclTextBox(this.spSearch.Column[0].This, "txtSearch");
    this.txtSearch.Text = ""
    this.txtSearch.VCLType = TVCLType.BS;

    this.txtSearch.This.style.backgroundColor = "#374850";
    this.txtSearch.This.style.border = "1px solid #374850";
    this.txtSearch.This.style.borderCollapse = "separate";
    this.txtSearch.This.style.color = "#cccccc";
    this.txtSearch.This.placeholder = UsrCfg.Traslate.GetLangText(this.Mythis, "txtSearch");

    this.btnSearch = new TVclImagen(this.spSearch.Column[1].This, "");
    this.btnSearch.Src = "image/24/Search.png";
    this.btnSearch.Title = "Search";
    this.btnSearch.Cursor = "pointer";
    this.btnSearch.This.style.marginTop = "5px";
    this.btnSearch.onClick = function () {
        if (_this.MenuProfiler != null)
            if (_this.MenuProfiler.OnSearch != null)
                _this.MenuProfiler.OnSearch(_this.MenuProfiler, _this);
    };

    this.BackPage = function () {
        if (_this.MenuProfiler.MainBody != null)
            _this.MenuProfiler.MainBody.ToggleMainSideBar(false);

        if (_this.ParentObject != null) {
            if (_this.MenuProfiler != null)
                if (_this.OnBeforeBackPage != null)
                    _this.OnBeforeBackPage(_this.MenuProfiler, _this);

            if (_this.MenuStatus == Source.Menu.TMenuStatus.Ready || _this.MenuStatus == Source.Menu.TMenuStatus.Continue) {
                $(_this.DivContrainer).fadeOut(100, function () {
                    _this.spContainerMain.Row.This.style.display = "none";
                    $(_this.ParentObject.DivContrainer).fadeIn(100, function () {
                        _this.ParentObject.spContainerMain.Row.This.style.display = "block";
                        if (_this.MenuProfiler != null)
                            if (_this.ParentObject.OnAfterBackPage != null)
                                _this.ParentObject.OnAfterBackPage(_this.MenuProfiler, _this);
                    });
                });
            }
        }
        else
            _this.Home(_this);

        
    }

    this.BackPageOnlyParent = function () {
        if (_this.ParentObject != null) {
            $(_this.DivContrainer).fadeOut(100, function () {
                _this.spContainerMain.Row.This.style.display = "none";
                $(_this.ParentObject.DivContrainer).fadeIn(100, function () {
                    _this.ParentObject.spContainerMain.Row.This.style.display = "block";
                    if (_this.MenuProfiler != null)
                        if (_this.ParentObject.OnAfterBackPage != null)
                            _this.ParentObject.OnAfterBackPage(_this.MenuProfiler, _this);
                });
            });
        }
        else
            _this.Home(_this);
    }


    this.Home = function (inPageObject) {
        if (inPageObject.ParentObject == null) {
            inPageObject.DivContrainer.style.display = "block";
        }
        else {
            inPageObject.DivContrainer.style.display = "none";
            _this.Home(inPageObject.ParentObject);
        }

        //if (_this.MenuProfiler.MainBody != null)
        //    _this.MenuProfiler.MainBody.ToggleMainSideBar(false);
    }

    this.GetDivData = function (inDivMenu, inDivMain) {
        if (_this.IsMobil) {

            if (inDivMenu != null)
                if (inDivMenu.This != undefined)
                    if (inDivMenu.This.parentNode != undefined)
                        inDivMenu.This.parentNode.removeChild(inDivMenu.This);

            if (inDivMain != null) {
                if (inDivMain.This != undefined) {
                    inDivMain.This.className = '';
                    inDivMain.This.classList.add("col-xs-12", "col-sm-12", "col-md-12", "col-lg-12", "col-xl-12");
                }
                //if (_this.ObjectDataMain != null)
                //    $(_this.ObjectDataMain).append(inDivMain.This);
            }

            return _this.ObjectData;
        }
        else {
            if (inDivMenu == null)
                return _this.ObjectData;
            else
                return inDivMenu.This;
        }
    }

    Object.defineProperty(this, 'ObjectDataMain', {
        get: function () {
            
            //if (this.ChildObject != null)
            //    this.ChildObject.spContainerMain.Row.This.style.display = "none";           

            //if (this.ParentObject != null)
            //    this.ParentObject.spContainerMain.Row.This.style.display = "none";

            if (this.MenuProfiler != null)
            {
                for (var i = 0; i < this.MenuProfiler.PageObject.length; i++) {
                    this.MenuProfiler.PageObject[i].spContainerMain.Row.This.style.display = "none";
                }
                
            }
            this.spContainerMain.Row.This.style.display = "block";
            return this._ObjectDataMain;

        },
        set: function (inValue) {
            this._ObjectDataMain = inValue;

        }
    });
}

Source.Menu.TMenuProfiler = function (inObjectHtmlMenu, inObjectHtmlMain) {
    var _this = this;
    this.ObjectHtmlMenu = inObjectHtmlMenu;
    this.ObjectHtmlMain = inObjectHtmlMain;
    this.ObjectHtmlMain.id = "soyContainerMain";
    this.PageObject = new Array();
    this.PageObjectSelect = null;
    this.OnSearch = null;
    this.MainBody = null;
    
    //this.OnBeforeBackPage = null;
    //this.OnAfterBackPage = null;
    this.IsMobil = true;
    this.spPageObjectMain = null;

    this.spPrincipal = new TVclStackPanel(this.ObjectHtmlMenu, "1", 1, [[12], [12], [12], [12], [12]]);

    this.spPageObject = new TVclStackPanel(this.spPrincipal.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);

    //this.spPageObjectMain = new TVclStackPanel(this.ObjectHtmlMain, "1", 1, [[12], [12], [12], [12], [12]]);
    this.spPageObjectMain = this.ObjectHtmlMain;
    //this.spPageObjectMain.Row.This.id = "soyObjectMain";

    //if (Source.Menu.IsMobil) {
    //    this.spPageObjectMain.Column[0].This.style.padding = "0px";
    //}

    this.NewPage = function (inPageObjet) {
        if (this.MainBody != null)
            this.MainBody.ToggleMainSideBar(false);

        var PageObjectTemp = null;
        //var PageTemp = _this.FindPage(inName);

        //if (PageTemp != null)
        //    PageObjectTemp = PageTemp;
        //else {

        PageObjectTemp = new Source.Menu.TMenuObjet(_this.spPageObject.Column[0].This, _this.spPageObjectMain);
        PageObjectTemp.ParentObject = inPageObjet;        
        PageObjectTemp.MenuProfiler = _this;
        PageObjectTemp.IsMobil = Source.Menu.IsMobil;

        _this.PageObject.push(PageObjectTemp);
        //}

        if (inPageObjet != null) {
            PageObjectTemp.spSearch.Row.Visible = false;
            inPageObjet.ChildObject = PageObjectTemp;
            if (Source.Menu.IsMobil) {
                inPageObjet.DivContrainer.style.display = "none";
                PageObjectTemp.DivContrainer.style.display = "block";

                inPageObjet.spContainerMain.Row.This.style.display = "none";
                //if (inPageObjet.ParentObject != null) {
                //    inPageObjet.DivContrainer.style.display = "none";
                //    PageObjectTemp.DivContrainer.style.display = "block";
                //    //$(inPageObjet.DivContrainer).fadeOut(0, function () {
                //    //    $(PageObjectTemp.DivContrainer).fadeIn(0);
                //    //});
                //}
                //else {
                //    inPageObjet.DivContrainer.style.display = "none";
                //    PageObjectTemp.DivContrainer.style.display = "block";
                //    //$(inPageObjet.DivContrainer).fadeOut(0, function () {
                //    //    $(PageObjectTemp.DivContrainer).fadeIn(0);
                //    //});
                //}
            }
            else {               
                PageObjectTemp.spBarra.Row.This.style.display = "none";
                PageObjectTemp.spSearch.Row.This.style.display = "none";
                inPageObjet.spContainerMain.Row.This.style.display = "none"
            }

        }
        else {
            PageObjectTemp.spSearch.Row.Visible = UsrCfg.Properties.UserATRole.isServiceDesk();;
            PageObjectTemp.spBarra.Row.This.style.display = "none";
            PageObjectTemp.DivContrainer.style.display = "block";
        }        

        

        return PageObjectTemp;
    }

    this.FindPage = function (inName) {
        var PageTemp = null;
        for (var i = 0; i < _this.PageObject.length; i++) {
            if (_this.PageObject[i].Name == inName) {
                PageTemp = _this.PageObject[i];
                break;
            }
        }
        return PageTemp;
    }

    this.BeforeBackPage = function (inPageObject) {
        if (_this.OnBeforeBackPage != null)
            _this.OnBeforeBackPage(_this, inPageObject);
    }

    this.AfterBackPage = function (inPageObject) {
        if (_this.OnAfterBackPage != null)
            _this.OnAfterBackPage(_this, inPageObject);
    }
}