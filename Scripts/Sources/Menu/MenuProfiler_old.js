﻿
Source.Menu.IsMobil = false;

Source.Menu.TMenuObjet = function (inObjectHtml) {
    var _this = this;
    this.MenuProfiler = null;
    this.ObjectHtml = inObjectHtml;
    this.ParentObject = null; //si es nulo el primero
    this.IsMobil = false;

    this.spContainer = new TVclStackPanel(this.ObjectHtml, "1", 1, [[12], [12], [12], [12], [12]]);
    this.DivContrainer = this.spContainer.Row.This;

    this.spPrincipal = new TVclStackPanel(this.spContainer.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);

    this.spObjectData = new TVclStackPanel(this.spContainer.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
    this.ObjectData = this.spObjectData.Column[0].This;

    //this.spObjectDataActive = new TVclStackPanel(this.spContainer.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
    //this.ObjectDataActive = this.spObjectDataActive.Column[0].This;

    this.spBarra = new TVclStackPanel(this.spPrincipal.Column[0].This, "1", 3, [[4, 4, 4], [4, 4, 4], [4, 4, 4], [4, 4, 4], [4, 4, 4]]);

    this.btnBack = new TVclImagen(this.spBarra.Column[0].This, "");
    this.btnBack.Src = "image/SilkyLine/32/Left5.png";
    this.btnBack.Title = "back";
    this.btnBack.Cursor = "pointer";
    this.btnBack.This.style.margin = "10px";
    this.btnBack.onClick = function () {
        _this.BackPage();
    };

    this.btnHome = new TVclImagen(this.spBarra.Column[1].This, "");
    this.btnHome.Src = "image/SilkyLine/32/Home2.png";
    this.btnHome.Title = "Home";
    this.btnHome.Cursor = "pointer";
    this.btnHome.This.style.margin = "10px";
    this.btnHome.onClick = function () {
        _this.Home(_this);
    };

    this.BackPage = function () {
        if (_this.ParentObject != null) {
            $(_this.DivContrainer).fadeOut(0, function () {
                $(_this.ParentObject.DivContrainer).fadeIn(0);
            });
        }
        else
            _this.Home(_this);
    }

    this.Home = function (inPageObject) {
        if (inPageObject.ParentObject == null) {
            inPageObject.DivContrainer.style.display = "block";
        }
        else {
            inPageObject.DivContrainer.style.display = "none";
            _this.Home(inPageObject.ParentObject);
        }
    }

    this.GetDivData = function (inDivMenu, inDivMain) {
        if (_this.IsMobil) {
            if (inDivMenu != null)
                inDivMenu.parentNode.removeChild(inDivMenu);

            if (inDivMain != null) {
                inDivMain.className = '';
                inDivMain.classList.add("col-xs-12", "col-sm-12", "col-md-12", "col-lg-12", "col-xl-12");
            }
            return _this.ObjectData;
        }
        else {
            if (inDivMenu == null)
                return _this.ObjectData;
            else
                return inDivMenu;
        }
    }
}

Source.Menu.TMenuProfiler = function (inObjectHtml) {
    var _this = this;
    this.ObjectHtml = inObjectHtml;
    this.PageObject = new Array();
    this.PageObjectSelect = null;
    this.OnChangePage = null;
    this.IsMobil = true;

    this.spPrincipal = new TVclStackPanel(this.ObjectHtml, "1", 1, [[12], [12], [12], [12], [12]]);

    this.spPageObject = new TVclStackPanel(this.spPrincipal.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);

    this.NewPage = function (inPageObjet) {

        var PageObjectTemp = null;
        PageObjectTemp = new Source.Menu.TMenuObjet(_this.spPageObject.Column[0].This);
        PageObjectTemp.ParentObject = inPageObjet;
        PageObjectTemp.MenuProfiler = _this;
        PageObjectTemp.IsMobil = Source.Menu.IsMobil;
       
        _this.PageObject.push(PageObjectTemp);
        if (inPageObjet != null) {
            if (Source.Menu.IsMobil) {
                if (inPageObjet.ParentObject != null) {
                    inPageObjet.DivContrainer.style.display = "none";
                    PageObjectTemp.DivContrainer.style.display = "block";
                    //$(inPageObjet.DivContrainer).fadeOut(0, function () {
                    //    $(PageObjectTemp.DivContrainer).fadeIn(0);
                    //});
                }
                else {
                    inPageObjet.DivContrainer.style.display = "none";
                    PageObjectTemp.DivContrainer.style.display = "block";
                    //$(inPageObjet.DivContrainer).fadeOut(0, function () {
                    //    $(PageObjectTemp.DivContrainer).fadeIn(0);
                    //});
                }
            }
            else { PageObjectTemp.spBarra.Row.This.style.display = "none"; }
        }
        else {
            PageObjectTemp.spBarra.Row.This.style.display = "none";
            PageObjectTemp.DivContrainer.style.display = "block";
        }
    
    return PageObjectTemp;
}


/*

    //this.spTree = new TVclStackPanel(this.spPrincipal.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);

//this.Tree = new TVclPageProfilerTree(this.spTree.Column[0].This, "ID");


this.getTreeNodes = function () {
    return this.Tree.TreeNodes;
}
this.getTreeNodesList = function () {
    return this.Tree.TreeNodeList;
}
this.AddTreeNodeRoot = function (Ruta, inImg) {
    var NodePosition = null;
    var NodeRoot = null;
    var words = Ruta.split('\\');

    for (var Counter = 0; Counter < words.length; Counter++) {
        var thirdWordFromRight = (Counter >= 0 ? words[Counter] : null);
        var Existe = false;
        if (Counter == 0) {
            for (var CounterNodo = 0; CounterNodo < this.Tree.TreeNodes.length; CounterNodo++) {
                if (this.Tree.TreeNodes[CounterNodo].Text == thirdWordFromRight) {
                    NodePosition = this.Tree.TreeNodes[CounterNodo];
                    NodeRoot = this.Tree.TreeNodes[CounterNodo];
                    Existe = true;
                }
            }
            if (Existe == false) {
                var _node = new TVclTreeNode(null, null);
                _node._RutaGeneral.push(Ruta);
                _node.Text = thirdWordFromRight;
                _node.IsRoot = true;
                _node.TreeParent = this.Tree;
                this.Tree.TreeNodes.push(_node);
                this.Tree.TreeNodeList.push(_node);
                this.Tree.TreeNodeRootList.push(_node);
                this.Tree.This.appendChild(_node.This);
                _node.This.style.paddingTop = "5px";
                _node.This.style.paddingBottom = "5px";
                NodePosition = _node;
                NodeRoot = _node;

            }
        }
        else {
            for (var CounterNodo = 0; CounterNodo < NodePosition.TreeNodes.length; CounterNodo++) {
                if (NodePosition.TreeNodes[CounterNodo].Text == thirdWordFromRight) {
                    NodePosition = NodePosition.TreeNodes[CounterNodo];
                    Existe = true;
                }
            }
            if (Existe == false) {
                var _node = new TVclTreeNode(null, null);
                _node._RutaGeneral.push(Ruta);
                _node.Text = thirdWordFromRight;
                _node.TreeRootNode = NodeRoot;
                _node.TreeParent = this.Tree;
                NodePosition.AddTreeNode(_node);
                NodePosition = _node;
            }
        }
    }
    if (inImg.length > 0)
        try {
            if (NodeRoot.Img.length > 0) {
                if (this.Tree.ImgReplace) {
                    NodeRoot.Img = inImg;
                }
            } else {
                NodeRoot.Img = inImg;
            }
        } catch (e) {
            NodeRoot.Img = inImg;
        }
    return NodeRoot;
}
this.AddTreeNode = function (Ruta, NodeText, inImg) {
    var TreeNodeLast = null;
    var NodeRoot = null;
    var NodePosition = null;
    var words = Ruta.split('\\');

    if (Ruta.length <= 0) {
        var _node = new TVclTreeNode(null, null);
        _node._RutaGeneral.push(Ruta);
        _node.Text = NodeText;
        _node.TreeParent = this.Tree;
        this.Tree.TreeNodes.push(_node);
        this.Tree.This.appendChild(_node.This);
        _node.This.style.paddingTop = "5px";
        _node.This.style.paddingBottom = "5px";
        NodePosition = _node;
        if (inImg.length > 0)
            NodePosition.Img = inImg;
        return NodePosition;
    }
    else {
        for (var Counter = 0; Counter < words.length; Counter++) {
            var thirdWordFromRight = (Counter >= 0 ? words[Counter] : null);
            var Existe = false;
            if (Counter == 0) {
                for (var CounterNodo = 0; CounterNodo < this.Tree.TreeNodes.length; CounterNodo++) {
                    if (this.Tree.TreeNodes[CounterNodo].Text == thirdWordFromRight) {
                        NodePosition = this.Tree.TreeNodes[CounterNodo];
                        NodeRoot = this.Tree.TreeNodes[CounterNodo];
                        Existe = true;
                    }
                }
                if (Existe == false) {
                    var _node = new TVclTreeNode(null, null);
                    _node._RutaGeneral.push(Ruta);
                    _node.Text = thirdWordFromRight;
                    _node.TreeParent = this.Tree;
                    this.Tree.TreeNodes.push(_node);
                    this.Tree.This.appendChild(_node.This);
                    _node.This.style.paddingTop = "5px";
                    _node.This.style.paddingBottom = "5px";
                    NodePosition = _node;

                }

                if (words.length == (Counter + 1)) {
                    var _node = new TVclTreeNode(null, null);
                    _node._RutaGeneral.push(Ruta);
                    _node.Text = NodeText;
                    _node.TreeParent = this.Tree;
                    NodePosition.AddTreeNode(_node);
                    NodePosition = _node;
                    TreeNodeLast = _node;
                    if (inImg.length > 0)
                        NodePosition.Img = inImg;
                }

            }
            else {
                for (var CounterNodo = 0; CounterNodo < NodePosition.TreeNodes.length; CounterNodo++) {
                    if (NodePosition.TreeNodes[CounterNodo].Text == thirdWordFromRight) {
                        NodePosition = NodePosition.TreeNodes[CounterNodo];
                        Existe = true;
                    }
                }
                if (Existe == false) {
                    var _node = new TVclTreeNode(null, null);
                    _node._RutaGeneral.push(Ruta);
                    _node.Text = thirdWordFromRight;
                    _node.TreeParent = this.Tree;
                    NodePosition.AddTreeNode(_node);
                    NodePosition = _node;
                }
                else {
                    if (words.length == (Counter + 1)) {
                        var _node = new TVclTreeNode(null, null);
                        _node._RutaGeneral.push(Ruta);
                        _node.Text = NodeText;
                        _node.TreeParent = this.Tree;
                        NodePosition.AddTreeNode(_node);
                        NodePosition = _node;
                        TreeNodeLast = _node;
                        _node.This.style.paddingBottom = "0px";
                        if (inImg.length > 0)
                            NodePosition.Img = inImg;
                    }
                }
            }
        }
    }

    return NodePosition;
}*/



}


/*
var TVclPageProfilerTree = function (ObjectHtml, Id) {
    this.This = Uul(ObjectHtml, Id);
    this.TreeNodeRootList = new Array();
    this.TreeNodeList = new Array();
    this.TreeNodes = new Array();
    this._Tag;
    this.ExpanAll = false;
    this.SePuedeBuscar = false;//Si se esta haciendo la funcion expanader o contraer todos
    this.rutaBuscada = "";//Para buscar
    this.ExpanClickTreeNode = false;
    this.ImgReplace = false; //si se desea remplazar la imagen en casi que exista una ya previa

    this.getTreeNodes = function () {
        return this.TreeNodes;
    }
    this.getTreeNodesList = function () {
        return this.TreeNodeList;
    }
    this.AddTreeNodeRoot = function (Ruta, inImg) {
        var NodePosition = null;
        var NodeRoot = null;
        var words = Ruta.split('\\');

        for (var Counter = 0; Counter < words.length; Counter++) {
            var thirdWordFromRight = (Counter >= 0 ? words[Counter] : null);
            var Existe = false;
            if (Counter == 0) {
                for (var CounterNodo = 0; CounterNodo < this.TreeNodes.length; CounterNodo++) {
                    if (this.TreeNodes[CounterNodo].Text == thirdWordFromRight) {
                        NodePosition = this.TreeNodes[CounterNodo];
                        NodeRoot = this.TreeNodes[CounterNodo];
                        Existe = true;
                    }
                }
                if (Existe == false) {
                    var _node = new TVclTreeNode(null, null);
                    _node._RutaGeneral.push(Ruta);
                    _node.Text = thirdWordFromRight;
                    _node.IsRoot = true;
                    _node.TreeParent = this;
                    this.TreeNodes.push(_node);
                    this.TreeNodeList.push(_node);
                    this.TreeNodeRootList.push(_node);
                    this.This.appendChild(_node.This);
                    _node.This.style.paddingTop = "5px";
                    _node.This.style.paddingBottom = "5px";
                    NodePosition = _node;
                    NodeRoot = _node;

                }
            }
            else {
                for (var CounterNodo = 0; CounterNodo < NodePosition.TreeNodes.length; CounterNodo++) {
                    if (NodePosition.TreeNodes[CounterNodo].Text == thirdWordFromRight) {
                        NodePosition = NodePosition.TreeNodes[CounterNodo];
                        Existe = true;
                    }
                }
                if (Existe == false) {
                    var _node = new TVclTreeNode(null, null);
                    _node._RutaGeneral.push(Ruta);
                    _node.Text = thirdWordFromRight;
                    _node.TreeRootNode = NodeRoot;
                    _node.TreeParent = this;
                    NodePosition.AddTreeNode(_node);
                    NodePosition = _node;
                }
            }
        }
        if (inImg.length > 0)
            try {
                if (NodeRoot.Img.length > 0) {
                    if (this.ImgReplace) {
                        NodeRoot.Img = inImg;
                    }
                } else {
                    NodeRoot.Img = inImg;
                }
            } catch (e) {
                NodeRoot.Img = inImg;
            }
        return NodeRoot;
    }

    this.AddTreeNode = function (Ruta, NodeText, inImg) {
        var TreeNodeLast = null;
        var NodeRoot = null;
        var NodePosition = null;
        var words = Ruta.split('\\');

        if (Ruta.length <= 0) {
            var _node = new TVclTreeNode(null, null);
            _node._RutaGeneral.push(Ruta);
            _node.Text = NodeText;
            _node.TreeParent = this;
            this.TreeNodes.push(_node);
            this.This.appendChild(_node.This);
            _node.This.style.paddingTop = "5px";
            _node.This.style.paddingBottom = "5px";
            NodePosition = _node;
            if (inImg.length > 0)
                NodePosition.Img = inImg;
            return NodePosition;
        }
        else {
            for (var Counter = 0; Counter < words.length; Counter++) {
                var thirdWordFromRight = (Counter >= 0 ? words[Counter] : null);
                var Existe = false;
                if (Counter == 0) {
                    for (var CounterNodo = 0; CounterNodo < this.TreeNodes.length; CounterNodo++) {
                        if (this.TreeNodes[CounterNodo].Text == thirdWordFromRight) {
                            NodePosition = this.TreeNodes[CounterNodo];
                            NodeRoot = this.TreeNodes[CounterNodo];
                            Existe = true;
                        }
                    }
                    if (Existe == false) {
                        var _node = new TVclTreeNode(null, null);
                        _node._RutaGeneral.push(Ruta);
                        _node.Text = thirdWordFromRight;
                        _node.TreeParent = this;
                        this.TreeNodes.push(_node);
                        this.This.appendChild(_node.This);
                        _node.This.style.paddingTop = "5px";
                        _node.This.style.paddingBottom = "5px";
                        NodePosition = _node;

                    }

                    if (words.length == (Counter + 1)) {
                        var _node = new TVclTreeNode(null, null);
                        _node._RutaGeneral.push(Ruta);
                        _node.Text = NodeText;
                        _node.TreeParent = this;
                        NodePosition.AddTreeNode(_node);
                        NodePosition = _node;
                        TreeNodeLast = _node;
                        if (inImg.length > 0)
                            NodePosition.Img = inImg;
                    }

                }
                else {
                    for (var CounterNodo = 0; CounterNodo < NodePosition.TreeNodes.length; CounterNodo++) {
                        if (NodePosition.TreeNodes[CounterNodo].Text == thirdWordFromRight) {
                            NodePosition = NodePosition.TreeNodes[CounterNodo];
                            Existe = true;
                        }
                    }
                    if (Existe == false) {
                        var _node = new TVclTreeNode(null, null);
                        _node._RutaGeneral.push(Ruta);
                        _node.Text = thirdWordFromRight;
                        _node.TreeParent = this;
                        NodePosition.AddTreeNode(_node);
                        NodePosition = _node;
                    }
                    else {
                        if (words.length == (Counter + 1)) {
                            var _node = new TVclTreeNode(null, null);
                            _node._RutaGeneral.push(Ruta);
                            _node.Text = NodeText;
                            _node.TreeParent = this;
                            NodePosition.AddTreeNode(_node);
                            NodePosition = _node;
                            TreeNodeLast = _node;
                            _node.This.style.paddingBottom = "0px";
                            if (inImg.length > 0)
                                NodePosition.Img = inImg;
                        }
                    }
                }
            }
        }

        return NodePosition;
    }

    this.TParent = function () {
        return this;
    }.bind(this);
}

var TVclPageProfilerTreeNode = function (ObjectHtml, Id) {
    var _this = this;

    this.This = Uli(ObjectHtml, Id);
    this.This.style.display = "block";
    this.This.style.paddingTop = "5px";
    this.This.style.paddingBottom = "5px";

    this.ThisUL = Uul(this.This, Id);
    $(this.ThisUL).hide();
    this.TreeNodes = new Array();
    this._Tag = null;
    this._RutaGeneral = [];
    this._Text = null;
    this._Imagen = null;
    this._TreeRootNode = null;
    this._IsRoot = false;
    this._Numero = null;
    this._MarginImageText = null;
    this._Link = null;
    this._TreeParent = null;
    //Estado Abierto cerrado
    this._estadoLi = false;
    this._textoBusquedad = null;//texto en busquedad


    Object.defineProperty(this, 'Text', {
        get: function () {
            return this._Text;
        },
        set: function (_Value) {
            this._Text = _Value;
            var _link = document.createElement("a");
            this._Link = _link;
            var _UlTemp = this.ThisUL;
            _link.appendChild(document.createTextNode(this._Text));
            _link.href = "#";
            _link.style.textDecoration = "none";
            //_link.style.display = "block";
            _link.onclick = function (e) {
                if (_this.TreeParent.SePuedeBuscar) {
                    if (_this._textoBusquedad == null && _this._estadoLi == false) {
                        _this._textoBusquedad = "buscando..";
                        var tag = "";
                        var ruta = "";
                        var texto = "";
                        texto = _this.Text;
                        if (_this._IsRoot) {
                            tag = _this.Tag;
                        } else {
                            tag = _this._TreeRootNode.Tag;
                        }
                        var posicion = (_this._RutaGeneral[0]).indexOf(texto);
                        if (posicion > -1) {
                            var nuevaRuta = (_this._RutaGeneral[0]).substring(0, posicion + (texto.length));
                            _this.ExpanderContraerNodo(_this.TreeParent.getTreeNodes(), true, nuevaRuta, texto, tag);
                        }
                        _this._textoBusquedad = null;
                    }
                }
                _this._estadoLi = !_this._estadoLi;
                e.preventDefault();
                $(_UlTemp).toggle("slow"); // mostrar los nodos;
            }
            _link.addEventListener("mouseover", function () {
                this.parentNode.style.backgroundColor = "#2C3B41";
                this.style.color = "white";
            });

            _link.addEventListener("mouseout", function () {
                this.parentNode.style.backgroundColor = "transparent";
                this.style.color = "#2d83a9";
            });
            this.This.insertBefore(_link, this.This.childNodes[0]);
        }
    });

    Object.defineProperty(this, 'Img', {
        get: function () {
            return this._Imagen;
        },
        set: function (_Value) {
            this._Imagen = _Value;
            var _img = document.createElement("img");
            var _UlTemp = this.ThisUL;
            _img.src = _Value;

            if (this._IsRoot) {
                _img.style.marginRight = "10px";
                this.This.insertBefore(_img, this.This.childNodes[0]);
            }
            else {
                _img.style.marginLeft = "10px";
                _img.style.cssFloat = "right";
                _img.style.marginRight = "10px";
                this.This.insertBefore(_img, this.This.childNodes[1]);
            }
        }
    });
    Object.defineProperty(this, 'Tag', {
        get: function () {
            return this._Tag;
        },
        set: function (_Value) {
            this._Tag = _Value;
        }
    });
    Object.defineProperty(this, 'IsRoot', {
        get: function () {
            return this._IsRoot;
        },
        set: function (_Value) {
            this._IsRoot = _Value;
        }
    });
    Object.defineProperty(this, 'Numero', {
        get: function () {
            return this._Numero;
        },
        set: function (_Value) {
            this._Numero = _Value;
            var _label = document.createElement("label");
            _label.appendChild(document.createTextNode(this._Numero));
            this.This.insertBefore(_label, this.This.lastChild);
        }
    });
    Object.defineProperty(this, 'TreeRootNode', {
        get: function () {
            return this._TreeRootNode;
        },
        set: function (_Value) {
            this._TreeRootNode = _Value;
        }
    });
    Object.defineProperty(this, 'Cursor', {
        get: function () {
            return this.This.style.cursor;
        },
        set: function (_Value) {
            this.This.style.cursor = _Value;
        }
    });

    //Object.defineProperty(this, 'onClick', {
    //    get: function () {
    //        return this.This.onclick;
    //    },
    //    set: function (_Value) {
    //        var _ThisTemp = this;
    //        this.This.onclick = function () {
    //            _Value(_ThisTemp);
    //        }
    //        //            this.This.onclick = _Value;
    //    }
    //});

    Object.defineProperty(this, 'Node', {
        get: function () {
            return this.This;
        }
    });
    Object.defineProperty(this, 'NodePadre', {
        get: function () {
            return this;
        }
    });
    Object.defineProperty(this, 'TreeParent', {
        get: function () {
            return this._TreeParent;
        },
        set: function (_Value) {
            this._TreeParent = _Value;
        }
    });

    this.AddTreeNode = function (VclTreeNode) {
        VclTreeNode.TreeParent = this.TreeParent;
        this.TreeNodes.push(VclTreeNode);
        this.ThisUL.appendChild(VclTreeNode.This);
    }
    this.Expand = function () {
        if (this._estadoLi == false) {
            $(this._Link).click();
        }
    }

    this.Contract = function () {
        if (this._estadoLi == true) {
            $(this._Link).click();
        }
    }

    this.ExpanderContraerNodo = function (treeNodesList, opcion, ruta, textoNodo, tagNodo) {
        for (var i = 0; i < treeNodesList.length; i++) {
            var texto = treeNodesList[i].Text;
            //            var rutaTemporal = treeNodesList[i]._RutaGeneral[0];
            //            if (treeNodesList[i]._IsRoot) {
            //                rutaTemporal = texto;
            //            }
            var rutaTemporal = "";

            var posicion = (treeNodesList[i]._RutaGeneral[0]).indexOf(texto);
            if (posicion > -1) {
                rutaTemporal = (treeNodesList[i]._RutaGeneral[0]).substring(0, posicion + (texto.length));
                //                _this.ExpanderContraerNodo(_this.TreeParent.getTreeNodes(), true, nuevaRuta, texto, tag);
            }
            //                    console.log("rutaTemporal=> " + rutaTemporal + " == " + ruta);

            var nameColumTexto = (texto).substring(0, (texto).indexOf(":"));
            var nameColumTextoNodo = (textoNodo).substring(0, (textoNodo).indexOf(":"));
            if (rutaTemporal.length > ruta.length) {
                if (rutaTemporal != null && texto != "" && texto != null && (("" + rutaTemporal.toLowerCase()).indexOf(("" + ruta.toLowerCase())) > -1)) {


                    //                    console.log("rutaTemporal A=> " + rutaTemporal + " == " + ruta);
                    if (textoNodo + "" != "" + texto && nameColumTexto != nameColumTextoNodo) {
                        treeNodesList[i].Expand();
                    } else {
                        treeNodesList[i].Contract();
                    }

                } else {
                    //                    console.log("rutaTemporal B=> " + rutaTemporal + " == " + ruta);
                    treeNodesList[i].Contract();
                }
            } else {
                if (rutaTemporal != null && texto != "" && texto != null && (("" + ruta.toLowerCase()).indexOf(("" + rutaTemporal.toLowerCase())) > -1)) {
                    //                    console.log("rutaTemporal A=> " + rutaTemporal + " == " + ruta);
                    //                    if (textoNodo + "" != "" + texto && texto.length == textoNodo.length) {
                    if (textoNodo + "" != "" + texto && nameColumTexto != nameColumTextoNodo) {
                        treeNodesList[i].Expand();
                    } else {
                        treeNodesList[i].Contract();
                    }

                } else {
                    //                    console.log("rutaTemporal B=> " + rutaTemporal + " == " + ruta);
                    treeNodesList[i].Contract();
                }
            }
            if (treeNodesList[i].TreeNodes != [] || treeNodesList[i].TreeNodes != null) {

                var tag = "";
                if (treeNodesList[i]._IsRoot) {
                    tag = treeNodesList[i].Tag;
                } else {
                    if (treeNodesList[i]._TreeRootNode != null) {
                        tag = treeNodesList[i]._TreeRootNode.Tag;
                    }

                }
                if (tag == tagNodo && textoNodo + "" != "" + texto) {
                    this.ExpanderContraerNodo(treeNodesList[i].TreeNodes, opcion, ruta, textoNodo, tagNodo);
                }
            }
        }

    }

    this.This.onclick.onclick = function () {
        if (_this.TreeParent != null)
        {
            if (_this.TreeParent.OnNodeClick != null)
                _this.TreeParent.OnNodeClick(_this.TreeParent, _this);
        }
            
    }
   
    this.TParent = function () {
        return this;
    }.bind(this);
}
*/