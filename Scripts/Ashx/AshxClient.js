﻿//Comunic.AjaxJson.Client.Methods
Comunic.Ashx.Client.Properties.TGetComunic = function () {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.ResErr = new SysCfg.Error.Properties.TResErr();
    this.response;
}

Comunic.Ashx.Client.Methods.GetComunic = function (infunction, data) {
    var GetComunic = new Comunic.Ashx.Client.Properties.TGetComunic;
    SysCfg.Error.Properties.ResErrfill(GetComunic.ResErr);


    //*******************************************
    $.ajax({
        url: SysCfg.App.Properties.xRaiz + 'Service/ComunicService/' + infunction + '.ashx',
        type: "POST",
        contentType: false,
        processData: false,
        data: data,
        async: false,
        // dataType: "json",
        success: function (response) {
            GetComunic.response = response;
            GetComunic.ResErr.NotError = true;
            GetComunic.ResErr.Mesaje = SysCfg.Error.Properties._ResOK;
        },
        error: function (response) {
            SysCfg.Log.Methods.WriteLog("AshxClient.js Comunic.Ashx.Client.Properties.TGetComunic " + infunction);
            GetComunic.ResErr.Mesaje = response.ErrorMessage;
        }
    });
    return (GetComunic);
}

Comunic.Ashx.Client.Methods.GetComunicP = function (infunction, data) {
    var GetComunic = new Comunic.Ashx.Client.Properties.TGetComunic;
    SysCfg.Error.Properties.ResErrfill(GetComunic.ResErr);


    try {

        var _send = "";
        for (var i = 0; i < data.length; i++) {
            _send = _send + data[i][0] + "=" + data[i][1] + "&";
        }
        //*******************************************
        var xhr = new XMLHttpRequest();
        xhr.open('POST', SysCfg.App.Properties.xRaiz + 'Service/ComunicService/' + infunction + '.ashx', true);
        xhr.responseType = 'arraybuffer';
        xhr.onload = function () {
            if (this.status === 200) {
                var filename = "";
                var disposition = xhr.getResponseHeader('Content-Disposition');
                if (disposition && disposition.indexOf('attachment') !== -1) {
                    var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                    var matches = filenameRegex.exec(disposition);
                    if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
                }
                var type = xhr.getResponseHeader('Content-Type');

                var blob = new Blob([this.response], { type: type });


                if (typeof window.navigator.msSaveBlob !== 'undefined') {
                    // IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
                    window.navigator.msSaveBlob(blob, filename);
                }
                else {
                    var URL = window.URL || window.webkitURL;
                    var downloadUrl = URL.createObjectURL(blob);

                    if (filename) {
                        // use HTML5 a[download] attribute to specify filename
                        var a = document.createElement("a");
                        // safari doesn't support this yet
                        if (typeof a.download === 'undefined') {
                            window.location = downloadUrl;
                        }
                        else {
                            a.href = downloadUrl;
                            a.download = filename;
                            document.body.appendChild(a);
                            a.click();
                        }
                    }
                    else {
                        window.location = downloadUrl;
                    }
                    setTimeout(function () { URL.revokeObjectURL(downloadUrl); }, 100); // cleanup
                }
            }
            else {
                SysCfg.Log.Methods.WriteLog("AshxClient.js Comunic.Ashx.Client.Properties.TGetComunicP Error Asc" + infunction);

            }
        };
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhr.send(_send);
        GetComunic.response = new Array(0);
        GetComunic.ResErr.NotError = true;
        GetComunic.ResErr.Mesaje = SysCfg.Error.Properties._ResOK;
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("AshxClient.js Comunic.Ashx.Client.Properties.TGetComunicP " + infunction);
        GetComunic.ResErr.Mesaje = e.message;
    }

    return (GetComunic);
}
