﻿


Comunic.Ashx.Client.Methods.GetSDfile = function (FileType, FileOperationType, IDCode, NameFile, ByteFile) {
    GetSDfile = new Comunic.Properties.TGetSDfile();
    switch (FileOperationType) {
        case Comunic.Properties.TFileOperationType._FileWrite:
            var data = new FormData();
            data.append(NameFile, ByteFile);
            data.append('FileType', FileType.value);
            data.append('FileOperationType', FileOperationType.value);
            data.append('IDCode', IDCode);
            data.append('NumSesion', SysCfg.App.Methods.Cfg_WindowsTask.NumSesion);
            data.append('NameFile', NameFile);
            /*data.append('NameFile', ByteFile);*/
            /*data.append('ByteFile', ByteFile);*/
            GetComunic = Comunic.Ashx.Client.Methods.GetComunic("GetSDfile", data);
            break;
        case Comunic.Properties.TFileOperationType._FileRead:
            var data = new Array(0);
            data.push(['FileType', FileType.value]);
            data.push(['FileOperationType', FileOperationType.value]);
            data.push(['IDCode', IDCode]);
            data.push(['NumSesion', SysCfg.App.Methods.Cfg_WindowsTask.NumSesion]);
            data.push(['NameFile', NameFile]);
            
            /*data.append('NameFile', ByteFile);*/
            /*data.append('ByteFile', ByteFile);*/
            GetComunic = Comunic.Ashx.Client.Methods.GetComunicP("GetSDfile", data);
            break;
        case Comunic.Properties.TFileOperationType._FileDelete:
            break;
        case Comunic.Properties.TFileOperationType._FileUpdate:
            break;
        default:
            break;
    }



    if (GetComunic.ResErr.NotError) {
        Comunic.AjaxJson.Client.Methods.CopyResErr(GetSDfile.ResErr, GetComunic.ResErr);//GetComunic.response.ResErr
        if (GetSDfile.ResErr.NotError) {
            switch (FileOperationType) {
                case Comunic.Properties.TFileOperationType._FileWrite:
                    GetSDfile.IDCode = GetComunic.response;
                    GetSDfile.ByteFile = new Array(0);
                    break;
                case Comunic.Properties.TFileOperationType._FileRead:
                    GetSDfile.ByteFile = GetComunic.response;
                    GetSDfile.IDCode = IDCode;
                    break;
                case Comunic.Properties.TFileOperationType._FileDelete:
                    GetSDfile.ByteFile = new Array(0);
                    GetSDfile.IDCode = IDCode;
                    break;
                case Comunic.Properties.TFileOperationType._FileUpdate:
                    GetSDfile.ByteFile = new Array(0);
                    GetSDfile.IDCode = IDCode;
                    break;
                default:
                    break;
            }
        }
        GetSDfile.FileType = FileType;//Comunic.Properties.TFileType.GetEnum(GetComunic.response.FileType);
        GetSDfile.FileOperationType = FileOperationType;//Comunic.Properties.TFileOperationType.GetEnum(GetComunic.response.FileOperationType);
        //GetSDfile.IDCode = GetComunic.response.IDCode;
        //GetSDfile.ByteFile = GetComunic.response.ByteFile;
    }
    else {
        GetSDfile.ResErr.Mesaje = GetComunic.ResErr.Mesaje;
    }
    return (GetSDfile);
}