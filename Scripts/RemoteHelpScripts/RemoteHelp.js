﻿//Version 1 Jaimes 2018/03/14
ItHelpCenter.TUfrRemoteHelp = function (inObject, incallback, inObjectBy) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.ObjectHtml = inObject;
    this.CallbackModalResult = incallback;
    this.TitleBarImgs = null;
    this.NavVisible = false;
    this.IconsTitleBar = Array(27);
    this.Grupos = Array(7);
    this.DivBody = null;
    this._Visible = true;
    this.NavBar_RH = null;
    this.DivHeaderC = null;
    this.RHOptions = new Array();
    this._EnabledModal = false;
    this.Modal = null;
    this.OnRemoteClick = null;
    this.DivBodyC = null;
    this.RHREQUESTOPTIONS = null;
    this.ShowGroups = false;
    this.hasPermissions = true;
    this.GPQUERY = null;

    try {
        this.DivBody=  inObjectBy
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("RemoteHelp.js ItHelpCenter.TUfrRemoteHelp", e);
    }


    //TRADUCCION          
    this.Mythis = "TUfrRemoteHelp";
   
    UsrCfg.Traslate.GetLangText(_this.Mythis, "NavTitle", "Remote Help");
     UsrCfg.Traslate.GetLangText(_this.Mythis, "btn_OpeRemotas_1", "Intel vPro technology");
     UsrCfg.Traslate.GetLangText(_this.Mythis, "btn_OpeRemotas_2", "Survey");
     UsrCfg.Traslate.GetLangText(_this.Mythis, "btn_OpeRemotas_3", "Generate Inventory");
     UsrCfg.Traslate.GetLangText(_this.Mythis, "btn_OpeRemotas_4", "Update Agent");
     UsrCfg.Traslate.GetLangText(_this.Mythis, "btn_OpeRemotas_5", "Change ID");
     UsrCfg.Traslate.GetLangText(_this.Mythis, "btn_OpeRemotas_6", "Look Dangerous");
     UsrCfg.Traslate.GetLangText(_this.Mythis, "Lab_Connection_1", "Internet Connection");
     UsrCfg.Traslate.GetLangText(_this.Mythis, "Lab_Connection_2", "Connect by Host");
     UsrCfg.Traslate.GetLangText(_this.Mythis, "btn_Oth_Options_1", "Delete PC");
     UsrCfg.Traslate.GetLangText(_this.Mythis, "btn_Oth_Options_2", "Information");
     UsrCfg.Traslate.GetLangText(_this.Mythis, "btn_Oth_Options_3", "Run S.D. File");
     UsrCfg.Traslate.GetLangText(_this.Mythis, "btn_Policies_1", "Update Policies");
     UsrCfg.Traslate.GetLangText(_this.Mythis, "btn_Policies_2", "Restart Allowed");
     UsrCfg.Traslate.GetLangText(_this.Mythis, "btn_Files_1", "Sending Files and Orders");
     UsrCfg.Traslate.GetLangText(_this.Mythis, "btn_Files_2", "Run Locally");
     UsrCfg.Traslate.GetLangText(_this.Mythis, "btn_Rem_Control_1", "Power on PC");
     UsrCfg.Traslate.GetLangText(_this.Mythis, "btn_Rem_Control_2", "Use Browser");
     UsrCfg.Traslate.GetLangText(_this.Mythis, "btn_Rem_Control_3", "Take Control");
     UsrCfg.Traslate.GetLangText(_this.Mythis, "Lab_Rem_Control_1", "Just See");
     UsrCfg.Traslate.GetLangText(_this.Mythis, "Lab_Rem_Control_2", "Lock Keyboard");
     UsrCfg.Traslate.GetLangText(_this.Mythis, "Lab_Rem_Control_3", "Do not Ask for Permission");
     UsrCfg.Traslate.GetLangText(_this.Mythis, "Lab_Rem_Control_4", "Low Bandwidth");
     UsrCfg.Traslate.GetLangText(_this.Mythis, "Lab_Rem_Control_5", "Record Session");
     UsrCfg.Traslate.GetLangText(_this.Mythis, "btn_Messaging_1", "Chat");
     UsrCfg.Traslate.GetLangText(_this.Mythis, "btn_Messaging_2", "Voice");
     UsrCfg.Traslate.GetLangText(_this.Mythis, "btn_Messaging_3", "Send Message");
     UsrCfg.Traslate.GetLangText(_this.Mythis, "RHelp_Item1", "Remote Operations");
     UsrCfg.Traslate.GetLangText(_this.Mythis, "RHelp_Item2", "Connection");
     UsrCfg.Traslate.GetLangText(_this.Mythis, "RHelp_Item3", "Other Options");
     UsrCfg.Traslate.GetLangText(_this.Mythis, "RHelp_Item4", "Policies");
     UsrCfg.Traslate.GetLangText(_this.Mythis, "RHelp_Item5", "File");
     UsrCfg.Traslate.GetLangText(_this.Mythis, "RHelp_Item6", "Remote Control");
     UsrCfg.Traslate.GetLangText(_this.Mythis, "RHelp_Item7", "Messaging");
     UsrCfg.Traslate.GetLangText(_this.Mythis, "Lab_Group", "Group ");
   
 
     UsrCfg.Traslate.GetLangText(_this.Mythis, "btnRHOutput_1", "Atis");
     UsrCfg.Traslate.GetLangText(_this.Mythis, "btnRHOutput_2", "ItHelpCenter");
     UsrCfg.Traslate.GetLangText(_this.Mythis, "btnRHOutput_3", "Remote Help");
     UsrCfg.Traslate.GetLangText(_this.Mythis, "RB_RHOutput_1", "In Browser");
     UsrCfg.Traslate.GetLangText(_this.Mythis, "RB_RHOutput_2", "In Win32");
     UsrCfg.Traslate.GetLangText(_this.Mythis, "ThisModal", "Remote Help");                         
     UsrCfg.Traslate.GetLangText(_this.Mythis, " modal.AddHeaderContent ", "Remote Help");

     //Agregar la hoja de estilos           
    _this.AddStyleFile(SysCfg.App.Properties.xRaiz + "Css/RemoteHelpCss/RemoteHelp.css", function () {              
        _this.importarScript(SysCfg.App.Properties.xRaiz + "Scripts/Persistence/RemoteHelp/RemoteHelpProperties.js", function () {                  
            _this.InitializeComponent();
        }, function (e) {
        });
    }, function (e) {
    });   

    //PROPERTIES
    Object.defineProperty(this, 'Visible', {
        get: function () {
            return this._Visible;
        },
        set: function (_Value) {            
            this._Visible = _Value;
            try {
                if (_this._Visible == true) {
                    $(this.DivHeaderC).css('display', "block");
                } else {
                    $(this.DivHeaderC).css('display', "none");
                }
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("RemoteHelp.js ItHelpCenter.TUfrRemoteHelp", e);
            }
        }
    });

    Object.defineProperty(this, 'EnabledModal', {
        get: function () {
            return this._EnabledModal;
        },
        set: function (_Value) {            
            this._EnabledModal = _Value;
        }
    });
    
}

////
ItHelpCenter.TUfrRemoteHelp.prototype.InitializeComponent = function () {
    var _this = this.TParent();    

    _this.RHREQUESTOPTIONS = new ItHelpCenter.RemoteHelp.TRHREQUESTOPTIONS;

    $(_this.ObjectHtml).html("");

    //Estructura de la barra title
    DivHeader = new TVclStackPanel(_this.ObjectHtml, "RemoteHelp_Header", 1, [[12], [12], [12], [12], [12]]);
    _this.DivHeaderC = DivHeader.Column[0].This;
    $(_this.DivHeaderC).addClass('Remote_Help_Header');

    //divs de los grupos
    var DivHeaderGrps = new TVclStackPanel(_this.DivHeaderC, "DivHeaderGrps", 9, [[1, 1, 1, 1, 1, 1, 1, 1, 1], [1, 1, 1, 1, 1, 1, 1, 1, 1], [1, 1, 1, 1, 1, 1, 1, 1, 1], [1, 1, 1, 1, 1, 1, 1, 1, 1], [1, 1, 1, 1, 1, 1, 1, 1, 1]]);
    var DivHeaderHd = DivHeaderGrps.Column[0].This;

    //Si tiene la opcion de visible false
    if (_this._Visible == false) {
        $(_this.DivHeaderC).css('display', "none");
    }

    var DivHeaderGrp0 = DivHeaderGrps.Column[1].This;
    _this.TitleBarImgs = DivHeaderGrp0; //div donde estan las imagenes seleccionadas
    $(_this.TitleBarImgs).css("display", "none");
    $(_this.TitleBarImgs).css("padding-left", "0px");
    var DivHeaderGrp1 = DivHeaderGrps.Column[2].This;
    var DivHeaderGrp2 =  DivHeaderGrps.Column[3].This;
    var DivHeaderGrp3 =  DivHeaderGrps.Column[4].This;
    var DivHeaderGrp4 =  DivHeaderGrps.Column[5].This;
    var DivHeaderGrp5 =  DivHeaderGrps.Column[6].This;
    var DivHeaderGrp6 =  DivHeaderGrps.Column[7].This;
    var DivHeaderGrp7 = DivHeaderGrps.Column[8].This;

    $(DivHeaderHd).addClass('Remote_Help_HeaderImg');
    $(DivHeaderGrp0).addClass('Remote_Help_HeaderImg');
    $(DivHeaderGrp1).addClass('Remote_Help_HeaderGrups');
    $(DivHeaderGrp2).addClass('Remote_Help_HeaderGrups');
    $(DivHeaderGrp3).addClass('Remote_Help_HeaderGrups');
    $(DivHeaderGrp4).addClass('Remote_Help_HeaderGrups');
    $(DivHeaderGrp5).addClass('Remote_Help_HeaderGrups');
    $(DivHeaderGrp6).addClass('Remote_Help_HeaderGrups');
    $(DivHeaderGrp7).addClass('Remote_Help_HeaderGrups');
    
    var DivHeaderGrp1_S = new TVclStackPanel(DivHeaderGrp1, "DivHeaderGrp1_S", 2, [[12,12], [12,12], [12,12], [12,12], [12,12]]);
    var DivHeaderGrp1_1 = DivHeaderGrp1_S.Column[0].This; //imagen del grupo
    var DivHeaderGrp1_2 = DivHeaderGrp1_S.Column[1].This; //imagenes de los elementos del grupo 
    var DivHeaderGrp2_S = new TVclStackPanel(DivHeaderGrp2, "DivHeaderGrp2_S", 2, [[12, 12], [12, 12], [12, 12], [12, 12], [12, 12]]);
    var DivHeaderGrp2_1 = DivHeaderGrp2_S.Column[0].This;
    var DivHeaderGrp2_2 = DivHeaderGrp2_S.Column[1].This;
    var DivHeaderGrp3_S = new TVclStackPanel(DivHeaderGrp3, "DivHeaderGrp3_S", 2, [[12, 12], [12, 12], [12, 12], [12, 12], [12, 12]]);
    var DivHeaderGrp3_1 = DivHeaderGrp3_S.Column[0].This;
    var DivHeaderGrp3_2 = DivHeaderGrp3_S.Column[1].This;
    var DivHeaderGrp4_S = new TVclStackPanel(DivHeaderGrp4, "DivHeaderGrp4_S", 2, [[12, 12], [12, 12], [12, 12], [12, 12], [12, 12]]);
    var DivHeaderGrp4_1 = DivHeaderGrp4_S.Column[0].This;
    var DivHeaderGrp4_2 = DivHeaderGrp4_S.Column[1].This;
    var DivHeaderGrp5_S = new TVclStackPanel(DivHeaderGrp5, "DivHeaderGrp5_S", 2, [[12, 12], [12, 12], [12, 12], [12, 12], [12, 12]]);
    var DivHeaderGrp5_1 = DivHeaderGrp5_S.Column[0].This;
    var DivHeaderGrp5_2 = DivHeaderGrp5_S.Column[1].This;
    var DivHeaderGrp6_S = new TVclStackPanel(DivHeaderGrp6, "DivHeaderGrp6_S", 2, [[12, 12], [12, 12], [12, 12], [12, 12], [12, 12]]);
    var DivHeaderGrp6_1 = DivHeaderGrp6_S.Column[0].This;
    var DivHeaderGrp6_2 = DivHeaderGrp6_S.Column[1].This;
    var DivHeaderGrp7_S = new TVclStackPanel(DivHeaderGrp7, "DivHeaderGrp7_S", 2, [[12, 12], [12, 12], [12, 12], [12, 12], [12, 12]]);
    var DivHeaderGrp7_1 = DivHeaderGrp7_S.Column[0].This;
    var DivHeaderGrp7_2 = DivHeaderGrp7_S.Column[1].This;
   
    $(DivHeaderGrp1).css("display", "none");
    $(DivHeaderGrp2).css("display", "none");
    $(DivHeaderGrp3).css("display", "none");
    $(DivHeaderGrp4).css("display", "none");
    $(DivHeaderGrp5).css("display", "none");
    $(DivHeaderGrp6).css("display", "none");
    $(DivHeaderGrp7).css("display", "none");

    $(DivHeaderGrp1_2).addClass('Remote_Help_HeaderGrups_Sub');
    $(DivHeaderGrp2_2).addClass('Remote_Help_HeaderGrups_Sub');
    $(DivHeaderGrp3_2).addClass('Remote_Help_HeaderGrups_Sub');
    $(DivHeaderGrp4_2).addClass('Remote_Help_HeaderGrups_Sub');
    $(DivHeaderGrp5_2).addClass('Remote_Help_HeaderGrups_Sub');
    $(DivHeaderGrp6_2).addClass('Remote_Help_HeaderGrups_Sub');
    $(DivHeaderGrp7_2).addClass('Remote_Help_HeaderGrups_Sub');

    //imagenes de los grupos
    var img1 = new Uimg(DivHeaderGrp1_1, "TitleBar_img1", "image/16/Handtool.png", "");
    $(img1).css("cursor", "pointer");  
    var activation1 = false;
    img1.addEventListener("click", function () {
        if (activation1 == true) {
            $(DivHeaderGrp1_2).css("display", "none");
            activation1 = false;
        } else {
            $(DivHeaderGrp1_2).css("display", "block");
            activation1 = true;
        }
    });
    var img2 = new Uimg(DivHeaderGrp2_1, "TitleBar_img2", "image/16/connection.png", "");
    $(img2).css("cursor", "pointer");  
    var activation2 = false;
    img2.addEventListener("click", function () {
        if (activation2 == true) {
            $(DivHeaderGrp2_2).css("display", "none");
            activation2 = false;
        } else {
            $(DivHeaderGrp2_2).css("display", "block");
            activation2 = true;
        }
    });
    var img3 = new Uimg(DivHeaderGrp3_1, "TitleBar_img3", "image/16/Options.png", "");
    $(img3).css("cursor", "pointer");    
    var activation3 = false;
    img3.addEventListener("click", function () {
        if (activation3 == true) {
            $(DivHeaderGrp3_2).css("display", "none");
            activation3 = false;
        } else {
            $(DivHeaderGrp3_2).css("display", "block");
            activation3 = true;
        }
    });
    var img4 = new Uimg(DivHeaderGrp4_1, "TitleBar_img4", "image/16/Rulers.png", "");
    $(img4).css("cursor", "pointer");   
    var activation4 = false;
    img4.addEventListener("click", function () {
        if (activation4 == true) {
            $(DivHeaderGrp4_2).css("display", "none");
            activation4 = false;
        } else {
            $(DivHeaderGrp4_2).css("display", "block");
            activation4 = true;
        }
    });
    var img5 = new Uimg(DivHeaderGrp5_1, "TitleBar_img5", "image/16/document.png", "");
    $(img5).css("cursor", "pointer");   
    var activation5 = false;
    img5.addEventListener("click", function () {
        if (activation5 == true) {
            $(DivHeaderGrp5_2).css("display", "none");
            activation5 = false;
        } else {
            $(DivHeaderGrp5_2).css("display", "block");
            activation5 = true;
        }
    });
    var img6 = new Uimg(DivHeaderGrp6_1, "TitleBar_img6", "image/16/Computer.png", "");
    $(img6).css("cursor", "pointer");    
    var activation6 = false;
    img6.addEventListener("click", function () {
        if (activation6 == true) {
            $(DivHeaderGrp6_2).css("display", "none");
            activation6 = false;
        } else {
            $(DivHeaderGrp6_2).css("display", "block");
            activation6 = true;
        }
    });
    var img7 = new Uimg(DivHeaderGrp7_1, "TitleBar_img7", "image/16/comment.png", "");
    $(img7).css("cursor", "pointer");    
    var activation7 = false;
    img7.addEventListener("click", function () {
        if (activation7 == true) {
            $(DivHeaderGrp7_2).css("display", "none");
            activation7 = false;
        } else {
            $(DivHeaderGrp7_2).css("display", "block");
            activation7 = true;
        }
    });

    //Estructura del Cuerpo
    if (this.DivBody != null) {
        var DivBody = new TVclStackPanel(_this.DivBody, "RemoteHelp_Body", 1, [[12], [12], [12], [12], [12]]);
    } else {
        var DivBody = new TVclStackPanel(_this.ObjectHtml, "RemoteHelp_Body", 1, [[12], [12], [12], [12], [12]]);
    }

    _this.DivBodyC = DivBody.Column[0].This;
    $(_this.DivBodyC).css("display", "none");
    $(_this.DivBodyC).addClass('Remote_Help_Body');

    //BARRA ENCABEZADO DE REMOTE HELP
    var img = new Uimg(DivHeaderHd, "TitleBar_img_RemHelp", "image/16/Monitor.png", "");
    $(img).css("cursor", "pointer");
    $(img).css("width", "24px");
    $(img).css("padding", "4px");
    img.addEventListener("click", function () {       
        var FilasSeleccionadas = _this.GridCFView.VclGridCF.GetSelectedRows();
        if (FilasSeleccionadas.length > 0) {
            if (_this.NavVisible == true) {
                _this.NavVisible = false;
                $(_this.DivBodyC).slideToggle("slow", function () {
                    $(_this.DivBodyC).css("display", "none");
                });
                if (_this._EnabledModal) {

                }
            } else {
                _this.NavVisible = true;
                $(_this.DivBodyC).slideToggle("slow", function () {
                    $(_this.DivBodyC).css("display", "block");
                });
                if (_this._EnabledModal) {
                    _this.Modal.ShowModal();
                }
            }
        }

        //_this.SendRHOptions(); //ENviar los valores de los cheks
            
    });

    ///CREACION DEL NAV 
    if (_this._EnabledModal) {
        _this.Modal = new TVclModal(document.body, null, "");
        //_this.Modal.Width = "auto";
        _this.Modal.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.Mythis, " modal.AddHeaderContent "));
        _this.Modal.Body.This.id = UsrCfg.Traslate.GetLangText(_this.Mythis, "ThisModal");
        _this.NavBar_RH = new VclNavBar(_this.Modal.Body.This, "Remote_Help_Nav");
        _this.Modal.OnAfterCloseModal = function () { _this.NavVisible = false; }
        
    } else {
        _this.NavBar_RH = new VclNavBar(_this.DivBodyC, "Remote_Help_Nav");
    }
     

    $(_this.NavBar_RH._Container).addClass('Remote_Help_Nav');     
    //_this.NavBar_RH.Title = UsrCfg.Traslate.GetLangText(_this.Mythis, "NavTitle");
   // _this.NavBar_RH.ImgTitle = "image/16/Monitor.png";

    //Elemento para visualizar en el area de trabajo para Operaciones Remotas 
    // 0 conexion header=======================================================================================================================
    //Contenedor Item 0
    var ConexionHd = new TVclStackPanel("", "ConexionHd", 1, [[12], [12], [12], [12], [12]]);
    var ConexionHdC = ConexionHd.Column[0].This;

    var ConexionHdCS = new TVclStackPanel(ConexionHdC, "ConexionHdCS", 3, [[12, 12, 12], [12, 12, 12], [5, 5, 2], [5, 5, 2], [5, 5, 2]]);
    var ConexionHdC_1 = ConexionHdCS.Column[0].This;
    var ConexionHdC_2 = ConexionHdCS.Column[1].This;
    var ConexionHdC_3 = ConexionHdCS.Column[2].This;

    //botones
    $(ConexionHdC_1).css("text-align", "center");
    $(ConexionHdC_2).css("text-align", "center");
    $(ConexionHdC_3).css("text-align", "center");
    $(ConexionHdC_1).css("overflow", "hidden");
    $(ConexionHdC_2).css("overflow", "hidden");
    $(ConexionHdC_3).css("overflow", "hidden");
    $(ConexionHdC_1).css("margin-bottom", "5px"); 
    $(ConexionHdC_2).css("margin-bottom", "5px");
    $(ConexionHdC_3).css("margin-bottom", "5px");

    var btnCnxHed_1 = new TVclButton(ConexionHdC_1, "btnCnxHed_1");
    btnCnxHed_1.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lab_Connection_1")
    btnCnxHed_1.VCLType = TVCLType.BS;
    btnCnxHed_1.Src = "image/16/Web-management.png";
    btnCnxHed_1.ImageLocation = "BeforeText";
    btnCnxHed_1.AlignElement = "Left"; //"Right";
    btnCnxHed_1.onClick = function myfunction() {       
        _this.ChkActivation(chkBoxConex_1, imgCnx1, btnCnxHed_1, false);
        //_this.Conn_Btn1();
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, chkBoxConex_1, "Chek");
        }
        _this.RHREQUESTOPTIONS.InternetConnection = chkBoxConex_1.Checked;
    }    
   // $(btnCnxHed_1.This).addClass('RHButton');

    var btnCnxHed_2 = new TVclButton(ConexionHdC_2, "btnCnxHed_2");
    btnCnxHed_2.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lab_Connection_2");
    btnCnxHed_2.VCLType = TVCLType.BS;
    btnCnxHed_2.Src = "image/16/Laptop.png";
    btnCnxHed_2.ImageLocation = "BeforeText";
    btnCnxHed_2.AlignElement = "Left"; //"Right";
    btnCnxHed_2.onClick = function myfunction() {
        _this.ChkActivation(chkBoxConex_2, imgCnx2, btnCnxHed_2, false);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, chkBoxConex_2, "Chek");
        }
        _this.RHREQUESTOPTIONS.ConnectbyHost = chkBoxConex_2.Checked;
    } 
  //  $(btnCnxHed_2.This).addClass('RHButton');

    //BOTON DE SETTING
    var btnHed_Setting = new TVclButton(ConexionHdC_3, "btnHed_3");
    btnHed_Setting.VCLType = TVCLType.BS;
    btnHed_Setting.Src = "image/16/database.png";   
    btnHed_Setting.onClick = function myfunction() { 
        if (_this.ShowGroups) {
            _this.ShowGroups = false;
            //ocultar div de grups
            for (var i = 0; i < _this.Grupos.length - 1; i++) {                
                idx = i + 1;              
                $(_this.Grupos[idx][5]).css("display", "none");
                for (var j = 0; j < _this.Grupos[idx][3].length; j++) {                   
                    $(_this.Grupos[idx][3][j].Column[0].This).css("display", "none");
                }                
            }            
        } else {
            _this.ShowGroups = true;
            //mostrar div de grups
            for (var i = 0; i < _this.Grupos.length - 1; i++) {
                idx = i + 1;
                $(_this.Grupos[idx][5]).css("display", "inline");
                for (var j = 0; j < _this.Grupos[idx][3].length; j++) {
                    $(_this.Grupos[idx][3][j].Column[0].This).css("display", "inline");
                    $(_this.Grupos[idx][3][j].Column[0].This).css("text-align", "center");
                    $(_this.Grupos[idx][3][j].Column[0].This).css("padding-top", "5px");
                }                
            }
        }        
    }

    // 1 Operaciones Remotas=======================================================================================================================
    //grupo  
    _this.Grupos[1] = new Array(5);
    _this.Grupos[1][0] = false; // indica si se esta a grupado  o no en la barra titulo
    _this.Grupos[1][1] = DivHeaderGrp1_2; //indica en cual div se agurpan las imagenes    
    _this.Grupos[1][2] = DivHeaderGrp1; //indica el div donde va la imagen del grupo
    _this.Grupos[1][3] = new Array(6); //div donde estan los botones del grupo  
    _this.Grupos[1][4] = true; //indica si se debe mostrar este item o no
    _this.Grupos[1][5] = null; //Div del group general 
    
    //Contenedor Item 1
    var OperacionesR = new TVclStackPanel("", "OperacionesR", 1, [[12], [12], [12], [12], [12]]);
    var OperacionesRC = OperacionesR.Column[0].This;    

    //div group Operaciones Remotas
    var OpeR_Div0 = new TVclStackPanel(OperacionesRC, "OperacionesR_Div0", 1, [[12], [12], [12], [12], [12]]);
    var OpeR_Div0C = OpeR_Div0.Column[0].This;
    _this.Grupos[1][5] = OpeR_Div0C; //Div del group general 

    var Lab_chkBoxOpeR_Div0 = new Vcllabel(OpeR_Div0C, "Lab_chkBoxOpeR_Div0", TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "Lab_Group"));
    Lab_chkBoxOpeR_Div0.style.fontWeight = "normal";
    var ChkBoxOpeR_Div0 = new TVclInputcheckbox(OpeR_Div0C, "ChkBoxOpeR_Div0");
    ChkBoxOpeR_Div0.VCLType = TVCLType.BS;
    ChkBoxOpeR_Div0.onClick = function myfunction() {
        if (ChkBoxOpeR_Div0.Checked == false) {
            _this.Grupos[1][0] = false;
        } else {
            _this.Grupos[1][0] = true;
        }
        _this.AddIconsTitleBar();
    }   
    $(OpeR_Div0C).css('border', "1px solid #c4c4c4");  
    $(Lab_chkBoxOpeR_Div0).css('margin-right', "5px");
    

    //Boton 1--------------------------------------------------------------------------------------------------------------------------
    var OpeR_Div1 = new TVclStackPanel(OperacionesRC, "OperacionesR_Div1", 2, [[1, 11], [1, 11], [1, 11], [1, 11], [1, 11]]);
    var OpeR_Div1_1C = OpeR_Div1.Column[0].This;
    var OpeR_Div1_2C = OpeR_Div1.Column[1].This;    
    $(OpeR_Div1_2C).addClass('DivText');

    _this.Grupos[1][3][0] = OpeR_Div1;
    $(OpeR_Div1.Row.This).css("position", "relative");
    //-check group
    $(OpeR_Div1_1C).addClass('RH_checkZone');
    var chkBoxOpeR_Div1 = new TVclInputcheckbox(OpeR_Div1_1C, "chkBoxOpeR_Div1");
    chkBoxOpeR_Div1.VCLType = TVCLType.BS;
    chkBoxOpeR_Div1.onClick = function myfunction() {       
        if (chkBoxOpeR_Div1.Checked == false) {
            _this.IconsTitleBar[1][0] = false;           
        } else {         
            _this.IconsTitleBar[1][0] = true;           
        }
        _this.AddIconsTitleBar();
    }
    
    //-boton del DIV
    var btnOpeR_1 = new TVclButton(OpeR_Div1_2C, "btnOpeR_1");
    btnOpeR_1.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btn_OpeRemotas_1");
    btnOpeR_1.VCLType = TVCLType.BS;
    btnOpeR_1.Src = "image/16/Drive.png";
    btnOpeR_1.ImageLocation = "BeforeText";
    btnOpeR_1.AlignElement = "Left"; //"Right";
    btnOpeR_1.onClick = function myfunction() {           
        _this.BtnActivation(btnOpeR_1, imgOpeR1);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, btnOpeR_1, "Buttom");
        }
    }
    btnOpeR_1.This.style.width = "80%";
    btnOpeR_1.This.style.marginLeft = "10%";
    OpeR_Div1_2C.style.padding = "5px"; 

    //-crear imagen para el titleBar
    var imgOpeR1 = new Uimg("", "TitleBar_img_btnOpeR_1", "image/16/Drive.png", "");
    $(imgOpeR1).css("cursor", "pointer");
    $(imgOpeR1).addClass('RH_imgBarTitle');
    imgOpeR1.addEventListener("click", function () {        
        _this.BtnActivation(btnOpeR_1, imgOpeR1);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, btnOpeR_1, "Buttom");
        }
    });
    //-agregar imagen a array y añadir a la barra titulo
    _this.IconsTitleBar[1] = new Array(3);
    _this.IconsTitleBar[1][0] = false;
    _this.IconsTitleBar[1][1] = imgOpeR1;
    _this.IconsTitleBar[1][2] = 1; //indice del grupo al que pertenece esta imagen

    //Boton 2--------------------------------------------------------------------------------------------------------------------------
    var OpeR_Div2 = new TVclStackPanel(OperacionesRC, "OperacionesR_Div2", 2, [[1, 11], [1, 11], [1, 11], [1, 11], [1, 11]]);
    var OpeR_Div2_1C = OpeR_Div2.Column[0].This;
    var OpeR_Div2_2C = OpeR_Div2.Column[1].This;
    $(OpeR_Div2_2C).addClass('DivText');
      

    _this.Grupos[1][3][1] = OpeR_Div2;
    $(OpeR_Div2.Row.This).css("position", "relative");
    //-check group
    $(OpeR_Div2_1C).addClass('RH_checkZone');
    var chkBoxOpeR_Div2 = new TVclInputcheckbox(OpeR_Div2_1C, "chkBoxOpeR_Div2");
    chkBoxOpeR_Div2.VCLType = TVCLType.BS;
    chkBoxOpeR_Div2.onClick = function myfunction() {
        if (chkBoxOpeR_Div2.Checked == false) {
            _this.IconsTitleBar[2][0] = false;
        } else {
            _this.IconsTitleBar[2][0] = true;
        }
        _this.AddIconsTitleBar();
    }
    //-boton    
    var btnOpeR_2 = new TVclButton(OpeR_Div2_2C, "btnOpeR_2");
    btnOpeR_2.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btn_OpeRemotas_2");
    btnOpeR_2.VCLType = TVCLType.BS;
    btnOpeR_2.ImageLocation = "BeforeText";
    btnOpeR_2.AlignElement = "Left";
    btnOpeR_2.Src = "image/16/checklist.png";
    btnOpeR_2.onClick = function myfunction() {
        _this.BtnActivation(btnOpeR_2, imgOpeR2);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, btnOpeR_2, "Buttom");
        }
    }
    btnOpeR_2.This.style.width = "80%";
    btnOpeR_2.This.style.marginLeft = "10%";
    OpeR_Div2_2C.style.padding = "5px";
    //-crear imagen para el titleBar
    var imgOpeR2 = new Uimg("", "TitleBar_img_btnOpeR_2", "image/16/checklist.png", "");
    $(imgOpeR2).css("cursor", "pointer");
    $(imgOpeR2).addClass('RH_imgBarTitle');
    imgOpeR2.addEventListener("click", function () {
        _this.BtnActivation(btnOpeR_2, imgOpeR2);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, btnOpeR_2, "Buttom");
        }
    });
    //-agregar imagen a array y añadir a la barra titulo
    _this.IconsTitleBar[2] = new Array(3);
    _this.IconsTitleBar[2][0] = false;
    _this.IconsTitleBar[2][1] = imgOpeR2;
    _this.IconsTitleBar[2][2] = 1;

    //boton 3--------------------------------------------------------------------------------------------------------------------------
    var OpeR_Div3 = new TVclStackPanel(OperacionesRC, "OperacionesR_Div3", 2, [[1, 11], [1, 11], [1, 11], [1, 11], [1, 11]]);
    var OpeR_Div3_1C = OpeR_Div3.Column[0].This;
    var OpeR_Div3_2C = OpeR_Div3.Column[1].This;
    $(OpeR_Div3_2C).addClass('DivText');

    _this.Grupos[1][3][2] = OpeR_Div3;
    $(OpeR_Div3.Row.This).css("position", "relative");
    //-check group
    $(OpeR_Div3_1C).addClass('RH_checkZone');
    var chkBoxOpeR_Div3 = new TVclInputcheckbox(OpeR_Div3_1C, "chkBoxOpeR_Div3");
    chkBoxOpeR_Div3.VCLType = TVCLType.BS;
    chkBoxOpeR_Div3.onClick = function myfunction() {
        if (chkBoxOpeR_Div3.Checked == false) {
            _this.IconsTitleBar[3][0] = false;
        } else {
            _this.IconsTitleBar[3][0] = true;
        }
        _this.AddIconsTitleBar();
    }
    //-boton   
    var btnOpeR_3 = new TVclButton(OpeR_Div3_2C, "btnOpeR_3");
    btnOpeR_3.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btn_OpeRemotas_3");
    btnOpeR_3.VCLType = TVCLType.BS;
    btnOpeR_3.Src = "image/16/Product.png";
    btnOpeR_3.ImageLocation = "BeforeText";
    btnOpeR_3.AlignElement = "Left";
    btnOpeR_3.onClick = function myfunction() {
        _this.BtnActivation(btnOpeR_3, imgOpeR3);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, btnOpeR_3, "Buttom");
        }
    }
    btnOpeR_3.This.style.width = "80%";
    btnOpeR_3.This.style.marginLeft = "10%";
    OpeR_Div3_2C.style.padding = "5px";
    //-crear imagen para el titleBar
    var imgOpeR3 = new Uimg("", "TitleBar_img_btnOpeR_3", "image/16/Product.png", "");
    $(imgOpeR3).css("cursor", "pointer");
    $(imgOpeR3).addClass('RH_imgBarTitle');
    imgOpeR3.addEventListener("click", function () {
        _this.BtnActivation(btnOpeR_3, imgOpeR3);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, btnOpeR_3, "Buttom");
        }
    });
    //-agregar imagen a array y añadir a la barra titulo
    _this.IconsTitleBar[3] = new Array(3);
    _this.IconsTitleBar[3][0] = false;
    _this.IconsTitleBar[3][1] = imgOpeR3;
    _this.IconsTitleBar[3][2] = 1;

    //boton 4--------------------------------------------------------------------------------------------------------------------------
    var OpeR_Div4 = new TVclStackPanel(OperacionesRC, "OperacionesR_Div4", 2, [[1, 11], [1, 11], [1, 11], [1, 11], [1, 11]]);
    var OpeR_Div4_1C = OpeR_Div4.Column[0].This;
    var OpeR_Div4_2C = OpeR_Div4.Column[1].This;
    $(OpeR_Div4_2C).addClass('DivText');

    _this.Grupos[1][3][3] = OpeR_Div4;
    $(OpeR_Div4.Row.This).css("position", "relative");
    //-check group
    $(OpeR_Div4_1C).addClass('RH_checkZone');
    var chkBoxOpeR_Div4 = new TVclInputcheckbox(OpeR_Div4_1C, "chkBoxOpeR_Div4");
    chkBoxOpeR_Div4.VCLType = TVCLType.BS;
    chkBoxOpeR_Div4.onClick = function myfunction() {
        if (chkBoxOpeR_Div4.Checked == false) {
            _this.IconsTitleBar[4][0] = false;
        } else {
            _this.IconsTitleBar[4][0] = true;
        }
        _this.AddIconsTitleBar();
    }
    //-boton   
    var btnOpeR_4 = new TVclButton(OpeR_Div4_2C, "btnOpeR_4");
    btnOpeR_4.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btn_OpeRemotas_4");
    btnOpeR_4.VCLType = TVCLType.BS;
    btnOpeR_4.Src = "image/16/Package-warning.png";
    btnOpeR_4.ImageLocation = "BeforeText";
    btnOpeR_4.AlignElement = "Left";
    btnOpeR_4.onClick = function myfunction() {
        _this.BtnActivation(btnOpeR_4, imgOpeR4);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, btnOpeR_4, "Buttom");
        }
    }
    btnOpeR_4.This.style.width = "80%";
    btnOpeR_4.This.style.marginLeft = "10%";
    OpeR_Div4_2C.style.padding = "5px";
    //-crear imagen para el titleBar
    var imgOpeR4 = new Uimg("", "TitleBar_img_btnOpeR_4", "image/16/Package-warning.png", "");
    $(imgOpeR4).css("cursor", "pointer");
    $(imgOpeR4).addClass('RH_imgBarTitle');
    imgOpeR4.addEventListener("click", function () {
        _this.BtnActivation(btnOpeR_4, imgOpeR4);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, btnOpeR_4, "Buttom");
        }
    });
    //-agregar imagen a array y añadir a la barra titulo
    _this.IconsTitleBar[4] = new Array(3);
    _this.IconsTitleBar[4][0] = false;
    _this.IconsTitleBar[4][1] = imgOpeR4;
    _this.IconsTitleBar[4][2] = 1;

    //Boton 5--------------------------------------------------------------------------------------------------------------------------
    var OpeR_Div5 = new TVclStackPanel(OperacionesRC, "OperacionesR_Div5", 2, [[1, 11], [1, 11], [1, 11], [1, 11], [1, 11]]);
    var OpeR_Div5_1C = OpeR_Div5.Column[0].This;
    var OpeR_Div5_2C = OpeR_Div5.Column[1].This;
    $(OpeR_Div5_2C).addClass('DivText');

    var OpeR_Div5_2_1 = new TVclStackPanel(OpeR_Div5_2C, "OperacionesR_Div5_2_1", 1, [[12], [12], [12], [12], [12]]);
    var OpeR_Div5_2_1C = OpeR_Div5_2_1.Column[0].This;
    var OpeR_Div5_2_2 = new TVclStackPanel(OpeR_Div5_2C, "OperacionesR_Div5_2_2", 1, [[12], [12], [12], [12], [12]]);
    var OpeR_Div5_2_2C = OpeR_Div5_2_2.Column[0].This;
    _this.Grupos[1][3][4] = OpeR_Div5;
    $(OpeR_Div5.Row.This).css("position", "relative");
    //-check group
    $(OpeR_Div5_1C).addClass('RH_checkZone');
    var chkBoxOpeR_Div5 = new TVclInputcheckbox(OpeR_Div5_1C, "chkBoxOpeR_Div5");
    chkBoxOpeR_Div5.VCLType = TVCLType.BS;
    chkBoxOpeR_Div5.onClick = function myfunction() {
        if (chkBoxOpeR_Div5.Checked == false) {
            _this.IconsTitleBar[5][0] = false;
        } else {
            _this.IconsTitleBar[5][0] = true;
        }
        _this.AddIconsTitleBar();
    }
    //-boton  
    var btnOpeR_5 = new TVclButton(OpeR_Div5_2_1C, "btnOpeR_5");
    btnOpeR_5.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btn_OpeRemotas_5");
    btnOpeR_5.VCLType = TVCLType.BS;
    btnOpeR_5.ImageLocation = "BeforeText";
    btnOpeR_5.AlignElement = "Left";
    btnOpeR_5.Src = "image/16/barcodes.png";
    btnOpeR_5.onClick = function myfunction() {
        _this.BtnActivation(btnOpeR_5, imgOpeR5);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, btnOpeR_5, "Buttom");
        }
    }
    btnOpeR_5.This.style.width = "80%";
    btnOpeR_5.This.style.marginLeft = "10%";
    OpeR_Div5_2_1C.style.padding = "5px";
    //-Text Btn 5   
    var txtOpeR_5 = new TVclTextBox(OpeR_Div5_2_2C, "txtOpeR_5");
    txtOpeR_5.This.style.width = "80%";
    txtOpeR_5.This.style.marginLeft = "10%";
    OpeR_Div5_2_2C.style.padding = "5px";
    //-crear imagen para el titleBar
    var imgOpeR5 = new Uimg("", "TitleBar_img_btnOpeR_5", "image/16/barcodes.png", "");
    $(imgOpeR5).css("cursor", "pointer");
    $(imgOpeR5).addClass('RH_imgBarTitle');
    imgOpeR5.addEventListener("click", function () {
        _this.BtnActivation(btnOpeR_5, imgOpeR5);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, btnOpeR_5, "Buttom");
        }
    });
    //-agregar imagen a array y añadir a la barra titulo
    _this.IconsTitleBar[5] = new Array(3);
    _this.IconsTitleBar[5][0] = false;
    _this.IconsTitleBar[5][1] = imgOpeR5;
    _this.IconsTitleBar[5][2] = 1;

    //Boton 6--------------------------------------------------------------------------------------------------------------------------
    var OpeR_Div6 = new TVclStackPanel(OperacionesRC, "OperacionesR_Div6", 2, [[1, 11], [1, 11], [1, 11], [1, 11], [1, 11]]);
    var OpeR_Div6_1C = OpeR_Div6.Column[0].This;
    var OpeR_Div6_2C = OpeR_Div6.Column[1].This;
    $(OpeR_Div6_2C).addClass('DivText');

    var OpeR_Div6_2_1 = new TVclStackPanel(OpeR_Div6_2C, "OperacionesR_Div6_2_1", 1, [[12], [12], [12], [12], [12]]);
    var OpeR_Div6_2_1C = OpeR_Div6_2_1.Column[0].This;
    var OpeR_Div6_2_2 = new TVclStackPanel(OpeR_Div6_2C, "OperacionesR_Div6_2_2", 1, [[12], [12], [12], [12], [12]]);
    var OpeR_Div6_2_2C = OpeR_Div6_2_2.Column[0].This;
    _this.Grupos[1][3][5] = OpeR_Div6;
    $(OpeR_Div6.Row.This).css("position", "relative");
    //-check group
    $(OpeR_Div6_1C).addClass('RH_checkZone');
    var chkBoxOpeR_Div6 = new TVclInputcheckbox(OpeR_Div6_1C, "chkBoxOpeR_Div6");
    chkBoxOpeR_Div6.VCLType = TVCLType.BS;
    chkBoxOpeR_Div6.onClick = function myfunction() {
        if (chkBoxOpeR_Div6.Checked == false) {
            _this.IconsTitleBar[6][0] = false;
        } else {
            _this.IconsTitleBar[6][0] = true;
        }
        _this.AddIconsTitleBar();
    }
    //-boton   
    var btnOpeR_6 = new TVclButton(OpeR_Div6_2_1C, "btnOpeR_6");
    btnOpeR_6.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btn_OpeRemotas_6");
    btnOpeR_6.VCLType = TVCLType.BS;
    btnOpeR_6.Src = "image/16/Magnifying-glass.png";
    btnOpeR_6.ImageLocation = "BeforeText";
    btnOpeR_6.AlignElement = "Left";
    btnOpeR_6.onClick = function myfunction() {
        _this.BtnActivation(btnOpeR_6, imgOpeR6);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, btnOpeR_6, "Buttom");
        }
    }
    btnOpeR_6.This.style.width = "80%";
    btnOpeR_6.This.style.marginLeft = "10%";
    OpeR_Div6_2_1C.style.padding = "5px";
    //-text btn 6  
    var txtOpeR_6 = new TVclTextBox(OpeR_Div6_2_2C, "txtOpeR_6");
    txtOpeR_6.This.style.width = "80%";
    txtOpeR_6.This.style.marginLeft = "10%";
    OpeR_Div6_2_2C.style.padding = "5px";
    //-crear imagen para el titleBar
    var imgOpeR6 = new Uimg("", "TitleBar_img_btnOpeR_6", "image/16/Magnifying-glass.png", "");
    $(imgOpeR6).css("cursor", "pointer");
    $(imgOpeR6).addClass('RH_imgBarTitle');
    imgOpeR6.addEventListener("click", function () {
        _this.BtnActivation(btnOpeR_6, imgOpeR6);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, btnOpeR_6, "Buttom");
        }
    });
    //-agregar imagen a array y añadir a la barra titulo
    _this.IconsTitleBar[6] = new Array(3);
    _this.IconsTitleBar[6][0] = false;
    _this.IconsTitleBar[6][1] = imgOpeR6;
    _this.IconsTitleBar[6][2] = 1;

    //2 Conexión=====================================================================================================================================================
    //grupo  
    _this.Grupos[2] = new Array(5);
    _this.Grupos[2][0] = false; // indica si se esta a grupado  o no en la barra titulo
    _this.Grupos[2][1] = DivHeaderGrp2_2; //indica en cual div se agurpan las imagenes1
    _this.Grupos[2][2] = DivHeaderGrp2; //indica el div donde va la imagen del grupo
    _this.Grupos[2][3] = new Array(2);    
    _this.Grupos[2][4] = true; //indica si se debe mostrar este item o no
    _this.Grupos[2][5] = null; //Div del group general 

    //Contenedor item 2
    var Conexion = new TVclStackPanel("", "Conexion", 1, [[12], [12], [12], [12], [12]]);
    var ConexionC = Conexion.Column[0].This;

    //div group Conexion
    var Conx_Div0 = new TVclStackPanel(ConexionC, "Conexion_Div0", 1, [[12], [12], [12], [12], [12]]);
    var Conx_Div0C = Conx_Div0.Column[0].This;
    _this.Grupos[2][5] = Conx_Div0C; //Div del group general 

    var Lab_chkBoxConx_Div0 = new Vcllabel(Conx_Div0C, "Lab_chkBoxConx_Div0", TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "Lab_Group"));
    Lab_chkBoxConx_Div0.style.fontWeight = "normal";
    var ChkBoxConx_Div0 = new TVclInputcheckbox(Conx_Div0C, "ChkBoxConx_Div0");
    ChkBoxConx_Div0.VCLType = TVCLType.BS;
    ChkBoxConx_Div0.onClick = function myfunction() {
        if (ChkBoxConx_Div0.Checked == false) {
            _this.Grupos[2][0] = false;
        } else {
            _this.Grupos[2][0] = true;
        }
        _this.AddIconsTitleBar();
    }
    $(Conx_Div0C).css('border', "1px solid  #c4c4c4"); //#0094ff
    $(Lab_chkBoxConx_Div0).css('margin-right', "5px");

    //- ChekBox 1--------------------------------------------------------------------------------------------------------------------------
    var Conex_Div1 = new TVclStackPanel(ConexionC, "Conex_Div1", 2, [[2, 9], [1, 11], [1, 11], [1, 11], [1, 11]]);
    var Conex_Div1_1C = Conex_Div1.Column[0].This;
    var Conex_Div1_2C = Conex_Div1.Column[1].This;    

    _this.Grupos[2][3][0] = Conex_Div1;
    $(Conex_Div1.Row.This).css("position", "relative");
    //-check group
    $(Conex_Div1_1C).addClass('RH_checkZone');
    var chkBoxConx_Div1 = new TVclInputcheckbox(Conex_Div1_1C, "chkBoxConx_Div1");
    chkBoxConx_Div1.VCLType = TVCLType.BS;
    chkBoxConx_Div1.onClick = function myfunction() {
        if (chkBoxConx_Div1.Checked == false) {
            _this.IconsTitleBar[7][0] = false;
        } else {
            _this.IconsTitleBar[7][0] = true;
        }
        _this.AddIconsTitleBar();
    }
    //chek Div
    var Conex_Div1_2S = new TVclStackPanel(Conex_Div1_2C, "Conexion_Div1", 3, [[1, 10, 1], [1, 10, 1], [1, 10, 1], [1, 10, 1], [1, 10, 1]]);
    var Conex_Div1_2_1C = Conex_Div1_2S.Column[0].This;
    var Conex_Div1_2_2C = Conex_Div1_2S.Column[1].This;
    var Conex_Div1_2_3C = Conex_Div1_2S.Column[2].This; 
    var img_chkBoxConex_1 = Uimg(Conex_Div1_2_1C, "img_chkBoxConex_1", "image/16/Web-management.png", "");
    var Lab_chkBoxConex_1 = new Vcllabel(Conex_Div1_2_2C, "Lab_chkBoxConex_1", TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "Lab_Connection_1"));
    $(Conex_Div1_2_1C).addClass("divIconItem");
    $(Conex_Div1_2_2C).addClass("divLabItem");

    Lab_chkBoxConex_1.style.fontWeight = "normal";
    var chkBoxConex_1 = new TVclInputcheckbox(Conex_Div1_2_3C, "chkBoxConex_1");
    chkBoxConex_1.VCLType = TVCLType.BS;
    chkBoxConex_1.onClick = function () {
        _this.ChkActivation(chkBoxConex_1, imgCnx1, btnCnxHed_1, true);
        //_this.Conn_Btn1();
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, chkBoxConex_1, "Chek");
        }

        _this.RHREQUESTOPTIONS.InternetConnection = chkBoxConex_1.Checked;
    }
    chkBoxConex_1.Tag = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lab_Connection_1");
    _this.RHOptions.push(chkBoxConex_1);
  
    //-crear imagen para el titleBar
    var imgCnx1 = new Uimg("", "TitleBar_img_ckbConx_1", "image/16/Web-management.png", "");
    $(imgCnx1).css("cursor", "pointer");
    $(imgCnx1).addClass('RH_imgBarTitle');
    imgCnx1.addEventListener("click", function () {     
        _this.ChkActivation(chkBoxConex_1, imgCnx1, btnCnxHed_1, false);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, chkBoxConex_1, "Chek");
        }

        _this.RHREQUESTOPTIONS.InternetConnection = chkBoxConex_1.Checked;
    });
    //-agregar imagen a array y añadir a la barra titulo
    _this.IconsTitleBar[7] = new Array(3);
    _this.IconsTitleBar[7][0] = false;
    _this.IconsTitleBar[7][1] = imgCnx1;
    _this.IconsTitleBar[7][2] = 2;

    //- ChekBox 2--------------------------------------------------------------------------------------------------------------------------
    var Conex_Div2 = new TVclStackPanel(ConexionC, "Conexion_Div2", 2, [[2, 9], [1, 11], [1, 11], [1, 11], [1, 11]]);
    var Conex_Div2_1C = Conex_Div2.Column[0].This;
    var Conex_Div2_2C = Conex_Div2.Column[1].This;
    _this.Grupos[2][3][1] = Conex_Div2;
    $(Conex_Div2.Row.This).css("position", "relative");
    //-check group
    $(Conex_Div2_1C).addClass('RH_checkZone');
    var chkBoxConx_Div2 = new TVclInputcheckbox(Conex_Div2_1C, "chkBoxConx_Div2");
    chkBoxConx_Div2.VCLType = TVCLType.BS;
    chkBoxConx_Div2.onClick = function myfunction() {
        if (chkBoxConx_Div2.Checked == false) {
            _this.IconsTitleBar[8][0] = false;
        } else {
            _this.IconsTitleBar[8][0] = true;
        }
        _this.AddIconsTitleBar();
    }
    //chek Div
    var Conex_Div2_2S = new TVclStackPanel(Conex_Div2_2C, "Conexion_Div2", 3, [[1, 10, 1], [1, 10, 1], [1, 10, 1], [1, 10, 1], [1, 10, 1]]);
    var Conex_Div2_2_1C = Conex_Div2_2S.Column[0].This;
    var Conex_Div2_2_2C = Conex_Div2_2S.Column[1].This;
    var Conex_Div2_2_3C = Conex_Div2_2S.Column[2].This;
    var img_chkBoxConex_2 = Uimg(Conex_Div2_2_1C, "img_chkBoxConex_2", "image/16/Laptop.png", "");
    var Lab_chkBoxConex_2 = new Vcllabel(Conex_Div2_2_2C, "Lab_chkBoxConex_2", TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "Lab_Connection_2"));
    $(Conex_Div2_2_1C).addClass("divIconItem");
    $(Conex_Div2_2_2C).addClass("divLabItem");

    Lab_chkBoxConex_2.style.fontWeight = "normal";
    var chkBoxConex_2 = new TVclInputcheckbox(Conex_Div2_2_3C, "chkBoxConex_2");
    chkBoxConex_2.VCLType = TVCLType.BS;
    chkBoxConex_2.onClick = function () {
        _this.ChkActivation(chkBoxConex_2, imgCnx2, btnCnxHed_2, true);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, chkBoxConex_2, "Chek");
        }
        _this.RHREQUESTOPTIONS.ConnectbyHost = chkBoxConex_2.Checked;
    }
    chkBoxConex_2.Tag = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lab_Connection_2");
    _this.RHOptions.push(chkBoxConex_2);

    //-crear imagen para el titleBar
    var imgCnx2 = new Uimg("", "TitleBar_img_ckbConx_2", "image/16/Laptop.png", "");
    $(imgCnx2).css("cursor", "pointer");
    $(imgCnx2).addClass('RH_imgBarTitle');
    imgCnx2.addEventListener("click", function () {
        _this.ChkActivation(chkBoxConex_2, imgCnx2, btnCnxHed_2, false);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, chkBoxConex_2, "Chek");
        }
        _this.RHREQUESTOPTIONS.ConnectbyHost = chkBoxConex_2.Checked;
    });
    //-agregar imagen a array y añadir a la barra titulo
    _this.IconsTitleBar[8] = new Array(3);
    _this.IconsTitleBar[8][0] = false;
    _this.IconsTitleBar[8][1] = imgCnx2;
    _this.IconsTitleBar[8][2] = 2;

    //3 Others Options=======================================================================================================================
    //grupo  
    _this.Grupos[3] = new Array(5);
    _this.Grupos[3][0] = false; // indica si se esta a grupado  o no en la barra titulo
    _this.Grupos[3][1] = DivHeaderGrp3_2; //indica en cual div se agurpan las imagenes
    _this.Grupos[3][2] = DivHeaderGrp3; //indica el div donde va la imagen del grupo
    _this.Grupos[3][3] = new Array(3);
    _this.Grupos[3][4] = true; //indica si se debe mostrar este item o no
    _this.Grupos[3][5] = null; //Div del group general 

    //Contenedor item 3
    var O_Options = new TVclStackPanel("", "O_Options", 1, [[12], [12], [12], [12], [12]]);
    var O_OptionsC = O_Options.Column[0].This;

    //div group Others Options
    var O_Opt_Div0 = new TVclStackPanel(O_OptionsC, "O_Options_Div0", 1, [[12], [12], [12], [12], [12]]);
    var O_Opt_Div0C = O_Opt_Div0.Column[0].This;
    _this.Grupos[3][5] = O_Opt_Div0C; //Div del group general 

    var Lab_chkBoxO_Opt_Div0 = new Vcllabel(O_Opt_Div0C, "Lab_chkBoxO_Opt_Div0", TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "Lab_Group"));
    Lab_chkBoxO_Opt_Div0.style.fontWeight = "normal";
    var ChkBoxO_Opt_Div0 = new TVclInputcheckbox(O_Opt_Div0C, "ChkBoxO_Opt_Div0");
    ChkBoxO_Opt_Div0.VCLType = TVCLType.BS;
    ChkBoxO_Opt_Div0.onClick = function myfunction() {
        if (ChkBoxO_Opt_Div0.Checked == false) {
            _this.Grupos[3][0] = false;
        } else {
            _this.Grupos[3][0] = true;
        }
        _this.AddIconsTitleBar();
    }
    $(O_Opt_Div0C).css('border', "1px solid #c4c4c4");
    $(Lab_chkBoxO_Opt_Div0).css('margin-right', "5px");

    //- Boton 1--------------------------------------------------------------------------------------------------------------------------
    var O_Opt_Div1 = new TVclStackPanel(O_OptionsC, "O_Opt_Div1", 2, [[1, 11], [1, 11], [1, 11], [1, 11], [1, 11]]);
    var O_Opt_Div1_1C = O_Opt_Div1.Column[0].This;
    var O_Opt_Div1_2C = O_Opt_Div1.Column[1].This;
    _this.Grupos[3][3][0] = O_Opt_Div1;
    $(O_Opt_Div1.Row.This).css("position", "relative");
    //-check group
    $(O_Opt_Div1_1C).addClass('RH_checkZone');
    var chkBoxO_Opt_Div1 = new TVclInputcheckbox(O_Opt_Div1_1C, "chkBoxO_Opt_Div1");
    chkBoxO_Opt_Div1.VCLType = TVCLType.BS;
    chkBoxO_Opt_Div1.onClick = function myfunction() {
        if (chkBoxO_Opt_Div1.Checked == false) {
            _this.IconsTitleBar[9][0] = false;
        } else {
            _this.IconsTitleBar[9][0] = true;
        }
        _this.AddIconsTitleBar();
    }
    //-Boton 
    var btnO_Opti_1 = new TVclButton(O_Opt_Div1_2C, "btnO_Opti_1");
    btnO_Opti_1.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btn_Oth_Options_1");
    btnO_Opti_1.VCLType = TVCLType.BS;
    btnO_Opti_1.Src = "image/16/Package-delete.png";
    btnO_Opti_1.ImageLocation = "BeforeText";
    btnO_Opti_1.AlignElement = "Left";
    btnO_Opti_1.onClick = function myfunction() {       
        _this.BtnActivation(btnO_Opti_1, imgO_Opt1);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, btnO_Opti_1, "Buttom");
        }
    }
    btnO_Opti_1.This.style.width = "80%";
    btnO_Opti_1.This.style.marginLeft = "10%";
    O_Opt_Div1_2C.style.padding = "5px";
    //-crear imagen para el titleBar
    var imgO_Opt1 = new Uimg("", "TitleBar_img_ckbO_Opti_1", "image/16/Package-delete.png", "");
    $(imgO_Opt1).css("cursor", "pointer");
    $(imgO_Opt1).addClass('RH_imgBarTitle');
    imgO_Opt1.addEventListener("click", function () {       
        _this.BtnActivation(btnO_Opti_1, imgO_Opt1);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, btnO_Opti_1, "Buttom");
        }
    });
    //-agregar imagen a array y añadir a la barra titulo
    _this.IconsTitleBar[9] = new Array(3);
    _this.IconsTitleBar[9][0] = false;
    _this.IconsTitleBar[9][1] = imgO_Opt1;
    _this.IconsTitleBar[9][2] = 3;

    //- Boton 2--------------------------------------------------------------------------------------------------------------------------
    var O_Opt_Div2 = new TVclStackPanel(O_OptionsC, "O_Opt_Div2", 2, [[1, 11], [1, 11], [1, 11], [1, 11], [1, 11]]);
    var O_Opt_Div2_1C = O_Opt_Div2.Column[0].This;
    var O_Opt_Div2_2C = O_Opt_Div2.Column[1].This;
    _this.Grupos[3][3][1] = O_Opt_Div2;
    $(O_Opt_Div2.Row.This).css("position", "relative");
    //-check group
    $(O_Opt_Div2_1C).addClass('RH_checkZone');
    var chkBoxO_Opt_Div2 = new TVclInputcheckbox(O_Opt_Div2_1C, "chkBoxO_Opt_Div2");
    chkBoxO_Opt_Div2.VCLType = TVCLType.BS;
    chkBoxO_Opt_Div2.onClick = function myfunction() {
        if (chkBoxO_Opt_Div2.Checked == false) {
            _this.IconsTitleBar[10][0] = false;
        } else {
            _this.IconsTitleBar[10][0] = true;
        }
        _this.AddIconsTitleBar();
    }
    //-Boton
    var btnO_Opti_2 = new TVclButton(O_Opt_Div2_2C, "btnO_Opti_2");
    btnO_Opti_2.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btn_Oth_Options_2");
    btnO_Opti_2.VCLType = TVCLType.BS;
    btnO_Opti_2.Src = "image/16/information.png";
    btnO_Opti_2.ImageLocation = "BeforeText";
    btnO_Opti_2.AlignElement = "Left";
    btnO_Opti_2.onClick = function myfunction() {
        _this.BtnActivation(btnO_Opti_2, imgO_Opt2);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, btnO_Opti_2, "Buttom");
        }
    }
    btnO_Opti_2.This.style.width = "80%";
    btnO_Opti_2.This.style.marginLeft = "10%";
    O_Opt_Div2_2C.style.padding = "5px";
    //-crear imagen para el titleBar
    var imgO_Opt2 = new Uimg("", "TitleBar_img_ckbO_Opti_2", "image/16/information.png", "");
    $(imgO_Opt2).css("cursor", "pointer");
    $(imgO_Opt2).addClass('RH_imgBarTitle');
    imgO_Opt2.addEventListener("click", function () {
        _this.BtnActivation(btnO_Opti_2, imgO_Opt2);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, btnO_Opti_2, "Buttom");
        }
    });
    //-agregar imagen a array y añadir a la barra titulo
    _this.IconsTitleBar[10] = new Array(3);
    _this.IconsTitleBar[10][0] = false;
    _this.IconsTitleBar[10][1] = imgO_Opt2;
    _this.IconsTitleBar[10][2] = 3;

    //- Boton 3--------------------------------------------------------------------------------------------------------------------------
    var O_Opt_Div3 = new TVclStackPanel(O_OptionsC, "O_Opti_Div3", 2, [[1, 11], [1, 11], [1, 11], [1, 11], [1, 11]]);
    var O_Opt_Div3_1C = O_Opt_Div3.Column[0].This;
    var O_Opt_Div3_2C = O_Opt_Div3.Column[1].This;
    _this.Grupos[3][3][2] = O_Opt_Div3;
    $(O_Opt_Div3.Row.This).css("position", "relative");
    //-check group
    $(O_Opt_Div3_1C).addClass('RH_checkZone');
    var chkBoxO_Opt_Div3 = new TVclInputcheckbox(O_Opt_Div3_1C, "chkBoxO_Opt_Div3");
    chkBoxO_Opt_Div3.VCLType = TVCLType.BS;
    chkBoxO_Opt_Div3.onClick = function myfunction() {
        if (chkBoxO_Opt_Div3.Checked == false) {
            _this.IconsTitleBar[11][0] = false;
        } else {
            _this.IconsTitleBar[11][0] = true;
        }
        _this.AddIconsTitleBar();
    }
    //-Boton
    var btnO_Opti_3 = new TVclButton(O_Opt_Div3_2C, "btnO_Opti_3");
    btnO_Opti_3.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btn_Oth_Options_3");
    btnO_Opti_3.VCLType = TVCLType.BS;
    btnO_Opti_3.Src = "image/16/Complete-file.png";
    btnO_Opti_3.ImageLocation = "BeforeText";
    btnO_Opti_3.AlignElement = "Left";
    btnO_Opti_3.onClick = function myfunction() {
        _this.BtnActivation(btnO_Opti_3, imgO_Opt3);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, btnO_Opti_3, "Buttom");
        }
    }
    btnO_Opti_3.This.style.width = "80%";
    btnO_Opti_3.This.style.marginLeft = "10%";
    O_Opt_Div3_2C.style.padding = "5px";
    //-crear imagen para el titleBar
    var imgO_Opt3 = new Uimg("", "TitleBar_img_ckbO_Opti_3", "image/16/Complete-file.png", "");
    $(imgO_Opt3).css("cursor", "pointer");
    $(imgO_Opt3).addClass('RH_imgBarTitle');
    imgO_Opt3.addEventListener("click", function () {
        _this.BtnActivation(btnO_Opti_3, imgO_Opt3);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, btnO_Opti_3, "Buttom");
        }
    });
    //-agregar imagen a array y añadir a la barra titulo
    _this.IconsTitleBar[11] = new Array(3);
    _this.IconsTitleBar[11][0] = false;
    _this.IconsTitleBar[11][1] = imgO_Opt3;
    _this.IconsTitleBar[11][2] = 3;

    //4 Policies=======================================================================================================================
    //grupo  
    _this.Grupos[4] = new Array(5);
    _this.Grupos[4][0] = false; // indica si se esta a grupado  o no en la barra titulo
    _this.Grupos[4][1] = DivHeaderGrp4_2; //indica en cual div se agurpan las imagenes
    _this.Grupos[4][2] = DivHeaderGrp4; //indica el div donde va la imagen del grupo
    _this.Grupos[4][3] = new Array(2);
    _this.Grupos[4][4] = true; //indica si se debe mostrar este item o no
    _this.Grupos[4][5] = null; //Div del group general 

    //Contenedor item 3
    var Policies = new TVclStackPanel("", "Policies", 1, [[12], [12], [12], [12], [12]]);
    var PoliciesC = Policies.Column[0].This;

    //div group Policies
    var Policies_Div0 = new TVclStackPanel(PoliciesC, "Policies_Div0", 1, [[12], [12], [12], [12], [12]]);
    var Policies_Div0C = Policies_Div0.Column[0].This;
    _this.Grupos[4][5] = Policies_Div0C;

    var Lab_chkBoxPolicies_Div0 = new Vcllabel(Policies_Div0C, "Lab_chkBoxPolicies_Div0", TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "Lab_Group"));
    Lab_chkBoxPolicies_Div0.style.fontWeight = "normal";
    var ChkBoxPolicies_Div0 = new TVclInputcheckbox(Policies_Div0C, "ChkBoxPolicies_Div0");
    ChkBoxPolicies_Div0.VCLType = TVCLType.BS;
    ChkBoxPolicies_Div0.onClick = function myfunction() {
        if (ChkBoxPolicies_Div0.Checked == false) {
            _this.Grupos[4][0] = false;
        } else {
            _this.Grupos[4][0] = true;
        }
        _this.AddIconsTitleBar();
    }
    $(Policies_Div0C).css('border', "1px solid #c4c4c4");
    $(Lab_chkBoxPolicies_Div0).css('margin-right', "5px");    

    //Boton 1---------------------------------------------------------------------------------------------------------------------------
    var Poli_Div1 = new TVclStackPanel(PoliciesC, "Poli_Div1", 2, [[1, 11], [1, 11], [1, 11], [1, 11], [1, 11]]);
    var Poli_Div1_1C = Poli_Div1.Column[0].This;
    var Poli_Div1_2C = Poli_Div1.Column[1].This;
    _this.Grupos[4][3][0] = Poli_Div1;
    $(Poli_Div1.Row.This).css("position", "relative");
    //-check group
    $(Poli_Div1_1C).addClass('RH_checkZone');
    var chkBoxPoli_Div1 = new TVclInputcheckbox(Poli_Div1_1C, "chkBoxPoli_Div1");
    chkBoxPoli_Div1.VCLType = TVCLType.BS;
    chkBoxPoli_Div1.onClick = function myfunction() {
        if (chkBoxPoli_Div1.Checked == false) {
            _this.IconsTitleBar[12][0] = false;
        } else {
            _this.IconsTitleBar[12][0] = true;
        }
        _this.AddIconsTitleBar();
    }
    //-Boton
    var btnO_Poli_1 = new TVclButton(Poli_Div1_2C, "btnO_Poli_1");
    btnO_Poli_1.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btn_Policies_1");
    btnO_Poli_1.VCLType = TVCLType.BS;
    btnO_Poli_1.Src = "image/16/Rules.png";
    btnO_Poli_1.ImageLocation = "BeforeText";
    btnO_Poli_1.AlignElement = "Left";
    btnO_Poli_1.onClick = function myfunction() {
        _this.BtnActivation(btnO_Poli_1, imgO_Poli1);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, btnO_Poli_1, "Buttom");
        }
    }
    btnO_Poli_1.This.style.width = "80%";
    btnO_Poli_1.This.style.marginLeft = "10%";
    Poli_Div1_2C.style.padding = "5px";
    //-crear imagen para el titleBar
    var imgO_Poli1 = new Uimg("", "TitleBar_img_ckbPoli_1", "image/16/Rules.png", "");
    $(imgO_Poli1).css("cursor", "pointer");
    $(imgO_Poli1).addClass('RH_imgBarTitle');
    imgO_Poli1.addEventListener("click", function () {
        _this.BtnActivation(btnO_Poli_1, imgO_Poli1);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, btnO_Poli_1, "Buttom");
        }
    });
    //-agregar imagen a array y añadir a la barra titulo
    _this.IconsTitleBar[12] = new Array(3);
    _this.IconsTitleBar[12][0] = false;
    _this.IconsTitleBar[12][1] = imgO_Poli1;
    _this.IconsTitleBar[12][2] = 4;

    // Boton 2 -----------------------------------------------------------------------------------------------------------
    var Poli_Div2 = new TVclStackPanel(PoliciesC, "Poli_Div2", 2, [[1, 11], [1, 11], [1, 11], [1, 11], [1, 11]]);
    var Poli_Div2_1C = Poli_Div2.Column[0].This;
    var Poli_Div2_2C = Poli_Div2.Column[1].This;
    _this.Grupos[4][3][1] = Poli_Div2;
    $(Poli_Div2.Row.This).css("position", "relative");
    //-check group
    $(Poli_Div2_1C).addClass('RH_checkZone');
    var chkBoxPoli_Div2 = new TVclInputcheckbox(Poli_Div2_1C, "chkBoxPoli_Div2");
    chkBoxPoli_Div2.VCLType = TVCLType.BS;
    chkBoxPoli_Div2.onClick = function myfunction() {
        if (chkBoxPoli_Div2.Checked == false) {
            _this.IconsTitleBar[13][0] = false;
        } else {
            _this.IconsTitleBar[13][0] = true;
        }
        _this.AddIconsTitleBar();
    }
    //- boton   
    var btnO_Poli_2 = new TVclButton(Poli_Div2_2C, "btnO_Poli_2");
    btnO_Poli_2.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btn_Policies_2");
    btnO_Poli_2.VCLType = TVCLType.BS;
    btnO_Poli_2.Src = "image/16/Backup-restore.png";
    btnO_Poli_2.ImageLocation = "BeforeText";
    btnO_Poli_2.AlignElement = "Left";
    btnO_Poli_2.onClick = function myfunction() {
        _this.BtnActivation(btnO_Poli_2, imgO_Poli2);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, btnO_Poli_2, "Buttom");
        }
    }
    btnO_Poli_2.This.style.width = "80%";
    btnO_Poli_2.This.style.marginLeft = "10%";
    Poli_Div2_2C.style.padding = "5px";
    //-crear imagen para el titleBar
    var imgO_Poli2 = new Uimg("", "TitleBar_img_ckbPoli_2", "image/16/Backup-restore.png", "");
    $(imgO_Poli2).css("cursor", "pointer");
    $(imgO_Poli2).addClass('RH_imgBarTitle');
    imgO_Poli2.addEventListener("click", function () {
        _this.BtnActivation(btnO_Poli_2, imgO_Poli2);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, btnO_Poli_2, "Buttom");
        }
    });
    //-agregar imagen a array y añadir a la barra titulo
    _this.IconsTitleBar[13] = new Array(3);
    _this.IconsTitleBar[13][0] = false;
    _this.IconsTitleBar[13][1] = imgO_Poli2;
    _this.IconsTitleBar[13][2] = 4;

    //5 Files=======================================================================================================================
    //grupo  
    _this.Grupos[5] = new Array(5);
    _this.Grupos[5][0] = false; // indica si se esta a grupado  o no en la barra titulo
    _this.Grupos[5][1] = DivHeaderGrp5_2; //indica en cual div se agurpan las imagenes
    _this.Grupos[5][2] = DivHeaderGrp5; //indica el div donde va la imagen del grupo
    _this.Grupos[5][3] = new Array(2);
    _this.Grupos[5][4] = true;
    _this.Grupos[5][5] = null; //Div del group general 

    //Contenedor del Item 5
    var Files = new TVclStackPanel("", "OperacionesR", 1, [[12], [12], [12], [12], [12]]);
    var FilesC = Files.Column[0].This;  

    //div group Files
    var Files_Div0 = new TVclStackPanel(FilesC, "Files_Div0", 1, [[12], [12], [12], [12], [12]]);
    var Files_Div0C = Files_Div0.Column[0].This;
    _this.Grupos[5][5] = Files_Div0C;

    var Lab_chkBoxFiles_Div0 = new Vcllabel(Files_Div0C, "Lab_chkBoxFiles_Div0", TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "Lab_Group"));
    Lab_chkBoxFiles_Div0.style.fontWeight = "normal";
    var ChkBoxFiles_Div0 = new TVclInputcheckbox(Files_Div0C, "ChkBoxFiles_Div0");
    ChkBoxFiles_Div0.VCLType = TVCLType.BS;
    ChkBoxFiles_Div0.onClick = function myfunction() {
        if (ChkBoxFiles_Div0.Checked == false) {
            _this.Grupos[5][0] = false;
        } else {
            _this.Grupos[5][0] = true;
        }
        _this.AddIconsTitleBar();
    }
    $(Files_Div0C).css('border', "1px solid #c4c4c4");
    $(Lab_chkBoxFiles_Div0).css('margin-right', "5px");

    //Boton 1 --------------------------------------------------------------------------------------------------------------------------
    var Files_Div1 = new TVclStackPanel(FilesC, "Files_Div1", 2, [[1, 11], [1, 11], [1, 11], [1, 11], [1, 11]]);
    var Files_Div1_1C = Files_Div1.Column[0].This;
    var Files_Div1_2C = Files_Div1.Column[1].This;
    _this.Grupos[5][3][0] = Files_Div1;
    $(Files_Div1.Row.This).css("position", "relative");
    //-check group
    $(Files_Div1_1C).addClass('RH_checkZone');
    var chkBoxFiles_Div1 = new TVclInputcheckbox(Files_Div1_1C, "chkBoxFiles_Div1");
    chkBoxFiles_Div1.VCLType = TVCLType.BS;
    chkBoxFiles_Div1.onClick = function myfunction() {
        if (chkBoxFiles_Div1.Checked == false) {
            _this.IconsTitleBar[14][0] = false;
        } else {
            _this.IconsTitleBar[14][0] = true;
        }
        _this.AddIconsTitleBar();
    }
    //-Boton
    var btnFiles_1 = new TVclButton(Files_Div1_2C, "btnFiles_1");
    btnFiles_1.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btn_Files_1");
    btnFiles_1.VCLType = TVCLType.BS;
    btnFiles_1.Src = "image/16/Packing.png";
    btnFiles_1.ImageLocation = "BeforeText";
    btnFiles_1.AlignElement = "Left";
    btnFiles_1.onClick = function myfunction() {
        _this.BtnActivation(btnFiles_1, imgFile1);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, btnFiles_1, "Buttom");
        }
    }
    btnFiles_1.This.style.width = "80%";
    btnFiles_1.This.style.marginLeft = "10%";
    Files_Div1_2C.style.padding = "5px";
    //-crear imagen para el titleBar
    var imgFile1 = new Uimg("", "TitleBar_img_ckbFiles_1", "image/16/Packing.png", "");
    $(imgFile1).css("cursor", "pointer");
    $(imgFile1).addClass('RH_imgBarTitle');
    imgFile1.addEventListener("click", function () {
        _this.BtnActivation(btnFiles_1, imgFile1);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, btnFiles_1, "Buttom");
        }
    });
    //-agregar imagen a array y añadir a la barra titulo
    _this.IconsTitleBar[14] = new Array(3);
    _this.IconsTitleBar[14][0] = false;
    _this.IconsTitleBar[14][1] = imgFile1;
    _this.IconsTitleBar[14][2] = 5;

    //Boton 2 --------------------------------------------------------------------------------------------------------------------------     
    var Files_Div2 = new TVclStackPanel(FilesC, "Files_Div2", 2, [[1, 11], [1, 11], [1, 11], [1, 11], [1, 11]]);
    var Files_Div2_1C = Files_Div2.Column[0].This;
    var Files_Div2_2C = Files_Div2.Column[1].This;
    var Files_Div2_2_1 = new TVclStackPanel(Files_Div2_2C, "OperacionesR_Div6_2_1", 1, [[12], [12], [12], [12], [12]]);
    var Files_Div2_2_1C = Files_Div2_2_1.Column[0].This;
    var Files_Div2_2_2 = new TVclStackPanel(Files_Div2_2C, "OperacionesR_Div6_2_2", 1, [[12], [12], [12], [12], [12]]);
    var Files_Div2_2_2C = Files_Div2_2_2.Column[0].This;
    _this.Grupos[5][3][1] = Files_Div2;
    $(Files_Div2.Row.This).css("position", "relative");
    //-check group
    $(Files_Div2_1C).addClass('RH_checkZone');
    var chkBoxFiles_Div2 = new TVclInputcheckbox(Files_Div2_1C, "chkBoxFiles_Div2");
    chkBoxFiles_Div2.VCLType = TVCLType.BS;
    chkBoxFiles_Div2.onClick = function myfunction() {
        if (chkBoxFiles_Div2.Checked == false) {
            _this.IconsTitleBar[15][0] = false;
        } else {
            _this.IconsTitleBar[15][0] = true;
        }
        _this.AddIconsTitleBar();
    }
    //- Boton
    var btnFiles_2 = new TVclButton(Files_Div2_2_1C, "btnFiles_2");
    btnFiles_2.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btn_Files_2");
    btnFiles_2.VCLType = TVCLType.BS;
    btnFiles_2.Src = "image/16/application.png";
    btnFiles_2.ImageLocation = "BeforeText";
    btnFiles_2.AlignElement = "Left";
    btnFiles_2.onClick = function myfunction() {
        _this.BtnActivation(btnFiles_2, imgFile2);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, btnFiles_2, "Buttom");
        }
    }
    btnFiles_2.This.style.width = "80%";
    btnFiles_2.This.style.marginLeft = "10%";
    Files_Div2_2_1C.style.padding = "5px";
    //-Text
    var Files_Div3 = new TVclStackPanel(Files_Div2_2_2C, "Files_Div3", 1, [[12], [12], [12], [12], [12]]);
    var Files_Div3C = Files_Div3.Column[0].This;
    var txtFiles_1 = new TVclTextBox(Files_Div3C, "txtFiles_1");
    txtFiles_1.This.style.width = "80%";
    txtFiles_1.This.style.marginLeft = "10%";
    Files_Div2_2_2C.style.padding = "5px";
    //-crear imagen para el titleBar
    var imgFile2 = new Uimg("", "TitleBar_img_ckbFiles_2", "image/16/application.png", "");
    $(imgFile2).css("cursor", "pointer");
    $(imgFile2).addClass('RH_imgBarTitle');
    imgFile2.addEventListener("click", function () {
        _this.BtnActivation(btnFiles_2, imgFile2);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, btnFiles_2, "Buttom");
        }
    });
    //-agregar imagen a array y añadir a la barra titulo
    _this.IconsTitleBar[15] = new Array(3);
    _this.IconsTitleBar[15][0] = false;
    _this.IconsTitleBar[15][1] = imgFile2;
    _this.IconsTitleBar[15][2] = 5;

    //6 Remote Control=======================================================================================================================
    //grupo  
    _this.Grupos[6] = new Array(5);
    _this.Grupos[6][0] = false; // indica si se esta a grupado  o no en la barra titulo
    _this.Grupos[6][1] = DivHeaderGrp6_2; //indica en cual div se agurpan las imagenes
    _this.Grupos[6][2] = DivHeaderGrp6; //indica el div donde va la imagen del grupo
    _this.Grupos[6][3] = new Array(8);
    _this.Grupos[6][4] = true; //indica si se debe mostrar este item o no
    _this.Grupos[6][5] = null; //Div del group general 

    //Contenedor para el item 6
    var R_Control = new TVclStackPanel("", "R_Control", 1, [[12], [12], [12], [12], [12]]);
    var R_ControlC = R_Control.Column[0].This;

    //div group Remote Control
    var R_Control_Div0 = new TVclStackPanel(R_ControlC, "R_Control_Div0", 1, [[12], [12], [12], [12], [12]]);
    var R_Control_Div0C = R_Control_Div0.Column[0].This;
    _this.Grupos[6][5] = R_Control_Div0C;

    var Lab_chkBoxR_Control_Div0 = new Vcllabel(R_Control_Div0C, "Lab_chkBoxR_Control_Div0", TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "Lab_Group"));
    Lab_chkBoxR_Control_Div0.style.fontWeight = "normal";
    var ChkBoxR_Control_Div0 = new TVclInputcheckbox(R_Control_Div0C, "ChkBoxR_Control_Div0");
    ChkBoxR_Control_Div0.VCLType = TVCLType.BS;
    ChkBoxR_Control_Div0.onClick = function myfunction() {
        if (ChkBoxR_Control_Div0.Checked == false) {
            _this.Grupos[6][0] = false;
        } else {
            _this.Grupos[6][0] = true;
        }
        _this.AddIconsTitleBar();
    }
    $(R_Control_Div0C).css('border', "1px solid #c4c4c4");
    $(Lab_chkBoxR_Control_Div0).css('margin-right', "5px");

    //Boton 1------------------------------------------------------------------------------------------------------------
    var RCont_Div1 = new TVclStackPanel(R_ControlC, "RCont_Div1", 2, [[2, 9], [1, 11], [1, 11], [1, 11], [1, 11]]);
    var RCont_Div1_1C = RCont_Div1.Column[0].This;
    var RCont_Div1_2C = RCont_Div1.Column[1].This;
    _this.Grupos[6][3][0] = RCont_Div1;
    $(RCont_Div1.Row.This).css("position", "relative");
    //-check group
    $(RCont_Div1_1C).addClass('RH_checkZone');
    var chkBoxRCont_Div1 = new TVclInputcheckbox(RCont_Div1_1C, "chkBoxRCont_Div1");
    chkBoxRCont_Div1.VCLType = TVCLType.BS;
    chkBoxRCont_Div1.onClick = function myfunction() {
        if (chkBoxRCont_Div1.Checked == false) {
            _this.IconsTitleBar[16][0] = false;
        } else {
            _this.IconsTitleBar[16][0] = true;
        }
        _this.AddIconsTitleBar();
    }
    //- Boton
    var btnRCont_1 = new TVclButton(RCont_Div1_2C, "btnRCont_1");
    btnRCont_1.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btn_Rem_Control_1"); 
    btnRCont_1.VCLType = TVCLType.BS;
    btnRCont_1.Src = "image/16/power.png";
    btnRCont_1.ImageLocation = "BeforeText";
    btnRCont_1.AlignElement = "Left";
    btnRCont_1.onClick = function myfunction() {
        _this.BtnActivation(btnRCont_1, imgR_Cont1);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, btnRCont_1, "Buttom");
        }
    }
    //$(btnRCont_1.This).addClass('RHButton');
    $(RCont_Div1_2C).css('text-align', "center");

    btnRCont_1.This.style.width = "80%";
    btnRCont_1.This.style.marginLeft = "10%";
    RCont_Div1_2C.style.padding = "5px";
    //-crear imagen para el titleBar
    var imgR_Cont1 = new Uimg("", "TitleBar_img_ckbRCont_1", "image/16/power.png", "");
    $(imgR_Cont1).css("cursor", "pointer");
    $(imgR_Cont1).addClass('RH_imgBarTitle');
    imgR_Cont1.addEventListener("click", function () {
        _this.BtnActivation(btnRCont_1, imgR_Cont1);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, btnRCont_1, "Buttom");
        }
    });
    //-agregar imagen a array y añadir a la barra titulo
    _this.IconsTitleBar[16] = new Array(3);
    _this.IconsTitleBar[16][0] = false;
    _this.IconsTitleBar[16][1] = imgR_Cont1;
    _this.IconsTitleBar[16][2] = 6;

    //Boton 2------------------------------------------------------------------------------------------------------------
    var RCont_Div2 = new TVclStackPanel(R_ControlC, "RCont_Div2", 2, [[2, 9], [1, 11], [1, 11], [1, 11], [1, 11]]);
    var RCont_Div2_1C = RCont_Div2.Column[0].This;
    var RCont_Div2_2C = RCont_Div2.Column[1].This;
    _this.Grupos[6][3][1] = RCont_Div2;
    $(RCont_Div2.Row.This).css("position", "relative");
    //-check group
    $(RCont_Div2_1C).addClass('RH_checkZone');
    var chkBoxRCont_Div2 = new TVclInputcheckbox(RCont_Div2_1C, "chkBoxRCont_Div2");
    chkBoxRCont_Div2.VCLType = TVCLType.BS;
    chkBoxRCont_Div2.onClick = function myfunction() {
        if (chkBoxRCont_Div2.Checked == false) {
            _this.IconsTitleBar[17][0] = false;
        } else {
            _this.IconsTitleBar[17][0] = true;
        }
        _this.AddIconsTitleBar();
    }
    //- Boton
    var btnRCont_2 = new TVclButton(RCont_Div2_2C, "btnRCont_2");
    btnRCont_2.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btn_Rem_Control_2");  
    btnRCont_2.VCLType = TVCLType.BS;
    btnRCont_2.Src = "image/16/Content-tree.png";
    btnRCont_2.ImageLocation = "BeforeText";
    btnRCont_2.AlignElement = "Left";
    btnRCont_2.onClick = function myfunction() {
        _this.RHREQUESTOPTIONS.RemoteHelpFuntion = Persistence.RemoteHelp.Properties.TRemoteHelpFuntion._UseBrowser.value;
        _this.BtnActivation(btnRCont_2, imgR_Cont2);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, btnRCont_2, "Buttom");
        }
        _this.RHSelectOutput();
        //_this.OnRemoteClick(_this, "OK", "Button");
    }
    btnRCont_2.This.style.width = "80%";
    btnRCont_2.This.style.marginLeft = "10%";
    RCont_Div2_2C.style.padding = "5px";
    //$(btnRCont_2.This).addClass('RHButton');
    $(RCont_Div2_2C).css('text-align', "center");

    //-crear imagen para el titleBar
    var imgR_Cont2 = new Uimg("", "TitleBar_img_ckbRCont_2", "image/16/Content-tree.png", "");
    $(imgR_Cont2).css("cursor", "pointer");
    $(imgR_Cont2).addClass('RH_imgBarTitle');
    imgR_Cont2.addEventListener("click", function () {
        _this.RHREQUESTOPTIONS.RemoteHelpFuntion = Persistence.RemoteHelp.Properties.TRemoteHelpFuntion._UseBrowser.value;
        _this.BtnActivation(btnRCont_2, imgR_Cont2);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, btnRCont_2, "Buttom");
        }
        _this.RHSelectOutput();
        //_this.OnRemoteClick(_this, "OK", "Button");
    });
    //-agregar imagen a array y añadir a la barra titulo
    _this.IconsTitleBar[17] = new Array(3);
    _this.IconsTitleBar[17][0] = false;
    _this.IconsTitleBar[17][1] = imgR_Cont2;
    _this.IconsTitleBar[17][2] = 6;

    //Boton 3------------------------------------------------------------------------------------------------------------
    var RCont_Div3 = new TVclStackPanel(R_ControlC, "RCont_Div3", 2, [[2, 9], [1, 11], [1, 11], [1, 11], [1, 11]]);
    var RCont_Div3_1C = RCont_Div3.Column[0].This;
    var RCont_Div3_2C = RCont_Div3.Column[1].This;
    _this.Grupos[6][3][2] = RCont_Div3;
    $(RCont_Div3.Row.This).css("position", "relative");
    //-check group
    $(RCont_Div3_1C).addClass('RH_checkZone');
    var chkBoxRCont_Div3 = new TVclInputcheckbox(RCont_Div3_1C, "chkBoxRCont_Div3");
    chkBoxRCont_Div3.VCLType = TVCLType.BS;
    chkBoxRCont_Div3.onClick = function myfunction() {
        _this.RHREQUESTOPTIONS.RemoteHelpFuntion = Persistence.RemoteHelp.Properties.TRemoteHelpFuntion._UseBrowser;
        if (chkBoxRCont_Div3.Checked == false) {
            _this.IconsTitleBar[18][0] = false;
        } else {
            _this.IconsTitleBar[18][0] = true;
        }
        _this.AddIconsTitleBar();
    }
    //- Boton
    var btnRCont_3 = new TVclButton(RCont_Div3_2C, "btnRCont_3");
    btnRCont_3.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btn_Rem_Control_3");   
    btnRCont_3.VCLType = TVCLType.BS;
    btnRCont_3.Src = "image/16/monitor.png";
    btnRCont_3.ImageLocation = "BeforeText";
    btnRCont_3.AlignElement = "Left";
    btnRCont_3.onClick = function myfunction() {
        _this.RHREQUESTOPTIONS.RemoteHelpFuntion = Persistence.RemoteHelp.Properties.TRemoteHelpFuntion._RemoteControl.value;
        _this.BtnActivation(btnRCont_3, imgR_Cont3);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, btnRCont_3, "Buttom");
        }

        _this.RHSelectOutput();        

    }
    btnRCont_3.This.style.width = "80%";
    btnRCont_3.This.style.marginLeft = "10%";
   // $(btnRCont_3.This).addClass('RHButton');     
    $(RCont_Div3_2C).css('text-align', "center");
    
    RCont_Div3_2C.style.padding = "5px";

    //-crear imagen para el titleBar
    var imgR_Cont3 = new Uimg("", "TitleBar_img_ckbRCont_3", "image/16/monitor.png", "");
    $(imgR_Cont3).css("cursor", "pointer");
    $(imgR_Cont3).addClass('RH_imgBarTitle');
    imgR_Cont3.addEventListener("click", function () {
        _this.RHREQUESTOPTIONS.RemoteHelpFuntion = Persistence.RemoteHelp.Properties.TRemoteHelpFuntion._RemoteControl.value;
        _this.BtnActivation(btnRCont_3, imgR_Cont3);       
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, btnRCont_3, "Buttom");
        }
        _this.RHSelectOutput();
    });

    //-agregar imagen a array y añadir a la barra titulo
    _this.IconsTitleBar[18] = new Array(3);
    _this.IconsTitleBar[18][0] = false;
    _this.IconsTitleBar[18][1] = imgR_Cont3;
    _this.IconsTitleBar[18][2] = 6;

    //Check  1------------------------------------------------------------------------------------------------------------
    var RCont_Div4 = new TVclStackPanel(R_ControlC, "RCont_Div4", 2, [[2, 9], [1, 11], [1, 11], [1, 11], [1, 11]]);
    var RCont_Div4_1C = RCont_Div4.Column[0].This;
    var RCont_Div4_2C = RCont_Div4.Column[1].This;
    _this.Grupos[6][3][3] = RCont_Div4;
    $(RCont_Div4.Row.This).css("position", "relative");
    //-check group
    $(RCont_Div4_1C).addClass('RH_checkZone');
    var chkBoxRCont_Div4 = new TVclInputcheckbox(RCont_Div4_1C, "chkBoxRCont_Div4");
    chkBoxRCont_Div4.VCLType = TVCLType.BS;
    chkBoxRCont_Div4.onClick = function myfunction() {
        if (chkBoxRCont_Div4.Checked == false) {
            _this.IconsTitleBar[19][0] = false;
        } else {
            _this.IconsTitleBar[19][0] = true;
        }
        _this.AddIconsTitleBar();
    }
    //- Check
    var RCont_Div4_2S = new TVclStackPanel(RCont_Div4_2C, "RCont_Div4_2S", 3, [[1, 10, 1], [1, 10, 1], [1, 10, 1], [1, 10, 1], [1, 10, 1]]);
    var RCont_Div4_2_1C = RCont_Div4_2S.Column[0].This;
    var RCont_Div4_2_2C = RCont_Div4_2S.Column[1].This;
    var RCont_Div4_2_3C = RCont_Div4_2S.Column[2].This;
   
    var img_chkBoxRCont_1 = Uimg(RCont_Div4_2_1C, "img_chkBoxRCont_1", "image/16/Eye.png", "");
    img_chkBoxRCont_1.style.marginRight = "5px";
    img_chkBoxRCont_1.style.marginLeft = "10%";
    var Lab_chkBoxRCont_1 = new Vcllabel(RCont_Div4_2_2C, "Lab_chkBoxRCont_1", TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "Lab_Rem_Control_1"));
    $(RCont_Div4_2_1C).addClass("divIconItem");
    $(RCont_Div4_2_2C).addClass("divLabItem");

    Lab_chkBoxRCont_1.style.marginRight = "5px";
    Lab_chkBoxRCont_1.style.fontWeight = "normal";
    var chkBoxRCont_1 = new TVclInputcheckbox(RCont_Div4_2_3C, "chkBoxConex_1");
    chkBoxRCont_1.VCLType = TVCLType.BS;
    chkBoxRCont_1.onClick = function () {
        _this.ChkActivation(chkBoxRCont_1, imgR_Cont4, "", true);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, chkBoxRCont_1, "Chek");
        }        
        _this.RHREQUESTOPTIONS.JustSee = chkBoxRCont_1.Checked;
    }
    chkBoxRCont_1.Tag = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lab_Rem_Control_1");
    _this.RHOptions.push(chkBoxRCont_1);

    //-crear imagen para el titleBar
    var imgR_Cont4 = new Uimg("", "TitleBar_img_ckbRCont_4", "image/16/Eye.png", "");
    $(imgR_Cont4).css("cursor", "pointer");
    $(imgR_Cont4).addClass('RH_imgBarTitle');
    imgR_Cont4.addEventListener("click", function () {
        _this.ChkActivation(chkBoxRCont_1, imgR_Cont4, "", false);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, chkBoxRCont_1, "Chek");
        }
        _this.RHREQUESTOPTIONS.JustSee = chkBoxRCont_1.Checked;
    });
    //-agregar imagen a array y añadir a la barra titulo
    _this.IconsTitleBar[19] = new Array(3);
    _this.IconsTitleBar[19][0] = false;
    _this.IconsTitleBar[19][1] = imgR_Cont4;
    _this.IconsTitleBar[19][2] = 6;

    //Check 2------------------------------------------------------------------------------------------------------------
    var RCont_Div5 = new TVclStackPanel(R_ControlC, "RCont_Div5", 2, [[2, 9], [1, 11], [1, 11], [1, 11], [1, 11]]);
    var RCont_Div5_1C = RCont_Div5.Column[0].This;
    var RCont_Div5_2C = RCont_Div5.Column[1].This;
    _this.Grupos[6][3][4] = RCont_Div5;
    $(RCont_Div5.Row.This).css("position", "relative");
    //-check group
    $(RCont_Div5_1C).addClass('RH_checkZone');
    var chkBoxRCont_Div5 = new TVclInputcheckbox(RCont_Div5_1C, "chkBoxRCont_Div5");
    chkBoxRCont_Div5.VCLType = TVCLType.BS;
    chkBoxRCont_Div5.onClick = function myfunction() {
        if (chkBoxRCont_Div5.Checked == false) {
            _this.IconsTitleBar[20][0] = false;
        } else {
            _this.IconsTitleBar[20][0] = true;
        }
        _this.AddIconsTitleBar();
    }
    //- Check
    var RCont_Div5_2S = new TVclStackPanel(RCont_Div5_2C, "RCont_Div5_2S", 3, [[1, 10, 1], [1, 10, 1], [1, 10, 1], [1, 10, 1], [1, 10, 1]]);
    var RCont_Div5_2_1C = RCont_Div5_2S.Column[0].This;
    var RCont_Div5_2_2C = RCont_Div5_2S.Column[1].This;
    var RCont_Div5_2_3C = RCont_Div5_2S.Column[2].This;
    $(RCont_Div5_2_1C).addClass("divIconItem");
    $(RCont_Div5_2_2C).addClass("divLabItem");

    var img_chkBoxRCont_2 = Uimg(RCont_Div5_2_1C, "img_chkBoxRCont_2", "image/16/Locked.png", "");
    img_chkBoxRCont_2.style.marginRight = "5px";
    img_chkBoxRCont_2.style.marginLeft = "10%";
    var Lab_chkBoxRCont_2 = new Vcllabel(RCont_Div5_2_2C, "Lab_chkBoxRCont_2", TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "Lab_Rem_Control_2"));
    Lab_chkBoxRCont_2.style.marginRight = "5px";
    Lab_chkBoxRCont_2.style.fontWeight = "normal";   

    var chkBoxRCont_2 = new TVclInputcheckbox(RCont_Div5_2_3C, "chkBoxRCont_2");
    chkBoxRCont_2.VCLType = TVCLType.BS;
    chkBoxRCont_2.onClick = function () {
        _this.ChkActivation(chkBoxRCont_2, imgR_Cont5, "", true);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, chkBoxRCont_2, "Chek");
        }
        _this.RHREQUESTOPTIONS.LockKeyboard = chkBoxRCont_2.Checked;
    }
    chkBoxRCont_2.Tag = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lab_Rem_Control_2");
    _this.RHOptions.push(chkBoxRCont_2);

    //-crear imagen para el titleBar
    var imgR_Cont5 = new Uimg("", "TitleBar_img_ckbRCont_5", "image/16/Locked.png", "");
    $(imgR_Cont5).css("cursor", "pointer");
    $(imgR_Cont5).addClass('RH_imgBarTitle');
    imgR_Cont5.addEventListener("click", function () {
        _this.ChkActivation(chkBoxRCont_2, imgR_Cont5, "", false);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, chkBoxRCont_2, "Chek");
        }
        _this.RHREQUESTOPTIONS.LockKeyboard = chkBoxRCont_2.Checked;
    });
    //-agregar imagen a array y añadir a la barra titulo
    _this.IconsTitleBar[20] = new Array(3);
    _this.IconsTitleBar[20][0] = false;
    _this.IconsTitleBar[20][1] = imgR_Cont5;
    _this.IconsTitleBar[20][2] = 6;

    //Check 3------------------------------------------------------------------------------------------------------------
    var RCont_Div6 = new TVclStackPanel(R_ControlC, "RCont_Div6", 2, [[2, 9], [1, 11], [1, 11], [1, 11], [1, 11]]);
    var RCont_Div6_1C = RCont_Div6.Column[0].This;
    var RCont_Div6_2C = RCont_Div6.Column[1].This;
    _this.Grupos[6][3][5] = RCont_Div6;
    $(RCont_Div6.Row.This).css("position", "relative");
    //-check group
    $(RCont_Div6_1C).addClass('RH_checkZone');
    var chkBoxRCont_Div6 = new TVclInputcheckbox(RCont_Div6_1C, "chkBoxRCont_Div6");
    chkBoxRCont_Div6.VCLType = TVCLType.BS;
    chkBoxRCont_Div6.onClick = function myfunction() {
        if (chkBoxRCont_Div6.Checked == false) {
            _this.IconsTitleBar[21][0] = false;
        } else {
            _this.IconsTitleBar[21][0] = true;
        }
        _this.AddIconsTitleBar();
    }


    //- Check
    var RCont_Div6_2S = new TVclStackPanel(RCont_Div6_2C, "RCont_Div6_2S", 3, [[1, 10, 1], [1, 10, 1], [1, 10, 1], [1, 10, 1], [1, 10, 1]]);
    var RCont_Div6_2_1C = RCont_Div6_2S.Column[0].This;
    var RCont_Div6_2_2C = RCont_Div6_2S.Column[1].This;
    var RCont_Div6_2_3C = RCont_Div6_2S.Column[2].This;
    $(RCont_Div6_2_1C).addClass("divIconItem");
    $(RCont_Div6_2_2C).addClass("divLabItem");

    var img_chkBoxRCont_3 = Uimg(RCont_Div6_2_1C, "img_chkBoxRCont_3", "image/16/Keys.png", "");
    img_chkBoxRCont_3.style.marginRight = "5px";
    img_chkBoxRCont_3.style.marginLeft = "10%";
    var Lab_chkBoxRCont_3 = new Vcllabel(RCont_Div6_2_2C, "Lab_chkBoxRCont_3", TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "Lab_Rem_Control_3"));
    Lab_chkBoxRCont_3.style.marginRight = "5px";
    Lab_chkBoxRCont_3.style.fontWeight = "normal";
    var chkBoxRCont_3 = new TVclInputcheckbox(RCont_Div6_2_3C, "chkBoxRCont_3");
    chkBoxRCont_3.VCLType = TVCLType.BS;
    chkBoxRCont_3.onClick = function () {
        _this.ChkActivation(chkBoxRCont_3, imgR_Cont6, "", true);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, chkBoxRCont_3, "Chek");
        }
        _this.RHREQUESTOPTIONS.DonotAskforPermission = chkBoxRCont_3.Checked;
    }
    chkBoxRCont_3.Tag = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lab_Rem_Control_3");
    _this.RHOptions.push(chkBoxRCont_3);

    //-crear imagen para el titleBar
    var imgR_Cont6 = new Uimg("", "TitleBar_img_ckbRCont_6", "image/16/Keys.png", "");
    $(imgR_Cont6).css("cursor", "pointer");
    $(imgR_Cont6).addClass('RH_imgBarTitle');
    imgR_Cont6.addEventListener("click", function () {
        _this.ChkActivation(chkBoxRCont_3, imgR_Cont6, "", false);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, chkBoxRCont_3, "Chek");
        }
        _this.RHREQUESTOPTIONS.DonotAskforPermission = chkBoxRCont_3.Checked;
    });
    //-agregar imagen a array y añadir a la barra titulo
    _this.IconsTitleBar[21] = new Array(3);
    _this.IconsTitleBar[21][0] = false;
    _this.IconsTitleBar[21][1] = imgR_Cont6;
    _this.IconsTitleBar[21][2] = 6;

    //Check 4------------------------------------------------------------------------------------------------------------
    var RCont_Div7 = new TVclStackPanel(R_ControlC, "RCont_Div7", 2, [[2, 9], [1, 11], [1, 11], [1, 11], [1, 11]]);
    var RCont_Div7_1C = RCont_Div7.Column[0].This;
    var RCont_Div7_2C = RCont_Div7.Column[1].This;
    _this.Grupos[6][3][6] = RCont_Div7;
    $(RCont_Div7.Row.This).css("position", "relative");
    //-check group
    $(RCont_Div7_1C).addClass('RH_checkZone');
    var chkBoxRCont_Div7 = new TVclInputcheckbox(RCont_Div7_1C, "chkBoxRCont_Div7");
    chkBoxRCont_Div7.VCLType = TVCLType.BS;
    chkBoxRCont_Div7.onClick = function myfunction() {
        if (chkBoxRCont_Div7.Checked == false) {
            _this.IconsTitleBar[22][0] = false;
        } else {
            _this.IconsTitleBar[22][0] = true;
        }
        _this.AddIconsTitleBar();
    }
    //- Check
    var RCont_Div7_2S = new TVclStackPanel(RCont_Div7_2C, "RCont_Div7_2S", 3, [[1, 10, 1], [1, 10, 1], [1, 10, 1], [1, 10, 1], [1, 10, 1]]);
    var RCont_Div7_2_1C = RCont_Div7_2S.Column[0].This;
    var RCont_Div7_2_2C = RCont_Div7_2S.Column[1].This;
    var RCont_Div7_2_3C = RCont_Div7_2S.Column[2].This;
    $(RCont_Div7_2_1C).addClass("divIconItem");
    $(RCont_Div7_2_2C).addClass("divLabItem");

    var img_chkBoxRCont_4 = Uimg(RCont_Div7_2_1C, "img_chkBoxRCont_4", "image/16/signal-tower.png", "");
    img_chkBoxRCont_4.style.marginRight = "5px";
    img_chkBoxRCont_4.style.marginLeft = "10%";
    var Lab_chkBoxRCont_4 = new Vcllabel(RCont_Div7_2_2C, "Lab_chkBoxRCont_4", TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "Lab_Rem_Control_4"));
    Lab_chkBoxRCont_4.style.marginRight = "5px";
    Lab_chkBoxRCont_4.style.fontWeight = "normal";
    var chkBoxRCont_4 = new TVclInputcheckbox(RCont_Div7_2_3C, "chkBoxRCont_4");
    chkBoxRCont_4.VCLType = TVCLType.BS;
    chkBoxRCont_4.onClick = function () {
        _this.ChkActivation(chkBoxRCont_4, imgR_Cont7, "", true);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, chkBoxRCont_4, "Chek");
        }
        _this.RHREQUESTOPTIONS.LowBandwidth = chkBoxRCont_4.Checked;
    }
    chkBoxRCont_4.Tag = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lab_Rem_Control_4");
    _this.RHOptions.push(chkBoxRCont_4);

    //-crear imagen para el titleBar
    var imgR_Cont7 = new Uimg("", "TitleBar_img_ckbRCont_7", "image/16/signal-tower.png", "");
    $(imgR_Cont7).css("cursor", "pointer");
    $(imgR_Cont7).addClass('RH_imgBarTitle');
    imgR_Cont7.addEventListener("click", function () {
        _this.ChkActivation(chkBoxRCont_4, imgR_Cont7, "", false);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, chkBoxRCont_4, "Chek");
        }
        _this.RHREQUESTOPTIONS.LowBandwidth = chkBoxRCont_4.Checked;
    });
    //-agregar imagen a array y añadir a la barra titulo
    _this.IconsTitleBar[22] = new Array(3);
    _this.IconsTitleBar[22][0] = false;
    _this.IconsTitleBar[22][1] = imgR_Cont7;
    _this.IconsTitleBar[22][2] = 6;

    //Check 5------------------------------------------------------------------------------------------------------------
    var RCont_Div8 = new TVclStackPanel(R_ControlC, "RCont_Div8", 2, [[2, 9], [1, 11], [1, 11], [1, 11], [1, 11]]);
    var RCont_Div8_1C = RCont_Div8.Column[0].This;
    var RCont_Div8_2C = RCont_Div8.Column[1].This;
    _this.Grupos[6][3][7] = RCont_Div8;
    $(RCont_Div8.Row.This).css("position", "relative");
    //-check group
    $(RCont_Div8_1C).addClass('RH_checkZone');
    var chkBoxRCont_Div8 = new TVclInputcheckbox(RCont_Div8_1C, "chkBoxRCont_Div8");
    chkBoxRCont_Div8.VCLType = TVCLType.BS;
    chkBoxRCont_Div8.onClick = function myfunction() {
        if (chkBoxRCont_Div8.Checked == false) {
            _this.IconsTitleBar[23][0] = false;
        } else {
            _this.IconsTitleBar[23][0] = true;
        }
        _this.AddIconsTitleBar();
    }
    //- Check
    var RCont_Div8_2S = new TVclStackPanel(RCont_Div8_2C, "RCont_Div8_2S", 3, [[1, 10, 1], [1, 10, 1], [1, 10, 1], [1, 10, 1], [1, 10, 1]]);
    var RCont_Div8_2_1C = RCont_Div8_2S.Column[0].This;
    var RCont_Div8_2_2C = RCont_Div8_2S.Column[1].This;
    var RCont_Div8_2_3C = RCont_Div8_2S.Column[2].This;
    $(RCont_Div8_2_1C).addClass("divIconItem");
    $(RCont_Div8_2_2C).addClass("divLabItem");

    var img_chkBoxRCont_5 = Uimg(RCont_Div8_2_1C, "img_chkBoxRCont_5", "image/16/Record.png", "");
    img_chkBoxRCont_5.style.marginRight = "5px";
    img_chkBoxRCont_5.style.marginLeft = "10%";
    var Lab_chkBoxRCont_5 = new Vcllabel(RCont_Div8_2_2C, "Lab_chkBoxRCont_5", TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "Lab_Rem_Control_5"));
    Lab_chkBoxRCont_5.style.marginRight = "5px";
    Lab_chkBoxRCont_5.style.fontWeight = "normal";
    var chkBoxRCont_5 = new TVclInputcheckbox(RCont_Div8_2_3C, "chkBoxRCont_5");
    chkBoxRCont_5.VCLType = TVCLType.BS;
    chkBoxRCont_5.onClick = function () {
        _this.ChkActivation(chkBoxRCont_5, imgR_Cont8, "", true);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, chkBoxRCont_5, "Chek");
        }
        _this.RHREQUESTOPTIONS.RecordSession = chkBoxRCont_5.Checked;
    }
    chkBoxRCont_5.Tag = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lab_Rem_Control_5");
    _this.RHOptions.push(chkBoxRCont_5);

    //-crear imagen para el titleBar
    var imgR_Cont8 = new Uimg("", "TitleBar_img_ckbRCont_8", "image/16/Record.png", "");
    $(imgR_Cont8).css("cursor", "pointer");
    $(imgR_Cont8).addClass('RH_imgBarTitle');
    imgR_Cont8.addEventListener("click", function () {
        _this.ChkActivation(chkBoxRCont_5, imgR_Cont8, "", false);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, chkBoxRCont_5, "Chek");
        }
        _this.RHREQUESTOPTIONS.RecordSession = chkBoxRCont_5.Checked;
    });
    //-agregar imagen a array y añadir a la barra titulo
    _this.IconsTitleBar[23] = new Array(3);
    _this.IconsTitleBar[23][0] = false;
    _this.IconsTitleBar[23][1] = imgR_Cont8;
    _this.IconsTitleBar[23][2] = 6;

    //7 Messaging =======================================================================================================================
    //grupo  
    _this.Grupos[7] = new Array(5);
    _this.Grupos[7][0] = false; // indica si se esta a grupado  o no en la barra titulo
    _this.Grupos[7][1] = DivHeaderGrp7_2; //indica en cual div se agurpan las imagenes
    _this.Grupos[7][2] = DivHeaderGrp7; //indica el div donde va la imagen del grupo
    _this.Grupos[7][3] = new Array(3);
    _this.Grupos[7][4] = true; //indica si se debe mostrar este item o no
    _this.Grupos[7][5] = null;

    //Contenedor item 7
    var Messaging = new TVclStackPanel("", "Messaging", 1, [[12], [12], [12], [12], [12]]);
    var MessagingC = Messaging.Column[0].This;

    //div group Messaging
    var Messaging_Div0 = new TVclStackPanel(MessagingC, "Messaging_Div0", 1, [[12], [12], [12], [12], [12]]);
    var Messaging_Div0C = Messaging_Div0.Column[0].This;
    _this.Grupos[7][5] = Messaging_Div0C;

    var Lab_chkBoxMessaging_Div0 = new Vcllabel(Messaging_Div0C, "Lab_chkBoxMessaging_Div0", TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "Lab_Group"));
    Lab_chkBoxMessaging_Div0.style.fontWeight = "normal";
    var ChkBoxMessaging_Div0 = new TVclInputcheckbox(Messaging_Div0C, "ChkBoxMessaging_Div0");
    ChkBoxMessaging_Div0.VCLType = TVCLType.BS;
    ChkBoxMessaging_Div0.onClick = function myfunction() {
        if (ChkBoxMessaging_Div0.Checked == false) {
            _this.Grupos[7][0] = false;
        } else {
            _this.Grupos[7][0] = true;
        }
        _this.AddIconsTitleBar();
    }
    $(Messaging_Div0C).css('border', "1px solid #c4c4c4");
    $(Lab_chkBoxMessaging_Div0).css('margin-right', "5px");

    //Boton 1 -----------------------------------------------------------------------------------------------------------------------
    var Mssg_Div1 = new TVclStackPanel(MessagingC, "Mssg_Div1", 2, [[1, 11], [1, 11], [1, 11], [1, 11], [1, 11]]);
    var Mssg_Div1_1C = Mssg_Div1.Column[0].This;
    var Mssg_Div1_2C = Mssg_Div1.Column[1].This;
    _this.Grupos[7][3][0] = Mssg_Div1;
    $(Mssg_Div1.Row.This).css("position", "relative");
    //-check group
    $(Mssg_Div1_1C).addClass('RH_checkZone');
    var chkBoxMssg_Div1 = new TVclInputcheckbox(Mssg_Div1_1C, "chkBoxMssg_Div1");
    chkBoxMssg_Div1.VCLType = TVCLType.BS;
    chkBoxMssg_Div1.onClick = function myfunction() {
        if (chkBoxMssg_Div1.Checked == false) {
            _this.IconsTitleBar[24][0] = false;
        } else {
            _this.IconsTitleBar[24][0] = true;
        }
        _this.AddIconsTitleBar();
    }
    //- Boton
    var btnMssg_1 = new TVclButton(Mssg_Div1_2C, "btnMssg_1");
    btnMssg_1.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btn_Messaging_1");
    btnMssg_1.VCLType = TVCLType.BS;
    btnMssg_1.Src = "image/16/text-message.png";
    btnMssg_1.ImageLocation = "BeforeText";
    btnMssg_1.AlignElement = "Left";
    btnMssg_1.onClick = function myfunction() {
        _this.BtnActivation(btnMssg_1, imgMssg1);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, btnMssg_1, "Buttom");
        }
    }
    btnMssg_1.This.style.width = "80%";
    btnMssg_1.This.style.marginLeft = "10%";
    Mssg_Div1_2C.style.padding = "5px";
    //-crear imagen para el titleBar
    var imgMssg1 = new Uimg("", "TitleBar_img_ckbMssg_1", "image/16/text-message.png", "");
    $(imgMssg1).css("cursor", "pointer");
    $(imgMssg1).addClass('RH_imgBarTitle');
    imgMssg1.addEventListener("click", function () {
        _this.BtnActivation(btnMssg_1, imgMssg1);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, btnMssg_1, "Buttom");
        }
    });
    //-agregar imagen a array y añadir a la barra titulo
    _this.IconsTitleBar[24] = new Array(3);
    _this.IconsTitleBar[24][0] = false;
    _this.IconsTitleBar[24][1] = imgMssg1;
    _this.IconsTitleBar[24][2] = 7;

    //Boton 2 -----------------------------------------------------------------------------------------------------------------------
    var Mssg_Div2 = new TVclStackPanel(MessagingC, "Mssg_Div2", 2, [[1, 11], [1, 11], [1, 11], [1, 11], [1, 11]]);
    var Mssg_Div2_1C = Mssg_Div2.Column[0].This;
    var Mssg_Div2_2C = Mssg_Div2.Column[1].This;
    _this.Grupos[7][3][1] = Mssg_Div2;
    $(Mssg_Div2.Row.This).css("position", "relative");
    //-check group
    $(Mssg_Div2_1C).addClass('RH_checkZone');
    var chkBoxMssg_Div2 = new TVclInputcheckbox(Mssg_Div2_1C, "chkBoxMssg_Div2");
    chkBoxMssg_Div2.VCLType = TVCLType.BS;
    chkBoxMssg_Div2.onClick = function myfunction() {
        if (chkBoxMssg_Div2.Checked == false) {
            _this.IconsTitleBar[25][0] = false;
        } else {
            _this.IconsTitleBar[25][0] = true;
        }
        _this.AddIconsTitleBar();
    }
    //- Boton
    var btnMssg_2 = new TVclButton(Mssg_Div2_2C, "btnMssg_2");
    btnMssg_2.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btn_Messaging_2");
    btnMssg_2.VCLType = TVCLType.BS;
    btnMssg_2.Src = "image/16/microphone.png";
    btnMssg_2.ImageLocation = "BeforeText";
    btnMssg_2.AlignElement = "Left";
    btnMssg_2.onClick = function myfunction() {
        _this.BtnActivation(btnMssg_2, imgMssg2);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, btnMssg_2, "Buttom");
        }
    }
    btnMssg_2.This.style.width = "80%";
    btnMssg_2.This.style.marginLeft = "10%";
    Mssg_Div2_2C.style.padding = "5px";
    //-crear imagen para el titleBar
    var imgMssg2 = new Uimg("", "TitleBar_img_ckbMssg_2", "image/16/microphone.png", "");
    $(imgMssg2).css("cursor", "pointer");
    $(imgMssg2).addClass('RH_imgBarTitle');
    imgMssg2.addEventListener("click", function () {
        _this.BtnActivation(btnMssg_2, imgMssg2);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, btnMssg_2, "Buttom");
        }
    });
    //-agregar imagen a array y añadir a la barra titulo
    _this.IconsTitleBar[25] = new Array(3);
    _this.IconsTitleBar[25][0] = false;
    _this.IconsTitleBar[25][1] = imgMssg2;
    _this.IconsTitleBar[25][2] = 7;

    //Boton 3 -----------------------------------------------------------------------------------------------------------------------
    var Mssg_Div3 = new TVclStackPanel(MessagingC, "Mssg_Div3", 2, [[1, 11], [1, 11], [1, 11], [1, 11], [1, 11]]);
    var Mssg_Div3_1C = Mssg_Div3.Column[0].This;
    var Mssg_Div3_2C = Mssg_Div3.Column[1].This;
    _this.Grupos[7][3][2] = Mssg_Div3;
    $(Mssg_Div3.Row.This).css("position", "relative");
    //-check group
    $(Mssg_Div3_1C).addClass('RH_checkZone');
    var chkBoxMssg_Div3 = new TVclInputcheckbox(Mssg_Div3_1C, "chkBoxMssg_Div3");
    chkBoxMssg_Div3.VCLType = TVCLType.BS;
    chkBoxMssg_Div3.onClick = function myfunction() {
        if (chkBoxMssg_Div3.Checked == false) {
            _this.IconsTitleBar[26][0] = false;
        } else {
            _this.IconsTitleBar[26][0] = true;
        }
        _this.AddIconsTitleBar();
    }
    //- Boton
    var btnMssg_3 = new TVclButton(Mssg_Div3_2C, "btnMssg_3");
    btnMssg_3.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btn_Messaging_3");
    btnMssg_3.VCLType = TVCLType.BS;
    btnMssg_3.Src = "image/16/send-text-message.png";
    btnMssg_3.ImageLocation = "BeforeText";
    btnMssg_3.AlignElement = "Left";
    btnMssg_3.onClick = function myfunction() {
        _this.BtnActivation(btnMssg_3, imgMssg3);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, btnMssg_3, "Buttom");
        }
    }
    btnMssg_3.This.style.width = "80%";
    btnMssg_3.This.style.marginLeft = "10%";
    Mssg_Div3_2C.style.padding = "5px";
    //-crear imagen para el titleBar
    var imgMssg3 = new Uimg("", "TitleBar_img_ckbMssg_3", "image/16/send-text-message.png", "");
    $(imgMssg3).css("cursor", "pointer");
    $(imgMssg3).addClass('RH_imgBarTitle');
    imgMssg3.addEventListener("click", function () {
        _this.BtnActivation(btnMssg_3, imgMssg3);
        if (_this.OnRemoteClick != null) {
            _this.OnRemoteClick(_this, btnMssg_3, "Buttom");
        }
    });
    //-agregar imagen a array y añadir a la barra titulo
    _this.IconsTitleBar[26] = new Array(3);
    _this.IconsTitleBar[26][0] = false;
    _this.IconsTitleBar[26][1] = imgMssg3;
    _this.IconsTitleBar[26][2] = 7;

    _this.Config();

    //*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/ Agregar Items AL NAVBAR /*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/
    var newItem = new Array();
    newItem.Text = "";
    newItem.Image = "";
    newItem.Style = "ControlAdd";
    newItem.Object_Contain = ConexionHdC;
    _this.NavBar_RH.AddItemNavBar(newItem);

    if ( _this.Grupos[1][4] == true) {
        var newItem = new Array();
        newItem.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "RHelp_Item1");
        newItem.Image = "image/16/Handtool.png";
        newItem.Style = "ControlContain";
        newItem.Object_Contain = OperacionesRC;
        _this.NavBar_RH.AddItemNavBar(newItem);
    }
    
    if (_this.Grupos[2][4] == true) {
        var newItem = new Array();
        newItem.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "RHelp_Item2");
        newItem.Image = "image/16/connection.png";
        newItem.Style = "ControlContain";
        newItem.Object_Contain = ConexionC;
        _this.NavBar_RH.AddItemNavBar(newItem);
    }
    
    if (_this.Grupos[3][4] == true) {
        var newItem = new Array();
        newItem.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "RHelp_Item3");
        newItem.Image = "image/16/Options.png";
        newItem.Style = "ControlContain";
        newItem.Object_Contain = O_OptionsC;
        _this.NavBar_RH.AddItemNavBar(newItem);
    }
    
    if (_this.Grupos[4][4] == true) {
        var newItem = new Array();
        newItem.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "RHelp_Item4");
        newItem.Image = "image/16/Rulers.png";
        newItem.Style = "ControlContain";
        newItem.Object_Contain = PoliciesC;
        _this.NavBar_RH.AddItemNavBar(newItem);
    }
    
    if (_this.Grupos[5][4] == true) {
        var newItem = new Array();
        newItem.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "RHelp_Item5");
        newItem.Image = "image/16/document.png";
        newItem.Style = "ControlContain";
        newItem.Object_Contain = FilesC;
        _this.NavBar_RH.AddItemNavBar(newItem);
    }
    
    if (_this.Grupos[6][4] == true) {
        var newItem = new Array();
        newItem.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "RHelp_Item6");
        newItem.Image = "image/16/Computer.png";
        newItem.Style = "ControlContain";
        newItem.Object_Contain = R_ControlC;
        _this.NavBar_RH.AddItemNavBar(newItem);
    }
    
    if (_this.Grupos[7][4] == true) {
        var newItem = new Array();
        newItem.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "RHelp_Item7");
        newItem.Image = "image/16/comment.png";
        newItem.Style = "ControlContain";
        newItem.Object_Contain = MessagingC;
        _this.NavBar_RH.AddItemNavBar(newItem);
    }
   
    //ocultar div de grups/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/
    for (var i = 0; i < _this.Grupos.length - 1; i++) {
        idx = i + 1;
        $(_this.Grupos[idx][5]).css("display", "none");
        for (var j = 0; j < _this.Grupos[idx][3].length; j++) {
            $(_this.Grupos[idx][3][j].Column[0].This).css("display", "none");
        }
    }

    _this.CallbackModalResult();
   

}

ItHelpCenter.TUfrRemoteHelp.prototype.AddStyleFile = function (nombre, onSuccess, onError) {
    var style = document.createElement("link");
    style.rel = "stylesheet";
    style.type = "text/css";
    style.href = nombre;
    var s = document.head.appendChild(style);
    s.onload = onSuccess;
    s.onerror = onError;
}

ItHelpCenter.TUfrRemoteHelp.prototype.Config = function () {
    var _this = this.TParent();
    $.ajax({
        type: "POST",
        async: false,
        url: SysCfg.App.Properties.xRaiz + 'Service/RemoteHelpService/RemoteHelp.svc/RemoteConfig',
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //RemoteOperations         
            _this.hasPermissions = response.d.hasPermissions;

            var isRemoteOperations = 6;
            if (response.d.isIntelVProTechnology == false) {
                $(_this.Grupos[1][3][0].Row.This).css("display", "none");
                isRemoteOperations = isRemoteOperations - 1;
            }
            if (response.d.isSurvey == false) {
                $(_this.Grupos[1][3][1].Row.This).css("display", "none");
                isRemoteOperations = isRemoteOperations - 1;
            }
            if (response.d.isGenerateInventory == false) {
                $(_this.Grupos[1][3][2].Row.This).css("display", "none");
                isRemoteOperations = isRemoteOperations - 1;
            }
            if (response.d.isUpdateAgent == false) {
                $(_this.Grupos[1][3][3].Row.This).css("display", "none");
                isRemoteOperations = isRemoteOperations - 1;
            }
            if (response.d.isChangeID == false) {
                $(_this.Grupos[1][3][4].Row.This).css("display", "none");
                isRemoteOperations = isRemoteOperations - 1;
            }
            if (response.d.isLookDangerous == false) {
                $(_this.Grupos[1][3][5].Row.This).css("display", "none");
                isRemoteOperations = isRemoteOperations - 1;
            }
            if (isRemoteOperations < 1) {
                _this.Grupos[1][4] = false;
            }
            //Conecction
            var isConecction = 2;
            if (response.d.isInternetConnection == false) {
                $(_this.Grupos[2][3][0].Row.This).css("display", "none");
                isConecction = isConecction - 1;
            }
            if (response.d.isConnectbyHosty == false) {
                $(_this.Grupos[2][3][1].Row.This).css("display", "none");
                isConecction = isConecction - 1;
            }
            if (isConecction < 1) {
                _this.Grupos[2][4] = false;
            }
            //Other Options
            var isOtherOptions = 3;
            if (response.d.isDeletePC == false) {
                $(_this.Grupos[3][3][0].Row.This).css("display", "none");
                isOtherOptions = isOtherOptions - 1;
            }
            if (response.d.isInformation == false) {
                $(_this.Grupos[3][3][1].Row.This).css("display", "none");
                isOtherOptions = isOtherOptions - 1;
            }
            if (response.d.isRunSDFile == false) {
                $(_this.Grupos[3][3][2].Row.This).css("display", "none");
                isOtherOptions = isOtherOptions - 1;
            }
            if (isOtherOptions < 1) {
                _this.Grupos[3][4] = false;
            }           
            //Policies
            var isPolicies = 2;
            if (response.d.isUpdatePolicies == false) {
                $(_this.Grupos[4][3][0].Row.This).css("display", "none");
                isPolicies = isPolicies - 1;
            }
            if (response.d.isRestartAllowed == false) {
                $(_this.Grupos[4][3][1].Row.This).css("display", "none");
                isPolicies = isPolicies - 1;
            }
            if (isPolicies < 1) {
                _this.Grupos[4][4] = false;
            }
            //File
            var isFile = 2;
            if (response.d.isSendingFilesandOrders == false) {
                $(_this.Grupos[5][3][0].Row.This).css("display", "none");
                isFile = isFile - 1;
            }
            if (response.d.isRunLocally == false) {
                $(_this.Grupos[5][3][1].Row.This).css("display", "none");
                isFile = isFile - 1;
            }
            if (isFile < 1) {
                _this.Grupos[5][4] = false;
            }
            //Remote Control
            var isRemoteControl = 8;
            if (response.d.isPoweronPC == false) {
                $(_this.Grupos[6][3][0].Row.This).css("display", "none");
                isRemoteControl = isRemoteControl - 1;
            }
            if (response.d.isUseBrowser == false) {
                $(_this.Grupos[6][3][1].Row.This).css("display", "none");
                isRemoteControl = isRemoteControl - 1;
            }
            if (response.d.isTakeControl == false) {
                $(_this.Grupos[6][3][2].Row.This).css("display", "none");
                isRemoteControl = isRemoteControl - 1;
            }
            if (response.d.isJustSee == false) {
                $(_this.Grupos[6][3][3].Row.This).css("display", "none");
                isRemoteControl = isRemoteControl - 1;
            }
            if (response.d.isLockKeyboard == false) {
                $(_this.Grupos[6][3][4].Row.This).css("display", "none");
                isRemoteControl = isRemoteControl - 1;
            }
            if (response.d.isDonotAskforPermission == false) {
                $(_this.Grupos[6][3][5].Row.This).css("display", "none");
                isRemoteControl = isRemoteControl - 1;
            }
            if (response.d.isLowBandwidth == false) {
                $(_this.Grupos[6][3][6].Row.This).css("display", "none");
                isRemoteControl = isRemoteControl - 1;
            }
            if (response.d.isRecordSession == false) {
                $(_this.Grupos[6][3][7].Row.This).css("display", "none");
                isRemoteControl = isRemoteControl - 1;
            }
            if (isRemoteControl < 1) {
                _this.Grupos[6][4] = false;
            }
            //Messaging
            var isMessaging = 3;
            if (response.d.isChat == false) {
                $(_this.Grupos[7][3][0].Row.This).css("display", "none");
                isMessaging = isMessaging - 1;
            }
            if (response.d.isVoice == false) {
                $(_this.Grupos[7][3][1].Row.This).css("display", "none");
                isMessaging = isMessaging - 1;
            }
            if (response.d.isSendMessage == false) {
                $(_this.Grupos[7][3][2].Row.This).css("display", "none");
                isMessaging = isMessaging - 1;
            }
            if (isMessaging < 1) {
                _this.Grupos[7][4] = false;
            }
        },
        error: function (error) {            
            alert('error: ' + error.txtDescripcion + " \n Location: RemoteHelp / RemoteConfig");
        }
    });
}

ItHelpCenter.TUfrRemoteHelp.prototype.AddIconsTitleBar = function () {
    var _this = this.TParent();
    $(_this.TitleBarImgs).html("");
    $(_this.Grupos[1][1]).html("");
    $(_this.Grupos[2][1]).html("");
    $(_this.Grupos[3][1]).html("");
    $(_this.Grupos[4][1]).html("");
    $(_this.Grupos[5][1]).html("");
    $(_this.Grupos[6][1]).html("");
    $(_this.Grupos[7][1]).html("");

    for (var i = 0; i < _this.IconsTitleBar.length; i++) {
        try {            
            if (_this.IconsTitleBar[i][0] == true) {  //valida si se debe mostrar o no la iamgens                
                if (_this.Grupos[_this.IconsTitleBar[i][2]][0] == true) { //valida si se esta agrupada
                    //imagenes agrupadas                    
                    $(_this.Grupos[_this.IconsTitleBar[i][2]][1]).append(_this.IconsTitleBar[i][1]);
                    $(_this.Grupos[_this.IconsTitleBar[i][2]][2]).css("display", "block");
                    $(_this.IconsTitleBar[i][1]).removeClass('RH_imgBarTitle');
                    $(_this.IconsTitleBar[i][1]).addClass('RH_imgBarGroup');
                } else {
                    //imagenes en la barra
                    $(_this.TitleBarImgs).append(_this.IconsTitleBar[i][1]);
                    $(_this.Grupos[_this.IconsTitleBar[i][2]][2]).css("display", "none");
                    $(_this.IconsTitleBar[i][1]).removeClass('RH_imgBarGroup');
                    $(_this.IconsTitleBar[i][1]).addClass('RH_imgBarTitle');                    
                }               
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("RemoteHelp.js ItHelpCenter.TUfrRemoteHelp.prototype.AddIconsTitleBar", e);
        }
    }
  
    //mostrar u ocultar el div donde se ven los iconos no agrupados
    if (_this.TitleBarImgs.childElementCount > 0) {
        $(_this.TitleBarImgs).css("display", "block");
    } else {
        $(_this.TitleBarImgs).css("display", "none");
    }
}
 


//FUNCIONES//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
ItHelpCenter.TUfrRemoteHelp.prototype.BtnActivation = function (Obj, img) {
    var _this = this.TParent();
    if (Obj.This.className.match(/Active*/)) {
        $(Obj.This).removeClass('Active');
        $(img).removeClass('ImgActive');        
    } else {
        $(Obj.This).addClass('Active');
        $(img).addClass('ImgActive');       
    }
}
ItHelpCenter.TUfrRemoteHelp.prototype.ChkActivation = function (Chk, img, Obj, call) {
    var _this = this.TParent();
    if (call == false) { //si la vaiable call llega en true es que esta siendo llamado directamente desde el check
        if (Chk.Checked) {
            Chk.Checked = false;
        } else {
            Chk.Checked = true;
        }
    }
 
    if (Chk.Checked == false) {
        $(Obj.This).removeClass('btnActive');
        $(img).removeClass('ImgActive');        
    } else {
        $(Obj.This).addClass('btnActive');
        $(img).addClass('ImgActive');
        $(Obj.This).css(' background-color', '#f39c12');
    }
}

//OUPPUT
ItHelpCenter.TUfrRemoteHelp.prototype.RHSelectOutput = function () {
    var _this = this.TParent();

    //Mostrar pantalla opciones extras
    $(_this.DivBodyC).html("");

    var rhRun = new RHSelectOutput(_this.DivBodyC, "", _this.DBTranslate, _this.Mythis);

    rhRun.EnabledButtonITHC = true;
    rhRun.EnabledButtonATIS = true;
    rhRun.EnabledButtonRH = true;


    rhRun.OnClickClose = function (objRun, btn) {
        _this.NavVisible = false;
        $(_this.DivBodyC).slideToggle("slow", function () {
            $(_this.DivBodyC).css("display", "none");
        });      

        //Cerrar Remote
        _this.NavVisible = false;
        $(_this.DivBodyC).css("display", "none");
        _this.Modal.CloseModal();
         
    }

    rhRun.OnClickITHC = function (objRun, btn) {       
        if (objRun.OptionRun.Value == "In Browser") {
            _this.RHREQUESTOPTIONS.IDRHTYPEEXECUTE = Persistence.RemoteHelp.Properties.TRHTYPEEXECUTE._BROWSER.value;
        } else {
            _this.RHREQUESTOPTIONS.IDRHTYPEEXECUTE = Persistence.RemoteHelp.Properties.TRHTYPEEXECUTE._WIN32.value;
        }

        _this.RHREQUESTOPTIONS.IDRHMODULE_EXECUTE = Persistence.RemoteHelp.Properties.TRHMODULE._ITHelpCenter.value;
       // rhRun.Modal.CloseModal();
        _this.NavVisible = false;
        $(_this.DivBodyC).slideToggle("slow", function () {
            $(_this.DivBodyC).css("display", "none");
        });

        //ENVIAR
        _this.OnRemoteClick(_this, "OK", "Button");

        //Cerrar Remote
        _this.NavVisible = false;
        $(_this.DivBodyC).css("display", "none");
        _this.Modal.CloseModal();
    }

    rhRun.OnClickATIS = function (objRun, btn) {
        if (objRun.OptionRun.Value == "In Browser") {
            _this.RHREQUESTOPTIONS.IDRHTYPEEXECUTE = Persistence.RemoteHelp.Properties.TRHTYPEEXECUTE._BROWSER.value;
        } else {
            _this.RHREQUESTOPTIONS.IDRHTYPEEXECUTE = Persistence.RemoteHelp.Properties.TRHTYPEEXECUTE._WIN32.value;
        }
        _this.RHREQUESTOPTIONS.IDRHMODULE_EXECUTE = Persistence.RemoteHelp.Properties.TRHMODULE._ATIS.value;
        //rhRun.Modal.CloseModal();
        _this.NavVisible = false;
        $(_this.DivBodyC).slideToggle("slow", function () {
            $(_this.DivBodyC).css("display", "none");
        });

        //ENVIAR
        _this.OnRemoteClick(_this, "OK", "Button");

        //Cerrar Remote
        _this.NavVisible = false;
        $(_this.DivBodyC).css("display", "none");
        _this.Modal.CloseModal();
    }

    rhRun.OnClickRH = function (objRun, btn) {
        if (objRun.OptionRun.Value == "In Browser") {
            _this.RHREQUESTOPTIONS.IDRHTYPEEXECUTE = Persistence.RemoteHelp.Properties.TRHTYPEEXECUTE._BROWSER.value;
        } else {
            _this.RHREQUESTOPTIONS.IDRHTYPEEXECUTE = Persistence.RemoteHelp.Properties.TRHTYPEEXECUTE._WIN32.value;
        }
        _this.RHREQUESTOPTIONS.IDRHMODULE_EXECUTE = Persistence.RemoteHelp.Properties.TRHMODULE._RemoteHelp.value;
       // rhRun.Modal.CloseModal();
    

        //ENVIAR
        _this.OnRemoteClick(_this, "OK", "Button");

        //Cerrar Remote
        _this.NavVisible = false;
        $(_this.DivBodyC).css("display", "none");
        _this.Modal.CloseModal();
    }
    rhRun.Show();    

}

ItHelpCenter.TUfrRemoteHelp.prototype.importarScript = function (nombre, onSuccess, onError) {
    var s = document.createElement("script");
    s.onload = onSuccess;
    s.onerror = onError;
    s.src = nombre;
    document.querySelector("head").appendChild(s);
}
