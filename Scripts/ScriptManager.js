﻿ItHelpCenter.TScriptManager = function () {
    this.TParent = function () {
        return this;
    }.bind(this);

    this.Divice = "";
    this.Path = "";
    this.PathDivice = "";
    this.PathCSS = "";
    this.Validate = true;    
}

ItHelpCenter.TScriptManager.prototype.GetStyle = function (inPath) { //SD/compor/sadsad.css
    var _this = this.TParent();
    _this.Path = inPath;

    _this.Divice = SysCfg.Properties.Device;
    if (_this.Divice == "Tablet") {
        _this.PathDivice = SysCfg.Properties.TDevice.Tablet;
    } else if (_this.Divice == "Mobile") {
        _this.PathDivice = SysCfg.Properties.TDevice.Mobile;
    } else {
        _this.PathDivice = SysCfg.Properties.TDevice.Destok;
    }
    
    _this.PathCSS = SysCfg.App.Properties.xRaiz + "Css" + _this.PathDivice + _this.Path;

    //validar si el archivo buscado existe  
    _this.fileExist(_this.PathCSS);
   
    if (_this.Validate == false)
    {        
       // Caragar el estilo default
        _this.PathCSS = SysCfg.App.Properties.xRaiz + "Css/" + _this.Path;
    }

    _this.fileExist(_this.PathCSS);

    if (_this.Validate == false) {
        alert("Style file not found. Path: " & _this.Path)
        _this.PathCSS = "";
    }
    return _this.PathCSS;
}

ItHelpCenter.TScriptManager.prototype.fileExist = function (PathCSS) {
    var _this = this.TParent();
    try {
        $.ajax({
            url: PathCSS,
            type: 'HEAD',
            async:false,
            error: function () {               
                _this.Validate = false;
            },
            success: function () {               
                _this.Validate = true;
            }
        });
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ScriptManager.js ItHelpCenter.TScriptManager.prototype.fileExist", e);
        _this.Validate = false;
    }
}

ItHelpCenter.TScriptManager.prototype.AddStyleFile = function (path, inID, onSuccess, onError) {
    var _this = this.TParent();
  

    if (path == "") {
        return;
    }
    //try {
    //    hoja_estilos = document.getElementById(inID);
    //    if (hoja_estilos != null) {
    //        hoja_estilos.parentNode.removeChild(_this.Path);
    //    }       
    //} catch (e) {
    //    SysCfg.Log.Methods.WriteLog("ScriptManager.js ItHelpCenter.TScriptManager.prototype.AddStyleFile path=" + path + ", inID=" + inID + " ", e);
    //}  

    try {
        var style = document.createElement("link");
        style.rel = "stylesheet";
        style.type = "text/css";
        style.id = inID;
        style.href = path;
        var s = document.head.appendChild(style);
        s.onload = onSuccess;
        s.onerror = onError;
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ScriptManager.js ItHelpCenter.TScriptManager.prototype.AddStyleFile", e);

    }
   
}