﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frLoguinConfig.aspx.cs" Inherits="ITHelpCenter.frLoguinConfig" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="UTF-8" />
    <title>Splash</title>
    <link href="Css/style.css" rel="stylesheet" />
    <script src="Scripts/jquery2-1-4min.js"></script>
    <script src="frLoguinConfig.js"></script>
    
</head>
<body>
    <div class="login">
        <div class="login-triangle"></div>
        <h2 id="title1" class="login-header">IT Help Center - Server Host</h2>
        <form id="form1" runat="server" class="login-container">
            <div id="serverhost">
                <p><input id="txtHost1" type="text" placeholder="Server Host" /></p>
                <div>
                    <div style="float:left;margin:0 auto;width:50%">
                        <p><input id="btnSave" type="submit" value="Save"/></p>
                    </div>
                    <div style="float:right;margin:0 auto;width:50%">
                        <p><input id="btnCancel" type="submit" value="Cancel"/></p>
                    </div>
                </div>
                

            </div>
        </form>
    </div>
</body>
</html>
