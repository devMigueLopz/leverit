﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CloseSession.aspx.cs" Inherits="ITHelpCenter.CloseSession" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- 
        viewport   //Resolucion para moviles
        content="width=device-width    // Ancho del sistema a la resolucion del ancho del dispositivo
        initial-scale=1.0"            //Escala del zoom
        
        -->
    <title></title>
    <link href="Componet/BootStrap_Source/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="Componet/BootStrap_Source/bootstrap-table/src/bootstrap-table.css" rel="stylesheet">
    <link href="Scripts/jquery-ui.min.css" rel="stylesheet">
    <link href="Scripts/jquery-ui-timepicker-addon.css" rel="stylesheet">
    <link href="Css/main.css" rel="stylesheet">
    <link id="linkestilo" href="Css/Main_StyleA.css" rel="stylesheet">
    <link href="Css/Login.css" rel="stylesheet">
    <link href="Css/ChangePassword.css" rel="stylesheet">
    <link href="Css/LoginConfig.css" rel="stylesheet">
    <link href="Css/Component/Tooltip.css" rel="stylesheet">

    <script src="Scripts/jquery2-1-4min.js"></script>
    <script src="Scripts/jquery-ui.min.js"></script>
    <script src="Scripts/jquery-ui-timepicker-addon.js"></script>
    <script src="Scripts/jquery.mousewheel.js"></script>
    <script src="Componet/BootStrap_Source/bootstrap/js/bootstrap.min.js"></script>
    <script src="Componet/BootStrap_Source/bootstrap-table/src/bootstrap-table.js"></script>    

    <link href="SD/CaseUser/Atention/DynamicSTEFAtentionCase.css" rel="stylesheet">
    <link href="SD/CaseManager/Atention/CaseAtention.css" rel="stylesheet">
    <link href="Css/index.css" rel="stylesheet">
    <link href="Css/Component/UCode/VCL/GridControlCF/ionicons/css/ionicons.css" rel="stylesheet">
    <link href="Css/Component/UCode/VCL/GridControlCF/loader.css" rel="stylesheet">
    <link href="Css/SystemStyle.css" rel="stylesheet">


    <style type="text/css">
        /*menu a copiar a en alguna css*/
        .menuContenedorMobil {
            color: #cccccc !important;
        }

            .menuContenedorMobil label {
                color: #cccccc !important;
            }

            .menuContenedorMobil button {
                background-color: transparent !important;
                border: 1px solid #cccccc !important;
            }
        /*fin de menu*/
        .ColorTab > a {
            color: #fff;
        }
        /* Large Devices, Wide Screens */
        @media all and (min-width : 1200px) {

            #colIzquierda {
                width: 265px;
            }

            #colDerecha {
                width: calc(100% - 265px);
            }

            #asideleft_idmain {
                width: 265px;
            }

            #Cont2_idmain_0 {
                width: calc(100% - 265px);
                background: rgb(236,240,245);
                padding-left: 0px;
                padding-right: 0px;
                min-height: 100%;
            }

                #Cont2_idmain_0[class~="burger_invisible"] {
                    width: calc(100%);
                }


            #asideright_idmain {
                width: 250px;
            }

            #main_idmain {
                width: calc(100% - 250px);
            }

                #main_idmain[class~="wheel_invisible"] {
                    width: calc(100%);
                }

            .v-p6 {
                display: normal;
            }

            .h-p6 {
                display: none;
            }
        }

        @media (min-width : 993px) and (max-width : 1199px) {
            #colIzquierda {
                width: 265px;
            }

            #colDerecha {
                width: calc(100% - 265px);
            }

            #asideleft_idmain {
                width: 265px;
            }

            #Cont2_idmain_0 {
                width: calc(100% - 265px);
                background: rgb(236,240,245);
                padding-left: 0px;
                padding-right: 0px;
                min-height: 100%;
            }

                #Cont2_idmain_0[class~="burger_invisible"] {
                    width: calc(100%);
                    height: 100%;
                }

                #Cont2_idmain_0[class~="burger_visible"] {
                    width: calc(100% - 250px);
                }

            #asideright_idmain {
                width: 250px;
                display: none;
            }

            #main_idmain {
                width: calc(100%);
            }

                #main_idmain[class~="wheel_invisible"] {
                    width: calc(100%);
                }

                #main_idmain[class~="wheel_visible"] {
                    width: calc(100% - 250px);
                }

            .v-p5 {
                display: normal;
            }

            .h-p5 {
                display: none;
            }
        }

        /* Medium Devices, Desktops */
        @media (min-width : 769px) and (max-width : 992px) {
            #colIzquierda {
                width: 265px;
            }

            #colDerecha {
                width: calc(100% - 265px);
            }

            #asideleft_idmain {
                width: 265px;
            }

            #Cont2_idmain_0 {
                width: calc(100% - 265px);
                background: rgb(236,240,245);
                padding-left: 0px;
                padding-right: 0px;
                min-height: 100%;
            }

                #Cont2_idmain_0[class~="burger_invisible"] {
                    width: calc(100%);
                }

                #Cont2_idmain_0[class~="burger_visible"] {
                    width: calc(100% - 250px);
                    display: none;
                }

            #asideright_idmain {
                width: 250px;
                display: none;
            }

            #main_idmain {
                width: calc(100%);
            }

                #main_idmain[class~="wheel_invisible"] {
                    width: calc(100%);
                }

                #main_idmain[class~="wheel_visible"] {
                    width: calc(100% - 250px);
                    display: none;
                }

            .v-p4 {
                display: normal;
            }

            .h-p4 {
                display: none;
            }
        }

        /* Small Devices, Tablets */
        @media (min-width : 481px) and (max-width : 768px) {
            #colIzquierda {
                width: 100%;
            }

            #colDerecha {
                width: 100%;
            }

            #asideleft_idmain {
                width: 100%;
                display: none;
            }

            #Cont2_idmain_0 {
                width: 100%;
                background: rgb(236,240,245);
                padding-left: 0px;
                padding-right: 0px;
                min-height: 100%;
            }

                #Cont2_idmain_0[class~="burger_invisible"] {
                    width: calc(100%);
                }

                #Cont2_idmain_0[class~="burger_visible"] {
                    width: calc(100% - 250px);
                    display: none;
                }

            #asideright_idmain {
                width: 100%;
                display: none;
            }

            #main_idmain {
                width: calc(100%);
            }

                #main_idmain[class~="wheel_invisible"] {
                    width: calc(100%);
                }

                #main_idmain[class~="wheel_visible"] {
                    width: calc(100% - 250px);
                    display: none;
                }

            .v-p3 {
                display: normal;
            }

            .h-p3 {
                display: none;
            }
        }

        /* Extra Small Devices, Phones */
        @media (min-width : 321px) and (max-width : 480px) {
            #colIzquierda {
                width: 100%;
            }

            #colDerecha {
                width: 100%;
            }

            #asideleft_idmain {
                width: 100%;
                display: none;
            }

            #Cont2_idmain_0 {
                width: 100%;
                background: rgb(236,240,245);
                padding-left: 0px;
                padding-right: 0px;
                min-height: 100%;
            }

                #Cont2_idmain_0[class~="burger_invisible"] {
                    width: calc(100%);
                }

                #Cont2_idmain_0[class~="burger_visible"] {
                    width: calc(100% - 250px);
                    display: none;
                }

            #asideright_idmain {
                width: 100%;
                display: none;
            }

            #main_idmain {
                width: calc(100%);
            }

                #main_idmain[class~="wheel_invisible"] {
                    width: calc(100%);
                }

                #main_idmain[class~="wheel_visible"] {
                    width: calc(100% - 250px);
                    display: none;
                }

            .v-p2 {
                display: normal;
            }

            .h-p2 {
                display: none;
            }
        }

        /* Custom, iPhone Retina */
        @media (min-width : 1px) and (max-width : 320px) {
            #colIzquierda {
                width: 100%;
            }

            #colDerecha {
                width: 100%;
            }

            #asideleft_idmain {
                width: 100%;
                display: none;
            }

            #Cont2_idmain_0 {
                width: 100%;
                background: rgb(236,240,245);
                padding-left: 0px;
                padding-right: 0px;
                min-height: 100%;
            }

                #Cont2_idmain_0[class~="burger_invisible"] {
                    width: calc(100%);
                }

                #Cont2_idmain_0[class~="burger_visible"] {
                    width: calc(100% - 250px);
                    display: none;
                }

            #asideright_idmain {
                width: 100%;
                display: none;
            }

            #main_idmain {
                width: calc(100%);
            }

                #main_idmain[class~="wheel_invisible"] {
                    width: calc(100%);
                    /*display:none;*/
                }

                #main_idmain[class~="wheel_visible"] {
                    width: calc(100% - 250px);
                    /*display:none;*/
                }

            .v-p1 {
                display: normal;
            }

            .h-p1 {
                display: none;
            }
        }
    </style>
</head>
<body>
    <div id="IndexContainer">
        <div id="IndexDiv1">
            </div>
        <div id="IndexDiv2">
            <img id="IndexLogo" src="image/High/IT-Help-Center-Final file.png"></div>
        <div id="IndexDiv3">
            <label id="IndexTitle"></label></div>
        <div id="IndexDiv4">
            <label id="IndexInfo">Successful session finished</label></div>
        <div id="IndexDiv5">
            <img id="Indexicon1" style="cursor: pointer;" src="image/48/user-orange.png" title="Login">
            <img id="Indexicon2" style="display:none; border:none;" src="image/Anime/waiting-wheel.gif" width="72" >
    </div>

        <script type="text/javascript">
            $(document).ready(function () {
                $("#Indexicon1").click(function () {
                    $(this).fadeOut(100, function () {
                        $("#Indexicon2").fadeIn(100, function () {
                            location.href = "Index.aspx";
                        });
                    });                   
                });
            });           

        </script>
</body>

   
</html>
