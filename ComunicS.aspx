﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ComunicS.aspx.cs" Inherits="ITHelpCenter.ComunicS" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <script src="Scripts/jquery2-1-4min.js"></script>
    <script src="Scripts/namespace.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            StrProtocolToDateTime = function (DateTimeStr) {
                var Res = new Date(1970, 0, 1, 0, 0, 0);
                try {
                    var dy = parseInt(DateTimeStr.substring(0, 2));
                    var mn = parseInt(DateTimeStr.substring(2, 4)) - 1;
                    var yy = parseInt(DateTimeStr.substring(4, 8));
                    var hh = parseInt(DateTimeStr.substring(8, 10));
                    var mm = parseInt(DateTimeStr.substring(10, 12));
                    var ss = parseInt(DateTimeStr.substring(12, 14));
                    Res = new Date(yy, mn, dy, hh, mm, ss, 0);
                } catch (e) {

                }
                return(Res);
            }

            DateTimeToStrProtocol = function (inValueDateTime2) {
                var ValueDateTime2 = inValueDateTime2;
                var dy = ValueDateTime2.getDate().toString();
                if (dy.length == 1) dy = "0" + dy;
                var mn = (ValueDateTime2.getMonth() + 1).toString();
                if (mn.length == 1) mn = "0" + mn;
                var yy = ValueDateTime2.getFullYear().toString();
                if (yy.length == 1) yy = "000" + yy;
                if (yy.length == 2) yy = "00" + yy;
                if (yy.length == 3) yy = "0" + yy;
                var hh = ValueDateTime2.getHours().toString();
                if (hh.length == 1) hh = "0" + hh;
                var mm = ValueDateTime2.getMinutes().toString();
                if (mm.length == 1) mm = "0" + mm;
                var ss = ValueDateTime2.getSeconds().toString();
                if (ss.length == 1) ss = "0" + ss;
                var DateTimeStr = dy + mn + yy + hh + mm + ss;
                return (DateTimeStr);
            }


            GetEnum = function (Param, Objet) {
                for (var item in Objet) {
                    if (typeof (Param) == 'string') {
                        if (Objet[item].name == Param)
                            return Objet[item];
                    }
                    if (typeof (Param) == 'number') {
                        if (Objet[item].value == Param)
                            return Objet[item];
                    }
                }
                return undefined;
            }

      


            TDataType = {
                GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
                Unknown: { value: 0, name: "Unknown" },
                String: { value: 1, name: "String" },
                Int32: { value: 2, name: "Int32" },
                Boolean: { value: 3, name: "Boolean" },
                //AutoInc: { value: 4, name: "AutoInc" },
                Text: { value: 5, name: "Text" },
                Double: { value: 6, name: "Double" },
                DateTime: { value: 7, name: "DateTime" },
                //Object: { value: 8, name: "Object" },
                Decimal: { value: 9, name: "Decimal" },
            }
    
        //DIVS
        var iDiv1 = document.createElement('div');
        iDiv1.id = 'Elements';
        iDiv1.style.minHeight = "50px";
        iDiv1.style.border = "1px solid grey";
        document.getElementsByTagName('body')[0].appendChild(iDiv1);

        var iDiv1_1 = document.createElement('div');
        iDiv1_1.id = 'DivBotones1';
        iDiv1_1.style.minHeight = "50px";
        iDiv1.appendChild(iDiv1_1);

        var iDiv1_2 = document.createElement('div');
        iDiv1_2.id = 'DivArea';
        iDiv1_2.style.minHeight = "50px";
        iDiv1_2.style.paddingTop = "10px";
        iDiv1_2.style.paddingBottom = "40px";
        iDiv1.appendChild(iDiv1_2);

        var iDiv1_3 = document.createElement('div');
        iDiv1_3.id = 'DivBotones2';
        iDiv1_3.style.minHeight = "50px";
        iDiv1_3.style.paddingTop = "40px";
        iDiv1_3.style.borderTop = "1px solid grey";
        iDiv1.appendChild(iDiv1_3);

        var iDiv1_4 = document.createElement('div');
        iDiv1_4.id = 'DivTextos';
        iDiv1_4.style.minHeight = "50px";
        iDiv1_4.style.paddingTop = "10px";
        iDiv1.appendChild(iDiv1_4);

        var iDiv2 = document.createElement('div');
        iDiv2.id = 'Grid';
        iDiv2.style.minHeight = "50px";
        iDiv2.style.border = "1px solid grey";
        document.getElementsByTagName('body')[0].appendChild(iDiv2);



        //***********************    OPENSQL prefix   ***********************
        var btn1 = document.createElement("Button");
        btn1.id = "OpenSQL";
        btn1.textContent = "Open Prefix";
        btn1.style.marginRight = "5px";
        btn1.onclick = function () {
            var Divshow = document.getElementById('Grid');
            _params = new Array();
            Parametro1 = { ParamInt: TDataType.String.value, ParamnName: "FDCADENA", ParamString: "UnString", isWhere:false };
            Parametro2 = { ParamInt: TDataType.Text.value, ParamnName: "FDTEXTO", ParamString: "UnTexto", isWhere:false };
            Parametro3 = { ParamInt: TDataType.Int32.value, ParamnName: "FDENTRO", ParamString: "1", isWhere:false };
            Parametro4 = { ParamInt: TDataType.Double.value, ParamnName: "FDFLOTANTE", ParamString: "1.1", isWhere:false };
            Parametro5 = { ParamInt: TDataType.Decimal.value, ParamnName: "DECIMAL", ParamString: "2.2", isWhere:false };
            Parametro6 = { ParamInt: TDataType.Boolean.value, ParamnName: "FDBINARIO", ParamString: "0", isWhere: false };//0 false 1 true
            Parametro7 = { ParamInt: TDataType.DateTime.value, ParamnName: "FDDATETIME", ParamString: DateTimeToStrProtocol(new Date(2017, 0, 1, 10, 10, 10)), isWhere:false };
            Parametro8 = { ParamInt: TDataType.Unknown.value, ParamnName: "UNKNOWN", ParamString: "cualquier texto no agrega contenido extra", isWhere: false };
            _params.push(Parametro1);
            _params.push(Parametro2);
            _params.push(Parametro3);
            _params.push(Parametro4);
            _params.push(Parametro5);
            _params.push(Parametro6);
            _params.push(Parametro7);
            _params.push(Parametro8);

            var data = {
                'Prefix': inputTx1.value,
                'ID': inputTx2.value,
                'Params': _params
            };
            $.ajax({
                type: "POST",
                url: SysCfg.App.Properties.xRaiz + 'Service/ComunicServiceS/ComunicMethodsS.svc/OpenSQLPrefix',
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (response) {                   
                    if (response.NoError == true) {
                        $(Divshow).html("");
                        creartabla(response);
                    }
                    else {
                        $(Divshow).html("");
                    }

                },
                error: function (response) {
                    alert(response);
                }
            });
        }



        
        //***********************    OPENSQL query    ***********************
        var btn2 = document.createElement("Button");
        btn2.id = "OpenAtis";
        btn2.textContent = "Open SQL";
        btn2.style.marginRight = "5px";
        btn2.onclick = function () {
            var Divshow = document.getElementById('Grid');
            _params = new Array();
            Parametro1 = { ParamInt: TDataType.String.value, ParamnName: "FDCADENA", ParamString: "UnString", isWhere: false };
            Parametro2 = { ParamInt: TDataType.Text.value, ParamnName: "FDTEXTO", ParamString: "UnTexto", isWhere: false };
            Parametro3 = { ParamInt: TDataType.Int32.value, ParamnName: "FDENTRO", ParamString: "1", isWhere: false };
            Parametro4 = { ParamInt: TDataType.Double.value, ParamnName: "FDFLOTANTE", ParamString: "1.1", isWhere: false };
            Parametro5 = { ParamInt: TDataType.Decimal.value, ParamnName: "DECIMAL", ParamString: "2.2", isWhere: false };
            Parametro6 = { ParamInt: TDataType.Boolean.value, ParamnName: "FDBINARIO", ParamString: "0", isWhere: false };//0 false 1 true
            Parametro7 = { ParamInt: TDataType.DateTime.value, ParamnName: "FDDATETIME", ParamString: DateTimeToStrProtocol(new Date(2017, 0, 1, 10, 10, 10)), isWhere: false };
            Parametro8 = { ParamInt: TDataType.Unknown.value, ParamnName: "UNKNOWN", ParamString: "cualquier texto no agrega contenido extra", isWhere: false };
            _params.push(Parametro1);
            _params.push(Parametro2);
            _params.push(Parametro3);
            _params.push(Parametro4);
            _params.push(Parametro5);
            _params.push(Parametro6);
            _params.push(Parametro7);
            _params.push(Parametro8);

            var data = {
                SQLstr: input.value,
                Params: _params
            };
            $.ajax({
                type: "POST",
                url: SysCfg.App.Properties.xRaiz + 'Service/ComunicServiceS/ComunicMethodsS.svc/OpenSQL',
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (response) {
                    if (response.NoError == true) {
                        $(Divshow).html("");
                        creartabla(response);
                    }
                    else {
                        $(Divshow).html("");
                    }

                },
                error: function (response) {
                    alert(response);
                }
            });
        }

        //***********************    ExecSQL query  ***********************
        var btn3 = document.createElement("Button");
        btn3.id = "ExecuteSQL";
        btn3.textContent = "Execute SQL";
        btn3.style.marginRight = "5px";
        btn3.onclick = function () {
            var Divshow = document.getElementById('Grid');
            _params = new Array();
            Parametro1 = { ParamInt: TDataType.String.value, ParamnName: "FDCADENA", ParamString: "UnString", isWhere: false };
            Parametro2 = { ParamInt: TDataType.Text.value, ParamnName: "FDTEXTO", ParamString: "UnTexto", isWhere: false };
            Parametro3 = { ParamInt: TDataType.Int32.value, ParamnName: "FDENTRO", ParamString: "1", isWhere: false };
            Parametro4 = { ParamInt: TDataType.Double.value, ParamnName: "FDFLOTANTE", ParamString: "1.1", isWhere: false };
            Parametro5 = { ParamInt: TDataType.Decimal.value, ParamnName: "DECIMAL", ParamString: "2.2", isWhere: false };
            Parametro6 = { ParamInt: TDataType.Boolean.value, ParamnName: "FDBINARIO", ParamString: "0", isWhere: false };//0 false 1 true
            Parametro7 = { ParamInt: TDataType.DateTime.value, ParamnName: "FDDATETIME", ParamString: DateTimeToStrProtocol(new Date(2017, 0, 1, 10, 10, 10)), isWhere: false };
            Parametro8 = { ParamInt: TDataType.Unknown.value, ParamnName: "UNKNOWN", ParamString: "cualquier texto no agrega contenido extra", isWhere: false };
            _params.push(Parametro1);
            _params.push(Parametro2);
            _params.push(Parametro3);
            _params.push(Parametro4);
            _params.push(Parametro5);
            _params.push(Parametro6);
            _params.push(Parametro7);
            _params.push(Parametro8);


            var data = {
                'SQLstr': input.value,
                'Params': _params
            };
            $.ajax({
                type: "POST",
                url: SysCfg.App.Properties.xRaiz + 'Service/ComunicServiceS/ComunicMethodsS.svc/ExecSQL',
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (response) {
                    $(Divshow).html("");                  
                    if (response.NoError == true) {
                        alert("La ejecución del QUERY fue exitosa");
                    }
                    else {
                        alert("Error en la Ejecución del QUERY");
                        var newlabelErr = document.createElement("Label");
                        newlabelErr.innerText = response.ResErr.Mesaje;
                        Divshow.appendChild(newlabelErr);
                    }
                },
                error: function (response) {
                    alert(response);
                }
            });
        }
        

        //*************************     ExecSQL Prefix    ***********************
        var btn4 = document.createElement("Button");
        btn4.id = "ExecutePrefix";
        btn4.textContent = "Execute Prefix";
        btn4.style.marginRight = "5px";
        btn4.onclick = function () {

            var Divshow = document.getElementById('Grid');
            _params = new Array();
            Parametro1 = { ParamInt: TDataType.String.value, ParamnName: "FDCADENA", ParamString: "UnString", isWhere: false };
            Parametro2 = { ParamInt: TDataType.Text.value, ParamnName: "FDTEXTO", ParamString: "UnTexto", isWhere: false };
            Parametro3 = { ParamInt: TDataType.Int32.value, ParamnName: "FDENTRO", ParamString: "1", isWhere: false };
            Parametro4 = { ParamInt: TDataType.Double.value, ParamnName: "FDFLOTANTE", ParamString: "1.1", isWhere: false };
            Parametro5 = { ParamInt: TDataType.Decimal.value, ParamnName: "DECIMAL", ParamString: "2.2", isWhere: false };
            Parametro6 = { ParamInt: TDataType.Boolean.value, ParamnName: "FDBINARIO", ParamString: "0", isWhere: false };//0 false 1 true
            Parametro7 = { ParamInt: TDataType.DateTime.value, ParamnName: "FDDATETIME", ParamString: DateTimeToStrProtocol(new Date(2017, 0, 1, 10, 10, 10)), isWhere: false };
            Parametro8 = { ParamInt: TDataType.Unknown.value, ParamnName: "UNKNOWN", ParamString: "cualquier texto no agrega contenido extra", isWhere: false };
            _params.push(Parametro1);
            _params.push(Parametro2);
            _params.push(Parametro3);
            _params.push(Parametro4);
            _params.push(Parametro5);
            _params.push(Parametro6);
            _params.push(Parametro7);
            _params.push(Parametro8);


            var data = {
                'Prefix': inputTx1.value,
                'ID': inputTx2.value,
                'Params': _params
            };
            $.ajax({
                type: "POST",
                url: SysCfg.App.Properties.xRaiz + 'Service/ComunicServiceS/ComunicMethodsS.svc/ExecSQLPrefix',
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (response) {
                    $(Divshow).html("");
                    if (response.NoError == true) {
                        alert("La ejecución del QUERY fue exitosa");
                    }
                    else {
                        alert("Error en la Ejecución del QUERY");
                        var newlabelErr = document.createElement("Label");
                        newlabelErr.innerText = response.ResErr.Mesaje;
                        Divshow.appendChild(newlabelErr);
                    }
                },
                error: function (response) {
                    alert(response);
                }
            });

        }

        var newlabel1 = document.createElement("Label");
        newlabel1.innerText = "Query SQL"

        var input = document.createElement('TEXTAREA');
        input.setAttribute('name', 'post');
        input.setAttribute('maxlength', 250);
        input.setAttribute('cols', 80);
        input.setAttribute('rows', 4);
        input.innerText = "SELECT * FROM CMDBCI"

        var newlabel2 = document.createElement("Label");
        newlabel2.innerText = "Prefix Atis"
        var inputTx1 = document.createElement('input');
        inputTx1.type = "text";
        inputTx1.innerText = "Demo";
        inputTx1.style.marginRight = "10px";

        var newlabel3 = document.createElement("Label");
        newlabel3.innerText = "ID Atis"
        var inputTx2 = document.createElement('input');
        inputTx2.type = "text";
        inputTx2.innerText = "DMTABLA_GETID";

        var DivElements1 = document.getElementById('DivBotones1');      
        DivElements1.appendChild(btn2);
        DivElements1.appendChild(btn3);
       
        var DivElements2 = document.getElementById('DivArea');
        DivElements2.appendChild(newlabel1);
        DivElements2.appendChild(input);

        var DivElements3 = document.getElementById('DivBotones2');
        DivElements3.appendChild(btn1);
        DivElements3.appendChild(btn4);

        var DivElements4 = document.getElementById('DivTextos');
        DivElements4.appendChild(newlabel2);
        DivElements4.appendChild(inputTx1);
        DivElements4.appendChild(newlabel3);
        DivElements4.appendChild(inputTx2);

        function creartabla(DataTableJs) {

            var num_rows = DataTableJs.Rows.length;
            var num_cols = DataTableJs.Columns.length;
            var theader = "<table id='table1' style='border: 1px solid blue'><thead>";
            for (var j = 0; j < num_cols; j++) {
                theader += "<th>";
                theader += DataTableJs.Columns[j].ColumnName;
                theader += "</th>"
            }
            theader += "</thead>";
            var tbody = "";

            for (var i = 0; i < num_rows; i++) {
                tbody += "<tr>";
                for (var j = 0; j < num_cols; j++) {
                    tbody += "<td>";
                    if (TDataType.GetEnum(DataTableJs.Columns[j].DataType) == TDataType.DateTime) {
                        tbody += StrProtocolToDateTime(DataTableJs.Rows[i].Cells[j]);
                        //tbody += "None";
                    }
                    else {

                    
                        tbody += DataTableJs.Rows[i].Cells[j];
                    }
                    tbody += "</td>"
                }
                tbody += "</tr>";
            }
            var tfooter = "</table>";

            iDiv2.innerHTML = theader + tbody + tfooter;
        }
     
    })
    </script>
</head>
<body id ="IDComunicS">
</body>
</html>
