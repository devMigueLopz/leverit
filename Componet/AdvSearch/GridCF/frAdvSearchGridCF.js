﻿

        //public void Inicialize(string inCOD_SEARCH, string Prefix, string ID, Byte[] SQLPARAM)
        //{
        //    Inicialize(inCOD_SEARCH, SysCfg.DB.SQL.Methods.GetSQLPrefixIDBuild(Prefix, ID, SQLPARAM).StrSQL);          
        //}

Componet.AdvSearch.TfrAdvSearchGridCF = function (inObject, incallback) {
    this.Object = inObject;
    this.Callback = incallback;

    this.TParent = function () {
        return this;
    }.bind(this);

    var _this = this.TParent();
    this.Mythis = "frAdvSearch";
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lblShow", "Show: ");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lblSelectColumnSearch", "Select column to search: ");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lblSearch", "Search: ");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lblUseResult", "Use this result");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnClickHere", "Test");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1", "Show all");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2", "Search");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3", "(ALL)");

    _this.InitializeComponent();
}
    Componet.AdvSearch.TfrAdvSearchGridCF.prototype.InitializeComponent = function () {
        var _this = this.TParent();
        var ObjHtml = _this.Object;
        //construir el contenido
        //var GridMainContArr = new Array(1); // container
        //GridMainContArr[0] = new Array(2); // Filas principales 
        //GridMainContArr[0][0] = new Array(3); // columnas  
        var TagMainContDef_Content = new Array(1); // container
        TagMainContDef_Content[0] = new Array(4); // row
        TagMainContDef_Content[0][0] = new Array(2); // col
        TagMainContDef_Content[0][1] = new Array(2); // col
        TagMainContDef_Content[0][2] = new Array(2); // col
        TagMainContDef_Content[0][3] = new Array(1); // col
        TagMainContDef_Content[0][4] = new Array(2); // col

        var combosearch = "";
        //var GridContaint = VclDivitions(ObjHtml, "GridMainCont", GridMainContArr);
        var TagMainCont = VclDivitions(ObjHtml, "TagMainCont_Content", TagMainContDef_Content);
        BootStrapCreateRows(TagMainCont[0].Child);
        BootStrapCreateColumns(TagMainCont[0].Child[0].Child, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
        BootStrapCreateColumns(TagMainCont[0].Child[1].Child, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
        BootStrapCreateColumns(TagMainCont[0].Child[2].Child, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);
        BootStrapCreateColumns(TagMainCont[0].Child[3].Child, [[10, 2], [10, 2], [10, 2], [10, 2], [10, 2]]);
        BootStrapCreateColumns(TagMainCont[0].Child[4].Child, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);

        //Show row 1         
        var oLabelShow = new TVcllabel(TagMainCont[0].Child[0].Child[0].This, "", TlabelType.H0);
        oLabelShow.VCLType = TVCLType.BS;
        oLabelShow.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblShow");
        var Lines1 = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1");
        var Lines2 = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2");
        var Shows = [
             { "Value": "1", "Text": Lines1 },
             { "Value": "2", "Text": Lines2 }
        ]
        var oComboShow = new TVclComboBox(TagMainCont[0].Child[0].Child[1].This, "ComboShow", Shows, 1);
        //oComboShow.VCLType = TVCLType.BS;
        //Show row 2

        var oComboSelectColumnSearch;
        var olblSelectColumnSearch = new TVcllabel(TagMainCont[0].Child[1].Child[0].This, "", TlabelType.H0);
        olblSelectColumnSearch.VCLType = TVCLType.BS;
        olblSelectColumnSearch.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblSelectColumnSearch");
        var Linea3 = { 'Linea3': UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3") }
        Linea3 = JSON.stringify(Linea3);

        var ResErr = new SysCfg.Error.Properties.TResErr();
        var Param = new SysCfg.Stream.Properties.TParam();
        Param.Inicialize();

        var SQLBuild = SysCfg.DB.SQL.Methods.GetSQLPrefixIDBuild("Atis", "SDGROUPUSER_GET", new Array());

        var OpenDataSet = SysCfg.DB.SQL.Methods.OpenDataSet("SELECT * FROM (" + SQLBuild.StrSQL + " ) as TblSearch ", Param.ToBytes());
        ResErr = OpenDataSet.ResErr;
        if (ResErr.NotError) {
            if (OpenDataSet.DataSet.FieldDefs.length > 0) {
                var arraySelects = [];
                for (var i = 0; i < OpenDataSet.DataSet.FieldDefs.length; i++) {
                    if (OpenDataSet.DataSet.FieldDefs[i].DataType.name == 'String') {
                        arraySelects.push({ "Value": OpenDataSet.DataSet.FieldDefs[i].DataType.value, "Text": OpenDataSet.DataSet.FieldDefs[i].FieldName });
                    }
                }
                oComboSelectColumnSearch = new TVclComboBox(TagMainCont[0].Child[1].Child[1].This, "ComboSelectColumnSearch", arraySelects, 0);
            }
            else {
                OpenDataSet.ResErr.NotError = false;
                OpenDataSet.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }       
        //Show row 3
        var olblSearch = new TVcllabel(TagMainCont[0].Child[2].Child[0].This, "", TlabelType.H0);
        olblSearch.VCLType = TVCLType.BS;
        olblSearch.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblSearch");
        var oVclTextBox = new TVclTextBox(TagMainCont[0].Child[2].Child[1].This, "txtSearch");
        oVclTextBox.VCLType = TVCLType.BS;
        //Show row 4
        //ACA SE FORMARA ERL GRID
        //Show roe 5
        var olblUseResult = new TVcllabel(TagMainCont[0].Child[4].Child[0].This, "", TlabelType.H0);
        olblUseResult.VCLType = TVCLType.BS;
        olblUseResult.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblUseResult");
        var btnClickHere = new TVclInputbutton(TagMainCont[0].Child[4].Child[1].This, "");
        btnClickHere.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnClickHere");
        btnClickHere.VCLType = TVCLType.BS;
        btnClickHere.onClick = function () {
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "HelloMessage"));
        };

        oComboShow.This.addEventListener('change', function () {
            var valor = this.options[this.selectedIndex].innerHTML;
            if (valor == Lines1) {
                Hidden();
                Fill_All();
            }
            else {
                Visible();
                Clear_All();
            }
        });

        //oComboSelectColumnSearch.Select.addEventListener('change', function () {
        //    //combosearch = $('#selectComboSelectColumnSearch option:selected').html();
        //});

        oVclTextBox.This.addEventListener("keyup", function () {
            var texto = document.getElementById("txtSearch").value;
            if (texto.length >= 3) {
                Fill_Criterio(oComboSelectColumnSearch.This.options[oComboSelectColumnSearch.This.selectedIndex].innerHTML, texto);
            }
            else {
                Clear_All();
            }
        })

        function Hidden() {
            olblSelectColumnSearch.Visible = false;
            oComboSelectColumnSearch.Visible = false;
            olblSearch.Visible = false;
            oVclTextBox.Visible = false;
        }
        function Visible() {
            olblSelectColumnSearch.Visible = true;
            oComboSelectColumnSearch.Visible = true;
            olblSearch.Visible = true;
            oVclTextBox.Visible = true;
        }
        function Fill_All() {

            try {
                var ResErr = new SysCfg.Error.Properties.TResErr();
                var Param = new SysCfg.Stream.Properties.TParam();
                Param.Inicialize();
                var SQLBuild = SysCfg.DB.SQL.Methods.GetSQLPrefixIDBuild("Atis", "SDGROUPUSER_GET", new Array());
                var OpenDataSet = SysCfg.DB.SQL.Methods.OpenDataSet("SELECT * FROM (" + SQLBuild.StrSQL + " ) as TblSearch ", Param.ToBytes());
                ResErr = OpenDataSet.ResErr;
                if (ResErr.NotError) {
                    if (OpenDataSet.DataSet.RecordCount > 0) {
                        $(TagMainCont[0].Child[3].Child[0].This).html("");
                        var ContResponsive = Udiv(TagMainCont[0].Child[3].Child[0].This, "IDTableResponsive");
                        BootStrapCreateTableResponsive(ContResponsive);
                        var DivHeader = new TVclStackPanel(ContResponsive, "DemoTopBarIcons_Header", 1, [[12], [12], [12], [12], [12]]);
                        var DivHeaderC = DivHeader.Column[0].This;
                        $(DivHeaderC).css("border", "1px solid black");
                        
                        this.VclGridCF = new Componet.GridCF.TGridCFView(ContResponsive, "", TlabelType.H0);
                        this.VclGridCF.VclGridCF.DataSource = OpenDataSet.DataSet;

                    }
                    else {
                        OpenDataSet.ResErr.NotError = false;
                        OpenDataSet.ResErr.Mesaje = "RECORDCOUNT = 0";
                    }
                }
            }
            finally {
                Param.Destroy();
            }



            //direc = SysCfg.App.Properties.xRaiz + 'Componet/AdvSearch/wcffrAdvSearch.svc/OpenSQL';
            //$.ajax({
            //    type: "POST",
            //    url: direc,
            //    async: false,
            //    data: "",
            //    contentType: "application/json; charset=utf-8",
            //    dataType: "json",
            //    success: function (response) {
            //        if (response.ResErr.NotError == true) {
            //            $(TagMainCont[0].Child[3].Child[0].This).html("");

            //            var ContResponsive = Udiv(TagMainCont[0].Child[3].Child[0].This, "IDTableResponsive");
            //            BootStrapCreateTableResponsive(ContResponsive);

            //            //VclGridTableHeader
            //            var GridTable = VclGridTable(ContResponsive, "IdTablaQuery", response.DataTableJs, function myfunction() {
            //                alert("El ID de esta fila es: " + this.getElementsByTagName("td")[0].innerHTML);
            //            });
            //            BootStrapCreateTable(GridTable);
            //        }
            //        else {
            //            $(TagMainCont[0].Child[3].Child[0].This).html("");
            //        }
            //    },
            //    error: function (error) {
            //        alert('error: ' + error.txtDescripcion + " \n Location: Create a new Case / LoadContactType");
            //    }
            //});
        }

        function Clear_All() {
            $(TagMainCont[0].Child[3].Child[0].This).html("");
        }

        function Fill_Criterio(combo, text) {
            $(TagMainCont[0].Child[3].Child[0].This).html("");
            var ResErr = new SysCfg.Error.Properties.TResErr();
            var Param = new SysCfg.Stream.Properties.TParam();
            Param.Inicialize();
            var SQLBuild = SysCfg.DB.SQL.Methods.GetSQLPrefixIDBuild("Atis", "SDGROUPUSER_GET", new Array());
            
            if (combo != "(ALL)")
            {
                if (combo != "")
                {
                    if (text.length >= 3)
                    {
                        try
                        {
                            var Parametro1 = "";
                            var Parametro2 = "";
                            var Parametro3 = "";
                            Parametro3 = " WHERE " + combo + " LIKE '%" + text + "%'";
                            Sql = "SELECT " + Parametro1 + " * FROM (" + SQLBuild.StrSQL + " ) as TblSearch " + Parametro2 + Parametro3;
                            
                            var OpenDataSet = SysCfg.DB.SQL.Methods.OpenDataSet(Sql, Param.ToBytes());
                           
                            ResErr = OpenDataSet.ResErr;
                            if (ResErr.NotError) {
                                if (OpenDataSet.DataSet.RecordCount > 0) {
                                    $(TagMainCont[0].Child[3].Child[0].This).html("");
                                    var ContResponsive = Udiv(TagMainCont[0].Child[3].Child[0].This, "IDTableResponsive");
                                    BootStrapCreateTableResponsive(ContResponsive);

                                    var DivHeader = new TVclStackPanel(ContResponsive, "DemoTopBarIcons_Header", 1, [[12], [12], [12], [12], [12]]);
                                    var DivHeaderC = DivHeader.Column[0].This;
                                    $(DivHeaderC).css("border", "1px solid black");
                                    $(DivHeaderC).css("min-height", "20px");                                    

                                    this.VclGridCF = new Componet.GridCF.TGridCFView(ContResponsive, "", TlabelType.H0);
                                    this.VclGridCF.VclGridCF.DataSource = OpenDataSet.DataSet;

                                }
                                else {
                                    OpenDataSet.ResErr.NotError = false;
                                    OpenDataSet.ResErr.Mesaje = "RECORDCOUNT = 0";
                                }
                            }
                        }
                        finally
                        {

                        }
                    }
                }
            }
            else
            {
                if (text.Length >= 3)
                {
                    try
                    {
                        var Parametro1 = "";
                        var Parametro2 = "";
                        var Parametro3 = "";
                        
                        var SQLBuild = SysCfg.DB.SQL.Methods.GetSQLPrefixIDBuild("Atis", "SDGROUPUSER_GET", new Array());

                        var OpenDataSet = SysCfg.DB.SQL.Methods.OpenDataSet("SELECT * FROM (" + SQLBuild.StrSQL + " ) as TblSearch ", Param.ToBytes());
                        ResErr = OpenDataSet.ResErr;
                        if (ResErr.NotError) {
                            if (OpenDataSet.DataSet.FieldDefs.length > 0) {
                                var arraySelects = [];
                                for (var i = 0; i < OpenDataSet.DataSet.FieldDefs.length; i++) {
                                    if (OpenDataSet.DataSet.FieldDefs[i].DataType.name == 'String') {
                                        arraySelects.push({ "Value": OpenDataSet.DataSet.FieldDefs[i].DataType.value, "Text": OpenDataSet.DataSet.FieldDefs[i].FieldName });
                                    }
                                }   
                            }
                        }       

                        for (var i = 0; i < arraySelects.length; i++) {
                            var key = arraySelects[i].Text;
                            if (i == 1)
                                Parametro3 = " WHERE " + key + " LIKE '%" + text + "%'";
                            else
                                Parametro3 += " OR " + key + " LIKE '%" + text + "%'";
                        }

                        var OpenDataSet = SysCfg.DB.SQL.Methods.OpenDataSet(Sql, Param.ToBytes());

                        ResErr = OpenDataSet.ResErr;
                        if (ResErr.NotError) {
                            if (OpenDataSet.DataSet.RecordCount > 0) {
                                $(TagMainCont[0].Child[3].Child[0].This).html("");
                                var ContResponsive = Udiv(TagMainCont[0].Child[3].Child[0].This, "IDTableResponsive");
                                BootStrapCreateTableResponsive(ContResponsive);
                                var DivHeader = new TVclStackPanel(ContResponsive, "DemoTopBarIcons_Header", 1, [[12], [12], [12], [12], [12]]);
                                var DivHeaderC = DivHeader.Column[0].This;
                                $(DivHeaderC).css("border", "1px solid black");
                                $(DivHeaderC).css("min-height", "20px");
                                

                                this.BarControls = new TBarControls(DivHeaderC, "IDTopBar");   //Indicamos el Contenedor y el ID
                                this.BarControls.Visible = false;
                                this.GridCFToolbar = new Componet.GridCF.TGridCFToolbar(ContResponsive, this.BarControls, function (OutRes) { });
                                this.VclGridCF = new Componet.GridCF.TGridCFView(ContResponsive, function (OutRes) { }, OpenDataSet.DataSet, this.GridCFToolbar);
                            }
                            else {
                                OpenDataSet.ResErr.NotError = false;
                                OpenDataSet.ResErr.Mesaje = "RECORDCOUNT = 0";
                            }
                        }
                    }
                    finally
                    {
                    }
                }
            }
        }
    };
   
//};