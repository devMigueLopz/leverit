﻿ 
ItHelpCenter.SD.CaseManager.NewCase.frAdvSearchDataSet = function (inObject, incallback) {

    this.TParent = function () {
        return this;
    }.bind(this);
  
    var _this = this.TParent();
    this.ObjectHtml = inObject;
    this.CallbackModalResult = incallback;
    this.DBTranslate = null;
    this.Mythis = "frAdvSearchDataSet";
    this.ID = this.Mythis + (Math.random());
 
    this.ShowAll = false;
    this.divGrid = null; 
    this.RowSelected = null;
    

    UsrCfg.Traslate.GetLangText(_this.Mythis, "label1", "All");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "label2", "Column");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "label3", "Filter");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "label4", "Use this result");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "IDCMDBCI", "ID CMDB CI");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "CI_GENERICNAME", "CI Generic Name");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "FIRSTNAME", "First name");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "MIDDLENAME", "Middle name");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "LASTNAME", "Last name");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "EMPLOYEENUMBER", "Employee number");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "ROLENAME", "Role name");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "USERTITLE", "User Title");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "IDCMDBUSER", "ID CMDB User");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "IDATROLE", "ID Role");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "msgNotResult", "Select a user");
    _this.LoadComponenet()
}
ItHelpCenter.SD.CaseManager.NewCase.frAdvSearchDataSet.prototype.LoadComponenet = function () {
   
    var _this = this.TParent();
    _this.InitializeComponent();
  
}

ItHelpCenter.SD.CaseManager.NewCase.frAdvSearchDataSet.prototype.InitializeComponent = function () {
    var _this = this.TParent();
    _this.ObjectHtml.innerHTML = ""; 

    //ESTRUCTURA DIVS
    //var DivsMain = new TVclStackPanel(_this.ObjectHtml, "DivsMain" + _this.ID, 2, [[12, 12], [12, 12], [12, 12], [12, 12], [12, 12]]);
    //var Div2C = DivsMain.Column[0].This;
    //var Div1C = DivsMain.Column[1].This;

    
    var Div2 = new TVclStackPanel(_this.ObjectHtml, "Div2" + _this.ID, 2, [[0,12], [1,10], [1,10], [2,8], [2,8]]);
    var Div2C = Div2.Column[1].This;

    var Div1 = new TVclStackPanel(_this.ObjectHtml, "Div1" + _this.ID, 2, [[0, 12], [1, 10], [1, 10], [2, 8], [2, 8]]);
    var Div1C = Div1.Column[1].This;  

    var DivBtn = new TVclStackPanel(_this.ObjectHtml, "DivBtn" + _this.ID, 2, [[0, 12], [1, 10], [1, 10], [2, 8], [2, 8]]);
    var DivBtnC = DivBtn.Column[1].This;
      
    $(Div1C).css("padding", "0px 10px");
    $(Div2C).css("padding", "10px 20px");     
    $(DivBtnC).css("padding", "10px");

    if (SysCfg.App.Properties.Device.value == SysCfg.App.TDevice.Desktop.value) {        
        $(Div1C).addClass("StackPanelContainer");
        $(Div2C).addClass("StackPanelContainer");
        $(DivBtnC).addClass("StackPanelContainer");
    } else {
        //$(_this.ObjectHtml).css("padding", "0px");       
    }   

    $(Div1C).css("min-height", "30px");
    $(Div1C).css("max-height", "330px");
    $(Div1C).css("overflow-y", "scroll");
    _this.divGrid = Div1C; 
    
    var Divs2_1 = new TVclStackPanel(Div2C, "Div2_1C1" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
    var Div2_1C1 = Divs2_1.Column[0].This;
   

    var Divs2_2 = new TVclStackPanel(Div2C, "Div2_1C2" + _this.ID, 2, [[12, 12], [12, 12], [12, 12], [12, 12], [12, 12]]);
    var Div2_1C2 = Divs2_2.Column[0].This;
    var Div2_1C3 = Divs2_2.Column[1].This;    

    var Divs2_3 = new TVclStackPanel(Div2C, "Div2_1C4" + _this.ID, 2, [[12, 12], [12, 12], [12, 12], [12, 12], [12, 12]]);
    var Div2_1C4 = Divs2_3.Column[0].This;
    var Div2_1C5 = Divs2_3.Column[1].This;

    if (SysCfg.App.Properties.Device.value == SysCfg.App.TDevice.Desktop.value) {
        $(Divs2_1.Row.This).css("padding-bottom", "10px");
        $(Divs2_2.Row.This).css("padding-bottom", "10px");
        $(Divs2_3.Row.This).css("padding-bottom", "10px");
    } else {
        $(Divs2_1.Row.This).css("padding-bottom", "0px");
        $(Divs2_2.Row.This).css("padding-bottom", "0px");
        $(Divs2_3.Row.This).css("padding-bottom", "0px");
    }

    //ELEMENTOS
    //**Select
    var Check1 = new TVclInputcheckbox(Div2_1C1, "Check1" + _this.ID);
    Check1.VCLType = TVCLType.BS;
    Check1.onClick = function myfunction() {
        _this.LoadUsers();
    }

    var label1 = new Vcllabel(Div2_1C1, "label1" + _this.ID, TVCLType.BS, TlabelType.H0, "");
    label1.innerText = UsrCfg.Traslate.GetLangText(_this.Mythis, "label1");
    $(label1).css("margin-left", "5px");

    var label2 = new Vcllabel(Div2_1C2, "label2" + _this.ID, TVCLType.BS, TlabelType.H0, "");
    label2.innerText = UsrCfg.Traslate.GetLangText(_this.Mythis, "label2");
    $(Div2_1C2).addClass("frAdvSearchDivlabel");
    _this.Divlabel2 = Div2_1C2;

    _this.DivCombo = Div2_1C3; 
    _this.FillCOMBO();

    var label3 = new Vcllabel(Div2_1C4, "label3" + _this.ID, TVCLType.BS, TlabelType.H0, "");
    label3.innerText = UsrCfg.Traslate.GetLangText(_this.Mythis, "label3");
    $(Div2_1C4).addClass("frAdvSearchDivlabel");
    _this.Divlabel3 = Div2_1C4;

   _this.TexBoxt1 = new TVclTextBox(Div2_1C5, "TexBoxt1" + _this.ID);
   $(_this.TexBoxt1.This).css("width", "100%");
    _this.DivText = Div2_1C5;
    $(_this.TexBoxt1.This).addClass("TextFormat");

    _this.TexBoxt1.This.onkeydown = function validar(e) {
        if ((_this.TexBoxt1.Text).length > 1) {
            _this.FiltrarUsers(_this.TexBoxt1.Text, _this.Combo_Columns);
        } else {
            _this.divGrid.innerHTML = "";
        }
    }
    
    $(DivBtnC).css("text-align", "center");
    var btn_UseThis = new TVclButton(DivBtnC, "btn_UseThis" + _this.ID);
    btn_UseThis.VCLType = TVCLType.BS;
    btn_UseThis.Src = "image/32/search-good.png";
    btn_UseThis.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "label4");
    if (SysCfg.App.Properties.Device.value == SysCfg.App.TDevice.Desktop.value) {
        btn_UseThis.This.style.minWidth = "40%";
    } else {
        btn_UseThis.This.style.minWidth = "70%";
    }   
    btn_UseThis.This.classList.add("btn-xs");
    btn_UseThis.onClick = function myfunction() {
        if (_this.RowSelected == null) {
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "msgNotResult"));
        } else {          
            _this.CallbackModalResult(_this.RowSelected);
        }
    }

}

ItHelpCenter.SD.CaseManager.NewCase.frAdvSearchDataSet.prototype.FillCOMBO = function () {
    var _this = this.TParent();   

    var ResErr = new SysCfg.Error.Properties.TResErr();
    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();

    var SQLBuild = SysCfg.DB.SQL.Methods.GetSQLPrefixIDBuild("Atis", "SDGROUPUSER_GET", new Array());

    var OpenDataSet = SysCfg.DB.SQL.Methods.OpenDataSet("SELECT * FROM (" + SQLBuild.StrSQL + " ) as TblSearch ", Param.ToBytes());
    ResErr = OpenDataSet.ResErr;
    if (ResErr.NotError) {
        if (OpenDataSet.DataSet.FieldDefs.length > 0) {
            var arraySelects = [];
            arraySelects.push({ "Value": "0", "Text": '('+UsrCfg.Traslate.GetLangText(_this.Mythis, "label1")+')' });
            for (var i = 0; i < OpenDataSet.DataSet.FieldDefs.length; i++) {
                if (OpenDataSet.DataSet.FieldDefs[i].DataType.name == 'String') {                  
                    var Text = UsrCfg.Traslate.GetLangText(_this.Mythis, OpenDataSet.DataSet.FieldDefs[i].FieldName);
                    arraySelects.push({ "Value": OpenDataSet.DataSet.FieldDefs[i].FieldName, "Text": Text });
                }
            }
            _this.Combo_Columns = new TVclComboBox(_this.DivCombo, "ComboSelectColumnSearch", arraySelects, 0);
            _this.Combo_Columns.onChange = function () {
                if ((_this.TexBoxt1.Text).length > 0) {
                    _this.FiltrarUsers(_this.TexBoxt1.Text, _this.Combo_Columns);
                } else {
                    _this.divGrid.innerHTML = "";
                }
            }
            $(_this.Combo_Columns.This).css('width', "100%");
            $(_this.Combo_Columns.This).addClass("TextFormat");
        }
        else {
            OpenDataSet.ResErr.NotError = false;
            OpenDataSet.ResErr.Mesaje = "RECORDCOUNT = 0";
        }
    }
}


 
ItHelpCenter.SD.CaseManager.NewCase.frAdvSearchDataSet.prototype.LoadUsers = function () {
    var _this = this.TParent();
    try {
        
        $(_this.DivCombo).css("display", "none");
        $(_this.DivText).css("display", "none");
        $(_this.Divlabel2).css("display", "none");
        $(_this.Divlabel3).css("display", "none");

        if (_this.ShowAll == false) {
            _this.ShowAll = true; 

            var ResErr = new SysCfg.Error.Properties.TResErr();
            try {
                var Param = new SysCfg.Stream.Properties.TParam();
                Param.Inicialize();
                var SQLBuild = SysCfg.DB.SQL.Methods.GetSQLPrefixIDBuild("Atis", "SDGROUPUSER_GET", new Array());
                var OpenDataSet = SysCfg.DB.SQL.Methods.OpenDataSet("SELECT * FROM (" + SQLBuild.StrSQL + " ) as TblSearch ", Param.ToBytes());
                ResErr = OpenDataSet.ResErr;
                if (ResErr.NotError) {
                    _this.ViewGrid(OpenDataSet);
                } else {
                    OpenDataSet.ResErr.NotError = false;
                    OpenDataSet.ResErr.Mesaje = "RECORDCOUNT = 0";
                    _this.divGrid.innerHTML = "";
                }
            } finally {
                Param.Destroy();
            }
          

        } else {
            _this.ShowAll = false;
            _this.divGrid.innerHTML = "";
            $(_this.DivCombo).css("display", "block");
            $(_this.DivText).css("display", "block");
            $(_this.Divlabel2).css("display", "block");
            $(_this.Divlabel3).css("display", "block");

        } 
       
    }
    catch (e) {
        
    }
         
}



ItHelpCenter.SD.CaseManager.NewCase.frAdvSearchDataSet.prototype.FiltrarUsers = function (textFilter, Combo_Columns) {
    var _this = this.TParent();
    var Parametro = "";

    if ((Combo_Columns.Text != "") && (textFilter.length >= 3)) {
         var SQLBuild = SysCfg.DB.SQL.Methods.GetSQLPrefixIDBuild("Atis", "SDGROUPUSER_GET", new Array());

        if (Combo_Columns.Text != '(' + UsrCfg.Traslate.GetLangText(_this.Mythis, "label1") + ')') {

            var Parametro = "";
            Parametro = " WHERE " + Combo_Columns.Value + " LIKE '%" + textFilter + "%'";

        } else {
            var Param = new SysCfg.Stream.Properties.TParam();
            Param.Inicialize();
            var OpenDataSet = SysCfg.DB.SQL.Methods.OpenDataSet("SELECT TOP 1 * FROM (" + SQLBuild.StrSQL + " ) as TblSearch ", Param.ToBytes());
            var ResErr = OpenDataSet.ResErr;
            if (ResErr.NotError) {
                if (OpenDataSet.DataSet.FieldDefs.length > 0) {

                    var arraySelects = [];
                    for (var i = 0; i < OpenDataSet.DataSet.FieldDefs.length; i++) {
                        if (OpenDataSet.DataSet.FieldDefs[i].DataType.name == 'String') {
                            arraySelects.push({ "Value": OpenDataSet.DataSet.FieldDefs[i].DataType.value, "Text": OpenDataSet.DataSet.FieldDefs[i].FieldName });
                        }
                    }

                   
                    for (var i = 0; i < arraySelects.length; i++) {
                        var key = arraySelects[i].Text;
                        if (i == 0)
                            Parametro = " WHERE " + key + " LIKE '%" + textFilter + "%'";
                        else
                            Parametro += " OR " + key + " LIKE '%" + textFilter + "%'";
                    }
                }
            }
            Param.Destroy();
        }

        if (Parametro != "") {
            Sql = "SELECT * FROM (" + SQLBuild.StrSQL + " ) as TblSearch " + Parametro;

            try {
                var Param = new SysCfg.Stream.Properties.TParam();
                Param.Inicialize();

                var OpenDataSet = SysCfg.DB.SQL.Methods.OpenDataSet(Sql, Param.ToBytes());
                var ResErr = OpenDataSet.ResErr;
                if (ResErr.NotError) {
                    _this.ViewGrid(OpenDataSet);
                } else {
                    OpenDataSet.ResErr.NotError = false;
                    OpenDataSet.ResErr.Mesaje = "RECORDCOUNT = 0";
                    _this.divGrid.innerHTML = "";
                }
            } finally  {

                Param.Destroy();
            }
          
        }       
    }   
     
}


ItHelpCenter.SD.CaseManager.NewCase.frAdvSearchDataSet.prototype.ViewGrid = function (OpenDataSet) {
    var _this = this.TParent();
   
  
    _this.divGrid.innerHTML = "";
    var Grilla = new TGrid(_this.divGrid, "", ""); //CREA UN GRID CONTRO
    Grilla.AutoTranslate = true;
    Grilla.DataSource = OpenDataSet.DataSet;
    

    //for (var i = 0; i < OpenDataSet.DataSet.FieldCount; i++) {
    //    var Col0 = new TColumn(); //CREA UNA COLUMNA PARA AGREGAR AL GRID CONTROL
    //    Col0.Name = OpenDataSet.DataSet.FieldDefs[i].FieldName;
    //    Col0.Caption = UsrCfg.Traslate.GetLangText(_this.Mythis, OpenDataSet.DataSet.FieldDefs[i].FieldName);
    //    Col0.Index = i;
    //    Col0.EnabledEditor = false; //ASIGNA O EXTRAE UN VALO TRUE O FALSE QUE INDICA SI LA COLUMNA MOSTRARA UN EDITOR DE DATOS             
    //    Grilla.AddColumn(Col0); //AGREGA UNA COLUMNA AL GRIDCONTROL 
    //}

    //for (var i = 0; i < OpenDataSet.DataSet.RecordCount; i++) {//rows
    //    var NewRow = new TRow(); //CREA UN ROW PARA AGREGAR AL GRIDCONTROL  
    //    for (var j = 0; j < OpenDataSet.DataSet.FieldCount; j++) {//columns
    //        var Cell = new TCell();
    //        Cell.Value = OpenDataSet.DataSet.Records[i].Fields[j].Value;
    //        Cell.IndexColumn = j;
    //        Cell.IndexRow = i;
    //        NewRow.AddCell(Cell);

    //    }
          
    //    Grilla.AddRow(NewRow);
    //}

        
    Grilla.OnRowClick = function (sender, EventArgs) {          
        _this.RowSelected = EventArgs;
    }

 
}
