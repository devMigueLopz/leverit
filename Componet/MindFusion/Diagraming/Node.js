﻿//Autor:Carlos 

ItHelpCenter.ComponentMindFusionManagerDiagramingProvider.TNode = function (_id, _text) {
    this.Id = _id; //string{ get; set; }
    this.Text = _text; // string{ get; set; }
    this.Titulo = ""; // string{ get; set; }
    this.SubTitulo = ""; // string{ get; set; }
    this.Visible = true; // bool{ get; set; }
    this.Targets = new Array(); // List<TNode>{ get; set; }
    this.Background = null; // Color{ get; set; }
    this.BorderColor = null; // SolidColorBrush{ get; set; }
    this.Expanded = true; // bool{ get; set; }
    this.Imagen = null; // ImageSource{ get; set; }

    this.DisplayParents = true; // { get; set; }
    this.DisplayFriends = true; // { get; set; }
    this.DisplayChilds = true; // { get; set; }

    this.DiagramNode = null; //MindFusion.Diagramming.DiagramNode{ get; set; }

    //this.TNode2 = function(_id, _text)
    // {
    //     Id = _id;
    //     Text = _text;
    // }

    // this.TNode3 = function(_id, _text, _color)
    // {
    //     Id = _id;
    //     Text = _text;
    //     Background = _color;
    // }

    // this.TNode4 = function(_id, _text, _color, _expanded)
    // {
    //     Id = _id;
    //     Text = _text;
    //     Background = _color;
    //     Expanded = _expanded;
    //     Titulo = _id + " - " + _text;
    // }

    // this.TNode7 = function(_id, _text, _color, _expanded, _displayParents, _displayFriends, _displayChilds)
    // {
    //     Id = _id;
    //     Text = _text;
    //     Background = _color;
    //     Expanded = _expanded;
    //     DisplayParents = _displayParents;
    //     DisplayFriends = _displayFriends;
    //     DisplayChilds = _displayChilds;
    // }

    // this.TNode = function(_id, _text, _color, _expanded, _titulo, _subtitulo, _visible)
    // {
    //     Id = _id;
    //     Text = _text;
    //     Background = _color;
    //     Expanded = _expanded;
    //     Titulo = _titulo;
    //     SubTitulo = _subtitulo;
    //     Visible = _visible;
    // }       
}

