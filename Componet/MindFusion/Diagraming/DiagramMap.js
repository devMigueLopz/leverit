﻿
ItHelpCenter.ComponentMindFusionManagerDiagramingProvider.TDiagramMap = function ()
{
    this.Nodes = new Array(); //List<TNode>
    this.Links = new Array(); //List<TLink>
    this.ClearNodes = true;
    this.Diagrama = null; //Diagram
    this.contenedor = null; //object { get; set; }

    this.PreparedDiagram = function()
    {
        if (this.ClearNodes)
        {
            this.Links = new Array();
            this.Nodes = new Array();
        }
    }

    this.ConstruirMapaSteps = function(_Diag)
    {
        _Diag.ShapeHandlesStyle = MindFusion.Diagramming.HandlesStyle.Invisible;
        var nodeMap = []; // new Dictionary<string, DiagramNode>();
        //var bounds = new Rect(((ScrollViewer)this.contenedor).HorizontalOffset + 50, ((ScrollViewer)this.contenedor).VerticalOffset + 50, 120, 40);
        var bounds = new MindFusion.Drawing.Rect(0, 0, 120, 40);

       

        layout.LinkType = MindFusion.Graphs.LayeredLayoutLinkType.Cascading;

        for (var i = 0; i < this.Nodes.length; i++) {
            var item = this.Nodes[i];
            var diagramNode = MindFusion.Diagramming.ShapeNode(_Diag); //  _Diag.Factory.CreateShapeNode(bounds);
            diagramNode.setBounds(rect);
            nodeMap[item.Id.toString()] = diagramNode;
            diagramNode.Id = item.Id;
            diagramNode.Text = item.Text;
            //diagramNode.Brush = new SolidColorBrush(item.Background);

            _Diag.addItem(diagramNode);
            
        }

        for (var i = 0; i < this.Links.length; i++) {
            var item = this.Links[i];
            var link = new MindFusion.Diagramming.DiagramLink(_Diag, nodeMap[item.Origin.Id.toString()], nodeMap[item.Target.Id.toString()]);
                   
            _Diag.addItem(link);
        }
        var LayeredLayout = MindFusion.Graphs.LayeredLayout;

        var layout = new MindFusion.Graphs.LayeredLayout();

        layout.direction = MindFusion.Graphs.LayoutDirection.TopToBottom;
        layout.siftingRounds = 0;
        layout.nodeDistance = 8;
        layout.layerDistance = 8;
       

        layout.Anchoring = MindFusion.Graphs.Anchoring.Reassign;
        layout.Arrange(_Diag);

        _Diag.resizeToFitItems(5);
        //_Diag.zoomToRect(_Diag.Bounds);
            
    }

    this.ConstruirMapa = function(_Diag)
    {
        //        _Diag.Links = new Array();
        //        _Diag.Nodes = new Array();

        //        var nodeMap = new Array();// new Dictionary<string, DiagramNode>();
        //        var bounds = new Rect(0, 0, 120, 40);

        //        foreach (TNode item in Nodes)
        //    {
        //                    var nodecheck = new NodeCI();
        //        var diagramNode = new ControlNode(_Diag, nodecheck);
        //        _Diag.Nodes.push(diagramNode);
        //        diagramNode.Bounds = new Rect(0, 0, 200, 100);
        //        diagramNode.Expanded = item.Expanded;
        //        diagramNode.Brush = new SolidColorBrush(item.Background);
        //        nodeMap[item.Id.toString()] = diagramNode;
        //        diagramNode.Id = item.Id;
        //        //diagramNode.Text = item.Text;
        //        diagramNode.Tag = item;
        //    }

        //    foreach (TLink item in Links)
        //{
        //                _Diag.Factory.CreateDiagramLink(nodeMap[item.Origin.Id.toString()], nodeMap[item.Target.Id.toString()]).Tag = item;
        //}
        //var layout = new LayeredLayout();
        //layout.LinkType = LayeredLayoutLinkType.Cascading;
        //layout.Margins = new Size(5, 5);
        //layout.NodeDistance = 140;
        //layout.Arrange(_Diag);
    }

    this.ConstruirMapaFractal = function(_Diag)
    {
        //        _Diag.ClearAll();

        //        var nodeMap = [];

        //        foreach (TNode item in Nodes)
        //    {
        //        //creamos el control que ira dentro del nodo
        //                    var nodecheck = new NodeCI();
        //        nodecheck.Titulo = item.Titulo;
        //        nodecheck.SubTitulo = item.SubTitulo;
        //        nodecheck.Visible = item.Visible;
        //        nodecheck.Id = item.Id;

        //        //creamos el nodo que contiene al control 
        //        var diagramNode = new ControlNode(_Diag, nodecheck);

        //        //setear en el control al nodo papa
        //        nodecheck.Owner = diagramNode;

        //        //agregamos el nodo al diagrama
        //        _Diag.Nodes.push(diagramNode);
        //        diagramNode.Bounds = new Rect(0, 0, 200, 100);
        //        diagramNode.Expanded = item.Expanded;
        //        diagramNode.Visible = item.Visible;

        //        nodecheck.brdTest.BorderBrush = item.BorderColor;

        //        nodeMap[item.Id.toString()] = diagramNode;
        //        diagramNode.Id = item.Id;
        //        diagramNode.Tag = item;
        //    }

        //    foreach (TLink item in Links)
        //{
        //                DiagramLink _link = _Diag.Factory.CreateDiagramLink(nodeMap[item.Origin.Id.toString()], nodeMap[item.Target.Id.toString()]);
        //    _link.Tag = item;
        //    _link.Visible = item.Vibisible;
        //}

        //if (_Diag.Nodes[0] != null)
        //    Arrange(_Diag, _Diag.Nodes[0]);
        //else
        //    Arrange(_Diag);
    }

    //this.BuildDiagramRelations = function(_Diag)
    //{
    //    _Diag.ClearAll();

    //    var nodeMap = [];
    //    var bounds = new MindFusion.Drawing.Rect(0, 0, 150, 50);
    //    for (var i = 0; i < this.Nodes.length; i++) {
    //        var item = this.Nodes[i];
    //        //creamos el nodo que contiene al control 
    //        var diagramNode = new MindFusion.Diagramming.ShapeNode(_Diag);
    //        diagramNode.setBounds(bounds);
    //        diagramNode.setText(item.Titulo);
    //        //diagramNode.ToolTip = new ToolTip() { Content = item.Text };
    //        diagramNode.Visible = item.Visible;
    //        nodeMap[item.Id.toString()] = diagramNode;
    //        diagramNode.Id = item.Id;
    //        diagramNode.Tag = item;
            
    //        //diagramNode.ima
    //        diagramNode.Image = item.Imagen;
    //        diagramNode.ImageAlign = ImageAlign.TopCenter;
    //        diagramNode.ImageStretch = Stretch.None;
    //        diagramNode.VerticalTextBlockAlignment = VerticalAlignment.Bottom;


    //        //agregamos el nodo al diagrama
    //        _Diag.addItem(diagramNode);
    //    }

    //    for (var i = 0; i < this.Links.length; i++) {
    //        var item = this.Links[i];
    //        var link = new MindFusion.Diagramming.DiagramLink(_Diag, nodeMap[item.Origin.Id.toString()], nodeMap[item.Target.Id.toString()]);
                   
    //        _Diag.addItem(link);
    //    }

    //    if (_Diag.Nodes[0] != null)
    //        Arrange(_Diag, _Diag.Nodes[0]);
    //    else
    //        Arrange(_Diag);

            
    //    _Diag.resizeToFitItems(5);
    //    _Diag.zoomToRect(_Diag.Bounds);
    //}

    this.BuildDiagramRelations = function(_Diag, ClearAll)
    {
        if(ClearAll)
            _Diag.clearAll();

        var nodeMap = [];
        var bounds = new MindFusion.Drawing.Rect(0, 0, 60, 15);

        for (var i = 0; i < this.Nodes.length; i++) {
            var item = this.Nodes[i];
            //creamos el nodo que contiene al control 
           
            var diagramNode = new MindFusion.Diagramming.ShapeNode(_Diag);
            diagramNode.setBounds(bounds);
            diagramNode.setText(item.Titulo);
            //diagramNode.ToolTip = new ToolTip() { Content = item.Text };
            diagramNode.setVisible(item.Visible);            
            diagramNode.setId(item.Id);
            diagramNode.Tag = item;
            diagramNode.setImage(item.Imagen);
            diagramNode.setBrush({ type: 'SolidBrush', color: 'rgba(0, 0, 255, 0)' });
            diagramNode.setStroke("#fff");
            diagramNode.setShadowColor("#fff");

            diagramNode.setTextAlignment(MindFusion.Diagramming.Alignment.Center);
            
            diagramNode.setImageAlign(MindFusion.Drawing.ImageAlign.TopCenter)
            var thiknes = new MindFusion.Drawing.Thickness(0,10,0,0);
            diagramNode.setTextPadding(thiknes);

            nodeMap[item.Id.toString()] = diagramNode;

            ////diagramNode.ima
            //diagramNode.Image = item.Imagen;
            //diagramNode.ImageAlign = ImageAlign.TopCenter;
            //diagramNode.ImageStretch = Stretch.None;
            //diagramNode.VerticalTextBlockAlignment = VerticalAlignment.Bottom;


            //agregamos el nodo al diagrama
            _Diag.addItem(diagramNode);
        }

        for (var i = 0; i < this.Links.length; i++) {
            var item = this.Links[i];
            var link = new MindFusion.Diagramming.DiagramLink(_Diag, nodeMap[item.Origin.Id.toString()], nodeMap[item.Target.Id.toString()]);
                   
            _Diag.addItem(link);
        }

        if (_Diag.nodes[0] != null)
            this.Arrange2(_Diag, _Diag.nodes[0]);
        else
            this.Arrange1(_Diag);

        //_Diag.setZoomFactor(8)
            
        _Diag.resizeToFitItems(5);
        //_Diag.zoomToRect(bounds);
    }

    this.SincronizarData = function(_Diag)
    {
    }

    this.AddNodeMaps = function(_Diag)
    {
    }

    this.Arrange1 = function(_Diag)
    {
        var layout = new MindFusion.Graphs.FractalLayout();
        //layout.Root = root;
        //layout.Margins = new Size(40, 40);
           
        layout.anchoring = MindFusion.Graphs.Anchoring.Reassign;
        _Diag.arrange(layout);

          

        //layout.LayoutLink.no

        //_Diag.ResizeToFitItems(5);
        //_Diag.ZoomToRect(_Diag.Bounds);
    }

    this.Arrange2 = function(_Diag, root)
    {
        var layout = new MindFusion.Graphs.FractalLayout();
        layout.root = root;
        layout.anchoring = MindFusion.Graphs.Anchoring.Reassign;
        _Diag.arrange(layout);

        //_Diag.ResizeToFitItems(5);
        //_Diag.ZoomToRect(_Diag.Bounds);
    }

    this.AddNode = function(_n)
    {
        if (!this.IsNodeDuplicated(_n))
            this.Nodes.push(_n);
    }

    this.AddOrUpdateNode = function(_n)
    {
        if (this.FindNode(_n.Id) == null)
            this.Nodes.push(_n);
        else
            this.UpdateNode(this.FindNode(_n.Id), _n);
    }

    this.UpdateNode = function(_Old, _New)
    {
        _Old.Text = _New.Text;
        _Old.Titulo = _New.Titulo;
        _Old.SubTitulo = _New.SubTitulo;
        _Old.Visible = _New.Visible;
        _Old.Targets = _New.Targets;
        _Old.Background = _New.Background;
        _Old.BorderColor = _New.BorderColor;
        _Old.Expanded = _New.Expanded;
        _Old.DisplayParents = _New.DisplayParents;
        _Old.DisplayFriends = _New.DisplayFriends;
        _Old.DisplayChilds = _New.DisplayChilds;
    }

    this.AddLink = function(_l)
    {
        if (!this.IsLinkDuplicated(_l))
            this.Links.push(_l);
    }

    this.AddOrUpdateLink = function(_l)
    {
        if (this.FindLink(_l.Origin.Id, _l.Target.Id) == null)
            this.Links.push(_l);
        else
            this.UpdateLink(this.FindLink(_l.Origin.Id, _l.Target.Id), _l);
    }

    this.UpdateLink = function(_Old, _New)
    {
        this.UpdateNode(_Old.Origin, _New.Origin);
        this.UpdateNode(_Old.Origin, _New.Origin);
        _Old.Vibisible = _New.Vibisible;
    }

    this.IsNodeDuplicated = function(_n)
    {
        for (var i = 0; i < this.Nodes.length; i++) {
            var item = this.Nodes[i];
            if (item.Id == _n.Id)
                return true;
        }
        return false;
    }

    this.IsLinkDuplicated = function(_l)
    {       
        for (var i = 0; i < this.Links.length; i++) {
            var item = this.Links[i];
            if (item.Origin.Id == _l.Origin.Id &&  item.Target.Id == _l.Target.Id)
                return true;
        }
        return false;

    }

    this.FindNode = function(Id)
    {
        //return Nodes.Find(z => z.Id == Id);
        for (var i = 0; i < this.Nodes.length; i++) {
            var item = this.Nodes[i];
            if (item.Id == Id)
                return item;
        }
        return null;
    }

    this.FindLink = function(IdOrigin, IdTarget)
    {
        //return this.Links.Find(z => z.Origin.Id == IdOrigin && z.Target.Id == IdTarget);
        for (var i = 0; i < this.Links.length; i++) {
            var item = this.Links[i];
            if (item.Origin.Id == IdOrigin &&  item.Target.Id == IdTarget)
                return item;
        }
        return null;
    }

    this.FindDiagramNode = function(Id)
    {
        for (var i = 0; i < this.Diagrama.nodes.length; i++)
        {
            var item = this.Diagrama.nodes[i];
            if (item.Id === Id)
                return item;
        }
        return null;
    }

    this.AddDiagramNode = function(_n)
    {
        ///Diagrama.Nodes.push()
    }

    this.UpdateNode = function(_n)
    {
    }
}