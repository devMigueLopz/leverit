﻿ItHelpCenter.SD.frAdvMsgEdit = function (inObject, incallback, inParent) {
    this.TParent = function () {
        return this;
    }.bind(this);

    var _this = this.TParent();
    this.ObjectHtml = inObject;

    this.ParentThis = inParent;
    this.CallbackModalResult = incallback;
    this.DBTranslate = null;
    this.Mythis = "frAdvMsgEdit";
    this.ID = this.Mythis + (Math.floor(Math.random() * 1000));

    //TRADUCCION   
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lblMostrar", "Show:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnAccept", "Accept");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnCancel", "Cancel");
    _this.InitializeComponent();
}

ItHelpCenter.SD.frAdvMsgEdit.prototype.InitializeComponent = function () {
    var _this = this.TParent();

    var Div1C = new TVclStackPanel(_this.ObjectHtml, "Div1" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
    var Div1 = Div1C.Column[0].This;

    var Div2C = new TVclStackPanel(_this.ObjectHtml, "Div2" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
    var Div2 = Div2C.Column[0].This;

    var Div3C = new TVclStackPanel(_this.ObjectHtml, "Div3" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
    var Div3 = Div3C.Column[0].This;

    //Element
    _this.LblComentario = new Vcllabel(Div1, "lblMostrar" + _this.ID, TVCLType.BS, TlabelType.H0, "");
    _this.LblComentario.innerText = "";

    _this.TxtDescripcion = new TVclMemo(Div2, "AreaTextDescription_" + _this.ID)
    $(_this.TxtDescripcion.This).css('width', "100%");
    $(_this.TxtDescripcion.This).css('height', "100px");
    $(_this.TxtDescripcion.This).addClass("TextFormat");


    _this.btnAceptar = new TVclButton(Div3, "btnAceptar" + _this.ID);
    _this.btnAceptar.VCLType = TVCLType.BS;
    _this.btnAceptar.Src = "image/24/accept24.png"
    _this.btnAceptar.Cursor = "pointer";
    _this.btnAceptar.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnAccept");
    _this.btnAceptar.onClick = function () {
        _this.btnAceptar_Click(_this, _this.btnAceptar);
    }
    $(_this.btnAceptar.This).css("float", "right");
    $(_this.btnAceptar.This).css("margin", "10px");
    $(_this.btnAceptar.This).css("cursor", "pointer");

    _this.btnCancelar = new TVclButton(Div3, "btnCancel" + _this.ID);
    _this.btnCancelar.VCLType = TVCLType.BS;
    _this.btnCancelar.Src = "image/24/close24.png"
    _this.btnCancelar.Cursor = "pointer";
    _this.btnCancelar.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnCancel");
    _this.btnCancelar.onClick = function () {
        _this.btnCancelar_Click(_this, _this.btnCancelar);
    }
    $(_this.btnCancelar.This).css("float", "right");
    $(_this.btnCancelar.This).css("margin", "10px");
    $(_this.btnCancelar.This).css("cursor", "pointer");

    _this.CallbackModalResult(_this);
}

ItHelpCenter.SD.frAdvMsgEdit.prototype.Inicialize = function (inTitle, inDescription, inCallback) {
    var _this = this.TParent();

    _this.CallbackModalResult = inCallback;
    _this.LblComentario.innerText = inTitle;
    _this.TxtDescripcion.Text = inDescription;
}

ItHelpCenter.SD.frAdvMsgEdit.prototype.btnAceptar_Click = function (sender, e) {
    _this = sender;
    _this.CallbackModalResult(true, _this.TxtDescripcion.Text, _this.ParentThis);
}

ItHelpCenter.SD.frAdvMsgEdit.prototype.btnCancelar_Click = function (sender, e) {
    _this = sender;
    _this.CallbackModalResult(false, "", _this.ParentThis);
}