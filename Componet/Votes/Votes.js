Componet.TVotes = function(inObjectHtml, Id, inCallBack, inElectoralPadron){
	this.TParent = function () {
		return this;
	}.bind(this);

	var _this = this.TParent();
	this._ObjectHtml = inObjectHtml;
	this._Id = Id;
	this._CallBack = inCallBack;
	this._ElectoralPadron = inElectoralPadron;
	this._QuestionText = "";
	this._CommentText = "";
	this._CommentRequired = true;
	this._ShowComment = false;
	this._ShowCommentsGroup= false;
	this._NumberOfTotalVotes = 0;
	this._IdUser = 0;
	this._NameUser = '';
	this._ProgressColor = 'rgb(255,99,80)';
	this._ProgressTitle = 'Votes';
	this._TotalProgressTitle = 'Padron';
	this._ListItemToChoose = new Array();
	this._ListItemVote = new Array();
	this._ListItemVoteTemp = new Array();
	this._ItemVoteTemp = null;
	this._FunctionVote = null;
	this.TypeGraphics = {
		GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
		Bar: 	 {value: 1, name: 'Bar'},
		Pie: 	 {value: 2, name: 'Pie'},
		Treemap: {value: 3, name: 'Treemap'}
	};
	this._SetTypeGraphics = this.TypeGraphics.GetEnum(1);

	Object.defineProperty(this, 'FunctionVote', {
		get: function(){
			return this._FunctionVote;
		},
		set: function(_Value){
			this._FunctionVote = _Value;
		}
	});

	Object.defineProperty(this, 'ElectoralPadron', {
		get: function(){
			return this._ElectoralPadron;
		},
		set: function(_Value){
			this._ElectoralPadron = _Value;
		}
	});
	Object.defineProperty(this, 'QuestionText', {
		get: function(){
			return this._QuestionText;
		},
		set: function(_Value){
			this._QuestionText = _Value;
		}
	});
	Object.defineProperty(this, 'CommentText', {
		get: function(){
			return this._CommentText;
		},
		set: function(_Value){
			this._CommentText = _Value;
		}
	});
	Object.defineProperty(this, 'CommentRequired', {
		get: function(){
			return this._CommentRequired;
		},
		set: function(_Value){
			this._CommentRequired = _Value;
		}
	});
	Object.defineProperty(this, 'ShowComment', {
		get: function(){
			return this._ShowComment;
		},
		set: function(_Value){
			this._ShowComment = _Value;
		}
	});
	Object.defineProperty(this, 'ShowCommentSGroup', {
		get: function(){
			return this._ShowCommentsGroup;
		},
		set: function(_Value){
			this._ShowCommentsGroup = _Value;
		}
	});
	Object.defineProperty(this, 'NumberOfTotalVotes', {
		get: function(){
			return this._NumberOfTotalVotes;
		},
		set: function(_Value){
			this._NumberOfTotalVotes = _Value;
		}
	});
	Object.defineProperty(this, 'IdUser', {
		get: function(){
			return this._IdUser;
		},
		set: function(_Value){
			this._IdUser = _Value;
		}
	});
	Object.defineProperty(this, 'NameUser', {
		get: function(){
			return this._NameUser;
		},
		set: function(_Value){
			this._NameUser = _Value;
		}
	});
	Object.defineProperty(this, 'ProgressColor', {
		get: function(){
			return this._ProgressColor;
		},
		set: function(_Value){
			if(_Value != ''){
				this._ProgressColor = _Value;
			}
		}
	});
	Object.defineProperty(this, 'ProgressTitle', {
		get: function(){
			return this._ProgressTitle;
		},
		set: function(_Value){
			if(_Value != ''){
				this._ProgressTitle = _Value;
			}
		}
	});
	Object.defineProperty(this, 'TotalProgressTitle', {
		get: function(){
			return this._TotalProgressTitle;
		},
		set: function(_Value){
			if(_Value != ''){
				this._TotalProgressTitle = _Value;
			}
		}
	});
	Object.defineProperty(this, 'SetTypeGraphics', {
		get: function(){
			return this._SetTypeGraphics;
		},
		set: function(_Value){
			this._SetTypeGraphics =  _Value;
		}
	});
	this.TVotesItemToChooseAdd = function (inElementChoose) {
		this._ListItemToChoose.push(inElementChoose);
	};
	this.TVotesItemVoteAdd = function (inElementVote) {
		this._ListItemVoteTemp.push(inElementVote);
		this.NumberOfTotalVotes++;
	};
	this.TVotesItemVoteUserAdd = function (inElementVote) {
		this._ListItemVoteTemp.push(inElementVote);
		this._ListItemVote.push(inElementVote);
		this.NumberOfTotalVotes++;
	}
}

Componet.TVotesElements.TItemToChoose = function(){
	this._Id = "";
	this._Name = "";
	this._Description = "";
	Object.defineProperty(this, 'Id', {
		get: function(){
			return this._Id;
		},
		set: function(_Value){
			this._Id = _Value;
		}
	});
	Object.defineProperty(this, 'Name', {
		get: function(){
			return this._Name;
		},
		set: function(_Value){
			this._Name = _Value;
		}
	});
	Object.defineProperty(this, 'Description', {
		get: function(){
			return this._Description;
		},
		set: function(_Value){
			this._Description =  _Value;
		}
	});
}

Componet.TVotesElements.TItemVote = function(){
	this._IdNumberChoose = "";
	this._IDCMDBCI = "";
	this._Name = "";
	this._Commentary = "";
	Object.defineProperty(this, 'IdNumberChoose', {
		get: function(){
			return this._IdNumberChoose;
		},
		set: function(_Value){
			this._IdNumberChoose = _Value;
		}
	});
	Object.defineProperty(this, 'IDCMDBCI', {
		get: function(){
			return this._IDCMDBCI;
		},
		set: function(_Value){
			this._IDCMDBCI =  _Value;
		}
	});
	Object.defineProperty(this, 'Name', {
		get: function(){
			return this._Name;
		},
		set: function(_Value){
			this._Name =  _Value;
		}
	});
	Object.defineProperty(this, 'Commentary', {
		get: function(){
			return this._Commentary;
		},
		set: function(_Value){
			this._Commentary =  _Value;
		}
	});
}

Componet.TVotes.prototype.View = function(){
	var _this = this.TParent();
	_this.ListBtn = new Array();
	_this.FunctionVote = function(){};
	try{
		if(_this.ElectoralPadron > _this.NumberOfTotalVotes){

			_this.ElementsButtom = new Array();

			var Modal = new TVclModal(document.body, null, "");
			Modal.AddHeaderContent(_this.QuestionText);
			Modal.HeaderContainer.style.fontSize = '25px';

			function MediaQuery(x) {
				if (x.matches) { // menos de 780px
					Modal.This.This.style.paddingRight = '0%';
					Modal.This.This.style.paddingLeft = '0%';
				}
				else {// mayor de 780px
					Modal.This.This.style.paddingRight = '30%';
					Modal.This.This.style.paddingLeft = '30%';
				}
			}

			var x = window.matchMedia("(max-width: 700px)");
			MediaQuery(x) // Call listener function at run time
			x.addListener(MediaQuery) // Attach listener function on state changes

			Modal.ShowModal();

			var ConCandidates = new TVclStackPanel(Modal.Body.This, "", 1, [[12], [12], [12], [12], [12]]);

			for(var i = 0 ; i < _this._ListItemToChoose.length; i++){
				var ConChoose = new TVclStackPanel(ConCandidates.Column[0].This, "Choose_"+i, 2, [[6,6], [6,6], [6,6], [6,6], [6,6]]);
				ConChoose.Row.This.style.marginTop = '5px';
				ConChoose.Column[0].This.style.textAlign = 'center';
				var lblChoose = new TVcllabel(ConChoose.Column[0].This, "", TlabelType.H0);
				lblChoose.Text = _this._ListItemToChoose[i].Name;
				lblChoose.This.setAttribute('data-toggle', 'tooltip');
				lblChoose.This.setAttribute('title', _this._ListItemToChoose[i].Description);
				lblChoose.This.style.cursor = 'pointer';
				lblChoose.This.style.marginTop = '5px';
				lblChoose.VCLType = TVCLType.BS;

				var btnChoose = new TVclButton(ConChoose.Column[1].This, "");
				btnChoose.Text = 'Vote';
				btnChoose.VCLType = TVCLType.BS;
				btnChoose.This.setAttribute('data-id', _this._ListItemToChoose[i].Id);
				btnChoose.This.setAttribute('data-control', i);
				btnChoose.This.style.width = '83px';
				btnChoose.Src = "image/icon-graphics/voto(16).png";
				btnChoose.ImageLocation = "BeforeText";
				btnChoose.onClick = function(){
					if(_this.CommentRequired){
						if(memoCommentary.Text.trim() !== ''){
							var ElementVotes = new Componet.TVotesElements.TItemVote();
							ElementVotes.IdNumberChoose = parseInt(this.dataset.id);
							ElementVotes.IDCMDBCI = _this.IdUser;
							ElementVotes.Name = _this.NameUser;
							ElementVotes.Commentary = memoCommentary.Text;
							_this.TVotesItemVoteUserAdd(ElementVotes)
							_this._ItemVoteTemp = ElementVotes;
							// _this.NumberOfTotalVotes++;
							Modal.CloseModal();
							_this.ShowStatistic();

							// Función para ser creada desde fuera del componente (ejem. guardar los datos)
							_this.FunctionVote(ElementVotes);
						}
						else{
							alert('The comment is required');
						}
					}
					else{
						var ElementVotes = new Componet.TVotesElements.TItemVote();
						ElementVotes.IdNumberChoose = parseInt(this.dataset.id);
						ElementVotes.IDCMDBCI = _this.IdUser;
						ElementVotes.Name = _this.NameUser;
						ElementVotes.Commentary = memoCommentary.Text;
						_this.TVotesItemVoteUserAdd(ElementVotes)
						_this._ItemVoteTemp = ElementVotes;
						// _this.NumberOfTotalVotes++;
						Modal.CloseModal();
						_this.ShowStatistic();

						// Función para ser creada desde fuera del componente (ejem. guardar los datos)
						_this.FunctionVote(ElementVotes);
					}
				}
				btnChoose.This.style.padding = '0px';

				_this.ListBtn.push(btnChoose);
			}
			$('[data-toggle="tooltip"]').tooltip();

			var ConCommentary = new TVclStackPanel(Modal.Body.This, "", 2, [[12, 12], [12, 12], [12, 12], [12, 12], [12, 12]]);
			ConCommentary.Row.This.style.marginTop = '10px'
			var lblCommentary = new TVcllabel(ConCommentary.Column[0].This, "", TlabelType.H0);
			lblCommentary.Text = 'Commentary'/*_this.DBTranslate.get_Text2(_this.Mythis, "DATASQL");*/
			lblCommentary.VCLType = TVCLType.BS;
			var memoCommentary = new TVclMemo(ConCommentary.Column[1].This, "");
			memoCommentary.VCLType = TVCLType.BS;
			memoCommentary.This.removeAttribute("cols");
			memoCommentary.This.removeAttribute("rows");
			memoCommentary.This.classList.add("form-control");
			memoCommentary.This.setAttribute("placeholder", "Enter your comment here...");
			memoCommentary.Text = '';
			memoCommentary.This.style.marginBottom = '5px'

			if(!_this.CommentRequired){
				if(!_this.ShowComment){
					ConCommentary.Row.This.style.display = 'none';
				}
			}

			Modal.FunctionAfterClose = function () {
				$(this.This.This).remove();
			}
		
		}else{
			alert('The number of voters is complete');
			_this.ShowStatistic(); //si se desea que al estar lleno la votación en este se muestre solo los gráficos
		}
	} 
	catch (e) {
		SysCfg.Log.Methods.WriteLog("Votes.js Componet.TVotes.prototype.View", e);
	}
}

Componet.TVotes.prototype.ShowStatistic = function(){
	var _this = this.TParent();

	_this.importarScript(SysCfg.App.Properties.xRaiz + "Componet/GraphicsPBI/Scripts/Plugin/Chart/Chart.bundle.min.js", function () {
		_this.importarScript(SysCfg.App.Properties.xRaiz + "Componet/GraphicsPBI/Scripts/Plugin/D3/d3.min.js", function () {
			_this.importarStyle(SysCfg.App.Properties.xRaiz + "Componet/GraphicsPBI/Bar/css/Bar.css", function () {
				_this.importarStyle(SysCfg.App.Properties.xRaiz + "Componet/GraphicsPBI/Pie/css/Pie.css", function () {
					_this.importarStyle(SysCfg.App.Properties.xRaiz + "Componet/GraphicsPBI/TreeMap/css/TreeMap.css", function () {
							_this.CreateGraphics();
					}, function (e) {
						SysCfg.Log.Methods.WriteLog("Votes.js Componet.TVotes.prototype.ShowStatistic _this.importarStyle(" + _this.importarStyle + "Componet/GraphicsPBI/TreeMap/css/TreeMap.css)", e);
					});
				}, function (e) {
					SysCfg.Log.Methods.WriteLog("Votes.js Componet.TVotes.prototype.ShowStatistic _this.importarStyle(" + _this.importarStyle + "Componet/GraphicsPBI/Pie/css/Pie.css)", e);
				});
			}, function (e) {
				SysCfg.Log.Methods.WriteLog("Votes.js Componet.TVotes.prototype.ShowStatistic _this.importarStyle(" + _this.importarStyle + "Componet/GraphicsPBI/Bar/css/Bar.css)", e);
			});
		}, function (e) {
			SysCfg.Log.Methods.WriteLog("Votes.js Componet.TVotes.prototype.ShowStatistic _this.importarScript(" + _this.importarScript + "Componet/GraphicsPBI/Scripts/Plugin/D3/d3.min.js)", e);
		});
	}, function (e) { 
		SysCfg.Log.Methods.WriteLog("Votes.js Componet.TVotes.prototype.ShowStatistic _this.importarScript(" + _this.importarScript + "Componet/GraphicsPBI/Scripts/Plugin/Chart/Chart.bundle.min.js)", e);
	});
}

Componet.TVotes.prototype.CreateGraphics = function(){
	var _this = this.TParent();
	try{
		_this._ArrayColors =['#01B8AA', '#FEAB85', '#BE4A47', '#A66999', '#B59525', '#374649', '#F95C38', '#F2C80F', '#8AD4EB', '#A66999',
							'#3599B8', '#AF9393', '#2B6863', '#1C2325', '#047283', '#C19028', '#142D1A', '#9B0F7D', '#FFED28', '#10099B'];
		_this._TempColors = new Array();

		$(_this._ObjectHtml).html('');

		var ContStatistic = new TVclStackPanel(_this._ObjectHtml, "", 1, [[12], [12], [12], [12], [12]]);
		ContStatistic.Row.This.style.marginBottom = '20px';

		var ContImages = new TVclStackPanel(ContStatistic.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
		ContImages.Row.This.style.marginBottom = '5px';
		var ImageBar = new TVclImagen(ContImages.Column[0].This, "");
		ImageBar.This.style.marginLeft = "5px";
		ImageBar.Title = "Bar";
		ImageBar.Src = "image/icon-graphics/graph(24).png";
		ImageBar.Cursor = "pointer";
		ImageBar.onClick = function () {
			_this.GraphicBar(ContGraphic.Column[0].This);
		}
		var ImagePie = new TVclImagen(ContImages.Column[0].This, "");
		ImagePie.This.style.marginLeft = "5px";
		ImagePie.Title = "Pie";
		ImagePie.Src = "image/icon-graphics/pie-chart(24).png";
		ImagePie.Cursor = "pointer";
		ImagePie.onClick = function () {
			_this.GraphicPie(ContGraphic.Column[0].This);
		}
		var ImageTreemap = new TVclImagen(ContImages.Column[0].This, "");
		ImageTreemap.This.style.marginLeft = "5px";
		ImageTreemap.Title = "Treemap";
		ImageTreemap.Src = "image/icon-graphics/cuadricula(24).png";
		ImageTreemap.Cursor = "pointer";
		ImageTreemap.onClick = function () {
			_this.GraphicTreemap(ContGraphic.Column[0].This);
		}	
		
		var ContGraphic = new TVclStackPanel(ContStatistic.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
		ContGraphic.Row.This.style.paddingTop = '5px';
		
		switch (_this.SetTypeGraphics.value) {
		  case 1:
			_this.GraphicBar(ContGraphic.Column[0].This);
			break;
		  case 2:
			_this.GraphicPie(ContGraphic.Column[0].This);
			break;
		  case 3:
			_this.GraphicTreemap(ContGraphic.Column[0].This);
			break;
		}
		
		var ContCounter = new TVclStackPanel(ContStatistic.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
		ContCounter.Row.This.style.height = '30px';
		ContCounter.Column[0].This.removeAttribute('class');

		var lblCounterVotes = new TVcllabel(ContCounter.Column[0].This, "", TlabelType.H0);
		lblCounterVotes.Text = _this.ProgressTitle + ': ' + _this.NumberOfTotalVotes;
		lblCounterVotes.VCLType = TVCLType.BS;
		lblCounterVotes.This.style.float = 'left';
		lblCounterVotes.This.style.fontFamily = 'cursive';

		var lblCounterPadron = new TVcllabel(ContCounter.Column[0].This, "", TlabelType.H0);
		lblCounterPadron.Text = _this.TotalProgressTitle + ': ' + _this.ElectoralPadron;
		lblCounterPadron.VCLType = TVCLType.BS;
		lblCounterPadron.This.style.float = 'right';
		lblCounterPadron.This.style.fontFamily = 'cursive';

		$(ContCounter.Column[0].This).progressbar({
			value: _this.NumberOfTotalVotes,
			min: 0,
			max: _this.ElectoralPadron
		});
		ContCounter.Column[0].This.style.backgroundColor = 'transparent';
		ContCounter.Column[0].This.children[2].style.backgroundColor = _this.ProgressColor;
		// alterado por mobile
		ContCounter.Row.This.style.paddingLeft = '15px';
		ContCounter.Row.This.style.paddingRight = '15px';

		var ContUserCommentary = new TVclStackPanel(_this._ObjectHtml, "", 1, [[12], [12], [12], [12], [12]]);
		ContUserCommentary.Column[0].This.style.minHeight = '300px';
		ContUserCommentary.Column[0].This.style.backgroundColor = '#f7f7f7';
		ContUserCommentary.Column[0].This.style.border = '2px solid #c3bebd';
		// ContUserCommentary.Column[0].This.style.borderRadius = '10px';
		var userCommentary = new Udiv(ContUserCommentary.Column[0].This, '');
		userCommentary.style.width = '100%';
		// alterado por mobile
		ContUserCommentary.Row.This.removeAttribute('class');

		_this.importarStyle(SysCfg.App.Properties.xRaiz + "Componet/Votes/css/Votes.css", function () {
			for(var i = 0 ; i < _this._ListItemVoteTemp.length ; i++){
				for(var j = 0 ; j < _this._ListItemToChoose.length ; j ++){
					if(parseInt(_this._ListItemVoteTemp[i].IdNumberChoose) ===  parseInt(_this._ListItemToChoose[j].Id)){
						
						var bubbleCommentary = new TVclStackPanel(userCommentary, "", 2, [[4, 8], [4, 8], [4, 8], [2, 10], [1, 11]]);

						bubbleCommentary.Row.This.style.marginTop = '5px';
						bubbleCommentary.Row.This.style.marginRight = '23px';
						bubbleCommentary.Row.This.style.marginBottom = '5px';

						var iconUser = new TVclImagen(bubbleCommentary.Column[0].This, '');
						iconUser.Src = 'image/24/user-blue2.png';
						iconUser.This.style.display = 'block';
						iconUser.This.style.margin = '0 auto 0 auto';
						var nameUser = new Udiv(bubbleCommentary.Column[0].This, '');
						nameUser.innerText = _this._ListItemVoteTemp[i].Name/*'Usr ' + _this._ListItemVoteTemp[i].IDCMDBCI*/;
						nameUser.style.textAlign = 'center';
						nameUser.style.fontSize = '10px';

						bubbleCommentary.Column[1].This.classList.add('bubbleComent');
						bubbleCommentary.Column[1].This.innerText = _this._ListItemToChoose[j].Name + ":\n" + _this._ListItemVoteTemp[i].Commentary;
						bubbleCommentary.Column[1].This.style.wordWrap = 'break-word'; //evitar que las letras salgan del contenedor

					}
				}
			}
		}, function (e) { });

		if(!_this.CommentRequired){
			if(!_this.ShowCommentSGroup){
				ContUserCommentary.Column[0].This.style.display = 'none';
			}
		}
	} 
	catch (e) {
		SysCfg.Log.Methods.WriteLog("Votes.js Componet.TVotes.prototype.CreateGraphics", e);
	}
}

Componet.TVotes.prototype.GraphicBar = function(container){
	var _this = this.TParent();
	$(container).html('');
	try{
		var objBars = new Componet.TGraphicsPBI.TVclBars(container, "");
		for(var i = 0 ; i < _this._ListItemToChoose.length; i++){
			var Element = new Componet.TGraphicsElements.TVclBarsElements();
			Element.DisplayValue = _this._ListItemToChoose[i].Name;
			// var color = _this._GetRandomColor()
			var color = Source.Sequencial.Methods.GetColor();
			var cont = 0;
			for(var j = 0 ; j < _this._ListItemVoteTemp.length ; j++){
				if(parseInt(_this._ListItemVoteTemp[j].IdNumberChoose) ===  parseInt(_this._ListItemToChoose[i].Id)){
					cont++;
				}
			}
			Element.Value = cont;
			Element.Color = color;
			Element.Text = 'Votes';
			Element.ColorOption = 'white';
			Element.BorderOption = '#00a98f';
			Element.DisplayOption = false;
			Element.ColorText = 'black';
			Element.BorderColorGraphic = color;
			Element.HoverColorGraphic = color;
			Element.Id = _this._ListItemToChoose[i].Id;
			Element.onClick = function (inElement) {
				// 
			};
			objBars.BarsElementsAdd(Element);
		}
		/*DECLARACION DE PROPIEDADES PARA PLUGIN*/
		objBars.Title = "Votes"
		objBars.BorderWidth = 1;
		objBars.BeginAtZero = true;
		objBars.MinRotation = 40;
		objBars.MaxRotation = 40;
		objBars.HeightPlugin = "300px";
		objBars.CreateGraphics();
		objBars.BoxGraphics[1].removeAttribute('class');
		objBars.BoxGraphics[1].setAttribute('class','col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12');
		objBars.onClickPlugin = function (chartData, idx) {
			// 
		}
	} 
	catch (e) {
		SysCfg.Log.Methods.WriteLog("Votes.js Componet.TVotes.prototype.GraphicBar", e);
	}
}

Componet.TVotes.prototype.GraphicPie = function(container){
	try{
		var _this = this.TParent();
		$(container).html('');
		var objPieOne = new Componet.TGraphicsPBI.TVclPie(container, "");
		for(var i = 0 ; i < _this._ListItemToChoose.length; i++){
			var Element = new Componet.TGraphicsElements.TVclPieElements();
			Element.DisplayValue = _this._ListItemToChoose[i].Name;
			var color = _this._GetRandomColor()
			var cont = 0;
			for(var j = 0 ; j < _this._ListItemVoteTemp.length ; j++){
				if(parseInt(_this._ListItemVoteTemp[j].IdNumberChoose) ===  parseInt(_this._ListItemToChoose[i].Id)){
					cont++;
				}
			}
			Element.Value = cont;
			Element.Color = color;
			Element.Text = 'Votes';
			Element.ColorOption = 'white';
			Element.BorderOption = '#00a98f';
			Element.DisplayOption = true;
			Element.ColorText = 'black';
			Element.BorderColorGraphic = color;
			Element.HoverColorGraphic = color;
			Element.Id = _this._ListItemToChoose[i].Id;
			Element.onClick = function (inElement) {
				// 
			};
			objPieOne.PieElementsAdd(Element);
		}
		objPieOne.Title = "Votes"
		objPieOne.BorderWidth = 1;
		objPieOne.BeginAtZero = true;
		objPieOne.Option = false;
		objPieOne.HeightPlugin = "300px";
		objPieOne.CreateGraphics();
		objPieOne.onClickPlugin = function (chartData, idx) {
			// 
		}
	} 
	catch (e) {
		SysCfg.Log.Methods.WriteLog("Votes.js Componet.TVotes.prototype.GraphicPie", e);
	}
}

Componet.TVotes.prototype.GraphicTreemap = function(container){
	try{
		var _this = this.TParent();
		$(container).html('');
		var objTreeMap = new Componet.TGraphicsPBI.TVclTreeMap(container, '');
		for(var i = 0 ; i < _this._ListItemToChoose.length; i++){
			var Element = new Componet.TGraphicsElements.TVclTreeMapElements();
			Element.Name = _this._ListItemToChoose[i].Name;
			var color = _this._GetRandomColor()
			var cont = 0;
			for(var j = 0 ; j < _this._ListItemVoteTemp.length ; j++){
				if(parseInt(_this._ListItemVoteTemp[j].IdNumberChoose) ===  parseInt(_this._ListItemToChoose[i].Id)){
					cont++;
				}
			}
			Element.Value = cont;
			Element.Color = color;
			Element.Id = _this._ListItemToChoose[i].Id;
			Element.onClick = function (inElement) {
				// 
			};
			objTreeMap.TreeMapElementsAdd(Element);
		}
		objTreeMap.HeightPlugin = "300px";
		objTreeMap.CreateGraphics();
		objTreeMap.ObjectHtml.children[0].style.marginLeft = '4%'
	} 
	catch (e) {
		SysCfg.Log.Methods.WriteLog("Votes.js Componet.TVotes.prototype.GraphicTreemap", e);
	}
}

Componet.TVotes.prototype._GetRandomColor = function () {
	try{
		var _this = this.TParent();
		if (_this._ArrayColors.length === 0) {
			_this._ArrayColors = _this._TempColors;
			_this._TempColors = new Array();
		}
		var randomValue = Math.floor((Math.random() * _this._ArrayColors.length));
		var randomColor = _this._ArrayColors[randomValue];
		_this._ArrayColors.splice(randomValue, 1);
		_this._TempColors.push(randomColor);
		return randomColor;
	} 
	catch (e) {
		SysCfg.Log.Methods.WriteLog("Votes.js Componet.TVotes.prototype._GetRandomColor", e);
	}
}

Componet.TVotes.prototype.importarScript = function (nombre, onSuccess, onError) {
	var s = document.createElement("script");
	s.onload = onSuccess;
	s.onerror = onError;
	s.src = nombre;
	document.querySelector("head").appendChild(s);
}

Componet.TVotes.prototype.importarStyle = function (nombre, onSuccess, onError) {
	var style = document.createElement("link");
	style.rel = "stylesheet";
	style.type = "text/css";
	style.href = nombre;
	var s = document.head.appendChild(style);
	s.onload = onSuccess;
	s.onerror = onError;
}
