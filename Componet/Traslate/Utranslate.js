﻿ 
var UindexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
var IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction;
var IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange
var arrayManuelElements = []; //arreglo guarda los elementos fijos y la traduccion temporal

function DBTranslate(inDBName, inVersion) {
    this.DBName = inDBName;
    this.Version = inVersion;
    this.thisDb = null;   
    this.JS_IDSCheck = [];
    this.varParent = function () {
        return this;
    }.bind(this);
}

//Abrir la Base local
DBTranslate.prototype.Open = function (onSuccessCallback, onErrorCallBack) {
    var Parent = this.varParent();
    var openRequest = UindexedDB.open(this.DBName, this.Version);
    openRequest.onupgradeneeded = function (e) {
        // Create Translate 
        Parent.thisDb = e.target.result;
        if (!Parent.thisDb.objectStoreNames.contains("Translate")) {
            //make the Translate 
            var objectStore = Parent.thisDb.createObjectStore("Translate", { keyPath: "Idx", autoIncrement: true });
            objectStore.createIndex('ID', 'ID', { unique: false });
            objectStore.createIndex('IDSUB', 'IDSUB', { unique: false });
            objectStore.createIndex('LangTextDefault', 'LangTextDefault', { unique: false });
        }
    };
    openRequest.onsuccess = function (e) {       
        Parent.thisDb = e.target.result; // almacenamos la base da datos                 
        onSuccessCallback(); 
    };

    openRequest.onerror = onErrorCallBack;
};

//Proceso inicial
DBTranslate.prototype.SetTranslate = function (JS_IDS, onSuccessCallback, onErrorCallBack) {
    var Parent = this.varParent();
    
    this.ReadBD(JS_IDS, function (res) {      
        if (res == 1) {
            //proceso exitoso
            onSuccessCallback();
        } else if (res == 2) {
            //no existe el ID // Guardar todo el arreglo inicial
           Parent.InsertToServer(JS_IDS, function (res) {
                if (res == 1) {
                    onSuccessCallback();
                } else {
                    onErrorCallBack();
                }
            });            
        } else if (res == 3) {
            // No existe elgun SubID
            var JS_IDSADD = [];
            var idx = 0;
            
            for (var y = 0; y < Parent.JS_IDSCheck.length; y++) {
                if (Parent.JS_IDSCheck[y] == false) {
                    var Atributos = [];
                    Atributos[0] = JS_IDS[y][0];
                    Atributos[1] = JS_IDS[y][1];
                    Atributos[2] = JS_IDS[y][2];
                    JS_IDSADD[idx] = Atributos;
                    idx = idx + 1;
                }
            }
            
            //Guardar SUBIDs que no existen
            Parent.InsertToServer(JS_IDSADD, function (res) {
               
                if (res == 1) {
                    onSuccessCallback();
                } else {
                    onErrorCallBack();
                }
            });            
        } else {
            //Error
            onErrorCallBack();
        }
    });
}

//leer el cursor Traslate y llenar el arreglo Resultado
DBTranslate.prototype.ReadBD = function (JS_IDS, CallBack) {
    var Parent = this.varParent();
    var foundId = false;
    var data = null;
    var Fnd = 0;    
    var Idx = arrayManuelElements.length;
    var xrows = JS_IDS.length;    
    for (var i = 0; i < xrows; i++) {
        this.JS_IDSCheck[i] = false;
    }
    var transaction = this.thisDb.transaction(["Translate"], "readonly");
    var MyObjectStore = transaction.objectStore("Translate");
    MyObjectStore.openCursor().onsuccess = function (e) {       
        var cursor = e.target.result;
        if (cursor) {            
            if (cursor.value.ID == JS_IDS[0][0]) { //validar que exista el ID buscado
                foundId = true;
                data = cursor.value;
                //como ya existe cargamos resultados
                for (var x = 0; x < JS_IDS.length; x++) { //recorrer el arreglo en busqeuda de los SUBID's
                    if (cursor.value.IDSUB == JS_IDS[x][1]) {//Validar SUBID
                        if (Parent.JS_IDSCheck[x] == false) { //Validar que el SUBID localizado no haya sido agregado ya en un ciclo anterior
                            var arraytemp = [];
                            arraytemp[0] = cursor.value.ID;
                            arraytemp[1] = cursor.value.IDSUB;
                            arraytemp[2] = cursor.value.LangTextDefault;
                            
                            arrayManuelElements[Idx] = arraytemp;
                            Idx = Idx + 1; //aumentar el indice del arreglo
                            Fnd = Fnd + 1; //Fnd indica la cantidad de SUBID's encontrados, para la validacion del acceso a loadpage()
                            Parent.JS_IDSCheck[x] = true; //señala que el indice x del arreglo fue encontrado                               
                        }
                    }
                }
            }
            if (Fnd == (xrows)) { // validar que todos los SUBID's hayan sido encontrados
                CallBack(1);
            } else {
                cursor.continue();
            }
        } else {
            if (foundId == false) {//no existe el ID,
                CallBack(2);
            } else {
                CallBack(3);
            }
        }        
    }
}

//Insertar un arreglo en la BD del servidor
DBTranslate.prototype.InsertToServer = function (JS_IDS, CallBack) {  
    var Parent = this.varParent();
    var dataArray = {
        arrayString: JS_IDS
    };
     
    direc = SysCfg.App.Properties.xRaiz + 'Componet/Traslate/WcfTraslate.svc/GetLangTextList';
    $.ajax({
        type: "POST",
        async: false,
        url: direc,
        data: JSON.stringify(dataArray),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {          
            //ajustamos los resultados A LOS ELEMENTOS               
            var lista = (typeof response.d) == 'string' ?
                             eval('(' + response.d + ')') :
                             response.d;            
            for (var i = 0; i < lista.length; i++) {              
                Parent.Add(lista[i].ID, lista[i].IDSUB, lista[i].LangTextDefault);
            }
            
            Parent.ReadBD(JS_IDS, function (res) {               
                CallBack(res);
            });           
        },
        error: function (error) {
            SysCfg.Log.Methods.WriteLog("Traslate.js DBTranslate.prototype.InsertToServer Componet/Traslate/WcfTraslate.svc/GetLangTextList \n Location: Translate / Send manual List " + error.txtDescripcion);
            CallBack(4);
        }
    });
}


//Agregar un registro a IndexDB
DBTranslate.prototype.Add = function (xID, xIDSUB, xLangTextDefault) {
    var transaction = this.thisDb.transaction(["Translate"], "readwrite");
    var objectStore = transaction.objectStore("Translate");
    var add = objectStore.put({ ID: xID, IDSUB: xIDSUB, LangTextDefault: xLangTextDefault });

    add.onsuccess = function (event) {
        //alert("Add: " + ID +" "+ IDSUB +" "+ event.target.result);
    }
    add.onerror = function (event) {
        //alert('Err : ' + event.target.result);
    };
}


DBTranslate.prototype.get_Text2 = function (ID,SUBID) {
    var res = "";
    for (var i = 0; i < arrayManuelElements.length; i++) {    
        if ((arrayManuelElements[i][1] == SUBID) && (arrayManuelElements[i][0] == ID)) {
            
            res = arrayManuelElements[i][2];
            i = arrayManuelElements.length;
        }
    }
    return res;
}

//Devolver un texto buscado por el SUBID
DBTranslate.prototype.get_Text = function (SUBID) {
    var res = "";
    for (var i = 0; i < arrayManuelElements.length; i++) {
        if (arrayManuelElements[i][1] == SUBID) {
            res = arrayManuelElements[i][2];
            i = arrayManuelElements.length;
        }
    }
    return res;
}

DBTranslate.prototype.ClearDB = function (onSuccessCallback, onErrorCallBack) {
    var transaction = this.thisDb.transaction(["Translate"], "readwrite");
    transaction.oncomplete = function (event) {      
        
    };
    transaction.onerror = function (event) {       
        onErrorCallBack();
    };

    var objectStore = transaction.objectStore("Translate");

    var objectStoreRequest = objectStore.clear();

    objectStoreRequest.onsuccess = function (event) {      
        onSuccessCallback();
    };
};


DBTranslate.prototype.SET_ID = function (IDValue, onSuccessCallback, onErrorCallBack) {
    var Parent = this.varParent();  
    //guardar el codigo de lenguaje
    var dataArray = {
        'IDLANGUAGE': IDValue
    };
    direc = SysCfg.App.Properties.xRaiz + 'Componet/Traslate/WcfTraslate.svc/SETIDLANGUAGE';
    //enviamos el arreglo a la webService  
    $.ajax({
        type: "POST",
        async: false,
        url: direc,
        data: JSON.stringify(dataArray),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            onSuccessCallback();
        },
        error: function (error) {
            onErrorCallBack();
        }
    });
}

DBTranslate.prototype.GET_ID = function (onSuccessCallback, onErrorCallBack) {
    var dataArray = {};
    direc = SysCfg.App.Properties.xRaiz + 'Componet/Traslate/WcfTraslate.svc/GETIDLANGUAGE';
    $.ajax({
        type: "POST",
        async: false,
        url: direc,
        data: JSON.stringify(dataArray),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            onSuccessCallback(response.d);             
        },
        error: function (error) {
            onErrorCallBack();
        }
    });
}