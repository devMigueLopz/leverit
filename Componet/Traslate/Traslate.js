﻿var arrayManuelElements = []; //arreglo guarda los elementos fijos y la traduccion temporal
var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
var IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction;
var IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange
var xrows;
var lenguaje;

var thisDb;
 

$(document).ready(function () {    
});

function startDB() {
    var openRequest = indexedDB.open("TraslateBD", 1);
    openRequest.onupgradeneeded = function (e) {
        thisDb = e.target.result; // almacenamos la base da datos
        // Create Traslate
        if (!thisDb.objectStoreNames.contains("Translate")) {
            //console.log("I need to make the Traslate objectstore");

            var objectStore = thisDb.createObjectStore("Translate", { keyPath: "Idx", autoIncrement: true });
            objectStore.createIndex('ID', 'ID', { unique: false });
            objectStore.createIndex('IDSUB', 'IDSUB', { unique: false });
            objectStore.createIndex('LangTextDefault', 'LangTextDefault', { unique: false });
        }
    };

    openRequest.onsuccess = function (e) {
        thisDb = e.target.result; // almacenamos la base da datos                 
        load(); // la funcion load debe existir en el archivo que llama el archivo tralate.js
    };

    openRequest.onerror = function (e) {
        console.log('Error cargando la base de datos'); 
        load();
    };

    openRequest.onblocked = function (e) {
        console.log('Acceso bloqueado la base local de datos');
        load();
    };
}


function DeleteBase() {    
    var req = indexedDB.deleteDatabase("TraslateBD");
    req.onsuccess = function () {       
        reloadpage();
    };
    req.onerror = function () {       
        clearData();
        //console.log("Couldn't delete database");
    };
    req.onblocked = function () {        
        clearData();
        //console.log("Couldn't delete database due to the operation being blocked");
    };
}

function clearData() {
    var transaction = thisDb.transaction(["Translate"], "readwrite");
    transaction.oncomplete = function (event) {
        //note.innerHTML += '<li>Transaction completed: database modification finished.</li>';
    };
    transaction.onerror = function (event) {
        //note.innerHTML += '<li>Transaction not opened due to error: ' + transaction.error + '</li>';
    };
    // create an object store on the transaction
    var objectStore = transaction.objectStore("Translate");

    // clear all the data out of the object store
    var objectStoreRequest = objectStore.clear();

    objectStoreRequest.onsuccess = function (event) {
        //note.innerHTML += '<li>Data cleared.</li>';
        reloadpage();
    };
};



function objectBD_Add(ID, IDSUB, LangTextDefault) { //agregamos registros
    
    var transaction = thisDb.transaction(["Translate"], "readwrite");
    var objectStore = transaction.objectStore("Translate");

    var add = objectStore.put({ ID: ID, IDSUB: IDSUB, LangTextDefault: LangTextDefault });

    add.onsuccess = function (event) {
        //alert("Add: " + ID +" "+ IDSUB +" "+ event.target.result);
    }
    add.onerror = function (event) {
        //alert('Err : ' + event.target.result);
    };
}

function objectBD_Exist(JS_IDS, rows) { //validar si existe un ID  
    
    var res = false;
    var Idx = 0;
    xrows = rows;
    var foundId = false;
    var JS_IDSN = [rows];
    for (var i = 0; i < rows; i++) {
        JS_IDSN[i] = false;
    }
    var transaction = thisDb.transaction(["Translate"], "readonly");
    var objectStore = transaction.objectStore("Translate");
    objectStore.openCursor().onsuccess = function (event) {       
        var cursor = event.target.result;
        if (cursor) {
                       
            if (cursor.value.ID == JS_IDS[0][0]) { //validar que exista el ID buscado
                foundId = true;               
                //como ya existe cargamos resultados
                for (var x = 0; x < JS_IDS.length; x++) { //recorrer el arreglo en busqeuda de los SUBID's
                    if (cursor.value.IDSUB == JS_IDS[x][1]) {//Validar SUBID
                        if (JS_IDSN[x] == false) { //Validar que el SUBID localizado no haya sido agregado ya en un ciclo anterior
                            var arraytemp = [];
                            arraytemp[0] = cursor.value.IDSUB;
                            arraytemp[1] = cursor.value.LangTextDefault;
                            arrayManuelElements[Idx] = arraytemp;
                            Idx = Idx + 1; //Idx indica la cantidad de SUBID's encontrados, para la validacion del acceso a loadpage()
                            JS_IDSN[x] = true; //señala que el indice x del arreglo fue encontrado
                            
                        }                      
                    }
                }                         
            }           
            if (Idx >= (xrows)) { // validar que todos los SUBID's hayan sido encontrados
                               
                loadpage();//llamar la funcion principal de la pagina que llamo traslate.js            
            } else {
                cursor.continue();
            }
        }
        else {
           
            //al ingresar aqui es porque no encontro el ID o algun SUBID
            if (foundId == false) {
                get_Traduction(JS_IDS); //no existe el ID
            } else {
                
                var JS_IDSADD = [];
                var idx = 0;
                for (var y = 0; y < JS_IDSN.length; y++) {
                    if (JS_IDSN[y] == false) {
                        var Atributos = [];
                        Atributos[0] = JS_IDS[y][0];
                        Atributos[1] = JS_IDS[y][1];
                        Atributos[2] = JS_IDS[y][2];
                        JS_IDSADD[idx] = Atributos;
                        idx = idx + 1;
                    }                       
                }
                
                get_Traduction(JS_IDSADD); //JS_IDSADD arreglo de SUBIDs que no existen
            }     
        }
    };
}


// Jaimes funciones manuales -----------------------------
function tras_manualIDs(JS_IDS) {
    
    var rows = JS_IDS.length;
    //alert(JS_IDS[0][0]);
    // validar si el elemento ya existe en indexdb
    objectBD_Exist(JS_IDS, rows);
}

function get_Traduction(JS_IDS) {
    
    //enviamos el arreglo a la webService y agregarla en el indexDB
    var dataArray = {
        arrayString: JS_IDS
    };

    direc = SysCfg.App.Properties.xRaiz + 'Componet/Traslate/WcfTraslate.svc/GetLangTextList';
    $.ajax({
        type: "POST",
        async: false,
        url: direc,
        data: JSON.stringify(dataArray),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //ajustamos los resultados A LOS ELEMENTOS               
            var lista = (typeof response.d) == 'string' ?
                             eval('(' + response.d + ')') :
                             response.d;
            for (var i = 0; i < lista.length; i++) {
                
                objectBD_Add(lista[i].ID, lista[i].IDSUB, lista[i].LangTextDefault);
            }
            
            var rows = lista.length;
            objectBD_Exist(JS_IDS, rows);//cargamos el cursor con la informacion
        },
        error: function (error) {
            SysCfg.Log.Methods.WriteLog("Traslate.js get_Traduction() Componet/Traslate/WcfTraslate.svc/GetLangTextList"+error.txtDescripcion + " \n Location: Traslate / Send manual List"); 
        }
    });
}

function get_LangTextDefault(SUBID) {
    
    var res = "";
    for (var i = 0; i < arrayManuelElements.length; i++) {
        if (arrayManuelElements[i][0] == SUBID) {
            res = arrayManuelElements[i][1];
            i = arrayManuelElements.length;
        }
    }
    return res;
}

// Jaimes funciones automaticas///////////////////////////////////////////////////////////////////////
function tras_catchIDs(nombre) {

    //ID           IDSUB               Spanich    Ingles    
    // frmain      SpNode1_1.text        Chat       Chat   

    //elementos a buscar
    var elements = [];   
    elements[0] = "document.getElementsByTagName('span')";
    elements[1] = "document.getElementsByClassName('tooltip')";
    elements[2] = "document.getElementsByTagName('input')";

    arrayElements = [];
    //Recorrer elementos
    var j = 0;
    for (var x = 0; x < elements.length; x++) {
        capas = eval(elements[x]); //document.getElementsByTagName('span')
        var cant = capas.length;
        for (i = 0; i < cant; i++) {
            idSeek = capas[i].id;
            if (idSeek != "") {
                idElement = document.getElementById(idSeek).id; //buscar el elemento por el id
                //Text
                try {
                    text = document.getElementById(capas[i].id).textContent;
                    var Atributos = [];
                    Atributos[0] = nombre;
                    Atributos[1] = idElement + ".text";
                    Atributos[2] = text;
                    arrayElements[j] = Atributos;
                    j = j + 1;
                } catch (e) {
                    SysCfg.Log.Methods.WriteLog("Traslate.js", e);
                }
                // Value
                try {
                    if (x == 2) { //en caso de que sean los input
                        value = document.getElementById(capas[i].id).textContent;
                        var Atributos = [];
                        Atributos[0] = nombre;
                        Atributos[1] = idElement + ".value";
                        Atributos[2] = value;
                        arrayElements[j] = Atributos;
                        j = j + 1;
                    }
                } catch (e) {
                    SysCfg.Log.Methods.WriteLog("Traslate.js tras_catchIDs ", e);
                }
            }
        }
    }


    //ENVIAR ELEMENTOS A LA WEBSERVICIE 
    
    var dataArray = {
        arrayString: arrayElements
    };

    
    //direc = SysCfg.App.Properties.xRaiz + 'Componet/Traslate/WcfTraslate.svc/getprueba';
    try {
        direc = SysCfg.App.Properties.xRaiz + 'Componet/Traslate/WcfTraslate.svc/GetLangTextList';
    } catch (e) {
        direc = 'WcfTraslate.svc/GetLangTextList';
        SysCfg.Log.Methods.WriteLog("Traslate.js", e);
    }
  
    //enviamos el arreglo a la webService  
    $.ajax({
        type: "POST",
        async: false,
        url: direc,
        data: JSON.stringify(dataArray),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            
            //ajustamos los resultados A LOS ELEMENTOS
            var lista = (typeof response.d) == 'string' ?
                             eval('(' + response.d + ')') :
                             response.d;

            for (var i = 0; i < lista.length; i++) {
                datos = lista[i].IDSUB.split(".");
                var textDefault = lista[i].LangTextDefault;
                textDefault = textDefault.replace('"', ' ');
                if (datos[1] == "text") {                   
                    code = 'document.getElementById("' + datos[0] + '").innerText = "' + textDefault + '";';
                } else {
                    code = 'document.getElementById("' + datos[0] + '").value = "' + textDefault + '";';
                }
               
                try {   
                    eval(code);
                } catch (e) {
                    SysCfg.Log.Methods.WriteLog("Traslate.js tras_catchIDs ", e);
                }                
            }

        },
        error: function (error) {
            SysCfg.Log.Methods.WriteLog("Traslate.js tras_catchIDs() Componet/Traslate/WcfTraslate.svc/GetLangTextList " + error.txtDescripcion + " \n Location: Traslate / Send list text");
        }
    });
}

function arrayManuelElement_Add(UnElemento) {
    var countaerray = arrayManuelElements.length;

    var NoExiste = objectBD_Exist(UnElemento.ID, UnElemento.IDSUB, UnElemento.LangTextDefault);
    /*
    var NoExiste = true;
    for (var i = 0; i < arrayManuelElements.length; i++) {
        if ((arrayManuelElements[i].ID == UnElemento.ID) && (arrayManuelElements[i].IDSUB == UnElemento.IDSUB))
        {
            NoExiste = false;
            break;
        }
    }*/
    if (NoExiste == false) {
        objectBD_Add(UnElemento.ID, UnElemento.IDSUB, UnElemento.LangTextDefault);
        //arrayManuelElements[countaerray] = UnElemento;
    }
}
function ChangeSLangue(idLenguaje) {    
    lenguaje = idLenguaje;
    DeleteBase(); //borrar la BD de indexDB  
}

function reloadpage() {
    SaveLangue();
}

function SaveLangue() {
    //guardar el codigo de lenguaje
   
    var valor = lenguaje.value;
    var dataArray = {
        'IDLANGUAGE': valor
    };
    
    direc = SysCfg.App.Properties.xRaiz + 'Componet/Traslate/WcfTraslate.svc/SETIDLANGUAGE';
    //enviamos el arreglo a la webService  
    $.ajax({
        type: "POST",
        async: false,
        url: direc,
        data: JSON.stringify(dataArray),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var URLactual = window.location;
            var URLactualh = $("#mainFr")[0].src;
            var URLactualh2 = $("#mainFr").attr('src');
            window.location.reload();
        },
        error: function (error) {
            SysCfg.Log.Methods.WriteLog("Traslate.js SaveLangue() Componet/Traslate/WcfTraslate.svc/SETIDLANGUAGE  \n Location: Traslate / Send manual List:" + error.txtDescripcion);
        }
    });
}


//https://rolandocaldas.com/html5/indexeddb-tu-base-de-datos-local-en-html5
//https://www.tutorialspoint.com/html5/html5_indexeddb.htm
//https://www.w3.org/TR/IndexedDB/

