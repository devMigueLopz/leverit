﻿/*<summary>
Midifico fecha   
hans     24-04-2017
Funcion: Genera un elemento html de tipo texto header con escala de letra 6
ejemplos:
Uh6(Elementhtm, "Id_del_elemento", "texto_del_elemento");
</summary>
<param name="Object">Elemento html al cual se le insertara el elemento creado por esta funcion</param>
<param name="id">Id de elemento creado</param>
<param name="text">texto del elemento</param>
<returns>retorna un elemento de tipo texto header</returns>*/
function Uh6(Object, id, text) {
    var Res = undefined;
    var h6 = document.createElement('h6');
    var _text = document.createTextNode(text);
    h6.appendChild(_text);
    Res = h6;
    $(Object).append(Res);
    return Res;
}

/*<summary>
Midifico fecha   
hans     24-04-2017
Funcion: Genera un elemento html de tipo texto header con escala de letra 5
ejemplos:
Uh5(Elementhtm, "Id_del_elemento", "texto_del_elemento");
</summary>
<param name="Object">Elemento html al cual se le insertara el elemento creado por esta funcion</param>
<param name="id">Id de elemento creado</param>
<param name="text">texto del elemento</param>
<returns>retorna un elemento de tipo texto header</returns>*/
function Uh5(Object, id, text) {
    var Res = undefined;
    var h5 = document.createElement('h5');
    var _text = document.createTextNode(text);
    h5.appendChild(_text);
    Res = h5;
    $(Object).append(Res);
    return Res;
}

/*<summary>
Midifico fecha   
hans     24-04-2017
Funcion: Genera un elemento html de tipo texto header con escala de letra 4
ejemplos:
Uh4(Elementhtm, "Id_del_elemento", "texto_del_elemento");
</summary>
<param name="Object">Elemento html al cual se le insertara el elemento creado por esta funcion</param>
<param name="id">Id de elemento creado</param>
<param name="text">texto del elemento</param>
<returns>retorna un elemento de tipo texto header</returns>*/
function Uh4(Object, id, text) {
    var Res = undefined;
    var h4 = document.createElement('h4');
    var _text = document.createTextNode(text);
    h4.appendChild(_text);
    Res = h4;
    $(Object).append(Res);
    return Res;
}

/*<summary>
Midifico fecha   
hans     24-04-2017
Funcion: Genera un elemento html de tipo texto header con escala de letra 3
ejemplos:
Uh3(Elementhtm, "Id_del_elemento", "texto_del_elemento");
</summary>
<param name="Object">Elemento html al cual se le insertara el elemento creado por esta funcion</param>
<param name="id">Id de elemento creado</param>
<param name="text">texto del elemento</param>
<returns>retorna un elemento de tipo texto header</returns>*/
function Uh3(Object, id, text) {
    var Res = undefined;
    var h3 = document.createElement('h3');
    var _text = document.createTextNode(text);
    h3.appendChild(_text);
    Res = h3;
    $(Object).append(Res);
    return Res;
}

/*<summary>
Midifico fecha   
hans     24-04-2017
Funcion: Genera un elemento html de tipo texto header con escala de letra 3
ejemplos:
Uh2(Elementhtm, "Id_del_elemento", "texto_del_elemento");
</summary>
<param name="Object">Elemento html al cual se le insertara el elemento creado por esta funcion</param>
<param name="id">Id de elemento creado</param>
<param name="text">texto del elemento</param>
<returns>retorna un elemento de tipo texto header</returns>*/
function Uh2(Object, id, text) {
    var Res = undefined;
    var h2 = document.createElement('h2');
    var _text = document.createTextNode(text);
    h2.appendChild(_text);
    Res = h2;
    $(Object).append(Res);
    return Res;
}

/*<summary>
Midifico fecha   
hans     24-04-2017
Funcion: Genera un elemento html de tipo texto header con escala de letra 1
ejemplos:
Uh1(Elementhtm, "Id_del_elemento", "texto_del_elemento");
</summary>
<param name="Object">Elemento html al cual se le insertara el elemento creado por esta funcion</param>
<param name="id">Id de elemento creado</param>
<param name="text">texto del elemento</param>
<returns>retorna un elemento de tipo texto header</returns>*/
function Uh1(Object, id, text) {
    var Res = undefined;
    var h1 = document.createElement('h1');
    var _text = document.createTextNode(text);
    h1.appendChild(_text);
    Res = h1;
    $(Object).append(Res);
    return Res;
}