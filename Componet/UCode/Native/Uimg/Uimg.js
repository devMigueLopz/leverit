﻿/*<summary>
Midifico fecha   
hans     24-04-2017
Funcion: Genera un elemento html de tipo imagen
ejemplos:
Uimg(Elementhtm, "Id_del_elemento", "ruta/imagen.jpg", "hint de la imagen");
</summary>
<param name="Object">Elemento html al cual se le insertara el elemento creado por esta funcion</param>
<param name="id">Id de elemento creado</param>
<param name="Src">Ruta de la imagen en el servidor</param>
<param name="alt">Hint o tooltip de la imagen</param>
<returns>retorna un elemento img html</returns>*/
function Uimg(Object, id, Src, alt) {
    var Res = undefined;
    var FullSrc = SysCfg.App.Properties.xRaiz + Src;
    var img = document.createElement('img');
    img.id = id
    img.src = FullSrc;
    img.alt = alt;
    Res = img;
    $(Object).append(Res);
    return Res;
}