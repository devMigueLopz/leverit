﻿/*<summary>
Midifico fecha   
Carlos     24-04-2017
Miguel     25-09-2019
Funcion: Crea un elemento de tipo ul de html
ejemplos:
Uul(Elementhtml, "Id ul");
</summary>
<param name="Object">Elemento html al cual se le insertara la ul creada por esta funcion</param>
<param name="id">Id que va a ser asignado a la ul</param>
<returns>Retorna un elemento de tipo ul</returns>*/
function Uul(Object, id) {
    var Res = undefined;
    var ul = document.createElement('ul');
    ul.id = id;
    Res = ul;
    $(Object).append(Res);
    return Res;
}