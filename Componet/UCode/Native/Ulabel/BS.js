﻿/*<summary>
Midifico fecha   
Carlos     24-04-2017
Funcion: Aplica estilo de button de bootstrap a elementos html
ejemplos:
BootStrapCreateButton(ElementHTML);
</summary>
<param name="Object">Elemento html al cual se le aplicara el estilo de button de bootstrap</param>
<returns></returns>*/
function BootStrapCreateLabel(Object) {
    Object.classList.add("control-label");
}