﻿/*<summary>
Midifico fecha   
hans     24-04-2017
Funcion: Genera un elemento html de tipo header de tabla 
ejemplos:
Uh2(Elementhtm, "Id_del_elemento");
</summary>
<param name="Object">Elemento html al cual se le insertara el elemento creado por esta funcion</param>
<param name="id">Id de elemento creado</param>
<returns>retorna un elemento de tipo header de tabla </returns>*/
function Uthead(Object, id) {
    var Res = undefined;
    var thead = document.createElement('thead');
    thead.id = id;
    Res = thead;
    $(Object).append(Res);
    return Res;
}