﻿/*<summary>
Midifico fecha   
hans     24-04-2017
Funcion: Genera un elemento html de tipo th celda de header de tabla 
ejemplos:
Uh2(Elementhtm, "Id_del_elemento");
</summary>
<param name="Object">Elemento html al cual se le insertara el elemento creado por esta funcion</param>
<param name="id">Id de elemento creado</param>
<returns>retorna un elemento de tipo th celda de header de tabla</returns>*/
function Uth(Object, id) {
    var Res = undefined;
    var th = document.createElement('th');
    th.id = id;
    Res = th;
    $(Object).append(Res);
    return Res;
}