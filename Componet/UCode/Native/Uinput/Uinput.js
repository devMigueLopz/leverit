﻿/*<summary>
Midifico fecha   
Carlos     24-04-2017
Funcion: variable que contiene los tipos de input html. 
ejemplos: 
TImputtype.text;
</summary>
*/
var TImputtype = {
    text: { value: 0, name: "text" },
    hidden: { value: 1, name: "hidden" },
    button: { value: 2, name: "button" },
    checkbox: { value: 3, name: "checkbox" },
    number: { value: 4, name: "number" },
    password: { value: 5, name: "password" },
    radio: { value: 6, name: "radio" },
    file: { value: 7, name: "file" }
};

/*<summary>
Midifico fecha   
Carlos     24-04-2017
Funcion: Crea un elemento html de tipo Input
ejemplos:
Uinput(Elementhtm, Imputtype.text, "Id del elemento", "descripcion", true);
</summary>
<param name="Object">Elemento html al cual se le insertara el elemento creado por esta funcion</param>
<param name="Imputtype">tipo de input que se va a crear</param>
<param name="id">Id de elemento creado</param>
<param name="value">el valor por defecto que va a contener el input</param>
<param name="onclick">valor bool que indica si se crea el evento click para el input generado</param>
<returns>Retorna un elemento input html</returns>*/
function Uinput(Object, Imputtype, id, value, onclick) {
    var Res = undefined;
    var input = document.createElement('input');
    input.id = id;
    input.type = Imputtype.name;
    if (onclick) input.onclick = id + '_onclick';
    input.value = value;
    Res = input;
    $(Object).append(Res);
    return Res;
}