﻿/*<summary>
Midifico fecha
Miguel     25-09-2019
Funcion: Genera un elemento html de tipo time
ejemplos:
Utime(Elementhtm, "Id Utime", "Text Utime");
</summary>
<param name="Object">Elemento html al cual se le insertara el elemento Utime creada por esta funcion</param>
<param name="id">Id que va a ser asignado a Utime</param>
<param name="text">texto del elemento para el Utime</param>
<returns>retorna un elemento de tipo Utime</returns>*/
function Utime(Object, id, text) {
    var Res = undefined;
    var time = document.createElement('time');
    var _text = document.createTextNode(text);
    time.id = id;
    time.appendChild(_text);
    Res = time;
    $(Object).append(Res);
    return Res;
}