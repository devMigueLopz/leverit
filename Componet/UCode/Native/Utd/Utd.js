﻿/*<summary>
Midifico fecha   
hans     24-04-2017
Funcion: Genera un elemento html de tipo td celda de body de tabla 
ejemplos:
Uh2(Elementhtm, "Id_del_elemento");
</summary>
<param name="Object">Elemento html al cual se le insertara el elemento creado por esta funcion</param>
<param name="id">Id de elemento creado</param>
<returns>retorna un elemento de tipo td celda de body de tabla</returns>*/
function Utd(Object, id) {
    var Res = undefined;
    var td = document.createElement('td');
    td.id = id;
    Res = td;
    $(Object).append(Res);
    return Res;
}