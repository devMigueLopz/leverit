﻿/*<summary>
Midifico fecha   
Miguel     25-09-2019
Funcion: Crea un elemento de tipo ol
ejemplos:
Uol(Elementhtml, "Id Uol");
</summary>
<param name="Object">Elemento html al cual se le insertara elemento Uol creada por esta funcion</param>
<param name="id">Id que va a ser asignado a Uol</param>
<returns>Retorna un elemento de tipo Uol</returns>*/
function Uol(Object, id) { 
    var Res = undefined;
    var ul = document.createElement('ol');
    Res = ul;
    $(Object).append(Res);
    return Res;
}