﻿function Uabbr(Object, id, text, title) {
    var Res = undefined;
    var _element = document.createElement('abbr');
    var _text = document.createTextNode(text);
    _element.appendChild(_text);
    _element.title = title;
    _element.id = id;
    Res = _element;
    $(Object).append(Res);
    return Res;
}

