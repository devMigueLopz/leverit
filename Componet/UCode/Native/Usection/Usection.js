﻿/*<summary>
Midifico fecha   
Miguel     25-09-2019
Funcion: Crea un elemento de tipo section 
ejemplos:
Usection(Elementhtml, "Id Usection");
</summary>
<param name="Object">Elemento html al cual se le insertara el elemento Usection creada por esta funcion</param>
<param name="id">Id que va a ser asignado a Usection</param>
<returns>Retorna un elemento de tipo Usection</returns>*/
function Usection(Object, id) {
    var Res = undefined;
    var section = document.createElement('section');
    section.id = id;
    Res = section
    $(Object).append(Res)
    return Res;
}