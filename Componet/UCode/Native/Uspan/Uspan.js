﻿/*<summary>
Midifico fecha   
hans     24-04-2017
Miguel     25-09-2019
Funcion: Genera un elemento html de tipo span
ejemplos:
Uspan(Elementhtm, "Id Uspan", "Text Uspan");
</summary>
<param name="Object">Elemento html al cual se le insertara el elemento Uspan creada por esta funcion</param>
<param name="id">Id que va a ser asignado a Uspan</param>
<param name="text">texto del elemento para el Uspan</param>
<returns>retorna un elemento de tipo Uspan</returns>*/
function Uspan(Object, id, text) { 
    var Res = undefined;
    var span = document.createElement('span');
    span.id = id;
    var _text = document.createTextNode(text);
    span.appendChild(_text);
    Res = span;
    $(Object).append(Res);
    return Res;
}