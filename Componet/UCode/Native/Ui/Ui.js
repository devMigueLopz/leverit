﻿/*<summary>
Midifico fecha   
Miguel     25-09-2019
Funcion: Crea un elemento html del tipo i de html
ejemplos:
Ui(Elementhtm, "Id Ui");
</summary>
<param name="Object">Elemento html al cual se le insertara el elemento Ui creada por esta funcion</param>
<param name="id">Id que va a ser asignado a  Ui</param>
<returns>Retorna un elemento de tipo Ui</returns>*/
function Ui(Object, id) {
    var Res = undefined;
    var i = document.createElement('i');
    i.id = id;
    Res = i;
    $(Object).append(Res);
    return Res;
}