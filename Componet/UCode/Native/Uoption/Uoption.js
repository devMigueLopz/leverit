﻿/*<summary>
Midifico fecha   
hans     24-04-2017
Funcion: Genera un elemento html de tipo texto header con escala de letra 6
ejemplos:
Uh2(Elementhtm, "Id_del_elemento", "texto_del_elemento");
</summary>
<param name="Object">Elemento html al cual se le insertara el elemento creado por esta funcion</param>
<param name="id">Id de elemento creado</param>
<param name="text">texto del elemento</param>
<returns>retorna un elemento de tipo texto header</returns>*/
function Uoption(Object, id, value, text) {
    var Res = undefined;
    var option = document.createElement('option');
    option.id = id;
    option.value = value;
    option.innerHTML = text;
    Res = option;
    $(Object).append(Res);
    return Res;
}