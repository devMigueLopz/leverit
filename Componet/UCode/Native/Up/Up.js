﻿/*<summary>
Midifico fecha   
hans     24-04-2017
Funcion: Genera un elemento html de tipo parrafo
ejemplos:
Up(Elementhtm, "Id_del_elemento", "texto_del_elemento");
</summary>
<param name="Object">Elemento html al cual se le insertara el elemento creado por esta funcion</param>
<param name="id">Id de elemento creado</param>
<param name="text">texto del elemento</param>
<returns>retorna un elemento de tipo parrafo</returns>*/
function Up(Object, id, text) { //parrafo
    var Res = undefined;
    var p = document.createElement('p');
    var _text = document.createTextNode(text);
    p.appendChild(_text);
    Res = p;
    $(Object).append(Res);
    return Res;
}