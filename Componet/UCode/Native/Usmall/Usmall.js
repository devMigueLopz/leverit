﻿/*<summary>
Midifico fecha   
Miguel     25-09-2019
Funcion: Crea un elemento html del tipo small
ejemplos:
Usmall(Elementhtm, "Id Usmall");
</summary>
<param name="Object">Elemento html al cual se le insertara el elemento Usmall creada por esta funcion</param>
<param name="id">Id que va a ser asignado a Usmall</param>
<returns>Retorna un elemento de tipo Usmall</returns>*/
function Usmall(Object, id, text) {
    var Res = undefined;
    var small = document.createElement('small');
    small.id = id;
    var _text = document.createTextNode(text);
    small.appendChild(_text);
    Res = small;
    $(Object).append(Res);
    return Res;
}