﻿/*<summary>
Midifico fecha   
hans     24-04-2017
Funcion: Genera un elemento html de tipo canvas
ejemplos:
Uspan(Elementhtm, "Id_del_elemento");
</summary>
<param name="Object">Elemento html al cual se le insertara el elemento creado por esta funcion</param>
<param name="id">Id de elemento creado</param>
*/
function UCanvas(Object, id) {  //una linea 
    var Res = document.createElement('canvas');
    $(Object).append(Res);
    Res.id = id;
    return Res;
}