﻿/*<summary>
Midifico fecha   
Carlos     24-04-2017
Miguel     25-09-2019
Funcion: Crea un elemento de tipo li de html
ejemplos:
Uli(Elementhtml, "Id Uli");
</summary>
<param name="Object">Elemento html al cual se le insertara el elemento Uli creada por esta funcion</param>
<param name="id">Id que va a ser asignado a Uli</param>
<returns>Retorna un elemento de tipo Uli</returns>*/
function Uli(Object, id) {
    var Res = undefined;
    var li = document.createElement('li');
    li.id = id;
    Res = li;
    $(Object).append(Res);
    return Res;
}