﻿/*<summary>
Midifico fecha   
hans     24-04-2017
Funcion: Genera un elemento html de tipo tabla
ejemplos:
Uh2(Elementhtm, "Id_del_elemento");
</summary>
<param name="Object">Elemento html al cual se le insertara el elemento creado por esta funcion</param>
<param name="id">Id de elemento creado</param>
<returns>retorna un elemento de tipo tabla</returns>*/
function Utable(Object, id) {
    var Res = undefined;
    var table = document.createElement('table');
    table.id = id;
    Res = table;
    $(Object).append(Res);
    return Res;
}