﻿/*<summary>
Midifico fecha   
Carlos     24-04-2017
Funcion: Aplica estilo de table de bootstrap a elementos html
ejemplos:
BootStrapCreateTable(ElementHTML);
</summary>
<param name="Object">Elemento html al cual se le aplicara el estilo de table de bootstrap</param>
<returns></returns>*/
function BootStrapCreateTable(Object) {
    Object.classList.add("table");
}

function BootStrapCreateTableResponsive(Object) {
    Object.classList.add("table-responsive");
}