﻿/*<summary>
Midifico fecha   
hans     24-04-2017
Funcion: Genera un elemento html de tipo body de tabla 
ejemplos:
Uh2(Elementhtm, "Id_del_elemento");
</summary>
<param name="Object">Elemento html al cual se le insertara el elemento creado por esta funcion</param>
<param name="id">Id de elemento creado</param>
<returns>retorna un elemento de tipo body de tabla </returns>*/
function Utbody(Object, id) {
    var Res = undefined;
    var tbody = document.createElement('tbody');
    tbody.id = id;
    Res = tbody;
    $(Object).append(Res);
    return Res;
}