﻿/*<summary>
Midifico fecha   
hans     24-04-2017
Funcion: Genera un elemento html de tipo enlace
ejemplos:
Ua(Elementhtm, "Id_del_elemento", "texto_del_elemento");
</summary>
<param name="Object">Elemento html al cual se le insertara el elemento creado por esta funcion</param>
<param name="id">Id de elemento creado</param>
<param name="text">texto del elemento</param>
<returns>retorna un elemento de tipo enlace</returns>*/
function Ua(Object, id, text) { //link
    var Res = undefined;
    var a = document.createElement('a');
    var _text = document.createTextNode(text);
    a.id = id;
    a.appendChild(_text);
    Res = a;
    $(Object).append(Res);
    return Res;
}