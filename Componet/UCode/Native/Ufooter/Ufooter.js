﻿/*<summary>
Midifico fecha   
Carlos     24-04-2017
Miguel     25-09-2019
Funcion: Crea un elemento html del tipo footer de html
ejemplos:
Ufooter(Elementhtm, "Id Ufooter");
</summary>
<param name="Object">Elemento html al cual se le insertara el elemento Ufooter creada por esta funcion</param>
<param name="id">Id que va a ser asignado a Ufooter</param>
<returns>Retorna un elemento de tipo Ufooter</returns>*/
function Ufooter(Object, id) {
    var Res = undefined;
    var Tag = document.createElement("footer");
    Tag.id = id;
    Res = Tag;
    $(Object).append(Res);
    return Res;
}
