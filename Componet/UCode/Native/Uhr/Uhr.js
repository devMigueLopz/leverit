﻿/*<summary>
Midifico fecha   
Miguel     25-09-2019
Funcion: Crea un elemento html del tipo hr de html
ejemplos:
Uhr(Elementhtm, "Id Uhr");
</summary>
<param name="Object">Elemento html al cual se le insertara el elemento Uhr creada por esta funcion</param>
<param name="id">Id que va a ser asignado a Uhr</param>
<returns>Retorna un elemento de tipo Uhr</returns>*/
function Uhr(Object, id) { 
    var Res = undefined;
    var hr = document.createElement('hr');
    hr.id = id;
    Res = hr;
    $(Object).append(Res);
    return Res;
}