﻿/*<summary>
Midifico fecha   
Carlos     24-04-2017
Funcion: Crea un elemento html del tipo TTagType
ejemplos:
UAddTag(Elementhtm, "Id del elemento", TTagType.article);
</summary>
<param name="Object">Elemento html al cual se le insertara el elemento creado por esta funcion</param>
<param name="id">Id de elemento creado</param>
<param name="TagType">Tipo de elemento que se va a crear</param>
<returns>Retorna un elemento html del tipo asignado</returns>*/
function Umain(Object, id) {
    var Tag = document.createElement("main");
    Tag.id = id;
    $(Object).append(Tag);
    return Tag;
}