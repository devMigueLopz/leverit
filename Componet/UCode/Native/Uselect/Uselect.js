﻿
/*<summary>
Midifico fecha   
hans     24-04-2017
Funcion: Genera un elemento html de tipo combobox
ejemplos:
USelect(Elementhtm, "Id_del_elemento");
</summary>
<param name="Object">Elemento html al cual se le insertara el elemento creado por esta funcion</param>
<param name="id">Id de elemento creado</param>
<returns>retorna un elemento de tipo combobox</returns>*/
function Uselect(Object, id) {
    var Res = undefined;
    var select = document.createElement('select');
    select.id = id;
    Res = select;
    $(Object).append(Res);
    return Res;
}