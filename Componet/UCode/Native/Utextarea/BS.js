﻿/*<summary>
Midifico fecha   
Carlos     24-04-2017
Funcion: Aplica estilo de row bootstrap a elementos html
ejemplos:
BootStrapCreateRows(ArryaHtml[]);
</summary>
<param name="Object">Arrray de elementos html a los cuales se le aplicara el estilo de row de bootstrap</param>
<returns></returns>*/
function BootStrapCreateTextArea(Object) {
    Object.classList.add("form-control");
}