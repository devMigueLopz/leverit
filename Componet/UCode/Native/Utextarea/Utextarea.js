﻿/*<summary>
Midifico fecha   
Carlos     24-04-2017
Funcion: Crea un elemento de tipo Textarea de html
ejemplos:
Utextarea(Elementhtml, "Id textarea", "texto que va dentro", 10, 50);
</summary>
<param name="Object">Elemento html al cual se le insertara el textarea creado por esta funcion</param>
<param name="id">Id que va a ser asignado al textarea</param>
<param name="text">Texto insertado dentro del elemento textarea</param>
<param name="rows">Numero de filas (alto) del textarea</param>
<param name="cols">Numero de columnas (ancho) del textarea</param>
<returns>Retorna un elemento de tipo Textarea</returns>*/
function Utextarea(Object, id, text, rows, cols) { //memo x y 
    var Res = undefined;
    var textarea = document.createElement('textarea');
    var _text = document.createTextNode(text);
    textarea.appendChild(_text);
    textarea.id = id;
    textarea.cols = cols;
    textarea.rows = rows;
    Res = textarea;
    $(Object).append(textarea);
    return Res;
}