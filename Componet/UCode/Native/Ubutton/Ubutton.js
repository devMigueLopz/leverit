﻿/*<summary>
Midifico fecha   
Carlos     24-04-2017
Funcion: variable que contiene los tipos de de buttons html. 
ejemplos: 
TButtontype.submit;
</summary>
*/
var TButtontype = {
    submit: {
        value: 0,
        name: "submit"
    }
};

/*<summary>
Midifico fecha   
Carlos     24-04-2017
Funcion: Crea un elemento html de tipo button 
ejemplos:
Ubutton(Elementhtm, TButtontype.submit, "Id del elemento", "descripcion", true);
</summary>
<param name="Object">Elemento html al cual se le insertara el elemento creado por esta funcion</param>
<param name="Imputtype">tipo de button que se va a crear</param>
<param name="id">Id de elemento creado</param>
<param name="value">descripcion del button</param>
<param name="onclick">valor bool que indica si se crea el evento click para el button generado</param>
<returns>Retorna un elemento input html</returns>*/
function Ubutton(Object, Buttontype, id, value, onclick) {
    var Res = undefined;
    var button = document.createElement('button');
    button.id = id;
    //button.type = Buttontype.name;
    if (onclick) button.onclick = id + '_onclick';
   // var _text = document.createTextNode(value);
    var _text = document.createElement("div");
    var _img = document.createElement("div");
    button.appendChild(_text);
    button.appendChild(_img);
    Res = button;
    $(Object).append(Res);
    return Res;
}


/*
function Tbutton(Object, Buttontype, id, value, onclick,TMode) {
   
    var Res = Ubutton(Object, Buttontype, id, value, onclick);
    var reisize

    return Res;
}*/