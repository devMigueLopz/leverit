﻿/*<summary>
Midifico fecha   
Carlos     24-04-2017
Funcion: Aplica estilo de row bootstrap a elementos html
ejemplos:
BootStrapCreateRows(ArryaHtml[]);
</summary>
<param name="Object">Arrray de elementos html a los cuales se le aplicara el estilo de row de bootstrap</param>
<returns></returns>*/
function BootStrapCreateRows(Object) {
    for (var i = 0; i < Object.length; i++) {
        Object[i].This.classList.add("row");
    }
}


/*<summary>
Midifico fecha   
Carlos     24-04-2017
Funcion: Aplica estilo de row bootstrap a elemento html
ejemplos:
BootStrapCreateRow(ElementHtml);
</summary>
<param name="Object">Elemento html al cual se le aplicara el estilo de row de bootstrap</param>
<returns></returns>*/
function BootStrapCreateRow(Object) {

    Object.classList.add("row");
}

/*<summary>
Midifico fecha   
Carlos     24-04-2017
Funcion: Aplica estilo de column bootstrap a elementos html
ejemplos:
BootStrapCreateColumns(ArryaHtml[2], MatrizDefine[[2,10], [2,10], [2,10], [2,10], [2,10]]);
</summary>
<param name="Object">Arrray de elementos html a los cuales se le aplicara el estilo de column de bootstrap</param>
<param name="DefineRows">Matriz que define las distintos valores de numero de columnas que se aplicara para las distintas resoluciones</param>
<returns></returns>*/
function BootStrapCreateColumns(Object, DefineRows) {
    for (var rowout = 0; rowout < Object.length; rowout++) {
        for (var e = 0; e < DefineRows.length; e++) {
            for (var rowdef = 0; rowdef < DefineRows[e].length; rowdef++) {
                if (rowout == rowdef) {
                    switch (e) {
                        case 0:
                            Object[rowout].This.classList.add("col-xs-" + DefineRows[e][rowdef]);
                            break;
                        case 1:
                            Object[rowout].This.classList.add("col-sm-" + DefineRows[e][rowdef]);
                            break;
                        case 2:
                            Object[rowout].This.classList.add("col-md-" + DefineRows[e][rowdef]);
                            break;
                        case 3:
                            Object[rowout].This.classList.add("col-lg-" + DefineRows[e][rowdef]);
                            break;
                        case 4:
                            Object[rowout].This.classList.add("col-xl-" + DefineRows[e][rowdef]);
                            break;
                    }

                }
            }
        }
    }



}

/*<summary>
Midifico fecha   
Carlos     24-04-2017
Funcion: Aplica estilo de column bootstrap a elemento html
ejemplos:
BootStrapCreateColumn(ElementHtml);
</summary>
<param name="Object">Elemento html al cual se le aplicara el estilo de column de bootstrap</param>
<returns></returns>*/
function BootStrapCreateColumn(Object, DefineRow) {
    for (var rowdef = 0; rowdef < DefineRow.length; rowdef++) {

        switch (rowdef) {
            case 0:
                Object.classList.add("col-xs-" + DefineRow[rowdef]);
                break;
            case 1:
                Object.classList.add("col-sm-" + DefineRow[rowdef]);
                break;
            case 2:
                Object.classList.add("col-md-" + DefineRow[rowdef]);
                break;
            case 3:
                Object.classList.add("col-lg-" + DefineRow[rowdef]);
                break;
            case 4:
                Object.classList.add("col-xl-" + DefineRow[rowdef]);
                break;
        }


    }
}


/*<summary>
Midifico fecha   
Carlos     24-04-2017
Funcion: variable que contiene los tipos de conteiner de bootstrap
ejemplos:
TBootStrapContainerType.none;
</summary>
*/
var TBootStrapContainerType = {
    none: { value: 0, name: "" },
    fluid: { value: 0, name: "-fluid" },
};


/*<summary>
Midifico fecha   
Carlos     24-04-2017
Funcion: Aplica estilo de container bootstrap a elemento html
ejemplos:
BootStrapContainer(Elementhtm, BootStrapContainerType.fluid);
</summary>
<param name="Object">Elemento html al cual se le aplicara el estilo de container de bootstrap</param>
<param name="BootStrapContainerType">Tipo de container que se aplica al elemento html</param>
<returns></returns>*/
function BootStrapContainer(Object, BootStrapContainerType) {
    Object.classList.add("container" + BootStrapContainerType.name);
}