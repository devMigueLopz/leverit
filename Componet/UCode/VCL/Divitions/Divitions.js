﻿/*<summary>
Midifico fecha   
Carlos     24-04-2017
Funcion: Representa una clase de tipo div
ejemplos:
var Div = new TDiv();
</summary>
<returns>instancia de la clase TDiv</returns>*/
function TDiv() {
    this.ClassName;
    this.SubDiv;
}

/*<summary>
Midifico fecha   
Carlos     24-04-2017
Funcion: Crea un elemento de la clase TUdiv
ejemplos:
UdivRoot(Elementhtm, "Id base", ArrayDef[]);
</summary>
<param name="Object">Elemento html al cual se le insertara los divs creados por esta funcion</param>
<param name="inName">Id base que se utiliza para generar los Ids de los divs</param>
<param name="Div_Define">Matriz con la estructura de los divs que se van a crear</param>
<returns>Retorna una clase de tipo TUdiv</returns>*/
function VclDivitionsRoot(Object, inName, Div_Define) { //contenedor
    var _Udiv = new TUdiv();
    _Udiv.This = Udiv(Object, inName); //document.createElement('div');
    //_Udiv.This.id = inName;
    //$(Object).append(_Udiv.This);
    _Udiv.Child = VclDivitions(_Udiv.This, inName, Div_Define);
    return _Udiv;
}

/*<summary>
Midifico fecha   
Carlos     24-04-2017
Funcion: Representa una clase que contiene un div html (This) y un array de elementos div html (Child)
ejemplos:
var Udiv = new TUdiv();
</summary>
<returns></returns>*/
function TUdiv() {
    this.This;
    this.Child;
}


/*<summary>
Midifico fecha   
Carlos     24-04-2017
Funcion: Crea un elemento de la clase TUdiv
ejemplos:
Udiv(Elementhtm, "Id base", ArrayDef[]);
</summary>
<param name="Object">Elemento html al cual se le insertara los divs creados por esta funcion</param>
<param name="inName">Id base que se utiliza para generar los Ids de los divs</param>
<param name="Div_Define">Matriz con la estructura de los divs que se van a crear</param>
<returns>Retorna una clase de tipo TUdiv</returns>*/
function VclDivitions(Object, inName, Div_Define) {
    var Res = null;

    Res = Array(Div_Define.length);
    for (var i = 0; i < Div_Define.length; i++) {
        Res[i] = new TUdiv();
        var id = inName + "_" + i;

        var div = Udiv(Object, id);

        Res[i].This = div;
        if (Div_Define[i] != undefined) {
            Res[i].Child = VclDivitions(Res[i].This, id, Div_Define[i])
        }
    }
    return Res;
}