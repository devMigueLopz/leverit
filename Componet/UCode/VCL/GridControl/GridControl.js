﻿var TGrid = function (inObjectHTML, ID, inCallBack) {

    this.importStyle = function (nombre, onSuccess, onError) { //LFVA: Carga estilo
        var style = document.createElement("link");
        style.rel = "stylesheet";
        style.type = "text/css";
        style.href = nombre;
        var s = document.head.appendChild(style);
        s.onload = onSuccess;
        s.onerror = onError;
    }
    this.importStyle(SysCfg.App.Properties.xRaiz + "Css/Component/UCode/VCL/GridControlCF/GridToolTip.css", function () {
    }, function (e) { });


    this.OnRowClick = null;
    this._Columns = new Array();
    this._Rows = new Array();
    this._FocusedRowHandle = null;
    this._DataSource = null;
    this._IndexRow = -1;
    this.ResponsiveCont = Udiv(inObjectHTML, "");
    this.ResponsiveCont.classList.add("table-responsive");

    this._EnabledResizeColumn = true;
    Object.defineProperty(this, 'EnabledResizeColumn', {
        get: function () {
            return this._EnabledResizeColumn;
        },
        set: function (inValue) {
            this._EnabledResizeColumn = inValue;
            if (this._Columns.length > 0) {
                for (var i = 0; i < this._Columns.length; i++) {
                    this._Columns[i].Caption = this._Columns[i].Caption;
                }
            }
            if (this._Rows.length > 0) {
                for (var x = 0; x < this._Rows.length; x++) {
                    for (var i = 0; i < this._Columns.length; i++) {
                        this._Rows[x].Cells[i].This.removeAttribute("style");
                    }
                }
            }
        }
    });

    this.This = Utable(this.ResponsiveCont, ID);
    this.Header = Uthead(this.This, ID);
    this.RowHeader = Utr(this.Header, "");
    this.Body = Utbody(this.This, ID);

    //stylos para darle la vista a la grilla 
    this.This.classList.add("table");
    this.This.classList.add("table-bordered");
    this.This.classList.add("table-hover");
    this.This.style.fontSize = "12px";
    this.Body.style.cursor = "pointer";
    this._AutoTranslate = false;
    //fin de los estilos 

    Object.defineProperty(this, 'AutoTranslate', {
        get: function () {
            return this._AutoTranslate;
        },
        set: function (inValue) {
            this._AutoTranslate = inValue;
            if (inValue)
                this.TranslateColumns();
        }
    });

    Object.defineProperty(this, 'DataSource', {
        get: function () {
            return this._DataSource;
        },
        set: function (inValue) {
            this._DataSource = inValue;
            this.LoadDataSource();
        }
    });

    Object.defineProperty(this, 'Columns', {
        get: function () {
            return this._Columns;
        },
        set: function (inValue) {
            this._Columns = inValue;
        }
    });

    Object.defineProperty(this, 'Rows', {
        get: function () {
            return this._Rows;
        },
        set: function (inValue) {
            this._Rows = inValue;
        }
    });

    Object.defineProperty(this, 'FocusedRowHandle', {
        get: function () {
            return this._FocusedRowHandle;
        },
        set: function (inValue) {
            this._FocusedRowHandle = inValue;
        }
    });

    Object.defineProperty(this, 'IndexRow', {
        get: function () {
            return this._IndexRow;
        },
        set: function (inValue) {
            this._IndexRow = inValue;
            if (inValue >= 0) {
                if (this._Rows.length > 0) {

                    if (this._FocusedRowHandle != null)
                        this._FocusedRowHandle.This.style.backgroundColor = "";
                    this._FocusedRowHandle = this._Rows[inValue];
                    this._FocusedRowHandle.This.style.backgroundColor = "lightyellow";

                }
            }
        }
    });

    this.TranslateColumns = function () {
        if (this._Columns.length > 0) {
            for (var i = 0; i < this._Columns.length; i++) {
                var Column = this._Columns[i];
                if (this._AutoTranslate)
                    Column.Caption = UsrCfg.Traslate.GetLangTextdb(Column.Caption, Column.Caption);
            }
        }
    }

    this.AddColumn = function (inColumn) {
        inColumn.GridParent = this;
        this._Columns.push(inColumn);
        $(this.RowHeader).append(inColumn.This)
    }

    this.AddRow = function (inRow) {
        if (this._Rows.length == 0)
            this.FocusedRowHandle = inRow;
        inRow.Index = this._Rows.length;
        this._Rows.push(inRow);
        inRow.GridParent = this;
        for (var i = 0; i < this._Columns.length; i++) {
            inRow.Cells[i].Column = this._Columns[i];
            inRow.Cells[i].InitCell();
        }
        $(this.Body).append(inRow.This)
    }

    this.RemoveRow = function (Index) {
        this._Rows[Index].This.parentNode.removeChild(this._Rows[Index].This);
        this._Rows.splice(Index, 1);
    }

    this.RemoveFocusedRowHandle = function () {
        this._FocusedRowHandle.This.parentNode.removeChild(this._FocusedRowHandle.This);
        this._Rows.splice(this._FocusedRowHandle.Index, 1);
    }

    this.ClearAllColumns = function () {
        this._Columns.splice(0, this._Columns.length);
        $(this.RowHeader).html("");
    }

    this.ClearAllRows = function () {
        this._Rows.splice(0, this._Rows.length);
        this._FocusedRowHandle = null;
        $(this.Body).html("");
    }

    this.ClearAll = function () {
        this.ClearAllRows();
        this.ClearAllColumns();
    }

    this.LoadDataSource = function () {
        this.ClearAll();
        for (var i = 0; i < this._DataSource.FieldDefs.length; i++) {
            var Col1 = new TColumn();
            Col1.Name = this._DataSource.FieldDefs[i].FieldName;
            if (this._AutoTranslate)
                Col1.Caption = UsrCfg.Traslate.GetLangTextdb(Col1.Name, Col1.Name);
            else
                Col1.Caption = this._DataSource.FieldDefs[i].FieldName;
            Col1.DataType = this._DataSource.FieldDefs[i].DataType;
            Col1.Index = i;
            Col1.GridParent = this;
            this.AddColumn(Col1);
        }

        if (this._DataSource.RecordCount > 0) {
            this._DataSource.First();

            var conta = 0;
            while (!(this._DataSource.Eof)) {
                var NewRow = new TRow();

                for (var i = 0; i < this._DataSource.FieldDefs.length; i++) {
                    var CellTemp = new TCell();

                    switch (this._DataSource.FieldDefs[i].DataType) {
                        case SysCfg.DB.Properties.TDataType.String:
                            CellTemp.Value = this._DataSource.RecordSet.FieldName(this._DataSource.FieldDefs[i].FieldName).asString();
                            CellTemp.IndexColumn = i;
                            CellTemp.IndexRow = conta;
                            CellTemp.Column = this.Columns[i];
                            NewRow.AddCell(CellTemp);
                            break;
                        case SysCfg.DB.Properties.TDataType.Int32:
                            CellTemp.Value = this._DataSource.RecordSet.FieldName(this._DataSource.FieldDefs[i].FieldName).asInt32();
                            CellTemp.IndexColumn = i;
                            CellTemp.IndexRow = conta;
                            CellTemp.Column = this.Columns[i];
                            NewRow.AddCell(CellTemp);
                            break;
                        case SysCfg.DB.Properties.TDataType.Boolean:
                            CellTemp.Value = this._DataSource.RecordSet.FieldName(this._DataSource.FieldDefs[i].FieldName).asBoolean();
                            CellTemp.IndexColumn = i;
                            CellTemp.IndexRow = conta;
                            CellTemp.Column = this.Columns[i];
                            NewRow.AddCell(CellTemp);
                            break;
                        case SysCfg.DB.Properties.TDataType.Text:
                            CellTemp.Value = this._DataSource.RecordSet.FieldName(this._DataSource.FieldDefs[i].FieldName).asText();
                            CellTemp.IndexColumn = i;
                            CellTemp.IndexRow = conta;
                            CellTemp.Column = this.Columns[i];
                            NewRow.AddCell(CellTemp);
                            break;
                        case SysCfg.DB.Properties.TDataType.Double:
                            CellTemp.Value = this._DataSource.RecordSet.FieldName(this._DataSource.FieldDefs[i].FieldName).asDouble();
                            CellTemp.IndexColumn = i;
                            CellTemp.IndexRow = conta;
                            CellTemp.Column = this.Columns[i];
                            NewRow.AddCell(CellTemp);
                            break;
                        case SysCfg.DB.Properties.TDataType.DateTime:
                            CellTemp.Value = this._DataSource.RecordSet.FieldName(this._DataSource.FieldDefs[i].FieldName).asDateTime();
                            CellTemp.IndexColumn = i;
                            CellTemp.IndexRow = conta;
                            CellTemp.Column = this.Columns[i];
                            NewRow.AddCell(CellTemp);
                            break;
                        case SysCfg.DB.Properties.TDataType.Object:
                            CellTemp.Value = this._DataSource.RecordSet.FieldName(this._DataSource.FieldDefs[i].FieldName).asString();
                            CellTemp.IndexColumn = i;
                            CellTemp.IndexRow = conta;
                            CellTemp.Column = this.Columns[i];
                            NewRow.AddCell(CellTemp);
                            break;
                        case SysCfg.DB.Properties.TDataType.Decimal:
                            CellTemp.Value = this._DataSource.RecordSet.FieldName(this._DataSource.FieldDefs[i].FieldName).asDouble();
                            CellTemp.IndexColumn = i;
                            CellTemp.IndexRow = conta;
                            CellTemp.Column = this.Columns[i];
                            NewRow.AddCell(CellTemp);
                            break;
                        default:
                            break;
                    }
                }
                conta = conta + 1;
                this.AddRow(NewRow);
                this._DataSource.Next();

            }
        }
    }


    //LFVA: Ordenación de columnas
    this.ColumnOrder = null;
    this.TypeOrder = 1;
    this.OrderRow = function (Column) {//LFVA y lo arreglo Carlos CRC
        if (this.EnabledResizeColumn) {
            var comparar = null;
            var listItems = new Array();
            if ((this.Rows.length > 0) && (typeof (Column.Index) != "undefined")) {
                for (var i = 0; i < this.Rows.length; i++) {
                    var cellValue = new Array();
                    for (var j = 0; j < this.Rows[i].Cells.length; j++) {
                        cellValue.push(this.Rows[i].Cells[j]._Value);
                    }

                    listItems.push({
                        id: i,
                        value: this.Rows[i].Cells[Column.Index]._Value,
                        cells: cellValue
                    });


                }

                switch (Column.DataType) {
                    case SysCfg.DB.Properties.TDataType.String:
                        comparar = function (a, b) {
                            if (a.value > b.value) {
                                return 1;
                            }
                            if (a.value < b.value) {
                                return -1;
                            }
                            return 0;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Text:
                        comparar = function (a, b) {
                            if (a.value > b.value) {
                                return 1;
                            }
                            if (a.value < b.value) {
                                return -1;
                            }
                            return 0;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Int32:

                        comparar = function (a, b) {
                            return a.value - b.value;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Double:
                        comparar = function (a, b) {
                            return a.value - b.value;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Boolean:
                        comparar = function (a, b) {
                            var a1 = a.value + "";
                            var b1 = b.value + "";
                            if (a1 > b1) {
                                return 1;
                            }
                            if (a1 < b1) {
                                return -1;
                            }
                            return 0;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.DateTime:

                        comparar = function (a, b) {
                            return new Date(a.value) - new Date(b.value);
                        }
                        break
                    default:
                        comparar = null;
                        break;
                }
                if (comparar != null) {
                    if (this.TypeOrder == 1) {
                        listItems.sort(function (a, b) {
                            return comparar(a, b);
                        });
                    }
                    else {
                        listItems.sort(function (a, b) {
                            return comparar(b, a);
                        });

                    }
                    this.ClearAllRows();
                    for (var i = 0; i < listItems.length; i++) {
                        var NewRow = new TRow();
                        for (var h = 0; h < this.Columns.length; h++) {
                            var Cell0 = new TCell();
                            //Cell0.Value = listRowsTemp[listItems[i].id].Cells[h]._Value;
                            Cell0.Value = listItems[i].cells[h];

                            Cell0.IndexColumn = h;
                            Cell0.IndexRow = i;
                            NewRow.AddCell(Cell0);
                        }
                        this.AddRow(NewRow);
                    }
                    if (this.ColumnOrder != null) {
                        this.ColumnOrder.Caption = this.ColumnOrder.Caption;
                        this.TypeOrder = (this.TypeOrder == 1) ? 2 : 1;
                    }
                    Column.SetCaptionOrder(this.TypeOrder);
                    this.ColumnOrder = Column;
                }

            }
        }
    }
    //LFVA: Fin Ordenación de columnas

    //CJRC: Visualizacion de Columnas
    this.SetVisibleColumns = function (Column, Visible) {
        var _index = Column.Index;
        for (var i = 0; i < this.Rows.length; i++) {
            this.Rows[i].Cells[_index].Visible = Visible;
        }
    }
    //CJRC: Fin Visualizacion de Columnas
}

var TColumn = function () {
    var _this = this;
    this._Name;
    this._Index;
    this._Caption;
    this._EnabledEditor = false;
    this._DataType = SysCfg.DB.Properties.TDataType.Unknown;
    this._ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.None;
    this.This = Uth("", "");
    this._Width = "100px";
    this._Visible = true;
    this.GridParent = null;

    Object.defineProperty(this, 'Name', {
        get: function () {
            return this._Name;
        },
        set: function (inValue) {
            this._Name = inValue;
        }
    });

    Object.defineProperty(this, 'Index', {
        get: function () {
            return this._Index;
        },
        set: function (inValue) {
            this._Index = inValue;
        }
    });

    Object.defineProperty(this, 'Caption', {
        get: function () {
            return this._Caption;
        },
        set: function (inValue) {
            this._Caption = inValue;
            $(this.This).html("");
            if (this.GridParent == null || this.GridParent.EnabledResizeColumn) {
                this.SetResize();//LFVA: Resize Column   
            }
            $(this.This).append(inValue);
        }
    });

    Object.defineProperty(this, 'EnabledEditor', {
        get: function () {
            return this._EnabledEditor;
        },
        set: function (inValue) {
            this._EnabledEditor = inValue;
        }
    });

    Object.defineProperty(this, 'DataType', {
        get: function () {
            return this._DataType;
        },
        set: function (inValue) {
            this._DataType = inValue;
        }
    });

    Object.defineProperty(this, 'ColumnStyle', {
        get: function () {
            return this._ColumnStyle;
        },
        set: function (inValue) {
            this._ColumnStyle = inValue;
        }
    });


    Object.defineProperty(this, 'Width', {//LFVA: Ancho de Column
        get: function () {
            return this._Width;
        },
        set: function (inValue) {
            this._Width = inValue;
            this.This.style.width = inValue;
            if (this.GridParent != null) {
                for (var i = 0, t = this.GridParent.Columns.length; i < t; i++) {
                    for (var j = 0; j < this.GridParent.Rows.length > 0; j++) {
                        var temRow = this.GridParent.Rows[j].Cells[i].This;
                        temRow.style.maxWidth = inValue + "px";
                        temRow.style.overflow = "hidden";
                        temRow.style.textOverflow = "ellipsis";
                        temRow.style.whiteSpace = "nowrap";
                    }
                }
            }
        }
    });

    Object.defineProperty(this, 'Visible', {
        get: function () {
            return this._Visible;
        },
        set: function (inValue) {
            this._Visible = inValue;
            if (inValue)
                this.This.style.display = "";
            else
                this.This.style.display = "none";

            if (this.GridParent != null) {
                this.GridParent.SetVisibleColumns(this, inValue);//CJRC: Visible Column   
            }
        }
    });

    this.SetCaptionOrder = function (typeOrder) { //LFVA: Colocar imagen segun ordenamiento

        $(this.This).html("");
        this.SetResize();
        var Textnode = document.createTextNode("" + this._Caption);         // Create a text node
        var ImgOrder = document.createElement("i");
        ImgOrder.classList.add("icon");
        ImgOrder.style.color = "#9E9E9E";
        ImgOrder.style.marginLeft = "5px";
        if (typeOrder == 1) {
            ImgOrder.classList.add("ion-arrow-down-a");
        }
        else {
            ImgOrder.classList.add("ion-arrow-up-a");

        }
        this.This.appendChild(Textnode);
        this.This.appendChild(ImgOrder);
    }
    this.SetResize = function () {//LFVA: Resize column
        var divResize = document.createElement("div");
        this.This.appendChild(divResize);
        divResize.style.height = "10px";
        divResize.style.width = "100%";
        divResize.classList.add("columScroll");
        $(divResize).resizable();
    }
    this.This.onclick = function () {//LFVA: Orden column

        if (_this.GridParent != null) {
            _this.GridParent.OrderRow(_this);
        }

    };


}

var TRow = function () {
    var _this = this;
    this._Cells = Array();
    this._Index;
    this.GridParent = null;
    this.This = Utr("", "");

    Object.defineProperty(this, 'Cells', {
        get: function () {
            return this._Cells;
        },
        set: function (inValue) {
            this._Cells = inValue;
        }
    });

    Object.defineProperty(this, 'Index', {
        get: function () {
            return this._Index;
        },
        set: function (inValue) {
            this._Index = inValue;
        }
    });

    //Object.defineProperty(this, 'onclick', {
    //    get: function () {
    //        return this.This.onclick;
    //    },
    //    set: function (inValue) {
    //        var _this = this;
    //        this.This.onclick = function () {
    //            if (_this.GridParent != null) {
    //                if (_this.GridParent._FocusedRowHandle != null)
    //                    _this.GridParent._FocusedRowHandle.This.style.backgroundColor = "";
    //                _this.GridParent._FocusedRowHandle = _this;
    //                _this.GridParent._FocusedRowHandle.This.style.backgroundColor = "lightyellow";
    //            }
    //            inValue();
    //        };
    //    }
    //});

    this.GetCell = function (Indexcolumn) {
        return this._Cell[Indexcolumn];
    }

    this.AddCell = function (inCell) {
        this._Cells.push(inCell);
        inCell._Row = this;
        $(this.This).append(inCell.This);
    }


    this.GetCellValue = function (NameColumn) {
        for (var i = 0; i < this._Cells.length; i++) {
            if (this._Cells[i].Column.Name == NameColumn)
                return this._Cells[i].Value;
        }
        return null;
    }

    this.SetCellValue = function (Value, NameColumn) {
        for (var i = 0; i < this._Cells.length; i++) {
            if (this._Cells[i].Column.Name == NameColumn)
                this._Cells[i].Value = Value;
        }
    }

    this.This.onclick = function () {
        if (_this.GridParent != null) {
            if (_this.GridParent._FocusedRowHandle != null)
                _this.GridParent._FocusedRowHandle.This.style.backgroundColor = "";
            _this.GridParent._FocusedRowHandle = _this;
            _this.GridParent._FocusedRowHandle.This.style.backgroundColor = "lightyellow";

            if (_this.GridParent.OnRowClick != null)
                _this.GridParent.OnRowClick(_this.GridParent, _this);
        }

    }
}

var TCell = function () {
    this._Value;
    this._Row;
    this._Column = null;
    this._IndexRow;
    this._IndexColumn;
    this.Control;
    this.This = Utd("", "");
    this._Visible = true;

    this.InitCell = function () {
        if (this._Column != null) {
            if (this._Column.EnabledEditor) {
                $(this.This).html("");
                switch (this._Column.DataType) {
                    case SysCfg.DB.Properties.TDataType.String: // SysCfg.DB.Properties.TDataType.String   
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //CREAR UN TEXTBOX
                                this.Control = new TVclTextBox(this.This, "");
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.TextPassword:
                                //CREAR UN TEXTBOX DE TIPO PASSWORD
                                this.Control = new TVclTextBox(this.This, "");
                                this.Control.Type = TImputtype.password;
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.LookUp:
                                //CREAR UN COMBOBOX 
                                this.Control = new TVclComboBox2(this.This, "");
                                this.cargarLookUp(this.Control);
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.LookUpOption:
                                //CREAR UN RADIO BUTTON
                                this.Control = new TVclInputRadioButton(this.This, "");
                                this.CargarLookUpOption(this.Control);
                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Text: //SysCfg.DB.Properties.TDataType.Text  
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //CREAR UN MEMO TEXT
                                this.Control = new TVclMemo(this.This, "");
                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Int32: // SysCfg.DB.Properties.TDataType.Int32:   
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //CREAR UN NUMERICUPDOWN
                                this.Control = new TVclTextBox(this.This, "");
                                this.Control.Type = TImputtype.number;
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.LookUp:
                                //CREAR UN COMBOBOX 
                                this.Control = new TVclComboBox2(this.This, "");
                                this.cargarLookUp(this.Control);
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.LookUpOption:
                                //CREAR UN RADIO BUTTON
                                this.Control = new TVclInputRadioButton(this.This, "");
                                this.CargarLookUpOption(this.Control);
                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Double: //SysCfg.DB.Properties.TDataType.Double:   
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //CREAR UN NUMERICUPDOWN                    
                                this.Control = new TVclTextBox(this.This, "");
                                this.Control.Type = TImputtype.number;
                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Boolean: //SysCfg.DB.Properties.TDataType.Boolean:     
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //CREAR UN CHECKBOX
                                this.Control = new TVclInputcheckbox(this.This, "");
                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.DateTime: //SysCfg.DB.Properties.TDataType.DateTime: 
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //CREAR UN DATATIME PICKER
                                this.Control = new TVclTextBox(this.This, "");
                                $(this.Control.This).datetimepicker({
                                    timeFormat: "hh:mm tt"
                                });
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.Date:
                                //CREAR UN DATE PICKER 
                                this.Control = new TVclTextBox(this.This, "");
                                $(this.Control.This).datetimepicker({
                                    showButtonPanel: true
                                });
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.Time:
                                //CREAR UN TIME PICKER
                                this.Control = new TVclTextBox(this.This, "");
                                $(this.Control.This).timepicker({
                                    timeFormat: "hh:mm tt"
                                });
                                break;
                        }
                        break
                    default:
                        break;
                }

            }
            this.SetValue(this._Value);
        }
    }

    this.SetValue = function (inValue) {

        if (this._Column != null) {

            if (this._Column.EnabledEditor) {
                switch (this._Column.DataType) {
                    case SysCfg.DB.Properties.TDataType.String: // SysCfg.DB.Properties.TDataType.String   
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //LIMPIAR UN TEXTBOX
                                this.Control.Text = inValue;
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.TextPassword:
                                //LIMPIAR UN TEXTBOX DE TIPO PASSWORD   
                                this.Control.Text = inValue;
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.LookUp:
                                //LIMPIAR UN COMBOBOX 
                                this.Control.Value = inValue;
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.LookUpOption:
                                this.Control.Value = inValue;
                                //LIMPIAR UN RADIO BUTTON
                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Text: //SysCfg.DB.Properties.TDataType.Text  
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //LIMPIAR UN MEMO TEXT
                                this.Control.Text = inValue;
                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Int32: // SysCfg.DB.Properties.TDataType.Int32:   
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //LIMPIAR UN NUMERICUPDOWN
                                this.Control.Text = inValue;
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.LookUp:
                                //LIMPIAR UN COMBOBOX 
                                this.Control.Value = inValue;
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.LookUpOption:
                                this.Control.Value = inValue;
                                //LIMPIAR UN RADIO BUTTON
                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Double: //SysCfg.DB.Properties.TDataType.Double:   
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //LIMPIAR UN NUMERICUPDOWN                    
                                this.Control.Text = inValue;
                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Boolean: //SysCfg.DB.Properties.TDataType.Boolean:     
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //LIMPIAR UN CHECKBOX
                                this.Control.Checked = inValue;
                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.DateTime: //SysCfg.DB.Properties.TDataType.DateTime: 
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //LIMPIAR UN DATATIME PICKER
                                this.Control.Text = inValue;
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.Date:
                                //LIMPIAR UN DATE PICKER 
                                this.Control.Text = inValue;
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.Time:
                                //LIMPIAR UN TIME PICKER
                                this.Control.Text = inValue;
                                break;
                        }
                        break
                    default:
                        break;
                }
            }
            else {
                switch (this._Column.DataType) {
                    case SysCfg.DB.Properties.TDataType.String: // SysCfg.DB.Properties.TDataType.String   
                        $(this.This).html("");
                        //LFVA: Recortar texto y mostrarlo en un ModalFV
                        var textStrech = inValue;
                        if (inValue != null && inValue.length > 150) {
                            var textStrech = document.createElement("a");
                            textStrech.innerHTML = inValue;

                            textStrech.onclick = function (e) {
                                var textAreaStrech = document.createElement("textarea");
                                textAreaStrech.cols = "40";
                                textAreaStrech.rows = "4";
                                textAreaStrech.innerHTML = inValue;
                                var modalFV = new Componet.GridCF.TGridCFView.TModalFV(textAreaStrech, textStrech);
                                var evt = e ? e : window.event;
                                if (evt.stopPropagation) evt.stopPropagation();
                                if (evt.cancelBubble != null) evt.cancelBubble = true;
                            }
                            this.This.appendChild(textStrech);
                        }
                        else {
                            $(this.This).html(textStrech);
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Text: //SysCfg.DB.Properties.TDataType.Text  
                        $(this.This).html("");
                        var textStrech = inValue;
                        //LFVA: Recortar texto y mostrarlo en un ModalFV
                        if (inValue != null && inValue.length > 100) {
                            var textStrech = document.createElement("a");
                            textStrech.innerHTML = inValue;
                            textStrech.onclick = function () {
                                var textAreaStrech = document.createElement("textarea");
                                textAreaStrech.cols = "40";
                                textAreaStrech.rows = "4";
                                textAreaStrech.innerHTML = inValue;
                                var modalFV = new Componet.GridCF.TGridCFView.TModalFV(textAreaStrech, textStrech);
                                var e = window.event || arguments.callee.caller.arguments[0];

                                e.stopPropagation();
                            }
                            this.This.appendChild(textStrech);
                        }
                        else {
                            $(this.This).html(textStrech);
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Int32: // SysCfg.DB.Properties.TDataType.Int32:   
                        $(this.This).html(inValue);
                        break;
                    case SysCfg.DB.Properties.TDataType.Double: //SysCfg.DB.Properties.TDataType.Double:   
                        $(this.This).html(inValue);
                        break;
                    case SysCfg.DB.Properties.TDataType.Boolean: //SysCfg.DB.Properties.TDataType.Boolean:     
                        $(this.This).html('' + inValue);
                        break;
                    case SysCfg.DB.Properties.TDataType.DateTime: //SysCfg.DB.Properties.TDataType.DateTime: 
                        $(this.This).html(formatJSONDate2(inValue));
                        break
                    default:

                        $(this.This).html(inValue);

                        break;
                }
                //LFVA: Aplica el recorte de celdas
                var size = this._Column.Width;
                var temRow = this.This;
                temRow.style.maxWidth = size;
                temRow.style.overflow = "hidden";
                temRow.style.textOverflow = "ellipsis";
                temRow.style.whiteSpace = "nowrap";
            }
        }
    }

    this.GetValue = function () {
        if (this._Column != null) {
            if (this._Column.EnabledEditor) {
                switch (this._Column.DataType) {
                    case SysCfg.DB.Properties.TDataType.String: // SysCfg.DB.Properties.TDataType.String   
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //LIMPIAR UN TEXTBOX
                                this._Value = this.Control.Text;
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.TextPassword:
                                //LIMPIAR UN TEXTBOX DE TIPO PASSWORD   
                                this._Value = this.Control.Text;
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.LookUp:
                                //LIMPIAR UN COMBOBOX 
                                this._Value = this.Control.Value;
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.LookUpOption:
                                //LIMPIAR UN RADIO BUTTON
                                this._Value = this.Control.Value;
                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Text: //SysCfg.DB.Properties.TDataType.Text  
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //LIMPIAR UN MEMO TEXT
                                this._Value = this.Control.Text;
                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Int32: // SysCfg.DB.Properties.TDataType.Int32:   
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //LIMPIAR UN NUMERICUPDOWN
                                this._Value = parseInt(this.Control.Text);
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.LookUp:
                                //LIMPIAR UN COMBOBOX 
                                this._Value = this.Control.Value;
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.LookUpOption:
                                //LIMPIAR UN RADIO BUTTON
                                this._Value = this.Control.Value;
                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Double: //SysCfg.DB.Properties.TDataType.Double:   
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //LIMPIAR UN NUMERICUPDOWN                    
                                this._Value = parseFloat(this.Control.Text);
                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Boolean: //SysCfg.DB.Properties.TDataType.Boolean:     
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //LIMPIAR UN CHECKBOX
                                this._Value = this.Control.Checked;
                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.DateTime: //SysCfg.DB.Properties.TDataType.DateTime: 
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //LIMPIAR UN DATATIME PICKER
                                this._Value = this.Control.Text;
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.Date:
                                //LIMPIAR UN DATE PICKER 
                                this._Value = this.Control.Text;
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.Time:
                                //LIMPIAR UN TIME PICKER
                                this._Value = this.Control.Text;
                                break;
                        }
                        break
                    default:
                        break;
                }
            }
        }
        return this._Value;
    }

    Object.defineProperty(this, 'Value', {
        get: function () {
            return this.GetValue();
        },
        set: function (inValue) {
            inValue = "" + inValue;
            this._Value = "" + inValue;
            $(this.This).html(inValue);
            this.SetValue(inValue);
        }
    });

    Object.defineProperty(this, 'Row', {
        get: function () {
            return this._Row;
        },
        set: function (inValue) {
            this._Row = inValue;
        }
    });

    Object.defineProperty(this, 'Column', {
        get: function () {
            return this._Column;
        },
        set: function (inValue) {
            this._Column = inValue;
        }
    });

    Object.defineProperty(this, 'IndexRow', {
        get: function () {
            return this._IndexRow;
        },
        set: function (inValue) {
            this._IndexRow = inValue;
        }
    });

    Object.defineProperty(this, 'IndexColumn', {
        get: function () {
            return this._IndexColumn;
        },
        set: function (inValue) {
            this._IndexColumn = inValue;
        }
    });

    Object.defineProperty(this, 'Control', {
        get: function () {
            return this._Control;
        },
        set: function (inValue) {
            this._Control = inValue;
        }
    });

    Object.defineProperty(this, 'Visible', {
        get: function () {
            return this._Visible;
        },
        set: function (_Value) {
            this._Visible = _Value;
            if (_Value)
                this.This.style.display = "";
            else
                this.This.style.display = "none";
        }
    });
}