﻿// Constantes, variables globales, enums, definicon de estructuras 
//Componet.GridCF.Properties
//*********************** GridControlCF ****************
Componet.GridCF.Properties.TConfig = function () {
    this._RowShowInfo = "20";//Numero de filas que se muestran en el grid
    this._RowShowDetails = "2";//Numero de columnas en que se divide el detalle del grid (Grid color azul)
    this._FontFamily = "";//Fuente para la tabla, por ejemplo: arial,san serif,ect
    this._FontSize = "14";//Tamaño de la letra
    this._TextAlign = "";//Alieancion left,right,justify o center
    this._FontWeight = "";//Letra en negrita: bold
    this._FontStyle = "";//Letra en cursica:cursive
    this._HeaderColor = "#797979";//Color de texto de las columnas
    this._BodyColor = "";//Color de texto de las celdas
    this._Color = "";//Color de los textos del componente
    /*Propiedad para obtener y establecer el Numero de filas y actulizar el IniDB.WriteString*/
    Object.defineProperty(this, 'RowShowInfo', {
        get: function () {
            return this._RowShowInfo;
        },
        set: function (Value) {
            this._RowShowInfo = Value;
            SysCfg.App.IniDB.WriteString("Componet_GridCF", "RowShowInfo", Value);
        }
    });
    /*Propiedad para obtener y establecer el Numero de columnas en que se divide el detalle del grid y actulizar el IniDB.WriteString*/
    Object.defineProperty(this, 'RowShowDetails', {
        get: function () {
            return this._RowShowDetails;
        },
        set: function (Value) {
            this._RowShowDetails = Value;
            SysCfg.App.IniDB.WriteString("Componet_GridCF", "RowShowDetails", Value)
        }
    });
    /*Propiedad para obtener y establecer el Color de los textos del componente y actulizar el IniDB.WriteString*/
    Object.defineProperty(this, 'Color', {
        get: function () {
            return this._Color;
        },
        set: function (Value) {
            this._Color = Value;
        }
    });
    /*Propiedad para obtener y establecer el Color de texto de las celdas y actulizar el IniDB.WriteString*/
    Object.defineProperty(this, 'BodyColor', {
        get: function () {
            return this._BodyColor;
        },
        set: function (Value) {
            this._BodyColor = Value;
            SysCfg.App.IniDB.WriteString("Componet_GridCF", "BodyColor", Value);
        }
    });
    /*Propiedad para obtener y establecer el Color de texto de las columnas y actulizar el IniDB.WriteString*/
    Object.defineProperty(this, 'HeaderColor', {
        get: function () {
            return this._HeaderColor;
        },
        set: function (Value) {
            this._HeaderColor = Value;
            SysCfg.App.IniDB.WriteString("Componet_GridCF", "HeaderColor", Value);

        }
    });
    /*Propiedad para obtener y establecer la Letra en negrita y actulizar el IniDB.WriteString*/
    Object.defineProperty(this, 'FontWeight', {
        get: function () {
            return this._FontWeight;
        },
        set: function (Value) {
            this._FontWeight = Value;
            SysCfg.App.IniDB.WriteString("Componet_GridCF", "FontWeight", Value) ;

        }
    });
    /*Propiedad para obtener y establecer el tamaño de letra  y actulizar el IniDB.WriteString*/
    Object.defineProperty(this, 'FontSize', {
        get: function () {
            return this._FontSize;
        },
        set: function (Value) {
            this._FontSize = Value;
            SysCfg.App.IniDB.WriteString("Componet_GridCF", "FontSize", Value);

        }
    });
    /*Propiedad para obtener y establecer la letra en cursiva y actulizar el IniDB.WriteString*/
    Object.defineProperty(this, 'FontStyle', {
        get: function () {
            return this._FontStyle;
        },
        set: function (Value) {
            this._FontStyle = Value;
            SysCfg.App.IniDB.WriteString("Componet_GridCF", "FontStyle", Value);

        }
    });
    /*Propiedad para obtener y establecer la alineación de las filas y actulizar el IniDB.WriteString*/
    Object.defineProperty(this, 'TextAlign', {
        get: function () {
            return this._TextAlign;
        },
        set: function (Value) {

            this._TextAlign = Value;
            SysCfg.App.IniDB.WriteString("Componet_GridCF", "TextAlign", Value);
        }
    });
    /*Propiedad para obtener y establecer la fuente de la letra y actulizar el IniDB.WriteString*/
    Object.defineProperty(this, 'FontFamily', {
        get: function () {
            return this._FontFamily;
        },
        set: function (Value) {
            this._FontFamily = Value;
            SysCfg.App.IniDB.WriteString("Componet_GridCF", "FontFamily", Value);
        }
    });
}
Componet.GridCF.Properties.VConfig = new Componet.GridCF.Properties.TConfig();
Componet.GridCF.Properties.FontSize = 12;//Eliminar cuando se integre a indexDB
















//Anexo 
//Como guardar en ini
//Componet.GridCF.Properties.FontSize = 100;
//SysCfg.App.Methods.SavebyIniIndexDb();
//SysCfg.App.Properties.Device
//UsrCfg.SD.Properties.TSDCASEPERMISSION.GetEnum(undefined);

//UsrCfg.SD.Properties.TIDSDOPERATION_ESCTYPE =
//{
//    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
//    _ASSIGNEDNEXTUSER: { value: 0, name: "_ASSIGNEDNEXTUSER" },
//    _ASSIGNEDNEXTLAVEL: { value: 1, name: "_ASSIGNEDNEXTLAVEL" },
//    _ASSIGNEDUSER: { value: 2, name: "_ASSIGNEDUSER" }
//}

//SysCfg.DB.Properties.TDataType = {
//    GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
//    Unknown: { value: 0, name: "Unknown" },
//    String: { value: 1, name: "String" },
//    Int32: { value: 2, name: "Int32" },
//    Boolean: { value: 3, name: "Boolean" },
//    AutoInc: { value: 4, name: "AutoInc" },
//    Text: { value: 5, name: "Text" },
//    Double: { value: 6, name: "Double" },
//    DateTime: { value: 7, name: "DateTime" },
//    Object: { value: 8, name: "Object" },
//    Decimal: { value: 9, name: "Decimal" },
//}

//var BDDAccess0 = SysCfg.DB.Properties.TBDDAccess.GetEnum(undefined);
//var BDDAccess0 = SysCfg.DB.Properties.TBDDAccess._None;
//var BDDAccess1 = SysCfg.DB.Properties.TBDDAccess.GetEnum("None");
//var BDDAccess3 = SysCfg.DB.Properties.TBDDAccess.GetEnum(1);
//var BDDAccess4 = SysCfg.DB.Properties.TBDDAccess.GetEnum("ADO");