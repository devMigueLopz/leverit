﻿﻿var TVclGridCF = function (inObjectHTML, ID, inCallBack) {

    this.FileSRVEvent = null;
    this.OnDataSourceChanged = null;
    this.OnAutoGenerateColumnsChanged = null;
    this.OnImgGruopOpen = null;
    this._AutoGenerateColumns = true; this._DataSource = null;
    this.IsTree = false;
    this.IsInfo = false;
    this.IsMobile = false;
    Object.defineProperty(this, 'AutoGenerateColumns', {
        get: function () {
            return this._AutoGenerateColumns;
        },
        set: function (Value) {
            this._AutoGenerateColumns = Value;
            this.AutoGenerateColumnsChanged(Value)
        }
    });
    Object.defineProperty(this, 'DataSource', {
        get: function () {
            return this._DataSource;
        },
        set: function (Value) {
            this.DataSourceChanged(Value);
        }
    });
    this.DataSourceChanged = function (inDataSource) {
        //codigo de cuando se cambia el datasource, cambiar columnas, ordenar todo nuevamente.   
        if (this.OnDataSourceChanged != null)
            this.OnDataSourceChanged(this, inDataSource);
    }
    this.AutoGenerateColumnsChanged = function (inAutoGenerateColumns) {
        //codigo de cuando se cambia el valor de autogenerate de false a true o viceversa, calcular todo nuevamente      
        if (this.OnAutoGenerateColumnsChanged != null)
            this.OnAutoGenerateColumnsChanged(this, inAutoGenerateColumns);
    }
    this._Columns = new Array();
    this._Rows = new Array();
    this._FocusedRowHandle = null;
    this._DataSource;
    this._IndexRow = -1;
    this._IndexCol = -1;//LFVA

    this._FocusedColHandle = null;//LFVA
    this._EnableEditOptionsColumn = false;//Mostrar las opciones en las filas.
    this._EnableNavigatorControl = false;
    this._EnableNavigatorEdit = false;
    this._EnableDescriptionRow = false;
    this._EnablePanelGroup = false;
    this._EnableColumnGrid = false;
    this._DivNavigatorControlLeft = null;
    this._DivNavigatorControlRight = null;
    this._DivNavigatorEdit = null;
    this._DivDescriptionRow = null;
    this._DivPanelStatus = null;
    this._DivPanelGroup = null;
    this._DivColumnGrid = null;
    this.ResponsiveCont = Udiv(inObjectHTML, "");
    this.ResponsiveCont.classList.add("table-responsive");
    this.This = Utable(this.ResponsiveCont, ID);
    //if (SysCfg.App.Properties.Device == SysCfg.App.TDevice.Desktop) 
    this.ResponsiveCont.style.marginBottom = "0px";
    this.This.style.marginBottom = "0px";
    this.Header = Uthead(this.This, ID);
    this.RowHeader = Utr(this.Header, "");
    this.Body = Utbody(this.This, ID);
    this.Order = "ninguno";
    this.OnEventPagination = null;
    this.EventPagination = function () {
        if (this.OnEventPagination != null) {
            this.OnEventPagination();
        }
    }
    this.This.classList.add("table");
    this.This.classList.add("table-bordered");
    this.This.classList.add("table-hover");
    this.This.style.fontSize = "12px";
    this.Body.style.cursor = "pointer";
    this._CustomDrawCell = null;
    this.CustomDrawCell = null;

    Object.defineProperty(this, 'CustomDrawCell', {
        get: function () {
            return this._CustomDrawCell;
        },
        set: function (inValue) {
            this._CustomDrawCell = inValue;

        }
    });
    Object.defineProperty(this, 'EnableEditOptionsColumn', {
        get: function () {
            return this._EnableEditOptionsColumn;
        },
        set: function (inValue) {
            this._EnableEditOptionsColumn = inValue;
            this.showHideColumn(this.Columns.length - 1, inValue);
        }
    });
    Object.defineProperty(this, 'EnablePanelStatus', {
        get: function () {
            return this._EnablePanelStatus;
        },
        set: function (inValue) {
            this._EnablePanelStatus = inValue;
            if (this._DivPanelStatus != null) {
                if (inValue) {
                    this._DivPanelStatus.Row.This.style.display = "";
                } else {
                    this._DivPanelStatus.Row.This.style.display = "none";
                }
            }
        }
    });
    Object.defineProperty(this, 'EnableNavigatorControl', {
        get: function () {
            return this._EnableNavigatorControl;
        },
        set: function (inValue) {
            this._EnableNavigatorControl = inValue;
            if (this._DivNavigatorControlLeft != null && this._DivNavigatorControlRight != null) {
                if (inValue) {
                    this._DivNavigatorControlLeft.Row.This.style.display = "";
                    this._DivNavigatorControlRight.Row.This.style.display = "";
                } else {
                    this._DivNavigatorControlLeft.Row.This.style.display = "none";
                    this._DivNavigatorControlRight.Row.This.style.display = "none";
                }
            }
        }
    });
    Object.defineProperty(this, 'EnableNavigatorEdit', {
        get: function () {
            return this._EnableNavigatorEdit;
        },
        set: function (inValue) {
            this._EnableNavigatorEdit = inValue;
            if (this._DivNavigatorEdit != null) {
                if (inValue) {
                    this._DivNavigatorEdit.Row.This.style.display = "";
                } else {
                    this._DivNavigatorEdit.Row.This.style.display = "none";
                }
            }
        }
    });
    Object.defineProperty(this, 'EnableDescriptionRow', {
        get: function () {
            return this._EnableDescriptionRow;
        },
        set: function (inValue) {
            this._EnableDescriptionRow = inValue;
            if (this._DivDescriptionRow != null) {
                if (inValue) {
                    this._DivDescriptionRow.Row.This.style.display = "";
                } else {
                    this._DivDescriptionRow.Row.This.style.display = "none";
                }
            }
        }
    });
    Object.defineProperty(this, 'EnablePanelGroup', {
        get: function () {
            return this._EnablePanelGroup;
        },
        set: function (inValue) {
            this._EnablePanelGroup = inValue;
            if (this._DivPanelGroup != null) {
                if (inValue) {
                    this._DivPanelGroup.Row.This.style.display = "";
                } else {
                    this._DivPanelGroup.Row.This.style.display = "none";
                }
            }
        }
    });

    Object.defineProperty(this, 'EnablePanelGroupTree', {
        get: function () {
            return this._EnablePanelGroup;
        },
        set: function (inValue) {
            this._EnablePanelGroup = inValue;
            if (this._DivPanelGroup != null) {
                if (inValue) {
                    $(this._DivPanelGroup.Row.This).show("fold", {}, 500);
                } else {

                    $(this._DivPanelGroup.Row.This).hide();
                }
            }
        }
    });
    Object.defineProperty(this, 'EnableColumnGrid', {
        get: function () {
            return this._EnableColumnGrid;
        },
        set: function (inValue) {
            this._EnableColumnGrid = inValue;
            if (this._DivColumnGrid != null) {
                if (inValue) {
                    this._DivColumnGrid.Row.This.style.display = "";
                } else {
                    this._DivColumnGrid.Row.This.style.display = "none";
                }
            }
        }
    });

    Object.defineProperty(this, 'Columns', {
        get: function () {
            /*
             var _columnRes = new Array();
             for (var i = 0; i < this._Columns.length; i++) {
                 if (!this._Columns[i].IsOption)
                     _columnRes.push(this._Columns[i])
             }
             return _columnRes;           
            */
            return this._Columns;
        },
        set: function (inValue) {
            this._Columns = inValue;
        }
    });

    Object.defineProperty(this, 'ColumnsAll', {
        get: function () {
            return this._Columns;
        }
    });

    Object.defineProperty(this, 'Rows', {
        get: function () {
            return this._Rows;
        },
        set: function (inValue) {
            this._Rows = inValue;
        }
    });

    Object.defineProperty(this, 'FocusedRowHandle', {
        get: function () {
            return this._FocusedRowHandle;
        },
        set: function (inValue) {
            this._FocusedRowHandle = inValue;
        }
    });

    Object.defineProperty(this, 'IndexRow', {
        get: function () {
            return this._IndexRow;
        },
        set: function (inValue) {
            this._IndexRow = inValue;
            if (inValue >= 0) {
                if (this._Rows.length > 0) {

                    if (this._FocusedRowHandle != null) {
                        this._FocusedRowHandle.This.style.backgroundColor = "";
                        for (var i = 0, t = this.Columns.length; i < t; i++) {
                            this._FocusedRowHandle.Cells[i].ColorBack();
                        }

                    }

                    this._FocusedRowHandle = this._Rows[inValue];
                    this._FocusedRowHandle.This.style.backgroundColor = "lightyellow";
                    for (var i = 0, t = this.Columns.length; i < t; i++) {
                        this._FocusedRowHandle.Cells[i].RestoreFunctionColor();
                    }

                }
            }
        }
    });
    Object.defineProperty(this, 'FocusedColHandle', {
        get: function () {
            return this._FocusedColHandle;
        },
        set: function (inValue) {
            this._FocusedColHandle = inValue;
        }
    });

    Object.defineProperty(this, 'IndexCol', {
        get: function () {
            return this._IndexCol;
        },
        set: function (inValue) {
            this._IndexCol = inValue;
            if (inValue >= 0) {
                if (this._Columns.length > 0) {

                    if (this._FocusedColHandle != null)
                        this._FocusedColHandle.This.style.backgroundColor = "";
                    this._FocusedColHandle = this._Columns[inValue];


                }
            }
        }
    });


    Object.defineProperty(this, 'ExistOption2', {
        get: function () {
            var Res = false;
            for (var i = 0; i < this._Columns.length; i++) {
                if (this._Columns[i].IsOption)
                    return true;
            }
            return Res;
        }
    });
    this._ExistOption = false;
    Object.defineProperty(this, 'ExistOption', {
        get: function () {
            return this._ExistOption;
        },
        set: function (inValue) {
            if (inValue) {
                this._ExistOption = true;
            }

        }
    });
    this.OrderColumns = function (listOrder) {
        listOrderNumber = new Array();

        if (listOrder != null) {
            if (listOrder.length != this.Columns.length - 2) {
                var tempListOrder = new Array();
                var tempListOrderB = new Array();
                for (var i = 0, t = listOrder.length; i < t; i++) {
                    tempListOrder.push(listOrder[i]);
                    tempListOrderB.push(listOrder[i]);
                }
                for (var i = 1, t = this.Columns.length - 1; i < t; i++) {
                    var bandera = false;
                    for (var j = 0, t1 = tempListOrder.length; j < t1; j++) {
                        if (tempListOrder[j] == this.Columns[i].Name) {
                            bandera = true;

                        }
                    }
                    if (!bandera) {
                        tempListOrderB.push(this.Columns[i].Name);
                    }
                }
                listOrder = tempListOrderB;
            }
            var banderaAplicarOrden = false;

            for (var i = 0, t = listOrder.length; i < t; i++) {
                if (this.Columns[i + 1].Name == listOrder[i]) {
                    listOrderNumber.push(i + 1);
                } else {
                    banderaAplicarOrden = true;
                }

            }
            if (banderaAplicarOrden) {
                listOrderNumber = new Array();

            } else {

                return listOrderNumber;
            }


            var listempColumns = new Array();
            for (var i = 0, t = listOrder.length; i < t; i++) {

                for (var j = 0, t1 = this.Columns.length; j < t1; j++) {
                    if (!this.Columns[j].IsOption && !this.Columns[j].IsOptionCheck) {
                        if (listOrder[i] == this.Columns[j].Name) {
                            listOrderNumber.push(j);
                            var temporalColumnA = new TVclColumnCF();
                            temporalColumnA.IsOption = this.Columns[j].IsOption;
                            temporalColumnA.IsOptionCheck = this.Columns[j].IsOptionCheck;
                            temporalColumnA._ActiveEditor = this.Columns[j]._ActiveEditor;
                            temporalColumnA._Name = this.Columns[j]._Name;
                            temporalColumnA._Index = this.Columns[j]._Index;
                            temporalColumnA.IndexDataSourceDefault = this.Columns[j].IndexDataSourceDefault;
                            temporalColumnA.IsColumnSelectOrder = this.Columns[j].IsColumnSelectOrder;
                            temporalColumnA._Caption = this.Columns[j]._Caption;
                            temporalColumnA._EnabledEditor = this.Columns[j]._EnabledEditor;
                            temporalColumnA._DataType = this.Columns[j]._DataType;
                            temporalColumnA._ColumnStyle = this.Columns[j]._ColumnStyle;
                            temporalColumnA._Size = this.Columns[j]._Size;
                            temporalColumnA.OnRefreshColumn = this.Columns[j].OnRefreshColumn;

                            temporalColumnA.DataEventControl = this.Columns[j].DataEventControl;
                            temporalColumnA.onEventControl = this.Columns[j].onEventControl;

                            temporalColumnA._FileType = this.Columns[j]._FileType;
                            temporalColumnA._FileSRVEvent = this.Columns[j]._FileSRVEvent;
                            temporalColumnA.OptionProgressBar = this.Columns[j].OptionProgressBar;

                            temporalColumnA.onEventFilterColumn = this.Columns[j].onEventFilterColumn;
                            temporalColumnA._ColumEdit = this.Columns[j]._ColumEdit;
                            temporalColumnA._ImgOrder = this.Columns[j]._ImgOrder;
                            temporalColumnA.GridParent = this.Columns[j].GridParent;
                            temporalColumnA.ColumEdit = this.Columns[j].ColumEdit;
                            temporalColumnA._InsertActiveEditor = this.Columns[j]._InsertActiveEditor;
                            temporalColumnA._UpdateActiveEditor = this.Columns[j]._UpdateActiveEditor;
                            temporalColumnA.DataBuildCustomCell = this.Columns[j].DataBuildCustomCell;
                            temporalColumnA._CustomDrawCell = this.Columns[j]._CustomDrawCell;
                            temporalColumnA._ToolBarButton = this.Columns[j]._ToolBarButton;
                            temporalColumnA._Visible = this.Columns[j]._Visible;
                            temporalColumnA._VisibleVertical = this.Columns[j]._VisibleVertical;
                            temporalColumnA._Visible_IsShowValue = this.Columns[j]._Visible_IsShowValue;
                            temporalColumnA._VisibleVertical_IsShowValue = this.Columns[j]._VisibleVertical_IsShowValue;

                            this.Columns[j].DataBuildCustomCell = null;
                            this.Columns[j]._CustomDrawCell = null;
                            listempColumns.push(temporalColumnA);
                        }
                    }

                }

            }
            var contador = 0;
            for (var i = 0, t = this.Columns.length; i < t; i++) {
                if (!this.Columns[i].IsOption && !this.Columns[i].IsOptionCheck) {
                    this.Columns[i].IsOption = listempColumns[contador].IsOption;
                    this.Columns[i].IsOptionCheck = listempColumns[contador].IsOptionCheck;
                    this.Columns[i]._ActiveEditor = listempColumns[contador]._ActiveEditor;
                    this.Columns[i].Name = listempColumns[contador]._Name;
                    this.Columns[i]._Index = i;
                    this.Columns[i].IndexDataSourceDefault = listempColumns[contador].IndexDataSourceDefault;
                    this.Columns[i]._EnabledEditor = listempColumns[contador]._EnabledEditor;
                    this.Columns[i]._DataType = listempColumns[contador]._DataType;
                    this.Columns[i]._ColumnStyle = listempColumns[contador]._ColumnStyle;
                    this.Columns[i]._Size = listempColumns[contador]._Size;
                    this.Columns[i].OnRefreshColumn = listempColumns[contador].OnRefreshColumn;

                    this.Columns[i].DataEventControl = listempColumns[contador].DataEventControl;
                    this.Columns[i].onEventControl = listempColumns[contador].onEventControl;
                    this.Columns[i]._FileType = listempColumns[contador]._FileType;
                    this.Columns[i]._FileSRVEvent = listempColumns[contador]._FileSRVEvent;
                    this.Columns[i].OptionProgressBar = listempColumns[contador].OptionProgressBar;

                    this.Columns[i].onEventFilterColumn = listempColumns[contador].onEventFilterColumn;
                    this.Columns[i]._ColumEdit = listempColumns[contador]._ColumEdit;
                    this.Columns[i]._ImgOrder = listempColumns[contador]._ImgOrder;
                    this.Columns[i].GridParent = listempColumns[contador].GridParent;
                    this.Columns[i].ColumEdit = listempColumns[contador].ColumEdit;
                    this.Columns[i]._InsertActiveEditor = listempColumns[contador]._InsertActiveEditor;
                    this.Columns[i]._UpdateActiveEditor = listempColumns[contador]._UpdateActiveEditor;
                    this.Columns[i]._ToolBarButton = listempColumns[contador]._ToolBarButton;
                    this.Columns[i].DataBuildCustomCell = listempColumns[contador].DataBuildCustomCell;
                    this.Columns[i]._CustomDrawCell = listempColumns[contador]._CustomDrawCell;
                    this.Columns[i].IsColumnSelectOrder = listempColumns[contador].IsColumnSelectOrder;
                    this.Columns[i].CaptionStyle = listempColumns[contador]._Caption;
                    this.Columns[i]._Visible = listempColumns[contador]._Visible;
                    this.Columns[i]._VisibleVertical = listempColumns[contador]._VisibleVertical;
                    this.Columns[i]._Visible_IsShowValue = listempColumns[contador]._Visible_IsShowValue;
                    this.Columns[i]._VisibleVertical_IsShowValue = listempColumns[contador]._VisibleVertical_IsShowValue;
                    contador++;
                }
            }
            if (this.Rows.length > 0) {
                for (var i = 0, t = this.Rows.length; i < t; i++) {
                    var valuesCell = new Array();
                    for (var j = 0, t1 = listOrderNumber.length; j < t1; j++) {
                        var cell = {
                            Value: this.Rows[i].Cells[listOrderNumber[j]]._Value,
                            CellColor: this.Rows[i].Cells[listOrderNumber[j]].CellColor
                        }
                        valuesCell.push(cell);
                    }

                    for (var j = 0, t1 = valuesCell.length; j < t1; j++) {
                        this.Rows[i].Cells[j + 1].RestoreFunctionColor();
                        this.Rows[i].Cells[j + 1].Column = this.Columns[j + 1];
                        this.Rows[i].Cells[j + 1]._Value = valuesCell[j].Value;
                        this.Rows[i].Cells[j + 1].InitCell();
                        this.Rows[i].Cells[j + 1].CellColor = valuesCell[j].CellColor;

                    }
                }

            }

        }

        return listOrderNumber;
    }
    this.fnClone = (function () {
        // @Private
        var _toString = Object.prototype.toString;

        // @Private
        function _clone(from, dest, objectsCache) {
            var prop;
            // determina si @from es un valor primitivo o una funcion
            if (from === null || typeof from !== "object") return from;
            // revisa si @from es un objeto ya guardado en cache
            if (_toString.call(from) === "[object Object]") {
                if (objectsCache.filter(function (item) {
                    return item === from;
                }).length) return from;
                // guarda la referencia de los objetos creados
                objectsCache.push(from);
            }
            // determina si @from es una instancia de alguno de los siguientes constructores
            if (from.constructor === Date || from.constructor === RegExp || from.constructor === Function ||
                from.constructor === String || from.constructor === Number || from.constructor === Boolean) {
                return new from.constructor(from);
            }
            if (from.constructor !== Object && from.constructor !== Array) return from;
            // crea un nuevo objeto y recursivamente itera sus propiedades
            dest = dest || new from.constructor();
            for (prop in from) {
                // TODO: allow overwrite existing properties
                dest[prop] = (typeof dest[prop] === "undefined" ?
                    _clone(from[prop], null, objectsCache) :
                    dest[prop]);
            }
            return dest;
        }

        // función retornada en el closure
        return function (from, dest) {
            var objectsCache = [];
            return _clone(from, dest, objectsCache);
        };

    }());
    this.MoveColumns = function (posicionA, posicionB) {
        var temporalCaptionColumnValueA = this.Columns[posicionA].Caption;
        var temporalCaptionColumnValueB = this.Columns[posicionB].Caption;
        var temporalNameColumnValueA = this.Columns[posicionA]._Name;
        var temporalNameColumnValueB = this.Columns[posicionB]._Name;

        var temporalColumnA = new TVclColumnCF();
        temporalColumnA.IsOption = this.Columns[posicionA].IsOption;
        temporalColumnA.IsOptionCheck = this.Columns[posicionA].IsOptionCheck;
        temporalColumnA._ActiveEditor = this.Columns[posicionA]._ActiveEditor;
        temporalColumnA._Name = this.Columns[posicionA]._Name;
        temporalColumnA._Index = this.Columns[posicionA]._Index;
        temporalColumnA.IndexDataSourceDefault = this.Columns[posicionA].IndexDataSourceDefault;
        temporalColumnA.IsColumnSelectOrder = this.Columns[posicionA].IsColumnSelectOrder;
        temporalColumnA._Caption = this.Columns[posicionA]._Caption;
        temporalColumnA._EnabledEditor = this.Columns[posicionA]._EnabledEditor;
        temporalColumnA._DataType = this.Columns[posicionA]._DataType;
        temporalColumnA._ColumnStyle = this.Columns[posicionA]._ColumnStyle;
        temporalColumnA._Size = this.Columns[posicionA]._Size;
        temporalColumnA.OnRefreshColumn = this.Columns[posicionA].OnRefreshColumn;

        temporalColumnA.DataEventControl = this.Columns[posicionA].DataEventControl;
        temporalColumnA.onEventControl = this.Columns[posicionA].onEventControl;
        temporalColumnA._FileType = this.Columns[posicionA]._FileType;
        temporalColumnA._FileSRVEvent = this.Columns[posicionA]._FileSRVEvent;
        temporalColumnA.OptionProgressBar = this.Columns[posicionA].OptionProgressBar;


        temporalColumnA.onEventFilterColumn = this.Columns[posicionA].onEventFilterColumn;
        temporalColumnA._ColumEdit = this.Columns[posicionA]._ColumEdit;
        temporalColumnA._ImgOrder = this.Columns[posicionA]._ImgOrder;
        temporalColumnA.GridParent = this.Columns[posicionA].GridParent;
        temporalColumnA.ColumEdit = this.Columns[posicionA].ColumEdit;
        temporalColumnA._InsertActiveEditor = this.Columns[posicionA]._InsertActiveEditor;
        temporalColumnA._UpdateActiveEditor = this.Columns[posicionA]._UpdateActiveEditor;
        temporalColumnA.DataBuildCustomCell = this.Columns[posicionA].DataBuildCustomCell;
        temporalColumnA._CustomDrawCell = this.Columns[posicionA]._CustomDrawCell;
        temporalColumnA._ToolBarButton = this.Columns[posicionA]._ToolBarButton;
        temporalColumnA._Visible = this.Columns[posicionA]._Visible;
        temporalColumnA._VisibleVertical = this.Columns[posicionA]._VisibleVertical;
        temporalColumnA._Visible_IsShowValue = this.Columns[posicionA]._Visible_IsShowValue;
        temporalColumnA._VisibleVertical_IsShowValue = this.Columns[posicionA]._VisibleVertical_IsShowValue;

        var temporalColumnB = new TVclColumnCF();
        temporalColumnB.IsOption = this.Columns[posicionB].IsOption;
        temporalColumnB.IsOptionCheck = this.Columns[posicionB].IsOptionCheck;
        temporalColumnB._ActiveEditor = this.Columns[posicionB]._ActiveEditor;
        temporalColumnB._Name = this.Columns[posicionB]._Name;
        temporalColumnB._Index = this.Columns[posicionB]._Index;
        temporalColumnB.IndexDataSourceDefault = this.Columns[posicionB].IndexDataSourceDefault;
        temporalColumnB.IsColumnSelectOrder = this.Columns[posicionB].IsColumnSelectOrder;
        temporalColumnB._Caption = this.Columns[posicionB]._Caption;
        temporalColumnB._EnabledEditor = this.Columns[posicionB]._EnabledEditor;
        temporalColumnB._DataType = this.Columns[posicionB]._DataType;
        temporalColumnB._ColumnStyle = this.Columns[posicionB]._ColumnStyle;
        temporalColumnB._Size = this.Columns[posicionB]._Size;
        temporalColumnB.OnRefreshColumn = this.Columns[posicionB].OnRefreshColumn;
        temporalColumnB.DataEventControl = this.Columns[posicionB].DataEventControl;
        temporalColumnB.onEventControl = this.Columns[posicionB].onEventControl;
        temporalColumnB._FileType = this.Columns[posicionB]._FileType;
        temporalColumnB._FileSRVEvent = this.Columns[posicionB]._FileSRVEvent;
        temporalColumnB.OptionProgressBar = this.Columns[posicionB].OptionProgressBar;


        temporalColumnB.onEventFilterColumn = this.Columns[posicionB].onEventFilterColumn;
        temporalColumnB._ColumEdit = this.Columns[posicionB]._ColumEdit;
        temporalColumnB._ImgOrder = this.Columns[posicionB]._ImgOrder;
        temporalColumnB.GridParent = this.Columns[posicionB].GridParent;
        temporalColumnB.ColumEdit = this.Columns[posicionB].ColumEdit;
        temporalColumnB._InsertActiveEditor = this.Columns[posicionB]._InsertActiveEditor;
        temporalColumnB._UpdateActiveEditor = this.Columns[posicionB]._UpdateActiveEditor;
        temporalColumnB.DataBuildCustomCell = this.Columns[posicionB].DataBuildCustomCell;
        temporalColumnB._CustomDrawCell = this.Columns[posicionB]._CustomDrawCell;
        temporalColumnB._ToolBarButton = this.Columns[posicionB]._ToolBarButton;
        temporalColumnB._Visible = this.Columns[posicionB]._Visible;
        temporalColumnB._VisibleVertical = this.Columns[posicionB]._VisibleVertical;

        temporalColumnB._Visible_IsShowValue = this.Columns[posicionB]._Visible_IsShowValue;
        temporalColumnB._VisibleVertical_IsShowValue = this.Columns[posicionB]._VisibleVertical_IsShowValue;

        temporalColumnA.Name = this.Columns[posicionB].Name;
        temporalColumnB.Name = this.Columns[posicionA].Name;
        temporalColumnA.CaptionOrder = temporalCaptionColumnValueB;
        temporalColumnB.CaptionOrder = temporalCaptionColumnValueA;
        temporalColumnA.Index = posicionB;
        temporalColumnB.Index = posicionA;
        this.Columns[posicionA].IsOption = temporalColumnB.IsOption;
        this.Columns[posicionA].IsOptionCheck = temporalColumnB.IsOptionCheck;
        this.Columns[posicionA]._ActiveEditor = temporalColumnB._ActiveEditor;
        this.Columns[posicionA]._Name = temporalColumnB._Name;
        this.Columns[posicionA]._Index = temporalColumnB._Index;
        this.Columns[posicionA].IndexDataSourceDefault = temporalColumnB.IndexDataSourceDefault;
        this.Columns[posicionA].IsColumnSelectOrder = temporalColumnB.IsColumnSelectOrder;
        this.Columns[posicionA]._Caption = temporalColumnB._Caption;
        this.Columns[posicionA]._EnabledEditor = temporalColumnB._EnabledEditor;
        this.Columns[posicionA]._DataType = temporalColumnB._DataType;
        this.Columns[posicionA]._ColumnStyle = temporalColumnB._ColumnStyle;
        this.Columns[posicionA]._Size = temporalColumnB._Size;
        this.Columns[posicionA].OnRefreshColumn = temporalColumnB.OnRefreshColumn;

        this.Columns[posicionA].DataEventControl = temporalColumnB.DataEventControl;
        this.Columns[posicionA].onEventControl = temporalColumnB.onEventControl;
        this.Columns[posicionA]._FileType = temporalColumnB._FileType;
        this.Columns[posicionA]._FileSRVEvent = temporalColumnB._FileSRVEvent;
        this.Columns[posicionA].OptionProgressBar = temporalColumnB.OptionProgressBar;

        this.Columns[posicionA].onEventFilterColumn = temporalColumnB.onEventFilterColumn;
        this.Columns[posicionA]._ColumEdit = temporalColumnB._ColumEdit;
        this.Columns[posicionA]._ImgOrder = temporalColumnB._ImgOrder;
        this.Columns[posicionA].GridParent = temporalColumnB.GridParent;
        this.Columns[posicionA].ColumEdit = temporalColumnB.ColumEdit;
        this.Columns[posicionA]._InsertActiveEditor = temporalColumnB._InsertActiveEditor;
        this.Columns[posicionA]._UpdateActiveEditor = temporalColumnB._UpdateActiveEditor;
        this.Columns[posicionA].DataBuildCustomCell = temporalColumnB.DataBuildCustomCell;
        this.Columns[posicionA]._CustomDrawCell = temporalColumnB._CustomDrawCell;
        this.Columns[posicionA]._Visible = temporalColumnB._Visible;
        this.Columns[posicionA]._VisibleVertical = temporalColumnB._VisibleVertical;

        this.Columns[posicionA]._Visible_IsShowValue = temporalColumnB._Visible_IsShowValue;
        this.Columns[posicionA]._VisibleVertical_IsShowValue = temporalColumnB._VisibleVertical_IsShowValue;

        this.Columns[posicionA]._ToolBarButton = temporalColumnB._ToolBarButton;
        this.Columns[posicionB].IsOption = temporalColumnA.IsOption;
        this.Columns[posicionB].IsOptionCheck = temporalColumnA.IsOptionCheck;
        this.Columns[posicionB]._ActiveEditor = temporalColumnA._ActiveEditor;
        this.Columns[posicionB]._Name = temporalColumnA._Name;
        this.Columns[posicionB]._Index = temporalColumnA._Index;
        this.Columns[posicionB].IndexDataSourceDefault = temporalColumnA.IndexDataSourceDefault;
        this.Columns[posicionB].IsColumnSelectOrder = temporalColumnA.IsColumnSelectOrder;
        this.Columns[posicionB].Caption = temporalCaptionColumnValueA;
        this.Columns[posicionB]._EnabledEditor = temporalColumnA._EnabledEditor;
        this.Columns[posicionB]._DataType = temporalColumnA._DataType;
        this.Columns[posicionB]._ColumnStyle = temporalColumnA._ColumnStyle;
        this.Columns[posicionB]._Size = temporalColumnA._Size;
        this.Columns[posicionB].OnRefreshColumn = temporalColumnA.OnRefreshColumn;

        this.Columns[posicionB].DataEventControl = temporalColumnA.DataEventControl;
        this.Columns[posicionB].onEventControl = temporalColumnA.onEventControl;
        this.Columns[posicionB]._FileType = temporalColumnA._FileType;
        this.Columns[posicionB]._FileSRVEvent = temporalColumnA._FileSRVEvent;
        this.Columns[posicionB].OptionProgressBar = temporalColumnA.OptionProgressBar;

        this.Columns[posicionB].onEventFilterColumn = temporalColumnA.onEventFilterColumn;
        this.Columns[posicionB]._ColumEdit = temporalColumnA._ColumEdit;
        this.Columns[posicionB]._ImgOrder = temporalColumnA._ImgOrder;
        this.Columns[posicionB].GridParent = temporalColumnA.GridParent;
        this.Columns[posicionB].ColumEdit = temporalColumnA.ColumEdit;
        this.Columns[posicionB]._InsertActiveEditor = temporalColumnA._InsertActiveEditor;
        this.Columns[posicionB]._UpdateActiveEditor = temporalColumnA._UpdateActiveEditor;
        this.Columns[posicionB].DataBuildCustomCell = temporalColumnA.DataBuildCustomCell;
        this.Columns[posicionB]._CustomDrawCell = temporalColumnA._CustomDrawCell;
        this.Columns[posicionB]._Visible = temporalColumnA._Visible;
        this.Columns[posicionB]._VisibleVertical = temporalColumnA._VisibleVertical;

        this.Columns[posicionB]._Visible_IsShowValue = temporalColumnA._Visible_IsShowValue;
        this.Columns[posicionB]._VisibleVertical_IsShowValue = temporalColumnA._VisibleVertical_IsShowValue;

        this.Columns[posicionB]._ToolBarButton = temporalColumnA._ToolBarButton;
        this.Columns[posicionA].CaptionOrder = temporalCaptionColumnValueB;
        this.Columns[posicionB].CaptionOrder = temporalCaptionColumnValueA;
        this.Columns[posicionA]._Name = temporalNameColumnValueB;
        this.Columns[posicionB]._Name = temporalNameColumnValueA;
        if (this.Rows.length > 0) {
            for (var i = 0, t = this.Rows.length; i < t; i++) {
                if (this.Rows[i].RowHijos.length == 0) {
                    var temporalCeldaValueA = this.Rows[i].Cells[posicionA]._Value;
                    var temporalCeldaValueB = this.Rows[i].Cells[posicionB]._Value;
                    var temporalCeldaACellColor = this.Rows[i].Cells[posicionA].CellColor;
                    var temporalCeldaBCellColor = this.Rows[i].Cells[posicionB].CellColor;
                    this.Rows[i].Cells[posicionA].RestoreFunctionColor();
                    this.Rows[i].Cells[posicionB].RestoreFunctionColor();
                    this.Rows[i].Cells[posicionA].Column = this.Columns[posicionA];
                    this.Rows[i].Cells[posicionB].Column = this.Columns[posicionB];
                    this.Rows[i].Cells[posicionA]._Value = temporalCeldaValueB;
                    this.Rows[i].Cells[posicionB]._Value = temporalCeldaValueA;
                    this.Rows[i].Cells[posicionA].CellColor = temporalCeldaBCellColor;
                    this.Rows[i].Cells[posicionB].CellColor = temporalCeldaACellColor;
                    
                    this.Rows[i].Cells[posicionA].InitCell();
                    this.Rows[i].Cells[posicionB].InitCell();
                }
            }
        }

        if (this._FocusedRowHandle != null) {
            this._FocusedRowHandle.This.style.backgroundColor = "lightyellow";
            for (var i = 0, t = this.Columns.length; i < t; i++) {
                this._FocusedRowHandle.Cells[i].RestoreFunctionColor();
            }

        }
    }
    this.AddColumn = function (inColumn) {
        //inColumn.This.style.lineHeight = 0;
        inColumn.GridParent = this;
        var _indexTemp = (this._Columns.length > 0) ? (this._Columns.length - 1) : 0;
        var _ColTemp = this._Columns[_indexTemp];

        if (this.ExistOption) {
            inColumn.Index = _indexTemp;
            this._Columns.splice(_indexTemp, 0, inColumn);
            $(inColumn.This).insertBefore(_ColTemp.This);
            this._Columns[this._Columns.length - 1].Index = this._Columns.length - 1;

        }
        else {
            this._Columns.push(inColumn);
            //$(this.RowHeader).append(inColumn.This);
            this.RowHeader.appendChild(inColumn.This);
        }
        this.ExistOption = inColumn.IsOption;
        if (this.Rows.length > 0) {
            for (var i = 0; i < this.Rows.length; i++) {
                var Cell0 = new TVclCellCF();
                Cell0.IndexColumn = inColumn.Index;
                Cell0.Column = inColumn;
                //                Cell0.IndexRow = this.Rows[i].IndexRow;
                Cell0.IndexRow = this.Rows[i].Index;
                Cell0.InitCell();
                Cell0.Visible = Cell0.Column.Visible;
                if (this.ExistOption)
                    this.Rows[i].InsertCell(Cell0, _indexTemp);
                else
                    this.Rows[i].AddCell(Cell0);
            }
        }

        //inColumn.This.style.verticalAlign = "top";
    }
    this.showHideColumn = function (indexColumn, value) {

        if (this.Columns[indexColumn].Visible != value) {
            this.Columns[indexColumn].Visible = value;

            if (this.Rows != null && this._Rows.length > 0) {
                for (var i = 0; i < this._Rows.length; i++) {
                    if (this.Rows[i].RowHijos.length == 0) {
                        this.Rows[i].Cells[indexColumn].Visible = value;
                    }


                }
            }
        }

    }
    this.showHideRow = function (indexRow, value) {
        if (this.Rows[indexRow] != null) {
            this.Rows[indexRow].Visible = value;
        }
    }
    this.showHideRowAll = function (value) {
        if (this.Rows.length > 0) {
            for (var i = 0; i < this.Rows.length; i++) {
                this.Rows[i].Visible = value;
            }
        }

    }
    this.AddRowInsert = function (inRowOld, inRow) {

        if (this._Rows.length > 0) {
            inRow.Index = this._Rows.length;
            this._Rows.push(inRow);
            inRow.GridParent = this;
            for (var i = 0; i < this._Columns.length; i++) {
                inRow.Cells[i].Column = this._Columns[i];
                inRow.Cells[i].InitCell();
                inRow.Cells[i].Visible = inRow.Cells[i].Column.Visible;

            }
        } else {
            this.AddRow(inRow);
        }

        var parentNode = inRowOld.This.parentNode;
        parentNode.insertBefore(inRow.This, inRowOld.This);
    }
    this.AddRow = function (inRow) {
        if (this._Rows.length == 0)
            this.FocusedRowHandle = inRow;
        inRow.Index = this._Rows.length;
        this._Rows.push(inRow);
        inRow.GridParent = this;
        for (var i = 0, t = this._Columns.length; i < t; i++) {
            inRow.Cells[i].Column = this._Columns[i];
            //inRow.Cells[i].This.setAttribute('data-title', this._Columns[i].Caption); //atributo y valor
            inRow.Cells[i].InitCell();
            inRow.Cells[i].Visible = inRow.Cells[i].Column.Visible;

        }
        //$(this.Body).append(inRow.This).fadeIn(500);
        //.fadeIn(3000);
        this.Body.appendChild(inRow.This);

    }
    this.AddRowTreeGrid = function (inRow) {
        if (this._Rows.length == 0)
            this.FocusedRowHandle = inRow;
        inRow.Index = this._Rows.length;
        this._Rows.push(inRow);
        inRow.GridParent = this;
        this.Body.appendChild(inRow.This);
    }
    this.AddRowInfo = function (inRow) {
        if (this._Rows.length == 0)
            this.FocusedRowHandle = inRow;
        inRow.Index = this._Rows.length;
        this._Rows.push(inRow);
        inRow.GridParent = this;
        for (var i = 0, t = inRow.Cells.length; i < t; i++) {
            inRow.Cells[i].InitCell();
        }
        this.Body.appendChild(inRow.This);
    }
    this.AddRowAfter = function (opcionInsercion, inRow, RowReference, colspan) {
        inRow.GridParent = this;
        if (opcionInsercion == 0) {
            inRow.Cells[0].This.colSpan = "" + colspan;
            this.Body.appendChild(inRow.This);

        } else if (opcionInsercion == 1) {
            inRow.Cells[0].This.colSpan = "" + colspan;
            RowReference.This.parentNode.insertBefore(inRow.This, RowReference.This.nextSibling);

        } else if (opcionInsercion == 2) {

            for (var i = 0, t = this._Columns.length; i < t; i++) {
                inRow.Cells[i].Column = this._Columns[i];
                inRow.Cells[i].InitCell();
                inRow.Cells[i].Visible = inRow.Cells[i].Column.Visible;
            }
            RowReference.This.parentNode.insertBefore(inRow.This, RowReference.This.nextSibling);
        }
        var index = $(inRow.This).index();
        inRow.Index = index;
        this._Rows.splice(index, 0, inRow);
    }
    this.AddRowBefore = function (reOrder, inRow, RowReference) {
        this._Rows.splice(RowReference.Index, 0, inRow);
        RowReference.GridParent = this;
        inRow.GridParent = this;
        for (var i = 0, t = this._Columns.length; i < t; i++) {
            inRow.Cells[i].Column = this._Columns[i];
            //inRow.Cells[i].This.setAttribute('data-title', this._Columns[i].Caption); //atributo y valor
            inRow.Cells[i].InitCell();
            var temRow = inRow.Cells[i].This;
            temRow.style.maxWidth = 100 + "px";
            temRow.style.overflow = "hidden";
            temRow.style.textOverflow = "ellipsis";
            temRow.style.whiteSpace = "nowrap";
            inRow.Cells[i].Visible = inRow.Cells[i].Column.Visible;
        }
        RowReference.This.parentNode.insertBefore(inRow.This, RowReference.This);
        if (reOrder) {
            for (var i = RowReference.Index, t = this._Rows.length; i < t; i++) {
                this._Rows[i].Index = i;
            }
        }
    }
    this.UpdatesRow = function (inRow, inRowOld) {
        inRow.GridParent = this;

        if (inRowOld.RowPadre != null) {

            inRowOld.RowPadre.RowHijos.push(inRow);
            inRow.RowPadre = inRowOld.RowPadre;
        }

        for (var i = 0, t = this._Columns.length; i < t; i++) {
            inRow.Cells[i].Column = this._Columns[i];
            //inRow.Cells[i].This.setAttribute('data-title', this._Columns[i].Caption); //atributo y valor

            inRow.Cells[i].InitCell();
            inRow.Cells[i].Visible = inRow.Cells[i].Column.Visible;
            //if (!this.IsMobile) {
            if (this._Columns[i]._ColumnStyle != Componet.Properties.TExtrafields_ColumnStyle.LookUpOption &&
                this._Columns[i]._ColumnStyle != Componet.Properties.TExtrafields_ColumnStyle.LookUp
            ) {
                inRow.Cells[i].This.style.maxWidth = 100 + "px";
                inRow.Cells[i].This.style.overflow = "hidden";
                inRow.Cells[i].This.style.textOverflow = "ellipsis";
                inRow.Cells[i].This.style.whiteSpace = "nowrap";
            }

            //}
            //else {
            //if (inRow.Cells[i].Column.IsOption || inRow.Cells[i].Column.IsOptionCheck) {
            //    inRow.Cells[i].This.setAttribute('data-title', ""); //atributo y valor
            //    inRow.Cells[i].This.style.paddingLeft = "1%";
            //}
            //}
        }
        inRow.Visible = inRowOld.Visible;
        this._Rows[inRowOld.Index] = inRow;



        this.Body.replaceChild(inRow.This, inRowOld.This);
    }
    this.RemoveRow = function (Index) {
        this._Rows[Index].This.parentNode.removeChild(this._Rows[Index].This);
        this._Rows.splice(Index, 1);
    }
    this.RemoveFocusedRowHandle = function () {
        this._FocusedRowHandle.This.parentNode.removeChild(this._FocusedRowHandle.This);
        this._Rows.splice(this._FocusedRowHandle.Index, 1);
    }

    this.ClearAllColumns = function () {
        this._Columns = new Array();
        this.Order = "";
        this._ExistOption = false;
        $(this.RowHeader).html("");
    }

    this.ClearAllRows = function () {
        this._Rows = new Array();
        this._FocusedRowHandle = null;
        $(this.Body).html("");
    }

    this.ClearAll = function () {
        this.ClearAllRows();
        this.ClearAllColumns();
    }
    this.getGridFormatJSON = function (arrayIndexColumn) {
        var array = [];
        var arrayColumns = [];
        var indexColumnOption = null;
        var listColumn = [];
        if (arrayIndexColumn === undefined) {
            listColumn = this.Columns;
        }
        else {
            for (var j = 0, t = arrayIndexColumn.length; j < t; j++) {
                if (this.Columns[arrayIndexColumn[j]].IsOption == false && this.Columns[arrayIndexColumn[j]].IsOptionCheck == false) {
                    listColumn.push(this.Columns[arrayIndexColumn[j]]);
                }
            }
        }
        if (listColumn != null) {
            for (var j = 0, t = listColumn.length; j < t; j++) {
                arrayColumns.push(listColumn[j].Name);
            }
        }
        if (this.Rows != null && listColumn != null) {
            for (var i = 0, t = this.Rows.length; i < t; i++) {
                var data = [];
                for (var j = 0, t1 = listColumn.length; j < t1; j++) {
                    data.push(this.Rows[i].Cells[listColumn[j].Index].Value);
                }
                array.push(data);
            }
        }
        return { rows: array, columns: arrayColumns };
    }

    this.ContracAllColumns = function () {
        //if (!this.IsMobile) {
        var size = "100";
        for (var i = 0, t = this._Columns.length; i < t; i++) {

            if (size != "") {
                if (this._Columns[i]._ColumnStyle != Componet.Properties.TExtrafields_ColumnStyle.LookUpOption &&
                    this._Columns[i]._ColumnStyle != Componet.Properties.TExtrafields_ColumnStyle.LookUp
                ) {
                    this._Columns[i].Width = size + "px";
                    for (var j = 0; j < this.Rows.length > 0; j++) {
                        if (this.Rows[j]._ImgGruopDown == null) {

                            var temRow = this.Rows[j].Cells[i].This;

                            temRow.style.maxWidth = size + "px";
                            temRow.style.overflow = "hidden";
                            temRow.style.textOverflow = "ellipsis";
                            temRow.style.whiteSpace = "nowrap";
                        }

                    }
                }


            }
        }
        //}


    }
    this.ContracAllColumnsTree = function () {
        var size = "100";
        for (var i = 0, t = this._Columns.length; i < t; i++) {

            if (size != "") {

                this._Columns[i].Width = size + "px";
                for (var j = 0; j < this.Rows.length > 0; j++) {
                    var temRow = this.Rows[j].Cells[i].This;
                    temRow.style.maxWidth = size + "px";
                    temRow.style.overflow = "hidden";
                    temRow.style.textOverflow = "ellipsis";
                    temRow.style.whiteSpace = "nowrap";
                }

            }
        }
    }


    this.ExpandAllColumns = function () {
        //if (!this.IsMobile) {
        var size = "600";
        for (var i = 0, t = this._Columns.length; i < t; i++) {

            if (size != "") {

                this._Columns[i].Width = size + "px";
                for (var j = 0; j < this.Rows.length > 0; j++) {
                    var temRow = this.Rows[j].Cells[i].This;
                    temRow.style.maxWidth = size + "px";
                    temRow.style.overflow = "hidden";
                    temRow.style.textOverflow = "ellipsis";
                    temRow.style.whiteSpace = "nowrap";
                }

            }
        }
        //}

    }
    this.ExpandAllColumnsTree = function () {

        var size = "200";
        for (var i = 0, t = this._Columns.length; i < t; i++) {

            if (size != "") {

                this._Columns[i].Width = size + "px";
                for (var j = 0; j < this.Rows.length > 0; j++) {
                    var temRow = this.Rows[j].Cells[i].This;
                    temRow.style.maxWidth = size + "px";
                    temRow.style.overflow = "hidden";
                    temRow.style.textOverflow = "ellipsis";
                    temRow.style.whiteSpace = "nowrap";
                }

            }
        }
    }
    this._ColumnCheckVisible = true;
    Object.defineProperty(this, 'ColumnCheckVisible', {
        get: function () {
            return this._ColumnCheckVisible;
        },
        set: function (inValue) {
            this._ColumnCheckVisible = inValue;
            this.showHideColumn(this.GetColumn("OptionButtonCkeck").Index, inValue);
        }
    });

    this.SetSelectedRows = function (inValue) {

        for (var i = 0, t = this._Rows.length; i < t; i++) {
            if (this.Rows[i].RowHijos.length == 0) {
                if (this.Rows[i].Cells[0].Column.IsOptionCheck) {
                    this.Rows[i].Cells[0].Control.Checked = inValue;
                }
            }

        }

    }

    this.GetSelectedRows = function () {
        var ListaRows = new Array();
        for (var i = 0, t = this._Rows.length; i < t; i++) {
            if (this.Rows[i].RowHijos.length == 0) {
                if (this.Rows[i].GetCellValue("OptionButtonCkeck")) {
                    ListaRows.push(this.Rows[i]);
                }
            }

        }
        return ListaRows;
    }

    this.GetUnSelectedRows = function () {
        var ListaRows = new Array();
        for (var i = 0, t = this.Rows.length; i < t; i++) {
            if (this.Rows[i].RowHijos.length == 0) {
                if (!this.Rows[i].GetCellValue("OptionButtonCkeck")) {
                    ListaRows.push(this.Rows[i]);
                }
            }

        }
        return ListaRows;
    }

    this.GetColumn = function (NameColumn) {
        for (var i = 0, t = this.Columns.length; i < t; i++) {
            if (this.Columns[i].Name.toUpperCase() === NameColumn.toUpperCase()) {
                return this.Columns[i];
            }
        }
        return null;
    }

    this.showHideChild = function (inRow, option) {
        for (var i = 0, t = inRow.RowHijos.length; i < t; i++) {
            inRow.RowHijos[i].showHideChild(option);

        }
    }

}

var TVclColumnCF = function () {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.IndexDataSourceDefault = null;
    this.IsOption = false;
    this.IsOptionCheck = false;
    this._ActiveEditor = true;
    this._Name;
    this._Index;
    this._Caption;
    this._EnabledEditor = false;
    this._DataType = SysCfg.DB.Properties.TDataType.Unknown;
    this._ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.None;
    this._Size = 0;
    this.This = Uth("", "");
    //this.This.style.lineHeight = "13px";
    this.OnRefreshColumn = null;
    this.DataEventControl = null;
    this.onEventControl = null;
    this._FileSRVEvent = null;
    this._FileType = null;
    this.onEventFilterColumn = null;
    this._ColumEdit = null;
    this._ImgOrder = null;
    this.GridParent = null;
    this.ColumEdit = function (inStyle, inEventControl) {
        this.ColumnStyle = inStyle
        this.onEventControl = inEventControl;
        if (this.GridParent != null && this.GridParent.Rows.length > 0) {
            for (var i = 0, t = this.GridParent.Rows.length; i < t; i++) {
                this.GridParent.Rows[i].Cells[this.Index].InitCell();
            }
        }
    }
    this.GetWidtText = function (text, fontName, fontSize) {
        $('body').append('<span style="display:none" id="contentText">' + text + '</span>');//Creamos un span oculto
        var widthCadena = $('#contentText').width();
        $('#contentText').remove();
        return widthCadena;
    }
    this.WordWra = function (text, separador) {
        var WidthCol = 90;

        var ActualWidth = this.GetWidtText(text, null, null);
        //alert(WidthCol + " " + ActualWidth);
        if (ActualWidth <= WidthCol) {
            return text;
        }
        var resp = "";
        var tmp = "";
        var listado = new Array();
        var lista = text.split(separador);//*
        for (var i = 0, t = lista.length; i < t; i++) {

            tmp += lista[i] + " ";

            var wp = {
                parte1: tmp,
                ancho1: this.GetWidtText(tmp, null, null),
                parte2: "",
                ancho2: "",
                resta: 0,
            };

            for (var j = i + 1, t1 = lista.length; j < t1; j++) {
                wp.parte2 += lista[j] + " ";
            }
            //textBlock.Text = wp.Parte2;
            wp.ancho2 = this.GetWidtText(wp.parte2, null, null);
            listado.push(wp);
        }
        var minimo = 500;
        var elegido = null;

        for (var i = 0, t = listado.length; i < t; i++) {
            if (listado[i].ancho1 <= WidthCol) {
                elegido = listado[i];
            }
        }
        var resp = text;
        if (elegido != null) {
            resp = elegido.parte1.trim() + "<br>" + this.WordWra(elegido.parte2.trim(), " ");
        }
        return resp;
    }
    this._CustomDrawCell = null;
    this._InsertActiveEditor = false;
    this._UpdateActiveEditor = false;
    this.IsColumnSelectOrder = null;
    this.DataBuildCustomCell = null;
    this._ToolBarButton = {
        InsertActive: true,
        UpdateActive: true,
        DeleteActive: true,
    };
    this.inOnclick = false;
    this.selectAll = null;
    this._CaptionHorizontal = true;
    this.ControlFilter = null;
    this.onEventFilter = null;
    this.UimgLocal = function (Object, id, Src, alt) {
        var Res = undefined;
        var FullSrc = SysCfg.App.Properties.xRaiz + Src;
        var img = document.createElement('img');
        img.id = id
        img.src = FullSrc;
        img.alt = alt;
        Res = img;
        Object.appendChild(img)
        return Res;
    }
    //this.VclStackPanelField = null;
    this.CreateDialogControlFilter = function (type) {

        if (type == 1) {
            var divConten = document.createElement("div");
            var div1 = document.createElement("div");
            var div2 = document.createElement("div");
            var div3 = document.createElement("div");
            var div4 = document.createElement("div");
            divConten.appendChild(div1);
            divConten.appendChild(div2);
            divConten.appendChild(div3);
            divConten.appendChild(div4);
            div3.style.textAlign = "right";

            div2.appendChild(this.ControlFilter.This);
            this.ControlFilter.This.classList.add("form-control");
            this.ControlFilter.This.placeholder = "Filter..."
            var CellButton = document.createElement("button");
            CellButton.style.width = "100%";
            div3.appendChild(CellButton);
            var iconButton = document.createElement("i");
            var spanButton = document.createElement("span");
            iconButton.classList.add("icon");
            iconButton.classList.add("ion-search");
            spanButton.innerHTML = " " + "Search";
            CellButton.appendChild(iconButton);
            CellButton.appendChild(spanButton);
            CellButton.onclick = function () {

                if (_this.onEventFilter != null) {
                    spanButton.innerHTML = "";
                    CellButton.disabled = true;
                    spanButton.innerHTML = " Loading...";
                    setTimeout(function () {
                        var value = _this.ControlFilter.This.value;
                        _this.ControlFilter.This.style.color = "#337ab7";
                        var count = _this.onEventFilter(_this, value);
                        spanButton.innerHTML = "";
                        CellButton.disabled = false;
                        spanButton.innerHTML = " " + "Search";
                        SpanInfo.style.display = "";
                        SpanInfo.innerHTML = "";
                        SpanInfo.innerHTML = "Records: " + count;
                        if (count > 0) {
                            SpanInfo.style.backgroundColor = "#f0ad4e";
                        } else {
                            SpanInfo.style.backgroundColor = "#d9534f";
                        }

                    }, 0);


                }
            }

            CellButton.style.marginTop = "8px";
            CellButton.classList.add("btn");
            CellButton.classList.add("btn-info");
            var SpanInfo = document.createElement("span");
            div4.style.marginTop = "8px";
            //SpanInfo.style.opacity = "0.9";
            SpanInfo.classList.add("label");
            SpanInfo.classList.add("label-success");
            SpanInfo.style.display = "none";
            div4.style.textAlign = "right";
            div4.appendChild(SpanInfo);

            var modalFV = new Componet.GridCF.TGridCFView.TModalFV(divConten, _this._ImgFieldFilter);
            this.ControlFilter.This.focus();
            this.ControlFilter.onKeyPress = function () {
                SpanInfo.style.display = "none";
                _this.ControlFilter.This.style.color = "#555";
            }
            this.ControlFilter.onKeyUp = function (e) {
                SpanInfo.style.display = "none";
                _this.ControlFilter.This.style.color = "#555";
                if (e.keyCode == 13) {
                    CellButton.click();
                    // Do something
                }

            }
        }
        else if (type == 2) {
            var divConten = document.createElement("div");
            var div1 = document.createElement("div");
            var div2 = document.createElement("div");
            var div3 = document.createElement("div");
            divConten.appendChild(div1);
            divConten.appendChild(div2);
            divConten.appendChild(div3);
            var CellButtonTrue = document.createElement("button");
            div3.appendChild(CellButtonTrue);
            var iconButton = document.createElement("i");
            var spanButton = document.createElement("span");
            iconButton.classList.add("icon");
            iconButton.classList.add("ion-ios-circle-outline");
            spanButton.innerHTML = " " + "True";
            CellButtonTrue.appendChild(iconButton);
            CellButtonTrue.appendChild(spanButton);
            CellButtonTrue.onclick = function () {
                CellButtonTrue.disabled = true;
                CellButtonFalse.disabled = false;

                _this.onEventFilter(_this, true);
            }
            CellButtonTrue.classList.add("btn");
            CellButtonTrue.classList.add("btn-success");

            var CellButtonFalse = document.createElement("button");
            div3.appendChild(CellButtonFalse);

            var spanButtonFalse = document.createElement("span");
            spanButtonFalse.innerHTML = " " + "False";
            CellButtonFalse.appendChild(spanButtonFalse);

            CellButtonFalse.onclick = function () {
                CellButtonTrue.disabled = false;
                CellButtonFalse.disabled = true;
                _this.onEventFilter(_this, false);
            }
            CellButtonFalse.classList.add("btn");
            CellButtonFalse.classList.add("btn-default");
            CellButtonTrue.style.borderTopRightRadius = "0px";
            CellButtonTrue.style.borderBottomRightRadius = "0px";
            CellButtonFalse.style.borderTopLeftRadius = "0px";
            CellButtonFalse.style.borderBottomLeftRadius = "0px";



            var modalFV = new Componet.GridCF.TGridCFView.TModalFV(divConten, _this._ImgFieldFilter);
        }
        else if (type == 3) {

            var divConten = document.createElement("div");
            var divContenA = document.createElement("div");
            var divContenB = document.createElement("div");
            divConten.appendChild(divContenA);
            divConten.appendChild(divContenB);
            var label1 = document.createElement("label");
            var label2 = document.createElement("label");
            label1.innerHTML = "From:";
            label2.innerHTML = "To:";
            var div1 = document.createElement("div");
            var div2 = document.createElement("div");
            var div3 = document.createElement("div");
            divContenA.appendChild(div1);
            divContenA.appendChild(div2);
            divContenA.appendChild(div3);
            div2.style.marginTop = "5px";
            div1.appendChild(label1);
            div1.appendChild(this.ControlFilterDateStart.This);

            div2.appendChild(label2);
            div2.appendChild(this.ControlFilterDateEnd.This);
            this.ControlFilterDateStart.This.classList.add("form-control");
            this.ControlFilterDateEnd.This.classList.add("form-control");
            var CellButton = document.createElement("button");
            CellButton.style.width = "100%";
            div3.appendChild(CellButton);
            var iconButton = document.createElement("i");
            var spanButton = document.createElement("span");
            iconButton.classList.add("icon");
            iconButton.classList.add("ion-search");
            spanButton.innerHTML = " " + "Search";
            CellButton.appendChild(iconButton);
            CellButton.appendChild(spanButton);
            CellButton.onclick = function () {

                if (_this.onEventFilter != null) {
                    spanButton.innerHTML = "";
                    CellButton.disabled = true;
                    spanButton.innerHTML = " Loading...";
                    setTimeout(function () {

                        var valorStart = $(_this.ControlFilterDateStart.This).val();
                        var valorEnd = $(_this.ControlFilterDateEnd.This).val();
                        _this.tempDate.dateStart = valorStart;
                        _this.tempDate.dateEnd = valorEnd;
                        _this.onEventFilter(_this, _this.tempDate);
                        _this.ControlFilterDateStart.This.color = "rgb(43, 86, 154)";
                        _this.ControlFilterDateEnd.This.color = "rgb(43, 86, 154)";
                        spanButton.innerHTML = "";
                        CellButton.disabled = false;
                        spanButton.innerHTML = " " + "Search";

                    }, 0);


                }

            }

            CellButton.style.marginTop = "8px";
            CellButton.classList.add("btn");
            CellButton.classList.add("btn-info");
            divContenB.style.marginTop = "8px";
            this.VclComboBox2DivitionColumns = new TVclComboBox2(divContenB, "");
            var listRange = ["Range", "To Day", "Month", "Year"];
            for (var i = 0; i < listRange.length; i++) {
                var VclComboBoxItem = new TVclComboBoxItem();
                VclComboBoxItem.Value = i;
                VclComboBoxItem.Text = "&nbsp;&nbsp;&nbsp;" + listRange[i];
                VclComboBoxItem.Tag = i;
                _this.VclComboBox2DivitionColumns.AddItem(VclComboBoxItem);
                
            }
            _this.VclComboBox2DivitionColumns.onChange = function () {
                var opcion = _this.VclComboBox2DivitionColumns.Value;
                if (_this.onEventFilter != null) {
                    
                    spanButton.innerHTML = "";
                    CellButton.disabled = true;
                    spanButton.innerHTML = " Loading...";
                    setTimeout(function () {
                        if (opcion != 0) {
                            if (opcion == 1) {
                                var date = new Date();
                                var date2 = new Date();
                                date.setHours(0);
                                date.setMinutes(0);
                                date.setSeconds(0);
                                var valorStart = date;
                                date2.setHours(23);
                                date2.setMinutes(59);
                                date2.setSeconds(0);
                                var valorEnd = date2;
                                
                            }
                            if (opcion == 2) {
                                var date = new Date();
                                var valorStart = new Date(date.getFullYear(), date.getMonth(), 1);
                                var valorEnd = new Date(date.getFullYear(), date.getMonth() + 1, 0);

                            }
                            if (opcion == 3) {
                                var date = new Date();
                                var valorStart = new Date(date.getFullYear(), 1, 1);
                                var valorEnd = new Date(date.getFullYear(), date.getMonth(), date.getDay());
                            }
                            _this.tempDate.dateStart = valorStart;
                            _this.tempDate.dateEnd = valorEnd;
                            _this.onEventFilter(_this, _this.tempDate);
                            _this.ControlFilterDateStart.This.color = "rgb(43, 86, 154)";
                            _this.ControlFilterDateEnd.This.color = "rgb(43, 86, 154)";
                        }
                        spanButton.innerHTML = "";
                        CellButton.disabled = false;
                        spanButton.innerHTML = " " + "Search";

                    }, 0);

                }


            }
            _this.VclComboBox2DivitionColumns.Value = 0;

            _this.VclComboBox2DivitionColumns.This.classList.add("form-control");
            _this.VclComboBox2DivitionColumns.This.style.textAlignLast = "center";
            _this.VclComboBox2DivitionColumns.Value = 0;

            var modalFV = new Componet.GridCF.TGridCFView.TModalFV(divConten, _this._ImgFieldFilter);
            this.ControlFilterDateStart.This.readOnly = true;//Desabilitar Escritura de campo text Time
            this.ControlFilterDateEnd.This.readOnly = true;//Desabilitar Escritura de campo text Time

            this.ControlFilterDateStart.This.placeholder = "Select..";
            this.ControlFilterDateEnd.This.placeholder = "Select..";

            this.tempDate = {
                dateStart: null,
                dateEnd: null
            }
            this.ControlFilterDateStart.onChange = function () {
                _this.ControlFilterDateStart.This.color = "#555";
                _this.ControlFilterDateStart.This.color = "#555";

            }
            this.ControlFilterDateEnd.onChange = function () {
                _this.ControlFilterDateStart.This.color = "#555";
                _this.ControlFilterDateStart.This.color = "#555";


            }
            this.ControlFilterDateStart.This.onclick = function () {
                var e = window.event || arguments.callee.caller.arguments[0];
                e.cancelBubble = true;
                e.returnValue = false;
                if (e.stopPropagation) e.stopPropagation();
                if (e.preventDefault) e.preventDefault();
            }
            this.ControlFilterDateEnd.This.onclick = function () {
                var e = window.event || arguments.callee.caller.arguments[0];
                e.cancelBubble = true;
                e.returnValue = false;
                if (e.stopPropagation) e.stopPropagation();
                if (e.preventDefault) e.preventDefault();
            }
            this.ControlFilterDateStart.This.style.backgroundColor = "#fff";
            this.ControlFilterDateEnd.This.style.backgroundColor = "#fff";

        }
        else if (type == 4) {

            var divConten = document.createElement("div");
            var divContenA = document.createElement("div");
            var divContenB = document.createElement("div");
            divConten.appendChild(divContenA);
            divConten.appendChild(divContenB);
            var label1 = document.createElement("label");
            var label2 = document.createElement("label");
            label1.innerHTML = "From:";
            label2.innerHTML = "To:";
            var div1 = document.createElement("div");
            var div2 = document.createElement("div");
            var div3 = document.createElement("div");
            divContenA.appendChild(div1);
            divContenA.appendChild(div2);
            divContenA.appendChild(div3);
            div2.style.marginTop = "5px";
            div1.appendChild(label1);
            div1.appendChild(this.ControlFilterDateStart.This);

            div2.appendChild(label2);
            div2.appendChild(this.ControlFilterDateEnd.This);
            this.ControlFilterDateStart.This.classList.add("form-control");
            this.ControlFilterDateEnd.This.classList.add("form-control");
            var CellButton = document.createElement("button");
            CellButton.style.width = "100%";
            div3.appendChild(CellButton);
            var iconButton = document.createElement("i");
            var spanButton = document.createElement("span");
            iconButton.classList.add("icon");
            iconButton.classList.add("ion-search");
            spanButton.innerHTML = " " + "Search";
            CellButton.appendChild(iconButton);
            CellButton.appendChild(spanButton);
            CellButton.onclick = function () {

                if (_this.onEventFilter != null) {
                    spanButton.innerHTML = "";
                    CellButton.disabled = true;
                    spanButton.innerHTML = " Loading...";
                    setTimeout(function () {

                        var valorStart = $(_this.ControlFilterDateStart.This).val();
                        var valorEnd = $(_this.ControlFilterDateEnd.This).val();
                        _this.tempDate.dateStart = valorStart;
                        _this.tempDate.dateEnd = valorEnd;
                        _this.onEventFilter(_this, _this.tempDate);
                        _this.ControlFilterDateStart.This.color = "rgb(43, 86, 154)";
                        _this.ControlFilterDateEnd.This.color = "rgb(43, 86, 154)";
                        spanButton.innerHTML = "";
                        CellButton.disabled = false;
                        spanButton.innerHTML = " " + "Search";

                    }, 0);


                }

            }

            CellButton.style.marginTop = "8px";
            CellButton.classList.add("btn");
            CellButton.classList.add("btn-info");
            divContenB.style.marginTop = "8px";
            this.VclComboBox2DivitionColumns = new TVclComboBox2(divContenB, "");
            var listRange = ["Range", "To Day", "Month", "Year"];
            for (var i = 0; i < listRange.length; i++) {
                var VclComboBoxItem = new TVclComboBoxItem();
                VclComboBoxItem.Value = i;
                VclComboBoxItem.Text = "&nbsp;&nbsp;&nbsp;" + listRange[i];
                VclComboBoxItem.Tag = i;
                _this.VclComboBox2DivitionColumns.AddItem(VclComboBoxItem);
            }
            _this.VclComboBox2DivitionColumns.onChange = function () {
                var opcion = _this.VclComboBox2DivitionColumns.Value;
                if (_this.onEventFilter != null) {

                    spanButton.innerHTML = "";
                    CellButton.disabled = true;
                    spanButton.innerHTML = " Loading...";
                    setTimeout(function () {
                        if (opcion != 0) {
                            if (opcion == 1) {
                                var date = new Date();
                                var date2 = new Date();
                                date.setHours(0);
                                date.setMinutes(0);
                                date.setSeconds(0);
                                var valorStart = date;
                                date2.setHours(23);
                                date2.setMinutes(59);
                                date2.setSeconds(0);
                                var valorEnd = date2;
                            }
                            if (opcion == 2) {
                                var date = new Date();
                                var valorStart = new Date(date.getFullYear(), date.getMonth(), 1);
                                var valorEnd = new Date(date.getFullYear(), date.getMonth() + 1, 0);

                            }
                            if (opcion == 3) {
                                var date = new Date();
                                var valorStart = new Date(date.getFullYear(), 1, 1);
                                var valorEnd = new Date(date.getFullYear(), date.getMonth(), date.getDay());
                            }
                            _this.tempDate.dateStart = valorStart;
                            _this.tempDate.dateEnd = valorEnd;
                            _this.onEventFilter(_this, _this.tempDate);
                            _this.ControlFilterDateStart.This.color = "rgb(43, 86, 154)";
                            _this.ControlFilterDateEnd.This.color = "rgb(43, 86, 154)";
                        }
                        spanButton.innerHTML = "";
                        CellButton.disabled = false;
                        spanButton.innerHTML = " " + "Search";

                    }, 0);

                }


            }
            _this.VclComboBox2DivitionColumns.Value = 0;

            _this.VclComboBox2DivitionColumns.This.classList.add("form-control");
            _this.VclComboBox2DivitionColumns.This.style.textAlignLast = "center";
            _this.VclComboBox2DivitionColumns.Value = 0;

            var modalFV = new Componet.GridCF.TGridCFView.TModalFV(divConten, _this._ImgFieldFilter);
            this.ControlFilterDateStart.This.readOnly = true;//Desabilitar Escritura de campo text Time
            this.ControlFilterDateEnd.This.readOnly = true;//Desabilitar Escritura de campo text Time

            this.ControlFilterDateStart.This.placeholder = "Select..";
            this.ControlFilterDateEnd.This.placeholder = "Select..";

            this.tempDate = {
                dateStart: null,
                dateEnd: null
            }
            this.ControlFilterDateStart.onChange = function () {
                _this.ControlFilterDateStart.This.color = "#555";
                _this.ControlFilterDateStart.This.color = "#555";

            }
            this.ControlFilterDateEnd.onChange = function () {
                _this.ControlFilterDateStart.This.color = "#555";
                _this.ControlFilterDateStart.This.color = "#555";


            }
            this.ControlFilterDateStart.This.onclick = function () {
                var e = window.event || arguments.callee.caller.arguments[0];
                e.cancelBubble = true;
                e.returnValue = false;
                if (e.stopPropagation) e.stopPropagation();
                if (e.preventDefault) e.preventDefault();
            }
            this.ControlFilterDateEnd.This.onclick = function () {
                var e = window.event || arguments.callee.caller.arguments[0];
                e.cancelBubble = true;
                e.returnValue = false;
                if (e.stopPropagation) e.stopPropagation();
                if (e.preventDefault) e.preventDefault();
            }
            this.ControlFilterDateStart.This.style.backgroundColor = "#fff";
            this.ControlFilterDateEnd.This.style.backgroundColor = "#fff";

        }
        else if (type == 5) {

            var divConten = document.createElement("div");
            var divContenA = document.createElement("div");
            var divContenB = document.createElement("div");
            divConten.appendChild(divContenA);
            divConten.appendChild(divContenB);
            var label1 = document.createElement("label");
            var label2 = document.createElement("label");
            label1.innerHTML = "From:";
            label2.innerHTML = "To:";
            var div1 = document.createElement("div");
            var div2 = document.createElement("div");
            var div3 = document.createElement("div");
            divContenA.appendChild(div1);
            divContenA.appendChild(div2);
            divContenA.appendChild(div3);
            div2.style.marginTop = "5px";
            div1.appendChild(label1);
            div1.appendChild(this.ControlFilterDateStart.This);

            div2.appendChild(label2);
            div2.appendChild(this.ControlFilterDateEnd.This);
            this.ControlFilterDateStart.This.classList.add("form-control");
            this.ControlFilterDateEnd.This.classList.add("form-control");
            var CellButton = document.createElement("button");
            CellButton.style.width = "100%";
            div3.appendChild(CellButton);
            var iconButton = document.createElement("i");
            var spanButton = document.createElement("span");
            iconButton.classList.add("icon");
            iconButton.classList.add("ion-search");
            spanButton.innerHTML = " " + "Search";
            CellButton.appendChild(iconButton);
            CellButton.appendChild(spanButton);
            CellButton.onclick = function () {

                if (_this.onEventFilter != null) {
                    spanButton.innerHTML = "";
                    CellButton.disabled = true;
                    spanButton.innerHTML = " Loading...";
                    setTimeout(function () {

                        var valorStart = $(_this.ControlFilterDateStart.This).val();
                        var valorEnd = $(_this.ControlFilterDateEnd.This).val();
                        _this.tempDate.dateStart = valorStart;
                        _this.tempDate.dateEnd = valorEnd;
                        _this.onEventFilter(_this, _this.tempDate);
                        _this.ControlFilterDateStart.This.color = "rgb(43, 86, 154)";
                        _this.ControlFilterDateEnd.This.color = "rgb(43, 86, 154)";
                        spanButton.innerHTML = "";
                        CellButton.disabled = false;
                        spanButton.innerHTML = " " + "Search";

                    }, 0);


                }

            }

            CellButton.style.marginTop = "8px";
            CellButton.classList.add("btn");
            CellButton.classList.add("btn-info");


            var modalFV = new Componet.GridCF.TGridCFView.TModalFV(divConten, _this._ImgFieldFilter);
            this.ControlFilterDateStart.This.readOnly = true;//Desabilitar Escritura de campo text Time
            this.ControlFilterDateEnd.This.readOnly = true;//Desabilitar Escritura de campo text Time

            this.ControlFilterDateStart.This.placeholder = "Select..";
            this.ControlFilterDateEnd.This.placeholder = "Select..";

            this.tempDate = {
                dateStart: null,
                dateEnd: null
            }
            this.ControlFilterDateStart.onChange = function () {
                _this.ControlFilterDateStart.This.color = "#555";
                _this.ControlFilterDateStart.This.color = "#555";

            }
            this.ControlFilterDateEnd.onChange = function () {
                _this.ControlFilterDateStart.This.color = "#555";
                _this.ControlFilterDateStart.This.color = "#555";


            }
            this.ControlFilterDateStart.This.onclick = function () {
                var e = window.event || arguments.callee.caller.arguments[0];
                e.cancelBubble = true;
                e.returnValue = false;
                if (e.stopPropagation) e.stopPropagation();
                if (e.preventDefault) e.preventDefault();
            }
            this.ControlFilterDateEnd.This.onclick = function () {
                var e = window.event || arguments.callee.caller.arguments[0];
                e.cancelBubble = true;
                e.returnValue = false;
                if (e.stopPropagation) e.stopPropagation();
                if (e.preventDefault) e.preventDefault();
            }
            this.ControlFilterDateStart.This.style.backgroundColor = "#fff";
            this.ControlFilterDateEnd.This.style.backgroundColor = "#fff";

        }
    }
    this.CreateControlFilter = function () {
        //var VclStackPanelField = _this.VclStackPanelField;
        //$(VclStackPanelField.Column[0].This).html("");
        switch (this.DataType) {
            case SysCfg.DB.Properties.TDataType.String: // SysCfg.DB.Properties.TDataType.String   
                this.ControlFilter = new TVclTextBox("", "");
                this.CreateDialogControlFilter(1);

                break;
            case SysCfg.DB.Properties.TDataType.Text: //SysCfg.DB.Properties.TDataType.Text  
                this.ControlFilter = new TVclTextBox("", "");
                this.CreateDialogControlFilter(1);
                break;
            case SysCfg.DB.Properties.TDataType.Int32: // SysCfg.DB.Properties.TDataType.Int32:   
                this.ControlFilter = new TVclTextBox("", "");
                this.ControlFilter.Type = TImputtype.number;
                this.CreateDialogControlFilter(1);

                break;
            case SysCfg.DB.Properties.TDataType.Double: //SysCfg.DB.Properties.TDataType.Double:   
                this.ControlFilter = new TVclTextBox("", "");
                this.ControlFilter.Type = TImputtype.number;
                this.CreateDialogControlFilter(1);
                break;
            case SysCfg.DB.Properties.TDataType.Boolean: //SysCfg.DB.Properties.TDataType.Boolean:     
                this.CreateDialogControlFilter(2);

                break;
            case SysCfg.DB.Properties.TDataType.DateTime: //SysCfg.DB.Properties.TDataType.DateTime: 
                switch (this.ColumnStyle) {

                    case Componet.Properties.TExtrafields_ColumnStyle.None:
                        var divModalContent = document.createElement("div");

                        this.ControlFilterDateStart = new TVclTextBox(divModalContent, "");
                        $(this.ControlFilterDateStart.This).datetimepicker();
                        this.ControlFilterDateEnd = new TVclTextBox(divModalContent, "");
                        $(this.ControlFilterDateEnd.This).datetimepicker();
                        this.CreateDialogControlFilter(3);

                        break;
                    case Componet.Properties.TExtrafields_ColumnStyle.Date:

                        var divModalContent = document.createElement("div");

                        this.ControlFilterDateStart = new TVclTextBox(divModalContent, "");
                        $(this.ControlFilterDateStart.This).datepicker();
                        this.ControlFilterDateEnd = new TVclTextBox(divModalContent, "");
                        $(this.ControlFilterDateEnd.This).datepicker();
                        this.CreateDialogControlFilter(4);
                        break;
                    case Componet.Properties.TExtrafields_ColumnStyle.Time:
                        var divModalContent = document.createElement("div");

                        this.ControlFilterDateStart = new TVclTextBox(divModalContent, "");
                        $(this.ControlFilterDateStart.This).timepicker(
                            {
                                timeFormat: "hh:mm TT",
                            }
                        );
                        this.ControlFilterDateEnd = new TVclTextBox(divModalContent, "");
                        $(this.ControlFilterDateEnd.This).timepicker({
                            timeFormat: "hh:mm TT",
                        }
                        );
                        this.CreateDialogControlFilter(5);
                }


                //this.ControlFilterDateStart.This.style.display = "none";
                //this.ControlFilterDateEnd.This.style.display = "none";
                break;
        }
        if (_this.DataType != SysCfg.DB.Properties.TDataType.Boolean && _this.DataType != SysCfg.DB.Properties.TDataType.DateTime) {
            this.ControlFilter.This.onclick = function () {
                var e = window.event || arguments.callee.caller.arguments[0];
                e.cancelBubble = true;
                e.returnValue = false;
                if (e.stopPropagation) e.stopPropagation();
                if (e.preventDefault) e.preventDefault();
            }
        }


    }
    this.Textnode = null;
    this._Visible = true;
    this._VisibleVertical = true;
    this._Visible_IsShowValue = true;
    this._VisibleVertical_IsShowValue = true;
    this.onEventFilter = function (Column, value) {

        if (!_this.GridParent.IsTree) {
            if (_this.GridParent != null && _this.GridParent.Rows.length > 0) {
                if (this.DataType == SysCfg.DB.Properties.TDataType.DateTime) {
                    var dateStart = value.dateStart;
                    var dateEnd = value.dateEnd;
                    if (dateEnd == null || dateStart == null) {
                        return true;
                    }

                }
                for (var i = 0, t = _this.GridParent.Rows.length; i < t; i++) {
                    row = _this.GridParent.Rows[i];
                    if (this.DataType == SysCfg.DB.Properties.TDataType.DateTime) {

                        var dateStart = value.dateStart;
                        var dateEnd = value.dateEnd;
                        if (dateStart != null && dateEnd != null) {
                            var cadena = "" + row.Cells[_this.Index]._Value;
                            dateStart = new Date(dateStart);
                            dateEnd = new Date(dateEnd);
                            cadena = new Date(cadena);
                            row.Visible = (cadena >= dateStart && cadena <= dateEnd);
                        }

                    }
                    else if (this.DataType == SysCfg.DB.Properties.TDataType.Boolean) {
                        value = (value == "true" || value);
                        var valueCell = (row.Cells[_this.Index]._Value == "true");
                        row.Visible = (value == valueCell);


                    } else {
                        var cadena = "" + row.Cells[_this.Index]._Value;
                        cadena = cadena.toUpperCase();

                        row.Visible = (cadena.indexOf(('' + value).toUpperCase()) != -1);
                    }
                }
            }
        } else {
            if (_this.GridParent != null && _this.GridParent.Rows.length > 0) {
                if (value == "" || value == null) {
                    for (var i = 0, t = _this.GridParent.Rows.length; i < t; i++) {
                        var row = _this.GridParent.Rows[i];
                        row.Visible = (row.RowPadre == null);
                        if (row.RowHijos.length > 0) {
                            row._ImgGruopDown.style.display = 'none';
                            row._ImgGruopRigth.style.display = '';
                        }
                    }
                }
                else {

                    if (this.DataType == SysCfg.DB.Properties.TDataType.DateTime) {
                        var dateStart = value.dateStart;
                        var dateEnd = value.dateEnd;
                        if (dateEnd == null || dateStart == null) {
                            return true;
                        }

                    }
                    var numeroColumnas = _this.GridParent.Columns.length;
                    for (var i = 0, t = _this.GridParent.Rows.length; i < t; i++) {
                        row = _this.GridParent.Rows[i];

                        if (row.Cells.length == numeroColumnas) {

                            if (this.DataType == SysCfg.DB.Properties.TDataType.DateTime) {

                                var dateStart = value.dateStart;
                                var dateEnd = value.dateEnd;
                                if (dateStart != null && dateEnd != null) {
                                    var cadena = "" + row.Cells[_this.Index]._Value;
                                    dateStart = new Date(dateStart);
                                    dateEnd = new Date(dateEnd);
                                    cadena = new Date(cadena);
                                    row.Visible = (cadena >= dateStart && cadena <= dateEnd);
                                }

                            } else if (this.DataType == SysCfg.DB.Properties.TDataType.Boolean) {
                                value = (value == "true" || value);
                                var valueCell = (row.Cells[_this.Index]._Value == "true");
                                row.Visible = (value == valueCell);


                            } else {
                                var cadena = "" + row.Cells[_this.Index]._Value;
                                cadena = cadena.toUpperCase();

                                row.Visible = (cadena.indexOf(('' + value).toUpperCase()) != -1);
                            }

                        }
                        else {
                            row.Visible = false;
                        }


                    }
                }
            }
        }

    }
    this._ModalEvent = null;
    this.OptionProgressBar = {
        "Theme": 0,
        "ShowNumber": true,
        "ShowPercentage": false,
    }

    Object.defineProperty(this, 'FileType', {
        get: function () {
            return this._FileType;
        },
        set: function (inValue) {
            this._FileType = inValue;
        }
    });
    Object.defineProperty(this, 'FileSRVEvent', {
        get: function () {
            return this._FileSRVEvent;
        },
        set: function (inValue) {
            this._FileSRVEvent = inValue;
        }
    });
    Object.defineProperty(this, 'InsertActiveEditor', {
        get: function () {
            return this._InsertActiveEditor;
        },
        set: function (inValue) {

            this._InsertActiveEditor = inValue;
            this.CaptionOrder = this.Caption;
        }
    });
    Object.defineProperty(this, 'UpdateActiveEditor', {
        get: function () {
            return this._UpdateActiveEditor;
        },
        set: function (inValue) {

            this._UpdateActiveEditor = inValue;
            this.CaptionOrder = this.Caption;
        }
    });
    Object.defineProperty(this, 'ToolBarButton', {
        get: function () {
            return this._ToolBarButton;
        },

        set: function (inValue) {
            var bandera = false;

            if (typeof (inValue.InsertActive) != "undefined") {
                if (this._ToolBarButton.InsertActive != inValue.InsertActive)
                    bandera = true;
                this._ToolBarButton.InsertActive = inValue.InsertActive;

            }
            if (typeof (inValue.UpdateActive) != "undefined") {
                if (this._ToolBarButton.UpdateActive != inValue.UpdateActive)
                    bandera = true;
                this._ToolBarButton.UpdateActive = inValue.UpdateActive;
            }
            if (typeof (inValue.DeleteActive) != "undefined") {
                if (this._ToolBarButton.DeleteActive != inValue.DeleteActive)
                    bandera = true;
                this._ToolBarButton.DeleteActive = inValue.DeleteActive;
            }
            if (bandera) {

                if (this._ToolBarButton != null && this.GridParent != null && this.GridParent.Rows.length > 1) {

                    for (var i = 0, t = this.GridParent.Rows.length; i < t; i++) {
                        this.GridParent.Rows[i].Cells[this.GridParent.Columns.length - 1].InitCell();
                    }
                }
            }




        }
    });
    Object.defineProperty(this, 'CustomDrawCell', {
        get: function () {
            return this._CustomDrawCell;
        },
        set: function (inValue) {
            this._CustomDrawCell = inValue;
            if (this.GridParent != null && this.GridParent.Rows.length > 1) {
                for (var i = 0, t = this.GridParent.Rows.length; i < t; i++) {

                    this.GridParent.Rows[i].Cells[this.Index].InitCell();
                }
            }

        }
    });
    Object.defineProperty(this, 'ActiveEditor', {
        get: function () {
            return this._ActiveEditor;
        },
        set: function (inValue) {
            this._ActiveEditor = inValue;
        }
    });
    Object.defineProperty(this, 'onclick', {
        get: function () {
            return this.This.onclick;
        },
        set: function (inValue) {
            var _this = this;
            this.This.onclick = function () {
                var nuevo = _this;
                inValue(nuevo);
            };
        }
    });
    Object.defineProperty(this, 'Name', {
        get: function () {
            return this._Name;
        },
        set: function (inValue) {
            this._Name = inValue;
        }
    });
    Object.defineProperty(this, 'Index', {
        get: function () {
            return this._Index;
        },
        set: function (inValue) {
            this._Index = inValue;
        }
    });
    Object.defineProperty(this, 'Caption', {
        get: function () {
            return this._Caption;
        },
        set: function (inValue) {
            this.This.style.verticalAlign = "top";
            this._Caption = inValue;
            var textnode = document.createTextNode("" + inValue);         // Create a text node
            this.This.appendChild(textnode);
            if (this.IsOptionCheck) {
                this.selectAll = new TVclInputcheckbox(this.This, "");
                this.selectAll.onClick = function () {

                    _this.GridParent.SetSelectedRows(_this.selectAll.Checked);
                }
            }

        }
    });
    Object.defineProperty(this, 'CaptionButton', {
        get: function () {
            return this._Caption;
        },
        set: function (inValue) {
            this.This.style.verticalAlign = "top";
            this._Caption = inValue;
            $(this.This).append("<br>" + inValue + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");


        }
    });
    Object.defineProperty(this, 'CaptionOrder', {

        set: function (inValue) {

            $(this.This).html("");
            var color = "#9E9E9E"
            this.This.style.verticalAlign = "top";
            this._Caption = inValue;
            this.divResize = document.createElement("div");         // Create a text node
            this.This.appendChild(this.divResize);
            this.divResize.style.height = "10px";
            this.divResize.style.width = "100%";
            this.divResize.classList.add("columScroll");
     
            $(this.divResize).resizable();
            this.divResize.onclick = function () {
                if (SysCfg.App.Properties.Device == SysCfg.App.TDevice.Desktop) {
                var e = window.event || arguments.callee.caller.arguments[0];
                e.cancelBubble = true;
                e.returnValue = false;
                if (e.stopPropagation) e.stopPropagation();
                if (e.preventDefault) e.preventDefault();
                }
            }
            var VclStackPanelOptions = document.createElement("div");
            this.This.appendChild(VclStackPanelOptions);
            //_this.VclStackPanelField = new TVclStackPanel(this.This, "1", 1, [[12], [12], [12], [12], [12]]);
            VclStackPanelOptions.style.textAlign = "right";
            this._ImgFieldStatus = document.createElement("i");
            this._ImgFieldStatus.classList.add("icon");
            this._ImgFieldStatus.classList.add("ion-pin");
            this._ImgFieldStatus.style.color = color;
            this._ImgOrder = document.createElement("i");
            this._ImgOrder.classList.add("icon");
            this._ImgFieldFilter = document.createElement("i");
            this._ImgFieldFilter.classList.add("icon");
            this._ImgFieldFilter.classList.add("ion-search");
            this._ImgFieldFilter.classList.add("iconGridOption");
            this._ImgFieldFilter.style.color = color;
            this._ImgFieldStatus.style.float = "left";
            if (!this.InsertActiveEditor && !this.UpdateActiveEditor) {
                this._ImgFieldStatus.style.display = 'none';
            } else {
                this._ImgFieldStatus.style.display = '';
            }
            this._ImgOrder.style.display = 'none';
            this._ImgOrder.style.marginRight = "3px";
            this._ImgFieldFilter.style.marginRight = "6px";
            this._ImgFieldFilter.style.display = '';
            this._ImgFieldFilter.style.cursor = "pointer";
            _this.FilterClick = false;
            this._ImgFieldFilter.onclick = function (e) {
                if (SysCfg.App.Properties.Device == SysCfg.App.TDevice.Desktop) {
                    _this.CreateControlFilter();
                    _this.FilterClick = !_this.FilterClick;
                    //_this.VclStackPanelField.Row.This.style.display = (_this.FilterClick) ? "" : "none";

                    var evt = e ? e : window.event;
                    if (evt.stopPropagation) evt.stopPropagation();
                    if (evt.cancelBubble != null) evt.cancelBubble = true;
                }
               
            }
            var a = document.createElement("i");
            a.classList.add("icon");
            a.classList.add("ion-navicon-round");
            a.classList.add("iconGridOption");
            a.style.color = color;
            a.style.cursor = "pointer";
            a.onclick = function () {
                if (SysCfg.App.Properties.Device == SysCfg.App.TDevice.Desktop) {
                    if (_this.onEventFilterColumn != null) {
                        _this.onEventFilterColumn(_this);
                    }
                }
            }

            VclStackPanelOptions.appendChild(this._ImgFieldStatus);
            VclStackPanelOptions.appendChild(this._ImgOrder);
            VclStackPanelOptions.appendChild(this._ImgFieldFilter);
            VclStackPanelOptions.appendChild(a);

            this.Textnode = document.createTextNode("" + inValue);         // Create a text node

            this.This.appendChild(this.Textnode);
            if (this.IsColumnSelectOrder != null) {
                if (this.IsColumnSelectOrder == "asc") {
                    this._ImgOrder.style.display = "";
                    this._ImgOrder.classList.add("ion-arrow-up-a");
                    this._ImgOrder.style.color = color;
                }
                else if (this.IsColumnSelectOrder == "des") {
                    this._ImgOrder.style.display = "";
                    this._ImgOrder.classList.add("ion-arrow-down-a");
                    this._ImgOrder.style.color = color;
                }
            }

        }

    });
    Object.defineProperty(this, 'CaptionStyle', {
        get: function () {
            return this._Caption;
        },
        set: function (inValue) {
            this._Caption = inValue;
            this.Textnode.nodeValue = inValue;
            this._ImgOrder.style.display = 'none';

        }

    });
    Object.defineProperty(this, 'CaptionAutoTranslete', {
        get: function () {
            return this._Caption;
        },
        set: function (inValue) {
            this._Caption = inValue;
            //var textnode = document.createTextNode("" + inValue);         // Create a text node
            this.Textnode.nodeValue = inValue;

        }

    });
    Object.defineProperty(this, 'SrcImgOrder', {

        set: function (_Value) {
            if (_Value != null) {
                this._ImgOrder.src = _Value;
                this._ImgOrder.style.display = '';
            }
            else
                this._ImgOrder.style.display = 'none';

        }
    });
    Object.defineProperty(this, 'Size', {
        get: function () {
            return this._Size;
        },
        set: function (inValue) {
            this._Size = inValue;
        }
    });
    Object.defineProperty(this, 'EnabledEditor', {
        get: function () {
            return this._EnabledEditor;
        },
        set: function (inValue) {
            this._EnabledEditor = inValue;
            if (this.OnRefreshColumn != null)
                this.OnRefreshColumn(this);
        }
    });
    Object.defineProperty(this, 'DataType', {
        get: function () {
            return this._DataType;
        },
        set: function (inValue) {
            this._DataType = inValue;
            if (this.OnRefreshColumn != null)
                this.OnRefreshColumn(this);
        }
    });
    Object.defineProperty(this, 'ColumnStyle', {
        get: function () {
            return this._ColumnStyle;
        },
        set: function (inValue) {
            this._ColumnStyle = inValue;
            if (this.OnRefreshColumn != null)
                this.OnRefreshColumn(this);
        }
    });
    Object.defineProperty(this, 'Visible', {
        get: function () {
            //if (this.This.style.display == "none")
            //    return false;
            //return true;
            return this._Visible;
        },
        set: function (_Value) {
            this._Visible = _Value;
            if (_Value)
                this.This.style.display = "";
            else
                this.This.style.display = 'none';

        }
    });
    Object.defineProperty(this, 'Visible_IsShowValue', {
        get: function () {

            return this._Visible_IsShowValue;
        },
        set: function (_Value) {
            this._Visible_IsShowValue = _Value;
            if (!_Value) {

                if (this.GridParent != null && this.GridParent.Rows.length > 1) {
                    for (var i = 0, t = this.GridParent.Rows.length; i < t; i++) {
                        $(this.GridParent.Rows[i].Cells[this.Index].This).html("");
                    }
                }
            }


        }
    });
    Object.defineProperty(this, 'Width', {
        get: function () {
            return this.This.style.width;
        },
        set: function (inValue) {
            this.This.style.width = inValue;
        }
    });
    Object.defineProperty(this, 'ModalEvent', {
        get: function () {
            return this._ModalEvent;
        },
        set: function (inValue) {

            this._ModalEvent = inValue;

            if (inValue != null && this.GridParent != null && this.GridParent.Rows.length > 0) {
                for (var i = 0, t = this.GridParent.Rows.length; i < t; i++) {

                    this.GridParent.Rows[i].Cells[this.Index].InitCell();
                }
            }
        }
    });
}

var TVclRowCF = function () {
    this._Cells = Array();
    this._Index;
    this._IndexTreeGrid;
    this.IndexDataSet = null;
    this.GridParent = null;
    this.This = Utr("", "");
    var _this = this;
    Object.defineProperty(this, 'Cells', {
        get: function () {
            return this._Cells;
        },
        set: function (inValue) {
            this._Cells = inValue;
        }
    });

    Object.defineProperty(this, 'Index', {
        get: function () {
            return this._Index;
        },
        set: function (inValue) {
            this._Index = inValue;
        }
    });
    Object.defineProperty(this, 'IndexTreeGrid', {
        get: function () {
            return this._IndexTreeGrid;
        },
        set: function (inValue) {
            this._IndexTreeGrid = inValue;
        }
    });

    Object.defineProperty(this, 'onclick', {
        get: function () {
            return this.This.onclick;
        },
        set: function (inValue) {
            var _this = this;
            this.This.onclick = function () {
                var nuevo = _this;
                inValue(nuevo);
            };
        }
    });

    Object.defineProperty(this, 'ondblclick', {
        get: function () {
            return this.This.ondblclick;
        },
        set: function (inValue) {
            var _this = this;
            this.This.ondblclick = function () {
                var nuevo = _this;
                inValue(nuevo);
            };
        }
    });

    Object.defineProperty(this, 'Visible', {
        get: function () {
            if (this.This.style.display == "none")
                return false;
            return true;
        },
        set: function (_Value) {

            if (_Value)
                this.This.style.display = "";
            else
                this.This.style.display = 'none';

        }
    });
    this.GetCell = function (Indexcolumn) {
        return this._Cell[Indexcolumn];
    }

    this.AddCell = function (inCell) {
        this._Cells.push(inCell);
        inCell._Row = this;
        this.This.appendChild(inCell.This);
    }

    this.AddCellTree = function (inCell, Column) {
        this._Cells.push(inCell);
        inCell._Row = this;
        inCell.Column = Column;
        inCell.InitCell();
        this.This.appendChild(inCell.This);
    }
    this.InsertCell = function (inCell, inIndex) {
        var cellTemp = this._Cells[inIndex];
        this._Cells.splice(inIndex, 0, inCell);
        inCell._Row = this;
        $(inCell.This).insertBefore(cellTemp.This);
    }
    this.UpdateCell = function (inCell, inCellOld) {
        //inCellOld._Row = this;
        inCell._Row = this;
        //inCell._Row = this;
        //$(this.This).appendChild(inCell.This);
        //inCell.This.parentNode = this.This;
        //this.This.insertBefore(inCell.This, inCellOld.This);
        //this.This.removeChild(inCellOld.This);
        inCell.This.parentNode = inCellOld.This.parentNode;
        //this.GridParentBody
        inCellOld._Row.This.replaceChild(inCell.This, inCellOld.This);
        inCell.InitCell();
        //this.Body.replaceChild(inRow.This, inRowOld.This);
    }
    this.UpdateCell1 = function (inCell, inCellOld) {
        inCellOld._Row = this;
        inCell.This.parentNode = inCellOld.This.parentNode;
        inCellOld.This.parentNode.replaceChild(inCellOld.This, inCell.This);
    }
    this.GetCellValue = function (NameColumn) {
        for (var i = 0, t = this._Cells.length; i < t; i++) {
            if (this._Cells[i].Column.Name == NameColumn)
                return this._Cells[i].Value;
        }
        return null;
    }
    this.SetCellValue = function (Value, NameColumn) {
        for (var i = 0, t = this._Cells.length; i < t; i++) {
            if (this._Cells[i].Column.Name == NameColumn)
                this._Cells[i].Value = Value;
        }
    }
    this.UimgLocal = function (Object, id, Src, alt) {
        var Res = undefined;
        var FullSrc = SysCfg.App.Properties.xRaiz + Src;
        var img = document.createElement('img');
        img.id = id
        img.src = FullSrc;
        img.alt = alt;
        Res = img;
        Object.appendChild(img)
        return Res;
    }
    this.RowHijos = new Array();
    this.RowPadre = null;
    this._CantidadHijos = 0;
    this._PaddingGroup = 0;
    this._ImgGruopDown = null;

    this.showHideChild = function (option) {
        this.Visible = option;

        if (_this.RowHijos.length > 0) {
            _this._ImgGruopDown.style.display = 'none';
            _this._ImgGruopRigth.style.display = '';
            for (var i = 0, t = _this.RowHijos.length; i < t; i++) {
                _this.RowHijos[i].showHideChild(option);
                if (_this.PanelPaginationGroup != null) {
                    _this.PanelPaginationGroup.style.display = "none";
                    _this.VclStackPanelGrup.style.display = "";
                }
            }
        }

    }

    /*FILA NAV BAR*/
    this.IconLeftNavBar = "image/24/folder.png";
    this._PaddindTextNavBar = 20;
    this._TextNavBar = "";
    this._TypeNavBar = "V";
    this.EventClose = null;
    this.OnEventGruopDown = null;
    this.RutaGruopDown = null;
    Object.defineProperty(this, 'TextNavBar', {

        set: function (_Value) {
            if (this.Cells.length == 0) {
                var Cell = new TVclCellCF();
                Cell._Value = "";
                this.AddCell(Cell);
            }
            this._TextNavBar = _Value;
            $(this.Cells[0].This).html("");
            if (this._TypeNavBar == "SP") {
                var VclStackPanelOptions = new TVclStackPanel(this.Cells[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
                var texto = "&nbsp;&nbsp;" + _Value;
                this._ImgIconLeftNavBar = this.UimgLocal(VclStackPanelOptions.Column[0].This, "", this.IconLeftNavBar, "Filter");
                this._ImgIconLeftNavBar.style.width = "16px";
                this.LabelTextGroup = new TVcllabel(VclStackPanelOptions.Column[0].This, "", TlabelType.H0);
                this.LabelTextGroup.Text = texto;
                this.LabelTextGroup.This.style.color = "rgb(117, 117, 117)";
                this._ImgIconLeftNavBar.style.marginTop = "-3px";
                this.iDown = document.createElement("i");
                this.iDown.classList.add("icon");
                this.iDown.classList.add("ion-android-close");
                this.iDown.style.color = "rgb(117, 117, 117)"
                this.iDown.style.float = "right";
                VclStackPanelOptions.Column[0].This.appendChild(this.iDown);
                this.iUp.style.display = "none";
                this.onclick = function (NewRow) {
                    if (_this.EventClose != null) {
                        _this.EventClose();
                    }

                }
                this._ImgGruopDown = this.UimgLocal(VclStackPanelOptions.Column[0].This, "", "image/16/right_round-16-diagonal2.png", "Filter");
                this._ImgGruopRigth = this.UimgLocal(VclStackPanelOptions.Column[0].This, "", "image/16/right_round-32.png", "Filter");
                this._ImgGruopDown.style.display = 'none';
                this._ImgGruopRigth.style.display = 'none';

            } else if (this._TypeNavBar == "P") {
                var VclStackPanelOptions = new TVclStackPanel(this.Cells[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
                var texto = "&nbsp;&nbsp;" + _Value;
                this._ImgIconLeftNavBar = this.UimgLocal(VclStackPanelOptions.Column[0].This, "", this.IconLeftNavBar, "Filter");
                this._ImgIconLeftNavBar.style.width = "16px";
                this.LabelTextGroup = new TVcllabel(VclStackPanelOptions.Column[0].This, "", TlabelType.H0);
                this.LabelTextGroup.Text = texto;
                this.LabelTextGroup.This.style.color = "rgb(117, 117, 117)";
                this._ImgIconLeftNavBar.style.marginTop = "-3px";
                this.iDown = document.createElement("i");
                this.iUp = document.createElement("i");
                this.iDown.classList.add("icon");
                this.iDown.classList.add("ion-arrow-down-b");
                this.iDown.style.color = "rgb(117, 117, 117)"
                this.iDown.style.float = "right";
                this.iUp.classList.add("icon");
                this.iUp.classList.add("ion-arrow-up-b");
                this.iUp.style.color = "rgb(117, 117, 117)"
                this.iUp.style.float = "right";
                VclStackPanelOptions.Column[0].This.appendChild(this.iUp);
                VclStackPanelOptions.Column[0].This.appendChild(this.iDown);
                this.iUp.style.display = "none";
                this.OpenClick = true;
                this.onclick = function (NewRow) {
                    _this.OpenClick = !_this.OpenClick;
                    if (!_this.OpenClick) {
                        _this.GridParent.showHideChild(_this, false);
                        _this.iUp.style.display = "";
                        _this.iDown.style.display = "none";
                    } else {
                        _this.iUp.style.display = "none";
                        _this.iDown.style.display = "";
                        for (var i = 0, t = _this.RowHijos.length; i < t; i++) {
                            _this.RowHijos[i].Visible = true;

                        }
                    }


                }
                this._ImgGruopDown = this.UimgLocal(VclStackPanelOptions.Column[0].This, "", "image/16/right_round-16-diagonal2.png", "Filter");
                this._ImgGruopRigth = this.UimgLocal(VclStackPanelOptions.Column[0].This, "", "image/16/right_round-32.png", "Filter");
                this._ImgGruopDown.style.display = 'none';
                this._ImgGruopRigth.style.display = 'none';

            } else if (this._TypeNavBar == "H") {
                var VclStackPanelOptions = new TVclStackPanel(this.Cells[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
                var texto = "&nbsp;&nbsp;" + _Value;
                this._ImgIconLeftNavBar = this.UimgLocal(VclStackPanelOptions.Column[0].This, "", this.IconLeftNavBar, "Filter");
                this._ImgIconLeftNavBar.style.width = "16px";
                this.LabelTextGroup = new TVcllabel(VclStackPanelOptions.Column[0].This, "", TlabelType.H0);
                this.LabelTextGroup.Text = texto;
                VclStackPanelOptions.Column[0].This.style.marginLeft = _this._PaddindTextNavBar + "px";
                this.LabelTextGroup.This.style.color = "rgb(117, 117, 117)";

            } else if (this._TypeNavBar == "V") {
                var texto = "&nbsp;&nbsp;" + _Value;
                var Fila1 = new TVclStackPanel(this.Cells[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
                var Fila2 = new TVclStackPanel(this.Cells[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
                Fila1.Column[0].This.classList.add("text-center");
                Fila2.Column[0].This.classList.add("text-center");
                this._ImgIconLeftNavBar = this.UimgLocal(Fila1.Column[0].This, "", this.IconLeftNavBar, "Filter");
                this._ImgIconLeftNavBar.style.width = "24px";
                this.LabelTextGroup = new TVcllabel(Fila2.Column[0].This, "", TlabelType.H0);
                this.LabelTextGroup.Text = texto;
                this.LabelTextGroup.This.style.color = "rgb(117, 117, 117)";
                //VclStackPanelOptions.Column[0].This.style.marginLeft = _this._PaddindTextNavBar + "px";
            }
            //this.style.backgroundColor = "#3e94ba";




        }
    });
    Object.defineProperty(this, 'PanelNavBar', {

        set: function (_Value) {
            if (this.Cells.length == 0) {
                var Cell = new TVclCellCF();
                //Cell._Value = "";
                this.AddCell(Cell);
            }

            $(this.Cells[0].This).html("");
            this.Cells[0].This.appendChild(_Value);

        }
    });
    /*FIN DE FILA NAV BAR*/

    //this.PanelGroup = null;
    this.PanelPaginationGroup = null;
    this.PanelPagination = null;
    this.setPaginationRow = function () {
        if (this.PanelPaginationGroup == null) {

            var texto = "&nbsp;&nbsp;" + this.Cells[0]._Value;
            this.PanelPaginationGroup = this.VclStackPanelPagination.Column[0].This;
            this.PanelPaginationGroup.style.display = "none";

            this.PanelPagination = document.createElement("div");
            this.PanelPaginationGroup.style.marginLeft = this._PaddingGroup + "px";
            var divLabel = document.createElement("div");
            //divLabel.style.backgroundColor = "#337ab7";
            divLabel.style.marginRight = "4px";
            this.PanelPaginationGroup.appendChild(divLabel);
            this.PanelPaginationGroup.appendChild(this.PanelPagination);

            divLabel.style.float = "left";
            divLabel.style.height = "33px";

            this.PanelPagination.style.float = "left";
            this.PanelPagination.style.height = "30px";
            this._ImgGruopDown2 = this.UimgLocal(divLabel, "", "image/16/right_round-16-diagonal2.png", "Filter");
            this._ImgGruopDown2.onclick = function () {

                _this.PanelPaginationGroup.style.display = "none";
                _this.VclStackPanelGrup.style.display = "";
                if (_this.OnEventGruopDown != null) {
                    _this.OnEventGruopDown(false, _this.RutaGruopDown, _this);
                }

                _this._ImgGruopDown.style.display = 'none';
                _this._ImgGruopRigth.style.display = '';
                _this.GridParent.showHideChild(_this, false);

                var e = window.event || arguments.callee.caller.arguments[0];
                e.cancelBubble = true;
                e.returnValue = false;
                if (e.stopPropagation) e.stopPropagation();
                if (e.preventDefault) e.preventDefault();

            }

            this.LabelTextGroup2 = new TVcllabel(divLabel, "", TlabelType.H0);
            this.LabelTextGroup2.Text = texto;
            this.LabelTextGroup2.This.style.color = "#FFFFFF";
            this.LabelTextGroup2.This.style.marginTop = "7px";

            this._ImgGruopOpen = document.createElement('img');
            this._ImgGruopOpen.style.width = "12px";
            this._ImgGruopOpen.style.marginTop = "-2px";
            //this._ImgGruopOpen.src = "image/24/arrows2-edit-group.png";
            this._ImgGruopOpen.src = "image/24/arrows2-edit.png";

            this.LinkImgGruopOpen = document.createElement('button');
            this.LinkImgGruopOpen.classList.add("btn");
            this.LinkImgGruopOpen.classList.add("btn-default");
            this.LinkImgGruopOpen.classList.add("btn-xs");
            this.LinkImgGruopOpen.style.height = "32px";
            this.LinkImgGruopOpen.style.width = "32px";
            //this.LinkImgGruopOpen.style.cursor = "pointer";
            this.LinkImgGruopOpen.appendChild(this._ImgGruopOpen);
            this.LinkImgGruopOpen.onclick = function () {

                if (_this.GridParent != null && _this.GridParent.OnImgGruopOpen != null) {
                    _this.GridParent.OnImgGruopOpen(_this.RutaGruopDown, _this);
                }
                var e = window.event || arguments.callee.caller.arguments[0];
                e.cancelBubble = true;
                e.returnValue = false;
                if (e.stopPropagation) e.stopPropagation();
                if (e.preventDefault) e.preventDefault();


            }
            this.LinkImgGruopOpen.style.marginLeft = "4px";
            divLabel.appendChild(this.LinkImgGruopOpen);

            divLabel.style.paddingLeft = "5px";
            divLabel.style.paddingRight = "0px";
            divLabel.style.borderRadius = "4px";

            _this.PanelPaginationGroup.style.display = "";
            _this.VclStackPanelGrup.style.display = "none";


            //divLabel.style.borderRadius = "1px";
            //divLabel.style.background = "#FAFAFA";
            //divLabel.style.boxShadow = "1px 1px #c4c4c4";
            //this.LabelTextGroup2.This.style.color = "#757575";
            //divLabel.style.height = "31px";
        }

    }

    Object.defineProperty(this, 'PaddingGroup', {
        get: function () {
            return this._PaddingGroup;
        },
        set: function (_Value) {

            this._PaddingGroup = _Value;
            $(this.Cells[0].This).html("");
            this.Cells[0].This.style.textAlign = "left";

            var VclStackPanel = new TVclStackPanel(this.Cells[0].This, "1", 1, [[12], [12], [12], [12], [12]]);

            var VclStackPanelOptions = new TVclStackPanel(VclStackPanel.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
            var VclStackPanelPagination = new TVclStackPanel(VclStackPanel.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
            this.VclStackPanelPagination = VclStackPanelPagination;
            this.VclStackPanelGrup = VclStackPanelOptions.Row.This;
            var texto = "&nbsp;&nbsp;" + this.Cells[0]._Value;
            this._ImgGruopDown = this.UimgLocal(VclStackPanelOptions.Column[0].This, "", "image/16/right_round-16-diagonal2.png", "Filter");
            this._ImgGruopRigth = this.UimgLocal(VclStackPanelOptions.Column[0].This, "", "image/16/right_round-32.png", "Filter");
            this.LabelTextGroup = new TVcllabel(VclStackPanelOptions.Column[0].This, "", TlabelType.H0);
            this.LabelTextGroup.Text = texto;
            VclStackPanelOptions.Column[0].This.style.marginLeft = this._PaddingGroup + "px";
            this._ImgGruopRigth.style.display = 'none';
            this.LabelTextGroup.This.style.color = "#ffffff";
            this._ImgGruopDown.onclick = function () {
                //cerrando agrupacion
                if (_this.PanelPaginationGroup != null) {
                    _this.PanelPaginationGroup.style.display = "none";
                    _this.VclStackPanelGrup.style.display = "";
                }

                if (_this.OnEventGruopDown != null) {
                    _this.OnEventGruopDown(false, _this.RutaGruopDown, _this);
                }
                //else {
                //    if (_this.GridParent != null) {
                //        _this.GridParent.showHideChild(_this);

                //    }
                //}

                _this._ImgGruopDown.style.display = 'none';
                _this._ImgGruopRigth.style.display = '';
                _this.GridParent.showHideChild(_this, false);
                var e = window.event || arguments.callee.caller.arguments[0];
                e.cancelBubble = true;
                e.returnValue = false;
                if (e.stopPropagation) e.stopPropagation();
                if (e.preventDefault) e.preventDefault();


            }
            this._ImgGruopRigth.onclick = function () {

                if (_this.PanelPaginationGroup != null) {
                    _this.PanelPaginationGroup.style.display = "";
                    _this.VclStackPanelGrup.style.display = "none";
                }

                if (_this.OnEventGruopDown != null) {
                    _this.OnEventGruopDown(true, _this.RutaGruopDown, _this);
                }
                _this._ImgGruopRigth.style.display = 'none';
                _this._ImgGruopDown.style.display = '';
                for (var i = 0, t = _this.RowHijos.length; i < t; i++) {
                    _this.RowHijos[i].Visible = true;

                }

                var e = window.event || arguments.callee.caller.arguments[0];
                e.cancelBubble = true;
                e.returnValue = false;
                if (e.stopPropagation) e.stopPropagation();
                if (e.preventDefault) e.preventDefault();

            }
            /***Build***/
            //this.PanelGroup = VclStackPanelOptions.Column[0].This;



        }
    });
    Object.defineProperty(this, 'CantidadHijos', {
        get: function () {
            return this._CantidadHijos;
        },
        set: function (_Value) {
            this._CantidadHijos = _Value;
            var texto = "&nbsp;&nbsp;" + this.Cells[0]._Value + " (" + _Value + ")";
            this.LabelTextGroup.Text = texto;
            if (this.PanelPaginationGroup != null) {
                this.LabelTextGroup2.Text = texto;
            }


        }
    });
}

var TVclCellCF = function () {

    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this._Value;
    this._Row;
    this._Column = null;
    this._IndexRow;
    this._IndexRowTreeGrid;
    this._IndexColumn;
    this.Control;
    this.CellColor = "";
    this.This = Utd("", "");

    /*****/
    this.btnInsert = null;
    this.btnDelete = null;
    this.btnUpdate = null;
    this.btnPost = null;
    this.btnCancel = null;
    /*****/

    this.InitCell = function () {
        if (SysCfg.App.Properties.Device == SysCfg.App.TDevice.Desktop) {
            this.This.style.lineHeight = "11px";
        } else {
            this.This.style.lineHeight = "18px";
        }
        
        if (this._Column != null) {
            if ((this._Column.EnabledEditor || this._Column.IsOptionCheck) && this._Column.ModalEvent == null && this._Column.ActiveEditor) {

                if (this.Control == null) {
                    $(this.This).html("");//Se realizo para colocar los botones la tabla, puesto que se coloca un stak panel antes ejecutar el metodo InitCell 
                }
                switch (this._Column.DataType) {
                    case SysCfg.DB.Properties.TDataType.Boolean: //SysCfg.DB.Properties.TDataType.Boolean:     

                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //CREAR UN CHECKBOX
                                this.Control = new TVclInputcheckbox(this.This, "");
                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.String: // SysCfg.DB.Properties.TDataType.String   

                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //CREAR UN TEXTBOX
                                this.Control = new TVclTextBox(this.This, "");
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.TextPassword:
                                //CREAR UN TEXTBOX DE TIPO PASSWORD
                                this.Control = new TVclTextBox(this.This, "");
                                this.Control.Type = TImputtype.password;
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.LookUp:
                                //CREAR UN COMBOBOX 
                                this.Control = new TVclComboBox2(this.This, "");
                                if (this._Column.onEventControl != null) {

                                    this._Column.onEventControl(this.Control, this._Column.DataEventControl);
                                }

                                break;

                            case Componet.Properties.TExtrafields_ColumnStyle.LookUpOption:
                                //CREAR UN RADIO BUTTON
                                this.Control = new TVclInputRadioButton(this.This, "");

                                if (this._Column.onEventControl != null) {
                                    this._Column.onEventControl(this.Control, this._Column.DataEventControl);
                                }
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.Text:
                                this.Control = new TVclMemo(this.This, "");

                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Text: //SysCfg.DB.Properties.TDataType.Text  

                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //CREAR UN MEMO TEXT
                                this.Control = new TVclMemo(this.This, "");
                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Int32: // SysCfg.DB.Properties.TDataType.Int32:   
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //CREAR UN NUMERICUPDOWN
                                this.Control = new TVclTextBox(this.This, "");
                                this.Control.Type = TImputtype.number;
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.LookUp:
                                //CREAR UN COMBOBOX 
                                this.Control = new TVclComboBox2(this.This, "");
                                if (this._Column.onEventControl != null) {
                                    this._Column.onEventControl(this.Control, this._Column.DataEventControl);
                                }
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.LookUpOption:
                                //CREAR UN RADIO BUTTON
                                this.Control = new TVclInputRadioButton(this.This, "");

                                if (this._Column.onEventControl != null) {
                                    this._Column.onEventControl(this.Control, this._Column.DataEventControl);
                                }
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.ProgressBar:
                                this.Control = new TVclTextBox(this.This, "");
                                this.Control.Type = TImputtype.number;

                                break;

                        }
                        //this.Control.This.style.width = "60px";
                        break;
                    case SysCfg.DB.Properties.TDataType.Double: //SysCfg.DB.Properties.TDataType.Double:   
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //CREAR UN NUMERICUPDOWN                    
                                this.Control = new TVclTextBox(this.This, "");
                                this.Control.Type = TImputtype.number;
                                break;
                        }
                        //this.Control.This.style.width = "60px";
                        break;

                    case SysCfg.DB.Properties.TDataType.DateTime: //SysCfg.DB.Properties.TDataType.DateTime: 
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //CREAR UN DATE PICKER 
                                this.Control = new TVclTextBox(this.This, "");
                                $(this.Control.This).datetimepicker({
                                    showButtonPanel: true
                                });

                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.Date:
                                //CREAR UN DATATIME PICKER
                                this.Control = new TVclTextBox(this.This, "");
                                //timeFormat: "yyyy-mm-dd"
                                $(this.Control.This).datepicker();
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.Time:
                                //CREAR UN TIME PICKER
                                this.Control = new TVclTextBox(this.This, "");
                                $(this.Control.This).timepicker({
                                    timeFormat: "hh:mm TT",
                                });
                                break;
                        }

                        this.Control.This.readOnly = true;//Desabilitar Escritura de campo text Time
                        break;
                    default:
                        if (this.btnInsert != null && this.btnDelete != null && this.btnUpdate != null && this.btnPost != null && this.btnCancel != null) {
                            if (this._Column.ToolBarButton != null) {
                                if (typeof (this._Column.ToolBarButton.InsertActive) != "undefined") {
                                    this.btnInsert.This.style.display = (this._Column.ToolBarButton.InsertActive) ? "" : "none";
                                }
                                if (typeof (this._Column.ToolBarButton.UpdateActive) != "undefined") {
                                    this.btnUpdate.This.style.display = (this._Column.ToolBarButton.UpdateActive) ? "" : "none";
                                }
                                if (typeof (this._Column.ToolBarButton.DeleteActive) != "undefined") {
                                    this.btnDelete.This.style.display = (this._Column.ToolBarButton.DeleteActive) ? "" : "none";
                                }

                                if (!this._Column.ToolBarButton.DeleteActive && !this._Column.ToolBarButton.UpdateActive && !this._Column.ToolBarButton.InsertActive) {
                                    //this.btnDelete.This.setAttribute("class", (this._Column.ToolBarButton.DeleteActive) ? "" : "hidden");
                                    this.btnDelete.This.style.display = "none";
                                    this.btnPost.This.style.display = "none";
                                    this.btnCancel.This.style.display = "none";
                                } else {
                                    this.btnPost.This.style.display = "";
                                    this.btnCancel.This.style.display = "";
                                }
                            }

                        }
                        break;
                }
            }
            else {
                if (this._Column.GridParent != null && this._Column.GridParent.IsInfo) {
                    if (!this._Column._VisibleVertical_IsShowValue) {
                        //$(this.This).html("");
                        return true;
                    }
                } else if (!this._Column._Visible_IsShowValue) {
                    //$(this.This).html("");
                    return true;
                }

            }

            this.SetValue(this._Value);

            if (this.Column.CustomDrawCell != null) {
                this.Column.CustomDrawCell(this, this.Column.DataType, this._Value, this.Column.DataBuildCustomCell)
            }



        }
    }
    this.SetValue = function (inValue) {

        if (this._Column != null) {
            //if ((this._Column.EnabledEditor || this._Column.IsOptionCheck) && this._Column.ModalEvent == null && this._Column.ActiveEditor) {
            if ((this._Column.EnabledEditor || this._Column.IsOptionCheck) && this._Column.ModalEvent == null && this._Column.ActiveEditor) {
                switch (this._Column.DataType) {
                    case SysCfg.DB.Properties.TDataType.Boolean: //SysCfg.DB.Properties.TDataType.Boolean:     
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //LIMPIAR UN CHECKBOX
                                this.Control.Checked = (inValue == "true" || inValue == true);
                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.String: // SysCfg.DB.Properties.TDataType.String   
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //LIMPIAR UN TEXTBOX
                                this.Control.Text = inValue;
                                this.Control.This.classList.add("form-control");
                                this.Control.This.style.borderRadius = "0px";
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.TextPassword:
                                //LIMPIAR UN TEXTBOX DE TIPO PASSWORD   
                                this.Control.Text = inValue;
                                this.Control.This.classList.add("form-control");
                                this.Control.This.style.borderRadius = "0px";
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.LookUp:
                                //LIMPIAR UN COMBOBOX 
                                this.Control.Value = inValue;
                                this.Control.This.classList.add("form-control");
                                this.Control.This.style.borderRadius = "0px";
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.LookUpOption:
                                this.Control.Value = inValue;
                                this.Control.This.classList.add("form-control");
                                this.Control.This.style.borderRadius = "0px";
                                //LIMPIAR UN RADIO BUTTON
                                break;

                            case Componet.Properties.TExtrafields_ColumnStyle.FileSRV:
                                $(this.This).html("");

                                if (this._Column.GridParent.FileSRVEvent != null && _this._Column._FileType != null) {
                                    var divFile1 = document.createElement("div");
                                    divFile1.style.float = "left";
                                    divFile1.style.marginRight = "3px";
                                    divFile1.style.marginTop = "7px";
                                    var divFile2 = document.createElement("div");
                                    divFile2.style.marginTop = "9px";
                                    this.This.appendChild(divFile1);
                                    this.This.appendChild(divFile2);

                                    var imgUpload = document.createElement("i");
                                    imgUpload.classList.add("icon");
                                    imgUpload.classList.add("ion-android-upload");
                                    imgUpload.classList.add("text-danger");
                                    imgUpload.style.fontSize = "14px";

                                    //var UaModal = Ua(this.This, "", "»»»");
                                    var UaModal = Ua(divFile1, "", "");
                                    UaModal.appendChild(imgUpload);
                                    //UaModal.appendChild(inValue);
                                    UaModal.onclick = function () {
                                        _this._Column.GridParent.FileSRVEvent(_this, _this._Column._FileType);
                                    }

                                    if (inValue != null && inValue != "") {
                                        try {
                                            var FILESRVList = new Array();
                                            Persistence.GP.Methods.StrtoFILESRV(inValue, FILESRVList);
                                            if (FILESRVList.length > 0) {
                                                var unFILESRV = FILESRVList[0];
                                                var UaModal2 = Ua(divFile2, "", "" + unFILESRV.FILENAME);
                                                UaModal2.onclick = function () {
                                                    _this._Column.GridParent.FileSRVEvent(_this, _this._Column._FileType);
                                                }
                                            } else {
                                                var UaModal2 = Ua(divFile2, "", "...");
                                                UaModal2.onclick = function () {
                                                    _this._Column.GridParent.FileSRVEvent(_this, _this._Column._FileType);
                                                }
                                            }
                                        }
                                        catch (e) {

                                            SysCfg.Log.Methods.WriteLog("GridControlCF.js SetValue Componet.Properties.TExtrafields_ColumnStyle.FileSRV", e);
                                        }
                                        //$(this.This).html(inValue);

                                    } else {
                                        var UaModal2 = Ua(divFile2, "", "...");
                                        UaModal2.onclick = function () {
                                            _this._Column.GridParent.FileSRVEvent(_this, _this._Column._FileType);
                                        }
                                    }
                                }

                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.Text:
                                //debugger;
                                $(this.This).html("");
                                var divFile1 = document.createElement("div");
                                divFile1.style.float = "left";
                                divFile1.style.marginRight = "3px";
                                divFile1.style.marginTop = "7px";
                                var divFile2 = document.createElement("div");
                                divFile2.style.marginTop = "9px";
                                this.This.appendChild(divFile1);
                                this.This.appendChild(divFile2);

                                var imgUpload = document.createElement("i");
                                imgUpload.classList.add("icon");
                                //imgUpload.classList.add("ion-chatbox-working");
                                imgUpload.classList.add("ion-compose");

                                imgUpload.classList.add("text-danger");
                                imgUpload.style.fontSize = "14px";

                                //var UaModal = Ua(this.This, "", "»»»");
                                var UaModal = Ua(divFile1, "", "");
                                UaModal.appendChild(imgUpload);
                                //UaModal.appendChild(inValue);
                                UaModal.onclick = function (e) {

                                    var modalFV = null;
                                    var textAreaStrech = document.createElement("textarea");
                                    textAreaStrech.cols = "40";
                                    textAreaStrech.rows = "4";
                                    textAreaStrech.innerHTML = inValue;
                                    textAreaStrech.onkeypress = function (e) {
                                        if (e.keyCode == 13) {
                                            texto = this.value;
                                            texto = texto.substring(0, texto.length - 1);
                                            if (modalFV != null) {
                                                modalFV.hide();
                                            }
                                        }
                                    }
                                    textAreaStrech.onkeyup = function (e) {
                                        if (e.keyCode == 13) {
                                            texto = this.value;
                                            texto = texto.substring(0, texto.length - 1);
                                            if (modalFV != null) {
                                                modalFV.hide();
                                            }
                                        }
                                    }
                                    modalFV = new Componet.GridCF.TGridCFView.TModalFV(textAreaStrech, divFile1);
                                    textAreaStrech.focus();
                                    modalFV.Data = textAreaStrech;
                                    modalFV.onOutside = function (Data) {

                                        _this.onOutside(Data);
                                    }
                                    var evt = e ? e : window.event;
                                    if (evt.stopPropagation) evt.stopPropagation();
                                    if (evt.cancelBubble != null) evt.cancelBubble = true;
                                }

                                if (inValue != null && inValue != "") {
                                    var UaModal2 = Ua(divFile2, "", "" + inValue);
                                    UaModal2.onclick = function (e) {
                                        var modalFV = null;
                                        var textAreaStrech = document.createElement("textarea");
                                        textAreaStrech.cols = "40";
                                        textAreaStrech.rows = "4";
                                        textAreaStrech.innerHTML = inValue;
                                        textAreaStrech.onkeypress = function (e) {
                                            if (e.keyCode == 13) {
                                                texto = this.value;
                                                texto = texto.substring(0, texto.length - 1);
                                                if (modalFV != null) {
                                                    modalFV.hide();
                                                }
                                            }
                                        }
                                        textAreaStrech.onkeyup = function (e) {
                                            if (e.keyCode == 13) {
                                                texto = this.value;
                                                texto = texto.substring(0, texto.length - 1);
                                                if (modalFV != null) {
                                                    modalFV.hide();
                                                }
                                            }
                                        }
                                        modalFV = new Componet.GridCF.TGridCFView.TModalFV(textAreaStrech, divFile1);
                                        textAreaStrech.focus();
                                        modalFV.Data = textAreaStrech;
                                        modalFV.onOutside = function (Data) {

                                            _this.onOutside(Data);
                                        }
                                        var evt = e ? e : window.event;
                                        if (evt.stopPropagation) evt.stopPropagation();
                                        if (evt.cancelBubble != null) evt.cancelBubble = true;
                                    }
                                } else {
                                    var UaModal2 = Ua(divFile2, "", "...");
                                    UaModal2.onclick = function (e) {
                                        var textAreaStrech = document.createElement("textarea");
                                        textAreaStrech.cols = "40";
                                        textAreaStrech.rows = "4";
                                        textAreaStrech.innerHTML = inValue;
                                        var modalFV = new Componet.GridCF.TGridCFView.TModalFV(textAreaStrech, divFile1);
                                        modalFV.Data = textAreaStrech;
                                        modalFV.onOutside = function (Data) {

                                            _this.onOutside(Data);
                                        }
                                        var evt = e ? e : window.event;
                                        if (evt.stopPropagation) evt.stopPropagation();
                                        if (evt.cancelBubble != null) evt.cancelBubble = true;
                                    }
                                }


                                //if (inValue != null) {
                                //    var textStrech = document.createElement("a");
                                //    textStrech.innerHTML = inValue;
                                //    textStrech.onclick = function (e) {
                                //        var textAreaStrech = document.createElement("textarea");
                                //        textAreaStrech.cols = "40";
                                //        textAreaStrech.rows = "4";
                                //        textAreaStrech.innerHTML = inValue;
                                //        var modalFV = new Componet.GridCF.TGridCFView.TModalFV(textAreaStrech, textStrech);
                                //        modalFV.Data= textAreaStrech;
                                //        modalFV.onOutside = function (Data) {
                                //           //debugger;
                                //            _this.onOutside(Data);
                                //        }
                                //        var evt = e ? e : window.event;
                                //        if (evt.stopPropagation) evt.stopPropagation();
                                //        if (evt.cancelBubble != null) evt.cancelBubble = true;
                                //    }
                                //    this.This.appendChild(textStrech);
                                //} else {
                                //    $(this.This).html(textStrech);
                                //}

                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Text: //SysCfg.DB.Properties.TDataType.Text  
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //LIMPIAR UN MEMO TEXT
                                this.Control.Text = inValue;
                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Int32: // SysCfg.DB.Properties.TDataType.Int32:   
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //LIMPIAR UN NUMERICUPDOWN
                                this.Control.Text = inValue;
                                this.Control.This.classList.add("form-control");
                                this.Control.This.style.borderRadius = "0px";
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.LookUp:
                                //LIMPIAR UN COMBOBOX 
                                this.Control.Value = inValue;
                                this.Control.This.classList.add("form-control");
                                this.Control.This.style.borderRadius = "0px";
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.LookUpOption:
                                this.Control.Value = inValue;
                                this.Control.This.classList.add("form-control");
                                this.Control.This.style.borderRadius = "0px";
                                //LIMPIAR UN RADIO BUTTON
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.ProgressBar:
                                this.Control.Text = inValue;
                                this.Control.This.classList.add("form-control");
                                this.Control.This.style.borderRadius = "0px";
                                //LIMPIAR UN RADIO BUTTON
                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Double: //SysCfg.DB.Properties.TDataType.Double:   
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //LIMPIAR UN NUMERICUPDOWN                    
                                this.Control.Text = inValue;
                                this.Control.This.classList.add("form-control");
                                this.Control.This.style.borderRadius = "0px";
                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.DateTime: //SysCfg.DB.Properties.TDataType.DateTime: 
                        var date = new Date(inValue);
                        if (date == "Invalid Date") {
                            var inValue = parseInt(inValue);
                            if (!isNaN(inValue)) {
                                date = new Date(inValue);
                                inValue = date;
                            } else {
                                inValue = "";
                            }
                        }
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //SysCfg.DateTimeMethods.ToDateTimeString 
                                if (inValue != null && inValue != "" && inValue != "Mon Jan 01 0001 00:00:00 GMT-0800 (Pacific Standard Time)") {
                                    var fecha = new Date(inValue);
                                    fecha = SysCfg.DateTimeMethods.ToDateTimeString(fecha);
                                    this.Control.Text = fecha;
                                } else {
                                    this.Control.Text = "";
                                }
                                this.Control.This.classList.add("form-control");
                                this.Control.This.style.borderRadius = "0px";
                                this.Control.This.style.backgroundColor = "#FFFFFF";
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.Date:
                                if (inValue != null && inValue != "" && inValue != "Mon Jan 01 0001 00:00:00 GMT-0800 (Pacific Standard Time)") {
                                    var fecha = new Date(inValue);
                                    fecha = SysCfg.DateTimeMethods.ToDateString(fecha);
                                    this.Control.Text = fecha;
                                } else {
                                    this.Control.Text = "";
                                }
                                this.Control.This.classList.add("form-control");
                                this.Control.This.style.borderRadius = "0px";
                                this.Control.This.style.backgroundColor = "#FFFFFF";
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.Time:
                                if (inValue != null && inValue != "" && inValue != "Mon Jan 01 0001 00:00:00 GMT-0800 (Pacific Standard Time)") {
                                    var fecha = new Date(inValue);
                                    fecha = SysCfg.DateTimeMethods.ToTimeString(fecha);
                                    this.Control.Text = fecha;
                                } else {
                                    this.Control.Text = "";
                                }
                                this.Control.This.classList.add("form-control");
                                this.Control.This.style.borderRadius = "0px";
                                this.Control.This.style.backgroundColor = "#FFFFFF";
                                break;
                        }
                        break
                    default:
                        break;
                }
            }
            else {
                switch (this._Column.DataType) {
                    case SysCfg.DB.Properties.TDataType.String: // SysCfg.DB.Properties.TDataType.String  
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                var textStrech = "";
                                textStrech = inValue;
                                if (this._Column.GridParent.IsInfo == false && inValue != null && inValue.length > 100) {
                                    var textStrech = document.createElement("a");
                                    textStrech.innerHTML = inValue;
                                    textStrech.Enabled = false;
                                    textStrech.onclick = function (e) {
                                        var textAreaStrech = document.createElement("textarea");
                                        textAreaStrech.cols = "40";
                                        textAreaStrech.rows = "4";
                                        textAreaStrech.innerHTML = inValue;
                                        var modalFV = new Componet.GridCF.TGridCFView.TModalFV(textAreaStrech, textStrech);
                                        var evt = e ? e : window.event;
                                        if (evt.stopPropagation) evt.stopPropagation();
                                        if (evt.cancelBubble != null) evt.cancelBubble = true;
                                    }
                                    $(this.This).html("");
                                    this.This.appendChild(textStrech);
                                } else {
                                    $(this.This).html(textStrech);
                                }

                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.TextPassword:
                                var textStrech = "";
                                textStrech = inValue;

                                if (this._Column.GridParent.IsInfo == false && inValue != null && inValue.length > 100) {
                                    var textStrech = document.createElement("a");
                                    textStrech.innerHTML = inValue;
                                    textStrech.onclick = function (e) {
                                        var textAreaStrech = document.createElement("textarea");
                                        textAreaStrech.cols = "40";
                                        textAreaStrech.rows = "4";
                                        textAreaStrech.innerHTML = inValue;
                                        var modalFV = new Componet.GridCF.TGridCFView.TModalFV(textAreaStrech, textStrech);
                                        var evt = e ? e : window.event;
                                        if (evt.stopPropagation) evt.stopPropagation();
                                        if (evt.cancelBubble != null) evt.cancelBubble = true;
                                    }
                                    this.This.appendChild(textStrech);
                                } else {
                                    $(this.This).html(textStrech);
                                }
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.LookUp:

                                //debugger;
                                var ValueTemp = inValue;
                                $(this.This).html("");
                                if (inValue != undefined && inValue != null && inValue != "") {
                                    var ControlTemp = new TVclComboBox2(this.This, "");
                                    if (this._Column.onEventControl != null) {
                                        this._Column.onEventControl(ControlTemp, this._Column.DataEventControl);
                                        //ControlTemp.This.style.display = 'none';
                                        ControlTemp.Enabled = false;

                                        try {
                                            ControlTemp.Value = inValue;
                                            ValueTemp = ControlTemp.Text;

                                        }
                                        catch (e) {
                                            //SysCfg.Log.Methods.WriteLog("GridControlCF.js SetValue SysCfg.DB.Properties.TDataType.Int32", e);
                                        }
                                        ControlTemp.This.classList.add("form-control");
                                        ControlTemp.This.style.borderRadius = "0px";
                                        this.This.appendChild(ControlTemp.This);

                                    }
                                }
                                //$(this.This).html(ValueTemp);
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.LookUpOption:
                                //debugger;
                                $(this.This).html(inValue);
                                break;

                            case Componet.Properties.TExtrafields_ColumnStyle.FileSRV:
                                $(this.This).html("");
                                this.FILESRVList = new Array();
                                if (this._Column.GridParent.FileSRVEvent != null && _this._Column._FileType != null) {
                                    if (inValue != null && inValue != "") {
                                        try {
                                            this.FILESRVList = new Array();
                                            Persistence.GP.Methods.StrtoFILESRV(inValue, this.FILESRVList);
                                        }
                                        catch (e) {
                                            $(this.This).html("&nbsp;");
                                            SysCfg.Log.Methods.WriteLog("GridControlCF.js SetValue Componet.Properties.TExtrafields_ColumnStyle.FileSRV", e);
                                        }
                                    }
                                    if (this.FILESRVList.length > 0) {
                                        var divFile1 = document.createElement("div");
                                        divFile1.style.float = "left";
                                        divFile1.style.marginRight = "3px";
                                        var divFile2 = document.createElement("div");
                                        this.This.appendChild(divFile1);
                                        this.This.appendChild(divFile2);
                                        var imgUpload = document.createElement("i");
                                        imgUpload.classList.add("icon");
                                        imgUpload.classList.add("ion-archive");
                                        imgUpload.classList.add("text-primary");
                                        var UaModal = Ua(divFile1, "", "");
                                        UaModal.appendChild(imgUpload);
                                        var divFile3 = document.createElement("div");
                                        UaModal.onclick = function () {
                                            $(divFile3).html("");
                                            var OpenFile = new TOpenFile(divFile3, "", true);
                                            var SaveFile = new TSaveFile(divFile3, "");
                                            divFile3.style.display = "none";
                                            _this.This.appendChild(divFile3);
                                            try {
                                                GetSDfile = Comunic.Ashx.Client.Methods.GetSDfile(Comunic.Properties.TFileType.GetEnum(_this._Column._FileType), Comunic.Properties.TFileOperationType._FileRead, _this.FILESRVList[0].IDFILE, _this.FILESRVList[0].FILENAME, new Array());
                                            } catch (e) {
                                                alert("The file could not be downloaded");
                                                SysCfg.Log.Methods.WriteLog("FileSRVEvent Componet.Properties.TExtrafields_ColumnStyle.FileSRV", e);
                                            }
                                        }

                                        var UaModal2 = Ua(divFile2, "", "" + _this.FILESRVList[0].FILENAME);
                                        UaModal2.onclick = function () {
                                            $(divFile3).html("");
                                            var OpenFile = new TOpenFile(divFile3, "", true);
                                            var SaveFile = new TSaveFile(divFile3, "");
                                            divFile3.style.display = "none";
                                            _this.This.appendChild(divFile3);
                                            try {
                                                GetSDfile = Comunic.Ashx.Client.Methods.GetSDfile(Comunic.Properties.TFileType.GetEnum(_this._Column._FileType), Comunic.Properties.TFileOperationType._FileRead, _this.FILESRVList[0].IDFILE, _this.FILESRVList[0].FILENAME, new Array());
                                            } catch (e) {
                                                alert("The file could not be downloaded");
                                                SysCfg.Log.Methods.WriteLog("FileSRVEvent Componet.Properties.TExtrafields_ColumnStyle.FileSRV", e);
                                            }
                                        }
                                    } else {
                                        $(this.This).html("&nbsp;");
                                    }
                                }
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.Text:
                                //$(this.This).html("");
                                //var inValueTemp = inValue;
                                //if (inValueTemp == null || inValueTemp == "") {
                                //    inValueTemp = "...";
                                //}
                                //var divFile1 = document.createElement("div");
                                //divFile1.style.float = "left";
                                //divFile1.style.marginRight = "3px";
                                //var divFile2 = document.createElement("div");
                                //this.This.appendChild(divFile1);
                                //this.This.appendChild(divFile2);
                                //var imgUpload = document.createElement("i");
                                //imgUpload.classList.add("icon");
                                //imgUpload.classList.add("ion-chatbubble-working");
                                //imgUpload.classList.add("text-primary");
                                //var UaModal = Ua(divFile1, "", "");
                                //UaModal.appendChild(imgUpload);
                                //var divFile3 = document.createElement("div");
                                //UaModal.onclick = function (e) {
                                //    var textAreaStrech = document.createElement("textarea");
                                //    textAreaStrech.cols = "40";
                                //    textAreaStrech.rows = "4";
                                //    textAreaStrech.disabled = true;
                                //    textAreaStrech.innerHTML = inValueTemp;
                                //    var modalFV = new Componet.GridCF.TGridCFView.TModalFV(textAreaStrech, UaModal2);

                                //    var evt = e ? e : window.event;
                                //    if (evt.stopPropagation) evt.stopPropagation();
                                //    if (evt.cancelBubble != null) evt.cancelBubble = true;

                                //}

                                //var UaModal2 = Ua(divFile2, "", "" + inValueTemp);
                                //UaModal2.onclick = function (e) {
                                //    var textAreaStrech = document.createElement("textarea");
                                //    textAreaStrech.cols = "40";
                                //    textAreaStrech.rows = "4";
                                //    textAreaStrech.disabled = true;
                                //    textAreaStrech.innerHTML = inValueTemp;
                                //    var modalFV = new Componet.GridCF.TGridCFView.TModalFV(textAreaStrech, UaModal2);

                                //    var evt = e ? e : window.event;
                                //    if (evt.stopPropagation) evt.stopPropagation();
                                //    if (evt.cancelBubble != null) evt.cancelBubble = true;
                                //}
                                $(this.This).html("");
                                var textStrech = "";
                                textStrech = inValue;
                                var inValueTemp = inValue;
                                if (inValue == null || inValue == "") {
                                    inValueTemp = "...";
                                }
                                if (this._Column.GridParent.IsInfo == false) {
                                    var textStrech = document.createElement("a");
                                    textStrech.innerHTML = inValueTemp;
                                    textStrech.onclick = function (e) {
                                        var textAreaStrech = document.createElement("textarea");
                                        textAreaStrech.cols = "40";
                                        textAreaStrech.rows = "4";
                                        textAreaStrech.disabled = true;
                                        textAreaStrech.innerHTML = inValueTemp;
                                        var modalFV = new Componet.GridCF.TGridCFView.TModalFV(textAreaStrech, textStrech);

                                        var evt = e ? e : window.event;
                                        if (evt.stopPropagation) evt.stopPropagation();
                                        if (evt.cancelBubble != null) evt.cancelBubble = true;

                                    }
                                    this.This.appendChild(textStrech);
                                } else {
                                    $(this.This).html(textStrech);
                                }


                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Text: //SysCfg.DB.Properties.TDataType.Text  
                        var textStrech = "";
                        textStrech = inValue;

                        if (this._Column.GridParent.IsInfo == false && inValue != null && inValue.length > 100) {
                            var textStrech = document.createElement("a");
                            textStrech.innerHTML = inValue;
                            textStrech.onclick = function (e) {
                                var textAreaStrech = document.createElement("textarea");
                                textAreaStrech.cols = "40";
                                textAreaStrech.rows = "4";
                                textAreaStrech.innerHTML = inValue;
                                var modalFV = new Componet.GridCF.TGridCFView.TModalFV(textAreaStrech, textStrech);
                                var evt = e ? e : window.event;
                                if (evt.stopPropagation) evt.stopPropagation();
                                if (evt.cancelBubble != null) evt.cancelBubble = true;
                            }
                            this.This.appendChild(textStrech);
                        } else {
                            $(this.This).html(textStrech);
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Int32: // SysCfg.DB.Properties.TDataType.Int32:   
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                $(this.This).html(inValue);
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.LookUp:
                                //debugger;
                                var ValueTemp = inValue;
                                $(this.This).html("");
                                if (inValue != undefined && inValue != null && inValue != "") {
                                    var ControlTemp = new TVclComboBox2(this.This, "");
                                    if (this._Column.onEventControl != null) {
                                        this._Column.onEventControl(ControlTemp, this._Column.DataEventControl);
                                        //ControlTemp.This.style.display = 'none';
                                        ControlTemp.Enabled = false;

                                        try {
                                            ControlTemp.Value = inValue;
                                            ValueTemp = ControlTemp.Text;

                                        }
                                        catch (e) {
                                            //SysCfg.Log.Methods.WriteLog("GridControlCF.js SetValue SysCfg.DB.Properties.TDataType.Int32", e);
                                        }
                                        ControlTemp.This.classList.add("form-control");
                                        ControlTemp.This.style.borderRadius = "0px";
                                        this.This.appendChild(ControlTemp.This);

                                    }
                                }
                                //$(this.This).html(ValueTemp);
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.LookUpOption:
                                $(this.This).html(inValue);
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.ProgressBar:
                                //var divProgress = document.createElement("div");
                                //var divProgressContent = document.createElement("div");
                                //divProgress.appendChild(divProgressContent);
                                //debugger;
                                //var ContentProgress = "<div class='progress'><div class='progress-bar bg-info' role='progressbar' style='width: " + inValue+"%' aria-valuenow='" + inValue+"' aria-valuemin='0' aria-valuemax='100'></div></div>";
                                //var ContentProgress = "<div class='progress' style='margin-bottom: 0px;'><div class='progress-bar progress-bar-striped' role='progressbar' style='width: " + inValue + "%' aria-valuenow='" + inValue + "' aria-valuemin='0' aria-valuemax='100'>" + inValue+"</div></div>";
                                //var ContentProgress = "<div class='progress' style='margin-bottom: 0px;'><div class='progress-bar progress-bar-striped' role='progressbar' style='width: " + inValue + "%' aria-valuenow='" + inValue + "' aria-valuemin='0' aria-valuemax='100'></div></div>";
                                var ContentProgress = "<div class='progress' style='margin-bottom: 0px;'><div class='progress-bar progress-bar-striped' role='progressbar' style='width: " + inValue + "%' aria-valuenow='" + inValue + "' aria-valuemin='0' aria-valuemax='100'><span style='font-size:11px'>";
                                if (this._Column.OptionProgressBar.Theme != 0) {
                                    ContentProgress = "<div class='progress' style='margin-bottom: 0px;'><div class='progress-bar' role='progressbar' style='width: " + inValue + "%' aria-valuenow='" + inValue + "' aria-valuemin='0' aria-valuemax='100'><span style='font-size:11px'>";
                                }

                                if (this._Column.OptionProgressBar.ShowNumber) {
                                    ContentProgress = ContentProgress + inValue;
                                    if (this._Column.OptionProgressBar.ShowPercentage) {
                                        ContentProgress = ContentProgress + "%";
                                    }
                                }
                                ContentProgress = ContentProgress + "</span></div></div>";
                                $(this.This).html(ContentProgress);
                                break;
                        }

                        break;
                    case SysCfg.DB.Properties.TDataType.Double: //SysCfg.DB.Properties.TDataType.Double:   
                        $(this.This).html(inValue);
                        break;
                    case SysCfg.DB.Properties.TDataType.Boolean: //SysCfg.DB.Properties.TDataType.Boolean:     
                        $(this.This).html('' + inValue);
                        break;
                    case SysCfg.DB.Properties.TDataType.DateTime: //SysCfg.DB.Properties.TDataType.DateTime: 

                        //var date = new Date(parseInt(inValue));
                        var date = new Date(inValue);
                        if (date == "Invalid Date") {
                            var inValue = parseInt(inValue);
                            if (!isNaN(inValue)) {
                                date = new Date(inValue);
                                inValue = date;
                            } else {
                                inValue = "";
                            }
                        }


                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //SysCfg.DateTimeMethods.ToDateTimeString 
                                if (inValue != null && inValue != "" && inValue != "Mon Jan 01 0001 00:00:00 GMT-0800 (Pacific Standard Time)") {
                                    var fecha = new Date(inValue);
                                    fecha = SysCfg.DateTimeMethods.ToDateTimeString(fecha);
                                    // fecha = SysCfg.DateTimeMethods.ToDateTimeString(fecha);
                                    $(this.This).html(fecha);
                                } else {
                                    $(this.This).html("DD/MM/YY hh:mm");
                                }


                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.Date:
                                if (inValue != null && inValue != "" && inValue != "Mon Jan 01 0001 00:00:00 GMT-0800 (Pacific Standard Time)") {
                                    var fecha = new Date(inValue);
                                    fecha = SysCfg.DateTimeMethods.ToDateString(fecha);
                                    $(this.This).html(fecha);
                                } else {
                                    $(this.This).html("DD/MM/YY");
                                }
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.Time:
                                if (inValue != null && inValue != "" && inValue != "Mon Jan 01 0001 00:00:00 GMT-0800 (Pacific Standard Time)") {
                                    var fecha = new Date(inValue);
                                    fecha = SysCfg.DateTimeMethods.ToTimeString(fecha);
                                    $(this.This).html(fecha);
                                } else {
                                    $(this.This).html("hh:mm --");
                                }
                                break;
                        }

                        break;

                    default:

                        $(this.This).html(inValue);
                        break;
                }
                if (this._Column.ModalEvent != null && this._Column.EnabledEditor) {
                    var UaModal = Ua(this.This, "", " »»»");
                    UaModal.onclick = function () {
                        _this._Column.ModalEvent(_this);
                    }
                }
            }
        }
    }
    this.GetValue = function () {
        if (this._Column != null) {
            if ((this._Column.EnabledEditor || this._Column.IsOptionCheck) && this._Column.ModalEvent == null && this._Column.ActiveEditor) {
                switch (this._Column.DataType) {
                    case SysCfg.DB.Properties.TDataType.String: // SysCfg.DB.Properties.TDataType.String   
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //LIMPIAR UN TEXTBOX
                                this._Value = this.Control.Text;
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.TextPassword:
                                //LIMPIAR UN TEXTBOX DE TIPO PASSWORD   
                                this._Value = this.Control.Text;
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.LookUp:
                                //LIMPIAR UN COMBOBOX 
                                this._Value = this.Control.Value;
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.LookUpOption:
                                //LIMPIAR UN RADIO BUTTON
                                this._Value = this.Control.Value;
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.FileSRV:
                                this._Value = this._Value;
                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Text: //SysCfg.DB.Properties.TDataType.Text  
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //LIMPIAR UN MEMO TEXT
                                this._Value = this.Control.Text;
                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Int32: // SysCfg.DB.Properties.TDataType.Int32:   
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //LIMPIAR UN NUMERICUPDOWN
                                this._Value = parseInt(this.Control.Text);
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.LookUp:
                                //LIMPIAR UN COMBOBOX 
                                this._Value = this.Control.Value;
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.LookUpOption:
                                //LIMPIAR UN RADIO BUTTON
                                this._Value = this.Control.Value;
                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Double: //SysCfg.DB.Properties.TDataType.Double:   
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //LIMPIAR UN NUMERICUPDOWN                    
                                this._Value = parseFloat(this.Control.Text);
                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Boolean: //SysCfg.DB.Properties.TDataType.Boolean:     
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //LIMPIAR UN CHECKBOX
                                this._Value = this.Control.Checked;
                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.DateTime: //SysCfg.DB.Properties.TDataType.DateTime: 
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //LIMPIAR UN DATATIME PICKER
                                this._Value = this.Control.Text;
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.Date:
                                //LIMPIAR UN DATE PICKER 
                                this._Value = this.Control.Text;
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.Time:
                                //LIMPIAR UN TIME PICKER
                                this._Value = this.Control.Text;
                                break;
                        }
                        break
                    default:
                        break;
                }
            }
        }
        return this._Value;
    }
    this.ColorBack = function () {
        _this.Color = _this.CellColor;
    }
    this.RestoreFunctionColor = function () {
        //this.This.style.backgroundColor = "#ECF0F5";
        this.This.style.backgroundColor = "";
    }
    Object.defineProperty(this, 'Value', {
        get: function () {
            return this.GetValue();
        },
        set: function (inValue) {
            inValue = "" + inValue;
            this._Value = "" + inValue;
            $(this.This).html(inValue);
            this.SetValue(inValue);
        }
    });

    Object.defineProperty(this, 'Row', {
        get: function () {
            return this._Row;
        },
        set: function (inValue) {
            this._Row = inValue;
        }
    });

    Object.defineProperty(this, 'Column', {
        get: function () {
            return this._Column;
        },
        set: function (inValue) {
            this._Column = inValue;
        }
    });

    Object.defineProperty(this, 'IndexRow', {
        get: function () {
            return this._IndexRow;
        },
        set: function (inValue) {
            this._IndexRow = inValue;
        }
    });
    Object.defineProperty(this, 'IndexRowTreeGrid', {
        get: function () {
            return this._IndexRowTreeGrid;
        },
        set: function (inValue) {
            this._IndexRowTreeGrid = inValue;
        }
    });

    Object.defineProperty(this, 'IndexColumn', {
        get: function () {
            return this._IndexColumn;
        },
        set: function (inValue) {
            this._IndexColumn = inValue;
        }
    });

    Object.defineProperty(this, 'Control', {
        get: function () {
            return this._Control;
        },
        set: function (inValue) {
            this._Control = inValue;
        }
    });
    Object.defineProperty(this, 'Color', {
        get: function () {
            return this.CellColor;
        },
        set: function (inValue) {
            this.CellColor = inValue;
            this.This.style.backgroundColor = inValue;
        }
    });
    
    Object.defineProperty(this, 'Visible', {
        get: function () {
            if (this.This.style.visibility == "hidden" || this.This.style.visibility == "collapse")
                return false;
            return true;
        },
        set: function (_Value) {
            if (_Value)
                this.This.style.display = '';
            else
                this.This.style.display = 'none';

        }
    });
}