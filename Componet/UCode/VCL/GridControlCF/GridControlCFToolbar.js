﻿﻿Componet.GridCF.TToolbarControlls =
    {
        GetEnum: function (Param) { return (SysCfg.GetEnum(Param, this)); },
        ButtonImg_ExportPDFBar: { value: 1, name: "ButtonImg_ExportPDFBar" },
        ButtonImg_ExportExcelBar: { value: 2, name: "ButtonImg_ExportExcelBar" },
        ButtonImg_ExportWordBar: { value: 3, name: "ButtonImg_ExportWordBar" },
        ButtonImg_RowEditToolsBar: { value: 4, name: "ButtonImg_RowEditToolsBar" },
        ButtonImg_ColumnGroupBar: { value: 5, name: "ButtonImg_ColumnGroupBar" },
        ButtonImg_NavigatorToolsBar: { value: 6, name: "ButtonImg_NavigatorToolsBar" },
        ButtonImg_EditToolsBar: { value: 7, name: "ButtonImg_EditToolsBar" },
        ButtonImg_DatailsViewBar: { value: 8, name: "ButtonImg_DatailsViewBar" },
        ButtonImg_StatusBar: { value: 9, name: "ButtonImg_StatusBar" },
        ButtonImg_ExpanderBar: { value: 10, name: "ButtonImg_ExpanderBar" },
        ButtonImg_ContraerBar: { value: 11, name: "ButtonImg_ContraerBar" },
        ButtonImg_DeleteBar: { value: 12, name: "ButtonImg_DeleteBar" },
        ButtonImg_AddBar: { value: 13, name: "ButtonImg_AddBar" },
        ButtonImg_EditBar: { value: 14, name: "ButtonImg_EditBar" },
        ButtonImg_PostBar: { value: 15, name: "ButtonImg_PostBar" },
        ButtonImg_CancelBar: { value: 16, name: "ButtonImg_CancelBar" },
        ButtonImg_PriorFirstBar: { value: 17, name: "ButtonImg_PriorFirstBar" },
        ButtonImg_PriorBar: { value: 18, name: "ButtonImg_PriorBar" },
        ButtonImg_NextBar: { value: 19, name: "ButtonImg_NextBar" },
        ButtonImg_NextLastBar: { value: 20, name: "ButtonImg_NextLastBar" },
        ButtonImg_GridVisibleColumnBar: { value: 25, name: "ButtonImg_GridVisibleColumnBar" },
        ButtonImg_GridDetailsVisibleColumnBar: { value: 26, name: "ButtonImg_GridDetailsVisibleColumnBar" },
        ButtonImg_GridVisibleColumnBar: { value: 27, name: "ButtonImg_GridVisibleColumnBar" },
        ButtonImg_ContraerColumnsBar: { value: 28, name: "ButtonImg_ContraerColumnsBar" },
        ButtonImg_ExpanderColumnsBar: { value: 29, name: "ButtonImg_ExpanderColumnsBar" },
        ButtonImg_RowCheckToolsBar: { value: 30, name: "ButtonImg_RowCheckToolsBar" },
        ButtonImg_MoveColumnsBar: { value: 29, name: "ButtonImg_MoveColumnsBar" },
        ButtonImg_EditColumHeader: { value: 30, name: "ButtonImg_EditColumHeader" },
        ButtonImg_Font: { value: 31, name: "ButtonImg_Font" },
        ButtonImg_Size: { value: 32, name: "ButtonImg_Size" },
        ButtonImg_Bold: { value: 33, name: "ButtonImg_Bold" },
        ButtonImg_Kursive: { value: 34, name: "ButtonImg_Kursive" },
        ButtonImg_AlignCenter: { value: 35, name: "ButtonImg_AlignCenter" },
        ButtonImg_AlignJustify: { value: 36, name: "ButtonImg_AlignJustify" },
        ButtonImg_AlignLeft: { value: 37, name: "ButtonImg_AlignLeft" },
        ButtonImg_AlignRight: { value: 38, name: "ButtonImg_AlignRight" },
        ButtonImg_Refresh: { value: 39, name: "ButtonImg_Refresh" },


    }

Componet.GridCF.TGridCFToolbar = function (inObject, BarControls, incallbackModalResult) {
    this.TParent = function () {
        return this;
    }.bind(this);

    var _this = this.TParent();
    this.Object = inObject;
    this.CallbackModalResult = incallbackModalResult;
    this.NavTabControls = null;
    this.ButtonImg_ExportPDFBar = null;
    this.ButtonImg_ExportExcelBar = null;
    this.ButtonImg_ExportWordBar = null;
    this.ButtonImg_RowEditToolsBar = null;
    this.ButtonImg_ColumnGroupBar = null;
    this.ButtonImg_NavigatorToolsBar = null;
    this.ButtonImg_EditToolsBar = null;
    this.ButtonImg_EditColumHeader = null;
    this.ButtonImg_DatailsViewBar = null;
    this.ButtonImg_StatusBar = null;
    this.ButtonImg_ExpanderBar = null;
    this.ButtonImg_ContraerBar = null;
    this.ButtonImg_DeleteBar = null;
    this.ButtonImg_AddBar = null;
    this.ButtonImg_EditBar = null;
    this.ButtonImg_PostBar = null;
    this.ButtonImg_CancelBar = null;
    this.ButtonImg_PriorFirstBar = null;
    this.ButtonImg_PriorBar = null;
    this.ButtonImg_NextBar = null;
    this.ButtonImg_NextLastBar = null;
    this.ButtonImg_GridVisibleColumnBar = null;
    this.ButtonImg_GridDetailsVisibleColumnBar = null;
    this.ButtonImg_ContraerColumnsBar = null;
    this.ButtonImg_ExpanderColumnsBar = null;
    this.ButtonImg_RowCheckToolsBar = null;
    this.ButtonImg_MoveColumnsBar = null;
    this._EditTools = false;

    Object.defineProperty(this, 'EditTools', {
        get: function () {
            return this._EditTools;
        },
        set: function (Value) {
            this._EditTools = Value;
            this.ButtonImg_EditToolsBar_Visible = Value;
            this.ButtonImg_DeleteBar_Visible = Value;
            this.ButtonImg_AddBar_Visible = Value;
            this.ButtonImg_EditBar_Visible = Value;
            this.ButtonImg_PostBar_Visible = Value;
            this.ButtonImg_CancelBar_Visible = Value;
        }
    });

    Object.defineProperty(this, 'ButtonImg_ExportPDFBar_Visible', {
        get: function () {
            return this.ButtonImg_ExportPDFBar.Visible;
        },
        set: function (Value) {
            this.ButtonImg_ExportPDFBar.Visible = Value;
        }
    });
    Object.defineProperty(this, 'ButtonImg_ExportExcelBar_Visible', {
        get: function () {
            return this.ButtonImg_ExportExcelBar.Visible;
        },
        set: function (Value) {
            this.ButtonImg_ExportExcelBar.Visible = Value;
        }
    });
    Object.defineProperty(this, 'ButtonImg_ExportWordBar_Visible', {
        get: function () {
            return this.ButtonImg_ExportWordBar.Visible;
        },
        set: function (Value) {
            this.ButtonImg_ExportWordBar.Visible = Value;
        }
    });
    Object.defineProperty(this, 'ButtonImg_RowEditToolsBar_Visible', {
        get: function () {
            return this.ButtonImg_RowEditToolsBar.Visible;
        },
        set: function (Value) {
            this.ButtonImg_RowEditToolsBar.Visible = Value;
        }
    });
    Object.defineProperty(this, 'ButtonImg_ColumnGroupBar_Visible', {
        get: function () {
            return this.ButtonImg_ColumnGroupBar.Visible;
        },
        set: function (Value) {
            this.ButtonImg_ColumnGroupBar.Visible = Value;
        }
    });
    Object.defineProperty(this, 'ButtonImg_NavigatorToolsBar_Visible', {
        get: function () {
            return this.ButtonImg_NavigatorToolsBar.Visible;
        },
        set: function (Value) {
            this.ButtonImg_NavigatorToolsBar.Visible = Value;
        }
    });
    Object.defineProperty(this, 'ButtonImg_EditToolsBar_Visible', {
        get: function () {
            return this.ButtonImg_EditToolsBar.Visible;
        },
        set: function (Value) {
            this.ButtonImg_EditToolsBar.Visible = Value;
        }
    });

    Object.defineProperty(this, 'ButtonImg_EditColumHeader_Visible', {
        get: function () {
            return this.ButtonImg_EditColumHeader.Visible;
        },
        set: function (Value) {
            this.ButtonImg_EditColumHeader.Visible = Value;
        }
    });
    Object.defineProperty(this, 'ButtonImg_DatailsViewBar_Visible', {
        get: function () {
            return this.ButtonImg_DatailsViewBar.Visible;
        },
        set: function (Value) {
            this.ButtonImg_DatailsViewBar.Visible = Value;
        }
    });
    Object.defineProperty(this, 'ButtonImg_StatusBar_Visible', {
        get: function () {
            return this.ButtonImg_StatusBar.Visible;
        },
        set: function (Value) {
            this.ButtonImg_StatusBar.Visible = Value;
        }
    });
    Object.defineProperty(this, 'ButtonImg_ExpanderBar_Visible', {
        get: function () {
            return this.ButtonImg_ExpanderBar.Visible;

        },
        set: function (Value) {

            this.ButtonImg_ExpanderBar.Visible = Value;
        }
    });
    Object.defineProperty(this, 'ButtonImg_ContraerBar_Visible', {
        get: function () {
            return this.ButtonImg_ContraerBar.Visible;
        },
        set: function (Value) {
            this.ButtonImg_ContraerBar.Visible = Value;
        }
    });

    this.DesactiveAllOptionEditTool = function () {
        if (!this._ButtonImg_DeleteBar_Active && !this._ButtonImg_AddBar_Active && !this._ButtonImg_EditBar_Active) {
            this.ButtonImg_PostBar_Active = false;
            this.ButtonImg_CancelBar_Active = false;
        } else {
            this.ButtonImg_PostBar_Active = true;
            this.ButtonImg_CancelBar_Active = true;
            //this.ButtonImg_PostBar_Visible = true;
            //this.ButtonImg_CancelBar_Visible = true;
        }
    }

    this._ButtonImg_DeleteBar_Active = true;
    Object.defineProperty(this, 'ButtonImg_DeleteBar_Active', {
        get: function () {
            return this._ButtonImg_DeleteBar_Active;
        },
        set: function (Value) {
            this._ButtonImg_DeleteBar_Active = Value;
            this.ButtonImg_DeleteBar_Visible = Value;
            //if (!Value) {
            //    this.ButtonImg_DeleteBar_Visible = false;
            //}
            this.DesactiveAllOptionEditTool();
        }
    });

    this._ButtonImg_AddBar_Active = true;
    Object.defineProperty(this, 'ButtonImg_AddBar_Active', {
        get: function () {
            return this._ButtonImg_AddBar_Active;
        },
        set: function (Value) {
            this._ButtonImg_AddBar_Active = Value;
            this.ButtonImg_AddBar_Visible = Value;
            //if (!Value) {
            //    this.ButtonImg_AddBar_Visible = false;
            //}
            this.DesactiveAllOptionEditTool();
        }
    });

    this._ButtonImg_EditBar_Active = true;
    Object.defineProperty(this, 'ButtonImg_EditBar_Active', {
        get: function () {
            return this._ButtonImg_EditBar_Active;
        },
        set: function (Value) {
            this._ButtonImg_EditBar_Active = Value;
            this.ButtonImg_EditBar_Visible = Value;
            //if (!Value) {
            //    this.ButtonImg_EditBar_Visible = false;
            //}
            this.DesactiveAllOptionEditTool();
        }
    });


    this._ButtonImg_PostBar_Active = true;
    Object.defineProperty(this, 'ButtonImg_PostBar_Active', {
        get: function () {
            return this._ButtonImg_PostBar_Active;
        },
        set: function (Value) {
            this._ButtonImg_PostBar_Active = Value;
            this.ButtonImg_PostBar_Visible = Value;
            //if (!Value) {
            //    this.ButtonImg_PostBar_Visible = false;
            //}
        }
    });

    this._ButtonImg_CancelBar_Active = true;
    Object.defineProperty(this, 'ButtonImg_CancelBar_Active', {
        get: function () {
            return this._ButtonImg_CancelBar_Active;
        },
        set: function (Value) {
            this._ButtonImg_CancelBar_Active = Value;
            this.ButtonImg_CancelBar_Visible = Value;
            //if (!Value) {
            //    this.ButtonImg_CancelBar_Visible = false;
            //}
        }
    });
    Object.defineProperty(this, 'ButtonImg_DeleteBar_Visible', {
        get: function () {
            return this.ButtonImg_DeleteBar.Visible;
        },
        set: function (Value) {

            this.ButtonImg_DeleteBar.Visible = (!this.ButtonImg_DeleteBar_Active) ? false : Value;
        }
    });

    Object.defineProperty(this, 'ButtonImg_AddBar_Visible', {
        get: function () {
            return this.ButtonImg_AddBar.Visible;
        },
        set: function (Value) {
            this.ButtonImg_AddBar.Visible = (!this.ButtonImg_AddBar_Active) ? false : Value;
        }
    });
    Object.defineProperty(this, 'ButtonImg_EditBar_Visible', {
        get: function () {
            return this.ButtonImg_EditBar.Visible;
        },
        set: function (Value) {
            this.ButtonImg_EditBar.Visible = (!this.ButtonImg_EditBar_Active) ? false : Value;
        }
    });
    Object.defineProperty(this, 'ButtonImg_PostBar_Visible', {
        get: function () {
            return this.ButtonImg_PostBar.Visible;
        },
        set: function (Value) {
            this.ButtonImg_PostBar.Visible = (!this.ButtonImg_PostBar_Active) ? false : Value;
        }
    });
    Object.defineProperty(this, 'ButtonImg_CancelBar_Visible', {
        get: function () {
            return this.ButtonImg_CancelBar.Visible;
        },
        set: function (Value) {
            this.ButtonImg_CancelBar.Visible = (!this.ButtonImg_CancelBar_Active) ? false : Value;;

        }
    });
    Object.defineProperty(this, 'ButtonImg_PriorFirstBar_Visible', {
        get: function () {
            return this.ButtonImg_PriorFirstBar.Visible;
        },
        set: function (Value) {
            this.ButtonImg_PriorFirstBar.Visible = Value;
        }
    });
    Object.defineProperty(this, 'ButtonImg_PriorBar_Visible', {
        get: function () {
            return this.ButtonImg_PriorBar.Visible;
        },
        set: function (Value) {
            this.ButtonImg_PriorBar.Visible = Value;
        }
    });
    Object.defineProperty(this, 'ButtonImg_NextBar_Visible', {
        get: function () {
            return this.ButtonImg_NextBar.Visible;
        },
        set: function (Value) {
            this.ButtonImg_NextBar.Visible = Value;
        }
    });
    Object.defineProperty(this, 'ButtonImg_NextLastBar_Visible', {
        get: function () {
            return this.ButtonImg_NextLastBar.Visible;
        },
        set: function (Value) {
            this.ButtonImg_NextLastBar.Visible = Value;
        }
    });


    Object.defineProperty(this, 'ButtonImg_GridDetailsVisibleColumnBar_Visible', {
        get: function () {
            return this.ButtonImg_GridDetailsVisibleColumnBar.Visible;
        },
        set: function (Value) {
            this.ButtonImg_GridDetailsVisibleColumnBar.Visible = Value;
        }
    });

    Object.defineProperty(this, 'ButtonImg_GridVisibleColumnBar_Visible', {
        get: function () {
            return this.ButtonImg_GridVisibleColumnBar.Visible;
        },
        set: function (Value) {
            this.ButtonImg_GridVisibleColumnBar.Visible = Value;
        }
    });

    Object.defineProperty(this, 'ButtonImg_ContraerColumnsBar_Visible', {
        get: function () {
            return this.ButtonImg_ContraerColumnsBar.Visible;
        },
        set: function (Value) {
            this.ButtonImg_ContraerColumnsBar.Visible = Value;
        }
    });

    Object.defineProperty(this, 'ButtonImg_ExpanderColumnsBar_Visible', {
        get: function () {
            return this.ButtonImg_ExpanderColumnsBar.Visible;
        },
        set: function (Value) {
            this.ButtonImg_ExpanderColumnsBar.Visible = Value;
        }
    });
    Object.defineProperty(this, 'ButtonImg_RowCheckToolsBar_Visible', {
        get: function () {
            return this.ButtonImg_RowCheckToolsBar.Visible;
        },
        set: function (Value) {
            this.ButtonImg_RowCheckToolsBar.Visible = Value;
        }
    });

    Object.defineProperty(this, 'ButtonImg_MoveColumnsBar_Visible', {
        get: function () {
            return this.ButtonImg_MoveColumnsBar.Visible;
        },
        set: function (Value) {
            this.ButtonImg_MoveColumnsBar.Visible = Value;
        }
    });

    Object.defineProperty(this, 'ButtonImg_Refresh_Visible', {
        get: function () {
            return this.ButtonImg_Refresh.Visible;
        },
        set: function (Value) {
            this.ButtonImg_Refresh.Visible = Value;
        }
    });



    Object.defineProperty(this, 'ButtonImg_RowEditToolsBar_ChekedClickDown', {
        get: function () {
            return this.ButtonImg_RowEditToolsBar.ChekedClickDown;
        },
        set: function (Value) {
            if (this.ButtonImg_RowEditToolsBar.ChekedClickDown != Value) {
                this.ButtonImg_RowEditToolsBar.ChekedClickDown = Value;
                _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_RowEditToolsBar);
            }

        }
    });

    Object.defineProperty(this, 'ButtonImg_ColumnGroupBar_ChekedClickDown', {
        get: function () {
            return this.ButtonImg_ColumnGroupBar.ChekedClickDown;
        },
        set: function (Value) {
            if (this.ButtonImg_ColumnGroupBar.ChekedClickDown != Value) {
                this.ButtonImg_ColumnGroupBar.ChekedClickDown = Value;
                _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_ColumnGroupBar);
            }

        }
    });

    Object.defineProperty(this, 'ButtonImg_NavigatorToolsBar_ChekedClickDown', {
        get: function () {
            return this.ButtonImg_NavigatorToolsBar.ChekedClickDown;
        },
        set: function (Value) {
            //if (this.ButtonImg_NavigatorToolsBar.ChekedClickDown != Value) {
                this.ButtonImg_NavigatorToolsBar.ChekedClickDown = Value;
                _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_NavigatorToolsBar);
            //}

        }
    });
    Object.defineProperty(this, 'ButtonImg_EditToolsBar_ChekedClickDown', {
        get: function () {
            return this.ButtonImg_EditToolsBar.ChekedClickDown;

        },
        set: function (Value) {
            var tempChecked = this.ButtonImg_EditToolsBar.ChekedClickDown;
            this.ButtonImg_EditToolsBar.ChekedClickDown = Value;
            if (tempChecked != Value) {
                _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_EditToolsBar);
            }
            this.ChekedAllOptionEditTool();
        }
    });

    this.ChekedAllOptionEditTool = function () {

        var value = this.ButtonImg_EditToolsBar.ChekedClickDown;

        if (this.ButtonImg_DeleteBar_Active) {
            this.ButtonImg_DeleteBar_Visible = value;
        }
        if (this.ButtonImg_AddBar_Active) {
            this.ButtonImg_AddBar_Visible = value;
        }
        if (this.ButtonImg_EditBar_Active) {
            this.ButtonImg_EditBar_Visible = value;
        }

        this.ButtonImg_PostBar_Visible = value;
        this.ButtonImg_CancelBar_Visible = value;

    }

    Object.defineProperty(this, 'ButtonImg_DatailsViewBar_ChekedClickDown', {
        get: function () {
            return this.ButtonImg_DatailsViewBar.ChekedClickDown;
        },
        set: function (Value) {
            if (this.ButtonImg_DatailsViewBar.ChekedClickDown != Value) {
                this.ButtonImg_DatailsViewBar.ChekedClickDown = Value;
                _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_DatailsViewBar);
            }

        }
    });
    Object.defineProperty(this, 'ButtonImg_Grid_ChekedClickDown', {
        get: function () {
            return this.ButtonImg_Grid.ChekedClickDown;
        },
        set: function (Value) {
            var tempCheck = this.ButtonImg_Grid.ChekedClickDown;
            this.ButtonImg_Grid.ChekedClickDown = Value;
            if (tempCheck != Value) {
                _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_Grid);
            }
        }
    });
    Object.defineProperty(this, 'ButtonImg_StatusBar_ChekedClickDown', {
        get: function () {
            return this.ButtonImg_StatusBar.ChekedClickDown;
        },
        set: function (Value) {
            var tempCheck = this.ButtonImg_StatusBar.ChekedClickDown;
            this.ButtonImg_StatusBar.ChekedClickDown = Value;
            if (tempCheck != Value) {
                _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_StatusBar);
            }
        }
    });
    Object.defineProperty(this, 'ButtonImg_GridVisibleColumnBar_ChekedClickDown', {
        get: function () {
            return this.ButtonImg_GridVisibleColumnBar.ChekedClickDown;
        },
        set: function (Value) {
            //if (this.ButtonImg_GridVisibleColumnBar.ChekedClickDown != Value) {
                this.ButtonImg_GridVisibleColumnBar.ChekedClickDown = Value;
                _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_GridVisibleColumnBar);
            //}

        }
    });
    Object.defineProperty(this, 'ButtonImg_GridDetailsVisibleColumnBar_ChekedClickDown', {
        get: function () {
            return this.ButtonImg_GridDetailsVisibleColumnBar.ChekedClickDown;
        },
        set: function (Value) {
            //if (this.ButtonImg_GridDetailsVisibleColumnBar.ChekedClickDown != Value) {
                this.ButtonImg_GridDetailsVisibleColumnBar.ChekedClickDown = Value;
                _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_GridDetailsVisibleColumnBar);
            //}

        }
    });
    Object.defineProperty(this, 'ButtonImg_RowCheckToolsBar_ChekedClickDown', {
        get: function () {
            return this.ButtonImg_RowCheckToolsBar.ChekedClickDown;
        },
        set: function (Value) {
            var tempCheck = this.ButtonImg_RowCheckToolsBar.ChekedClickDown;
            this.ButtonImg_RowCheckToolsBar.ChekedClickDown = Value;
            if (tempCheck != Value) {
                _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_RowCheckToolsBar);
            }

        }
    });


    //****************************************************************************************************************************************************************
    _this.NavTabControls = new ItHelpCenter.Componet.NavTabControls.TNavTabControls(this.Object, null);
    _this.NavTabControls.VerticalPosition = false;//Posición del NavTabControls
    _this.NavTabControls.PaddingTopNavBar = 10;//Padding entre Top/Bottom 
    _this.NavTabControls.PaddingNavBar = 10;//Padding entre Right/Left 
    _this.Mythis = "TGridCFToolbar";
    /*Start Tabs*/
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1", "Home");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2", "Data");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3", "Design");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines4", "View");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines5", "Font");
    /*End Tabs*/

    /*Start Tabs: Home*/
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines6", "Export");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines7", "Selection");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines8", "Group");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines9", "Navigator");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines10", "Refresh");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines11", "Pdf");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines12", "Excel");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines13", "Word");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines14", "Row Check");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines15", "Set Group");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines16", "Expand");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines17", "Collapse");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines18", "First");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines19", "Back");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines20", "Next");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines21", "Last");
    /*End Tabs: Home*/

    /*Start Tabs: Data*/
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines22", "Tools");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines23", "View");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines24", "Navigator");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines25", "Insert");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines26", "Update");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines27", "Delete");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines28", "Accept");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines29", "Cancel");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines30", "Grid Row Tools");    
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines31", "First");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines32", "Back");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines33", "Next");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines34", "Last");
    /*End Tabs: Home*/

    /*Start Tabs: Design*/
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines35", "Column");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines36", "Size");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines37", "Move");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines38", "Edit Header");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines39", "Visible Grid");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines40", "Visible Detail");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines41", "Expand");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines42", "Stretch");
    /*End Tabs: Design*/

    /*Start Tabs: View*/
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines43", "Enable");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines44", "Grid");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines45", "Status");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines46", "Navigator");
    
    /*End Tabs: View*/

    /*Start Tabs: Font*/
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines47", "Style");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines48", "Align");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines49", "Color");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines50", "Cursive");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines51", "Bold");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines52", "Center");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines53", "Justify");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines54", "Left");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines55", "Right");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines56", "Head");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines57", "Body");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines58", "Details");
    /*End Tabs: Font*/


    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines59", "Rows to display");
    /*Creamos TTabPage*/
    this.TabPagHome = new TTabPage();
    this.TabPagHome.Name = "Home";
    this.TabPagHome.Caption = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1");
    this.TabPagHome.Active = true;
    this.TabPagHome.Tag = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1");
    this.TabPagData = new TTabPage();
    this.TabPagData.Name = "Data";
    this.TabPagData.Caption = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2");
    this.TabPagData.Active = false;
    this.TabPagData.Tag = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2");
    this.TabPagDesign = new TTabPage();
    this.TabPagDesign.Name = "Design";
    this.TabPagDesign.Caption = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3");
    this.TabPagDesign.Active = false;
    this.TabPagDesign.Tag = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3");
    this.TabPagView = new TTabPage();
    this.TabPagView.Name = "View";
    this.TabPagView.Caption = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines4");
    this.TabPagView.Active = false;
    this.TabPagView.Tag = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines4");
    _this.NavTabControls.AddTabPages(this.TabPagHome);
    _this.NavTabControls.AddTabPages(this.TabPagData);
    _this.NavTabControls.AddTabPages(this.TabPagDesign);
    _this.NavTabControls.AddTabPages(this.TabPagView);
    _this.NavTabControls.AutoScroll = false;//Desactiva la creacion del scroll

    /*Seccion Home*/

    _this.NavPanelBarHome_Refresh = new ItHelpCenter.Componet.NavTabControls.TNavPanelBar(this.TabPagHome);
    _this.NavPanelBarHome_Refresh.Title = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
    _this.NavPanelBarHome_Export = new ItHelpCenter.Componet.NavTabControls.TNavPanelBar(this.TabPagHome);
    _this.NavPanelBarHome_Export.Title = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines6");

    _this.NavPanelBarHome_Selection = new ItHelpCenter.Componet.NavTabControls.TNavPanelBar(this.TabPagHome);
    _this.NavPanelBarHome_Selection.Title = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines7");
    _this.NavPanelBarHomeGroup = new ItHelpCenter.Componet.NavTabControls.TNavPanelBar(this.TabPagHome);
    _this.NavPanelBarHomeGroup.Title = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines8");
    _this.NavPanelBarHomeNavigator = new ItHelpCenter.Componet.NavTabControls.TNavPanelBar(this.TabPagHome);
    _this.NavPanelBarHomeNavigator.Title = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines9");

    _this.ButtonImg_ExportPDFBar = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.ButtonImg_ExportPDFBar.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines11");
    _this.ButtonImg_ExportPDFBar.SrcImg = "image/24/Pdf3.png";
    _this.ButtonImg_ExportPDFBar.Opacity = "1";
    _this.ButtonImg_ExportPDFBar.TextColor = "#757575";
    //_this.ButtonImg_ExportPDFBar.ToolTip = "Export document";
    _this.ButtonImg_ExportPDFBar.onclick = function (NavBar) {
        _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_ExportPDFBar);
    }

    _this.ButtonImg_ExportExcelBar = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.ButtonImg_ExportExcelBar.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines12");
    _this.ButtonImg_ExportExcelBar.SrcImg = "image/24/Excel2.png";
    _this.ButtonImg_ExportExcelBar.Opacity = "1";
    _this.ButtonImg_ExportExcelBar.TextColor = "#757575";
    _this.ButtonImg_ExportExcelBar.onclick = function (NavBar) {
        _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_ExportExcelBar);
    }

    _this.ButtonImg_ExportWordBar = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.ButtonImg_ExportWordBar.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines13");
    _this.ButtonImg_ExportWordBar.SrcImg = "image/24/Word2.png";
    _this.ButtonImg_ExportWordBar.Opacity = "1";
    _this.ButtonImg_ExportWordBar.TextColor = "#757575";
    _this.ButtonImg_ExportWordBar.onclick = function (NavBar) {
        _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_ExportWordBar);

    }

    _this.ButtonImg_ColumnGroupBar = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.ButtonImg_ColumnGroupBar.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines15");
    //_this.ButtonImg_ColumnGroupBar.SrcImg = "image/24/placeanorder3.png";
    _this.ButtonImg_ColumnGroupBar.SrcImg = "image/24/Organization.png";
    _this.ButtonImg_ColumnGroupBar.Opacity = "1";
    _this.ButtonImg_ColumnGroupBar.TextColor = "#757575";
    _this.ButtonImg_ColumnGroupBar.ChekedButton = true;
    _this.ButtonImg_ColumnGroupBar.onclick = function (NavBar) {
        _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_ColumnGroupBar);

    }

    _this.ButtonImg_ExpanderBar = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.ButtonImg_ExpanderBar.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines16");
    _this.ButtonImg_ExpanderBar.SrcImg = "image/24/Gealogy-view2_Collapse.png";
    _this.ButtonImg_ExpanderBar.Opacity = "1";
    _this.ButtonImg_ExpanderBar.TextColor = "#757575";
    _this.ButtonImg_ExpanderBar.onclick = function (NavBar) {
        _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_ExpanderBar);

    }
    _this.ButtonImg_ContraerBar = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.ButtonImg_ContraerBar.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines17");
    _this.ButtonImg_ContraerBar.SrcImg = "image/24/Gealogy-view2.png";
    _this.ButtonImg_ContraerBar.Opacity = "1";
    _this.ButtonImg_ContraerBar.TextColor = "#757575";
    _this.ButtonImg_ContraerBar.onclick = function (NavBar) {
        _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_ContraerBar);

    }
    _this.ButtonImg_RowCheckToolsBar = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.ButtonImg_RowCheckToolsBar.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines14");
    _this.ButtonImg_RowCheckToolsBar.SrcImg = "image/24/Place-an-order-listCheck.png";
    _this.ButtonImg_RowCheckToolsBar.Opacity = "1";
    _this.ButtonImg_RowCheckToolsBar.ChekedButton = true;
    _this.ButtonImg_RowCheckToolsBar.TextColor = "#757575";
    _this.ButtonImg_RowCheckToolsBar.onclick = function (NavBar) {
        _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_RowCheckToolsBar);

    }


    _this.ButtonImg_PriorFirstBar = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.ButtonImg_PriorFirstBar.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines18");
    _this.ButtonImg_PriorFirstBar.SrcImg = "image/24/Skip-backward.png";
    _this.ButtonImg_PriorFirstBar.Opacity = "1";
    _this.ButtonImg_PriorFirstBar.TextColor = "#757575";
    _this.ButtonImg_PriorFirstBar.onclick = function (NavBar) {

        _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_PriorFirstBar);
    }
    _this.ButtonImg_PriorBar = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.ButtonImg_PriorBar.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines19");
    _this.ButtonImg_PriorBar.SrcImg = "image/24/Fast-backward.png";
    _this.ButtonImg_PriorBar.Opacity = "1";
    _this.ButtonImg_PriorBar.TextColor = "#757575";
    _this.ButtonImg_PriorBar.onclick = function (NavBar) {
        _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_PriorBar);
    }
    _this.ButtonImg_NextBar = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.ButtonImg_NextBar.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines20");
    _this.ButtonImg_NextBar.SrcImg = "image/24/Fast-forward.png";
    _this.ButtonImg_NextBar.Opacity = "1";
    _this.ButtonImg_NextBar.TextColor = "#757575";
    _this.ButtonImg_NextBar.onclick = function (NavBar) {
        _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_NextBar);
    }
    _this.ButtonImg_NextLastBar = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.ButtonImg_NextLastBar.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines21");
    _this.ButtonImg_NextLastBar.SrcImg = "image/24/Skip-forward.png";
    _this.ButtonImg_NextLastBar.Opacity = "1";
    _this.ButtonImg_NextLastBar.TextColor = "#757575";
    _this.ButtonImg_NextLastBar.onclick = function (NavBar) {
        _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_NextLastBar);
    }

    _this.NavPanelBarHome_Export.addItemBar(_this.ButtonImg_ExportPDFBar);
    _this.NavPanelBarHome_Export.addItemBar(_this.ButtonImg_ExportExcelBar);
    _this.NavPanelBarHome_Export.addItemBar(_this.ButtonImg_ExportWordBar);
    _this.NavPanelBarHome_Selection.addItemBar(_this.ButtonImg_RowCheckToolsBar);
    _this.NavPanelBarHomeGroup.addItemBar(_this.ButtonImg_ColumnGroupBar);
    _this.NavPanelBarHomeGroup.addItemBar(_this.ButtonImg_ExpanderBar);
    _this.NavPanelBarHomeGroup.addItemBar(_this.ButtonImg_ContraerBar);
    _this.NavPanelBarHomeNavigator.addItemBar(_this.ButtonImg_PriorFirstBar);
    _this.NavPanelBarHomeNavigator.addItemBar(_this.ButtonImg_PriorBar);
    _this.NavPanelBarHomeNavigator.addItemBar(_this.ButtonImg_NextBar);
    _this.NavPanelBarHomeNavigator.addItemBar(_this.ButtonImg_NextLastBar);



    _this.ButtonImg_Refresh = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.ButtonImg_Refresh.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines10");
    //_this.NavPanelBarHome_Refresh.
    _this.ButtonImg_Refresh.SrcImg = "image/24/Refresh.png";
    _this.ButtonImg_Refresh.Opacity = "1";
    _this.ButtonImg_Refresh.TextColor = "#757575";
    _this.ButtonImg_Refresh.onclick = function (NavBar) {
        _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_Refresh);
    }
    _this.NavPanelBarHome_Refresh.addItemBar(_this.ButtonImg_Refresh);

    /*Seccion Data*/
    _this.NavPanelBarData_Tools = new ItHelpCenter.Componet.NavTabControls.TNavPanelBar(this.TabPagData);
    _this.NavPanelBarData_Tools.Title = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines22");
    _this.NavPanelBarData_View = new ItHelpCenter.Componet.NavTabControls.TNavPanelBar(this.TabPagData);
    _this.NavPanelBarData_View.Title = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines23");
    _this.NavPanelBarHomeNavigatorB = new ItHelpCenter.Componet.NavTabControls.TNavPanelBar(this.TabPagData);
    _this.NavPanelBarHomeNavigatorB.Title = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines24");
    _this.ButtonImg_AddBar = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.ButtonImg_AddBar.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines25");
    _this.ButtonImg_AddBar.SrcImg = "image/24/Add1.png";
    _this.ButtonImg_AddBar.Opacity = "0.85";
    _this.ButtonImg_AddBar.TextColor = "#757575";
    _this.ButtonImg_AddBar.onclick = function (NavBar) {
        _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_AddBar);

    }

    _this.ButtonImg_EditBar = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.ButtonImg_EditBar.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines26");
    _this.ButtonImg_EditBar.SrcImg = "image/24/edit.png";
    _this.ButtonImg_EditBar.Opacity = "0.85";
    _this.ButtonImg_EditBar.TextColor = "#757575";
    _this.ButtonImg_EditBar.onclick = function (NavBar) {
        _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_EditBar);

    }

    _this.ButtonImg_DeleteBar = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.ButtonImg_DeleteBar.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines27");
    _this.ButtonImg_DeleteBar.SrcImg = "image/24/Close.png";
    _this.ButtonImg_DeleteBar.Opacity = "0.85";
    _this.ButtonImg_DeleteBar.TextColor = "#757575";
    _this.ButtonImg_DeleteBar.onclick = function (NavBar) {
        _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_DeleteBar);

    }

    _this.ButtonImg_PostBar = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.ButtonImg_PostBar.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines28");
    _this.ButtonImg_PostBar.SrcImg = "image/24/Save-as.png";
    _this.ButtonImg_PostBar.TextColor = "#757575";
    _this.ButtonImg_PostBar.onclick = function (NavBar) {
        _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_PostBar);

    }

    _this.ButtonImg_CancelBar = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.ButtonImg_CancelBar.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines29");
    _this.ButtonImg_CancelBar.SrcImg = "image/24/Cancel.png";
    _this.ButtonImg_CancelBar.TextColor = "#757575";
    _this.ButtonImg_CancelBar.onclick = function (NavBar) {
        _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_CancelBar);

    }
    _this.ButtonImg_RowEditToolsBar = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.ButtonImg_RowEditToolsBar.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines30");
    _this.ButtonImg_RowEditToolsBar.SrcImg = "image/24/Ordering.png";
    _this.ButtonImg_RowEditToolsBar.TextColor = "#757575";
    _this.ButtonImg_RowEditToolsBar.ChekedButton = true;
    _this.ButtonImg_RowEditToolsBar.onclick = function (NavBar) {
        _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_RowEditToolsBar);

    }

    _this.ButtonImg_PriorFirstBarB = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.ButtonImg_PriorFirstBarB.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines31");
    _this.ButtonImg_PriorFirstBarB.SrcImg = "image/24/Skip-backward.png";
    _this.ButtonImg_PriorFirstBarB.Opacity = "1";
    _this.ButtonImg_PriorFirstBarB.TextColor = "#757575";
    _this.ButtonImg_PriorFirstBarB.onclick = function (NavBar) {

        _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_PriorFirstBar);
    }
    _this.ButtonImg_PriorBarB = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.ButtonImg_PriorBarB.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines32");
    _this.ButtonImg_PriorBarB.SrcImg = "image/24/Fast-backward.png";
    _this.ButtonImg_PriorBarB.Opacity = "1";
    _this.ButtonImg_PriorBarB.TextColor = "#757575";
    _this.ButtonImg_PriorBarB.onclick = function (NavBar) {
        _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_PriorBar);
    }
    _this.ButtonImg_NextBarB = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.ButtonImg_NextBarB.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines33");
    _this.ButtonImg_NextBarB.SrcImg = "image/24/Fast-forward.png";
    _this.ButtonImg_NextBarB.Opacity = "1";
    _this.ButtonImg_NextBarB.TextColor = "#757575";
    _this.ButtonImg_NextBarB.onclick = function (NavBar) {
        _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_NextBar);
    }
    _this.ButtonImg_NextLastBarB = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.ButtonImg_NextLastBarB.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines34");
    _this.ButtonImg_NextLastBarB.SrcImg = "image/24/Skip-forward.png";
    _this.ButtonImg_NextLastBarB.Opacity = "1";
    _this.ButtonImg_NextLastBarB.TextColor = "#757575";
    _this.ButtonImg_NextLastBarB.onclick = function (NavBar) {

        _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_NextLastBar);
    }


    _this.NavPanelBarData_Tools.addItemBar(_this.ButtonImg_AddBar);
    _this.NavPanelBarData_Tools.addItemBar(_this.ButtonImg_EditBar);
    _this.NavPanelBarData_Tools.addItemBar(_this.ButtonImg_DeleteBar);
    _this.NavPanelBarData_Tools.addItemBar(_this.ButtonImg_PostBar);
    _this.NavPanelBarData_Tools.addItemBar(_this.ButtonImg_CancelBar);
    _this.NavPanelBarData_View.addItemBar(_this.ButtonImg_RowEditToolsBar);
    _this.NavPanelBarHomeNavigatorB.addItemBar(_this.ButtonImg_PriorFirstBarB);
    _this.NavPanelBarHomeNavigatorB.addItemBar(_this.ButtonImg_PriorBarB);
    _this.NavPanelBarHomeNavigatorB.addItemBar(_this.ButtonImg_NextBarB);
    _this.NavPanelBarHomeNavigatorB.addItemBar(_this.ButtonImg_NextLastBarB);

    /*Seccion Design*/
    _this.NavPanelBarDesign_Column = new ItHelpCenter.Componet.NavTabControls.TNavPanelBar(this.TabPagDesign);
    _this.NavPanelBarDesign_Column.Title = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines35");
    _this.NavPanelBarDesign_Size = new ItHelpCenter.Componet.NavTabControls.TNavPanelBar(this.TabPagDesign);
    _this.NavPanelBarDesign_Size.Title = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines36");

    _this.ButtonImg_MoveColumnsBar = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.ButtonImg_MoveColumnsBar.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines37");
    _this.ButtonImg_MoveColumnsBar.SrcImg = "image/24/Import-export.png";
    _this.ButtonImg_MoveColumnsBar.TextColor = "#757575";
    _this.ButtonImg_MoveColumnsBar.onclick = function (NavBar) {
        _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_MoveColumnsBar);

    }

    _this.ButtonImg_EditColumHeader = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.ButtonImg_EditColumHeader.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines38");
    _this.ButtonImg_EditColumHeader.SrcImg = "image/24/Rename.png";
    _this.ButtonImg_EditColumHeader.TextColor = "#757575";
    _this.ButtonImg_EditColumHeader.onclick = function (NavBar) {
        _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_EditColumHeader);

    }

    _this.ButtonImg_GridVisibleColumnBar = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.ButtonImg_GridVisibleColumnBar.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines39");
    _this.ButtonImg_GridVisibleColumnBar.SrcImg = "image/24/Options.png";
    _this.ButtonImg_GridVisibleColumnBar.TextColor = "#757575";
    _this.ButtonImg_GridVisibleColumnBar.onclick = function (NavBar) {
        _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_GridVisibleColumnBar);

    }

    _this.ButtonImg_GridDetailsVisibleColumnBar = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.ButtonImg_GridDetailsVisibleColumnBar.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines40");
    _this.ButtonImg_GridDetailsVisibleColumnBar.SrcImg = "image/24/Options-details.png";
    _this.ButtonImg_GridDetailsVisibleColumnBar.TextColor = "#757575";
    _this.ButtonImg_GridDetailsVisibleColumnBar.onclick = function (NavBar) {
        _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_GridDetailsVisibleColumnBar);

    }


    _this.ButtonImg_ExpanderColumnsBar = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.ButtonImg_ExpanderColumnsBar.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines41");
    _this.ButtonImg_ExpanderColumnsBar.SrcImg = "image/24/arrows2.png";
    _this.ButtonImg_ExpanderColumnsBar.Opacity = "1";
    _this.ButtonImg_ExpanderColumnsBar.ChekedButton = true;
    _this.ButtonImg_ExpanderColumnsBar.TextColor = "#757575";
    _this.ButtonImg_ExpanderColumnsBar.onclick = function (NavBar) {
        _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_ExpanderColumnsBar);


    }
    _this.ButtonImg_ContraerColumnsBar = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.ButtonImg_ContraerColumnsBar.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines42");
    _this.ButtonImg_ContraerColumnsBar.SrcImg = "image/24/Gealogy-view2.png";
    _this.ButtonImg_ContraerColumnsBar.Opacity = "1";
    _this.ButtonImg_ContraerColumnsBar.TextColor = "#757575";
    _this.ButtonImg_ContraerColumnsBar.ChekedButton = true;
    _this.ButtonImg_ContraerColumnsBar.Visible = false;
    _this.ButtonImg_ContraerColumnsBar.onclick = function (NavBar) {

        _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_ContraerColumnsBar);

    }

    _this.NavPanelBarDesign_Column.addItemBar(_this.ButtonImg_MoveColumnsBar);
    _this.NavPanelBarDesign_Column.addItemBar(_this.ButtonImg_EditColumHeader);
    _this.NavPanelBarDesign_Column.addItemBar(_this.ButtonImg_GridVisibleColumnBar);
    _this.NavPanelBarDesign_Column.addItemBar(_this.ButtonImg_GridDetailsVisibleColumnBar);
    _this.NavPanelBarDesign_Size.addItemBar(_this.ButtonImg_ExpanderColumnsBar);
    _this.NavPanelBarDesign_Size.addItemBar(_this.ButtonImg_ContraerColumnsBar);

    /*Seccion View*/
    _this.NavPanelBarView_Enabled = new ItHelpCenter.Componet.NavTabControls.TNavPanelBar(this.TabPagView);
    _this.NavPanelBarView_Enabled.Title = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines43");
    
    _this.ButtonImg_DatailsViewBar = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.ButtonImg_DatailsViewBar.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines58", "Details");
    _this.ButtonImg_DatailsViewBar.SrcImg = "image/24/Place-an-order_ProductSaleReport2.png";
    _this.ButtonImg_DatailsViewBar.TextColor = "#757575";
    _this.ButtonImg_DatailsViewBar.ChekedButton = true;
    _this.ButtonImg_DatailsViewBar.onclick = function (NavBar) {
        _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_DatailsViewBar);

    }

    _this.ButtonImg_Grid = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.ButtonImg_Grid.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines44");
    _this.ButtonImg_Grid.SrcImg = "image/24/Place-an-order.png";
    _this.ButtonImg_Grid.TextColor = "#757575";
    _this.ButtonImg_Grid.ChekedButton = true;
    _this.ButtonImg_Grid.Cheked = true;
    _this.ButtonImg_Grid.onclick = function (NavBar) {
        _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_Grid);

    }
    
    _this.ButtonImg_StatusBar = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.ButtonImg_StatusBar.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines45");
    _this.ButtonImg_StatusBar.SrcImg = "image/24/Process-info.png";
    _this.ButtonImg_StatusBar.Opacity = "1";
    _this.ButtonImg_StatusBar.TextColor = "#757575";
    _this.ButtonImg_StatusBar.ChekedButton = true;
    _this.ButtonImg_StatusBar.onclick = function (NavBar) {

        _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_StatusBar);

    }
    _this.ButtonImg_EditToolsBar = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.ButtonImg_EditToolsBar.Text = "Edit Tools eliminar";
    _this.ButtonImg_EditToolsBar.SrcImg = "image/24/Gealogy-view2.png";
    _this.ButtonImg_EditToolsBar.Opacity = "1";
    _this.ButtonImg_EditToolsBar.TextColor = "#757575";
    _this.ButtonImg_EditToolsBar.ChekedButton = true;
    _this.ButtonImg_EditToolsBar.onclick = function (NavBar) {
        _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_EditToolsBar);
        _this.ChekedAllOptionEditTool();


    }
    _this.ButtonImg_NavigatorToolsBar = new ItHelpCenter.Componet.NavTabControls.TNavBar();
    _this.ButtonImg_NavigatorToolsBar.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines46");
    _this.ButtonImg_NavigatorToolsBar.SrcImg = "image/24/arrows1.png";
    _this.ButtonImg_NavigatorToolsBar.Opacity = "1";
    _this.ButtonImg_NavigatorToolsBar.TextColor = "#757575";
    _this.ButtonImg_NavigatorToolsBar.ChekedButton = true;
    _this.ButtonImg_NavigatorToolsBar.onclick = function (NavBar) {
        _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_NavigatorToolsBar);


    }

    /********/
    var toolShowRowsPaginationSection = new ItHelpCenter.Componet.NavTabControls.TNavPanelBar(this.TabPagView);
    toolShowRowsPaginationSection.Title = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines59");
    var toolRowsPagination = new ItHelpCenter.Componet.NavTabControls.TNavBarStackPanel();
    
    toolRowsPagination.TextColor = "#757575";
    _this.VclComboBox_RowsShowToolsBar = new TVclComboBox2(toolRowsPagination.Panel, "");
    toolShowRowsPaginationSection.addItemBar(toolRowsPagination);
    _this.VclComboBox_RowsShowToolsBar.This.classList.add("form-control");
    _this.VclComboBox_RowsShowToolsBar.This.style.height="43px";
    _this.NavPanelBarView_Enabled.addItemBar(_this.ButtonImg_Grid);
    _this.NavPanelBarView_Enabled.addItemBar(_this.ButtonImg_DatailsViewBar);
    _this.NavPanelBarView_Enabled.addItemBar(_this.ButtonImg_StatusBar);
    _this.NavPanelBarView_Enabled.addItemBar(_this.ButtonImg_NavigatorToolsBar);
    

    if (1 == 1) {
        var TabFont = new TTabPage();
        TabFont.Name = "Font";
        TabFont.Caption = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines5");
        TabFont.Active = false;
        TabFont.Tag = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines5");

        _this.NavTabControls.AddTabPages(TabFont);

        _this.NavPanelBarFont_Grid = new ItHelpCenter.Componet.NavTabControls.TNavPanelBar(TabFont);
        _this.NavPanelBarFont_Grid.Title = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines47");
        _this.NavPanelBarFont_Align = new ItHelpCenter.Componet.NavTabControls.TNavPanelBar(TabFont);
        _this.NavPanelBarFont_Align.Title = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines48");


        _this.NavGroupBarFuente = new ItHelpCenter.Componet.NavTabControls.TNavGroupBar();
        _this.NavGroupBarTamanio = new ItHelpCenter.Componet.NavTabControls.TNavGroupBar();
        var Componet_GridCF_FontSize = SysCfg.App.IniDB.ReadString("Componet_GridCF", "FontSize", null);
        var Componet_GridCF_FontFamily = SysCfg.App.IniDB.ReadString("Componet_GridCF", "FontFamily", null);
        var Componet_GridCF_FontWeight = SysCfg.App.IniDB.ReadString("Componet_GridCF", "FontWeight", null) + "";
        var Componet_GridCF_FontStyle = SysCfg.App.IniDB.ReadString("Componet_GridCF", "FontStyle", null) + "";
        var Componet_GridCF_HeaderColor = SysCfg.App.IniDB.ReadString("Componet_GridCF", "HeaderColor", null) + "";
        var Componet_GridCF_BodyColor = SysCfg.App.IniDB.ReadString("Componet_GridCF", "BodyColor", null) + "";
        var Componet_GridCF_TextAlign = SysCfg.App.IniDB.ReadString("Componet_GridCF", "TextAlign", null) + "";


        _this.ItemGroupBarFuente = new ItHelpCenter.Componet.NavTabControls.TItemGroupBar();
        _this.ItemGroupBarFuente.Text = (Componet_GridCF_FontFamily != "" && Componet_GridCF_FontFamily != null) ? Componet_GridCF_FontFamily + "" : "normal";
        _this.ItemGroupBarFuente.Width = "24px";
        _this.ItemGroupBarFuente.SrcImg = "image/24/font-color.png";

        _this.ItemGroupBarTamanio = new ItHelpCenter.Componet.NavTabControls.TItemGroupBar();
        _this.ItemGroupBarTamanio.Text = Componet_GridCF_FontSize + "";
        _this.ItemGroupBarTamanio.Width = "22px";
        _this.ItemGroupBarTamanio.SrcImg = "image/24/font.png";
        var functionFuente = function (NavBar) {
            //_this.ItemGroupBarFuente.Text = NavBar.Text;
            _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_Font);
        }
        var funcitionTamanio = function (NavBar) {
            //_this.ItemGroupBarTamanio.Text = NavBar.Text;
            _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_Size);
        }
        //var arrayFuentes = ["serif", "sans-serif", "cursive", "fantasy", "monospace", "normal"];
        var arrayFuentes = ["Agency FB", "Antiqua", "Architect", "Arial", "BankFuturistic",
            "BankGothic",
            "Blackletter",
            "Calibri",
            "Comic Sans",
            "Courier",
            "Cursiva",
            "Decorativa",
            "fantasy",
            "Fraktur",
            "BankFuturistic", "monospace", "normal", "Times New Roman"];
        var arrayTamanios = ["8", "10", "12", "13","14", "16", "18", "20", "22", "24", "26", "28"];

        for (var i = 0; i < arrayFuentes.length; i++) {
            var ItemFuente = new ItHelpCenter.Componet.NavTabControls.TItemGroupBar();
            ItemFuente.Text = arrayFuentes[i];
            ItemFuente.SrcImg = "";
            ItemFuente.onclick = functionFuente;
            _this.ItemGroupBarFuente.addItemGroupBar(ItemFuente);
        }
        for (var i = 0; i < arrayTamanios.length; i++) {
            var ItemTamanio = new ItHelpCenter.Componet.NavTabControls.TItemGroupBar();
            ItemTamanio.Text = arrayTamanios[i];
            ItemTamanio.SrcImg = "";
            ItemTamanio.onclick = funcitionTamanio;
            _this.ItemGroupBarTamanio.addItemGroupBar(ItemTamanio);
        }

        _this.NavGroupBarFuente.addItemGroupBar(_this.ItemGroupBarFuente);
        _this.NavGroupBarTamanio.addItemGroupBar(_this.ItemGroupBarTamanio);

        _this.ButtonImg_Kursive = new ItHelpCenter.Componet.NavTabControls.TNavBar();
        _this.ButtonImg_Kursive.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines50");
        _this.ButtonImg_Kursive.SrcImg = "image/24/Text-itailc.png";
        _this.ButtonImg_Kursive.Opacity = "1";
        _this.ButtonImg_Kursive.TextColor = "#757575";
        _this.ButtonImg_Kursive.ChekedButton = true;

        _this.ButtonImg_Kursive.Cheked = (Componet_GridCF_FontStyle + "" == "italic");
        _this.ButtonImg_Kursive.onclick = function (NavBar) {
            _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_Kursive);

        }

        _this.ButtonImg_Bold = new ItHelpCenter.Componet.NavTabControls.TNavBar();
        _this.ButtonImg_Bold.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines51");
        _this.ButtonImg_Bold.SrcImg = "image/24/Text-bold.png";
        _this.ButtonImg_Bold.Opacity = "1";
        _this.ButtonImg_Bold.TextColor = "#757575";
        _this.ButtonImg_Bold.ChekedButton = true;
        _this.ButtonImg_Bold.Cheked = (Componet_GridCF_FontWeight + "" == "bold");
        _this.ButtonImg_Bold.onclick = function (NavBar) {
            _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_Bold);
        }

        _this.ButtonImg_AlignCenter = new ItHelpCenter.Componet.NavTabControls.TNavBar();
        _this.ButtonImg_AlignCenter.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines52");
        _this.ButtonImg_AlignCenter.SrcImg = "image/24/Text-align-center.png";
        _this.ButtonImg_AlignCenter.Opacity = "1";
        _this.ButtonImg_AlignCenter.TextColor = "#757575";
        _this.ButtonImg_AlignCenter.ChekedButton = true;
        _this.ButtonImg_AlignCenter.Cheked = (Componet_GridCF_TextAlign + "" == "center");
        _this.ButtonImg_AlignCenter.onclick = function (NavBar) {
            _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_AlignCenter);
        }

        _this.ButtonImg_AlignJustify = new ItHelpCenter.Componet.NavTabControls.TNavBar();
        _this.ButtonImg_AlignJustify.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines53");
        _this.ButtonImg_AlignJustify.SrcImg = "image/24/Text-align-justify.png";
        _this.ButtonImg_AlignJustify.Opacity = "1";
        _this.ButtonImg_AlignJustify.TextColor = "#757575";
        _this.ButtonImg_AlignJustify.ChekedButton = true;
        _this.ButtonImg_AlignJustify.Cheked = (Componet_GridCF_TextAlign + "" == "justify");
        _this.ButtonImg_AlignJustify.onclick = function (NavBar) {
            _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_AlignJustify);
        }


        _this.ButtonImg_AlignLeft = new ItHelpCenter.Componet.NavTabControls.TNavBar();
        _this.ButtonImg_AlignLeft.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines54");
        _this.ButtonImg_AlignLeft.SrcImg = "image/24/Text-align-left.png";
        _this.ButtonImg_AlignLeft.Opacity = "1";
        _this.ButtonImg_AlignLeft.TextColor = "#757575";
        _this.ButtonImg_AlignLeft.ChekedButton = true;
        _this.ButtonImg_AlignLeft.Cheked = (Componet_GridCF_TextAlign + "" == "left");
        _this.ButtonImg_AlignLeft.onclick = function (NavBar) {
            _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_AlignLeft);
        }

        _this.ButtonImg_AlignRight = new ItHelpCenter.Componet.NavTabControls.TNavBar();
        _this.ButtonImg_AlignRight.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines55");
        _this.ButtonImg_AlignRight.SrcImg = "image/24/Text-align-right.png";
        _this.ButtonImg_AlignRight.Opacity = "1";
        _this.ButtonImg_AlignRight.TextColor = "#757575";
        _this.ButtonImg_AlignRight.ChekedButton = true;
        _this.ButtonImg_AlignRight.Cheked = (Componet_GridCF_TextAlign + "" == "right");
        _this.ButtonImg_AlignRight.onclick = function (NavBar) {
            _this.Click(_this, Componet.GridCF.TToolbarControlls.ButtonImg_AlignRight);
        }
        _this.ButtonImg_FontColor = new ItHelpCenter.Componet.NavTabControls.TNavBarStackPanel();
        _this.ButtonImg_FontColor.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines56");
        _this.ButtonImg_FontColor.TextColor = "#757575";

        _this.ButtonImg_FontBody = new ItHelpCenter.Componet.NavTabControls.TNavBarStackPanel();
        _this.ButtonImg_FontBody.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines57");
        _this.ButtonImg_FontBody.TextColor = "#757575";

        _this.NavPanelBarFont_Grid.addItemBar(_this.ButtonImg_Kursive);
        _this.NavPanelBarFont_Grid.addItemBar(_this.ButtonImg_Bold)
        _this.NavPanelBarFont_Grid.addItemBar(_this.NavGroupBarFuente);
        _this.NavPanelBarFont_Grid.addItemBar(_this.NavGroupBarTamanio);

        _this.NavPanelBarFont_Align.addItemBar(_this.ButtonImg_AlignCenter);
        _this.NavPanelBarFont_Align.addItemBar(_this.ButtonImg_AlignJustify)
        _this.NavPanelBarFont_Align.addItemBar(_this.ButtonImg_AlignLeft);
        _this.NavPanelBarFont_Align.addItemBar(_this.ButtonImg_AlignRight);


        _this.NavPanelBarFont_Color = new ItHelpCenter.Componet.NavTabControls.TNavPanelBar(TabFont);
        _this.NavPanelBarFont_Color.Title = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines49");
        _this.NavPanelBarFont_Color.addItemBar(_this.ButtonImg_FontColor);
        _this.NavPanelBarFont_Color.addItemBar(_this.ButtonImg_FontBody);

    }
    
    this.onClick = null;
}

Componet.GridCF.TGridCFToolbar.prototype.Click = function (Obj, ToolbarControlls) {
    //Función para enviar por fuera los eventos producidos en el ToolBar.
    var _this = this.TParent();
    if (_this.onClick != null) {
        _this.onClick(Obj, ToolbarControlls);//Enviamos evento
    }
}