﻿/*Componente TGridCFView */
Componet.GridCF.TGridCFView = function (inObject, incallbackModalResult, GridCFToolbar) {
    $(inObject).html("");
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.Mythis = "TGridCFView";
    //Traducción
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1", "Invalid format");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2", "Successful export");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3", "Select a field to download the file");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines4", "Row empty");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines5", "There is no option");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines6", "The file could not be downloaded");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines7", "File code");//Nueva traducción
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines8", "Enter file description");//Nuevas traducción
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines9", "Upload File");//Nueva traducción
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines10", "Upload");//Nueva traducción
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines11", "Browse");//Nueva traducción
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines12", "Download");//Nueva traducción
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines13", "File name");//Nueva traducción
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines14", "Creation date");//Nueva traducción
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines15", "Save");//Nueva traducción
    this.OnActiveChangeControls = false;//Inidica si se realizo un cambio al editar o insertar una celda. Esto sirve para indicarle al gridcdview si se debe o no llamar al evento post al cambiar de fila.
    this.IndexRowGridPorScroll = null;
    this.OrderColumnsDefault = new Array();
    this._Columns = new Array();//Lista de columnas CF
    this.IsLoadDataSetRefresh = false;//Indica que el evento ChangeIndex se pude ejecutar.
    this.ListStyleGrid = new Array();//Lista de estilos
    this._StyleGrid = null;//Estilos del gridcfview
    this._OrderColumns = null;//Orde de columnas
    this._SetSelectedRows = false;//True = Filas chekeas, false=filas des-chekeadas.
    this._DivitionColumnInformation = 1;// Número de columnas en que se desea dividir el detalle del grid.
    this._ListAgrupation = new Array();//Lista de agrupación.
    this._ListCustomFilter = new Array();//Lista de filtros.
    this._NumberRowVisible = 5;/*Número de filas a mostrar tanto en la paginación como en el scroll*/
    this.OrderTCfg = {//Orden del Grid
        Type: 0,
        ColumnName: null
    };
    this._AutoTranslete = false;//Traducción en las columnas.
    this.Object = inObject;//Div donde se dibuja el Componente.
    this.CallbackModalResult = incallbackModalResult;//CallbackModalResult(OutStr);        
    this.DataSet = null; //DataSet;
    this.VclGridCF = null;//Grid Principal
    this.VclTreeGridControl = null;//TreeGrid
    this._GridCFToolbar = GridCFToolbar;//GridCFToolbar
    this.IsAddColumn = false;//Indica si se agrego una columna al cfview.
    this.Information = null;//Componente que crea el grid para el detalle de la fila selecionada.
    this.Status = null;//Componente que crea el estado del dataset en el grid.
    this.VclListBoxColumns = null;//ListBox para las columnas del grid.
    this.VclListBoxInformationColumns = null;//ListBox para las columnas del detalle.
    this.VclListBoxColumnsLinq = null;//ListBox para las agrupaciones.
    this.ItemTreePanel = [];//lista de records de datata Set que contienen los ordenes por cada campo, se utiliza ademas para los filtros
    this.nameColumns = [];//Arreglo de columnas
    this.OnRefreschRecordSet = null;//evento
    this.OnRefresch = null;//evento
    this.OnBeforeChange = null;//evento
    this.OnAfterChange = null;//evento    
    this.OnAddColumn = null;//evento    
    this.OnChangeIndex = null;//evento  
    this.FilterQuery = null;//Cadena de filtro 
    this.VclModalColumnExport = null;//Modal para exportar datos
    this.VMoveColum = null;//Control para mover columnas.
    this.VclModalColumnsCustomLinq = null;//Modal para los filtros personalizados.
    this.VclModalColumnsLinq = null;//Modal para las agrupaciones.
    this.VclGridCFColumnsHeader = null;//Grid para cambiar el caption de las columnas, se crea cuando se seleciona "ButtonImg_EditColumHeader"
    this.VclStackPanelComboxTable = null;//Panel para el combo de las agrupaciones.
    this.VclStackPanelContentMemTable = null;//Panel para el grid.
    this.VclStackPanelContentInfoMenTable = null;//Panel para el detalle.
    this.ColumnaSelecionada = null;//Columna que se esta ordenando.
    this.VclDrag = null;//Componente para arrastrar elementos.
    this.IndiceLogico = null;//Indica que el orden de las filas en el grid es distinto al orden de las filas en el DataSet
    /*Propiedad para la traducción de columnas*/
    Object.defineProperty(this, 'AutoTranslete', {
        get: function () {
            return this._AutoTranslete;
        },
        set: function (inValue) {
            this._AutoTranslete = inValue;
            if (inValue) {
                for (var i = 0; i < _this.VclGridCF.Columns.length; i++) {
                    var column = _this.VclGridCF.Columns[i];
                    if (!(column.IsOptionCheck || column.IsOption)) {
                        var name = column.Name;
                        var caption = column.Caption;
                        var Header = UsrCfg.Traslate.GetLangTextdb(caption, caption);
                        column.CaptionAutoTranslete = Header;
                    }
                }
                if (_this.VclGridCF.Rows.length > 0 && _this.VclGridCF.FocusedRowHandle != null) {
                    _this.CancelMostrarInformacion(_this.VclGridCF.FocusedRowHandle);
                }
            }
        }
    });
    /*Propiedad para indicar el número de filas tanto en la paginación como en el scroll*/
    Object.defineProperty(this, 'NumberRowVisible', {
        get: function () {
            return this._NumberRowVisible;
        },
        set: function (inValue) {
            this._NumberRowVisible = inValue;
        }
    });
    /*Propiedad para indicar una lista de filtros, por ejemplo: [{VALUE:"",COLUMNAME:"",OPERATOR:"==",ANDOR:"and"}]*/
    Object.defineProperty(this, 'ListCustomFilter', {
        get: function () {
            return this._ListCustomFilter;
        },
        set: function (inValue) {
            this._ListCustomFilter = inValue;
        }
    });
    /*Propiedad para get/set Columns*/
    Object.defineProperty(this, 'Columns', {
        get: function () {
            return this._Columns;
        },
        set: function (inValue) {
            this._Columns = inValue;
        }
    });
    /*Propiedad para get/set GridCFToolbar*/
    Object.defineProperty(this, 'GridCFToolbar', {
        get: function () {
            return this._GridCFToolbar;
        },
        set: function (inValue) {
            this._GridCFToolbar = inValue;
        }
    });
    /*Propiedad para mostrar o ocultar el bar refresh*/
    Object.defineProperty(this, 'ButtonImg_Refresh_Visible', {
        get: function () {
            return this._GridCFToolbar.ButtonImg_Refresh_Visible;
        },
        set: function (inValue) {
            this._GridCFToolbar.ButtonImg_Refresh_Visible = inValue;
        }
    });
    /*Propiedad para get/set NavTabControls*/
    Object.defineProperty(this, 'NavTabControls', {
        get: function () {
            return this._GridCFToolbar.NavTabControls;
        },
    })
    /*Propiedad para aplicar las vistas al grid*/
    Object.defineProperty(this, 'StyleGrid', {
        get: function () {
            return this._StyleGrid;
        },
        set: function (inValue) {

            this._StyleGrid = inValue;
            if (_this.DataSet != null && _this._StyleGrid != null) {
                if (typeof (_this.StyleGrid.DivitionColumnInformation) != "undefined")
                    _this.DivitionColumnInformation = _this.StyleGrid.DivitionColumnInformation;

                if (typeof (_this.StyleGrid.ToolBar) != "undefined") {

                    var ToolBarButton = {
                        bandera: false,
                    };

                    if (typeof (_this.StyleGrid.ToolBar.ButtonImg_DeleteBar) != "undefined") {

                        if (typeof (_this.StyleGrid.ToolBar.ButtonImg_DeleteBar.Active) != "undefined") {
                            _this.GridCFToolbar.ButtonImg_DeleteBar_Active = _this.StyleGrid.ToolBar.ButtonImg_DeleteBar.Active;
                            ToolBarButton.DeleteActive = _this.StyleGrid.ToolBar.ButtonImg_DeleteBar.Active;
                            ToolBarButton.bandera = true;
                        }
                    }

                    if (typeof (_this.StyleGrid.ToolBar.ButtonImg_AddBar) != "undefined") {

                        if (typeof (_this.StyleGrid.ToolBar.ButtonImg_AddBar.Active) != "undefined") {
                            _this.GridCFToolbar.ButtonImg_AddBar_Active = _this.StyleGrid.ToolBar.ButtonImg_AddBar.Active;
                            ToolBarButton.InsertActive = _this.StyleGrid.ToolBar.ButtonImg_AddBar.Active;
                            ToolBarButton.bandera = true;
                        }
                    }

                    if (typeof (_this.StyleGrid.ToolBar.ButtonImg_EditBar) != "undefined") {

                        if (typeof (_this.StyleGrid.ToolBar.ButtonImg_EditBar.Active) != "undefined") {
                            _this.GridCFToolbar.ButtonImg_EditBar_Active = _this.StyleGrid.ToolBar.ButtonImg_EditBar.Active;
                            ToolBarButton.UpdateActive = _this.StyleGrid.ToolBar.ButtonImg_EditBar.Active;
                            ToolBarButton.bandera = true;
                        }
                    }


                    if (ToolBarButton.bandera) {
                        this.VclGridCF.Columns[this.VclGridCF.Columns.length - 1].ToolBarButton = ToolBarButton;
                        if (!this.VclGridCF.Columns[this.VclGridCF.Columns.length - 1].ToolBarButton.InsertActive && !this.VclGridCF.Columns[this.VclGridCF.Columns.length - 1].ToolBarButton.UpdateActive && !this.VclGridCF.Columns[this.VclGridCF.Columns.length - 1].ToolBarButton.DeleteActive) {
                            _this.StyleGrid.ToolBar.ButtonImg_EditToolsBar.Visible = false;
                            _this.StyleGrid.ToolBar.ButtonImg_RowEditToolsBar.Visible = false;
                        }
                    }

                    if (typeof (_this.StyleGrid.ToolBar.ButtonImg_EditToolsBar) != "undefined") {
                        if (typeof (_this.StyleGrid.ToolBar.ButtonImg_EditToolsBar.ChekedClickDown) != "undefined") {
                            _this.GridCFToolbar.ButtonImg_EditToolsBar_ChekedClickDown = _this.StyleGrid.ToolBar.ButtonImg_EditToolsBar.ChekedClickDown;
                        }

                        if (typeof (_this.StyleGrid.ToolBar.ButtonImg_EditToolsBar.Visible) != "undefined") {
                            _this.GridCFToolbar.ButtonImg_EditToolsBar_Visible = _this.StyleGrid.ToolBar.ButtonImg_EditToolsBar.Visible;
                            if (!_this.StyleGrid.ToolBar.ButtonImg_EditToolsBar.Visible) {
                                _this.GridCFToolbar.ButtonImg_EditToolsBar_ChekedClickDown = false;
                            }
                        }
                    }

                    if (typeof (_this.StyleGrid.ToolBar.ButtonImg_RowEditToolsBar) != "undefined") {
                        if (typeof (_this.StyleGrid.ToolBar.ButtonImg_RowEditToolsBar.ChekedClickDown) != "undefined") {
                            _this.GridCFToolbar.ButtonImg_RowEditToolsBar_ChekedClickDown = _this.StyleGrid.ToolBar.ButtonImg_RowEditToolsBar.ChekedClickDown;
                        }
                        if (typeof (_this.StyleGrid.ToolBar.ButtonImg_RowEditToolsBar.Visible) != "undefined") {
                            _this.GridCFToolbar.ButtonImg_RowEditToolsBar_Visible = _this.StyleGrid.ToolBar.ButtonImg_RowEditToolsBar.Visible;
                            if (!_this.StyleGrid.ToolBar.ButtonImg_RowEditToolsBar.Visible) {
                                _this.GridCFToolbar.ButtonImg_RowEditToolsBar_ChekedClickDown = false;
                            }
                        }
                    }

                    if (typeof (_this.StyleGrid.ToolBar.ButtonImg_DatailsViewBar) != "undefined") {
                        if (typeof (_this.StyleGrid.ToolBar.ButtonImg_DatailsViewBar.ChekedClickDown) != "undefined") {
                            _this.GridCFToolbar.ButtonImg_DatailsViewBar_ChekedClickDown = _this.StyleGrid.ToolBar.ButtonImg_DatailsViewBar.ChekedClickDown;
                        }
                        if (typeof (_this.StyleGrid.ToolBar.ButtonImg_DatailsViewBar.Visible) != "undefined") {
                            _this.GridCFToolbar.ButtonImg_DatailsViewBar_Visible = _this.StyleGrid.ToolBar.ButtonImg_DatailsViewBar.Visible;
                            if (!_this.StyleGrid.ToolBar.ButtonImg_DatailsViewBar.Visible) {
                                _this.GridCFToolbar.ButtonImg_DatailsViewBar_ChekedClickDown = false;
                            }
                        }
                    }
                    if (typeof (_this.StyleGrid.ToolBar.ButtonImg_Grid) != "undefined") {
                        if (typeof (_this.StyleGrid.ToolBar.ButtonImg_Grid.ChekedClickDown) != "undefined") {
                            _this.GridCFToolbar.ButtonImg_Grid_ChekedClickDown = _this.StyleGrid.ToolBar.ButtonImg_Grid.ChekedClickDown;
                        }

                    }

                    if (typeof (_this.StyleGrid.ToolBar.ButtonImg_RowCheckToolsBar) != "undefined") {
                        if (typeof (_this.StyleGrid.ToolBar.ButtonImg_RowCheckToolsBar.ChekedClickDown) != "undefined") {
                            _this.GridCFToolbar.ButtonImg_RowCheckToolsBar_ChekedClickDown = _this.StyleGrid.ToolBar.ButtonImg_RowCheckToolsBar.ChekedClickDown;
                        }
                        if (typeof (_this.StyleGrid.ToolBar.ButtonImg_RowCheckToolsBar.Visible) != "undefined") {
                            _this.GridCFToolbar.ButtonImg_RowCheckToolsBar_Visible = _this.StyleGrid.ToolBar.ButtonImg_RowCheckToolsBar.Visible;
                            if (!_this.StyleGrid.ToolBar.ButtonImg_RowCheckToolsBar.Visible) {
                                _this.GridCFToolbar.ButtonImg_RowCheckToolsBar_ChekedClickDown = false;
                            }
                        }
                    }

                    if (typeof (_this.StyleGrid.ToolBar.ButtonImg_ColumnGroupBar) != "undefined") {
                        if (typeof (_this.StyleGrid.ToolBar.ButtonImg_ColumnGroupBar.ChekedClickDown) != "undefined") {
                            _this.GridCFToolbar.ButtonImg_ColumnGroupBar_ChekedClickDown = _this.StyleGrid.ToolBar.ButtonImg_ColumnGroupBar.ChekedClickDown;

                        }
                        if (typeof (_this.StyleGrid.ToolBar.ButtonImg_ColumnGroupBar.Visible) != "undefined") {
                            _this.GridCFToolbar.ButtonImg_ColumnGroupBar_Visible = _this.StyleGrid.ToolBar.ButtonImg_ColumnGroupBar.Visible;
                            if (!_this.StyleGrid.ToolBar.ButtonImg_ColumnGroupBar.Visible) {
                                _this.GridCFToolbar.ButtonImg_ColumnGroupBar_ChekedClickDown = false;
                            }
                        }

                    }

                    if (typeof (_this.StyleGrid.ToolBar.ButtonImg_NavigatorToolsBar) != "undefined") {

                        if (typeof (_this.StyleGrid.ToolBar.ButtonImg_NavigatorToolsBar.ChekedClickDown) != "undefined") {
                            _this.GridCFToolbar.ButtonImg_NavigatorToolsBar_ChekedClickDown = _this.StyleGrid.ToolBar.ButtonImg_NavigatorToolsBar.ChekedClickDown;
                        }
                        if (typeof (_this.StyleGrid.ToolBar.ButtonImg_NavigatorToolsBar.Visible) != "undefined") {
                            _this.GridCFToolbar.ButtonImg_NavigatorToolsBar_Visible = _this.StyleGrid.ToolBar.ButtonImg_NavigatorToolsBar.Visible;
                            if (!_this.StyleGrid.ToolBar.ButtonImg_NavigatorToolsBar.Visible) {
                                _this.GridCFToolbar.ButtonImg_NavigatorToolsBar_ChekedClickDown = false;
                            }
                        }
                    }

                    if (typeof (_this.StyleGrid.ToolBar.ButtonImg_StatusBar) != "undefined") {
                        if (typeof (_this.StyleGrid.ToolBar.ButtonImg_StatusBar.ChekedClickDown) != "undefined") {
                            _this.GridCFToolbar.ButtonImg_StatusBar_ChekedClickDown = _this.StyleGrid.ToolBar.ButtonImg_StatusBar.ChekedClickDown;
                        }
                        if (typeof (_this.StyleGrid.ToolBar.ButtonImg_StatusBar.Visible) != "undefined") {
                            _this.GridCFToolbar.ButtonImg_StatusBar_Visible = _this.StyleGrid.ToolBar.ButtonImg_StatusBar.Visible;
                            if (!_this.StyleGrid.ToolBar.ButtonImg_StatusBar.Visible) {
                                _this.GridCFToolbar.ButtonImg_StatusBar_ChekedClickDown = false;
                            }
                        }
                    }

                    if (typeof (_this.StyleGrid.ToolBar.ButtonImg_GridVisibleColumnBar) != "undefined") {

                        if (typeof (_this.StyleGrid.ToolBar.ButtonImg_GridVisibleColumnBar.Visible) != "undefined") {
                            _this.GridCFToolbar.ButtonImg_GridVisibleColumnBar_Visible = _this.StyleGrid.ToolBar.ButtonImg_GridVisibleColumnBar.Visible;
                            if (_this.GridCFToolbar.ButtonImg_DatailsViewBar_ChekedClickDown && _this.GridCFToolbar.ButtonImg_GridVisibleColumnBar_Visible) {
                                _this.GridCFToolbar.ButtonImg_GridDetailsVisibleColumnBar.Visible = true;
                            }

                        }
                    }

                    if (typeof (_this.StyleGrid.ToolBar.ButtonImg_MoveColumnsBar) != "undefined") {

                        if (typeof (_this.StyleGrid.ToolBar.ButtonImg_MoveColumnsBar.Visible) != "undefined") {
                            _this.GridCFToolbar.ButtonImg_MoveColumnsBar_Visible = _this.StyleGrid.ToolBar.ButtonImg_MoveColumnsBar.Visible;

                        }
                    }

                    if (typeof (_this.StyleGrid.ToolBar.ButtonImg_ExportPDFBar) != "undefined") {

                        if (typeof (_this.StyleGrid.ToolBar.ButtonImg_ExportPDFBar.Visible) != "undefined") {
                            _this.GridCFToolbar.ButtonImg_ExportPDFBar_Visible = _this.StyleGrid.ToolBar.ButtonImg_ExportPDFBar.Visible;

                        }
                    }

                    if (typeof (_this.StyleGrid.ToolBar.ButtonImg_ExportExcelBar) != "undefined") {

                        if (typeof (_this.StyleGrid.ToolBar.ButtonImg_ExportExcelBar.Visible) != "undefined") {
                            _this.GridCFToolbar.ButtonImg_ExportExcelBar_Visible = _this.StyleGrid.ToolBar.ButtonImg_ExportExcelBar.Visible;

                        }
                    }

                    if (typeof (_this.StyleGrid.ToolBar.ButtonImg_ExportWordBar) != "undefined") {

                        if (typeof (_this.StyleGrid.ToolBar.ButtonImg_ExportWordBar.Visible) != "undefined") {
                            _this.GridCFToolbar.ButtonImg_ExportWordBar_Visible = _this.StyleGrid.ToolBar.ButtonImg_ExportWordBar.Visible;

                        }
                    }

                }

                if (typeof (_this.StyleGrid.OptionButtonCkeck) != "undefined") {
                    if (typeof (_this.StyleGrid.OptionButtonCkeck.SetSelectedRows) != "undefined") {
                        _this.SetSelectedRows = _this.StyleGrid.OptionButtonCkeck.SetSelectedRows;
                    }

                }


                if (typeof (_this.StyleGrid.OrderColumns) != "undefined") {

                    _this.OrderColumns = _this.StyleGrid.OrderColumns;

                }
                if (typeof (_this.StyleGrid.ListAgrupation) != "undefined") {
                    _this.ListAgrupation = _this.StyleGrid.ListAgrupation;
                }

                if (_this.StyleGrid.Order != null && typeof (_this.StyleGrid.Order) != "undefined" && typeof (_this.StyleGrid.Order.ColumnName) && "undefined" && typeof (_this.StyleGrid.Order.Type) != "undefined") {
                    var tempOrder = "ninguno";
                    this.OrderTCfg.Type = _this.StyleGrid.Order.Type;
                    this.OrderTCfg.ColumnName = _this.StyleGrid.Order.ColumnName;

                    if (_this.StyleGrid.Order.Type == 1) {
                        tempOrder = "asc";
                    } else if (_this.StyleGrid.Order.Type == 2) {
                        tempOrder = "des";
                    }
                    if (_this.StyleGrid.Order.ColumnName != null) {
                        var tempColumnOrder = _this.VclGridCF.GetColumn(_this.StyleGrid.Order.ColumnName);
                        if (_this.VclGridCF.Order != tempOrder || _this.ColumnaSelecionada + "" != (tempColumnOrder.Index) + "") {
                            this.Order(tempColumnOrder, _this.StyleGrid.Order.Type);
                        }
                    } else {

                        if ("" + _this.ColumnaSelecionada != "" && "" + _this.ColumnaSelecionada != "null" && _this.VclGridCF.Order != "ninguno") {
                            _this.VclGridCF.Order = "asc";
                            _this.ColumnClick(_this.VclGridCF.Columns[_this.ColumnaSelecionada]);

                        }
                    }

                }

                var IsAllShowValue = true;
                if (typeof (_this.StyleGrid.IsAllShowValue) != "undefined") {
                    IsAllShowValue = _this.StyleGrid.IsAllShowValue;
                }


                if (typeof (_this.StyleGrid.Columns) != "undefined") {

                    for (var i = 1; i < this.VclGridCF.Columns.length - 1; i++) {

                        for (var j = 0; j < this.StyleGrid.Columns.length; j++) {
                            if (typeof (_this.StyleGrid.Columns[j].Name) != "undefined" && this.StyleGrid.Columns[j].Name == this.VclGridCF.Columns[i].Name) {

                                if (typeof (_this.StyleGrid.Columns[j].Caption) != "undefined") {
                                    if (this.VclGridCF.Columns[i].Caption != this.StyleGrid.Columns[j].Caption)
                                        this.VclGridCF.Columns[i].CaptionStyle = this.StyleGrid.Columns[j].Caption;
                                }
                                if (typeof (_this.StyleGrid.Columns[j].OptionProgressBar) != "undefined") {
                                    this.VclGridCF.Columns[i].OptionProgressBar = this.StyleGrid.Columns[j].OptionProgressBar;
                                }

                                if (typeof (_this.StyleGrid.Columns[j].CustomDrawCell) != "undefined") {

                                    if (typeof (_this.StyleGrid.Columns[j].CustomDrawCell.data) != "undefined") {

                                        this.VclGridCF.Columns[i].DataBuildCustomCell = this.StyleGrid.Columns[j].CustomDrawCell.data;
                                    }
                                    if (typeof (_this.StyleGrid.Columns[j].CustomDrawCell.function) != "undefined") {
                                        if (this.VclGridCF.Columns[i].CustomDrawCell != this.StyleGrid.Columns[j].CustomDrawCell.function)
                                            this.VclGridCF.Columns[i].CustomDrawCell = this.StyleGrid.Columns[j].CustomDrawCell.function;
                                    }
                                }


                                if (typeof (_this.StyleGrid.Columns[j].Show) != "undefined") {
                                    _this.VclGridCF.showHideColumn(_this.VclGridCF.Columns[i].Index, _this.StyleGrid.Columns[j].Show);
                                    _this.VclGridCF.Columns[i]._Visible_IsShowValue = true;
                                    _this.VclGridCF.Columns[i]._VisibleVertical_IsShowValue = true;
                                    _this.VclGridCF.Columns[i]._VisibleVertical = _this.StyleGrid.Columns[j].Show;
                                    if (!IsAllShowValue) {
                                        _this.VclGridCF.Columns[i].Visible_IsShowValue = _this.StyleGrid.Columns[j].Show;
                                        _this.VclGridCF.Columns[i]._VisibleVertical_IsShowValue = _this.StyleGrid.Columns[j].Show;
                                    }
                                }

                                /* Para activar el info visible Vertical, pero corregir el metodo de arriba
                                if (typeof (_this.StyleGrid.Columns[j].Show) != "undefined") {
                                    _this.VclGridCF.showHideColumn(_this.VclGridCF.Columns[i].Index, _this.StyleGrid.Columns[j].Show);
                                    _this.VclGridCF.Columns[i]._Visible_IsShowValue = true;
                                    if (!IsAllShowValue) {
                                        _this.VclGridCF.Columns[i].Visible_IsShowValue = _this.StyleGrid.Columns[j].Show;
                                    }
                                }
                                if (typeof (_this.StyleGrid.Columns[j].ShowVertical) != "undefined") {
                                    _this.VclGridCF.Columns[i]._VisibleVertical = _this.StyleGrid.Columns[j].ShowVertical;
                                    _this.VclGridCF.Columns[i]._VisibleVertical_IsShowValue = true;
                                    if (!IsAllShowValue) {
                                        _this.VclGridCF.Columns[i]._VisibleVertical_IsShowValue = _this.VclGridCF.Columns[i]._VisibleVertical;
                                    }
                                }
                                */
                                if (typeof (_this.StyleGrid.Columns[j].DataEventControl) != "undefined") {

                                    _this.VclGridCF.Columns[i].DataEventControl = _this.StyleGrid.Columns[j].DataEventControl;
                                }
                                if (typeof (_this.StyleGrid.Columns[j].OnEventControl) != "undefined") {
                                    _this.VclGridCF.Columns[i].ColumEdit(_this.StyleGrid.Columns[j].Style, _this.StyleGrid.Columns[j].OnEventControl);
                                }

                                if (typeof (_this.StyleGrid.Columns[j].FileType) != "undefined") {

                                    _this.VclGridCF.Columns[i]._FileType = _this.StyleGrid.Columns[j].FileType;
                                }
                                if (typeof (_this.StyleGrid.Columns[j].FileSRVEvent) != "undefined") {

                                    _this.VclGridCF.Columns[i]._FileSRVEvent = _this.StyleGrid.Columns[j].FileSRVEvent;
                                }

                                if (typeof (_this.StyleGrid.Columns[j].OptionProgressBar) != "undefined") {

                                    _this.VclGridCF.Columns[i].OptionProgressBar = _this.StyleGrid.Columns[j].OptionProgressBar;

                                }


                                if (typeof (_this.StyleGrid.Columns[j].Style) != "undefined"
                                    && (_this.StyleGrid.Columns[j].Style == Componet.Properties.TExtrafields_ColumnStyle.FileSRV ||
                                        _this.StyleGrid.Columns[j].Style == Componet.Properties.TExtrafields_ColumnStyle.Text ||
                                        _this.StyleGrid.Columns[j].Style == Componet.Properties.TExtrafields_ColumnStyle.ProgressBar ||
                                        _this.StyleGrid.Columns[j].Style == Componet.Properties.TExtrafields_ColumnStyle.Time ||
                                        _this.StyleGrid.Columns[j].Style == Componet.Properties.TExtrafields_ColumnStyle.Date


                                    )
                                ) {
                                    _this.VclGridCF.Columns[i].ColumEdit(_this.StyleGrid.Columns[j].Style, _this.StyleGrid.Columns[j].OnEventControl);
                                }

                                if (typeof (_this.StyleGrid.Columns[j].ModalEvent) != "undefined") {

                                    this.VclGridCF.Columns[i].ModalEvent = this.StyleGrid.Columns[j].ModalEvent;
                                }


                                if (typeof (_this.StyleGrid.Columns[j].ActiveEditor) != "undefined")
                                    this.VclGridCF.Columns[i].ActiveEditor = this.StyleGrid.Columns[j].ActiveEditor;

                                if (typeof (_this.StyleGrid.Columns[j].InsertActiveEditor) != "undefined") {
                                    if (this.VclGridCF.Columns[i].InsertActiveEditor != this.StyleGrid.Columns[j].InsertActiveEditor) {
                                        this.VclGridCF.Columns[i].InsertActiveEditor = this.StyleGrid.Columns[j].InsertActiveEditor;
                                    }
                                }
                                if (typeof (_this.StyleGrid.Columns[j].UpdateActiveEditor) != "undefined") {
                                    if (this.VclGridCF.Columns[i].UpdateActiveEditor != this.StyleGrid.Columns[j].UpdateActiveEditor) {
                                        this.VclGridCF.Columns[i].UpdateActiveEditor = this.StyleGrid.Columns[j].UpdateActiveEditor;
                                    }

                                }

                            }
                        }
                    }
                }


                if (typeof (_this.StyleGrid.AutoTranslete) != "undefined") {

                    _this.AutoTranslete = _this.StyleGrid.AutoTranslete;
                }

                _this.Information.BuildGridInfo(_this.Information.DivitionColumn, _this.VclGridCF);
            }


        }
    });
    /*Propiedad para aplicar el orden de las columnas en el grid*/
    Object.defineProperty(this, 'OrderColumns', {
        get: function () {
            return this._OrderColumns;

        },
        set: function (inValue) {

            this._OrderColumns = inValue;

            if (!_this.IsLoadDataSourceComplete) {
                if (this._OrderColumns != null) {
                    _this.VclGridCF._AutoGenerateColumns = false;
                    var listOrderNumber = _this.VclGridCF.OrderColumns(this._OrderColumns);
                    var banderaAplicarOrden = false;
                    for (var i = 0; i < listOrderNumber.length; i++) {
                        if (_this.VclGridCF.Columns[i + 1].Index != listOrderNumber[i]) {
                            banderaAplicarOrden = true;
                            break;
                        }
                    }
                    if (banderaAplicarOrden) {
                        if (_this.VclGridCF.Columns.length > 0 && this._OrderColumns != null) {
                            _this.nameColumns = _this.GetNames();
                            _this.VclTreeGridControl.nameColumns = _this.nameColumns;
                        }
                    }
                }
            } else {
                if (_this.VclGridCF.Rows.length > 0 && this._OrderColumns != null) {
                    _this.VclGridCF._AutoGenerateColumns = false;

                    var listOrderNumber = _this.VclGridCF.OrderColumns(this._OrderColumns);
                    var banderaAplicarOrden = false;

                    for (var i = 0; i < listOrderNumber.length; i++) {
                        if (_this.VclGridCF.Columns[i + 1].Index != listOrderNumber[i]) {
                            banderaAplicarOrden = true;
                            break;
                        }
                    }

                    if (banderaAplicarOrden) {

                        if (_this.VclGridCF.Columns.length > 0 && this._OrderColumns != null) {

                            _this.nameColumns = _this.GetNames();
                            _this.VclTreeGridControl.nameColumns = _this.nameColumns;
                        }
                        for (var i = 0; i < _this.ItemTreePanel.length; i++) {
                            var temp = new Array();
                            for (var j = 0; j < listOrderNumber.length; j++) {
                                temp.push(_this.ItemTreePanel[i][listOrderNumber[j] - 1]);
                            }

                            for (var j = 0; j < _this.VclGridCF.Columns.length - 2; j++) {
                                _this.ItemTreePanel[i][j] = temp[j];
                            }

                        }

                        _this.DataSet.Cancel();
                    }



                }
            }


        }
    });
    /*Propiedad para establecer las filas chekeadas o sin chekear*/
    Object.defineProperty(this, 'SetSelectedRows', {
        get: function () {
            return this._SetSelectedRows;

        },
        set: function (inValue) {
            this._SetSelectedRows = inValue;
            if (_this.VclGridCF.Rows.length > 0) {
                _this.VclGridCF.SetSelectedRows(inValue);
            }


        }
    });
    /*Propiedad para dividir las columnas del detalle*/
    Object.defineProperty(this, 'DivitionColumnInformation', {
        get: function () {

            return this._DivitionColumnInformation;

        },
        set: function (inValue) {
            this._DivitionColumnInformation = inValue;
            if (this.Information != null) {
                if (this.Information.DivitionColumn != inValue) {
                    this.Information.DivitionColumn = inValue
                    if (this.Information.VclGridCFInfo.Rows.length > 0)
                        this.Information.BuildGridInfo(inValue, _this.VclGridCF);
                }
            }

        }
    });
    /*Propiedad para agrupar las columnas en el treegrid*/
    Object.defineProperty(this, 'ListAgrupation', {
        get: function () {
            return this._ListAgrupation;
        },
        set: function (inValue) {
            this._ListAgrupation = inValue;
            if (this.VclTreeGridControl != null) {
                if (_this.GridCFToolbar.ButtonImg_ColumnGroupBar_ChekedClickDown) {
                    this.VclTreeGridControl.ListAgrupation = inValue;
                    this._ListAgrupation = this.VclTreeGridControl.ListAgrupation;
                    this.VclTreeGridControl.Show = _this.GridCFToolbar.ButtonImg_ColumnGroupBar_ChekedClickDown;

                } else {
                    this.VclTreeGridControl.ListAgrupation = inValue;
                }
            }
        }
    });
    /*Función para mover columnas en el cfview (Grid y TreeGrid)*/
    this.MoveColumns = function (posicionA, posicionB) {
        //Funcion para mover columnas
        _this.VclGridCF._AutoGenerateColumns = false;
        _this.VclGridCF.MoveColumns(posicionA, posicionB);
        if (_this.VclTreeGridControl.inTreeGrid) {
            _this.VclTreeGridControl.GridTreeGridControl.MoveColumns(posicionA, posicionB);
        }
        for (var i = 0; i < _this.ItemTreePanel.length; i++) {
            var temp = _this.ItemTreePanel[i][posicionA - 1];
            _this.ItemTreePanel[i][posicionA - 1] = _this.ItemTreePanel[i][posicionB - 1];
            _this.ItemTreePanel[i][posicionB - 1] = temp;
        }
        var tempName = _this.nameColumns[posicionA - 1];
        _this.nameColumns[posicionA - 1] = _this.nameColumns[posicionB - 1];
        _this.nameColumns[posicionB - 1] = tempName;
    }
    /*Función para el doble click en la fila*/
    this.RowDblClick = function (NewRow) {
        if (this.GridCFToolbar.ButtonImg_EditBar_Visible) {
            this.GridCFToolbar.TabPagData.Active = true;
            _this.OnActiveChangeControls = false;
            if (_this.VclTreeGridControl != null && _this.VclTreeGridControl.inTreeGrid) {
                _this.VclTreeGridControl.buttonEventUpdateTreeGrid();
            } else {
                _this.EventUpdate(_this.VclGridCF.FocusedRowHandle);
            }
        }
    }
    /*Funcion click en fila*/
    this.RowClick = function (NewRow) {
        if ((_this.DataSet.Status == SysCfg.MemTable.Properties.TStatus.Apped
            || _this.DataSet.Status == SysCfg.MemTable.Properties.TStatus.Update) && _this.OnActiveChangeControls) {
            if (_this.VclTreeGridControl != null && _this.VclTreeGridControl.inTreeGrid) {
                _this.VclTreeGridControl.buttonEventPostTreeGrid();
            } else {
                _this.EventPost(_this.VclGridCF.FocusedRowHandle);
            }

        } else {

            _this.CancelMostrarInformacion(_this.VclGridCF.FocusedRowHandle);
            _this.IndexRowGridPorScroll = NewRow.Index;
            if (NewRow.IndexDataSet != _this.DataSet.Index) {
                _this.DataSet.SetIndex(NewRow.IndexDataSet);
                _this.VclGridCF.IndexRow = NewRow.Index;
            } else {

                _this.VclGridCF.IndexRow = NewRow.Index;
                _this.DataSet.RecordSet = _this.DataSet.Records[_this.VclGridCF.FocusedRowHandle.IndexDataSet];
                _this.HabilitarCamposFormGrid();//Actualizamos el detalle del Grid.
            }
            if (SysCfg.App.Properties.Device != SysCfg.App.TDevice.Desktop) {
                if (this.GridCFToolbar.ButtonImg_EditBar_Visible) {
                    this.GridCFToolbar.TabPagData.Active = true;
                    _this.OnActiveChangeControls = false;
                    if (_this.VclTreeGridControl != null && _this.VclTreeGridControl.inTreeGrid) {
                        _this.VclTreeGridControl.buttonEventUpdateTreeGrid();
                    } else {
                        _this.EventUpdate(_this.VclGridCF.FocusedRowHandle);
                    }
                }
            }

        }

    }
    /*Funcion...*/
    this.Order = function (ColMem1, inOrderType) {
        var inOrder = "ninguno";
        if (inOrderType == 1) {
            var inOrder = "asc";
        } else if (inOrderType == 2) {
            var inOrder = "des";
        }
        _this.DataSet.Cancel();
        var posicionColumna = ColMem1.Index;
        if (posicionColumna != null) {
            _this.VclGridCF.Order = inOrder;
            if ("" + _this.ColumnaSelecionada != "" && "" + _this.ColumnaSelecionada != "null" && _this.ColumnaSelecionada + "" != "" + posicionColumna) {
                _this.VclGridCF.Columns[_this.ColumnaSelecionada].IsColumnSelectOrder = null;
                _this.VclGridCF.Columns[_this.ColumnaSelecionada].CaptionOrder = _this.DataSet.FieldDefs["" + (_this.ObtenerIndexDataSource(_this.ColumnaSelecionada))].FieldName;
            }
        }

        if (posicionColumna != null) {
            _this.ColumnaSelecionada = posicionColumna;
            var columna = ColMem1.Index;
            if (_this.IndiceLogico != null) {
                var order = _this.VclGridCF.Order;
                switch (ColMem1.DataType) {
                    case SysCfg.DB.Properties.TDataType.String:
                        comparar = function (a, b) {
                            if (a[columna - 1][0] > b[columna - 1][0]) {
                                return 1;
                            }
                            if (a[columna - 1][0] < b[columna - 1][0]) {
                                return -1;
                            }
                            return 0;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Text:
                        comparar = function (a, b) {
                            if (a[columna - 1][0] > b[columna - 1][0]) {
                                return 1;
                            }
                            if (a[columna - 1][0] < b[columna - 1][0]) {
                                return -1;
                            }
                            return 0;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Int32:

                        comparar = function (a, b) {
                            return a[columna - 1][0] - b[columna - 1][0];
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Double:
                        comparar = function (a, b) {
                            return a[columna - 1][0] - b[columna - 1][0];
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Boolean:
                        comparar = function (a, b) {
                            var a1 = a[columna - 1][0] + "";
                            var b1 = b[columna - 1][0] + "";
                            if (a1 > b1) {
                                return 1;
                            }
                            if (a1 < b1) {
                                return -1;
                            }
                            return 0;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.DateTime:

                        comparar = function (a, b) {
                            return new Date(a[columna - 1][0]) - new Date(b[columna - 1][0]);
                        }
                        break
                    default:
                        break;
                }
                if (order == "asc") {
                    _this.ItemTreePanel.sort(function (a, b) {
                        return comparar(a, b);
                    });
                }
                else {
                    _this.ItemTreePanel.sort(function (a, b) {
                        return comparar(b, a);
                    });
                }
                if (_this.IsPagination) {
                    _this.loadDataSetRecordsPaginationFilter(null, null, null, null, true);
                }
                else {
                    _this.loadDataSetRecordsScrollFilter(null, null, null, null, true);
                }
            }
            _this.VclGridCF.Columns[columna].IsColumnSelectOrder = order;
            _this.VclGridCF.Columns[columna].CaptionOrder = _this.VclGridCF.Columns[columna].Caption;
        }



    }
    /*Función click en columna*/
    this.ColumnClick = function (ColMem1) {
        if (SysCfg.App.Properties.Device != SysCfg.App.TDevice.Desktop) {
            this.CreateModalOptionColumn(ColMem1);
        }
        else {
            _this.EventOrderColumn(ColMem1);
        }
    }
    /*Función para crear modal al relizar click en la columna, funciona para dispositivos móviles.*/
    this.CreateModalOptionColumn = function (ColMem1) {
        var divConten = document.createElement("div");
        var div1 = document.createElement("div");
        var div2 = document.createElement("div");
        var div3 = document.createElement("div");
        var div4 = document.createElement("div");
        divConten.appendChild(div1);
        divConten.appendChild(div2);
        divConten.appendChild(div3);
        divConten.appendChild(div4);
        div1.style.width = "300px";
        var sliderColumn = document.createElement("div");
        $(sliderColumn).slider({

            change: function () {

                valorSlide = $(sliderColumn).slider("value");
                ColMem1.divResize.style.width = (widthInitialize * valorSlide / 20) + "px";

                var e = window.event || arguments.callee.caller.arguments[0];
                e.cancelBubble = true;
                e.returnValue = false;
                if (e.stopPropagation) e.stopPropagation();
                if (e.preventDefault) e.preventDefault();
            }
        });
        var valueLastSlider = 20;
        $(sliderColumn).slider("value", valueLastSlider);
        var widthInitialize = $(ColMem1.This).width();

        div2.appendChild(sliderColumn);
        div3.style.marginTop = "10px";
        var buttonOrder = document.createElement("button");
        var spanbuttonOrder = document.createElement("span");
        var iconbuttonOrder = document.createElement("i");
        iconbuttonOrder.classList.add("icon");
        iconbuttonOrder.classList.add("ion-arrow-up-c");
        var iconbuttonOrder2 = document.createElement("i");
        iconbuttonOrder2.classList.add("icon");
        iconbuttonOrder2.classList.add("ion-arrow-down-c");
        spanbuttonOrder.innerHTML = " " + "Order";
        buttonOrder.onclick = function (e) {
            modalFV.hide();
            _this.EventOrderColumn(ColMem1);
            //modalFV.hide();

        }
        var buttonSearch = document.createElement("button");
        var spanbuttonSearch = document.createElement("span");
        var iconbuttonSearch = document.createElement("i");
        iconbuttonSearch.classList.add("icon");
        iconbuttonSearch.classList.add("ion-search");
        spanbuttonSearch.innerHTML = " " + "Search";
        buttonSearch.onclick = function (e) {
            modalFV.hide();
            ColMem1.CreateControlFilter();
            var e = window.event || arguments.callee.caller.arguments[0];
            e.cancelBubble = true;
            e.returnValue = false;
            if (e.stopPropagation) e.stopPropagation();
            if (e.preventDefault) e.preventDefault();
        }

        var buttonGroup = document.createElement("button");
        var spanbuttonGroup = document.createElement("span");
        var iconbuttonGroup = document.createElement("i");
        iconbuttonGroup.classList.add("icon");
        iconbuttonGroup.classList.add("ion-navicon-round");
        spanbuttonGroup.innerHTML = " " + "Group";
        buttonGroup.onclick = function () {
            modalFV.hide();
            if (ColMem1.onEventFilterColumn != null) {
                ColMem1.onEventFilterColumn(ColMem1);
            }
            var e = window.event || arguments.callee.caller.arguments[0];
            e.cancelBubble = true;
            e.returnValue = false;
            if (e.stopPropagation) e.stopPropagation();
            if (e.preventDefault) e.preventDefault();
        }
        buttonSearch.appendChild(iconbuttonSearch);
        buttonSearch.appendChild(spanbuttonSearch);
        buttonGroup.appendChild(iconbuttonGroup);
        buttonGroup.appendChild(spanbuttonGroup);
        buttonOrder.appendChild(iconbuttonOrder);
        buttonOrder.appendChild(iconbuttonOrder2);
        buttonOrder.appendChild(spanbuttonOrder);
        buttonSearch.classList.add("btn");
        buttonSearch.classList.add("btn-default");
        buttonGroup.classList.add("btn");
        buttonGroup.classList.add("btn-default");
        buttonOrder.classList.add("btn");
        buttonOrder.classList.add("btn-default");
        buttonOrder.style.width = "33%";
        buttonSearch.style.width = "34%";
        buttonGroup.style.width = "33%";
        div3.appendChild(buttonOrder);
        div3.appendChild(buttonSearch);
        div3.appendChild(buttonGroup);


        var modalFV = new Componet.GridCF.TGridCFView.TModalFV(divConten, ColMem1.This);

        var e = window.event || arguments.callee.caller.arguments[0];
        e.cancelBubble = true;
        e.returnValue = false;
        if (e.stopPropagation) e.stopPropagation();
        if (e.preventDefault) e.preventDefault();


    }
    /*Función para ordenar las columnas*/
    this.EventOrderColumn = function (ColMem1) {
        if (_this.DataSet.Status != SysCfg.MemTable.Properties.TStatus.Apped
            && _this.DataSet.Status != SysCfg.MemTable.Properties.TStatus.Update) {
            if (_this.VclTreeGridControl != null && _this.VclTreeGridControl.inTreeGrid) {
                _this.DataSet.Cancel();
                var UnDataSet = _this.DataSet;
                var posicionColumna = ColMem1.Index;
                if ("" + _this.VclTreeGridControl.ColumnaSelecionada != "" && "" + _this.VclTreeGridControl.ColumnaSelecionada != "null" && _this.VclTreeGridControl.ColumnaSelecionada + "" != "" + posicionColumna) {
                    _this.VclTreeGridControl.GridTreeGridControl.Order = "ninguno";
                    _this.VclTreeGridControl.GridTreeGridControl.Columns[_this.VclTreeGridControl.ColumnaSelecionada].IsColumnSelectOrder = null;
                    _this.VclTreeGridControl.GridTreeGridControl.Columns[_this.VclTreeGridControl.ColumnaSelecionada].CaptionOrder = _this.VclTreeGridControl.GridTreeGridControl.Columns[_this.VclTreeGridControl.ColumnaSelecionada].Caption;
                }
                if (posicionColumna != null) {
                    if (_this.VclTreeGridControl.GridTreeGridControl.Order == "des" || _this.VclTreeGridControl.GridTreeGridControl.Order == "ninguno" || _this.VclTreeGridControl.GridTreeGridControl.Order == "") {
                        _this.VclTreeGridControl.GridTreeGridControl.Order = "asc";
                    }
                    else {
                        _this.VclTreeGridControl.GridTreeGridControl.Order = "des";
                    }
                    _this.VclTreeGridControl.ColumnaSelecionada = posicionColumna;

                    var columna = ColMem1.Index;
                    var order = _this.VclTreeGridControl.GridTreeGridControl.Order;
                    switch (ColMem1.DataType) {
                        case SysCfg.DB.Properties.TDataType.String:
                            comparar = function (a, b) {
                                if (a[columna - 1][0] > b[columna - 1][0]) {
                                    return 1;
                                }
                                if (a[columna - 1][0] < b[columna - 1][0]) {
                                    return -1;
                                }
                                return 0;
                            }
                            break;
                        case SysCfg.DB.Properties.TDataType.Text:
                            comparar = function (a, b) {
                                if (a[columna - 1][0] > b[columna - 1][0]) {
                                    return 1;
                                }
                                if (a[columna - 1][0] < b[columna - 1][0]) {
                                    return -1;
                                }
                                return 0;
                            }
                            break;
                        case SysCfg.DB.Properties.TDataType.Int32:

                            comparar = function (a, b) {
                                return a[columna - 1][0] - b[columna - 1][0];
                            }
                            break;
                        case SysCfg.DB.Properties.TDataType.Double:
                            comparar = function (a, b) {
                                return a[columna - 1][0] - b[columna - 1][0];
                            }
                            break;
                        case SysCfg.DB.Properties.TDataType.Boolean:
                            comparar = function (a, b) {
                                var a1 = a[columna - 1][0] + "";
                                var b1 = b[columna - 1][0] + "";
                                if (a1 > b1) {
                                    return 1;
                                }
                                if (a1 < b1) {
                                    return -1;
                                }
                                return 0;
                            }
                            break;
                        case SysCfg.DB.Properties.TDataType.DateTime:
                            if (ColMem1.ColumnStyle == Componet.Properties.TExtrafields_ColumnStyle.Time) {
                                comparar = function (a, b) {

                                    var fechaA = new Date(a[columna - 1][0]);
                                    var fechaB = new Date(b[columna - 1][0]);
                                    fechaA.setFullYear(2018);
                                    fechaA.setMonth(1);
                                    fechaA.setDate(1);
                                    fechaB.setFullYear(2018);
                                    fechaB.setMonth(1);
                                    fechaB.setDate(1);
                                    return fechaA - fechaB;
                                }

                            } else {
                                comparar = function (a, b) {
                                    return new Date(a[columna - 1][0]) - new Date(b[columna - 1][0]);
                                }
                            }


                            break
                        default:
                            break;
                    }
                    if (order == "asc") {
                        _this.ItemTreePanel.sort(function (a, b) {
                            return comparar(a, b);
                        });
                    }
                    else {
                        _this.ItemTreePanel.sort(function (a, b) {
                            return comparar(b, a);
                        });
                    }
                    _this.SetRecordsPaginationFilterItemTreePanel(null, null, null, null, true);
                    _this.VclTreeGridControl.ItemTreePanel = _this.ItemTreePanel;
                    _this.VclTreeGridControl.agruparPorCampo();

                    _this.VclTreeGridControl.GridTreeGridControl.Columns[columna].IsColumnSelectOrder = order;
                    _this.VclTreeGridControl.GridTreeGridControl.Columns[columna].CaptionOrder = _this.VclTreeGridControl.GridTreeGridControl.Columns[columna].Caption;
                }
            }
            else {

                _this.DataSet.Cancel();
                var UnDataSet = _this.DataSet;
                var posicionColumna = ColMem1.Index;

                if ("" + _this.ColumnaSelecionada != "" && "" + _this.ColumnaSelecionada != "null" && _this.ColumnaSelecionada + "" != "" + posicionColumna) {
                    _this.VclGridCF.Order = "ninguno";
                    _this.VclGridCF.Columns[_this.ColumnaSelecionada].IsColumnSelectOrder = null;
                    _this.VclGridCF.Columns[_this.ColumnaSelecionada].CaptionOrder = _this.VclGridCF.Columns[_this.ColumnaSelecionada].Caption;
                }
                if (posicionColumna != null) {
                    if (_this.VclGridCF.Order == "des" || _this.VclGridCF.Order == "ninguno" || _this.VclGridCF.Order == "") {
                        _this.VclGridCF.Order = "asc";
                    }
                    else {
                        _this.VclGridCF.Order = "des";
                    }
                    _this.ColumnaSelecionada = posicionColumna;

                    var columna = ColMem1.Index;
                    if (_this.IndiceLogico != null) {
                        var order = _this.VclGridCF.Order;
                        switch (ColMem1.DataType) {
                            case SysCfg.DB.Properties.TDataType.String:
                                comparar = function (a, b) {
                                    if (a[columna - 1][0] > b[columna - 1][0]) {
                                        return 1;
                                    }
                                    if (a[columna - 1][0] < b[columna - 1][0]) {
                                        return -1;
                                    }
                                    return 0;
                                }
                                break;
                            case SysCfg.DB.Properties.TDataType.Text:
                                comparar = function (a, b) {
                                    if (a[columna - 1][0] > b[columna - 1][0]) {
                                        return 1;
                                    }
                                    if (a[columna - 1][0] < b[columna - 1][0]) {
                                        return -1;
                                    }
                                    return 0;
                                }
                                break;
                            case SysCfg.DB.Properties.TDataType.Int32:

                                comparar = function (a, b) {
                                    return a[columna - 1][0] - b[columna - 1][0];
                                }
                                break;
                            case SysCfg.DB.Properties.TDataType.Double:
                                comparar = function (a, b) {
                                    return a[columna - 1][0] - b[columna - 1][0];
                                }
                                break;
                            case SysCfg.DB.Properties.TDataType.Boolean:
                                comparar = function (a, b) {
                                    var a1 = a[columna - 1][0] + "";
                                    var b1 = b[columna - 1][0] + "";
                                    if (a1 > b1) {
                                        return 1;
                                    }
                                    if (a1 < b1) {
                                        return -1;
                                    }
                                    return 0;
                                }
                                break;
                            case SysCfg.DB.Properties.TDataType.DateTime:

                                if (ColMem1.ColumnStyle == Componet.Properties.TExtrafields_ColumnStyle.Time) {
                                    comparar = function (a, b) {

                                        var fechaA = new Date(a[columna - 1][0]);
                                        var fechaB = new Date(b[columna - 1][0]);
                                        fechaA.setFullYear(2018);
                                        fechaA.setMonth(1);
                                        fechaA.setDate(1);
                                        fechaB.setFullYear(2018);
                                        fechaB.setMonth(1);
                                        fechaB.setDate(1);
                                        return fechaA - fechaB;
                                    }

                                } else {
                                    comparar = function (a, b) {
                                        return new Date(a[columna - 1][0]) - new Date(b[columna - 1][0]);
                                    }
                                }
                                break
                            default:
                                break;
                        }
                        if (order == "asc") {
                            _this.ItemTreePanel.sort(function (a, b) {
                                return comparar(a, b);
                            });
                        }
                        else {
                            _this.ItemTreePanel.sort(function (a, b) {
                                return comparar(b, a);
                            });
                        }
                        if (_this.IsPagination) {
                            _this.loadDataSetRecordsPaginationFilter(null, null, null, null, true);
                        }
                        else {
                            _this.loadDataSetRecordsScrollFilter(null, null, null, null, true);
                        }
                    }
                    _this.VclGridCF.Columns[columna].IsColumnSelectOrder = order;
                    _this.VclGridCF.Columns[columna].CaptionOrder = _this.VclGridCF.Columns[columna].Caption;
                }



            }
        }

    }
    /*Función para crear los botones insert, update, delete, post y cancel en la fila."*/
    this.DibujarCellButton = function (opcion, NewRow, CellButton) {
        //var divCellButtonControl = document.createElement("div");
        //CellButton.This.appendChild(divCellButtonControl);
        //CellButton.Control = divCellButtonControl;
        var divCellButtonControl = CellButton.This;
        CellButton.Control = true;//Asignamos un valor cualquiera        
        CellButton.btnInsert = new TVclButton(divCellButtonControl, "");
        CellButton.btnInsert.Text = "";
        CellButton.btnInsert.VCLType = TVCLType.BS;
        CellButton.btnInsert.ObjectButton = NewRow;
        CellButton.btnInsert.onClickObjectButton = function (NewRow) {
            _this.EventInsert(NewRow);
            var e = window.event || arguments.callee.caller.arguments[0];
            e.cancelBubble = true;
            e.returnValue = false;
            if (e.stopPropagation) e.stopPropagation();
            if (e.preventDefault) e.preventDefault();
        }
        CellButton.btnDelete = new TVclButton(divCellButtonControl, "");
        CellButton.btnDelete.Text = "";
        CellButton.btnDelete.VCLType = TVCLType.BS;
        CellButton.btnDelete.ObjectButton = NewRow;
        CellButton.btnDelete.onClickObjectButton = function (NewRow) {
            debugger;
            _this.EventDelete(NewRow);
            var e = window.event || arguments.callee.caller.arguments[0];
            e.cancelBubble = true;
            e.returnValue = false;
            if (e.stopPropagation) e.stopPropagation();
            if (e.preventDefault) e.preventDefault();
        }
        CellButton.btnUpdate = new TVclButton(divCellButtonControl, "");
        CellButton.btnUpdate.Text = "";
        CellButton.btnUpdate.VCLType = TVCLType.BS;
        CellButton.btnUpdate.ObjectButton = NewRow;
        CellButton.btnUpdate.onClickObjectButton = function (NewRow) {
            NewRow.onclick = function () { };
            _this.EventUpdate(NewRow);
            var e = window.event || arguments.callee.caller.arguments[0];
            e.cancelBubble = true;
            e.returnValue = false;
            if (e.stopPropagation) e.stopPropagation();
            if (e.preventDefault) e.preventDefault();
        }
        CellButton.btnPost = new TVclButton(divCellButtonControl, "");
        CellButton.btnPost.Text = "";
        CellButton.btnPost.VCLType = TVCLType.BS;
        CellButton.btnPost.ObjectButton = NewRow;
        CellButton.btnPost.onClickObjectButton = function (NewRow) {
            _this.EventPost(NewRow);
        }
        CellButton.btnCancel = new TVclButton(divCellButtonControl, "");
        CellButton.btnCancel.Text = "";
        CellButton.btnCancel.VCLType = TVCLType.BS;
        CellButton.btnCancel.ObjectButton = NewRow;
        CellButton.btnCancel.onClickObjectButton = function (NewRow) {
            _this.EventCancel(NewRow)
        }
        if (opcion) {
            CellButton.btnInsert.Enabled = true;
            CellButton.btnUpdate.Enabled = true;
            CellButton.btnDelete.Enabled = true;
            CellButton.btnPost.Enabled = false;
            CellButton.btnCancel.Enabled = false;
        } else {
            CellButton.btnInsert.Enabled = false;
            CellButton.btnUpdate.Enabled = false;
            CellButton.btnDelete.Enabled = false;
            CellButton.btnPost.Enabled = true;
            CellButton.btnCancel.Enabled = true;
        }

        var iApped = document.createElement("i");
        var iEdit = document.createElement("i");
        var iDelete = document.createElement("i");
        var iPost = document.createElement("i");
        var iCancel = document.createElement("i");

        iApped.classList.add("icon");
        iApped.classList.add("ion-plus-circled");
        iApped.classList.add("text-success");

        iEdit.classList.add("icon");
        iEdit.classList.add("ion-edit");
        iEdit.style.color = "#FFC400";

        iDelete.classList.add("icon");
        iDelete.classList.add("ion-close-circled");
        iDelete.classList.add("text-danger");

        iPost.classList.add("icon");
        iPost.classList.add("ion-checkmark-circled");
        iPost.classList.add("text-success");

        iCancel.classList.add("demo-icon");
        iCancel.classList.add("ion-minus-circled");
        iCancel.classList.add("text-danger");

        CellButton.btnInsert.This.appendChild(iApped);
        CellButton.btnUpdate.This.appendChild(iEdit);
        CellButton.btnDelete.This.appendChild(iDelete);
        CellButton.btnPost.This.appendChild(iPost);
        CellButton.btnCancel.This.appendChild(iCancel);
        NewRow.AddCell(CellButton);
    }
    /*Función que establece valores cuando el registro que se intenta insertar no se encuentra en el filtro general.*/
    this.EventCancelCustomFilter = function (Row) {
        if (_this.ItemTreePanel.length == 0) {
            var indiceFila = 0;
            _this.VclGridCF.RemoveRow(indiceFila);
            _this.Information.VclGridCFInfo.ClearAll();
            _this.GridCFToolbar.ButtonImg_PriorFirstBar.Enabled = false;
            _this.GridCFToolbar.ButtonImg_PriorBar.Enabled = false;
            _this.GridCFToolbar.ButtonImg_NextBar.Enabled = false;
            _this.GridCFToolbar.ButtonImg_NextLastBar.Enabled = false;
            _this.GridCFToolbar.ButtonImg_PriorFirstBarB.Enabled = false;
            _this.GridCFToolbar.ButtonImg_PriorBarB.Enabled = false;
            _this.GridCFToolbar.ButtonImg_NextBarB.Enabled = false;
            _this.GridCFToolbar.ButtonImg_NextLastBarB.Enabled = false;
            _this.GridCFToolbar.ButtonImg_DeleteBar.Enabled = false;
            _this.GridCFToolbar.ButtonImg_AddBar.Enabled = true;
            _this.GridCFToolbar.ButtonImg_EditBar.Enabled = false;
            _this.GridCFToolbar.ButtonImg_PostBar.Enabled = false;
            _this.GridCFToolbar.ButtonImg_CancelBar.Enabled = false;
            return true;
        }
        if (!_this.VclTreeGridControl.inTreeGrid) {
            _this.VclGridCF.RemoveRow(Row.Index);
        }
        if (_this.DataSet != null) {
            if (_this.IndiceLogico != null) {
                _this.IndiceLogico = Row.Index;
            }
            _this.DataSet.Status = SysCfg.MemTable.Properties.TStatus.None;
            _this.VclGridCF.IndexRow = Row.Index;
            _this.DataSet.RecordSet = _this.DataSet.Records[_this.VclGridCF.FocusedRowHandle.IndexDataSet];
            _this.HabilitarCamposFormGrid();//Actualizamos el detalle del Grid.
        }
    }
    /*Función para cancelar"*/
    this.EventCancel = function (Row) {
        if (_this.IndiceLogico == null) {
            if (_this.DataSet.Records.length == 0) {
                var indiceFila = 0;
                _this.VclGridCF.RemoveRow(indiceFila);
                _this.DataSet.Status = SysCfg.MemTable.Properties.TStatus.None;
                _this.DataSet.Cancel();
                _this.Information.VclGridCFInfo.ClearAll();
                return true;
            }
        } else {

            if (_this.ItemTreePanel.length == 0) {
                var indiceFila = 0;
                _this.VclGridCF.RemoveRow(indiceFila);
                _this.DataSet.Status = SysCfg.MemTable.Properties.TStatus.None;
                _this.DataSet.Cancel();
                _this.Information.VclGridCFInfo.ClearAll();
                _this.GridCFToolbar.ButtonImg_PriorFirstBar.Enabled = false;
                _this.GridCFToolbar.ButtonImg_PriorBar.Enabled = false;
                _this.GridCFToolbar.ButtonImg_NextBar.Enabled = false;
                _this.GridCFToolbar.ButtonImg_NextLastBar.Enabled = false;
                _this.GridCFToolbar.ButtonImg_PriorFirstBarB.Enabled = false;
                _this.GridCFToolbar.ButtonImg_PriorBarB.Enabled = false;
                _this.GridCFToolbar.ButtonImg_NextBarB.Enabled = false;
                _this.GridCFToolbar.ButtonImg_NextLastBarB.Enabled = false;
                _this.GridCFToolbar.ButtonImg_DeleteBar.Enabled = false;
                _this.GridCFToolbar.ButtonImg_AddBar.Enabled = true;
                _this.GridCFToolbar.ButtonImg_EditBar.Enabled = false;
                _this.GridCFToolbar.ButtonImg_PostBar.Enabled = false;
                _this.GridCFToolbar.ButtonImg_CancelBar.Enabled = false;
                return true;
            }
        }

        if (!_this.VclTreeGridControl.inTreeGrid) {

            _this.CancelMostrarInformacion(_this.VclGridCF.FocusedRowHandle);
        }
        if (_this.DataSet != null) {

            if (_this.IndiceLogico != null) {
                _this.IndiceLogico = Row.Index;
            }
            _this.DataSet.Status = SysCfg.MemTable.Properties.TStatus.None;
            // _this.DataSet.SetIndex(Row.IndexDataSet);
            _this.VclGridCF.IndexRow = Row.Index;
            _this.DataSet.RecordSet = _this.DataSet.Records[_this.VclGridCF.FocusedRowHandle.IndexDataSet];
            _this.HabilitarCamposFormGrid();//Actualizamos el detalle del Grid.
        }
    }
    /*Función para actualizar"*/
    this.EventUpdate = function (Row) {
        if (_this.DataSet.Records.length == 0) {
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines4"));
            return true;
        }
        _this.CancelMostrarInformacion(_this.VclGridCF.FocusedRowHandle);
        if (_this.IndiceLogico != null) {
            _this.IndiceLogico = Row.Index;
            _this.DataSet.Index = Row.IndexDataSet;
        } else {
            _this.DataSet.Index = Row.IndexDataSet;
        }
        _this.VclGridCF.IndexRow = Row.Index;
        if (_this.DataSet != null) {
            _this.DataSet.Update();
        }

    }
    /*Función para eliminar"*/
    this.EventDelete = function (Row) {
        if (_this.IndiceLogico == null) {
            if (_this.DataSet.Records.length == 0) {
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines4"));
                return true;
            }
        } else {
            if (_this.ItemTreePanel.length == 0) {
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines4"));
                return true;
            }
        }
        _this.CancelMostrarInformacion(_this.VclGridCF.FocusedRowHandle);
        if (_this.IndiceLogico != null) {
            _this.IndiceLogico = Row.Index;
            _this.DataSet.Index = Row.IndexDataSet;
        } else {
            _this.DataSet.Index = Row.IndexDataSet;
        }
        if (_this.DataSet != null) {
            _this.DataSet.Delete();
        }
    }
    /*Función para post"*/
    this.EventPost = function (Row) {
        if (_this.DataSet != null)
            _this.DataSet.Post();


    }
    /*Función para insertar una fila"*/
    this.EventInsert = function (Row) {
        _this.CancelMostrarInformacion(_this.VclGridCF.FocusedRowHandle);
        var sizeRecord = (_this.IndiceLogico == null) ? _this.DataSet.Records.length : _this.ItemTreePanel.length;
        if (sizeRecord == 0) {
            var indiceFila = 0;
            _this.DataSet.Append();
            var NewRowA = new TVclRowCF();
            var CellCheck = new TVclCellCF(); //CREA UNA CELDA PARA QUE SE AGREGA AL ROW
            CellCheck.Value = false;
            CellCheck.IndexColumn = 0; // ASIGNA O EXTRAE EL INDEX DE LA COLUMNA 
            CellCheck.IndexRow = indiceFila; // ASIGNA O EXTRAE EL INDEX DEL ROW 
            NewRowA.AddCell(CellCheck); // AGREGA O EXTRAE LA CELDA AL ROW

            for (var ContX = 1; ContX < _this.VclGridCF.Columns.length - 1; ContX++) {
                _this.VclGridCF.Columns[ContX].EnabledEditor = true;
                var tempDisable = _this.VclGridCF.Columns[ContX].ActiveEditor;
                _this.VclGridCF.Columns[ContX].ActiveEditor = _this.VclGridCF.Columns[ContX]._InsertActiveEditor;
                var Cell0 = new TVclCellCF();
                if (ContX == 0) {
                    Cell0.Value = true;
                } else {
                    if (_this.VclGridCF.Columns[ContX].DataType == SysCfg.DB.Properties.TDataType.DateTime) {

                        Cell0.Value = "";
                    } else {
                        Cell0.Value = _this.DataSet.RecordSet.Fields[_this.ObtenerIndexDataSource(ContX)].Value;
                    }
                    //Cell0.Value = _this.DataSet.RecordSet.Fields[_this.ObtenerIndexDataSource(ContX)].Value;
                }

                Cell0.IndexColumn = ContX;
                Cell0.IndexRow = indiceFila;
                NewRowA.AddCell(Cell0);

            }
            var CellButton = new TVclCellCF();
            CellButton.Value = "";
            CellButton.IndexColumn = _this.DataSet.RecordSet.FieldCount;
            CellButton.IndexRow = indiceFila;
            //CellButton.Control = new TVclStackPanel(CellButton.This, "1", 1, [[12]]);
            _this.DibujarCellButton(false, NewRowA, CellButton);
            NewRowA.onclick = function (NewRowA) {
            }
            NewRowA.ondblclick = function (NewRow) {

            }

            _this.VclGridCF.AddRow(NewRowA);
            NewRowA.Index = indiceFila;
            for (var ContX = 0; ContX < _this.VclGridCF.Columns.length - 1; ContX++) {
                _this.VclGridCF.Columns[ContX]._EnabledEditor = false;
            }
            _this.VclTreeGridControl.Init(_this.VclGridCF, _this.nameColumns, _this.ItemTreePanel, false, "asc", _this.DataSet);

            _this.Information.Init(_this.nameColumns.length, _this.DivitionColumnInformation);
            _this.Information.Refresh(NewRowA, _this.DataSet);

            return true;
        }
        else {
            var indiceRow = Row.Index;
            var indiceTreeGrid = Row.IndexTreeGrid;
            var indiceDataSet = Row.IndexDataSet;
            _this.DataSet._Index = indiceDataSet;
            _this.VclGridCF.IndexRow = indiceRow;
            _this.DataSet.Append();
            if (_this.DataSet.RecordSet != null) {
                _this.DataSet.Append();
                var NewRowA = new TVclRowCF();
                var CellCheck = new TVclCellCF();
                CellCheck.Value = false;
                CellCheck.IndexColumn = 0;
                CellCheck.IndexRow = indiceRow;
                NewRowA.AddCell(CellCheck);
                for (var ContX = 1; ContX < _this.VclGridCF.Columns.length - 1; ContX++) {
                    _this.VclGridCF.Columns[ContX].EnabledEditor = true;
                    var tempDisable = _this.VclGridCF.Columns[ContX].ActiveEditor;
                    _this.VclGridCF.Columns[ContX].ActiveEditor = _this.VclGridCF.Columns[ContX]._InsertActiveEditor;
                    var Cell0 = new TVclCellCF();
                    if (ContX == 0) {
                        Cell0.Value = true;
                    } else {
                        Cell0.Value = _this.DataSet.RecordSet.Fields[_this.ObtenerIndexDataSource(ContX)].Value;
                    }
                    Cell0.IndexColumn = ContX;
                    Cell0.IndexRow = indiceRow;
                    NewRowA.AddCell(Cell0);
                }
                var CellButton = new TVclCellCF();
                CellButton.Value = "";
                CellButton.IndexColumn = _this.DataSet.RecordSet.FieldCount;
                CellButton.IndexRow = indiceRow;
                //CellButton.Control = new TVclStackPanel(CellButton.This, "1", 1, [[12]]);
                _this.DibujarCellButton(false, NewRowA, CellButton);
                NewRowA.onclick = function (NewRowA) {
                }
                NewRowA.ondblclick = function (NewRow) {

                }

                _this.VclGridCF.AddRowBefore(false, NewRowA, _this.VclGridCF.FocusedRowHandle);
                NewRowA.Index = indiceRow;
                NewRowA.IndexTreeGrid = indiceTreeGrid;
                _this.VclGridCF.IndexRow = Row.Index;
                for (var ContX = 0; ContX < _this.VclGridCF.Columns.length - 1; ContX++) {
                    _this.VclGridCF.Columns[ContX]._EnabledEditor = false;
                }
                _this.HabilitarCamposFormGrid();//Actualizamos el detalle del Grid.
            }
        }
    }
    /*Función para actualizar el detalle de la fila seleccionada en el grid"*/
    this.HabilitarCamposFormGrid = function () {
        var isTreeGrid = _this.VclTreeGridControl.inTreeGrid
        var Row = (isTreeGrid) ? _this.VclTreeGridControl.getRowSeleccionado() : _this.VclGridCF.FocusedRowHandle;
        if (Row != null) {
            _this.Information.Refresh(Row, _this.DataSet);
        } else {
            _this.Information.VclGridCFInfo.ClearAll();
        }
    }
    /*Función...*/
    this.CancelMostrarInformacion = function (Row) {
        if (Row != null) {
            if (_this.DataSet.Status == SysCfg.MemTable.Properties.TStatus.Apped) {
                _this.VclGridCF.RemoveRow(Row.Index);
            }
            else if (_this.DataSet.Status == SysCfg.MemTable.Properties.TStatus.Update) {
                var NewRowA = new TVclRowCF();
                for (var ContX = 0; ContX < _this.VclGridCF.Columns.length - 1; ContX++) {
                    _this.VclGridCF.Columns[ContX].EnabledEditor = false;
                    var Cell0 = new TVclCellCF();
                    Cell0.IndexColumn = ContX;
                    Cell0.IndexRow = Row.Index;
                    if (ContX == 0) {
                        Cell0.Value = _this.VclGridCF.Rows[Row.Index].Cells[0].Value;
                    } else {
                        Cell0.Value = _this.DataSet.Records[Row.IndexDataSet].Fields[_this.VclGridCF.Columns[ContX].IndexDataSourceDefault].Value;
                    }
                    NewRowA.AddCell(Cell0);
                }
                var CellButton = new TVclCellCF();
                CellButton.Value = "";
                CellButton.IndexColumn = _this.DataSet.FieldCount;
                CellButton.IndexRow = Row.Index;

                _this.DibujarCellButton(true, NewRowA, CellButton);
                NewRowA.onclick = function (NewRow) {

                    _this.RowClick(NewRow);
                }
                NewRowA.ondblclick = function (NewRow) {
                    _this.RowDblClick(NewRow);
                }
                for (var ContX = 0; ContX < _this.VclGridCF.Columns.length - 1; ContX++) {
                    _this.VclGridCF.Columns[ContX].EnabledEditor = false;
                }
                NewRowA.Index = Row.Index;
                NewRowA.IndexTreeGrid = Row.IndexTreeGrid;
                NewRowA.IndexDataSet = Row.IndexDataSet;
                _this.VclGridCF.UpdatesRow(NewRowA, _this.VclGridCF._Rows[NewRowA.Index]);

            }
            _this.DataSet.Status = SysCfg.MemTable.Properties.TStatus.None;
            _this.DataSet.Cancel();

        }
    }
    /*Función para cambiar celdas de la fila a editables.*/
    this.MostrarInformacion = function () {
        if (_this.DataSet.RecordSet != null) {
            var indiceFila = _this.VclGridCF.IndexRow;
            var indiceTreeGrid = _this.VclGridCF.FocusedRowHandle.IndexTreeGrid;
            var indiceDataSet = _this.VclGridCF.FocusedRowHandle.IndexDataSet;
            var NewRowA = new TVclRowCF();
            var CellCheck = new TVclCellCF();
            CellCheck.Value = _this.SetSelectedRows;
            CellCheck.IndexColumn = 0;
            CellCheck.IndexRow = indiceFila;
            NewRowA.AddCell(CellCheck);
            for (var ContX = 1; ContX < _this.VclGridCF.Columns.length - 1; ContX++) {
                _this.VclGridCF.Columns[ContX].EnabledEditor = true;
                var tempDisable = _this.VclGridCF.Columns[ContX].ActiveEditor;
                _this.VclGridCF.Columns[ContX].ActiveEditor = _this.VclGridCF.Columns[ContX]._UpdateActiveEditor;
                var Cell0 = new TVclCellCF();
                Cell0.IndexColumn = ContX;
                Cell0.IndexRow = indiceFila;
                if (ContX == 0) {
                    Cell0.Value = _this.VclGridCF.Rows[indiceFila].Cells[0].Value;
                } else {
                    Cell0.Value = _this.DataSet.RecordSet.Fields[_this.VclGridCF.Columns[ContX].IndexDataSourceDefault].Value;
                }
                NewRowA.AddCell(Cell0);
            }
            var CellButton = new TVclCellCF();
            CellButton.Value = "";
            CellButton.IndexColumn = _this.DataSet.RecordSet.FieldCount;
            CellButton.IndexRow = indiceFila;
            //CellButton.Control = new TVclStackPanel(CellButton.This, "1", 1, [[12]]);
            _this.DibujarCellButton(false, NewRowA, CellButton);
            NewRowA.onclick = function (NewRowA) {
            }
            NewRowA.ondblclick = function (NewRow) {

            }
            NewRowA.Index = indiceFila;
            _this.VclGridCF.UpdatesRow(NewRowA, _this.VclGridCF._Rows[indiceFila]);
            for (var ContX = 0; ContX < _this.VclGridCF.Columns.length - 1; ContX++) {
                _this.VclGridCF.Columns[ContX].EnabledEditor = false;
            }
            NewRowA.Index = indiceFila;
            NewRowA.IndexTreeGrid = indiceTreeGrid;
            NewRowA.IndexDataSet = indiceDataSet;
            _this.VclGridCF.IndexRow = indiceFila;
        }
    }
    /*Función para la agrupación de registros*/
    this.groupBy = function (miarray, prop) {
        return miarray.reduce(function (groups, item) {
            var val = "" + item[prop][0];
            groups[val] = groups[val] || item;
            return groups;
        }, {});
    }
    /*Función para agrupar y mostrar las agrupación en listbox*/
    this.QueryLinqModal = function (Column) {
        _this.VclListBoxColumnsLinq.ClearAll();
        var listaGrupos = _this.groupBy(_this.ItemTreePanel, Column);
        var ListBoxItem = new TVclListBoxItem();
        ListBoxItem.Text = "All";
        ListBoxItem.Tag = "Custom";
        ListBoxItem.Index = -1;
        _this.VclListBoxColumnsLinq.AddListBoxItem(ListBoxItem);
        var ListBoxItemCostum = new TVclListBoxItem();
        ListBoxItemCostum.Text = "Custom" + _this.FilterQuery.GetStringQuery();
        ListBoxItemCostum.Tag = "Custom";
        ListBoxItemCostum.Index = -2;
        _this.VclListBoxColumnsLinq.AddListBoxItem(ListBoxItemCostum);

        var ListBoxItemFilterAllColumn = new TVclListBoxItem();
        ListBoxItemFilterAllColumn.Text = "Filter All Columns";
        ListBoxItemFilterAllColumn.Tag = "Filter All Columns";
        ListBoxItemFilterAllColumn.Index = -4;
        _this.VclListBoxColumnsLinq.AddListBoxItem(ListBoxItemFilterAllColumn);
        if (_this.VclGridCF.Columns[Column + 1].DataType != SysCfg.DB.Properties.TDataType.DateTime) {
            $.each(listaGrupos, function (index, itemGrupo) {
                ListBoxItem1 = new TVclListBoxItem();
                ListBoxItem1.Text = itemGrupo[Column][0];
                ListBoxItem1.Tag = itemGrupo[Column][0];
                ListBoxItem1.Index = Column;
                _this.VclListBoxColumnsLinq.AddListBoxItem(ListBoxItem1);
            });
        } else {
            $.each(listaGrupos, function (index, itemGrupo) {
                var datetemp = new Date(itemGrupo[Column][0]);
                try {
                    switch (_this.VclGridCF.Columns[Column + 1].ColumnStyle) {
                        case Componet.Properties.TExtrafields_ColumnStyle.None:
                            datetemp = SysCfg.DateTimeMethods.ToDateString(datetemp);
                            break;
                        case Componet.Properties.TExtrafields_ColumnStyle.Date:
                            datetemp = SysCfg.DateTimeMethods.ToDateString(datetemp);
                            break;
                        case Componet.Properties.TExtrafields_ColumnStyle.Time:
                            datetemp = SysCfg.DateTimeMethods.ToDateTimeString(datetemp);
                            break;
                    }
                }
                catch (e) {
                    datetemp = null;
                }
                ListBoxItem1 = new TVclListBoxItem();
                ListBoxItem1.Text = datetemp;
                ListBoxItem1.Tag = itemGrupo[Column][0];
                ListBoxItem1.Index = Column;
                _this.VclListBoxColumnsLinq.AddListBoxItem(ListBoxItem1);

            });
        }


    }
    /*Función para filtrar los registros según la agrupación selecionada*/
    this.QueryLinq = function (column, value, consulta) {
        switch (column) {
            case -1:
                _this.VclModalColumnsLinq.CloseModal();
                if (_this.VclTreeGridControl != null && _this.VclTreeGridControl.inTreeGrid) {
                    _this.SetRecordsPaginationFilterItemTreePanel(true, null, null, null, false);
                    _this.VclTreeGridControl.ItemTreePanel = _this.ItemTreePanel;
                    _this.VclTreeGridControl.agruparPorCampo();
                } else {
                    if (_this.IsPagination) {
                        _this.loadDataSetRecordsPaginationFilter(true, null, null, null, false);
                    } else {
                        _this.loadDataSetRecordsScrollFilter(true, null, null, null, false);
                    }
                }
                break;
            case -2:
                _this.VclModalColumnsLinq.CloseModal();
                _this.FilterQuery.ShowFilterAllColumn = false;
                _this.VclModalColumnsCustomLinq.ShowModal();
                break;
            case -3:
                if (_this.VclTreeGridControl != null && _this.VclTreeGridControl.inTreeGrid) {
                    _this.SetRecordsPaginationFilterItemTreePanel(false, null, null, consulta, false);
                    _this.VclTreeGridControl.ItemTreePanel = _this.ItemTreePanel;
                    _this.VclTreeGridControl.agruparPorCampo();
                } else {
                    if (_this.IsPagination) {
                        _this.loadDataSetRecordsPaginationFilter(false, null, null, consulta, false);
                    } else {
                        _this.loadDataSetRecordsScrollFilter(false, null, null, consulta, false);
                    }
                }


                break;
            case -4:
                _this.VclModalColumnsLinq.CloseModal();
                _this.FilterQuery.ShowFilterAllColumn = true;
                _this.VclModalColumnsCustomLinq.ShowModal();

                break;
            default:
                var VclGridCF = _this.VclGridCF;

                if (_this.VclTreeGridControl != null && _this.VclTreeGridControl.inTreeGrid) {
                    VclGridCF = _this.VclTreeGridControl.GridTreeGridControl;
                }

                _this.VclModalColumnsLinq.CloseModal();
                if (_this.VclGridCF.Columns[column + 1].DataType == SysCfg.DB.Properties.TDataType.Int32 ||
                    _this.VclGridCF.Columns[column + 1].DataType == SysCfg.DB.Properties.TDataType.Double ||
                    _this.VclGridCF.Columns[column + 1].DataType == SysCfg.DB.Properties.TDataType.Boolean) {
                    consulta = "$[" + column + "][0] ==" + value + "";
                } else {
                    consulta = "$[" + column + "][0] =='" + value + "'";
                }
                if (_this.VclTreeGridControl != null && _this.VclTreeGridControl.inTreeGrid) {
                    _this.SetRecordsPaginationFilterItemTreePanel(false, null, null, consulta, false);
                    _this.VclTreeGridControl.ItemTreePanel = _this.ItemTreePanel;
                    _this.VclTreeGridControl.agruparPorCampo();
                } else {
                    if (_this.IsPagination) {
                        _this.loadDataSetRecordsPaginationFilter(false, null, null, consulta, false);
                    } else {
                        _this.loadDataSetRecordsScrollFilter(false, null, null, consulta, false);
                    }
                }
                break;
        }
    }
    /*Función que se ejecuta al abrir la agrupación que ha sido expandida en el grid principal*/
    this.OnOpenTreeGridGroup = function () {
        /*Volvemos a crear los registros para poder agruparlos, ya que cuando se expandio los registros del itemtreepanel solo contenian los de la agrupación expandida
        Por ejemplo:
         -Al iniciar teníamos los elementos [0,1,2,3,4,5,6,7,8,9].(Valor del ItemTreePanel)
         -Luego agrupamos y nos quedo [0,2,4,6,8] y [1,3,5,7,9]
         -Luego expandimos el primer grupo en el gridPrincipal quedandonos [0,2,4,6,8] (Valor del ItemTreePanel).
        Ahora lo que hace esta función es volver a generar la agrupación, pero antes debemos recuperar el valor del ItemTreePanel para realizar la agrupación.
          -Llamamos a la función a SetRecordsFilterItemTreePanel() la setea el valor del ItemTreePanel [0,1,2,3,4,5,6,7,8,9].
          -this.VclTreeGridControl.OpenTreeGridGroup() forma la agrupación en el TreeGrid
        */
        _this.VclGridCF.EnablePanelGroup = true;//Mostramos div de la agrupación
        _this.GridCFToolbar.ButtonImg_ExpanderBar.Visible = true;//Mostramos bar de expander agrupación.
        _this.GridCFToolbar.ButtonImg_ContraerBar.Visible = true;//Mostramos bar de contraer agrupación.
        _this.GridCFToolbar.ButtonImg_NavigatorToolsBar_ChekedClickDown = false;//Ocultamos flecha de navegación
        _this.GridCFToolbar.ButtonImg_ColumnGroupBar.Cheked = true;//Marcamos el bar de agrupción
        _this.SetRecordsFilterItemTreePanel();//Seteamos el valor del ItemTreePanel
        _this.VclTreeGridControl.ItemTreePanel = _this.ItemTreePanel;//Actualizamos el ItemTreePanel del TreeGrid
        _this.VPagination.LinkImgGruopOpen.style.display = "none";//Ocultamos el boton de expandir de la paginación.
        _this.VclTreeGridControl.OpenTreeGridGroup();//Esta función realiza la agrupación en el treeGrid
    }
    /*Función que se ejecuta al abrir la edición de un campo FileSRV */
    this.FileSRVEvent = function (CellCampo, FileType) {
        var Modal2 = new TVclModal(document.body, null, "");
        Modal2.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines9"));
        Modal2.FooterContainer.style.textAlign = "center";
        var inObjectHtmlModal = Modal2.Body.This;
        ////////////////////////////////////////////////////
        if (SysCfg.App.Properties.Device != SysCfg.App.TDevice.Desktop) {
            var contOpenFile = new TVclStackPanel(inObjectHtmlModal, "1", 1, [[12], [12], [12], [12], [12]]);
            var contOpenFileData1 = new TVclStackPanel(inObjectHtmlModal, "1", 1, [[12], [12], [12], [12], [12]]);
            var contOpenFileData2 = new TVclStackPanel(inObjectHtmlModal, "1", 1, [[12], [12], [12], [12], [12]]);
            var contOpenFileData3 = new TVclStackPanel(inObjectHtmlModal, "1", 1, [[12], [12], [12], [12], [12]]);
            var contOpenFileData4 = new TVclStackPanel(inObjectHtmlModal, "1", 1, [[12], [12], [12], [12], [12]]);
            var OpenFile = new TOpenFile(contOpenFile.Column[0].This, "", true);
            var btnOpen = new TVclInputbutton(contOpenFileData1.Column[0].This, "");
            var txtDownload1 = new TVclTextBox(contOpenFileData1.Column[0].This, "txtName");
            var txtDownload2 = new TVclTextBox(contOpenFileData2.Column[0].This, "txtId");
            var txtDownload3 = new TVclTextBox(contOpenFileData3.Column[0].This, "txtFecha");
            var txtDownload4 = new TVclTextBox(contOpenFileData4.Column[0].This, "txtDescripcion");
        }
        else {
            var contOpenFile = new TVclStackPanel(inObjectHtmlModal, "1", 4, [[1, 8, 2, 1], [1, 8, 2, 1], [1, 8, 2, 1], [1, 8, 2, 1], [1, 8, 2, 1]]);
            var contOpenFileData1 = new TVclStackPanel(inObjectHtmlModal, "1", 4, [[1, 8, 2, 1], [1, 8, 2, 1], [1, 8, 2, 1], [1, 8, 2, 1], [1, 8, 2, 1]]);
            var contOpenFileData2 = new TVclStackPanel(inObjectHtmlModal, "1", 4, [[1, 8, 2, 1], [1, 8, 2, 1], [1, 8, 2, 1], [1, 8, 2, 1], [1, 8, 2, 1]]);
            var contOpenFileData3 = new TVclStackPanel(inObjectHtmlModal, "1", 4, [[1, 8, 2, 1], [1, 8, 2, 1], [1, 8, 2, 1], [1, 8, 2, 1], [1, 8, 2, 1]]);
            var contOpenFileData4 = new TVclStackPanel(inObjectHtmlModal, "1", 4, [[1, 8, 2, 1], [1, 8, 2, 1], [1, 8, 2, 1], [1, 8, 2, 1], [1, 8, 2, 1]]);
            var txtDownload1 = new TVclTextBox(contOpenFileData1.Column[1].This, "txtName");
            var txtDownload2 = new TVclTextBox(contOpenFileData2.Column[1].This, "txtId");
            var txtDownload3 = new TVclTextBox(contOpenFileData3.Column[1].This, "txtFecha");
            var txtDownload4 = new TVclTextBox(contOpenFileData4.Column[1].This, "txtDescripcion");
            var OpenFile = new TOpenFile(contOpenFile.Column[2].This, "", true);
            var btnOpen = new TVclInputbutton(contOpenFileData1.Column[2].This, "");
        }
        txtDownload2.This.placeholder = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines7") + "...";
        txtDownload4.This.placeholder = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines8") + "...";
        txtDownload1.This.placeholder = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines13") + "...";
        txtDownload3.This.placeholder = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines14") + "...";
        txtDownload1.VCLType = TVCLType.BS;
        txtDownload2.VCLType = TVCLType.BS;
        txtDownload3.VCLType = TVCLType.BS;
        txtDownload4.VCLType = TVCLType.BS;
        txtDownload1.Enabled = false;
        txtDownload2.Enabled = false;
        txtDownload3.Enabled = false;
        var senderGeneral = null;
        btnOpen.Text = "  " + UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines11") + "...  "
        btnOpen.onClick = function () {
            OpenFile.SetFile(function (sender, e) {
                senderGeneral = sender;
                txtDownload1.Text = sender.NameFile;
                var dateFile = new Date();
                dateFile = SysCfg.DateTimeMethods.ToDateString(dateFile);
                txtDownload3.Text = dateFile;
                txtDownload4.Text = "";
                btnEncode.Enabled = true;
                btnSave.Enabled = false;
            })
        }

        var btnEncode = new TVclInputbutton("", "");
        btnEncode.Text = "  " + UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines10");
        btnEncode.This.style.marginRight = "5px";
        btnEncode.VCLType = TVCLType.BS;
        btnEncode.onClick = function () {

            var idCode = txtDownload2.Text
            if (senderGeneral != null) {
                GetSDfile = Comunic.Ashx.Client.Methods.GetSDfile(Comunic.Properties.TFileType.GetEnum(FileType), Comunic.Properties.TFileOperationType._FileWrite, "", senderGeneral.NameFile, senderGeneral.File);
                idCode = GetSDfile.IDCode;
            }

            var unFILESRV = new Persistence.GP.Properties.TFILESRV();
            unFILESRV.FILENAME = txtDownload1.Text;
            unFILESRV.IDFILE = idCode;
            unFILESRV.ATTACHNETDATE = txtDownload3.Text;
            unFILESRV.DESCRIPTION = txtDownload4.Text;

            var FILESRVList = new Array();
            FILESRVList.push(unFILESRV);
            _this.ExecuteEventModal(CellCampo, Persistence.GP.Methods.FILESRVListtoStr(FILESRVList))
            console.log(Persistence.GP.Methods.FILESRVListtoStr(FILESRVList));
            Modal2.CloseModal();

        }

        var SaveFile = new TSaveFile(inObjectHtmlModal, "");
        SaveFile.NameFile = txtDownload1.Text;
        SaveFile.Path = txtDownload4.Text;

        var btnSave = new TVclInputbutton("", "");
        btnSave.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines12")
        btnSave.VCLType = TVCLType.BS;
        btnSave.onClick = function () {
            try {

                GetSDfile = Comunic.Ashx.Client.Methods.GetSDfile(Comunic.Properties.TFileType.GetEnum(FileType), Comunic.Properties.TFileOperationType._FileRead, txtDownload2.Text, txtDownload1.Text, new Array());


            } catch (e) {
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines6"));
                SysCfg.Log.Methods.WriteLog("FileSRVEvent Componet.Properties.TExtrafields_ColumnStyle.FileSRV", e);
            }
        }

        Modal2.AddFooterContent(btnEncode.This);
        Modal2.AddFooterContent(btnSave.This);

        if (CellCampo._Value != null && CellCampo._Value != "") {
            try {
                var FILESRVList = new Array();
                Persistence.GP.Methods.StrtoFILESRV(CellCampo.Value, FILESRVList);
                if (FILESRVList.length > 0) {
                    var unFILESRV = FILESRVList[0];
                    txtDownload1.Text = unFILESRV.FILENAME;
                    txtDownload2.Text = unFILESRV.IDFILE;
                    txtDownload3.Text = unFILESRV.ATTACHNETDATE;
                    txtDownload4.Text = unFILESRV.DESCRIPTION;
                    btnEncode.Enabled = true;
                }
            } catch (e) {
                txtDownload1.Text = "";
                txtDownload2.Text = "";
                txtDownload3.Text = "";
                txtDownload4.Text = "";
                btnSave.Enabled = false;
                btnEncode.Enabled = false;
            }

        }
        else {
            btnSave.Enabled = false;
            btnEncode.Enabled = false;
        }
        btnOpen.This.classList.add("btn");
        btnOpen.This.classList.add("btn-default");
        btnEncode.This.classList.add("btn");
        btnEncode.This.classList.add("btn-primary");
        btnSave.This.classList.add("btn");
        btnSave.This.classList.add("btn-success");
        Modal2.ShowModal();
    }
    /*Función ejecutada en toolbar cuando los registros de un filtro son vacíos*/
    this.SetToolBarRowEmpty = function () {
        //Habilitamos el bar Insert y desabilitamos el update,delete,post,cancel y las flechas. 
        _this.GridCFToolbar.ButtonImg_PriorFirstBar.Enabled = false;
        _this.GridCFToolbar.ButtonImg_PriorBar.Enabled = false;
        _this.GridCFToolbar.ButtonImg_NextBar.Enabled = false;
        _this.GridCFToolbar.ButtonImg_NextLastBar.Enabled = false;
        _this.GridCFToolbar.ButtonImg_PriorFirstBarB.Enabled = false;
        _this.GridCFToolbar.ButtonImg_PriorBarB.Enabled = false;
        _this.GridCFToolbar.ButtonImg_NextBarB.Enabled = false;
        _this.GridCFToolbar.ButtonImg_NextLastBarB.Enabled = false;
        _this.GridCFToolbar.ButtonImg_DeleteBar.Enabled = false;
        _this.GridCFToolbar.ButtonImg_AddBar.Enabled = true;
        _this.GridCFToolbar.ButtonImg_EditBar.Enabled = false;
        _this.GridCFToolbar.ButtonImg_PostBar.Enabled = false;
        _this.GridCFToolbar.ButtonImg_CancelBar.Enabled = false;
    }
    /*Función para mostrar un loader*/
    this.loaderShow = function (inValue) {
        if (inValue) {
            _this.VclStackPanelLoader.Row.This.style.display = "";
            _this.DivLoader.classList.add("loader");
            _this.VclStackPanelContentMemTable.Row.This.style.display = "none";
        } else {
            _this.VclStackPanelLoader.Row.This.style.display = "none";
            _this.DivLoader.classList.remove = "loader";
            _this.VclStackPanelLoader.Row.This.classList.remove("loader");
            _this.VclStackPanelContentMemTable.Row.This.style.display = "";
        }
    }
    /*Función para obtner el indice del Field en el DataSet*/
    this.ObtenerIndexDataSource = function (posicionColum) {
        if (_this.VclGridCF.AutoGenerateColumns) {
            return posicionColum - 1;
        }
        var nameColumns = _this.VclGridCF.Columns[posicionColum].Name;
        for (var i = 0; i < _this.DataSet.FieldCount; i++) {
            if (nameColumns == _this.DataSet.FieldDefs[i].FieldName) {
                return i;
            }
        }
        return null;
    }
    /*Función para agregar columnas el cfview*/
    this.AddColumnCF = function (inValue) {
        this._Columns.push(inValue);
    }
    /*Función para iniciar el componnete, crear todos los elementos del componente*/
    this.Init = function () {
        var VclStackPanelContentNavBar = new TVclStackPanel(_this.Object, "1", 1, [[12], [12], [12], [12], [12]]);
        _this.PanelMenuLeft = new TVclStackPanel(_this.Object, "1", 2, [[12, 12], [12, 12], [3, 9], [3, 9], [3, 9]]);
        _this.PanelMenuLeft.Column[0].This.style.paddingRight = "5px";
        _this.PanelMenuLeft.Column[1].This.style.paddingLeft = "0px";
        var NavMenuLeft = new ItHelpCenter.Componet.NavTabControls.TNavMenu(_this.PanelMenuLeft.Column[0].This);
        this.NavGroupMenu = new ItHelpCenter.Componet.NavTabControls.TNavGroupMenu(NavMenuLeft);
        this.NavGroupMenu._TypeNavBar = "SP";
        this.NavGroupMenu.Text = "Operaciones";
        this.NavGroupMenu.SrcImg = "image/24/Checklist.png";
        this.NavGroupMenu.NewRow.GridParent.This.style.marginBottom = "0px";
        this.NavGroupMenu.NewRow.GridParent.This.style.marginTop = "10px";
        this.ContenNavGroupMenuContenStackPanel = new TVclStackPanel(_this.PanelMenuLeft.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
        this.NavGroupMenuContenStackPanel = this.ContenNavGroupMenuContenStackPanel.Column[0].This;
        this.NavGroupMenu.NewRow.EventClose = function () {
            _this.PanelMenuLeft.Column[0].This.style.display = "none";
            _this.PanelMenuLeft.Column[1].This.classList.remove("col-md-9");
            _this.PanelMenuLeft.Column[1].This.classList.remove("col-lg-9");
            _this.PanelMenuLeft.Column[1].This.classList.remove("col-xl-9");
            _this.PanelMenuLeft.Column[1].This.classList.add("col-md-12");
            _this.PanelMenuLeft.Column[1].This.classList.add("col-lg-12");
            _this.PanelMenuLeft.Column[1].This.classList.add("col-xl-12");
            _this.PanelMenuLeft.Column[1].This.style.paddingLeft = "";
        }
        _this.PanelMenuLeft.Column[0].This.style.display = "none";
        _this.PanelMenuLeft.Column[1].This.classList.remove("col-md-9");
        _this.PanelMenuLeft.Column[1].This.classList.remove("col-lg-9");
        _this.PanelMenuLeft.Column[1].This.classList.remove("col-xl-9");
        _this.PanelMenuLeft.Column[1].This.classList.add("col-md-12");
        _this.PanelMenuLeft.Column[1].This.classList.add("col-lg-12");
        _this.PanelMenuLeft.Column[1].This.classList.add("col-xl-12");
        _this.PanelMenuLeft.Column[1].This.style.paddingLeft = "";

        _this.GridCFToolbar = new Componet.GridCF.TGridCFToolbar(VclStackPanelContentNavBar.Column[0].This, null, function (OutRes) { });
        _this.ButtonImg_Refresh_Visible = false;
        var objectHtmlToolBarBody = _this.PanelMenuLeft.Column[1].This;
        try {
            var ContenTextArea = new TVclStackPanel(objectHtmlToolBarBody, "1", 1, [[12], [12], [12], [12], [12]]);
            ContenTextArea.Row.This.style.display = "none";
            var textArea1 = document.createElement("iframe");
            textArea1.id = "txtArea1"
            ContenTextArea.Column[0].This.appendChild(textArea1);
        } catch (e) {
            console.log(e);
        }
        objectHtmlToolBarBody.style.color = Componet.GridCF.Properties.VConfig.Color;

        var ContenColumnModal = new TVclStackPanel(objectHtmlToolBarBody, "1", 1, [[12], [12], [12], [12], [12]]);
        _this.VclModalColumnExport = new TVclModal(ContenColumnModal.Column[0].This, ContenColumnModal.Column[0].This, "");
        _this.VclModalColumnExport.AddHeaderContent("Export");
        var divVMoveColum = document.createElement("div");
        _this.VMoveColum = new Componet.GridCF.TGridCFView.TMoveColum(divVMoveColum);
        _this.VclModalColumnsCustomLinq = new TVclModal(ContenColumnModal.Column[0].This, ContenColumnModal.Column[0].This, "");
        _this.VclModalColumnsLinq = new TVclModal(ContenColumnModal.Column[0].This, ContenColumnModal.Column[0].This, "");
        _this.VclListBoxColumnsLinq = new TVclListBox(_this.VclModalColumnsLinq.Body.This, "");
        _this.VclListBoxColumnsLinq.EnabledCheckBox = false;
        _this.VclListBoxColumnsLinq.OnCheckedListBoxItemChange = function (EventArgs, Object) {
            _this.QueryLinq(Object.FocusedListBoxItem.Index, Object.FocusedListBoxItem.Tag, "");
        }
        _this.VclListBoxColumns = new TVclListBox(_this.NavGroupMenuContenStackPanel, "");
        _this.VclListBoxColumns.EnabledCheckBox = true;
        _this.VclListBoxColumns.OnCheckedListBoxItemChange = function (EventArgs, Object) {
            _this.VclGridCF.showHideColumn(Object.FocusedListBoxItem.Index, EventArgs.Checked);
            if (_this.VclTreeGridControl != null && _this.VclTreeGridControl.inTreeGrid) {
                _this.VclTreeGridControl.showHideColumns(Object.FocusedListBoxItem.Index, EventArgs.Checked);
            }
        }
        _this.VclListBoxInformationColumns = new TVclListBox(_this.NavGroupMenuContenStackPanel, "");
        _this.VclListBoxInformationColumns.EnabledCheckBox = true;
        _this.VclListBoxInformationColumns.OnCheckedListBoxItemChange = function (EventArgs, Object) {

            _this.VclGridCF.Columns[Object.FocusedListBoxItem.Index]._VisibleVertical = EventArgs.Checked;

            if (_this.VclTreeGridControl != null && _this.VclTreeGridControl.inTreeGrid) {
                if (_this.VclTreeGridControl.GridTreeGridControl != null) {
                    _this.VclTreeGridControl.GridTreeGridControl.Columns[Object.FocusedListBoxItem.Index]._VisibleVertical = EventArgs.Checked;
                    _this.Information.BuildGridInfo(_this.Information.DivitionColumn, _this.VclTreeGridControl.GridTreeGridControl);
                }
            } else {
                _this.Information.BuildGridInfo(_this.Information.DivitionColumn, _this.VclGridCF);

            }
        }
        _this.VclStackPanelComboxTable = new TVclStackPanel(objectHtmlToolBarBody, "1", 1, [[12], [12], [12], [12], [12]]);
        _this.VclStackPanelComboxTable.Row.This.style.marginTop = "2px";
        _this.VclDrag = new TVclDrag("");
        var VclStackPanelContentHtml = new TVclStackPanel(objectHtmlToolBarBody, "1", 1, [[12], [12], [12], [12], [12]]);
        _this.VclStackPanelLoader = new TVclStackPanel(VclStackPanelContentHtml.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
        _this.VclStackPanelLoader.Row.This.style.display = "none";
        _this.VclStackPanelLoader.Column[0].This.classList.add("text-center");
        _this.DivLoader = document.createElement('div');
        _this.VclStackPanelLoader.Column[0].This.appendChild(_this.DivLoader);
        _this.DivLoader.style.display = "inline-block";
        _this.VclStackPanelContentMemTable = new TVclStackPanel(VclStackPanelContentHtml.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
        _this.VclStackPanelContentMemTable.Row.This.style.marginTop = "-2px";
        _this.VclStackPanelContentInfoMenTable = new TVclStackPanel(VclStackPanelContentHtml.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
        _this.VclStackPanelContentInfoMenTable.Row.This.style.display = "none";

        /*************/
        _this.VclStackPanelContentMenTableClose = new TVclStackPanel(_this.VclStackPanelContentMemTable.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
        var linkTable = document.createElement("a");
        var iconTable = document.createElement("div");
        iconTable.style.lineHeight = "0";
        iconTable.style.marginTop = "1px";
        iconTable.style.color = "#757575"
        linkTable.href = "#";
        iconTable.classList.add("icon");
        iconTable.classList.add("ion-backspace-outline");
        linkTable.appendChild(iconTable);
        linkTable.onclick = function () {
            _this.GridCFToolbar.ButtonImg_Grid_ChekedClickDown = false;

        }
        _this.VclStackPanelContentMenTableClose.Column[0].This.appendChild(linkTable);
        linkTable.style.float = "right";
        /*************/
        _this.VPagination = new Componet.GridCF.TGridCFView.TPagination(_this.VclStackPanelContentMemTable.Column[0].This, null, null, _this.GridCFToolbar.VclComboBox_RowsShowToolsBar);
        _this.VPagination.OnOpenTreeGridGroup = _this.OnOpenTreeGridGroup;
        _this.div0 = document.createElement("div");
        _this.div1 = document.createElement("div");
        _this.div2 = document.createElement("div");
        _this.div1.appendChild(_this.div2);
        _this.VclStackPanelContentMemTable.Column[0].This.appendChild(_this.div0);
        _this.VclStackPanelContentMemTable.Column[0].This.appendChild(_this.div1);
        _this.Status = new Componet.GridCF.TGridCFView.TStatusCF(VclStackPanelContentHtml.Column[0].This);
        _this.Status.ContlabelBE.Row.This.style.display = "none";
        _this.Status.linkInfoStatus.onclick = function () {
            _this.GridCFToolbar.ButtonImg_StatusBar_ChekedClickDown = false;
        }
        _this.VclGridCF = new TVclGridCF(_this.div0, "", "");
        _this.VclGridCF.ResponsiveCont.style.border = "1px solid #c4c4c4";//Linea modificada
        _this.VclStackPanelContentCustomFilter = new TVclStackPanel(VclStackPanelContentHtml.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
        _this.VclStackPanelContentCustomFilter.Row.This.style.marginTop = "10px";
        _this.VclListBoxCustomFilter = new TVclListBox(_this.VclStackPanelContentCustomFilter.Column[0].This, "");
        _this.VclListBoxCustomFilter.EnabledCheckBox = true;
        _this.VclListBoxCustomFilter.OnCheckedListBoxItemChange = function () {
            for (var i = 1; i < _this.VclGridCF.Columns.length - 1; i++) {
                var ListBoxItem1 = new TVclListBoxItem();
                //ListBoxItem1.This.Row.This.style.marginLeft = "0px";
                //ListBoxItem1.This.Row.This.style.marginRight= "0px";
                if (!_this.VclGridCF.Columns[i].Visible) {
                    ListBoxItem1.This.Row.This.style.display = "none";
                }
                ListBoxItem1.Text = _this.VclGridCF.Columns[i].Caption;
                ListBoxItem1.Index = i;
                ListBoxItem1.Checked = _this.VclGridCF.Columns[i].Visible;
                _this.VclListBoxColumns.AddListBoxItem(ListBoxItem1);
            }
        };
        _this.VclGridCF.This.style.fontSize = SysCfg.App.IniDB.ReadString("Componet_GridCF", "FontSize", null) + "px";
        _this.VclGridCF.This.style.fontFamily = SysCfg.App.IniDB.ReadString("Componet_GridCF", "FontFamily", null) + "";
        _this.VclGridCF.This.style.fontWeight = SysCfg.App.IniDB.ReadString("Componet_GridCF", "FontWeight", null) + "";
        _this.VclGridCF.This.style.fontStyle = SysCfg.App.IniDB.ReadString("Componet_GridCF", "FontStyle", null) + "";
        _this.VclGridCF.Header.style.color = SysCfg.App.IniDB.ReadString("Componet_GridCF", "HeaderColor", null) + "";
        _this.VclGridCF.Body.style.color = SysCfg.App.IniDB.ReadString("Componet_GridCF", "BodyColor", null) + "";
        _this.VclGridCF.This.style.textAlign = SysCfg.App.IniDB.ReadString("Componet_GridCF", "TextAlign", null) + "";
        _this.Information = new Componet.GridCF.TGridCFView.TInformation(this, _this.VclStackPanelContentInfoMenTable.Column[0].This, "", "", _this.VclListBoxInformationColumns);
        _this.Information.VclGridCFInfo.This.style.fontSize = SysCfg.App.IniDB.ReadString("Componet_GridCF", "FontSize", null) + "px";
        _this.Information.VclGridCFInfo.This.style.fontFamily = SysCfg.App.IniDB.ReadString("Componet_GridCF", "FontFamily", null) + "";
        _this.Information.VclGridCFInfo.This.style.fontWeight = SysCfg.App.IniDB.ReadString("Componet_GridCF", "FontWeight", null) + "";
        _this.Information.VclGridCFInfo.This.style.fontStyle = SysCfg.App.IniDB.ReadString("Componet_GridCF", "FontStyle", null) + "";
        _this.Information.linkInfoTable.onclick = function () {
            _this.GridCFToolbar.ButtonImg_DatailsViewBar_ChekedClickDown = false;

        }
        _this.VclTreeGridControl = new Componet.GridCF.TVclTreeGridControl(_this.VclStackPanelComboxTable.Column[0].This, _this.VclStackPanelContentMemTable.Row.This, _this.VclListBoxColumns, _this.GridCFToolbar.ButtonImg_ExpanderBar, _this.GridCFToolbar.ButtonImg_ContraerBar);
        _this.VclTreeGridControl.GridCFToolbar = _this.GridCFToolbar;
        _this.GridCFToolbar.ButtonImg_ExpanderBar.Visible = false;
        _this.GridCFToolbar.ButtonImg_ContraerBar.Visible = false;
        _this.VclTreeGridControl.VclDrag = _this.VclDrag;
        _this.VclGridCF.OnDataSourceChanged = function (Event, inDataSource) {

            _this.DataSet = inDataSource;
            _this.loadDataSetColumns(_this.DataSet);
            _this.loadDataSetRecords();

        }
        _this.VclGridCF.OnAutoGenerateColumnsChanged = function (Event, inAutoGenerateColumns) {
        }
        _this.VclModalColumnsCustomLinq.AddHeaderContent("Custom Query");
        _this.FilterQuery = new Componet.GridCF.TGridCFView.TFilterQuery(_this.VclModalColumnsCustomLinq.Body.This);
        var btnQuery = new TVclInputbutton("", "");
        btnQuery.Text = "Excute query";
        btnQuery.VCLType = TVCLType.BS;
        btnQuery.onClick = function () {
            var query = _this.FilterQuery.CreateQuery();
            if (query != null) {
                _this.QueryLinq(-3, "", query);
                _this.VclModalColumnsCustomLinq.CloseModal();
            } else {
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1"));
            }
        }
        _this.VclModalColumnsCustomLinq.AddFooterContent(btnQuery.This);
        _this.GridCFToolbar.onClick = function (Obj, ToolbarControlls) {
            if (_this.DataSet == null) {
                return true;
            }
            switch (ToolbarControlls) {

                case Componet.GridCF.TToolbarControlls.ButtonImg_Refresh:
                    _this.GridRefresch();
                    break;
                case Componet.GridCF.TToolbarControlls.ButtonImg_Font:
                    _this.VclGridCF.This.style.fontFamily = Obj.ItemGroupBarFuente.Text;
                    _this.Information.VclGridCFInfo.This.style.fontFamily = Obj.ItemGroupBarFuente.Text;
                    Componet.GridCF.Properties.VConfig.FontFamily = Obj.ItemGroupBarFuente.Text;
                    if (_this.VclTreeGridControl != null && _this.VclTreeGridControl.inTreeGrid) {
                        _this.VclTreeGridControl.GridTreeGridControl.This.style.fontFamily = Obj.ItemGroupBarFuente.Text;

                    }
                    break;
                case Componet.GridCF.TToolbarControlls.ButtonImg_Size:
                    _this.VclGridCF.This.style.fontSize = Obj.ItemGroupBarTamanio.Text + "px";
                    Componet.GridCF.Properties.VConfig.FontSize = Obj.ItemGroupBarTamanio.Text;
                    if (_this.VclTreeGridControl != null && _this.VclTreeGridControl.inTreeGrid) {
                        _this.VclTreeGridControl.GridTreeGridControl.This.style.fontSize = Obj.ItemGroupBarTamanio.Text + "px";
                    }
                    _this.Information.VclGridCFInfo.This.style.fontSize = Obj.ItemGroupBarTamanio.Text + "px";
                    //alert(Obj.ItemGroupBarTamanio.Text);
                    break;

                case Componet.GridCF.TToolbarControlls.ButtonImg_Bold:
                    if (Obj.ButtonImg_Bold.Cheked) {
                        _this.VclGridCF.This.style.fontWeight = "bold";
                        _this.Information.VclGridCFInfo.This.style.fontWeight = "bold";

                    } else {
                        _this.VclGridCF.This.style.fontWeight = "";
                        _this.Information.VclGridCFInfo.This.style.fontWeight = "";
                    }
                    Componet.GridCF.Properties.VConfig.FontWeight = _this.VclGridCF.This.style.fontWeight;



                    break;
                case Componet.GridCF.TToolbarControlls.ButtonImg_Kursive:
                    if (Obj.ButtonImg_Kursive.Cheked) {
                        _this.VclGridCF.This.style.fontStyle = "italic";
                        _this.Information.VclGridCFInfo.This.style.fontStyle = "italic";
                    } else {
                        _this.VclGridCF.This.style.fontStyle = "";
                        _this.Information.VclGridCFInfo.This.style.fontStyle = "";
                    }
                    Componet.GridCF.Properties.VConfig.FontStyle = _this.VclGridCF.This.style.fontStyle;
                    if (_this.VclTreeGridControl != null && _this.VclTreeGridControl.inTreeGrid) {
                        _this.VclTreeGridControl.GridTreeGridControl.This.style.fontStyle = _this.VclGridCF.This.style.fontStyle;
                    }
                    break;

                case Componet.GridCF.TToolbarControlls.ButtonImg_AlignCenter:

                    if (Obj.ButtonImg_AlignCenter.Cheked) {
                        _this.VclGridCF.This.style.textAlign = "center";
                    } else {
                        _this.VclGridCF.This.style.textAlign = "";

                    }
                    if (_this.VclTreeGridControl != null && _this.VclTreeGridControl.inTreeGrid) {
                        _this.VclTreeGridControl.GridTreeGridControl.This.style.textAlign = _this.VclGridCF.This.style.textAlign;
                    }
                    Componet.GridCF.Properties.VConfig.TextAlign = _this.VclGridCF.This.style.textAlign;
                    Obj.ButtonImg_AlignCenter.Cheked = Obj.ButtonImg_AlignCenter.Cheked;
                    Obj.ButtonImg_AlignJustify.Cheked = false;
                    Obj.ButtonImg_AlignLeft.Cheked = false;
                    Obj.ButtonImg_AlignRight.Cheked = false;


                    break;
                case Componet.GridCF.TToolbarControlls.ButtonImg_AlignJustify:
                    if (Obj.ButtonImg_AlignJustify.Cheked) {
                        _this.VclGridCF.This.style.textAlign = "justify";
                    } else {
                        _this.VclGridCF.This.style.textAlign = "";
                    }

                    if (_this.VclTreeGridControl != null && _this.VclTreeGridControl.inTreeGrid) {
                        _this.VclTreeGridControl.GridTreeGridControl.This.style.textAlign = _this.VclGridCF.This.style.textAlign;
                    }
                    Componet.GridCF.Properties.VConfig.textAlign = _this.VclGridCF.This.style.textAlign;

                    Obj.ButtonImg_AlignCenter.Cheked = false;
                    Obj.ButtonImg_AlignJustify.Cheked = Obj.ButtonImg_AlignJustify.Cheked;
                    Obj.ButtonImg_AlignLeft.Cheked = false;
                    Obj.ButtonImg_AlignRight.Cheked = false;

                    break;
                case Componet.GridCF.TToolbarControlls.ButtonImg_AlignLeft:

                    if (Obj.ButtonImg_AlignLeft.Cheked) {
                        _this.VclGridCF.This.style.textAlign = "left";
                    } else {
                        _this.VclGridCF.This.style.textAlign = "";
                    }
                    if (_this.VclTreeGridControl != null && _this.VclTreeGridControl.inTreeGrid) {
                        _this.VclTreeGridControl.GridTreeGridControl.This.style.textAlign = _this.VclGridCF.This.style.textAlign;
                    }
                    Componet.GridCF.Properties.VConfig.TextAlign = _this.VclGridCF.This.style.textAlign;
                    Obj.ButtonImg_AlignCenter.Cheked = false;
                    Obj.ButtonImg_AlignJustify.Cheked = false;
                    Obj.ButtonImg_AlignLeft.Cheked = Obj.ButtonImg_AlignLeft.Cheked;
                    Obj.ButtonImg_AlignRight.Cheked = false;

                    break;
                case Componet.GridCF.TToolbarControlls.ButtonImg_AlignRight:

                    if (Obj.ButtonImg_AlignRight.Cheked) {
                        _this.VclGridCF.This.style.textAlign = "right";
                    } else {
                        _this.VclGridCF.This.style.textAlign = "";
                    }
                    if (_this.VclTreeGridControl != null && _this.VclTreeGridControl.inTreeGrid) {
                        _this.VclTreeGridControl.GridTreeGridControl.This.style.textAlign = _this.VclGridCF.This.style.textAlign;
                    }
                    Componet.GridCF.Properties.VConfig.TextAlign = _this.VclGridCF.This.style.textAlign;
                    Obj.ButtonImg_AlignCenter.Cheked = false;
                    Obj.ButtonImg_AlignJustify.Cheked = false;
                    Obj.ButtonImg_AlignLeft.Cheked = false;
                    Obj.ButtonImg_AlignRight.Cheked = Obj.ButtonImg_AlignRight.Cheked;

                    break;
                case Componet.GridCF.TToolbarControlls.ButtonImg_ExportPDFBar:
                    _this.VclModalColumnExport.ShowModal();
                    if (_this.VclListBoxColumnsExport != null) {
                        _this.VclListBoxColumnsExport.ClearAll();
                    }

                    $(_this.VclModalColumnExport.Body.This).html("");
                    var VclStackPanelFormato = new TVclStackPanel(_this.VclModalColumnExport.Body.This, "1", 1, [[12], [12], [12], [12], [12]]);
                    var row0 = new TVclStackPanel(VclStackPanelFormato.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
                    var row1 = new TVclStackPanel(VclStackPanelFormato.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
                    //var VclStackPanelFormato .Column[0].This.style.marginTop = "-20px";
                    var btnExportFormato = new TVclButton(row0.Column[0].This, "");
                    btnExportFormato.Text = "Download";
                    btnExportFormato.VCLType = TVCLType.BS;
                    btnExportFormato.onClick = function () {
                        var arrayIndexColumn = new Array();
                        for (var i = 0; i < _this.VclListBoxColumnsExport.ListBoxItems.length; i++) {
                            if (_this.VclListBoxColumnsExport.ListBoxItems[i].CheckBox.Checked) {
                                arrayIndexColumn.push(_this.VclListBoxColumnsExport.ListBoxItems[i].Index);
                            }
                        }
                        if (arrayIndexColumn.length > 0) {
                            _this.ExportData(1, arrayIndexColumn);
                            UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2");

                        } else {
                            UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3");
                        }

                    }

                    var VclListBoxExportCheked = new TVclListBox(row0.Column[0].This, "");
                    VclListBoxExportCheked.EnabledCheckBox = true;
                    VclListBoxExportCheked.OnCheckedListBoxItemChange = function (EventArgs, Object) {
                        var cheked = Object.FocusedListBoxItem.Checked;
                        if (cheked) {
                            _this.VclListBoxColumnsExport.CheckedAll();
                        } else {
                            _this.VclListBoxColumnsExport.UnCheckedAll();
                        }

                    }
                    var ListBoxItemVclListBoxExportCheked = new TVclListBoxItem();
                    ListBoxItemVclListBoxExportCheked.Text = "CHECKED All (COLUMNS OF TABLE)";
                    ListBoxItemVclListBoxExportCheked.Index = i;
                    ListBoxItemVclListBoxExportCheked.Checked = true;
                    VclListBoxExportCheked.AddListBoxItem(ListBoxItemVclListBoxExportCheked);
                    _this.VclListBoxColumnsExport = new TVclListBox(row0.Column[0].This, "");
                    _this.VclListBoxColumnsExport.EnabledCheckBox = true;

                    for (var i = 1; i < _this.VclGridCF.Columns.length - 1; i++) {
                        var ListBoxItem1 = new TVclListBoxItem();
                        if (!_this.VclGridCF.Columns[i].Visible) {
                            ListBoxItem1.This.Row.This.style.display = "none";
                        }
                        ListBoxItem1.Text = _this.VclGridCF.Columns[i].Caption;
                        ListBoxItem1.Index = i;
                        ListBoxItem1.Checked = true;
                        _this.VclListBoxColumnsExport.AddListBoxItem(ListBoxItem1);
                    }


                    break;
                case Componet.GridCF.TToolbarControlls.ButtonImg_ExportExcelBar:
                    _this.ExportData(2);
                    break;
                case Componet.GridCF.TToolbarControlls.ButtonImg_ExportWordBar:
                    _this.VclModalColumnExport.ShowModal();
                    if (_this.VclListBoxColumnsExport != null) {
                        _this.VclListBoxColumnsExport.ClearAll();
                    }

                    $(_this.VclModalColumnExport.Body.This).html("");
                    var VclStackPanelFormato = new TVclStackPanel(_this.VclModalColumnExport.Body.This, "1", 1, [[12], [12], [12], [12], [12]]);
                    var row0 = new TVclStackPanel(VclStackPanelFormato.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
                    var row1 = new TVclStackPanel(VclStackPanelFormato.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
                    //var VclStackPanelFormato .Column[0].This.style.marginTop = "-20px";
                    var btnExportFormato = new TVclButton(row0.Column[0].This, "");
                    btnExportFormato.Text = "Download";
                    btnExportFormato.VCLType = TVCLType.BS;
                    btnExportFormato.onClick = function () {
                        var arrayIndexColumn = new Array();
                        for (var i = 0; i < _this.VclListBoxColumnsExport.ListBoxItems.length; i++) {
                            if (_this.VclListBoxColumnsExport.ListBoxItems[i].CheckBox.Checked) {

                                arrayIndexColumn.push(_this.VclListBoxColumnsExport.ListBoxItems[i].Index);
                            }

                        }

                        if (arrayIndexColumn.length > 0) {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2"));
                            var datatablaJSON = null;
                            if (_this.IndiceLogico == null) {
                                datatablaJSON = _this.VclGridCF.getGridFormatJSON(arrayIndexColumn);
                            } else {
                                datatablaJSON = _this.getGridFormatJSON(arrayIndexColumn);
                            }

                            //var border = '"' + 1+ '"';
                            var cadenaTabla = '<table border="' + 1 + '"><tr>';
                            for (var i = 0; i < datatablaJSON.columns.length; i++) {
                                cadenaTabla += '<td>' + datatablaJSON.columns[i] + '</td>';
                            }
                            cadenaTabla = cadenaTabla + '</tr>';
                            if (datatablaJSON.rows.length > 0) {
                                for (var i = 0; i < datatablaJSON.rows.length; i++) {
                                    var filaRow = '<tr>';
                                    for (var j = 0; j < datatablaJSON.columns.length; j++) {
                                        filaRow += '<td>' + datatablaJSON.rows[i][j] + '</td>';
                                    }
                                    var filaRow = filaRow + '</tr>';
                                    cadenaTabla += filaRow;
                                }
                            }
                            cadenaTabla = cadenaTabla + '</table>';
                            var divTablaWord = document.createElement("div");
                            $(divTablaWord).html(cadenaTabla);
                            $(divTablaWord).wordExport();
                            $(divTablaWord).html("");
                        } else {
                            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3"));
                        }

                    }
                    var VclListBoxExportCheked = new TVclListBox(row0.Column[0].This, "");
                    VclListBoxExportCheked.EnabledCheckBox = true;
                    VclListBoxExportCheked.OnCheckedListBoxItemChange = function (EventArgs, Object) {
                        var cheked = Object.FocusedListBoxItem.Checked;
                        if (cheked) {
                            _this.VclListBoxColumnsExport.CheckedAll();
                        } else {
                            _this.VclListBoxColumnsExport.UnCheckedAll();
                        }

                    }
                    var ListBoxItemVclListBoxExportCheked = new TVclListBoxItem();
                    ListBoxItemVclListBoxExportCheked.Text = "CHECKED All (COLUMNS OF TABLE)";
                    ListBoxItemVclListBoxExportCheked.Index = i;
                    ListBoxItemVclListBoxExportCheked.Checked = true;
                    VclListBoxExportCheked.AddListBoxItem(ListBoxItemVclListBoxExportCheked);
                    _this.VclListBoxColumnsExport = new TVclListBox(row0.Column[0].This, "");
                    _this.VclListBoxColumnsExport.EnabledCheckBox = true;


                    _this.VclListBoxColumnsExport = new TVclListBox(row0.Column[0].This, "");
                    _this.VclListBoxColumnsExport.EnabledCheckBox = true;
                    for (var i = 1; i < _this.VclGridCF.Columns.length - 1; i++) {
                        var ListBoxItem1 = new TVclListBoxItem();
                        if (!_this.VclGridCF.Columns[i].Visible) {
                            ListBoxItem1.This.Row.This.style.display = "none";
                        }
                        ListBoxItem1.Text = _this.VclGridCF.Columns[i].Caption;
                        ListBoxItem1.Index = i;
                        ListBoxItem1.Checked = true;
                        _this.VclListBoxColumnsExport.AddListBoxItem(ListBoxItem1);
                    }

                    break;
                case Componet.GridCF.TToolbarControlls.ButtonImg_RowEditToolsBar:
                    _this.VclGridCF.EnableEditOptionsColumn = Obj.ButtonImg_RowEditToolsBar_ChekedClickDown;
                    if (_this.VclTreeGridControl != null && _this.VclTreeGridControl.inTreeGrid) {
                        //_this.VclTreeGridControl.enableEditOptiosColumn(Obj.ButtonImg_RowEditToolsBar_ChekedClickDown);
                    } else {

                    }
                    break;
                case Componet.GridCF.TToolbarControlls.ButtonImg_RowCheckToolsBar:
                    _this.VclGridCF.showHideColumn(0, Obj.ButtonImg_RowCheckToolsBar_ChekedClickDown);
                    if (_this.VclTreeGridControl != null && _this.VclTreeGridControl.inTreeGrid) {
                        _this.VclTreeGridControl.GridTreeGridControl.showHideColumn(0, Obj.ButtonImg_RowCheckToolsBar_ChekedClickDown);
                    }
                    break;
                case Componet.GridCF.TToolbarControlls.ButtonImg_MoveColumnsBar:
                    _this.NavGroupMenu.Text = Obj.ButtonImg_MoveColumnsBar.Text;
                    _this.NavGroupMenu.SrcImg = Obj.ButtonImg_MoveColumnsBar.SrcImg;
                    _this.NavGroupMenu.NewRow.LabelTextGroup.This.style.fontWeight = "bold";
                    _this.PanelMenuLeft.Column[0].This.style.display = "";
                    _this.PanelMenuLeft.Column[1].This.classList.remove("col-md-12");
                    _this.PanelMenuLeft.Column[1].This.classList.remove("col-lg-12");
                    _this.PanelMenuLeft.Column[1].This.classList.remove("col-xl-12");
                    _this.PanelMenuLeft.Column[1].This.classList.add("col-md-9");
                    _this.PanelMenuLeft.Column[1].This.classList.add("col-lg-9");
                    _this.PanelMenuLeft.Column[1].This.classList.add("col-xl-9");
                    _this.PanelMenuLeft.Column[1].This.style.paddingLeft = "5px";
                    $(_this.NavGroupMenuContenStackPanel).html("");
                    _this.VMoveColum = new Componet.GridCF.TGridCFView.TMoveColum(_this.NavGroupMenuContenStackPanel);
                    _this.VMoveColum.Init(_this.nameColumns, _this);
                    break;

                case Componet.GridCF.TToolbarControlls.ButtonImg_ColumnGroupBar:
                    _this.VclGridCF.EnablePanelGroup = Obj.ButtonImg_ColumnGroupBar_ChekedClickDown;
                    if (!Obj.ButtonImg_ColumnGroupBar_ChekedClickDown) {
                        _this.GridCFToolbar.ButtonImg_ExpanderBar.Visible = false;
                        _this.GridCFToolbar.ButtonImg_ContraerBar.Visible = false;
                        _this.GridCFToolbar.ButtonImg_NavigatorToolsBar_ChekedClickDown = true;
                        $(_this.VclStackPanelContentMemTable.Row.This).show();
                        _this.VPagination.LinkImgGruopOpen.style.display = "none";
                        _this.restartDataRecords();
                        if (_this.VclTreeGridControl.GridTreeGridControl != null) {
                            _this.VclTreeGridControl.GridTreeGridControl.ClearAllRows();
                        }
                    }
                    _this.VclTreeGridControl.Show = _this.VclGridCF.EnablePanelGroup;

                    break;
                case Componet.GridCF.TToolbarControlls.ButtonImg_NavigatorToolsBar:
                    _this.VclGridCF.EnableNavigatorControl = Obj.ButtonImg_NavigatorToolsBar_ChekedClickDown;
                    Obj.ButtonImg_PriorFirstBar.Visible = _this.VclGridCF.EnableNavigatorControl;
                    Obj.ButtonImg_PriorBar.Visible = _this.VclGridCF.EnableNavigatorControl;
                    Obj.ButtonImg_NextBar.Visible = _this.VclGridCF.EnableNavigatorControl;
                    Obj.ButtonImg_NextLastBar.Visible = _this.VclGridCF.EnableNavigatorControl;
                    Obj.ButtonImg_PriorFirstBarB.Visible = _this.VclGridCF.EnableNavigatorControl;
                    Obj.ButtonImg_PriorBarB.Visible = _this.VclGridCF.EnableNavigatorControl;
                    Obj.ButtonImg_NextBarB.Visible = _this.VclGridCF.EnableNavigatorControl;
                    Obj.ButtonImg_NextLastBarB.Visible = _this.VclGridCF.EnableNavigatorControl;
                    break;
                case Componet.GridCF.TToolbarControlls.ButtonImg_EditToolsBar:
                    _this.VclGridCF.EnableNavigatorEdit = Obj.ButtonImg_EditToolsBar_ChekedClickDown;
                    break;
                case Componet.GridCF.TToolbarControlls.ButtonImg_EditColumHeader:
                    _this.NavGroupMenu.Text = _this.NavGroupMenu.Text = Obj.ButtonImg_EditColumHeader.Text;
                    _this.NavGroupMenu.SrcImg = Obj.ButtonImg_EditColumHeader.SrcImg;
                    _this.NavGroupMenu.NewRow.LabelTextGroup.This.style.fontWeight = "bold";
                    _this.PanelMenuLeft.Column[0].This.style.display = "";
                    _this.PanelMenuLeft.Column[1].This.classList.remove("col-md-12");
                    _this.PanelMenuLeft.Column[1].This.classList.remove("col-lg-12");
                    _this.PanelMenuLeft.Column[1].This.classList.remove("col-xl-12");
                    _this.PanelMenuLeft.Column[1].This.classList.add("col-md-9");
                    _this.PanelMenuLeft.Column[1].This.classList.add("col-lg-9");
                    _this.PanelMenuLeft.Column[1].This.classList.add("col-xl-9");
                    _this.PanelMenuLeft.Column[1].This.style.paddingLeft = "5px";
                    if (_this.VclGridCFColumnsHeader != null) {
                        _this.VclGridCFColumnsHeader.ClearAll();
                    }
                    $(_this.NavGroupMenuContenStackPanel).html("");
                    var VclInputbuttonColumnsHeader = new TVclButton(_this.NavGroupMenuContenStackPanel, ""); //CREA UN BOTON
                    VclInputbuttonColumnsHeader.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines15");// PROPIEDAD PARA ASIGNAR O EXTRAER EL TEXTO
                    VclInputbuttonColumnsHeader.This.classList.add("btn");
                    VclInputbuttonColumnsHeader.This.classList.add("btn-primary");
                    VclInputbuttonColumnsHeader.onClick = function () { // ASIGNAR EL EVENTO CUANDO SE HAGA CLIC EN EL BOTON
                        for (i = 0; i < _this.VclGridCFColumnsHeader.Rows.length; i++) {
                            _this.VclGridCF.Columns[i + 1].CaptionOrder = _this.VclGridCFColumnsHeader.Rows[i].Cells[1].Value;

                            if (_this.VclTreeGridControl.inTreeGrid && !_this.VclTreeGridControl.OptionVisibleGrid) {
                                _this.VclTreeGridControl.GridTreeGridControl.Columns[i + 1].CaptionOrder = _this.VclGridCFColumnsHeader.Rows[i].Cells[1].Value;
                            }

                        }
                    }
                    VclInputbuttonColumnsHeader.This.style.width = "100%";
                    VclInputbuttonColumnsHeader.This.style.borderRadius = "0px";

                    _this.VclGridCFColumnsHeader = new TVclGridCF(_this.NavGroupMenuContenStackPanel, "", "");
                    var ColMem1 = new TVclColumnCF();
                    ColMem1.EnabledEditor = true;
                    ColMem1.DataType = SysCfg.DB.Properties.TDataType.String;
                    ColMem1.ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.None;
                    for (var i = 1; i < _this.VclGridCF.Columns.length - 1; i++) {
                        var NewRow = new TVclRowCF();
                        var CellInfo0 = new TVclCellCF();
                        CellInfo0.Value = _this.VclGridCF.Columns[i].Name + ":";
                        CellInfo0.Column = null;
                        CellInfo0.IndexColumn = 0;
                        CellInfo0.IndexRow = i;
                        var CellInfo1 = new TVclCellCF();
                        CellInfo1.Value = _this.VclGridCF.Columns[i].Caption;
                        CellInfo1.Column = ColMem1;
                        CellInfo1.IndexColumn = 1;
                        CellInfo1.IndexRow = i;
                        NewRow.AddCell(CellInfo0);
                        NewRow.AddCell(CellInfo1);
                        _this.VclGridCFColumnsHeader.AddRowInfo(NewRow);
                    }
                    break;
                case Componet.GridCF.TToolbarControlls.ButtonImg_DatailsViewBar:
                    _this.VclGridCF.EnableDescriptionRow = Obj.ButtonImg_DatailsViewBar_ChekedClickDown;
                    Obj.ButtonImg_GridDetailsVisibleColumnBar.Visible = Obj.ButtonImg_DatailsViewBar_ChekedClickDown;
                    break;
                case Componet.GridCF.TToolbarControlls.ButtonImg_Grid:
                    if (Obj.ButtonImg_Grid_ChekedClickDown) {
                        _this.VclStackPanelContentMemTable.Row.This.style.display = "";
                    } else {
                        _this.VclStackPanelContentMemTable.Row.This.style.display = "none";
                    }
                    break;
                case Componet.GridCF.TToolbarControlls.ButtonImg_StatusBar:
                    _this.VclGridCF.EnablePanelStatus = Obj.ButtonImg_StatusBar_ChekedClickDown;
                    break
                case Componet.GridCF.TToolbarControlls.ButtonImg_GridVisibleColumnBar:
                    _this.NavGroupMenu.Text = Obj.ButtonImg_GridVisibleColumnBar.Text;
                    _this.NavGroupMenu.SrcImg = Obj.ButtonImg_GridVisibleColumnBar.SrcImg;
                    _this.NavGroupMenu.NewRow.LabelTextGroup.This.style.fontWeight = "bold";
                    _this.PanelMenuLeft.Column[0].This.style.display = "";
                    _this.PanelMenuLeft.Column[1].This.classList.remove("col-md-12");
                    _this.PanelMenuLeft.Column[1].This.classList.remove("col-lg-12");
                    _this.PanelMenuLeft.Column[1].This.classList.remove("col-xl-12");
                    _this.PanelMenuLeft.Column[1].This.classList.add("col-md-9");
                    _this.PanelMenuLeft.Column[1].This.classList.add("col-lg-9");
                    _this.PanelMenuLeft.Column[1].This.classList.add("col-xl-9");
                    _this.PanelMenuLeft.Column[1].This.style.paddingLeft = "5px";
                    _this.VclListBoxColumns.ClearAll();
                    $(_this.NavGroupMenuContenStackPanel).html("");
                    var divContent = document.createElement("div");
                    divContent.style.width = "0px";
                    divContent.classList.add("efectMenuLeftExtend");
                    divContent.style.width = "100%";
                    _this.NavGroupMenuContenStackPanel.appendChild(divContent);
                    var tempEnabledCheckBox = _this.VclListBoxColumns.EnabledCheckBox;
                    var tempOnCheckedListBoxItemChange = _this.VclListBoxColumns.OnCheckedListBoxItemChange;
                    _this.VclListBoxColumns = new TVclListBox(divContent, "");
                    _this.VclListBoxColumns.This.Row.This.style.marginLeft = "0px";
                    _this.VclListBoxColumns.This.Row.This.style.marginRight = "0px";
                    _this.VclListBoxColumns.This.Row.This.style.bordeTop = "1px solid #ddd";
                    _this.VclListBoxColumns.This.Row.This.style.borderBottom = "1px solid #ddd";
                    _this.VclListBoxColumns.This.Row.This.style.borderRight = "1px solid #ddd";
                    _this.VclListBoxColumns.This.Row.This.style.borderLeft = "1px solid #ddd";
                    _this.VclListBoxColumns.EnabledCheckBox = tempEnabledCheckBox;
                    _this.VclListBoxColumns.OnCheckedListBoxItemChange = tempOnCheckedListBoxItemChange;
                    var visible = false;
                    for (var i = 1; i < _this.VclGridCF.Columns.length - 1; i++) {
                        var ListBoxItem1 = new TVclListBoxItem();
                        ListBoxItem1.Label.This.style.overflow = "hidden";
                        ListBoxItem1.Label.This.style.textOverflow = "ellipsis";
                        ListBoxItem1.Label.This.style.whiteSpace = "nowrap";
                        if (!_this.VclGridCF.Columns[i]._Visible_IsShowValue) {
                            ListBoxItem1.This.Row.This.style.display = "none";
                        } else {
                            visible = _this.VclGridCF.Columns[i]._Visible;
                        }
                        ListBoxItem1.This.Row.This.style.display
                        ListBoxItem1.Text = _this.VclGridCF.Columns[i].Caption;
                        ListBoxItem1.Index = i;
                        ListBoxItem1.Checked = _this.VclGridCF.Columns[i].Visible;
                        _this.VclListBoxColumns.AddListBoxItem(ListBoxItem1);
                        ListBoxItem1.CheckBox.This.style.display = "none";
                        var imgEye = document.createElement("i");
                        imgEye.style.fontSize = "17px";
                        imgEye.classList.add("icon");
                        imgEye.classList.add("ion-eye");
                        imgEye.classList.add("text-primary");
                        imgEye.style.marginRight = "4px";
                        imgEye.style.cursor = "pointer";
                        imgEye.ListBoxItem = ListBoxItem1;
                        imgEye.onclick = function () {
                            $(this.ListBoxItem.CheckBox.This).click();
                            if (this.ListBoxItem.Checked) {
                                this.classList.remove("ion-eye-disabled");
                                this.classList.add("ion-eye");
                                this.style.opacity = "1";
                            } else {
                                this.classList.remove("ion-eye");
                                this.classList.add("ion-eye-disabled");
                                this.style.opacity = "0.6";
                            }
                        }
                        if (visible) {
                            imgEye.style.opacity = "1";
                        } else {
                            imgEye.style.opacity = "0.6";
                        }
                        var texto = ListBoxItem1._Text;
                        texto = document.createTextNode("" + texto);         // Create a text node
                        $(ListBoxItem1.Label.This).html("");
                        ListBoxItem1.Label.This.appendChild(imgEye);
                        ListBoxItem1.Label.This.appendChild(texto);
                        imgEye.title = ListBoxItem1._Text;
                        var c = ListBoxItem1.This.Column[0].This.childNodes;
                        c[1].style.display = "none";
                        ListBoxItem1.Label.This.title = ListBoxItem1.Text;

                    }

                    break;
                case Componet.GridCF.TToolbarControlls.ButtonImg_GridDetailsVisibleColumnBar:

                    _this.NavGroupMenu.Text = Obj.ButtonImg_GridDetailsVisibleColumnBar.Text;
                    _this.NavGroupMenu.SrcImg = Obj.ButtonImg_GridDetailsVisibleColumnBar.SrcImg;
                    _this.NavGroupMenu.NewRow.LabelTextGroup.This.style.fontWeight = "bold";
                    _this.PanelMenuLeft.Column[0].This.style.display = "";
                    _this.PanelMenuLeft.Column[1].This.classList.remove("col-md-12");
                    _this.PanelMenuLeft.Column[1].This.classList.remove("col-lg-12");
                    _this.PanelMenuLeft.Column[1].This.classList.remove("col-xl-12");
                    _this.PanelMenuLeft.Column[1].This.classList.add("col-md-9");
                    _this.PanelMenuLeft.Column[1].This.classList.add("col-lg-9");
                    _this.PanelMenuLeft.Column[1].This.classList.add("col-xl-9");
                    _this.PanelMenuLeft.Column[1].This.style.paddingLeft = "5px";
                    _this.VclListBoxInformationColumns.ClearAll();
                    $(_this.NavGroupMenuContenStackPanel).html("");
                    var tempEnabledCheckBox = _this.VclListBoxInformationColumns.EnabledCheckBox;
                    var tempOnCheckedListBoxItemChange = _this.VclListBoxInformationColumns.OnCheckedListBoxItemChange;
                    _this.VclListBoxInformationColumns = new TVclListBox(_this.NavGroupMenuContenStackPanel, "");
                    _this.VclListBoxInformationColumns.EnabledCheckBox = tempEnabledCheckBox;
                    _this.VclListBoxInformationColumns.OnCheckedListBoxItemChange = tempOnCheckedListBoxItemChange;
                    _this.VclListBoxInformationColumns.This.Row.This.style.marginLeft = "0px";
                    _this.VclListBoxInformationColumns.This.Row.This.style.marginRight = "0px";
                    _this.VclListBoxInformationColumns.This.Row.This.style.bordeTop = "1px solid #ddd";
                    _this.VclListBoxInformationColumns.This.Row.This.style.borderBottom = "1px solid #ddd";
                    _this.VclListBoxInformationColumns.This.Row.This.style.borderRight = "1px solid #ddd";
                    _this.VclListBoxInformationColumns.This.Row.This.style.borderLeft = "1px solid #ddd";
                    var visible = true;
                    for (var i = 1; i < _this.VclGridCF.Columns.length - 1; i++) {
                        var ListBoxItem1 = new TVclListBoxItem();
                        ListBoxItem1.Label.This.style.overflow = "hidden";
                        ListBoxItem1.Label.This.style.textOverflow = "ellipsis";
                        ListBoxItem1.Label.This.style.whiteSpace = "nowrap";
                        if (!_this.VclGridCF.Columns[i]._VisibleVertical_IsShowValue) {
                            ListBoxItem1.This.Row.This.style.display = "none";
                        } else {
                            visible = _this.VclGridCF.Columns[i]._VisibleVertical;
                        }
                        ListBoxItem1.Text = _this.VclGridCF.Columns[i].Caption;
                        ListBoxItem1.Index = i;
                        ListBoxItem1.Checked = _this.VclGridCF.Columns[i]._VisibleVertical;
                        _this.VclListBoxInformationColumns.AddListBoxItem(ListBoxItem1);

                        ListBoxItem1.CheckBox.This.style.display = "none";
                        var imgEye = document.createElement("i");
                        imgEye.style.fontSize = "17px";
                        imgEye.classList.add("icon");
                        imgEye.classList.add("ion-eye");
                        imgEye.classList.add("text-primary");
                        imgEye.style.marginRight = "4px";
                        imgEye.style.cursor = "pointer";
                        imgEye.ListBoxItem = ListBoxItem1;
                        imgEye.onclick = function () {
                            $(this.ListBoxItem.CheckBox.This).click();
                            if (this.ListBoxItem.Checked) {
                                this.classList.remove("ion-eye-disabled");
                                this.classList.add("ion-eye");
                                this.style.opacity = "1";
                            } else {
                                this.classList.remove("ion-eye");
                                this.classList.add("ion-eye-disabled");
                                this.style.opacity = "0.6";
                            }
                        }
                        if (visible) {
                            imgEye.style.opacity = "1";
                        } else {
                            imgEye.style.opacity = "0.6";
                        }
                        var texto = ListBoxItem1._Text;
                        texto = document.createTextNode("" + texto);         // Create a text node
                        $(ListBoxItem1.Label.This).html("");
                        ListBoxItem1.Label.This.appendChild(imgEye);
                        ListBoxItem1.Label.This.appendChild(texto);
                        imgEye.title = ListBoxItem1._Text;
                        var c = ListBoxItem1.This.Column[0].This.childNodes;
                        c[1].style.display = "none";
                        ListBoxItem1.Label.This.title = ListBoxItem1.Text;

                    }

                    break;
                case Componet.GridCF.TToolbarControlls.ButtonImg_ExpanderBar:

                    if (_this.VclTreeGridControl != null && _this.VclTreeGridControl.inTreeGrid) {
                        _this.VclTreeGridControl.expander(true);
                    }
                    break;
                case Componet.GridCF.TToolbarControlls.ButtonImg_ContraerBar:

                    if (_this.VclTreeGridControl != null && _this.VclTreeGridControl.inTreeGrid) {
                        _this.VclTreeGridControl.expander(false);
                    }
                    break;

                case Componet.GridCF.TToolbarControlls.ButtonImg_PriorFirstBar:

                    if (_this.DataSet != null) {
                        _this.First();
                    }
                    break;
                case Componet.GridCF.TToolbarControlls.ButtonImg_PriorBar:
                    if (_this.DataSet != null) {
                        _this.Prior();
                    }
                    break;
                case Componet.GridCF.TToolbarControlls.ButtonImg_DeleteBar:
                    if (_this.VclTreeGridControl != null && _this.VclTreeGridControl.inTreeGrid) {
                        _this.VclTreeGridControl.buttonEventDeleteTreeGrid();
                    } else {
                        if (_this.DataSet != null) {
                            _this.DataSet.Delete();
                        }
                    }
                    break;
                case Componet.GridCF.TToolbarControlls.ButtonImg_AddBar:
                    _this.OnActiveChangeControls = false;
                    if (_this.VclTreeGridControl != null && _this.VclTreeGridControl.inTreeGrid) {
                        _this.VclTreeGridControl.buttonEventInsertTreeGrid();
                    } else {
                        _this.EventInsert(_this.VclGridCF.FocusedRowHandle);
                    }
                    break;
                case Componet.GridCF.TToolbarControlls.ButtonImg_EditBar:
                    _this.OnActiveChangeControls = false;
                    if (_this.VclTreeGridControl != null && _this.VclTreeGridControl.inTreeGrid) {
                        _this.VclTreeGridControl.buttonEventUpdateTreeGrid();
                    } else {
                        _this.EventUpdate(_this.VclGridCF.FocusedRowHandle);
                    }

                    break;
                case Componet.GridCF.TToolbarControlls.ButtonImg_PostBar:
                    if (_this.VclTreeGridControl != null && _this.VclTreeGridControl.inTreeGrid) {
                        _this.VclTreeGridControl.buttonEventPostTreeGrid();
                    } else {
                        _this.EventPost(_this.VclGridCF.FocusedRowHandle);
                    }
                    break;
                case Componet.GridCF.TToolbarControlls.ButtonImg_CancelBar:
                    if (_this.VclTreeGridControl != null && _this.VclTreeGridControl.inTreeGrid) {
                        _this.VclTreeGridControl.buttonEventCancelTreeGrid();
                    } else {
                        _this.EventCancel(_this.VclGridCF.FocusedRowHandle);
                    }
                    break;
                case Componet.GridCF.TToolbarControlls.ButtonImg_NextBar:

                    if (_this.DataSet != null) {
                        _this.Next();
                    }

                    break;
                case Componet.GridCF.TToolbarControlls.ButtonImg_NextLastBar:

                    if (_this.DataSet != null) {
                        _this.Last();
                    }
                    break;


                case Componet.GridCF.TToolbarControlls.ButtonImg_ContraerColumnsBar:
                    if (_this.VclTreeGridControl != null && _this.VclTreeGridControl.inTreeGrid) {
                        _this.VclTreeGridControl.ContracAllColumns();
                    } else {
                        _this.VclGridCF.ContracAllColumns();
                    }
                    break;
                case Componet.GridCF.TToolbarControlls.ButtonImg_ExpanderColumnsBar:

                    if (_this.VclTreeGridControl != null && _this.VclTreeGridControl.inTreeGrid) {
                        _this.VclTreeGridControl.ExpandAllColumns();
                    } else {
                        if (Obj.ButtonImg_ExpanderColumnsBar.Cheked) {
                            Obj.ButtonImg_ExpanderColumnsBar.SrcImg = "image/24/arrows2-edit.png";
                            _this.VclGridCF.ExpandAllColumns();
                        } else {
                            Obj.ButtonImg_ExpanderColumnsBar.SrcImg = "image/24/arrows2.png";
                            _this.VclGridCF.ContracAllColumns();
                        }

                    }

                    break;

                default:
                    alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines5"));
                    break;
            }
        }
    }
    /*Función para importar un sytle*/
    this.importarStyle = function (nombre, onSuccess, onError) {
        var style = document.createElement("link");
        style.rel = "stylesheet";
        style.type = "text/css";
        style.href = nombre;
        var s = document.head.appendChild(style);
        s.onload = onSuccess;
        s.onerror = onError;
    }
    /*Función para importar un script*/
    this.importarScript = function (nombre, onSuccess, onError) {
        var s = document.createElement("script");
        s.onload = onSuccess;
        s.onerror = onError;
        s.src = nombre;
        document.querySelector("head").appendChild(s);
    }
    _this.Init();
    _this.importarScript(SysCfg.App.Properties.xRaiz + "Componet/Color/Plugin/spectrum.js", function () {
        _this.importarStyle(SysCfg.App.Properties.xRaiz + "Componet/Color/Plugin/spectrum.css", function () {
            ObjColorHead = new Componet.TPalleteColor(_this.GridCFToolbar.ButtonImg_FontColor.Panel, "newColor1");
            ObjColorHead.CreatePallete();
            var ObjColorBody = new Componet.TPalleteColor(_this.GridCFToolbar.ButtonImg_FontBody.Panel, "newColor1");
            ObjColorBody.CreatePallete();
            ObjColorHead.ChangeColor = function (objColor) {
                _this.VclGridCF.Header.style.color = objColor.Color;
                if (_this.VclTreeGridControl != null && _this.VclTreeGridControl.inTreeGrid) {
                    _this.VclTreeGridControl.GridTreeGridControl.Header.style.color = objColor.Color;
                }
                Componet.GridCF.Properties.VConfig.HeaderColor = objColor.Color;
            }
            ObjColorBody.ChangeColor = function (objColor) {
                _this.VclGridCF.Body.style.color = objColor.Color;
                Componet.GridCF.Properties.VConfig.BodyColor = objColor.Color;
                if (_this.VclTreeGridControl != null && _this.VclTreeGridControl.inTreeGrid) {
                    _this.VclTreeGridControl.GridTreeGridControl.Body.style.color = objColor.Color;
                }
            }
            ObjColorHead.Color = Componet.GridCF.Properties.VConfig.HeaderColor;
            ObjColorBody.Color = Componet.GridCF.Properties.VConfig.BodyColor;
            _this.importarStyle(SysCfg.App.Properties.xRaiz + "Css/Component/UCode/VCL/GridControlCF/GridToolTip.css", function () {


            }, function (e) { });
        }, function (e) { });
    }, function (e) { });
}
/*Función para iniciar los eventos de DataSet*/
Componet.GridCF.TGridCFView.prototype.InitEventDataSource = function () {
    var _this = this.TParent();
    if (_this.DataSet != null) {
        //************* INICIO TAB MEM TABLE ********************************///
        _this.DataSet.OnRefreschRecordSet = function (Object, EventArgs) {
            var UnDataSet = Object;//DataSet
            var UnRecord = UnDataSet.RecordSet;//EventArgs-RecordSet;
            var isnotNull = (UnDataSet != null) && (UnRecord != null) && (EventArgs != null);//Validamos que DataSet es distinto de nulo.
            if (isnotNull) {//Validamos que DataSet es distinto de nulo.
                if (!_this.VclTreeGridControl.inTreeGrid) {//Validamos que estamos en el grid principal.
                    if (UnDataSet.Status == SysCfg.MemTable.Properties.TStatus.None) {//Validamos que estamos en estado None
                        if (_this.IndiceGrillaEventDataSourcePostType != null) {//Validamos que exista una acción que se producirá en el OnRefreschRecordSet.
                            if (_this.IndiceGrillaEventDataSourcePostType == 0) {//Validamos que exista 
                                _this.IndiceGrillaEventDataSourcePostType = null;//Seteamos el valor a null.
                                if (_this.VclGridCF.FocusedRowHandle.IndexDataSet != _this.DataSet.Index) {//Validamos que el índice del DataSet sea distinto al índice dataset de la fila.
                                    _this.DataSet.SetIndex(_this.VclGridCF.FocusedRowHandle.IndexDataSet);//Establecemos índice.
                                } else {
                                    _this.HabilitarCamposFormGrid();//Actualizamos el detalle del Grid.
                                }
                            }
                            if (_this.IndiceGrillaEventDataSourcePostType == 1) {//Acción producida cuando se elimino el último registro y se cambia de paginación.
                                if (_this.IsPagination) {//Validamos si estamos en una paginación.
                                    _this.isEventChangePaginationScroll = false;//acción que se produce en el OnEventGoTo de TPagination.
                                    _this.IndiceGrillaEventDataSourcePostType = null;//Seteamos el valor a null.
                                    _this.VPagination.GoTo(_this.VPagination.IndexPage - 1);//Cambiamos a página anterior.
                                    _this.HabilitarCamposFormGrid();//Actualizamos el detalle del Grid.
                                } else {
                                    _this.isEventChangePaginationScroll = false;//acción que se produce en el OnEventGoTo de TPagination.
                                    var scrollTop = _this.heightFilas * ((_this.ultimoIndice) * _this.filasMostradas) + _this.heightEncabezados;//Altura donde se posiciona el scroll.
                                    var indiceActual = parseInt(scrollTop / (_this.heightFilas * _this.filasMostradas));//Calculamos le nuevo índice del Scroll.
                                    if (_this.ultimoIndice != indiceActual && indiceActual <= _this.numeroMaximoIndices) {//Validamos que el nuevo índice en el scroll este en un rango válido.
                                        _this.isEventChangePaginationScroll = false;//acción que se produce en el OnEventGoTo de Scroll.
                                        _this.div1.scrollTop = scrollTop;//Altura donde se posiciona scroll
                                    }
                                    _this.HabilitarCamposFormGrid();//Actualizamos el detalle del Grid.
                                }
                            }
                            if (_this.IndiceGrillaEventDataSourcePostType == 2) {
                                if (_this.IsPagination) {//Validamos si estamos en una paginación.
                                    _this.isEventChangePaginationScroll = true;//acción que se produce en el OnEventGoTo de TPagination.
                                    _this.IndiceGrillaEventDataSourcePostType = null;//Seteamos el valor a null.
                                    _this.VPagination.GoTo(_this.VPagination.IndexPage + 1);//Cambiamos a página anterior.
                                    _this.HabilitarCamposFormGrid();//Actualizamos el detalle del Grid.
                                } else {
                                    _this.isEventChangePaginationScroll = false;//acción que se produce en el OnEventGoTo de Scroll.
                                    var scrollTop = _this.heightFilas * ((_this.ultimoIndice + 1) * _this.filasMostradas) + _this.heightEncabezados;//Altura donde se posiciona el scroll.
                                    var indiceActual = parseInt(scrollTop / (_this.heightFilas * _this.filasMostradas));//Calculamos le nuevo índice del Scroll
                                    if (_this.ultimoIndice != indiceActual && indiceActual <= _this.numeroMaximoIndices) {//Validamos que el nuevo índice en el scroll este en un rango válido.
                                        _this.isEventChangePaginationScroll = false;//acción que se produce en el OnEventGoTo de Scroll.
                                        _this.div1.scrollTop = scrollTop;//Altura donde se posiciona scroll
                                    }
                                    _this.HabilitarCamposFormGrid();//Actualizamos el detalle del Grid.
                                }
                            }

                            if (_this.IndiceGrillaEventDataSourcePostType == 3) {//Cuando el registro que se intenta registrar no se encuentra en el filtro.
                                //No se agrega registro, ya que el registro no esta dentro del filtro Genral.
                                _this.IndiceGrillaEventDataSourcePostType = null;//Seteamos el valor a null.
                                if (_this.VclTreeGridControl != null && !_this.VclTreeGridControl.inTreeGrid) {//Validamos si estamos en el Grid
                                    _this.EventCancelCustomFilter(_this.VclGridCF._FocusedRowHandle);//Establece valores cuando el registro que se intenta registrar no se encuentra en el filtro.
                                }
                            }
                        } else {
                            _this.HabilitarCamposFormGrid();//Actualizamos el detalle del Grid.
                        }
                    }
                    if (UnDataSet.Status == SysCfg.MemTable.Properties.TStatus.Apped) {
                        //
                    }
                    if (UnDataSet.Status == SysCfg.MemTable.Properties.TStatus.Update) {
                        _this.MostrarInformacion();//Función para cambiar celdas de la fila a editables.
                        _this.HabilitarCamposFormGrid();//Actualizamos el detalle del Grid.
                    }
                    if (_this.IndiceLogico != null) {
                        //cuando record.length==0, se debe desactivar las flechas, cuando se esta editando o registrando también se debe desactivar las flechas
                        //para saber si estamos en la última fila, en el IndexTreeGrid se guarda la posicion original de su arreglo de datos
                        var recordCount = _this.ItemTreePanel.length;//Número de registros del ItemTreePanel
                        var posicion = recordCount;//posición
                        if (_this.VclGridCF.FocusedRowHandle != null) {
                            posicion = _this.VclGridCF.FocusedRowHandle.IndexTreeGrid;//posición de fila en el ItemTreePanel
                        }
                        var isFirst = ((posicion > 0) && (_this.DataSet.Status == SysCfg.MemTable.Properties.TStatus.None));//Si estamos en primera posición.
                        var isLast = ((posicion < recordCount - 1) && (_this.DataSet.Status == SysCfg.MemTable.Properties.TStatus.None));//Si estamos en última posición.
                        var isNext = ((posicion < recordCount - 1) && (_this.DataSet.Status == SysCfg.MemTable.Properties.TStatus.None));//Si estamos en última posición
                        var isPrior = ((posicion > 0) && (_this.DataSet.Status == SysCfg.MemTable.Properties.TStatus.None));//Si estamos en primera posición.
                        _this.GridCFToolbar.ButtonImg_PriorFirstBar.Enabled = isFirst;//Valor enabled de los bar (Flechas de navegación).
                        _this.GridCFToolbar.ButtonImg_PriorBar.Enabled = isPrior;//Valor enabled de los bar (Flechas de navegación).
                        _this.GridCFToolbar.ButtonImg_NextBar.Enabled = isNext;//Valor enabled de los bar (Flechas de navegación).
                        _this.GridCFToolbar.ButtonImg_NextLastBar.Enabled = isLast;//Valor enabled de los bar (Flechas de navegación).
                        _this.GridCFToolbar.ButtonImg_PriorFirstBarB.Enabled = isFirst;//Valor enabled de los bar (Flechas de navegación).
                        _this.GridCFToolbar.ButtonImg_PriorBarB.Enabled = isPrior;//Valor enabled de los bar (Flechas de navegación).
                        _this.GridCFToolbar.ButtonImg_NextBarB.Enabled = isNext;//Valor enabled de los bar (Flechas de navegación).
                        _this.GridCFToolbar.ButtonImg_NextLastBarB.Enabled = isLast;//Valor enabled de los bar (Flechas de navegación).
                    }
                    _this.Status.Refresch(isFirst, isFirst, UnDataSet.Index, UnDataSet.Status.name);//Actualizamos status
                }
                else {//Si estamos en TreeGrid
                    if (UnDataSet.Status == SysCfg.MemTable.Properties.TStatus.None) {//Si status es None
                        _this.VclTreeGridControl.ItemTreePanel = _this.ItemTreePanel;
                        if (_this.IndiceGrillaEventDataSourcePostType != null) {//Validamos que exista una acción que se producirá en el OnRefreschRecordSet.
                            if (_this.IndiceGrillaEventDataSourcePostType == 0 || _this.IndiceGrillaEventDataSourcePostType == 1) {
                                _this.IndiceGrillaEventDataSourcePostType = null;//Seteamos el valor a null.
                                _this.VclTreeGridControl.VPagination.OnEventGoTo(_this.VclTreeGridControl.VPagination.IndexPage);
                            }
                        } else {
                            _this.VclTreeGridControl.ItemTreePanel = _this.ItemTreePanel;//Actualizamo valor de ItemTreePanel en TreeGrid
                            if (_this.LastStatusEventTreeGridControl != null) {
                                _this.VclTreeGridControl.EventAfterChange(_this.LastStatusEventTreeGridControl.Status, _this.ItemTreePanel.length - 1);//Evento en TreeGrid
                            } else {
                                _this.HabilitarCamposFormGrid();//Actualizamos el detalle del Grid.
                            }
                        }
                    }
                    if (UnDataSet.Status == SysCfg.MemTable.Properties.TStatus.Update) {
                        _this.HabilitarCamposFormGrid();//Actualizamos el detalle del Grid.
                    }
                    if (UnDataSet.Status == SysCfg.MemTable.Properties.TStatus.Apped) {
                        _this.HabilitarCamposFormGrid();//Actualizamos el detalle del Grid.
                    }
                    _this.Status.Refresch(isFirst, isFirst, UnDataSet.Index, UnDataSet.Status.name);//Actualizamos status
                }
                _this.GridCFToolbar.ButtonImg_DeleteBar.Enabled = UnDataSet.isDelete();//Si status es Delete.
                _this.GridCFToolbar.ButtonImg_AddBar.Enabled = UnDataSet.isAppend();//Si status es Append.
                _this.GridCFToolbar.ButtonImg_EditBar.Enabled = UnDataSet.isUpdate();//Si status es Update.
                _this.GridCFToolbar.ButtonImg_PostBar.Enabled = UnDataSet.isPost();//Si status es Post.
                _this.GridCFToolbar.ButtonImg_CancelBar.Enabled = UnDataSet.isCancel();//Si status es Cancel.
            }
        }
        _this.DataSet.OnRefresch = function (Object, EventArgs) {
            _this.loadDataSetColumns(_this.DataSet);//Cargar columnas.
            _this.loadDataSetRecords();//Cargar records.
        };
        _this.DataSet.OnBeforeChange = function (Object, EventArgs) {
            var UnDataSet = Object;
            var UnRecord = UnDataSet.RecordSet;//EventArgs;
            var isnotNull = (UnDataSet != null) && (UnRecord != null);
            if (isnotNull) {
                //***************************** Calculando old values **********************************            
                var RecordOld = null;//Record del DataSet

                if ((UnDataSet.RecordCount != 0)) {
                    RecordOld = UnDataSet.Records[UnDataSet.Index];////Record del DataSet
                }
                //****************************** Calculando New Nalues *********************************            
                var RecordNew = UnDataSet.RecordSet;//RecordSet del DataSet
                //************************************************************************************** 
                var isCancel = new SysCfg.ref(false);
                //llamamos a la BeforeChange que se implementara por fuera del componente, aqui es donde se crean los mensajes, por ejemplo "Desea continuar con la operación"
                _this.BeforeChange(UnDataSet, RecordOld, RecordNew, UnDataSet.Status, isCancel);
                if (isCancel.Value) {//validamos si cancelamos operación.
                    if (_this.VclTreeGridControl != null && _this.VclTreeGridControl.inTreeGrid) {//Validamos si estamos en TreeGrid
                        _this.VclTreeGridControl.buttonEventCancelTreeGrid();//cancelamos operación en el TreeGrid
                    }
                    if (_this.VclTreeGridControl != null && !_this.VclTreeGridControl.inTreeGrid) {//Validamos si estamos en el Grid
                        _this.EventCancel(_this.VclGridCF._FocusedRowHandle);//cancelamos operación en el grid
                    }
                    //_this.DataSet.Cancel();
                }
            }
        }
        _this.DataSet.OnAfterChange = function (Object, EventArgs) {
            var UnDataSet = Object;//DataSet
            var UnRecord = UnDataSet.RecordSet;//RecordSet
            var isnotNull = (UnDataSet != null) && (UnRecord != null);//Validamos que DataSet sea distinto de nulo.
            if (isnotNull) {
                var RecordOld = null;//Record actual del DataSet
                if ((UnDataSet.RecordCount != 0) && (!UnDataSet.Eof) && (!UnDataSet.Bof)) {
                    RecordOld = UnDataSet.Records[UnDataSet.Index];//Record actual del DataSet.
                }
                var RecordNew = UnDataSet.RecordSet;//RecordSet del DataSet.
                try {
                    _this.AfterChange(RecordOld, RecordNew);//Enviamos la función por fuera del componente.
                }
                catch (err) {
                    //Si ocurre un error cancelamos la operación en el grid o treeGrid.
                    SysCfg.Log.Methods.WriteLog("GridControlCFView.js Componet.GridCF.TGridCFView.prototype.InitEventDataSource", err);
                    alert(err.message);
                    if (_this.VclTreeGridControl != null && _this.VclTreeGridControl.inTreeGrid) {
                        if (UnDataSet.Status == SysCfg.MemTable.Properties.TStatus.Apped) {
                            var indiceRowGrilla = _this.VclTreeGridControl.GridTreeGridControl._FocusedRowHandle.Index;
                            _this.VclTreeGridControl.GridTreeGridControl.RemoveRow(indiceRowGrilla);
                            _this.VclTreeGridControl.GridTreeGridControl.IndexRow = indiceRowGrilla;
                        }
                        _this.IndiceGrillaEventDataSourcePostType = 1;//Acción que se producirá en el OnRefreschRecordSet.
                    } else {
                        _this.EventCancel(_this.VclGridCF._FocusedRowHandle);
                    }
                    return true;
                }
                /*
                Exiten 2 casos al realizar el Insert en la Grilla:         
                - Cuando la Grilla no esta agrupada  =>_this.IndiceLogico!=null
                - Cuando la Grilla esta agrupada   =>_this.VclTreeGridControl.inTreeGrid==true
                Exiten 3 casos al realizar el Insert en el MemTable caso:
                - Cuando el record.length == 0
                - Cuando el DataSet.Index sea la última posición
                - Cuando el DataSet.Index no esta en cualquiera de los anteriores casos.
                Si Grilla esta ordenada o agrupada, entonces insertar el Record en la última posición de DataSet.
                Se deben actulizar los index del los Row del Gid
                */
                if (_this.VclTreeGridControl != null && !_this.VclTreeGridControl.inTreeGrid) {
                    //Validamos que estamos en el Grid principal
                    if (UnDataSet.Status == SysCfg.MemTable.Properties.TStatus.Apped) {
                        var isInsideQuery = true;//variable que indica si registro esta dentro de en un filtro.
                        var isLastRecordInApped = false;
                        if (_this.IndiceLogico != null) {
                            /*
                            - Insertamos los record en la última posición del DataSet
                            - Insertamos las filas en posicion del focus RowHandler
                            */
                            UnDataSet._Index = UnDataSet.Records.length - 1;//Posición igual a última fila del DataSet
                            if (_this.filterSQLDefault != null) {//Validamos si existe un filtro general.
                                var ArrayGrid = new Array();//Creamos un array para pasarlo por linq
                                var subItemsArrayGrid = [];
                                for (var ContX = 1; ContX < _this.VclGridCF.Columns.length - 1; ContX++) {
                                    var IndexDataSourceDefault = _this.VclGridCF.Columns[ContX].IndexDataSourceDefault;
                                    subItemsArrayGrid.push([(RecordNew.Fields[IndexDataSourceDefault].Value), 0]);
                                }
                                ArrayGrid.push(subItemsArrayGrid);
                                var element = Enumerable.From(ArrayGrid).Where(_this.filterSQLDefault).Select("$").ToArray();//validamos si registro esta dentro del filtro
                                isInsideQuery = (!(element[0] == [] || element[0] == null));//Esta registro en el filtro.
                            }
                            if (!isInsideQuery) {//Validamos que el nuevo registro este dentro del filtro para poder insertarlo.
                                //No se agrega registro, ya que el registro no esta dentro del filtro.
                                _this.IndiceGrillaEventDataSourcePostType = 3;//Acción que se producirá en el OnRefreschRecordSet.
                            }
                            else {
                                var indiceRowGrilla = null;
                                var indiceRowDataSet = UnDataSet.Records.length;
                                var indiceTreeGrid = null;
                                var subItems = [];
                                if (_this.ItemTreePanel.length == 0) {// Cuando el record.length == 0
                                    indiceRowGrilla = 0;
                                    var indiceTreeGrid = 0;//Índice=0
                                    _this.VclGridCF.RemoveRow(indiceRowGrilla);//Eliminamos fila
                                    var NewRowRecordGrid = new TVclRowCF();//Creamos Row
                                    for (var ContX = 0; ContX < _this.VclGridCF.Columns.length - 1; ContX++) {//Recorremos columnas del grid.
                                        if (ContX == 0) {//Validamos si es primer el recorrido
                                            var Cell0 = new TVclCellCF();//Creamos Cell
                                            Cell0.Value = true;//Value del check
                                            Cell0.IndexColumn = ContX;//Índice de la columna.
                                            Cell0.IndexRow = indiceRowGrilla;//Índice de la fila.
                                            NewRowRecordGrid.AddCell(Cell0);//Agregamos fila.
                                        } else {
                                            var Cell0 = new TVclCellCF();//Creamos Cell.
                                            Cell0.Value = RecordNew.Fields[_this.VclGridCF.Columns[ContX].IndexDataSourceDefault].Value;//Obtenemos valor del Record.
                                            Cell0.IndexColumn = ContX;//Índice de columna.
                                            Cell0.IndexRow = indiceRowGrilla;//Índice de la fila.
                                            NewRowRecordGrid.AddCell(Cell0);//Agregamos celda.
                                            subItems.push(
                                                [("" + RecordNew.Fields[_this.VclGridCF.Columns[ContX].IndexDataSourceDefault].Value), UnDataSet.Index]);//Valor, Orden
                                        }
                                    }
                                    var CellButtonNewRow = new TVclCellCF();//Creamos Cell para botones
                                    CellButtonNewRow.Value = "";//Value
                                    CellButtonNewRow.IndexColumn = UnDataSet.FieldCount;//Índice de columna.
                                    CellButtonNewRow.IndexRow = indiceRowGrilla;//Índice de fila
                                    //CellButtonNewRow.Control = new TVclStackPanel(CellButtonNewRow.This, "1", 1, [[12]]);
                                    _this.DibujarCellButton(true, NewRowRecordGrid, CellButtonNewRow);//Dibujamos los botones en el Cell.
                                    NewRowRecordGrid.onclick = function (NewRowRecordGrid) { // ASIGNA UN EVENTO CUANDO SE HACE CLICK EN EL ROW DEL GRIDCONTROL
                                        _this.RowClick(NewRowRecordGrid);//Clic en fila.
                                    }
                                    NewRowRecordGrid.ondblclick = function (NewRow) {//Función doble clic.
                                        _this.RowDblClick(NewRow);//Doble clic en fila.
                                    }
                                    _this.VclGridCF.AddRow(NewRowRecordGrid);//Agregamos fila.
                                    NewRowRecordGrid.IndexDataSet = indiceRowDataSet;//Índice del DataSet
                                    subItems.push("");
                                    subItems.push(indiceRowDataSet);
                                    _this.ItemTreePanel.push(subItems);//Agregamos item.
                                }
                                else {
                                    indiceRowGrilla = _this.VclGridCF._FocusedRowHandle.Index;//Índice de fila actual.
                                    var indiceTreeGrid = _this.VclGridCF._FocusedRowHandle.IndexTreeGrid;//Índice de fila actual el ItemTreePanel.
                                    _this.VclGridCF.RemoveRow(indiceRowGrilla);//Eliminamos fila.
                                    var NewRowRecordGrid = new TVclRowCF();//Creamos Fila
                                    for (var ContX = 0; ContX < _this.VclGridCF.Columns.length - 1; ContX++) {//Recorremos columnas.
                                        if (ContX == 0) {//Primera celda
                                            var Cell0 = new TVclCellCF();//Creamos celda.
                                            Cell0.Value = true;//Valor true.
                                            Cell0.IndexColumn = ContX;//Índice de columna.
                                            Cell0.IndexRow = indiceRowGrilla;//Índice de fila
                                            NewRowRecordGrid.AddCell(Cell0);//Agregamos celda.
                                        } else {
                                            var Cell0 = new TVclCellCF();//Creamos celda.
                                            Cell0.Value = RecordNew.Fields[_this.VclGridCF.Columns[ContX].IndexDataSourceDefault].Value;//Obtenemos valor del Record.
                                            Cell0.IndexColumn = ContX;//Índice de columna.
                                            Cell0.IndexRow = indiceRowGrilla;//Índice de fila.
                                            NewRowRecordGrid.AddCell(Cell0);//Agregamos celda.
                                            subItems.push(
                                                [("" + RecordNew.Fields[_this.VclGridCF.Columns[ContX].IndexDataSourceDefault].Value), UnDataSet.Index]);//Valor, Orden

                                        }
                                    }
                                    var CellButtonNewRow = new TVclCellCF();//Creamos Celda para los botones.
                                    CellButtonNewRow.Value = "";
                                    CellButtonNewRow.IndexColumn = UnDataSet.FieldCount;//Índice de columna.
                                    CellButtonNewRow.IndexRow = indiceRowGrilla;//Índice de fila.
                                    //CellButtonNewRow.Control = new TVclStackPanel(CellButtonNewRow.This, "1", 1, [[12]]);
                                    _this.DibujarCellButton(true, NewRowRecordGrid, CellButtonNewRow);//Dibujamos los botones en la celda.
                                    NewRowRecordGrid.onclick = function (NewRowRecordGrid) { // ASIGNA UN EVENTO CUANDO SE HACE CLICK EN EL ROW DEL GRIDCONTROL
                                        _this.RowClick(NewRowRecordGrid);//Clic en la fila.
                                    }
                                    NewRowRecordGrid.ondblclick = function (NewRow) {
                                        _this.RowDblClick(NewRow);//Doble clic.
                                    }
                                 
                                    _this.VclGridCF.AddRowBefore(true, NewRowRecordGrid, _this.VclGridCF.Rows[indiceRowGrilla]);//Agregar fila en la posición actual.
                                    NewRowRecordGrid.IndexDataSet = indiceRowDataSet;//Índice del DataSet.
                                    subItems.push("");
                                    subItems.push(indiceRowDataSet);
                                    _this.ItemTreePanel.splice(indiceTreeGrid, 0, subItems);//Agregamos item en la posición actual.
                                }
                                /*Actulizamos los IndexDataSet de la grilla y el IndexTreeGrid*/
                                for (var i = indiceRowGrilla, indiceTreeGridContador = indiceTreeGrid, t = _this.VclGridCF.Rows.length; i < t; i++ , indiceTreeGridContador++) {
                                    _this.VclGridCF.Rows[i].IndexTreeGrid = indiceTreeGridContador;//Índice del ItemTreePanel.
                                }
                            }
                        }
                        if (isInsideQuery) {//Validamos que el nuevo registro este dentro del filtro para poder insertarlo.
                            if (_this.IsPagination) {//Validamos si estamos en paginación
                                _this.VPagination.NumeroFilas = (_this.IndiceLogico == null) ? (_this.DataSet.Records.length + 1) : _this.ItemTreePanel.length;//Número de filas.
                                _this.VPagination.Refresch();//Actualizamos paginación.
                                if (_this.VclGridCF.Rows.length > _this.VPagination.FilasMostradas) {//Filas actuales del Grid es mayor al número de filas mostradas en la paginación.
                                    _this.VclGridCF.RemoveRow(_this.VclGridCF.Rows.length - 1);//Removemos la última fila del Grid
                                }
                            }
                            else {//Para scroll
                                ++_this.numeroFilas;//Aumentamos el número de filas.
                                _this.heightFilas = _this.heightFilas;//Alto de filas.
                                _this.heightEncabezados = _this.heightEncabezados;//Alto de encabezados.
                                _this.subiendo = _this.subiendo;//Estado del scroll (Subiendo)
                                _this.numeroMaximoIndices = parseInt(_this.numeroFilas / _this.filasMostradas) - 1;//Calculamos el número máximo de índices.
                                _this.heightTotal = ((_this.heightFilas * _this.numeroFilas) + _this.heightEncabezados);//Alto de la página
                                _this.heightTotalPx = ((_this.heightFilas * _this.numeroFilas) + _this.heightEncabezados) + "px";//Alto de encabezados.
                                if (_this.numeroFilas > _this.filasMostradas && _this.numeroFilas % _this.filasMostradas > 0) {//
                                    var tempFilas = parseInt(_this.numeroFilas / _this.filasMostradas) + 1;
                                    _this.heightTotal = ((tempFilas * _this.filasMostradas * _this.heightFilas) + _this.heightEncabezados);//Calculamos nuevo Height.
                                    _this.heightTotalPx = ((tempFilas * _this.filasMostradas * _this.heightFilas) + _this.heightEncabezados) + "px";//Calculamos nuevo Height.
                                    _this.numeroMaximoIndices = parseInt(_this.numeroFilas / _this.filasMostradas);//Calculamos nuevo número máximo.
                                }
                                _this.div1.style.height = _this.heightInicialPx;//Actualizamos Height
                                _this.div2.style.height = _this.heightTotalPx;//Actualizamos Height
                                if (_this.VclGridCF.Rows.length > _this.filasMostradas) {//Filas actuales del Grid es mayor al número de filas mostradas en la paginación.
                                    _this.VclGridCF.RemoveRow(_this.VclGridCF.Rows.length - 1);//Removemos última fila del Grid.
                                }
                            }
                        }
                    }
                    else if (UnDataSet.Status == SysCfg.MemTable.Properties.TStatus.Update) {
                        if (_this.IndiceLogico != null) {
                            var indiceDataSet = UnDataSet.Index;//índice DataSet
                            var indiceRowGrilla = _this.VclGridCF.FocusedRowHandle.Index;//ndice de fila actual en el grid.
                            var indiceRowDataSet = UnDataSet.Index;//índice DataSet
                            var FocusedRowHandle = _this.VclGridCF.FocusedRowHandle;//Fila focus
                            var indiceTreeGrid = _this.VclGridCF._FocusedRowHandle.IndexTreeGrid;//Índice de la fila en el ItemTreePanel.
                            var NewRowA = new TVclRowCF();//Creamos fila.
                            var CellCheck = new TVclCellCF();//Creamos celda para el check.
                            CellCheck.Value = true;//Valor marcado.
                            CellCheck.IndexColumn = 0;//Índice de columna.
                            CellCheck.IndexRow = indiceRowGrilla;//Índice de fila
                            NewRowA.AddCell(CellCheck);//Agregamos celda
                            for (var ContX = 1; ContX < _this.VclGridCF.Columns.length - 1; ContX++) {
                                _this.VclGridCF.Columns[ContX].EnabledEditor = false;//Celda no editable
                                var Cell0 = new TVclCellCF();//Creamos celda.
                                Cell0.IndexColumn = ContX;//Índice de columna.
                                Cell0.IndexRow = indiceRowGrilla;//Índice de fila.
                                Cell0.Value = RecordNew.Fields[_this.VclGridCF.Columns[ContX].IndexDataSourceDefault].Value;//Valor de Record.
                                _this.ItemTreePanel[indiceTreeGrid][ContX - 1][0] = ("" + RecordNew.Fields[_this.VclGridCF.Columns[ContX].IndexDataSourceDefault].Value);//Agregamos valor de record a TreePanel
                                NewRowA.AddCell(Cell0);//Agregamos celda
                            }
                            var CellButton = new TVclCellCF();//Creamos celda para los botones.
                            CellButton.Value = "";//valor de celda
                            CellButton.IndexColumn = _this.DataSet.FieldCount;//Índice de columna.
                            CellButton.IndexRow = indiceRowGrilla;//Índice de grid.
                            //CellButton.Control = new TVclStackPanel(CellButton.This, "1", 1, [[12]]);
                            _this.DibujarCellButton(true, NewRowA, CellButton);//Dibujamos botones en celda.
                            NewRowA.onclick = function (NewRow) {
                                _this.RowClick(NewRow);//clic en fila.
                            }
                            NewRowA.ondblclick = function (NewRow) {
                                _this.RowDblClick(NewRow);//doble clic.
                            }
                            _this.VclGridCF.UpdatesRow(NewRowA, _this.VclGridCF._Rows[indiceRowGrilla]);//Reemplazamos nueva fila por la antigua.
                            NewRowA.Index = indiceRowGrilla;//Índice de fila
                            NewRowA.IndexDataSet = indiceRowDataSet;//Índice de DataSet
                            NewRowA.IndexTreeGrid = indiceTreeGrid;//Índice del fila en ItemTreePanel
                            _this.VclGridCF.IndexRow = indiceRowGrilla;//Índece de fila focus en el Grid.
                        }
                    }
                    else if (UnDataSet.Status == SysCfg.MemTable.Properties.TStatus.Delete) {
                        var indiceDataSet = UnDataSet.Index;//índice DataSet
                        var indiceRowGrilla = _this.VclGridCF.FocusedRowHandle.Index;//índice fila focus.
                        var indiceRowDataSet = UnDataSet.Index;//índice DataSet
                        var indiceTreeGrid = _this.VclGridCF._FocusedRowHandle.IndexTreeGrid;//índice de fila en TreePanel.
                        _this.ItemTreePanel.splice(indiceTreeGrid, 1);//Eliminamos item del ItemTreePanel.
                        _this.VclGridCF.RemoveRow(indiceRowGrilla);//Removemos fila.
                        if (_this.VclGridCF.Rows.length > 0) {
                            if (indiceRowGrilla >= _this.VclGridCF.Rows.length) {
                                //Si se encuentra en la última posicion
                                indiceRowGrilla = indiceRowGrilla - 1;
                            } else {
                                //Si el registro eliminado es menor al número de filas
                                indiceRowGrilla = indiceRowGrilla;
                            }
                        }
                        if (_this.IsPagination) {//Validamos si estamos en una paginación.
                            var recordCount = (_this.IndiceLogico == null) ? (_this.DataSet.Records.length - 1) : _this.ItemTreePanel.length;//Número de filas
                            if (_this.VclGridCF.Rows.length == 0 && recordCount == 0) {//Si no existen filas en el grid ni el ItemTreePanel
                                _this.Information.VclGridCFInfo.ClearAll();//Eliminamos el detalle del grid.
                            } else if (_this.VclGridCF.Rows.length == 0 && recordCount > 0) {//Si no existen filas en el grid, pero si en el ItemTreePanel
                                if (_this.IndiceLogico != null) {
                                    var posionIndexNameColumns = _this.nameColumns.length + 1;//Posición de columna en el ItemTreePanel
                                    for (var i = 0; i < _this.ItemTreePanel.length; i++) {//Recorremos el ItemTrePanel y actulizamos los índices de los items.
                                        if (_this.ItemTreePanel[i][posionIndexNameColumns] > indiceDataSet) {//Si índice es mayor
                                            _this.ItemTreePanel[i][posionIndexNameColumns] = _this.ItemTreePanel[i][posionIndexNameColumns] - 1;//Actualizamos índice
                                        }
                                    }
                                }
                                _this.IndiceGrillaEventDataSourcePostType = 1;//Acción que se producirá en el OnRefreschRecordSet.
                            } else if (_this.VPagination.IndexPage != _this.VPagination.GetNumeroMaximoIndices()) {//Si al eliminar fila se cambia de página
                                var indiceRow = _this.VclGridCF.Rows.length;//Índice de fila
                                var indiceTreeGrid = (_this.VPagination.IndexPage * _this.VPagination.FilasMostradas);//último índice de la fila de una paginación que es igual índice de ItemTreePanel.
                                var indiceRecord = indiceTreeGrid;//Índice de ItemTreePanel
                                if (_this.IndiceLogico != null) {
                                    var posionIndexNameColumns = _this.nameColumns.length + 1;//Posición donde esta el índice de la fila.
                                    for (var i = 0; i < _this.ItemTreePanel.length; i++) {//Recorremos el ItemTreePanel
                                        if (_this.ItemTreePanel[i][posionIndexNameColumns] > indiceDataSet) {//Validamos si el índice del ItemTreePanel es mayor al índice de DataSet.
                                            _this.ItemTreePanel[i][posionIndexNameColumns] = _this.ItemTreePanel[i][posionIndexNameColumns] - 1;//Actualizamos índice.
                                        }
                                    }
                                    indiceRecord = _this.ItemTreePanel[indiceTreeGrid - 1][_this.nameColumns.length + 1];
                                    indiceRecord++; //Para las columnas filtradas
                                }
                                var NewRowRecordGrid = _this.BuilVclRow(_this.DataSet.Records[indiceRecord], indiceRow);//Construir fila.
                                _this.VclGridCF.AddRow(NewRowRecordGrid);//Agregar fila.
                                for (var e = ((_this.VPagination.IndexPage - 1) * _this.VPagination.FilasMostradas), indexRow = 0, tDataSet = (_this.VPagination.IndexPage * _this.filasMostradas), cantidadRecord = ((_this.IndiceLogico == null) ? (UnDataSet.Records.length) : _this.ItemTreePanel.length); e < tDataSet && e < cantidadRecord; e++ , indexRow++) {//Recorremos paginación
                                    if (_this.IndiceLogico != null) {
                                        var IndiceReal = _this.ItemTreePanel[e][_this.nameColumns.length + 1];//Obtnemos el índice del DataSet del item.
                                        _this.VclGridCF.Rows[indexRow].Index = indexRow;//Actualizamos índice de fila.
                                        _this.VclGridCF.Rows[indexRow].IndexDataSet = IndiceReal;//Índice del DataSet
                                        _this.VclGridCF.Rows[indexRow].IndexTreeGrid = e;//Índice de la fila en el TreePanel.
                                    }
                                }
                                _this.VclGridCF.IndexRow = indiceRowGrilla;//Índice de fila
                                _this.IndexRowGridPorScroll = indiceRowGrilla;//Índice de fila
                                _this.IndiceGrillaEventDataSourcePostType = 0;//Acción que se producirá en el OnRefreschRecordSet.
                            } else {//Si al eliminar fila no se producen los casos anteriores.
                                if (_this.IndiceLogico != null) {
                                    var posionIndexNameColumns = _this.nameColumns.length + 1;//Índice donde se encuentra el DataSet
                                    for (var i = 0; i < _this.ItemTreePanel.length; i++) {
                                        if (_this.ItemTreePanel[i][posionIndexNameColumns] > indiceDataSet) {//Validamos que índice sea mayor
                                            _this.ItemTreePanel[i][posionIndexNameColumns] = _this.ItemTreePanel[i][posionIndexNameColumns] - 1;//Restamos al índice del item -1.
                                        }
                                    }
                                }
                                for (var e = ((_this.VPagination.IndexPage - 1) * _this.VPagination.FilasMostradas),
                                    indexRow = 0,
                                    cantidadRecord = UnDataSet.Records.length - 1;
                                    e < cantidadRecord;
                                    e++ ,
                                    indexRow++) {//Recorremos paginación
                                    if (_this.IndiceLogico != null) {
                                        var IndiceReal = _this.ItemTreePanel[e][_this.nameColumns.length + 1];//Índice de DataSet
                                        _this.VclGridCF.Rows[indexRow].Index = indexRow;//Índice de fila
                                        _this.VclGridCF.Rows[indexRow].IndexDataSet = IndiceReal;//Índice de data set
                                        _this.VclGridCF.Rows[indexRow].IndexTreeGrid = e;//Índice de fila en el ItemTreePanel.
                                    }
                                }
                                _this.VclGridCF.IndexRow = indiceRowGrilla;//Índice de fila
                                _this.IndexRowGridPorScroll = indiceRowGrilla;//Índice de fila
                                _this.IndiceGrillaEventDataSourcePostType = 0;//Acción que se producirá en el OnRefreschRecordSet.
                            }
                            _this.VPagination.NumeroFilas = recordCount;//Número de filas.
                            _this.VPagination.Refresch();//Actualizamos paginación.
                        }
                        else {
                            var recordCount = (_this.IndiceLogico == null) ? (_this.DataSet.Records.length - 1) : _this.ItemTreePanel.length;//Número de registros
                            if (_this.VclGridCF.Rows.length == 0 && recordCount == 0) {
                                _this.Information.VclGridCFInfo.ClearAll();//Limpiamos el detalle
                            } else if (_this.VclGridCF.Rows.length == 0 && recordCount > 0) {
                                if (_this.IndiceLogico != null) {
                                    var posionIndexNameColumns = _this.nameColumns.length + 1;
                                    //Actualizamos índices del ItemTreePanel
                                    for (var i = 0; i < _this.ItemTreePanel.length; i++) {
                                        if (_this.ItemTreePanel[i][posionIndexNameColumns] > indiceDataSet) {
                                            _this.ItemTreePanel[i][posionIndexNameColumns] = _this.ItemTreePanel[i][posionIndexNameColumns] - 1;
                                        }
                                    }
                                }
                                _this.IndiceGrillaEventDataSourcePostType = 1;//Acción que se producirá en el OnRefreschRecordSet.
                            }
                            else if (_this.ultimoIndice != _this.numeroMaximoIndices && _this.numeroMaximoIndices != -1) {//Si no estamos en la ultima página
                                var indiceRow = _this.VclGridCF.Rows.length;//Índice de fila
                                var indiceTreeGrid = (_this.VPagination.IndexPage * _this.VPagination.FilasMostradas);//Índice de ItemTreePanel
                                var indiceRecord = indiceTreeGrid;//Índice de TreePanel
                                if (_this.IndiceLogico != null) {
                                    //Actualizamos los índices del DataSet en los ItemTreePanel
                                    var posionIndexNameColumns = _this.nameColumns.length + 1;
                                    for (var i = 0; i < _this.ItemTreePanel.length; i++) {
                                        if (_this.ItemTreePanel[i][posionIndexNameColumns] > indiceDataSet) {
                                            _this.ItemTreePanel[i][posionIndexNameColumns] = _this.ItemTreePanel[i][posionIndexNameColumns] - 1;
                                        }
                                    }
                                    indiceRecord = _this.ItemTreePanel[indiceTreeGrid - 1][_this.nameColumns.length + 1];
                                }
                                var NewRowRecordGrid = _this.BuilVclRow(_this.DataSet.Records[indiceRecord], indiceRow);//Construimos Row
                                _this.VclGridCF.AddRow(NewRowRecordGrid);//Agregamos Row
                                //Recorremos paginación y actualizamos sus índices
                                for (var e = ((_this.VPagination.IndexPage - 1) * _this.VPagination.FilasMostradas), indexRow = 0, tDataSet = (_this.VPagination.IndexPage * _this.filasMostradas), cantidadRecord = ((_this.IndiceLogico == null) ? (UnDataSet.Records.length) : _this.ItemTreePanel.length); e < tDataSet && e < cantidadRecord; e++ , indexRow++) {
                                    if (_this.IndiceLogico != null) {
                                        var IndiceReal = _this.ItemTreePanel[e][_this.nameColumns.length + 1];
                                        _this.VclGridCF.Rows[indexRow].Index = indexRow;
                                        _this.VclGridCF.Rows[indexRow].IndexDataSet = IndiceReal;
                                        _this.VclGridCF.Rows[indexRow].IndexTreeGrid = e;
                                    }
                                }
                                _this.VclGridCF.IndexRow = indiceRowGrilla;//Índice de Grid
                                _this.IndexRowGridPorScroll = indiceRowGrilla;//Índice de fila en Grid
                                _this.IndiceGrillaEventDataSourcePostType = 0;//Acción que se producirá en el OnRefreschRecordSet.
                            } else {
                                if (_this.IndiceLogico != null) {
                                    //Actulizamos índices del ItemTreePanel
                                    var posionIndexNameColumns = _this.nameColumns.length + 1;
                                    for (var i = 0; i < _this.ItemTreePanel.length; i++) {
                                        if (_this.ItemTreePanel[i][posionIndexNameColumns] > indiceDataSet) {
                                            _this.ItemTreePanel[i][posionIndexNameColumns] = _this.ItemTreePanel[i][posionIndexNameColumns] - 1;
                                        }
                                    }
                                }
                                for (
                                    var e = (_this.ultimoIndice * _this.filasMostradas),
                                    indexRow = 0,
                                    cantidadRecord = UnDataSet.Records.length - 1;
                                    e < cantidadRecord;
                                    e++ ,
                                    indexRow++) {
                                    //Actualizamos índices en el Grid
                                    if (_this.IndiceLogico != null) {
                                        var IndiceReal = _this.ItemTreePanel[e][_this.nameColumns.length + 1];
                                        _this.VclGridCF.Rows[indexRow].Index = indexRow;
                                        _this.VclGridCF.Rows[indexRow].IndexDataSet = IndiceReal;
                                        _this.VclGridCF.Rows[indexRow].IndexTreeGrid = e;
                                    }
                                }
                                _this.VclGridCF.IndexRow = indiceRowGrilla;//Índice de Fila
                                _this.IndexRowGridPorScroll = indiceRowGrilla;//Índice de Fila
                                _this.IndiceGrillaEventDataSourcePostType = 0;//Acción que se producirá en el OnRefreschRecordSet.
                            }
                            --_this.numeroFilas;//restamos una fila.
                            _this.heightFilas = _this.heightFilas;//actualizamos alto de filas
                            _this.heightEncabezados = _this.heightEncabezados;//Actualizamos encaezados
                            _this.subiendo = _this.subiendo;//Variable subiendo
                            _this.numeroMaximoIndices = parseInt(_this.numeroFilas / _this.filasMostradas) - 1;//Numero maximo
                            _this.heightTotal = ((_this.heightFilas * _this.numeroFilas) + _this.heightEncabezados);//Heiht Filas
                            _this.heightTotalPx = ((_this.heightFilas * _this.numeroFilas) + _this.heightEncabezados) + "px";//Heiht encabezados
                            if (_this.numeroFilas > _this.filasMostradas && _this.numeroFilas % _this.filasMostradas > 0) {
                                //Calculamos height de fila y encabezados
                                var tempFilas = parseInt(_this.numeroFilas / _this.filasMostradas) + 1;
                                _this.heightTotal = ((tempFilas * _this.filasMostradas * _this.heightFilas) + _this.heightEncabezados);
                                _this.heightTotalPx = ((tempFilas * _this.filasMostradas * _this.heightFilas) + _this.heightEncabezados) + "px";
                                _this.numeroMaximoIndices = parseInt(_this.numeroFilas / _this.filasMostradas);
                            }
                            _this.div1.style.height = _this.heightInicialPx;//Nuevo height
                            _this.div2.style.height = _this.heightTotalPx;//Nuevo height
                        }
                        if (_this.VclGridCF.Rows.length == 0) {//Si fila es igual a 0
                            _this.VclGridCF.FocusedRowHandle = null;//Fila Focus igual a null
                        }
                    }
                }
                else{//Validamos que estamos en una agrupación (TreeGrid)
                    var isInsideQuery = true;//Si el nuevo registro se encuentra en el filtro
                    if (UnDataSet.Status == SysCfg.MemTable.Properties.TStatus.Apped) {//Validamos la operación APPED
                        /****inicio*******/
                        var indiceRowGrilla = _this.VclTreeGridControl.GridTreeGridControl._FocusedRowHandle.Index;//Índice de la grilla en el TreeGrid
                        _this.VclTreeGridControl.GridTreeGridControl.RemoveRow(indiceRowGrilla);//Removemos la fila del TreeGrid
                        _this.VclTreeGridControl.GridTreeGridControl.IndexRow = indiceRowGrilla;//Actualizamos el indexRow del TreeGrid
                        UnDataSet._Index = UnDataSet.Records.length - 1;//Colocamos el index del dataset en la última posición
                        var ItemTreePanel = null;//
                        if (_this.filterSQLDefault != null) {//Validamos que exista un filtro 
                            var ArrayGrid = new Array();
                            var subItemsArrayGrid = [];//Creamos un array para pasarlo por linq
                            for (var ContX = 1; ContX < _this.VclGridCF.Columns.length - 1; ContX++) {//Creamos un array para pasarlo por linq
                                var IndexDataSourceDefault = _this.VclGridCF.Columns[ContX].IndexDataSourceDefault;//Creamos un array para pasarlo por linq
                                subItemsArrayGrid.push([(RecordNew.Fields[IndexDataSourceDefault].Value), 0]);//Creamos un array para pasarlo por linq
                            }
                            ArrayGrid.push(subItemsArrayGrid);//Creamos un array para pasarlo por linq
                            var element = Enumerable.From(ArrayGrid).Where(_this.filterSQLDefault).Select("$").ToArray();//validamos si registro esta dentro del filtro
                            isInsideQuery = (!(element[0] == [] || element[0] == null));//Esta registro en el filtro.
                        }
                        if (!isInsideQuery) {//Validamos que el nuevo registro este dentro del filtro para poder insertarlo.
                            //No se agrega registro, ya que el registro no esta dentro del filtro.
                            _this.IndiceGrillaEventDataSourcePostType = 1;//Acción que se producirá en el OnRefreschRecordSet.
                        }
                        else {
                            //Agregamos registro, ya que se encuentra en el filtro.
                            var indiceRowDataSet = UnDataSet.Records.length;//Último índice del dataSet.
                            var indiceTreeGrid = null;
                            var subItems = [];
                            for (var ContX = 1; ContX < _this.VclGridCF.Columns.length - 1; ContX++) {//Insertamos nuevo record al itemtreePanel
                                var IndexDataSourceDefault = _this.VclGridCF.Columns[ContX].IndexDataSourceDefault;
                                subItems.push([(RecordNew.Fields[IndexDataSourceDefault].Value), 0]);
                            }
                            subItems.push("");
                            subItems.push(indiceRowDataSet);
                            _this.ItemTreePanel.push(subItems);//agregamos subitem.
                            var rutaGrupTemp = _this.VclTreeGridControl.buildRutaAgrupacion(subItems);//Construimos la nueva ruta que produce el registro, por ejemplo "país:perú"
                            var tempOpen = _this.VclTreeGridControl.open;//guardamos el valor open del treegrid
                            if (rutaGrupTemp != _this.VclTreeGridControl.LastRutaGroup.RutaGruopDown) {
                                //validamos que la ruta del grupo que esta abierta sea distinta a la nueva ruta,
                                //por ejemplo en el grupo "país:colombia" es distinta a "país:perú", si se da este caso entonces indicamos de debemos cerrar la agrupacion antigua y abrir la nueva
                                _this.VclTreeGridControl.open = true;
                            }
                            _this.VclTreeGridControl.createRowAgrupation(rutaGrupTemp);//creamos la agrupacion en el TreeGrid
                            _this.VclTreeGridControl.open = tempOpen;
                            ItemTreePanel = _this.VclTreeGridControl.ItemGroupPanel[rutaGrupTemp].ItemTreePanel;//actualizamo el valor del panel
                            _this.IndiceGrillaEventDataSourcePostType = 0;//Acción que se producirá en el OnRefreschRecordSet.

                            if (ItemTreePanel.length == 0) {// Cuando el record.length == 0
                                indiceRowGrilla = 0;
                                var indiceTreeGrid = 0;
                                ItemTreePanel.push(subItems);
                            }
                            else {
                                indiceRowGrilla = _this.VclTreeGridControl.GridTreeGridControl._FocusedRowHandle.Index;
                                var indiceTreeGrid = _this.VclTreeGridControl.GridTreeGridControl._FocusedRowHandle.IndexTreeGrid;
                                ItemTreePanel.splice(indiceTreeGrid, 0, subItems);
                            }
                            _this.VclTreeGridControl.removeRowPagination();
                        }
                    }
                    if (UnDataSet.Status == SysCfg.MemTable.Properties.TStatus.Update) {
                        _this.IndiceGrillaEventDataSourcePostType = 1;//Acción que se producirá en el OnRefreschRecordSet.
                    }
                    if (UnDataSet.Status == SysCfg.MemTable.Properties.TStatus.Delete) {
                        //Eliminamos el registro y actualizamos los índices del treePanel
                        var indiceDataSet = UnDataSet.Index;
                        var posionIndexNameColumns = _this.nameColumns.length + 1;
                        for (var i = 0; i < _this.ItemTreePanel.length; i++) {//Buscamos el record en el itemTreePanel
                            if (_this.ItemTreePanel[i][posionIndexNameColumns] == indiceDataSet) {
                                _this.ItemTreePanel.splice(i, 1);//eliminamos el item
                                break;
                            }
                        }
                        var ItemTreePanelAgrupation = _this.VclTreeGridControl.ItemGroupPanel[_this.VclTreeGridControl.LastRutaGroup.RutaGruopDown].ItemTreePanel;//item Treepanel de la agrupacion actual
                        //Eliminamos el item del itemTreePanel de la agrupaciones.
                        for (var i = 0; i < ItemTreePanelAgrupation.length; i++) {
                            if (ItemTreePanelAgrupation[i][posionIndexNameColumns] == indiceDataSet) {
                                ItemTreePanelAgrupation.splice(i, 1);//eliminamos items
                                break;
                            }
                        }
                        //Actualizamos los índices del data set en el ItemTreePanel ya que se a eliminado un elemento del data set
                        for (var i = 0; i < _this.ItemTreePanel.length; i++) {
                            if (_this.ItemTreePanel[i][posionIndexNameColumns] > indiceDataSet) {
                                _this.ItemTreePanel[i][posionIndexNameColumns] = _this.ItemTreePanel[i][posionIndexNameColumns] - 1;
                            }
                        }
                        //Actualizamos los índices del data set en el ItemTreePanelAgrupado ya que se a eliminado un elemento del data set
                        for (var x = 0; x < _this.VclTreeGridControl.ListRouteGroup.length; x++) {//obtenemos las rutas
                            var rutaGrupTemp = _this.VclTreeGridControl.ListRouteGroup[x];//obtnemos ruta
                            var ItemTreePanel = _this.VclTreeGridControl.ItemGroupPanel[rutaGrupTemp].ItemTreePanel;//itemTrepanel de la ruta
                            for (var i = 0; i < ItemTreePanel.length; i++) {//lista de item
                                if (ItemTreePanel[i][posionIndexNameColumns] > indiceDataSet) {//Si índice del item es mayor al índice que se elimno en el DataSet
                                    ItemTreePanel[i][posionIndexNameColumns] = ItemTreePanel[i][posionIndexNameColumns] - 1;//Restamos una posicion al índice del item
                                }
                            }
                            _this.VclTreeGridControl.ItemGroupPanel[rutaGrupTemp].ItemTreePanel = ItemTreePanel;//Actualizamos el ItemTreePanel en la agrupación.
                        }
                        if (_this.VclTreeGridControl.VPagination.GetNumeroMaximoIndices() == _this.VclTreeGridControl.VPagination.IndexPage) {//Validamos que estemos en la última página
                            if (_this.VclTreeGridControl.VPagination.IndexPage == 1) {//Si la página actual es 1
                                _this.VclTreeGridControl.VPagination.NumeroFilas = _this.VclTreeGridControl.ItemGroupPanel[_this.VclTreeGridControl.LastRutaGroup.RutaGruopDown].ItemTreePanel.length;//Actualizamos el número de filas
                                _this.VclTreeGridControl.VPagination.Refresch();//Actualizamos la paginación
                            } else {
                                //Si estamos en una página mayor a 1
                                var IndeceMaximoVpagination = _this.VclTreeGridControl.VPagination.GetNumeroMaximoIndices();//Calculamos y asignamos el nuevo número de páginas.
                                _this.VclTreeGridControl.VPagination.NumeroFilas = _this.VclTreeGridControl.ItemGroupPanel[_this.VclTreeGridControl.LastRutaGroup.RutaGruopDown].ItemTreePanel.length;//Actualizamos el número de paginas
                                if (_this.VclTreeGridControl.VPagination.GetNumeroMaximoIndices() != IndeceMaximoVpagination) {//validamos si el nuevo número  máximo es distinto al nuevo
                                    _this.VclTreeGridControl.VPagination.IndexPage = _this.VclTreeGridControl.VPagination.IndexPage - 1;//retrocedemos una página
                                }
                                _this.VclTreeGridControl.VPagination.Refresch();//Refrescamos la paginación.
                            }
                        }
                        else {
                            //No estamos en la última página.
                            _this.VclTreeGridControl.VPagination.NumeroFilas = _this.VclTreeGridControl.ItemGroupPanel[_this.VclTreeGridControl.LastRutaGroup.RutaGruopDown].ItemTreePanel.length;//Actualizamos el número de páginas
                            _this.VclTreeGridControl.VPagination.Refresch();//Actualizamos la paginación.
                        }
                        _this.IndiceGrillaEventDataSourcePostType = 1;//Acción que se producirá en el OnRefreschRecordSet.
                    }
                }
                
            }
        }
        _this.DataSet.OnChangeIndex = function (Object, EventArgs) {
            var isTreeGrid = _this.VclTreeGridControl.inTreeGrid;//Valor inTreeGrid.
            var Row = (isTreeGrid) ? _this.VclTreeGridControl.getRowSeleccionado() : _this.VclGridCF.FocusedRowHandle;//Obtenemos fila del grid o treegrid.
            if (!isTreeGrid) {//Estamos en grid
                if (_this.IndiceLogico == null) {//Si orden del grid es el mismo del dataset
                    if (_this.IndexRowGridPorScroll != null) {//indice de fila en grid
                        _this.VclGridCF.IndexRow = _this.IndexRowGridPorScroll;//indice de fila en grid
                    } else {
                        _this.VclGridCF.IndexRow = EventArgs;//Índice de fila en grid (mismo índice del dataset)
                    }
                }
                else {
                    if (_this.IndexRowGridPorScroll != null) {//indice de fila en grid
                        _this.VclGridCF.IndexRow = _this.IndexRowGridPorScroll;//indice de fila en grid
                    } else {
                        _this.VclGridCF.IndexRow = _this.IndiceLogico;//indice de fila en grid igual a 0
                    }
                }
                if (_this.VclGridCF.FocusedRowHandle != null) {//Fila focus distinta de nulo.
                    var indexRecord = _this.VclGridCF.FocusedRowHandle.IndexDataSet;//Índice de fila en DataSet
                    _this.ChangeIndex(indexRecord);//Enviamos el índice a ChangeIndex, la cual enviará por fuera el valor.
                }
            } else {
                _this.ChangeIndex(EventArgs);//Enviamos el índice a ChangeIndex, la cual enviará por fuera el valor.
            }
        }
        _this.DataSet.OnAddColumn = function (Object, EventArgs) {
            if (_this.IsAddColumn == false) {//Si no se estan creando las columnas en el DataSet.
                var ColMem1 = new TVclColumnCF();//Creamos columna.
                ColMem1.Name = EventArgs.FieldName;//Name
                ColMem1.Caption = EventArgs.FieldName;//Caption
                ColMem1.Index = Object.FieldDefs.length - 1;//Index
                ColMem1.DataType = EventArgs.DataType;//Tipo de dato
                ColMem1.ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.None;//Estilo de columna.
                _this.VclGridCF.AddColumn(ColMem1);//Agregamos columna.
            }
        }
    }
}
/*Función para navegar al primer registro*/
Componet.GridCF.TGridCFView.prototype.First = function () {
    var _this = this.TParent();
    if (_this.VclTreeGridControl.inTreeGrid) {//Validamos si estamos en el TreeGrid
        if (_this.VclTreeGridControl.LastRutaGroup != null && _this.VclTreeGridControl.GridTreeGridControl.Rows.length > 0) {//validamos que una agrupación este abierta en el TreeGrid
            _this.VclTreeGridControl.VPagination.GoTo(1);//Navegamos a la primera página de la agrución selecionada
            _this.DataSet.SetIndex(_this.VclTreeGridControl.GridTreeGridControl.FocusedRowHandle.IndexDataSet);//Actulizamos valor de Index
        }
    } else {
        //Validamos si estamos en el Grid
        if (_this.VclGridCF.Rows.length > 0) {//Validamos que esistan filas
            if (_this.IsPagination) {//Validamos si estamos en una paginación.
                if (_this.VclGridCF.Rows[0].IndexTreeGrid == 0) {//Validamos si estamos en la primera posición del ItemTreeGrid
                    _this.VclGridCF.IndexRow = 0;//Seleccionamos la primera fila
                    _this.IndexRowGridPorScroll = _this.VclGridCF.FocusedRowHandle.Index;//Indice de la fila actual en el grid
                    _this.DataSet.SetIndex(_this.VclGridCF.FocusedRowHandle.IndexDataSet);//Actualizamos indice de dataset
                } else {
                    _this.VPagination.GoTo(1);//cambiamos de página
                }
            } else {
                _this.div1.scrollTop = 0;//Subimos el scroll al inicio
                _this.VclGridCF.IndexRow = 0;//Seleccionamos la primera fila
                _this.IndexRowGridPorScroll = _this.VclGridCF.FocusedRowHandle.Index;//Indice de la fila actual en el grid
            }
            _this.DataSet.SetIndex(_this.VclGridCF.FocusedRowHandle.IndexDataSet);//Actualizamos el indice del DataSet
        }
    }
}
/*Función para navegar al registro anterior*/
Componet.GridCF.TGridCFView.prototype.Prior = function () {
    var _this = this.TParent();
    if (!_this.VclTreeGridControl.inTreeGrid) {
        //Validamos que estamos en el Grid principal 
        if (_this.VclGridCF.Rows.length > 0) {//Validamos que existan filas
            if (_this.VclGridCF.FocusedRowHandle.Index == 0 && _this.VclGridCF.FocusedRowHandle.IndexTreeGrid > 0) {//Para cambiar a una página anterior
                if (_this.IsPagination) {//validamos que estamos en una paginación
                    _this.isEventChangePaginationScroll = false;
                    _this.VPagination.GoTo(_this.VPagination.IndexPage - 1);//retrocedemos una página
                    _this.IndexRowGridPorScroll = _this.VclGridCF.FocusedRowHandle.Index;//indice actual del grid
                } else {
                    var scrollTop = _this.heightFilas * (_this.VclGridCF.FocusedRowHandle.IndexTreeGrid - 2) + _this.heightEncabezados;//Subimos el scroll de acuerdo al posicion del grupo anterior
                    var indiceActual = parseInt(scrollTop / (_this.heightFilas * _this.filasMostradas));//obtnemos el indice actual del scroll
                    if (_this.ultimoIndice != indiceActual && indiceActual <= _this.numeroMaximoIndices) {//validamos si el indice esta dentro de una rango valido
                        _this.isEventChangePaginationScroll = false;
                        _this.div1.scrollTop = scrollTop;//subimos el scroll
                    }
                }
            }
            else {//nos econtramos en la misma página
                _this.VclGridCF.IndexRow = _this.VclGridCF.IndexRow - 1;//retrocedemos una fila
                _this.IndexRowGridPorScroll = _this.VclGridCF.FocusedRowHandle.Index;//indice de la fila focus delgrid
                _this.DataSet.SetIndex(_this.VclGridCF.FocusedRowHandle.IndexDataSet);
                _this.DataSet.RecordSet = _this.DataSet.Records[_this.VclGridCF.FocusedRowHandle.IndexDataSet];//cambiamos indice
            }
        }
    }
    else {//Validamos si estamos en el TreeGrid
        if (_this.VclTreeGridControl.LastRutaGroup != null && _this.VclTreeGridControl.GridTreeGridControl.Rows.length > 0) {//validamos que una agrupación este abierta en el TreeGrid
            if (_this.VclTreeGridControl.GridTreeGridControl.FocusedRowHandle.Index == _this.VclTreeGridControl.LastRutaGroup.IndexRowStart && _this.VclTreeGridControl.GridTreeGridControl.FocusedRowHandle.IndexTreeGrid > 0) {
                _this.VclTreeGridControl.isEventChangePaginationScroll = false;
                _this.VclTreeGridControl.VPagination.GoTo(_this.VclTreeGridControl.VPagination.IndexPage - 1);//cambiamos de paginación
                _this.VclTreeGridControl.IndexRowGridPorScroll = _this.VclTreeGridControl.GridTreeGridControl.FocusedRowHandle.Index;//indice del actual de la fila en el treeGrid
            }
            else {
                _this.VclTreeGridControl.GridTreeGridControl.IndexRow = _this.VclTreeGridControl.GridTreeGridControl.IndexRow - 1;
                _this.VclTreeGridControl.IndexRowGridPorScroll = _this.VclTreeGridControl.GridTreeGridControl.FocusedRowHandle.Index;
                _this.DataSet.SetIndex(_this.VclTreeGridControl.GridTreeGridControl.FocusedRowHandle.IndexDataSet);
                _this.DataSet.RecordSet = _this.DataSet.Records[_this.VclTreeGridControl.GridTreeGridControl.FocusedRowHandle.IndexDataSet];
            }
        }
    }

}
/*Función para navegar al siguiente registro*/
Componet.GridCF.TGridCFView.prototype.Next = function () {
    var _this = this.TParent();


    if (_this.VclTreeGridControl.inTreeGrid) {

        if (_this.VclTreeGridControl.LastRutaGroup != null && _this.VclTreeGridControl.GridTreeGridControl.Rows.length > 0) {

            if (_this.VclTreeGridControl.GridTreeGridControl.FocusedRowHandle.Index >= _this.VclTreeGridControl.LastRutaGroup.IndexRowEnd) {
                _this.VclTreeGridControl.VPagination.GoTo(_this.VclTreeGridControl.VPagination.IndexPage + 1);
                _this.VclTreeGridControl.GridTreeGridControl.IndexRow = _this.VclTreeGridControl.LastRutaGroup.IndexRowStart;
            }
            else {
                _this.VclTreeGridControl.GridTreeGridControl.IndexRow = _this.VclTreeGridControl.GridTreeGridControl.IndexRow + 1;
                //_this.IndexRowGridPorScroll = _this.VclGridCF.FocusedRowHandle.Index;
                _this.DataSet.SetIndex(_this.VclTreeGridControl.GridTreeGridControl.FocusedRowHandle.IndexDataSet);
                _this.DataSet.RecordSet = _this.DataSet.Records[_this.VclTreeGridControl.GridTreeGridControl.FocusedRowHandle.IndexDataSet];
            }
        }
    }
    else {
        if (_this.VclGridCF.Rows.length > 0) {

            if (_this.VclGridCF.FocusedRowHandle.Index >= _this.VclGridCF.Rows.length - 1) {
                if (_this.IsPagination) {

                    _this.VPagination.GoTo(_this.VPagination.IndexPage + 1);
                    _this.VclGridCF.IndexRow = 0;
                    _this.IndexRowGridPorScroll = _this.VclGridCF.FocusedRowHandle.Index;
                } else {
                    var scrollTop = _this.heightFilas * (_this.VclGridCF.FocusedRowHandle.IndexTreeGrid + 1) + _this.heightEncabezados;
                    var indiceActual = parseInt(scrollTop / (_this.heightFilas * _this.filasMostradas));
                    if (_this.ultimoIndice != indiceActual && indiceActual <= _this.numeroMaximoIndices) {
                        _this.div1.scrollTop = scrollTop;
                        //_this.isEventChangePaginationScroll = true;
                    }
                }

            }
            else {
                _this.VclGridCF.IndexRow = _this.VclGridCF.IndexRow + 1;
                _this.IndexRowGridPorScroll = _this.VclGridCF.FocusedRowHandle.Index;
                _this.DataSet.SetIndex(_this.VclGridCF.FocusedRowHandle.IndexDataSet);
                _this.DataSet.RecordSet = _this.DataSet.Records[_this.VclGridCF.FocusedRowHandle.IndexDataSet];
            }
        }
    }



}
/*Función para navegar al último registro*/
Componet.GridCF.TGridCFView.prototype.Last = function () {
    var _this = this.TParent();
    if (_this.VclTreeGridControl.inTreeGrid) {
        if (_this.VclTreeGridControl.LastRutaGroup != null && _this.VclTreeGridControl.GridTreeGridControl.Rows.length > 0) {
            if (_this.VclTreeGridControl.VPagination.GetNumeroMaximoIndices() != _this.VclTreeGridControl.VPagination.IndexPage) {
                _this.VclTreeGridControl.isEventChangePaginationScroll = false;
                _this.VclTreeGridControl.VPagination.GoTo(_this.VclTreeGridControl.VPagination.GetNumeroMaximoIndices());
            } else {
                _this.VclTreeGridControl.GridTreeGridControl.IndexRow = _this.VclTreeGridControl.GridTreeGridControl.Rows[_this.VclTreeGridControl.LastRutaGroup.IndexRowEnd].Index;
                _this.DataSet.SetIndex(_this.VclTreeGridControl.GridTreeGridControl.FocusedRowHandle.IndexDataSet);
            }
        }
    }
    else {
        if (_this.VclGridCF.Rows.length > 0) {

            if (_this.IsPagination) {

                if (_this.VPagination.GetNumeroMaximoIndices() != _this.VPagination.IndexPage) {
                    _this.isEventChangePaginationScroll = false;
                    _this.VPagination.GoTo(_this.VPagination.GetNumeroMaximoIndices());
                    _this.IndexRowGridPorScroll = _this.VclGridCF.FocusedRowHandle.Index;
                } else {
                    _this.VclGridCF.IndexRow = _this.VclGridCF.Rows.length - 1;
                    _this.IndexRowGridPorScroll = _this.VclGridCF.FocusedRowHandle.Index;
                    _this.DataSet.SetIndex(_this.VclGridCF.FocusedRowHandle.IndexDataSet);
                }


            } else {
                var scrollTop = _this.heightFilas * (_this.numeroMaximoIndices * _this.filasMostradas) + _this.heightEncabezados;
                var indiceActual = parseInt(scrollTop / (_this.heightFilas * _this.filasMostradas));
                if (_this.ultimoIndice != indiceActual && indiceActual <= _this.numeroMaximoIndices) {
                    _this.isEventChangePaginationScroll = false;
                    _this.div1.scrollTop = scrollTop;
                } else {
                    _this.VclGridCF.IndexRow = _this.VclGridCF.Rows.length - 1;
                    _this.IndexRowGridPorScroll = _this.VclGridCF.FocusedRowHandle.Index;
                    _this.DataSet.SetIndex(_this.VclGridCF.FocusedRowHandle.IndexDataSet);
                }
            }

        }

    }

}
Componet.GridCF.TGridCFView.prototype.filterSQLDefault = null;//Indica al grid si existe un filtro por defecto.
Componet.GridCF.TGridCFView.prototype.IndiceGrillaEventDataSourcePostType = null;//Acción que se producirá en el OnRefreschRecordSet.
Componet.GridCF.TGridCFView.prototype.LastStatusEventTreeGridControl = null;//Último Evento en TreeGrid.
Componet.GridCF.TGridCFView.prototype.IsLoadDataSourceComplete = true;//Indica que se cargo el dataset.
Componet.GridCF.TGridCFView.prototype.isEventChangePaginationScroll = true;//Acción que se produce en el OnEventGoTo de TPagination.
Componet.GridCF.TGridCFView.prototype.IsPagination = true;//Inidica: true = Paginación activa, false = Scroll activo 
Componet.GridCF.TGridCFView.prototype.FilterQueryCustomFilter = null;//Filtro general
Componet.GridCF.TGridCFView.prototype.OnGridRefresch = null;//Función que se ejecuta cuando al seleccionar el Bar refresh (Se implementa por fuera).
/*Función que se ejecuta cuando al seleccionar el Bar refresh (Llamado desde el gpview)*/
Componet.GridCF.TGridCFView.prototype.GridRefresch = function () {
    var _this = this.TParent();
    if (_this.OnGridRefresch != null) {//Validamos que funcón sea distinta de nula.
        _this.OnGridRefresch(_this, _this.GridCFToolbar);//Ejecutamos función.
    }
}
/*Función para cargar columnas*/
Componet.GridCF.TGridCFView.prototype.loadDataSetColumns = function (DataSet) {
    var _this = this.TParent();
    _this.IsLoadDataSourceComplete = false;
    _this.DataSet = DataSet;
    var UnDataSet = _this.DataSet;
    var UnRecord = UnDataSet.RecordSet;//EventArgs;
    var isnotNull = (UnDataSet != null)
    if (isnotNull)// Validamos que el DataSet sea distinto de null
    {
        UnDataSet._EnableControls = true;
        _this.VclGridCF.FileSRVEvent = _this.FileSRVEvent;//Evento para la subida y descarga de archivos
        if (UnDataSet.EnableControls) {
            _this.ItemTreePanel = new Array();
            _this.nameColumns = new Array();//Lista de las columnas
            _this.OrderColumnsDefault = new Array();//Orden de las columnas inicial - Le pasamo este orden a TCfg para crear las vistas.
            _this.ColumnaSelecionada = null;// Columna Ordenada
            _this.IndiceLogico = null;//Indica que el orden de las filas en el grid es distinto al orden de las filas en el DataSet
            //_this.inOnEvenFilterColumn = false;//Indica si se esta realizando un filtro 
            _this.VclGridCF.ClearAll();//Limpiamos el grid
            var parentRowHeader = _this.VclGridCF.RowHeader.parentNode;//Obtener el parentNode
            parentRowHeader.removeChild(_this.VclGridCF.RowHeader);//Removemos el parentNode para luego agregarlo
            var ColMemCkeck = new TVclColumnCF();//Instanciamos columna
            ColMemCkeck.Name = "OptionButtonCkeck";
            ColMemCkeck.IsOptionCheck = true;//Indica que la columna en de tipo OptionCheck
            ColMemCkeck.Caption = "";//Caption de columna
            ColMemCkeck.Index = 0;//Indice de columna
            ColMemCkeck.EnabledEditor = true;//Indicamos que sus filas son editables.
            ColMemCkeck.DataType = SysCfg.DB.Properties.TDataType.Boolean;//Tipo de dato
            ColMemCkeck.ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.None;//estilo
            _this.VclGridCF.IndexCol = 0;
            _this.VclGridCF.AddColumn(ColMemCkeck);//Agregamos columna
            for (var i = 0, t = UnDataSet.FieldCount; i < t; i++) {//Recorremos las columnas del DataSet
                var ColMem1 = new TVclColumnCF();//Instanciamos columna
                ColMem1.Name = UnDataSet.FieldDefs[_this.ObtenerIndexDataSource(i + 1)].FieldName;//Obtnemos y asignamos el value del TField
                ColMem1.Index = i + 1;//Asignamos el indice a la columna
                ColMem1.OnEventClickTextNode = function (Col) {
                    _this.CreateModalOptionColumn(Col);
                }
                ColMem1.IndexDataSourceDefault = i;// !IndexDataSourceDefault: Indice que ocupa el TField en la columna del DataSet. Por ejemplo si movemos la columna, entonces su indice cambia respecto al orden en la grilla, pero el "IndexDataSourceDefault" se conserva.
                ColMem1.DataType = UnDataSet.FieldDefs[_this.ObtenerIndexDataSource(i + 1)].DataType;////Obtnemos y asignamos el DataType del TField
                ColMem1.ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.None;//Asignamos por defecto el estilo "None"
                var captionHeader = UnDataSet.FieldDefs[_this.ObtenerIndexDataSource(i + 1)].FieldName;//Obtnemos value del TField
                if (_this.AutoTranslete) {//_this.AutoTranslete igual a true, indica que el name de las columnas se deden traducir.
                    captionHeader = UsrCfg.Traslate.GetLangTextdb(captionHeader, captionHeader);//Traducimos el texto.
                }
                ColMem1.CaptionOrder = captionHeader + ""; //Asignamos el Capition de la columna.
                _this.VclGridCF.IndexCol = i + 1;//Establecemos el indice de la columna en el grid.
                _this.VclGridCF.AddColumn(ColMem1);//Agregamos columna.
                ColMem1.onclick = function (Col) {
                    //Evento para la columna en el grid
                    //if (!_this.inOnEvenFilterColumn) {// validamos si no esta a
                    _this.ColumnClick(Col);
                    //}
                    //_this.inOnEvenFilterColumn = false;
                }
                ColMem1.onEventFilterColumn = function (Col) {
                    //Propiedad para mostrar el modal con las agrupación de fila que pertenescan a la columna.
                    //_this.inOnEvenFilterColumn = true;
                    _this.QueryLinqModal(Col.Index - 1);
                    _this.VclModalColumnsLinq.ShowModal();

                }
                ColMem1.onEventFilter = function (Column, value) {
                    /*
                    Esta función es llamada cuando se ejecuta la consulta en el filtro de la columna.
                    Realiza la busqueda y actuliza las filas de la tabla. Además devuelve el número de registros que se encontro
                    */
                    var count = 0;//Por defecto número de registros encontrados = 0
                    if (Column.DataType == SysCfg.DB.Properties.TDataType.DateTime) {//Validamos si el tipo de dato es TDataType.DateTime
                        var dateStart = value.dateStart;//Fecha de inicio
                        var dateEnd = value.dateEnd;//Fecha fin
                        if (dateStart != null && dateEnd != null) {// Validamos si los dos campos son distintos de nulo.
                            dateStart = new Date(dateStart);//convertimos el valor a clase Date
                            dateEnd = new Date(dateEnd);//convertimos el valor a clase Date
                        } else {
                            count = _this.ItemTreePanel.length;//Número de registros encontrados.
                            return count;
                        }
                    }
                    if (_this.VclTreeGridControl != null && _this.VclTreeGridControl.inTreeGrid) {//Filtro en el Tree
                        _this.VclTreeGridControl.LastRutaGroup = null;// Colocamos LastRutaGroup igual null
                        _this.SetRecordsPaginationFilterItemTreePanel(false, Column, value, null, false);//Realizamos la busquedad
                        _this.VclTreeGridControl.ItemTreePanel = _this.ItemTreePanel;//Actualizamos el ItemTreePanel que sirve para agrupagar el tree.
                        _this.VclTreeGridControl.agruparPorCampo();//Agrupamos el Tree
                    }
                    else {
                        if (_this.IsPagination) {//Validamos si el grid esta paginado.
                            _this.loadDataSetRecordsPaginationFilter(false, Column, value, null, false);//Realizamos la busquedad y actulizamos el grid con el paginador.
                        } else {
                            _this.loadDataSetRecordsScrollFilter(false, Column, value, null, false);//Realizamos la busquedad y actulizamos el grid con el scroll.
                        }
                    }
                    count = _this.ItemTreePanel.length;//Número de registros encontrados.
                    return count;//Retornamos el número de registros.
                }
                _this.nameColumns.push("" + UnDataSet.FieldDefs[_this.ObtenerIndexDataSource(i + 1)].FieldName);//Agregamos el name
                _this.OrderColumnsDefault.push("" + UnDataSet.FieldDefs[_this.ObtenerIndexDataSource(i + 1)].FieldName);//Agregamos el orden de la columnas inicial.
                _this.VclDrag.addDragable(ColMem1, ColMem1.This);//!Indicamos que la columna se podra arrastrar.
                //!Establecemos que la column es un contenedor de arrastre
                _this.VclDrag.addEventContendor(ColMem1, ColMem1.This, function (Content, Component, Event) {
                    var VclColumConten = Content;//Obtnemos el contenedor
                    var VclColumn = Component;//Obtnemos el elemento que se esta arrastrando
                    if (VclColumConten.Index != VclColumn.Index) {//Validamos que los indices sean distintos.
                        _this.MoveColumns(VclColumConten.Index, VclColumn.Index);//Movemos las columnas.
                        _this.DataSet.Cancel();//Cancelamos el DataSet.
                    }
                });
            }
            var ColMemButton = new TVclColumnCF();//Instanciamos una Columna
            ColMemButton.Name = "OptionButton";//Asignamos un Name
            ColMemButton.CaptionButton = "OPTIONS";//Asignamos un caption
            ColMemButton.Index = UnDataSet.FieldCount;//Establecemos un Indice
            ColMemButton.EnabledEditor = true;//Indicamos que sus filas son editables.
            ColMemButton.ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.None;//Estilo del control
            _this.VclGridCF.IndexCol = UnDataSet.FieldCount;//Establecemos el indice de la columna en el grid.
            ColMemButton.Visible = false;//Indicamos que por defecto la columna no se muestre
            ColMemButton.IsOption = true;//Indicamos que  columna es una option
            _this.VclGridCF.AddColumn(ColMemButton);//Agregamos columna
            parentRowHeader.appendChild(_this.VclGridCF.RowHeader); //Agregamos las columnas al parentNode.
            _this.VclGridCF._DivPanelStatus = _this.Status.ContlabelBE;//Asignamos el div del status.
            _this.VclGridCF._DivDescriptionRow = _this.VclStackPanelContentInfoMenTable;
            _this.VclGridCF._DivPanelGroup = _this.VclStackPanelComboxTable;//Asignamos el div del combo para la agrupación.
            _this.VclGridCF.EnableNavigatorControl = false;//Propieadad para habilitar el las flechas de navegacion.
            _this.VclGridCF.EnablePanelStatus = false;//Propieadad para habilitar el estatus.
            _this.VclGridCF.EnableNavigatorEdit = false;//Propieadad para habilitar los editables.
            _this.VclGridCF.EnableDescriptionRow = false;
            _this.VclGridCF.EnablePanelGroup = false;//Propieadad para habilitar agrupación.
            _this.VclGridCF.EnableEditOptionsColumn = false;
            _this.VclGridCF.EnableColumnGrid = false;
            //!Establecemos el ancho de la tabla, sirve para que las columnas en los dispositivos móviles sean mas anchas
            if (SysCfg.App.Properties.Device != SysCfg.App.TDevice.Desktop && SysCfg.Device.IsMovile1()) {//Validamos si es dispositivo móvil.
                if (_this.VclGridCF.Columns.length > 5) {//Validamos si el numeor de columnas es mayor a 5
                    var widthInitialize = $(_this.VclGridCF.This).width();//Obtenemos el ancho de la tabla
                    _this.VclGridCF.This.style.width = (widthInitialize * 3) + "px";//Triplicamos el ancho de la tabla.
                }
            }
        }
    }

}
/*Función para cargar registros*/
Componet.GridCF.TGridCFView.prototype.loadDataSetRecords = function () {
    var _this = this.TParent();
    _this.SetSelectedRows = false;//filas desmarcadas.
    _this.IsLoadDataSetRefresh = true;//Indica que el evento ChangeIndex se pude ejecutar.
    _this.filterSQLDefault = null;//
    _this.IsLoadDataSourceComplete = true;
    if (_this.FilterQueryCustomFilter != null) {//Validamos si existe un TFilterQuery 
        $(_this.VclStackPanelContentCustomFilter.Column[0].This).html("");//Limpiamos el div del filtro
        _this.FilterQueryCustomFilter = null;//Colocamo el filtro en null
    }
    if (_this.ListCustomFilter != null && _this.ListCustomFilter.length > 0) {//Validamos si existe una lista de filtro
        var tempListCustomFilter = new Array();//Creamos un temporal de la lista de filtros.
        for (var i = 0; i < _this.ListCustomFilter.length; i++) {
            tempListCustomFilter.push(_this.ListCustomFilter[i]);
        }
        _this.FilterQueryCustomFilter = new Componet.GridCF.TGridCFView.TFilterQuery();//Creamos un TFilterQuery
        _this.FilterQueryCustomFilter.VclGridCF = _this.VclGridCF;
        _this.FilterQueryCustomFilter.ListCustomFilter = tempListCustomFilter;
        _this.filterSQLDefault = _this.FilterQueryCustomFilter.CreateQueryCustomFilter();
        if (_this.filterSQLDefault == "") {
            _this.filterSQLDefault = null;
        }
        else {
            _this.VclStackPanelContentCustomFilter.Row.This.style.marginTop = "10px";
            $(_this.VclStackPanelContentCustomFilter.Column[0].This).html("");
            _this.VclListBoxCustomFilter = new TVclListBox(_this.VclStackPanelContentCustomFilter.Column[0].This, "");
            _this.VclListBoxCustomFilter.EnabledCheckBox = true;
            _this.VclListBoxCustomFilter.OnCheckedListBoxItemChange = function (EventArgs, Object) {
                var cheked = Object.FocusedListBoxItem.Checked;
                _this.DataSet.Status = SysCfg.MemTable.Properties.TStatus.None;
                if (cheked) {
                    _this.filterSQLDefault = _this.FilterQueryCustomFilter.CreateQueryCustomFilter();

                } else {
                    _this.filterSQLDefault = null;
                }

                if (_this.VclTreeGridControl != null && _this.VclTreeGridControl.inTreeGrid) {
                    _this.Information.VclGridCFInfo.ClearAll();
                    _this.SetRecordsPaginationFilterItemTreePanel(true, null, null, null, false);
                    _this.VclTreeGridControl.ItemTreePanel = _this.ItemTreePanel;
                    _this.VclTreeGridControl.agruparPorCampo();

                } else {
                    if (_this.IsPagination) {

                        _this.loadDataSetRecordsPaginationFilter(true, null, null, null, false);
                    } else {
                        _this.loadDataSetRecordsScrollFilter(true, null, null, null, false);
                    }
                }



            }
            var ListBoxItemCustomFilter = new TVclListBoxItem();
            ListBoxItemCustomFilter.Text = _this.FilterQueryCustomFilter.GetStringQueryCustomFilter();
            ListBoxItemCustomFilter.Index = i;
            ListBoxItemCustomFilter.Checked = true;
            _this.VclListBoxCustomFilter.AddListBoxItem(ListBoxItemCustomFilter);
        }
        _this.ListCustomFilter = null;
    }
    _this.InitEventDataSource();//Iniciamos los eventos del DataSet
    _this.Information.Init(_this.nameColumns.length, _this.DivitionColumnInformation);//Actualizamos el detalle del grid
    _this.FilterQuery.Init(_this.nameColumns, _this.VclGridCF);//
    if (_this.GridCFToolbar != null) {
        _this.VclGridCF.EnableDescriptionRow = _this.GridCFToolbar.ButtonImg_DatailsViewBar_ChekedClickDown;
        _this.VclGridCF.EnablePanelStatus = _this.GridCFToolbar.ButtonImg_StatusBar_ChekedClickDown;
        _this.VclGridCF.EnableEditOptionsColumn = _this.GridCFToolbar.ButtonImg_RowEditToolsBar_ChekedClickDown;
        _this.VclGridCF.EnableNavigatorEdit = (_this.GridCFToolbar.ButtonImg_EditToolsBar_Visible == false) ? false : _this.GridCFToolbar.ButtonImg_EditToolsBar_ChekedClickDown;
        _this.GridCFToolbar.ButtonImg_DeleteBar_Visible = _this.VclGridCF.EnableNavigatorEdit;
        _this.GridCFToolbar.ButtonImg_AddBar_Visible = _this.VclGridCF.EnableNavigatorEdit;
        _this.GridCFToolbar.ButtonImg_EditBar_Visible = _this.VclGridCF.EnableNavigatorEdit;
        _this.GridCFToolbar.ButtonImg_PostBar_Visible = _this.VclGridCF.EnableNavigatorEdit;
        _this.GridCFToolbar.ButtonImg_CancelBar_Visible = _this.VclGridCF.EnableNavigatorEdit;
        _this.VclGridCF.EnableNavigatorControl = _this.GridCFToolbar.ButtonImg_NavigatorToolsBar_ChekedClickDown;
        _this.GridCFToolbar.ButtonImg_PriorFirstBar.Visible = _this.VclGridCF.EnableNavigatorControl;
        _this.GridCFToolbar.ButtonImg_PriorBar.Visible = _this.VclGridCF.EnableNavigatorControl;
        _this.GridCFToolbar.ButtonImg_NextBar.Visible = _this.VclGridCF.EnableNavigatorControl;
        _this.GridCFToolbar.ButtonImg_NextLastBar.Visible = _this.VclGridCF.EnableNavigatorControl;
        _this.VclGridCF.EnablePanelGroup = _this.GridCFToolbar.ButtonImg_ColumnGroupBar_ChekedClickDown;
        _this.VclTreeGridControl.Show = _this.VclGridCF.EnablePanelGroup;
        _this.VclTreeGridControl.GridCFView = _this;
        _this.VclGridCF.showHideColumn(0, _this.GridCFToolbar.ButtonImg_RowCheckToolsBar_ChekedClickDown);
    }
    if (_this.IsPagination) {
        _this.NumberRowVisible = SysCfg.App.IniDB.ReadString("Componet_GridCF", "RowShowInfo", null);//Obtnemos el número de  filas que se mostraran en el grid.
        _this.VPagination.FilasMostradas = _this.NumberRowVisible;//Establecemos el número de filas en VPagination, el cual forma los elemento de la paginación.
        _this.VPagination.IndexPage = 1;//Indice de página por defecto en 1
        _this.loadDataSetRecordsPaginationFilter(true, null, null, null, false);//Cargamos los datos en la paginación.
    }
    else {
        _this.div0.style.float = "left";
        _this.div1.style.float = "left";
        _this.div0.style.width = "98%";
        _this.div1.style.width = "2%";
        _this.filasMostradas = _this.NumberRowVisible;
        _this.VclGridCF.This.style.width = "100%";
        _this.div1.style.overflowY = "scroll";
        _this.div1.style.position = "relative";
        _this.div1.classList.add("classScrollGrid");
        _this.lastPosition = 0;
        _this.ultimoIndice = 0;
        _this.VclGridCF.ResponsiveCont.style.overflowY = "hidden";
        _this.div0.style.overflowY = "hidden";
        var timerMouseWhell;
        var _tableMouseWheel = function (event) {
            event.preventDefault();
            if (timerMouseWhell) {
                window.clearTimeout(timerMouseWhell);
            }

            timerMouseWhell = window.setTimeout(function () {
                var up = false;
                var down = false;
                var original = event;
                if (original.deltaY >= 1) {
                    up = true;
                } else {
                    down = true;
                }
                if (up) {
                    if (_this.ultimoIndice != 0) {
                        var scrollTop = (_this.ultimoIndice - 1) * (_this.heightFilas * _this.filasMostradas);
                        _this.div1.scrollTop = scrollTop;
                    }
                }
                if (down) {
                    var scrollTop = (_this.ultimoIndice + 1) * (_this.heightFilas * _this.filasMostradas);
                    _this.div1.scrollTop = scrollTop;
                }
                event.preventDefault();

            }, 20);
        }
        $(_this.VclGridCF.This).on("mousewheel", _tableMouseWheel);
        $(_this.VclGridCF.This).on("DOMMouseScroll", _tableMouseWheel);
        _this.loadDataSetRecordsScrollFilter(true, null, null, null, false);
    }

    if (_this.OrderTCfg.ColumnName != null) {//Validamos si exite un orden del grid inicial
        var tempColumnOrder = _this.VclGridCF.GetColumn(_this.OrderTCfg.ColumnName);//Obtenemos la columna por name.
        _this.Order(tempColumnOrder, _this.OrderTCfg.Type);//Ordenamos el grid
    }
    else {
        if ("" + _this.ColumnaSelecionada != "" && "" + _this.ColumnaSelecionada != "null" && _this.ColumnaSelecionada + "" != "") {
            _this.VclGridCF.Order = "ninguno";
            _this.VclGridCF.Columns[_this.ColumnaSelecionada].IsColumnSelectOrder = null;
            _this.VclGridCF.Columns[_this.ColumnaSelecionada].CaptionOrder = _this.VclGridCF.Columns[_this.ColumnaSelecionada].Caption;
        }
    }
}
/*Implementacion de Scroll*/
Componet.GridCF.TGridCFView.prototype.loadDataSetRecordsScrollFilter = function (isAllRecord, Column, filter, querySQLLinq, isOrder) {
    var _this = this.TParent();
    _this.IndiceLogico = 0;
    var UnDataSet = _this.DataSet;
    if (!(isOrder == true)) {
        _this.ItemTreePanel = new Array();
        if (!isAllRecord) {
            if (typeof (querySQLLinq) == "undefined" || querySQLLinq == null) {
                for (var e = 0, i1 = 0, tDataSet = UnDataSet.Records.length; e < tDataSet; e++ , i1++) {
                    var subItems = [];
                    var cadena = "" + UnDataSet.Records[e].Fields[Column.IndexDataSourceDefault].Value;
                    cadena = cadena.toUpperCase();
                    var isInsideQuery = true;
                    if (_this.filterSQLDefault != null) {
                        var ArrayGrid = new Array();
                        var subItemsArrayGrid = [];
                        for (var ContX = 1; ContX < _this.VclGridCF.Columns.length - 1; ContX++) {
                            var IndexDataSourceDefault = _this.VclGridCF.Columns[ContX].IndexDataSourceDefault;
                            subItemsArrayGrid.push([(_this.DataSet.Records[e].Fields[IndexDataSourceDefault].Value), i]);
                        }
                        ArrayGrid.push(subItemsArrayGrid);
                        var element = Enumerable.From(ArrayGrid).Where(_this.filterSQLDefault).Select("$").ToArray();
                        isInsideQuery = (!(element[0] == [] || element[0] == null))
                    }


                    if (isInsideQuery && cadena.indexOf(('' + filter).toUpperCase()) != -1) {
                        for (var ContX = 1, t = _this.VclGridCF.Columns.length - 1; ContX < t; ContX++) {
                            var IndexDataSourceDefault = _this.VclGridCF.Columns[ContX].IndexDataSourceDefault;
                            subItems.push(
                                [(UnDataSet.Records[e].Fields[IndexDataSourceDefault].Value), e]);//Valor, Orden
                        }
                        subItems.push("");
                        subItems.push(e);
                        _this.ItemTreePanel.push(subItems);
                    }
                }

            }
            else {
                for (var e = 0, i1 = 0, tDataSet = UnDataSet.Records.length; e < tDataSet; e++ , i1++) {
                    var subItems = [];
                    var ArrayGrid = new Array();
                    var subItemsArrayGrid = [];
                    for (var ContX = 1; ContX < _this.VclGridCF.Columns.length - 1; ContX++) {
                        var IndexDataSourceDefault = _this.VclGridCF.Columns[ContX].IndexDataSourceDefault;
                        subItemsArrayGrid.push([(_this.DataSet.Records[e].Fields[IndexDataSourceDefault].Value), i]);
                    }
                    ArrayGrid.push(subItemsArrayGrid);

                    var isInsideQuery = true;
                    if (_this.filterSQLDefault != null) {
                        var element = Enumerable.From(ArrayGrid).Where(querySQLLinq + " && " + _this.filterSQLDefault).Select("$").ToArray();
                    } else {
                        var element = Enumerable.From(ArrayGrid).Where(querySQLLinq).Select("$").ToArray();
                    }

                    if (!(element[0] == [] || element[0] == null)) {
                        for (var ContX = 1, t = _this.VclGridCF.Columns.length - 1; ContX < t; ContX++) {
                            var IndexDataSourceDefault = _this.VclGridCF.Columns[ContX].IndexDataSourceDefault;
                            subItems.push(
                                [(UnDataSet.Records[e].Fields[IndexDataSourceDefault].Value), e]);//Valor, Orden
                        }
                        subItems.push("");
                        subItems.push(e);
                        _this.ItemTreePanel.push(subItems);
                    }

                }
            }

        }
        else {
            for (var e = 0, i1 = 0, tDataSet = UnDataSet.Records.length; e < tDataSet; e++ , i1++) {
                var subItems = [];
                var isInsideQuery = true;
                if (_this.filterSQLDefault != null) {
                    var ArrayGrid = new Array();
                    var subItemsArrayGrid = [];
                    for (var ContX = 1; ContX < _this.VclGridCF.Columns.length - 1; ContX++) {
                        var IndexDataSourceDefault = _this.VclGridCF.Columns[ContX].IndexDataSourceDefault;
                        subItemsArrayGrid.push([(_this.DataSet.Records[e].Fields[IndexDataSourceDefault].Value), i]);
                    }
                    ArrayGrid.push(subItemsArrayGrid);
                    var element = Enumerable.From(ArrayGrid).Where(_this.filterSQLDefault).Select("$").ToArray();
                    isInsideQuery = (!(element[0] == [] || element[0] == null))
                }
                if (isInsideQuery) {
                    for (var ContX = 1, t = _this.VclGridCF.Columns.length - 1; ContX < t; ContX++) {
                        var IndexDataSourceDefault = _this.VclGridCF.Columns[ContX].IndexDataSourceDefault;
                        subItems.push(
                            [(UnDataSet.Records[e].Fields[IndexDataSourceDefault].Value), e]);//Valor, Orden
                    }
                    subItems.push("");
                    subItems.push(e);
                    _this.ItemTreePanel.push(subItems);
                }


            }
        }

    }
    _this.lastPosition = 0;
    _this.ultimoIndice = 0;

    _this.numeroFilas = _this.ItemTreePanel.length;
    _this.heightFilas = 34;
    _this.heightEncabezados = 54;
    _this.filasMostradas = _this.filasMostradas;
    _this.subiendo = true;
    _this.heightInicial = ((_this.heightFilas * _this.filasMostradas) + _this.heightEncabezados);
    _this.heightInicialPx = ((_this.heightFilas * _this.filasMostradas) + _this.heightEncabezados) + "px";
    _this.numeroMaximoIndices = parseInt(_this.numeroFilas / _this.filasMostradas) - 1;
    _this.heightTotal = ((_this.heightFilas * _this.numeroFilas) + _this.heightEncabezados);
    _this.heightTotalPx = ((_this.heightFilas * _this.numeroFilas) + _this.heightEncabezados) + "px";
    if (_this.numeroFilas > _this.filasMostradas && _this.numeroFilas % _this.filasMostradas > 0) {
        var tempFilas = parseInt(_this.numeroFilas / _this.filasMostradas) + 1;
        _this.heightTotal = ((tempFilas * _this.filasMostradas * _this.heightFilas) + _this.heightEncabezados);
        _this.heightTotalPx = ((tempFilas * _this.filasMostradas * _this.heightFilas) + _this.heightEncabezados) + "px";
        _this.numeroMaximoIndices = parseInt(_this.numeroFilas / _this.filasMostradas);
    }
    _this.div1.style.height = _this.heightInicialPx;
    _this.div2.style.height = _this.heightTotalPx;
    _this.div1.onscroll = function () {
    }
    _this.div1.scrollTop = 0;
    _this.div1.scrollLeft = 0;
    _this.div1.onscroll = function () {

        var scrollTop = $(_this.div1).scrollTop();
        _this.lastPosition = scrollTop;
        //_this.VclGridCF.This.style.marginTop = scrollTop + "px";
        var indiceActual = parseInt(scrollTop / (_this.heightFilas * _this.filasMostradas));
        if (_this.ultimoIndice != indiceActual && indiceActual <= _this.numeroMaximoIndices) {
            _this.DataSet.Status = SysCfg.MemTable.Properties.TStatus.None;
            _this.VclGridCF.ClearAllRows();
            _this.VclGridCF._FocusedRowHandle = null;
            var parentBody = _this.VclGridCF.Body.parentNode;
            parentBody.removeChild(_this.VclGridCF.Body);
            for (var e = (_this.filasMostradas * indiceActual), indexRow = 0, tDataSet = ((indiceActual + 1) * _this.filasMostradas), cantidadRecord = _this.ItemTreePanel.length; e < tDataSet && e < cantidadRecord; e++ , indexRow++) {
                var subItems = [];

                var NewRow = new TVclRowCF(); //CREA UN ROW PARA AGREGAR AL GRIDCONTROL 
                var CellCheck = new TVclCellCF(); //CREA UNA CELDA PARA QUE SE AGREGA AL ROW
                CellCheck.Value = _this.SetSelectedRows;
                CellCheck.IndexColumn = ContX; // ASIGNA O EXTRAE EL INDEX DE LA COLUMNA 
                CellCheck.IndexRow = e; // ASIGNA O EXTRAE EL INDEX DEL ROW 
                NewRow.AddCell(CellCheck); // AGREGA O EXTRAE LA CELDA AL ROW
                var IndiceReal = _this.ItemTreePanel[e][_this.nameColumns.length + 1];

                for (var ContX = 1, t = _this.VclGridCF.Columns.length - 1; ContX < t; ContX++) {
                    var IndexDataSourceDefault = _this.VclGridCF.Columns[ContX].IndexDataSourceDefault;
                    var Cell0 = new TVclCellCF();
                    Cell0.CellColor = "";
                    Cell0.Value = UnDataSet.Records[IndiceReal].Fields[IndexDataSourceDefault].Value; // ASIGNA O EXTRAE EL VALOR QUE SE MOSTRARA EN LA CELDA DEL GRIDCONTROL 
                    Cell0.IndexColumn = ContX; // ASIGNA O EXTRAE EL INDEX DE LA COLUMNA 
                    Cell0.IndexRow = e; // ASIGNA O EXTRAE EL INDEX DEL ROW 
                    NewRow.AddCell(Cell0); // AGREGA O EXTRAE LA CELDA AL ROW
                }
                //**Agrego celda para botones
                var CellButton = new TVclCellCF();
                CellButton.Value = "";
                CellButton.IndexColumn = UnDataSet.FieldCount;
                CellButton.IndexRow = e;
                //CellButton.Control = new TVclStackPanel(CellButton.This, "1", 1, [[12]]);
                _this.DibujarCellButton(true, NewRow, CellButton);
                NewRow.AddCell(CellButton);
                NewRow.onclick = function (NewRow) { // ASIGNA UN EVENTO CUANDO SE HACE CLICK EN EL ROW DEL GRIDCONTROL
                    _this.RowClick(NewRow);
                }
                NewRow.ondblclick = function (NewRow) {
                    _this.RowDblClick(NewRow);
                }
                _this.VclGridCF.AddRow(NewRow);
                NewRow.Index = indexRow;
                NewRow.IndexDataSet = IndiceReal;
                NewRow.IndexTreeGrid = e
            }
            parentBody.appendChild(_this.VclGridCF.Body);
            _this.VclGridCF.ContracAllColumns();
            _this.ultimoIndice = indiceActual;
            if (_this.isEventChangePaginationScroll) {
                _this.VclGridCF.IndexRow = 0;
                _this.IndexRowGridPorScroll = _this.VclGridCF.FocusedRowHandle.Index;
                _this.DataSet.SetIndex(_this.VclGridCF.FocusedRowHandle.IndexDataSet);
            } else {
                _this.isEventChangePaginationScroll = true;
                _this.VclGridCF.IndexRow = _this.VclGridCF.Rows.length - 1;
                _this.IndexRowGridPorScroll = _this.VclGridCF.FocusedRowHandle.Index;
                _this.DataSet.SetIndex(_this.VclGridCF.FocusedRowHandle.IndexDataSet);

            }
            if (_this.VclGridCF.Rows.length != _this.filasMostradas) {
                _this.VclGridCF.ResponsiveCont.style.height = _this.heightInicialPx;
            } else {
                _this.VclGridCF.ResponsiveCont.style.height = "";
            }
        }
    }
    _this.VclGridCF.ClearAllRows();
    var columnaDeIndexDataSorce = _this.nameColumns.length + 1;
    if (_this.ItemTreePanel.length > 0) {
        var parentBody = _this.VclGridCF.Body.parentNode;
        parentBody.removeChild(_this.VclGridCF.Body);
        for (var e = 0, tDataSet = _this.filasMostradas, cantidadRecord = _this.ItemTreePanel.length; e < tDataSet && e < cantidadRecord; e++) {
            var IndiceReal = _this.ItemTreePanel[e][_this.nameColumns.length + 1];
            var NewRow = new TVclRowCF(); //CREA UN ROW PARA AGREGAR AL GRIDCONTROL 
            var CellCheck = new TVclCellCF(); //CREA UNA CELDA PARA QUE SE AGREGA AL ROW
            CellCheck.Value = _this.SetSelectedRows;
            CellCheck.IndexColumn = ContX; // ASIGNA O EXTRAE EL INDEX DE LA COLUMNA 
            CellCheck.IndexRow = e; // ASIGNA O EXTRAE EL INDEX DEL ROW 
            NewRow.AddCell(CellCheck); // AGREGA O EXTRAE LA CELDA AL ROW
            for (var ContX = 1, t = _this.VclGridCF.Columns.length - 1; ContX < t; ContX++) {

                var IndexDataSourceDefault = _this.VclGridCF.Columns[ContX].IndexDataSourceDefault;
                var Cell0 = new TVclCellCF();
                Cell0.CellColor = "";
                Cell0.Value = UnDataSet.Records[IndiceReal].Fields[IndexDataSourceDefault].Value; // ASIGNA O EXTRAE EL VALOR QUE SE MOSTRARA EN LA CELDA DEL GRIDCONTROL 
                Cell0.IndexColumn = ContX; // ASIGNA O EXTRAE EL INDEX DE LA COLUMNA 
                Cell0.IndexRow = e; // ASIGNA O EXTRAE EL INDEX DEL ROW 
                NewRow.AddCell(Cell0); // AGREGA O EXTRAE LA CELDA AL ROW
            }
            var CellButton = new TVclCellCF();
            CellButton.Value = "";
            CellButton.IndexColumn = UnDataSet.FieldCount;
            CellButton.IndexRow = e;
            //CellButton.Control = new TVclStackPanel(CellButton.This, "1", 1, [[12]]);
            _this.DibujarCellButton(true, NewRow, CellButton);
            NewRow.AddCell(CellButton);
            NewRow.onclick = function (NewRow) { // ASIGNA UN EVENTO CUANDO SE HACE CLICK EN EL ROW DEL GRIDCONTROL

                _this.RowClick(NewRow);
            }
            NewRow.ondblclick = function (NewRow) {
                _this.RowDblClick(NewRow);
            }
            _this.VclGridCF.AddRow(NewRow);
            NewRow.Index = e;
            NewRow.IndexDataSet = IndiceReal;
            NewRow.IndexTreeGrid = e;
        }
        parentBody.appendChild(_this.VclGridCF.Body);
        _this.VclGridCF.ContracAllColumns();
        _this.VclGridCF.IndexRow = 0;
        _this.IndexRowGridPorScroll = 0;
        _this.DataSet.SetIndex(_this.VclGridCF.FocusedRowHandle.IndexDataSet);
    }
}
/*Implementacion de Paginación*/
Componet.GridCF.TGridCFView.prototype.loadDataSetRecordsPaginationFilter = function (isAllRecord, Column, filter, querySQLLinq, isFilterRecord) {
    var _this = this.TParent();
    _this.IndiceLogico = 0;//Indicamos que el orden de las filas no es el mismo que el del datset
    _this.VPagination.OnEventGoTo = null;//Evento al cambiar de página
    var UnDataSet = _this.DataSet;//Actulizo DataSet
    //isFilterRecord igual a true, indica que registros ya se encuetran filtrados y no es necesario realizar una busquedad
    //isFilterRecord igual a false, indica que registros se deben filtrar
    if (!isFilterRecord) {//Validamos que isFilterRecord sea igual a false para realizar el filtro
        _this.ItemTreePanel = new Array();
        //-isAllRecord = true, indica que se desea hacer un búsquedad solo con filterSQLDefault(*sentencia linq general).
        //-isAllRecord = false, indica que se desea hacer una búsquedad que viene de un "linq" o un "filtro", por ejemplo los filtros que viene cuando le damos click en la lupa de la columna y nos abre un dialogo de busquedad o cuando hacemos una consulta personalizada.
        if (!isAllRecord) {
            if (typeof (querySQLLinq) == "undefined" || querySQLLinq == null) {// validamos que no sea una sentencia "Linq"
                if (typeof (filter.dateStart) == "undefined") {//Validamos que no sea un filtro de fechas

                    for (var e = 0, i1 = 0, tDataSet = UnDataSet.Records.length; e < tDataSet; e++ , i1++) {//Recorremos los records
                        var subItems = [];
                        var cadena = "" + UnDataSet.Records[e].Fields[Column.IndexDataSourceDefault].Value;//Obtenemos el valor del field
                        cadena = cadena.toUpperCase();//convertimos a mayúsculas
                        var isInsideQuery = true;//variable que indica si nuestro campo esta dentro de en un filtro.
                        if (_this.filterSQLDefault != null) {//Validamos si existe un filtro general
                            var ArrayGrid = new Array();//Creamos un array para convertirlo a formato linq
                            var subItemsArrayGrid = [];
                            for (var ContX = 1; ContX < _this.VclGridCF.Columns.length - 1; ContX++) {//Recorremos las columnas
                                var IndexDataSourceDefault = _this.VclGridCF.Columns[ContX].IndexDataSourceDefault;//Obtemos el indice del field
                                subItemsArrayGrid.push([(_this.DataSet.Records[e].Fields[IndexDataSourceDefault].Value), i]);//agregamos el valor del fiel al array
                            }
                            ArrayGrid.push(subItemsArrayGrid);// agregamos el subitem al array
                            var element = Enumerable.From(ArrayGrid).Where(_this.filterSQLDefault).Select("$").ToArray();//ejecutamos la consulta linq con el _this.filterSQLDefault
                            isInsideQuery = (!(element[0] == [] || element[0] == null))//verificamos si el registro se encuentra en la consulta linq _this.filterSQLDefault.
                        }
                        if (isInsideQuery && cadena.indexOf(('' + filter).toUpperCase()) != -1) {//validamos si el registro no se encuentra en un registro general y que concida con el filtro
                            for (var ContX = 1, t = _this.VclGridCF.Columns.length - 1; ContX < t; ContX++) {
                                var IndexDataSourceDefault = _this.VclGridCF.Columns[ContX].IndexDataSourceDefault;//Obtenemos el indice del field
                                subItems.push(
                                    [(UnDataSet.Records[e].Fields[IndexDataSourceDefault].Value), e]);//Agregamos el valor al sub item
                            }
                            subItems.push("");
                            subItems.push(e);
                            _this.ItemTreePanel.push(subItems);//agregamos el sub-item al _this.ItemTreePanel
                        }
                    }
                }
                else {
                    //Validación para filtros de fechas
                    var dateStart = filter.dateStart;//fecha de inicio
                    var dateEnd = filter.dateEnd;//fecha de Fin
                    if (Column.ColumnStyle == Componet.Properties.TExtrafields_ColumnStyle.Time) {//validamos si el estilo de la columna es time
                        //Convertimos valores de fechas a un formato en el cual podamos validar si una fecha es mayor o igual a otra.
                        var valorFecha = filter.dateStart.split(":");//Creamos las fechas
                        var hour = valorFecha[0];//Valor de hora
                        valorFecha = valorFecha[1].split(" ");//Valor de fecha
                        var minute = valorFecha[0];//Valor de minuto
                        if (valorFecha[1].toUpperCase() == "PM") {//validamos si el formato de fecha e PM
                            hour = parseInt(hour) + 12;//Aumentamos 12 horas a la fecha
                        }
                        var fechaA = new Date();//Creamos una fecha A
                        fechaA.setHours(hour);//Establecemos la hora
                        fechaA.setMinutes(minute);//Establecemos los minuto
                        fechaA.setSeconds(0);////Establecemos los segundos
                        var fechaB = new Date();//Creamos una fecha A
                        valorFecha = filter.dateEnd.split(":");//Establecemos una hora
                        hour = valorFecha[0];//Establecemos la hora
                        valorFecha = valorFecha[1].split(" ");
                        minute = valorFecha[0];//Establecemos minutos
                        if (valorFecha[1].toUpperCase() == "PM") {//validamos si el formato de fecha e PM
                            hour = parseInt(hour) + 12;//Aumentamos 12 horas
                        }
                        fechaB.setHours(hour);//Establecemos hora
                        fechaB.setMinutes(minute);//Establecemos minutos
                        fechaB.setSeconds(59);//Establecemos segundos

                        var fechaRecordTemp = new Date();//Creamos una fecha temporar donde iremos guardando las fechas de los TFiled date
                        /*Debido a que estamos relizamos un filtro en "hh::mm:ss" Asignamos a las fechas un mismo año,mes y día*/
                        fechaA.setFullYear(2018);
                        fechaA.setMonth(1);
                        fechaA.setDate(1);
                        fechaB.setFullYear(2018);
                        fechaB.setMonth(1);
                        fechaB.setDate(1);
                        fechaRecordTemp.setFullYear(2018);
                        fechaRecordTemp.setMonth(1);
                        fechaRecordTemp.setDate(1);
                        /*****************/

                        for (var e = 0, i1 = 0, tDataSet = UnDataSet.Records.length; e < tDataSet; e++ , i1++) {//recorremos el los records
                            var subItems = [];
                            var cadena = UnDataSet.Records[e].Fields[Column.IndexDataSourceDefault].Value;//obtnemos el valor del field
                            var isInsideQuery = true;//variable que indica si nuestro campo esta dentro de en un filtro.
                            if (_this.filterSQLDefault != null) {//Validamos si existe un filtro general
                                var ArrayGrid = new Array();//Creamos un array para convertirlo a formato linq
                                var subItemsArrayGrid = [];
                                for (var ContX = 1; ContX < _this.VclGridCF.Columns.length - 1; ContX++) {//Recorremos las columnas
                                    var IndexDataSourceDefault = _this.VclGridCF.Columns[ContX].IndexDataSourceDefault;//Obtemos el indice del field
                                    subItemsArrayGrid.push([(_this.DataSet.Records[e].Fields[IndexDataSourceDefault].Value), i]);//agregamos el valor del fiel al array
                                }
                                ArrayGrid.push(subItemsArrayGrid);// agregamos el subitem al array
                                var element = Enumerable.From(ArrayGrid).Where(_this.filterSQLDefault).Select("$").ToArray();//ejecutamos la consulta linq con el _this.filterSQLDefault
                                isInsideQuery = (!(element[0] == [] || element[0] == null))//verificamos si el registro se encuentra en la consulta linq _this.filterSQLDefault.
                            }

                            cadena = new Date(cadena);//Convertimos cadena en una clase Date
                            fechaRecordTemp.setHours(cadena.getHours());//obtenemos y establecemos hora
                            fechaRecordTemp.setMinutes(cadena.getMinutes());//obtenemos y establecemos minutos
                            fechaRecordTemp.setSeconds(cadena.getSeconds());//obtenemos y establecemos segundos

                            if (isInsideQuery && fechaRecordTemp >= fechaA && fechaRecordTemp <= fechaB) {//validamos que no se encuentre en un filtro general y queeste entre el rango de fechas
                                for (var ContX = 1, t = _this.VclGridCF.Columns.length - 1; ContX < t; ContX++) {
                                    var IndexDataSourceDefault = _this.VclGridCF.Columns[ContX].IndexDataSourceDefault;//Obtenemos el indice del field
                                    subItems.push(
                                        [(UnDataSet.Records[e].Fields[IndexDataSourceDefault].Value), e]);//Agregamos el valor al sub item
                                }
                                subItems.push("");
                                subItems.push(e);
                                _this.ItemTreePanel.push(subItems);//agregamos el sub-item al _this.ItemTreePanel
                            }
                        }

                    }
                    else {
                        //validamos si el estilo de la columna es none o date
                        var fechaA = new Date(dateStart);//Creamos feha A
                        var fechaB = new Date(dateEnd);//Creamos feha B
                        if (Column.ColumnStyle == Componet.Properties.TExtrafields_ColumnStyle.Date) {//Validamos si es estilo Date
                            //Establecemos los minutos, horas y segundos en valores por defecto.
                            fechaA.setHours(0);//establecemos horas
                            fechaA.setMinutes(0);//establecemos minutos
                            fechaA.setSeconds(0);//establecemos segundos
                            fechaB.setHours(23);//establecemos horas
                            fechaB.setMinutes(59);//establecemos minutos
                            fechaB.setSeconds(59);//establecemos segundos
                        }
                        for (var e = 0, i1 = 0, tDataSet = UnDataSet.Records.length; e < tDataSet; e++ , i1++) {//Recorremos los records
                            var subItems = [];
                            var cadena = UnDataSet.Records[e].Fields[Column.IndexDataSourceDefault].Value;
                            var isInsideQuery = true;//variable que indica si nuestro campo esta dentro de en un filtro.
                            if (_this.filterSQLDefault != null) {//Validamos si existe un filtro general
                                var ArrayGrid = new Array();//Creamos un array para convertirlo a formato linq
                                var subItemsArrayGrid = [];
                                for (var ContX = 1; ContX < _this.VclGridCF.Columns.length - 1; ContX++) {//Recorremos las columnas
                                    var IndexDataSourceDefault = _this.VclGridCF.Columns[ContX].IndexDataSourceDefault;//Obtemos el indice del field
                                    subItemsArrayGrid.push([(_this.DataSet.Records[e].Fields[IndexDataSourceDefault].Value), i]);//agregamos el valor del fiel al array
                                }
                                ArrayGrid.push(subItemsArrayGrid);// agregamos el subitem al array
                                var element = Enumerable.From(ArrayGrid).Where(_this.filterSQLDefault).Select("$").ToArray();//ejecutamos la consulta linq con el _this.filterSQLDefault
                                isInsideQuery = (!(element[0] == [] || element[0] == null))//verificamos si el registro se encuentra en la consulta linq _this.filterSQLDefault.
                            }
                            if (isInsideQuery && cadena >= fechaA && cadena <= fechaB) {
                                for (var ContX = 1, t = _this.VclGridCF.Columns.length - 1; ContX < t; ContX++) {
                                    var IndexDataSourceDefault = _this.VclGridCF.Columns[ContX].IndexDataSourceDefault;//Obtenemos el indice del field
                                    subItems.push(
                                        [(UnDataSet.Records[e].Fields[IndexDataSourceDefault].Value), e]);//Agregamos el valor al sub item
                                }
                                subItems.push("");
                                subItems.push(e);
                                _this.ItemTreePanel.push(subItems);//agregamos el sub-item al _this.ItemTreePanel
                            }
                        }
                    }
                }
            }
            else {
                //Validamos que tengamos una sentencia "Linq" como parametro
                for (var e = 0, i1 = 0, tDataSet = UnDataSet.Records.length; e < tDataSet; e++ , i1++) {//recorremos record
                    var subItems = [];
                    var ArrayGrid = new Array();
                    var subItemsArrayGrid = [];
                    for (var ContX = 1; ContX < _this.VclGridCF.Columns.length - 1; ContX++) {
                        var IndexDataSourceDefault = _this.VclGridCF.Columns[ContX].IndexDataSourceDefault;//Obtenemos el indice del field
                        subItemsArrayGrid.push([(_this.DataSet.Records[e].Fields[IndexDataSourceDefault].Value), i]);//Agregamos el valor al sub item
                    }
                    ArrayGrid.push(subItemsArrayGrid);
                    var isInsideQuery = true;//variable que indica si nuestro campo esta dentro de en un filtro.

                    if (_this.filterSQLDefault != null) {//Validamos si existe un filtro general
                        var element = Enumerable.From(ArrayGrid).Where(querySQLLinq + " && " + _this.filterSQLDefault).Select("$").ToArray();
                    } else {
                        var element = Enumerable.From(ArrayGrid).Where(querySQLLinq).Select("$").ToArray();
                    }
                    isInsideQuery = (!(element[0] == [] || element[0] == null))//verificamos si el registro se encuentra en la consulta linq _this.filterSQLDefault.
                    if (isInsideQuery) {
                        for (var ContX = 1, t = _this.VclGridCF.Columns.length - 1; ContX < t; ContX++) {
                            var IndexDataSourceDefault = _this.VclGridCF.Columns[ContX].IndexDataSourceDefault;//Obtenemos el indice del field
                            subItems.push(
                                [(UnDataSet.Records[e].Fields[IndexDataSourceDefault].Value), e]);//Agregamos el valor al sub item
                        }
                        subItems.push("");
                        subItems.push(e);
                        _this.ItemTreePanel.push(subItems);//agregamos el sub-item al _this.ItemTreePanel
                    }
                }
            }
        }
        else {
            //Para búsquedad solo con filterSQLDefault(*sentencia linq general).
            for (var e = 0, i1 = 0, tDataSet = UnDataSet.Records.length; e < tDataSet; e++ , i1++) {//recorremos records
                var subItems = [];
                var isInsideQuery = true;//variable que indica si nuestro campo esta dentro de en un filtro.
                if (_this.filterSQLDefault != null) {//Validamos si existe un filtro general
                    var ArrayGrid = new Array();//Creamos un array para convertirlo a formato linq
                    var subItemsArrayGrid = [];
                    for (var ContX = 1; ContX < _this.VclGridCF.Columns.length - 1; ContX++) {//Recorremos las columnas
                        var IndexDataSourceDefault = _this.VclGridCF.Columns[ContX].IndexDataSourceDefault;//Obtemos el indice del field
                        subItemsArrayGrid.push([(_this.DataSet.Records[e].Fields[IndexDataSourceDefault].Value), i]);//agregamos el valor del fiel al array
                    }
                    ArrayGrid.push(subItemsArrayGrid);// agregamos el subitem al array
                    var element = Enumerable.From(ArrayGrid).Where(_this.filterSQLDefault).Select("$").ToArray();//ejecutamos la consulta linq con el _this.filterSQLDefault
                    isInsideQuery = (!(element[0] == [] || element[0] == null))//verificamos si el registro se encuentra en la consulta linq _this.filterSQLDefault.
                }
                if (isInsideQuery) {
                    for (var ContX = 1, t = _this.VclGridCF.Columns.length - 1; ContX < t; ContX++) {
                        var IndexDataSourceDefault = _this.VclGridCF.Columns[ContX].IndexDataSourceDefault;//Obtenemos el indice del field
                        subItems.push(
                            [(UnDataSet.Records[e].Fields[IndexDataSourceDefault].Value), e]);//Agregamos el valor al sub item
                    }
                    subItems.push("");
                    subItems.push(e);
                    _this.ItemTreePanel.push(subItems);//agregamos el sub-item al _this.ItemTreePanel
                }

            }
        }
    }
    _this.VPagination.NumeroFilas = _this.ItemTreePanel.length;//Actulizamos el número de fila en la paginación.
    _this.VPagination.IndexPage = 1;//Establecemos la paginación en 1
    _this.filasMostradas = _this.VPagination.FilasMostradas;//Filas mostradas en el grid
    _this.VPagination.Refresch();//Refrescamos la paginación
    _this.VPagination.OnEventGoTo = function (indicePage) {//Funcion para cambiar de página
        _this.filasMostradas = _this.VPagination.FilasMostradas;//Filas mostrada
        _this.DataSet.Status = SysCfg.MemTable.Properties.TStatus.None;//Status de DataSet en none
        var indiceActual = indicePage - 1;//Indice actual es el indice de la página menos 1(Debido a que la paginación se enumera desde 1 y no desde cero).
        _this.VclGridCF.ClearAllRows();//Limpiamos las filas
        _this.VclGridCF._FocusedRowHandle = null;//Establecemos fila focus en null
        var parentBody = _this.VclGridCF.Body.parentNode;//Obtnemos el parent node
        parentBody.removeChild(_this.VclGridCF.Body);//removemos el body
        for (var e = (_this.filasMostradas * indiceActual), indexRow = 0, tDataSet = ((indiceActual + 1) * _this.filasMostradas), cantidadRecord = _this.ItemTreePanel.length; e < tDataSet && e < cantidadRecord; e++ , indexRow++) {//recorremos el itemTreepanel el cual tiene los registros filtrados
            var IndiceReal = _this.ItemTreePanel[e][_this.nameColumns.length + 1];//Obtnemos el indice real del record en el DataSet
            var NewRow = new TVclRowCF(); //CREA UN ROW PARA AGREGAR AL GRIDCONTROL 
            var CellCheck = new TVclCellCF(); //CREA UNA CELDA PARA QUE SE AGREGA AL ROW
            CellCheck.Value = _this.SetSelectedRows;//obtenemos si las Filas son cheakeadas o no
            CellCheck.IndexColumn = ContX; // ASIGNA O EXTRAE EL INDEX DE LA COLUMNA 
            CellCheck.IndexRow = e; // ASIGNA O EXTRAE EL INDEX DEL ROW 
            NewRow.AddCell(CellCheck); // AGREGA O EXTRAE LA CELDA AL ROW
            for (var ContX = 1, t = _this.VclGridCF.Columns.length - 1; ContX < t; ContX++) {
                var IndexDataSourceDefault = _this.VclGridCF.Columns[ContX].IndexDataSourceDefault;
                var Cell0 = new TVclCellCF();
                Cell0.CellColor = "";//Color de celda
                Cell0._Value = _this.DataSet.Records[IndiceReal].Fields[IndexDataSourceDefault].Value; // ASIGNA O EXTRAE EL VALOR QUE SE MOSTRARA EN LA CELDA DEL GRIDCONTROL 
                Cell0.IndexColumn = ContX; // ASIGNA O EXTRAE EL INDEX DE LA COLUMNA 
                Cell0.IndexRow = e;  //Indice de la fila en ItemTreePanel 
                NewRow.AddCell(Cell0); // AGREGA O EXTRAE LA CELDA AL ROW
            }
            //**Agrego celda para botones
            var CellButton = new TVclCellCF();
            CellButton.Value = "";//valor de celda
            CellButton.IndexColumn = _this.DataSet.FieldCount;//Indice de la columna
            CellButton.IndexRow = e;//Indice de la fila en ItemTreePanel
            //CellButton.Control = new TVclStackPanel(CellButton.This, "1", 1, [[12]]);
            _this.DibujarCellButton(true, NewRow, CellButton);
            NewRow.AddCell(CellButton);
            NewRow.onclick = function (NewRow) { // ASIGNA UN EVENTO CUANDO SE HACE CLICK EN EL ROW DEL GRIDCONTROL
                _this.RowClick(NewRow);
            }
            NewRow.ondblclick = function (NewRow) {
                _this.RowDblClick(NewRow);
            }
            _this.VclGridCF.AddRow(NewRow);//Agregamos fila
            NewRow.Index = indexRow;//!Indice de la fila en el grid
            NewRow.IndexDataSet = IndiceReal;//!Indice de la fila en el Dataset
            NewRow.IndexTreeGrid = e;//Indice de la fila en ItemTreePanel
        }
        parentBody.appendChild(_this.VclGridCF.Body);//Agregamos body al table
        _this.VclGridCF.ContracAllColumns();//Contraemos el ancho de las columnas

        //_this.isEventChangePaginationScroll igual True: indica si estamos cambiando la paginación desde TPagination
        //_this.isEventChangePaginationScroll igual False: indica que se cambio de página por que se eliminaron filas
        if (_this.isEventChangePaginationScroll) { //_this.isEventChangePaginationScroll igual True: indica si estamos cambiando la paginación desde TPagination
            _this.VclGridCF.IndexRow = 0;//Al cambiar de página el indice de la fila es igual a 0
            if (_this.VclGridCF.FocusedRowHandle != null) {//Validamos si el focus es distinto que null
                _this.IndexRowGridPorScroll = _this.VclGridCF.FocusedRowHandle.Index;//Indice de la fila focus.
                _this.DataSet._Index = -1;//Establecemos el indice en -1 para asegurarnos que el indice en el data Set sea distinto y poder ejecutar el refresh record set
                _this.DataSet.SetIndex(_this.VclGridCF.FocusedRowHandle.IndexDataSet);//Cambiamos el indice y se refresca el record set
            }
        }
        else {
            //_this.isEventChangePaginationScroll igual True: indica si estamos cambiando la paginación desde TPagination
            if (_this.VclGridCF.FocusedRowHandle != null) {
                _this.isEventChangePaginationScroll = true;//Establecemos la variable en true
                _this.VclGridCF.IndexRow = _this.VclGridCF.Rows.length - 1;//Como se elimino una fila el FocusedRowHandle hacer la última Row
                _this.IndexRowGridPorScroll = _this.VclGridCF.FocusedRowHandle.Index;//Indice de la fila focus.
                _this.DataSet._Index = -1;//Establecemos el indice en -1 para asegurarnos que el indice en el data Set sea distinto y poder ejecutar el refresh record set
                _this.DataSet.SetIndex(_this.VclGridCF.FocusedRowHandle.IndexDataSet);//Cambiamos el indice y se refresca el record set
            }
        }
    }
    var columnaDeIndexDataSorce = _this.nameColumns.length + 1;
    _this.VclGridCF.ClearAllRows();
    _this.VclGridCF._FocusedRowHandle = null;
    if (_this.ItemTreePanel.length > 0) {
        var parentBody = _this.VclGridCF.Body.parentNode;
        parentBody.removeChild(_this.VclGridCF.Body);
        for (var e = 0, tDataSet = _this.filasMostradas, cantidadRecord = _this.ItemTreePanel.length; e < tDataSet && e < cantidadRecord; e++) {
            var IndiceReal = _this.ItemTreePanel[e][_this.nameColumns.length + 1];
            var NewRow = new TVclRowCF(); //CREA UN ROW PARA AGREGAR AL GRIDCONTROL 
            var CellCheck = new TVclCellCF(); //CREA UNA CELDA PARA QUE SE AGREGA AL ROW
            CellCheck.Value = _this.SetSelectedRows;
            CellCheck.IndexColumn = ContX; // ASIGNA O EXTRAE EL INDEX DE LA COLUMNA 
            CellCheck.IndexRow = e; // ASIGNA O EXTRAE EL INDEX DEL ROW 
            NewRow.AddCell(CellCheck); // AGREGA O EXTRAE LA CELDA AL ROW
            for (var ContX = 1, t = _this.VclGridCF.Columns.length - 1; ContX < t; ContX++) {
                var IndexDataSourceDefault = _this.VclGridCF.Columns[ContX].IndexDataSourceDefault;
                var Cell0 = new TVclCellCF();
                Cell0.CellColor = "";
                Cell0._Value = _this.DataSet.Records[IndiceReal].Fields[IndexDataSourceDefault].Value; // ASIGNA O EXTRAE EL VALOR QUE SE MOSTRARA EN LA CELDA DEL GRIDCONTROL 
                Cell0.IndexColumn = ContX; // ASIGNA O EXTRAE EL INDEX DE LA COLUMNA 
                Cell0.IndexRow = e; // ASIGNA O EXTRAE EL INDEX DEL ROW 
                NewRow.AddCell(Cell0); // AGREGA O EXTRAE LA CELDA AL ROW
            }
            //**Agrego celda para botones
            var CellButton = new TVclCellCF();
            CellButton.Value = "";
            CellButton.IndexColumn = _this.DataSet.FieldCount;
            CellButton.IndexRow = e;
            //CellButton.Control = new TVclStackPanel(CellButton.This, "1", 1, [[12]]);
            _this.DibujarCellButton(true, NewRow, CellButton);
            NewRow.AddCell(CellButton);
            NewRow.onclick = function (NewRow) { // ASIGNA UN EVENTO CUANDO SE HACE CLICK EN EL ROW DEL GRIDCONTROL

                _this.RowClick(NewRow);
            }
            NewRow.ondblclick = function (NewRow) {
                _this.RowDblClick(NewRow);
            }
            _this.VclGridCF.AddRow(NewRow);
            NewRow.Index = e;
            NewRow.IndexDataSet = IndiceReal;
            NewRow.IndexTreeGrid = e;
        }
        parentBody.appendChild(_this.VclGridCF.Body);
        _this.IndexRowGridPorScroll = 0;
        _this.VclGridCF.IndexRow = 0;
        /*Solucionar dataset*/
        _this.DataSet._Index = -1;
        _this.DataSet.SetIndex(_this.VclGridCF.FocusedRowHandle.IndexDataSet);
        _this.VclGridCF.ContracAllColumns();
    }
    else {
        //No existen registros
        _this.SetToolBarRowEmpty();
    }
    _this.VclTreeGridControl.Init(_this.VclGridCF, _this.nameColumns, _this.ItemTreePanel, false, "asc", _this.DataSet);
}
Componet.GridCF.TGridCFView.prototype.SetRecordsPaginationFilterItemTreePanel = function (isAllRecord, Column, filter, querySQLLinq, isOrder) {
    var _this = this.TParent();
    var UnDataSet = _this.DataSet;
    if (!(isOrder == true)) {
        _this.ItemTreePanel = new Array();
        if (!isAllRecord) {
            if (typeof (querySQLLinq) == "undefined" || querySQLLinq == null) {
                if (typeof (filter.dateStart) == "undefined") {
                    for (var e = 0, i1 = 0, tDataSet = UnDataSet.Records.length; e < tDataSet; e++ , i1++) {
                        var subItems = [];
                        var cadena = "" + UnDataSet.Records[e].Fields[Column.IndexDataSourceDefault].Value;
                        cadena = cadena.toUpperCase();
                        var isInsideQuery = true;
                        if (_this.filterSQLDefault != null) {
                            var ArrayGrid = new Array();
                            var subItemsArrayGrid = [];
                            for (var ContX = 1; ContX < _this.VclGridCF.Columns.length - 1; ContX++) {
                                var IndexDataSourceDefault = _this.VclGridCF.Columns[ContX].IndexDataSourceDefault;
                                subItemsArrayGrid.push([(_this.DataSet.Records[e].Fields[IndexDataSourceDefault].Value), i]);
                            }
                            ArrayGrid.push(subItemsArrayGrid);
                            var element = Enumerable.From(ArrayGrid).Where(_this.filterSQLDefault).Select("$").ToArray();
                            isInsideQuery = (!(element[0] == [] || element[0] == null))
                        }
                        if (isInsideQuery && cadena.indexOf(('' + filter).toUpperCase()) != -1) {
                            for (var ContX = 1, t = _this.VclGridCF.Columns.length - 1; ContX < t; ContX++) {
                                var IndexDataSourceDefault = _this.VclGridCF.Columns[ContX].IndexDataSourceDefault;
                                subItems.push(
                                    [(UnDataSet.Records[e].Fields[IndexDataSourceDefault].Value), e]);//Valor, Orden
                            }
                            subItems.push("");
                            subItems.push(e);
                            _this.ItemTreePanel.push(subItems);
                        }
                    }
                }
                else {
                    var dateStart = filter.dateStart;
                    var dateEnd = filter.dateEnd;
                    var fechaA = new Date(filter.dateStart);
                    var fechaB = new Date(filter.dateEnd);
                    for (var e = 0, i1 = 0, tDataSet = UnDataSet.Records.length; e < tDataSet; e++ , i1++) {
                        var subItems = [];
                        var cadena = UnDataSet.Records[e].Fields[Column.IndexDataSourceDefault].Value;
                        // cadena = new Date(dateEnd[0] + "/" + dateEnd[1] + "/" + dateEnd[2]);
                        var isInsideQuery = true;
                        if (_this.filterSQLDefault != null) {
                            var ArrayGrid = new Array();
                            var subItemsArrayGrid = [];
                            for (var ContX = 1; ContX < _this.VclGridCF.Columns.length - 1; ContX++) {
                                var IndexDataSourceDefault = _this.VclGridCF.Columns[ContX].IndexDataSourceDefault;
                                subItemsArrayGrid.push([(_this.DataSet.Records[e].Fields[IndexDataSourceDefault].Value), i]);
                            }
                            ArrayGrid.push(subItemsArrayGrid);
                            var element = Enumerable.From(ArrayGrid).Where(_this.filterSQLDefault).Select("$").ToArray();
                            isInsideQuery = (!(element[0] == [] || element[0] == null))
                        }


                        if (isInsideQuery && cadena >= fechaA && cadena <= fechaB) {
                            for (var ContX = 1, t = _this.VclGridCF.Columns.length - 1; ContX < t; ContX++) {
                                var IndexDataSourceDefault = _this.VclGridCF.Columns[ContX].IndexDataSourceDefault;
                                subItems.push(
                                    [(UnDataSet.Records[e].Fields[IndexDataSourceDefault].Value), e]);//Valor, Orden
                            }
                            subItems.push("");
                            subItems.push(e);
                            _this.ItemTreePanel.push(subItems);
                        }
                    }
                }
            }
            else {
                for (var e = 0, i1 = 0, tDataSet = UnDataSet.Records.length; e < tDataSet; e++ , i1++) {
                    var subItems = [];
                    var ArrayGrid = new Array();
                    var subItemsArrayGrid = [];
                    for (var ContX = 1; ContX < _this.VclGridCF.Columns.length - 1; ContX++) {
                        var IndexDataSourceDefault = _this.VclGridCF.Columns[ContX].IndexDataSourceDefault;
                        subItemsArrayGrid.push([(_this.DataSet.Records[e].Fields[IndexDataSourceDefault].Value), i]);
                    }
                    ArrayGrid.push(subItemsArrayGrid);

                    var isInsideQuery = true;
                    if (_this.filterSQLDefault != null) {
                        var element = Enumerable.From(ArrayGrid).Where(querySQLLinq + " && " + _this.filterSQLDefault).Select("$").ToArray();
                    } else {
                        var element = Enumerable.From(ArrayGrid).Where(querySQLLinq).Select("$").ToArray();
                    }

                    if (!(element[0] == [] || element[0] == null)) {
                        for (var ContX = 1, t = _this.VclGridCF.Columns.length - 1; ContX < t; ContX++) {
                            var IndexDataSourceDefault = _this.VclGridCF.Columns[ContX].IndexDataSourceDefault;
                            subItems.push(
                                [(UnDataSet.Records[e].Fields[IndexDataSourceDefault].Value), e]);//Valor, Orden
                        }
                        subItems.push("");
                        subItems.push(e);
                        _this.ItemTreePanel.push(subItems);
                    }

                }
            }

        }
        else {
            for (var e = 0, i1 = 0, tDataSet = UnDataSet.Records.length; e < tDataSet; e++ , i1++) {
                var subItems = [];
                var isInsideQuery = true;
                if (_this.filterSQLDefault != null) {
                    var ArrayGrid = new Array();
                    var subItemsArrayGrid = [];
                    for (var ContX = 1; ContX < _this.VclGridCF.Columns.length - 1; ContX++) {
                        var IndexDataSourceDefault = _this.VclGridCF.Columns[ContX].IndexDataSourceDefault;
                        subItemsArrayGrid.push([(_this.DataSet.Records[e].Fields[IndexDataSourceDefault].Value), i]);
                    }
                    ArrayGrid.push(subItemsArrayGrid);
                    var element = Enumerable.From(ArrayGrid).Where(_this.filterSQLDefault).Select("$").ToArray();
                    isInsideQuery = (!(element[0] == [] || element[0] == null))
                }
                if (isInsideQuery) {
                    for (var ContX = 1, t = _this.VclGridCF.Columns.length - 1; ContX < t; ContX++) {
                        var IndexDataSourceDefault = _this.VclGridCF.Columns[ContX].IndexDataSourceDefault;
                        subItems.push(
                            [(UnDataSet.Records[e].Fields[IndexDataSourceDefault].Value), e]);//Valor, Orden
                    }
                    subItems.push("");
                    subItems.push(e);
                    _this.ItemTreePanel.push(subItems);
                }


            }
        }
    }
    if (_this.ItemTreePanel.length == 0) {
        _this.SetToolBarRowEmpty();
    }
}
/*Función para volver a generar el ItemTreePanel despúes de expander una agrupación, esta función es llamada desde OnOpenTreeGridGroup*/
Componet.GridCF.TGridCFView.prototype.SetRecordsFilterItemTreePanel = function () {
    var _this = this.TParent();
    var UnDataSet = _this.DataSet;
    _this.ItemTreePanel = new Array();
    for (var e = 0, i1 = 0, tDataSet = UnDataSet.Records.length; e < tDataSet; e++ , i1++) {
        var subItems = [];
        var isInsideQuery = true;
        if (_this.filterSQLDefault != null) {
            var ArrayGrid = new Array();
            var subItemsArrayGrid = [];
            for (var ContX = 1; ContX < _this.VclGridCF.Columns.length - 1; ContX++) {
                var IndexDataSourceDefault = _this.VclGridCF.Columns[ContX].IndexDataSourceDefault;
                subItemsArrayGrid.push([(_this.DataSet.Records[e].Fields[IndexDataSourceDefault].Value), i]);
            }
            ArrayGrid.push(subItemsArrayGrid);
            var element = Enumerable.From(ArrayGrid).Where(_this.filterSQLDefault).Select("$").ToArray();
            isInsideQuery = (!(element[0] == [] || element[0] == null))
        }
        if (isInsideQuery) {
            for (var ContX = 1, t = _this.VclGridCF.Columns.length - 1; ContX < t; ContX++) {
                var IndexDataSourceDefault = _this.VclGridCF.Columns[ContX].IndexDataSourceDefault;
                subItems.push(
                    [(UnDataSet.Records[e].Fields[IndexDataSourceDefault].Value), e]);//Valor, Orden
            }
            subItems.push("");
            subItems.push(e);
            _this.ItemTreePanel.push(subItems);
        }
    }
    if (_this.ItemTreePanel.length == 0) {
        _this.SetToolBarRowEmpty();
    }
}
/*Función construir filtro al expandir una agrupación del TreeGrid*/
Componet.GridCF.TGridCFView.prototype.openTreeGridPaginationFilterItemTreePanel = function (inItemTreePanel) {
    var _this = this.TParent();
    var UnDataSet = _this.DataSet;
    _this.VPagination.LinkImgGruopOpen.style.display = "";//Ocultamos la flecha que se crea en el row del treeGrid que estamos expandiendo.
    _this.ItemTreePanel = inItemTreePanel;//Obtener el itemTreePanel de la agrupación
    _this.VclGridCF.EnablePanelGroup = false;//Ocultamos el panel de la agrupación
    _this.GridCFToolbar.ButtonImg_ColumnGroupBar.Cheked = false;//El bar de la agrupación la desmarcamos
    _this.VclTreeGridControl.inTreeGrid = false;//Indicamos que ya no estamos en el TreeGrid
    _this.GridCFToolbar.ButtonImg_ExpanderBar.Visible = false;//Ocultamos el bar expander de la agrupación.
    _this.GridCFToolbar.ButtonImg_ContraerBar.Visible = false;//Ocultamos el bar contraer de la agrupación.
    _this.GridCFToolbar.ButtonImg_NavigatorToolsBar_ChekedClickDown = true;//Desmarcamos las flechas de navegación.
    $(_this.VclStackPanelContentMemTable.Row.This).show("highlight", {}, 500);//Damos efecto para mostrar el div del Grid
    if (_this.VclTreeGridControl.GridTreeGridControl != null) {//Validamos que el grid que crea el TreeGrid sea distinto de nulo para eliminarla.
        _this.VclTreeGridControl.GridTreeGridControl.ClearAllRows();//Eliminamos la tabla que se crea en el TreeGrid.
    }
    _this.IndiceLogico = 0;//Indicamos que el orden de los registro no es igual al orden del DataSet
    _this.VPagination.OnEventGoTo = null;//Evento al cambiar de paginación igual a null.
    _this.VPagination.NumeroFilas = _this.ItemTreePanel.length;//Actualizamos el número de filas.
    _this.VPagination.IndexPage = 1;//Actulizamos el indice de página
    _this.filasMostradas = _this.VPagination.FilasMostradas;//Actualizamos la filas que se muestran.
    _this.VPagination.Refresch();//Actualizamos la paginación
    _this.VPagination.OnEventGoTo = function (indicePage) {
        //Creamos la función que se llama al paginar
        _this.filasMostradas = _this.VPagination.FilasMostradas;
        _this.DataSet.Status = SysCfg.MemTable.Properties.TStatus.None;
        var indiceActual = indicePage - 1;
        _this.VclGridCF.ClearAllRows();
        _this.VclGridCF._FocusedRowHandle = null;
        var parentBody = _this.VclGridCF.Body.parentNode;
        parentBody.removeChild(_this.VclGridCF.Body);
        for (var e = (_this.filasMostradas * indiceActual), indexRow = 0, tDataSet = ((indiceActual + 1) * _this.filasMostradas), cantidadRecord = _this.ItemTreePanel.length; e < tDataSet && e < cantidadRecord; e++ , indexRow++) {
            var IndiceReal = _this.ItemTreePanel[e][_this.nameColumns.length + 1];
            var NewRow = new TVclRowCF(); //CREA UN ROW PARA AGREGAR AL GRIDCONTROL 
            var CellCheck = new TVclCellCF(); //CREA UNA CELDA PARA QUE SE AGREGA AL ROW
            CellCheck.Value = _this.SetSelectedRows;
            CellCheck.IndexColumn = ContX; // ASIGNA O EXTRAE EL INDEX DE LA COLUMNA 
            CellCheck.IndexRow = e; // ASIGNA O EXTRAE EL INDEX DEL ROW 
            NewRow.AddCell(CellCheck); // AGREGA O EXTRAE LA CELDA AL ROW

            for (var ContX = 1, t = _this.VclGridCF.Columns.length - 1; ContX < t; ContX++) {

                var IndexDataSourceDefault = _this.VclGridCF.Columns[ContX].IndexDataSourceDefault;
                var Cell0 = new TVclCellCF();
                Cell0.CellColor = "";
                Cell0.Value = _this.DataSet.Records[IndiceReal].Fields[IndexDataSourceDefault].Value; // ASIGNA O EXTRAE EL VALOR QUE SE MOSTRARA EN LA CELDA DEL GRIDCONTROL 
                Cell0.IndexColumn = ContX; // ASIGNA O EXTRAE EL INDEX DE LA COLUMNA 
                Cell0.IndexRow = e; // ASIGNA O EXTRAE EL INDEX DEL ROW 
                NewRow.AddCell(Cell0); // AGREGA O EXTRAE LA CELDA AL ROW
            }
            //**Agrego celda para botones
            var CellButton = new TVclCellCF();
            CellButton.Value = "";
            CellButton.IndexColumn = _this.DataSet.FieldCount;
            CellButton.IndexRow = e;
            //CellButton.Control = new TVclStackPanel(CellButton.This, "1", 1, [[12]]);
            _this.DibujarCellButton(true, NewRow, CellButton);
            NewRow.AddCell(CellButton);
            NewRow.onclick = function (NewRow) { // ASIGNA UN EVENTO CUANDO SE HACE CLICK EN EL ROW DEL GRIDCONTROL

                _this.RowClick(NewRow);
            }
            NewRow.ondblclick = function (NewRow) {
                _this.RowDblClick(NewRow);
            }
            _this.VclGridCF.AddRow(NewRow);
            NewRow.Index = indexRow;
            NewRow.IndexDataSet = IndiceReal;
            NewRow.IndexTreeGrid = e;
        }
        parentBody.appendChild(_this.VclGridCF.Body);
        _this.VclGridCF.ContracAllColumns();
        if (_this.isEventChangePaginationScroll) {
            _this.VclGridCF.IndexRow = 0;
            if (_this.VclGridCF.FocusedRowHandle != null) {
                _this.IndexRowGridPorScroll = _this.VclGridCF.FocusedRowHandle.Index;
                _this.DataSet.SetIndex(_this.VclGridCF.FocusedRowHandle.IndexDataSet);
            }
        } else {
            if (_this.VclGridCF.FocusedRowHandle != null) {
                _this.isEventChangePaginationScroll = true;
                _this.VclGridCF.IndexRow = _this.VclGridCF.Rows.length - 1;
                _this.IndexRowGridPorScroll = _this.VclGridCF.FocusedRowHandle.Index;
                _this.DataSet.SetIndex(_this.VclGridCF.FocusedRowHandle.IndexDataSet);
            }
        }
    }
    var columnaDeIndexDataSorce = _this.nameColumns.length + 1;
    _this.VclGridCF.ClearAllRows();
    _this.VclGridCF._FocusedRowHandle = null;
    if (_this.ItemTreePanel.length > 0) {
        var parentBody = _this.VclGridCF.Body.parentNode;
        parentBody.removeChild(_this.VclGridCF.Body);
        for (var e = 0, tDataSet = _this.filasMostradas, cantidadRecord = _this.ItemTreePanel.length; e < tDataSet && e < cantidadRecord; e++) {
            var IndiceReal = _this.ItemTreePanel[e][_this.nameColumns.length + 1];
            var NewRow = new TVclRowCF(); //CREA UN ROW PARA AGREGAR AL GRIDCONTROL 
            var CellCheck = new TVclCellCF(); //CREA UNA CELDA PARA QUE SE AGREGA AL ROW
            CellCheck.Value = _this.SetSelectedRows;
            CellCheck.IndexColumn = ContX; // ASIGNA O EXTRAE EL INDEX DE LA COLUMNA 
            CellCheck.IndexRow = e; // ASIGNA O EXTRAE EL INDEX DEL ROW 
            NewRow.AddCell(CellCheck); // AGREGA O EXTRAE LA CELDA AL ROW

            for (var ContX = 1, t = _this.VclGridCF.Columns.length - 1; ContX < t; ContX++) {

                var IndexDataSourceDefault = _this.VclGridCF.Columns[ContX].IndexDataSourceDefault;
                var Cell0 = new TVclCellCF();
                Cell0.CellColor = "";
                Cell0.Value = _this.DataSet.Records[IndiceReal].Fields[IndexDataSourceDefault].Value; // ASIGNA O EXTRAE EL VALOR QUE SE MOSTRARA EN LA CELDA DEL GRIDCONTROL 
                Cell0.IndexColumn = ContX; // ASIGNA O EXTRAE EL INDEX DE LA COLUMNA 
                Cell0.IndexRow = e; // ASIGNA O EXTRAE EL INDEX DEL ROW 
                NewRow.AddCell(Cell0); // AGREGA O EXTRAE LA CELDA AL ROW
            }
            //**Agrego celda para botones
            var CellButton = new TVclCellCF();
            CellButton.Value = "";
            CellButton.IndexColumn = _this.DataSet.FieldCount;
            CellButton.IndexRow = e;
            //CellButton.Control = new TVclStackPanel(CellButton.This, "1", 1, [[12]]);
            _this.DibujarCellButton(true, NewRow, CellButton);
            NewRow.AddCell(CellButton);
            NewRow.onclick = function (NewRow) { // ASIGNA UN EVENTO CUANDO SE HACE CLICK EN EL ROW DEL GRIDCONTROL

                _this.RowClick(NewRow);
            }
            NewRow.ondblclick = function (NewRow) {
                _this.RowDblClick(NewRow);
            }
            _this.VclGridCF.AddRow(NewRow);
            NewRow.Index = e;
            NewRow.IndexDataSet = IndiceReal;
            NewRow.IndexTreeGrid = e;
        }
        parentBody.appendChild(_this.VclGridCF.Body);
        _this.IndexRowGridPorScroll = 0;
        _this.VclGridCF.IndexRow = 0;
        _this.DataSet._Index = -1;
        _this.DataSet.SetIndex(_this.VclGridCF.FocusedRowHandle.IndexDataSet);
        _this.VclGridCF.ContracAllColumns();

    } else {
        _this.SetToolBarRowEmpty();
    }
}
/*Función reiniciar la carga de registros, esta función se utiliza cuando desmarcamos el filtro general (Filtro linq)*/
Componet.GridCF.TGridCFView.prototype.restartDataRecords = function () {
    var _this = this.TParent();
    var tempFilterSQLDefault = _this.filterSQLDefault;//Guardamos el filtro general para no perder su valor
    if (_this.filterSQLDefault != null && !_this.VclListBoxCustomFilter.ListBoxItems[0].Checked) {//validamos que exista el filtro general y que el listbox este desmarcado.
        _this.filterSQLDefault = null;//Colocamos el filtro general con nulo
    }
    _this.VclListBoxCustomFilter.Checked = true;//marcamos el listbox
    _this.Information.Init(_this.nameColumns.length, _this.DivitionColumnInformation);//Iniciamos el detalle del grid
    if (_this.IsPagination) {//Para paginación
        _this.VPagination.IndexPage = 1;//colocamos la paginación en 1
        _this.loadDataSetRecordsPaginationFilter(true, null, null, null, false);//cargamos la registros en paginación.
    }
    else {//Para scroll
        _this.loadDataSetRecordsScrollFilter(true, null, null, null, false);//cargamos la registros en scroll.
    }
    if ("" + _this.ColumnaSelecionada != "" && "" + _this.ColumnaSelecionada != "null" && _this.ColumnaSelecionada + "" != "") {//validamos si una columa esta ordenada para quitarle el estilo de orden
        _this.VclGridCF.Order = "ninguno";
        _this.VclGridCF.Columns[_this.ColumnaSelecionada].IsColumnSelectOrder = null;//Inidicamo la columna que no esta ordenada
        _this.VclGridCF.Columns[_this.ColumnaSelecionada].CaptionOrder = _this.VclGridCF.Columns[_this.ColumnaSelecionada].Caption;//actualizamos su caption
    }
    _this.filterSQLDefault = tempFilterSQLDefault;//Recuperamos el valor del filtro general.
}
/*Función construir filas*/
Componet.GridCF.TGridCFView.prototype.BuilVclRow = function (Record, indiceRow) {
    var _this = this.TParent();
    var NewRowRecordGrid = new TVclRowCF();
    for (var ContX = 0; ContX < _this.VclGridCF.Columns.length - 1; ContX++) {
        if (ContX == 0) {
            var Cell0 = new TVclCellCF();
            Cell0.Value = true;
            Cell0.IndexColumn = ContX;
            Cell0.IndexRow = indiceRow;
            NewRowRecordGrid.AddCell(Cell0);
        } else {
            var Cell0 = new TVclCellCF();
            Cell0.Value = Record.Fields[_this.VclGridCF.Columns[ContX].IndexDataSourceDefault].Value;
            Cell0.IndexColumn = ContX;
            Cell0.IndexRow = indiceRow;
            NewRowRecordGrid.AddCell(Cell0);
        }
    }
    var CellButtonNewRow = new TVclCellCF();
    CellButtonNewRow.Value = "";
    CellButtonNewRow.IndexColumn = _this.DataSet.FieldCount;
    CellButtonNewRow.IndexRow = indiceRow;
    //CellButtonNewRow.Control = new TVclStackPanel(CellButtonNewRow.This, "1", 1, [[12]]);
    _this.DibujarCellButton(true, NewRowRecordGrid, CellButtonNewRow);
    NewRowRecordGrid.onclick = function (NewRowRecordGrid) { // ASIGNA UN EVENTO CUANDO SE HACE CLICK EN EL ROW DEL GRIDCONTROL
        _this.RowClick(NewRowRecordGrid);
    }
    NewRowRecordGrid.ondblclick = function (NewRow) {
        _this.RowDblClick(NewRow);
    }
    return NewRowRecordGrid;
}
/*Función que devuelve la lista de columnas del grid*/
Componet.GridCF.TGridCFView.prototype.GetNames = function () {
    var NameColumns = new Array();
    var _this = this.TParent();
    if (_this.VclGridCF.Columns.length > 0) {
        for (var i = 0; i < _this.VclGridCF.Columns.length; i++) {
            if (!_this.VclGridCF.Columns[i].IsOption && !_this.VclGridCF.Columns[i].IsOptionCheck) {
                NameColumns.push(_this.VclGridCF.Columns[i].Name);
            }
        }
    }
    return NameColumns;
}
/*Función para validar si el navegador es IExplorer */
Componet.GridCF.TGridCFView.prototype.detectIE = function () {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }
    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }
    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
        // Edge (IE 12+) => return version number
        return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }
    return false;
}
/*Función para construir una lista json del grid y poder realizar la exportación a pdf*/
Componet.GridCF.TGridCFView.prototype.getGridFormatJSON = function (arrayIndexColumn) {
    var _this = this.TParent();
    var array = [];
    var arrayColumns = [];
    var indexColumnOption = null;
    var listColumn = [];
    if (arrayIndexColumn === undefined) {
        listColumn = _this.VclGridCF.Columns;
    }
    else {
        for (var j = 0, t = arrayIndexColumn.length; j < t; j++) {
            if (_this.VclGridCF.Columns[arrayIndexColumn[j]].IsOption == false && _this.VclGridCF.Columns[arrayIndexColumn[j]].IsOptionCheck == false) {
                listColumn.push(_this.VclGridCF.Columns[arrayIndexColumn[j]]);
            }
        }
    }
    if (listColumn != null) {
        for (var j = 0, t = listColumn.length; j < t; j++) {
            arrayColumns.push(listColumn[j].Name);
        }
    }
    if (_this.ItemTreePanel != null && listColumn != null) {
        for (var i = 0, t = _this.ItemTreePanel.length; i < t; i++) {
            var data = [];
            var IndiceReal = _this.ItemTreePanel[i][_this.nameColumns.length + 1];
            for (var j = 0, t1 = listColumn.length; j < t1; j++) {
                var IndexDataSourceDefault = listColumn[j].IndexDataSourceDefault;
                var valueData = _this.DataSet.Records[IndiceReal].Fields[IndexDataSourceDefault].Value;
                if (listColumn[j].DataType == SysCfg.DB.Properties.TDataType.DateTime) {
                    switch (listColumn[j].ColumnStyle) {
                        case Componet.Properties.TExtrafields_ColumnStyle.None:
                            if (valueData != null && valueData != "" && valueData != "Mon Jan 01 0001 00:00:00 GMT-0800 (Pacific Standard Time)") {
                                var fecha = new Date(valueData);
                                valueData = SysCfg.DateTimeMethods.ToDateString(fecha);
                            } else {
                                valueData = "DD/MM/YY hh:mm";
                            }
                            break;
                        case Componet.Properties.TExtrafields_ColumnStyle.Date:
                            if (valueData != null && valueData != "" && valueData != "Mon Jan 01 0001 00:00:00 GMT-0800 (Pacific Standard Time)") {
                                var fecha = new Date(valueData);
                                valueData = SysCfg.DateTimeMethods.ToDateString(fecha);
                            } else {
                                valueData = "DD/MM/YY";
                            }
                            break;
                        case Componet.Properties.TExtrafields_ColumnStyle.Time:
                            if (valueData != null && valueData != "" && valueData != "Mon Jan 01 0001 00:00:00 GMT-0800 (Pacific Standard Time)") {
                                var fecha = new Date(valueData);
                                valueData = SysCfg.DateTimeMethods.ToDateTimeString(fecha);
                            } else {
                                valueData = "hh:mm --";
                            }
                            break;
                    }
                }
                data.push(valueData);
            }
            array.push(data);
        }
    }
    return { rows: array, columns: arrayColumns };
}
/*Función para exportar los datos a formatos pdf, excel y word*/
Componet.GridCF.TGridCFView.prototype.ExportData = function (EventArgs, arrayIndexColumn) {
    var _this = this.TParent();
    this.exportPDF = function (arrayIndexColumn) {
        var doc = new jsPDF("landscape");
        //doc.setPage('a3', 'portrait');
        doc.setFontSize(12);
        doc.setTextColor(128, 128, 128);
        doc.text(14, 15, "LEVERIT");
        doc.setDrawColor(128, 128, 128);
        doc.line(14, 17, 280, 17); // horizontal line
        doc.setFontSize(16);
        var data = null;
        if (_this.IndiceLogico == null) {
            data = _this.VclGridCF.getGridFormatJSON(arrayIndexColumn);
        } else {
            data = _this.getGridFormatJSON(arrayIndexColumn);
        }
        doc.autoTable(data.columns, data.rows,
            {
                theme: 'grid', //striped,grid,plain
                margin: { top: 22 }
            }
        );
        if (_this.detectIE()) {
            doc.save('datos.pdf');
        } else {
            doc.save('datos.pdf');
        }
    }
    this.tableToExcel = (function (arrayIndexColumn) {
        var uri = 'data:application/vnd.ms-excel;base64,'
            , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
            , base64 = function (s) {
                return window.btoa(unescape(encodeURIComponent(s)))
            }
            , format = function (s, c) {
                return s.replace(/{(\w+)}/g, function (m, p) {
                    return c[p];
                })
            }
        return function (table, name) {
            if (!_this.detectIE()) {
                var arrayIndexColumn = [];
                for (var i = 1; i < _this.VclGridCF.Columns.length - 1; i++) {
                    if (_this.VclGridCF.Columns[i].Visible) {
                        arrayIndexColumn.push(i);
                    }

                }
                var datatablaJSON = null;
                if (_this.IndiceLogico == null) {
                    datatablaJSON = _this.VclGridCF.getGridFormatJSON(arrayIndexColumn);
                } else {
                    datatablaJSON = _this.getGridFormatJSON(arrayIndexColumn);
                }
                var cadenaTabla = '<table><tr>';
                for (var i = 0; i < datatablaJSON.columns.length; i++) {
                    cadenaTabla += '<td>' + datatablaJSON.columns[i] + '</td>';
                }
                cadenaTabla = cadenaTabla + '</tr>';
                if (datatablaJSON.rows.length > 0) {
                    for (var i = 0; i < datatablaJSON.rows.length; i++) {
                        var filaRow = '<tr>';
                        for (var j = 0; j < datatablaJSON.columns.length; j++) {
                            filaRow += '<td>' + datatablaJSON.rows[i][j] + '</td>';
                        }
                        var filaRow = filaRow + '</tr>';
                        cadenaTabla += filaRow;
                    }
                }
                cadenaTabla = cadenaTabla + '</table>';
                table = _this.VclGridCF.This;
                var ctx = { worksheet: name || 'Worksheet', table: cadenaTabla }
                var anchor = document.createElement("a");
                anchor.style = "display:none !important";
                anchor.id = "downloadanchor";
                document.body.appendChild(anchor);
                if ("download" in anchor) {
                    anchor.download = "table" + "." + "xls";
                }
                anchor.href = uri + base64(format(template, ctx));
                anchor.click();
                anchor.remove();

            } else {
                var ua = window.navigator.userAgent;

                var edge = ua.indexOf('Edge/');
                if (edge > 0) {
                    _this.xport.toCSV(_this.VclGridCF.This);

                } else {
                    _this.xport.xportActive();
                    alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2"));
                }

            }
        }
    })();
    this.xport = {
        _fallbacktoCSV: true,
        xportActive: function () {

            var arrayIndexColumn = [];
            for (var i = 1; i < _this.VclGridCF.Columns.length - 1; i++) {
                if (_this.VclGridCF.Columns[i].Visible) {
                    arrayIndexColumn.push(i);
                }

            }
            var datatablaJSON = null;
            if (_this.IndiceLogico == null) {
                datatablaJSON = _this.VclGridCF.getGridFormatJSON(arrayIndexColumn);
            } else {
                datatablaJSON = _this.getGridFormatJSON(arrayIndexColumn);
            }


            var cadenaTabla = '<table><tr>';
            for (var i = 0; i < datatablaJSON.columns.length; i++) {
                cadenaTabla += '<td>' + datatablaJSON.columns[i] + '</td>';
            }
            cadenaTabla = cadenaTabla + '</tr>';
            if (datatablaJSON.rows.length > 0) {
                for (var i = 0; i < datatablaJSON.rows.length; i++) {
                    var filaRow = '<tr>';
                    for (var j = 0; j < datatablaJSON.columns.length; j++) {
                        filaRow += '<td>' + datatablaJSON.rows[i][j] + '</td>';
                    }
                    var filaRow = filaRow + '</tr>';
                    cadenaTabla += filaRow;
                }
            }
            cadenaTabla = cadenaTabla + '</table>';
            cadenaTabla = cadenaTabla.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
            cadenaTabla = cadenaTabla.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
            cadenaTabla = cadenaTabla.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params
            var txtArea1 = document.getElementById('txtArea1');
            txtArea1.contentWindow.document.open("txt/html", "replace");
            txtArea1.contentWindow.document.write(cadenaTabla);
            txtArea1.contentWindow.document.close();
            txtArea1.contentWindow.focus();
            var sa = txtArea1.contentWindow.document.execCommand("SaveAs", true, "download.xls");
            $("#txtArea1").contents().find("body").html('');

        },
        toCSV: function (tableId, filename) {
            var table = _this.VclGridCF.This;
            this._filename = (typeof filename === 'undefined') ? "tabla" : filename;
            var csv = this._tableToCSV(tableId);
            var blob = new Blob([csv], { type: "text/csv" });
            if (navigator.msSaveOrOpenBlob) {
                navigator.msSaveOrOpenBlob(blob, this._filename + ".csv");
            } else {
                this._downloadAnchor(URL.createObjectURL(blob), 'csv');
            }
        },
        _downloadAnchor: function (content, ext) {
            var anchor = document.createElement("a");
            anchor.style = "display:none !important";
            anchor.id = "downloadanchor";
            document.body.appendChild(anchor);
            if ("download" in anchor) {
                anchor.download = this._filename + "." + ext;
            }
            anchor.href = content;
            anchor.click();
            anchor.remove();
        },
        _tableToCSV: function (table) {
            var slice = Array.prototype.slice;
            return slice
                .call(table.rows)
                .map(function (row) {
                    return slice
                        .call(row.cells)
                        .map(function (cell) {
                            return '"t"'.replace("t", cell.textContent);
                        })
                        .join(",");
                })
                .join("\r\n");
        }
    };
    try {
        switch (EventArgs) {
            case 1:
                this.exportPDF(arrayIndexColumn);
                break;
            case 2:
                this.tableToExcel(arrayIndexColumn);
                break;
        }
    }
    catch (error) {
        SysCfg.Log.Methods.WriteLog("Componet.GridCF.TGridCFView exportPDF", error);
    }

}
/*Función que se activa cuando se  ejecuta el RefreschRecordSet del DataSet*/
Componet.GridCF.TGridCFView.prototype.RefreschRecordSet = function (EventArgs) {
    var _this = this.TParent();
    if (_this.EnableControls) {
        if (_this.OnRefreschRecordSet != null) {
            _this.OnRefreschRecordSet(_this, EventArgs);
        }
    }
}
/*Función que se activa cuando se  ejecuta el Refresch del DataSet*/
Componet.GridCF.TGridCFView.prototype.Refresch = function (EventArgs) {
    var _this = this.TParent();
    if (_this.EnableControls) {
        if (_this.OnRefresch != null) {
            _this.OnRefresch(_this, EventArgs);//Enviamos a la función por fuera del componente
        }
    }
}
/*Función que se activa cuando se  ejecuta el BeforeChange del DataSet*/
Componet.GridCF.TGridCFView.prototype.BeforeChange = function (DataSet, RecordOld, RecordNew, Status, isCancel) {
    var _this = this.TParent();
    if (_this.OnBeforeChange != null) {
        _this.OnBeforeChange(DataSet, RecordOld, RecordNew, Status, isCancel);//Enviamos a la función por fuera del componente
    }

}
/*Función que se activa cuando se  ejecuta el AfterChange del DataSet*/
Componet.GridCF.TGridCFView.prototype.AfterChange = function (OldRecord, NewRecord) {
    var _this = this.TParent();
    if (_this.OnAfterChange != null) {
        _this.OnAfterChange(_this, OldRecord, NewRecord);//Enviamos a la función por fuera del componente
    }
}
/*Función que se activa cuando se  ejecuta el ChangeIndex del DataSet*/
Componet.GridCF.TGridCFView.prototype.ChangeIndex = function (EventArgs) {
    var _this = this.TParent();
    if (_this.IsLoadDataSetRefresh) {//validamos que se han cargado los records
        if (_this.OnChangeIndex != null && _this.DataSet.Records.length > 0) {
            _this.OnChangeIndex(_this, _this.DataSet.Records[EventArgs]);//Enviamos el record
        }
    }
}
/*Función que agregar una columna en el grid desde el cfview*/
Componet.GridCF.TGridCFView.prototype.AddColumn = function (EventArgs) {
    var _this = this.TParent();
    _this.EventCancel(null);
    _this.IsAddColumn = true;
    EventArgs.onclick = function (ColMem1) {
        _this.ColumnClick(ColMem1);
    }
    _this.VclGridCF.AddColumn(EventArgs);
    _this.DataSet.AddFields(EventArgs.Name, EventArgs.DataType, EventArgs.Size);
    if (_this.OnAddColumn != null) {
        _this.OnAddColumn(_this, EventArgs);
    }
    for (var i = 0; i < _this.ItemTreePanel.length; i++) {
        _this.ItemTreePanel[i].splice(_this.nameColumns.length, 0, ["", i]);
    }

    _this.nameColumns.push(EventArgs.Name);
    var VclComboBoxItem = new TVclComboBoxItem();
    var tempIndex = _this.Information.VclComboBox2DivitionColumns.Value;
    VclComboBoxItem.Value = _this.Information.VclComboBox2DivitionColumns.Options.length + 1;
    VclComboBoxItem.Text = _this.Information.VclComboBox2DivitionColumns.Options.length + 1;
    VclComboBoxItem.Tag = _this.Information.VclComboBox2DivitionColumns.Options.length + 1;

    _this.Information.VclComboBox2DivitionColumns.AddItem(VclComboBoxItem);
    _this.Information.VclComboBox2DivitionColumns.Value = tempIndex;
    _this.Information.Refresh(_this.VclGridCF.FocusedRowHandle, _this.DataSet);
    _this.VclTreeGridControl.AddItems();
    var ListBoxItem = new TVclListBoxItem();
    ListBoxItem.Text = EventArgs.Caption;
    ListBoxItem.Index = _this.Information.VclComboBox2DivitionColumns.Options.length;
    ListBoxItem.Checked = true;
    var ListBoxItem2 = new TVclListBoxItem();
    ListBoxItem2.Text = EventArgs.Caption;
    ListBoxItem2.Index = _this.Information.VclComboBox2DivitionColumns.Options.length;
    ListBoxItem2.Checked = true;
    _this.VclListBoxColumns.AddListBoxItem(ListBoxItem);
    _this.VclListBoxInformationColumns.AddListBoxItem(ListBoxItem2);
    _this.FilterQuery.Init(_this.nameColumns, _this.VclGridCF);

    if (_this.VclTreeGridControl.inTreeGrid)
        _this.VclTreeGridControl.agruparPorCampo();
}
/*Función que actulizar el value de las Cells desde un modal*/
Componet.GridCF.TGridCFView.prototype.ExecuteEventModal = function (VclCellCF, inValue) {
    var _this = this.TParent();
    _this.Information.Row.Cells[VclCellCF.Column.Index].Column.EnabledEditor = true;//Habilitamos celda editable
    _this.Information.Row.Cells[VclCellCF.Column.Index]._Value = inValue;//Actualizamos valor de celda en detalle
    _this.Information.Row.Cells[VclCellCF.Column.Index].SetValue(inValue);//Cambiamos valor
    //_this.DataSet.Record.set
    _this.DataSet.FieldsIndex(VclCellCF.Column.IndexDataSourceDefault, inValue);//Actualimos valor del field en el RecordSet
    _this.Information.Row.Cells[VclCellCF.Column.Index].Column.EnabledEditor = false;//Desabilitamos la edición  en columna
    _this.Information.BuildGridInfo(_this.Information.DivitionColumn, _this.VclGridCF);//Construimos el detalle
}
/*Función para crear una cadena sql*/
Componet.GridCF.TGridCFView.prototype.GeneraQuery = function (DataSet, RecordOld, RecordNew, Status, SurceDbTable) {
    var SQLPart = "";
    var SQLPartA = "";
    var SQLPartB = "";
    if (Status == SysCfg.MemTable.Properties.TStatus.Apped) {
        for (var ContX = 0; ContX <= DataSet.FieldCount - 1; ContX++) {
            if (ContX == 0) {
                SQLPartA = DataSet.FieldDefs[ContX].FieldName;
                SQLPartB = RecordNew.Fields[ContX].Value;
            }
            else {
                SQLPartA = SQLPartA + "," + DataSet.FieldDefs[ContX].FieldName;
                SQLPartB = SQLPartB + "," + RecordNew.Fields[ContX].Value;
            }
        }
        SQLPart = "INSERT INTO " + SurceDbTable + " (" + SQLPartA + ") Values (" + SQLPartB + ")";
    }
    else if (DataSet.Status == SysCfg.MemTable.Properties.TStatus.Update) {
        for (var ContX = 0; ContX <= DataSet.FieldCount - 1; ContX++) {
            if (ContX == 0) {
                SQLPartA = DataSet.FieldDefs[ContX].FieldName + "=" + RecordNew.Fields[ContX].Value;
                SQLPartB = DataSet.FieldDefs[ContX].FieldName + "=" + RecordOld.Fields[ContX].Value;
            }
            else {
                SQLPartA = SQLPartA + "" + DataSet.FieldDefs[ContX].FieldName + "=" + RecordNew.Fields[ContX].Value;
                SQLPartB = SQLPartB + "," + DataSet.FieldDefs[ContX].FieldName + "=" + RecordOld.Fields[ContX].Value;
            }
        }
        SQLPart = "UPDATE " + SurceDbTable + " SET " + SQLPartA + " WHERE " + SQLPartB;
    }
    else if (DataSet.Status == SysCfg.MemTable.Properties.TStatus.Delete) {
        for (var ContX = 0; ContX <= DataSet.FieldCount - 1; ContX++) {
            if (ContX == 0) {
                SQLPartB = DataSet.FieldDefs[ContX].FieldName + "=" + RecordOld.Fields[ContX].Value;//SysCfg.DB.Format.objectToParam(RecordOld.Fields[ContX].Value, DataSet.FieldDefs[ContX].FieldName, DataSet.FieldDefs[ContX].DataTypetoType(), SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            }
            else {
                SQLPartB = SQLPartB + "," + DataSet.FieldDefs[ContX].FieldName + "=" + RecordOld.Fields[ContX].Value;//SysCfg.DB.Format.objectToParam(RecordOld.Fields[ContX].Value, DataSet.FieldDefs[ContX].FieldName, DataSet.FieldDefs[ContX].DataTypetoType(), SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
            }
            SQLPart = "DELETE " + SurceDbTable + " WHERE " + SQLPartB;
        }
    }
    return (SQLPart);
}
/*Función para cargar un TCfg*/
Componet.GridCF.TGridCFView.prototype.LoadTCfg = function (TCfg) {
    var _this = this.TParent();
    if (TCfg.OrderColumns.length == 0) {
        TCfg.OrderColumns = _this.OrderColumnsDefault;
    }
    var StyleTCfg = TCfg.getStyle(_this.VclGridCF);//Generamos un estilo a partir del grid.
    _this.StyleGrid = StyleTCfg;//Asignamos estilo
    if (_this.VclGridCF._FocusedRowHandle != null) {
        _this.VclGridCF._FocusedRowHandle.This.style.backgroundColor = "lightyellow";//Colocamo el fondo por defecto
        for (var i = 0; i < _this.VclGridCF.Columns.length; i++) {
            _this.VclGridCF._FocusedRowHandle.Cells[i].RestoreFunctionColor();//Restauramos color
        }
    }
}
/*************************COMPONENTES****************************************************/
/*Componente para construir el detalle del grid*/
Componet.GridCF.TGridCFView.TInformation = function (TGridCFView, inObject, ID, inCallBack, inListBoxColumns) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.Mythis = "Information";
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1", "Columns");
    _this.VclStackPanelContentInfoMenTableClose = new TVclStackPanel(inObject, "1", 1, [[12], [12], [12], [12], [12]]);
    var linkInfoTable = document.createElement("a");
    var iconInfoTable = document.createElement("i");
    linkInfoTable.href = "#";
    iconInfoTable.classList.add("icon");
    iconInfoTable.classList.add("ion-backspace-outline");
    linkInfoTable.appendChild(iconInfoTable);
    this.linkInfoTable = linkInfoTable;
    this.LabelColumns = new TVcllabel(_this.VclStackPanelContentInfoMenTableClose.Column[0].This, "", TlabelType.H0);
    this.LabelColumns.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1") + " &nbsp;&nbsp;";
    this.VclComboBox2DivitionColumns = new TVclComboBox2(_this.VclStackPanelContentInfoMenTableClose.Column[0].This, "");
    _this.VclStackPanelContentInfoMenTableClose.Column[0].This.appendChild(linkInfoTable);
    linkInfoTable.style.float = "right";
    linkInfoTable.style.color = "#757575";
    linkInfoTable.style.marginTop = "0px";
    this.VclGridCFInfo = new TVclGridCF(inObject, ID, inCallBack);
    this.VclGridCFInfo.IsInfo = true;
    this.VclGridCF = null;
    this.DataSet = null;
    this.Row = null;
    this.ListBoxColumns = inListBoxColumns;
    this.DivitionColumn = 1;
    /*Función actualizar el detalle del grid*/
    this.Refresh = function (Row, inDataSet) {
        _this.DataSet = inDataSet;//Actulizamos el valor del DataSet
        //var Enabled = (_this.DataSet.Status == SysCfg.MemTable.Properties.TStatus.Apped || _this.DataSet.Status == SysCfg.MemTable.Properties.TStatus.Update);//Validamos el estado del DataSet
        _this.VclGridCF = Row.GridParent;//Actulizamos el valor del grid
        _this.Row = (_this.DataSet.Status == SysCfg.MemTable.Properties.TStatus.Apped) ? _this.VclGridCF.Rows[Row.Index] : Row;//Actulizamos la fila seleciona del grid
        _this.BuildGridInfo(_this.VclComboBox2DivitionColumns.Value, Row.GridParent);//Construimos el detalle de la fila selecionada
    }
    /*Función para iniciar componente*/
    this.Init = function (size, inDivitionColumn) {
        this.VclComboBox2DivitionColumns.This.style.marginTop = "4px";
        this.VclComboBox2DivitionColumns.This.style.height = "32px";
        this.VclComboBox2DivitionColumns.This.style.width = "80px";
        this.VclGridCFInfo.This.style.marginTop = "3px";

        _this.DivitionColumn = inDivitionColumn;
        for (var i = 1; i <= size; i++) {
            var VclComboBoxItem = new TVclComboBoxItem();
            VclComboBoxItem.Value = i;
            VclComboBoxItem.Text = "&nbsp;" + i;
            VclComboBoxItem.Tag = i;
            _this.VclComboBox2DivitionColumns.AddItem(VclComboBoxItem);
            if (i == 4) {
                break;
            }
        }
        _this.VclComboBox2DivitionColumns.onChange = function () {
            _this.BuildGridInfo(_this.VclComboBox2DivitionColumns.Value, _this.VclGridCF);
            Componet.GridCF.Properties.VConfig.RowShowDetails = _this.VclComboBox2DivitionColumns.Value;
        }
        _this.VclComboBox2DivitionColumns.Value = _this.DivitionColumn;
    }
    /*Función recursiva para buscar la siguiente posicion de la columna que es visible*/
    this.findNext = function (position) {
        if (_this.VclGridCF.Columns.length - 2 == position) {
            if (_this.VclGridCF.Columns[position]._VisibleVertical) {
                return position;
            } else {
                return -1;
            }
        } else {
            if (_this.VclGridCF.Columns[position]._VisibleVertical) {
                return position;
            } else {
                return this.findNext(position + 1);
            }
        }
    }
    /*Función para la construción del detalle*/
    this.BuildGridInfo = function (setColumns, VclGridCF) {
        _this.VclGridCF = VclGridCF;
        var ContXTemp = 1;//1
        _this.DivitionColumn = setColumns;
        _this.VclComboBox2DivitionColumns.Value = setColumns;
        if (_this.DataSet != null) {
            _this.VclGridCF.IsInfo = true;
            var Enabled = (_this.DataSet.Status == SysCfg.MemTable.Properties.TStatus.Apped || _this.DataSet.Status == SysCfg.MemTable.Properties.TStatus.Update);
            this.VclGridCFInfo.ClearAll();
            var lenghtColumns = _this.VclGridCF.Columns.length - 2;//6
            var setRows = 1;
            if (lenghtColumns % setColumns == 0) {
                setRows = parseInt(lenghtColumns / setColumns);
            } else {
                setRows = parseInt(lenghtColumns / setColumns) + 1;
            }
            var listCells = new Array();
            for (var i = 0; i < setRows; i++) {
                var NewRow = new TVclRowCF();
                for (var j = 0; j < setColumns; j++) {
                    if (lenghtColumns >= ContXTemp) {
                        if (ContXTemp == -1) {
                            break;
                        }
                        ContXTemp = this.findNext(ContXTemp);
                        if (ContXTemp == -1) {
                            break;
                        }
                        _this.VclGridCF.Columns[ContXTemp].EnabledEditor = Enabled;
                        var CellInfo0 = new TVclCellCF();
                        CellInfo0.Column = null;
                        CellInfo0.IndexColumn = j * 2;
                        CellInfo0.IndexRow = ContXTemp;
                        var LabelTextGroup = new TVcllabel(CellInfo0.This, "", TlabelType.H0);
                        LabelTextGroup.Text = _this.VclGridCF.Columns[ContXTemp].Caption + ":";
                        CellInfo0.Color = "#3e94ba";
                        CellInfo0.This.style.width = "250px";
                        LabelTextGroup.This.style.color = "#FFFFFF";
                        var CellInfo1 = new TVclCellCF();

                        if (ContXTemp == 0) {
                            CellInfo1.Value = true;
                        } else {
                            CellInfo1.Value = _this.DataSet.RecordSet.Fields[_this.VclGridCF.Columns[ContXTemp].IndexDataSourceDefault].Value;
                        }

                        CellInfo1.Column = _this.VclGridCF.Columns[ContXTemp];
                        CellInfo1.IndexColumn = j * 2 + 1;
                        CellInfo1.IndexRow = i;
                        NewRow.AddCell(CellInfo0);
                        NewRow.AddCell(CellInfo1);
                        listCells.push(CellInfo1);
                        ContXTemp++;
                    } else {
                        var CellInfo0 = new TVclCellCF();
                        var CellInfo1 = new TVclCellCF();
                        CellInfo0.Value = "";
                        CellInfo0.Column = null;
                        CellInfo0.IndexColumn = 0;
                        CellInfo0.IndexRow = ContXTemp;
                        CellInfo0.Visible = false;
                        CellInfo1.Value = "";
                        CellInfo1.Column = null;
                        CellInfo1.IndexColumn = 0;
                        CellInfo1.IndexRow = ContXTemp;
                        CellInfo1.Visible = false;
                        NewRow.AddCell(CellInfo0);
                        NewRow.AddCell(CellInfo1);

                        ContXTemp++;
                    }
                }

                _this.VclGridCFInfo.AddRowInfo(NewRow);
            }
            if (Enabled) {
                var ListCaptionNameColumn = new Array();
                for (var i = 0; i < listCells.length; i++) {
                    if (listCells[i].Column.ModalEvent == null && listCells[i].Column.ActiveEditor) {
                        _this.AddEventeCell(listCells[i]);
                        var column = listCells[i].Column;
                        var name = column.Name;
                        ListCaptionNameColumn[name] = name;

                    }
                }
                for (var i = 1; i < _this.VclGridCF.Columns.length - 1; i++) {
                    var column = _this.VclGridCF.Columns[i];
                    if (typeof (ListCaptionNameColumn[column.Name]) == "undefined") {
                        if (column.ModalEvent == null && column.ActiveEditor) {
                            _this.AddEventeCell(_this.Row.Cells[i]);
                        }

                    }

                }
                ListCaptionNameColumn = new Array();
            }
            for (var i = 0; i < _this.VclGridCF.Columns.length - 1; i++) {
                _this.VclGridCF.Columns[i]._EnabledEditor = false;

            }
            _this.VclGridCF.IsInfo = false;
        }
    }
    /*Función para agregar eventos en las celdas del detalle grid*/
    this.AddEventeCell = function (Cell) {
        switch (Cell.Column.DataType) {
            case SysCfg.DB.Properties.TDataType.String:
                switch (Cell.Column.ColumnStyle) {
                    case Componet.Properties.TExtrafields_ColumnStyle.None:
                        _this.AddEventControlTVclTextBox(Cell);
                        break;
                    case Componet.Properties.TExtrafields_ColumnStyle.TextPassword:
                        _this.AddEventControlTVclTextBox(Cell);
                        break;
                    case Componet.Properties.TExtrafields_ColumnStyle.LookUp:
                        _this.AddEventControlTVclComboBox2(Cell);
                        break;
                    case Componet.Properties.TExtrafields_ColumnStyle.LookUpOption:

                        _this.AddEventControlTVclInputRadioButton(Cell);
                        break;
                    case Componet.Properties.TExtrafields_ColumnStyle.Text:
                        _this.AddEventControlTVclTextArea(Cell);
                        break;
                }
                break;
            case SysCfg.DB.Properties.TDataType.Text:
                switch (Cell.Column.ColumnStyle) {
                    case Componet.Properties.TExtrafields_ColumnStyle.None:
                        _this.AddEventControlTVclTextArea(Cell);
                        break;
                }
                break;
            case SysCfg.DB.Properties.TDataType.Int32:
                switch (Cell.Column.ColumnStyle) {
                    case Componet.Properties.TExtrafields_ColumnStyle.None:
                        _this.AddEventControlTVclTextBox(Cell);
                        break;
                    case Componet.Properties.TExtrafields_ColumnStyle.LookUp:
                        _this.AddEventControlTVclComboBox2(Cell);
                        break;
                    case Componet.Properties.TExtrafields_ColumnStyle.LookUpOption:
                        _this.AddEventControlTVclInputRadioButton(Cell);
                        break;
                    case Componet.Properties.TExtrafields_ColumnStyle.ProgressBar:
                        _this.AddEventControlTVclTextBox(Cell);
                        break;
                }
                break;
            case SysCfg.DB.Properties.TDataType.Double:
                switch (Cell.Column.ColumnStyle) {
                    case Componet.Properties.TExtrafields_ColumnStyle.None:
                        _this.AddEventControlTVclTextBox(Cell);
                        break;
                }
                break;
            case SysCfg.DB.Properties.TDataType.Boolean:
                switch (Cell.Column.ColumnStyle) {
                    case Componet.Properties.TExtrafields_ColumnStyle.None:
                        _this.AddEventControlTVclCheckbox(Cell);
                        break;
                }
                break;
            case SysCfg.DB.Properties.TDataType.DateTime:
                switch (Cell.Column.ColumnStyle) {
                    case Componet.Properties.TExtrafields_ColumnStyle.None:
                        _this.AddEventControlTVclTextBoxDate(Cell);
                        break;
                    case Componet.Properties.TExtrafields_ColumnStyle.Date:
                        _this.AddEventControlTVclTextBoxDate(Cell);
                        break;
                    case Componet.Properties.TExtrafields_ColumnStyle.Time:
                        _this.AddEventControlTVclTextBoxTime(Cell);
                        break;
                }
                break
            default:
                alert("Not event");
                break;
        }
        if (Cell.Column.Index == 0)
            _this.Row.Cells[Cell.Column.Index].Control.This.focus();
    }
    /*Función para agregar eventos a input*/
    this.AddEventControlTVclTextBox = function (Cell) {
        _this.Row.Cells[Cell.Column.Index].Control.This.id = Cell.Column.Index;
        _this.Row.Cells[Cell.Column.Index].Control.onKeyPress = function () {
            var id = this.id;
            var valor = this.value;
            _this.VclGridCF.Columns[id].EnabledEditor = true;
            _this.VclGridCFInfo.Rows[Cell.IndexRow]._Cells[Cell.IndexColumn].Column = _this.VclGridCF.Columns[id];
            _this.VclGridCFInfo.Rows[Cell.IndexRow]._Cells[Cell.IndexColumn].SetValue("" + valor);
            _this.DataSet.FieldsIndex(_this.VclGridCF.Columns[id].IndexDataSourceDefault, valor);
            _this.VclGridCF.Columns[id].EnabledEditor = false;
            TGridCFView.OnActiveChangeControls = true;

        }
        _this.Row.Cells[Cell.Column.Index].Control.onKeyUp = function () {

            var id = this.id;
            var valor = this.value;
            _this.VclGridCF.Columns[id].EnabledEditor = true;
            _this.VclGridCFInfo.Rows[Cell.IndexRow]._Cells[Cell.IndexColumn].Column = _this.VclGridCF.Columns[id];
            _this.VclGridCFInfo.Rows[Cell.IndexRow]._Cells[Cell.IndexColumn].SetValue(valor);
            _this.DataSet.FieldsIndex(_this.VclGridCF.Columns[id].IndexDataSourceDefault, valor);
            _this.VclGridCF.Columns[id].EnabledEditor = false;
            TGridCFView.OnActiveChangeControls = true;
        }
        Cell.Control.This.id = Cell.Column.Index;
        Cell.Control.onKeyPress = function () {

            var id = this.id;
            var valor = this.value;
            _this.VclGridCF.Columns[id].EnabledEditor = true;
            _this.Row._Cells[id].Column = _this.VclGridCF.Columns[id];
            _this.Row._Cells[id].SetValue("" + valor);
            _this.DataSet.FieldsIndex(_this.VclGridCF.Columns[id].IndexDataSourceDefault, valor);
            _this.VclGridCF.Columns[id].EnabledEditor = false;
            TGridCFView.OnActiveChangeControls = true;
        }
        Cell.Control.onKeyUp = function () {

            var id = this.id;
            var valor = this.value;
            _this.VclGridCF.Columns[id].EnabledEditor = true;
            _this.Row._Cells[id].Column = _this.VclGridCF.Columns[id];
            _this.Row._Cells[id].SetValue(valor);
            _this.DataSet.FieldsIndex(_this.VclGridCF.Columns[id].IndexDataSourceDefault, valor);
            _this.VclGridCF.Columns[id].EnabledEditor = false;
            TGridCFView.OnActiveChangeControls = true;
        }
    }
    /*Función para agregar eventos a text area*/
    this.AddEventControlTVclTextArea = function (Cell) {
        var CellArea = _this.Row.Cells[Cell.Column.Index];
        CellArea.onOutside = function (inData) {
            //Funcion que proviene al cerrar el modal del text area desde el grid
            var textArea = inData;
            var id = Cell.Column.Index;
            var valor = textArea.value;
            var valorAntiguo = _this.VclGridCFInfo.Rows[Cell.IndexRow]._Cells[Cell.IndexColumn]._Value;
            _this.VclGridCF.Columns[id].EnabledEditor = true;
            _this.VclGridCFInfo.Rows[Cell.IndexRow]._Cells[Cell.IndexColumn].Column = _this.VclGridCF.Columns[id];
            _this.VclGridCFInfo.Rows[Cell.IndexRow]._Cells[Cell.IndexColumn].SetValue(valor);
            _this.Row._Cells[id].Column = _this.VclGridCF.Columns[id];
            _this.Row._Cells[id].SetValue(valor);
            _this.DataSet.FieldsIndex(Cell.Column.IndexDataSourceDefault, valor);
            _this.VclGridCF.Columns[id].EnabledEditor = false;
            if (valorAntiguo != valor) {
                TGridCFView.OnActiveChangeControls = true;
            }
        }
        Cell.onOutside = function (inData) {
            //Funcion que proviene al cerrar el modal del text area desde el detalle del grid
            var textArea = inData;
            var id = Cell.Column.Index;
            var valor = textArea.value;
            var valorAntiguo = _this.VclGridCFInfo.Rows[Cell.IndexRow]._Cells[Cell.IndexColumn]._Value;
            _this.VclGridCF.Columns[id].EnabledEditor = true;
            _this.VclGridCFInfo.Rows[Cell.IndexRow]._Cells[Cell.IndexColumn].Column = _this.VclGridCF.Columns[id];
            _this.VclGridCFInfo.Rows[Cell.IndexRow]._Cells[Cell.IndexColumn].SetValue(valor);
            _this.Row._Cells[id].Column = _this.VclGridCF.Columns[id];
            _this.Row._Cells[id].SetValue(valor);
            _this.DataSet.FieldsIndex(Cell.Column.IndexDataSourceDefault, valor);
            _this.VclGridCF.Columns[id].EnabledEditor = false;
            if (valorAntiguo != valor) {
                TGridCFView.OnActiveChangeControls = true;
            }
        }
    }
    /*Función para agregar eventos a input-checkbox*/
    this.AddEventControlTVclCheckbox = function (Cell) {
        _this.Row.Cells[Cell.Column.Index].Control.This.id = Cell.Column.Index;
        _this.Row.Cells[Cell.Column.Index].Control.onClick = function () {
            var id = this.id;
            var valor = this.checked;
            _this.VclGridCF.Columns[id].EnabledEditor = true;
            _this.VclGridCFInfo.Rows[Cell.IndexRow]._Cells[Cell.IndexColumn].Column = _this.VclGridCF.Columns[id];
            _this.VclGridCFInfo.Rows[Cell.IndexRow]._Cells[Cell.IndexColumn].SetValue(this.checked);
            _this.DataSet.FieldsIndex(_this.VclGridCF.Columns[id].IndexDataSourceDefault, valor);
            _this.VclGridCF.Columns[id].EnabledEditor = false;
            TGridCFView.OnActiveChangeControls = true;
        }
        Cell.Control.This.id = Cell.Column.Index;
        Cell.Control.onClick = function () {
            var id = this.id;
            var valor = this.checked;
            _this.VclGridCF.Columns[id].EnabledEditor = true;
            _this.Row._Cells[id].Column = _this.VclGridCF.Columns[id];
            _this.Row._Cells[id].SetValue(this.checked);
            _this.DataSet.FieldsIndex(_this.VclGridCF.Columns[id].IndexDataSourceDefault, valor);
            _this.VclGridCF.Columns[id].EnabledEditor = false;
            TGridCFView.OnActiveChangeControls = true;
        }
    }
    /*Función para agregar eventos a input-date*/
    this.AddEventControlTVclTextBoxDate = function (Cell) {
        _this.Row.Cells[Cell.Column.Index].Control.This.name = Cell.Column.Index;
        _this.Row.Cells[Cell.Column.Index].Control.onChange = function () {
            var id = this.name;
            var valor = $(this).val();
            _this.VclGridCF.Columns[id].EnabledEditor = true;
            _this.VclGridCFInfo.Rows[Cell.IndexRow]._Cells[Cell.IndexColumn].Column = _this.VclGridCF.Columns[id];
            _this.VclGridCFInfo.Rows[Cell.IndexRow]._Cells[Cell.IndexColumn].SetValue(valor);
            _this.DataSet.FieldsIndex(_this.VclGridCF.Columns[id].IndexDataSourceDefault, valor);
            _this.VclGridCF.Columns[id].EnabledEditor = false;
            TGridCFView.OnActiveChangeControls = true;

        }
        Cell.Control.This.name = Cell.Column.Index;
        Cell.Control.onChange = function () {
            var id = this.name;
            var valor = this.value;
            _this.VclGridCF.Columns[id].EnabledEditor = true;
            _this.Row._Cells[id].Column = _this.VclGridCF.Columns[id];
            _this.Row._Cells[id].SetValue(valor);
            _this.DataSet.FieldsIndex(_this.VclGridCF.Columns[id].IndexDataSourceDefault, valor);
            _this.VclGridCF.Columns[id].EnabledEditor = false;
            TGridCFView.OnActiveChangeControls = true;
        }
    }
    /*Función para agregar eventos a input-date-time*/
    this.AddEventControlTVclTextBoxTime = function (Cell) {
        var valorFechaCell = new Date(_this.Row.Cells[Cell.Column.Index]._Value);
        if (isNaN(valorFechaCell.getTime())) {  // d.valueOf() could also work
            valorFechaCell = new Date();
        }
        _this.Row.Cells[Cell.Column.Index].Control.This.name = Cell.Column.Index;
        _this.Row.Cells[Cell.Column.Index].Control.onChange = function () {
            //var valorStart = new Date(date.getFullYear(), date.getMonth(), date.getDay());
            var id = this.name;
            var valor = $(this).val();

            var valorFecha = valor.split(":");
            var hour = valorFecha[0];
            valorFecha = valorFecha[1].split(" ");
            var minute = valorFecha[0];
            if (valorFecha[1].toUpperCase() == "PM") {
                hour = parseInt(hour) + 12;
            }
            var second = "0";
            valorFechaCell.setHours(hour);
            valorFechaCell.setMinutes(minute);
            valorFechaCell.setSeconds(second);

            _this.VclGridCF.Columns[id].EnabledEditor = true;
            _this.VclGridCFInfo.Rows[Cell.IndexRow]._Cells[Cell.IndexColumn].Column = _this.VclGridCF.Columns[id];
            _this.VclGridCFInfo.Rows[Cell.IndexRow]._Cells[Cell.IndexColumn].SetValue(valor);

            _this.DataSet.FieldsIndex(_this.VclGridCF.Columns[id].IndexDataSourceDefault, valorFechaCell);
            _this.VclGridCF.Columns[id].EnabledEditor = false;
            TGridCFView.OnActiveChangeControls = true;

        }
        Cell.Control.This.name = Cell.Column.Index;
        Cell.Control.onChange = function () {
            var id = this.name;
            var valor = this.value;
            _this.VclGridCF.Columns[id].EnabledEditor = true;
            _this.Row._Cells[id].Column = _this.VclGridCF.Columns[id];
            _this.Row._Cells[id].SetValue(valor);
            _this.DataSet.FieldsIndex(_this.VclGridCF.Columns[id].IndexDataSourceDefault, valor);
            _this.VclGridCF.Columns[id].EnabledEditor = false;
            TGridCFView.OnActiveChangeControls = true;
        }
    }
    /*Función para agregar eventos a combos*/
    this.AddEventControlTVclComboBox2 = function (Cell) {
        _this.Row.Cells[Cell.Column.Index].Control.This.name = Cell.Column.Index;
        _this.Row.Cells[Cell.Column.Index].Control.onChange = function () {
            var id = this.name;
            var valor = $(this).val();
            _this.VclGridCF.Columns[id].EnabledEditor = true;
            _this.VclGridCFInfo.Rows[Cell.IndexRow]._Cells[Cell.IndexColumn].Column = _this.VclGridCF.Columns[id];
            _this.VclGridCFInfo.Rows[Cell.IndexRow]._Cells[Cell.IndexColumn].SetValue(valor);
            _this.DataSet.FieldsIndex(_this.VclGridCF.Columns[id].IndexDataSourceDefault, valor);
            _this.VclGridCF.Columns[id].EnabledEditor = false;
            TGridCFView.OnActiveChangeControls = true;

        }
        Cell.Control.This.name = Cell.Column.Index;
        Cell.Control.onChange = function () {
            var id = this.name;
            var valor = this.value;
            _this.VclGridCF.Columns[id].EnabledEditor = true;
            _this.Row._Cells[id].Column = _this.VclGridCF.Columns[id];
            _this.Row._Cells[id].SetValue(valor);
            _this.DataSet.FieldsIndex(_this.VclGridCF.Columns[id].IndexDataSourceDefault, valor);
            _this.VclGridCF.Columns[id].EnabledEditor = false;
            TGridCFView.OnActiveChangeControls = true;
        }
    }
    /*Función para agregar eventos a radio-button2*/
    this.AddEventControlTVclInputRadioButton2 = function (Cell) {

        for (var i = 0; i < _this.Row.Cells[Cell.Column.Index].Control.Options.length; i++) {
            _this.Row.Cells[Cell.Column.Index].Control.Options[i].This.name = Cell.Column.Index;
            _this.Row.Cells[Cell.Column.Index].Control.Options[i].This.onclick = function () {
                var id = this.name;
                _this.VclGridCF.Columns[id].EnabledEditor = true;
                var valor = _this.Row.Cells[Cell.Column.Index].Control.Value;

                _this.VclGridCFInfo.Rows[Cell.IndexRow].Cells[Cell.IndexColumn].Column = _this.VclGridCF.Columns[id];
                _this.VclGridCFInfo.Rows[Cell.IndexRow].Cells[Cell.IndexColumn].SetValue(valor);
                _this.DataSet.FieldsIndex(_this.VclGridCF.Columns[id].IndexDataSourceDefault, valor);
                _this.Row.Cells[Cell.Column.Index].SetValue(valor);
                _this.VclGridCF.Columns[id].EnabledEditor = false;
                TGridCFView.OnActiveChangeControls = true;

            }

            Cell.Control.Options[i].This.name = Cell.Column.Index;
            Cell.Control.Options[i].This.onclick = function () {
                var id = this.name;
                _this.VclGridCF.Columns[id].EnabledEditor = true;
                var valor = Cell.Control.Value;

                _this.Row.Cells[id].Column = _this.VclGridCF.Columns[id];
                _this.Row.Cells[id].SetValue(valor);
                _this.DataSet.FieldsIndex(_this.VclGridCF.Columns[id].IndexDataSourceDefault, valor);
                Cell.SetValue(valor);
                _this.VclGridCF.Columns[id].EnabledEditor = false;
                TGridCFView.OnActiveChangeControls = true;

            }

        }


    }
    /*Función para agregar eventos a radio-button*/
    this.AddEventControlTVclInputRadioButton = function (Cell) {

        for (var i = 0; i < _this.Row.Cells[Cell.Column.Index].Control.Options.length; i++) {
            _this.Row.Cells[Cell.Column.Index].Control.Options[i].This.name = Cell.Column.Index;
            _this.Row.Cells[Cell.Column.Index].Control.Options[i].This.onclick = function () {
                var id = this.name;
                _this.VclGridCF.Columns[id].EnabledEditor = true;
                var valor = _this.Row.Cells[Cell.Column.Index].Control.Value;
                Cell.Control.Value = valor;
                _this.Row.Cells[Cell.Column.Index].Control.Value = valor;
                _this.DataSet.FieldsIndex(_this.VclGridCF.Columns[Cell.Column.Index].IndexDataSourceDefault, valor);
                TGridCFView.OnActiveChangeControls = true;
            }


            Cell.Control.Options[i].This.onclick = function () {
                var valor = Cell.Control.Value;
                _this.Row.Cells[Cell.Column.Index].Control.Value = valor;
                Cell.Control.Value = valor;

                _this.DataSet.FieldsIndex(_this.VclGridCF.Columns[Cell.Column.Index].IndexDataSourceDefault, valor);
                TGridCFView.OnActiveChangeControls = true;

            }

        }


    }

}
/*Componente para construir el status del DataSet y del Grid en el CFView*/
Componet.GridCF.TGridCFView.TStatusCF = function (inObject) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.Object = inObject;
    this.ContlabelBE = null;
    this.LabelBof = null;
    this.LabelEof = null;
    this.LabelIndex = null;
    this.LabelStatus = null;
    this.Init = function () {
        _this.ContlabelBE = new TVclStackPanel(_this.Object, "", 4, [[12, 1, 1, 1], [12, 1, 1, 1], [12, 1, 1, 1], [12, 1, 1, 1], [12, 1, 1, 1]]);
        _this.ContlabelBE.Row.This.style.marginTop = "0px";
        _this.VclStackPanelContentStatus = new TVclStackPanel(_this.ContlabelBE.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
        var linkInfoStatus = document.createElement("a");
        var iconInfoStatus = document.createElement("i");
        linkInfoStatus.href = "#";
        iconInfoStatus.classList.add("icon");
        iconInfoStatus.classList.add("ion-backspace-outline");
        iconInfoStatus.style.color = "#757575";
        linkInfoStatus.appendChild(iconInfoStatus);
        _this.linkInfoStatus = linkInfoStatus;


        _this.LabelBof = new TVcllabel(_this.ContlabelBE.Column[0].This, "", TlabelType.H0);
        _this.LabelBof.Text = "...";
        _this.LabelBof.VCLType = TVCLType.BS;
        _this.LabelEof = new TVcllabel(_this.ContlabelBE.Column[0].This, "", TlabelType.H0);
        _this.LabelEof.Text = "...";
        _this.LabelEof.VCLType = TVCLType.BS;
        _this.LabelIndex = new TVcllabel(_this.ContlabelBE.Column[0].This, "", TlabelType.H0);
        _this.LabelIndex.Text = "...";
        _this.LabelIndex.VCLType = TVCLType.BS;
        _this.LabelStatus = new TVcllabel(_this.ContlabelBE.Column[0].This, "", TlabelType.H0);
        _this.LabelStatus.Text = "...";
        _this.LabelStatus.VCLType = TVCLType.BS;
        _this.ContlabelBE.Column[0].This.appendChild(linkInfoStatus);
        linkInfoStatus.style.float = "right";


        _this.LabelBof.This.className = "label label-primary";
        _this.LabelEof.This.className = "label label-primary";
        _this.LabelIndex.This.className = "label label-primary";
        _this.LabelStatus.This.className = "label label-primary";
        _this.LabelEof.This.style.marginLeft = "10px";
        _this.LabelIndex.This.style.marginLeft = "10px";
        _this.LabelStatus.This.style.marginLeft = "10px";

        _this.LabelEof.This.style.display = "none";
        _this.LabelIndex.This.style.display = "none";
        _this.LabelStatus.This.style.display = "none";

        _this.ContlabelBE2 = new TVclStackPanel(_this.Object, "", 4, [[3, 3, 3, 3], [3, 3, 3, 3], [3, 3, 3, 3], [3, 3, 3, 3], [3, 3, 3, 3]]);
        _this.ContlabelBE2.Row.This.style.marginTop = "5px";

    }
    /*Función para actulizar el valor del status del DataSet*/
    this.Refresch = function (Bof, Eof, Index, Status) {

        _this.LabelBof.Text = "Bof:" + Bof + " &nbsp;&nbsp;&nbsp;Eof:" + Eof + " &nbsp;&nbsp;&nbsp;Index:" + Index + " &nbsp;&nbsp;&nbsp;Status:" + Status;
    }
    /*Función para mostrar componente*/
    Object.defineProperty(this, 'Visible', {
        get: function () {
            return (_this.ContlabelBE.Row.This.style.display == "") ? true : false;

        },
        set: function (inValue) {
            if (this._DivPanelStatus != null) {
                if (inValue) {
                    _this.ContlabelBE.Row.This.style.display = "";
                } else {
                    _this.ContlabelBE.Row.This.style.display = "none";
                }
            }
        }
    });
    this.Init();
}
/*Componente para construir los filtros mediante LINQ*/
Componet.GridCF.TGridCFView.TFilterQuery = function (inObject) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    if (typeof (inObject) == "undefined" || inObject == null) {
        inObject = document.createElement("div");
    }
    this.Object = inObject;
    this.Elements = new Array();
    this.ListCombox = new Array();
    this.NameColumns = new Array();
    this.VclGridCF = null;
    this.Operations = new Array();
    this.OperationsAnexo = new Array();
    this.index = 0;
    this.VclStackPanelCreateQuery = null;
    this.VclStackPanelFilterAllColumn = null;
    this.isFilterAllColumn = false;
    this.VclTextBoxFilter = null;
    this.ListCustomFilter = new Array();//Lista de filtros
    Object.defineProperty(this, 'ShowFilterAllColumn', {
        get: function () {
            return _this.isFilterAllColumn;

        },
        set: function (inValue) {
            _this.isFilterAllColumn = inValue;
            if (inValue) {
                _this.VclStackPanelCreateQuery.Row.This.style.display = "none";
                _this.VclStackPanelFilterAllColumn.Row.This.style.display = "";
            } else {
                _this.VclStackPanelCreateQuery.Row.This.style.display = "";
                _this.VclStackPanelFilterAllColumn.Row.This.style.display = "none";
            }

        }
    });
    /*Función inicializar componente*/
    this.Init = function (inNameColumns, inVclGridCF) {
        _this.VclGridCF = inVclGridCF;
        _this.RemoveElementsAll();
        _this.Operations = new Array();
        _this.OperationsAnexo = new Array();
        _this.Operations.push({ value: "==", text: "=" });
        _this.Operations.push({ value: "!=", text: "≠" });
        _this.Operations.push({ value: ">", text: ">" });
        _this.Operations.push({ value: ">=", text: ">=" });
        _this.Operations.push({ value: "<", text: "<" });
        _this.Operations.push({ value: "<=", text: "<=" });

        _this.Operations.push({ value: "like", text: "like" });
        _this.OperationsAnexo.push({ value: "&&", text: "And" });
        _this.OperationsAnexo.push({ value: "||", text: "Or" });
        _this.NameColumns = inNameColumns;
        if (_this.VclStackPanelFilterAllColumn != null) {
            $(_this.VclStackPanelFilterAllColumn.Column[0].This).html("");
            $(_this.VclStackPanelFilterAllColumn.This).remove();

        }
        _this.VclStackPanelCreateQuery = new TVclStackPanel(_this.Object, "1", 1, [[12], [12], [12], [12], [12]]);
        _this.VclStackPanelFilterAllColumn = new TVclStackPanel(_this.Object, "1", 1, [[12], [12], [12], [12], [12]]);
        _this.VclTextBoxFilter = new TVclTextBox(_this.VclStackPanelFilterAllColumn.Column[0].This, "");
        _this.ShowFilterAllColumn = false;
        _this.AddElements();
        _this.index = 0;

    }
    /*Función para agregar elementos*/
    this.AddElements = function () {
        var VclStackPanelQuery = new TVclStackPanel(_this.VclStackPanelCreateQuery.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
        VclStackPanelQuery.Row.This.style.marginTop = "5px";
        var VclInputbutton = new TVclInputbutton(VclStackPanelQuery.Column[0].This, "");
        VclInputbutton.Text = "+";
        VclInputbutton.Visible = true;
        var VclInputbuttonRemove = new TVclInputbutton(VclStackPanelQuery.Column[0].This, "");
        VclInputbuttonRemove.Text = "x";
        VclInputbuttonRemove.Visible = true;
        var VclComboBox2Anexo = new TVclComboBox2(VclStackPanelQuery.Column[0].This, "");
        if (_this.Elements.length == 0) {
            VclComboBox2Anexo.Visible = false;
            VclInputbuttonRemove.Visible = false;
            _this.index = 0;
        } else {
            _this.index++;
        }
        VclInputbuttonRemove.ObjectButton = _this.index;
        VclInputbuttonRemove.onClickObjectButton = function (index) {
            _this.RemoveElements(index);
        }
        VclInputbutton.onClick = function () {
            _this.AddElements(_this.NameColumns);
        }

        for (var i = 0; i < _this.OperationsAnexo.length; i++) {
            var VclComboBoxItem = new TVclComboBoxItem();
            VclComboBoxItem.Value = _this.OperationsAnexo[i].value;
            VclComboBoxItem.Text = _this.OperationsAnexo[i].text;
            VclComboBoxItem.Tag = _this.OperationsAnexo[i].text;
            VclComboBox2Anexo.AddItem(VclComboBoxItem);
        }
        var VclComboBox2 = new TVclComboBox2(VclStackPanelQuery.Column[0].This, "");
        for (var i = 0; i < _this.NameColumns.length; i++) {
            var VclComboBoxItem = new TVclComboBoxItem();
            VclComboBoxItem.Value = i;
            VclComboBoxItem.Text = _this.NameColumns[i];
            VclComboBoxItem.Tag = _this.NameColumns[i];
            VclComboBox2.AddItem(VclComboBoxItem);
        }
        var VclComboBox2Operation = new TVclComboBox2(VclStackPanelQuery.Column[0].This, "");
        for (var i = 0; i < _this.Operations.length; i++) {
            var VclComboBoxItem = new TVclComboBoxItem();
            VclComboBoxItem.Value = _this.Operations[i].value;
            VclComboBoxItem.Text = _this.Operations[i].text;
            VclComboBoxItem.Tag = _this.Operations[i].text;
            VclComboBox2Operation.AddItem(VclComboBoxItem);
        }

        VclTextBox = new TVclTextBox(VclStackPanelQuery.Column[0].This, "");

        _this.Elements.push(
            {

                Index: _this.index,
                VclStackPanelQuery: VclStackPanelQuery,
                VclComboBox2Anexo: VclComboBox2Anexo,
                VclInputbutton: VclInputbutton,
                VclInputbuttonRemove: VclInputbuttonRemove,
                VclComboBox2: VclComboBox2,
                VclComboBox2Operation: VclComboBox2Operation,
                VclTextBox: VclTextBox
            }
        );

    }
    /*Función para remover elementos de un index*/
    this.RemoveElements = function (index) {
        for (var i = 0; i < _this.Elements.length; i++) {
            if (_this.Elements[i].Index == index) {
                _this.Elements[i].VclInputbutton.getThis[0].setAttribute("class", "hidden");
                $(_this.Elements[i].VclInputbutton).remove();
                _this.Elements[i].VclInputbuttonRemove.getThis[0].setAttribute("class", "hidden");
                $(_this.Elements[i].VclInputbuttonRemove).remove();
                $(_this.Elements[i].VclComboBox2Anexo.getThis).remove();
                $(_this.Elements[i].VclComboBox2Operation.getThis).remove();
                $(_this.Elements[i].VclComboBox2.getThis).remove();
                $(_this.Elements[i].VclTextBox.This).remove();
                _this.Elements[i].VclStackPanelQuery.Row.This.style.marginTop = "0px";
                $(_this.Elements[i].VclStackPanelQuery.Column[0].This).html("");
                $(_this.Elements[i].VclStackPanelQuery.This).remove();
                _this.Elements.splice(i, 1);
                return true;
            }

        }

    }
    /*Función para eiminar todos los elementos de la interfaz de la consulta personalizada*/
    this.RemoveElementsAll = function () {
        for (var i = 0; i < _this.Elements.length; i++) {
            _this.Elements[i].VclInputbutton.getThis[0].setAttribute("class", "hidden");
            $(_this.Elements[i].VclInputbutton).remove();
            _this.Elements[i].VclInputbuttonRemove.getThis[0].setAttribute("class", "hidden");
            $(_this.Elements[i].VclInputbuttonRemove).remove();
            $(_this.Elements[i].VclComboBox2Anexo.getThis).remove();
            $(_this.Elements[i].VclComboBox2Operation.getThis).remove();
            $(_this.Elements[i].VclComboBox2.getThis).remove();
            $(_this.Elements[i].VclTextBox.This).remove();
            _this.Elements[i].VclStackPanelQuery.Row.This.style.marginTop = "0px";
            $(_this.Elements[i].VclStackPanelQuery.Column[0].This).html("");
            $(_this.Elements[i].VclStackPanelQuery.This).remove();
            _this.Elements.splice(i, 1);
        }
        _this.index = 0;
    }
    //Función para obtner una cadena de consulta con formato LINQ
    this.CreateQuery = function () {
        var queryWhere = "";
        if (!_this.ShowFilterAllColumn) {
            for (var i = 0; i < _this.Elements.length; i++) {
                if (
                    _this.Elements[i].VclComboBox2.Value == null
                    || _this.Elements[i].VclComboBox2Operation.Value == null
                    || _this.Elements[i].VclTextBox.Text == null
                    || _this.Elements[i].VclTextBox.Text == ""
                ) {

                    return null;
                }
                if (i > 0) {
                    _this.Elements[i].VclComboBox2Anexo.Value
                    queryWhere = queryWhere + " " + _this.Elements[i].VclComboBox2Anexo.Value + " ";
                }

                if (_this.Elements[i].VclComboBox2Operation.Value == "like") {
                    queryWhere = queryWhere + "(~$[" + _this.Elements[i].VclComboBox2.Value + "][0].toUpperCase().indexOf('";
                    queryWhere = queryWhere + _this.Elements[i].VclTextBox.Text + "'.toUpperCase())) ";
                }
                else {
                    queryWhere = queryWhere + "$[" + _this.Elements[i].VclComboBox2.Value + "][0] ";

                    if (_this.VclGridCF != null) {
                        for (var count = 1; count < _this.VclGridCF.Columns.length - 1; count++) {
                            if (_this.Elements[i].VclComboBox2.Text == _this.VclGridCF.Columns[count].Name) {

                                if (_this.VclGridCF.Columns[count].DataType == SysCfg.DB.Properties.TDataType.Int32 || _this.VclGridCF.Columns[count].DataType == SysCfg.DB.Properties.TDataType.Double) {
                                    queryWhere = queryWhere + _this.Elements[i].VclComboBox2Operation.Value + " ";
                                    queryWhere = queryWhere + _this.Elements[i].VclTextBox.Text + " ";
                                } else {
                                    queryWhere = queryWhere + _this.Elements[i].VclComboBox2Operation.Value + " '";
                                    queryWhere = queryWhere + _this.Elements[i].VclTextBox.Text + "' ";
                                }

                                break;
                            }

                        }
                    }


                }

            }
        } else {
            for (var i = 0; i < _this.NameColumns.length; i++) {

                if (i > 0) {
                    queryWhere = queryWhere + " || ";
                }
                queryWhere = queryWhere + "(~$[" + i + "][0].toUpperCase().indexOf('";
                queryWhere = queryWhere + _this.VclTextBoxFilter.Text + "'.toUpperCase())) ";

            }
            _this.VclTextBoxFilter.Text = "";
        }
        queryWhere = "(" + queryWhere + ")";

        return queryWhere;
    }
    //Función para obtner una cadena de consulta con formato SQL
    this.GetStringQuery = function () {
        var queryWhere = "";
        for (var i = 0; i < _this.Elements.length; i++) {
            if (
                _this.Elements[i].VclComboBox2.Value == null
                || _this.Elements[i].VclComboBox2Operation.Value == null
                || _this.Elements[i].VclTextBox.Text == null
                || _this.Elements[i].VclTextBox.Text == ""

            ) {

                return "";
            }
            if (i > 0) {
                queryWhere = queryWhere + " " + _this.Elements[i].VclComboBox2Anexo.Text + " ";
            }
            queryWhere = queryWhere + "" + _this.Elements[i].VclComboBox2.Text + " ";
            queryWhere = queryWhere + _this.Elements[i].VclComboBox2Operation.Text + " ";
            queryWhere = queryWhere + _this.Elements[i].VclTextBox.Text + " ";

        }
        if (queryWhere != "") {
            return ": (" + queryWhere + ")";
        } else {
            return queryWhere;
        }

    }
    //Función para obtner una cadena de consulta con formato LINQ desde una lista de filtros.
    this.CreateQueryCustomFilter = function () {
        var queryWhere = "";

        for (var i = 0; i < _this.ListCustomFilter.length; i++) {
            var operator = _this.ListCustomFilter[i].OPERATOR;
            var andOR = _this.ListCustomFilter[i].ANDOR;

            operator = operator.toLowerCase();
            andOR = andOR.toLowerCase();
            if (operator == "=") {
                operator = "==";
            }
            if (operator == "<>") {
                operator = "!=";
            }
            if (andOR == "and") {
                andOR = "&&";
            }
            if (andOR == "or") {
                andOR = "||";
            }
            var indexColumn = null;
            for (var count = 1; count < _this.VclGridCF.Columns.length - 1; count++) {
                if (_this.ListCustomFilter[i].COLUMNAME == _this.VclGridCF.Columns[count].Name) {
                    indexColumn = count - 1;
                    break;
                }
            }
            if (i > 0) {
                queryWhere = queryWhere + " " + andOR + " ";
            }
            if (operator == "like") {
                queryWhere = queryWhere + "(~$[" + indexColumn + "][0].toUpperCase().indexOf('";
                queryWhere = queryWhere + _this.ListCustomFilter[i].VALUE + "'.toUpperCase())) ";
            } else {
                queryWhere = queryWhere + "$[" + indexColumn + "][0] ";
                if (_this.ListCustomFilter[i].COLUMNTYPE == SysCfg.DB.Properties.TDataType.Int32 || _this.ListCustomFilter[i].COLUMNTYPE == SysCfg.DB.Properties.TDataType.Double) {
                    queryWhere = queryWhere + operator + " ";
                    queryWhere = queryWhere + _this.ListCustomFilter[i].VALUE + " ";
                } else {
                    queryWhere = queryWhere + operator + " '";
                    queryWhere = queryWhere + _this.ListCustomFilter[i].VALUE + "' ";
                }
            }

        }
        if (queryWhere != "") {
            queryWhere = "(" + queryWhere + ")";
        }

        return queryWhere;
    }
    //Función para obtner una cadena de consulta con formato SQL desde una lista de filtros.
    this.GetStringQueryCustomFilter = function () {
        var queryWhere = "";
        for (var i = 0; i < _this.ListCustomFilter.length; i++) {
            if (i > 0) {
                queryWhere = queryWhere + " " + _this.ListCustomFilter[i].ANDOR + " ";
            }
            queryWhere = queryWhere + "" + _this.ListCustomFilter[i].COLUMNAME + " ";
            queryWhere = queryWhere + _this.ListCustomFilter[i].OPERATOR + " ";
            queryWhere = queryWhere + _this.ListCustomFilter[i].VALUE + " ";
        }
        if (queryWhere != "") {
            return "(" + queryWhere + ")";
        } else {
            return queryWhere;
        }

    }

}
/*Componente mover columnas*/
Componet.GridCF.TGridCFView.TMoveColum = function (inObject) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.Object = inObject;
    this.NameColumns = null;
    this.VclListBox = null;
    this.VclStackPanelBody = null;
    this.position = 1;
    this.positionSelected = 0;
    this.GridCFView = null;
    this.Init = function (inNameColumns, inGridCFView) {
        _this.GridCFView = inGridCFView;
        _this.NameColumns = _this.GridCFView.GetNames();
        if (_this.VclStackPanelBody == null) {
            var columnLeft = document.createElement("div");
            inObject.appendChild(columnLeft);
            var columnRight = document.createElement("div");
            var columnBotton = document.createElement("div");
            columnRight.appendChild(columnBotton);
            inObject.appendChild(columnRight);
            columnRight.style.float = "left";
            columnLeft.style.float = "left";
            columnBotton.style.textAlign = "right"
            _this.VclStackPanelBody = new TVclStackPanel(columnLeft, "1", 2, [[12, 12], [12, 12], [12, 12], [12, 12], [12, 12]]);
            _this.VclStackPanelBody.Column[0].This.style.marginTop = "-20px";
            Label = new TVcllabel(_this.VclStackPanelBody.Column[0].This, "", TlabelType.H0);
            Label.Text = "";
            _this.VclListBox = new TVclListBox(_this.VclStackPanelBody.Column[1].This, "");
            _this.VclListBox.This.Row.This.style.marginLeft = "0px";
            _this.VclListBox.This.Row.This.style.marginRight = "0px";
            _this.VclListBox.This.Row.This.style.bordeTop = "1px solid #ddd";
            _this.VclListBox.This.Row.This.style.borderBottom = "1px solid #ddd";
            _this.VclListBox.This.Row.This.style.borderLeft = "1px solid #ddd";
            columnRight.style.marginLeft = "0px";
            columnRight.style.marginRight = "0px";
            columnRight.style.bordeTop = "1px solid #ddd";
            columnRight.style.borderBottom = "1px solid #ddd";
            columnRight.style.borderRight = "1px solid #ddd";
            _this.VclListBox.EnabledCheckBox = false;
            _this.VclContenButton = new TVclStackPanel(_this.VclStackPanelBody.Column[0].This, "1", 2, [[6, 6], [6, 6], [6, 6], [6, 6], [6, 6]]);
            _this.VclContenButton.Column[0].This.style.paddingRight = "0px";
            _this.VclContenButton.Column[1].This.style.paddingLeft = "0px";
            var btnNext = new TVclButton(columnBotton, "");
            btnNext.Text = "";
            btnNext.VCLType = TVCLType.BS;
            btnNext.onClick = function () {
                var posicion = _this.VclListBox.FocusedListBoxItem.Index;
                var posicionColumn = _this.VclListBox.FocusedListBoxItem.IndexGridColumn;
                var posicionNext = _this.VclListBox.ListBoxItems[posicion + 1].IndexGridColumn;
                _this.VclListBox.ListBoxItems[posicion].IndexGridColumn = posicionNext;
                _this.VclListBox.ListBoxItems[posicion + 1].IndexGridColumn = posicionColumn;
                _this.VclListBox.Selected = posicion + 1;
                _this.GridCFView.MoveColumns(posicionColumn, posicionNext);
                btnNext.Enabled = !(posicion + 1 == _this.VclListBox.ListBoxItems.length - 1);
                btnBack.Enabled = !(posicion + 1 == 0);
                var contColTemp = 0;
                for (var i = 0; i < _this.GridCFView.nameColumns.length; i++) {
                    if (_this.GridCFView.VclGridCF.Columns[i + 1]._Visible) {
                        _this.VclListBox.ListBoxItems[contColTemp].Text = _this.GridCFView.VclGridCF.Columns[i + 1].Caption;
                        _this.VclListBox.ListBoxItems[contColTemp].IndexGridColumn = i + 1;
                        contColTemp++;
                    }
                }
                columnBotton.style.marginTop = (_this.VclListBox.FocusedListBoxItem.Index * 39) + "px";
                var evt = window.event;
                if (evt.stopPropagation) evt.stopPropagation();
                if (evt.cancelBubble != null) evt.cancelBubble = true;
            }
            var iNext = document.createElement("i");
            iNext.classList.add("icon");
            iNext.classList.add("ion-arrow-down-a");
            iNext.classList.add("text-primary");
            btnNext.This.appendChild(iNext);
            btnNext.This.style.borderRadius = "360px";
            var btnBack = new TVclButton(columnBotton, "");
            btnBack.Text = "";
            btnBack.VCLType = TVCLType.BS;
            btnBack.onClick = function () {
                var posicion = _this.VclListBox.FocusedListBoxItem.Index;
                var posicionColumn = _this.VclListBox.FocusedListBoxItem.IndexGridColumn;
                var posicionBack = _this.VclListBox.ListBoxItems[posicion - 1].IndexGridColumn;
                _this.VclListBox.ListBoxItems[posicion].IndexGridColumn = posicionBack;
                _this.VclListBox.ListBoxItems[posicion - 1].IndexGridColumn = posicionColumn;
                _this.VclListBox.Selected = posicion - 1;
                _this.GridCFView.MoveColumns(posicionColumn, posicionBack);
                btnNext.Enabled = !(posicion - 1 == _this.VclListBox.ListBoxItems.length - 1);
                btnBack.Enabled = !(posicion - 1 == 0);
                var contColTemp = 0;
                for (var i = 0; i < _this.GridCFView.nameColumns.length; i++) {
                    if (_this.GridCFView.VclGridCF.Columns[i + 1]._Visible) {
                        _this.VclListBox.ListBoxItems[contColTemp].Text = _this.GridCFView.VclGridCF.Columns[i + 1].Caption;
                        _this.VclListBox.ListBoxItems[contColTemp].IndexGridColumn = i + 1;
                        contColTemp++;
                    }
                }
                columnBotton.style.marginTop = (_this.VclListBox.FocusedListBoxItem.Index * 39) + "px";
                var evt = window.event;
                if (evt.stopPropagation) evt.stopPropagation();
                if (evt.cancelBubble != null) evt.cancelBubble = true;
            }
            btnBack.This.style.borderRadius = "360px";
            var iBack = document.createElement("i");
            iBack.classList.add("icon");
            iBack.classList.add("ion-arrow-up-a");
            iBack.classList.add("text-primary");
            btnBack.This.appendChild(iBack);
            btnNext.Enabled = !(_this.position == _this.VclListBox.ListBoxItems.length);
            btnBack.Enabled = !(_this.position == 1);
        } else {
            _this.VclListBox.ClearAll();
        }

        //var widthContent = _this.GridCFView.NavGroupMenu.GrillaNav.This.clientWidth;
        var widthContent = $(_this.GridCFView.NavGroupMenu.GrillaNav.This).width();
        columnRight.style.width = "76px"
        columnLeft.style.width = (widthContent - 76) + "px"
        _this.VclListBox.OnCheckedListBoxItemChange = function (EventArgs, Object) {

            _this.position = Object.FocusedListBoxItem.Index;
            btnNext.Enabled = !(_this.position == _this.VclListBox.ListBoxItems.length - 1);
            btnBack.Enabled = !(_this.position == 0);
            columnBotton.style.marginTop = (Object.FocusedListBoxItem.Index * 39) + "px";
        }
        var contCol = 0;
        var contColTemp = 0;
        for (var i = 0; i < _this.GridCFView.nameColumns.length; i++) {
            if (_this.GridCFView.VclGridCF.Columns[i + 1]._Visible) {
                if (contCol) {
                    _this.position = i + 1;
                }
                var ListBoxItem = new TVclListBoxItem();
                ListBoxItem.Text = _this.GridCFView.VclGridCF.Columns[i + 1].Caption;
                ListBoxItem.Index = contColTemp;
                ListBoxItem.IndexGridColumn = i + 1;
                ListBoxItem.Label.This.style.overflow = "hidden";
                ListBoxItem.Label.This.style.textOverflow = "ellipsis";
                ListBoxItem.Label.This.style.whiteSpace = "nowrap";
                _this.VclListBox.AddListBoxItem(ListBoxItem);
                var c = ListBoxItem.This.Column[0].This.childNodes;
                c[1].style.display = "none";
                ListBoxItem.Label.This.title = ListBoxItem.Text;
                contColTemp++;
            }
        }
        _this.VclListBox.Selected = 0;
        columnRight.style.height = $(columnLeft).height() + "px";
        columnBotton.style.marginTop = (_this.VclListBox.FocusedListBoxItem.Index * 39) + "px";
        columnBotton.style.backgroundColor = "lightyellow";
        columnBotton.style.height = "39px";
        btnNext.This.style.marginTop = "2px";
        btnBack.This.style.marginTop = "2px";
    }
}
/*Componente para la creacion y configuración de vistas y/o estilos en el cfview*/
Componet.GridCF.TGridCFView.TCfg = function () {
    /*Este componente configura las vistas con todas las opciones que deseamos activar en el CFVIEW. Esto nos perimite tener distintos estilos en el CFVIEW*/
    this.IsAllShowValue = false;//Para no inicializar los valores de las celdas en columnas ocultas.
    this.AutoTranslete = false;//Para que los los caption de las columnas no se traduscan.
    this.OrderColumns = [];//Orden de las columnas, por ejemplo "[Nombre,Edad,dni]"
    this.ListAgrupation = [];//Lista de agrupaciones, por ejemplo agrupar el grid por [Nombre,Edad]
    this.OptionButtonCkeck = {//Configuracion para establecer las filas en check o no.
        SetSelectedRows: true
    };
    this.DivitionColumnInformation = SysCfg.App.IniDB.ReadString("Componet_GridCF", "RowShowDetails", null);//Número de columnas del detalle
    this.ToolBar = {//Configuracion del toolbar
        ButtonImg_DeleteBar: {//Bar: eleiminar
            Active: false,
        },
        ButtonImg_AddBar: {//Bar: insertar
            Active: false,
        },
        ButtonImg_EditBar: {//Bar: editar
            Active: false,
        },
        ButtonImg_EditToolsBar: {//Bar para mostrar los bar ButtonImg_DeleteBar,ButtonImg_AddBar y ButtonImg_Ed itBar
            ChekedClickDown: false,
            Visible: false
        },
        ButtonImg_DatailsViewBar: {//Bar: detalle grid
            ChekedClickDown: false,
            Visible: true
        },
        ButtonImg_Grid: {//Bar: mostrar grid
            ChekedClickDown: true,
        },
        ButtonImg_RowCheckToolsBar: {//Bar: Check en grid
            ChekedClickDown: false,
            Visible: true
        },
        ButtonImg_ColumnGroupBar: {//Bar: agrupación
            ChekedClickDown: false,
            Visible: true
        },
        ButtonImg_RowEditToolsBar: {//Bar: mostrar los botones: insert,update,delete,post y cancel en el grid
            ChekedClickDown: false,
            Visible: true
        },
        ButtonImg_NavigatorToolsBar: {//Bar: flechas de navegacion: first, prior,last y next.
            ChekedClickDown: true,
            Visible: true
        },
        ButtonImg_StatusBar: {//Bar: status
            ChekedClickDown: false,
            Visible: true
        },
        ButtonImg_GridVisibleColumnBar: {//Bar:Columnas visibles del grid, este bar llama un a función que lista las columnas y permite al usuario escoger cual desea que sean visibles o no en le grid
            Visible: true
        },
        ButtonImg_MoveColumnsBar: {//Bar: Mover columnas
            Visible: true
        },
        ButtonImg_ContraerBar: {//Bar: Contraer filas en el tree
            Visible: true
        },
        ButtonImg_ExpanderBar: {//Bar: Expander filas en el tree
            Visible: true
        },

        ButtonImg_ExportPDFBar: {//Bar: Exportar a pdf
            Visible: true
        },
        ButtonImg_ExportExcelBar: {//Bar: Exportar a excel
            Visible: true
        },
        ButtonImg_ExportWordBar: {//Bar: Exportar a word
            Visible: true
        },

    };
    //Configuración del orden
    this.Order = {
        Type: 0,
        ColumnName: null
    };
    //Configuración de las columnas por defecto
    this.ColumnsOptionsDef = {
        Name: null,
        Caption: null,//Caption de la columna
        Style: Componet.Properties.TExtrafields_ColumnStyle.None,//Estilo de la columna
        DataEventControl: null,//Data que se envia a las columnas (Sirve para la creación de combos en las celdas)
        OnEventControl: null,//Evento para dibujar el combo en la celda
        FileSRVEvent: null,//Evento el FileSRVEvent
        FileType: null,//Tipo de File
        CustomDrawCell://Para configurar color en las celdas
        {
            data: null,
            function: null,
        },
        Show: true,//Columna del grid visible
        ShowVertical: true,//Columna del detalle del grid visible
        ModalEvent: null,//Columna con modal
        InsertActiveEditor: false,//Columna se pueda insertar
        UpdateActiveEditor: false,//Columna se pueda editar
        OptionProgressBar: {//Configuración del progress
            "Theme": 0,//Estilo en que se muestra el progress
            "ShowNumber": true,//Muestra número en el progress
            "ShowPercentage": false,//Muestra porcentaje en el progress
        }
    };
    this.Columns = new Array();//Lista de columnas que deseamos configurar
    this.ColumnsPadre = new Array();//Lista de columnas padre que sirven para heredar configuraciones
    this.ColumnsConFormato = new Array();
    this._TCfgConfig = null;
    /*Propiedad que sirve para indicar establecer un lista padre y heredar su configuración*/
    Object.defineProperty(this, 'TCfgConfig', {
        get: function () {
            return this._TCfgConfig;
        },
        set: function (inValue) {

            this._TCfgConfig = inValue;
            this.IsAllShowValue = this._TCfgConfig.IsAllShowValue;
            this.AutoTranslete = this._TCfgConfig.AutoTranslete;
            this.OrderColumns = this._TCfgConfig.OrderColumns;
            this.ListAgrupation = this._TCfgConfig.ListAgrupation;
            this.OptionButtonCkeck = this._TCfgConfig.OptionButtonCkeck;
            this.DivitionColumnInformation = this._TCfgConfig.DivitionColumnInformation;
            this.ToolBar = this._TCfgConfig.ToolBar;
            this.Order = this._TCfgConfig.Order;
            this.ColumnsPadre = this._TCfgConfig.Columns;
        }
    });
    /*Funcion para dar formato a las columnas del grid, donde se compara las configuraciones por defecto y las personalizadas*/
    this.DarFormatoAcolumnaDeGrid = function (VclGridCF) {
        this.ColumnsConFormato = new Array();
        if (typeof (this.Columns) != "undefined" && Array.isArray(this.Columns)) {
            for (var i = 0; i < VclGridCF.Columns.length; i++) {
                if (!VclGridCF.Columns[i].IsOption && !VclGridCF.Columns[i].IsOptionCheck) {
                    var arrayColumn = {
                        Name: null,
                        Caption: null,
                        Style: this.TCfgConfig.ColumnsOptionsDef.Style,
                        DataEventControl: this.TCfgConfig.ColumnsOptionsDef.DataEventControl,
                        OnEventControl: this.TCfgConfig.ColumnsOptionsDef.OnEventControl,
                        FileType: this.TCfgConfig.ColumnsOptionsDef.FileType,
                        FileSRVEvent: this.TCfgConfig.ColumnsOptionsDef.FileSRVEvent,
                        OptionProgressBar: this.TCfgConfig.ColumnsOptionsDef.OptionProgressBar,



                        CustomDrawCell:
                        {
                            data: this.TCfgConfig.ColumnsOptionsDef.CustomDrawCell.data,
                            function: this.TCfgConfig.ColumnsOptionsDef.CustomDrawCell.function,
                        },
                        Show: this.TCfgConfig.ColumnsOptionsDef.Show,
                        ShowVertical: this.TCfgConfig.ColumnsOptionsDef.ShowVertical,
                        ModalEvent: this.TCfgConfig.ColumnsOptionsDef.ModalEvent,
                        InsertActiveEditor: this.TCfgConfig.ColumnsOptionsDef.InsertActiveEditor,
                        UpdateActiveEditor: this.TCfgConfig.ColumnsOptionsDef.UpdateActiveEditor,
                    };
                    var existeColumnPadre = false;
                    if (this.ColumnsPadre.length > 0) {
                        for (var k1 = 0; k1 < this.ColumnsPadre.length; k1++) {

                            if (VclGridCF.Columns[i].Name == this.ColumnsPadre[k1].Name) {
                                if (typeof (this.ColumnsPadre[k1].Name) != "undefined")
                                    arrayColumn.Name = this.ColumnsPadre[k1].Name;

                                if (typeof (this.ColumnsPadre[k1].Caption) != "undefined")
                                    arrayColumn.Caption = this.ColumnsPadre[k1].Caption;

                                if (typeof (this.ColumnsPadre[k1].CustomDrawCell) != "undefined") {
                                    if (typeof (this.ColumnsPadre[k1].CustomDrawCell.data) != "undefined") {
                                        arrayColumn.CustomDrawCell.data = this.ColumnsPadre[k1].CustomDrawCell.data;
                                    }
                                    if (typeof (this.ColumnsPadre[k1].CustomDrawCell.function) != "undefined") {
                                        arrayColumn.CustomDrawCell.function = this.ColumnsPadre[k1].CustomDrawCell.function;
                                    }
                                }

                                if (typeof (this.ColumnsPadre[k1].Show) != "undefined")
                                    arrayColumn.Show = this.ColumnsPadre[k1].Show;

                                if (typeof (this.ColumnsPadre[k1].Style) != "undefined") {
                                    arrayColumn.Style = this.ColumnsPadre[k1].Style;
                                }

                                if (typeof (this.ColumnsPadre[k1].DataEventControl) != "undefined") {

                                    //arrayColumn.Style = this.ColumnsPadre[k1].Style;
                                    arrayColumn.DataEventControl = this.ColumnsPadre[k1].DataEventControl;
                                }
                                if (typeof (this.ColumnsPadre[k1].OnEventControl) != "undefined") {
                                    arrayColumn.Style = this.ColumnsPadre[k1].Style;
                                    arrayColumn.OnEventControl = this.ColumnsPadre[k1].OnEventControl;
                                }
                                if (typeof (this.ColumnsPadre[k1].FileType) != "undefined") {
                                    arrayColumn.FileType = this.ColumnsPadre[k1].FileType;
                                }
                                if (typeof (this.ColumnsPadre[k1].FileSRVEvent) != "undefined") {
                                    arrayColumn.FileSRVEvent = this.ColumnsPadre[k1].FileSRVEvent;
                                }
                                if (typeof (this.ColumnsPadre[k1].OptionProgressBar) != "undefined") {
                                    arrayColumn.OptionProgressBar = this.ColumnsPadre[k1].OptionProgressBar;
                                }

                                if (typeof (this.ColumnsPadre[k1].ShowVertical) != "undefined")
                                    arrayColumn.ShowVertical = this.ColumnsPadre[k1].ShowVertical;

                                if (typeof (this.ColumnsPadre[k1].ModalEvent) != "undefined")
                                    arrayColumn.ModalEvent = this.ColumnsPadre[k1].ModalEvent;

                                if (typeof (this.ColumnsPadre[k1].ActiveEditor) != "undefined")
                                    arrayColumn.ActiveEditor = this.ColumnsPadre[k1].ActiveEditor;

                                if (typeof (this.ColumnsPadre[k1].InsertActiveEditor) != "undefined")
                                    arrayColumn.InsertActiveEditor = this.ColumnsPadre[k1].InsertActiveEditor;

                                if (typeof (this.ColumnsPadre[k1].UpdateActiveEditor) != "undefined") {
                                    arrayColumn.UpdateActiveEditor = this.ColumnsPadre[k1].UpdateActiveEditor;
                                }
                                break;
                            }
                        }

                    }
                    if (!existeColumnPadre) {
                        arrayColumn.Name = VclGridCF.Columns[i].Name;
                        arrayColumn.Caption = VclGridCF.Columns[i].Name;
                    }


                    for (var j = 0; j < this.Columns.length; j++) {
                        if (this.Columns[j].Name == VclGridCF.Columns[i].Name) {
                            if (typeof (this.Columns[j].Name) != "undefined")
                                arrayColumn.Name = this.Columns[j].Name;

                            if (typeof (this.Columns[j].Caption) != "undefined")
                                arrayColumn.Caption = this.Columns[j].Caption;

                            if (typeof (this.Columns[j].CustomDrawCell) != "undefined") {
                                if (typeof (this.Columns[j].CustomDrawCell.data) != "undefined") {
                                    arrayColumn.CustomDrawCell.data = this.Columns[j].CustomDrawCell.data;
                                }
                                if (typeof (this.Columns[j].CustomDrawCell.function) != "undefined") {
                                    arrayColumn.CustomDrawCell.function = this.Columns[j].CustomDrawCell.function;
                                }
                            }

                            if (typeof (this.Columns[j].Show) != "undefined")
                                arrayColumn.Show = this.Columns[j].Show;


                            if (typeof (this.Columns[j].DataEventControl) != "undefined") {
                                //  arrayColumn.Style = this.Columns[j].Style;
                                arrayColumn.DataEventControl = this.Columns[j].DataEventControl;
                            }
                            if (typeof (this.Columns[j].Style) != "undefined") {
                                arrayColumn.Style = this.Columns[j].Style;
                            }
                            if (typeof (this.Columns[j].OnEventControl) != "undefined") {
                                arrayColumn.Style = this.Columns[j].Style;
                                arrayColumn.OnEventControl = this.Columns[j].OnEventControl;
                            }

                            if (typeof (this.Columns[j].FileType) != "undefined") {

                                arrayColumn.FileType = this.Columns[j].FileType;
                            }
                            if (typeof (this.Columns[j].FileSRVEvent) != "undefined") {

                                arrayColumn.FileSRVEvent = this.Columns[j].FileSRVEvent;
                            }
                            if (typeof (this.Columns[j].OptionProgressBar) != "undefined") {
                                arrayColumn.OptionProgressBar = this.Columns[j].OptionProgressBar;
                            }


                            if (typeof (this.Columns[j].ShowVertical) != "undefined")
                                arrayColumn.ShowVertical = this.Columns[j].ShowVertical;

                            if (typeof (this.Columns[j].ModalEvent) != "undefined")
                                arrayColumn.ModalEvent = this.Columns[j].ModalEvent;

                            if (typeof (this.Columns[j].ActiveEditor) != "undefined")
                                arrayColumn.ActiveEditor = this.Columns[j].ActiveEditor;

                            if (typeof (this.Columns[j].InsertActiveEditor) != "undefined")
                                arrayColumn.InsertActiveEditor = this.Columns[j].InsertActiveEditor;

                            if (typeof (this.Columns[j].UpdateActiveEditor) != "undefined") {
                                arrayColumn.UpdateActiveEditor = this.Columns[j].UpdateActiveEditor;
                            }
                            break;

                        }


                    }
                    this.ColumnsConFormato.push(arrayColumn);

                }

            }
        }
    }
    /*Funcion para obtner el array con todas las configuraciones de la vista*/
    this.getStyle = function (VclGridCF) {
        this.DarFormatoAcolumnaDeGrid(VclGridCF);
        var style = {
            IsAllShowValue: this.IsAllShowValue,
            AutoTranslete: this.AutoTranslete,
            OrderColumns: this.OrderColumns,
            ListAgrupation: this.ListAgrupation,
            OptionButtonCkeck: this.OptionButtonCkeck,
            DivitionColumnInformation: this.DivitionColumnInformation,
            ToolBar: this.ToolBar,
            Order: this.Order,
            Columns: this.ColumnsConFormato
        };
        return style;
    }
    /*Funcion para inicializar el componente*/
    this.Initialize = function () {
        this.TCfgConfig = this;
    }
    this.Initialize();
}
/*Componente para paginar elementos*/
Componet.GridCF.TGridCFView.TPagination = function (inObject, inAlign, inNumberRowShow, inVclComboBox) {

    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.Mythis = "TPagination";
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1", "Showing");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2", "Go");
    this.OnOpenTreeGridGroup = null;
    this.ultimoIndice = 0;
    this.NumeroFilas = null;
    if (typeof (inAlign) == "undefined" || inAlign == null) {
        this._Align = false;
    }
    else {
        this._Align = inAlign;
    }
    if (typeof (inNumberRowShow) == "undefined" || inNumberRowShow == null) {
        this._FilasMostradas = 10;
    } else {
        this._FilasMostradas = inNumberRowShow;
    }
    this.ControlCombo = inVclComboBox;//Combo de paginación
    this.IndexPage = 1;//Indice de paginación
    this.Rang = [5, 10, 20, 50, 100, 200, 500, 1000, 2000, 5000];//Rangos número de filas que se desea mostrar
    this.numeroMaximoIndices = 0;
    this.VclStackPanelPagination = null;//Div para la paginación: (Combo,paginación,boton go)
    this.VclStackPanelPaginationUL = null;//Div para la número de paginas que se forma
    this.UlPagination = null;
    this.OnEventGoTo = null;
    /*Propiedad para configurar las filasMostradas*/
    Object.defineProperty(this, 'FilasMostradas', {
        get: function () {
            return this._FilasMostradas;
        },
        set: function (inValue) {
            this._FilasMostradas = inValue;
            _this.ControlCombo.Value = inValue;
            if (_this.OnEventGoTo != null) {
                _this.OnEventGoTo(inValue);
            }
        }
    });
    /*Propiedad para configurar los padding*/
    Object.defineProperty(this, 'Padding', {
        set: function (inValue) {
            _this.VclStackPanelPagination.Row.This.style.marginRight = inValue + "px";
        }
    });
    /*Función para ir a página*/
    this.GoTo = function (page) {
        page = parseInt(page);
        if (page > 0 && _this.GetNumeroMaximoIndices() >= page && this.IndexPage != page) {
            this.IndexPage = page;
            this.Refresch();
            if (this.OnEventGoTo != null) {
                this.OnEventGoTo(page);
            }
        }

    }
    /*Función para obtner el número de paginas*/
    this.GetNumeroMaximoIndices = function () {
        var numeroMaximoIndices = parseInt(this.NumeroFilas / this.FilasMostradas) + 1;
        if (this.NumeroFilas % this.FilasMostradas == 0) {
            numeroMaximoIndices--;
        }
        return numeroMaximoIndices;
    }
    /*Función para formar la paginación*/
    this.Refresch = function () {
        if (this.NumeroFilas != null) {
            this.VclStackPanelPagination.Row.This.style.display = "";
            if (this.UlPagination != null) {
                $(this.UlPagination).html("");

            }
            this.UlPagination = new Uul(_this.VclStackPanelPaginationUL, "myPager");
            this.UlPagination.style.marginTop = "0.5px";
            this.UlPagination.style.marginBottom = "0px";
            this.UlPagination.id = "myPager";
            this.UlPagination.classList.add("pagination");
            this.numeroMaximoIndices = parseInt(this.NumeroFilas / this.FilasMostradas) + 1;
            if (this.NumeroFilas % this.FilasMostradas == 0) {
                this.numeroMaximoIndices--;
            }
            var RangePagination = this.GetRangePagination(parseInt(this.IndexPage), this.numeroMaximoIndices);
            for (var i = 0; i < RangePagination.length; i++) {
                $('<li><a class="page_link" style="z-index: 0;cursor:pointer;height:32.3px">' + (RangePagination[i]) + '</a></li>').appendTo(this.UlPagination);
            }
            var list_Li_a = $(_this.UlPagination).find('li .page_link');

            $(_this.UlPagination).find('li .page_link').click(function () {
                var clickedPage = $(this).html();
                if (clickedPage != "...") {
                    _this.GoTo(clickedPage.valueOf());
                }

                return false;
            });
            for (var i = 0; i < list_Li_a.length; i++) {
                var clickedPage = $(list_Li_a[i]).html();
                list_Li_a[i].style.height = "32.3px";
                if (clickedPage + "" == "" + this.IndexPage) {
                    $(_this.UlPagination).children().eq(i).addClass("active");
                    break;
                }
            }
            if (((this.IndexPage) * this.FilasMostradas) > this.NumeroFilas) {
                this.LabelInformation.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1") + " " + ((this.IndexPage - 1) * this.FilasMostradas + 1) + " - " + (this.NumeroFilas) + " of " + this.NumeroFilas;
            } else {
                this.LabelInformation.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1") + " " + ((this.IndexPage - 1) * this.FilasMostradas + 1) + " - " + ((this.IndexPage) * this.FilasMostradas) + " of " + this.NumeroFilas;
            }

            this.Control.This.style.display = "";
            this.ControlCombo.This.style.display = "";
            this.VclInputbuttonGo.Visible = true;
        }

    }
    /*Función para obtner los rangos en la paginación, por ejempo: 1,2,3...40,30*/
    this.GetRangePagination = function (c, m) {
        var current = c,
            last = m,
            delta = 2,
            left = current - delta,
            right = current + delta + 1,
            range = [],
            rangeWithDots = [],
            l;

        for (var i = 1; i <= last; i++) {
            if (i == 1 || i == last || i >= left && i < right) {
                range.push(i);
            }
        }

        for (var j = 0; j < range.length; j++) {
            var i = range[j];
            if (l) {
                if (i - l === 2) {
                    rangeWithDots.push(l + 1);
                } else if (i - l !== 1) {
                    rangeWithDots.push('...');
                }
            }
            rangeWithDots.push(i);
            l = i;
        }

        return rangeWithDots;
    }
    /*Función para inicializar componente*/
    this.Initialize = function () {
        if (this._Align) {//this._Align=TRUE: Paginación que NO viene del grid principal 
            this.VclStackPanelPagination = new TVclStackPanel(inObject, "1", 1, [[12], [12], [12], [12], [12]]);
            this.VclStackPanelPagination.Row.This.style.display = "none";
            var DivNavegationCombo = document.createElement("div");
            var DivNavegationInput = document.createElement("div");
            var DivNavegation = document.createElement("div");
            var DivContendorUl = document.createElement("div");
            var DivInformation = document.createElement("div");
            DivNavegationCombo.style.float = "left";
            DivNavegationInput.style.float = "left";
            DivNavegation.style.float = "left";
            DivContendorUl.style.float = "left";
            DivContendorUl.style.height = "30px";
            DivInformation.style.float = "left";
            DivNavegationCombo.style.marginRight = "4px";
            DivNavegationInput.style.marginRight = "4px";
            DivNavegation.style.marginRight = "4px";
            DivInformation.style.marginRight = "4px";
            DivContendorUl.style.marginRight = "4px";
            DivInformation.style.color = "#3e94ba";
            _this.VclStackPanelPagination.Column[0].This.appendChild(DivNavegationCombo);
            _this.VclStackPanelPagination.Column[0].This.appendChild(DivNavegationInput);
            _this.VclStackPanelPagination.Column[0].This.appendChild(DivNavegation);
            _this.VclStackPanelPagination.Column[0].This.appendChild(DivContendorUl);
            _this.VclStackPanelPagination.Column[0].This.appendChild(DivInformation);
        }
        else {
            //this._Align=false: Paginaciónpara el grid, para este caso el combo se crea en el NabTabControl
            var DivInformation = document.createElement("div");
            var DivImgGroup = document.createElement("div");
            var DivNavegationInput = document.createElement("div");
            var DivNavegation = document.createElement("div");
            var DivContendorUl = document.createElement("div");
            var DivNavegationCombo = document.createElement("div");
            if (SysCfg.App.Properties.Device == SysCfg.App.TDevice.Desktop) {
                this.VclStackPanelPagination = new TVclStackPanel(inObject, "1", 2, [[12, 12], [12, 12], [4, 8], [4, 8], [4, 8]]);
                DivImgGroup.style.float = "right";
                DivNavegation.style.float = "right";
                DivNavegationInput.style.float = "right";
                DivImgGroup.style.marginRight = "4px";
                DivNavegationInput.style.marginRight = "4px";
                DivNavegationCombo.style.float = "right";
                DivNavegationCombo.style.marginRight = "4px";
                DivNavegation.style.marginRight = "4px";
                DivContendorUl.style.float = "right";
                _this.VclStackPanelPagination.Column[0].This.appendChild(DivInformation);
                _this.VclStackPanelPagination.Column[1].This.appendChild(DivContendorUl);
                _this.VclStackPanelPagination.Column[1].This.appendChild(DivNavegation);
                _this.VclStackPanelPagination.Column[1].This.appendChild(DivNavegationInput);
                _this.VclStackPanelPagination.Column[1].This.appendChild(DivNavegationCombo);
                _this.VclStackPanelPagination.Column[1].This.appendChild(DivImgGroup);
            }
            else {
                DivImgGroup.style.float = "left";
                DivNavegation.style.float = "left";
                DivNavegationInput.style.float = "left";
                DivImgGroup.style.marginRight = "4px";
                DivNavegationInput.style.marginRight = "4px";
                DivNavegationCombo.style.float = "left";
                DivNavegationCombo.style.marginRight = "4px";
                DivNavegation.style.marginRight = "4px";
                this.VclStackPanelPagination = new TVclStackPanel(inObject, "1", 3, [[12, 12, 12], [12, 12, 12], [12, 12, 12], [12, 12, 12], [12, 12, 12]]);
                _this.VclStackPanelPagination.Column[0].This.style.display = "none";
                _this.VclStackPanelPagination.Column[0].This.appendChild(DivInformation);
                _this.VclStackPanelPagination.Column[1].This.appendChild(DivNavegation);
                _this.VclStackPanelPagination.Column[1].This.appendChild(DivNavegationInput);
                _this.VclStackPanelPagination.Column[1].This.appendChild(DivNavegationCombo);
                _this.VclStackPanelPagination.Column[1].This.appendChild(DivImgGroup);
                _this.VclStackPanelPagination.Column[1].This.appendChild(DivContendorUl);
            }
            this.VclStackPanelPagination.Row.This.style.display = "none";
            this._ImgGruopOpen = document.createElement('img');
            this._ImgGruopOpen.style.width = "16px";
            this._ImgGruopOpen.src = "image/24/arrows2-edit.png";
            this.LinkImgGruopOpen = document.createElement('button');
            this.LinkImgGruopOpen.classList.add("btn");
            this.LinkImgGruopOpen.classList.add("btn-default");
            this.LinkImgGruopOpen.appendChild(this._ImgGruopOpen);
            this.LinkImgGruopOpen.onclick = function () {
                //función para expandir una agrupación.
                if (_this.OnOpenTreeGridGroup != null) {
                    _this.OnOpenTreeGridGroup();
                }

            }
            DivImgGroup.appendChild(this.LinkImgGruopOpen);
            this.LinkImgGruopOpen.style.display = "none";
        }
        this.LabelInformation = new TVcllabel(DivInformation, "", TlabelType.H0);
        this.LabelInformation.Text = "";
        this.Control = new TVclTextBox(DivNavegationInput, "");
        this.Control.Type = TImputtype.number;
        this.VclInputbuttonGo = new TVclInputbutton(DivNavegation, "");
        this.VclInputbuttonGo.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2");
        this.VclInputbuttonGo.Visible = false;
        this.VclInputbuttonGo.This.classList.add("btn");
        this.VclInputbuttonGo.This.classList.add("btn-default");
        this.Control.This.classList.add("form");
        this.Control.This.classList.add("form-control");
        this.Control.This.style.width = "60px";
        this.Control.This.style.display = "none";
        if (this.ControlCombo != null) {
            this.ControlCombo.ClearItems();
            DivNavegationCombo.style.display = "none";
        } else {
            this.ControlCombo = new TVclComboBox2(DivNavegationCombo, "");
            this.ControlCombo.This.style.display = "none";
            this.ControlCombo.This.style.marginTop = "1px";
            this.ControlCombo.This.style.height = "33px";
            this.ControlCombo.This.style.width = "60px";
        }
        var rangosPaginacion = _this.Rang;
        for (var i = 0; i < rangosPaginacion.length; i++) {
            var VclComboBoxItem = new TVclComboBoxItem();
            VclComboBoxItem.Value = rangosPaginacion[i];
            VclComboBoxItem.Text = "&nbsp;" + rangosPaginacion[i];
            VclComboBoxItem.Tag = rangosPaginacion[i];
            _this.ControlCombo.AddItem(VclComboBoxItem);
        }
        _this.ControlCombo.onChange = function () {

            _this._FilasMostradas = _this.ControlCombo.Value;
            if (!_this._Align) {
                Componet.GridCF.Properties.VConfig.RowShowInfo = _this._FilasMostradas;
            }

            _this.IndexPage = 1;
            _this.Refresch();
            if (_this.OnEventGoTo != null) {
                _this.OnEventGoTo(1);
            }
        }
        _this.ControlCombo.Value = _this.FilasMostradas;
        if (this._Align) {
            this.ControlCombo.This.style.marginTop = "0px";
            this.ControlCombo.This.style.height = "32px";
            this.Control.This.style.height = "32px";
            this.VclInputbuttonGo.This.style.height = "32px";
        }
        var DivContendorUlContent = document.createElement("div");
        if (SysCfg.App.Properties.Device != SysCfg.App.TDevice.Desktop) {
            DivContendorUl.classList.add("ContainerNavTabControlDivScroll");
            DivContendorUlContent.classList.add("ContainerNavTabControl");
        }
        DivContendorUl.appendChild(DivContendorUlContent);
        this.VclStackPanelPaginationUL = DivContendorUlContent;
        this.VclInputbuttonGo.onClick = function () {
            if (_this.Control.Text > 0 && _this.Control.Text != _this.IndexPage) {
                _this.GoTo(_this.Control.Text);
            }
        }
    }
    this.Initialize();
}
/*Componente para crear un modal*/
Componet.GridCF.TGridCFView.TModalFV = function (inObject, referenceObject, position) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    var doc = null;
    this.DivPanel = null;
    this.Data = null;
    this.open = function () {
        this.link.click();
    }
    this.close = function () {
        this.link.click();
    }
    this.Initialize = function () {
        var elementos = document.getElementsByClassName("TModalFV");
        for (x = 0; x < elementos.length; x++) {
            elementos[x].style.display = "none";
        }
        var uiDatepicker = $("#ui-datepicker-div");
        if (typeof (uiDatepicker) != "undefined") {
            $("#ui-datepicker-div").bind("click", function (e) {
                e.stopPropagation();
            });
        }

        this.PanelPrimary = document.createElement("div");
        this.PanelPrimary.classList.add("TModalFV");
        document.body.appendChild(this.PanelPrimary);
        var CellButton = document.createElement("button");
        CellButton.classList.add("btn");
        CellButton.classList.add("btn-xs");
        var iconButton = document.createElement("i");
        iconButton.classList.add("icon");
        iconButton.classList.add("ion-close");
        CellButton.appendChild(iconButton);
        CellButton.onclick = function () {
            _this.hide();
        }
        CellButton.classList.add("TModalHeaderButton");
        var Header = document.createElement("div");
        Header.style.textAlign = "right";
        Header.appendChild(CellButton);
        this.Body = document.createElement("div");
        this.Body.classList.add("TModalBody");
        this.PanelPrimary.appendChild(Header);
        this.PanelPrimary.appendChild(this.Body);
        this.Body.appendChild(inObject);
        this.PanelPrimary.style.position = "absolute";
        var p = $(referenceObject);
        //var offset = p.offset();
        var offset = _this.getOffset($(this.PanelPrimary), p);
        this.PanelPrimary.style.top = offset.top + 0 + "px";
        this.PanelPrimary.style.left = offset.left + "px";
        doc = this.PanelPrimary.ownerDocument;
        $(_this.PanelPrimary).bind("click", function (e) {
            e.stopPropagation();
        });
        $(doc).bind("click", _this.clickout);
        $(window).bind("resize", _this.resize);
        //$(this.PanelPrimary).show("scale", {}, 500, function () { });
    }
    this.clickout = function (e) {
        var container = $(_this.PanelPrimary);
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            _this.hide();

        } else {

        }
    }
    this.resize = function (e) {


        setTimeout(function () {
            var p = $(referenceObject);
            var offset = _this.getOffset($(_this.PanelPrimary), p);
            _this.PanelPrimary.style.top = offset.top + 0 + "px";
            _this.PanelPrimary.style.left = offset.left + "px";

        }, 10);


    }
    this.hide = function () {
        if (_this.onOutside != null) {
            _this.onOutside(_this.Data);
        }
        $(_this.PanelPrimary).html("");
        _this.PanelPrimary.style.display = "none";
        $(doc).unbind("click", _this.clickout);
        $(window).unbind("resize", _this.resize);

    }
    this.getOffset = function (picker, input) {

        var extraY = 0;
        var dpWidth = picker.outerWidth();
        var dpHeight = picker.outerHeight();
        var inputHeight = input.outerHeight();
        var doc = picker[0].ownerDocument;
        var docElem = doc.documentElement;
        var viewWidth = docElem.clientWidth + $(doc).scrollLeft();
        var viewHeight = docElem.clientHeight + $(doc).scrollTop();
        var offset = input.offset();
        offset.top += inputHeight;

        offset.left -=
            Math.min(offset.left, (offset.left + dpWidth > viewWidth && viewWidth > dpWidth) ?
                Math.abs(offset.left + dpWidth - viewWidth) : 0);

        offset.top -=
            Math.min(offset.top, ((offset.top + dpHeight > viewHeight && viewHeight > dpHeight) ?
                Math.abs(dpHeight + inputHeight - extraY) : extraY));

        return offset;
    }
    this.onOutside = null;
    this.Initialize();
}