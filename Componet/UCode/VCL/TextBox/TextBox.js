﻿var TVclTextBox = function (ObjectHtml, Id) {
    this.This = Uinput(ObjectHtml, TImputtype.text, Id, "", false);
    this._VCLType;

    //#region propiedades que alteran los atributos del elemento html   

    Object.defineProperty(this, 'Text', {
        get: function () {
            return $(this.This).val();
        },
        set: function (_Value) {
            $(this.This).val(_Value);
        }
    });

    Object.defineProperty(this, 'Enabled', {
        get: function () {
            return !this.This.disabled;
        },
        set: function (_Value) {
            this.This.disabled = !_Value;
        }
    });

    Object.defineProperty(this, 'onClick', {
        get: function () {
            return this.This.onclick;
        },
        set: function (_Value) {
            this.This.onclick = _Value;
        }
    });

    Object.defineProperty(this, 'onKeyPress', {
        get: function () {
            return this.This.onkeypress;
        },
        set: function (_Value) {
            this.This.onkeypress = _Value;
        }
    });

    Object.defineProperty(this, 'onChange', {
        get: function () {
            return this.This.onchange;
        },
        set: function (_Value) {
            this.This.onchange = _Value;
        }
    });

    Object.defineProperty(this, 'onKeyUp', {
        get: function () {
            return this.This.onkeyup;
        },
        set: function (_Value) {
            this.This.onkeyup = _Value;
        }
    });

    Object.defineProperty(this, 'VCLType', {
        get: function () {
            return this._VCLType;
        },
        set: function (_Value) {
            this._VCLType = _Value;
            switch (_Value) {
                case TVCLType.AG:
                    break;
                case TVCLType.BS:
                    BootStrapCreateInpunttextbox(this.This);
                    break;
                case TVCLType.DV:
                    break;
                case TVCLType.KD:
                    break;
                default:
            }
        }
    });

    Object.defineProperty(this, 'Type', {
        get: function () {
            return this.This.type;
        },
        set: function (_Value) {
            this.This.type = _Value.name;
        }
    });

    Object.defineProperty(this, 'Visible', {
        get: function () {
            if (this.This.style.visibility == "hidden" || this.This.style.visibility == "collapse")
                return false;
            return true;
        },
        set: function (_Value) {
            if (_Value)
                this.This.style.visibility = "visible";
            else
                this.This.style.visibility = "hidden";
        }
    });
    //#endregion

    //#region propiedades que alteran los estilos del elemento html 
    Object.defineProperty(this, 'Width', {
        get: function () {
            return this.This.style.width;
        },
        set: function (_Value) {
            this.This.style.width = _Value;
        }
    });
    //#endregion

    var TParent = function () {
        return this;
    }.bind(this);
}


function VclTextBox(Object, id, VCLType, text, onclick) {

    var Res = Uinput(Object, TImputtype.text, id, text, onclick);
    switch (VCLType) {
        case TVCLType.AG:
            break;
        case TVCLType.BS:
            BootStrapCreateInpunttextbox(Res);
            break;
        case TVCLType.DV:
            break;
        case TVCLType.KD:
            break;
        default:
    }
    return Res;
}