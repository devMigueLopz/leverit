﻿var TVclInputcheckbox = function (ObjectHtml, Id)
{
    this.TParent = function ()
    {
        return this;
    }.bind(this);

    var _this = this.TParent();

    this.OnCheckedChanged = null;
    this._Tag = null;
    this.This = Uinput(ObjectHtml, TImputtype.checkbox, Id, "", false);    



    this.lbl = Ulabel(ObjectHtml, "", "");
    $(this.lbl).attr("for", this.This.id);
    this.lbl.style.marginLeft = "4px";
    $(this.lbl).css("font-weight", "normal");

    Object.defineProperty(this, 'Id', {
        get: function ()
        {
            return this.This.id;
        },
        set: function (_Value)
        {
            this.This.id = _Value;
            $(this.lbl).attr("for", _Value);
        }
    });

    Object.defineProperty(this, 'Text', {
        get: function ()
        {
            return this.lbl.innerHTML;
        },
        set: function (_Value)
        {
            this.lbl.innerHTML = _Value;
        }
    });

    Object.defineProperty(this, 'Checked', {
        get: function () {
            return this.This.checked;
        },
        set: function (_Value) {
            this.This.checked = _Value;
        }
    });

    Object.defineProperty(this, 'Enabled', {
        get: function () {
            return !this.This.disabled;
        },
        set: function (_Value) {
            this.This.disabled = !_Value;
        }
    });

    Object.defineProperty(this, 'Tag', {
        get: function () {
            return this._Tag;
        },
        set: function (Value) {
            this._Tag = Value;
        }
    });

    Object.defineProperty(this, 'onClick', {
        get: function () {
            return this.This.onclick;
        },
        set: function (_Value) {
            this.This.onclick = _Value;
            //this.This.addEventListener("click", function () {

            //});
        }
    });

    Object.defineProperty(this, 'VCLType', {
        get: function () {
            return this.VCLType;
        },
        set: function (_Value) {
            switch (_Value) {
                case TVCLType.AG:
                    break;
                case TVCLType.BS:
                    BootStrapCreateInpuntcheckbox(this.This);
                    break;
                case TVCLType.DV:
                    break;
                case TVCLType.KD:
                    break;
                default:
            }
        }
    });

    Object.defineProperty(this, 'Width', {
        get: function () {
            return this.This.style.width;
        },
        set: function (_Value) {
            this.This.style.width = _Value;
        }
    });

    Object.defineProperty(this, 'Visible', {
        get: function ()
        {
            if (this.This.style.visibility == "hidden" || this.This.style.visibility == "collapse")
                return false;
            return true;
        },
        set: function (_Value)
        {
            if (_Value)
            {
                this.This.style.visibility = "visible";
                this.lbl.style.visibility = "visible";
            }
            else
            {
                this.This.style.visibility = "hidden";
                this.lbl.style.visibility = "hidden";
            }
        }
    });

    Object.defineProperty(this, 'OnCheckedChange', {
        get: function () {
            return this.This.onchange;
        },
        set: function (_Value)
        {
            this.This.onchange = _Value;            
        }
    });


    this.This.onchange = function ()
    {
        if (_this.OnCheckedChanged != null)
            _this.OnCheckedChanged(_this);
    };

    this.Click = function () {
        this.This.click();
    }

   
}
/*<summary>
Midifico fecha   
Carlos     24-04-2017
Funcion: Crea un elemento html de tipo Input
ejemplos:
VclInputcheckbox(Elementhtm, Imputtype.text, "Id del elemento", "descripcion", true);
</summary>
<param name="Object">Elemento html al cual se le insertara el elemento creado por esta funcion</param>

<param name="id">Id de elemento creado</param>
<param name="value">el valor por defecto que va a contener el input</param>
<param name="onclick">valor bool que indica si se crea el evento click para el input generado</param>
<returns>Retorna un elemento input html</returns>*/
function VclInputcheckbox(Object, id, VCLType, value, onclick) {
    var Res = new TVclInputcheckbox()
    Res.This = Uinput(Object, TImputtype.checkbox, id, value, onclick);
    switch (VCLType) {
        case TVCLType.AG:
            break;
        case TVCLType.BS:
            BootStrapCreateInpuntcheckbox(Res.This);
            break;
        case TVCLType.DV:
            break;
        case TVCLType.KD:
            break;
        default:
    }
    return Res;
}