﻿var TTabControl = function (inObjectHTML, ID, inCallBack)
{
    this.This;
    this.OnChangeTab = null;
    this.OnBeforeChangeTab = null;
    this.OnAfterChangeTab = null;
    this.TabPageSelectOld = null;
    this.TabPageSelect = null;
    this._TabPages = new Array();
    this.TabsContainer = Uul(inObjectHTML, "");//<ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
    this.TabsContainer.id = "tabs";
    this.PageContainer = Udiv(inObjectHTML, ""); //<div id="my-tab-content" class="tab-content">

    // Inicio de estilos
    $(this.TabsContainer).attr("data-tabs", "tabs");
    $(this.TabsContainer).addClass("nav");
    $(this.TabsContainer).addClass("nav-tabs");

    $(this.PageContainer).addClass("tab-content");
    // Fin de estilos 

    Object.defineProperty(this, 'TabPages', {
        get: function ()
        {
            return this._TabPages;
        },
        set: function (inValue)
        {
            this._TabPages = inValue;
        }
    });

    this.ClearTabPages = function ()
    {
        //alert("paso por aki");
        this._TabPages = new Array();
        $(this.TabsContainer).html("");
        $(this.PageContainer).html("");
    }

    this.ClearTabPagesExcept = function (namePage)
    {
        //alert("paso por aki");

        for (var i = 0; i < this._TabPages.length; i++)
        {
            if (this._TabPages[i].Name != namePage)
            {
                this._TabPages[i].Page.parentNode.removeChild(this._TabPages[i].Page);
                this._TabPages[i].Tab.parentNode.removeChild(this._TabPages[i].Tab);
                this._TabPages.splice(i, 1);
            }
        }
    }

    this.AddTabPages = function (inTabPage)
    {
        inTabPage.ControlParent = this;
        inTabPage.Index = this._TabPages.length;

        $(this.TabsContainer).append(inTabPage.Tab);
        $(this.PageContainer).append(inTabPage.Page);
        this._TabPages.push(inTabPage);

    }


}

var TTabPage = function ()
{
    var _this = this;
    this.Tab = Uli("", "");
    this.TabLink = Ua(this.Tab, "", "");//<li><a href="#tab2" data-toggle="tab">Java</a></li>    
    this.Page = Udiv("", "");//<div class="tab-pane  active" id="tab1">
    this._Name;
    this._Active = false;
    this._Tag;
    this.ControlParent = null;

    //Inicio stilos de tabs y conteiner tabs
    $(this.TabLink).attr("data-toggle", "tab");
    $(this.Page).addClass("tab-pane");
    //fin de estilos de tabs y conteiner tabs



    Object.defineProperty(this, 'Name', {
        get: function ()
        {
            return this._Name;
        },
        set: function (inValue)
        {
            this._Name = inValue;
            $(this.TabLink).attr("href", "#" + inValue);
            this.Page.id = inValue;
        }
    });

    Object.defineProperty(this, 'Index', {
        get: function ()
        {
            return this._Index;
        },
        set: function (inValue)
        {
            this._Index = inValue;
        }
    });

    Object.defineProperty(this, 'Active', {
        get: function ()
        {
            return this._Active;
        },
        set: function (inValue)
        {
            if (inValue)
            {
                $(this.Tab).siblings().removeClass("active");
                $(this.Tab).addClass("active");

                $(this.Page).siblings().removeClass("active");
                $(this.Page).addClass("active");

                if (this.ControlParent != null)
                {
                    this.ControlParent.TabPageSelectOld = this.ControlParent.TabPageSelect;
                    this.ControlParent.TabPageSelect = this;
                }

            }
            else
            {
                $(this.Container).removeClass("active");
                $(this.Page).removeClass("active");
            }



            this._Active = inValue;
        }
    });

    Object.defineProperty(this, 'Caption', {
        get: function ()
        {
            return this._Caption;
        },
        set: function (inValue)
        {
            this._Caption = inValue;
            $(this.TabLink).html(inValue);
        }
    });

    Object.defineProperty(this, 'Tag', {
        get: function ()
        {
            return this._Tag;
        },
        set: function (inValue)
        {
            this._Tag = inValue;
        }
    });

    this.AddContentHTML = function (inContent)
    {
        $(this.Page).append(inContent);
    }

    this.TabLink.onclick = function ()
    {
        if (_this.ControlParent != null)
        {
            _this.Active = true;
            if (_this.ControlParent.OnBeforeChangeTab != null)
            {
                _this.ControlParent.OnBeforeChangeTab(_this.ControlParent, _this);
            }

            _this.ControlParent.TabPageSelectOld = _this.ControlParent.TabPageSelect;
            _this.ControlParent.TabPageSelect = _this;

            if (_this.ControlParent.OnChangeTab != null)
            {
                _this.ControlParent.OnChangeTab(_this.ControlParent, _this);
            }

            if (_this.ControlParent.OnAfterChangeTab != null)
            {
                _this.ControlParent.OnAfterChangeTab(_this.ControlParent, _this);
            }
        }
    }
}