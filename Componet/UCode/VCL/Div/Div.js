﻿var TVclDiv = function (ObjectHtml) {
    var TParent = function () {
        return this;
    }.bind(this);
    this.This = ObjectHtml;
    this._DesignClient = false;
    this._LeftClient = "0px";
    this._TopClient = "0px";
    this._BottomClient = "0px";
    this._RightClient = "0px";
    this._HeightClient = "0px";
    this._WidthClient = "0px";

    Object.defineProperty(this, 'BackgroundColor', {
        get: function () {
            return this.This.style.backgroundColor;
        },
        set: function (_Value) {
            this.This.style.backgroundColor = _Value;
        }
    });

    Object.defineProperty(this, 'Border', {//"0px solid";
        get: function () {
            return this.This.style.border;
        },
        set: function (_Value) {
            this.This.style.border = _Value;
        }
    });
    Object.defineProperty(this, 'BoderTop', {//"0px solid";
        get: function () {
            return this.This.style.borderTop;
        },
        set: function (_Value) {
            this.This.style.borderTop = _Value;
        }
    });
    Object.defineProperty(this, 'BoderBottom', {//"0px solid";
        get: function () {
            return this.This.style.borderBottom;
        },
        set: function (_Value) {
            this.This.style.borderBottom = _Value;
        }
    });
    Object.defineProperty(this, 'BoderLeft', {//"0px solid";
        get: function () {
            return this.This.style.borderLeft;
        },
        set: function (_Value) {
            this.This.style.borderLeft = _Value;
        }
    });
    Object.defineProperty(this, 'BoderRight', {//"0px solid";
        get: function () {
            return this.This.style.borderRight;
        },
        set: function (_Value) {
            this.This.style.borderRight = _Value;
        }
    });
    /*
    var borderTop = "3px solid #F39C12";
    var borderBottom = "1px solid #E6E6E6";
    var borderLeft = "1px solid #E6E6E6";
    var orderRight = "1px solid #E6E6E6";
    */

    Object.defineProperty(this, 'BoderTopColor', {
        get: function () {
            return this.This.style.borderTopColor;
        },
        set: function (_Value) {
            this.This.style.borderTopColor = _Value;
        }
    });
    Object.defineProperty(this, 'BoderBottomColor', {
        get: function () {
            return this.This.style.borderBottomColor;
        },
        set: function (_Value) {
            this.This.style.borderBottomColor = _Value;
        }
    });
    Object.defineProperty(this, 'BoderLeftColor', {
        get: function () {
            return this.This.style.borderLeftColor;
        },
        set: function (_Value) {
            this.This.style.borderTopColor = _Value;
        }
    });
    Object.defineProperty(this, 'BoderRightColor', {
        get: function () {
            return this.This.style.borderRightColor;
        },
        set: function (_Value) {
            this.This.style.borderRightColor = _Value;
        }
    });
    Object.defineProperty(this, 'BoderAllColor', {
        get: function () {
            return this.This.style.borderBottomColor;
        },
        set: function (_Value) {
            this.This.style.borderTopColor = _Value
            this.This.style.borderBottomColor = _Value
            this.This.style.borderLeftColor = _Value
            this.This.style.borderRightColor = _Value
        }
    });
    Object.defineProperty(this, 'Margin', {//"0px"
        get: function () {
            return this.This.style.margin;
        },
        set: function (_Value) {
            this.This.style.margin = _Value;
        }
    });
    Object.defineProperty(this, 'MarginTop', {//"0px"
        get: function () {
            return this.This.style.marginTop;
        },
        set: function (_Value) {
            this.This.style.marginTop = _Value;
        }
    });
    Object.defineProperty(this, 'MarginBottom', {//"0px"
        get: function () {
            return this.This.style.marginBottom;
        },
        set: function (_Value) {
            this.This.style.marginBottom = _Value;
        }
    });
    Object.defineProperty(this, 'MarginLeft', {//"0px"
        get: function () {
            return this.This.style.marginLeft;
        },
        set: function (_Value) {
            this.This.style.marginLeft = _Value;
        }
    });
    Object.defineProperty(this, 'MarginRight', {//"0px"
        get: function () {
            return this.This.style.marginRight;
        },
        set: function (_Value) {
            this.This.style.marginRight = _Value;
        }
    });

    Object.defineProperty(this, 'Padding', {//"0px"
        get: function () {
            return this.This.style.padding;
        },
        set: function (_Value) {
            this.This.style.padding = _Value;
        }
    });
    Object.defineProperty(this, 'PaddingTop', {//"0px"
        get: function () {
            return this.This.style.paddingTop;
        },
        set: function (_Value) {
            this.This.style.paddingTop = _Value;
        }
    });
    Object.defineProperty(this, 'PaddingBottom', {//"0px"
        get: function () {
            return this.This.style.paddingBottom;
        },
        set: function (_Value) {
            this.This.style.paddingBottom = _Value;
        }
    });
    Object.defineProperty(this, 'PaddingLeft', {//"0px"
        get: function () {
            return this.This.style.paddingLeft;
        },
        set: function (_Value) {
            this.This.style.paddingLeft = _Value;
        }
    });
    Object.defineProperty(this, 'PaddingRight', {//"0px"
        get: function () {
            return this.This.style.paddingRight;
        },
        set: function (_Value) {
            this.This.style.paddingRight = _Value;
        }
    });

    Object.defineProperty(this, 'Visible', {
        get: function () {
            if (this.This.style.display == "none")
                return false;
            return true;
        },
        set: function (_Value) {           
            if (_Value)
                this.This.style.display = "block";
            else
                this.This.style.display = "none";
        }
    });
    Object.defineProperty(this, 'Height', {//"0px"
        get: function () {
            return this.This.style.height;
        },
        set: function (_Value) {
            this.This.style.height = _Value;
        }
    });
    Object.defineProperty(this, 'Width', {//"0px"
        get: function () {
            return this.This.style.width;
        },
        set: function (_Value) {
            if (this.This !== null) {
                this.This.style.width = _Value;
            }

        }
    });
    Object.defineProperty(this, 'Top', {//"0px"
        get: function () {
            return this.This.style.top;
        },
        set: function (_Value) {
            if (this.This !== null) {
                this.This.style.top = _Value;
            }
        }
    });
    Object.defineProperty(this, 'Left', {//"0px"
        get: function () {
            return this.This.style.left;
        },
        set: function (_Value) {
            if (this.This !== null) {
                this.This.style.left = _Value;
            }

        }
    });
    Object.defineProperty(this, 'Right', {//"0px"
        get: function () {
            return this.This.style.right;
        },
        set: function (_Value) {
            if (this.This !== null) {
                this.This.style.right = _Value;
            }
        }
    });
    Object.defineProperty(this, 'Bottom', {//"0px"
        get: function () {
            return this.This.style.bottom;
        },
        set: function (_Value) {
            if (this.This !== null) {
                this.This.style.bottom = _Value;
            }

        }
    });
    /**************Plugins Graficos --- No Tocar**********/
    Object.defineProperty(this, 'DesignClient', {
        get: function () {
            return this._DesignClient;
        },
        set: function (_Value) {
            this._DesignClient = _Value;
            if (this._DesignClient) {
                if (this.This !== null) {
                    this.WidthClient = this._WidthClient;
                    this.HeightClient = this._HeightClient;
                    this.TopClient = this._TopClient;
                    this.LeftClient = this._LeftClient;
                }
            }
            else {
                this.This.removeAttribute("style");
            }
        }
    });
    Object.defineProperty(this, 'LeftClient', {//"0px"
        get: function () {
            return this._LeftClient;
        },
        set: function (_Value) {
            this._LeftClient = _Value;
            if (this.This !== null) {
                if (this._DesignClient) {
                    this.This.style.removeProperty("width");
                    var ParentNode = this.This.getBoundingClientRect();
                    var ParentNodeWidth = parseFloat(ParentNode.width);
                    var computedStyle = getComputedStyle(this.This)
                    var elementPadding = parseFloat(computedStyle.paddingLeft) + parseFloat(computedStyle.paddingRight);
                    ParentNodeWidth -= elementPadding;
                    var nodeLeft = Number.parseFloat(_Value);
                    var TotalWidth = 0;
                    TotalWidth = parseFloat(this._WidthClient) + nodeLeft;
                    if (TotalWidth <= ParentNodeWidth) {
                        this.This.style.left = _Value;
                    }
                    if (parseFloat(this._WidthClient) <= ParentNodeWidth) {
                        this.This.style.width = this._WidthClient;
                    }
                }
            }
        }
    });
    Object.defineProperty(this, 'TopClient', {//"0px"
        get: function () {
            return this._TopClient;
        },
        set: function (_Value) {
            this._TopClient = _Value;
            if (this.This !== null) {
                if (this._DesignClient) {
                    this.This.style.removeProperty("height");
                    var ParentNode = this.This.getBoundingClientRect();
                    var ParentNodeHeight = parseFloat(ParentNode.height);
                    var computedStyle = getComputedStyle(this.This)
                    var elementPadding = parseFloat(computedStyle.paddingTop) + parseFloat(computedStyle.paddingBottom);
                    ParentNodeHeight -= elementPadding;
                    var nodeTop = Number.parseFloat(_Value);
                    var TotalHeight = 0;
                    TotalHeight = parseFloat(this._HeightClient) + nodeTop;
                    if (TotalHeight <= ParentNodeHeight) {
                        this.This.style.top = _Value;
                    }
                    if (parseFloat(this._HeightClient) <= ParentNodeHeight) {
                        this.This.style.height = this._HeightClient;
                    }
                }
            }
        }
    });
    Object.defineProperty(this, 'HeightClient', {//"0px"
        get: function () {
            return this._HeightClient;
        },
        set: function (_Value) {
            this._HeightClient = _Value;
            if (this._DesignClient) {
                if (this.This !== null) {
                    this.This.style.removeProperty("height");
                    var ParentNode = this.This.getBoundingClientRect();
                    var ParentNodeHeight = parseFloat(ParentNode.height);
                    var computedStyle = getComputedStyle(this.This)
                    var elementPadding = parseFloat(computedStyle.paddingTop) + parseFloat(computedStyle.paddingBottom);
                    ParentNodeHeight -= elementPadding;
                    if (Number.parseFloat(_Value) <= ParentNodeHeight) {
                        this.This.style.height = _Value;
                    }
                }
            }
        }
    });
    Object.defineProperty(this, 'WidthClient', {
        get: function () {
            return this._WidthClient;
        },
        set: function (_Value) {
            this._WidthClient = _Value;
            if (this._DesignClient) {
                if (this.This !== null) {
                    this.This.style.removeProperty("width");
                    var ParentNode = this.This.getBoundingClientRect();
                    var ParentNodeWidth = parseFloat(ParentNode.width);
                    var computedStyle = getComputedStyle(this.This)
                    var elementPadding = parseFloat(computedStyle.paddingLeft) + parseFloat(computedStyle.paddingRight);
                    ParentNodeWidth -= elementPadding;
                    if (Number.parseInt(_Value) <= ParentNodeWidth) {
                        this.This.style.width = _Value;
                    }
                }
            }
        }
    });
    /********************************************/


    this.AddtHtml = function (ObjectHtml) {
        $(this.Row).append(ObjectHtml);
    }
    this.Clean = function () {
        $(this.Row).html("");
    }

}
