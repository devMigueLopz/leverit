﻿//1 8-03-2018 james agrega label null﻿

var TVclButton = function (ObjectHtml, Id) {
    this.This = Ubutton(ObjectHtml, TButtontype.submit, Id, "", false);
    this._VCLType;
    this._Tag;
    this._Src;
    this._AlignImage = null;
    this.ObjectButton = null;
    this._img = null;
    this.Label = null;

    Object.defineProperty(this, 'onClickObjectButton', {
        get: function () {
            return this.This.onclick;
        },
        set: function (_inValue) {
            var nuevo = this.ObjectButton;
            this.This.onclick = function () {
                _inValue(nuevo);
            };

        }
    });

    //#region propiedades que alteran los atributos del elemento html    
    Object.defineProperty(this, 'Text', {
        get: function () {
            return $(this.This).val();
        },
        set: function (_Value) {
            this._Text = _Value;
            $(this.This).html("");
            if (this._ImageLocation == "BeforeText") {
                if (this._Src != "" && this._Src != null && this._Src != undefined) {
                    this._img = Uimg(this.This, "", this._Src, "");
                    this._img.style.padding = "10px";
                }
                this.Label = new TVcllabel(this.This, "", TlabelType.H0);
                this.Label.Text = this._Text;
                this.Label.VCLType = TVCLType.BS;
                //$(this.This).append(this.Label);
                $(this.Label.This).css("font-weight", "normal");

            } else {
                this.Label = new TVcllabel(this.This, "", TlabelType.H0);
                this.Label.Text = this._Text;
                this.Label.VCLType = TVCLType.BS;
                //$(this.This).append(this.Label);
                $(this.Label.This).css("font-weight", "normal");

                if (this._Src != "" && this._Src != null && this._Src != undefined) {
                    this._img = Uimg(this.This, "", this._Src, "");
                    this._img.style.padding = "10px";
                }
            }
        }
    });

    Object.defineProperty(this, 'Enabled', {
        get: function () {
            return !this.This.disabled;
        },
        set: function (_Value) {
            this.This.disabled = !_Value;
        }
    });

    Object.defineProperty(this, 'Tag', {
        get: function () {
            return this._Tag;
        },
        set: function (_Value) {
            this._Tag = _Value;
        }
    });

    Object.defineProperty(this, 'Src', {
        get: function () {
            return this._Src;
        },
        set: function (_Value) {
            this._Src = _Value;
            if (_Value != "" && _Value != null) {
                $(this.This).html("");
                if (this._ImageLocation == "BeforeText") {
                    this._img = Uimg(this.This, "", this._Src, "");
                    this._img.style.padding = "10px";

                    this.Label = new TVcllabel(this.This, "", TlabelType.H0);
                    this.Label.Text = this._Text;
                    this.Label.VCLType = TVCLType.BS;
                    $(this.Label.This).css("font-weight", "normal");
                    //$(this.This).append(this.Label.This);
                } else {
                    this.Label = new TVcllabel(this.This, "", TlabelType.H0);
                    this.Label.Text = this._Text;
                    this.Label.VCLType = TVCLType.BS;
                    //$(this.This).append(this.Label.This);
                    $(this.Label.This).css("font-weight", "normal");

                    this._img = Uimg(this.This, "", this._Src, "");
                    this._img.style.padding = "10px";
                }
            }
        }
    });
    /*

    Object.defineProperty(this, 'Src', {
        get: function () {
            return this._Src;
        },
        set: function (_Value) {
            this._Src = _Value;
            if (_Value != "" && _Value != null) {
                $(this.This).html("");
                if (this._ImageLocation == "BeforeText") {
                    var _img = Uimg(this.This, "", this._Src, "");
                    _img.style.padding = "10px";
                    $(this.This).append(this._Text);
                } else {
                    $(this.This).append(this._Text);
                    var _img = Uimg(this.This, "", this._Src, "");
                    _img.style.padding = "10px";
                }
            }                   
        }
    });
    */

    //this.This.onclick = function myfunction() {

    //};

    Object.defineProperty(this, 'onClick', {
        get: function () {
            return this.This.onclick;
        },
        set: function (_Value) {
            this.This.onclick = _Value;
        }
    });


    Object.defineProperty(this, 'Image', {
        get: function () {
            return this._img;
        },
        set: function (_Value) {
            this._img = _Value;
        }
    });

    Object.defineProperty(this, 'VCLType', {
        get: function () {
            return this._VCLType;
        },
        set: function (_Value) {
            this._VCLType = _Value;
            switch (_Value) {
                case TVCLType.AG:
                    break;
                case TVCLType.BS:
                    BootStrapCreateButton(this.This);
                    break;
                case TVCLType.DV:
                    break;
                case TVCLType.KD:
                    break;
                default:
            }
        }
    });
    //#endregion

    //#region propiedades que alteran los estilos del elemento html 
    Object.defineProperty(this, 'Width', {
        get: function () {
            return this.This.style.width;
        },
        set: function (_Value) {
            this.This.style.width = _Value;
        }
    });

    Object.defineProperty(this, 'Visible', {
        get: function () {
            if (this.This.style.visibility == "hidden" || this.This.style.visibility == "collapse")
                return false;
            return true;
        },
        set: function (_Value) {
            if (_Value)
                this.This.style.visibility = "visible";
            else
                this.This.style.visibility = "hidden";
        }
    });
    //#endregion

    var TParent = function () {
        return this;
    }.bind(this);
}


/*<summary>
 Midifico fecha   
 Carlos     24-04-2017
 Funcion: Crea un elemento html de tipo button 
 ejemplos:
 Vclbutton(Elementhtm, TButtontype.submit, "Id del elemento", "descripcion", true);
 </summary>
 <param name="Object">Elemento html al cual se le insertara el elemento creado por esta funcion</param>
 <param name="Imputtype">tipo de button que se va a crear</param>
 <param name="id">Id de elemento creado</param>
 <param name="value">descripcion del button</param>
 <param name="onclick">valor bool que indica si se crea el evento click para el button generado</param>
 <returns>Retorna un elemento input html</returns>*/
function VclButton(Object, Buttontype, id, VCLType, value, onclick) {
    var Res = new TVclButton();
    Res.This = Ubutton(Object, Buttontype, id, value, onclick);
    switch (VCLType) {
        case TVCLType.AG:
            break;
        case TVCLType.BS:
            BootStrapCreateButton(Res.This)
            break;
        case TVCLType.DV:
            break;
        case TVCLType.KD:
            break;
        default:
    }
    return Res;
}

var TVclInputbutton = function (ObjectHtml, Id) {
    this.This = Uinput(ObjectHtml, TImputtype.button, Id, "", false);
    this._Tag;
    this.ObjectButton = null;

    Object.defineProperty(this, 'getThis', {
        get: function () {
            return $(this.This);
        }
    });
    Object.defineProperty(this, 'Text', {
        get: function () {
            return $(this.This).val();
        },
        set: function (_Value) {
            $(this.This).val(_Value);
        }
    });

    Object.defineProperty(this, 'Enabled', {
        get: function () {
            return !this.This.disabled;
        },
        set: function (_Value) {
            this.This.disabled = !_Value;
        }
    });

    Object.defineProperty(this, 'Tag', {
        get: function () {
            return this._Tag;
        },
        set: function (_Value) {
            this._Tag = _Value;
        }
    });

    Object.defineProperty(this, 'onClick', {
        get: function () {
            return this.This.onclick;
        },
        set: function (_Value) {
            this.This.onclick = _Value;
            //this.This.addEventListener("click", function () {

            //});
        }
    });
    Object.defineProperty(this, 'onClickObjectButton', {
        get: function () {
            return this.This.onclick;
        },
        set: function (_inValue) {
            var nuevo = this.ObjectButton;
            this.This.onclick = function () {
                _inValue(nuevo);
            };

        }
    });

    Object.defineProperty(this, 'VCLType', {
        get: function () {
            return this.VCLType;
        },
        set: function (_Value) {
            switch (_Value) {
                case TVCLType.AG:
                    break;
                case TVCLType.BS:
                    BootStrapCreateInpuntButton(this.This);
                    break;
                case TVCLType.DV:
                    break;
                case TVCLType.KD:
                    break;
                default:
            }
        }
    });

    Object.defineProperty(this, 'Width', {
        get: function () {
            return this.This.style.width;
        },
        set: function (_Value) {
            this.This.style.width = _Value;
        }
    });

    Object.defineProperty(this, 'Visible', {
        get: function () {
            if (this.This.style.visibility == "hidden" || this.This.style.visibility == "collapse")
                return false;
            return true;
        },
        set: function (_Value) {
            if (_Value)
                this.This.style.visibility = "visible";
            else
                this.This.style.visibility = "hidden";
        }
    });

    this.Click = function () {
        this.This.click();
    }

    var TParent = function () {
        return this;
    }.bind(this);
}

/*<summary>
 Midifico fecha   
 Carlos     24-04-2017
 Funcion: Crea un elemento html de tipo Input
 ejemplos:
 VclInputbutton(Elementhtm, Imputtype.text, "Id del elemento", "descripcion", true);
 </summary>
 <param name="Object">Elemento html al cual se le insertara el elemento creado por esta funcion</param>
 
 <param name="id">Id de elemento creado</param>
 <param name="value">el valor por defecto que va a contener el input</param>
 <param name="onclick">valor bool que indica si se crea el evento click para el input generado</param>
 <returns>Retorna un elemento input html</returns>*/
function VclInputbutton(Object, id, VCLType, value, onclick) {
    var Res = new TVclInputbutton();
    Res.This = Uinput(Object, TImputtype.button, id, value, onclick);
    switch (VCLType) {
        case TVCLType.AG:
            break;
        case TVCLType.BS:
            BootStrapCreateInpuntButton(Res.This);
            break;
        case TVCLType.DV:
            break;
        case TVCLType.KD:
            break;
        default:
    }
    return Res;
}