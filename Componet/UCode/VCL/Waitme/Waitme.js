﻿var TvclWaitme = function ()
{
    this.ObjectHtml = null;
    this.DivBackground = null;

    this.ShowWaitme = function (inObjectHtml) {
        this.ObjectHtml = inObjectHtml;
        if (this.ObjectHtml == null || this.ObjectHtml == undefined)
            this.ObjectHtml = document.body;

        var oDivBackground = Udiv(this.ObjectHtml, "");
        this.DivBackground = new TVclDiv(oDivBackground);

        //aplicacion de styles
        this.DivBackground.This.style.zIndex = "1500";
        this.DivBackground.This.style.transition = "all 1s ease";
        this.DivBackground.This.style.position = "fixed";
        this.DivBackground.This.style.left = "0";
        this.DivBackground.This.style.top = "0";
        this.DivBackground.This.style.width = "100%";
        this.DivBackground.This.style.height = "100%";
        this.DivBackground.This.style.overflow = "auto";
        this.DivBackground.This.style.left = "0";
        this.DivBackground.This.style.backgroundColor = "rgba(0,0,0,0.4)";

        var oDivLoader = Udiv(this.DivBackground.This, "");
        this.DivLoader = new TVclDiv(oDivLoader);
        this.DivLoader.This.style.border = "15px solid #ccc";
        this.DivLoader.This.style.borderTopColor = "#367fa9";
        this.DivLoader.This.style.borderTopStyle = "inset";
        this.DivLoader.This.style.height = "100px";
        this.DivLoader.This.style.width = "100px";
        this.DivLoader.This.style.borderRadius = "100%";
        this.DivLoader.This.style.position = "absolute";
        this.DivLoader.This.style.left = "0";
        this.DivLoader.This.style.top = "0";
        this.DivLoader.This.style.right = "0";
        this.DivLoader.This.style.bottom = "0";
        this.DivLoader.This.style.margin = "auto";
        this.DivLoader.This.style.animation = "girarLoader 1.5s linear infinite";

    }
    this.CloseWaitme = function () {
        if (this.DivBackground.This.parentNode != undefined)
            this.DivBackground.This.parentNode.removeChild(this.DivBackground.This);
    }
}
