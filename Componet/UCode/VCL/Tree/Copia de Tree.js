﻿var TVclTree = function (ObjectHtml, Id) {
    this.This = Uul(ObjectHtml, Id);
    this.TreeNodeRootList = new Array();
    this.TreeNodeList = new Array();
    this.TreeNodes = new Array();
    this._Tag;
    
    this.AddTreeNodeRoot = function (Ruta,inImg) {
        var NodePosition = null;
        var NodeRoot = null;
        var words = Ruta.split('\\');

        for (var Counter = 0; Counter < words.length; Counter++) {
            var thirdWordFromRight = (Counter >= 0 ? words[Counter] : null);
            var Existe = false;
            if (Counter == 0) {
                for (var CounterNodo = 0; CounterNodo < this.TreeNodes.length; CounterNodo++) {
                    if (this.TreeNodes[CounterNodo].Text == thirdWordFromRight) {
                        NodePosition = this.TreeNodes[CounterNodo];
                        NodeRoot = this.TreeNodes[CounterNodo];
                        Existe = true;
                    }
                }
                if (Existe == false) {
                    var _node = new TVclTreeNode(null, null);
                    _node.Text = thirdWordFromRight;
                    _node.IsRoot = true;
                    this.TreeNodes.push(_node);
                    this.TreeNodeList.push(_node);
                    this.TreeNodeRootList.push(_node);
                    this.This.appendChild(_node.This);
                    _node.This.style.paddingTop = "5px";
                    _node.This.style.paddingBottom = "5px";
                    NodePosition = _node;
                    NodeRoot = _node;
                }
            }
            else {
                for (var CounterNodo = 0; CounterNodo < NodePosition.TreeNodes.length; CounterNodo++) {
                    if (NodePosition.TreeNodes[CounterNodo].Text == thirdWordFromRight) {
                        NodePosition = NodePosition.TreeNodes[CounterNodo];
                        Existe = true;
                    }
                }
                if (Existe == false) {
                    var _node = new TVclTreeNode(null, null);
                    _node.Text = thirdWordFromRight;
                    _node.TreeRootNode = NodeRoot;
                    NodePosition.AddTreeNode(_node);
                    NodePosition = _node;
                }
            }
        }
        if (inImg.length > 0) NodeRoot.Img = inImg;
        return NodeRoot;
    }

    this.AddTreeNode = function (Ruta, NodeText, inImg)
    {
        var TreeNodeLast = null;
        var NodeRoot = null;
        var NodePosition = null;        
        var words = Ruta.split('\\');

        if (Ruta.length <= 0) {
            var _node = new TVclTreeNode(null, null);
            _node.Text = NodeText;
            this.TreeNodes.push(_node);
            this.This.appendChild(_node.This);
            _node.This.style.paddingTop = "5px";
            _node.This.style.paddingBottom = "5px";
            NodePosition = _node;
            if (inImg.length > 0) NodePosition.Img = inImg;
            return NodePosition;
        }
        else {
            for (var Counter = 0; Counter < words.length; Counter++) {
                var thirdWordFromRight = (Counter >= 0 ? words[Counter] : null);
                var Existe = false;
                if (Counter == 0) {
                    for (var CounterNodo = 0; CounterNodo < this.TreeNodes.length; CounterNodo++) {
                        if (this.TreeNodes[CounterNodo].Text == thirdWordFromRight) {
                            NodePosition = this.TreeNodes[CounterNodo];
                            NodeRoot = this.TreeNodes[CounterNodo];
                            Existe = true;
                        }
                    }
                    if (Existe == false) {
                        var _node = new TVclTreeNode(null, null);
                        _node.Text = thirdWordFromRight;
                        this.TreeNodes.push(_node);
                        this.This.appendChild(_node.This);
                        _node.This.style.paddingTop = "5px";
                        _node.This.style.paddingBottom = "5px";
                        NodePosition = _node;

                    }

                    if (words.length == (Counter + 1)) {
                        var _node = new TVclTreeNode(null, null);
                        _node.Text = NodeText;
                        NodePosition.AddTreeNode(_node);
                        NodePosition = _node;
                        TreeNodeLast = _node;
                        if (inImg.length > 0) NodePosition.Img = inImg;
                    }
                    
                }
                else {
                    for (var CounterNodo = 0; CounterNodo < NodePosition.TreeNodes.length; CounterNodo++) {
                        if (NodePosition.TreeNodes[CounterNodo].Text == thirdWordFromRight) {
                            NodePosition = NodePosition.TreeNodes[CounterNodo];
                            Existe = true;
                        }
                    }
                    if (Existe == false) {
                        var _node = new TVclTreeNode(null, null);
                        _node.Text = thirdWordFromRight;
                        NodePosition.AddTreeNode(_node);
                        NodePosition = _node;
                    }
                    else {
                        if (words.length == (Counter + 1)) {
                            var _node = new TVclTreeNode(null, null);
                            _node.Text = NodeText;
                            NodePosition.AddTreeNode(_node);
                            NodePosition = _node;
                            TreeNodeLast = _node;
                            _node.This.style.paddingBottom = "0px";
                            if (inImg.length > 0) NodePosition.Img = inImg;
                        }
                    }
                }
            }
        }
        
        return NodePosition;
    }

    var TParent = function () {
        return this;
    }.bind(this);
}

var TVclTreeNode = function (ObjectHtml, Id) {    
    this.This = Uli(ObjectHtml, Id);
    this.This.style.display = "block";
    this.This.style.paddingTop = "5px";
    this.This.style.paddingBottom = "5px";   

    this.ThisUL = Uul(this.This, Id);
    $(this.ThisUL).hide();
    this.TreeNodes = new Array();
    this._Tag = null;
    this._Text = null;
    this._Imagen = null;
    this._TreeRootNode = null;
    this._IsRoot = false;
    this._Numero = null;
    this._MarginImageText = null;
    this._Link = null;


    Object.defineProperty(this, 'Text', {
        get: function () {
            return this._Text;
        },
        set: function (_Value) {
            this._Text = _Value;
            var _link = document.createElement("a");
            this._Link = _link;
            var _UlTemp = this.ThisUL;
            _link.appendChild(document.createTextNode(this._Text));
            _link.href = "#";
            _link.style.textDecoration = "none";
            //_link.style.display = "block";
            _link.onclick = function (e) {
                e.preventDefault();
                $(_UlTemp).toggle("slow"); // mostrar los nodos;
            }
            _link.addEventListener("mouseover", function () {
                this.parentNode.style.backgroundColor = "#2C3B41";
                this.style.color = "white";
            });

            _link.addEventListener("mouseout", function () {
                this.parentNode.style.backgroundColor = "transparent";
                this.style.color = "#2d83a9";
            });

            this.This.insertBefore(_link, this.This.childNodes[0]);
        }
    });

    Object.defineProperty(this, 'Img', {
        get: function () {
            return this._Imagen;
        },
        set: function (_Value) {
            this._Imagen = _Value;
            var _img = document.createElement("img");
            var _UlTemp = this.ThisUL;
            _img.src = _Value;
            
            if (this._IsRoot) {
                _img.style.marginRight = "10px";                
                this.This.insertBefore(_img, this.This.childNodes[0]);
            }
            else {
                _img.style.marginLeft = "10px";
                _img.style.cssFloat = "right";
                _img.style.marginRight = "10px";
                this.This.insertBefore(_img, this.This.childNodes[1]);
            }
        }
    });

    Object.defineProperty(this, 'Tag', {
        get: function () {
            return this._Tag;
        },
        set: function (_Value) {
            this._Tag = _Value;
        }
    });

    Object.defineProperty(this, 'IsRoot', {
        get: function () {
            return this._IsRoot;
        },
        set: function (_Value) {
            this._IsRoot = _Value;
        }
    });

    Object.defineProperty(this, 'Numero', {
        get: function () {
            return this._Numero;
        },
        set: function (_Value) {
            this._Numero = _Value;
            var _label = document.createElement("label");
            _label.appendChild(document.createTextNode(this._Numero));
            this.This.insertBefore(_label, this.This.lastChild);
        }
    });

    Object.defineProperty(this, 'TreeRootNode', {
        get: function () {
            return this._TreeRootNode;
        },
        set: function (_Value) {
            this._TreeRootNode = _Value;
        }
    });

    Object.defineProperty(this, 'Cursor', {
        get: function () {
            return this.This.style.cursor;
        },
        set: function (_Value) {
            this.This.style.cursor = _Value;
        }
    });

    Object.defineProperty(this, 'onClick', {
        get: function () {
            return this.This.onclick;
        },
        set: function (_Value) {
            var _ThisTemp = this;
            this.This.onclick = function () {
                _Value(_ThisTemp);
            }
                
        }
    });


    this.Expand = function () {
        $(this.ThisUL).show("slow");
    }

    this.Contract = function () {
        $(this.ThisUL).hide("slow");
    }


    this.AddTreeNode = function (VclTreeNode) {
        this.TreeNodes.push(VclTreeNode);
        this.ThisUL.appendChild(VclTreeNode.This);
    }

    var TParent = function () {
        return this;
    }.bind(this);
}