﻿//Jorge Jaimes Version 1 //2018/03/13
function VclNavBar(inObject, id) {
    var TParent = function () {
        return this;
    }.bind(this);  
 
    this.NavBarItems = new Array();
    this._Tag;
    this._Title = "";
    this._ImgTitle = "";
    this._Container = null;
    this._WorkSpace = null;    

    //Container
    _Container = new TVclStackPanel(inObject, "NavBar_Container_" + id, 1, [[12], [12], [12], [12], [12]]);
    this._Container = _Container.Column[0].This;

    //Titulo y subtitulo
    var xTitle = new TVclStackPanel(this._Container, id + "_NavTitle", 1, [[12], [12], [12], [12], [12]]);
    NavTitle = xTitle.Column[0].This;
    //$(NavTitle).css("border", "1px solid #c4c4c4");

    var WorkSpace = new TVclStackPanel(this._Container, id + "_WorkSpace", 1, [[12], [12], [12], [12], [12]]);
    this._WorkSpace = WorkSpace.Column[0].This;

    Object.defineProperty(this, 'Title', {
        get: function () {
            return this._Title;
        },
        set: function (_Value) {
            this._Title = _Value;
            $(NavTitle).html("");
            if (this._ImgTitle != "") {
                var Img = new Uimg(NavTitle, "Nav_ImgTitle", this._ImgTitle, "");
                $(Img).css("margin-right", "10px");
            }
            var LabelTitle = new TVcllabel(NavTitle, "", TlabelType.H0);
            LabelTitle.Text = _Value;
            LabelTitle.This.style.fontStyle = "normal";
            LabelTitle.This.style.fontSize = "14px";
            LabelTitle.This.style.fontWeight = "normal";
        }
    });

    Object.defineProperty(this, 'ImgTitle', {
        get: function () {
            return this._ImgTitle;
        },
        set: function (_Value) {
            $(NavTitle).html("");
            this._ImgTitle = _Value;
            var Img = new Uimg(NavTitle, "Nav_ImgTitle", _Value, "");
            $(Img).css("margin-right", "10px");
            if (this._Title != "") {
                var LabelTitle = new TVcllabel(NavTitle, "", TlabelType.H0);
                LabelTitle.Text = this._Title;
                LabelTitle.This.style.fontStyle = "normal";
                LabelTitle.This.style.fontSize = "14px";
                LabelTitle.This.style.fontWeight = "normal";
            }            
        }
    });

    this.AddItemNavBar = function (_Value) {
        var _this = this;   
        this.NavBarItems.push(_Value);
        this.NavBarItems[this.NavBarItems.length - 1].Index = this.NavBarItems.length - 1;
        
        //Funciones cuando panel del item se abra o se cierre
        this.DDPanelOpen = function (Item) {
            //recorer los items y cerrar los que esten abiertos
            for (var i = 0; i < _this.NavBarItems.length; i++) {           
                try {
                    if (_this.NavBarItems[i].Text != Item.Text && _this.NavBarItems[i].DropDownPanel._IsOpen) {
                        _this.NavBarItems[i].DropDownPanel.ClosePanel();                            
                    }
                } catch (e) {
                    SysCfg.Log.Methods.WriteLog("NavBar.js", e);
                } 
            }            
        };
        this.DDPanelClose = function (Item) {
             
        };

        //agregar elemento
        if (_Value.Style == "ControlContain") {
            _Value.DropDownPanel = new TVclDropDownPanel(_this._WorkSpace, "");//CREA EL DROP DOWN PANEL
            _Value.DropDownPanel.BoderTopColor = "green"; //ASIGNA O EXTRAE EL COLOR DEL BORDE SUPERIOR 
            _Value.DropDownPanel.BoderAllColor = "gray"; //ASIGNA O EXTRAE EL COLOR DE TODO EL BODER 
            _Value.DropDownPanel.BackgroundColor = "white"; //ASIGNA O EXTRAE EL COLOR DE FONDO DEL DROPDOWNPANEL 
            _Value.DropDownPanel.MarginBottom = "0px"; //ASIGNA O EXTRAE EL MARGEN INFERIOR DEL DROPDOWNPANEL
            _Value.DropDownPanel.MarginTop = "0px"; //ASIGNA O EXTRAE EL MARGEN SUPERIOR DEL DROPDOWNPANEL
            _Value.DropDownPanel.MarginLeft = "20px"; //ASIGNA O EXTRAE EL MARGEN IZQUIERDO DEL DROPDOWNPANEL
            _Value.DropDownPanel.MarginRight = "20px"; //ASIGNA O EXTRAE EL MARGEN DERECHO DEL DROPDOWNPANEL
            _Value.DropDownPanel._functionOpen = function () {                
                _this.DDPanelOpen(_Value);
            }
            _Value.DropDownPanel._functionClose = function () {                 
                _this.DDPanelClose(_Value);
            }
            //encabezado
            var imgHead = new TVclImagen("", "");
            imgHead.Src = _Value.Image;
            //imgHead.Title = "esta es una imagen de prueba";
            imgHead.This.style.marginRight = "10px";

            var lblHead = new TVcllabel("", "", TlabelType.H0);
            lblHead.Text = _Value.Text;
            lblHead.VCLType = TVCLType.BS;

            _Value.DropDownPanel.AddToHeader(imgHead.This); //ASIGNA EL CONTENIDO HTML AL HEADER DEL DROPDOWNPANEL 
            _Value.DropDownPanel.AddToHeader(lblHead.This);//ASIGNA EL CONTENIDO HTML AL HEADER DEL DROPDOWNPANEL 
            //cuerpo
            _Value.DropDownPanel.AddToBody(_Value.Object_Contain); //ASIGNA EL CONTENIDO HTML AL BODY DEL DROPDOWNPANEL 
        } else if (_Value.Style == "ControlAdd") {
            var xItem = new TVclStackPanel(_this._WorkSpace, "", 1, [[12], [12], [12], [12], [12]]);
            var Item = xItem.Column[0].This;           
            xItem.Row.MarginLeft = "20px";
            xItem.Row.MarginRight = "20px";
            Item.style.cursor = "pointer";
            Item.style.borderTop = "3px solid green";
            Item.style.borderBottom = "1px solid gray";
            Item.style.borderLeft = "1px solid gray";
            Item.style.borderRight = "1px solid gray";
            Item.style.padding = "8px";
            Item.style.backgroundColor = "white";

            $(Item).append(_Value.Object_Contain);
        } else {
            var xItem = new TVclStackPanel(_this._WorkSpace, "", 1, [[12], [12], [12], [12], [12]]);
            var Item = xItem.Column[0].This;          
            Item.onclick = function () {               
                _Value.onClick();
            }
            xItem.Row.MarginLeft = "20px";
            xItem.Row.MarginRight = "20px";
            Item.style.cursor = "pointer";  
            Item.style.borderTop = "3px solid green";
            Item.style.borderBottom = "1px solid gray";
            Item.style.borderLeft = "1px solid gray";
            Item.style.borderRight = "1px solid gray";
            Item.style.padding = "8px";
            Item.style.backgroundColor = "white";

            var imgHead = new TVclImagen(Item, "");
            imgHead.Src = _Value.Image;
            //imgHead.Title = "";
            imgHead.This.style.marginRight = "10px";

            var lblHead = new TVcllabel(Item, "", TlabelType.H0);
            lblHead.Text = _Value.Text;
            lblHead.VCLType = TVCLType.BS;
        }
      
    }    

   // $(this._Container).css("border", "1px solid #c4c4c4");

    return this;
}

function VclNavBarItem() {
    var TParent = function () {
        return this;
    }.bind(this);

   
    this._Text = "";
    this._Image = "";
    this._Style = null;
    this._Object_Contain = "";
    this._Tag = "";
    this.onClick = null;
    this._Index = null;
    this._DropDownPanel = null;

    Object.defineProperty(this, 'Text', {
        get: function () {
            return this._Text;
        },
        set: function (_Value) {
            this._Text = _Value;
        }
    });

    Object.defineProperty(this, 'Image', {
        get: function () {
            return this._Image;
        },
        set: function (_Value) {
            this._Image = _Value;
        }
    });

    Object.defineProperty(this, 'Style', {
        get: function () {
            return this._Style;
        },
        set: function (_Value) {           
            this._Style = _Value;
            switch (_Value) {
                case "Defaul":
                    break;
                case "ControlContain":
                    break;               
                default:
            }
        }
    });

    Object.defineProperty(this, 'Object_Contain', {
        get: function () {
            return this._Object_Contain;
        },
        set: function (_Value) {
            this._Object_Contain = _Value;
        }
    });

    Object.defineProperty(this, 'Tag', {
        get: function () {
            return this._Tag;
        },
        set: function (_Value) {
            this._Tag = _Value;
        }
    });

    Object.defineProperty(this, 'Index', {
        get: function () {
            return this._Index;
        },
        set: function (_Value) {
            this._Index = _Value;
        }
    });

    Object.defineProperty(this, 'DropDownPanel', {
        get: function () {
            return this._DropDownPanel;
        },
        set: function (_Value) {
            this._DropDownPanel = _Value;
        }
    });
}