﻿/*<summary>
Midifico fecha   
Carlos     24-04-2017
Funcion: Representa una clase TUVCLRepositoryLookUp que genera un lookup para mandar mensajes
ejemplos:
var UVCLRepositoryLookUp = new TUVCLRepositoryLookUp();
</summary>
<returns></returns>*/
function TUVCLRepositoryLookUp() {
    this.Caption;
    this.Char_ascii;
    this.DivRootContainer;
    this.ButtonCancelCaption;
    this.ButtonOkCaption;
    this.Uulli;
    this.Active = false;
}

/*<summary>
Midifico fecha   
Carlos     24-04-2017
Funcion: Crea un elemento de la clase TUVCLRepositoryLookUp
ejemplos:
UVCLRepositoryLookUp(Elementhtm, "Id UVCLRepositoryLookUp", true, true);
</summary>
<param name="Object">Elemento html al cual se le insertara el TUVCLRepositoryLookUp creado</param>
<param name="id">Id del TUVCLRepositoryLookUp</param>
<param name="Caption">Texto que llevara el lookup</param>
<param name="isButtonCancelCaption">valor de verdadero o falso que indica si se muestra el boton cancel</param>
<param name="isButtonOkCaption">valor de verdadero o falso que indica si se muestra el boton ok</param>
<returns>Retorna una clase de tipo TUVCLRepositoryLookUp</returns>*/
function UVCLRepositoryLookUp(Object, id, Caption, isButtonCancelCaption, isButtonOkCaption, onClick) {
    var Res = new TUVCLRepositoryLookUp();
   
    Res.Uulli = VclList(Object, id, 1);


    Res.Caption = Ua(Res.Uulli.li[0], "messageOpen", Caption);
    Res.Caption.classList.add("message-case");
    Res.Char_ascii = Uspan(Res.Caption, "", '▼');

    Res.Caption.addEventListener("click", function () {
        $(this).next('#' + id + "_DivContainer").slideToggle();
        $(this).toggleClass('active');

        if ($(this).hasClass('active')) {
            Res.Active = true;
            $(this).find('span').html('&#x25B2;');
        } else {
            Res.Active = false;
            $(this).find('span').html('&#x25BC;');
        }

        if ($(window).width() <= 400) {
            Res.DivRootContainer.This.style.width = $(window).width() + 'px';
            Res.DivRootContainer.This.style.right = "-50px";
        }
        else {
            Res.DivRootContainer.This.style.width = "400px";
            Res.DivRootContainer.This.style.right = "22px";
        }

        //Jaimes 08/06/2018 //funcion enviada por parametros para cuando se de click a Res.Caption
        if (onClick != null) {
            onClick();
        }
        
    });

    var LueDef = new Array(1);
    LueDef[0] = new Array(3);
    LueDef[0][0] = new Array(2);
    LueDef[0][1] = new Array(2);
    LueDef[0][2] = new Array(2);
    Res.DivRootContainer = VclDivitionsRoot(Res.Uulli.li[0], id + "_DivContainer", LueDef);

    if (isButtonCancelCaption) {
        Res.ButtonCancelCaption = new TVclInputbutton(Res.DivRootContainer.Child[0].Child[2].Child[0].This, id + "_ButtonCancelCaption");
        Res.ButtonCancelCaption.Text = "Cancel";

    }
    if (isButtonOkCaption) {
        Res.ButtonOkCaption = new TVclInputbutton(Res.DivRootContainer.Child[0].Child[2].Child[1].This, id + "_ButtonOkCaption");
        Res.ButtonOkCaption.Text = "Send";
    }

    return Res;
}