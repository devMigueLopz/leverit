﻿var TOpenFile = function (inObjectHtml, Id, _IsBytes) {
    this.TParent = function () {
        return this;
    }.bind(this);

    var _this = this.TParent();

    this.This = Uinput(inObjectHtml, TImputtype.file, Id, "", false);
    this._VCLType = null;
    this.NameFile = "File.txt";
    this.Path = "document";
    this.tag = null;
    this.Bytes = new Array();
    this.File = null;
    this.ResErr = new SysCfg.Error.Properties.TResErr();
    SysCfg.Error.Properties.ResErrfill(this.ResErr);
    this.This.style.visibility = "hidden";
    this.IsBytes = _IsBytes;
    this.CallBack = null;

    this.SetFile = function (callBack) {
        _this.CallBack = callBack;
        _this.This.click();
    }

    this.This.onchange = function (event) {
        _this.NameFile = _this.This.files[0].name;
        _this.Path = _this.Text;
        _this.ResErr.NotError = true;
        _this.ResErr.Mesaje = "OK";
        _this.File = _this.This.files[0];
        if (_this.IsBytes) {

            var reader = new FileReader();
            reader.onload = function () {
                _this.Bytes = [].slice.call(new Uint8Array(this.result))
                if (_this.ResErr.NotError)
                    _this.CallBack(_this, _this.ResErr);
            }
            reader.readAsArrayBuffer(_this.This.files[0]);
        }
        else
            _this.CallBack(_this, _this.ResErr);
    }


    //#region propiedades que alteran los atributos del elemento html   

    Object.defineProperty(this, 'Text', {
        get: function () {
            return $(this.This).val();
        },
        set: function (_Value) {
            $(this.This).val(_Value);
        }
    });

    Object.defineProperty(this, 'Enabled', {
        get: function () {
            return !this.This.disabled;
        },
        set: function (_Value) {
            this.This.disabled = !_Value;
        }
    });

    Object.defineProperty(this, 'onClick', {
        get: function () {
            return this.This.onclick;
        },
        set: function (_Value) {
            this.This.onclick = _Value;
        }
    });

    Object.defineProperty(this, 'onKeyPress', {
        get: function () {
            return this.This.onkeypress;
        },
        set: function (_Value) {
            this.This.onkeypress = _Value;
        }
    });

    Object.defineProperty(this, 'onChange', {
        get: function () {
            return this.This.onchange;
        },
        set: function (_Value) {
            this.This.onchange = _Value;
        }
    });

    Object.defineProperty(this, 'onKeyUp', {
        get: function () {
            return this.This.onkeyup;
        },
        set: function (_Value) {
            this.This.onkeyup = _Value;
        }
    });

    Object.defineProperty(this, 'VCLType', {
        get: function () {
            return this._VCLType;
        },
        set: function (_Value) {
            this._VCLType = _Value;
            switch (_Value) {
                case TVCLType.AG:
                    break;
                case TVCLType.BS:
                    BootStrapCreateInpunttextbox(this.This);
                    break;
                case TVCLType.DV:
                    break;
                case TVCLType.KD:
                    break;
                default:
            }
        }
    });

    Object.defineProperty(this, 'Type', {
        get: function () {
            return this.This.type;
        },
        set: function (_Value) {
            this.This.type = _Value.name;
        }
    });

    Object.defineProperty(this, 'Visible', {
        get: function () {
            if (this.This.style.visibility == "hidden" || this.This.style.visibility == "collapse")
                return false;
            return true;
        },
        set: function (_Value) {
            if (_Value)
                this.This.style.visibility = "visible";
            else
                this.This.style.visibility = "hidden";
        }
    });
    //#endregion

    //#region propiedades que alteran los estilos del elemento html 
    Object.defineProperty(this, 'Width', {
        get: function () {
            return this.This.style.width;
        },
        set: function (_Value) {
            this.This.style.width = _Value;
        }
    });
    //#endregion

}
//var TOpenFile = function (inObjectHtml, Id) {
//    this.TParent = function () {
//        return this;
//    }.bind(this);

//    var _this = this.TParent();

//    this.This = Uinput(inObjectHtml, TImputtype.file, Id, "", false);    
//    this._VCLType = null;
//    this.NameFile = "File.txt";
//    this.Path = "document";
//    this.tag = null;
//    this.Bytes = new Array();
//    this.ResErr = new SysCfg.Error.Properties.TResErr();
//    SysCfg.Error.Properties.ResErrfill(this.ResErr);
//    this.This.style.visibility = "hidden";

//    this.SetFile = function (callBack) {        
//        _this.This.click();
//        _this.NameFile = _this.This.files[0].name;
//        _this.Path = _this.Text;
//        _this.ResErr.NotError = true;
//        _this.ResErr.Mesaje = "OK";
        
//        var reader = new FileReader();
//        reader.onload = function () {
//            _this.Bytes = [].slice.call(new Uint8Array(this.result))
//            if (_this.ResErr.NotError)
//                callBack(_this, _this.ResErr);
//        }
//        reader.readAsArrayBuffer(_this.This.files[0]);


//    }

//    //this.This.onchange = function (event) {
//    //    alert(event);
//    //}


//    //#region propiedades que alteran los atributos del elemento html   

//    Object.defineProperty(this, 'Text', {
//        get: function () {
//            return $(this.This).val();
//        },
//        set: function (_Value) {
//            $(this.This).val(_Value);
//        }
//    });

//    Object.defineProperty(this, 'Enabled', {
//        get: function () {
//            return !this.This.disabled;
//        },
//        set: function (_Value) {
//            this.This.disabled = !_Value;
//        }
//    });

//    Object.defineProperty(this, 'onClick', {
//        get: function () {
//            return this.This.onclick;
//        },
//        set: function (_Value) {
//            this.This.onclick = _Value;
//        }
//    });

//    Object.defineProperty(this, 'onKeyPress', {
//        get: function () {
//            return this.This.onkeypress;
//        },
//        set: function (_Value) {
//            this.This.onkeypress = _Value;
//        }
//    });

//    Object.defineProperty(this, 'onChange', {
//        get: function () {
//            return this.This.onchange;
//        },
//        set: function (_Value) {
//            this.This.onchange = _Value;
//        }
//    });

//    Object.defineProperty(this, 'onKeyUp', {
//        get: function () {
//            return this.This.onkeyup;
//        },
//        set: function (_Value) {
//            this.This.onkeyup = _Value;
//        }
//    });

//    Object.defineProperty(this, 'VCLType', {
//        get: function () {
//            return this._VCLType;
//        },
//        set: function (_Value) {
//            this._VCLType = _Value;
//            switch (_Value) {
//                case TVCLType.AG:
//                    break;
//                case TVCLType.BS:
//                    BootStrapCreateInpunttextbox(this.This);
//                    break;
//                case TVCLType.DV:
//                    break;
//                case TVCLType.KD:
//                    break;
//                default:
//            }
//        }
//    });

//    Object.defineProperty(this, 'Type', {
//        get: function () {
//            return this.This.type;
//        },
//        set: function (_Value) {
//            this.This.type = _Value.name;
//        }
//    });

//    Object.defineProperty(this, 'Visible', {
//        get: function () {
//            if (this.This.style.visibility == "hidden" || this.This.style.visibility == "collapse")
//                return false;
//            return true;
//        },
//        set: function (_Value) {
//            if (_Value)
//                this.This.style.visibility = "visible";
//            else
//                this.This.style.visibility = "hidden";
//        }
//    });
//    //#endregion

//    //#region propiedades que alteran los estilos del elemento html 
//    Object.defineProperty(this, 'Width', {
//        get: function () {
//            return this.This.style.width;
//        },
//        set: function (_Value) {
//            this.This.style.width = _Value;
//        }
//    });
//    //#endregion

    




//}