﻿var TVclImagen = function (ObjectHtml, Id)
{
    this.TParent = function ()
    {
        return this;
    }.bind(this);

    var _this = this.TParent();

    this.This = Uimg(ObjectHtml, Id, "", "");
    this._Tag = null;
    this._Tag1 = null;
    this.OnClick = null;


    Object.defineProperty(this, 'Tag', {
        get: function () {
            return this._Tag;
        },
        set: function (_Value) {
            this._Tag = _Value;
        }
    });

    Object.defineProperty(this, 'Tag1', {
        get: function ()
        {
            return this._Tag1;
        },
        set: function (_Value)
        {
            this._Tag1 = _Value;
        }
    });

    Object.defineProperty(this, 'Cursor', {
        get: function () {
            return this.This.style.cursor;
        },
        set: function (_Value) {
            this.This.style.cursor = _Value;
        }
    });

    Object.defineProperty(this, 'Src', {
        get: function () {
            return this.This.src;
        },
        set: function (_Value) {
            this.This.src = _Value;
        }
    });

    Object.defineProperty(this, 'Alt', {
        get: function () {
            return this.This.alt;
        },
        set: function (_Value) {
            this.This.alt = _Value;
        }
    });

    Object.defineProperty(this, 'Title', {
        get: function () {
            return this.This.title;
        },
        set: function (_Value) {
            this.This.title = _Value;
        }
    });

    Object.defineProperty(this, 'Enabled', {
        get: function () {
            return !this.This.disabled;
        },
        set: function (_Value) {
            this.This.disabled = !_Value;
        }
    });

    Object.defineProperty(this, 'onClick', {
        get: function () {
            return this.This.onclick;
        },
        set: function (_Value) {
            this.This.onclick = _Value;
        }
    });

    Object.defineProperty(this, 'VCLType', {
        get: function () {
            return this.VCLType;
        },
        set: function (_Value) {
            switch (_Value) {
                case TVCLType.AG:
                    break;
                case TVCLType.BS:
                    BootStrapCreateCheckbox(this.This);
                    break;
                case TVCLType.DV:
                    break;
                case TVCLType.KD:
                    break;
                default:
            }
        }
    });

    Object.defineProperty(this, 'Width', {
        get: function () {
            return this.This.style.width;
        },
        set: function (_Value) {
            this.This.style.width = _Value;
        }
    });

    Object.defineProperty(this, 'Visible', {
        get: function () {
            if (this.This.style.visibility == "hidden" || this.This.style.visibility == "collapse")
                return false;
            return true;
        },
        set: function (_Value) {
            if (_Value)
                this.This.style.visibility = "visible";
            else
                this.This.style.visibility = "hidden";
        }
    });

    this.Click = function () {
        this.This.click();
    }

    this.This.onclick = function ()
    {
        if (_this.OnClick != null)
            _this.OnClick(_this);
    }    
}