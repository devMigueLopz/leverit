﻿/*<summary>
Midifico fecha   
Carlos     24-04-2017
Funcion: Representa una clase que contiene una lista (ul) html y un array de elementos (li) html 
ejemplos:
var Uulli = new TUulli();
</summary>
<returns></returns>*/
function TUulli() {
    this.ul;
    this.li;
}

/*<summary>
Midifico fecha   
Carlos     24-04-2017
Funcion: Crea una lista (ul) y sus elementos (li)
ejemplos:
Uulli(Elementhtm, "Id Lista", 4);
</summary>
<param name="Object">Elemento html al cual se le insertara la lista creada por esta funcion</param>
<param name="id">Id que va a ser asignado a la lista</param>
<param name="count_li">Numero de elemento que va a tener la lista</param>
<returns>Retorna un elemento de tipo ul (Lista html)</returns>*/
function VclList(Object, id, count_li) {
    var Res = undefined;
    var _Uulli = new TUulli();
    _Uulli.ul = Uul(Object, id + '_ul')
    _Uulli.li = new Array(count_li);
    for (var i = 0; i < count_li; i++) {
        _Uulli.li[i] = Uli(_Uulli.ul, id + '_li' + i)
    }
    Res = _Uulli;
    $(Object).append(_Uulli.ul);
    return Res;
}