/*Script para agrupar items*/
var GridAgrupaciones = new TVclGridCF(div, null, null);
var ListaPersonas = new Array();
var div = document.createElement("div");
div.classList.add("col-md-12");
var ListaPersonas = new Array();


//Forma 1:
//listaPersona[0]="juan"
//listaPersona[1]="pepe"
//Forma 2:
//ListaPersonas["juan"]
//ListaPersonas["pepe"]
ListaPersonas = [
    {
        "nombre": "felipe",
        "pais": "peru"
    },
    {
        "nombre": "juan",
        "pais": "peru"
    },
    {
        "nombre": "felipe",
        "pais": "colombia"
    }

]

function CrearGrid() {
    GridAgrupaciones.ClearAll();
    var ColPais = new TVclColumnCF();
    ColPais.Name = "Pais";
    ColPais.Caption = "Pais";
    ColPais.Index = 0;
    var ColNombre = new TVclColumnCF();
    ColNombre.Name = "Nombre";
    ColNombre.Caption = "Nombre";
    ColNombre.Index = 1;
    GridAgrupaciones.AddColumn(ColPais);
    GridAgrupaciones.AddColumn(ColNombre);
    $(document.body).html("");
    div.appendChild(GridAgrupaciones.This);
    document.body.appendChild(div);
}//Crea Grid
function CrearFilaGrupo(textoAgrupacion) {
    var NewRow = new TVclRowCF();
    NewRow.This.style.backgroundColor = "#3e94ba";
    var Cell = new TVclCellCF();
    Cell.Value = textoAgrupacion;
    NewRow.AddCell(Cell);
    NewRow.PaddingGroup = 0;
    NewRow._ImgGruopDown.style.display = '';
    NewRow._ImgGruopRigth.style.display = 'none';
    GridAgrupaciones.AddRowAfter(0, NewRow, null, 2);
    return NewRow;
}//Crea fila azul
function AgregarFila(item, NewRowPadre) {
    var NewRow = new TVclRowCF();
    var CellPais = new TVclCellCF();
    CellPais.Value = item.pais;
    NewRow.AddCell(CellPais);
    var CellNombre = new TVclCellCF();
    CellNombre.Value = item.nombre;
    NewRow.AddCell(CellNombre);
    GridAgrupaciones.AddRowAfter(2, NewRow, NewRowPadre, null);
    NewRow.RowPadre = NewRowPadre;
    NewRowPadre.RowHijos.push(NewRow);

}//agrega fila a grid
function AgruparPorPais() {
    CrearGrid();
    var ListaGrupoRow = new Array();//Lista de Row Grupo
    for (var i = 0; i < ListaPersonas.length; i++) {
        var pais = ListaPersonas[i].pais;//valor pais del item
        /*
        iteracion 1 = ListaGrupoRow[peru]
        iteracion 2 = ListaGrupoRow[peru]
        iteracion 3 = ListaGrupoRow[colombia]
        //ListaGrupoRow[colombia]= RowPadre;
        */
        if (typeof (ListaGrupoRow[pais]) == "undefined") {
            //Crea y devuelve fila azul.
            ListaGrupoRow[pais] = CrearFilaGrupo(pais);
        }
        //agregamos fila a grid.
        AgregarFila(ListaPersonas[i], ListaGrupoRow[pais]);
        
    }
    return ListaGrupoPais;
}//Agrupar filas por pa�s
function AgruparPorNombre() {
    var ListaGrupoNombres = new Array();
    for (var i = 0; i < ListaPersonas.length; i++) {
        var nombre = ListaPersonas[i].nombre;
        //ListaGrupoNombres["felipe"]
        if (typeof (ListaGrupoNombres[nombre]) == "undefined") {
            ListaGrupoNombres[nombre] = new Array();
        } 
        ListaGrupoNombres[nombre].push(ListaPersonas[i]);
    }
    return ListaGrupoNombres;
}

/*Busquedad por Linq*/
function buscarLinq(nombre) {
    var queryResult2 = Enumerable.From(ListaPersonas)
        .Where("$.nombre == '" + nombre+"'").Select("$").ToArray();
    return queryResult2;
}
console.log(AgruparPorNombre());
console.log(AgruparPorPais());
console.log(buscarLinq("felipe"));