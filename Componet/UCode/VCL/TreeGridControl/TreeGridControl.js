﻿Componet.GridCF.TVclTreeGridControl = function (inObjectHTML, objectContenGridHtml, ListBox, ButtonImg_ExpanderBar, ButtonImg_ContraerBar) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.Mythis = "TVclTreeGridControl";
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1", "Drag the column to group");//Modifiqué descripción
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2", "Select");////Modifiqué descripción
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3", "The column is grouped");//agregé nueva traducción
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines4", "select a row");//agregé nueva traducción
    this.TextCombobox = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2") + "...";
    this.sizeColumnTVclGridCFSource = 0;
    this.nameColumns = null;//Array de columnas
    this.GridCFView = null;//Componente cfview
    this.ItemTreePanel = null;//ItemTreePanel
    this.ColumnaSelecionada = null;//Columna ordenada     
    this.open = false;//Indica el estado del bar: expand o collapse
    this.orden = "";//Orden
    this.DataSet = null;//DataSet
    this.ListBox = ListBox;
    this.ContCboTreeGrid = null;//Div donde formamos los combos para selecionar agrupación.
    this.ConTreeControl = null;//Div de la agrupación
    this.nivelesTree = new Array();//INDICA EL NÚMERO DE NIVELES A ORDENAR
    this.lastCombobox = null;//ULTIMO COMBO *Optimizar
    this.lastBtn = null;//Ultimo boton
    this.itemsComboDinamicos = new Array();//Array con los combos agrupados
    this.rowSelecionado = null;//Fila selecionada
    this.accionInsertar = false;//Si se esta insertando
    this.temporalIndex = null; //Temporal de índice  
    this.Init = function (inTVclGridCFSource, inNameColumns, inItemTreePanel, inOpen, inOrden, inDataSet) {
        this.nameColumns = inNameColumns;//Array de columnas
        this.ItemTreePanel = inItemTreePanel;//ItemTreePanel
        this.TVclGridCFSource = inTVclGridCFSource;//Grid del cfview
        this.open = inOpen;//Estado abierto/cerrado
        this.orden = inOrden;//orden asc/desc
        this.DataSet = inDataSet;//DataSet
        this.lastBtn = null;//Seteamos el valor del último botón como nulo
        this.lastCombobox = null;//Seteamos el valor del último combo como nulo
        this.nivelesTree = [];//Seteamos el valor nivelesTree como nulo        
        this.itemsComboDinamicos = [];//Seteamos el valor itemsComboDinamicos como nulo   
        this.rowSelecionado = null;//Seteamos el valor rowSelecionado como nulo  
        this.accionInsertar = false;//Seteamos el valor de acción como false
        this.temporalIndex = null;//Seteamos el valor de acción como nulo 
        this.inTreeGrid = false;//Seteamos el valor de inTreeGrid como false 
        var Parent_inObjectHTML = inObjectHTML.parentNode;//parent node
        Parent_inObjectHTML.removeChild(inObjectHTML);//removemos el div
        $(inObjectHTML).html("");//limpiamos div
        _this.ContCboTreeGridPanel = new TVclStackPanel(inObjectHTML, "1", 1, [[12], [12], [12], [12], [12]]);//Creamos StackPanel
        _this.ContCboTreeGrid = new TVclStackPanel(_this.ContCboTreeGridPanel.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);//Creamos StackPanel
        _this.ContCboTreeGrid.Row.This.style.marginTop = "0px";//margin top
        _this.ContCboTreeGrid.Row.This.style.marginBottom = "0px";//margin bottom
        _this.LabelBof = new TVcllabel(_this.ContCboTreeGridPanel.Column[0].This, "", TlabelType.H0);//Creamo label
        _this.LabelBof.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1") + " ...";//Asignamos texto
        _this.LabelBof.VCLType = TVCLType.BS;//Estilo de label
        _this.LabelBof.This.style.color = "rgb(117, 117, 117);";//color
        _this.LabelBof.This.style.fontStyle = "normal";//fuente
        _this.LabelBof.This.style.fontSize = " 11px";//tamaño de fuente

        if (_this.nameColumns != null && _this.nameColumns.length > 0) {//validamos que exita en array de name columns
            _this.VclDrag.addEventContendor(_this.ContCboTreeGridPanel, _this.ContCboTreeGridPanel.Row.This, function (Content, Component, Event) {//Agregamos como contenedor
                var VclColumn = Component;
                if (!_this.IsColumnGroupInGrid(VclColumn.Name))/*Validamos si la columna ya esta agrupada*/
                    alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3"));
            }
            );
            _this.ConTreeControl = new TVclStackPanel(inObjectHTML, "1", 1, [[12], [12], [12], [12], [12]]);//Creamos StackPanel
            _this.ComboInicial();//llamamos a la función combo inicial
        } else {
            _this.LabelBof.Text = "";//Asignamos texto
        }
        Parent_inObjectHTML.appendChild(inObjectHTML);//asignamos el div a su parent node
    }
    this.showHideColumGrupTable = function (Grilla) {}
    this.contador = 0;
    this._ListAgrupation = new Array();//Lista de agrupación
    this.agruparPorCampoEnabled = true;//Para verificar si podemos agrupar, esta variable la utilizó cuando paso una lista de agrupación.
    this._VclDrag = null;
    /*Propiedad para arrastar columnas*/
    Object.defineProperty(this, 'VclDrag', {
        get: function () {
            return this._VclDrag;
        },
        set: function (inValue) {
            this._VclDrag = inValue;
        }
    });
    /*Propiedad para formar agrupación desde una lista */
    Object.defineProperty(this, 'ListAgrupation', {
        get: function () {
            return this._ListAgrupation;
        },
        set: function (inValue) {
            this._ListAgrupation = inValue;
            this.Show = _this.inTreeGrid;
        }
    });
    /*Propiedad para mostrar Tree*/
    Object.defineProperty(this, 'Show', {
        get: function () {
            return _this.inTreeGrid;
        },
        set: function (inValue) {
            if (inValue) {
                var temp = this.inTreeGrid;//Guardamos valor de inTreeGrid
                this.Init(this.TVclGridCFSource, this.nameColumns, this.ItemTreePanel, this.open, this.orden, this.DataSet);//Volvemos a inicializar el componente
                var newListAgrupation = new Array();
                /*newListAgrupation: este array nos sirve para colocar solo las cadenas
                que se pueden agrupar. Estan cadenas se deben encontrar en las columnas visibles del grid.                
                */
                for (var i = 0; i < _this._ListAgrupation.length; i++) {//Lista de agrupaciones.
                    var column = _this.TVclGridCFSource.GetColumn(_this._ListAgrupation[i]);//Obtenemos la columna
                    if (column != null && !(column.IsOptionCheck || column.IsOption) && (column._Visible_IsShowValue || column._VisibleVertical_IsShowValue)) {
                        //Validamos si columna no es IsOptionCheck ni IsOption y que se puedan visualizar
                        newListAgrupation.push(column.Name);//agregar item
                    }
                }
                _this._ListAgrupation = newListAgrupation;//actualizamos valor de _ListAgrupation
                for (var i = 0; i < _this._ListAgrupation.length; i++) {//recorremos lista
                    for (var j = 0; j < _this.lastCombobox.Options.length; j++) {
                        temp = true;
                        if (_this.lastCombobox.Options[j].Tag == _this._ListAgrupation[i]) {
                            _this.lastCombobox.selectedIndex = j;
                            if (_this._ListAgrupation.length > 1) {
                                _this.agruparPorCampoEnabled = (_this._ListAgrupation.length - 1 == i);//habilitamos agrupacion si estamos en el último recorrido
                            }
                            _this.eventOnchange(_this.lastCombobox);//Forma los botones del combo
                        }
                    }
                }
                _this.agruparPorCampoEnabled = true;//Indicamos que ya podemos realizar el dibujado de la agrupación
                this.inTreeGrid = temp;//Recuperamos el valor inicial del inTreeGrid
                if (_this._ListAgrupation.length == 0) {//validamos si lista de agrupación esta vacía
                    $(objectContenGridHtml).show();//mostramos Grid del CFView
                    this.GridTreeGridControl = null;//Seteamos valor del grid
                    this.inTreeGrid = false;// ocultamos inTreeGrid
                }
            }
            if (inValue) {
                _this._ListAgrupation = new Array();//seteamos valor del listAgrupation
            }
        }
    });    
    /*Combo que se crea al inicio*/
    this.ComboInicial = function () {
        _this.nameColumns = new Array();//Nombre de columnas
        _this.sizeColumnTVclGridCFSource = _this.TVclGridCFSource.Columns.length - 2;
        //Numero de columnas
        for (var i = 0; i < _this.TVclGridCFSource.Columns.length; i++) {//Recorremos Columnas del Grid cfview
            var column = _this.TVclGridCFSource.Columns[i];//columnas del item
            if (!(column.IsOptionCheck || column.IsOption) && (column._Visible_IsShowValue || column._VisibleVertical_IsShowValue)) {//Validamos que no sea la columna check o button y que se encuentre visible, 
                var name = column.Name;
                _this.nameColumns.push(name);//agreagamos name al array
            }
        }
        var cbCombobox = new TVclComboBox2(_this.ContCboTreeGrid.Column[0].This, ""); //CREA UN COMBOBOX CONTROL
        cbCombobox.This.style.height = "32px";
        _this.lastCombobox = cbCombobox;//Indicamos que somos el último combo
        //Creamos boton (Quitar agrupación)
        var btn = new TVclButton(_this.ContCboTreeGrid.Column[0].This, "");//ceamos boton
        btn.Text = "";
        btn.Visible = false;//Ocultamos por defecto
        btn.Src = "image/16/Cancel.png";//Imagen
        btn.This.style.display = "none";//Ocultamos
        btn.VCLType = TVCLType.BS;//Estilo  boostrap
        btn.This.style.padding = "0px 9px";
        btn.This.style.marginBottom = "2px";
        btn.This.style.height = "32px";
        btn.Image.style.padding = "0px";
        btn.Image.style.opacity = "0.9";
        _this.lastBtn = btn;//Indicamos que es el último boton.
        var VclComboObjectNinguno = new TVclComboBoxItem();//Creamos combo 
        VclComboObjectNinguno.Value = -1;//posición -1
        VclComboObjectNinguno.Text = _this.TextCombobox;//Texto "Any"
        VclComboObjectNinguno.Tag = _this.TextCombobox;//tag.
        _this.itemsComboDinamicos = _this.nameColumns;//Seteamos el valor del array itemsComboDinamicos.
        cbCombobox.AddItem(VclComboObjectNinguno);//Agregamos combo.
        for (var i = 0; i < _this.nameColumns.length; i++) {//Recorremos columnas
            var VclComboObject = new TVclComboBoxItem();//creamos item de combo
            VclComboObject.Value = i;//valor
            VclComboObject.Text = _this.TVclGridCFSource.GetColumn(_this.nameColumns[i]).Caption;//calor caption columna
            VclComboObject.Tag = _this.nameColumns[i];//valor name columna
            cbCombobox.AddItem(VclComboObject);//Agregamos item
            cbCombobox.onChange = function () {//Evento al cambiar de opción
                _this.DataSet.Cancel();//**Colocarlo antes del IntreeGrid
                $(objectContenGridHtml).hide();//Ocultamos div del grid
                _this.inTreeGrid = true;//Configuramos como inTreeGrid=true
                ButtonImg_ExpanderBar.Visible = true;//Colocamos el bar expander como visible
                ButtonImg_ContraerBar.Visible = true;//Colocamos el bar contraer como visible
                _this.GridCFToolbar.ButtonImg_NavigatorToolsBar_ChekedClickDown = false;//ocultamos los bar de navegación
                var VclComboBoxItem = cbCombobox.Options[cbCombobox.This.selectedIndex];//Obtenemos el item del combo selecionado
                if (VclComboBoxItem.Value != -1) {//Value distinto de -1
                    cbCombobox.Enabled = false;//desabilitamos el combo
                    _this.createCombo(VclComboBoxItem.Tag);//creamos combo
                    btn.Visible = true;//Colocamos como visible el boton "Quitar agrupación"
                    btn.This.style.display = "";//Colocamos como visible el boton "Quitar agrupación"
                    btn.onClick = function () {//Evento para remover
                        $(_this.lastCombobox.getThis).remove();//eliminamos combo
                        btn.This.style.display = "none";//ocultamos botón
                        $(btn.This).remove();//eliminamos botón
                        $(cbCombobox.getThis).remove();//eliminamos botón
                        _this.RemoverAgrupacion(VclComboBoxItem.Tag);//
                    }
                }
            }
            cbCombobox.Value = -1;
        }

    }
    /*Esta función sirve para saber si una columna que estamos arrastrando ya esta  agrupada */
    this.IsColumnGroupInGrid = function (descripcion) {
        var bandera = false;
        for (var j = 0; j < _this.lastCombobox.Options.length; j++) {//Recorremos array
            if (_this.lastCombobox.Options[j].Tag == descripcion) {//validamos si la descripción existe
                _this.lastCombobox.selectedIndex = j;
                _this.eventOnchange(_this.lastCombobox);//Forma los combos
                bandera = true;
                break;
            }
        }
        return bandera;
    }
    this.eventOnchange = function (lastCombobox) {
        var cbCombobox = lastCombobox;
        var btn = _this.lastBtn;
        btn.Visible = true;
        btn.This.style.display = "";
        if (_this.nivelesTree.length == 0) {//Si no hay combos de agrupación selecionados(nivelesTree).
            _this.DataSet.Cancel();//**Colocarlo antes del IntreeGrid
            $(objectContenGridHtml).hide();
            _this.inTreeGrid = true;
            ButtonImg_ExpanderBar.Visible = true;
            ButtonImg_ContraerBar.Visible = true;
            _this.GridCFToolbar.ButtonImg_NavigatorToolsBar_ChekedClickDown = false;
            var VclComboBoxItem = cbCombobox.Options[cbCombobox.This.selectedIndex];
            if (VclComboBoxItem.Value != -1) {
                btn.Visible = true;
                cbCombobox.Enabled = false;
                _this.createCombo(VclComboBoxItem.Tag);
                btn.Visible = true;
                btn.This.style.display = "";
                btn.onClick = function () {
                    $(_this.lastCombobox.getThis).remove();
                    btn.This.style.display = "none";
                    $(btn.This).remove();
                    $(cbCombobox.getThis).remove();
                    _this.RemoverAgrupacion(VclComboBoxItem.Tag);
                }
            }
        }
        else {
            var VclComboBoxItem2 = cbCombobox.Options[cbCombobox.This.selectedIndex];
            if (VclComboBoxItem2.Value != -1) {
                btn.Visible = true;
                btn.This.style.display = "";
                _this.createCombo(VclComboBoxItem2.Tag);
                cbCombobox.Enabled = false;
                btn.onClick = function () {
                    if (_this.lastCombobox != null) {
                        $(_this.lastCombobox.getThis).remove();
                    }
                    btn.This.style.display = "none";
                    $(btn.This).remove();
                    $(cbCombobox.getThis).remove();
                    _this.RemoverAgrupacion(VclComboBoxItem2.Tag);
                }
            }

        }

    }
    /*Función para crear combo*/
    this.createCombo = function (posionSeleccionada) {
        if (posionSeleccionada != -1 || posionSeleccionada != _this.TextCombobox) {
            var cbCombobox2 = new TVclComboBox2(_this.ContCboTreeGrid.Column[0].This, ""); //CREA UN COMBOBOX CONTROL 
            cbCombobox2.This.style.marginLeft = "10px";
            cbCombobox2.This.style.height = "32px";
            var btn = new TVclButton(_this.ContCboTreeGrid.Column[0].This, "");
            btn.Text = "";
            btn.Visible = false;
            btn.Src = "image/16/Cancel.png";
            btn.This.style.display = "none";
            btn.VCLType = TVCLType.BS;
            btn.This.style.padding = "0px 9px";
            btn.This.style.marginBottom = "2px";
            btn.This.style.height = "32px";
            btn.Image.style.padding = "0px";
            btn.Image.style.opacity = "0.9";
            var VclComboObjectNinguno = new TVclComboBoxItem();
            VclComboObjectNinguno.Value = -1;
            VclComboObjectNinguno.Text = _this.TextCombobox;
            VclComboObjectNinguno.Tag = _this.TextCombobox;
            cbCombobox2.AddItem(VclComboObjectNinguno);
            var listaAA = _this.itemsComboDinamicos;
            _this.itemsComboDinamicos = [];
            for (var i = 0; i < listaAA.length; i++) {
                if (posionSeleccionada != listaAA[i]) {
                    _this.itemsComboDinamicos.push(listaAA[i]);
                }
                else {
                    for (var h = 0; h < _this.nameColumns.length; h++) {
                        if (_this.nameColumns[h] == posionSeleccionada) {
                            _this.nivelesTree.push(h);
                        }
                    }
                }
            }
            _this.agruparPorCampo();
            for (var i = 0; i < listaAA.length; i++) {
                if ("" + posionSeleccionada != "" + listaAA[i]) {
                    var VclComboObject2 = new TVclComboBoxItem();
                    VclComboObject2.Value = i;
                    VclComboObject2.Text = _this.TVclGridCFSource.GetColumn(listaAA[i]).Caption;
                    VclComboObject2.Tag = listaAA[i];
                    cbCombobox2.AddItem(VclComboObject2);
                    cbCombobox2.onChange = function () {
                        var VclComboBoxItem2 = cbCombobox2.Options[cbCombobox2.This.selectedIndex];
                        if (VclComboBoxItem2.Value != -1) {
                            btn.Visible = true;
                            btn.This.style.display = "";
                            _this.createCombo(VclComboBoxItem2.Tag);
                            cbCombobox2.Enabled = false;
                            btn.onClick = function () {
                                if (_this.lastCombobox != null) {
                                    $(_this.lastCombobox.getThis).remove();
                                }
                                btn.This.style.display = "none";
                                $(btn.This).remove();
                                $(cbCombobox2.getThis).remove();
                                _this.RemoverAgrupacion(VclComboBoxItem2.Tag);
                            }
                        }
                    }
                }
            }
            _this.lastCombobox = cbCombobox2;
            _this.lastBtn = btn;
            cbCombobox2.Value = -1;
        }
    }
     /*Función para eliminar combo después de selecionar una agrupación*/
    this.RemoverAgrupacion = function (valor) {
        var posionNivel = null;
        if (_this.nivelesTree.length == 1) {//Si hay un combo de agrupación selecionado(nivelesTree).
            _this.nivelesTree = [];
            _this.lastCombobox.Visible = false;
            $(_this.ConTreeControl.Column[0].This).html("");
            $(objectContenGridHtml).show();
            _this.GridTreeGridControl.ClearAllRows();
            this.GridTreeGridControl = null;
            _this.inTreeGrid = false;
            ButtonImg_ExpanderBar.Visible = false;
            ButtonImg_ContraerBar.Visible = false;
            _this.GridCFToolbar.ButtonImg_NavigatorToolsBar_ChekedClickDown = true;
            if (_this.GridCFView != null) {
                _this.GridCFView.restartDataRecords();
                _this.GridCFView.VPagination.LinkImgGruopOpen.style.display = "none";
            }
           
            return true;
        }
        for (var i = 0; i < _this.nameColumns.length; i++) {
            if ("" + _this.nameColumns[i] == "" + valor) {
                posionNivel = i;

            }
        }
        var cbCombobox2 = new TVclComboBox2(_this.ContCboTreeGrid.Column[0].This, ""); //CREA UN COMBOBOX CONTROL
        cbCombobox2.This.style.marginLeft = "10px";
        cbCombobox2.This.style.height = "32px";
        var btn = new TVclButton(_this.ContCboTreeGrid.Column[0].This, "");
        btn.Text = "";
        btn.Visible = false;
        btn.Src = "image/16/Cancel.png";
        btn.This.style.display = "none";
        btn.VCLType = TVCLType.BS;
        btn.This.style.padding = "0px 9px";
        btn.This.style.marginBottom = "2px";
        btn.This.style.height = "32px";
        btn.Image.style.padding = "0px";
        btn.Image.style.opacity = "0.9";
        var VclComboObjectNinguno = new TVclComboBoxItem();
        VclComboObjectNinguno.Value = -1;
        VclComboObjectNinguno.Text = _this.TextCombobox;
        VclComboObjectNinguno.Tag = _this.TextCombobox;
        cbCombobox2.AddItem(VclComboObjectNinguno);
        _this.itemsComboDinamicos.push(valor);
        var listaAA = _this.itemsComboDinamicos;
        var tempNiveles = _this.nivelesTree;
        _this.itemsComboDinamicos = [];
        _this.nivelesTree = [];
        for (var i = 0; i < tempNiveles.length; i++) {
            if ("" + posionNivel != "" + tempNiveles[i]) {
                _this.nivelesTree.push(tempNiveles[i]);
            }
        }
        for (var i = 0; i < listaAA.length; i++) {
            for (var j = 0; j < _this.nameColumns.length; j++) {
                if ("" + listaAA[i] == "" + _this.nameColumns[j]) {
                    _this.itemsComboDinamicos.push(_this.nameColumns[j]);
                }
            }
        }
        _this.agruparPorCampo();
        for (var i = 0; i < _this.itemsComboDinamicos.length; i++) {
            var VclComboObject2 = new TVclComboBoxItem();
            VclComboObject2.Value = i;
            VclComboObject2.Text = _this.TVclGridCFSource.GetColumn(_this.itemsComboDinamicos[i]).Caption;
            VclComboObject2.Tag = _this.itemsComboDinamicos[i];
            cbCombobox2.AddItem(VclComboObject2);
            cbCombobox2.onChange = function () {
                var VclComboBoxItem2 = cbCombobox2.Options[cbCombobox2.This.selectedIndex];
                if (VclComboBoxItem2.Value != -1) {
                    _this.createCombo(VclComboBoxItem2.Tag);
                    cbCombobox2.Enabled = true;
                    btn.Visible = true;
                    btn.This.style.display = "";
                    btn.onClick = function () {
                        btn.This.style.display = "none";
                        $(btn.This).remove();
                        $(cbCombobox2.getThis).remove();
                        if (_this.lastCombobox != null) {
                            $(_this.lastCombobox.getThis).remove();
                        }
                        _this.RemoverAgrupacion(VclComboBoxItem2.Tag);
                    }
                }
            }
        }
        _this.lastCombobox = cbCombobox2;
        _this.lastBtn = btn;
        cbCombobox2.Value = -1;
    }
    this.tipoOperacion = null;//TIPO DE OPERACION QUE SE REALIZARA EN EL POST --- 0="UPDATE",1="INSERT"
    /*Función para dibujar los botones*/
    this.dibujarCellButton = function (opcion, NewRow, CellButton, objetos) {
        var divCellButtonControl = CellButton.This;
        CellButton.Control = true;//Asignamos un valor cualquiera   
        CellButton.btnInsert = new TVclButton(divCellButtonControl, "");
        CellButton.btnInsert.Text = "";
        CellButton.btnInsert.VCLType = TVCLType.BS;
        CellButton.btnInsert.ObjectButton = objetos;
        CellButton.btnInsert.onClickObjectButton = function (objetos) {
            if (_this.rowSelecionado != null && _this.rowSelecionado != objetos["Row"]) {
                _this.EventCancel(_this.rowSelecionado);
            }
            objetos["Row"].onclick = function () {
            }
            _this.rowSelecionado = objetos["Row"];
            _this.EventInsert(_this.rowSelecionado);
        }
        CellButton.btnUpdate = new TVclButton(divCellButtonControl, "");
        CellButton.btnUpdate.Text = "";
        CellButton.btnUpdate.VCLType = TVCLType.BS;
        CellButton.btnUpdate.ObjectButton = objetos;
        CellButton.btnUpdate.onClickObjectButton = function (objetos) {
            if (_this.rowSelecionado != null && _this.rowSelecionado != objetos["Row"]) {
                _this.EventCancel(_this.rowSelecionado);
            }
            objetos["Row"].onclick = function () {
            }
            _this.rowSelecionado = objetos["Row"];
            _this.seleccionarItem(objetos["Row"]);
        }
        CellButton.btnDelete = new TVclButton(divCellButtonControl, "");
        CellButton.btnDelete.Text = "";
        CellButton.btnDelete.VCLType = TVCLType.BS;
        CellButton.btnDelete.ObjectButton = objetos;
        CellButton.btnDelete.onClickObjectButton = function (objetos) {
            if (_this.rowSelecionado != null && _this.rowSelecionado != objetos["Row"]) {
                _this.EventCancel(_this.rowSelecionado);
            }
            objetos["Row"].onclick = function () {
            }
            _this.rowSelecionado = objetos["Row"];
            _this.EventDelete(objetos["Row"]);
        }
        CellButton.btnPost = new TVclButton(divCellButtonControl, "");
        CellButton.btnPost.Text = "";
        CellButton.btnPost.VCLType = TVCLType.BS;
        CellButton.btnPost.ObjectButton = objetos;
        CellButton.btnPost.onClickObjectButton = function (objetos) {
            _this.EventPost(objetos["Row"]);
        }
        CellButton.btnCancel = new TVclButton(divCellButtonControl, "");
        CellButton.btnCancel.Text = "";
        CellButton.btnCancel.VCLType = TVCLType.BS;
        CellButton.btnCancel.ObjectButton = objetos;
        CellButton.btnCancel.onClickObjectButton = function (objetos) {
            if (_this.rowSelecionado != null) {
                _this.EventCancel(_this.rowSelecionado);
            }
        }
        if (opcion) {
            CellButton.btnInsert.Enabled = true;
            CellButton.btnUpdate.Enabled = true;
            CellButton.btnDelete.Enabled = true;
            CellButton.btnPost.Enabled = false;
            CellButton.btnCancel.Enabled = false;
        } else {
            CellButton.btnInsert.Enabled = false;
            CellButton.btnUpdate.Enabled = false;
            CellButton.btnDelete.Enabled = false;
            CellButton.btnPost.Enabled = true;
            CellButton.btnCancel.Enabled = true;
        }
        var iApped = document.createElement("i");
        var iEdit = document.createElement("i");
        var iDelete = document.createElement("i");
        var iPost = document.createElement("i");
        var iCancel = document.createElement("i");
        iApped.classList.add("icon");
        iApped.classList.add("ion-plus-circled");
        iApped.classList.add("text-success");
        iEdit.classList.add("icon");
        iEdit.classList.add("ion-edit");
        iEdit.style.color = "#FFC400";
        iDelete.classList.add("icon");
        iDelete.classList.add("ion-close-circled");
        iDelete.classList.add("text-danger");
        iPost.classList.add("icon");
        iPost.classList.add("ion-checkmark-circled");
        iPost.classList.add("text-success");
        iCancel.classList.add("demo-icon");
        iCancel.classList.add("ion-minus-circled");
        iCancel.classList.add("text-danger");
        CellButton.btnInsert.This.appendChild(iApped);
        CellButton.btnUpdate.This.appendChild(iEdit);
        CellButton.btnDelete.This.appendChild(iDelete);
        CellButton.btnPost.This.appendChild(iPost);
        CellButton.btnCancel.This.appendChild(iCancel);
        NewRow.AddCell(CellButton);
    }
    /*Función para eliminar del DataSet*/
    this.EventDelete = function (Row) {
        if (Row == null || _this.LastRutaGroup == null || _this.ItemGroupPanel[_this.LastRutaGroup.RutaGruopDown].ItemTreePanel.length == 0) {//validamos que exista una fila selecionada
            alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines4"));//mostramos mensaje
            return true;
        }
        _this.tipoOperacion = 2;//cambiamos el tipo de operación
        _this.DataSet.SetIndex(Row.IndexDataSet);//cambiamos de index
        if (_this.DataSet != null) {//Validamos que DataSet sea distinto de nulo
            _this.DataSet.Delete();//llamamos a la función delete del DataSet
        }
    }
    /*Función para post del DataSet*/
    this.EventPost = function (Row) {       
        if (_this.DataSet != null) {//Validamos que DataSet sea distinto de nulo
            _this.DataSet.Post();
        }
    }
    /*Función para insertar del DataSet*/
    this.EventInsert = function (Row) {
        if (Row == null || _this.LastRutaGroup == null || _this.ItemGroupPanel[_this.LastRutaGroup.RutaGruopDown].ItemTreePanel.length == 0) {
            alert("select a row");
            return true;
        }
        _this.tipoOperacion = 1;//cambiamos el tipo de operación
        _this.rowSelecionado = Row;
        _this.agregarNuevaFila(Row.GridParent);//agregamos nueva fila
    }
    /*Función para cancelar operación del DataSet*/
    this.EventCancel = function (Row) {
        if (Row != null && (_this.DataSet.Status == SysCfg.MemTable.Properties.TStatus.Apped || _this.DataSet.Status == SysCfg.MemTable.Properties.TStatus.Update)) {//validamos que exista una fila selecionada y que se este produciendo un evento insert o delete
            if (_this.DataSet.Status == SysCfg.MemTable.Properties.TStatus.Apped) {
                var indexEliminado = Row.Index;
                _this.GridTreeGridControl.RemoveRow(indexEliminado);
                _this.GridTreeGridControl.IndexRow = indexEliminado;
                _this.rowSelecionado = _this.GridTreeGridControl._FocusedRowHandle;
                _this.rowSelecionado.Index = indexEliminado;
            }
            if (_this.DataSet.Status == SysCfg.MemTable.Properties.TStatus.Update) {
                var index = Row.IndexDataSet;
                //var item = _this.ItemTreePanel[Row.IndexTreeGrid];
                var NewRow = new TVclRowCF();//Creamos nueva Row
                for (var j = 0; j < Row.GridParent.Columns.length - 1; j > j++) {
                    Row.GridParent.Columns[j].EnabledEditor = false;
                    var Cell = new TVclCellCF();//Creamos celda
                    if (j == 0) {
                        Cell.Value = Row.Cells[0].Value;
                    } else {
                        Cell.Value = _this.DataSet.Records[index].Fields[Row.GridParent.Columns[j].IndexDataSourceDefault].Value;
                    }
                    Cell.IndexColumn = j; // ASIGNA O EXTRAE EL INDEX DE LA COLUMNA 
                    Cell.IndexRow = index;//Índice de fila
                    NewRow.AddCell(Cell);
                }
                var CellButton = new TVclCellCF();//Creamos celda
                CellButton.Value = "";//Value celda
                CellButton.IndexColumn = _this.sizeColumnTVclGridCFSource;//Índice de columna
                CellButton.IndexRow = Row.Index;//Índice de fila
                var objetos = { "Row": NewRow, "Grilla": Row.GridParent };
                _this.dibujarCellButton(true, NewRow, CellButton, objetos);
                NewRow.Index = Row.Index;
                NewRow.IndexTreeGrid = Row.IndexTreeGrid;
                NewRow.IndexDataSet = Row.IndexDataSet;
                for (var j = 0; j < _this.sizeColumnTVclGridCFSource; j++) {
                    Row.GridParent.Columns[j].EnabledEditor = false;
                }
                NewRow.onclick = function (NewRow) { // ASIGNA UN EVENTO CUANDO SE HACE CLICK EN EL ROW DEL GRIDCONTROL
                    _this.Grilla_rowclick_Tree(NewRow);
                }
                var indexActulizado = Row.Index;
                Row.GridParent.UpdatesRow(NewRow, _this.rowSelecionado);
                NewRow.Index = indexActulizado;
                _this.GridTreeGridControl.IndexRow = indexActulizado;
                _this.rowSelecionado = NewRow;

            }
            if (_this.ItemTreePanel.length > 0) {
                _this.DataSet.Status = SysCfg.MemTable.Properties.TStatus.None;
                _this.GridTreeGridControl.IndexRow = Row.Index;
                _this.DataSet.RecordSet = _this.DataSet.Records[_this.GridTreeGridControl.FocusedRowHandle.IndexDataSet];
                _this.GridCFView.HabilitarCamposFormGrid();
                _this.DataSet.Cancel();
            } else {
                if (_this.DataSet != null) {
                    _this.DataSet.Status = SysCfg.MemTable.Properties.TStatus.None;
                    _this.DataSet.Cancel();
                    //_this.DataSet.Cancel();
                }
            }
        }
    }
    this.contadorIndiceRow = 0;
    this.RowGroup = new Array();//Lista de grupos de Rows
    this.ListRouteGroup = new Array();//Lista grupos de rutas
    this.LastRutaGroup = null;//Última ruta
    this.agruparPorCampo = function () {
        if (_this.agruparPorCampoEnabled) {//Verificamos si podemos agrupar, esta variable la utilizó cuando paso una lista de agrupación
            _this.DataSet.Status = SysCfg.MemTable.Properties.TStatus.None;//Status en none
            _this.GridCFView.Information.VclGridCFInfo.ClearAll();//Limpiamos grid
            _this.contadorIndiceRow = 0;
            _this.rowSelecionado = null;
            _this.LastRutaGroup = null;//Última ruta
            _this.groupByGridTabla(_this.ItemTreePanel, _this.sizeColumnTVclGridCFSource);//Agrupar tabla
        }
    }
    /*Función clic*/
    this.Grilla_rowclick_Tree = function (NewRow) {
        NewRow.Index = $(NewRow.This).index();//Índice real de la fila en la tabla
        if (_this.rowSelecionado != null) {//Si existe una fila selecionada
            _this.EventCancel(_this.rowSelecionado);//Cancelamos
        }
        _this.rowSelecionado = NewRow;//Nueva fila selecionada
        _this.DataSet.SetIndex(NewRow.IndexDataSet);//Cambiamos índice del dataset
        NewRow.Index = $(NewRow.This).index();//seteamos índice de fila
        NewRow.GridParent.IndexRow = NewRow.Index;//seteamos IndexRow
    }
    /*Función para cambiar la Row a editable*/
    this.seleccionarItem = function (Row) {
        var index = Row.Index;   
        var NewRow = new TVclRowCF();//Creamos nueva Row
        for (var j = 0; j < Row.GridParent.Columns.length - 1; j > j++) {
            Row.GridParent.Columns[j].EnabledEditor = true;
            Row.GridParent.Columns[j].ActiveEditor = Row.GridParent.Columns[j].UpdateActiveEditor;
            var Cell = new TVclCellCF();//Creamos celda
            if (j == 0) {
                Cell.Value = Row.Cells[0].Value;//Value celda
            } else {
                Cell.Value = _this.DataSet.RecordSet.Fields[Row.GridParent.Columns[j].IndexDataSourceDefault].Value;//Value celda
            }
            Cell.IndexColumn = j;//Índice de columna
            Cell.IndexRow = index;//Índice de fila
            Cell.IndexRowTreeGrid = Row.IndexTreeGrid;
            NewRow.AddCell(Cell);
        }
        NewRow.onclick = function (NewRow) {

        }
        //creamos celda para botones
        var CellButton = new TVclCellCF();//Creamos celda
        CellButton.Value = "";//Value celda
        CellButton.IndexColumn = _this.sizeColumnTVclGridCFSource;//Índice de columna igual al número de columnas
        CellButton.IndexRow = index;//Índice de fila
        CellButton.IndexRowTreeGrid = _this.ItemTreePanel[Row.IndexTreeGrid][_this.sizeColumnTVclGridCFSource + 1];
        var objetos = { "Row": NewRow, "Grilla": Row.GridParent };
        _this.dibujarCellButton(false, NewRow, CellButton, objetos);
        NewRow.Index = index;
        NewRow.IndexTreeGrid = Row.IndexTreeGrid;
        NewRow.IndexDataSet = Row.IndexDataSet;
        Row.GridParent.UpdatesRow(NewRow, Row);
        NewRow.GridParent.FocusedRowHandle = NewRow;
        _this.rowSelecionado = NewRow;
        if (_this.DataSet != null) {
            _this.DataSet.Update();
        }
    }
    /*Función insert una Row a editable temporalmente*/
    this.agregarNuevaFila = function (Grilla) {
        var RecordSet = new SysCfg.MemTable.Properties.TRecord();
        for (var ContX = 0; ContX <= _this.DataSet.FieldCount - 1; ContX++) {
            var Field = new SysCfg.MemTable.Properties.TField();
            Field.FieldDef = _this.DataSet.FieldDefs[ContX];
            switch (_this.DataSet.FieldDefs[ContX].DataType) {
                case SysCfg.DB.Properties.TDataType.String:
                    Field.Value = "";
                    break;
                case SysCfg.DB.Properties.TDataType.Text:
                    Field.Value = "";
                    break;
                case SysCfg.DB.Properties.TDataType.Int32:
                    Field.Value = 0;
                    break;
                case SysCfg.DB.Properties.TDataType.Double:
                    Field.Value = 0.00;
                    break;
                case SysCfg.DB.Properties.TDataType.Decimal:
                    Field.Value = 0.00;
                    break;
                case SysCfg.DB.Properties.TDataType.Boolean:
                    Field.Value = false;
                    break;
                case SysCfg.DB.Properties.TDataType.DateTime:
                    Field.Value = Date.now();
                    break;
                case SysCfg.DB.Properties.TDataType.Object:
                    Field.Value = null;
                    break;
            }
            RecordSet.Fields.push(Field);
        }
        var NewRow = new TVclRowCF();//Creamos nueva Row
        for (var j = 0; j < Grilla.Columns.length - 1; j > j++) {
            Grilla.Columns[j].EnabledEditor = true;
            Grilla.Columns[j].ActiveEditor = Grilla.Columns[j].InsertActiveEditor;
            var Cell = new TVclCellCF();//Creamos celda
            if (j == 0) {
                Cell.Value = true;//Value celda
            } else {
                Cell.Value = RecordSet.Fields[j - 1].Value;//Value celda
            }
            Cell.IndexColumn = j;//Índice de columna
            Cell.IndexRow = _this.ItemTreePanel.length;//Índice de fila
            NewRow.AddCell(Cell);
        }
        var objetos = { "Row": NewRow, "Grilla": Grilla };
        var CellButton = new TVclCellCF();//Creamos celda
        CellButton.Value = "";//Value celda
        CellButton.IndexColumn = Grilla.length;//Índice de columna
        CellButton.IndexRow = -1//Índice de fila
        _this.dibujarCellButton(false, NewRow, CellButton, objetos);
        Grilla.AddRowBefore(false, NewRow, _this.rowSelecionado);
        Grilla.FocusedRowHandle = NewRow;
        NewRow.Index = _this.rowSelecionado.Index;
        NewRow.IndexTreeGrid = _this.rowSelecionado.IndexTreeGrid;
        //NewRow.Cells[0].Control.This.focus();
        for (var j = 0; j < Grilla.Columns.length - 1; j > j++) {
            Grilla.Columns[j]._EnabledEditor = false;
        }
        _this.rowSelecionado = NewRow;
        if (_this.DataSet != null) {
            _this.DataSet.Append();
        }
    }
    this.groupBy = function (miarray, prop) {
        return miarray.reduce(function (groups, item) {
            var val = item[prop];
            groups[val] = groups[val] || item;
            return groups;

        }, {});
    }//AGRUPA LOS ARRAY SEGUN UN VALOR
    /*Función para expander o contraer agrupaciones*/
    this.expander = function (opcion) {
        if (opcion) {
            for (var i = 0; i < _this.GridTreeGridControl.Rows.length; i++) {
                var row = _this.GridTreeGridControl.Rows[i];
                row.Visible = true;
                if (row.RowHijos.length > 0) {
                    row._ImgGruopDown.style.display = '';//Mostramos imagen de flecha("Expandida")
                    row._ImgGruopRigth.style.display = 'none';//Ocultamos imagen de flecha("Collapse")
                }
            }
        }
        else {
            for (var i = 0; i < _this.GridTreeGridControl.Rows.length; i++) {
                var row = _this.GridTreeGridControl.Rows[i];
                row.Visible = (row.RowPadre == null);

                if (row.RowHijos.length > 0) {
                    row._ImgGruopDown.style.display = 'none';//Ocultamos imagen de flecha("Expandida")
                    row._ImgGruopRigth.style.display = '';//Mostramos imagen de flecha("Collapse")
                }

            }
        }
    }
     /*Función para mostrar o ocultar una columna*/
    this.showHideColumns = function (index, valor) {
        _this.GridTreeGridControl.showHideColumn(index, valor);
    };
     /*Función que retorna el row selecionado*/
    this.getRowSeleccionado = function () {
        return _this.rowSelecionado;
    };
     /*Función para eliminar del DataSet desde el Cfview*/
    this.buttonEventDeleteTreeGrid = function () {
        if (this.getRowSeleccionado() != null) {
            _this.EventDelete(this.getRowSeleccionado());
        } else {
            alert("select a row");
        }
    };
    /*Función para insert del DataSet desde el Cfview*/
    this.buttonEventInsertTreeGrid = function () {
        if (this.getRowSeleccionado() != null) {
            _this.EventInsert(this.getRowSeleccionado());
        } else {
            alert("select a row");
        }
    };
    /*Función para update del DataSet desde el Cfview*/
    this.buttonEventUpdateTreeGrid = function () {
        if (this.getRowSeleccionado() == null || _this.LastRutaGroup == null || _this.ItemGroupPanel[_this.LastRutaGroup.RutaGruopDown].ItemTreePanel.length == 0) {
            alert("select a row");
        } else {
            _this.seleccionarItem(this.getRowSeleccionado());
        }
    };
    /*Función para post del DataSet desde el Cfview*/
    this.buttonEventPostTreeGrid = function () {
        if (this.getRowSeleccionado() != null) {
            _this.EventPost(this.getRowSeleccionado());
        } else {
            alert("select a row");
        }
    };
    /*Función para cancel del DataSet desde el Cfview*/
    this.buttonEventCancelTreeGrid = function () {
        if (this.getRowSeleccionado() != null) {
            _this.EventCancel(this.getRowSeleccionado());
        }
    };
     /*Función para mostrar o ocultar la columna de botones desde el Cfview*/
    this.enableEditOptiosColumn = function (opcion) {
        _this.GridTreeGridControl.EnableEditOptionsColumn = opcion;
    };
    /*Función para contraer el tamaño de las columnas del grid*/
    this.ContracAllColumns = function () {
        _this.GridTreeGridControl.ContracAllColumns();
    }
    /*Función para expander el tamaño de las columnas del grid*/
    this.ExpandAllColumns = function () {
        _this.GridTreeGridControl.ExpandAllColumnsTree();
    }
     /*Función que se produce en el EventAfterChange del cfview*/
    this.EventAfterChange = function (Status, DataSetIndex) {
        var Row = this.getRowSeleccionado();//Row selecionado
        if (Status == SysCfg.MemTable.Properties.TStatus.Apped) {
            //Si se insertó un nuevo record, entonces creamos una agrupación o agregamos el nuevo item a una ya existente
            var z = DataSetIndex;
            var colspan = _this.TVclGridCFSource.Columns.length;
            var OpcionVisibleButton = _this.TVclGridCFSource.Columns[colspan - 1].Visible;
            var OpcionVisibleCheck = _this.TVclGridCFSource.Columns[0].Visible;
            var grilla = _this.GridTreeGridControl;
            var IndexFila = Row.Index;
            grilla.RemoveRow(Row.Index);
            grilla.Rows[IndexFila].This.style.backgroundColor = "";
            _this.rowSelecionado = null;
            var item = _this.ItemTreePanel[z];
            var ruta = "";
            var val = "";
            var i = _this.contadorIndiceRow;
            var IndexTreeGrid = z;
            var ImgGruop = new Array();
            ImgGruop[true] = "";
            ImgGruop[false] = "none";
            for (var m = 0; m < _this.nivelesTree.length; m++) {//Recorremos la lista que tiene la cadena de los combos selecionados.
                if (m + 1 == _this.nivelesTree.length) {//Si estamos en el último item del array (nivelesTree)
                    ruta = ruta + _this.nameColumns[_this.nivelesTree[m]] + ":" + item[_this.nivelesTree[m]][0] + "";//formamos cadena de la ruta
                } else {
                    ruta = ruta + _this.nameColumns[_this.nivelesTree[m]] + ":" + item[_this.nivelesTree[m]][0] + "\\";//formamos cadena de la ruta
                }
            }
            val = ruta;
            var subRuta = "";//SubRutas: por ejemplo la ruta ["País:Ciudad"], entonces tenemos con sub rutas["Perú:Lima","Perú:Trujillo"]
            var espacios = 0;//Espacios que correr el texto a la derecha.
            if (typeof (_this.RowGroup[val]) == "undefined") {
                var words = ruta.split('\\');//separamos el texto de la ruta
                subRuta = subRuta + words[0];//Agregamos el primer texto de la separación.
                for (var Counter = 0; Counter < words.length; Counter++) {//Recorremos el array de la palabras separadas
                    var subRutaPadre = subRuta;//SubRutas: por ejemplo la ruta ["País:Ciudad"], entonces tenemos con sub rutas["Perú:Lima","Perú:Trujillo"]
                    if (Counter > 0) {
                        subRuta = subRuta + "\\" + words[Counter];//SubRutas: por ejemplo la ruta ["País:Ciudad"], entonces tenemos con sub rutas["Perú:Lima","Perú:Trujillo"]
                        espacios = espacios + 20;//Agregamos espacios, esto sirve para correr el texto a la derecha.
                    }
                    var NewRow = new TVclRowCF();//Creamos nueva Row
                    if (typeof (_this.RowGroup[subRuta]) == "undefined") {
                        var Cell = new TVclCellCF();//Creamos celda
                        Cell._Value = words[Counter];//Value celda ("Texto de la sub agrupación")
                        Cell.IndexColumn = 0;//Índice de columna
                        Cell.IndexRow = i;//Índice de fila
                        Cell.IndexRowTreeGrid = IndexTreeGrid;
                        NewRow.AddCell(Cell);
                        _this.RowGroup[subRuta] = NewRow;//agregamos a array que guarda Row Group.
                        NewRow.PaddingGroup = espacios;//Agregamos espacios, esto sirve para correr el texto a la derecha.
                        NewRow.This.style.backgroundColor = "#3e94ba";//Bacukground de fila que es agrupación
                        NewRow.onclick = function (NewRow) {
                        }
                        if (Counter > 0) {//Si existen sub rutas
                            NewRow.Visible = _this.open;
                            var cantidadHijos = (_this.RowGroup[subRutaPadre].CantidadHijos + 1);//Aumentamos +1 la cantidad de hijas de la fila padre
                            _this.RowGroup[subRutaPadre].RowHijos.push(NewRow);
                            _this.RowGroup[subRutaPadre].CantidadHijos = cantidadHijos; //Actualizamos cantidad de hijos
                            NewRow._ImgGruopDown.style.display = ImgGruop[_this.open];//Mostramos imagen de flecha("Expandida") si por defecto la agrupación esta abierta
                            NewRow._ImgGruopRigth.style.display = ImgGruop[!_this.open];//Ocultamos imagen de flecha("Collapse") si por defecto la agrupación esta cerrada
                            grilla.AddRowAfter(1, NewRow, _this.RowGroup[subRutaPadre], colspan);//Agregamos fila sub-padre("Ruta principal"), por ejemplo tenemos la rutas["País:Perú",["ciudad:lima","ciudad:trujillo"]] y ["Pais:Colombia",["ciudad:cali","ciudad:bogotá"]] las sub-rutas l son ["ciudad:lima"],["ciudad:trujillo"],["ciudad:cali"] y ["ciudad:bogotá"]
                            NewRow.RowPadre = _this.RowGroup[subRutaPadre];
                        } else {
                            grilla.AddRowAfter(0, NewRow, null, colspan);//Agregamos fila padre("Ruta principal"), por ejemplo tenemos la rutas["País:Perú","ciudad:lima"] y ["Pais:Colombia","ciudad:cali"] la ruta padre pincipal son ["País:Perú"] y ["País:Colombia"]
                            NewRow._ImgGruopDown.style.display = 'none';//Ocultamos imagen de flecha("Expandida")
                            NewRow._ImgGruopRigth.style.display = '';//Mostramos imagen de flecha("Collapse")
                        }
                    }
                }
                _this.ItemGroupPanel[val] = {
                    "ItemTreePanel": new Array()
                }
                _this.RowGroup[val].RutaGruopDown = val;
                /*Función al abrir agrupación*/
                _this.RowGroup[val].OnEventGruopDown = function (collapse, RutaGruopDown, NewRowPadre) {

                    if (collapse) {
                        //_this.GridTreeGridControl.IndexRow = null;
                        if (_this.LastRutaGroup != null) {

                            _this.RowGroup[_this.LastRutaGroup.RutaGruopDown]._ImgGruopDown.style.display = 'none';//Ocultamos imagen de flecha("Expandida")
                            _this.RowGroup[_this.LastRutaGroup.RutaGruopDown]._ImgGruopRigth.style.display = '';//Mostramos imagen de flecha("Collapse")
                            var tempIndexRowStart = _this.LastRutaGroup.IndexRowStart;
                            for (var IndexRowStart = _this.LastRutaGroup.IndexRowStart; IndexRowStart <= _this.LastRutaGroup.IndexRowEnd; IndexRowStart++) {
                                _this.GridTreeGridControl.RemoveRow(tempIndexRowStart);
                            }
                        }
                        _this.LastRutaGroup = {
                            RutaGruopDown: RutaGruopDown,
                            IndexRowStart: null,
                            IndexRowEnd: null,
                        }
                        var lastRow = null;
                        for (var y = 0; y < _this.ItemGroupPanel[RutaGruopDown].ItemTreePanel.length; y++) {
                            var item = _this.ItemGroupPanel[RutaGruopDown].ItemTreePanel[y];
                            var IndexTreeGrid = y;
                            var NewRow2 = new TVclRowCF();//Creamos nueva Row
                            NewRow2.Visible = true;
                            var Cell0 = new TVclCellCF();//Creamos celda
                            Cell0.Value = true;//Value celda
                            Cell0.IndexColumn = 0;//Índice de columna
                            Cell0.IndexRow = y;//Índice de fila
                            Cell0.Visible = OpcionVisibleCheck;
                            Cell0.IndexRowTreeGrid = IndexTreeGrid;
                            NewRow2.AddCell(Cell0);
                            var IndiceReal = item[_this.sizeColumnTVclGridCFSource + 1];
                            for (var j = 1; j < _this.TVclGridCFSource.Columns.length - 1; j++) {//Recorremos Columnas del Grid cfview
                                var IndexDataSourceDefault = _this.TVclGridCFSource.Columns[j].IndexDataSourceDefault;
                                var Cell = new TVclCellCF();//Creamos celda
                                Cell.Value = _this.DataSet.Records[IndiceReal].Fields[IndexDataSourceDefault].Value;//Value celda
                                Cell.IndexColumn = j;//Índice de columna
                                Cell.IndexRow = y;
                                Cell.IndexRowTreeGrid = IndexTreeGrid;
                                NewRow2.AddCell(Cell);
                            }

                            var CellButton = new TVclCellCF();//Creamos celda
                            CellButton.Value = "";//Value celda
                            CellButton.IndexColumn = colspan;//Índice de columna
                            CellButton.IndexRow = y;//Índice de fila
                            CellButton.Visible = OpcionVisibleButton;
                            CellButton.IndexRowTreeGrid = IndexTreeGrid;
                            var objetos = { "Row": NewRow2, "Grilla": grilla };
                            _this.dibujarCellButton(true, NewRow2, CellButton, objetos);
                            NewRow2.IndexTreeGrid = IndexTreeGrid;
                            NewRow2.onclick = function (NewRow) {

                                _this.Grilla_rowclick_Tree(NewRow);

                            }
                            NewRowPadre.CantidadHijos = NewRowPadre.CantidadHijos + 1;//Aumentamos +1 la cantidad de hijas de la fila padre
                            NewRowPadre.RowHijos.push(NewRow2);
                            NewRow2.RowPadre = NewRowPadre;
                            if (lastRow == null) {
                                grilla.AddRowAfter(2, NewRow2, NewRowPadre, null);
                                lastRow = NewRow2;
                            } else {
                                grilla.AddRowAfter(2, NewRow2, lastRow, null);
                                lastRow = NewRow2;
                            }

                            NewRow2.Index = $(NewRow2.This).index();
                            NewRow2.IndexDataSet = IndiceReal;
                            NewRow2.IndexTreeGrid = y;

                            if (y == 0) {
                                _this.LastRutaGroup.IndexRowStart = $(NewRow2.This).index();
                                _this.GridTreeGridControl.IndexRow = $(NewRow2.This).index();
                            }
                            if (y == _this.ItemGroupPanel[RutaGruopDown].ItemTreePanel.length - 1) {
                                _this.LastRutaGroup.IndexRowEnd = _this.LastRutaGroup.IndexRowStart + (_this.ItemGroupPanel[RutaGruopDown].ItemTreePanel.length - 1);
                            }

                        }


                    } else {
                        if (_this.LastRutaGroup != null) {

                            _this.RowGroup[_this.LastRutaGroup.RutaGruopDown]._ImgGruopDown.style.display = 'none';//Ocultamos imagen de flecha("Expandida")
                            _this.RowGroup[_this.LastRutaGroup.RutaGruopDown]._ImgGruopRigth.style.display = '';//Mostramos imagen de flecha("Collapse")
                            var tempIndexRowStart = _this.LastRutaGroup.IndexRowStart;
                            for (var IndexRowStart = _this.LastRutaGroup.IndexRowStart; IndexRowStart <= _this.LastRutaGroup.IndexRowEnd; IndexRowStart++) {
                                _this.GridTreeGridControl.RemoveRow(tempIndexRowStart);
                            }
                            _this.LastRutaGroup = null;
                        }
                    }
                }
            }

            _this.ItemGroupPanel[val].ItemTreePanel.push(item);
            if (_this.LastRutaGroup != null && _this.LastRutaGroup.RutaGruopDown == val) {
                _this.LastRutaGroup.IndexRowEnd++;
                var NewRow2 = new TVclRowCF();//Creamos nueva Row
                NewRow2.Visible = true;
                var Cell0 = new TVclCellCF();//Creamos celda
                Cell0.Value = true;//Value celda ("Texto de la sub agrupación")
                Cell0.IndexColumn = 0;//Índice de columna
                Cell0.IndexRow = z;//Índice de fila
                Cell0.Visible = OpcionVisibleCheck;
                Cell0.IndexRowTreeGrid = IndexTreeGrid;
                NewRow2.AddCell(Cell0);
                var IndiceReal = item[_this.sizeColumnTVclGridCFSource + 1];
                for (var j = 1; j < _this.TVclGridCFSource.Columns.length - 1; j++) {//Recorremos Columnas del Grid cfview
                    var Cell = new TVclCellCF();//Creamos celda
                    Cell.Value = _this.DataSet.Records[IndiceReal].Fields[IndexDataSourceDefault].Value;//Value celda
                    Cell.IndexColumn = j;//Índice de columna
                    Cell.IndexRow = z;//Índice de fila
                    Cell.IndexRowTreeGrid = IndexTreeGrid;
                    NewRow2.AddCell(Cell);
                }
                var CellButton = new TVclCellCF();//Creamos celda
                CellButton.Value = "";//Value celda
                CellButton.IndexColumn = colspan;//Índice de columna
                CellButton.IndexRow = z;//Índice de fila
                CellButton.Visible = OpcionVisibleButton;
                CellButton.IndexRowTreeGrid = IndexTreeGrid;
                var objetos = { "Row": NewRow2, "Grilla": grilla };
                _this.dibujarCellButton(true, NewRow2, CellButton, objetos);
                NewRow2.IndexTreeGrid = IndexTreeGrid;
                NewRow2.onclick = function (NewRow) {
                    _this.Grilla_rowclick_Tree(NewRow);
                }
                _this.RowGroup[val].CantidadHijos = _this.RowGroup[val].CantidadHijos + 1;//Aumentamos +1 la cantidad de hijas de la fila padre
                _this.RowGroup[val].RowHijos.push(NewRow2);
                NewRow2.RowPadre = _this.RowGroup[val];
                grilla.AddRowAfter(2, NewRow2, _this.RowGroup[val], null);
                NewRow2.IndexTreeGrid = IndexTreeGrid;
                 NewRow2.IndexTreeGrid = IndexTreeGrid;
                NewRow2.Index = $(NewRow2).index();
                NewRow2.IndexDataSet = IndiceReal;              
                grilla.IndexRow = NewRow2.Index;
                _this.rowSelecionado = NewRow2;
            }
        }
        else if (Status == SysCfg.MemTable.Properties.TStatus.Update) {
             //Si se actulizó un nuevo record, entonces validamos si creamos una agrupación o actualizamos el nuevo item a una ya existente. Pueda darse el caso que al actualizar un dato del item este forme parte de otra agrupación
            var z = DataSetIndex;
            var ImgGruop = new Array();
            ImgGruop[true] = "";
            ImgGruop[false] = "none";
            var colspan = _this.TVclGridCFSource.Columns.length;
            var OpcionVisibleButton = _this.TVclGridCFSource.Columns[colspan - 1].Visible;
            var OpcionVisibleCheck = _this.TVclGridCFSource.Columns[0].Visible;
            var grilla = _this.GridTreeGridControl;
            var IndexFila = Row.Index;
            _this.rowSelecionado = null;
            var item = _this.ItemTreePanel[z];
            var ruta = "";
            var val = "";
            var i = _this.contadorIndiceRow;
            var IndexTreeGrid = z;
            for (var m = 0; m < _this.nivelesTree.length; m++) {//Recorremos la lista que tiene la cadena de los combos selecionados.
                if (m + 1 == _this.nivelesTree.length) {//Si estamos en el último item del array (nivelesTree)
                    ruta = ruta + _this.nameColumns[_this.nivelesTree[m]] + ":" + item[_this.nivelesTree[m]][0] + "";//formamos cadena de la ruta
                } else {
                    ruta = ruta + _this.nameColumns[_this.nivelesTree[m]] + ":" + item[_this.nivelesTree[m]][0] + "\\";//formamos cadena de la ruta
                }
            }

            val = ruta;
            var subRuta = "";//SubRutas: por ejemplo la ruta ["País:Ciudad"], entonces tenemos con sub rutas["Perú:Lima","Perú:Trujillo"]
            var espacios = 0;//Espacios que correr el texto a la derecha.
            var isNewGrup = false;
            if (typeof (_this.RowGroup[val]) == "undefined") {
                isNewGrup = true;
                var words = ruta.split('\\');//separamos el texto de la ruta
                subRuta = subRuta + words[0];//Agregamos el primer texto de la separación.
                for (var Counter = 0; Counter < words.length; Counter++) {//Recorremos el array de la palabras separadas
                    var subRutaPadre = subRuta;//SubRutas: por ejemplo la ruta ["País:Ciudad"], entonces tenemos con sub rutas["Perú:Lima","Perú:Trujillo"]
                    if (Counter > 0) {
                        subRuta = subRuta + "\\" + words[Counter];//SubRutas: por ejemplo la ruta ["País:Ciudad"], entonces tenemos con sub rutas["Perú:Lima","Perú:Trujillo"]
                        espacios = espacios + 20;//Agregamos espacios, esto sirve para correr el texto a la derecha.
                    }
                    var NewRow = new TVclRowCF();//Creamos nueva Row
                    if (typeof (_this.RowGroup[subRuta]) == "undefined") {
                        var Cell = new TVclCellCF();//Creamos celda
                        Cell._Value = words[Counter];
                        Cell.IndexColumn = 0;//Índice de columna
                        Cell.IndexRow = i;//Índice de fila
                        Cell.IndexRowTreeGrid = IndexTreeGrid;
                        NewRow.AddCell(Cell);
                        _this.RowGroup[subRuta] = NewRow;//SubRutas: por ejemplo la ruta ["País:Ciudad"], entonces tenemos con sub rutas["Perú:Lima","Perú:Trujillo"]
                        NewRow.PaddingGroup = espacios;//Agregamos espacios, esto sirve para correr el texto a la derecha.
                        NewRow.This.style.backgroundColor = "#3e94ba";//Bacukground de fila que es agrupación
                        NewRow.onclick = function (NewRow) {
                        }
                        if (Counter > 0) {
                            NewRow.Visible = _this.open;
                            var cantidadHijos = (_this.RowGroup[subRutaPadre].CantidadHijos + 1);//Aumentamos +1 la cantidad de hijas de la fila padre
                            _this.RowGroup[subRutaPadre].RowHijos.push(NewRow);
                            _this.RowGroup[subRutaPadre].CantidadHijos = cantidadHijos;//Actualizamos cantidad de hijos
                            NewRow._ImgGruopDown.style.display = ImgGruop[_this.open];//Mostramos imagen de flecha("Expandida") si por defecto la agrupación esta abierta
                            NewRow._ImgGruopRigth.style.display = ImgGruop[!_this.open];//Ocultamos imagen de flecha("Collapse") si por defecto la agrupación esta cerrada
                            grilla.AddRowAfter(1, NewRow, _this.RowGroup[subRutaPadre], colspan);//Agregamos fila sub-padre("Ruta principal"), por ejemplo tenemos la rutas["País:Perú",["ciudad:lima","ciudad:trujillo"]] y ["Pais:Colombia",["ciudad:cali","ciudad:bogotá"]] las sub-rutas l son ["ciudad:lima"],["ciudad:trujillo"],["ciudad:cali"] y ["ciudad:bogotá"]
                            NewRow.RowPadre = _this.RowGroup[subRutaPadre];
                        } else {
                            grilla.AddRowAfter(0, NewRow, null, colspan);//Agregamos fila padre("Ruta principal"), por ejemplo tenemos la rutas["País:Perú","ciudad:lima"] y ["Pais:Colombia","ciudad:cali"] la ruta padre pincipal son ["País:Perú"] y ["País:Colombia"]
                            NewRow._ImgGruopDown.style.display = 'none';//Ocultamos imagen de flecha("Expandida")
                            NewRow._ImgGruopRigth.style.display = ''; //Mostramos imagen de flecha("Collapse")
                        }
                    }
                }
            }

            var NewRow2 = new TVclRowCF();//Creamos nueva Row
            NewRow2.Visible = _this.open;
            if (!_this.open) {
                NewRow2.Visible = _this.RowGroup[val].Visible;
            }
            var Cell0 = new TVclCellCF();//Creamos celda
            Cell0.Value = true;//Value celda
            Cell0.IndexColumn = 0;//Índice de columna
            Cell0.IndexRow = z;//Índice de fila
            Cell0.Visible = OpcionVisibleCheck;
            Cell0.IndexRowTreeGrid = IndexTreeGrid;
            NewRow2.AddCell(Cell0);
            for (var j = 1; j < _this.TVclGridCFSource.Columns.length - 1; j++) {//Recorremos Columnas del Grid cfview
                var Cell = new TVclCellCF();//Creamos celda
                Cell.Value = item[j - 1][0];//Value celda
                Cell.IndexColumn = j;//Índice de columna
                Cell.IndexRow = z;
                Cell.IndexRowTreeGrid = IndexTreeGrid;
                NewRow2.AddCell(Cell);
            }
            var CellButton = new TVclCellCF();//Creamos celda
            CellButton.Value = "";//Value celda
            CellButton.IndexColumn = colspan;//Índice de columna
            CellButton.IndexRow = z;//Índice de fila
            CellButton.Visible = OpcionVisibleButton;
            CellButton.IndexRowTreeGrid = IndexTreeGrid;
            var objetos = { "Row": NewRow2, "Grilla": grilla };
            _this.dibujarCellButton(true, NewRow2, CellButton, objetos);
            NewRow2.IndexTreeGrid = IndexTreeGrid;
            NewRow2.onclick = function (NewRow) {
                _this.Grilla_rowclick_Tree(NewRow);
            }
            _this.RowGroup[val].CantidadHijos = _this.RowGroup[val].CantidadHijos + 1;//Aumentamos +1 la cantidad de hijas de la fila padre
            _this.RowGroup[val].RowHijos.push(NewRow2);

            if (isNewGrup) {
                grilla.RemoveRow(Row.Index);
                NewRow2.RowPadre = _this.RowGroup[val];
                grilla.AddRowAfter(2, NewRow2, _this.RowGroup[val], null);
            } else {
                grilla.UpdatesRow(NewRow2, Row);

            }

            NewRow2.IndexTreeGrid = IndexTreeGrid;
            _this.ItemTreePanel[z][_this.sizeColumnTVclGridCFSource + 1] = IndexTreeGrid;
            NewRow2.IndexTreeGrid = IndexTreeGrid;
            NewRow2.Index = $(NewRow2).index();
            grilla.IndexRow = NewRow2.Index;
            _this.rowSelecionado = NewRow2;
        }
        else if (Status == SysCfg.MemTable.Properties.TStatus.Delete) {
         //Si se eliminó un nuevo record, entonces eliminamos el item y validamos si tenemos que eliminar la agrupación
            var z = DataSetIndex;
            var grilla = _this.GridTreeGridControl;
            var IndexFila = Row.Index;
            grilla.RemoveRow(Row.Index);
            var IndexTreeGrid = z;
            var ruta = "";
            for (var m = 0; m < _this.nivelesTree.length; m++) {//Recorremos la lista que tiene la cadena de los combos selecionados.
                if (m + 1 == _this.nivelesTree.length) {//Si estamos en el último item del array (nivelesTree)
                    ruta = ruta + _this.nameColumns[_this.nivelesTree[m]] + ":" + Row.Cells[_this.nivelesTree[m] + 1]._Value + "";//formamos cadena de la ruta
                } else {
                    ruta = ruta + _this.nameColumns[_this.nivelesTree[m]] + ":" + Row.Cells[_this.nivelesTree[m] + 1]._Value + "\\";//formamos cadena de la ruta
                }
            }
            if (typeof (_this.RowGroup[ruta]) != "undefined") {
                _this.RowGroup[ruta].CantidadHijos = _this.RowGroup[ruta].CantidadHijos - 1;//Disminuimos en -1 la cantidad de hijas de la fila padre
            }
            _this.rowSelecionado = null;

            for (var m = 0; m < grilla.Rows.length; m++) {
                if (grilla.Rows[m].RowHijos.length == 0 && grilla.Rows[m].IndexTreeGrid > IndexTreeGrid) {
                    grilla.Rows[m].IndexTreeGrid = grilla.Rows[m].IndexTreeGrid - 1;
                }
            }
        }
    }
}
Componet.GridCF.TVclTreeGridControl.prototype.inTreeGrid = false;/*Indica si estamos visualizando o no la agrupación.*/
Componet.GridCF.TVclTreeGridControl.prototype.GridTreeGridControl = null;/*Grid del TreeGridControl*/
Componet.GridCF.TVclTreeGridControl.prototype.TVclGridCFSource = null;/*Grid del CFview*/
Componet.GridCF.TVclTreeGridControl.prototype.GridCFToolbar = null;/*GridCFToolbar*/
/*Función para crear grid en la agrupación*/
Componet.GridCF.TVclTreeGridControl.prototype.createTableGridRow = function () {
    var _this = this.TParent();
    var Grilla = null;
    if (this.GridTreeGridControl == null) {//Validamos que no exista grilla
        $(_this.ConTreeControl.Column[0].This).html("");
        var Grilla = new TVclGridCF(_this.ConTreeControl.Column[0].This, "", "");//Creamos grilla;
        //Grilla.ResponsiveCont.style.height = "300px";
        //Grilla.ResponsiveCont.style.overflow = "scroll";
        Grilla.IsTree = true;//Grilla de tipo Tree
        this.GridTreeGridControl = Grilla;//Seteamos grilla
        //Colocamos los estilos del grid cfview
        _this.GridTreeGridControl.This.style.fontSize = _this.GridCFView.GridCFToolbar.ItemGroupBarTamanio.Text + "px";
        _this.GridTreeGridControl.This.style.fontFamily = _this.TVclGridCFSource.This.style.fontFamily;
        _this.GridTreeGridControl.This.style.fontWeight = _this.TVclGridCFSource.This.style.fontWeight;
        _this.GridTreeGridControl.This.style.fontStyle = _this.TVclGridCFSource.This.style.fontStyle;
        _this.GridTreeGridControl.This.style.textAlign = _this.TVclGridCFSource.This.style.textAlign;
        _this.GridTreeGridControl.Header.style.color = _this.TVclGridCFSource.Header.style.color;
        _this.GridTreeGridControl.Body.style.color = _this.TVclGridCFSource.Body.style.color;
        _this.GridTreeGridControl.OnImgGruopOpen = function (route, Row) {
            if (_this.GridCFView != null) {
                _this.GridCFView.openTreeGridPaginationFilterItemTreePanel(_this.ItemGroupPanel[route].ItemTreePanel);
            }
        }
        var parentRowHeader = _this.GridTreeGridControl.RowHeader.parentNode;
        parentRowHeader.removeChild(_this.GridTreeGridControl.RowHeader);
        //Creamos columnas con las priedades del grid cfview
        var ColMemCkeck = new TVclColumnCF();
        ColMemCkeck.Name = "OptionButtonCkeck";
        ColMemCkeck.IsOptionCheck = true;
        ColMemCkeck.Caption = "";
        ColMemCkeck.Index = 0;
        ColMemCkeck.EnabledEditor = true;
        ColMemCkeck.DataType = SysCfg.DB.Properties.TDataType.Boolean;
        ColMemCkeck.Visible = _this.TVclGridCFSource.Columns[0].Visible;
        ColMemCkeck.ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.None;
        Grilla.IndexCol = 0;
        Grilla.AddColumn(ColMemCkeck);
        Grilla.FileSRVEvent = _this.TVclGridCFSource.FileSRVEvent;
        
        for (var i = 1; i < _this.TVclGridCFSource.Columns.length - 1; i++) {//Recorremos Columnas del Grid cfview
            /*Creamos columnas con las priedades del grid cfview*/
            var Col = new TVclColumnCF();
            Col.Name = _this.TVclGridCFSource.Columns[i].Name;
            Col.Index = i;
            Col.EnabledEditor = _this.TVclGridCFSource.Columns[i].IsOptionCheck;
            Col.DataType = _this.TVclGridCFSource.Columns[i].DataType;
            Col._InsertActiveEditor = _this.TVclGridCFSource.Columns[i].InsertActiveEditor;
            Col._UpdateActiveEditor = _this.TVclGridCFSource.Columns[i].UpdateActiveEditor;
            Col.IndexDataSourceDefault = _this.TVclGridCFSource.Columns[i].IndexDataSourceDefault;
            Col.CaptionOrder = _this.TVclGridCFSource.Columns[i].Caption;
            //Col.Caption = _this.TVclGridCFSource.Columns[i].Caption;
            Col.ColumnStyle = _this.TVclGridCFSource.Columns[i].ColumnStyle;
            Col.DataEventControl = _this.TVclGridCFSource.Columns[i].DataEventControl;
            Col.onEventControl = _this.TVclGridCFSource.Columns[i].onEventControl;
            Col.onEventFilter = _this.TVclGridCFSource.Columns[i].onEventFilter;
            Col.IsOptionCheck = _this.TVclGridCFSource.Columns[i].IsOptionCheck;
            Col.CustomDrawCell = _this.TVclGridCFSource.Columns[i].CustomDrawCell;
            Col.onEventFilterColumn = _this.TVclGridCFSource.Columns[i].onEventFilterColumn;
            Col.ModalEvent = _this.TVclGridCFSource.Columns[i].ModalEvent;
            Col.DataBuildCustomCell = _this.TVclGridCFSource.Columns[i].DataBuildCustomCell;
            Col.Visible = _this.TVclGridCFSource.Columns[i]._Visible;
            Col._VisibleVertical = _this.TVclGridCFSource.Columns[i]._VisibleVertical;
            Col.Visible = _this.TVclGridCFSource.Columns[i]._Visible;
            Col._VisibleVertical = _this.TVclGridCFSource.Columns[i]._VisibleVertical;
            Col._Visible_IsShowValue = _this.TVclGridCFSource.Columns[i]._Visible_IsShowValue;
            Col._VisibleVertical_IsShowValue = _this.TVclGridCFSource.Columns[i]._VisibleVertical_IsShowValue;
            Col._FileType = _this.TVclGridCFSource.Columns[i]._FileType;
            Col.onclick = _this.TVclGridCFSource.Columns[i].onclick;
            Grilla.AddColumn(Col);
            if (_this.VclDrag != null) {
                _this.VclDrag.addDragable(Col, Col.This);
                _this.VclDrag.addEventContendor(Col, Col.This, function (Content, Component, Event) {
                    var VclColumConten = Content;
                    var VclColumn = Component;

                    if (VclColumConten.Index != VclColumn.Index) {

                        _this.MoveColumns(VclColumConten.Index, VclColumn.Index);
                        _this.DataSet.Cancel();
                    }


                });
            }
        }

        //creamos columa para los botones.
        var ColMemButton = new TVclColumnCF();
        ColMemButton.Name = "optionButton";
        ColMemButton.IsOption = true;
        ColMemButton.CaptionButton = "<br>OPTIONS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        //ColMemButton.CaptionButton = "Options";
        ColMemButton.Index = _this.sizeColumnTVclGridCFSource;
        ColMemButton.EnabledEditor = true;
        ColMemButton.Visible = true;
        ColMemButton.ToolBarButton = _this.TVclGridCFSource.Columns[_this.TVclGridCFSource.Columns.length - 1].ToolBarButton;
        Grilla.IndexCol = _this.sizeColumnTVclGridCFSource;
        Grilla.AddColumn(ColMemButton);
        //Grilla.EnableEditOptionsColumn = _this.TVclGridCFSource.EnableEditOptionsColumn;
        Grilla.EnableEditOptionsColumn = false;
        parentRowHeader.appendChild(_this.GridTreeGridControl.RowHeader);
    }
    else {
        this.GridTreeGridControl.ClearAllRows();
        return this.GridTreeGridControl;
    }
    return Grilla;
}
/*Función para contruir la ruta de un item*/
Componet.GridCF.TVclTreeGridControl.prototype.buildRutaAgrupacion = function (item) {
    var _this = this.TParent();
    var ruta = "";
    for (var m = 0; m < _this.nivelesTree.length; m++) {//Recorremos el array creado por los combos selecionados
        if (m + 1 == _this.nivelesTree.length) {//Si estamos en el último item del array (nivelesTree)
            ruta = ruta + _this.nameColumns[_this.nivelesTree[m]] + ":" + item[_this.nivelesTree[m]][0] + "";//formamos cadena de la ruta
        } else {
            ruta = ruta + _this.nameColumns[_this.nivelesTree[m]] + ":" + item[_this.nivelesTree[m]][0] + "\\";//formamos cadena de la ruta
        }
    }
    return ruta;//Devuelve ruta con el formato: "columnaA:valorA//columnaB:valorB"
}
/*Función para crear la agrupación*/
Componet.GridCF.TVclTreeGridControl.prototype.groupByGridTabla = function (miarray, prop) {
    var _this = this.TParent();
    this.RowGroup = new Array();//Array con las filas agrupadas.
    this.ListRouteGroup = new Array();//Array rutas agrupadas.
    this.ItemGroupPanel = new Array();//ItemTreePanel
    var ImgGruop = new Array();//Variable para manejar los true o false
    ImgGruop[true] = "";
    ImgGruop[false] = "none";
    var ListCaptionNameColumn = new Array();//Lista de texto de la columnas, nos sirve para formar de una manera mas rápida rutas.
    for (var i = 0; i < _this.TVclGridCFSource.Columns.length; i++) {//Recorremos Columnas del Grid cfview
        var column = _this.TVclGridCFSource.Columns[i];//Columna
        if (!(column.IsOptionCheck || column.IsOption)) {//Validamos si es la primerra columna(Check) o la columna de los botones
            var name = column.Name;
            var caption = column.Caption;
            ListCaptionNameColumn[name] = caption;//Agregamos los caption a la lista
        }
    }
    var colspan = _this.TVclGridCFSource.Columns.length;//Número de filas que vamos a unir para formar la fila de agrupación
    var OpcionVisibleButton = _this.TVclGridCFSource.Columns[colspan - 1].Visible;//Obtnemos el valor visible de la última columna.
    var OpcionVisibleCheck = _this.TVclGridCFSource.Columns[0].Visible;//Obtnemos el valor visible de la primera columna columna.
    var grilla = _this.createTableGridRow();//Creamos la grilla del TreeGrid
    var parentBody = grilla.Body.parentNode;//Obtnemos el parent del body
    parentBody.removeChild(grilla.Body);//Removemos el body
    for (var z = 0; z < miarray.length; z++) {//recorremos array
        var item = miarray[z];//Item 
        var ruta = "";//ruta de agrupación
        var val = "";//ruta de agrupación
        var i = _this.contadorIndiceRow;//Posición de fila según se este dibujando.
        var IndexTreeGrid = z;//Indice en el array
        for (var m = 0; m < _this.nivelesTree.length; m++) {//Recorremos la lista que tiene la cadena de los combos selecionados.
            if (m + 1 == _this.nivelesTree.length) {//Si estamos en el último item del array (nivelesTree)
                ruta = ruta + ListCaptionNameColumn[_this.nameColumns[_this.nivelesTree[m]]] + ":" + item[_this.nivelesTree[m]][0] + "";//formamos cadena de la ruta
            } else {
                ruta = ruta + ListCaptionNameColumn[_this.nameColumns[_this.nivelesTree[m]]] + ":" + item[_this.nivelesTree[m]][0] + "\\";//formamos cadena de la ruta
            }          
        }
        val = ruta;//seteamos valor de ruta
        _this.contadorIndiceRow++;
        var rowAInsertar = null;
        var subRuta = "";//SubRutas: por ejemplo la ruta ["País:Ciudad"], entonces tenemos con sub rutas["Perú:Lima","Perú:Trujillo"]
        var espacios = 0;//Espacios que correr el texto a la derecha.
        if (typeof (_this.RowGroup[val]) == "undefined") {//Validamos si la ruta no existe en la lista de agrupaciones.Por ejemplo: tenemos la ruta "Perú:Lima" solo se debe formar una vez
            _this.ListRouteGroup.push(val);//Asignamos ruta al array que guarda todas las rutas
            var words = ruta.split('\\');//separamos el texto de la ruta
            subRuta = subRuta + words[0];//Agregamos el primer texto de la separación.
            for (var Counter = 0; Counter < words.length; Counter++) {//Recorremos el array de la palabras separadas
                var subRutaPadre = subRuta;//SubRutas: por ejemplo la ruta ["País:Ciudad"], entonces tenemos con sub rutas["Perú:Lima","Perú:Trujillo"]
                if (Counter > 0) {
                    subRuta = subRuta + "\\" + words[Counter];//Creamos sub rutas, por ejemplo la ruta ["País:Ciudad"], entonces tenemos con sub rutas["Perú:Lima","Perú:Trujillo"]
                    espacios = espacios + 20;//Agregamos espacios, esto sirve para correr el texto a la derecha.
                }
                var NewRow = new TVclRowCF();//Creamos nueva Row
                /*Función al abrir agrupación*/
                NewRow.OnEventGruopDown = function (collapse, RutaGruopDown, NewRowPadre) {
                    var Row = _this.GridTreeGridControl.FocusedRowHandle;//Fila actual
                    //Antes de abrir agrupación verificamos si en una agrupación esta abierta y se esta insertando o editando, si se da este caso debemos cancelar la operación insert o update
                    if (Row != null && (_this.DataSet.Status == SysCfg.MemTable.Properties.TStatus.Apped || _this.DataSet.Status == SysCfg.MemTable.Properties.TStatus.Update)) {//Si estamos insertando o eliminado
                        if (_this.DataSet.Status == SysCfg.MemTable.Properties.TStatus.Apped) {//Si estamos insertando
                            var indexEliminado = Row.Index;//Index que eliminaremos
                            _this.GridTreeGridControl.RemoveRow(indexEliminado);//Removemos fila
                            _this.GridTreeGridControl.IndexRow = indexEliminado;//actualizamos índice de fila
                            _this.rowSelecionado = _this.GridTreeGridControl._FocusedRowHandle;//índice de fila
                            _this.rowSelecionado.Index = indexEliminado;
                        }
                        _this.DataSet.Status = SysCfg.MemTable.Properties.TStatus.None;
                        _this.DataSet.Cancel();//Cancelamos operación.
                    }
                    if (_this.LastRutaGroup != null) {
                        $(_this.RowGroup[_this.LastRutaGroup.RutaGruopDown].PanelPagination).html("");
                        _this.RowGroup[_this.LastRutaGroup.RutaGruopDown]._ImgGruopDown.style.display = 'none';//Ocultamos imagen de flecha("Expandida")
                        _this.RowGroup[_this.LastRutaGroup.RutaGruopDown]._ImgGruopRigth.style.display = '';//Mostramos imagen de flecha("Collapse")
                        _this.RowGroup[_this.LastRutaGroup.RutaGruopDown].PanelPaginationGroup.style.display = "none";
                        _this.RowGroup[_this.LastRutaGroup.RutaGruopDown].VclStackPanelGrup.style.display = "";
                        var tempIndexRowStart = _this.LastRutaGroup.IndexRowStart;
                        for (var IndexRowStart = _this.LastRutaGroup.IndexRowStart; IndexRowStart <= _this.LastRutaGroup.IndexRowEnd; IndexRowStart++) {
                            _this.GridTreeGridControl.RemoveRow(tempIndexRowStart);
                        }
                        _this.LastRutaGroup = null;
                    }
                }
                if (typeof (_this.RowGroup[subRuta]) == "undefined") {
                    var Cell = new TVclCellCF();//Creamos celda
                    Cell._Value = words[Counter];//Valor de la celda con el nombre de la agrupación
                    Cell.IndexColumn = 0;//Índice de columna
                    Cell.IndexRow = i;//Índice de fila
                    Cell.IndexRowTreeGrid = IndexTreeGrid;
                    NewRow.AddCell(Cell);
                    _this.RowGroup[subRuta] = NewRow;//Agregamos a fila _this.RowGroup con el valo de la sub ruta, por ejemplo _this.RowGroup["País:Perú"]=NewRow
                    NewRow.PaddingGroup = espacios;//Agregamos espacios, esto sirve para correr el texto a la derecha.
                    NewRow.This.style.backgroundColor = "#3e94ba";//Bacukground de fila que es agrupación
                    NewRow.onclick = function (NewRow) {
                    }
                    if (Counter > 0) {
                        NewRow.Visible = _this.open;
                        var cantidadHijos = (_this.RowGroup[subRutaPadre].CantidadHijos + 1);//Aumentamos +1 la cantidad de hijas de la fila padre
                        _this.RowGroup[subRutaPadre].RowHijos.push(NewRow);
                        _this.RowGroup[subRutaPadre].CantidadHijos = cantidadHijos;//Actualizamos cantidad de hijos
                        NewRow._ImgGruopDown.style.display = ImgGruop[_this.open];//Mostramos imagen de flecha("Expandida") si por defecto la agrupación esta abierta
                        NewRow._ImgGruopRigth.style.display = ImgGruop[!_this.open];//Ocultamos imagen de flecha("Collapse") si por defecto la agrupación esta cerrada
                        grilla.AddRowAfter(1, NewRow, _this.RowGroup[subRutaPadre], colspan);//Agregamos fila sub-padre("Ruta principal"), por ejemplo tenemos la rutas["País:Perú",["ciudad:lima","ciudad:trujillo"]] y ["Pais:Colombia",["ciudad:cali","ciudad:bogotá"]] las sub-rutas l son ["ciudad:lima"],["ciudad:trujillo"],["ciudad:cali"] y ["ciudad:bogotá"]
                        NewRow.RowPadre = _this.RowGroup[subRutaPadre];
                    } else {
                        grilla.AddRowAfter(0, NewRow, null, colspan);//Agregamos fila padre("Ruta principal"), por ejemplo tenemos la rutas["País:Perú","ciudad:lima"] y ["Pais:Colombia","ciudad:cali"] la ruta padre pincipal son ["País:Perú"] y ["País:Colombia"]
                        NewRow._ImgGruopDown.style.display = 'none';//Ocultamos imagen de flecha("Expandida")
                        NewRow._ImgGruopRigth.style.display = '';//Mostramos imagen de flecha("Collapse")
                    }
                }
            }
            _this.ItemGroupPanel[val] = {
                "ItemTreePanel": new Array()
            }
            _this.RowGroup[val].RutaGruopDown = val;//Asignamos ruta de la agrupación en row
            /*Función al abrir agrupación*/
            _this.RowGroup[val].OnEventGruopDown = function (collapse, RutaGruopDown, NewRowPadre) {
                var Row = _this.GridTreeGridControl.FocusedRowHandle;
                if (Row != null && (_this.DataSet.Status == SysCfg.MemTable.Properties.TStatus.Apped || _this.DataSet.Status == SysCfg.MemTable.Properties.TStatus.Update)) {
                    if (_this.DataSet.Status == SysCfg.MemTable.Properties.TStatus.Apped) {
                        var indexEliminado = Row.Index;
                        _this.GridTreeGridControl.RemoveRow(indexEliminado);
                        _this.GridTreeGridControl.IndexRow = indexEliminado;
                        _this.rowSelecionado = _this.GridTreeGridControl._FocusedRowHandle;
                        _this.rowSelecionado.Index = indexEliminado;
                    }

                    _this.DataSet.Status = SysCfg.MemTable.Properties.TStatus.None;
                    //   _this.DataSet.Cancel();
                }
                var OpcionVisibleButton = _this.TVclGridCFSource.Columns[colspan - 1].Visible;
                var OpcionVisibleCheck = _this.TVclGridCFSource.Columns[0].Visible;
                NewRowPadre.setPaginationRow();
                if (collapse) {
                    if (_this.LastRutaGroup != null) {
                        if (typeof (_this.RowGroup[_this.LastRutaGroup.RutaGruopDown]) != "undefined") {
                            $(_this.RowGroup[_this.LastRutaGroup.RutaGruopDown].PanelPagination).html("");
                            _this.RowGroup[_this.LastRutaGroup.RutaGruopDown]._ImgGruopDown.style.display = 'none';//Ocultamos imagen de flecha("Expandida")
                            _this.RowGroup[_this.LastRutaGroup.RutaGruopDown]._ImgGruopRigth.style.display = '';//Mostramos imagen de flecha("Collapse")
                            _this.RowGroup[_this.LastRutaGroup.RutaGruopDown].PanelPaginationGroup.style.display = "none";
                            _this.RowGroup[_this.LastRutaGroup.RutaGruopDown].VclStackPanelGrup.style.display = "";
                            var tempIndexRowStart = _this.LastRutaGroup.IndexRowStart;
                            for (var IndexRowStart = _this.LastRutaGroup.IndexRowStart; IndexRowStart <= _this.LastRutaGroup.IndexRowEnd; IndexRowStart++) {
                                _this.GridTreeGridControl.RemoveRow(tempIndexRowStart);
                            }
                        }
                    }
                    _this.LastRutaGroup = {
                        RutaGruopDown: RutaGruopDown,
                        IndexRowStart: null,
                        IndexRowEnd: null,
                    }
                    $(NewRowPadre.PanelPagination).html("");
                    _this.VPagination = new Componet.GridCF.TGridCFView.TPagination(NewRowPadre.PanelPagination, true, 5);
                    _this.VPagination.Padding = NewRowPadre._PaddingGroup;
                    _this.loadDataSetRecordsPaginationFilter(NewRowPadre, _this.ItemGroupPanel[RutaGruopDown].ItemTreePanel);
                    return true;
                    var lastRow = null;
                    for (var y = 0; y < _this.ItemGroupPanel[RutaGruopDown].ItemTreePanel.length; y++) {
                        var item = _this.ItemGroupPanel[RutaGruopDown].ItemTreePanel[y];
                        var IndexTreeGrid = y;
                        var NewRow2 = new TVclRowCF();//Creamos nueva Row
                        NewRow2.Visible = true;
                        var Cell0 = new TVclCellCF();//Creamos celda
                        Cell0.Value = true;//Value celda
                        Cell0.IndexColumn = 0;//Índice de columna
                        Cell0.IndexRow = y;
                        Cell0.Visible = OpcionVisibleCheck;
                        Cell0.IndexRowTreeGrid = IndexTreeGrid;
                        NewRow2.AddCell(Cell0);
                        var IndiceReal = item[_this.sizeColumnTVclGridCFSource + 1];
                        for (var j = 1; j < _this.TVclGridCFSource.Columns.length - 1; j++) {//Recorremos Columnas del Grid cfview
                            var IndexDataSourceDefault = _this.TVclGridCFSource.Columns[j].IndexDataSourceDefault;
                            var Cell = new TVclCellCF();//Creamos celda
                            Cell.Value = _this.DataSet.Records[IndiceReal].Fields[IndexDataSourceDefault].Value;//Value celda
                            Cell.IndexColumn = j;//Índice de columna
                            Cell.IndexRow = y;//Índice de fila
                            Cell.IndexRowTreeGrid = IndexTreeGrid;
                            NewRow2.AddCell(Cell);
                        }

                        var CellButton = new TVclCellCF();//Creamos celda
                        CellButton.Value = "";//Value celda
                        CellButton.IndexColumn = colspan;//Índice de columna
                        CellButton.IndexRow = y;//Índice de fila
                        CellButton.Visible = OpcionVisibleButton;
                        CellButton.IndexRowTreeGrid = IndexTreeGrid;
                        var objetos = { "Row": NewRow2, "Grilla": grilla };
                        _this.dibujarCellButton(true, NewRow2, CellButton, objetos);
                        NewRow2.IndexTreeGrid = IndexTreeGrid;
                        NewRow2.onclick = function (NewRow) {

                            _this.Grilla_rowclick_Tree(NewRow);

                        }
                        NewRowPadre.CantidadHijos = NewRowPadre.CantidadHijos + 1;//Aumentamos +1 la cantidad de hijas de la fila padre
                        NewRowPadre.RowHijos.push(NewRow2);
                        NewRow2.RowPadre = NewRowPadre;
                        if (lastRow == null) {
                            grilla.AddRowAfter(2, NewRow2, NewRowPadre, null);
                            lastRow = NewRow2;
                        } else {
                            grilla.AddRowAfter(2, NewRow2, lastRow, null);
                            lastRow = NewRow2;
                        }
                        NewRow2.Index = $(NewRow2.This).index();
                        NewRow2.IndexDataSet = IndiceReal;
                        NewRow2.IndexTreeGrid = y;
                        if (y == 0) {
                            _this.LastRutaGroup.IndexRowStart = $(NewRow2.This).index();
                            _this.GridTreeGridControl.IndexRow = $(NewRow2.This).index();
                        }
                        if (y == _this.ItemGroupPanel[RutaGruopDown].ItemTreePanel.length - 1) {
                            _this.LastRutaGroup.IndexRowEnd = _this.LastRutaGroup.IndexRowStart + (_this.ItemGroupPanel[RutaGruopDown].ItemTreePanel.length - 1);
                        }
                    }
                }
                else {
                    _this.GridCFView.Information.VclGridCFInfo.ClearAll();
                    if (_this.LastRutaGroup != null) {
                        $(_this.RowGroup[_this.LastRutaGroup.RutaGruopDown].PanelPagination).html("");
                        _this.RowGroup[_this.LastRutaGroup.RutaGruopDown]._ImgGruopDown.style.display = 'none';//Ocultamos imagen de flecha("Expandida")
                        _this.RowGroup[_this.LastRutaGroup.RutaGruopDown]._ImgGruopRigth.style.display = '';//Mostramos imagen de flecha("Collapse")
                        _this.RowGroup[_this.LastRutaGroup.RutaGruopDown].PanelPaginationGroup.style.display = "none";
                        _this.RowGroup[_this.LastRutaGroup.RutaGruopDown].VclStackPanelGrup.style.display = "";
                        var tempIndexRowStart = _this.LastRutaGroup.IndexRowStart;
                        for (var IndexRowStart = _this.LastRutaGroup.IndexRowStart; IndexRowStart <= _this.LastRutaGroup.IndexRowEnd; IndexRowStart++) {
                            _this.GridTreeGridControl.RemoveRow(tempIndexRowStart);
                        }
                        _this.LastRutaGroup = null;
                    }
                }
            }

        }
        //Formamos array
        var temArray = new Array();
        for (var xy = 0; xy < miarray[z].length; xy++) {
            temArray.push(miarray[z][xy]);
        }
        _this.ItemGroupPanel[val].ItemTreePanel.push(temArray);//Agregamos ItemTreePanel al grupo de ItemTreePanel.
        _this.RowGroup[val].CantidadHijos = _this.RowGroup[val].CantidadHijos + 1;//Aumentamos +1 la cantidad de hijas de la fila padre
    }
    parentBody.appendChild(grilla.Body);//Agregamos body a la tabla
    ListCaptionNameColumn = new Array();//Setamos array
}
/*Función mover columnas en el TreeGrid*/
Componet.GridCF.TVclTreeGridControl.prototype.MoveColumns = function (posicionA, posicionB) {
    var _this = this.TParent();
    _this.TVclGridCFSource._AutoGenerateColumns = false;
    _this.TVclGridCFSource.MoveColumns(posicionA, posicionB);
    _this.GridTreeGridControl.MoveColumns(posicionA, posicionB);
    for (var i = 0; i < _this.ItemTreePanel.length; i++) {
        var temp = _this.ItemTreePanel[i][posicionA - 1];
        _this.ItemTreePanel[i][posicionA - 1] = _this.ItemTreePanel[i][posicionB - 1];
        _this.ItemTreePanel[i][posicionB - 1] = temp;
    }
    var tempName = _this.nameColumns[posicionA - 1];
    _this.nameColumns[posicionA - 1] = _this.nameColumns[posicionB - 1];
    _this.nameColumns[posicionB - 1] = tempName;
}
/*Acción que se produce en el OnEventGoTo de TPagination.*/
Componet.GridCF.TVclTreeGridControl.prototype.isEventChangePaginationScroll = true;
/*Función crear las filas del ItemTreePanel de la agrupación abierta*/
Componet.GridCF.TVclTreeGridControl.prototype.loadDataSetRecordsPaginationFilter = function (NewRowPadre, ItemTreePanel) {
    var _this = this.TParent();
    try {
        _this.VPagination.OnEventGoTo = null;
        var UnDataSet = _this.DataSet;
        _this.VPagination.NumeroFilas = ItemTreePanel.length;
        _this.VPagination.IndexPage = 1;
        _this.filasMostradas = _this.VPagination.FilasMostradas;
        _this.VPagination.Refresch();
        _this.VPagination.OnEventGoTo = function (indicePage) {

            if (_this.LastRutaGroup != null) {
                _this.RowGroup[_this.LastRutaGroup.RutaGruopDown]._ImgGruopDown.style.display = 'none';//Ocultamos imagen de flecha("Expandida")
                _this.RowGroup[_this.LastRutaGroup.RutaGruopDown]._ImgGruopRigth.style.display = '';//Mostramos imagen de flecha("Collapse")
                var tempIndexRowStart = _this.LastRutaGroup.IndexRowStart;
                if (_this.LastRutaGroup.IndexRowStart != null) {
                    for (var IndexRowStart = _this.LastRutaGroup.IndexRowStart; IndexRowStart <= _this.LastRutaGroup.IndexRowEnd; IndexRowStart++) {
                        _this.GridTreeGridControl.RemoveRow(tempIndexRowStart);
                    }
                }

            }

            _this.filasMostradas = _this.VPagination.FilasMostradas;
            _this.DataSet.Status = SysCfg.MemTable.Properties.TStatus.None;
            var indiceActual = indicePage - 1;
            _this.GridTreeGridControl._FocusedRowHandle = null;
            var lastRow = null;
            var firstRow = null;
            var ItemTreePanel = _this.ItemGroupPanel[_this.LastRutaGroup.RutaGruopDown].ItemTreePanel;
            NewRowPadre.CantidadHijos = ItemTreePanel.length;//Número de hijos igual al tamaño del itemTreePanel de la ruta
            NewRowPadre.RowHijos = new Array();
            if (ItemTreePanel.length == 0) {
                _this.LastRutaGroup = null
            }
            for (var e = (_this.filasMostradas * indiceActual), indexRow = 0, tDataSet = ((indiceActual + 1) * _this.filasMostradas), cantidadRecord = ItemTreePanel.length; e < tDataSet && e < cantidadRecord; e++ , indexRow++) {
                var IndiceReal = ItemTreePanel[e][_this.sizeColumnTVclGridCFSource + 1];
                var NewRow = new TVclRowCF(); //CREA UN ROW PARA AGREGAR AL GRIDCONTROL 
                var CellCheck = new TVclCellCF(); //CREA UNA CELDA PARA QUE SE AGREGA AL ROW
                CellCheck.Value = _this.SetSelectedRows;//Value celda
                CellCheck.IndexColumn = ContX; // ASIGNA O EXTRAE EL INDEX DE LA COLUMNA 
                CellCheck.IndexRow = e; // ASIGNA O EXTRAE EL INDEX DEL ROW 
                NewRow.AddCell(CellCheck); // AGREGA O EXTRAE LA CELDA AL ROW
                for (var ContX = 1, t = _this.GridTreeGridControl.Columns.length - 1; ContX < t; ContX++) {
                    var IndexDataSourceDefault = _this.GridTreeGridControl.Columns[ContX].IndexDataSourceDefault;
                    var Cell0 = new TVclCellCF();//Creamos celda
                    Cell0.CellColor = "";//Value color celda
                    Cell0.Value = _this.DataSet.Records[IndiceReal].Fields[IndexDataSourceDefault].Value; // ASIGNA O EXTRAE EL VALOR QUE SE MOSTRARA EN LA CELDA DEL GRIDCONTROL 
                    Cell0.IndexColumn = ContX; // ASIGNA O EXTRAE EL INDEX DE LA COLUMNA 
                    Cell0.IndexRow = e; // ASIGNA O EXTRAE EL INDEX DEL ROW 
                    NewRow.AddCell(Cell0); // AGREGA O EXTRAE LA CELDA AL ROW
                }
                //**Agrego celda para botones
                var CellButton = new TVclCellCF();//Creamos celda
                CellButton.Value = "";
                CellButton.IndexColumn = _this.DataSet.FieldCount;//Índice de columna
                CellButton.IndexRow = e;//Índice de fila
                var objetos = { "Row": NewRow, "Grilla": _this.GridTreeGridControl };
                _this.dibujarCellButton(true, NewRow, CellButton, objetos);
                NewRow.AddCell(CellButton);
                NewRow.onclick = function (NewRow) { // ASIGNA UN EVENTO CUANDO SE HACE CLICK EN EL ROW DEL GRIDCONTROL

                    _this.Grilla_rowclick_Tree(NewRow);
                }
                NewRowPadre.RowHijos.push(NewRow);
                NewRow.RowPadre = NewRowPadre;
                if (lastRow == null) {
                    _this.GridTreeGridControl.AddRowAfter(2, NewRow, NewRowPadre, null);
                    lastRow = NewRow;
                } else {
                    _this.GridTreeGridControl.AddRowAfter(2, NewRow, lastRow, null);
                    lastRow = NewRow;
                }
                NewRow.Index = $(NewRow.This).index();
                NewRow.IndexDataSet = IndiceReal;
                NewRow.IndexTreeGrid = e;
                if (indexRow == 0) {
                    firstRow = NewRow;
                    _this.LastRutaGroup.IndexRowStart = $(NewRow.This).index();
                    _this.GridTreeGridControl.IndexRow = $(NewRow.This).index();
                }
                if (e == ItemTreePanel.length - 1 || _this.filasMostradas - 1 == indexRow) {
                    _this.LastRutaGroup.IndexRowEnd = $(NewRow.This).index();
                }
            }
            _this.GridTreeGridControl.ContracAllColumns();
            if (firstRow != null) {
                _this.Grilla_rowclick_Tree(firstRow);
                if (_this.isEventChangePaginationScroll) {
                    _this.GridTreeGridControl.IndexRow = $(firstRow.This).index();
                    if (_this.GridTreeGridControl.FocusedRowHandle != null) {
                        _this.IndexRowGridPorScroll = _this.GridTreeGridControl.FocusedRowHandle.Index;
                    }
                }
                else {
                    if (_this.GridTreeGridControl.FocusedRowHandle != null) {
                        if (lastRow != null) {
                            _this.isEventChangePaginationScroll = true;
                            _this.GridTreeGridControl.IndexRow = $(lastRow.This).index();
                            _this.IndexRowGridPorScroll = _this.GridTreeGridControl.FocusedRowHandle.Index;
                        }

                    }
                }
            }


        }
        var columnaDeIndexDataSorce = _this.sizeColumnTVclGridCFSource + 1;
        if (ItemTreePanel.length > 0) {
            var lastRow = null;
            var firstRow = null;
            for (var e = 0, tDataSet = _this.filasMostradas, cantidadRecord = ItemTreePanel.length; e < tDataSet && e < cantidadRecord; e++) {
                var IndiceReal = ItemTreePanel[e][_this.sizeColumnTVclGridCFSource + 1];
                var NewRow = new TVclRowCF(); //CREA UN ROW PARA AGREGAR AL GRIDCONTROL 
                var CellCheck = new TVclCellCF(); //CREA UNA CELDA PARA QUE SE AGREGA AL ROW
                CellCheck.Value = _this.SetSelectedRows;//Value celda
                CellCheck.IndexColumn = ContX; // ASIGNA O EXTRAE EL INDEX DE LA COLUMNA 
                CellCheck.IndexRow = e; // ASIGNA O EXTRAE EL INDEX DEL ROW 
                NewRow.AddCell(CellCheck); // AGREGA O EXTRAE LA CELDA AL ROW
                NewRowPadre.CantidadHijos = ItemTreePanel.length;//Número de hijos igual al tamaño del itemTreePanel de la ruta
                NewRowPadre.RowHijos = new Array();
                for (var ContX = 1, t = _this.GridTreeGridControl.Columns.length - 1; ContX < t; ContX++) {
                    var IndexDataSourceDefault = _this.GridTreeGridControl.Columns[ContX].IndexDataSourceDefault;
                    var Cell0 = new TVclCellCF();//Creamos celda
                    Cell0.CellColor = "";//Color de celda
                    Cell0.Value = _this.DataSet.Records[IndiceReal].Fields[IndexDataSourceDefault].Value; // ASIGNA O EXTRAE EL VALOR QUE SE MOSTRARA EN LA CELDA DEL GRIDCONTROL 
                    Cell0.IndexColumn = ContX; // ASIGNA O EXTRAE EL INDEX DE LA COLUMNA 
                    Cell0.IndexRow = e; // ASIGNA O EXTRAE EL INDEX DEL ROW 
                    NewRow.AddCell(Cell0); // AGREGA O EXTRAE LA CELDA AL ROW
                }
                //**Agrego celda para botones
                var CellButton = new TVclCellCF();//Creamos celda
                CellButton.Value = "";
                CellButton.IndexColumn = _this.DataSet.FieldCount; // ASIGNA O EXTRAE EL INDEX DE LA COLUMNA 
                CellButton.IndexRow = e;//Índice de fila
                var objetos = { "Row": NewRow, "Grilla": _this.GridTreeGridControl };
                _this.dibujarCellButton(true, NewRow, CellButton, objetos);
                NewRow.AddCell(CellButton);
                NewRow.onclick = function (NewRow) { // ASIGNA UN EVENTO CUANDO SE HACE CLICK EN EL ROW DEL GRIDCONTROL

                    _this.Grilla_rowclick_Tree(NewRow);
                }
                NewRowPadre.RowHijos.push(NewRow);
                NewRow.RowPadre = NewRowPadre;
                if (lastRow == null) {
                    _this.GridTreeGridControl.AddRowAfter(2, NewRow, NewRowPadre, null);
                    lastRow = NewRow;
                } else {
                    _this.GridTreeGridControl.AddRowAfter(2, NewRow, lastRow, null);
                    lastRow = NewRow;
                }

                NewRow.Index = $(NewRow.This).index();
                NewRow.IndexDataSet = IndiceReal;
                NewRow.IndexTreeGrid = e;
                if (e == 0) {
                    _this.LastRutaGroup.IndexRowStart = $(NewRow.This).index();
                    _this.GridTreeGridControl.IndexRow = $(NewRow.This).index();
                    firstRow = NewRow;
                }
                if (e == ItemTreePanel.length - 1 || _this.filasMostradas - 1 == e) {
                    _this.LastRutaGroup.IndexRowEnd = $(NewRow.This).index();
                }

            }
            _this.IndexRowGridPorScroll = 0;
            if (firstRow != null) {
                _this.Grilla_rowclick_Tree(firstRow);
            }
            _this.GridTreeGridControl.ContracAllColumns();

        }
    }
    catch (e) {
        _this.LastRutaGroup = null;
        console.log(e);
        SysCfg.Log.Methods.WriteLog("TVclTreeGridControl.js loadDataSetRecordsPaginationFilter", e);
    }


}
/*Función para crear una fila de agrupación*/
Componet.GridCF.TVclTreeGridControl.prototype.createRowAgrupation = function (val) {
    var _this = this.TParent();
    var grilla = _this.GridTreeGridControl;//Grid del Tree
    var colspan = _this.TVclGridCFSource.Columns.length;//Separación left del texto con la fila que lo contiene.
    var ruta = val;//Ruta del item
    var subRuta = "";//SubRutas: por ejemplo la ruta ["País:Ciudad"], entonces tenemos con sub rutas["Perú:Lima","Perú:Trujillo"]
    var espacios = 0;//Espacios que correr el texto a la derecha.
    var ImgGruop = new Array();
    ImgGruop[true] = "";
    ImgGruop[false] = "none";
    if (typeof (_this.RowGroup[val]) == "undefined") {//Verificamos que no exista ruta
        _this.ListRouteGroup.push(val);//Asignamos ruta al array que guarda todas las rutas
        var words = ruta.split('\\');//separamos el texto de la ruta.
        subRuta = subRuta + words[0];//Agregamos el primer texto de la separación.//SubRutas: por ejemplo la ruta ["País:Ciudad"], entonces tenemos con sub rutas["Perú:Lima","Perú:Trujillo"]
        for (var Counter = 0; Counter < words.length; Counter++) {//Recorremos el array de la palabras separadas
            var subRutaPadre = subRuta;//SubRutas: por ejemplo la ruta ["País:Ciudad"], entonces tenemos con sub rutas["Perú:Lima","Perú:Trujillo"]
            if (Counter > 0) {
                subRuta = subRuta + "\\" + words[Counter];//SubRutas: por ejemplo la ruta ["País:Ciudad"], entonces tenemos con sub rutas["Perú:Lima","Perú:Trujillo"]
                espacios = espacios + 20;//Agregamos espacios, esto sirve para correr el texto a la derecha.
            }
            var NewRow = new TVclRowCF();//Creamos nueva Row
            /*Función al abrir agrupación*/
            NewRow.OnEventGruopDown = function (collapse, RutaGruopDown, NewRowPadre) {
                var Row = _this.GridTreeGridControl.FocusedRowHandle;
                if (Row != null && (_this.DataSet.Status == SysCfg.MemTable.Properties.TStatus.Apped || _this.DataSet.Status == SysCfg.MemTable.Properties.TStatus.Update)) {
                    if (_this.DataSet.Status == SysCfg.MemTable.Properties.TStatus.Apped) {
                        var indexEliminado = Row.Index;
                        _this.GridTreeGridControl.RemoveRow(indexEliminado);
                        _this.GridTreeGridControl.IndexRow = indexEliminado;
                        _this.rowSelecionado = _this.GridTreeGridControl._FocusedRowHandle;
                        _this.rowSelecionado.Index = indexEliminado;
                    }
                    _this.DataSet.Status = SysCfg.MemTable.Properties.TStatus.None;
                    _this.DataSet.Cancel();
                }

                if (_this.LastRutaGroup != null) {
                    $(_this.RowGroup[_this.LastRutaGroup.RutaGruopDown].PanelPagination).html("");
                    _this.RowGroup[_this.LastRutaGroup.RutaGruopDown]._ImgGruopDown.style.display = 'none';//Ocultamos imagen de flecha("Expandida")
                    _this.RowGroup[_this.LastRutaGroup.RutaGruopDown]._ImgGruopRigth.style.display = '';//Mostramos imagen de flecha("Collapse")
                    _this.RowGroup[_this.LastRutaGroup.RutaGruopDown].PanelPaginationGroup.style.display = "none";
                    _this.RowGroup[_this.LastRutaGroup.RutaGruopDown].VclStackPanelGrup.style.display = "";
                    var tempIndexRowStart = _this.LastRutaGroup.IndexRowStart;
                    for (var IndexRowStart = _this.LastRutaGroup.IndexRowStart; IndexRowStart <= _this.LastRutaGroup.IndexRowEnd; IndexRowStart++) {
                        _this.GridTreeGridControl.RemoveRow(tempIndexRowStart);
                    }
                    _this.LastRutaGroup = null;
                }

            }
            if (typeof (_this.RowGroup[subRuta]) == "undefined") {//Validamos que no exista ruta
                var Cell = new TVclCellCF();//Creamos celda
                Cell._Value = words[Counter];//Value celda (Texto agrupación)
                Cell.IndexColumn = 0; // ASIGNA O EXTRAE EL INDEX DE LA COLUMNA 
                NewRow.AddCell(Cell);
                _this.RowGroup[subRuta] = NewRow;//SubRutas: por ejemplo la ruta ["País:Ciudad"], entonces tenemos con sub rutas["Perú:Lima","Perú:Trujillo"]
                NewRow.PaddingGroup = espacios;//Agregamos espacios, esto sirve para correr el texto a la derecha.
                NewRow.This.style.backgroundColor = "#3e94ba";//Bacukground de fila que es agrupación
                NewRow.onclick = function (NewRow) {
                }
                if (Counter > 0) {
                    NewRow.Visible = _this.open;
                    var cantidadHijos = (_this.RowGroup[subRutaPadre].CantidadHijos + 1);//Aumentamos +1 la cantidad de hijas de la fila padre
                    _this.RowGroup[subRutaPadre].RowHijos.push(NewRow);
                    _this.RowGroup[subRutaPadre].CantidadHijos = cantidadHijos;//Actualizamos cantidad de hijos
                    NewRow._ImgGruopDown.style.display = ImgGruop[_this.open];//Mostramos imagen de flecha("Expandida") si por defecto la agrupación esta abierta
                    NewRow._ImgGruopRigth.style.display = ImgGruop[!_this.open];//Ocultamos imagen de flecha("Collapse") si por defecto la agrupación esta cerrada
                    grilla.AddRowAfter(1, NewRow, _this.RowGroup[subRutaPadre], colspan);//Agregamos fila sub-padre("Ruta principal"), por ejemplo tenemos la rutas["País:Perú",["ciudad:lima","ciudad:trujillo"]] y ["Pais:Colombia",["ciudad:cali","ciudad:bogotá"]] las sub-rutas l son ["ciudad:lima"],["ciudad:trujillo"],["ciudad:cali"] y ["ciudad:bogotá"]
                    NewRow.RowPadre = _this.RowGroup[subRutaPadre];
                }
                else {
                    grilla.AddRowAfter(0, NewRow, null, colspan);//Agregamos fila padre("Ruta principal"), por ejemplo tenemos la rutas["País:Perú","ciudad:lima"] y ["Pais:Colombia","ciudad:cali"] la ruta padre pincipal son ["País:Perú"] y ["País:Colombia"]
                    NewRow._ImgGruopDown.style.display = 'none';//Ocultamos imagen de flecha("Expandida")
                    NewRow._ImgGruopRigth.style.display = '';//Mostramos imagen de flecha("Collapse")
                }

            }
        }

        _this.ItemGroupPanel[val] = {
            "ItemTreePanel": new Array()
        }
        _this.RowGroup[val].CantidadHijos = _this.RowGroup[val].CantidadHijos + 1;//Aumentamos +1 la cantidad de hijas de la fila padre
        _this.RowGroup[val].RutaGruopDown = val;
        /*Función al abrir agrupación*/
        _this.RowGroup[val].OnEventGruopDown = function (collapse, RutaGruopDown, NewRowPadre) {

            var Row = _this.GridTreeGridControl.FocusedRowHandle;
            if (Row != null && (_this.DataSet.Status == SysCfg.MemTable.Properties.TStatus.Apped || _this.DataSet.Status == SysCfg.MemTable.Properties.TStatus.Update)) {
                if (_this.DataSet.Status == SysCfg.MemTable.Properties.TStatus.Apped) {
                    var indexEliminado = Row.Index;
                    _this.GridTreeGridControl.RemoveRow(indexEliminado);
                    _this.GridTreeGridControl.IndexRow = indexEliminado;
                    _this.rowSelecionado = _this.GridTreeGridControl._FocusedRowHandle;
                    _this.rowSelecionado.Index = indexEliminado;
                }

                _this.DataSet.Status = SysCfg.MemTable.Properties.TStatus.None;
            }
            var OpcionVisibleButton = _this.TVclGridCFSource.Columns[colspan - 1].Visible;
            var OpcionVisibleCheck = _this.TVclGridCFSource.Columns[0].Visible;
            NewRowPadre.setPaginationRow();
            if (collapse) {
                if (_this.LastRutaGroup != null) {

                    if (typeof (_this.RowGroup[_this.LastRutaGroup.RutaGruopDown]) != "undefined") {
                        $(_this.RowGroup[_this.LastRutaGroup.RutaGruopDown].PanelPagination).html("");
                        _this.RowGroup[_this.LastRutaGroup.RutaGruopDown]._ImgGruopDown.style.display = 'none';//Ocultamos imagen de flecha("Expandida")
                        _this.RowGroup[_this.LastRutaGroup.RutaGruopDown]._ImgGruopRigth.style.display = '';//Mostramos imagen de flecha("Collapse")
                        _this.RowGroup[_this.LastRutaGroup.RutaGruopDown].PanelPaginationGroup.style.display = "none";
                        _this.RowGroup[_this.LastRutaGroup.RutaGruopDown].VclStackPanelGrup.style.display = "";
                        var tempIndexRowStart = _this.LastRutaGroup.IndexRowStart;
                        for (var IndexRowStart = _this.LastRutaGroup.IndexRowStart; IndexRowStart <= _this.LastRutaGroup.IndexRowEnd; IndexRowStart++) {
                            _this.GridTreeGridControl.RemoveRow(tempIndexRowStart);
                        }
                    }
                }

                _this.LastRutaGroup = {
                    RutaGruopDown: RutaGruopDown,
                    IndexRowStart: null,
                    IndexRowEnd: null,
                }
                $(NewRowPadre.PanelPagination).html("");
                _this.VPagination = new Componet.GridCF.TGridCFView.TPagination(NewRowPadre.PanelPagination, true, 5);
                _this.loadDataSetRecordsPaginationFilter(NewRowPadre, _this.ItemGroupPanel[RutaGruopDown].ItemTreePanel);
                return true;
            } else {
                _this.GridCFView.Information.VclGridCFInfo.ClearAll();
                if (_this.LastRutaGroup != null) {

                    _this.RowGroup[_this.LastRutaGroup.RutaGruopDown]._ImgGruopDown.style.display = 'none';//Ocultamos imagen de flecha("Expandida")
                    _this.RowGroup[_this.LastRutaGroup.RutaGruopDown]._ImgGruopRigth.style.display = '';//Mostramos imagen de flecha("Collapse")
                    _this.RowGroup[_this.LastRutaGroup.RutaGruopDown].PanelPaginationGroup.style.display = "none";
                    _this.RowGroup[_this.LastRutaGroup.RutaGruopDown].VclStackPanelGrup.style.display = "";
                    var tempIndexRowStart = _this.LastRutaGroup.IndexRowStart;
                    for (var IndexRowStart = _this.LastRutaGroup.IndexRowStart; IndexRowStart <= _this.LastRutaGroup.IndexRowEnd; IndexRowStart++) {
                        _this.GridTreeGridControl.RemoveRow(tempIndexRowStart);
                    }
                    _this.LastRutaGroup = null;
                }
            }
        }
    }
}
/*Función para remover una fila de agrupación*/
Componet.GridCF.TVclTreeGridControl.prototype.removeRowPagination = function () {
    var _this = this.TParent();
    if (_this.LastRutaGroup != null) {//validamos si existe última ruta abierta.
        _this.RowGroup[_this.LastRutaGroup.RutaGruopDown]._ImgGruopDown.style.display = 'none';//Ocultamos imagen de flecha("Expandida")
        _this.RowGroup[_this.LastRutaGroup.RutaGruopDown]._ImgGruopRigth.style.display = '';//Mostramos imagen de flecha("Collapse")
        var tempIndexRowStart = _this.LastRutaGroup.IndexRowStart;//Índice de fila donde comienza la agrupación de la ruta
        var tempIndexRowEnd = _this.LastRutaGroup.IndexRowEnd;//Índice de fila donde finaliza la agrupación de la ruta
        if (_this.GridTreeGridControl.FocusedRowHandle != null) {
            /*Realizamos para actualizar el inicio y fin de la agrupación.
            Supongamos que en el grid estamos insertando, pero se cierra la agrupación
            entonces debemos actualizar el inicio y fin de la agrupacion por que esa fila
            que se insertó temporalmente esta ocupando una fila.
            */
            var IndexRowStartGrid = $(_this.GridTreeGridControl.FocusedRowHandle.This).index();//Índice real de la fila
            _this.LastRutaGroup.IndexRowStart = IndexRowStartGrid;//Índice de fila
            _this.LastRutaGroup.IndexRowEnd = IndexRowStartGrid + (tempIndexRowEnd - tempIndexRowStart);//Último índice de fila
            tempIndexRowStart = _this.LastRutaGroup.IndexRowStart;
        }
        for (var IndexRowStart = _this.LastRutaGroup.IndexRowStart; IndexRowStart <= _this.LastRutaGroup.IndexRowEnd; IndexRowStart++) {//Recorremos las filas en este rango para eliminar las filas de esa agrupación
            _this.GridTreeGridControl.RemoveRow(tempIndexRowStart);//Removemos fila
        }
        _this.LastRutaGroup.IndexRowStart = null;//Seteamos los indice donde comienza la agrupación
        _this.LastRutaGroup.IndexRowEnd = null;//Seteamos los indice donde comienza la agrupación     
    }
}
/*Función para volver a crear la agrupación desde el grid(Cuando se abre agrupación) */
Componet.GridCF.TVclTreeGridControl.prototype.OpenTreeGridGroup = function () {
    var _this = this.TParent();
    _this.inTreeGrid = true;
    $(objectContenGridHtml).hide();//Ocultamos div del grid cfview
    _this.agruparPorCampo();//agrupamos registros
}
/*Al agregar una nueva columna agregamos a la lista de combos el valor de la nueva columna*/
Componet.GridCF.TVclTreeGridControl.prototype.AddItems = function () {
    var _this = this.TParent();
    if (_this.lastCombobox != null) {
        var VclComboObject = new TVclComboBoxItem();//Creamos combo
        VclComboObject.Value = _this.nameColumns.length;//Calor de la ultima columna
        VclComboObject.Text = _this.TVclGridCFSource.GetColumn(_this.nameColumns[_this.nameColumns.length - 1]).Caption;//Caption
        VclComboObject.Tag = _this.nameColumns[_this.nameColumns.length - 1];//Tag último item del array
        _this.lastCombobox.AddItem(VclComboObject);//Agregamos item al último combo formado
        _this.lastCombobox.selectedIndex = 0;
        _this.itemsComboDinamicos.push(_this.nameColumns[_this.nameColumns.length - 1]);//Agregamos item al itemsComboDinamicos
    }
}