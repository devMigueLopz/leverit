﻿var TVclTimer = function () {
    this._Enabled = false;

    Object.defineProperty(this, 'Enabled', {
        get: function () {
            return this._Enabled;
        },
        set: function (_Value) {
            this._Enabled = _Value;
            if (!_Value)
                this.Stop();
            else
                this.Start();
            
        }
    });

    this.Methods = new Array();
    this.AddMethod = function (_MethodName, _MethodCall, _MethodTime) {
        var MethodTemp = new TVclTimeElement();
        MethodTemp.MethodName = _MethodName;
        MethodTemp.MethodCall = _MethodCall;
        MethodTemp.MethodTime = _MethodTime;
        MethodTemp.Id = 0;
        //MethodTemp.Id = setInterval(_MethodCall, _MethodTime);
        this.Methods.push(MethodTemp);

    }

    this.RemoveMethod = function (_MethodName) {
        for (var i = 0; i < this.Methods.length; i++) {
            if (this.Methods[i].MethodName == _MethodName) {
                clearInterval(this.Methods[i].Id);
                this.Methods.splice(i, 1);
            }
        }        
    }

    this.Destroy = function ()
    {
        for (var i = 0; i < this.Methods.length; i++) {
            clearInterval(this.Methods[i].Id);
            this.Methods.splice(i, 1);
        }
        //delete this;
    }
    this.Stop = function()
    {
        for (var i = 0; i < this.Methods.length; i++) {
            clearInterval(this.Methods[i].Id);
        }
        this._Enabled = false;
    }

    this.Start = function () {
        if (!this._Enabled) {
            this._Enabled = true;
            for (var i = 0; i < this.Methods.length; i++) {
                this.Methods[i].Id = setInterval(this.Methods[i].MethodCall, this.Methods[i].MethodTime);
            }
        }
    }

}//new SysCfg.ref("");

var TVclTimeElement = function () {
    this.Id = 0;
    this.MethodName = null;
    this.MethodCall = null;
    this.MethodTime = 0;
}