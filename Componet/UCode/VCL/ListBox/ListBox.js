﻿var TVclListBox = function (inObjectHtml, Id) {
    this.This = new TVclStackPanel(inObjectHtml, "1", 1, [[12], [12], [12], [12], [12]]);
    this.ListBoxItems = new Array();
    this._EnabledCheckBox = false;
    this._FocusedListBoxItem = null;
    this.ClearAll = function () {
        $(this.This.Column[0].This).html("");
        this.ListBoxItems = new Array();

    }

    Object.defineProperty(this, 'Selected', {
      
        set: function (inValue) {
            this.ListBoxItems[inValue].Selected = true;
        }
    });

    Object.defineProperty(this, 'FocusedListBoxItem', {
        get: function () {
            return this._FocusedListBoxItem;
        },
        set: function (inValue) {
            if (this._FocusedListBoxItem != null) {
                $(this._FocusedListBoxItem.This.Row.This).css("background-color", "");
                this._FocusedListBoxItem.IsSelected = false;
            }
            this._FocusedListBoxItem = inValue;
        }
    });

    Object.defineProperty(this, 'EnabledCheckBox', {
        get: function () {
            return this._EnabledCheckBox;
        },
        set: function (inValue) {
            this._EnabledCheckBox = inValue;
            for (var i = 0; i < this.ListBoxItems.length; i++) {
                this.ListBoxItems[i].EnabledCheckBox = inValue;
            }
        }
    });

    this.AddListBoxItem = function (inListBoxItem) {
        this.ListBoxItems.push(inListBoxItem);
        inListBoxItem.ListBoxParent = this;
        inListBoxItem.EnabledCheckBox = this.EnabledCheckBox;
        $(this.This.Column[0].This).append(inListBoxItem.This.Row.This);
    }

    this.GetListBoxItemChecked = function () {
        var arrTem = new Array();
        for (var i = 0; i < this.ListBoxItems.length; i++) {
            if (this.ListBoxItems[i].CheckBox.Checked)
                arrTem.push(this.ListBoxItems[i]);
        }
    }

    this.CheckedAll = function () {
        for (var i = 0; i < this.ListBoxItems.length; i++) {
            this.ListBoxItems[i].CheckBox.Checked = true;
        }
    }

    this.UnCheckedAll = function () {
        for (var i = 0; i < this.ListBoxItems.length; i++) {
            this.ListBoxItems[i].CheckBox.Checked = false;
        }
    }



    this.OnCheckedListBoxItemChange = null;
    this.CheckedListBoxItemChange = function (EventArgs) {
        
        if (this.OnCheckedListBoxItemChange != null)
            this.OnCheckedListBoxItemChange(EventArgs, this);
    };
    
}

var TVclListBoxItem = function () {
    var _this = this;
    this.This = new TVclStackPanel(null, "1", 1, [[12], [12], [12], [12], [12]]);
    this.This.Row.This.style.cursor = "pointer";
    this.This.Row.This.style.paddingTop = "6px";
    this.This.Row.This.style.paddingBottom = "3px";
    this.ListBoxParent = null;
    this.IsSelected = false;
    this._Index = -1;
    this._Tag = null;


    var old_style = null;
    $(this.This.Row.This).hover(function () {
        if (!_this.IsSelected) {
            old_style = $(_this.This.Row.This).css("background-color");
            $(_this.This.Row.This).css("background-color", "#f9f9f9");
        }
    }, function () {
        if (!_this.IsSelected) {
            $(_this.This.Row.This).css("background-color", old_style);
        }

    });


    this.This.Row.This.onclick = function () {
        
        if (_this.ListBoxParent != null) {
            if (_this.ListBoxParent.FocusedListBoxItem != null)
                _this.ListBoxParent.FocusedListBoxItem.This.Row.This.style.backgroundColor = "";
            _this.ListBoxParent.FocusedListBoxItem = _this;
            _this.ListBoxParent.FocusedListBoxItem.This.Row.This.style.backgroundColor = "lightyellow";
            _this.IsSelected = true;
            if (!_this.EnabledCheckBox) {
                _this.ListBoxParent.CheckedListBoxItemChange(_this);
            }
                
        }
    }

    this.CheckBox = new TVclInputcheckbox(this.This.Column[0].This, "");
    this.CheckBox.This.style.marginRight = "8px";
    this.CheckBox.This.onchange = function () {
        if (_this.ListBoxParent != null) {
            if (_this.ListBoxParent.FocusedListBoxItem != null)
                _this.ListBoxParent.FocusedListBoxItem.This.Row.This.style.backgroundColor = "";
            _this.ListBoxParent.FocusedListBoxItem = _this;
            _this.ListBoxParent.FocusedListBoxItem.This.Row.This.style.backgroundColor = "lightyellow";
            _this.IsSelected = true;
            _this.ListBoxParent.CheckedListBoxItemChange(_this);
        }
        
    }

    //this.CheckBox.This.style.marginTop = "8px";
    this.Label = new TVcllabel(this.This.Column[0].This, "", TlabelType.H0);

    this._Text = null;
    this._Checked = null;
    this._EnabledCheckBox = false;

    Object.defineProperty(this, 'OnClick', {
        get: function () {
            return this.This.onclick;
        },
        set: function (inValue) {
            var _this1 = this;
            this.This.Row.This.onclick = function () {
                if (_this1.GridParent != null) {
                    if (_this1.ListBoxParent.FocusedListBoxItem != null)
                        _this1.ListBoxParent.FocusedListBoxItem.This.Row.This.style.backgroundColor = "";
                    _this1.ListBoxParent.FocusedListBoxItem = _this1;
                    _this1.ListBoxParent.FocusedListBoxItem.This.Row.This.style.backgroundColor = "lightyellow";
                }
                inValue();
            };
        }
    });

    Object.defineProperty(this, 'Index', {
        get: function () {
            return this._Index;
        },
        set: function (inValue) {
            this._Index = inValue;
        }
    });

    Object.defineProperty(this, 'Tag', {
        get: function () {
            return this._Tag;
        },
        set: function (inValue) {
            this._Tag = inValue;
        }
    });

    Object.defineProperty(this, 'Text', {
        get: function () {
            return this._Text;
        },
        set: function (inValue) {
            this._Text = inValue;
            this.Label.Text = inValue;
        }
    });

    Object.defineProperty(this, 'Checked', {
        get: function () {
            return this.CheckBox.Checked;
        },
        set: function (inValue) {
            this._Checked = inValue;
            this.CheckBox.Checked = inValue;
        }
    });

    Object.defineProperty(this, 'EnabledCheckBox', {
        get: function () {
            return this._EnabledCheckBox;
        },
        set: function (inValue) {
            this._EnabledCheckBox = inValue;
            if (inValue)
                this.CheckBox.This.style.display = "inline-block";
            else
                this.CheckBox.This.style.display = "none";
        }
    });

    Object.defineProperty(this, 'Selected', {
        get: function () {
            return this.IsSelected;
        },
        set: function (inValue) {
            if (inValue) {
                if (_this.ListBoxParent != null) {
                    if (_this.ListBoxParent.FocusedListBoxItem != null) {
                        _this.ListBoxParent.FocusedListBoxItem.IsSelected = false;
                        _this.ListBoxParent.FocusedListBoxItem.This.Row.This.style.backgroundColor = "";
                    }
                    _this.ListBoxParent.FocusedListBoxItem = _this;
                    _this.ListBoxParent.FocusedListBoxItem.This.Row.This.style.backgroundColor = "lightyellow";
                    _this.IsSelected = true;
              }
            }
        }
    });
}