﻿var TSaveFile = function (inObjectHtml, Id) {
    this.TParent = function () {
        return this;
    }.bind(this);

    var _this = this.TParent();

    this.This = Ua(inObjectHtml, Id, "");
    this._VCLType = null;
    this.NameFile = 'File.txt';
    this.Path = "documents";
    this.tag = null;
    this.Bytes = new Array();
    this.ResErr = new SysCfg.Error.Properties.TResErr();
    SysCfg.Error.Properties.ResErrfill(this.ResErr);
    this.This.style.visibility = "hidden";

    this.SetFile = function (callBack) {
        var blob = new Blob([new Uint8Array(_this.Bytes)]);
        var url = window.URL.createObjectURL(blob);
        if (window.navigator.msSaveOrOpenBlob) {
            var blobObject = new Blob([new Uint8Array(_this.Bytes)]);
            window.navigator.msSaveOrOpenBlob(blobObject, _this.NameFile);
        } else {
            this.This.href = url;
            this.This.download = _this.NameFile;
            _this.This.click();
        }
    
        _this.ResErr.NotError = true;
        _this.ResErr.Mesaje = "OK";
        callBack(_this, _this.ResErr);
    }

    //this.This.onchange = function (event) {
    //    alert(event);
    //}

    //#region propiedades que alteran los atributos del elemento html   

    Object.defineProperty(this, 'Text', {
        get: function () {
            return $(this.This).val();
        },
        set: function (_Value) {
            $(this.This).val(_Value);
        }
    });

    Object.defineProperty(this, 'Enabled', {
        get: function () {
            return !this.This.disabled;
        },
        set: function (_Value) {
            this.This.disabled = !_Value;
        }
    });

    Object.defineProperty(this, 'onClick', {
        get: function () {
            return this.This.onclick;
        },
        set: function (_Value) {
            this.This.onclick = _Value;
        }
    });

    Object.defineProperty(this, 'onKeyPress', {
        get: function () {
            return this.This.onkeypress;
        },
        set: function (_Value) {
            this.This.onkeypress = _Value;
        }
    });

    Object.defineProperty(this, 'onChange', {
        get: function () {
            return this.This.onchange;
        },
        set: function (_Value) {
            this.This.onchange = _Value;
        }
    });

    Object.defineProperty(this, 'onKeyUp', {
        get: function () {
            return this.This.onkeyup;
        },
        set: function (_Value) {
            this.This.onkeyup = _Value;
        }
    });

    Object.defineProperty(this, 'VCLType', {
        get: function () {
            return this._VCLType;
        },
        set: function (_Value) {
            this._VCLType = _Value;
            switch (_Value) {
                case TVCLType.AG:
                    break;
                case TVCLType.BS:
                    BootStrapCreateInpunttextbox(this.This);
                    break;
                case TVCLType.DV:
                    break;
                case TVCLType.KD:
                    break;
                default:
            }
        }
    });

    Object.defineProperty(this, 'Type', {
        get: function () {
            return this.This.type;
        },
        set: function (_Value) {
            this.This.type = _Value.name;
        }
    });

    Object.defineProperty(this, 'Visible', {
        get: function () {
            if (this.This.style.visibility == "hidden" || this.This.style.visibility == "collapse")
                return false;
            return true;
        },
        set: function (_Value) {
            if (_Value)
                this.This.style.visibility = "visible";
            else
                this.This.style.visibility = "hidden";
        }
    });
    //#endregion

    //#region propiedades que alteran los estilos del elemento html 
    Object.defineProperty(this, 'Width', {
        get: function () {
            return this.This.style.width;
        },
        set: function (_Value) {
            this.This.style.width = _Value;
        }
    });
    //#endregion






}