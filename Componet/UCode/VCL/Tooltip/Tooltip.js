﻿var TVclTooltip = function (ObjectHtml, Id) {

    $(ObjectHtml).addClass("Divtooltip");
    this.This = ObjectHtml;

    this._Image = null;
    this._LabText = null;
    this.ID = Id;
    this._Position = "";

    this._Image = new TVclImagen(ObjectHtml, this.ID + "_ImageTooltip");
    this._Image.Cursor = "pointer";   
     
    this._LabText = new TVcllabel(ObjectHtml, this.ID + "_LabelTooltip", TlabelType.H0);
    this._LabText.VCLType = TVCLType.BS;
    $(this._LabText.This).addClass("tooltiptext");
    $(this._LabText.This).css("border","1px solid black");
    this._LabText.This.style.marginTop = "-3px";
    this._LabText.This.style.marginLeft = "5px";

    Object.defineProperty(this, 'Image', {
        get: function () {
            return this._Image.Src;
        },
        set: function (_Value) {
            this._Image.Src = _Value;
        }
    });


    Object.defineProperty(this, 'Text', {
        get: function () {
            return this._LabText.Text;
        },
        set: function (_Value) {
            this._LabText.Text = _Value;
        }
    });

    Object.defineProperty(this, 'Enabled', {
        get: function () {
            return !this._Image.disabled;
        },
        set: function (_Value) {
            this._Image.disabled = !_Value;            
        }
    });

    Object.defineProperty(this, 'onClick', {
        get: function () {
            return this._Image.onClick;
        },
        set: function (_Value) {
            this._Image.onClick = _Value;
        }
    });

    Object.defineProperty(this, 'Visible', {
        get: function () {
            if (this.This.style.visibility == "hidden" || this.This.style.visibility == "collapse")
                return false;
            return true;
        },
        set: function (_Value) {
            if (_Value)
                this.This.style.visibility = "visible";
            else
                this.This.style.visibility = "hidden";
        }
    });

    Object.defineProperty(this, 'BackgroundColor', {
        get: function () {
            return this._LabText.This.style.backgroundColor;
        },
        set: function (_Value) {
            this._LabText.This.style.backgroundColor = _Value;
        }
    });


    Object.defineProperty(this, 'TextColor', {
        get: function () {
            return this._LabText.This.style.color;
        },
        set: function (_Value) {
            this._LabText.This.style.color = _Value;
        }
    });

    Object.defineProperty(this, 'BorderColor', {
        get: function () {
            return this._LabText.This.style.borderColor;
        },
        set: function (_Value) {
            this._LabText.This.style.borderColor = _Value;
        }
    });

    Object.defineProperty(this, 'Width', {
        get: function () {
            return this._LabText.This.style.width;
        },
        set: function (_Value) {
            this._LabText.This.style.width = _Value;
        }
    });

    Object.defineProperty(this, 'Position', {
        get: function () {
            return this._Position;
        },
        set: function (_Value) {
            this._Position = _Value
            if (_Value == "Top") {
                this._LabText.This.style.marginTop = "-35px";
                this._LabText.This.style.marginLeft = "-25px";
            }
            if (_Value == "Bottom") {
                this._LabText.This.style.marginTop = "25px";
                this._LabText.This.style.marginLeft = "-25px";
            }
            if (_Value == "Left") {
                this._LabText.This.style.marginTop = "-3px";
                var width = ((parseInt(this._LabText.This.style.width) + 25) *-1);
                width = width + "px";
                this._LabText.This.style.marginLeft = width;
            }
            if (_Value == "Rigth") {
                this._LabText.This.style.marginTop = "-3px";
                this._LabText.This.style.marginLeft = "8px";
            }
        }
    });
      
    var TParent = function () {
        return this;
    }.bind(this);

}