﻿//function clsComboBox() {

var TVclComboBox2 = function (ObjectHtml, Id) {
    this.This = Uselect(ObjectHtml, Id);
    this.Options = new Array();
    this._Tag = null;
    this._ValueMember = null;
    this._DisplayMember = null;
    this._ValueTag = null;
    this._DataSource = null;
    this.This.selectedIndex = -1;
    
    
    //Preguntar como obtner el this del objeto creado
    Object.defineProperty(this, 'getThis', {
        get: function () {
            return this.This;
        }
    });
    //
    Object.defineProperty(this, 'DataSource', {
        get: function () {
            return this._DataSource;
        },
        set: function (_Value) {
            this._DataSource = _Value;
            for (var i = 0; i < this._DataSource.length; i++) {
                
                var VclComboBoxItem = new TVclComboBoxItem();
                if (this._ValueMember != null && this._ValueMember != "")
                    VclComboBoxItem.Value = this._DataSource[0][this._ValueMember];
                if (this._DisplayMember != null && this._DisplayMember != "")
                    VclComboBoxItem.Text = this._DataSource[0][this._DisplayMember];
                VclComboBoxItem.Tag = this._DataSource[0];
                this.AddItem(VclComboBoxItem);
            }
        }
    });

    Object.defineProperty(this, 'ValueMember', {
        get: function () {
            return this._ValueMember;
        },
        set: function (_Value) {
            this._ValueMember = _Value;
        }
    });

    Object.defineProperty(this, 'DisplayMember', {
        get: function () {
            return this._DisplayMember
        },
        set: function (_Value) {
            this._DisplayMember = _Value;
        }
    });

    Object.defineProperty(this, 'Tag', {
        get: function () {
            return this._Tag;
        },
        set: function (_Value) {
            this._Tag = _Value;
        }
    });

    Object.defineProperty(this, 'selectedIndex', {
        get: function () {
            return this.This.selectedIndex;
        },
        set: function (_Value) {
            this.This.selectedIndex = _Value;
            
        }
    });

    Object.defineProperty(this, 'Text', {
        get: function () {
            return this.This.options[this.This.selectedIndex].innerHTML
        },
        set: function (_Value) {
            this.This.options[this.This.selectedIndex].innerHTML = _Value;
        }
    });

    Object.defineProperty(this, 'Value', {
        get: function () {
            return $(this.This).val();
        },
        set: function (_Value) {
            $(this.This).val(_Value);
        }
    });

    Object.defineProperty(this, 'Enabled', {
        get: function () {
            return !this.This.disabled;
        },
        set: function (_Value) {
            this.This.disabled = !_Value;
        }
    });

    Object.defineProperty(this, 'onClick', {
        get: function () {
            return this.This.onclick;
        },
        set: function (_Value) {
            this.This.onclick = _Value;
        }
    });

    Object.defineProperty(this, 'onChange', {
        get: function () {
            return this.This.onchange;
        },
        set: function (_Value) {
            this.This.onchange = _Value;
        }
    });

    Object.defineProperty(this, 'VCLType', {
        get: function () {
            return this.VCLType;
        },
        set: function (_Value) {
            switch (_Value) {
                case TVCLType.AG:
                    break;
                case TVCLType.BS:
                    BootStrapCreateCheckbox(this.This);
                    break;
                case TVCLType.DV:
                    break;
                case TVCLType.KD:
                    break;
                default:
            }
        }
    });

    Object.defineProperty(this, 'Width', {
        get: function () {
            return this.This.style.width;
        },
        set: function (_Value) {
            this.This.style.width = _Value;
        }
    });

    Object.defineProperty(this, 'Visible', {
        get: function () {
            if (this.This.style.visibility == "hidden" || this.This.style.visibility == "collapse")
                return false;
            return true;
        },
        set: function (_Value) {
            if (_Value)
                this.This.style.visibility = "visible";
            else
                this.This.style.visibility = "hidden";
        }
    });

    this.Click = function () {
        this.This.click();
    }

    var TParent = function () {
        return this;
    }.bind(this);    

    //metodos 
    this.AddItem = function (VclComboBoxItem) {
        $(this.This).append(VclComboBoxItem.This);
        this.Options.push(VclComboBoxItem);
        this.This.selectedIndex = -1;
    }

    this.ClearItems = function () {
        this.This.innerHTML = "";
        this.Options = new Array();
        this.This.selectedIndex = -1;
    }
    this.SearchOption = function (value) {
        var option = null;
        for (var i = 0; i < this.Options.length; i++) {
            if (this.Options[i].Value == value) {
                option = this.Options[i];
            }
        }
        return (option);
    }
}

var TVclComboBoxItem = function () {
    this.This = document.createElement('option');
    //this.This.id = Id;
    this._Tag = null;

    //for (var i =Uoption 0; i < Data.length; i++) {
    //    this.Options[i] = Uoption(this.This, "option" + Id + i, Data[i].Value, Data[i].Text);
    //    if (IndexSelect >= 0)
    //        if (i == IndexSelect)
    //            this.This.value = Data[i].Value;
    //}
    Object.defineProperty(this, 'Id', {
        get: function () {
            return this.This.id;
        },
        set: function (_Value) {
            this.This.id = _Value;
        }
    });

    Object.defineProperty(this, 'Tag', {
        get: function () {
            return this._Tag;
        },
        set: function (_Value) {
            this._Tag = _Value;
        }
    });

    Object.defineProperty(this, 'Value', {
        get: function () {
            return this.This.value;
        },
        set: function (_Value) {
            this.This.value = _Value;
        }
    });

    Object.defineProperty(this, 'Text', {
        get: function () {
            return this.This.innerHTML;
        },
        set: function (_Value) {
            this.This.innerHTML = _Value;            
        }
    });

    var TParent = function () {
        return this;
    }.bind(this);

    //metodos 
}
































var TVclComboBox = function (ObjectHtml, Id, Data, IndexSelect) {
    this.This = Uselect(ObjectHtml, Id);
    this.Options = new Array(Data.length);

    
    for (var i = 0; i < Data.length; i++) {
        this.Options[i] = Uoption(this.This, "option" + Id + i, Data[i].Value, Data[i].Text);
        if (IndexSelect >= 0)
            if (i == IndexSelect)
                this.This.value = Data[i].Value;
    }

    Object.defineProperty(this, 'Text', {
        get: function () {
            return this.This.options[this.This.selectedIndex].innerHTML
        },
        set: function (_Value) {
            this.This.options[this.This.selectedIndex].innerHTML = _Value;
        }
    });

    Object.defineProperty(this, 'Value', {
        get: function () {
            return $(this.This).val();
        },
        set: function (_Value) {
            $(this.This).val(_Value);
        }
    });

    Object.defineProperty(this, 'Enabled', {
        get: function () {
            return !this.This.disabled;
        },
        set: function (_Value) {
            this.This.disabled = !_Value;
        }
    });

    Object.defineProperty(this, 'onClick', {
        get: function () {
            return this.This.onclick;
        },
        set: function (_Value) {
            this.This.onclick = _Value;
        }
    });
     
    Object.defineProperty(this, 'onChange', {
        get: function () {
            return this.This.onchange;
        },
        set: function (_Value) {
            this.This.onchange = _Value;
        }
    });

    Object.defineProperty(this, 'VCLType', {
        get: function () {
            return this.VCLType;
        },
        set: function (_Value) {
            switch (_Value) {
                case TVCLType.AG:
                    break;
                case TVCLType.BS:
                    BootStrapCreateCheckbox(this.This);
                    break;
                case TVCLType.DV:
                    break;
                case TVCLType.KD:
                    break;
                default:
            }
        }
    });

    Object.defineProperty(this, 'Width', {
        get: function () {
            return this.This.style.width;
        },
        set: function (_Value) {
            this.This.style.width = _Value;
        }
    });

    Object.defineProperty(this, 'Visible', {
        get: function () {
            if (this.This.style.visibility == "hidden" || this.This.style.visibility == "collapse")
                return false;
            return true;
        },
        set: function (_Value) {
            if (_Value)
                this.This.style.visibility = "visible";
            else
                this.This.style.visibility = "hidden";
        }
    });

    this.Click = function () {
        this.This.click();
    }

    var TParent = function () {
        return this;
    }.bind(this);
}




function TVclComboBox(Object, Id, Data, IndexSelect) {
    var ComboBox = new TVclComboBox();
    ComboBox.This = Uselect(Object, "select" + Id);
    ComboBox.Options = new Array(Data.length);

    for (var i = 0; i < Data.length; i++) {
        ComboBox.Options[i] = Uoption(ComboBox.This, "select" + Id + i, Data[i].Value, Data[i].Text);
        if (IndexSelect >= 0)
            if (i == IndexSelect)
                ComboBox.This.value = Data[i].Value;
    }
    return ComboBox;
}


