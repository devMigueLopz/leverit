﻿// Version 1 cabeza //2018/03/13
var TVclDropDownPanel = function (ObjectHtml, Id) {
    var TParent = function () {
        return this;
    }.bind(this);
    var _this = TParent()

    this.This = null;
    this.RowHeader = null;
    this.RowBody = null;
    this.Image = null;
    this._SrcUp = "image/24/Navigate-up.png";
    this._SrcDown = "image/24/Navigate-down.png";
    this._IsOpen = false;
    this._BorderAllColor = null;
    this._Tag = null;
    this.Mythis = "TVclDropDownPanel";

    UsrCfg.Traslate.GetLangText(this.Mythis, "Line1", "Open");
    UsrCfg.Traslate.GetLangText(this.Mythis, "Line2", "Close");
    this.OnFunctionOpen = function () {
        this._functionOpen();
        $(_this.RowBody.This).toggle("slow");
    }

    this.OnFunctionClose = function () {
        this._functionClose();
        $(_this.RowBody.This).toggle("slow");
    }

    this._functionOpen = function () {};
    this._functionClose = function () { };

    //Jorge Jaimes Version 1 //2018/03/13 Agrego funcion para mandar cerrar el panel desde el exterior por codigo
    this.ClosePanel = function () {        
        $(_this.RowBody.This).toggle("slow", function () {
            if (_this._IsOpen) {
                _this.Image.Src = _this._SrcDown;
                _this.RowBody.This.style.borderTop = "0px solid";
                _this.RowBody.This.style.borderTopColor = "";
                _this.Image.Title = UsrCfg.Traslate.GetLangText(_this.Mythis, "Line1");
                _this._IsOpen = false;
                _this._functionClose();
            }           
        });
    };


    var arrCont = new Array(1); //row principal
    arrCont[0] = new Array(1); //col principal
    arrCont[0][0] = new Array(2); //rows header y body
    arrCont[0][0][0] = new Array(2) // cols del header
    arrCont[0][0][1] = new Array(1) // cols del body

    
    this.This = VclDivitions(ObjectHtml, "Acodion" + Id, arrCont);   

    BootStrapCreateRow(this.This[0].This);
    BootStrapCreateColumn(this.This[0].Child[0].This, [12, 12, 12, 12, 12]);
    BootStrapCreateRows(this.This[0].Child[0].Child);
    BootStrapCreateColumns(this.This[0].Child[0].Child[0].Child, [[10, 2], [10, 2], [11, 1], [11, 1], [11, 1]]);
    BootStrapCreateColumn(this.This[0].Child[0].Child[1].Child[0].This, [12, 12, 12, 12, 12]);
    
    this.RowHeader = new TVclDiv(this.This[0].Child[0].Child[0].Child[0].This);
    this.RowBody = new TVclDiv(this.This[0].Child[0].Child[1].Child[0].This);

    this.RowHeader.This.style.padding = "0px";
    this.RowBody.This.style.padding = "0px";

    $(this.RowBody.This).toggle();

    this.This[0].This.style.borderTop = "3px solid #F39C12";
    this.This[0].This.style.borderBottom = "1px solid #E6E6E6";
    this.This[0].This.style.borderLeft = "1px solid #E6E6E6";
    this.This[0].This.style.borderRight = "1px solid #E6E6E6";
  

    this.Image = new TVclImagen(this.This[0].Child[0].Child[0].Child[1].This, "");
    //this.Image.This.style.float = "right";
    //this.Image.This.style.marginTop = "auto";
    //this.Image.This.style.marginBottom = "auto";
    //this.Image.This.style.marginLeft = "auto";
    this.Image.This.style.float = "right";   

    this.Image.onClick = function () {
        var _this = TParent();
        if (_this._IsOpen) {
            _this.Image.Src = _this._SrcDown;
            _this.RowBody.This.style.borderTop = "0px solid";
            _this.RowBody.This.style.borderTopColor = "";
            _this.Image.Title = UsrCfg.Traslate.GetLangText(_this.Mythis, "Line1");
            _this._IsOpen = false;
            _this.OnFunctionClose();

        }
        else {
            _this.RowBody.This.style.borderTop = "1px solid";
            _this.RowBody.This.style.borderTopColor = _this.This[0].This.style.borderBottomColor;
            _this.Image.Src = _this._SrcUp;
            _this.Image.Title = UsrCfg.Traslate.GetLangText(_this.Mythis, "Line2");
            _this._IsOpen = true;
            _this.OnFunctionOpen();
        }
           
        //alert("click de la imagen");
    }

    this.Image.Src = this._SrcDown;
    this.Image.Title = UsrCfg.Traslate.GetLangText(_this.Mythis, "Line1");
    this.Image.Cursor = "pointer";
    this.Image.This.style.marginTop = "8px";

    Object.defineProperty(this, 'BackgroundColor', {
        get: function () {
            return this.This[0].This.style.backgroundColor;
        },
        set: function (_Value) {
            this.This[0].This.style.backgroundColor = _Value;
        }
    });

    Object.defineProperty(this, 'BoderTopColor', {
        get: function () {
            return this.This[0].This.style.borderTopColor;
        },
        set: function (_Value) {
            this.This[0].This.style.borderTopColor = _Value;
        }
    });

    Object.defineProperty(this, 'BoderAllColor', {
        get: function () {
            return this.This[0].This.style.borderBottomColor;
        },
        set: function (_Value) {
            this.This[0].This.style.borderBottomColor = _Value
            this.This[0].This.style.borderLeftColor = _Value
            this.This[0].This.style.borderRightColor = _Value
        }
    });

    Object.defineProperty(this, 'SrcUp', {
        get: function () {
            return this._SrcUp;
        },
        set: function (_Value) {
            this._SrcUp = _Value;
        }
    });

    Object.defineProperty(this, 'SrcDown', {
        get: function () {
            return this._SrcDown;
        },
        set: function (_Value) {
            this._SrcDown = _Value;
        }
    });

    Object.defineProperty(this, 'FunctionOpen', {
        get: function () {
            return this._functionOpen;
        },
        set: function (_Value) {
            this._functionOpen = _Value;
        }
    });

    Object.defineProperty(this, 'FunctionClose', {
        get: function () {
            return this._functionClose;
        },
        set: function (_Value) {
            this._functionClose = _Value;
        }
    });

    Object.defineProperty(this, 'MarginTop', {
        get: function () {
            return this.This[0].This.style.marginTop;
        },
        set: function (_Value) {
            this.This[0].This.style.marginTop = _Value;
        }
    });

    Object.defineProperty(this, 'MarginBottom', {
        get: function () {
            return this.This[0].This.style.marginBottom;
        },
        set: function (_Value) {
            this.This[0].This.style.marginBottom = _Value;
        }
    });

    Object.defineProperty(this, 'MarginLeft', {
        get: function () {
            return this.This[0].This.style.marginLeft;
        },
        set: function (_Value) {
            this.This[0].This.style.marginLeft = _Value;
        }
    });

    Object.defineProperty(this, 'Tag', {
        get: function () {
            return this._Tag;
        },
        set: function (_Value) {
            this._Tag = _Value;
        }
    });

    Object.defineProperty(this, 'MarginRight', {
        get: function () {
            return this.This[0].This.style.marginRight;
        },
        set: function (_Value) {
            this.This[0].This.style.marginRight = _Value;
        }
    });

    Object.defineProperty(this, 'Visible', {
        get: function () {
            if (this.This[0].This.style.display == "none")
                return false;
            return true;
        },
        set: function (_Value) {
            if (_Value)
                this.This[0].This.style.display = "";
            else
                this.This[0].This.style.display = "none";
        }
    });

    this.AddToHeader = function (ObjectHtml) {
        $(this.RowHeader.This).append(ObjectHtml);
    }

    this.AddToBody = function (ObjectHtml) {
        $(this.RowBody.This).append(ObjectHtml);
    }

    this.CleanHeader = function () {
        $(this.RowHeader.This).html("");
    }

    this.CleanBody = function () {
        $(this.RowBody.This).html("");
    }

   

}

