﻿var TVclBodyMain = function (ObjectBodyHTML, Id) {
    var _this = this;
    this.FirstLoad = true;
    this.ObjectBody = ObjectBodyHTML;
    this.Wraper = Udiv(this.ObjectBody, "");
    this.MainHeader = Uheader(this.Wraper, "");
    this.MainHeaderLogo = Udiv(this.MainHeader, "");
    this.MainHeaderNav = Unav(this.MainHeader, "");
    this.MainHeaderNavContainer = Udiv(this.MainHeaderNav, "");
    this.MainSideBar = Uaside(this.Wraper, "");
    this.MainSideBarContent = Udiv(this.MainSideBar, "");
    this.MainSideBarContentClient = Udiv(this.MainSideBarContent, "");
    this.MainWraper = Udiv(this.Wraper, "");
    this.MainWraperContent = Udiv(this.MainWraper, "");
    this.MainFooter = Ufooter(this.Wraper, "");
    this.MainFooterContent = Udiv(this.MainFooter, "");
    this.ControlSideBar = Uaside(this.Wraper, "");
    this.ControlSideBarContent = Udiv(this.ControlSideBar, "");
    this.ControlSideBarContentClient = Udiv(this.ControlSideBarContent, "");
    this.ControlSideBarBg = Udiv(this.Wraper, "");

    //this.MainWraperContent.style.paddingLeft = "8px";

    this.MainWraperContent.style.minHeight = $(window).height() + 'px';


    //asignacion de clases 
    this.Wraper.classList.add("wrapper-main");
    this.MainHeader.classList.add("main-header");
    this.MainHeaderLogo.classList.add("logo");
    this.MainHeaderLogo.classList.add("text-center");
    this.MainHeaderNav.classList.add("navbar");
    this.MainHeaderNavContainer.classList.add("container-fluid");
    this.MainSideBar.classList.add("main-sidebar");
    this.MainSideBarContent.classList.add("main-sidebar-content");
    this.MainSideBarContentClient.classList.add("container-fluid");
    this.MainWraper.classList.add("content-wrapper");
    this.MainWraperContent.classList.add("container-fluid");
    //this.MainWraperContent.classList.add("container-fluid2");
    this.MainFooter.classList.add("main-footer");
    this.MainFooterContent.classList.add("container-fluid");
    this.ControlSideBar.classList.add("control-sidebar");
    this.ControlSideBar.classList.add("control-sidebar-dark");
    this.ControlSideBarContent.classList.add("control-sidebar-content");
    this.ControlSideBarContentClient.classList.add("container-fluid");
    this.ControlSideBarBg.classList.add("control-sidebar-bg");

    

    if ($(window).width() > 767) {
        $(this.MainSideBarContent).slimscroll({
            color: '#fff',
            height: ($(window).height() - 50) + 'px'
        });

        $(this.ControlSideBarContent).slimscroll({
            color: '#fff',
            position: 'left',
            height: ($(window).height() - 50) + 'px'
        });
    }


    this.ToggleMainSideBar = function (FromClick) {
        $(this.ControlSideBar).removeClass("control-sidebar-open");
        if (FromClick) {
            if (this.FirstLoad) {
                if ($(window).width() <= 767) {
                    $(this.ObjectBody).removeClass("sidebar-collapsed");
                    $(this.ObjectBody).addClass("sidebar-open");
                }
                else {
                    $(this.ObjectBody).addClass("sidebar-collapsed");
                    $(this.ObjectBody).removeClass("sidebar-open");
                }
                this.FirstLoad = false;
            }
            else {
                if ($(this.ObjectBody).hasClass("sidebar-collapsed")) {
                    $(this.ObjectBody).removeClass("sidebar-collapsed");
                    $(this.ObjectBody).addClass("sidebar-open");
                }
                else {
                    $(this.ObjectBody).addClass("sidebar-collapsed");
                    $(this.ObjectBody).removeClass("sidebar-open");
                }
            }
        }
        else {
            if ($(window).width() <= 767) {
                if ($(this.ObjectBody).hasClass("sidebar-collapsed")) {
                    $(this.ObjectBody).removeClass("sidebar-collapsed");
                    $(this.ObjectBody).addClass("sidebar-open");
                }
                else {
                    $(this.ObjectBody).addClass("sidebar-collapsed");
                    $(this.ObjectBody).removeClass("sidebar-open");
                }
            }
        }


    }

    this.ToggleControlSideBar = function () {
        if ($(window).width() <= 767) {
            $(this.ObjectBody).addClass("sidebar-collapsed");
        }
        if ($(this.ControlSideBar).hasClass("control-sidebar-open")) {
            $(this.ControlSideBar).removeClass("control-sidebar-open");
        }
        else {
            $(this.ControlSideBar).addClass("control-sidebar-open");
        }
    }

    $(window).resize(function () {
        $(_this.ControlSideBar).removeClass("control-sidebar-open");
        if ($(this).width() <= 767) {
            $(_this.MainSideBarContent).slimScroll({ destroy: true }).height('auto');
            $(_this.ControlSideBarContent).slimScroll({ destroy: true }).height('auto');
            $(_this.ObjectBody).addClass("sidebar-collapsed");
            $(_this.ObjectBody).removeClass("sidebar-open");
        }
        else {
            $(_this.MainSideBarContent).slimscroll({
                color: '#fff',
                height: ($(window).height() - 50) + 'px'
            });
            $(_this.ControlSideBarContent).slimscroll({
                color: '#fff',
                position: 'left',
                height: ($(window).height() - 50) + 'px'
            });
            $(_this.ObjectBody).removeClass("sidebar-collapsed");
        }
    });
}