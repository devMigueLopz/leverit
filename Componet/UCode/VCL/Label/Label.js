﻿var TVcllabel = function (ObjectHtml, Id, labelType) {
    this.This;
    this._VCLType;

    switch (labelType) {
        case TlabelType.H0:
            this.This = Ulabel(ObjectHtml, Id, "");
                   
            break;
        case TlabelType.H1:
            this.This = Uh1(ObjectHtml, Id, "");
            break;
        case TlabelType.H2:
            this.This = Uh2(ObjectHtml, Id, "");
            break;
        case TlabelType.H3:
            this.This = Uh3(ObjectHtml, Id, "");
            break;
        case TlabelType.H4:
            this.This = Uh4(ObjectHtml, Id, "");
            break;
        case TlabelType.H5:
            this.This = Uh5(ObjectHtml, Id, "");
            break;
        case TlabelType.H6:
            this.This = Uh6(ObjectHtml, Id, "");
            break;
        default:
    }

    //#region propiedades que alteran los atributos del elemento html    
    Object.defineProperty(this, 'Text', {
        get: function () {
            return $(this.This).html();
        },
        set: function (_Value) {
            $(this.This).html(_Value);
        }
    });

    Object.defineProperty(this, 'Enabled', {
        get: function () {
            return !this.This.disabled;
        },
        set: function (_Value) {
            this.This.disabled = !_Value;
        }
    });

    //didijoca
    Object.defineProperty(this, 'Visible', {
        get: function () {
            if (this.This.style.visibility == "hidden" || this.This.style.visibility == "collapse")
                return false;
            return true;
        },
        set: function (_Value) {
            if (_Value)
                this.This.style.visibility = "visible";
            else
                this.This.style.visibility = "hidden";
        }
    });

    Object.defineProperty(this, 'onClick', {
        get: function () {
            return this.This.onclick;
        },
        set: function (_Value) {
            this.This.onclick = _Value;
        }
    });

    Object.defineProperty(this, 'VCLType', {
        get: function () {
            return this._VCLType;
        },
        set: function (_Value) {
            this._VCLType = _Value;
            switch (labelType) {
                case TlabelType.H0:
                    switch (_Value) {
                        case TVCLType.AG:
                            break;
                        case TVCLType.BS:
                            BootStrapCreateLabel(this.This);
                            break;
                        case TVCLType.DV:
                            break;
                        case TVCLType.KD:
                            break;
                        default:
                    }
                    
                default:
            }           
        }
    });
    //#endregion
    this.This.style.fontWeight = "normal";
    var TParent = function () {
        return this;
    }.bind(this);
}

var TlabelType = {
    H0: { value: 0, name: "Default"},
    H1: { value: 1, name: "1" },
    H2: { value: 2, name: "2" },
    H3: { value: 3, name: "3" },
    H4: { value: 4, name: "4" },
    H5: { value: 5, name: "5" },
    H6: { value: 6, name: "6" }
};





/*<summary>
Midifico fecha   
hans     24-04-2017
Funcion: Genera un elemento html de tipo texto header con escala de letra 6
ejemplos:
Vcllabel(Elementhtm, "Id_del_elemento", "texto_del_elemento");
</summary>
<param name="Object">Elemento html al cual se le insertara el elemento creado por esta funcion</param>
<param name="id">Id de elemento creado</param>
<param name="text">texto del elemento</param>
<returns>retorna un elemento de tipo texto header</returns>*/
function Vcllabel(Object, id, VCLType, labelType, text) {
    var Res = undefined;
    switch (labelType) {
        case TlabelType.H0:
            Res = Ulabel(Object, id, text);
            switch (VCLType) {
                case TVCLType.AG:
                    break;
                case TVCLType.BS:
                    BootStrapCreateLabel(Res);
                    break;
                case TVCLType.DV:
                    break;
                case TVCLType.KD:
                    break;
                default:
            }
            break;
        case TlabelType.H1:
            Res = Uh1(Object, id, text);
            break;
        case TlabelType.H2:
            Res = Uh2(Object, id, text);
            break;
        case TlabelType.H3:
            Res = Uh3(Object, id, text);
            break;
        case TlabelType.H4:
            Res = Uh4(Object, id, text);
            break;
        case TlabelType.H5:
            Res = Uh5(Object, id, text);
            break;
        case TlabelType.H6:
            Res = Uh6(Object, id, text);
            break;
        default:
    }
    Res.style.fontWeight = "normal";
    return Res;
}


