﻿var TVclItemTopBar = function () {
    var TParent = function () {
        return this;
    }.bind(this);

    this.This = Udiv(null, "");
    this.This.style.marginRight = "20px";

    this._Imagen = null;
    this._Label = null;

    Object.defineProperty(this, 'Imagen', {//"0px"
        get: function () {
            return this._Imagen;
        },
        set: function (_Value) {
            this._Imagen = _Value;
            $(this.This).append(_Value.This);
        }
    });

    Object.defineProperty(this, 'Label', {//"0px"
        get: function () {
            return this._Label;
        },
        set: function (_Value) {
            this._Label = _Value;
            this._Label.This.style.marginLeft = "3px";
            $(this.This).append(this._Label.This);
        }
    });

    Object.defineProperty(this, 'Float', {//"0px"
        get: function () {
            return this.This.style.float;
        },
        set: function (_Value) {
            this.This.style.float = _Value;
        }
    });
}

var TVclTopBar = function (ObjectHtml, AsideIzquierda, AsideDerecha, ObjectoContainer, ObjectBody) {
    var TParent = function () {
        return this;
    }.bind(this);

    AsideIzquierda.style.display = "block";
    AsideIzquierda.style.overflowY = "auto";
    //AsideIzquierda.style.minHeight = "100%";
    AsideIzquierda.style.maxHeight = "95%";

    AsideDerecha.style.display = "none";
    ObjectoContainer.classList.add("wheel_invisible");


    ObjectoContainer.style.float = "left";
    AsideDerecha.style.float = "left";


    var Cont_idmain_0 = ObjectBody.ContenedorGeneral;
    var Cont2_idmain_0 = Cont_idmain_0.children.Cont2_idmain_0;
    var Cont2m_idmain_0 = Cont2_idmain_0.children.Cont2m_idmain_0;

    Cont_idmain_0.classList.add("row-eq-height");
    Cont_idmain_0.style.minHeight = "100%";

    Cont2_idmain_0.style.minHeight = "100%";

    Cont2m_idmain_0.style.minHeight = "780px";
    Cont2m_idmain_0.classList.add("row-eq-height");


    AsideDerecha.style.minHeight = "780px";
    AsideDerecha.style.float = "left";
    AsideDerecha.style.display = "none";
    AsideDerecha.style.position = "absolute";
    AsideDerecha.style.zIndex = "214748";
    AsideDerecha.style.minHeight = "780px";

    this.This = null;
    this._Logo = null;
    this.DivLeft = null;
    this.DivRight = null;

    var ArrayEstruc = new Array(1);
    ArrayEstruc[0] = new Array(2); //cols
    ArrayEstruc[0][1] = new Array(1); //row
    ArrayEstruc[0][1][0] = new Array(2); //cols
    ArrayEstruc[0][1][0][1] = new Array(1); //row
    ArrayEstruc[0][1][0][1][0] = new Array(1); //col

    var arrObjCont = VclDivitions(ObjectHtml, "TopBar_", ArrayEstruc);

    this.This = arrObjCont[0].This;

    BootStrapCreateRow(arrObjCont[0].This);
    //arrObjCont[0].This.style.backgroundColor = "#3e94ba";
    arrObjCont[0].This.classList.add("mainSource_Header");

    BootStrapCreateColumns(arrObjCont[0].Child, [[12, 12], [3, 9], [2, 10], [2, 10], [2, 10]]);
    arrObjCont[0].Child[0].This.id = "colIzquierda";
    arrObjCont[0].Child[1].This.id = "colDerecha";

    BootStrapCreateRow(arrObjCont[0].Child[1].Child[0].This);
    arrObjCont[0].Child[1].Child[0].This.style.marginTop = "15px";
    arrObjCont[0].Child[1].Child[0].This.style.minHeight = "30px";

    BootStrapCreateColumns(arrObjCont[0].Child[1].Child[0].Child, [[2, 10], [1, 11], [1, 11], [1, 11], [1, 11]]);

    BootStrapCreateRow(arrObjCont[0].Child[1].Child[0].Child[1].Child[0].This);
    arrObjCont[0].Child[1].Child[0].Child[1].Child[0].This.style.color = "white";
    arrObjCont[0].Child[1].Child[0].Child[1].Child[0].This.style.float = "right !important";

    BootStrapCreateColumn(arrObjCont[0].Child[1].Child[0].Child[1].Child[0].Child[0].This, [12, 12, 12, 12, 12]);

    $(ObjectHtml).append(arrObjCont[0].This);

    //arrObjCont[0].Child[0].This.style.backgroundColor = "#2d83a9";
    arrObjCont[0].Child[0].This.classList.add("mainSource_LogoMain");

    var oCenter = Ucenter(arrObjCont[0].Child[0].This);

    this.DivLeft = arrObjCont[0].Child[1].Child[0].Child[0].This;
    this.DivRight = arrObjCont[0].Child[1].Child[0].Child[1].Child[0].Child[0].This;

    Object.defineProperty(this, 'Logo', {//"0px"
        get: function () {
            return this._Logo;
        },
        set: function (_Value) {
            this._Logo = _Value;
            $(oCenter).append(this._Logo.This);
            this._Logo.This.style.maxWidth = "210px";
            this._Logo.This.classList.add("img-responsive");
        }
    });

    this.AddItemLeft = function (_TVclItemTopBar) {
        $(this.DivLeft).append(_TVclItemTopBar.This);
    }

    this.AddItemRight = function (_TVclItemTopBar) {
        $(this.DivRight).append(_TVclItemTopBar.This);
    }

    $(ObjectoContainer).bind('DOMNodeInserted DOMNodeRemoved', function (event) {
        if (event.type == 'DOMNodeInserted') {
            //AsideDerecha.style.display = "none";
            //$(ObjectoContainer).removeClass("wheel_visible");
            //$(ObjectoContainer).removeClass("wheel_invisible");

            //AsideIzquierda.style.display = "";
            //$("#Cont2_idmain_0").removeClass("burger_visible");
            //$("#Cont2_idmain_0").removeClass("burger_invisible");
        }
    });

    $(window).on("resize", function () {

        AsideDerecha.style.display = "none";
        AsideDerecha.style.right = "-250px";
        ObjectoContainer.classList.remove("wheel_visible");
        ObjectoContainer.classList.add("wheel_invisible");
        //$(ObjectoContainer).removeClass("wheel_visible");
        //$(ObjectoContainer).addClass("wheel_invisible");

        AsideIzquierda.style.display = "";
        Cont2_idmain_0.classList.remove("burger_visible");
        Cont2_idmain_0.classList.remove("burger_invisible");
        //$(Cont2_idmain_0).removeClass("burger_visible");
        //$(Cont2_idmain_0).removeClass("burger_invisible");
    })

    //creo la imagen de la burger
    var BurguerMenu = new TVclImagen(null, "");
    BurguerMenu.Text = "";
    BurguerMenu.Src = "image/imgMono/24/database.png";
    BurguerMenu.Cursor = "pointer";
    BurguerMenu.onClick = function () {
        if (AsideIzquierda.style.display == "none" || AsideIzquierda.style.display == "") {
            AsideIzquierda.style.display = "block";
            Cont2_idmain_0.classList.remove("burger_invisible");
            Cont2_idmain_0.classList.add("burger_visible");
            //$("#Cont2_idmain_0").removeClass("burger_invisible");
            //$("#Cont2_idmain_0").addClass("burger_visible");
        }
        else {
            AsideIzquierda.style.display = "none";
            Cont2_idmain_0.classList.remove("burger_visible");
            Cont2_idmain_0.classList.add("burger_invisible");
            //$("#Cont2_idmain_0").removeClass("burger_visible");
            //$("#Cont2_idmain_0").addClass("burger_invisible");            
        }
    }

    //ITHelpCenter.MainBody.BtnBurgerMenu = BurguerMenu;

    //creo el item de la barra
    var VclItemTopBar_Burguer = new TVclItemTopBar();
    VclItemTopBar_Burguer.Imagen = BurguerMenu;

    //agrego item en la parte derecha de la barra
    this.AddItemLeft(VclItemTopBar_Burguer);

    //agrego el item de settings menu en la parte derecha
    var SettingsMenu = new TVclImagen(null, "");
    SettingsMenu.Text = "";
    SettingsMenu.Src = "image/imgMono/24/wheels.png";
    SettingsMenu.Cursor = "pointer";
    SettingsMenu.onClick = function () {
        if (AsideDerecha.style.display == "none" || AsideDerecha.style.display == "") {

            AsideDerecha.style.display = "block";
            AsideDerecha.style.right = "0px";
            ObjectoContainer.classList.remove("wheel_invisible");
            ObjectoContainer.classList.add("wheel_visible");
        }
        else {
            AsideDerecha.style.display = "none";
            ObjectoContainer.classList.remove("wheel_visible");
            ObjectoContainer.classList.add("wheel_invisible");
        }
    }

    //creo item settings 
    var VclItemTopBar_settings = new TVclItemTopBar();
    VclItemTopBar_settings.Imagen = SettingsMenu;
    VclItemTopBar_settings.Float = "right";
    VclItemTopBar_settings.This.classList.add("v-p6");
    VclItemTopBar_settings.This.classList.add("v-p5");
    VclItemTopBar_settings.This.classList.add("v-p4");
    VclItemTopBar_settings.This.classList.add("v-p3");
    VclItemTopBar_settings.This.classList.add("v-p2");
    VclItemTopBar_settings.This.classList.add("h-p1");

    //lo agrego a la parte derecha de la barra
    this.AddItemRight(VclItemTopBar_settings);
    //_this.Object_asideright.style.display = 'none';
    //AsideDerecha.style.display = 'none';
}

var TVclMainTopBar = function (ObjectMainBody) {
    var TParent = function () {
        return this;
    }.bind(this);

    var _this = this;

    this.MainBody = ObjectMainBody;

    this.This = null;
    this._Logo = null;
    this.DivLeft = null;
    this.DivRight = null;

    var spMainTopBar = new TVclStackPanel(this.MainBody.MainHeaderNavContainer, "", 2, [[4, 8], [4, 8], [2, 10], [2, 10], [2, 10]]);
    this.This = spMainTopBar.Row;

    var spMainTopBarRight = new TVclStackPanel(spMainTopBar.Column[1].This, "", 1, [[12], [12], [12], [12], [12]]);
    spMainTopBarRight.Row.This.style.color = "white";
    spMainTopBarRight.Column[0].This.classList.add("margin-small");

    this.DivLeft = spMainTopBar.Column[0].This;
    this.DivRight = spMainTopBarRight.Column[0].This;

    Object.defineProperty(this, 'Logo', {//"0px"
        get: function () {
            return this._Logo;
        },
        set: function (_Value) {
            this._Logo = _Value;
            $(this.MainBody.MainHeaderLogo).append(this._Logo.This);
            this._Logo.This.style.maxWidth = "210px";
            this._Logo.This.style.maxHeight = "50px";
        }
    });

    this.AddItemLeft = function (_TVclItemTopBar) {
        $(this.DivLeft).append(_TVclItemTopBar.This);
    }

    this.AddItemRight = function (_TVclItemTopBar) {
        $(this.DivRight).append(_TVclItemTopBar.This);
    }

    //creo la imagen logo
    var imgLogo = new TVclImagen(null, "");
    imgLogo.Text = "";
    imgLogo.Src = "image/High/logoITHC36.png";
    imgLogo.This.classList.add("mini-logo");

    $(this.DivLeft).append(imgLogo.This);


    //creo la imagen de la burger
    var BurguerMenu = new TVclImagen(null, "");
    BurguerMenu.Text = "";
    BurguerMenu.Src = "image/imgMono/24/database.png";
    BurguerMenu.Cursor = "pointer";
    BurguerMenu.onClick = function () {
        _this.MainBody.ToggleMainSideBar(true);
    }

    //creo el item de la barra
    var VclItemTopBar_Burguer = new TVclMainItemTopBar();
    VclItemTopBar_Burguer.Imagen = BurguerMenu;

    //agrego item en la parte derecha de la barra
    this.AddItemLeft(VclItemTopBar_Burguer);

    //agrego el item de settings menu en la parte derecha
    var SettingsMenu = new TVclImagen(null, "");
    SettingsMenu.Text = "";
    SettingsMenu.Src = "image/imgMono/24/wheels.png";
    SettingsMenu.Cursor = "pointer";
    SettingsMenu.onClick = function () {
        _this.MainBody.ToggleControlSideBar();
    }

    //creo item settings 
    var VclItemTopBar_settings = new TVclMainItemTopBar();
    VclItemTopBar_settings.Imagen = SettingsMenu;
    VclItemTopBar_settings.Float = "right";

    //lo agrego a la parte derecha de la barra
    this.AddItemRight(VclItemTopBar_settings);
}

var TVclMainItemTopBar = function () {
    var TParent = function () {
        return this;
    }.bind(this);

    this.This = Udiv(null, "");
    this.This.classList.add("icon-navbar");

    this._Imagen = null;
    this._Label = null;
    this._Tooltip = null;

    Object.defineProperty(this, 'Imagen', {//"0px"
        get: function () {
            return this._Imagen;
        },
        set: function (_Value) {
            this._Imagen = _Value;
            $(this.This).append(_Value.This);
        }
    });

    Object.defineProperty(this, 'Label', {//"0px"
        get: function () {
            return this._Label;
        },
        set: function (_Value) {
            this._Label = _Value;
            this._Label.This.style.marginLeft = "3px";
            $(this.This).append(this._Label.This);
        }
    });

    Object.defineProperty(this, 'Float', {//"0px"
        get: function () {
            return this.This.style.float;
        },
        set: function (_Value) {
            this.This.style.float = _Value;
        }
    });

    Object.defineProperty(this, 'Tooltip', {
        get: function () {
            return this._Tooltip;
        },
        set: function (_Value) {
            this._Tooltip = _Value;
            $(this.This).append(this.Tooltip.This);
            $(this.This).addClass("Divtooltip");
            $(this.Tooltip.This).addClass("tooltiptext");
        }
    });
}