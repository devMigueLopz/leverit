Componet.TreeView.TTreeMenuView = function (inObject, id, incallback) {
    this.TParent = function () {
        return this;
    }.bind(this);
    this.ObjectHtml = inObject;
    this.Id = id;
    this.CallbackModalResult = incallback;
    this.NavMenu = new Object();
    this.GroupNavMenu = new Array();
    this.ListBox = new Array();
    this.ListData = new Array();
    this.ListDataChild = new Array();
}
Componet.TreeView.TTreeMenuView.prototype.Load = function (listObjData) {
    var _this = this.TParent();
    try {
        _this.LoadData(listObjData);
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("TreeView.js Componet.ListView.TListView.prototype.Load", e);
    }
}
Componet.TreeView.TTreeMenuView.prototype.LoadData = function (listObjData) {
    var _this = this.TParent();
    try {
        var navMenuInit = new ItHelpCenter.Componet.NavTabControls.TNavMenu(_this.ObjectHtml);
        _this.NavMenu = navMenuInit;
        for (var i = 0; i < listObjData.length; i++) {
            var navGroupMenu = new ItHelpCenter.Componet.NavTabControls.TNavGroupMenu(navMenuInit);
            _this.GroupNavMenu.push(navGroupMenu);
            navGroupMenu.Id = listObjData[i].Id;
            navGroupMenu.TypeNavBar = "P";
            navGroupMenu.Text = listObjData[i].Text;
            navGroupMenu.SrcImg = listObjData[i].Image;
            navGroupMenu.Tag = listObjData[i].Tag;
            navGroupMenu.BackgroundColor = listObjData[i].Background;
            navGroupMenu.Color = listObjData[i].Color;
            var boxChildNavGroupMenu = new Udiv("", "BoxChildNavGroupMenu " + "_" + _this.Id);
            var listBox = new Componet.ListBox.TTreeListBox(boxChildNavGroupMenu, "ListBox " + "_" + _this.Id);
            listBox.Id = listObjData[i].Id;
            listBox.Tag = listObjData[i].Tag;
            _this.ListBox.push(listBox);
            listBox.EnabledCheckBox = true;
            listObjData[i].ListBoxChild = listBox;
            for (var x = 0; x < listObjData[i].Child.length; x++) {
                var listBoxItem = new Componet.ListBox.TTreeListBox.TVcTreeListBoxItem();
                listBoxItem.Id = listObjData[i].Child[x].Id;
                listBoxItem.Text = listObjData[i].Child[x].Text;
                listBoxItem.Image = listObjData[i].Child[x].Image;
                listBoxItem.Index = listObjData[i].Child[x].Index;
                listBoxItem.Tag = listObjData[i].Child[x].Tag;
                listBoxItem.Parent = listObjData[i];
                listBox.AddListBoxItem(listBoxItem);
                _this.ListDataChild.push(listObjData[i].Child[x])
            }
            var navItem = new ItHelpCenter.Componet.NavTabControls.TNavItemMenu(boxChildNavGroupMenu);
            navGroupMenu.AddNavItemMenuPanel(navItem);
        }
        _this.ListData = listObjData;
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("TreeView.js Componet.TreeView.TTreeView.prototype.LoadData", e);
    }
}
Componet.TreeView.TTreeMenuView.prototype.GetBoxById = function (id) {
    var _this = this.TParent();
    try {
        for (var i = 0; i < _this.ListBox.length; i++) {
            if (_this.ListBox[i].Id === id) {
                return (_this.ListBox[i]);
            }
        }
        return (null);
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("TreeView.js Componet.TreeView.TTreeView.prototype.GetBoxById", e);
    }
}
Componet.TreeView.TTreeMenuView.prototype.AddNavMenu = function (objData) {
    var _this = this.TParent();
    var success = false;
    try {
        if (_this._ValidateFullNavMenu(objData)) {
            var newListData = _this.ListData;
            newListData.push(objData);
            _this._ClearObject();
            _this.ListData = newListData;
            _this.Load(_this.ListData);
            var success = true;
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("TreeView.js Componet.TreeView.TTreeView.prototype.AddNavMenu", e);
    }
    return (success);
}
Componet.TreeView.TTreeMenuView.prototype.GetNavMenu = function (id) {
    var _this = this.TParent();
    var element = null;
    try {
        if (!isNaN(id)) {
            for (var i = 0; i < _this.ListData.length; i++) {
                if (_this.ListData[i].Id == id) {
                    element = _this._ConstructObjectNavMenu(_this.ListData[i]);
                }
            }
        }

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("TreeView.js Componet.TreeView.TTreeView.prototype.GetNavMenu", e);
    }
    return (element);
}
Componet.TreeView.TTreeMenuView.prototype.UpdateNavMenu = function (objData) {
    var _this = this.TParent();
    var success = false;
    try {
        if (_this._ValidateFullNavMenu(objData)) {
            for (var i = 0; i < _this.ListData.length; i++) {
                if (_this.ListData[i].Id == objData.Id) {
                    _this.ListData[i].Id = objData.Id;
                    _this.ListData[i].Text = objData.Text;
                    _this.ListData[i].Tag = objData.Tag;
                    _this.ListData[i].Image = objData.Image;
                    _this.ListData[i].Background = objData.Background;
                    _this.ListData[i].Color = objData.Color;
                    _this.ListData[i].Child = objData.Child;
                    success = true;
                }
            }
            if (success) {
                var newListData = _this.ListData;
                _this._ClearObject();
                _this.ListData = newListData;
                _this.Load(_this.ListData);
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("TreeView.js Componet.TreeView.TTreeView.prototype.UpdateNavMenu", e);
    }
    return (success);
}
Componet.TreeView.TTreeMenuView.prototype.DeleteNavMenu = function (id) {
    var _this = this.TParent();
    var newElements = new Array();
    try {
        if (!isNaN(id)) {
            for (var i = 0; i < _this.ListData.length; i++) {
                if (_this.ListData[i].Id != id) {
                    newElements.push(_this.ListData[i]);
                }
            }
            _this._ClearObject();
            _this.ListData = newElements;
            _this.Load(_this.ListData);
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("TreeView.js Componet.TreeView.TTreeView.prototype.DeleteNavMenu", e);
    }
    return (newElements);
}
Componet.TreeView.TTreeMenuView.prototype.AddItemNavMenu = function (objData) {
    var _this = this.TParent();
    var success = false;
    try {
        if (_this._ValidateItemNavMenu(objData)) {
            for (var i = 0; i < _this.ListData.length; i++) {
                if (_this.ListData[i].Id == objData.Parent.Id) {
                    _this.ListData[i].Child.push(objData);
                    var success = true;
                }
            }
            if (success) {
                var newListData = _this.ListData;
                _this._ClearObject();
                _this.ListData = newListData;
                _this.Load(_this.ListData);
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("TreeView.js Componet.TreeView.TTreeView.prototype.AddItemNavMenu", e);
    }
    return (success);
}
Componet.TreeView.TTreeMenuView.prototype.GetItemNavMenu = function (id) {
    var _this = this.TParent();
    var element = null;
    try {
        if (!isNaN(id)) {
            for (var i = 0; i < _this.ListDataChild.length; i++) {
                if (_this.ListDataChild[i].Id == id) {
                    element = _this._ConstructObjectItemNavMenu(_this.ListDataChild[i]);
                }
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("TreeView.js Componet.TreeView.TTreeView.prototype.GetItemNavMenu", e);
    }
    return (element);
}
Componet.TreeView.TTreeMenuView.prototype.UpdateItemNavMenu = function (objData) {
    var _this = this.TParent();
    var success = false;
    try {
        if (_this._ValidateEditItemNavMenu(objData)) {
            for (var i = 0; i < _this.ListData.length; i++) {
                for (var x = 0; x < _this.ListData[i].Child.length; x++) {
                    if (_this.ListData[i].Child[x].Id == objData.Id) {
                        _this.ListData[i].Child[x].Id = objData.Id;
                        _this.ListData[i].Child[x].Index = objData.Index;
                        _this.ListData[i].Child[x].Tag = objData.Tag;
                        _this.ListData[i].Child[x].Text = objData.Text;
                        _this.ListData[i].Child[x].Image = objData.Image;
                        success = true;
                    }
                }
            }
            if (success) {
                var newListData = _this.ListData;
                _this._ClearObject();
                _this.ListData = newListData;
                _this.Load(_this.ListData);
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("TreeView.js Componet.TreeView.TTreeView.prototype.UpdateItemNavMenu", e);
    }
    return (success);
}
Componet.TreeView.TTreeMenuView.prototype.DeleteItemNavMenu = function (id) {
    var _this = this.TParent();
    var sucess = false;
    var elements = new Array();
    try {
        if (!isNaN(id)) {
            for (var i = 0; i < _this.ListData.length; i++) {
                var newNavMenu = new Array();
                for (var x = 0; x < _this.ListData[i].Child.length; x++) {
                    if (_this.ListData[i].Child[x].Id != id) {
                        newNavMenu.push(_this.ListData[i].Child[x])
                    }
                    else {
                        sucess = true;
                    }
                }
                _this.ListData[i].Child=newNavMenu;
            }
            elements = _this.ListData;
            if (sucess) {
                _this._ClearObject();
                _this.Load(elements);
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("TreeView.js Componet.TreeView.TTreeView.prototype.DeleteItemNavMenu", e);
    }
    return (elements);
}
Componet.TreeView.TTreeMenuView.prototype._ConstructObjectNavMenu = function (objFullMenu) {
    var _this = this.TParent();
    var element = null;
    try {
        var element = new _this.View();
        element.Id = objFullMenu.Id;
        element.Text = objFullMenu.Text;
        element.Tag = objFullMenu.Tag;
        element.Image = objFullMenu.Image;
        element.Background = objFullMenu.Background;
        element.Color = objFullMenu.Color;
        for (var i = 0; i < objFullMenu.Child.length; i++) {
            var child = new _this.ItemView();
            child.Id = objFullMenu.Child[i].Id;
            child.Index = objFullMenu.Child[i].Index;
            child.Tag = objFullMenu.Child[i].Tag;
            child.Text = objFullMenu.Child[i].Text;
            child.Image = objFullMenu.Child[i].Image;
            child.Parent = objFullMenu.Child[i].Parent;
            element.Child.push(child);
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("TreeView.js Componet.TreeView.TTreeView.prototype.ConstructObjectNavMenu", e);
    }
    return (element);
}
Componet.TreeView.TTreeMenuView.prototype._ValidateFullNavMenu = function (objData) {
    var _this = this.TParent();
    var success = false;
    try {
        if (typeof objData === 'object') {
            if (!SysCfg.Str.Methods.IsNullOrEmpity(objData.Id) && !SysCfg.Str.Methods.IsNullOrEmpity(objData.Text) && !SysCfg.Str.Methods.IsNullOrEmpity(objData.Image)) {
                if (!isNaN(objData.Id)) {
                    if (Array.isArray(objData.Child)) {
                        for (var i = 0; i < objData.Child.length; i++) {
                            if (!SysCfg.Str.Methods.IsNullOrEmpity(objData.Child[i].Id) && !SysCfg.Str.Methods.IsNullOrEmpity(objData.Child[i].Index) && !SysCfg.Str.Methods.IsNullOrEmpity(objData.Child[i].Text) && !SysCfg.Str.Methods.IsNullOrEmpity(objData.Child[i].Image)) {
                                if (!isNaN(objData.Child[i].Id) && !isNaN(objData.Child[i].Index)) {
                                    success = true;;
                                }
                                else {
                                    success = false;
                                }
                            }
                            else {
                                success = false;
                            }
                        }
                    }
                    else {
                        success = false;
                    }
                }
                else {
                    success = false;
                }
            }
            else {
                success = false;
            }
        }
        else {
            success = false;
        }

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("TreeView.js Componet.TreeView.TTreeView.prototype._ValidateFullNavMenu", e);
    }
    return (success)
}
Componet.TreeView.TTreeMenuView.prototype._ConstructObjectItemNavMenu = function (itemMenu) {
    var _this = this.TParent();
    var element = null;
    try {
        element = new _this.ItemView();
        element.Id = itemMenu.Id;
        element.Index = itemMenu.Index;
        element.Tag = itemMenu.Tag;
        element.Text = itemMenu.Text;
        element.Image = itemMenu.Image;
        element.Parent = itemMenu.Parent;
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("TreeView.js Componet.TreeView.TTreeView.prototype._ConstructObjectItemNavMenu", e);
    }
    return (element);
}
Componet.TreeView.TTreeMenuView.prototype._ValidateItemNavMenu = function (objData) {
    var _this = this.TParent();
    var success = false;
    try {
        if (typeof objData.Parent === 'object') {
            if (!SysCfg.Str.Methods.IsNullOrEmpity(objData.Parent.Id)) {
                if (!isNaN(objData.Parent.Id)) {
                    if (!SysCfg.Str.Methods.IsNullOrEmpity(objData.Id) && !SysCfg.Str.Methods.IsNullOrEmpity(objData.Index) && !SysCfg.Str.Methods.IsNullOrEmpity(objData.Text) && !SysCfg.Str.Methods.IsNullOrEmpity(objData.Image)) {
                        if (!isNaN(objData.Id) && !isNaN(objData.Index)) {
                            success = true;
                        }
                    }
                }
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("TreeView.js Componet.TreeView.TTreeView.prototype._ValidateItemNavMenu", e);
    }
    return (success)
}
Componet.TreeView.TTreeMenuView.prototype._ValidateEditItemNavMenu = function (objData) {
    var _this = this.TParent();
    var success = false;
    try {
        if (!SysCfg.Str.Methods.IsNullOrEmpity(objData.Id) && !SysCfg.Str.Methods.IsNullOrEmpity(objData.Index) && !SysCfg.Str.Methods.IsNullOrEmpity(objData.Text) && !SysCfg.Str.Methods.IsNullOrEmpity(objData.Image)) {
            if (!isNaN(objData.Id) && !isNaN(objData.Index)) {
                success = true;
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("TreeView.js Componet.TreeView.TTreeView.prototype._ValidateItemNavMenu", e);
    }
    return (success)
}
Componet.TreeView.TTreeMenuView.prototype._ClearObject = function (objData) {
    var _this = this.TParent();
    try {
        $(_this.ObjectHtml).html("");
        _this.NavMenu = new Object();
        _this.GroupNavMenu = new Array();
        _this.ListBox = new Array();
        _this.ListData = new Array();
        _this.ListDataChild = new Array();
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("TreeView.js Componet.TreeView.TTreeView.prototype.ClearObject", e);
    }
}
Componet.TreeView.TTreeMenuView.prototype.ListItemsCheck = function (id) {
    var _this = this.TParent();
    var elementsCheck = new Array();
    var items = new Array();
    try {
        if (!isNaN(id)) {
            for (var i = 0; i < _this.ListData.length; i++) {
                if (_this.ListData[i].Id == id) {
                    items = _this.ListData[i].ListBoxChild.GetListBoxItemChecked();
                }
            }
            for (var x = 0; x < items.length; x++) {
                elementsCheck.push(_this.GetItemNavMenu(items[x].Id));
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("TreeView.js Componet.TreeView.TTreeView.prototype.ListItemsCheck", e);
    }
    return (elementsCheck);
}
Componet.TreeView.TTreeMenuView.prototype.View = function () {
    this.Id = 0;
    this.Text = "";
    this.Tag = "";
    this.Image = "";
    this.Background = "";
    this.Color = "";
    this.ListBoxChild = new Object();
    this.Child = new Array();
}
Componet.TreeView.TTreeMenuView.prototype.ItemView = function () {
    this.Id = 0;
    this.Index = 0;
    this.Tag = "";
    this.Text = "";
    this.Image = "";
    this.Parent = "";
}