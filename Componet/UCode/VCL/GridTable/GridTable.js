﻿var TVclGridTable = function (ObjectHtml, id, DataTable, EventClickRow) {
    this.This;
    this.This = Utable(ObjectHtml, id);
    var TableHead = Uthead(this.This, "");
    var TablaHeadRow = Utr(TableHead, "");

    for (var i = 0; i < DataTable.Columnas.length; i++) {
        var TableHeadCol = Uth(TablaHeadRow, "");
        $(TableHeadCol).append(DataTable.Columnas[i].NombreColumna);
    }

    var TableBody = Utbody(this.This, "");
    for (var i = 0; i < DataTable.Filas.length; i++) {
        var TablaBodyRow = Utr(TableBody, "");
        TablaBodyRow.onclick = EventClickRow;
        for (var j = 0; j < DataTable.Filas[i].Celdas.length; j++) {
            var TableCel = Utd(TablaBodyRow, "");
            $(TableCel).append(DataTable.Filas[i].Celdas[j].Valor);
        }
    }
}

var TVclGridTableHeader = function (ObjectHtml, id, DataTable, EventClickRow) {
    this.This;
    this.This = Utable(ObjectHtml, id);
    var TableHead = Uthead(this.This, "");
    var TablaHeadRow = Utr(TableHead, "");

    for (var i = 0; i < DataTable.Columnas.length; i++) {
        var TableHeadCol = Uth(TablaHeadRow, "");
        $(TableHeadCol).append(DataTable.Columnas[i].Header);
    }

    var TableBody = Utbody(this.This, "");
    for (var i = 0; i < DataTable.Filas.length; i++) {
        var TablaBodyRow = Utr(TableBody, "");
        TablaBodyRow.onclick = EventClickRow;
        for (var j = 0; j < DataTable.Filas[i].Celdas.length; j++) {
            var TableCel = Utd(TablaBodyRow, "");
            $(TableCel).append(DataTable.Filas[i].Celdas[j].Valor);
        }
    }
}

function VclGridTable(Object, id, DataTable, EventClickRow) {
    var Res = new TVclGridTable(Object, id, DataTable, EventClickRow); 

    //Res.This = Utable(Object, id);
    //var TableHead = Uthead(Res.This, "");
    //var TablaHeadRow = Utr(TableHead, "");

    //for (var i = 0; i < DataTable.Columnas.length; i++) {
    //    var TableHeadCol = Uth(TablaHeadRow, "");
    //    $(TableHeadCol).append(DataTable.Columnas[i].NombreColumna);
    //}

    //var TableBody = Utbody(Res.This, "");
    //for (var i = 0; i < DataTable.Filas.length; i++) {
    //    var TablaBodyRow = Utr(TableBody, "");
    //    TablaBodyRow.onclick = EventClickRow;
    //    for (var j = 0; j < DataTable.Filas[i].Celdas.length; j++) {
    //        var TableCel = Utd(TablaBodyRow, "");
    //        $(TableCel).append(DataTable.Filas[i].Celdas[j].Valor);
    //    }
    //}

    return Res.This;
}

function VclGridTableHeader(Object, id, DataTable, EventClickRow) {
    var Res = new TVclGridTableHeader(Object, id, DataTable, EventClickRow);
    return Res.This;
}


function VclUnionTables(DataTable1, Datatable2) {
  
    var nCol = DataTable1.Columnas.length;
    //Columnas
    for (var i = 0; i < Datatable2.Columnas.length; i++) {
        var dataArray = {
            'NombreColumna': Datatable2.Columnas[i].NombreColumna, 'IDCMDBCIDEFINEEXTRAFIELDS': 0, 'Tipo': 0
        };
        DataTable1.Columnas[nCol+i] = dataArray;
    }
 
    //Filas
    try {
        for (var i = 0; i < DataTable1.Filas.length; i++) {
            for (var j = 0; j < Datatable2.Filas.length; j++) {
                var dataArray = {
                    'Valor': Datatable2.Filas[i].Celdas[j].Valor
                };
                DataTable1.Filas[i].Celdas[nCol + j] = dataArray;
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("GridTable.js VclUnionTables", e);
    }
    return DataTable1
}

function VclAddColumn(DataTable, NombreColumn, Header) {
    var dataArray = {
        'NombreColumna': NombreColumn, 'IDCMDBCIDEFINEEXTRAFIELDS': 0, 'Tipo': 0, 'Header': Header
    };
    var ncol = DataTable.Columnas.length;
    DataTable.Columnas[ncol] = dataArray;
    for (var i = 0; i < DataTable.Filas.length; i++) {
        var dataArray = {
            'Valor': " "
        };
        DataTable.Filas[i].Celdas[ncol] = dataArray;
    }
    return DataTable;
}

//Jaimes 08/06/2018 Funcion para agregar columnas a un TopenDataset
function VclAddColumn2(DataTable, ColumnName, Header) {
    var dataArrayDataType = {
        'name': "String", 'value': 1
    };
    var dataArray = {
        'DataType': dataArrayDataType, 'FieldName': ColumnName, 'Size': -1
    };
    var ncol = DataTable.FieldDefs.length;
    DataTable.FieldDefs[ncol] = dataArray;
    for (var i = 0; i < DataTable.Records.length; i++) {
        var dataArray = {
            'Value': " "
        };
        DataTable.Records[i].Fields[ncol] = dataArray;
    }
    return DataTable;
}



