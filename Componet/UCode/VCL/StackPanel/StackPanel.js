﻿//obj1 = new TVclStackPanel(TagMainCont[0].Child[14].Child[1].This, "1", 1, [[12], [12], [12], [12], [12]]);
//obj2 = new TVclStackPanel(TagMainCont[0].Child[14].Child[1].This, "1", 4, [[3, 3, 3, 3], [3, 3, 3, 3], [3, 3, 3, 3], [3, 3, 3, 3], [3, 3, 3, 3]]);
////obj2.DefineRows([[2,10], [2,10], [2,10], [2,10], [2,10]]);
//Div1 = obj1.Column[0].This;
//Div1 = obj1.Column[0].This;
//RowA = obj1.Row;
//Div2 = obj2.Column[0].This;
//Div3 = obj2.Column[1].This;
//Div4 = obj2.Column[2].This;
//Div5 = obj2.Column[3].This;
//RowB = obj2.Row;
//Vcllabel(Div1, "", TVCLType.BS, TlabelType.H0, "Div1");
//Vcllabel(Div2, "", TVCLType.BS, TlabelType.H0, "Div2");
//Vcllabel(Div3, "", TVCLType.BS, TlabelType.H0, "Div3");
//Vcllabel(Div4, "", TVCLType.BS, TlabelType.H0, "Div4");
//Vcllabel(Div5, "", TVCLType.BS, TlabelType.H0, "Div5");
//obj1.Row.Clean();
///************************************************




//TagMainCont[0].Child[14].Child[1].This,"1",1,[[12], [12], [12], [12], [12]]
var TVclStackPanel = function (ObjectHtml, Id, inColumnCount, DefineRows) {
    
    var TParent = function () {
        return this;
    }.bind(this);
    this.This = null;
    this.ColumnCount = inColumnCount;
    this.Column = new Array();
    this.Row = null;
    var arrContDef = new Array(1); //row principal   
    arrContDef[0] = new Array(inColumnCount); //col principal
    var arrObjCont = VclDivitions(ObjectHtml, "StackPanel_" + Id, arrContDef);
    BootStrapCreateRow(arrObjCont[0].This);
    this.Row = new TVclDiv(arrObjCont[0].This);
    $(this.Row.This).attr("data-type", "Row");
    BootStrapCreateColumns(arrObjCont[0].Child, DefineRows);    
    this.DefineRows = function (DefineRows) {
        BootStrapCreateColumns(arrObjCont[0].Child, DefineRows);
    }
    
    for (var i = 0; i < arrObjCont[0].Child.length; i++) {
        UnColumnObj = new TVclDiv(arrObjCont[0].Child[i].This);
        $(UnColumnObj.This).attr("data-type", "Col");
        this.Column.push(UnColumnObj);
    }

    Object.defineProperty(this, 'Visible', {
        get: function () {
                return this.Row.Visible;
        },
        set: function (_Value) {
            this.Row.Visible = _Value;
        }
    });
}

