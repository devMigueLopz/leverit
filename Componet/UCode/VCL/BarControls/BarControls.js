﻿//Version 1 2018/03/14/Jaiames
//Version 2 2018/03/16/Felipe
var TBarControls = function (inObjectHtml, inID) {
    var TParent = function () {
        return this;
    }.bind(this);

    this.ObjectHtml = inObjectHtml;
    this.ID = null;
    this.ToolBarList = new Array();

    //Propiedades
    this._Visible = true;
    this._Direcction = "Horizontal";
    this.ImgDirection = null;
    this._ToolBar_Direction = true;
    this._HeadBorderColor = "";
    this._BodyBorderColor = "";
    this.ImgDirecction = null; 
    
    if (inID == 'undefined') {
        this.ID = "TBarControls_" + Math.random();
    } else {
        this.ID = "TBarControls_" + inID;
    }    
   
    var DIV = new TVclStackPanel(this.ObjectHtml, inID, 3, [[12, 12, 12], [12, 12, 12], [12, 12, 12], [12, 12, 12], [12, 12, 12]]);
    this.DivHead = DIV.Column[1].This;
    this.ControlModal = DIV.Column[0].This;
    this.ControlBody = DIV.Column[2].This;  

    var DIV2 = new TVclStackPanel(this.DivHead, inID + "Head", 1, [[12], [12], [12], [12], [12]]);
    this.ControlHead = DIV2.Column[0].This;
    $(this.ControlHead).css("min-height", "40px");
    //$(this.ControlHead).css("padding-left", "2px");

    Object.defineProperty(this, 'Visible', {
        get: function () {
            return this._Visible;
        },
        set: function (_Value) {
            this._Visible = _Value;
            if (_Value) {
                $(this.ControlHead).css("display", "block");
            } else {
                $(this.ControlHead).css("display", "none");
            }
        }
    });

    Object.defineProperty(this, 'Direcction', {
        get: function () {
            return this._Direcction;
        },
        set: function (_Value) {
            this._Direcction = _Value;
            if (_Value == "Horizontal") {
                $(this.ControlHead).css("width", "100%");
                //$(this.ControlHead).css("padding", "2px");
            } else {
                $(this.ControlHead).css("width", "auto");
                $(this.ControlHead).css("padding-top", "7px");
            }
        }
    });
   
    Object.defineProperty(this, 'ToolBar_Direction', {
        get: function () {
            return this._ToolBar_Direction;
        },
        set: function (_Value) {
            this._ToolBar_Direction = _Value;
            try {
                if (_Value == false) {
                    $(this.ImgDirection).css("display", "none");
                } else {
                    $(this.ImgDirection).css("display", "block");
                }
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("BarControls.js TBarControls", e);
            }           
        }
    });


    Object.defineProperty(this, 'HeadBorder', {
        get: function () {
            return this._HeadBorderColor;
        },
        set: function (_Value) {
            this._HeadBorderColor = _Value;
            if (_Value) {
                $(this.DivHead).css("border", "1px solid  " + _Value);
            }
        }
    });


    Object.defineProperty(this, 'BodyBorder', {
        get: function () {
            return this._BodyBorderColor;
        },
        set: function (_Value) {
            this._BodyBorderColor = _Value;
            if (_Value) {
                $(this.ControlBody).css("border", "1px solid " + _Value);
            }
        }
    });
}

TBarControls.TToolBar = function (inTBarControls, inID) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();

    this.ToolList = new Array();
    this.ID = null;
    this._Width = "Auto";
    this.parent = inTBarControls;
    this.inTBarControls = inTBarControls;

    //cuando el evento click del toolBar viene desde otro lado, se asigna aqui
    this._Item = null;

    this.This_onClick = function () {        
        ///
        _this.ClickInTool();
    };

    if (inID == 'undefined') {
        this.ID = inTBarControls.ID + "_TToolBar_" + inTBarControls.ToolBarList.length + "_" + Math.random();
    } else {
        this.ID = inTBarControls.ID + "_TToolBar_" + inTBarControls.ToolBarList.length + "_" + inID;
    }

    //Propiedades
    this._Image_Src = "";
    this._Direcction = "Horizontal";
    this._DirecctionAuto = "Horizontal";
    this._Visible = true;
    this._BorderColor = "grey";
    this._Down = true;
    this._Show = false;   
  
    //Toolbar para cambiar direccionabilidad del barcontrols
    if (inTBarControls.ToolBarList.length == 0) {
        var div = document.createElement('div');
        $(div).css("width", "auto");
        $(div).css("float", "left");
        $(div).css("height", "38px");
        $(div).css("min-width", "40px");
        $(div).css("margin", "4px 2px");
        $(div).css("padding", "4px 6px");
        $(div).css("text-align", "center");
        $(div).css("border", "1px solid grey");

        inTBarControls.ImgDirecction = document.createElement("img");
        $(inTBarControls.ImgDirecction).css("cursor", "pointer");
        $(inTBarControls.ImgDirecction).css("margin", "0px 4px");
        $(inTBarControls.ImgDirecction).css("padding", "4px");
        $(inTBarControls.ImgDirecction).css("width", "28px");
        $(inTBarControls.ImgDirecction).css("height", "28px");
        inTBarControls.ImgDirecction.setAttribute("src", "image/24/Back.png");
        inTBarControls.ImgDirecction.addEventListener("click", function () {
            if (inTBarControls.Direcction == "Vertical") {
                inTBarControls.Direcction = "Horizontal";
               
                $(inTBarControls.DivHead).css("width", "100%");
                $(inTBarControls.ControlBody).css("width", "100%");

                $(inTBarControls.DivHead).css("height", "auto");
                $(inTBarControls.ControlHead).css("position", "static");
                inTBarControls.ImgDirecction.setAttribute("src", "image/24/Back.png");
            } else {
                inTBarControls.Direcction = "Vertical";

                $(inTBarControls.ControlHead).css("position", "absolute");
                $(inTBarControls.ControlHead).css("width", "500px");
                $(inTBarControls.ControlHead).css("margin", "0px 0px");
                //$(inTBarControls.ControlHead).css("border", "1px solid green");
               // $(inTBarControls.DivHead).css("border", "1px solid blue");

                var NtoolBar = inTBarControls.ToolBarList.length;
                var HeadHeight = NtoolBar * 57;
                $(inTBarControls.DivHead).css("height", HeadHeight + "px");

                $(inTBarControls.DivHead).css("width", "65px");
                $(inTBarControls.ControlBody).css("float", "left");
                $(inTBarControls.ControlBody).css("width", "calc(100% - 90px)");
                inTBarControls.ImgDirecction.setAttribute("src", "image/24/Up.png");
            }

            for (var i = 0; i < inTBarControls.ToolBarList.length; i++) {
                if (inTBarControls.Direcction == "Vertical") {
                    $(inTBarControls.ToolBarList[i].This).css("clear", "both");
                } else {
                    $(inTBarControls.ToolBarList[i].This).css("clear", "none");
                }               

                inTBarControls.ToolBarList[i].DirecctionAuto = inTBarControls.Direcction;
            }
            try {
                $(inTBarControls.ControlBody).css("min-height", inTBarControls.ControlBody.offsetHeight);
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("BarControls.js TBarControls.TToolBar", e);
            }
            
        });

        div.appendChild(inTBarControls.ImgDirecction);
        inTBarControls.ImgDirection = div;
        inTBarControls.ControlHead.appendChild(div);

        if (inTBarControls._ToolBar_Direction == false) {
            $(inTBarControls.ImgDirection).css("display", "none");
        }
    }

    this.This = document.createElement('div');  //Div Principal
    this.This.ID = this.ID;
    $(this.This).css("width", "auto");
    $(this.This).css("float", "left");
    $(this.This).css("height", "38px");
    $(this.This).css("min-width", "40px");
    $(this.This).css("margin", "4px 2px");
    $(this.This).css("padding", "4px 5px");
    $(this.This).css("text-align", "center");
    $(this.This).css("border", "1px solid grey");

    this.IconBox = document.createElement('div'); //Div Icon cuando esta en forma vertical 
    this.IconBox.ID = this.ID + "IconBox";
    $(this.IconBox).css("display", "none");   
    $(this.IconBox).css("width", "auto");

    this.ToolBox = document.createElement('div'); //Div para los iconos agregados
    this.ToolBox.ID = this.ID + "ToolBox";
    $(this.ToolBox).css("overflow ", "hidden");
    $(this.ToolBox).css("width", "auto");
    $(this.ToolBox).css("float", "left");

    this.This.appendChild(this.IconBox);
    this.This.appendChild(this.ToolBox);

    inTBarControls.ControlHead.appendChild(this.This);
    inTBarControls.ToolBarList.push(this);
     
    this.Img = document.createElement("img");   
    this.Img.setAttribute("src", "image/24/folder.png");  
    $(this.Img).css("cursor", "pointer");
    $(this.Img).css("margin", "0px 4px");
    $(this.Img).css("padding", "4px");
    $(this.Img).css("width", "28px");
    $(this.Img).css("height", "28px");
    this.IconBox.appendChild(this.Img);

    //$(this.IconBox).css("border", "1px solid green");
    //$(this.ToolBox).css("border", "1px solid red");

    Object.defineProperty(this, 'Visible', {
        get: function () {
            return this._Visible;
        },
        set: function (_Value) {           
            this._Visible = _Value;
            if (_Value) {
                $(this.This).css("display", "block");
            } else {
                $(this.This).css("display", "none");
            }
        }
    });

    Object.defineProperty(this, 'BorderColor', {
        get: function () {
            return this._BorderColor;
        },
        set: function (_Value) {
            this._BorderColor = _Value;
            $(this.This).css("border", "1px solid " + _Value);
        }
    });

    Object.defineProperty(this, 'EnabledBorder', {
        get: function () {
            return this._Down;
        },
        set: function (_Value) {
            this._Down = _Value;
            if (this._Down) {
                $(this.This).css("border", "1px solid #c4c4c4");
            } else {
                $(this.This).css("border", "0px solid #c4c4c4");
            }
        }
    });

    Object.defineProperty(this, 'Direcction', {
        get: function () {
            return this._Direcction;
        },
        set: function (_Value) {           
            this._Direcction = _Value;            
            if (this._Direcction != "Horizontal") {
                $(this.IconBox).css("display", "block");
                $(this.ToolBox).css("display", "none");
                $(this.ToolBox).css("background-color", "white");  
                $(this.ToolBox).css("position", "absolute");
                $(this.ToolBox).css("padding", "2px 4px 2px 2px");
                $(this.ToolBox).css("z-index", "2");               
                $(this.ToolBox).css("border", "1px solid " + this._BorderColor);
                $(this.ToolBox).css("margin-top", "5px");
                $(this.ToolBox).css("margin-left", "-6px");
                $(this.this).css("position", "relative"); 
            }
        }
    });

    Object.defineProperty(this, 'DirecctionAuto', {
        get: function () {
            return this._DirecctionAuto;
        },
        set: function (_Value) {
            this._DirecctionAuto = _Value;
            if (this._DirecctionAuto == "Vertical") {
                $(this.IconBox).css("display", "block");
                $(this.ToolBox).css("display", "none");
                $(this.ToolBox).css("background-color", "white");    
                $(this.ToolBox).css("position", "absolute");
                $(this.ToolBox).css("padding", "2px 5px 5px 2px");
                $(this.ToolBox).css("z-index", "2");
                $(this.ToolBox).css("border", "1px solid " + this._BorderColor);
                $(this.ToolBox).css("margin-top", "-33px");
                $(this.ToolBox).css("margin-left", "42px");
                $(this.ToolBox).css("min-width", "60px");
                
                $(this.ToolBox).css("width", "auto");         
                $(this.this).css("position", "relative");
                $(this.this).css("background-color", "grey");

                for (var i = 0; i < this.ToolList.length; i++) {
                    if (this.ToolList[i]._Type == "Divition") {
                        $(this.ToolList[i].This).css("display", "none");
                    }
                    if (this._Direcction == "Horizontal") {                       
                            $(this.ToolList[i].ThisDiv).removeClass("Divtooltip");
                            $(this.ToolList[i].ThisDiv).css("clear", "both");
                            $(this.ToolList[i].ThisDiv).css("margin-top", "4px");                       
                    }
                }

            } else {        
                $(this.ToolBox).css("min-width", "40px");
                if (this._Direcction == "Horizontal") {                  
                    $(this.IconBox).css("display", "none");
                    $(this.ToolBox).css("display", "Block");
                    $(this.ToolBox).css("background-color", "transparent"); 
                    $(this.ToolBox).css("position", "static");
                    $(this.ToolBox).css("margin-top", "0px");
                    $(this.ToolBox).css("margin-left", "0px");                    
                    $(this.ToolBox).css("padding", "0px 4px 0px 2px");
                    $(this.ToolBox).css("border", "0px solid grey");
                    $(this.this).css("position", "static");
                    for (var i = 0; i < this.ToolList.length; i++) {
                        $(this.ToolList[i].ThisDiv).addClass("Divtooltip");
                        $(this.ToolList[i].ThisDiv).css("clear", "none");
                        $(this.ToolList[i].ThisDiv).css("margin-top", "0px");
                        if (this.ToolList[i]._Type == "Divition") {
                            if (this.ToolList[i]._Visible) {
                                $(this.ToolList[i].This).css("display", "block");
                            }                           
                        }
                    }
                  } else {
                    $(this.IconBox).css("display", "block");
                    $(this.ToolBox).css("display", "none");
                    $(this.ToolBox).css("background-color", "white");   
                    $(this.ToolBox).css("position", "absolute");
                    $(this.ToolBox).css("padding", "2px 4px 2px 2px");
                    $(this.ToolBox).css("z-index", "2");
                    $(this.ToolBox).css("border", "1px solid " + this._BorderColor);
                    $(this.ToolBox).css("margin-top", "5px");
                    $(this.ToolBox).css("margin-left", "-6px");
                    $(this.this).css("position", "relative");
                }                
            }
        }
    });

    Object.defineProperty(this, 'Image', {
        get: function () {
            return this._Image_Src;
        },
        set: function (_Value) {
            this._Image_Src = _Value;
            _this.Img.setAttribute("src", _Value);
        }
    });

    this.Img.addEventListener("click", function () {        
        ///
        _this.ClickInTool();
    });


    Object.defineProperty(this, 'Width', {
        get: function () {
            return this._Width;
        },
        set: function (_Value) {
            this._Width = _Value;
            $(this.ToolBox).css("width", _Value);
        }
    });

    Object.defineProperty(this, 'Modal', {
        get: function () {
            return this._Modal;
        },
        set: function (_Value) {
            this._Modal = _Value;            
        }
    });
}

TBarControls.TToolBar.prototype.ClickInTool = function () {
    var _this = this.TParent();     
    for (var i = 0; i < _this.inTBarControls.ToolBarList.length; i++) {
        if (_this.inTBarControls.ToolBarList[i]._Direcction == "Vertical") {
            if (_this.inTBarControls.ToolBarList[i].ID != _this.ID) {
                $(_this.inTBarControls.ToolBarList[i].ToolBox).css("display", "none");
                _this.inTBarControls.ToolBarList[i]._Show = false;
            }
        } else {
            if (_this.inTBarControls._Direcction == "Vertical") {
                if (_this.inTBarControls.ToolBarList[i].ID != _this.ID) {
                    $(_this.inTBarControls.ToolBarList[i].ToolBox).css("display", "none");
                    _this.inTBarControls.ToolBarList[i]._Show = false;
                }
            }
        }
    }
    if (_this._Direcction != "Static") {
        if (_this._Show) {
            _this._Show = false;
            $(_this.ToolBox).css("display", "none");
        } else {
            _this._Show = true;
            $(_this.ToolBox).css("display", "block");
        }
    }   
}

 

//////////////////////////////////////////////////////////////////////////////////
TBarControls.TToolBar.ButtonImg = function (inTToolBar, inID)
{   
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();    
  
    //Propiedades
    this.ClickDown = false;
    this._Visible = true;
    this._Down = false
    this._Image = "";
    this._text = ""; //Tooltip / Hint  / label
    this._textColor = "black";
    this._hoverColor = "#353e41";
    this._hoverColorLabel = "white";  
    this._Enabled = true;
    this._Type = "Button";
    this.ID = null;   

    if (inID == 'undefined') {
        this.ID = inTToolBar.ID + "_TToolButton_" + inTToolBar.ToolList.length + "_" + Math.random();
    } else {
        this.ID = inTToolBar.ID + "_TToolButton_" + inTToolBar.ToolList.length + "_" + inID;
    }

    //Div 
    this.ThisDiv = document.createElement('div');
    this.ThisE= document.createElement('div');  
    this.ThisDiv.appendChild(this.ThisE);
    inTToolBar.ToolBox.appendChild(this.ThisDiv);

    $(this.DivE).css("width", "auto");
    $(this.DivE).css("float", "left");
    $(this.DivE).css("display", "inline-block");
    $(this.DivE).css("cursor", "pointer");
    $(this.DivE).css("padding-right", "3px");

    $(this.ThisDiv).css("width", "auto");
    $(this.ThisDiv).css("float", "left");
    $(this.ThisDiv).css("display", "inline-block");
    $(this.ThisDiv).css("cursor", "pointer");
    $(this.ThisDiv).css("padding-right", "3px");
     
    $(this.ThisDiv).hover(function () {
        $(this).css("background-color", _this._hoverColor);
        $(_this.Label).css("color", _this._hoverColorLabel);                
    }, function () { 
        $(this).css("background-color", "transparent");
        $(_this.Label).css("color", _this._textColor);
    });

    if (inTToolBar._Direcction == "Horizontal") {      
        $(this.ThisDiv).addClass("Divtooltip");
    } else {               
        $(this.ThisDiv).css("clear", "both");
        $(this.ThisDiv).css("margin-top", "4px");
        $(this.ThisDiv).css("width", "100%");
    }

    Div1 = document.createElement('div');
    this.ThisE.appendChild(Div1);
    $(Div1).css("float", "left");
    $(Div1).css("width", "auto");

    this.Div2 = document.createElement('div');
    this.ThisE.appendChild(this.Div2);
    $(this.Div2).css("float", "left");
    $(this.Div2).css("width", "auto");
    $(this.Div2).css("padding-top", "4px");

    //Btn
    this.ID = inTToolBar.ID + "ButtonImg" + inID;
    this.Element = document.createElement("img");
    this.Element.ID = this.ID;
    Div1.appendChild(this.Element);
    $(this.Element).css("cursor", "pointer");
    $(this.Element).css("margin", "0px 4px");
    $(this.Element).css("padding", "4px");
    $(this.Element).css("width", "28px");
    $(this.Element).css("height", "28px");     

    //label    
    this.Label = document.createElement("Label");
    this.Label.style.fontWeight = "normal";
    this.Div2.appendChild(this.Label);
    if (inTToolBar._Direcction == "Horizontal") {
        $(this.Label).addClass("tooltiptext");
    }    
    $(this.Label).css("display","none");


    inTToolBar.ToolList.push(this);

    this.ThisDiv.addEventListener("click", function () {
        if (_this._Down) {
            if (_this.ClickDown) {
                _this.ClickDown = false;
            } else {
                _this.ClickDown = true;
            }
            if (_this.ClickDown) {
                $(_this.Element).css("border", "1px solid #c4c4c4");
            } else {
                $(_this.Element).css("border", "0px solid #c4c4c4");
            }
        }
        
        for (var i = 0; i < inTToolBar.parent.ToolBarList.length; i++) {
            if (inTToolBar.parent.ToolBarList[i]._Direcction == "Vertical") {
                $(inTToolBar.parent.ToolBarList[i].ToolBox).css("display", "none");
                inTToolBar.parent.ToolBarList[i]._Show = false;
            } else { //Horizontal
                if (inTToolBar.parent._Direcction == "Vertical") {
                    $(inTToolBar.parent.ToolBarList[i].ToolBox).css("display", "none");
                    inTToolBar.parent.ToolBarList[i]._Show = false;
                } 
            }
        }
          
    });

    Object.defineProperty(this, 'visible', {
        get: function () {
            return this._Visible;
        },
        set: function (_Value) {
            this._Visible = _Value;
            if (_Value) {
                $(this.ThisE).css("display", "block");
            } else {
                $(this.ThisE).css("display", "none");
            }
        }
    });

    Object.defineProperty(this, 'Image', {
        get: function () {
            return this._Image;
        },
        set: function (_Value) {
            this._Image = _Value;
            this.Element.setAttribute("src", _Value);
        }
    });

    Object.defineProperty(this, 'EnabledDown', {
        get: function () {
            return this._Down;
        },
        set: function (_Value) {
            this._Down = _Value;
        }
    });

    Object.defineProperty(this, 'Text', {
        get: function () {
            return this._Text;
        },
        set: function (_Value) {
            this._Text = _Value;
            if (_Value != null && _Value != "") {
                $(this.Label).css("display", "block");                
                this.Label.innerText = _Value;
            } else {
                $(this.Label).css("display", "none");
            }
        }
    });

     
    Object.defineProperty(this, 'onClick', {
        get: function () {
            return this.ThisDiv.onclick;//felipe return this.ThisE.onclick;
        },
        set: function (_Value) {
            this.ThisDiv.onclick = _Value;//felipe this.ThisE.onclick = _Value;
        }
    });

     Object.defineProperty(this, 'ChekedClickDown', {
        get: function () {
            return this.ClickDown;
        },
        set: function (_Value) {
            this.ClickDown = _Value;
            if (this.ClickDown) {
                $(this.Element).css("border", "1px solid #c4c4c4");
            } else {
                $(this.Element).css("border", "0px solid #c4c4c4");
            }
        }
     });

    Object.defineProperty(this, 'TextColor', {
        get: function () {
            return this._textColor;
        },
        set: function (_Value) {
            this._textColor = _Value;
        }
    });
    Object.defineProperty(this, 'HoverColor', {
        get: function () {
            return this._hoverColor;
        },
        set: function (_Value) {
            this._hoverColor = _Value;
        }
    });
    Object.defineProperty(this, 'HoverColorLabel', {
        get: function () {
            return this._hoverColorLabel;
        },
        set: function (_Value) {
            this._hoverColorLabel = _Value;
        }
    });

    Object.defineProperty(this, 'Enabled', {
        get: function () {
            return this._Enabled;
        },
        set: function (_Value) {
            this._Enabled = _Value;
            if (_Value) {
                this.Element.style.opacity = "1";
                this.ThisE.style.pointerEvents = "auto";
                $(this.ThisDiv).css("cursor", "pointer");
            } else {
                this.Element.style.opacity = "0.5";
                this.ThisE.style.pointerEvents = "none";
                $(this.ThisDiv).css("cursor", "not-allowed");
            }
        }
    });
}

TBarControls.TToolBar.Edit = function (inTToolBar, inID)
{
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();

    this.ID = inTToolBar.ID + "Edit" + inID;
    this._Width = "50px";
    this._Visible = true;
    this._Text = "";
    this._Caption = "";
    this._Type = "Edit";

    this.ID = null;   

    if (inID == 'undefined') {
        this.ID = inTToolBar.ID + "_TToolEdit_" + inTToolBar.ToolList.length + "_" + Math.random();
    } else {
        this.ID = inTToolBar.ID + "_TToolEdit_" + inTToolBar.ToolList.length + "_" + inID;
    }

    //Div del edit
    this.ThisDiv = document.createElement('div');
    inTToolBar.ToolBox.appendChild(this.ThisDiv);
    $(this.ThisDiv).css("width", "auto");
    $(this.ThisDiv).css("float", "left");
    $(this.ThisDiv).css("margin", "0px 3px");
    if (inTToolBar._Direcction != "Horizontal") {    
        $(this.ThisDiv).css("clear", "both");
    }

    //Label Edit
    DivLabel = document.createElement('div');
    $(DivLabel).css("width", "auto");
    $(DivLabel).css("float", "left");
    $(DivLabel).css("padding-top", "4px"); 

    this.Label = document.createElement("Label");  
    this.Label.style.fontWeight = "normal";
    $(this.Label).css("margin-Bottom", "5px");
    DivLabel.appendChild(this.Label);
    this.ThisDiv.appendChild(DivLabel);

    //Text Edit
    this.Element = document.createElement('input');
    this.Element.ID = this.ID;   
    this.Element.type = "text";
    this.Element.style.marginRight = "3px";
    this.Element.style.marginLeft = "3px";
    this.Element.style.marginTop = "4px";
    this.Element.style.width = "50px";
    this.ThisDiv.appendChild(this.Element);
    $(this.Element).css("border", "1px solid #000000");
    
    inTToolBar.ToolList.push(this);

    Object.defineProperty(this, 'Visible', {
        get: function () {
            return this._visible;
        },
        set: function (_Value) {
            this._visible = _Value;
            if (_Value) {
                $(this.ThisDiv).css("display", "block");
            } else {
                $(this.ThisDiv).css("display", "none");
            }
        }
    });

    Object.defineProperty(this, 'Width', {
        get: function () {
            return this._Width;
        },
        set: function (_Value) {
            this._Width = _Value;
            $(this.Element).css("width", _Value);
        }
    });

    Object.defineProperty(this, 'Text', {
        get: function () {
            return this._Text;
        },
        set: function (_Value) {
            this._Text = _Value;
            this.Element.innerText = _Value;
        }
    });

    Object.defineProperty(this, 'Caption', {
        get: function () {
            return this._Caption;
        },
        set: function (_Value) {
            if (_Value != null && _Value != "") {
                this._Caption = _Value;
                this.Label.innerText = _Value;
            }
        }
    });    
}

TBarControls.TToolBar.Label = function (inTToolBar, inID)
{
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();

    this.ID = inTToolBar.ID + "Caption" + inID;
    this._Visible = true;
    this._Text = "";
    this._Color = "black";
    this._Type = "Label";

    this.ID = null;

    if (inID == 'undefined') {
        this.ID = inTToolBar.ID + "_TToolLabel_" + inTToolBar.ToolList.length + "_" + Math.random();
    } else {
        this.ID = inTToolBar.ID + "_TToolLabel_" + inTToolBar.ToolList.length + "_" + inID;
    }

    this.Element = document.createElement("Label");
    this.Element.ID = this.ID;
    inTToolBar.ToolBox.appendChild(this.Element);
    this.Element.style.fontWeight = "normal";
    this.Element.style.marginRight = "0px";
    this.Element.style.marginLeft = "3px";
    this.Element.style.marginBottom = "10px";

    inTToolBar.ToolList.push(this);

    Object.defineProperty(this, 'visible', {
        get: function () {
            return this._Visible;
        },
        set: function (_Value) {
            this._Visible = _Value;
            if (_Value) {
                $(this.Element).css("display", "block");
            } else {
                $(this.Element).css("display", "none");
            }
        }
    });


    Object.defineProperty(this, 'Color', {
        get: function () {
            return this._Color;
        },
        set: function (_Value) {
            this._Color = _Value;
            $(this.Element).css("color", _Value);
        }
    });

    Object.defineProperty(this, 'Text', {
        get: function () {
            return this._Text;
        },
        set: function (_Value) {
            this._Text = _Value;
            this.Element.innerText = _Value;
        }
    });
}

TBarControls.TToolBar.Divition = function (inTToolBar, inID) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();

    this.ID = inTToolBar.ID + "Divition" + inID;
    this._Visible = true;
    this._Color = "#dedede";
    this._Type = "Divition";
    this.ID = null;

    if (inID == 'undefined') {
        this.ID = inTToolBar.ID + "_TToolDivition_" + inTToolBar.ToolList.length + "_" + Math.random();
    } else {
        this.ID = inTToolBar.ID + "_TToolDivition_" + inTToolBar.ToolList.length + "_" + inID;
    }

    //Div 
    this.This = document.createElement('div');
    inTToolBar.ToolBox.appendChild(this.This);
    $(this.This).css("width", "1px");
    $(this.This).css("min-height", "27px");
    $(this.This).css("float", "left");
    $(this.This).css("border-left", "1px solid #dedede");

    inTToolBar.ToolList.push(this);   

    Object.defineProperty(this, 'visible', {
        get: function () {
            return this._Visible;
        },
        set: function (_Value) {
            this._Visible = _Value;
            if (_Value) {
                $(this.This).css("display", "block");
            } else {
                $(this.This).css("display", "none");
            }
        }
    });

    Object.defineProperty(this, 'Color', {
        get: function () {
            return this._Color;
        },
        set: function (_Value) {            
            this._Color = _Value;
            $(this.This).css("border-color", _Value);
        }
    });
}

