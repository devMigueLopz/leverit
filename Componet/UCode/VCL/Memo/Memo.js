﻿/*<summary>
Midifico fecha   
Carlos     24-04-2017
Funcion: Crea un elemento de tipo Textarea de html
ejemplos:
UVclMemo(Elementhtml, "Id textarea", "texto que va dentro", 10, 50);
</summary>
<param name="Object">Elemento html al cual se le insertara el textarea creado por esta funcion</param>
<param name="id">Id que va a ser asignado al textarea</param>
<param name="text">Texto insertado dentro del elemento textarea</param>
<param name="rows">Numero de filas (alto) del textarea</param>
<param name="cols">Numero de columnas (ancho) del textarea</param>
<returns>Retorna un elemento de tipo Textarea</returns>*/

function VclMemo(Object, id,VCLType, text, rows, cols) { //memo x y 
    
    var Res = Utextarea(Object, id, text, rows, cols);
    switch (VCLType) {
        case TVCLType.AG:
            break;
        case TVCLType.BS:
            BootStrapCreateTextArea(Res);
            break;
        case TVCLType.DV:
            break;
        case TVCLType.KD:
            break;
        default:
    }
    return Res;
}





var TVclMemo = function (ObjectHtml, Id) {
    this.This = Utextarea(ObjectHtml, Id, "", 5, 20);;//VclMemo

    Object.defineProperty(this, 'Text', {
        get: function () {
            return this.This.value;
        },
        set: function (_Value) {
            this.This.value = _Value;
        }
    });

    Object.defineProperty(this, 'Rows', {
        get: function () {
            this.This.getAttribute("rows");
        },
        set: function (_Value) {
            this.This.setAttribute("rows", _Value);
        }
    });

    Object.defineProperty(this, 'Cols', {
        get: function () {
            this.This.getAttribute("cols");
        },
        set: function (_Value) {
            this.This.setAttribute("cols", _Value);
        }
    });

    Object.defineProperty(this, 'Enabled', {
        get: function () {
            return !this.This.disabled;
        },
        set: function (_Value) {
            this.This.disabled = !_Value;
        }
    });

    Object.defineProperty(this, 'onClick', {
        get: function () {
            return this.This.onclick;
        },
        set: function (_Value) {
            this.This.onclick = _Value;
            //this.This.addEventListener("click", function () {

            //});
        }
    });

    Object.defineProperty(this, 'VCLType', {
        get: function () {
            return this.VCLType;
        },
        set: function (_Value) {
            switch (_Value) {
                case TVCLType.AG:
                    break;
                case TVCLType.BS:
                    BootStrapCreateTextArea(this.This);
                    break;
                case TVCLType.DV:
                    break;
                case TVCLType.KD:
                    break;
                default:
            }
        }
    });

    Object.defineProperty(this, 'Width', {
        get: function () {
            return this.This.style.width;
        },
        set: function (_Value) {
            this.This.style.width = _Value;
        }
    });

    Object.defineProperty(this, 'Visible', {
        get: function () {
            if (this.This.style.visibility == "hidden" || this.This.style.visibility == "collapse")
                return false;
            return true;
        },
        set: function (_Value) {
            if (_Value)
                this.This.style.visibility = "visible";
            else
                this.This.style.visibility = "hidden";
        }
    });

    this.Click = function () {
        this.This.click();
    }

    var TParent = function () {
        return this;
    }.bind(this);

    //this.Hide;
    //this.Destroy;
    //this.Disable;
    //this.Caption;
    //this.Hint;
    //this.Text;
    //this.Tag;
    //this.OnClick;
    //this.Width;
    //this.Height;
    //this.ScrollBars;// ssNone,ssBoth,ssHorizontal,ssVertical
}