﻿var RHSelectOutput = function (inObjectHTML, ID, inTraslate, Mythis) {
   
    var _this = this;

    this.OnClickOk = null;
    this.OnClickITHC = false;
    this.OnClickATIS = null;
    this.OnClickRH = null;
    this.OnClickClose = null;

    this.modal = new TVclModal(inObjectHTML, null, ID);
    this.DBTranslate = inTraslate;    
    this.Mythis = Mythis;

    UsrCfg.Traslate.GetLangText(_this.Mythis, "labTitleRHOutput", "RUN ");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "labinBrowse", "In Browse");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "labInWin32", "In Win 32");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "labSelectRHOutput", "Select Output ");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "modal.AddHeaderContent", "Select Output");


    //CONFIGURATION
    this.ModuleConfig = {
        inITHelpcenter: {
            Visible: {
            },
            Run: {
                inBrowse: {},
                inWin32: {}
            }
        },
        inAtis: {
            Visible: {
            },
            Run: {
                inBrowse: {},
                inWin32: {}
            }
        },
        inRemoteHelp: {
            Visible: {
            },
            Run: {
                inBrowse: {},
                inWin32: {}
            }
        }
    }

    //habilitar botones
    _this.ModuleConfig.inITHelpcenter.Visible = false;
    _this.ModuleConfig.inAtis.Visible = false;
    _this.ModuleConfig.inRemoteHelp.Visible = false;

    //
    _this.ModuleConfig.inITHelpcenter.Run.inBrowse = true;
    _this.ModuleConfig.inITHelpcenter.Run.inWin32 = false;
    _this.ModuleConfig.inAtis.Run.inBrowse = true;
    _this.ModuleConfig.inAtis.Run.inWin32 = false;
    _this.ModuleConfig.inRemoteHelp.Run.inBrowse = true;
    _this.ModuleConfig.inRemoteHelp.Run.inWin32 = true;

    //WEBSERVICE  
    $.ajax({
        type: "POST",
        async: false,
        url: SysCfg.App.Properties.xRaiz + 'Service/RemoteHelpService/RemoteHelp.svc/ListSeModuleActive',
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //habilitar botones   
            _this.ModuleConfig.inITHelpcenter.Visible = response.d.ITHelpcenterVisible;
            _this.ModuleConfig.inAtis.Visible = response.d.AtisVisible;
            _this.ModuleConfig.inRemoteHelp.Visible = response.d.RemoteHelpVisible;
        },
        error: function (error) {
            SysCfg.Log.Methods.WriteLog("RHSelectOutput.js RHSelectOutput () \n Location: RH Select Output / RemoteConfig " + error.txtDescripcion);            
        }
    });

    var main = new TVclStackPanel("", "", 1, [[12], [12], [12], [12], [12]]);
    main.Row.This.style.textAlign = "center";

    var mainTitle = new TVclStackPanel(main.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
    var mainOptions = new TVclStackPanel(main.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
    var mainSubTitle = new TVclStackPanel(main.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
    var mainBotones = new TVclStackPanel(main.Column[0].This, "", 3, [[4, 4, 4], [4, 4, 4], [4, 4, 4], [4, 4, 4], [4, 4, 4]]);
    var mainButtonOk = new TVclStackPanel(main.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);

    this.lblMainTitle = new TVcllabel(mainTitle.Column[0].This, "", TlabelType.H3);
    this.lblMainTitle.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "labTitleRHOutput");
    this.lblMainTitle.VCLType = TVCLType.BS;    

    this.OptionRun = new TVclInputRadioButton(mainOptions.Column[0].This, "");

    var VclInputRadioButtonItem1 = new TVclInputRadioButtonItem();
    VclInputRadioButtonItem1.Id = "1";
    VclInputRadioButtonItem1.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "labinBrowse");
    VclInputRadioButtonItem1.Value = "In Browser";
    VclInputRadioButtonItem1.OnChange = function () {      
        _this.ButtonITHC.Enabled = true;
        _this.ButtonATIS.Enabled = true;
        _this.ButtonRH.Enabled = true;
        if (_this.ModuleConfig.inITHelpcenter.Visible == false) {
            _this.ButtonITHC.Enabled = false;
        }
        if (_this.ModuleConfig.inAtis.Visible == false) {
            _this.ButtonATIS.Enabled = false;
        }
       
    }
    this.OptionRun.AddItem(VclInputRadioButtonItem1);

    var VclInputRadioButtonItem2 = new TVclInputRadioButtonItem();
    VclInputRadioButtonItem2.Id = "2";
    VclInputRadioButtonItem2.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "labInWin32");
    VclInputRadioButtonItem2.Value = "In Win 32";
    VclInputRadioButtonItem2.OnChange = function () {

        _this.ButtonITHC.Enabled = false;
        _this.ButtonATIS.Enabled = false;
        _this.ButtonRH.Enabled = true;
        if (_this.ModuleConfig.inITHelpcenter.Visible == false) {
            _this.ButtonITHC.Enabled = false;
        }
        if (_this.ModuleConfig.inAtis.Visible == false) {
            _this.ButtonATIS.Enabled = false;
        }
    }
    this.OptionRun.AddItem(VclInputRadioButtonItem2);
    this.OptionRun.selectedIndex = 0;

    this.lblMainTitle = new TVcllabel(mainSubTitle.Column[0].This, "", TlabelType.H3);
    this.lblMainTitle.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "labSelectRHOutput");
    this.lblMainTitle.VCLType = TVCLType.BS;

    this.DivButtonITHC = mainBotones.Column[0].This;
    var DivBtn1 = new TVclStackPanel(mainBotones.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
    var Divlbl1 = new TVclStackPanel(mainBotones.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);    
    this.ButtonITHC = new TVclButton(DivBtn1.Column[0].This, ID)
    this.ButtonITHC.VCLType = TVCLType.BS;
    this.ButtonITHC.Src = "Componet/UCode/VCL/RHSelectOutput/ithelpcenter.png";
    this.ButtonITHC.Enabled = false;
    this.ButtonITHC.onClick = function () {
        if (_this.OnClickITHC != null)
            _this.OnClickITHC(_this, this);
        _this.modal.CloseModal();
    }
    this.lbl1 = new TVcllabel(Divlbl1.Column[0].This, "", TlabelType.H0);
    this.lbl1.Text = "IT Help Center";
    this.lbl1.VCLType = TVCLType.BS;


    this.DivButtonATIS = mainBotones.Column[1].This;
    var DivBtn2 = new TVclStackPanel(mainBotones.Column[1].This, "", 1, [[12], [12], [12], [12], [12]]);
    var Divlbl2 = new TVclStackPanel(mainBotones.Column[1].This, "", 1, [[12], [12], [12], [12], [12]]);

    this.ButtonATIS = new TVclButton(DivBtn2.Column[0].This, ID)
    this.ButtonATIS.VCLType = TVCLType.BS;
    this.ButtonATIS.Src = "Componet/UCode/VCL/RHSelectOutput/atis.png";
    this.ButtonATIS.Enabled = false;
    this.ButtonATIS.onClick = function () {
        if (_this.OnClickATIS != null)
            _this.OnClickATIS(_this, this);
        _this.modal.CloseModal();
    }
    this.lbl2 = new TVcllabel(Divlbl2.Column[0].This, "", TlabelType.H0);
    this.lbl2.Text = "ATIS";
    this.lbl2.VCLType = TVCLType.BS;

    this.DivButtonRHS = mainBotones.Column[2].This;
    var DivBtn3 = new TVclStackPanel(mainBotones.Column[2].This, "", 1, [[12], [12], [12], [12], [12]]);
    var Divlbl3 = new TVclStackPanel(mainBotones.Column[2].This, "", 1, [[12], [12], [12], [12], [12]]);
    this.ButtonRH = new TVclButton(DivBtn3.Column[0].This, ID)
    this.ButtonRH.VCLType = TVCLType.BS;
    this.ButtonRH.Src = "Componet/UCode/VCL/RHSelectOutput/remotehelp.png";
    this.ButtonRH.Enabled = false;
    this.ButtonRH.onClick = function () {
        if (_this.OnClickRH != null)
            _this.OnClickRH(_this, this);
        _this.modal.CloseModal();
    }
    this.lbl3 = new TVcllabel(Divlbl3.Column[0].This, "", TlabelType.H0);
    this.lbl3.Text = "Remote Help";
    this.lbl3.VCLType = TVCLType.BS;

    //this.ButtonOK = new TVclButton(mainButtonOk.Column[0].This, ID);
    //this.ButtonOK.Text = "OK"
    //this.ButtonOK.VCLType = TVCLType.BS;
    //this.ButtonOK.onClick = function () {
       
    //}

    this.modal.AddHeaderContent(UsrCfg.Traslate.GetLangText(_this.Mythis, "modal.AddHeaderContent"));
    this.modal.AddContent(main.Row.This);
    this.modal.OnAfterCloseModal = function () {
        _this.OnClickClose(_this, this);
    }

    //VALIDACIONES
    if (_this.ModuleConfig.inITHelpcenter.Visible == false) {
        //$(this.DivButtonITHC).css("display", "none");
        this.ButtonITHC.Enabled = false;
    } else {
        this.ButtonITHC.Enabled = true;
    }

    if (_this.ModuleConfig.inAtis.Visible == false) {
        // $(this.DivButtonATIS).css("display", "none");
        this.ButtonATIS.Enabled = false;
    } else {
        this.ButtonATIS.Enabled = true;
    }
   
    if (_this.ModuleConfig.inRemoteHelp.Visible == false) {
        //$(this.DivButtonRHS).css("display", "none");
        this.ButtonRH.Enabled = false;
        this.OptionRun.Enabled = false;
    } else {
        this.ButtonRH.Enabled = true;
        this.OptionRun.Enabled = true;
    }


    //this._EnabledButtonITHC = false;
    //this._EnabledButtonATIS = false;
    //this._EnabledButtonRH = false;

    //this._VisibleButtonITHC = false;
    //this._VisibleButtonATIS = false;
    //this._VisibleButtonRH = false;

    //Object.defineProperty(this, 'EnabledButtonITHC', {
    //    get: function () {
    //        return this._EnabledButtonITHC;
    //    },
    //    set: function (inValue) {
    //        this._EnabledButtonITHC = inValue;
    //        this.ButtonITHC.Enabled = inValue;
    //    }
    //});

    //Object.defineProperty(this, 'EnabledButtonATIS', {
    //    get: function () {
    //        return this._EnabledButtonATIS;
    //    },
    //    set: function (inValue) {
    //        this._EnabledButtonATIS = inValue;
    //        this.ButtonATIS.Enabled = inValue;
    //    }
    //});

    //Object.defineProperty(this, 'EnabledButtonRH', {
    //    get: function () {
    //        return this._EnabledButtonRH;
    //    },
    //    set: function (inValue) {
    //        this._EnabledButtonRH = inValue;
    //        this.ButtonRH.Enabled = inValue;
    //    }
    //});


    Object.defineProperty(this, 'VisibleButtonITHC', {
        get: function () {
            return this._VisibleButtonITHC;
        },
        set: function (inValue) {
            this._VisibleButtonITHC = inValue;
            this.ButtonITHC.Visible = inValue;
            this.lbl1.Visible = inValue;
        }
    });

    Object.defineProperty(this, 'VisibleButtonATIS', {
        get: function () {
            return this._VisibleButtonATIS;
        },
        set: function (inValue) {
            this._VisibleButtonATIS = inValue;
            this.ButtonATIS.Visible = inValue;
            this.lbl2.Visible = inValue;
        }
    });

    Object.defineProperty(this, 'VisibleButtonRH', {
        get: function () {
            return this._VisibleButtonRH;
        },
        set: function (inValue) {
            this._VisibleButtonRH = inValue;
            this.ButtonRH.Visible = inValue;
            this.lbl3.Visible = inValue;
        }
    });
    

    this.Show = function () {
        this.modal.ShowModal();
        this.modal.Content.This.style.width = "500px";
    }

    this.Close = function () {
        this.modal.CloseModal();
    }
}