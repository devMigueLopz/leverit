﻿function GPQuery() {
    this.Title;
    this.Path;
    this.SQL;
    this.VerticalColumnCount;
}

function VclQueryView(ObjectHtml, GPQuery) {
    var QueryViewDef = new Array(1);
    QueryViewDef[0] = new Array(7);
    QueryViewDef[0][0] = new Array(1);
    QueryViewDef[0][1] = new Array(1);
    QueryViewDef[0][2] = new Array(1);
    QueryViewDef[0][3] = new Array(1);
    QueryViewDef[0][4] = new Array(1);
    QueryViewDef[0][5] = new Array(1);
    QueryViewDef[0][6] = new Array(1);

    var QueryViewCtrl = VclDivitions(ObjectHtml, "QueryView", QueryViewDef);

    BootStrapContainer(QueryViewCtrl[0].This, TBootStrapContainerType.fluid);
    QueryViewCtrl[0].This.style.marginTop = "10px";

    BootStrapCreateRows(QueryViewCtrl[0].Child);
    BootStrapCreateColumn(QueryViewCtrl[0].Child[0].Child[0].This, [12, 12, 12, 12, 12]);
    BootStrapCreateColumn(QueryViewCtrl[0].Child[1].Child[0].This, [12, 12, 12, 12, 12]);
    BootStrapCreateColumn(QueryViewCtrl[0].Child[2].Child[0].This, [12, 12, 12, 12, 12]);
    BootStrapCreateColumn(QueryViewCtrl[0].Child[3].Child[0].This, [12, 12, 12, 12, 12]);
    BootStrapCreateColumn(QueryViewCtrl[0].Child[4].Child[0].This, [12, 12, 12, 12, 12]);
    BootStrapCreateColumn(QueryViewCtrl[0].Child[5].Child[0].This, [12, 12, 12, 12, 12]);
    BootStrapCreateColumn(QueryViewCtrl[0].Child[6].Child[0].This, [12, 12, 12, 12, 12]);

    Vcllabel(QueryViewCtrl[0].Child[0].Child[0].This, "lblTitleQueryView", TVCLType.BS, TlabelType.H0, GPQuery.Title);
    Vcllabel(QueryViewCtrl[0].Child[1].Child[0].This, "lblPathQueryView", TVCLType.BS, TlabelType.H0, GPQuery.Path);

    var btnRefresh = new TVclButton(QueryViewCtrl[0].Child[2].Child[0].This, "btnRefreshQueryView");
    btnRefresh.Width = "30px";
    btnRefresh.VCLType = TVCLType.BS;
    btnRefresh.onClick = btnRefresh_onClick;
    Uimg(btnRefresh.This, "", "image/16/Refresh.png", "Refresh");
    QueryViewCtrl[0].Child[6].This.style.height = "200px";

    function btnRefresh_onClick() {
        LoadQuery(QueryViewCtrl[0].Child[4].Child[0].This, QueryViewCtrl[0].Child[6].Child[0].This, GPQuery);
    }
    //btnRefresh.This.addEventListener("click", function () {
    //    LoadQuery(QueryViewCtrl[0].Child[4].Child[0].This, QueryViewCtrl[0].Child[6].Child[0].This, GPQuery);
    //});





    LoadQuery(QueryViewCtrl[0].Child[4].Child[0].This, QueryViewCtrl[0].Child[6].Child[0].This, GPQuery);


    
}

function LoadQuery(ObjectHtmlGrid, ObjectHtmlVGrid, GPQuery) {
    
    var data = {
        'SQLstr': GPQuery.SQL
    };
    $.ajax({
        type: "POST",
        url: SysCfg.App.Properties.xRaiz + 'Tools/SQL/frUSQL.svc/OpenSQL',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (response) {
            if (response.ResErr.NotError == true) {
                LoagGrid(ObjectHtmlGrid, response);
                LoadVerticalGrid(ObjectHtmlVGrid, response);                
            }
            else {
                $(ObjectHtml).html("");
            }            
        },
        error: function (response) {
            SysCfg.Log.Methods.WriteLog("QueryView.js  LoadQuery " + response);
        }
    });
}

function LoagGrid(ObjectHtmlGrid, response) {
    $(ObjectHtmlGrid).html("");

    var ContResponsive = Udiv(ObjectHtmlGrid, "IDTableResponsive");
    BootStrapCreateTableResponsive(ContResponsive);

    ContResponsive.style.height = "300px";

    var GridTable = VclGridTable(ContResponsive, "IdTablaQuery", response.DataTableJs, function myfunction() {
        alert("El ID de esta fila es: " + this.getElementsByTagName("td")[0].innerHTML);
    });
    BootStrapCreateTable(GridTable);
}

function LoadVerticalGrid(ObjectHtmlVGrid, response) {
    
    $(ObjectHtmlVGrid).html("");
    VerticalGridDef = new Array(response.DataTableJs.Columnas.length); // rows
    for (var i = 0; i < response.DataTableJs.Columnas.length; i++) {
        VerticalGridDef[i] = new Array(2); // col
    }

    var VerticalGrid = VclDivitions(ObjectHtmlVGrid, "VerticalGrid", VerticalGridDef);

    for (var i = 0; i < response.DataTableJs.Columnas.length; i++) {
        BootStrapCreateColumns(VerticalGrid[i].Child, [[2, 10], [2, 10], [2, 10], [2, 10], [2, 10]]);

        //VerticalGrid[i].Child[0].This.style.padding = "30px";
        Vcllabel(VerticalGrid[i].Child[0].This, "", TVCLType.BS, TlabelType.H0, response.DataTableJs.Columnas[i].NombreColumna);
        var Textbox1 = new TVclTextBox(VerticalGrid[i].Child[1].This, "");
        Textbox1.Text = "Write a text" + i;
        Textbox1.VCLType = TVCLType.BS;
    }
}
