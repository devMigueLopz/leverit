﻿var TNavTabControls = function (inObjectHtml, inID) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.ObjectHtml = inObjectHtml;
    this.StatusInitialize = false;//Indica si se agrego un tab al control
    this.AutoScroll = false;
    this.VerticalPosition = false;
    this.AutoScrollHeight = 120;
    this.PaddingTopNavBar = 10;
    this.PaddingNavBar = 5;
    this.PaddingTopNavBarMobile = 3;
    this.PaddingNavBarMobile = 3;
    this.TextColorPanel = "#757575";
    this.ID = null;
    this._SetScroll = false;
    this._TextColor = null;
    Object.defineProperty(this, 'TextColor', {
        get: function () {
            return this._TextColor;
        },
        set: function (inValue) {

            this._TextColor = inValue;
            if (this.StatusInitialize && inValue != null) {
                this.ObjectHtml.style.color = inValue;
            }


        }
    });
    Object.defineProperty(this, 'SetScroll', {
        get: function () {
            return _this._SetScroll;
        },
        set: function (inValue) {
            _this._SetScroll = inValue;

            if (inValue) {
                this.ContainerNavTabControlDivScroll.style.overflow = "auto";
                this.ContainerNavTabControl.style.width = "3000px";
            } else {
                this.ContainerNavTabControlDivScroll.style.overflow = "";
                this.ContainerNavTabControl.style.width = "100%";
            }
        }
    });
    Object.defineProperty(this, 'BackgroundHeader', {
        get: function () {
            return this.TabControl.TabsContainer.style.background;
        },
        set: function (inValue) {
            if (!this.VerticalPosition) {
                this.HeaderTabControl.Column[0].This.style.background = inValue;
                this.TabControl.TabsContainer.style.background = inValue;
            }

        }
    });
    Object.defineProperty(this, 'OnChangeTab', {
        set: function (inValue) {
            this.TabControl.OnChangeTab = inValue;

        }
    });
    Object.defineProperty(this, 'Visible', {
        get: function () {
            if (this.ContainerNavTabControl.style.display == "") {
                return true;
            } else {
                return false;
            }
        },
        set: function (inValue) {
            if (inValue) {
                this.ContainerNavTabControl.style.display = "";
            } else {
                this.ContainerNavTabControl.style.display = "none";
            }
        }
    });
    Object.defineProperty(this, 'VisibleHeaderTabControl', {

        set: function (inValue) {

            if (this.TabControl != null) {
                if (inValue) {
                    this.HeaderTabControl.Row.This.style.display = "";

                } else {
                    this.HeaderTabControl.Row.This.style.display = "none";
                }

            }
        }
    });
    Object.defineProperty(this, 'VisibleTabsContainer', {
        get: function () {
            if (this.TabControl.TabsContainer.style.display == "") {
                return true;
            } else {
                return false;
            }
        },
        set: function (inValue) {

            if (this.TabControl != null) {
                if (!this.VerticalPosition) {
                    if (inValue) {

                        this.TabControl.TabsContainer.style.display = "";
                        if (!this.VerticalPosition) {
                            if (SysCfg.App.Properties.Device == SysCfg.App.TDevice.Movile) {
                                this.HeaderTabControl.Column[0].This.style.height = "25px";
                            } else {
                                this.HeaderTabControl.Column[0].This.style.height = "18px";
                            }
                        }
                    } else {
                        this.HeaderTabControl.Column[0].This.style.height = "25px";
                        this.TabControl.TabsContainer.style.display = "none";
                    }
                } else {
                    if (inValue) {
                        this.TabControl.TabsContainer.style.display = "";
                    } else {
                        this.TabControl.TabsContainer.style.display = "none";
                    }
                }


            }
        }
    });

    this.TabPags = new Array();

    this.AddTabPages = function (TabPag) {

        if (!this.StatusInitialize) {
            this.InicializeComponent();
            this.StatusInitialize = true;

            this.TextColor = this._TextColor;
        }
        TabPag.CountPanels = 0;
        TabPag.NavTabControls = _this;
        TabPag.Page.style.paddingBottom = "0px";

        this.TabControl.AddTabPages(TabPag);
        TabPag.ContenNav = document.createElement("div");
        TabPag.Page.appendChild(TabPag.ContenNav);


        if (!this.VerticalPosition) {
            $(TabPag.Tab).addClass("ColorTab");
            TabPag.ContenNav.classList.add("row");
            TabPag.ContenNav.style.marginLeft = "0px";
            TabPag.ContenNav.style.marginRight = "0px";
            TabPag.ContenNav.style.bordeTop = "1px solid #e0e0e0";
            TabPag.ContenNav.style.borderBottom = "1px solid #e0e0e0";
            TabPag.ContenNav.style.borderRight = "1px solid #e0e0e0";
            TabPag.ContenNav.style.borderLeft = "1px solid #e0e0e0";
            TabPag.ContenNav.style.background = "#f3f3f3";
            if (SysCfg.App.Properties.Device == SysCfg.App.TDevice.Movile) {
                TabPag.TabLink.style.paddingLeft = "3px";
                TabPag.TabLink.style.paddingRight = "3px";
                TabPag.TabLink.style.paddingTop = "0px";
                TabPag.TabLink.style.paddingBottom = "0px";
                TabPag.Page.style.fontSize = "13px";

            }
            if (this.TabControl.TabPages.length > 1) {
                this.VisibleTabsContainer = true;
            }
            else {
                this.VisibleTabsContainer = false;
            }
        } else {
            if (this.TabControl.TabPages.length > 1) {
                this.VisibleTabsContainer = true;
            }
            else {
                this.VisibleTabsContainer = false;
            }
            $(TabPag.Tab).addClass("ColorTabBlack");

            TabPag.ContenNav.style.background = "#222d32";

            TabPag.TabLink.style.borderTopLeftRadius = "0px";
            TabPag.TabLink.style.borderTopRightRadius = "0px";
        }

    }
    this.TabControl = null;
    this.ControlModal = null;
    this.InicializeComponentHorizontal = function () {

        this.ContainerFirst = new TVclStackPanel(this.ObjectHtml, inID, 1, [[12], [12], [12], [12], [12]]);
        this.ContainerNavTabControlDivScroll = document.createElement("div");
        this.ContainerNavTabControl = document.createElement("div");
        this.ContainerFirst.Column[0].This.appendChild(this.ContainerNavTabControlDivScroll);
        this.ContainerNavTabControlDivScroll.appendChild(this.ContainerNavTabControl);
        this.HeaderTabControl = new TVclStackPanel(this.ContainerNavTabControl, "1", 1, [[12], [12], [12], [12], [12]]);
        this.HeaderTabControl.Column[0].This.style.height = "18px";
        this.linkIcon = document.createElement("a");
        this.linkIcon.style.cursor = "pointer";
        this.icon = document.createElement("i");
        this.icon.classList.add("icon");
        if (SysCfg.App.Properties.Device == SysCfg.App.TDevice.Movile) {
            this.icon.classList.add("ion-android-more-horizontal");
            this.icon.style.float = "left";
            this.icon.style.marginLeft = "-10px";
            this.HeaderTabControl.Column[0].This.style.height = "25px";
            this.icon.style.marginTop = "2px";
        } else {
            this.icon.classList.add("ion-arrow-down-b");
            this.icon.style.float = "right";
            this.icon.style.marginTop = "2px";
        }
        this.icon.style.color = "#f3f3f3";
        this.linkIcon.appendChild(this.icon);
        this.HeaderTabControl.Column[0].This.appendChild(this.linkIcon);

        this.ContTabControl = new TVclStackPanel(this.ContainerNavTabControl, "1", 1, [[12], [12], [12], [12], [12]]);
        this.TabControl = new TTabControl(this.ContTabControl.Column[0].This, "", "");
        this.ControlModalRow = new TVclStackPanel(this.ContainerNavTabControl, "1", 1, [[12], [12], [12], [12], [12]]);
        this.ControlModal = this.ControlModalRow.Column[0].This;
        this.HeaderTabControl.Column[0].This.style.background = "#227446";
        this.HeaderTabControl.Row.This.classList.remove("row");
        this.ContainerNavTabControlDivScroll.classList.add("ContainerNavTabControlDivScroll");
        this.ContainerNavTabControl.classList.add("ContainerNavTabControl");
        this.linkIcon.onclick = function (e) {
            if (SysCfg.App.Properties.Device == SysCfg.App.TDevice.Movile) {
                $(_this.ContTabControl.Column[0].This).slideToggle();
            } else {
                $(_this.TabControl.PageContainer).slideToggle();
            }
            var evt = e ? e : window.event;
            if (evt.stopPropagation) evt.stopPropagation();
            if (evt.cancelBubble != null) evt.cancelBubble = true;
        }
        this.HeaderTabControl.Column[0].This.style.cursor = "pointer";
        this.HeaderTabControl.Column[0].This.onclick = function () {
            if (SysCfg.App.Properties.Device == SysCfg.App.TDevice.Movile) {
                $(_this.ContTabControl.Column[0].This).slideToggle();
            } else {
                $(_this.TabControl.PageContainer).slideToggle();
            }
        }
        if (SysCfg.App.Properties.Device == SysCfg.App.TDevice.Movile) {

            _this.ContTabControl.Column[0].This.style.display = "none";
        }
    }

    this.InicializeComponentVertical = function () {
        this.TabControl = new TTabControl(this.ObjectHtml, "", "");

    }
    this.InicializeComponent = function () {

        if (this.VerticalPosition) {
            this.InicializeComponentVertical();

            this.TabControl.TabsContainer.style.background = "#181f23";

        } else {
            this.InicializeComponentHorizontal();
            this.TabControl.TabsContainer.style.background = "#227446";
        }


    }

    var PathCSS = ItHelpCenter.ScriptManager.GetStyle("Component/UCode/VCL/NavTabControls/style.css");
    var IDparthCss = "styleNavTabControls";
    ItHelpCenter.ScriptManager.AddStyleFile(PathCSS, IDparthCss, function () {

    }, function (e) {
    });
}
var TNavPanelBar = function (TabPag) {

    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.TabPag = TabPag;
    this.divContenedorHeader = null;
    this.LastWidth = null;
    this.VerticalPosition = true;
    this._SetScroll = false;
    Object.defineProperty(this, 'SetScroll', {
        get: function () {
            return _this._SetScroll;
        },
        set: function (inValue) {
            _this._SetScroll = inValue;

            if (inValue) {
                this.ContainerNavTabControlDivScroll.style.overflow = "auto";
                this.ContainerNavTabControl.style.width = "700px";
            } else {
                this.ContainerNavTabControlDivScroll.style.overflow = "";
                this.ContainerNavTabControl.style.width = "100%";
            }
        }
    });
    if (!this.TabPag.NavTabControls.VerticalPosition) {
        var divA1 = document.createElement('div');
        this.divContenedor = document.createElement('div');
        this.divContenedor.style.float = "left";
        this.divContenedor.appendChild(divA1);
        var tablePanel = new Utable(divA1, "");
        var tableBodyPanel = Utbody(tablePanel, ID);
        var Fila1Panel = Utr(tableBodyPanel, "");
        var Td1Panel = Utd(Fila1Panel, "");
        var Fila2Panel = Utr(tableBodyPanel, "");
        var Td2Panel = Utd(Fila2Panel, "");

        this.RowBody = document.createElement('div');
        var tableBody = new Utable(this.RowBody, "");
        var tableBodyBody = Utbody(tableBody, ID);
        tableBody.style.width = "100%";
        var Fila1Body = Utr(tableBodyBody, "");
        this.RowBody.marginLeft = "0px";
        this.RowBody.marginRight = "0px";
        this.pText = document.createElement('div');
        this.pText.style.color = "#757575"
        this.RowTitle = document.createElement('div');
        this.RowTitle.classList.add("text-center");
        this.RowTitle.marginLeft = "0px";
        this.RowTitle.marginRight = "0px";
        this.RowTitle.appendChild(this.pText);
        Td1Panel.appendChild(this.RowBody);
        Td2Panel.appendChild(this.RowTitle);
        divA1.style.borderRight = "1px solid #e0e0e0";
        if (SysCfg.App.Properties.Device == SysCfg.App.TDevice.Movile) {
            this.pText.style.marginBottom = "0px";
            divA1.style.paddingLeft = _this.TabPag.NavTabControls.PaddingNavBarMobile + "px";
            divA1.style.paddingRight = _this.TabPag.NavTabControls.PaddingNavBarMobile + "px";
        } else {
            this.pText.style.marginBottom = _this.TabPag.NavTabControls.PaddingTopNavBar + "px";
            divA1.style.paddingLeft = _this.TabPag.NavTabControls.PaddingNavBar + "px";
            divA1.style.paddingRight = _this.TabPag.NavTabControls.PaddingNavBar + "px";
        }

    }
    else {

        this.divContenedor = document.createElement('div');
        this.divContenedor.style.backgroundColor = "#222d32";
        this.divContenedor.style.paddingLeft = "5px";
        this.divContenedor.style.paddingRight = "5px";
        this.divContenedor.style.paddingBottom = "4px";
        this.divContenedor.style.paddingTop = "5px";
        this.divContenedor.style.marginBottom = "5px";
        if (this.TabPag.CountPanels == 0) {
            //this.divContenedor.style.paddingTop = "5px";

        }
        else {
            //this.divContenedor.style.borderTop = "1px solid #c4c4c4";
            this.divContenedorHeader = document.createElement('div');
            this.divContenedorHeader.style.paddingLeft = "7.5px";
            var divContenedorHeader2 = document.createElement('div');
            divContenedorHeader2.style.borderTop = "1px solid #c4c4c4";
            divContenedorHeader2.style.height = "1px";
            this.divContenedorHeader.appendChild(divContenedorHeader2);
            this.TabPag.ContenNav.appendChild(this.divContenedorHeader);
        }
        this.TabPag.CountPanels++;
        var divA1 = document.createElement('div');
        this.pText = document.createElement('div');
        this.pText.style.fontSize = "16px";
        this.pText.style.fontWeight = "400";


        this.ContainerNavTabControlDivScroll = document.createElement("div");
        this.ContainerNavTabControl = document.createElement("div");
        this.ContainerNavTabControlDivScroll.appendChild(this.ContainerNavTabControl);
        this.divContenedor.appendChild(this.ContainerNavTabControlDivScroll);


        this.ContainerNavTabControl.appendChild(this.pText);
        this.ContainerNavTabControl.appendChild(divA1);

    }

    var tabPage = TabPag.ContenNav;
    this.divContenedor.style.color = this.TabPag.NavTabControls.TextColorPanel;
    tabPage.appendChild(this.divContenedor);
    this._Title = null;
    this._TitleColor = null;
    Object.defineProperty(this, 'Title', {
        get: function () {
            return this._Title;
        },
        set: function (inValue) {
            this._Title = inValue;
            $(this.pText).html("");
            $(this.pText).html(inValue);

        }
    });
    Object.defineProperty(this, 'TitleColor', {
        get: function () {
            return this._TitleColor;
        },
        set: function (inValue) {
            this._TitleColor = inValue;
            this.pText.style.color = inValue;


        }
    });
    Object.defineProperty(this, 'Visible', {
        get: function () {
            if (this.divContenedor.style.display == "") {
                return true;
            } else {
                return false;
            }
        },
        set: function (inValue) {
            if (inValue) {
                if (this.divContenedorHeader != null)
                    this.divContenedorHeader.style.display = "";
                this.divContenedor.style.display = "";
            } else {
                if (this.divContenedorHeader != null)
                    this.divContenedorHeader.style.display = "none";
                this.divContenedor.style.display = "none";
            }
        }
    });
    this.ListItemBar = new Array();
    this.CountWidth = 0;
    this.MaxHeight = 0;
    this.addItemBar = function (itemBar) {
        this.ListItemBar.push(itemBar);
        itemBar.NavPanelBar = _this;

        if (!this.TabPag.NavTabControls.VerticalPosition) {
            itemBar.InitilizeStyle();
            var TD1Body = Utd(Fila1Body, "");
            if (this.ListItemBar.length == 1) {
                itemBar.divContentSubPanel.style.marginLeft = "0px";
            }
            TD1Body.appendChild(itemBar.divContentSubPanel);
            if (_this.TabPag.NavTabControls.AutoScroll && !_this.TabPag.NavTabControls.SetScroll) {
                var heightDiv = _this.TabPag.NavTabControls.AutoScrollHeight;
                if (heightDiv > _this.TabPag.Page.offsetHeight) {
                    _this.TabPag.NavTabControls.SetScroll = true;

                }
            }
        }
        else {

            if (this.VerticalPosition) {
                itemBar.InitilizeStyle();
                divA1.appendChild(itemBar.divContentSubPanel);
                if (itemBar.Type == "TNavBar" && itemBar.Border == "icon" && itemBar.ControlBar == null) {
                    var width = 21;

                    if (itemBar._Size == "normal") {
                        width = 29;
                    }
                    if (itemBar._Size == "big") {
                        width = 41;
                    }
                    if (this.LastWidth == null) {
                        this.LastWidth = width;
                        itemBar.DivContentLinkRight.style.width = width + "px";
                    } else {
                        if (this.LastWidth < width) {
                            this.LastWidth = width;
                            this.isAllUpdateWidthPanelBarRight(this.LastWidth);

                        } else {
                            itemBar.DivContentLinkRight.style.width = this.LastWidth + "px";
                        }

                    }
                    //this.isAllUpdateWidthPanelBarRight();

                }

            }
            else {
                if (this.CountWidth == 0) {
                    this.tableBody = new Utable(divA1, "");
                    this.tableBodyBody = Utbody(this.tableBody, ID);
                    this.tableBody.style.marginTop = "2px";
                    this.Fila1Body = Utr(this.tableBodyBody, "");

                }
                //this.CountWidth++;
                var TD1Body = Utd(this.Fila1Body, "");
                            

                itemBar.InitilizeStyleSecondForm();
                TD1Body.appendChild(itemBar.divContentSubPanel);
              
                this.CountWidth = this.CountWidth + itemBar.divContentSubPanel.offsetWidth;

                if (this.CountWidth > this.tableBody.offsetWidth) {
                    _this.SetScroll = true;
                }
                

            }

        }
    }
    this.isAllVisibleItem = function () {
        var Visible = false;
        for (var i = 0, t = this.ListItemBar.length; i < t; i++) {
            if (this.ListItemBar[i].Visible) {
                Visible = true;
                break;
            }
        }
        this.Visible = Visible;
    }
    this.isAllUpdateWidthPanelBarRight = function (width) {
        for (var i = 0, t = this.ListItemBar.length; i < t; i++) {
            if (this.ListItemBar[i].Type == "TNavBar" && this.ListItemBar[i].Border == "icon") {
                this.ListItemBar[i].DivContentLinkRight.style.width = width + "px";
            }
        }
    }
}
var TNavBar = function () {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.Type = "TNavBar";
    this.NavPanelBar = null;
    this._Text = "";
    this.Style = 0;
    this._ToolTip = "";
    this._SrcImg = "";
    this._ChekedButton = false;
    this.ClickDown = false;
    this._Disabled = false;
    this._Enabled = true;
    this._onclick = null;
    this._Opacity = null;
    this._Width = null;
    this._TextColor = null;
    this._Visible = null;
    this._Size = "normal";//Estados: small-normal-big
    this._Border = "icon";//Estados: icon-full
    this.TypeCheked = 0;
    this.ControlBar = null;
    this.ControlButton = null;
    Object.defineProperty(this, 'Size', {//revisar como se comporta en vertical
        get: function () {
            return this._Size;
        },
        set: function (inValue) {
            this._Size = inValue;
            if (this.NavPanelBar != null && this.NavPanelBar.TabPag.NavTabControls.VerticalPosition) {
                if (this.Border == "icon" && (!this.ChekedButton || (this.ChekedButton && this.TypeCheked != 0) || (this.ChekedButton && this.TypeCheked == 0 && !this.NavPanelBar.VerticalPosition))) {
                    if (this.NavPanelBar.VerticalPosition) {
                        if (inValue == "small") {
                            this.divContentLink.style.width = "20px";
                            this.divContentLink.style.height = "20px";
                            this.divContentDivText.style.paddingLeft = "5px";
                            this.divContentDivText.style.paddingTop = "4px";
                            var parentLink = this.divContentLink;
                            this.Img.style.width = "16px";
                            $(parentLink).css("border", "1px solid #c4c4c4");
                            parentLink.style.paddingTop = "0x";
                            parentLink.style.paddingLeft = "1px";
                            parentLink.style.borderRadius = "2px";
                            this.Img.style.marginBottom = "5px";

                        }
                        if (inValue == "normal") {
                            this.Img.style.width = "20px";
                            this.divContentLink.style.width = "28px";
                            this.divContentLink.style.height = "28px";
                            this.divContentDivText.style.paddingLeft = "5px";
                            this.divContentDivText.style.paddingTop = "9px";
                            var parentLink = this.divContentLink;
                            $(parentLink).css("border", "1px solid #c4c4c4");

                            parentLink.style.paddingTop = "1.5px";
                            //parentLink.style.paddingBottom = "5px";
                            //parentLink.style.paddingRight = "5px";
                            parentLink.style.paddingLeft = "3.4px";
                            parentLink.style.borderRadius = "2px";

                        }
                        if (inValue == "big") {
                            this.Img.style.width = "24px";
                            this.divContentLink.style.width = "40px";
                            this.divContentLink.style.height = "40px";
                            this.divContentDivText.style.paddingLeft = "5px";
                            this.divContentDivText.style.paddingTop = "5px";
                            var parentLink = this.divContentLink;
                            $(parentLink).css("border", "1px solid #c4c4c4");
                            parentLink.style.paddingTop = "7px";
                            parentLink.style.paddingBottom = "7px";
                            parentLink.style.paddingRight = "7px";
                            parentLink.style.paddingLeft = "7px";
                            parentLink.style.borderRadius = "2px";

                        }
                    }
                    else {
                        if (inValue == "small") {
                            this.Img.style.width = "20px";
                        } else if (inValue == "normal") {
                            this.Img.style.width = "24px";

                        } else if (inValue == "big") {
                            this.Img.style.width = "34px";
                        }
                    }


                }
            }

        }
    });

    Object.defineProperty(this, 'Border', {//revisar como se comporta en vertical
        get: function () {
            return this._Border;
        },
        set: function (inValue) {
            this._Border = inValue;
        }
    });
    Object.defineProperty(this, 'Text', {//revisar como se comporta en vertical
        get: function () {
            return this._Text;
        },
        set: function (inValue) {
            this._Text = inValue;
            if (this.NavPanelBar != null) {
                $(this.pText).html("");
                var textTemp = "";
                var textBar = inValue;
                var textBar2 = "&nbsp;";
                if (inValue.length > 14) {
                    textBar = inValue.substring(0, 13);
                    textBar2 = inValue.substring(13, inValue.length);
                    if (textBar2.length > 14) {
                        textBar2 = textBar2.substring(0, 10) + "...";

                    } else {
                        textBar2 = textBar2.substring(0, textBar2.length);
                    }
                }
                if (this.NavPanelBar.TabPag.NavTabControls.VerticalPosition) {
                    if (textBar2 != "") {

                        textTemp = textBar + "&nbsp;" + textBar2;
                    }
                    else {
                        textTemp = textBar + textBar2;
                    }
                    if (this.NavPanelBar.VerticalPosition) {
                        if (this.Border == "icon") {
                            if (this.ControlBar != null) {
                                $(this.ControlBar.lbl).html("");
                                this.ControlBar.Text = textTemp;
                            }
                            else {
                                $(this.pText).html("");
                                $(this.pText).html(textTemp);
                            }
                            this.pText.style.lineHeight = "1";
                        }
                        else {
                            this.ControlBar.Text = textTemp;
                            //this.ControlBar.lbl.fontSize = "14px";
                        }
                    }
                    else {
                        if (this.ControlBar != null) {
                            this.ControlBar.Text = textTemp;
                            this.ControlBar.lbl.fontSize = "14px";
                        } else {
                            textTemp = textBar;
                            $(this.pText).html("");
                            $(this.pText).html(textTemp);
                        }

                    }

                }
                else {
                    textTemp = textBar + "<br>" + textBar2;
                    $(this.pText).html("");
                    $(this.pText).html(textTemp);
                }


                this.ToolTip = "" + this._Text;

            }

        }
    });
    Object.defineProperty(this, 'ToolTip', {
        get: function () {
            return this._ToolTip;
        },
        set: function (inValue) {

            this._ToolTip = inValue;
            if (this.NavPanelBar != null) {
                if (this._SrcImg != "") {
                    this.Img.title = inValue;
                }
            }

        }
    });
    Object.defineProperty(this, 'SrcImg', {
        get: function () {
            return this._SrcImg;
        },
        set: function (inValue) {
            this._SrcImg = inValue;
            if (this.NavPanelBar != null) {
                if (this.NavPanelBar.TabPag.NavTabControls.VerticalPosition) {
                    if (this.Border == "icon") {
                        this.Img.src = inValue;
                        this.ToolTip = "" + this._Text;
                    } else {
                        this.ControlBar.Src = inValue;
                        this.ToolTip = "" + this._Text;
                    }
                } else {
                    this.Img.src = inValue;
                    this.ToolTip = "" + this._Text;
                }


            }

        }
    });
    Object.defineProperty(this, 'onclick', {
        get: function () {
            return this._onclick;
        },
        set: function (inValue) {
            this._onclick = inValue;
            if (this.NavPanelBar != null) {
                if (!this.NavPanelBar.TabPag.NavTabControls.VerticalPosition) {
                    this.Link.onclick = function () {
                        if (_this._Disabled == false) {
                            if (_this.ChekedButton) {
                                _this.ClickDown = !_this.ClickDown;
                                _this.Cheked = _this.ClickDown;
                            }
                            inValue(_this);
                        }
                    };
                }
                else {
                    if (this.ControlBar != null) {
                        this.ControlBar.onClick = function () {
                            if (_this._Disabled == false) {
                                if (_this.ChekedButton) {
                                    _this.ClickDown = !_this.ClickDown;
                                    _this.Cheked = _this.ClickDown;
                                }
                                inValue(_this);
                            }
                        };
                    } else {
                        if (this.NavPanelBar.VerticalPosition) {
                            this.DivContentSubPanelStack.Column[0].This.onclick = function () {
                                if (_this._Disabled == false) {
                                    if (_this.ChekedButton) {
                                        _this.ClickDown = !_this.ClickDown;
                                        _this.Cheked = _this.ClickDown;
                                    }
                                    inValue(_this);
                                }
                            };
                        }
                        else {
                            this.Img.style.cursor = "pointer";
                            this.Img.onclick = function () {
                                if (_this._Disabled == false) {
                                    if (_this.ChekedButton) {
                                        _this.ClickDown = !_this.ClickDown;
                                        _this.Cheked = _this.ClickDown;
                                    }
                                    inValue(_this);
                                }
                            };
                        }

                    }

                }

            }

        }
    });
    Object.defineProperty(this, 'ChekedButton', {
        get: function () {
            return this._ChekedButton;
        },
        set: function (inValue) {
            this._ChekedButton = inValue;
        }
    });
    Object.defineProperty(this, 'Cheked', {//revisar como se comporta en posicion vertical
        get: function () {
            return this.ClickDown;
        },
        set: function (_Value) {
            this.ClickDown = _Value;
            if (this.NavPanelBar != null) {
                if (!this.NavPanelBar.TabPag.NavTabControls.VerticalPosition) {
                    if (this.ClickDown) {
                        $(this.divContentSubPanel).css("border", "1px solid #c4c4c4");
                        this.divContentSubPanel.style.marginTop = "2px";
                        if (SysCfg.App.Properties.Device == SysCfg.App.TDevice.Movile) {
                            if (this.NavPanelBar.TabPag.NavTabControls.PaddingTopNavBarMobile > 2) {
                                this.Img.style.marginTop = (this.NavPanelBar.TabPag.NavTabControls.PaddingTopNavBarMobile - 2) + "px";
                            }
                        } else {
                            if (this.NavPanelBar.TabPag.NavTabControls.PaddingTopNavBar > 2) {
                                this.Img.style.marginTop = (this.NavPanelBar.TabPag.NavTabControls.PaddingTopNavBar - 2) + "px";
                            }
                        }
                        this.divContentSubPanel.style.paddingLeft = "5px";
                        this.divContentSubPanel.style.paddingRight = "5px";
                        this.divContentSubPanel.style.borderRadius = "1px";
                        this.divContentSubPanel.style.background = "#FAFAFA";
                        this.divContentSubPanel.style.boxShadow = "1px 1px #c4c4c4";
                    }
                    else {
                        $(this.divContentSubPanel).css("border", "0px solid #c4c4c4");

                        if (SysCfg.App.Properties.Device == SysCfg.App.TDevice.Movile) {
                            this.divContentSubPanel.style.marginTop = this.NavPanelBar.TabPag.NavTabControls.PaddingTopNavBarMobile + "px";
                            this.divContentSubPanel.style.marginLeft = this.NavPanelBar.TabPag.NavTabControls.PaddingNavBarMobile + "px";

                        } else {
                            this.divContentSubPanel.style.marginTop = this.NavPanelBar.TabPag.NavTabControls.PaddingTopNavBar + "px";
                            this.divContentSubPanel.style.marginLeft = this.NavPanelBar.TabPag.NavTabControls.PaddingNavBar + "px";
                        }
                        this.Img.style.marginTop = "0px";
                        this.divContentSubPanel.style.paddingLeft = "0px";
                        this.divContentSubPanel.style.paddingRight = "0px";
                        this.divContentSubPanel.style.borderRadius = "1px";
                        this.divContentSubPanel.style.background = "";
                        this.divContentSubPanel.style.boxShadow = "";
                    }
                }
                else {
                    if (this.Border == "icon") {
                        var parentDiv = this.Link.parentNode;
                        if (this.ControlBar != null) {
                            this.ControlBar.Checked = _Value;
                        } else {
                            if (this.ClickDown) {

                                parentDiv.style.background = "#FAFAFA";
                                parentDiv.style.boxShadow = "1px 1px #c4c4c4";
                            } else {

                                parentDiv.style.background = "";
                                parentDiv.style.boxShadow = "0px 0px #c4c4c4";
                            }
                        }
                    }


                }
            }
        }
    });
    Object.defineProperty(this, 'ChekedClickDown', {
        get: function () {
            return this.Cheked;
        },
        set: function (_Value) {

            this.Cheked = _Value;
        }
    });
    Object.defineProperty(this, 'Opacity', {
        get: function () {
            return this._Opacity;
        },
        set: function (inValue) {
            this._Opacity = inValue;
            if (this.NavPanelBar != null && inValue != null) {
                this.Img.style.opacity = inValue;
            }

        }
    });
    Object.defineProperty(this, 'Width', {
        get: function () {
            return this._Width;
        },
        set: function (inValue) {
            this._Width = null;
            if (this.NavPanelBar != null && inValue != null) {
                this.Img.style.width = inValue;
            }

        }
    });
    Object.defineProperty(this, 'TextColor', {
        get: function () {
            return this._TextColor;
        },
        set: function (inValue) {
            this._TextColor = inValue;
            if (this.NavPanelBar != null && inValue != null) {
                this.pText.style.color = inValue;
            }
        }
    });
    Object.defineProperty(this, 'Visible', {
        get: function () {
            return this._Visible;
        },
        set: function (inValue) {
            this._Visible = inValue;
            if (this.NavPanelBar != null && inValue != null) {
                if (inValue) {
                    this.divContentSubPanel.style.display = "";
                } else {
                    this.divContentSubPanel.style.display = "none";
                }
                this.NavPanelBar.isAllVisibleItem();
            }
        }
    });
    Object.defineProperty(this, 'Disabled', {
        get: function () {
            return this._Disabled;

        },
        set: function (inValue) {
            this._Disabled = inValue;
            if (this.NavPanelBar != null) {
                if (this.NavPanelBar.TabPag.NavTabControls.VerticalPosition) {
                    if (inValue) {
                        if (this.NavPanelBar.VerticalPosition) {
                            this.DivContentSubPanelStack.Column[0].This.style.cursor = "not-allowed";
                        } else {
                            this.divContentSubPanel.style.cursor = "not-allowed";
                        }
                        this.Opacity = "0.65";
                    } else {
                        if (this.NavPanelBar.VerticalPosition) {
                            this.DivContentSubPanelStack.Column[0].This.style.cursor = "pointer";
                        } else {
                            this.divContentSubPanel.style.cursor = "pointer";
                        }

                        this.Opacity = "";
                    }
                } else {
                    if (inValue) {
                        this.Img.style.cursor = "not-allowed";
                        this.Opacity = "0.65";
                    } else {
                        this.Img.style.cursor = "pointer";
                        this.Opacity = "";
                    }
                }

            }

        }
    });
    Object.defineProperty(this, 'Enabled', {
        get: function () {
            return this._Enabled;
        },
        set: function (inValue) {
            this._Enabled = inValue;
            this._Disabled = !inValue;
            if (this.NavPanelBar != null) {
                if (this.NavPanelBar.TabPag.NavTabControls.VerticalPosition) {
                    if (inValue) {
                        if (this.NavPanelBar.VerticalPosition) {
                            this.DivContentSubPanelStack.Column[0].This.style.cursor = "pointer";
                        } else {
                            this.divContentSubPanel.style.cursor = "pointer";
                        }
                        this.Opacity = "";
                    } else {
                        if (this.NavPanelBar.VerticalPosition) {
                            this.DivContentSubPanelStack.Column[0].This.style.cursor = "not-allowed";
                        } else {
                            this.divContentSubPanel.style.cursor = "not-allowed";
                        }
                        //this.DivContentSubPanelStack.Column[0].This.style.cursor = "pointer";
                        this.Opacity = "0.65";
                    }
                } else {
                    if (!inValue) {
                        this.Img.style.cursor = "not-allowed";
                        this.Opacity = "0.65";
                    } else {
                        this.Img.style.cursor = "pointer";
                        this.Opacity = "";
                    }
                }

            }

        }
    });
    this.onclick = function () { };
    this.InitilizeStyleborrar = function () {
        this.Img = document.createElement('img');
        this.Img.style.width = "24px";
        this.Link = document.createElement('a');
        this.Link.style.cursor = "pointer";
        this.Link.appendChild(this.Img);
        this.pText = document.createElement('div');
        this.divText = document.createElement('div');

        this.divText.appendChild(this.pText);
        this.divContentSubPanel = document.createElement('div');
        if (!this.NavPanelBar.TabPag.NavTabControls.VerticalPosition) {
            this.divText.classList.add("text-center");
            this.divContentSubPanel.style.textAlign = "center";
            this.divContentSubPanel.appendChild(this.Link);
            this.divContentSubPanel.appendChild(this.divText);
            if (SysCfg.App.Properties.Device == SysCfg.App.TDevice.Movile) {
                this.divContentSubPanel.style.marginTop = this.NavPanelBar.TabPag.NavTabControls.PaddingTopNavBarMobile + "px";
                this.divContentSubPanel.style.marginLeft = this.NavPanelBar.TabPag.NavTabControls.PaddingNavBarMobile + "px";
                this.divContentSubPanel.style.marginRight = "0px";
            }
            else {

                this.divContentSubPanel.classList.add("ColorSelect");
                this.divContentSubPanel.style.marginTop = this.NavPanelBar.TabPag.NavTabControls.PaddingTopNavBar + "px";
                this.divContentSubPanel.style.marginLeft = this.NavPanelBar.TabPag.NavTabControls.PaddingNavBar + "px";
                this.divContentSubPanel.style.marginRight = "0px";
            }

        }
        else {

            this.DivContentSubPanelStack = new TVclStackPanel(this.divContentSubPanel, "1", 1, [[12], [12], [12], [12], [12]]);
            var DivContentLinkRight = document.createElement("div");
            this.divContentLink = document.createElement("div");

            this.divContentDivText = document.createElement("div");
            this.DivContentSubPanelStack.Column[0].This.classList.add("hoverSelectionBar");
            this.DivContentSubPanelStack.Column[0].This.style.cursor = "pointer";
            this.DivContentSubPanelStack.Column[0].This.appendChild(this.divContentDivText);

            this.DivContentSubPanelStack.Column[0].This.appendChild(this.divContentLink);

            this.DivContentSubPanelStack.Column[0].This.style.paddingTop = "3px";
            this.DivContentSubPanelStack.Column[0].This.style.paddingBottom = "3px";
            this.divContentLink.style.float = "right";
            this.divContentDivText.style.float = "left";

            this.divContentLink.appendChild(this.Link);
            this.divContentDivText.appendChild(this.divText);
            this.divText.style.fontSize = "14px";
            this.divText.style.fontWeight = "400";
            this.Size = this._Size;
        }
        this.ChekedButton = this._ChekedButton;
        this.Cheked = this.Cheked;
        this.Disabled = this._Disabled;
        this.Visible = this._Visible;
        this.TextColor = this._TextColor;
        this.Opacity = this._Opacity;
        this.Width = this._Width;
        this.SrcImg = this._SrcImg;
        this.ToolTip = this._ToolTip;
        this.Text = this._Text;
        this.onclick = this._onclick;

    }
    this.InitilizeStyle = function () {
        this.Img = document.createElement('img');
        this.Img.style.width = "24px";
        this.Link = document.createElement('a');
        this.Link.style.cursor = "pointer";
        this.Link.appendChild(this.Img);
        this.pText = document.createElement('div');
        this.divText = document.createElement('div');

        this.divText.appendChild(this.pText);
        this.divContentSubPanel = document.createElement('div');
        if (!this.NavPanelBar.TabPag.NavTabControls.VerticalPosition) {
            this.divText.classList.add("text-center");
            this.divContentSubPanel.style.textAlign = "center";
            this.divContentSubPanel.appendChild(this.Link);
            this.divContentSubPanel.appendChild(this.divText);
            if (SysCfg.App.Properties.Device == SysCfg.App.TDevice.Movile) {
                this.divContentSubPanel.style.marginTop = this.NavPanelBar.TabPag.NavTabControls.PaddingTopNavBarMobile + "px";
                this.divContentSubPanel.style.marginLeft = this.NavPanelBar.TabPag.NavTabControls.PaddingNavBarMobile + "px";
                this.divContentSubPanel.style.marginRight = "0px";
            }
            else {

                this.divContentSubPanel.classList.add("ColorSelect");
                this.divContentSubPanel.style.marginTop = this.NavPanelBar.TabPag.NavTabControls.PaddingTopNavBar + "px";
                this.divContentSubPanel.style.marginLeft = this.NavPanelBar.TabPag.NavTabControls.PaddingNavBar + "px";
                this.divContentSubPanel.style.marginRight = "0px";
            }

        }
        else {

            this.DivContentSubPanelStack = new TVclStackPanel(this.divContentSubPanel, "1", 1, [[12], [12], [12], [12], [12]]);
            this.DivContentLinkRight = document.createElement("div");
            this.divContentLink = document.createElement("div");

            this.divContentDivText = document.createElement("div");
            this.DivContentSubPanelStack.Column[0].This.classList.add("hoverSelectionBar");
            this.DivContentSubPanelStack.Column[0].This.style.cursor = "pointer";
            this.DivContentSubPanelStack.Column[0].This.appendChild(this.divContentDivText);
            this.DivContentLinkRight.appendChild(this.divContentLink);
            this.DivContentSubPanelStack.Column[0].This.appendChild(this.DivContentLinkRight);


            this.DivContentSubPanelStack.Column[0].This.style.paddingTop = "3px";
            this.DivContentSubPanelStack.Column[0].This.style.paddingBottom = "3px";
            //this.divContentLink.style.float = "right";
            this.DivContentLinkRight.style.float = "right";
            this.DivContentLinkRight.style.width = "41px";


            //this.divContentDivText.style.paddingLeft = "5px";
            //this.divContentDivText.style.paddingTop = "12px";
            //if (this._Size) {

            //}

            if (this.Border == "icon") {
                this.divContentDivText.style.float = "left";
                if (this.ChekedButton && this.TypeCheked == 0) {
                    this.divContentDivText.style.paddingLeft = "5px";
                    this.ControlBar = new TVclInputcheckbox(this.divContentDivText, "");
                    this.ControlBar.Text = this.Text;
                    this.divContentDivText.appendChild(this.divText);
                } else {

                    this.divContentLink.appendChild(this.Link);
                    this.divContentDivText.appendChild(this.divText);
                }



            }
            else {
                //this.divContentDivText.style.textAlign = "center";
                this.divContentDivText.style.marginLeft = "5px";
                //this.divContentDivText.width="95%"
                this.ControlBar = new TVclButton(this.divContentDivText, "btnResolve");
                this.ControlBar.Cursor = "pointer";
                this.ControlBar.VCLType = TVCLType.BS;
                this.ControlBar.This.style.minWidth = "100%";
                this.ControlBar.This.classList.add("btn-xs");
                this.ControlBar.This.style.backgroundColor = "transparent";
                this.ControlBar.This.style.color = "inherit";


            }

            this.divText.style.fontSize = "14px";
            this.divText.style.fontWeight = "400";
            this.Size = this._Size;

        }
        this.ChekedButton = this._ChekedButton;
        this.Cheked = this.Cheked;
        this.Disabled = this._Disabled;
        this.Visible = this._Visible;
        this.TextColor = this._TextColor;
        this.Opacity = this._Opacity;
        this.Width = this._Width;
        this.SrcImg = this._SrcImg;
        this.ToolTip = this._ToolTip;
        this.Text = this._Text;
        this.onclick = this._onclick;

    }

    this.InitilizeStyleSecondForm = function () {
        this.Img = document.createElement('img');
        //this.Img.style.width = "24px";
        this.Link = document.createElement('a');
        this.Link.style.cursor = "pointer";
        this.Link.appendChild(this.Img);
        this.pText = document.createElement('div');
        this.divText = document.createElement('div');


        this.divContentSubPanel = document.createElement('div');
        this.divText.classList.add("text-center");
        this.divContentSubPanel.style.textAlign = "center";
        this.divContentSubPanel.appendChild(this.Link);
        this.divContentSubPanel.appendChild(this.divText);
        this.divContentSubPanel.style.marginLeft = "5px";

        if (this.ChekedButton && this.TypeCheked == 0) {
            this.ControlBar = new TVclInputcheckbox(this.divText, "");
            this.ControlBar.Text = this.Text;

        } else {
            this.divText.appendChild(this.pText);
        }


        this.ChekedButton = this._ChekedButton;
        this.Cheked = this.Cheked;
        this.Disabled = this._Disabled;
        this.Visible = this._Visible;
        this.TextColor = this._TextColor;
        this.Opacity = this._Opacity;
        this.Width = this._Width;
        this.SrcImg = this._SrcImg;
        this.ToolTip = this._ToolTip;
        this.Text = this._Text;
        this.onclick = this._onclick;
        this.Size = this._Size;
    }
}
var TNavBarStackPanel = function () {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.Style = 0;
    this.Type = "TNavBarStackPanel";
    this.NavPanelBar = null;
    this.pText = document.createElement('p');
    this.divText = document.createElement('div');
    this.divText.classList.add("text-center");
    this.divText.appendChild(this.pText);
    this.divContentSubPanel = document.createElement('div');

    this.divContentSubPanel.style.textAlign = "center";
    this.Panel = document.createElement("div");
    this.divContentSubPanel.appendChild(this.Panel);
    this.divContentSubPanel.appendChild(this.divText);

    this._Text = "";
    this._ToolTip = "";
    this._Visible = null;
    this._TextColor = null;
    Object.defineProperty(this, 'Text', {
        get: function () {
            return this._Text;
        },
        set: function (inValue) {
            this._Text = inValue;
            if (this.NavPanelBar != null) {
                $(this.pText).html("");
                $(this.pText).html(inValue);
            }
        }
    });
    Object.defineProperty(this, 'TextColor', {
        get: function () {
            return this._TextColor;
        },
        set: function (inValue) {
            this._TextColor = inValue;
            if (this.NavPanelBar != null && inValue != null) {
                this.pText.style.color = inValue;
            }
        }
    });
    Object.defineProperty(this, 'Visible', {
        get: function () {
            return this._Visible;
        },
        set: function (inValue) {
            this._Visible = null;
            if (this.NavPanelBar != null && inValue != null) {
                if (inValue) {
                    this.divContentSubPanel.style.display = "";
                } else {
                    this.divContentSubPanel.style.display = "none";
                }
                this.NavPanelBar.isAllVisibleItem();
            }
        }
    });
    this.InitilizeStyle = function () {
        this.Text = this._Text;
        this.TextColor = this._TextColor;
        this.Visible = this._Visible;
        if (SysCfg.App.Properties.Device == SysCfg.App.TDevice.Movile) {

            this.divContentSubPanel.style.marginTop = this.NavPanelBar.TabPag.NavTabControls.PaddingTopNavBarMobile + "px";
            this.divContentSubPanel.style.marginLeft = this.NavPanelBar.TabPag.NavTabControls.PaddingNavBarMobile + "px";
            this.divContentSubPanel.style.marginRight = "0px";

        }
        else {
            this.divContentSubPanel.classList.add("ColorSelect");
            this.divContentSubPanel.style.marginTop = this.NavPanelBar.TabPag.NavTabControls.PaddingTopNavBar + "px";
            this.divContentSubPanel.style.marginLeft = this.NavPanelBar.TabPag.NavTabControls.PaddingNavBar + "px";
            this.divContentSubPanel.style.marginRight = "0px";

        }

    }
    this.InitilizeStyleSecondForm = function () {
        this.InitilizeStyle();
    }
}
var TNavGroupBar = function () {
    this.NavPanelBar = null;
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.Style = 0;
    this.Type = "TNavGroupBar";
    this.Ul = document.createElement('ul');
    this.Ul.classList.add("list-group");
    this.Ul.classList.add("text-left");
    this.divContentSubPanel = document.createElement('div');
    this.divContentSubPanel.style.textAlign = "center";
    this.divContentSubPanel.appendChild(this.Ul);
    this.ListItemGroupBar = new Array();
    this.addItemGroupBar = function (ItemGroupBar) {
        this.ListItemGroupBar.push(ItemGroupBar);
        this.Ul.appendChild(ItemGroupBar.Li);

    }
    Object.defineProperty(this, 'Visible', {
        get: function () {
            if (this.divContentSubPanel.style.display == "") {
                return true;
            } else {
                return false;
            }
        },
        set: function (inValue) {
            if (inValue) {
                this.divContentSubPanel.style.display = "";
            } else {
                this.divContentSubPanel.style.display = "none";
            }
        }
    });
    this.InitilizeStyle = function () {
        if (SysCfg.App.Properties.Device == SysCfg.App.TDevice.Movile) {
            this.divContentSubPanel.style.marginTop = this.NavPanelBar.TabPag.NavTabControls.PaddingTopNavBarMobile + "px";
            this.divContentSubPanel.style.marginLeft = this.NavPanelBar.TabPag.NavTabControls.PaddingNavBarMobile + "px";
            this.divContentSubPanel.style.marginRight = "0px";
        }
        else {
            this.divContentSubPanel.style.marginTop = this.NavPanelBar.TabPag.NavTabControls.PaddingTopNavBar + "px";
            this.divContentSubPanel.style.marginLeft = this.NavPanelBar.TabPag.NavTabControls.PaddingNavBar + "px";
            this.divContentSubPanel.style.marginRight = "0px";
        }
        if (this.NavPanelBar != null && this.NavPanelBar.TabPag.NavTabControls.VerticalPosition) {
            this.Ul.style.marginBottom = "0px";
            for (var i = 0; i < this.ListItemGroupBar.length; i++) {
                this.ListItemGroupBar[i].SetInitializeVertical();
            }

        }

    }
    this.InitilizeStyleSecondForm = function () {
        this.InitilizeStyle();
        this.divContentSubPanel.style.marginTop = "0px";
        var c = this.Ul.children;
        for (var i = 0; i < c.length; i++) {
            c[i].style.marginTop = "0px";
        }
    }
}
var TItemGroupBar = function () {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.Li = document.createElement('li');

    this.Li.style.listStyle = "none";
    this.Link = document.createElement('a');
    this.Link.style.cursor = "pointer";
    this.Link.style.textDecoration = "none";
    this.Img = document.createElement('img');
    this.Img.style.width = "16px";
    this.Link.appendChild(this.Img);
    this.Li.appendChild(this.Link);
    this._Text = "";
    this._ToolTip = "";
    this._SrcImg = "";
    this._Cheked = false;
    this.Ul = null;
    this.ListItemGroupBar = new Array();
    this._ChekedButton = false;
    this.ClickDown = false;
    this._Disabled = false;
    this._Enabled = true;
    this._Father = null;
    this.IsVertical = false;

    this.SetInitializeVertical = function () {
        if (this._Father == null) {
            //$(this.Img).css("border", "1px solid #c4c4c4");
            this.Img.style.width = "18px";
            //this.Img.style.paddingTop = "2px";
            //this.Img.style.paddingBottom = "2px";
            //this.Img.style.paddingRight = "2px";
            //this.Img.style.paddingLeft = "2px";
            //this.Img.style.borderRadius = "1px";
            //this.Img.removeChild

            this.Img.parentNode.removeChild(this.Img);
            this.Link.parentNode.removeChild(this.Link);

            this.Li.appendChild(this.Img);
            this.Li.appendChild(this.Link);
            this.Li.style.paddingLeft = "15px";
            this.Li.style.marginTop = "5px";
            this.Link.style.paddingLeft = "5px";
            this.Link.style.marginTop = "10px";

        }
    }

    Object.defineProperty(this, 'Text', {
        get: function () {
            return this._Text;
        },
        set: function (inValue) {
            if (this.IsVertical) {
                this._Text = inValue;
                inValue = inValue + "&nbsp;&nbsp;&nbsp;";
                $(this.Link).html("");
                $(this.Link).html("&nbsp;" + inValue);

            } else {
                var textOld = this._Text;
                this._Text = inValue;
                inValue = inValue + "&nbsp;";
                if (this.ListItemGroupBar.length > 0) {
                    $(this.Link).html("");
                    $(this.Link).html("&nbsp;" + inValue);
                    this.Ul = null;
                    for (var i = 0; i < _this.ListItemGroupBar.length; i++) {
                        if (this.Ul == null) {
                            this.Ul = document.createElement('ul');
                            this.Ul.classList.add("dropdown-menu");

                            var span = document.createElement('span');
                            span.classList.add("caret");
                            //debugger;
                            //span.style.marginLeft = "5px";
                            this.Link.appendChild(span);
                            this.Li.appendChild(this.Ul);
                        }

                        this.Ul.appendChild(this.ListItemGroupBar[i].Li);
                    }



                }
                else {
                    if (inValue != "" && inValue != null) {
                        if (textOld == "") {
                            this.Link.removeChild(this.Img);
                            this.Li.removeChild(this.Link);
                            this.Li.appendChild(this.Img);
                            this.Li.appendChild(this.Link);
                        }

                    }
                    else {
                        if (textOld != "") {
                            $(this.Link).html("");
                            this.Li.removeChild(this.Img);
                            this.Li.removeChild(this.Link);
                            this.Link.appendChild(this.Img);
                            this.Li.appendChild(this.Link);
                        }
                    }

                    if (this._Text != "") {
                        $(this.Link).html("");
                        $(this.Link).html("&nbsp;" + inValue);
                    }

                }
            }



        }
    });

    Object.defineProperty(this, 'ToolTip', {
        get: function () {
            return this._ToolTip;
        },
        set: function (inValue) {
            this._ToolTip = inValue;
            if (this._SrcImg != "") {
                this.Img.title = inValue;
            }

        }
    });
    Object.defineProperty(this, 'SrcImg', {
        get: function () {
            return this._SrcImg;
        },
        set: function (inValue) {
            this._SrcImg = inValue;
            this.Img.src = inValue;

        }
    });
    Object.defineProperty(this, 'onclick', {
        get: function () {
            return this.Link.onclick;
        },
        set: function (inValue) {
            this.Link.onclick = function () {
                if (!_this._Disabled) {
                    if (_this.ChekedButton) {
                        _this.ClickDown = !_this.ClickDown;
                        _this.Cheked = _this.ClickDown;
                    } else {
                        if (_this._Father != null) {
                            _this._Father.Text = _this.Text;
                            if (SysCfg.App.Properties.Device == SysCfg.App.TDevice.Movile) {
                                _this._Father.Link.onclick = function () {
                                    _this._Father.Ul.style.position = "fixed";
                                    var p = $(_this._Father.Link);
                                    var offset = p.offset();
                                    _this._Father.Ul.style.top = offset.top + 1 + "px";
                                    _this._Father.Ul.style.left = offset.left + "px";

                                }
                            }

                        }
                    }

                    inValue(_this);
                }

            };
        }
    });
    Object.defineProperty(this, 'ChekedButton', {
        get: function () {
            return this._ChekedButton;
        },
        set: function (inValue) {
            this._ChekedButton = inValue;
        }
    });
    Object.defineProperty(this, 'Cheked', {
        get: function () {
            return this.ClickDown;
        },
        set: function (_Value) {
            this.ClickDown = _Value;
            if (this.ClickDown) {
                $(this.Li).css("border", "1px solid #c4c4c4");
            } else {
                $(this.Li).css("border", "0px solid #c4c4c4");
            }

        }
    });
    Object.defineProperty(this, 'ChekedClickDown', {
        get: function () {
            return this.Cheked;
        },
        set: function (_Value) {
            this.Cheked = _Value;


        }
    });
    Object.defineProperty(this, 'Opacity', {
        get: function () {
            return this.Img.style.opacity;
        },
        set: function (inValue) {
            this.Img.style.opacity = inValue;
        }
    });
    Object.defineProperty(this, 'Width', {
        get: function () {
            return this.Img.style.width;
        },
        set: function (inValue) {
            this.Img.style.width = inValue;

        }
    });
    Object.defineProperty(this, 'Visible', {
        get: function () {
            if (this.Li.style.display == "") {
                return true;
            } else {
                return false;
            }
        },
        set: function (inValue) {
            if (inValue) {
                this.Li.style.display = "";
            } else {
                this.Li.style.display = "none";
            }

        }
    });
    Object.defineProperty(this, 'TextColor', {
        get: function () {
            return this.Link.style.color;
        },
        set: function (inValue) {
            this.Link.style.color = inValue;
        }
    });
    Object.defineProperty(this, 'Disabled', {
        get: function () {
            return this._Disabled;
        },
        set: function (inValue) {
            this._Disabled = inValue;
            this._Enabled = !inValue;
            if (inValue) {
                this.Link.style.cursor = "not-allowed";
                this.Opacity = "0.65";
                if (typeof ($(this.Link).attr("data-toggle")) != "undefined") {
                    $(this.Link).remove("data-toggle");
                }
            } else {
                this.Link.style.cursor = "";
                this.Opacity = "";
                if (typeof ($(this.Link).attr("data-toggle")) != "undefined") {
                    $(this.Link).attr("data-toggle", "dropdown");
                }
            }
        }
    });
    Object.defineProperty(this, 'Enabled', {
        get: function () {
            return this._Enabled;
        },
        set: function (inValue) {
            this._Enabled = inValue;
            this._Disabled = !inValue;
            if (!inValue) {
                this.Link.style.cursor = "not-allowed";
                this.Opacity = "0.65";
                if (typeof ($(this.Link).attr("data-toggle")) != "undefined") {
                    $(this.Link).remove("data-toggle");
                }

            } else {
                this.Link.style.cursor = "";
                this.Opacity = "";
                if (typeof ($(this.Link).attr("data-toggle")) != "undefined") {
                    $(this.Link).attr("data-toggle", "dropdown");
                }
            }
        }
    });
    this.addItemGroupBar = function (ItemGroupBar) {
        if (this.Ul == null) {
            this.Ul = document.createElement('ul');
            this.Ul.classList.add("dropdown-menu");
            this.Li.classList.add("dropdown");
            if (!_this._Disabled) {
                $(this.Link).attr("data-toggle", "dropdown");
            }

            var span = document.createElement('span');
            span.classList.add("caret");
            this.Link.appendChild(span);
            this.Li.appendChild(this.Ul);

            if (SysCfg.App.Properties.Device == SysCfg.App.TDevice.Movile) {
                _this.Link.onclick = function () {
                    _this.Ul.style.position = "fixed";
                    var p = $(_this.Link);
                    var offset = p.offset();
                    _this.Ul.style.top = offset.top + 1 + "px";
                    _this.Ul.style.left = offset.left + "px";

                }
            }

        }

        ItemGroupBar._Father = _this;
        this.ListItemGroupBar.push(ItemGroupBar);
        ItemGroupBar.Li.removeChild(ItemGroupBar.Img);
        this.Ul.appendChild(ItemGroupBar.Li);
    }
    this.onclick = function () { };
}
var TNavMenu = function (inObjectHtml) {
    this.ObjectHtml = inObjectHtml;
    this.InicializeComponent = function () {
        this.GrillaNav = new TVclGridCF(this.ObjectHtml, "", "");
        this.GrillaNav.ResponsiveCont.classList.remove("table-responsive");
    }

    this.AddNavGroupMenu = function (inNavGroupMenu) {
        inNavGroupMenu.GrillaNav = this.GrillaNav;
        this.GrillaNav.AddRowAfter(0, inNavGroupMenu.NewRow, null, 1);
    }
    this.InicializeComponent();
}
var TNavGroupMenu = function (NavMenu) {

    this.NewRow = null;
    this.NavMenu = NavMenu;
    this._Text = "";
    this._SrcImg = "";
    this._TypeNavBar = "H";
    this._PaddindTextNavBar = 0;
    this.GrillaNav = null;
    this._Color = null;
    this._BackgroundColor = null;

    Object.defineProperty(this, 'SrcImg', {
        get: function () {
            return this._SrcImg;
        },
        set: function (inValue) {
            this._SrcImg = inValue;
            this.NewRow.IconLeftNavBar = inValue;
            this.NewRow.TextNavBar = this._Text;
        }
    });
    Object.defineProperty(this, 'Text', {
        get: function () {
            return this._Text;
        },
        set: function (inValue) {
            this._Text = inValue;
            this.NewRow._TypeNavBar = this._TypeNavBar;
            this.NewRow._PaddindTextNavBar = this._PaddindTextNavBar;

            this.NewRow.TextNavBar = inValue;
            this.Color = this._Color;
            this.BackgroundColor = this._BackgroundColor;
        }
    });
    Object.defineProperty(this, 'Color', {
        get: function () {
            return this._Color;
        },
        set: function (inValue) {
            this._Color = inValue;
            if (inValue != null && this.NewRow.Cells.length > 0) {
                this.NewRow.LabelTextGroup.This.style.color = inValue;
            }


        }
    });

    Object.defineProperty(this, 'BackgroundColor', {
        get: function () {
            return this._BackgroundColor;
        },
        set: function (inValue) {

            this._BackgroundColor = inValue;
            if (inValue != null && this.NewRow.Cells.length > 0) {
                this.NewRow.Cells[0].Color = inValue;
            }

        }
    });

    this.AddNavItemMenu = function (inNavItemMenu) {
        var RowReference = this.NewRow;
        var numeroHijos = this.NewRow.RowHijos.length;
        if (numeroHijos > 0) {
            RowReference = this.NewRow.RowHijos[numeroHijos - 1];
        }
        this.NewRow.RowHijos.push(inNavItemMenu.NewRow);
        inNavItemMenu.NewRow.RowPadre = this.NewRow;
        inNavItemMenu.NewRow._TypeNavBar = this._TypeNavBar;
        inNavItemMenu.NewRow.TextNavBar = inNavItemMenu.NewRow._TextNavBar;
        this.GrillaNav.AddRowAfter(1, inNavItemMenu.NewRow, RowReference, 1);

    }
    this.AddNavItemMenuPanel = function (inNavItemMenu) {
        var RowReference = this.NewRow;
        var numeroHijos = this.NewRow.RowHijos.length;
        if (numeroHijos > 0) {
            RowReference = this.NewRow.RowHijos[numeroHijos - 1];
        }
        this.NewRow.RowHijos.push(inNavItemMenu.NewRow);
        inNavItemMenu.NewRow.RowPadre = this.NewRow;
        this.GrillaNav.AddRowAfter(1, inNavItemMenu.NewRow, RowReference, 1);

    }
    this.InicializeComponent = function () {
        this.GrillaNav = NavMenu.GrillaNav;
        this.NewRow = new TVclRowCF();
        this.NewRow._TypeNavBar = "P";
        this.NewRow.TextNavBar = this.NewRow._TextNavBar;
        this.GrillaNav.AddRowAfter(0, this.NewRow, null, 1);
    }
    this.InicializeComponent();
}
var TNavItemMenu = function (PanelDiv) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this._Text = "";
    this._SrcImg = "";
    this.NewRow = null;
    Object.defineProperty(this, 'SrcImg', {
        get: function () {
            return this._SrcImg;
        },
        set: function (inValue) {
            this._SrcImg = inValue;
            this.NewRow.IconLeftNavBar = inValue;
            this.NewRow.TextNavBar = this._Text;
        }
    });
    Object.defineProperty(this, 'Text', {
        get: function () {
            return this._Text;
        },
        set: function (inValue) {
            this._Text = inValue;
            this.NewRow.TextNavBar = inValue;
        }
    });


    Object.defineProperty(this, 'onclick', {
        get: function () {
            return this.NewRow.onclick;
        },
        set: function (inValue) {
            this.NewRow.onclick = function () {
                inValue(_this);
            };
        }
    });
    this.InicializeComponent = function () {

        if (typeof (PanelDiv) == "undefined") {

            this.NewRow = new TVclRowCF();
            this.NewRow._TypeNavBar = "H";
            var Cell = new TVclCellCF();
            Cell._Value = "";
            this.NewRow.AddCell(Cell);
            Cell.This.style.border = "0px";
        } else {
            this.NewRow = new TVclRowCF();
            var Cell = new TVclCellCF();
            this.NewRow.AddCell(Cell);
            Cell.This.style.border = "0px";
            this.NewRow.PanelNavBar = PanelDiv;
        }

    }
    this.InicializeComponent();
}