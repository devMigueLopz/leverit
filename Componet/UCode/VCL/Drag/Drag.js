﻿var TVclDrag = function (ObjectHtml) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this._ObjectDrag = null;
    this.Componente = null;

    this.ListVclDragItem = new Array();
    this.allowDrop = function (ev) {
        ev.preventDefault();
    }

    this.drag = function (ObjectDrag, ev) {

        this.ObjectDrag = ObjectDrag;
        ev.dataTransfer.setData("text", ev.target.id);
    }

    this.drop = function (VclDragContent, ObjectDrag, ev) {
        ev.preventDefault();
        var data = ev.dataTransfer.getData("text");
       if (VclDragContent.onDrog != null) {
            VclDragContent.onDrog(VclDragContent.Componente, ObjectDrag, ev);
        } else {
            ev.target.appendChild(document.getElementById(data));
        }
    }
   
    this.addEventContendor = function (Componente, object, onDrog) {
        var VclDragContent = new TVclDragContent(_this);
        VclDragContent.setEventContendor(Componente, object,onDrog);

    }

    this.addDragable = function (Componente, object) {
        var VclDragItem = new TVclDragItem(_this);
        VclDragItem.setDragable(Componente, object);
    }
    Object.defineProperty(this, 'ObjectDrag', {
        get: function () {
            return this._ObjectDrag;
        },
        set: function (inValue) {
            this._ObjectDrag = inValue;
        }
    });
}
var TVclDragContent = function (TVclDrag) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.TVclDrag = TVclDrag;
    this.Componente = null;
    this.objectComponet = null;
    this.onDragObject = null;
    this.onDrog = null;
    this.setEventContendor = function (Componente, objectComponet, onDrog) {
        this.onDrog = onDrog;
        _this.Componente = Componente;
        _this.objectComponet = objectComponet;

        if (objectComponet.id == "" || objectComponet.id == null) {
            objectComponet.id = "" + new Date().getTime() + "" + parseInt(Math.random() * 1000000000000000);
        }
        objectComponet.ondrop = function (event) {
            var eventTemp = event;
            _this.TVclDrag.drop(_this,_this.TVclDrag.ObjectDrag, eventTemp);
        }
        objectComponet.ondragover = function (event) {
            _this.TVclDrag.allowDrop(event);
        }

    }

}

var TVclDragItem = function (TVclDrag) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.TVclDrag = TVclDrag;
    this.Componente = null;
    this.objectComponet = null;
    this.setDragable = function (Componente, objectComponet) {
        _this.Componente = Componente;
        _this.objectComponet = objectComponet;
        if (objectComponet.id == "" || objectComponet.id == null) {
            objectComponet.id = "" + new Date().getTime() + "" + parseInt(Math.random() * 1000000000000000);
        }
        objectComponet.draggable = true;
        objectComponet.ondragstart = function (event) {
            _this.TVclDrag.drag(_this.Componente, event);
        }
    }

}