﻿var TVclDrag = function (ObjectHtml) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this._ObjectDrag = null;
    this.Componente = null;
    this.onDrog = null;
    this.allowDrop = function (ev) {
        ev.preventDefault();
    }

    this.drag = function (ObjectDrag, ev) {

        this.ObjectDrag = ObjectDrag;
        ev.dataTransfer.setData("text", ev.target.id);
    }

    this.drop = function (ObjectDrag, ev) {
      //  _this.Component = ObjectDrag;
        ev.preventDefault();
        var data = ev.dataTransfer.getData("text");
        if (this.onDrog != null) {
            this.onDrog(ObjectDrag, ev);
        } else {
            ev.target.appendChild(document.getElementById(data));
        }
    }

    this.setEventContendor = function (Componente, object) {
        _this.Componente = Componente;

        if (object.id == "" || object.id == null) {
            object.id = "" + new Date().getTime() + "" + parseInt(Math.random() * 1000000000000000);
        }
        object.ondrop = function (event) {
            var w = event;
            var a = "s";
            _this.drop(_this.Componente, w);
        }
        object.ondragover = function (event) {
            _this.allowDrop(event);
        }

    }

    this.setDragable = function (Componente, object) {
        _this.Componente = Componente;
        if (object.id == "" || object.id == null) {
            object.id = "" + new Date().getTime() + "" + parseInt(Math.random() * 1000000000000000);
        }
        object.draggable = true;
        object.ondragstart = function (event) {
            _this.drag(_this.Componente, event);
        }
    }
    Object.defineProperty(this, 'ObjectDrag', {
        get: function () {
            return this._ObjectDrag;
        },
        set: function (inValue) {
            this._ObjectDrag = inValue;
        }
    });
}


