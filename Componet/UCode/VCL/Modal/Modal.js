﻿var TModal = function (inObjectHTML, ID, inCallBack) {
    this.This;
    this._Titulo = "";
    this.TituloHtml;
    this.This = Udiv(inObjectHTML, "");

    $(this.This).addClass("modal");
    $(this.This).addClass("fade");
    $(this.This).attr("role", "dialog");

    this.ModalDiv = Udiv(this.This, "");
    $(this.ModalDiv).addClass("modal-dialog");
    $(this.ModalDiv).addClass("modal-lg");

    this.ModalContent = Udiv(this.ModalDiv, "");
    $(this.ModalContent).addClass("modal-content");

    this.ModalHead = Udiv(this.ModalContent, "");
    $(this.ModalHead).addClass("modal-header");
    $(this.ModalHead).append('<button type="button" class="close" data-dismiss="modal">&times;</button>');

    this.TituloHtml = Uh4(this.ModalHead, ID, "");
    $(this.TituloHtml).addClass("modal-title");

    this.ModalBody = Udiv(this.ModalContent, "");
    $(this.ModalBody).addClass("modal-body");

    this.ModalFooter = Udiv(this.ModalContent, "");
    $(this.ModalFooter).addClass("modal-footer");
    $(this.ModalFooter).append(' <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');

    Object.defineProperty(this, 'Titulo', {
        get: function () {
            return this._Titulo;
        },
        set: function (inValue) {
            this._Titulo = inValue;
            $(this.TituloHtml).html(inValue);
        }
    });

    this.Show = function () {
        $(this.This).modal("show");
    }

    this.Hide = function () {
        $(this.This).modal("hide");
    }

}