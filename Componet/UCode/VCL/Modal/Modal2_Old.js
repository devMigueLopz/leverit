﻿SysCfg.Controls.TAlign = function () {
    this._None = null;
    this._Top = null;
    this._Bottom = null;
    this._Left = null;
    this._Right = null;
    this._Client = null;
    this._Custom = null;

    this.OnReAlign = null
    this.ReAlign = function () {
        if (this.OnReAlign != null)
            this.OnReAlign(this)
    }
       

    Object.defineProperty(this, 'None', {
        get: function () {
            return this._None;
        },
        set: function (inValue) {
            this._None = inValue;
            this.ReAlign();
        }
    });

    Object.defineProperty(this, 'Top', {
        get: function () {
            return this._Top;
        },
        set: function (inValue) {
            this._Top = inValue;
            this.ReAlign();
        }
    });

    Object.defineProperty(this, 'Bottom', {
        get: function () {
            return this._Bottom;
        },
        set: function (inValue) {
            this._Bottom = inValue;
            this.ReAlign();
        }
    });

    Object.defineProperty(this, 'Left', {
        get: function () {
            return this._Left;
        },
        set: function (inValue) {
            this._Left = inValue;
            this.ReAlign();
        }
    });

    Object.defineProperty(this, 'Right', {
        get: function () {
            return this._Right;
        },
        set: function (inValue) {
            this._Right = inValue;
            this.ReAlign();
        }
    });

    Object.defineProperty(this, 'Client', {
        get: function () {
            return this._Client;
        },
        set: function (inValue) {
            this._Client = inValue;
            this.ReAlign();
        }
    });

    Object.defineProperty(this, 'Custom', {
        get: function () {
            return this._Custom;
        },
        set: function (inValue) {
            this._Custom = inValue;
            this.ReAlign();
        }
    });
}

SysCfg.Controls.TSize = function () {
    this._Width = 0;
    this._Height = 0;

    this.OnReSize = null
    this.ReSize = function () {
        if (this.OnReSize != null)
            this.OnReSize(this)
    }

    Object.defineProperty(this, 'Width', {
        get: function () {
            return this._Width;
        },
        set: function (inValue) {
            this._Width = inValue;
            this.ReSize();
        }
    });

    Object.defineProperty(this, 'Height', {
        get: function () {
            return this._Height;
        },
        set: function (inValue) {
            this._Height = inValue;
            this.ReSize();
        }
    });
}

SysCfg.Controls.TPosition = function () {
    this._Top = 0;
    this._Left = 0;

    this.OnRePosition = null
    this.Reposition = function () {
        if(this.OnRePosition != null)
            this.OnRePosition(this)
    }

    Object.defineProperty(this, 'Top', {
        get: function () {
            return this._Top;
        },
        set: function (inValue) {
            this._Top = inValue;
            this.Reposition();
        }
    });

    Object.defineProperty(this, 'Left', {
        get: function () {
            return this._Left;
        },
        set: function (inValue) {
            this._Left = inValue;
            this.Reposition();
        }
    });
}

var TVclModal = function (inObjectHTML, inObjectHTMLBack, ID) {
    var _this = this;

    this._inObjectHTML = inObjectHTML;
    this._inObjectHTMLBack = inObjectHTMLBack;
    //estructura del modal
    var oDivMod = Udiv(this._inObjectHTML, "");

    this.This = new TVclDiv(oDivMod);
    
    this.Content = null;

    this._functionClose = function () {

    };
    this._functionAfterClose = function () {

    };

    var oDivCont = Udiv(this.This.This, "");
    this.Content = new TVclDiv(oDivCont);

    var oDivHead = Udiv(this.Content.This, "");
    this.Header = new TVclDiv(oDivCont);
    this.btnClose = Uspan(this.Header.This, "", "")
    this.btnClose.innerHTML = "&times;";

    this.HeaderContainer = Uh2(this.Header.This, "", "");
    this.HeaderContainer.style.fontSize = "20px"; 
    this.HeaderContainer.style.fontWeight = "bold"; 
    this.HeaderContainer.style.marginTop = "0px";

    var oDivBody = Udiv(this.Content.This, "");   
    this.Body = new TVclDiv(oDivBody);

    this.Body.This.style.padding = "8px"; 
    this.Body.This.style.border = "1px solid rgb(192, 192, 192)"; 
    this.Body.This.style.borderImage = "none"; 
    this.Body.This.style.color = "black"; 
    this.Body.This.style.backgroundColor = "rgb(236, 240, 245)";

    var oDivFooter = Udiv(this.Content.This, "");
    this.Footer = new TVclDiv(oDivFooter);
    this.FooterContainer = Uh3(this.Footer.This, "", "");

    this.FooterContainer.style.marginTop = "5px";

    this.Footer.This.style.padding = "5px";
    this.Footer.This.style.color = "white";

    this._Width = "95%";

    //aplicacion de styles
    this.This.This.style.zIndex = "1";
    this.This.This.style.display = "none";
    this.This.This.style.position = "fixed";
    //this.This.This.style.paddingTop = "100px";
    this.This.This.style.left = "0";
    this.This.This.style.top = "0";
    this.This.This.style.width = "100%";
    this.This.This.style.height = "100%";
    this.This.This.style.overflow = "auto";
    this.This.This.style.left = "0";
    this.This.This.style.backgroundColor = "rgba(0,0,0,0.4)";


    //this.Content.This.style.margin= "20px 10%";
    this.Content.This.style.borderRadius= "3px"; 
    this.Content.This.style.paddingTop = "20px"; 
    this.Content.This.style.paddingRight = "20px"; 
    this.Content.This.style.paddingLeft = "20px"; 
    this.Content.This.style.borderTop ="5px solid rgb(0, 166, 90)";
    this.Content.This.style.boxShadow = "7px 9px 20px -2px #595c5a"; 
    this.Content.This.style.backgroundColor = "white";

    this.Content.This.style.zIndex = "9999999999";
    this.Content.This.style.position = "relative";
    this.Content.This.style.margin = "auto";
    //this.Content.This.style.marginRight = "30px";
    //this.Content.This.style.padding = "0";
    //this.Content.This.style.border = "1px solid #888";
    this.Content.This.style.width = "100%";
    this.Content.This.style.top = "4%";
    //this.Content.This.style.boxShadow = "0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19)";
    this.Content.This.style.WebkitAnimationName = "animatetop";
    this.Content.This.style.WebkitAnimationDuration = "0.8s";
    this.Content.This.style.animationName = "animatetop";
    this.Content.This.style.animationDuration = "0.4s";

    this.btnClose.style.color = "black";
    this.btnClose.style.float = "right";
    this.btnClose.style.fontSize = "28px";
    this.btnClose.style.fontWeight = "bold";
    this.btnClose.style.textDecoration = "none";
    this.btnClose.style.marginTop = "-15px";
    this.btnClose.style.marginRight = "-5px";
    this.btnClose.style.float = "right";
    this.btnClose.style.cursor = "pointer";


    this.btnClose.onmouseover = function () {
        this.style.color = "black";
        this.style.textDecoration = "none";
        this.style.cursor = "pointer";
    };
    this.btnClose.onfocus = function () {
        this.style.color = "black";
        this.style.textDecoration = "none";
        this.style.cursor = "pointer";
    };
    this.btnClose.onmouseout = function () {
        this.style.color = "black";
    };
    this.btnClose.onfocusout = function () {
        this.style.color = "black";
    };
    
    this.btnClose.onclick = function () {
        _this._functionClose();
        _this.CloseModal();
        _this._functionAfterClose();
    };


    //this.Header.This.style.padding = "2px 16px";
    //this.Header.This.style.backgroundColor = "#3e94ba";
    //this.Header.This.style.color = "white";

    //this.Body.This.style.padding = "2px 16px";
    //this.Body.This.style.color = "black";
    //this.Body.This.style.backgroundColor = "rgb(236,240,245)";

    //this.Footer.This.style.padding = "2px 16px";
    //this.Footer.This.style.backgroundColor = "#3e94ba";
    //this.Footer.This.style.color = "white";

    this._Align = new SysCfg.Controls.TAlign();
    this._Size = new SysCfg.Controls.TSize();
    this._Position = new SysCfg.Controls.TPosition();

    this.OnReAlign = null;
    this.OnReSize = null;
    this.OnRePosition = null;

    this._Align.OnReAlign = function (Args) {
        //aki codigo para cambia los estilos de acuerdo a las propiedades
        if (_this.OnReAlign != null)
            _this.OnReAlign(_this, Args);
    }
    
    this._Size.OnReSize = function (Args) {
        //aki codigo para cambia los estilos de acuerdo a las propiedades
        if (_this.OnReSize != null)
            _this.OnReSize(_this, Args);
    }

    this._Position.OnRePosition = function (Args) {
        //aki codigo para cambia los estilos de acuerdo a las propiedades
        if (_this.OnRePosition != null)
            _this.OnRePosition(_this, Args);
    }

    Object.defineProperty(this, 'Align', {
        get: function () {
            return this._Align;
        },
        set: function (inValue) {
            this._Align = inValue;
        }
    });

    Object.defineProperty(this, 'Size', {
        get: function () {
            return this._Size;
        },
        set: function (inValue) {
            this._Size = inValue;
        }
    });

    Object.defineProperty(this, 'Position', {
        get: function () {
            return this._Position;
        },
        set: function (inValue) {
            this._Position = inValue;
        }
    });

    //Agregado por Jaimes 17/02/2018
    Object.defineProperty(this, 'FunctionClose', {
        get: function () {
            return this._functionClose;
        },
        set: function (_Value) {
            this._functionClose = _Value;
        }
    });

    //Agregado por MIGUEL 16/04/2018
    Object.defineProperty(this, 'FunctionAfterClose', {
        get: function () {
            return this._functionAfterClose;
        },
        set: function (_Value) {
            this._functionAfterClose = _Value;
        }
    });

    Object.defineProperty(this, 'Width', {
        get: function () {
            return this._Width;
        },
        set: function (inValue) {
            this._Width = inValue;
        }
    });

    this.ShowModal = function () {
        _this.This.This.style.display = "block";
        _this.Content.This.style.display = "block";       
        
        if (_this._inObjectHTMLBack != null && _this._inObjectHTMLBack != undefined) {
            $(_this._inObjectHTMLBack).append(_this.Content.This);
            $('html, body').animate({ scrollTop: $(_this.Content.This).offset().top }, 'slow');
        }
        else { _this.Content.This.style.width = _this.Width; }
        //_this.Content.This.style.zIndex = "1";        
    }

    this.CloseModal = function () {
        _this.This.This.style.display = "none";
        _this.Content.This.style.display = "none";
        if (_this._inObjectHTMLBack != null && _this._inObjectHTMLBack != undefined) {
            _this.Content.This.parentNode.removeChild(_this.Content.This);
        }
    }

    this.AddHeaderContent = function (inContentHTML) {
        $(this.HeaderContainer).append(inContentHTML);
    }

    this.AddContent = function (inContentHTML) {
        $(this.Body.This).append(inContentHTML);
    }

    this.AddFooterContent = function (inContentHTML) {
        $(this.FooterContainer).append(inContentHTML);
    }
}



