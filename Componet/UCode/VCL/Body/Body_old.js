﻿var TVclBody = function (ObjectHtml, Id) {
    var idx = "_"+Id;
    this.header;
    this.main;
    this.footer;

    this.header = Uheader(ObjectHtml, "header" + idx);

    var arrayMain = new Array(1);
    var oArrayMain = VclDivitions(ObjectHtml, "Cont" + idx, arrayMain);
    BootStrapCreateRow(oArrayMain[0].This);
    this.ContenedorGeneral = oArrayMain[0].This;
    ObjectHtmlMain = oArrayMain[0].This;      
    this.asideleft = Uaside(ObjectHtmlMain, "asideleft" + idx);

    var oArrayMainRight = VclDivitions(ObjectHtmlMain, "Cont2" + idx, new Array(1));
    ObjectHtmlMainRight = oArrayMainRight[0].This;

    var oArrayMainMachetaso = VclDivitions(ObjectHtmlMainRight, "Cont2m" + idx, new Array(1));
    ObjectHtmlMainMachetaso = oArrayMainMachetaso[0].This;

    this.main = Umain(ObjectHtmlMainMachetaso, "main" + idx);
    this.asideright = Uaside(ObjectHtmlMainMachetaso, "asideright" + idx);
    BootStrapCreateColumn(this.asideleft, [3, 3, 3, 3, 3]);
    BootStrapCreateColumn(ObjectHtmlMainRight, [9, 9, 9, 9, 9]);

    var arrayFooter = new Array(1);
    var oArrayFooter = VclDivitions(ObjectHtml, "Foot" + idx, arrayMain);
    BootStrapCreateRow(oArrayFooter[0].This);
    ObjectHtmlFooter = oArrayFooter[0].This
    this.footer = Ufooter(ObjectHtmlFooter, "footer" + idx);

    var TParent = function () {
        return this;
    }.bind(this);

}

/*<summary>
Midifico fecha   
Carlos     24-04-2017
Funcion: Representa una clase que contienes 3 elementos, header, main y footer
ejemplos:
var TBody = new TBody();
</summary>
<returns>instancia de la clase TBody</returns>*/
function TBody() {
    this.header;
    this.main;
    this.footer;


}

/*<summary>
Midifico fecha   
Carlos     24-04-2017
Funcion: Crea un elemento html de la clase TBody
ejemplos:
UCrateBody(Elementhtm, "Id del elemento");
</summary>
<param name="Object">Elemento html al cual se le insertara el elemento creado por esta funcion</param>
<param name="id">Id de elemento creado</param>
<returns>Retorna un elemento TBody html</returns>*/
function VclCreateBody(Object, id) {
    var Body = new TBody();
    Body.header = Uheader(Object, id + "header");
    Body.main = Umain(Object, id + "main");
    Body.footer = Ufooter(Object, id + "footer");
    return Body;
}