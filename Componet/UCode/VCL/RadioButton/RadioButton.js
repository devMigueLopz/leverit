﻿var TVclInputRadioButton = function (ObjectHtml, Id) {
    this.This = Udiv(ObjectHtml, Id);
    this.Options = new Array();
    this._Tag = null;
    this._selectedIndex = -1;
    this._Name;  

    Object.defineProperty(this, 'Tag', {
        get: function () {
            return this._Tag;
        },
        set: function (_Value) {
            this._Tag = _Value;
        }
    });

    Object.defineProperty(this, 'selectedIndex', {
        get: function ()
        {
            for (var i = 0; i < this.Options.length; i++)
            {
                if (this.Options[i].This.checked == true)
                {
                    this._selectedIndex = i;
                    break;
                }
            }
            return this._selectedIndex;
        },
        set: function (_Value)
        {
            if (_Value >= 0)
            {
                if (this.Options.length > 0)
                {
                    this.Options[_Value].This.checked = true;
                    this._selectedIndex = _Value;
                }
            }
        }
    });

    //Object.defineProperty(this, 'Text', {
    //    get: function () {
    //        return this.This.options[this.This.selectedIndex].innerHTML
    //    },
    //    set: function (_Value) {
    //        this.This.options[this.This.selectedIndex].innerHTML = _Value;
    //    }
    //});

    Object.defineProperty(this, 'Name', {
        get: function () {
            return this._Name;
        },
        set: function (_Value) {
            for (var i = 0; i < this.Options.length; i++) {
                this.Options[i].This.name = _Value;
            }
            this._Name = _Value;
        }
    });

    Object.defineProperty(this, 'Value', {
        get: function () {
            for (var i = 0; i < this.Options.length; i++) {
                if (this.Options[i].This.checked) {
                    return this.Options[i].This.value;
                    break;
                }
            }
        },
        set: function (_Value) {
            for (var i = 0; i < this.Options.length; i++) {
                if (this.Options[i].This.value == _Value)
                    this.Options[i].This.checked = true;
                else
                    this.Options[i].This.checked = false;
            }
        }
    });

    Object.defineProperty(this, 'Enabled', {
        get: function () {
            return !this.This.disabled;
        },
        set: function (_Value) {
            this.This.disabled = !_Value;
        }
    });

    Object.defineProperty(this, 'Width', {
        get: function () {
            return this.This.style.width;
        },
        set: function (_Value) {
            this.This.style.width = _Value;
        }
    });

    Object.defineProperty(this, 'Visible', {
        get: function () {
            if (this.This.style.visibility == "hidden" || this.This.style.visibility == "collapse")
                return false;
            return true;
        },
        set: function (_Value) {
            if (_Value)
                this.This.style.visibility = "visible";
            else
                this.This.style.visibility = "hidden";
        }
    });

    this.Click = function () {
        this.This.click();
    }

    var TParent = function () {
        return this;
    }.bind(this);

    //metodos 
    this.AddItem = function (TVclInputRadioButtonItem) {
        $(this.This).append(TVclInputRadioButtonItem.This);        
        $(this.This).append(TVclInputRadioButtonItem.lbl);
        TVclInputRadioButtonItem.This.name = this._Name;
        TVclInputRadioButtonItem.lbl.style.marginRight = "10px";
        this.Options.push(TVclInputRadioButtonItem);
    }

    this.ClearOptions = function()
    {
        this.Options = new Array();
        this.This.innerHTML = "";
        this.selectedIndex = -1;
    }
}


var TVclInputRadioButtonItem = function () {
    this.This = Uinput("", TImputtype.radio, "", "", false);
    this.lbl = Ulabel("", "", "");
    this._Tag = null;

    this.lbl.style.marginLeft = "4px";
    //for (var i =Uoption 0; i < Data.length; i++) {
    //    this.Options[i] = Uoption(this.This, "option" + Id + i, Data[i].Value, Data[i].Text);
    //    if (IndexSelect >= 0)
    //        if (i == IndexSelect)
    //            this.This.value = Data[i].Value;
    //}
    Object.defineProperty(this, 'Id', {
        get: function () {
            return this.This.id;
        },
        set: function (_Value) {
            this.This.id = _Value;
            $(this.lbl).attr("for", _Value);
        }
    });

    Object.defineProperty(this, 'Tag', {
        get: function () {
            return this._Tag;
        },
        set: function (_Value) {
            this._Tag = _Value;
        }
    });

    Object.defineProperty(this, 'Value', {
        get: function () {
            return this.This.value;
        },
        set: function (_Value) {
            this.This.value = _Value;
        }
    });

    Object.defineProperty(this, 'Text', {
        get: function () {
            return this.lbl.innerHTML;
        },
        set: function (_Value) {
            this.lbl.innerHTML = _Value;
        }
    });

    Object.defineProperty(this, 'OnChange', {
        get: function () {
            return this.This.onchange();
        },
        set: function (_Value) {
            this.This.onchange = _Value;
        }
    });

    var TParent = function () {
        return this;
    }.bind(this);

    //metodos 
}