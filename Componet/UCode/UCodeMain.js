﻿/*<summary>
Midifico fecha   
Carlos     03-05-2017
Funcion: Representa un tipo de libreria q vamos a utilizar
ejemplos:
TVCLType.BS;
</summary>
<returns></returns>*/
var TVCLType = {
    AG: { value: 0, name: "Angular" },
    BS: { value: 1, name: "BootStrap" },
    DV: { value: 2, name: "DeveloperExpress" },
    KD: { value: 3, name: "Kendo" }
};

/*<summary>
Midifico fecha   
Carlos     24-04-2017
Funcion: variable que contiene los tipos de secciones html. 
ejemplos: 
TTagType.article;
</summary>
*/
var TTagType = {
    article: { value: 0, name: "article" },
    aside: { value: 0, name: "aside" },
    details: { value: 0, name: "details" },
    figcaption: { value: 0, name: "figcaption" },
    figure: { value: 0, name: "figure" },
    footer: { value: 0, name: "footer" },
    header: { value: 0, name: "header" },
    main: { value: 0, name: "main" },
    mark: { value: 0, name: "mark" },
    nav: { value: 0, name: "nav" },
    section: { value: 0, name: "section" },
    summary: { value: 0, name: "summary" },
    time: { value: 0, name: "time" }
};



