Componet.TPalleteColor = function (ObjectHtml, Id) {
	this.TParent = function () {
		return this;
	}.bind(this);
	this.ObjectHtml = ObjectHtml;
	this.EnumPalleteColor = {
		Hexadecimal: { value: 0, name: "hex" },
		Hexadecimal3: { value: 1, name: "hex3" },
		Hsl: { value: 2, name: "hsl" },
		Rgb: { value: 3, name: "rgb" },
		Name: { value: 4, name: "name" }
	};
	this.ID = Id;
	this.This = null;
	this._Color = "";
	this._ChooseText = "Save";
	this._CancelText = "Cancel";
	this._TogglePaletteMoreText = "More";
	this._TogglePaletteLessText = "Hide";
	this._ContainerClassName = "";
	this._ReplacerClassName = "";
	this._localStorageKey = "";
	this._ShowSelectionPalette = false;
	this._SelectionPalette = new Array();
	this._PreferredFormat = this.EnumPalleteColor.Rgb.name;
	this._Flat = false;
	this._ShowInput =true;
	this._ShowInitial = true;
	this._AllowEmpty = true;
	this._ShowAlpha = true;
	this._Disabled = false;
	this._ShowPalette = true;
	this._TogglePaletteOnly = false;
	this._ShowPaletteOnly = false;
	this._HideAfterPaletteSelect = false;
	this._ShowButtons = true;
	this._MaxSelectionSize = 0;
	this._ClickOutFiresChange = true;
	this._Palette =[['black', 'white', 'red'], ['green', 'pink', 'yellow'], ['orange', 'brown', 'silver'], ['blue', 'purple']];
	this._Change = null;
	this._Move = null;
	Object.defineProperty(this, 'Color', {
		/*El color que marcara la paleta de colores por defecto*/
		get: function () {
			return this._Color;
		},
		set: function (Value) {
			this._Color = Value;
			if (this.This !== null) { this.CreatePallete() };
		}
	});
	Object.defineProperty(this, 'OkButtonText', {
		/*Nombre del boton aceptar*/
		get: function () {
			return this._ChooseText;
		},
		set: function (Value) {
			this._ChooseText = Value;
			if (this.This !== null) { this.CreatePallete() };
		}
	});
	Object.defineProperty(this, 'LocalColor', {
		/*Agrega un Nombre para que  guarde los colores elegidos en la paleta de favoritos, esto se mostrara as� recarge la pagina.Est�s caracteristicas debe de tener para funcionar showPalette:true, showSelectionPalette : true , palette lleno */
		get: function () {
			return this._localStorageKey;
		},
		set: function (Value) {
			this._localStorageKey = Value;
			if (this.This !== null) { this.CreatePallete() };
		}
	});
	Object.defineProperty(this, 'CancelButtonText', {
		/*Nombre del boton cancelar*/
		get: function () {
			return this._CancelText;
		},
		set: function (Value) {
			this._CancelText = Value;
			if (this.This !== null) { this.CreatePallete() };
		}
	});
	Object.defineProperty(this, 'MoreButtonText', {
		/*Para la Paleta only el nombre del boton m�s*/
		get: function () {
			return this._TogglePaletteMoreText;
		},
		set: function (Value) {
			this._TogglePaletteMoreText = Value;
			if (this.This !== null) { this.CreatePallete() };
		}
	});
	Object.defineProperty(this, 'HideButtonText', {
		/*Para la Paleta only el nombre del boton ocultar*/
		get: function () {
			return this._TogglePaletteLessText;
		},
		set: function (Value) {
			this._TogglePaletteLessText = Value;
			if (this.This !== null) { this.CreatePallete() };
		}
	});
	Object.defineProperty(this, 'MinPaletteClassStyle', {
		/*Agrega  clase para darle estilos al contenido de la paleta*/
		get: function () {
			return this._ContainerClassName;
		},
		set: function (Value) {
			this._ContainerClassName = Value;
			if (this.This !== null) { this.CreatePallete() };
		}
	});
	Object.defineProperty(this, 'FullPaletteClassStyle', {
		/*Agrega clase para darle estilos al contendio de la mini paleta*/
		get: function () {
			return this._ReplacerClassName;
		},
		set: function (Value) {
			this._ReplacerClassName = Value;
			if (this.This !== null) { this.CreatePallete() };;
		}
	});
	Object.defineProperty(this, 'FavoritePalette', {
		/*Colores por defecta que tendra la paleta de favoritos. Condiciones showPalette : true , palette : [ ], showSelectionPalette : true*/
		get: function () {
			return this._SelectionPalette;
		},
		set: function (Value) {
			this._SelectionPalette = Value;
			if (this.This !== null) { this.CreatePallete() };
		}
	});
	Object.defineProperty(this, 'FormatColor', {
		/*Formato de como guardara los colores*/
		get: function () {
			return this._PreferredFormat;
		},
		set: function (Value) {
			this._PreferredFormat = Value;
			if (this.This !== null) { this.CreatePallete() };
		}
	});
	Object.defineProperty(this, 'FloatingPallete', {
		/*True:Permite mostrar la paleta directamente. False: No muestra la paleta directamente si no un cuadro peque�o de colores.*/
		get: function () {
			return this._Flat;
		},
		set: function (Value) {
			this._Flat = Value;
			if (this.This !== null) { this.CreatePallete() };
		}
	});
	Object.defineProperty(this, 'RememberSelectionPallete', {
		/*True: Agrega colores a la paleta de colores favoritos, recordara siempre el ultimo color que haya elegido. False: No muestra esa caracteristica.*/
		get: function () {
			return this._ShowSelectionPalette;
		},
		set: function (Value) {
			this._ShowSelectionPalette = Value;
			if (this.This !== null) { this.CreatePallete() };
		}
	});
	Object.defineProperty(this, 'SearchColor', {
		/*True: Muestra una caja de texto en la paleta, para que el usuario puede digitar directamente el color en caso sea necesario. False: No muestra.*/
		get: function () {
			return this._ShowInput;
		},
		set: function (Value) {
			this._ShowInput = Value;
			if (this.This !== null) { this.CreatePallete() };
		}
	});
	Object.defineProperty(this, 'ColorInitial', {
		/*True: Muestra el color inicial con el que abrio la paleta, mientras por otro lado muestra el nuevo color a elegir. False: No lo Muestra*/
		get: function () {
			return this._ShowInitial;
		},
		set: function (Value) {
			this._ShowInitial = Value;
			if (this.This !== null) { this.CreatePallete() };
		}
	});
	Object.defineProperty(this, 'DeleteColor', {
		/*True: Muestra un icono en la parte superior izquierda la cual retorna el color al valor de inicio. False: No lo muestra.*/
		get: function () {
			return this._AllowEmpty;
		},
		set: function (Value) {
			this._AllowEmpty = Value;
			if (this.This !== null) { this.CreatePallete() };
		}
	});
	Object.defineProperty(this, 'Transparency', {
		/*True: Muestra la barra de transparencia del color. False: No lo Muestra*/
		get: function () {
			return this._ShowAlpha;
		},
		set: function (Value) {
			this._ShowAlpha = Value;
			if (this.This !== null) { this.CreatePallete() };
		}
	});
	Object.defineProperty(this, 'Disabled', {
		/*True: Habilita que la paleta de colores no se muestre. False: Muestra la paleta de colores*/
		get: function () {
			return this._Disabled;
		},
		set: function (Value) {
			this._Disabled = Value;
			if (this.This !== null) { this.CreatePallete() };
		}
	});
	Object.defineProperty(this, 'OptionsPallete', {
		/*True: Habilita colores favoritos en la paleta de colores. False: No lo muestra.*/
		get: function () {
			return this._ShowPalette;
		},
		set: function (Value) {
			this._ShowPalette = Value;
			if (this.This !== null) { this.CreatePallete() };
		}
	});
	Object.defineProperty(this, 'OptionsPalleteOnly', {
		/*True:Muestra la Paleta de Colores en linea siempre y cuando haya datos en el array Palette y el showPalette este en True. False: No lo muesta. */
		get: function () {
			return this._ShowPaletteOnly;
		},
		set: function (Value) {
			this._ShowPaletteOnly = Value;
			if (this.This !== null) { this.CreatePallete() };
		}
	});
	Object.defineProperty(this, 'ShowButtons', {
		/*True: Muestra los botones de cancelar y aceptar. No muestra los botones de Aceptar y Cancelar*/
		get: function () {
			return this._ShowButtons;
		},
		set: function (Value) {
			this._ShowButtons = Value;
			if (this.This !== null) { this.CreatePallete() };
		}
	});
	Object.defineProperty(this, 'HideAfterPaletteColorSelect', {
		/*True:Se oculta autom�ticamente despu�s de seleccionar un color de paleta. False: No se Oculta*/
		get: function () {
			return this._HideAfterPaletteSelect;
		},
		set: function (Value) {
			this._HideAfterPaletteSelect = Value;
			if (this.This !== null) { this.CreatePallete() };
		}
	});
	Object.defineProperty(this, 'FavoritePaletteSize', {
		/*Maximo de colores permitidos en la paleta de Colores favoritas*/
		get: function () {
			return this._MaxSelectionSize;
		},
		set: function (Value) {
			this._MaxSelectionSize = Value;
			if (this.This !== null) { this.CreatePallete() };
		}
	});
	Object.defineProperty(this, 'ClickOutChangeColorPallete', {
		/*Permite que al cambiar de color y hacer click en otra parte de la pantalla cambie su valor. False: No cambia hasta que haga click en la confirmaci�n*/
		get: function () {
			
			return this._ClickOutFiresChange;
		},
		set: function (Value) {
			this._ClickOutFiresChange = Value;
			if (this.This !== null) { this.CreatePallete() };
		}
	});
	Object.defineProperty(this, 'DivitionsPallete', {
		/*True: Habilia la opci�n de que muestra la paleta de colores favoritas(array palette con data) en linea y en caso queira personalizar haya 2 botones TogglePaletteMoreText y TogglePaletteLessText. False: No muestra esa caracteristica*/
		get: function () {
			return this._TogglePaletteOnly;
		},
		set: function (Value) {
			this._TogglePaletteOnly = Value;
			if (this.This !== null) { this.CreatePallete() };
		}
	});
	Object.defineProperty(this, 'Pallete', {
		/*Array que contendra los colores, esto siempre y cuando este activado el Show Palette*/
		get: function () {
			return this._Palette;
		},
		set: function (Value) {
			this._Palette = Value;
			if (this.This !== null) { this.CreatePallete() };
		}
	});
	Object.defineProperty(this, 'ChangeColor', {
		get: function () {
			return this._Change;
		},
		set: function (Value) {
			this._Change = Value;
		}
	});
	Object.defineProperty(this, 'MoveColor', {
		get: function () {
			return this._Move;
		},
		set: function (Value) {
			this._Move = Value;
		}
	});
}
Componet.TPalleteColor.prototype.CreatePallete = function () {
    var _this = this.TParent();
    try {
        $(_this.ObjectHtml).html("");
        _this.This = Uinput(_this.ObjectHtml, TImputtype.text, _this.ID, "", false)
        _this.This = $(_this.This).spectrum({
            color: _this._Color,
            flat: _this._Flat,
            showInput: _this._ShowInput,
            showInitial: _this._ShowInitial,
            allowEmpty: _this._AllowEmpty,
            showAlpha: _this._ShowAlpha,
            disabled: _this._Disabled,
            localStorageKey: _this._localStorageKey,
            showPalette: _this._ShowPalette,
            showPaletteOnly: _this._ShowPaletteOnly,
            togglePaletteOnly: _this._TogglePaletteOnly,
            showSelectionPalette: _this._ShowSelectionPalette,
            clickoutFiresChange: _this._ClickOutFiresChange,
            cancelText: _this._CancelText,
            chooseText: _this._ChooseText,
            togglePaletteMoreText: _this._TogglePaletteMoreText,
            togglePaletteLessText: _this._TogglePaletteLessText,
            containerClassName: _this._ContainerClassName,
            replacerClassName: _this._ReplacerClassName,
            preferredFormat: _this._PreferredFormat,
            maxSelectionSize: _this._MaxSelectionSize,
            palette: _this._Palette,
            selectionPalette: _this._SelectionPalette,
            hideAfterPaletteSelect: _this._HideAfterPaletteSelect,
            showButtons: _this._ShowButtons,
            change: function (colorChange) {

                switch (_this._PreferredFormat) {
                    case _this.EnumPalleteColor.Hexadecimal.name:
                        _this._Color = (colorChange !== null) ? colorChange.toHexString() : "";
                        break;
                    case _this.EnumPalleteColor.Hexadecimal3.name:
                        _this._Color = (colorChange !== null) ? colorChange.toHexString() : "";
                        break;
                    case _this.EnumPalleteColor.Hsl.name:
                        _this._Color = (colorChange !== null) ? colorChange.toHslString() : "";
                        break;
                    case _this.EnumPalleteColor.Rgb.name:
                        _this._Color = (colorChange !== null) ? colorChange.toRgbString() : "";
                        break;
                    case _this.EnumPalleteColor.Name.name:
                        _this._Color = (colorChange !== null) ? colorChange.toName() : "";
                        break;
                }
                if (_this._Change !== null) {
                    _this.ChangeColor(_this);
                }
            },
            move: function (colorMove) {
                var _moveColor = "";
                switch (_this._PreferredFormat) {
                    case _this.EnumPalleteColor.Hexadecimal.name:
                        _moveColor = (colorMove !== null) ? colorMove.toHexString() : "";
                        break;
                    case _this.EnumPalleteColor.Hexadecimal3.name:
                        moveColorr = (colorMove !== null) ? colorMove.toHexString() : "";
                        break;
                    case _this.EnumPalleteColor.Hsl.name:
                        _moveColor = (colorMove !== null) ? colorMove.toHslString() : "";
                        break;
                    case _this.EnumPalleteColor.Rgb.name:
                        _moveColor = (colorMove !== null) ? colorMove.toRgbString() : "";
                        break;
                    case _this.EnumPalleteColor.Name.name:
                        _moveColor = (colorMove !== null) ? colorMove.toName() : "";
                        break;
                }
                if (_this._Move !== null) {
                    _this.MoveColor(_this, _moveColor);
                }
            }
        });

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Color.js Componet.TPalleteColor.prototype.CreatePallete", e);
    }
}

