﻿ 
ItHelpCenter.SD.AdvSearch.TAdvSearchManager = function (inObject, incallback, inParent) {
    this.TParent = function () {
        return this;
    }.bind(this);
    

    var _this = this.TParent();
    this.ObjectHtml = inObject;   

    this.ParentThis = inParent;
    this.CallbackModalResult = incallback;
    this.DBTranslate = null;
    this.Mythis = "TAdvSearchManager";
    this.ID = this.Mythis + (Math.floor(Math.random() * 1000));   
 
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lblMostrar", "Show:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lblMostrar_X_Columna", "Select column to search:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lblBuscador", "Show:");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1", "View all");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2", "Search");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3", "(ALL)");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines4", "Before leaving, you must select an item in the grid");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnAccept", "Accept");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnCancel", "Cancel");
    _this.InitializeComponent();
}

ItHelpCenter.SD.AdvSearch.TAdvSearchManager.prototype.InitializeComponent = function () {
    var _this = this.TParent();

    var DivCont = new TVclStackPanel(_this.ObjectHtml, "DivCont" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
    var DivC = DivCont.Column[0].This;

    var Div1 = new TVclStackPanel(DivC, "Div1" + _this.ID, 2, [[12, 12], [5, 7], [4, 8], [4, 8], [4, 8]]);
    var Div1C1 = Div1.Column[0].This;
    var Div1C2 = Div1.Column[1].This;

    var Div2 = new TVclStackPanel(DivC, "Div2" + _this.ID, 2, [[12, 12], [5, 7], [4, 8], [4, 8], [4, 8]]);
    var Div2C1 = Div2.Column[0].This;
    var Div2C2 = Div2.Column[1].This;
    _this.DivMostrar_X_Columna = Div2.Row.This;

    var Div3 = new TVclStackPanel(DivC, "Div3" + _this.ID, 2, [[12, 12], [5, 7], [4, 8], [4, 8], [4, 8]]);
    var Div3C1 = Div3.Column[0].This;
    var Div3C2 = Div3.Column[1].This;
    _this.DivtxtBuscador = Div3.Row.This;

    var Div4 = new TVclStackPanel(DivC, "Div4" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
    var Div4C = Div4.Column[0].This;
    $(Div4C).css("height", "300px"); 
    $(Div4C).css("overflow-y", "scroll");
    
    _this.DivGrid = Div4C;
    $(_this.DivGrid).css("border", "1px solid #c4c4c4");
    

    $(Div4.Row.This).css("padding", "10px");
    if (SysCfg.App.Properties.Device.value != SysCfg.App.TDevice.Desktop.value) {
        $(Div4.Row.This).css("padding", "0px");
        $(_this.DivGrid).css("padding", "0px");
    }

    var Div5 = new TVclStackPanel(DivC, "Div5" + _this.ID, 1, [[12], [12], [12], [12], [12]]);
    var Div5C = Div5.Column[0].This;

    //ELEMETS
    _this.lblMostrar = new Vcllabel(Div1C1, "lblMostrar" + _this.ID, TVCLType.BS, TlabelType.H0, "");
    _this.lblMostrar.innerText = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblMostrar");

    _this.cbMostrarTodo = new TVclComboBox2(Div1C2, "cbMostrarTodo" + _this.ID, "", 0);
    $(_this.cbMostrarTodo.This).css('width', "100%");
    $(_this.cbMostrarTodo.This).addClass("TextFormat");
    _this.cbMostrarTodo.onChange = function () {
        _this.cbMostrarTodo_SelectedIndexChanged(_this, _this.cbMostrarTodo);
    }
    var cbMostrarTodoItem1 = new TVclComboBoxItem();
    cbMostrarTodoItem1.Value = 1;
    cbMostrarTodoItem1.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1");
    cbMostrarTodoItem1.Tag = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1");
    _this.cbMostrarTodo.AddItem(cbMostrarTodoItem1);

    var cbMostrarTodoItem2 = new TVclComboBoxItem();
    cbMostrarTodoItem2.Value = 2;
    cbMostrarTodoItem2.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2");
    cbMostrarTodoItem2.Tag = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2");
    _this.cbMostrarTodo.AddItem(cbMostrarTodoItem2);  
    _this.cbMostrarTodo.selectedIndex = 1;


    _this.lblMostrar_X_Columna = new Vcllabel(Div2C1, "lblMostrar_X_Columna" + _this.ID, TVCLType.BS, TlabelType.H0, "");
    _this.lblMostrar_X_Columna.innerText = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblMostrar_X_Columna");
    
    _this.cbMostrar_X_Columna = new TVclComboBox2(Div2C2, "cbMostrar_X_Columna" + _this.ID, "", 0);
    $(_this.cbMostrar_X_Columna.This).css('width', "100%");
    $(_this.cbMostrar_X_Columna.This).addClass("TextFormat");
    _this.cbMostrar_X_Columna.onChange = function () {
        _this.FiltrarUsers(_this, _this.cbMostrar_X_Columna);
    }
    
    var cbMostrar_X_ColumnaItem1 = new TVclComboBoxItem();
    cbMostrar_X_ColumnaItem1.Value = 1;
    cbMostrar_X_ColumnaItem1.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3");
    cbMostrar_X_ColumnaItem1.Tag = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3");
    _this.cbMostrar_X_Columna.AddItem(cbMostrar_X_ColumnaItem1);
    _this.cbMostrar_X_Columna.selectedIndex = 0;

    _this.lblBuscador = new Vcllabel(Div3C1, "lblBuscador" + _this.ID, TVCLType.BS, TlabelType.H0, "");
    _this.lblBuscador.innerText = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblBuscador");
    
    _this.txtBuscador = new TVclTextBox(Div3C2, "txtBuscador" + _this.ID);
    $(_this.txtBuscador.This).css('width', "100%");
    $(_this.txtBuscador.This).addClass("TextFormat");
    _this.txtBuscador.This.onkeydown = function validar(e) {
        if ((_this.txtBuscador.Text).length > 2) {
            _this.FiltrarUsers(_this, _this.txtBuscador);        
        }
    }

    _this.btnAceptar = new TVclButton(Div5C, "btnAceptar" + _this.ID);
    _this.btnAceptar.VCLType = TVCLType.BS;
    _this.btnAceptar.Src = "image/24/accept24.png"
    _this.btnAceptar.Cursor = "pointer";
    _this.btnAceptar.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnAccept");
    _this.btnAceptar.onClick = function () {
        _this.btnAceptar_Click(_this, _this.btnAceptar);
    }
    $(_this.btnAceptar.This).css("float", "right");
    $(_this.btnAceptar.This).css("margin", "10px");
    $(_this.btnAceptar.This).css("cursor", "pointer");
    
    _this.btnCancelar = new TVclButton(Div5C, "btnCancel" + _this.ID);
    _this.btnCancelar.VCLType = TVCLType.BS;
    _this.btnCancelar.Src = "image/24/close24.png"
    _this.btnCancelar.Cursor = "pointer";
    _this.btnCancelar.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnCancel");
    _this.btnCancelar.onClick = function () {
        _this.btnCancelar_Click(_this, _this.btnCancelar);
    }
    $(_this.btnCancelar.This).css("float", "right");
    $(_this.btnCancelar.This).css("margin", "10px");
    $(_this.btnCancelar.This).css("cursor", "pointer");


    _this.CallbackModalResult(_this);
}

ItHelpCenter.SD.AdvSearch.TAdvSearchManager.prototype.Inicialize = function (inCOD_SEARCH, inSQL, inCallback) {
    var _this = this.TParent();   
    _this.CallbackModalResult = inCallback;

    _this.SQL = inSQL;
    _this.COD_SEARCH = inCOD_SEARCH;

    _this.CargarComboFilas();
    _this.VerCombos();
}

ItHelpCenter.SD.AdvSearch.TAdvSearchManager.prototype.CargarComboFilas = function () {
    var _this = this.TParent();

    var Parametro1 = "Top 0";
    var Parametro2 = "";
    var Parametro3 = "";
 
    Open = SysCfg.DB.SQL.Methods.Open("SELECT " + Parametro1 + " * FROM (" + _this.SQL + " ) as TblSearch " + Parametro2 + Parametro3, new Array());

    if (Open.ResErr.NotError)
    {
        for (var i = 0; i < Open.DataSet.FieldDefs.length; i++)
        {
            if (Open.DataSet.FieldDefs[i].DataType == SysCfg.DB.Properties.TDataType.String)
            {
                var cbMostrar_X_ColumnaItem1 = new TVclComboBoxItem();
                cbMostrar_X_ColumnaItem1.Value = 1;
                cbMostrar_X_ColumnaItem1.Text = Open.DataSet.FieldDefs[i].FieldName;
                cbMostrar_X_ColumnaItem1.Tag = Open.DataSet.FieldDefs[i].FieldName;
                _this.cbMostrar_X_Columna.AddItem(cbMostrar_X_ColumnaItem1);
            }
        }
    }
}

ItHelpCenter.SD.AdvSearch.TAdvSearchManager.prototype.VerCombos = function () {
    var _this = this.TParent();

    if (_this.cbMostrarTodo.selectedIndex == 1) {        
  
        $(_this.DivMostrar_X_Columna).css("display", "");
        $(_this.DivtxtBuscador).css("display", "");       
        
        _this.cbMostrar_X_Columna.selectedIndex = 0;
    }
    else {
        $(_this.DivMostrar_X_Columna).css("display", "none");
        $(_this.DivtxtBuscador).css("display", "none");
    }
}

ItHelpCenter.SD.AdvSearch.TAdvSearchManager.prototype.cbMostrarTodo_SelectedIndexChanged = function (sender, e) {
    _this = sender;
    if (_this.cbMostrarTodo.selectedIndex == 0)
    {
        var Parametro1 = "";
        var Parametro2 = "";
        var Parametro3 = "";
        try {
            var Open = SysCfg.DB.SQL.Methods.Open("SELECT " + Parametro1 + " * FROM (" + _this.SQL + " ) as TblSearch " + Parametro2 + Parametro3, new Array());
            if (Open.ResErr.NotError) {
                _this.LoadGrid(Open.DataSet);
                _this.VerCombos();
            } else {
                SysCfg.App.Methods.ShowMessage(Open.ResErr.Mesaje);
            }
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("AdvSearch.js ItHelpCenter.SD.AdvSearch.TAdvSearchManager.prototype.cbMostrarTodo_SelectedIndexChanged ", e);
           //debugger;
        }       
 
    } else {
        _this.VerCombos();
    }
}

ItHelpCenter.SD.AdvSearch.TAdvSearchManager.prototype.LoadGrid = function (DataSet) {
    var _this = this.TParent();

    _this.DivGrid.innerHTML = "";
    var Grilla = new TGrid(_this.DivGrid, "", ""); //CREA UN GRID CONTRO

    for (var i = 0; i < DataSet.FieldCount; i++) {
        var Col0 = new TColumn(); //CREA UNA COLUMNA PARA AGREGAR AL GRID CONTROL
        Col0.Name = DataSet.FieldDefs[i].FieldName;
        Col0.Caption = DataSet.FieldDefs[i].FieldName;
        Col0.Index = i;
        Col0.EnabledEditor = false; //ASIGNA O EXTRAE UN VALO TRUE O FALSE QUE INDICA SI LA COLUMNA MOSTRARA UN EDITOR DE DATOS             
        Grilla.AddColumn(Col0); //AGREGA UNA COLUMNA AL GRIDCONTROL 
    }

    for (var i = 0; i < DataSet.RecordCount; i++) {//rows
        var NewRow = new TRow(); //CREA UN ROW PARA AGREGAR AL GRIDCONTROL  
        for (var j = 0; j < DataSet.FieldCount; j++) {//columns
            var Cell = new TCell();
            Cell.Value = DataSet.Records[i].Fields[j].Value;
            Cell.IndexColumn = j;
            Cell.IndexRow = i;
            NewRow.AddCell(Cell);

        }

        Grilla.AddRow(NewRow);
    }


    Grilla.OnRowClick = function (sender, EventArgs) {
        _this.RowSelected = EventArgs;
    }

}

ItHelpCenter.SD.AdvSearch.TAdvSearchManager.prototype.btnAceptar_Click = function (sender, e) {
    _this = sender;
    _this.CallbackModalResult(_this.RowSelected, _this.ParentThis);
}

ItHelpCenter.SD.AdvSearch.TAdvSearchManager.prototype.btnCancelar_Click = function (sender, e) {
    _this = sender;
    _this.CallbackModalResult(null, _this.ParentThis);
}

ItHelpCenter.SD.AdvSearch.TAdvSearchManager.prototype.FiltrarUsers = function (sender, e) {
    _this = sender;
    try {
        if (_this.cbMostrar_X_Columna.Text != UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3")) {
            if (_this.cbMostrar_X_Columna.Text != "") {
                if (_this.txtBuscador.Text.length > 2) {

                    var Parametro1 = "";
                    var Parametro2 = "";
                    var Parametro3 = "";

                    Parametro3 = " WHERE " + _this.cbMostrar_X_Columna.Text + " LIKE '%" + _this.txtBuscador.Text + "%'";

                   
                    var  Open = SysCfg.DB.SQL.Methods.Open("SELECT " + Parametro1 + " * FROM (" + _this.SQL + " ) as TblSearch " + Parametro2 + Parametro3, new Array());
                    if (Open.ResErr.NotError) {
                        _this.LoadGrid(Open.DataSet);
                    }
                    else {
                       //debugger;
                    }
                }
            }
        } else {
            if (_this.cbMostrar_X_Columna.Text != "") {
                var Parametro1 = "";
                var Parametro2 = "";
                var Parametro3 = "";

                for (var i = 1; i < _this.cbMostrar_X_Columna.Options.length; i++) {
                    if (i == 1) {
                        Parametro3 = " WHERE " + _this.cbMostrar_X_Columna.Options[i].Text + " LIKE '" + _this.txtBuscador.Text + "%'";
                    }
                    else {
                        Parametro3 += " OR " + _this.cbMostrar_X_Columna.Options[i].Text + " LIKE '" + _this.txtBuscador.Text + "%'";
                    }
                }

                var Open = SysCfg.DB.SQL.Methods.Open("SELECT " + Parametro1 + " * FROM (" + _this.SQL + " ) as TblSearch " + Parametro2 + Parametro3, new Array());

                if (Open.ResErr.NotError) {
                    _this.LoadGrid(Open.DataSet);
                }
                else {
                    
                }
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("AdvSearch.js ItHelpCenter.SD.AdvSearch.TAdvSearchManager.prototype.FiltrarUsers", e);
       //debugger;
    }

    
}



