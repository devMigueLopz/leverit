Componet.TGantt = function(inObjectHtml, Id, inCallBack){
	this.TParent = function () {
		return this;
	}.bind(this);
	var _this = this.TParent();
	this._ObjectHtml = inObjectHtml;
	this._Id = Id;
	this.CallBack = inCallBack;
	this._ProjectName = '';
	this._ProjectDuration = '';
	this._Height = '400px'
	this._SplitterPosition = 470; //posición en la que se mostrara el diagrama desde la izquierda;
	this._FitAll = true;
	this._Data = new Array();
	this._anychart = null;
	this._chart = null;
	this._eventClick = null;
	this._ColumnName = 'Name';
	this._ColumnDuration = 'Duration';
	this._ColumnStartTime = 'Start Time';
	this._ColumnEndTime = 'End Time';
	this._ColumnResources = 'Resources';
	this._ScaleMinimum = null;
	this._ScaleMaximum = null;
	this._DataGrid = true;
	this._BaseLine = true;
	this._ZoomUnit = '';
	this._ZoomCount = 0;

	Object.defineProperty(this, 'ProjectName', {
		get: function(){
			return this._ProjectName;
		},
		set: function(_Value){
			this._ProjectName = _Value;
		}
	});
	Object.defineProperty(this, 'ProjectDuration', {
		get: function(){
			return this._ProjectDuration;
		},
		set: function(_Value){
			this._ProjectDuration = _Value;
		}
	});
	Object.defineProperty(this, 'Height', {
		get: function(){
			return this._Height;
		},
		set: function(_Value){
			this._Height = _Value;
		}
	});
	Object.defineProperty(this, 'SplitterPosition', {
		get: function(){
			return this._SplitterPosition;
		},
		set: function(_Value){
			this._SplitterPosition = _Value;
		}
	});
	Object.defineProperty(this, 'FitAll', {
		get: function(){
			return this._FitAll;
		},
		set: function(_Value){
			this._FitAll = _Value;
		}
	});
	Object.defineProperty(this, 'ColumnName', {
		get: function(){
			return this._ColumnName;
		},
		set: function(_Value){
			if(_Value.trim() != ""){
				this._ColumnName = _Value;
			}
		}
	});
	Object.defineProperty(this, 'ColumnDuration', {
		get: function(){
			return this._ColumnDuration;
		},
		set: function(_Value){
			if(_Value.trim() != ""){
				this._ColumnDuration = _Value;
			}
		}
	});
	Object.defineProperty(this, 'ColumnStartTime', {
		get: function(){
			return this._ColumnStartTime;
		},
		set: function(_Value){
			if(_Value.trim() != ""){
				this._ColumnStartTime = _Value;
			}
		}
	});
	Object.defineProperty(this, 'ColumnEndTime', {
		get: function(){
			return this._ColumnEndTime;
		},
		set: function(_Value){
			if(_Value.trim() != ""){
				this._ColumnEndTime = _Value;
			}
		}
	});
	Object.defineProperty(this, 'ColumnResources', {
		get: function(){
			return this._ColumnResources;
		},
		set: function(_Value){
			if(_Value.trim() != ""){
				this._ColumnResources = _Value;
			}
		}
	});
	Object.defineProperty(this, 'DataGrid', {
		get: function(){
			return this._DataGrid;
		},
		set: function(_Value){
			this._DataGrid = _Value;
		}
	});
	Object.defineProperty(this, 'BaseLine', {
		get: function(){
			return this._BaseLine;
		},
		set: function(_Value){
			this._BaseLine = _Value;
		}
	});
	Object.defineProperty(this, 'ZoomUnit', {
		get: function(){
			return this._ZoomUnit;
		},
		set: function(_Value){
			this._ZoomUnit = _Value;
		}
	});
	Object.defineProperty(this, 'ZoomCount', {
		get: function(){
			return this._ZoomCount;
		},
		set: function(_Value){
			this._ZoomCount = _Value;
		}
	});
	Object.defineProperty(this, 'ScaleMinimum', {
		get: function(){
			return this._ScaleMinimum;
		},
		set: function(_Value){
			this._ScaleMinimum = _Value;
		}
	});
	Object.defineProperty(this, 'ScaleMaximum', {
		get: function(){
			return this._ScaleMaximum;
		},
		set: function(_Value){
			this._ScaleMaximum = _Value;
		}
	});
	Object.defineProperty(this, 'eventClick', {
		get: function(){
			return this._eventClick;
		},
		set: function(_Value){
			this._eventClick = _Value;
		}
	});
	this.TGanttDataAdd = function (inElement) {
		this._Data.push(inElement);
	}
}

Componet.TElements.TGanttElement = function(){
	this._Id = null;
	this._Index = null;
	this._Name = null;
	this._Parent= null;
	this._Duration = null;
	this._Resources = null;
	this._ProgressValue = null;
	this._ActualStart = null;
	this._ActualEnd = null;
	this._BaseLineStart = null;
	this._BaseLineEnd = null;
	this._Connector = new Array();

	Object.defineProperty(this, 'Id', {
		get: function(){
			return this._Id;
		},
		set: function(_Value){
			this._Id = _Value;
		}
	});
	Object.defineProperty(this, 'Index', {
		get: function(){
			return this._Index;
		},
		set: function(_Value){
			this._Index = _Value;
		}
	});
	Object.defineProperty(this, 'Name', {
		get: function(){
			return this._Name;
		},
		set: function(_Value){
			this._Name = _Value;
		}
	});
	Object.defineProperty(this, 'Parent', {
		get: function(){
			return this._Parent;
		},
		set: function(_Value){
			this._Parent = _Value;
		}
	});
	Object.defineProperty(this, 'Duration', {
		get: function(){
			return this._Duration;
		},
		set: function(_Value){
			this._Duration = _Value;
		}
	});
	Object.defineProperty(this, 'Resources', {
		get: function(){
			return this._Resources;
		},
		set: function(_Value){
			this._Resources = _Value;
		}
	});
	Object.defineProperty(this, 'ProgressValue', {
		get: function(){
			return this._ProgressValue;
		},
		set: function(_Value){
			this._ProgressValue = _Value;
		}
	});
	Object.defineProperty(this, 'ActualStart', {
		get: function(){
			return this._ActualStart;
		},
		set: function(_Value){
			this._ActualStart = _Value;
		}
	});
	Object.defineProperty(this, 'ActualEnd', {
		get: function(){
			return this._ActualEnd;
		},
		set: function(_Value){
			this._ActualEnd = _Value;
		}
	});
	Object.defineProperty(this, 'BaseLineStart', {
		get: function(){
			return this._BaseLineStart;
		},
		set: function(_Value){
			this._BaseLineStart = _Value; 
		}
	});
	Object.defineProperty(this, 'BaseLineEnd', {
		get: function(){
			return this._BaseLineEnd;
		},
		set: function(_Value){
			this._BaseLineEnd = _Value;
		}
	});
	Object.defineProperty(this, 'Connector', {
		get: function(){
			return this._Connector;
		},
		set: function(_Value){
			this._Connector = _Value;
		}
	});
}

Componet.TGantt.prototype.Create = function () {
	var _this = this.TParent();
	_this.eventClick = function(e,c,a,f){};
	_this.importarScript("Componet/Gantt/Scripts/anychart-base.min.js", function () {
		_this.importarScript("Componet/Gantt/Scripts/anychart-ui.min.js", function () {
			_this.importarScript("Componet/Gantt/Scripts/anychart-exports.min.js", function () {
				_this.importarScript("Componet/Gantt/Scripts/anychart-gantt.min.js", function () {
					_this.importarScript("Componet/Gantt/Scripts/anychart-data-adapter.min.js", function () {
						_this.importarStyle("Componet/Gantt/Css/anychart-ui.min.css", function () {
							_this.importarStyle("Componet/Gantt/Css/anychart-font.min.css", function () {
								_this._Drawing();
							}, function (e) {
								SysCfg.Log.Methods.WriteLog("Gantt.js Componet.TGantt.prototype.Create Componet.TGantt.prototype.importarStyle(" + SysCfg.App.Properties.xRaiz + "Componet/Gantt/Css/anychart-font.min.css)", e);
							});
						}, function (e) {
							SysCfg.Log.Methods.WriteLog("Gantt.js Componet.TGantt.prototype.Create Componet.TGantt.prototype.importarStyle(" + SysCfg.App.Properties.xRaiz + "Componet/Gantt/Css/anychart-ui.min.css)", e);
						});
					}, function (e) {
						SysCfg.Log.Methods.WriteLog("Gantt.js Componet.TGantt.prototype.Create Componet.TGantt.prototype.importarScript(" + SysCfg.App.Properties.xRaiz + "Componet/Gantt/Scripts/anychart-data-adapter.min.js)", e);
					});
				}, function (e) {
					SysCfg.Log.Methods.WriteLog("Gantt.js Componet.TGantt.prototype.Create Componet.TGantt.prototype.importarScript(" + SysCfg.App.Properties.xRaiz + "Componet/Gantt/Scripts/anychart-gantt.min.js)", e);
				});
			}, function (e) {
				SysCfg.Log.Methods.WriteLog("Gantt.js Componet.TGantt.prototype.Create Componet.TGantt.prototype.importarScript(" + SysCfg.App.Properties.xRaiz + "Componet/Gantt/Scripts/anychart-exports.min.js)", e);
			});
		}, function (e) {
			SysCfg.Log.Methods.WriteLog("Gantt.js Componet.TGantt.prototype.Create Componet.TGantt.prototype.importarScript(" + SysCfg.App.Properties.xRaiz + "Componet/Gantt/Scripts/anychart-ui.min.js)", e);
		});
	}, function (e) {
		SysCfg.Log.Methods.WriteLog("Gantt.js Componet.TGantt.prototype.Create Componet.TGantt.prototype.importarScript(" + SysCfg.App.Properties.xRaiz + "Componet/Gantt/Scripts/anychart-base.min.js)", e);
	});
}

Componet.TGantt.prototype._Drawing = function(){
	var _this = this.TParent();

	// _this.inicio = new Date((_this.ScaleMinimum.getTime())-(2*86400000)); //Fecha inicial
	// _this.fin = new Date((_this.ScaleMaximum.getTime())+(2*86400000)); //Fecha final
	// _this.timeDiff = Math.abs(_this.fin.getTime() - _this.inicio.getTime());
	// _this.diffDays = Math.ceil(_this.timeDiff / (86400000)); //Días entre las dos fechas

	var GanttContainer = new TVclStackPanel(_this._ObjectHtml, 'GanttContainer_' + _this._Id, 1, [[12], [12], [12], [12], [12]]);

	GanttContainer.Column[0].This.style.height = _this.Height;

	_this.data = new Array();
	if(_this.BaseLine){
		if(_this._Data.length > 0){
			_this.data.push({
				id: 0,
				index: 0,
				name: _this.ProjectName,
				parent: null,
				duration: _this.ProjectDuration,
				resources: '',
				progressValue: '0%',
				actualStart: _this.ScaleMinimum,
				actualEnd: _this.ScaleMaximum,
				connector: null,
				actual: {fill: 'gray'}
			});
		}
	}
	for(var i = 0 ; i < _this._Data.length; i++){
		_this.data.push({
			id: _this._Data[i].Id,
			index: _this._Data[i].Index,
			name: _this._Data[i].Name,
			parent: _this._Data[i].Parent,
			duration: _this._Data[i].Duration,
			resources: _this._Data[i].Resources,
			progressValue: _this._Data[i].ProgressValue + '%',
			actualStart: _this._Data[i].ActualStart,
			actualEnd: _this._Data[i].ActualEnd,
			connector: _this._Data[i].Connector
		});
		
	}
	anychart.onDocumentReady(function() {
		var treeData = anychart.data.tree(_this.data, 'as-table');

		var chart = anychart.ganttProject();
		_this._anychart = anychart;
		_this._chart = chart; // puedo diagramar sin llamar al componente de nuevo solo ingresandole nuevos datos char.data(Datos*);

		chart.data(treeData);
		chart.splitterPosition(_this.SplitterPosition); //posición del diagrama de gantt

		var dataGrid = chart.dataGrid();
		timeline = chart.getTimeline();

		dataGrid.tooltip().format(function(){
			return 	"Start Date: " + _this.ToDateTimeMeridian(new Date(this.actualStart)) + "\n" +
					"End Date: " + _this.ToDateTimeMeridian(new Date(this.actualEnd)) + "\n" +
					"Complete: " + this.progressValue + "\n"
			});
		timeline.tooltip().format(function(){ 
			return 	"Start Date: " + _this.ToDateTimeMeridian(new Date(this.actualStart)) + "\n" +
					"End Date: " + _this.ToDateTimeMeridian(new Date(this.actualEnd)) + "\n" +
					"Complete: " + this.progressValue + "\n"
		});
		
		// set first column settings
		dataGrid.column(0)
		.title('#')
		.width(30)
		.labels({
			hAlign: 'center'
		})
		/*.format("{%index}")*/;

		// set second column settings
		dataGrid.column(1)
		.title(_this.ColumnName)
		.labels()
		.hAlign('left')
		.width(180);

		dataGrid.column(2)
		.title(_this.ColumnDuration)
		.labels()
		.hAlign('left')
		.width(180)
		.format("{%duration}");
		// dataGrid.column(2).labels().useHtml(true);

		// set third column settings
		dataGrid.column(3)
		.title(_this.ColumnStartTime)
		.width(70)
		.labels()
		.hAlign('right')
		.format(function() {
			var date = new Date(this.actualStart);
			return _this.ToDateTimeMeridian(date);
		});

		dataGrid.column(4)
		.title(_this.ColumnEndTime)
		.width(80)
		.labels()
		.hAlign('right')
		.format(function() {
			var date = new Date(this.actualEnd);
			return _this.ToDateTimeMeridian(date);
		});

		dataGrid.column(5)
		.title(_this.ColumnResources)
		.labels()
		.hAlign('left')
		.width(180)
		.format("{%resources}");

		// Contenedor Gantt
		chart.container(GanttContainer.Column[0].This);

		// chart.credits().text('Hello')  // Solo posible si se cuenta con licencia
		chart.dataGrid(_this.DataGrid); //para que no se muestre las columnas
		// initiate chart drawing
		chart.draw();
		
		if(_this.FitAll){
			chart.fitAll(); // permite visualizar el gantt completo
		}

		// chart.xScale().minimum(Date.UTC(2018, 6, 2));
		// chart.xScale().maximum(Date.UTC(2042, 7, 2));
		// // Set zoom to range.
		// chart.zoomTo("day", 20, "first-date");
		var scale = chart.xScale();
		scale.minimum(new Date((_this.ScaleMinimum.getTime())-(2*86400000)));
		scale.maximum(new Date((_this.ScaleMaximum.getTime())+(2*86400000)));
		_this.ZoomTo(_this.ZoomUnit, _this.ZoomCount);
		
		
		// // Determinar sabados y domingos
		// var tl = chart.getTimeline();
		// var arrDays = new Array(); 
		// var contador = 0;
		// for (var i = 0; i < _this.diffDays; i++) 
		// {
		// 	//0 => Domingo - 6 => Sábado
		// 	if (_this.inicio.getDay() == 0 || _this.inicio.getDay() == 6) {
		// 		contador++;
		// 		tl.rangeMarker(contador).from(new Date(_this.inicio.getTime())).to(new Date(_this.inicio.getTime()+86400000));
		// 		tl.rangeMarker(contador).fill('#FFE4C4 0.5');
		// 	}
		// 	_this.inicio.setDate(_this.inicio.getDate() + 1);
		// }
		
		chart.listen('rowClick', function(e){
			_this.eventClick(e.item, _this.data);
		})
		
		setTimeout(function(){
			$(document.getElementsByClassName('anychart-credits')).remove();
		}, 10);
	});
}

Componet.TGantt.prototype.CleanData = function(){
	var _this = this.TParent();
	_this._Data = new Array();
	_this.data = new Array();
}

Componet.TGantt.prototype.Refresh = function(){
	var _this = this.TParent();
	if(_this.BaseLine){
		if(_this._Data.length > 0){
			_this.data.push({
				id: 0,
				index: 0,
				name: _this.ProjectName,
				parent: null,
				duration: _this.ProjectDuration,
				resources: '',
				progressValue: '0%',
				actualStart: _this.ScaleMinimum,
				actualEnd: _this.ScaleMaximum,
				connector: null,
				actual: {fill: 'gray'}
			});
		}
	}
	for(var i = 0 ; i < _this._Data.length; i++){
		_this.data.push({
			id: _this._Data[i].Id,
			index: _this._Data[i].Index,
			name: _this._Data[i].Name,
			parent: _this._Data[i].Parent,
			duration: _this._Data[i].Duration,
			resources: _this._Data[i].Resources,
			progressValue: _this._Data[i].ProgressValue + '%',
			actualStart: _this._Data[i].ActualStart,
			actualEnd: _this._Data[i].ActualEnd,
			connector: _this._Data[i].Connector
		});
		
	}
	var treeData = _this._anychart.data.tree(_this.data, 'as-table');
	_this._chart.data(treeData);
	if(_this.FitAll){
		_this._chart.fitAll();
	}
	var scale = _this._chart.xScale();
	scale.minimum(new Date((_this.ScaleMinimum.getTime())-(2*86400000)));
	scale.maximum(new Date((_this.ScaleMaximum.getTime())+(2*86400000)));

	_this.ZoomTo(_this.ZoomUnit, _this.ZoomCount);
}

Componet.TGantt.prototype.ZoomTo = function(unit, count){
	var _this = this.TParent();
	if(unit != '' && unit != null){
		if(count != 0 && count != null){
			// if(_this.ScaleMinimum != null && _this.ScaleMaximum != null){
			// 	_this._chart.xScale().minimum(_this.ScaleMinimum);
			// 	_this._chart.xScale().maximum(_this.ScaleMaximum);
			// }
			_this.ZoomUnit = unit;
			_this.ZoomCount = count;
			_this._chart.zoomTo(unit, count, "first-date");
		}
	}
}

Componet.TGantt.prototype.GenerateBaseLine = function(){
	var _this = this.TParent();
	_this.data = new Array();
	if(_this.BaseLine){
		if(_this._Data.length > 0){
			_this.data.push({
				id: 0,
				index: 0,
				name: _this.ProjectName,
				parent: null,
				duration: _this.ProjectDuration,
				resources: '',
				progressValue: '0%',
				actualStart: _this.ScaleMinimum,
				actualEnd: _this.ScaleMaximum,
				connector: null,
				baselineStart: _this.ScaleMinimum,
				baselineEnd: _this.ScaleMaximum,
				actual: {fill: 'gray'},
				baseline:
				{
					//'stroke': '3 black',
					fill: {'color': 'gray'}
				}
			});
		}
	}
	for(var i = 0 ; i < _this._Data.length; i++){
		_this.data.push({
			id: _this._Data[i].Id,
			index: _this._Data[i].Index,
			name: _this._Data[i].Name,
			parent: _this._Data[i].Parent,
			duration: _this._Data[i].Duration,
			resources: _this._Data[i].Resources,
			progressValue: _this._Data[i].ProgressValue + '%',
			actualStart: _this._Data[i].ActualStart,
			actualEnd: _this._Data[i].ActualEnd,
			connector: _this._Data[i].Connector,
			baselineStart: _this._Data[i].BaseLineStart,
			baselineEnd: _this._Data[i].BaseLineEnd,
			baseline:
			{
				//'stroke': '3 black',
				fill: {'color': 'gray'}
			}
		});
		
	}
	var treeData = _this._anychart.data.tree(_this.data, 'as-table');
	_this._chart.data(treeData);
	_this._chart.getTimeline().baselines().above(true);
	if(_this.FitAll){
		_this._chart.fitAll();
	}
	var scale = _this._chart.xScale();
	scale.minimum(new Date((_this.ScaleMinimum.getTime())-(2*86400000)));
	scale.maximum(new Date((_this.ScaleMaximum.getTime())+(2*86400000)));
	_this.ZoomTo(_this.ZoomUnit, _this.ZoomCount);
}


Componet.TGantt.prototype.importarScript = function (nombre, onSuccess, onError) {
	var s = document.createElement("script");
	s.onload = onSuccess;
	s.onerror = onError;
	s.src = nombre;
	document.querySelector("head").appendChild(s);
}

Componet.TGantt.prototype.importarStyle = function (nombre, onSuccess, onError) {
	var style = document.createElement("link");
	style.rel = "stylesheet";
	style.type = "text/css";
	style.href = nombre;
	var s = document.head.appendChild(style);
	s.onload = onSuccess;
	s.onerror = onError;
}

// Reference : "Scripts/SysCfgScripts/DateTimeMethods.js"
Componet.TGantt.prototype.ToDateTimeMeridian = function(d) {
	var month = d.getMonth() + 1;
	var day = d.getDate();
	var year = d.getFullYear();
	var hour = d.getHours();
	var min = d.getMinutes();
	var AMPM = (hour > 11) ? "PM" : "AM";
	if(hour > 12) {
		hour -= 12;
	}
	else if(hour == 0) {
		hour = "12";
	}
	var date = SysCfg.DateTimeMethods.Pad(day, 2) + "/" + SysCfg.DateTimeMethods.Pad(month, 2) + "/" + year + " " + SysCfg.DateTimeMethods.Pad(hour, 2) + ":" + SysCfg.DateTimeMethods.Pad(min, 2) + " " + AMPM;
	return date;
}