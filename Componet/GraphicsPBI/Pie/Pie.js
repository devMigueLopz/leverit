Componet.TGraphicsPBI.TVclPie = function (ObjectHtml, Id) {
    this.TParent = function () {
        return this;
    }.bind(this);
    this.ObjectHtml = ObjectHtml;
    this.ID = Id;
    this._Elements = new Array();
    this.Plugin = null;
    this._onClickPlugin = null;
    this.BoxGraphics = [];
    this._LegendDisplay = false;
    this._Title = "";
    this._BorderWidth = 1;
    this._Option = true;
    this.BoxPlugin = null;
    this._HeightPlugin = "250px";
    this._Data = new Array();
    this._TitleHighlight = "Highligh";
    this.PieElementsAdd = function (inElemento) {
        this._Elements.push(inElemento);
    }
    this.ResetElementsHighlight = function () {
        for (var i = 0; i < this.Elements.length; i++) {
            this.Elements[i].ValueHighlight = 0;
            this.Elements[i].DisplayHighlight = false;
        }
    }
    this.ResetOption = function () {
        for (var i = 0; i < this.Elements.length; i++) {
            this.Elements[i].ValueText = this._Elements[i]._Value;
        }
    }
    this.ResetFlagClick = function () {
        for (var i = 0; i < this.Elements.length; i++) {
            this.Elements[i].FlagClick = false;
        }
    }
    this.SearchElement = function (Label) {
        for (var i = 0; i < this.Elements.length; i++) {
            if (this.Elements[i]._DisplayValue.trim() === Label.trim()) {
                return this._Elements[i];
            }
        }
        return null;
    }
    this._Type = {
        Bar: { value: 1, name: "bar" }
    };
    Object.defineProperty(this, 'Elements', {
        get: function () {
            return this._Elements;
        },
        set: function (_Value) {
            this._Elements = _Value;
        }
    });
    Object.defineProperty(this, 'Option', {
        get: function () {
            return this._Option;
        },
        set: function (_Value) {
            this._Option = _Value;
        }
    });
    Object.defineProperty(this, 'BoxPlugin', {
        get: function () {
            return this._BoxPlugin;
        },
        set: function (_Value) {
            this._BoxPlugin = _Value;
        }
    });

    Object.defineProperty(this, 'HeightPlugin', {
        get: function () {
            return this._HeightPlugin;
        },
        set: function (_Value) {
            this._HeightPlugin = _Value;
        }
    });
    Object.defineProperty(this, 'BorderWidth', {
        get: function () {
            return this._BorderWidth;
        },
        set: function (_Value) {
            this._BorderWidth = _Value;
        }
    });
    Object.defineProperty(this, 'Title', {
        get: function () {
            return this._Title;
        },
        set: function (_Value) {
            this._Title = _Value;
            if (this.Plugin != null) {
                this.Plugin.config.data.datasets[0].label = _Value;
                this.Plugin.update();
            }
        }
    });
    Object.defineProperty(this, 'TitleHighlight', {
        get: function () {
            return this._TitleHighlight;
        },
        set: function (_Value) {
            this._TitleHighlight = _Value;
        }
    });
    Object.defineProperty(this, 'LegendDisplay', {
        get: function () {
            return this._LegendDisplay;
        },
        set: function (_Value) {
            this._LegendDisplay = _Value;
            if (this.Plugin != null) {
                this.Plugin.defaults.global.legend.display = false;
                this.Plugin.update();
            }
        }
    });
    Object.defineProperty(this, 'onClickPlugin', {
        get: function () {
            return this._onClickPlugin;
        },
        set: function (_Value) {
            this._onClickPlugin = function (evt) {
                var activePoints = this.getElementsAtEvent(evt);
                if (activePoints[0]) {
                    var chartData = activePoints[0]['_chart'].config.data;
                    var idx = activePoints[0]['_index'];
                    _Value(chartData, idx)
                }
            };
            if (this.Plugin != null) {
                this.Plugin.config.options.onClick = this._onClickPlugin;
            }
        }
    });

}
Componet.TGraphicsElements.TVclPieElements = function () {
    this.ColorHighlight = {
        ColorElementFather: { value: 1, name: "rgba(166, 166, 166, 0.46)" },
        ColorElementChild: { value: 2, name: "rgba(95, 118, 129, 1)" }
    };
    this._Color = null;
    this._Display = null;
    this._Value = null;
    this._ValueText = null;
    this._Id = null;
    this._DisplayValue = null;
    this._Text = null;
    this._ColorOption = null;
    this._BorderOption = null;
    this._DisplayOption = null;
    this._BorderColorGraphic = null;
    this._HoverColorGraphic = null;
    this.FlagClick = false;
    this._ColorText = null;
    this._ElementoOptionDOM = null;
    this._DisplayHighlight = true;
    this._ValueHighlight = 0;
    this.IndexHighlight = 0;
    this.Parent = null;
    this._onClick = null;
    var _this = this;

    Object.defineProperty(this, 'Color', {
        get: function () {
            return this._Color;
        },
        set: function (_Value) {
            this._Color = _Value;
            if (this.Parent !== null) {
                this.Parent.Plugin.config.data.datasets[0].backgroundColor[this.IndexHighlight + 1] = this._Color;
                this.Parent.Plugin.update();
            }
        }
    });
    Object.defineProperty(this, 'ColorOption', {
        get: function () {
            return this._ColorOption;
        },
        set: function (_Value) {
            this._ColorOption = _Value;
            if (this._ElementoOptionDOM !== null) {
                this._ElementoOptionDOM.BBC.style.background = _Value;
            }
        }
    });
    Object.defineProperty(this, 'ColorText', {
        get: function () {
            return this._ColorText;
        },
        set: function (_Value) {
            this._ColorText = _Value;
            if (this._ElementoOptionDOM !== null) {
                this._ElementoOptionDOM.BBC.style.color = _Value;
            }
        }
    });
    Object.defineProperty(this, 'ValueHighlight', {
        get: function () {
            return this._ValueHighlight;
        },
        set: function (_Value) {
            this._ValueHighlight = _Value;
            if (this.Parent !== null) {
                if (this._DisplayHighlight) {
                    this.Parent.Plugin.config.data.datasets[0].data[this.IndexHighlight] = parseInt(this._ValueHighlight);
                    this.Parent.Plugin.config.data.datasets[0].data[this.IndexHighlight + 1] = (parseInt(this._Value) - parseInt(this._ValueHighlight));
                    this.Parent.Plugin.config.data.datasets[0].backgroundColor[this.IndexHighlight + 1] = this.ColorHighlight.ColorElementFather.name;
                    this.Parent.Plugin.config.data.datasets[0].borderColor[this.IndexHighlight + 1] = this.ColorHighlight.ColorElementFather.name;
                    this.Parent.Plugin.config.data.datasets[0].hoverBackgroundColor[this.IndexHighlight + 1] = this.ColorHighlight.ColorElementFather.name;
                    this.Parent.Plugin.update();
                }
            }
        }
    });
    Object.defineProperty(this, 'DisplayHighlight', {
        get: function () {
            return this._DisplayHighlight;
        },
        set: function (_Value) {
            this._DisplayHighlight = _Value;
            if (this.Parent !== null) {
                if (this._DisplayHighlight) {
                    this.Parent.Plugin.config.data.datasets[0].data[this.IndexHighlight] = parseInt(this._ValueHighlight);
                    this.Parent.Plugin.config.data.datasets[0].data[this.IndexHighlight + 1] = (parseInt(this._Value) - parseInt(this._ValueHighlight));
                    this.Parent.Plugin.config.data.datasets[0].backgroundColor[this.IndexHighlight + 1] = this.ColorHighlight.ColorElementFather.name;
                    this.Parent.Plugin.config.data.datasets[0].borderColor[this.IndexHighlight + 1] = this.ColorHighlight.ColorElementFather.name;
                    this.Parent.Plugin.config.data.datasets[0].hoverBackgroundColor[this.IndexHighlight + 1] = this.ColorHighlight.ColorElementFather.name;
                    this.Parent.Plugin.update();

                }
                else {
                    this.Parent.Plugin.config.data.datasets[0].data[this.IndexHighlight + 1] = parseInt(this._Value);
                    this.Parent.Plugin.config.data.datasets[0].data[this.IndexHighlight] = 0;
                    this.Parent.Plugin.config.data.datasets[0].backgroundColor[this.IndexHighlight + 1] = this._Color;
                    this.Parent.Plugin.config.data.datasets[0].borderColor[this.IndexHighlight + 1] = this._BorderColorGraphic;
                    this.Parent.Plugin.config.data.datasets[0].hoverBackgroundColor[this.IndexHighlight + 1] = this._HoverColorGraphic;

                }
                this.Parent.Plugin.update();
            }
        }
    });
    Object.defineProperty(this, 'BorderOption', {
        get: function () {
            return this._BorderOption;
        },
        set: function (_Value) {
            this._BorderOption = _Value;
            if (this._ElementoOptionDOM !== null) {
                this._ElementoOptionDOM.BBC.style.borderColor = _Value;
            }
        }
    });
    Object.defineProperty(this, 'Text', {
        get: function () {
            return this._Text;
        },
        set: function (_Value) {
            this._Text = _Value;
            if (this._ElementoOptionDOM !== null) {
                this._ElementoOptionDOM.Text.innerText = _Value;
            }
        }
    });
    Object.defineProperty(this, 'Value', {
        get: function () {
            return this._Value;
        },
        set: function (_Value) {
            this._Value = _Value;
            if (this.Parent !== null) {
                if (this._Value > 0) {
                    this.Parent.Plugin.config.data.datasets[0].data[this.IndexHighlight + 1] = parseInt(this._Value) - parseInt(this.Parent.Plugin.config.data.datasets[0].data[this.IndexHighlight]);
                }
                else {
                    this.Parent.Plugin.config.data.datasets[0].data[this.IndexHighlight + 1] = 0;
                }

                this.Parent.Plugin.update();
            }
            if (this._ElementoOptionDOM !== null) {
                this._ElementoOptionDOM.Value.innerText = _Value;
            }
        }
    });
    Object.defineProperty(this, 'ValueText', {
        get: function () {
            return this._ValueText;
        },
        set: function (_Value) {
            if (this._ElementoOptionDOM !== null) {
                this._ElementoOptionDOM.ValueText.innerText = _Value;
            }
        }
    });
    Object.defineProperty(this, 'DisplayValue', {
        get: function () {
            return this._DisplayValue;
        },
        set: function (_Value) {
            this._DisplayValue = _Value;
            if (this.Parent !== null) {
                this.Parent.Plugin.config.data.labels[this.IndexHighlight + 1] = this._DisplayValue;
                this.Parent.Plugin.update();
            }
            if (this._ElementoOptionDOM !== null) {
                this._ElementoOptionDOM.DisplayValue.innerText = _Value;
            }
        }
    });
    Object.defineProperty(this, 'DisplayOption', {
        get: function () {
            return this._DisplayOption;
        },
        set: function (_Value) {
            this._DisplayOption = _Value;
            if (this._ElementoOptionDOM !== null) {
                _Value ? this._ElementoOptionDOM.BoxOption.style.display = "block" : this._ElementoOptionDOM.BoxOption.style.display = "none";
            }
        }
    });
    Object.defineProperty(this, 'BorderColorGraphic', {
        get: function () {
            return this._BorderColorGraphic;
        },
        set: function (_Value) {
            this._BorderColorGraphic = _Value;
            if (this.Parent !== null) {
                this.Parent.Plugin.config.data.datasets[0].borderColor[this.IndexHighlight + 1] = this._BorderColorGraphic;
                this.Parent.Plugin.update();
            }
        }
    });
    Object.defineProperty(this, 'HoverColorGraphic', {
        get: function () {
            return this._HoverColorGraphic;
        },
        set: function (_Value) {
            this._HoverColorGraphic = _Value;
            if (this.Parent !== null) {
                this.Parent.Plugin.config.data.datasets[0].hoverBackgroundColor[this.IndexHighlight + 1] = this._HoverColorGraphic;
                this.Parent.Plugin.update();
            }
        }
    });
    Object.defineProperty(this, 'Id', {
        get: function () {
            return this._Id;
        },
        set: function (_Value) {
            this._Id = _Value;
        }
    });
    Object.defineProperty(this, 'ElementoOptionDOM', {
        get: function () {
            return this._ElementoOptionDOM;
        },
        set: function (_Value) {
            this._ElementoOptionDOM = _Value;
            if (this._ElementoOptionDOM != null) this._ElementoOptionDOM.BoxOption.onclick = this._onClick;
        }
    });
    Object.defineProperty(this, 'onClick', {
        get: function () {
            return this._onClick
        },
        set: function (_Value) {
            this._onClick = function () {
                _Value(_this);
            };
            if (this._ElementoOptionDOM != null) this._ElementoOptionDOM.BoxOption.onclick = this._onClick;

        }
    })
}
Componet.TGraphicsPBI.TVclPie.prototype.CreateGraphics = function () {
    var _this = this.TParent();
    try {
        if (_this.Option) {
            var boxPie = new TVclStackPanel(_this.ObjectHtml, _this.ID + "_Box", 2, [[12, 12], [6, 6], [7, 5], [6, 6], [5, 7]]);
            _this._BoxPlugin = boxPie;
            _this.BoxGraphics.push(boxPie.Column[0].This);
            _this.BoxGraphics.push(boxPie.Column[1].This);
            var canvas = UCanvas(_this.BoxGraphics[0], _this.BoxGraphics[0].id + "Pie")
            var ctx = canvas.getContext('2d');
            canvas.style.height = _this._HeightPlugin;//'250px';
        }
        else {
            var boxPie = new TVclStackPanel(_this.ObjectHtml, _this.ID + "_Box", 2, [[12, 12], [12, 12], [12, 12], [12, 7], [12, 12]]);
            _this._BoxPlugin = boxPie;
            _this.BoxGraphics.push(boxPie.Column[0].This);
            _this.BoxGraphics.push(boxPie.Column[1].This);
            _this.BoxGraphics[1].style.display = "none";
            var canvas = UCanvas(_this.BoxGraphics[0], _this.BoxGraphics[0].id + "Pie")
            var ctx = canvas.getContext('2d');
            canvas.style.height = _this._HeightPlugin;//'150px';
        }
        var DataValue = _this.CreateDataValue();
        _this.Plugin = new Chart(ctx, {
            type: "pie",
            data: {
                labels: DataValue.labels,
                datasets: [{
                    label: _this._Title,
                    data: DataValue.data,
                    backgroundColor: DataValue.backgroundColor,
                    borderColor: DataValue.borderColor,
                    borderWidth: _this._BorderWidth,
                    hoverBackgroundColor: DataValue.hoverBackgroundColor,
                }]
            },
            options: {
                legend: { display: _this._LegendDisplay },
                responsive: true,
                maintainAspectRatio: false,
                tooltips: {
                    callbacks: {
                        label: function (tooltipItem, data) {
                            var result = 0;
                            if (tooltipItem.index != 0 && ((tooltipItem.index % 2) == 1)) {

                                result = data.datasets[0].data[tooltipItem.index] + data.datasets[0].data[tooltipItem.index - 1]

                            }
                            else {
                                result = data.datasets[0].data[tooltipItem.index]
                            }
                            return tooltipItem.yLabel = data.labels[tooltipItem.index] + " : " + result;
                        }
                    }
                }
            }
        });
        this.DrawOptionDOM(_this.BoxGraphics[1]);
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Pie.js Componet.TGraphicsPBI.TVclPie.prototype.CreateGraphics ", e);
    }
}
Componet.TGraphicsPBI.TVclPie.prototype.DrawOptionDOM = function (divGraphicBarras_c1) {
    var _this = this.TParent();
    try {
        for (var i = 0; i < _this._Elements.length; i++) {
            var divGraphicOption = new TVclStackPanel(divGraphicBarras_c1, _this.ID + "_BoxOptions", 1, [[12], [12], [12], [12], [12]]);
            var divGraphicOption_c = divGraphicOption.Column[0].This;
            var InfoBoxContent = Udiv(divGraphicOption_c, "");
            var InfoBox = Udiv(InfoBoxContent, "");
            //
            var spanText = Uspan(InfoBox, "", "");
            var spanNumber = Uspan(InfoBox, "", "");
            var spanContent = Uspan(InfoBox, "", "");
            //
            spanText.setAttribute("class", "info-box-text-pie");
            spanText.innerHTML = _this._Elements[i]._DisplayValue;
            spanNumber.setAttribute("class", "info-box-number-pie");
            spanNumber.innerHTML = _this._Elements[i]._Value;
            spanContent.setAttribute("class", "info-box-contentText-pie");
            spanContent.innerHTML = _this._Elements[i]._Text;
            InfoBox.setAttribute("class", "info-box-content-pie");
            InfoBoxContent.setAttribute("class", "info-box-pie");
            /**/
            InfoBoxContent.style.borderLeftColor = _this._Elements[i]._BorderOption;
            spanNumber.style.color = _this._Elements[i]._BorderOption;
            InfoBoxContent.style.background = _this._Elements[i]._ColorOption;
            _this._Elements[i]._DisplayOption ? divGraphicOption.Row.This.style.display = "block" : divGraphicOption.Row.This.style.display = "none"
            InfoBoxContent.style.color = _this._Elements[i]._ColorText;
            /**/
            _this._Elements[i].ElementoOptionDOM = { BoxOption: divGraphicOption.Row.This, BBC: InfoBoxContent, Value: spanNumber, ValueText: spanNumber, DisplayValue: spanText, Text: spanContent }
            _this._Elements[i].Parent = _this;
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Pie.js Componet.TGraphicsPBI.TVclPie.prototype.DrawOptionDOM", e);
    }
}
Componet.TGraphicsPBI.TVclPie.prototype.CreateDataValue = function () {
    var _this = this.TParent();
    var _Obj = { data: [], labels: [], backgroundColor: [], borderColor: [], hoverBackgroundColor: [] };
    try {
        for (var i = 0; i < _this._Elements.length; i++) {
            _this._Elements[i].IndexHighlight = _Obj.data.length
            _Obj.data.push(0);
            _Obj.labels.push(_this._TitleHighlight);
            _Obj.backgroundColor.push(_this._Elements[i].ColorHighlight.ColorElementChild.name);
            _Obj.borderColor.push(_this._Elements[i].ColorHighlight.ColorElementChild.name);
            _Obj.hoverBackgroundColor.push(_this._Elements[i].ColorHighlight.ColorElementChild.name);
            /***************************************************************************************/
            _Obj.data.push(_this._Elements[i]._Value);
            _Obj.labels.push(_this._Elements[i]._DisplayValue);
            _Obj.backgroundColor.push(_this._Elements[i]._Color);
            _Obj.borderColor.push(_this._Elements[i]._BorderColorGraphic);
            _Obj.hoverBackgroundColor.push(_this._Elements[i]._HoverColorGraphic);
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Pie.js Componet.TGraphicsPBI.TVclPie.prototype.CreateDataValue", e);
    }
    return _Obj;
}