Componet.TGraphicsPBI.TVclFunnel = function (inObjectHtml, Id) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.ObjectHtml = inObjectHtml;
    this.Id = "GraphicBarRow_" + Id;
    this._BoxPlugin = null;
    this._Height = null;
    this._Width = null;
    this._Left = null;
    this._Rigth = null;
    this._Top = null;
    this._Bottom = null;
    this._AlighLeft = null;
    this._AlighRight = null;
    this._AlighTop = null;
    this._AlighBottom = null;
    this._Title = "Graphic Title";
    this._DisplayTitle = false;
    this._Data = new Array();
    this._Elements = new Array();
    this.ResetFlagClick= function () {
        for (var i = 0; i < this.Elements.length; i++) {
            this.Elements[i].FlagClick = false;
        }
    }
    this.ResetElementsHighlight = function () {
        for (var i = 0; i < this.Elements.length; i++) {
            this.Elements[i].Highlight = 0;
            this.Elements[i].UnHighlighted = true;
        }
    }
    this.ResetOption = null
    Object.defineProperty(this, 'Left', {
        get: function () {
            return this._Left;
        },
        set: function (_Value) {
            this._Left = _Value;
            $(this.ObjectHtml).css('left', this.Left);
        }
    });

    Object.defineProperty(this, 'Right', {
        get: function () {
            return this._Rigth;
        },
        set: function (_Value) {
            this._Rigth = _Value;
            $(this.ObjectHtml).css('right', this.Right);
        }
    });

    Object.defineProperty(this, 'Top', {
        get: function () {
            return this._Top;
        },
        set: function (_Value) {
            this._Top = _Value;
            $(this.ObjectHtml).css('top', this.Top);
        }
    });

    Object.defineProperty(this, 'Bottom', {
        get: function () {
            return this._Bottom;
        },
        set: function (_Value) {
            this._Bottom = _Value;
            $(this.ObjectHtml).css('bottom', this.Bottom);
        }
    });

    Object.defineProperty(this, 'Width', {
        get: function () {
            return this._Width;
        },
        set: function (_Value) {
            this._Width = _Value;
            $(this.ObjectHtml.children).css('width', this.Width);
        }
    });

    Object.defineProperty(this, 'Height', {
        get: function () {
            return this._Height;
        },
        set: function (_Value) {
            this._Height = _Value;
            $(this.ObjectHtml).css({ 'height': this.Height });
            if (this._DisplayTitle) {
                $(this.ObjectHtml.children[1]).css({ 'overflow-y': 'auto', 'max-height': parseInt(this.Height) - 60, 'height': parseInt(this.Height) - 60 });
            } else {
                $(this.ObjectHtml.children[1]).css({ 'overflow-y': 'auto', 'max-height': this.Height, 'height': this.Height });
            }
        }
    });

    Object.defineProperty(this, 'AlighLeft', {
        get: function () {
            return this._AlighLeft;
        },
        set: function (_Value) {
            this._AlighLeft = _Value;
            $(this.ObjectHtml).css('padding-left', this.AlighLeft);
        }
    });

    Object.defineProperty(this, 'AlighRight', {
        get: function () {
            return this._AlighRight;
        },
        set: function (_Value) {
            this._AlighRight = _Value;
            $(this.ObjectHtml).css('padding-right', this.AlighRight);
        }
    });

    Object.defineProperty(this, 'AlighTop', {
        get: function () {
            return this._AlighTop;
        },
        set: function (_Value) {
            this._AlighTop = parseInt(_Value);
            if (this.Height == null) {
                this.Height = $(this.ObjectHtml).css('height');
            }
            $(this.ObjectHtml).css('padding-top', this.AlighTop);
            if (this.DisplayTitle) {
                $(this.ObjectHtml.children[1]).css({ 'overflow-y': 'auto', 'max-height': parseInt(this.Height) - (this.AlighBottom + 60 + this.AlighTop), 'height': parseInt(this.Height) - (this.AlighBottom + 60 + this.AlighTop) });
            } else {
                $(this.ObjectHtml.children[1]).css({ 'overflow-y': 'auto', 'max-height': parseInt(this.Height) - (this.AlighBottom + this.AlighTop), 'height': parseInt(this.Height) - (this.AlighBottom + this.AlighTop) });
            }
        }
    });

    Object.defineProperty(this, 'AlighBottom', {
        get: function () {
            return this._AlighBottom;
        },
        set: function (_Value) {
            this._AlighBottom = parseInt(_Value);
            if (this.Height == null) {
                this.Height = $(this.ObjectHtml).css('height');
            }
            $(this.ObjectHtml).css('padding-bottom', this.AlighBottom);
            if (this.DisplayTitle) {
                $(this.ObjectHtml.children[1]).css({ 'overflow-y': 'auto', 'max-height': parseInt(this.Height) - (this.AlighBottom + 60 + this.AlighTop), 'height': parseInt(this.Height) - (this.AlighBottom + 60 + this.AlighTop) });
            } else {
                $(this.ObjectHtml.children[1]).css({ 'overflow-y': 'auto', 'max-height': parseInt(this.Height) - (this.AlighBottom + this.AlighTop), 'height': parseInt(this.Height) - (this.AlighBottom + this.AlighTop) });
            }
        }
    });

    Object.defineProperty(this, 'Title', {
        get: function () {
            return this._Title;
        },
        set: function (_Value) {
            this._Title = _Value;
        }
    });
    Object.defineProperty(this, 'DisplayTitle', {
        get: function () {
            return this._DisplayTitle;
        },
        set: function (_Value) {
            this._DisplayTitle = _Value;
        }
    });
    Object.defineProperty(this, 'Data', {
        get: function () {
            this._Data.splice(0, this._Elements.length);
            for (var i = 0; i < this._Elements.length; i++) {
                this._Data.push(this._Elements[i].Value);
            }
            return this._Data
        },
        set: function (_Value) {

            this._Data = _Value;
        }
    });
    Object.defineProperty(this, 'BoxPlugin', {
        get: function () {
            return this._BoxPlugin;
        },
        set: function (_Value) {
            this._BoxPlugin = _Value;
        }
    });
    Object.defineProperty(this, 'Elements', {
        get: function () {
            return this._Elements;
        },
        set: function (_Value) {
            this._Elements = _Value;
        }
    });
    this.TVclGraphicElementsAdd = function (inElement) {
        this._Elements.push(inElement);
        this._Data.push(inElement._Value);
    }
}

Componet.TGraphicsElements.TVclGraphicElementFunnel = function () {
    var _this = this;
    this._Id = 0;
    this._Label = "";
    this._Value = null;
    this.FlagClick = false;
    this._Color = null;
    this._Highlight = null;
    this._UnHighlighted = false;
    this._liElement = null;
    this._onClick = null;
    Object.defineProperty(this, 'Id', {
        get: function () {
            return this._Id;
        },
        set: function (_Value) {
            this._Id = _Value;
        }
    });
    Object.defineProperty(this, 'Label', {
        get: function () {
            return this._Label;
        },
        set: function (_Value) {
            this._Label = _Value;
        }
    });
    Object.defineProperty(this, 'Value', {
        get: function () {
            return this._Value;
        },
        set: function (_Value) {
            this._Value = _Value;

        }
    });
    Object.defineProperty(this, 'Color', {
        get: function () {
            return this._Color;
        },
        set: function (_Value) {
            this._Color = _Value;
            if (this.liElement != null) {
                if (this.Highlight != null) {
                    var aux = this.Highlight
                    this.UnHighlighted = true;
                    $(this.liElement).css('background-color', this.Color)
                    $(this.liElement.children).css('background-color', this.Color)
                    this.Highlight = aux;
                } else {
                    $(this.liElement).css('background-color', this.Color)
                    $(this.liElement.children).css('background-color', this.Color)
                }
            }
        }
    });
    Object.defineProperty(this, 'Highlight', {
        get: function () {
            return this._Highlight;
        },
        set: function (_Value) {
            this._Highlight = _Value;
            if (this.liElement != null) {
                this.RefreshElementFunnel(_this, this.Highlight);
            }
        }
    });
    Object.defineProperty(this, 'UnHighlighted', {
        get: function () {
            return this._UnHighlighted;
        },
        set: function (_Value) {
            this._UnHighlighted = _Value;
            if (this.liElement != null) {
                if (this.UnHighlighted) {
                    this.Highlight = this.Value;
                    this.RefreshElementFunnel(_this, this.Value);
                }
                else {
                    this.RefreshElementFunnel(_this, this.Highlight);
                }
            }
        }
    });
    Object.defineProperty(this, 'liElement', {
        get: function () {
            return this._liElement;
        },
        set: function (_Value) {
            this._liElement = _Value;
            if (this._liElement != null) this._liElement.onclick = this._onClick;
        }
    });
    Object.defineProperty(this, 'onClick', {
        get: function () {
            return this._onClick
        },
        set: function (_Value) {
            this._onClick = function () {
                _Value(_this);
            };
            if (this._liElement != null) this._liElement.onclick = this._onClick;
        }
    })
}

Componet.TGraphicsPBI.TVclFunnel.prototype.CreateGraphic = function () {
    var _this = this.TParent();
    try {
        var TitleFunnel = new Uh1(_this.ObjectHtml, '', _this.Title);
        $(TitleFunnel).addClass('title-funnel');

        var AllContainer = new TVclStackPanel(_this.ObjectHtml, _this.Id, 2, [[5, 7], [4, 8], [4, 8], [4, 8], [4, 8]]);
        var Container_1 = AllContainer.Column[0].This;
        $(Container_1).addClass('container-text');
        var Container_2 = AllContainer.Column[1].This;

        var _max = Math.max.apply(null, _this.Data);

        var EmptyContent = new Udiv(Container_1, '');

        var InitLineContent = new Udiv(Container_2, '');
        InitLineContent.style.width = ((_this.Data[0] * 100) / _max) + "%";
        $(InitLineContent).addClass('line-content');

        var InitInterval = new Uspan(InitLineContent, '', '100%');
        $(InitInterval).addClass('init-interval');

        var InitImage = new TVclImagen(InitLineContent, '');
        InitImage.Src = 'Componet/GraphicsPBI/Funnel/img/Bar.png';
        $(InitImage.This).addClass('init-img');

        for (var i = 0; i < _this.Elements.length; i++) {
            var _x = (_this.Elements[i].Value * 100) / _max;
            var ContentText = new Udiv(Container_1, '');
            ContentText.innerText = _this.Elements[i].Label;
            $(ContentText).addClass('text-content')

            var ContentBar = new Udiv(Container_2, '');
            $(ContentBar).addClass('bar-content');
            ContentBar.style.width = _x + "%";

            var BarElement = new Udiv(ContentBar, '');
            $(BarElement).addClass('bar-element');
            BarElement.innerText = _this.Elements[i].Value;
            BarElement.style.backgroundColor = _this.Elements[i].Color;

            var _col = $(BarElement).css('background-color');
            var _newcol = _col.slice(0, -1);
            $(ContentBar).css('background-color', _newcol + ', 0.4)');

            if (_this.Elements[i].Highlight != null) {
                if (_this.Elements[i].Highlight == 0) {
                    BarElement.style.visibility = 'hidden'; //hidden - visible
                    $(BarElement).css('background-color', _newcol + ', 0)');
                }
                else {
                    BarElement.style.width = ((_this.Elements[i].Highlight * 100) / _this.Elements[i].Value) + "%";
                    BarElement.innerText = _this.Elements[i].Highlight;
                }
            }
            _this._Elements[i].liElement = ContentBar;

        }

        var acu = 0;
        var _y = parseFloat(((_this.Data[_this.Data.length - 1] * 100) / _this.Data[0]).toFixed(1));
        if (_y.toFixed() != _y) acu = _y;
        else acu = _y.toFixed();

        var EmptyContentF = new Udiv(Container_1, '');

        var FinalLineContent = new Udiv(Container_2, '');
        $(FinalLineContent).addClass("line-content");
        FinalLineContent.style.width = ((_this.Data[_this.Data.length - 1] * 100) / _max) + "%";

        var FinalImage = new TVclImagen(FinalLineContent, '');
        FinalImage.Src = 'Componet/GraphicsPBI/Funnel/img/Bar.png';
        $(FinalImage.This).addClass('final-img');

        var FinalInterval = new Uspan(FinalLineContent, '', acu + "%");
        $(FinalInterval).addClass('final-interval');

        if (!_this.DisplayTitle) {
            TitleFunnel.style.display = 'none';
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Funnel.js Componet.TGraphicsPBI.TVclFunnel.prototype.CreateGraphic", e);
    }
}

Componet.TGraphicsPBI.TVclFunnel.prototype.RefreshGraphic = function () {
    var _this = this.TParent();
    try {
        $(_this.ObjectHtml).html("");
        this.CreateGraphic();
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Funnel.js Componet.TGraphicsPBI.TVclFunnel.prototype.RefreshGraphic", e);
    }
}

Componet.TGraphicsPBI.TVclFunnel.prototype.DestroyGraphic = function () {
    var _this = this.TParent();
    $(_this.ObjectHtml).html("");
}

Componet.TGraphicsElements.TVclGraphicElementFunnel.prototype.RefreshElementFunnel = function (element, value) {
    try {
        var _col = $(element.liElement).css('background-color');
        var _newcol = _col.slice(0, -1);
        $(element.liElement).css('background-color', _newcol + ', 0.4)');
        if (value == 0) {
            element.liElement.children[0].style.visibility = 'hidden';
            $(element.liElement.children[0]).css('background-color', _newcol + ', 0)');
        }
        else {
            element.liElement.children[0].style.visibility = 'visible';
            element.liElement.children[0].style.width = ((value * 100) / element.Value) + "%";
            element.liElement.children[0].innerText = value;
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Funnel.js Componet.TGraphicsElements.TVclGraphicElementFunnel.prototype.RefreshElementFunnel", e);
    }
}

