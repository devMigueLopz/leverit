Componet.TGraphicsPBI.TVclTemplate = function (ObjectBody, Id, DefineDivCol, DefineClassCol) {
    var TParent = function () {
        return this;
    }.bind(this);
    this.CountColumn = DefineDivCol;
    this._Element = null;
    this.Row = null;
    this.Column = [];
    Object.defineProperty(this, 'Element', {
        get: function () {
            return this._Element;
        },
    });
    try {
        var TemplateGraphics = Udiv(ObjectBody, Id + "_TemplateGraphics");
        BootStrapContainer(TemplateGraphics, TBootStrapContainerType.fluid);
        var TemplateGraphicsRowColumn = new TVclStackPanel(TemplateGraphics, "", 1, [[12], [12], [12], [12], [12]]);
        var _ArrayColumn = new Array(1);
        _ArrayColumn[0] = new Array(this.CountColumn);
        var TemplateGraphicsRowColumnGraphics = VclDivitions(TemplateGraphicsRowColumn.Column[0].This, Id + "_TemplateGraphicsRowGraphics", _ArrayColumn);
        BootStrapCreateRow(TemplateGraphicsRowColumnGraphics[0].This);
        BootStrapCreateColumns(TemplateGraphicsRowColumnGraphics[0].Child, DefineClassCol);
        this.Row = new TVclDiv(TemplateGraphicsRowColumnGraphics[0].This);
        for (var i = 0; i < TemplateGraphicsRowColumnGraphics[0].Child.length; i++) {
            UnColumnObj = new TVclDiv(TemplateGraphicsRowColumnGraphics[0].Child[i].This);
            var Element = new Componet.TGraphicsElements.TVclTemplateElement(UnColumnObj);
            this.Column.push(Element);
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Template.js Componet.TGraphicsPBI.TVclTemplate ", e);
    }
}
Componet.TGraphicsElements.TVclTemplateElement = function (UnColumnObj) {
    var TParent = function () {
        return this;
    }.bind(this);
    try {
        this.ColumnInit = UnColumnObj;
        this.Child = [];
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Template.js Componet.TGraphicsElements.TVclTemplateElement", e);
    }
}
Componet.TGraphicsElements.TVclTemplateElement.prototype.AddRowColumn = function (Id, DefineDivCol, DefineClassCol) {
    var ColumnChild = [];
    var _ArrayColumn = new Array(1);
    try {
        var _CountColumn = DefineDivCol;
        _ArrayColumn[0] = new Array(_CountColumn);
        var TemplateGraphicsRowColumnGraphics = VclDivitions(this.ColumnInit.This, Id + "_TemplateGraphicsRowGraphics", _ArrayColumn);
        BootStrapCreateRow(TemplateGraphicsRowColumnGraphics[0].This);
        BootStrapCreateColumns(TemplateGraphicsRowColumnGraphics[0].Child, DefineClassCol);
        var _Row = new TVclDiv(TemplateGraphicsRowColumnGraphics[0].This);
        for (var i = 0; i < TemplateGraphicsRowColumnGraphics[0].Child.length; i++) {
            UnColumnObj = new TVclDiv(TemplateGraphicsRowColumnGraphics[0].Child[i].This);
            ColumnChild.push(UnColumnObj);
        }
        this.Child.push({ Row: _Row, ChildsColumn: ColumnChild, CountColumn: _CountColumn });
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Template.js Componet.TGraphicsElements.TVclTemplateElement.prototype.AddRowColumn", e);
    }
}
