﻿Componet.TGraphicsPBI = function (inObjectHtml, Id, inCallBack) {
	this.TParent = function () {
		return this;
	}.bind(this);
	// var _this = this.TParent();
	this._ObjectHtml = inObjectHtml;
}

Componet.TGraphicsPBI.prototype.Load = function(){
	var _this = this.TParent();

		/*var ArregloElementos_2 = [
		{ name: "AGarcia", value: 7, color: "rgb(1,184,170)" },
		{ name: "ARincon", value: 8, color: "rgb(254,171,133)" },
		{ name: "ARodriguez", value: 19, color: "rgb(104,159,176)" },
		{ name: "DSalazar", value: 15, color: "rgb(190,74,71)" },
		{ name: "NRojas", value: 3, color: "rgb(184,135,173)" },
		{ name: "ARincon", value: 8, color: "rgb(254,171,133)" },
		{ name: "ARodriguez", value: 19, color: "rgb(104,159,176)" },
		{ name: "DSalazar", value: 15, color: "rgb(190,74,71)" },
		{ name: "NRojas", value: 3, color: "rgb(184,135,173)" },
		{ name: "ARincon", value: 8, color: "rgb(254,171,133)" },
		{ name: "ARodriguez", value: 19, color: "rgb(104,159,176)" },
		{ name: "ARodriguez", value: 19, color: "rgb(104,159,176)" },
		{ name: "DSalazar", value: 15, color: "rgb(190,74,71)" },
		{ name: "NRojas", value: 3, color: "rgb(184,135,173)" },
		{ name: "ARincon", value: 8, color: "rgb(254,171,133)" },
		{ name: "ARodriguez", value: 19, color: "rgb(104,159,176)" },
		{ name: "DSalazar", value: 15, color: "rgb(190,74,71)" },
		{ name: "NRojas", value: 3, color: "rgb(184,135,173)" }];
		GraficoEmbudo = new Componet.TGraphicsPBI.TFunnel(_this._ObjectHtml, '' + '_Funnel1');
		for (var i = 0 ; i < ArregloElementos_2.length ; i++) {
			var Elemento2 = new Componet.TGraphicsElements.TGraphicElementFunnel();
			Elemento2.Label = ArregloElementos_2[i]["name"];
			Elemento2.Value = ArregloElementos_2[i]["value"];
			Elemento2.Color = ArregloElementos_2[i]["color"];
			Elemento2.Id = "Element2_" + i;
			Elemento2.onClick = function (inElemento) {
				//alert(inElemento.Label + ': ' + inElemento.Value);
				if(inElemento.Highlight != null){
				alert(inElemento.Label + ': ' + inElemento.Value + '\nResaltado: ' + inElemento.Highlight);
				}
				else{
					alert(inElemento.Label + ': ' + inElemento.Value);
				}
			};
			GraficoEmbudo.TVclGraphicElementsAdd(Elemento2);
		}

		GraficoEmbudo.Title = "Asesores";
		GraficoEmbudo.DisplayTitle = true;
		GraficoEmbudo.CreateGraphic();

		// GraficoEmbudo.Height = 500;
		// GraficoEmbudo.Width = 400;
		// GraficoEmbudo.Left = 450;
		// GraficoEmbudo.Bottom = 450;
		// GraficoEmbudo.Right = 50;
		// GraficoEmbudo.Top = 150;

		// GraficoEmbudo.AlighLeft = 50;
		// GraficoEmbudo.AlighRight = 50;
		// GraficoEmbudo.AlighTop = 100;
		// GraficoEmbudo.AlighBottom = 100;
	
		
		// GraficoEmbudo.BoxPlugin.Column[0].Width  = "150px";
		// GraficoEmbudo.BoxPlugin.Column[0].Height = "150px";
		// GraficoEmbudo.BoxPlugin.Column[0].Left   = "150px";
		// GraficoEmbudo.BoxPlugin.Column[0].Top    = "150px";
		// GraficoEmbudo.BoxPlugin.Column[1].Width  = "150px";
		// GraficoEmbudo.BoxPlugin.Column[1].Height = "150px";
		// GraficoEmbudo.BoxPlugin.Column[1].Left   = "150px";
		// GraficoEmbudo.BoxPlugin.Column[1].Top    = "150px";

		GraficoEmbudo.Elements[0].Color = "red";
		
		GraficoEmbudo.Elements[0].Highlight = 3;
		GraficoEmbudo.Elements[1].Highlight = 4;
		GraficoEmbudo.Elements[2].Highlight = 10;
		GraficoEmbudo.Elements[3].Highlight = 0;*/

		GraficoTable = new Componet.TGraphicsPBI.TTable(_this._ObjectHtml, '' + "_Table1")
        var ArregloElementos_5 = [
            { name: "Empresa1", value: 10, color: "blue", chicamaso: "Chicamaso 1", algo: "mas", otro: "mas de mas" },
            { name: "Empresa2", value: 30, color: "orange", chicamaso: "Chicamaso 2 mas puntos suspensivos", algo: "mas", otro: "mas de mas" },
            { name: "Empresa3", value: 60, color: "red", chicamaso: "Chicamaso 3", algo: "mas", otro: "mas de mas" },
            { name: "Empresa4", value: 40, color: "black", chicamaso: "Chicamaso 3", algo: "mas", otro: "mas de mas" },
            { name: "Empresa5", value: 34, color: "pink", chicamaso: "Chicamaso 4", algo: "mas", otro: "mas de mas" },
            { name: "Empresa6", value: 54, color: "red", chicamaso: "Chicamaso 5", algo: "mas", otro: "mas de mas" },
            { name: "Empresa7", value: 54, color: "blue", chicamaso: "Chicamaso 6", algo: "mas", otro: "mas de mas" },
            { name: "Empresa8", value: 64, color: "green", chicamaso: "Chicamaso 7", algo: "mas", otro: "mas de mas" },
            { name: "Empresa9", value: 74, color: "white", chicamaso: "Chicamaso 8", algo: "mas", otro: "mas de mas" },
            { name: "Empresa10", value: 24, color: "yellow", chicamaso: "Chicamaso 9", algo: "mas", otro: "mas de mas" },
            { name: "Empresa11", value: 34, color: "black", chicamaso: "Chicamaso 10", algo: "mas", otro: "mas de mas" },
            { name: "Empresa12", value: 44, color: "yellow", chicamaso: "Chicamaso 11", algo: "mas", otro: "mas de mas" },
            { name: "Empresa13", value: 54, color: "green", chicamaso: "Chicamaso 12", algo: "mas", otro: "mas de mas" },
            { name: "Empresa14", value: 37, color: "yellow", chicamaso: "Chicamaso 13", algo: "mas", otro: "mas de mas" },
            { name: "Empresa15", value: 38, color: "black", chicamaso: "Chicamaso 14", algo: "mas", otro: "mas de mas" },
            { name: "Empresa16", value: 39, color: "white", chicamaso: "Chicamaso 15", algo: "mas", otro: "mas de mas" },
            { name: "Empresa17", value: 41, color: "yellow", chicamaso: "Chicamaso 16", algo: "mas", otro: "mas de mas" },
            { name: "Empresa18", value: 43, color: "green", chicamaso: "Chicamaso 17", algo: "mas", otro: "mas de mas" },
            { name: "Empresa19", value: 34, color: "white", chicamaso: "Chicamaso 18", algo: "mas", otro: "mas de mas" },
            { name: "Empresa20", value: 35, color: "yellow", chicamaso: "Chicamaso 19", algo: "mas", otro: "mas de mas" },
            { name: "Empresa21", value: 39, color: "black", chicamaso: "Chicamaso 20", algo: "mas", otro: "mas de mas" },
            { name: "Empresa22", value: 60, color: "white", chicamaso: "Chicamaso 21", algo: "mas", otro: "mas de mas" },
            { name: "Empresa23", value: 65, color: "green", chicamaso: "Chicamaso 22", algo: "mas", otro: "mas de mas" }];
        for (var i = 0 ; i < ArregloElementos_5.length ; i++) {
            var Elemento5 = new Componet.TGraphicsElements.TGraphicElementTable();
            Elemento5.Id = "Element4_" + i;
            Elemento5.Array = ArregloElementos_5[i];
            Elemento5.onClick = function (inElemento) {
                alert(inElemento.Array["name"] + ': ' + inElemento.Array["value"]);
            };
            GraficoTable.TVclGraphicElementsAdd(Elemento5);
        }
        GraficoTable.Elements[0].Highlight = false;
        
        GraficoTable.Title = "Tabla de Clientes";
        GraficoTable.DisplayTitle = true;
        GraficoTable.CreateGraphic();
        // GraficoTable.DestroyGraphic();
        
        // GraficoTable.Height = '400px';
        // GraficoTable.Width = '200px';
        // GraficoTable.Left = '50px';
        // GraficoTable.Bottom = '150px';
        // GraficoTable.Right = '50px';
        // GraficoTable.Top = '150px';
        
        // GraficoTable.AlighLeft = 50;
        // GraficoTable.AlighRight = 50;
        // GraficoTable.AlighTop = 50;
        // GraficoTable.AlighBottom = 50;


        // GraficoTable.BoxPlugin.Column[0].Width  = "150px";
        // GraficoTable.BoxPlugin.Column[0].Height = "150px";
        // GraficoTable.BoxPlugin.Column[0].Left   = "150px";
        // GraficoTable.BoxPlugin.Column[0].Top    = "150px";
}