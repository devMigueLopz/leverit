Componet.TGraphicsPBI.TVclTreeMap = function (ObjectHtml, Id) {
    this.TParent = function () {
        return this;
    }.bind(this);
    this.ObjectHtml = ObjectHtml;
    this.ID = Id;
    this._BoxPlugin = null;
    this._HeightPlugin = "400px";
    this._Elements = new Array();
    this.BoxGraphics = null;
    this.Plugin = null;
    this.TreeMapElementsAdd = function (inElements) {
        var len = this._Elements.length;
        inElements._Index = len;
        this._Elements.push(inElements);
    };
    this.ResetElementsHighlight = function () {
        for (var i = 0; i < this.Elements.length; i++) {
            this.Elements[i].ValueHighlight = 0;
            this.Elements[i].DisplayHighlight = false;
        }
    }
    this.ResetOption = null;
    this.ResetFlagClick = function () {
        for (var i = 0; i < this.Elements.length; i++) {
            this.Elements[i].FlagClick = false;
        }
    }
    Object.defineProperty(this, 'Elements', {
        get: function () {
            return this._Elements;
        },
        set: function (_Value) {
            this._Elements = _Value;
        }
    });
    Object.defineProperty(this, 'BoxPlugin', {
        get: function () {
            return this._BoxPlugin;
        },
        set: function (_Value) {
            this._BoxPlugin = _Value;
        }
    });
    Object.defineProperty(this, 'HeightPlugin', {
        get: function () {
            return this._HeightPlugin;
        },
        set: function (_Value) {
            this._HeightPlugin = _Value;
            if (this.BoxGraphics != null) {
                if (this.BoxGraphics.childNodes.length > 0) {
                    this.BoxGraphics.childNodes[0].style.height = this._HeightPlugin;
                }
            }
        }
    });
}
Componet.TGraphicsElements.TVclTreeMapElements = function () {
    this.TParent = function () {
        return this;
    }.bind(this);
    this._Div = null;
    this.DivHighlight = null;
    this.DeleteTreeMap = null;
    this.CreateTreeMap = null;
    this.BoxGraphics = null;
    this.CalculateHeightPixel = null;
    this._Color = null;
    this._Name = null;
    this._Index = null;
    this._Value = null;
    this._Id = null;
    this._DisplayHighlight = false;
    this.FlagClick = false;
    this._ValueHighlight = 0;
    this._ColorOpacity = null;
    this._onClick = null;
    var _this = this;
    Object.defineProperty(this, 'Color', {
        get: function () {
            return this._Color;
        },
        set: function (_Value) {
            this._Color = _Value;
            if (this._Div != null) {
                this._Div.style.background = this._Color;
            }
        }
    });
    Object.defineProperty(this, 'Name', {
        get: function () {
            return this._Name;
        },
        set: function (_Value) {
            this._Name = _Value;
            if (this._Div != null) {
                this._Div.innerText = _Value;
                this._Div.setAttribute("data-original-title", this._Name + " : " + this._Value);
            }
        }
    });
    Object.defineProperty(this, 'Id', {
        get: function () {
            return this._Id;
        },
        set: function (_Value) {
            this._Id = _Value;
        }
    });
    Object.defineProperty(this, 'Value', {
        get: function () {
            return this._Value;
        },
        set: function (_Value) {
            this._Value = parseInt(_Value);
            if (this.BoxGraphics != null) {
                if (this.DeleteTreeMap != null) {
                    this.DeleteTreeMap();
                    if (this.CreateTreeMap != null) {
                        this.CreateTreeMap();
                        if (this.CalculateHeightPixel != null) {
                            this.CalculateHeightPixel();
                        }
                    }
                }
            }
        }
    });
    Object.defineProperty(this, 'Index', {
        get: function () {
            return this._Index;
        },
        set: function (_Value) {
            this._Index = _Value;
        }
    });
    Object.defineProperty(this, 'DisplayHighlight', {
        get: function () {
            return this._DisplayHighlight;
        },
        set: function (_Value) {
            this._DisplayHighlight = _Value;
            if (this._DisplayHighlight) {
                if (this._ValueHighlight <= 0) {
                    this._ValueHighlight = 0;
                    if (this.DivHighlight != null) {
                        this.DivHighlight.style.height = "0%";
                    }
                }
                else if (this._ValueHighlight > 0 && this._ValueHighlight <= this._Value) {
                    var pixel100Percent = this._Div.offsetHeight;
                    var NewPercentValue = ((this._ValueHighlight * 100) / this._Value)
                    var NewPixelHeightHighlight = ((pixel100Percent * NewPercentValue) / 100)
                    NewPixelHeightHighlight = pixel100Percent - NewPixelHeightHighlight;
                    if (this.DivHighlight != null) {
                        if (NewPixelHeightHighlight == 0) {
                            this.DivHighlight.style.height = this._Div.style.height;
                        }
                        else {
                            this.DivHighlight.style.height = NewPixelHeightHighlight + "px";
                        }
                    }
                }
                else {
                    this._ValueHighlight = this._Value;
                    if (this.DivHighlight != null) {
                        this.DivHighlight.style.height = this._Div.style.height;
                    }
                }
            }
            else {
                this.DivHighlight.style.height = "0%";
            }
        }
    });
    Object.defineProperty(this, 'ValueHighlight', {
        get: function () {
            return this._ValueHighlight;
        },
        set: function (_Value) {
            this._ValueHighlight = parseInt(_Value);
            if (this._DisplayHighlight) {
                if (this._ValueHighlight <= 0) {
                    this._ValueHighlight = 0;
                    if (this.DivHighlight != null) {
                        this.DivHighlight.style.height = "0%";
                    }
                }
                else if (this._ValueHighlight > 0 && this._ValueHighlight <= this._Value) {
                    var pixel100Percent = this._Div.offsetHeight;
                    var NewPercentValue = ((this._ValueHighlight * 100) / this._Value)
                    var NewPixelHeightHighlight = ((pixel100Percent * NewPercentValue) / 100)
                    NewPixelHeightHighlight = pixel100Percent - NewPixelHeightHighlight;
                    if (this.DivHighlight != null) {
                        if (NewPixelHeightHighlight == 0) {
                            this.DivHighlight.style.height = this._Div.style.height;
                        }
                        else {
                            this.DivHighlight.style.height = NewPixelHeightHighlight + "px";
                        }
                    }
                }
                else {
                    this._ValueHighlight = this._Value;
                    if (this.DivHighlight != null) {
                        this.DivHighlight.style.height = this._Div.style.height;
                    }
                }
            }
        }
    });
    Object.defineProperty(this, 'Div', {
        get: function () {
            return this._Div;
        },
        set: function (_Value) {
            this._Div = _Value;
            if (this._Div != null) this._Div.onclick = this._onClick;
        }
    });
    Object.defineProperty(this, 'onClick', {
        get: function () {
            return this._onClick
        },
        set: function (_Value) {
            this._onClick = function () {
                var ObjParent = _this.TParent();
                var _Array = []
                for (var i = 0; i < ObjParent.Plugin[0].length; i++) {
                    if (i != 0) {
                        _Array.push(ObjParent.Plugin[0][i])
                    }
                }
                for (var i = 0; i < _Array.length; i++) {
                    ObjParent._Elements[i].DisplayHighlight = false;
                    if (_this._Index != ObjParent._Elements[i]._Index) {
                        ObjParent._Elements[i]._Div.style.opacity = "0.6";

                    }
                    else {
                        ObjParent._Elements[i]._Div.style.opacity = "1";
                    }
                }
                _Value(_this);
            };
            if (this._Div != null) this._Div.onclick = this._onClick;
        }
    })
}
Componet.TGraphicsPBI.TVclTreeMap.prototype.CreateGraphics = function () {
    var _this = this.TParent();
    try {
        var boxTreeMap = new TVclStackPanel(_this.ObjectHtml, _this.ID + "_Box", 1, [[12], [12], [12], [12], [12]]);
        _this._BoxPlugin = boxTreeMap;
        _this.BoxGraphics = boxTreeMap.Column[0].This;
        boxTreeMap.Column[0].Height = _this._HeightPlugin;
        var divTreemap = Udiv(_this.BoxGraphics, _this.BoxGraphics.id + "_TreeMapDiv");
        divTreemap.setAttribute("class", "treemap");
        var treemap = d3.layout.treemap().size([100, 100]).sticky(true).value(function (d) { return d.value; });
        var div = d3.select("#" + divTreemap.id);
        var data = _this.CreateDataValue();
        _this.Plugin =
            div.datum(data).selectAll(".node")
                .data(treemap.nodes)
                .enter().append("div")
                .attr("class", "node")
                .attr("data-toggle", "tooltip")
                .attr("data-placement", "left")
                .attr("title", function (d) {
                    return d.name + " : " + d.value;
                })
                .call(function () {
                    this.style("left", function (d) { return d.x + "%"; })
                    this.style("top", function (d) { return d.y + "%"; })
                    this.style("width", function (d) { return d.dx + "%"; })
                    this.style("height", function (d) { return d.dy + "%"; });
                })
                .style("background", function getColor(d) {
                    return d.color;
                })
                .text(function getLabel(d) {
                    return d.name;
                });
        $('[data-toggle="tooltip"]').tooltip();
        var _Array = [];
        _Array = _this.Plugin[0];
        _Array.shift();
        for (var i = 0; i < _Array.length; i++) {
            _this._Elements[i].Div = _Array[i];
            var ElementHighlight = Udiv(divTreemap, "");
            ElementHighlight.setAttribute("class", "nodeHighlight");
            ElementHighlight.setAttribute("data-placement", _this._Elements[i]._Div.getAttribute("data-placement"));
            ElementHighlight.style.left = _this._Elements[i]._Div.style.left;
            ElementHighlight.style.top = _this._Elements[i]._Div.style.top;
            ElementHighlight.style.width = _this._Elements[i]._Div.style.width;
            ElementHighlight.style.height = 0;
            ElementHighlight.style.background = "rgba(255, 255, 255, 0.6)";
            divTreemap.appendChild(ElementHighlight);
            _this._Elements[i].DivHighlight = ElementHighlight;
            _this._Elements[i].DeleteTreeMap = _this.ClearGraphics;
            _this._Elements[i].CreateTreeMap = _this.CreateGraphics;
            _this._Elements[i].BoxGraphics = _this.BoxGraphics;
            _this._Elements[i].CalculateHeightPixel = _this.CalculateHeightPixel;
            _this._Elements[i].TParent = this.TParent;
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("TreeMap.js Componet.TGraphicsPBI.TVclTreeMap.prototype.CreateGraphics ", e);
    }
}
Componet.TGraphicsPBI.TVclTreeMap.prototype.ClearGraphics = function () {
    try {
        var _this = this.TParent();
        if (_this.BoxGraphics != null) {
            var element = document.getElementById(_this.BoxGraphics.id).parentNode.parentNode;
            while (element.firstChild) {
                element.removeChild(element.firstChild);
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("TreeMap.js Componet.TGraphicsPBI.TVclTreeMap.prototype.ClearGraphics", e);
    }
}
Componet.TGraphicsPBI.TVclTreeMap.prototype.CreateDataValue = function () {
    var _this = this.TParent();
    var _Obj = { name: "", children: [] };
    try {
        for (var i = 0; i < _this._Elements.length; i++) {
            _Obj.children.push({ name: _this._Elements[i]._Name, value: _this._Elements[i]._Value, color: _this._Elements[i]._Color });
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("TreeMap.js Componet.TGraphicsPBI.TVclTreeMap.prototype.CreateDataValue", e);
    }
    return _Obj;
}
Componet.TGraphicsPBI.TVclTreeMap.prototype.CalculateHeightPixel = function () {
    var _this = this.TParent();
    try {
        for (var i = 0; i < _this._Elements.length; i++) {
            if (_this._Elements[i]._DisplayHighlight) {
                if (_this._Elements[i]._ValueHighlight <= 0) {
                    if (_this._Elements[i].DivHighlight != null) {
                        _this._Elements[i].DivHighlight.style.height = "0%";
                    }
                }
                else if (_this._Elements[i]._ValueHighlight > 0 && _this._Elements[i]._ValueHighlight <= _this._Elements[i]._Value) {
                    if (_this._Elements[i]._Div != null) {
                        var pixel100Percent = _this._Elements[i]._Div.offsetHeight;
                        var NewPercentValue = ((_this._Elements[i]._ValueHighlight * 100) / _this._Elements[i]._Value)
                        var NewPixelHeightHighlight = ((pixel100Percent * NewPercentValue) / 100)
                        NewPixelHeightHighlight = pixel100Percent - NewPixelHeightHighlight;
                        if (_this._Elements[i].DivHighlight != null) {
                            if (NewPixelHeightHighlight == 0) {
                                _this._Elements[i].DivHighlight.style.height = _this._Elements[i]._Div.style.height;
                            }
                            else {
                                _this._Elements[i].DivHighlight.style.height = NewPixelHeightHighlight + "px";
                            }
                        }
                    }
                }
                else {
                    if (_this._Elements[i].DivHighlight != null) {
                        _this._Elements[i].DivHighlight.style.height = _this._Elements[i]._Div.style.height;
                    }
                }
            }
            else {
                if (_this._Elements[i].DivHighlight != null) {
                    _this._Elements[i].DivHighlight.style.height = "0%";
                }
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("TreeMap.js Componet.TGraphicsPBI.TVclTreeMap.prototype.CalculateHeightPixel", e);
    }
}