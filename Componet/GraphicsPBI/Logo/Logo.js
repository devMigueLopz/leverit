Componet.TGraphicsPBI.TVclLogo = function(inObjectHtml, Id){

	this.TParent = function () {
		return this;
	}.bind(this);
	var _this = this.TParent();
	
	this.ObjectHtml = inObjectHtml;
	this.Id = "GraphicLogo_"+Id;
	this._Height = '125px';
	this._Width = '100%';
	this._Left = null;
	this._Right = null;
	this._Top = null;
	this._Bottom = null;
	this._AlighLeft = null;
	this._AlighRight = null;
	this._AlighTop = null;
	this._AlighBottom = null;
	this._ElementDOM = null;
	this._Url = 'https://c.ymcdn.com/sites/www.aceds.org/resource/resmgr/images/affiliate_folder/Compliance_DS_rgb.jpg'; //'img/img_tab/contentImage/computer.png'; 
	this._onClick = null;

	Object.defineProperty(this, 'Url', {
		get: function(){
			return this._Url;
		},set: function(_Value){
			this._Url = _Value;
			if(this._ElementDOM != null) this._ElementDOM.style.backgroundImage = "url("+this.Url+")";
		}
	});
	Object.defineProperty(this, 'Height', {
		get: function(){
			return this._Height;
		}, 
		set: function(_Value){
			this._Height = _Value;
			if(this._ElementDOM != null) this._ElementDOM.style.height = this.Height;
		}
	});
	Object.defineProperty(this, 'Width', {
		get: function(){
			return this._Width;
		}, 
		set: function(_Value){
			this._Width = _Value;
			if(this._ElementDOM != null) this._ElementDOM.style.width = this.Width;
		}
	});
	Object.defineProperty(this, 'Left', {
		get: function(){
			return this._Left;
		}, 
		set: function(_Value){
			this._Left = _Value;
			if(this._ElementDOM != null) this._ElementDOM.style.left = this.Left;
		}
	});
	Object.defineProperty(this, 'Right', {
		get: function(){
			return this._Right;
		}, 
		set: function(_Value){
			this._Right = _Value;
			if(this._ElementDOM != null) this._ElementDOM.style.right = this.Right;
		}
	});
	Object.defineProperty(this, 'Top', {
		get: function(){
			return this._Top;
		}, 
		set: function(_Value){
			this._Top = _Value;
			if(this._ElementDOM != null) this._ElementDOM.style.top = this.Top;
		}
	});
	Object.defineProperty(this, 'Bottom', {
		get: function(){
			return this._Bottom;
		}, 
		set: function(_Value){
			this._Bottom = _Value;
			if(this._ElementDOM != null) this._ElementDOM.style.bottom = this.Bottom;
		}
	});
	Object.defineProperty(this, 'AlighLeft', {
		get: function(){
			return this._AlighLeft;
		}, 
		set: function(_Value){
			this._AlighLeft = _Value;
			if(this._ElementDOM != null) this._ElementDOM.parentNode.style.paddingLeft = this.AlighLeft;
		}
	});
	Object.defineProperty(this, 'AlighRight', {
		get: function(){
			return this._AlighRight;
		}, 
		set: function(_Value){
			this._AlighRight = _Value;
			if(this._ElementDOM != null) this._ElementDOM.parentNode.style.paddingRight = this.AlighRight;
		}
	});
	Object.defineProperty(this, 'AlighTop', {
		get: function(){
			return this._AlighTop;
		}, 
		set: function(_Value){
			this._AlighTop = _Value;
			if(this._ElementDOM != null){
				this._ElementDOM.parentNode.style.paddingTop = this.AlighTop;
				if(this.AlighBottom != null){
					this._ElementDOM.style.height = parseInt(this.Height)-(parseInt(this.AlighBottom)+parseInt(this.AlighTop))+"px";
				}else{
					this._ElementDOM.style.height = (parseInt(this.Height)-parseInt(this.AlighTop))+"px";
				}
			}
		}
	});
	Object.defineProperty(this, 'AlighBottom', {
		get: function(){
			return this._AlighBottom;
		}, 
		set: function(_Value){
			this._AlighBottom = _Value;
			if(this._ElementDOM != null){
				this._ElementDOM.parentNode.style.paddingBottom = this.AlighBottom;
				if(this.AlighTop != null){
					this._ElementDOM.style.height = parseInt(this.Height)-(parseInt(this.AlighBottom)+parseInt(this.AlighTop))+"px";
				}else{
					this._ElementDOM.style.height = (parseInt(this.Height)-parseInt(this.AlighBottom))+"px";
				}
			}
		}
	});

	Object.defineProperty(this, 'onClick', {
		get: function () {
			return this._onClick;
		},
		set: function (_Value) {
			this._onClick = function () {
				_Value(_this);
			};
			if (this._ElementDOM != null) this._ElementDOM.onclick = this._onClick;
		}
	})
}

Componet.TGraphicsPBI.TVclLogo.prototype.CreateGraphics = function () {
    var _this = this.TParent();
    try {
        var AllContainer = new TVclStackPanel(_this.ObjectHtml, _this.Id, 1, [[12], [12], [12], [12], [12]]);
        var Container_1 = AllContainer.Column[0].This;

        Container_1.style.backgroundImage = "url(" + _this.Url + ")";
        Container_1.style.backgroundSize = "100% 100%";

        Container_1.style.height = _this.Height;
        Container_1.style.width = _this.Width;
        Container_1.style.right = _this.Right;
        Container_1.style.left = _this.Left;
        Container_1.style.top = _this.Top;
        Container_1.style.bottom = _this.Bottom;
        AllContainer.Row.This.style.paddingLeft = _this.AlighLeft;
        AllContainer.Row.This.style.paddingRight = _this.AlighRight;
        AllContainer.Row.This.style.paddingTop = _this.AlighTop;
        AllContainer.Row.This.style.paddingBottom = _this.AlighBottom;
        if (this.AlighTop != null && this.AlighBottom != null) {
            Container_1.style.height = parseInt(this.Height) - (parseInt(this.AlighBottom) + parseInt(this.AlighTop)) + "px";
        } else if (this.AlighTop != null && this.AlighBottom == null) {
            Container_1.style.height = (parseInt(this.Height) - parseInt(this.AlighTop)) + "px";
        } else if (this.AlighBottom != null && this.AlighTop == null) {
            Container_1.style.height = (parseInt(this.Height) - parseInt(this.AlighBottom)) + "px";
        }

        _this._ElementDOM = Container_1;
        _this._ElementDOM.onclick = _this.onClick;
    }
    catch (e) {
        SysCfg.Log.Methods.WriteLog("Logo.js Componet.TGraphicsPBI.TVclLogo.prototype.CreateGraphics", e);
    }
}