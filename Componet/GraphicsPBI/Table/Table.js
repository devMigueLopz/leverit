Componet.TGraphicsPBI.TVclTable = function (inObjectHtml, Id) {
    this.TParent = function () {
        return this;
    }.bind(this);

    var _this = this.TParent();
    this.ObjectHtml = inObjectHtml;
    this.Id = "GraphicTable_" + Id;
    this._BoxPlugin = null;
    this._Width = null;
    this._Height = 400;
    this._Left = null;
    this._Rigth = null;
    this._Top = null;
    this._Bottom = null;
    this._AlighLeft = null;
    this._AlighRight = null;
    this._AlighTop = null;
    this._AlighBottom = null;
    this._Label = new Array();
    this._Title = "Graphic Title";
    this._DisplayTitle = false;
    this._Elements = new Array();
    this.ResetElementsHighlight = function () {
        for (var i = 0; i < this.Elements.length; i++) {
            this.Elements[i].Highlight = false;
        }
    }
    this.ResetFlagClick = function () {
        for (var i = 0; i < this.Elements.length; i++) {
            this.Elements[i].FlagClick = false;
        }
    }
    Object.defineProperty(this, 'Left', {
        get: function () {
            return this._Left;
        },
        set: function (_Value) {
            this._Left = _Value;
            $(this.ObjectHtml).css('left', this.Left);
        }
    });

    Object.defineProperty(this, 'Right', {
        get: function () {
            return this._Rigth;
        },
        set: function (_Value) {
            this._Rigth = _Value;
            $(this.ObjectHtml).css('right', this.Right);
        }
    });

    Object.defineProperty(this, 'Top', {
        get: function () {
            return this._Top;
        },
        set: function (_Value) {
            this._Top = _Value;
            $(this.ObjectHtml).css('top', this.Top);
        }
    });

    Object.defineProperty(this, 'Bottom', {
        get: function () {
            return this._Bottom;
        },
        set: function (_Value) {
            this._Bottom = _Value;
            $(this.ObjectHtml).css('bottom', this.Bottom);
        }
    });

    Object.defineProperty(this, 'Width', {
        get: function () {
            return this._Width;
        },
        set: function (_Value) {
            this._Width = _Value;
            $(this.ObjectHtml.children).css('width', this.Width);
        }
    });

    Object.defineProperty(this, 'Height', {
        get: function () {
            return this._Height;
        },
        set: function (_Value) {
            this._Height = _Value;
            $(this.ObjectHtml).css({ 'height': this.Height });
            if (this.ObjectHtml.children[this.ObjectHtml.children.length - 1] != undefined) {
                if (this._DisplayTitle) {
                    $(this.ObjectHtml.children[this.ObjectHtml.children.length - 1]).css({ 'overflow-y': 'auto', 'max-height': parseInt(this.Height) - 60, 'height': parseInt(this.Height) - 60 });
                } else {
                    $(this.ObjectHtml.children[this.ObjectHtml.children.length - 1]).css({ 'overflow-y': 'auto', 'max-height': this.Height, 'height': this.Height });
                }
            }
        }
    });

    Object.defineProperty(this, 'AlighLeft', {
        get: function () {
            return this._AlighLeft;
        },
        set: function (_Value) {
            this._AlighLeft = _Value;
            $(this.ObjectHtml).css('padding-left', this.AlighLeft);
        }
    });

    Object.defineProperty(this, 'AlighRight', {
        get: function () {
            return this._AlighRight;
        },
        set: function (_Value) {
            this._AlighRight = _Value;
            $(this.ObjectHtml).css('padding-right', this.AlighRight);
        }
    });

    Object.defineProperty(this, 'AlighTop', {
        get: function () {
            return this._AlighTop;
        },
        set: function (_Value) {
            this._AlighTop = parseInt(_Value);
            $(this.ObjectHtml).css('padding-top', this.AlighTop);
            if (this.DisplayTitle) {
                $(this.ObjectHtml.children[this.ObjectHtml.children.length - 1]).css({ 'overflow-y': 'auto', 'max-height': parseInt(this.Height) - (this.AlighTop + 60 + this.AlighBottom), 'height': parseInt(this.Height) - (this.AlighTop + 60 + this.AlighBottom) });
            } else {
                $(this.ObjectHtml.children[this.ObjectHtml.children.length - 1]).css({ 'overflow-y': 'auto', 'max-height': parseInt(this.Height) - (this.AlighBottom + this.AlighTop), 'height': parseInt(this.Height) - (this.AlighBottom + this.AlighTop) });
            }
        }
    });

    Object.defineProperty(this, 'AlighBottom', {
        get: function () {
            return this._AlighBottom;
        },
        set: function (_Value) {
            this._AlighBottom = parseInt(_Value);
            $(this.ObjectHtml).css('padding-bottom', this.AlighBottom);
            if (this.DisplayTitle) {
                $(this.ObjectHtml.children[this.ObjectHtml.children.length - 1]).css({ 'overflow-y': 'auto', 'max-height': parseInt(this.Height) - (this.AlighBottom + 60 + this.AlighTop), 'height': parseInt(this.Height) - (this.AlighBottom + 60 + this.AlighTop) });
            } else {
                $(this.ObjectHtml.children[this.ObjectHtml.children.length - 1]).css({ 'overflow-y': 'auto', 'max-height': parseInt(this.Height) - (this.AlighBottom + this.AlighTop), 'height': parseInt(this.Height) - (this.AlighBottom + this.AlighTop) });
            }
        }
    });

    Object.defineProperty(this, 'Label', {
        get: function () {
            return this._Label;
        },
        set: function (_Value) {
            this._Label = _Value;
        }
    });

    Object.defineProperty(this, 'Title', {
        get: function () {
            return this._Title;
        },
        set: function (_Value) {
            this._Title = _Value;
        }
    });
    Object.defineProperty(this, 'DisplayTitle', {
        get: function () {
            return this._DisplayTitle;
        },
        set: function (_Value) {
            this._DisplayTitle = _Value;
        }
    });
    Object.defineProperty(this, 'Elements', {
        get: function () {
            return this._Elements;
        },
        set: function (_Value) {
            this._Elements = _Value;
        }
    });

    this.TVclGraphicElementsAdd = function (inElement) {
        this._Elements.push(inElement);
    }
}

Componet.TGraphicsElements.TVclGraphicElementTable = function () {
    var _this = this;
    this._Id = null;
    this._Array = null;
    this._Highlight = true;
    this._liElement = null;
    this._onClick = null;
    this.FlagClick = false;
    Object.defineProperty(this, 'Id', {
        get: function () {
            return this._Id;
        },
        set: function (_Value) {
            this._Id = _Value;
        }
    });
    Object.defineProperty(this, 'Array', {
        get: function () {
            return this._Array;
        },
        set: function (_Value) {
            this._Array = _Value;
        }
    });
    Object.defineProperty(this, 'Highlight', {
        get: function () {
            return this._Highlight;
        },
        set: function (_Value) {
            this._Highlight = _Value;
            if (this.liElement != null) {
                this.RefreshElementTable(_this);
            }
        }
    });
    Object.defineProperty(this, 'liElement', {
        get: function () {
            return this._liElement;
        },
        set: function (_Value) {
            this._liElement = _Value;
            if (this._liElement != null) this._liElement.onclick = this._onClick;
        }
    });
    Object.defineProperty(this, 'onClick', {
        get: function () {
            return this._onClick;
        },
        set: function (_Value) {
            this._onClick = function () {
                _Value(_this);
            };
            if (this._liElement != null) this._liElement.onclick = this._onClick;
        }
    });
}

Componet.TGraphicsPBI.TVclTable.prototype.CreateGraphic = function () {
    var _this = this.TParent();
    try {
        var TitleTable = new Uh1(_this.ObjectHtml, '', _this.Title);
        $(TitleTable).addClass('title-table');

        var AllContainer = new TVclStackPanel(_this.ObjectHtml, _this.Id, 1, [[12], [12], [12], [12], [12]]);
        AllContainer.Row.This.style.marginRight = '0px';
        AllContainer.Row.This.style.marginLeft = '0px';
        var Container_1 = AllContainer.Column[0].This;

        $(Container_1).addClass('content-table');

        _this._Label = Object.keys(_this._Elements[0]._Array);

        var Table = new TGrid(Container_1, Container_1.id + "_TTable", "");
        Table.This.style.width = '100%';
        Table.ResponsiveCont.classList.remove('table-responsive')

        for (var i = 0; i < _this.Label.length; i++) {
            var Col = new TColumn();
            Col.Name = "Columna" + (i + 2);
            Col.Caption = _this.Label[i];
            Col.Index = i;
            Col.DataType = SysCfg.DB.Properties.TDataType.Unknown;
            Table.AddColumn(Col);
            $(Table.Columns[i].This).addClass('g-thead-th');
        }
        for (var i = 0; i < _this._Elements.length; i++) {
            var NewRow = new TRow();
            for (var j = 0; j < _this.Label.length; j++) {
                var Cell = new TCell();
                Cell.Value = _this._Elements[i]._Array[_this._Label[j]];
                Cell.IndexColumn = j;
                Cell.IndexRow = i;
                NewRow.AddCell(Cell);

                if (j % 2 == 0) {
                    NewRow._Cells[j].This.style.color = 'rgb(1,184,170)';
                }
                else {
                    NewRow._Cells[j].This.style.color = 'rgb(230,70,50)';
                }

                $(NewRow.Cells[j].This).addClass('g-tbody-td');
            }
            if (!_this._Elements[i].Highlight) NewRow.This.style.opacity = '0.4';
            Table.AddRow(NewRow);

            _this._Elements[i].liElement = NewRow.This;
        }

        if (_this.DisplayTitle) {
            TitleTable.style.display = 'block';
            $(this.ObjectHtml.children[this.ObjectHtml.children.length - 1]).css({ 'overflow-y': 'auto', 'max-height': parseInt(this.Height) - 60, 'height': parseInt(this.Height) - 60 });
        } else {

            TitleTable.style.display = 'none';
            $(this.ObjectHtml.children[this.ObjectHtml.children.length - 1]).css({ 'overflow-y': 'auto', 'max-height': this.Height, 'height': this.Height });
        }

        $(Table.Header).addClass('g-thead');
        $(Table.Body).addClass('g-tbody');

        Table.EnabledResizeColumn = false;


        /*Function for order*/
        $('th').click(function () {
            var table = $(this).parents('table').eq(0)
            var rows = table.find('tr:gt(0)').toArray().sort(comparer($(this).index()))
            this.asc = !this.asc
            if (!this.asc) {
                rows = rows.reverse()
            }
            for (var i = 0; i < rows.length; i++) {
                table.append(rows[i])
            }
            setIcon($(this), this.asc);
        });
        function comparer(index) {
            return function (a, b) {
                var valA = getCellValue(a, index),
                    valB = getCellValue(b, index)
                return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.localeCompare(valB)
            }
        }
        function getCellValue(row, index) {
            return $(row).children('td').eq(index).html()
        }
        function setIcon(element, asc) {
            $("th").each(function (index) {
                $(this).removeClass("sorting");
                $(this).removeClass("asc");
                $(this).removeClass("desc");
            });
            element.addClass("sorting");
            if (asc) element.addClass("asc");
            else element.addClass("desc");
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Table.js Componet.TGraphicsPBI.TVclTable.prototype.CreateGraphic", e);
    }
}

Componet.TGraphicsPBI.TVclTable.prototype.RefreshGraphic = function () {
    var _this = this.TParent();
    try {
        $(_this.ObjectHtml).find("h1").remove();
        $(_this.ObjectHtml).find("div").remove();
        this.CreateGraphic();
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Table.js Componet.TGraphicsPBI.TVclTable.prototype.RefreshGraphic", e);
    }
}

Componet.TGraphicsPBI.TVclTable.prototype.DestroyGraphic = function () {
    var _this = this.TParent();
    try {
        $(_this.ObjectHtml).html("");
        $(_this.ObjectHtml).find("h1").remove();
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Table.js Componet.TGraphicsPBI.TVclTable.prototype.DestroyGraphic", e);
    }
}

Componet.TGraphicsElements.TVclGraphicElementTable.prototype.RefreshElementTable = function (element) {
    try {
        if (element.Highlight) { $(element.liElement).css('opacity', '0.4'); }
        else { $(element.liElement).css('opacity', '1'); }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Table.js Componet.TGraphicsElements.TVclGraphicElementTable.prototype.RefreshElementTable", e);
    }
}
