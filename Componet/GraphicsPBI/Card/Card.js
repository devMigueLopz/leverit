Componet.TGraphicsPBI.TVclCard = function (ObjectHtml, Id) {
    this.TParent = function () {
        return this;
    }.bind(this);
    this.ObjectHtml = ObjectHtml;
    this.ID = Id;
    this._Elements = new Array();
    Object.defineProperty(this, 'Elements', {
        get: function () {
            return this._Elements;
        },
        set: function (_Value) {
            this._Elements = _Value;
        }
    });
    this.ResetElementsHighlight = null;
    this.CardElementsAdd = function (inElement) {
        this._Elements.push(inElement);
    }
    this.ResetOption = function () {
        for (var i = 0; i < this.Elements.length; i++) {
            this.Elements[i].ValueText = this._Elements[i]._Value;
        }
    }
    this.ResetFlagClick = function () {
        for (var i = 0; i < this.Elements.length; i++) {
            this.Elements[i].FlagClick = false;
        }
    }
}
Componet.TGraphicsElements.TVclCardElements = function () {
    this._Color = null;
    this._Border = null;
    this._Display = null;
    this._Height = null;
    this._Right = null;
    this.FlagClick = false;
    this._ColorText = null;
    this._ColorValue = null;
    this._ColorDisplay = null;
    this._Value = null;
    this._Text = null;
    this._Id = null;
    this._DisplayValue = null;
    this._onClick = null;
    this._ElementDOM = null;
    var _this = this;
    Object.defineProperty(this, 'Color', { /*el color del fondo*/
        get: function () {
            return this._Color;
        },
        set: function (_Value) {
            this._Color = _Value;
            if (this._ElementDOM !== null) {
                this._ElementDOM.Color.style.background = _Value;
            }

        }
    });
    Object.defineProperty(this, 'Display', { /*si se muestra o se oculta*/
        get: function () {
            return this._Display;
        },
        set: function (_Value) {
            this._Display = _Value;
            if (this._ElementDOM !== null) {
                _Value ? this._ElementDOM.Display.style.display = "block" : this._ElementDOM.Display.style.display = "none";
            }
        }
    });

    Object.defineProperty(this, 'Border', { /*border*/
        get: function () {
            return this._Border;
        },
        set: function (_Value) {
            this._Border = _Value;
            if (this._ElementDOM !== null) {
                this._ElementDOM.Border.style.borderColor = _Value;
            }

        }
    });
    Object.defineProperty(this, 'ColorText', {
        get: function () {
            return this._ColorText;
        },
        set: function (_Value) {
            this._ColorText = _Value;
            if (this._ElementDOM !== null) {
                this._ElementDOM.Text.style.color = _Value;
            }
        }
    });
    Object.defineProperty(this, 'Height', {
        get: function () {
            return this._Height;
        },
        set: function (_Value) {
            this._Height = _Value;
            if (this._ElementDOM !== null) {

                this._ElementDOM.BoxPlugin.style.height = _Value;
            }
        }
    });
    Object.defineProperty(this, 'Width', {//"0px"
        get: function () {
            return this._Width;
        },
        set: function (_Value) {
            this._Width = _Value;
            if (this._ElementDOM !== null) {
                this._ElementDOM.BoxPlugin.style.width = _Value;
            }
            //
        }
    });
    Object.defineProperty(this, 'Left', {//"0px"
        get: function () {
            return this._Left;
        },
        set: function (_Value) {
            this._Left = _Value;
            if (this._ElementDOM !== null) {
                this._ElementDOM.BoxPlugin.style.left = _Value;
            }
            //this.This.style.left = _Value;
        }
    });
    Object.defineProperty(this, 'Top', {//"0px"
        get: function () {
            return this._Top;
        },
        set: function (_Value) {
            this._Top = _Value;
            if (this._ElementDOM !== null) {
                this._ElementDOM.BoxPlugin.style.top = _Value;
            }
            //this.This.style.top = _Value;
        }
    });
    Object.defineProperty(this, 'Bottom', {//"0px"
        get: function () {
            return this._Bottom;
        },
        set: function (_Value) {
            this._Bottom = _Value;
            if (this._ElementDOM !== null) {
                this._ElementDOM.BoxPlugin.style.bottom = _Value;
            }
            //this.This.style.bottom = _Value;
        }
    });
    Object.defineProperty(this, 'Right', {//"0px"
        get: function () {
            return this._Right;
        },
        set: function (_Value) {
            this._Right = _Value;
            if (this._ElementDOM !== null) {
                this._ElementDOM.BoxPlugin.style.right = _Value;
            }
            //this.This.style.right = _Value;
        }
    });

    Object.defineProperty(this, 'ColorValue', {
        get: function () {
            return this._ColorValue;
        },
        set: function (_Value) {
            this._ColorValue = _Value;
            if (this._ElementDOM !== null) {
                this._ElementDOM.Value.style.color = _Value;
            }
        }
    });
    Object.defineProperty(this, 'ColorDisplay', {
        get: function () {
            return this._ColorDisplay;
        },
        set: function (_Value) {
            this._ColorDisplay = _Value;
            if (this._ElementDOM !== null) {
                this._ElementDOM.DisplayValue.style.color = _Value;
            }
        }
    });
    Object.defineProperty(this, 'Text', { /*resumen*/
        get: function () {
            return this._Text;
        },
        set: function (_Value) {
            this._Text = _Value;
            if (this._ElementDOM !== null) {
                this._ElementDOM.Text.innerText = _Value;
            }
        }
    });
    Object.defineProperty(this, 'Value', {/*el numero*/
        get: function () {
            return this._Value;
        },
        set: function (_Value) {
            this._Value = _Value;
            if (this._ElementDOM !== null) {
                this._ElementDOM.Value.innerText = _Value;
            }
        }
    });
    Object.defineProperty(this, 'ValueText', {/*el numero*/
        get: function () {
            return this._Value;
        },
        set: function (_Value) {
            if (this._ElementDOM !== null) {
                this._ElementDOM.Value.innerText = _Value;
            }
        }
    });
    Object.defineProperty(this, 'Id', {
        get: function () {
            return this._Id;
        },
        set: function (_Value) {
            this._Id = _Value;
        }
    });
    Object.defineProperty(this, 'DisplayValue', {/*titulo*/
        get: function () {
            return this._DisplayValue;
        },
        set: function (_Value) {
            this._DisplayValue = _Value;
            if (this._ElementDOM !== null) {
                this._ElementDOM.DisplayValue.innerText = _Value;
            }
        }
    });
    Object.defineProperty(this, 'ElementDOM', {
        get: function () {
            return this._ElementDOM;
        },
        set: function (_Value) {
            this._ElementDOM = _Value;
            if (this._ElementDOM != null) this._ElementDOM.Display.onclick = this._onClick;
        }
    });
    Object.defineProperty(this, 'onClick', {
        get: function () {
            return this._onClick
        },
        set: function (_Value) {
            this._onClick = function () {
                _Value(_this);
            };
            if (this._ElementDOM != null) this._ElementDOM.Display.onclick = this._onClick;

        }
    })
}
Componet.TGraphicsPBI.TVclCard.prototype.CreateCards = function (i) {
    var _this = this.TParent();
    try {
        var boxCard = new TVclStackPanel(_this.ObjectHtml, _this.ID + "_Box", 1, [[12], [12], [12], [12], [12]]); /*row*/
        var Options = boxCard.Column[0].This;
        for (var i = 0; i < _this._Elements.length; i++) {
            Options.appendChild(createCardDOM(i))
        }
        function createCardDOM(i) {
            var RowColumnInit = new TVclStackPanel("", "", 1, [[12], [12], [12], [12], [12]]);
            var Content_number = Uspan(RowColumnInit.Column[0].This, "", "");
            var Content_text1 = Uspan(RowColumnInit.Column[0].This, "", "");
            var Content_text2 = Uspan(RowColumnInit.Column[0].This, "", "");
            /*Agregar Contendio*/
            Content_number.innerHTML = _this._Elements[i]._Value;
            Content_text1.innerHTML = _this._Elements[i]._DisplayValue;
            Content_text2.innerHTML = _this._Elements[i]._Text;
            /*Agregar Estilos*/
            RowColumnInit.Column[0].This.style.background = _this._Elements[i]._Color;
            RowColumnInit.Column[0].This.style.border = "1px solid " + _this._Elements[i]._Border;
            Content_text1.style.color = _this._Elements[i]._ColorDisplay;
            Content_text2.style.color = _this._Elements[i]._ColorText;
            Content_number.style.color = _this._Elements[i]._Value;
            /**/
            /*clases para los estilos */
            Content_text1.className = "titleCard contentCard";
            Content_number.className = "valueCard contentCard";
            Content_text2.className = "textCard contentCard";
            if (!_this._Elements[i].Display) { RowColumnInit.Column[0].This.style.display = "none"; }
            //
            _this._Elements[i].ElementDOM = { Border: RowColumnInit.Column[0].This, DisplayValue: Content_text1, Value: Content_number, ValueText: Content_number, Text: Content_text2, Color: RowColumnInit.Column[0].This, Display: RowColumnInit.Column[0].This, BoxPlugin: RowColumnInit.Column[0].This }
            return RowColumnInit.Column[0].This;
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Card.js Componet.TGraphicsPBI.TVclCard.prototype.CreateCards", e);
    }
}
Componet.TGraphicsPBI.TVclCard.prototype.deleteAllCardsDOM = function () {
    var _this = this.TParent();
    try {
        var idParent = _this.ObjectHtml.id;
        var elementChild = document.querySelector("#" + idParent + " > div");
        _this.ObjectHtml.removeChild(elementChild);
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Card.js Componet.TGraphicsPBI.TVclCard.prototype.deleteAllCardsDOM", e);
    }
}
