﻿Componet.TPanelServices = function (inObjectHtml, Id, inCallBack) {

	this.TParent = function () {
		return this;
	}.bind(this);

	var _this = this.TParent();
	this._ObjectHtml = inObjectHtml;
	_this.InCallBack = inCallBack;
	this._Id = Id;
	this._HeightTab = '350px';
	this._BackgroundContainer = 'transparent';
	this._Elements = new Array();
	/*this._Activate = null;*/
	this._ElementsDOMH = null;
	this._ElementsDOMV = null;
	this.Click = null;
	this.OrientationType = {
		Horizontal: {value: 1, name: 'Horizontal'},
		Vertical: {value: 2, name: 'Vertical'}
	};
	this._Orientation = this.OrientationType.Horizontal;;
	Object.defineProperty(this, 'HeightTab', {
		get: function(){
			return this._HeightTab;
		},
		set: function(_Value){
			this._HeightTab = _Value;
		}
	});
	Object.defineProperty(this, 'BackgroundContainer', {
		get: function(){
			return this._BackgroundContainer;
		},
		set: function(_Value){
			this._BackgroundContainer = _Value;
		}
	});
	Object.defineProperty(this, 'Elements', {
		get: function () {
			return this._Elements;
		},
		set: function (_Value) {
			this._Elements = _Value;
		}
	});
	Object.defineProperty(this, 'Orientation', {
		get: function(){
			return this._Orientation;
		},
		set: function(_Value){
			this._Orientation = _Value;
		}
	});
	this.TPanelServicesElementsAdd = function (inElement) {
		this._Elements.push(inElement);
	}
}

Componet.TElements.TPanelServicesElementTab = function (){
	this.This = null;
	this._Id = null;
	this._Position = null;
	this._Image = null;
	this._Title = null;

	this._TitleColor = 'black';
	this._BackgroundActive = 'silver';
	this._TitleActiveColor = 'black';
	this._Border = 'black';
	this._BackgroundHover = 'lightgray';
	this._TitleHoverColor = 'black';
	this._Description = '';

	this._ElementsDOM = null;
	this._ElementsContent = new Array();
	
	Object.defineProperty(this, 'Id', {
		get: function(){
			return this._Id;
		},
		set: function(_Value){
			this._Id = _Value;
		}
	});
	Object.defineProperty(this, 'Position', {
		get: function () {
			return this._Position;
		},
		set: function (_Value) {
			this._Position = _Value;
			if(this._ElementsDOM != null) this._ElementsDOM.This.Load();
		}
	});
	Object.defineProperty(this, 'Image', {
		get: function(){
			return this._Image;
		},
		set: function(_Value){
			this._Image = _Value;
			if(this._ElementsDOM != null) $(this._ElementsDOM.img.This).attr('src',_Value);
		}
	});
	Object.defineProperty(this, 'Title', {
		get: function(){
			return this._Title;
		},
		set: function(_Value){
			this._Title = _Value;
			if(this._ElementsDOM != null) this._ElementsDOM.title.innerText = _Value;
		}
	});
	Object.defineProperty(this, 'ElementsContent', {
		get: function(){
			return this._ElementsContent;
		},
		set: function(_Value){
			this._ElementsContent = _Value;
		}
	});
	Object.defineProperty(this, 'TitleColor', {
		get: function () {
			return this._TitleColor;
		},
		set: function (_Value) {
			 if(_Value != '') this._TitleColor = _Value;
		}
	});
	Object.defineProperty(this, 'BackgroundActive', {
		get: function () {
			return this._BackgroundActive;
		},
		set: function (_Value) {
			if (_Value != '') this._BackgroundActive = _Value;
		}
	});
	Object.defineProperty(this, 'TitleActiveColor', {
		get: function () {
			return this._TitleActiveColor;
		},
		set: function (_Value) {
			if (_Value != '') this._TitleActiveColor = _Value;
		}
	});
	Object.defineProperty(this, 'Border', {
		get: function () {
			return this._Border;
		},
		set: function (_Value) {
			if (_Value != '') this._Border = _Value;
		}
	});
	Object.defineProperty(this, 'BackgroundHover', {
		get: function () {
			return this._BackgroundHover;
		},
		set: function (_Value) {
			if (_Value != '') this._BackgroundHover = _Value;
		}
	});
	Object.defineProperty(this, 'TitleHoverColor', {
		get: function () {
			return this._TitleHoverColor;
		},
		set: function (_Value) {
			if (_Value != '') this._TitleHoverColor = _Value;
		}
	});
	Object.defineProperty(this, 'Description', {
		get: function(){
			return this._Description;
		},
		set: function (_Value){
			this._Description = _Value;
		}
	});
	this.TPanelServiceElementsContentAdd = function (inElement) {
		this._ElementsContent.push(inElement);
	}
}

Componet.TElements.TPanelServicesElementTabContent = function(){
	this.This = null;
	this._ContentId = null;
	this._ContentPosition = null;
	this._ContentImage = null;
	this._ContentTitle = null;
	this._ContentDescription = null;
	this._ContentPhrase = null;

	//SECUNDARY_TEXTCOLOR
	this._ContentTextColor = 'black';

	this._ElementsDOM = null;

	Object.defineProperty(this, 'ContentId', {
		get: function(){
			return this._ContentId;
		},
		set: function(_Value){
			this._ContentId = _Value;
		}
	});
	Object.defineProperty(this, 'ContentPosition', {
		get: function(){
			return this._ContentPosition;
		},
		set: function(_Value){
			this._ContentPosition = _Value;
			if(this._ElementsDOM != null) this._ElementsDOM.This.Load();
		}
	});
	Object.defineProperty(this, 'ContentImage', {
		get: function(){
			return this._ContentImage;
		},
		set: function(_Value){
			this._ContentImage = _Value;
			if(this._ElementsDOM != null) $(this._ElementsDOM.contentImg.This).attr('src',_Value);
		}
	});
	Object.defineProperty(this, 'ContentTitle', {
		get: function(){
			return this._ContentTitle;
		},
		set: function(_Value){
			this._ContentTitle = _Value;
			if(this._ElementsDOM != null) this._ElementsDOM.contentTitle.innerText = _Value;
		}
	});
	Object.defineProperty(this, 'ContentDescription', {
		get: function(){
			return this._ContentDescription;
		},
		set: function(_Value){
			this._ContentDescription = _Value;
			if(this._ElementsDOM != null) this._ElementsDOM.contentDescription.innerText = _Value;
		}
	});
	Object.defineProperty(this, 'ContentPhrase', {
		get: function(){
			return this._ContentPhrase;
		},
		set: function(_Value){
			this._ContentPhrase = _Value;
			if(this._ElementsDOM != null) this._ElementsDOM.contentPhrase.innerText = _Value;
		}
	});
	Object.defineProperty(this, 'ContentTextColor', {
		get: function () {
			return this._ContentTextColor;
		},
		set: function (_Value) {
			this._ContentTextColor = _Value;
		}
	});
}

Componet.TPanelServices.prototype.Load = function(){
	var _this = this.TParent();
	try {
		if(_this.Orientation.name == 'Horizontal'){
			_this.CreateHorizontal();
		}
		else if(_this.Orientation.name == 'Vertical'){
			_this.CreateVertical();
		}
	} catch (e) {
		SysCfg.Log.Methods.WriteLog("PanelService.js Componet.TPanelServices.prototype.Load", e);
	}
}

Componet.TPanelServices.prototype.CreateHorizontal = function(){
	var _this = this.TParent();
	try {
		_this.Destroy();
		_this.OrderElements();

		_this._ObjectHtml.style.background = _this.BackgroundContainer;

		var AllContainer = new TVclStackPanel(_this._ObjectHtml, 'AllContainer_' + _this._Id, 2, [[12, 12], [12, 12], [12, 12], [12, 12], [12, 12]]);
		AllContainer.Row.This.style.width = "100%";
		AllContainer.Row.This.style.margin = "0 0 0 0px";
		var AllContainer_1 = AllContainer.Column[0].This;
		$(AllContainer_1).addClass('PS-container-fluid');
		$(AllContainer_1).css({ "z-index": "1", "bottom": "-0.5px" });
		//$(AllContainer_1).css({ "z-index": "1", "bottom": "-0.5px", "padding-right": "0px"}); //no va en el demo

		var AllContainer_2 = AllContainer.Column[1].This;
		$(AllContainer_2).addClass('container-fluid PS-init tab-content');

		var Container = new Udiv(AllContainer_1, 'Container_' + _this._Id);
		// $(Container).addClass('container resis');
		$(Container).addClass('PS-resize');

		var ScrollLeft = new Udiv(Container, '');
		$(ScrollLeft).addClass('PS-scroller PS-scroller-left');

		var IconLeft = new TVclImagen(ScrollLeft, "");
		IconLeft.Src = 'img/img_tab/nav-left(32).png';

		var ScrollRight = new Udiv(Container, '');
		$(ScrollRight).addClass('PS-scroller PS-scroller-right');

		var IconRight = new TVclImagen(ScrollRight, "");
		IconRight.Src = 'img/img_tab/nav-right(32).png';

		var Wrapper = new Udiv(Container, '');
		$(Wrapper).addClass('PS-wrapper-h');

		var List = VclList(Wrapper, '', _this.Elements.length);
		$(List.ul).addClass('nav nav-tabs PS-list');
		$(List.li[0]).addClass('active');
		_this._ElementsDOMH = { containerTab: AllContainer_2, list: List, wrapper: Wrapper, scrollLeft: ScrollLeft, scrollRight: ScrollRight };
		var dropDownList = new Array();
		for (var i = 0; i < List.li.length; i++) {
			var IconTag = new Ua(List.li[i], '', '');
			$(IconTag).attr({ 'href': '#TabPanel_' + i + _this._Id, 'data-toggle': 'tab' });
			IconTag.setAttribute('data-id', i);
			var IconBox = new Udiv(IconTag, '');
			$(IconBox).addClass('icon-box')
			var IconImage = new TVclImagen(IconBox, "");
			IconImage.Src = _this.Elements[i].Image;
			$(IconImage.This).addClass('PS-icon-img');
			IconImage.This.onerror = noIconImage;
			function noIconImage(e){
				// e.target.src= 'https://www.blackwallst.directory/images/NoImageAvailable.png';
				e.target.src= 'img/img_tab/no-image.png';
			}

			var IconTitle = new Udiv(IconBox, '');
			$(IconTitle).addClass('PS-icon-title');
			
			IconTitle.style.color = _this.Elements[i].TitleColor;
			IconTag.setAttribute('data-id', i);

			if (Source.Menu.IsMobil) {
				$(IconTag).css({'width':'100px', 'max-width': '100px', 'padding-right':'0px', 'padding-left': '0px'});
				IconImage.This.style.height = '40px';
				IconImage.This.style.width = '40px';
				IconTitle.style.fontSize = '9px';
				IconTitle.style.height = '30px';
				Wrapper.style.minHeight = '105px';
				AllContainer_1.style.height = '105px';
				ScrollLeft.style.height = '105px';
				ScrollRight.style.height = '105px';
				if (_this.Elements[i].Title.length > 16) {
					$(IconTitle).css('padding-top', '0px');
						IconTitle.innerHTML = _this.cutWord(_this.Elements[i].Title, 16, 2);
				}
				else {
					$(IconTitle).css('padding-top', '8px');
					IconTitle.innerText = _this.Elements[i].Title;
				}
				IconTitle.setAttribute('title', _this.Elements[i].Title);
			}
			else{
				$(IconTag).css({'width':'160px', 'max-width': '160px', 'padding-right':'0px', 'padding-left': '0px'});
				IconImage.This.style.height = '50px';
				IconImage.This.style.width = '50px';
				IconTitle.style.fontSize = '11px';
				IconTitle.style.height = '45px';
				Wrapper.style.minHeight = '130px';
				ScrollLeft.style.height = '130px';
				ScrollRight.style.height = '130px';
				if (_this.Elements[i].Title.length > 20) {
					$(IconTitle).css('padding-top', '8px');
					if (_this.Elements[i].Title.length > 38) {
						$(IconTitle).css('padding-top', '0px');
					}
					IconTitle.innerHTML = _this.cutWord(_this.Elements[i].Title, 20, 3);
				}
				else {
					$(IconTitle).css('padding-top', '15px');
					IconTitle.innerText = _this.Elements[i].Title;
				}
				IconTitle.setAttribute('title', _this.Elements[i].Title);
			}

			_this.Elements[i]._ElementsDOM = { iconTag: IconTag, img: IconImage, title: IconTitle, This: _this };

			var TabPanel = new Udiv(AllContainer_2, 'TabPanel_' + i + _this._Id);
			$(TabPanel).addClass('PS-resize PS-tab-panel tab-pane');
			_this.Elements[i].This = TabPanel; // Creación de This para TabPanel
			if (i == 0) { $(TabPanel).addClass('active'); }

			var Element_1_Description = new Udiv(TabPanel, '');
			$(Element_1_Description).addClass('row PS-element');
			var Element_2_Description = new Udiv(Element_1_Description, '');
			$(Element_2_Description).addClass('col-md-9 PS-col-center PS-element-description'); //element-description

			var InfoTabDescription = new Uspan(Element_2_Description, '', _this.Elements[i].Description);

			for (var j = 0; j < _this.Elements[i].ElementsContent.length; j++) {

				var Element_1 = new Udiv(TabPanel, '');
				$(Element_1).addClass('row PS-element');

				var Element_2 = new Udiv(Element_1, '');
				$(Element_2).addClass('col-md-9 PS-col-center');

				var InfoBoxIcon = new Udiv(Element_2, '');
				$(InfoBoxIcon).addClass('PS-info-box-icon');

				var IconBoxIcon = new TVclImagen(InfoBoxIcon, '');
				IconBoxIcon.Src = _this.Elements[i].ElementsContent[j].ContentImage;
				$(IconBoxIcon.This).addClass('img-responsive');
				IconBoxIcon.This.onerror = noImageIconBox;
				function noImageIconBox(e){
					e.target.src= 'img/img_tab/no-image.png';
				}

				var InfoBoxTitle = new Uspan(Element_2, '', _this.Elements[i].ElementsContent[j].ContentTitle);
				$(InfoBoxTitle).addClass('PS-info-box-title');

				var InfoBoxDescription = new Uspan(Element_2, '', _this.Elements[i].ElementsContent[j].ContentDescription);
				$(InfoBoxDescription).addClass('PS-info-box-text');

				var PanelPhrase = new Udiv(Element_2, '');
				$(PanelPhrase).addClass('col-md-12');
				PanelPhrase.innerHTML = _this.Elements[i].ElementsContent[j].ContentPhrase;
				_this.Elements[i].ElementsContent[j].This = PanelPhrase;  // Creación de This para Panel de DropDownPanel

				var VclDropDownPanel = new TVclDropDownPanel(Element_2, "PSDDP" + _this._Id);
				VclDropDownPanel.BoderTopColor = "transparent";
				VclDropDownPanel.BoderAllColor = "transparent";
				VclDropDownPanel.BackgroundColor = "transparent";
				VclDropDownPanel.MarginBottom = "5px";
				VclDropDownPanel.MarginTop = "5px";
				VclDropDownPanel.MarginLeft = "20px";
				VclDropDownPanel.MarginRight = "20px";
				VclDropDownPanel.Tag = PanelPhrase.textContent;

				VclDropDownPanel.This[0].This.style.color = _this.Elements[i].ElementsContent[j].ContentTextColor;

				VclDropDownPanel.AddToHeader(InfoBoxIcon);
				VclDropDownPanel.AddToHeader(InfoBoxTitle);
				VclDropDownPanel.AddToHeader(InfoBoxDescription);
				VclDropDownPanel.AddToBody(PanelPhrase);
				VclDropDownPanel.RowBody.This.classList.add("firstMapping");
				_this.Elements[i].ElementsContent[j]._ElementsDOM = { contentImg: IconBoxIcon, contentTitle: InfoBoxTitle, contentDescription: InfoBoxDescription, contentPhrase: PanelPhrase, This: _this };
				VclDropDownPanel.objCallback = { PhraseBody: PanelPhrase.innerHTML, ContentRowBody: VclDropDownPanel.RowBody.This };
				VclDropDownPanel._functionOpen = function () {
					if (_this.Click !== null) {
						_this.Click(this.objCallback);
					}
				}
				IconClick = function (elementVclDropDownPanel, elementIconBoxIcon) {
					elementIconBoxIcon.onClick = function () {
						elementVclDropDownPanel.Image.onClick();
					}
				}
				IconClick(VclDropDownPanel, IconBoxIcon)
				if (Source.Menu.IsMobil) {
					IconBoxIcon.This.style.height = '60px';
					IconBoxIcon.This.style.width = '60px';
					VclDropDownPanel.MarginBottom = "0px";
					VclDropDownPanel.MarginTop = "0px";
					VclDropDownPanel.MarginLeft = "0px";
					VclDropDownPanel.MarginRight = "0px";
					$(Element_2).removeClass('col-md-9 PS-col-center PS-element-description');
					$(Element_2).addClass('col-md-12');
					$(Element_2).css({ "padding-left": "0px", "padding-right": "0px" });
					$(PanelPhrase).css({ "padding-left": "0px", "padding-right": "0px" });
				}
				else{
					IconBoxIcon.This.style.height = '70px';
					IconBoxIcon.This.style.width = '70px';
				}
			}

			List.li[i].firstChild.onclick = function () {
				for (var k = 0; k < _this._ElementsDOMH.list.li.length; k++) {
					_this._ElementsDOMH.list.li[k].firstChild.style.background = 'transparent';
					_this.Elements[k]._ElementsDOM.title.style.color = _this.Elements[k].TitleColor;
					_this.Elements[k]._ElementsDOM.iconTag.style.borderLeft = 'none';
					_this.Elements[k]._ElementsDOM.iconTag.style.borderTop = 'none';
					_this.Elements[k]._ElementsDOM.iconTag.style.borderRight = 'none';
					$(_this._ElementsDOMH.list.li[k].firstChild).css('border-bottom-color', 'transparent'); //para camuflar el borde debajo del tab
				}
				this.style.background = _this.Elements[this.dataset.id].BackgroundActive;
				_this.Elements[this.dataset.id]._ElementsDOM.title.style.color = _this.Elements[this.dataset.id].TitleActiveColor;
				_this._ElementsDOMH.containerTab.style.background = _this.Elements[this.dataset.id].BackgroundActive;
				_this._ElementsDOMH.containerTab.style.borderColor = _this.Elements[this.dataset.id].Border;
				this.style.borderLeft = '1px solid ' + _this.Elements[this.dataset.id].Border;
				this.style.borderTop = '1px solid ' + _this.Elements[this.dataset.id].Border;
				this.style.borderRight = '1px solid ' + _this.Elements[this.dataset.id].Border;

				$(this).css('border-bottom-color', _this.Elements[this.dataset.id].BackgroundActive); //para camuflar el borde debajo del tab
			}

			List.li[i].firstChild.onmouseover = function () {
				if (this.parentNode.classList[0] != 'active') {
					this.style.background = _this.Elements[this.dataset.id].BackgroundHover;
					_this.Elements[this.dataset.id]._ElementsDOM.title.style.color = _this.Elements[this.dataset.id].TitleHoverColor;
				}
			}
			List.li[i].firstChild.onmouseout = function () {
				if (this.parentNode.classList[0] != 'active') {
					this.style.background = 'transparent';
					_this.Elements[this.dataset.id]._ElementsDOM.title.style.color = _this.Elements[this.dataset.id].TitleColor;

				}
			}

			if (i == 0) {
				List.li[i].firstChild.style.background = _this.Elements[i].BackgroundActive;
				$(List.li[i].firstChild).css('border-bottom-color', _this.Elements[i].BackgroundActive); //para camuflar el borde debajo del tab
				_this._ElementsDOMH.containerTab.style.background = _this.Elements[i].BackgroundActive;
				_this.Elements[i]._ElementsDOM.title.style.color = _this.Elements[i].TitleActiveColor;
				_this.Elements[i]._ElementsDOM.iconTag.style.borderLeft = '1px solid ' + _this.Elements[i].Border;
				_this.Elements[i]._ElementsDOM.iconTag.style.borderTop = '1px solid ' + _this.Elements[i].Border;
				_this.Elements[i]._ElementsDOM.iconTag.style.borderRight = '1px solid ' + _this.Elements[i].Border;
				_this._ElementsDOMH.containerTab.style.borderColor = _this.Elements[i].Border;
			}
			else {
				_this.Elements[i]._ElementsDOM.iconTag.style.borderLeft = 'none';
				_this.Elements[i]._ElementsDOM.iconTag.style.borderTop = 'none';
				_this.Elements[i]._ElementsDOM.iconTag.style.borderRight = 'none';
			}
			
			if (Source.Menu.IsMobil) {
				$(Element_2_Description).removeClass('col-md-9 PS-col-center PS-element-description');
				$(Element_2_Description).addClass('col-md-12');
				$(Element_2_Description).css({ "padding-left": "0px", "padding-right": "0px", "padding-top": "0px" });
			}
		}

		_this.importarStyle(SysCfg.App.Properties.xRaiz + "Componet/PanelServices/Css/PSHorizontal.css", function () {

			_this.FunctionsTabPanelHorizontal();
			//fix
			if (typeof _this.InCallBack === 'function') {
				_this.InCallBack();
			}
			//

		}, function (e) {
			SysCfg.Log.Methods.WriteLog("PanelServices.js Componet.TPanelServices.prototype.CreateHorizontal this.importarScript(" + this.importarStyle + "Componet/PanelServices/Css/PSHorizontal.css)", e);
		});
	} catch (e) {
		SysCfg.Log.Methods.WriteLog("PanelServices.js Componet.TPanelServices.prototype.CreateHorizontal", e);
	}
}

Componet.TPanelServices.prototype.cutWord = function (title, sizeMax, limitLine){
	var titleTemp = title;
	var arrayWord = title.split(" ")
	var word = "";
	var cont = 0;
	var line1 = "";
	var line2 = "";
	if(limitLine == 2){ //sizeMax = 16
		for(var i = 0; i < arrayWord.length; i++){				
			if((word + arrayWord[i]).length <= sizeMax){
				word +=  arrayWord[i] + " ";
				if(cont == 0) line1 = word.trim();
				else line2 = word.trim();
			}
			else{
				if(cont == 0){
					if(word == ""){
						line1 = arrayWord[i].substring(0, sizeMax - 1);//Linea 1 hasta los 14 caracteres
						line2 = arrayWord[i].substring(sizeMax - 1, arrayWord[i].length);
					}
					else{
						line2 = arrayWord[i];
						cont = 1;
					}
					word = line2 + " ";
				}
				else{
					word += arrayWord[i];
				}
			}
		}
		line2 = word.substring(0, sizeMax - 3) + "...";
		titleTemp = line1 + "<br>" + line2;
	}
	else if(limitLine == 3){ //sizeMax = 17
		for(var i = 0; i < arrayWord.length; i++){
			if((word + arrayWord[i]).length <= sizeMax){
				word +=  arrayWord[i] + " ";
				if(cont <= 1) line1 = word.trim();
				else line2 = word.trim();
			}else{
				if(cont == 0){
					if(word == ""){
						line1 = arrayWord[i].substring(0, sizeMax - 1);//Linea 1 hasta los 17 caracteres
						titleTemp = line1 + "<br>";
						line1 = arrayWord[i].substring(sizeMax - 1, arrayWord[i].length);
					}
					else{
						titleTemp = line1 + "<br>";
						line1 = arrayWord[i];
						cont = 1;
					}
					word = line1 + " ";
				}
				else if(cont == 1){
					if(word == ""){
						line1 = arrayWord[i].substring(0, sizeMax - 1);//Linea 1 hasta los 14 caracteres
						line2 = arrayWord[i].substring(sizeMax - 1, arrayWord[i].length);
					}
					else{
						line2 = arrayWord[i];
						cont = 2;
					}
					word = line2 + " ";
				}
				else{
					word += arrayWord[i];
				}
			}
		}
		line2 = word.substring(0, sizeMax - 3) + "...";
		titleTemp += line1 + "<br>" + line2;
	}
	return titleTemp;
}

Componet.TPanelServices.prototype.FunctionsTabPanelHorizontal = function (){
	var _this = this.TParent();
	try {
		var tam_li = $(_this._ElementsDOMH.list.li).outerWidth();
		var tam = parseInt($(_this._ElementsDOMH.list.li).length);
		var cont = Math.floor($(_this._ElementsDOMH.wrapper).outerWidth() / tam_li);
		var aux = cont;

		this.widthOfList = function () {
			var itemsWidth = 0;
			$(_this._ElementsDOMH.list.li).each(function () {
				var itemWidth = $(this).outerWidth();
				itemsWidth += itemWidth;
			});
			return itemsWidth;
		};
		this.getActive = function () {
			for (var i = 0; i < $(_this._ElementsDOMH.list.li).length; i++) {
				if ($(_this._ElementsDOMH.list.li)[i].className == 'active') {
					return i + 1;
				}
			}
		}
		this.getLeftPosi = function () {
			return $(_this._ElementsDOMH.list.ul).position().left;
		};

		this.reAdjust = function () {
			$(_this._ElementsDOMH.list.ul).css('left', 0);
			cont = Math.floor($(_this._ElementsDOMH.wrapper).outerWidth() / tam_li);
			aux = cont;
			if (_this.getActive() > cont) {
				//console.log(_this.getActive() + " " + cont);
				$(_this._ElementsDOMH.list.ul).css('left', ((cont * tam_li) - _this.getActive() * tam_li));
				//console.log(((cont * tam_li) - _this.getActive() * tam_li) + 'k');
				cont = _this.getActive();
			}
			if (($(_this._ElementsDOMH.wrapper).outerWidth()) < _this.widthOfList()) {
				if (tam > cont) {
					$(_this._ElementsDOMH.scrollRight).show();
					// console.log('aqui');
				}
				else {
					var temp = $(_this._ElementsDOMH.wrapper).outerWidth() - _this.widthOfList();
					$(_this._ElementsDOMH.list.ul).stop().animate({ left: temp }, 'fast');
					$(_this._ElementsDOMH.scrollRight).hide();
					// console.log('o aqui');
				}
			}
			else {
				$(_this._ElementsDOMH.scrollRight).hide();
			}

			if (_this.getLeftPosi() < 0) {
				$(_this._ElementsDOMH.scrollLeft).show();
			}
			else {
				$(_this._ElementsDOMH.scrollLeft).hide();
			}
		}
		_this.reAdjust();
		$(window).resize(function () { _this.reAdjust(); });
		// SysCfg.addResizeEvent(reAdjust);

		$(_this._ElementsDOMH.scrollRight).click(function () {
			if (cont < tam) {
				$(_this._ElementsDOMH.scrollLeft).show();
				$(_this._ElementsDOMH.scrollRight).show();
				$(_this._ElementsDOMH.list.ul).animate({ left: "-=" + tam_li }, 'fast');
				cont++;
				if (cont == tam) {
					var temp = $(_this._ElementsDOMH.wrapper).outerWidth() - _this.widthOfList();
					$(_this._ElementsDOMH.list.ul).animate({ left: temp }, 'fast');
					$(_this._ElementsDOMH.scrollRight).hide();
				}
			}
		});
		$(_this._ElementsDOMH.scrollLeft).click(function () {
			if (aux < cont) {
				$(_this._ElementsDOMH.scrollLeft).show();
				$(_this._ElementsDOMH.scrollRight).show();
				$(_this._ElementsDOMH.list.ul).animate({ left: "+=" + tam_li }, 'fast');
				cont--;
				if (aux == cont) {
					$(_this._ElementsDOMH.list.ul).animate({ left: 0 }, 'fast');
					$(_this._ElementsDOMH.scrollLeft).hide();
				}
			}
		});
	} catch (e) {
		SysCfg.Log.Methods.WriteLog("PanelService.js Componet.TPanelServices.prototype.FunctionsTabPanelHorizontal", e);
	}
}

Componet.TPanelServices.prototype.CreateVertical = function () {
	var _this = this.TParent();
	try {
		_this.Destroy();
		_this.OrderElements();

		_this._ObjectHtml.style.background = _this.BackgroundContainer;

		var AllContainer = new TVclStackPanel(_this._ObjectHtml, 'AllContainer_' + _this._Id, 1, [[12], [12], [12], [12], [12]]);
		AllContainer.Row.This.style.width = "100%"
		var AllContainer_1 = AllContainer.Column[0].This;
		$(AllContainer_1).addClass('container-fluid main-vertical');

		var TabContainer = new Udiv(AllContainer_1, 'Container_' + _this._Id);
		$(TabContainer).addClass('container-v');
		var PanelContainer = new Udiv(AllContainer_1, 'Container_' + _this._Id);
		$(PanelContainer).addClass('init-v tab-content');

		var ScrollUp = new Udiv(TabContainer, '');
		$(ScrollUp).addClass('scroller-v scroller-up-v');
		var IconUp = new TVclImagen(ScrollUp, "");
		IconUp.Src = 'img/img_tab/nav-up(32).png';

		var Wrapper = new Udiv(TabContainer, '');
		$(Wrapper).addClass('wrapper-v');

		var ScrollDown = new Udiv(TabContainer, '');
		$(ScrollDown).addClass('scroller-v scroller-down-v');
		var IconDown = new TVclImagen(ScrollDown, "");
		IconDown.Src = 'img/img_tab/nav-down(32).png';

		var List = VclList(Wrapper, '', _this.Elements.length);
		$(List.ul).addClass('nav nav-tabs list-v');
		$(List.li[0]).addClass('active');

		_this._ElementsDOMV = { containerTab: PanelContainer, list: List, wrapper: Wrapper, scrollUp: ScrollUp, scrollDown: ScrollDown };
		var dropDownList = new Array();
		for (var i = 0; i < List.li.length; i++) {
			var IconTag = new Ua(List.li[i], '', '');
			$(IconTag).attr({ 'href': '#TabPanel_' + i, 'data-toggle': 'tab' });
			IconTag.setAttribute('data-id', i);
			$(IconTag).addClass('element-icon-v');
			var IconBox = new Udiv(IconTag, '');
			$(IconBox).addClass('icon-box-v')
			var IconImage = new TVclImagen(IconBox, "");
			IconImage.Src = _this.Elements[i].Image;
			$(IconImage.This).addClass('icon-img-v');
			var IconTitle = new Uspan(IconBox, '', _this.Elements[i].Title);
			$(IconTitle).addClass('icon-title-v');

			if (_this.Elements[i].Title.length > 18) {
				$(IconTitle).css('padding-top', '8px');
				if (_this.Elements[i].Title.length > 36) {
					$(IconTitle).css('padding-top', '0px');
				}
			}
			else {
				$(IconTitle).css('padding-top', '15px');
			}

			_this.Elements[i]._ElementsDOM = { iconTag: IconTag, img: IconImage, title: IconTitle, This: _this };

			var TabPanel = new Udiv(PanelContainer, 'TabPanel_' + i);
			$(TabPanel).addClass('tab-pane tab-pane-v');
			_this.Elements[i].This = TabPanel; // Creación de This para TabPanel
			if (i == 0) { $(TabPanel).addClass('active'); }

			for (var j = 0; j < _this.Elements[i].ElementsContent.length; j++) {
				var Element_1 = new Udiv(TabPanel, '');
				$(Element_1).addClass('row element-v');

				var Element_2 = new Udiv(Element_1, '');
				$(Element_2).addClass('col-md-9 col-center-v');

				var InfoBoxIcon = new Udiv(Element_2, '');
				$(InfoBoxIcon).addClass('info-box-icon-v');

				var IconBoxIcon = new TVclImagen(InfoBoxIcon, '');
				IconBoxIcon.Src = _this.Elements[i].ElementsContent[j].ContentImage;
				$(IconBoxIcon.This).addClass('img-responsive');

				var InfoBoxTitle = new Uspan(Element_2, '', _this.Elements[i].ElementsContent[j].ContentTitle);
				$(InfoBoxTitle).addClass('info-box-title-v');

				var InfoBoxDescription = new Uspan(Element_2, '', _this.Elements[i].ElementsContent[j].ContentDescription);
				$(InfoBoxDescription).addClass('info-box-text-v');

				var PanelPhrase = new Udiv(Element_2, '');
				$(PanelPhrase).addClass('col-md-12');
				PanelPhrase.innerHTML = _this.Elements[i].ElementsContent[j].ContentPhrase;
				_this.Elements[i].ElementsContent[j].This = PanelPhrase;  // Creación de This para Panel de DropDownPanel

				var VclDropDownPanel = new TVclDropDownPanel(Element_2, "");
				VclDropDownPanel.BoderTopColor = "transparent";
				VclDropDownPanel.BoderAllColor = "transparent";
				VclDropDownPanel.BackgroundColor = "transparent";
				VclDropDownPanel.MarginBottom = "20px";
				VclDropDownPanel.MarginTop = "20px";
				VclDropDownPanel.MarginLeft = "20px";
				VclDropDownPanel.MarginRight = "20px";

				VclDropDownPanel.This[0].This.style.color = _this.Elements[i].ElementsContent[j].ContentTextColor;

				VclDropDownPanel.AddToHeader(InfoBoxIcon);
				VclDropDownPanel.AddToHeader(InfoBoxTitle);
				VclDropDownPanel.AddToHeader(InfoBoxDescription);
				VclDropDownPanel.AddToBody(PanelPhrase);
				VclDropDownPanel.RowBody.This.classList.add("firstMapping");
				_this.Elements[i].ElementsContent[j]._ElementsDOM = { contentImg: IconBoxIcon, contentTitle: InfoBoxTitle, contentDescription: InfoBoxDescription, contentPhrase: PanelPhrase, This: _this };
				VclDropDownPanel.objCallback = { PhraseBody: PanelPhrase.innerHTML, ContentRowBody: VclDropDownPanel.RowBody.This };
				VclDropDownPanel._functionOpen = function () {
					if (_this.Click !== null) {
						_this.Click(this.objCallback);
					}
				}
				IconClick = function (elementVclDropDownPanel, elementIconBoxIcon) {
					elementIconBoxIcon.onClick = function () {
						elementVclDropDownPanel.Image.onClick();
					}
				}
				IconClick(VclDropDownPanel, IconBoxIcon)
			}

			List.li[i].firstChild.onclick = function () {
				for (var k = 0; k < _this._ElementsDOMV.list.li.length; k++) {
					_this._ElementsDOMV.list.li[k].firstChild.style.background = 'transparent';
					_this.Elements[k]._ElementsDOM.title.style.color = _this.Elements[k].TitleColor;
					_this.Elements[k]._ElementsDOM.iconTag.style.borderLeft = 'none';
					_this.Elements[k]._ElementsDOM.iconTag.style.borderTop = 'none';
					_this.Elements[k]._ElementsDOM.iconTag.style.borderBottom = 'none';
				}
				this.style.background = _this.Elements[this.dataset.id].BackgroundActive;
				_this.Elements[this.dataset.id]._ElementsDOM.title.style.color = _this.Elements[this.dataset.id].TitleActiveColor;
				_this._ElementsDOMV.containerTab.style.background = _this.Elements[this.dataset.id].BackgroundActive;
				_this._ElementsDOMV.containerTab.style.borderColor = _this.Elements[this.dataset.id].Border;
				this.style.borderLeft = '1px solid ' + _this.Elements[this.dataset.id].Border;
				this.style.borderTop = '1px solid ' + _this.Elements[this.dataset.id].Border;
				this.style.borderBottom = '1px solid ' + _this.Elements[this.dataset.id].Border;
				this.style.borderRight = '0px';
			}

			List.li[i].firstChild.onmouseover = function () {
				if (this.parentNode.classList[0] != 'active') {
					this.style.background = _this.Elements[this.dataset.id].BackgroundHover;
					_this.Elements[this.dataset.id]._ElementsDOM.title.style.color = _this.Elements[this.dataset.id].TitleHoverColor;
					this.style.borderColor = 'transparent';
				}
			}
			List.li[i].firstChild.onmouseout = function () {
				if (this.parentNode.classList[0] != 'active') {
					this.style.background = 'transparent';
					_this.Elements[this.dataset.id]._ElementsDOM.title.style.color = _this.Elements[this.dataset.id].TitleColor;
				}
			}

			if (i == 0) {
				List.li[i].firstChild.style.background = _this.Elements[i].BackgroundActive;
				_this._ElementsDOMV.containerTab.style.background = _this.Elements[i].BackgroundActive;
				_this.Elements[i]._ElementsDOM.title.style.color = _this.Elements[i].TitleActiveColor;
				_this.Elements[i]._ElementsDOM.iconTag.style.borderLeft = '1px solid ' + _this.Elements[i].Border;
				_this.Elements[i]._ElementsDOM.iconTag.style.borderTop = '1px solid ' + _this.Elements[i].Border;
				_this.Elements[i]._ElementsDOM.iconTag.style.borderBottom = '1px solid ' + _this.Elements[i].Border;
				_this._ElementsDOMV.containerTab.style.borderColor = _this.Elements[i].Border;

				_this.Elements[i]._ElementsDOM.iconTag.style.borderRight = '0px';
			}

		}

		_this.importarStyle(SysCfg.App.Properties.xRaiz + "Componet/PanelServices/Css/PSVertical.css", function () {
			_this.FunctionsTabPanelVertical();
			//fix
			if (typeof _this.InCallBack === 'function') {
				_this.InCallBack();
			}
			//
		}, function (e) {
			SysCfg.Log.Methods.WriteLog("PanelService.js Componet.TPanelServices.prototype.CreateVertical this.importarStyle(" + this.importarStyle + "Componet/PanelServices/Css/PSVertical.css)", e);
		});
	} catch (e) {
		SysCfg.Log.Methods.WriteLog("PanelService.js Componet.TPanelServices.prototype.CreateVertical", e);
	}
}

Componet.TPanelServices.prototype.FunctionsTabPanelVertical = function () {
	var _this = this.TParent();
	try {
		var tam_li_v = $(_this._ElementsDOMV.list.li).outerHeight();
		var tam_v = parseInt($(_this._ElementsDOMV.list.li).length);
		var cont_v = Math.floor(parseInt($(_this._ElementsDOMV.wrapper).outerHeight()) / tam_li_v);
		var aux_v = cont_v;
		this.heightOfList_v = function () {
			var itemsHeight_v = 0;
			$(_this._ElementsDOMV.list.li).each(function () {
				var itemHeight_v = $(this).outerHeight();
				itemsHeight_v += itemHeight_v;
			});
			return itemsHeight_v;
		};
		this.getActive_v = function () {
			for (var i = 0; i < $(_this._ElementsDOMV.list.li).length ; i++) {
				if ($(_this._ElementsDOMV.list.li)[i].className == 'active') {
					return i + 1;
				}
			}
		}
		this.getTopPosi_v = function () {
			return $(_this._ElementsDOMV.list.ul).position().top;
		};
		this.reAdjust_v = function () {
			$(_this._ElementsDOMV.list.ul).css('top', 0);
			cont_v = Math.floor(parseInt($(_this._ElementsDOMV.wrapper).outerHeight()) / tam_li_v);
			aux_v = cont_v;
			if (_this.getActive_v() > cont_v) {
				$(_this._ElementsDOMV.list.ul).css('top', ((cont_v * tam_li_v) - (_this.getActive_v() * tam_li_v)));
				cont_v = _this.getActive_v();
			}
			if (($(_this._ElementsDOMV.wrapper).outerHeight()) < _this.heightOfList_v()) {
				if (tam_v > cont_v) {
					$(_this._ElementsDOMV.scrollDown).show();
				}
				else {
					var temp_v = $(_this._ElementsDOMV.wrapper).outerHeight() - _this.heightOfList_v();
					$(_this._ElementsDOMV.list.ul).stop().animate({ top: temp_v }, 'fast');
					$(_this._ElementsDOMV.scrollDown).hide();
				}
			}
			else {
				$(_this._ElementsDOMV.scrollDown).hide();
			}
			if (_this.getTopPosi_v() < 0) {
				$(_this._ElementsDOMV.scrollUp).show();
			}
			else {
				$(_this._ElementsDOMV.scrollUp).hide();
			}
		}
		_this.reAdjust_v();
		$(window).resize(function(){_this.reAdjust_v();});
		$(_this._ElementsDOMV.scrollDown).click(function () {
			if (cont_v < tam_v) {
				$(_this._ElementsDOMV.scrollUp).show();
				$(_this._ElementsDOMV.scrollDown).show();
				$(_this._ElementsDOMV.list.ul).animate({ top: "-=" + tam_li_v }, 'fast');
				cont_v++;
				if (cont_v == tam_v) {
					var temp_v = $(_this._ElementsDOMV.wrapper).outerHeight() - _this.heightOfList_v();
					$(_this._ElementsDOMV.list.ul).animate({ top: temp_v }, 'fast');
					$(_this._ElementsDOMV.scrollDown).hide();
				}
			}

		});
		$(_this._ElementsDOMV.scrollUp).click(function () {
		if (aux_v < cont_v) {
			$(_this._ElementsDOMV.scrollDown).show();
			$(_this._ElementsDOMV.scrollUp).show();
			$(_this._ElementsDOMV.list.ul).animate({ top: "+=" + tam_li_v }, 'fast');
			cont_v--;
			if (aux_v == cont_v) {
				$(_this._ElementsDOMV.list.ul).animate({ top: 0 }, 'fast');
				$(_this._ElementsDOMV.scrollUp).hide();
			}
		}
	});
	} catch (e) {
		SysCfg.Log.Methods.WriteLog("PanelService.js Componet.TPanelServices.prototype.FunctionsTabPanelVertical", e);
	}
}

Componet.TPanelServices.prototype.OrderElements = function(){
	var _this = this.TParent();
	try {
		_this.Elements.sort(compare);
		function compare(a,b){ return a.Position - b.Position; }
	
		for(var i = 0 ; i < _this.Elements.length ; i++){
			_this.Elements[i].ElementsContent.sort(compare1);
			function compare1(a,b){ return a.ContentPosition-b.ContentPosition;}
		}
	} catch (e) {
		SysCfg.Log.Methods.WriteLog("PanelService.js Componet.TPanelServices.prototype.OrderElements", e);
	}
}

Componet.TPanelServices.prototype.Destroy = function(){
	var _this = this.TParent();
	$(_this._ObjectHtml).html('');
}

Componet.TPanelServices.prototype.AddElementTab = function (inElement) {
	try {
		var _this = this.TParent();
		for (var i = 0; i < inElement.length; i++) {
			var Element = new Componet.TElements.TPanelServicesElementTab();
			Element.Position = inElement[i]["position"];
			Element.Id = inElement[i]["id"];
			Element.Image = inElement[i]["image"];
			Element.Title = inElement[i]["title"];
			_this.TPanelServicesElementsAdd(Element);
			for (var j = 0; j < inElement[i].content.length; j++) {
				var ElementContent = new Componet.TElements.TPanelServicesElementTabContent();
				ElementContent.ContentPosition = inElement[i].content[j]["contentPosition"];
				ElementContent.ContentId = inElement[i].content[j]["contentId"];
				ElementContent.ContentImage = inElement[i].content[j]["contentImage"];
				ElementContent.ContentTitle = inElement[i].content[j]["contentTitle"];
				ElementContent.ContentDescription = inElement[i].content[j]["contentDescription"];
				ElementContent.ContentPhrase = inElement[i].content[j]["contentPhrase"];
				_this.Elements[_this.Elements.length-1].TPanelServiceElementsContentAdd(ElementContent);
			}
		}
		_this.Load();
	} catch (e) {
		SysCfg.Log.Methods.WriteLog("PanelService.js Componet.TPanelServices.prototype.AddElementTab", e);
	}
}

Componet.TPanelServices.prototype.importarStyle = function (nombre, onSuccess, onError) {
	var style = document.createElement("link");
	style.rel = "stylesheet";
	style.type = "text/css";
	style.href = nombre;
	var s = document.head.appendChild(style);
	s.onload = onSuccess;
	s.onerror = onError;
}