Componet.Scheduler.TScheduleEvents = function (plugin, objectScheduler) {
    this.TParent = function () {
        return this;
    }.bind(this);
    this.Scheduler = objectScheduler;
    this.PluginScheduler = plugin;
    var _Scheduler = objectScheduler;
    Object.defineProperty(this, 'OnAfterEventDisplay', {
        /*
          Type: Void
          Parameters: ( id de cadena , vista de cadena )
          ->(String) Id del Evento
          ->(String) el nombre de una vista utilizada para mostrar el evento
          Description:se dispara cuando el planificador cambia las vistas, los d�as, la hora, etc. para mostrar el evento especificado por el m�todo 'showEvent' y los incendios DESPU�S de que se muestre el evento
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onAfterEventDisplay", function (id, view) {
                _Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { Id: id, View: view }
                });
            });
        }
    });
    Object.defineProperty(this, 'OnAfterFolderToggle', {
        /*
          Type: Void
          Parameters: ( objeto | secci�n booleana , boolean isOpen , boolean allSections )
          ->(Objeto / Boolean) el objeto de configuraci�n de la rama abierta / cerrada.Toma el valor verdadero, si todas las ramas se cerraron / abrieron a la vez mediante los m�todos closeAllSections () / openAllSections ().
          ->(Boolean) indica si la rama fue abierta ( verdadera ) o cerrada ( falsa )
          ->(Boolean) oma el valor real , si todas las ramas de los �rboles se cerraron / abrieron a la vez mediante los m�todos closeAllSections () / openAllSections ()
          Description:dispara despu�s de que se abri� o cerr� la rama de un �rbol (solo en la vista L�nea de tiempo, modo '�rbol')
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onAfterFolderToggle", function (section, isOpen, allSections) {
                _Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { Section: section, IsOpen: isOpen, AllSections: allSections }
                });
            });
        }
    });
    Object.defineProperty(this, 'OnAfterCloseModalBox', {
        /*
          Type: Void
          Parameters: ( string eventId )
          ->(String) Id del Evento
          Description:dispara despu�s de que se cierra el formulario de evento emergente
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onAfterQuickInfo", function (eventId) {
                _Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { EventId: eventId }
                });
            });
        }
    });
    Object.defineProperty(this, 'OnAfterSchedulerResize', {
        /*
          Type: Void
          Parameters: none
          Description:dispara despu�s de que el programador ha cambiado su tama�o y el �rea de datos fue repintada
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("OnAfterSchedulerResize", function () {
                _Value({ ObjScheduler: _Scheduler });
            });
        }
    });
    Object.defineProperty(this, 'OnBeforeCollapse', {
        /*
          Type: Void
          Parameters: none
          return: define si la acci�n predeterminada del evento se activar� ( verdadero ) o se cancelar� ( falso )
          Description: se dispara cuando un usuario hace clic en el �cono expandir para cambiar el tama�o del planificador de "pantalla completa" a original.
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onBeforeCollapse", function () {
                return (_Value({ ObjScheduler: _Scheduler }));
            });
        }
    });
    Object.defineProperty(this, 'OnBeforeDrag', {
        /*
          Type: Void
          Parameters: ( cadena de Identificaci�n , cadena modo , Evento e )
          ->(String) id del evento
          ->(String) el modo de arrastrar: "mover", "cambiar el tama�o" o "crear"
          ->(Evento) un objeto de evento nativo
          return: define si la acci�n predeterminada del evento se activar� ( verdadero ) o se cancelar� ( falso )
          Description: se dispara cuando el usuario inicia la operaci�n de arrastrar / cambiar tama�o (versi�n 2.1+)
          El evento se dispara cuando el usuario hace clic dentro del planificador en el elemento que puede arrastrarse.
          Para el modo "crear", no se proporciona el valor de identificaci�n (a�n no se ha creado un nuevo evento).
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onBeforeDrag", function (id, mode, e) {
                return _Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { EventId: id, Mode: mode, EventNative: e }
                });
            });
        }
    });
    Object.defineProperty(this, 'OnBeforeEventChanged', {
        /*
          Type: Void
          Parameters: ( object ev , Event e , boolean is_new , object original )
          ->(Objeto) el objeto de configuraci�n de la rama abierta / cerrada.Toma el valor verdadero, si todas las ramas se cerraron / abrieron a la vez mediante los m�todos closeAllSections () / openAllSections ().
          ->(EventoNativo) un objeto de evento nativo
          ->(Boolean) devuelve 'verdadero', si el usuario cambia un evento nuevo. 'falso' - si el evento editado ya existe
          ->(Boolean) el objeto de datos del evento antes de los cambios
          return: define si la acci�n predeterminada del evento se activar� ( verdadero ) o se cancelar� ( falso )
          Description: se dispara cuando el evento ha sido cambiado por drag-n-drop, pero los cambios a�n no se han guardado.
          El evento ocurre cuando se agrega un nuevo "evento" o uno existente se modifica mediante la acci�n de arrastrar y soltar.
          Tenga en cuenta que el primer par�metro de la funci�n del manejador toma el objeto del elemento de datos, no el ID del elemento de datos (porque los elementos de datos reci�n creados pueden no tener ID a�n).
          El evento no modificado ser�a un objeto vac�o en caso de crear nuevos elementos de datos.
          El evento es bloqueable: la devoluci�n falsa del controlador evitar� la actualizaci�n de datos.
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onBeforeEventChanged", function (ev, e, is_new, originals) {
                return (_Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { ObjEventNew: ev, EventNative: e, IsNew: is_new, ObjEventOriginal: originals }
                }))
            });
        }
    });
    Object.defineProperty(this, 'OnBeforeEventCreated', {
        /*
          Type: Void
          Parameters: ( Evento e )
          ->(EventoNativo) un objeto de evento nativo
          return: define si la acci�n predeterminada del evento se activar� ( verdadero ) o se cancelar� ( falso )
          Description: se dispara cuando el usuario crea un nuevo evento arrastrando el cursor sobre el programador
          Tenga en cuenta que el evento se activar� solo si la opci�n de configuraci�n drag_create est� habilitada.
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onBeforeEventCreated", function (e) {
                return _Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { EventNative: e }
                });
            });
        }
    });
    Object.defineProperty(this, 'OnBeforeEventDelete', {
        /*
          Type: Void
          Parameters: ( id de cadena , ev de objeto )
          ->(String) la identificaci�n del evento
          ->(Objeto) el objeto de datos del evento
          return: define si la acci�n predeterminada del evento se activar� ( verdadero ) o se cancelar� ( falso )
          Description: se dispara cuando el usuario crea un nuevo evento arrastrando el cursor sobre el programador
          El evento es bloqueable. Devuelve falso para cancelar el procesamiento predeterminado.
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onBeforeEventDelete", function (id, ev) {
                return (_Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { EventId: id, ObjectEvent: ev }
                }));
            });
        }
    });
    Object.defineProperty(this, 'OnBeforeEventDisplay', {
        /*
          Type: Void
          Parameters: ( string id , string view )
          ->(String) la identificaci�n del evento
          ->(String) el nombre de una vista utilizada para mostrar el evento
          return: define si la acci�n predeterminada del evento se activar� ( verdadero ) o se cancelar� ( falso )
          Description: se dispara cuando se llama al m�todo 'showEvent' para mostrar un evento espec�fico y se dispara ANTES de que se muestre el evento
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("OnBeforeEventDisplay", function (id, view) {
                return (_Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { EventId: id, View: view }
                }));
            });
        }
    });
    Object.defineProperty(this, 'OnBeforeEventDragIn', {
        /*
          Type: Void
          Parameters: ( string id , Evento e )
          ->(String) la identificaci�n del evento
          ->(Event) un objeto de evento nativo
          return: define si la acci�n predeterminada del evento se activar� ( verdadero ) o se cancelar� ( falso )
          Description: dispara antes de que un evento arrastrado se mueva sobre el programador
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onBeforeEventDragIn", function (id, e) {
                return (_Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { EventId: id, EventNative: e }
                }));
            });
        }
    });
    Object.defineProperty(this, 'OnBeforeEventDragOut', {
        /*
          Type: Void
          Parameters: ( id cadena , objeto ev , Evento e )
          ->(String) la identificaci�n del evento
          ->(Object) el objeto de datos del evento
          ->(Event) un objeto de evento nativo
          return: define si la acci�n predeterminada del evento se activar� ( verdadero ) o se cancelar� ( falso )
          Description: dispara antes de que el evento arrastrado se salga del programador
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onBeforeEventDragOut", function (id, ev, e) {
                return (_Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { EventId: id, ObjectEvent: ev, EventNative: e }
                }));
            });
        }
    });
    Object.defineProperty(this, 'OnBeforeExpand', {
        /*
          Type: Void
          Parameters: none
          return: define si la acci�n predeterminada del evento se activar� ( verdadero ) o se cancelar� ( falso )
          Description: se dispara cuando un usuario hace clic en el �cono expandir para cambiar el tama�o del planificador del original a "pantalla completa".
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onBeforeExpand", function () {
                return (_Value({ ObjScheduler: _Scheduler }));
            });
        }
    });
    Object.defineProperty(this, 'OnBeforeExternalDragIn', {
        /*
          Type: Void
          Parameters: ( HTMLElement source , object dhtmlx , HTMLElement tArea , HTMLElement tNode , Event e )
          ->(HTMLElement) un elemento HTML que se arrastrar� al programador
          ->(Object) un objeto global DHTMLX
          ->(HTMLElement) un objeto HTML del �rea de datos del planificador
          ->(HTMLElement) el objeto HTML del planificador de destino (una columna en la vista D�a, una secci�n en la vista L�nea de tiempo, etc.)
          ->(EventNative) un objeto de evento nativo
          return: define si la acci�n predeterminada del evento se activar� ( verdadero ) o se cancelar� ( falso )
          Description: se dispara antes de que alg�n elemento comience a arrastrarse al planificador desde un componente DHTMLX externo (solo con la extensi�n dnd habilitada)
          El evento es bloqueable. Devuelve falso y el elemento externo no se arrastrar� al planificador.
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onBeforeExternalDragIn", function (source, dhtmlx, tArea, tNode, e) {
                return (_Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { Source: source, ElementDhtmlx: dhtmlx, AreaHtml: tArea, NodeHtml: tNode, EventNAtive: e }
                }));
            });
        }
    });
    Object.defineProperty(this, 'OnBeforeFolderToggle', {
        /*
          Type: Void
          Parameters: ( objeto | secci�n booleana , boolean isOpen , boolean allSections )
          ->(Object Boolean) el objeto de configuraci�n de la rama para abrir / cerrar.Toma el valor verdadero , si todas las ramas se cerrar�n / abrir�n a la vez mediante los m�todos closeAllSections () / openAllSections ().
          ->(Boolean) indica si la rama se abrir� ( verdadera ) o cerrada ( falsa )
          ->(Boolean) toma el valor verdadero , si todas las ramas de los �rboles se cerrar�n / abrir�n a la vez mediante los m�todos closeAllSections () / openAllSections (), false - si solo se abrir� / cerrar� una rama.
          return: define si la acci�n predeterminada del evento se activar� ( verdadero ) o se cancelar� ( falso )
          Description: incendios antes de que la rama de un �rbol se abra o se cierre (la vista L�nea de tiempo, modo '�rbol' solamente)
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onBeforeFolderToggle", function (section, isOpen, allSections) {
                return (_Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { Section: section, IsOpen: isOpen, AllSections: allSections }
                }));
            });
        }
    });
    Object.defineProperty(this, 'OnBeforeModalbox', {
        /*
          Type: Void
          Parameters: ( string id )
          ->(String) la identificaci�n del evento
          return: define si la acci�n predeterminada del evento se activar� ( verdadero ) o se cancelar� ( falso )
          Description: se dispara inmediatamente antes de que el usuario abra el lightbox (editar formulario)
          El evento es bloqueable. Devuelve falso para cancelar el procesamiento predeterminado (apertura del lightbox).
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onBeforeLightbox", function (id) {
                return (_Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { EventId: id }
                }));
            });
        }
    });
    Object.defineProperty(this, 'OnBeforeSectionRender', {
        /*
          Type: Void
          Parameters: ( cadena modo , objeto secci�n , objeto l�nea de tiempo )
          ->(String) el modo de l�nea de tiempo: 'celda', 'barra' o '�rbol'
          ->(Object)el objeto de secci�n con las propiedades 'clave' y 'etiqueta' especificadas en la matriz 'y_unit' del objeto de configuraci�n de la l�nea de tiempo (por ejemplo, {clave: 1, etiqueta: "James Smith"})
          ->(Object) el objeto de configuraci�n Timeline
          return: define si la acci�n predeterminada del evento se activar� ( verdadero ) o se cancelar� ( falso )
          Description:se dispara antes de que se haya configurado una sola secci�n de la l�nea de tiempo, pero a�n no se haya procesado (solo la vista de la l�nea de tiempo)
          El evento se puede usar para personalizar las secciones de la l�nea de tiempo.
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onBeforeSectionRender", function (mode, section, timeline) {
                return (_Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { Mode: mode, Section: section, TimeLine: timeline }
                }));
            });
        }
    });
    Object.defineProperty(this, 'OnBeforeTodayDisplayed', {
        /*
          Type: Void
          Parameters: none
          return: define si la acci�n predeterminada del evento se activar� ( verdadero ) o se cancelar� ( falso )
          Description:se dispara cuando el usuario hace clic en el bot�n 'Hoy' en el programador
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onBeforeTodayDisplayed", function () {
                return (_Value({ ObjScheduler: _Scheduler }));
            });
        }
    });
    Object.defineProperty(this, 'OnBeforeTooltip', {
        /*
          Type: Void
          Parameters: ( string id )
          ->(String) la identificaci�n de un elemento de datos que se mostrar� en la informaci�n sobre herramientas para
          return: define si la acci�n predeterminada del evento se activar� ( verdadero ) o se cancelar� ( falso )
          Description: se dispara antes de que se muestre la informaci�n sobre herramientas para un elemento de datos (solo con la extensi�n 'tooltip' habilitada)
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onBeforeTooltip", function (id) {
                return (_Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { ElementDataId: id }
                }));
            });
        }
    });
    Object.defineProperty(this, 'OnBeforeViewChange', {
        /*
          Type: Void
          Parameters:  (string old_mode , object old_date , string mode , object date )
          ->(String) la vista actualmente activa
          ->(Object) la fecha actualmente activa
          ->(String) la nueva vista
          ->(Object) la nueva fecha
          return: define si la acci�n predeterminada del evento se activar� ( verdadero ) o se cancelar� ( falso )
          Description:se dispara antes de que el usuario cambie la vista actual a alguna otra
          El evento es bloqueable. Devuelve falso y el planificador dejar� abierta la vista actual.
          El evento tambi�n se dispara cuando el planificador se est� procesando inicialmente en la p�gina. En este caso, los par�metros old_mode y old_date est�n subfinados.
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onBeforeViewChange", function (old_mode, old_date, mode, date) {
                return (_Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { OldView: old_mode, OldDate: old_date, NewView: mode, NewDate: date }
                }));
            });
        }
    });
    Object.defineProperty(this, 'OnCellClick', {
        /*
          Type: Void
          Parameters: 
          ->(Number)el �ndice de columna de la celda cliqueada (numeraci�n basada en cero)
          ->(Number)el �ndice de fila de la celda cliqueada (numeraci�n basada en cero)
          ->(Object)un objeto de fecha de la marca de tiempo de inicio de la celda cliqueada
          ->(Matriz)una matriz de objetos de elementos de datos resid�a en la celda clicada
          ->(Event)un objeto de evento nativo
          Description:se dispara cuando el usuario hace un solo clic en una celda (solo la vista de la l�nea de tiempo)
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onCellClick", function (x_ind, y_ind, x_val, y_val, e) {
                _Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { IndeColumn: x_ind, IndexRow: y_ind, ObjDateColumn: x_val, ArrayRow: y_val, EventNative: e }
                });
            });
        }
    });
    Object.defineProperty(this, 'OnCellDblClick', {
        /*
          Type: Void
          Parameters: 
          ->(Number)el �ndice de columna de la celda cliqueada (numeraci�n basada en cero)
          ->(Number)el �ndice de fila de la celda cliqueada (numeraci�n basada en cero)
          ->(Object)un objeto de fecha de la marca de tiempo de inicio de la celda cliqueada
          ->(Matriz)una matriz de objetos de elementos de datos resid�a en la celda clicada
          ->(Event)un objeto de evento nativo
          Description:se dispara cuando el usuario hace un solo clic en una celda (solo la vista de la l�nea de tiempo)
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onCellDblClick", function (x_ind, y_ind, x_val, y_val, e) {
                _Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { IndeColumn: x_ind, IndexRow: y_ind, ObjDateColumn: x_val, ArrayRow: y_val, EventNative: e }
                });
            });
        }
    });
    Object.defineProperty(this, 'OnClearAll', {
        /*
          Type: Void
          Parameters: none
          Description:incendios despu�s de que se borraron los datos en el programador.El evento se invoca desde el m�todo clearAll .
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onClearAll", function () {
                _Value({ ObjScheduler: _Scheduler });
            });
        }
    });
    Object.defineProperty(this, 'OnClick', {
        /*
          Type: Void
          Parameters: ( string id , Evento e )
          ->(String) la identificaci�n del evento
          ->(EventNative) un objeto de evento nativo
          return: define si la acci�n predeterminada del evento se activar� ( verdadero ) o se cancelar� ( falso )
          Description:se dispara cuando el usuario hace clic con el bot�n izquierdo del mouse en un evento
          El evento es bloqueable. Si el manejador devuelve un valor no verdadero, la reacci�n predeterminada se bloquear� (de forma predeterminada, aparece la barra de selecci�n).
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onClick", function (id, e) {
                return (_Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { EventId: id, EventNative: e }
                }));
            });
        }
    });
    Object.defineProperty(this, 'OnCollapse', {
        /*
          Type: Void
          Parameters: none
          Description:se dispara cuando un usuario hace clic en el �cono expandir para cambiar el tama�o del planificador de "pantalla completa" a original.
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onCollapse", function () {
                _Value({ ObjScheduler: _Scheduler });
            });
        }
    });
    Object.defineProperty(this, 'OnConfirmedBeforeEventDelete', {
        /*
          Type: Void
          Parameters: ( string id , Evento e )
          ->(String) la identificaci�n del evento
          ->(Event) un objeto de evento
          return: define si la acci�n predeterminada del evento se activar� ( verdadero ) o se cancelar� ( falso )
          Description:dispara despu�s de que el usuario hace clic en el bot�n Eliminar y confirma la eliminaci�n (en la barra de eventos o en la ventana de detalles)
          El evento es bloqueable. Devuelve falso para cancelar el procesamiento predeterminado.
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onConfirmedBeforeEventDelete", function (id, event) {
                return (_Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { EventId: id, EventNative: event }
                }));
            });
        }
    });
    Object.defineProperty(this, 'OnContextMenu', {
        /*
          Type: Void
          Parameters: ( string id , Evento e )
          ->(String) la identificaci�n del evento
          ->(EventNative) un objeto de evento nativo
          Description:se dispara cuando el usuario llama al men� contextual haciendo clic con el bot�n derecho dentro del programador
          si el usuario hace clic en un evento, el controlador tomar� la identificaci�n del evento; de lo contrario, ser� nulo.
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onContextMenu", function () {
                _Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { EventId: id, EventNative: e }
                });
            });
        }
    });
    Object.defineProperty(this, 'OnDataRender', {
        /*
          Type: Void
          Parameters: none
          Description:incendios despu�s de que se hayan procesado los datos en la p�gina
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onDataRender", function () {
                _Value({ ObjScheduler: _Scheduler });
            });
        }
    });
    Object.defineProperty(this, 'OnDblClick', {
        /*
          Type: Void
          Parameters: ( string id , Evento e )
          ->(String) la identificaci�n del evento
          ->(EventNative) un objeto de evento nativo
          Description:se dispara cuando el usuario hace doble clic en un evento
          El evento es bloqueable. Devuelve falso para cancelar el comportamiento predeterminado.
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onDblClick", function (id, e) {
                return (_Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { EventId: id, EventNative: e }
                }));
            });
        }
    });
    Object.defineProperty(this, 'OnDragEnd', {
        /*
          Type: Void
          Parameters: ( cadena de Identificaci�n , cadena modo , Evento e )
          ->(String) la identificaci�n del evento
          ->(String) el modo de arrastrar: "mover", "cambiar el tama�o" o "crear"
          ->(EventNative) un objeto de evento nativo
          Description: se dispara cuando la operaci�n de arrastrar / cambiar tama�o finaliza
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onDragEnd", function (id, mode, e) {
                return (_Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { EventId: id, Mode: mode, EventNative: e }
                }));
            });
        }
    });
    Object.defineProperty(this, 'OnEmptyClick', {
        /*
          Type: Void
          Parameters: ( fecha del objeto , Evento e )
          ->(Object) una fecha que corresponde al punto en el que el usuario hace clic
          ->(Date) un objeto de evento nativo
          Description:se dispara cuando el usuario hace clic en un espacio vac�o en el programador (no en eventos)
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onEmptyClick", function (date, e) {
                return (_Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { Date: date, EventNative: e }
                }));
            });
        }
    });
    Object.defineProperty(this, 'OnEventAdd', {
        /*
          Type: Void
          Parameters: 
          ->(String) la identificaci�n del evento
          ->(Object) 	el objeto del evento
          Description:se dispara cuando el usuario agrega un nuevo evento al programador
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("OnEventAdded", function (id, ev) {
                return(_Scheduler._EventAdd(_Value, id,ev));
            });
        }
    });
    Object.defineProperty(this, 'OnEventCancel', {
        /*
          Type: Void
          Parameters: ( string id , boolean flag ) 
          ->(String) Id del evento
          ->(Boolean) devuelve 'verdadero', si el usuario est� cancelando un nuevo evento,'falso' - si el evento editado ya existe
          Description:se dispara cuando el usuario hace clic en el bot�n "Cancelar" en el lightbox (formulario de edici�n)
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onEventCancel", function (id, flag) {
                _Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { EventId: id, Flag: flag }
                });
            });
        }
    });
    Object.defineProperty(this, 'OnEventChanged', {
        /*
          Type: Void
          Parameters: ( id de cadena , ev de objeto )
          ->(String) id del evento
          ->(Object) el objeto del evento
          Description:ocurre despu�s de que el usuario edit� un evento y guard� los cambios (despu�s de hacer clic en los botones editar y guardar en la barra del evento o en la ventana de detalles)
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onEventChanged", function (id, ev) {
                return (_Scheduler._EventEdit(_Value, id, ev));
            });
        }
    });
    Object.defineProperty(this, 'OnEventCollision', {
        /*
          Type: Void
          Parameters: ( ev de objeto , evs de matriz )
          ->(Object) el objeto del evento
          ->(Object) una colecci�n de objetos de eventos que ya ocupan el mismo espacio de tiempo
          return: define si la acci�n predeterminada del evento se activar� ( verdadero ) o se cancelar� ( falso )
          Description:se dispara cuando el usuario intenta crear un nuevo evento (o modificar uno existente) dentro de un intervalo de tiempo ya ocupado
          La devoluci�n verdadera desde la funci�n del controlador bloquea el evento para que no se agregue / edite. Devoluci�n falsa : permite la colisi�n, es decir, agregar / editar eventos.
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onEventCollision", function (ev, evs) {
                return (_Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { ObjectEvent: ev, ArrayEvents: evs }
                }));
            });
        }
    });
    Object.defineProperty(this, 'OnEventCopied', {
        /*
          Type: Void
          Parameters: ( ev de objeto )
          ->(Object) el objeto del evento copiado
          Description:se dispara cuando el usuario presiona el comando de teclado 'CTRL + C' (solo con la extensi�n de 'navegaci�n de teclado' habilitada)
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onEventCopied", function (ev) {
                _Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { ObjectEvent: ev }
                });
            });
        }
    });
    Object.defineProperty(this, 'OnEventCreated', {
        /*
          Type: Void
          Parameters: ( string id , Evento e )
          ->(String) id del evento
          ->(EventNative) un objeto de evento nativo
          Description:se dispara cuando el usuario comienza a crear un nuevo evento (haciendo doble clic o arrastrando)
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onEventCreated", function (id, e) {
                _Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { EventId: id, EventNative: e }
                });
            });
        }
    });
    Object.defineProperty(this, 'OnEventCut', {
        /*
          Type: Void
          Parameters: ( ev de objeto )
          ->(Object) el objeto del evento copiado
          Description:se dispara cuando el usuario presiona el comando de teclado 'CTRL + X' (solo con la extensi�n de 'navegaci�n de teclado' habilitada)
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onEventCut", function (ev) {
                _Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { ObjectEvent: ev }
                });
            });
        }
    });
    Object.defineProperty(this, 'OnEventDelete', {
        /*
          Type: Void
          Parameters: ( id de cadena , ev de objeto )
          ->(String) id del evento
          ->(Object) Objecto del evento
          Description:dispara despu�s de que se elimin� el evento especificado
          El evento se activar� independientemente de si se utiliza o no la biblioteca de DataProcessor.
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onEventDeleted", function (id, ev) {
                if (!_Scheduler.targetEventDelete) {
                    return (_Scheduler._EventDelete(_Value, id, ev));
                }
            });
        }
    });
    Object.defineProperty(this, 'OnEventDrag', {
        /*
          Type: Void
          Parameters: ( cadena ID , modo cadena , Evento e )
          ->(String) id del evento
          ->(String) el modo de arrastrar: "mover", "cambiar tama�o" o "tama�o nuevo" (crear nuevos eventos)
          ->(EventNative) un objeto de evento nativo
          return: define si la acci�n predeterminada del evento se activar� ( verdadero ) o se cancelar� ( falso )
          Description:se dispara cuando el usuario arrastra / cambia el tama�o de los eventos en el programador
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onEventDrag", function (id, mode, e) {
                return (_Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { EventId: id, Mode: mode, EventNative: e }
                }));
            });
        }
    });
    Object.defineProperty(this, 'OnEventDragIn', {
        /*
          Type: Void
          Parameters: ( string id , Evento e )
          ->(String) id del evento
          ->(EventNative) un objeto de evento nativo
          Description:se dispara cuando un evento arrastrado se mueve al programador
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onEventDragIn", function (id, e) {
                _Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { EventId: id, EventNative: e }
                });
            });
        }
    });
    Object.defineProperty(this, 'OnEventDragOut', {
        /*
          Type: Void
          Parameters: ( string id , Evento e )
          ->(String) id del evento
          ->(EventNative) un objeto de evento nativo
          Description:se dispara cuando un evento arrastrado se mueve fuera del planificador
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onEventDragOut", function (id, e) {
                _Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { EventId: id, EventNative: e }
                });
            });
        }
    });
    Object.defineProperty(this, 'OnEventDropOut', {
        /*
          Type: Void
          Parameters: ( id. de cadena , objeto ev , objeto de , Evento e ) {...};
          ->(String) id del evento
          ->(Object) objeto evento
          ->(Object)el programador de destino (nulo, si se coloca en un �rea vac�a)
          ->(EventNative)un objeto de evento nativo
          Description:se dispara cuando un evento arrastrado se coloca en el �rea fuera del programador
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onEventDropOut", function (id, original, to, e) {
                _Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { EventId: id, ObjectEvent: original, DestinyEvent: to, EventNAtive: e }
                });
            });
        }
    });
    Object.defineProperty(this, 'OnEventIdChange', {
        /*
          Type: Void
          Parameters: ( string old_id , string new_id )
          ->(String) old id event
          ->(String) new id event
          Description:se dispara cuando se cambia el id de un evento
          Normalmente, el evento ocurre despu�s de recibir la confirmaci�n de la operaci�n de inserci�n (cambiando la ID del lado del cliente por la ID de DB)
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onEventIdChange", function (old_id, new_id) {
                _Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { EventId: old_id, NewEventId: new_id }
                });
            });
        }
    });
    Object.defineProperty(this, 'OnEventLoading', {
        /*
          Type: Void
          Parameters: ( object ev )
          ->(Object) el objeto de evento (el objeto de un elemento de datos)
          return: define si la acci�n predeterminada del evento se activar� ( verdadero ) o se cancelar� ( falso )
          Description:se dispara cuando se carga un evento desde la fuente de datos
          El evento es bloqueable. Devuelve falso y el elemento de datos no se cargar� en el planificador.
          El evento se llama para cada elemento de datos en la fuente de datos.
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onEventLoading", function (ev) {
                return (_Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { ObjectEvent: ev }
                }));
            });
        }
    });
    Object.defineProperty(this, 'OnEventPasted', {
        /*
          Type: Void
          Parameters:  ( boolean isCopy , objeto pasted_ev , object original_ev )
          ->(Boolean) indica si el evento fue copiado o cortado antes de pegarlo. El verdadero valor 'dice' que el evento fue copiado
          ->(Object)el objeto del nuevo elemento de datos (el evento que se crea despu�s de pegar)
          ->(Object)el objeto del elemento de datos original (el evento que fue copiado / cortado)
          Description:se dispara cuando el usuario presiona el comando de teclado 'CTRL + V'.La extensi�n 'navegaci�n de teclado' debe estar habilitada.
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onEventPasted", function (isCopy, pasted_ev, original_ev) {
                _Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { IsCopy: isCopy, PastedEvent: pasted_ev, OldObjectEvent: original_ev }
                });
            });
        }
    });
    Object.defineProperty(this, 'OnEventSave', {
        /*
          Type: Void
          Parameters: ( id. de cadena , ev de objeto , fecha is_new )
          ->(String) id del evento
          ->(Object) un objeto de evento intermedio que contiene los valores de la caja de luz
          ->(Date) devuelve la fecha de creaci�n del evento (es decir, la fecha actual), si el usuario est� guardando un nuevo evento. null - si el evento para guardar ya existe
          return: define si la acci�n predeterminada del evento se activar� ( verdadero ) o se cancelar� ( falso )
          Description:se dispara cuando el usuario hace clic en el bot�n 'guardar' en el lightbox (formulario de edici�n)
          El evento es bloqueable y se puede usar para validaci�n. Devuelve falso para cancelar el procesamiento predeterminado.
          Cuando se desencadena el evento, los valores establecidos en el lightbox no se han aplicado todav�a al evento original y scheduler.getEvent(id)le devolver�n una instancia no modificada.
          El objeto 'ev' contendr� solo los valores establecidos por el lightbox, es decir, si el lightbox tiene solo 1 entrada, el objeto 'ev' tendr� solo 1 propiedad definida
          scheduler.attachEvent("onEventSave",function(id,ev,is_new){
            if (!ev.text) {
                alert("Text must not be empty");
                return false;
            }
            if (!ev.text.length<20) {
                alert("Text too small");
                return false;
            }
            return true;
          })
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onEventSave", function (id, ev, is_new) {
                return (_Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { EventId: id, ObjectEvent: ev, DateNewEvent: is_new }
                }));
            });
        }
    });
    Object.defineProperty(this, 'OnExpand', {
        /*
          Type: Void
          Parameters:none 
          Description:se dispara cuando un usuario hace clic en el �cono expandir para cambiar el tama�o del planificador del original a "pantalla completa".
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onExpand", function () {
                _Value({ ObjScheduler: _Scheduler });
            });
        }
    });
    Object.defineProperty(this, 'OnExternalDragIn', {
        /*
          Type: Void
          Parameters: ( string id , fuente del objeto , Evento e )
          -> (String) la identificaci�n del elemento de datos
          -> (Object) el elemento HTML fuente que se arrastr� al planificador
          -> (EventNative) un objeto de evento nativo
          return: define si la acci�n predeterminada del evento se activar� ( verdadero ) o se cancelar� ( falso )
          Description:se dispara cuando se arrastran algunos datos al planificador desde un componente DHTMLX externo (solo con la extensi�n dnd activada)
          El evento se puede usar para personalizar eventos reci�n creados (que son el resultado de operaciones de arrastre).
          El evento es bloqueable. Devuelve falso , y arrastrando no producir� un nuevo evento
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onExternalDragIn", function (id, source, e) {
                return (_Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { DataId: id, Source: source, EventNative: e }
                }));
            });
        }
    });
    Object.defineProperty(this, 'OnModalbox', {
        /*
          Type: Void
          Parameters: ( id de cadena )
          ->(String) id del evento
          Description:dispara despu�s de que el usuario haya abierto el lightbox (edite el formulario).Usar este evento es una buena forma de personalizar algo en el lightbox.
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onLightbox", function (id) {
                _Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { EventId: id }
                });
            });
        }
    });
    Object.defineProperty(this, 'OnModalBoxButton', {
        /*
          Type: Void
          Parameters: ( id de cadena , nodo HTMLElement , evento e 
          ->(String)la identificaci�n del bot�n
          ->(NodoHTML)un elemento HTML del bot�n cliqueado
          ->(EventNative)un objeto de evento nativo 'clic'
          Description:se dispara cuando el usuario hace clic en un bot�n personalizado en la caja de luz
          El evento se dispara solo para botones personalizados en la parte inferior de la caja de luz y no se dispara para los botones predeterminados o de secci�n.
          Para verificar si la caja de luz est� actualmente abierta o cerrada, use la propiedad lightbox_id del objeto de estado devuelto por el m�todo getState . Si se abre la caja de luz, el m�todo devolver� la identificaci�n del evento abierto, de lo contrario se devolver� 'nulo' o 'indefinido':
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onLightboxButton", function (id, node, e) {
                _Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { ElementId: id, NodeHtml: node, EventNative: e }
                });
            });
        }
    });
    Object.defineProperty(this, 'OnLimitViolation', {
        /*
          Type: Void
          Parameters: ( id de cadena , objeto obj )
          ->(String)la identificaci�n del evento
          ->(Object) el objeto del evento
          Description:se dispara cuando el usuario intenta establecer para un evento el tiempo que est� actualmente limitado / bloqueado.Si devuelve "verdadero" del controlador, el evento relacionado no se bloquear� y puede tener un tiempo no permitido.
          Tenga en cuenta que se llama al evento cuando el usuario intenta establecer para un evento el tiempo actualmente limitado / bloqueado mediante:
          las opciones de configuraci�n limit_start y limit_end
          los m�todos blockTime o addMarkedTimespan
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onLimitViolation", function (id, obj) {
                _Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { EventId: id, ObjectEvent: obj }
                });
            });
        }
    });
    Object.defineProperty(this, 'OnLoadError', {
        /*
          Type: Void
          Parameters: none
          return: XMLHttpRequest un objeto de solicitud Ajax
          Description:se dispara si el planificador no ha podido analizar los datos o si el servidor ha devuelto el estado de respuesta 4xx o 5xx
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onLoadError", function (response) {
                _Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { Response: response }
                });
            });
        }
    });
    Object.defineProperty(this, 'OnLocationError', {
        /*
          Type: Void
          Parameters: ( id de cadena)
          ->(String) id del evento
          Description:se dispara cuando la ubicaci�n del evento no se puede encontrar en el mapa (solo la vista del mapa)
          �C�mo funciona el evento?
          Si un evento en la base de datos no tiene los valores 'lat' y 'lng', el planificador intentar� resolverlos bas�ndose en el valor 'event_location', mientras carga eventos en el planificador. Si se identificar� la ubicaci�n especificada, las coordenadas correspondientes se guardar�n en el DB. De lo contrario, el planificador activar� el evento onLocationError .
          La habilitaci�n de la propiedad de configuraci�n map_resolve_event_location es �til para la migraci�n, pero no para la etapa de producci�n.
          El evento afectar� solo los eventos que se cargan desde DB.
          El evento se puede usar para proporcionar una reacci�n personalizada a la situaci�n, cuando el planificador intenta cargar un evento con una ubicaci�n no v�lida o vac�a.
          Por ejemplo, es posible devolver un objeto google.maps.LatLng con coordenadas geogr�ficas que se aplicar�n al evento en caso de error.
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onLocationError", function (id) {
                _Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { EventId: id }
                });
            });
        }
    });
    Object.defineProperty(this, 'OnMouseDown', {
        /*
          Type: Void
          Parameters: ( string className )
          ->(String) el nombre de una clase css definida para el elemento cliqueado
          Description:se dispara cuando el usuario hace clic en un elemento del planificador que no tiene el controlador predefinido 'onlick'.Los elementos del planificador que tienen los controladores predefinidos 'onclick' se enumeran en la tabla a continuaci�n.
          dhx_cal_event_line	Eventos de varios d�as en las vistas D�a, Semana, Mes, Unidades y cualquier evento en la vista L�nea de tiempo
          dhx_cal_event_clear	Eventos de un d�a en la vista de mes
          dhx_event_move	El encabezado del cuadro del evento utilizado para arrastrar el evento en las vistas D�a, Semana, Unidades
          dhx_wa_ev_body	Un evento en la vista WeekAgenda
          dhx_event_resize	La parte inferior de la caja del evento utilizada para cambiar el tama�o del evento en las vistas D�a, Semana, Unidades
          dhx_scale_holder	Una columna en las vistas D�a, Semana, Unidades
          dhx_scale_holder_now	Una columna resaltada con la fecha actual en las vistas D�a, Semana, Unidades
          dhx_month_body	Una celda sin encabezado en la vista de mes
          dhx_matrix_cell	Una celda en la vista L�nea de tiempo
          dhx_marked_timespan	C�lulas marcadas (resaltadas)
          dhx_time_block	C�lulas bloqueadas        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onMouseDown", function (className) {
                _Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { ClassName: className }
                });
            });
        }
    });
    Object.defineProperty(this, 'OnMouseMove', {
        /*
          Type: Void
          Parameters: ( string id , Evento e )
          ->(String) la identificaci�n del evento
          ->(String) un objeto de evento nativo
          Description:se dispara cuando el cursor del mouse se mueve sobre el programador
          Si el usuario mueve el cursor sobre un evento, la funci�n del manejador toma la identificaci�n del evento; de lo contrario, es nulo.
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onMouseMove", function (id, e) {
                _Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { EventId: id, EventNative: e }
                });
            });
        }
    });
    Object.defineProperty(this, 'OnOptionsLoad', {
        /*
          Type: Void
          Parameters:none
          Description:dispara despu�s de que se haya cargado una colecci�n de opciones o secciones desde el servidor, pero a�n no se haya analizado
          El evento se dispara solo cuando se carga una colecci�n con la ayuda de dhtxmlConnector o mediante el m�todo updateCollection .
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onOptionsLoad", function () {
                _Value({ ObjScheduler: _Scheduler });
            });
        }
    });
    Object.defineProperty(this, 'OnOptionsLoadFinal', {
        /*
          Type: Void
          Parameters:none
          Description: se completan los incendios despu�s de la carga de una colecci�n de opciones (secciones) (solo la vista de la l�nea de tiempo)
          El evento se dispara solo cuando se carga una colecci�n con la ayuda de dhtxmlConnector o mediante el m�todo updateCollection .
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onOptionsLoadFinal", function () {
                _Value({ ObjScheduler: _Scheduler });
            });
        }
    });
    Object.defineProperty(this, 'OnOptionsLoadStart', {
        /*
          Type: Void
          Parameters:none
          Description: dispara antes de que una colecci�n de opciones o secciones comience a cargarse desde el servidor (solo la vista de la l�nea de tiempo)
          El evento se dispara solo cuando se carga una colecci�n con la ayuda de dhtxmlConnector o mediante el m�todo updateCollection .
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onOptionsLoadStart", function () {
                _Value({ ObjScheduler: _Scheduler });
            });
        }
    });
    Object.defineProperty(this, 'OnQuickInfo', {
        /*
          Type: Void
          Parameters: ( string eventId )
          ->(String) Id del Evento
          Description:se dispara cuando aparece el formulario de edici�n emergente
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onQuickInfo", function (eventId) {
                _Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { EventId: eventId }
                });
            });
        }
    });
    Object.defineProperty(this, 'OnSaveError', {
        /*
          Type: Void
          Parameters: ( array ids , XMLHttpRequest respuesta )
          ->(Array)una matriz de identificadores de eventos que no se pudo actualizar
          ->(XMLHttpRequest)un objeto de solicitud Ajax
          Description:se dispara cuando se ha producido alg�n error durante la actualizaci�n de los datos.El evento se invocar� solo si utiliza la biblioteca dataProcessor para la comunicaci�n entre el cliente y el servidor.
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onSaveError", function (ids, resp) {
                _Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { ArrayEventsId: ids, Response: resp }
                });
            });
        }
    });
    Object.defineProperty(this, 'OnScaleAdd', {
        /*
          Type: Void
          Parameters: ( HTMLElement unit , object date )
          ->(ElementHTML) un objeto HTML de la unidad de vista relacionada
          ->(Object) la fecha de la unidad
          Description:dispara despu�s de que una unidad de vista �nica (columna, secci�n, celda de d�a, etc.) se haya procesado en el programador
          Las vistas disponibles tienen diferentes unidades:
          Vista de d�a : una columna con un d�a (toda la vista);
          Vista de semana - una columna con un d�a;
          Vista de mes - una celda con un d�a;
          Unidades - una secci�n;
          Cronolog�a - una secci�n;
          A�o - una c�lula con un d�a.
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onScaleAdd", function (unit, date) {
                _Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { Unit: unit, DateUnit: date }
                });
            });
        }
    });
    Object.defineProperty(this, 'OnScaleDblClick', {
        /*
          Type: Void
          Parameters:none
          Description:se dispara cuando el usuario hace doble clic en la escala de tiempo
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onScaleDblClick", function () {
                _Value({ ObjScheduler: _Scheduler });
            });
        }
    });
    Object.defineProperty(this, 'OnSchedulerResize', {
        /*
          Type: Void
          Parameters:none
          Description:se dispara antes de que el planificador cambie su tama�o
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onSchedulerResize", function () {
                _Value({ ObjScheduler: _Scheduler });
            });
        }
    });
    Object.defineProperty(this, 'OnSchedulerReady', {
        /*
          Type: Void
          Parameters:none
          Description:se dispara despu�s de la inicializaci�n del planificador, pero el planificador a�n no se representa en la p�gina.
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onSchedulerReady", function () {
                _Value({ ObjScheduler: _Scheduler });
            });
        }
    });
    Object.defineProperty(this, 'OnTemplatesReady', {
        /*
          Type: Void
          Parameters:none
          Description:se dispara cuando las plantillas del planificador se inicializan
          El evento informa que las plantillas del planificador est�n listas.
          Es una buena pr�ctica escribir el c�digo de creaci�n de vista personalizada en el controlador del evento onTemplatesReady . Garantizar� que las plantillas de vista personalizada estar�n listas antes de la inicializaci�n del planificador, y la vista personalizada se representar� correctamente en la p�gina.
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onTemplatesReady", function () {
                _Value({ ObjScheduler: _Scheduler });
            });
        }
    });
    Object.defineProperty(this, 'OnTimelineCreated', {
        /*
          Type: Void
          Parameters: ( configuraci�n del objeto )
          ->(Object) el objeto de configuraci�n de vista Timeline
          Description:se dispara despu�s de que se haya inicializado la vista L�nea de tiempo, pero a�n no se representa en la p�gina (solo en la vista L�nea de tiempo)
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onTimelineCreated", function (config) {
                _Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { ConfigView: config }
                });
            });
        }
    });
    Object.defineProperty(this, 'OnViewChange', {
        /*
          Type: Void
          Parameters: ( string new_mode , object new_date )
          ->(String) una nueva vista
          ->(Date) una nueva fecha
          Description:dispara despu�s de que la vista actual ha sido cambiada por otra. El evento se llama cada vez que se cambia la vista actual.
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onViewChange", function (new_mode, new_date) {
                _Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { NewView: new_mode, NewDate: new_date }
                });
            });
        }
    });
    Object.defineProperty(this, 'OnViewMoreClick', {
        /*
          Type: Void
          Parameters: ( fecha del objeto )
          ->(Date) la fecha de la celda en la que el usuario hace clic en el enlace 'Ver m�s' en
          return: Boolean define si la acci�n predeterminada del evento se activar� ( verdadero ) o se cancelar� ( falso )
          Description: se dispara cuando el usuario hace clic en el enlace 'Ver m�s' en la vista Mes (solo en la vista Mes)
          El evento es bloqueable. Devuelve falso , y la vista de mes no se cambiar� a la vista de d�a despu�s de hacer clic en el enlace 'Ver m�s'.
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onViewMoreClick", function (date) {
                return (_Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { DateEvent: date }
                }));
            });
        }
    });
    Object.defineProperty(this, 'OnXLE', {
        /*
          Type: Void
          Parameters:none
          Description:dispara despu�s de cargar los datos de la fuente de datos est� completa
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onXLE", function () {
                _Value({ ObjScheduler: _Scheduler });
            });
        }
    });
    Object.defineProperty(this, 'OnXLS', {
        /*
          Type: Void
          Parameters:none
          Description:se dispara inmediatamente antes de cargar los datos de la fuente de datos se ha iniciado
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onXLS", function () {
                _Value({ ObjScheduler: _Scheduler });
            });
        }
    });
    Object.defineProperty(this, 'OnXScaleClick', {
        /*
         Type: Void
         Parameters: ( �ndice de n�mero , valor del objeto , Evento e )
         ->(Number) el �ndice de columna de la celda cliqueada (numeraci�n basada en cero)
         ->(Object) un objeto de fecha de la marca de tiempo de inicio de la celda cliquead
         ->(EventNative) un objeto de evento nativo
         Description: se dispara cuando el usuario hace un solo clic en una celda en el eje x (solo la vista de la l�nea de tiempo)
       */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onXScaleClick", function (index, value, e) {
                _Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { IndexColumn: index, ValueColumn: value, EventNative: e }
                });
            });
        }
    });
    Object.defineProperty(this, 'OnXScaleDblClick', {
        /*
         Type: Void
         Parameters: ( �ndice de n�mero , valor del objeto , Evento e )
         ->(Number) el �ndice de columna de la celda cliqueada (numeraci�n basada en cero)
         ->(Object) un objeto de fecha de la marca de tiempo de inicio de la celda cliquead
         ->(EventNative) un objeto de evento nativo
         Description: se dispara cuando el usuario hace doble clic en una celda en el eje x (solo la vista de la l�nea de tiempo)
       */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onXScaleDblClick", function () {
                _Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { IndexColumn: index, ValueColumn: value, EventNative: e }
                });
            });
        }
    });
    Object.defineProperty(this, 'OnColumnScaleClick', {
        /*
         Type: Void
         Parameters: ( �ndice de n�mero , valor del objeto , Evento e )
         ->(Number) el �ndice de fila de la celda cliqueada (numeraci�n basada en cero)
         ->(Object) un objeto de datos de la celda cliqueada
         ->(EventNative) un objeto de evento nativo
         Description:se dispara cuando el usuario hace un solo clic en una celda en el eje y (solo la vista de la l�nea de tiempo)
       */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onYScaleClick", function (index, section, e) {
                _Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { IndexRow: index, ValueRow: section, EventNative: e }
                });
            });
        }
    });
    Object.defineProperty(this, 'OnRowScaleDblClick', {
        /*
          Type: Void
          Parameters: ( �ndice de n�mero , valor del objeto , Evento e )
          ->(Number) el �ndice de fila de la celda cliqueada (numeraci�n basada en cero)
          ->(Object) un objeto de datos de la celda cliqueada
          ->(EventNative) un objeto de evento nativo
          Description:se dispara cuando el usuario hace un doble clic en una celda en el eje y (solo la vista de la l�nea de tiempo)
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        },
        set: function (_Value) {
            this.PluginScheduler.attachEvent("onYScaleDblClick", function (index, section, e) {
                _Value({
                    ObjScheduler: _Scheduler,
                    DataInEvent: { IndexRow: index, ValueRow: section, EventNative: e }
                });
            });
        }
    });
}