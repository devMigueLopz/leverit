Componet.Scheduler.TSchedulerConfig = function (plugin, objectScheduler) {
    this.TParent = function () {
        return this;
    }.bind(this);
    this.Scheduler = objectScheduler;
    this.PluginScheduler = plugin;
    /*
        %d	d�a como n�mero con cero inicial, 01..31
        %j	d�a como n�mero, 1..31
        %D	nombre corto del d�a, Su Mo Tu ...
        %l	nombre completo del d�a, domingo lunes martes ...
        %m	mes como n�mero con cero inicial, 01..12
        %n	mes como n�mero, 1..12
        %M	nombre corto del mes, Ene Feb Mar ...
        %F	nombre completo del mes, enero febrero marzo ...
        %y	a�o como n�mero, 2 d�gitos
        %Y     a�o como n�mero, 4 d�gitos
        %h	formato de 12 horas con cero inicial, 01..12)
        %g	formato de 12 horas, 1.12)
        %H	formato de 24 horas con cero a la izquierda, 01..24
        %G	horas 24-formato, 1..24
        %i	minutos con cero inicial, 01..59
        %s	segundos con cero inicial, 1..59
        %a	am o pm
        %A	AM or PM
        %%	escapar por%
        %u	milisegundos
        %P	desplazamiento de la zona horaria
     */
    Object.defineProperty(this, 'NumberColumnYear', {
        /*
            Type: Number
            Descripci�n: establece el n�mero de columnas en la vista de a�o
            Valor por defecto: 3
            Vistas aplicables: Vista de a�o
        */
        get: function () {
            return this.PluginScheduler.config.year_y;
        },
        set: function (_Value) {
            this.PluginScheduler.config.year_y = _Value;
        }
    });
    Object.defineProperty(this, 'NumberRowYear', {
        /*
            Type: Number
            Descripci�n: establece el n�mero de columnas en la vista de a�o
            Valor por defecto: 3
            Vistas aplicables: Vista de a�o
        */
        get: function () {
            return this.PluginScheduler.config.year_x;
        },
        set: function (_Value) {
            this.PluginScheduler.config.year_x = _Value;
        }
    });
    Object.defineProperty(this, 'FormatDate', {
        /*
           Type: String
           Descripci�n: establece el formato de fecha que se utiliza para analizar los datos del conjunto de dato
           Valor por defecto: "%m/%d/%Y %H:%i"
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.xml_date;
        },
        set: function (_Value) {
            this.PluginScheduler.config.xml_date = _Value;
        }
    });
    Object.defineProperty(this, 'ModalWidth', {
        /*
           Type: Boolean
           Descripci�n: habilita / deshabilita la visualizaci�n del lightbox est�ndar (ancho) en lugar del shortbox est�ndar
           Valor por defecto: true
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.wide_form;
        },
        set: function (_Value) {
            this.PluginScheduler.config.wide_form = _Value;
        }
    });
    Object.defineProperty(this, 'FormatWeekDate', {
        /*
           Type: String
           Descripci�n: establece el formato de la fecha en el sub encabezado de la vista de mes
           Valor por defecto: "%l"
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.week_date;
        },
        set: function (_Value) {
            this.PluginScheduler.config.week_date = _Value;
        }
    });
    Object.defineProperty(this, 'FormatMonthDate', {
        /*
           Type: String
           Descripci�n: establece el formato de la fecha en el sub encabezado de la vista de la semana
           Valor por defecto: "%F %Y"
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.month_date;
        },
        set: function (_Value) {
            this.PluginScheduler.config.month_date = _Value;
        }
    });
    Object.defineProperty(this, 'FormatDateMonthDay', {
        /*
           Type: String
           Descripci�n: establece el formato de la fecha en el sub encabezado de la vista del d�a de la semana
           Valor por defecto: "%d"
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.month_day;
        },
        set: function (_Value) {
            this.PluginScheduler.config.month_day = _Value;
        }
    });
    Object.defineProperty(this, 'FormatDateDay', {
        /*
           Type: String
           Descripci�n: establece el formato de fecha para el eje X de la semana y las vistas de unidades
           Valor por defecto:  "%D, %F %j"
           Vistas aplicables: Week View, Units View
        */
        get: function () {
            return this.PluginScheduler.config.day_date;
        },
        set: function (_Value) {
            this.PluginScheduler.config.day_date = _Value;
        }
    });
    Object.defineProperty(this, 'SupportScreenReader', {
        /*
           Type: Boolean 
           Descripci�n: permite el soporte de WAI-ARIA para que el componente sea reconocible para los lectores de pantalla
           Valor por defecto: true
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.wai_aria_attributes;
        },
        set: function (_Value) {
            this.PluginScheduler.config.wai_aria_attributes = _Value;
        }
    });
    Object.defineProperty(this, 'SupportScreenReaderApp', {
        /*
           Type: Boolean 
           Descripci�n: define si role = "aplicaci�n" se usar� para el contenedor principal del programador y los elementos minicalendar.Define c�mo los lectores de pantalla interact�an con el programador.
           Valor por defecto: true
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.wai_aria_application_role;
        },
        set: function (_Value) {
            this.PluginScheduler.config.wai_aria_application_role = _Value;
        }
    });
    Object.defineProperty(this, 'EventWidthAllCell', {
        /*
           Type: Boolean
           Descripci�n: define que los eventos ocupan todo el ancho de la celda. Por defecto, los eventos ocupan todo el ancho de la celda. Establezca la opci�n en falso para que un evento ocupe solo una parte del ancho de la celda y deje espacio para el men� del lado izquierdo
           Valor por defecto: true
           Vistas aplicables: Vista de d�a , Vista de semana , Vista de unidades
        */
        get: function () {
            return this.PluginScheduler.config.use_select_menu_space;
        },
        set: function (_Value) {
            this.PluginScheduler.config.use_select_menu_space = _Value;
        }
    });
    Object.defineProperty(this, 'UpdateRender', {
        /*
           Type: Boolean
           Descripci�n: actualiza el modo cuando el programador se repinta completamente en cualquier acci�n.Cuando se arrastra o cambia el tama�o de un evento, el planificador repinta solo el evento afectado por motivos de rendimiento. El repintado completo ocurre solo cuando el DnD est� terminado.
           Sin embargo, en algunos casos, la posici�n del evento arrastrado afecta las posiciones de los eventos circundantes, y para visualizar todos los eventos correctamente, debe volver a dibujar todo el programador.
           Por ejemplo, cuando tiene varios eventos en la misma celda y mueve uno m�s bajo, el planificador repintar� solo el evento en el que se est� moviendo y, como resultado, puede cubrir algunos eventos m�s importantes. Cuando suelte el bot�n del mouse, todos los eventos tomar�n la posici�n correcta pero, al mover los eventos, pueden superponerse entre s�. Al habilitar la propiedad update_render , se asegura que todos los datos se volver�n a pintar completamente despu�s de cada acci�n.
           Valor por defecto: false.
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.update_render;
        },
        set: function (_Value) {
            this.PluginScheduler.config.update_render = _Value;
        }
    });
    Object.defineProperty(this, 'TouchTooltip', {
        /*
           Type: Boolean
           Descripci�n: deshabilita la informaci�n sobre herramientas de dhtmxlScheduler en los dispositivos t�ctiles
           Valor por defecto: false
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.touch_tooltip;
        },
        set: function (_Value) {
            this.PluginScheduler.config.touch_tooltip = _Value;
        }
    });
    Object.defineProperty(this, 'TouchTip', {
        /*
           Type: Boolean
           Descripci�n: habilita / deshabilita los mensajes de ayuda en la esquina superior derecha de la pantalla
           Valor por defecto: false
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.touch_tip;
        },
        set: function (_Value) {
            this.PluginScheduler.config.touch_tip = _Value;
        }
    });
    Object.defineProperty(this, 'TouchDrag', {
        /*
           Type: Number
           Descripci�n: define el per�odo de tiempo en milisegundos que se usa para diferenciar el gesto de toque prolongado del gesto de desplazamiento. Tenga en cuenta que si establece el par�metro en falso , el usuario no podr� arrastrar eventos.
           Valor por defecto: 500
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.touch_drag;
        },
        set: function (_Value) {
            this.PluginScheduler.config.touch_drag = _Value;
        }
    });
    Object.defineProperty(this, 'Touch', {
        /*
           Type: Number / String 
           Descripci�n: habilita / deshabilita el soporte t�ctil en el programador.Entonces, hay 3 valores posibles que el par�metro puede tomar:
           true : el planificador intenta detectar el dispositivo t�ctil analizando la cadena de agente de usuario del navegador y, si se detecta un dispositivo t�ctil, habilita el soporte t�ctil.
           'force' : habilita el soporte t�ctil persistente, sin importar qu� tipo de dispositivo se use.
           falso - deshabilita el soporte t�ctil.
           Valor por defecto: true
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.touch;
        },
        set: function (_Value) {
            this.PluginScheduler.config.touch = _Value;
        }
    });
    Object.defineProperty(this, 'TimeLineSwapResize', {
        /*
           Type: Boolean
           Descripci�n: define que durante el cambio de tama�o del evento, la fecha de finalizaci�n del evento se puede intercambiar para la fecha de inicio (despu�s de que la fecha de finalizaci�n se programa antes del inicio)
           Si la propiedad se configura como falsa , no le permitir� arrastrar la fecha de finalizaci�n a la izquierda de la fecha de inicio (y viceversa) cuando cambie el tama�o del evento arrastrando y soltando.
           Valor por defecto: false
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.timeline_swap_resize;
        },
        set: function (_Value) {
            this.PluginScheduler.config.timeline_swap_resize = _Value;
        }
    });
    Object.defineProperty(this, 'TimeStep', {
        /*
           Type: Number
           Descripci�n: establece el paso m�nimo (en minutos) para los valores de tiempo del evento
           Las horas de inicio y fin de un evento tendr�n los valores m�ltiples del paso de tiempo, es decir, si time_step = 20 , el evento solo puede comenzar a las: 0, 20, 40 minutos, etc.
           Esta opci�n no es aplicable a la vista L�nea de tiempo, ya que la duraci�n de un evento en esta vista est� definida por la propiedad x_step . Consulte el art�culo createTimelineView para m�s detalles.
           El selector de tiempo de la caja de luz tendr� el mismo paso de tiempo (tambi�n es cierto para la vista de la l�nea de tiempo).
           Valor por defecto: 5
           Vistas aplicables: Vista de d�a , Vista de semana , Vista de mes , Vista de unidades
        */
        get: function () {
            return this.PluginScheduler.config.time_step;
        },
        set: function (_Value) {
            this.PluginScheduler.config.time_step = _Value;
        }
    });
    Object.defineProperty(this, 'StartOnMonday', {
        /*
           Type: Boolean
           Descripci�n: establece el d�a de inicio de las semanas
           Si el par�metro se establece en verdadero , una semana comenzar� a partir del lunes (de lo contrario, desde el domingo).
           Valor por defecto: true
           Vistas aplicables: Ver todo el mes , Semana Ver , Semana de Vista de agenda , A�o Ver
        */
        get: function () {
            return this.PluginScheduler.config.start_on_monday;
        },
        set: function (_Value) {
            this.PluginScheduler.config.start_on_monday = _Value;
        }
    });
    Object.defineProperty(this, 'ShowQuickInfo', {
        /*
           Type: Boolean
           Descripci�n: activa / desactiva la extensi�n 'quick_info' (formulario de detalles de la tarea emergente)
           Valor por defecto: true
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.show_quick_info;
        },
        set: function (_Value) {
            this.PluginScheduler.config.show_quick_info = _Value;
        }
    });
    Object.defineProperty(this, 'ShowLoading', {
        /*
           Type: Boolean
           Descripci�n: permite mostrar un progreso / ruleta mientras se cargan los datos (�til para la carga din�mica)
           Valor por defecto: false
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.show_loading;
        },
        set: function (_Value) {
            this.PluginScheduler.config.show_loading = _Value;
        }
    });
    Object.defineProperty(this, 'ServerUTC', {
        /*
           Type: Boolean
           Descripci�n: Permite convertir las fechas del lado del servidor de UTC a una zona horaria local (y hacia atr�s) mientras se env�an datos al servidor
           Valor por defecto: false
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.server_utc;
        },
        set: function (_Value) {
            this.PluginScheduler.config.server_utc = _Value;
        }
    });
    Object.defineProperty(this, 'SeparateShortEvents', {
        /*
           Type: Boolean
           Descripci�n: permite evitar la superposici�n de eventos cortos
           Valor por defecto: false
           Vistas aplicables: Vista de d�a , Vista de semana , Vista de unidades
        */
        get: function () {
            return this.PluginScheduler.config.separate_short_events;
        },
        set: function (_Value) {
            this.PluginScheduler.config.separate_short_events = _Value;
        }
    });
    Object.defineProperty(this, 'BarSelection', {
        /*
           Type: Boolean
           Descripci�n: muestra / oculta la barra de selecci�n en el cuadro del evento
           Valor por defecto: true
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.select;
        },
        set: function (_Value) {
            this.PluginScheduler.config.select = _Value;
        }
    });
    Object.defineProperty(this, 'SectionDelimiter', {
        /*
           Type: String
           Descripci�n: especifica el del�metro que se usar� para separar varias secciones / unidades en la propiedad de datos relacionados del evento
           Valor por defecto: ","
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.section_delimiter;
        },
        set: function (_Value) {
            this.PluginScheduler.config.section_delimiter = _Value;
        }
    });
    Object.defineProperty(this, 'ScrollHour', {
        /*
           Type: Number
           Descripci�n: establece la posici�n inicial del desplazamiento vertical en el programador (una hora en el formato de reloj de 24 horas)
           Valor por defecto: 0 (es decir, el programador muestra la escala de horas desde la medianoche - 00:00)
           Vistas aplicables: Vista de d�a , Vista de semana , Vista de unidades
        */
        get: function () {
            return this.PluginScheduler.config.scroll_hour;
        },
        set: function (_Value) {
            this.PluginScheduler.config.scroll_hour = _Value;
        }
    });
    Object.defineProperty(this, 'ResizeMonthTimed', {
        /*
           Type: Boolean
           Descripci�n: habilita la posibilidad de cambiar el tama�o de los eventos de un d�a en la vista mensual arrastrando y soltando
           Valor por defecto: false
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.resize_month_timed;
        },
        set: function (_Value) {
            this.PluginScheduler.config.resize_month_timed = _Value;
        }
    });
    Object.defineProperty(this, 'ResizeMonthEvents', {
        /*
           Type: Boolean
           Descripci�n: permite la posibilidad de cambiar el tama�o de los eventos de varios d�as en la vista del mes arrastrando y soltando
           Valor por defecto: false
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.resize_month_events;
        },
        set: function (_Value) {
            this.PluginScheduler.config.resize_month_events = _Value;
        }
    });
    Object.defineProperty(this, 'RepeatPrecise', {
        /*
           Type: Boolean
           Descripci�n: evita incluir d�as pasados a eventos con la recurrencia semanal
           De forma predeterminada, cuando el usuario especifica la recurrencia 'semanal', el planificador incluye la semana actual a esta recurrencia, independientemente de si el usuario crea un evento despu�s, entre o antes del d�a (s) incluido (s).
           Por ejemplo, el usuario crea un evento el jueves y establece la repetici�n "semanal" los lunes y mi�rcoles. El evento guardado contendr� la semana actual, es decir, el pasado lunes y mi�rcoles, aunque se haya creado el jueves.
           Si establece la opci�n repeat_precise en verdadero , la fecha de inicio de un evento recurrente ser� la fecha de la primera ocurrencia real, es decir, en nuestro ejemplo ser� el lunes de la pr�xima semana.
           Valor por defecto: false
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.repeat_precise;
        },
        set: function (_Value) {
            this.PluginScheduler.config.repeat_precise = _Value;
        }
    });
    Object.defineProperty(this, 'RepeatDate', {
        /*
           Type: String
           Descripci�n: establece el formato de fecha del campo 'Finalizar por' en la caja de luz 'recurrente'
           Valor por defecto: %m.%d.%Y"
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.repeat_date;
        },
        set: function (_Value) {
            this.PluginScheduler.config.repeat_date = _Value;
        }
    });
    Object.defineProperty(this, 'RecurringWorkDays', {
        /*
           Type: Array
           Descripci�n: especifica d�as laborables que afectar�n el evento recurrente cuando el usuario selecciona la opci�n "" Todos los d�as de trabajo "en la caja de luz
           Valor por defecto: [1, 2, 3, 4, 5]
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.recurring_workdays;
        },
        set: function (_Value) {
            this.PluginScheduler.config.recurring_workdays = _Value;
        }
    });
    Object.defineProperty(this, 'ModalReadOnly', {
        /*
           Type: Boolean
           Descripci�n: activa el modo de solo lectura para la caja de luz
           Valor por defecto: false
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.readonly_form;
        },
        set: function (_Value) {
            this.PluginScheduler.config.readonly_form = _Value;
        }
    });
    Object.defineProperty(this, 'ReadOnly', {
        /*
          Type: Boolean
          Descripci�n: activa el modo de solo lectura para el programador
          Valor por defecto: false
          Vistas aplicables: All
       */
        get: function () {
            return this.PluginScheduler.config.readonly;
        },
        set: function (_Value) {
            this.PluginScheduler.config.readonly = _Value;
        }
    });
    Object.defineProperty(this, 'QuickInfoDetached', {
        /*
           Type: Boolean
           Descripci�n: define si el formulario de evento aparecer� desde el lado izquierdo / derecho de la pantalla o cerca del evento seleccionado
           Valor por defecto: true ( el formulario de evento aparecer� cerca del evento seleccionado )
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.quick_info_detached;
        },
        set: function (_Value) {
            this.PluginScheduler.config.quick_info_detached = _Value;
        }
    });
    Object.defineProperty(this, 'PreventCache', {
        /*
           Type: Boolean
           Descripci�n: habilita / deshabilita el almacenamiento en cach� de solicitudes GET en el navegador.Se recomienda habilitar la propiedad.
           Valor por defecto: false
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.prevent_cache;
        },
        set: function (_Value) {
            this.PluginScheduler.config.prevent_cache = _Value;
        }
    });
    Object.defineProperty(this, 'ScrollMoveNavigation', {
        /*
           Type: Boolean
           Descripci�n: cancela la preservaci�n de la posici�n de desplazamiento actual mientras se navega entre las fechas de la misma vista
           Valor por defecto: true
           Vistas aplicables: Vista de d�a , Vista de semana , Vista de unidades
        */
        get: function () {
            return this.PluginScheduler.config.preserve_scroll;
        },
        set: function (_Value) {
            this.PluginScheduler.config.preserve_scroll = _Value;
        }
    });
    Object.defineProperty(this, 'PreserveLength', {
        /*
           Type: Boolean
           Descripci�n: conserva la duraci�n visible de un evento mientras se arrastra a lo largo de una escala de tiempo no lineal.
           El modo est� habilitado por defecto.
           Cuando el modo est� habilitado, un evento conserva la longitud visible, en lugar de la longitud real (definida por las fechas de inicio y finalizaci�n) durante la operaci�n de arrastrar y soltar.
           Supongamos que tiene un evento de dos d�as en la vista de mes y los fines de semana est�n ocultos. Si arrastra el evento y lo coloca para ocupar el viernes y el lunes, la diferencia real entre el d�a de inicio y el de finalizaci�n ser� de 4 d�as, pero el planificador conservar� la duraci�n visible: 2 d�as.
           Valor por defecto: true
           Vistas aplicables: mes , vista de l�nea de tiempo
        */
        get: function () {
            return this.PluginScheduler.config.preserve_length;
        },
        set: function (_Value) {
            this.PluginScheduler.config.preserve_length = _Value;
        }
    });
    Object.defineProperty(this, 'SaveClosing', {
        /*
           Type: Boolean
           Descripci�n: define el comportamiento de 'guardar' para el caso, cuando el usuario edita el texto del evento directamente en el recuadro del evento
           Al hacer clic en el bot�n Editar en la barra de selecci�n, se abre un formulario para editar el texto del evento. Cualquier clic externo cierra el formulario y cancela los cambios. Para evitar esto y guardar los cambios realizados en el formulario, establezca la opci�n en verdadero .
           Valor por defecto: false
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.positive_closing;
        },
        set: function (_Value) {
            this.PluginScheduler.config.positive_closing = _Value;
        }
    });
    Object.defineProperty(this, 'OccurrenceTimeStampInUtc', {
        /*
           Type: Boolean
           Descripci�n: permite trabajar con eventos recurrentes independientemente de las zonas horarias. Si la opci�n est� habilitada, las marcas de tiempo de los eventos se almacenan como hora UNIX.
           Valor por defecto: false
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.occurrence_timestamp_in_utc;
        },
        set: function (_Value) {
            this.PluginScheduler.config.occurrence_timestamp_in_utc = _Value;
        }
    });
    Object.defineProperty(this, 'NowDate', {
        /*
           Type: Date
           Descripci�n: establece la fecha del marcador de tiempo actual en la extensi�n L�mite (habilitada por la configuraci�n - mark_now).a opci�n es aplicable solo a la extensi�n L�mite .
           Valor por defecto: none
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.now_date;
        },
        set: function (_Value) {
            this.PluginScheduler.config.now_date = _Value;
        }
    });
    Object.defineProperty(this, 'MultisectionShiftAll', {
        /*
           Type: Boolean
           Descripci�n: specifica si, al arrastrar los eventos asignados a varias secciones de la l�nea de tiempo o la vista Unidades, todas las instancias se deben arrastrar a la vez ('verdadero') o solo el seleccionado ('falso')
           Valor por defecto: true
           Vistas aplicables: l�nea de tiempo , vista de unidades
        */
        get: function () {
            return this.PluginScheduler.config.multisection_shift_all;
        },
        set: function (_Value) {
            this.PluginScheduler.config.multisection_shift_all = _Value;
        }
    });
    Object.defineProperty(this, 'Multisection', {
        /*
           Type: Boolean
           Descripci�n: habilita la posibilidad de representar los mismos eventos en varias secciones de la l�nea de tiempo o la vista Unidades
           Valor por defecto: false
           Vistas aplicables: l�nea de tiempo , vista de unidades
        */
        get: function () {
            return this.PluginScheduler.config.multisection;
        },
        set: function (_Value) {
            this.PluginScheduler.config.multisection = _Value;
        }
    });
    Object.defineProperty(this, 'MultiDayHeightLimit', {
        /*
           Type: Number / Boolean
           Descripci�n: establece la altura del �rea que muestra eventos de varios d�as.Como valor booleano, la propiedad solo puede tomar el valor falso establecido de manera predeterminada.
           Valor por defecto: false
           Vistas aplicables: Vista de d�a , Vista de semana , Vista de unidades
        */
        get: function () {
            return this.PluginScheduler.config.multi_day_height_limit;
        },
        set: function (_Value) {
            this.PluginScheduler.config.multi_day_height_limit = _Value;
        }
    });
    Object.defineProperty(this, 'MultiDay', {
        /*
           Type: Boolean
           Descripci�n: permite renderizar eventos de varios d�as
           Valor por defecto: false
           Vistas aplicables: Vista de d�a , Vista de semana , Vista de unidades
        */
        get: function () {
            return this.PluginScheduler.config.multi_day;
        },
        set: function (_Value) {
            this.PluginScheduler.config.multi_day = _Value;
        }
    });
    Object.defineProperty(this, 'MonthDayMinHeight ', {
        /*
           Type: Number
           Descripci�n: establece la altura m�nima de celdas en la vista de mes
           Valor por defecto: 90
           Vistas aplicables: mes
        */
        get: function () {
            return this.PluginScheduler.config.month_day_min_height;
        },
        set: function (_Value) {
            this.PluginScheduler.config.month_day_min_height = _Value;
        }
    });
    Object.defineProperty(this, 'SmallCalendar', {
        /*
           Type: Object
           Descripci�n: especifica el mini objeto de calendario
           Valor por defecto: {mark_events: true}
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.minicalendar;
        },
        set: function (_Value) {
            this.PluginScheduler.config.minicalendar = _Value;
        }
    });
    Object.defineProperty(this, 'SmallCalendarMarkEvent', {
        /*
           Type: Boolean
           Descripci�n: define si los eventos se resaltar�n en el mini calendario
           Valor por defecto: true
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.minicalendar.mark_events;
        },
        set: function (_Value) {
            this.PluginScheduler.config.minicalendar.mark_events = _Value;
        }
    });
    Object.defineProperty(this, 'MinMapSize', {
        /*
           Type: Number
           Descripci�n: Define el tama�o m�nimo posible de la vista de mapa durante el autoevaluaci�n
           Valor por defecto: 400
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.min_map_size;
        },
        set: function (_Value) {
            this.PluginScheduler.config.min_map_size = _Value;
        }
    });
    Object.defineProperty(this, 'MinGridSize', {
        /*
           Type: Number
           Descripci�n: Define el tama�o m�nimo posible de la vista de cuadr�cula durante el autoevaluaci�n
           Valor por defecto: 25
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.min_grid_size;
        },
        set: function (_Value) {
            this.PluginScheduler.config.min_grid_size = _Value;
        }
    });
    Object.defineProperty(this, 'MaxMonthEvents', {
        /*
           Type: Number
           Descripci�n: establece el n�mero m�ximo de eventos que se pueden visualizar en una celda
           Si la cantidad de eventos asignados excede el valor de la opci�n, el programador mostrar� el enlace 'Ver m�s'. El enlace redirigir� al usuario a la vista D�a que muestra una lista completa de eventos asignados.
           Valor por defecto: none
           Vistas aplicables: Vista del mes
        */
        get: function () {
            return this.PluginScheduler.config.max_month_events;
        },
        set: function (_Value) {
            this.PluginScheduler.config.max_month_events = _Value;
        }
    });
    Object.defineProperty(this, 'MarkCurrentTime', {
        /*
           Type: Boolean
           Descripci�n: habilita / inhabilita el marcador que muestra la hora actual
           Valor por defecto: true
           Vistas aplicables: Vista de d�a , Vista de semana
        */
        get: function () {
            return this.PluginScheduler.config.mark_now;
        },
        set: function (_Value) {
            this.PluginScheduler.config.mark_now = _Value;
        }
    });
    Object.defineProperty(this, 'MapZoomAfterResolve', {
        /*
           Type: Number
           Descripci�n: establece el zoom que se usar� para mostrar la ubicaci�n del usuario, si el usuario acepta la oferta del navegador para mostrarlo
           Valor por defecto: 15
           Vistas aplicables: Vista del mapa
        */
        get: function () {
            return this.PluginScheduler.config.map_zoom_after_resolve;
        },
        set: function (_Value) {
            this.PluginScheduler.config.map_zoom_after_resolve = _Value;
        }
    });
    Object.defineProperty(this, 'MapType', {
        /*
           Type: google.maps.MapTypeId
           Descripci�n:establece el tipo de Google Maps
           Enum: google.maps.MapTypeId.HYBRID, google.maps.MapTypeId.ROADMAP, google.maps.MapTypeId.SATELLITE, google.maps.MapTypeId.TERRAIN,
           Valor por defecto: google.maps.MapTypeId.ROADMAP
           Vistas aplicables: Map View
        */
        get: function () {
            return this.PluginScheduler.config.map_type;
        },
        set: function (_Value) {
            this.PluginScheduler.config.map_type = _Value;
        }
    });
    Object.defineProperty(this, 'MapStart', {
        /*
           Type: Date
           Descripci�n: establece la fecha para comenzar a mostrar eventos de Map
           Valor por defecto: la fecha del usuario actual
           Vistas aplicables: Vista del mapa
        */
        get: function () {
            return this.PluginScheduler.config.map_start;
        },
        set: function (_Value) {
            this.PluginScheduler.config.map_start = _Value;
        }
    });
    Object.defineProperty(this, 'MapEnd', {
        /*
           Type: Date
           Descripci�n: establece la fecha para finalizar el  mostrar eventos de Map
           Valor por defecto: la fecha del usuario actual
           Vistas aplicables: Vista del mapa
        */
        get: function () {
            return this.PluginScheduler.config.map_end;
        },
        set: function (_Value) {
            this.PluginScheduler.config.map_end = _Value;
        }
    });
    Object.defineProperty(this, 'MapResolveUserLocation', {
        /*
           Type: Boolean
           Descripci�n: habilita / deshabilita las solicitudes pidiendo al usuario que comparta su ubicaci�n para mostrar en el mapa
           Algunos navegadores abren la oportunidad de determinar la ubicaci�n del usuario. Y si esta opci�n se configura como verdadera , se ofrecer� dicha oportunidad mientras se carga el mapa.
           Valor por defecto: true
           Vistas aplicables: Vista del mapa
        */
        get: function () {
            return this.PluginScheduler.config.map_resolve_user_location;
        },
        set: function (_Value) {
            this.PluginScheduler.config.map_resolve_user_location = _Value;
        }
    });
    Object.defineProperty(this, 'MapResolveEventLocation', {
        /*
           Type: Boolean
           Descripci�n: activa los intentos de resolver la ubicaci�n del evento, si la base de datos no tiene almacenadas las coordenadas del evento
           Si la opci�n se establece en verdadero y un evento en la base de datos no tiene los valores 'lat' y 'lng', el planificador intentar� resolver las coordenadas bas�ndose en el valor 'ubicaci�n_evento' mientras carga eventos en el programador . Si se identificar� la ubicaci�n especificada, las coordenadas correspondientes se guardar�n en el DB. De lo contrario, el planificador activar� el evento onLocationError .
           La habilitaci�n de la propiedad es �til para la migraci�n, pero no para la etapa de producci�n.
           Valor por defecto: true
           Vistas aplicables: Map
        */
        get: function () {
            return this.PluginScheduler.config.map_resolve_event_location;
        },
        set: function (_Value) {
            this.PluginScheduler.config.map_resolve_event_location = _Value;
        }
    });
    Object.defineProperty(this, 'MapInitialZoom', {
        /*
           Type: Number
           Descripci�n: establece el zoom inicial de Google Maps en la vista de Mapa
           Valor por defecto: 1
           Vistas aplicables: Map
        */
        get: function () {
            return this.PluginScheduler.config.map_initial_zoom;
        },
        set: function (_Value) {
            this.PluginScheduler.config.map_initial_zoom = _Value;
        }
    });
    Object.defineProperty(this, 'MapInitialPosition', {
        /*
           Type: google.maps.LatLng
           Descripci�n: stablece la posici�n inicial del mapa
           Valor por defecto: google.maps.LatLng (48.724, 8.215
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.map_initial_position;
        },
        set: function (_Value) {
            this.PluginScheduler.config.map_initial_position = _Value;
        }
    });
    Object.defineProperty(this, 'MapInfoWindowMaxWidth ', {
        /*
           Type: Number
           Descripci�n: el ancho m�ximo del marcador emergente de Google Maps en la vista del mapa
           Valor por defecto: 300
           Vistas aplicables: Map
        */
        get: function () {
            return this.PluginScheduler.config.map_infowindow_max_width;
        },
        set: function (_Value) {
            this.PluginScheduler.config.map_infowindow_max_width = _Value;
        }
    });
    Object.defineProperty(this, 'MapErrorPosition', {
        /*
           Type: google.maps.LatLng
           Descripci�n: establece la posici�n que se mostrar� en el mapa en caso de que no se pueda identificar la ubicaci�n del evento. La 'posici�n de error' se aplicar� en 2 casos:
           Un evento no tiene una de las coordenadas (o ambas) especificada (es decir, una coordenada tiene el valor '0', 'nulo', 'indefinido') y la opci�n map_resolve_event_location est� deshabilitada.
           Un evento no tiene una de las coordenadas (o ambas) especificadas y la opci�n map_resolve_event_location est� habilitada, pero el planificador no puede resolver la ubicaci�n.
           Valor por defecto: google.maps.LatLng (15, 15)
           Vistas aplicables: Map
        */
        get: function () {
            return this.PluginScheduler.config.map_error_position;
        },
        set: function (_Value) {
            this.PluginScheduler.config.map_error_position = _Value;
        }
    });
    Object.defineProperty(this, 'LoadDate ', {
        /*
           Type: String 
           Descripci�n: establece el formato de los par�metros de solicitud del servidor 'de', 'a' en caso de carga din�mica
           Valor por defecto:"%Y-%m-%d"
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.load_date;
        },
        set: function (_Value) {
            this.PluginScheduler.config.load_date = _Value;
        }
    });
    Object.defineProperty(this, 'LimitView', {
        /*
           Type: Boolean
           Descripci�n: limita el per�odo de la fecha durante el cual el usuario puede ver los eventos.Por ejemplo, si establecemos un l�mite para el a�o 2010, no podemos pasar al a�o 2009, solo 2010 y m�s.
           Valor por defecto: false
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.limit_view;
        },
        set: function (_Value) {
            this.PluginScheduler.config.limit_view = _Value;
        }
    });
    Object.defineProperty(this, 'LimitTimeSelect', {
        /*
           Type: Boolean
           Descripci�n: establece los valores m�ximo y m�nimo del selector de tiempo en el lightbox a los valores de las opciones 'last_hour' y 'first_hour'
           Valor por defecto: false;
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.limit_time_select;
        },
        set: function (_Value) {
            this.PluginScheduler.config.limit_time_select = _Value;
        }
    });
    Object.defineProperty(this, 'LimitStart', {
        /*
           Type: Date
           Descripci�n: establece el borde izquierdo del rango de fechas permitidas
           Valor por defecto: null
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.limit_start;
        },
        set: function (_Value) {
            this.PluginScheduler.config.limit_start = _Value;
        }
    });
    Object.defineProperty(this, 'LimitEnd', {
        /*
           Type: Date
           Descripci�n: establece el borde derecho del rango de fechas permitidas
           Valor por defecto: null
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.limit_end;
        },
        set: function (_Value) {
            this.PluginScheduler.config.limit_end = _Value;
        }
    });
    Object.defineProperty(this, 'LimitDragOut', {
        /*
           Type: Boolean
           Descripci�n: niega arrastrar eventos fuera del �rea visible del planificador
           niega arrastrar eventos fuera del �rea visible de la l�nea de tiempo
           Valor por defecto: false
           Vistas aplicables: l�nea de tiempo
        */
        get: function () {
            return this.PluginScheduler.config.limit_drag_out;
        },
        set: function (_Value) {
            this.PluginScheduler.config.limit_drag_out = _Value;
        }
    });
    Object.defineProperty(this, 'ModalRecurring', {
        /*
           Type: String
           Descripci�n: define el comportamiento de la caja de luz, cuando el usuario abre la caja de luz para editar un evento recurrente
           EnumValue: ask, instance, series
           Ask:antes de que se abra la caja de luz, un cuadro de mensaje alerta y pregunta al usuario si va a editar una determinada instancia o todo el evento recurrente
           Instance: la caja de luz se abre directamente para editar una determinada instancia del evento.
           Serie: el lightbox se abre directamente para editar todo el evento recurrente.
           Valor por defecto: "Ask"
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.lightbox_recurring;
        },
        set: function (_Value) {
            this.PluginScheduler.config.lightbox_recurring = _Value;
        }
    });
    Object.defineProperty(this, 'ModalBox', {
        /*
           Type: object
           Descripci�n: especifica el objeto lightbox
           Valor por defecto: [{name:"description", height:200, map_to:"text", type:"textarea" , focus:true},{name:"time", height:72, type:"time", map_to:"auto"}]
           scheduler.config.lightbox.sections ->> propiedades:
           name - nombre:( String ) el nombre de la secci�n (de acuerdo con este nombre, el planificador tomar� la etiqueta de la secci�n de la colecci�n locale.labels ). Por ejemplo, para la secci�n "tiempo" , el planificador tomar� la etiqueta almacenada como scheduler.locale.labels.section_time .
           height - altura:( Number ) la altura de la secci�n
           map_to - mapa para: ( 'auto' or string) el nombre de una propiedad de datos que se asignar� a la secci�n (ver detalles a continuaci�n)
           La propiedad 'map_to' puede tomar el valor 'auto'. El valor 'autom�tico' se relaciona con lo siguiente:
           El control no devolver� ning�n valor y cambiar� directamente el valor de las propiedades del evento relacionado de acuerdo con el m�todo 'set_value ()' ( Custom Lightbox Control ).
           En general, el valor 'autom�tico' se usa para controles complejos que funcionan con propiedades m�ltiples de un evento
           type - tipo: ( textarea,time,select,template,multiselect,radio,checkbox ) el tipo de control de la secci�n (editor)
           time_format - formato de tiempo:( string ) establece el orden de los controles de fecha y hora en la secci�n "Periodo de tiempo"
           focus - atenci�n:( boolean ) si se establece en verdadero , la secci�n se enfocar� en abrir el lightbox
           default_value - valor por defecto:( any ) el valor predeterminado del control de la secci�n
           onchange - en cambio:( function ) especifica la funci�n del controlador de eventos 'onChange' para el control de la secci�n (solo para el control 'seleccionar' )
           options - opciones:( arrays objects ) define las opciones de selecci�n del control ( para los controles 'select', 'multiselect,' radio ',' combo ' ).
           Cada objeto en la matriz especifica una sola opci�n y toma estas propiedades:
           clave - ( String ) la identificaci�n de la opci�n. Este atributo se compara con la propiedad de datos del evento para asignar opciones de selecci�n a eventos
           label - ( String ) la etiqueta de la opci�n
           vertical - vertical:( Boolean ) especifica si los botones de radio deben colocarse verticalmente ( verdadero ) u horizontalmente ( falso ) (solo para el control 'seleccionar' )
           checked_value - checked_value:( Boolean ) el valor de la casilla de verificaci�n en estado comprobado. Opcional. Por defecto, verdadero (solo para el control 'casilla de verificaci�n' )
           unchecked_value - unchecked_value:( Boolean ) el valor de la casilla de verificaci�n en el estado no verificado. Opcional. Por defecto, falso (solo para el control de 'casilla de verificaci�n' )
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.lightbox.sections;
        },
        set: function (_Value) {
            this.PluginScheduler.config.lightbox.sections = _Value;
        }
    });
    Object.defineProperty(this, 'LeftBoderEvent', {
        /*
           Type: Boolean
           Descripci�n: agrega el borde izquierdo punteado al programador
           Valor por defecto: false
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.left_border;
        },
        set: function (_Value) {
            this.PluginScheduler.config.left_border = _Value;
        }
    });
    Object.defineProperty(this, 'HourScaleYFirst', {
        /*
           Type: Number
           Descripci�n: establece el valor minimo de la escala de horas (eje Y)
           Valor por defecto: 0
           Vistas aplicables: Vista de d�a , Vista de semana , Vista de unidades
        */
        get: function () {
            return this.PluginScheduler.config.first_hour;
        },
        set: function (_Value) {
            this.PluginScheduler.config.first_hour = _Value;
        }
    });
    Object.defineProperty(this, 'HourScaleYLast', {
        /*
           Type: Number
           Descripci�n: establece el valor m�ximo de la escala de horas (eje Y)
           Valor por defecto: 24
           Vistas aplicables: Vista de d�a , Vista de semana , Vista de unidades
        */
        get: function () {
            return this.PluginScheduler.config.last_hour;
        },
        set: function (_Value) {
            this.PluginScheduler.config.last_hour = _Value;
        }
    });
    Object.defineProperty(this, 'KeyNavStep', {
        /*
           Type: Number
           Descripci�n: define el paso m�nimo (en minutos) para navegar eventos
           Valor por defecto: 30
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.key_nav_step;
        },
        set: function (_Value) {
            this.PluginScheduler.config.key_nav_step = _Value;
        }
    });
    Object.defineProperty(this, 'KeyNav', {
        /*
           Type: Boolean
           Descripci�n: habilita la navegaci�n del teclado en el programador
           Valor por defecto: true
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.key_nav;
        },
        set: function (_Value) {
            this.PluginScheduler.config.key_nav = _Value;
        }
    });
    Object.defineProperty(this, 'IncludeEndBy', {
        /*
           Type: boolean
           Descripci�n: define si la fecha especificada en el campo 'Fin por' debe ser exclusiva o inclusiva
           Por defecto, la fecha establecida en el campo 'Finalizar por' es exclusiva.
           Por ejemplo, si el usuario especifica el valor '15 .01.2013 'en el campo' Finalizar ', entonces:
           if include_end_by = false(predeterminado): la serie peri�dica finalizar� el 14.01.2013.
           Si include_end_by = true- la serie recurrente terminar� el 15.01.2013
           Valor por defecto: false
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.include_end_by;
        },
        set: function (_Value) {
            this.PluginScheduler.config.include_end_by = _Value;
        }
    });
    Object.defineProperty(this, 'IconsSelect', {
        /*
           Type: Array
           Descripci�n: almacena una colecci�n de iconos visibles en el men� de selecci�n lateral del cuadro del evento
           Valor por defecto: ['icon_details', 'icon_edit', 'icon_delete']
           Vistas aplicables: Vista de d�a , Vista de semana , Vista de unidades
        */
        get: function () {
            return this.PluginScheduler.config.icons_select;
        },
        set: function (_Value) {
            this.PluginScheduler.config.icons_select = _Value;
        }
    });
    Object.defineProperty(this, 'IconsEdit', {
        /*
           Type: Array
           Descripci�n: almacena una colecci�n de iconos visibles en el men� de edici�n lateral del cuadro del evento
           Valor por defecto: ['icon_save', 'icon_cancel']
           Vistas aplicables: Vista de d�a , Vista de semana , Vista de unidades
        */
        get: function () {
            return this.PluginScheduler.config.icons_edit;
        },
        set: function (_Value) {
            this.PluginScheduler.config.icons_edit = _Value;
        }
    });
    Object.defineProperty(this, 'HourScaleYSize', {
        /*
           Type: Number
           Descripci�n: establece la altura de una unidad de hora en p�xeles
           Valor por defecto: 42
           Vistas aplicables: Vista de d�a , Vista de semana , Vista de unidades
        */
        get: function () {
            return this.PluginScheduler.config.hour_size_px;
        },
        set: function (_Value) {
            this.PluginScheduler.config.hour_size_px = _Value;
        }
    });
    Object.defineProperty(this, 'HourScaleYFormat', {
        /*
           Type: String
           Descripci�n: establece el formato de tiempo del eje Y. Tambi�n se usa en el evento predeterminado y en las plantillas de lighbox para configurar la parte de tiempo.
           Valor por defecto: "%H:%i"
           Vistas aplicables:Vista de d�a , Vista de semana , Vista de unidades
        */
        get: function () {
            return this.PluginScheduler.config.hour_date;
        },
        set: function (_Value) {
            this.PluginScheduler.config.hour_date = _Value;
        }
    });

    Object.defineProperty(this, 'HighlightDisplayedEvent', {
        /*
           Type: Boolean
           Descripci�n: Especifica si los eventos recuperados por el m�todo showEvent deben resaltarse mientras se muestran. se usa solo en el contexto del m�todo showEvent .
           Valor por defecto: true
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.highlight_displayed_event;
        },
        set: function (_Value) {
            this.PluginScheduler.config.highlight_displayed_event = _Value;
        }
    });
    Object.defineProperty(this, 'FullDay', {
        /*
           Type: Boolean
           Descripci�n: permite configurar la duraci�n del evento al d�a completo
           Si la opci�n se establece en verdadero , los campos de entrada en la secci�n " Per�odo de tiempo " del lightbox se bloquean y el per�odo de tiempo se establece en el d�a completo desde 00.00, la fecha actual de la celda hasta 00.00 el d�a siguiente.
           Valor por defecto: false
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.full_day;
        },
        set: function (_Value) {
            this.PluginScheduler.config.full_day = _Value;
        }
    });
    Object.defineProperty(this, 'FixTabPosition', {
        /*
           Type: Boolean
           Descripci�n: mueve las pesta�as de las vistas de izquierda a derecha
           De forma predeterminada, el programador "dhx_terrace-skinned" presenta las pesta�as de vistas en el lado izquierdo. Para colocar las pesta�as en el lado derecho, configure la opci�n en falso .
           Valor por defecto: True
           Vistas aplicables: Vista de d�a , Vista de semana , Vista de unidades
        */
        get: function () {
            return this.PluginScheduler.config.fix_tab_position;
        },
        set: function (_Value) {
            this.PluginScheduler.config.fix_tab_position = _Value;
        }
    });
    Object.defineProperty(this, 'EventDuration', {
        /*
           Type: Number
           Descripci�n: establece la duraci�n inicial de los eventos en minutos
           El par�metro se usa solo en pares con la opci�n auto_end_date .
           Si la opci�n auto_end_date se establece en true , cuando cambie la hora o fecha del evento de inicio en el lightbox, la hora y fecha del evento final se cambiar�n autom�ticamente para que la duraci�n del evento sea igual al valor de la opci�n 'event_duration' .
           Valor por defecto: 5
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.event_duration;
        },
        set: function (_Value) {
            this.PluginScheduler.config.event_duration = _Value;
        }
    });
    Object.defineProperty(this, 'EditOnCreate ', {
        /*
           Type: Boolean
           Descripci�n: 'dice' para abrir el lightbox mientras se crean nuevos eventos
           Valor por defecto: true
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.edit_on_create;
        },
        set: function (_Value) {
            this.PluginScheduler.config.edit_on_create = _Value;
        }
    });
    Object.defineProperty(this, 'EventDragResize', {
        /*
           Type: Boolean
           Descripci�n: permite la posibilidad de cambiar el tama�o de los eventos arrastrando y soltando
           Valor por defecto: false
           Vistas aplicables: Vista de d�a , Vista de semana , Vista de unidades
        */
        get: function () {
            return this.PluginScheduler.config.drag_resize;
        },
        set: function (_Value) {
            this.PluginScheduler.config.drag_resize = _Value;
        }
    });
    Object.defineProperty(this, 'EventDragOut', {
        /*
           Type: Boolean
           Descripci�n: restringir el arrastre de eventos desde el planificador de llamadas a cualquier otro planificador (s)
           Valor por defecto: true
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.drag_out;
        },
        set: function (_Value) {
            this.PluginScheduler.config.drag_out = _Value;
        }
    });
    Object.defineProperty(this, 'EventDragMove', {
        /*
           Type: Boolean
           Descripci�n: permite la posibilidad de mover eventos arrastrando y soltando
           Valor por defecto: true
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.drag_move;
        },
        set: function (_Value) {
            this.PluginScheduler.config.drag_move = _Value;
        }
    });
    Object.defineProperty(this, 'ModalBoxDrag', {
        /*
           Type: Boolean
           Descripci�n: permite la posibilidad de arrastrar el lightbox por el encabezado
           Valor por defecto: true
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.drag_lightbox;
        },
        set: function (_Value) {
            this.PluginScheduler.config.drag_lightbox = _Value;
        }
    });
    Object.defineProperty(this, 'EventDragIn', {
        /*
           Type: Boolean
           Descripci�n: restringe el arrastre de eventos al planificador de llamadas desde cualquier otro planificador (s)
           Valor por defecto: true
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.drag_in;
        },
        set: function (_Value) {
            this.PluginScheduler.config.drag_in = _Value;
        }
    });
    Object.defineProperty(this, 'EventDragHighlight', {
        /*
           Type: Boolean
           Descripci�n: resalta la duraci�n del evento en la escala de tiempo cuando arrastra un evento sobre el programador
           Valor por defecto: true
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.drag_highlight;
        },
        set: function (_Value) {
            this.PluginScheduler.config.drag_highlight = _Value;
        }
    });
    Object.defineProperty(this, 'EventDragCreate', {
        /*
           Type: Boolean
           Descripci�n: permite la posibilidad de crear nuevos eventos arrastrando y soltando
           Valor por defecto: true
           Vistas aplicables: Vista de d�a , Vista de mes , Vista de l�nea de tiempo , Vista de semana , Vista de unidades
        */
        get: function () {
            return this.PluginScheduler.config.drag_create;
        },
        set: function (_Value) {
            this.PluginScheduler.config.drag_create = _Value;
        }
    });
    Object.defineProperty(this, 'DisplayEventTextColor', {
        /*
           Type: String
           Descripci�n: establece el color de fuente predeterminado para los eventos recuperados por el m�todo showEvent ()
           Se usa solo en el contexto del m�todo showEvent .
           Valor por defecto: "#7e2727"
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.displayed_event_text_color;
        },
        set: function (_Value) {
            this.PluginScheduler.config.displayed_event_text_color = _Value;
        }
    });
    Object.defineProperty(this, 'DisplayEventColor', {
        /*
           Type: String
           Descripci�n: establece el color de fondo predeterminado para los eventos recuperados por el m�todo showEvent ()
           Valor por defecto: "#ffc5ab"
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.displayed_event_color;
        },
        set: function (_Value) {
            this.PluginScheduler.config.displayed_event_color = _Value;
        }
    });
    Object.defineProperty(this, 'DisplayMarkedTimeSpans', {
        /*
           Type: Boolean
           Descripci�n: define si los intervalos de tiempo marcados (bloqueados) deben resaltarse en el programador
           Si establece la opci�n en falso , los intervalos de tiempo seguir�n bloqueados, pero se mostrar�n como las casillas del planificador habitual.
           Valor por defecto: true
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.display_marked_timespans;
        },
        set: function (_Value) {
            this.PluginScheduler.config.display_marked_timespans = _Value;
        }
    });
    Object.defineProperty(this, 'DetailsOnDblclick ', {
        /*
           Type: Boolean
           Descripci�n: 'dice' para abrir la caja de luz luego de hacer doble clic en un evento
           Valor por defecto: false
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.details_on_dblclick;
        },
        set: function (_Value) {
            this.PluginScheduler.config.details_on_dblclick = _Value;
        }
    });
    Object.defineProperty(this, 'DetailsOnCreate', {
        /*
           Type: Boolean
           Descripci�n: 'dice' para usar la forma extendida al crear nuevos eventos arrastrando o haciendo doble clic
           Valor por defecto: false
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.details_on_create;
        },
        set: function (_Value) {
            this.PluginScheduler.config.details_on_create = _Value;
        }
    });
    Object.defineProperty(this, 'DelayRender', {
        /*
           Type: Number
           Descripci�n: establece un tiempo de espera (en milisegundos) que envuelve las llamadas updateView y setCurrentView (que provocan el re-dibujo del programador)
           Muchas configuraciones del planificador requieren volver a dibujar. Y, en caso de que tenga una configuraci�n compleja, puede terminar con funciones separadas, cada una de ellas especifica alguna configuraci�n y actualiza el programador para poder aplicarla. Una gran cantidad de nuevos dibujos afectar�n el rendimiento de su aplicaci�n.
           Use la opci�n delay_render para minimar el n�mero de nuevos dibujos.
           Por ejemplo, si establece scheduler.config.delay_render = 30;, cada vez que el c�digo invoque el re-dibujo, el planificador colocar� la llamada en una cola y esperar� 30 milisegundos. Si se obtiene otra llamada 're-draw' durante este tiempo, el planificador restablecer� el tiempo de espera y esperar� otros 30 ms. Como resultado, si el m�todo updateView o / y setCurrentView es / son llamados varias veces en un corto per�odo de tiempo (lo que generalmente sucede cuando el reinicio se desencadena desde diferentes lugares del c�digo personalizado), solo se ejecutar� la �ltima llamada .
           Valor por defecto: 30 (Esta opci�n puede ayudarte a aumentar el rendimiento.)
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.delay_render;
        },
        set: function (_Value) {
            this.PluginScheduler.config.delay_render = _Value;
        }
    });
    Object.defineProperty(this, 'DefaultDate', {
        /*
           Type: String
           Descripci�n: establece el formato de fecha utilizado por las plantillas 'day_date', 'week_date', 'day_scale_date' para establecer la fecha en los encabezados de las vistas
           Valor por defecto: "%j %M %Y"
           Vistas aplicables: Ver d�a , Cronolog�a Ver , vista de semana , Semana de Vista de agenda , Unidades Ver
        */
        get: function () {
            return this.PluginScheduler.config.default_date;
        },
        set: function (_Value) {
            this.PluginScheduler.config.default_date = _Value;
        }
    });
    Object.defineProperty(this, 'DblclickCreate', {
        /*
           Type: Boolean
           Descripci�n: permite la posibilidad de crear eventos haciendo doble clic
           Valor por defecto: true
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.dblclick_create;
        },
        set: function (_Value) {
            this.PluginScheduler.config.dblclick_create = _Value;
        }
    });
    Object.defineProperty(this, 'ContainerAutoresize', {
        /*
           Type: Boolean
           Descripci�n: fuerza al contenedor del programador a cambiar autom�ticamente su tama�o para mostrar todo el contenido sin desplazarse
           Valor por defecto: true
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.container_autoresize;
        },
        set: function (_Value) {
            this.PluginScheduler.config.container_autoresize = _Value;
        }
    });
    Object.defineProperty(this, 'CollisionLimit', {
        /*
           Type: Number
           Descripci�n: establece el n�mero m�ximo permitido de eventos por intervalo de tiempo
           Valor por defecto: 1
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.collision_limit;
        },
        set: function (_Value) {
            this.PluginScheduler.config.collision_limit = _Value;
        }
    });
    Object.defineProperty(this, 'CheckLimits', {
        /*
           Type: Boolean
           Descripci�n: activa / desactiva la verificaci�n de l�mites.Tiene sentido desactivar esta opci�n cuando no establece ning�n l�mite y simplemente resalta o marca la hora actual; esto aumentar� la velocidad de rendimiento. Pero si tiene alg�n conjunto de limitaciones, deshabilitar esta opci�n deshabilitar� todos los m�todos de 'bloqueo'
           Valor por defecto: true
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.check_limits;
        },
        set: function (_Value) {
            this.PluginScheduler.config.check_limits = _Value;
        }
    });
    Object.defineProperty(this, 'cascadeEventDisplay', {
        /*
           Type: Boolean
           Descripci�n: establece el modo de visualizaci�n 'en cascada'
        Por defecto, los eventos programados a la misma hora se muestran uno por uno. Si desea presentar tales eventos como una cascada, establezca la opci�n en verdadero .
           Valor por defecto: false
           Vistas aplicables: Vista de d�a , Vista de semana , Vista de unidades
        */
        get: function () {
            return this.PluginScheduler.config.cascade_event_display;
        },
        set: function (_Value) {
            this.PluginScheduler.config.cascade_event_display = _Value;
        }
    });
    Object.defineProperty(this, 'cascadeEventCount', {
        /*
           Type: Number
           Descripci�n: establece la cantidad m�xima de eventos en una cascada
           Valor por defecto: 4
           Vistas aplicables: Vista de d�a , Vista de semana , Vista de unidades
        */
        get: function () {
            return this.PluginScheduler.config.cascade_event_count;
        },
        set: function (_Value) {
            this.PluginScheduler.config.cascade_event_count = _Value;
        }
    });
    Object.defineProperty(this, 'cascadeEventMargin', {
        /*
           Type: Number
           Descripci�n: establece el margen izquierdo para una cascada de eventos
           Valor por defecto: 30
           Vistas aplicables: Vista de d�a , Vista de semana , Vista de unidades
        */
        get: function () {
            return this.PluginScheduler.config.cascade_event_margin;
        },
        set: function (_Value) {
            this.PluginScheduler.config.cascade_event_margin = _Value;
        }
    });
    Object.defineProperty(this, 'buttonsRight', {
        /*
           Type: Array
           Descripci�n: almacena una colecci�n de botones que se encuentran en la esquina inferior derecha de la caja de luz
           scheduler.config.buttons_right = ["custom_btn_info"];
           scheduler.locale.labels["custom_btn_info"] = "Info";
           scheduler.init('scheduler_here',new Date(2013,05,11),"week");
           ...
           scheduler.attachEvent("onLightboxButton", function(button_id, node, e){
               if(button_id == "custom_btn_info"){
                   alert("Info!");
               }
           });
           Valor por defecto: ["dhx_delete_btn"]
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.buttons_right;
        },
        set: function (_Value) {
            this.PluginScheduler.config.buttons_right = _Value;
        }
    });
    Object.defineProperty(this, 'buttonsLeft', {
        /*
           Type: Array
           Descripci�n: almacena una colecci�n de botones que se encuentran en la esquina izquierda derecha de la caja de luz
           scheduler.config.buttons_left  = ["custom_btn_info"];
           scheduler.locale.labels["custom_btn_info"] = "Info";
           scheduler.init('scheduler_here',new Date(2013,05,11),"week");
           ...
           scheduler.attachEvent("onLightboxButton", function(button_id, node, e){
               if(button_id == "custom_btn_info"){
                   alert("Info!");
               }
           });
           Valor por defecto: [["dhx_save_btn", "dhx_cancel_btn"]
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.buttons_left;
        },
        set: function (_Value) {
            this.PluginScheduler.config.buttons_left = _Value;
        }
    });
    Object.defineProperty(this, 'AutoEndDate', {
        /*
           Type: Boolean
           Descripci�n: permite el cambio autom�tico de la fecha del evento final despu�s de cambiar la fecha de inicio
           El par�metro se usa solo en el par con la opci�n event_duration .
           Si el par�metro se establece en verdadero , cuando cambie la hora o fecha del evento de inicio en el lightbox, la hora y la fecha del evento final se cambiar�n autom�ticamente, para que la duraci�n del evento sea igual al valor de la opci�n event_duration .
           Valor por defecto: false
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.auto_end_date;
        },
        set: function (_Value) {
            this.PluginScheduler.config.auto_end_date = _Value;
        }
    });
    Object.defineProperty(this, 'ApiDate', {
        /*
           Type: Date
           Descripci�n: define el formato de fecha para la plantilla api_date
           Valor por defecto: "%d-%m-%Y %H:%i"
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.api_date;
        },
        set: function (_Value) {
            this.PluginScheduler.config.api_date = _Value;
        }
    });
    Object.defineProperty(this, 'AllTimed', {
        /*
           Type: Boolean | Stringf
           Descripci�n: dice' para mostrar eventos de varios d�as de la manera habitual (a medida que se muestran los eventos de un d�a)
           Como cadena, el par�metro puede tomar el �nico valor: 'corto' .
           Entonces, hay 3 valores posibles que el par�metro puede tomar:
           'sort' : para mostrar solo eventos de varios d�as que duran menos de 24 horas (comienza un d�a y finaliza otro) de la manera habitual.
           true - para mostrar todos los eventos de varios d�as de la manera habitual.
           false - para mostrar todos los eventos de varios d�as como l�neas en la parte superior del planificador (el modo de visualizaci�n est�ndar para eventos de varios d�as).
           Valor por defecto: "short"
           Vistas aplicables: Vista de d�a , Vista de semana , Vista de unidades
        */
        get: function () {
            return this.PluginScheduler.config.all_timed;
        },
        set: function (_Value) {
            this.PluginScheduler.config.all_timed = _Value;
        }
    });
    Object.defineProperty(this, 'AjaxError', {
        /*
           Type: String / Boolean
           Descripci�n: especifica c�mo mostrar la notificaci�n de error predeterminada en caso de que falle la carga de datos XML
           logs error message to console
           supresses the default error messages
           scheduler.config.ajax_error = false;
           La notificaci�n de error predeterminada (es decir, cuando scheduler.config.ajax_error = "alert") se ve as�:
           Valor por defecto: "alert"
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.ajax_error;
        },
        set: function (_Value) {
            this.PluginScheduler.config.ajax_error = _Value;
        }
    });
    Object.defineProperty(this, 'AgendaStart', {
        /*
           Type: Date
           Descripci�n: establece la fecha para comenzar a mostrar eventos de
           Valor por defecto:  la fecha del usuario actual
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.agenda_start;
        },
        set: function (_Value) {
            this.PluginScheduler.config.agenda_start = _Value;
        }
    });
    Object.defineProperty(this, 'AgendaEnd', {
        /*
           Type: Date
           Descripci�n: establece la fecha para mostrar eventos hasta
           Valor por defecto: 'agenda_start' (value) + 1 year
           Vistas aplicables: All
        */
        get: function () {
            return this.PluginScheduler.config.agenda_end;
        },
        set: function (_Value) {
            this.PluginScheduler.config.agenda_end = _Value;
        }
    });
    Object.defineProperty(this, 'ActiveLinkView', {
        /*
           Type: String
           Descripci�n: 'dice' para presentar los n�meros de d�as en la vista de Mes como enlaces clicables que abren el d�a relacionado en la vista especificada
           La propiedad debe configurarse con el nombre de la vista en la que desea abrir los d�as del mes.
           Valor por defecto: "day"
           Vistas aplicables: Month View
        */
        get: function () {
            return this.PluginScheduler.config.active_link_view;
        },
        set: function (_Value) {
            this.PluginScheduler.config.active_link_view = _Value;
        }
    });
}