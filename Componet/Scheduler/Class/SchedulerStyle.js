Componet.Scheduler.TSchedulerStyle= function (plugin, objectScheduler) {
    this.TParent = function () {
        return this;
    }.bind(this);
    this.Scheduler = objectScheduler;
    this.PluginScheduler = plugin;
    Object.defineProperty(this, 'BarHeightMonth', {
        /*
            Type: number
            Descripci�n: la altura de las barras de tareas en la vista de mes
            Valor por defecto: 20
            Vistas aplicables: mes
        */
        get: function () {
            return this.PluginScheduler.xy.bar_height;
        },
        set: function (_Value) {
            this.PluginScheduler.xy.bar_height = _Value;
        }
    });
    Object.defineProperty(this, 'WidthEventTextInput', {
        /*
            Type: number
            Descripci�n: el ancho de la entrada de texto del evento
            Valor por defecto: 140
            Vistas aplicables: d�a, semana, unidades
        */
        get: function () {
            return this.PluginScheduler.xy.editor_width;
        },
        set: function (_Value) {
            this.PluginScheduler.xy.editor_width = _Value;
        }
    });
    Object.defineProperty(this, 'ModalHeight', {
        /*
            Type: number
            Descripci�n: aumenta la longitud de la caja de luz
            Valor por defecto: 50
            Vistas aplicables: todas las vistas
        */
        get: function () {
            return this.PluginScheduler.xy.lightbox_additional_height;
        },
        set: function (_Value) {
            this.PluginScheduler.xy.lightbox_additional_height = _Value;
        }
    });
    Object.defineProperty(this, 'WidthMapDate', {
        /*
            Type: number
            Descripci�n: el ancho de la columna de fecha en la vista de Mapa
            Valor por defecto: 188
            Vistas aplicables: map
        */
        get: function () {
            return this.PluginScheduler.xy.map_date_width;
        },
        set: function (_Value) {
            this.PluginScheduler.xy.map_date_width = _Value;
        }
    });
    Object.defineProperty(this, 'WidthMapDescription', {
        /*
            Type: number
            Descripci�n: el ancho de la columna de descripci�n en la vista de Mapa
            Valor por defecto: 400
            Vistas aplicables: map
        */
        get: function () {
            return this.PluginScheduler.xy.map_description_width;
        },
        set: function (_Value) {
            this.PluginScheduler.xy.map_description_width = _Value;
        }
    });
    Object.defineProperty(this, 'MarginLeftContainerMain', {
        /*
            Type: number
            Descripci�n: el margen izquierdo del �rea principal del programador
            Valor por defecto: 0
            Vistas aplicables: Todas Las vistas
        */
        get: function () {
            return this.PluginScheduler.xy.margin_left;
        },
        set: function (_Value) {
            this.PluginScheduler.xy.margin_left = _Value;
        }
    });
    Object.defineProperty(this, 'MarginTopContainerMain', {
        /*
            Type: number
            Descripci�n: el margen inferior del �rea del planificador principal
            Valor por defecto: 0
            Vistas aplicables: Todas Las vistas
        */
        get: function () {
            return this.PluginScheduler.xy.margin_top;
        },
        set: function (_Value) {
            this.PluginScheduler.xy.margin_top = _Value;
        }
    });
    Object.defineProperty(this, 'WidthMenuSelection', {
        /*
            Type: number
            Descripci�n: el ancho del men� de selecci�n
            Valor por defecto: 25
            Vistas aplicables: d�a, semana, unidades
        */
        get: function () {
            return this.PluginScheduler.xy.menu_width;
        },
        set: function (_Value) {
            this.PluginScheduler.xy.menu_width = _Value;
        }
    });
    Object.defineProperty(this, 'HeightMinBoxEvent', {
        /*
            Type: number
            Descripci�n: la altura m�nima del cuadro del evento
            Valor por defecto: 40
            Vistas aplicables: d�a, semana, unidades
        */
        get: function () {
            return this.PluginScheduler.xy.min_event_height;
        },
        set: function (_Value) {
            this.PluginScheduler.xy.min_event_height = _Value;
        }
    });
    Object.defineProperty(this, 'TopOffSetEventMonth', {
        /*
            Type: number
            Descripci�n: el desplazamiento superior de un evento en una celda en la vista de mes
            Valor por defecto: 20
            Vistas aplicables: mes
        */
        get: function () {
            return this.PluginScheduler.xy.month_scale_height;
        },
        set: function (_Value) {
            this.PluginScheduler.xy.month_scale_height = _Value;
        }
    });
    Object.defineProperty(this, 'NavHeight', {
        /*
            Type: number
            Descripci�n: la altura de la barra de navegaci�n
            Valor por defecto: 22
            Vistas aplicables: Tdoas Las vistas
        */
        get: function () {
            return this.PluginScheduler.xy.nav_height;
        },
        set: function (_Value) {
            this.PluginScheduler.xy.nav_height = _Value;
        }
    });
    Object.defineProperty(this, 'ScaleHeight', {
        /*
            Type: number
            Descripci�n: la altura del eje X
            Valor por defecto: 20
            Vistas aplicables: Tdoas Las vistas
        */
        get: function () {
            return this.PluginScheduler.xy.scale_height;
        },
        set: function (_Value) {
            this.PluginScheduler.xy.scale_height = _Value;
            50
        }
    });
    Object.defineProperty(this, 'WidthY', {
        /*
            Type: number
            Descripci�n: el ancho del eje Y
            Valor por defecto: 50
            Vistas aplicables: d�a, semana, l�nea de tiempo, unidades
        */
        get: function () {
            return this.PluginScheduler.xy.scale_width;
        },
        set: function (_Value) {
            this.PluginScheduler.xy.scale_width = _Value;
        }
    });
    Object.defineProperty(this, 'ScrollWidth', {
        /*
            Type: number
            Descripci�n: el ancho del �rea de la barra de desplazamiento
            Valor por defecto: 18
            Vistas aplicables: todas las vistas
        */
        get: function () {
            return this.PluginScheduler.xy.scroll_width;
        },
        set: function (_Value) {
            this.PluginScheduler.xy.scroll_width = _Value;
        }
    });


}