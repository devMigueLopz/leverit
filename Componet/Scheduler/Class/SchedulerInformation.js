Componet.Scheduler.TSchedulerInformation = function (plugin, objectScheduler) {
    this.TParent = function () {
        return this;
    }.bind(this);
    this.Scheduler = objectScheduler;
    this.PluginScheduler = plugin;
    var _Scheduler = objectScheduler;
    Object.defineProperty(this, 'MonthNameFullDate', {
        get: function () {
            return this.PluginScheduler.locale.date.month_full;
        },
        set: function (_Value) {
            this.PluginScheduler.locale.date.month_full = _Value;
        }
    });
    Object.defineProperty(this, 'MonthNameShortDate', {
        get: function () {
            return this.PluginScheduler.locale.date.month_short;
        },
        set: function (_Value) {
            this.PluginScheduler.locale.date.month_short = _Value;
        }
    });
    Object.defineProperty(this, 'DayNameFullDate', {
        get: function () {
            return this.PluginScheduler.locale.date.day_full;
        },
        set: function (_Value) {
            this.PluginScheduler.locale.date.day_full = _Value;
        }
    });
    Object.defineProperty(this, 'DayNameShortDate', {
        get: function () {
            return this.PluginScheduler.locale.date.labels.day_short;
        },
        set: function (_Value) {
            this.PluginScheduler.locale.date.day_short = _Value;
        }
    });
    Object.defineProperty(this, 'TodayButtonLabel', {
        get: function () {
            return this.PluginScheduler.locale.labels.dhx_cal_today_button;
        },
        set: function (_Value) {
            this.PluginScheduler.locale.labels.dhx_cal_today_button = _Value;
        }
    });
    Object.defineProperty(this, 'DayTabLabel', {
        get: function () {
            return this.PluginScheduler.locale.labels.day_tab;
        },
        set: function (_Value) {
            this.PluginScheduler.locale.labels.day_tab = _Value;
        }
    });
    Object.defineProperty(this, 'WeekTabLabel', {
        get: function () {
            return this.PluginScheduler.locale.labels.week_tab;
        },
        set: function (_Value) {
            this.PluginScheduler.locale.labels.week_tab = _Value;
        }
    });
    Object.defineProperty(this, 'MonthTabLabel', {
        get: function () {
            return this.PluginScheduler.locale.labels.month_tab;
        },
        set: function (_Value) {
            this.PluginScheduler.locale.labels.month_tab = _Value;
        }
    });
    Object.defineProperty(this, 'NewEventLabel', {
        get: function () {
            return this.PluginScheduler.locale.labels.new_event;
        },
        set: function (_Value) {
            this.PluginScheduler.locale.labels.new_event = _Value;
        }
    });
    Object.defineProperty(this, 'IconSaveLabel', {
        get: function () {
            return this.PluginScheduler.locale.labels.icon_save;
        },
        set: function (_Value) {
            this.PluginScheduler.locale.labels.icon_save = _Value;
        }
    });
    Object.defineProperty(this, 'IconCancelLabel', {
        get: function () {
            return this.PluginScheduler.locale.labels.icon_cancel;
        },
        set: function (_Value) {
            this.PluginScheduler.locale.labels.icon_cancel = _Value;
        }
    });
    Object.defineProperty(this, 'IconDetailsLabel', {
        get: function () {
            return this.PluginScheduler.locale.labels.icon_details;
        },
        set: function (_Value) {
            this.PluginScheduler.locale.labels.icon_details = _Value;
        }
    });
    Object.defineProperty(this, 'IconEditLabel', {
        get: function () {
            return this.PluginScheduler.locale.labels.icon_edit;
        },
        set: function (_Value) {
            this.PluginScheduler.locale.labels.icon_edit = _Value;
        }
    });
    Object.defineProperty(this, 'IconDeleteLabel', {
        get: function () {
            return this.PluginScheduler.locale.labels.icon_delete;
        },
        set: function (_Value) {
            this.PluginScheduler.locale.labels.icon_delete = _Value;
        }
    });
    Object.defineProperty(this, 'ConfirmClosingLabel', {
        get: function () {
            return this.PluginScheduler.locale.labels.confirm_closing;
        },
        set: function (_Value) {
            this.PluginScheduler.locale.labels.confirm_closing = _Value;
        }
    });
    Object.defineProperty(this, 'ConfirmDeletingLabel', {
        get: function () {
            return this.PluginScheduler.locale.labels.confirm_deleting;
        },
        set: function (_Value) {
            this.PluginScheduler.locale.labels.confirm_deleting = _Value;
        }
    });
    Object.defineProperty(this, 'SectionDescriptionLabel', {
        get: function () {
            return this.PluginScheduler.locale.labels.section_description;
        },
        set: function (_Value) {
            this.PluginScheduler.locale.labels.section_description = _Value;
        }
    });
    Object.defineProperty(this, 'SectionTimeLabel', {
        get: function () {
            return this.PluginScheduler.locale.labels.section_time;
        },
        set: function (_Value) {
            this.PluginScheduler.locale.labels.section_time = _Value;
        }
    });
}
Componet.Scheduler.TSchedulerInformation.prototype.AddInformationLabel = function (_textProperity, value) {
    try {
        var _this = this.TParent();
        var textProperity = _textProperity
        _this.PluginScheduler.locale.labels[textProperity] = value;
        var properity = textProperity + "Label";
        Object.defineProperty(_this, properity, {
            get: function () {
                return this.PluginScheduler.locale.labels[properity];
            },
            set: function (_Value) {
                this.PluginScheduler.locale.labels[properity] = _Value;
            }
        });
        _this[properity] = value;
    } catch (e) {
        console.log("TSchedulerInformation.prototype.AddInformationDate ", e);
    }
}
Componet.Scheduler.TSchedulerInformation.prototype.AddInformationDate = function (_textProperity, value) {
    try {
        var _this = this.TParent();
        var textProperity = _textProperity
        _this.PluginScheduler.locale.date[textProperity] = value;
        var properity = textProperity + "Date";
        Object.defineProperty(_this, properity, {
            get: function () {
                return this.PluginScheduler.locale.date[properity];
            },
            set: function (_Value) {
                this.PluginScheduler.locale.date[properity] = _Value;
            }
        });
        _this[properity] = value;
    } catch (e) {
        console.log("TSchedulerInformation.prototype.AddInformationDate", e);
    }
}