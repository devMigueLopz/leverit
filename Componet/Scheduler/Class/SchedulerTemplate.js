Componet.Scheduler.TSchedulerTemplate = function (plugin, objectScheduler) {
    this.TParent = function () {
        return this;
    }.bind(this);
    this.Scheduler = objectScheduler;
    this.PluginScheduler = plugin;
    Object.defineProperty(this, 'AgendaText', {
        /*
            Type: CallBack or value any
            Descripci�n: especifica el texto en la segunda columna de la vista Agenda.Tenga en cuenta que si no se especifica la plantilla agenda_text , la parte 'dmy' de la fecha se establecer� de acuerdo con la plantilla day_date .
            CallBack:(la fecha cuando un evento est� programado para comenzar, la fecha en que un evento est� programado para completarse,el objeto del evento)
            Value Any: none
            Valor por defecto: none
            Vistas aplicables: Agenda
            Example:
            scheduler.templates.agenda_text = function(start,end,ev){
            return ev.text;
            };
        */
        get: function () {
            return this.PluginScheduler.templates.agenda_text;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.agenda_text = _Value;
        }
    });
    Object.defineProperty(this, 'AgendaDate', {
        /*
            Type: CallBack or value any
            Descripci�n: especifica la fecha en el encabezado de la vista
            CallBack: (la fecha de inicio de la vista, la fecha de finalizaci�n de la vista)
            Value Any: none
            Valor por defecto: none
            Vistas aplicables: agenda
            Example:
            scheduler.templates.agenda_date = function(start, end) {
            return '';
            };
        */
        get: function () {
            return this.PluginScheduler.templates.agenda_date;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.agenda_date = _Value;
        }
    });
    Object.defineProperty(this, 'AgendaTime', {
        /*
            Type: CallBack or value any
            Descripci�n: especifica la fecha en la primera columna de la vista de Agenda
            CallBack:(la fecha cuando un evento est� programado para comenzar, la fecha en que un evento est� programado para completarse, el objeto del evento)
            Value Any: none
            Valor por defecto: none
            Vistas aplicables: agenda
            Example:
            scheduler.templates.agenda_time = function(start,end,ev){
            if (ev._timed)
            return this.day_date(ev.start_date, ev.end_date, ev)+" "+this.event_date(start);
            else
            return scheduler.templates.day_date(start)+" &ndash; "+
            scheduler.templates.day_date(end);
            };
        */
        get: function () {
            return this.PluginScheduler.templates.agenda_time;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.agenda_time = _Value;
        }
    });
    Object.defineProperty(this, 'ApiDate', {
        /*
            Type: CallBack or value any
            Descripci�n: especifica el formato de las fechas que se establecen mediante m�todos API. Usado para analizar fechas entrantes
            CallBack:(la fecha que necesita formatear)
            Value Any: none
            Valor por defecto: none
            Vistas aplicables: all
            Example:
            scheduler.templates.api_date = function(date){
            return scheduler.date.str_to_date(scheduler.config.api_date);
            };
        */
        get: function () {
            return this.PluginScheduler.templates.api_date;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.api_date = _Value;
        }
    });
    Object.defineProperty(this, 'CalendarDate', {
        /*
            Type: CallBack or value any
            Descripci�n: especifica el formato de la fecha en una celda
            CallBack: none
            Value Any: date cell
            Valor por defecto: none
            Vistas aplicables: all
            Example:
            scheduler.templates.calendar_date = scheduler.date.date_to_str("%d");
        */
        get: function () {
            return this.PluginScheduler.templates.calendar_date;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.calendar_date = _Value;
        }
    });
    Object.defineProperty(this, 'CalendarMonth', {
        /*
            Type: CallBack or value any
            Descripci�n: especifica la fecha en el encabezado del calendario
            CallBack: none
            Value Any: la fecha que necesita formatear
            Valor por defecto:  none
            Vistas aplicables: all
            Example:
            scheduler.templates.calendar_month = scheduler.date.date_to_str("%F %Y");
        */
        get: function () {
            return this.PluginScheduler.templates.calendar_month;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.calendar_month = _Value;
        }
    });
    Object.defineProperty(this, 'CalendarScaleDate ', {
        /*
            Type: CallBack or value any
            Descripci�n: especifica el nombre del d�a en el sub-encabezado de la semana de la vista
            CallBack: none
            Value Any: 
            Valor por defecto: none
            Vistas aplicables: All
            Example:
            scheduler.templates.calendar_scale_date = scheduler.date.date_to_str("%D");
        */
        get: function () {
            return this.PluginScheduler.templates.calendar_scale_date;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.calendar_scale_date = _Value;
        }
    });
    Object.defineProperty(this, 'CalendarTime', {
        /*
            Type: CallBack or value any
            Descripci�n: especifica el formato de fecha de las entradas de fecha de inicio y fin de la caja de luz
            CallBack: none
            Value Any: 
            Valor por defecto: none
            Vistas aplicables: All
            Example:
            scheduler.templates.calendar_time = scheduler.date.date_to_str("%d-%m-%Y");
        */
        get: function () {
            return this.PluginScheduler.templates.calendar_time;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.calendar_time = _Value;
        }
    });
    Object.defineProperty(this, 'DayDate', {
        /*
            Type: CallBack or value any
            Descripci�n: especifica la fecha en el encabezado de las vistas D�a y Unidades
            CallBack: (la fecha que necesita formatear)
            Value Any: none
            Valor por defecto: none
            Vistas aplicables: Vista de d�a , Vista de unidades
            Example:
            scheduler.templates.day_date = function(date){
            var formatFunc = scheduler.date.date_to_str(scheduler.config.default_date);
            return formatFunc(date);
            };
        */
        get: function () {
            return this.PluginScheduler.templates.day_date;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.day_date = _Value;
        }
    });
    Object.defineProperty(this, 'DayScaleDate', {
        /*
           Type: CallBack or value any
           Descripci�n: especifica la fecha en el sub-encabezado de la vista D�a
           CallBack: (la fecha que necesita formatear)
           Value Any: none
           Valor por defecto: none
           Vistas aplicables: Day View
           Example:
           scheduler.templates.day_scale_date = function(date){
               return scheduler.date.date_to_str(scheduler.config.default_date);
           };
       */
        get: function () {
            return this.PluginScheduler.templates.day_scale_date;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.day_scale_date = _Value;
        }
    });
    Object.defineProperty(this, 'DragMarkerClass', {
        /*
            Type: CallBack or value any
            Descripci�n: especifica la clase de CSS que se aplicar� a la duraci�n del evento resaltado en la escala de tiempo
            CallBack:(la fecha cuando un evento est� programado para comenzar, la fecha en que un evento est� programado para completarse, el objeto del evento)
            Value Any: Style myclass{background: green;}
            Valor por defecto: none
            Vistas aplicables: All
            Example:
            .myclass{
            background: green;
            }
            scheduler.templates.drag_marker_class = function(start, end, event){
            return "myclass";
            };
        */
        get: function () {
            return this.PluginScheduler.templates.drag_marker_class;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.drag_marker_class = _Value;
        }
    });
    Object.defineProperty(this, 'DragMarkerContent', {
        /*
            Type: CallBack or value any
            Descripci�n: especifica el contenido del bloque resaltado en la escala de tiempo
            CallBack:(la fecha cuando un evento est� programado para comenzar, la fecha en que un evento est� programado para completarse, el objeto del evento)
            Value Any: text or content HTML example:"<b>my text</b>"
            Valor por defecto: none
            Vistas aplicables: All
            scheduler.templates.drag_marker_content = function(start, end, event){
                return "<b>my text</b>";
            };
        */
        get: function () {
            return this.PluginScheduler.templates.drag_marker_content;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.drag_marker_content = _Value;
        }
    });
    Object.defineProperty(this, 'EventDateDay', {
        /*
           Type: CallBack or value any
           Descripci�n: especifica la fecha de un evento. Aplicado solo a eventos de un d�a
           CallBack:(la fecha cuando un evento est� programado para comenzar, la fecha en que un evento est� programado para completarse, el objeto del evento)
           Value Any: text or content HTML example: "� <b>"+scheduler.templates.event_date(start)+"</b> "
           Valor por defecto: none
           Vistas aplicables: day
           scheduler.templates.event_bar_date = function(start,end,ev){
                return "� <b>"+scheduler.templates.event_date(start)+"</b> ";
           };
       */
        get: function () {
            return this.PluginScheduler.templates.event_bar_date;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.event_bar_date = _Value;
        }
    });
    Object.defineProperty(this, 'EventBarText', {
        /*
           Type: CallBack or value any
           Descripci�n: especifica el texto del evento. Aplicado solo a eventos de varios d�as
           CallBack:(la fecha cuando un evento est� programado para comenzar, la fecha en que un evento est� programado para completarse, el objeto del evento)
           Value Any: text
           Valor por defecto: none
           Vistas aplicables: mes , vista de l�nea de tiempo
           scheduler.templates.event_bar_text = function(start,end,event){
                 return event.text;
           };
       */
        get: function () {
            return this.PluginScheduler.templates.event_bar_text;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.event_bar_text = _Value;
        }
    });
    Object.defineProperty(this, 'EventContainerStyle', {
        /*
           Type: CallBack or value any
           Descripci�n: especifica la clase de CSS que se aplicar� al contenedor del evento.En el caso de la vista L�nea de tiempo, la plantilla se aplica solo a los modos 'Barra' y '�rbol'.
           CallBack:(la fecha cuando un evento est� programado para comenzar, la fecha en que un evento est� programado para completarse, el objeto del evento)
           Value Any: text
           Valor por defecto: none
           Vistas aplicables: Ver d�a , Ver mes , vista semana , a�o Ver , Unidades Ver , fechas Ver
           scheduler.templates.event_class = function(start,end,ev){
               return "";
           };
       */
        get: function () {
            return this.PluginScheduler.templates.event_class;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.event_class = _Value;
        }
    });
    Object.defineProperty(this, 'EventDate', {
        /*
           Type: CallBack or value any
           Descripci�n:especifica la parte de tiempo de las fechas de inicio y finalizaci�n del evento. Usado principalmente por otras plantillas para presentar per�odos de tiempo
           CallBack:(la fecha que necesita formatear)
           Value Any: date
           Valor por defecto: none
           Vistas aplicables: Ver d�a , Ver mes , vista semana , a�o Ver , Unidades Ver , fechas Ver
           scheduler.templates.event_date = function(date){
               var formatFunc = scheduler.date.date_to_str(scheduler.config.hour_date);
               return formatFunc(date);
           }
       */
        get: function () {
            return this.PluginScheduler.templates.event_date;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.event_date = _Value;
        }
    });
    Object.defineProperty(this, 'EventHeader', {
        /*
           Type: CallBack or value any
           Descripci�n:especifica el encabezado del evento
           CallBack:(la fecha cuando un evento est� programado para comenzar, la fecha en que un evento est� programado para completarse, el objeto del evento)
           Value Any: text
           Valor por defecto: none
           Vistas aplicables: Vista de d�a , Vista de semana , Vista de unidades
           scheduler.templates.event_header = function(start,end,ev){
               return scheduler.templates.event_date(start)+" - "+
               scheduler.templates.event_date(end);
           };
       */
        get: function () {
            return this.PluginScheduler.templates.event_header;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.event_header = _Value;
        }
    });
    Object.defineProperty(this, 'EventText', {
        /*
           Type: CallBack or value any
           Descripci�n:especifica el texto del evento
           CallBack:(la fecha cuando un evento est� programado para comenzar, la fecha en que un evento est� programado para completarse, el objeto del evento)
           Value Any: text
           Valor por defecto: none
           Vistas aplicables: Vista de d�a , Vista de semana , Vista de unidades
           scheduler.templates.event_text=function(start, end, event){
              return "<a href='http://some.com/details.php?for="+event.id+"'>"
              +event.text+"</a>";
           }
       */
        get: function () {
            return this.PluginScheduler.templates.event_text;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.event_text = _Value;
        }
    });
    Object.defineProperty(this, 'HourScaleY', {
        /*
           Type: CallBack or value any
           Descripci�n:especifica los elementos del eje Y
           CallBack:(la fecha que necesita formatear)
           Value Any: date
           Valor por defecto: none
           Vistas aplicables:Vista de d�a , Vista de semana , Vista de unidades
           scheduler.templates.hour_scale = function(date){
               return scheduler.date.date_to_str(scheduler.config.hour_date)(date);
           };
       */
        get: function () {
            return this.PluginScheduler.templates.hour_scale;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.hour_scale = _Value;
        }
    });
    Object.defineProperty(this, 'LoadFormat', {
        /*
           Type: CallBack or value any
           Descripci�n:especifica el formato de las solicitudes en el modo de carga din�mica
           CallBack:(la fecha que necesita formatear)
           Value Any: date
           Valor por defecto: none
           Vistas aplicables: All
           scheduler.templates.load_format = function(date){
               var dateToStr_func = scheduler.date.date_to_str(scheduler.config.load_date);
               return  dateToStr_func(date);
           }
       */
        get: function () {
            return this.PluginScheduler.templates.load_format;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.load_format = _Value;
        }
    });
    Object.defineProperty(this, 'MapDate ', {
        /*
           Type: CallBack or value any
           Descripci�n:especifica la fecha en el encabezado de la vista
           CallBack:(la fecha cuando un evento est� programado para comenzar, la fecha en que un evento est� programado para completarse)
           Value Any: text
           Valor por defecto: none
           Vistas aplicables: Mapa
           scheduler.templates.map_date = function(start, end) {
               return '';
           };
       */
        get: function () {
            return this.PluginScheduler.templates.map_date;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.map_date = _Value;
        }
    });
    Object.defineProperty(this, 'MapText', {
        /*
           Type: CallBack or value any
           Descripci�n:especifica el texto en la segunda columna de la vista
           CallBack:(la fecha cuando un evento est� programado para comenzar, la fecha en que un evento est� programado para completarse, el objeto del evento)
           Value Any: text
           Valor por defecto: none
           Vistas aplicables: Mapa
           scheduler.templates.map_text = function(start,end,ev){
               return ev.text;
           }
       */
        get: function () {
            return this.PluginScheduler.templates.map_text;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.map_text = _Value;
        }
    });
    Object.defineProperty(this, 'MapTime', {
        /*
            Type: CallBack or value any
            Descripci�n:especifica la fecha en la primera columna de la vista
            CallBack:(la fecha cuando un evento est� programado para comenzar, la fecha en que un evento est� programado para completarse, el objeto del evento)
            Value Any: text
            Valor por defecto: none
            Vistas aplicables: Mapa
            scheduler.templates.map_time = function(start,end,ev){
                if (ev._timed)
                    return this.day_date(ev.start_date, ev.end_date, ev) + " " +
                    this.event_date(start);
                else
                    return scheduler.templates.day_date(start) + " &ndash; " +
                    scheduler.templates.day_date(end);
            };
        */
        get: function () {
            return this.PluginScheduler.templates.map_time;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.map_time = _Value;
        }
    });
    Object.defineProperty(this, 'MarkerDate ', {
        /*
            Type: CallBack or value any
            Descripci�n:especifica la fecha del evento en el marcador emergente de Google Maps
            CallBack:(la fecha cuando un evento est� programado para comenzar, la fecha en que un evento est� programado para completarse, el objeto del evento)
            Value Any: date
            Valor por defecto: none
            Vistas aplicables: Mapa
            scheduler.templates.marker_date = function(date){
                return scheduler.date.date_to_str("%Y-%m-%d %H:%i");
            };
        */
        get: function () {
            return this.PluginScheduler.templates.marker_date;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.marker_date = _Value;
        }
    });
    Object.defineProperty(this, 'MarkerText', {
        /*
            Type: CallBack or value any
            Descripci�n:especifica el texto del evento en el marcador emergente de Google Maps
            CallBack:(la fecha cuando un evento est� programado para comenzar, la fecha en que un evento est� programado para completarse, el objeto del evento)
            Value Any: text
            Valor por defecto: none
            scheduler.templates.marker_text = function(start,end,ev){
                 return "<div><b>" + ev.text + "</b><br/><br/>" + (ev.event_location || '') +
                 "<br/><br/>" + scheduler.templates.marker_date(start) + " - " +
                 scheduler.templates.marker_date(end) + "</div>";
            };
        */
        get: function () {
            return this.PluginScheduler.templates.marker_text;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.marker_text = _Value;
        }
    });
    Object.defineProperty(this, 'MonthDate', {
        /*
           Type: CallBack or value any
           Descripci�n:especifica la fecha en el encabezado de la vista
           CallBack:(la fecha que necesita formatear)
           Value Any: date
           Valor por defecto: none
           Vistas aplicables: mes
           scheduler.templates.month_date = function(date){
               var dateToStr_func = scheduler.date.date_to_str(scheduler.config.month_date);
               return  dateToStr_func(date);
           };
       */
        get: function () {
            return this.PluginScheduler.templates.month_date;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.month_date = _Value;
        }
    });
    Object.defineProperty(this, 'MonthDateStyle', {
        /*
           Type: CallBack or value any
           Descripci�n:especifica la clase de CSS que se aplicar� a una celda de d�a
           CallBack:(la fecha que necesita formatear)
           Value Any: text
           Valor por defecto: none
           Vistas aplicables: mes
           scheduler.templates.month_date_class = function(date){
               return "";
           };
       */
        get: function () {
            return this.PluginScheduler.templates.month_date_class;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.month_date_class = _Value;
        }
    });
    Object.defineProperty(this, 'MonthDay', {
        /*
           Type: CallBack or value any
           Descripci�n:especifica el formato del d�a en una celda
           CallBack:(la fecha que necesita formatear)
           Value Any: date
           Valor por defecto: none
           Vistas aplicables: mes , vista de a�o
           scheduler.templates.month_day = function(date){
               var dateToStr_func = scheduler.date.date_to_str(scheduler.config.month_day);
               return  dateToStr_func(date);
           };
       */
        get: function () {
            return this.PluginScheduler.templates.month_day;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.month_day = _Value;
        }
    });
    Object.defineProperty(this, 'MonthEventsLink', {
        /*
            Type: CallBack or value any
            Descripci�n: especifica la presentaci�n del enlace 'Ver m�s' en la celda de la vista Mes
            CallBack:(la fecha cuando un evento est� programado para comenzar, la fecha en que un evento est� programado para completarse)
            Value Any: text
            Valor por defecto: none
            Vistas aplicables: mes
            Example:
            scheduler.templates.month_events_link = function(date, count){
            return "<a>View more("+count+" events)</a>";};
        */
        get: function () {
            return this.PluginScheduler.templates.month_events_link;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.month_events_link = _Value;
        }
    });
    Object.defineProperty(this, 'MonthScaleDate', {
        /*
            Type: CallBack or value any
            Descripci�n: especifica el formato de fecha del eje X de la vista
            CallBack:(la fecha que necesita formatear)
            Value Any: date
            Valor por defecto: none
            Vistas aplicables: mes
            Example:
            scheduler.templates.month_scale_date = function(date){
            return scheduler.date.date_to_str(scheduler.config.week_date);};
        */
        get: function () {
            return this.PluginScheduler.templates.month_scale_date;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.month_scale_date = _Value;
        }
    });
    Object.defineProperty(this, 'QuickInfoContent', {
        /*
            Type: CallBack or value any
            Descripci�n: especifica el contenido del formulario de edici�n emergente
            CallBack:(la fecha cuando un evento est� programado para comenzar, la fecha en que un evento est� programado para completarse, el objeto del evento)
            Value Any: text content
            Valor por defecto: none
            Vistas aplicables: all
            Example:
            scheduler.templates.quick_info_content = function(start, end, ev){
            return ev.details || ev.text;};
        */
        get: function () {
            return this.PluginScheduler.templates.quick_info_content;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.quick_info_content = _Value;
        }
    });
    Object.defineProperty(this, 'QuickInfoDate', {
        /*
            Type: CallBack or value any
            Descripci�n: especifica la fecha del formulario de edici�n emergente
            CallBack:(la fecha cuando un evento est� programado para comenzar, la fecha en que un evento est� programado para completarse, el objeto del evento)
            Value Any: text content
            Valor por defecto: none
            Vistas aplicables: all
            Example:
            scheduler.templates.quick_info_date = function(start, end, ev){
            if (scheduler.isOneDayEvent(ev)){
            return scheduler.templates.day_date(start, end, ev) + " " +
            scheduler.templates.event_header(start, end, ev);
            }else{
            return scheduler.templates.week_date(start, end, ev);}};
        */
        get: function () {
            return this.PluginScheduler.templates.quick_info_date;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.quick_info_date = _Value;
        }
    });
    Object.defineProperty(this, 'QuickInfoTitle', {
        /*
            Type: CallBack or value any
            Descripci�n: especifica el t�tulo del formulario de edici�n emergente
            CallBack:(la fecha cuando un evento est� programado para comenzar, la fecha en que un evento est� programado para completarse, el objeto del evento)
            Value Any: text content
            Valor por defecto: none
            Vistas aplicables: all
            Example:
            scheduler.templates.quick_info_title = function(start, end, ev){
            return ev.text.substr(0,50);};
        */
        get: function () {
            return this.PluginScheduler.templates.quick_info_title;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.quick_info_title = _Value;
        }
    });
    Object.defineProperty(this, 'TimePicker', {
        /*
            Type: CallBack or value any
            Descripci�n: especifica el selector de tiempo desplegable en la caja de luz
            CallBack: none
            Value Any: hour or text
            Valor por defecto: none
            Vistas aplicables: all
            Example:
            scheduler.templates.time_picker =scheduler.date.date_to_str(scheduler.config.hour_date);
        */
        get: function () {
            return this.PluginScheduler.templates.time_picker;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.time_picker = _Value;
        }
    });
    Object.defineProperty(this, 'TooltipDateFormat', {
        /*
            Type: CallBack or value any
            Descripci�n: especifica el formato de las fechas de inicio y finalizaci�n que se muestran en la informaci�n sobre herramientas
            CallBack: (la fecha que necesita formatear)
            Value Any: date or text
            Valor por defecto: none
            Vistas aplicables: all
            Example:
            scheduler.templates.tooltip_date_format=function (date){
            var formatFunc = scheduler.date.date_to_str("%Y-%m-%d %H:%i");
            return formatFunc(date);}
        */
        get: function () {
            return this.PluginScheduler.templates.tooltip_date_format;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.tooltip_date_format = _Value;
        }
    });
    Object.defineProperty(this, 'TooltipText', {
        /*
            Type: CallBack or value any
            Descripci�n: especifica el texto de la informaci�n sobre herramientas
            CallBack:(la fecha cuando un evento est� programado para comenzar, la fecha en que un evento est� programado para completarse, el objeto del evento)
            Value Any: text content or html
            Valor por defecto: none
            Vistas aplicables: all
            Example:
            scheduler.templates.tooltip_text = function(start,end,ev){
            return "<b>Event:</b> "+ev.text+"<br/><b>Start date:</b> " +
            scheduler.templates.tooltip_date_format(start)+
            "<br/><b>End date:</b> "+scheduler.templates.tooltip_date_format(end);};
        */
        get: function () {
            return this.PluginScheduler.templates.tooltip_text;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.tooltip_text = _Value;
        }
    });
    Object.defineProperty(this, 'WeekAgendaEventText', {
        /*
            Type: CallBack or value any
            Descripci�n: especifica el texto del evento
            CallBack:(la fecha cuando un evento est� programado para comenzar,la fecha en que un evento est� programado para completarse,evento,la fecha de una celda diurna que muestra un evento de un d�a o una sola ocurrencia del evento recurrente en, la posici�n de una �nica aparici�n en el evento recurrente: 'inicio' - la primera ocurrencia, 'fin' - la �ltima ocurrencia, 'medio' - para las ocurrencias restantes )
            Value Any: text
            Valor por defecto: none
            Vistas aplicables: agenda de la semana
            Example:
            scheduler.templates.week_agenda_event_text = function(start,end,event,cellDate,pos){
                return scheduler.templates.event_date(start_date) + " " + event.text;
            };
        */
        get: function () {
            return this.PluginScheduler.templates.week_agenda_event_text;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.week_agenda_event_text = _Value;
        }
    });
    Object.defineProperty(this, 'WeekAgendaScaleDate', {
        /*
            Type: CallBack or value any
            Descripci�n: la fecha de una celda de d�a de la vista
            CallBack:(la fecha que necesita formatear)
            Value Any: date or text
            Valor por defecto: none
            Vistas aplicables: semana
            Example:
            scheduler.templates.week_agenda_scale_date = function(date) {
                var scale_date_format = scheduler.date.date_to_str("%l, %F %d");
                return scale_date_format(date);
            };
        */
        get: function () {
            return this.PluginScheduler.templates.week_agenda_scale_date;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.week_agenda_scale_date = _Value;
        }
    });
    Object.defineProperty(this, 'WeekDate', {
        /*
            Type: CallBack or value any
            Descripci�n: especifica la fecha en el encabezado de la vista
            CallBack:(la fecha de inicio de la vista, la fecha fin de la vista)
            Value Any: date or text
            Valor por defecto: none
            Vistas aplicables: semana , vista de agenda de semana
            Example:
            scheduler.templates.week_date = function(start, end){
                return scheduler.templates.day_date(start)+" &ndash; "+ scheduler.templates.day_date(scheduler.date.add(end,-1,"day"));
            };
        */
        get: function () {
            return this.PluginScheduler.templates.week_date;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.week_date = _Value;
        }
    });
    Object.defineProperty(this, 'WeekDateStyle', {
        /*
            Type: CallBack or value any
            Descripci�n: especifica la clase de CSS que se aplicar� a una celda de d�a
            CallBack:(la fecha de inicio de la vista, la fecha fin de la vista)
            Value Any: text
            Valor por defecto: none
            Vistas aplicables: semana , vista de unidades
            Example:
            scheduler.templates.week_date_class = function(start, today){
            return "";
            };
        */
        get: function () {
            return this.PluginScheduler.templates.week_date_class;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.week_date_class = _Value;
        }
    });
    Object.defineProperty(this, 'WeekScaleDate', {
        /*
            Type: CallBack or value any
             Descripci�n: especifica la fecha en el sub encabezado de la vista
            CallBack:(la fecha que necesita formatear)
            Value Any: date or text
            Valor por defecto: none
            Vistas aplicables: semana
            Example:
            var format = scheduler.date.date_to_str(scheduler.config.day_date);
            scheduler.templates.week_scale_date = function(date){
                return format(date);
            };
        */
        get: function () {
            return this.PluginScheduler.templates.week_scale_date;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.week_scale_date = _Value;
        }
    });
    Object.defineProperty(this, 'XmlDate', {
        /*
            Type: CallBack or value any
            Descripci�n: una cadena de un archivo XML se convierte en un objeto de fecha de conformidad con esta plantilla
            CallBack: (la cadena que necesita ser analizada Fecha)
            Value Any: date or text
            Valor por defecto: none
            Vistas aplicables: all
            Example:
            var cfg = scheduler.config;
            var str_to_date = scheduler.date.str_to_date(cfg.xml_date, cfg.server_utc);
            scheduler.templates.xml_date = function(date){
                return str_to_date(date);
            };
        */
        get: function () {
            return this.PluginScheduler.templates.xml_date;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.xml_date = _Value;
        }
    });
    Object.defineProperty(this, 'XmlFormat', {
        /*
            Type: CallBack or value any
            Descripci�n: un objeto de fecha se convierte en una cadena de conformidad con esta plantilla. Se usa para enviar datos al servidor
            CallBack: (la cadena que necesita ser analizada Fecha)
            Value Any: date or text
            Valor por defecto: none
            Vistas aplicables: all
            Example:
            var cfg = scheduler.config;
            var date_to_str = scheduler.date.date_to_str(cfg.xml_date, cfg.server_utc);
            scheduler.templates.xml_format = function(date){
                return date_to_str(date);
            };
                    */
        get: function () {
            return this.PluginScheduler.templates.xml_format;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.xml_format = _Value;
        }
    });
    Object.defineProperty(this, 'YearDate', {
        /*
            Type: CallBack or value any
            Descripci�n: especifica la fecha en el encabezado de la vista
            CallBack: (la cadena que necesita ser analizada Fecha)
            Value Any: date or text
            Valor por defecto: none
            Vistas aplicables: year
            Example:
            var date_to_str=scheduler.date.date_to_str(scheduler.locale.labels.year_tab +" %Y");
            scheduler.templates.year_date = function(date){
                return date_to_str(date);
            };
        */
        get: function () {
            return this.PluginScheduler.templates.year_date;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.year_date = _Value;
        }
    });
    Object.defineProperty(this, 'YearMonth', {
        /*
            Type: CallBack or value any
            Descripci�n: especifica el nombre del mes en el encabezado de un bloque de mes de la vista.
            CallBack: (la cadena que necesita ser analizada Fecha)
            Value Any: date or text
            Valor por defecto: none
            Vistas aplicables: year
            Example:
            scheduler.templates.year_month = function(date){
                return scheduler.date.date_to_str("%F");
            };
        */
        get: function () {
            return this.PluginScheduler.templates.year_month;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.year_month = _Value;
        }
    });
    Object.defineProperty(this, 'YearScaleDate', {
        /*
            Type: CallBack or value any
            Descripci�n: especifica el nombre del d�a en el subencabezado de un bloque de un mes de la vista
            CallBack: (la cadena que necesita ser analizada Fecha)
            Value Any: date or text
            Valor por defecto: none
            Vistas aplicables: year
            Example:
            scheduler.templates.year_scale_date = function(date){
                return scheduler.date.date_to_str("%D");
            };
        */
        get: function () {
            return this.PluginScheduler.templates.year_scale_date;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.year_scale_date = _Value;
        }
    });
    Object.defineProperty(this, 'YearTooltip', {
        /*
            Type: CallBack or value any
            Descripci�n:especifica la informaci�n sobre herramientas sobre una celda de d�a que contiene algunos eventos programados
            CallBack:(la fecha cuando un evento est� programado para comenzar, la fecha en que un evento est� programado para completarse, el objeto del evento)
            Value Any: text content
            Valor por defecto: none
            Vistas aplicables: year
            Example:
            scheduler.templates.year_tooltip = function(start,end,ev){
                return ev.text;
            };
        */
        get: function () {
            return this.PluginScheduler.templates.year_tooltip;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.year_tooltip = _Value;
        }
    });
    Object.defineProperty(this, 'ModalBoxHeader', {
        /*
            Type: CallBack or value any
            Descripci�n:especifica el encabezado de la caja de luz.Tenga en cuenta que, si no se especifica la plantilla lightbox_header , la parte de fecha del encabezado se establecer� de acuerdo con la plantilla event_header .
            CallBack:(la fecha cuando un evento est� programado para comenzar, la fecha en que un evento est� programado para completarse, el objeto del evento)
            Value Any: text content
            Valor por defecto: none
            Vistas aplicables: all
            Example:
            scheduler.templates.lightbox_header = function(start,end,ev){
                return scheduler.templates.event_header(ev.start_date,ev.end_date,ev)
                + scheduler.templates.event_bar_text(ev.start_date,ev.end_date,ev);
            };
        */
        get: function () {
            return this.PluginScheduler.templates.lightbox_header;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.lightbox_header = _Value;
        }
    });
    Object.defineProperty(this, 'GridDate', {
        /*
            Type: CallBack or value any
            Descripci�n: especifica la fecha en el encabezado de la vista
            CallBack: (Fecha inicio de la vista, fecha fin)
            Value Any: date or text
            Valor por defecto: none
            Vistas aplicables: cuadricula
            Example:
            scheduler.templates.grid_date = function(start, end){
                return scheduler.templates.day_date(start)
                + " - "
                + scheduler.templates.day_date(end);
            };
        */
        get: function () {
            return this.PluginScheduler.templates.grid_date;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.grid_date = _Value;
        }
    });
    Object.defineProperty(this, 'GridFullDate', {
        /*
            Type: CallBack or value any
            Descripci�n: especifica el formato de fechas en columnas con id = 'fecha'
            CallBack: (Fecha inicio de la evento, fecha fin evento, obj evento)
            Value Any: date or text
            Valor por defecto: none
            Vistas aplicables: cuadricula
            Example:
            scheduler.templates.grid_full_date = function(start,end,event){
            if (scheduler.isOneDayEvent(event))
            return scheduler.templates.grid_single_date(start);
            else
            return scheduler.templates.day_date(start)+" &ndash; "
            +scheduler.templates.day_date(end);
            };
        */
        get: function () {
            return this.PluginScheduler.templates.grid_full_date;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.grid_full_date = _Value;
        }
    });
    Object.defineProperty(this, 'GridSingleDate', {
        /*
            Type: CallBack or value any
            Descripci�n: especifica el formato de las fechas en las columnas con id = 'start_date' o id = 'end_date'
            CallBack: (Fecha necesita dormatear)
            Value Any: date or  text
            Valor por defecto: none
            Vistas aplicables: cuadricula
            Example:
            scheduler.templates.grid_single_date = function(date){
                return scheduler.templates.day_date(date)+" "+this.event_date(date);
            };
        */
        get: function () {
            return this.PluginScheduler.templates.grid_single_date;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.grid_single_date = _Value;
        }
    });
    Object.defineProperty(this, 'GridField', {
        /*
            Type: CallBack or value any
            Descripci�n: especifica el texto en las columnas
            CallBack:(identificaci�n de columnba, obj vento)
            Value Any: texto
            Valor por defecto: none
            Vistas aplicables: cuadricula
            Example:
            scheduler.templates.grid_field = function(field_name, event){
            return event[field_name];
            };
        */
        get: function () {
            return this.PluginScheduler.templates.grid_field;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.grid_field = _Value;
        }
    });
    Object.defineProperty(this, 'TemplateTimelineCellValue', {
        /*
            Type: CallBack or value any
            Descripci�n: especifica el n�mero de eventos programados en una celda de la vista
            CallBack: (una matriz de objetos de eventos contenidos en una celda, la fecha de una celda)
            Value Any: text
            Valor por defecto: none
            Vistas aplicables: linea de tiempo
            Example:
            scheduler.templates.timeline_cell_value = function(evs, date){
                return evs?evs.length:"";
            };
        */
        get: function () {
            return this.PluginScheduler.templates.timeline_cell_value;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.timeline_cell_value = _Value;
        }
    });
    Object.defineProperty(this, 'TimelineScalexTyle', {
        /*
            Type: CallBack or value any
            Descripci�n: especifica el nombre de una clase de CSS que se aplicar� a los elementos del eje X.
            CallBack:(la fecha que necesita formatear)
            Value Any: text
            Valor por defecto: none
            Vistas aplicables: Plantillas de vista de l�nea de tiempo
            Example:
            scheduler.templates.timeline_scalex_class = function(date){
                return "";
            };
        */
        get: function () {
            return this.PluginScheduler.templates.timeline_scalex_class;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.timeline_scalex_class = _Value;
        }
    });
    Object.defineProperty(this, 'TimelineSecondScalexTyle', {
        /*
            Type: CallBack or value any
            Descripci�n: especifica el nombre de una clase de CSS que se aplicar� a los elementos del segundo eje X.
            CallBack:(la fecha que necesita formatear)
            Value Any: text
            Valor por defecto: none
            Vistas aplicables: Plantillas de vista de l�nea de tiempo
            Example:
            scheduler.templates.timeline_second_scalex_class = function(date){
                return "";
            };
        */
        get: function () {
            return this.PluginScheduler.templates.timeline_second_scalex_class;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.timeline_second_scalex_class = _Value;
        }
    });
    Object.defineProperty(this, 'TimelineScaleyStyle', {
        /*
            Type: CallBack or value any
            Descripci�n: especifica el nombre de una clase de CSS que se aplicar� a elementos del eje Y
            CallBack: (la identificaci�n de la secci�n, la etiqueta de la secci�n, el objeto de secci�n que contiene las propiedades 'clave' y 'etiqueta')
            Value Any: text
            Valor por defecto: none
            Vistas aplicables: linea de tiempo
            Example:
            scheduler.templates.timeline_scaley_class = function(key, label,  section){
                return "";
            };
        */
        get: function () {
            return this.PluginScheduler.templates.timeline_scaley_class;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.timeline_scaley_class = _Value;
        }
    });
    Object.defineProperty(this, 'TimelineScaleLabel', {
        /*
            Type: CallBack or value any
            Descripci�n: especifica elementos del eje Y
            CallBack: (la identificaci�n de la secci�n, la etiqueta de la secci�n, el objeto de secci�n que contiene las propiedades 'clave' y 'etiqueta')
            Value Any: text
            Valor por defecto: none
            Vistas aplicables: linea de tiempo
            Example:
            scheduler.templates.timeline_scale_label = function(key, label, section){
                return label;
            };
        */
        get: function () {
            return this.PluginScheduler.templates.timeline_scale_label;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.timeline_scale_label = _Value;
        }
    });
    Object.defineProperty(this, 'TimelineTooltip', {
        /*
            Type: CallBack or value any
            Descripci�n: especifica la informaci�n sobre herramientas sobre una celda de d�a que contiene algunos eventos programados
            CallBack:(la fecha cuando un evento est� programado para comenzar, la fecha en que un evento est� programado para completarse,el objeto del evento)
            Value Any: text
            Valor por defecto: none
            Vistas aplicables: linea de tiempo
            Example:
            scheduler.templates.timeline_tooltip = function(start,end,event){
                return event.text;
            };
        */
        get: function () {
            return this.PluginScheduler.templates.timeline_tooltip;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.timeline_tooltip = _Value;
        }
    });
    Object.defineProperty(this, 'TimelineDate', {
        /*
            Type: CallBack or value any
            Descripci�n: especifica la fecha en el encabezado de la vista
            CallBack:(la fecha cuando un evento est� programado para comenzar, la fecha en que un evento est� programado para completarse)
            Value Any: text or date 
            Valor por defecto: none
            Vistas aplicables: linea de tiempo
            Example:
            scheduler.templates.timeline_date = function(date1, date2){
            if (date1.getDay()==date2.getDay() && date2-date1<(24*60*60*1000))
            return scheduler.templates.day_date(date1);
            return scheduler.templates.week_date(date1, date2);
            };
        */
        get: function () {
            return this.PluginScheduler.templates.timeline_date;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.timeline_date = _Value;
        }
    });
    Object.defineProperty(this, 'TimelineScaleDate', {
        /*
            Type: CallBack or value any
            Descripci�n: especifica elementos del eje X
            CallBack:(la fecha que necesita formatear)
            Value Any: text or date
            Valor por defecto: none
            Vistas aplicables: linea de tiempo
            Example:
            scheduler.templates.timeline_scale_date = function(date){
            var timeline = scheduler.matrix.timeline;
            var func=scheduler.date.date_to_str(timeline.x_date||scheduler.config.hour_date);
            return func(date);
            }
        */
        get: function () {
            return this.PluginScheduler.templates.timeline_scale_date;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.timeline_scale_date = _Value;
        }
    });
    Object.defineProperty(this, 'TimelineSecondScaleDate', {
        /*
            Type: CallBack or value any
            Descripci�n: especifica elementos del segundo eje X
            CallBack:(la fecha que necesita formatear)
            Value Any: text or date
            Valor por defecto: none
            Vistas aplicables: linea de tiempo
            Example:
            scheduler.templates.timeline_scale_date = function(date){
            var timeline = scheduler.matrix.timeline;
            var func=scheduler.date.date_to_str(timeline.x_date||scheduler.config.hour_date);
            return func(date);
            }
        */
        get: function () {
            return this.PluginScheduler.templates.timeline_second_scale_date;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.timeline_second_scale_date = _Value;
        }
    });
    Object.defineProperty(this, 'UnitDate', {
        /*
            Type: CallBack or value any
            Descripci�n:especifica la fecha en el encabezado de la vista
            CallBack:(la fecha que necesita formatear)
            Value Any: text or date
            Valor por defecto: none
            Vistas aplicables: unidades
            Example:
            scheduler.templates.unit_date = function(date){
                    return scheduler.templates.day_date(date);
            };
        */
        get: function () {
            return this.PluginScheduler.templates.unit_date;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.unit_date = _Value;
        }
    });
    Object.defineProperty(this, 'UnitScaleText', {
        /*
            Type: CallBack or value any
            Descripci�n: especifica elementos del eje X
            CallBack: (la identificaci�n de la unidad (clave), la etiqueta de la unidad, el objeto de la unidad que contiene las propiedades 'clave' y 'etiqueta')
            Value Any: text or HTML
            Valor por defecto: none
            Vistas aplicables: unidades
            Example:
            scheduler.templates.unit_scale_text = function(key, label, unit) {
            if (option.css) {
            return "<span class='" + option.css + "'>" + label + "</span>";
            } else {
            return label;
            }
            };
        */
        get: function () {
            return this.PluginScheduler.templates.unit_scale_text;
        },
        set: function (_Value) {
            this.PluginScheduler.templates.unit_scale_text = _Value;
        }
    });



}