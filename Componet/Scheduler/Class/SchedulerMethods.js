Componet.Scheduler.TSchedulerMethods = function (plugin, objectScheduler) {
    this.TParent = function () {
        return this;
    }.bind(this);
    this.Scheduler = objectScheduler;
    this.PluginScheduler = plugin;
    Object.defineProperty(this, 'AddEvent', {
        /*
          Type: String
          Parameters: Object Event
          Description: agrega un nuevo evento.El m�todo invoca el evento onEventAdded o onEventChanged
          start_date - ( Date, string ) la fecha, cuando el evento est� programado para comenzar. Si la propiedad se especifica como una cadena, se debe usar el formato "% d-% m-% Y% H:% i" (para cambiar el formato predeterminado, use la opci�n api_date ). Para eventos recurrentes, el valor de la propiedad start_date debe tener el tipo de fecha.
          end_date - ( Fecha, cadena ) la fecha, cuando el evento est� programado para completarse. Si la propiedad se especifica como una cadena, se debe usar el formato "% d-% m-% Y% H:% i" (para cambiar el formato predeterminado, use la opci�n api_date ). Para eventos recurrentes, el valor de la propiedad end_date debe tener el tipo de fecha.
          texto - ( cadena ) el texto del evento.
          id - ( cadena ) la identificaci�n del evento. Si no se especifica, la identificaci�n para el evento se generar� autom�ticamente.
          userdata - ( hash ) una colecci�n de propiedades personalizadas presentadas como pares 'clave-valor'.
          Example:
          scheduler.addEvent({
          start_date: "16-06-2013 09:00",
          end_date:   "16-06-2013 12:00",
          text:   "Meeting",
          id: 1,
          tag: data user
          });
          return: la identificaci�n del evento
        */
        get: function () {
            return this.PluginScheduler.addEvent;
        }
    });
    Object.defineProperty(this, 'AddEventNow', {
        /*
          Type: String
          Parameters: el objeto del evento
          Description: agrega un nuevo evento y abre el lightbox para confirmar
          scheduler.addEventNow();Por defecto, el valor de la fecha actual + time_step . 
          //or
          scheduler.addEventNow({
             start_date: new Date(2013,0,10,8,30),
             end_date:   new Date(2013,0,10,10,30),
             text:   "Meeting",
             holder: "John", //userdata
             room:   "5"     //userdata
          });
          return: la identificaci�n del evento
        */
        get: function () {
            return this.PluginScheduler.addEventNow;
        }
    });
    Object.defineProperty(this, 'AddSection', {
        /*
          Type: el objeto de la secci�n para agregar
          Parameters: grega una secci�n a la vista actualmente activa (si la vista abierta no es la L�nea de tiempo en el modo '�rbol', el m�todo ser� ignorado)El m�todo se usa solo para el modo �rbol
          scheduler.createTimelineView({
          name:   "timeline",
          render:"tree",
          ...
          y_unit:[
                {key:"production", label:"Production Department", children:[
                   {key:"p1", label:"Managers", children:[
                       {key:"pm1", label:"John Williams"},
                       {key:"pm2", label:"David Miller"}
                   ]}
                ]},
                {key:"sales", label:"Sales and Marketing", children:[
                   {key:"s1", label:"Kate Moss"},
                   {key:"s2", label:"Dian Fossey"}
                ]}
             ]
          }); 
          scheduler.addSection( {key:1, label:"James Smith"}, "p1");
          scheduler.addSection( {key:2, label:"Alex White"}, "sales");
          Description: la identificaci�n de la secci�n padre Pase 'nulo' si est� agregando una secci�n a la ra�z
          return: devuelve 'verdadero', si la secci�n se agreg� con �xito y 'falsa' en otros casos (por ejemplo, se especific� parent_id incorrecto).
        */
        get: function () {
            return this.PluginScheduler.addSection;
        }
    });
    Object.defineProperty(this, 'AddShorCut', {
        /*
          Type: Void
          Parameters:String=>el nombre de la clave o el nombre de la combinaci�n de teclas para un acceso directo ( sintaxis de acceso directo ), Function: el controlador de la llamada de atajo, String: el nombre del elemento de contexto para adjuntar la funci�n del controlador a ( lista de �mbitos )
          En caso de que no se establezca el tercer par�metro, el controlador se adjuntar� al �mbito del planificador.
          scheduler.addShortcut("shift+w", function(e){ 
          var eventId = scheduler.locate(e); 
             if(eventId) 
                scheduler.showQuickInfo(eventId);
          },"event");
          Description: agrega un nuevo atajo de teclado
          return: none
        */
        get: function () {
            return this.PluginScheduler.addShortcut;
        }
    });
    Object.defineProperty(this, 'AddEventNative', {
        /*
          Type: String
          Parameters: SchedulerEventName	el nombre del evento, no distingue entre may�sculas y min�sculas, la funci�n de controlador
          Description: conecta el controlador a un evento interno de dhtmlxScheduler
          scheduler.attachEvent("onEventSave",function(id,ev){
          if (!ev.text) {
             alert("Text must not be empty");
             return false;
          }
          return true;
          })  
          return: Identificaci�n de la identificaci�n del controlador de eventos adjunto
        */
        get: function () {
            return this.PluginScheduler.attachEvent;
        }
    });
    Object.defineProperty(this, 'Backbone', {
        /*
          Type: Void
          Parameters: la colecci�n de datos Backbone
          Description: hace que el programador refleje todos los cambios de datos en el modelo Backbone y viceversa
          $(".myscheduler").dhx_scheduler({
             date:new Date(2009,5,25),
             mode:"month"
          });
          scheduler.backbone(events);
          return: none
        */
        get: function () {
            return this.PluginScheduler.backbone;
        }
    });
    Object.defineProperty(this, 'BlockTime', {
        /*
          Type: Void
          Parameters: (Fecha | fecha num�rica , matriz time_points , [ object items ])
          una fecha para bloquear (si se proporciona un n�mero, el par�metro se tratar� como un d�a de la semana : el �ndice '0' se refiere al domingo, '6' al s�bado),
          una matriz [start_minute, end_minute, .., start_minute_N, end_minute_N] , donde cada par establece un cierto rango de l�mites. La matriz puede tener cualquier cantidad de tales pares,
          define elementos espec�ficos de vista (s) para bloquear
          Description: bloquea la fecha especificada y le aplica el estilo predeterminado 'atenuado'.El m�todo est� en desuso. Le recomendamos encarecidamente que utilice el m�todo addMarkedTimespan en su lugar
          blocks events from 0 till 8 hours for each Wednesday 
          BUT only for the items with id=1, id=4 in the Units view
          scheduler.blockTime(3, [0,8*60], { unit: [1,4] });
          return: 
        */
        get: function () {
            return this.PluginScheduler.blockTime;
        }
    });
    Object.defineProperty(this, 'CallEvent', {
        /*
          Type: Boolean
          Parameters: el nombre del evento, no distingue entre may�sculas y min�sculas, una matriz de datos relacionados con eventos
          Description: llama un evento interno
          var res = scheduler.callEvent("CustomEvent", [param1, param2]);
          return: falso , si algunos de los controladores de eventos devuelven falso . De lo contrario, cierto
        */
        get: function () {
            return this.PluginScheduler.callEvent;
        }
    });
    Object.defineProperty(this, 'ChangeEventId', {
        /*
          Type: Void
          Parameters: la identificaci�n del evento actual String, la nueva identificaci�n del evento String
          scheduler.changeEventId("ev15", "ev25"); //changes the event's id "ev15" -> "ev25"
          Description: Cambia la identificaci�n del evento
          return: none
        */
        get: function () {
            return this.PluginScheduler.changeEventId;
        }
    });
    Object.defineProperty(this, 'EventCollision', {
        /*
          Type: Boolean
          Parameters: el objeto del evento
          Description: comprueba si el evento especificado ocurre en el momento que ya ha sido ocupado por otro evento (s)
          var event = {
          text : "New Event",
          start_date : new Date(2013, 02, 20, 10, 00),
          end_date : new Date(2013, 02, 20, 14, 00)
          };
          var isOccupied = scheduler.checkCollision(event); //returns 'true' or 'false'
          return: devuelve true , si la hora del evento ya ha sido ocupada; de lo contrario, es falsa .
        */
        get: function () {
            return this.PluginScheduler.checkCollision;
        }
    });
    Object.defineProperty(this, 'CheckEvent', {
        /*
          Type: Boolean
          Parameters: SchedulerEventName	el nombre del evento
          Description: comprueba si un evento tiene alg�n manejador especificado
          scheduler.attachEvent("onEventSave",function(id,data){
          if (data.text.length<20) {
           alert("Text too small");
           return false;
          }
          return true;
          })
          ...        
          scheduler.checkEvent("onEventSave"); //returns 'true'
          return: devuelve verdadero , si se especifica alg�n controlador para el evento
        */
        get: function () {
            return this.PluginScheduler.checkEvent;
        }
    });
    Object.defineProperty(this, 'checkEventInMarkedTimespan', {
        /*
          Type: Boolean
          Parameters: (evento de objeto , intervalo de tiempo de cadena)
          Description: comprueba si un evento reside en un intervalo de tiempo de un tipo espec�fico
          scheduler.addMarkedTimespan({
          start_date: new Date(2013,4,1), 
          end_date: new Date(2013,7,1), 
          css: "red_section",
          type:"discount"
          });
          var eventId = scheduler.addEvent({
          start_date: "16-05-2013 09:00",
          end_date:   "16-05-2013 12:00",
          text:   "Meeting",
          holder: "John", 
          room:   "5"     
          })
          ...
          scheduler.checkInMarkedTimespan(scheduler.getEvent(eventId), "discount"); //->true
          return: verdadero , si el evento est� en el intervalo de tiempo del tipo especificado
        */
        get: function () {
            return this.PluginScheduler.checkInMarkedTimespan;
        }
    });
    Object.defineProperty(this, 'CheckEventLimitViolation', {
        /*
          Type:Boolean 
          Parameters:  evento de objeto 
          Description: comprueba si el evento especificado tiene lugar en el per�odo de tiempo bloqueado
          var event = {
         text : "New Event",
         start_date : new Date(2013, 02, 20, 10, 00),
         end_date : new Date(2013, 02, 20, 14, 00)
          };
          var isBlocked = scheduler.checkLimitViolation(event); //returns 'true' or 'false'
          return: devuelve true , si el evento ocurre en el tiempo bloqueado; de lo contrario, es falso 
        */
        get: function () {
            return this.PluginScheduler.checkLimitViolation;
        }
    });
    Object.defineProperty(this, 'ClearEventAll', {
        /*
          Type: Void
          Parameters: none
          Description: elimina todos los eventos del planificador
          return: none
        */
        get: function () {
            return this.PluginScheduler.clearAll;
        }
    });
    Object.defineProperty(this, 'CloseAllSection', {
        /*
          Type: void
          Parameters: none
          Description: cierra todas las secciones en la vista actualmente activa (si la vista abierta no es la L�nea de tiempo en el modo '�rbol' - el m�todo ser� ignorado)
          return: none
        */
        get: function () {
            return this.PluginScheduler.closeAllSections;
        }
    });
    Object.defineProperty(this, 'CloseSection', {
        /*
          Type: void
          Parameters: la identificaci�n de la secci�n
          Description: cierra la secci�n especificada en la vista activa actualmente (si la vista abierta no es la L�nea de tiempo en el modo '�rbol' - el m�todo ser� ignorado).El m�todo se usa solo para el modo �rbol
          scheduler.createTimelineView({
          name:   "timeline",
          render:"tree",
          ...
          y_unit:[
           {key:"managers",    label:"Administration", children: [
               {key:1, label:"James Smith"},
               {key:2, label:"John Williams"}
           ]},
           {key:"accounts",    label:"Accounting Department", children: [
               {key:3, label:"David Miller"},
               {key:4, label:"Linda Brown"}           
           ]},
           {key:"sales",       label:"Sales and Marketing"},
           {key:"production",  label:"Production Department"}
          ]
          });
          ...
          scheduler.closeSection("managers");
          return: none
        */
        get: function () {
            return this.PluginScheduler.closeSection;
        }
    });
    Object.defineProperty(this, 'collapseFullCalendar', {
        /*
          Type: Void
          Parameters: none
          Description: colapsa el programador expandido de nuevo al tama�o normal
          return: none
        */
        get: function () {
            return this.PluginScheduler.collapse();
        }
    });
    Object.defineProperty(this, 'CreateGridView', {
        /*
          Type: Void
          Parameters: el objeto de configuraci�n de la vista de Grilla
          Description: crea la vista de cuadr�cula en el programador
          scheduler.createGridView({
          name:"grid",
          fields:[
             {id:"id",       label:'Book Title', width:'*',  align:'right',  sort:'str'},
             {id:"date",     label:'Author',     width:100},
             {id:"text",     label:'Votes',      width:200,  align:'left',   sort:'int'}
          ],
          from:new  Date(2000, 00, 01),
          to:new Date(2013, 00, 01)
          });
          nombre:( cadena ) la identificaci�n de la vista
          campos: ( conjunto de objetos ) configura las columnas de la cuadr�cula.Cada objeto en la matriz especifica una sola columna y puede tomar estas propiedades
          Seleccionar:( Boolean ) habilita / deshabilita la selecci�n en la grilla (de manera predeterminada, la selecci�n est� habilitada)
          altura de la fila	( n�mero ) la altura de las filas en la grilla
          paginaci�n	( booleano ) habilita / deshabilita la navegaci�n con botones  en la grilla (detalles)
          unidad	( minuto, hora, d�a, semana, mes, a�o ) la unidad de tiempo de desplazamiento. Por defecto, 'mes'
          paso	( n�mero ) la cantidad de unidades desplazadas a la vez. Por defecto, 1.
          de	( Fecha ) establece el borde izquierdo del rango de fechas permitidas. Se usa para limitar el rango de fechas en el programador	
          return: none
        */
        get: function () {
            return this.PluginScheduler.createGridView;
        }
    });
    Object.defineProperty(this, 'CreateTimelineView', {
        /*
          Type: Void
          Parameters: config el objeto de configuraci�n de la vista L�nea de tiempo
          Description: crea la vista L�nea de tiempo en el programador
          scheduler.createTimelineView({
           name:      "timeline",
           x_unit:    "minute",
           x_date:    "%H:%i",
           x_step:    30,
           x_size:    24,
           x_start:   16,
           x_length:  48,
           y_unit:[   
             {key:1, label:"Section A"},
             {key:2, label:"Section B"},
             {key:3, label:"Section C"},
             {key:4, label:"Section D"}  
           ],
           y_property: "section_id",
           render:    "bar"
          });
          return: void
        */
        get: function () {
            return this.PluginScheduler.createTimelineView;
        }
    });
    Object.defineProperty(this, 'CreateSectionView', {
        /*
          Type: 
          Parameters: config objeto. El objeto de configuraci�n de la vista Unidades
          Description: crea la vista Unidades en el programador
          scheduler.createUnitsView({
          name:"unit",
          property:"unit_id",
          list:[
             {key:1, label:"Section A"},
             {key:2, label:"Section B"},
             {key:3, label:"Section C"}  
          ]
          });
          scheduler.init('scheduler_here',new Date(2009,5,30),"unit");
          scheduler.parse([
          {start_date:"06/30/2009 09:00",end_date:"06/30/2009 12:00",text:"Task1",unit_id:1},
          {start_date:"06/30/2009 12:00",end_date:"06/30/2009 20:00",text:"Task2",unit_id:3},
          {start_date:"06/30/2009 08:00",end_date:"06/30/2009 12:00",text:"Task3",unit_id:2}
          ],"json");
          nombre	( cadena ) la identificaci�n de la vista. Si especifica el nombre de una vista de Unidades ya existente, se sobrescribir�
          propiedad	( cadena ) el nombre de una propiedad de datos que se usar� para asignar eventos a ciertas unidades
          lista	( conjunto de objetos ) define unidades de la vista. 
          Cada objeto en la matriz especifica una sola unidad y toma estas propiedades:
          clave - ( cadena ) la identificaci�n de la unidad. Este atributo se compara con la propiedad de datos de evento para asignar el evento a una unidad
          label - ( string ) la etiqueta de la unidad
          dias	( n�mero ) una cantidad de art�culos (d�as) en el eje Y
          skip_incorrect	( booleano ) si es verdadero , los eventos que no pertenecen a ninguna de las unidades no se mostrar�n. Si es falso , dichos eventos se asignar�n a la primera unidad. Por defecto - falso . Opcional
          tama�o	( n�mero ) la cantidad de unidades que se deben mostrar en la vista (si el conteo real excede este valor, se mostrar� la barra de desplazamiento). Opcional
          paso	( n�mero ) la cantidad de unidades que se desplazar�n a la vez. Opcional
          return: none
        */
        get: function () {
            return this.PluginScheduler.createUnitsView;
        }
    });
    Object.defineProperty(this, 'DeleteAllSections', {
        /*
          Type: Void
          Parameters: none
          Description: borra todas las secciones de la vista actualmente activa (si la vista abierta no es la L�nea de tiempo en el modo '�rbol', el m�todo ser� ignorado).El m�todo se usa solo para el modo �rbol
          scheduler.deleteAllSections()
          return: none
          vista: Linea de Tiempo
        */
        get: function () {
            return this.PluginScheduler.deleteAllSections;
        }
    });
    Object.defineProperty(this, 'DeleteEvent', {
        /*
          Type: Void
          Parameters: String or Number. 
          Description: borra el evento especificado
          scheduler.deleteEvent(3);
          return: none
          vista: All
        */
        get: function () {
            return this.PluginScheduler.deleteEvent;
        }
    });
    Object.defineProperty(this, 'DeleteMarkedTimespan', {
        /*
          Type: Void
          Parameters: (String or Number)la identificaci�n del tiempo
          Description:elimina el marcado / bloqueo establecido por los m�todos addMarkedTimespan () y blockTime ()
          scheduler.deleteMarkedTimespan(spanID);
          deleteMarkedTimespan () - no toma par�metros y elimina todo el bloqueo / marcado.
          deleteMarkedTimespan (id) : toma la identificaci�n del intervalo de tiempo.
          deleteMarkedTimespan (config) - toma ciertas propiedades de configuraci�n.
          return:none
          vista: Tiempos de espera destacados en la vista de mes
        */
        get: function () {
            return this.PluginScheduler.deleteMarkedTimespan;
        }
    });
    Object.defineProperty(this, 'AddMarkedTimespan', {
        /*
          Type: Void
          Parameters: (String or Number)la identificaci�n del tiempo
          Description:elimina el marcado / bloqueo establecido por los m�todos addMarkedTimespan () y blockTime ()
          scheduler.deleteMarkedTimespan(spanID);
          deleteMarkedTimespan () - no toma par�metros y elimina todo el bloqueo / marcado.
          deleteMarkedTimespan (id) : toma la identificaci�n del intervalo de tiempo.
          deleteMarkedTimespan (config) - toma ciertas propiedades de configuraci�n.
          return:none
          vista: Tiempos de espera destacados en la vista de mes
        */
        get: function () {
            return this.PluginScheduler.addMarkedTimespan;
        }
    });
    Object.defineProperty(this, 'DeleteSection', {
        /*
          Type: Boolean
          Parameters: la identificaci�n de la secci�n
          Description: borra una secci�n de la vista actualmente activa (si la vista abierta no es la L�nea de tiempo en el modo '�rbol' - el m�todo ser� ignorado).
          scheduler.deleteSection("sales");
          return: devuelve ** verdadero **, si la secci�n se elimin� con �xito y ** falso ** en otros casos (por ejemplo, se especific� una secci�n incorrecta).
          vista: l�nea de tiempo
        */
        get: function () {
            return this.PluginScheduler.deleteSection;
        }
    });
    Object.defineProperty(this, 'DestroyCalendar', {
        /*
          Type: Void
          Parameters: el objeto del mini-calendario (si no se especifica, el planificador intenta destruir el �ltimo mini calendario creado)
          Description: destruye el mini-calendario creado previamente
          scheduler.destroyCalendar(calendar);
          return: none
          vista: All
        */
        get: function () {
            return this.PluginScheduler.destroyCalendar;
        }
    });
    Object.defineProperty(this, 'DetachEvent', {
        /*
          Type: Void
          Parameters: (String or Number)la identificaci�n del evento
          Description: separa un controlador de un evento (que se adjunt� anteriormente mediante el m�todo attachEvent)
          scheduler.detachEvent(myEvent);
          return: none
          vista: All
        */
        get: function () {
            return this.PluginScheduler.detachEvent;
        }
    });
    Object.defineProperty(this, 'ShowEditEvent', {
        /*
          Type: Void
          Parameters: (String or Number) la identificaci�n del evento
          Description: abre el editor en l�nea para modificar el texto del evento (el editor en el recuadro del evento)
          scheduler.edit(eventId);
          return: none
          vista: All
        */
        get: function () {
            return this.PluginScheduler.edit;
        }
    });
    Object.defineProperty(this, 'CloseEditEvent', {
        /*
          Type: Void
          Parameters: (String or Number) la identificaci�n del evento
          Description: cierra el editor de eventos en l�nea, si est� abierto actualmente
          scheduler.editStop(eventId);
          return: none
          vista: All
        */
        get: function () {
            return this.PluginScheduler.editStop;
        }
    });
    Object.defineProperty(this, 'CloseModal', {
        /*
          Type: Void
          Parameters: modo booleano , [ HTMLElement box ]
          si se establece en verdadero , los cambios, hechos en el lightbox, se guardar�n antes del cierre.Si - falso , los cambios ser�n cancelados.(Boolean)
          el contenedor HTML para la caja de luz (HTML)
          Description: cierra el editor de eventos en l�nea, si est� abierto actualmente
          scheduler.endLightbox(true, document.getElementById("my_form"));
          scheduler.endLightbox(false);
          return: none
          vista: All
        */
        get: function () {
            return this.PluginScheduler.endLightbox;
        }
    });
    Object.defineProperty(this, 'ConectEventHTML', {
        /*
          Type: Void
          Parameters: (el nodo HTML o su ID, el nombre de un evento HTML (sin el prefijo 'on'), el controlador de eventos, un objeto al que se refiere esta palabra clave )
          adds a handler for the 'onclick' event
          scheduler.event("divId", "click", function(e){
                //e - a native event object
                do_something();
          });
          Description: conecta un controlador de eventos a un elemento HTML
          return: none
          vista: all
        */
        get: function () {
            return this.PluginScheduler.event;
        }
    });
    Object.defineProperty(this, 'EventRemoveHTML', {
        /*
          Type: Void
          Parameters: (string or number) a identificaci�n de un controlador de eventos
          Description: elimina un controlador de eventos de un elemento HTML
          scheduler.eventRemove(eventId);
          return: none
          vista: all
        */
        get: function () {
            return this.PluginScheduler.eventRemove;
        }
    });
    Object.defineProperty(this, 'ExpandFullCalendar', {
        /*
          Type: Void
          Parameters: none
          Description: expande el planificador a la vista de pantalla completa
          scheduler.expand();
          return: none
          vista: all
        */
        get: function () {
            return this.PluginScheduler.expand;
        }
    });
    Object.defineProperty(this, 'Focus', {
        /*
          Type: Void
          Parameters: none
          Description: establece el foco en el programador
          scheduler.focus();
          return: none
          vista: all
        */
        get: function () {
            return this.PluginScheduler.focus;
        }
    });
    Object.defineProperty(this, 'FormSection', {
        /*
          Type: Void
          Parameters: string / section -- (el nombre de una secci�n lightbox)
          Description: establece el foco en el programador
          var time = scheduler.formSection('time');
          var descr = scheduler.formSection('description');
          descr.setValue('abc');
          time.setValue(null,{start_date:new Date(2009,03,10),end_date:new Date(2012,03,10)});
          scheduler.formSection(section);
          return: el objeto de la secci�n (ver los miembros del objeto a continuaci�n)
          vista: all
        */
        get: function () {
            return this.PluginScheduler.FormSection;
        }
    });
    Object.defineProperty(this, 'GetActionData', {
        /*
          Type: Void
          Parameters: Evento e
          Description:  devuelve la fecha y secci�n actual del cursor (si est� definida)
          scheduler.attachEvent("onMouseMove", function(id, e){
             var action_data = scheduler.getActionData(e);
             // -> {date:Tue Jun 30 2009 09:10:00, section:2}
             ...
          })
          return:date - ( Date ) el objeto de la fecha del cursor se�alado, secci�n - ( cadena, n�mero ) la identificaci�n de la secci�n de punta del cursor ( para la l�nea de tiempo y la vista Unidades )
          vista: All
        */
        get: function () {
            return this.PluginScheduler.getActionData;
        }
    });
    Object.defineProperty(this, 'GetEventByID', {
        /*
          Type: Void
          Parameters: (string or number) a identificaci�n de un controlador de eventos
          Description: devuelve el objeto del evento por su id
          var eventId = scheduler.addEvent({
          start_date: "16-05-2013 09:00",
          end_date:   "16-05-2013 12:00",
          text:   "Meeting"
          });
          ...
          var eventObj = scheduler.getEvent(eventId);
          return: devuelve el objeto del evento
          vista: All
        */
        get: function () {
            return this.PluginScheduler.getEvent;
        }
    });
    Object.defineProperty(this, 'GetEventByDate', {
        /*
          Type: Void
          Parameters: (la fecha de inicio del per�odo, la fecha de finalizaci�n del per�odo)
          Description: devuelve una colecci�n de eventos que ocurren durante el per�odo especificado
          var evs = scheduler.getEvents(new Date(2013,1,10),new Date(2013,2,10));
          for (var i=0; i<evs.length; i++){
                 alert(evs[i].text);
          }
          // or
          var evs = scheduler.getEvents();// will return all events
          return: devuelve una colecci�n de eventos que ocurren durante el per�odo especificado
          vista: All
        */
        get: function () {
            return this.PluginScheduler.getEvents;
        }
    });
    Object.defineProperty(this, 'getEventsLabel', {
        /*
          Type: Void
          Parameters: (el nombre de una propiedad de datos que el control est� asignado a, la id de la opci�n Este par�metro se compara con la propiedad de datos del evento para asignar la opci�n de selecci�n a un evento)
          Description: obtiene la etiqueta de un control de selecci�n en la caja de luz
          scheduler.config.lightbox.sections=[
          {name:"custom", type:"select", map_to:"unit_id", options:[
            {key:1, label:"James Smith"},
            {key:2, label:"John Williams"}]},
          ...
          ];
          ...
          var holder2 = scheduler.getLabel("unit_id", 2);// ->"John Williams"
          return: name Label
          vista: All
        */
        get: function () {
            return this.PluginScheduler.getLabel;
        }
    });
    Object.defineProperty(this, 'getModalBox', {
        /*
          Type: Void
          Parameters: none
          Description:obtiene el elemento de objeto HTML de lightbox
          return: el elemento HTML de la caja de luz
          vista: All
        */
        get: function () {
            return this.PluginScheduler.getLightbox;
        }
    });
    Object.defineProperty(this, 'GetRecDates', {
        /*
          Type: Void
          Parameters:(la identificaci�n de un evento recurrente,el n�mero m�ximo de apariciones a devolver (de forma predeterminada, 100) )(cadena de identificaci�n , n�mero de serie )
          Description: devuelve todas las apariciones de un evento recurrente
          var dates = scheduler.getRecDates(22);
          return: objeto (start_date - ( Date ) la fecha de inicio de una �nica ocurrencia - end_date - ( Date ) la fecha de finalizaci�n de una �nica ocurrencia)
          vista: All
        */
        get: function () {
            return this.PluginScheduler.getRecDates;
        }
    });
    Object.defineProperty(this, 'geHTMLEvent', {
        /*
          Type: Void
          Parameters:(string or number) a identificaci�n de un controlador de eventos
          Description: obtiene el objeto del evento que se muestra actualmente
          var eventId = scheduler.addEvent({
          start_date: "16-05-2013 09:00",
          end_date:   "16-05-2013 12:00",
          text:   "Meeting"
          });
          var eventObj = scheduler.getRenderedEvent(eventId);
          //-> <div event_id="123649234723" ...>09:00 Meeting</div>
          return:** el objeto HTML del evento ** - si el evento se muestra actualmente en el programador.** 'nulo' ** - si el evento no se muestra en el programador en el momento de llamar al m�todo. (HTML)
          vista: All
        */
        get: function () {
            return this.PluginScheduler.getRenderedEvent;
        }
    });
    Object.defineProperty(this, 'GetSection', {
        /*
          Type: Void
          Parameters:(string or number) a identificaci�n de un controlador de secci�n
          Description: obtiene el objeto de la secci�n especificada en la vista actualmente activa (si la vista abierta no es la L�nea de tiempo en el modo '�rbol', el m�todo ser� ignorado)
          scheduler.createTimelineView({
          name:   "timeline",
          render:"tree",
          ...
          y_unit:[
          {key:"managers",    label:"Administration"},
          {key:"accounts",    label:"Accounting Department"},
          {key:"sales",       label:"Sales and Marketing"},
          {key:"production",  label:"Production Department"}
          ]
          });
          ...
          scheduler.getSection("sales");//->{key:"sales",label:"Sales and Marketing"}
          return: el objeto de la secci�n
          vista: l�nea de tiempo
        */
        get: function () {
            return this.PluginScheduler.getSection;
        }
    });
    Object.defineProperty(this, 'GetShortcutHandler', {
        /*
          Type: Void
          Parameters: ( cadena de acceso directo , cadena de alcance )el nombre de la clave o el nombre de la combinaci�n de teclas para un acceso directo ( sintaxis de acceso directo ) , el nombre del elemento de contexto para adjuntar la funci�n del controlador a ( lista de �mbitos )
          Description: obtiene un manejador de acceso directo de navegaci�n clave
          var shortcut_handler = scheduler.getShortcutHandler(shortcut, scope);
          return: el controlador de la llamada de atajo
          vista: All
        */
        get: function () {
            return this.PluginScheduler.getShortcutHandler;
        }
    });
    Object.defineProperty(this, 'GetState', {
        /*
          Type: Void
          Parameters: none
          Description: obtiene el estado actual del planificador
          var mode = scheduler.getState().mode;
          if(mode == "day"){
              // custom logic here
          }
          else {
              // custom logic here
          }
          return: el objeto de estado
          vista:All
        */
        get: function () {
            return this.PluginScheduler.getState;
        }
    });
    Object.defineProperty(this, 'GetUserData', {
        /*
          Type: Void
          Parameters: ( cadena de Identificaci�n , cadena de nombre ) -- (la identificaci�n del evento, el nombre de datos del usuario);
          Description: obtiene los datos del usuario asociados con el evento especificado
          var eventId = scheduler.addEvent({
                start_date: "16-06-2013 09:00",
                end_date:   "16-06-2013 12:00",
                text:   "Meeting"
          });
          scheduler.setUserData(eventId, "holder", "John");
          scheduler.getUserData(eventId, "holder");// ->"John"
          return: el valor de los datos del usuario
          vista: All
        */
        get: function () {
            return this.PluginScheduler.getUserData;
        }
    });
    Object.defineProperty(this, 'HideCover', {
        /*
          Type: Void
          Parameters: [ HTMLElement box ] un elemento para esconder
          Description: oculta la superposici�n modal de la caja de luz que bloquea las interacciones con la pantalla restante.Si especifica el par�metro de entrada, el m�todo ocultar� el elemento de objeto HTML especificado (estableciendo la propiedad de visualizaci�n en "ninguno").
          scheduler.hideCover(scheduler.getLightbox());
          return: none
          vista: All
        */
        get: function () {
            return this.PluginScheduler.hideCover;
        }
    });
    Object.defineProperty(this, 'HideQuickInfo', {
        /*
          Type: Void
          Parameters: none
          Description: oculta el formulario de evento emergente (si est� activo actualmente)
          scheduler.hideQuickInfo();
          return: none
          vista: all
        */
        get: function () {
            return this.PluginScheduler.hideQuickInfo;
        }
    });
    Object.defineProperty(this, 'HighlightEventPosition', {
        /*
          Type: Void
          Parameters: el objeto del evento
          Description: resalta la duraci�n del evento en la escala de tiempo
          scheduler.highlightEventPosition(scheduler.getEvent(1));
          return: none
          vista: All
        */
        get: function () {
            return this.PluginScheduler.highlightEventPosition;
        }
    });
    Object.defineProperty(this, 'Initialize', {
        /*
          Type: Void
          Parameters: (un contenedor HTML (o su id) donde se inicializar� un objeto dhtmlxScheduler, la fecha inicial del planificador (por defecto, la fecha actual), el nombre de la vista inicial (por defecto, "semana"))
          scheduler.init("scheduler_here",new Date(2010,0,6),"month");
          Description: un constructor de un objeto dhtmlxScheduler
          return: none
          vista: All
        */
        get: function () {
            return this.PluginScheduler.Init;
        }
    });
    Object.defineProperty(this, 'InvertZones', {
        /*
          Type: Void
          Parameters:(matriz) una matriz ** [start_minute, end_minute, .., start_minute_N, end_minute_N] **donde cada par establece un cierto rango de l�mites (en minutos). La matriz puede tener cualquiercantidad de tales pares
          Description:invierte las zonas horarias especificadas
          var zones = scheduler.invertZones([500, 1000]); // => [0, 500, 1000, 1440]
          return: none
          vista: All
        */
        get: function () {
            return this.PluginScheduler.invertZones;
        }
    });
    Object.defineProperty(this, 'IsCalendarVisible', {
        /*
          Type: Void
          Parameters: none
          Description: comprueba si el calendario est� abierto actualmente en el programador
          var check= scheduler.isCalendarVisible()
          return:  boolean
          vista: All
        */
        get: function () {
            return this.PluginScheduler.isCalendarVisible;
        }
    });
    Object.defineProperty(this, 'IsOneDayEvent', {
        /*
          Type: Void
          Parameters: el objeto del evento
          Description: verifica si el evento especificado es de un d�a o de varios d�as
          var eventId = scheduler.addEvent({
                start_date: "16-06-2013 09:00",
                end_date:   "16-06-2013 12:00",
                text:   "Meeting"
          });
          scheduler.isOneDayEvent(scheduler.getEvent(eventId));//->true
          return: devuelve verdadero , si el evento especificado es de un d�a. De lo contrario, falso
          vista: All
        */
        get: function () {
            return this.PluginScheduler.isOneDayEvent;
        }
    });
    Object.defineProperty(this, 'IsViewExists', {
        /*
          Type: Void
          Parameters: el nombre de la vista
          Description: verifica si existe una vista con el nombre especificado
          scheduler.isViewExists("month"); //->true
          return: verdadero , si la vista existe De lo contrario, falso
          vista: All
        */
        get: function () {
            return this.PluginScheduler.isViewExists;
        }
    });
    Object.defineProperty(this, 'LinkCalendar', {
        /*
          Type: Void
          Parameters: (el mini objeto de calendario,una funci�n que define la diferencia entre las fechas activas en el mini calendario y el programador. La funci�n toma la fecha del planificador como par�metro y devuelve la fecha que se debe mostrar en el mini calendario )
          Description: 'dice' para cambiar la fecha activa en el mini calendario cada vez, se cambia la fecha activa en el programador
          var calendar = scheduler.renderCalendar({
          container:"cal_here",
          navigation:true,
          handler:function(date){
              scheduler.setCurrentView(date, scheduler._mode);
          }
          });mini calendar will always show one month later than the scheduler
          scheduler.linkCalendar(calendar, function(date){
              return scheduler.date.add(date, 1, "month");
          });
          return: none
          vista: All
        */
        get: function () {
            return this.PluginScheduler.linkCalendar;
        }
    });
    Object.defineProperty(this, 'Load', {
        /*
          Type: Void
          Parameters: (a URL del lado del servidor (puede ser un archivo est�tico o un script del lado del servidor que genera datoscomo XML) , ('json', 'xml', 'ical') el tipo de datos. El valor predeterminado - 'xml' ,la funci�n de devoluci�n de llamada )
          Description: carga datos al programador desde una fuente de datos externa
          scheduler.load("data.php",function(){
          alert("Data has been successfully loaded");
          });
          //or
          scheduler.load("data.xml"); //loading data in the XML format
          //or
          scheduler.load("data.ical","ical"); //loading data in the iCal format
          //or
          scheduler.load("data.json","json"); //loading data in the JSON format
          return:none
          vista: All
        */
        get: function () {
            return this.PluginScheduler.load;
        }
    });
    Object.defineProperty(this, 'StyleMiniCalendar', {
        /*
          Type: Void
          Parameters: calendario de objetos , fecha de fecha , cadena css (el objeto del calendario, la fecha para marcar , el nombre de una clase css)
          Description: aplica una clase css a la fecha especificada
          <style>
          my_style{
              color:red !important;//use the 'important' keyword to make sure that
          }                        // the css property will be applied to the specified date
          </style>
          <script>
              var calendar = scheduler.renderCalendar({...});
              ...
              scheduler.markCalendar(calendar, new Date(2010,3,1), "my_style");
          </script>
          return: none
          vista: All
        */
        get: function () {
            return this.PluginScheduler.markCalendar;
        }
    });
    Object.defineProperty(this, 'MarkTimespan', {
        /*
          Type: Void
          Parameters: configuraci�n del objeto
          Description: marca y / o fecha (s) de bloques aplicando el estilo predeterminado o personalizado. El marcado se cancela inmediatamente despu�s de cualquier actualizaci�n interna en la aplicaci�n. Se puede usar para resaltar
          scheduler.markTimespan({
              days:  5,               // marks each Friday
              zones: "fullday",       // marks the entire day
              css:   "gray_section"   // the applied css style
          });
          marks and blocks dates
          scheduler.markTimespan({
              days:  5,
              zones: "fullday",
              css:   "gray_section",
              type:  "dhx_time_block" //the hardcoded value
          });
          return: none
          vista: All
        */
        get: function () {
            return this.PluginScheduler.markTimespan;
        }
    });
    Object.defineProperty(this, 'OpenAllSections', {
        /*
          Type: Void
          Parameters: none
          Description: abre todas las secciones en la vista actualmente activa (si la vista abierta no es la L�nea de tiempo en el modo '�rbol' - el m�todo ser� ignorado)
          scheduler.openAllSections();
          return: none
          vista: Linea de Tiempo
        */
        get: function () {
            return this.PluginScheduler.openAllSections;
        }
    });
    Object.defineProperty(this, 'OpenSection', {
        /*
          Type: Void
          Parameters: la identificaci�n de la secci�n
          Description:abre la secci�n especificada en la vista actualmente activa (si la vista abierta no es la L�nea de tiempo en el modo '�rbol' - el m�todo ser� ignorado)
          scheduler.createTimelineView({
          name:   "timeline",
          render:"tree",
              ...
              y_unit:[
                  {key:"managers",    label:"Administration", children: [
                      {key:1, label:"James Smith"},
                      {key:2, label:"John Williams"}
                  ]},
                  {key:"accounts",    label:"Accounting Department", children: [
                      {key:3, label:"David Miller"},
                      {key:4, label:"Linda Brown"}
                  ]},
                  {key:"sales",       label:"Sales and Marketing"},
                  {key:"production",  label:"Production Department"}
              ]
          });
          ...
          scheduler.openSection("managers");
          return: none
          vista: Linea de Tiempo
        */
        get: function () {
            return this.PluginScheduler.openSection;
        }
    });
    Object.defineProperty(this, 'ParseData', {
        /*
          Type: Void
          Parameters: ( datos de objeto , [ tipo de cadena ]);(una cadena u objeto que representa datos, ( 'json', 'xml', 'ical' ) el tipo de datos. El valor predeterminado - 'xml')
          Description:carga datos de un recurso del lado del cliente
          scheduler.parse([
          { start_date:"2013-05-13 6:00", end_date:"2009-05-13 8:00", text:"Event 1"},
          { start_date:"2013-06-09 6:00", end_date:"2009-06-09 8:00", text:"Event 2"}
          ],"json");
          return: none
          vista: All
        */
        get: function () {
            return this.PluginScheduler.parse;
        }
    });
    Object.defineProperty(this, 'CreateMiniCalendar', {
        /*
          Type: Void
          Parameters: el objeto de configuraci�n del calendario
          var calendar = scheduler.renderCalendar({
          container:"cal_here",
          navigation:true,
          handler:function(date, calendar){
          scheduler.setCurrentView(date, scheduler._mode);
          }
          });
          Description: crea un mini calendario
          return: None
          vista: All
        */
        get: function () {
            return this.PluginScheduler.renderCalendar;
        }
    });
    Object.defineProperty(this, 'Removeshortcut', {
        /*
          Type: Void
          Parameters: atajo de cadena , alcance del objeto. (el nombre de la clave o el nombre de la combinaci�n de teclas para un acceso directo ( sintaxis de acceso directo ),el elemento al que se adjunta el acceso directo ( lista de �mbitos ) )
          Description: elimina un atajo de teclado
          return: none
          vista: All
        */
        get: function () {
            return this.PluginScheduler.removeShortcut;
        }
    });
    Object.defineProperty(this, 'RenderStyleEvent', {
        /*
          Type: Void
          Parameters: contenedor HTMLElement , evento de objeto
          Description: genera contenido HTML para el cuadro de un evento personalizado
          cheduler.renderEvent = function(container, ev) {
          var container_width = container.style.width;
          var html = "<div class='dhx_event_move my_event_move' style='width:" +
          + container_width + "'></div>";
          ...
          container.innerHTML = html;
          return true;
          }
          return: true - el planificador muestra un formulario personalizado, falso - el programador muestra la forma predeterminada
          vista: Tenga en cuenta que el m�todo funciona solo para vistas con la escala vertical, como la vista D�a, la vista Semana, etc.
        */
        get: function () {
            return this.PluginScheduler.renderEvent;
        }
    });
    Object.defineProperty(this, 'ResetElementHTMLModal', {
        /*
          Type: Void
          Parameters: none
          Description: elimina el elemento de objeto HTML de la caja de luz actual.El m�todo se puede usar para cambiar din�micamente la configuraci�n de la caja de luz: llama al m�todo para eliminar el objeto lightbox actual y regenerar uno nuevo en funci�n de la configuraci�n de la caja de luz .
          var full_lightbox = [
          { name: "description", map_to: "text", type: "textarea", focus: true},
          { name: "time",        map_to: "auto", type: "time"}
          ];
          var restricted_lightbox = [
          { name: "description", map_to: "text", type: "textarea", focus: true},
          ];
          ...
          scheduler.attachEvent("onBeforeLightbox", function(event_id) {
          scheduler.resetLightbox();
          var ev = scheduler.getEvent(event_id);  
          if (ev.restricted ==true){
          scheduler.config.lightbox.sections = restricted_lightbox;
          } else {
          scheduler.config.lightbox.sections = full_lightbox;
          };   
          return true;
          }); 
          return: none
          vista: All
        */
        get: function () {
            return this.PluginScheduler.resetLightbox;
        }
    });
    Object.defineProperty(this, 'ScrollUnit', {
        /*
          Type: Void
          Parameters: (Number)el n�mero de unidades para desplazarse ( establecer el valor positivo para desplazar las unidades hacia ellado derecho , el valor negativo hacia el lado izquierdo ).
          Description: desplaza el n�mero especificado de unidades en la vista Unidades
          scheduler.scrollUnit(5);
          return: none
          vista: unidades
        */
        get: function () {
            return this.PluginScheduler.scrollUnit;
        }
    });
    Object.defineProperty(this, 'SelectEvent', {
        /*
          Type: Void
          Parameters: (string or number) a identificaci�n de un controlador de evento
          Description: selecciona el evento especificado
          var eventId = scheduler.addEvent({
          start_date: "16-06-2013 09:00",
          end_date:   "16-06-2013 12:00",
          text:   "Meeting"
          });
          scheduler.select(eventId);
          return: event selected
          vista: All
        */
        get: function () {
            return this.PluginScheduler.select;
        }
    });
    Object.defineProperty(this, 'ServerList', {
        /*
          Type: Void
          Parameters: (el nombre de una lista, una variedad de opciones)
          returns a list of options with the name 'my_list'
          var list = scheduler.serverList("my_list");
          ...
          creates and returns the specified list
          var list = scheduler.serverList("options", [
          {key: 1, label: "John"},
          {key: 2, label: "Adam"},
          {key: 3, label: "Diane"}
          ]);
          Description: devuelve una lista de opciones
          return: devuelve una lista de opciones
          vista: 
        */
        get: function () {
            return this.PluginScheduler.serverList;
        }
    });
    Object.defineProperty(this, 'SetCurrentView', {
        /*
          Type: Void
          Parameters: (la fecha para mostrar, el nombre de una vista para mostrar)
          Description: muestra la vista y la fecha especificadasLos nombres para las vistas predeterminadas son 'd�a', 'semana', 'mes'. Para especificar cualquier otra vista, use su par�metro de nombre .El m�todo invoca onBeforeViewChange , onViewChange .El m�todo es similar a updateView . La �nica diferencia entre los m�todos es que updateView no genera ning�n evento .
          displays the current view and date. Doesn't change anything, just refreshes
          scheduler.setCurrentView();
          displays the 4th July,2012 in the currently active view
          sheduler.setCurrentView(new Date(2012,7,4));
          displays the 3rd May,2012 in the Week view
          scheduler.setCurrentView(new Date(2012,5,3), "week");
          return: none
          vista: All
        */
        get: function () {
            return this.PluginScheduler.setCurrentView;
        }
    });
    Object.defineProperty(this, 'SetEvent', {
        /*
          Type: Void
          Parameters: ( string | id de n�mero , evento de objeto ); (la identificaci�n del evento,el objeto del evento)
          Description: agrega un nuevo evento al grupo de datos del planificador.La diferencia entre los m�todos setEvent () y addEvent () es:El addEvent dibuja el evento en el programador e invoca los onEventAdded / onEventChanged eventos que pueden desencadenar la actualizaci�n de datos en el origen de datos original (por ejemplo, la base de datos).El m�todo setEvent () no invoca ning�n evento y simplemente agrega un evento al grupo de datos. Para dibujar el evento en el programador, debe llamar al m�todo setCurrentView de manera adicional.
          scheduler.setEvent(1, {
          start_date: new Date(2013, 05, 16, 09, 00),
          end_date:   new Date(2013, 05, 16, 12, 00),
          text:   "Meeting",
          holder: "John",
          room:   "5"
          });
          scheduler.setCurrentView();
          return: none
          vista: All
        */
        get: function () {
            return this.PluginScheduler.setEvent;
        }
    });
    Object.defineProperty(this, 'UpdateSizeModal', {
        /*
          Type: Void
          Parameters: none
          Description: fuerza a la caja de luz para cambiar el tama�o.El m�todo se puede usar para actualizar el tama�o de la caja de luz despu�s de ocultar / mostrar alguna secci�n.
          var control = scheduler.formSection("description");
          control.header.style.display = "none";
          scheduler.setLightboxSize();
          return: none
          vista: all 
        */
        get: function () {
            return this.PluginScheduler.setLightboxSize;
        }
    });
    Object.defineProperty(this, 'SetLoadMode', {
        /*
          Type: Void
          Parameters: el modo de carga
          Description: establece el modo que permite cargar datos por partes (habilita la carga din�mica)
          scheduler.config.load_date = "%Y.%m.%d";
          scheduler.init('scheduler_here',new Date(2009,10,1),"month");
          scheduler.setLoadMode("month")
          scheduler.load("data/events.php");
          return: none
          vista: All
        */
        get: function () {
            return this.PluginScheduler.setLoadMode;
        }
    });
    Object.defineProperty(this, 'SetUserAddData', {
        /*
          Type: Void
          Parameters: (la identificaci�n del evento, el nombre de datos del usuario, el valor de los datos del usuario)
          Description: establece los datos del usuario asociados con el evento especificado
          var eventId = scheduler.addEvent({
          start_date: "16-06-2013 09:00",
          end_date:   "16-06-2013 12:00",
          text:   "Meeting"
          });
          scheduler.setUserData(eventId, "holder", "John");
          scheduler.setUserData(eventId, "room", 5);
          return: none
          vista: All
        */
        get: function () {
            return this.PluginScheduler.setUserData;
        }
    });
    Object.defineProperty(this, 'ShowCover', {
        /*
          Type: Void
          Parameters: [ HTMLElement box ] un elemento para esconder
          Description: muestra la superposici�n modal de la caja de luz que bloquea las interacciones con la pantalla restante.Si especifica el par�metro de entrada, el m�todo ocultar� el elemento de objeto HTML especificado (estableciendo la propiedad de visualizaci�n en "ninguno").
          scheduler.showCover(scheduler.getLightbox());
          return: none
          vista: All
        */
        get: function () {
            return this.PluginScheduler.showCover;
        }
    });
    Object.defineProperty(this, 'ShowAndHighlightsEvent', {
        /*
          Type: Void
          Parameters: (la identificaci�n del evento,el nombre de la vista )
          Description: muestra y resalta el evento especificado en la vista actual o especificada
          var eventId = scheduler.addEvent({
          start_date: "08-06-2013 09:00",
          end_date:   "08-06-2013 11:00",
          text:   "Meeting"
          });
          scheduler.showEvent(eventId);
          return: none
          vista: All
        */
        get: function () {
            return this.PluginScheduler.showEvent;
        }
    });
    Object.defineProperty(this, 'OpenModalEvent', {
        /*
          Type: Void
          Parameters: Evento Id
          Description: abre la caja de luz para el evento especificado
          var eventId = scheduler.addEvent({
          start_date: "08-06-2013 09:00",
          end_date:   "08-06-2013 11:00",
          text:   "Meeting"
          });
          scheduler.showLightbox(eventId);
          return: none
          vista: All
        */
        get: function () {
            return this.PluginScheduler.showLightbox;
        }
    });
    Object.defineProperty(this, 'ShowQuickInfo', {
        /*
          Type: Void
          Parameters: la identificaci�n del evento
          Description: muestra el formulario de evento emergente para el evento especificado
          var eventId = scheduler.addEvent({
          start_date: "08-06-2013 09:00",
          end_date:   "08-06-2013 11:00",
          text:   "Meeting"
          });
          window.setTimeout(function(){
          scheduler.showQuickInfo(eventId);
          },1);
          return: none
          vista: all
        */
        get: function () {
            return this.PluginScheduler.showQuickInfo;
        }
    });
    Object.defineProperty(this, 'StartModalCustom ', {
        /*
          Type: Void
          Parameters: ( id. de cadena , cuadro HTMLElement ); (la identificaci�n del evento , el contenedor HTML de la caja de luz)
          Description:muestra un lightbox personalizado en el contenedor HTML especificado centrado en la pantalla
          <div id="my_form">
          </div>
          <script>
          scheduler.showLightbox = function(id) {
          scheduler.startLightbox(id, document.getElementById("my_form"));
          };
          </script>
          return: none
          vista: All
        */
        get: function () {
            return this.PluginScheduler.startLightbox;
        }
    });
    Object.defineProperty(this, 'Ical', {
        /*
          Type: Void
          Parameters: ([ encabezado de cadena ]);
          Description: convierte los datos del planificador al formato ICal
          var str = scheduler.toICal("My calendar");
          return: una cadena de datos en el formato ICal
          vista: All
        */
        get: function () {
            return this.PluginScheduler.TOICal;
        }
    });
    Object.defineProperty(this, 'Json', {
        /*
          Type: Void
          Parameters: none
          Description: convierte los datos del planificador en el formato JSON
          var str = scheduler.toJSON();
          return: una cadena de datos en el formato JSON
          vista: All
        */
        get: function () {
            return this.PluginScheduler.toJSON;
        }
    });
    Object.defineProperty(this, 'Pdf', {
        /*
          Type: Void
          Parameters: (la ruta al convertidor de PDF del lado del servidor, el mapa de color del documento PDF resultante)
          Description: exporta la vista actual a un documento PDF (se puede usar para imprimir)
          El segundo par�metro ( modo ) del m�todo puede tomar solo uno de los valores del conjunto predefinido:
          'color' - impresi�n a todo color (predeterminado)
          'gris' - impresiones en tonos de blanco y negro
          'bw' : solo se imprime en blanco y negro, no hay opciones de color disponibles
          'personalizado' : se puede usar para habilitar un mapa de color personalizado. Requiere codificaci�n de PHP ( detalles )
          'fullcolor' : fondo real y colores de texto que se utilizan durante la exportaci�n
          scheduler.toPDF("./service/generate.php","color");
          return: none
          vista: All
        */
        get: function () {
            return this.PluginScheduler.toPDF;
        }
    });
    Object.defineProperty(this, 'PdfRange', {
        /*
          Type: Void
          Parameters: (la fecha para comenzar a exportar eventos de, la fecha para exportar eventos hasta, el nombre de una vista que la exportaci�n debe aplicarse a,la ruta al archivo php que genera un archivo PDF ( detalles ), el mapa de colores en uso )
          Description: exporta varias vistas del planificador a un documento PDF (se puede usar para imprimir)
          exports pages of the 'week' view from the 1st January, 2012
          till the 1st February, 2012
          scheduler.toPDFRange(new Date(2012,0,1), new Date(2012, 1,1),'week',
          'generate.php', 'fullcolor');
          return: none
          vista: All
        */
        get: function () {
            return this.PluginScheduler.toPDFRange;
        }
    });
    Object.defineProperty(this, 'Xml', {
        /*
          Type: Void
          Parameters: none
          Description: convierte los datos del planificador en el formato XML
          var str = scheduler.toXML();
          return: none
          vista: all
        */
        get: function () {
            return this.PluginScheduler.toXML;
        }
    });
    Object.defineProperty(this, 'GenerateUniqueID', {
        /*
          Type: Void
          Parameters: none
          Description: genera una ID �nica (�nica dentro del planificador actual, no GUID)
          var new_id = scheduler.uid();
          return: id
          vista:all 
        */
        get: function () {
            return this.PluginScheduler.uid;
        }
    });
    Object.defineProperty(this, 'DeleteStyleMiniCalendar', {
        /*
          Type: Void
          Parameters: (el mini objeto de calendario, la fecha para desmarcar, el nombre de una clase CSS para eliminar)
          Description: elimina una clase css de la fecha especificada
          var calendar = scheduler.renderCalendar({
          container:"cal_here",
          navigation:true,
          handler:function(date){
          scheduler.setCurrentView(date, scheduler._mode);
          }
          });
          scheduler.markCalendar(calendar, new Date(2010,3,1), "my_style");
          scheduler.unmarkCalendar(calendar, new Date(2010,3,1), "my_style");
          return: none
          vista: all
        */
        get: function () {
            return this.PluginScheduler.unmarkCalendar;
        }
    });
    Object.defineProperty(this, 'UnmarkTimespan', {
        /*
          Type: Void
          Parameters: ( HTMLElement | array divs )un intervalo de tiempo para eliminar el marcado / bloqueo de (o una serie de intervalos de tiempo)
          Description: elimina el marcado / bloqueo establecido por el m�todo markTimespan ()
          var spanDIV = scheduler.markTimespan({
          days:  [0,6],
          zones: "fullday"
          });
          scheduler.unmarkTimespan(spanDIV);
          return: none
          vista: all
        */
        get: function () {
            return this.PluginScheduler.unmarkTimespan;
        }
    });
    Object.defineProperty(this, 'UnSelectEvent', {
        /*
          Type: Void
          Parameters: [ id de cadena ]la identificaci�n del evento (si no se especifica, el evento seleccionado actualmente no se seleccionar�)
          Description: deselecciona el evento especificado
          var eventId = scheduler.addEvent({
          start_date: "16-06-2013 09:00",
          end_date:   "16-06-2013 12:00",
          text:   "Meeting"
          });
          scheduler.select(eventId);
          scheduler.unselect();
          return: none
          vista: all
        */
        get: function () {
            return this.PluginScheduler.unselect;
        }
    });
    Object.defineProperty(this, 'ShowDateMiniCalendar', {
        /*
          Type: Void
          Parameters: ( calendario de objetos , fecha new_date ); (el mini objeto de calendario, una nueva fecha para mostrar en el mini calendario)
          Description: muestra la fecha especificada en el mini calendario
          var calendar = scheduler.renderCalendar({
          container:"cal_here",
          navigation:true,
          handler:function(date){
          scheduler.setCurrentView(date, scheduler._mode);
          }
          });
          scheduler.updateCalendar(calendar, new Date(2013,01,01))
          return: none
          vista: all
        */
        get: function () {
            return this.PluginScheduler.updateCalendar;
        }
    });
    Object.defineProperty(this, 'UpdateCollection', {
        /*
          Type: Void
          Parameters: ( colecci�n de cadenas , opciones de matriz ); (el nombre de la colecci�n para actualizar,los nuevos valores de la colecci�n )
          Description: actualiza la colecci�n especificada con nuevas opciones
          scheduler.config.lightbox.sections=[
          {name:"description", height:130, map_to:"text", type:"textarea" , focus:true},
          {name:"items", height:23, type:"select",
          options:scheduler.serverList("goods", goods_array), map_to:"section_id" },
          {name:"time", height:72, type:"time", map_to:"auto"}
          ];
          return: cierto , si la actualizaci�n fue exitosa; falso , si no se encontr� la colecci�n
          vista: all
        */
        get: function () {
            return this.PluginScheduler.updateCollection;
        }
    });
    Object.defineProperty(this, 'UpdateEvent', {
        /*
          Type: Void
          Parameters: la identificaci�n del evento
          Description: actualiza el evento especificado
          var eventId = scheduler.addEvent({
          start_date: "16-06-2013 09:00",
          end_date:   "16-06-2013 12:00",
          text:   "Meeting"
          });
          scheduler.getEvent(eventId).text = "Conference"; //changes event's data
          scheduler.updateEvent(eventId); // renders the updated event
          return: none
          vista: all
        */
        get: function () {
            return this.PluginScheduler.updateEvent;
        }
    });
    Object.defineProperty(this, 'UpdateView', {
        /*
          Type: Void
          Parameters:  ([ Fecha de la fecha , vista de la cadena ]); ((opcional) la fecha para establecer,(opcional) el nombre de la vista )
          Description: muestra la vista y la fecha especificadas (no invoca ning�n evento)
          displays the current view and date. Doesn't change anything, just refreshes
          scheduler.updateView();
          displays the 4th July,2012 in the currently active view
          scheduler.updateView(new Date(2012,7,4));
          displays the 3rd May,2012 in the Week view
          scheduler.updateView(new Date(2012,5,3), "week");
          return: none
          vista: all
        */
        get: function () {
            return this.PluginScheduler.updateView;
        }
    });
}