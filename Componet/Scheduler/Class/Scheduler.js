Componet.Scheduler.TScheduler = function (inObject, id, incallback) {
    this.TParent = function () {
        return this;
    }.bind(this);
    this.ObjectHtml = inObject;
    this.CallbackModalResult = incallback;
    this.Id = id;
    this.PluginScheduler = new Scheduler.getSchedulerInstance();
    this.Config = new Componet.Scheduler.TSchedulerConfig(this.PluginScheduler, this);
    this.Events = new Componet.Scheduler.TScheduleEvents(this.PluginScheduler, this);
    this.Methods = new Componet.Scheduler.TSchedulerMethods(this.PluginScheduler, this);
    this.Style = new Componet.Scheduler.TSchedulerStyle(this.PluginScheduler, this);
    this.Template = new Componet.Scheduler.TSchedulerTemplate(this.PluginScheduler, this);
    this.Information = new Componet.Scheduler.TSchedulerInformation(this.PluginScheduler, this);
    this.Section = null;
    this.SectionChild = new Array();
    this.SectionEvents = new Array();
    this.BtnSaveClick = null;
    this.targetEventDelete = false;
    this.MarkBlockDay = new Array();
    this.MarkDateYearInit = null;
    this.MarkDateYearFinish = null;
    this.MarkDateYearColor = "green";
    //
    this.Mythis = "TScheduler";
    UsrCfg.Traslate.GetLangText(this.Mythis, "DEFAULT", "Default");
    UsrCfg.Traslate.GetLangText(this.Mythis, "SELECTSECTIONEDIT", "Select a section before editing.");
    UsrCfg.Traslate.GetLangText(this.Mythis, "EDITNOTSECTION", "You can not edit a section by default.");
    UsrCfg.Traslate.GetLangText(this.Mythis, "SELECTSECTIONDELETE", "Select a section before deleting.");
    UsrCfg.Traslate.GetLangText(this.Mythis, "DELETENOTSECTION", "You can not delete a section by default.");
}
Componet.Scheduler.TScheduler.prototype.InitializeDesigner = function () {
    var _this = this.TParent();
    try {
        if (Source.Menu.IsMobil) {
            var DivitionsConatiner = new TVclStackPanel(_this.ObjectHtml, "DivitionsConatiner" + "_" + _this.Id, 2, [[12, 12], [12, 12], [12, 12], [12, 12], [12, 12]]);
            var SectionIcons = new TVclStackPanel(DivitionsConatiner.Column[0].This, "DivitionsConatiner" + "_" + _this.Id, 1, [[12], [12], [12], [12], [12]]);
            var BtnAdd = new TVclButton(SectionIcons.Column[0].This, "BtnAdd" + "_" + _this.Id);
            BtnAdd.VCLType = TVCLType.BS;
            BtnAdd.Src = "image/16/add.png";
            BtnAdd.This.style.marginRight = "15px";
            BtnAdd.onClick = function myfunction() {
                try {
                    $(_this.SectionAdd.Row.This).show(500);
                    $(_this.BtnLinkSave).show();
                    $(_this.BtnLinkEdit).hide();
                } catch (e) {
                    SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype.InitializeDesigner BtnAdd.onClick()", e);
                }
            }
            var BtnEdit = new TVclButton(SectionIcons.Column[0].This, "BtnEdit" + "_" + _this.Id);
            BtnEdit.VCLType = TVCLType.BS;
            BtnEdit.Src = "image/16/edit.png";
            BtnEdit.This.style.marginRight = "15px";
            BtnEdit.onClick = function myfunction() {
                try {
                    $(_this.SectionAdd.Row.This).show(500);
                    $(_this.BtnLinkEdit).show();
                    $(_this.BtnLinkSave).hide();
                } catch (e) {
                    SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype.InitializeDesigner BtnEdit.onClick()", e);
                }
            }
            var BtnDelete = new TVclButton(SectionIcons.Column[0].This, "BtnDelete" + "_" + _this.Id);
            BtnDelete.VCLType = TVCLType.BS;
            BtnDelete.Src = "image/16/delete.png";
            BtnDelete.onClick = function myfunction() {
                try {
                    $(_this.SectionAdd.Row.This).hide(500);
                    $(_this.BtnLinkEdit).hide();
                    $(_this.BtnLinkSave).hide();
                    _this._BtnDeleteClick();
                } catch (e) {
                    SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype.InitializeDesigner BtnDelete.onClick()", e);
                }
            }
            _this.SectionAdd = new TVclStackPanel(DivitionsConatiner.Column[0].This, "SectionAdd" + "_" + _this.Id, 2, [[10, 2], [10, 2], [10, 2], [10, 2], [10, 2]]);
            _this.SectionAdd.Row.This.style.paddingTop = "10px"
            _this.TextSave = new TVclTextBox(_this.SectionAdd.Column[0].This, "TextSave" + "_" + _this.Id);
            _this.TextSave.VCLType = TVCLType.BS;
            _this.BtnLinkSave = new Ua(_this.SectionAdd.Column[1].This, "BtnLinkSave" + "_" + _this.Id, "");
            _this.BtnLinkSave.style.cursor = "pointer";
            _this.BtnLinkSave.style.marginTop = "10px";
            var BtnSaveSection = new TVclImagen(_this.BtnLinkSave, "BtnSaveSection" + "_" + _this.Id);
            BtnSaveSection.Src = "image/16/Accept.png";
            BtnSaveSection.Title = "Save";
            $(_this.BtnLinkSave).click(function () {
                try {
                    _this._BtnSaveClick();
                } catch (e) {
                    SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype.InitializeDesigner $(_this.BtnLinkSave).click()", e);
                }
            });
            _this.BtnLinkEdit = new Ua(_this.SectionAdd.Column[1].This, "BtnLinkEdit" + "_" + _this.Id, "");
            _this.BtnLinkEdit.style.cursor = "pointer";
            _this.BtnLinkEdit.style.marginTop = "10px";
            var BtnEditSection = new TVclImagen(_this.BtnLinkEdit, "BtnEditSection" + "_" + _this.Id);
            BtnEditSection.Src = "image/16/Refresh.png";
            BtnEditSection.Title = "Edit";
            $(_this.BtnLinkEdit).click(function () {
                try {
                    _this._BtnEditClick();
                } catch (e) {
                    SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype.InitializeDesigner $(_this.BtnLinkEdit).click()", e);
                }
            });
            var SectionItems = new TVclStackPanel(DivitionsConatiner.Column[0].This, "SectionItems" + "_" + _this.Id, 1, [[12], [12], [12], [12], [12]]);
            SectionItems.Row.This.style.paddingTop = "10px";
            _this.ListItem = new Udiv(SectionItems.Column[0].This, "ListItem" + "_" + _this.Id);
            _this.ListItem.style.minHeight = "300px";
            _this.ListItem.style.border = "1px solid #cecece"
            _this.ListItem.className = "list-group";
            $(document).on("click", ".list-group a", function (e) {
                try {
                    e.preventDefault()
                    $that = $(this);
                    $that.parent().find('a').removeClass('active');
                    $that.addClass('active');
                } catch (e) {
                    SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype.InitializeDesigner", e);
                }
            });
            var ContentInitScheduler = new Udiv(DivitionsConatiner.Column[1].This, "ContentInitScheduler" + "_" + _this.Id);
            ContentInitScheduler.className = "container-fluid";
            ContentInitScheduler.style.paddingRight = "0px";
            ContentInitScheduler.style.paddingLeft = "0px";
            _this.ContentSchedulerHere = new Udiv(ContentInitScheduler, "ContentSchedulerHere" + "_" + _this.Id);
            _this.ContentSchedulerHere.className = "dhx_cal_container panel";
            _this.ContentSchedulerHere.style.height = "1200px";
            _this.ContentSchedulerHere.style.border = "1px solid #cecece";
            var ContentDhxCalNavLine = new Udiv(_this.ContentSchedulerHere, "ContentDhxCalNavLine" + "_" + _this.Id)
            ContentDhxCalNavLine.className = "dhx_cal_navline";
            var ContentDhxCalPrevButton = new Udiv(ContentDhxCalNavLine, "ContentDhxCalPrevButton" + "_" + _this.Id);
            ContentDhxCalPrevButton.className = "dhx_cal_prev_button";
            var ContentDhxCalNextButton = new Udiv(ContentDhxCalNavLine, "ContentDhxCalNextButton" + "_" + _this.Id);
            ContentDhxCalNextButton.className = "dhx_cal_next_button";
            var ContentDhxCalDate = new Udiv(ContentDhxCalNavLine, "ContentDhxCalDate" + "_" + _this.Id);
            ContentDhxCalDate.className = "dhx_cal_date";
            ContentDhxCalDate.style.left = "14px";
            ContentDhxCalDate.style.width = "0px";
            var ContentDhxCalHeader = new Udiv(_this.ContentSchedulerHere, "ContentDhxCalHeader" + "_" + _this.Id);
            ContentDhxCalHeader.className = "dhx_cal_header";
            var ContentDhxCalData = new Udiv(_this.ContentSchedulerHere, "ContentDhxCalData" + "_" + _this.Id);
            ContentDhxCalData.className = "dhx_cal_data";
            $(_this.SectionAdd.Row.This).hide();
            $(_this.BtnLinkEdit).hide();
            $(_this.BtnLinkSave).hide();
        }
        else {
            var DivitionsConatiner = new TVclStackPanel(_this.ObjectHtml, "DivitionsConatiner" + "_" + _this.Id, 2, [[3, 9], [3, 9], [3, 9], [3, 9], [3, 9]]);
            var SectionIcons = new TVclStackPanel(DivitionsConatiner.Column[0].This, "DivitionsConatiner" + "_" + _this.Id, 1, [[12], [12], [12], [12], [12]]);
            var BtnAdd = new TVclButton(SectionIcons.Column[0].This, "BtnAdd" + "_" + _this.Id);
            BtnAdd.VCLType = TVCLType.BS;
            BtnAdd.Src = "image/16/add.png";
            BtnAdd.This.style.marginRight = "15px";
            BtnAdd.onClick = function myfunction() {
                try {
                    $(_this.SectionAdd.Row.This).show(500);
                    $(_this.BtnLinkSave).show();
                    $(_this.BtnLinkEdit).hide();
                } catch (e) {
                    SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype.InitializeDesigner BtnAdd.onClick()", e);
                }
            }
            var BtnEdit = new TVclButton(SectionIcons.Column[0].This, "BtnEdit" + "_" + _this.Id);
            BtnEdit.VCLType = TVCLType.BS;
            BtnEdit.Src = "image/16/edit.png";
            BtnEdit.This.style.marginRight = "15px";
            BtnEdit.onClick = function myfunction() {
                try {
                    $(_this.SectionAdd.Row.This).show(500);
                    $(_this.BtnLinkEdit).show();
                    $(_this.BtnLinkSave).hide();
                } catch (e) {
                    SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype.InitializeDesigner BtnEdit.onClick()", e);
                }
            }
            var BtnDelete = new TVclButton(SectionIcons.Column[0].This, "BtnDelete" + "_" + _this.Id);
            BtnDelete.VCLType = TVCLType.BS;
            BtnDelete.Src = "image/16/delete.png";
            BtnDelete.onClick = function myfunction() {
                try {
                    $(_this.SectionAdd.Row.This).hide(500);
                    $(_this.BtnLinkEdit).hide();
                    $(_this.BtnLinkSave).hide();
                    _this._BtnDeleteClick();
                } catch (e) {
                    SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype.InitializeDesigner BtnDelete.onClick()", e);
                }
            }
            _this.SectionAdd = new TVclStackPanel(DivitionsConatiner.Column[0].This, "SectionAdd" + "_" + _this.Id, 2, [[10, 2], [10, 2], [10, 2], [10, 2], [10, 2]]);
            _this.SectionAdd.Row.This.style.paddingTop = "10px"
            _this.TextSave = new TVclTextBox(_this.SectionAdd.Column[0].This, "TextSave" + "_" + _this.Id);
            _this.TextSave.VCLType = TVCLType.BS;
            _this.BtnLinkSave = new Ua(_this.SectionAdd.Column[1].This, "BtnLinkSave" + "_" + _this.Id, "");
            _this.BtnLinkSave.style.cursor = "pointer";
            _this.BtnLinkSave.style.marginTop = "10px";
            var BtnSaveSection = new TVclImagen(_this.BtnLinkSave, "BtnSaveSection" + "_" + _this.Id);
            BtnSaveSection.Src = "image/16/Accept.png";
            BtnSaveSection.Title = "Save";
            $(_this.BtnLinkSave).click(function () {
                try {
                    _this._BtnSaveClick();
                } catch (e) {
                    SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype.InitializeDesigner $(_this.BtnLinkSave).click()", e);
                }
            });
            _this.BtnLinkEdit = new Ua(_this.SectionAdd.Column[1].This, "BtnLinkEdit" + "_" + _this.Id, "");
            _this.BtnLinkEdit.style.cursor = "pointer";
            _this.BtnLinkEdit.style.marginTop = "10px";
            var BtnEditSection = new TVclImagen(_this.BtnLinkEdit, "BtnEditSection" + "_" + _this.Id);
            BtnEditSection.Src = "image/16/Refresh.png";
            BtnEditSection.Title = "Edit";
            $(_this.BtnLinkEdit).click(function () {
                try {
                    _this._BtnEditClick();
                } catch (e) {
                    SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype.InitializeDesigner $(_this.BtnLinkEdit).click()", e);
                }
            });
            var SectionItems = new TVclStackPanel(DivitionsConatiner.Column[0].This, "SectionItems" + "_" + _this.Id, 1, [[12], [12], [12], [12], [12]]);
            SectionItems.Row.This.style.paddingTop = "10px";
            _this.ListItem = new Udiv(SectionItems.Column[0].This, "ListItem" + "_" + _this.Id);
            _this.ListItem.style.minHeight = "300px";
            _this.ListItem.style.border = "1px solid #cecece"
            _this.ListItem.className = "list-group";
            $(document).on("click", ".list-group a", function (e) {
                try {
                    e.preventDefault()
                    $that = $(this);
                    $that.parent().find('a').removeClass('active');
                    $that.addClass('active');
                } catch (e) {
                    SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype.InitializeDesigner", e);
                }
            });
            var ContentInitScheduler = new Udiv(DivitionsConatiner.Column[1].This, "ContentInitScheduler" + "_" + _this.Id);
            ContentInitScheduler.className = "container-fluid";
            _this.ContentSchedulerHere = new Udiv(ContentInitScheduler, "ContentSchedulerHere" + "_" + _this.Id);
            _this.ContentSchedulerHere.className = "dhx_cal_container panel";
            _this.ContentSchedulerHere.style.height = "1200px";
            _this.ContentSchedulerHere.style.border = "1px solid #cecece";
            var ContentDhxCalNavLine = new Udiv(_this.ContentSchedulerHere, "ContentDhxCalNavLine" + "_" + _this.Id)
            ContentDhxCalNavLine.className = "dhx_cal_navline";
            var ContentDhxCalPrevButton = new Udiv(ContentDhxCalNavLine, "ContentDhxCalPrevButton" + "_" + _this.Id);
            ContentDhxCalPrevButton.className = "dhx_cal_prev_button";
            var ContentDhxCalNextButton = new Udiv(ContentDhxCalNavLine, "ContentDhxCalNextButton" + "_" + _this.Id);
            ContentDhxCalNextButton.className = "dhx_cal_next_button";
            var ContentDhxCalTodayButton = new Udiv(ContentDhxCalNavLine, "ContentDhxCalTodayButton" + "_" + _this.Id);
            ContentDhxCalTodayButton.className = "dhx_cal_today_button";
            var ContentDhxCalDate = new Udiv(ContentDhxCalNavLine, "ContentDhxCalDate" + "_" + _this.Id);
            ContentDhxCalDate.className = "dhx_cal_date";
            _this.BtnDay = new Udiv(ContentDhxCalNavLine, "ContentDhxCalTab0" + "_" + _this.Id);
            _this.BtnDay.setAttribute("name", "day_tab");
            _this.BtnDay.className = "dhx_cal_tab";
            _this.BtnWeek = new Udiv(ContentDhxCalNavLine, "ContentDhxCalTab1" + "_" + _this.Id);
            _this.BtnWeek.setAttribute("name", "week_tab");
            _this.BtnWeek.className = "dhx_cal_tab";
            _this.BtnMonth = new Udiv(ContentDhxCalNavLine, "ContentDhxCalTab2" + "_" + _this.Id);
            _this.BtnMonth.setAttribute("name", "month_tab");
            _this.BtnMonth.className = "dhx_cal_tab";
            _this.BtnYear = new Udiv(ContentDhxCalNavLine, "ContentDhxCalTab3" + "_" + _this.Id);
            _this.BtnYear.setAttribute("name", "year_tab");
            _this.BtnYear.className = "dhx_cal_tab";
            _this.BtnSection = new Udiv(ContentDhxCalNavLine, "ContentDhxCalTab4" + "_" + _this.Id);
            _this.BtnSection.setAttribute("name", "unit_tab");
            _this.BtnSection.className = "dhx_cal_tab";
            var ContentDhxCalHeader = new Udiv(_this.ContentSchedulerHere, "ContentDhxCalHeader" + "_" + _this.Id);
            ContentDhxCalHeader.className = "dhx_cal_header";
            var ContentDhxCalData = new Udiv(_this.ContentSchedulerHere, "ContentDhxCalData" + "_" + _this.Id);
            ContentDhxCalData.className = "dhx_cal_data";
            $(_this.SectionAdd.Row.This).hide();
            $(_this.BtnLinkEdit).hide();
            $(_this.BtnLinkSave).hide();
        }
        _this.Events.OnViewChange = function (obj) {
            try {
                if (_this.MarkDateYearInit !== null && _this.MarkDateYearFinish && obj.DataInEvent.NewView == "year") {
                    $(".dhx_year_body td").each(function () {
                        var itemDate = new Date($(this).attr("aria-label"));
                        var initDate = new Date(_this.MarkDateYearInit);
                        var finishDate = new Date(_this.MarkDateYearFinish);
                        if (SysCfg.DateTimeMethods.ExtensionDate.inRange(itemDate, initDate, finishDate)) {
                            $(this).find(".dhx_month_head").css("background-color", _this.MarkDateYearColor);
                            $(this).data("mark", true);
                        }
                    });
                }
            } catch (e) {
                SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype.InitializeDesigner  _this.Events.OnViewChange ", e);
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype.InitializeDesigner", e);
    }
}
Componet.Scheduler.TScheduler.prototype.CreateUnitsSectionsDefault = function () {
    var _this = this.TParent();
    try {
        //DESIGNER
        _this.InitializeDesigner()
        //
        //ADD INFROMATION OBJECT //
        var objSection = new _this.Sections();
        objSection.Id = 1;
        objSection.Name = "unit";
        objSection.MapProperty = "Section_Id";
        objSection.Description = "Description Section";
        var SectionOptions = new _this.SectionOptions();
        SectionOptions.Id = -1;
        SectionOptions.IdSection = 1;
        SectionOptions.Name = UsrCfg.Traslate.GetLangText(_this.Mythis, "DEFAULT")
        SectionOptions.Description = "Default Description";
        var ItemDefault = Ua(_this.ListItem, "", SectionOptions.Name);
        ItemDefault.className = "list-group-item active default";
        ItemDefault.href = "#";
        $(ItemDefault).data('Id', SectionOptions.Id)
        SectionOptions.Html = ItemDefault;
        _this.SectionChild.push(SectionOptions);
        objSection.ListOptions = _this.SectionChild;
        _this.Section = objSection;
        //
        // CONFIGURATE LIST
        var list = _this.Methods.ServerList.call(_this.PluginScheduler, "options", [
            { key: SectionOptions.Id, label: SectionOptions.Name }
        ]);
        //
        //CONFIGURATE MODAL
        _this.Config.ModalBox = [
            { name: "title", height: 35, map_to: "title", type: "textarea" },
            { name: "description", height: 80, map_to: "text", type: "textarea" },
            { name: "sections", height: 23, type: "select", options: list, map_to: objSection.MapProperty },
            { name: "time", height: 72, type: "time", map_to: "auto" },
        ];
        //
        //CONFIGURATE UNIT SECTIONS
        _this.Methods.CreateSectionView({
            name: objSection.Name,
            property: objSection.MapProperty,
            list: list
        });
        //
        //
        //CREATE COMPONENT GENERAL PLUGIN
        _this.PluginScheduler.locale.labels.unit_tab = "Sections"
        _this._FixPlugin();
        _this.PluginScheduler.init(_this.ContentSchedulerHere.id, new Date(), objSection.Name);
        //
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype.CreateUnitsSectionsDefault", e);
    }
}
Componet.Scheduler.TScheduler.prototype.Sections = function () {
    this.Id = "1";
    this.Name = "";
    this.MapProperty = "";
    this.Description = "";
    this.ListOptions = new Array();
    this.Tag = "";
}
Componet.Scheduler.TScheduler.prototype.SectionOptions = function () {
    this.Id = "";
    this.IdSection = 1;
    this.Name = "";
    this.Description = "";
    this.Html = "";
    this.Events = new Array();
    this.Tag = "";
}
Componet.Scheduler.TScheduler.prototype.Event = function () {
    this.Id = "";
    this.IdSection = 1;
    this.IdSectionOption = 1;
    this.Title = "";
    this.Description = "";
    this.ColorText = "";
    this.Background = "";
    this.TimeStart = "";
    this.TimeEnd = "";
    this.Tag = "";
}
Componet.Scheduler.TScheduler.prototype.ConfigMark = function () {
    this.Id = "";
    this.StarDate = "";
    this.EndDate = "";
    this.NameClass = "";
    this.Html = "";
}
Componet.Scheduler.TScheduler.prototype._BtnSaveClick = function () {
    var _this = this.TParent();
    try {
        if (typeof _this.BtnSaveClick === "function") {
            var sectionOption = new _this.SectionOptions();
            sectionOption.IdSection = _this.Section.Id;
            var action = _this.BtnSaveClick(_this, sectionOption, _this.BtnLinkSave);
            if (action) {
                var listNew = new Array();
                var oldList = _this.Methods.ServerList.call(_this.PluginScheduler, "options");
                listNew.push({ key: sectionOption.Id, label: sectionOption.Name });
                for (var i = 0; i < oldList.length; i++) {
                    listNew.push(oldList[i]);
                }
                _this.Methods.UpdateCollection.call(_this.PluginScheduler, "options", listNew);
                if (sectionOption.Events.length > 0) {
                    var listEvent = new Array();
                    for (var i = 0; i < sectionOption.Events.length; i++) {
                        var obj = {};
                        obj["id"] = sectionOption.Events[i].Id;
                        obj["start_date"] = sectionOption.Events[i].TimeStart;
                        obj["end_date"] = sectionOption.Events[i].TimeEnd;
                        obj["title"] = sectionOption.Events[i].Title;
                        obj["text"] = sectionOption.Events[i].Description;
                        obj[_this.Section.MapProperty] = sectionOption.Events[i].IdSectionOption;
                        obj["color"] = sectionOption.Events[i].Background;
                        obj["textColor"] = sectionOption.Events[i].ColorText;
                        listEvent.push(obj);
                        _this.SectionEvents.push(sectionOption.Events[i]);
                    }
                    _this.Methods.ParseData.call(_this.PluginScheduler, listEvent, "json");
                }
                var Item = Ua(_this.ListItem, "", sectionOption.Name);
                Item.className = "list-group-item";
                Item.href = "#";
                $(Item).data('Id', sectionOption.Id)
                sectionOption.Html = Item;
                _this.SectionChild.push(sectionOption);
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype._BtnSaveClick", e);
    }
}
Componet.Scheduler.TScheduler.prototype.SaveSection = function (ListObjSection) {
    var _this = this.TParent();
    var success = false;
    try {
        if (Array.isArray(ListObjSection)) {
            //ADD SECTION AND INTERNAL DATA OBJECT
            var listNew = new Array();
            for (var i = 0; i < ListObjSection.length; i++) {
                listNew.push({ key: ListObjSection[i].Id, label: ListObjSection[i].Name })
                var Item = Ua(_this.ListItem, "", ListObjSection[i].Name);
                Item.className = "list-group-item";
                Item.href = "#";
                $(Item).data('Id', ListObjSection[i].Id);
                ListObjSection[i].Html = Item;
                _this.SectionChild.push(ListObjSection[i]);
            }
            var oldList = _this.Methods.ServerList.call(_this.PluginScheduler, "options");
            for (var x = 0; x < oldList.length; x++) {
                listNew.push(oldList[x]);
            }
            _this.Methods.UpdateCollection.call(_this.PluginScheduler, "options", listNew);
            //ADD EVENTS
            for (var x = 0; x < ListObjSection.length; x++) {
                if (ListObjSection[x].Events.length > 0) {
                    var listEvent = new Array();
                    for (var y = 0; y < ListObjSection[x].Events.length; y++) {
                        var obj = {};
                        obj["id"] = ListObjSection[x].Events[y].Id;
                        obj["start_date"] = ListObjSection[x].Events[y].TimeStart;
                        obj["end_date"] = ListObjSection[x].Events[y].TimeEnd;
                        obj["title"] = ListObjSection[x].Events[y].Title;
                        obj["text"] = ListObjSection[x].Events[y].Description;
                        obj[_this.Section.MapProperty] = ListObjSection[x].Events[y].IdSectionOption;
                        obj["color"] = ListObjSection[x].Events[y].Background;
                        obj["textColor"] = ListObjSection[x].Events[y].ColorText;
                        listEvent.push(obj);
                        _this.SectionEvents.push(ListObjSection[x].Events[y]);
                    }
                    _this.Methods.ParseData.call(_this.PluginScheduler, listEvent, "json");
                }
            }
            //
            success = true;
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype.SaveSection", e);
    }
    return (success);
}
Componet.Scheduler.TScheduler.prototype.SaveEvents = function (ListObjEvents) {
    var _this = this.TParent();
    var success = false;
    try {
        if (Array.isArray(ListObjEvents)) {
            var listEvent = new Array();
            for (var i = 0; i < ListObjEvents.length; i++) {
                var obj = {};
                obj["id"] = ListObjEvents[i].Id;
                obj["start_date"] = ListObjEvents[i].TimeStart;
                obj["end_date"] = ListObjEvents[i].TimeEnd;
                obj["title"] = ListObjEvents[i].Title;
                obj["text"] = ListObjEvents[i].Description;
                obj["color"] = ListObjEvents[i].Background;
                obj["textColor"] = ListObjEvents[i].ColorText;
                var sectionOption = _this.GetSectionChild(ListObjEvents[i].IdSectionOption);
                if (sectionOption) {
                    obj[_this.Section.MapProperty] = ListObjEvents[i].IdSectionOption;
                    sectionOption.Events.push(ListObjEvents[i]);
                }
                else {
                    obj[_this.Section.MapProperty] = -1;
                    ListObjEvents[i].IdSectionOption = -1;
                    sectionOption = _this.GetSectionDefault();
                    sectionOption.Events.push(ListObjEvents[i]);
                }
                listEvent.push(obj);
                _this.SectionEvents.push(ListObjEvents[i]);
            }
            _this.Methods.ParseData.call(_this.PluginScheduler, listEvent, "json");
            success = true;
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype.SaveEvents", e);
    }
    return (success);
}
Componet.Scheduler.TScheduler.prototype._BtnEditClick = function () {
    var _this = this.TParent();
    try {
        if (typeof _this.BtnEditClick === "function") {
            var idEdit = $(_this.ListItem).find(".active").data("Id");
            if (idEdit === undefined) {
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "SELECTSECTIONEDIT"))
                return false; return false;
            }
            if (idEdit === -1) {
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "EDITNOTSECTION"))
                return false;
            }
            var section = new _this.SectionOptions();
            for (var i = 0; i < _this.SectionChild.length; i++) {
                if (_this.SectionChild[i].Id === idEdit) {
                    section = _this.SectionChild[i];
                }
            }
            var action = _this.BtnEditClick(_this, section, _this.BtnLinkEdit);
            if (action) {
                var listNew = new Array();
                var oldList = _this.Methods.ServerList.call(_this.PluginScheduler, "options");
                for (var x = 0; x < oldList.length; x++) {
                    if (oldList[x].key === idEdit) {
                        oldList[x].key = section.Id;
                        oldList[x].label = section.Name;
                    }
                    listNew.push(oldList[x])
                }
                _this.Methods.UpdateCollection.call(_this.PluginScheduler, "options", listNew);
                for (var y = 0; y < section.Events.length; y++) {
                    _this.EditEvents(section.Events[y], section.Events[y].Id)
                }
                $(_this.ListItem).find(".active").data('Id', section.Id);
                $(".list-group").find(".active").text(section.Name);
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype._BtnEditClick", e);
    }
}
Componet.Scheduler.TScheduler.prototype.EditSection = function (ObjSectionNew, id) {
    var _this = this.TParent();
    var success = false;
    try {
        if (!SysCfg.Str.Methods.IsNullOrEmpity(ObjSectionNew) && !SysCfg.Str.Methods.IsNullOrEmpity(id)) {
            var idEdit = id;
            if (idEdit === -1 || idEdit > 0) {
                var section = null;
                for (var i = 0; i < _this.SectionChild.length; i++) {
                    if (_this.SectionChild[i].Id === idEdit) {
                        _this.SectionChild[i].Id = ObjSectionNew.Id;
                        _this.SectionChild[i].IdSection = ObjSectionNew.IdSection;
                        _this.SectionChild[i].Name = ObjSectionNew.Name;
                        _this.SectionChild[i].Description = ObjSectionNew.Description;
                        _this.SectionChild[i].Tag = ObjSectionNew.Tag;
                        section = _this.SectionChild[i];
                    }
                }
                for (var z = 0; z < ObjSectionNew.Events.length; z++) {
                    _this.EditEvents(ObjSectionNew.Events[z], ObjSectionNew.Events[z].Id)
                }
                var listNew = new Array();
                var oldList = _this.Methods.ServerList.call(_this.PluginScheduler, "options");
                for (var x = 0; x < oldList.length; x++) {
                    if (oldList[x].key === idEdit) {
                        oldList[x].key = section.Id;
                        oldList[x].label = section.Name;
                    }
                    listNew.push(oldList[x])
                }
                _this.Methods.UpdateCollection.call(_this.PluginScheduler, "options", listNew);
                var ElementsHtml = $(_this.ListItem).find("a");
                for (var y = 0; y < ElementsHtml.length; y++) {
                    if ($(ElementsHtml).data("Id") === idEdit) {
                        $(ElementsHtml).data('Id', section.Id);
                        $(ElementsHtml).text(section.Name);
                    }
                }
                success = true;
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype.EditSection", e);
    }
    return (success);
}
Componet.Scheduler.TScheduler.prototype.EditEvents = function (NewEvent, id) {
    var _this = this.TParent();
    var success = false;
    try {
        if (!SysCfg.Str.Methods.IsNullOrEmpity(NewEvent) && !SysCfg.Str.Methods.IsNullOrEmpity(id)) {
            var idEdit = id;
            if (idEdit === -1 || idEdit > 0) {
                var EventEdit = _this.GetSectionEvent(idEdit);
                if (EventEdit) {
                    EventEdit.Id = NewEvent.Id;
                    EventEdit.IdSection = NewEvent.IdSection;
                    EventEdit.IdSectionOption = NewEvent.IdSectionOption;
                    EventEdit.Title = NewEvent.Title;
                    EventEdit.Description = NewEvent.Description;
                    EventEdit.ColorText = NewEvent.ColorText;
                    EventEdit.Background = NewEvent.Background;
                    EventEdit.TimeStart = NewEvent.TimeStart;
                    EventEdit.TimeEnd = NewEvent.TimeEnd;
                    EventEdit.Tag = NewEvent.Tag;
                    _this.MoveEvent(NewEvent.Id, NewEvent.IdSectionOption, true);
                    _this.Methods.ChangeEventId.call(_this.PluginScheduler, EventEdit.Id, NewEvent.Id)
                    var EventPlugin = _this.Methods.GetEventByID.call(_this.PluginScheduler, NewEvent.Id);
                    EventPlugin["id"] = NewEvent.Id;
                    EventPlugin["start_date"] = new Date(NewEvent.TimeStart);
                    EventPlugin["end_date"] = new Date(NewEvent.TimeEnd);
                    EventPlugin["title"] = NewEvent.Title;
                    EventPlugin["text"] = NewEvent.Description;
                    EventPlugin[_this.Section.MapProperty] = NewEvent.IdSectionOption;
                    EventPlugin["color"] = NewEvent.Background;
                    EventPlugin["textColor"] = NewEvent.ColorText;
                    _this.Methods.UpdateEvent.call(_this.PluginScheduler, NewEvent.Id);
                    success = true;
                }
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype.EditEvents", e);
    }
    return (success);
}
Componet.Scheduler.TScheduler.prototype._BtnDeleteClick = function () {
    var _this = this.TParent();
    var success = false;
    try {
        if (typeof _this.BtnDeleteClick === "function") {
            var idDelete = $(_this.ListItem).find(".active").data("Id");
            if (idDelete === undefined) {
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "SELECTSECTIONDELETE"))
                return false;
            }
            if (idDelete === -1) {
                alert(UsrCfg.Traslate.GetLangText(_this.Mythis, "DELETENOTSECTION"))
                return false;
            }
            var section = _this.GetSectionChild(idDelete);
            section = section ? section : new _this.SectionOptions();
            var action = _this.BtnDeleteClick(_this, section, _this.BtnLinkDelete);
            if (action) {
                //EVENTS
                for (var z = 0; z < section.Events.length; z++) {
                    section.Events[z].Status = 0;
                    _this._TargetEvent(true);
                    _this.Methods.DeleteEvent.call(_this.PluginScheduler, section.Events[z].Id);
                    _this._TargetEvent(false);
                }
                _this._NewListEvent();
                _this._NewListEventSectionChild();
                //
                //section
                section.Status = 0;
                _this._NewListSectionChild();
                _this._NewListOptionSection();
                //
                var listNew = new Array();
                var oldList = _this.Methods.ServerList.call(_this.PluginScheduler, "options");
                for (var x = 0; x < oldList.length; x++) {
                    if (oldList[x].key !== idDelete) {
                        listNew.push(oldList[x]);
                    }
                }
                _this.Methods.UpdateCollection.call(_this.PluginScheduler, "options", listNew);
                $(_this.ListItem).find(".active").remove();
                $(_this.ListItem).find(".default")[0].className = "list-group-item active default";
                success = true;
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype._BtnDeleteClick", e);
    }
    return (success);
}
Componet.Scheduler.TScheduler.prototype.DeleteSections = function (ListSectionId) {
    var _this = this.TParent();
    var success = false;
    try {
        if (Array.isArray(ListSectionId) && ListSectionId.length > 0) {
            for (var i = 0; i < ListSectionId.length; i++) {
                var section = _this.GetSectionChild(ListSectionId[i]);
                if (section) {
                    //EVENTS
                    for (var z = 0; z < section.Events.length; z++) {
                        section.Events[z].Status = 0;
                        _this._TargetEvent(true);
                        _this.Methods.DeleteEvent.call(_this.PluginScheduler, section.Events[z].Id);
                        _this._TargetEvent(false);
                    }
                    _this._NewListEvent();
                    _this._NewListEventSectionChild();
                    //
                    section.Status = 0;
                    _this._NewListSectionChild();
                    _this._NewListOptionSection();
                    $(section.Html).remove()
                    //
                    var listNew = new Array();
                    var oldList = _this.Methods.ServerList.call(_this.PluginScheduler, "options");
                    for (var x = 0; x < oldList.length; x++) {
                        if (oldList[x].key !== section.Id) {
                            listNew.push(oldList[x]);
                        }
                    }
                    _this.Methods.UpdateCollection.call(_this.PluginScheduler, "options", listNew);
                }
            }
            $(_this.ListItem).find(".default")[0].className = "list-group-item active default";
            success = true;
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype.DeleteSections", e);
    }
    return (success)
}
Componet.Scheduler.TScheduler.prototype.DeleteEvents = function (ListEventId) {
    var _this = this.TParent();
    var success = false;
    try {
        if (Array.isArray(ListEventId)) {
            var newListEvents = new Array();
            for (var i = 0; i < ListEventId.length; i++) {
                var event = _this.GetSectionEvent(ListEventId[i]);
                event.Status = 0;
                _this._TargetEvent(true);
                _this.Methods.DeleteEvent.call(_this.PluginScheduler, ListEventId[i])
                _this._TargetEvent(false);
            }
            _this._NewListEvent();
            _this._NewListEventSectionChild();
            success = true;
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype.DeleteEvents", e);
    }
    return (success);
}
Componet.Scheduler.TScheduler.prototype.MoveEvent = function (idEvent, newIdSectionOption, notEvenPlugin) {
    var _this = this.TParent();
    var success = false;
    try {
        var event = _this.GetSectionEvent(idEvent);
        if (event) {
            event.IdSectionOption = newIdSectionOption;
            event.Move = 1;
            var newListMove = new Array();
            for (var i = 0; i < _this.SectionChild.length; i++) {
                var newListEvent = new Array();
                for (var y = 0; y < _this.SectionChild[i].Events.length; y++) {
                    if (_this.SectionChild[i].Events[y].Move === 1) {
                        newListMove.push(_this.SectionChild[i].Events[y]);
                    }
                    else {
                        newListEvent.push(_this.SectionChild[i].Events[y])
                    }
                }
                _this.SectionChild[i].Events = newListEvent;
            }
            for (var z = 0; z < _this.SectionChild.length; z++) {
                for (var t = 0; t < newListMove.length; t++) {
                    if (newListMove[t].IdSectionOption === _this.SectionChild[z].Id) {
                        _this.SectionChild[z].Events.push(newListMove[t]);
                    }
                }
            }
            if (!notEvenPlugin) {
                var EventPlugin = _this.Methods.GetEventByID.call(_this.PluginScheduler, event.Id);
                EventPlugin[_this.Section.MapProperty] = newIdSectionOption;
                _this.Methods.UpdateEvent.call(_this.PluginScheduler, event.Id);
            }
            success = true;
        }

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype.MoveEvent", e);
    }
    return (success);

}
Componet.Scheduler.TScheduler.prototype._NewListEvent = function () {
    var _this = this.TParent();
    var success = false;
    try {
        var newListEvent = new Array();
        for (var i = 0; i < _this.SectionEvents.length; i++) {
            if (!(_this.SectionEvents[i].Status === 0)) {
                newListEvent.push(_this.SectionEvents[i]);
            }
        }
        _this.SectionEvents = newListEvent;
        success = true;

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype._NewListEvent", e);
    }
    return (success);
}
Componet.Scheduler.TScheduler.prototype._NewListEventSectionChild = function () {
    var _this = this.TParent();
    var success = false;
    try {
        for (var i = 0; i < _this.SectionChild.length; i++) {
            var newListEvent = new Array();
            for (var y = 0; y < _this.SectionChild[i].Events.length; y++) {
                if (!(_this.SectionChild[i].Events[y].Status === 0)) {
                    newListEvent.push(_this.SectionChild[i].Events[y]);
                }
            }
            _this.SectionChild[i].Events = newListEvent;
        }
        success = true;

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype._NewListEventSectionChild", e);
    }
    return (success);
}
Componet.Scheduler.TScheduler.prototype._NewListSectionChild = function () {
    var _this = this.TParent();
    var success = false;
    try {
        var newListEvent = new Array();
        for (var i = 0; i < _this.SectionChild.length; i++) {
            if (!(_this.SectionChild[i].Status === 0)) {
                newListEvent.push(_this.SectionChild[i]);
            }
        }
        _this.SectionChild = newListEvent;
        success = true;

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype._NewListSectionChild", e);
    }
    return (success);
}
Componet.Scheduler.TScheduler.prototype._NewListOptionSection = function () {
    var _this = this.TParent();
    var success = false;
    try {
        var newListEvent = new Array();
        for (var i = 0; i < _this.Section.ListOptions.length; i++) {
            if (!(_this.Section.ListOptions[i].Status === 0)) {
                newListEvent.push(_this.Section.ListOptions[i]);
            }
        }
        _this.Section.ListOptions = newListEvent;
        success = true;

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype._NewListOptionSection", e);
    }
}
Componet.Scheduler.TScheduler.prototype.GetSectionChild = function (id) {
    var _this = this.TParent();
    var success = false;
    try {
        for (var i = 0; i < _this.SectionChild.length; i++) {
            if (_this.SectionChild[i].Id === id) {
                return (_this.SectionChild[i]);
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype.GetSectionChild", e);
    }
    return (success);
}
Componet.Scheduler.TScheduler.prototype.GetSectionDefault = function () {
    var _this = this.TParent();
    var success = false;
    try {
        for (var i = 0; i < _this.SectionChild.length; i++) {
            if (_this.SectionChild[i].Id === -1) {
                return (_this.SectionChild[i]);
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype.GetSectionDefault", e);
    }
    return (success);
}
Componet.Scheduler.TScheduler.prototype.GetSectionEvent = function (id) {
    var _this = this.TParent();
    var success = false;
    try {
        for (var i = 0; i < _this.SectionEvents.length; i++) {
            if (_this.SectionEvents[i].Id === id) {
                return (_this.SectionEvents[i]);
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype.GetSectionEvent", e);
    }
    return (success);
}
Componet.Scheduler.TScheduler.prototype._EventAdd = function (callback, eventId, _event) {
    var _this = this.TParent();
    var successApp = false;
    var successAction = false;
    try {
        var event = new _this.Event();
        event.Id = 0;
        event.IdSection = _this.Section.Id;
        event.IdSectionOption = parseInt(_event.Section_Id);
        event.Title = SysCfg.Str.Methods.IsNullOrEmpity(_event.title) ? "" : _event.title;
        event.Description = _event.text;
        event.ColorText = SysCfg.Str.Methods.IsNullOrEmpity(_event.textColor) ? "#5b9be0" : _event.textColor;
        event.Background = SysCfg.Str.Methods.IsNullOrEmpity(_event.color) ? "#fff" : _event.color;
        event.TimeStart = _this.PluginScheduler.date.date_to_str("%Y/%m/%d %H:%i")(_event.start_date);
        event.TimeEnd = _this.PluginScheduler.date.date_to_str("%Y/%m/%d %H:%i")(_event.end_date);
        successAction = callback(_this, event);
        if (successAction) {
            var oldId = eventId
            _this.Methods.ChangeEventId.call(_this.PluginScheduler, oldId, event.Id);
            _event["id"] = event.Id;
            _event["start_date"] = new Date(event.TimeStart);
            _event["end_date"] = new Date(event.TimeEnd);
            _event["title"] = event.Title;
            _event["text"] = event.Description;
            _event[_this.Section.MapProperty] = event.IdSectionOption;
            _event["color"] = event.Background;
            _event["textColor"] = event.ColorText;
            _this.SectionEvents.push(event);
            var sectionOption = _this.GetSectionChild(event.IdSectionOption);
            if (sectionOption) {
                sectionOption.Events.push(event);
            }
            else {
                _event[_this.Section.MapProperty] = -1;
                _this.GetSectionDefault().Events.push(event);
            }
            _this.Methods.UpdateEvent.call(_this.PluginScheduler, event.Id);
            successApp = true;

        }
        else {
            _this._TargetEvent(true);
            _this.Methods.DeleteEvent.call(_this.PluginScheduler, eventId);
            _this._TargetEvent(false)
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype._EventAdd", e);
    }
    return (successApp)
}
Componet.Scheduler.TScheduler.prototype._EventDelete = function (callback, eventId, _event) {
    var _this = this.TParent();
    var successApp = false;
    var successAction = false;
    try {
        var event = _this.GetSectionEvent(parseInt(eventId));
        successAction = callback(_this, event);
        if (successAction) {
            event.Status = 0;
            _this._NewListEvent();
            _this._NewListEventSectionChild();
            successApp = true;
        }
        else {
            var newEvent = {};
            newEvent["id"] = event.Id;
            newEvent["start_date"] = event.TimeStart;
            newEvent["end_date"] = event.TimeEnd;
            newEvent["title"] = event.Title;
            newEvent["text"] = event.Description;
            newEvent[_this.Section.MapProperty] = event.IdSectionOption;
            newEvent["color"] = event.Background;
            _this.Methods.ParseData.call(_this.PluginScheduler, new Array(newEvent), "json");
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype._EventDelete", e);
    }
    return (successApp)
}
Componet.Scheduler.TScheduler.prototype._EventEdit = function (callback, eventId, _event) {
    var _this = this.TParent();
    var successApp = false;
    var successAction = false;
    try {
        var oldEventMethod = new _this.Event();
        var oldEvent = new _this.Event();
        var event = _this.GetSectionEvent(parseInt(eventId));
        //
        oldEventMethod.Id = event.Id;
        oldEventMethod.IdSection = event.IdSection;
        oldEventMethod.IdSectionOption = event.IdSectionOption;
        oldEventMethod.Title = event.Title;
        oldEventMethod.Description = event.Description;
        oldEventMethod.ColorText = event.ColorText;
        oldEventMethod.Background = event.Background;
        oldEventMethod.TimeStart = event.TimeStart;
        oldEventMethod.TimeEnd = event.TimeEnd;
        //
        oldEvent.Id = event.Id;
        oldEvent.IdSection = event.IdSection;
        oldEvent.IdSectionOption = event.IdSectionOption;
        oldEvent.Title = event.Title;
        oldEvent.Description = event.Description;
        oldEvent.ColorText = event.ColorText;
        oldEvent.Background = event.Background;
        oldEvent.TimeStart = event.TimeStart;
        oldEvent.TimeEnd = event.TimeEnd;
        oldEventMethod.TimeEnd = event.TimeEnd;
        //
        event.Id = _event.id;
        event.IdSection = _this.Section.Id;
        event.IdSectionOption = parseInt(_event.Section_Id);
        event.Title = SysCfg.Str.Methods.IsNullOrEmpity(_event.title) ? "" : _event.title;
        event.Description = _event.text;
        event.ColorText = SysCfg.Str.Methods.IsNullOrEmpity(_event.textColor) ? "#5b9be0" : _event.textColor;
        event.Background = SysCfg.Str.Methods.IsNullOrEmpity(_event.color) ? "#fff" : _event.color;
        event.TimeStart = _this.PluginScheduler.date.date_to_str("%Y/%m/%d %H:%i")(_event.start_date);
        event.TimeEnd = _this.PluginScheduler.date.date_to_str("%Y/%m/%d %H:%i")(_event.end_date);
        //
        successAction = callback(_this, oldEvent, event);
        if (successAction) {
            _this.MoveEvent(event.Id, event.IdSectionOption, true);
            _this.Methods.ChangeEventId.call(_this.PluginScheduler, oldEventMethod.Id, event.Id)
            var EventPluginNew = _this.Methods.GetEventByID.call(_this.PluginScheduler, event.Id);
            EventPluginNew["id"] = event.Id;
            EventPluginNew["start_date"] = new Date(event.TimeStart);
            EventPluginNew["end_date"] = new Date(event.TimeEnd);
            EventPluginNew["title"] = event.Title;
            EventPluginNew["text"] = event.Description;
            EventPluginNew[_this.Section.MapProperty] = event.IdSectionOption;
            EventPluginNew["color"] = event.Background;
            EventPluginNew["textColor"] = event.ColorText;
            _this.Methods.UpdateEvent.call(_this.PluginScheduler, event.Id);
            successApp = true;
        }
        else {
            var EventPluginOld = _this.Methods.GetEventByID.call(_this.PluginScheduler, oldEventMethod.Id);
            EventPluginOld["id"] = oldEventMethod.Id;
            EventPluginOld["start_date"] = new Date(oldEventMethod.TimeStart);
            EventPluginOld["end_date"] = new Date(oldEventMethod.TimeEnd);
            EventPluginOld["title"] = oldEventMethod.Title;
            EventPluginOld["text"] = oldEventMethod.Description;
            EventPluginOld[_this.Section.MapProperty] = oldEventMethod.IdSectionOption;
            EventPluginOld["color"] = oldEventMethod.Background;
            EventPluginOld["textColor"] = oldEventMethod.ColorText;
            _this.Methods.UpdateEvent.call(_this.PluginScheduler, oldEventMethod.Id);
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype._EventEdit", e);
    }
    return (successApp)
}
Componet.Scheduler.TScheduler.prototype._TargetEvent = function (target) {
    var _this = this.TParent();
    try {
        if (target) {
            _this.targetEventDelete = true;
        }
        else {
            _this.targetEventDelete = false;
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype._TargetEvent", e);
    }
}
Componet.Scheduler.TScheduler.prototype.BlockDay = function (objConfigMark) {
    var _this = this.TParent();
    try {
        var config = {
            start_date: SysCfg.Str.Methods.IsNullOrEmpity(objConfigMark.StarDate) ? new Date() : new Date(objConfigMark.StarDate),
            end_date: SysCfg.Str.Methods.IsNullOrEmpity(objConfigMark.EndDate) ? new Date() : new Date(objConfigMark.EndDate),
            css: SysCfg.Str.Methods.IsNullOrEmpity(objConfigMark.NameClass) ? "" : objConfigMark.NameClass,
            html: SysCfg.Str.Methods.IsNullOrEmpity(objConfigMark.Html) ? "" : objConfigMark.Html,
            type: "dhx_time_block"
        }
        objConfigMark.Id = _this.Methods.AddMarkedTimespan(config);
        objConfigMark.Type = "Block";
        _this.Methods.UpdateView.call(_this.PluginScheduler);
        _this.MarkBlockDay.push(objConfigMark);
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype.BloackDate", e);
    }
}
Componet.Scheduler.TScheduler.prototype.ShowDay = function (action) {
    var _this = this.TParent();
    try {
        (action) ? $(_this.BtnDay).show() : $(_this.BtnDay).hide();
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype.ShowDay", e);
    }
}
Componet.Scheduler.TScheduler.prototype.ShowWeek = function (action) {
    var _this = this.TParent();
    try {
        (action) ? $(_this.BtnWeek).show() : $(_this.BtnWeek).hide();
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype.ShowWeek", e);
    }
}
Componet.Scheduler.TScheduler.prototype.ShowMonth = function (action) {
    var _this = this.TParent();
    try {
        (action) ? $(_this.BtnMonth).show() : $(_this.BtnMonth).hide();
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype.ShowMonth", e);
    }
}
Componet.Scheduler.TScheduler.prototype.ShowYear = function (action) {
    var _this = this.TParent();
    try {
        (action) ? $(_this.BtnYear).show() : $(_this.BtnYear).hide();
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype.ShowYear", e);
    }
}
Componet.Scheduler.TScheduler.prototype.ShowSection = function (action) {
    var _this = this.TParent();
    try {
        (action) ? $(_this.BtnSection).show() : $(_this.BtnSection).hide();
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype.ShowSection", e);
    }
}
Componet.Scheduler.TScheduler.prototype._FixPlugin = function (action) {
    var _this = this.TParent();
    try {
        _this.PluginScheduler.$keyboardNavigation.dispatcher._old_getActiveNode = _this.PluginScheduler.$keyboardNavigation.dispatcher.getActiveNode;
        _this.PluginScheduler.$keyboardNavigation.dispatcher.getActiveNode = function () {
            var node = this._old_getActiveNode();
            if (!node) {
                this.setDefaultNode();
                return this.getDefaultNode();
            }
            return node;
        };
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("Scheduler.js Componet.Scheduler.TScheduler.prototype._FixPlugin", e);
    }
}
