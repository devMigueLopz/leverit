var url = window.location;

$(document).ready(function() {

});



/*
 * Funciones Generales *********************************************************
 */
function showModal(nameModel) {
    $('#'+nameModel).modal({
        show: true,
        backdrop: 'static',
        keyboard: false
    });
    $('#'+nameModel+' button.close').css('display', 'none');
}
function hideModal(nameModel) {
    reset();
    $('#'+nameModel).modal('hide');
}

function reset(){
    $('input[type=text]').val('');
    $('input[type=hidden]').val('0');
    $('#textarea').val(''); 
    $('input[type=select]').val('');
    $('input[type=radio]').val('');
    $('input[type=checkbox]').prop('checked', true); // Check: true  -  Uncheck: false
    $('select').prop('selectedIndex', 0);
}

/*******************************************************************************/