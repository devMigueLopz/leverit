Componet.ListView.TListView = function (inObject, id, incallback) {
    this.TParent = function () {
        return this;
    }.bind(this);
    this.ObjectHtml = inObject;
    this.CallbackModalResult = incallback;
    this.Id = id;
    this.PluginListView = null;
    this._Template = {
        "Template1": "<div class='col-xs-1 col-sm-2 nospaceTemplate1 firstTemplate1'><span class='dataIdTemplate1'>#Id#</span></div><div class='col-xs-11 col-sm-10 nospaceTemplate1 secondTemplate1'><span class='dataValueTemplate1'>#Value#</span><span class='dataDescriptionTemplate1'>#Description#</span></div>",
        "Template2": "<div class='col-xs-1 col-sm-2'><span class='dataIdTemplate2'>#Id#</span></div><div class='col-xs-10 col-sm-10'><span class='dataValueTemplate2'>#Value#</span></div>",
        "Template3": "<div class='col-xs-12'><span class='dataValueTemplate3'>#Value#</span></div>",
    }
    this.Template = this._Template.Template1;
    this.Data = new Array();
    Object.defineProperty(this, 'UnSelectAll', {

        get: function () {
            var _listView = this.PluginListView;
            return function () {
                _listView.unselectAll.call(_listView);
            }
        }
    });
    Object.defineProperty(this, 'SelectAll', {

        get: function () {
            var _listView = this.PluginListView;
            return function () {
                _listView.selectAll.call(_listView);
            }
        }
    });
    Object.defineProperty(this, 'UnSelect', {

        get: function () {
            var _listView = this.PluginListView;
            return function (id) {
                if (SysCfg.Str.Methods.IsNullOrEmpity(id))
                    _listView.unselect.call(_listView);
                else {
                    _listView.unselect.call(_listView, id);
                }
            }
        }
    });
    Object.defineProperty(this, 'Select', {

        get: function () {
            var _listView = this.PluginListView;
            return function (id) {
                if (SysCfg.Str.Methods.IsNullOrEmpity(id))
                    _listView.select.call(_listView);
                else {
                    _listView.select.call(_listView, id);
                }
            }
        }
    });
    Object.defineProperty(this, 'ShowElement', {

        get: function () {
            var _listView = this.PluginListView;
            return function (id) {
                if (!SysCfg.Str.Methods.IsNullOrEmpity(id))
                    _listView.show.call(_listView);
            }
        }
    });
    Object.defineProperty(this, 'Refresh', {

        get: function () {
            var _listView = this.PluginListView;
            return function () {
                _listView.refresh.call(_listView);
            }
        }
    });
    Object.defineProperty(this, 'IsSelect', {

        get: function () {
            var _listView = this.PluginListView;
            return function (id) {
                if (!SysCfg.Str.Methods.IsNullOrEmpity(id))
                    _listView.isSelected.call(_listView);
            }
        }
    });
    Object.defineProperty(this, 'OnDbClick', {
        get: function () {
            return this.PluginListView.attachEvent;
        },
        set: function (_Value) {
            var _listView = this;
            this.PluginListView.attachEvent.call(_listView.PluginListView,"onItemDblClick", function (id, ev, html) {
                if (!SysCfg.Str.Methods.IsNullOrEmpity(_Value) && !SysCfg.Str.Methods.IsNullOrEmpity(_listView._OnDbClick)) {
                    var objDataEvent = { Id: parseInt(id), Event: ev, ElementHtml: html };
                    var element = _listView._OnDbClick(objDataEvent);
                    _Value(element);
                    return true;
                }
            });
        }
    });
    Object.defineProperty(this, 'OnClick', {
        get: function () {
            return this.PluginListView.attachEvent;
        },
        set: function (_Value) {
            var _listView = this;
            this.PluginListView.attachEvent.call(_listView.PluginListView,"onItemClick", function (id, ev, html) {
                if (!SysCfg.Str.Methods.IsNullOrEmpity(_Value) && !SysCfg.Str.Methods.IsNullOrEmpity(_listView._OnClick)) {
                    var objDataEvent = { Id: parseInt(id), Event: ev, ElementHtml: html };
                    var element = _listView._OnClick(objDataEvent);
                    _Value(element);
                    return true;
                }
            });
        }
    });
    Object.defineProperty(this, 'OnSelectChange', {
        get: function () {
            return this.PluginListView.attachEvent;
        },
        set: function (_Value) {
            var _listView = this;
            this.PluginListView.attachEvent.call(_listView.PluginListView,"onSelectChange", function (elements) {
                if (!SysCfg.Str.Methods.IsNullOrEmpity(_Value) && !SysCfg.Str.Methods.IsNullOrEmpity(_listView._OnSelectChange)) {
                    var objDataEvent = { Elements: elements };
                    var elements = _listView._OnSelectChange(objDataEvent);
                    _Value(elements);
                    return true;
                }
            });
        }
    });
}
Componet.ListView.TListView.prototype.InitializeDesigner = function () {
    var _this = this.TParent();
    try {
        _this.ContainerInit = new TVclStackPanel(_this.ObjectHtml, "Container" + "_" + _this.Id, 1, [[12], [12], [12], [12], [12]]);
        _this.ContainerPlugin = new Udiv(_this.ContainerInit.Column[0].This, "ContainerPlugin" + "_" + _this.Id);
        _this.ContainerPlugin.style.border = "1px solid #c0c0c0";
        _this.ContainerPlugin.style.backgroundColor = "white";
        _this.ContainerPlugin.style.height = "400px";
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ListView.js Componet.ListView.TListView.prototype.InitializeDesigner", e);
    }
}
Componet.ListView.TListView.prototype.Load = function (objData) {
    var _this = this.TParent();
    try {
        _this.InitializeDesigner();
        _this.LoadData(objData);
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ListView.js Componet.ListView.TListView.prototype.Load", e);
    }
}
Componet.ListView.TListView.prototype.LoadData = function (listObjectData) {
    var _this = this.TParent();
    try {
        _this.PluginListView = new dhtmlXList({
            container: _this.ContainerPlugin.id,
            type: {
                template: _this.Template,
                height: "auto"
            }
        });
        _this.PluginListView.parse(listObjectData, "json");
        _this._ChangeId(listObjectData);
        for (var i = 0; i < listObjectData.length; i++) {
            var element = new _this.Member;
            element.Id = listObjectData[i].Id;
            element.Value = listObjectData[i].Value;
            element.Description = listObjectData[i].Description;
            element.Tag = listObjectData[i].Tag;
            _this.Data.push(element);
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ListView.js Componet.ListView.TListView.prototype.LoadData", e);
    }
}
Componet.ListView.TListView.prototype.AddElement = function (objData) {
    var _this = this.TParent();
    var success = false;
    try {
        if (!SysCfg.Str.Methods.IsNullOrEmpity(objData.Id) && objData.Id > 0 && !SysCfg.Str.Methods.IsNullOrEmpity(objData.Value) && !isNaN(objData.Id)) {
            _this.PluginListView.add(objData);
            _this._ChangeId([objData]);
            var element = new _this.Member;
            element.Id = objData.Id;
            element.Value = objData.Value;
            element.Description = objData.Description;
            element.Tag = objData.Tag;
            _this.Data.push(element);
            success = true;
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ListView.js Componet.ListView.TListView.prototype.AddElement", e);
    }
    return (success)
}
Componet.ListView.TListView.prototype.RemoveElement = function (id) {
    var _this = this.TParent();
    var element = null;
    try {
        if (!SysCfg.Str.Methods.IsNullOrEmpity(id) && id > 0 && !isNaN(id)) {
            var _element = _this._SearchElement(id);
            if (!SysCfg.Str.Methods.IsNullOrEmpity(_element)) {
                _this._NewData(id);
                _this.PluginListView.remove(id);
                element = new _this.Member;
                element.Id = _element.Id;
                element.Value = _element.Value;
                element.Description = _element.Description;
                element.Tag = _element.Tag;
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ListView.js Componet.ListView.TListView.prototype.RemoveElement", e);
    }
    return (element)
}
Componet.ListView.TListView.prototype.RemoveAllElement = function () {
    var _this = this.TParent();
    var elements = new Array();
    try {
        if (_this.Data.length > 0) {
            elements = _this.Data;
            _this.Data = new Array();
            _this.PluginListView.clearAll();
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ListView.js Componet.ListView.TListView.prototype.RemoveAllElement", e);
    }
    return (elements)
}
Componet.ListView.TListView.prototype.EditElement = function (objData) {
    var _this = this.TParent();
    var element = null;
    try {
        if (!SysCfg.Str.Methods.IsNullOrEmpity(objData.Id) && objData.Id > 0 && !SysCfg.Str.Methods.IsNullOrEmpity(objData.Value) && !isNaN(objData.Id)) {
            var _element = _this._SearchElement(objData.Id);
            if (!SysCfg.Str.Methods.IsNullOrEmpity(_element)) {
                //old element
                element = new _this.Member;
                element.Id = _element.Id;
                element.Value = _element.Value;
                element.Description = _element.Description;
                element.Tag = _element.Tag;
                //new element
                _element.Id = objData.Id;
                _element.Value = objData.Value;
                _element.Description = objData.Description;
                _element.Tag = objData.Tag;
                //new element plugin
                var elementPlugin = _this.PluginListView.get(_element.Id);
                elementPlugin.Id = objData.Id;
                elementPlugin.Value = objData.Value;
                elementPlugin.Description = objData.Description;
                elementPlugin.Tag = objData.Tag;
                _this.PluginListView.update(_element.Id, elementPlugin);
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ListView.js Componet.ListView.TListView.prototype.EditElement", e);
    }
    return (element)
}
Componet.ListView.TListView.prototype.GetElement = function (id) {
    var _this = this.TParent();
    var element = null;
    try {
        if (!isNaN(id)) {
            _element = _this._SearchElement(id);
            var elementPlugin = _this.PluginListView.get(id)
            if (!SysCfg.Str.Methods.IsNullOrEmpity(_element) && !SysCfg.Str.Methods.IsNullOrEmpity(elementPlugin)) {
                element = new _this.Member;
                element.Id = _element.Id;
                element.Value = _element.Value;
                element.Description = _element.Description;
                element.Tag = _element.Tag;
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ListView.js Componet.ListView.TListView.prototype.GetElement", e);
    }
    return (element)
}
Componet.ListView.TListView.prototype.GetSelectElement = function () {
    var _this = this.TParent();
    var element = null;
    try {
        var idSearch = null;
        var _element = null;
        idSearch = _this.PluginListView.getSelected();
        if (!isNaN(idSearch)) {
            _element = _this._SearchElement(idSearch);
            if (!SysCfg.Str.Methods.IsNullOrEmpity(idSearch) && !SysCfg.Str.Methods.IsNullOrEmpity(_element)) {
                element = new _this.Member;
                element.Id = _element.Id;
                element.Value = _element.Value;
                element.Description = _element.Description;
                element.Tag = _element.Tag;
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ListView.js Componet.ListView.TListView.prototype.GetSelectElement", e);
    }
    return (element)
}
Componet.ListView.TListView.prototype._SearchElement = function (id) {
    var _this = this.TParent();
    var element = null;
    try {
        for (var i = 0; i < _this.Data.length; i++) {
            if (_this.Data[i].Id == id)
                element = _this.Data[i];
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ListView.js Componet.ListView.TListView.prototype._SearchElement", e);
    }
    return (element);
}
Componet.ListView.TListView.prototype._NewData = function (id) {
    var _this = this.TParent();
    var newData = new Array;
    try {
        for (var i = 0; i < _this.Data.length; i++) {
            if (_this.Data[i].Id != id)
                newData.push(_this.Data[i]);
        }
        _this.Data = null;
        _this.Data = newData;
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ListView.js Componet.ListView.TListView.prototype._NewData", e);
    }
}
Componet.ListView.TListView.prototype._ChangeId = function (listObjectData) {
    var _this = this.TParent();
    try {
        for (var i = 0; i < listObjectData.length; i++) {
            _this.PluginListView.changeId(listObjectData[i].id, listObjectData[i].Id);
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ListView.js Componet.ListView.TListView.prototype._ChangeId", e);
    }
}
Componet.ListView.TListView.prototype._OnClick = function (objectEvent) {
    var _this = this.TParent();
    var element = null;
    try {
        if (!SysCfg.Str.Methods.IsNullOrEmpity(objectEvent) && !SysCfg.Str.Methods.IsNullOrEmpity(objectEvent.Id) && objectEvent.Id > 0) {
            _element = _this._SearchElement(objectEvent.Id);
            if (!SysCfg.Str.Methods.IsNullOrEmpity(_element)) {
                element = new _this.Member;
                element.Id = _element.Id;
                element.Value = _element.Value;
                element.Description = _element.Description;
                element.Tag = _element.Tag;
            }
        }

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ListView.js Componet.ListView.TListView.prototype._OnClick", e);
    }
    return (element)
}
Componet.ListView.TListView.prototype._OnDbClick = function (objectEvent) {
    var _this = this.TParent();
    var element = null;
    try {
        if (!SysCfg.Str.Methods.IsNullOrEmpity(objectEvent) && !SysCfg.Str.Methods.IsNullOrEmpity(objectEvent.Id) && objectEvent.Id > 0) {
            _element = _this._SearchElement(objectEvent.Id);
            if (!SysCfg.Str.Methods.IsNullOrEmpity(_element)) {
                element = new _this.Member;
                element.Id = _element.Id;
                element.Value = _element.Value;
                element.Description = _element.Description;
                element.Tag = _element.Tag;
            }
        }

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ListView.js Componet.ListView.TListView.prototype._OnDbClick", e);
    }
    return (element)
}
Componet.ListView.TListView.prototype._OnSelectChange = function (objectEvent) {
    var _this = this.TParent();
    var listElements = new Array;
    try {
        if (!SysCfg.Str.Methods.IsNullOrEmpity(objectEvent) && !SysCfg.Str.Methods.IsNullOrEmpity(objectEvent.Elements) && objectEvent.Elements.length > 0) {
            for (var i = 0; i < objectEvent.Elements.length; i++) {
                var _element = _this._SearchElement(parseInt(objectEvent.Elements[i]));
                if (!SysCfg.Str.Methods.IsNullOrEmpity(_element)) {
                    var element = new _this.Member;
                    element.Id = _element.Id;
                    element.Value = _element.Value;
                    element.Description = _element.Description;
                    element.Tag = _element.Tag;
                    listElements.push(element);
                }
            }
        }
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("ListView.js Componet.ListView.TListView.prototype._OnSelectChange", e);
    }
    return (listElements)
}
Componet.ListView.TListView.prototype.Member = function (objData) {
    this.Id = 0;
    this.Value = "";
    this.Description = "";
    this.Tag = "";
}
