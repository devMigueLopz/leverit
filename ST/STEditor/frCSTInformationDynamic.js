﻿var TCMDBCIDEFINEEXTRATABLE_SOURCETYPE = {
    IDSDCASEMT: { value: 1, name: "IDSDCASEMT" },
    IDSDCASE: { value: 2, name: "IDSDCASE" },
    IDSDCASEEF: { value: 3, name: "IDSDCASEEF" }
}



function CMDBCIDEFINEEXTRATABLE_SOURCETYPE(valor) {
    //debugger;
    for (var item in TCMDBCIDEFINEEXTRATABLE_SOURCETYPE) {
        if (TCMDBCIDEFINEEXTRATABLE_SOURCETYPE[item].value == valor)
            return TCMDBCIDEFINEEXTRATABLE_SOURCETYPE[item];
    }
    //for (var i = 0; i < TCMDBCIDEFINEEXTRATABLE_SOURCETYPElength; i++) {
    //    if (TCMDBCIDEFINEEXTRATABLE_SOURCETYPE[0].value == valor)
    //        return TCMDBCIDEFINEEXTRATABLE_SOURCETYPE[i];
    //}
    //return TCMDBCIDEFINEEXTRATABLE_SOURCETYPE[0];
}

function CargarTabControlPendingcases(Objeto, response, IDSOURCE) {
    //var IdxIDSDWHOTOCASE = '_' + inIDSDWHOTOCASE;
    response = response.d;
    btnGrabarETArray = new Array();
       
    for (var i = 0; i < response.MDLIFESTATUSProfiler.MDCASESTEXTRATABLEList.length; i++) {
        if (response.MDLIFESTATUSProfiler.MDCASESTEXTRATABLEList[i].MDSERVICEEXTRATABLE.EXTRATABLE_IDSOURCE == 3) {
            var ExtraTableArr = new Array(1);
            ExtraTableArr[0] = new Array(1);
            var ExtraTable_udiv = VclDivitions(Objeto, "ExtraTable", ExtraTableArr);
            BootStrapCreateRow(ExtraTable_udiv[0].This);
            ExtraTable_udiv[0].This.style.backgroundColor = "rgb(133,194,38)";
            BootStrapCreateColumn(ExtraTable_udiv[0].Child[0].This, [12, 12, 12, 12, 12]);
            var Titulo_uh4 = Vcllabel(ExtraTable_udiv[0].Child[0].This, "", TVCLType.BS, TlabelType.H4, response.MDLIFESTATUSProfiler.MDCASESTEXTRATABLEList[i].MDSERVICEEXTRATABLE.EXTRATABLE_NAMETABLE);
            Titulo_uh4.style.fontSize = "15px";
            Titulo_uh4.style.fontWeight = "700";
            var ContExtraTable_udiv = VclDivitions(Objeto, "", new Array(1));
            BootStrapCreateRow(ContExtraTable_udiv[0].This);
            var ColExtraTable_udiv = VclDivitions(ContExtraTable_udiv[0].This, "", new Array(1));
            BootStrapCreateColumn(ColExtraTable_udiv[0].This, [12, 12, 12, 12, 12]);
            for (var e = 0; e < response.MDSERVICEEXTRATABLEList.length; e++) {
                if (response.MDLIFESTATUSProfiler.MDCASESTEXTRATABLEList[i].MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE == response.MDSERVICEEXTRATABLEList[e].IDMDSERVICEEXTRATABLE) {            
                    Vcllabel(ColExtraTable_udiv[0].This, "", TVCLType.BS, TlabelType.H5, response.MDLIFESTATUSProfiler.MDCASESTEXTRATABLEList[i].LSCHANGETABLE_COMMENTS);
                    var IdxIDSDWHOTOCASE = "_" + response.MDLIFESTATUSProfiler.MDCASESTEXTRATABLEList[i].IDMDCASESTEXTRATABLE;
                    Uinput(ColExtraTable_udiv[0].This, TImputtype.hidden, "IDMDLIFESTATUSBEHAVIOR_" + response.MDLIFESTATUSProfiler.MDCASESTEXTRATABLEList[i].MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE + IdxIDSDWHOTOCASE, "1", false);
                    Uinput(ColExtraTable_udiv[0].This, TImputtype.hidden, "GRIDENABLE_" + response.MDLIFESTATUSProfiler.MDCASESTEXTRATABLEList[i].MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE + IdxIDSDWHOTOCASE, response.MDLIFESTATUSProfiler.MDCASESTEXTRATABLEList[i].MDSERVICEEXTRATABLE.GRIDENABLE, false);
                    Uinput(ColExtraTable_udiv[0].This, TImputtype.hidden, "READONLY_" + response.MDLIFESTATUSProfiler.MDCASESTEXTRATABLEList[i].MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE + IdxIDSDWHOTOCASE, response.MDLIFESTATUSProfiler.MDCASESTEXTRATABLEList[i].MDSERVICEEXTRATABLE.READONLY, false);

                    var btnGrabarET = UGetExtraFields(ColExtraTable_udiv[0].This, response.MDLIFESTATUSProfiler.MDCASESTEXTRATABLEList[i].IDMDCASESTEXTRATABLE, response.MDSERVICEEXTRATABLEList[e]);
                    btnGrabarETArray.push(btnGrabarET);
                    break;
                }
            }
        }
    }
    return btnGrabarETArray;
}

function UGetExtraFields(objContenedor, inIDSDWHOTOCASE, MDSERVICEEXTRATABLE) {
    var IdxIDSDWHOTOCASE = '_' + inIDSDWHOTOCASE;
    var FieldsIDExtraTables = new Array(2);
    FieldsIDExtraTables[0] = new Array(2);
    FieldsIDExtraTables[1] = new Array(2);

    FieldsIDExtraTables_udiv = VclDivitions(objContenedor, "ContExtraTableCtrls" + inIDSDWHOTOCASE, FieldsIDExtraTables);

    BootStrapCreateRow(FieldsIDExtraTables_udiv[0].This);
    BootStrapCreateColumns(FieldsIDExtraTables_udiv[0].Child, [[3, 9], [3, 9], [3, 9], [3, 9], [3, 9]]);
    Vcllabel(FieldsIDExtraTables_udiv[0].Child[0].This, "", TVCLType.BS, TlabelType.H0, "IDMDST_EF" + MDSERVICEEXTRATABLE.EXTRATABLE_NAMETABLE);
    var FieldExtraTable_input = Uinput(FieldsIDExtraTables_udiv[0].Child[1].This, TImputtype.text, "FieldExtraTable" + MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE + IdxIDSDWHOTOCASE, "0", false);
    FieldExtraTable_input.setAttribute("data-NameField", "IDMDST_EF" + MDSERVICEEXTRATABLE.EXTRATABLE_NAMETABLE);
    FieldExtraTable_input.setAttribute("data-DataType", TDataType.Int32);
    $(FieldExtraTable_input).attr("disabled", true);
    FieldsIDExtraTables_udiv[0].This.style.display = "none";

    BootStrapCreateRow(FieldsIDExtraTables_udiv[1].This);
    BootStrapCreateColumns(FieldsIDExtraTables_udiv[1].Child, [[3, 9], [3, 9], [3, 9], [3, 9], [3, 9]]);
    //debugger;
    Vcllabel(FieldsIDExtraTables_udiv[1].Child[0].This, "", TVCLType.BS, TlabelType.H0, CMDBCIDEFINEEXTRATABLE_SOURCETYPE(MDSERVICEEXTRATABLE.EXTRATABLE_IDSOURCE).name/*"IDSDCASEMT"*/);
    var FieldIDSDCASEMT_input = Uinput(FieldsIDExtraTables_udiv[1].Child[1].This, TImputtype.text, "FieldIDSDCASEMT" + MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE + IdxIDSDWHOTOCASE, "0", false);
    FieldIDSDCASEMT_input.setAttribute("data-NameField", CMDBCIDEFINEEXTRATABLE_SOURCETYPE(MDSERVICEEXTRATABLE.EXTRATABLE_IDSOURCE).name/*"IDSDCASEMT"*/);
    FieldIDSDCASEMT_input.setAttribute("data-DataType", TDataType.Int32);
    $(FieldIDSDCASEMT_input).attr("disabled", true);
    FieldsIDExtraTables_udiv[1].This.style.display = "none";
    //debugger;
    var ArrayControls = new Array(MDSERVICEEXTRATABLE.MDSERVICEEXTRAFIELDSList.length);

    for (var i = 0; i < MDSERVICEEXTRATABLE.MDSERVICEEXTRAFIELDSList.length; i++) {
        var FieldsIDExtraTablesDina = new Array(1);
        FieldsIDExtraTablesDina[0] = new Array(2);
        FieldsIDExtraTablesDina_udiv = VclDivitions(objContenedor, "ContExtraTableCtrls" + inIDSDWHOTOCASE + "_" + (i + 2), FieldsIDExtraTablesDina);
        BootStrapCreateRow(FieldsIDExtraTablesDina_udiv[0].This);
        BootStrapCreateColumns(FieldsIDExtraTablesDina_udiv[0].Child, [[3, 9], [3, 9], [3, 9], [3, 9], [3, 9]]);
        FieldsIDExtraTablesDina_udiv[0].This.style.display = UGetVisibility(MDSERVICEEXTRATABLE.MDSERVICEEXTRAFIELDSList[i].IDMDLIFESTATUSPERMISSION);
        Vcllabel(FieldsIDExtraTablesDina_udiv[0].Child[0].This, "", TVCLType.BS, TlabelType.H0, MDSERVICEEXTRATABLE.MDSERVICEEXTRAFIELDSList[i].EXTRAFIELDS_NAME);
        //debugger;
        var ctrl = UConstruirControl(FieldsIDExtraTablesDina_udiv[0].Child[1].This, inIDSDWHOTOCASE, MDSERVICEEXTRATABLE.MDSERVICEEXTRAFIELDSList[i], MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE)
        ArrayControls[i] = ctrl;
    }

    var ContBotonesET = new Array(1);
    ContBotonesET[0] = new Array(1);
    var ContBotonesET_udiv = VclDivitions(objContenedor, "ContBotonesET_" + MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE + IdxIDSDWHOTOCASE, ContBotonesET);
    BootStrapCreateRow(ContBotonesET_udiv[0].This);
    BootStrapCreateColumn(ContBotonesET_udiv[0].Child[0].This, [12, 12, 12, 12, 12]);


    var btnNuevoET = new TVclInputbutton(ContBotonesET_udiv[0].Child[0].This, "");
    btnNuevoET.Text = get_LangTextDefault("btnNuevoET");
    btnNuevoET.VCLType = TVCLType.BS;
    btnNuevoET.This.classList.add("btnNuevoET");
    //btnNuevoET.classList.add("btnstep");
    btnNuevoET.This.setAttribute("data-IdET", MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE);
    btnNuevoET.This.setAttribute("data-IdWHOTT", inIDSDWHOTOCASE);
    btnNuevoET.onClick = function () {
        var IdET = parseInt($(this).attr('data-IdET'));
        var ArrayComtrolsTemp = ArrayControls;
        ArrayComtrolsTemp.push(FieldIDSDCASEMT_input);
        ArrayComtrolsTemp.push(FieldExtraTable_input);
        ULimpiarControles(ArrayComtrolsTemp);
        UHabilitaControles(true, ArrayControls)
        $('#stateForm' + MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE + IdxIDSDWHOTOCASE).val(StateFormulario._Nuevo);
        UHabilitaBotones(btnNuevoET, btnGrabarET, btnEliminarET, btnCancelarET, false, true, false, true, MDSERVICEEXTRATABLE.GRIDENABLE, MDSERVICEEXTRATABLE.READONLY, false);
    }

    //debugger;
    var btnGrabarET = new VclInputbutton(ContBotonesET_udiv[0].Child[0].This, "btnGrabarET_" + IdxIDSDWHOTOCASE);
    btnGrabarET.Text = get_LangTextDefault("btnGrabarET");
    btnGrabarET.VCLType = TVCLType.BS;
    btnGrabarET.This.classList.add("btnGrabarET");
    //btnGrabarET.classList.add("btnstep");
    btnGrabarET.This.setAttribute("data-IdET", MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE);
    btnGrabarET.This.setAttribute("data-IdWHOTT", inIDSDWHOTOCASE);
    btnGrabarET.onClick = function () {

        //debugger;
        //***************** Validacion de Mandatoy *************
        btnGrabarET.Tag = new OutMandatory();
        btnGrabarET.Tag.isMANDATORY = false;
        btnGrabarET.Tag.StrOut = "";
        var IdET = parseInt($(this).attr('data-IdET'));
        var IdWHOTT = parseInt($(this).attr('data-IdWHOTT'));
        var ArrayComtrolsTemp = ArrayControls;
        for (var i = 0; i < ArrayComtrolsTemp.length; i++) {
                               
            data_NameField = $(ArrayComtrolsTemp[i]).attr('data-NameField');
         
            data_Description = $(ArrayComtrolsTemp[i]).attr('data-Description');
            //data_Description = data_NameField;//agragar el header para no confundir el usuario //UnMDSERVICEEXTRAFIELDS.EXTRAFIELDS_DESCRIPTION;
            //debugger;
            data_Mandatory = $(ArrayComtrolsTemp[i]).attr('data-Mandatory') == 'true';
        
            data_Val = $(ArrayComtrolsTemp[i]).val();
            data_DataType = parseInt($(ArrayComtrolsTemp[i]).attr('data-DataType'));
            var idt = "\n";
            if (btnGrabarET.Tag.isMANDATORY) idt = ", ";
        
            switch (data_DataType) {
                case TDataType.String:

                    if (data_Mandatory && $(ArrayComtrolsTemp[i]).val() == "") {
                        btnGrabarET.Tag.StrOut += idt + data_Description;
                        btnGrabarET.Tag.isMANDATORY = true;
                    }
                    break;
                case TDataType.Int32:
                    if (data_Mandatory && $(ArrayComtrolsTemp[i]).val() == "") {
                        btnGrabarET.Tag.StrOut += idt + data_Description;
                        btnGrabarET.Tag.isMANDATORY = true;
                    }
                    break;
                case TDataType.Boolean:
                    break;
                case TDataType.Text:
                    if (data_Mandatory && $(ArrayComtrolsTemp[i]).val() == "") {
                        btnGrabarET.Tag.StrOut += idt + data_Description;
                        btnGrabarET.Tag.isMANDATORY = true;
                    }
                    break;
                case TDataType.Double:
                    if (data_Mandatory && $(ArrayComtrolsTemp[i]).val() == "") {
                        btnGrabarET.Tag.StrOut += idt + data_Description;
                        btnGrabarET.Tag.isMANDATORY = true;
                    }
                    break;
                case TDataType.DateTime:
                    if (data_Mandatory && $(ArrayComtrolsTemp[i]).val() == "") {
                        btnGrabarET.Tag.StrOut += idt + data_Description;
                        btnGrabarET.Tag.isMANDATORY = true;
                    }
                    break;
                case TDataType.Decimal:
                    if (data_Mandatory && $(ArrayComtrolsTemp[i]).val() == "") {
                        btnGrabarET.Tag.StrOut += idt + data_Description;
                        btnGrabarET.Tag.isMANDATORY = true;
                    }
                    break;
                default:

            }
    
        }
        if (btnGrabarET.Tag.isMANDATORY) {
            return;
        }

        //***************** End Validacion de Mandatoy *************



        var IdET = parseInt($(this).attr('data-IdET'));
        var IdWHOTT = parseInt($(this).attr('data-IdWHOTT'));

        var ArrayComtrolsTemp = ArrayControls;
        ArrayComtrolsTemp.push(FieldIDSDCASEMT_input);
        ArrayComtrolsTemp.push(FieldExtraTable_input);

        var parametros = [];
        for (var i = 0; i < ArrayComtrolsTemp.length; i++) {
            var Atributos = [];
            Atributos[0] = $(ArrayComtrolsTemp[i]).attr('data-NameField');
            Atributos[1] = $(ArrayComtrolsTemp[i]).val();
            Atributos[2] = $(ArrayComtrolsTemp[i]).attr('data-DataType');
            parametros[i] = Atributos;
        }
        var _stateForm = $('#stateForm' + MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE + IdxIDSDWHOTOCASE).val();


        _data = JSON.stringify({
            IDSDWHOTOCASE: inIDSDWHOTOCASE,
            IDMDSERVICEEXTRATABLE: MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE,
            arrayString: parametros,
            stateForm: _stateForm
        });

        $.ajax({
            type: "POST",
            url: xRaiz + "ST/STEditor/frCASESTEditor.svc/GrabarExtraTable",
            contentType: "application/json; charset=utf-8",
            data: _data,
            success: function (response) {

                var NroADN = ContarADN(MDSERVICEEXTRATABLE.MDSERVICEEXTRAFIELDSList);

                if (_stateForm == StateFormulario._update) {
                    if (NroADN > 0) {
                        ULimpiarControles(ArrayComtrolsTemp);
                        UHabilitaControles(false, ArrayControls)
                        $('#stateForm' + MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE + IdxIDSDWHOTOCASE).val(StateFormulario._Nuevo);
                        UHabilitaBotones(btnNuevoET, btnGrabarET, btnEliminarET, btnCancelarET, true, false, false, false, MDSERVICEEXTRATABLE.GRIDENABLE, MDSERVICEEXTRATABLE.READONLY, false);
                    }
                    else {
                        $('#stateForm' + MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE + IdxIDSDWHOTOCASE).val(StateFormulario._update);
                        UHabilitaBotones(btnNuevoET, btnGrabarET, btnEliminarET, btnCancelarET, true, true, true, false, MDSERVICEEXTRATABLE.GRIDENABLE, MDSERVICEEXTRATABLE.READONLY, false);
                    }
                } else {
                    if (_stateForm == StateFormulario._update) {
                        if (NroADN > 0) {
                            ULimpiarControles(ArrayComtrolsTemp);
                            UHabilitaControles(false, ArrayControls)
                            UHabilitaBotones(btnNuevoET, btnGrabarET, btnEliminarET, btnCancelarET, true, false, false, false, MDSERVICEEXTRATABLE.GRIDENABLE, MDSERVICEEXTRATABLE.READONLY, false);
                        }
                        else {
                            UHabilitaBotones(btnNuevoET, btnGrabarET, btnEliminarET, btnCancelarET, true, true, true, false, MDSERVICEEXTRATABLE.GRIDENABLE, MDSERVICEEXTRATABLE.READONLY, false);
                        }
                    }
                }
                ROWEXTRATABLE_GET_update(objContenedor, inIDSDWHOTOCASE, MDSERVICEEXTRATABLE, MDSERVICEEXTRATABLE.MDSERVICEEXTRAFIELDSList);
                var NroADN = ContarADN(MDSERVICEEXTRATABLE.MDSERVICEEXTRAFIELDSList);

                if (!(NroADN > 0)) {
                    btnNuevoET.Visible = false;
                    btnCancelarET.Visible = false;
                }
            },
            error: function (response) {
                alert(response);
            }
        });
    }

    var btnEliminarET = new TVclInputbutton(ContBotonesET_udiv[0].Child[0].This, "");
    btnEliminarET.Text = get_LangTextDefault("btnEliminarET");
    btnEliminarET.VCLType = TVCLType.BS;

    btnEliminarET.This.classList.add("btnEliminarET");
    //btnEliminarET.classList.add("btnstep");
    btnEliminarET.This.setAttribute("data-IdET", MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE);
    btnEliminarET.This.setAttribute("data-IdWHOTT", inIDSDWHOTOCASE);
    btnEliminarET.onClick = function () {
        var IdET = parseInt($(this).attr('data-IdET'));

        var ArrayComtrolsTemp = ArrayControls;
        ArrayComtrolsTemp.push(FieldIDSDCASEMT_input);
        ArrayComtrolsTemp.push(FieldExtraTable_input);

        var parametros = [];
        for (var i = 0; i < ArrayComtrolsTemp.length; i++) {
            var Atributos = [];
            Atributos[0] = $(ArrayComtrolsTemp[i]).attr('data-NameField');
            Atributos[1] = $(ArrayComtrolsTemp[i]).val();
            Atributos[2] = $(ArrayComtrolsTemp[i]).attr('data-DataType');
            parametros[i] = Atributos;
        }

        _data = JSON.stringify({
            IDSDWHOTOCASE: inIDSDWHOTOCASE, // parseInt($('#IDSDWHOTOCASE').val()),
            IDMDSERVICEEXTRATABLE: MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE,
            arrayString: parametros,
            stateForm: StateFormulario._Delete
        });
        //_data = JSON.stringify(NuevoObjeto);
        $.ajax({
            type: "POST",
            url: xRaiz + "ST/STEditor/frCASESTEditor.svc/GrabarExtraTable",
            contentType: "application/json; charset=utf-8",
            data: _data,
            success: function (response) {
                ROWEXTRATABLE_GET_update(objContenedor, inIDSDWHOTOCASE, MDSERVICEEXTRATABLE, MDSERVICEEXTRATABLE.MDSERVICEEXTRAFIELDSList);
                var NroADN = ContarADN(MDSERVICEEXTRATABLE.MDSERVICEEXTRAFIELDSList);

                if (!(NroADN > 0)) {
                    btnNuevoET.Visible = false;
                    btnCancelarET.Visible = false;
                }
                ULimpiarControles(ArrayComtrolsTemp);
                alert("Se eliminio correctamente");
            },
            error: function (response) {
                alert(response);
            }
        });
    }

    var btnCancelarET = new TVclInputbutton(ContBotonesET_udiv[0].Child[0].This, "");
    btnEliminarET.Text = get_LangTextDefault("btnCancelarET");
    btnEliminarET.VCLType = TVCLType.BS;

    btnCancelarET.This.classList.add("btnCancelarET");
    //btnCancelarET.classList.add("btnstep");
    btnCancelarET.This.setAttribute("data-IdET", MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE);
    btnCancelarET.This.setAttribute("data-IdWHOTT", inIDSDWHOTOCASE);
    btnCancelarET.onClick = function () {
        var IdET = parseInt($(this).attr('data-IdET'));
        var ArrayComtrolsTemp = ArrayControls;
        ArrayComtrolsTemp.push(FieldIDSDCASEMT_input);
        ArrayComtrolsTemp.push(FieldExtraTable_input);
        ULimpiarControles(ArrayComtrolsTemp);
        UHabilitaControles(false, ArrayControls)
        $('#stateForm' + MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE + IdxIDSDWHOTOCASE).val(StateFormulario._Cancel);
        UHabilitaBotones(btnNuevoET, btnGrabarET, btnEliminarET, btnCancelarET, true, false, false, false, MDSERVICEEXTRATABLE.GRIDENABLE, MDSERVICEEXTRATABLE.READONLY, false);
    }

    UROWEXTRATABLE_GET(objContenedor, inIDSDWHOTOCASE, MDSERVICEEXTRATABLE, MDSERVICEEXTRATABLE.MDSERVICEEXTRAFIELDSList);

    var NroADN = ContarADN(MDSERVICEEXTRATABLE.MDSERVICEEXTRAFIELDSList);

    if (!(NroADN > 0)) {
        btnNuevoET.Visible = false;
        btnCancelarET.Visible = false;
        UHabilitaBotones(btnNuevoET, btnGrabarET, btnEliminarET, btnCancelarET, false, true, true, false, MDSERVICEEXTRATABLE.GRIDENABLE, MDSERVICEEXTRATABLE.READONLY, true);
        UHabilitaControles(true, ArrayControls)
    }
    else {
        HabilitaBotones(btnNuevoET, btnGrabarET, btnEliminarET, btnCancelarET, true, false, false, false, MDSERVICEEXTRATABLE.GRIDENABLE, MDSERVICEEXTRATABLE.READONLY, false);
        UHabilitaControles(false, ArrayControls)
    }

    return btnGrabarET;
}


function UROWEXTRATABLE_GET(objContenedor, inIDSDWHOTOCASE, MDSERVICEEXTRATABLE, MDSERVICEEXTRAFIELDList) {
    var data = {
        IDSDWHOTOCASE: inIDSDWHOTOCASE, //parseInt($('#IDSDWHOTOCASE').val()),
        IDMDSERVICEEXTRATABLE: MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE
    };
    var NroADN_Result = 0;
    $.ajax({
        type: "POST",
        url: xRaiz + 'ST/STEditor/frCASESTEditor.svc/ROWEXTRATABLE_GET',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (response) {
            var IdxIDSDWHOTOCASE = '_' + inIDSDWHOTOCASE;
            var TableET = Utable(objContenedor, "GridFields" + MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE + IdxIDSDWHOTOCASE);
            var TableHeadET = Uthead(TableET, "");
            var TableHeadRowET = Utr(TableHeadET, "");

            for (var i = 0; i < response.Columnas.length; i++) {
                var TableHeadThET = Uth(TableHeadRowET, response.Columnas[i].IDMDSERVICEEXTRAFIELDS + IdxIDSDWHOTOCASE);
                TableHeadThET.innerHTML = response.Columnas[i].NombreColumna;
                TableHeadThET.setAttribute("data-IdEF", response.Columnas[i].IDMDSERVICEEXTRAFIELDS);
                TableHeadThET.setAttribute("data-IdET", MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE);
            }

            var TableBodyET = Utbody(TableET, "");
            for (var i = 0; i < response.Filas.length; i++) {
                var TableBodyRowET = Utr(TableBodyET, "");
                TableBodyRowET.addEventListener("click", function () {
                    ClickPrimerRow(TableBodyET, IdxIDSDWHOTOCASE, TableBodyRowET);
                });
                for (var j = 0; j < response.Filas[i].Celdas.length; j++) {
                    var TableBodyTdET = Utd(TableBodyRowET, "");
                    TableBodyTdET.innerHTML = response.Filas[i].Celdas[j].Valor;
                }
            }

            var NroADN = ContarADN(MDSERVICEEXTRAFIELDList);

            Uinput(objContenedor, TImputtype.hidden, "NroADN" + MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE + IdxIDSDWHOTOCASE, NroADN, false);

            var input_StateForm = Uinput(objContenedor, TImputtype.hidden, "stateForm" + MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE + IdxIDSDWHOTOCASE, StateFormulario._Nuevo, false);

            if (!(NroADN > 0)) {
                var row = $(TableBodyET).children('tr:first');
                var stateForm = ClickPrimerRow(TableBodyET, IdxIDSDWHOTOCASE, row);
                input_StateForm.value = stateForm;
                TableET.style.display = "none";
            }
            else {
                input_StateForm.value = StateFormulario._Cancel;
            }
        },
        error: function (response) {
            alert(response.ErrorMessage);
        }
    });
}

function ROWEXTRATABLE_GET_update(objContenedor, inIDSDWHOTOCASE, MDSERVICEEXTRATABLE, MDSERVICEEXTRAFIELDSList) {

    var data = {
        IDSDWHOTOCASE: inIDSDWHOTOCASE,
        'IDMDSERVICEEXTRATABLE': MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE
    };

    $.ajax({
        type: "POST",
        url: xRaiz + 'ST/STEditor/frCASESTEditor.svc/ROWEXTRATABLE_GET',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var IdxIDSDWHOTOCASE = '_' + inIDSDWHOTOCASE;
            $("#GridFields" + MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE + IdxIDSDWHOTOCASE).remove();

            var TableET = Utable(objContenedor, "GridFields" + MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE + IdxIDSDWHOTOCASE);
            var TableHeadET = Uthead(TableET, "");
            var TableHeadRowET = Utr(TableHeadET, "");

            for (var i = 0; i < response.Columnas.length; i++) {
                var TableHeadThET = Uth(TableHeadRowET, response.Columnas[i].IDMDSERVICEEXTRAFIELDS + IdxIDSDWHOTOCASE);
                TableHeadThET.innerHTML = response.Columnas[i].NombreColumna;
                TableHeadThET.setAttribute("data-IdEF", response.Columnas[i].IDMDSERVICEEXTRAFIELDS);
                TableHeadThET.setAttribute("data-IdET", MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE);
            }

            var TableBodyET = Utbody(TableET, "");
            for (var i = 0; i < response.Filas.length; i++) {
                var TableBodyRowET = Utr(TableBodyET, "");
                TableBodyRowET.addEventListener("click", function () {
                    ClickPrimerRow(TableBodyET, IdxIDSDWHOTOCASE, TableBodyRowET);
                });
                for (var j = 0; j < response.Filas[i].Celdas.length; j++) {
                    var TableBodyTdET = Utd(TableBodyRowET, "");
                    TableBodyTdET.innerHTML = response.Filas[i].Celdas[j].Valor;
                }
            }

            var NroADN = ContarADN(MDSERVICEEXTRAFIELDSList);
            $("#NroADN" + MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE + IdxIDSDWHOTOCASE).remove();
            $("#stateForm" + MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE + IdxIDSDWHOTOCASE).remove();

            Uinput(objContenedor, TImputtype.hidden, "NroADN" + MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE + IdxIDSDWHOTOCASE, NroADN, false);

            var input_StateForm = Uinput(objContenedor, TImputtype.hidden, "stateForm" + MDSERVICEEXTRATABLE.IDMDSERVICEEXTRATABLE + IdxIDSDWHOTOCASE, StateFormulario._Nuevo, false);

            if (!(NroADN > 0)) {
                var row = $(TableBodyET).children('tr:first');
                var stateForm = ClickPrimerRow(TableBodyET, IdxIDSDWHOTOCASE, row);
                input_StateForm.value = stateForm;
                TableET.style.display = "none";
            }
        },
        error: function (response) {
            alert(response.ErrorMessage);
        }
    });
}

function UConstruirLookUp(MDSERVICEEXTRAFIELDS, IDMDSERVICEEXTRATABLE, Objeto_Cont, inIDSDWHOTOCASE) {
    var IdxIDSDWHOTOCASE = '_' + inIDSDWHOTOCASE;
    var combo;

    var data = {
        IDSDWHOTOCASE: inIDSDWHOTOCASE,
        'IDMDSERVICEEXTRATABLE': IDMDSERVICEEXTRATABLE,
        'IDMDSERVICEEXTRAFIELDS': MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS
    };

    $.ajax({
        type: "POST",
        url: xRaiz + 'ST/STEditor/frCASESTEditor.svc/GetComboExtraField',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (response) {
            combo = Uselect(Objeto_Cont, "Field" + MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS + IdxIDSDWHOTOCASE);
            combo.setAttribute("data-NameField", MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME);
            combo.setAttribute("data-DataType", MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES);
            combo.setAttribute("data-Columnstyle", TExtrafields_ColumnStyle.LookUp);
            combo.setAttribute("data-Mandatory", MDSERVICEEXTRAFIELDS.MANDATORY);
            combo.setAttribute("data-Description", MDSERVICEEXTRAFIELDS.EXTRAFIELDS_DESCRIPTION);
            combo.style.display = UGetVisibility(MDSERVICEEXTRAFIELDS.IDMDLIFESTATUSPERMISSION);
            combo.style.marginBottom = "4px";
            $(combo).attr("disabled", UGetReadOnly(MDSERVICEEXTRAFIELDS.IDMDLIFESTATUSPERMISSION));
            for (var i = 0; i < response.length; i++) {
                Uoption(combo, "", response[i].ValueMember, response[i].DisplayMember);
            }
        },
        error: function (response) {
            alert(response.ErrorMessage);
        }
    });
    return combo;
}

function UConstruirLookUpOption(MDSERVICEEXTRAFIELDS, IDMDSERVICEEXTRATABLE, Objeto_Cont, inIDSDWHOTOCASE) {
    var IdxIDSDWHOTOCASE = '_' + inIDSDWHOTOCASE;
    var radiobuton;
    var data = {
        IDSDWHOTOCASE: inIDSDWHOTOCASE,
        'IDMDSERVICEEXTRATABLE': IDMDSERVICEEXTRATABLE,
        'IDMDSERVICEEXTRAFIELDS': MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS
    };

    var textito;
    $.ajax({
        type: "POST",
        url: xRaiz + 'ST/STEditor/frCASESTEditor.svc/GetComboExtraField',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (response) {
            //"<input name='" + NombreContenedor + "Field" + NombreContenedor + CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS + "' id='" + NombreContenedor + "Field" + NombreContenedor + CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS + "' data-NameField='" + CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_NAME + "' data-DataType='" + CMDBCIDEFINEEXTRAFIELDS.IDCMDBDBDATATYPES + "'>";
            radiobuton = new Array(response.length);
            var _div = Udiv(Objeto_Cont, '')
            _div.style.display = UGetVisibility(MDSERVICEEXTRAFIELDS.IDMDLIFESTATUSPERMISSION);
            _div.style.textAlign = 'left';
            for (var i = 0; i < response.length; i++) {

                var ctr_field = Uinput(_div, TImputtype.radio, "Field" + MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS + IdxIDSDWHOTOCASE + i, response[i].ValueMember, false);
                $(ctr_field).attr("data-NameField", MDSERVICEEXTRAFIELDS.EXTRAFIELDS_NAME);
                $(ctr_field).attr("data-DataType", MDSERVICEEXTRAFIELDS.IDCMDBDBDATATYPES);
                $(ctr_field).attr("data-Columnstyle", TExtrafields_ColumnStyle.LookUpOption);
                $(ctr_field).attr("data-Mandatory", MDSERVICEEXTRAFIELDS.MANDATORY);
                $(ctr_field).attr("data-Description", MDSERVICEEXTRAFIELDS.EXTRAFIELDS_DESCRIPTION);
                $(ctr_field).attr("name", "Field" + MDSERVICEEXTRAFIELDS.IDMDSERVICEEXTRAFIELDS + IdxIDSDWHOTOCASE);
                $(ctr_field).attr("disabled", UGetReadOnly(MDSERVICEEXTRAFIELDS.IDMDLIFESTATUSPERMISSION));
                ctr_field.style.marginBottom = "4px";
                ctr_field.style.marginRight = "2px";

                var lbl = Ulabel(_div, "", response[i].DisplayMember)
                lbl.style.marginRight = "9px";
                $(lbl).attr("for", ctr_field.id);

                radiobuton[i] = ctr_field;
                //var combo;

                //combo += "<input type='radio' name='" + NombreContenedor + "Field" + NombreContenedor + CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS + "' id='" + NombreContenedor + "Field" + NombreContenedor + CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS + "' data-Columnstyle='" + TExtrafields_ColumnStyle.LookUpOption + "' data-NameField='" + CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_NAME + "' data-DataType='" + CMDBCIDEFINEEXTRAFIELDS.IDCMDBDBDATATYPES + "' value='" + response[i].ValueMember + "'>"
                //combo += ;
            }
            //combo += "</select>";
            //textito = combo;
        },
        error: function (response) {
            alert(response.ErrorMessage);
        }
    });

    return radiobuton;
}




