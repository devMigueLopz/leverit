﻿ItHelpCenter.CMDB.CIEditor.frCIRelationGraphics = function (inObjectHtml, Id) {
    this.TParent = function () {
        return this;
    }.bind(this);

    var _this = this.TParent();
    this.Id = Id;
    this.Name = "frCIRelationGraphics";
    this.Mythis = "frCIRelationGraphics";

    this.ObjectHtml = inObjectHtml;
    this._Diag = null; //TDiagramMap
    this._Prov = null; //Atis.CMDB.DiagramMap.DiagramMapProvider
    this.CIProfiler = null;
    this.InitializeComponent();

    _this.Diagramis = MindFusion.AbstractionLayer.createControl(MindFusion.Diagramming.Diagram, null, null, null, _this.ElementDiagramGraphic);

    _this.Diagramis.addEventListener(MindFusion.Diagramming.Events.nodeDoubleClicked, function (sender, e) {
        var _node = e.node;

        _this.CIProfiler.GETCI(parseInt(_node.getId()));

        _this._Prov.BuildMapRealationsClicked(_this._Diag, _this.Diagramis, _this.CIProfiler);
    });

    var overview = MindFusion.AbstractionLayer.createControl(MindFusion.Diagramming.Overview, null, null, null, _this.ElementFractalGraphic);
    overview.setDiagram(_this.Diagramis);

}.Implements(new Source.Page.Properties.Page());

ItHelpCenter.CMDB.CIEditor.frCIRelationGraphics.prototype.BtnZoomUpClick = function (sender, element) {
    try {
        sender.Diagramis.setZoomFactor(Math.max(20, sender.Diagramis.zoomFactor + 10));
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("frCIRelationGraphics.js ItHelpCenter.CMDB.CIEditor.frCIRelationGraphics.prototype.BtnZoomUpClick", e);
    }
}

ItHelpCenter.CMDB.CIEditor.frCIRelationGraphics.prototype.BtnZoomDownClick = function (sender, element) {
    try {
        sender.Diagramis.setZoomFactor(Math.max(20, sender.Diagramis.zoomFactor - 10));
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("frCIRelationGraphics.js ItHelpCenter.CMDB.CIEditor.frCIRelationGraphics.prototype.BtnZoomDownClick", e);
    }
}

ItHelpCenter.CMDB.CIEditor.frCIRelationGraphics.prototype.BtnSavePngClick = function (sender, element) {
    try {
        var canvas = sender.ElementDiagramGraphic;
        if (canvas.msToBlob) {
            var blob = canvas.msToBlob();
            window.navigator.msSaveBlob(blob, 'Diagram.png');
        } else {
            element.href = canvas.toDataURL();
            element.download = "Diagram.png";
        }

    } catch (e) {
        SysCfg.Log.Methods.WriteLog("frCIRelationGraphics.js ItHelpCenter.CMDB.CIEditor.frCIRelationGraphics.prototype.BtnSavePngClick", e);
    }
}

ItHelpCenter.CMDB.CIEditor.frCIRelationGraphics.prototype.Initialize = function (_CIProfiler)
{
    var _this = this.TParent();

    _this.CIProfiler = _CIProfiler;
    _this._Diag = new ItHelpCenter.ComponentMindFusionManagerDiagramingProvider.TDiagramMap();
    _this._Prov = new ItHelpCenter.CMDB.DiagramMap.DiagramMapProvider();

    
    //_this.Diagramis.id = _this.Name + _this.id + "_Diagram";

    

    _this._Prov.BuildMapRealations(_this._Diag, _this.Diagramis, _this.CIProfiler);
    //_this.btnTextoMuestra.Visible = false;
}

ItHelpCenter.CMDB.CIEditor.frCIRelationGraphics.prototype.btnTextoOculta_Click_1 = function(sender, e)
{
    //foreach (var item in Diagramis.Nodes)
    //{
    //    (item as MindFusion.Diagramming.ShapeNode).Text = "";
    //    // (item as MindFusion.Diagramming.ShapeNode).ToolTip = new ToolTip() { Content = ((TNode)item.Tag).Text };


    //    //(item as MindFusion.Diagramming.ShapeNode).ResizeToFitImage();
    //}
    //btnTextoOculta.Visibility = System.Windows.Visibility.Collapsed;
    //btnTextoMuestra.Visibility = System.Windows.Visibility.Visible;
}

ItHelpCenter.CMDB.CIEditor.frCIRelationGraphics.prototype.btnTextoMuestra_Click_1 = function(sender, e)
{
    //foreach (var item in Diagramis.Nodes)
    //{
    //    (item as MindFusion.Diagramming.ShapeNode).Text = ((TNode)item.Tag).Text;
    //    //(item as MindFusion.Diagramming.ShapeNode).ToolTip = null;
    //}
    //btnTextoOculta.Visibility = System.Windows.Visibility.Visible;
    //btnTextoMuestra.Visibility = System.Windows.Visibility.Collapsed;
}