﻿







//TDataType = {
//    Unknown: 0,
//    String: 1,
//    Int32: 2,
//    Boolean: 3,
//    AutoInc: 4,
//    Text: 5,
//    Double: 6,
//    DateTime: 7,
//    Object: 8,
//    Decimal: 9
//}

//TKeyType = {
//    EXT: 0,
//    ADN: 1,
//    VAR: 2,
//    NEW: 3,
//    INS: 4,
//    LOG: 5,
//}


ItHelpCenter.CIEditor.TfrCiEditor = function (inObjectHtml, ID, inCallBack) {
    this.ID = "CIEditor_" + ID;

    var frCIEditor = new TVclStackPanel(inObjectHtml, this.ID, 2, [[12, 12], [5, 7], [5, 7], [5, 7], [5, 7]]);
    var CIEditorSearch = frCIEditor.Column[0].This; // busquedas de CIs
    var CIEditorMain = frCIEditor.Column[1].This; // Editor del CI seleccionado
    this.CISearchCIEditor;
    this.CIEditor;

    var _this = this;
    this.CIEditor = new ItHelpCenter.CIEditor.TCIEditor(CIEditorMain, ID, function (CI) {
        _this.CISearchCIEditor.Grilla.FocusedRowHandle.SetCellValue(CI, "CI_SERIALNUMBER");
    });


    this.CISearchCIEditor = new ItHelpCenter.CIEditor.TCISearchCIEditor(CIEditorSearch, ID, function (Newrow) {
        //CARGAMOS EDITOR CON PESTAÑAS
        //alert(Newrow.This);
        _this.CIEditor.GetCMDBCI(parseInt(Newrow));
    });

}

// Inicio Estructura del Editor
ItHelpCenter.CIEditor.TCIEditor = function (inObjectHTML, ID, inCallBack) {
    this.IDCMDBCI = 0;
    this.tabControlInfo;
    this.tabControlOR;
    this.tabControlIR;
    this.StateForm = UsrCfg.CMDB.Properties.StateFormulario.New;

    var _this = this;

    this.CargarCIdefine = function (cbDefine) {
        $.ajax({
            type: "POST",
            url: SysCfg.App.Properties.xRaiz + 'CMDB/CIEditor/CIEditor.svc/GetCIDefine',
            data: {},
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (response) {
                for (var i = 0; i < response.length; i++) {
                    var cbItem = new TVclComboBoxItem();
                    cbItem.Value = response[i].IDCMDBCIDEFINE;
                    cbItem.Text = response[i].CIDEFINE_NAME;
                    cbItem.Tag = response[i];
                    cbDefine.AddItem(cbItem);
                }
                cbDefine.selectedIndex = 0;
            },
            error: function (response) {
                alert("no se puede acceder a [GetCIDefine]");
            }
        });
    }

    this.CargarState = function (cbState) {
        $.ajax({
            type: "POST",
            url: SysCfg.App.Properties.xRaiz + 'CMDB/CIEditor/CIEditor.svc/GetCIState',
            data: {},
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (response) {
                for (var i = 0; i < response.length; i++) {
                    var cbItem = new TVclComboBoxItem();
                    cbItem.Value = response[i].ValueMember;
                    cbItem.Text = response[i].DisplayMember;
                    cbItem.Tag = response[i];
                    cbState.AddItem(cbItem);
                }
                cbState.selectedIndex = 0;
            },
            error: function (response) {
                alert('no se puede acceder a [GetCIState]');
            }
        });
    }

    this.CargarBrand = function (cbBrand) {
        $.ajax({
            type: "POST",
            url: SysCfg.App.Properties.xRaiz + 'CMDB/CIEditor/CIEditor.svc/GetCIBrand',
            data: {},
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (response) {
                for (var i = 0; i < response.length; i++) {
                    var cbItem = new TVclComboBoxItem();
                    cbItem.Value = response[i].IDCMDBBRAND;
                    cbItem.Text = response[i].BRAND_NAME;
                    cbItem.Tag = response[i];
                    cbBrand.AddItem(cbItem);
                }
                cbBrand.selectedIndex = 0;
            },
            error: function (response) {
                alert('no se puede acceder a [GetCIBrand]');
            }
        });
    }

    this.CrearBasiInformation = function () {

        //this.tabControlInfo.ClearTabPages();

        var tabPageBasic = new TTabPage();
        tabPageBasic.Name = "Basic";
        tabPageBasic.Caption = "Basic";
        tabPageBasic.Active = true;
        this.tabControlInfo.AddTabPages(tabPageBasic);

        //var BIIDCMDBCI = new TVclStackPanel(tabPageBasic.Page, ID, 2, [[12, 12], [5, 7], [5, 7], [5, 7], [5, 7]]);
        //this.lblIDCMDBCI = new TVcllabel(BIIDCMDBCI.Column[0].This, "txtIDCMDBCI", TlabelType.H0);
        //this.lblIDCMDBCI.Text = "IDCMDBCI: ";
        //this.txtIDCMDBCI = new TVclTextBox(BIIDCMDBCI.Column[1].This, "");

        var BISerial = new TVclStackPanel(tabPageBasic.Page, ID, 2, [[12, 12], [5, 7], [5, 7], [5, 7], [5, 7]]);
        this.lblSerial = new TVcllabel(BISerial.Column[0].This, "txtSerial", TlabelType.H0);
        this.lblSerial.Text = "Serial: ";
        this.txtSerial = new TVclTextBox(BISerial.Column[1].This, "");

        var BIName = new TVclStackPanel(tabPageBasic.Page, ID, 2, [[12, 12], [5, 7], [5, 7], [5, 7], [5, 7]]);
        this.lblName = new TVcllabel(BIName.Column[0].This, "lblName", TlabelType.H0);
        this.lblName.Text = "Name: ";
        this.txtName = new TVclTextBox(BIName.Column[1].This, "");

        var BIDefine = new TVclStackPanel(tabPageBasic.Page, ID, 2, [[12, 12], [5, 7], [5, 7], [5, 7], [5, 7]]);
        this.lblDefine = new TVcllabel(BIDefine.Column[0].This, "lblDefine", TlabelType.H0);
        this.lblDefine.Text = "CI Define: ";
        this.cbDefine = new TVclComboBox2(BIDefine.Column[1].This, 'cbDefine');
        this.CargarCIdefine(this.cbDefine);

        var BIAcquiredDate = new TVclStackPanel(tabPageBasic.Page, ID, 2, [[12, 12], [5, 7], [5, 7], [5, 7], [5, 7]]);
        this.lblAcquiredDate = new TVcllabel(BIAcquiredDate.Column[0].This, "lblAcquiredDate", TlabelType.H0);
        this.lblAcquiredDate.Text = "Acquired Date: ";
        this.txtAcquiredDate = new TVclTextBox(BIAcquiredDate.Column[1].This, "");
        $(this.txtAcquiredDate.This).datetimepicker({ showButtonPanel: true });

        var BIState = new TVclStackPanel(tabPageBasic.Page, ID, 2, [[12, 12], [5, 7], [5, 7], [5, 7], [5, 7]]);
        this.lblState = new TVcllabel(BIState.Column[0].This, "lblState", TlabelType.H0);
        this.lblState.Text = "State: ";
        this.cbState = new TVclComboBox2(BIState.Column[1].This, 'cbState');
        this.CargarState(this.cbState);

        var BIDisposeDate = new TVclStackPanel(tabPageBasic.Page, ID, 2, [[12, 12], [5, 7], [5, 7], [5, 7], [5, 7]]);
        this.lblDisposeDate = new TVcllabel(BIDisposeDate.Column[0].This, "lblDisposeDate", TlabelType.H0);
        this.lblDisposeDate.Text = "Dispose Date: ";
        this.txtDisposeDate = new TVclTextBox(BIDisposeDate.Column[1].This, "");
        $(this.txtDisposeDate.This).datetimepicker({ showButtonPanel: true });

        var BIBrand = new TVclStackPanel(tabPageBasic.Page, ID, 2, [[12, 12], [5, 7], [5, 7], [5, 7], [5, 7]]);
        this.lblBrand = new TVcllabel(BIBrand.Column[0].This, "lblBrand", TlabelType.H0);
        this.lblBrand.Text = "Brand: ";
        this.cbBrand = new TVclComboBox2(BIBrand.Column[1].This, 'cbBrand');
        this.CargarBrand(this.cbBrand);

        var BIOtherDetails = new TVclStackPanel(tabPageBasic.Page, ID, 2, [[12, 12], [5, 7], [5, 7], [5, 7], [5, 7]]);
        this.lblOtherDetails = new TVcllabel(BIOtherDetails.Column[0].This, "lblOtherDetails", TlabelType.H0);
        this.lblOtherDetails.Text = "Other Details: ";
        this.txtOtherDetails = new TVclTextBox(BIOtherDetails.Column[1].This, "");
    }

    this.ContButtons = new TVclStackPanel(inObjectHTML, ID, 2, [[6, 6], [6, 6], [6, 6], [6, 6], [6, 6]]);



    //Inicio contruccion el tabs principal con las 3 pestañas
    var tabControlPrincipal = new TTabControl(inObjectHTML, ID, inCallBack);
    var tabPageInfo = new TTabPage();
    tabPageInfo.Name = "CIInformation";
    tabPageInfo.Caption = "CI Information";
    tabPageInfo.Active = true;
    tabControlPrincipal.AddTabPages(tabPageInfo);

    var tabPageOutputRelation = new TTabPage();
    tabPageOutputRelation.Name = "OutputRelation";
    tabPageOutputRelation.Caption = "Output Relation";
    tabControlPrincipal.AddTabPages(tabPageOutputRelation);

    var tabPageInputRelation = new TTabPage();
    tabPageInputRelation.Name = "InputRelation";
    tabPageInputRelation.Caption = "Input Relation";
    tabControlPrincipal.AddTabPages(tabPageInputRelation);
    //fin contruccion el tab principal con las 3 pestañas 

    this.tabControlOR = new TTabControl(tabPageOutputRelation.Page, ID, inCallBack);

    this.tabControlIR = new TTabControl(tabPageInputRelation.Page, ID, inCallBack);

    //Inicio contruccion tabs de informacion     
    this.tabControlInfo = new TTabControl(tabPageInfo.Page, ID, inCallBack);

    this.CrearBasiInformation();

    this.EnabledControls = function (value) {
        this.txtSerial.Enabled = value;
        this.txtName.Enabled = value;
        this.cbDefine.Enabled = value;
        this.txtAcquiredDate.Enabled = value;
        this.cbState.Enabled = value;
        this.txtDisposeDate.Enabled = value;
        this.cbBrand.Enabled = value;
        this.txtOtherDetails.Enabled = value;
    }

    this.CleanControls = function () {
        this.txtSerial.Text = "";
        this.txtName.Text = "";
        this.txtAcquiredDate.Text = "";
        this.txtDisposeDate.Text = "";
        this.txtOtherDetails.Text = "";
    }

    this.EnabledButton = function (_New, _Save, _Delete, _Cancel) {
        this.btnNuevo.Enabled = _New;
        this.btnGuardar.Enabled = _Save;
        this.btnEliminar.Enabled = _Delete;
        this.btnCancelar.Enabled = _Cancel;
    }

    this.btnNuevo = new TVclButton(this.ContButtons.Column[1].This, ID);
    this.btnNuevo.Text = "New";
    this.btnNuevo.onClick = function () {
        _this.CleanControls();
        _this.tabControlInfo.ClearTabPagesExcept("Basic");
        _this.tabControlInfo.TabPages[0].Active = true;
        //falta eliminar tabs de relaciones output e input
        _this.EnabledControls(true);
        _this.EnabledButton(false, true, false, true);
        _this.StateForm = UsrCfg.CMDB.Properties.StateFormulario.New;
        //seleccionar el primer tab de inf basic
    }

    this.btnGuardar = new TVclButton(this.ContButtons.Column[1].This, ID);
    this.btnGuardar.Text = "Save";
    this.btnGuardar.onClick = function () {
        var _IDCMDBCI = _this.IDCMDBCI;
        var _CI_SERIALNUMBER = _this.txtSerial.Text;
        var _CI_GENERICNAME = _this.txtName.Text;
        var _CI_DESCRIPTION = _this.txtOtherDetails.Text;
        var _IDCMDBCIDEFINE = parseInt(_this.cbDefine.Value);
        var _IDCMDBCISTATE = parseInt(_this.cbState.Value);
        var _IDCMDBBRAND = parseInt(_this.cbBrand.Value);

        var _CIDEFINE_NAME = _this.cbDefine.Text;

        var _CMDBCI = {
            IDCMDBCI: _IDCMDBCI,
            CI_SERIALNUMBER: _CI_SERIALNUMBER,
            CI_GENERICNAME: _CI_GENERICNAME,
            CI_DESCRIPTION: _CI_DESCRIPTION,
            IDCMDBCIDEFINE: _IDCMDBCIDEFINE,
            IDCMDBCISTATE: _IDCMDBCISTATE,
            IDCMDBBRAND: _IDCMDBBRAND
        };



        var _data = JSON.stringify({ CMDBCI: _CMDBCI, StateForm: _this.StateForm });

        $.ajax({
            type: "POST",
            url: SysCfg.App.Properties.xRaiz + "CMDB/CIEditor/CIEditor.svc/GrabarCI",
            contentType: "application/json; charset=utf-8",
            data: _data,
            success: function (response) {
                _this.CleanControls();
                _this.tabControlInfo.ClearTabPagesExcept("Basic");
                _this.tabControlInfo.TabPages[0].Active = true;
                //falta eliminar tabs de relaciones output e input
                _this.EnabledControls(false);
                _this.EnabledButton(true, false, false, false);
                alert('Se grabo correctamente.');
                if (_this.StateForm == UsrCfg.CMDB.Properties.StateFormulario.Update)
                    inCallBack(_CMDBCI);
            },
            error: function (response) {
                alert(response);
            }
        });
    }

    this.btnEliminar = new TVclButton(this.ContButtons.Column[1].This, ID);
    this.btnEliminar.Text = "Delete";
    this.btnEliminar.onClick = function () {

    }

    this.btnCancelar = new TVclButton(this.ContButtons.Column[1].This, ID);
    this.btnCancelar.Text = "Cancel";
    this.btnCancelar.onClick = function () {
        _this.CleanControls();
        _this.tabControlInfo.ClearTabPagesExcept("Basic");
        _this.tabControlInfo.TabPages[0].Active = true;
        //falta eliminar tabs de relaciones output e input
        _this.EnabledControls(false);
        _this.EnabledButton(true, false, false, false);
        _this.StateForm = UsrCfg.CMDB.Properties.StateFormulario.New;
    }

    this.GetCMDBCI = function (inIDCMDBCI) {

        //this.CrearBasiInformation();
        this.tabControlInfo.ClearTabPagesExcept("Basic");        
        this.tabControlInfo.TabPages[0].Active = true;

        this.tabControlOR.ClearTabPages();
        this.tabControlIR.ClearTabPages();

        this.StateForm = UsrCfg.CMDB.Properties.StateFormulario.Update;

        this.EnabledControls(true);
        this.EnabledButton(true, true, false, true);

        var _this = this;

        this.IDCMDBCI = inIDCMDBCI;
        var data = {
            'IDCMDBCI': this.IDCMDBCI
        };
        $.ajax({
            type: "POST",
            url: SysCfg.App.Properties.xRaiz + 'CMDB/CIEditor/CIEditor.svc/GetCI',
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (CMDBCI) {
                //_this.txtIDCMDBCI.Text = CMDBCI.IDCMDBCI;
                _this.txtSerial.Text = CMDBCI.CI_SERIALNUMBER;
                _this.txtName.Text = CMDBCI.CI_GENERICNAME;
                _this.cbDefine.Value = CMDBCI.IDCMDBCIDEFINE;
                _this.txtAcquiredDate.Text = formatJSONDateTime(CMDBCI.CI_DATEIN);
                _this.cbState.Value = CMDBCI.IDCMDBCISTATE;
                _this.txtDisposeDate.Text = formatJSONDateTime(CMDBCI.CI_DATEOUT);
                _this.cbBrand.Value = CMDBCI.CMDBBRAND.IDCMDBBRAND;
                _this.txtOtherDetails.Text = CMDBCI.CI_DESCRIPTION;

                var ExtraTablas = CMDBCI.CMDBCIDEFINE.CMDBCIDEFINEEXTRATABLElist;

                for (var i = 0; i < ExtraTablas.length; i++) {
                    var tabPageET = new TTabPage();
                    tabPageET.Name = ExtraTablas[i].EXTRATABLE_NAME;
                    tabPageET.Caption = ExtraTablas[i].EXTRATABLE_NAME;
                    tabPageET.Tag = ExtraTablas[i];

                    var ExtraTableEditor = new ItHelpCenter.CIEditor.TExtraTableEditor(tabPageET.Page, ID, inCallBack, inIDCMDBCI, ExtraTablas[i])

                    _this.tabControlInfo.AddTabPages(tabPageET);
                }

            },
            error: function (response) {
                alert(response.ErrorMessage);
            }
        });

        $.ajax({
            type: "POST",
            url: SysCfg.App.Properties.xRaiz + 'CMDB/CIEditor/CIEditor.svc/GetTyepeRelationsChilds',
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                for (var i = 0; i < response.length; i++) {
                    var tabPageET = new TTabPage();
                    tabPageET.Name = "OR" + response[i].IDCMDBCIDEFINERELATIONTYPE;
                    tabPageET.Caption = response[i].CIDEFINERELATIONTYPE_NAME;
                    tabPageET.Tag = response[i];

                    var CIRelationsEditor = new ItHelpCenter.CIEditor.TCIRelationsEditor(tabPageET.Page, ID, inCallBack, response[i], inIDCMDBCI, true)

                    _this.tabControlOR.AddTabPages(tabPageET);
                }
                if (response.length > 0)
                    _this.tabControlOR.TabPages[0].Active = true;
            },
            error: function (response) {
                alert(response.ErrorMessage);
            }
        });

        $.ajax({
            type: "POST",
            url: SysCfg.App.Properties.xRaiz + 'CMDB/CIEditor/CIEditor.svc/GetTyepeRelationsParents',
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                for (var i = 0; i < response.length; i++) {
                    var tabPageET1 = new TTabPage();
                    tabPageET1.Name = "IR" + response[i].IDCMDBCIDEFINERELATIONTYPE;
                    tabPageET1.Caption = response[i].CIDEFINERELATIONTYPE_NAME;
                    tabPageET1.Tag = response[i];

                    var CIRelationsEditor = new ItHelpCenter.CIEditor.TCIRelationsEditor(tabPageET1.Page, ID, inCallBack, response[i], inIDCMDBCI, false)

                    _this.tabControlIR.AddTabPages(tabPageET1);
                }
                if (response.length > 0)
                    _this.tabControlIR.TabPages[0].Active = true;
            },
            error: function (response) {
                alert(response.ErrorMessage);
            }
        });

    }
    //fin contruccion tabs de informacion basica
}
// Fin Estructura del Editor

//Inicio Estructura del ExtraTableEditor
ItHelpCenter.CIEditor.TExtraTableEditor = function (inObjectHTML, ID, inCallBack, inIDCMDBCI, inCMDBCIDEFINEEXTRATABLE) {
    this.This;
    this.StateForm = UsrCfg.CMDB.Properties.StateFormulario.New;
    this.btnNuevo;
    this.btnGuardar;
    this.btnEliminar;
    this.btnCancelar;
    this.Grilla;
    this.IsADN = false;
    this.ExtraFieldEditors = new Array();

    this.IDCMDBCI = inIDCMDBCI;
    this.txt_IDCMDB_EF;
    this.txt_IDCMDB;
    this.CMDBCIDEFINEEXTRATABLE = inCMDBCIDEFINEEXTRATABLE;

    for (var i = 0; i < inCMDBCIDEFINEEXTRATABLE.CMDBCIDEFINEEXTRAFIELDSList.length; i++) {
        if (inCMDBCIDEFINEEXTRATABLE.CMDBCIDEFINEEXTRAFIELDSList[i].IDCMDBKEYTYPE == SysCfg.DB.Properties.TKeyType.ADN) {
            this.IsADN = true;
        }
    }

    var _this = this;

    this.Contenedor = new TVclStackPanel(inObjectHTML, ID, 1, [[12], [12], [12], [12], [12]]);

    var ConteIDCMDB_EF = new TVclStackPanel(this.Contenedor.Column[0].This, ID, 2, [[12, 12], [5, 7], [5, 7], [5, 7], [5, 7]]);
    var lbl_IDCMDB_EF = new TVcllabel(ConteIDCMDB_EF.Column[0].This, "", TlabelType.H0);
    lbl_IDCMDB_EF.Text = "IDCMDB_EF" + this.CMDBCIDEFINEEXTRATABLE.EXTRATABLE_NAMETABLE;
    this.txt_IDCMDB_EF = new TVclTextBox(ConteIDCMDB_EF.Column[1].This, "");
    this.txt_IDCMDB_EF.Enabled = false;

    var ConteIDCMDBCI = new TVclStackPanel(this.Contenedor.Column[0].This, ID, 2, [[12, 12], [5, 7], [5, 7], [5, 7], [5, 7]]);
    var lbl_IDCMDBCI = new TVcllabel(ConteIDCMDBCI.Column[0].This, "", TlabelType.H0);
    lbl_IDCMDBCI.Text = "IDCMDBCI";
    this.txt_IDCMDB = new TVclTextBox(ConteIDCMDBCI.Column[1].This, "");
    this.txt_IDCMDB.Enabled = false;

    this.AddExtraFieldEditor = function (ExtraFieldEditor) {
        $(this.Contenedor.Column[0].This).append(ExtraFieldEditor.Contenedor.Row.This);
        this.ExtraFieldEditors.push(ExtraFieldEditor);
    }


    for (var j = 0; j < this.CMDBCIDEFINEEXTRATABLE.CMDBCIDEFINEEXTRAFIELDSList.length; j++) {
        var ExtraFieldEditor = new ItHelpCenter.CIEditor.TExtraFieldEditor("", ID, inCallBack, inIDCMDBCI, this.CMDBCIDEFINEEXTRATABLE.CMDBCIDEFINEEXTRAFIELDSList[j]);
        $(this.Contenedor.Column[0].This).append(ExtraFieldEditor.Contenedor.Row.This);
        this.ExtraFieldEditors.push(ExtraFieldEditor);
    }

    var Contbuttons = new TVclStackPanel(inObjectHTML, ID, 1, [[12], [12], [12], [12], [12]]);

    this.pnGrilla = new TVclStackPanel(inObjectHTML, ID, 1, [[12], [12], [12], [12], [12]]);

    this.RefreshGrid = function () {
        var data2 = {
            'IDCMDBCIDEFINEEXTRATABLE': this.CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE,
            'IDCMDBCI': this.IDCMDBCI
        };

        $.ajax({
            type: "POST",
            url: SysCfg.App.Properties.xRaiz + 'CMDB/CIEditor/CIEditor.svc/ROWEXTRATABLE_GET',
            data: JSON.stringify(data2),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (response) {

                _this.Grilla = new TGrid(_this.pnGrilla.Column[0].This, ID, inCallBack);

                for (var i = 0; i < response.Columnas.length; i++) {
                    var Col = new TColumn();
                    Col.Name = response.Columnas[i].NombreColumna
                    Col.Caption = response.Columnas[i].NombreColumna
                    Col.Index = i;
                    _this.Grilla.AddColumn(Col);
                }

                for (var i = 0; i < response.Filas.length; i++) {
                    var NewRow = new TRow();
                    for (var j = 0; j < response.Filas[i].Celdas.length; j++) {
                        var Cell = new TCell();
                        Cell.Value = response.Filas[i].Celdas[j].Valor;
                        Cell.IndexColumn = j;
                        Cell.IndexRow = i;
                        NewRow.AddCell(Cell);
                    }
                    _this.Grilla.AddRow(NewRow);
                }

                if (!_this.IsADN) {
                    _this.SetExtraField();
                }

            },
            error: function (response) {
                alert(response.ErrorMessage);
            }
        });
    }

    this.btnNuevo = new TVclButton(Contbuttons.Column[0].This, ID);
    this.btnNuevo.Text = "New";
    this.btnNuevo.onClick = function () {
        _this.CleanControls();
        _this.EnabledControls(true);
        _this.StateForm = UsrCfg.CMDB.Properties.StateFormulario.New;
        _this.EnabledButton(false, true, false, true);
    }

    this.btnGuardar = new TVclButton(Contbuttons.Column[0].This, ID);
    this.btnGuardar.Text = "Save";
    this.btnGuardar.onClick = function () {
        var parametros = [];
        for (var i = 0; i < _this.ExtraFieldEditors.length; i++) {
            var Atributos = [];
            Atributos[0] = _this.ExtraFieldEditors[i].CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_NAME
            Atributos[1] = _this.ExtraFieldEditors[i].GetValue();
            Atributos[2] = _this.ExtraFieldEditors[i].CMDBCIDEFINEEXTRAFIELDS.IDCMDBDBDATATYPES
            parametros[i] = Atributos;
        }

        var IdCI = [];
        IdCI[0] = "IDCMDBCI";
        IdCI[1] = _this.txt_IDCMDB.Text;
        IdCI[2] = SysCfg.DB.Properties.TDataType.Int32.value;
        parametros.push(IdCI);

        var IdEF = [];
        IdEF[0] = "IDCMDB_EF" + _this.CMDBCIDEFINEEXTRATABLE.EXTRATABLE_NAMETABLE;
        IdEF[1] = _this.txt_IDCMDB_EF.Text;
        IdEF[2] = SysCfg.DB.Properties.TDataType.Int32.value;
        parametros.push(IdEF)

        _data = JSON.stringify({
            IDCMDBCIDEFINEEXTRATABLE: _this.CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE,
            arrayString: parametros,
            stateForm: _this.StateForm,
            IDCMDBCI: _this.IDCMDBCI
        });

        $.ajax({
            type: "POST",
            url: SysCfg.App.Properties.xRaiz + "CMDB/CIEditor/CIEditor.svc/GrabarExtraTable",
            contentType: "application/json; charset=utf-8",
            data: _data,
            async: false,
            success: function (response) {
                if (_this.StateForm == UsrCfg.CMDB.Properties.StateFormulario.Update) {
                    if (_this.IsADN) {
                        _this.CleanControls();
                        _this.EnabledControls(false);
                        _this.StateForm = UsrCfg.CMDB.Properties.StateFormulario.New;
                        _this.EnabledButton(true, false, false, false);
                    }
                    else {
                        _this.StateForm = UsrCfg.CMDB.Properties.StateFormulario.Update;
                        _this.EnabledButton(true, true, true, false);
                    }
                } else {
                    if (_this.StateForm == UsrCfg.CMDB.Properties.StateFormulario.Update) {
                        if (_this.IsADN) {
                            _this.CleanControls();
                            _this.EnabledControls(false);
                            _this.EnabledButton(true, false, false, false);
                        }
                        else {
                            _this.EnabledButton(true, true, true, false);
                        }
                    }
                }
                alert('Se grabo correctamente.');
                _this.RefreshGrid();
            },
            error: function (response) {
                alert("No se puede acceder a [GrabarExtraTable]");
            }
        });
    }

    this.btnEliminar = new TVclButton(Contbuttons.Column[0].This, ID);
    this.btnEliminar.Text = "Delete";
    this.btnEliminar.onClick = function () {
        var parametros = [];
        for (var i = 0; i < _this.ExtraFieldEditors.length; i++) {
            var Atributos = [];
            Atributos[0] = _this.ExtraFieldEditors[i].CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_NAME
            Atributos[1] = _this.ExtraFieldEditors[i].GetValue();
            Atributos[2] = _this.ExtraFieldEditors[i].CMDBCIDEFINEEXTRAFIELDS.IDCMDBDBDATATYPES
            parametros[i] = Atributos;
        }

        var IdCI = [];
        IdCI[0] = "IDCMDBCI";
        IdCI[1] = _this.txt_IDCMDB.Text;
        IdCI[2] = SysCfg.DB.Properties.TDataType.Int32;
        parametros.push(IdCI);

        var IdEF = [];
        IdEF[0] = "IDCMDB_EF" + _this.CMDBCIDEFINEEXTRATABLE.EXTRATABLE_NAMETABLE;
        IdEF[1] = _this.txt_IDCMDB_EF.Text;
        IdEF[2] = SysCfg.DB.Properties.TDataType.Int32;
        parametros.push(IdEF)

        _data = JSON.stringify({
            IDCMDBCIDEFINEEXTRATABLE: _this.CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE,
            arrayString: parametros,
            stateForm: UsrCfg.CMDB.Properties.StateFormulario.Delete,
            IDCMDBCI: _this.IDCMDBCI
        });

        $.ajax({
            type: "POST",
            url: SysCfg.App.Properties.xRaiz + "CMDB/CIEditor/CIEditor.svc/GrabarExtraTable",
            contentType: "application/json; charset=utf-8",
            data: _data,
            async: false,
            success: function (response) {
                _this.CleanControls();
                _this.RefreshGrid();
                alert("Se elimino correctamente");
            },
            error: function (response) {
                alert(response);
            }
        });
    }

    this.btnCancelar = new TVclButton(Contbuttons.Column[0].This, ID);
    this.btnCancelar.Text = "Cancel";
    this.btnCancelar.onClick = function () {
        _this.CleanControls();
        _this.EnabledControls(false);
        _this.StateForm = UsrCfg.CMDB.Properties.StateFormulario.Cancel;
        _this.EnabledButton(true, false, false, false);
    }

    this.GetExtraFieldEditor = function (NameExtraField) {
        for (var i = 0; i < this.ExtraFieldEditors.length; i++) {
            if (this.ExtraFieldEditors[i].CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_NAME == NameExtraField)
                return this.ExtraFieldEditors[i];
        }
        return null;
    }

    this.SetExtraField = function () {
        if (this.Grilla.FocusedRowHandle != null)
            this.StateForm = UsrCfg.CMDB.Properties.StateFormulario.Update;
        else
            this.StateForm = UsrCfg.CMDB.Properties.StateFormulario.New;

        this.txt_IDCMDB_EF.Text = this.Grilla.FocusedRowHandle.GetCellValue("IDCMDB_EF" + this.CMDBCIDEFINEEXTRATABLE.EXTRATABLE_NAMETABLE);
        this.txt_IDCMDB.Text = this.Grilla.FocusedRowHandle.GetCellValue("IDCMDBCI");

        for (var i = 0; i < this.CMDBCIDEFINEEXTRATABLE.CMDBCIDEFINEEXTRAFIELDSList.length; i++) {
            //setear value de extrafieldeditor y hacer funcion para traer el extrafield por nombre
            var _ExtraFieldEditor = this.GetExtraFieldEditor(this.CMDBCIDEFINEEXTRATABLE.CMDBCIDEFINEEXTRAFIELDSList[i].EXTRAFIELDS_NAME);
            _ExtraFieldEditor.SetValue(this.Grilla.FocusedRowHandle.GetCellValue(this.CMDBCIDEFINEEXTRATABLE.CMDBCIDEFINEEXTRAFIELDSList[i].EXTRAFIELDS_NAME));
        }

    }

    this.EnabledControls = function (value) {
        for (var i = 0; i < this.ExtraFieldEditors.length; i++) {
            this.ExtraFieldEditors[i].Enabled = value;
        }
    }

    this.CleanControls = function () {
        for (var i = 0; i < this.ExtraFieldEditors.length; i++) {
            this.ExtraFieldEditors[i].CleanValue()
        }
    }

    this.EnabledButton = function (_New, _Save, _Delete, _Cancel) {
        this.btnNuevo.Enabled = _New;
        this.btnGuardar.Enabled = _Save;
        this.btnEliminar.Enabled = _Delete;
        this.btnCancelar.Enabled = _Cancel;
    }

    if (this.IsADN) { this.pnGrilla.Row.Visible = true; this.btnNuevo.Visible = true; this.btnCancelar.Visible = true; this.EnabledControls(false); this.EnabledButton(true, false, false, false); }
    else { this.pnGrilla.Row.Visible = false; this.btnNuevo.Visible = false; this.btnCancelar.Visible = false; this.EnabledControls(true); this.EnabledButton(false, true, true, false); }

    this.RefreshGrid();
}
//Fin Estructura del ExtraTableEditor

//Inicio Estructura del ExtraFieldEditor
ItHelpCenter.CIEditor.TExtraFieldEditor = function (inObjectHTML, ID, inCallBack, inIDCMDBCI, inCMDBCIDEFINEEXTRAFIELDS) {
    this.This;
    this.IDCMDBCI = inIDCMDBCI;
    this.CMDBCIDEFINEEXTRAFIELDS = inCMDBCIDEFINEEXTRAFIELDS;
    this.Label;
    this.Control;
    this.Contenedor = new TVclStackPanel("", ID, 2, [[12, 12], [5, 7], [5, 7], [5, 7], [5, 7]]);

    this.Label = new TVcllabel(this.Contenedor.Column[0].This, "", TlabelType.H0);
    this.Label.Text = this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_NAME;

    this.cargarLookUp = function (cbLookUp) {
        var data = {
            'IDCMDBCIDEFINEEXTRATABLE': this.CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRATABLE,
            'IDCMDBCIDEFINEEXTRAFIELDS': this.CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS
        };
        $.ajax({
            type: "POST",
            url: SysCfg.App.Properties.xRaiz + 'CMDB/CIEditor/CIEditor.svc/GetComboExtraField',
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (response) {
                for (var i = 0; i < response.length; i++) {
                    var cbItem = new TVclComboBoxItem();
                    cbItem.Value = response[i].ValueMember;
                    cbItem.Text = response[i].DisplayMember;
                    cbItem.Tag = response[i];
                    cbLookUp.AddItem(cbItem);
                }
                //if (response.length > 0)
                //    cbLookUp.selectedIndex = 0;
            },
            error: function (response) {
                alert("no se puede acceder a [GetComboExtraField]");
            }
        });
    }

    this.CargarLookUpOption = function (rbLookUpOption) {
        var data = {
            'IDCMDBCIDEFINEEXTRATABLE': this.CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRATABLE,
            'IDCMDBCIDEFINEEXTRAFIELDS': this.CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS
        };


        var textito;
        $.ajax({
            type: "POST",
            url: SysCfg.App.Properties.xRaiz + 'CMDB/CIEditor/CIEditor.svc/GetComboExtraField',
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (response) {
                for (var i = 0; i < response.length; i++) {
                    var VclInputRadioButtonItem = new TVclInputRadioButtonItem();
                    VclInputRadioButtonItem.Id = "rbItem" + response[i].ValueMember;
                    VclInputRadioButtonItem.Text = response[i].DisplayMember
                    VclInputRadioButtonItem.Value = response[i].ValueMember
                    rbLookUpOption.AddItem(VclInputRadioButtonItem);
                }
            },
            error: function (response) {
                alert("no se pude acceder a [GetComboExtraField]");
            }
        });

        return textito;
    }

    this.CleanValue = function () {
        //switch (this.CMDBCIDEFINEEXTRAFIELDS.IDCMDBDBDATATYPES) {
        switch (SysCfg.DB.Properties.TDataType.GetEnum(this.CMDBCIDEFINEEXTRAFIELDS.IDCMDBDBDATATYPES)) {
            case SysCfg.DB.Properties.TDataType.String: // SysCfg.DB.Properties.TDataType.String   
                switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                        //LIMPIAR UN TEXTBOX
                        this.Control.Text = "";
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.TextPassword:
                        //LIMPIAR UN TEXTBOX DE TIPO PASSWORD   
                        this.Control.Text = "";
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.LookUp:
                        //LIMPIAR UN COMBOBOX 
                        this.Control.selectedIndex = 0;
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.LookUpOption:
                        //LIMPIAR UN RADIO BUTTON
                        break;
                }
                break;
            case SysCfg.DB.Properties.TDataType.Text: //SysCfg.DB.Properties.TDataType.Text  
                switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                        //LIMPIAR UN MEMO TEXT
                        this.Control.Text = "";
                        break;
                }
                break;
            case SysCfg.DB.Properties.TDataType.Int32: // SysCfg.DB.Properties.TDataType.Int32:   
                switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                        //LIMPIAR UN NUMERICUPDOWN
                        this.Control.Text = "";
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.LookUp:
                        //LIMPIAR UN COMBOBOX 
                        this.Control.selectedIndex = 0;
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.LookUpOption:
                        //LIMPIAR UN RADIO BUTTON
                        break;
                }
                break;
            case SysCfg.DB.Properties.TDataType.Double: //SysCfg.DB.Properties.TDataType.Double:   
                switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                        //LIMPIAR UN NUMERICUPDOWN                    
                        this.Control.Text = "0";
                        break;
                }
                break;
            case SysCfg.DB.Properties.TDataType.Boolean: //SysCfg.DB.Properties.TDataType.Boolean:     
                switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                        //LIMPIAR UN CHECKBOX
                        this.Control.Checked = true;
                        break;
                }
                break;
            case SysCfg.DB.Properties.TDataType.DateTime: //SysCfg.DB.Properties.TDataType.DateTime: 
                switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                        //LIMPIAR UN DATATIME PICKER
                        this.Control.Text = "";
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.Date:
                        //LIMPIAR UN DATE PICKER 
                        this.Control.Text = "";
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.Time:
                        //LIMPIAR UN TIME PICKER
                        this.Control.Text = "";
                        break;
                }
                break
            default:
                break;
        }
    }

    this.GetValue = function () {
        var Valor;
        //switch (this.CMDBCIDEFINEEXTRAFIELDS.IDCMDBDBDATATYPES) {
        switch (SysCfg.DB.Properties.TDataType.GetEnum(this.CMDBCIDEFINEEXTRAFIELDS.IDCMDBDBDATATYPES)) {
            case SysCfg.DB.Properties.TDataType.String: // SysCfg.DB.Properties.TDataType.String   
                switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                        //LIMPIAR UN TEXTBOX
                        Valor = this.Control.Text;
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.TextPassword:
                        //LIMPIAR UN TEXTBOX DE TIPO PASSWORD   
                        Valor = this.Control.Text;
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.LookUp:
                        //LIMPIAR UN COMBOBOX 
                        Valor = this.Control.Value;
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.LookUpOption:
                        //LIMPIAR UN RADIO BUTTON
                        Valor = this.Control.Value;
                        break;
                }
                break;
            case SysCfg.DB.Properties.TDataType.Text: //SysCfg.DB.Properties.TDataType.Text  
                switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                        //LIMPIAR UN MEMO TEXT
                        Valor = this.Control.Text;
                        break;
                }
                break;
            case SysCfg.DB.Properties.TDataType.Int32: // SysCfg.DB.Properties.TDataType.Int32:   
                switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                        //LIMPIAR UN NUMERICUPDOWN
                        Valor = parseInt(this.Control.Text);
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.LookUp:
                        //LIMPIAR UN COMBOBOX 
                        Valor = this.Control.Value;
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.LookUpOption:
                        //LIMPIAR UN RADIO BUTTON
                        Valor = this.Control.Value;
                        break;
                }
                break;
            case SysCfg.DB.Properties.TDataType.Double: //SysCfg.DB.Properties.TDataType.Double:   
                switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                        //LIMPIAR UN NUMERICUPDOWN                    
                        Valor = parseFloat(this.Control.Text);
                        break;
                }
                break;
            case SysCfg.DB.Properties.TDataType.Boolean: //SysCfg.DB.Properties.TDataType.Boolean:     
                switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                        //LIMPIAR UN CHECKBOX
                        Valor = this.Control.Checked;
                        break;
                }
                break;
            case SysCfg.DB.Properties.TDataType.DateTime: //SysCfg.DB.Properties.TDataType.DateTime: 
                switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                        //LIMPIAR UN DATATIME PICKER
                        Valor = this.Control.Text;
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.Date:
                        //LIMPIAR UN DATE PICKER 
                        Valor = this.Control.Text;
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.Time:
                        //LIMPIAR UN TIME PICKER
                        Valor = this.Control.Text;
                        break;
                }
                break
            default:
                break;
        }
        return Valor;
    }

    this.SetValue = function (Valor) {

        //switch (this.CMDBCIDEFINEEXTRAFIELDS.IDCMDBDBDATATYPES) {
        var _type = SysCfg.DB.Properties.TDataType.GetEnum(this.CMDBCIDEFINEEXTRAFIELDS.IDCMDBDBDATATYPES);
        switch (_type)  {
            case SysCfg.DB.Properties.TDataType.String: // SysCfg.DB.Properties.TDataType.String   
                switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                        //LIMPIAR UN TEXTBOX
                        this.Control.Text = Valor;
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.TextPassword:
                        //LIMPIAR UN TEXTBOX DE TIPO PASSWORD   
                        this.Control.Text = Valor;
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.LookUp:
                        //LIMPIAR UN COMBOBOX 
                        this.Control.Value = Valor;
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.LookUpOption:
                        this.Control.Value = Valor;
                        //LIMPIAR UN RADIO BUTTON
                        break;
                }
                break;
            case SysCfg.DB.Properties.TDataType.Text: //SysCfg.DB.Properties.TDataType.Text  
                switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                        //LIMPIAR UN MEMO TEXT
                        this.Control.Text = Valor;
                        break;
                }
                break;
            case SysCfg.DB.Properties.TDataType.Int32: // SysCfg.DB.Properties.TDataType.Int32:   
                switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                        //LIMPIAR UN NUMERICUPDOWN
                        this.Control.Text = Valor;
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.LookUp:
                        //LIMPIAR UN COMBOBOX 
                        this.Control.Value = Valor;
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.LookUpOption:
                        this.Control.Value = Valor;
                        //LIMPIAR UN RADIO BUTTON
                        break;
                }
                break;
            case SysCfg.DB.Properties.TDataType.Double: //SysCfg.DB.Properties.TDataType.Double:   
                switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                        //LIMPIAR UN NUMERICUPDOWN                    
                        this.Control.Text = Valor;
                        break;
                }
                break;
            case SysCfg.DB.Properties.TDataType.Boolean: //SysCfg.DB.Properties.TDataType.Boolean:     
                switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                        //LIMPIAR UN CHECKBOX
                        this.Control.Checked = Valor;
                        break;
                }
                break;
            case SysCfg.DB.Properties.TDataType.DateTime: //SysCfg.DB.Properties.TDataType.DateTime: 
                switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                        //LIMPIAR UN DATATIME PICKER
                        this.Control.Text = Valor;
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.Date:
                        //LIMPIAR UN DATE PICKER 
                        this.Control.Text = Valor;
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.Time:
                        //LIMPIAR UN TIME PICKER
                        this.Control.Text = Valor;
                        break;
                }
                break
            default:
                break;
        }
    }


    switch (SysCfg.DB.Properties.TDataType.GetEnum(this.CMDBCIDEFINEEXTRAFIELDS.IDCMDBDBDATATYPES)) {
        case SysCfg.DB.Properties.TDataType.String: // SysCfg.DB.Properties.TDataType.String   
            switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                    //CREAR UN TEXTBOX
                    this.Control = new TVclTextBox(this.Contenedor.Column[1].This, "");
                    break;
                case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.TextPassword:
                    //CREAR UN TEXTBOX DE TIPO PASSWORD
                    this.Control = new TVclTextBox(this.Contenedor.Column[1].This, "");
                    this.Control.Type = TImputtype.password;
                    break;
                case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.LookUp:
                    //CREAR UN COMBOBOX 
                    this.Control = new TVclComboBox2(this.Contenedor.Column[1].This, "");
                    this.cargarLookUp(this.Control);
                    break;
                case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.LookUpOption:
                    //CREAR UN RADIO BUTTON
                    this.Control = new TVclInputRadioButton(this.Contenedor.Column[1].This, "");
                    this.CargarLookUpOption(this.Control);
                    break;
            }
            break;
        case SysCfg.DB.Properties.TDataType.Text: //SysCfg.DB.Properties.TDataType.Text  
            switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                    //CREAR UN MEMO TEXT
                    this.Control = new TVclMemo(this.Contenedor.Column[1].This, "");
                    break;
            }
            break;
        case SysCfg.DB.Properties.TDataType.Int32: // SysCfg.DB.Properties.TDataType.Int32:   
            switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                    //CREAR UN NUMERICUPDOWN
                    this.Control = new TVclTextBox(this.Contenedor.Column[1].This, "");
                    this.Control.Type = TImputtype.number;
                    break;
                case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.LookUp:
                    //CREAR UN COMBOBOX 
                    this.Control = new TVclComboBox2(this.Contenedor.Column[1].This, "");
                    this.cargarLookUp(this.Control);
                    break;
                case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.LookUpOption:
                    //CREAR UN RADIO BUTTON
                    this.Control = new TVclInputRadioButton(this.Contenedor.Column[1].This, "");
                    this.CargarLookUpOption(this.Control);
                    break;
            }
            break;
        case SysCfg.DB.Properties.TDataType.Double: //SysCfg.DB.Properties.TDataType.Double:   
            switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                    //CREAR UN NUMERICUPDOWN                    
                    this.Control = new TVclTextBox(this.Contenedor.Column[1].This, "");
                    this.Control.Type = TImputtype.number;
                    break;
            }
            break;
        case SysCfg.DB.Properties.TDataType.Boolean: //SysCfg.DB.Properties.TDataType.Boolean:     
            switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                    //CREAR UN CHECKBOX
                    this.Control = new TVclInputcheckbox(this.Contenedor.Column[1].This, "");
                    break;
            }
            break;
        case SysCfg.DB.Properties.TDataType.DateTime: //SysCfg.DB.Properties.TDataType.DateTime: 
            switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                    //CREAR UN DATATIME PICKER
                    this.Control = new TVclTextBox(this.Contenedor.Column[1].This, "");
                    $(this.Control.This).datetimepicker({
                        timeFormat: "hh:mm tt"
                    });
                    break;
                case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.Date:
                    //CREAR UN DATE PICKER 
                    this.Control = new TVclTextBox(this.Contenedor.Column[1].This, "");
                    $(this.Control.This).datetimepicker({
                        showButtonPanel: true
                    });
                    break;
                case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.Time:
                    //CREAR UN TIME PICKER
                    this.Control = new TVclTextBox(this.Contenedor.Column[1].This, "");
                    $(this.Control.This).timepicker({
                        timeFormat: "hh:mm tt"
                    });
                    break;
            }
            break
        default:
            break;
    }

    Object.defineProperty(this, 'Enabled', {
        get: function () {
            return this.Control.Enabled;
        },
        set: function (_Value) {
            this.Control.Enabled = _Value;
        }
    });
}
//Fin Estructura del ExtraFieldEditor

// Inicio Estructura del Search del CI Editor
ItHelpCenter.CIEditor.TCISearchCIEditor = function (inObjectHTML, ID, inCallBack) {
    var Buscador = new TVclStackPanel(inObjectHTML, ID, 1, [[12], [12], [12], [12], [12]]);
    var DivLabel = new TVclStackPanel(Buscador.Column[0].This, "DivlblShow_" + ID, 1, [[12], [12], [12], [12], [12]]);
    var LabelShow = new Vcllabel(DivLabel.Column[0].This, "lblShow_" + ID, TVCLType.BS, TlabelType.H0, "Show: ");

    var DivComboModo = new TVclStackPanel(Buscador.Column[0].This, "DivcbModo_" + ID, 1, [[12], [12], [12], [12], [12]]);
    var ComboBoxModo = new TVclComboBox2(DivComboModo.Column[0].This, 'cbModo_' + ID);
    ComboBoxModo.VCLType = TVCLType.BS;

    var cbItemShowAll = new TVclComboBoxItem();
    cbItemShowAll.Value = 0;
    cbItemShowAll.Text = "Show All";
    cbItemShowAll.Tag = "Show All";
    ComboBoxModo.AddItem(cbItemShowAll);

    var cbItemSearch = new TVclComboBoxItem();
    cbItemSearch.Value = 1;
    cbItemSearch.Text = "Search";
    cbItemSearch.Tag = "Search";
    ComboBoxModo.AddItem(cbItemSearch);
    ComboBoxModo.selectedIndex = 1;

    var DivlblCol = new TVclStackPanel(Buscador.Column[0].This, "DivlblCol_" + ID, 1, [[12], [12], [12], [12], [12]]);
    var LabelCol = new Vcllabel(DivlblCol.Column[0].This, "lblCol_" + ID, TVCLType.BS, TlabelType.H0, "Select Column to Search: ");

    var DivComboCol = new TVclStackPanel(Buscador.Column[0].This, "DivcbColumnas_" + ID, 1, [[12], [12], [12], [12], [12]]);
    var ComboBoxCol = new TVclComboBox2(DivComboCol.Column[0].This, 'cbColumnas_' + ID);
    ComboBoxCol.VCLType = TVCLType.BS;

    var cbItemAll = new TVclComboBoxItem();
    cbItemAll.Value = "(ALL)";
    cbItemAll.Text = "(ALL)";
    cbItemAll.Tag = "(ALL)";
    ComboBoxCol.AddItem(cbItemAll);

    var cbItemSerialNumber = new TVclComboBoxItem();
    cbItemSerialNumber.Value = "CI_SERIALNUMBER";
    cbItemSerialNumber.Text = "CI_SERIALNUMBER";
    cbItemSerialNumber.Tag = "CI_SERIALNUMBER";
    ComboBoxCol.AddItem(cbItemSerialNumber);

    var cbItemGenericName = new TVclComboBoxItem();
    cbItemGenericName.Value = "CI_GENERICNAME";
    cbItemGenericName.Text = "CI_GENERICNAME";
    cbItemGenericName.Tag = "CI_GENERICNAME";
    ComboBoxCol.AddItem(cbItemGenericName);

    var cbItemDefineName = new TVclComboBoxItem();
    cbItemDefineName.Value = "CIDEFINE_NAME";
    cbItemDefineName.Text = "CIDEFINE_NAME";
    cbItemDefineName.Tag = "CIDEFINE_NAME";
    ComboBoxCol.AddItem(cbItemDefineName);
    ComboBoxCol.selectedIndex = 0;

    var DivlblSearch = new TVclStackPanel(Buscador.Column[0].This, "DivlblSearch_" + ID, 1, [[12], [12], [12], [12], [12]]);
    var LabelSearch = new Vcllabel(DivlblSearch.Column[0].This, "lblsearch_" + ID, TVCLType.BS, TlabelType.H0, "Select Column to Search: ");

    var DivTxtSearch = new TVclStackPanel(Buscador.Column[0].This, "DivTxtSearch_" + ID, 1, [[12], [12], [12], [12], [12]]);
    var TxtSearch = new TVclTextBox(DivTxtSearch.Column[0].This, "TxtSearch_" + ID);
    TxtSearch.VCLType = TVCLType.BS;

    var DivGrid = new TVclStackPanel(inObjectHTML, "DivGrid_" + ID, 1, [[12], [12], [12], [12], [12]]);

    ComboBoxModo.onChange = function () {
        var VclComboBoxItem = ComboBoxModo.Options[ComboBoxModo.This.selectedIndex];
        if (VclComboBoxItem.Value == 0) {
            DivlblCol.Visible = false;
            DivComboCol.Visible = false;
            DivlblSearch.Visible = false;
            DivTxtSearch.Visible = false;
            //me quede aki viendo para que cargue la grilla....
            var CIGridSearch = new ItHelpCenter.CIEditor.TCIGridSearch(DivGrid.Column[0].This, ID, inCallBack, '', '', true);
        }
        else {
            DivlblCol.Visible = true;
            DivComboCol.Visible = true;
            DivlblSearch.Visible = true;
            DivTxtSearch.Visible = true;
        }
        //alert("Value =  " + VclComboBoxItem.Value + ", Text = " + VclComboBoxItem.Text + ", Tag = " + VclComboBoxItem.Tag);
    }

    TxtSearch.onKeyUp = function () {
        if (TxtSearch.Text.length >= 3) {
            var CIGridSearch = new ItHelpCenter.CIEditor.TCIGridSearch(DivGrid.Column[0].This, ID, inCallBack, ComboBoxCol.Text, TxtSearch.Text, false);
        }
    }

}

ItHelpCenter.CIEditor.TCIGridSearch = function (inObjectHTML, ID, inCallBack, Paramcolumna, ParamValue, showAll) {
    inObjectHTML.innerHTML = "";

    var ctrGrid = new TGrid(inObjectHTML, ID, inCallBack)

    var Col1 = new TColumn();
    Col1.Name = "IDCMDBCI";
    Col1.Caption = "IDCMDBCI";
    Col1.Index = 0;
    ctrGrid.AddColumn(Col1);

    var Col2 = new TColumn();
    Col2.Name = "CI_SERIALNUMBER";
    Col2.Caption = "CI_SERIALNUMBER";
    Col2.Index = 1;
    ctrGrid.AddColumn(Col2);

    var Col3 = new TColumn();
    Col3.Name = "CI_GENERICNAME";
    Col3.Caption = "CI_GENERICNAME";
    Col3.Index = 2;
    ctrGrid.AddColumn(Col3);

    var Col4 = new TColumn();
    Col4.Name = "CIDEFINE_NAME";
    Col4.Caption = "CIDEFINE_NAME";
    Col4.Index = 3;
    ctrGrid.AddColumn(Col4);


    $.ajax({
        type: "POST",
        url: SysCfg.App.Properties.xRaiz + 'CMDB/CIEditor/CIEditor.svc/GetCIS',
        data: JSON.stringify({ Paramcolumna: Paramcolumna, ParamValue: ParamValue, ShowAll: showAll }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (response) {
            for (var i = 0; i < response.length; i++) {

                this.NewRow = new TRow();

                var Cell1 = new TCell();
                Cell1.Value = response[i].IDCMDBCI
                Cell1.IndexColumn = 0;
                Cell1.IndexRow = i;
                this.NewRow.AddCell(Cell1);

                var Cell2 = new TCell();
                Cell2.Value = response[i].CI_SERIALNUMBER
                Cell2.IndexColumn = 1;
                Cell2.IndexRow = i;
                this.NewRow.AddCell(Cell2);

                var Cell3 = new TCell();
                Cell3.Value = response[i].CI_GENERICNAME
                Cell3.IndexColumn = 2;
                Cell3.IndexRow = i;
                this.NewRow.AddCell(Cell3);

                var Cell4 = new TCell();
                Cell4.Value = response[i].CIDEFINE_NAME
                Cell4.IndexColumn = 3;
                Cell4.IndexRow = i;
                this.NewRow.AddCell(Cell4);

                var _IDCMDBCI = response[i].IDCMDBCI;
                this.NewRow.onclick = function () {
                    var _IDCMDBCI = ctrGrid.FocusedRowHandle.GetCellValue("IDCMDBCI");
                    inCallBack(_IDCMDBCI);
                }

                ctrGrid.AddRow(this.NewRow);

                //var nuevaFila = "<tr data-IdCI = '" + response[i].IDCMDBCI + "' data-CtrCIEditor = '" + NombreContenedor + "'>";
                //nuevaFila += "<td>" + response[i].IDCMDBCI + "</td>";
                //nuevaFila += "<td>" + response[i].CI_SERIALNUMBER + "</td>";
                //nuevaFila += "<td>" + response[i].CI_GENERICNAME + "</td>";
                //nuevaFila += "<td>" + response[i].CIDEFINE_NAME + "</td>";
                //nuevaFila += "</tr>";
                //$("#" + NombreContenedor + " .gridSearchCI tbody").append(nuevaFila);
            }
            //formatearTabla();
        },
        error: function (response) {
            alert(response.ErrorMessage);
        }
    });
}
// Fin Estructura del Search del CI Editor 

//Inicio Estructura del CI_relations
ItHelpCenter.CIEditor.TCIRelationsEditor = function (inObjectHTML, ID, inCallBack, inCMDBCIDEFINERELATIONTYPE, inIDCMDBCI, _IsChild) {
    this.This;
    this.btnNuevo;
    this.btnEliminar;
    this.Grilla;
    this.IsChild = _IsChild;
    this.IDCMDBCI = inIDCMDBCI;
    this.CMDBCIDEFINERELATIONTYPE = inCMDBCIDEFINERELATIONTYPE
    this.Urlrel;

    this.ContButtons = new TVclStackPanel(inObjectHTML, ID, 2, [[1, 11], [7, 5], [7, 5], [7, 5], [7, 5]]);

    this.Contenedor = new TVclStackPanel(inObjectHTML, ID, 1, [[12], [12], [12], [12], [12]]);

    this.ContModal = new TVclStackPanel(inObjectHTML, ID, 1, [[12], [12], [12], [12], [12]]);

    var _this = this;    

    this.CargarGrilla = function ()
    {
        if (this.IsChild)
            this.Urlrel = SysCfg.App.Properties.xRaiz + 'CMDB/CIEditor/CIEditor.svc/GetTyepeRelationsChildsList';
        else
            this.Urlrel = SysCfg.App.Properties.xRaiz + 'CMDB/CIEditor/CIEditor.svc/GetTyepeRelationsParentsList';

        

        var data = {
            'IDCMDBCIDEFINERELATIONTYPE': this.CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINERELATIONTYPE,
            'IDCMDBCI': inIDCMDBCI
        };
        $.ajax({
            type: "POST",
            url: this.Urlrel,
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (response) {
                $(_this.Contenedor.Column[0].This).html("");
                _this.Grilla = new TGrid(_this.Contenedor.Column[0].This, ID, inCallBack);

                var Col0 = new TColumn();
                Col0.Name = "FDMA01";
                Col0.Caption = "FDMA01";
                Col0.Index = 0;
                Col0.EnabledEditor = true;
                Col0.DataType = SysCfg.DB.Properties.TDataType.Boolean;
                Col0.ColumnStyle = UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None;
                _this.Grilla.AddColumn(Col0);

                var Col1 = new TColumn();
                Col1.Name = "CI_DATEPLANNED";
                Col1.Caption = "CI_DATEPLANNED";
                Col1.Index = 1;
                //Col1.EnabledEditor = true;
                //Col1.DataType = SysCfg.DB.Properties.TDataType.DateTime;
                //Col1.ColumnStyle = UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.Date;
                _this.Grilla.AddColumn(Col1);

                var Col2 = new TColumn();
                Col2.Name = "CI_DATEIN";
                Col2.Caption = "CI_DATEIN";
                Col2.Index = 2;
                _this.Grilla.AddColumn(Col2);

                var Col3 = new TColumn();
                Col3.Name = "CI_DATEOUT";
                Col3.Caption = "CI_DATEOUT";
                Col3.Index = 3;
                _this.Grilla.AddColumn(Col3);

                var Col4 = new TColumn();
                Col4.Name = "CI_GENERICNAME";
                Col4.Caption = "CI_GENERICNAME";
                Col4.Index = 4;
                _this.Grilla.AddColumn(Col4);

                var Col5 = new TColumn();
                Col5.Name = "CI_DESCRIPTION";
                Col5.Caption = "CI_DESCRIPTION";
                Col5.Index = 5;
                _this.Grilla.AddColumn(Col5);

                var Col6 = new TColumn();
                Col6.Name = "IDCMDBCI";
                Col6.Caption = "IDCMDBCI";
                Col6.Index = 6;
                _this.Grilla.AddColumn(Col6);

                var Col7 = new TColumn();
                Col7.Name = "IDCMDBBRAND";
                Col7.Caption = "IDCMDBBRAND";
                Col7.Index = 7;
                _this.Grilla.AddColumn(Col7);

                var Col8 = new TColumn();
                Col8.Name = "CI_SERIALNUMBER";
                Col8.Caption = "CI_SERIALNUMBER";
                Col8.Index = 8;
                _this.Grilla.AddColumn(Col8);

                var Col9 = new TColumn();
                Col9.Name = "CI_PURCHASEDORRENTED";
                Col9.Caption = "CI_PURCHASEDORRENTED";
                Col9.Index = 9;
                _this.Grilla.AddColumn(Col9);

                var Col10 = new TColumn();
                Col10.Name = "CIDEFINE_NAME";
                Col10.Caption = "CIDEFINE_NAME";
                Col10.Index = 10;
                _this.Grilla.AddColumn(Col10);

                var Col11 = new TColumn();
                Col11.Name = "CIDEFINERELATIONTYPE_NAME";
                Col11.Caption = "CIDEFINERELATIONTYPE_NAME";
                Col11.Index = 11;
                _this.Grilla.AddColumn(Col11);

                var Col12 = new TColumn();
                Col12.Name = "IDCMDBCIDEFINE_CHILD";
                Col12.Caption = "IDCMDBCIDEFINE_CHILD";
                Col12.Index = 12;
                _this.Grilla.AddColumn(Col12);

                var Col13 = new TColumn();
                Col13.Name = "IDCMDBCIDEFINE_PARENT";
                Col13.Caption = "IDCMDBCIDEFINE_PARENT";
                Col13.Index = 13;
                _this.Grilla.AddColumn(Col13);

                var Col14 = new TColumn();
                Col14.Name = "IDCMDBRELATIONTYPE";
                Col14.Caption = "IDCMDBRELATIONTYPE";
                Col14.Index = 14;
                _this.Grilla.AddColumn(Col14);

                var Col15 = new TColumn();
                Col15.Name = "IDCMDBCI_CHILD";
                Col15.Caption = "IDCMDBCI_CHILD";
                Col15.Index = 15;
                _this.Grilla.AddColumn(Col15);

                var Col16 = new TColumn();
                Col16.Name = "IDCMDBCI_PARENT";
                Col16.Caption = "IDCMDBCI_PARENT";
                Col16.Index = 16;
                _this.Grilla.AddColumn(Col16);

                var Col17 = new TColumn();
                Col17.Name = "IDCMDBCIRELATION";
                Col17.Caption = "IDCMDBCIRELATION";
                Col17.Index = 17;
                _this.Grilla.AddColumn(Col17);

                var Col18 = new TColumn();
                Col18.Name = "IDCMDBCIDEFINERELATIONTYPE";
                Col18.Caption = "IDCMDBCIDEFINERELATIONTYPE";
                Col18.Index = 18;
                _this.Grilla.AddColumn(Col18);

                for (var i = 0; i < response.length; i++) {
                    var NewRow = new TRow();

                    var Cell0 = new TCell();
                    Cell0.Value = false;
                    Cell0.IndexColumn = 0;
                    Cell0.IndexRow = i;
                    NewRow.AddCell(Cell0);

                    var Cell1 = new TCell();
                    Cell1.Value = formatJSONDateTime(response[i].CI_DATEPLANNED);
                    Cell1.IndexColumn = 1;
                    Cell1.IndexRow = i;
                    NewRow.AddCell(Cell1);

                    var Cell2 = new TCell();
                    Cell2.Value = formatJSONDateTime(response[i].CI_DATEIN);
                    Cell2.IndexColumn = 2;
                    Cell2.IndexRow = i;
                    NewRow.AddCell(Cell2);

                    var Cell3 = new TCell();
                    Cell3.Value = formatJSONDateTime(response[i].CI_DATEOUT);
                    Cell3.IndexColumn = 3;
                    Cell3.IndexRow = i;
                    NewRow.AddCell(Cell3);

                    var Cell4 = new TCell();
                    Cell4.Value = response[i].CI_GENERICNAME;
                    Cell4.IndexColumn = 4;
                    Cell4.IndexRow = i;
                    NewRow.AddCell(Cell4);

                    var Cell5 = new TCell();
                    Cell5.Value = response[i].CI_DESCRIPTION;
                    Cell5.IndexColumn = 5;
                    Cell5.IndexRow = i;
                    NewRow.AddCell(Cell5);

                    var Cell6 = new TCell();
                    Cell6.Value = response[i].IDCMDBCI;
                    Cell6.IndexColumn = 6;
                    Cell6.IndexRow = i;
                    NewRow.AddCell(Cell6);

                    var Cell7 = new TCell();
                    Cell7.Value = response[i].IDCMDBBRAND;
                    Cell7.IndexColumn = 7;
                    Cell7.IndexRow = i;
                    NewRow.AddCell(Cell7);

                    var Cell8 = new TCell();
                    Cell8.Value = response[i].CI_SERIALNUMBER;
                    Cell8.IndexColumn = 8;
                    Cell8.IndexRow = i;
                    NewRow.AddCell(Cell8);

                    var Cell9 = new TCell();
                    Cell9.Value = response[i].CI_PURCHASEDORRENTED;
                    Cell9.IndexColumn = 9;
                    Cell9.IndexRow = i;
                    NewRow.AddCell(Cell9);

                    var Cell10 = new TCell();
                    Cell10.Value = response[i].CIDEFINE_NAME;
                    Cell10.IndexColumn = 10;
                    Cell10.IndexRow = i;
                    NewRow.AddCell(Cell10);

                    var Cell11 = new TCell();
                    Cell11.Value = response[i].CIDEFINERELATIONTYPE_NAME;
                    Cell11.IndexColumn = 11;
                    Cell11.IndexRow = i;
                    NewRow.AddCell(Cell11);

                    var Cell12 = new TCell();
                    Cell12.Value = response[i].IDCMDBCIDEFINE_CHILD;
                    Cell12.IndexColumn = 12;
                    Cell12.IndexRow = i;
                    NewRow.AddCell(Cell12);

                    var Cell13 = new TCell();
                    Cell13.Value = response[i].IDCMDBCIDEFINE_PARENT;
                    Cell13.IndexColumn = 13;
                    Cell13.IndexRow = i;
                    NewRow.AddCell(Cell13);

                    var Cell14 = new TCell();
                    Cell14.Value = response[i].IDCMDBRELATIONTYPE;
                    Cell14.IndexColumn = 14;
                    Cell14.IndexRow = i;
                    NewRow.AddCell(Cell14);

                    var Cell15 = new TCell();
                    Cell15.Value = response[i].IDCMDBCI_CHILD;
                    Cell15.IndexColumn = 15;
                    Cell15.IndexRow = i;
                    NewRow.AddCell(Cell15);

                    var Cell16 = new TCell();
                    Cell16.Value = response[i].IDCMDBCI_PARENT;
                    Cell16.IndexColumn = 16;
                    Cell16.IndexRow = i;
                    NewRow.AddCell(Cell16);

                    var Cell17 = new TCell();
                    Cell17.Value = response[i].IDCMDBCIRELATION;
                    Cell17.IndexColumn = 17;
                    Cell17.IndexRow = i;
                    NewRow.AddCell(Cell17);

                    var Cell18 = new TCell();
                    Cell18.Value = response[i].IDCMDBCIDEFINERELATIONTYPE;
                    Cell18.IndexColumn = 18;
                    Cell18.IndexRow = i;
                    NewRow.AddCell(Cell18);

                    _this.Grilla.AddRow(NewRow);
                }
            },
            error: function (response) {
                alert(response.ErrorMessage);
            }
        });
    }

    this.CargarGrilla();

    this.btnNuevo = new TVclButton(this.ContButtons.Column[0].This, ID);
    this.btnNuevo.Text = "New";
    this.btnNuevo.onClick = function () {
        //cargar las nuevas relaciones
        var Modal = new TModal(_this.ContModal.Column[0].This, ID, inCallBack);

        var CIRelationsList = new ItHelpCenter.CIEditor.TCIRelationsList(Modal.ModalBody, ID, function (resultado) {
            Modal.Hide();
            _this.CargarGrilla();
        }, _this.CMDBCIDEFINERELATIONTYPE, _this.IDCMDBCI, _this.IsChild);

        Modal.Show();
    }

    this.btnEliminar = new TVclButton(this.ContButtons.Column[0].This, ID);
    this.btnEliminar.Text = "Delete";
    this.btnEliminar.onClick = function () {
        //delete las relaciones seleccionadas
        var _ListRel = [];
        var cont = 0;
        for (var i = 0; i < _this.Grilla.Rows.length; i++) {
            if (_this.Grilla.Rows[i].GetCellValue("FDMA01")) {
                var _IDCMDBRELATIONTYPE;
                if (_this.CMDBCIDEFINERELATIONTYPE.IDCMDBRELATIONTYPE != 0)
                    _IDCMDBRELATIONTYPE = _this.CMDBCIDEFINERELATIONTYPE.IDCMDBRELATIONTYPE;
                else
                    _IDCMDBRELATIONTYPE = parseInt(_this.Grilla.Rows[i].GetCellValue("IDCMDBRELATIONTYPE"));

                var _TCMDBCIRELATION = {
                    IDCMDBCIRELATION: parseInt(_this.Grilla.Rows[i].GetCellValue("IDCMDBCIRELATION")),
                    IDCMDBCI_CHILD: parseInt(_this.Grilla.Rows[i].GetCellValue("IDCMDBCI_CHILD")),
                    IDCMDBCI_PARENT: parseInt(_this.IDCMDBCI),
                    IDCMDBRELATIONTYPE: _IDCMDBRELATIONTYPE
                }

                _ListRel[cont] = _TCMDBCIRELATION;
                cont += 1;
            }
        }

        $.ajax({
            type: "POST",
            url: SysCfg.App.Properties.xRaiz + 'CMDB/CIEditor/CIEditor.svc/DelRelations',
            data: JSON.stringify({ ListRel: _ListRel, IDCMDBCI: _this.IDCMDBCI, ListChild: _this.IsChild }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (response) {
                _this.CargarGrilla();
            },
            error: function (response) {
                alert(response.ErrorMessage);
            }
        });
    }
}

ItHelpCenter.CIEditor.TCIRelationsList = function (inObjectHTML, ID, inCallBack, inCMDBCIDEFINERELATIONTYPE, inIDCMDBCI, IsChild) {
    this.this;
    this.Grilla;
    this.CMDBCIDEFINERELATIONTYPE = inCMDBCIDEFINERELATIONTYPE;
    this.IDCMDBCI = inIDCMDBCI;
    this.IsChild = IsChild;

    this.ContButtons = new TVclStackPanel(inObjectHTML, ID, 1, [[12], [12], [12], [12], [12]]);
    this.Contenedor = new TVclStackPanel(inObjectHTML, ID, 1, [[12], [12], [12], [12], [12]]);

    var _this = this;

    var data = {
        'IDCMDBCIDEFINERELATIONTYPE': inCMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINERELATIONTYPE,
        'IDCMDBCI': inIDCMDBCI,
        'ListChild': IsChild
    };
    $.ajax({
        type: "POST",
        url: SysCfg.App.Properties.xRaiz + 'CMDB/CIEditor/CIEditor.svc/GetRelationsXAdd',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (response) {

            _this.Grilla = new TGrid(_this.Contenedor.Column[0].This, ID, inCallBack);

            var Col0 = new TColumn();
            Col0.Name = "FDMA01";
            Col0.Caption = "FDMA01";
            Col0.Index = 0;
            Col0.EnabledEditor = true;
            Col0.DataType = SysCfg.DB.Properties.TDataType.Boolean;
            Col0.ColumnStyle = UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None;
            _this.Grilla.AddColumn(Col0);

            for (var i = 0; i < response.Columnas.length; i++) {
                var Col = new TColumn();
                Col.Name = response.Columnas[i].NombreColumna
                Col.Caption = response.Columnas[i].NombreColumna
                Col.Index = i + 1;
                _this.Grilla.AddColumn(Col);
            }

            for (var i = 0; i < response.Filas.length; i++) {
                var NewRow = new TRow();

                var Cell0 = new TCell();
                Cell0.Value = false;
                Cell0.IndexColumn = 0;
                Cell0.IndexRow = i;
                NewRow.AddCell(Cell0);

                for (var j = 0; j < response.Filas[i].Celdas.length; j++) {
                    var Cell = new TCell();
                    Cell.Value = response.Filas[i].Celdas[j].Valor;
                    Cell.IndexColumn = j + 1;
                    Cell.IndexRow = i;
                    NewRow.AddCell(Cell);
                }
                _this.Grilla.AddRow(NewRow);
            }
        },
        error: function (response) {
            alert(response.ErrorMessage);
        }
    });

    this.btnGrabar = new TVclButton(this.ContButtons.Column[0].This, ID);
    this.btnGrabar.Text = "Save";
    this.btnGrabar.onClick = function () {
        //grabar las nuevas relaciones
        var _ListRel = [];
        var cont = 0;
        for (var i = 0; i < _this.Grilla.Rows.length; i++) {
            if (_this.Grilla.Rows[i].GetCellValue("FDMA01"))
            {
                var _IDCMDBRELATIONTYPE;
                if (_this.CMDBCIDEFINERELATIONTYPE.IDCMDBRELATIONTYPE != 0)
                    _IDCMDBRELATIONTYPE = _this.CMDBCIDEFINERELATIONTYPE.IDCMDBRELATIONTYPE;
                else
                    _IDCMDBRELATIONTYPE = parseInt(_this.Grilla.Rows[i].GetCellValue("IDCMDBRELATIONTYPE"));

                var _TCMDBCIRELATION = {
                    IDCMDBCI_CHILD: parseInt(_this.Grilla.Rows[i].GetCellValue("IDCMDBCI")),
                    IDCMDBCI_PARENT: parseInt(_this.IDCMDBCI),
                    IDCMDBRELATIONTYPE: _IDCMDBRELATIONTYPE
                }
                _ListRel[cont] = _TCMDBCIRELATION;
                cont += 1;
            }
        }

        $.ajax({
            type: "POST",
            url: SysCfg.App.Properties.xRaiz + 'CMDB/CIEditor/CIEditor.svc/AddRelations',
            data: JSON.stringify({ ListRel: _ListRel, IDCMDBCI: _this.IDCMDBCI, ListChild: _this.IsChild }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (response) {
                inCallBack(response);
                //ejecutar el callback de el guardado;
            },
            error: function (response) {
                alert(response.ErrorMessage);
            }
        });

    }
}
//Fin Estructura del CI_relations



//crear un evento addrow //en FocusedRowHandle cuando doy addrow creo uno nuevo(new values) y lo asigno a FocusedRowHandle, si cancelas se elimina y recuperas el anterior en new values 
//luego crear un evento deleterow //si borrras se pasa el indice inferior oldvalues activo 
//crear un enveto updaterow //old vakues y neew vaules 
//crear un evento cancelar
//manejo de oldvalues y newvalues
//manejar indices 
