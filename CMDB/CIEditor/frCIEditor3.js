﻿
ItHelpCenter.CIEditor.TfrCiEditor = function (inObjectHtml, ID, inCallBack) {
    var _this = this;

    this.OnSelectCI = null;
    this.ObjectHtml = inObjectHtml;
    this.Open;
    this.CIProfiler = null;

    this.ID = "CIEditor_" + ID;

    var frCIEditor = new TVclStackPanel(inObjectHtml, this.ID, 2, [[12, 12], [12, 12], [5, 7], [5, 7], [5, 7]]);
    var CIEditorSearch = frCIEditor.Column[0].This; // busquedas de CIs
    var CIEditorMain = frCIEditor.Column[1].This; // Editor del CI seleccionado

    this.CIEditor = new ItHelpCenter.CIEditor.TCIEditor(CIEditorMain, ID);

    this.CISearchCIEditor = new ItHelpCenter.CIEditor.TCISearchCIEditor(CIEditorSearch, ID);
    this.CISearchCIEditor.OnSelectCI = function (sender, evenArgs) {
        _this.CIEditor.setCI(evenArgs);
    }

    this.CIEditor.OnAfterSave = function (sender, evenArgs) {
        var CI = evenArgs;
        if (_this.CIEditor.StateForm == UsrCfg.CMDB.Properties.StateFormulario.Nuevo) {
            var NewRow = new ItHelpCenter.CIEditor.TRow();

            var Cell1 = new ItHelpCenter.CIEditor.TCell();
            Cell1.Value = CI.IDCMDBCI
            Cell1.IndexColumn = 0;
            Cell1.IndexRow = i;
            NewRow.AddCell(Cell1);

            var Cell2 = new ItHelpCenter.CIEditor.TCell();
            Cell2.Value = CI.CI_SERIALNUMBER
            Cell2.IndexColumn = 1;
            Cell2.IndexRow = i;
            NewRow.AddCell(Cell2);

            var Cell3 = new ItHelpCenter.CIEditor.TCell();
            Cell3.Value = CI.CI_GENERICNAME
            Cell3.IndexColumn = 2;
            Cell3.IndexRow = i;
            NewRow.AddCell(Cell3);

            var Cell4 = new ItHelpCenter.CIEditor.TCell();
            Cell4.Value = CI.CIDEFINE_NAME
            Cell4.IndexColumn = 3;
            Cell4.IndexRow = i;
            NewRow.AddCell(Cell4);

            _this.CISearchCIEditor.CIGridSearch.ctrGrid.AddRow(NewRow);

        }
        else {
            if (_this.CISearchCIEditor.CIGridSearch.ctrGrid.FocusedRowHandle != null) {
                _this.CISearchCIEditor.CIGridSearch.ctrGrid.FocusedRowHandle.SetCellValue(CI.CI_SERIALNUMBER, "CI_SERIALNUMBER");
                _this.CISearchCIEditor.CIGridSearch.ctrGrid.FocusedRowHandle.SetCellValue(CI.CI_GENERICNAME, "CI_GENERICNAME");
                _this.CISearchCIEditor.CIGridSearch.ctrGrid.FocusedRowHandle.SetCellValue(CI.CIDEFINE_NAME, "CIDEFINE_NAME");
            }
        }

    }
}

ItHelpCenter.CIEditor.TCIEditor = function (inObjectHTML, ID) {
    var _this = this;
    this.ObjectHtml = inObjectHTML;
    this.ID = ID;
    this.Open;
    this.CIProfiler = null;
    this.StateForm = UsrCfg.CMDB.Properties.StateFormulario.New;
    this.OnAfterSave = null;
    this.OnCancel = null;

    _this.Mythis = "TCIEditor";

    UsrCfg.Traslate.GetLangText(_this.Mythis, "tabPageInfo", "CI Information");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "tabPageOutputRelation", "Output Relation");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "tabPageInputRelation", "Input Relation");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "tabPageBasic", "Basic");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lblSerial", "Serial: ");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lblName", "Name: ");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lblDefine", "CI Define: ");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lblAcquiredDate", "Acquired Date: ");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lblState", "State: ");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lblDisposeDate", "Dispose Date: ");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lblBrand", "Brand: ");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "lblOtherDetails", "Other Details: ");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnNuevo", "New");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnGuardar", "Save");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnEliminar", "Delete");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnCancelar", "Cancel");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnGraphic", "Graphic");
    this.InitializeComponent();

    this.CargarCIdefine = function () {
        var Param = new SysCfg.Stream.Properties.TParam();
        Param.Inicialize();
        Param.AddUnknown("BLPREFIX", "", SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        var Open = SysCfg.DB.SQL.Methods.Open("CMDB", "CMDBCIDEFINEQUERY", Param.ToBytes());
        if (Open.ResErr.NotError) {
            if (Open.DataSet.RecordCount > 0) {
                Open.DataSet.First();
                while (!(Open.DataSet.Eof)) {
                    var cbItem = new TVclComboBoxItem();
                    cbItem.Value = Open.DataSet.RecordSet.FieldName("IDCMDBCIDEFINE").asInt32();;
                    cbItem.Text = Open.DataSet.RecordSet.FieldName("CIDEFINE_NAME").asString();
                    cbItem.Tag = Open.DataSet.RecordSet;
                    this.cbDefine.AddItem(cbItem);

                    Open.DataSet.Next();
                }
                this.cbDefine.selectedIndex = 0;
            }
            else {
                Open.ResErr.NotError = false;
                Open.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
        else {
            SysCfg.App.Methods.ShowMessage(Open.ResErr.Mesaje);
        }
    }

    this.CargarState = function () {
        var Open = SysCfg.DB.SQL.Methods.Open("CMDB", "CMDBCISTATEQUERY", new Array(0));
        if (Open.ResErr.NotError) {
            if (Open.DataSet.RecordCount > 0) {
                Open.DataSet.First();
                while (!(Open.DataSet.Eof)) {
                    var cbItem = new TVclComboBoxItem();
                    cbItem.Value = Open.DataSet.RecordSet.FieldName("IDCMDBCISTATE").asInt32();;
                    cbItem.Text = Open.DataSet.RecordSet.FieldName("CISTATE_NAME").asString();
                    cbItem.Tag = Open.DataSet.RecordSet;
                    this.cbState.AddItem(cbItem);

                    Open.DataSet.Next();
                }
                this.cbState.selectedIndex = 0;
            }
            else {
                Open.ResErr.NotError = false;
                Open.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
        else {
            SysCfg.App.Methods.ShowMessage(Open.ResErr.Mesaje);
        }
    }

    this.CargarBrand = function () {
        var Open = SysCfg.DB.SQL.Methods.Open("CMDB", "CMDBBRANDQUERY_ACTIVE", new Array(0));
        if (Open.ResErr.NotError) {
            if (Open.DataSet.RecordCount > 0) {
                Open.DataSet.First();
                while (!(Open.DataSet.Eof)) {
                    var cbItem = new TVclComboBoxItem();
                    cbItem.Value = Open.DataSet.RecordSet.FieldName("IDCMDBBRAND").asInt32();;
                    cbItem.Text = Open.DataSet.RecordSet.FieldName("BRAND_NAME").asString();
                    cbItem.Tag = Open.DataSet.RecordSet;
                    this.cbBrand.AddItem(cbItem);

                    Open.DataSet.Next();
                }
                this.cbBrand.selectedIndex = 0;
            }
            else {
                Open.ResErr.NotError = false;
                Open.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
        else {
            SysCfg.App.Methods.ShowMessage(Open.ResErr.Mesaje);
        }
    }

    this.Inicialize = function (unCIProfiler) {
        this.CIProfiler = new UsrCfg.CMDB.TCIProfiler();
        this.CIProfiler.CIDEFINE_ListFill(this.CIProfiler.CIDEFINEList, this.CIProfiler.CIDEFINEEXTRATABLEList, this.CIProfiler.CIDEFINEEXTRAFIELDSList, UsrCfg.CMDB.Properties.TCMDBCIDEFINELEVEL._LevelNone, "", 0, "", UsrCfg.CMDB.Properties.TCMDBCIDEFINETYPE._GENERIC);
    }

    this.LoadExtraTables = function () {
        this.tabControlInfo.ClearTabPagesExcept("Basic");
        this.tabControlInfo.TabPages[0].Active = true;

        for (var i = 0; i < this.CIProfiler.SETCI.CMDBCIDEFINE.CMDBCIDEFINEEXTRATABLElist.length; i++) {
            var tabPageET = new TTabPage();
            tabPageET.Name = this.CIProfiler.SETCI.CMDBCIDEFINE.CMDBCIDEFINEEXTRATABLElist[i].EXTRATABLE_NAME;
            tabPageET.Caption = this.CIProfiler.SETCI.CMDBCIDEFINE.CMDBCIDEFINEEXTRATABLElist[i].EXTRATABLE_NAME;
            tabPageET.Tag = this.CIProfiler.SETCI.CMDBCIDEFINE.CMDBCIDEFINEEXTRATABLElist[i];

            var ExtraTableEditor = new ItHelpCenter.CIEditor.TExtraTableEditor(tabPageET.Page, "", this.CIProfiler, this.CIProfiler.SETCI.IDCMDBCI, this.CIProfiler.SETCI.CMDBCIDEFINE.CMDBCIDEFINEEXTRATABLElist[i])

            this.tabControlInfo.AddTabPages(tabPageET);
        }
    }

    this.LoadRelationsChilds = function () {
        this.tabControlOR.ClearTabPages();
        for (var i = 0; i < this.CIProfiler.CMDBCIDEFINERELATIONTYPEList.length; i++) {
            if (this.CIProfiler.CMDBCIDEFINERELATIONTYPEList[i].IDCMDBCIDEFINE_PARENT == this.CIProfiler.SETCI.CMDBCIDEFINE.IDCMDBCIDEFINE) {
                var tabPageET = new TTabPage();
                tabPageET.Name = "OR" + this.CIProfiler.CMDBCIDEFINERELATIONTYPEList[i].IDCMDBCIDEFINERELATIONTYPE;
                tabPageET.Caption = this.CIProfiler.CMDBCIDEFINERELATIONTYPEList[i].CIDEFINERELATIONTYPE_NAME;
                tabPageET.Tag = this.CIProfiler.CMDBCIDEFINERELATIONTYPEList[i];

                var CIRelationsEditor = new ItHelpCenter.CIEditor.TCIRelationsEditor(tabPageET.Page, ID, this.CIProfiler, this.CIProfiler.CMDBCIDEFINERELATIONTYPEList[i], this.CIProfiler.SETCI.IDCMDBCI, true)

                for (var j = 0; j < this.CIProfiler.SETCI.CIRELATIONListChild.length; j++) //(UsrCfg.CMDB.Properties.TCMDBCIRELATION item2 in CIProfiler.SETCI.CIRELATIONListParent.FindAll(s => s.IDCMDBCIDEFINERELATIONTYPE == item.IDCMDBCIDEFINERELATIONTYPE))
                {
                    if (this.CIProfiler.SETCI.CIRELATIONListChild[j].IDCMDBCIDEFINERELATIONTYPE == this.CIProfiler.CMDBCIDEFINERELATIONTYPEList[i].IDCMDBCIDEFINERELATIONTYPE) {
                        CIRelationsEditor.AddToGrid(this.CIProfiler.SETCI.CIRELATIONListChild[j]);
                    }
                }
                this.tabControlOR.AddTabPages(tabPageET);
            }
        }

        var oRT = new UsrCfg.CMDB.Properties.TCMDBCIDEFINERELATIONTYPE();
        oRT.IDCMDBRELATIONTYPE = 0;
        oRT.CIDEFINERELATIONTYPE_NAME = "OTHERS";

        var tabPageETO = new TTabPage();
        tabPageETO.Name = "OR" + oRT.IDCMDBCIDEFINERELATIONTYPE;
        tabPageETO.Caption = oRT.CIDEFINERELATIONTYPE_NAME;
        tabPageETO.Tag = oRT;

        var CIRelationsEditor = new ItHelpCenter.CIEditor.TCIRelationsEditor(tabPageETO.Page, ID, this.CIProfiler, oRT, this.CIProfiler.SETCI.IDCMDBCI, true)

        this.tabControlOR.AddTabPages(tabPageETO);

        if (this.CIProfiler.CMDBCIDEFINERELATIONTYPEList.length > 0)
            this.tabControlOR.TabPages[0].Active = true;
    }

    this.LoadRelationsParents = function () {
        this.tabControlIR.ClearTabPages();
        for (var i = 0; i < this.CIProfiler.CMDBCIDEFINERELATIONTYPEList.length; i++) {
            if (this.CIProfiler.CMDBCIDEFINERELATIONTYPEList[i].IDCMDBCIDEFINE_CHILD == this.CIProfiler.SETCI.CMDBCIDEFINE.IDCMDBCIDEFINE) {
                var tabPageET = new TTabPage();
                tabPageET.Name = "IR" + this.CIProfiler.CMDBCIDEFINERELATIONTYPEList[i].IDCMDBCIDEFINERELATIONTYPE;
                tabPageET.Caption = this.CIProfiler.CMDBCIDEFINERELATIONTYPEList[i].CIDEFINERELATIONTYPE_NAME;
                tabPageET.Tag = this.CIProfiler.CMDBCIDEFINERELATIONTYPEList[i];

                var CIRelationsEditor = new ItHelpCenter.CIEditor.TCIRelationsEditor(tabPageET.Page, ID, this.CIProfiler, this.CIProfiler.CMDBCIDEFINERELATIONTYPEList[i], this.CIProfiler.SETCI.IDCMDBCI, false)

                for (var j = 0; j < this.CIProfiler.SETCI.CIRELATIONListParent.length; j++) //(UsrCfg.CMDB.Properties.TCMDBCIRELATION item2 in CIProfiler.SETCI.CIRELATIONListParent.FindAll(s => s.IDCMDBCIDEFINERELATIONTYPE == item.IDCMDBCIDEFINERELATIONTYPE))
                {
                    if (this.CIProfiler.SETCI.CIRELATIONListParent[j].IDCMDBCIDEFINERELATIONTYPE == this.CIProfiler.CMDBCIDEFINERELATIONTYPEList[i].IDCMDBCIDEFINERELATIONTYPE) {
                        CIRelationsEditor.AddToGrid(this.CIProfiler.SETCI.CIRELATIONListParent[j]);
                    }
                }
                this.tabControlIR.AddTabPages(tabPageET);
            }
        }

        var oRT = new UsrCfg.CMDB.Properties.TCMDBCIDEFINERELATIONTYPE();
        oRT.IDCMDBRELATIONTYPE = 0;
        oRT.CIDEFINERELATIONTYPE_NAME = "OTHERS";

        var tabPageETO = new TTabPage();
        tabPageETO.Name = "IR" + oRT.IDCMDBCIDEFINERELATIONTYPE;
        tabPageETO.Caption = oRT.CIDEFINERELATIONTYPE_NAME;
        tabPageETO.Tag = oRT;

        var CIRelationsEditor = new ItHelpCenter.CIEditor.TCIRelationsEditor(tabPageETO.Page, ID, this.CIProfiler, oRT, this.CIProfiler.SETCI.IDCMDBCI, false)

        this.tabControlIR.AddTabPages(tabPageETO);

        if (this.CIProfiler.CMDBCIDEFINERELATIONTYPEList.length > 0)
            this.tabControlIR.TabPages[0].Active = true;
    }

    this.EnabledControls = function (value) {
        this.txtSerial.Enabled = value;
        this.txtName.Enabled = value;
        this.cbDefine.Enabled = value;
        this.txtAcquiredDate.Enabled = value;
        this.cbState.Enabled = value;
        this.txtDisposeDate.Enabled = value;
        this.cbBrand.Enabled = value;
        this.txtOtherDetails.Enabled = value;
    }

    this.CleanControls = function () {
        this.txtSerial.Text = "";
        this.txtName.Text = "";
        this.txtAcquiredDate.Text = "";
        this.txtDisposeDate.Text = "";
        this.txtOtherDetails.Text = "";
    }

    this.EnabledButton = function (_New, _Save, _Delete, _Cancel, _Graphic) {
        this.btnNuevo.Enabled = _New;
        this.btnGuardar.Enabled = _Save;
        this.btnEliminar.Enabled = _Delete;
        this.btnCancelar.Enabled = _Cancel;
        this.btnGraphic.Enabled = _Graphic;
    }

    this.CargarCIdefine();
    this.CargarState();
    this.CargarBrand();

    this.EnabledControls(false);
    this.EnabledButton(true, false, false, false, false, false);

    this.setCI = function (IDCMDBCI) {
        this.tabControlPrincipal.TabPages[0].Active = true;

        this.CIProfiler.GETCI(IDCMDBCI);

        var CI = this.CIProfiler.SETCI;
        if (CI.IDCMDBCI > 0) {
            this.txtSerial.Text = CI.CI_SERIALNUMBER;
            this.txtName.Text = CI.CI_GENERICNAME;
            this.cbDefine.Value = CI.IDCMDBCIDEFINE;
            this.txtAcquiredDate.Text = CI.CI_DATEIN;
            this.cbState.Value = CI.IDCMDBCISTATE.value;
            this.txtDisposeDate.Text = CI.CI_DATEOUT;
            this.cbBrand.Value = CI.CMDBBRAND.IDCMDBBRAND;
            this.txtOtherDetails.Text = CI.CI_DESCRIPTION;

            this.LoadExtraTables();
            this.LoadRelationsChilds();
            this.LoadRelationsParents();
            this.EnabledButton(false, true, true, true, true, true);
            this.EnabledControls(true);
            this.StateForm = UsrCfg.CMDB.Properties.StateFormulario.Update;
        }
    }


    this.btnNuevo.onClick = function () {
        _this.CleanControls();
        _this.tabControlInfo.ClearTabPagesExcept("Basic");
        _this.tabControlInfo.TabPages[0].Active = true;
        //falta eliminar tabs de relaciones output e input
        _this.tabControlOR.ClearTabPages();
        _this.tabControlIR.ClearTabPages();

        _this.EnabledControls(true);
        _this.EnabledButton(false, true, false, true);
        _this.StateForm = UsrCfg.CMDB.Properties.StateFormulario.New;
        //seleccionar el primer tab de inf basic
    }

    this.btnGuardar.onClick = function () {
        var _IDCMDBCI = _this.IDCMDBCI;
        var _CI_SERIALNUMBER = _this.txtSerial.Text;
        var _CI_GENERICNAME = _this.txtName.Text;
        var _CI_DESCRIPTION = _this.txtOtherDetails.Text;
        var _IDCMDBCIDEFINE = parseInt(_this.cbDefine.Value);
        var _IDCMDBCISTATE = UsrCfg.CMDB.Properties.TCMDBCISTATE.GetEnum(parseInt(_this.cbState.Value));
        var _IDCMDBBRAND = parseInt(_this.cbBrand.Value);

        var _CIDEFINE_NAME = _this.cbDefine.Text;



        if (_this.StateForm == UsrCfg.CMDB.Properties.StateFormulario.Nuevo) {
            _this.CIProfiler.CIADD(_CI_GENERICNAME, _CI_DESCRIPTION, _CI_SERIALNUMBER, _IDCMDBCIDEFINE, _IDCMDBBRAND);
        }
        else if (_this.StateForm == UsrCfg.CMDB.Properties.StateFormulario.Update) {
            _this.CIProfiler.CIUPD(_CI_GENERICNAME, _CI_DESCRIPTION, _CI_SERIALNUMBER, _IDCMDBBRAND, _IDCMDBCISTATE);
        }

        _this.CleanControls();
        _this.tabControlInfo.ClearTabPagesExcept("Basic");
        _this.tabControlInfo.TabPages[0].Active = true;
        //falta eliminar tabs de relaciones output e input
        _this.tabControlOR.ClearTabPages();
        _this.tabControlIR.ClearTabPages();

        _this.EnabledControls(false);
        _this.EnabledButton(true, false, false, false);
        alert('Se grabo correctamente.');
        if (_this.StateForm == UsrCfg.CMDB.Properties.StateFormulario.Update) {
            if (_this.OnAfterSave != null)
                _this.OnAfterSave(_this, _this.CIProfiler.SETCI);
        }
    }

    this.btnEliminar.onClick = function () {

    }

    this.btnCancelar.onClick = function () {
        _this.CleanControls();
        _this.tabControlInfo.ClearTabPagesExcept("Basic");
        _this.tabControlInfo.TabPages[0].Active = true;
        //falta eliminar tabs de relaciones output e input
        _this.tabControlOR.ClearTabPages();
        _this.tabControlIR.ClearTabPages();

        _this.EnabledControls(false);
        _this.EnabledButton(true, false, false, false);
        _this.StateForm = UsrCfg.CMDB.Properties.StateFormulario.New;

        if (_this.OnCancel != null)
            _this.OnCancel(_this);
    }

    this.btnGraphic.onClick = function () {

        var Modal = new TVclModal(document.body, null, "IDModalDetailGraphic02");
        Modal.Width = "85%";
        Modal.AddHeaderContent("Graphic CI");
        Modal.Body.This.id = "Modal_DetailGraphi02";

        var CIRelationGraphics = new ItHelpCenter.CMDB.CIEditor.frCIRelationGraphics(document.getElementsByTagName("BODY")[0], "1")        
        CIRelationGraphics.Initialize(_this.CIProfiler);
        $(Modal.Body.This).append(CIRelationGraphics.container.Row.This);
        Modal.ShowModal();
        
    }

    this.Inicialize();



}

ItHelpCenter.CIEditor.TCIEditor.prototype.InitializeComponent = function () {
    var _this = this;
    this.ContCIEditor = new TVclStackPanel(this.ObjectHtml, this.ID, 1, [[12], [12], [12], [12], [12]]);
    this.ContCIEditor.Row.This.style.marginRight = "10px";

    this.ContCIEditor.Column[0].This.classList.add("StackPanelContainer");
    this.ContCIEditor.Column[0].This.style.padding = "15px";

    this.ContButtons = new TVclStackPanel(this.ContCIEditor.Column[0].This, this.ID, 2, [[0, 12], [0, 12], [0, 12], [3, 9], [3, 9]]);



    //Inicio contruccion el tabs principal con las 3 pestañas
    this.tabControlPrincipal = new TTabControl(this.ContCIEditor.Column[0].This, this.ID, null);
    var tabPageInfo = new TTabPage();
    tabPageInfo.Name = "CIInformation";
    tabPageInfo.Caption = UsrCfg.Traslate.GetLangText(_this.Mythis, "tabPageInfo");
    tabPageInfo.Active = true;
    this.tabControlPrincipal.AddTabPages(tabPageInfo);

    var tabPageOutputRelation = new TTabPage();
    tabPageOutputRelation.Name = "OutputRelation";
    tabPageOutputRelation.Caption = UsrCfg.Traslate.GetLangText(_this.Mythis, "tabPageOutputRelation");
    this.tabControlPrincipal.AddTabPages(tabPageOutputRelation);

    var tabPageInputRelation = new TTabPage();
    tabPageInputRelation.Name = "InputRelation";
    tabPageInputRelation.Caption = UsrCfg.Traslate.GetLangText(_this.Mythis, "tabPageInputRelation");
    this.tabControlPrincipal.AddTabPages(tabPageInputRelation);
    //fin contruccion el tab principal con las 3 pestañas 

    this.tabControlOR = new TTabControl(tabPageOutputRelation.Page, this.ID, null);

    this.tabControlIR = new TTabControl(tabPageInputRelation.Page, this.ID, null);

    //Inicio contruccion tabs de informacion     
    this.tabControlInfo = new TTabControl(tabPageInfo.Page, this.ID, null);

    var tabPageBasic = new TTabPage();
    tabPageBasic.Name = "Basic";
    tabPageBasic.Caption = UsrCfg.Traslate.GetLangText(_this.Mythis, "tabPageBasic");
    tabPageBasic.Active = true;
    this.tabControlInfo.AddTabPages(tabPageBasic);

    var BISerial = new TVclStackPanel(tabPageBasic.Page, this.ID, 2, [[12, 12], [12, 12], [6, 6], [5, 7], [5, 7]]);
    BISerial.Row.This.style.margin = "5px";
    this.lblSerial = new TVcllabel(BISerial.Column[0].This, "txtSerial", TlabelType.H0);
    this.lblSerial.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblSerial");
    this.txtSerial = new TVclTextBox(BISerial.Column[1].This, "");
    this.txtSerial.VCLType = TVCLType.BS;

    var BIName = new TVclStackPanel(tabPageBasic.Page, this.ID, 2, [[12, 12], [12, 12], [6, 6], [5, 7], [5, 7]]);
    BIName.Row.This.style.margin = "5px";
    this.lblName = new TVcllabel(BIName.Column[0].This, "lblName", TlabelType.H0);
    this.lblName.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblName");
    this.txtName = new TVclTextBox(BIName.Column[1].This, "");
    this.txtName.VCLType = TVCLType.BS;



    var BIDefine = new TVclStackPanel(tabPageBasic.Page, this.ID, 2, [[12, 12], [12, 12], [6, 6], [5, 7], [5, 7]]);
    BIDefine.Row.This.style.margin = "5px";
    this.lblDefine = new TVcllabel(BIDefine.Column[0].This, "lblDefine", TlabelType.H0);
    this.lblDefine.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblDefine");
    this.cbDefine = new TVclComboBox2(BIDefine.Column[1].This, 'cbDefine');
    this.cbDefine.VCLType = TVCLType.BS;

    var BIAcquiredDate = new TVclStackPanel(tabPageBasic.Page, this.ID, 2, [[12, 12], [12, 12], [6, 6], [5, 7], [5, 7]]);
    BIAcquiredDate.Row.This.style.margin = "5px";
    this.lblAcquiredDate = new TVcllabel(BIAcquiredDate.Column[0].This, "lblAcquiredDate", TlabelType.H0);
    this.lblAcquiredDate.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblAcquiredDate");
    this.txtAcquiredDate = new TVclTextBox(BIAcquiredDate.Column[1].This, "");
    this.txtAcquiredDate.VCLType = TVCLType.BS;
    $(this.txtAcquiredDate.This).datetimepicker({ showButtonPanel: true });

    var BIState = new TVclStackPanel(tabPageBasic.Page, this.ID, 2, [[12, 12], [12, 12], [6, 6], [5, 7], [5, 7]]);
    BIState.Row.This.style.margin = "5px";
    this.lblState = new TVcllabel(BIState.Column[0].This, "lblState", TlabelType.H0);
    this.lblState.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblState");
    this.cbState = new TVclComboBox2(BIState.Column[1].This, 'cbState');
    this.cbState.VCLType = TVCLType.BS;

    var BIDisposeDate = new TVclStackPanel(tabPageBasic.Page, this.ID, 2, [[12, 12], [12, 12], [6, 6], [5, 7], [5, 7]]);
    BIDisposeDate.Row.This.style.margin = "5px";
    this.lblDisposeDate = new TVcllabel(BIDisposeDate.Column[0].This, "lblDisposeDate", TlabelType.H0);
    this.lblDisposeDate.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblDisposeDate");
    this.txtDisposeDate = new TVclTextBox(BIDisposeDate.Column[1].This, "");
    this.txtDisposeDate.VCLType = TVCLType.BS;
    $(this.txtDisposeDate.This).datetimepicker({ showButtonPanel: true });

    var BIBrand = new TVclStackPanel(tabPageBasic.Page, this.ID, 2, [[12, 12], [12, 12], [6, 6], [5, 7], [5, 7]]);
    BIBrand.Row.This.style.margin = "5px";
    this.lblBrand = new TVcllabel(BIBrand.Column[0].This, "lblBrand", TlabelType.H0);
    this.lblBrand.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblBrand");
    this.cbBrand = new TVclComboBox2(BIBrand.Column[1].This, 'cbBrand');
    this.cbBrand.VCLType = TVCLType.BS;

    var BIOtherDetails = new TVclStackPanel(tabPageBasic.Page, this.ID, 2, [[12, 12], [12, 12], [6, 6], [5, 7], [5, 7]]);
    BIOtherDetails.Row.This.style.margin = "5px";
    this.lblOtherDetails = new TVcllabel(BIOtherDetails.Column[0].This, "lblOtherDetails", TlabelType.H0);
    this.lblOtherDetails.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "lblOtherDetails");
    this.txtOtherDetails = new TVclMemo(BIOtherDetails.Column[1].This, "");
    this.txtOtherDetails.This.classList.add("form-control");
    this.txtOtherDetails.Rows = 5;
    this.txtOtherDetails.Cols = 50;

    this.btnNuevo = new TVclButton(this.ContButtons.Column[1].This, this.ID);
    this.btnNuevo.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnNuevo");
    this.btnNuevo.Src = "image/16/add.png";
    this.btnNuevo.VCLType = TVCLType.BS;
    this.btnNuevo.This.style.marginRight = "10px";

    this.btnGuardar = new TVclButton(this.ContButtons.Column[1].This, this.ID);
    this.btnGuardar.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnGuardar");
    this.btnGuardar.Src = "image/16/success.png";
    this.btnGuardar.VCLType = TVCLType.BS;
    this.btnGuardar.This.style.marginRight = "10px";

    this.btnEliminar = new TVclButton(this.ContButtons.Column[1].This, this.ID);
    this.btnEliminar.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnEliminar");
    this.btnEliminar.Src = "image/16/Delete.png";
    this.btnEliminar.VCLType = TVCLType.BS;
    this.btnEliminar.This.style.marginRight = "10px";

    this.btnCancelar = new TVclButton(this.ContButtons.Column[1].This, this.ID);
    this.btnCancelar.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnCancelar");
    this.btnCancelar.VCLType = TVCLType.BS;
    this.btnCancelar.Src = "image/16/Cancel.png";
    this.btnCancelar.This.style.marginRight = "10px";

    this.btnGraphic = new TVclButton(this.ContButtons.Column[1].This, this.ID);
    this.btnGraphic.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnGraphic");
    this.btnGraphic.VCLType = TVCLType.BS;
    this.btnGraphic.Src = "image/16/Question-type-drag-drop.png";
    this.btnGraphic.This.style.marginRight = "10px";
}

// Inicio Estructura del Search del CI Editor
ItHelpCenter.CIEditor.TCISearchCIEditor = function (inObjectHTML, ID, inCallBack) {
    this.CIGridSearch = null;
    this.OnSelectCI = null;
    this.Mythis = "TCISearchCIEditor";
    var _this = this;

    UsrCfg.Traslate.GetLangText(_this.Mythis, "LabelShow", "Show: ");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "cbItemShowAll", "Show All");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "cbItemSearch", "Search");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "LabelCol", "Select Column to Search: ");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "cbItemAll", "(ALL)");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "cbItemSerialNumber", "CI_SERIALNUMBER");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "cbItemGenericName", "CI_GENERICNAME");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "cbItemDefineName", "CIDEFINE_NAME");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "LabelSearch", "Select Column to Search: ");


    var Buscador = new TVclStackPanel(inObjectHTML, ID, 1, [[12], [12], [12], [12], [12]]);
    Buscador.Row.This.style.marginLeft = "10px";
    Buscador.Row.This.style.marginRight = "10px";
    Buscador.Row.This.style.marginBottom = "10px";

    Buscador.Column[0].This.classList.add("StackPanelContainer");

    var DivLabel = new TVclStackPanel(Buscador.Column[0].This, "DivlblShow_" + ID, 1, [[12], [12], [12], [12], [12]]);
    var LabelShow = new Vcllabel(DivLabel.Column[0].This, "lblShow_" + ID, TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "LabelShow"));

    var DivComboModo = new TVclStackPanel(Buscador.Column[0].This, "DivcbModo_" + ID, 1, [[12], [12], [12], [12], [12]]);
    var ComboBoxModo = new TVclComboBox2(DivComboModo.Column[0].This, 'cbModo_' + ID);
    ComboBoxModo.VCLType = TVCLType.BS;

    var cbItemShowAll = new TVclComboBoxItem();
    cbItemShowAll.Value = 0;
    cbItemShowAll.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "cbItemShowAll");
    cbItemShowAll.Tag = "Show All";
    ComboBoxModo.AddItem(cbItemShowAll);

    var cbItemSearch = new TVclComboBoxItem();
    cbItemSearch.Value = 1;
    cbItemSearch.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "cbItemSearch");
    cbItemSearch.Tag = "Search";
    ComboBoxModo.AddItem(cbItemSearch);
    ComboBoxModo.selectedIndex = 1;

    var DivlblCol = new TVclStackPanel(Buscador.Column[0].This, "DivlblCol_" + ID, 1, [[12], [12], [12], [12], [12]]);
    var LabelCol = new Vcllabel(DivlblCol.Column[0].This, "lblCol_" + ID, TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "LabelCol"));

    var DivComboCol = new TVclStackPanel(Buscador.Column[0].This, "DivcbColumnas_" + ID, 1, [[12], [12], [12], [12], [12]]);
    var ComboBoxCol = new TVclComboBox2(DivComboCol.Column[0].This, 'cbColumnas_' + ID);
    ComboBoxCol.VCLType = TVCLType.BS;


    var cbItemAll = new TVclComboBoxItem();
    cbItemAll.Value = "(ALL)";
    cbItemAll.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "cbItemAll");
    cbItemAll.Tag = "(ALL)";
    ComboBoxCol.AddItem(cbItemAll);

    var cbItemSerialNumber = new TVclComboBoxItem();
    cbItemSerialNumber.Value = "CI_SERIALNUMBER";
    cbItemSerialNumber.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "cbItemSerialNumber");
    cbItemSerialNumber.Tag = "CI_SERIALNUMBER";
    ComboBoxCol.AddItem(cbItemSerialNumber);

    var cbItemGenericName = new TVclComboBoxItem();
    cbItemGenericName.Value = "CI_GENERICNAME";
    cbItemGenericName.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "cbItemGenericName");
    cbItemGenericName.Tag = "CI_GENERICNAME";
    ComboBoxCol.AddItem(cbItemGenericName);

    var cbItemDefineName = new TVclComboBoxItem();
    cbItemDefineName.Value = "CIDEFINE_NAME";
    cbItemDefineName.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "cbItemDefineName");
    cbItemDefineName.Tag = "CIDEFINE_NAME";
    ComboBoxCol.AddItem(cbItemDefineName);
    ComboBoxCol.selectedIndex = 0;

    var DivlblSearch = new TVclStackPanel(Buscador.Column[0].This, "DivlblSearch_" + ID, 1, [[12], [12], [12], [12], [12]]);
    var LabelSearch = new Vcllabel(DivlblSearch.Column[0].This, "lblsearch_" + ID, TVCLType.BS, TlabelType.H0, UsrCfg.Traslate.GetLangText(_this.Mythis, "LabelSearch"));

    var DivTxtSearch = new TVclStackPanel(Buscador.Column[0].This, "DivTxtSearch_" + ID, 1, [[12], [12], [12], [12], [12]]);

    var TxtSearch = new TVclTextBox(DivTxtSearch.Column[0].This, "TxtSearch_" + ID);
    TxtSearch.VCLType = TVCLType.BS;

    var DivGrid = new TVclStackPanel(Buscador.Column[0].This, "DivGrid_" + ID, 1, [[12], [12], [12], [12], [12]]);
    DivGrid.Row.This.style.marginTop = "10px";
    DivGrid.Row.This.style.marginBottom = "10px";

    ComboBoxModo.onChange = function () {
        var VclComboBoxItem = ComboBoxModo.Options[ComboBoxModo.This.selectedIndex];
        if (VclComboBoxItem.Value == 0) {
            DivlblCol.Row.Visible = false;
            DivComboCol.Row.Visible = false;
            DivlblSearch.Row.Visible = false;
            DivTxtSearch.Row.Visible = false;
            //me quede aki viendo para que cargue la grilla....
            _this.CIGridSearch = new ItHelpCenter.CIEditor.TCIGridSearch(DivGrid.Column[0].This, ID, '', '', true);
            _this.CIGridSearch.OnSelectCI = function (sender, evenArgs) {
                if (_this.OnSelectCI != null)
                    _this.OnSelectCI(sender, evenArgs);
            }
        }
        else {
            DivlblCol.Row.Visible = true;
            DivComboCol.Row.Visible = true;
            DivlblSearch.Row.Visible = true;
            DivTxtSearch.Row.Visible = true;
        }
        //alert("Value =  " + VclComboBoxItem.Value + ", Text = " + VclComboBoxItem.Text + ", Tag = " + VclComboBoxItem.Tag);
    }

    TxtSearch.onKeyUp = function () {
        if (TxtSearch.Text.length >= 3) {
            _this.CIGridSearch = new ItHelpCenter.CIEditor.TCIGridSearch(DivGrid.Column[0].This, ID, ComboBoxCol.Value, TxtSearch.Text, false);
            _this.CIGridSearch.OnSelectCI = function (sender, evenArgs) {
                if (_this.OnSelectCI != null)
                    _this.OnSelectCI(sender, evenArgs);
            }
        }
    }

}

ItHelpCenter.CIEditor.TCIGridSearch = function (inObjectHTML, ID, Paramcolumna, ParamValue, showAll) {
    inObjectHTML.innerHTML = "";

    this.OnSelectCI = null;
    this.ctrGrid = new TGrid(inObjectHTML, ID, function () { });
    this.ctrGrid.AutoTranslate = true;
    var _this = this;
    this.ctrGrid.OnRowClick = function (sender, evenArgs) {

        var _IDCMDBCI = sender.FocusedRowHandle.GetCellValue("IDCMDBCI");
        if (_this.OnSelectCI != null)
            _this.OnSelectCI(_this, _IDCMDBCI);
    }



    var Columnas = ["(ALL)", "CI_SERIALNUMBER", "CI_GENERICNAME", "CIDEFINE_NAME"];

    var Lista = new Array();

    var StrOut = "SELECT IDCMDBCI,CI_SERIALNUMBER,CI_GENERICNAME,CIDEFINE_NAME FROM CMDBCI LEFT JOIN CMDBCIDEFINE ON CMDBCI.IDCMDBCIDEFINE=CMDBCIDEFINE.IDCMDBCIDEFINE";
    var Param = "";

    if (!showAll) {
        if (Paramcolumna == "(ALL)") {
            Param = " WHERE " + Columnas[1] + " LIKE '" + ParamValue + "%'";

            for (var i = 2; i < Columnas.length; i++) {
                Param += " OR " + Columnas[i] + " LIKE '" + ParamValue + "%'";
            }
        }
        else {
            Param = " WHERE " + Paramcolumna + " LIKE '%" + ParamValue + "%'";
        }
    }

    StrOut = "SELECT * FROM (" + StrOut + " ) as TblSearch " + Param;

    var Param2 = new SysCfg.Stream.Properties.TParam();
    Param2.Inicialize();

    var Open = SysCfg.DB.SQL.Methods.Open(StrOut, Param2.ToBytes());

    if (Open.ResErr.NotError) {
        this.ctrGrid.DataSource = Open.DataSet;
    }


}
// Fin Estructura del Search del CI Editor 

//Inicio Estructura del ExtraTableEditor
ItHelpCenter.CIEditor.TExtraTableEditor = function (inObjectHTML, ID, inCIProfiler, inIDCMDBCI, inCMDBCIDEFINEEXTRATABLE) {
    this.This;
    this.CIProfiler = inCIProfiler;
    this.StateForm = UsrCfg.CMDB.Properties.StateFormulario.New;
    this.btnNuevo;
    this.btnGuardar;
    this.btnEliminar;
    this.btnCancelar;
    this.Grilla;
    this.IsADN = false;
    this.ExtraFieldEditors = new Array();

    this.IDCMDBCI = inIDCMDBCI;
    this.txt_IDCMDB_EF;
    this.txt_IDCMDB;
    this.CMDBCIDEFINEEXTRATABLE = inCMDBCIDEFINEEXTRATABLE;
    var _this = this;
    this.Mythis = "TExtraTableEditor";
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnNuevo", "New");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnGuardar", "Save");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnEliminar", "Delete");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnCancelar", "Cancel");


    for (var i = 0; i < inCMDBCIDEFINEEXTRATABLE.CMDBCIDEFINEEXTRAFIELDSList.length; i++) {
        if (inCMDBCIDEFINEEXTRATABLE.CMDBCIDEFINEEXTRAFIELDSList[i].IDCMDBKEYTYPE == SysCfg.DB.Properties.TKeyType.ADN) {
            this.IsADN = true;
        }
    }



    this.Contenedor = new TVclStackPanel(inObjectHTML, ID, 1, [[12], [12], [12], [12], [12]]);

    var ConteIDCMDB_EF = new TVclStackPanel(this.Contenedor.Column[0].This, ID, 2, [[12, 12], [12, 12], [6, 6], [5, 7], [5, 7]]);
    ConteIDCMDB_EF.Row.This.style.margin = "5px";

    var lbl_IDCMDB_EF = new TVcllabel(ConteIDCMDB_EF.Column[0].This, "", TlabelType.H0);
    lbl_IDCMDB_EF.Text = "IDCMDB_EF" + this.CMDBCIDEFINEEXTRATABLE.EXTRATABLE_NAMETABLE;
    this.txt_IDCMDB_EF = new TVclTextBox(ConteIDCMDB_EF.Column[1].This, "");
    this.txt_IDCMDB_EF.VCLType = TVCLType.BS;
    this.txt_IDCMDB_EF.Enabled = false;

    var ConteIDCMDBCI = new TVclStackPanel(this.Contenedor.Column[0].This, ID, 2, [[12, 12], [12, 12], [6, 6], [5, 7], [5, 7]]);
    ConteIDCMDBCI.Row.This.style.margin = "5px";

    var lbl_IDCMDBCI = new TVcllabel(ConteIDCMDBCI.Column[0].This, "", TlabelType.H0);
    lbl_IDCMDBCI.Text = "IDCMDBCI";
    this.txt_IDCMDB = new TVclTextBox(ConteIDCMDBCI.Column[1].This, "");
    this.txt_IDCMDB.VCLType = TVCLType.BS;
    this.txt_IDCMDB.Enabled = false;

    this.AddExtraFieldEditor = function (ExtraFieldEditor) {
        $(this.Contenedor.Column[0].This).append(ExtraFieldEditor.Contenedor.Row.This);
        this.ExtraFieldEditors.push(ExtraFieldEditor);
    }


    for (var j = 0; j < this.CMDBCIDEFINEEXTRATABLE.CMDBCIDEFINEEXTRAFIELDSList.length; j++) {
        var ExtraFieldEditor = new ItHelpCenter.CIEditor.TExtraFieldEditor("", ID, inCIProfiler, inIDCMDBCI, this.CMDBCIDEFINEEXTRATABLE.CMDBCIDEFINEEXTRAFIELDSList[j]);
        $(this.Contenedor.Column[0].This).append(ExtraFieldEditor.Contenedor.Row.This);
        this.ExtraFieldEditors.push(ExtraFieldEditor);
    }

    var Contbuttons = new TVclStackPanel(inObjectHTML, ID, 1, [[12], [12], [12], [12], [12]]);

    this.pnGrilla = new TVclStackPanel(inObjectHTML, ID, 1, [[12], [12], [12], [12], [12]]);

    this.RefreshGrid = function () {
        var OpenTemp = this.CIProfiler.ROWEXTRATABLE_GET(this.CMDBCIDEFINEEXTRATABLE);
        if (OpenTemp != null) {
            _this.Grilla = new ItHelpCenter.CIEditor.TGrid(_this.pnGrilla.Column[0].This, ID);
            _this.Grilla.DataSource = OpenTemp.DataSet;

            if (!_this.IsADN) {
                _this.SetExtraField();
            }
        }
    }

    this.GenerateParameters = function () {
        var Param = new SysCfg.Stream.Properties.TParam();
        Param.Inicialize();

        Param.AddInt32("IDCMDBCI", _this.txt_IDCMDB.Text, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32("IDCMDB_EF" + _this.CMDBCIDEFINEEXTRATABLE.EXTRATABLE_NAMETABLE, _this.txt_IDCMDB_EF.Text, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);

        for (var i = 0; i < _this.ExtraFieldEditors.length; i++) {
            var _Control = _this.ExtraFieldEditors[i];
            switch (_Control.CMDBCIDEFINEEXTRAFIELDS.IDCMDBDBDATATYPES) {
                case SysCfg.DB.Properties.TDataType.String.value:
                    Param.AddString(_Control.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_NAME, _Control.GetValue(), SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                    break;
                case SysCfg.DB.Properties.TDataType.Text.value:
                    Param.AddString(_Control.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_NAME, _Control.GetValue(), SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                    break;
                case SysCfg.DB.Properties.TDataType.Int32.value:
                    Param.AddInt32(_Control.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_NAME, _Control.GetValue(), SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                    break;
                case SysCfg.DB.Properties.TDataType.Double.value:
                    Param.AddDouble(_Control.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_NAME, _Control.GetValue(), SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                    break;
                case SysCfg.DB.Properties.TDataType.Boolean.value:
                    Param.AddBoolean(_Control.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_NAME, _Control.GetValue(), SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                    break;
                case SysCfg.DB.Properties.TDataType.DateTime.value:
                    Param.AddDateTime(_Control.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_NAME, _Control.GetValue(), SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
                    break;
                default:
                    break;
            }
        }
        return Param;
    }

    this.btnNuevo = new TVclButton(Contbuttons.Column[0].This, ID);
    this.btnNuevo.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnNuevo");
    this.btnNuevo.Src = "image/16/add.png";
    this.btnNuevo.VCLType = TVCLType.BS;
    this.btnNuevo.This.style.marginRight = "10px";
    this.btnNuevo.onClick = function () {
        _this.CleanControls();
        _this.EnabledControls(true);
        _this.StateForm = UsrCfg.CMDB.Properties.StateFormulario.New;
        _this.EnabledButton(false, true, false, true);
    }

    this.btnGuardar = new TVclButton(Contbuttons.Column[0].This, ID);
    this.btnGuardar.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnGuardar");
    this.btnGuardar.Src = "image/16/success.png";
    this.btnGuardar.VCLType = TVCLType.BS;
    this.btnGuardar.This.style.marginRight = "10px";
    this.btnGuardar.onClick = function () {
        var tipe = typeof ("number");

        if (_this.StateForm == UsrCfg.CMDB.Properties.StateFormulario.New) {
            _this.CIProfiler.ROWEXTRATABLE_ADD(_this.CMDBCIDEFINEEXTRATABLE, _this.GenerateParameters());
            if (_this.IsADN) {
                _this.CleanControls();
                _this.EnabledControls(false);
                _this.StateForm = UsrCfg.CMDB.Properties.StateFormulario.New;
                _this.EnabledButton(true, false, false, false);
            }
            else {
                _this.StateForm = UsrCfg.CMDB.Properties.StateFormulario.Update;
                _this.EnabledButton(true, true, true, false);
            }
        }
        else {
            if (_this.StateForm == UsrCfg.CMDB.Properties.StateFormulario.Update) {
                _this.CIProfiler.ROWEXTRATABLE_UPD(_this.CMDBCIDEFINEEXTRATABLE, _this.GenerateParameters());

                if (_this.IsADN) {
                    _this.CleanControls();
                    _this.EnabledControls(false);
                    _this.EnabledButton(true, false, false, false);
                }
                else {
                    _this.EnabledButton(true, true, true, false);
                }
            }
        }
        _this.RefreshGrid();
        alert('Se grabo correctamente.');
    }

    this.btnEliminar = new TVclButton(Contbuttons.Column[0].This, ID);
    this.btnEliminar.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnEliminar");
    this.btnEliminar.Src = "image/16/Delete.png";
    this.btnEliminar.VCLType = TVCLType.BS;
    this.btnEliminar.This.style.marginRight = "10px";
    this.btnEliminar.onClick = function () {

        _this.CIProfiler.ROWEXTRATABLE_DEL(_this.CMDBCIDEFINEEXTRATABLE, _this.GenerateParameters());
        _this.CleanControls();
        _this.RefreshGrid();
        alert("Se elimino correctamente");
    }

    this.btnCancelar = new TVclButton(Contbuttons.Column[0].This, ID);
    this.btnCancelar.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnCancelar");
    this.btnCancelar.Src = "image/16/Cancel.png";
    this.btnCancelar.VCLType = TVCLType.BS;
    this.btnCancelar.This.style.marginRight = "10px";
    this.btnCancelar.onClick = function () {
        _this.CleanControls();
        _this.EnabledControls(false);
        _this.StateForm = UsrCfg.CMDB.Properties.StateFormulario.Cancel;
        _this.EnabledButton(true, false, false, false);
    }

    this.GetExtraFieldEditor = function (NameExtraField) {
        for (var i = 0; i < this.ExtraFieldEditors.length; i++) {
            if (this.ExtraFieldEditors[i].CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_NAME == NameExtraField)
                return this.ExtraFieldEditors[i];
        }
        return null;
    }

    this.SetExtraField = function () {

        if (this.Grilla.FocusedRowHandle != null) {
            this.StateForm = UsrCfg.CMDB.Properties.StateFormulario.Update;
            this.txt_IDCMDB_EF.Text = this.Grilla.FocusedRowHandle.GetCellValue("IDCMDB_EF" + this.CMDBCIDEFINEEXTRATABLE.EXTRATABLE_NAMETABLE);
            this.txt_IDCMDB.Text = this.Grilla.FocusedRowHandle.GetCellValue("IDCMDBCI");

            for (var i = 0; i < this.CMDBCIDEFINEEXTRATABLE.CMDBCIDEFINEEXTRAFIELDSList.length; i++) {
                //setear value de extrafieldeditor y hacer funcion para traer el extrafield por nombre
                var _ExtraFieldEditor = this.GetExtraFieldEditor(this.CMDBCIDEFINEEXTRATABLE.CMDBCIDEFINEEXTRAFIELDSList[i].EXTRAFIELDS_NAME);
                _ExtraFieldEditor.SetValue(this.Grilla.FocusedRowHandle.GetCellValue(this.CMDBCIDEFINEEXTRATABLE.CMDBCIDEFINEEXTRAFIELDSList[i].EXTRAFIELDS_NAME));
            }
        }
        else
            this.StateForm = UsrCfg.CMDB.Properties.StateFormulario.New;
    }

    this.EnabledControls = function (value) {
        for (var i = 0; i < this.ExtraFieldEditors.length; i++) {
            this.ExtraFieldEditors[i].Enabled = value;
        }
    }

    this.CleanControls = function () {
        _this.txt_IDCMDB.Text = "";
        _this.txt_IDCMDB_EF.Text = "";

        for (var i = 0; i < this.ExtraFieldEditors.length; i++) {
            this.ExtraFieldEditors[i].CleanValue()
        }
    }

    this.EnabledButton = function (_New, _Save, _Delete, _Cancel) {
        this.btnNuevo.Enabled = _New;
        this.btnGuardar.Enabled = _Save;
        this.btnEliminar.Enabled = _Delete;
        this.btnCancelar.Enabled = _Cancel;
    }

    if (this.IsADN) { this.pnGrilla.Row.Visible = true; this.btnNuevo.Visible = true; this.btnCancelar.Visible = true; this.EnabledControls(false); this.EnabledButton(true, false, false, false); }
    else { this.pnGrilla.Row.Visible = false; this.btnNuevo.Visible = false; this.btnCancelar.Visible = false; this.EnabledControls(true); this.EnabledButton(false, true, true, false); }

    this.RefreshGrid();
}
//Fin Estructura del ExtraTableEditor

//Inicio Estructura del ExtraFieldEditor
ItHelpCenter.CIEditor.TExtraFieldEditor = function (inObjectHTML, ID, inCIProfiler, inIDCMDBCI, inCMDBCIDEFINEEXTRAFIELDS) {
    this.This;
    this.CIProfiler = inCIProfiler;
    this.IDCMDBCI = inIDCMDBCI;
    this.CMDBCIDEFINEEXTRAFIELDS = inCMDBCIDEFINEEXTRAFIELDS;
    this.Label;
    this.Control;
    this.Contenedor = new TVclStackPanel("", ID, 2, [[12, 12], [12, 12], [6, 6], [5, 7], [5, 7]]);
    this.Contenedor.Row.This.style.margin = "5px";

    this.Label = new TVcllabel(this.Contenedor.Column[0].This, "", TlabelType.H0);
    this.Label.Text = this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_NAME;

    this.cargarLookUp = function (cbLookUp) {
        var Open = SysCfg.DB.SQL.Methods.Open(this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_LOOKUPSQL, new Array(0));
        if (Open.ResErr.NotError) {
            if (Open.DataSet.RecordCount > 0) {
                Open.DataSet.First();
                while (!(Open.DataSet.Eof)) {
                    var cbItem = new TVclComboBoxItem();
                    cbItem.Value = Open.DataSet.RecordSet.FieldName(this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_LOOKUPID).asInt32();
                    cbItem.Text = Open.DataSet.RecordSet.FieldName(this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_LOOKUPDISPLAY).asString();
                    cbItem.Tag = Open.DataSet.RecordSet;
                    cbLookUp.AddItem(cbItem);

                    Open.DataSet.Next();
                }
            }
            else {
                Open.ResErr.NotError = false;
                Open.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
        else {
            SysCfg.App.Methods.ShowMessage(Open.ResErr.Mesaje);
        }
    }

    this.CargarLookUpOption = function (rbLookUpOption) {
        var Open = SysCfg.DB.SQL.Methods.Open(this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_LOOKUPSQL, new Array(0));
        if (Open.ResErr.NotError) {
            if (Open.DataSet.RecordCount > 0) {
                Open.DataSet.First();
                while (!(Open.DataSet.Eof)) {
                    var VclInputRadioButtonItem = new TVclInputRadioButtonItem();
                    VclInputRadioButtonItem.Id = "rbItem" + Open.DataSet.RecordSet.FieldName(this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_LOOKUPID).asInt32();
                    VclInputRadioButtonItem.Text = Open.DataSet.RecordSet.FieldName(this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_LOOKUPDISPLAY).asString();
                    VclInputRadioButtonItem.Value = Open.DataSet.RecordSet.FieldName(this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_LOOKUPID).asInt32();
                    rbLookUpOption.AddItem(VclInputRadioButtonItem);

                    Open.DataSet.Next();
                }
            }
            else {
                Open.ResErr.NotError = false;
                Open.ResErr.Mesaje = "RECORDCOUNT = 0";
            }
        }
        else {
            SysCfg.App.Methods.ShowMessage(Open.ResErr.Mesaje);
        }
    }

    this.CleanValue = function () {
        //switch (this.CMDBCIDEFINEEXTRAFIELDS.IDCMDBDBDATATYPES) {
        switch (SysCfg.DB.Properties.TDataType.GetEnum(this.CMDBCIDEFINEEXTRAFIELDS.IDCMDBDBDATATYPES)) {
            case SysCfg.DB.Properties.TDataType.String: // SysCfg.DB.Properties.TDataType.String   
                switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                        //LIMPIAR UN TEXTBOX
                        this.Control.Text = "";
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.TextPassword:
                        //LIMPIAR UN TEXTBOX DE TIPO PASSWORD   
                        this.Control.Text = "";
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.LookUp:
                        //LIMPIAR UN COMBOBOX 
                        this.Control.selectedIndex = 0;
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.LookUpOption:
                        //LIMPIAR UN RADIO BUTTON
                        break;
                }
                break;
            case SysCfg.DB.Properties.TDataType.Text: //SysCfg.DB.Properties.TDataType.Text  
                switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                        //LIMPIAR UN MEMO TEXT
                        this.Control.Text = "";
                        break;
                }
                break;
            case SysCfg.DB.Properties.TDataType.Int32: // SysCfg.DB.Properties.TDataType.Int32:   
                switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                        //LIMPIAR UN NUMERICUPDOWN
                        this.Control.Text = "";
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.LookUp:
                        //LIMPIAR UN COMBOBOX 
                        this.Control.selectedIndex = 0;
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.LookUpOption:
                        //LIMPIAR UN RADIO BUTTON
                        break;
                }
                break;
            case SysCfg.DB.Properties.TDataType.Double: //SysCfg.DB.Properties.TDataType.Double:   
                switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                        //LIMPIAR UN NUMERICUPDOWN                    
                        this.Control.Text = "0";
                        break;
                }
                break;
            case SysCfg.DB.Properties.TDataType.Boolean: //SysCfg.DB.Properties.TDataType.Boolean:     
                switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                        //LIMPIAR UN CHECKBOX
                        this.Control.Checked = true;
                        break;
                }
                break;
            case SysCfg.DB.Properties.TDataType.DateTime: //SysCfg.DB.Properties.TDataType.DateTime: 
                switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                        //LIMPIAR UN DATATIME PICKER
                        this.Control.Text = "";
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.Date:
                        //LIMPIAR UN DATE PICKER 
                        this.Control.Text = "";
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.Time:
                        //LIMPIAR UN TIME PICKER
                        this.Control.Text = "";
                        break;
                }
                break
            default:
                break;
        }
    }

    this.GetValue = function () {
        var Valor;
        //switch (this.CMDBCIDEFINEEXTRAFIELDS.IDCMDBDBDATATYPES) {
        switch (SysCfg.DB.Properties.TDataType.GetEnum(this.CMDBCIDEFINEEXTRAFIELDS.IDCMDBDBDATATYPES)) {
            case SysCfg.DB.Properties.TDataType.String: // SysCfg.DB.Properties.TDataType.String   
                switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                        //LIMPIAR UN TEXTBOX
                        Valor = this.Control.Text;
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.TextPassword:
                        //LIMPIAR UN TEXTBOX DE TIPO PASSWORD   
                        Valor = this.Control.Text;
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.LookUp:
                        //LIMPIAR UN COMBOBOX 
                        Valor = this.Control.Value;
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.LookUpOption:
                        //LIMPIAR UN RADIO BUTTON
                        Valor = this.Control.Value;
                        break;
                }
                break;
            case SysCfg.DB.Properties.TDataType.Text: //SysCfg.DB.Properties.TDataType.Text  
                switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                        //LIMPIAR UN MEMO TEXT
                        Valor = this.Control.Text;
                        break;
                }
                break;
            case SysCfg.DB.Properties.TDataType.Int32: // SysCfg.DB.Properties.TDataType.Int32:   
                switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                        //LIMPIAR UN NUMERICUPDOWN
                        Valor = parseInt(this.Control.Text);
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.LookUp:
                        //LIMPIAR UN COMBOBOX 
                        Valor = this.Control.Value;
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.LookUpOption:
                        //LIMPIAR UN RADIO BUTTON
                        Valor = this.Control.Value;
                        break;
                }
                break;
            case SysCfg.DB.Properties.TDataType.Double: //SysCfg.DB.Properties.TDataType.Double:   
                switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                        //LIMPIAR UN NUMERICUPDOWN                    
                        Valor = parseFloat(this.Control.Text);
                        break;
                }
                break;
            case SysCfg.DB.Properties.TDataType.Boolean: //SysCfg.DB.Properties.TDataType.Boolean:     
                switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                        //LIMPIAR UN CHECKBOX
                        Valor = this.Control.Checked;
                        break;
                }
                break;
            case SysCfg.DB.Properties.TDataType.DateTime: //SysCfg.DB.Properties.TDataType.DateTime: 
                switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                        //LIMPIAR UN DATATIME PICKER
                        Valor = this.Control.Text;
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.Date:
                        //LIMPIAR UN DATE PICKER 
                        Valor = this.Control.Text;
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.Time:
                        //LIMPIAR UN TIME PICKER
                        Valor = this.Control.Text;
                        break;
                }
                break
            default:
                break;
        }
        return Valor;
    }

    this.SetValue = function (Valor) {

        //switch (this.CMDBCIDEFINEEXTRAFIELDS.IDCMDBDBDATATYPES) {
        var _type = SysCfg.DB.Properties.TDataType.GetEnum(this.CMDBCIDEFINEEXTRAFIELDS.IDCMDBDBDATATYPES);
        switch (_type) {
            case SysCfg.DB.Properties.TDataType.String: // SysCfg.DB.Properties.TDataType.String   
                switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                        //LIMPIAR UN TEXTBOX
                        this.Control.Text = Valor;
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.TextPassword:
                        //LIMPIAR UN TEXTBOX DE TIPO PASSWORD   
                        this.Control.Text = Valor;
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.LookUp:
                        //LIMPIAR UN COMBOBOX 
                        this.Control.Value = Valor;
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.LookUpOption:
                        this.Control.Value = Valor;
                        //LIMPIAR UN RADIO BUTTON
                        break;
                }
                break;
            case SysCfg.DB.Properties.TDataType.Text: //SysCfg.DB.Properties.TDataType.Text  
                switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                        //LIMPIAR UN MEMO TEXT
                        this.Control.Text = Valor;
                        break;
                }
                break;
            case SysCfg.DB.Properties.TDataType.Int32: // SysCfg.DB.Properties.TDataType.Int32:   
                switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                        //LIMPIAR UN NUMERICUPDOWN
                        this.Control.Text = Valor;
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.LookUp:
                        //LIMPIAR UN COMBOBOX 
                        this.Control.Value = Valor;
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.LookUpOption:
                        this.Control.Value = Valor;
                        //LIMPIAR UN RADIO BUTTON
                        break;
                }
                break;
            case SysCfg.DB.Properties.TDataType.Double: //SysCfg.DB.Properties.TDataType.Double:   
                switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                        //LIMPIAR UN NUMERICUPDOWN                    
                        this.Control.Text = Valor;
                        break;
                }
                break;
            case SysCfg.DB.Properties.TDataType.Boolean: //SysCfg.DB.Properties.TDataType.Boolean:     
                switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                        //LIMPIAR UN CHECKBOX
                        this.Control.Checked = Valor;
                        break;
                }
                break;
            case SysCfg.DB.Properties.TDataType.DateTime: //SysCfg.DB.Properties.TDataType.DateTime: 
                switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                        //LIMPIAR UN DATATIME PICKER
                        this.Control.Text = Valor;
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.Date:
                        //LIMPIAR UN DATE PICKER 
                        this.Control.Text = Valor;
                        break;
                    case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.Time:
                        //LIMPIAR UN TIME PICKER
                        this.Control.Text = Valor;
                        break;
                }
                break
            default:
                break;
        }
    }


    switch (SysCfg.DB.Properties.TDataType.GetEnum(this.CMDBCIDEFINEEXTRAFIELDS.IDCMDBDBDATATYPES)) {
        case SysCfg.DB.Properties.TDataType.String: // SysCfg.DB.Properties.TDataType.String   
            switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                    //CREAR UN TEXTBOX
                    this.Control = new TVclTextBox(this.Contenedor.Column[1].This, "");
                    this.Control.VCLType = TVCLType.BS;
                    break;
                case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.TextPassword:
                    //CREAR UN TEXTBOX DE TIPO PASSWORD
                    this.Control = new TVclTextBox(this.Contenedor.Column[1].This, "");
                    this.Control.VCLType = TVCLType.BS;
                    this.Control.Type = TImputtype.password;
                    break;
                case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.LookUp:
                    //CREAR UN COMBOBOX 
                    this.Control = new TVclComboBox2(this.Contenedor.Column[1].This, "");
                    this.Control.VCLType = TVCLType.BS;
                    this.cargarLookUp(this.Control);
                    break;
                case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.LookUpOption:
                    //CREAR UN RADIO BUTTON
                    this.Control = new TVclInputRadioButton(this.Contenedor.Column[1].This, "");
                    this.CargarLookUpOption(this.Control);
                    break;
            }
            break;
        case SysCfg.DB.Properties.TDataType.Text: //SysCfg.DB.Properties.TDataType.Text  
            switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                    //CREAR UN MEMO TEXT
                    this.Control = new TVclMemo(this.Contenedor.Column[1].This, "");
                    this.Control.This.classList.add("form-control");
                    break;
            }
            break;
        case SysCfg.DB.Properties.TDataType.Int32: // SysCfg.DB.Properties.TDataType.Int32:   
            switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                    //CREAR UN NUMERICUPDOWN
                    this.Control = new TVclTextBox(this.Contenedor.Column[1].This, "");
                    this.Control.VCLType = TVCLType.BS;
                    this.Control.Type = TImputtype.number;
                    break;
                case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.LookUp:
                    //CREAR UN COMBOBOX 
                    this.Control = new TVclComboBox2(this.Contenedor.Column[1].This, "");
                    this.Control.VCLType = TVCLType.BS;
                    this.cargarLookUp(this.Control);
                    break;
                case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.LookUpOption:
                    //CREAR UN RADIO BUTTON
                    this.Control = new TVclInputRadioButton(this.Contenedor.Column[1].This, "");
                    this.CargarLookUpOption(this.Control);
                    break;
            }
            break;
        case SysCfg.DB.Properties.TDataType.Double: //SysCfg.DB.Properties.TDataType.Double:   
            switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                    //CREAR UN NUMERICUPDOWN                    
                    this.Control = new TVclTextBox(this.Contenedor.Column[1].This, "");
                    this.Control.VCLType = TVCLType.BS;
                    this.Control.Type = TImputtype.number;
                    break;
            }
            break;
        case SysCfg.DB.Properties.TDataType.Boolean: //SysCfg.DB.Properties.TDataType.Boolean:     
            switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                    //CREAR UN CHECKBOX
                    this.Control = new TVclInputcheckbox(this.Contenedor.Column[1].This, "");
                    break;
            }
            break;
        case SysCfg.DB.Properties.TDataType.DateTime: //SysCfg.DB.Properties.TDataType.DateTime: 
            switch (this.CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None:
                    //CREAR UN DATATIME PICKER
                    this.Control = new TVclTextBox(this.Contenedor.Column[1].This, "");
                    this.Control.VCLType = TVCLType.BS;
                    $(this.Control.This).datetimepicker({
                        timeFormat: "hh:mm tt"
                    });
                    break;
                case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.Date:
                    //CREAR UN DATE PICKER 
                    this.Control = new TVclTextBox(this.Contenedor.Column[1].This, "");
                    this.Control.VCLType = TVCLType.BS;
                    $(this.Control.This).datetimepicker({
                        showButtonPanel: true
                    });
                    break;
                case UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.Time:
                    //CREAR UN TIME PICKER
                    this.Control = new TVclTextBox(this.Contenedor.Column[1].This, "");
                    this.Control.VCLType = TVCLType.BS;
                    $(this.Control.This).timepicker({
                        timeFormat: "hh:mm tt"
                    });
                    break;
            }
            break
        default:
            break;
    }

    Object.defineProperty(this, 'Enabled', {
        get: function () {
            return this.Control.Enabled;
        },
        set: function (_Value) {
            this.Control.Enabled = _Value;
        }
    });
}
//Fin Estructura del ExtraFieldEditor

//Inicio Estructura del CI_relations
ItHelpCenter.CIEditor.TCIRelationsEditor = function (inObjectHTML, ID, inCIProfiler, inCMDBCIDEFINERELATIONTYPE, inIDCMDBCI, _IsChild) {
    this.This;
    this.CIProfiler = inCIProfiler;
    this.btnNuevo;
    this.btnEliminar;
    this.Grilla;
    this.IsChild = _IsChild;
    this.IDCMDBCI = inIDCMDBCI;
    this.CMDBCIDEFINERELATIONTYPE = inCMDBCIDEFINERELATIONTYPE
    this.Urlrel;
    this.TCI_RELATION_DEFINETYPEList = new Array();

    var _this = this;
    this.Mythis = "TCIRelationsEditor";
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnNuevo", "New");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnEliminar", "Delete");



    this.ContButtons = new TVclStackPanel(inObjectHTML, ID, 2, [[1, 11], [7, 5], [7, 5], [7, 5], [7, 5]]);
    this.ContButtons.Row.This.style.marginTop = "10px";
    this.ContButtons.Row.This.style.marginBottom = "10px";

    this.Contenedor = new TVclStackPanel(inObjectHTML, ID, 1, [[12], [12], [12], [12], [12]]);

    this.ContModal = new TVclStackPanel(inObjectHTML, ID, 1, [[12], [12], [12], [12], [12]]);

    this.CargarGrilla = function () {
        //if (this.IsChild) {
        $(this.Contenedor.Column[0].This).html("");
        this.Grilla = new ItHelpCenter.CIEditor.TGrid(_this.Contenedor.Column[0].This, "");

        var Col0 = new ItHelpCenter.CIEditor.TColumn();
        Col0.Name = "FDMA01";
        Col0.Caption = "FDMA01";
        Col0.Index = 0;
        Col0.EnabledEditor = true;
        Col0.DataType = SysCfg.DB.Properties.TDataType.Boolean;
        Col0.ColumnStyle = UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.None;
        this.Grilla.AddColumn(Col0);

        var Col1 = new ItHelpCenter.CIEditor.TColumn();
        Col1.Name = "CI_DATEPLANNED";
        Col1.Caption = "CI_DATEPLANNED";
        Col1.Index = 1;
        //Col1.EnabledEditor = true;
        //Col1.DataType = SysCfg.DB.Properties.TDataType.DateTime;
        //Col1.ColumnStyle = UsrCfg.CMDB.Properties.TExtrafields_ColumnStyle.Date;
        this.Grilla.AddColumn(Col1);

        var Col2 = new ItHelpCenter.CIEditor.TColumn();
        Col2.Name = "CI_DATEIN";
        Col2.Caption = "CI_DATEIN";
        Col2.Index = 2;
        this.Grilla.AddColumn(Col2);

        var Col3 = new ItHelpCenter.CIEditor.TColumn();
        Col3.Name = "CI_DATEOUT";
        Col3.Caption = "CI_DATEOUT";
        Col3.Index = 3;
        this.Grilla.AddColumn(Col3);

        var Col4 = new ItHelpCenter.CIEditor.TColumn();
        Col4.Name = "CI_GENERICNAME";
        Col4.Caption = "CI_GENERICNAME";
        Col4.Index = 4;
        this.Grilla.AddColumn(Col4);

        var Col5 = new ItHelpCenter.CIEditor.TColumn();
        Col5.Name = "CI_DESCRIPTION";
        Col5.Caption = "CI_DESCRIPTION";
        Col5.Index = 5;
        this.Grilla.AddColumn(Col5);

        var Col6 = new ItHelpCenter.CIEditor.TColumn();
        Col6.Name = "IDCMDBCI";
        Col6.Caption = "IDCMDBCI";
        Col6.Index = 6;
        this.Grilla.AddColumn(Col6);

        var Col7 = new ItHelpCenter.CIEditor.TColumn();
        Col7.Name = "IDCMDBBRAND";
        Col7.Caption = "IDCMDBBRAND";
        Col7.Index = 7;
        this.Grilla.AddColumn(Col7);

        var Col8 = new ItHelpCenter.CIEditor.TColumn();
        Col8.Name = "CI_SERIALNUMBER";
        Col8.Caption = "CI_SERIALNUMBER";
        Col8.Index = 8;
        this.Grilla.AddColumn(Col8);

        var Col9 = new ItHelpCenter.CIEditor.TColumn();
        Col9.Name = "CI_PURCHASEDORRENTED";
        Col9.Caption = "CI_PURCHASEDORRENTED";
        Col9.Index = 9;
        this.Grilla.AddColumn(Col9);

        var Col10 = new ItHelpCenter.CIEditor.TColumn();
        Col10.Name = "CIDEFINE_NAME";
        Col10.Caption = "CIDEFINE_NAME";
        Col10.Index = 10;
        this.Grilla.AddColumn(Col10);

        var Col11 = new ItHelpCenter.CIEditor.TColumn();
        Col11.Name = "CIDEFINERELATIONTYPE_NAME";
        Col11.Caption = "CIDEFINERELATIONTYPE_NAME";
        Col11.Index = 11;
        this.Grilla.AddColumn(Col11);

        var Col12 = new ItHelpCenter.CIEditor.TColumn();
        Col12.Name = "IDCMDBCIDEFINE_CHILD";
        Col12.Caption = "IDCMDBCIDEFINE_CHILD";
        Col12.Index = 12;
        this.Grilla.AddColumn(Col12);

        var Col13 = new ItHelpCenter.CIEditor.TColumn();
        Col13.Name = "IDCMDBCIDEFINE_PARENT";
        Col13.Caption = "IDCMDBCIDEFINE_PARENT";
        Col13.Index = 13;
        this.Grilla.AddColumn(Col13);

        var Col14 = new ItHelpCenter.CIEditor.TColumn();
        Col14.Name = "IDCMDBRELATIONTYPE";
        Col14.Caption = "IDCMDBRELATIONTYPE";
        Col14.Index = 14;
        this.Grilla.AddColumn(Col14);

        var Col15 = new ItHelpCenter.CIEditor.TColumn();
        Col15.Name = "IDCMDBCI_CHILD";
        Col15.Caption = "IDCMDBCI_CHILD";
        Col15.Index = 15;
        this.Grilla.AddColumn(Col15);

        var Col16 = new ItHelpCenter.CIEditor.TColumn();
        Col16.Name = "IDCMDBCI_PARENT";
        Col16.Caption = "IDCMDBCI_PARENT";
        Col16.Index = 16;
        this.Grilla.AddColumn(Col16);

        var Col17 = new ItHelpCenter.CIEditor.TColumn();
        Col17.Name = "IDCMDBCIRELATION";
        Col17.Caption = "IDCMDBCIRELATION";
        Col17.Index = 17;
        this.Grilla.AddColumn(Col17);

        var Col18 = new ItHelpCenter.CIEditor.TColumn();
        Col18.Name = "IDCMDBCIDEFINERELATIONTYPE";
        Col18.Caption = "IDCMDBCIDEFINERELATIONTYPE";
        Col18.Index = 18;
        this.Grilla.AddColumn(Col18);
    }

    this.AddToGrid = function (CMDBCIRELATION) {
        var CI_RELATION_DEFINETYPE = new ItHelpCenter.CIEditor.TCI_RELATION_DEFINETYPE();
        //******** chekEdit ****************
        CI_RELATION_DEFINETYPE.FDMA01 = false;
        //********TCMDBCIRELATION********
        CI_RELATION_DEFINETYPE.IDCMDBCI_CHILD = CMDBCIRELATION.IDCMDBCI_CHILD;
        CI_RELATION_DEFINETYPE.IDCMDBCIRELATION = CMDBCIRELATION.IDCMDBCIRELATION;
        CI_RELATION_DEFINETYPE.IDCMDBCIDEFINERELATIONTYPE = CMDBCIRELATION.IDCMDBCIDEFINERELATIONTYPE;
        //********TCMDBCIDEFINERELATIONTYPE********
        CI_RELATION_DEFINETYPE.CIDEFINERELATIONTYPE_NAME = CMDBCIRELATION.CMDBCIDEFINERELATIONTYPE.CIDEFINERELATIONTYPE_NAME;
        CI_RELATION_DEFINETYPE.IDCMDBCIDEFINE_CHILD = CMDBCIRELATION.CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINE_CHILD;
        CI_RELATION_DEFINETYPE.IDCMDBCIDEFINE_PARENT = CMDBCIRELATION.CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINE_PARENT;
        CI_RELATION_DEFINETYPE.IDCMDBRELATIONTYPE = CMDBCIRELATION.IDCMDBRELATIONTYPE;

        var UnCI = null;
        if (this.IsChild) {
            UnCI = CMDBCIRELATION.CI_CHILD;
        }
        else {
            UnCI = CMDBCIRELATION.CI_PARENT;
        }
        if (UnCI != null) {
            //********TCI********
            CI_RELATION_DEFINETYPE.CI_DATEPLANNED = UnCI.CI_DATEPLANNED;
            CI_RELATION_DEFINETYPE.CI_DATEIN = UnCI.CI_DATEIN;
            CI_RELATION_DEFINETYPE.CI_DATEOUT = UnCI.CI_DATEOUT;
            CI_RELATION_DEFINETYPE.CI_GENERICNAME = UnCI.CI_GENERICNAME;
            CI_RELATION_DEFINETYPE.CI_DESCRIPTION = UnCI.CI_DESCRIPTION;
            CI_RELATION_DEFINETYPE.IDCMDBCI = UnCI.IDCMDBCI;
            CI_RELATION_DEFINETYPE.IDCMDBBRAND = UnCI.IDCMDBBRAND;
            CI_RELATION_DEFINETYPE.CI_SERIALNUMBER = UnCI.CI_SERIALNUMBER;
            CI_RELATION_DEFINETYPE.CI_PURCHASEDORRENTED = UnCI.CI_PURCHASEDORRENTED;
            CI_RELATION_DEFINETYPE.CIDEFINE_NAME = UnCI.CIDEFINE_NAME;
        }
        this.TCI_RELATION_DEFINETYPEList.push(CI_RELATION_DEFINETYPE);

        var NewRow = new ItHelpCenter.CIEditor.TRow();

        var Cell0 = new ItHelpCenter.CIEditor.TCell();
        Cell0.Value = false;
        Cell0.IndexColumn = 0;
        Cell0.IndexRow = this.Grilla.Rows.length;
        NewRow.AddCell(Cell0);

        var Cell1 = new ItHelpCenter.CIEditor.TCell();
        Cell1.Value = CI_RELATION_DEFINETYPE.CI_DATEPLANNED;
        Cell1.IndexColumn = 1;
        Cell1.IndexRow = this.Grilla.Rows.length;
        NewRow.AddCell(Cell1);

        var Cell2 = new ItHelpCenter.CIEditor.TCell();
        Cell2.Value = CI_RELATION_DEFINETYPE.CI_DATEIN;
        Cell2.IndexColumn = 2;
        Cell2.IndexRow = this.Grilla.Rows.length;
        NewRow.AddCell(Cell2);

        var Cell3 = new ItHelpCenter.CIEditor.TCell();
        Cell3.Value = CI_RELATION_DEFINETYPE.CI_DATEOUT;
        Cell3.IndexColumn = 3;
        Cell3.IndexRow = this.Grilla.Rows.length;
        NewRow.AddCell(Cell3);

        var Cell4 = new ItHelpCenter.CIEditor.TCell();
        Cell4.Value = CI_RELATION_DEFINETYPE.CI_GENERICNAME;
        Cell4.IndexColumn = 4;
        Cell4.IndexRow = this.Grilla.Rows.length;
        NewRow.AddCell(Cell4);

        var Cell5 = new ItHelpCenter.CIEditor.TCell();
        Cell5.Value = CI_RELATION_DEFINETYPE.CI_DESCRIPTION;
        Cell5.IndexColumn = 5;
        Cell5.IndexRow = this.Grilla.Rows.length;
        NewRow.AddCell(Cell5);

        var Cell6 = new ItHelpCenter.CIEditor.TCell();
        Cell6.Value = CI_RELATION_DEFINETYPE.IDCMDBCI;
        Cell6.IndexColumn = 6;
        Cell6.IndexRow = this.Grilla.Rows.length;
        NewRow.AddCell(Cell6);

        var Cell7 = new ItHelpCenter.CIEditor.TCell();
        Cell7.Value = CI_RELATION_DEFINETYPE.IDCMDBBRAND;
        Cell7.IndexColumn = 7;
        Cell7.IndexRow = this.Grilla.Rows.length;
        NewRow.AddCell(Cell7);

        var Cell8 = new ItHelpCenter.CIEditor.TCell();
        Cell8.Value = CI_RELATION_DEFINETYPE.CI_SERIALNUMBER;
        Cell8.IndexColumn = 8;
        Cell8.IndexRow = this.Grilla.Rows.length;
        NewRow.AddCell(Cell8);

        var Cell9 = new ItHelpCenter.CIEditor.TCell();
        Cell9.Value = CI_RELATION_DEFINETYPE.CI_PURCHASEDORRENTED;
        Cell9.IndexColumn = 9;
        Cell9.IndexRow = this.Grilla.Rows.length;
        NewRow.AddCell(Cell9);

        var Cell10 = new ItHelpCenter.CIEditor.TCell();
        Cell10.Value = CI_RELATION_DEFINETYPE.CIDEFINE_NAME;
        Cell10.IndexColumn = 10;
        Cell10.IndexRow = this.Grilla.Rows.length;
        NewRow.AddCell(Cell10);

        var Cell11 = new ItHelpCenter.CIEditor.TCell();
        Cell11.Value = CI_RELATION_DEFINETYPE.CIDEFINERELATIONTYPE_NAME;
        Cell11.IndexColumn = 11;
        Cell11.IndexRow = this.Grilla.Rows.length;
        NewRow.AddCell(Cell11);

        var Cell12 = new ItHelpCenter.CIEditor.TCell();
        Cell12.Value = CI_RELATION_DEFINETYPE.IDCMDBCIDEFINE_CHILD;
        Cell12.IndexColumn = 12;
        Cell12.IndexRow = this.Grilla.Rows.length;
        NewRow.AddCell(Cell12);

        var Cell13 = new ItHelpCenter.CIEditor.TCell();
        Cell13.Value = CI_RELATION_DEFINETYPE.IDCMDBCIDEFINE_PARENT;
        Cell13.IndexColumn = 13;
        Cell13.IndexRow = this.Grilla.Rows.length;
        NewRow.AddCell(Cell13);

        var Cell14 = new ItHelpCenter.CIEditor.TCell();
        Cell14.Value = CI_RELATION_DEFINETYPE.IDCMDBRELATIONTYPE;
        Cell14.IndexColumn = 14;
        Cell14.IndexRow = this.Grilla.Rows.length;
        NewRow.AddCell(Cell14);

        var Cell15 = new ItHelpCenter.CIEditor.TCell();
        Cell15.Value = CI_RELATION_DEFINETYPE.IDCMDBCI_CHILD;
        Cell15.IndexColumn = 15;
        Cell15.IndexRow = this.Grilla.Rows.length;
        NewRow.AddCell(Cell15);

        var Cell16 = new ItHelpCenter.CIEditor.TCell();
        Cell16.Value = CI_RELATION_DEFINETYPE.IDCMDBCI_PARENT;
        Cell16.IndexColumn = 16;
        Cell16.IndexRow = this.Grilla.Rows.length;
        NewRow.AddCell(Cell16);

        var Cell17 = new ItHelpCenter.CIEditor.TCell();
        Cell17.Value = CI_RELATION_DEFINETYPE.IDCMDBCIRELATION;
        Cell17.IndexColumn = 17;
        Cell17.IndexRow = this.Grilla.Rows.length;
        NewRow.AddCell(Cell17);

        var Cell18 = new ItHelpCenter.CIEditor.TCell();
        Cell18.Value = CI_RELATION_DEFINETYPE.IDCMDBCIDEFINERELATIONTYPE;
        Cell18.IndexColumn = 18;
        Cell18.IndexRow = this.Grilla.Rows.length;
        NewRow.AddCell(Cell18);

        this.Grilla.AddRow(NewRow);
    }

    this.CargarGrilla();

    this.btnNuevo = new TVclButton(this.ContButtons.Column[0].This, ID);
    this.btnNuevo.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnNuevo");
    this.btnNuevo.Src = "image/16/add.png";
    this.btnNuevo.VCLType = TVCLType.BS;
    this.btnNuevo.This.style.marginRight = "10px";
    this.btnNuevo.onClick = function () {
        //cargar las nuevas relaciones
        var Modal = new TVclModal(_this.ContModal.Column[0].This, null, "");
        Modal.AddHeaderContent("Add Relations");
        Modal.Width = "90%";
        //var CIRelationsList = new ItHelpCenter.CIEditor.TCIRelationsList(Modal.ModalBody, ID, function (resultado) {
        //    Modal.Hide();
        //    _this.CargarGrilla();
        //}, _this.CMDBCIDEFINERELATIONTYPE, _this.IDCMDBCI, _this.IsChild);

        var CIRelationsList = new ItHelpCenter.CIEditor.TCIRelationsList(Modal.Body.This, ID, _this.CIProfiler, _this.CMDBCIDEFINERELATIONTYPE, _this.IDCMDBCI, _this.IsChild);
        CIRelationsList.OnAfterSave = function (sender, eventArgs) {
            var ListaRows = eventArgs;

            for (var i = 0; i < ListaRows.length; i++) {
                var CMDBCIRELATION = new UsrCfg.CMDB.Properties.TCMDBCIRELATION();

                CMDBCIRELATION.IDCMDBCI_CHILD = ListaRows[i].GetCellValue("IDCMDBCI")// Convert.ToInt32(currentRow["IDCMDBCI"]);
                CMDBCIRELATION.IDCMDBCI_PARENT = _this.CIProfiler.SETCI.IDCMDBCI;
                if (_this.CMDBCIDEFINERELATIONTYPE.IDCMDBRELATIONTYPE != 0) {
                    CMDBCIRELATION.IDCMDBRELATIONTYPE = _this.CMDBCIDEFINERELATIONTYPE.IDCMDBRELATIONTYPE;
                }
                else {
                    CMDBCIRELATION.IDCMDBRELATIONTYPE = ListaRows[i].GetCellValue("IDCMDBRELATIONTYPE");
                }

                this.CIProfiler.CMDBCIRELATION_listADD(CMDBCIRELATION);
                _this.AddToGrid(CMDBCIRELATION);
            }
            Modal.CloseModal();
        }
        Modal.ShowModal();
    }

    this.btnEliminar = new TVclButton(this.ContButtons.Column[0].This, ID);
    this.btnEliminar.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnEliminar");
    this.btnEliminar.Src = "image/16/Delete.png";
    this.btnEliminar.VCLType = TVCLType.BS;
    this.btnEliminar.This.style.marginRight = "10px";
    this.btnEliminar.onClick = function () {
        //delete las relaciones seleccionadas
        var _ListRel = [];
        var cont = 0;
        for (var i = 0; i < _this.Grilla.Rows.length; i++) {
            if (_this.Grilla.Rows[i].GetCellValue("FDMA01")) {
                var _IDCMDBRELATIONTYPE;
                if (_this.CMDBCIDEFINERELATIONTYPE.IDCMDBRELATIONTYPE != 0)
                    _IDCMDBRELATIONTYPE = _this.CMDBCIDEFINERELATIONTYPE.IDCMDBRELATIONTYPE;
                else
                    _IDCMDBRELATIONTYPE = parseInt(_this.Grilla.Rows[i].GetCellValue("IDCMDBRELATIONTYPE"));

                var _TCMDBCIRELATION = {
                    IDCMDBCIRELATION: parseInt(_this.Grilla.Rows[i].GetCellValue("IDCMDBCIRELATION")),
                    IDCMDBCI_CHILD: parseInt(_this.Grilla.Rows[i].GetCellValue("IDCMDBCI_CHILD")),
                    IDCMDBCI_PARENT: parseInt(_this.IDCMDBCI),
                    IDCMDBRELATIONTYPE: _IDCMDBRELATIONTYPE
                }

                _ListRel[cont] = _TCMDBCIRELATION;
                cont += 1;
            }
        }

        $.ajax({
            type: "POST",
            url: SysCfg.App.Properties.xRaiz + 'CMDB/CIEditor/CIEditor.svc/DelRelations',
            data: JSON.stringify({ ListRel: _ListRel, IDCMDBCI: _this.IDCMDBCI, ListChild: _this.IsChild }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (response) {
                _this.CargarGrilla();
            },
            error: function (response) {
                SysCfg.Log.Methods.WriteLog("frCIEditor3.js ItHelpCenter.CIEditor.TCIRelationsEditor CMDB/CIEditor/CIEditor.svc/DelRelations" + response.ErrorMessage);
            }
        });
    }
}

ItHelpCenter.CIEditor.TCIRelationsList = function (inObjectHTML, ID, inCIProfiler, inCMDBCIDEFINERELATIONTYPE, inIDCMDBCI, IsChild) {
    var _this = this;
    this.this;
    this.CIProfiler = inCIProfiler;
    this.Grilla;
    this.CMDBCIDEFINERELATIONTYPE = inCMDBCIDEFINERELATIONTYPE;
    this.IDCMDBCI = inIDCMDBCI;
    this.IsChild = IsChild;
    this.OnAfterSave = null;

    this.ContButtons = new TVclStackPanel(inObjectHTML, ID, 1, [[12], [12], [12], [12], [12]]);
    this.ContButtons.Row.This.style.marginTop = "10px";

    this.Contenedor = new TVclStackPanel(inObjectHTML, ID, 1, [[12], [12], [12], [12], [12]]);

    this.Mythis = "TCIRelationsList";
    UsrCfg.Traslate.GetLangText(_this.Mythis, "btnGrabar", "Save");


    var Param = new SysCfg.Stream.Properties.TParam();
    Param.Inicialize();
    Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCI.IDCMDBCI.FieldName, this.CIProfiler.SETCI.IDCMDBCI, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    Param.AddInt32(UsrCfg.InternoCMDBNames.CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINERELATIONTYPE.FieldName, this.CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINERELATIONTYPE, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
    var StrOut = "";
    if (this.IsChild) {
        if (this.CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINERELATIONTYPE != 0) {
            StrOut = SysCfg.DB.SQL.Methods.GetSQLPrefixIDBuild("CMDB", "CMDBCIDEFINERELATIONTYPE_GET_TABDEFINE", Param.ToBytes()).StrSQL;
        }
        else {
            StrOut = SysCfg.DB.SQL.Methods.GetSQLPrefixIDBuild("CMDB", "CMDBCIDEFINERELATIONTYPE_GET_TABOTHER", Param.ToBytes()).StrSQL;
        }
    }
    else {
        if (this.CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINERELATIONTYPE != 0) {
            StrOut = SysCfg.DB.SQL.Methods.GetSQLPrefixIDBuild("CMDB", "CMDBCIDEFINERELATIONEXTERNALTYPE_GET_TABDEFINE", Param.ToBytes()).StrSQL;
        }
        else {
            StrOut = SysCfg.DB.SQL.Methods.GetSQLPrefixIDBuild("CMDB", "CMDBCIDEFINERELATIONEXTERNALTYPE_GET_TABOTHER", Param.ToBytes()).StrSQL;
        }
    }

    StrOut = "SELECT CAST(0 AS BIT) FDMA01,TBLMA01.* FROM (" + StrOut + ") TBLMA01"

    this.Grilla = new ItHelpCenter.CIEditor.TGrid(this.Contenedor.Column[0].This, "");

    var Open = SysCfg.DB.SQL.Methods.Open(StrOut, new Array(0));

    if (Open.ResErr.NotError) {
        this.Grilla.DataSource = Open.DataSet;
        this.Grilla.Columns[0].EnabledEditor = true;
    }

    this.btnGrabar = new TVclButton(this.ContButtons.Column[0].This, ID);
    this.btnGrabar.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "btnGrabar");
    this.btnGrabar.Src = "image/16/success.png";
    this.btnGrabar.VCLType = TVCLType.BS;
    this.btnGrabar.onClick = function () {
        //grabar las nuevas relaciones
        var ListaRows = new Array();
        for (var i = 0; i < _this.Grilla.Rows.length; i++) {
            if (_this.Grilla.Rows[i].GetCellValue("FDMA01")) {
                ListaRows.push(_this.Grilla.Rows[i]);
            }
        }
        if (_this.OnAfterSave != null)
            _this.OnAfterSave(_this, ListaRows);
    }
}
//Fin Estructura del CI_relations

ItHelpCenter.CIEditor.TCI_RELATION_DEFINETYPE = function () {
    var FDMA01;
    var CI_DATEPLANNED;
    var CI_DATEIN;
    var CI_DATEOUT;
    var CI_GENERICNAME;
    var CI_DESCRIPTION;
    var IDCMDBCI;
    var IDCMDBBRAND;
    var CI_SERIALNUMBER;
    var CI_PURCHASEDORRENTED;
    var CIDEFINE_NAME;
    var CIDEFINERELATIONTYPE_NAME;
    var IDCMDBCIDEFINE_CHILD;
    var IDCMDBCIDEFINE_PARENT;
    var IDCMDBRELATIONTYPE;
    var IDCMDBCI_CHILD;
    var IDCMDBCI_PARENT;
    var IDCMDBCIRELATION;
    var IDCMDBCIDEFINERELATIONTYPE;
}

ItHelpCenter.CIEditor.TGrid = function (inObjectHTML, ID) {
    this.OnRowClick = null;
    this._Columns = new Array();
    this._Rows = new Array();
    this._FocusedRowHandle = null;
    this._DataSource;
    this._IndexRow = -1;
    this.ResponsiveCont = Udiv(inObjectHTML, "");
    this.ResponsiveCont.classList.add("table-responsive");

    this.This = Utable(this.ResponsiveCont, ID);
    this.Header = Uthead(this.This, ID);
    this.RowHeader = Utr(this.Header, "");
    this.Body = Utbody(this.This, ID);

    //stylos para darle la vista a la grilla 
    this.This.classList.add("table");
    this.This.classList.add("table-bordered");
    this.This.classList.add("table-hover");
    this.This.style.fontSize = "12px";
    this.Body.style.cursor = "pointer";
    this._AutoTranslate = false;
    //fin de los estilos 
    Object.defineProperty(this, 'AutoTranslate', {
        get: function () {
            return this._AutoTranslate;
        },
        set: function (inValue) {
            this._AutoTranslate = inValue;
            if (inValue)
                this.TranslateColumns();
        }
    });


    Object.defineProperty(this, 'Columns', {
        get: function () {
            return this._Columns;
        },
        set: function (inValue) {
            this._Columns = inValue;
        }
    });

    Object.defineProperty(this, 'Rows', {
        get: function () {
            return this._Rows;
        },
        set: function (inValue) {
            this._Rows = inValue;
        }
    });

    Object.defineProperty(this, 'FocusedRowHandle', {
        get: function () {
            return this._FocusedRowHandle;
        },
        set: function (inValue) {
            this._FocusedRowHandle = inValue;
        }
    });

    Object.defineProperty(this, 'IndexRow', {
        get: function () {
            return this._IndexRow;
        },
        set: function (inValue) {
            this._IndexRow = inValue;
            if (inValue >= 0) {
                if (this._Rows.length > 0) {

                    if (this._FocusedRowHandle != null)
                        this._FocusedRowHandle.This.style.backgroundColor = "";
                    this._FocusedRowHandle = this._Rows[inValue];
                    this._FocusedRowHandle.This.style.backgroundColor = "lightyellow";

                }
            }
        }
    });

    Object.defineProperty(this, 'DataSource', {
        get: function () {
            return this._DataSource;
        },
        set: function (inValue) {
            this._DataSource = inValue;
            this.LoadDataSource();
        }
    });


    this.TranslateColumns = function () {
        if (this._Columns.length > 0) {
            for (var i = 0; i < this._Columns.length; i++) {
                var Column = this._Columns[i];
                if (this._AutoTranslate)
                    Column.Caption = UsrCfg.Traslate.GetLangTextdb(Column.Caption, Column.Caption);
            }
        }
    }
    this.AddColumn = function (inColumn) {
        this._Columns.push(inColumn);
        $(this.RowHeader).append(inColumn.This)
    }

    this.AddRow = function (inRow) {
        if (this._Rows.length == 0)
            this.FocusedRowHandle = inRow;
        inRow.Index = this._Rows.length;
        this._Rows.push(inRow);
        inRow.GridParent = this;
        for (var i = 0; i < this._Columns.length; i++) {
            inRow.Cells[i].Column = this._Columns[i];
            inRow.Cells[i].InitCell();
        }
        inRow.OnRowClick = this.OnRowClick;
        $(this.Body).append(inRow.This)
    }

    this.RemoveRow = function (Index) {
        this._Rows[Index].This.parentNode.removeChild(this._Rows[Index].This);
        this._Rows.splice(Index, 1);
    }

    this.RemoveFocusedRowHandle = function () {
        this._FocusedRowHandle.This.parentNode.removeChild(this._FocusedRowHandle.This);
        this._Rows.splice(this._FocusedRowHandle.Index, 1);
    }

    this.ClearAllColumns = function () {
        this._Columns.splice(0, this._Columns.length);
        $(this.RowHeader).html("");
    }

    this.ClearAllRows = function () {
        this._Rows.splice(0, this._Rows.length);
        this._FocusedRowHandle = null;
        $(this.Body).html("");
    }

    this.ClearAll = function () {
        this.ClearAllRows();
        this.ClearAllColumns();
    }

    this.LoadDataSource = function () {
        this.ClearAll();
        for (var i = 0; i < this._DataSource.FieldDefs.length; i++) {
            var Col1 = new ItHelpCenter.CIEditor.TColumn();
            Col1.Name = this._DataSource.FieldDefs[i].FieldName;
            if (this._AutoTranslate)
                Col1.Caption = UsrCfg.Traslate.GetLangTextdb(Col1.Name, Col1.Name);
            else
                Col1.Caption = this._DataSource.FieldDefs[i].FieldName;

            Col1.DataType = this._DataSource.FieldDefs[i].DataType;
            Col1.Index = i;
            Col1.GridParent = this;
            this.AddColumn(Col1);
        }

        if (this._DataSource.RecordCount > 0) {
            this._DataSource.First();

            var conta = 0;
            while (!(this._DataSource.Eof)) {
                var NewRow = new ItHelpCenter.CIEditor.TRow();

                for (var i = 0; i < this._DataSource.FieldDefs.length; i++) {
                    var CellTemp = new ItHelpCenter.CIEditor.TCell();

                    switch (this._DataSource.FieldDefs[i].DataType) {
                        case SysCfg.DB.Properties.TDataType.String:
                            CellTemp.Value = this._DataSource.RecordSet.FieldName(this._DataSource.FieldDefs[i].FieldName).asString();
                            CellTemp.IndexColumn = i;
                            CellTemp.IndexRow = conta;
                            CellTemp.Column = this.Columns[i];
                            NewRow.AddCell(CellTemp);
                            break;
                        case SysCfg.DB.Properties.TDataType.Int32:
                            CellTemp.Value = this._DataSource.RecordSet.FieldName(this._DataSource.FieldDefs[i].FieldName).asInt32();
                            CellTemp.IndexColumn = i;
                            CellTemp.IndexRow = conta;
                            CellTemp.Column = this.Columns[i];
                            NewRow.AddCell(CellTemp);
                            break;
                        case SysCfg.DB.Properties.TDataType.Boolean:
                            CellTemp.Value = this._DataSource.RecordSet.FieldName(this._DataSource.FieldDefs[i].FieldName).asBoolean();
                            CellTemp.IndexColumn = i;
                            CellTemp.IndexRow = conta;
                            CellTemp.Column = this.Columns[i];
                            NewRow.AddCell(CellTemp);
                            break;
                        case SysCfg.DB.Properties.TDataType.Text:
                            CellTemp.Value = this._DataSource.RecordSet.FieldName(this._DataSource.FieldDefs[i].FieldName).asText();
                            CellTemp.IndexColumn = i;
                            CellTemp.IndexRow = conta;
                            CellTemp.Column = this.Columns[i];
                            NewRow.AddCell(CellTemp);
                            break;
                        case SysCfg.DB.Properties.TDataType.Double:
                            CellTemp.Value = this._DataSource.RecordSet.FieldName(this._DataSource.FieldDefs[i].FieldName).asDouble();
                            CellTemp.IndexColumn = i;
                            CellTemp.IndexRow = conta;
                            CellTemp.Column = this.Columns[i];
                            NewRow.AddCell(CellTemp);
                            break;
                        case SysCfg.DB.Properties.TDataType.DateTime:
                            CellTemp.Value = this._DataSource.RecordSet.FieldName(this._DataSource.FieldDefs[i].FieldName).asDateTime();
                            CellTemp.IndexColumn = i;
                            CellTemp.IndexRow = conta;
                            CellTemp.Column = this.Columns[i];
                            NewRow.AddCell(CellTemp);
                            break;
                        case SysCfg.DB.Properties.TDataType.Object:
                            CellTemp.Value = this._DataSource.RecordSet.FieldName(this._DataSource.FieldDefs[i].FieldName).asString();
                            CellTemp.IndexColumn = i;
                            CellTemp.IndexRow = conta;
                            CellTemp.Column = this.Columns[i];
                            NewRow.AddCell(CellTemp);
                            break;
                        case SysCfg.DB.Properties.TDataType.Decimal:
                            CellTemp.Value = this._DataSource.RecordSet.FieldName(this._DataSource.FieldDefs[i].FieldName).asDouble();
                            CellTemp.IndexColumn = i;
                            CellTemp.IndexRow = conta;
                            CellTemp.Column = this.Columns[i];
                            NewRow.AddCell(CellTemp);
                            break;
                        default:
                            break;
                    }
                }
                conta = conta + 1;
                this.AddRow(NewRow);
                this._DataSource.Next();

            }
        }
    }
}

ItHelpCenter.CIEditor.TColumn = function () {
    this._Name;
    this._Index;
    this._Caption;
    this._EnabledEditor = false;
    this._DataType = SysCfg.DB.Properties.TDataType.Unknown;
    this._ColumnStyle = Componet.Properties.TExtrafields_ColumnStyle.None;
    this.This = Uth("", "");
    this.GridParent = null;

    Object.defineProperty(this, 'Name', {
        get: function () {
            return this._Name;
        },
        set: function (inValue) {
            this._Name = inValue;
        }
    });

    Object.defineProperty(this, 'Index', {
        get: function () {
            return this._Index;
        },
        set: function (inValue) {
            this._Index = inValue;
        }
    });

    Object.defineProperty(this, 'Caption', {
        get: function () {
            return this._Caption;
        },
        set: function (inValue) {
            this._Caption = inValue;
            $(this.This).append(inValue);
        }
    });

    Object.defineProperty(this, 'EnabledEditor', {
        get: function () {
            return this._EnabledEditor;
        },
        set: function (inValue) {
            this._EnabledEditor = inValue;
            if (this.GridParent != null) {
                for (var i = 0; i < this.GridParent.Rows.length; i++) {
                    this.GridParent.Rows[i].Cells[this._Index].InitCell();
                }
            }
        }
    });

    Object.defineProperty(this, 'DataType', {
        get: function () {
            return this._DataType;
        },
        set: function (inValue) {
            this._DataType = inValue;
        }
    });

    Object.defineProperty(this, 'ColumnStyle', {
        get: function () {
            return this._ColumnStyle;
        },
        set: function (inValue) {
            this._ColumnStyle = inValue;
        }
    });

}

ItHelpCenter.CIEditor.TRow = function () {
    this._Cells = Array();
    this._Index;
    this.GridParent = null;
    this.This = Utr("", "");

    Object.defineProperty(this, 'Cells', {
        get: function () {
            return this._Cells;
        },
        set: function (inValue) {
            this._Cells = inValue;
        }
    });

    Object.defineProperty(this, 'Index', {
        get: function () {
            return this._Index;
        },
        set: function (inValue) {
            this._Index = inValue;
        }
    });

    //Object.defineProperty(this, 'onclick', {
    //    get: function () {
    //        return this.This.onclick;
    //    },
    //    set: function (inValue) {
    //        var _this = this;
    //        this.This.onclick = function () {
    //            if (_this.GridParent != null) {
    //                if (_this.GridParent._FocusedRowHandle != null)
    //                    _this.GridParent._FocusedRowHandle.This.style.backgroundColor = "";
    //                _this.GridParent._FocusedRowHandle = _this;
    //                _this.GridParent._FocusedRowHandle.This.style.backgroundColor = "lightyellow";
    //            }
    //            if (_this.OnRowClick != null)
    //                _this.OnRowClick(_this.GridParent, _this);
    //        };
    //    }
    //});


    this.GetCell = function (Indexcolumn) {
        return this._Cell[Indexcolumn];
    }

    this.AddCell = function (inCell) {
        this._Cells.push(inCell);
        inCell._Row = this;
        $(this.This).append(inCell.This);
    }


    this.GetCellValue = function (NameColumn) {
        for (var i = 0; i < this._Cells.length; i++) {
            if (this._Cells[i].Column.Name == NameColumn)
                return this._Cells[i].Value;
        }
        return null;
    }

    this.SetCellValue = function (Value, NameColumn) {
        for (var i = 0; i < this._Cells.length; i++) {
            if (this._Cells[i].Column.Name == NameColumn)
                this._Cells[i].Value = Value;
        }
    }

    var _this = this;

    this.This.onclick = function () {
        if (_this.GridParent != null) {
            if (_this.GridParent._FocusedRowHandle != null)
                _this.GridParent._FocusedRowHandle.This.style.backgroundColor = "";
            _this.GridParent._FocusedRowHandle = _this;
            _this.GridParent._FocusedRowHandle.This.style.backgroundColor = "lightyellow";
        }
        if (_this.OnRowClick != null)
            _this.OnRowClick(_this.GridParent, _this);
    }
}

ItHelpCenter.CIEditor.TCell = function () {
    this._Value = "";
    this._Row;
    this._Column = null;
    this._IndexRow;
    this._IndexColumn;
    this.Control;
    this.This = Utd("", "");

    this.InitCell = function () {
        if (this._Column != null) {
            if (this._Column.EnabledEditor) {
                $(this.This).html("");
                switch (this._Column.DataType) {
                    case SysCfg.DB.Properties.TDataType.String: // SysCfg.DB.Properties.TDataType.String   
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //CREAR UN TEXTBOX
                                this.Control = new TVclTextBox(this.This, "");
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.TextPassword:
                                //CREAR UN TEXTBOX DE TIPO PASSWORD
                                this.Control = new TVclTextBox(this.This, "");
                                this.Control.Type = TImputtype.password;
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.LookUp:
                                //CREAR UN COMBOBOX 
                                this.Control = new TVclComboBox2(this.This, "");
                                this.cargarLookUp(this.Control);
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.LookUpOption:
                                //CREAR UN RADIO BUTTON
                                this.Control = new TVclInputRadioButton(this.This, "");
                                this.CargarLookUpOption(this.Control);
                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Text: //SysCfg.DB.Properties.TDataType.Text  
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //CREAR UN MEMO TEXT
                                this.Control = new TVclMemo(this.This, "");
                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Int32: // SysCfg.DB.Properties.TDataType.Int32:   
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //CREAR UN NUMERICUPDOWN
                                this.Control = new TVclTextBox(this.This, "");
                                this.Control.Type = TImputtype.number;
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.LookUp:
                                //CREAR UN COMBOBOX 
                                this.Control = new TVclComboBox2(this.This, "");
                                this.cargarLookUp(this.Control);
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.LookUpOption:
                                //CREAR UN RADIO BUTTON
                                this.Control = new TVclInputRadioButton(this.This, "");
                                this.CargarLookUpOption(this.Control);
                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Double: //SysCfg.DB.Properties.TDataType.Double:   
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //CREAR UN NUMERICUPDOWN                    
                                this.Control = new TVclTextBox(this.This, "");
                                this.Control.Type = TImputtype.number;
                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Boolean: //SysCfg.DB.Properties.TDataType.Boolean:     
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //CREAR UN CHECKBOX
                                this.Control = new TVclInputcheckbox(this.This, "");
                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.DateTime: //SysCfg.DB.Properties.TDataType.DateTime: 
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //CREAR UN DATATIME PICKER
                                this.Control = new TVclTextBox(this.This, "");
                                $(this.Control.This).datetimepicker({
                                    timeFormat: "hh:mm tt"
                                });
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.Date:
                                //CREAR UN DATE PICKER 
                                this.Control = new TVclTextBox(this.This, "");
                                $(this.Control.This).datetimepicker({
                                    showButtonPanel: true
                                });
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.Time:
                                //CREAR UN TIME PICKER
                                this.Control = new TVclTextBox(this.This, "");
                                $(this.Control.This).timepicker({
                                    timeFormat: "hh:mm tt"
                                });
                                break;
                        }
                        break
                    default:
                        break;
                }

            }
            this.SetValue(this._Value);
        }
    }

    this.SetValue = function (inValue) {
        if (this._Column != null) {
            if (this._Column.EnabledEditor) {
                switch (this._Column.DataType) {
                    case SysCfg.DB.Properties.TDataType.String: // SysCfg.DB.Properties.TDataType.String   
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //LIMPIAR UN TEXTBOX
                                this.Control.Text = inValue;
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.TextPassword:
                                //LIMPIAR UN TEXTBOX DE TIPO PASSWORD   
                                this.Control.Text = inValue;
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.LookUp:
                                //LIMPIAR UN COMBOBOX 
                                this.Control.Value = inValue;
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.LookUpOption:
                                this.Control.Value = inValue;
                                //LIMPIAR UN RADIO BUTTON
                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Text: //SysCfg.DB.Properties.TDataType.Text  
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //LIMPIAR UN MEMO TEXT
                                this.Control.Text = inValue;
                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Int32: // SysCfg.DB.Properties.TDataType.Int32:   
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //LIMPIAR UN NUMERICUPDOWN
                                this.Control.Text = inValue;
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.LookUp:
                                //LIMPIAR UN COMBOBOX 
                                this.Control.Value = inValue;
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.LookUpOption:
                                this.Control.Value = inValue;
                                //LIMPIAR UN RADIO BUTTON
                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Double: //SysCfg.DB.Properties.TDataType.Double:   
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //LIMPIAR UN NUMERICUPDOWN                    
                                this.Control.Text = inValue;
                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Boolean: //SysCfg.DB.Properties.TDataType.Boolean:     
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //LIMPIAR UN CHECKBOX
                                this.Control.This.checked = inValue;
                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.DateTime: //SysCfg.DB.Properties.TDataType.DateTime: 
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //LIMPIAR UN DATATIME PICKER
                                this.Control.Text = inValue;
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.Date:
                                //LIMPIAR UN DATE PICKER 
                                this.Control.Text = inValue;
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.Time:
                                //LIMPIAR UN TIME PICKER
                                this.Control.Text = inValue;
                                break;
                        }
                        break
                    default:
                        break;
                }
            }
            else {
                switch (this._Column.DataType) {
                    case SysCfg.DB.Properties.TDataType.String: // SysCfg.DB.Properties.TDataType.String   
                        $(this.This).html(inValue);
                        break;
                    case SysCfg.DB.Properties.TDataType.Text: //SysCfg.DB.Properties.TDataType.Text  
                        $(this.This).html(inValue);
                        break;
                    case SysCfg.DB.Properties.TDataType.Int32: // SysCfg.DB.Properties.TDataType.Int32:   
                        $(this.This).html(inValue);
                        break;
                    case SysCfg.DB.Properties.TDataType.Double: //SysCfg.DB.Properties.TDataType.Double:   
                        $(this.This).html(inValue);
                        break;
                    case SysCfg.DB.Properties.TDataType.Boolean: //SysCfg.DB.Properties.TDataType.Boolean:     
                        $(this.This).html('' + inValue);
                        break;
                    case SysCfg.DB.Properties.TDataType.DateTime: //SysCfg.DB.Properties.TDataType.DateTime: 
                        $(this.This).html(inValue);
                        break
                    default:
                        $(this.This).html(inValue);
                        break;
                }

            }
        }
        else { $(this.This).html(inValue); }
    }

    this.GetValue = function () {
        if (this._Column != null) {
            if (this._Column.EnabledEditor) {
                switch (this._Column.DataType) {
                    case SysCfg.DB.Properties.TDataType.String: // SysCfg.DB.Properties.TDataType.String   
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //LIMPIAR UN TEXTBOX
                                this._Value = this.Control.Text;
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.TextPassword:
                                //LIMPIAR UN TEXTBOX DE TIPO PASSWORD   
                                this._Value = this.Control.Text;
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.LookUp:
                                //LIMPIAR UN COMBOBOX 
                                this._Value = this.Control.Value;
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.LookUpOption:
                                //LIMPIAR UN RADIO BUTTON
                                this._Value = this.Control.Value;
                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Text: //SysCfg.DB.Properties.TDataType.Text  
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //LIMPIAR UN MEMO TEXT
                                this._Value = this.Control.Text;
                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Int32: // SysCfg.DB.Properties.TDataType.Int32:   
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //LIMPIAR UN NUMERICUPDOWN
                                this._Value = parseInt(this.Control.Text);
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.LookUp:
                                //LIMPIAR UN COMBOBOX 
                                this._Value = this.Control.Value;
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.LookUpOption:
                                //LIMPIAR UN RADIO BUTTON
                                this._Value = this.Control.Value;
                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Double: //SysCfg.DB.Properties.TDataType.Double:   
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //LIMPIAR UN NUMERICUPDOWN                    
                                this._Value = parseFloat(this.Control.Text);
                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.Boolean: //SysCfg.DB.Properties.TDataType.Boolean:     
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //LIMPIAR UN CHECKBOX
                                this._Value = this.Control.Checked;
                                break;
                        }
                        break;
                    case SysCfg.DB.Properties.TDataType.DateTime: //SysCfg.DB.Properties.TDataType.DateTime: 
                        switch (this._Column.ColumnStyle) {
                            case Componet.Properties.TExtrafields_ColumnStyle.None:
                                //LIMPIAR UN DATATIME PICKER
                                this._Value = this.Control.Text;
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.Date:
                                //LIMPIAR UN DATE PICKER 
                                this._Value = this.Control.Text;
                                break;
                            case Componet.Properties.TExtrafields_ColumnStyle.Time:
                                //LIMPIAR UN TIME PICKER
                                this._Value = this.Control.Text;
                                break;
                        }
                        break
                    default:
                        break;
                }
            }
        }
        return this._Value;
    }

    Object.defineProperty(this, 'Value', {
        get: function () {
            return this.GetValue();
        },
        set: function (inValue) {
            this._Value = inValue;
            //inValue = "" + inValue;
            //this._Value = "" + inValue;
            //$(this.This).html(inValue);
            this.SetValue(this._Value);
        }
    });

    Object.defineProperty(this, 'Row', {
        get: function () {
            return this._Row;
        },
        set: function (inValue) {
            this._Row = inValue;
        }
    });

    Object.defineProperty(this, 'Column', {
        get: function () {
            return this._Column;
        },
        set: function (inValue) {
            this._Column = inValue;
        }
    });

    Object.defineProperty(this, 'IndexRow', {
        get: function () {
            return this._IndexRow;
        },
        set: function (inValue) {
            this._IndexRow = inValue;
        }
    });

    Object.defineProperty(this, 'IndexColumn', {
        get: function () {
            return this._IndexColumn;
        },
        set: function (inValue) {
            this._IndexColumn = inValue;
        }
    });

    Object.defineProperty(this, 'Control', {
        get: function () {
            return this._Control;
        },
        set: function (inValue) {
            this._Control = inValue;
        }
    });
}