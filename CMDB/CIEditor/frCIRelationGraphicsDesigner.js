﻿ItHelpCenter.CMDB.CIEditor.frCIRelationGraphics.prototype.InitializeComponent = function () {
    var _this = this.TParent();
    _this.container = new TVclStackPanel(_this.ObjectHtml, "", 1, [[12], [12], [12], [12], [12]]);
    var containerHeader = new TVclStackPanel(_this.container.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
    var containerHeaderTitle = new TVclStackPanel(containerHeader.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
    _this.elementContainerHeaderTitle = new Uh3(containerHeaderTitle.Column[0].This, '', UsrCfg.Traslate.GetLangText(_this.Mythis, "GRAPHICATTENTIONMODEL"));
    var containerBody = new TVclStackPanel(_this.container.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
    var containerBodyButtonBox = new TVclStackPanel(containerBody.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
    containerBodyButtonBox.Column[0].This.style.marginBottom = "10px";
    _this.BtnZoomUp = new TVclImagen(containerBodyButtonBox.Column[0].This, "");
    _this.BtnZoomUp.Src = "image/24/Zoom-in.png";
    _this.BtnZoomUp.Title = "Zoom In";
    _this.BtnZoomUp.Cursor = "pointer";
    _this.BtnZoomUp.This.style.margin = "0 10px 0 0";
    _this.BtnZoomUp.onClick = function () {
        try {
            _this.BtnZoomUpClick(_this, _this.BtnZoomUp);

        } catch (e) {
            SysCfg.Log.Methods.WriteLog("frCIRelationGraphics.js ItHelpCenter.CIEditor.frCIRelationGraphics.InitializeComponent _this.BtnZoomUp.onClick()", e);
        }
    }
    _this.BtnZoomDown = new TVclImagen(containerBodyButtonBox.Column[0].This, "");
    _this.BtnZoomDown.Src = "image/24/Zoom-out.png";
    _this.BtnZoomDown.Title = "Zoom Out";
    _this.BtnZoomDown.Cursor = "pointer";
    _this.BtnZoomDown.This.style.margin = "0 10px 0 0";
    _this.BtnZoomDown.onClick = function () {
        try {
            _this.BtnZoomDownClick(_this, _this.BtnZoomDown);
        } catch (e) {
            SysCfg.Log.Methods.WriteLog("frCIRelationGraphics.js ItHelpCenter.CIEditor.frCIRelationGraphics.InitializeComponent _this.BtnZoomDown.onClick()", e);
        }
    }
    _this.BtnSavePng = new Ua(containerBodyButtonBox.Column[0].This, "", "");
    _this.BtnSavePng.style.cursor = "pointer";
    _this.ImageSavePng = new TVclImagen(_this.BtnSavePng, "");
    _this.ImageSavePng.Src = "image/24/PNG.png";
    _this.ImageSavePng.Title = "Save Image";
    $(_this.BtnSavePng).click(function () {
        try {
            _this.BtnSavePngClick(_this, _this.BtnSavePng)

        } catch (e) {
            SysCfg.Log.Methods.WriteLog("frCIRelationGraphics.js ItHelpCenter.CIEditor.frCIRelationGraphics.prototype.InitializeComponent $(_this.BtnSavePng).click()", e);
        }
    });

    var containerBodyMain = new TVclStackPanel(containerBody.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
    var graphicContainerBodyMain = new TVclStackPanel(containerBodyMain.Column[0].This, "", 2, [[10, 2], [10, 2], [10, 2], [10, 2], [10, 2]]);
    var diagramGraphicContainerBodyMain = new TVclStackPanel(graphicContainerBodyMain.Column[0].This, "", 1, [[12], [12], [12], [12], [12]]);
    var fractalGraphicContainerBodyMain = new TVclStackPanel(graphicContainerBodyMain.Column[1].This, "", 1, [[12], [12], [12], [12], [12]]);
    diagramGraphicContainerBodyMain.Column[0].This.style.overflow = "auto";
    _this.ElementDiagramGraphic = UCanvas(diagramGraphicContainerBodyMain.Column[0].This, "ElementDiagramGraphic");
    _this.ElementDiagramGraphic.width = 200;
    _this.ElementDiagramGraphic.height = 200;
    var contentOverview = new Udiv(fractalGraphicContainerBodyMain.Column[0].This, "");
    contentOverview.style.position = "absolute";
    contentOverview.style.left = "0px";
    contentOverview.style.top = "0px";
    contentOverview.style.bottom = "0px";
    contentOverview.style.width = "150px";
    contentOverview.style.height = "150px";
    contentOverview.style.border = "2px solid black";
    contentOverview.style.overflow = "hidden";
    contentOverview.style.verticalAlign = "top";
    var contentOverviewCanvas = new Udiv(contentOverview, "");
    contentOverviewCanvas.style.position = "absolute";
    contentOverviewCanvas.style.top = "0px";
    contentOverviewCanvas.style.bottom = "0px";
    contentOverviewCanvas.style.right = "0px";
    contentOverviewCanvas.style.width = "150px";
    contentOverviewCanvas.style.height = "150px";
    contentOverviewCanvas.style.borderBottom = "1px solid #e2e4e7";
    contentOverviewCanvas.style.backgroundColor = "#c0c0c0";
    _this.ElementFractalGraphic = UCanvas(contentOverviewCanvas, "ElementFractalGraphic");
    _this.ElementFractalGraphic.width = 200;
    _this.ElementFractalGraphic.height = 200;
    if ($(window).width() < 768) {
        diagramGraphicContainerBodyMain.Row.This.style.overflow = "auto";
    }
    $(window).resize(function () {
        if ($(window).width() < 768) {
            diagramGraphicContainerBodyMain.Row.This.style.overflow = "auto";
        };
    });
}