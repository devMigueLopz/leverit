﻿var TCMDBCIDEFINEEXTRATABLE_SOURCETYPE = {
    IDCMDBCI: { value: 1, name: "IDCMDBCI" }
}

StateFormulario = {
    _Insert: 0,
    _update: 1,
    _Delete: 2,
    _Nuevo: 3,
    _Cancel: 4
}

TCMDBCIMP_TYPE = {
    MODE_NORMAL: 1,
    MODE_VIRTUAL: 2,
    MODE_INCOMING: 3
}

TExtrafields_ColumnStyle =
{
    None: 0,
    TextPassword: 1,
    Date: 2,
    Time: 3,
    ProgressBar: 4,
    LookUp: 5,
    LookUpOption: 6
}

TDataType = {
    Unknown: 0,
    String: 1,
    Int32: 2,
    Boolean: 3,
    AutoInc: 4,
    Text: 5,
    Double: 6,
    DateTime: 7,
    Object: 8,
    Decimal: 9
}

TKeyType = {
    EXT: 0,
    ADN: 1,
    VAR: 2,
    NEW: 3,
    INS: 4,
    LOG: 5,
}


function GenerarChangeTexto(NombreContenedor) {
    $('#txt' + NombreContenedor + 'ParamSerch').on('keyup', function () {
        if ($(this).val().length >= 3) {
            CargarGrilla(NombreContenedor, $('#cb' + NombreContenedor + 'ColumnsSearch').val(), $(this).val(), false);
        }
    });
}

function formatearTabla() {
    $(".gridSearchCI thead").css("display", "inline-block");
    var table = $('.gridSearchCI'),
        headCells = table.find('thead tr:first').children(),
        colWidth;

    colWidth = headCells.map(function () {
        var ancho = $(this).width();
        return ancho;
    }).get();

    $(".gridSearchCI tbody").children().each(function (i, v) {
        $(v).children().each(function (j, z) {
            $(z).width(colWidth[j]);
        });
    });

    $(".gridSearchCI tbody").css("display", "inline-block");
}
function CrearGrilla(NombreContenedor) {
    var SearchCI ="";
    SearchCI += "<div class='SearchCI' style='background-color:white;'>";
    SearchCI += "<table>";
    SearchCI += "<tr><td>Show:</td></tr>";
    SearchCI += "<tr><td>";
    SearchCI += "<select id='cb" + NombreContenedor + "TypeSearch'>";
    SearchCI += "<option value='0'>Show All</option>";
    SearchCI += "<option value='1' selected>Search</option>";
    SearchCI += "</select>";
    SearchCI += "</td></tr>";
    SearchCI += "</table>";
    SearchCI += "<table id='pn" + NombreContenedor + "ColumnsSearch'>";    
    SearchCI += "<tr><td>Select Column to Search</td></tr>";
    SearchCI += "<tr><td>";
    SearchCI += "<select id='cb" + NombreContenedor + "ColumnsSearch'>";
    SearchCI += "<option value='(ALL)'>(ALL)</option>";
    SearchCI += "<option value='CI_SERIALNUMBER'>CI_SERIALNUMBER</option>";
    SearchCI += "<option value='CI_GENERICNAME'>CI_GENERICNAME</option>";
    SearchCI += "<option value='CIDEFINE_NAME'>CIDEFINE_NAME</option>";
    SearchCI += "</select>";
    SearchCI += "</td></tr>";
    SearchCI += "<tr><td>Search:</td></tr>";
    SearchCI += "<tr><td><input id='txt" + NombreContenedor + "ParamSerch' type='text' /></td></tr>";
    SearchCI += "</table>";
    SearchCI += "<table class='gridSearchCI'>";
    SearchCI += "<thead>";
    SearchCI += "<tr>";
    SearchCI += "<th>IDCMDBCI</th>";
    SearchCI += "<th>CI_SERIALNUMBER</th>";
    SearchCI += "<th>CI_GENERICNAME</th>";
    SearchCI += "<th>CIDEFINE_NAME</th>";
    SearchCI += "</tr>";
    SearchCI += "</thead>";
    SearchCI += "<tbody>";
    SearchCI += "</tbody>";
    SearchCI += "</table>";
    //SearchCI += "<input type='button' id='btnProbar' name='btnProbar' value='probar' />";
    SearchCI += "</div>";
    $("#" + NombreContenedor).append(SearchCI);
    GenerarChangeCombo(NombreContenedor);
    GenerarChangeTexto(NombreContenedor);
    
    
}
function CargarGrilla(NombreContenedor, Paramcolumna, ParamValue, showAll) {
    //////////carga los cis///////////// 
    $.ajax({
        type: "POST",
        url: 'CIEditor.svc/GetCIS',
        data: JSON.stringify({ Paramcolumna: Paramcolumna, ParamValue: ParamValue, ShowAll: showAll }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (response) {
            $("#" + NombreContenedor + " .gridSearchCI tbody").html('');
            for (var i = 0; i < response.length; i++) {
                var nuevaFila = "<tr data-IdCI = '" + response[i].IDCMDBCI + "' data-CtrCIEditor = '" + NombreContenedor + "'>";
                nuevaFila += "<td>" + response[i].IDCMDBCI + "</td>";
                nuevaFila += "<td>" + response[i].CI_SERIALNUMBER + "</td>";
                nuevaFila += "<td>" + response[i].CI_GENERICNAME + "</td>";
                nuevaFila += "<td>" + response[i].CIDEFINE_NAME + "</td>";
                nuevaFila += "</tr>";
                $("#" + NombreContenedor + " .gridSearchCI tbody").append(nuevaFila);
            }
            formatearTabla();
        },
        error: function (response) {
            alert(response.ErrorMessage);
        }
    });
    //////////////////////////////
}
function RegistrarEventosGrilla(NombreContenedor) {
    var rowOld;
    ///carga el ci elegido 
    $("#" + NombreContenedor + " .gridSearchCI tbody").on('click', 'tr', function (e) {
        e.preventDefault();
        var IDCMDBCI = $(this).attr('data-IdCI');
        $(rowOld).removeClass('rowSelect');
        $(this).addClass('rowSelect');
        rowOld = this;

        $('#' + NombreContenedor + 'tabPrincipal #' + NombreContenedor + 'tabPrincipalLU.tabs li:first').click();
        $('#' + NombreContenedor + 'tabSecundario #' + NombreContenedor + 'tabSecundarioLU.tabs li:first').click();

        GetCMDBCI(IDCMDBCI, NombreContenedor);
        GetCMDBExtraTables(IDCMDBCI, NombreContenedor);
        GetRelationsChilds(IDCMDBCI, NombreContenedor);
        GetRelationsParents(IDCMDBCI, NombreContenedor);
        //GetRelationsChilds(IDCMDBCI);
        //$("#" + NombreContenedor + "tabBasicoLi").click();
        
    });
}
function CrearCIEditor(NombreContenedor) {
    var CIEditor = "";
    CIEditor += "<div class='CIEditor' style='background-color:white;'>";
    CIEditor += "<div id='" + NombreContenedor + "Head'>";
    CIEditor += "<div id='" + NombreContenedor + "contBotones' style='margin-top:10px; margin-bottom:10px; float:right;'>";
    CIEditor += "<input id='" + NombreContenedor + "btnNuevo' value='New' type='button' />";
    CIEditor += "<input id='" + NombreContenedor + "btnGuardar' value='Save' type='button' />";
    CIEditor += "<input id='" + NombreContenedor + "btnEliminar' value='Delete' type='button' />";
    CIEditor += "<input id='" + NombreContenedor + "btnCancelar' value='Cancel' type='button' />";
    CIEditor += "</div>";
    CIEditor += "</div>";
    CIEditor += "<div class='clr'></div>"
    CIEditor += "<section class='wrapper' id='" + NombreContenedor + "tabPrincipal'>";
    CIEditor += CrearModalRelations(NombreContenedor);
    CIEditor += "<ul class='tabs' id='" + NombreContenedor + "tabPrincipalLU'>";
    CIEditor += "<li><a href='#" + NombreContenedor + "tabCIInformation'>CI Information</a></li>";
    CIEditor += "<li><a href='#" + NombreContenedor + "tabOutputRelation'>Output Relation</a></li>";
    CIEditor += "<li><a href='#" + NombreContenedor + "tabInputRelation'>Input Relation</a></li>";
    CIEditor += "</ul>";
    CIEditor += "<div class='clr'></div>";
    CIEditor += "<section class='block' id='" + NombreContenedor + "blockPrincipal'>";
    CIEditor += "<article id='" + NombreContenedor + "tabCIInformation' class='articlePrincipal'>";
    CIEditor += CrearTabPageCIInformation(NombreContenedor);
    CIEditor += "</article>";
    CIEditor += "<article id='" + NombreContenedor + "tabOutputRelation' class='articlePrincipal'>";
    CIEditor += CrearTabPageOR(NombreContenedor);
    CIEditor += "</article>";
    CIEditor += "<article id='" + NombreContenedor + "tabInputRelation' class='articlePrincipal'>";
    CIEditor += CrearTabPageIR(NombreContenedor);
    CIEditor += "</article>";
    CIEditor += "</section>";
    CIEditor += "</section>";
    CIEditor += "</div>";
    $("#" + NombreContenedor).append(CIEditor);
    GenerarclickBtnPrincipales(NombreContenedor);
    EnabledControlesPrincipales(NombreContenedor, false);
    EnabledBotonesPrincipales(NombreContenedor, true, false, false, false);

}
function CrearTabPageCIInformation(NombreContenedor) {
    var TabPageCIInformation = "";
    TabPageCIInformation += "<section class='wrapper' id='" + NombreContenedor + "tabSecundario' style='margin-top: 5px;'>";
    TabPageCIInformation += "<input type='hidden' id='" + NombreContenedor + "stateFormPrincipal' value='" + StateFormulario._Nuevo + "'>";
    TabPageCIInformation += "<input type='hidden' id='" + NombreContenedor + "IDCMDBCI' value='0'>";
    TabPageCIInformation += "<ul class='tabs' id='" + NombreContenedor + "tabSecundarioLU'>";
    TabPageCIInformation += "<li id='" + NombreContenedor + "tabBasicoLi'><a href='#" + NombreContenedor + "tabBasic'>BASIC</a></li>";
    TabPageCIInformation += "</ul>";
    TabPageCIInformation += "<div class='clr'></div>";
    TabPageCIInformation += "<section class='block' id='" + NombreContenedor + "blockSecundario'>";
    TabPageCIInformation += "<article id='" + NombreContenedor + "tabBasic' class='articleSecundario'>";
    TabPageCIInformation += "<table>";
    TabPageCIInformation += "<tr>";
    TabPageCIInformation += "<td>N° Serial</td>";
    TabPageCIInformation += "<td>";
    TabPageCIInformation += "<input id='" + NombreContenedor + "txtSerial' type='text' value='' /></td>";
    TabPageCIInformation += "</tr>";
    TabPageCIInformation += "<tr>";
    TabPageCIInformation += "<td>Name</td>";
    TabPageCIInformation += "<td>";
    TabPageCIInformation += "<input id='" + NombreContenedor + "txtName' type='text' value=''  /></td>";
    TabPageCIInformation += "</tr>";
    TabPageCIInformation += "<tr>";
    TabPageCIInformation += "<td>CI Define</td>";
    TabPageCIInformation += "<td>";
    //TabPageCIInformation += "<input id='" + NombreContenedor + "txtCIDefine' type='text' /></td>";
    //TabPageCIInformation += "<select id='" + NombreContenedor + "txtCIDefine'>"
    TabPageCIInformation += CargarCIDefine(NombreContenedor);
    //TabPageCIInformation += "</select> </td>";
    TabPageCIInformation += "</td>";
    TabPageCIInformation += "</tr>";
    TabPageCIInformation += "<tr>";
    TabPageCIInformation += "<td>Acquired Date</td>";
    TabPageCIInformation += "<td>";
    TabPageCIInformation += "<input id='" + NombreContenedor + "txtAcquiredDate' type='text' value=''  /></td>";
    TabPageCIInformation += "</tr>";
    TabPageCIInformation += "<tr>";
    TabPageCIInformation += "<td>State</td>";
    TabPageCIInformation += "<td>";
    TabPageCIInformation += CargarCIState(NombreContenedor);
    TabPageCIInformation += "</td>";
    TabPageCIInformation += "</tr>";
    TabPageCIInformation += "<tr>";
    TabPageCIInformation += "<td>Dispose Date</td>";
    TabPageCIInformation += "<td>";
    TabPageCIInformation += "<input id='" + NombreContenedor + "txtDisposeDate' type='text' value=''/></td>";
    TabPageCIInformation += "</tr>";
    TabPageCIInformation += "<tr>";
    TabPageCIInformation += "<td>Brand</td>";
    TabPageCIInformation += "<td>";
    TabPageCIInformation += CargarCIBrand(NombreContenedor);
    TabPageCIInformation += "</td>";
    TabPageCIInformation += "</tr>";
    TabPageCIInformation += "<tr>";
    TabPageCIInformation += "<td>Other Details</td>";
    TabPageCIInformation += "<td>";
    //TabPageCIInformation += "<input id='" + NombreContenedor + "txtOtherDetails' type='text' /></td>";
    TabPageCIInformation += "<textarea id='" + NombreContenedor + "txtOtherDetails' rows='4' cols='50'>";
    TabPageCIInformation += "</textarea>";
    TabPageCIInformation += "</td>";
    TabPageCIInformation += "</tr>";
    TabPageCIInformation += "</table>";
    TabPageCIInformation += "</article>";
    TabPageCIInformation += "</section>";
    TabPageCIInformation += "</section>";
    return TabPageCIInformation;
}
function GenerarTabsClicksPrimarios(NombreContenedor) {
    ///tabs para el editor 
    $('#' + NombreContenedor + 'tabPrincipal #' + NombreContenedor + 'tabPrincipalLU.tabs li:first').addClass('active');
    $('#' + NombreContenedor + 'tabPrincipal #' + NombreContenedor + 'blockPrincipal.block .articlePrincipal').hide();
    $('#' + NombreContenedor + 'tabPrincipal #' + NombreContenedor + 'blockPrincipal.block article:first').show();
    $('#' + NombreContenedor + 'tabPrincipal #' + NombreContenedor + 'tabPrincipalLU.tabs').on('click', 'li', function () {
        $('#' + NombreContenedor + 'tabPrincipal #' + NombreContenedor + 'tabPrincipalLU.tabs li').removeClass('active');
        $(this).addClass('active')
        $('#' + NombreContenedor + 'tabPrincipal #' + NombreContenedor + 'blockPrincipal.block .articlePrincipal').hide();
        var activeTab = $(this).find('a').attr('href');
        $(activeTab).show();
        return false;
    });


}
function GenerarTabsClicksSecundarios(NombreContenedor) {
    $('#' + NombreContenedor + 'tabSecundario #' + NombreContenedor + 'tabSecundarioLU.tabs li:first').addClass('active');
    $('#' + NombreContenedor + 'tabSecundario #' + NombreContenedor + 'blockSecundario.block .articleSecundario').hide();
    $('#' + NombreContenedor + 'tabSecundario #' + NombreContenedor + 'blockSecundario.block article:first').show();
    $('#' + NombreContenedor + 'tabSecundario #' + NombreContenedor + 'tabSecundarioLU.tabs').on('click', 'li', function () {
        $('#' + NombreContenedor + 'tabSecundario #' + NombreContenedor + 'tabSecundarioLU.tabs li').removeClass('active');
        $(this).addClass('active')
        $('#' + NombreContenedor + 'tabSecundario #' + NombreContenedor + 'blockSecundario.block .articleSecundario').hide();
        var activeTab = $(this).find('a').attr('href');
        $(activeTab).show();
        return false;
    });
}
function GenerarChangeCombo(NombreContenedor) {
    $('#cb' + NombreContenedor + 'TypeSearch').on('change', function () {
        if (parseInt($(this).val()) == 0)
        {
            $("#pn" + NombreContenedor + "ColumnsSearch").css('display', 'none');
            CargarGrilla(NombreContenedor, '', '', true);
        }
        else
        {
            $("#pn" + NombreContenedor + "ColumnsSearch").css('display', 'block');
        }
    });
}

function GenerarChangeTexto(NombreContenedor) {
    $('#txt' + NombreContenedor + 'ParamSerch').on('keyup', function () {
        if ($(this).val().length >= 3) {
            CargarGrilla(NombreContenedor, $('#cb' + NombreContenedor + 'ColumnsSearch').val(), $(this).val(), false);
        }
    });
}

function GetCMDBCI(IDCMDBCI, NombreContenedor) {
    var data = {
        'IDCMDBCI': IDCMDBCI
    };

    $.ajax({
        type: "POST",
        url: 'CIEditor.svc/GetCI',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {;
            EnabledControlesPrincipales(NombreContenedor, true);
            EnabledBotonesPrincipales(NombreContenedor, true, true, false, true);
            $("#" + NombreContenedor + "stateFormPrincipal").val(StateFormulario._update);
            $('#' + NombreContenedor + 'IDCMDBCI').val(response.IDCMDBCI);
            $('#' + NombreContenedor + 'txtSerial').val(response.CI_SERIALNUMBER);
            $('#' + NombreContenedor + 'txtName').val(response.CI_GENERICNAME);
            $('#' + NombreContenedor + 'cbCIDefine').val(response.IDCMDBCIDEFINE);
            $('#' + NombreContenedor + 'txtAcquiredDate').val(formatJSONDateTime(response.CI_DATEIN));
            $('#' + NombreContenedor + 'cbCIState').val(response.IDCMDBCISTATE);
            $('#' + NombreContenedor + 'txtDisposeDate').val(formatJSONDateTime(response.CI_DATEOUT));
            $('#' + NombreContenedor + 'cbCIBrand').val(response.CMDBBRAND.IDCMDBBRAND);
            $('#' + NombreContenedor + 'txtOtherDetails').html("");
            $('#' + NombreContenedor + 'txtOtherDetails').append(response.CI_DESCRIPTION);
        },
        error: function (response) {
            alert(response.ErrorMessage);
        }
    });
}
function GetCMDBExtraTables(IDCMDBCI, NombreContenedor) {
    var data = {
        'IDCMDBCI': IDCMDBCI
    };

    $.ajax({
        type: "POST",
        url: 'CIEditor.svc/GetExtraTables',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#' + NombreContenedor + 'tabSecundarioLU li').remove(".tabDinamic");
            $('#' + NombreContenedor + 'blockSecundario article').remove(".Articulodinamico");
            for (var i = 0; i < response.length; i++) {
                $('#' + NombreContenedor + 'tabSecundarioLU').append("<li class='tabDinamic'><a href='#" + NombreContenedor + "tab" + response[i].IDCMDBCIDEFINEEXTRATABLE + "'>" + response[i].EXTRATABLE_NAME + "</a></li>");
                GetCMDBExtraFields(response[i], IDCMDBCI, NombreContenedor);
            }

            $('.cmdb-datetime').datetimepicker({
                timeFormat: "hh:mm tt"
            });

            $('.cmdb-date').datepicker({ showButtonPanel: true });

            $('.cmdb-time').timepicker({
                timeFormat: "hh:mm tt"
            });

        },
        error: function (response) {

            alert(response.ErrorMessage);
        }
    });
}



function GetTCMDBCIDEFINEEXTRATABLE_SOURCETYPE(Id) {
    var Res = "IDCMDBCI" ;
    switch (Id) {
        case 1:
            Res = TCMDBCIDEFINEEXTRATABLE_SOURCETYPE.IDCMDBCI.name;
            break;
        default:
    } 
    return Res;
}
//var IDSOURCE = GetTCMDBCIDEFINEEXTRATABLE_SOURCETYPE(CMDBCIDEFINEEXTRATABLE.EXTRATABLE_IDSOURCE);
//var EXTRATABLE_LOG = CMDBCIDEFINEEXTRATABLE.EXTRATABLE_LOG; //BLLOGRev07

function GetCMDBExtraFields(CMDBCIDEFINEEXTRATABLE, IDCMDBCI, NombreContenedor) {
    var data = {
        'IDCMDBCIDEFINEEXTRATABLE': CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE,
        'IDCMDBCI': IDCMDBCI
    };
    $.ajax({
        type: "POST",
        url: 'CIEditor.svc/GetExtraFields',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async:false,
        success: function (response) {          

            var ContenedorET = "<article id='"+ NombreContenedor + "tab" + CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE + "' class='articleSecundario Articulodinamico' style='display: none;'>";
            ContenedorET += "<table>";
            if (CMDBCIDEFINEEXTRATABLE.CMDBCIMP_TYPE != TCMDBCIMP_TYPE.MODE_VIRTUAL) {
                 
                ContenedorET += "<tr>";
                ContenedorET += "<td>IDCMDB_EF" + CMDBCIDEFINEEXTRATABLE.EXTRATABLE_NAMETABLE + "</td>";
                ContenedorET += "<td><input id='" + NombreContenedor + "FieldExtraTable" + CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE + "' data-NameField='IDCMDB_EF" + CMDBCIDEFINEEXTRATABLE.EXTRATABLE_NAMETABLE + "' data-DataType='" + TDataType.Int32 + "' type='text'  disabled /></td>";
                ContenedorET += "</tr>";

                
                ContenedorET += "<tr>";
                ContenedorET += "<td>IDCMDBCI</td>";
                ContenedorET += "<td><input id='" + NombreContenedor + "FieldIDCMDBCI" + CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE + "' data-NameField='IDCMDBCI' data-DataType='" + TDataType.Int32 + "' type='text'  disabled/></td>";
                ContenedorET += "</tr>";
            }
            for (var i = 0; i < response.length; i++) {
                ContenedorET += "<tr>";
                ContenedorET += "<td>" + response[i].EXTRAFIELDS_NAME + "</td>";
                ContenedorET += "<td>" + ConstruirControlCMDB(response[i], CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE, NombreContenedor) + "</td>";
                ContenedorET += "</tr>";
            }
            ContenedorET += "<tr>";
            ContenedorET += "<td></td>";
            ContenedorET += "<td><input class='btnNuevoET' data-IDCI='" + IDCMDBCI + "' data-IdET='" + CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE + "' value='New' type='button'>";
            ContenedorET += "<input class='btnGrabarET' data-IDCI='" + IDCMDBCI + "' data-IdET='" + CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE + "' value='Save' type='button'>";
            ContenedorET += "<input class='btnEliminarET' data-IDCI='" + IDCMDBCI + "' data-IdET='" + CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE + "' value='Delete' type='button'>";
            ContenedorET += "<input class='btnCancelarET' data-IDCI='" + IDCMDBCI + "' data-IdET='" + CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE + "' value='Cancel' type='button'></td>";
            ContenedorET += "</tr>";
            ContenedorET += "</table>";
            ContenedorET += "<article id='" + NombreContenedor + "ArtiFields" + CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE + "' style='padding: 0px;'>";
            ContenedorET += "</article>";
            ContenedorET += "</article>";

            $('#' + NombreContenedor + 'blockSecundario').append(ContenedorET);
            
            

            ROWCMDBEXTRATABLE_GET(CMDBCIDEFINEEXTRATABLE, response, IDCMDBCI, NombreContenedor);
        },
        error: function (response) {
            alert(response.ErrorMessage);
        }
    });

}
function ConstruirControlCMDB(CMDBCIDEFINEEXTRAFIELDS, IDCMDBCIDEFINEEXTRATABLE, NombreContenedor) {
    //if (CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE == 5) {

    //    var data = {
    //        'IDCMDBCIDEFINEEXTRATABLE': IDCMDBCIDEFINEEXTRATABLE,
    //        'IDCMDBCIDEFINEEXTRAFIELDS': CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS
    //    };

    //    var textito;
    //    $.ajax({
    //        type: "POST",
    //        url: 'CIEditor.svc/GetComboExtraField',
    //        data: JSON.stringify(data),
    //        contentType: "application/json; charset=utf-8",
    //        dataType: "json",
    //        async: false,
    //        success: function (response) {
    //            var combo = "<select id='" + NombreContenedor + "Field" + NombreContenedor + CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS + "' data-NameField='" + CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_NAME + "' data-DataType='" + CMDBCIDEFINEEXTRAFIELDS.IDCMDBDBDATATYPES + "'>";
    //            for (var i = 0; i < response.length; i++) {
    //                combo += "<option value='" + response[i].ValueMember + "'>" + response[i].DisplayMember + "</option>";
    //            }
    //            combo += "</select>";
    //            textito = combo;
    //        },
    //        error: function (response) {
    //            alert(response.ErrorMessage);
    //        }
    //    });

    //    return textito;
    //}
    //else {
        switch (CMDBCIDEFINEEXTRAFIELDS.IDCMDBDBDATATYPES) {
            case TDataType.String: // SysCfg.DB.Properties.TDataType.String   
                switch (CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case TExtrafields_ColumnStyle.None:
                        return "<input id='" + NombreContenedor + "Field" + NombreContenedor + CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS + "' data-Columnstyle='" + TExtrafields_ColumnStyle.None + "' data-NameField='" + CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_NAME + "' data-DataType='" + CMDBCIDEFINEEXTRAFIELDS.IDCMDBDBDATATYPES + "' type='text' />";
                        break;
                    case TExtrafields_ColumnStyle.TextPassword:
                        return "<input id='" + NombreContenedor + "Field" + NombreContenedor + CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS + "' data-Columnstyle='" + TExtrafields_ColumnStyle.TextPassword + "' data-NameField='" + CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_NAME + "' data-DataType='" + CMDBCIDEFINEEXTRAFIELDS.IDCMDBDBDATATYPES + "' type='password' />";
                        break;
                    case TExtrafields_ColumnStyle.LookUp:
                        return ConstruirLookUp(CMDBCIDEFINEEXTRAFIELDS, IDCMDBCIDEFINEEXTRATABLE, NombreContenedor);
                        break;
                    case TExtrafields_ColumnStyle.LookUpOption:
                        return ConstruirLookUpOption(CMDBCIDEFINEEXTRAFIELDS, IDCMDBCIDEFINEEXTRATABLE, NombreContenedor);
                        break;
                }                
                break;
            case TDataType.Text: //SysCfg.DB.Properties.TDataType.Text     
                switch (CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case TExtrafields_ColumnStyle.None:
                        return "<textarea id='" + NombreContenedor + "Field" + NombreContenedor + CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS + "' data-Columnstyle='" + TExtrafields_ColumnStyle.None + "' data-NameField='" + CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_NAME + "' data-DataType='" + CMDBCIDEFINEEXTRAFIELDS.IDCMDBDBDATATYPES + "' rows='10' cols='50'></textarea>";
                        break;
                }
                break;
            case TDataType.Int32: // SysCfg.DB.Properties.TDataType.Int32:    
                switch (CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case TExtrafields_ColumnStyle.None:
                        return "<input id='" + NombreContenedor + "Field" + NombreContenedor + CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS + "' data-Columnstyle='" + TExtrafields_ColumnStyle.None + "' data-NameField='" + CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_NAME + "' data-DataType='" + CMDBCIDEFINEEXTRAFIELDS.IDCMDBDBDATATYPES + "' type='number' step='1' />";
                        break;
                    case TExtrafields_ColumnStyle.LookUp:
                        return ConstruirLookUp(CMDBCIDEFINEEXTRAFIELDS, IDCMDBCIDEFINEEXTRATABLE, NombreContenedor);
                        break;
                    case TExtrafields_ColumnStyle.LookUpOption:
                        return ConstruirLookUpOption(CMDBCIDEFINEEXTRAFIELDS, IDCMDBCIDEFINEEXTRATABLE, NombreContenedor);
                        break;
                }
                
                break;
            case TDataType.Double: //SysCfg.DB.Properties.TDataType.Double:  
                switch (CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case TExtrafields_ColumnStyle.None:
                        return "<input id='" + NombreContenedor + "Field" + NombreContenedor + CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS + "' data-Columnstyle='" + TExtrafields_ColumnStyle.None + "' data-NameField='" + CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_NAME + "' data-DataType='" + CMDBCIDEFINEEXTRAFIELDS.IDCMDBDBDATATYPES + "' type='number' step='1' />";
                        break;
                }                
                break;
            case TDataType.Boolean: //SysCfg.DB.Properties.TDataType.Boolean: 
                switch (CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case TExtrafields_ColumnStyle.None:
                        return "<input id='" + NombreContenedor + "Field" + NombreContenedor + CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS + "' data-Columnstyle='" + TExtrafields_ColumnStyle.None + "' data-NameField='" + CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_NAME + "' data-DataType='" + CMDBCIDEFINEEXTRAFIELDS.IDCMDBDBDATATYPES + "' type='checkbox'/>";
                        break;
                }                
                break;
            case TDataType.DateTime: //SysCfg.DB.Properties.TDataType.DateTime:   
                switch (CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                    case TExtrafields_ColumnStyle.None:
                        return  "<input id='" + NombreContenedor + "Field" + NombreContenedor + CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS + "' data-Columnstyle='" + TExtrafields_ColumnStyle.None + "' data-NameField='" + CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_NAME + "' data-DataType='" + CMDBCIDEFINEEXTRAFIELDS.IDCMDBDBDATATYPES + "' class='cmdb-datetime' type='datetime' />";
                        break;
                    case TExtrafields_ColumnStyle.Date:
                        return "<input id='" + NombreContenedor + "Field" + NombreContenedor + CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS + "' data-Columnstyle='" + TExtrafields_ColumnStyle.Date + "' data-NameField='" + CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_NAME + "' data-DataType='" + CMDBCIDEFINEEXTRAFIELDS.IDCMDBDBDATATYPES + "' class='cmdb-date' type='date' />";
                        break;
                    case TExtrafields_ColumnStyle.Time:
                        return "<input id='" + NombreContenedor + "Field" + NombreContenedor + CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS + "' data-Columnstyle='" + TExtrafields_ColumnStyle.Time + "' data-NameField='" + CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_NAME + "' data-DataType='" + CMDBCIDEFINEEXTRAFIELDS.IDCMDBDBDATATYPES + "' class='cmdb-time' type='time' />";
                        break;
                }                
                break;
            default:
                break;
        }
    //}
}

function ConstruirLookUp(CMDBCIDEFINEEXTRAFIELDS, IDCMDBCIDEFINEEXTRATABLE, NombreContenedor) {
    var data = {
        'IDCMDBCIDEFINEEXTRATABLE': IDCMDBCIDEFINEEXTRATABLE,
        'IDCMDBCIDEFINEEXTRAFIELDS': CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS
    };

    var textito;
    $.ajax({
        type: "POST",
        url: 'CIEditor.svc/GetComboExtraField',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (response) {
            var combo = "<select id='" + NombreContenedor + "Field" + NombreContenedor + CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS + "' data-Columnstyle='" + TExtrafields_ColumnStyle.LookUp + "' data-NameField='" + CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_NAME + "' data-DataType='" + CMDBCIDEFINEEXTRAFIELDS.IDCMDBDBDATATYPES + "'>";
            for (var i = 0; i < response.length; i++) {
                combo += "<option value='" + response[i].ValueMember + "'>" + response[i].DisplayMember + "</option>";
            }
            combo += "</select>";
            textito = combo;
        },
        error: function (response) {
            alert(response.ErrorMessage);
        }
    });

    return textito;
}

function ConstruirLookUpOption(CMDBCIDEFINEEXTRAFIELDS, IDCMDBCIDEFINEEXTRATABLE, NombreContenedor) {
    var data = {
        'IDCMDBCIDEFINEEXTRATABLE': IDCMDBCIDEFINEEXTRATABLE,
        'IDCMDBCIDEFINEEXTRAFIELDS': CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS
    };

    var textito;
    $.ajax({
        type: "POST",
        url: 'CIEditor.svc/GetComboExtraField',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (response) {
            var combo = ""; //"<input name='" + NombreContenedor + "Field" + NombreContenedor + CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS + "' id='" + NombreContenedor + "Field" + NombreContenedor + CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS + "' data-NameField='" + CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_NAME + "' data-DataType='" + CMDBCIDEFINEEXTRAFIELDS.IDCMDBDBDATATYPES + "'>";
            for (var i = 0; i < response.length; i++) {
                combo += "<input type='radio' name='" + NombreContenedor + "Field" + NombreContenedor + CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS + "' id='" + NombreContenedor + "Field" + NombreContenedor + CMDBCIDEFINEEXTRAFIELDS.IDCMDBCIDEFINEEXTRAFIELDS + "' data-Columnstyle='" + TExtrafields_ColumnStyle.LookUpOption + "' data-NameField='" + CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_NAME + "' data-DataType='" + CMDBCIDEFINEEXTRAFIELDS.IDCMDBDBDATATYPES + "' value='" + response[i].ValueMember + "'>"
                combo += response[i].DisplayMember;
            }
            //combo += "</select>";
            textito = combo;
        },
        error: function (response) {
            alert(response.ErrorMessage);
        }
    });

    return textito;
}

function ROWCMDBEXTRATABLE_GET(CMDBCIDEFINEEXTRATABLE, CMDBCIDEFINEEXTRAFIELDList, IDCMDBCI, NombreContenedor) {
    var data = {
        'IDCMDBCIDEFINEEXTRATABLE': CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE,
        'IDCMDBCI': IDCMDBCI
    };

    $.ajax({
        type: "POST",
        url: 'CIEditor.svc/ROWEXTRATABLE_GET',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //$('#ArtiFields' + CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE).remove("#GridFields" + CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE);
            $('#ArtiFields' + CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE).html("");
            var ContenedorET = "<table id='" + NombreContenedor + "GridFields" + CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE + "' class='GridFieldStyle'>";
            ContenedorET += "<thead>";
            ContenedorET += "<tr>";
            for (var i = 0; i < response.Columnas.length; i++) {
                ContenedorET += "<th id='" + NombreContenedor + response.Columnas[i].IDCMDBCIDEFINEEXTRAFIELDS + "' data-IdEF='" + response.Columnas[i].IDCMDBCIDEFINEEXTRAFIELDS + "' data-IdET='" + CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE + "'>" + response.Columnas[i].NombreColumna + "</th>";
            }
            ContenedorET += "</tr>";
            ContenedorET += "<tbody>";
            for (var i = 0; i < response.Filas.length; i++) {
                ContenedorET += "<tr class='trExtraField'>";
                for (var j = 0; j < response.Filas[i].Celdas.length; j++) {
                    ContenedorET += "<td>" + response.Filas[i].Celdas[j].Valor + "</td>";
                }
                ContenedorET += "</tr>";
            }
            ContenedorET += "</tbody>";
            ContenedorET += "</table>";
            $('#' + NombreContenedor + 'ArtiFields' + CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE).append(ContenedorET);
            var NroADN = ContarADN(CMDBCIDEFINEEXTRAFIELDList);
            $('#' + NombreContenedor + 'ArtiFields' + CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE).append("<input type='hidden' id='" + NombreContenedor + "NroADN" + CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE + "' value='" + NroADN + "'>");
            $('#' + NombreContenedor + 'ArtiFields' + CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE).append("<input type='hidden' id='" + NombreContenedor + "stateForm" + CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE + "' value='" + StateFormulario._Nuevo + "'>");
            if (!(NroADN > 0)) {
                var stateForm = ClickPrimerRowCMDB("#" + NombreContenedor + "GridFields" + CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE, NombreContenedor);
                $('#' + NombreContenedor + 'stateForm' + CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE).val(stateForm);
                $('#' + NombreContenedor + 'ArtiFields' + CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE).css('display', 'none');
                $("#" + NombreContenedor + "tab" + CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE + ' .btnNuevoET').css('display', 'none');
                $("#" + NombreContenedor + "tab" + CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE + ' .btnCancelarET').css('display', 'none');
                HabilitaBotonesCMDB(false, true, true, false, CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE);
                HabilitaControlesCMDB(CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE, true)
            }
            else {
                $('#' + NombreContenedor + 'stateForm' + CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE).val(StateFormulario._Cancel);
                HabilitaBotonesCMDB(true, false, false, false, CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE);
                HabilitaControlesCMDB(CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE, false)
            }
        },
        error: function (response) {
            alert(response.ErrorMessage);
        }
    });
}
function HabilitaBotonesCMDB(_New, _Save, _Delete, _Cancel, IdET, NombreContenedor) {
    $('#' + NombreContenedor + 'tab' + IdET + ' .btnNuevoET').prop('disabled', !_New);
    $('#' + NombreContenedor + 'tab' + IdET + ' .btnGrabarET').prop('disabled', !_Save);
    $('#' + NombreContenedor + 'tab' + IdET + ' .btnEliminarET').prop('disabled', !_Delete);
    $('#' + NombreContenedor + 'tab' + IdET + ' .btnCancelarET').prop('disabled', !_Cancel);

}
function HabilitaControlesCMDB(IdET, Estado, NombreContenedor) {
    var Controles = $("#" + NombreContenedor + "tab" + IdET + " [data-NameField]");
    for (var i = 0; i < Controles.length; i++) {
        FieldExtraTable = NombreContenedor +  "FieldExtraTable" + IdET;
        FieldIDCMDBCI = NombreContenedor + "FieldIDCMDBCI" + IdET;
        if ($(Controles[i]).attr('id') != FieldExtraTable && $(Controles[i]).attr('id') != FieldIDCMDBCI) {
            $(Controles[i]).prop("disabled", !Estado);
        }
    }
}
function LimpiarControlesCDMB(IdET, NombreContenedor) {
    var Controles = $("#" + NombreContenedor + "tab" + IdET + " [data-NameField]");
    for (var i = 0; i < Controles.length; i++) {
        $(Controles[i]).val("");
    }
}
function ClickPrimerRowCMDB(tabla, NombreContenedor) {
    var row = $(tabla + " tbody").children('tr:first');

    if (row.length > 0) {
        var columnas = $("#" + $($(row).parent().parent()).attr("id") + " thead").children(':eq(' + $(row).index() + ')').children();

        if (columnas.length > 0) {
            var idET = $(columnas[0]).attr("data-IdET");
            var IdcontrolET = "#" + NombreContenedor + "FieldExtraTable" + idET;
            var valorCeldaET = $(row).children()[1].outerText;
            $(IdcontrolET).val(valorCeldaET);

            var IdcontrolCI = "#" + NombreContenedor + "FieldIDCMDBCI" + idET;
            var valorCeldaCI = $(row).children()[0].outerText;
            $(IdcontrolCI).val(valorCeldaCI);
        }

        for (var i = 2; i < columnas.length; i++) {
            //var nameCol = columnas[i].outerText;
            var IdControl = "#" + NombreContenedor + "Field" + columnas[i].id;
            var nameControl = NombreContenedor + "Field" + columnas[i].id;
            var valorCelda = $(row).children()[i].outerText;
            var columnStyle = parseInt($(IdControl).attr("data-Columnstyle"));
            var columntype = parseInt($(IdControl).attr("data-DataType"));


            switch (columntype) {
                case TDataType.String: // SysCfg.DB.Properties.TDataType.String   
                    switch (columnStyle) {
                        case TExtrafields_ColumnStyle.None:
                            $(IdControl).val(valorCelda);
                            break;
                        case TExtrafields_ColumnStyle.TextPassword:
                            $(IdControl).val(valorCelda);
                            break;
                        case TExtrafields_ColumnStyle.LookUp:
                            $(IdControl).val(valorCelda);
                            break;
                        case TExtrafields_ColumnStyle.LookUpOption:
                            $('input[name="' + nameControl + '"][value="' + valorCelda + '"]').prop('checked', true);
                            break;
                    }
                    break;
                case TDataType.Text: //SysCfg.DB.Properties.TDataType.Text     
                    switch (columnStyle) {
                        case TExtrafields_ColumnStyle.None:
                            $(IdControl).val(valorCelda);
                            break;
                    }
                    break;
                case TDataType.Int32: // SysCfg.DB.Properties.TDataType.Int32:    
                    switch (columnStyle) {
                        case TExtrafields_ColumnStyle.None:
                            $(IdControl).val(valorCelda);
                            break;
                        case TExtrafields_ColumnStyle.LookUp:
                            $(IdControl).val(valorCelda);
                            break;
                        case TExtrafields_ColumnStyle.LookUpOption:
                            $('input[name="' + nameControl + '"][value="' + valorCelda + '"]').prop('checked', true);
                            break;
                    }
                    break;
                case TDataType.Double: //SysCfg.DB.Properties.TDataType.Double:  
                    switch (columnStyle) {
                        case TExtrafields_ColumnStyle.None:
                            $(IdControl).val(valorCelda);
                            break;
                    }
                    break;
                case TDataType.Boolean: //SysCfg.DB.Properties.TDataType.Boolean: 
                    switch (columnStyle) {
                        case TExtrafields_ColumnStyle.None:
                            $(IdControl).val(valorCelda);
                            $(IdControl).prop("checked", valorCelda);
                            break;
                    }
                    break;
                case TDataType.DateTime: //SysCfg.DB.Properties.TDataType.DateTime:   
                    switch (columnStyle) {
                        case TExtrafields_ColumnStyle.None:
                            $(IdControl).datetimepicker('setDate', (new Date(valorCelda)));
                            break;
                        case TExtrafields_ColumnStyle.Date:
                            $(IdControl).datepicker('setDate', (new Date(valorCelda)));
                            //$(IdControl).val(formatTextDate(valorCelda));
                            break;
                        case TExtrafields_ColumnStyle.Time:
                            $(IdControl).datetimepicker('setDate', (new Date(valorCelda)));
                            break;
                    }
                    break;
            }           
        }
        return StateFormulario._update;
    }
    else
        return StateFormulario._Nuevo;
}
function RegistrarClickCMDB(NombreContenedor) {
    $("#" + NombreContenedor + "blockSecundario").on('click', '.trExtraField', function (e) {
        e.preventDefault();
        var columnas = $("#" + $($(this).parent().parent()).attr("id") + " thead").children(':eq(' + 0 + ')').children();

        if (columnas.length > 0) {
            var IdET = $(columnas[0]).attr("data-IdET");
            var IdcontrolET = "#" + NombreContenedor + "FieldExtraTable" + IdET;
            var valorCeldaET = $(this).children()[1].outerText;
            $(IdcontrolET).val(valorCeldaET);

            var IdcontrolCI = "#" + NombreContenedor + "FieldIDCMDBCI" + IdET;
            var valorCeldaCI = $(this).children()[0].outerText;
            $(IdcontrolCI).val(valorCeldaCI);
        }

        for (var i = 2; i < columnas.length; i++) {
            var IdControl = "#" + NombreContenedor + "Field" + columnas[i].id;
            var valorCelda = $(this).children()[i].outerText;

            var columnStyle = parseInt($(IdControl).attr("data-Columnstyle"));
            if (columnStyle == TExtrafields_ColumnStyle.LookUpOption)
                $(IdControl).filter('[value=' + valorCelda + ']').prop('checked', true);
            else {
                $(IdControl).val(valorCelda);
                $(IdControl).prop("checked", valorCelda);
            }

            //if ($(IdControl).type == "radio")
            //    $(IdControl).filter('[value=' + valorCelda + ']').attr('checked', true);
            //else {
            //    $(IdControl).val(valorCelda);
            //    $(IdControl).prop("checked", valorCelda);
            //}
        }

        var NroADN = parseInt($('#' + NombreContenedor + 'NroADN' + IdET).val());

        if (NroADN > 0) {
            $('#' + NombreContenedor + 'stateForm' + IdET).val(StateFormulario._update);
        }
        else {
            $('#' + NombreContenedor + 'stateForm' + IdET).val(StateFormulario._Nuevo);
        }

        HabilitaControlesCMDB(IdET, true, NombreContenedor);
        HabilitaBotonesCMDB(false, true, false, true, IdET, NombreContenedor);
    });
}
function GrabarCMDBExtraTable(NombreContenedor) {
    $("#" + NombreContenedor + "blockSecundario").on('click', '.btnGrabarET', function () {
        var IdET = parseInt($(this).attr('data-IdET'));
        var IdCI = parseInt($(this).attr('data-IdCI'));

        var Controles = $("#" + NombreContenedor + "tab" + IdET + " [data-NameField]");
        var parametros = [];
        var name_temp = "";
        for (var i = 0; i < Controles.length; i++) {
            var Atributos = [];
            
            //if (parseInt($(Controles[i]).attr('data-NameField')) == IdET) { ///falta cambiar codigo aki
            var columnstyle = parseInt($(Controles[i]).attr('data-columnstyle'));

            Atributos[0] = $(Controles[i]).attr('data-NameField');

            
            if (columnstyle == TExtrafields_ColumnStyle.LookUpOption) {
                if (name_temp != Controles[i].id) {
                    Atributos[1] = $('input[name="' + Controles[i].id + '"]:checked').val()
                    name_temp = Controles[i].id;
                }
                //Atributos[1] = $('input[name="' + Controles[i].id + '"][value="' + valorCelda + '"]').prop('checked');
            }
            else {
                Atributos[1] = $(Controles[i]).val();
                var name_temp = "";
            }
            Atributos[2] = $(Controles[i]).attr('data-DataType');

            parametros[i] = Atributos;
            //}
        }
        var _stateForm = $('#' + NombreContenedor + 'stateForm' + IdET).val();


        _data = JSON.stringify({ IDCMDBCIDEFINEEXTRATABLE: IdET, arrayString: parametros, stateForm: _stateForm, IDCMDBCI : IdCI });
        //_data = JSON.stringify(NuevoObjeto);
        $.ajax({
            type: "POST",
            url: "CIEditor.svc/GrabarExtraTable",
            contentType: "application/json; charset=utf-8",
            data: _data,
            success: function (response) {
                var NroADN = parseInt($('#' + NombreContenedor + 'NroADN' + IdET).val());

                if (_stateForm == StateFormulario._update) {
                    if (NroADN > 0) {
                        LimpiarControlesCDMB(IdET, NombreContenedor);
                        HabilitaControlesCMDB(IdET, false, NombreContenedor);
                        $('#' + NombreContenedor + 'stateForm' + IdET).val(StateFormulario._Nuevo);
                        HabilitaBotonesCMDB(true, false, false, false, IdET, NombreContenedor);
                    }
                    else {
                        $('#' + NombreContenedor + 'stateForm' + IdET).val(StateFormulario._update);
                        HabilitaBotonesCMDB(true, true, true, false, IdET, NombreContenedor);
                    }
                } else {
                    if (_stateForm == StateFormulario._update) {
                        if (NroADN > 0) {
                            LimpiarControlesCDMB(IdET, NombreContenedor);
                            HabilitaControlesCMDB(IdET, false, NombreContenedor);
                            HabilitaBotonesCMDB(true, false, false, false, IdET, NombreContenedor);
                        }
                        else {
                            HabilitaBotonesCMDB(true, true, true, false, IdET, NombreContenedor);
                        }
                    }
                }
                ROWCMDBEXTRATABLE_GET_update(IdET, IdCI, NombreContenedor);
                alert('Se grabo correctamente.');
            },
            error: function (response) {
                alert(response);
            }
        });
    });
}
function EliminarCMDBExtraTable(NombreContenedor) {
    $("#" + NombreContenedor + "blockSecundario").on('click', '.btnEliminarET', function () {
        var IdET = parseInt($(this).attr('data-IdET'));
        var IdCI = parseInt($(this).attr('data-IdCI'));
        var Controles = $("#" + NombreContenedor + "tab" + IdET + " [data-NameField]");
        var parametros = [];
        for (var i = 0; i < Controles.length; i++) {
            var Atributos = [];
            //if (parseInt($(Controles[i]).attr('data-NameField')) == IdET) { ///falta cambiar codigo aki
            Atributos[0] = $(Controles[i]).attr('data-NameField');
            Atributos[1] = $(Controles[i]).val();
            Atributos[2] = $(Controles[i]).attr('data-DataType');
            parametros[i] = Atributos;
            //}
        }

        _data = JSON.stringify({ IDCMDBCIDEFINEEXTRATABLE: IdET, arrayString: parametros, stateForm: StateFormulario._Delete, IDCMDBCI: IdCI });
        //_data = JSON.stringify(NuevoObjeto);
        $.ajax({
            type: "POST",
            url: "CIEditor.svc/GrabarExtraTable",
            contentType: "application/json; charset=utf-8",
            data: _data,
            success: function (response) {
                ROWCMDBEXTRATABLE_GET_update(IdET,IdCI, NombreContenedor);
                LimpiarControlesCDMB(IdET, NombreContenedor);
                alert("Se elimino correctamente");
            },
            error: function (response) {
                alert(response);
            }
        });
    });
}
function CancelarCMDBExtraTable(NombreContenedor) {
    $("#" + NombreContenedor + "blockSecundario").on('click', '.btnCancelarET', function () {
        var IdET = parseInt($(this).attr('data-IdET'));
        LimpiarControlesCDMB(IdET, NombreContenedor);
        HabilitaControlesCMDB(IdET, false, NombreContenedor);
        $('#' + NombreContenedor + 'stateForm' + IdET).val(StateFormulario._Cancel);
        HabilitaBotonesCMDB(true, false, false, false, IdET, NombreContenedor);
    });
}
function NuevoCMDBExtraTable(NombreContenedor) {
    $("#" + NombreContenedor + "blockSecundario").on('click', '.btnNuevoET', function () {
        var IdET = parseInt($(this).attr('data-IdET'));
        LimpiarControlesCDMB(IdET, NombreContenedor);
        HabilitaControlesCMDB(IdET, true, NombreContenedor);
        $('#' + NombreContenedor + 'stateForm' + IdET).val(StateFormulario._Nuevo);
        HabilitaBotonesCMDB(false, true, false, true, IdET, NombreContenedor);
    });
}
function ROWCMDBEXTRATABLE_GET_update(IDCMDBCIDEFINEEXTRATABLE, IDCMDBCI, NombreContenedor) {
    var data = {
        'IDCMDBCIDEFINEEXTRATABLE': IDCMDBCIDEFINEEXTRATABLE,
        'IDCMDBCI': IDCMDBCI
    };

    $.ajax({
        type: "POST",
        url: 'CIEditor.svc/ROWEXTRATABLE_GET',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async:false,
        success: function (response) {
            //$('#ArtiFields' + CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE).remove("#GridFields" + CMDBCIDEFINEEXTRATABLE.IDCMDBCIDEFINEEXTRATABLE);
            var NroADN = $("#" + NombreContenedor + "NroADN" + IDCMDBCIDEFINEEXTRATABLE).val();
            $('#' + NombreContenedor + 'ArtiFields' + IDCMDBCIDEFINEEXTRATABLE).html("");
            var ContenedorET = "<table id='" + NombreContenedor + "GridFields" + IDCMDBCIDEFINEEXTRATABLE + "' class='GridFieldStyle'>";
            ContenedorET += "<thead>";
            ContenedorET += "<tr>";
            for (var i = 0; i < response.Columnas.length; i++) {
                ContenedorET += "<th id='" + NombreContenedor + response.Columnas[i].IDCMDBCIDEFINEEXTRAFIELDS + "' data-IdEF='" + response.Columnas[i].IDCMDBCIDEFINEEXTRAFIELDS + "' data-IdET='" + IDCMDBCIDEFINEEXTRATABLE + "'>" + response.Columnas[i].NombreColumna + "</th>";
            }
            ContenedorET += "</tr>";
            ContenedorET += "<tbody>";
            for (var i = 0; i < response.Filas.length; i++) {
                ContenedorET += "<tr class='trExtraField'>";
                for (var j = 0; j < response.Filas[i].Celdas.length; j++) {
                    ContenedorET += "<td>" + response.Filas[i].Celdas[j].Valor + "</td>";
                }
                ContenedorET += "</tr>";
            }
            ContenedorET += "</tbody>";
            ContenedorET += "</table>";
            $('#' + NombreContenedor + 'ArtiFields' + IDCMDBCIDEFINEEXTRATABLE).append(ContenedorET);

            $('#' + NombreContenedor + 'ArtiFields' + IDCMDBCIDEFINEEXTRATABLE).append("<input type='hidden' id='" + NombreContenedor + "NroADN" + IDCMDBCIDEFINEEXTRATABLE + "' value='" + NroADN + "'>");
            $('#' + NombreContenedor + 'ArtiFields' + IDCMDBCIDEFINEEXTRATABLE).append("<input type='hidden' id='" + NombreContenedor + "stateForm" + IDCMDBCIDEFINEEXTRATABLE + "' value='" + StateFormulario._Nuevo + "'>");
            if (!(NroADN > 0)) {
                var stateForm = ClickPrimerRowCMDB("#" + NombreContenedor + "GridFields" + IDCMDBCIDEFINEEXTRATABLE, NombreContenedor);
                $('#' + NombreContenedor + 'stateForm' + IDCMDBCIDEFINEEXTRATABLE).val(stateForm);
                $('#' + NombreContenedor + 'ArtiFields' + IDCMDBCIDEFINEEXTRATABLE).css('display', 'none');
                $("#" + NombreContenedor + "tab" + IDCMDBCIDEFINEEXTRATABLE + ' .btnNuevoET').css('display', 'none');
                $("#" + NombreContenedor + "tab" + IDCMDBCIDEFINEEXTRATABLE + ' .btnCancelarET').css('display', 'none');
            }
        },
        error: function (response) {
            alert(response.ErrorMessage);
        }
    });
}
function ContarADN(CMDBCIDEFINEEXTRAFIELDList) {
    var count = 0;
    for (var i = 0; i < CMDBCIDEFINEEXTRAFIELDList.length; i++) {
        if (CMDBCIDEFINEEXTRAFIELDList[i].IDCMDBKEYTYPE == 1) {
            count += 1;
        }
    }
    return count;
}
function CrearModalRelations(NombreContenedor) {
    var ModalCIEditor = "";
    ModalCIEditor += '<div id="'+ NombreContenedor + 'ContendorModal">';
    ModalCIEditor += '<a href="#' + NombreContenedor + 'ModalRel" id="' + NombreContenedor + 'linkModalRel" style="display:none;">Abrir Modal</a>';
    ModalCIEditor += '<div id="' + NombreContenedor + 'ModalRel" class="modalDialog">';
    ModalCIEditor += '<div>';
    ModalCIEditor += '<a href="#close" title="Close" class="close">X</a>';
    ModalCIEditor += '<input id="' + NombreContenedor + 'AddRel" data-IdDefRelType="0" data-IdRelType="0" style="margin-bottom: 5px;" type="button" value="Add">';
    ModalCIEditor += '<article class="" id="' + NombreContenedor + 'ContTablaRel" style=" height: 550px; overflow: scroll;">';
    ModalCIEditor += '</article>';
    ModalCIEditor += '</div>';
    ModalCIEditor += '</div>';
    ModalCIEditor += '</div>';
    return ModalCIEditor;
}
function GenerarclickBtnPrincipales(NombreContenedor) {
    $("#" + NombreContenedor + "btnNuevo").off('click');
    $("#" + NombreContenedor + "btnNuevo").on('click', function () {
        LimpiarControlesPrincipales(NombreContenedor);
        LimpiarTabsExtraTables(NombreContenedor);
        LimpiarTabsRelations(NombreContenedor);        
        EnabledControlesPrincipales(NombreContenedor, true);
        EnabledBotonesPrincipales(NombreContenedor, false, true, false, true);
        $("#" + NombreContenedor + "stateFormPrincipal").val(StateFormulario._Nuevo);
        $('#' + NombreContenedor + 'tabPrincipal #' + NombreContenedor + 'tabPrincipalLU.tabs li:first').click();
    });

    $("#" + NombreContenedor + "btnGuardar").off('click');
    $("#" + NombreContenedor + "btnGuardar").on('click', function () {
        var _IDCMDBCI = parseInt($('#' + NombreContenedor + 'IDCMDBCI').val());
        var _CI_SERIALNUMBER = $("#" + NombreContenedor + "txtSerial").val();
        var _CI_GENERICNAME = $("#" + NombreContenedor + "txtName").val();
        var _CI_DESCRIPTION = $("#" + NombreContenedor + "txtOtherDetails").val();
        var _IDCMDBCIDEFINE = parseInt($("#" + NombreContenedor + "cbCIDefine").val());
        var _IDCMDBCISTATE = parseInt($("#" + NombreContenedor + "cbCIState").val());
        var _IDCMDBBRAND = parseInt($("#" + NombreContenedor + "cbCIBrand").val());

        var _CIDEFINE_NAME = $("#" + NombreContenedor + "cbCIDefine option:selected").text();

        var _CMDBCI = {
            IDCMDBCI: _IDCMDBCI,
            CI_SERIALNUMBER: _CI_SERIALNUMBER,
            CI_GENERICNAME: _CI_GENERICNAME,
            CI_DESCRIPTION: _CI_DESCRIPTION,
            IDCMDBCIDEFINE: _IDCMDBCIDEFINE,
            IDCMDBCISTATE: _IDCMDBCISTATE,
            IDCMDBBRAND: _IDCMDBBRAND
        };

        var _StateForm = $("#" + NombreContenedor + "stateFormPrincipal").val();

        var _data = JSON.stringify({ CMDBCI: _CMDBCI, StateForm: _StateForm });

        $.ajax({
            type: "POST",
            url: "CIEditor.svc/GrabarCI",
            contentType: "application/json; charset=utf-8",
            data: _data,
            success: function (response) {
                if (_StateForm == StateFormulario._update) {
                    var RowSelect = $("#" + NombreContenedor + " .gridSearchCI tbody tr.rowSelect");

                    $(RowSelect).children()[0].innerHTML = _IDCMDBCI;
                    $(RowSelect).children()[1].innerHTML = _CI_SERIALNUMBER;
                    $(RowSelect).children()[2].innerHTML = _CI_GENERICNAME;
                    $(RowSelect).children()[3].innerHTML = _CIDEFINE_NAME;
                }

                LimpiarControlesPrincipales(NombreContenedor);
                LimpiarTabsExtraTables(NombreContenedor);
                LimpiarTabsRelations(NombreContenedor);
                EnabledControlesPrincipales(NombreContenedor, false);
                EnabledBotonesPrincipales(NombreContenedor, true, false, false, false);
                $('#' + NombreContenedor + 'tabPrincipal #' + NombreContenedor + 'tabPrincipalLU.tabs li:first').click();                

                //$("#" + NombreContenedor + "stateFormPrincipal").val(StateFormulario._Nuevo);

                

                //var NroADN = parseInt($('#' + NombreContenedor + 'NroADN' + IdET).val());

                //if (_stateForm == StateFormulario._update) {
                //    if (NroADN > 0) {
                //        LimpiarControlesCDMB(IdET, NombreContenedor);
                //        HabilitaControlesCMDB(IdET, false, NombreContenedor);
                //        $('#' + NombreContenedor + 'stateForm' + IdET).val(StateFormulario._Nuevo);
                //        HabilitaBotonesCMDB(true, false, false, false, IdET, NombreContenedor);
                //    }
                //    else {
                //        $('#' + NombreContenedor + 'stateForm' + IdET).val(StateFormulario._update);
                //        HabilitaBotonesCMDB(true, true, true, false, IdET, NombreContenedor);
                //    }
                //} else {
                //    if (_stateForm == StateFormulario._update) {
                //        if (NroADN > 0) {
                //            LimpiarControlesCDMB(IdET, NombreContenedor);
                //            HabilitaControlesCMDB(IdET, false, NombreContenedor);
                //            HabilitaBotonesCMDB(true, false, false, false, IdET, NombreContenedor);
                //        }
                //        else {
                //            HabilitaBotonesCMDB(true, true, true, false, IdET, NombreContenedor);
                //        }
                //    }
                //}
                //ROWCMDBEXTRATABLE_GET_update(IdET, IdCI, NombreContenedor);
                alert('Se grabo correctamente.');
            },
            error: function (response) {
                alert(response);
            }
        });        
    });

    $("#" + NombreContenedor + "btnEliminar").off('click');
    $("#" + NombreContenedor + "btnEliminar").on('click', function () {
        //alert("eliminar");
    });

    $("#" + NombreContenedor + "btnCancelar").off('click');
    $("#" + NombreContenedor + "btnCancelar").on('click', function () {
        LimpiarControlesPrincipales(NombreContenedor);
        LimpiarTabsExtraTables(NombreContenedor);
        LimpiarTabsRelations(NombreContenedor);
        EnabledControlesPrincipales(NombreContenedor, false);
        EnabledBotonesPrincipales(NombreContenedor, true, false, false, false);
        $("#" + NombreContenedor + "stateFormPrincipal").val(StateFormulario._Nuevo);
        $('#' + NombreContenedor + 'tabPrincipal #' + NombreContenedor + 'tabPrincipalLU.tabs li:first').click();
    });
}

//funciones de mantenimiento de CIs
function CargarCIDefine(NombreContenedor) {
    var Combo = ""
    $.ajax({
        type: "POST",
        url: 'CIEditor.svc/GetCIDefine',
        data: {},
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (response) {
            Combo += "<select id='" + NombreContenedor + "cbCIDefine'>"
            for (var i = 0; i < response.length; i++) {
                Combo += "<option value='" + response[i].IDCMDBCIDEFINE + "'>" + response[i].CIDEFINE_NAME + "</option>";
            }
            Combo += "</select>"
        },
        error: function (response) {
            alert(response.ErrorMessage);
        }
    });
    return Combo;
}
function CargarCIState(NombreContenedor) {
    var Combo = ""
    $.ajax({
        type: "POST",
        url: 'CIEditor.svc/GetCIState',
        data: {},
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (response) {
            Combo += "<select id='" + NombreContenedor + "cbCIState'>"
            for (var i = 0; i < response.length; i++) {
                Combo += "<option value='" + response[i].ValueMember + "'>" + response[i].DisplayMember + "</option>";
            }
            Combo += "</select>"
        },
        error: function (response) {
            alert(response.ErrorMessage);
        }
    });
    return Combo;
}
function CargarCIBrand(NombreContenedor) {
    var Combo = ""
    $.ajax({
        type: "POST",
        url: 'CIEditor.svc/GetCIBrand',
        data: {},
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (response) {
            Combo += "<select id='" + NombreContenedor + "cbCIBrand'>"
            for (var i = 0; i < response.length; i++) {
                Combo += "<option value='" + response[i].IDCMDBBRAND + "'>" + response[i].BRAND_NAME + "</option>";
            }
            Combo += "</select>"
        },
        error: function (response) {
            alert(response.ErrorMessage);
        }
    });
    return Combo;
}

function LimpiarControlesPrincipales(NombreContenedor) {
    $("#" + NombreContenedor + "txtSerial").val("");
    $("#" + NombreContenedor + "txtName").val("");
    $("#" + NombreContenedor + "txtAcquiredDate").val("");
    $("#" + NombreContenedor + "txtDisposeDate").val("");
    $("#" + NombreContenedor + "txtOtherDetails").val("");
}
function LimpiarTabsExtraTables(NombreContenedor) {
    $('#' + NombreContenedor + 'tabSecundarioLU li').remove(".tabDinamic");
    $('#' + NombreContenedor + 'blockSecundario article').remove(".Articulodinamico");
}
function LimpiarTabsRelations(NombreContenedor) {
    $('#' + NombreContenedor + 'tabSecundarioLUOR').html("");
    $('#' + NombreContenedor + "blockSecundarioOR").html("");

    $('#' + NombreContenedor + 'tabSecundarioLUIR').html("");
    $('#' + NombreContenedor + "blockSecundarioIR").html("");
}

function EnabledControlesPrincipales(NombreContenedor, estado) {
    $("#" + NombreContenedor + "txtSerial").prop('disabled', !estado);
    $("#" + NombreContenedor + "txtName").prop('disabled', !estado);
    $("#" + NombreContenedor + "txtAcquiredDate").prop('disabled', !estado);
    $("#" + NombreContenedor + "txtDisposeDate").prop('disabled', !estado);
    $("#" + NombreContenedor + "txtOtherDetails").prop('disabled', !estado);
    $("#" + NombreContenedor + "cbCIDefine").prop('disabled', !estado);
    $("#" + NombreContenedor + "cbCIState").prop('disabled', !estado);
    $("#" + NombreContenedor + "cbCIBrand").prop('disabled', !estado);
}
function EnabledBotonesPrincipales(NombreContenedor, nuevo, guardar, eliminar, cancelar) {
    $("#" + NombreContenedor + "btnNuevo").prop('disabled', !nuevo);
    $("#" + NombreContenedor + "btnGuardar").prop('disabled', !guardar);
    $("#" + NombreContenedor + "btnEliminar").prop('disabled', !eliminar);
    $("#" + NombreContenedor + "btnCancelar").prop('disabled', !cancelar);
}
//fin funciones de mantenimiento de CIs

//funciones de los Output relations
function CrearTabPageOR(NombreContenedor) {
    var TabPageOR = "";
    TabPageOR += "<section class='wrapper' id='" + NombreContenedor + "tabSecundarioOR' style='margin-top: 5px;'>";
    TabPageOR += "<ul class='tabs' id='" + NombreContenedor + "tabSecundarioLUOR'>";
    //TabPageOR += "<li id='" + NombreContenedor + "tabBasicoLiOR'><a href='#" + NombreContenedor + "tabBasicOR'>BASIC</a></li>";
    TabPageOR += "</ul>";
    TabPageOR += "<div class='clr'></div>";
    TabPageOR += "<section class='block' id='" + NombreContenedor + "blockSecundarioOR'>";
    //TabPageOR += "<article id='" + NombreContenedor + "tabBasicOR' class='articleSecundario'>";
    TabPageOR += "</section>";
    TabPageOR += "</section>";
    return TabPageOR;
}
function GetRelationsChilds(IDCMDBCI, NombreContenedor) {
    var data = {
        'IDCMDBCI': IDCMDBCI
    };

    $.ajax({
        type: "POST",
        url: 'CIEditor.svc/GetTyepeRelationsChilds',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //alert(response);
            $('#' + NombreContenedor + 'tabSecundarioLUOR').html("");
            $('#' + NombreContenedor + "blockSecundarioOR").html("");
            for (var i = 0; i < response.length; i++) {
                $('#' + NombreContenedor + 'tabSecundarioLUOR').append("<li class='articleSecundarioOR'><a href='#tab" + NombreContenedor + response[i].IDCMDBCIDEFINERELATIONTYPE + "OR'>" + response[i].CIDEFINERELATIONTYPE_NAME + "</a></li>");
                GetRelationsChildsList(response[i], IDCMDBCI, NombreContenedor);
            }
            GenerarTabsClicksSecundariosOR(NombreContenedor);
            RegistrarEventosGrillaOR(NombreContenedor);
            RegistrarclickRelAddOR(NombreContenedor, IDCMDBCI, true);
            RegistrarclickRelDelOR(NombreContenedor, IDCMDBCI, true);
            RegistrarClickAddRelations(NombreContenedor, IDCMDBCI, true);


        },
        error: function (response) {
            alert(response.ErrorMessage);
        }
    });
}
function GetRelationsChildsList(CMDBCIDEFINERELATIONTYPE, IDCMDBCI, NombreContenedor) {
    var data = {
        'IDCMDBCIDEFINERELATIONTYPE': CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINERELATIONTYPE,
        'IDCMDBCI': IDCMDBCI
    };
    $.ajax({
        type: "POST",
        url: 'CIEditor.svc/GetTyepeRelationsChildsList',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async:false,
        success: function (response) {            
            var ContenedorET = "<article id='tab" + NombreContenedor + CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINERELATIONTYPE + "OR' class='articleSecundarioOR ArticulodinamicoOR' style='display: none;'>";
            ContenedorET += "<input class='btnAddOR' data-IdDefRelType='" + CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINERELATIONTYPE + "' data-IdRelType='" + CMDBCIDEFINERELATIONTYPE.IDCMDBRELATIONTYPE + "' value='Add' type='button' style='margin-top:8px;'>";
            ContenedorET += "<input class='btnDeleteOR' data-IdDefRelType='" + CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINERELATIONTYPE + "' data-IdRelType='" + CMDBCIDEFINERELATIONTYPE.IDCMDBRELATIONTYPE + "' value='Delete' type='button' style='margin-top:8px;'>";
            ContenedorET += "<div style='overflow: auto; overflow-y: hidden;'>";
            ContenedorET += "<table id='GridRels" + NombreContenedor + CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINERELATIONTYPE + "OR' class='GridFieldStyle gridOR' style='margin-top:8px;'>";
            ContenedorET += "<thead>";
            ContenedorET += "<tr>";
            ContenedorET += "<th>FDMA01</th>";
            ContenedorET += "<th>CI_DATEPLANNED</th>";
            ContenedorET += "<th>CI_DATEIN</th>";
            ContenedorET += "<th>CI_DATEOUT</th>";
            ContenedorET += "<th>CI_GENERICNAME</th>";
            ContenedorET += "<th>CI_DESCRIPTION</th>";
            ContenedorET += "<th>IDCMDBCI</th>";
            ContenedorET += "<th>IDCMDBBRAND</th>";
            ContenedorET += "<th>CI_SERIALNUMBER</th>";
            ContenedorET += "<th>CI_PURCHASEDORRENTED</th>";
            ContenedorET += "<th>CIDEFINE_NAME</th>";
            ContenedorET += "<th>CIDEFINERELATIONTYPE_NAME</th>";
            ContenedorET += "<th>IDCMDBCIDEFINE_CHILD</th>";
            ContenedorET += "<th>IDCMDBCIDEFINE_PARENT</th>";
            ContenedorET += "<th>IDCMDBRELATIONTYPE</th>";
            ContenedorET += "<th>IDCMDBCI_CHILD</th>";
            ContenedorET += "<th>IDCMDBCI_PARENT</th>";
            ContenedorET += "<th>IDCMDBCIRELATION</th>";
            ContenedorET += "<th>IDCMDBCIDEFINERELATIONTYPE</th>";
            ContenedorET += "</tr>";
            ContenedorET += "</thead>";
            ContenedorET += "<tbody>";
            for (var i = 0; i < response.length; i++) {
                ContenedorET += "<tr>";
                ContenedorET += "<td><input type='checkbox'></td>";
                ContenedorET += "<td>" + formatJSONDateTime(response[i].CI_DATEPLANNED) + "</td>";
                ContenedorET += "<td>" + formatJSONDateTime(response[i].CI_DATEIN) + "</td>";
                ContenedorET += "<td>" + formatJSONDateTime(response[i].CI_DATEOUT) + "</td>";
                ContenedorET += "<td>" + response[i].CI_GENERICNAME + "</td>";
                ContenedorET += "<td>" + response[i].CI_DESCRIPTION + "</td>";
                ContenedorET += "<td>" + response[i].IDCMDBCI + "</td>";
                ContenedorET += "<td>" + response[i].IDCMDBBRAND + "</td>";
                ContenedorET += "<td>" + response[i].CI_SERIALNUMBER + "</td>";
                ContenedorET += "<td>" + response[i].CI_PURCHASEDORRENTED + "</td>";
                ContenedorET += "<td>" + response[i].CIDEFINE_NAME + "</td>";
                ContenedorET += "<td>" + response[i].CIDEFINERELATIONTYPE_NAME + "</td>";
                ContenedorET += "<td>" + response[i].IDCMDBCIDEFINE_CHILD + "</td>";
                ContenedorET += "<td>" + response[i].IDCMDBCIDEFINE_PARENT + "</td>";
                ContenedorET += "<td>" + response[i].IDCMDBRELATIONTYPE + "</td>";
                ContenedorET += "<td>" + response[i].IDCMDBCI_CHILD + "</td>";
                ContenedorET += "<td>" + response[i].IDCMDBCI_PARENT + "</td>";
                ContenedorET += "<td>" + response[i].IDCMDBCIRELATION + "</td>";
                ContenedorET += "<td>" + response[i].IDCMDBCIDEFINERELATIONTYPE + "</td>";
                ContenedorET += "</tr>";
            }
            ContenedorET += "</tbody>";
            ContenedorET += "</table>";
            ContenedorET += "</div>";
            ContenedorET += "</article>";

            $('#' + NombreContenedor + "blockSecundarioOR").append(ContenedorET);

            //ROWEXTRATABLE_GET(CMDBCIDEFINEEXTRATABLE, response);
        },
        error: function (response) {
            alert(response.ErrorMessage);
        }
    });
}
function GenerarTabsClicksSecundariosOR(NombreContenedor) {
    $('#' + NombreContenedor + 'tabSecundarioOR #' + NombreContenedor + 'tabSecundarioLUOR.tabs li:first').addClass('active');
    $('#' + NombreContenedor + 'tabSecundarioOR #' + NombreContenedor + 'blockSecundarioOR.block .articleSecundarioOR').hide();
    $('#' + NombreContenedor + 'tabSecundarioOR #' + NombreContenedor + 'blockSecundarioOR.block article:first').show();
    $('#' + NombreContenedor + 'tabSecundarioOR #' + NombreContenedor + 'tabSecundarioLUOR.tabs').on('click', 'li', function () {
        $('#' + NombreContenedor + 'tabSecundarioOR #' + NombreContenedor + 'tabSecundarioLUOR.tabs li').removeClass('active');
        $(this).addClass('active')
        $('#' + NombreContenedor + 'tabSecundarioOR #' + NombreContenedor + 'blockSecundarioOR.block .articleSecundarioOR').hide();
        var activeTab = $(this).find('a').attr('href');
        $(activeTab).show();
        return false;
    });
}
function RegistrarEventosGrillaOR(NombreContenedor) {
    var rowOldOR;
    ///carga el ci elegido 
    $("#" + NombreContenedor + " .gridOR tbody").on('click', 'tr', function (e) {
        //e.preventDefault();
        //var IDCMDBCI = $(this).attr('data-IdCI');
        $(rowOldOR).removeClass('rowSelect');
        $(this).addClass('rowSelect');
        rowOldOR = this;
    });
}
function RegistrarclickRelAddOR(NombreContenedor, IDCMDBCI, ListChild) {
    $("#" + NombreContenedor).off('click', '.btnAddOR');
    $("#" + NombreContenedor).on('click', '.btnAddOR', function () {
        IDCMDBCIDEFINERELATIONTYPE = parseInt($(this).attr('data-IdDefRelType'));
        IDCMDBRELATIONTYPE = parseInt($(this).attr('data-IdRelType'));

        $('#' + NombreContenedor + 'AddRel').attr('data-IdDefRelType', IDCMDBCIDEFINERELATIONTYPE);
        $('#' + NombreContenedor + 'AddRel').attr('data-IdRelType', IDCMDBRELATIONTYPE);

        GetRelationsXAdd(IDCMDBCIDEFINERELATIONTYPE, IDCMDBCI, ListChild, NombreContenedor, IDCMDBRELATIONTYPE);

        document.location = '#' + NombreContenedor + 'ModalRel';
    });    
}
function RegistrarclickRelDelOR(NombreContenedor, IDCMDBCI, ListChild) {
    $("#" + NombreContenedor).off('click', '.btnDeleteOR');
    $("#" + NombreContenedor).on('click', '.btnDeleteOR', function () {
        IDCMDBCIDEFINERELATIONTYPE = parseInt($(this).attr('data-IdDefRelType'));
        IDCMDBRELATIONTYPE = parseInt($(this).attr('data-IdRelType'));

        var _ListRel = [];
        var _ListRows = [];
        var cont = 0;

        $('#GridRels' + NombreContenedor + IDCMDBCIDEFINERELATIONTYPE + 'OR tbody tr').each(function (index2) {
            var isSelect = $('#GridRels' + NombreContenedor + IDCMDBCIDEFINERELATIONTYPE + 'OR tbody tr:eq(' + index2 + ')').find('td:eq(0) input').is(':checked')
           
            if (isSelect) {
                var _IDCMDBRELATIONTYPE;
                if (IDCMDBRELATIONTYPE != 0)
                    _IDCMDBRELATIONTYPE = IDCMDBRELATIONTYPE;
                else
                    _IDCMDBRELATIONTYPE = parseInt($(this).find("td").eq(13).html());

                var _TCMDBCIRELATION = {
                    IDCMDBCIRELATION: parseInt($(this).find("td").eq(17).html()),
                    IDCMDBCI_CHILD: parseInt($(this).find("td").eq(6).html()),
                    IDCMDBCI_PARENT: parseInt(IDCMDBCI),
                    IDCMDBRELATIONTYPE: _IDCMDBRELATIONTYPE
                }
                _ListRows[cont] = this;
                _ListRel[cont] = _TCMDBCIRELATION;
                cont += 1;
            }            
        });

        $.ajax({
            type: "POST",
            url: 'CIEditor.svc/DelRelations',
            data: JSON.stringify({ ListRel: _ListRel, IDCMDBCI: IDCMDBCI, ListChild: ListChild }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (response) {
                if (response) {
                    for (var i = 0; i < _ListRows.length; i++) {
                        $(_ListRows[i]).remove();
                    }
                }
            },
            error: function (response) {
                alert(response.ErrorMessage);
            }
        });
    });
}

function GetRelationsXAdd(IDCMDBCIDEFINERELATIONTYPE, IDCMDBCI, ListChild, NombreContenedor, IDCMDBRELATIONTYPE) {
    var data = {
        'IDCMDBCIDEFINERELATIONTYPE': IDCMDBCIDEFINERELATIONTYPE,
        'IDCMDBCI': IDCMDBCI,
        'ListChild': ListChild
    };
    $.ajax({
        type: "POST",
        url: 'CIEditor.svc/GetRelationsXAdd',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (response) {
            var ContenedorET = "<table id='" + NombreContenedor + "GridFields" + IDCMDBCIDEFINERELATIONTYPE + "' class='GridFieldStyle'>";
            ContenedorET += "<thead>";
            ContenedorET += "<tr>";
            ContenedorET += "<th>FDMA01</th>"
            for (var i = 0; i < response.Columnas.length; i++) {
                ContenedorET += "<th>" + response.Columnas[i].NombreColumna + "</th>";
            }

            ContenedorET += "</tr>";
            ContenedorET += "<tbody>";
            for (var i = 0; i < response.Filas.length; i++) {
                ContenedorET += "<tr>";
                ContenedorET += "<td><input type='checkbox'></td>";
                for (var j = 0; j < response.Filas[i].Celdas.length; j++) {
                    ContenedorET += "<td>" + response.Filas[i].Celdas[j].Valor + "</td>";
                }
                ContenedorET += "</tr>";
            }
            ContenedorET += "</tbody>";
            ContenedorET += "</table>";
            $('#' + NombreContenedor + 'ContTablaRel').html("");
            $('#' + NombreContenedor + 'ContTablaRel').append(ContenedorET)
        },
        error: function (response) {
            alert(response.ErrorMessage);
        }
    });
}
function RegistrarClickAddRelations(NombreContenedor, IDCMDBCI, ListChild) {
    $("#" + NombreContenedor).off('click', '#' + NombreContenedor + 'AddRel');
    $("#" + NombreContenedor).on('click', '#' + NombreContenedor + 'AddRel', function () {

        IDCMDBCIDEFINERELATIONTYPE = parseInt($(this).attr('data-IdDefRelType'));
        IDCMDBRELATIONTYPE = parseInt($(this).attr('data-IdRelType'));

        var _ListRel = [];
        var cont = 0;
        
        $('#' + NombreContenedor + 'GridFields' + IDCMDBCIDEFINERELATIONTYPE + ' tbody tr').each(function (index2) {
            var isSelect = $('#' + NombreContenedor + 'GridFields' + IDCMDBCIDEFINERELATIONTYPE + ' tbody tr:eq(' + index2 + ')').find('td:eq(0) input').is(':checked')
            if (isSelect) {
                var _IDCMDBRELATIONTYPE;
                if (IDCMDBRELATIONTYPE != 0)
                    _IDCMDBRELATIONTYPE = IDCMDBRELATIONTYPE;
                else
                    _IDCMDBRELATIONTYPE = parseInt($(this).find("td").eq(13).html());

                var _TCMDBCIRELATION = {
                    IDCMDBCI_CHILD : parseInt($(this).find("td").eq(6).html()),
                    IDCMDBCI_PARENT : parseInt(IDCMDBCI),
                    IDCMDBRELATIONTYPE: _IDCMDBRELATIONTYPE
                }
                _ListRel[cont] = _TCMDBCIRELATION;
                cont += 1;
            }
        });

        $.ajax({
            type: "POST",
            url: 'CIEditor.svc/AddRelations',
            data: JSON.stringify({ ListRel: _ListRel, IDCMDBCI: IDCMDBCI, ListChild: ListChild }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (response) {
                var ContenedorET = "";
                for (var i = 0; i < response.length; i++) {
                    ContenedorET += "<tr>";
                    ContenedorET += "<td><input type='checkbox'></td>";
                    ContenedorET += "<td>" + formatJSONDateTime(response[i].CI_DATEPLANNED) + "</td>";
                    ContenedorET += "<td>" + formatJSONDateTime(response[i].CI_DATEIN) + "</td>";
                    ContenedorET += "<td>" + formatJSONDateTime(response[i].CI_DATEOUT) + "</td>";
                    ContenedorET += "<td>" + response[i].CI_GENERICNAME + "</td>";
                    ContenedorET += "<td>" + response[i].CI_DESCRIPTION + "</td>";
                    ContenedorET += "<td>" + response[i].IDCMDBCI + "</td>";
                    ContenedorET += "<td>" + response[i].IDCMDBBRAND + "</td>";
                    ContenedorET += "<td>" + response[i].CI_SERIALNUMBER + "</td>";
                    ContenedorET += "<td>" + response[i].CI_PURCHASEDORRENTED + "</td>";
                    ContenedorET += "<td>" + response[i].CIDEFINE_NAME + "</td>";
                    ContenedorET += "<td>" + response[i].CIDEFINERELATIONTYPE_NAME + "</td>";
                    ContenedorET += "<td>" + response[i].IDCMDBCIDEFINE_CHILD + "</td>";
                    ContenedorET += "<td>" + response[i].IDCMDBCIDEFINE_PARENT + "</td>";
                    ContenedorET += "<td>" + response[i].IDCMDBRELATIONTYPE + "</td>";
                    ContenedorET += "<td>" + response[i].IDCMDBCI_CHILD + "</td>";
                    ContenedorET += "<td>" + response[i].IDCMDBCI_PARENT + "</td>";
                    ContenedorET += "<td>" + response[i].IDCMDBCIRELATION + "</td>";
                    ContenedorET += "<td>" + response[i].IDCMDBCIDEFINERELATIONTYPE + "</td>";
                    ContenedorET += "</tr>";                   
                }
                if (ListChild)
                    $("#GridRels" + NombreContenedor + IDCMDBCIDEFINERELATIONTYPE + "OR tbody").append(ContenedorET);
                else
                    $("#GridRels" + NombreContenedor + IDCMDBCIDEFINERELATIONTYPE + "IR tbody").append(ContenedorET);

                
            },
            error: function (response) {
                alert(response.ErrorMessage);
            }
        });

        document.location = "#Close";
    });
}


//funciones de los Input relations
function CrearTabPageIR(NombreContenedor) {
    var TabPageIR = "";
    TabPageIR += "<section class='wrapper' id='" + NombreContenedor + "tabSecundarioIR' style='margin-top: 5px;'>";
    TabPageIR += "<ul class='tabs' id='" + NombreContenedor + "tabSecundarioLUIR'>";
    //TabPageOR += "<li id='" + NombreContenedor + "tabBasicoLiOR'><a href='#" + NombreContenedor + "tabBasicOR'>BASIC</a></li>";
    TabPageIR += "</ul>";
    TabPageIR += "<div class='clr'></div>";
    TabPageIR += "<section class='block' id='" + NombreContenedor + "blockSecundarioIR'>";
    //TabPageOR += "<article id='" + NombreContenedor + "tabBasicOR' class='articleSecundario'>";
    TabPageIR += "</section>";
    TabPageIR += "</section>";
    return TabPageIR;
}
function GetRelationsParents(IDCMDBCI, NombreContenedor) {
    var data = {
        'IDCMDBCI': IDCMDBCI
    };

    $.ajax({
        type: "POST",
        url: 'CIEditor.svc/GetTyepeRelationsParents',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //alert(response);
            $('#' + NombreContenedor + 'tabSecundarioLUIR').html("");
            $('#' + NombreContenedor + "blockSecundarioIR").html("");
            for (var i = 0; i < response.length; i++) {
                $('#' + NombreContenedor + 'tabSecundarioLUIR').append("<li class='articleSecundarioIR'><a href='#tab" + NombreContenedor + response[i].IDCMDBCIDEFINERELATIONTYPE + "IR'>" + response[i].CIDEFINERELATIONTYPE_NAME + "</a></li>");
                GetRelationsParentsList(response[i], IDCMDBCI, NombreContenedor);
            }
            GenerarTabsClicksSecundariosIR(NombreContenedor);
            RegistrarEventosGrillaIR(NombreContenedor);
            RegistrarclickRelAddIR(NombreContenedor, IDCMDBCI, false);
            RegistrarclickRelDelIR(NombreContenedor, IDCMDBCI, false);
            RegistrarClickAddRelations(NombreContenedor, IDCMDBCI, false);
        },
        error: function (response) {
            alert(response.ErrorMessage);
        }
    });
}
function GetRelationsParentsList(CMDBCIDEFINERELATIONTYPE, IDCMDBCI, NombreContenedor) {
    var data = {
        'IDCMDBCIDEFINERELATIONTYPE': CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINERELATIONTYPE,
        'IDCMDBCI': IDCMDBCI
    };
    $.ajax({
        type: "POST",
        url: 'CIEditor.svc/GetTyepeRelationsParentsList',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (response) {
            var ContenedorET = "<article id='tab" + NombreContenedor + CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINERELATIONTYPE + "IR' class='articleSecundarioIR ArticulodinamicoIR' style='display: none;'>";
            ContenedorET += "<input class='btnAddIR' data-IdDefRelType='" + CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINERELATIONTYPE + "' data-IdRelType='" + CMDBCIDEFINERELATIONTYPE.IDCMDBRELATIONTYPE + "' value='Add' type='button' style='margin-top:8px;'>";
            ContenedorET += "<input class='btnDeleteIR' data-IdDefRelType='" + CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINERELATIONTYPE + "' data-IdRelType='" + CMDBCIDEFINERELATIONTYPE.IDCMDBRELATIONTYPE + "' value='Delete' type='button' style='margin-top:8px;'>";
            ContenedorET += "<div style='overflow: auto; overflow-y: hidden;'>";
            ContenedorET += "<table id='GridRels" + NombreContenedor + CMDBCIDEFINERELATIONTYPE.IDCMDBCIDEFINERELATIONTYPE + "OR' class='GridFieldStyle gridIR' style='margin-top:5px;'>";
            ContenedorET += "<thead>";
            ContenedorET += "<tr>";
            ContenedorET += "<th>FDMA01</th>";
            ContenedorET += "<th>CI_DATEPLANNED</th>";
            ContenedorET += "<th>CI_DATEIN</th>";
            ContenedorET += "<th>CI_DATEOUT</th>";
            ContenedorET += "<th>CI_GENERICNAME</th>";
            ContenedorET += "<th>CI_DESCRIPTION</th>";
            ContenedorET += "<th>IDCMDBCI</th>";
            ContenedorET += "<th>IDCMDBBRAND</th>";
            ContenedorET += "<th>CI_SERIALNUMBER</th>";
            ContenedorET += "<th>CI_PURCHASEDORRENTED</th>";
            ContenedorET += "<th>CIDEFINE_NAME</th>";
            ContenedorET += "<th>CIDEFINERELATIONTYPE_NAME</th>";
            ContenedorET += "<th>IDCMDBCIDEFINE_CHILD</th>";
            ContenedorET += "<th>IDCMDBCIDEFINE_PARENT</th>";
            ContenedorET += "<th>IDCMDBRELATIONTYPE</th>";
            ContenedorET += "<th>IDCMDBCI_CHILD</th>";
            ContenedorET += "<th>IDCMDBCI_PARENT</th>";
            ContenedorET += "<th>IDCMDBCIRELATION</th>";
            ContenedorET += "<th>IDCMDBCIDEFINERELATIONTYPE</th>";
            ContenedorET += "</tr>";
            ContenedorET += "</thead>";
            ContenedorET += "<tbody>";
            for (var i = 0; i < response.length; i++) {
                ContenedorET += "<tr>";
                ContenedorET += "<td><input type='checkbox'></td>";
                ContenedorET += "<td>" + formatJSONDateTime(response[i].CI_DATEPLANNED) + "</td>";
                ContenedorET += "<td>" + formatJSONDateTime(response[i].CI_DATEIN) + "</td>";
                ContenedorET += "<td>" + formatJSONDateTime(response[i].CI_DATEOUT) + "</td>";
                ContenedorET += "<td>" + response[i].CI_GENERICNAME + "</td>";
                ContenedorET += "<td>" + response[i].CI_DESCRIPTION + "</td>";
                ContenedorET += "<td>" + response[i].IDCMDBCI + "</td>";
                ContenedorET += "<td>" + response[i].IDCMDBBRAND + "</td>";
                ContenedorET += "<td>" + response[i].CI_SERIALNUMBER + "</td>";
                ContenedorET += "<td>" + response[i].CI_PURCHASEDORRENTED + "</td>";
                ContenedorET += "<td>" + response[i].CIDEFINE_NAME + "</td>";
                ContenedorET += "<td>" + response[i].CIDEFINERELATIONTYPE_NAME + "</td>";
                ContenedorET += "<td>" + response[i].IDCMDBCIDEFINE_CHILD + "</td>";
                ContenedorET += "<td>" + response[i].IDCMDBCIDEFINE_PARENT + "</td>";
                ContenedorET += "<td>" + response[i].IDCMDBRELATIONTYPE + "</td>";
                ContenedorET += "<td>" + response[i].IDCMDBCI_CHILD + "</td>";
                ContenedorET += "<td>" + response[i].IDCMDBCI_PARENT + "</td>";
                ContenedorET += "<td>" + response[i].IDCMDBCIRELATION + "</td>";
                ContenedorET += "<td>" + response[i].IDCMDBCIDEFINERELATIONTYPE + "</td>";
                ContenedorET += "</tr>";
            }
            ContenedorET += "</tbody>";
            ContenedorET += "</table>";
            ContenedorET += "</div>";
            ContenedorET += "</article>";

            $('#' + NombreContenedor + "blockSecundarioIR").append(ContenedorET);

            //ROWEXTRATABLE_GET(CMDBCIDEFINEEXTRATABLE, response);
        },
        error: function (response) {
            alert(response.ErrorMessage);
        }
    });

}
function GenerarTabsClicksSecundariosIR(NombreContenedor) {
    $('#' + NombreContenedor + 'tabSecundarioIR #' + NombreContenedor + 'tabSecundarioLUIR.tabs li:first').addClass('active');
    $('#' + NombreContenedor + 'tabSecundarioIR #' + NombreContenedor + 'blockSecundarioIR.block .articleSecundarioIR').hide();
    $('#' + NombreContenedor + 'tabSecundarioIR #' + NombreContenedor + 'blockSecundarioIR.block article:first').show();
    $('#' + NombreContenedor + 'tabSecundarioIR #' + NombreContenedor + 'tabSecundarioLUIR.tabs').on('click', 'li', function () {
        $('#' + NombreContenedor + 'tabSecundarioIR #' + NombreContenedor + 'tabSecundarioLUIR.tabs li').removeClass('active');
        $(this).addClass('active')
        $('#' + NombreContenedor + 'tabSecundarioIR #' + NombreContenedor + 'blockSecundarioIR.block .articleSecundarioIR').hide();
        var activeTab = $(this).find('a').attr('href');
        $(activeTab).show();
        return false;
    });
}
function RegistrarEventosGrillaIR(NombreContenedor) {
    var rowOldiR;
    ///carga el ci elegido 
    $("#" + NombreContenedor + " .gridIR tbody").on('click', 'tr', function (e) {
        //e.preventDefault();
        //var IDCMDBCI = $(this).attr('data-IdCI');
        $(rowOldiR).removeClass('rowSelect');
        $(this).addClass('rowSelect');
        rowOldiR = this;
    });
}
function RegistrarclickRelAddIR(NombreContenedor, IDCMDBCI, ListChild) {
    $("#" + NombreContenedor).off('click', '.btnAddIR');
    $("#" + NombreContenedor).on('click', '.btnAddIR', function () {
        IDCMDBCIDEFINERELATIONTYPE = parseInt($(this).attr('data-IdDefRelType'));
        IDCMDBRELATIONTYPE = parseInt($(this).attr('data-IdRelType'));

        $('#' + NombreContenedor + 'AddRel').attr('data-IdDefRelType', IDCMDBCIDEFINERELATIONTYPE);
        $('#' + NombreContenedor + 'AddRel').attr('data-IdRelType', IDCMDBRELATIONTYPE);

        GetRelationsXAdd(IDCMDBCIDEFINERELATIONTYPE, IDCMDBCI, ListChild, NombreContenedor, IDCMDBRELATIONTYPE);

        document.location = '#' + NombreContenedor + 'ModalRel';
    });
}
function RegistrarclickRelDelIR(NombreContenedor, IDCMDBCI, ListChild) {
    $("#" + NombreContenedor).off('click', '.btnDeleteIR');
    $("#" + NombreContenedor).on('click', '.btnDeleteIR', function () {
        IDCMDBCIDEFINERELATIONTYPE = parseInt($(this).attr('data-IdDefRelType'));
        IDCMDBRELATIONTYPE = parseInt($(this).attr('data-IdRelType'));

        var _ListRel = [];
        var _ListRows = [];
        var cont = 0;

        $('#GridRels' + NombreContenedor + IDCMDBCIDEFINERELATIONTYPE + 'IR tbody tr').each(function (index2) {
            var isSelect = $('#GridRels' + NombreContenedor + IDCMDBCIDEFINERELATIONTYPE + 'IR tbody tr:eq(' + index2 + ')').find('td:eq(0) input').is(':checked')

            if (isSelect) {
                var _IDCMDBRELATIONTYPE;
                if (IDCMDBRELATIONTYPE != 0)
                    _IDCMDBRELATIONTYPE = IDCMDBRELATIONTYPE;
                else
                    _IDCMDBRELATIONTYPE = parseInt($(this).find("td").eq(13).html());

                var _TCMDBCIRELATION = {
                    IDCMDBCIRELATION: parseInt($(this).find("td").eq(17).html()),
                    IDCMDBCI_CHILD: parseInt($(this).find("td").eq(6).html()),
                    IDCMDBCI_PARENT: parseInt(IDCMDBCI),
                    IDCMDBRELATIONTYPE: _IDCMDBRELATIONTYPE
                }
                _ListRows[cont] = this;
                _ListRel[cont] = _TCMDBCIRELATION;
                cont += 1;
            }

            $.ajax({
                type: "POST",
                url: 'CIEditor.svc/DelRelations',
                data: JSON.stringify({ ListRel: _ListRel, IDCMDBCI: IDCMDBCI, ListChild: ListChild }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (response) {
                    if (response) {
                        for (var i = 0; i < _ListRows.length; i++) {
                            $(_ListRows[i]).remove();
                        }
                    }
                },
                error: function (response) {
                    alert(response.ErrorMessage);
                }
            });
        });
    });
}



//funciones de inicializacion del componente
function Inicialize(NombreEditor) {
    //$('#' + NombreEditor).css('background-color', 'white');
    CrearGrilla(NombreEditor);
    RegistrarEventosGrilla(NombreEditor);
    CrearCIEditor(NombreEditor);
    GenerarTabsClicksPrimarios(NombreEditor);
    GenerarTabsClicksSecundarios(NombreEditor);
    GrabarCMDBExtraTable(NombreEditor);
    EliminarCMDBExtraTable(NombreEditor);
    CancelarCMDBExtraTable(NombreEditor);
    NuevoCMDBExtraTable(NombreEditor);
    
}

$(document).ready(function () {
    Inicialize("CtrEditor1");
    
    //Inicialize("CtrEditor2");
    //Prueba();
});



function formatJSONDateTime(jsonDate) {
    var dateString = jsonDate.substr(6);
    var currentTime = new Date(parseInt(dateString));
    var month = currentTime.getMonth() + 1;
    var day = currentTime.getDate();
    var year = currentTime.getFullYear();
    var hour = currentTime.getHours();
    var min = currentTime.getMinutes();
    var date = day + "/" + month + "/" + year
    return date;
}

function formatTextDateTime(dateString) {
    var currentTime = new Date(dateString);
    var month = currentTime.getMonth() + 1;
    var day = currentTime.getDate();
    var year = currentTime.getFullYear();
    var hour = currentTime.getHours();
    var min = currentTime.getMinutes();
    var date = day + "/" + month + "/" + year;
    return date;
}

function formatTextDate(dateString) {
    var currentTime = new Date(dateString);
    var month = currentTime.getMonth() + 1;
    var day = currentTime.getDate();
    var year = currentTime.getFullYear();
    var hour = currentTime.getHours();
    var min = currentTime.getMinutes();
    var date = day + "/" + month + "/" + year
    return date;
}

function formatTextTime(dateString) {
    var currentTime = new Date(dateString);
    var month = currentTime.getMonth() + 1;
    var day = currentTime.getDate();
    var year = currentTime.getFullYear();
    var hour = currentTime.getHours();
    var min = currentTime.getMinutes();
    var date = day + "/" + month + "/" + year
    return date;
}

//function Prueba() {

//    $("#btnProbar").click(function () {

//        Prueba2();

//        var date = new Date();
//        date = "\/Date(" + date.valueOf() + ")\/";
//        var arreglo = [["Pinball", "Holiday camp"], ["nuevo", "nuevo 2"]];

//        var _varSegundo = {
//            VarEntera: 2,
//            VarDecimal: 2.5,
//            VarDouble: 3.5
//            //varArray : arreglo
//        }

//        var _varObjeto = {
//            VarEntera: 1,
//            VarCadena: 'hola',
//            VarDateTime: date,
//            SegundoObjeto: _varSegundo
//        }
//        _data = JSON.stringify({ NuevoObjeto: _varObjeto, nombre: 'carlos', arrayString: arreglo });
//        //_data = JSON.stringify(NuevoObjeto);
//        $.ajax({
//            type: "POST",
//            url: "CIEditor.svc/SetNuevoObjeto",
//            contentType: "application/json; charset=utf-8",
//            data: _data,
//            success: function (response) {
//                alert(response);
//            },
//            error: function (response) {
//                alert(response);
//            }
//        });
//    });

//}

//function Prueba2() {
//    $("#btnProbar2").click(function () {
//        alert("si sale por este medio");
//    });
//}



function myfunction() {
    switch (CMDBCIDEFINEEXTRAFIELDS.IDCMDBDBDATATYPES) {
        case TDataType.String: // SysCfg.DB.Properties.TDataType.String   
            switch (CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                case TExtrafields_ColumnStyle.None:                   
                    break;
                case TExtrafields_ColumnStyle.TextPassword:                    
                    break;
                case TExtrafields_ColumnStyle.LookUp:
                    break;
                case TExtrafields_ColumnStyle.LookUpOption:
                    break;
            }
            break;
        case TDataType.Text: //SysCfg.DB.Properties.TDataType.Text     
            switch (CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                case TExtrafields_ColumnStyle.None:
                    break;
            }
            break;
        case TDataType.Int32: // SysCfg.DB.Properties.TDataType.Int32:    
            switch (CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                case TExtrafields_ColumnStyle.None:
                    break;
                case TExtrafields_ColumnStyle.LookUp:
                     break;
                case TExtrafields_ColumnStyle.LookUpOption:
                     break;
            }
            break;
        case TDataType.Double: //SysCfg.DB.Properties.TDataType.Double:  
            switch (CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                case TExtrafields_ColumnStyle.None:
                    break;
            }
            break;
        case TDataType.Boolean: //SysCfg.DB.Properties.TDataType.Boolean: 
            switch (CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                case TExtrafields_ColumnStyle.None:
                     break;
            }
            break;
        case TDataType.DateTime: //SysCfg.DB.Properties.TDataType.DateTime:   
            switch (CMDBCIDEFINEEXTRAFIELDS.EXTRAFIELDS_COLUMNSTYLE) {
                case TExtrafields_ColumnStyle.None:
                    break;
                case TExtrafields_ColumnStyle.Date:
                    break;
                case TExtrafields_ColumnStyle.Time:
                    break;
            }
            break;
    }
}