﻿$(document).ready(function () {
    Inicialize("CIDefine1");
});

function Inicialize(NombreCIDefine) {
    CreaDivTree(NombreCIDefine);
    CreaDivEditor(NombreCIDefine);
    Add(NombreCIDefine);
    Grabar(NombreCIDefine);
    Edit(NombreCIDefine);
   
}

function CreaDivTree(NombreCIDefine) {
    var DivTree = "";
    DivTree += "<div id='ContentTree" + NombreCIDefine + "'>";
    DivTree += "<div id='ContentTreeBtns" + NombreCIDefine + "'>";
    DivTree += "<input id='btnNuevo" + NombreCIDefine + "' value='Add' type='button' />";
    DivTree += "<input id='btnElimnar" + NombreCIDefine + "' value='Delete' type='button' />";
    DivTree += "<input id='btnEditar" + NombreCIDefine + "' value='Edit' type='button' />";
    DivTree += "</div>";
    DivTree += "<div id='Tree" + NombreCIDefine + "'>";
    DivTree += "</div>";
    DivTree += "</div>";
    $("#" + NombreCIDefine).append(DivTree);
    CargarTree("Tree" + NombreCIDefine);
}

function CargarTree(NombreTree) {
    //cargar el tree
    $("#" + NombreTree).dynatree({
        imagePath: ' ',
        isLazy: true,
        fx: { height: "toggle", duration: 200 },
        persist: false,
        async:false,
        initAjax: {
            type: "POST",
            url: "CIDefine.svc/GetCIDefineXIDParent",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ key: 0 })
        },
        onLazyRead: function (dtnode) {
            dtnode.appendAjax({
                type: "POST",
                url: "CIDefine.svc/GetCIDefineXIDParent2",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({ key: parseInt(dtnode.data.key) })               

            });
        },
        onExpand: function (bandera, dtnode) {
            if (bandera) {
                dtnode.appendAjax({
                    type: "POST",
                    url: "CIDefine.svc/GetCIDefineXIDParent2",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({ key: parseInt(dtnode.data.key) })
                });
            }
        },
        dnd: {
            preventVoidMoves: true, // Prevent dropping nodes 'before self', etc.
            onDragStart: function (node) {
                /** This function MUST be defined to enable dragging for the tree.
                 *  Return false to cancel dragging of node.
                 */
                return true;
            },
            onDragEnter: function (node, sourceNode) {
                /** sourceNode may be null for non-dynatree droppables.
                 *  Return false to disallow dropping on node. In this case
                 *  onDragOver and onDragLeave are not called.
                 *  Return 'over', 'before, or 'after' to force a hitMode.
                 *  Return ['before', 'after'] to restrict available hitModes.
                 *  Any other return value will calc the hitMode from the cursor position.
                 */
                // Prevent dropping a parent below another parent (only sort
                // nodes under the same parent)
                if (node.parent !== sourceNode.parent) {
                    return false;
                }
                // Don't allow dropping *over* a node (would create a child)
                return ["before", "after"];
            },
            onDrop: function (node, sourceNode, hitMode, ui, draggable) {
                /** This function MUST be defined to enable dropping of items on
                 *  the tree.
                 */
                sourceNode.move(node, hitMode);
            }

        }
    });
}

function CreaDivEditor(NombreCIDefine) {
    var DivContEditor = "";
    DivContEditor += "<div id='ContentEditor" + NombreCIDefine + "' style='display:none;'>"
    DivContEditor += "<div id='ContentBotones" + NombreCIDefine + "'>";
    DivContEditor += "</div>";
    DivContEditor += "<div id='ContentControles" + NombreCIDefine + "'>";
    DivContEditor += "<table>";
    DivContEditor += "<tr>";
    DivContEditor += "<td></td>";
    DivContEditor += "<td>";
    DivContEditor += "<input id='stateForm" + NombreCIDefine + "' type='hidden' value='0'/>";
    DivContEditor += "<input id='txtIDCMDBCIDEFINE" + NombreCIDefine + "' type='hidden' value='0'/>";
    DivContEditor += "<input id='txtIDCMDBCIDEFINESUPERCLASS" + NombreCIDefine + "' type='hidden' value='0'/>";
    DivContEditor += "<input id='txtCIDEFINE_IDFILE" + NombreCIDefine + "' type='hidden' value='0'/>";
    DivContEditor += "<input type='radio' name='Nivel" + NombreCIDefine + "' value='Brother' checked>Brother";
    DivContEditor += "<input type='radio' name='Nivel" + NombreCIDefine + "' value='Child'>Child";
    DivContEditor += "</td>";
    DivContEditor += "</tr>";
    DivContEditor += "<tr>";
    DivContEditor += "<td>";
    DivContEditor += "CI Define :";
    DivContEditor += "</td>";    
    DivContEditor += "<td>";
    DivContEditor += "<input id='txtName" + NombreCIDefine + "' type='text' />";
    DivContEditor += "</td>";
    DivContEditor += "</tr>";
    DivContEditor += "<tr>";
    DivContEditor += "<td>";
    DivContEditor += "CI Define Description :";
    DivContEditor += "</td>";
    DivContEditor += "<td>";
    DivContEditor += "<input id='txtDescripcion" + NombreCIDefine + "' type='text' />";
    DivContEditor += "</td>";
    DivContEditor += "</tr>";
    DivContEditor += "<tr>";
    DivContEditor += "<td>";
    DivContEditor += "CI Define Type :";
    DivContEditor += "</td>";
    DivContEditor += "<td>";
    DivContEditor += "<select id='cbDefineType" + NombreCIDefine + "' />";
    DivContEditor += "</td>";
    DivContEditor += "</tr>";
    DivContEditor += "<tr style='display:none;'>";
    DivContEditor += "<td>";
    DivContEditor += "Img File :";
    DivContEditor += "</td>";
    DivContEditor += "<td>";
    DivContEditor += "<input id='txtImg" + NombreCIDefine + "' type='file'>";
    DivContEditor += "</td>";
    DivContEditor += "</tr>";
    DivContEditor += "<tr>";
    DivContEditor += "<td>";
    DivContEditor += "Img File :";
    DivContEditor += "</td>";
    DivContEditor += "<td>";
    DivContEditor += "<input id='txtNameImgOld" + NombreCIDefine + "' type='text' disabled style='display:none;'>";
    DivContEditor += "<input id='txtNameImg" + NombreCIDefine + "' type='text' disabled ><input id='btnImg" + NombreCIDefine + "' type='button' value='Examinar'/>";
    DivContEditor += "</td>";
    DivContEditor += "</tr>";
    DivContEditor += "<tr>";
    DivContEditor += "<td>";
    DivContEditor += "</td>";
    DivContEditor += "<td>";
    DivContEditor += "<img id='picImg" + NombreCIDefine + "' src='' />";
    DivContEditor += "</td>";
    DivContEditor += "</tr>";
    DivContEditor += "<tr>";
    DivContEditor += "<td>";
    DivContEditor += "</td>";
    DivContEditor += "<td>";
    DivContEditor += "<input id='btnGrabar" + NombreCIDefine + "' value='Save' type='button' />";
    DivContEditor += "</td>";
    DivContEditor += "</tr>";
    DivContEditor += "</table>";
    DivContEditor += "</div>";
    DivContEditor += "</div>";
    $("#" + NombreCIDefine).append(DivContEditor);
    CargarTypeDefine("cbDefineType" + NombreCIDefine);
    ClickFileUpload(NombreCIDefine);
}

function ClickFileUpload(NombreCIDefine) {
    $("#btnImg" + NombreCIDefine).off("click");
    $("#btnImg" + NombreCIDefine).on("click", function (event) {
        event.preventDefault();
        $("#txtImg" + NombreCIDefine).click();
    });

    $("#txtImg" + NombreCIDefine).off("change")
    $("#txtImg" + NombreCIDefine).on("change", function() {        
        fileData = document.getElementById("txtImg" + NombreCIDefine).files[0];
        if (fileData != undefined)
            $("#txtNameImg" + NombreCIDefine).val(fileData.name);
    });


}

function CargarTypeDefine(ComboId) {
    $.ajax({
        type: "POST",
        url: 'CIDefine.svc/GetCIDefineType',
        data: {},
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async:false,
        success: function (response) {
            for (var i = 0; i < response.length; i++) {
                $("#" + ComboId).append("<option value='" + response[i].ValueMember + "'>" + response[i].DisplayMember + "</option>");
            }
            
        },
        error: function (response) {
            SysCfg.Log.Methods.WriteLog("frCIDefine.js CargarTypeDefine()" + response.ErrorMessage);
        }
    });
}

function Grabar(NombreCIDefine)
{
    $("#btnGrabar" + NombreCIDefine).off('click');
    $("#btnGrabar" + NombreCIDefine).on('click', function () {
        fileData = document.getElementById("txtImg" + NombreCIDefine).files[0];
        //var data = new FormData();

        var _StateForm = $("#stateForm" + NombreCIDefine).val();
        var _CIDEFINE_IDFILE = $("#txtCIDEFINE_IDFILE" + NombreCIDefine).val();
        var _CIDEFINE_FILENAME = $("#txtNameImg" + NombreCIDefine).val();
        var _CIDEFINE_FILENAMEOLD = $("#txtNameImgOld" + NombreCIDefine).val();

        var Path;
        var Bandera = false;

        if (_StateForm._Nuevo)
            Path = "CIDefine.svc/InsertFile";
        else {
            if (_CIDEFINE_FILENAME == "" || _CIDEFINE_IDFILE == "")
                Path = "CIDefine.svc/InsertFile";
            else {
                if (_CIDEFINE_FILENAME != _CIDEFINE_FILENAMEOLD)
                    Path = "CIDefine.svc/UpdateFile";
                else
                    Bandera = true;
            }
        }

        $.ajax({
            url: Path,
            type: 'POST',
            data: fileData,
            cache: false,
            dataType: 'json',
            async: false,
            processData: false, // Don't process the files
            contentType: "application/octet-stream", // Set content type to false as jQuery will tell the server its a query string request
            success: function (GetSDfile) {
                //var data = new FormData();
                var node = $("#Tree" + NombreCIDefine).dynatree("getActiveNode");

                var files = $("#txtImg" + NombreCIDefine).get(0).files;
                var _BYTEIMAGE;

                var _IDCMDBCIDEFINE = parseInt($('#txtIDCMDBCIDEFINE' + NombreCIDefine).val());
                var _CIDEFINE_INDEX = 0;
                var _CIDEFINE_DESCRIPTION = $('#txtDescripcion' + NombreCIDefine).val();
                var _IDCMDBCIDEFINELEVEL = 0;
                var _CIDEFINE_NAME = $('#txtName' + NombreCIDefine).val();
                if (node != null)
                    var _IDCMDBCIDEFINESUPERCLASS = (node.data.key != "_1") ? node.data.key : 0; // $('#txtIDCMDBCIDEFINESUPERCLASS' + NombreCIDefine).val();
                var _IDCMDBCIDEFINETYPE = $('#cbDefineType' + NombreCIDefine).val();

                if (!Bandera)
                    _CIDEFINE_IDFILE = GetSDfile.IDCode;

                var _CMDBCIDEFINE = {
                    IDCMDBCIDEFINE: _IDCMDBCIDEFINE,
                    CIDEFINE_INDEX: _CIDEFINE_INDEX,
                    CIDEFINE_DESCRIPTION: _CIDEFINE_DESCRIPTION,
                    IDCMDBCIDEFINELEVEL: _IDCMDBCIDEFINELEVEL,
                    CIDEFINE_NAME: _CIDEFINE_NAME,
                    IDCMDBCIDEFINESUPERCLASS: _IDCMDBCIDEFINESUPERCLASS,
                    CIDEFINE_IDFILE: _CIDEFINE_IDFILE,
                    CIDEFINE_FILENAME: fileData.name,
                    IDCMDBCIDEFINETYPE: _IDCMDBCIDEFINETYPE,
                };

                

                var _IsBrother = $("input[name=Nivel" + NombreCIDefine + "]:checked").val() == "Brother" ? true : false;

                if (_StateForm == StateFormulario._Nuevo) {
                    if (!_IsBrother) {
                        if (node != null)
                            _CMDBCIDEFINE.IDCMDBCIDEFINESUPERCLASS = (node.data.key != "_1") ? node.data.key : 0;
                    }
                    else {
                        _CMDBCIDEFINE.IDCMDBCIDEFINESUPERCLASS = parseInt($('#txtIDCMDBCIDEFINESUPERCLASS' + NombreCIDefine).val());
                    }
                }
                
                var _data = JSON.stringify({ CMDBCIDEFINE: _CMDBCIDEFINE, StateForm: _StateForm, IsBrother: _IsBrother });

                $.ajax({
                    type: "POST",
                    url: "CIDefine.svc/GrabarCIDEFINE",
                    contentType: "application/json; charset=utf-8",
                    data: _data,
                    async: false,
                    success: function (response) {
                        if (_StateForm == StateFormulario._update)
                            node.setTitle(_CMDBCIDEFINE.CIDEFINE_NAME);

                        $("#Tree" + NombreCIDefine).dynatree("getTree").reload();

                        $("#ContentEditor" + NombreCIDefine).hide();
                        alert('Se grabo correctamente.');
                    },
                    error: function (response) {
                        SysCfg.Log.Methods.WriteLog("frCIDefine.js Grabar() CIDefine.svc/GrabarCIDEFINE " + response);                        
                    }
                });
            },
            error: function (data) {
                SysCfg.Log.Methods.WriteLog("frCIDefine.js Grabar() " + Path + " Some error Occurred! ");
            }
        });


        

        

    });
}

function Add(NombreCIDefine) {
    $("#btnNuevo" + NombreCIDefine).off('click');
    $("#btnNuevo" + NombreCIDefine).on('click', function () {
        $("#stateForm" + NombreCIDefine).val(StateFormulario._Nuevo);
        LimpiarControles(NombreCIDefine);
        $("#ContentEditor" + NombreCIDefine).show();
    });

}

function Remove(NombreCIDefine) {
    $("#btnElimnar" + NombreCIDefine).off('click');
    $("#btnElimnar" + NombreCIDefine).on('click', function () {

    });
}

function Edit(NombreCIDefine) {
    $("#btnEditar" + NombreCIDefine).off('click');
    $("#btnEditar" + NombreCIDefine).on('click', function () {

        var node = $("#Tree" + NombreCIDefine).dynatree("getActiveNode");
        if (node.isFolder)
            return;

        var IDCMDBCIDEFINE = 0;
        if (node != null)
            IDCMDBCIDEFINE = (node.data.key != "_1") ? node.data.key : 0;

        var data = {
            'IDCMDBCIDEFINE': IDCMDBCIDEFINE
        };

        if (IDCMDBCIDEFINE > 0) {
            $.ajax({
                type: "POST",
                url: 'CIDefine.svc/GetCIDefineXIDCMDBCIDEFINE',
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    $("#txtIDCMDBCIDEFINE" + NombreCIDefine).val(response.IDCMDBCIDEFINE);
                    $("#txtIDCMDBCIDEFINESUPERCLASS" + NombreCIDefine).val(response.IDCMDBCIDEFINESUPERCLASS);
                    $("#cbDefineType" + NombreCIDefine).val(response.IDCMDBCIDEFINETYPE);
                    $("#txtName" + NombreCIDefine).val(response.CIDEFINE_NAME);
                    $("#txtDescripcion" + NombreCIDefine).val(response.CIDEFINE_DESCRIPTION);
                    $("#txtNameImg" + NombreCIDefine).val(response.CIDEFINE_FILENAME);
                    $("#txtNameImgOld" + NombreCIDefine).val(response.CIDEFINE_FILENAME);
                    $("#txtCIDEFINE_IDFILE" + NombreCIDefine).val(response.CIDEFINE_IDFILE);
                    //$("#picImg" + NombreCIDefine).attr('src', "data:image/png;base64," + response.BYTEIMAGE);
                    document.getElementById("picImg" + NombreCIDefine).src = "data:image/png;base64," + arrayBufferToBase64(response.BYTEIMAGE);
                    $("#txtImg" + NombreCIDefine).val("");
                    $("#stateForm" + NombreCIDefine).val(StateFormulario._update);
                    $("#ContentEditor" + NombreCIDefine).show();

                },
                error: function (response) {
                    SysCfg.Log.Methods.WriteLog("frCIDefine.js Edit() " + response.ErrorMessage);
                }
            });
        }
        else
            alert("Seleccione un nodo");
    });
}

function arrayBufferToBase64(buffer) {
    var binary = '';
    var bytes = new Uint8Array(buffer);
    var len = bytes.byteLength;
    for (var i = 0; i < len; i++) {
        binary += String.fromCharCode(bytes[i]);
    }
    return window.btoa(binary);
}

function base64ToArrayBuffer(base64) {
    var binary_string = window.atob(base64);
    var len = binary_string.length;
    var bytes = new Uint8Array(len);
    for (var i = 0; i < len; i++) {
        bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes.buffer;
}

function LimpiarControles(NombreCIDefine) {
    $("#txtIDCMDBCIDEFINE" + NombreCIDefine).val("0");
    $("#txtIDCMDBCIDEFINESUPERCLASS" + NombreCIDefine).val("0");
    $("#txtCIDEFINE_IDFILE" + NombreCIDefine).val("");
    $("#txtNameImgOld" + NombreCIDefine).val("");
    $("#txtNameImg" + NombreCIDefine).val("");
    $("#txtName" + NombreCIDefine).val("");
    $("#txtDescripcion" + NombreCIDefine).val("");
    $("#txtImg" + NombreCIDefine).val("");
}

/*
StateFormulario = {
    _Insert: 0,
    _update: 1,
    _Delete: 2,
    _Nuevo: 3,
    _Cancel: 4
}*/