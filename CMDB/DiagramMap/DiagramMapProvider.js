﻿ItHelpCenter.CMDB.DiagramMap.DiagramMapProvider = function () {

}

ItHelpCenter.CMDB.DiagramMap.DiagramMapProvider.prototype.BuilMapCMDB = function (DiagramMap, _Diag, AssetManager) {
    DiagramMap.PreparedDiagram();

    var _node = new ItHelpCenter.ComponentMindFusionManagerDiagramingProvider.TNode(AssetManager.SETCI.IDCMDBCI.toString(), AssetManager.SETCI.CI_GENERICNAME)
    //_node.Background = Colors.LightGray;
    _node.Expanded = true;
    _node.Titulo = this.GetTitulo1(AssetManager.SETCI);
    _node.SubTitulo = AssetManager.SETCI.CI_GENERICNAME;
    _node.Visible = true;

    DiagramMap.AddNode(_node);

    for (var i = 0; i < AssetManager.SETCI.ListFriends.length; i++) {
        var item = AssetManager.SETCI.ListFriends[i];
        var _n = new ItHelpCenter.ComponentMindFusionManagerDiagramingProvider.TNode(item.IDCMDBCI.toString(), item.CI_GENERICNAME);
        //_n.Background = Colors.LightGray;
        _n.Expanded = false;
        _n.Titulo = this.GetTitulo2(item.IDCMDBCI, item.CIDEFINE_NAME);
        _n.SubTitulo = item.CI_GENERICNAME;
        _n.Visible = false;
        //_n.BorderColor = new SolidColorBrush(Colors.Black);

        DiagramMap.AddNode(_n);
        DiagramMap.AddLink(new ItHelpCenter.ComponentMindFusionManagerDiagramingProvider.TLink(_node, _n));
    }

    for (var i = 0; i < AssetManager.SETCI.ListParent.length; i++) {
        var item = AssetManager.SETCI.ListParent[i];
        var _n = new ItHelpCenter.ComponentMindFusionManagerDiagramingProvider.TNode(item.IDCMDBCI.toString(), item.CI_GENERICNAME);
        //_n.Background = Colors.LightGray;
        _n.Expanded = false;
        _n.Titulo = this.GetTitulo2(item.IDCMDBCI, item.CIDEFINE_NAME);
        _n.SubTitulo = item.CI_GENERICNAME;
        _n.Visible = false;
        //_n.BorderColor = new SolidColorBrush(Colors.Black);

        DiagramMap.AddNode(_n);
        DiagramMap.AddLink(new ItHelpCenter.ComponentMindFusionManagerDiagramingProvider.TLink(_node, _n));
    }

    for (var i = 0; i < AssetManager.SETCI.ListChildren.length; i++) {
        var item = AssetManager.SETCI.ListChildren[i];
        var _n = new ItHelpCenter.ComponentMindFusionManagerDiagramingProvider.TNode(item.IDCMDBCI.toString(), item.CI_GENERICNAME);
        //_n.Background = Colors.LightGray;
        _n.Expanded = false;
        _n.Titulo = this.GetTitulo2(item.IDCMDBCI, item.CIDEFINE_NAME);
        _n.SubTitulo = item.CI_GENERICNAME;
        _n.Visible = false;
        //_n.BorderColor = new SolidColorBrush(Colors.Black);

        DiagramMap.AddNode(_n);
        DiagramMap.AddLink(new ItHelpCenter.ComponentMindFusionManagerDiagramingProvider.TLink(_node, _n));
    }

    //_Diag.AlignToGrid = true;
    DiagramMap.ConstruirMapaFractal(_Diag);
}

ItHelpCenter.CMDB.DiagramMap.DiagramMapProvider.prototype.UpdateMapCMDB = function (DiagramMap, _Diag, AssetManager) {
    var _node = new ItHelpCenter.ComponentMindFusionManagerDiagramingProvider.TNode(AssetManager.SETCI.IDCMDBCI.toString(), AssetManager.SETCI.CI_GENERICNAME)
    //_node.Background = Colors.LightGray;
    _node.Expanded = true;
    _node.Titulo = this.GetTitulo1(AssetManager.SETCI);
    _node.SubTitulo = AssetManager.SETCI.CI_GENERICNAME;
    _node.Visible = true;

    DiagramMap.AddOrUpdateNode(_node);

    for (var i = 0; i < AssetManager.SETCI.ListFriends.length; i++) {
        var item = AssetManager.SETCI.ListFriends[i];
        var _n = new ItHelpCenter.ComponentMindFusionManagerDiagramingProvider.TNode(item.IDCMDBCI.toString(), item.CI_GENERICNAME);
        //_n.Background = Colors.LightGray;
        _n.Expanded = false;
        _n.Titulo = this.GetTitulo2(item.IDCMDBCI, item.CIDEFINE_NAME);
        _n.SubTitulo = item.CI_GENERICNAME;
        _n.Visible = false;
        //_n.BorderColor = new SolidColorBrush(Colors.Black);

        DiagramMap.AddOrUpdateNode(_n);
        DiagramMap.AddOrUpdateLink(new ItHelpCenter.ComponentMindFusionManagerDiagramingProvider.TLink(_node, _n));
    }

    for (var i = 0; i < AssetManager.SETCI.ListParent.length; i++) {
        var item = AssetManager.SETCI.ListParent[i];
        var _n = new ItHelpCenter.ComponentMindFusionManagerDiagramingProvider.TNode(item.IDCMDBCI.toString(), item.CI_GENERICNAME);
        //_n.Background = Colors.LightGray;
        _n.Expanded = false;
        _n.Titulo = this.GetTitulo2(item.IDCMDBCI, item.CIDEFINE_NAME);
        _n.SubTitulo = item.CI_GENERICNAME;
        _n.Visible = false;
        //_n.BorderColor = new SolidColorBrush(Colors.Black);

        DiagramMap.AddOrUpdateNode(_n);
        DiagramMap.AddOrUpdateLink(new ItHelpCenter.ComponentMindFusionManagerDiagramingProvider.TLink(_node, _n));
    }

    for (var i = 0; i < AssetManager.SETCI.ListChildren.length; i++) {
        var item = AssetManager.SETCI.ListChildren[i];
        var _n = new ItHelpCenter.ComponentMindFusionManagerDiagramingProvider.TNode(item.IDCMDBCI.toString(), item.CI_GENERICNAME);
        //_n.Background = Colors.LightGray;
        _n.Expanded = false;
        _n.Titulo = this.GetTitulo2(item.IDCMDBCI, item.CIDEFINE_NAME);
        _n.SubTitulo = item.CI_GENERICNAME;
        _n.Visible = false;
        //_n.BorderColor = new SolidColorBrush(Colors.Black);

        DiagramMap.AddOrUpdateNode(_n);
        DiagramMap.AddOrUpdateLink(new ItHelpCenter.ComponentMindFusionManagerDiagramingProvider.TLink(_node, _n));
    }

    DiagramMap.ConstruirMapaFractal(_Diag);
}

ItHelpCenter.CMDB.DiagramMap.DiagramMapProvider.prototype.GetTitulo1 = function (item) {
    return "(" + item.IDCMDBCI.toString() + ") " + item.CIDEFINE_NAME;
}

ItHelpCenter.CMDB.DiagramMap.DiagramMapProvider.prototype.GetTitulo2 = function (id, name) {
    return "(" + id.toString() + ") " + name;
}


ItHelpCenter.CMDB.DiagramMap.DiagramMapProvider.prototype.BuildMapRealations = function (DiagramMap, _Diag, AssetManager) {

    DiagramMap.PreparedDiagram();

    var _node = new ItHelpCenter.ComponentMindFusionManagerDiagramingProvider.TNode(AssetManager.SETCI.IDCMDBCI.toString(), AssetManager.SETCI.CI_GENERICNAME)
    //_node.Background = Colors.LightGray;
    _node.Id = AssetManager.SETCI.IDCMDBCI.toString();
    _node.Titulo = AssetManager.SETCI.CI_GENERICNAME;
    _node.Text = this.GET_CIInformation(AssetManager.SETCI);
    _node.Visible = true;
    //_node.BorderColor = new SolidColorBrush(Colors.Orange);
    _node.Imagen = this.ByteToImage(AssetManager.SETCI.CMDBCIDEFINE.BYTEIMAGE);

    DiagramMap.AddNode(_node);


    for (var i = 0; i < AssetManager.SETCI.CIRELATIONListChild.length; i++) {
        var item = AssetManager.SETCI.CIRELATIONListChild[i];
        var _n = new ItHelpCenter.ComponentMindFusionManagerDiagramingProvider.TNode(item.CI_CHILD.IDCMDBCI.toString(), item.CI_CHILD.CI_GENERICNAME);
        _n.Id = item.CI_CHILD.IDCMDBCI.toString();
        _n.Titulo = item.CI_CHILD.CI_GENERICNAME;
        _n.Text = this.GET_CIInformation(item.CI_CHILD);
        _n.Visible = true;
        //_n.BorderColor = new SolidColorBrush(Colors.Black);
        _n.Imagen = this.ByteToImage(item.CI_CHILD.CMDBCIDEFINE.BYTEIMAGE);
        DiagramMap.AddNode(_n);
        var _l = new ItHelpCenter.ComponentMindFusionManagerDiagramingProvider.TLink(_node, _n);
        _l.Text = item.CMDBCIDEFINERELATIONTYPE.CIDEFINERELATIONTYPE_NAME;
        DiagramMap.AddLink(_l);
    }

    for (var i = 0; i < AssetManager.SETCI.CIRELATIONListParent.length; i++) {
        var item = AssetManager.SETCI.CIRELATIONListParent[i];
        var _n = new ItHelpCenter.ComponentMindFusionManagerDiagramingProvider.TNode(item.CI_PARENT.IDCMDBCI.toString(), item.CI_PARENT.CI_GENERICNAME);
        _n.Id = item.CI_PARENT.IDCMDBCI.toString();
        _n.Titulo = item.CI_PARENT.CI_GENERICNAME;
        _n.Text = this.GET_CIInformation(item.CI_PARENT);
        _n.Visible = true;
        //_n.BorderColor = new SolidColorBrush(Colors.Black);
        _n.Imagen = this.ByteToImage(item.CI_CHILD.CMDBCIDEFINE.BYTEIMAGE);
        DiagramMap.AddNode(_n);
        var _l = new ItHelpCenter.ComponentMindFusionManagerDiagramingProvider.TLink(_node, _n);
        _l.Text = item.CMDBCIDEFINERELATIONTYPE.CIDEFINERELATIONTYPE_NAME;
        DiagramMap.AddLink(_l);
    }

    DiagramMap.BuildDiagramRelations(_Diag, true);
}

ItHelpCenter.CMDB.DiagramMap.DiagramMapProvider.prototype.BuildMapRealationsClicked = function (DiagramMap, _Diag, AssetManager) {
    var _node = DiagramMap.FindNode(AssetManager.SETCI.IDCMDBCI.toString());

    for (var i = 0; i < AssetManager.SETCI.CIRELATIONListChild.length; i++) {
        var item = AssetManager.SETCI.CIRELATIONListChild[i];
        if (DiagramMap.FindNode(item.CI_CHILD.IDCMDBCI.toString()) == null) {
            var _n = new ItHelpCenter.ComponentMindFusionManagerDiagramingProvider.TNode(item.CI_CHILD.IDCMDBCI.toString(), item.CI_CHILD.CI_GENERICNAME);
            _n.Id = item.CI_CHILD.IDCMDBCI.toString();
            _n.Titulo = item.CI_CHILD.CI_GENERICNAME;
            _n.Text = this.GET_CIInformation(item.CI_CHILD);
            _n.Visible = true;
            //_n.BorderColor = new SolidColorBrush(Colors.Black);
            _n.Imagen = this.ByteToImage(item.CI_CHILD.CMDBCIDEFINE.BYTEIMAGE);
            DiagramMap.AddNode(_n);
            var _l = new ItHelpCenter.ComponentMindFusionManagerDiagramingProvider.TLink(_node, _n);
            _l.Text = item.CMDBCIDEFINERELATIONTYPE.CIDEFINERELATIONTYPE_NAME;
            DiagramMap.AddLink(_l);
        }
    }

    for (var i = 0; i < AssetManager.SETCI.CIRELATIONListParent.length; i++) {
        var item = AssetManager.SETCI.CIRELATIONListParent[i];
        if (DiagramMap.FindNode(item.CI_PARENT.IDCMDBCI.toString()) == null) {
            var _n = new ItHelpCenter.ComponentMindFusionManagerDiagramingProvider.TNode(item.CI_PARENT.IDCMDBCI.toString(), item.CI_PARENT.CI_GENERICNAME);
            _n.Id = item.CI_PARENT.IDCMDBCI.toString();
            _n.Titulo = item.CI_PARENT.CI_GENERICNAME;
            _n.Text = this.GET_CIInformation(item.CI_PARENT);
            _n.Visible = true;
            //_n.BorderColor = new SolidColorBrush(Colors.Black);
            _n.Imagen = this.ByteToImage(item.CI_CHILD.CMDBCIDEFINE.BYTEIMAGE);
            DiagramMap.AddNode(_n);
            var _l = new ItHelpCenter.ComponentMindFusionManagerDiagramingProvider.TLink(_node, _n);
            _l.Text = item.CMDBCIDEFINERELATIONTYPE.CIDEFINERELATIONTYPE_NAME;
            DiagramMap.AddLink(_l);
        }
    }
    DiagramMap.BuildDiagramRelations(_Diag, true);
}

ItHelpCenter.CMDB.DiagramMap.DiagramMapProvider.prototype.ByteToImage = function (imageData) {
    if (imageData == null)
        return null;

    var blob = new Blob([new Uint8Array(imageData)]);
    var url = window.URL.createObjectURL(blob);
    //return url;

    var image = new TVclImagen();
    image.Src = url;
    //image.This.Width = "32px";
    image.This.style.width = "24px";
    image.This.style.height = "24px";
    //image.This.style.float = "right";
    return image.This;
    //BitmapImage biImg = new BitmapImage();
    //MemoryStream ms = new MemoryStream(imageData, 0, imageData.Length);
    //biImg.SetSource(ms);

    //ImageSource imgSrc = biImg as ImageSource;

    //return imgSrc;
}

ItHelpCenter.CMDB.DiagramMap.DiagramMapProvider.prototype.GET_CIInformation = function (_CI) {
    var _Information = "";
    _Information += "Name: " + _CI.CI_GENERICNAME + '\n';
    _Information += "Serial Number: " + _CI.CI_SERIALNUMBER + '\n';
    _Information += "CI DEfine: " + _CI.CMDBCIDEFINE.CIDEFINE_NAME + '\n';
    _Information += "Date In: " + _CI.CI_DATEIN + '\n';
    _Information += "State: " + _CI.IDCMDBCISTATE + '\n';
    _Information += "Date out: " + _CI.CI_DATEOUT + '\n';
    _Information += "Brand: " + _CI.CMDBBRAND.BRAND_NAME + '\n';
    _Information += "Descripcion: " + _CI.CI_DESCRIPTION;
    return _Information;
}
