﻿var TSQL = function (MenuObject,inObject, incallback) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.MenuObject = MenuObject;
    //debugger;
    var spPrincipal = new TVclStackPanel(inObject, "2", 2, [[9, 3], [9, 3], [9, 3], [9, 3], [9, 3]]);
    var DivBotonera = _this.MenuObject.GetDivData(spPrincipal.Column[1], spPrincipal.Column[0]);
    var stackPanelBotonera = new TVclStackPanel(DivBotonera, "1", 1, [[12], [12], [12], [12], [12]]);
    this.stackPanelHeader = new TVclStackPanel(stackPanelBotonera.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);

    // var MenuObjetNewCase = UsrCfg.Properties.MenuProfiler.NewPage(_this.MenuObject);
    var stackPanel = new TVclStackPanel(spPrincipal.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
    stackPanel.Column[0].This.style.backgroundColor = "#FAFAFA";
    stackPanel.Column[0].This.style.color = "#797979";
    if (SysCfg.App.Properties.Device == SysCfg.App.TDevice.Desktop) {
        stackPanel.Row.This.style.margin = "0px 0px";
        //stackPanel.Column[0].This.style.padding = "20px 20px";
        stackPanel.Column[0].This.style.border = "1px solid rgba(0, 0, 0, 0.08)";
        stackPanel.Column[0].This.style.boxShadow = " 0 0px 8px 0 rgba(0, 0, 0, 0.06), 0 1px 0px 0 rgba(0, 0, 0, 0.02);";
        stackPanel.Column[0].This.style.marginTop = "15px";

        //stackPanelBotonera.Row.This.style.margin = "0px 14px";
        stackPanelBotonera.Column[0].This.style.backgroundColor = "#FAFAFA";
        stackPanelBotonera.Column[0].This.style.color = "#797979";
        stackPanelBotonera.Column[0].This.style.border = "1px solid rgba(0, 0, 0, 0.08)";
        stackPanelBotonera.Column[0].This.style.boxShadow = " 0 0px 8px 0 rgba(0, 0, 0, 0.06), 0 1px 0px 0 rgba(0, 0, 0, 0.02);";
        stackPanelBotonera.Column[0].This.style.marginTop = "15px";
        stackPanelBotonera.Column[0].This.style.paddingRight = "25px";
        
    }
    else {
        stackPanel.Row.This.classList.remove("row");
        stackPanel.Row.This.style.marginTop = "-20px";
        stackPanel.Row.This.style.width = "100%";
        //stackPanel.Row.This.style.paddingLeft = "1px";
        //stackPanel.Row.This.style.paddingRight = "1px";
        stackPanel.Column[0].This.style.padding = "0px";
    }
    
    //0 date-time
    //2 date
    //3 time
    
    this.stackPanelHeader.Row.This.style.minHeight = "39px";
    this.stackPanelHeader.Row.This.style.marginBottom = "10px";
    var label = new TVcllabel(this.stackPanelHeader.Column[0].This, "", TlabelType.H0);
    label.This.style.fontSize = "24px;"
    label.This.style.color = "#999"
    label.Text = "SQL";
    label.This.style.marginTop = "20px";
    this.stackPanelBody = new TVclStackPanel(stackPanel.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);

    this.Object = this.stackPanelBody.Column[0].This;
    //this.Object = inObject;
    this.Callback = incallback;
    this.Mythis = "TSQL";

    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1", "Result SQL");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2", "SQL Open");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3", "SQL Execute");

    _this.CreateLayout();
}
TSQL.prototype.CreateLayout = function () {
    var _this = this.TParent();
    var VclStackPanelRowA = new TVclStackPanel(_this.stackPanelHeader.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
    //var VclStackPanelRowA0 = new TVclStackPanel(VclStackPanelRowA.Column[0].This, "1", 3, [[2, 5, 5], [2, 5, 5], [2, 5, 5], [2, 5, 5], [2, 5, 5]]);

    //var VclStackPanelRowOptionButton = new TVclStackPanel(VclStackPanelRowA0.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
    //var VclStackPanelRowOptionButtonFila1 = new TVclStackPanel(VclStackPanelRowOptionButton.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
    //var VclStackPanelRowOptionButtonFila2 = new TVclStackPanel(VclStackPanelRowOptionButton.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);


    this.VclStackPanelContMemTable = new TVclStackPanel(_this.Object, "1", 1, [[12], [12], [12], [12], [12]]);
    this.MemoSQL = new TVclMemo(VclStackPanelRowA.Column[0].This, "");
    _this.MemoSQL.Text = "SELECT * FROM CMDBCI WHERE IDCMDBCI<=10";
    _this.MemoSQL.VCLType = TVCLType.BS;
    _this.MemoSQL.Rows = 4;
    this.MemoResult = new TVclMemo(VclStackPanelRowA.Column[0].This, "");
    _this.MemoResult.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1");
    _this.MemoResult.VCLType = TVCLType.BS;
    _this.MemoResult.Rows = 4;
    _this.MemoResult.This.disabled = true;
    this.MemoSQL.This.style.marginTop = "0px";
    this.MemoResult.This.style.marginTop = "20px";
    this.MemoSQL.This.style.width = "100%";
    this.MemoResult.This.style.width = "100%";
    var divButton = document.createElement("div");
    VclStackPanelRowA.Column[0].This.appendChild(divButton);
    divButton.style.marginTop = "20px";

    var btnSQLOpen = new TVclButton(divButton, "");
    btnSQLOpen.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2");
    btnSQLOpen.VCLType = TVCLType.BS;
    btnSQLOpen.resize = 20;
    btnSQLOpen.This.classList.add("btn-info");
    btnSQLOpen.onClick = function () {

        $(_this.VclStackPanelContMemTable.Column[0].This).html("");
        var Open = _this.OpenDataSet(_this.MemoSQL.Text);
        var UnDataSet = Open.DataSet;
        _this.ContMemTable = new TVclStackPanel(_this.VclStackPanelContMemTable.Column[0].This, "1", 1, [[12], [12], [12], [12], [12]]);
        _this.ContMemTable.Row.This.style.marginTop = "20px";
        _this.GridCFView = new Componet.GridCF.TGridCFView(_this.ContMemTable.Column[0].This, function (OutRes) { }, null);
        //_this.GridCFView.VclGridCF.ResponsiveCont.style.border = "0px solid #c4c4c4";
        //_this.GridCFView.IsPagination = false;
        if (_this.detectIE()) {
          
            setTimeout(function () {
                _this.GridCFView.loadDataSetColumns(UnDataSet);
                _this.GridCFView.NumberRowVisible = 5;
                var VTCfgFactoryConfig = new Componet.GridCF.TGridCFView.TCfg();
                VTCfgFactoryConfig.ToolBar.ButtonImg_DeleteBar.Active = true;
                VTCfgFactoryConfig.ToolBar.ButtonImg_EditBar.Active = true;
                VTCfgFactoryConfig.ToolBar.ButtonImg_AddBar.Active = true;
                VTCfgFactoryConfig.ColumnsOptionsDef.InsertActiveEditor = true;
                VTCfgFactoryConfig.ColumnsOptionsDef.UpdateActiveEditor = true;
                VTCfgFactoryConfig.ToolBar.ButtonImg_EditToolsBar.Visible = true;
                VTCfgFactoryConfig.ToolBar.ButtonImg_DatailsViewBar.ChekedClickDown = true;
                //_this.GridCFView.GridCFToolbar.NavTabControls.BackgroundHeader = "#2B569A";
                VTCfgFactoryConfig.ToolBar.ButtonImg_EditToolsBar.ChekedClickDown = true;
                _this.GridCFView.LoadTCfg(VTCfgFactoryConfig);
                _this.GridCFView.loadDataSetRecords();
                _this.GridCFView.AutoTranslete = true;
                //window.scrollTo(0, 200);
                _this.GridCFView.loaderShow(false);

            }, 0);
        }
        else {
            
            _this.GridCFView.loaderShow(true);
            setTimeout(function () {
                _this.GridCFView.loadDataSetColumns(UnDataSet);
                _this.GridCFView.NumberRowVisible = 5;
                var VTCfgFactoryConfig = new Componet.GridCF.TGridCFView.TCfg();
                VTCfgFactoryConfig.ToolBar.ButtonImg_DeleteBar.Active = true;
                VTCfgFactoryConfig.ToolBar.ButtonImg_EditBar.Active = true;
                VTCfgFactoryConfig.ToolBar.ButtonImg_AddBar.Active = true;
                VTCfgFactoryConfig.ColumnsOptionsDef.InsertActiveEditor = true;
                VTCfgFactoryConfig.ColumnsOptionsDef.UpdateActiveEditor = true;
                VTCfgFactoryConfig.ToolBar.ButtonImg_EditToolsBar.Visible = true;
                VTCfgFactoryConfig.ToolBar.ButtonImg_DatailsViewBar.ChekedClickDown = true;
                //_this.GridCFView.GridCFToolbar.NavTabControls.BackgroundHeader = "#2B569A";
                VTCfgFactoryConfig.ToolBar.ButtonImg_EditToolsBar.ChekedClickDown = true;
                _this.GridCFView.LoadTCfg(VTCfgFactoryConfig);
                _this.GridCFView.loadDataSetRecords();
                _this.GridCFView.AutoTranslete = true;
        //window.scrollTo(0, 200);
                _this.GridCFView.loaderShow(false);
            }, 0);
        }
       
    }

    var btnSQLExecute = new TVclButton(divButton, "");
    btnSQLExecute.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3");
    btnSQLExecute.VCLType = TVCLType.BS;
    btnSQLOpen.This.classList.add("btn-success");
    
    btnSQLExecute.onClick = function () {
        var Exec = _this.OpenDataSet(_this.MemoSQL.Text);
        if (!Exec.ResErr.NotError) {
            alert("Sql Error");
        };
    }
    btnSQLOpen.This.style.width = "50%";
    btnSQLExecute.This.style.width = "50%";
    btnSQLOpen.This.click();
}

TSQL.prototype.detectIE = function () {

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        return true;
    }
    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        return true;
    }

    return false;

}
TSQL.prototype.Exec = function (StrSQL) {

    var _this = this.TParent();
    var Exec = new SysCfg.DB.SQL.Properties.TExec;
    SysCfg.Error.Properties.ResErrfill(Exec.ResErr);
    Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddBoolean(UsrCfg.InternoDemoNames.DMTABLA.FDBINARIO.FieldName, false, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoDemoNames.DMTABLA.FDCADENA.FieldName, "Salvador", SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoDemoNames.DMTABLA.FDDATETIME.FieldName, new Date(1970, 1, 1, 0, 0, 0), SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoDemoNames.DMTABLA.FDENTRO.FieldName, 40, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDouble(UsrCfg.InternoDemoNames.DMTABLA.FDFLOTANTE.FieldName, 19.16, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoDemoNames.DMTABLA.FDTEXTO.FieldName, "Prueba completa", SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoDemoNames.DMTABLA.IDDMTABLA.FieldName, 1, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Exec = SysCfg.DB.SQL.Methods.Exec(StrSQL, Param.ToBytes());
        _this.MemoResult.Text = Exec.ResErr.Mesaje;
    }
    finally {
        Param.Destroy();
    }
    return (Exec);
}
TSQL.prototype.OpenDataSet = function (StrSQL) {
    var _this = this.TParent();
    var OpenDataSet = new SysCfg.DB.SQL.Properties.TOpenDataSet;
    SysCfg.Error.Properties.ResErrfill(OpenDataSet.ResErr);
    Param = new SysCfg.Stream.Properties.TParam();
    try {
        Param.Inicialize();
        Param.AddBoolean(UsrCfg.InternoDemoNames.DMTABLA.FDBINARIO.FieldName, false, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddString(UsrCfg.InternoDemoNames.DMTABLA.FDCADENA.FieldName, "Salvador", SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDateTime(UsrCfg.InternoDemoNames.DMTABLA.FDDATETIME.FieldName, new Date(1970, 1, 1, 0, 0, 0), SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoDemoNames.DMTABLA.FDENTRO.FieldName, 40, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddDouble(UsrCfg.InternoDemoNames.DMTABLA.FDFLOTANTE.FieldName, 19.16, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddText(UsrCfg.InternoDemoNames.DMTABLA.FDTEXTO.FieldName, "Prueba completa", SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        Param.AddInt32(UsrCfg.InternoDemoNames.DMTABLA.IDDMTABLA.FieldName, 1, SysCfg.DB.Properties.TMotor.None, SysCfg.DB.Properties.TExtra.None, SysCfg.DB.Properties.TStyle.Normal);
        OpenDataSet = SysCfg.DB.SQL.Methods.OpenDataSet(StrSQL, Param.ToBytes());
        if (OpenDataSet.ResErr.NotError) {
            if (OpenDataSet.DataSet.RecordCount > 0) {
                //alert("Count:" + OpenDataSet.DataSet.RecordCount);
            }
            else {
                OpenDataSet.ResErr.NotError = true;
            }
        }
        else {
        }
        _this.MemoResult.Text = OpenDataSet.ResErr.Mesaje;
    }
    finally {
        Param.Destroy();
    }
    return (OpenDataSet);
}
function formatJSONDate2(currentTime) {

    var date = "";
    try {
        //var dateString = jsonDate.substr(6);
        //var currentTime = new Date(parseInt(dateString));
        var month = currentTime.getMonth() + 1;
        var day = currentTime.getDate();
        var year = currentTime.getFullYear();
        var hour = currentTime.getHours();
        var min = currentTime.getMinutes();
        var seg = currentTime.getSeconds();
        date = pad(day, 2) + "/" + pad(month, 2) + "/" + year + " " + pad(hour, 2) + ":" + pad(min, 2) + ":" + pad(seg, 2);
    } catch (e) {
        SysCfg.Log.Methods.WriteLog("frUSQL.js formatJSONDate2", e);
        // date = formatJSONDate2(new Date(1970, 1, 1, 0, 0, 0));
    }
    return date;
}
function pad(str, max) {
    str = str.toString();
    return str.length < max ? pad("0" + str, max) : str;
}