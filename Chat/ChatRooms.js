﻿var TChatSupport = function (inObject, incallback, inIDCMDBCI) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();

    this.IsCreateRoom = false;

    this.IDCMDBCI = inIDCMDBCI;

    this.Modal.Body.This.innerHTML = "";
    this.Object = this.Modal.Body.This;
    _this.Modal._inObjectHTMLBack = inObject;
    _this.name = "TChatRooms";

    this.Callback = incallback;
    this.NameForm = "ChatRoomsSuport"
    this.Mythis = "TChatRooms";
    this.MyRoom = null;
    this.IDSMCHATASSIGNED = null;

    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1", "Complete the following fields to start the online support");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2", "Subject");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3", "Description");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines4", "Start Chat");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines5", "Write a message");

    _this.CreateLayout();


}.Implements(new Source.Page.Properties.PageModal());

TChatSupport.prototype.CreateLayout = function () 
{
    var _this = this.TParent();

    var sp_Principal = new TVclStackPanel(_this.Object, "container", 1, [[12], [12], [12], [12], [12]]);

    var sp_Titulo = new TVclStackPanel(sp_Principal.Column[0].This, "zonaTitulo", 1, [[12], [12], [12], [12], [12]]);

    var sp_Detalle = new TVclStackPanel(sp_Principal.Column[0].This, "zonaDetalle", 1, [[12], [12], [12], [12], [12]]);

    var sp_Asunto = new TVclStackPanel(sp_Detalle.Column[0].This, "zonaAsunto", 2, [[1, 11], [1, 11], [1, 11], [1, 11], [1, 11]]);
    var sp_Descripcion = new TVclStackPanel(sp_Detalle.Column[0].This, "zonaDescripcion", 2, [[1, 11], [1, 11], [1, 11], [1, 11], [1, 11]]);
    var sp_Boton = new TVclStackPanel(sp_Detalle.Column[0].This, "zonaBoton", 2, [[10, 2], [10, 2], [10, 2], [10, 2], [10, 2]]);

    sp_Asunto.Row.MarginTop = "5px";
    sp_Descripcion.Row.MarginTop = "5px";
    sp_Boton.Row.MarginTop = "5px";

    var lblTitulo = new TVcllabel(sp_Titulo.Column[0].This, "", TlabelType.H0);
    lblTitulo.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines1");
    lblTitulo.VCLType = TVCLType.BS;
    lblTitulo.This.style.fontSize = "18px";
    
    var lblAsunto = new TVcllabel(sp_Asunto.Column[0].This, "", TlabelType.H0);
    lblAsunto.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines2");
    lblAsunto.VCLType = TVCLType.BS;

    var txtAsunto = new TVclTextBox(sp_Asunto.Column[1].This, "txtAsunto");
    txtAsunto.Text = "";
    txtAsunto.VCLType = TVCLType.BS;


    var lblDescripcion = new TVcllabel(sp_Descripcion.Column[0].This, "", TlabelType.H0);
    lblDescripcion.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines3");
    lblDescripcion.VCLType = TVCLType.BS;

    var Memo1 = new TVclMemo(sp_Descripcion.Column[1].This, "txtDescription");
    Memo1.Text = "";
    Memo1.VCLType = TVCLType.BS;
    Memo1.Rows = 15;

    var lblStartChat = new TVcllabel(sp_Asunto.Column[0].This, "", TlabelType.H0);
    lblStartChat.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines4");
    lblStartChat.VCLType = TVCLType.BS;
    lblStartChat.This.style.float = "right";

    var btnStartChat = new TVclImagen(sp_Boton.Column[1].This, "idimg");
    var dirimg = "image/32/My-cases1.png";
    btnStartChat.Src = dirimg;
    btnStartChat.Title = "Start chat";
    btnStartChat.Cursor = "pointer";
    btnStartChat.This.style.float = "right";
    btnStartChat.onClick = function () {
        var param = {
            'Subject': txtAsunto.Text,
            'Descripcion': Memo1.Text
        };
        param = JSON.stringify(param);

        direc = SysCfg.App.Properties.xRaiz + 'Chat/ChatRooms.svc/CreateChat';
        $.ajax({
            type: "POST", url: direc,
            data: param,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response)
            {
                if (response != null) {

                    var SMCHATASSIGNEDMEMBER;
                    for (var j = 0; j < response.SMCHATASSIGNEDMEMBERList.length; j++) {
                        if (response.SMCHATASSIGNEDMEMBERList[j].IDCMDBCI_MEMBER == _this.IDCMDBCI) {
                            SMCHATASSIGNEDMEMBER = response.SMCHATASSIGNEDMEMBERList[j];
                            break;
                        }
                    }

                    if (response.Waiting_Users > 0) {
                        response.ASSIGNED_TITLE = response.ASSIGNED_TITLE + " ( waiting users: " + bloqueChat.chat.Waiting_Users.toString() + " )";
                    }
                    else {
                        response.ASSIGNED_TITLE = response.ASSIGNED_TITLE;
                    }

                    _this.IDSMCHATASSIGNED = response.IDSMCHATASSIGNED;

                    _this.MyRoom = new TRoom(
                        "image/24/customer-service.png",
                        response.ASSIGNED_TITLE,
                        formatJSONDate(response.ASSIGNED_DATE),
                        response.IDSMCHATASSIGNED,
                        response.ASSIGNED_ENABLE,
                        SMCHATASSIGNEDMEMBER.ENABLE_MEMBER,
                        SMCHATASSIGNEDMEMBER.IDSMCHATASSIGNEDMEMBER,
                        _this,
                        _this.IDCMDBCI);
                    
                    $(sp_Principal.Column[0].This).html("");
                    _this.MyRoom.CreateLayout(sp_Principal.Column[0].This, 1);
                    _this.IsCreateRoom = true;
                }
            },
            error: function (response) {
                SysCfg.Log.Methods.WriteLog("ChatRooms.js TChatSupport.prototype.CreateLayout  Chat/ChatRooms.svc/CreateChat " + "Error no llamo al servicio CreateChat");
            }
        });
    }
};

TChatSupport.prototype.LaunchChat = function () {
    var _this = this.TParent();
    if (_this.MyRoom.VclDropDownPanel._IsOpen) {
        _this.MyRoom.LaunchChat(_this.IDSMCHATASSIGNED, 1);

        if (!_this.MyRoom.AssignedEnable) {
            if (_this.MyRoom.btnClose != null)
                $(_this.MyRoom.btnClose.This).css("display", "none");
            if (_this.MyRoom.sp_EnvioMensaje != null)
                $(_this.MyRoom.sp_EnvioMensaje.Row.This).css("display", "none");
        }

       
        if (!_this.MyRoom.EnableMember) {
            if (_this.MyRoom.btnDelete != null)
                $(_this.MyRoom.btnDelete.This).css("display", "none");
            _this.MyRoom.VclDropDownPanel.Visible = false;
            $(_this.Object).html("");
            _this.MyRoom = null;
            _this.CreateLayout();
        }
    }
}

TChatSupport.prototype.CloseRoom = function (IDSMCHATASSIGNED) {
    var _this = this.TParent();
    var index = { 'IDSMCHATASSIGNED': IDSMCHATASSIGNED };
    index = JSON.stringify(index);
    direc = SysCfg.App.Properties.xRaiz + 'Chat/ChatRooms.svc/CloseRoom';
    $.ajax({
        type: "POST", url: direc,
        data: index,
        contentType: "application/json; charset=utf-8",
        dataType: "json"        
    });
    //$(_this.Object).html("");
    //_this.CreateLayout();

    

    
    
    
}

TChatSupport.prototype.DeleteRoom = function (IDSMCHATASSIGNED, IDSMCHATASSIGNEDMEMBER) {
    var _this = this.TParent();
    var index = {
        'IDSMCHATASSIGNED': IDSMCHATASSIGNED,
        'IDSMCHATASSIGNEDMEMBER': IDSMCHATASSIGNEDMEMBER
    };
    index = JSON.stringify(index);
    direc = SysCfg.App.Properties.xRaiz + 'Chat/ChatRooms.svc/DeleteRoom';
    $.ajax({
        type: "POST", url: direc,
        data: index,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
    });    
}

/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////

var TChatRooms = function (inObject, incallback, inIDCMDBCI) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();
    this.NameForm = "ChatRooms"

    this.Modal.Body.This.innerHTML = "";
    this.Object = this.Modal.Body.This;
    this.Modal._inObjectHTMLBack = inObject;
    this.name = "TChatRooms";

    this.Callback = incallback;
    this.IDCMDBCI = inIDCMDBCI;
    this.RoomsList = new Array();
    this.Mythis = "TChatRooms";
    this.ColDetalle;

    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines6", "Chat rooms");

    _this.CreateChatLayout();
    _this.CreateChatRooms();

}.Implements(new Source.Page.Properties.PageModal());

TChatRooms.prototype.CreateChatLayout = function () {
    var _this = this.TParent();

    var sp_Principal = new TVclStackPanel(_this.Object, "container", 1, [[12], [12], [12], [12], [12]]);

    var sp_Titulo = new TVclStackPanel(sp_Principal.Column[0].This, "zonaTitulo", 1, [[12], [12], [12], [12], [12]]);

    var sp_Detalle = new TVclStackPanel(sp_Principal.Column[0].This, "zonaDetalle", 1, [[12], [12], [12], [12], [12]]);
    _this.ColDetalle = sp_Detalle.Column[0].This;

    var lblTitulo = new TVcllabel(sp_Titulo.Column[0].This, "", TlabelType.H0);
    lblTitulo.Text = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines6");
    lblTitulo.VCLType = TVCLType.BS;
    lblTitulo.This.style.fontSize = "18px";
};

TChatRooms.prototype.CreateChatRooms = function () {
    var _this = this.TParent();
    direc = SysCfg.App.Properties.xRaiz + 'Chat/ChatRooms.svc/GetRoomList';
    $.ajax({
        type: "POST", url: direc,
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            for (var i = 0; i < response.length; i++) {
                var existe = false;
                for (var j = 0; j < _this.RoomsList.length; j++) {
                    if (_this.RoomsList[j].Assigned == response[i].IDSMCHATASSIGNED) {
                        existe = true;

                        if (_this.RoomsList[j].VclDropDownPanel._IsOpen)
                            _this.RoomsList[j].LaunchChat(response[i].IDSMCHATASSIGNED, j);

                        if (!_this.RoomsList[j].AssignedEnable)
                        {
                            if (_this.RoomsList[j].btnClose != null)
                                $(_this.RoomsList[j].btnClose.This).css("display", "none");
                            if (_this.RoomsList[j].sp_EnvioMensaje != null)
                                $(_this.RoomsList[j].sp_EnvioMensaje.Row.This).css("display", "none");
                        }

                        if (!_this.RoomsList[j].EnableMember) {
                            if (_this.RoomsList[j].btnDelete != null)
                                $(_this.RoomsList[j].btnDelete.This).css("display", "none");
                            _this.RoomsList[j].VclDropDownPanel.Visible = false;
                        }
                        break;
                    }
                }

                if (!existe) {
                    var SMCHATASSIGNEDMEMBER;
                    for (var j = 0; j < response[i].SMCHATASSIGNEDMEMBERList.length; j++) {
                        if (response[i].SMCHATASSIGNEDMEMBERList[j].IDCMDBCI_MEMBER == _this.IDCMDBCI) {
                            SMCHATASSIGNEDMEMBER = response[i].SMCHATASSIGNEDMEMBERList[j];
                            break;
                        }
                    }

                    if (response.Waiting_Users > 0) {
                        response[i].ASSIGNED_TITLE = response[i].ASSIGNED_TITLE + " ( waiting users: " + bloqueChat.chat.Waiting_Users.toString() + " )";
                    }
                    else {
                        response[i].ASSIGNED_TITLE = response[i].ASSIGNED_TITLE;
                    }

                    var room = new TRoom(
                        "image/24/customer-service.png",
                        response[i].ASSIGNED_TITLE,
                        formatJSONDate(response[i].ASSIGNED_DATE),
                        response[i].IDSMCHATASSIGNED,
                        response[i].ASSIGNED_ENABLE,
                        SMCHATASSIGNEDMEMBER.ENABLE_MEMBER,
                        SMCHATASSIGNEDMEMBER.IDSMCHATASSIGNEDMEMBER,
                        _this,
                        _this.IDCMDBCI);
                    _this.RoomsList.push(room);
                    room.CreateLayout(_this.ColDetalle, i);
                }
            }
        },
        error: function (response) {
            SysCfg.Log.Methods.WriteLog("ChatRooms.js TChatRooms.prototype.CreateChatRooms Error no llamo al servicio GetRoomList");
        }
    });
}

TChatRooms.prototype.CloseRoom = function (IDSMCHATASSIGNED) {
    var _this = this.TParent();
    var index = { 'IDSMCHATASSIGNED': IDSMCHATASSIGNED };
    index = JSON.stringify(index);
    direc = SysCfg.App.Properties.xRaiz + 'Chat/ChatRooms.svc/CloseRoom';
    $.ajax({
        type: "POST", url: direc,
        data: index,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
    });
}

TChatRooms.prototype.DeleteRoom = function (IDSMCHATASSIGNED, IDSMCHATASSIGNEDMEMBER) {
    var _this = this.TParent();
    var index = {
        'IDSMCHATASSIGNED': IDSMCHATASSIGNED,
        'IDSMCHATASSIGNEDMEMBER': IDSMCHATASSIGNEDMEMBER
    };
    index = JSON.stringify(index);
    direc = SysCfg.App.Properties.xRaiz + 'Chat/ChatRooms.svc/DeleteRoom';
    $.ajax({
        type: "POST", url: direc,
        data: index,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
    });
}

/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////

var TRoom = function (inImage, inTitle, inDate, inAssigned, inAssignedEnable, inEnableMember, inIdAssignedMember, inChatRooms, inIDCMDBCI) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();

    this.Image = inImage;
    this.Title = inTitle;
    this.Date = inDate;
    this.Assigned = inAssigned;
    this.AssignedEnable = inAssignedEnable;
    this.EnableMember = inEnableMember;
    this.ChatRooms = inChatRooms;
    this.IdAssignedMember = inIdAssignedMember;
    this.Messages = new Array();
    this.IDCMDBCI = inIDCMDBCI;
    this.VclDropDownPanel;
    this.ColDetalle;
    this.btnClose = null;
    this.btnDelete = null;
    _this.sp_EnvioMensaje = null;

    this.Mythis = "TRoom";
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines5", "Write a message");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines6", "Close");
    UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines7", "Delete");
}

TRoom.prototype.CreateLayout = function (inObject, inIdx) {
    var _this = this.TParent();

    this.VclDropDownPanel = new TVclDropDownPanel(inObject, "");
    this.VclDropDownPanel.BoderTopColor = "#F39C12";
    this.VclDropDownPanel.BoderAllColor = "gray";
    this.VclDropDownPanel.BackgroundColor = "white";

    this.VclDropDownPanel.MarginBottom = "10px";
    if (!Source.Menu.IsMobil)
    {
        this.VclDropDownPanel.MarginLeft = "5px";
        this.VclDropDownPanel.MarginRight = "5px";
    }

    ////////////////////////////
    ////////////////////////////

    var sp_Cabecera = new TVclStackPanel(null, "cabecera" + inIdx, 5, [[1, 4, 2, 2, 2], [1, 4, 2, 2, 2], [1, 4, 2, 2, 2], [1, 4, 3, 2, 2], [1, 4, 3, 2, 2]]);
    sp_Cabecera.Row.MarginTop = "3px";
    sp_Cabecera.Row.MarginLeft = "0px";

    var img = Uimg(sp_Cabecera.Column[0].This, "idimage1" + inIdx, "image/24/text-message_2.png", "");

    var lblTituloChat = new TVcllabel(sp_Cabecera.Column[1].This, "", TlabelType.H0);
    lblTituloChat.Text = this.Title;
    lblTituloChat.VCLType = TVCLType.BS;

    if (!Source.Menu.IsMobil)
    {
        var lblFechaChat = new TVcllabel(sp_Cabecera.Column[2].This, "", TlabelType.H0);
        lblFechaChat.Text = this.Date;
        lblFechaChat.VCLType = TVCLType.BS;
    }

    if (this.AssignedEnable)
    {
        _this.btnClose = new TVclImagen(sp_Cabecera.Column[3].This, "");
        var dirimg = "image/24/Contact2.png";
        _this.btnClose.Src = dirimg;
        _this.btnClose.Title = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines6");
        _this.btnClose.Cursor = "pointer";
        _this.btnClose.onClick = function () {
            _this.AssignedEnable = false;
            //if (_this.ChatRooms.NameForm == "ChatRooms")
            //    _this.ChatRooms.CloseRoom(_this.Assigned);
            //else
                _this.ChatRooms.CloseRoom(_this.Assigned);
        }
    }

    if (this.EnableMember) {
        _this.btnDelete = new TVclImagen(sp_Cabecera.Column[4].This, "");
        var dirimg = "image/24/Trash-can.png";
        _this.btnDelete.Src = dirimg;
        _this.btnDelete.Title = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines7");
        _this.btnDelete.Cursor = "pointer";
        _this.btnDelete.onClick = function () {
            _this.EnableMember = false;
            //if (_this.ChatRooms.NameForm == "ChatRooms")
            //    _this.ChatRooms.DeleteRoom(_this.Assigned, _this.IdAssignedMember);
            //else
            _this.ChatRooms.DeleteRoom(_this.Assigned, _this.IdAssignedMember);
        }
    }
    else {
        //Escondo el row
        this.VclDropDownPanel.Visible = false;
    }
    this.VclDropDownPanel.AddToHeader(sp_Cabecera.Row.This);

    ////////////////////////////
    ////////////////////////////
    ////////////////////////////
    ////////////////////////////

    var sp_Mensajes = new TVclStackPanel(null, "mensajes" + inIdx, 1, [[12], [12], [12], [12], [12]]);
    _this.ColDetalle = sp_Mensajes.Column[0].This;
    sp_Mensajes.Row.Padding = "5px";
    this.VclDropDownPanel.AddToBody(sp_Mensajes.Row.This);

    ////////////////////////////
    ////////////////////////////
    ////////////////////////////
    ////////////////////////////

    if (this.AssignedEnable) {
        _this.sp_EnvioMensaje = new TVclStackPanel(null, "enviomensaje" + inIdx, 2, [[8, 4], [8, 4], [8, 4], [11, 1], [11, 1]]);
        _this.sp_EnvioMensaje.Row.Padding = "5px";

        var Memo1 = new TVclTextBox(_this.sp_EnvioMensaje.Column[0].This, "txtmessage_" + inIdx);
        Memo1.Text = "";
        Memo1.VCLType = TVCLType.BS;
        Memo1.This.placeholder = UsrCfg.Traslate.GetLangText(_this.Mythis, "Lines5");
        Memo1.This.style.height = "50px";

        var btnSendMessage = new TVclButton(_this.sp_EnvioMensaje.Column[1].This, "idimgsend_" + inIdx);
        btnSendMessage.VCLType = TVCLType.BS;
        var dirimg = "image/24/send-text-message.png";
        btnSendMessage.Src = dirimg;
        btnSendMessage.This.style.height = "50px";
        btnSendMessage.onClick = function () {
            if (Memo1.Text != "") {
                _this.SendMessage(Memo1.Text, _this.Assigned, inIdx);
                Memo1.Text = "";
            }
        }
        this.VclDropDownPanel.AddToBody(_this.sp_EnvioMensaje.Row.This);
    }

    ////////////////////////////
    ////////////////////////////
    ////////////////////////////
    ////////////////////////////

    _this.LaunchChat(_this.Assigned, inIdx);
}

TRoom.prototype.LaunchChat = function (IDSMCHATASSIGNED, inIdx) {
    var _this = this.TParent();
    var param = { 'IDSMCHATASSIGNED': IDSMCHATASSIGNED };
    param = JSON.stringify(param);
    direc = SysCfg.App.Properties.xRaiz + 'Chat/ChatRooms.svc/GetChat';
    $.ajax({
        type: "POST", url: direc,
        data: param,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            if (response != null) {
                var SMCHATASSIGNEDMEMBER;
                for (var j = 0; j < response.SMCHATASSIGNEDMEMBERList.length; j++) {
                    if (response.SMCHATASSIGNEDMEMBERList[j].IDCMDBCI_MEMBER == _this.IDCMDBCI) {
                        SMCHATASSIGNEDMEMBER = response.SMCHATASSIGNEDMEMBERList[j];
                        break;
                    }
                }
                _this.AssignedEnable = response.ASSIGNED_ENABLE;
                _this.EnableMember = SMCHATASSIGNEDMEMBER.ENABLE_MEMBER;


                for (var i = 0; i < response.SMCHATList.length; i++) {
                    var existe = false;
                    for (var j = 0; j < _this.Messages.length; j++) {
                        if (_this.Messages[j].Message.IDSMCHAT == response.SMCHATList[i].IDSMCHAT) {
                            existe = true;
                            break;
                        }
                    }

                    if (!existe) {
                        var objMessage = new TMessage(response.SMCHATList[i], _this.IDCMDBCI);
                        _this.Messages.push(objMessage);
                        objMessage.CreateLayout(_this.ColDetalle, i);
                    }
                }
            }
        },
        error: function (response) {
            SysCfg.Log.Methods.WriteLog("ChatRooms.js TRoom.prototype.LaunchChat Error no llamo al servicio LaunchChat");
        }
    });
}

TRoom.prototype.SendMessage = function (txtmessage, IDSMCHATASSIGNED, inIdx) {
    var _this = this.TParent();
    var index = { 'IDSMCHATASSIGNED': IDSMCHATASSIGNED, 'txtmessage': txtmessage };
    index = JSON.stringify(index);
    direc = SysCfg.App.Properties.xRaiz + 'Chat/ChatRooms.svc/SendMessage';
    $.ajax({
        type: "POST", url: direc,
        data: index,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
    });
    _this.LaunchChat(IDSMCHATASSIGNED, inIdx);
}

/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////

var TMessage = function (inMessage, inIDCMDBCI) {
    this.TParent = function () {
        return this;
    }.bind(this);
    var _this = this.TParent();

    this.Message = inMessage;
    this.IDCMDBCI = inIDCMDBCI;
}

TMessage.prototype.CreateLayout = function (inObject, inIdx) {
    var _this = this.TParent();

    var sp_Mensaje = new TVclStackPanel(inObject, "mensaje" + inIdx, 4, [[1, 3, 5, 3], [1, 3, 5, 3], [1, 3, 5, 3], [1, 3, 5, 3], [1, 3, 5, 3]]);
    sp_Mensaje.Row.MarginBottom = "3px";

    var lblMessage = new TVcllabel(sp_Mensaje.Column[2].This, "", TlabelType.H0);
    lblMessage.Text = this.Message.CHATMESSAGES;
    lblMessage.VCLType = TVCLType.BS;
    lblMessage.This.style.fontWeight = "normal";


    var lblDate = new TVcllabel(sp_Mensaje.Column[3].This, "", TlabelType.H0);
    lblDate.Text = formatJSONDateTime(this.Message.EVENTDATE);
    lblDate.VCLType = TVCLType.BS;
    lblDate.This.style.fontWeight = "normal";
    lblDate.This.style.fontSize = "12px";
    lblDate.This.style.color = "#A7AEBE";

    sp_Mensaje.Column[2].This.style.paddingTop = "4px";
    sp_Mensaje.Column[2].This.style.borderRadius = "6px";

    if (this.Message.SMCHATASSIGNEDMEMBER.IDCMDBCI_MEMBER == this.IDCMDBCI) //soy yo
    {
        Uimg(sp_Mensaje.Column[0].This, "idimage1" + inIdx, "image/24/user-blue2.png", "");
        sp_Mensaje.Column[2].This.style.backgroundColor = "#D2D6DE";
    }
    else
    {
        Uimg(sp_Mensaje.Column[0].This, "idimage1" + inIdx, "image/24/customer-service.png", "[ " + this.Message.SMCHATASSIGNEDMEMBER.CI_GENERICNAME + " ]");
        sp_Mensaje.Column[2].This.style.backgroundColor = "#F39C12";
        lblMessage.This.style.color = "white";

        if (!Source.Menu.IsMobil)
        {
            var lblNombreUsuario = new TVcllabel(sp_Mensaje.Column[1].This, "", TlabelType.H0);
            lblNombreUsuario.Text = "[ " + this.Message.SMCHATASSIGNEDMEMBER.CI_GENERICNAME + " ]";
            lblNombreUsuario.VCLType = TVCLType.BS;
            lblNombreUsuario.This.style.fontWeight = "normal";
            lblNombreUsuario.This.style.fontSize = "12px";
            lblNombreUsuario.This.style.color = "#A7AEBE";
            lblNombreUsuario.This.style.marginTop = "4px";
        }
    }
}
